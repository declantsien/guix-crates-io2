(define-module (crates-io co mp compiler-tools) #:use-module (crates-io))

(define-public crate-compiler-tools-0.1.0 (c (n "compiler-tools") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "087jy0dv8kr201521k9g9mdb9gvlv4k0yg17zj6xqq6wjn8xlc5y") (f (quote (("use_regex" "regex" "once_cell") ("default" "serde" "use_regex"))))))

(define-public crate-compiler-tools-0.1.1 (c (n "compiler-tools") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n9apra6pyhyphcyv273w157apki0g2qsanrglg15ahnw19sj048") (f (quote (("use_regex" "regex" "once_cell") ("default" "serde" "use_regex"))))))

(define-public crate-compiler-tools-0.1.2 (c (n "compiler-tools") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10qi299yzlj61i9li68ri7wwg0mhxikzbviqwcs1ff1kqacajd9a") (f (quote (("use_regex" "regex" "once_cell") ("default" "serde" "use_regex"))))))

(define-public crate-compiler-tools-0.1.3 (c (n "compiler-tools") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wrgrm8bigj79kiv7g1pi5439vj2s86xyqpj6fdzr2wly3znara2") (f (quote (("use_regex" "regex" "once_cell") ("default" "serde" "use_regex"))))))

(define-public crate-compiler-tools-0.1.4 (c (n "compiler-tools") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j4i0h4959fsmis2mi2w9d2jqa95ksld49pz6kh5jhqxprdfpsza") (f (quote (("use_regex" "regex" "once_cell") ("default" "serde" "use_regex"))))))

(define-public crate-compiler-tools-0.1.5 (c (n "compiler-tools") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fhq2cnc9vfj0zx1sf9girs1zy8yhfmwqbfp17kih9rkzd6039b1") (f (quote (("use_regex" "regex" "once_cell") ("default" "serde" "use_regex"))))))

(define-public crate-compiler-tools-0.1.6 (c (n "compiler-tools") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1h2r73v4yryc60n926xi123mhk1dcir9f83rfry6dm2yfyc3ffr8") (f (quote (("use_regex" "regex" "once_cell") ("default" "serde" "use_regex"))))))

(define-public crate-compiler-tools-0.1.7 (c (n "compiler-tools") (v "0.1.7") (d (list (d (n "once_cell") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rcxyj3qkx5ah69g53p934245mswl1q7ri50d9w5vx3qr9sx9s0s") (f (quote (("use_regex" "regex" "once_cell") ("default" "serde" "use_regex"))))))

(define-public crate-compiler-tools-0.2.0 (c (n "compiler-tools") (v "0.2.0") (d (list (d (n "regex") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14af0z1334p8h8ixjjjf4wlgdnzm4mslpylxlisn5shiaqqbv06c") (f (quote (("use_regex" "regex") ("default" "serde" "use_regex")))) (r "1.75.0")))

