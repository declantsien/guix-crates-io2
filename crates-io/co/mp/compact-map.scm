(define-module (crates-io co mp compact-map) #:use-module (crates-io))

(define-public crate-compact-map-0.1.0 (c (n "compact-map") (v "0.1.0") (d (list (d (n "heapless") (r "^0.8") (d #t) (k 0)))) (h "17m5npwpahzifxivq3x3fxk3z6g1flwicd2nhnf3md4vfdirmzyy") (f (quote (("map_try_insert") ("map_entry_replace") ("many_mut") ("extract_if") ("entry_insert"))))))

