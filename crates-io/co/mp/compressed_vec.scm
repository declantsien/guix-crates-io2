(define-module (crates-io co mp compressed_vec) #:use-module (crates-io))

(define-public crate-compressed_vec-0.1.0 (c (n "compressed_vec") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "enum_dispatch") (r "^0.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3") (f (quote ("into_bits"))) (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "scroll") (r "^0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "0y6g5cjkqyj4jamy25rgs14qrhwympl764gcg4zipcngmxh66b8c")))

