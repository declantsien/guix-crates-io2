(define-module (crates-io co mp components-arena-traits) #:use-module (crates-io))

(define-public crate-components-arena-traits-1.0.0 (c (n "components-arena-traits") (v "1.0.0") (h "0r6v55k7ccpqdi9dnhxyyzh6ml960y530nqci9a36fz5lkgz4bbx") (f (quote (("nightly") ("default" "nightly")))) (r "1.44")))

(define-public crate-components-arena-traits-1.0.1 (c (n "components-arena-traits") (v "1.0.1") (h "0w4i4943jp6ngmzcb39zka4swp84idmz13qqa9a5m0bzgd9fc629") (f (quote (("nightly") ("default" "nightly")))) (r "1.44")))

(define-public crate-components-arena-traits-1.1.0 (c (n "components-arena-traits") (v "1.1.0") (h "1lk0qak7lwq3sfaqsl3n6ghwmq9xx45r2g5rlq7yrajg095v6ari") (f (quote (("nightly") ("default" "nightly")))) (r "1.44")))

(define-public crate-components-arena-traits-1.1.1 (c (n "components-arena-traits") (v "1.1.1") (h "1hxy943ba1m6rg4r3c0arf61ikvdcipp34ggislwkylnri5s3fvm") (f (quote (("nightly") ("default" "nightly")))) (r "1.44")))

