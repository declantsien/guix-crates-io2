(define-module (crates-io co mp compression-module) #:use-module (crates-io))

(define-public crate-compression-module-0.2.0 (c (n "compression-module") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "module-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "pingora-core") (r "^0.2.0") (d #t) (k 0)) (d (n "pingora-proxy") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1zy6k9bbn2yr8izd0k27hg2s2j4664w5d2cxgq2wwkc70d3vy7wd")))

