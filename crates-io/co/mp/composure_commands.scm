(define-module (crates-io co mp composure_commands) #:use-module (crates-io))

(define-public crate-composure_commands-0.0.1 (c (n "composure_commands") (v "0.0.1") (d (list (d (n "composure") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)))) (h "114j1ag983fpmg77zgm7cn6yapkp57d09q730hwqpbfq35vryfnv")))

(define-public crate-composure_commands-0.0.2 (c (n "composure_commands") (v "0.0.2") (d (list (d (n "composure") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)))) (h "0vd2nabbb245k1iyfb0ahx9hl05sv18pg1w0jrnd7qy3zyg5khwq")))

