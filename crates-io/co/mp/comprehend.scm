(define-module (crates-io co mp comprehend) #:use-module (crates-io))

(define-public crate-comprehend-0.1.0 (c (n "comprehend") (v "0.1.0") (h "00g60sydbyd9dcpfjwlx0spm7l4ddrhykvgkzqm7m9r53sg18vrg")))

(define-public crate-comprehend-0.1.1 (c (n "comprehend") (v "0.1.1") (h "0y6l56m4kflk7qv3rsg8h12hfcwz1wdgqibnm277ml0rmf9a7ybi")))

