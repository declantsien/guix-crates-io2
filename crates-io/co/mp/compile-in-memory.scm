(define-module (crates-io co mp compile-in-memory) #:use-module (crates-io))

(define-public crate-compile-in-memory-0.0.1 (c (n "compile-in-memory") (v "0.0.1") (h "0aay15ppwvwraf39zsv8h8mplgznimfiw3ll8z2ccsiaaz82aprh")))

(define-public crate-compile-in-memory-0.2.0 (c (n "compile-in-memory") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "loader") (r "^0.2.1") (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 2)))) (h "0cwdcbjig4mk8acq6syxr22fg6ld00i60m3r24h2qbqhym0gpxam")))

