(define-module (crates-io co mp comptime) #:use-module (crates-io))

(define-public crate-comptime-0.0.1 (c (n "comptime") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19s3rbcswacqkf18alkyaljy6i9dygj3am9sn1hadqnrm540mlrf")))

(define-public crate-comptime-0.1.0 (c (n "comptime") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nll5rs82x9fdwhllwvm4x8dhrirglv3lvqpg644awm071rmpb82")))

(define-public crate-comptime-0.1.1 (c (n "comptime") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jr0vfc0hdj6iqdap7ld8kdc38j4znw3vnr931348yylhgavvq16")))

