(define-module (crates-io co mp compute-pi) #:use-module (crates-io))

(define-public crate-compute-pi-0.1.0 (c (n "compute-pi") (v "0.1.0") (d (list (d (n "rug") (r "^1.13.0") (d #t) (k 0)))) (h "13scnis3hhal7k1299gkrd3cwlflah9xyww93vw7qpl8i31qdb0r") (y #t)))

(define-public crate-compute-pi-0.1.1 (c (n "compute-pi") (v "0.1.1") (d (list (d (n "rug") (r "^1.13.0") (d #t) (k 0)))) (h "07lspaqcadbxvpmjh2dgaq5ikm1mchgwkdnj82x6vj6dlk4q98b8") (y #t)))

(define-public crate-compute-pi-0.1.2 (c (n "compute-pi") (v "0.1.2") (d (list (d (n "rug") (r "^1.13.0") (d #t) (k 0)))) (h "05s790g643ys2cqh7mjvmpm6xrm6ijcpbmq6p2hgzqbb19zwv4hl") (y #t)))

(define-public crate-compute-pi-0.1.3 (c (n "compute-pi") (v "0.1.3") (d (list (d (n "rug") (r "^1.13.0") (d #t) (k 0)))) (h "1qf210v5xl6qmjvjbr2n0hl1hwda82h0mb11g6iys804gv2bmgka") (y #t)))

(define-public crate-compute-pi-0.2.0 (c (n "compute-pi") (v "0.2.0") (d (list (d (n "rug") (r "^1.13.0") (d #t) (k 0)))) (h "1kk2cj8i0yywbj5p49wzx3vhzwv5aqwg5855fhwxni2llqjjs5mg") (y #t)))

(define-public crate-compute-pi-0.2.1 (c (n "compute-pi") (v "0.2.1") (d (list (d (n "rug") (r "^1.13.0") (d #t) (k 0)))) (h "0mr4ac50qhxvgw1m9xdfqb3bycpfz55n46639bxvxr9cay0ah1f9")))

(define-public crate-compute-pi-0.2.2 (c (n "compute-pi") (v "0.2.2") (d (list (d (n "rug") (r "^1.24") (d #t) (k 0)))) (h "0h8lmjc2hc8z4wm3638lp0978k5pnndqyvihi3ymldax4mqhbxc6")))

(define-public crate-compute-pi-0.2.3 (c (n "compute-pi") (v "0.2.3") (d (list (d (n "rug") (r "^1.24") (d #t) (k 0)))) (h "1w5y9l11yjqkimz4w2blw7jvvpjy7d5cmjzipwipd370p7qf15sc")))

(define-public crate-compute-pi-0.2.4 (c (n "compute-pi") (v "0.2.4") (d (list (d (n "rug") (r "^1.24") (d #t) (k 0)))) (h "0ab1vkb7vsdnm72mg8r5k3vgcic2nz4nl8kzfxicadx8gfmas8m5")))

(define-public crate-compute-pi-0.2.5 (c (n "compute-pi") (v "0.2.5") (d (list (d (n "rug") (r "^1.24") (d #t) (k 0)))) (h "1bccfn097qzfid9bdrqgjzp7n5hy488jvkndxrbsbdck0fms5bl8")))

