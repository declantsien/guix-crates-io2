(define-module (crates-io co mp component_group) #:use-module (crates-io))

(define-public crate-component_group-1.0.0 (c (n "component_group") (v "1.0.0") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "component_group_derive") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.14") (d #t) (k 0)) (d (n "specs-derive") (r "^0.3") (d #t) (k 2)))) (h "18d2ggaaajvjmzj03j8mq021nazffk24wl6r067mxmh4f2ng86fg")))

(define-public crate-component_group-2.0.0 (c (n "component_group") (v "2.0.0") (d (list (d (n "compiletest_rs") (r "^0.4") (f (quote ("stable"))) (d #t) (k 2)) (d (n "component_group_derive") (r "^2.0") (d #t) (k 0)) (d (n "specs") (r "^0.15") (d #t) (k 0)) (d (n "specs-derive") (r "^0.4") (d #t) (k 2)))) (h "16xcsq2ny1j19spp5sx0ra5vrmmx0q8vw2lgl20pqas9hzxr1rwg")))

(define-public crate-component_group-3.0.0 (c (n "component_group") (v "3.0.0") (d (list (d (n "compiletest_rs") (r "^0.5") (f (quote ("stable"))) (d #t) (k 2)) (d (n "component_group_derive") (r "^3.0") (d #t) (k 0)) (d (n "specs") (r "^0.16") (d #t) (k 0)) (d (n "specs-derive") (r "^0.4") (d #t) (k 2)))) (h "1iymlk8iyhvgdxk7x4mm0p2kq7hm48s8nxyyfr94d124kxqckz7s")))

