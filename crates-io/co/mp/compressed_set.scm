(define-module (crates-io co mp compressed_set) #:use-module (crates-io))

(define-public crate-compressed_set-0.1.0 (c (n "compressed_set") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)))) (h "04zm4xsx0v5snygqk3wr6bwgrf01gmg2g8r2ynaq6v8q2vykjhx1")))

