(define-module (crates-io co mp composure) #:use-module (crates-io))

(define-public crate-composure-0.0.1 (c (n "composure") (v "0.0.1") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vxcll9amgzqrwa2p71barkyqi9cqy8swi99widnm95mrg89bl28")))

(define-public crate-composure-0.0.2 (c (n "composure") (v "0.0.2") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.12") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bmvfixi9gqwz89k1hyivpl9w7giv1fjz58cxzmnsll1wanh14y6")))

