(define-module (crates-io co mp compressed_string) #:use-module (crates-io))

(define-public crate-compressed_string-1.0.0 (c (n "compressed_string") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "1ryhlrh11lary104jkz4gsaph82di7cg3m13qaah3ssrrnwiy6fk") (f (quote (("with_serde" "serde" "serde_derive"))))))

