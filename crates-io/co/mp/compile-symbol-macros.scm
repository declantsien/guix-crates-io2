(define-module (crates-io co mp compile-symbol-macros) #:use-module (crates-io))

(define-public crate-compile-symbol-macros-0.1.0 (c (n "compile-symbol-macros") (v "0.1.0") (d (list (d (n "docify") (r "^0.1") (d #t) (k 0)))) (h "1gcakssb0mk2k5z7idd6i89y8v8cvq2cfi06nzsmlagwbrwsw292")))

(define-public crate-compile-symbol-macros-0.1.1 (c (n "compile-symbol-macros") (v "0.1.1") (d (list (d (n "docify") (r "^0.1") (d #t) (k 0)))) (h "1yl5kw5yc8rlr9xp7047c94dfp7ygggbxdcfdvsphc60x0bvdhnx")))

(define-public crate-compile-symbol-macros-0.1.2 (c (n "compile-symbol-macros") (v "0.1.2") (d (list (d (n "docify") (r "^0.1") (d #t) (k 0)))) (h "0gqszv9zzxrn9qw4zacaknv4w1xy17g5s3pxgl34rcqm1m2frj5v")))

(define-public crate-compile-symbol-macros-0.1.3 (c (n "compile-symbol-macros") (v "0.1.3") (d (list (d (n "docify") (r "^0.1") (d #t) (k 0)))) (h "14yv8c8zdglw6r89dq5f0jlrpjar8asi6hz4c0dz6qkpx0gmvakz")))

(define-public crate-compile-symbol-macros-0.1.4 (c (n "compile-symbol-macros") (v "0.1.4") (d (list (d (n "docify") (r "^0.1") (d #t) (k 0)))) (h "1wspggill6yw8rnlprvyppx2cbbqgjr9285a3pxh6c0m0my3wylp")))

