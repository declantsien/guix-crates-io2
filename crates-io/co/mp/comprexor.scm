(define-module (crates-io co mp comprexor) #:use-module (crates-io))

(define-public crate-comprexor-0.1.4 (c (n "comprexor") (v "0.1.4") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "humansize") (r "^2.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)))) (h "0sdmk09gs79zhsclpmd5gx75cvgxgf0pqiv8s4hjxjzjas3fr1z3")))

(define-public crate-comprexor-0.1.5 (c (n "comprexor") (v "0.1.5") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "humansize") (r "^2.1.3") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "0820zkwjy7wqgv7gaxrpjd0hb35nj16rs4jsnxpvq4a5gfm1pphy")))

(define-public crate-comprexor-0.1.520 (c (n "comprexor") (v "0.1.520") (d (list (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "humansize") (r "^2.1.3") (d #t) (k 0)) (d (n "tar") (r "^0.4.39") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 0)))) (h "1jrqayzia8f98ikfamh8jrx0xiwlh85k796xvb0ynvbxmag8f906")))

