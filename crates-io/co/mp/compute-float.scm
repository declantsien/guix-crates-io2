(define-module (crates-io co mp compute-float) #:use-module (crates-io))

(define-public crate-compute-float-0.0.1 (c (n "compute-float") (v "0.0.1") (h "1s35p9y0vs1069iwg4q25rmnrzpby37zgkgmjxa9a5318cp0lchn") (y #t)))

(define-public crate-compute-float-0.1.0 (c (n "compute-float") (v "0.1.0") (h "0ydxrq69m0pdran7q9smm3qmddmccsmhcf0i9klma47lkxgdin4g")))

