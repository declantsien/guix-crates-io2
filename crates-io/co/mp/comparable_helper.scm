(define-module (crates-io co mp comparable_helper) #:use-module (crates-io))

(define-public crate-comparable_helper-0.5.2 (c (n "comparable_helper") (v "0.5.2") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v34ar2axhda9f46n7k4m84h9f9a3wd3fw065myqh1g0x81bld4k")))

(define-public crate-comparable_helper-0.5.3 (c (n "comparable_helper") (v "0.5.3") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00v2kdrzif25mzyqsn40k97vwl4pr9jp3a7sb67bim7k351a3haj")))

(define-public crate-comparable_helper-0.5.4 (c (n "comparable_helper") (v "0.5.4") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hsj0ysqxp6lm40m63z5wc3gfna833zyq2r7j4f0hqg84zikfm7v")))

