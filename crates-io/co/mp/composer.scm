(define-module (crates-io co mp composer) #:use-module (crates-io))

(define-public crate-composer-0.1.0 (c (n "composer") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1x2x3iswwj3xc91ycnqfgxf444yinr9n53w1fymdry20h5y5a8ai")))

(define-public crate-composer-0.2.1 (c (n "composer") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0ix02jam7dyrdfynpinl2qrz5230fh0bwiwvd09p9jj8lar543fq")))

