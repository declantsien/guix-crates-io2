(define-module (crates-io co mp completeq-rs) #:use-module (crates-io))

(define-public crate-completeq-rs-0.1.0 (c (n "completeq-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "async-std") (r "^1.11.0") (f (quote ("attributes" "default"))) (d #t) (k 2)) (d (n "async-timer-rs") (r "^0.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "19jjkj5xz3gjjhijg15kfjhvi3mpacib7fx4wg8l2szwrk9hzaxi")))

(define-public crate-completeq-rs-0.1.1 (c (n "completeq-rs") (v "0.1.1") (d (list (d (n "async-timer-rs") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "async-std") (r "^1.11.0") (f (quote ("attributes" "default"))) (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "1ayxpa58019ai7zkvd4ys0sqb7n9lbc2z234a5f143lqy27s2v0c")))

