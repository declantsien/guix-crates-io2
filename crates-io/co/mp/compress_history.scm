(define-module (crates-io co mp compress_history) #:use-module (crates-io))

(define-public crate-compress_history-0.1.0 (c (n "compress_history") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1na5i7ln9pa6jid8q8wfp6kskzgvla64bgpfqhylixh22292rxkg")))

(define-public crate-compress_history-0.1.1 (c (n "compress_history") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1zhykqn86a8fkwv1q99j55k0b2wgx2jcr4ras034gj7s5x8n737q")))

(define-public crate-compress_history-0.1.2 (c (n "compress_history") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "197vl6jdrlg88mj8crvca1cld1m9367vyk75x8kdilvrj9gvkw9j")))

(define-public crate-compress_history-0.1.3 (c (n "compress_history") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0kvkf51fgqcg8j0zil72xsyfnjs928jbwcqiy2k3nr07kmxjmjly")))

(define-public crate-compress_history-0.1.4 (c (n "compress_history") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "directories") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1w9b0piasrrknfaxxfakc9gzl3hn4nkbx7f3z9yrfmxj00injk29")))

(define-public crate-compress_history-0.2.0 (c (n "compress_history") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1xxnbq6r488fvzbsvl2nvr6gddaf9dh4z850ff9p90zmlasy78i0")))

