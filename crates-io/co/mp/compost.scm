(define-module (crates-io co mp compost) #:use-module (crates-io))

(define-public crate-compost-0.1.0 (c (n "compost") (v "0.1.0") (h "0426xgphz3kxy8c72khck2qmj8nmm3bv4ln8zsxchm97swbp9d2w")))

(define-public crate-compost-0.1.1 (c (n "compost") (v "0.1.1") (h "1pxbmg52jqipq46bixkwy1h4846305q04gdgdkawc0iwjlx57346")))

(define-public crate-compost-0.2.0 (c (n "compost") (v "0.2.0") (h "06050c37nz297x4dy35ihjwp6xj4pqgmm63gpqgm530vfzg4ag5n")))

(define-public crate-compost-0.3.0 (c (n "compost") (v "0.3.0") (h "1d55b5xamjcish66scyljallpvhf2hd774gl53rmr7pr5baipsvk")))

(define-public crate-compost-0.4.0 (c (n "compost") (v "0.4.0") (h "09d90qcjnfch2s6h62ng56p6pd6z6d9nbr2kjwjwp9c95c4f0mwi")))

