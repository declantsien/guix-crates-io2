(define-module (crates-io co mp compensated-summation) #:use-module (crates-io))

(define-public crate-compensated-summation-0.1.0 (c (n "compensated-summation") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)))) (h "17sdp42b9r92285j2a9v537jl3rpscwnydvp07dw69dwvdc4vcz7") (y #t)))

(define-public crate-compensated-summation-0.2.0 (c (n "compensated-summation") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)))) (h "113qplm42lm56r1vlwj34dryvf6rdxla5k1qy9923i2clfxknr7z") (y #t)))

(define-public crate-compensated-summation-0.3.0 (c (n "compensated-summation") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)))) (h "0s8llz649vzcpqqrswflhx6hw82hbhjy2zii26l1ri8kwm2326pd") (f (quote (("dev"))))))

