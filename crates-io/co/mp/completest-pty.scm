(define-module (crates-io co mp completest-pty) #:use-module (crates-io))

(define-public crate-completest-pty-0.3.0 (c (n "completest-pty") (v "0.3.0") (d (list (d (n "completest") (r "^0.3.0") (d #t) (k 0)) (d (n "ptyprocess") (r "^0.4.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "vt100") (r "^0.15.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0dg2y7fnhk1vy00kcm3h4sx0is7hm42i9vh2fjz82d24hs7i5xw3") (r "1.70.0")))

(define-public crate-completest-pty-0.3.1 (c (n "completest-pty") (v "0.3.1") (d (list (d (n "completest") (r "^0.3.1") (d #t) (k 0)) (d (n "ptyprocess") (r "^0.4.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "vt100") (r "^0.15.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0j50nf3274b8mbpy8bhp8f9qncv7rcfrl2xsbrjgcwm08dw0vmjz") (r "1.70.0")))

(define-public crate-completest-pty-0.4.0 (c (n "completest-pty") (v "0.4.0") (d (list (d (n "completest") (r "^0.4.0") (d #t) (k 0)) (d (n "ptyprocess") (r "^0.4.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "vt100") (r "^0.15.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0mg0r4m548mi9gbsmms2arx3qqhjhzzx3f1xiffn8vf0j56ilfkw") (r "1.70.0")))

(define-public crate-completest-pty-0.5.0 (c (n "completest-pty") (v "0.5.0") (d (list (d (n "completest") (r "^0.4.0") (d #t) (k 0)) (d (n "ptyprocess") (r "^0.4.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "vt100") (r "^0.15.2") (d #t) (t "cfg(unix)") (k 0)))) (h "11fib7yly355yjkzjp8bj5rqvv83xlm7mrkbc6bqyq3zw9r14v9a") (r "1.70.0")))

