(define-module (crates-io co mp complex) #:use-module (crates-io))

(define-public crate-complex-0.0.1 (c (n "complex") (v "0.0.1") (h "1h7nma50lbnl73p80abm48rzjj3amb590lmy45wavjn0qzqglizi")))

(define-public crate-complex-0.1.0 (c (n "complex") (v "0.1.0") (h "09v3x8lfy3b2rcvf89g8ij60jzg2fzw3ysdn6458r8svn5j27fql")))

(define-public crate-complex-0.1.1 (c (n "complex") (v "0.1.1") (h "0bfyp7fjbsfn9aknzjvnlaqb3nl708i4hm3qhp2n2zf36aryv2k4")))

(define-public crate-complex-0.2.0 (c (n "complex") (v "0.2.0") (h "0qwymfq0jk6ahlds2j7jj75pkfwljmz1w1rapk7rcv9d9ivj2fxp")))

(define-public crate-complex-0.3.0 (c (n "complex") (v "0.3.0") (h "153q53m4m3b2cvdld1sis400200i2010gvf297shil1yfghxxzww")))

(define-public crate-complex-0.4.0 (c (n "complex") (v "0.4.0") (h "0zwav4j6c4khpn9qasd978c3r31df6by3jfl23x85q3p1hiavbf6")))

(define-public crate-complex-0.4.1 (c (n "complex") (v "0.4.1") (h "1cbf24qj71ic7wymak3nrcc3x03v9iwnb7nwxxh0gfdalaljcw7v")))

(define-public crate-complex-0.5.0 (c (n "complex") (v "0.5.0") (h "0pvd4br6p8xl1i7a81w4bq0bda8gsbhlcqx55ac2w1i0sx3pzx9z")))

(define-public crate-complex-0.6.0 (c (n "complex") (v "0.6.0") (h "0payq7ydpz8sj82y2wdi42hadg6mp69fji3hvpzd61m7cdaydrh4")))

(define-public crate-complex-0.7.0 (c (n "complex") (v "0.7.0") (h "1mix0w3d8bhsy4aiwf00skc025kmwi4x6i4yiwg78k4rhvbhff6i")))

(define-public crate-complex-0.7.1 (c (n "complex") (v "0.7.1") (h "1d7r6airxppmy08v6kgnfg9imm4ifg9qz9qlfiz6bxw8jpc8vdrk")))

(define-public crate-complex-0.7.5 (c (n "complex") (v "0.7.5") (h "0lynps8sla2pbc4gk6lpnn8b59lfa3l0hgri43rybjz61y29y7gm")))

(define-public crate-complex-0.8.0 (c (n "complex") (v "0.8.0") (h "1b36h9z9qq7rjc6hscq5z9b564ygcaapannjl5avmhdwnac47i2r")))

