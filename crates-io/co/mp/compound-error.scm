(define-module (crates-io co mp compound-error) #:use-module (crates-io))

(define-public crate-compound-error-0.1.0 (c (n "compound-error") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "101g5hcsxqiigi43ngzs8q08sdjl7bkw5dzqb86l8gsjkq30k041")))

(define-public crate-compound-error-0.1.1 (c (n "compound-error") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0n6wqifd193hy8ib5jhamk3ghvspmk5pbn24phmrx5jgvlls40xb")))

(define-public crate-compound-error-0.1.2 (c (n "compound-error") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1i8pns1fqm53ggrvd3m7s597rrk684v2c0w2nlvp10bdl6d62vqh")))

(define-public crate-compound-error-0.1.3 (c (n "compound-error") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01j3hg6phbcdxyhljjbna5nj3xzbmzi2vxy83p66jk66pmqx4csh")))

(define-public crate-compound-error-0.1.4 (c (n "compound-error") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1a7lxqw37ql1kllympqmfdjcknqwn9hr903cv2l91ih8gppkv3rk")))

(define-public crate-compound-error-0.1.5 (c (n "compound-error") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1w3146qk4yzlw5d45rxgwzzcn5nanwi1lqwvlgk3niq4jwd2kvg2")))

