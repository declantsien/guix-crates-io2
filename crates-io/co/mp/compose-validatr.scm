(define-module (crates-io co mp compose-validatr) #:use-module (crates-io))

(define-public crate-compose-validatr-0.1.0 (c (n "compose-validatr") (v "0.1.0") (d (list (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "0gl0zbzmc5dbdsq1b3sbq6xwf2nfnsrvc708s9s27vq9q9mn7b70")))

(define-public crate-compose-validatr-0.1.1 (c (n "compose-validatr") (v "0.1.1") (d (list (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "1jiqrcq1wyxa8jzjmxyp60kmqq93xa9hdk45gm5wphy71mwp7hd7")))

(define-public crate-compose-validatr-0.1.2 (c (n "compose-validatr") (v "0.1.2") (d (list (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "0ndr6mhajpji2q6zxlak57k05g77jay6j67sva6wpc5xksqa57cz")))

(define-public crate-compose-validatr-0.1.3 (c (n "compose-validatr") (v "0.1.3") (d (list (d (n "ipnetwork") (r "^0.20.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.187") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "0gbfpkhlbpaxnsx19yccl91cvxwssc9bv5mfb6nd7aw82mqmn2l6")))

