(define-module (crates-io co mp compu-brotli-sys) #:use-module (crates-io))

(define-public crate-compu-brotli-sys-0.1.0 (c (n "compu-brotli-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.43") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "15nw2hxqiv6h96mm2fzdc7znbsddk8rc7gf3n6pc99ljffy6jz95") (f (quote (("build-bindgen" "bindgen"))))))

(define-public crate-compu-brotli-sys-0.1.1 (c (n "compu-brotli-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.43") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0fb6pywf1zw3gsh2xlv4gc29q5bqxw6zklfjky9cyb6vnnjv5ddv") (f (quote (("docs") ("build-bindgen" "bindgen"))))))

(define-public crate-compu-brotli-sys-0.1.2 (c (n "compu-brotli-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.43") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "12amgdzxkfrywcwgyfhwh00i2a8ldxw1lgf6bmb2r88p2y7sdlqs") (f (quote (("docs") ("build-bindgen" "bindgen"))))))

(define-public crate-compu-brotli-sys-0.1.3 (c (n "compu-brotli-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0chzcgy5naqgq5zvrhzcmpclwb5wyvssaknsd9k1gmyqmv87gbrg") (f (quote (("docs") ("build-bindgen" "bindgen"))))))

(define-public crate-compu-brotli-sys-1.0.9 (c (n "compu-brotli-sys") (v "1.0.9") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "12py87dmjv95hlyh7rdhys2wg4bqhllrs5c3wwfcs6m14h4jgayg") (f (quote (("download-sources") ("build-bindgen" "bindgen")))) (y #t)))

(define-public crate-compu-brotli-sys-1.0.9+1 (c (n "compu-brotli-sys") (v "1.0.9+1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0rp75szj6fv7ngscjng0sgm3yjj3ykb46mvd90334rr8ig76gip5") (f (quote (("download-sources") ("build-bindgen" "bindgen"))))))

(define-public crate-compu-brotli-sys-1.0.10 (c (n "compu-brotli-sys") (v "1.0.10") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0q3imwmf0b8r4zkj5aamvcj5dscn17fv3vr5ibnxgjf9yd1r9p7r") (f (quote (("download-sources") ("build-bindgen" "bindgen"))))))

(define-public crate-compu-brotli-sys-1.1.0 (c (n "compu-brotli-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1ffahl3r9n0s9lp1zaxg1ajwx67rxpscsh6iqaxs1wa4zjqkwjwa") (f (quote (("download-sources") ("build-bindgen" "bindgen"))))))

