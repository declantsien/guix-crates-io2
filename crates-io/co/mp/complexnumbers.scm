(define-module (crates-io co mp complexnumbers) #:use-module (crates-io))

(define-public crate-ComplexNumbers-0.1.0 (c (n "ComplexNumbers") (v "0.1.0") (h "11v3hf3l5sdg8x5lh05a72z4i1bfw5bmxbkbp4pmd127xiy9rp82")))

(define-public crate-ComplexNumbers-0.1.1 (c (n "ComplexNumbers") (v "0.1.1") (h "0dw0byf114qhj42sw37yd8z0nlzhw2ml24zqgm546b9idgwr6k9b")))

(define-public crate-ComplexNumbers-0.1.2 (c (n "ComplexNumbers") (v "0.1.2") (h "1j0yg9n81705cwmpjicbfmfm1dzg3srp3jv8lraalw7pj519b9bj")))

(define-public crate-ComplexNumbers-0.1.3 (c (n "ComplexNumbers") (v "0.1.3") (h "08wyj4934k8vkn049h7ip50yckljrgw8rbgfla3r7797wr8ffgjg")))

