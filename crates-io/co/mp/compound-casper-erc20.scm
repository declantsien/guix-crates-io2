(define-module (crates-io co mp compound-casper-erc20) #:use-module (crates-io))

(define-public crate-compound-casper-erc20-0.1.0 (c (n "compound-casper-erc20") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("alloc"))) (k 0)) (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "once_cell") (r "^1.8.0") (k 0)))) (h "1cb0w5gzd0brmp8jwj6cic7r9fpmrggj5fhj0mqrg43k5zmiksq2") (f (quote (("std" "casper-contract/std" "casper-types/std") ("default" "std"))))))

