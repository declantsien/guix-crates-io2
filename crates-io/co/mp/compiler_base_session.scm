(define-module (crates-io co mp compiler_base_session) #:use-module (crates-io))

(define-public crate-compiler_base_session-0.0.1 (c (n "compiler_base_session") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.1") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "0y90zf0s00r7pyxlyb194h4157kdvlkadkqwp2sky8fkgvbhf8v9")))

(define-public crate-compiler_base_session-0.0.2 (c (n "compiler_base_session") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.1") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "0fn759z0al8b8rqjrgr0dy91c1xs08gx76lncm0aqhm3bs815dn6")))

(define-public crate-compiler_base_session-0.0.3 (c (n "compiler_base_session") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.2") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "1cvfm6xyqm6hwci462vvrvijfi3sn61ypgimmvbgmay48vf2gdih")))

(define-public crate-compiler_base_session-0.0.4 (c (n "compiler_base_session") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.3") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "197sgj19wfij07k04slsv5vz96j1smi3iv0phhbvw1l1s4phbdq3")))

(define-public crate-compiler_base_session-0.0.5 (c (n "compiler_base_session") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.4") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "1f8l7a30iga8ycc0gf6vxj2bx9syn5yxfq3kjhkxvs59x4v77rjr")))

(define-public crate-compiler_base_session-0.0.6 (c (n "compiler_base_session") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.5") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "0w6jr5jdld99l5740vn1h7zvxmmd2g4b2f550z58cghd1b140a5h")))

(define-public crate-compiler_base_session-0.0.7 (c (n "compiler_base_session") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.5") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "17c5z162n07vg4g5l3fv8n341a1zxbw2xs9qrrdwv9nmc7r6dh0m")))

(define-public crate-compiler_base_session-0.0.8 (c (n "compiler_base_session") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.5") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "0bbjwnxbp921dc45vhixzrspkz88dpyyk9mp69hg1bkffzdv428m")))

(define-public crate-compiler_base_session-0.0.9 (c (n "compiler_base_session") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.6") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "0wg28rfxm5xg3zy37ihcxy8wdx2izw9jddfqgh42bi04cbsj818d")))

(define-public crate-compiler_base_session-0.0.10 (c (n "compiler_base_session") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.7") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "03wjlfcy13a79wsnllnbrzclq5h1sxpvjs7qbxdrf2k0a8rvsr23")))

(define-public crate-compiler_base_session-0.0.11 (c (n "compiler_base_session") (v "0.0.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.8") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "0cgpy6809v1yvphpmp2kdr3yrva8ql1jlw3hm3h5hac7k2r02i4l")))

(define-public crate-compiler_base_session-0.0.13 (c (n "compiler_base_session") (v "0.0.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.0.10") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.0.1") (d #t) (k 0)))) (h "0jfw7kzcq6kqwpl30z01x3dkkp87l2fpwz9f5gzaws0ldx6snqjc")))

(define-public crate-compiler_base_session-0.1.0 (c (n "compiler_base_session") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.1.2") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.1.1") (d #t) (k 0)))) (h "078k7p2k09ris0p9wbfyzpvgpnf9qaxyihpix4rjdyc7v8iddziq")))

(define-public crate-compiler_base_session-0.1.1 (c (n "compiler_base_session") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.1.3") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.1.1") (d #t) (k 0)))) (h "04cr04qq3wwyy58hs6r21x6vi5rzih16sv690wwzyncw5vj1ja7k")))

(define-public crate-compiler_base_session-0.1.2 (c (n "compiler_base_session") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.1.5") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.1.2") (d #t) (k 0)))) (h "1dz1kr1p9rkxcrdlj4kjw7vwhj0klh36wbxiaqcsxj0583d43cwp")))

(define-public crate-compiler_base_session-0.1.3 (c (n "compiler_base_session") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "compiler_base_error") (r "^0.1.6") (d #t) (k 0)) (d (n "compiler_base_span") (r "^0.1.2") (d #t) (k 0)))) (h "1ncszwx6dgj8wkid258js6bi31p33z8c827c8pqckn91ah5iyhb7")))

