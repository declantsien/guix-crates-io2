(define-module (crates-io co mp compilation-guard) #:use-module (crates-io))

(define-public crate-compilation-guard-0.1.0 (c (n "compilation-guard") (v "0.1.0") (h "17q5brqxh5r1gq5pj8ib3isijmqbrwi6bqvhzjwkq14qzwkbqhxj")))

(define-public crate-compilation-guard-0.1.1 (c (n "compilation-guard") (v "0.1.1") (h "043ggfp695zqx1vn41nayncmlqvbfr8z4dblpb508s94n3q476jn")))

