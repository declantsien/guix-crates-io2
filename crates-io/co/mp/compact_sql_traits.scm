(define-module (crates-io co mp compact_sql_traits) #:use-module (crates-io))

(define-public crate-compact_sql_traits-0.0.1 (c (n "compact_sql_traits") (v "0.0.1") (d (list (d (n "tokio-postgres") (r "^0.7") (d #t) (k 0)))) (h "1sj9j9982gmj69x0xqcvdwy08p6svpd9xk96nlcqw1k0n7gjs6ra") (r "1.63")))

