(define-module (crates-io co mp compiler_base_macros) #:use-module (crates-io))

(define-public crate-compiler_base_macros-0.1.0 (c (n "compiler_base_macros") (v "0.1.0") (h "04xdqlkcpf94xhcr0qga38dm9l667dl61v1xjl1qdarmn3784xdr")))

(define-public crate-compiler_base_macros-0.0.1 (c (n "compiler_base_macros") (v "0.0.1") (h "1dqvrhzylsmhr2xw3hknhqfs1dk6m2yyk04ga6p8z8ykmb1cvqf9")))

(define-public crate-compiler_base_macros-0.1.1 (c (n "compiler_base_macros") (v "0.1.1") (h "1vhc3q29msmg6h1vy10b148d6rlfgdby0rpzlmhghsabycs01411")))

