(define-module (crates-io co mp compile-symbol) #:use-module (crates-io))

(define-public crate-compile-symbol-0.1.1 (c (n "compile-symbol") (v "0.1.1") (d (list (d (n "compile-symbol-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "docify") (r "^0.1") (d #t) (k 0)))) (h "0hzq48xwzkwnifahx291qwhf3yhqq4b0s3yidn6jq3djnc4bxy17") (f (quote (("generate-readme") ("default")))) (y #t)))

(define-public crate-compile-symbol-0.1.2 (c (n "compile-symbol") (v "0.1.2") (d (list (d (n "compile-symbol-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "docify") (r "^0.1") (d #t) (k 0)))) (h "1qxv1kqihwf9c1yhch00yq4kkfj7mnxlq5dsmi97gyfsks0bdapx") (f (quote (("generate-readme") ("default")))) (y #t)))

(define-public crate-compile-symbol-0.1.3 (c (n "compile-symbol") (v "0.1.3") (d (list (d (n "compile-symbol-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "docify") (r "^0.2") (d #t) (k 0)))) (h "1h5x8338w0ipbplccnrcn98rm9qmb7y90qq242n4xd5jjdvwrcby") (f (quote (("generate-readme") ("default")))) (y #t)))

(define-public crate-compile-symbol-0.1.4 (c (n "compile-symbol") (v "0.1.4") (d (list (d (n "compile-symbol-macros") (r "^0.1.4") (d #t) (k 0)) (d (n "docify") (r "^0.2") (d #t) (k 0)))) (h "1359sbfsgymjyi0dhriq5chkj7rb77bx8wc8w14gn6ac35p6sf3a") (f (quote (("generate-readme") ("default")))) (y #t)))

