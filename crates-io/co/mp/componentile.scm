(define-module (crates-io co mp componentile) #:use-module (crates-io))

(define-public crate-componentile-0.1.0 (c (n "componentile") (v "0.1.0") (h "1zcaqhrkjddpdadh87c33z92fnx5jp9iqgw8cy62i6z1cj51fwa9")))

(define-public crate-componentile-0.2.0 (c (n "componentile") (v "0.2.0") (h "197ljzfsdnws1yj494mlg0dwkxmbgvlzdardlkwcb5isc7grf9kv")))

(define-public crate-componentile-0.3.0 (c (n "componentile") (v "0.3.0") (h "0ay6qz8yik0fwi6rykswi2z7sj0qn02zbgak7ddflha0lc1nppj7")))

(define-public crate-componentile-0.4.0 (c (n "componentile") (v "0.4.0") (h "1yw85wnp6ja2i4ijmqqbmwvy7rd5fvdv472hx0iczp2i6yasxwp6")))

(define-public crate-componentile-0.5.0 (c (n "componentile") (v "0.5.0") (h "1m9qn8d2pmp9wijq25brnhdgvfqgc1ki29z6z0mhbljvvwmjkxsc")))

