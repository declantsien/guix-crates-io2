(define-module (crates-io co mp compose_parser) #:use-module (crates-io))

(define-public crate-compose_parser-0.1.0 (c (n "compose_parser") (v "0.1.0") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "15kxz3vmra03hwz81l9avbp0iaxwjp4rsd4s1ajgr8lab1yf4jjv")))

(define-public crate-compose_parser-0.1.1 (c (n "compose_parser") (v "0.1.1") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0g7wlng19pcygqicyvc33hp6rf3gianqwb0n3y6h8v5k3hgv7z7g")))

(define-public crate-compose_parser-0.1.2 (c (n "compose_parser") (v "0.1.2") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "17k8bjm3pys6c8msdbh2fhw7c2d48fhd3cs1q6adzk758rygyjn1")))

(define-public crate-compose_parser-1.0.0 (c (n "compose_parser") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_generator") (r "^2.7.5") (d #t) (k 0)))) (h "0fak4pis2z3daf8f98ai3rvscawb588gaplxv1v6imsf9ch2haxa")))

(define-public crate-compose_parser-1.0.1 (c (n "compose_parser") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_generator") (r "^2.7.5") (d #t) (k 0)))) (h "0yisqpshjhxc5xjb9mhmfdc9vl9wx8gk8nv2jrb3shnxldh6ibvs")))

(define-public crate-compose_parser-2.0.0 (c (n "compose_parser") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_generator") (r "^2.7.5") (d #t) (k 0)))) (h "1whamqqibx0c5p7zb3b0f4373i6xhnvkxk7jbjvlwcr25z6rjv5x")))

(define-public crate-compose_parser-2.0.1 (c (n "compose_parser") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_generator") (r "^2.7.5") (d #t) (k 0)))) (h "1v8yps4zw93jh7fc0l4k8an884sjrfxsb6ahahrbrxnvfp77r89c")))

(define-public crate-compose_parser-2.1.0 (c (n "compose_parser") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_generator") (r "^2.7.5") (d #t) (k 0)))) (h "08a7gjksp0kf2z9l111gwvfk8jls63ll23p1iy5iq656d9b1pq79")))

