(define-module (crates-io co mp compacts-dict) #:use-module (crates-io))

(define-public crate-compacts-dict-0.1.0 (c (n "compacts-dict") (v "0.1.0") (d (list (d (n "compacts-bits") (r "^0.1") (d #t) (k 0)) (d (n "compacts-prim") (r "^0.1") (d #t) (k 0)))) (h "1vnkbcblxfhya1zs2ilrnkdmnsg6pf3gzyb6h1imldfi8dnl3192")))

(define-public crate-compacts-dict-0.2.0 (c (n "compacts-dict") (v "0.2.0") (d (list (d (n "compacts-bits") (r "^0.2") (d #t) (k 0)) (d (n "compacts-prim") (r "^0.2") (d #t) (k 0)))) (h "1rgddgpbpyp7gf0zafpzmdfr48kahriblcmv52dc4kdrnznafksa")))

(define-public crate-compacts-dict-0.2.1 (c (n "compacts-dict") (v "0.2.1") (d (list (d (n "compacts-bits") (r "^0.2") (d #t) (k 0)) (d (n "compacts-prim") (r "^0.2") (d #t) (k 0)))) (h "0nad9vks0a5ygfzc1raay1aqjd59ra9fkbwkm0xxkq9sa0nkdf9n")))

