(define-module (crates-io co mp compact_bitset) #:use-module (crates-io))

(define-public crate-compact_bitset-0.1.0 (c (n "compact_bitset") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0mvyxaaqny92pnwrrwhrv46f7dra8v6yvxzgzx78kvqkqnvb04cd")))

(define-public crate-compact_bitset-0.1.1 (c (n "compact_bitset") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1n1k8nc0247shshrzj5pw382fswkjcbyjyk26chw73fs19r33fxd")))

(define-public crate-compact_bitset-0.1.2 (c (n "compact_bitset") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "01bs43qyva7m7r4msrq9331a9mjs7dmwkpgnsc1himzdvs4w1rql")))

(define-public crate-compact_bitset-0.1.3 (c (n "compact_bitset") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12f3hz9ck9vycdwqxxy0id9dqxivn23qihh0s3m6vwc3hysq54bv")))

