(define-module (crates-io co mp comparable_test) #:use-module (crates-io))

(define-public crate-comparable_test-0.1.0 (c (n "comparable_test") (v "0.1.0") (d (list (d (n "comparable") (r "^0.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1anb9vhjhpbqzjhf4njcz8m54yymgfii5lday83wgknwyb0jnccy")))

(define-public crate-comparable_test-0.2.0 (c (n "comparable_test") (v "0.2.0") (d (list (d (n "comparable") (r "^0.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1bgqhz45pl49alhqq6nqmrsrfgks15xc3ad30xzbgdwqkn24n1kd")))

(define-public crate-comparable_test-0.3.0 (c (n "comparable_test") (v "0.3.0") (d (list (d (n "comparable") (r "^0.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1vhmypdkz6ah5hzqd6rwiqcwdi74hrg718zr9birnq14hswshgqk")))

(define-public crate-comparable_test-0.3.1 (c (n "comparable_test") (v "0.3.1") (d (list (d (n "comparable") (r "^0.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "187rm7lx4hlz6d5xaxd7ay4sip1r80wmcn2nw3ad8pc4vjdzpj6d")))

(define-public crate-comparable_test-0.3.2 (c (n "comparable_test") (v "0.3.2") (d (list (d (n "comparable") (r "^0.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "116n3z3b49q540hssh5kr0w4nzlwx1057abdqvgz03g9cwibbary")))

(define-public crate-comparable_test-0.4.0 (c (n "comparable_test") (v "0.4.0") (d (list (d (n "comparable") (r "^0.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1b383md247bam4kyssmr4dl4rysyq5g649ca1l87gdp6aspkwsfp")))

(define-public crate-comparable_test-0.5.0 (c (n "comparable_test") (v "0.5.0") (d (list (d (n "comparable") (r "^0.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "10alwjgzi5c86k7iiic1mv1q6cnm2v8zx32fskvhs3miydlsz4y2")))

(define-public crate-comparable_test-0.5.1 (c (n "comparable_test") (v "0.5.1") (d (list (d (n "comparable") (r "^0.5") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1gxqqnn385q1qy9k4cmz1w85w1vbqdiwzk1qibij4g259nr54gz7")))

(define-public crate-comparable_test-0.5.2 (c (n "comparable_test") (v "0.5.2") (d (list (d (n "comparable") (r "^0.5.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0njrpd6ryndpc3x29i1cnfv4j85rvadp2w0kfdd1fxzf5373sl88")))

(define-public crate-comparable_test-0.5.3 (c (n "comparable_test") (v "0.5.3") (d (list (d (n "comparable") (r "^0.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0rg2qbmhr1p80vmf1gjxdaifhan8nj6rq1r0vimdys9j3dzwkq6n")))

(define-public crate-comparable_test-0.5.4 (c (n "comparable_test") (v "0.5.4") (d (list (d (n "comparable") (r "^0.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "049anil3qfgnaizilqd2kv6jpr25c0x384avdnxby8m4ay72z7c0")))

