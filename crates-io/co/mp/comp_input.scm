(define-module (crates-io co mp comp_input) #:use-module (crates-io))

(define-public crate-comp_input-0.1.0 (c (n "comp_input") (v "0.1.0") (h "1jr7c9zxyz8g3365fdfk5yzrqf2h6i75f6l6yqikn2kr3nqvr4dw")))

(define-public crate-comp_input-0.1.1 (c (n "comp_input") (v "0.1.1") (h "05lbwamh3vxqb3869lchbswcq4bs9p7p345fdaj5i6nxhkk88yks")))

(define-public crate-comp_input-0.2.0 (c (n "comp_input") (v "0.2.0") (d (list (d (n "memchr") (r "^2.2.1") (d #t) (k 0)))) (h "1gqkqklqzf84wcy5rv83rhibygx1yb4npx4bmi94c4w4a0f8spxq")))

(define-public crate-comp_input-0.2.1 (c (n "comp_input") (v "0.2.1") (d (list (d (n "memchr") (r "^2.2.1") (d #t) (k 0)))) (h "11z1aqivfh0jiksmslnn050md1h0hj0vxxb7rgli8p6bvpqd5dmj")))

(define-public crate-comp_input-0.2.2 (c (n "comp_input") (v "0.2.2") (d (list (d (n "memchr") (r "^2.2.1") (d #t) (k 0)))) (h "0lri6np4xn9qn9c706g9lr3j2dhqx03ilxk2qvvakc1am18nah48")))

