(define-module (crates-io co mp compio-macros) #:use-module (crates-io))

(define-public crate-compio-macros-0.1.0 (c (n "compio-macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0f7nvkbdplijivv60d757v7abchgbzrjz0jis96bhzndvjqfcl45")))

(define-public crate-compio-macros-0.1.1-beta.1 (c (n "compio-macros") (v "0.1.1-beta.1") (d (list (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "04a1gvbnx80gjpgc05z8sslvjs8qd20hhaby0wn5afvyiyjndciq")))

(define-public crate-compio-macros-0.1.1 (c (n "compio-macros") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1w7qxqrilnr1jixaid5kh55n2rn173qmiiqkjj4jr2zbjxphgpqd")))

