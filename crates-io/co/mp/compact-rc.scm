(define-module (crates-io co mp compact-rc) #:use-module (crates-io))

(define-public crate-compact-rc-0.3.0 (c (n "compact-rc") (v "0.3.0") (h "0wcpgagqkpf0c95g4z9w2ky4ywgspcrh1lbn20qvjzbijnkhz5gd")))

(define-public crate-compact-rc-0.3.1 (c (n "compact-rc") (v "0.3.1") (h "00wiv25ckyh2gd27apgiz30p112c7yjjl9p2r66dnawlg7nk5wc7")))

(define-public crate-compact-rc-0.4.0 (c (n "compact-rc") (v "0.4.0") (h "0lmf2f58j7gkbgvxn6svf9gyiq0b4f021zrqrgnrm7mqafgm6idn")))

(define-public crate-compact-rc-0.4.1 (c (n "compact-rc") (v "0.4.1") (h "0h5f9yvgi2bj7vpa47lz666ir8fhmx6cxa105chm347fzm6cilw3") (y #t)))

(define-public crate-compact-rc-0.4.2 (c (n "compact-rc") (v "0.4.2") (h "1s98jvfnjlycv7ypwyyhsdszlvgm2xqp3bvdv9ihf61f4az3zq3c")))

(define-public crate-compact-rc-0.5.0 (c (n "compact-rc") (v "0.5.0") (h "0smlssd5srsnwhk0dc13bwh3nsswn1d0cikns08jzrjazd47m4mf")))

(define-public crate-compact-rc-0.5.1 (c (n "compact-rc") (v "0.5.1") (h "1am48napl7jpxn7732z1pmjic1zs8r5mmwc8bpcxc5rpf8jfn4f8") (y #t)))

(define-public crate-compact-rc-0.5.2 (c (n "compact-rc") (v "0.5.2") (h "1h3mj9sx79m38plyixqb89ycvakyxqm6ippyzpww9axmak7xbab8")))

(define-public crate-compact-rc-0.5.4 (c (n "compact-rc") (v "0.5.4") (d (list (d (n "dropcount") (r "^0.1") (d #t) (k 2)))) (h "08sapb18mpyg4dpqhkw38rd17dlkf97xrlahyhf27v8mr6bxqaxz")))

(define-public crate-compact-rc-0.5.5 (c (n "compact-rc") (v "0.5.5") (d (list (d (n "dropcount") (r "^0.1") (d #t) (k 2)))) (h "1x4kr7yr66g6ih9vhlk1m7dqz769bk8nssk1ckb84z020rh0xwis")))

