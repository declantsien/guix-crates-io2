(define-module (crates-io co mp compile-time) #:use-module (crates-io))

(define-public crate-compile-time-0.1.0 (c (n "compile-time") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "1jg5isykn9k3hlv5gr703jw7z1jbhsph9zh7m23nwc40lfqj7qbs")))

(define-public crate-compile-time-0.2.0 (c (n "compile-time") (v "0.2.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("macros" "formatting"))) (d #t) (k 0)))) (h "00yr5ln6qc8qdp6hyi3c3sp56qxjpqx78lv8j0lcbmylg59dwpp5")))

