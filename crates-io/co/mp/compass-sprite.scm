(define-module (crates-io co mp compass-sprite) #:use-module (crates-io))

(define-public crate-compass-sprite-0.0.1 (c (n "compass-sprite") (v "0.0.1") (d (list (d (n "image") (r "^0.3.7") (d #t) (k 0)))) (h "0isw8fq4lxfq4s0f3ms8pdwhgn2vby1fz2943mk1fzbkz563pmd1")))

(define-public crate-compass-sprite-0.0.2 (c (n "compass-sprite") (v "0.0.2") (d (list (d (n "image") (r "^0.3.7") (d #t) (k 0)))) (h "05prvfgj9nz03swn7nxjigx2zfdppdhx831id32n84fy332idciy")))

(define-public crate-compass-sprite-0.0.3 (c (n "compass-sprite") (v "0.0.3") (d (list (d (n "image") (r "^0.3.7") (d #t) (k 0)))) (h "1a3dsy38xfrpjdfka3risw6l6hs6yajcfagnkprjhklsmfibiwpn")))

