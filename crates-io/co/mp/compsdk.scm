(define-module (crates-io co mp compsdk) #:use-module (crates-io))

(define-public crate-compsdk-1.0.0 (c (n "compsdk") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.5") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "14wx9hphyi58j940nb3ar99n70jihrad52k0xmy4bj9bi4p0n68d")))

