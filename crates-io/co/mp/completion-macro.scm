(define-module (crates-io co mp completion-macro) #:use-module (crates-io))

(define-public crate-completion-macro-0.1.0 (c (n "completion-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "proc-macro" "parsing" "printing" "visit-mut"))) (k 0)))) (h "100dyv01fxw4d0l2jk16fhkf4nc5kzick3q99xmr77n4d7l2jiwb")))

(define-public crate-completion-macro-0.2.0 (c (n "completion-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full" "parsing" "printing" "proc-macro" "visit-mut"))) (k 0)))) (h "1rp4vr5jy64dl476clpf1q4gslqfk0g7cmkklw13mqya03p8q4hv")))

