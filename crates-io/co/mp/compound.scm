(define-module (crates-io co mp compound) #:use-module (crates-io))

(define-public crate-compound-0.1.0 (c (n "compound") (v "0.1.0") (h "12ywy7bgh7w3ygcsnia3gqynf258c39qs6jfysw2243h32rz3z90")))

(define-public crate-compound-0.1.1 (c (n "compound") (v "0.1.1") (h "04vfp540kj05n279d0l4mz51m1cg9jk0hs1hj7isid33q8hvi5dm")))

