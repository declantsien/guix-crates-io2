(define-module (crates-io co mp compressor) #:use-module (crates-io))

(define-public crate-compressor-0.1.0 (c (n "compressor") (v "0.1.0") (d (list (d (n "dsp-chain") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "envelope_detector") (r "^0.1.0") (d #t) (k 0)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 0)))) (h "1ikndn14xv5v33cf1h78wdckyj7vnhyf0n94zj3csq2k1rkda741") (f (quote (("default" "dsp-chain"))))))

(define-public crate-compressor-0.1.1 (c (n "compressor") (v "0.1.1") (d (list (d (n "dsp-chain") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "envelope_detector") (r "^0.1.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.1") (d #t) (k 2)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 0)))) (h "0q14ax0zndaiqyk1r66ppi24s1y9a36mjqvy3315gv3i98a22xdq") (f (quote (("default" "dsp-chain"))))))

(define-public crate-compressor-0.1.2 (c (n "compressor") (v "0.1.2") (d (list (d (n "dsp-chain") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "envelope_detector") (r "^0.1.2") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.1") (d #t) (k 2)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 0)))) (h "0ai5dz8fbc8byy0sqn45vh6xry13ysajd2z0sjrg69dwlnnv35lb") (f (quote (("default" "dsp-chain"))))))

(define-public crate-compressor-0.2.0 (c (n "compressor") (v "0.2.0") (d (list (d (n "dsp-chain") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "envelope_detector") (r "^0.1.2") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.3") (d #t) (k 2)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 0)))) (h "1ldbisscv28bw7gl33yckv5kpdhzkhx8zrqp85r37ngpn48y5045") (f (quote (("default" "dsp-chain"))))))

(define-public crate-compressor-0.3.0 (c (n "compressor") (v "0.3.0") (d (list (d (n "dsp-chain") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "envelope_detector") (r "^0.2.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.6.4") (d #t) (k 2)) (d (n "time_calc") (r "^0.11.0") (d #t) (k 0)))) (h "1m7yllzav7b00pifpv526c477963lh4gyxl3xyhcnsq5zysnfpq3") (f (quote (("default" "dsp-chain"))))))

