(define-module (crates-io co mp componentize-mbt) #:use-module (crates-io))

(define-public crate-componentize-mbt-0.1.0 (c (n "componentize-mbt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wast") (r "^70.0.2") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.16.0") (d #t) (k 0)) (d (n "wit-bindgen-mbt") (r "^0.1.0") (f (quote ("clap"))) (d #t) (k 0)) (d (n "wit-component") (r "^0.20.0") (d #t) (k 0)))) (h "056ivmr1qclz9w59xq8z4ga3wrb89f10kmpkbahrvhzc9jl14hqq")))

