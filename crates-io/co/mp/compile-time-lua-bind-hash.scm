(define-module (crates-io co mp compile-time-lua-bind-hash) #:use-module (crates-io))

(define-public crate-compile-time-lua-bind-hash-1.0.0 (c (n "compile-time-lua-bind-hash") (v "1.0.0") (d (list (d (n "lua_bind_hash") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nnapgqxy3bky6rj7spczjkfpp3f01h79qfspikyh51b9wy063cx")))

(define-public crate-compile-time-lua-bind-hash-1.1.0 (c (n "compile-time-lua-bind-hash") (v "1.1.0") (d (list (d (n "lua_bind_hash") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1064q5v2v7v528cbkxwb3vzk9ywzy743b016y89kxhbwayh546zs")))

(define-public crate-compile-time-lua-bind-hash-1.1.1 (c (n "compile-time-lua-bind-hash") (v "1.1.1") (d (list (d (n "lua_bind_hash") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1h703c52qkff5x4hrzglcljpvddj9zmdjljrd35kcj5a9b6wq2v6")))

