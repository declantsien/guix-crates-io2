(define-module (crates-io co mp compacto) #:use-module (crates-io))

(define-public crate-compacto-1.0.0 (c (n "compacto") (v "1.0.0") (d (list (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (f (quote ("alloc"))) (k 0)))) (h "050lrvv9c6rydma03mv25aa7bdhc7dmignj921pnvapwmpjg2ppr")))

(define-public crate-compacto-1.0.1 (c (n "compacto") (v "1.0.1") (d (list (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (f (quote ("alloc"))) (k 0)))) (h "1flx0g7i86l7wkmai0mrqiibb55k3wpndswriy35jknk6vddsxjl")))

(define-public crate-compacto-1.0.2 (c (n "compacto") (v "1.0.2") (d (list (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (f (quote ("alloc"))) (k 0)))) (h "1ak3z0aabqdlx6crij7rh6fglxzzaml2cakadqajckzzdbj0rz9q")))

