(define-module (crates-io co mp compact) #:use-module (crates-io))

(define-public crate-compact-0.1.0 (c (n "compact") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "0gl3dpijg48pa6p4wsb83dq9az01z68xxxw55a7gmhsyh25svl6k")))

(define-public crate-compact-0.2.0 (c (n "compact") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1n4i6g30r5rm9rclicl62bgiim9srldxrmw744jkhfmsq36z40r0")))

(define-public crate-compact-0.2.1 (c (n "compact") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1gqd76nvwsnnym6dyblh08awvvckdq3rch1z54aylf7l5plpis8f") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.2 (c (n "compact") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1jzamxhkyy9rcnp1svd9gv88m6v35gwcgqv1ymmvbmvv4q200qcn") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.3 (c (n "compact") (v "0.2.3") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "0srak5sdvjlmdcxcgfz1zhk4k6w92bkiag3rgdgfxs1fxvarr76z") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.4 (c (n "compact") (v "0.2.4") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "0zzbh6vc78y43zrf6a1h7v66wdzzvf62abfpix3pd02nr9695fi6") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.5 (c (n "compact") (v "0.2.5") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "022cklnx456325p8l17vjc5yvygafh7z1vd96hx3lc87lkr58c7h") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.6 (c (n "compact") (v "0.2.6") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "199nfr7i2iyynl90ph5r89gdd4nchdzr39k4ad1gmwjwjvjnmjdh") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.7 (c (n "compact") (v "0.2.7") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1lj5mjq17r7lm4z7wj4h1lb1dhi2cxjjjnrmlsm3kajp69402x6j") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.8 (c (n "compact") (v "0.2.8") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1lhhyd1001dy29p9m32cm8xxa0c405r6fl96s1fcycql1s37lkn5") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.9 (c (n "compact") (v "0.2.9") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "00zpki2p33pz2083x8dwyqzg4cmzcr8828a7ch65254sgyv0b2y0") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.10 (c (n "compact") (v "0.2.10") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "0i1i37590bayjg3ynsbg2k3yg93xpnxxy0l6z2h3vg04608fr2ks") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.11 (c (n "compact") (v "0.2.11") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "0mlfn8sjix2p4g1m3in9nr7kw4fyxryrcmwa8a45bcgs9dszxpkb") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.12 (c (n "compact") (v "0.2.12") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "00ag82pclsw40y1x0lk8p52vlji1da6jdycqgc5mj5gx77p6ssmz") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.13 (c (n "compact") (v "0.2.13") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "08sdjnxy575nb5xfwbi90pk7i05hpzpg5ngq24gl9fpzirmdgfki") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.14 (c (n "compact") (v "0.2.14") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "0wir6ap62gggdrq68rnzvlf6fdpdn9mqg7kv8bpfsj3r29i1mgmf") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.15 (c (n "compact") (v "0.2.15") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1mrgg4mr4yxsc8762bmga71r2wg98vr4hlm8n9ifqjhbr6nvar2h") (f (quote (("serde-serialization" "serde"))))))

(define-public crate-compact-0.2.16 (c (n "compact") (v "0.2.16") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "simple_allocator_trait") (r "^0.1.0") (d #t) (k 0)))) (h "1xgfks00igzn8wg956vq821rg24g9mzffbpafmifndady8m299bv") (f (quote (("serde-serialization" "serde"))))))

