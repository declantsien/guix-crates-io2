(define-module (crates-io co mp compile-ints) #:use-module (crates-io))

(define-public crate-compile-ints-0.1.0 (c (n "compile-ints") (v "0.1.0") (d (list (d (n "proc-macro2") (r "~0.4") (d #t) (k 0)) (d (n "quote") (r "~0.6") (d #t) (k 0)) (d (n "rand") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "syn") (r "~0.15") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1lj5h6998a6hmvgxjr1xvvwz4w587w36fb8df0357sbhnix6f72w")))

