(define-module (crates-io co mp compiletime) #:use-module (crates-io))

(define-public crate-compiletime-1.0.0 (c (n "compiletime") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "191dxarcifvs43i3yqn261v16ang3ag3jz9qkq9c8kfbklisl3zb")))

(define-public crate-compiletime-1.1.0 (c (n "compiletime") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yqfnj66fwvp44r5fc3hqzanfwwd31dnd6l8i83d7c0kw9wlx88h") (y #t)))

