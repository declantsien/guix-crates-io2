(define-module (crates-io co mp compiler) #:use-module (crates-io))

(define-public crate-compiler-0.0.0 (c (n "compiler") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "boilerplate") (r "^0.2.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "html-escaper") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1k5c98jzpkhadvb9q2xsd3qa1wcwx50rjzsd32qya81nb3szi2jq")))

