(define-module (crates-io co mp compt) #:use-module (crates-io))

(define-public crate-compt-0.1.0 (c (n "compt") (v "0.1.0") (h "0gb86iikbkzpds7hwr0lv8rj2xlma35l9ilhk3lkxrbzks7jvf1s")))

(define-public crate-compt-0.1.1 (c (n "compt") (v "0.1.1") (h "0qdkaifw5imvkck3gw8cpphjh1r6c5kgars0jcw5ngkf7msxfs87")))

(define-public crate-compt-0.2.0 (c (n "compt") (v "0.2.0") (h "088sixhhqh5hn3i1gir2p4y94davvwfyjyqvfpv7v1klhpw962wg")))

(define-public crate-compt-1.0.0 (c (n "compt") (v "1.0.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "10jd2wh380ci8m5h4dryr962jbrvj7c11z3f1riw687lpzqim1qd")))

(define-public crate-compt-1.1.0 (c (n "compt") (v "1.1.0") (d (list (d (n "is_sorted") (r "^0.1.0") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "14b55a96v8d4imylyz18ncq841y5d8pq9nygzw1q8846qdwd64am")))

(define-public crate-compt-1.2.0 (c (n "compt") (v "1.2.0") (d (list (d (n "is_sorted") (r "^0.1.0") (d #t) (k 2)) (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1cg0llk9g315sw0v7079yhifg8mlls7931dq0ln63fv6ng3ynjdr")))

(define-public crate-compt-1.2.1 (c (n "compt") (v "1.2.1") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1vqc1vjbpww5gs11x8cqgsajsjqpmc2m579psbych6a3b4yhq72y")))

(define-public crate-compt-1.2.2 (c (n "compt") (v "1.2.2") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0vw7w9ka2m7j0jrmxygb2s4isgzvjzkl8q0zfzgffx3zxhzj84p7")))

(define-public crate-compt-1.2.3 (c (n "compt") (v "1.2.3") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1y3v2bpbppsw6r1230ywpp407ia3gwdsdyn1mvvw5rxg89hg71cx")))

(define-public crate-compt-1.2.4 (c (n "compt") (v "1.2.4") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1l465l3yhapq1ckgzrpl0w7f6n4q00dwic6fw9n1296c2cr0qv7z")))

(define-public crate-compt-1.2.5 (c (n "compt") (v "1.2.5") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1f1gg03ywkv6rlc5c000n4cg04xjrvgs0rhky8idz0nc35xb1gbi")))

(define-public crate-compt-1.3.0 (c (n "compt") (v "1.3.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "128qnsn0krx0753kz084dlsvpqjjwkb2iswnjwss2r0m5rzawf94")))

(define-public crate-compt-1.3.1 (c (n "compt") (v "1.3.1") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1cq07lvyzwi0jxqz43cyl4fai9yq47466k9a5aig2m6wzkffh57i")))

(define-public crate-compt-1.3.2 (c (n "compt") (v "1.3.2") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1lcf0pdgc76lzbyz4g1wdakwvb1z5519wajd91alvg60rhnrgbig")))

(define-public crate-compt-1.4.0 (c (n "compt") (v "1.4.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0y9m76nywliz4qwd4xq7f6j2cp5l4mws1plpzl6qwxcw4zq7ac8d")))

(define-public crate-compt-1.5.0 (c (n "compt") (v "1.5.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1zx2jjs84jnj65l0dpl3jnd1hvd3mn0sp86vp22c7lpvypmlvbfz")))

(define-public crate-compt-1.5.1 (c (n "compt") (v "1.5.1") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1x8g001r2ssyhsdq3p5zi29izmh3zn3wq58kv1702ad9asvf9864")))

(define-public crate-compt-1.5.2 (c (n "compt") (v "1.5.2") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1cbn8nzmyx03bnhgsiv1b5ca51ylh6ffkd98sgk50cwbzxjapzf0")))

(define-public crate-compt-1.5.3 (c (n "compt") (v "1.5.3") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0m0raaybwrd41v9zs34x7yicms32hyrfdn04z0abjvr78payd8sh")))

(define-public crate-compt-1.5.4 (c (n "compt") (v "1.5.4") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "11zsb4x405bw2p1j63x52jxa4jbfvcx94yhkfd76jj18sxiqrwls")))

(define-public crate-compt-1.5.5 (c (n "compt") (v "1.5.5") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0qwl93kw7pa0vzyyr9lnd57blwspbvjg56yp1v4xzgzzq11cn7gr")))

(define-public crate-compt-1.5.6 (c (n "compt") (v "1.5.6") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0kbmb22q0gn3byw68nmk33mlkqn5jf168knvx3szr6q88y0zf3j7")))

(define-public crate-compt-1.6.0 (c (n "compt") (v "1.6.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "08zry846wpbk4ksl1dvxsvjsl9n3zrq87lkrhm01pdsbgblg3bdg")))

(define-public crate-compt-1.6.1 (c (n "compt") (v "1.6.1") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0qg2zjsgw7r7zmgcgrqjra88k0p3s4v5nxf20fdkz62imik2xzhf")))

(define-public crate-compt-1.6.3 (c (n "compt") (v "1.6.3") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0qk5cisdwz63wdv5kq6g0c1g4qfbvbz5c1akvsmpkxdnjhiafn0k")))

(define-public crate-compt-1.7.0 (c (n "compt") (v "1.7.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0155bw3y2xvzyyza3h2kyd40rp2hxafk6m7a4708r0z0wazp4ia8")))

(define-public crate-compt-1.7.1 (c (n "compt") (v "1.7.1") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0rfxh4c47mklrr3qxvng5lm9i2ksqv1rhk2l4wanv9rcn0h93qbh")))

(define-public crate-compt-1.8.0 (c (n "compt") (v "1.8.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "02xs2vnpfdgmr7hgw141vccwnrq66pvm4c23zrw4npll4abrywvg")))

(define-public crate-compt-1.8.1 (c (n "compt") (v "1.8.1") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0hp1aqic721kz1m3rq2afqx4mrh0nr6h69njnmj6sd5wk6f82qkc")))

(define-public crate-compt-1.9.0 (c (n "compt") (v "1.9.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1n074mlilsn3phz9xiasvqc024rckjf6mi1b7rlql8xk8g7rd56a")))

(define-public crate-compt-1.9.1 (c (n "compt") (v "1.9.1") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "13rr4an9z5ij64y4r8nrbjb3qgc8k1qyag9h1gq63pk73frkn0wl")))

(define-public crate-compt-1.10.0 (c (n "compt") (v "1.10.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1rikwjws6fkgvvzgw4vcw2hbiqls543fc7cpgrdyp5qx2dm0fc0v")))

(define-public crate-compt-1.10.1 (c (n "compt") (v "1.10.1") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "05x63s53ln13813qnqy5b48d6a45vimjkfcn570zhib7q6m78zf3")))

(define-public crate-compt-1.10.2 (c (n "compt") (v "1.10.2") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "1dms39ks9cl4laddmgnx6l408k6kfwq0z1859b70wf5b835yzrvs")))

(define-public crate-compt-2.0.0 (c (n "compt") (v "2.0.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0b9p16nm8dw6gqs4i0b9arw9936rcx85rk1lpqszvi4dziys8ab5")))

(define-public crate-compt-2.0.1 (c (n "compt") (v "2.0.1") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0sr708fnj0rx1kiwhnbfnmvj02dpihvdrln9k1w2xq7z2l8jxq1f")))

(define-public crate-compt-2.1.0 (c (n "compt") (v "2.1.0") (d (list (d (n "rustc-test") (r "^0.3.0") (d #t) (k 2)))) (h "0giy0msrp381b81lnazwfc26k6b4h218cjfzq0bsmaxrdpjh6sz7")))

