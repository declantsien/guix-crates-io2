(define-module (crates-io co mp compressed_collections) #:use-module (crates-io))

(define-public crate-compressed_collections-0.4.0 (c (n "compressed_collections") (v "0.4.0") (d (list (d (n "brotli") (r "^3.3.4") (d #t) (k 0)) (d (n "postcard") (r "^1.0.0") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yp3z5r2f1dp8bpsjjmhanppazl5rpjdj8l2irsyg88qn714scdv")))

