(define-module (crates-io co mp compio-log) #:use-module (crates-io))

(define-public crate-compio-log-0.1.0-beta.1 (c (n "compio-log") (v "0.1.0-beta.1") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0jaj60xakx3idf3iddbcjihfi5j0r63svb6nkfljvbyv04hf113z") (f (quote (("enable_log"))))))

(define-public crate-compio-log-0.1.0 (c (n "compio-log") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0bqk9vpc8ra4afp04f03z4db8r4m234vfsd3imhnp6f12c15ckpw") (f (quote (("enable_log"))))))

