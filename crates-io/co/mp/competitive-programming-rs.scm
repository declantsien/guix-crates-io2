(define-module (crates-io co mp competitive-programming-rs) #:use-module (crates-io))

(define-public crate-competitive-programming-rs-0.1.0 (c (n "competitive-programming-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "0izxhll0z9riizb8fhvnvzwjfvmddlifh7ql22fx8axqi9ihfq96")))

(define-public crate-competitive-programming-rs-0.1.1 (c (n "competitive-programming-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "157ny2q288gjp1b4p83rrcgg1pk7hhj7qgsnk8p1c0c6y21lq5y3")))

(define-public crate-competitive-programming-rs-0.1.2 (c (n "competitive-programming-rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "1jgxmwa466z0ax1iajhzm8ymybxd479ys79fvisa3lihybr5b821")))

(define-public crate-competitive-programming-rs-0.1.3 (c (n "competitive-programming-rs") (v "0.1.3") (d (list (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "18gd6zwhgz40111vx9smc7s7zbm1v2myi2hi8h1ql2vzdma83m1i")))

(define-public crate-competitive-programming-rs-0.1.4 (c (n "competitive-programming-rs") (v "0.1.4") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "00w56b9qfmi8x932sirc7g2v7hqmkv7180kga10zj49xnmpi03d4")))

(define-public crate-competitive-programming-rs-0.1.5 (c (n "competitive-programming-rs") (v "0.1.5") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0b3jv64r02sffnvff6qb6475w08wx8p0l0zjv1wgdcxpzdfs1vhw")))

(define-public crate-competitive-programming-rs-0.1.6 (c (n "competitive-programming-rs") (v "0.1.6") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1wbpnk1ks9scxx46flm4kxbrv99zdypgxdrqn1awd2bp6yab7b52")))

(define-public crate-competitive-programming-rs-0.1.7 (c (n "competitive-programming-rs") (v "0.1.7") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1v0mf8q3f3kp0ypdh73qcyynhhkgygbfiwj09klw6dwmh5cl2p2x")))

(define-public crate-competitive-programming-rs-0.1.8 (c (n "competitive-programming-rs") (v "0.1.8") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0scla0qh6s7fardjvw3xd3cd67cck4wr2x8cn80rn2wn8ka3b20n")))

(define-public crate-competitive-programming-rs-0.1.9 (c (n "competitive-programming-rs") (v "0.1.9") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0zkrcs420y23ig2y5043jikqxsivjc0knz2mmk0gdsziz046nw8w")))

(define-public crate-competitive-programming-rs-0.1.10 (c (n "competitive-programming-rs") (v "0.1.10") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1rp2d5da03dz83a2wr3h09450zglypqaib1pxs4ql0iv5hbz60nh")))

(define-public crate-competitive-programming-rs-0.1.11 (c (n "competitive-programming-rs") (v "0.1.11") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0mngwvg2ivj5kyvillf5ysjhb227555jpi3kz2psx1sypla290pz")))

(define-public crate-competitive-programming-rs-0.1.12 (c (n "competitive-programming-rs") (v "0.1.12") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0x6ins1ghkqls4khk6ll3aw6y2565ybw7v9pjf17py03szhxprvd")))

(define-public crate-competitive-programming-rs-0.1.13 (c (n "competitive-programming-rs") (v "0.1.13") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "045bzi3ccxf2gjmm4l89s14wrmhhahmjahs1kfln9qr9mkcxnawd")))

(define-public crate-competitive-programming-rs-0.1.14 (c (n "competitive-programming-rs") (v "0.1.14") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "11b4hazz675g4crmxbg05c437bmirihx9pr3qcw4p3hnwli2nr5j")))

(define-public crate-competitive-programming-rs-0.1.15 (c (n "competitive-programming-rs") (v "0.1.15") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "16j9pgw89dgbfi8z3525g43w9ww5ca0ma6czrhjnmgdvbjpqpbwy")))

(define-public crate-competitive-programming-rs-0.1.16 (c (n "competitive-programming-rs") (v "0.1.16") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0zqzr03pc5j2xr0aibczh528y3ndbnv6hln13dfdidh17xazpy25")))

(define-public crate-competitive-programming-rs-0.1.17 (c (n "competitive-programming-rs") (v "0.1.17") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1y1clzdmnx036cygwg1cxlj3pl1ak6lbqahayvf7f82lar98z0dd")))

(define-public crate-competitive-programming-rs-0.3.0 (c (n "competitive-programming-rs") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "03vxl33cl2ylsz400r0hg81cdrrknp3y5c3ybmi3lifxzn45pkk2")))

(define-public crate-competitive-programming-rs-0.4.0 (c (n "competitive-programming-rs") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1jx49az2azyh9z8hcy6hkw3f2ypznz5azgdcmk7zkdnwsmvpcncp")))

(define-public crate-competitive-programming-rs-0.5.0 (c (n "competitive-programming-rs") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "19sghrrfjfchcqycw88afva6b6v9xzv8hpxgvly855g1wfjsplmk")))

(define-public crate-competitive-programming-rs-0.7.0 (c (n "competitive-programming-rs") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "10nkrkrxlhimgz3mz025vbgdqdikhv7bqm4f61q89lnqagcv8cpj")))

(define-public crate-competitive-programming-rs-0.9.0 (c (n "competitive-programming-rs") (v "0.9.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "080llxspykgi36a57skfyad7mjhzc4yvj80f24gdz1mha8sby898")))

(define-public crate-competitive-programming-rs-0.9.1 (c (n "competitive-programming-rs") (v "0.9.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "04g6b4g17zxdkmkr54vyc3gzwpmb1ia3wm3zp0glikz23hhz32l0")))

(define-public crate-competitive-programming-rs-0.9.2 (c (n "competitive-programming-rs") (v "0.9.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0cmzvanpk26d1z4q64202dhvjhkww5039hzyq9l1lyldfxl2848p")))

(define-public crate-competitive-programming-rs-0.10.0 (c (n "competitive-programming-rs") (v "0.10.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "18kgbk16zfzfv0x75l1w4dc55ncxfy9iv192hgy854v3jpis0frm")))

(define-public crate-competitive-programming-rs-0.10.1 (c (n "competitive-programming-rs") (v "0.10.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1nbwa27a29iv5233cvzvg4qsv31hawlv96lqdawb39rnmkf6qm3l")))

(define-public crate-competitive-programming-rs-0.10.2 (c (n "competitive-programming-rs") (v "0.10.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0h1i5qcm5dwz85ykcva0ipmmsd3fvxhnwkvpdm716qx53i08p0ic")))

(define-public crate-competitive-programming-rs-11.0.0 (c (n "competitive-programming-rs") (v "11.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0ibm9w6dggzbm9h0f38qmxksjdvr4mbxcnczg1zvz1dqrc3ynqwk")))

(define-public crate-competitive-programming-rs-12.0.0 (c (n "competitive-programming-rs") (v "12.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0bfq6ag1hgz01zc0717v3qbcqqmk6b7kg6f5h39rafqn8pszb2bk")))

(define-public crate-competitive-programming-rs-13.0.0 (c (n "competitive-programming-rs") (v "13.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "02fd233163wxif5zvh74amfcsflbj92a8ak7wsjwjh96bi1zpixn")))

(define-public crate-competitive-programming-rs-14.0.0 (c (n "competitive-programming-rs") (v "14.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1npapshaf5nwzmldsz500712v1iw6b7ipdxvr51qbk21rcpfa4ki")))

(define-public crate-competitive-programming-rs-15.0.0 (c (n "competitive-programming-rs") (v "15.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "08akcixfb4civgk3d6hinfyhczn8csqgiv1p9jljwm89y9cffs8r")))

(define-public crate-competitive-programming-rs-16.0.0 (c (n "competitive-programming-rs") (v "16.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0l31ra94kyl5hhxjg6ddfqmankdakhxm00n4cragqzzkb8k4bb9a")))

(define-public crate-competitive-programming-rs-17.0.0 (c (n "competitive-programming-rs") (v "17.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "19251cvfxjybbwm0k44r0al8k4l2z4wkvp7nanvyxs7w6w1qb84g")))

(define-public crate-competitive-programming-rs-18.0.0 (c (n "competitive-programming-rs") (v "18.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "04mbghddlasvh1nki3sys5v9mdjxqgn7ycvw52y7smkq8h86s1r3")))

(define-public crate-competitive-programming-rs-19.0.0 (c (n "competitive-programming-rs") (v "19.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0nzk9zbfydya8zi4zghdylzhk0glrbbyd51qgmd9pxs9s5m71vxj")))

(define-public crate-competitive-programming-rs-20.0.0 (c (n "competitive-programming-rs") (v "20.0.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0xx1zqkhmaw65s5aw51c9060mn132k43cz3hgwvaixavbnmvydql")))

(define-public crate-competitive-programming-rs-21.0.0 (c (n "competitive-programming-rs") (v "21.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "17p2yhdmkis1xq6iii3npa7bhfmrdpiina785gf6i94agm31bhc1")))

(define-public crate-competitive-programming-rs-22.0.0 (c (n "competitive-programming-rs") (v "22.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1rxxbqgh5zk3ry91iizjzbppbia2gcps1bbjbqlxmc7mjxbhix52")))

(define-public crate-competitive-programming-rs-23.0.0 (c (n "competitive-programming-rs") (v "23.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0jm80zwbhaxvghll00d0xqq2h1fbm923w50zb6ycgaapp7gi41v3")))

(define-public crate-competitive-programming-rs-24.0.0 (c (n "competitive-programming-rs") (v "24.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0s3f2vy23kp6z051p8wdyvz35wk7h1f1z8dn9xl43swg24crr4b4")))

(define-public crate-competitive-programming-rs-25.0.0 (c (n "competitive-programming-rs") (v "25.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "08l24mxvbw8b7cv45f9al57m1xqwzk37v156mrb431a4yinj6mc4")))

(define-public crate-competitive-programming-rs-27.0.0 (c (n "competitive-programming-rs") (v "27.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1xb6k0d04gasrbkmgbdfgcr885blfwi28wa61sf7ml4pih5x61q2")))

(define-public crate-competitive-programming-rs-28.0.0 (c (n "competitive-programming-rs") (v "28.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0bzgkg7c79ywwzw2xf4drdh9m7qxkcyijn9zpw876fp57qzvxyzr")))

(define-public crate-competitive-programming-rs-29.0.0 (c (n "competitive-programming-rs") (v "29.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1x0sz7ikh7298lwd68xlslpnl4giyx4afxdwrwa00iwqjwgsvjiy")))

(define-public crate-competitive-programming-rs-30.0.0 (c (n "competitive-programming-rs") (v "30.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0b5slq126jg2b4gzgwm90q0a434rppd1abbnfzb077h3gpca8x6v")))

(define-public crate-competitive-programming-rs-32.0.0 (c (n "competitive-programming-rs") (v "32.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1f0wgj8aqfdw6y1qn94r1vbknvc7j4zvd73hdxhhmhq8bpiwqskj")))

(define-public crate-competitive-programming-rs-33.0.0 (c (n "competitive-programming-rs") (v "33.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0y1dz7fkr82dyb6yczr4bg7bc4jp18ldafdbz7r723wwzz4mz5vl")))

(define-public crate-competitive-programming-rs-34.0.0 (c (n "competitive-programming-rs") (v "34.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0wfni82648frn228j8ng2dh3mnk59h8cy6qchzfazsrz97msfny4")))

(define-public crate-competitive-programming-rs-38.0.0 (c (n "competitive-programming-rs") (v "38.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0y3xa0nz5ndrv2lhbqmf8fv6ch0awj6dfiai357pi57cdsyjmiaf")))

(define-public crate-competitive-programming-rs-39.0.0 (c (n "competitive-programming-rs") (v "39.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1kb9a54wl5v986ldi242a1p33lnwnrk8sxkbqix4ygmgki4xqdq3")))

(define-public crate-competitive-programming-rs-40.0.0 (c (n "competitive-programming-rs") (v "40.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "19pdpy3c8mglslmxbdwpnlshn89arn125ljxz700f9nsfs53jnkd")))

(define-public crate-competitive-programming-rs-41.0.0 (c (n "competitive-programming-rs") (v "41.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "17mdsp87jgnjdpbf51cy0q70nzcycfnmvyijjkg1nx0ga4qgfv4w")))

