(define-module (crates-io co mp complex_test_helper) #:use-module (crates-io))

(define-public crate-complex_test_helper-0.1.0 (c (n "complex_test_helper") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aw3h222c0069cqgqss1m4kl74q2jwhhalzhr2f1656vd2hx76kh")))

