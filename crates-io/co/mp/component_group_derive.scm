(define-module (crates-io co mp component_group_derive) #:use-module (crates-io))

(define-public crate-component_group_derive-1.0.0 (c (n "component_group_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zmlhr5skk6lhq3zqsvy1bfq56v5k9jw9csmlyq91pf9pb76dfya")))

(define-public crate-component_group_derive-2.0.0 (c (n "component_group_derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0ryhp462fx2qinfwcv72qpyh9gyjww94dcafyqp26s95l5p2j07p")))

(define-public crate-component_group_derive-3.0.0 (c (n "component_group_derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03dzy5z375dhrx30ndgxpvmqhhkqb3qgjv7lw02g1lfsjzl32kxh")))

