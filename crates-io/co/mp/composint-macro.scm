(define-module (crates-io co mp composint-macro) #:use-module (crates-io))

(define-public crate-composint-macro-0.1.0 (c (n "composint-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1500ixv20k5ya0jcghxazsy0f9rnci4br5bqiq26r1jfqj188h0z")))

