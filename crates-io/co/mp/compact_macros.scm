(define-module (crates-io co mp compact_macros) #:use-module (crates-io))

(define-public crate-compact_macros-0.1.0 (c (n "compact_macros") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.4.1") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "= 0.11.11") (d #t) (k 0)))) (h "12hf1vlzs1cfxmjh0i4lq1rd0f4alv3riqns5ir8kxbj5lalfy1y")))

