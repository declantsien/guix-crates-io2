(define-module (crates-io co mp compress) #:use-module (crates-io))

(define-public crate-compress-0.0.2 (c (n "compress") (v "0.0.2") (h "0ldv2c9aclzijwn7n2pvlwnmy7a9b31pf2w0x53s7ygwgrb3a55d")))

(define-public crate-compress-0.1.0 (c (n "compress") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1p7x31sp389d01dgsln3y7qwhigzgmxdba6sidg61vcmj73qni8v") (f (quote (("zlib") ("unstable") ("rle") ("lz4") ("flate") ("entropy") ("default" "bwt" "checksum" "entropy" "flate" "lz4" "zlib" "rle") ("checksum") ("bwt"))))))

(define-public crate-compress-0.1.1 (c (n "compress") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "08sgdbkdp5qh24adx2bwfrzdjw5hlpcy6pbfnrrl6lk2pq3jlc81") (f (quote (("zlib") ("unstable") ("rle") ("lz4") ("flate") ("entropy") ("default" "bwt" "checksum" "entropy" "flate" "lz4" "zlib" "rle") ("checksum") ("bwt"))))))

(define-public crate-compress-0.1.2 (c (n "compress") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0s4jgapzsir3d5gbdwidsh81b2srizjnmhvlsl9lpmwif5jz4xdi") (f (quote (("zlib" "flate" "checksum") ("unstable") ("rle") ("lz4") ("flate") ("entropy") ("default" "bwt" "checksum" "entropy" "flate" "lz4" "zlib" "rle") ("checksum") ("bwt"))))))

(define-public crate-compress-0.2.0 (c (n "compress") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0ya2l69gaxvrw57dc8s4wa0kkq102zdcd7yag3nwgha40liclg9j") (f (quote (("zlib" "flate" "checksum") ("unstable") ("rle") ("lz4") ("flate") ("entropy") ("default" "bwt" "checksum" "entropy" "flate" "lz4" "zlib" "rle") ("checksum") ("bwt"))))))

(define-public crate-compress-0.2.1 (c (n "compress") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0pn5mplcssvplw6638rn8r4pid46wfd0w5w9m9n352g0m633ds42") (f (quote (("zlib" "flate" "checksum") ("unstable") ("rle") ("lz4") ("flate") ("entropy") ("default" "bwt" "checksum" "entropy" "flate" "lz4" "zlib" "rle") ("checksum") ("bwt"))))))

