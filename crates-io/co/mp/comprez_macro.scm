(define-module (crates-io co mp comprez_macro) #:use-module (crates-io))

(define-public crate-comprez_macro-0.1.0 (c (n "comprez_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q916i8q5gkpzsra58k9453cf6dwi4c271ii7m27m9fh95hxmnnv")))

(define-public crate-comprez_macro-0.1.1 (c (n "comprez_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15ffxrv2pz91gwn7acby9cj1h6znk22h38xlsnjpl6p1yvlvyzw7")))

(define-public crate-comprez_macro-0.2.0 (c (n "comprez_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19fnkhq2c7hqrx3p45i556gvg03f3gf3i5p8arifbi7mvqvh2c22")))

(define-public crate-comprez_macro-0.2.5 (c (n "comprez_macro") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g0b0mp9j0jdl9lwx5gbqjd6dzpg4kyaj26lsz9mawf763fmbsx8")))

(define-public crate-comprez_macro-0.2.6 (c (n "comprez_macro") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0466v8xxwm68wx9ayxlmi56w15wx0nd40nhv80lyyz4xagcf71bn")))

(define-public crate-comprez_macro-0.2.7 (c (n "comprez_macro") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nyzxvcqz1mmyd3d9js1hjm3ymbwhl4b5hpxj84pcb4p74vzqsj1")))

