(define-module (crates-io co mp compressed-sparse-fiber) #:use-module (crates-io))

(define-public crate-compressed-sparse-fiber-0.0.1 (c (n "compressed-sparse-fiber") (v "0.0.1") (d (list (d (n "sequence_trie") (r "^0.3.6") (d #t) (k 0)))) (h "1nhx33700nhzdwx37hjik27adqxb15yp9bglxs8s94fk9xv6qira")))

(define-public crate-compressed-sparse-fiber-0.0.2 (c (n "compressed-sparse-fiber") (v "0.0.2") (d (list (d (n "sequence_trie") (r "^0.3.6") (d #t) (k 0)))) (h "0dwzrr874jg9yzrcydn4lxyi80k5va63hbkbcady9y4ffrc61k6d")))

(define-public crate-compressed-sparse-fiber-0.0.3 (c (n "compressed-sparse-fiber") (v "0.0.3") (d (list (d (n "sequence_trie") (r "^0.3.6") (d #t) (k 0)))) (h "1n04qj62hxl8jg62madmx9mjp10a86cml4bc2mkq59hpmlgz4qd8")))

(define-public crate-compressed-sparse-fiber-0.0.4 (c (n "compressed-sparse-fiber") (v "0.0.4") (d (list (d (n "sequence_trie") (r "^0.3.6") (f (quote ("btreemap"))) (d #t) (k 0)))) (h "1p1zhqy7nhzprrhp8dhwcri4gfhb38lc5ph54dbbz6x5q165009p")))

(define-public crate-compressed-sparse-fiber-0.0.5 (c (n "compressed-sparse-fiber") (v "0.0.5") (d (list (d (n "sequence_trie") (r "^0.3.6") (f (quote ("btreemap"))) (d #t) (k 0)))) (h "1sashp8kzq3ab8w6g3zl9da2wxcm69868a4fcqm24sypksi6k6w7")))

(define-public crate-compressed-sparse-fiber-0.0.6 (c (n "compressed-sparse-fiber") (v "0.0.6") (d (list (d (n "sequence_trie") (r "^0.3.6") (f (quote ("btreemap"))) (d #t) (k 0)))) (h "1hkbphqnwhwn8wd484hvj3l2kzzzyzsw50aakz0xlri6h1pb7s4x")))

