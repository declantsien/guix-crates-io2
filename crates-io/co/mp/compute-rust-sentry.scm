(define-module (crates-io co mp compute-rust-sentry) #:use-module (crates-io))

(define-public crate-compute-rust-sentry-0.1.2 (c (n "compute-rust-sentry") (v "0.1.2") (d (list (d (n "fastly") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde-human-readable"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1djivxgrc2mq558sbin237fivxvd04ifybczyv2krgxd3lvassny")))

