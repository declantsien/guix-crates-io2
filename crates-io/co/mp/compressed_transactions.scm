(define-module (crates-io co mp compressed_transactions) #:use-module (crates-io))

(define-public crate-compressed_transactions-1.0.0 (c (n "compressed_transactions") (v "1.0.0") (d (list (d (n "bitcoin") (r "^0.31.1") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.18.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "honggfuzz") (r "^0.5") (d #t) (k 0)) (d (n "secp256k1") (r "^0.28.2") (f (quote ("global-context"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fdv4amc85dgi7z41iakn3n4cpv2gabwsav18qal35h7lydwm94h")))

