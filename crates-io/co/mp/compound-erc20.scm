(define-module (crates-io co mp compound-erc20) #:use-module (crates-io))

(define-public crate-compound-erc20-0.1.0 (c (n "compound-erc20") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "compound-casper-erc20") (r "^0.1.0") (d #t) (k 0)))) (h "1ll8q4fwq9nrli0s1zrn9jns80q5wbmj6x8qjw8cirq7diakkl15")))

