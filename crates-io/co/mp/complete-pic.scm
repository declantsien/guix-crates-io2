(define-module (crates-io co mp complete-pic) #:use-module (crates-io))

(define-public crate-complete-pic-0.1.0 (c (n "complete-pic") (v "0.1.0") (d (list (d (n "x86_64") (r "^0.14.10") (f (quote ("instructions"))) (k 0)))) (h "0sbxr6y8nnpy34fb2zkcddnlwzmyn1bg55g3ypav1cimrg4lsqfx") (f (quote (("default" "apic") ("apic") ("8259pic"))))))

(define-public crate-complete-pic-0.1.1 (c (n "complete-pic") (v "0.1.1") (d (list (d (n "x86_64") (r "^0.14.10") (f (quote ("instructions"))) (k 0)))) (h "0whx5k74n7zcc8xvbg7pd1g953jappchjfb2n7zrmgzd0rbxczva") (f (quote (("default" "apic") ("apic") ("8259pic"))))))

(define-public crate-complete-pic-0.2.0 (c (n "complete-pic") (v "0.2.0") (d (list (d (n "x86_64") (r "^0.14.10") (f (quote ("instructions"))) (k 0)))) (h "0vc25ccvr8ghfnvwg6qfzwgsx2h6wy6isgy8c8gxdbflb2a28ghc") (f (quote (("default" "apic") ("apic") ("8259pic"))))))

(define-public crate-complete-pic-0.3.0 (c (n "complete-pic") (v "0.3.0") (d (list (d (n "x86_64") (r "^0.14.10") (f (quote ("instructions"))) (k 0)))) (h "04v8d6j805qkpfy37ifwwqx4ribky7yyf5i7gnd6mym0nmrl7g0n") (f (quote (("default" "apic") ("apic") ("8259pic"))))))

(define-public crate-complete-pic-0.3.1 (c (n "complete-pic") (v "0.3.1") (d (list (d (n "x86_64") (r "^0.14.10") (f (quote ("instructions"))) (k 0)))) (h "0g946gp19q7zqhdgl19kpg4n60j0x99z334358s8wv0js7zm3cvg") (f (quote (("default" "apic") ("apic") ("8259pic"))))))

