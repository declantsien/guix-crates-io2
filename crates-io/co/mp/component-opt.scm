(define-module (crates-io co mp component-opt) #:use-module (crates-io))

(define-public crate-component-opt-0.1.0 (c (n "component-opt") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "compile-claw") (r "^0.2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.200.0") (d #t) (k 0)) (d (n "wasm-opt") (r "^0.116.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.200.0") (d #t) (k 0)) (d (n "wasmprinter") (r "^0.2.75") (d #t) (k 0)) (d (n "wasmtime") (r "^15.0.0") (f (quote ("component-model"))) (d #t) (k 2)))) (h "1pvdz3yx00av2lmpwy591a5lfwvr3krnzag4qlihlihdgbyzny67")))

