(define-module (crates-io co mp compose_spec_macros) #:use-module (crates-io))

(define-public crate-compose_spec_macros-0.1.0 (c (n "compose_spec_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "syn") (r "^2.0.1") (d #t) (k 0)))) (h "0z4r9n38h5346ksfilkaxa76zzivdhvj9axq8yjm6wv9nhl1pf25") (r "1.70")))

