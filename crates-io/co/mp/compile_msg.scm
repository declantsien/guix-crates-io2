(define-module (crates-io co mp compile_msg) #:use-module (crates-io))

(define-public crate-compile_msg-0.1.0 (c (n "compile_msg") (v "0.1.0") (h "1s13c3b55bclbgckrdfz7i4zaxnlsvfpgxwx3r7vzxx82bmxm20h")))

(define-public crate-compile_msg-0.1.1 (c (n "compile_msg") (v "0.1.1") (h "0sa55344x5d28b2rbiq699gg4y69ijn3916vn64nq6kwcb2h75fw")))

(define-public crate-compile_msg-0.1.2 (c (n "compile_msg") (v "0.1.2") (h "090xcjpx5zjr2c0cmx0jd1s9bcjlql9rajjkbd0ksb84b6bjchv7")))

(define-public crate-compile_msg-0.1.3 (c (n "compile_msg") (v "0.1.3") (h "0k1faipbfmpbvzcykrq5s76wg1qjnihsl3kha78f1q2478bm1rl3")))

(define-public crate-compile_msg-0.1.4 (c (n "compile_msg") (v "0.1.4") (h "0xvj8d6w8zms6ib5lbgpfdc1hc0czbanibwqhjr7zppgvpkb7349")))

(define-public crate-compile_msg-0.1.5 (c (n "compile_msg") (v "0.1.5") (h "1rz7965549w92rgp1rz3y9rxx0sg5z8c4dwg1b8y9d318iid08rl")))

