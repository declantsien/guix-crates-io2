(define-module (crates-io co mp comp-flow) #:use-module (crates-io))

(define-public crate-comp-flow-0.1.0 (c (n "comp-flow") (v "0.1.0") (d (list (d (n "eqsolver") (r "^0.1.3") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1inxbgniw9rb5sjb3kjfswv7yfm5fkfjv0nmh5a6kbc6ir1k6wcb")))

(define-public crate-comp-flow-0.1.1 (c (n "comp-flow") (v "0.1.1") (d (list (d (n "eqsolver") (r "^0.1.3") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1b6d39d529rbias5ljp8b3cpwnfgc7nfx5vx9dp05pi1rw2fciz0")))

