(define-module (crates-io co mp comparable_derive) #:use-module (crates-io))

(define-public crate-comparable_derive-0.1.0 (c (n "comparable_derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z867kb49vv50h74264c92hdb9wm01gswmz3m35ly2pi6mvhczzm")))

(define-public crate-comparable_derive-0.2.0 (c (n "comparable_derive") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bmrh2ksnf1rp9mf4l79z29nizlzy3bf3yhcyrypzpchrxaaf250")))

(define-public crate-comparable_derive-0.3.0 (c (n "comparable_derive") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bf6kwbr59j6s3kb54mmsc7dwd13cp9qj7hkyp1kzifsmrlbdfd1")))

(define-public crate-comparable_derive-0.3.1 (c (n "comparable_derive") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0301h3ifdr3vialvzmsq5jfijnbcm94abprgikplp9jc1fibv7bl")))

(define-public crate-comparable_derive-0.3.2 (c (n "comparable_derive") (v "0.3.2") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mklswhz30azwdvf4n9v0f398gpikwmavbcx3y0d99lvsycckcxl")))

(define-public crate-comparable_derive-0.4.0 (c (n "comparable_derive") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cixy8h4ygfv07r13cwrxzfr0aa6lpyh8nad6z2aqsz6r3995maw")))

(define-public crate-comparable_derive-0.5.0 (c (n "comparable_derive") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0djgg5yr48hbawxrn4fyx13g2r49aih1in2j4sz2b7ff5554mzc3")))

(define-public crate-comparable_derive-0.5.1 (c (n "comparable_derive") (v "0.5.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y8vg3xp1n0ji9np7yjwqfmb243jpablkxxwb3dq5yvdbpn4z1f2")))

(define-public crate-comparable_derive-0.5.2 (c (n "comparable_derive") (v "0.5.2") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dfjfx0203d37dpmp8iykj6hx5pab21fkzc0mgf35rc8zm4j3hcb")))

(define-public crate-comparable_derive-0.5.3 (c (n "comparable_derive") (v "0.5.3") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1923mdzdlw7hj5rz96k2w282aq2gpxk8mnqagqmqfvhv29awarsb") (f (quote (("serde"))))))

(define-public crate-comparable_derive-0.5.4 (c (n "comparable_derive") (v "0.5.4") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a5qpv4cpcv0yc31fqnrs7q7ypsf1v4zsdhxbnf9kf2f0m09qjx5") (f (quote (("serde"))))))

