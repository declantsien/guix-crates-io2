(define-module (crates-io co pp coppers) #:use-module (crates-io))

(define-public crate-coppers-0.0.1 (c (n "coppers") (v "0.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0yaj79c0lsfmpnxmlap0922sy7qwdbva24c621gprwqx4c4h50x9") (y #t)))

(define-public crate-coppers-0.1.0 (c (n "coppers") (v "0.1.0") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.16") (f (quote ("auto-initialize"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0haq5q2lx73wgysmwmnxfshyhcrf28b3x76q39kffalbjqqkc7gv") (f (quote (("visualization" "pyo3"))))))

(define-public crate-coppers-0.1.1 (c (n "coppers") (v "0.1.1") (d (list (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.16") (f (quote ("auto-initialize"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "064kbycfz5cl15c77y0y5jcpfyfpm5xrmyijaj5d7gzsanvlvs2x") (f (quote (("visualization" "pyo3"))))))

