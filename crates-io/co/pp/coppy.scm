(define-module (crates-io co pp coppy) #:use-module (crates-io))

(define-public crate-coppy-0.1.0 (c (n "coppy") (v "0.1.0") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)))) (h "0pv2wiszaccybm31wv87vx589pwprr7a5v1v8qsmivglprfhr3pk")))

(define-public crate-coppy-0.1.1 (c (n "coppy") (v "0.1.1") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)))) (h "10fps16qvwa3c9mf47b6s4jch8qhdccvafrw0h9vklng6575y40v")))

(define-public crate-coppy-0.1.2 (c (n "coppy") (v "0.1.2") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)))) (h "12438rkah05l1w40xkzx3xxrhjn442qh75s4fsh03v6wn24fpgqp")))

