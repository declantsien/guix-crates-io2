(define-module (crates-io co pp coppeliasim_zmq_remote_api) #:use-module (crates-io))

(define-public crate-coppeliasim_zmq_remote_api-4.6.0 (c (n "coppeliasim_zmq_remote_api") (v "4.6.0") (d (list (d (n "ciborium") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9.2") (f (quote ("vendored"))) (d #t) (k 0)))) (h "14pj028xqag61grsc8ja1n0jazd5w8r3kgji8p3d1ik72hb86wj3")))

(define-public crate-coppeliasim_zmq_remote_api-4.6.1 (c (n "coppeliasim_zmq_remote_api") (v "4.6.1") (d (list (d (n "ciborium") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9.2") (f (quote ("vendored"))) (d #t) (k 0)))) (h "0lr483agv9k1swiqlp02wwbvpbjfwr432l07fc6si23ld425jh9k")))

