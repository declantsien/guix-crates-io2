(define-module (crates-io co pp copperline) #:use-module (crates-io))

(define-public crate-copperline-0.0.1 (c (n "copperline") (v "0.0.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)))) (h "08z9glyzj97yg9iqwv4k7kirngx56pihwb2mbf6npm7704q8yk6j")))

(define-public crate-copperline-0.0.2 (c (n "copperline") (v "0.0.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)))) (h "01w584kwz709ffgn0rp8dld58yk1s7kmjp3bvhiapxwbnp1q3fsy")))

(define-public crate-copperline-0.0.3 (c (n "copperline") (v "0.0.3") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)))) (h "0kp1d4wd493qcla2r06bkyqxwh4fv6j9lcw1gx8k7gf8aan9i6q7")))

(define-public crate-copperline-0.1.0 (c (n "copperline") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)))) (h "08ya6mp2z94diwj9irdn5a2ba1bqb6h0nk880r8ghz436kpfi4wh")))

(define-public crate-copperline-0.1.1 (c (n "copperline") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)))) (h "0hmhxkbmldhbwrax9hjvyczisf8jjiv22rxwyxq6qnawr06pxxcc")))

(define-public crate-copperline-0.1.2 (c (n "copperline") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "nix") (r "^0.3.9") (d #t) (k 0)))) (h "1pkgfk1sww9v3f47g1zj91d1l6j44x610axbv047niarhv4pvvq9")))

(define-public crate-copperline-0.1.3 (c (n "copperline") (v "0.1.3") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)))) (h "0w1wskjsk0zi3n9a2j2h7bxd7yxjxqvw4yc964jfmh51ncrpfzq3")))

(define-public crate-copperline-0.2.0 (c (n "copperline") (v "0.2.0") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)))) (h "13as168zkm3yccazvz4fnnfb4q29lm5c8dhwjimzrlwlb18gf216")))

(define-public crate-copperline-0.2.1 (c (n "copperline") (v "0.2.1") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)))) (h "13yfxmnpwgbqhv3i8c9ga8qh3bf0dm9z33qykkljci8ay6vki9ms")))

(define-public crate-copperline-0.3.0 (c (n "copperline") (v "0.3.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "nix") (r "^0.4.0") (d #t) (k 0)) (d (n "strcursor") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.3") (d #t) (k 0)))) (h "1wr6svf23jinq0rs5fx2d57ihf1ixr0j86f2slxrsi3p3d3sd01y")))

