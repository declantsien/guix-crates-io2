(define-module (crates-io co ng congruence) #:use-module (crates-io))

(define-public crate-congruence-0.1.0 (c (n "congruence") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "06f6rpmj7ijrp5mg7vpkqihgyrwxwvyn67v9k2xwyjva0n0qva6v")))

(define-public crate-congruence-0.1.1 (c (n "congruence") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "06jg5n46lhqjf84p2jwzy47xqyw99zhxqqa52k7xf82rny1wsyyh")))

(define-public crate-congruence-0.2.0 (c (n "congruence") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.11") (f (quote ("raw"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "06r11hc41z7r6cnc2yy4qhqhkmx5nq5hvxqydm16qi6ly6s3p1l4")))

(define-public crate-congruence-0.3.0 (c (n "congruence") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.11") (f (quote ("raw"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "02jw8b6qflg6dcdgz7wb047g82lmpj1bd0v33c6vc8rizq11dph4")))

(define-public crate-congruence-0.4.0 (c (n "congruence") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11") (f (quote ("raw"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "1rr44g9x6a2xrir3r2szqn4zqd1w4xj3yd5mrxf49mvxwbw1f9pi")))

