(define-module (crates-io co nh conhash) #:use-module (crates-io))

(define-public crate-conhash-0.1.0 (c (n "conhash") (v "0.1.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)))) (h "05swwv6y4kc1nb5zmk6b33gnj3fcy5lhmnrcy81w3ip7iyl9hrbq")))

(define-public crate-conhash-0.2.0 (c (n "conhash") (v "0.2.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)))) (h "06a7i86v8148v30bfvbdxgwilw6pa7g3gspn10nmpiyqnkf83v6y")))

(define-public crate-conhash-0.3.0 (c (n "conhash") (v "0.3.0") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)))) (h "01y6bsa6ldg11v1fj5jin2134bwnxb5slvpcmhc1w485qg6m9fx8")))

(define-public crate-conhash-0.3.1 (c (n "conhash") (v "0.3.1") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)))) (h "1hd4wkb5c5680j6g8vzvw5cpz0l24apifqfhrwra88yz6ljj5jkc")))

(define-public crate-conhash-0.3.2 (c (n "conhash") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)))) (h "1f3ka6ka83zwm9rb1mjg48fmbax29il2rnb7sisbyvdqqk29cz3y")))

(define-public crate-conhash-0.3.3 (c (n "conhash") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1zn55qs0iwfl44ql0a44s2y4a4c0glf1wnyvg203bhkzal15npgl")))

(define-public crate-conhash-0.4.0 (c (n "conhash") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)))) (h "1jhzkf744si69mrvg4il1p8pqdysh9cgl530igcx0y47096kdmlr")))

(define-public crate-conhash-0.5.0 (c (n "conhash") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 2)))) (h "0xf7siza443c67sdcgsvp0rpdcvvqbr3hv28ia5l5159b6h689aw")))

(define-public crate-conhash-0.5.1 (c (n "conhash") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 2)))) (h "13dgzz9n0yrd2whyspmrq8qhwnjwwhxdcm83vdwr0l2kgq5rakv7")))

