(define-module (crates-io co #{2m}# co2meter-rs) #:use-module (crates-io))

(define-public crate-co2meter-rs-0.1.0 (c (n "co2meter-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.8.0") (d #t) (k 0)))) (h "02430j8w2nzjzgk9yly4k0s4irxiawchr9qbd8z5whpla5p3lpc5")))

(define-public crate-co2meter-rs-0.1.1 (c (n "co2meter-rs") (v "0.1.1") (d (list (d (n "hidapi") (r "^1.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.8.0") (d #t) (k 0)))) (h "1qqf3jfva2y75cm0dq2pjgzvafg8jbwc0dc72d5mn0r8mkf613ba")))

