(define-module (crates-io co py copydeps) #:use-module (crates-io))

(define-public crate-copydeps-5.0.0 (c (n "copydeps") (v "5.0.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "goblin") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)))) (h "14qsjdmia3l5mc0087zy5hfsys8dnl5l0r90d49vvq9izzydad7q")))

(define-public crate-copydeps-5.0.1 (c (n "copydeps") (v "5.0.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "goblin") (r "^0.5.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)))) (h "02w17brinz0vhr3n96g3srjx1452qvzxyvjkyg8p7b2a5k4y5mxh")))

