(define-module (crates-io co py copy_to_output) #:use-module (crates-io))

(define-public crate-copy_to_output-1.0.0 (c (n "copy_to_output") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)))) (h "1ylr0bbva0f27l700xxc8ahvs0v7281gfiygfg7r6fr1lrh1mz5r")))

(define-public crate-copy_to_output-2.0.0 (c (n "copy_to_output") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)))) (h "15k7na2xxgz4f20q4yvrn25kf00dpq7609l59nh3x7hrg6fbxc1q")))

(define-public crate-copy_to_output-2.0.1 (c (n "copy_to_output") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)))) (h "1z456n61qn44wdb9rv9k6hd550p37abqpsq3fmip3fhxj5k0cbw5")))

(define-public crate-copy_to_output-2.1.0 (c (n "copy_to_output") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)))) (h "0qbrjki4ph56rszk65baczi51mfxav5xi10a3f06xg260il9pwnj")))

(define-public crate-copy_to_output-2.2.0 (c (n "copy_to_output") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "build-target") (r "^0.4") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3") (d #t) (k 0)))) (h "1bpbs94f3iyb2r1ik10dh34f7vjfipyi4xf0acv3ac5snsqrwfp4")))

