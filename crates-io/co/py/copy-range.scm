(define-module (crates-io co py copy-range) #:use-module (crates-io))

(define-public crate-copy-range-0.1.0 (c (n "copy-range") (v "0.1.0") (h "1bc6sqp4m3iwayxrijiasmxmzliqrh9vsm34643h56wvb6rdb5fl") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-copy-range-0.1.1 (c (n "copy-range") (v "0.1.1") (h "0v7nk4z9ngi3fsrkm1y6v94qwvzxivxxl756pr9g4a2yyw1vy336") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

