(define-module (crates-io co py copyless) #:use-module (crates-io))

(define-public crate-copyless-0.1.0 (c (n "copyless") (v "0.1.0") (h "0gb3wm5ybc8dp5r1i5ngkyk93j4dm5c8mh3c6wr1f6k3dq5mgsmd")))

(define-public crate-copyless-0.1.1 (c (n "copyless") (v "0.1.1") (h "0zanxp5gwg2shyg10ciymkyakfas9b8dp6kkdgdargfz1r3mzmji")))

(define-public crate-copyless-0.1.2 (c (n "copyless") (v "0.1.2") (h "08vw3pvb32np65adqigil1wvij8na7z1gmhrsmfv7ixmsci7gpjr")))

(define-public crate-copyless-0.1.3 (c (n "copyless") (v "0.1.3") (h "03lsdydfvx0qz2gl4kvpj1vvnhgy4388kisq2lzlfggh2jds5zzq")))

(define-public crate-copyless-0.1.4 (c (n "copyless") (v "0.1.4") (h "09s1fagd14fqch3xq9ijschay014la2i8hqfxw2rr95jkxncbybg")))

(define-public crate-copyless-0.1.5 (c (n "copyless") (v "0.1.5") (h "0dmmxsq3m0i6g9s2kj96n777qhmm7vjgv4r7agc2v6w6bl7rdpx2")))

