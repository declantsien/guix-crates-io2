(define-module (crates-io co py copypasteck) #:use-module (crates-io))

(define-public crate-copypasteck-0.1.0 (c (n "copypasteck") (v "0.1.0") (h "06ynvna1g51b1kcsadryyz08q6cg624vmnfvam95lkj9864950yv")))

(define-public crate-copypasteck-0.1.1 (c (n "copypasteck") (v "0.1.1") (h "11jbckzn1c4fpr3xc062h6kvkpv1fcnvh902ag1i9d831pz8pb7b")))

(define-public crate-copypasteck-0.1.2 (c (n "copypasteck") (v "0.1.2") (h "1h3vcpx8v908z8rf4sby4y9wpb7c2qnp6lsv77c3kxvv3bx1iknv")))

(define-public crate-copypasteck-0.1.3 (c (n "copypasteck") (v "0.1.3") (h "0cnsidsmidkq8h5q62sdmf8br1kr24a4lh21pgjq1diiybf6j810")))

(define-public crate-copypasteck-0.1.4 (c (n "copypasteck") (v "0.1.4") (h "1ch339c50qv8jryw0j7am26s3m452wk5jjyn4r37ygwxhd3dzpyp")))

