(define-module (crates-io co py copyrat) #:use-module (crates-io))

(define-public crate-copyrat-0.5.5 (c (n "copyrat") (v "0.5.5") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "sequence_trie") (r "^0.3.6") (d #t) (k 0)) (d (n "termion") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "000887q413mdl2b3zxjwf8i18vgwkpfplhmp4mbqv5hfafh670vc")))

