(define-module (crates-io co py copycat) #:use-module (crates-io))

(define-public crate-copycat-0.1.0 (c (n "copycat") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clipboard2") (r "^0.1.1") (d #t) (k 0)))) (h "1yz1s5wcn042m6yashfwjyp8a3m1295m7s2c65zjfk177ivy1062")))

(define-public crate-copycat-0.1.1 (c (n "copycat") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clipboard2") (r "^0.1.1") (d #t) (k 0)))) (h "1gmpxg24wdwzqqqdb2hbsm9x9bmxjry99wnpr085mpxzrw6b1ry5")))

(define-public crate-copycat-0.2.0 (c (n "copycat") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (k 0)))) (h "0m4l0bf4wbz5q930qvk3zfy8izkqicgic2ljvqh65450bnv9z696")))

(define-public crate-copycat-0.2.1 (c (n "copycat") (v "0.2.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (k 0)))) (h "1a5znk0r3ch979ghfpssrs3cypcq9npfddwc328l3adm14by85pw")))

(define-public crate-copycat-0.3.0 (c (n "copycat") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (k 0)))) (h "0z18xqyc70pfkjfnb2h62in0qmn5rhkrzxq7hbnpwvczdyis49bw")))

(define-public crate-copycat-0.4.0 (c (n "copycat") (v "0.4.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (k 0)))) (h "15nix85vlx80y3h4hy1g6yz8iq8b75dj4j9pxyyv6m727mwrypyz")))

(define-public crate-copycat-0.5.0 (c (n "copycat") (v "0.5.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.1.0") (d #t) (k 0)))) (h "0i0hfyiz052mx1mbr2lvq8fk66kaz8b4dp5plj19jnq7f1rw8v04")))

(define-public crate-copycat-0.6.0 (c (n "copycat") (v "0.6.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.1.0") (d #t) (k 0)))) (h "1j5biqldwpaz9kpss8rr182h755ax92akdcycqllndcmp44wsacp")))

(define-public crate-copycat-0.6.1 (c (n "copycat") (v "0.6.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.1.0") (d #t) (k 0)))) (h "1cnnj41x76w2b9qgxpzmnc9m703c3ixpcx5jp789fqykh8mfwrx1")))

(define-public crate-copycat-0.6.2 (c (n "copycat") (v "0.6.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.1.0") (d #t) (k 0)))) (h "0s8wnzjl90r3xw3jz1kpz5xql8r9qndw4jm9iyq72l01dp6j7xd1")))

(define-public crate-copycat-0.6.3 (c (n "copycat") (v "0.6.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.1.0") (d #t) (k 0)))) (h "0nv7ja67yyqd8nhqdkfcryl70c7hv8rr997zrzqvs913k8y095a6")))

(define-public crate-copycat-0.7.0 (c (n "copycat") (v "0.7.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.1.0") (d #t) (k 0)))) (h "13kx0mj0rfhzjg54nlr0wd3dpwv86f23wyvxassmypfvh2rzpvrh")))

(define-public crate-copycat-0.7.1 (c (n "copycat") (v "0.7.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clipboard-win") (r "^3.1.0") (d #t) (k 0)))) (h "0c9awdg3pla7jf92zbpv43kl594ph7wykanlz7lj7k0i7ljnxyx7")))

