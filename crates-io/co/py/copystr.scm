(define-module (crates-io co py copystr) #:use-module (crates-io))

(define-public crate-copystr-0.0.1 (c (n "copystr") (v "0.0.1") (h "1isqgczk1k2lbvx1hac3lk7d6fz2cbbwhkwvvshyaf0j8f4kz014")))

(define-public crate-copystr-0.0.2 (c (n "copystr") (v "0.0.2") (h "0lx0iqqkxyiim0pk7hjdq48p1c2hhsghicgkhn4kclzfnphymh8h")))

(define-public crate-copystr-0.0.3 (c (n "copystr") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "1mir9srw9wxa4rz7sb98rpclqa14mpd014b43d06cwh9cghw1gmi")))

(define-public crate-copystr-0.0.4 (c (n "copystr") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "008955rl0f165j84cb99ms26viy5h2caqgjy7ka0zh8mvz0nxgki")))

(define-public crate-copystr-0.0.5 (c (n "copystr") (v "0.0.5") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "0hqdvhmjcwpmphzqvzg9k303b9rikkj53zh5mzx1m19wiwxxjzjl")))

