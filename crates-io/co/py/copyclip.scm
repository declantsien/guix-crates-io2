(define-module (crates-io co py copyclip) #:use-module (crates-io))

(define-public crate-copyclip-0.1.0 (c (n "copyclip") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)))) (h "09xb45m2bg84g755bwd4wjrc4f5mzgrg66vn5yhbjqsgzyk354h8")))

