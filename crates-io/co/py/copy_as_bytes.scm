(define-module (crates-io co py copy_as_bytes) #:use-module (crates-io))

(define-public crate-copy_as_bytes-0.1.0 (c (n "copy_as_bytes") (v "0.1.0") (h "17n6jggn0i0hxxrvbigyk5jnpwgc523m1i0n31xhvzc2b8yg7m8f")))

(define-public crate-copy_as_bytes-0.1.1 (c (n "copy_as_bytes") (v "0.1.1") (h "1gx30vxfv1i1iq5dkpgb57csz0mfz01an9jfg925ndhdr10m2x2l")))

