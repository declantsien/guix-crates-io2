(define-module (crates-io co py copy_arena) #:use-module (crates-io))

(define-public crate-copy_arena-0.1.0 (c (n "copy_arena") (v "0.1.0") (h "07nmhvkdczqp76ci7sb7v39xw8jp1m9ylhrrl717dg835bwld440")))

(define-public crate-copy_arena-0.1.1 (c (n "copy_arena") (v "0.1.1") (h "0h0vg69y0xh3wd8bmwvzs57w21gihfxhj91f2mz40nv4k9ljccwr")))

