(define-module (crates-io co py copy_in_place) #:use-module (crates-io))

(define-public crate-copy_in_place-0.1.0 (c (n "copy_in_place") (v "0.1.0") (h "03ipq2m3v9ssykdcjw2qiy75cc7s13mjdgm50m2z7aprdzwzn71j")))

(define-public crate-copy_in_place-0.1.1 (c (n "copy_in_place") (v "0.1.1") (h "0nq4wmj4hvdibmjjzz0rinv1ff9aamdfsbwsis8z0ml9znnlqqdy")))

(define-public crate-copy_in_place-0.2.0 (c (n "copy_in_place") (v "0.2.0") (h "1bdsvmi3v107f51q78k5x4dl68k5317d68a7l3lvajzl3rms94mp")))

(define-public crate-copy_in_place-0.2.1 (c (n "copy_in_place") (v "0.2.1") (h "1s5jydr8qaxr55kyfmbnc4nxpyl1mj4mzijqy04wm3lgk7ja8w1l")))

(define-public crate-copy_in_place-0.2.2 (c (n "copy_in_place") (v "0.2.2") (h "0hkgxpyf5mr6w6hjklaz730l0m3mldjf3h56dnnzhf4jw69ywzp5")))

