(define-module (crates-io co py copy_impl) #:use-module (crates-io))

(define-public crate-copy_impl-0.3.0 (c (n "copy_impl") (v "0.3.0") (h "1ln6npwp5q6mx14jqq2kal1ilashi324rjxzpnjx2ssc09isdf53") (y #t)))

(define-public crate-copy_impl-0.3.1 (c (n "copy_impl") (v "0.3.1") (h "1zn7f242xp9zaxqp5sqsm9cffrg01zrflzvij5r9lhq8dqhdzs4n") (y #t)))

(define-public crate-copy_impl-0.3.2 (c (n "copy_impl") (v "0.3.2") (h "0qfls692iz0c857pkf24xhfprnm26cfllhzzh8p31x91hrs8mlk7")))

(define-public crate-copy_impl-0.3.3 (c (n "copy_impl") (v "0.3.3") (h "01xmfk2il3zgp8f6k56q4pfipl86k43z2pkk4mw0c3sancsw3rya")))

