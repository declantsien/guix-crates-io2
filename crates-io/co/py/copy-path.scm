(define-module (crates-io co py copy-path) #:use-module (crates-io))

(define-public crate-copy-path-0.1.0 (c (n "copy-path") (v "0.1.0") (d (list (d (n "clipboard-win") (r "^4.5.0") (d #t) (k 0)))) (h "15vwchhl6xc176iln9djj6p2zj4r8b5b6z3lqapq80xyn9bh0xxx")))

(define-public crate-copy-path-0.1.1 (c (n "copy-path") (v "0.1.1") (d (list (d (n "clipboard-win") (r "^4.5.0") (d #t) (k 0)))) (h "1ipr8q00yxh54a3gcnax53fwiqc8bjgs8yznvq6jl19nx1mnkrvv")))

(define-public crate-copy-path-0.1.2 (c (n "copy-path") (v "0.1.2") (d (list (d (n "clipboard-win") (r "^4.5.0") (d #t) (k 0)))) (h "0p681885crsn62w8akvd0iypi1v26nrigsby6lsvk3f4g50c020q")))

(define-public crate-copy-path-0.1.3 (c (n "copy-path") (v "0.1.3") (d (list (d (n "clipboard-win") (r "^4.5.0") (d #t) (k 0)))) (h "0f0f6wvxwmrfkns8rzraksv9iah8b3gv5j3yw15px6iqc4l74iks")))

