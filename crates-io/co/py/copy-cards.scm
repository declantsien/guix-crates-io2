(define-module (crates-io co py copy-cards) #:use-module (crates-io))

(define-public crate-copy-cards-0.1.0 (c (n "copy-cards") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "14vpzx79f6np3vwfsy5qcf7v8p7fxd24vvcxrqfnhb6i1rff3z0x")))

(define-public crate-copy-cards-1.1.0 (c (n "copy-cards") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "1yga2sdzgppd38snbng8zb9c00ihxxl2bwcpbb2j7w7rbq3wjq2m")))

(define-public crate-copy-cards-1.1.1 (c (n "copy-cards") (v "1.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "0pxzal8d3fvj41q7rnsm1fq2wn3safh2vm8rkq0bcicp54rxd5y6")))

(define-public crate-copy-cards-1.1.2 (c (n "copy-cards") (v "1.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "1hvwmn71svdiy42hkggzxr6fdm2hh4rkibrpxz6shhhv5qfxa052")))

(define-public crate-copy-cards-1.1.3 (c (n "copy-cards") (v "1.1.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "18kklb4j8c4ygisbr9ydbavsfz2b0ijbl9pmnlxdjj28w9f1s86i")))

(define-public crate-copy-cards-1.1.4 (c (n "copy-cards") (v "1.1.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "0y5158piyxwphia8zj8mrnsrvglj8pnmbh99qzf777cmm1p5dzjm")))

(define-public crate-copy-cards-1.1.5 (c (n "copy-cards") (v "1.1.5") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "03amirywa1jnnggzavxlgq20dlfml0qjypnp7x3yp27lh7lla5pm")))

(define-public crate-copy-cards-1.1.6 (c (n "copy-cards") (v "1.1.6") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "small-card-deck") (r "^0.1.1") (d #t) (k 0)))) (h "1dr7m138lff8jrx3p76jcjcwlhh3awajkicb8mlhd59sp9lbc5c4")))

