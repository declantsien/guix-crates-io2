(define-module (crates-io co py copy_dir) #:use-module (crates-io))

(define-public crate-copy_dir-0.1.0 (c (n "copy_dir") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1xwar1lmarr69gvrg94yng80vnp6dzkdxfd95y8r16vl9ajzgzld")))

(define-public crate-copy_dir-0.1.1 (c (n "copy_dir") (v "0.1.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "07d4c75ga8j0sdxb2sgzxfbxvkv70gj4lgkdkayqb77825qzns0p")))

(define-public crate-copy_dir-0.1.2 (c (n "copy_dir") (v "0.1.2") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1c7qx86z3v0vn45xjfq8df6w93zrik2ajnigpm1lhr1l2q1q2hkf")))

(define-public crate-copy_dir-0.1.3 (c (n "copy_dir") (v "0.1.3") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "18rckga6rf3nzlw8avyi6chdlid0kp7lhfjyy0pnw27g738isgal")))

