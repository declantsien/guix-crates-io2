(define-module (crates-io co py copy_from_str) #:use-module (crates-io))

(define-public crate-copy_from_str-0.1.0 (c (n "copy_from_str") (v "0.1.0") (d (list (d (n "extension-trait") (r "^0.1.2") (d #t) (k 0)))) (h "10fklf88l90f1hzy28kc13z60r5w56hjp2nbj9q6xxqzda72v9ci")))

(define-public crate-copy_from_str-0.1.1 (c (n "copy_from_str") (v "0.1.1") (d (list (d (n "extension-trait") (r "^0.1.2") (d #t) (k 0)))) (h "0hmag1n8j7073sy7shi0fnp3dnwm298y11lak1i03gmn9yrazz8l")))

(define-public crate-copy_from_str-0.1.2 (c (n "copy_from_str") (v "0.1.2") (d (list (d (n "extension-trait") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "1m6yw3vy0i3r3i7zj3l0q71fz3qfpj9a4a7vj9rdfscbgn7kxzsk")))

(define-public crate-copy_from_str-1.0.0 (c (n "copy_from_str") (v "1.0.0") (d (list (d (n "extension-trait") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0vz7zgpn6simqmwpnzqwzzilqcas1g994q7sn69wzhnyq3gxzz0n")))

(define-public crate-copy_from_str-1.0.1 (c (n "copy_from_str") (v "1.0.1") (d (list (d (n "extension-trait") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "0c54nnrfg8a5r4lv9hirw2vsvbc4cb5l996n91li1w7z2n0czdkx")))

(define-public crate-copy_from_str-1.0.2 (c (n "copy_from_str") (v "1.0.2") (d (list (d (n "extension-trait") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "1d4c891wn8khgkmc9dnx8g3dvknaw2nlsj3rmyhimjk8z7317cp8")))

(define-public crate-copy_from_str-1.0.3 (c (n "copy_from_str") (v "1.0.3") (d (list (d (n "extension-trait") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "0z2n7cskb86vcpjcikn2ymhqh435hsmpa940h9qq2zrix6zsdgqx")))

(define-public crate-copy_from_str-1.0.4 (c (n "copy_from_str") (v "1.0.4") (d (list (d (n "extension-trait") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "1mj3mkv0ln4jicwghad4d9z74s32mhrijgdkz3im51h6n79alrj8")))

(define-public crate-copy_from_str-1.0.5 (c (n "copy_from_str") (v "1.0.5") (d (list (d (n "extension-trait") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1z7q17nmg6151y40rl2aq9k3kmz255zc6k7f2bk47rf20v428bpg")))

(define-public crate-copy_from_str-1.0.6 (c (n "copy_from_str") (v "1.0.6") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0h7g0vx84p7l1i7cf8qh2hzq54y6vs1sg74nhmr25dz9d1ly19wa")))

