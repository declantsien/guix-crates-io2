(define-module (crates-io co py copy_arrayvec) #:use-module (crates-io))

(define-public crate-copy_arrayvec-0.1.0 (c (n "copy_arrayvec") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.176") (d #t) (k 2)))) (h "1x3jfb6f46bs846m9saa4x6diil7zfg82mpdlbk7i5i89nzsj814") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

