(define-module (crates-io co z- coz-temporary) #:use-module (crates-io))

(define-public crate-coz-temporary-0.1.3 (c (n "coz-temporary") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)))) (h "0p4hfmsq622dkvf888cjwigvfy2pmwby887q95h4l11xcmra0s0j")))

