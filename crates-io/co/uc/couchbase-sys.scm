(define-module (crates-io co uc couchbase-sys) #:use-module (crates-io))

(define-public crate-couchbase-sys-0.1.0 (c (n "couchbase-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.20") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gwq94r84rqrp4p7y5i8y32misghx581ln5vnly0ww3spnyrvzkw")))

(define-public crate-couchbase-sys-0.1.1 (c (n "couchbase-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.20") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1p4wscdq242w3pzyslqapfdxyxfb9i3m4h5xwwpyqj5cjzijf82h")))

(define-public crate-couchbase-sys-0.2.0 (c (n "couchbase-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.20") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0fpp78qga6m8lq75zy8l5khlakc6xg9nvpqlb8q8lil5jmgrfyfy")))

(define-public crate-couchbase-sys-0.3.0 (c (n "couchbase-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.30") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "03nh6fkv1rw5s5k99706sh9lrrb8x5v27ik2bcvzv2cd6908rwq9") (f (quote (("generate-binding" "bindgen") ("build-lcb" "cmake"))))))

(define-public crate-couchbase-sys-1.0.0-alpha.1 (c (n "couchbase-sys") (v "1.0.0-alpha.1") (d (list (d (n "bindgen") (r "^0.48") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0m97gl3fp6i2fx3wggvqj8psmb93s34yxzw1i0lm3ja3hcj6qxlw") (l "libcouchbase")))

(define-public crate-couchbase-sys-1.0.0-alpha.2 (c (n "couchbase-sys") (v "1.0.0-alpha.2") (d (list (d (n "bindgen") (r "^0.48") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "17f1qwd8wzpn7gd6s1rszmjv2amq5axi38y75jg2g2550bm9h3gf") (l "libcouchbase")))

(define-public crate-couchbase-sys-1.0.0-alpha.3 (c (n "couchbase-sys") (v "1.0.0-alpha.3") (d (list (d (n "bindgen") (r "^0.52") (f (quote ("logging" "runtime" "which-rustfmt"))) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1a5a1grqbjbwjgfkmp80vm7qziwlwh5gwxcni1rn73igwlkx6dqn") (l "libcouchbase")))

(define-public crate-couchbase-sys-1.0.0-alpha.4 (c (n "couchbase-sys") (v "1.0.0-alpha.4") (d (list (d (n "bindgen") (r "^0.54") (f (quote ("logging" "runtime" "which-rustfmt"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9.58") (d #t) (k 0)))) (h "1yk0yragdhjb6bb2pyfndlyabg28ghbn58dy429fcw3vl55jzpxw") (f (quote (("volatile") ("link-static") ("default")))) (l "libcouchbase")))

