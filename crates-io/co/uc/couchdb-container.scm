(define-module (crates-io co uc couchdb-container) #:use-module (crates-io))

(define-public crate-couchdb-container-0.1.0 (c (n "couchdb-container") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.14") (f (quote ("compat"))) (d #t) (k 0)) (d (n "harbourmaster") (r "^0.1.0") (d #t) (k 0)) (d (n "shiplift") (r "^0.5.0") (d #t) (k 0)))) (h "0h0mdynkl5grn62w6p3k8s5h2ywmzmzyqyw9jvdhy4vwd3n05z9v")))

(define-public crate-couchdb-container-0.1.1 (c (n "couchdb-container") (v "0.1.1") (d (list (d (n "harbourmaster") (r "^0.2.0") (d #t) (k 0)) (d (n "shiplift") (r "^0.5.0") (d #t) (k 0)))) (h "1wd4mhp0jkwlqn2yf97avj2mmdxvgsilf3zcsbs1nbrjj4dbhvna")))

(define-public crate-couchdb-container-0.2.0 (c (n "couchdb-container") (v "0.2.0") (d (list (d (n "harbourmaster") (r "^0.3.0") (d #t) (k 0)) (d (n "shiplift") (r "^0.5.0") (d #t) (k 0)))) (h "1dgh2rzd8amz4cif4ysjk7r0s3jzvr1sr84bxz61d1afg92rhc4b")))

