(define-module (crates-io co uc couch_rs_test) #:use-module (crates-io))

(define-public crate-couch_rs_test-0.2.0 (c (n "couch_rs_test") (v "0.2.0") (d (list (d (n "couch_rs") (r "^0.9") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)))) (h "19d9r28841vjk7psrq6jn8rlg6hf2kkp25pibrd6bac7c3ykmk7b")))

(define-public crate-couch_rs_test-0.2.1 (c (n "couch_rs_test") (v "0.2.1") (d (list (d (n "couch_rs") (r "^0.9.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (d #t) (k 0)))) (h "0xsk013pw8ppid5xlkpdbkgcg7nsny2waah39i2g1js5rx09plbz")))

