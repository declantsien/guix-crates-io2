(define-module (crates-io co uc couch_rs_derive) #:use-module (crates-io))

(define-public crate-couch_rs_derive-0.8.19 (c (n "couch_rs_derive") (v "0.8.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "0nsfgp09gn8dmip25bcigahzx78zqkkvlnkwswrgsz6ma13hqjsk")))

(define-public crate-couch_rs_derive-0.8.20 (c (n "couch_rs_derive") (v "0.8.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "02x9660jf1an57wv0cx8q4kl5fav0207krnfqw0z4b4wrr7rkrkg")))

(define-public crate-couch_rs_derive-0.8.21 (c (n "couch_rs_derive") (v "0.8.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "06licx0rcidjwbc22vcqjnyj6pxy494aiagm5v5bl7rqi164svzm")))

(define-public crate-couch_rs_derive-0.8.22 (c (n "couch_rs_derive") (v "0.8.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "0qyjq72mm6kch9zi02yishhra9qanbq0n4bmss4jw2d1nppd3vb8")))

(define-public crate-couch_rs_derive-0.8.23 (c (n "couch_rs_derive") (v "0.8.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "0jzr24cyzmyxzvbm0daz5033xgipidilzy3yhpx48y9sq1yhb83j")))

(define-public crate-couch_rs_derive-0.8.24 (c (n "couch_rs_derive") (v "0.8.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "13z1q6mbs42fvgbdmvmx68l1161j0xh2ca3k460cyapnf33rvhcy")))

(define-public crate-couch_rs_derive-0.8.25 (c (n "couch_rs_derive") (v "0.8.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "1v7agysk82waxam13ihn2p43vxv54l773ys09vzzdaxyc08lvqzl")))

(define-public crate-couch_rs_derive-0.8.26 (c (n "couch_rs_derive") (v "0.8.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "038y384fya0gzkaynyikaws0iil3z9wb2zhaslxkcn166k39nyc7")))

(define-public crate-couch_rs_derive-0.8.27 (c (n "couch_rs_derive") (v "0.8.27") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "1zrh4dd2izrli7vvlncxigbhvgq44aq1sjzx4placclqxj4qhmmw")))

(define-public crate-couch_rs_derive-0.8.28 (c (n "couch_rs_derive") (v "0.8.28") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "0a6gc6ihlmmjwy1nqgh7x08h41w5a4pa43vvsivdrsa9svp3zypy")))

(define-public crate-couch_rs_derive-0.8.29 (c (n "couch_rs_derive") (v "0.8.29") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "1lmzvd8zr156ihb182wzlcp9picix4hw9l0l220ljlp6qamkf08a")))

(define-public crate-couch_rs_derive-0.8.30 (c (n "couch_rs_derive") (v "0.8.30") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "10d3mixhrhq1jhxrcpvsifawzlsf11gj5ml8dlhnvylz7vvpdcdp")))

(define-public crate-couch_rs_derive-0.8.31 (c (n "couch_rs_derive") (v "0.8.31") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "18bix6c762mrq4q4wmi645nga7rsjz4wgkc9hbjxzs5gi8a9ycl0")))

(define-public crate-couch_rs_derive-0.8.32 (c (n "couch_rs_derive") (v "0.8.32") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "0rhxs2kfwhzvpaavjhpbqa66kh9qwa9d77mb27mdl4w8nm829x65")))

(define-public crate-couch_rs_derive-0.8.33 (c (n "couch_rs_derive") (v "0.8.33") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("visit"))) (d #t) (k 0)))) (h "01ccqibvpzmp8bv3rh3760b9rhggmrbqqs5zn8hpsqqf210w5kgk")))

(define-public crate-couch_rs_derive-0.8.34 (c (n "couch_rs_derive") (v "0.8.34") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1x4my8699b90bdzafdbh442z4d04xayi1vz3z3sw42jaad8nkjnz")))

(define-public crate-couch_rs_derive-0.8.35 (c (n "couch_rs_derive") (v "0.8.35") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1f7cm4sgaj9pzn5d9jffw2h9578h8hdjr08rccn5nl7anz6s2244")))

(define-public crate-couch_rs_derive-0.8.36 (c (n "couch_rs_derive") (v "0.8.36") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "025c1sgvda6y5aylhh7drrsgxy4l439ivy14qfmgakx7g24m09sm")))

(define-public crate-couch_rs_derive-0.8.37 (c (n "couch_rs_derive") (v "0.8.37") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0nhww0dxbk0ijlpyr7mxdrblj1g1vrw92i5cna5hzc1by2bksc7w")))

(define-public crate-couch_rs_derive-0.8.38 (c (n "couch_rs_derive") (v "0.8.38") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1ghx0z5a9dsf18k5apivyqzxvqcpw2vwzljy13jrwm1250yappbl")))

(define-public crate-couch_rs_derive-0.8.39 (c (n "couch_rs_derive") (v "0.8.39") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "07hp1r35lbgyq6lk5sm8lpz7l0kay2mbsjfk2vhka492lqb09ska")))

(define-public crate-couch_rs_derive-0.9.0 (c (n "couch_rs_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "06dd3cg7z39a6h57px899j2rwv1zz0df5xa6p657d2ia7qr5kj8b")))

(define-public crate-couch_rs_derive-0.9.1 (c (n "couch_rs_derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1b4iyj9z2v9wgqmipzdakwcrj25m3zq8lkg73zpxs3hwqvhjmmna")))

(define-public crate-couch_rs_derive-0.9.2 (c (n "couch_rs_derive") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "11aq479dy4rpg863hx2f5gq42530fml7f17i40fxpnr2ivs4kr52")))

(define-public crate-couch_rs_derive-0.9.3 (c (n "couch_rs_derive") (v "0.9.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1xnjz5vda2lbfxpn7rvh4mbnwjqpdsq3cjrx86pix2k21lr9s9jm")))

(define-public crate-couch_rs_derive-0.9.4 (c (n "couch_rs_derive") (v "0.9.4") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "02hjjzpj9b9ygx2mf8j185h9avpns1xms62grywagnjygsvn688a")))

(define-public crate-couch_rs_derive-0.10.0 (c (n "couch_rs_derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "13nxjxy1mqql68zzadvhva9cq1ryrrzijgv9zx08qkf2v07fhx99")))

(define-public crate-couch_rs_derive-0.10.1 (c (n "couch_rs_derive") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1isq58x8g5zaq8s1rg0vz6wzx9wiyjnr1ry9czrxzdhbsb3pfvvs")))

