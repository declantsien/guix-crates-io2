(define-module (crates-io co ut cout2json) #:use-module (crates-io))

(define-public crate-cout2json-0.1.0 (c (n "cout2json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13215df03cpfgl7i8dfdc5gfypw9i7kyqb0nvz2xvl0yfj0mm3c3")))

