(define-module (crates-io co ut coutils) #:use-module (crates-io))

(define-public crate-coutils-1.0.0 (c (n "coutils") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rgw1abwqj4zhkmpqx4xx51a1s2kr313zj6gkvfxxr98haam3xsf")))

(define-public crate-coutils-1.1.0 (c (n "coutils") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hlvjfjahpns3a8wja4ddyk81pjxwxiak22a24amngd61kqdwx1l")))

(define-public crate-coutils-1.2.0 (c (n "coutils") (v "1.2.0") (d (list (d (n "file-serve") (r "^0.2.4") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0gx8n9ajfml5agnxmy6ki81w9r82p2izlnzs9w4w76nqcmw64x92")))

(define-public crate-coutils-1.3.0 (c (n "coutils") (v "1.3.0") (d (list (d (n "file-serve") (r "^0.2.4") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "127b7bn05nnl60svb1kb980lqqchnzqxvhfcgrifprns8di5j3ad")))

(define-public crate-coutils-1.4.0 (c (n "coutils") (v "1.4.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "file-serve") (r "^0.2.4") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nbwhixcx5jgsjfdjj0l5lmcbh7f3f2ic8snzy6bqi0y7a3rih8n")))

(define-public crate-coutils-1.5.0 (c (n "coutils") (v "1.5.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zn38rq3vn3fgyy80rghiq5142df2v90jmwqbkfcg9g4j9wyhn3f")))

(define-public crate-coutils-1.6.0 (c (n "coutils") (v "1.6.0") (d (list (d (n "chrono") (r "^0.4.37") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "045p495d9d0zrg7kkjghz9i67av6g2p2z1yq1xg2lahl49y7xsa5") (f (quote (("time" "chrono") ("networking" "git2" "filesystem" "fs_extra") ("filesystem" "fs_extra")))) (y #t)))

(define-public crate-coutils-2.0.0 (c (n "coutils") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.37") (o #t) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xxhj9qw8bqbs596cp3y8x4wk6ankalggjb3f63s9c2qv488749s") (f (quote (("time" "chrono") ("networking" "filesystem" "git2") ("filesystem" "fs_extra"))))))

