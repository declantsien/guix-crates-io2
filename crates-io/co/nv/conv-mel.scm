(define-module (crates-io co nv conv-mel) #:use-module (crates-io))

(define-public crate-conv-mel-0.6.0 (c (n "conv-mel") (v "0.6.0") (d (list (d (n "melodium-core") (r "^0.6.0") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.6.0") (d #t) (k 0)))) (h "0nq7327avd8r8cs0zv8mq4s6dhpmyk98m0wxp5lk80x5afxf03w3") (r "1.60")))

(define-public crate-conv-mel-0.7.0-rc1 (c (n "conv-mel") (v "0.7.0-rc1") (d (list (d (n "melodium-core") (r "=0.7.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0-rc1") (d #t) (k 0)))) (h "17xsh4pb3iv619avksl2an2685yg6iqnj0vq5zcdyanmh7idbr87") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-conv-mel-0.7.0 (c (n "conv-mel") (v "0.7.0") (d (list (d (n "melodium-core") (r "=0.7.0") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0") (d #t) (k 0)))) (h "01h0fhzclwx6ar8mn2953a525kp6mhjvd8xbkr9kqhydi029zgi1") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-conv-mel-0.7.1 (c (n "conv-mel") (v "0.7.1") (d (list (d (n "melodium-core") (r "^0.7.1") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.7.1") (d #t) (k 0)))) (h "09k11cnki1mrrz4q4g6r5fhi8nllax72yxz8ywvnqmq1dcxmwqw3") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

