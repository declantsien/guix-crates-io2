(define-module (crates-io co nv convert-map) #:use-module (crates-io))

(define-public crate-convert-map-0.1.0 (c (n "convert-map") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0x7jjxcxj8q9336bgmh4ck1wks1942gf6yynahzab0nkvj4cvsx0")))

