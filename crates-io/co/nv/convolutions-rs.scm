(define-module (crates-io co nv convolutions-rs) #:use-module (crates-io))

(define-public crate-convolutions-rs-0.1.0 (c (n "convolutions-rs") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)))) (h "0mg46fwm3m7skwx1lm9ya8vr53qs3qkwrgc3kgj8zra61xhmfvbd")))

(define-public crate-convolutions-rs-0.2.0 (c (n "convolutions-rs") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0ig3rsva2js5xcsm60cf5ng14n4hbd671w53nhri0vj7xi3jfsck")))

(define-public crate-convolutions-rs-0.2.1 (c (n "convolutions-rs") (v "0.2.1") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0mc9ww5hzar1fkifa1w7rx37crpgj284b1lkmcnx541ynds3pgga")))

(define-public crate-convolutions-rs-0.2.2 (c (n "convolutions-rs") (v "0.2.2") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0laq1p174jjnd13hm4bkf9pqdwjzna7x79djm82n5rgdbm6ndsdn")))

(define-public crate-convolutions-rs-0.2.3 (c (n "convolutions-rs") (v "0.2.3") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "159jl4j76mpdmpv67zky8xhhq3y4qpz6slcw2pfmawl3y94blahl")))

(define-public crate-convolutions-rs-0.3.0 (c (n "convolutions-rs") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0hivb8r2cdjsycc13lv6w01hvra8chmprz1npkjaa50hvhzxkh9g")))

(define-public crate-convolutions-rs-0.3.1 (c (n "convolutions-rs") (v "0.3.1") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "13m1zs61mr4wg575azlz8gdi4gihnpi0d9gzvjpd39zkz70yaiix")))

(define-public crate-convolutions-rs-0.3.2 (c (n "convolutions-rs") (v "0.3.2") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "01vvlxy0r0kg4pkbpiqvpglmgw0gsrk75kgagb2wbdsdc11ylsn4")))

(define-public crate-convolutions-rs-0.3.3 (c (n "convolutions-rs") (v "0.3.3") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0l2arqkix589mbwz2qifwryzgs66zrj1ymhhv3y17d8y9i1q5xj8")))

(define-public crate-convolutions-rs-0.3.4 (c (n "convolutions-rs") (v "0.3.4") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0m8jfffkxrclh2rx8mj5yhsakgpa0hg3va566qcjcmqjxpr2lifw")))

