(define-module (crates-io co nv converge) #:use-module (crates-io))

(define-public crate-converge-0.0.1 (c (n "converge") (v "0.0.1") (d (list (d (n "converge_derive") (r "^0.0.1") (d #t) (k 0)))) (h "17lq8wa307pr0mn9bffgvg3vb5i1597vrj77d7jg802xscaahqq0")))

(define-public crate-converge-0.0.2 (c (n "converge") (v "0.0.2") (d (list (d (n "converge_derive") (r "^0.0.1") (d #t) (k 0)))) (h "0pg77lwn5sghvfgf6bz4q04skb1yd6146c9qbir80lxjkxn14m68")))

(define-public crate-converge-0.0.3 (c (n "converge") (v "0.0.3") (d (list (d (n "converge_derive") (r "^0.0.3") (d #t) (k 0)))) (h "0bx6rdd7l2rrji9lxc51476sw9m8cn1hdgvcrxhg3hk5mxq5pgsx")))

(define-public crate-converge-0.0.4 (c (n "converge") (v "0.0.4") (d (list (d (n "converge_derive") (r "^0.0.4") (d #t) (k 0)))) (h "1i6gjmblc8xx6y0cpn6aqjjdkrvcm2asa2nkryanbd6rrvc539m4")))

(define-public crate-converge-0.0.5 (c (n "converge") (v "0.0.5") (d (list (d (n "converge_derive") (r "^0.0.5") (d #t) (k 0)))) (h "1sln966x5pavzrvk3ikvkyz2x7vlxx15ivkr8r5lbd2q1x79mnn2")))

