(define-module (crates-io co nv conventional-commits-parser) #:use-module (crates-io))

(define-public crate-conventional-commits-parser-0.0.0 (c (n "conventional-commits-parser") (v "0.0.0") (h "15ck288r2fzvgw9456fs9grpply4yn86f7bxv45zpwv7kf7v5a8x")))

(define-public crate-conventional-commits-parser-0.1.0 (c (n "conventional-commits-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 2)) (d (n "conventional-commits-types") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "nom-unicode") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "ron") (r "^0.6.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1pr0n9cfxv5s2lwgr8bknb38yf7l4mzr47jag4f84c2v5m5197hb") (f (quote (("serde" "conventional-commits-types/serde"))))))

(define-public crate-conventional-commits-parser-0.1.1 (c (n "conventional-commits-parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 2)) (d (n "conventional-commits-types") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "nom-unicode") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "ron") (r "^0.6.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 2)))) (h "1dxcnik6mz1d5db1n0if2ga9d07czyq64yi3j98cnzr1mfq5gpwj") (f (quote (("serde" "conventional-commits-types/serde"))))))

