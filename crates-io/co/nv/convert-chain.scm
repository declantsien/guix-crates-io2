(define-module (crates-io co nv convert-chain) #:use-module (crates-io))

(define-public crate-convert-chain-0.1.0 (c (n "convert-chain") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "185zq256akxalchv85zfff2x1hcwvhdkg3n48hcm1b038f68mnlv")))

(define-public crate-convert-chain-0.1.1 (c (n "convert-chain") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "1jrrc34ccssax6h1xkzz8paarmp3fkmv37c770qzyamh972dbyvr") (y #t)))

(define-public crate-convert-chain-0.1.2 (c (n "convert-chain") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "03cj3k66m9pma0hryjni1xvhgilvrhxih0g9bkd8vf85gvwzcm78") (y #t)))

(define-public crate-convert-chain-0.1.3 (c (n "convert-chain") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0sy1fv2yl7k9r3bn8y9gwbp6j1npipzl7l7y5zqrqisiiwj179yq")))

