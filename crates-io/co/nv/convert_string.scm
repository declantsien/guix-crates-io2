(define-module (crates-io co nv convert_string) #:use-module (crates-io))

(define-public crate-convert_string-0.1.0 (c (n "convert_string") (v "0.1.0") (h "13314c9bj63wpb023f2ac8153vv51jwz2kh1wggx8rpz4fw1lsw2")))

(define-public crate-convert_string-0.1.1 (c (n "convert_string") (v "0.1.1") (h "0id9siz21n81jip8ibbpmxw6wd1zqgmic5rznp4ijdzqj0pbk4p0")))

(define-public crate-convert_string-0.1.2 (c (n "convert_string") (v "0.1.2") (h "0vp1y8f3j80imrqfbkdkqaz01ljjyn640cz878a03jbvm14x1kz7")))

(define-public crate-convert_string-0.1.3 (c (n "convert_string") (v "0.1.3") (h "1nggbq6dqrn5nc802g8p8mkhgyaja2c5f5ns2i7swqq5qsh0zbjy")))

