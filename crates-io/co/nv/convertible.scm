(define-module (crates-io co nv convertible) #:use-module (crates-io))

(define-public crate-convertible-0.0.0 (c (n "convertible") (v "0.0.0") (h "0sf70a681bbn03j9lvhvs0mbz7607k8phpy1109bqv284rx5jpfy")))

(define-public crate-convertible-0.0.1 (c (n "convertible") (v "0.0.1") (h "0xhrz50aj9c0mcw2j296708296s22i6hf70y6fr3fb021lrr3q8x")))

