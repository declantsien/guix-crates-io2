(define-module (crates-io co nv convert-params) #:use-module (crates-io))

(define-public crate-convert-params-0.1.0 (c (n "convert-params") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "106x30lnczms9lzymlj392985lw92xwpwvyj1h095gp5lm7snjzg")))

