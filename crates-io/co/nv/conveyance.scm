(define-module (crates-io co nv conveyance) #:use-module (crates-io))

(define-public crate-conveyance-0.1.0 (c (n "conveyance") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1syz3lm8kmicbf07z726arr3vvrawpfj78i0idfrh783vqhq53d5")))

(define-public crate-conveyance-0.1.1 (c (n "conveyance") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "0bif1q9w7gpj9v362x5wmjrbd28mkwfwy96ws7slywcbjsyynbjd")))

(define-public crate-conveyance-0.1.2 (c (n "conveyance") (v "0.1.2") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "16yn516r2agbppjsialvf8zcb36gca1hh2kyxva9i796x0if329x")))

(define-public crate-conveyance-0.1.3 (c (n "conveyance") (v "0.1.3") (d (list (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1gknk2n560ik496dj8bnsa5iach51paz6bdcjhxi57iyvx3rnbj6")))

