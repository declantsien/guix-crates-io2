(define-module (crates-io co nv convolician) #:use-module (crates-io))

(define-public crate-convolician-0.1.0 (c (n "convolician") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1zm7qaw277gc3363ivfq8mdxihjy4yb6242zyk1kf6v86yhy06g4")))

