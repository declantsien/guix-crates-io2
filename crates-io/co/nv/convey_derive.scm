(define-module (crates-io co nv convey_derive) #:use-module (crates-io))

(define-public crate-convey_derive-0.1.0 (c (n "convey_derive") (v "0.1.0") (d (list (d (n "convey") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0q6qqvsxxd0p6g363mbz73n8jdwvqzvzxlr5z5jfmvn0mipnm4lg")))

(define-public crate-convey_derive-0.2.0 (c (n "convey_derive") (v "0.2.0") (d (list (d (n "convey") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0r2zksfhywm7z344sjy9qmg9la3463rm2nr088axms1f4klf24rv")))

