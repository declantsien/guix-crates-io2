(define-module (crates-io co nv conventus) #:use-module (crates-io))

(define-public crate-conventus-0.1.0 (c (n "conventus") (v "0.1.0") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)))) (h "1lwv3qbf715j8qpir38hnx2kdmk4dqcf7vwhwyvwq8qa0phm5hsr")))

(define-public crate-conventus-0.2.0 (c (n "conventus") (v "0.2.0") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)))) (h "1kk6k6l2kscfvdy7d3p9z1gpa000672lds8qsjcv1s2xfanrl50h")))

(define-public crate-conventus-0.3.0 (c (n "conventus") (v "0.3.0") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)))) (h "1vdpzhrmcbqw4bwq3d7y5ynas98z76mzmp659xv1zmdr8c7vagvp")))

