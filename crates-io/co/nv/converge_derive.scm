(define-module (crates-io co nv converge_derive) #:use-module (crates-io))

(define-public crate-converge_derive-0.0.1 (c (n "converge_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kysawgmnms89gq10wb4y6bq765d7xpqs2kn6gy09l8xkrlf1kmw")))

(define-public crate-converge_derive-0.0.3 (c (n "converge_derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00ihkpvz2mv7xdhw8s619arrhivm01j9hcmdvkaq3i837b8ryi8z")))

(define-public crate-converge_derive-0.0.4 (c (n "converge_derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k91g0mvb3rzak3hsaw5ysmfwph3dn3n173gd7x1gf73a07j4bl3")))

(define-public crate-converge_derive-0.0.5 (c (n "converge_derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16h5ff3dlyyfg0nr8m72zps6yiy9qwsir8p3qyk2sh4p8l3nmmzc")))

