(define-module (crates-io co nv convert-nonogram) #:use-module (crates-io))

(define-public crate-convert-nonogram-0.1.0 (c (n "convert-nonogram") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.11") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20.0") (d #t) (k 0)))) (h "0bn1rmljdgxq8fjfz2ichh4jnfx5h8hy3hd1idkjk26sa8ix2n4s")))

(define-public crate-convert-nonogram-0.1.1 (c (n "convert-nonogram") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.11") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20.0") (d #t) (k 0)))) (h "1zs7d1z67hszsq3qnvcvck2vwyf56v6zz5bb8aa2yl3lx99pp5vx")))

(define-public crate-convert-nonogram-0.1.2 (c (n "convert-nonogram") (v "0.1.2") (d (list (d (n "clap") (r ">=2.33.3, <3.0.0") (d #t) (k 0)) (d (n "image") (r ">=0.23.11, <0.24.0") (d #t) (k 0)) (d (n "indoc") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quick-xml") (r ">=0.20.0, <0.21.0") (d #t) (k 0)))) (h "1y2y0s11xzmrndvwl23nw4bfpk60izqxabjh2f4cs9l409rdbfqb")))

