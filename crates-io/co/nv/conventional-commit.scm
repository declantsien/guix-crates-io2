(define-module (crates-io co nv conventional-commit) #:use-module (crates-io))

(define-public crate-conventional-commit-0.1.0 (c (n "conventional-commit") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 0)))) (h "1hp28gs1pgmw9v2ka7yl7h5kk9h8pljalbxyirbqllf5nwdkmc3n")))

