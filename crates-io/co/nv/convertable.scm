(define-module (crates-io co nv convertable) #:use-module (crates-io))

(define-public crate-convertable-0.1.0 (c (n "convertable") (v "0.1.0") (h "0wrx4b1xdx18b2h7g7qzcsfwxj8lzmyikrjnd4vpwm4ska2ihwx2")))

(define-public crate-convertable-0.1.1 (c (n "convertable") (v "0.1.1") (h "0y2x0qmy7sm4q542vx8h28zs865pw0d0gjn5m666z4xkhmxcr88b")))

