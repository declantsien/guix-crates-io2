(define-module (crates-io co nv convec) #:use-module (crates-io))

(define-public crate-convec-1.0.0 (c (n "convec") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.4.8") (d #t) (k 0)))) (h "10iakcjcazvsn2hrpl56i6aqwixfjkwi4lgfk0nxh0iz20cz0a3v")))

(define-public crate-convec-2.0.0 (c (n "convec") (v "2.0.0") (d (list (d (n "parking_lot") (r "^0.4.8") (d #t) (k 0)))) (h "1ban33yvc9q9lh3b6pxqdaj8321qzhhzcpc1i2gisxcga0clh7jp")))

(define-public crate-convec-2.0.1 (c (n "convec") (v "2.0.1") (d (list (d (n "parking_lot") (r "^0.4.8") (d #t) (k 0)))) (h "105g4mpk3xmfpjigkd8g8lbihyaal6hlm0ilcszgz5mcalv80pz8")))

