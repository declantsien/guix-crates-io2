(define-module (crates-io co nv conveyor) #:use-module (crates-io))

(define-public crate-conveyor-0.1.0 (c (n "conveyor") (v "0.1.0") (d (list (d (n "prettytable-rs") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)))) (h "0p0ih5la2cx0vs26ga7ijiz0m0p8vm1l9yxqarfcv0p0qcxkz3rf")))

(define-public crate-conveyor-0.1.1 (c (n "conveyor") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)))) (h "08f0dl9jl9j1fczpnrxply3y733h07qn1kh5nscnqgkj1zvgdzzv")))

