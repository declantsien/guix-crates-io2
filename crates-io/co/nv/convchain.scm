(define-module (crates-io co nv convchain) #:use-module (crates-io))

(define-public crate-convchain-0.1.0 (c (n "convchain") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)))) (h "1llw2slbyl3lahksifb6vbq0rimqxhfqjxi2x0pnqpswyw2ra85v")))

(define-public crate-convchain-0.2.0 (c (n "convchain") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)))) (h "0jp2r8n37z5hmvmf9nwb9xlqa9ffpfsrb54hxs8bval8cs6fj24h")))

(define-public crate-convchain-0.2.1 (c (n "convchain") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)))) (h "1b4kah3g8i3gb9ki1qbdmsy7bzhmc9nwdz23ax9vbxzcr2hln011")))

