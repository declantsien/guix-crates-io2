(define-module (crates-io co nv conventional_commit_parser) #:use-module (crates-io))

(define-public crate-conventional_commit_parser-0.1.0 (c (n "conventional_commit_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0rr1xbwiy3bsgsdmjfx29pr8ky55j22nzq63d04vw5pifncjy1xb")))

(define-public crate-conventional_commit_parser-0.2.0 (c (n "conventional_commit_parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1wmbna7nzsvcjjjnks8b0hsyaj9wqzgn8riq640c9rj1nlsd5bks")))

(define-public crate-conventional_commit_parser-0.3.0 (c (n "conventional_commit_parser") (v "0.3.0") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1z3c4bs0bq7b4z0lb375b584n7rnscyr6jf4i59a60ai4qb93inl")))

(define-public crate-conventional_commit_parser-0.4.0 (c (n "conventional_commit_parser") (v "0.4.0") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1a71g1m8pn1c56x827m1rcs335vv9cz6d45hn5lj4pyp5jz0sy76")))

(define-public crate-conventional_commit_parser-0.4.1 (c (n "conventional_commit_parser") (v "0.4.1") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1dc34a534dcyqh3zp0xgfxd4kr7bc0k9zh6dpwxdsp6f86cg65pf")))

(define-public crate-conventional_commit_parser-0.4.2 (c (n "conventional_commit_parser") (v "0.4.2") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1p8xsg7cg15wv0d0sp02qrqccmmzhj72hb4c6m7573fkfl0jyia8")))

(define-public crate-conventional_commit_parser-0.5.2 (c (n "conventional_commit_parser") (v "0.5.2") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "08x7xgbcg61h8bx07pyrim5bfl972qiwfia3kb2nh01pf7snwz4s")))

(define-public crate-conventional_commit_parser-0.6.0 (c (n "conventional_commit_parser") (v "0.6.0") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "17zz8jc137868zm1s7b17bsaqq0sd58kaziac3jyq3485ka3rrar") (y #t)))

(define-public crate-conventional_commit_parser-0.6.1 (c (n "conventional_commit_parser") (v "0.6.1") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1szrbsqbz09741wgk5xndiwaqahdp1qnvj36jjyrzq85a3q9xjvx") (y #t)))

(define-public crate-conventional_commit_parser-0.7.0 (c (n "conventional_commit_parser") (v "0.7.0") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "13h2f9cv9lmm2mlzmx3hdi2jyws64zxkcr1kl5x9fyaacdfdk71v") (y #t)))

(define-public crate-conventional_commit_parser-0.7.1 (c (n "conventional_commit_parser") (v "0.7.1") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1kkhxhicqr89zk1i0062ffd96vy4m4lm20na1cnc6mjjzraj7r83") (y #t)))

(define-public crate-conventional_commit_parser-0.8.0 (c (n "conventional_commit_parser") (v "0.8.0") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "03dzwbjqfwaw43xafbf8vh5sjagvcsvhh7rknc89axh8zkw0scsw")))

(define-public crate-conventional_commit_parser-0.9.0 (c (n "conventional_commit_parser") (v "0.9.0") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "07dq0674b9xi5sqsgnw6ghsj84d9kiz071q2s74h6612yfzciayb")))

(define-public crate-conventional_commit_parser-0.9.1 (c (n "conventional_commit_parser") (v "0.9.1") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "speculoos") (r "^0.8.0") (d #t) (k 2)))) (h "1j0124jir4h9yym7c3y6vxa0ahci0zlr7pyq4jkr4bvydpqgz6fb")))

(define-public crate-conventional_commit_parser-0.9.2 (c (n "conventional_commit_parser") (v "0.9.2") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "speculoos") (r "^0.8.0") (d #t) (k 2)))) (h "1m5xz2hn2r2bh60ngbja25hp3529n1bfx1xii139frlhsldwnpq5")))

(define-public crate-conventional_commit_parser-0.9.3 (c (n "conventional_commit_parser") (v "0.9.3") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "speculoos") (r "^0.8.0") (d #t) (k 2)))) (h "0sj1p4mzppbl68qb66q43d0pmxva09nbxmx4cdblp28x9jsi00a8")))

(define-public crate-conventional_commit_parser-0.9.4 (c (n "conventional_commit_parser") (v "0.9.4") (d (list (d (n "assert-panic") (r "^1.0.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "speculoos") (r "^0.8.0") (d #t) (k 2)))) (h "1jzpsv3kwlmlxnpfv29179khq05sigm7651xqg4yxvjy3ng0yrjq")))

