(define-module (crates-io co nv convevo) #:use-module (crates-io))

(define-public crate-convevo-0.5.4 (c (n "convevo") (v "0.5.4") (d (list (d (n "cursive") (r "^0.16.3") (f (quote ("crossterm-backend"))) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_merge") (r "^0.1.3") (d #t) (k 0)) (d (n "shared_child") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0yxa6ra6ibhpc1s34z98v1yr2nz0baynxy9vbvwyhbgszm5hjvhs") (y #t)))

(define-public crate-convevo-0.6.2 (c (n "convevo") (v "0.6.2") (d (list (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_merge") (r "^0.1.3") (d #t) (k 0)) (d (n "shared_child") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0m7w8hwf9dpcb5qxnkhz8ygn9y7ybwh1n23n1406cjmcsd5nnf2b") (y #t)))

(define-public crate-convevo-0.6.3 (c (n "convevo") (v "0.6.3") (h "0d8s2rscr024485pd7yrail32k2mv9c4kqb85d92y6jkk4xxnkfy") (y #t)))

