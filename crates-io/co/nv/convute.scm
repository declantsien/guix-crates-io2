(define-module (crates-io co nv convute) #:use-module (crates-io))

(define-public crate-convute-0.1.0 (c (n "convute") (v "0.1.0") (h "0jbwa6a434xh1x2v8d1jr6c426zj963wbihbmsmgpgx1s3n2z7jg")))

(define-public crate-convute-0.1.1 (c (n "convute") (v "0.1.1") (h "0icadcqw9b7bq81p682z0p82llz5kisqghyvd67q5vmljzxxx5da")))

(define-public crate-convute-0.2.0 (c (n "convute") (v "0.2.0") (h "008fznvrbwsb35lb2r8sg90j8rb72xmrj2kwfr1x7qginn5q0vv2")))

