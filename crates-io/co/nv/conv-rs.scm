(define-module (crates-io co nv conv-rs) #:use-module (crates-io))

(define-public crate-conv-rs-0.1.0 (c (n "conv-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "01b00r4xxb21c4mj13z0nav00pjvb69smmqnlp5bw9qh78dky68c")))

(define-public crate-conv-rs-0.1.1 (c (n "conv-rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1p8gwz3kqls9annbfbmwl9248gmpazyjmjxxx1ky134dl2wb4wic")))

