(define-module (crates-io co nv convolve2d) #:use-module (crates-io))

(define-public crate-convolve2d-0.1.0 (c (n "convolve2d") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 2)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)))) (h "0959xsyp39r38lqhgkbkd6lhz5q3xg3v6nwjzi6i4nmj0w5j56sn") (f (quote (("std") ("full" "std" "rayon" "image") ("default" "std" "rayon"))))))

