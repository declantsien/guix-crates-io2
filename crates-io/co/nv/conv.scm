(define-module (crates-io co nv conv) #:use-module (crates-io))

(define-public crate-conv-0.1.0 (c (n "conv") (v "0.1.0") (d (list (d (n "custom_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.21") (d #t) (k 2)))) (h "1lq9m0l2qnz55fnca2ip443s9fh0xf4cncm4266fdia9w6kvv7p2")))

(define-public crate-conv-0.2.0 (c (n "conv") (v "0.2.0") (d (list (d (n "custom_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.21") (d #t) (k 2)))) (h "18vvs1w75xzjizlp37c9lkxa7prh7dvn3yk1qiwcdvb13qh2786a")))

(define-public crate-conv-0.2.1 (c (n "conv") (v "0.2.1") (d (list (d (n "custom_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.21") (d #t) (k 2)))) (h "05p88l04nlxjj7m6k85rb41bk4gq7s05wx4v7854yyz1c9nv8hrb")))

(define-public crate-conv-0.3.0 (c (n "conv") (v "0.3.0") (d (list (d (n "custom_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.21") (d #t) (k 2)))) (h "1wbn7f3y6va27820fwq1kfav2ahlnh40v6smvrhmqn8f4xdvmf0b")))

(define-public crate-conv-0.3.1 (c (n "conv") (v "0.3.1") (d (list (d (n "custom_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.21") (d #t) (k 2)))) (h "1vq638cm60pcz6rqr1lch4nizr4l5r143ns3n9jby2d6kgmklgmr")))

(define-public crate-conv-0.3.2 (c (n "conv") (v "0.3.2") (d (list (d (n "custom_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.21, < 0.2.25") (d #t) (k 2)))) (h "0d17213i97g1vbcn2nj8ysjsi4lzzk9fmakycbl1i3g2lrcrir59")))

(define-public crate-conv-0.3.3 (c (n "conv") (v "0.3.3") (d (list (d (n "custom_derive") (r "^0.1.5") (k 0)) (d (n "quickcheck") (r "^0.2.21, < 0.2.25") (d #t) (k 2)) (d (n "winapi") (r "< 0.2.6") (d #t) (k 2)))) (h "168j1npqrif1yqxbgbk0pdrx9shzhs5ylc5a4xw49b6hbxi11zvq") (f (quote (("std" "custom_derive/std") ("default" "std"))))))

