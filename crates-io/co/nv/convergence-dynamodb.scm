(define-module (crates-io co nv convergence-dynamodb) #:use-module (crates-io))

(define-public crate-convergence-dynamodb-0.3.1 (c (n "convergence-dynamodb") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "convergence") (r "^0.3.0") (d #t) (k 0)) (d (n "convergence-arrow") (r "^0.3.0") (d #t) (k 0)) (d (n "datafusion") (r "^5.0.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.46.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0.46.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.7") (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "1mqkhdr918ccyp3krpnj3fiamz68av7zbbhi9wkqlsi0nzf4g8hk")))

