(define-module (crates-io co nv convert_degrees) #:use-module (crates-io))

(define-public crate-convert_degrees-0.1.0 (c (n "convert_degrees") (v "0.1.0") (d (list (d (n "bpaf") (r "^0.7") (f (quote ("derive" "bright-color"))) (d #t) (k 0)))) (h "0rw5g8bv9xvf2zhmc6l3ygbnc188nz91idar14cpn6piv4vaqjxk")))

(define-public crate-convert_degrees-0.1.1 (c (n "convert_degrees") (v "0.1.1") (d (list (d (n "bpaf") (r "^0.7") (f (quote ("derive" "bright-color"))) (d #t) (k 0)))) (h "1flczwajmkc5g25x5wy95l9wiyjiqgvn91c3rka764vial63a80z")))

(define-public crate-convert_degrees-0.1.2 (c (n "convert_degrees") (v "0.1.2") (d (list (d (n "bpaf") (r "^0.7") (f (quote ("derive" "bright-color"))) (d #t) (k 0)))) (h "1bzhbji4gg9zhlbfzzlp31iw72x0y9z2cw2n70ra4990vy8cvgzn")))

(define-public crate-convert_degrees-0.1.3 (c (n "convert_degrees") (v "0.1.3") (d (list (d (n "bpaf") (r "^0.7") (f (quote ("derive" "bright-color"))) (d #t) (k 0)))) (h "0xjv2afng3ivx8lnvqh22sipv1xjbhi3k5h25hz6z8aymlgk49l0")))

(define-public crate-convert_degrees-0.2.0 (c (n "convert_degrees") (v "0.2.0") (d (list (d (n "bpaf") (r "^0.7") (f (quote ("derive" "bright-color"))) (o #t) (d #t) (k 0)))) (h "06wvv0irnh1jrhlzq9lc21si90mi4ny7qi3qs38v6m3sc3ylm2q7") (f (quote (("bin" "bpaf"))))))

(define-public crate-convert_degrees-0.2.1 (c (n "convert_degrees") (v "0.2.1") (d (list (d (n "bpaf") (r "^0.7") (f (quote ("derive" "bright-color"))) (o #t) (d #t) (k 0)))) (h "11jhqy73yhrr0yw174nl3zvbiqqc36fq09g8k1skjsx7rxz5jday") (f (quote (("bin" "bpaf"))))))

