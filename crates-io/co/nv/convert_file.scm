(define-module (crates-io co nv convert_file) #:use-module (crates-io))

(define-public crate-convert_file-0.1.0 (c (n "convert_file") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)))) (h "13l1kws3cn7wkvj2q3725s0p4va308jynm8180vgg4zz2ajfj5cr")))

