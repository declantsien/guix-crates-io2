(define-module (crates-io co nv convert-enum) #:use-module (crates-io))

(define-public crate-convert-enum-0.1.0 (c (n "convert-enum") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1rkd4vivpaqnhmm8ms12bav2jkn8big0vz89fda1rr5h98rlrvbr")))

