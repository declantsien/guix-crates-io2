(define-module (crates-io co nv conventional) #:use-module (crates-io))

(define-public crate-conventional-0.1.0 (c (n "conventional") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 0)))) (h "1wdfj4176dgv28g0wps2mlggnq29c4mkfnibrzb5lvx05a82ikp4")))

(define-public crate-conventional-0.2.0 (c (n "conventional") (v "0.2.0") (d (list (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "04bbcda8hya4a5ki6cbz1hwd673lbbljd9w9ip4m7g1gqcyrmkm2")))

(define-public crate-conventional-0.3.0 (c (n "conventional") (v "0.3.0") (d (list (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "1vlb9m0b982glpxrwxxhhl5l2xlpx8v65n3mcybmd9kb3mmz6l5k")))

(define-public crate-conventional-0.4.0 (c (n "conventional") (v "0.4.0") (d (list (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "0yfj3dq7wzg4j5i2hmdv0l16axa709991iqsvdw5fn7ylypam6jr")))

(define-public crate-conventional-0.4.1 (c (n "conventional") (v "0.4.1") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "1x43fmbbrxn57y7zv4ldd2wkn0fwad17i5mq1qzwz73v47yqcn9n")))

(define-public crate-conventional-0.5.0 (c (n "conventional") (v "0.5.0") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)) (d (n "unicase") (r "^2.5") (d #t) (k 0)))) (h "0ahfvmcyhhm2rw5wcimq5qpdnsi5z8snv3yqanvm0wrx7j9n3x3h")))

