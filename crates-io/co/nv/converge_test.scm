(define-module (crates-io co nv converge_test) #:use-module (crates-io))

(define-public crate-converge_test-0.0.1 (c (n "converge_test") (v "0.0.1") (d (list (d (n "converge") (r "^0.0.1") (d #t) (k 0)) (d (n "converge_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 0)))) (h "17dik4q2v0924fv0vi939jchy0wasqvdlm3dsmmb40rvh6jpmd7w")))

(define-public crate-converge_test-0.0.2 (c (n "converge_test") (v "0.0.2") (d (list (d (n "converge") (r "^0.0.1") (d #t) (k 0)) (d (n "converge_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 0)))) (h "05vc403ncfjabq543dxgh65vxz0l0hzyn3vy2vq47dapb1m6xi0w")))

(define-public crate-converge_test-0.0.3 (c (n "converge_test") (v "0.0.3") (d (list (d (n "converge") (r "^0.0.3") (d #t) (k 0)) (d (n "converge_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 0)))) (h "1z058axb9vz4hwsgyzckgfn4vyp6bmpxz8029a3ffji2pq340hw1")))

(define-public crate-converge_test-0.0.4 (c (n "converge_test") (v "0.0.4") (d (list (d (n "converge") (r "^0.0.4") (d #t) (k 0)) (d (n "converge_derive") (r "^0.0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 0)))) (h "028ii3pqkgzxv58hf9nckjnjvqavnwziwv8003m7zxin6w7ycjza")))

