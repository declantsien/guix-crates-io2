(define-module (crates-io co nv convi) #:use-module (crates-io))

(define-public crate-convi-0.0.1 (c (n "convi") (v "0.0.1") (h "1jpy268ckjsgsyr6rgi2zldjxgjvy5h0hasdkf3k4d305pws8n87") (f (quote (("min_target_ptr_width_64") ("min_target_ptr_width_32") ("min_target_ptr_width_16") ("min_target_ptr_width_128"))))))

(define-public crate-convi-0.0.2 (c (n "convi") (v "0.0.2") (h "1c1ii8a866h7ix5jl8lr8psi3v9yln4160mrmpibklkg48hkgli3") (f (quote (("min_target_pointer_width_64") ("min_target_pointer_width_32") ("min_target_pointer_width_16") ("min_target_pointer_width_128"))))))

(define-public crate-convi-0.0.3 (c (n "convi") (v "0.0.3") (h "1v3zbrmd09mnrzkn7pfs6f032g6hyqkz9r2cb6p1f6lv48lni8cd") (f (quote (("min_target_pointer_width_64") ("min_target_pointer_width_32") ("min_target_pointer_width_16") ("min_target_pointer_width_128"))))))

(define-public crate-convi-0.0.4 (c (n "convi") (v "0.0.4") (h "0vmkxr050bax91f3bnsfzl94rnvp5yss3ig47rwgmdcxh93s4r80") (f (quote (("min_target_pointer_width_64") ("min_target_pointer_width_32") ("min_target_pointer_width_16") ("min_target_pointer_width_128"))))))

(define-public crate-convi-0.0.5 (c (n "convi") (v "0.0.5") (h "0zpja4yr17kwz97dvzp6b3jc071f0q3pm1cyr8dqz7zj4s78y1c3") (f (quote (("min_target_pointer_width_64") ("min_target_pointer_width_32") ("min_target_pointer_width_16") ("min_target_pointer_width_128"))))))

(define-public crate-convi-0.0.6 (c (n "convi") (v "0.0.6") (h "11smpf0wmyv65hlh1slcsfblgpz9il2fqasw61r0qf2j1wx2za0l") (f (quote (("min_target_pointer_width_64") ("min_target_pointer_width_32") ("min_target_pointer_width_16") ("min_target_pointer_width_128"))))))

(define-public crate-convi-0.0.7 (c (n "convi") (v "0.0.7") (h "1v1247fmg110cacz41ihi9wz1jjykmdpc8kr56k0a1n40p8z0k12") (f (quote (("min_target_pointer_width_64") ("min_target_pointer_width_32") ("min_target_pointer_width_16") ("min_target_pointer_width_128"))))))

