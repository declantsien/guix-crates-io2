(define-module (crates-io co nv convert_case) #:use-module (crates-io))

(define-public crate-convert_case-0.1.0 (c (n "convert_case") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "15yhdclqcm5cwk9xmf1ln30sbj25vsdnfmbb8rrak4fhk0082vm5")))

(define-public crate-convert_case-0.2.0 (c (n "convert_case") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "0689k5xjykw34q41v8z80zmnpp7rjplw7dmga5fri3nhfnr0cx22")))

(define-public crate-convert_case-0.3.0 (c (n "convert_case") (v "0.3.0") (d (list (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)))) (h "00krc5bcqkirdawl3hvsghh1b4hgix77gr45xgzc1hf910d0wknq")))

(define-public crate-convert_case-0.3.1 (c (n "convert_case") (v "0.3.1") (d (list (d (n "strum") (r "^0.18.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 2)))) (h "06cfw3n8kxhkrwc58ajyjzhkjcxnw2a27sg92s9229d57wi7m6rj")))

(define-public crate-convert_case-0.3.2 (c (n "convert_case") (v "0.3.2") (d (list (d (n "strum") (r "^0.18.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 2)))) (h "1v7szrq4h3gk3xggqy1zfrg2xl3fdafrpj5y65cbgpqw8igh47vf")))

(define-public crate-convert_case-0.4.0 (c (n "convert_case") (v "0.4.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 2)))) (h "03jaf1wrsyqzcaah9jf8l1iznvdw5mlsca2qghhzr9w27sddaib2") (f (quote (("random" "rand"))))))

(define-public crate-convert_case-0.5.0 (c (n "convert_case") (v "0.5.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 2)))) (h "1f52nciv0ghq67a0z56mgxkr3ka2dx6i8qainkl0rzghmaqj8jpv") (f (quote (("random" "rand"))))))

(define-public crate-convert_case-0.6.0 (c (n "convert_case") (v "0.6.0") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1jn1pq6fp3rri88zyw6jlhwwgf6qiyc08d6gjv0qypgkl862n67c") (f (quote (("random" "rand"))))))

