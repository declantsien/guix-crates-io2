(define-module (crates-io co nv convex_hull) #:use-module (crates-io))

(define-public crate-convex_hull-0.1.1 (c (n "convex_hull") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.9.1") (d #t) (k 0)) (d (n "glium") (r "^0.13.5") (d #t) (k 0)) (d (n "half_edge_mesh") (r "^1.0.6") (d #t) (k 0)) (d (n "image") (r "^0.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1czdvd8kiq3gbknycmg99mfcv356gp8fd6klxw7ip515kjm6i1hd")))

(define-public crate-convex_hull-0.1.2 (c (n "convex_hull") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.10.0") (d #t) (k 0)) (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "half_edge_mesh") (r "^1.0.6") (d #t) (k 0)) (d (n "image") (r "^0.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0l6m3lsji7lcjjw078hhx0z0flh2aiw0cvijc6zzshy2p930b281")))

(define-public crate-convex_hull-0.1.3 (c (n "convex_hull") (v "0.1.3") (d (list (d (n "cgmath") (r "^0.10.0") (d #t) (k 0)) (d (n "glium") (r "^0.15.0") (d #t) (k 0)) (d (n "half_edge_mesh") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1z2wf3rgilkpb4gckm7bvfb34ajafij7h69xd3243qxgn7ihhdrh")))

(define-public crate-convex_hull-0.2.3 (c (n "convex_hull") (v "0.2.3") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "glium") (r "^0.22.0") (d #t) (k 0)) (d (n "half_edge_mesh") (r "^1.1.8") (d #t) (k 0)) (d (n "itertools") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1pz1r3lf419ap26prprgdfbdda2dha1q3bi07hvxdbvk9dapyjw5")))

