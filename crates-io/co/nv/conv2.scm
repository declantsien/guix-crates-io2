(define-module (crates-io co nv conv2) #:use-module (crates-io))

(define-public crate-conv2-0.4.0-alpha.1 (c (n "conv2") (v "0.4.0-alpha.1") (d (list (d (n "custom_derive") (r "^0.1.5") (k 0)) (d (n "quickcheck") (r "^0.9.2") (k 2)))) (h "1pmhn63x6nvzpj8j3zybrr66yhlals47rbghgyalffw31ifj22wk") (f (quote (("std" "custom_derive/std") ("default" "std")))) (r "1.56")))

