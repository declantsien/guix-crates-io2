(define-module (crates-io co nv convolve-image) #:use-module (crates-io))

(define-public crate-convolve-image-0.0.0-reserved (c (n "convolve-image") (v "0.0.0-reserved") (h "1pmwc4z2mvchi2kzqk5dhplw7if6jwa3bhfir0cp7wc2bz059ipx")))

(define-public crate-convolve-image-0.1.0 (c (n "convolve-image") (v "0.1.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "0jqrf3r1flxixhlmkhg5kjw2d3n8yr6cqk534gd3qha62k8mfqim")))

(define-public crate-convolve-image-0.2.0 (c (n "convolve-image") (v "0.2.0") (d (list (d (n "image") (r "^0.25.1") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)))) (h "0wnsxdn0sb4lb39a5phd7zpcyhb5z1wyafn06arfi5wbf72d99pl") (s 2) (e (quote (("ndarray" "dep:ndarray") ("image" "dep:image"))))))

(define-public crate-convolve-image-0.3.0 (c (n "convolve-image") (v "0.3.0") (d (list (d (n "image") (r "^0.25.1") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)))) (h "1n7h4qcyrkfrza41fd4pw94gknd5iy1qhm7n3d01j2kh7vy159fm") (s 2) (e (quote (("ndarray" "dep:ndarray") ("image" "dep:image"))))))

(define-public crate-convolve-image-0.4.0 (c (n "convolve-image") (v "0.4.0") (d (list (d (n "image") (r "^0.25.1") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)))) (h "131rm70k46zlm8lkxjflqggii0680i3sf6hzxspmz8sb9gcakvaa") (s 2) (e (quote (("ndarray" "dep:ndarray") ("image" "dep:image"))))))

