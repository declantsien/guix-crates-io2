(define-module (crates-io co nv convert-base) #:use-module (crates-io))

(define-public crate-convert-base-1.0.0 (c (n "convert-base") (v "1.0.0") (h "0a4kgajkwybkjy1w6ghzbnyagv4h2q6yxx7gcxw5rclsr8lizaw5")))

(define-public crate-convert-base-1.0.1 (c (n "convert-base") (v "1.0.1") (h "0nfjq9s872vxbj3y3py8lrvz0x0qjrg2z2347rqf68y1kgbqk8fv")))

(define-public crate-convert-base-1.0.2 (c (n "convert-base") (v "1.0.2") (h "1fh1bmcif1wg6id85z24ciwygdx44w3q7v1i048ghra586qkm540")))

(define-public crate-convert-base-1.1.0 (c (n "convert-base") (v "1.1.0") (h "08vsb1gkwaxyg3gk671cxzqrkcqdzc1r3i2zgrpxp3v6ckqvkkrl")))

(define-public crate-convert-base-1.1.1 (c (n "convert-base") (v "1.1.1") (h "1dz73mksg84sa9dpaxczrczdlk6ah1lwdvjnnd6rbg43hf7z76j9")))

(define-public crate-convert-base-1.1.2 (c (n "convert-base") (v "1.1.2") (h "12mc5js12z3xxck438dsfvbnpvs46z560ssi1al0pl0j457l0sp5")))

