(define-module (crates-io co nv convo) #:use-module (crates-io))

(define-public crate-convo-0.1.0 (c (n "convo") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0zc2xpn0a07xzhd2v4ghyz4nyfk7jxvdl8k0j3yh5b758zmq9kg0") (y #t)))

(define-public crate-convo-0.1.1 (c (n "convo") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0d3phz8dxdq5wyh51a09zf5hfgfmhgg321wrx28b1rq08d8wg1d9") (y #t)))

(define-public crate-convo-0.1.2 (c (n "convo") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1jqjc6m88r5jkf0g4qlb6yy6dn08m2jwwb2fk0f6rs03vxf47qrm")))

