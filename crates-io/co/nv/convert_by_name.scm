(define-module (crates-io co nv convert_by_name) #:use-module (crates-io))

(define-public crate-convert_by_name-0.0.1 (c (n "convert_by_name") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1rl4rwxsm75dhrl1vaxl82jgk0lx401vgayp16kqkzkc4fxh0qv9")))

(define-public crate-convert_by_name-0.0.2 (c (n "convert_by_name") (v "0.0.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "07s6bifpy5c9d5k54pknh3xl2m5m5gwyn46drm1h55jyf0w0ir4v")))

(define-public crate-convert_by_name-0.0.3 (c (n "convert_by_name") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0xpgr71k3a2i1sfb9knj0svry9n10zanijnxrldz8liq0g4ywxf4")))

(define-public crate-convert_by_name-0.0.4 (c (n "convert_by_name") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0frw2yq5qvap5pi2cin2l1hv77l1m40x10kbw4lvb2c49x7aczp7")))

