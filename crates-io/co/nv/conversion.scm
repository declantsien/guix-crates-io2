(define-module (crates-io co nv conversion) #:use-module (crates-io))

(define-public crate-conversion-0.1.0 (c (n "conversion") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1d7a3lkwa7hk5gcxki2z35swgyphd7d4ba8p0pn8hh281gdhmyh7") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("async" "alloc" "futures-core" "pin-project-lite") ("alloc"))))))

