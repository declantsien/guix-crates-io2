(define-module (crates-io co nv converter) #:use-module (crates-io))

(define-public crate-converter-0.1.0 (c (n "converter") (v "0.1.0") (d (list (d (n "stable-swap-math") (r "^1.5.3") (d #t) (k 0)))) (h "1w62p32aa8kczc0a5y1jc6xjx6m7j18jjx9jr9kv2vw97314qj87")))

(define-public crate-converter-0.1.3 (c (n "converter") (v "0.1.3") (d (list (d (n "stable-swap-math") (r "^1.5") (d #t) (k 0)))) (h "08ldl8bc7cqs66b9pi8i6j5bhs241yjymiibyh95vzvd9zda3pzw")))

(define-public crate-converter-0.2.0 (c (n "converter") (v "0.2.0") (d (list (d (n "stable-swap-math") (r "^1.7") (d #t) (k 0)))) (h "1g0mvh36j8fk64kj4lfl4cyry7kh3lacblif3rl961r6g14aawld")))

(define-public crate-converter-0.2.1 (c (n "converter") (v "0.2.1") (d (list (d (n "stable-swap-math") (r "^1.7") (d #t) (k 0)))) (h "1rc2yj8i76p093hmg78ycx6wd46073vxsaviwh1klfhyiizbdhs0")))

(define-public crate-converter-0.3.0 (c (n "converter") (v "0.3.0") (d (list (d (n "stable-swap-math") (r "^1.8") (d #t) (k 0)))) (h "065qzqy69nh3v13kkjjq33szaj5zw47c5swlsk1c63k625a65gyl")))

(define-public crate-converter-0.3.1 (c (n "converter") (v "0.3.1") (d (list (d (n "stable-swap-math") (r "^1.8") (d #t) (k 0)))) (h "1a0idikafwmg8m30w8d8z3czpc0vjs8h56rlp47v65zkixm3b37c")))

