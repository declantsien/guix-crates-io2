(define-module (crates-io co nv convex-typegen) #:use-module (crates-io))

(define-public crate-convex-typegen-0.0.1 (c (n "convex-typegen") (v "0.0.1") (d (list (d (n "oxc") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0vbkpdplwd2vxs6sarhfavvzb28g6fnjngg5q35dw0r7v82nkz2n")))

