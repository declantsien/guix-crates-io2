(define-module (crates-io co nv convert) #:use-module (crates-io))

(define-public crate-convert-0.1.1 (c (n "convert") (v "0.1.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "curl") (r "^0.2.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)))) (h "1762ss7pls35idkjhfx2zgkn2h66jlrsb5hswrjwawrf87820dwh")))

(define-public crate-convert-0.2.0 (c (n "convert") (v "0.2.0") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "curl") (r "^0.2.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)))) (h "1d6c0pnm8hf4qhgn49xhkyhp306drjd6rk5wkwssbbs3wi5fakh2")))

