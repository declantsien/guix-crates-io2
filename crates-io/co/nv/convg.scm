(define-module (crates-io co nv convg) #:use-module (crates-io))

(define-public crate-convg-0.1.0 (c (n "convg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "graph6-rs") (r "^0.1.2") (d #t) (k 0)))) (h "0y3flc7lsv1n7h2h8vr0cqr973x4kfdmw1wxs941lghjaqwmjlzc")))

(define-public crate-convg-0.1.1 (c (n "convg") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "graph6-rs") (r "^0.1.6") (d #t) (k 0)))) (h "1hhfnzkd7mxrhrxclygdp3qmmdh43hnrxjzszjzmsqyhf6cwxj0j")))

(define-public crate-convg-0.1.2 (c (n "convg") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "graph6-rs") (r "^0.1.6") (d #t) (k 0)))) (h "1j2dla0h4gf4bv6dhx11l8zma9fz0i73jkjzqjs7il2zrpl27a23")))

(define-public crate-convg-0.1.3 (c (n "convg") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "graph6-rs") (r "^0.1.7") (d #t) (k 0)))) (h "14zq6wsixdignxxjhpd7whzm78cyya4zg2lickdb1g1hmynaqkfk")))

(define-public crate-convg-0.1.4 (c (n "convg") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "graph6-rs") (r "^0.1.8") (d #t) (k 0)))) (h "1q4mkmi4rr847xsn9fhhx6ilj93jkhq7cxwp2wg4yakmvvbqp45d")))

