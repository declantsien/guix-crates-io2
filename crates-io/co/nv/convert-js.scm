(define-module (crates-io co nv convert-js) #:use-module (crates-io))

(define-public crate-convert-js-1.0.0-alpha.1 (c (n "convert-js") (v "1.0.0-alpha.1") (h "19hdl4wpg2a6ljzyypcbbdgzg39qgdyr11jym2nambx1v0jlm3jy")))

(define-public crate-convert-js-1.0.0-alpha.10 (c (n "convert-js") (v "1.0.0-alpha.10") (d (list (d (n "convert-js-macros") (r "^1.0.0-alpha.10") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0slj3spg89q4i5321jqd3dmgiwhbafsz0jmzip85glbz1aqr4bn5")))

(define-public crate-convert-js-1.0.0-alpha.11 (c (n "convert-js") (v "1.0.0-alpha.11") (d (list (d (n "convert-js-macros") (r "^1.0.0-alpha.11") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0447c3ysbaxynjvmn95zwsqh13l0w3bzfda4w5sfcph9l653f85a")))

(define-public crate-convert-js-1.0.0-alpha.12 (c (n "convert-js") (v "1.0.0-alpha.12") (d (list (d (n "convert-js-macros") (r "^1.0.0-alpha.12") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0dxg9r5aylmrx1h4rf18ycki2gqp8m76vzj3hybzsrsp3qysdxk6")))

(define-public crate-convert-js-1.0.0-alpha.13 (c (n "convert-js") (v "1.0.0-alpha.13") (d (list (d (n "convert-js-macros") (r "^1.0.0-alpha.13") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1j1whbzhvsc8fypyd4i93jvg1p5z5s1glczv3isjlr90d7wk55yq")))

(define-public crate-convert-js-1.0.0-alpha.14 (c (n "convert-js") (v "1.0.0-alpha.14") (d (list (d (n "convert-js-macros") (r "^1.0.0-alpha.14") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0vnicxbmz2bhfp8sfhmgfsgi67yzczjnl41hvh3xcryp483hj11x")))

(define-public crate-convert-js-1.0.0-alpha.15 (c (n "convert-js") (v "1.0.0-alpha.15") (d (list (d (n "convert-js-macros") (r "^1.0.0-alpha.15") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "06rrddq38zgfs64zc1h2g5apl972m81bhyxqs73igxzvw621r1vf")))

