(define-module (crates-io co nv convert-byte-size-string) #:use-module (crates-io))

(define-public crate-convert-byte-size-string-1.0.0 (c (n "convert-byte-size-string") (v "1.0.0") (h "1lw3681f825jfzx31kk3hm0f3w3sqlbx9wfzjgls4jwfc64d1r1a")))

(define-public crate-convert-byte-size-string-1.1.0 (c (n "convert-byte-size-string") (v "1.1.0") (h "02y5w6yhszp45yjf6sp650p4c9ym3vh8kkzvc0sihmy40my4f16p")))

(define-public crate-convert-byte-size-string-1.1.1 (c (n "convert-byte-size-string") (v "1.1.1") (h "01n68grzq3r759kcwv09akwd1w3xjdwnakjid1rcczxy15527gx3")))

(define-public crate-convert-byte-size-string-1.1.2 (c (n "convert-byte-size-string") (v "1.1.2") (h "1vc67h9hi117j0bkvaklmv6q5xm2w6c9671n9kgjns1ylxyrxwgm")))

