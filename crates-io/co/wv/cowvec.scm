(define-module (crates-io co wv cowvec) #:use-module (crates-io))

(define-public crate-cowvec-0.1.0 (c (n "cowvec") (v "0.1.0") (h "18khsx555159xnkk30lsci9jghy9id029plgfk6ar92ckhm62hxf")))

(define-public crate-cowvec-0.1.1 (c (n "cowvec") (v "0.1.1") (h "1mpn6rf5id2fh986v577dm9sqq83v2vay9mijqpigfn772n1zng8")))

(define-public crate-cowvec-0.1.2 (c (n "cowvec") (v "0.1.2") (h "1z0d4rfhldqwbjxqpf0dmmb6vp261j7hi02vhp4m9pvia8yvkhl1")))

