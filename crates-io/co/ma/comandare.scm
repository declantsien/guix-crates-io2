(define-module (crates-io co ma comandare) #:use-module (crates-io))

(define-public crate-comandare-0.1.0 (c (n "comandare") (v "0.1.0") (h "0ywqkxn4v7jc58ip5n0fnyzaxzzbgzjccx0f2dbcxy4zn14pg7ni")))

(define-public crate-comandare-0.2.0 (c (n "comandare") (v "0.2.0") (h "1za3ww44hlw2i52lvs30gw5hjnxn09asqx0b298ank6i0b6r4lx2")))

(define-public crate-comandare-0.3.0 (c (n "comandare") (v "0.3.0") (d (list (d (n "mockall") (r "^0.8.0") (d #t) (k 2)))) (h "06yzpzp33zhangv687wzvw1z5g5szgv3ic9yg3l2fiqs7s8l5mdq")))

