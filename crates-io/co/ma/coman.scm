(define-module (crates-io co ma coman) #:use-module (crates-io))

(define-public crate-coman-0.1.0 (c (n "coman") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)))) (h "1li1d1br4jz13f0z4jrx8vna7jagc3wc6hi6jaznlw3p9k02kv56")))

(define-public crate-coman-0.2.0 (c (n "coman") (v "0.2.0") (d (list (d (n "getargs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)))) (h "17zkrg6szqfnbvafil5fxv2n6711vg8qfm9bmdy579v6h70m3lbg")))

(define-public crate-coman-0.2.1 (c (n "coman") (v "0.2.1") (d (list (d (n "getargs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)))) (h "1b1knrp7yzfjmf4wpl56l0fsrdq1i4s9kivry86yhz813y3v0lb2")))

(define-public crate-coman-0.3.0 (c (n "coman") (v "0.3.0") (d (list (d (n "alphanumeric-sort") (r "^1.4.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "getargs") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "0sq3w28jr7l3vdn363dkqzqlq3amrwwn89j7bwnj3fr0aaxhqld8")))

(define-public crate-coman-0.3.1 (c (n "coman") (v "0.3.1") (d (list (d (n "alphanumeric-sort") (r "^1.4.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "getargs") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "xz2") (r "^0.1.6") (d #t) (k 0)))) (h "0qmd2pwgy1swlpf60gv04qdn8l9ypnyl2gg0vsq6l0sm1zr5k8cj")))

