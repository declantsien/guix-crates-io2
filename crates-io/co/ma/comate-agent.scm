(define-module (crates-io co ma comate-agent) #:use-module (crates-io))

(define-public crate-comate-agent-0.1.0 (c (n "comate-agent") (v "0.1.0") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "117vww6q4x9q6ln4r7bd05pj3b5ik3fjsml97nm9969g4s61w289")))

