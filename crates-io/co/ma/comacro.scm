(define-module (crates-io co ma comacro) #:use-module (crates-io))

(define-public crate-comacro-0.0.1 (c (n "comacro") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("max_level_trace" "release_max_level_off"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit" "parsing"))) (k 0)))) (h "11anrpyqgqk5pj8hwlvabia1bkx2mr588wsyz5vfqh6yrjsny6p0")))

