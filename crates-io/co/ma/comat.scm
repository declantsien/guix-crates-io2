(define-module (crates-io co ma comat) #:use-module (crates-io))

(define-public crate-comat-0.1.0 (c (n "comat") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "10z8b2v4anns7xiwf4bnqrpx8fasynzk34prip2vjvhvnnb78vr0") (y #t)))

(define-public crate-comat-0.1.1 (c (n "comat") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0n37w4hcy3h37jjpc8cvj2fgag959xdfd7xz7cmk3z4mak2wrisc")))

(define-public crate-comat-0.1.2 (c (n "comat") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0dxd3frbw0gd7aqwmmbqxw39d5yivg0dkyz65sy636llc1h4w6ii")))

(define-public crate-comat-0.1.3 (c (n "comat") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0wy14h60wcg4swanxcfrj0r3ly5f2r5v7ylcf5jpn0kzbp5h2kzj")))

