(define-module (crates-io co e- coe-rs) #:use-module (crates-io))

(define-public crate-coe-rs-0.1.0 (c (n "coe-rs") (v "0.1.0") (h "08akd3ah5ijpbhlisan32946bizly652kj4bgwaklcymwfahc4kj")))

(define-public crate-coe-rs-0.1.1 (c (n "coe-rs") (v "0.1.1") (h "0ic0wq1b160id7jc8fgdjw07hzg5sgc4946670dwivmd388zqs30")))

(define-public crate-coe-rs-0.1.2 (c (n "coe-rs") (v "0.1.2") (h "16660pc135s339lxarggb0fkrbk99g00s7lb48qpdh222mj1x3vy")))

