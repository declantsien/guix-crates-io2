(define-module (crates-io co w- cow-utils) #:use-module (crates-io))

(define-public crate-cow-utils-0.1.0 (c (n "cow-utils") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)))) (h "1br8xza6w97bijxvylanc07di10wahvjy4k8dadbk5fpz9d9bp3r") (f (quote (("nightly") ("default"))))))

(define-public crate-cow-utils-0.1.1 (c (n "cow-utils") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)))) (h "0hkvqghfxk28k3gsqavgvg5glc9r8rjqpsfw7iw4ap9pdy0sb2ad") (f (quote (("nightly") ("default"))))))

(define-public crate-cow-utils-0.1.2 (c (n "cow-utils") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0wq1hcqj17ivzb45w3l0l87q81srapvpbqxf055x4xazmzgkmfvr") (f (quote (("nightly") ("default"))))))

(define-public crate-cow-utils-0.1.3 (c (n "cow-utils") (v "0.1.3") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)))) (h "0y9cxf0hm2hy4bn050wl2785md5q4i5gy9asjq006ip1mwjfyys1") (f (quote (("nightly") ("default"))))))

