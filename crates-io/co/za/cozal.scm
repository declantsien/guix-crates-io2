(define-module (crates-io co za cozal) #:use-module (crates-io))

(define-public crate-cozal-0.0.1 (c (n "cozal") (v "0.0.1") (d (list (d (n "flume") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("time" "rt-threaded" "macros"))) (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "0vsb96qm8by4p6fncr60xakv47d6ch3k57mfkydgs4jg3pb2wawl")))

(define-public crate-cozal-0.0.2 (c (n "cozal") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 0)) (d (n "flume") (r "^0.7.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "pin-project") (r "^0.4.23") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (o #t) (d #t) (k 0)))) (h "0wa24w5i1niqaxjaxazkc4x6sny4bj81bs7vmciglsvzzkng59nr") (f (quote (("window" "winit") ("realtime" "tokio"))))))

