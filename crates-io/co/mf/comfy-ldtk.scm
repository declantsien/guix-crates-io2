(define-module (crates-io co mf comfy-ldtk) #:use-module (crates-io))

(define-public crate-comfy-ldtk-0.1.0 (c (n "comfy-ldtk") (v "0.1.0") (d (list (d (n "comfy-core") (r "^0.3.0") (d #t) (k 0)) (d (n "grids") (r "^0.2.3") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kr79c26k7773zr0yiq53ijjszrrhy8wyh8yakfxl29j4nxfwiqv")))

(define-public crate-comfy-ldtk-0.2.0 (c (n "comfy-ldtk") (v "0.2.0") (d (list (d (n "comfy-core") (r "^0.4.0") (d #t) (k 0)) (d (n "grids") (r "^0.2.3") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02a69jpfjqwadhxpvbnxjmh4qsrwgh67dirpr8sbyac27jrg5ndh") (f (quote (("default") ("ci-release"))))))

