(define-module (crates-io co mf comfy-print) #:use-module (crates-io))

(define-public crate-comfy-print-0.1.0 (c (n "comfy-print") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1wp2gg530si6wkza8c31wvf88mxfh2hf95f4vb9akcwy3sn9aysy") (f (quote (("default") ("async_impl")))) (y #t)))

(define-public crate-comfy-print-0.1.1 (c (n "comfy-print") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0ix7npfph7b0xc41yaivys1qrhzmp7kaq2s8b4rrv2j62ih9h43c") (f (quote (("default") ("async_impl")))) (y #t)))

(define-public crate-comfy-print-0.1.2 (c (n "comfy-print") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0gpgdhk17xvllxp3whswh48bx48dvcw7pfmmq25rvs16ldabxlrp") (f (quote (("default") ("async_impl")))) (y #t)))

(define-public crate-comfy-print-0.1.3 (c (n "comfy-print") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0k5id3z8zghc3rpg7zfdv0gazk8a968iwbysixg5499b4x3z3g8c") (f (quote (("default") ("async_impl")))) (y #t)))

(define-public crate-comfy-print-0.1.4 (c (n "comfy-print") (v "0.1.4") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "19s0x98jkkl2k7j48xvz5i2qqgh9h057v51y0as3m5swralyz94i") (f (quote (("default") ("async_impl")))) (y #t)))

(define-public crate-comfy-print-0.2.1 (c (n "comfy-print") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "185481r9gy1r6lzz1309qz4xfhlgyw9c4rz6rfh11j3sdhjdxbjw")))

(define-public crate-comfy-print-0.3.0 (c (n "comfy-print") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0jr84w5by39p0g70mcskf8hyn980yav3g30y0kad44fwssqdqihc")))

