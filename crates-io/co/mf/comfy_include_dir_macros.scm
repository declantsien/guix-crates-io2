(define-module (crates-io co mf comfy_include_dir_macros) #:use-module (crates-io))

(define-public crate-comfy_include_dir_macros-0.7.3 (c (n "comfy_include_dir_macros") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0y2kikbr67j8xivvw5na9ihdbh2q2s4mhlc6f4jxf0l80kphdx20") (f (quote (("nightly") ("metadata")))) (r "1.56")))

