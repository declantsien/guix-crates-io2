(define-module (crates-io co mf comfy_include_dir) #:use-module (crates-io))

(define-public crate-comfy_include_dir-0.7.3 (c (n "comfy_include_dir") (v "0.7.3") (d (list (d (n "comfy_include_dir_macros") (r "^0.7.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ilxv603mdivh848wlxgizcy4zsfym6pmwads041hzdq2f3v4mab") (f (quote (("nightly" "comfy_include_dir_macros/nightly") ("metadata" "comfy_include_dir_macros/metadata") ("default")))) (r "1.56")))

