(define-module (crates-io co mf comfy-git-version) #:use-module (crates-io))

(define-public crate-comfy-git-version-0.4.0 (c (n "comfy-git-version") (v "0.4.0") (d (list (d (n "comfy-git-version-macro") (r "=0.3.5") (d #t) (k 0)))) (h "15dk4gya0pcrbxnvqlm1qhcs0yz6hd7qak6533zasjm0fnp8wahp")))

