(define-module (crates-io co mf comfy-color-backtrace) #:use-module (crates-io))

(define-public crate-comfy-color-backtrace-0.6.0 (c (n "comfy-color-backtrace") (v "0.6.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0hyxhdr3wd0cgsknvmmc6bs9zgh2lv25cx5mrwkrqga480m3dd86") (f (quote (("resolve-modules" "regex") ("gimli-symbolize" "backtrace/gimli-symbolize") ("default" "gimli-symbolize"))))))

