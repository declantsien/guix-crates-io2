(define-module (crates-io co mf comfy-print-sync) #:use-module (crates-io))

(define-public crate-comfy-print-sync-0.1.0 (c (n "comfy-print-sync") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1fp02865acmr1jrg2hr9m98p62zhg9rcgnza4vld3ilvy3j184c6")))

(define-public crate-comfy-print-sync-0.1.1 (c (n "comfy-print-sync") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0i4gc7gymvywkyv11rcbrrjb1izqyhcl0h36lzv26nylagvbszwz")))

