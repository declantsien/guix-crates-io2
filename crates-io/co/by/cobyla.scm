(define-module (crates-io co by cobyla) #:use-module (crates-io))

(define-public crate-cobyla-0.1.0 (c (n "cobyla") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1q2l2cgpghngn64sjqhf2jhsyvfy5c5nl1hfp0fym2bhkcj3iyxb")))

(define-public crate-cobyla-0.1.1 (c (n "cobyla") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0abh16p2jbsj6pdwmbwdx2p0h3lhkxzx32hyqm5zrbx3qk0vbhfd")))

(define-public crate-cobyla-0.1.2 (c (n "cobyla") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1sfgkxfwg68vfhgwm0nqlykbd85jmn4ylm5qnchkdws3v7fc5bp3")))

(define-public crate-cobyla-0.1.3 (c (n "cobyla") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1nnsxpvrmjhdxa64shxf3v0nm4rmls8x50mv9l6rf1jw9nqgqf5z")))

(define-public crate-cobyla-0.1.4 (c (n "cobyla") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "12an1aasdyya8w42pjr9asnxcw09rh6hzxifrqfvn624v32i1h1m")))

(define-public crate-cobyla-0.2.0 (c (n "cobyla") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1i3f2w42v4ih8hrkxvhhmdn4db3lxf1xih7s29297pr3wwzy2vw7")))

(define-public crate-cobyla-0.3.0 (c (n "cobyla") (v "0.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "argmin") (r "^0.8.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0h296ws5l1xynhmixsbbngg0a1viagaizwigzm4cmz0l5x971ifd")))

(define-public crate-cobyla-0.3.1 (c (n "cobyla") (v "0.3.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "argmin") (r "^0.8.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "10p1i3ypv6jz4vi08i98n91hjpaqgsjnflx5sp845hmvd5qn2dm3")))

(define-public crate-cobyla-0.3.2 (c (n "cobyla") (v "0.3.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "argmin") (r "^0.8.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "19jgi5aa7iqw3wjyr1ryc1p72aiqdkjhs13ml5jjd4f9finwh2hy")))

(define-public crate-cobyla-0.3.3 (c (n "cobyla") (v "0.3.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "argmin") (r "^0.8.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "01kanbkailmzbv26yqgyafxzxaskdn1i4i3kh28s4xziy00jnjgp")))

(define-public crate-cobyla-0.4.0-alpha.1 (c (n "cobyla") (v "0.4.0-alpha.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "argmin") (r "^0.8.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "01z72pm99b4v6mw0pav67d2f1yww8q597pyj15zbgb4lczan80g0")))

(define-public crate-cobyla-0.4.0 (c (n "cobyla") (v "0.4.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "argmin") (r "^0.8.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0276hj5d30abzivcj590lrb5qrakyhikzdakfnldl4fjdk324b9b")))

(define-public crate-cobyla-0.5.0 (c (n "cobyla") (v "0.5.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "argmin") (r "^0.8.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1p3iz1xnvvni6zhz9m5zlfr78hl7wbyr0bhrvdqiqg1zg9q20d4a")))

(define-public crate-cobyla-0.5.1 (c (n "cobyla") (v "0.5.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "argmin") (r "^0.8.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0h232502kzjfdzkp1w0dnnn36clm9s31wghmbqqa9r40ychihqzr")))

(define-public crate-cobyla-0.6.0 (c (n "cobyla") (v "0.6.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "argmin") (r "^0.10") (d #t) (k 0)) (d (n "argmin-observer-slog") (r "^0.1") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09mrzrdhkh4lzzzllwjfvy9vij9i70kq9wsqjfd4x8v6zj7pklh0") (s 2) (e (quote (("serde1" "dep:serde" "argmin/serde1"))))))

