(define-module (crates-io co ck cock-tier) #:use-module (crates-io))

(define-public crate-cock-tier-0.1.0 (c (n "cock-tier") (v "0.1.0") (h "105b5rsafhydzaxank3bszrmhrwv8p6aq5f81g6s55qqxsh54zd1") (y #t)))

(define-public crate-cock-tier-0.1.1 (c (n "cock-tier") (v "0.1.1") (h "1hd0nhqdnywh59c4fd5s8px7qj4scn1wvlz0kg6kpz1hm9m15k7j") (y #t)))

(define-public crate-cock-tier-0.1.2 (c (n "cock-tier") (v "0.1.2") (h "0sxp43xc08s3vngpl4a4xl6y55zrr2ndcrz4lriy4ab6f3n9gyxq") (y #t)))

(define-public crate-cock-tier-0.1.3 (c (n "cock-tier") (v "0.1.3") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "1hq232pc42sh6n5qg0wk3g4mgrg7kzr4fn49zpq9qn7c5r0rwss5") (y #t)))

(define-public crate-cock-tier-0.1.4 (c (n "cock-tier") (v "0.1.4") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "1bjw0kbblmcny7rahlw4yr76q3qxx6ksp2cx706gfz8kg9q8zxnz") (y #t)))

(define-public crate-cock-tier-0.2.0 (c (n "cock-tier") (v "0.2.0") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "009aby78wsp64p4zpwx2zr03awbfbjdwysg9vbry00md7y33rb7j") (y #t)))

(define-public crate-cock-tier-0.2.1 (c (n "cock-tier") (v "0.2.1") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "1z869r2bvkzdx565l1lmpbb29mzs0qnalsyqpgdzb4srav05rxgc") (y #t)))

(define-public crate-cock-tier-0.2.2 (c (n "cock-tier") (v "0.2.2") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0ki3lgxk806ck36995mqyy09kamk6j6869l2s4vrrcblm8in4b8j") (y #t)))

(define-public crate-cock-tier-0.2.3 (c (n "cock-tier") (v "0.2.3") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "11xq6ffy6phadhv8878aq6l1ww6knndxnd0n6zc2mry8di8wpn1s") (y #t)))

(define-public crate-cock-tier-1.0.0 (c (n "cock-tier") (v "1.0.0") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "16v8wch44gwh0xf2ks5s7jl8xn3k85l2pn01s26jnjxl3izjvy1x") (y #t)))

(define-public crate-cock-tier-1.0.1 (c (n "cock-tier") (v "1.0.1") (d (list (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "10c7qhgkb77pjlyllli4kj5j4yk7kajfyg0829mkmdda9mi35s7f")))

