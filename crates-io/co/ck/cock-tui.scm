(define-module (crates-io co ck cock-tui) #:use-module (crates-io))

(define-public crate-cock-tui-0.8.0 (c (n "cock-tui") (v "0.8.0") (d (list (d (n "cock-lib") (r "^1.0.0") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "00h8162fb4d0yx8b7b6nialh9n66jfl8awkwsadjxjllvq8czkz4")))

(define-public crate-cock-tui-0.8.1 (c (n "cock-tui") (v "0.8.1") (d (list (d (n "cock-lib") (r "^1.0.1") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0c0ly366bpyvc0sxil3h8lcrgzphx8qbfa0v8ga3icahh28176fh")))

(define-public crate-cock-tui-1.0.0 (c (n "cock-tui") (v "1.0.0") (d (list (d (n "cock-lib") (r "^2.1.0") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)))) (h "0dj4gm4kmx935bni4cg1qhigndb5vv5cqp25gs83fy3s216lcyaz")))

