(define-module (crates-io co ck cocktail) #:use-module (crates-io))

(define-public crate-cocktail-1.0.0 (c (n "cocktail") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "018x0qjz0m1yk28szcbqwc6a9syqsz76lyv37vqzh0yh0vwgr3fm")))

