(define-module (crates-io co ck cocklock) #:use-module (crates-io))

(define-public crate-cocklock-0.1.0 (c (n "cocklock") (v "0.1.0") (d (list (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "postgres") (r "^0.19") (f (quote ("with-uuid-1"))) (d #t) (k 0)) (d (n "postgres-native-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "testcontainers") (r "^0.14.0") (d #t) (k 2)) (d (n "tokio-postgres") (r "^0.7.6") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0i2s2fdbqn4s476pmmxdh22dackqgs88j15482pmqprdff0z836z")))

