(define-module (crates-io co ck cock-cli) #:use-module (crates-io))

(define-public crate-cock-cli-1.0.0 (c (n "cock-cli") (v "1.0.0") (d (list (d (n "cock-lib") (r "^1.0.0") (d #t) (k 0)))) (h "1vh24nqvy6zp98ainc8v787sw3p5z0biinpxngmak128rn17h1nv")))

(define-public crate-cock-cli-1.0.1 (c (n "cock-cli") (v "1.0.1") (d (list (d (n "cock-lib") (r "^1.0.1") (d #t) (k 0)))) (h "12zxcs8nbm97vkzh75i03r1q32vx61pbfk40xpq96alv2x1xf09k")))

(define-public crate-cock-cli-2.0.0 (c (n "cock-cli") (v "2.0.0") (d (list (d (n "cock-lib") (r "^2.1.0") (d #t) (k 0)))) (h "15kfy09584bvbjqqk0vfqj6ns589rv89x1wpjsczzkdl1r56n41d")))

