(define-module (crates-io co ck cock-web) #:use-module (crates-io))

(define-public crate-cock-web-0.1.1 (c (n "cock-web") (v "0.1.1") (d (list (d (n "cock-lib") (r "^1.0.1") (d #t) (k 0)))) (h "1ajrg1xx8w3c7xgpya6dnghg2ri5dsir2n99v0mgs2qk4lxiv03y") (y #t)))

(define-public crate-cock-web-0.5.0 (c (n "cock-web") (v "0.5.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1z9rnf0lg0x10x2ggwrp54firmfixnf944bn4fjj1a451k2r5gy3") (y #t)))

(define-public crate-cock-web-0.7.0 (c (n "cock-web") (v "0.7.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1d4g42570zcvxwimfl1mr6l0k6fgc0lxrm3kf0sqy109pq4i5yp1") (y #t)))

