(define-module (crates-io co op cooper) #:use-module (crates-io))

(define-public crate-cooper-0.1.0 (c (n "cooper") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 0)))) (h "0fqrmi9sas7zwj6ni911297hnxlp2sizlbn6zcrfib8h8nc5gnwd")))

(define-public crate-cooper-0.1.1 (c (n "cooper") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 0)))) (h "02wrfyp2803whsl6mdc2gz5b3zckic3jny63qci0bdrx6ndshz8a")))

