(define-module (crates-io co op cooplan-mongodb) #:use-module (crates-io))

(define-public crate-cooplan-mongodb-0.1.0 (c (n "cooplan-mongodb") (v "0.1.0") (d (list (d (n "mongodb") (r "^2.3.1") (d #t) (k 0)))) (h "078syc8d35cfb0qxw4skrhh4n3jycrkxsszpsvpqs6fpm1m6fdk5")))

(define-public crate-cooplan-mongodb-0.2.0 (c (n "cooplan-mongodb") (v "0.2.0") (d (list (d (n "mongodb") (r "^2.3.1") (d #t) (k 0)))) (h "1zdjalb9nv6ss3lgs5gickivzq42zpq328gwls4bpycqqy4ygqrv")))

