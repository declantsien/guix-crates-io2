(define-module (crates-io co op cooplan-amqp-api-shared) #:use-module (crates-io))

(define-public crate-cooplan-amqp-api-shared-0.1.0 (c (n "cooplan-amqp-api-shared") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0vnzf6c3qmjjls8kgbds46bjvikxydhpncnhb1sdh2a7vrc5ks63")))

(define-public crate-cooplan-amqp-api-shared-0.1.1 (c (n "cooplan-amqp-api-shared") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0aa7vxwcjhmgksb61jjfk8pdc9p7xqpikl4iz42618j0xjqhnspq")))

(define-public crate-cooplan-amqp-api-shared-0.1.2 (c (n "cooplan-amqp-api-shared") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0r1adrvwp9a49bchr2r9fh8rkscr739p2xj9gll92nrfqfgy08nv")))

