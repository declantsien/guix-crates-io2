(define-module (crates-io co op cooplan-definition-schema-validator) #:use-module (crates-io))

(define-public crate-cooplan-definition-schema-validator-0.1.0 (c (n "cooplan-definition-schema-validator") (v "0.1.0") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "16xcyp8b7k5pcv7jrf20634zqqdk68p64y0icv1fr36rj6nqz6v1")))

(define-public crate-cooplan-definition-schema-validator-0.1.1 (c (n "cooplan-definition-schema-validator") (v "0.1.1") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "13f9glsm69ls95q5g4shvi8gi5sj9k2nhw4c7659cbmlkn7m362q")))

(define-public crate-cooplan-definition-schema-validator-0.2.0 (c (n "cooplan-definition-schema-validator") (v "0.2.0") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1cf555rg4c6baqj2k27np4sdkr5yxvpri3k3brki34bvpirhfyn1")))

(define-public crate-cooplan-definition-schema-validator-0.2.1 (c (n "cooplan-definition-schema-validator") (v "0.2.1") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0iavhz1lg6szvayx6f5dmpck17xsff6gv5k4ra9khqixc5zaw214")))

(define-public crate-cooplan-definition-schema-validator-0.2.2 (c (n "cooplan-definition-schema-validator") (v "0.2.2") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0b4gmry28dj75irk971kb57qc05rf9xwmd2hjvjwfl7p9dyqmi29")))

(define-public crate-cooplan-definition-schema-validator-0.2.3 (c (n "cooplan-definition-schema-validator") (v "0.2.3") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1ff0n1xz551ll580awi2dn8sxff4q9hc55js01l51l871vn5g2xg")))

