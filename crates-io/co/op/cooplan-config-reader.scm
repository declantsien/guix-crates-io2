(define-module (crates-io co op cooplan-config-reader) #:use-module (crates-io))

(define-public crate-cooplan-config-reader-0.1.0 (c (n "cooplan-config-reader") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wkh7va34rwc5imzrkiwp655pmk8ybczsqyzn8n0camnsm32hn95")))

