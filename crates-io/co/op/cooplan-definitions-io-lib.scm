(define-module (crates-io co op cooplan-definitions-io-lib) #:use-module (crates-io))

(define-public crate-cooplan-definitions-io-lib-0.1.0 (c (n "cooplan-definitions-io-lib") (v "0.1.0") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0xcnqdpbi4gbskhl4k62wpnzi5d9gpzcyczfcd18gc9ssxrnx0ws")))

(define-public crate-cooplan-definitions-io-lib-0.1.1 (c (n "cooplan-definitions-io-lib") (v "0.1.1") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0xpkxrrqinxj16a6kj8ssd25zb9ds9mm80mixg7g6x6924iacx6h")))

(define-public crate-cooplan-definitions-io-lib-0.1.2 (c (n "cooplan-definitions-io-lib") (v "0.1.2") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0csm21pgg3x3lypmlsqabgafn9c1igk2zkrr170nvqj00ci9qd4i")))

(define-public crate-cooplan-definitions-io-lib-0.1.3 (c (n "cooplan-definitions-io-lib") (v "0.1.3") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1x85q6a23i8dx9bw3nv17hplxjakx2iq4id42p77zlah4rp4lcbg")))

(define-public crate-cooplan-definitions-io-lib-0.1.4 (c (n "cooplan-definitions-io-lib") (v "0.1.4") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0sjacyfqqg36p9n8rx99qzr3smd5zspvrkbzrjkylrm2hcslxivz")))

(define-public crate-cooplan-definitions-io-lib-0.1.5 (c (n "cooplan-definitions-io-lib") (v "0.1.5") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0ay8bxqbn8b7fm8ha07w3v3dlfrmqhsmq30jgmhxfv06jk0sf21c")))

(define-public crate-cooplan-definitions-io-lib-0.1.6 (c (n "cooplan-definitions-io-lib") (v "0.1.6") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "15hdx8fbjfwjmcxq5jm2dq6s280279v3d3vwyfhfjk91w3w7w92a")))

(define-public crate-cooplan-definitions-io-lib-0.1.7 (c (n "cooplan-definitions-io-lib") (v "0.1.7") (d (list (d (n "cooplan-definitions-lib") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "10bc9qf6bl4xmza7bkk9kw1b8ywg7cc8wa7b0rp00h6dr4gr5i84")))

