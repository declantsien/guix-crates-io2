(define-module (crates-io co op cooplan-definition-git-downloader) #:use-module (crates-io))

(define-public crate-cooplan-definition-git-downloader-0.1.0 (c (n "cooplan-definition-git-downloader") (v "0.1.0") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "159gxh4hff1ych1fdx9ayav7lx2i6y30sqpa2kabm4042mxg1ii4")))

(define-public crate-cooplan-definition-git-downloader-0.1.1 (c (n "cooplan-definition-git-downloader") (v "0.1.1") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "16r8crjxrcddc2azyn60zc88lxj4xbi2apzj6q03mc9kkp3v5920")))

(define-public crate-cooplan-definition-git-downloader-0.1.2 (c (n "cooplan-definition-git-downloader") (v "0.1.2") (d (list (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0mwxlbggicg4sqgh15nlv89h0754wpd4y1zibpb1rwyp9jraya06")))

