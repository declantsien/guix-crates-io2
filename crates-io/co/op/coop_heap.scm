(define-module (crates-io co op coop_heap) #:use-module (crates-io))

(define-public crate-coop_heap-0.1.0 (c (n "coop_heap") (v "0.1.0") (h "0hhxnhrygfqqxmi41kjfx0wr07xrrihmwq11i1kcxw4n709zpz3y") (r "1.56")))

(define-public crate-coop_heap-0.1.1 (c (n "coop_heap") (v "0.1.1") (h "0byniq84k1cq2mpb3yr22fbd2wvggy53j725x9179azsg2c1f984") (r "1.56")))

