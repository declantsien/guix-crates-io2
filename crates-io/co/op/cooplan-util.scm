(define-module (crates-io co op cooplan-util) #:use-module (crates-io))

(define-public crate-cooplan-util-0.1.0 (c (n "cooplan-util") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0rwih54ysc382gxq8ls808qvs25jij7qml6jpzzp0xmr0hpdlwrr")))

(define-public crate-cooplan-util-0.1.1 (c (n "cooplan-util") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1v01sln432ggrmh44z9pcw0zb8vlmkm31xwmrlm89yy53fd3w53r")))

