(define-module (crates-io co op cooptex) #:use-module (crates-io))

(define-public crate-cooptex-0.1.0 (c (n "cooptex") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "loom") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1pb1y03cbkcvnn9ylkvqb03y7y0w0r3q6x62ysqifhzihfnvdmv0")))

(define-public crate-cooptex-0.1.1 (c (n "cooptex") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "loom") (r "^0.5.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0pmmpq8ng4r6fj2jilb3krrh3hinfm7sg8divd4ddqyqknz797dj") (f (quote (("loom-tests"))))))

(define-public crate-cooptex-0.1.2 (c (n "cooptex") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "loom") (r "^0.5.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0mn3wa1alpiahqb82whiyvs1fn6qwlrmms8n6f7ar93qil2wvsc0") (f (quote (("loom-tests"))))))

(define-public crate-cooptex-0.1.3 (c (n "cooptex") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "loom") (r "^0.5.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0lnnfj89d8kicnhhiswl7ysdh7dbhpr3s97fck2aqfn07j358k42") (f (quote (("loom-tests"))))))

(define-public crate-cooptex-0.2.0 (c (n "cooptex") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "frunk") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "17340ag0s6kvhg9c85vcmp7nsxn13izgz0wn3q26f0knv2i1r0nx") (f (quote (("loom-tests"))))))

(define-public crate-cooptex-0.2.1 (c (n "cooptex") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "frunk") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "loom") (r "^0.5.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1y22p9y62js0ri9lg28ca8vcjly1wr9mz6fyfhz0ywnr7pmgkx3k") (f (quote (("loom-tests"))))))

