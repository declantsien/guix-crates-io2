(define-module (crates-io co op cooplan-auth) #:use-module (crates-io))

(define-public crate-cooplan-auth-0.1.0 (c (n "cooplan-auth") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b29zz6aa4vpayb800wfah85hlzbngyxm73a2cd0incblnn1i3kf")))

(define-public crate-cooplan-auth-0.1.1 (c (n "cooplan-auth") (v "0.1.1") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0al2ks44a4dcjncrxzlw7zipxfacv5n1zs5xfpipzcsk40yj6jyj")))

(define-public crate-cooplan-auth-0.1.2 (c (n "cooplan-auth") (v "0.1.2") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lchyrxwr36gbqhm8c000nlychlb6qajzsbjlywhqb82l5zb113d")))

(define-public crate-cooplan-auth-0.1.3 (c (n "cooplan-auth") (v "0.1.3") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z2mk3820qx5jarf7b8283s6s1jq403kx16rh80ipa7lm9rs5bn4")))

(define-public crate-cooplan-auth-0.1.4 (c (n "cooplan-auth") (v "0.1.4") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bx58bh48fbp11n70j5c625fjmzdzvg164v9wm1h5w0jchpvjq5f")))

