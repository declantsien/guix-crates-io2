(define-module (crates-io co op cooplan-definitions-lib) #:use-module (crates-io))

(define-public crate-cooplan-definitions-lib-0.1.0 (c (n "cooplan-definitions-lib") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1r5rxws1bdbfwkx87ajxydc1hx8hc3f7snv5zfng06d8fdwhgn98")))

(define-public crate-cooplan-definitions-lib-0.1.1 (c (n "cooplan-definitions-lib") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1bvmvkgg9j2w2hbpwfc3dv16pgmi42kibhs74z6h23xzvyp7n2ar")))

(define-public crate-cooplan-definitions-lib-0.1.2 (c (n "cooplan-definitions-lib") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1zm9hig95v3wkrls1ncbr6n8i1h44h4nk9zmsfrja1a81g0936gw")))

(define-public crate-cooplan-definitions-lib-0.1.3 (c (n "cooplan-definitions-lib") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "004clmk7y2ikq6rz4wvxabzp6qqsn1ddv5fc54i00ylrjknds61b")))

(define-public crate-cooplan-definitions-lib-0.1.4 (c (n "cooplan-definitions-lib") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0f3qp1q5yfbvhc0mx6cqbk7h1n2ah4gx6scgzpv23wifyp3fpqqz")))

(define-public crate-cooplan-definitions-lib-0.1.5 (c (n "cooplan-definitions-lib") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1zmkmg4l2qz61hx3skasyrjappx06zflik2bynjwprnc032pr32n")))

(define-public crate-cooplan-definitions-lib-0.1.6 (c (n "cooplan-definitions-lib") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1kz3vxf7c0gh9i50z9fa3v8wz0d6cnm5b0ai4cf9q4phagbbxhqs")))

