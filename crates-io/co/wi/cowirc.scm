(define-module (crates-io co wi cowirc) #:use-module (crates-io))

(define-public crate-cowirc-0.1.0 (c (n "cowirc") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-native-tls") (r "^0.3.1") (d #t) (k 0)))) (h "1w3hf9cjmnbv1i4vbhqq5rpk1x9fwxcljl30jwyq2n9pxnnarr27")))

(define-public crate-cowirc-0.2.0 (c (n "cowirc") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-native-tls") (r "^0.3.1") (d #t) (k 0)))) (h "16p0ndpr7ni2c3z8r5d2s1n2q97wmv2c2vkwqab0d9dknlwhf47x")))

