(define-module (crates-io co vi covidtr) #:use-module (crates-io))

(define-public crate-covidtr-0.1.0 (c (n "covidtr") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "03mjfafnqcdjzmq54i5mi0rbay0s8jb2qars4vmvlnp6x40p1zsq")))

(define-public crate-covidtr-2.0.0 (c (n "covidtr") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.119") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "199dap3vhz2d3mshy6mvvhhlz4pjiysmj9hvrnm180z02z5dy6v8")))

