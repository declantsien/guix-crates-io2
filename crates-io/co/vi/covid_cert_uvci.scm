(define-module (crates-io co vi covid_cert_uvci) #:use-module (crates-io))

(define-public crate-covid_cert_uvci-0.1.0 (c (n "covid_cert_uvci") (v "0.1.0") (d (list (d (n "luhn-rs") (r "^0.0.1") (d #t) (k 0)))) (h "17k31bgpqhj9s4432g5kbcpx9m3ld2mqsq73bl5c37y9rzhkjvd6") (y #t)))

(define-public crate-covid_cert_uvci-0.2.0 (c (n "covid_cert_uvci") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "luhn-rs") (r "^0.0.1") (d #t) (k 0)))) (h "1m88wipjwg9ph5sx471psy26v0g5sf0rxxl9frh3sq3jsiswscsj")))

(define-public crate-covid_cert_uvci-0.2.1 (c (n "covid_cert_uvci") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "luhn-rs") (r "^0.0.1") (d #t) (k 0)))) (h "02sy8p17gvsbswfmnqr1pdxf2qqw32kap0arjby46q8yx7l1h91h")))

