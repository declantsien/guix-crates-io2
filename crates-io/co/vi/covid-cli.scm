(define-module (crates-io co vi covid-cli) #:use-module (crates-io))

(define-public crate-covid-cli-0.1.0 (c (n "covid-cli") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0bh99w28av36g3nnyw6pffjzvmdp8a3mnm0243djldn7fvcj8s54")))

