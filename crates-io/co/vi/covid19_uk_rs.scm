(define-module (crates-io co vi covid19_uk_rs) #:use-module (crates-io))

(define-public crate-covid19_uk_rs-1.0.0 (c (n "covid19_uk_rs") (v "1.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking" "gzip"))) (d #t) (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 0)))) (h "17c7y1ni1kz201apwxsv8smha9qcna014y8inzxkc4jlj3vlizb6")))

(define-public crate-covid19_uk_rs-1.0.1 (c (n "covid19_uk_rs") (v "1.0.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking" "gzip"))) (d #t) (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 0)))) (h "0nhg66pkwhmiwyw2qpw7gns7s13m1xdlrvz8pmpbs56kp2h5w9qv")))

(define-public crate-covid19_uk_rs-1.1.1 (c (n "covid19_uk_rs") (v "1.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking" "gzip"))) (d #t) (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 0)))) (h "19id7a20q8w2clvvndfyghasgfdr1kjpy3id1dnmh253zj0k6xk3")))

