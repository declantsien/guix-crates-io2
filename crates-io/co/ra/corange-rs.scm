(define-module (crates-io co ra corange-rs) #:use-module (crates-io))

(define-public crate-corange-rs-0.8.0 (c (n "corange-rs") (v "0.8.0") (d (list (d (n "gcc") (r "^0.3.20") (d #t) (k 1)) (d (n "gl") (r "^0.5.2") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.7.0") (d #t) (k 0)))) (h "1mig4d1hbjrc1cf7rm6f2i35q1vz2k7i76fq2nd33jy4ci6paf6n")))

