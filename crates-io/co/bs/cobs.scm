(define-module (crates-io co bs cobs) #:use-module (crates-io))

(define-public crate-cobs-0.1.1 (c (n "cobs") (v "0.1.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1a7dvg1f18ik4fd74ns3zfm5s2fbbx8ivq65grz9hjc2by1snlic")))

(define-public crate-cobs-0.1.2 (c (n "cobs") (v "0.1.2") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "19n7bn0sgxlvl5q75vjz7nq20w2b9ac3grdim80pcmixg88lan44")))

(define-public crate-cobs-0.1.3 (c (n "cobs") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)))) (h "1bblsbqvk6853zjsjqv8ywj5qi4a64q0z624r3297vy3bzyyvb4q") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-0.1.4 (c (n "cobs") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)))) (h "1xip86ifss0cqk8na5plpl6f7fygmxmmj79k1xxranbrzi2f8jy4") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-0.2.0 (c (n "cobs") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)))) (h "1sxrrxmvp6s4c8mvdw79dmwj83fzcjj6gxvkx2zqdk5q40vshqi7") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-0.2.1 (c (n "cobs") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)))) (h "1hnsdmz94bq15pnj3dqc5xcv78wh0bk7fjjg775367bw41gz7k0p") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-0.2.2 (c (n "cobs") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)))) (h "00g37zq8wi0p4xbk2bgx65sdf055gynbxj2y5n14010xy1jf6q3a") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-0.2.3 (c (n "cobs") (v "0.2.3") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)))) (h "05gd16mws4yd63h8jr3p08in8y8w21rpjp5jb55hzl9bgalh5fk7") (f (quote (("use_std") ("default" "use_std"))))))

