(define-module (crates-io co bs cobs-rs) #:use-module (crates-io))

(define-public crate-cobs-rs-1.0.0 (c (n "cobs-rs") (v "1.0.0") (h "0gp041msjfr6a06i97qbk6hwz3bcmxg2aqqg0b299bfln8v828jq")))

(define-public crate-cobs-rs-1.0.1 (c (n "cobs-rs") (v "1.0.1") (h "0xknr6ifn6hy7vgd0sm114p5y0qcsw85vya3p7i6b9sj6snx2y35")))

(define-public crate-cobs-rs-1.1.0 (c (n "cobs-rs") (v "1.1.0") (h "1qlvsd4vy2hyrgzxxj47qzqg41sfqaml9cyz5mfavd62sx72swsh")))

(define-public crate-cobs-rs-1.1.1 (c (n "cobs-rs") (v "1.1.1") (h "0x0mhif5kn2frj1axv4v854mq17xn7w6djdr2kf5k7md8z75gb5h")))

(define-public crate-cobs-rs-1.1.2 (c (n "cobs-rs") (v "1.1.2") (h "0czz6r3g9zl2fwj64kgpl7alcz44b40ks4g6vbp0grvrzqrmb1km")))

