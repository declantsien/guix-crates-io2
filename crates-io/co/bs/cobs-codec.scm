(define-module (crates-io co bs cobs-codec) #:use-module (crates-io))

(define-public crate-cobs-codec-0.1.0 (c (n "cobs-codec") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "macros"))) (k 2)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("codec"))) (d #t) (k 0)))) (h "0vvlm1s9z8q78fl53fwi1r5kamxxgd29rr3s549bsr9sbhlqs6yi")))

