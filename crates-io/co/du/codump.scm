(define-module (crates-io co du codump) #:use-module (crates-io))

(define-public crate-codump-0.1.0 (c (n "codump") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 2)))) (h "161gjb8f7i0qs5z4k6bdjg9zwg1mvc3z2i0h5kk7jbazvkp6i47h") (f (quote (("cli" "clap"))))))

(define-public crate-codump-0.1.1 (c (n "codump") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 2)))) (h "13xjpylqdyj19gz67w6p2hyn9q5c6kfdj9pdg53flddn791qyfhl") (f (quote (("cli" "clap"))))))

