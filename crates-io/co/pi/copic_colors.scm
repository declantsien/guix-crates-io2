(define-module (crates-io co pi copic_colors) #:use-module (crates-io))

(define-public crate-copic_colors-0.1.0 (c (n "copic_colors") (v "0.1.0") (h "1ss0rs8mryapxzajq3n644dv5qz8sfi3k12gir2yjdlnn29higpm")))

(define-public crate-copic_colors-0.1.1 (c (n "copic_colors") (v "0.1.1") (h "1np3s1f362xm1rms0lvxbmc9sz5z35330a8br74skrz2y0b7f69s")))

