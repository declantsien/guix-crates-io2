(define-module (crates-io co pi copie) #:use-module (crates-io))

(define-public crate-copie-0.1.0 (c (n "copie") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "130jizz0bz2z91yrr9w03dlmd4b1z8smcimhzxc9c7zwh4r5fcaz")))

(define-public crate-copie-0.2.0 (c (n "copie") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1byz4sdv7ac396xqjjpafcblkgg6xpcypf72mdn8sx6jdv9q1yia")))

