(define-module (crates-io co ws cowstr) #:use-module (crates-io))

(define-public crate-cowstr-0.1.0 (c (n "cowstr") (v "0.1.0") (h "0nqr5b2dzssgd6x98kz9nsdi0gbsx91dfr6hgbb538s1lqbrwrmg") (f (quote (("singlethreaded") ("default" "singlethreaded"))))))

(define-public crate-cowstr-0.1.1 (c (n "cowstr") (v "0.1.1") (h "191cdz0qn7y5hlgwidd674mxl8xpbw1v0nsmjlw07iqnrkzvdjpx") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.2.0 (c (n "cowstr") (v "0.2.0") (h "0bhwq6s24dd2apx6p6f6x3p3jd0s6xzxdcb24sndhwwkdjn8zh3d") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.3.0 (c (n "cowstr") (v "0.3.0") (h "0ca7wbbvydmmysqxlq0r47mlg6rygwkdhb3rz7wqzdw2il959mr5") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.4.0 (c (n "cowstr") (v "0.4.0") (h "1yg341ymazwzi32a1pihw5d6gdns2ql3fwxjgjqc7jd6256ngwqp") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.5.0 (c (n "cowstr") (v "0.5.0") (h "0p9c1shm8l16742gg6z7liwdvrswgnw2yjx3r5a23iwl0m8mc0yx") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.6.0 (c (n "cowstr") (v "0.6.0") (h "1zzyxfmg524vl8dr1ka9g2da93cf6xna96i5la5lm05m7aifbd6v") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.7.0 (c (n "cowstr") (v "0.7.0") (h "1h08pqa7zn1vbyxhqiza02wpbcsma9cyq0ck8r05xj14cycw6jg8") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.8.0 (c (n "cowstr") (v "0.8.0") (h "17a4zfarv221vqagyczg6lkiaqbiyqaqvxgpxpqacc8h1j7iizfc") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.8.1 (c (n "cowstr") (v "0.8.1") (h "128q7gpi6mlxf5ph6wijask08yfyqxs6m0xfir98dmm265gdq23j") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.8.2 (c (n "cowstr") (v "0.8.2") (h "0sxp64i1cqf1jqnvapqwzard3m0dc4w0107a8qxikkwk3drxvkc6") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.9.0 (c (n "cowstr") (v "0.9.0") (h "1cxw2pcrhzz6amzq5gd7ylq33i5g82crv8b559c6glaa2dbhrpxp") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.10.0 (c (n "cowstr") (v "0.10.0") (h "1h9yb78hnz86c13rf2wwqvdmmj28gmmczbp6gw6jxickv6x01m38") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.10.1 (c (n "cowstr") (v "0.10.1") (d (list (d (n "mutants") (r "^0.0.3") (d #t) (k 0)))) (h "1qhf8x2h74cw0ap37a7nd8kx3jrkacbhgw7li6f6r34cxvb092ij") (f (quote (("singlethreaded") ("default"))))))

(define-public crate-cowstr-0.11.0 (c (n "cowstr") (v "0.11.0") (d (list (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "0xd6vj6c11sby6ig04ipa990c00x763v8li6998r19mhkrhmgpzq") (f (quote (("default" "multithreaded")))) (s 2) (e (quote (("multithreaded" "dep:parking_lot"))))))

(define-public crate-cowstr-0.11.1 (c (n "cowstr") (v "0.11.1") (d (list (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "08wyqnvvw0f1zmh9m803rbw175wflj4x0sv12nq65wn9q4g8waqk") (f (quote (("default" "multithreaded")))) (s 2) (e (quote (("multithreaded" "dep:parking_lot"))))))

(define-public crate-cowstr-0.12.0 (c (n "cowstr") (v "0.12.0") (d (list (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "03mgibqjx5598g22281qq691xqjfmg80nsv5sggcggw15k44p9yg") (f (quote (("default" "multithreaded")))) (s 2) (e (quote (("multithreaded" "dep:parking_lot"))))))

(define-public crate-cowstr-0.13.0 (c (n "cowstr") (v "0.13.0") (d (list (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "threadcell") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "1px4ajz1zhik5wphp5k2sa7i9bjdalh4wikc050x3qy6dy1xbblz") (f (quote (("default" "multithreaded")))) (s 2) (e (quote (("multithreaded" "dep:threadcell"))))))

(define-public crate-cowstr-1.0.0-beta1 (c (n "cowstr") (v "1.0.0-beta1") (d (list (d (n "conf_test") (r "^0.3") (d #t) (k 1)) (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "threadcell") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "07jlkmbd0jiprmjsqwp0qfwldypg131k47a6ql201ynbqsv60ak3") (f (quote (("nightly_int_roundings") ("default" "multithreaded")))) (s 2) (e (quote (("multithreaded" "dep:threadcell"))))))

(define-public crate-cowstr-1.0.0-beta2 (c (n "cowstr") (v "1.0.0-beta2") (d (list (d (n "conf_test") (r "^0.3") (d #t) (k 1)) (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)) (d (n "threadcell") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1vsxjwpyy68g5vyxshzhkwa3ywg98s3fni7p164kml8y69dsvi7n") (f (quote (("nightly_string_remove_matches") ("nightly_int_roundings") ("nightly_extend_one") ("nightly_assert_matches") ("default" "multithreaded")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test") ("multithreaded" "dep:threadcell"))))))

(define-public crate-cowstr-1.0.0-beta3 (c (n "cowstr") (v "1.0.0-beta3") (d (list (d (n "conf_test") (r "^0.4") (d #t) (k 1)) (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)) (d (n "threadcell") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "1qqva1q3f9fm31w5hpiq9jm6nlr3h6jdlyqp7p747hckymfc5cv3") (f (quote (("nightly_string_remove_matches") ("nightly_int_roundings") ("nightly_extend_one") ("nightly_assert_matches") ("default" "multithreaded")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test") ("multithreaded" "dep:threadcell"))))))

(define-public crate-cowstr-1.0.0-beta4 (c (n "cowstr") (v "1.0.0-beta4") (d (list (d (n "conf_test") (r "^0.5") (d #t) (k 1)) (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)) (d (n "threadcell") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "0a1d5bd07j1dfl6c9pp680fvb4nhirrjjxmj8i0g292p28yd3g8i") (f (quote (("nightly_string_remove_matches") ("nightly_int_roundings") ("nightly_extend_one") ("nightly_assert_matches") ("default" "multithreaded")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test") ("multithreaded" "dep:threadcell"))))))

(define-public crate-cowstr-1.0.0-beta5 (c (n "cowstr") (v "1.0.0-beta5") (d (list (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)) (d (n "threadcell") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "0vbhg7c7vmfwfrdfvdgxab88cig75i19r911ypkfwqzxwnl4klq3") (f (quote (("nightly_int_roundings") ("nightly_extend_one") ("nightly" "nightly_int_roundings" "nightly_extend_one") ("default" "multithreaded")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test") ("multithreaded" "dep:threadcell"))))))

(define-public crate-cowstr-1.0.0-beta6 (c (n "cowstr") (v "1.0.0-beta6") (d (list (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)) (d (n "threadcell") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "0z98mqkigrdly7lb7s4vyxcpzaribj9rnx4q82w0q44a5haaa5hb") (f (quote (("nightly_int_roundings") ("nightly_extend_one") ("nightly" "nightly_int_roundings" "nightly_extend_one") ("default" "multithreaded")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test") ("multithreaded" "dep:threadcell"))))))

(define-public crate-cowstr-1.0.0-beta7 (c (n "cowstr") (v "1.0.0-beta7") (d (list (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)) (d (n "threadcell") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "1a6jsgsalfa6srlh40hjyz00npwdkmlbjrha703v2r5cg81aabj3") (f (quote (("nightly_int_roundings") ("nightly_extend_one") ("nightly" "nightly_int_roundings" "nightly_extend_one") ("default" "multithreaded")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test") ("multithreaded" "dep:threadcell"))))))

(define-public crate-cowstr-1.0.0-beta8 (c (n "cowstr") (v "1.0.0-beta8") (d (list (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)))) (h "1jck954svzycc565qgk4z2jd65mc6awagzvnk10vnqlz8hdl1sx0") (f (quote (("nightly_int_roundings") ("nightly_extend_one") ("nightly" "nightly_int_roundings" "nightly_extend_one") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test"))))))

(define-public crate-cowstr-1.0.0-beta9 (c (n "cowstr") (v "1.0.0-beta9") (d (list (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)))) (h "15kxfkszqpnyb8xvsmg2v8khv0iw4fnwpz2j7p1mz1vj166k7986") (f (quote (("nightly_int_roundings") ("nightly_fmt_internals") ("nightly_extend_one") ("nightly" "nightly_int_roundings" "nightly_extend_one" "nightly_fmt_internals") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test"))))))

(define-public crate-cowstr-1.0.0 (c (n "cowstr") (v "1.0.0") (d (list (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)))) (h "13p95hsnn17z82kh3az6pa6gp4lc2xxkc11626kv0ji435ajdklx") (f (quote (("nightly_int_roundings") ("nightly_fmt_internals") ("nightly_extend_one") ("nightly" "nightly_int_roundings" "nightly_extend_one" "nightly_fmt_internals") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test"))))))

(define-public crate-cowstr-1.1.0 (c (n "cowstr") (v "1.1.0") (d (list (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)))) (h "0gqfkp2l77vcwrfpdj97hg2ljr55bfz6lhc04m19r93ki61pqhbr") (f (quote (("nightly_int_roundings") ("nightly_fmt_internals") ("nightly_extend_one") ("nightly" "nightly_int_roundings" "nightly_extend_one" "nightly_fmt_internals") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test"))))))

(define-public crate-cowstr-1.2.0 (c (n "cowstr") (v "1.2.0") (d (list (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)))) (h "1sfy9wil9h4psanmmxsb0bqz9kz4b5y3ymla9m97ssz3h9rg1z8j") (f (quote (("nightly_int_roundings") ("nightly_fmt_internals") ("nightly_extend_one") ("nightly" "nightly_int_roundings" "nightly_extend_one" "nightly_fmt_internals") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test"))))))

(define-public crate-cowstr-1.3.0 (c (n "cowstr") (v "1.3.0") (d (list (d (n "code-product") (r "^0.3.0") (d #t) (k 0)) (d (n "constptr") (r "^0.2.0") (d #t) (k 0)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)))) (h "07krgjpq42jsm05b65n4b4grj0gwgp6z9dc1qp0317xj2rjf8gb1") (f (quote (("nightly_int_roundings") ("nightly_fmt_internals") ("nightly_extend_one") ("nightly" "nightly_int_roundings" "nightly_extend_one" "nightly_fmt_internals") ("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test")))) (r "1.70.0")))

