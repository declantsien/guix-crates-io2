(define-module (crates-io co ws cowsay) #:use-module (crates-io))

(define-public crate-cowsay-0.1.0 (c (n "cowsay") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "186qg7g58fgmqzknsc3rd6k27lm628dh52aah5k7fvdbg5mwymqw")))

(define-public crate-cowsay-0.2.0 (c (n "cowsay") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1s54n84x48l5668n6qx9mgkm26gjnw1nvdbdwdvca0v21a8lg5hg")))

(define-public crate-cowsay-0.3.0 (c (n "cowsay") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1dbyy29498x7jvyplpylhibj35z87hvz17974pasdlrxrm40zm6n")))

(define-public crate-cowsay-0.4.0 (c (n "cowsay") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1rnxb5hddf6hk1k7lim0g47j31s6rxv05shs428w1l686i28db9y")))

(define-public crate-cowsay-0.10.0 (c (n "cowsay") (v "0.10.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0s8336ah88hvmppb2akr90nmsp6js4jrh4z9mg6v912pppvk18z7")))

(define-public crate-cowsay-0.14.0 (c (n "cowsay") (v "0.14.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1wz934b2rvik8qbz8lm98kn6wwx6s7frp31r2gfnjlyddn38p3d2")))

