(define-module (crates-io co np conpty) #:use-module (crates-io))

(define-public crate-conpty-0.1.0 (c (n "conpty") (v "0.1.0") (d (list (d (n "windows") (r "^0.19.0") (d #t) (k 0)) (d (n "windows") (r "^0.19.0") (d #t) (k 1)))) (h "0w198i8wgsz1czx01lyfwyagw6nrmznafac25fbs00v2cabsvd4d")))

(define-public crate-conpty-0.1.1 (c (n "conpty") (v "0.1.1") (d (list (d (n "windows") (r "^0.19.0") (d #t) (k 0)) (d (n "windows") (r "^0.19.0") (d #t) (k 1)))) (h "17sip26ms6hpbwfix4scrcsi84m25b75ajkmjcq1ss23ibl448fg")))

(define-public crate-conpty-0.2.0 (c (n "conpty") (v "0.2.0") (d (list (d (n "conpty-bindings") (r "^0.0.0") (d #t) (k 0)) (d (n "windows") (r "^0.19.0") (d #t) (k 0)))) (h "09bsr8rzkyb48xnaayi546d2dg54spa14m83fpfsqy1ygfisfc8v")))

(define-public crate-conpty-0.2.1 (c (n "conpty") (v "0.2.1") (d (list (d (n "windows") (r "^0.19.0") (d #t) (k 0)) (d (n "windows") (r "^0.19.0") (d #t) (k 1)))) (h "0c3vlf526f609l17bppj69wjql9iamadqjb9a54q0nagm3a6nc0v")))

(define-public crate-conpty-0.3.0 (c (n "conpty") (v "0.3.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 2)) (d (n "windows") (r "^0.29.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Console" "Win32_System_Pipes" "Win32_System_SystemServices" "Win32_System_WindowsProgramming" "Win32_System_IO" "Win32_Storage_FileSystem"))) (d #t) (k 0)))) (h "0m27fflp74g0vi1gg6c7r7dbjypd9bmsi839pgwxfwv20bjalywp")))

(define-public crate-conpty-0.4.0 (c (n "conpty") (v "0.4.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 2)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Console" "Win32_System_Pipes" "Win32_System_SystemServices" "Win32_System_WindowsProgramming" "Win32_System_IO" "Win32_Storage_FileSystem"))) (d #t) (k 0)))) (h "0684fv8zvgdpvn3f681qzvnr6n8xd0gwnz68hx0djwxihx6mv09b")))

(define-public crate-conpty-0.5.0 (c (n "conpty") (v "0.5.0") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Console" "Win32_System_Pipes" "Win32_System_SystemServices" "Win32_System_WindowsProgramming" "Win32_System_IO" "Win32_Storage_FileSystem"))) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 2)))) (h "07lm6cm1bm1p7j3yl6m70nxc1ra91yfycy9zsjav97yjh2x9s6vi")))

(define-public crate-conpty-0.5.1 (c (n "conpty") (v "0.5.1") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Console" "Win32_System_Pipes" "Win32_System_SystemServices" "Win32_System_WindowsProgramming" "Win32_System_IO" "Win32_Storage_FileSystem"))) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 2)))) (h "1nmkhiysnssvbi4kqaq8cybb0ffngbl64kfpk8s86ihdg940caxp")))

