(define-module (crates-io co nj conjure-error) #:use-module (crates-io))

(define-public crate-conjure-error-0.3.1 (c (n "conjure-error") (v "0.3.1") (d (list (d (n "conjure-object") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1zzdh1yl26fmy0b1w1r304299d5jhssr1jizig3nbispk7h2ypbz")))

(define-public crate-conjure-error-0.3.2 (c (n "conjure-error") (v "0.3.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.3.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0b0kpv0dpz4ard5lzrnwndnzxxka7664cvpv8j68gng9sw0smh70")))

(define-public crate-conjure-error-0.3.3 (c (n "conjure-error") (v "0.3.3") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.3.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jw06ssqmqxxl4lnfm97i63v5iv4dwb6rx4f58smd573c1wvpmvm")))

(define-public crate-conjure-error-0.3.4 (c (n "conjure-error") (v "0.3.4") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.3.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rpn98ggdq0l5pq1hp3qicq5zf5kd8h2yxa24j7xv567f81v0d84")))

(define-public crate-conjure-error-0.3.5 (c (n "conjure-error") (v "0.3.5") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jbdy82cl14b08i28cvnrdr2idbvn7iwgh1yzlbncd4vz1m2f87c")))

(define-public crate-conjure-error-0.3.6 (c (n "conjure-error") (v "0.3.6") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.3.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jm8547zs620dpr8y8wgg7q3rfkgkwagk5mcafp2iz3pn2zbkjgg")))

(define-public crate-conjure-error-0.3.7 (c (n "conjure-error") (v "0.3.7") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.3.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ppv2gl7j6jwqf5jcc6d1nqxyl50xiwzy3c2ihaxmsmdnphbr0rb")))

(define-public crate-conjure-error-0.3.8 (c (n "conjure-error") (v "0.3.8") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.3.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1f1a73lllj2cndnql2vax6xnj33x24c7wik1x8vd0haf4vr9w5r9")))

(define-public crate-conjure-error-0.3.9 (c (n "conjure-error") (v "0.3.9") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.3.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bn1qa5p4mmc7knicxm5dn0k52z8zibq7qyg9kmsdbxj60q1fsl2")))

(define-public crate-conjure-error-0.3.10 (c (n "conjure-error") (v "0.3.10") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.3.10") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "17yflmirdrm0plvccdnvnysbyj9kyyh4yvypy1y729357qzb19kd")))

(define-public crate-conjure-error-0.4.0 (c (n "conjure-error") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "04k3vi25b5dwvx0h7m2kpnad0lgh45b76nk04gyvccgx1dkhvnci")))

(define-public crate-conjure-error-0.4.1 (c (n "conjure-error") (v "0.4.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.4.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m2ih63dm8abbxdk61xma4f49hvxpp76fzg5mzacmyib2zx26vy0")))

(define-public crate-conjure-error-0.4.2 (c (n "conjure-error") (v "0.4.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.4.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "000dzfc5d2aryw4naws3znhpj7lhjdh3m3nx6xqsrhprzcc965kb")))

(define-public crate-conjure-error-0.4.3 (c (n "conjure-error") (v "0.4.3") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.4.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jfphslypf57v9242m4qiwcz129mgypjf5iva62wx4bvgmygpmn2")))

(define-public crate-conjure-error-0.4.4 (c (n "conjure-error") (v "0.4.4") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.4.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "05f9cl7dnps1kvqll67imn7qs1criwacrmh9hz9ia230s6jl7ys7")))

(define-public crate-conjure-error-0.4.5 (c (n "conjure-error") (v "0.4.5") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.4.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ixidjv1llday3xbxq1fa7112qf2ydfh0qvk7s5w5vmdhj75dbv6")))

(define-public crate-conjure-error-0.4.6 (c (n "conjure-error") (v "0.4.6") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.4.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1grslvgx4zwqy39lsjam31s0pjjwxiwyfdv55rhfy1f33chzg62b")))

(define-public crate-conjure-error-0.5.0 (c (n "conjure-error") (v "0.5.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.5.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "160jpf7gb9v8hlkgbsfvs05jpd8mm2vw7gw9ikknm4awwg4i0bfj")))

(define-public crate-conjure-error-0.6.0 (c (n "conjure-error") (v "0.6.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.6.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0x27pyzlg0sin6qhgk9zix23isfgl3k51l6fg0ia09v0c0np11wk")))

(define-public crate-conjure-error-0.6.1 (c (n "conjure-error") (v "0.6.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.6.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "059nk3m498nlgkqjjgdr7z3ac1xrn771r9f1nlis66f9cbqqs3f9")))

(define-public crate-conjure-error-0.7.0 (c (n "conjure-error") (v "0.7.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "02hcnhf14j74sbx5ag79rdxbz85bqlc7cdl2shzlnf4qz2y1k1hd")))

(define-public crate-conjure-error-0.7.1 (c (n "conjure-error") (v "0.7.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.7.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ks41bwrh4x8cyf3rmfpvc2ynyww4fdbs6z9qcvxp06jgp7xsw30")))

(define-public crate-conjure-error-0.7.2 (c (n "conjure-error") (v "0.7.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.7.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1pxdjkgaxxy63ifhdyziyjfbvs5vpvdj4gh8dpz3nca59dyljrvm")))

(define-public crate-conjure-error-0.7.3 (c (n "conjure-error") (v "0.7.3") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.7.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ah2g20bn3l09036hvz5z7p2d0p7mksmm9cg018fhy1pl43hlldh")))

(define-public crate-conjure-error-0.7.4 (c (n "conjure-error") (v "0.7.4") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^0.7.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vpc4fsl3224ssd5qgyy3808ssqkn39ink4dhjy9vfxy5q34r357")))

(define-public crate-conjure-error-1.0.0 (c (n "conjure-error") (v "1.0.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^1.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1g5ihxhsi7mcgw6dznkibxaa9f8qm2cyffrlxarv8d3h9fvyrmki")))

(define-public crate-conjure-error-1.1.0 (c (n "conjure-error") (v "1.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^1.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "05ca3bz7chn67an0sgz5rgq9w3wd8q1fbcj2my1kwk3zia7wkx74")))

(define-public crate-conjure-error-2.0.0 (c (n "conjure-error") (v "2.0.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^2.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wzra6v187j12dasykd10vrlvj01ji9v0rr2vdnwfnvjwj3iih3z")))

(define-public crate-conjure-error-3.0.0 (c (n "conjure-error") (v "3.0.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^3.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "03v2m0i0vii1xx3wmxlw5bbxf5zjzwxbzwwvk9689j4i7wl3n12p")))

(define-public crate-conjure-error-3.1.0 (c (n "conjure-error") (v "3.1.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^3.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0kxi3rgf9bws3z23r393i86pkr30drkk9j88zg9hx5c8sj01pv66")))

(define-public crate-conjure-error-3.2.0 (c (n "conjure-error") (v "3.2.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "conjure-object") (r "^3.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0amrhlfis47al28crkv7v7j5f3mxlz9hs787cl8sq2v2g27wyg4w")))

(define-public crate-conjure-error-3.3.0 (c (n "conjure-error") (v "3.3.0") (d (list (d (n "conjure-object") (r "^3.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0zaqglw172baz1dga89bnvsjql2r1jk6l0iifj97d3vjj30nwy71")))

(define-public crate-conjure-error-3.4.0 (c (n "conjure-error") (v "3.4.0") (d (list (d (n "conjure-object") (r "^3.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0lprhc8nsb9m2lz4k02pr6si3n5k524a2abg002464gzlz0k5r7z")))

(define-public crate-conjure-error-3.5.0 (c (n "conjure-error") (v "3.5.0") (d (list (d (n "conjure-object") (r "^3.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1s916ykkizpb2vkf8cdvvhf8wxvjijivgyz0fgs4wk39vbnvi7gm")))

(define-public crate-conjure-error-3.6.0 (c (n "conjure-error") (v "3.6.0") (d (list (d (n "conjure-object") (r "^3.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1a45rpm4q9wanryz43ap22dcjgkpn4smfxj1cg5ym2bxagqcp5p9")))

(define-public crate-conjure-error-3.6.1 (c (n "conjure-error") (v "3.6.1") (d (list (d (n "conjure-object") (r "^3.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cnjxqngjsy1sfb96r8icg84n53cmd33j1mq9l8yal6d1fryddg6")))

(define-public crate-conjure-error-3.6.2 (c (n "conjure-error") (v "3.6.2") (d (list (d (n "conjure-object") (r "^3.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ng4vr2wswjvxinwp3hdgk7p01baf3h7fy6v7ah246dwi0jvhi3s")))

(define-public crate-conjure-error-3.6.3 (c (n "conjure-error") (v "3.6.3") (d (list (d (n "conjure-object") (r "^3.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0z00qb65qr2ibqdqxrhq8dbnbbn0d542mf63ky03smiwkwsschbk")))

(define-public crate-conjure-error-3.6.4 (c (n "conjure-error") (v "3.6.4") (d (list (d (n "conjure-object") (r "^3.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "03kdsz3c7sk6yg2dagy18xgdgljkqrp7v53ikk31nmfbdai715rd")))

(define-public crate-conjure-error-3.6.5 (c (n "conjure-error") (v "3.6.5") (d (list (d (n "conjure-object") (r "^3.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1sqy5b5sdpp3gj10y5grpb4pz08n2dssca7fcrhkmxm5nc27hq4j")))

(define-public crate-conjure-error-3.6.6 (c (n "conjure-error") (v "3.6.6") (d (list (d (n "conjure-object") (r "^3.6.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ngj31k0ixnaa87r718mkcx9rnzrjp6p53s64a13w0gs4gm3k7gy")))

(define-public crate-conjure-error-4.0.0-rc3 (c (n "conjure-error") (v "4.0.0-rc3") (d (list (d (n "conjure-object") (r "^4.0.0-rc3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1b6fhw0s9azljdqdbjijiacn6l2jzx2iklply78rh741pdg7l1pr")))

(define-public crate-conjure-error-4.0.0-rc4 (c (n "conjure-error") (v "4.0.0-rc4") (d (list (d (n "conjure-object") (r "^4.0.0-rc4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1c529hx80l0r7dk81f8bzlz832nychqjlyqxw5rsb6krg9w3r1q1")))

