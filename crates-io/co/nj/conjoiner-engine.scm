(define-module (crates-io co nj conjoiner-engine) #:use-module (crates-io))

(define-public crate-conjoiner-engine-1.0.0 (c (n "conjoiner-engine") (v "1.0.0") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "postcard") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p58in7dwfrqi2w5qqv86gsdra7b9hfzm890rhxnq6yhgd5ff773")))

(define-public crate-conjoiner-engine-1.1.0 (c (n "conjoiner-engine") (v "1.1.0") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "postcard") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vzqrjm9c7zafmmcz680gg463brgkysyagswvbh3q8rpddr3a8di")))

(define-public crate-conjoiner-engine-1.2.0 (c (n "conjoiner-engine") (v "1.2.0") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "postcard") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1awxq9bjah0jcnx5wgiqzdi2g8rxlchzr68rlihz3xrr9xl0m2jv")))

(define-public crate-conjoiner-engine-1.2.1 (c (n "conjoiner-engine") (v "1.2.1") (d (list (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "postcard") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1niy2wpqdaq2v75568zn1qmfpr1mwq24hpw2sjqa95m8f4h632dg")))

(define-public crate-conjoiner-engine-1.2.2 (c (n "conjoiner-engine") (v "1.2.2") (d (list (d (n "postcard") (r "^0.4") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y79riihvlm5kij3qx7d0qq0dcb347xhwn9y5igvz1z5dpd1y0ms")))

(define-public crate-conjoiner-engine-1.2.3 (c (n "conjoiner-engine") (v "1.2.3") (d (list (d (n "postcard") (r "^0.4") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zbxjql7wjfnrwfaqbbc9nrb36azvs77vccg92rprz1015rx7av0")))

