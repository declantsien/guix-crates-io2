(define-module (crates-io co nj conjecture) #:use-module (crates-io))

(define-public crate-conjecture-0.1.0 (c (n "conjecture") (v "0.1.0") (h "0rbgak8aqa1dmfgg5r1a1a914yj7rm2340whg7igchkm5gfnps7g")))

(define-public crate-conjecture-0.1.1 (c (n "conjecture") (v "0.1.1") (h "1xmbd5lz1a02rl3cqkig98s03hc3bpzkzpraciv5gw3yykjrq1zg")))

(define-public crate-conjecture-0.2.0 (c (n "conjecture") (v "0.2.0") (d (list (d (n "conjecture") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "133f31x8x43hh06hv7dhclj9gn1gm5kpv8v8337syd746zsbr41r")))

(define-public crate-conjecture-0.2.1 (c (n "conjecture") (v "0.2.1") (d (list (d (n "conjecture") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "17drpxbmwf54qp2hid9hkv4n82ipgrl8z5n86s3b6gca18ljlard")))

(define-public crate-conjecture-0.3.0 (c (n "conjecture") (v "0.3.0") (d (list (d (n "conjecture") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1ib7248722hh0djg0lfwj88avhc35l52hab7cly8m3r84lfz6pzi")))

(define-public crate-conjecture-0.4.0 (c (n "conjecture") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "110015qa9qwqbac5dyd2s68sr7pw73ynnsahvr3j02wnnlb3dnp9")))

(define-public crate-conjecture-0.6.0 (c (n "conjecture") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1wgadyybs73l2pi3cp7v393cbm3by3cqbazqq6ynb4gns86dhi5h")))

(define-public crate-conjecture-0.7.0 (c (n "conjecture") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0nxid2248ydaw78yz4v6c5x9ma0iawp86slhqjqklqrgabzkfjpn")))

