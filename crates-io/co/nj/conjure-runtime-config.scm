(define-module (crates-io co nj conjure-runtime-config) #:use-module (crates-io))

(define-public crate-conjure-runtime-config-0.1.0 (c (n "conjure-runtime-config") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "16nn4114vqfj56gz3ayhsd2x4m5jyaav5cvzw4rqszqkbr2ny1js")))

(define-public crate-conjure-runtime-config-0.1.1 (c (n "conjure-runtime-config") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0k5rsf09ckypcd9n9wj3p0hwb9mh7rpx8vi9w73p2qz1qrr6a3y6")))

(define-public crate-conjure-runtime-config-0.1.2 (c (n "conjure-runtime-config") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0vx6iysjjngvlxgw243207lw8gx1r0wzf0crl5l0c53c4jzabzz0")))

(define-public crate-conjure-runtime-config-0.2.0 (c (n "conjure-runtime-config") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0cp1ycs60fsqlfnwp5qypfhbs71g5rp1m7jkhx9a6cqr4n1lx10y")))

(define-public crate-conjure-runtime-config-0.2.1 (c (n "conjure-runtime-config") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "12bax6l288ddxiq6sj9n78nqkhi7jn8hmwpbjz0ck8xv9wkzqykf")))

(define-public crate-conjure-runtime-config-0.2.2 (c (n "conjure-runtime-config") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0z60v8v43csc40vnbxn3k5lpxhzxm7hg3pdf3npyqhr08k6dr7g1")))

(define-public crate-conjure-runtime-config-0.3.0 (c (n "conjure-runtime-config") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1xjjxsx9dqk5h97hfdcwhsdazxgm4q427pbkvlgff7312farigjh")))

(define-public crate-conjure-runtime-config-0.3.1 (c (n "conjure-runtime-config") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0j0fcdb1zi2sn4vsi5lkjb99sr96y8k1hd5yc6aahbgaqmz4lxp0")))

(define-public crate-conjure-runtime-config-0.4.0 (c (n "conjure-runtime-config") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0hj7fd0bfck64b7nacb3iisswk46im87sg7fpzs9j1yrfb89f2r5")))

(define-public crate-conjure-runtime-config-0.4.1 (c (n "conjure-runtime-config") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0bzlhfhv1fcl35pp6wrnyzp4zwn72cmq7rv5ys0y448aaia9vj8s")))

(define-public crate-conjure-runtime-config-0.4.2 (c (n "conjure-runtime-config") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1kdimjwirk2jb00f4v8lsy0cid2n56x171llgxy227h9aiyvaxsg")))

(define-public crate-conjure-runtime-config-1.0.0 (c (n "conjure-runtime-config") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-humantime") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1vaqxih6ygi8h63341mflddmv4wnxg18wy3k504nqjk7qfdjzjmg")))

(define-public crate-conjure-runtime-config-1.1.0 (c (n "conjure-runtime-config") (v "1.1.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "01b8l0qv0id76wg7vmqmvfmx4bd3n3yynly5d7ksdyi52m784inh")))

(define-public crate-conjure-runtime-config-1.2.0 (c (n "conjure-runtime-config") (v "1.2.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0mmgsyqfz3jjbg6dwzadyb6ap9n2ihpfsifs5ycya45h9hgpv97a")))

(define-public crate-conjure-runtime-config-2.0.0 (c (n "conjure-runtime-config") (v "2.0.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "033kh5djbgdphhvbwklglkpkf1wyskl6zfm9rzphfp4bz82p2v3p")))

(define-public crate-conjure-runtime-config-2.1.0 (c (n "conjure-runtime-config") (v "2.1.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "197wc95vfcj5kg5k5sx9lkb7yvpf489xj5j8g9bsbi6sajp2cwaa")))

(define-public crate-conjure-runtime-config-3.0.0 (c (n "conjure-runtime-config") (v "3.0.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0mdg9npjyn3fydbrhqm722999raiqcwvck5idvh5iqvspb33mmc2")))

(define-public crate-conjure-runtime-config-3.1.0 (c (n "conjure-runtime-config") (v "3.1.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0bza904pa06h4k3aryx5azijgl8dsla50rf0sp768lvx70gjcjdf")))

(define-public crate-conjure-runtime-config-4.0.0 (c (n "conjure-runtime-config") (v "4.0.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1h4h0kldr865yq8d0r6w9kd13bxkng36dznrrk2ipr7c41wdy33s")))

(define-public crate-conjure-runtime-config-4.1.0 (c (n "conjure-runtime-config") (v "4.1.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0416ym4860cq2cgjg2cnnldr2clc6n3bbllyqyf3npjablq8il5l")))

(define-public crate-conjure-runtime-config-4.2.0 (c (n "conjure-runtime-config") (v "4.2.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "09ld896qhjkgi9d7mq1d4gqh62x54m4mw2wsasrg9cg90vwz7hwk")))

(define-public crate-conjure-runtime-config-4.3.0 (c (n "conjure-runtime-config") (v "4.3.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "06z6pnkqbz1s6j40vh9l2bxmjs734rq1bnbwz6mqs8k5xwl97w33")))

(define-public crate-conjure-runtime-config-4.4.0 (c (n "conjure-runtime-config") (v "4.4.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0p77zc6mhx7fnal7yh9356yyk1p0vwlxy1v0hzc82b37apranizz")))

(define-public crate-conjure-runtime-config-4.5.0 (c (n "conjure-runtime-config") (v "4.5.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1wmig13b58w2w0frccwr2b5zw885m70wiy9y9rc3qwh75a0qp4q8")))

(define-public crate-conjure-runtime-config-4.7.0 (c (n "conjure-runtime-config") (v "4.7.0") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "10h1lwfjdl90kchcyr7r09l74405hhbhb7b1hc8j3ychjcd6wv3p")))

(define-public crate-conjure-runtime-config-4.7.1 (c (n "conjure-runtime-config") (v "4.7.1") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0g2r8l57q9w7rssfhk9hha310814bjar3njl1mlj7rjskz8abfk2")))

(define-public crate-conjure-runtime-config-5.0.0-rc1 (c (n "conjure-runtime-config") (v "5.0.0-rc1") (d (list (d (n "humantime-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "staged-builder") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0pjp6skq5bja4xf3rvjsd2071gd8wpzjr10wx4ynab8zzi1indsc")))

