(define-module (crates-io co bw cobweb) #:use-module (crates-io))

(define-public crate-cobweb-0.1.0 (c (n "cobweb") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "keybob") (r "^0.3.1") (d #t) (k 0)) (d (n "miscreant") (r "^0.4.2") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)) (d (n "tun-tap") (r "^0.1.1") (d #t) (k 0)))) (h "1z2vgrxs2lmmb7c4b6spfbj9sz7yn92afcz9g5dp4pd0lkl8v1v6")))

(define-public crate-cobweb-0.1.1 (c (n "cobweb") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "keybob") (r "^0.3.1") (d #t) (k 0)) (d (n "miscreant") (r "^0.4.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)) (d (n "tun-tap") (r "^0.1.1") (d #t) (k 0)) (d (n "tun-tap-mac") (r "^0.1.2") (d #t) (k 0)))) (h "196lrv7wbs0wwkqbmcdl5flkbb9xmig1faam3lzpxp80j0s7jlyl")))

(define-public crate-cobweb-0.1.2 (c (n "cobweb") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "keybob") (r "^0.3.1") (d #t) (k 0)) (d (n "miscreant") (r "^0.4.2") (f (quote ("soft-aes"))) (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)) (d (n "tun-tap") (r "^0.1.1") (d #t) (k 0)) (d (n "tun-tap-mac") (r "^0.1.2") (d #t) (k 0)))) (h "15ndjr02j7ylzfn32cn9cdcchlns1r7vdqrwr1l6lnb0ns0a80np")))

(define-public crate-cobweb-0.2.0 (c (n "cobweb") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "keybob") (r "^0.3.1") (d #t) (k 0)) (d (n "miscreant") (r "^0.4.2") (f (quote ("soft-aes"))) (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)) (d (n "tun-tap") (r "^0.1.1") (d #t) (k 0)) (d (n "tun-tap-mac") (r "^0.1.2") (d #t) (k 0)))) (h "1y8v6lv91bkhmm6wjbf68j4dbdfga79aa3w3ikmg7pd9k210s75n")))

(define-public crate-cobweb-0.2.1 (c (n "cobweb") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "keybob") (r "^0.3.1") (d #t) (k 0)) (d (n "miscreant") (r "^0.4.2") (f (quote ("soft-aes"))) (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)) (d (n "tun-tap") (r "^0.1.1") (d #t) (k 0)) (d (n "tun-tap-mac") (r "^0.1.2") (d #t) (k 0)))) (h "0787k9x7r08k0f83pknzfrpdjsxsrj6qk3sf5kw13b2y2hpipbfb")))

(define-public crate-cobweb-0.2.2 (c (n "cobweb") (v "0.2.2") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "keybob") (r "^0.3.1") (d #t) (k 0)) (d (n "miscreant") (r "^0.4.2") (f (quote ("soft-aes"))) (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)) (d (n "tun-tap") (r "^0.1.1") (d #t) (k 0)) (d (n "tun-tap-mac") (r "^0.1.2") (d #t) (k 0)))) (h "01ksz8fz85x7lfkwifysqj1szl0vblmqy914vxdb31chrcmwrd1k")))

