(define-module (crates-io co bh cobhan) #:use-module (crates-io))

(define-public crate-cobhan-0.1.0 (c (n "cobhan") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0y3z79y1cwviqfxljb2m9gc33rgh5qylldma9202kr8ag8if2y7p") (f (quote (("cobhan_debug"))))))

(define-public crate-cobhan-0.1.1 (c (n "cobhan") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1bxnsgg041rh49alxmsh9l8bnd6g7d1vgvz0s0fmaqivdql9nm6v") (f (quote (("cobhan_debug"))))))

