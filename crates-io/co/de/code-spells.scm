(define-module (crates-io co de code-spells) #:use-module (crates-io))

(define-public crate-code-spells-0.1.0 (c (n "code-spells") (v "0.1.0") (h "0s9m8m1mmzzr2v6bkqn43chga32d1qyrfzfr60kzza2jxywhc09f")))

(define-public crate-code-spells-0.2.0 (c (n "code-spells") (v "0.2.0") (h "1bdd15ljwlrbvh2bfv4322kkd931mfb7qw6b3hby5wcz2iyfkkb8")))

(define-public crate-code-spells-0.2.1 (c (n "code-spells") (v "0.2.1") (h "1b24jap5b6a3pic78k2mf7i6kwxs5gl58932yn8ikr97r0crwziq")))

(define-public crate-code-spells-0.2.2 (c (n "code-spells") (v "0.2.2") (h "0pvsr89mlr8yww7mgc8a6qkh84gqhfl6gq4vjrk0g0fy41k4m3k0")))

