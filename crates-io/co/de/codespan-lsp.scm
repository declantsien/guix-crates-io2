(define-module (crates-io co de codespan-lsp) #:use-module (crates-io))

(define-public crate-codespan-lsp-0.1.0 (c (n "codespan-lsp") (v "0.1.0") (d (list (d (n "codespan") (r "^0.1.2") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "languageserver-types") (r "^0.39.0") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "15yr2sai8mflgzjv0kq2n0qyr87s8s7xk30hym0bxzibn33an7pf")))

(define-public crate-codespan-lsp-0.2.0 (c (n "codespan-lsp") (v "0.2.0") (d (list (d (n "codespan") (r "^0.2.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "languageserver-types") (r "^0.51.0") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0h7jakaacq9hyvkg8b2fqjn0b7qcjnp6l1310nkg6vy3rbivvawn")))

(define-public crate-codespan-lsp-0.2.1 (c (n "codespan-lsp") (v "0.2.1") (d (list (d (n "codespan") (r "^0.2.1") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "languageserver-types") (r "^0.54.0") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0y676mh95bv5g0vw3cwn47nfix6p71r717n27rh3ipfpcz7x0625")))

(define-public crate-codespan-lsp-0.3.0 (c (n "codespan-lsp") (v "0.3.0") (d (list (d (n "codespan") (r "^0.3.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "languageserver-types") (r "^0.54.0") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0k0p3rlzp40v1cfwb67zjhj0nnn9wwr56r42wlqfsnw515dgbrrg")))

(define-public crate-codespan-lsp-0.4.0 (c (n "codespan-lsp") (v "0.4.0") (d (list (d (n "codespan") (r "^0.4.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.4.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.60") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0syqzh529hzsqf9r6ivl01mcldm58w955hki84n1pp967r0x8c44")))

(define-public crate-codespan-lsp-0.4.1 (c (n "codespan-lsp") (v "0.4.1") (d (list (d (n "codespan") (r "^0.4.1") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.4.1") (d #t) (k 0)) (d (n "lsp-types") (r "^0.60") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1ssmwli6qzfvk7fs2z1aig8h03m70dbvm2hqh68s8549wbmdsr40")))

(define-public crate-codespan-lsp-0.5.0 (c (n "codespan-lsp") (v "0.5.0") (d (list (d (n "codespan") (r "^0.5.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.5.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.61") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0a0va25nj38hmk8l7vyqgdlsmz8vsb4zy6mcsblxqy78vmnxwfbf")))

(define-public crate-codespan-lsp-0.6.0 (c (n "codespan-lsp") (v "0.6.0") (d (list (d (n "codespan") (r "^0.6.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.6.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.63") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "11a2h1c12rffzrp4kih6rzfnyc0qdjjr788wwp04f1clnrzh9nlv")))

(define-public crate-codespan-lsp-0.7.0 (c (n "codespan-lsp") (v "0.7.0") (d (list (d (n "codespan") (r "^0.7.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.7.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.68") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1zsmpv4m5y9hkbk85q6can0ps2kx45m470xfdmwjnsaxiydp5dqm")))

(define-public crate-codespan-lsp-0.8.0 (c (n "codespan-lsp") (v "0.8.0") (d (list (d (n "codespan") (r "^0.8.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.8.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.70") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1i9yjdfx63r5drrx5zh9sx8a16qr9xi8kfk06h7ik05y2nv2fc2c")))

(define-public crate-codespan-lsp-0.9.0 (c (n "codespan-lsp") (v "0.9.0") (d (list (d (n "codespan") (r "^0.9.0") (d #t) (k 0)) (d (n "lsp-types") (r ">= 0.70, < 0.74") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0bi9mvgfj9mzq8bjdd2sibfmcwccpinkm8mxyay5ni8avw8y5z79")))

(define-public crate-codespan-lsp-0.9.1 (c (n "codespan-lsp") (v "0.9.1") (d (list (d (n "codespan") (r "^0.9.1") (d #t) (k 0)) (d (n "lsp-types") (r ">= 0.70, < 0.74") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0z348nj5fkvfj9a2zjn41r88x6ixs7xwmmwr9idw49xf4ndp3pxn")))

(define-public crate-codespan-lsp-0.9.2 (c (n "codespan-lsp") (v "0.9.2") (d (list (d (n "codespan") (r "^0.9.2") (d #t) (k 0)) (d (n "lsp-types") (r ">= 0.70, < 0.74") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "071jrh0b512slpr3ybpx1aaqqs584pljnh7yn73kj3a8qij2q7af")))

(define-public crate-codespan-lsp-0.9.3 (c (n "codespan-lsp") (v "0.9.3") (d (list (d (n "codespan") (r "^0.9.3") (d #t) (k 0)) (d (n "lsp-types") (r ">= 0.70, < 0.75") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0p1l6ab15wnxpxcawb8zs0fjb7r00zdsbsqx9qb9j5jm7dragv0s")))

(define-public crate-codespan-lsp-0.9.4 (c (n "codespan-lsp") (v "0.9.4") (d (list (d (n "codespan") (r "^0.9.4") (d #t) (k 0)) (d (n "lsp-types") (r ">= 0.70, < 0.75") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0936lfyhqscz6ry3zhvh1hb9v4cwmwx8q2gmc5gkrbsi7hb1d2hp")))

(define-public crate-codespan-lsp-0.9.5 (c (n "codespan-lsp") (v "0.9.5") (d (list (d (n "codespan") (r "^0.9.5") (d #t) (k 0)) (d (n "lsp-types") (r ">=0.70, <0.75") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1avp0yqvpbm024z9gpy8va49b6fk657kfmgvj1w8v7pgpai487v5")))

(define-public crate-codespan-lsp-0.10.0 (c (n "codespan-lsp") (v "0.10.0") (d (list (d (n "codespan-reporting") (r "^0.9.5") (d #t) (k 0)) (d (n "lsp-types") (r ">=0.70, <0.78") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0cwf09cipqbvsciq6wr65cri1fmb9jasvdj58ajshssmvkkz353b")))

(define-public crate-codespan-lsp-0.10.1 (c (n "codespan-lsp") (v "0.10.1") (d (list (d (n "codespan-reporting") (r "^0.9.5") (d #t) (k 0)) (d (n "lsp-types") (r ">=0.70, <0.80") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1bjsnwqs4bmpa4b4jzlihna1g0l77j47ckh0s9fda36rj527qand")))

(define-public crate-codespan-lsp-0.11.0 (c (n "codespan-lsp") (v "0.11.0") (d (list (d (n "codespan-reporting") (r "^0.11.0") (d #t) (k 0)) (d (n "lsp-types") (r ">=0.84, <0.85") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "18gwhwm10nzpzchyffx5nhzkjf5cahq0v6w7iai64vfwqwszmfa3")))

(define-public crate-codespan-lsp-0.11.1 (c (n "codespan-lsp") (v "0.11.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lsp-types") (r ">=0.84, <0.89") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0s6vn9zvjafvxpcbz7wr619ibp66f74w0hmgkc9mf9zhdavmjhgw")))

