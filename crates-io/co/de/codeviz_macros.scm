(define-module (crates-io co de codeviz_macros) #:use-module (crates-io))

(define-public crate-codeviz_macros-0.1.0 (c (n "codeviz_macros") (v "0.1.0") (h "0halqsnpvhgd4mrrfpj9rm0shgvp7rvxhbgphny17imcfwcq8a4l")))

(define-public crate-codeviz_macros-0.1.1 (c (n "codeviz_macros") (v "0.1.1") (h "1xjjv26a1lqxhmc55ry663r09d1qncpnis0gm9rkj2lnwfvyjzcd")))

(define-public crate-codeviz_macros-0.2.0 (c (n "codeviz_macros") (v "0.2.0") (h "0nzq7964j0zz5rfidly97zjwd9d1asfsgfirga57lcnbmpx5rrf6")))

(define-public crate-codeviz_macros-0.2.1 (c (n "codeviz_macros") (v "0.2.1") (h "1ak5gyccz8ylmvhgz3x4bcmv6a5zdad3y2j2132d5j2955y8q3d8")))

(define-public crate-codeviz_macros-0.2.2 (c (n "codeviz_macros") (v "0.2.2") (h "1nnsig48x2slc0mzmjkgbl7jjnsv9rcl4r5v5jg9wjpgffj17280")))

