(define-module (crates-io co de codes-common) #:use-module (crates-io))

(define-public crate-codes-common-0.1.0 (c (n "codes-common") (v "0.1.0") (d (list (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "1n58hb2y3zvc511i7r0mj2qkav3yvic5lzgdnj6x23i7r7wqdm3q")))

(define-public crate-codes-common-0.1.1 (c (n "codes-common") (v "0.1.1") (d (list (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "05bzbcq9f8j3fdlk3p8c9xj69dji3k60d10sv71fjh6pgz5ycd16")))

(define-public crate-codes-common-0.1.2 (c (n "codes-common") (v "0.1.2") (d (list (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "0g2ghb6avfh5zbz0083w35agd7rnal77nghnj06zq6z80amcmx10")))

(define-public crate-codes-common-0.1.3 (c (n "codes-common") (v "0.1.3") (d (list (d (n "tera") (r "^1.17.1") (d #t) (k 0)))) (h "1wkz7lnx95j64hri8aapzncm8qm4pf964y4y66862fj9jxbhyalf")))

(define-public crate-codes-common-0.1.4 (c (n "codes-common") (v "0.1.4") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.17") (d #t) (k 0)))) (h "1kss7nyiwza9im7zw8yzwqlcgcnkqizqq063lc4ffxa87hyrlaaw") (f (quote (("csv_tools" "csv"))))))

(define-public crate-codes-common-0.1.5 (c (n "codes-common") (v "0.1.5") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.17") (d #t) (k 0)))) (h "0md1jv2g4dsgkbpzasg9bkw9f3b9b5jhcvh6l2sfaz60y7zl3y8l") (f (quote (("csv_tools" "csv"))))))

(define-public crate-codes-common-0.1.6 (c (n "codes-common") (v "0.1.6") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.17") (d #t) (k 0)))) (h "0wdhpvn4vgk8ccd1kpqksdgpz1q752vrrrd3i2ykxy9g31d601qy") (f (quote (("csv_tools" "csv"))))))

(define-public crate-codes-common-0.1.7 (c (n "codes-common") (v "0.1.7") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.17") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1nhp6p6gnh0b61q6492gnzwfmlzxnrw596qkqx5mbh7fcjljwcib") (f (quote (("csv_tools" "csv") ("check_digits"))))))

(define-public crate-codes-common-0.1.8 (c (n "codes-common") (v "0.1.8") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.17") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0cw9nihxjfiv86jfikdjd6l0ajyfdpc6f4175qxhrry282vi5r9b") (f (quote (("csv_tools" "csv"))))))

(define-public crate-codes-common-0.1.9 (c (n "codes-common") (v "0.1.9") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.17") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "11xjj93m7ax5b2c568f3xiv0m69vfd42njmxljj1w4g76w86pcd2") (f (quote (("csv_tools" "build" "csv") ("build" "tera" "tracing"))))))

