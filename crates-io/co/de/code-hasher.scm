(define-module (crates-io co de code-hasher) #:use-module (crates-io))

(define-public crate-code-hasher-0.1.0 (c (n "code-hasher") (v "0.1.0") (d (list (d (n "blake3") (r "^0.3.7") (d #t) (k 0)) (d (n "dusk-plonk") (r "^0.8") (d #t) (k 2)))) (h "0zm67bfg1wsvyjdaxv7zd612y84zjhllwpqxlcd8hhxllcykzxmx")))

