(define-module (crates-io co de code-product-macro) #:use-module (crates-io))

(define-public crate-code-product-macro-0.0.1 (c (n "code-product-macro") (v "0.0.1") (d (list (d (n "code-product-lib") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("proc-macro"))) (d #t) (k 0)))) (h "18mvk5yjc66hf14sm92y9gva7vvv4y0d4fq3vkxrwrnyk6aqwigi")))

(define-public crate-code-product-macro-0.2.0 (c (n "code-product-macro") (v "0.2.0") (d (list (d (n "code-product-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("proc-macro"))) (d #t) (k 0)))) (h "17l77yk04zl8vh6q05vcpr20ci87skr1sqf6bjkrs20jcvacg4hb")))

(define-public crate-code-product-macro-0.3.0 (c (n "code-product-macro") (v "0.3.0") (d (list (d (n "code-product-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("proc-macro"))) (d #t) (k 0)))) (h "04wfxkqz0pqw1vzxpcw5zylrjyx18rmbgcwn1z35z2dv68z40w83")))

(define-public crate-code-product-macro-0.4.0 (c (n "code-product-macro") (v "0.4.0") (d (list (d (n "code-product-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (f (quote ("proc-macro"))) (d #t) (k 0)))) (h "1lxz8hq81cn7hfmmwharahq48fc1xydnz88hv88ly7xzbwb0w1j4")))

