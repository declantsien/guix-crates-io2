(define-module (crates-io co de codeclippy) #:use-module (crates-io))

(define-public crate-codeclippy-0.0.1 (c (n "codeclippy") (v "0.0.1") (d (list (d (n "clap") (r "^4.2") (f (quote ("std" "help"))) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("default" "full" "visit"))) (d #t) (k 0)))) (h "1lk1430yhvygiiqnjzx2hs35819q3cd3vji2fvrdh4fgj6zg58m2")))

