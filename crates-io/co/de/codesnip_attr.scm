(define-module (crates-io co de codesnip_attr) #:use-module (crates-io))

(define-public crate-codesnip_attr-0.2.0 (c (n "codesnip_attr") (v "0.2.0") (d (list (d (n "codesnip_core") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "0nrzl6niy4a87fcjawn4awrc0dba0m7q897ndvfcb5nvi2iqyz9r")))

(define-public crate-codesnip_attr-0.2.1 (c (n "codesnip_attr") (v "0.2.1") (d (list (d (n "codesnip_core") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "0zjik8qmvyys1abw7j65zk2ny0cg0ry49gg4j36jgrg7a1l50jn8") (f (quote (("check" "codesnip_core"))))))

(define-public crate-codesnip_attr-0.2.2 (c (n "codesnip_attr") (v "0.2.2") (d (list (d (n "codesnip_core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.44") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("full"))) (d #t) (k 0)))) (h "16vlgm023scvfcsh7ikggzaa6p47i3k44dk4kviki38zj3agbd3l") (f (quote (("check" "codesnip_core"))))))

