(define-module (crates-io co de codetrotter_aoc_2019_solutions) #:use-module (crates-io))

(define-public crate-codetrotter_aoc_2019_solutions-0.7.3 (c (n "codetrotter_aoc_2019_solutions") (v "0.7.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)))) (h "1a68bl5wa52i5ygh1fa7gwdy2ffa5dy25naglcg4xdrl9754pml1")))

(define-public crate-codetrotter_aoc_2019_solutions-0.7.4 (c (n "codetrotter_aoc_2019_solutions") (v "0.7.4") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)))) (h "1yndh9q9fyxs07ybvcpglrwaj2r1k3dj4pyzal2vvng9lhg62003")))

(define-public crate-codetrotter_aoc_2019_solutions-0.8.0 (c (n "codetrotter_aoc_2019_solutions") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)))) (h "143jmfqpgqczs0kgj37sy6vs7kj0ikhdwkn8j7h15dvsxb6h8np4")))

(define-public crate-codetrotter_aoc_2019_solutions-0.9.0 (c (n "codetrotter_aoc_2019_solutions") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)))) (h "16a4ab9zlra763lmmbakpn8ph84zhnm5zcx5cd72bp1d48a2jgbc")))

