(define-module (crates-io co de code-errors) #:use-module (crates-io))

(define-public crate-code-errors-0.1.0 (c (n "code-errors") (v "0.1.0") (d (list (d (n "checked_command") (r "^0.2.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vgz5nh9x28mwya1m3ln36fnixdhgdjprxbk0q97bj2h0n1rxaaf")))

