(define-module (crates-io co de codec2) #:use-module (crates-io))

(define-public crate-codec2-0.1.0 (c (n "codec2") (v "0.1.0") (d (list (d (n "zerocopy") (r "^0.3.0") (d #t) (k 2)))) (h "1irk8b477c9pqzgzcs8v4gmz9c7yz4l7xmv0y2c9algs3s0myiz3")))

(define-public crate-codec2-0.2.0 (c (n "codec2") (v "0.2.0") (d (list (d (n "zerocopy") (r "^0.3.0") (d #t) (k 2)))) (h "1f68j04ndfijvq97cjjcp7nwjhy24cvkbvrgvq60jc1nccln40pz")))

(define-public crate-codec2-0.3.0 (c (n "codec2") (v "0.3.0") (d (list (d (n "zerocopy") (r "^0.3.0") (d #t) (k 2)))) (h "0nz6kmmkwxzsx0giz410lqqd0dhljgjsakglynn9dw52li6d1vrc")))

