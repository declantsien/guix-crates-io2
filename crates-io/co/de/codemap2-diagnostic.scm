(define-module (crates-io co de codemap2-diagnostic) #:use-module (crates-io))

(define-public crate-codemap2-diagnostic-0.1.0 (c (n "codemap2-diagnostic") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "codemap2") (r "^0.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "1rvmfw0j8i1gjxwvfi418pfc2wkjz1m1qhidp8fn8djw2myzd37b")))

