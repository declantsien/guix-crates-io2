(define-module (crates-io co de code_insights) #:use-module (crates-io))

(define-public crate-code_insights-0.1.0 (c (n "code_insights") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ab910jb0j0bnbr15bshs1ww6dncgk9yr1h9591vgyd19j7x4ym1")))

(define-public crate-code_insights-0.1.1 (c (n "code_insights") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15w076fn4gbg45a2rwv42x27131i16vm1745rqqqmcqzjgm3a9j9")))

