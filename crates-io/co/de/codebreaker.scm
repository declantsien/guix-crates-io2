(define-module (crates-io co de codebreaker) #:use-module (crates-io))

(define-public crate-codebreaker-0.1.0 (c (n "codebreaker") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2.4") (d #t) (k 0)))) (h "1kssrk4y9fg1nmbwfkgnw7hhgc5459b4c1h68gk0jmszj9mimwsf")))

(define-public crate-codebreaker-0.1.1 (c (n "codebreaker") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.2.4") (d #t) (k 0)))) (h "1mqwywm1rlq275kmphk43h2mjlf1nv17svb9mywk88y2rgw33mvd")))

(define-public crate-codebreaker-0.1.2 (c (n "codebreaker") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.2.4") (d #t) (k 0)))) (h "16q54di6lscm1nd95h2l8fnxc20myffdz46gr22q13xspivh0ggw")))

(define-public crate-codebreaker-0.1.3 (c (n "codebreaker") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.2.4") (d #t) (k 0)))) (h "0mhpp0pfjlga3jh4nribs1zvj0vzi48dgj1sxjppqcliq451fqk7")))

(define-public crate-codebreaker-0.1.4 (c (n "codebreaker") (v "0.1.4") (d (list (d (n "num-bigint") (r "^0.2.4") (d #t) (k 0)))) (h "0dih8csycn3k9msp93w55prg90xh4x6gigp29kygwcv4vqpj5c8s")))

(define-public crate-codebreaker-0.1.5 (c (n "codebreaker") (v "0.1.5") (d (list (d (n "num-bigint") (r "^0.2.4") (d #t) (k 0)))) (h "0wp3hxf55a65pssr6ximw8jy5lv10vv3ar710gfh7gvcr1zm48z2")))

(define-public crate-codebreaker-0.1.6 (c (n "codebreaker") (v "0.1.6") (d (list (d (n "num-bigint") (r "^0.2.4") (d #t) (k 0)))) (h "0rvbsm5b2p83rc72ildnxvqlac55apghix5kimd46idz2cp397sp")))

(define-public crate-codebreaker-0.2.0 (c (n "codebreaker") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)))) (h "00dyas85gg6ac15qqp28vxvkcn139dh3vzcclzaq1v1wz3b4i56r") (f (quote (("std" "num-bigint/std") ("default" "std"))))))

(define-public crate-codebreaker-0.2.1 (c (n "codebreaker") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)))) (h "13mgsygmqlc5wa6dibj71b3cqbrcigyc1yiyfqsz2vmg6dzlvzmx") (f (quote (("std" "num-bigint/std") ("default" "std"))))))

(define-public crate-codebreaker-0.2.2 (c (n "codebreaker") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)))) (h "0yrq4mjyxc2lcs8sn8q6br2f6dj6ai5h8vnnrjjf7h4s3nsgy8r7") (f (quote (("std" "num-bigint/std") ("default" "std"))))))

(define-public crate-codebreaker-0.2.3 (c (n "codebreaker") (v "0.2.3") (d (list (d (n "bytemuck") (r "^1.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)))) (h "1qc6r4wlsy0r34f6p89fxzbls2gqdr67nwncf80xfyg50vmr7m0j") (f (quote (("std" "num-bigint/std") ("default" "std"))))))

(define-public crate-codebreaker-0.2.4 (c (n "codebreaker") (v "0.2.4") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "14i3h88dhkskd20c84c1cxsyk4szr5r6w3z9zazbmmglspxzwj8j") (f (quote (("std" "num-bigint/std") ("default" "std"))))))

(define-public crate-codebreaker-0.2.5 (c (n "codebreaker") (v "0.2.5") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)))) (h "17ar4rrqzfp89537h9mx4xwnnjfi9cy9sam12jgccwxa1drjnkbc") (f (quote (("std" "num-bigint/std") ("default" "std"))))))

(define-public crate-codebreaker-0.2.6 (c (n "codebreaker") (v "0.2.6") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1s0qagykjq6jwyp5jh8f6rcjhm7jir2l42mhvi9cpg7xlmh77cb3") (f (quote (("std" "num-bigint/std") ("default" "std"))))))

(define-public crate-codebreaker-0.3.0 (c (n "codebreaker") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1v4c9gp1xm6c6b5g2kf7s2qgdahrgas1ccyq6czcv8zcqhq0wd1s") (f (quote (("std" "num-bigint/std") ("default" "std"))))))

(define-public crate-codebreaker-0.3.1 (c (n "codebreaker") (v "0.3.1") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1da2lgzkhvablvr3jmh5mis05dx04hlfvbx0qbjmgg6wzs8m2f1f") (f (quote (("std" "num-bigint/std") ("default" "std"))))))

(define-public crate-codebreaker-0.3.2 (c (n "codebreaker") (v "0.3.2") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1p96mch9n30smx43sl30ddwzyp1q0db3lk92fx5w5wz5bbfp4bmv") (f (quote (("std" "num-bigint/std") ("default" "std"))))))

