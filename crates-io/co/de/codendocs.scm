(define-module (crates-io co de codendocs) #:use-module (crates-io))

(define-public crate-codendocs-0.0.0 (c (n "codendocs") (v "0.0.0") (h "1hm6k656wnif7q4mkf0czsdz6qfv8s3il3p0ialcqrwsfvmsymj5") (y #t)))

(define-public crate-codendocs-0.0.1 (c (n "codendocs") (v "0.0.1") (h "1rb378da25py3axjab4h5hr57b1bq47n40xn9idjmb71rgdpcy0q") (y #t)))

(define-public crate-codendocs-0.0.2 (c (n "codendocs") (v "0.0.2") (d (list (d (n "codendocs") (r "^0.0.0") (d #t) (k 0)))) (h "1y3rfla7ffn1cdj08q6rplm0js6lx3yqz29m3rwilvxbrb78zxmj") (y #t)))

(define-public crate-codendocs-0.0.3 (c (n "codendocs") (v "0.0.3") (h "03da2mzrg8x4yx4srp1czr3d12ayj850dcbx1s2lscxsqshcrlmn") (y #t)))

(define-public crate-codendocs-0.0.6 (c (n "codendocs") (v "0.0.6") (h "13lndc3k2yhbm0s14q8qn918z94lyn3a8sbqranjydsivrkgx2ci") (y #t)))

(define-public crate-codendocs-0.0.4 (c (n "codendocs") (v "0.0.4") (h "0y4afcbqnf8yvvp45qcpfwhp8kq9j5nlbcy93p3y6465qpdc8mxv") (y #t)))

(define-public crate-codendocs-0.0.7 (c (n "codendocs") (v "0.0.7") (h "0ww2dd7jqwvabhdyc620pkbdbrc88qscmaqgiqm5l1bcclqk53sf")))

