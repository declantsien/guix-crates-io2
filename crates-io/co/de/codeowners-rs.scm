(define-module (crates-io co de codeowners-rs) #:use-module (crates-io))

(define-public crate-codeowners-rs-0.1.0 (c (n "codeowners-rs") (v "0.1.0") (d (list (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.27") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1rbgp08av4yv7m4ryqd3zwqp60h2032yx6k1vdx16gxx31cc0hc9")))

(define-public crate-codeowners-rs-0.1.1 (c (n "codeowners-rs") (v "0.1.1") (d (list (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.27") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "15f3x3gzbpdq61byjyzf61ar9jpzvq95av9qk3nqagk640xznzgn")))

