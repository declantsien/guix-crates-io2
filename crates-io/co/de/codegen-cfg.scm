(define-module (crates-io co de codegen-cfg) #:use-module (crates-io))

(define-public crate-codegen-cfg-0.1.0 (c (n "codegen-cfg") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nugine-rust-utils") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "0kkn31dx615ssz5if58nx3pvzi0zhjq1ka6zmh8xwybwcsidr3yq")))

(define-public crate-codegen-cfg-0.1.1 (c (n "codegen-cfg") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nugine-rust-utils") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "02bsphhycg0hfm2cpi165rpxiskgyhkr9mdrmn4z87lk0k0g1jbf")))

(define-public crate-codegen-cfg-0.1.2 (c (n "codegen-cfg") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nugine-rust-utils") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "1z3a4pz8kz4px0c26pafbpc0gk170z7pz2dgw62wlwzl25h9l379")))

(define-public crate-codegen-cfg-0.1.3 (c (n "codegen-cfg") (v "0.1.3") (d (list (d (n "bool-logic") (r "^0.1.3") (d #t) (k 0)) (d (n "nugine-rust-utils") (r "^0.1.4") (f (quote ("std"))) (d #t) (k 0)))) (h "14d7lq077mnd8nq5sylrq8z0s5l1hs73xvnlb49xixdxymqybrnb")))

(define-public crate-codegen-cfg-0.2.0 (c (n "codegen-cfg") (v "0.2.0") (d (list (d (n "bool-logic") (r "^0.2.0") (d #t) (k 0)) (d (n "nugine-rust-utils") (r "^0.2.1") (f (quote ("std"))) (d #t) (k 0)))) (h "1szzlcykvyc9qq1k0ldmd6zvn5wbl3ly2mlrpikysqbkds6ly525")))

