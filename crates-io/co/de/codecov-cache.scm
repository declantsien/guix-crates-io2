(define-module (crates-io co de codecov-cache) #:use-module (crates-io))

(define-public crate-codecov-cache-0.1.0 (c (n "codecov-cache") (v "0.1.0") (d (list (d (n "codecov") (r "^0.3.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1xscnri1xp91xmi5gywnwpn3i0x1f699sivx8p85lidrs5i2dsxi")))

(define-public crate-codecov-cache-0.1.1 (c (n "codecov-cache") (v "0.1.1") (d (list (d (n "codecov") (r "^0.3.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1afy369ym7b35b175na6fyqq7pdbilaj2m9k5f5h96w024v0wb80")))

(define-public crate-codecov-cache-0.1.2 (c (n "codecov-cache") (v "0.1.2") (d (list (d (n "codecov") (r "^0.3.5") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0y164jg5psqvp1xpbx7sa3l47mn4vbbddidm978qd97f94l7cf0f")))

(define-public crate-codecov-cache-0.2.0 (c (n "codecov-cache") (v "0.2.0") (d (list (d (n "codecov") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1xcpafjr299x70vml18b12xnw0zipgmj8ab43q471yqsms0wlvzz")))

