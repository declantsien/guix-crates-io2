(define-module (crates-io co de code-sandwich-crates-io-release-test) #:use-module (crates-io))

(define-public crate-code-sandwich-crates-io-release-test-0.1.0 (c (n "code-sandwich-crates-io-release-test") (v "0.1.0") (d (list (d (n "code-sandwich-crates-io-release-test-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0j12zqamdlax6m0088mihdzy4qj4j1j79nfw9gq30szj8nhlxgrx")))

(define-public crate-code-sandwich-crates-io-release-test-0.1.1 (c (n "code-sandwich-crates-io-release-test") (v "0.1.1") (d (list (d (n "code-sandwich-crates-io-release-test-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1gixmzgjf75bpkd26zrpx2dmkxfq0jabma9ggy227zlwjxfvgs6b")))

(define-public crate-code-sandwich-crates-io-release-test-0.1.2 (c (n "code-sandwich-crates-io-release-test") (v "0.1.2") (d (list (d (n "code-sandwich-crates-io-release-test-macros") (r "^0.1.1") (d #t) (k 0)))) (h "057ymx1mjmqmblc6934yjrac30h6bbwc2xgd94ka5q86l7a0n92v")))

(define-public crate-code-sandwich-crates-io-release-test-0.1.3 (c (n "code-sandwich-crates-io-release-test") (v "0.1.3") (d (list (d (n "code-sandwich-crates-io-release-test-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0z055kk96kb2z89czxhfx3jvwz9grqqv5vbmjnvacv1zypslckxa")))

(define-public crate-code-sandwich-crates-io-release-test-0.1.4 (c (n "code-sandwich-crates-io-release-test") (v "0.1.4") (d (list (d (n "code-sandwich-crates-io-release-test-macros") (r "^0.1.1") (d #t) (k 0)))) (h "09vp255wy9g0irl5m5fhx9qs0hcg8kh90qqirmk9mqaqsxk9hb9x")))

