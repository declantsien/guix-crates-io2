(define-module (crates-io co de codespan-derive-proc) #:use-module (crates-io))

(define-public crate-codespan-derive-proc-0.1.0 (c (n "codespan-derive-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1q99ny8jm2dky3sv7jp8q5b1j4hac7sb8x03f6mk13z88i6q57q7")))

(define-public crate-codespan-derive-proc-0.1.1 (c (n "codespan-derive-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "1lb93a76822si0vhf3ilyw2c83mrr43cms4j9nr047jz2qzmxa3w")))

