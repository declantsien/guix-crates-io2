(define-module (crates-io co de code-status) #:use-module (crates-io))

(define-public crate-code-status-0.1.0 (c (n "code-status") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "slab_tree") (r "^0.3.2") (d #t) (k 0)))) (h "1lfnd6qvpbp2l3rqgl2fw4w7wgbzq0p2y0bhqshs1b2mw0bngfxk")))

