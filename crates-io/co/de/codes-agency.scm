(define-module (crates-io co de codes-agency) #:use-module (crates-io))

(define-public crate-codes-agency-0.1.0 (c (n "codes-agency") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13m5plkklp05j6335jf0sang20f5mlkzs8iyijz0l5q30any1ykp") (f (quote (("serde_derive" "serde") ("default" "serde_derive"))))))

(define-public crate-codes-agency-0.1.2 (c (n "codes-agency") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "052fz0axhmmvam9ia8fykxak2lz7mk41xzwm51xag7y402cmjmrc") (f (quote (("default" "serde"))))))

(define-public crate-codes-agency-0.1.3 (c (n "codes-agency") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0a3x8dhgav9szb8jvmbifznvlbqqlj9v46fjz9nbn10sh1r1p9rp") (f (quote (("default" "serde"))))))

(define-public crate-codes-agency-0.1.4 (c (n "codes-agency") (v "0.1.4") (d (list (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1l28iacd8yriqfwbrs5hysikb1k1k4qid9mxi8nbgkp05q4qwdgd") (f (quote (("default" "serde"))))))

(define-public crate-codes-agency-0.1.5 (c (n "codes-agency") (v "0.1.5") (d (list (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18xch9zq6lqcwjfh1nr3sq9br98pa3kvpa4fkdaddnh4aa2jpfjq") (f (quote (("default" "serde"))))))

(define-public crate-codes-agency-0.1.6 (c (n "codes-agency") (v "0.1.6") (d (list (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19iwk8yiwdw892lljgy8vw06d250iwximapchpwi5l4nw60kk733") (f (quote (("default" "serde"))))))

(define-public crate-codes-agency-0.1.7 (c (n "codes-agency") (v "0.1.7") (d (list (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0q9dhl0wmdaq6xa8qb0sidgs051xbm5zdgjp46dn3d6szn1cpb8p") (f (quote (("default" "serde"))))))

(define-public crate-codes-agency-0.1.8 (c (n "codes-agency") (v "0.1.8") (d (list (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02fr2j5gz2bnyx0d0hzjwsvbgyak7fq1a4bcbwphzrs4z5697b1w") (f (quote (("default" "serde"))))))

(define-public crate-codes-agency-0.1.9 (c (n "codes-agency") (v "0.1.9") (d (list (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bqgs9im6s1px334kw1l1qrywy9ss5zlk6qzbb1rr53z4j87g6l0") (f (quote (("default" "serde"))))))

