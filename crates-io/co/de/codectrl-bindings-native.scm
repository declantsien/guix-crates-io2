(define-module (crates-io co de codectrl-bindings-native) #:use-module (crates-io))

(define-public crate-codectrl-bindings-native-0.0.0 (c (n "codectrl-bindings-native") (v "0.0.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-build") (r "^0.10") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0pj9l723aw4lw9cbx5i2mzz8n22ys9iba1mjj5rcapsmm9fx8bfq")))

