(define-module (crates-io co de codepage-strings) #:use-module (crates-io))

(define-public crate-codepage-strings-1.0.0 (c (n "codepage-strings") (v "1.0.0") (d (list (d (n "codepage") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "oem_cp") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0hn29s0c4c40hdknybqbzq1gm682bnqq7jgdv2a2gaq59lqiqxvf")))

(define-public crate-codepage-strings-1.0.1 (c (n "codepage-strings") (v "1.0.1") (d (list (d (n "codepage") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "oem_cp") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0llcmlgl894w9s0lv44bp4fgbrzznycpqw0c9a5wkhsvcgy2akyk")))

(define-public crate-codepage-strings-1.0.2 (c (n "codepage-strings") (v "1.0.2") (d (list (d (n "codepage") (r "^0.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "oem_cp") (r "^1.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "08r5fj6gz26a8fqvc7pi3dq6la924l6csysk77hqzbz1zk0drq4n")))

