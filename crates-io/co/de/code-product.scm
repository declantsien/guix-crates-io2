(define-module (crates-io co de code-product) #:use-module (crates-io))

(define-public crate-code-product-0.0.1 (c (n "code-product") (v "0.0.1") (d (list (d (n "code-product-lib") (r "^0.0.1") (d #t) (k 0)) (d (n "code-product-macro") (r "^0.0.1") (d #t) (k 0)))) (h "0iykpkc2b471lzhvrpz69gxz9d09p1lh121vcg1596gcadk0yyf6") (r "1.70.0")))

(define-public crate-code-product-0.2.0 (c (n "code-product") (v "0.2.0") (d (list (d (n "code-product-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "code-product-macro") (r "^0.2.0") (d #t) (k 0)))) (h "10avqy4hba5bd9h6mhndkw3260x12z3ib83wkj7wzdgbwnvgv6kq") (r "1.70.0")))

(define-public crate-code-product-0.3.0 (c (n "code-product") (v "0.3.0") (d (list (d (n "code-product-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "code-product-macro") (r "^0.3.0") (d #t) (k 0)))) (h "00gf6w9n9f8fc8akb5a26fy96scjfh9frhjpi2xkbd4ns61a9vlz") (r "1.70.0")))

(define-public crate-code-product-0.4.0 (c (n "code-product") (v "0.4.0") (d (list (d (n "code-product-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "code-product-macro") (r "^0.3.0") (d #t) (k 0)))) (h "15lpgs49vns99bxvgl5mj5c8nwlp4f52skzzcr1zh25nzpi4ly5g") (y #t) (r "1.70.0")))

(define-public crate-code-product-0.4.1 (c (n "code-product") (v "0.4.1") (d (list (d (n "code-product-lib") (r "^0.4.0") (d #t) (k 0)) (d (n "code-product-macro") (r "^0.4.0") (d #t) (k 0)))) (h "0dv92sp65s2sjnrdswdz0zhw3jfsfiz83xj84gwwf5xfydakm3ia") (r "1.70.0")))

