(define-module (crates-io co de codemonk-primes-cli) #:use-module (crates-io))

(define-public crate-codemonk-primes-cli-1.0.2 (c (n "codemonk-primes-cli") (v "1.0.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n0w5w2xmsy4dpfmnhq63mf0sahjy7pznwksfrqbvbfcnn8kxx86") (y #t)))

(define-public crate-codemonk-primes-cli-1.0.3 (c (n "codemonk-primes-cli") (v "1.0.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p6amqpp17nz1gh63zr06qirv91mr4n0wkwm7ziamb1r4gaz209w") (y #t)))

(define-public crate-codemonk-primes-cli-1.0.4 (c (n "codemonk-primes-cli") (v "1.0.4") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j62cjmdaqb1bvp7nbvy561kjl727ypbkjhn6mdgy77j4qxywayz")))

