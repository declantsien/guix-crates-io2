(define-module (crates-io co de codeviz_rust) #:use-module (crates-io))

(define-public crate-codeviz_rust-0.1.0 (c (n "codeviz_rust") (v "0.1.0") (d (list (d (n "codeviz_common") (r "^0.1.0") (d #t) (k 0)))) (h "063r74f2cvg0ab0cf1lj3bdq32x3lgnam0d1289jsk2lgqcqk504")))

(define-public crate-codeviz_rust-0.1.1 (c (n "codeviz_rust") (v "0.1.1") (d (list (d (n "codeviz_common") (r "^0.1.1") (d #t) (k 0)))) (h "02y0ziaxhl272fhg15vda90kv2icg17cw4bzg2dczpfbggq9xzki")))

(define-public crate-codeviz_rust-0.2.0 (c (n "codeviz_rust") (v "0.2.0") (d (list (d (n "codeviz_common") (r "^0.2.0") (d #t) (k 0)))) (h "0a426svad3j44kkjz3g061csqzh9hch48ci6lwj0xfk9ih2bwd6g")))

(define-public crate-codeviz_rust-0.2.1 (c (n "codeviz_rust") (v "0.2.1") (d (list (d (n "codeviz_common") (r "^0.2.1") (d #t) (k 0)))) (h "0lafl2kk7cr3ijspfw1mhwpji6m2qp9yib7m98ags9nhvmqi6ihg")))

(define-public crate-codeviz_rust-0.2.2 (c (n "codeviz_rust") (v "0.2.2") (d (list (d (n "codeviz_common") (r "^0.2.2") (d #t) (k 0)))) (h "0c7yl9hqfxqj3d6wi8b84gr2arzygcmnscrc5a6r9wcf0mcvmprz")))

