(define-module (crates-io co de codegen-for-async-graphql) #:use-module (crates-io))

(define-public crate-codegen-for-async-graphql-0.1.0 (c (n "codegen-for-async-graphql") (v "0.1.0") (d (list (d (n "codegen-for-async-graphql-renderer") (r "^0.1.0") (d #t) (k 0)))) (h "1qv53rz4an6np5zw3wx7qk8plqpr7l7djr2dz9p1g9assdp60456") (y #t)))

(define-public crate-codegen-for-async-graphql-0.2.0 (c (n "codegen-for-async-graphql") (v "0.2.0") (d (list (d (n "codegen-for-async-graphql-renderer") (r "^0.2.0") (d #t) (k 0)))) (h "1glmrxk9knm53yh83yygpbqvx85p907g9l9kg5bhhqqriw72si83") (y #t)))

(define-public crate-codegen-for-async-graphql-0.2.1 (c (n "codegen-for-async-graphql") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "codegen-for-async-graphql-renderer") (r "^0.2.1") (d #t) (k 0)))) (h "0xfx8n60yh4g3agvs5jnz9y75yn7nkmz7cj2v9lpslzq62vddlmh") (y #t)))

(define-public crate-codegen-for-async-graphql-0.2.2 (c (n "codegen-for-async-graphql") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "codegen-for-async-graphql-renderer") (r "^0.2.1") (d #t) (k 0)))) (h "0mpcbw4762ibp3vcadnyapcc8sw0371wsgrixy8vis3s4wm5y3j4") (y #t)))

(define-public crate-codegen-for-async-graphql-0.2.3 (c (n "codegen-for-async-graphql") (v "0.2.3") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "codegen-for-async-graphql-renderer") (r "^0.2.1") (d #t) (k 0)))) (h "0h9fs5ar5czrq9px9ygbrcfv8vcq63755byl8bky3jw3yzdb3f2c") (y #t)))

(define-public crate-codegen-for-async-graphql-0.2.4 (c (n "codegen-for-async-graphql") (v "0.2.4") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "codegen-for-async-graphql-renderer") (r "^0.2.1") (d #t) (k 0)))) (h "06f1x9wm8h6k6b5ja6xdr5yafvxp87xvrlm5qdbspnf9gs5nx56k")))

(define-public crate-codegen-for-async-graphql-0.2.5 (c (n "codegen-for-async-graphql") (v "0.2.5") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "codegen-for-async-graphql-renderer") (r "^0.2.1") (d #t) (k 0)))) (h "11f87i2isnh103f47vpfrs9fw7f3vrikzg5vwgdj2fylnlvfx51c")))

(define-public crate-codegen-for-async-graphql-0.2.6 (c (n "codegen-for-async-graphql") (v "0.2.6") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "codegen-for-async-graphql-renderer") (r "^0.2.6") (d #t) (k 0)))) (h "08yiwqrxx17hbz6mkxp8xwrvwhwwb4q1br4csp8dngm1kdgpa9x9")))

