(define-module (crates-io co de code-maven-experiment-one) #:use-module (crates-io))

(define-public crate-code-maven-experiment-one-0.1.0 (c (n "code-maven-experiment-one") (v "0.1.0") (h "0w6y9d4z7ki5mnz7i61jw21rzlnnmv4glazawp1dg2zn44brniwi")))

(define-public crate-code-maven-experiment-one-0.1.1 (c (n "code-maven-experiment-one") (v "0.1.1") (h "0p7dq07q97nndf57byqzzdf5v05s1xcmd05nbbijwwh44w7rpw10")))

(define-public crate-code-maven-experiment-one-0.1.2 (c (n "code-maven-experiment-one") (v "0.1.2") (h "1yncfk286rf2lj42zbnp3ibb9139sq704kc0xv5fgjvp0gq662n9")))

(define-public crate-code-maven-experiment-one-0.1.3 (c (n "code-maven-experiment-one") (v "0.1.3") (h "1x6k1chxxal110xp2lnb29n3z6vx042y2s18g3kh01ch18rncm31")))

