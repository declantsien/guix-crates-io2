(define-module (crates-io co de codeviz_js) #:use-module (crates-io))

(define-public crate-codeviz_js-0.1.0 (c (n "codeviz_js") (v "0.1.0") (d (list (d (n "codeviz_common") (r "^0.1.0") (d #t) (k 0)))) (h "09xx5jfkmkk1x71bwwdz53d6c0wnzi0m40vm8kq1r75nv3a3az46")))

(define-public crate-codeviz_js-0.1.1 (c (n "codeviz_js") (v "0.1.1") (d (list (d (n "codeviz_common") (r "^0.1.1") (d #t) (k 0)))) (h "0pcyr4rfmwjaslrs0z48qs079dmmam5bzcnms6yc7xyrp4i1gnym")))

(define-public crate-codeviz_js-0.2.0 (c (n "codeviz_js") (v "0.2.0") (d (list (d (n "codeviz_common") (r "^0.2.0") (d #t) (k 0)))) (h "0fi7bgm40x0b998mh0qg96rl5bbmh1a9694x8vkx1wjxn0v3v88l")))

(define-public crate-codeviz_js-0.2.1 (c (n "codeviz_js") (v "0.2.1") (d (list (d (n "codeviz_common") (r "^0.2.1") (d #t) (k 0)))) (h "1gsrbmixqzxa9bjg7z4x4rlpi15fhis7wy3qnzry6iwx3agcrhy3")))

(define-public crate-codeviz_js-0.2.2 (c (n "codeviz_js") (v "0.2.2") (d (list (d (n "codeviz_common") (r "^0.2.2") (d #t) (k 0)))) (h "1c9lrj0y64wv6n08bya5jhaj15kkmhfiy6z4f8ysj6q0qb6swqbk")))

