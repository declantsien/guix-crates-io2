(define-module (crates-io co de codespan_preprocessed) #:use-module (crates-io))

(define-public crate-codespan_preprocessed-0.1.0 (c (n "codespan_preprocessed") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0vxcbv9wfyfd95j3mmsn1sjfm7zi7cjpmi0b9b10klp46xs7gabc") (y #t)))

(define-public crate-codespan_preprocessed-0.1.1 (c (n "codespan_preprocessed") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1kkm56p1nj8idgb5pq3vkl6m51hsmfyb9l172fnfjy1y9ca9bkq5") (y #t)))

(define-public crate-codespan_preprocessed-0.1.2 (c (n "codespan_preprocessed") (v "0.1.2") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0wdbkxwn9bkizlvq3igdj2zn8b1arh835gy02rb0m0crxn5asx51") (y #t)))

(define-public crate-codespan_preprocessed-0.1.3 (c (n "codespan_preprocessed") (v "0.1.3") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0kqx7g0b2v8xk3yk6s60r3ys6m6n7yp85v741dn7spfyrswfh3ys") (y #t)))

(define-public crate-codespan_preprocessed-0.1.4 (c (n "codespan_preprocessed") (v "0.1.4") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "06x778n7anr65rrnlj0jkp0n2d29fqy3ky3z3sb8a2kwkzhngb2g") (y #t)))

(define-public crate-codespan_preprocessed-0.2.0 (c (n "codespan_preprocessed") (v "0.2.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "06lk0smfsl66fcja44lvcjdgjal46fkqw5pb0n506cfxaybg7lpm") (y #t)))

(define-public crate-codespan_preprocessed-0.2.1 (c (n "codespan_preprocessed") (v "0.2.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0841q1rjzjk1ljvvgj0p7xshppwgm7dh5rlgqma3j0imk8ncl6f3") (y #t)))

(define-public crate-codespan_preprocessed-0.2.2 (c (n "codespan_preprocessed") (v "0.2.2") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0z2f2x7fqcdjgj2kf4qm94y87ns680gkzwy4wrb6klnvs8kwnra6") (y #t)))

(define-public crate-codespan_preprocessed-0.2.3 (c (n "codespan_preprocessed") (v "0.2.3") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "06ff7jic7l88n4j2anc9qrmssmdhzr41qhnjjjsgxn7zbn0jwfn4") (y #t)))

(define-public crate-codespan_preprocessed-0.3.0 (c (n "codespan_preprocessed") (v "0.3.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0wzp0vfyds7by8ycn99a3vm4c9q8kbhklnay64rxh3bi33r1my6g") (y #t)))

(define-public crate-codespan_preprocessed-0.3.1 (c (n "codespan_preprocessed") (v "0.3.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1aivc7wkalnyp4n6d14y5yy7ajw126z0pskqxnw2036ap7in1khd") (y #t)))

(define-public crate-codespan_preprocessed-0.4.0 (c (n "codespan_preprocessed") (v "0.4.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0qsm3cwkzya2c1hbkz9n0d0cnq7y75717ig0687byjmsll72wbx7") (y #t)))

(define-public crate-codespan_preprocessed-0.4.1 (c (n "codespan_preprocessed") (v "0.4.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1dlrf5ffnvgl9i20g7xm561xmc0y0bkzvs0p9szgkhf8q919ap3h") (y #t)))

(define-public crate-codespan_preprocessed-0.4.2 (c (n "codespan_preprocessed") (v "0.4.2") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "083ynpv547i2c983z3qn8r11xy3dysc3xyn2ydlw8s2w693mbkmm") (y #t)))

(define-public crate-codespan_preprocessed-0.4.3 (c (n "codespan_preprocessed") (v "0.4.3") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0ljc1yyv78h36r66l3k887hsb4aaywb6kcsj54k6dn5x0gv7a1c1") (y #t)))

(define-public crate-codespan_preprocessed-0.4.4 (c (n "codespan_preprocessed") (v "0.4.4") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1s4wpjdsg2zi4ydxv3w4657m46534w9r5rr72y36xd6ih06g388s") (y #t)))

(define-public crate-codespan_preprocessed-0.4.5 (c (n "codespan_preprocessed") (v "0.4.5") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1df4g211r0das8sjs3cwj5l4zvvr5sxkwf1xyj4qdhcy7i50jn7z") (y #t)))

(define-public crate-codespan_preprocessed-0.4.6 (c (n "codespan_preprocessed") (v "0.4.6") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "04fvw80dh84fqs3n1xfpba8pw20db9576iw8524cmwra2yh1mmam") (y #t)))

(define-public crate-codespan_preprocessed-0.4.7 (c (n "codespan_preprocessed") (v "0.4.7") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "04gd8c8hgw561vkh9pn41l208sjli9fpmngikv74vqv5hiywl09m") (y #t)))

(define-public crate-codespan_preprocessed-0.5.0 (c (n "codespan_preprocessed") (v "0.5.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0qyfgh5gvnjhc98nk61hghhid6fkj78dkydjysci1blrjiyz8ivc") (y #t)))

(define-public crate-codespan_preprocessed-0.5.1 (c (n "codespan_preprocessed") (v "0.5.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "12n58796v6nf8fzf47djg7i32i7jll9w74lf0s17p7blbbz3hn3j") (y #t)))

(define-public crate-codespan_preprocessed-0.5.2 (c (n "codespan_preprocessed") (v "0.5.2") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0d7rzxrdz5m8ri8bwyb0l7zq7rsspy0azwxbsyi2wr088zw22wyk") (y #t)))

(define-public crate-codespan_preprocessed-0.5.3 (c (n "codespan_preprocessed") (v "0.5.3") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0bwwar6kw5p45hrymzyq93g5sdi8cifj2dl83szac32ybfmp38s6") (y #t)))

(define-public crate-codespan_preprocessed-0.5.4 (c (n "codespan_preprocessed") (v "0.5.4") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0kagxqi523i7qsld8wxfqvwym358rx0v9fxbph9a43812d2pdqvv") (y #t)))

(define-public crate-codespan_preprocessed-0.5.5 (c (n "codespan_preprocessed") (v "0.5.5") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "00dzlwnrlakwcnz4lwyrhwf7j8h5kfgw5wvcz05wg009gjz48iay") (y #t)))

(define-public crate-codespan_preprocessed-0.5.6 (c (n "codespan_preprocessed") (v "0.5.6") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1nlvh85c5gp0k9hcwapq0gc6x64kgizgxq87c3zhnqsp8h47n2ci") (y #t)))

(define-public crate-codespan_preprocessed-0.5.7 (c (n "codespan_preprocessed") (v "0.5.7") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "0fyhsh7q29rhdmg7p4hjh0l3ndh47j65c5ddiq8kps9iv9inrb93") (y #t)))

(define-public crate-codespan_preprocessed-0.5.8 (c (n "codespan_preprocessed") (v "0.5.8") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "147q8f4mymjwr42l40slwcv8w10kvdlw3n1905fg18dlyxw8vvvs") (y #t)))

(define-public crate-codespan_preprocessed-0.5.9 (c (n "codespan_preprocessed") (v "0.5.9") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "0g7jw250ynf5niiz3fwp2qrb0811pmjj0n620ynr67vvz00xqbv0") (y #t)))

(define-public crate-codespan_preprocessed-0.5.10 (c (n "codespan_preprocessed") (v "0.5.10") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "0x7msvp9pakbmxyn5bk01gf0hrd4bczip2qdapb919br4czsh7nv") (y #t)))

(define-public crate-codespan_preprocessed-0.5.11 (c (n "codespan_preprocessed") (v "0.5.11") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "16h5swlc6a0aplvf7grap6r6dhmpdhjq22idzl90h03awn6js5ym") (y #t)))

(define-public crate-codespan_preprocessed-0.5.13 (c (n "codespan_preprocessed") (v "0.5.13") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "0mvnxkyzf0mvhrm9c887mmfya5gfw4sc20db0w74b5zbk107r36z") (y #t)))

(define-public crate-codespan_preprocessed-0.5.14 (c (n "codespan_preprocessed") (v "0.5.14") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "0b9nzxziqp37ybm53njar0kp2mlpikyjfrp5np9kx4apmxd2wkd6") (y #t)))

(define-public crate-codespan_preprocessed-0.5.15 (c (n "codespan_preprocessed") (v "0.5.15") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "05kahcxdb9vfi7vllnsn7zr420fdny4cvx7n811czv2jbg3y7m4v")))

(define-public crate-codespan_preprocessed-0.6.1 (c (n "codespan_preprocessed") (v "0.6.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "1q3d8lrpd9lbd57s4v93az0xd7ikg1bwa6nkm4y9ffpn9ipfy4y1") (y #t)))

(define-public crate-codespan_preprocessed-0.6.2 (c (n "codespan_preprocessed") (v "0.6.2") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "0m5da7yf9qq9xzymckk06simndn7j5pcfhvnvg3axmzwwyqi6dw6") (y #t)))

(define-public crate-codespan_preprocessed-0.6.3 (c (n "codespan_preprocessed") (v "0.6.3") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "01kyxzb268jc0xkdiqyl3j5h26ib6zrd2ikkaimvnrbbv94iassh")))

(define-public crate-codespan_preprocessed-0.7.0 (c (n "codespan_preprocessed") (v "0.7.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "1ivp34w94mvrr8604vy6y63f3ypx54yd0rlcv5vzq7fjs53k3rzr") (y #t)))

(define-public crate-codespan_preprocessed-0.7.1 (c (n "codespan_preprocessed") (v "0.7.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "1ls04dsfff5sk9f09cqckv8rzmvds1b9gnlkc8hvp6lf85g45m90") (y #t)))

(define-public crate-codespan_preprocessed-0.7.2 (c (n "codespan_preprocessed") (v "0.7.2") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "1qd8z3w7rlhwj9x9wv3liiyjnaz0zjl78zm537rxfxqn0rsgds9s") (y #t)))

(define-public crate-codespan_preprocessed-0.7.3 (c (n "codespan_preprocessed") (v "0.7.3") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "0pip9x9p8zn790vdc7mm3m92b2jhj9n13ngv9c0fzkcrr3qjqzi0") (y #t)))

(define-public crate-codespan_preprocessed-0.7.4 (c (n "codespan_preprocessed") (v "0.7.4") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.1.8") (d #t) (k 2)))) (h "1rv6va54zkf1yygyaz2yi358v3ak4z1cf2fjd41z3a74cnk3rhr1") (y #t)))

(define-public crate-codespan_preprocessed-0.7.5 (c (n "codespan_preprocessed") (v "0.7.5") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "1gvmc5n37yr5xz08jnvqp43zwqf6p0k2v2sdkldqf0pnap3ii5nk") (y #t)))

(define-public crate-codespan_preprocessed-0.7.6 (c (n "codespan_preprocessed") (v "0.7.6") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "0v829ghqgfydv4abl109wi0hq16djyypbabgzdb66809if8s09pa")))

(define-public crate-codespan_preprocessed-0.7.7 (c (n "codespan_preprocessed") (v "0.7.7") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "0a5jiz1z82jj4h8jzrhilppnqw6cs3ih6jy23zjkf6cr8r2dgmn7") (y #t)))

(define-public crate-codespan_preprocessed-0.7.8 (c (n "codespan_preprocessed") (v "0.7.8") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "143vzp8njffg33l9zbd3kjvp6rsidws7k3h6fgn2bj6a0zrm47fn") (y #t)))

(define-public crate-codespan_preprocessed-0.7.9 (c (n "codespan_preprocessed") (v "0.7.9") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "08byb419ahf7050mw46xsgx26mv26ivclqpzmc96rxar3g2hvxra") (y #t)))

(define-public crate-codespan_preprocessed-0.7.10 (c (n "codespan_preprocessed") (v "0.7.10") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "0k4nmvvk15xchgpdavms5zalyv4id9qvjqy85rar86bkfj26bbrd") (y #t)))

(define-public crate-codespan_preprocessed-0.7.11 (c (n "codespan_preprocessed") (v "0.7.11") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "0hp0c7ghlxpdxprnlz7sz1fk8w2fyn5srbm7xgvkw6y3bl7xldwm")))

