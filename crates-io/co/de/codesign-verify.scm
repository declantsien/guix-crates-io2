(define-module (crates-io co de codesign-verify) #:use-module (crates-io))

(define-public crate-codesign-verify-0.1.0 (c (n "codesign-verify") (v "0.1.0") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "rust-crypto") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "processenv" "processthreadsapi" "softpub" "tlhelp32" "winbase" "winerror" "winnt" "wincrypt" "wintrust" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0lhr6apjm8xzcx06fbkjfph8hkfx3lnamjslc1w21nnd235z5ap2")))

(define-public crate-codesign-verify-0.1.1 (c (n "codesign-verify") (v "0.1.1") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "rust-crypto") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "processenv" "processthreadsapi" "softpub" "tlhelp32" "winbase" "winerror" "winnt" "wincrypt" "wintrust" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mlg41i5lyih6d3qqgyswrz1dsvrmgxg9pfav9g2kkp7qhahp72l")))

(define-public crate-codesign-verify-0.1.2 (c (n "codesign-verify") (v "0.1.2") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "rust-crypto") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "processenv" "processthreadsapi" "softpub" "tlhelp32" "winbase" "winerror" "winnt" "wincrypt" "wintrust" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "042s5sssj0b2xkjlwr2xzkqj0cpg0mxmj03w01b83zcaz3bfgp27")))

