(define-module (crates-io co de codeslide-cli) #:use-module (crates-io))

(define-public crate-codeslide-cli-0.6.3 (c (n "codeslide-cli") (v "0.6.3") (d (list (d (n "askama") (r "^0.12.0") (f (quote ("serde-json"))) (d #t) (k 0)) (d (n "if_chain") (r "^1.0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde-1" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "05szsm2kv6f6vm0zwxfg97740jspnb5iwpnyzvvsp47xis0dwxi1") (y #t)))

(define-public crate-codeslide-cli-0.6.4 (c (n "codeslide-cli") (v "0.6.4") (d (list (d (n "askama") (r "^0.12.0") (f (quote ("serde-json"))) (d #t) (k 0)) (d (n "if_chain") (r "^1.0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde-1" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0bd1cz7l6rb9b79dayi2gphv9nfjwciyf1pyy7x5r36z2icrw2w5") (y #t)))

(define-public crate-codeslide-cli-0.6.6 (c (n "codeslide-cli") (v "0.6.6") (d (list (d (n "askama") (r "^0.12.0") (f (quote ("serde-json"))) (d #t) (k 0)) (d (n "if_chain") (r "^1.0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde-1" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1cr0rc8i91l3bbck33shm1za3s88ry5c21wnl0qr131l13zswmic") (y #t)))

(define-public crate-codeslide-cli-0.6.7 (c (n "codeslide-cli") (v "0.6.7") (d (list (d (n "askama") (r "^0.12.0") (f (quote ("serde-json"))) (d #t) (k 0)) (d (n "if_chain") (r "^1.0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde-1" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1pwcqsh9drx3q2xmx9lxxfzwkpl5i0n6mzsfv0zx11bcqm5vlr6d") (y #t)))

(define-public crate-codeslide-cli-0.6.9 (c (n "codeslide-cli") (v "0.6.9") (d (list (d (n "askama") (r "^0.12.0") (f (quote ("serde-json"))) (d #t) (k 0)) (d (n "if_chain") (r "^1.0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde-1" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1jggjrpjxj7845ba4s12v5ahbmbcs4nvxh1gdgl907bwss3alw08") (y #t)))

(define-public crate-codeslide-cli-0.7.3 (c (n "codeslide-cli") (v "0.7.3") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "12771w40k6fy3afamcpm217qknhhkhlyl86kmsq5gbp4mq9ff480") (y #t)))

(define-public crate-codeslide-cli-0.7.4 (c (n "codeslide-cli") (v "0.7.4") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0wk5bf9vj7g0harn2h13xhzs6maiidhlz15sw75nwa4yfamsh2q3") (y #t)))

(define-public crate-codeslide-cli-0.7.5 (c (n "codeslide-cli") (v "0.7.5") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0khyq365ky385vrcc79sdsmnhvcq32q703978w1lwv33rw7r943h") (y #t)))

(define-public crate-codeslide-cli-0.7.6 (c (n "codeslide-cli") (v "0.7.6") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "178vw98npq1i39r9n75i69s9kbrkqglz0hghmhdw0ypw62wgms9n") (y #t)))

(define-public crate-codeslide-cli-0.7.7 (c (n "codeslide-cli") (v "0.7.7") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0m6mncrggqymacpi42vgsvmn99a6vnq6d0byrskva4p452lwgv79") (y #t)))

(define-public crate-codeslide-cli-0.7.8 (c (n "codeslide-cli") (v "0.7.8") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1bzfazsdl02izsvl4xlxzp87lx2bq9sfm75hbnkc0k89ma2dlw3z") (y #t)))

(define-public crate-codeslide-cli-0.7.9 (c (n "codeslide-cli") (v "0.7.9") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "14zvksmjrr7bsal28qjascy810zmgakcawzz2c7w83irnpzd9x0s") (y #t)))

(define-public crate-codeslide-cli-0.7.10 (c (n "codeslide-cli") (v "0.7.10") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1lgf60niyy6bvf2dbww148kkrpg2kah0bx38p03lvq5n685i7ssa") (y #t)))

(define-public crate-codeslide-cli-0.7.11 (c (n "codeslide-cli") (v "0.7.11") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0627c4jvncy8wf20ayz7kvrknf63znfmy1sw3yf6b02iwmqi22z5") (y #t)))

(define-public crate-codeslide-cli-0.7.12 (c (n "codeslide-cli") (v "0.7.12") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "11xlnqnn1mzfbcwm7vdwg56ijlv5n0f1wd97wmm32689kbnv9mx4") (y #t)))

(define-public crate-codeslide-cli-0.7.13 (c (n "codeslide-cli") (v "0.7.13") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "14s4apfnrh7l99r5nrc6f6x01p8k0wh2ycs6yamg2cg3brla4w7k") (y #t)))

(define-public crate-codeslide-cli-0.8.0 (c (n "codeslide-cli") (v "0.8.0") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0jwsgdzxhgjl8fg10dlp19m3qfyws01mqlnyj2r6av0g232g34xd") (y #t)))

(define-public crate-codeslide-cli-0.8.1 (c (n "codeslide-cli") (v "0.8.1") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "009n4jx1a083j7rwrhg382a49g9x9bjdxxdj8q207xm8z62908sk") (y #t)))

(define-public crate-codeslide-cli-0.8.2 (c (n "codeslide-cli") (v "0.8.2") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1wi1n3v7ynglzx2rr6yjdq3ah66ygdh112ps8q4si61prlpwsmac") (y #t)))

(define-public crate-codeslide-cli-0.8.3 (c (n "codeslide-cli") (v "0.8.3") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "18yajpfijnj3yjmddz0pc6jc9g9ijjrfnq0xx9rgc43k6p8banh9") (y #t)))

(define-public crate-codeslide-cli-0.9.0 (c (n "codeslide-cli") (v "0.9.0") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0xvld1sbj06jyijdg3fddz33dn81f324q41qmvlzz4ndi2r1pnbq") (y #t)))

(define-public crate-codeslide-cli-0.9.1 (c (n "codeslide-cli") (v "0.9.1") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "headless_chrome") (r "^1.0.5") (d #t) (k 0)) (d (n "minifier") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "02d11sz6dxhngs08njxhvlik5zk8iw2vkv90v8cmkpbjwx5yv4px") (y #t)))

