(define-module (crates-io co de codesigned) #:use-module (crates-io))

(define-public crate-codesigned-0.0.2 (c (n "codesigned") (v "0.0.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "widestring") (r "^1.0.0-beta.1") (d #t) (k 0)))) (h "0a4civwr704hc7zmi91lrz7im3iyn569gxr9j2k7rv7l0zicxdl8")))

