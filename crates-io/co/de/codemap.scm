(define-module (crates-io co de codemap) #:use-module (crates-io))

(define-public crate-codemap-0.1.0 (c (n "codemap") (v "0.1.0") (h "0lscnzppzj6jzybc269znnxy7lcxy7a6121bjg3yz64n79d4kp18")))

(define-public crate-codemap-0.1.1 (c (n "codemap") (v "0.1.1") (h "1qdvkiw78vjy18xhi48dcm7wdk31j9v978pijgam5ynlmnz03kx9")))

(define-public crate-codemap-0.1.2 (c (n "codemap") (v "0.1.2") (h "0wpdjbh42fihnax65snr3f26ming1nyf1l7yamha1k6bnyah1adz")))

(define-public crate-codemap-0.1.3 (c (n "codemap") (v "0.1.3") (h "091azkslwkcijj3lp9ymb084y9a0wm4fkil7m613ja68r2snkrxr")))

