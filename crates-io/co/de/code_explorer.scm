(define-module (crates-io co de code_explorer) #:use-module (crates-io))

(define-public crate-code_explorer-0.0.1 (c (n "code_explorer") (v "0.0.1") (d (list (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20.4") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20.5") (d #t) (k 0)))) (h "05j9injswqip42n2jnmm464d92nb634vmhaa2qiykzl9fj49zima")))

(define-public crate-code_explorer-0.0.2 (c (n "code_explorer") (v "0.0.2") (d (list (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.20.4") (d #t) (k 0)) (d (n "tree-sitter-typescript") (r "^0.20.5") (d #t) (k 0)))) (h "180xl416d1lr551ykkxcwfpkaskcx50vpcsxcmf5jjwrp67pcw2w")))

