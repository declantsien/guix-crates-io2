(define-module (crates-io co de codeviz_python) #:use-module (crates-io))

(define-public crate-codeviz_python-0.1.0 (c (n "codeviz_python") (v "0.1.0") (d (list (d (n "codeviz_common") (r "^0.1.0") (d #t) (k 0)))) (h "1n17pijyrj54gb9lqz9s47qfvjag4d7l2hfaid1bh9422dm90jkk")))

(define-public crate-codeviz_python-0.1.1 (c (n "codeviz_python") (v "0.1.1") (d (list (d (n "codeviz_common") (r "^0.1.1") (d #t) (k 0)))) (h "0j30fq4lj75zj04apccy0l18pd3q032gv6n2mxydc3ilaj0sa2a4")))

(define-public crate-codeviz_python-0.2.0 (c (n "codeviz_python") (v "0.2.0") (d (list (d (n "codeviz_common") (r "^0.2.0") (d #t) (k 0)))) (h "08y5zx7nbvklvpxgsz0w8g9j3rajhbvzbjmxmj6ag91vx2ff24f7")))

(define-public crate-codeviz_python-0.2.1 (c (n "codeviz_python") (v "0.2.1") (d (list (d (n "codeviz_common") (r "^0.2.1") (d #t) (k 0)))) (h "0qswhw4n9gisiqg05ds8zs5dylbyhjbf1bnsd00dsnlnwn2ga9dx")))

(define-public crate-codeviz_python-0.2.2 (c (n "codeviz_python") (v "0.2.2") (d (list (d (n "codeviz_common") (r "^0.2.2") (d #t) (k 0)))) (h "15ppj0k8myrajfjxd5l59ahf7s42j6b0y0j39qjbml1wvr0xb76g")))

