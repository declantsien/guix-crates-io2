(define-module (crates-io co de codeowners-enforcer) #:use-module (crates-io))

(define-public crate-codeowners-enforcer-1.0.0 (c (n "codeowners-enforcer") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "codeowners") (r "^0.1.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4.7") (d #t) (k 0)))) (h "1s177l1866j8vryiybkwy2dnb0pclgcxq5fi6w429063az3zbssh")))

(define-public crate-codeowners-enforcer-1.0.1 (c (n "codeowners-enforcer") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "codeowners") (r "^0.1.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4.7") (d #t) (k 0)))) (h "066wzgbac1gapzs58xp07920nwsf2zdji7721jp6gvdana8aj6cy")))

(define-public crate-codeowners-enforcer-1.0.3 (c (n "codeowners-enforcer") (v "1.0.3") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "codeowners") (r "^0.1.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4.7") (d #t) (k 0)))) (h "15lx0jr5sca18nzdhi5a1560wdglfnpcxg9v3xdviy282a3gn5k6")))

