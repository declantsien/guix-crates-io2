(define-module (crates-io co de code-explain) #:use-module (crates-io))

(define-public crate-code-explain-0.0.10 (c (n "code-explain") (v "0.0.10") (h "1gmn4pcn343fkvj3yf3dvyadsvmsj30nzibwy33zwd03kzykm8i7")))

(define-public crate-code-explain-0.0.11 (c (n "code-explain") (v "0.0.11") (h "0x3698jyz2d4000bfyr3l1k3rzcjbrfp0myx79k1gzc3kdhqcxi9")))

(define-public crate-code-explain-0.0.12 (c (n "code-explain") (v "0.0.12") (h "153vsjb769qx5ik02f5h8m93mh1q98m762b3yi9wq59xc0pkh5w5")))

(define-public crate-code-explain-0.0.13 (c (n "code-explain") (v "0.0.13") (h "09xygrbyshqigkv406v268wafzjvpywgqqlmnlvm0n2wvhbb9x8x")))

