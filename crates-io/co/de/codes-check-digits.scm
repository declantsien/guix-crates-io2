(define-module (crates-io co de codes-check-digits) #:use-module (crates-io))

(define-public crate-codes-check-digits-0.1.0 (c (n "codes-check-digits") (v "0.1.0") (d (list (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "rug") (r "^1.18.0") (f (quote ("integer"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1x9bza1iwqdniw7gby3dk4bha44hj0ycwnkn1mnfy1srrbddyy5n") (f (quote (("sedol") ("luhn") ("iso_7064") ("gs1") ("default" "gs1" "iso_7064" "luhn" "sedol" "big_integer") ("big_integer"))))))

(define-public crate-codes-check-digits-0.1.1 (c (n "codes-check-digits") (v "0.1.1") (d (list (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "rug") (r "^1.18.0") (f (quote ("integer"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "001kywjq5hs84y2fpn1diip2w5zyjbc4c4nvs5z3zfssv487lb81") (f (quote (("sedol") ("luhn") ("iso_7064") ("gs1") ("default" "gs1" "iso_7064" "luhn" "sedol" "big_integer") ("big_integer" "rug"))))))

(define-public crate-codes-check-digits-0.1.2 (c (n "codes-check-digits") (v "0.1.2") (d (list (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "rug") (r "^1.18.0") (f (quote ("integer"))) (o #t) (t "cfg(not(target_family = \"windows\"))") (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0rf5f64nqji8ni925794zfi91x2ap6w248dci361jc4m01kza8l2") (f (quote (("sedol") ("luhn") ("iso_7064") ("gs1") ("default" "gs1" "iso_7064" "luhn" "sedol")))) (s 2) (e (quote (("big_integer" "dep:rug"))))))

