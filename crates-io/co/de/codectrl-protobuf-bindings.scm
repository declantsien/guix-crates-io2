(define-module (crates-io co de codectrl-protobuf-bindings) #:use-module (crates-io))

(define-public crate-codectrl-protobuf-bindings-0.8.2 (c (n "codectrl-protobuf-bindings") (v "0.8.2") (d (list (d (n "native") (r "^0.0.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0) (p "codectrl-bindings-native")) (d (n "web") (r "^0.0.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0) (p "codectrl-bindings-web")))) (h "0dzwy8dvk90dx3ngzm3gam2csgr4krclgyfjnbmj3k3kzl4d8xz8")))

