(define-module (crates-io co de codegem) #:use-module (crates-io))

(define-public crate-codegem-0.1.0 (c (n "codegem") (v "0.1.0") (h "0lz41z8680x0kmak8k0npz39rgpfslfr04rq1kykf3dwgwzz9fwq") (y #t)))

(define-public crate-codegem-0.1.1 (c (n "codegem") (v "0.1.1") (h "1g5adwnmx6kicij77pcpa3vicm02rsx0nylm5hnnpir929v5cmyp")))

(define-public crate-codegem-0.1.2 (c (n "codegem") (v "0.1.2") (h "1m06vxn87w6lm3fnvfyhc0m56jpzzbr7szv2acj6009j4v94jqdh")))

(define-public crate-codegem-0.1.3 (c (n "codegem") (v "0.1.3") (h "19zww2g7jkszags2qbz6lq3dbzj47b07vkfi2fjpnrs4nmbx85mb")))

(define-public crate-codegem-0.1.4 (c (n "codegem") (v "0.1.4") (h "0ap5m881yrhdkwjd5sp1n73qcvsp7l0gzpqjiac7y5ykc1d22ajv")))

(define-public crate-codegem-0.1.5 (c (n "codegem") (v "0.1.5") (h "06gsflzz28inj7d9hqz49rhd4mdaya35iljymwpnqdxq3hvwylrn")))

(define-public crate-codegem-0.2.0 (c (n "codegem") (v "0.2.0") (h "1fnnyj9lb1p8llyfklz207p7pdcchqvsm37lskg72gmhscl00a7k")))

(define-public crate-codegem-0.2.1 (c (n "codegem") (v "0.2.1") (h "0x8b6psb6kbm6sxiksh7dbpnnqvqnzwapv6acip9dn5dw5xrcync")))

(define-public crate-codegem-0.2.2 (c (n "codegem") (v "0.2.2") (h "0n08vp7h3gyw1isz3d1vgllv3qcv6f0jmvpv45i3krs6r3wwqkyi")))

(define-public crate-codegem-0.2.3 (c (n "codegem") (v "0.2.3") (h "1jv5d9mxjk3q4p2sci62ij5a269i4vjsm6jb3fbdpccbix775ics")))

