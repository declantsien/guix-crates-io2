(define-module (crates-io co de codeowners) #:use-module (crates-io))

(define-public crate-codeowners-0.1.0 (c (n "codeowners") (v "0.1.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1h3ji7whkahmdvqlsa6ghcm01jpwm7hdrm3pib7iyl17vxfyaqq6")))

(define-public crate-codeowners-0.1.1 (c (n "codeowners") (v "0.1.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "07gj20vmlx6nxxhk2i8siqnhn1avyi82zwivqfphmphq49rl15cj")))

(define-public crate-codeowners-0.1.2 (c (n "codeowners") (v "0.1.2") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0fb3c5pjdxlqq422gq8bycgq176bfivcx7g5g9hc70ls5dsnph1b")))

(define-public crate-codeowners-0.1.3 (c (n "codeowners") (v "0.1.3") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1gvys08b1ak46p0mb9821sysxbhz9w15xjkg3m9a3arvj3fcnran")))

