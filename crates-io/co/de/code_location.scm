(define-module (crates-io co de code_location) #:use-module (crates-io))

(define-public crate-code_location-1.0.0-beta.1 (c (n "code_location") (v "1.0.0-beta.1") (h "1xkqb1r80i3jhakz6ivrf22rxdn1gdpqwxqfwy26yd5kafa9hwb3")))

(define-public crate-code_location-1.0.0 (c (n "code_location") (v "1.0.0") (h "13v9zybxn3q7ycc66ql46gsndais4y868jqkgsfbg0cyvg585q2c")))

(define-public crate-code_location-1.0.1 (c (n "code_location") (v "1.0.1") (h "1hj6qrw89h354qbwaq2qj6azw7cgybgsw1f04234x65sq3m668g2")))

(define-public crate-code_location-1.1.0 (c (n "code_location") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bp4hwxh9b1j17zm5q4y65y9pkbna7qr4xl1z3zr7jam9g557izg")))

(define-public crate-code_location-1.1.1 (c (n "code_location") (v "1.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1pq7rw871aig2j3s4ncifyvvk03vl8w5z97dmy3d01bcckspvjay")))

