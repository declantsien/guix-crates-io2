(define-module (crates-io co de codeviz_common) #:use-module (crates-io))

(define-public crate-codeviz_common-0.1.0 (c (n "codeviz_common") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0im2ybyc6ric33s7winm56n46zhmlfck8z4sp8mdlmra25kds0i7")))

(define-public crate-codeviz_common-0.1.1 (c (n "codeviz_common") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0xvmrczh6yc78piy71ppc7x0h8i6vg7ch658ghk6arh1gnz4abbn")))

(define-public crate-codeviz_common-0.2.0 (c (n "codeviz_common") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1hqn18krml3cqj9avc56cskknls0xxn0sc7nwzkyf1pja56wrsf2")))

(define-public crate-codeviz_common-0.2.1 (c (n "codeviz_common") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "08alfa9ikwv1xsm23zbdhsjng0xagsfgxr8c1an9ml7f9d3pchbv")))

(define-public crate-codeviz_common-0.2.2 (c (n "codeviz_common") (v "0.2.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "0187k984ahgki5shi824xnznjwn0g9fi936kl3r9wrinh4vvfavc")))

