(define-module (crates-io co de codepage) #:use-module (crates-io))

(define-public crate-codepage-0.0.1 (c (n "codepage") (v "0.0.1") (d (list (d (n "encoding_rs") (r "^0.7.2") (d #t) (k 0)))) (h "0yk54mrj3l8hcr2017lyxb75wgkxwdk2l0qx3hvbgnvzdscbfadl")))

(define-public crate-codepage-0.1.0 (c (n "codepage") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.13") (d #t) (k 0)))) (h "0h5y87z65flbxvk7j29fazf1hfmf82ij5j1kb23q68kq5yrd0na8")))

(define-public crate-codepage-0.1.1 (c (n "codepage") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.13") (d #t) (k 0)))) (h "1sdxp0f8dy2rscln646326lny9i0jm7ncgyp4yncdwndq0i943lb")))

