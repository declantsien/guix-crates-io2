(define-module (crates-io co de code_runner) #:use-module (crates-io))

(define-public crate-code_runner-0.1.0 (c (n "code_runner") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1iwk87ca16c9crk1a0kaql4jc9qrkmqfqv189d3xgcpb7gas96x0")))

