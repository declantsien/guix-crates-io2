(define-module (crates-io co de codebiber) #:use-module (crates-io))

(define-public crate-codebiber-0.0.0 (c (n "codebiber") (v "0.0.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 2)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "unwrap_display") (r "^0.0.1") (d #t) (k 2)))) (h "12nl42qmjcsb9xc4z5p2cf02q7g39x5zxq5jb05bzrcbiqmr7lhb")))

(define-public crate-codebiber-0.0.1 (c (n "codebiber") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "blake3") (r "=1.4.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 2)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "unwrap_display") (r "^0.0.1") (d #t) (k 2)))) (h "1h1qfv8a366qv9qaxchvmr3gv1jbdvpvq4jkbjadfzf72ds3asaq") (r "1.63")))

