(define-module (crates-io co de codeplatter) #:use-module (crates-io))

(define-public crate-codeplatter-0.0.1 (c (n "codeplatter") (v "0.0.1") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "iron") (r "^0.2.6") (d #t) (k 0)) (d (n "router") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "1fxxwwrs6rwqxc6k22q9wg5mhsygkpcs2ndk2wdm5v4gqx0awia0")))

(define-public crate-codeplatter-0.1.0 (c (n "codeplatter") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "iron") (r "^0.4.0") (d #t) (k 0)) (d (n "router") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0g0w2b4hbbvbikgfrgbma8dqvr9v67pjycgmi306vlcw59kzihp8")))

