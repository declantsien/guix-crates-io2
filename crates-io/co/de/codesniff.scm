(define-module (crates-io co de codesniff) #:use-module (crates-io))

(define-public crate-codesniff-0.1.0 (c (n "codesniff") (v "0.1.0") (h "1i1k6qg3vlvjzskxli3nssa72lz476m1jnyzxh30rr60s05r0kls")))

(define-public crate-codesniff-0.1.1 (c (n "codesniff") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (f (quote ("all-widgets"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "1qzls8m1ik9fhjhr04r2axpcnv2dspnkrvqfhy7wa4dbpg2cfwf6")))

(define-public crate-codesniff-0.1.2 (c (n "codesniff") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (f (quote ("all-widgets"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "067bgi25q4hpkyg3qp61fxwg6kzx0pnh7wdan2av384bs58vdk7g")))

