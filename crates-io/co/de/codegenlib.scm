(define-module (crates-io co de codegenlib) #:use-module (crates-io))

(define-public crate-CodeGenLib-0.0.1 (c (n "CodeGenLib") (v "0.0.1") (d (list (d (n "faerie") (r "^0.16.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)))) (h "189ildbkfmahp33hhdkhx5r9d2ad8jy2hjcca9qip1jx086zd73g") (f (quote (("jit") ("default" "all") ("all" "default" "jit"))))))

