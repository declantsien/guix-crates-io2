(define-module (crates-io co de coderpwh_pipeviewer) #:use-module (crates-io))

(define-public crate-coderpwh_pipeviewer-0.1.0 (c (n "coderpwh_pipeviewer") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "03h3swnc8cxdkzlhycjd8l7mfn6rrlfm2hcw9g1i5q1gv8xahpis")))

