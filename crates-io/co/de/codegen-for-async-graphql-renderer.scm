(define-module (crates-io co de codegen-for-async-graphql-renderer) #:use-module (crates-io))

(define-public crate-codegen-for-async-graphql-renderer-0.1.0 (c (n "codegen-for-async-graphql-renderer") (v "0.1.0") (d (list (d (n "async-graphql") (r "^1.15.14") (d #t) (k 0)) (d (n "async-graphql-parser") (r "^1.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "toolchain_find") (r "^0.1") (d #t) (k 0)))) (h "014c36qjf6c8g3i073ma54y54xlkdb5n74qkng110jq3kb0maqxz") (y #t)))

(define-public crate-codegen-for-async-graphql-renderer-0.2.0 (c (n "codegen-for-async-graphql-renderer") (v "0.2.0") (d (list (d (n "async-graphql") (r "^1.15.14") (d #t) (k 0)) (d (n "async-graphql-parser") (r "^1.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "toolchain_find") (r "^0.1") (d #t) (k 0)))) (h "1xy7glvyk0ni07kwfvc7abidnm05l9hprjs52l1pmazwz55kc3d7") (y #t)))

(define-public crate-codegen-for-async-graphql-renderer-0.2.1 (c (n "codegen-for-async-graphql-renderer") (v "0.2.1") (d (list (d (n "async-graphql") (r "^1.15.14") (d #t) (k 0)) (d (n "async-graphql-parser") (r "^1.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "toolchain_find") (r "^0.1") (d #t) (k 0)))) (h "10dnzdqpvwrq58rwa95saig8mf2nhz5qlhm15r8s2h23xww5d55w")))

(define-public crate-codegen-for-async-graphql-renderer-0.2.5 (c (n "codegen-for-async-graphql-renderer") (v "0.2.5") (d (list (d (n "async-graphql") (r "^1.16.6") (d #t) (k 0)) (d (n "async-graphql-parser") (r "^1.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "toolchain_find") (r "^0.1") (d #t) (k 0)))) (h "1xic2f29zb35q0nldbg8gk9kndhvbr9l4j0izd2dk720l3kxhana")))

(define-public crate-codegen-for-async-graphql-renderer-0.2.6 (c (n "codegen-for-async-graphql-renderer") (v "0.2.6") (d (list (d (n "async-graphql") (r "^1.16.6") (d #t) (k 0)) (d (n "async-graphql-parser") (r "^1.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "toolchain_find") (r "^0.1") (d #t) (k 0)))) (h "0kx6sb51jjjlqx7iakypmh8wwdk30qqhksphjy9m68ng0sl157r7")))

