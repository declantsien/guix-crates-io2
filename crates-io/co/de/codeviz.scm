(define-module (crates-io co de codeviz) #:use-module (crates-io))

(define-public crate-codeviz-0.0.1 (c (n "codeviz") (v "0.0.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "04mqmajsi9nz8jri1z096zwmbkf7ripip20jiqw91sdk8chkgbw9")))

(define-public crate-codeviz-0.0.2 (c (n "codeviz") (v "0.0.2") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "1bjzwhb8k09ijmgrw3am8nffbaxn56yjrf20as47pbqfr5x772xl")))

(define-public crate-codeviz-0.0.3 (c (n "codeviz") (v "0.0.3") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "1mikg38fkwdfd2lg1yh0fccgyb01yrbpnsy5pih8zbwx51zaij6a")))

(define-public crate-codeviz-0.0.4 (c (n "codeviz") (v "0.0.4") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0s8053rm94qb0xw0gcq95rs2byd6fhssv4fm19fm3lhpfrwvj6np")))

(define-public crate-codeviz-0.0.5 (c (n "codeviz") (v "0.0.5") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "03wwzj9whx4w9y6xs9xdl7paiww5l32xnzl2ki565l4dvsr5x94d")))

(define-public crate-codeviz-0.0.6 (c (n "codeviz") (v "0.0.6") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0f047lvkb02jgx7jkyk88hlhl6ffdxay9wn5x9dlzq7x4a2fsn5g")))

(define-public crate-codeviz-0.0.7 (c (n "codeviz") (v "0.0.7") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "176v4ha8d0hdkdxnsr44x2k4zy0b47qh71nzaq8dzywx7xlqxnb8")))

(define-public crate-codeviz-0.0.8 (c (n "codeviz") (v "0.0.8") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "18jr599cv835rzpcd8da1cmrlqpr0cx8g5aamv3xb1ibqw48miw9")))

(define-public crate-codeviz-0.0.9 (c (n "codeviz") (v "0.0.9") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0cirhfx6d0mpzbvffswbn17ss1qnx9haicy9crr3wna595h6hpha")))

(define-public crate-codeviz-0.0.10 (c (n "codeviz") (v "0.0.10") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0m63xbf0j8d4pwj538nhcpxx3qcdaf6p0s1wncari9y3g8vscvxz")))

(define-public crate-codeviz-0.0.11 (c (n "codeviz") (v "0.0.11") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "1n3klk31cgvbqcpdmxjf2mqynypqpfm0vripybsbkkwnf91q7m5j")))

(define-public crate-codeviz-0.0.12 (c (n "codeviz") (v "0.0.12") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0px4myqkzn31s81jfkrhplygccfny07vdy307jyj21ws5hgzklp4")))

(define-public crate-codeviz-0.0.13 (c (n "codeviz") (v "0.0.13") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "02cijapp4adhwlrslx2361c74wbmp7lzgqcdd0pvnm65sfs067pm")))

(define-public crate-codeviz-0.0.14 (c (n "codeviz") (v "0.0.14") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "1sxar1jj4qnfjpr6k1b17inyj9cxxihsqz8jxppk5iixc1c0bryj")))

(define-public crate-codeviz-0.0.15 (c (n "codeviz") (v "0.0.15") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0klf4ln375jp3ss924ijyjfvhanx6rvmkpazgc9mhahkncxwkjrg")))

(define-public crate-codeviz-0.0.16 (c (n "codeviz") (v "0.0.16") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "09776yhvc1zmp2fpq8isnmmnqa8pqw71zxb5mqcjszcb8mi4zzmw")))

(define-public crate-codeviz-0.1.0 (c (n "codeviz") (v "0.1.0") (d (list (d (n "codeviz_common") (r "^0.1.0") (d #t) (k 0)) (d (n "codeviz_java") (r "^0.1.0") (d #t) (k 0)) (d (n "codeviz_js") (r "^0.1.0") (d #t) (k 0)) (d (n "codeviz_macros") (r "^0.1.0") (d #t) (k 2)) (d (n "codeviz_python") (r "^0.1.0") (d #t) (k 0)) (d (n "codeviz_rust") (r "^0.1.0") (d #t) (k 0)))) (h "1jz5y1y1g4qqzz1cplk6k68abm88366bbh4k4fwiaazapmqj8ny2")))

(define-public crate-codeviz-0.1.1 (c (n "codeviz") (v "0.1.1") (d (list (d (n "codeviz_common") (r "^0.1.1") (d #t) (k 0)) (d (n "codeviz_java") (r "^0.1.1") (d #t) (k 0)) (d (n "codeviz_js") (r "^0.1.1") (d #t) (k 0)) (d (n "codeviz_macros") (r "^0.1.1") (d #t) (k 2)) (d (n "codeviz_python") (r "^0.1.1") (d #t) (k 0)) (d (n "codeviz_rust") (r "^0.1.1") (d #t) (k 0)))) (h "1w3kblicj2h86r6gbw0zxj5044acz8dhv5d7pjma7rmyi28ggg6a")))

(define-public crate-codeviz-0.2.0 (c (n "codeviz") (v "0.2.0") (d (list (d (n "codeviz_common") (r "^0.2.0") (d #t) (k 0)) (d (n "codeviz_java") (r "^0.2.0") (d #t) (k 0)) (d (n "codeviz_js") (r "^0.2.0") (d #t) (k 0)) (d (n "codeviz_macros") (r "^0.2.0") (d #t) (k 2)) (d (n "codeviz_python") (r "^0.2.0") (d #t) (k 0)) (d (n "codeviz_rust") (r "^0.2.0") (d #t) (k 0)))) (h "0h93n4g5skyj2ky6f9fv4g43z161162vpzfvrcblg106n7w0hfl4")))

(define-public crate-codeviz-0.2.2 (c (n "codeviz") (v "0.2.2") (d (list (d (n "codeviz_common") (r "^0.2.2") (d #t) (k 0)) (d (n "codeviz_java") (r "^0.2.2") (d #t) (k 0)) (d (n "codeviz_js") (r "^0.2.2") (d #t) (k 0)) (d (n "codeviz_macros") (r "^0.2.2") (d #t) (k 2)) (d (n "codeviz_python") (r "^0.2.2") (d #t) (k 0)) (d (n "codeviz_rust") (r "^0.2.2") (d #t) (k 0)))) (h "0srsl3ih1w947rnpix5ad329vy6fkxgj3fbh6iiak1mx9dhjf657")))

