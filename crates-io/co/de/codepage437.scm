(define-module (crates-io co de codepage437) #:use-module (crates-io))

(define-public crate-codepage437-0.1.0 (c (n "codepage437") (v "0.1.0") (d (list (d (n "ega_palette") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19mb403lfwxw9n47njabdsy559xj6p3sfhrf0ak08brhj5qxdgmk")))

