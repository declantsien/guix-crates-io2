(define-module (crates-io co de codes-gs1-gln) #:use-module (crates-io))

(define-public crate-codes-gs1-gln-0.1.0 (c (n "codes-gs1-gln") (v "0.1.0") (d (list (d (n "codes-agency") (r "^0.1") (d #t) (k 0)) (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "codes-common") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tera") (r "^1.17") (d #t) (k 1)))) (h "1ilq052p4k1qm8gv42hcxjlq743j754wy21j8d7b3wmgv1rm353m") (f (quote (("default" "serde"))))))

(define-public crate-codes-gs1-gln-0.1.1 (c (n "codes-gs1-gln") (v "0.1.1") (d (list (d (n "codes-agency") (r "^0.1") (d #t) (k 0)) (d (n "codes-check-digits") (r "^0.1") (f (quote ("gs1"))) (d #t) (k 0)) (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09xwh284v3ma2xxnfkpqcb1i2dfa9vvkpnsc0ddsvb72h79dglpp") (f (quote (("default" "serde"))))))

(define-public crate-codes-gs1-gln-0.1.2 (c (n "codes-gs1-gln") (v "0.1.2") (d (list (d (n "codes-agency") (r "^0.1") (d #t) (k 0)) (d (n "codes-check-digits") (r "^0.1") (f (quote ("gs1"))) (d #t) (k 0)) (d (n "codes-common") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v4j89sc6z3dsq21mb4vh4ra0v60wgblsdix2rqnzsilvmqdm6jk") (f (quote (("default" "serde"))))))

