(define-module (crates-io co de coder543-openldap) #:use-module (crates-io))

(define-public crate-coder543-openldap-0.1.0 (c (n "coder543-openldap") (v "0.1.0") (h "1695xgbvaapjpzcrkw0vd6w7k74n9zdchm9f23n4flxn86sc2l4v")))

(define-public crate-coder543-openldap-0.2.0 (c (n "coder543-openldap") (v "0.2.0") (h "17g1bac3zhgbrz6n82y2l0bby79kfw205sjdrxm3l0zlvd2pap0h")))

(define-public crate-coder543-openldap-0.2.1 (c (n "coder543-openldap") (v "0.2.1") (h "0i14mf0hsw8g870xdlzznsvgybikd8fnbq283bs2ri2glq02dgf8")))

