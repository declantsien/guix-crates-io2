(define-module (crates-io co de code-blocks) #:use-module (crates-io))

(define-public crate-code-blocks-0.1.0 (c (n "code-blocks") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "similar-asserts") (r "^1.4.2") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20.3") (d #t) (k 2)))) (h "1vqdf2aj1lzfjn2bhqgg8i48827vn4dirji81pnjc3bs0gwf5sp3")))

(define-public crate-code-blocks-0.2.0 (c (n "code-blocks") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "insta") (r "^1.26.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "similar-asserts") (r "^1.4.2") (d #t) (k 2)) (d (n "tree-sitter-python") (r "^0.20.2") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20.3") (d #t) (k 2)) (d (n "tree-sitter-typescript") (r "^0.20.2") (d #t) (k 2)))) (h "0laf1n6l2h4d9yaxpd2i8gndl0ay5ixz54wf4180ynrc6fjwmj7y")))

(define-public crate-code-blocks-0.3.0 (c (n "code-blocks") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)) (d (n "insta") (r "^1.26.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "similar-asserts") (r "^1.4.2") (d #t) (k 2)) (d (n "tree-sitter-python") (r "^0.20.2") (d #t) (k 2)) (d (n "tree-sitter-rust") (r "^0.20.3") (d #t) (k 2)) (d (n "tree-sitter-typescript") (r "^0.20.2") (d #t) (k 2)))) (h "1ykbxbd8qi30gxrqay7y7cmv7scq08bbrzr4bl64k0d1brfcwg1a")))

