(define-module (crates-io co de coderboard) #:use-module (crates-io))

(define-public crate-coderboard-0.1.0 (c (n "coderboard") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4.22") (d #t) (k 0)))) (h "0v2agq1cjkbncw6g1kpdkjb6gip8bny1b7gx6gkl2nf3zcm1kg0x")))

