(define-module (crates-io co de code_editor) #:use-module (crates-io))

(define-public crate-code_editor-0.1.0 (c (n "code_editor") (v "0.1.0") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "042bhvf1k0p7d1rhi3d8n908z43kz8nkm7rh7p8fpnj3472jksdy")))

(define-public crate-code_editor-0.2.0 (c (n "code_editor") (v "0.2.0") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "1yidpzy912x4nk172bdsri5ln6iya77qq2qnjcq6g2rwwcpi28vi")))

(define-public crate-code_editor-0.2.1 (c (n "code_editor") (v "0.2.1") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "1fqgwmw2w0gvazwg27mk8xcspzj4l3vr38n8c02qvw3g8ax0570g")))

(define-public crate-code_editor-0.2.2 (c (n "code_editor") (v "0.2.2") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "1iwrvirziw9akqd6hwz206yj656rm22isk4wdnlp04ji7z964y02")))

(define-public crate-code_editor-0.3.0 (c (n "code_editor") (v "0.3.0") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "19gw12sph1wxkhrv7r202gw2vbik9djkshp7vvxr6xnx51ya9m48")))

(define-public crate-code_editor-0.3.1 (c (n "code_editor") (v "0.3.1") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "1jwrlxhmcax2kcd85r6w6hp2i3h7sw7a97gqbxgbzikd7i0zsk6b")))

(define-public crate-code_editor-0.3.2 (c (n "code_editor") (v "0.3.2") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "1g367crrb6izafdhhqgcmdgw3fxa1in7wzq742fygwgsf9hqyg1h")))

(define-public crate-code_editor-0.3.3 (c (n "code_editor") (v "0.3.3") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "1akj06nj1qdvcpfh63pa22mfi28l6dp65xvi54dql9b87877cw3v")))

(define-public crate-code_editor-0.3.4 (c (n "code_editor") (v "0.3.4") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "0d29y9kwaxcq35yfqlfwcvp6f6qvaamgdc8v3bz3yl3pldlf8a6p")))

(define-public crate-code_editor-0.3.5 (c (n "code_editor") (v "0.3.5") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "1kawcjxrxc21mhygjy1m7d1rnff8g4zsr67kgax8wwqks9aly393")))

(define-public crate-code_editor-0.3.6 (c (n "code_editor") (v "0.3.6") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "16qpn9zyaxm0f9yxvfdqi3kl41ja60qwm82v0rqnk9h0ddlw6887")))

(define-public crate-code_editor-0.3.7 (c (n "code_editor") (v "0.3.7") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "1jflwr83w001v8vrvj4jmdp3980map1q33gdywdxz3an9z1djxl7")))

(define-public crate-code_editor-0.3.8 (c (n "code_editor") (v "0.3.8") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "1wys6cmkmymrdwxliyl7fi3qzqb6pic2s1kih72zckrqc56w9nph")))

(define-public crate-code_editor-0.3.9 (c (n "code_editor") (v "0.3.9") (d (list (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)))) (h "0h00jr1ck5b3x97l6bzlnyalwzdj4dqnmjc0cfn8k7f423qb2wh7")))

