(define-module (crates-io co de codemap-diagnostic) #:use-module (crates-io))

(define-public crate-codemap-diagnostic-0.1.0 (c (n "codemap-diagnostic") (v "0.1.0") (d (list (d (n "codemap") (r "^0.1.0") (d #t) (k 0)) (d (n "isatty") (r "^0.1.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "116damfzp5qyd1d9f80l7qkmap3ga1vhxl7rk5jclygq3j0s9rh5")))

(define-public crate-codemap-diagnostic-0.1.1 (c (n "codemap-diagnostic") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "codemap") (r "^0.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "0a2hpb57f97816fjz89qrsz1b5r4j2s2a1p9z58ffx17iszfd82b")))

(define-public crate-codemap-diagnostic-0.1.2 (c (n "codemap-diagnostic") (v "0.1.2") (d (list (d (n "codemap") (r "^0.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "08l1b84bn8r8a72rbvyi2v8a5i0j0kk0a5gr7fb6lmjvw05pf86c") (r "1.70")))

