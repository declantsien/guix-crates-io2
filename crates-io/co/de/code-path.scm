(define-module (crates-io co de code-path) #:use-module (crates-io))

(define-public crate-code-path-0.1.0 (c (n "code-path") (v "0.1.0") (d (list (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "109n1fi01ap7lc36v38ar0k1gvpl9cx692g8dkz3ccb2gpwz5x59")))

(define-public crate-code-path-0.2.0 (c (n "code-path") (v "0.2.0") (d (list (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "11xd8kyj8pf5vb124z2kzimpysygvhnvqkixbwz04hcs5ffb4zk6")))

(define-public crate-code-path-0.3.0 (c (n "code-path") (v "0.3.0") (d (list (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "0j72khzkzkcra9phf34m1gvmh3g4495licdyvj09wn94pp2xwxql")))

(define-public crate-code-path-0.3.1 (c (n "code-path") (v "0.3.1") (d (list (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "0qr4q2qv9cp1m4g4npmm50x1k2b569bjdx247f747bc8c2637a3g")))

