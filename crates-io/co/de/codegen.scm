(define-module (crates-io co de codegen) #:use-module (crates-io))

(define-public crate-codegen-0.0.0 (c (n "codegen") (v "0.0.0") (h "16cwm76l2isvp960k7jkdjcyszkymshkns918bhsl02s8g5dcjg2")))

(define-public crate-codegen-0.1.0 (c (n "codegen") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "0mr0crkh1nza60xg45wpz5d4s737y31k7i0x9yzni8q65y725x5k")))

(define-public crate-codegen-0.1.1 (c (n "codegen") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "0f327d21lbm8fl9r0jxbiqhrwg5pz48x8z1093hjx59527baq0mz")))

(define-public crate-codegen-0.1.2 (c (n "codegen") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "1pny9ylk17s9449bzq753zgf7f48vnx59ycx6gjssw2wgg681sir")))

(define-public crate-codegen-0.1.3 (c (n "codegen") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "02d6hmzna0xckycxdlzc2y408rqiry0b66nn7gn7g2cqkf59xi9l")))

(define-public crate-codegen-0.2.0 (c (n "codegen") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "07f45z842ippz5kblcb4p7iv27kgqr8f1jfwwxq3073pxl52hqgz")))

