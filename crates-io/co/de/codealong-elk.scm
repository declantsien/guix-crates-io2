(define-module (crates-io co de codealong-elk) #:use-module (crates-io))

(define-public crate-codealong-elk-0.1.0 (c (n "codealong-elk") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "codealong") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wz4xycmvn6k6v32syca0llh1frdc5l5n61zykh9yg9d9nk7h2ax")))

(define-public crate-codealong-elk-0.1.1 (c (n "codealong-elk") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "codealong") (r "^0.1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hostname") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "149pvikqyqhhn1n19ixy1faaam1z7n0b92drwb9vqghynry217sq")))

