(define-module (crates-io co de codename) #:use-module (crates-io))

(define-public crate-codename-0.1.0 (c (n "codename") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0s2bxxc1y55cpdnzay6h5ja1j5xg889nbh49yz3i3gp6128hqklx")))

(define-public crate-codename-0.1.1 (c (n "codename") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1y5hmcd8gd7yqdzyxp2cg5x07hmx0cgnjj516pp5x5rkcdllafxr")))

