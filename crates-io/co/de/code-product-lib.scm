(define-module (crates-io co de code-product-lib) #:use-module (crates-io))

(define-public crate-code-product-lib-0.0.1 (c (n "code-product-lib") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 2)))) (h "0h9zp0m3nkp40fwy9v9wrc500glm53jldajdqsxh8nq1phsiyrz9") (f (quote (("default") ("debug"))))))

(define-public crate-code-product-lib-0.2.0 (c (n "code-product-lib") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 2)))) (h "1d768lxwrng05vyww82vb0fm06y7w773pchxs9wmndl9zn4agq11") (f (quote (("default") ("debug"))))))

(define-public crate-code-product-lib-0.3.0 (c (n "code-product-lib") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 2)))) (h "0kmg18y4b3rqii8h20bvy5jknfxx74yq825019lpm3hrf49nhf2r") (f (quote (("default") ("debug"))))))

(define-public crate-code-product-lib-0.4.0 (c (n "code-product-lib") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 2)))) (h "0glb4ax9wa92m0m7w20cinkcvxfpnr1i0gkng9flzf6z9fq9biw7") (f (quote (("default") ("debug"))))))

