(define-module (crates-io co de codelist) #:use-module (crates-io))

(define-public crate-codelist-1.0.0 (c (n "codelist") (v "1.0.0") (h "0kpwbz9rsy44blnnkrksryar7igivajgh5rwgv0mwnrbz0cmk3qq")))

(define-public crate-codelist-1.0.1 (c (n "codelist") (v "1.0.1") (h "09lkjqjkscvc9jmwnk9w6c7p4rdkd180hp8dnhhwxdq59b3i06fl")))

(define-public crate-codelist-1.0.2 (c (n "codelist") (v "1.0.2") (h "1lpvvf63k3ga7j570kjyghcjl47hxz2nj9rggr2arrv3wsrxy535")))

(define-public crate-codelist-1.1.0 (c (n "codelist") (v "1.1.0") (h "1yqj1cpgfjgfh8ccz5cdil0p3fvlinlmdsgmc5615q8f5025k11s")))

(define-public crate-codelist-1.1.1 (c (n "codelist") (v "1.1.1") (h "0qncxczz77sbnbrzrs25wa2miacm7c21dfc656z4na1p7vxnfaw9")))

(define-public crate-codelist-1.1.2 (c (n "codelist") (v "1.1.2") (h "1vf1rlqgwyyxa4qc41k7bwjp07nnslrx5y2845kd5dgab611jpq6")))

