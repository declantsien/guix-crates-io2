(define-module (crates-io co de codecs) #:use-module (crates-io))

(define-public crate-codecs-0.0.1 (c (n "codecs") (v "0.0.1") (d (list (d (n "core2") (r "^0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1gw6fpk9pmfhxasxfa54z5cwk5g0zpcxa7fnvwn059j61hfq4zby") (f (quote (("std" "core2/std") ("default" "std"))))))

