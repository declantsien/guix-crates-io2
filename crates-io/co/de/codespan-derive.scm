(define-module (crates-io co de codespan-derive) #:use-module (crates-io))

(define-public crate-codespan-derive-0.1.0 (c (n "codespan-derive") (v "0.1.0") (d (list (d (n "codespan-derive-proc") (r "=0.1.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)))) (h "0ni6x0f4w902zlzaf8piwdsm9rg5329yl342lnjg6yb0g40ls9qp")))

(define-public crate-codespan-derive-0.1.1 (c (n "codespan-derive") (v "0.1.1") (d (list (d (n "codespan-derive-proc") (r "=0.1.1") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)))) (h "0lndqsh4sf9m827n4c41a5r4miwg6jf5qw1v8bld815y1xml4xiy")))

