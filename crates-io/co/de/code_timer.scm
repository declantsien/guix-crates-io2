(define-module (crates-io co de code_timer) #:use-module (crates-io))

(define-public crate-code_timer-0.1.0 (c (n "code_timer") (v "0.1.0") (h "01pvdf7524hb047p9br2y7ilx5sp2ihx40h187k99qhjxp1qyp4h")))

(define-public crate-code_timer-0.1.1 (c (n "code_timer") (v "0.1.1") (h "06kc1f8ja5inl87hckn4ggh5gf89z2h9k23ifaqy8hqhw251bg3p")))

(define-public crate-code_timer-0.1.2 (c (n "code_timer") (v "0.1.2") (h "0y4xv8zbmgb86zycncch3r3lqdpf5mqwxi5qqcar7s9d5imxvxp1")))

