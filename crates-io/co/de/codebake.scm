(define-module (crates-io co de codebake) #:use-module (crates-io))

(define-public crate-codebake-0.1.0 (c (n "codebake") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0kh3ywx37kma7id3klg8fc88prw8mrp5cwij8188vln71k04dh8x")))

(define-public crate-codebake-0.1.1 (c (n "codebake") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1g6vn7r6y3sj7dy282769zqhxwnwbpn6nr5hyzgn3ygsw4q4npbl")))

