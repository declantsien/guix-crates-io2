(define-module (crates-io co de code-tour) #:use-module (crates-io))

(define-public crate-code-tour-0.1.0 (c (n "code-tour") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qlndis27x6lbq0mlzsahxwbpkbxvp9fr2q70fgd96azpj152wqq") (f (quote (("default" "colours") ("colours"))))))

(define-public crate-code-tour-0.2.0 (c (n "code-tour") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d919kzcfzis76i552lhs6r9kx1z310mhnhdna4qdagqilh9whl8") (f (quote (("interactive") ("default" "colours") ("colours" "ansi_term"))))))

