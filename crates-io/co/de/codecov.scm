(define-module (crates-io co de codecov) #:use-module (crates-io))

(define-public crate-codecov-0.2.0 (c (n "codecov") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "02q8jgn0bbmidnz9d5vn34rf5bzb0d9ga3yr5m5facjjyfshya7g")))

(define-public crate-codecov-0.3.0 (c (n "codecov") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0ivvn1qjiymwxkv46w21yfng6kln8kbncfrwz764lc1vzbqnwiw8")))

(define-public crate-codecov-0.3.1 (c (n "codecov") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0kswgp0pklvv8rg5r8c9rm1q4vncvpn9ynvhzi5dzl07v8kqaf1l")))

(define-public crate-codecov-0.3.2 (c (n "codecov") (v "0.3.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "0mxv8biyy7d4giks5xkh0lwygp52yxpamdmmgxdxad3vz9c482kh")))

(define-public crate-codecov-0.3.3 (c (n "codecov") (v "0.3.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1jmz6hxazvnyynj7w32s39x0bar2y5ai2lfpmp2dpi4jravlqxmr")))

(define-public crate-codecov-0.3.4 (c (n "codecov") (v "0.3.4") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1lnigr0rpqqsb1gai3lis356bqnp1glhhxqar6ad2d479pjn7gyr")))

(define-public crate-codecov-0.3.5 (c (n "codecov") (v "0.3.5") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "170gzwdw87rc6dd21s0rqar8vg6iv5d8kgqzxsm2c7yqi40jcwnf")))

(define-public crate-codecov-0.4.0 (c (n "codecov") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1jq9ylsi2jbr6m2bhg5c4bab2zvna31484clz737c6ipvfnfgv7l")))

