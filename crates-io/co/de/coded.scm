(define-module (crates-io co de coded) #:use-module (crates-io))

(define-public crate-coded-0.1.0 (c (n "coded") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.63") (o #t) (d #t) (k 0)))) (h "10y47rsxfw1sm04vmchhz50v2xg58ax5vg3nwrqz8wjhmwazr72z") (f (quote (("unstable_std_backtrace") ("default"))))))

