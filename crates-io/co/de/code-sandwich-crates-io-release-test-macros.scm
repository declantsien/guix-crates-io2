(define-module (crates-io co de code-sandwich-crates-io-release-test-macros) #:use-module (crates-io))

(define-public crate-code-sandwich-crates-io-release-test-macros-0.1.0 (c (n "code-sandwich-crates-io-release-test-macros") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1f8jkhnh64nkw9kizg1rb8yd5qa5is0cwcb6ggh0zmcmbjvi7zr9")))

(define-public crate-code-sandwich-crates-io-release-test-macros-0.1.1 (c (n "code-sandwich-crates-io-release-test-macros") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("full"))) (d #t) (k 0)))) (h "00y396xmbmzr4yf4rjm9wd6r8lsrzfj36vm0yz431k8a0axjqzya")))

