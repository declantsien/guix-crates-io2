(define-module (crates-io co de codectrl-bindings-web) #:use-module (crates-io))

(define-public crate-codectrl-bindings-web-0.0.0 (c (n "codectrl-bindings-web") (v "0.0.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-build") (r "^0.10") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7") (f (quote ("codegen" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.7") (f (quote ("prost"))) (k 1)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "fast-rng" "macro-diagnostics" "js"))) (d #t) (k 0)))) (h "116n53m32wnnlqwvvqi4p37a60x140kpg8f4aarbs6pj284sryq1")))

