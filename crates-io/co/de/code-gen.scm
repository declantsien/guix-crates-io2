(define-module (crates-io co de code-gen) #:use-module (crates-io))

(define-public crate-code-gen-0.1.0 (c (n "code-gen") (v "0.1.0") (h "13b4qsy9rc570c9qlcx44hhx7d12hcgyspyicyrd1v4j2pvic4s9")))

(define-public crate-code-gen-0.2.0 (c (n "code-gen") (v "0.2.0") (h "1kp78d4gc3cwr4hqnxbwdrk2n6vi3x1jj24iqffmh4cpswdc7zpm")))

(define-public crate-code-gen-0.0.0 (c (n "code-gen") (v "0.0.0") (h "0w261zbyw2rhfkm3had2gc60a7la9r0mrivyarjpwn59gbpka6mr") (f (quote (("java"))))))

(define-public crate-code-gen-0.2.1 (c (n "code-gen") (v "0.2.1") (h "0h12d77v3lmfyhl7mlf8vq27svfdh6wyax1ppw5pghrg8bq6i5l7") (f (quote (("java"))))))

(define-public crate-code-gen-0.2.2 (c (n "code-gen") (v "0.2.2") (h "13irc897sja77kiqzija76k8rs26fd7yk2gcs382jbfi5x36jmmf") (f (quote (("java"))))))

(define-public crate-code-gen-0.2.3 (c (n "code-gen") (v "0.2.3") (h "13in1j0z0dpr60mrwqlk23h2pgkdz75836mifpgqa602772gcvrk") (f (quote (("java"))))))

(define-public crate-code-gen-0.2.4 (c (n "code-gen") (v "0.2.4") (h "0ynmniz1ca7aqm3314s3dlx1yw33ys1p874pwnx2f2b6z4kjzays") (f (quote (("java"))))))

(define-public crate-code-gen-0.3.0 (c (n "code-gen") (v "0.3.0") (h "0nvk7imaw9m1fq5naqda5a29naks2anp5838ys3vzz00rca14a8l")))

(define-public crate-code-gen-0.3.1 (c (n "code-gen") (v "0.3.1") (h "1ccac4ab1bq9f8914479r14wm4c20mdn76cym3hsf2wzz9i574in")))

(define-public crate-code-gen-0.4.0 (c (n "code-gen") (v "0.4.0") (h "0byw1vgyhvgw5y619l1di4j6b8kbm2b02qk5ds9g9i2k0s3n09bk") (f (quote (("rust"))))))

