(define-module (crates-io co de codeframe) #:use-module (crates-io))

(define-public crate-codeframe-0.1.0 (c (n "codeframe") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "k9") (r "^0.2.1") (d #t) (k 0)))) (h "164q8q4938gafc8011m53i3w5gd5xfv52jqrm0l54sihz02ynb7l")))

(define-public crate-codeframe-0.1.1 (c (n "codeframe") (v "0.1.1") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "k9") (r "^0.2.1") (d #t) (k 0)))) (h "0wwqnzyv04agjwf3sqzz9c213pgy72h37y5l4w14djyjksx2rdzh")))

(define-public crate-codeframe-0.1.2 (c (n "codeframe") (v "0.1.2") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "k9") (r "^0.2.1") (d #t) (k 0)))) (h "08w4vk5v4nwlppkaix5nwnb3b2270iw2w9sj9xj7l8d09s05dl93")))

(define-public crate-codeframe-0.1.3 (c (n "codeframe") (v "0.1.3") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "k9") (r "^0.2.1") (d #t) (k 2)))) (h "1qlm4nkfddsvj6xbviz56xbjam2lbmf5m1q25gwfjzgrixgc9k59")))

(define-public crate-codeframe-0.1.4 (c (n "codeframe") (v "0.1.4") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "k9") (r "^0.2.1") (d #t) (k 2)))) (h "16yszkiw0x6hc18d07x7s9440r6x1m70xvq4g82v2pzwlqywvh66")))

(define-public crate-codeframe-0.1.5 (c (n "codeframe") (v "0.1.5") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "k9") (r "^0.2.1") (d #t) (k 2)))) (h "0dnhgwj40qrvwxmcssphmz9j7rqgbgj85zngywf6ry5nzrjvjs0h")))

(define-public crate-codeframe-0.1.6 (c (n "codeframe") (v "0.1.6") (d (list (d (n "bytecount") (r "^0.6.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "k9") (r "^0.2.1") (d #t) (k 2)))) (h "0nzy809hw4wssypbvah7ldwk7f2fc57myy8sf5ln4jvq321vav90")))

