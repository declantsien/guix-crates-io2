(define-module (crates-io co de code-fuzzy-match) #:use-module (crates-io))

(define-public crate-code-fuzzy-match-0.1.0 (c (n "code-fuzzy-match") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1qlh61lx8kyaakmljk7g201w9d8pn47bpg51q24vn5paq1yd77lv")))

(define-public crate-code-fuzzy-match-0.1.1 (c (n "code-fuzzy-match") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0ywmsccq4divl2ppr0kw2klns160v8hgslyw6hkh40p26q2fs0hr")))

(define-public crate-code-fuzzy-match-0.2.0 (c (n "code-fuzzy-match") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0bf4c5zdg0lsnjlwx2qaa0dwhpnxhj28q2kd8v4qpgjp432pa68z")))

(define-public crate-code-fuzzy-match-0.2.1 (c (n "code-fuzzy-match") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0fjqi60849l7wmm65q56r4d3b3cwgnkq8ccn0cjfchcm1pdkh100")))

(define-public crate-code-fuzzy-match-0.2.2 (c (n "code-fuzzy-match") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ryk5yimcc92phzjf2yjbyb607a6a6p4c5xpw64idwm7cmzpji5c")))

