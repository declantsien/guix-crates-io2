(define-module (crates-io co de code-generation-example) #:use-module (crates-io))

(define-public crate-code-generation-example-0.1.0 (c (n "code-generation-example") (v "0.1.0") (h "185a07fhndhdhq0j7sx22lh9x359czzczj354zqhib1bgk3a5gmg")))

(define-public crate-code-generation-example-0.2.0 (c (n "code-generation-example") (v "0.2.0") (h "0l9b2xka9v3p0kxajidwjnhhzlk5bfm98c8n9bq4dplj0q0rn37k")))

