(define-module (crates-io co de codesim) #:use-module (crates-io))

(define-public crate-codesim-0.1.0 (c (n "codesim") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0zljdcby5pclhpcjhdl1zqmy90c5fcswz59hwiifmhbh63nkdzp7")))

(define-public crate-codesim-0.1.2 (c (n "codesim") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0r20awhh6hiwpjjqml8n47b2cssipjis80lzdxwkrm0jifzdfklh")))

