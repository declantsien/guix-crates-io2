(define-module (crates-io co de codespawn) #:use-module (crates-io))

(define-public crate-codespawn-0.0.1 (c (n "codespawn") (v "0.0.1") (h "0mq5fmdpg3zb6i1hq8if29y8yx3w6657fb0xsjqjjnllp3nlj213")))

(define-public crate-codespawn-0.1.0 (c (n "codespawn") (v "0.1.0") (d (list (d (n "json") (r "^0.8") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0pvddfxwwhw8ill9d1p2yh8la8aywg7m540c8jb05rf3z45k0kia")))

(define-public crate-codespawn-0.1.1 (c (n "codespawn") (v "0.1.1") (d (list (d (n "json") (r "^0.8") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1s2fahdpy2dxarmnh6xn6g6fjlv0vj8hhhv9zyjb48knj0y0hdrj")))

(define-public crate-codespawn-0.1.2 (c (n "codespawn") (v "0.1.2") (d (list (d (n "json") (r "^0.8") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1ldj75ddq0pqqxdxhwjf4grwjinva55sfnzydm157l1vk41qxhvx")))

(define-public crate-codespawn-0.1.3 (c (n "codespawn") (v "0.1.3") (d (list (d (n "json") (r "^0.8") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "14rph2ikwwxc8wbpl7zss7mwbap97p1bgs6kdkkkdpric5icbmkh")))

(define-public crate-codespawn-0.1.4 (c (n "codespawn") (v "0.1.4") (d (list (d (n "json") (r "^0.9") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1jrpanv56x6yj09ijrr1g47pq9pyljjbk5vcg25cllz4wfn5q3k0")))

(define-public crate-codespawn-0.1.5 (c (n "codespawn") (v "0.1.5") (d (list (d (n "json") (r "^0.9") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "12dwy90s4yqz7p34vpgxpbm03abnl64lw7y4r4xv3bz650jr9a55")))

(define-public crate-codespawn-0.2.0 (c (n "codespawn") (v "0.2.0") (d (list (d (n "json") (r "^0.9") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0kpg2yav9g12s9ix0lhk8mizw65zxywivqvddrxnj1qbl60wrw8j")))

(define-public crate-codespawn-0.2.1 (c (n "codespawn") (v "0.2.1") (d (list (d (n "json") (r "^0.9") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0pd8f46nh84mbjxrkgg701pryz0yjpiccq7lprqiqvfjzpzlv6qb")))

(define-public crate-codespawn-0.3.0 (c (n "codespawn") (v "0.3.0") (d (list (d (n "json") (r "^0.10") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "10njbghdajk3qav6qlgg175xvhnhmaxh541bskzqims19nydf3xi")))

(define-public crate-codespawn-0.3.1 (c (n "codespawn") (v "0.3.1") (d (list (d (n "json") (r "^0.10") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "149yw6b91l92avk740bmh3ymd2riyrqv5rmf1bk7qv98rmqmjfr9")))

(define-public crate-codespawn-0.3.2 (c (n "codespawn") (v "0.3.2") (d (list (d (n "json") (r "^0.10") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "00cbhgivd6ldd7v2hd1dv3zasw40w1lxlky58gv4qq5pqp2j0dp6")))

(define-public crate-codespawn-0.3.3 (c (n "codespawn") (v "0.3.3") (d (list (d (n "json") (r "^0.10") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "05xb5jjac5z96yagvhszl05pc4y02arnkz3cc8pc864w5kccbqkh")))

