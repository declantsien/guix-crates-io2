(define-module (crates-io co de codesign) #:use-module (crates-io))

(define-public crate-codesign-0.1.0 (c (n "codesign") (v "0.1.0") (d (list (d (n "bitness") (r "~0.3.0") (d #t) (k 0)) (d (n "chrono") (r "~0.4.0") (d #t) (k 0)) (d (n "clap") (r "~2.29.1") (d #t) (k 0)) (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "fern") (r "~0.5.2") (d #t) (k 0)) (d (n "log") (r "~0.4.1") (d #t) (k 0)) (d (n "winreg") (r "~0.5.0") (d #t) (k 0)))) (h "0z2rndfybin9f5qj38nl6jxg9671n0p1n8nqw6sp6qq2khl86la3")))

(define-public crate-codesign-0.1.1 (c (n "codesign") (v "0.1.1") (d (list (d (n "bitness") (r "~0.3.0") (d #t) (k 0)) (d (n "chrono") (r "~0.4.0") (d #t) (k 0)) (d (n "clap") (r "~2.29.1") (d #t) (k 0)) (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "fern") (r "~0.5.2") (d #t) (k 0)) (d (n "log") (r "~0.4.1") (d #t) (k 0)) (d (n "winreg") (r "~0.5.0") (d #t) (k 0)))) (h "0n4yxkf1300y9mwv8cag7ngfyfi7dvag0m4x6n84cjhnr75ih207")))

(define-public crate-codesign-0.2.0 (c (n "codesign") (v "0.2.0") (d (list (d (n "bitness") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "winreg") (r "^0.7.0") (d #t) (k 0)))) (h "1j1qd7lwsc421qg6j2n50b2qlry0sax7l3zr68cji0k9jwdf0n8m")))

(define-public crate-codesign-0.2.1 (c (n "codesign") (v "0.2.1") (d (list (d (n "bitness") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "winreg") (r "^0.7.0") (d #t) (k 0)))) (h "19ljllj4vxrdimj6292scxxsl0rpmnhfl5w2gb02v5dp13h8w080")))

