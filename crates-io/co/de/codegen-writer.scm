(define-module (crates-io co de codegen-writer) #:use-module (crates-io))

(define-public crate-codegen-writer-0.1.0 (c (n "codegen-writer") (v "0.1.0") (h "0ii5x8j9lfas13k6yb79nf47dn82qj4wbx7vrkcnw0ljix5smrpp")))

(define-public crate-codegen-writer-0.1.3-dev (c (n "codegen-writer") (v "0.1.3-dev") (h "1zmxq3hk4afk15i4h5lvbrzxgjxq4ksf2a6009y8s79wgz5dna7h") (y #t)))

(define-public crate-codegen-writer-0.2.0 (c (n "codegen-writer") (v "0.2.0") (h "15x5lc5b0v0v5crlx7v4dmv39bbhwwbj34xqplf7j6gy7bl1yraq")))

