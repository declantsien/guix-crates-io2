(define-module (crates-io co de codejam) #:use-module (crates-io))

(define-public crate-codejam-0.1.0 (c (n "codejam") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1f23cx3jl13ykfn2br79f7ap8ly71kpw4q41gg2i2airdv1ibca3")))

