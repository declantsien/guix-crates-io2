(define-module (crates-io co w_ cow_struct) #:use-module (crates-io))

(define-public crate-cow_struct-0.3.1 (c (n "cow_struct") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16q2s9d26drif36qc6331pyjbsp339csbaqs0ls3j2ayyk1zj7d0") (y #t)))

(define-public crate-cow_struct-0.0.1 (c (n "cow_struct") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dd6nmab7w40kghalv883kwvs1z75jn7fd6y2rs73s1l4f2n60vq")))

