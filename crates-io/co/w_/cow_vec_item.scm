(define-module (crates-io co w_ cow_vec_item) #:use-module (crates-io))

(define-public crate-cow_vec_item-0.1.0 (c (n "cow_vec_item") (v "0.1.0") (h "0gjvj1hclxilabpmrmv625y8q5dv4sd47ayqcyp7cg9hpwmrm5vh")))

(define-public crate-cow_vec_item-0.2.0 (c (n "cow_vec_item") (v "0.2.0") (h "0kdl1143xhxmfdl2r50fzbpq97aaca6chrhlfb3crznk0zp7fzfm")))

(define-public crate-cow_vec_item-0.3.0 (c (n "cow_vec_item") (v "0.3.0") (h "0z0rbj6m4a0q27a6a17nppbbkh8dj4lcndvkz9g1i224zijmibfs")))

(define-public crate-cow_vec_item-0.4.0 (c (n "cow_vec_item") (v "0.4.0") (h "0x79sg2a17g8f4jlm12jpprcn45myvvb7a04j5xmqahp00i4wkzm")))

(define-public crate-cow_vec_item-0.5.0 (c (n "cow_vec_item") (v "0.5.0") (h "0fac5pl4d35plkl5m4cyxkkp91gb2plx94ylmagl181ak9n0z5jn")))

(define-public crate-cow_vec_item-0.5.1 (c (n "cow_vec_item") (v "0.5.1") (h "1j7b0d90barqcvsmq24nc5iba4rwqcafljbr4ncfdsbz9lzbqsix")))

