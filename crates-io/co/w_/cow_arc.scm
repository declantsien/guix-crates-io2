(define-module (crates-io co w_ cow_arc) #:use-module (crates-io))

(define-public crate-cow_arc-0.2.0 (c (n "cow_arc") (v "0.2.0") (h "1y871rb333p3wvvgam8049jgslx2h90n0fg5fnrjbg6jlla794k4")))

(define-public crate-cow_arc-0.2.1 (c (n "cow_arc") (v "0.2.1") (h "1aqakh5ma7kza9gr1775ag259cdqyrkbd6yc54i778drjm9080wa")))

(define-public crate-cow_arc-0.2.2 (c (n "cow_arc") (v "0.2.2") (h "0wh8cjk047xjakdqszc9zg3f6cv8il0xijfczg72bab5qgvskqb8")))

