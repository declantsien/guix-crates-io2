(define-module (crates-io co lc colcon) #:use-module (crates-io))

(define-public crate-colcon-0.1.0 (c (n "colcon") (v "0.1.0") (h "1akhiz3p6gimkzh1bbxb9v6vlk33kl94y1jb4cj9gj3ya8i2aq9i") (f (quote (("D50"))))))

(define-public crate-colcon-0.1.1 (c (n "colcon") (v "0.1.1") (h "0crfar14m3lhh8rvjg9x5hhyr8r025f6x72f2afa079l5hcsl7by") (f (quote (("D50"))))))

(define-public crate-colcon-0.2.0 (c (n "colcon") (v "0.2.0") (h "1pxjz70z89hfyi8mcx20r2nh9dl9la6lnlaqixkdqnfn2vfiljmh") (f (quote (("D50"))))))

(define-public crate-colcon-0.2.1 (c (n "colcon") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0rwf7h6f8qfvw72x7qbw9plfn1cg3pxri1jf800ddwvl8a0h4j0a") (f (quote (("D50"))))))

(define-public crate-colcon-0.2.2 (c (n "colcon") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1b2hrimqbd3x96mdrhbihzn6mfgl608w755s26ypmvzkw5d92bdc") (f (quote (("D50"))))))

(define-public crate-colcon-0.3.0 (c (n "colcon") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0bym5mi59rp1y0f7nw3712fmixajsnlmiw2izks44gkpnf81rrs6") (f (quote (("D50"))))))

(define-public crate-colcon-0.4.0 (c (n "colcon") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0qaqjiif7y3fdwvq5gpjjcgf1p1gy7z43y4zlfak4p8ak0qblq8s")))

(define-public crate-colcon-0.4.1 (c (n "colcon") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1d6w04gfyy620gp5z4c3fh4b3qjhbj9chjzv2dxgca6qnnh1lhpa")))

(define-public crate-colcon-0.5.0 (c (n "colcon") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "17n0779llkh2mpd3jizjwcywyrgq6hwklw7sa385qkln9wi83ayf")))

(define-public crate-colcon-0.5.1 (c (n "colcon") (v "0.5.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1a4wx0iizv4s8fmfsvifi17cg7gzhlrdah7xqxhwfm5j38rcmlys")))

(define-public crate-colcon-0.6.0 (c (n "colcon") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "06b7p05qjvlgscrdynr1iz3sc8mm4vr07vgng4qnlnrfhrh2yq9r")))

(define-public crate-colcon-0.7.0 (c (n "colcon") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "10sxaa77lycxy1jk8bwfwqpjlr533k0vw4hi37l1z5mg6rixgyx2")))

(define-public crate-colcon-0.7.1 (c (n "colcon") (v "0.7.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "092n2ik3cfhc4zgzqg0ig94mn5yfb2yc3wwjn8bbjzpf654zbg1v")))

(define-public crate-colcon-0.8.0 (c (n "colcon") (v "0.8.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1h7vgvk6ihc1dwam36mz1yibag7i4czbm9byy2wnjqv6zk3n3m53")))

(define-public crate-colcon-0.8.1 (c (n "colcon") (v "0.8.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "09i0bpzp6crh6wygf4s4y7ff7cxj15zh52cj7skb769vyg2r8hlx")))

