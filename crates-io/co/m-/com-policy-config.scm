(define-module (crates-io co m- com-policy-config) #:use-module (crates-io))

(define-public crate-com-policy-config-0.1.0 (c (n "com-policy-config") (v "0.1.0") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Devices_Custom"))) (d #t) (k 0)) (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Devices_FunctionDiscovery"))) (d #t) (k 2)))) (h "0a2q25yjrh92kq8ps7z2x5sh9yd27kig1vxvrv8dy27w7b31mqfd")))

(define-public crate-com-policy-config-0.2.0 (c (n "com-policy-config") (v "0.2.0") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Devices_Custom"))) (d #t) (k 0)) (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Devices_FunctionDiscovery"))) (d #t) (k 2)))) (h "19lc0bzv0myi9q362s5gl958gsfvcha18k508jp7xg3migfmsrh4")))

(define-public crate-com-policy-config-0.3.0 (c (n "com-policy-config") (v "0.3.0") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Devices_Custom"))) (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Devices_FunctionDiscovery"))) (d #t) (k 2)))) (h "0npfb88sbb619pifsj1m5089dm4fryv2yka3cnvw27chwzgmgkz7")))

(define-public crate-com-policy-config-0.4.0 (c (n "com-policy-config") (v "0.4.0") (d (list (d (n "windows") (r "^0.51") (f (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_System_Variant" "Devices_Custom"))) (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Devices_FunctionDiscovery"))) (d #t) (k 2)))) (h "0579zv0q4b48csp2bchdcgiw6xqaqsilw3ybm0ahfxhvkn8835aj")))

(define-public crate-com-policy-config-0.5.0 (c (n "com-policy-config") (v "0.5.0") (d (list (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_System_Variant" "Devices_Custom"))) (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Devices_FunctionDiscovery"))) (d #t) (k 2)))) (h "1hhhc38pyailf4v4ni7m5k1rp4ardvjmfb3sglcj38vyddpqxpkd")))

