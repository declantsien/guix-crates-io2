(define-module (crates-io co m- com-scrape-types) #:use-module (crates-io))

(define-public crate-com-scrape-types-0.1.0 (c (n "com-scrape-types") (v "0.1.0") (h "00xlpxn75f2jzf3s8c3b3h4l944m4vy5d4cq6slfvi2fi9cc3ix5")))

(define-public crate-com-scrape-types-0.1.1 (c (n "com-scrape-types") (v "0.1.1") (h "1l1slxb9mypf59wmqmyzczcgnmbjf414174i41m77z5qfnwq7qaw")))

