(define-module (crates-io co m- com-croftsoft-lib-role) #:use-module (crates-io))

(define-public crate-com-croftsoft-lib-role-0.1.0 (c (n "com-croftsoft-lib-role") (v "0.1.0") (h "13grj9s91cazjybw2rppxb0bk4pf7la2llwdqv6xs4gdzp1gh30c") (y #t)))

(define-public crate-com-croftsoft-lib-role-0.1.1 (c (n "com-croftsoft-lib-role") (v "0.1.1") (h "07x22gizjgg87npfjky0p6f4k92d48bzazd45kg0l0nxf5qz4cwh") (y #t)))

(define-public crate-com-croftsoft-lib-role-0.1.2 (c (n "com-croftsoft-lib-role") (v "0.1.2") (h "1darnwn54ikw2sclc9m7c1arnw4d9apjr1wryhzyid775qbvad4z") (y #t)))

(define-public crate-com-croftsoft-lib-role-0.1.3 (c (n "com-croftsoft-lib-role") (v "0.1.3") (h "099bx38k9kfg3xhsjhc417a7vz7yli06wlv4m691ir0gqxv8fl4b")))

(define-public crate-com-croftsoft-lib-role-0.1.4 (c (n "com-croftsoft-lib-role") (v "0.1.4") (h "19fmadyzrn1qxz5hr64mhgkww46srv1h1xpd69z44w6343732hwq")))

(define-public crate-com-croftsoft-lib-role-0.2.0 (c (n "com-croftsoft-lib-role") (v "0.2.0") (h "1q35kmr2qndyllhzkh5ld9j9f1krk9nhf8m0rnnxlczgwq20sc7p")))

(define-public crate-com-croftsoft-lib-role-0.3.0 (c (n "com-croftsoft-lib-role") (v "0.3.0") (h "0pxjy22a9pw0acmh7zgwd88m5kirp179zqy2i16bbnk5qp1565cn")))

(define-public crate-com-croftsoft-lib-role-0.4.0 (c (n "com-croftsoft-lib-role") (v "0.4.0") (h "09w78frv67c34zmhfw01drvadf58fffsrvmyapdm5ill69n0w6v4")))

(define-public crate-com-croftsoft-lib-role-0.4.1 (c (n "com-croftsoft-lib-role") (v "0.4.1") (h "1hcy727d0m0zafahrz9061m7mfmd7v1pia8shp6825vxcg8ln3yw")))

(define-public crate-com-croftsoft-lib-role-0.5.0 (c (n "com-croftsoft-lib-role") (v "0.5.0") (h "1nppmz0nii2rrffg21vxcis30hpw2wgjqm676ibrc9w7ymxnjr1b")))

(define-public crate-com-croftsoft-lib-role-0.5.1 (c (n "com-croftsoft-lib-role") (v "0.5.1") (h "0q2hfx16wha628696avda0r777irmx4r93dv9n4yncldi5mf3kzh")))

