(define-module (crates-io co m- com-wrapper) #:use-module (crates-io))

(define-public crate-com-wrapper-0.1.0-alpha1 (c (n "com-wrapper") (v "0.1.0-alpha1") (d (list (d (n "derive-com-wrapper") (r "^0.1.0-alpha1") (d #t) (k 2)) (d (n "winapi") (r "^0.3.6") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("unknwnbase"))) (d #t) (k 2)) (d (n "wio") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0a499vj7bvsg916xk9ayp9vjafcwh579hs2bcz8szh6sn523wan2")))

(define-public crate-com-wrapper-0.1.0-alpha2 (c (n "com-wrapper") (v "0.1.0-alpha2") (d (list (d (n "derive-com-wrapper") (r "^0.1.0-alpha4") (d #t) (k 2)) (d (n "winapi") (r "^0.3.6") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("unknwnbase"))) (d #t) (k 2)) (d (n "wio") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0012fqamkg7nl79bvr6jz6yiz64dk5viz1srwpj8bz6ld64wxc37")))

(define-public crate-com-wrapper-0.1.0 (c (n "com-wrapper") (v "0.1.0") (d (list (d (n "derive-com-wrapper") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("unknwnbase"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0w5vpdsjlkibf3xxwq382rsym9wbg0had3qky3hrvw90ac90jfvr")))

