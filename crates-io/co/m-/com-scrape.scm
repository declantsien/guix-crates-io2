(define-module (crates-io co m- com-scrape) #:use-module (crates-io))

(define-public crate-com-scrape-0.1.0 (c (n "com-scrape") (v "0.1.0") (d (list (d (n "clang-sys") (r "^1") (f (quote ("clang_6_0" "runtime"))) (d #t) (k 0)))) (h "1w4bbglm1v1rnakb9f25yiplg810ayw5aga134cghzv13h9i0f7v")))

(define-public crate-com-scrape-0.1.1 (c (n "com-scrape") (v "0.1.1") (d (list (d (n "clang-sys") (r "^1") (f (quote ("clang_6_0" "runtime"))) (d #t) (k 0)))) (h "1paf1nw4hwl1g59if95vyrajk5pi8z3nbs133cdmb1ix27c20mw0")))

(define-public crate-com-scrape-0.1.2 (c (n "com-scrape") (v "0.1.2") (d (list (d (n "clang-sys") (r "^1") (f (quote ("clang_6_0" "runtime"))) (d #t) (k 0)))) (h "1c6xm25j4qy1glzr24wzfwc7wss2v668vsh89q9bdgd5ismhi4dp")))

(define-public crate-com-scrape-0.1.3 (c (n "com-scrape") (v "0.1.3") (d (list (d (n "clang-sys") (r "^1") (f (quote ("clang_6_0" "runtime"))) (d #t) (k 0)))) (h "0d8rfxv97g5ddxzzn8l90hvjv274r1abslzgmss7fvgvrjvqzslp")))

