(define-module (crates-io co m- com-impl) #:use-module (crates-io))

(define-public crate-com-impl-0.1.0-alpha1 (c (n "com-impl") (v "0.1.0-alpha1") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("unknwnbase"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1v82cf7gy51pz91rl65y0qzlmp4x78vbr0zs6g072bliyfbc4igg")))

(define-public crate-com-impl-0.1.0-alpha2 (c (n "com-impl") (v "0.1.0-alpha2") (d (list (d (n "winapi") (r "^0.3.6") (f (quote ("unknwnbase"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1wvkbxz921qlrn0v7x2dw9w2b0lr27nn61zdwasxj5gljbinkxlc")))

(define-public crate-com-impl-0.1.0 (c (n "com-impl") (v "0.1.0") (d (list (d (n "derive-com-impl") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("unknwnbase"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("dwrite" "winerror"))) (d #t) (k 2)) (d (n "wio") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)))) (h "19jrlav0jjjaz2ia5lg5qbvqgsn5kgq6ls9bcy1axbllpwgwym02")))

(define-public crate-com-impl-0.1.1 (c (n "com-impl") (v "0.1.1") (d (list (d (n "derive-com-impl") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("unknwnbase"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("dwrite" "winerror"))) (d #t) (k 2)) (d (n "wio") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1gixqvnpm85frzii6j4xvrmy1v1y1bx7vgj1xq8h9vc0d73w5siq")))

(define-public crate-com-impl-0.2.0 (c (n "com-impl") (v "0.2.0") (d (list (d (n "derive-com-impl") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("unknwnbase"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("dwrite" "winerror"))) (d #t) (k 2)) (d (n "wio") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1gw1ysv4f32b5nvyf57v4gm0fk7bsq5yhkl6nmpk9wsmfg87fw8h")))

