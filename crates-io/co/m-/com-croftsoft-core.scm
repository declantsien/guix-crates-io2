(define-module (crates-io co m- com-croftsoft-core) #:use-module (crates-io))

(define-public crate-com-croftsoft-core-0.1.0 (c (n "com-croftsoft-core") (v "0.1.0") (h "00p4iik0hvglh0v0ay4a1lfd1l7r22l7vwl0j54cpq9qacpqd849")))

(define-public crate-com-croftsoft-core-0.2.0 (c (n "com-croftsoft-core") (v "0.2.0") (h "0xa4ciwjmj5gg9v4bmsm93wlgm80apzmr9cpk3d34jv7cg9wblmx")))

(define-public crate-com-croftsoft-core-0.3.0 (c (n "com-croftsoft-core") (v "0.3.0") (h "14djdslqdnafl2qvaqv70x2rskqm6g1pvq1z1ci2h1vwhy6wbf6v")))

(define-public crate-com-croftsoft-core-0.4.0 (c (n "com-croftsoft-core") (v "0.4.0") (h "1h2a9a7icdjz8v97v1zylkh62g4qn09982wx16vhn57davh7szib")))

(define-public crate-com-croftsoft-core-0.5.0 (c (n "com-croftsoft-core") (v "0.5.0") (h "015v8h1chc29p96745nnyh3470rdz4jfgbwpsw25jb7k6pbvh23s")))

(define-public crate-com-croftsoft-core-0.6.0 (c (n "com-croftsoft-core") (v "0.6.0") (h "0fihzq3w9jzmq8wii88l6w4wq75gm7rx32y3g08yzl492kcndscj")))

(define-public crate-com-croftsoft-core-0.7.0 (c (n "com-croftsoft-core") (v "0.7.0") (h "0dbpksm66kdczi5yp9kcz62rlmyhjp5nnfvi12n55qjmi3d2ag88")))

(define-public crate-com-croftsoft-core-0.8.0 (c (n "com-croftsoft-core") (v "0.8.0") (h "1y7mg3jzl6sh2jka9lbz0wgdih0k42m95qm2zcaq6a2vy2lrrkxc")))

(define-public crate-com-croftsoft-core-0.9.0 (c (n "com-croftsoft-core") (v "0.9.0") (h "01mxsxzga77rq9ifg98nriqv0xy35zksv256fazpkyhiyffrnbsr")))

(define-public crate-com-croftsoft-core-0.10.0 (c (n "com-croftsoft-core") (v "0.10.0") (h "15aifp1hfqjilnf66f5bb92rzkfg75jinr0x7lbgf529vvlzb6v4")))

(define-public crate-com-croftsoft-core-0.11.0 (c (n "com-croftsoft-core") (v "0.11.0") (h "1i5rfzql5cgnfv8908zc40gmv73yh2injkcmgzdslgb146aixgiq")))

(define-public crate-com-croftsoft-core-0.12.0 (c (n "com-croftsoft-core") (v "0.12.0") (h "0m8y0j6r9c4q8gnj78bwf5r50hdix9s7sgqa42b14kq0347chxvk")))

(define-public crate-com-croftsoft-core-0.13.0 (c (n "com-croftsoft-core") (v "0.13.0") (h "1h84wzqhsj4drd3zb4pxc4b91nyxv06znpk3m1d1gm4zg18apzhi")))

(define-public crate-com-croftsoft-core-0.14.0 (c (n "com-croftsoft-core") (v "0.14.0") (h "1z1lk4kpw67i9ziqrgb9qwkwzm2vy4dcrnk9q3h32bvg40g0ij6i")))

