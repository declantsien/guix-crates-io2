(define-module (crates-io co m- com-shim) #:use-module (crates-io))

(define-public crate-com-shim-0.1.0 (c (n "com-shim") (v "0.1.0") (d (list (d (n "com-shim-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1k7vwscrr8y4ph3sw4ysqkyrrznsadnzny6ydhfdnzdymqqrcgyy")))

(define-public crate-com-shim-0.1.1 (c (n "com-shim") (v "0.1.1") (d (list (d (n "com-shim-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1m2c9yjizbhz50j23axdr8pz078c4cbrr8xll6wd0n8f70wq0y43")))

(define-public crate-com-shim-0.1.2 (c (n "com-shim") (v "0.1.2") (d (list (d (n "com-shim-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1wf7am86kqvbmv7pvgqgfbxbb7v4gzml1cgas8vs6fcx58bsvja4")))

(define-public crate-com-shim-0.2.1 (c (n "com-shim") (v "0.2.1") (d (list (d (n "com-shim-macro") (r "^0.2.1") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0h4f2kbvqd5f0gybnpxr44fahs7snjlsl39mk1q4aj9aymq20w40")))

(define-public crate-com-shim-0.2.2 (c (n "com-shim") (v "0.2.2") (d (list (d (n "com-shim-macro") (r "^0.2.2") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0g8rc94k4735nw1mshbxylxl9cbif63r90ljfmv9syqp0kqawl7l")))

(define-public crate-com-shim-0.3.0 (c (n "com-shim") (v "0.3.0") (d (list (d (n "com-shim-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1drib5bafyyz5zc0ir55wshq0fk8z2l0lq1qk36m89v36s4dpl5s") (f (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3.1 (c (n "com-shim") (v "0.3.1") (d (list (d (n "com-shim-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "19ipyry9klyf4fvbcz2lrr03c32v4sr7bgn0961pfgxh7vn8xs2f") (f (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3.2 (c (n "com-shim") (v "0.3.2") (d (list (d (n "com-shim-macro") (r "^0.3.2") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1hviz5b2xcqpl6pqdqlj1cg555rh6sr2b3kg9cbkdmg8a72lmv0g") (f (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3.3 (c (n "com-shim") (v "0.3.3") (d (list (d (n "com-shim-macro") (r "^0.3.3") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "15xvm54wmjdqv5wyhc53vddik2qnmfid971lbak7cxwqrj4ql6nl") (f (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3.4 (c (n "com-shim") (v "0.3.4") (d (list (d (n "com-shim-macro") (r "^0.3.4") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1x2zww38qwpqdfgdjk4pcmrqgn2jq1v1ar7bm3w8w93rcam8bal6") (f (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3.5 (c (n "com-shim") (v "0.3.5") (d (list (d (n "com-shim-macro") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_System_Variant" "Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1avwlsq1i0zqh6c5xdwrkajcv7kx1m7kj6w87zsz4hcs9vpx4dwx") (f (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3.6 (c (n "com-shim") (v "0.3.6") (d (list (d (n "com-shim-macro") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_System_Variant" "Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1b9ipjya9hffnjw92f329rik28p0lx7k1msr1hdicyr93554l0dx") (f (quote (("debug" "com-shim-macro/debug"))))))

