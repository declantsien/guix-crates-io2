(define-module (crates-io co m- com-shim-macro) #:use-module (crates-io))

(define-public crate-com-shim-macro-0.1.0 (c (n "com-shim-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "0iizkld6iq9mg8w59b5axka7h0m1p8y5jiszvjdws3kxmf1576gs")))

(define-public crate-com-shim-macro-0.1.1 (c (n "com-shim-macro") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1wc0af6mzd7hfg1fv7mhj2zdpisf6vay8djdr2gbfb5b8rsx7g9a")))

(define-public crate-com-shim-macro-0.1.2 (c (n "com-shim-macro") (v "0.1.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "15w1wmhbla829w3qzlfplcx9755w82afgw3asv2bdc8ryigwp8kk")))

(define-public crate-com-shim-macro-0.2.0 (c (n "com-shim-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "0z7wfnxpyzwpr526hd9pdhq8h13q7nbi7xfz7liajm8sjf8vmaai")))

(define-public crate-com-shim-macro-0.2.1 (c (n "com-shim-macro") (v "0.2.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1wrsnh2fk83z6wxayrf25n4ks2inlipz2fx821765nnnqm5ln2rx")))

(define-public crate-com-shim-macro-0.2.2 (c (n "com-shim-macro") (v "0.2.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1zc7plikwsww1ic5vk0a5sqip0yywjkg8a4kr7kz0asimw3c6aq1")))

(define-public crate-com-shim-macro-0.3.0 (c (n "com-shim-macro") (v "0.3.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "078jmiw4xrrk9h2qd727fwr5gchg0awl9k175vqqrrqjv2am9m7x") (f (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3.1 (c (n "com-shim-macro") (v "0.3.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1jd0db12fpg10yyjiqlzrnambgkzr84cwnjd8dvwps1c94193q8w") (f (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3.2 (c (n "com-shim-macro") (v "0.3.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1fpns2pgp9d7c6p7sz2106vkczrl8fng9srd3nk9vw3mpjh1ndyn") (f (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3.3 (c (n "com-shim-macro") (v "0.3.3") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "0xa5053nlkszpwbj3mwmlil15z75siawfcf62iarvbxghmi73p86") (f (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3.4 (c (n "com-shim-macro") (v "0.3.4") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1s5naxqxj6w4krpxyib1lw0rddjalcgsla4p23wn2i6l79xj6a2c") (f (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3.5 (c (n "com-shim-macro") (v "0.3.5") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "0d68fgv439fj2l1fs7yq0dxggpa7pwfxyg8v0frwr5g01xl7w52a") (f (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3.6 (c (n "com-shim-macro") (v "0.3.6") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1pa4phw71sp8sqr2gl2d08dc302fzibxkgpi33wna5h9d6p2gg8m") (f (quote (("debug"))))))

