(define-module (crates-io co m- com-croftsoft) #:use-module (crates-io))

(define-public crate-com-croftsoft-0.1.0 (c (n "com-croftsoft") (v "0.1.0") (h "05yz1hy8ffw5nl25izj9sdm1w888y637946ibfiapm8xrn0yi9jp") (y #t)))

(define-public crate-com-croftsoft-0.1.1 (c (n "com-croftsoft") (v "0.1.1") (d (list (d (n "com-croftsoft-core") (r "^0.1.0") (d #t) (k 0)))) (h "1bpajbkf3acfmfvp7v0gnnjgkhpf4a8ndmzq9qihic28dmfqb3j3") (y #t)))

(define-public crate-com-croftsoft-0.1.2 (c (n "com-croftsoft") (v "0.1.2") (d (list (d (n "com-croftsoft-core") (r "^0.1.0") (d #t) (k 0)))) (h "1x9wb5s10pnxdiy6r7rfnya7xwfgz208cpq6jh5k5ivwq6x888c9")))

(define-public crate-com-croftsoft-0.2.0 (c (n "com-croftsoft") (v "0.2.0") (d (list (d (n "com-croftsoft-core") (r "^0.2.0") (d #t) (k 0)))) (h "19df7r44ac37gcz3bwv1ybn7d5wrhmkz1b5lm3c6mcn5xkzh2air")))

(define-public crate-com-croftsoft-0.9.0 (c (n "com-croftsoft") (v "0.9.0") (d (list (d (n "com-croftsoft-core") (r "^0.9.0") (d #t) (k 0)))) (h "0745faypyai4vbax1arwf0lxrrc6jcpwg9q713dg31wfknh4dhc0")))

(define-public crate-com-croftsoft-0.10.0 (c (n "com-croftsoft") (v "0.10.0") (d (list (d (n "com-croftsoft-core") (r "^0.10.0") (d #t) (k 0)))) (h "1pd1ichk5a0ykpb0b45hcz1k15ihky6443lralcscwbg1b3bpd7d")))

(define-public crate-com-croftsoft-0.11.0 (c (n "com-croftsoft") (v "0.11.0") (d (list (d (n "com-croftsoft-core") (r "^0.14.0") (d #t) (k 0)) (d (n "com-croftsoft-lib") (r "^0.1.1") (d #t) (k 0)))) (h "09w9migv4brnfy1gflhgnknjq0kb5r87pbfkms86h5kqcl04af07") (r "1.73")))

