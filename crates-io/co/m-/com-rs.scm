(define-module (crates-io co m- com-rs) #:use-module (crates-io))

(define-public crate-com-rs-0.1.0 (c (n "com-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "winapi") (r "^0.1.20") (d #t) (k 0)))) (h "1v29aqiiq4kmqvzdwmk354105q7m5s8zgixpiw0xgdw4mhy9vjln")))

(define-public crate-com-rs-0.1.1 (c (n "com-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "winapi") (r "^0.1.20") (d #t) (k 0)))) (h "0jic20a9kwxfba0ln8a0dvpb47i15clmy9q6pp0p98kq0qc5a0vp")))

(define-public crate-com-rs-0.1.2 (c (n "com-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "winapi") (r "^0.1.20") (d #t) (k 0)))) (h "1m0i39gr3sfq8gxnjz509vvcljy14jaykgi7smvjlkakvb99hgsh")))

(define-public crate-com-rs-0.1.3 (c (n "com-rs") (v "0.1.3") (d (list (d (n "winapi") (r "~0.2.0") (d #t) (k 0)))) (h "1pm5k93hikch9sslrl1sipiqkkhqn8fl86jw5vdqnw6b54w69pkz")))

(define-public crate-com-rs-0.1.4 (c (n "com-rs") (v "0.1.4") (h "0yb69xppy1wvw1g2vqdgk9nshbqilpi71dg0ik1jj2db2qxb3md2")))

(define-public crate-com-rs-0.1.5 (c (n "com-rs") (v "0.1.5") (h "0hbr1lbj8shil5j6916f98168nmkd0zxcd07l7hybq3g9s1p12mg")))

(define-public crate-com-rs-0.2.0 (c (n "com-rs") (v "0.2.0") (h "0qrf5i1pvb1b4cc00j888908fai6npg671y435j38bnwz4qkj5lq")))

(define-public crate-com-rs-0.2.1 (c (n "com-rs") (v "0.2.1") (h "0hk6051kwpabjs2dx32qkkpy0xrliahpqfh9df292aa0fv2yshxz")))

