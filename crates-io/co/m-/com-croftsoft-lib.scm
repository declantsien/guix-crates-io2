(define-module (crates-io co m- com-croftsoft-lib) #:use-module (crates-io))

(define-public crate-com-croftsoft-lib-0.1.0 (c (n "com-croftsoft-lib") (v "0.1.0") (d (list (d (n "com-croftsoft-lib-animation") (r "^0.0.8") (d #t) (k 0)) (d (n "com-croftsoft-lib-role") (r "^0.5.1") (d #t) (k 0)) (d (n "com-croftsoft-lib-string") (r "^0.1.0") (d #t) (k 0)))) (h "03vx69jkgw5dh635gnvkccgzcabpjywf9qwp507di4zqbdnz5fb1") (r "1.73")))

(define-public crate-com-croftsoft-lib-0.1.1 (c (n "com-croftsoft-lib") (v "0.1.1") (d (list (d (n "com-croftsoft-lib-animation") (r "^0.0.8") (d #t) (k 0)) (d (n "com-croftsoft-lib-role") (r "^0.5.1") (d #t) (k 0)) (d (n "com-croftsoft-lib-string") (r "^0.1.0") (d #t) (k 0)))) (h "0lpkcw29sd3z4jz5691grc996c2wa0qrp3mn5mxr0wmmcbd0nawj") (r "1.73")))

