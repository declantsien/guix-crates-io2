(define-module (crates-io co nl conllu) #:use-module (crates-io))

(define-public crate-conllu-0.1.0 (c (n "conllu") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1dz79vy3pd1vhwni2vxapja9iilhxxlv1g1xrbdinf0han4pn3pi")))

(define-public crate-conllu-0.1.1 (c (n "conllu") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "16pmsycd7nmq9ikfir16yihgp1y21s3hj98a9ak9fm7g00r3x58x")))

(define-public crate-conllu-0.2.0 (c (n "conllu") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "11l6dxv77kfzcg1nwspbh9fyzbz04cxd1kpx49w1zjppyh17hl1b")))

(define-public crate-conllu-0.3.0 (c (n "conllu") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1fb48n74qqj9p48igfxqv2xn9x5icn199b4fjj9c4v5mn3amvvlx")))

(define-public crate-conllu-0.3.1 (c (n "conllu") (v "0.3.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "18fk7chcx18garvdakzra6zilsqym0v9nyq2b6hyfgj7s955cnl1")))

(define-public crate-conllu-0.4.0 (c (n "conllu") (v "0.4.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hm1gfl3d7i0zkkk2bgzsrh5dda5pvldfndmfn6akkicl4bmswd9")))

(define-public crate-conllu-0.4.1 (c (n "conllu") (v "0.4.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07clmaabzgag918y5122k157l41b4d505q3vmbhnammf2j3mky7c")))

(define-public crate-conllu-0.5.0 (c (n "conllu") (v "0.5.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01jkdjy83w46f7v9apsiqfknfqxd160g38zn1sbbbkasvr8bylan")))

(define-public crate-conllu-0.5.1 (c (n "conllu") (v "0.5.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1l9x8h3if9jxxcdnjsx3lar1yl1i0cbklhlg3zk9jigv6m66dmq1")))

(define-public crate-conllu-0.6.0 (c (n "conllu") (v "0.6.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "udgraph") (r "^0.6") (d #t) (k 0)))) (h "0dvgir5n7qw0ng58xpy9ipsmi2swzwq0difnds7sggfc9y8caa30")))

(define-public crate-conllu-0.7.0 (c (n "conllu") (v "0.7.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "udgraph") (r "^0.7") (d #t) (k 0)))) (h "15ksbff94nf35392fwzd9jshrg7h2x3njjycx2fhhlywxpfka475")))

(define-public crate-conllu-0.8.0 (c (n "conllu") (v "0.8.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "udgraph") (r "^0.8") (d #t) (k 0)))) (h "11m4vq94xymrqvwp05rijpxnfm1v7wp0kdmm936ql2bsw66ks47v")))

