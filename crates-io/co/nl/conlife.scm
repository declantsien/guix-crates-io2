(define-module (crates-io co nl conlife) #:use-module (crates-io))

(define-public crate-conlife-0.1.0 (c (n "conlife") (v "0.1.0") (h "0g50xkf5yfhxyzzcq7mkvjncc8j63lg7qlb9h7xkvbryc55jbqg6")))

(define-public crate-conlife-0.1.1 (c (n "conlife") (v "0.1.1") (h "0z4f8v1q9006xrzzs02z68rw6l8fcr9crfyl0wb1dj8kffn1v4iv")))

(define-public crate-conlife-0.1.2 (c (n "conlife") (v "0.1.2") (h "0lyn2h9wc124mkpxhblnicg3l97k0zm3wm7qak3j480qxlr1xf4v")))

(define-public crate-conlife-0.1.3 (c (n "conlife") (v "0.1.3") (h "1ny5najhb8dxmdy3q2zbr7kg4g6qqb7xm3l39pigq51r2y7yzk8p")))

(define-public crate-conlife-0.1.4 (c (n "conlife") (v "0.1.4") (h "19prdn4shf5jmbrz4ba77ysp5bj9q2fwb0d9iy8si7jgkyf8smwy")))

