(define-module (crates-io co nl conllx) #:use-module (crates-io))

(define-public crate-conllx-0.0.1 (c (n "conllx") (v "0.0.1") (h "1058yjw3psdmms9zfzcdxv8lpgqybv505sawi8vchxa4pi3r0iq7")))

(define-public crate-conllx-0.1.0 (c (n "conllx") (v "0.1.0") (h "0ms2kajag7vir8r61m3qjisxndy07i38a1iq13bjihayyzxiddp2")))

(define-public crate-conllx-0.2.0 (c (n "conllx") (v "0.2.0") (h "12r6g60pwhiwnjcbh3drv3gy174xn06m88xfrh70fhxc2bgl8h8d")))

(define-public crate-conllx-0.2.1 (c (n "conllx") (v "0.2.1") (h "1zgvqc67rj4vhfvd6i9ky57gba1cghn4x3m1vpsdyh7ddn8ldnda")))

(define-public crate-conllx-0.2.2 (c (n "conllx") (v "0.2.2") (h "0dpyrcw02pic2d44zf455iakn6xh648gibibwl6mbdlzr49yfhr4")))

(define-public crate-conllx-0.3.0 (c (n "conllx") (v "0.3.0") (h "1pdsx954rsigyg71ml9m5xr4vb8dci28gzjv2ny636xnbjjdnfq8")))

(define-public crate-conllx-0.3.1 (c (n "conllx") (v "0.3.1") (h "073rg8wiihm64jm3g81pxkhhslvmh9dinvyh7zfdjg7y7hg64mip")))

(define-public crate-conllx-0.3.2 (c (n "conllx") (v "0.3.2") (h "0awdh5zzmy53blvym4i0k54mhg5qd4ldah63vms79adlsxddxb7v")))

(define-public crate-conllx-0.4.0 (c (n "conllx") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.9") (d #t) (k 0)))) (h "0399bmw229rjm3vh0cq31nljb74bf02widk24qbdkrdbv2bx29g2")))

(define-public crate-conllx-0.5.0 (c (n "conllx") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.9") (d #t) (k 0)))) (h "1klsxcws54nm9gmg084i09ncdii7fr8xn2vac0plj7is34dby8p1")))

(define-public crate-conllx-0.6.0 (c (n "conllx") (v "0.6.0") (d (list (d (n "error-chain") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "1bbaxqx0k0i5ybr72mwlls7iwf41zn2dqh2sj9vjrghz3r6fsn8v")))

(define-public crate-conllx-0.7.0 (c (n "conllx") (v "0.7.0") (d (list (d (n "error-chain") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy-init") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "0y9wxs068ncz2cqqzdj9ag4d3d04kxfa8pfkdhrjwmzhfhy84dgp")))

(define-public crate-conllx-0.7.1 (c (n "conllx") (v "0.7.1") (d (list (d (n "error-chain") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy-init") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "1a2zaxc8a3rvyjlihkaj7lmx1knarrn29q7fbxs02238xc36y7q8")))

(define-public crate-conllx-0.8.0 (c (n "conllx") (v "0.8.0") (d (list (d (n "error-chain") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy-init") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "13q9aimk3vlzv4g1hnxhycwm65v5i8sfmq61i90bf57dkbywfxhv")))

(define-public crate-conllx-0.9.0 (c (n "conllx") (v "0.9.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy-init") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)))) (h "1gvxpbv8wyqvpmgwg4fblvhgsykj1vn5sddfa5mbmdd1x0sif3fw")))

(define-public crate-conllx-0.9.1 (c (n "conllx") (v "0.9.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy-init") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "maplit") (r "^0.1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "17ajc7g61rbjd76s4jvn2djwhlm8v6waqjvnnlyvhlp74lqx529p")))

(define-public crate-conllx-0.9.2 (c (n "conllx") (v "0.9.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy-init") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "maplit") (r "^0.1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0mhczn1izkm19y7z8xgvvqv23p0l0hjpkcmq86n8rmxg4c6grjj9")))

(define-public crate-conllx-0.10.0 (c (n "conllx") (v "0.10.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy-init") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "1bmsm251j3v48df87yqsjasmhqv262f874hwsr93mhn6sh8l6ljk")))

(define-public crate-conllx-0.10.1 (c (n "conllx") (v "0.10.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy-init") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "19bmpwxny9bb9vmi0c82l0qqq245y13c8vzll1s9x14fz7clxvn3")))

(define-public crate-conllx-0.10.2 (c (n "conllx") (v "0.10.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy-init") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0vp07bvml2im80g4sd9wr3fwsz353wbwwlbd5jmw1jg9894ddjia")))

(define-public crate-conllx-0.11.0 (c (n "conllx") (v "0.11.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy-init") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "1wzvd9vp2kafxxa1k5l5lih98r9qi8z12g78j0174gsj255snf5s")))

(define-public crate-conllx-0.11.1 (c (n "conllx") (v "0.11.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy-init") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "16zpq2p6rkf6mw41i5911np88bray1xpn9ibhvdjgb9x0w3cwmjb")))

(define-public crate-conllx-0.11.2 (c (n "conllx") (v "0.11.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy-init") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)))) (h "0sdw0vmah26qzghvxkbc3j1haxrxhsyh5sysy1iqa9vzgvys2alh")))

(define-public crate-conllx-0.12.0 (c (n "conllx") (v "0.12.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "10jx8ndccswx0kwvsv7gkhk5q9dqk66g5rzax4dd81l10qn9gxmr")))

(define-public crate-conllx-0.12.1 (c (n "conllx") (v "0.12.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 2)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "152az96alwvqfdimf06w0dawgs8g784q4ywidf319cp15ijcb3n3")))

