(define-module (crates-io co g- cog-cli) #:use-module (crates-io))

(define-public crate-cog-cli-0.1.0 (c (n "cog-cli") (v "0.1.0") (h "1v2cgknb40rf1rv37idd57jpv08mvm8ch72wchgwvnq58lr1ywd0")))

(define-public crate-cog-cli-0.1.1 (c (n "cog-cli") (v "0.1.1") (h "0wv6d929b7z1a3rblxlvqwpd5vjknj5b0yss3gffkmllhpsg4gar")))

(define-public crate-cog-cli-0.1.2 (c (n "cog-cli") (v "0.1.2") (h "1pszy48vzyr0f6r7lxc14f4nl9y049q5a825z5qskr0k1ybzs142")))

(define-public crate-cog-cli-0.1.3 (c (n "cog-cli") (v "0.1.3") (h "0v4623scxizsgvmlpwizbfg86jcjd2l5zwqcigvqk1sidv9gyfqr")))

