(define-module (crates-io co g- cog-gen-rust) #:use-module (crates-io))

(define-public crate-cog-gen-rust-0.1.0 (c (n "cog-gen-rust") (v "0.1.0") (d (list (d (n "cog-idl") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)))) (h "0wx32z2wqwy7kdx6xc4y6xkvra4i30903sm0f20hwswhs8v4l8xv")))

