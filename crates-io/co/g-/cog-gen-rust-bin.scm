(define-module (crates-io co g- cog-gen-rust-bin) #:use-module (crates-io))

(define-public crate-cog-gen-rust-bin-0.1.0 (c (n "cog-gen-rust-bin") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cog-gen-rust") (r "^0.1.0") (d #t) (k 0)) (d (n "cog-idl") (r "^0.1.0") (d #t) (k 0)))) (h "0l3pxppm276z17zv1msfvh8ww45hmlby77adzqi02pbglw8vn6hv")))

