(define-module (crates-io co g- cog-gen-c-bin) #:use-module (crates-io))

(define-public crate-cog-gen-c-bin-0.1.0 (c (n "cog-gen-c-bin") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cog-gen-c") (r "^0.1.0") (d #t) (k 0)) (d (n "cog-idl") (r "^0.1.0") (d #t) (k 0)))) (h "0klpv9wazj00b9jgqy7w87y9zwir4hg3aj2n9va2jrkgnx4skjd4")))

