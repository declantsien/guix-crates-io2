(define-module (crates-io co g- cog-idl-bin) #:use-module (crates-io))

(define-public crate-cog-idl-bin-0.1.0 (c (n "cog-idl-bin") (v "0.1.0") (d (list (d (n "cog-idl") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1nf4k1wh8rvahs0irnvcdgrcbpjmcb1bbx57xc6blizv577jlsr9")))

(define-public crate-cog-idl-bin-0.2.0 (c (n "cog-idl-bin") (v "0.2.0") (d (list (d (n "cog-idl") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1lyb5awf545nsnq1krwa8dzj8bhcjf6cnb3lxv8zx30f33lc5il4")))

