(define-module (crates-io co g- cog-idl) #:use-module (crates-io))

(define-public crate-cog-idl-0.1.0 (c (n "cog-idl") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1nygby4p0f7diwvk7lci59kxiriw96gyldla4n5mqxxyis06vb18")))

(define-public crate-cog-idl-0.1.1 (c (n "cog-idl") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0015h4nlxsvnb1brjnw9sj4fm699ihgxjgas3w1jcn4d2a0q2mcx") (f (quote (("default"))))))

