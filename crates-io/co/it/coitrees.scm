(define-module (crates-io co it coitrees) #:use-module (crates-io))

(define-public crate-coitrees-0.2.0 (c (n "coitrees") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1w1x91zks1p8y766vdd0z2c33wp9g2f326zgfihlaasglx6kr0pr")))

(define-public crate-coitrees-0.2.1 (c (n "coitrees") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1l2ybr8n02vm08wq9mrix7r07bgwm85i6fyachlm8d626w9w9d3f")))

(define-public crate-coitrees-0.3.0 (c (n "coitrees") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.7") (f (quote ("derive"))) (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "08qvm0nqgck6hzrlndni6xfyf36qnszcjpjdsv1l4kny5gnvb95i") (f (quote (("nosimd"))))))

(define-public crate-coitrees-0.4.0 (c (n "coitrees") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.7") (f (quote ("derive"))) (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qwb4c5gx30gl1kyi85rbq6z23l2f9lm0q02ym160n0fvc89c3r4") (f (quote (("nosimd"))))))

