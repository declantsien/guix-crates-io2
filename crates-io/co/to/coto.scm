(define-module (crates-io co to coto) #:use-module (crates-io))

(define-public crate-coto-0.1.0 (c (n "coto") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 0)) (d (n "console") (r "^0.11.3") (d #t) (k 0)) (d (n "device_query") (r "^0.2.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1sa425fnmyjfvky0dk1zc269vl2mm4q1b2vdm0a9pambq367ffha")))

