(define-module (crates-io co to cotoxy) #:use-module (crates-io))

(define-public crate-cotoxy-0.1.0 (c (n "cotoxy") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "miasht") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serdeconv") (r "^0.3") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "sloggers") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0s3lqp22350bp0698si1n7x4qf9x66prasmmzxv1zjqjxl64i3vf")))

