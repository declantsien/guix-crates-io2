(define-module (crates-io co ff coffee-time) #:use-module (crates-io))

(define-public crate-coffee-time-0.0.1 (c (n "coffee-time") (v "0.0.1") (h "01c10kwj3d25vdv10yvi43x43rvp2bz3is2p599wfzr341aqic7k")))

(define-public crate-coffee-time-0.0.3 (c (n "coffee-time") (v "0.0.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ma2bgbms9slblg44d082csrs29r420b5zf6l0bxif1ln80gvriz")))

