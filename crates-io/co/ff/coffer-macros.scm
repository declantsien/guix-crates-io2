(define-module (crates-io co ff coffer-macros) #:use-module (crates-io))

(define-public crate-coffer-macros-1.0.0 (c (n "coffer-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("default" "printing"))) (d #t) (k 0)))) (h "0spd3dnbkq5fi3jykk5szsyz15vzfysisla6sfl7zci2brcv17nh")))

