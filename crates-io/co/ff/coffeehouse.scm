(define-module (crates-io co ff coffeehouse) #:use-module (crates-io))

(define-public crate-coffeehouse-0.1.0 (c (n "coffeehouse") (v "0.1.0") (h "16by76b87hxxv44wdjcsy96v700xqxkw6lkxdrj5bm9p1npncf5p")))

(define-public crate-coffeehouse-0.1.1 (c (n "coffeehouse") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q72gn260nr7w218gn43b3b9ss17pxrx1b4j51lqjla6v3j940vl")))

