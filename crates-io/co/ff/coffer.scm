(define-module (crates-io co ff coffer) #:use-module (crates-io))

(define-public crate-coffer-1.0.0 (c (n "coffer") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "coffer-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08z5b6gcf2g2brgl6qwv7y91bhjq783x96v5gqgh5m72m4x7h07p") (f (quote (("default") ("backtrace"))))))

(define-public crate-coffer-1.0.1 (c (n "coffer") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "coffer-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yc323sihb039l7gpcnrq4jrnxgffy32ga1yqrglzd4a6p88d53f") (f (quote (("default") ("backtrace"))))))

