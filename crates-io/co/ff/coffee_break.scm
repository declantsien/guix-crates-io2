(define-module (crates-io co ff coffee_break) #:use-module (crates-io))

(define-public crate-coffee_break-1.0.0 (c (n "coffee_break") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "0f4rzv6j69r3fr1a94g675q4gxka9rz22n1q50ddv2rndzaqff3m") (r "1.63.0")))

(define-public crate-coffee_break-2.1.1 (c (n "coffee_break") (v "2.1.1") (d (list (d (n "syn") (r "^2.0.57") (f (quote ("proc-macro" "parsing"))) (k 0)))) (h "1j5gq17fjlqgmr3cg03cm1y7pa4gyasbk1fixx0hl4kypyhad1cj") (f (quote (("ra-friendly") ("default" "ra-friendly" "check-friendly") ("check-friendly")))) (r "1.63.0")))

