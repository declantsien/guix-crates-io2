(define-module (crates-io co ff coffret) #:use-module (crates-io))

(define-public crate-coffret-0.0.1 (c (n "coffret") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "rb-sys") (r "^0.8.1") (f (quote ("link-ruby"))) (d #t) (k 0)))) (h "0n5x8hqydl5xzxdgchfpyil5rpylcx3dmp0q395xj1cnb2cp7zfh")))

(define-public crate-coffret-0.0.2 (c (n "coffret") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rb-sys") (r "^0.8.1") (f (quote ("link-ruby"))) (d #t) (k 0)))) (h "1bk5smfqpnpwln7mfwwz0mk0xqjhpkc893ar2hfmhlx669m6fvv3")))

(define-public crate-coffret-0.0.3 (c (n "coffret") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rb-sys") (r "^0.9") (f (quote ("link-ruby"))) (d #t) (k 0)))) (h "17341v238b9nv3dnjry89amjpwq38kwr74mg74cyr7y8wwpv9c7g")))

