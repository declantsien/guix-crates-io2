(define-module (crates-io co ff coffeevis) #:use-module (crates-io))

(define-public crate-coffeevis-0.2.0 (c (n "coffeevis") (v "0.2.0") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "0nvgqk9mq70yfra5qfz7syi85jlajgxkb6n9aydm8h54frans8p7")))

(define-public crate-coffeevis-0.2.1 (c (n "coffeevis") (v "0.2.1") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "1mymf01jwqliia8r5pxhn23wm9aw52z4hgjzzfh93hjyns93jz48")))

(define-public crate-coffeevis-0.3.0 (c (n "coffeevis") (v "0.3.0") (d (list (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "1qh69jrc6lw13h862zzcxg4gmxklfbpnp0v89snjd9ywr8rp70al")))

(define-public crate-coffeevis-0.4.0 (c (n "coffeevis") (v "0.4.0") (d (list (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "minifb") (r "^0.24") (d #t) (k 0)))) (h "0nachajl25giq407z2mffpsrqgmyzq5sji3wj6ygrw5ahjk2lf6a")))

