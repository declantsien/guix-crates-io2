(define-module (crates-io co nd condtype) #:use-module (crates-io))

(define-public crate-condtype-1.0.0 (c (n "condtype") (v "1.0.0") (h "1rrx62s4sdfm6ns1w3iz63qaic0s1836xdx8sbs268mnxy97jmzw") (r "1.59.0")))

(define-public crate-condtype-1.1.0 (c (n "condtype") (v "1.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.141") (d #t) (k 2)))) (h "0p0q2sg0if014z71bjc4148ln7akbnrz14jbl2isdsfq861g8dhx") (r "1.59.0")))

(define-public crate-condtype-1.2.0 (c (n "condtype") (v "1.2.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.141") (d #t) (k 2)))) (h "1xagbhfh2xcdzicssqz173f3njgvgwzfr79a5kgrmyqcwq7zv7v0") (r "1.59.0")))

(define-public crate-condtype-1.3.0 (c (n "condtype") (v "1.3.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.141") (d #t) (k 2)))) (h "1by78npyhkc30jccc7kirvwip1fj0jhi2bwfmcw44dqz81xa1w5s") (r "1.59.0")))

