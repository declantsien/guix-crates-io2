(define-module (crates-io co nd conduit-log-requests) #:use-module (crates-io))

(define-public crate-conduit-log-requests-0.5.0 (c (n "conduit-log-requests") (v "0.5.0") (d (list (d (n "conduit") (r "*") (d #t) (k 0)) (d (n "conduit-middleware") (r "*") (d #t) (k 0)) (d (n "conduit-test") (r "*") (d #t) (k 0)))) (h "09yxsfz3zz36w6fvpzw9asjmvgmfnfmrnz135anmpqblpamyi78f")))

(define-public crate-conduit-log-requests-0.5.1 (c (n "conduit-log-requests") (v "0.5.1") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.1") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1bivvjgn5vhm9s8mm7wh091433f7mfyc1f5a5vpndz60yaj46x3q")))

(define-public crate-conduit-log-requests-0.5.2 (c (n "conduit-log-requests") (v "0.5.2") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.1") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "log") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1c1r3dyyw5hxl3w6wsjjp2w3apg8wq9rpm174mh7a663n9j5xyrn")))

(define-public crate-conduit-log-requests-0.5.3 (c (n "conduit-log-requests") (v "0.5.3") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.1") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "log") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1v28icisxaz1ggk79n1bvaxsdzvxrdwjx5xhq38bwx3gs0mhb75l")))

(define-public crate-conduit-log-requests-0.6.0 (c (n "conduit-log-requests") (v "0.6.0") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.6") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1kk0as880h6ahagd3pa2dahv5zznjcc1av8c3vcma864cvnyc3rp")))

(define-public crate-conduit-log-requests-0.6.1 (c (n "conduit-log-requests") (v "0.6.1") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.6") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "09463ql7bsps4rvy0wrsnnw7c27md9rmd806nhcdld4wl90sx5rn")))

(define-public crate-conduit-log-requests-0.6.2 (c (n "conduit-log-requests") (v "0.6.2") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.6") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1l9p68fqlx9y4zknnrmx9xmb3qvzw4q4njl60hv2rs0i0zvjghkk")))

(define-public crate-conduit-log-requests-0.6.3 (c (n "conduit-log-requests") (v "0.6.3") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.6") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1n1imw1nnbyqidncv11rg8dd4a1ri5i59flhw4flmm81mxnmsk4h")))

(define-public crate-conduit-log-requests-0.7.0 (c (n "conduit-log-requests") (v "0.7.0") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0c5msr04xsbc9snq4c14c9q8arq4imdnsrjhhv913n6b8djp38q8")))

(define-public crate-conduit-log-requests-0.7.1 (c (n "conduit-log-requests") (v "0.7.1") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1nm26yghvzrszj5nmh8xvmw8r85i24gqm6p6a8z8wna88rdz3610")))

(define-public crate-conduit-log-requests-0.7.2 (c (n "conduit-log-requests") (v "0.7.2") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "00br8836an9hi8ph534vm3bsq79xg6kq1pvar4i9ib71if1pzvl4")))

(define-public crate-conduit-log-requests-0.7.3 (c (n "conduit-log-requests") (v "0.7.3") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1b4n5cdn67lzw37fkidfnzssxsvbkkc04dix49ranrisfl85sxig")))

(define-public crate-conduit-log-requests-0.8.0 (c (n "conduit-log-requests") (v "0.8.0") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.8") (d #t) (k 0)) (d (n "conduit-test") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "00bn1gd82sk3rlnzhfik5fn4yvfc5z350am2414q5fsn52wwfw05")))

