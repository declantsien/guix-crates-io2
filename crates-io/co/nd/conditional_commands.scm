(define-module (crates-io co nd conditional_commands) #:use-module (crates-io))

(define-public crate-conditional_commands-0.1.0 (c (n "conditional_commands") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (k 0)))) (h "0mzgnrwyysmz7s6xnyn1d201rjv45ikxli9p6l2njwpv1blp8z8j")))

(define-public crate-conditional_commands-0.2.0 (c (n "conditional_commands") (v "0.2.0") (d (list (d (n "bevy") (r "^0.7.0") (k 0)))) (h "0pb5dip2iv616ibndbsb4j2kgr7wb5fx5cy145z4m9vlhhfrx7a9")))

(define-public crate-conditional_commands-0.3.0 (c (n "conditional_commands") (v "0.3.0") (d (list (d (n "bevy") (r "^0.7.0") (k 0)))) (h "0h610d6zw6x8si0v9x28nzj4ynqr2zxa5z4ji8hvcasqc6kl94f6")))

(define-public crate-conditional_commands-0.4.0 (c (n "conditional_commands") (v "0.4.0") (d (list (d (n "bevy") (r "^0.7.0") (k 0)))) (h "0h2f34pg313mvxcjkjk218sz5bjj0mr1r4wg2rnls1jip0l7vqgw")))

(define-public crate-conditional_commands-0.5.0 (c (n "conditional_commands") (v "0.5.0") (d (list (d (n "bevy") (r "^0.7.0") (k 0)))) (h "0am2k5dgakhh1gnli8z9fgnyhmx30x2584c24df6g6v9pd1s62s3")))

(define-public crate-conditional_commands-0.6.0 (c (n "conditional_commands") (v "0.6.0") (d (list (d (n "bevy") (r "^0.8.0") (k 0)))) (h "170cy5nh8kzlwly76zwjvq5zyr8v7n170wyxll037z9s8sbmrk3m")))

(define-public crate-conditional_commands-0.7.0 (c (n "conditional_commands") (v "0.7.0") (d (list (d (n "bevy") (r "^0.9.0") (k 0)))) (h "0mhlbynaajvy1wnhra1519hvi9qj2vlilgj36xzhnhpwzcqvvfi6")))

