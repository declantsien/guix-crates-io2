(define-module (crates-io co nd conditional-assignment) #:use-module (crates-io))

(define-public crate-conditional-assignment-0.1.0 (c (n "conditional-assignment") (v "0.1.0") (h "145apzy51v7585x2nyhhlmrdbbk6vgfg260lvdn7rdsqpr3ihc0i") (r "1.56.1")))

(define-public crate-conditional-assignment-0.2.0 (c (n "conditional-assignment") (v "0.2.0") (h "1dxzlkhnv510612x1ff4scy4cb4fqb6j0d2vbh5c32j45c101hja") (r "1.56.1")))

