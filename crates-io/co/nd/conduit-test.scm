(define-module (crates-io co nd conduit-test) #:use-module (crates-io))

(define-public crate-conduit-test-0.5.0 (c (n "conduit-test") (v "0.5.0") (d (list (d (n "conduit") (r "*") (d #t) (k 0)) (d (n "semver") (r "*") (d #t) (k 0)))) (h "1cwb2bnfnccf12wb3ngmak0b2zk1qfpqfyri9svxv5h7lhirdhmv")))

(define-public crate-conduit-test-0.5.1 (c (n "conduit-test") (v "0.5.1") (d (list (d (n "conduit") (r "^0.5.1") (d #t) (k 0)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0xkxdj06vjaax9bs09rdzqxrv6kypizrz988yqmxn9dx5s2hkkhm")))

(define-public crate-conduit-test-0.5.2 (c (n "conduit-test") (v "0.5.2") (d (list (d (n "conduit") (r "^0.5.1") (d #t) (k 0)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0jsg9q009viv9c11pf57n5vk5i53krs55mpxh0b40922vxnshxxr")))

(define-public crate-conduit-test-0.5.3 (c (n "conduit-test") (v "0.5.3") (d (list (d (n "conduit") (r "^0.5.1") (d #t) (k 0)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0cv7phmixks4g79dg4hs9d78ac4a919k8bbk8j65bgpnrly237jh")))

(define-public crate-conduit-test-0.6.0 (c (n "conduit-test") (v "0.6.0") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "0y4vkarh5x4cpsi07cs2vb2564za1hfjd9kngywqh7lbhwir6ys5")))

(define-public crate-conduit-test-0.6.1 (c (n "conduit-test") (v "0.6.1") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "0mfxp58mhnfd5c67qridcgp4764vixin2xgn1mj4g0m55hli6aqp")))

(define-public crate-conduit-test-0.6.2 (c (n "conduit-test") (v "0.6.2") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1d0pffnskqc24iwcxpjmcw6xqbb2y2sk4b4k6s9c118x83ygfmgd")))

(define-public crate-conduit-test-0.7.0 (c (n "conduit-test") (v "0.7.0") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "0dh5s66z4bavshls18v41df10xhmz78g98c5619ddadm41lly8ks")))

(define-public crate-conduit-test-0.7.1 (c (n "conduit-test") (v "0.7.1") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1m4h8q073cn4pma8bq6vr4fgh4bw34xvk4s8fxhwpnxa12mj9z9j")))

(define-public crate-conduit-test-0.7.2 (c (n "conduit-test") (v "0.7.2") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1qzixs429lhdnfxcnjz0k2ixc9p4k9f5pnsdrxmprglinw6a41yp")))

(define-public crate-conduit-test-0.7.3 (c (n "conduit-test") (v "0.7.3") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.2") (d #t) (k 0)))) (h "1ldgh7k9zdr5npk7i0730y1iggsqqiq1qhbb85ppkfpi15fg06yz")))

(define-public crate-conduit-test-0.8.0 (c (n "conduit-test") (v "0.8.0") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "1gk5i9nbvk9z68d6mrdqpzkn0qjpsrbrjchc500qsnvp3n65m1r6")))

(define-public crate-conduit-test-0.8.1 (c (n "conduit-test") (v "0.8.1") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "0afli48mafy8zqizig42pi482k3w9vkwk5jpz3xhf59yf7ilqs3m")))

(define-public crate-conduit-test-0.9.0-alpha.0 (c (n "conduit-test") (v "0.9.0-alpha.0") (d (list (d (n "conduit") (r "^0.9.0-alpha.0") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "0bw4x45yqrhq6sz9sb6xad6sww2nz3g2sxz8arx0hc3nk8kjl6yq")))

(define-public crate-conduit-test-0.9.0-alpha.1 (c (n "conduit-test") (v "0.9.0-alpha.1") (d (list (d (n "conduit") (r "^0.9.0-alpha.1") (d #t) (k 0)))) (h "1nbk3jlwv49jai4sb3pcb8glv916r9bhgrmbiv9rgvzzzds3b7m2")))

(define-public crate-conduit-test-0.9.0-alpha.2 (c (n "conduit-test") (v "0.9.0-alpha.2") (d (list (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)))) (h "1p1245rpjmimijdqv7i7xv8q668zgn84cynzyd15592pkxm2rxqr")))

(define-public crate-conduit-test-0.9.0-alpha.3 (c (n "conduit-test") (v "0.9.0-alpha.3") (d (list (d (n "conduit") (r "^0.9.0-alpha.3") (d #t) (k 0)))) (h "1x8yzv5crl0dlfbj83sc727fm79s16dmdn3qbqhwmial0nyz7ckm")))

(define-public crate-conduit-test-0.9.0-alpha.4 (c (n "conduit-test") (v "0.9.0-alpha.4") (d (list (d (n "conduit") (r "^0.9.0-alpha.4") (d #t) (k 0)))) (h "1lllsdz6lx17riagxn1616xz8dhza6r1ipdpqqvn464kq6iglk1w")))

(define-public crate-conduit-test-0.9.0-alpha.5 (c (n "conduit-test") (v "0.9.0-alpha.5") (d (list (d (n "conduit") (r "^0.9.0-alpha.5") (d #t) (k 0)))) (h "054vmfc2b23x9hmy723zqzr83gsr8jl7z83y8z1d9vj3s2811r9j")))

(define-public crate-conduit-test-0.9.0 (c (n "conduit-test") (v "0.9.0") (d (list (d (n "conduit") (r "^0.9.0") (d #t) (k 0)))) (h "0qnccz0vhlcyxamigjam2mfxnying7zkkzlyf11qzpb09g5flb7b")))

(define-public crate-conduit-test-0.10.0 (c (n "conduit-test") (v "0.10.0") (d (list (d (n "conduit") (r "^0.10.0") (d #t) (k 0)))) (h "193l17nmjwdxcclvszjfd70b8ikfgmc5rylb7rcy0p40capmgnzi")))

