(define-module (crates-io co nd conduit-git-http-backend) #:use-module (crates-io))

(define-public crate-conduit-git-http-backend-0.6.0 (c (n "conduit-git-http-backend") (v "0.6.0") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "flate2") (r "^0.1") (d #t) (k 0)))) (h "1ls9qkb4lbzdji3zxdqrakjx0amml9kdjn30qrdwbvxldrp9p3nq")))

(define-public crate-conduit-git-http-backend-0.7.0 (c (n "conduit-git-http-backend") (v "0.7.0") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0kgw7v82jd3x1b1ff1bbn5izk627gqx1nvnz3qw71vlhkn1mkwcj")))

(define-public crate-conduit-git-http-backend-0.7.1 (c (n "conduit-git-http-backend") (v "0.7.1") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0bc7rynkf67ynslpq1mn2igbmaf1nk9bifn0dhngjgifpc3cv10p")))

(define-public crate-conduit-git-http-backend-0.7.2 (c (n "conduit-git-http-backend") (v "0.7.2") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "1yr6sw2f5wfmx9hj1938ifsfhfdjvcfgm70qfy8m19lzw0azfs71")))

(define-public crate-conduit-git-http-backend-0.7.3 (c (n "conduit-git-http-backend") (v "0.7.3") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "05qijdndf34k2w3z3rzs4jh2ildkbq1w8wqka401k7kal87629m5")))

(define-public crate-conduit-git-http-backend-0.7.4 (c (n "conduit-git-http-backend") (v "0.7.4") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "12cvd170890r9hl5mmj1bqmsbfn6hn7ax7bx1g2zn64vga9yicwm")))

(define-public crate-conduit-git-http-backend-0.7.5 (c (n "conduit-git-http-backend") (v "0.7.5") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "15glkllyvr9s1blzwq7x12rgbimsx403krjs4c3gn3gggalvjyf1")))

(define-public crate-conduit-git-http-backend-0.8.0 (c (n "conduit-git-http-backend") (v "0.8.0") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0vmybz2pm2xgw7a3k7qrv7q6mpi4bb4ayqah5gayq2ypmw01jyh2")))

(define-public crate-conduit-git-http-backend-0.9.0-alpha.0 (c (n "conduit-git-http-backend") (v "0.9.0-alpha.0") (d (list (d (n "conduit") (r "^0.9.0-alpha.0") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1awyrq96x132pymh439ilyb5yzs87rg34w4s2p66wzqsjfywpyac")))

(define-public crate-conduit-git-http-backend-0.9.0-alpha.1 (c (n "conduit-git-http-backend") (v "0.9.0-alpha.1") (d (list (d (n "conduit") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "0jkx5azvfflcizy04y88djw9r1qkrh59llbqb51qxg8jnv664nbs")))

(define-public crate-conduit-git-http-backend-0.9.0-alpha.2 (c (n "conduit-git-http-backend") (v "0.9.0-alpha.2") (d (list (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "10y3f4v9zgaimaagxrw3if9yayfj32sk7c6p0cbzl4a1nprawh9a")))

(define-public crate-conduit-git-http-backend-0.9.0 (c (n "conduit-git-http-backend") (v "0.9.0") (d (list (d (n "conduit") (r "^0.9.0") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1w9jgdjx1zyllijw4ajcz1nc3z45hfdi5rd98bn175jgdd9vvlyj") (r "1.46.0")))

(define-public crate-conduit-git-http-backend-0.10.0 (c (n "conduit-git-http-backend") (v "0.10.0") (d (list (d (n "conduit") (r "^0.10.0") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1gf8q6nyqg6wdznd553fz9hwc1sfqj1070msx75gvnwg2irq7gqs") (r "1.46.0")))

