(define-module (crates-io co nd condition_variable) #:use-module (crates-io))

(define-public crate-condition_variable-0.1.0 (c (n "condition_variable") (v "0.1.0") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "09nmgd5zcgqq41z6cylgb3j3hym2yipibw1hij7mr6mjlgcmfawm")))

(define-public crate-condition_variable-0.1.1 (c (n "condition_variable") (v "0.1.1") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0z9z6nxcnxck6bfvwfj6v5icrxnb7wiskwlicv0vn453sd6ipdc0")))

(define-public crate-condition_variable-0.1.2 (c (n "condition_variable") (v "0.1.2") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "135vvjjx468y4xfssnsvy22zvcha8fk2r3h50f1jp6sd3ih66rzs")))

(define-public crate-condition_variable-0.1.3 (c (n "condition_variable") (v "0.1.3") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0flp3ian791555r9pdk7axlf86jm5psij0jasj8q5rwciq45aisg")))

(define-public crate-condition_variable-0.1.4 (c (n "condition_variable") (v "0.1.4") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0d1s49rynal8xkrq3mf9r9kqrydll6qizap9lyv4w2dkvbsrf82j")))

(define-public crate-condition_variable-0.1.5 (c (n "condition_variable") (v "0.1.5") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "106x7ismmfm3rggc8sp00zvblrc43m1v8lbiv5xz5kw7vpr0kx5y")))

