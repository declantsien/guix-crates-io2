(define-module (crates-io co nd condest) #:use-module (crates-io))

(define-public crate-condest-0.1.0 (c (n "condest") (v "0.1.0") (d (list (d (n "cblas") (r "^0.2") (d #t) (k 0)) (d (n "lapacke") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9") (d #t) (k 2)) (d (n "openblas-src") (r "^0.7") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.1") (d #t) (k 0)))) (h "0i0ima36d9679c063palrdclsi9kwafxv82siyzk69lmaxinnx36")))

(define-public crate-condest-0.2.0 (c (n "condest") (v "0.2.0") (d (list (d (n "cblas") (r "^0.2") (d #t) (k 0)) (d (n "lapacke") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9") (d #t) (k 2)) (d (n "openblas-src") (r "^0.7") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.1") (d #t) (k 0)))) (h "1aimbbdysgfk23j0s2kvcdd423cf40pd3vv44vy1dxwrx474kd75")))

(define-public crate-condest-0.2.1 (c (n "condest") (v "0.2.1") (d (list (d (n "cblas") (r "^0.2") (d #t) (k 0)) (d (n "lapacke") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.9") (d #t) (k 2)) (d (n "openblas-src") (r "^0.7") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.1") (d #t) (k 0)))) (h "049lql71cm15chgncffp7z93iwp88f5spa5l3424598b3aynvb41")))

