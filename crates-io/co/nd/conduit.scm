(define-module (crates-io co nd conduit) #:use-module (crates-io))

(define-public crate-conduit-0.5.0 (c (n "conduit") (v "0.5.0") (d (list (d (n "semver") (r "*") (d #t) (k 0)))) (h "1qg13kznzw49x47kxx3vf8qj025q1n0383000jwydq1c78w5wdir") (y #t)))

(define-public crate-conduit-0.5.1 (c (n "conduit") (v "0.5.1") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0qq74j8fbnvsyl807i81hadvv0adgqv5s30838jpi8cisj8ap0zc")))

(define-public crate-conduit-0.5.2 (c (n "conduit") (v "0.5.2") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1k7i04s9s1xb51y38ximy5k227mc4r7sg94358w7lmah5s833jzv")))

(define-public crate-conduit-0.5.3 (c (n "conduit") (v "0.5.3") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0maqh89x6lbrx1mwy25vhhcjq3rkw17p51iirmbj3g4sc5gxv5im")))

(define-public crate-conduit-0.5.4 (c (n "conduit") (v "0.5.4") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0b7m0zxf92a8cf1kvjvikzxvsfr9rdyyd3c8rvina6s3rlpk159j")))

(define-public crate-conduit-0.6.0 (c (n "conduit") (v "0.6.0") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0fm87lnw5a2ml03sxvzbr30k80n2bqdb79b2vfgx8gg211h0fbl7")))

(define-public crate-conduit-0.6.1 (c (n "conduit") (v "0.6.1") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1v43agj6fkadb8xzcpnxszin8i4skfazbm82djbi7pgxsh7l121y")))

(define-public crate-conduit-0.6.2 (c (n "conduit") (v "0.6.2") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "06g3b368jhab2nd6fxivmgqipvdd5m6jp1yf4wh9b37lkqi7cj30")))

(define-public crate-conduit-0.6.3 (c (n "conduit") (v "0.6.3") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "146fnii2asjszb1i6hv4smxpap46jj30kiy4g5brv3ib318b41gs")))

(define-public crate-conduit-0.6.4 (c (n "conduit") (v "0.6.4") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0rwkkpnzg4csmpazkyhhl6bs4km6h4nvvh7znwnahd1lfxghmr8g")))

(define-public crate-conduit-0.6.5 (c (n "conduit") (v "0.6.5") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1kpqdql0yd73j3q4dl6di2l7rafi5446vn0s28zminx3gsxfpdnm")))

(define-public crate-conduit-0.6.6 (c (n "conduit") (v "0.6.6") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0772c6y2a355nvmrg6gv9sffy3rg0jyvl03ymyxkx4wni4g5hx3g")))

(define-public crate-conduit-0.7.0 (c (n "conduit") (v "0.7.0") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "085f3npklxvqsn3f3n6y062vp1g7w2g81zl0372kv9n0hl7cf8q3")))

(define-public crate-conduit-0.7.1 (c (n "conduit") (v "0.7.1") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1zs833qinrkszlgf1876j550nck8pph972hczzzwh9vxsc8a2igi")))

(define-public crate-conduit-0.7.2 (c (n "conduit") (v "0.7.2") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1ns695gf4lav8ajvy4713ji2k8ybi5dn7hwq3g8v7nkh1a4rfnyn")))

(define-public crate-conduit-0.7.3 (c (n "conduit") (v "0.7.3") (d (list (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1xpgzd71gp116ff978anm3vz5fixmi5x16dihda5f4nziyz66n6r")))

(define-public crate-conduit-0.7.4 (c (n "conduit") (v "0.7.4") (d (list (d (n "semver") (r "^0.2.0") (d #t) (k 0)))) (h "0kf0zqlmcmkr3b5lv881srgrnwkls42rvqz6hd5rawldbm345ivh")))

(define-public crate-conduit-0.8.0 (c (n "conduit") (v "0.8.0") (d (list (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "0fnnggc2srd3z1s4qa07bn6iksry8947f84z5b3z1l26agl72667")))

(define-public crate-conduit-0.8.1 (c (n "conduit") (v "0.8.1") (d (list (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "10mw508gyskqh9b835wq8p2fbv2k498n46q79k0ps84cywqal36v")))

(define-public crate-conduit-0.9.0-alpha.0 (c (n "conduit") (v "0.9.0-alpha.0") (d (list (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1gwlj576fifdasgrp1r1l8xx52v23p6mwvi49wr4kl5g6xcjpbip")))

(define-public crate-conduit-0.9.0-alpha.1 (c (n "conduit") (v "0.9.0-alpha.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "159prchhc4v0wzq7mmx317yczcg559y41sh4jhlh2shv3zs079rc")))

(define-public crate-conduit-0.9.0-alpha.2 (c (n "conduit") (v "0.9.0-alpha.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "04p4vsi22qhh9z99k8l2gwlmwfhnm74wfgkmxmp34dz38vfjwhs7")))

(define-public crate-conduit-0.9.0-alpha.3 (c (n "conduit") (v "0.9.0-alpha.3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0ch4d89640c5iqc2dpy1iajndkgjvzk6za7aa3w2j4v0wgr5zxb8")))

(define-public crate-conduit-0.9.0-alpha.4 (c (n "conduit") (v "0.9.0-alpha.4") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1hz2qfrmx77w44v3n9syabla59ik7j8176nxyl1acm9imcdnc09l")))

(define-public crate-conduit-0.9.0-alpha.5 (c (n "conduit") (v "0.9.0-alpha.5") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1z2wx743rp9pw8mw2f3dwkcbhf25cas76sdh004wnbwz756rbs3c")))

(define-public crate-conduit-0.9.0 (c (n "conduit") (v "0.9.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "1az65aybqkz5b8x8w39sf1grpz06mw9gkg77hc3six546y07n9pi")))

(define-public crate-conduit-0.10.0 (c (n "conduit") (v "0.10.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0vx9m0iwjhiy11c122jbqkhnif8pnzsif204b8q4cqr98w3sirdl")))

