(define-module (crates-io co nd conduit-utils) #:use-module (crates-io))

(define-public crate-conduit-utils-0.5.0 (c (n "conduit-utils") (v "0.5.0") (d (list (d (n "conduit") (r "*") (d #t) (k 0)) (d (n "conduit-test") (r "*") (d #t) (k 0)) (d (n "semver") (r "*") (d #t) (k 0)))) (h "0zvcjh6h8hirpmyqz2sypqbm3d1ba9pakqvymq87yav8csia73g7")))

(define-public crate-conduit-utils-0.5.1 (c (n "conduit-utils") (v "0.5.1") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0qs0i16si3y8l62vjzashcz4pfgpg4q29psahynvqyn8qnv0ivqc")))

(define-public crate-conduit-utils-0.5.2 (c (n "conduit-utils") (v "0.5.2") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0cmhkcgyzar34lbhd89a11hm3rayhacymi300gkwppikpb2v066g")))

(define-public crate-conduit-utils-0.5.3 (c (n "conduit-utils") (v "0.5.3") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1vaj2lmx50n860lzfrxc9lyyivccjl57x7q0fy4dvjj9isc7zcl6")))

(define-public crate-conduit-utils-0.5.4 (c (n "conduit-utils") (v "0.5.4") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0gld7d4r2yx1gjfxbhymha7dkl9bmkp59jbnd2pjla3apwp02v9c")))

(define-public crate-conduit-utils-0.5.5 (c (n "conduit-utils") (v "0.5.5") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "13cza6f3x9cx9j2mfgy029gmjn6bcwwfd59hfai8jrkycp1ynpij")))

(define-public crate-conduit-utils-0.5.6 (c (n "conduit-utils") (v "0.5.6") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1ap4pyffhc1s0zxyl3a88xwf6zdcd3f9abapni2phbhw89bw94c5")))

(define-public crate-conduit-utils-0.6.0 (c (n "conduit-utils") (v "0.6.0") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "04fv8cw5kb39dbsnrwnqy8p470g77zsxxgqwx7mfsnyn0rif4iak")))

(define-public crate-conduit-utils-0.6.1 (c (n "conduit-utils") (v "0.6.1") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1pd6k6wzqi6dpzi4c08mryc5lcj0xqlfxha6ny106r16n1838v98")))

(define-public crate-conduit-utils-0.6.2 (c (n "conduit-utils") (v "0.6.2") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0s22hrzjxxa33y3z3ayvxdd2a9imr2sdyynd5gv59qh5wz7dxg9d")))

(define-public crate-conduit-utils-0.7.0 (c (n "conduit-utils") (v "0.7.0") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "08nign4gn280x4b1lgx0ybrqmy219fdvvpkr8vc41amcg2q39j02")))

(define-public crate-conduit-utils-0.7.1 (c (n "conduit-utils") (v "0.7.1") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0v56jaxbgg6yk44i06cqbb10z6j2v39skgbwhkr8bdxl7bby53b8")))

(define-public crate-conduit-utils-0.7.2 (c (n "conduit-utils") (v "0.7.2") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1n7gh0rxmip09h2ns991by3zy5r2iicvkbhwbs3b1wkfjaaxm5zc")))

(define-public crate-conduit-utils-0.7.3 (c (n "conduit-utils") (v "0.7.3") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1pz0pdlnm12z8n17f417kjcd40qi0y747j03czvycj3b2a0i5gqa")))

(define-public crate-conduit-utils-0.7.4 (c (n "conduit-utils") (v "0.7.4") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1ilcxi9xk5fqmskyxi37vgq9w1xah49r1517kakpjpkw7r0zyl7h")))

(define-public crate-conduit-utils-0.7.5 (c (n "conduit-utils") (v "0.7.5") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1m48yx25hwl9klfydar3r0z22djy0nrmhb6mkw9sgzp3hhd8ryh7")))

(define-public crate-conduit-utils-0.7.6 (c (n "conduit-utils") (v "0.7.6") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "semver") (r "^0.2.0") (d #t) (k 0)))) (h "0nvqzg3n1j9m2wljq50ji7lhl08fyja8gf89ddnrxs8hqm20hp8k")))

(define-public crate-conduit-utils-0.8.0 (c (n "conduit-utils") (v "0.8.0") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "conduit-test") (r "^0.8") (d #t) (k 2)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "1fckpbn9babi8dvmf5nfj5a8figa0sd78vqrj1xdfqm2lqwmfalj")))

