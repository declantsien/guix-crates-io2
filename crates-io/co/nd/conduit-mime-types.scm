(define-module (crates-io co nd conduit-mime-types) #:use-module (crates-io))

(define-public crate-conduit-mime-types-0.5.0 (c (n "conduit-mime-types") (v "0.5.0") (h "1g9y43mjh0gxp2wdhrczcpqa7mixg08ll8m7sdsakmmwqlgq9pav")))

(define-public crate-conduit-mime-types-0.5.1 (c (n "conduit-mime-types") (v "0.5.1") (h "0is9fdg8gfnz9g1655c9c93mqsnrg6wzvsn55w33rnpfcl5xi33i")))

(define-public crate-conduit-mime-types-0.5.2 (c (n "conduit-mime-types") (v "0.5.2") (d (list (d (n "rustc-serialize") (r "^0.1") (d #t) (k 0)))) (h "0b3y3vwml2q7x7bkdrgjh73j7zg80xmrb1cyav6893pz8jl1v53r")))

(define-public crate-conduit-mime-types-0.5.4 (c (n "conduit-mime-types") (v "0.5.4") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "0p8vdwny5bz31ayb3lmrw7i9xfnaa2jh7g7fb3xczanh3gx9b8hh")))

(define-public crate-conduit-mime-types-0.6.0 (c (n "conduit-mime-types") (v "0.6.0") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "1hx1nbdxz7mb93g5x2yv99mjj70hsasii3j2r25rsziizinsldq2")))

(define-public crate-conduit-mime-types-0.7.0 (c (n "conduit-mime-types") (v "0.7.0") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "0nisv8nj29qdmrzz60yfaxvhrdmkcyi83k2ysdjv5qw4qwrgw4zj")))

(define-public crate-conduit-mime-types-0.7.1 (c (n "conduit-mime-types") (v "0.7.1") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "1v1qs0s7il9hcc2c3cf4ckdnbsl346sfszcx5pkjz9sjd4cpcxyi")))

(define-public crate-conduit-mime-types-0.7.2 (c (n "conduit-mime-types") (v "0.7.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0arlx4zincbkvhg7wmahyi4icm7wmig8wah1nq0i13m12afb4yzr")))

(define-public crate-conduit-mime-types-0.7.3 (c (n "conduit-mime-types") (v "0.7.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1f6pidv56bcryh52xqa37wcdch613ijc4s7g4ncq1bw16ljk1jlm")))

(define-public crate-conduit-mime-types-0.8.0 (c (n "conduit-mime-types") (v "0.8.0") (d (list (d (n "phf") (r "^0.10.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0fgrpsqlmx9i5jmfjl7c3lffmr6hcz0xmvwyzz6lcim0hpnsn8i4")))

