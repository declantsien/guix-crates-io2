(define-module (crates-io co nd cond) #:use-module (crates-io))

(define-public crate-cond-1.0.0 (c (n "cond") (v "1.0.0") (h "1837p698kshpyb5xyzn0dlvbm4fqrrym5knk0xkizd7pyn4xx8if")))

(define-public crate-cond-1.0.1 (c (n "cond") (v "1.0.1") (h "0554w32079pjlya13a5x8ywcbmj240yvxx0b6wp8wzp0h19w3la2")))

(define-public crate-cond-1.0.2 (c (n "cond") (v "1.0.2") (h "1iarw8fj037685b1yhs19vx3jz0rnqhs14hxzy4j83r106rf1bsl") (y #t)))

(define-public crate-cond-1.0.3 (c (n "cond") (v "1.0.3") (h "0s4895fifcq4kfsb0v8qw3rqc223zyd3hkd730n7i6yr41yzs43l")))

(define-public crate-cond-1.0.4 (c (n "cond") (v "1.0.4") (h "1qrrjk6whki61n1qk5fjl327gar2w4i86gzarkx9s0q6fxhnm47b")))

(define-public crate-cond-1.0.5 (c (n "cond") (v "1.0.5") (h "0kg6gxl898ypwzm5salxjnv8ab0h7a2psz9sb934h4d69pz1igrz")))

