(define-module (crates-io co nd condow_fs) #:use-module (crates-io))

(define-public crate-condow_fs-0.10.0 (c (n "condow_fs") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "1y8xhzkm0hxc1mq29s8wkfqd5npnj5m3aq5vbh4y16cqlfhp3ah5")))

(define-public crate-condow_fs-0.11.0 (c (n "condow_fs") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "0bs65n0sll4f7nvq4haql6hgp69dy0axkrcrwlh2l1smicwdsnw9")))

(define-public crate-condow_fs-0.11.1 (c (n "condow_fs") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "1n4fnzfaqj5ck3zbbvrhl0ai126llzaj1n5prgx5r0xy6465s6z2")))

(define-public crate-condow_fs-0.11.2 (c (n "condow_fs") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "18vzc2drpwvb0vjk3iv7da0gl8yfblc6aipbb2g3kzay9zwldkrd")))

(define-public crate-condow_fs-0.11.3 (c (n "condow_fs") (v "0.11.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "0x9p4a88xdy659nqb5qagx872cbqywcfqz9fjs8mq2x0fyi1rgkz")))

(define-public crate-condow_fs-0.12.0 (c (n "condow_fs") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "1qs6by6l1l7z73rl53yark469ryasvq9ls0ydsgwd7h3djbfwrjf")))

(define-public crate-condow_fs-0.13.0 (c (n "condow_fs") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "1kbwbli8i2k35vz417qdhwjxsxfvr0zqdamwwwdqzy4gjpi61r28")))

(define-public crate-condow_fs-0.14.0 (c (n "condow_fs") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.13") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "0jdv7h4k8l261zwfnx578hqy91wxdq7xmp369zl2j5ndrf7if8h5")))

(define-public crate-condow_fs-0.15.0 (c (n "condow_fs") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.14") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "0i1lpf5dpp3krdvlwhvkqv302r7aar8gvv282libggw25fffjpyk")))

(define-public crate-condow_fs-0.16.0 (c (n "condow_fs") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.15") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "175hpnyp43iiqxcn3cv3x6bzz3mbbrdsqihljgbvnnzmgb862iwz")))

(define-public crate-condow_fs-0.17.0 (c (n "condow_fs") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.16") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "0z366fdbn3jmngp338b4kzgcmadm55r9l85yb138k3ls8wwy4d3m")))

(define-public crate-condow_fs-0.18.0 (c (n "condow_fs") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.17") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "0yx86brzl1hmhc123dwmxdix0cqf5l1c8f6yhfb1yiwg2z3m9lng")))

(define-public crate-condow_fs-0.18.1 (c (n "condow_fs") (v "0.18.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.17.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "05hh0gfmg3p9vqfy2b0xpnys2vyg8n26dam80z7hlcgkdgaijilr")))

(define-public crate-condow_fs-0.19.0 (c (n "condow_fs") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.18.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "184vs9r2fygilgi78cr7snwlv4ncdpb6r268y5wj04w0ig7291z8")))

(define-public crate-condow_fs-0.20.0-alpha.1 (c (n "condow_fs") (v "0.20.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.19.0-alpha.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "0nlkp9dbivyhsrjcddhd3cm83n2y0b4m2q7w93jyjw6v2qhm9q00")))

(define-public crate-condow_fs-0.20.0-alpha.2 (c (n "condow_fs") (v "0.20.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.19.0-alpha.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "0lrl6iaf1ybrrpkf1v7jliwf09sa499r0fwhvffbglq6mqgdzv76")))

(define-public crate-condow_fs-0.20.0-alpha.3 (c (n "condow_fs") (v "0.20.0-alpha.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "condow_core") (r "^0.19.0-alpha.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)))) (h "1lj02hw981q4sgh97168mc7jacdn7acj5fygsaxnih9iv3zih5mi")))

