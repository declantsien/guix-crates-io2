(define-module (crates-io co nd condvar) #:use-module (crates-io))

(define-public crate-condvar-0.1.0 (c (n "condvar") (v "0.1.0") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1wddxqyiv2f701xm4s9498601av0jc6y7k2jwkcg5xcb63k68aar")))

(define-public crate-condvar-0.1.1 (c (n "condvar") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1iagzimdiz5ag38maqnhbhm52kahj54jyqvw52immwsnyipaan6n")))

