(define-module (crates-io co nd conditional) #:use-module (crates-io))

(define-public crate-conditional-0.1.0 (c (n "conditional") (v "0.1.0") (d (list (d (n "conditional_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0ff7v0jbalwc68hlxpggvf7x9510rx0496m26zaad284r00qwc52")))

(define-public crate-conditional-0.1.1 (c (n "conditional") (v "0.1.1") (d (list (d (n "conditional_impl") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "10hv12hdpv1kkapd408sd7xvp2436fpij7cvi13vn623awgsy04j")))

