(define-module (crates-io co nd conduit-middleware) #:use-module (crates-io))

(define-public crate-conduit-middleware-0.5.0 (c (n "conduit-middleware") (v "0.5.0") (d (list (d (n "conduit") (r "*") (d #t) (k 0)))) (h "101jlgy3h7pk0pq9s77vd7p7xrr463mn64xs86k2cp0nkrjig3p7")))

(define-public crate-conduit-middleware-0.5.1 (c (n "conduit-middleware") (v "0.5.1") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)))) (h "124vcrb41hvpg9v4771l0rp51am629bsmbhidp2kvqawzhpflssq")))

(define-public crate-conduit-middleware-0.5.2 (c (n "conduit-middleware") (v "0.5.2") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)))) (h "1prsgy4ziqr3aza4bk1fznhp3sflp6v879vvj0wbd8n5aby7mpkd")))

(define-public crate-conduit-middleware-0.6.0 (c (n "conduit-middleware") (v "0.6.0") (d (list (d (n "conduit") (r "^0.6.0") (d #t) (k 0)))) (h "0ywdsg53bwrqfkjrarybxzcrbd4qk0ikay59vc68hp9cdj2w8ybf")))

(define-public crate-conduit-middleware-0.6.1 (c (n "conduit-middleware") (v "0.6.1") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1ppbg3vb4l23w0ba0hp7xy3c7r08yhpkrxj4q8xxdmzk4ygf4wd4")))

(define-public crate-conduit-middleware-0.6.2 (c (n "conduit-middleware") (v "0.6.2") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "0wb9mndljnbv4h8ciznlmac2h2f8c74x9ihvcssi81cmkw8cfhi2")))

(define-public crate-conduit-middleware-0.6.3 (c (n "conduit-middleware") (v "0.6.3") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "0jfp457637sz4naf6iqv3bmw9r4rvmjhk32vpj9ddwnl9q110b1l")))

(define-public crate-conduit-middleware-0.7.0 (c (n "conduit-middleware") (v "0.7.0") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1mq52qfsin743xpjqyg4i18jc6fc9c2rkwvld20p9cgymj8x38y0")))

(define-public crate-conduit-middleware-0.7.1 (c (n "conduit-middleware") (v "0.7.1") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1a1v6f9x8gzzr6dhjpjgasj2pzqqvff945wqihknf695xiqcywh3")))

(define-public crate-conduit-middleware-0.7.2 (c (n "conduit-middleware") (v "0.7.2") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "0rlcsr150d5wjcahmg88mgajb46rjq5jbl6g0rh5j77kbx22hh1b")))

(define-public crate-conduit-middleware-0.7.3 (c (n "conduit-middleware") (v "0.7.3") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "06pd7iiykp0dy6vci2zh2aqv9rmakacqf0k451jf22jwr617i3jx")))

(define-public crate-conduit-middleware-0.7.4 (c (n "conduit-middleware") (v "0.7.4") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "semver") (r "^0.2") (d #t) (k 0)))) (h "1wf9dr4k8gm6sagw87fdyvmdqwcdaq6iq2b5304ama7kwl4qz3ml")))

(define-public crate-conduit-middleware-0.8.0 (c (n "conduit-middleware") (v "0.8.0") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "0m6wqyvkvz408wdzifg01vjfmciarifp65528j2zk3dvgmscpmgw")))

(define-public crate-conduit-middleware-0.8.1 (c (n "conduit-middleware") (v "0.8.1") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "1l3lbd5mh6phx70f62g08xsx39kkymf8j3j93696z5qblsivbd0r")))

(define-public crate-conduit-middleware-0.9.0-alpha.0 (c (n "conduit-middleware") (v "0.9.0-alpha.0") (d (list (d (n "conduit") (r "^0.9.0-alpha.0") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "01d0kqx1357lxm9iz7nyigvd0qfq9saq9ahmjz6pbhlx8mcn7k43")))

(define-public crate-conduit-middleware-0.9.0-alpha.1 (c (n "conduit-middleware") (v "0.9.0-alpha.1") (d (list (d (n "conduit") (r "^0.9.0-alpha.1") (d #t) (k 0)))) (h "08r2ca25vx8rc62l7dcb9n5qyrpya0yj862z63qnr63v3djd62w0")))

(define-public crate-conduit-middleware-0.9.0-alpha.2 (c (n "conduit-middleware") (v "0.9.0-alpha.2") (d (list (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.2") (d #t) (k 2)))) (h "017vdaif433hg8ypcd9fq94kqkqd5wzwj11yy6r6g76dc0q1badx")))

(define-public crate-conduit-middleware-0.9.0-alpha.3 (c (n "conduit-middleware") (v "0.9.0-alpha.3") (d (list (d (n "conduit") (r "^0.9.0-alpha.3") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.3") (d #t) (k 2)))) (h "07szdbpb3dpx27kng5ml8fkpqk9h0b62fvd7cxw8b45pkycaw6ni")))

(define-public crate-conduit-middleware-0.9.0-alpha.4 (c (n "conduit-middleware") (v "0.9.0-alpha.4") (d (list (d (n "conduit") (r "^0.9.0-alpha.5") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.4") (d #t) (k 2)))) (h "06wxfva1y31h3vgx8b4gb1k9zwvf9wagn1jb08bwhiwqaw4gpifx")))

(define-public crate-conduit-middleware-0.9.0 (c (n "conduit-middleware") (v "0.9.0") (d (list (d (n "conduit") (r "^0.9.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0") (d #t) (k 2)))) (h "1viypz025ga0z7nh04jp230xnbpl12705aigqw6arh9xqp9faicj")))

(define-public crate-conduit-middleware-0.10.0 (c (n "conduit-middleware") (v "0.10.0") (d (list (d (n "conduit") (r "^0.10.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.10.0") (d #t) (k 2)))) (h "159sgy7ari9bk29rlq092zgn8rfdi11v65dsd6x2d7ybzavczyfd")))

