(define-module (crates-io co nd conduit-cookie) #:use-module (crates-io))

(define-public crate-conduit-cookie-0.1.0 (c (n "conduit-cookie") (v "0.1.0") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "cookie") (r "^0.1.0") (d #t) (k 0)))) (h "1bnw7apmgkjgcyficgj167dihbj49nic3jd76hdsai38pyn5hi89")))

(define-public crate-conduit-cookie-0.1.1 (c (n "conduit-cookie") (v "0.1.1") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "cookie") (r "^0.1.0") (d #t) (k 0)))) (h "0s4pdbcs2wl7yyiin37mndiwjyci1ks60jid7pcz3bqyl99n8l1f")))

(define-public crate-conduit-cookie-0.1.2 (c (n "conduit-cookie") (v "0.1.2") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "cookie") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.1") (d #t) (k 0)))) (h "0l2q7ny35xdgzqf8wk12qbvpccap5lbwcfcs300hbhn3618378xk")))

(define-public crate-conduit-cookie-0.1.3 (c (n "conduit-cookie") (v "0.1.3") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "cookie") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "16gn3bsvrwzh4x8kay1absb14100h485nxhhavdz848nj7sa20k9")))

(define-public crate-conduit-cookie-0.1.4 (c (n "conduit-cookie") (v "0.1.4") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "cookie") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "0mvbghfh99nvib1s4v04l9f2h0v6lqxfqp0diprv0fam152xgdhp")))

(define-public crate-conduit-cookie-0.1.5 (c (n "conduit-cookie") (v "0.1.5") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "cookie") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "0dx2pk3pmfj6v2r4hifrrgf2zmc4v550pdwkyhnq3qcf39vjbsd2")))

(define-public crate-conduit-cookie-0.6.0 (c (n "conduit-cookie") (v "0.6.0") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.6") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6") (d #t) (k 2)) (d (n "cookie") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "1n8gdz3x7s3qdqjs5hh5wfihnsib7bdgncs04rphyzabv8xs489w")))

(define-public crate-conduit-cookie-0.6.1 (c (n "conduit-cookie") (v "0.6.1") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.6") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6") (d #t) (k 2)) (d (n "cookie") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "1z18rrhjifz0zmng0d6r1fx4r25bypw5bzfma6hpgyghhbqk1p9m")))

(define-public crate-conduit-cookie-0.6.2 (c (n "conduit-cookie") (v "0.6.2") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.6") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6") (d #t) (k 2)) (d (n "cookie") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "08dxizh42p8fg241k07xc5sjz3qby08zcdjq9j80aljdrrvqacr0")))

(define-public crate-conduit-cookie-0.7.0 (c (n "conduit-cookie") (v "0.7.0") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "cookie") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "030h1ry6j6llshr69v2159aj1rxzd3dvjn54gnhfqz3mfxsfsyn6")))

(define-public crate-conduit-cookie-0.7.2 (c (n "conduit-cookie") (v "0.7.2") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "cookie") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "09k4v2zfsikdqnymw00aynf4ff813kf1v17j1fv024qp2iayy8w7")))

(define-public crate-conduit-cookie-0.7.3 (c (n "conduit-cookie") (v "0.7.3") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "cookie") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0190dmcnaw7ghdwvbbc0akmc9qj5d3k41g5nkxlc05za20ill7bn")))

(define-public crate-conduit-cookie-0.7.4 (c (n "conduit-cookie") (v "0.7.4") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "cookie") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0xdlwwfsk1wq6sxhf0pxjcf4ijwicmw0ivgfz2m94wkfmw1anrkp")))

(define-public crate-conduit-cookie-0.7.5 (c (n "conduit-cookie") (v "0.7.5") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "cookie") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "08sbf4lhrdjablniqzzmqaggxz0iq9q3gcmnhq4cfiiph0bnw50m")))

(define-public crate-conduit-cookie-0.7.6 (c (n "conduit-cookie") (v "0.7.6") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7") (d #t) (k 2)) (d (n "cookie") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0sqxb11v1krqjjfp0pzmsg80nq9sy5gwlcj4i1gf1k419d5w8wl6")))

(define-public crate-conduit-cookie-0.8.0 (c (n "conduit-cookie") (v "0.8.0") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.8") (d #t) (k 0)) (d (n "conduit-test") (r "^0.8") (d #t) (k 2)) (d (n "cookie") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qfsi2lcm8yms234wypf6r4lz7vmqpnzyfbph4n81zk2v5y51lih")))

(define-public crate-conduit-cookie-0.8.1 (c (n "conduit-cookie") (v "0.8.1") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.8") (d #t) (k 0)) (d (n "conduit-test") (r "^0.8") (d #t) (k 2)) (d (n "cookie") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "02lmysk2336h698pk637glryv7ylj55n6qckwmz040fpl5h52pyi")))

(define-public crate-conduit-cookie-0.8.2 (c (n "conduit-cookie") (v "0.8.2") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.8") (d #t) (k 0)) (d (n "conduit-test") (r "^0.8") (d #t) (k 2)) (d (n "cookie") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ir2dag546vdmm92y0dpfvprrjbgc0gca4jsjdg2f1ihvdpbfnq3")))

(define-public crate-conduit-cookie-0.8.3 (c (n "conduit-cookie") (v "0.8.3") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.8") (d #t) (k 0)) (d (n "conduit-test") (r "^0.8") (d #t) (k 2)) (d (n "cookie") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cia2vr214r6p6wyj88y4x6kb23dzzbc470f61kwnkh99nc4cyv4")))

(define-public crate-conduit-cookie-0.8.4 (c (n "conduit-cookie") (v "0.8.4") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.8") (d #t) (k 0)) (d (n "conduit-test") (r "^0.8") (d #t) (k 2)) (d (n "cookie") (r "^0.9") (f (quote ("secure"))) (d #t) (k 0)))) (h "0dby3xw41x4djbxz5nafh80h858a93shlir2m0zrifyzxxmw3l1f")))

(define-public crate-conduit-cookie-0.8.5 (c (n "conduit-cookie") (v "0.8.5") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.8") (d #t) (k 0)) (d (n "conduit-test") (r "^0.8") (d #t) (k 2)) (d (n "cookie") (r "^0.11") (f (quote ("secure"))) (d #t) (k 0)))) (h "0q8kr46wq5rx99zk0ngqk308skgs9j9s24x8fjqdcn24l4cgikhp")))

(define-public crate-conduit-cookie-0.9.0-alpha.0 (c (n "conduit-cookie") (v "0.9.0-alpha.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0-alpha.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.0") (d #t) (k 2)) (d (n "cookie") (r "^0.12") (f (quote ("secure"))) (d #t) (k 0)))) (h "1bsisfd8r7x3ybd6iyslyljry0z2xiwfccr410pnp3dhacvainwc")))

(define-public crate-conduit-cookie-0.9.0-alpha.1 (c (n "conduit-cookie") (v "0.9.0-alpha.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.1") (d #t) (k 2)) (d (n "cookie") (r "^0.12") (f (quote ("secure"))) (d #t) (k 0)))) (h "0ni6khhbasvwj879bbkdjdn62bmgagisfabvwxyaxnrzkgxf4ibn")))

(define-public crate-conduit-cookie-0.9.0-alpha.2 (c (n "conduit-cookie") (v "0.9.0-alpha.2") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.2") (d #t) (k 2)) (d (n "cookie") (r "^0.12") (f (quote ("secure"))) (d #t) (k 0)))) (h "07bi4z5p4g9pj1fhlw6qyjq0psg5da214swlxhb9ziq62g02hsgv")))

(define-public crate-conduit-cookie-0.9.0-alpha.3 (c (n "conduit-cookie") (v "0.9.0-alpha.3") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.2") (d #t) (k 2)) (d (n "cookie") (r "^0.13") (f (quote ("secure"))) (d #t) (k 0)) (d (n "time") (r "^0.2") (k 0)))) (h "01xnndhplgx4bd34ifjjff3xcd7vjql1ilkif9qkw27rnzs31i8g")))

(define-public crate-conduit-cookie-0.9.0-alpha.4 (c (n "conduit-cookie") (v "0.9.0-alpha.4") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.2") (d #t) (k 2)) (d (n "cookie") (r "^0.14") (f (quote ("secure"))) (d #t) (k 0)) (d (n "time") (r "^0.2") (k 0)))) (h "0s4x14vlpfwkzmcwya59ba9q9df4ir51rywwng5xpl941srxw0xf")))

(define-public crate-conduit-cookie-0.9.0-alpha.5 (c (n "conduit-cookie") (v "0.9.0-alpha.5") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0-alpha.3") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.2") (d #t) (k 2)) (d (n "cookie") (r "^0.15") (f (quote ("secure"))) (d #t) (k 0)) (d (n "time") (r "^0.2") (k 0)))) (h "0jvzf8lq8sq88fb26fbdjjy63i7dzxgxcl2dvh3253vi0hkxgw3d")))

(define-public crate-conduit-cookie-0.10.0 (c (n "conduit-cookie") (v "0.10.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "conduit") (r "^0.10.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.10.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.10.0") (d #t) (k 2)) (d (n "cookie") (r "^0.16.0-rc.1") (f (quote ("secure"))) (d #t) (k 0)))) (h "1wapyk8v4k74ic225ln9fyz91chnmp1zwjr40bm0nh8crkvai21g") (r "1.51.0")))

