(define-module (crates-io co nd conditional_impl) #:use-module (crates-io))

(define-public crate-conditional_impl-0.1.0 (c (n "conditional_impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "105fvlw21mx3fr3zfqa8q6z0n340zqic0i0p4mf8p74xx4xhjw0x")))

(define-public crate-conditional_impl-0.1.1 (c (n "conditional_impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rnrlp0ssxrmslk43r1xrgjna8caw99cn48qgcwagam4pi1gnbn9")))

