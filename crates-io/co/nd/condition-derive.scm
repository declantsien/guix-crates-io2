(define-module (crates-io co nd condition-derive) #:use-module (crates-io))

(define-public crate-condition-derive-0.1.0 (c (n "condition-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "0a61wqfk37nh9inhy3mzghd30zx0j18xlwrc6b2sxpry62xz4qsh")))

