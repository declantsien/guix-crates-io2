(define-module (crates-io co nd conduit-router) #:use-module (crates-io))

(define-public crate-conduit-router-0.5.0 (c (n "conduit-router") (v "0.5.0") (d (list (d (n "conduit") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)))) (h "0y5zzs3rw34mic3g159iwp2pmlhnfpvpkqcmm478i40b9k36s3ri")))

(define-public crate-conduit-router-0.5.1 (c (n "conduit-router") (v "0.5.1") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)))) (h "1x9wfjxyddjl17p8qjdgg88j9jbgffhn2aqrn0g59d9imm1b300x")))

(define-public crate-conduit-router-0.5.2 (c (n "conduit-router") (v "0.5.2") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)))) (h "0g5d0s5yal00q26way2vibmljj2nf4n1jf28jnfz70cgz7sp7pir")))

(define-public crate-conduit-router-0.5.3 (c (n "conduit-router") (v "0.5.3") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)))) (h "1qycfcd32f0iphffxigg4h45nfmav48s32fnbpf9ypixavmg321f")))

(define-public crate-conduit-router-0.5.4 (c (n "conduit-router") (v "0.5.4") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)))) (h "1vza0bnglczr22xifzg85hzswi53x11fb5nm6ni1qbcblsylwcvs")))

(define-public crate-conduit-router-0.6.0 (c (n "conduit-router") (v "0.6.0") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)))) (h "0lpa3wl5kqghj5qyc0a68h1vx5ss5kzd5hcb2sghxsmir2152lb8")))

(define-public crate-conduit-router-0.6.1 (c (n "conduit-router") (v "0.6.1") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "09d62jqiias9qf9wj6zwvqdls2bci1zzzar6vkrz7305g63ps2bd")))

(define-public crate-conduit-router-0.6.2 (c (n "conduit-router") (v "0.6.2") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "01pzsg5amfpllargcn5yp8c8yrib99ckd6w0cwji5vzbwsnsfr4a")))

(define-public crate-conduit-router-0.6.3 (c (n "conduit-router") (v "0.6.3") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "0k2rqvb3hgjj9hrdfvyv2p41dpkbbn7k5hwky662fxz63acrlyya")))

(define-public crate-conduit-router-0.6.4 (c (n "conduit-router") (v "0.6.4") (d (list (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1ifv027ljjdxnymiyjpji8ssnl2vx3df8cbn18jkd3w99n42hvqz")))

(define-public crate-conduit-router-0.7.0 (c (n "conduit-router") (v "0.7.0") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1mxrzq39vhd5svb1m96ypdqc9vvzi4ygwp2cpv6mwk4sq0ih4afn")))

(define-public crate-conduit-router-0.7.1 (c (n "conduit-router") (v "0.7.1") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1nqv3isxly00y51rlkn5k2j38xhjj26sl0kcpp8b29k7yn60j42y")))

(define-public crate-conduit-router-0.7.2 (c (n "conduit-router") (v "0.7.2") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.1") (d #t) (k 0)))) (h "1k2z9f1i9vqiimzmr2hann6gdj6xk5n76rgfpgp6c23sdva6np0r")))

(define-public crate-conduit-router-0.7.3 (c (n "conduit-router") (v "0.7.3") (d (list (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.2") (d #t) (k 0)))) (h "04q78wgibwbp7sp550mpjxvirvbqrcywjvlhg02lsmjkdlg6nisg")))

(define-public crate-conduit-router-0.8.0 (c (n "conduit-router") (v "0.8.0") (d (list (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "1v632hbknii33mnzpk3vgvd7abcpa830fdgzsj5dl6n4r38d42m7")))

(define-public crate-conduit-router-0.9.0-alpha.0 (c (n "conduit-router") (v "0.9.0-alpha.0") (d (list (d (n "conduit") (r "^0.9.0-alpha.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "11sj3n58n7a7rzvicshn80r536a1qmpzxhn6jnbwjgkdqfr1rgkw")))

(define-public crate-conduit-router-0.9.0-alpha.1 (c (n "conduit-router") (v "0.9.0-alpha.1") (d (list (d (n "conduit") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)))) (h "1j45zmkiq6xlm3slx0528884zn1w69f96wh54f72isswjn7gvxbr")))

(define-public crate-conduit-router-0.9.0-alpha.2 (c (n "conduit-router") (v "0.9.0-alpha.2") (d (list (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.2") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)))) (h "1v351wwckyrbfq0j4yniax76g9sz9376spsipzi63z1107m6iakf")))

(define-public crate-conduit-router-0.9.0-alpha.3 (c (n "conduit-router") (v "0.9.0-alpha.3") (d (list (d (n "conduit") (r "^0.9.0-alpha.3") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.3") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)))) (h "0ph3cdz606zs8zzzcbmspwx7xf5wnvkncnhm37gw1a5j22cmxmad")))

(define-public crate-conduit-router-0.9.0-alpha.4 (c (n "conduit-router") (v "0.9.0-alpha.4") (d (list (d (n "conduit") (r "^0.9.0-alpha.3") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.3") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 0)))) (h "1zgs1ga2ywqmpdcywh1i4s5wd15acv2yp5j5bmmhsjbi9p86m8hz")))

(define-public crate-conduit-router-0.9.0-alpha.5 (c (n "conduit-router") (v "0.9.0-alpha.5") (d (list (d (n "conduit") (r "^0.9.0-alpha.5") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.4") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.2") (d #t) (k 0)))) (h "02z1x6vbqalbldhbjy64w6vb7gvgxfx922bz1xx1ryz1n0agzfz1")))

(define-public crate-conduit-router-0.9.0-alpha.6 (c (n "conduit-router") (v "0.9.0-alpha.6") (d (list (d (n "conduit") (r "^0.9.0-alpha.5") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.4") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.3") (d #t) (k 0)))) (h "0wq1lv5vg7zjzrdnwm6pkxz5q6q1zcng57qkcs4a24krybagyyn1")))

(define-public crate-conduit-router-0.9.0-alpha.7 (c (n "conduit-router") (v "0.9.0-alpha.7") (d (list (d (n "conduit") (r "^0.9.0-alpha.5") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.25") (d #t) (k 2)))) (h "0i6h30ddbwl00h1vvx57mpljqahzvc582a3ywbc2dbbfksazvnzp")))

(define-public crate-conduit-router-0.9.0 (c (n "conduit-router") (v "0.9.0") (d (list (d (n "conduit") (r "^0.9.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "01px8xqmsxy13rz1ppd5d66ywpdwzk5g60ahm4balxnf4hshsnv1")))

(define-public crate-conduit-router-0.10.0 (c (n "conduit-router") (v "0.10.0") (d (list (d (n "conduit") (r "^0.10.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.10.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "route-recognizer") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0dgmzr0hyb5ljsh9ry5380g866vrcfwib4v5ilzyriskn6a17kqk")))

