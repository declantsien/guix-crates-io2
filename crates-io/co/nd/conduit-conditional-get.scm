(define-module (crates-io co nd conduit-conditional-get) #:use-module (crates-io))

(define-public crate-conduit-conditional-get-0.5.0 (c (n "conduit-conditional-get") (v "0.5.0") (d (list (d (n "conduit") (r "*") (d #t) (k 0)) (d (n "conduit-middleware") (r "*") (d #t) (k 0)) (d (n "conduit-test") (r "*") (d #t) (k 0)))) (h "0g8rwzsmyaz86walxh6rrsi8gcz0hayp3dj7azxg55i3762yjq9r")))

(define-public crate-conduit-conditional-get-0.5.1 (c (n "conduit-conditional-get") (v "0.5.1") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0flr8b3nblacjwfpq79rk4nmpal6mvyw10bhg7b76rpp3bsr3f0j")))

(define-public crate-conduit-conditional-get-0.5.2 (c (n "conduit-conditional-get") (v "0.5.2") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0nqy9y3iiwm9ljgc7gk3vnrmz9k776b8ky79cbii4685m9a3plr9")))

(define-public crate-conduit-conditional-get-0.5.3 (c (n "conduit-conditional-get") (v "0.5.3") (d (list (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.5.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.5.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0ljrzcg2nlr90jw29i86wz641n8hs4ilarjl8kpnysgqfqq59dq3")))

(define-public crate-conduit-conditional-get-0.6.0 (c (n "conduit-conditional-get") (v "0.6.0") (d (list (d (n "conduit") (r "^0.6.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.6.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "03f6f807lk93p6prcf3lc6whzm6y5vg6xcmixyg4nxxrzc79y10z")))

(define-public crate-conduit-conditional-get-0.6.1 (c (n "conduit-conditional-get") (v "0.6.1") (d (list (d (n "conduit") (r "^0.6.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.6.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "028vwv7d04hp8mw51lkysql5bsn2ki3vg1nqw5da507y5l077a8b")))

(define-public crate-conduit-conditional-get-0.6.2 (c (n "conduit-conditional-get") (v "0.6.2") (d (list (d (n "conduit") (r "^0.6.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.6.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.6.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0r0pidagcwi035mpvd43yl1ls9rvh3yibw8ig0iz3av2hbwz87np")))

(define-public crate-conduit-conditional-get-0.7.0 (c (n "conduit-conditional-get") (v "0.7.0") (d (list (d (n "conduit") (r "^0.7.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0dj0fr1hwbb5bjal2mwn1sj7h9zal84sbqafz7rpmvy3dagxp5y2")))

(define-public crate-conduit-conditional-get-0.7.1 (c (n "conduit-conditional-get") (v "0.7.1") (d (list (d (n "conduit") (r "^0.7.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1j971ldddaz1yn22c8vzi9jni9b6kca6r4s038nx8chw2ygac5in")))

(define-public crate-conduit-conditional-get-0.7.2 (c (n "conduit-conditional-get") (v "0.7.2") (d (list (d (n "conduit") (r "^0.7.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.7.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.7.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "11a0293bwfr5zssavjz0d3iszaacijdhlf5a5lghrd3hawy8g6p8")))

(define-public crate-conduit-conditional-get-0.8.0 (c (n "conduit-conditional-get") (v "0.8.0") (d (list (d (n "conduit") (r "^0.8.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.8.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.8.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1mn6gw0dwbr975jyp299yp3g2n7k0dr0c4wzzfwd0zs37q46fkv1")))

(define-public crate-conduit-conditional-get-0.9.0-alpha.0 (c (n "conduit-conditional-get") (v "0.9.0-alpha.0") (d (list (d (n "conduit") (r "^0.9.0-alpha.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0-alpha.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.0") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0dh12gc4jfk3zcimpy3krifsnyq5qh5jcxjsr2gnydl7v430n7ib")))

(define-public crate-conduit-conditional-get-0.9.0-alpha.1 (c (n "conduit-conditional-get") (v "0.9.0-alpha.1") (d (list (d (n "conduit") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.1") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "0h97g91dyzk1q37ql7f8m1s6rlbwvqw2fdmk52r28nw76cr6ys4s")))

(define-public crate-conduit-conditional-get-0.9.0-alpha.2 (c (n "conduit-conditional-get") (v "0.9.0-alpha.2") (d (list (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.2") (d #t) (k 2)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1a759p9n3n6x65x8bhc4idh8fw1g79h15skb2yzf2j7pkavh373n")))

(define-public crate-conduit-conditional-get-0.9.0-alpha.3 (c (n "conduit-conditional-get") (v "0.9.0-alpha.3") (d (list (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0-alpha.2") (d #t) (k 2)) (d (n "time") (r "^0.2") (f (quote ("std"))) (k 0)))) (h "1ykdkfrs3xwc1mjf8hn1disisx6ilgvzbd8xxa0y2bw3q04wqx4n")))

(define-public crate-conduit-conditional-get-0.9.0 (c (n "conduit-conditional-get") (v "0.9.0") (d (list (d (n "conduit") (r "^0.9.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.9.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.9.0") (d #t) (k 2)) (d (n "time") (r "^0.2") (f (quote ("std"))) (k 0)))) (h "19k2x4nvma6x9h8n1m73jk3ms35cqn9ij095ylq8j05dqwvwfsc0")))

(define-public crate-conduit-conditional-get-0.10.0 (c (n "conduit-conditional-get") (v "0.10.0") (d (list (d (n "conduit") (r "^0.10.0") (d #t) (k 0)) (d (n "conduit-middleware") (r "^0.10.0") (d #t) (k 0)) (d (n "conduit-test") (r "^0.10.0") (d #t) (k 2)) (d (n "time") (r "^0.2") (f (quote ("std"))) (k 0)))) (h "1b8m4sv76vhwr8x4rdmcla193dbb23y1frghyk0z8avc8xcvz8q3")))

