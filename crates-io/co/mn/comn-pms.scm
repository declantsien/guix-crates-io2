(define-module (crates-io co mn comn-pms) #:use-module (crates-io))

(define-public crate-comn-pms-0.1.0 (c (n "comn-pms") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0g0ip9yz7gc2h7vla5wqms6926chh49lmmpyk8mkw7dwhfyx98yg")))

