(define-module (crates-io co rs corsairmi) #:use-module (crates-io))

(define-public crate-corsairmi-0.1.0 (c (n "corsairmi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1i8yl8d9y6rm0a5aixmzaz8pm3nswa1dnynjp95k207fqk1fqkhf")))

(define-public crate-corsairmi-0.2.0 (c (n "corsairmi") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03pdc500y6mxr79kykv4s4qj1y2i7xkkv7qx81wp5mgdjnlpvzn5")))

(define-public crate-corsairmi-0.3.0 (c (n "corsairmi") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0fbir2fid363v5v4wv0qyhg1znj7rgglxzb8a69kcanjshpc8m6q")))

(define-public crate-corsairmi-0.3.1 (c (n "corsairmi") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1l308gs8adkamc8s7yyxh76nz4cvla1gx8pm56ldwannfhwvqx0x")))

(define-public crate-corsairmi-0.3.2 (c (n "corsairmi") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0i1yh8gbh330bk6cnwp7cc6paxigirbjpaa0akssgpfl52mvkl8a")))

(define-public crate-corsairmi-0.4.0 (c (n "corsairmi") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1gzz2d0vrzzrsrrppzjkj3l67xc19gcrdgd9j5xh9l0ahgvx97y4")))

(define-public crate-corsairmi-1.0.0 (c (n "corsairmi") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1gsfdkh9qa7lq3y9hbhp64by7a6q1radid8mqnlh9pg5mrldy8nv") (r "1.56")))

(define-public crate-corsairmi-2.0.0 (c (n "corsairmi") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "14ygiwb2n0npn6ax42fmxcaw4mqbb9qca6cga9np5ipp6szxm116") (r "1.56")))

