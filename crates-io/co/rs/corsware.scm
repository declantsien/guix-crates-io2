(define-module (crates-io co rs corsware) #:use-module (crates-io))

(define-public crate-corsware-0.1.0 (c (n "corsware") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "mount") (r "^0.3.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "unicase") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "0k39i37x916a88jqhrvaiqvsa97lq1sfqx3hrmq6c3sqqj7lqwa3")))

(define-public crate-corsware-0.1.1 (c (n "corsware") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "mount") (r "^0.3.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "unicase") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "0vf4z5xrvsqjskmg159yh2syb4lz25sjialdqp8vhwrkyxxin2w5")))

(define-public crate-corsware-0.1.2 (c (n "corsware") (v "0.1.2") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "mount") (r "^0.3.0") (d #t) (k 0)) (d (n "mount") (r "^0.3.0") (d #t) (k 2)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "unicase") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "10j19g41a8x9a856fi0rvphxfkpnp5mfh6rdfqx5cq23vqadcsqc")))

(define-public crate-corsware-0.1.3 (c (n "corsware") (v "0.1.3") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "mount") (r "^0.3.0") (d #t) (k 0)) (d (n "mount") (r "^0.3.0") (d #t) (k 2)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "unicase") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "1ksz7n2w915r5vgzmjwqdvkks5cnvgzg7pbjlipv4asjd5fvfc3r")))

(define-public crate-corsware-0.1.4 (c (n "corsware") (v "0.1.4") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "mount") (r "^0.3.0") (d #t) (k 2)) (d (n "router") (r "^0.5.1") (d #t) (k 2)) (d (n "unicase") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "0fhj3i4di7zpqcxk7sxhl1qv6g3j1apx7pfq0khblbwz7kjlpnzv")))

(define-public crate-corsware-0.1.5 (c (n "corsware") (v "0.1.5") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "mount") (r "^0.3.0") (d #t) (k 2)) (d (n "router") (r "^0.5.1") (d #t) (k 2)) (d (n "unicase") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "07lgnkrhv8z7d3jcg5xfxga6ywdz4jh7lbygii8k9d7ravf6878g")))

(define-public crate-corsware-0.2.0 (c (n "corsware") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "mount") (r "^0.4.0") (d #t) (k 2)) (d (n "router") (r "^0.6.0") (d #t) (k 2)) (d (n "unicase") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "0dw9ird5i9wa4z3dy24q3zhgzg147mnzmis3rn506n4r2kg7kwjw")))

