(define-module (crates-io co rs corstone300-pac) #:use-module (crates-io))

(define-public crate-corstone300-pac-0.2.0 (c (n "corstone300-pac") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1d4w63lklg1nz1s9bjijyf6h53453pxdjnfp51a2z6203h2kfavx") (f (quote (("rt" "cortex-m-rt/device"))))))

