(define-module (crates-io co na conan-build) #:use-module (crates-io))

(define-public crate-conan-build-0.1.0 (c (n "conan-build") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mclhzhxfw8b7ngw8z0rxqn8m6234mwzv36395dsgyfw7jnv1y7g")))

(define-public crate-conan-build-0.1.1 (c (n "conan-build") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y36xpikmhs6a04l92nc00sfxcwh4xvv2bsb8kfmqfzwdpnnb3cs")))

(define-public crate-conan-build-0.1.2 (c (n "conan-build") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gqwdzxcac2a7hg842vq7xqs42wqd0gs920y145fv9q963s2vp03")))

(define-public crate-conan-build-0.1.3 (c (n "conan-build") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cnmzdskc7pa9xnxspg2fkc5lvn8shqch39n76gxlan2iw32mcil")))

(define-public crate-conan-build-0.1.4 (c (n "conan-build") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rp6c7fh7v5dyblcl06llpmlsbkljh0dmv164dfna160mrazvw8p")))

