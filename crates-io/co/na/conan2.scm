(define-module (crates-io co na conan2) #:use-module (crates-io))

(define-public crate-conan2-0.1.0 (c (n "conan2") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vj0h8a9r1wywbgc2dip1dh7rh75pl87hmdf0z3aib5p1fyyl11v")))

(define-public crate-conan2-0.1.1 (c (n "conan2") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16hhkfzx9qjwr71qzk7887dyq086qjzr279pb92v8a6idyb5v1mq")))

