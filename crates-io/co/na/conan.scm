(define-module (crates-io co na conan) #:use-module (crates-io))

(define-public crate-conan-0.1.0 (c (n "conan") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^2.0") (d #t) (k 0)))) (h "17ra3fh5wmx551814f2lrf0dn2l8ba35a29q8y9rml72qywd4xwx")))

(define-public crate-conan-0.1.1 (c (n "conan") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^2.0") (d #t) (k 0)))) (h "120dbvmfa2sg99j1f8a9bdbz2l857ikfl95aql3ni4billi64lg6")))

(define-public crate-conan-0.1.2 (c (n "conan") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^2.0") (d #t) (k 0)))) (h "0c46yvk3c3x2lg6rpi4x8sqi7a2kyx264f1xs89a26dj22kcqinv")))

(define-public crate-conan-0.1.3 (c (n "conan") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^3.0") (k 0)))) (h "07j9wslhnlff3r3pnfqkzm82cz7c3lx9qwc2qa5q9zz8m45hl8mk")))

(define-public crate-conan-0.2.0 (c (n "conan") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^3.0") (k 0)))) (h "0xn3sqp1z9pb44c01nbsql5ha2jaxy1sf1f7fpika4k0l8amga2h")))

(define-public crate-conan-0.3.0 (c (n "conan") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^3.0") (k 0)))) (h "0mrgklx098li72fj5q6adm1n613zj3yhwlbdkg1kkfyqipf70zig")))

(define-public crate-conan-0.4.1 (c (n "conan") (v "0.4.1") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "which") (r "^3.0") (k 0)))) (h "0jxl4qca0apb6jz9siny99dna27j23j6ka68dc4v67p5jnb0arim")))

(define-public crate-conan-0.4.2 (c (n "conan") (v "0.4.2") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "which") (r "^3.0") (k 0)))) (h "1ymb0vhimmikqx63d9k9w7wqw60c0pxp0kvf2j9011kiwjgdi2lr")))

