(define-module (crates-io co na conan-export-recipes) #:use-module (crates-io))

(define-public crate-conan-export-recipes-0.1.0 (c (n "conan-export-recipes") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0rig5kcwpjfi0vmijh86mvafyn668i4v7qba2xfiyp05ga5hk01l")))

