(define-module (crates-io co he coherence) #:use-module (crates-io))

(define-public crate-coherence-0.1.0 (c (n "coherence") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros"))) (d #t) (k 0)))) (h "0c3zh83xl5s8nv2wqp19714z6x7awk8gc077x3bcl6i422s0b9zh") (y #t)))

(define-public crate-coherence-0.1.1 (c (n "coherence") (v "0.1.1") (h "0r97bpinqdsqljrslh3i6arlrfgcw0cdjvnl38lw4vx26lgyx5yw") (y #t)))

