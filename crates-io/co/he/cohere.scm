(define-module (crates-io co he cohere) #:use-module (crates-io))

(define-public crate-cohere-0.1.0 (c (n "cohere") (v "0.1.0") (h "0jc0hhq6qfawaj4a4c73963x364klw71bb6bxd6p828c65l78kn8") (y #t)))

(define-public crate-cohere-0.1.1 (c (n "cohere") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1fwskdxaqvdi7gda54kj7s19s1hipq90h6p0551865arw2bdp3ih") (y #t)))

