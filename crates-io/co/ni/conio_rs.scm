(define-module (crates-io co ni conio_rs) #:use-module (crates-io))

(define-public crate-conio_rs-0.1.0 (c (n "conio_rs") (v "0.1.0") (h "1a12sz7p0c94mg8kqqp43h7c6nazil7cvnjc0a5zi34l92ay5jv4")))

(define-public crate-conio_rs-0.1.1 (c (n "conio_rs") (v "0.1.1") (h "1ar1mpmb9nzcjy3zbvp92rd553ks56b0jwrxc95lfzrkicz5rl2m")))

(define-public crate-conio_rs-0.1.2 (c (n "conio_rs") (v "0.1.2") (h "0lwvbajydr8dy109j9wz9r80d8zy5s3ymrpqbgd51iyf5gwxn17z")))

