(define-module (crates-io co ni conio) #:use-module (crates-io))

(define-public crate-conio-0.1.0 (c (n "conio") (v "0.1.0") (h "0s4snzqy9x9wi53l0f20g26c0nqh7m8jbmpi8pxz2icmy1l7g4vi")))

(define-public crate-conio-0.1.1 (c (n "conio") (v "0.1.1") (h "17h7i8ja7fqgcd8zvqr3cysd4f9sm23sl3p5i64j91d2kwxx0sxj")))

(define-public crate-conio-0.1.2 (c (n "conio") (v "0.1.2") (h "0wrwxsc9vxs1y9q4p5831ydmycf9xc1pski6zaclrpi5hyw55x9s")))

