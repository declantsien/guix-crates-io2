(define-module (crates-io co ni conifer) #:use-module (crates-io))

(define-public crate-conifer-0.0.0 (c (n "conifer") (v "0.0.0") (d (list (d (n "framebuffer") (r "^0.1") (d #t) (k 0)))) (h "0hyx531hz19bk5ma0biri9r1ql5v7wd50ak66hpvqsngk82i2cch")))

(define-public crate-conifer-0.1.0 (c (n "conifer") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "evdev") (r "^0.10") (d #t) (k 0)) (d (n "flume") (r "^0.7") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "1diillqjbrsh4cc0ga91bhkds5yv72qqyz7r4pc3l0hwvanphz9c")))

