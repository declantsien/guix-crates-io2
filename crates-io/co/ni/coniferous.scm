(define-module (crates-io co ni coniferous) #:use-module (crates-io))

(define-public crate-coniferous-0.1.0 (c (n "coniferous") (v "0.1.0") (h "07wp7d0wan0i8rpihp442wir66bnvq308nz26g9frzy7rdp7vp1v")))

(define-public crate-coniferous-0.1.1 (c (n "coniferous") (v "0.1.1") (d (list (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1q0rr07cfc7xfdwg2g9nc0nkr0cdkcw2cma663gd38hyq78vr7ba")))

(define-public crate-coniferous-0.2.0 (c (n "coniferous") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1fkyyzcq6rg2nw98fp2lzb1xqmmabnfj02q9sfva2364vsmzqf46")))

(define-public crate-coniferous-0.2.1 (c (n "coniferous") (v "0.2.1") (d (list (d (n "parsnip") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0az6m79iaiz2j1ls2wqa66di7xgahgz2szfmmwp10q0dw9z29x86")))

