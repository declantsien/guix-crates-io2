(define-module (crates-io co wb cowblock) #:use-module (crates-io))

(define-public crate-cowblock-1.0.0 (c (n "cowblock") (v "1.0.0") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "fuser") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1qkww0lb5m1m6df30r7z5is04hwhgwv48bndnryim6djap4ii53a") (r "1.60")))

(define-public crate-cowblock-2.0.0 (c (n "cowblock") (v "2.0.0") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "fuser") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1phylxnsmb8lj2x1zwzf8srj3pd8s7zcq947yz7m08ljycy18c3w") (r "1.60")))

