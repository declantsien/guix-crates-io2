(define-module (crates-io co yo coyoneda) #:use-module (crates-io))

(define-public crate-coyoneda-0.1.0 (c (n "coyoneda") (v "0.1.0") (h "103c3vc2q4zq7iqw03h3pqcz44qc3bw6pr4fd7npc2i6ylwiii60")))

(define-public crate-coyoneda-0.2.0 (c (n "coyoneda") (v "0.2.0") (h "0gxy6xmzm99lslmfqz0vmxdjh69f24n5qsjwvk6vkvr704qpfqzm")))

(define-public crate-coyoneda-0.3.0 (c (n "coyoneda") (v "0.3.0") (h "1b3z22k3b70kfafankmvh6z9y4was9zm2cba091c6z86shcmz57h")))

(define-public crate-coyoneda-0.3.1 (c (n "coyoneda") (v "0.3.1") (h "1yriciffas82kpxjyr1d8pp538vf9xsqiwjbgzqf28jm34kb4y7f")))

(define-public crate-coyoneda-0.4.0 (c (n "coyoneda") (v "0.4.0") (h "1rdvqgxqxlh95yskcm4fm3kgmhwz70mnidd3wqdyv6fhrs2vsjds")))

(define-public crate-coyoneda-0.4.1 (c (n "coyoneda") (v "0.4.1") (h "1qc2y1nfxprpq3wizgs98i44fiwh4fjsp6kdnnh3lp6gs3wzw1kz")))

(define-public crate-coyoneda-0.5.0 (c (n "coyoneda") (v "0.5.0") (d (list (d (n "functor") (r "^0.1.1") (d #t) (k 0)))) (h "0sz73xyfa5i196xkycxigz3jsl936bvbji55aq0gqr9l4clpjwix")))

(define-public crate-coyoneda-0.5.1 (c (n "coyoneda") (v "0.5.1") (d (list (d (n "functor") (r "^0.1.1") (d #t) (k 0)) (d (n "morphism") (r "^0.4.0") (d #t) (k 0)))) (h "01za86qyl8fm9r0zax051m3v5klfn5kh4rjpw8pn5lrz5a3rank7")))

(define-public crate-coyoneda-0.5.2 (c (n "coyoneda") (v "0.5.2") (d (list (d (n "functor") (r "^0.1.1") (d #t) (k 0)) (d (n "morphism") (r "^0.4.0") (d #t) (k 0)))) (h "0b6h842gzv10l3v6c1s0aixsiy8lx2bz3aj7h79ry52gscggfhv5")))

