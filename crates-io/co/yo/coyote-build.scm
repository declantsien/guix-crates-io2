(define-module (crates-io co yo coyote-build) #:use-module (crates-io))

(define-public crate-coyote-build-0.1.0 (c (n "coyote-build") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "shlex") (r "^1.3.0") (d #t) (k 0)))) (h "0qp3kczhn105jcyg63dq9wb59x0qzirxsihy1y8924gqx9a4si6x")))

