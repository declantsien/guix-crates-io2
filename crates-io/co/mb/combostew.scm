(define-module (crates-io co mb combostew) #:use-module (crates-io))

(define-public crate-combostew-0.1.0 (c (n "combostew") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "1pgv9v5f6y8496nfbyqj2f74p7g2dj11aqnbmd293nn80s58v4ac") (f (quote (("output-test-images"))))))

(define-public crate-combostew-0.1.1 (c (n "combostew") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "04p6dcmpxpycx26ad1921fbvxvbpw13rlqlcqxxi7h3h4f830jmh") (f (quote (("output-test-images"))))))

(define-public crate-combostew-0.1.2 (c (n "combostew") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "0h19qvjvz5pgj548239s6cy7jvk0pa16rw3zm0y9qp617qfgabqw") (f (quote (("output-test-images"))))))

(define-public crate-combostew-0.2.0 (c (n "combostew") (v "0.2.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "09q73l7v07xfmfr1n6ysigsrgbdvhwx2f2ck44bj7rf3vrxqk2dw") (f (quote (("output-test-images"))))))

(define-public crate-combostew-0.3.0 (c (n "combostew") (v "0.3.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "1mbayfqbask8334sav9ak01wiysis2k98fha0r44k3qc76k8vpi3") (f (quote (("output-test-images"))))))

