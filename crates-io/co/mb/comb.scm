(define-module (crates-io co mb comb) #:use-module (crates-io))

(define-public crate-comb-0.1.1 (c (n "comb") (v "0.1.1") (h "1vyq8ky7sizzifl3j5kwcc76kg182r3iir1qfr797f4hld8nwl8q") (y #t)))

(define-public crate-comb-0.1.0 (c (n "comb") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.3") (d #t) (k 0)) (d (n "quit") (r "^1.1.4") (d #t) (k 0)))) (h "0lvk38pirk6makckicvnr16ygnyz4f59nc6phyfdr8rmdn4n03fg")))

(define-public crate-comb-0.2.0 (c (n "comb") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.3") (d #t) (k 0)) (d (n "quit") (r "^1.1.4") (d #t) (k 0)))) (h "1j8yrps5x3sr051nw1ww4rjcmmbb1ad0naw4vziz6qc1iksa1spj")))

