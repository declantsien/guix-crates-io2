(define-module (crates-io co mb combid) #:use-module (crates-io))

(define-public crate-combid-0.5.0 (c (n "combid") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1jg9wdfl0yi4rbm0l06nrb6aq8q2k14j7rfacf9f3ci6fz0zw8d7")))

(define-public crate-combid-0.5.1 (c (n "combid") (v "0.5.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0m3carynjl5d45hxfrg4yjdz9qcfcz55lp3fhsif7cd9arm5mm0h")))

(define-public crate-combid-0.5.2 (c (n "combid") (v "0.5.2") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "01iw9pfqkkcsc8pqs373rjiaqmhr8qjcpm114mnzbiq7i4c30mqh")))

(define-public crate-combid-0.6.0 (c (n "combid") (v "0.6.0") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "13vzqrmqkjhw9pq1h2a03jk0ba243zkiwplicvvldsfysc3w20qy") (y #t)))

(define-public crate-combid-0.6.1 (c (n "combid") (v "0.6.1") (d (list (d (n "byteorder") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "074iksp6vpd9v78g9izydlxrr276pd64gai9bs9jszpaprlk47kn")))

