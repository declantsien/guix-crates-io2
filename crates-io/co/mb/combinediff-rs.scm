(define-module (crates-io co mb combinediff-rs) #:use-module (crates-io))

(define-public crate-combinediff-rs-0.1.0 (c (n "combinediff-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "patch-rs") (r "^0.6.2") (d #t) (k 0)))) (h "04jfjghmh6049yqwps4sn807y0kpdd9avx8pabbgnnnhz6ajy19d")))

(define-public crate-combinediff-rs-0.1.1 (c (n "combinediff-rs") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "patch-rs") (r "^0.6.2") (d #t) (k 0)))) (h "1v5a0spajxhxnj9jfl2cwkajhi0nvws49cb3y22kfd0jykz3xi9p")))

