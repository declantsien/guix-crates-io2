(define-module (crates-io co mb combinedfun) #:use-module (crates-io))

(define-public crate-combinedfun-0.1.0 (c (n "combinedfun") (v "0.1.0") (h "1ahrpbcjrwp72dhhvl3ww0zyfwlp9a35q2gnx8abbbyra0vafclm")))

(define-public crate-combinedfun-0.1.1 (c (n "combinedfun") (v "0.1.1") (h "0naqmd275manzp47jm9w77brfhy35c0ns1my39g5v8lmbvhgr51i")))

(define-public crate-combinedfun-0.1.2 (c (n "combinedfun") (v "0.1.2") (h "1fcqgmq0yxnj04f2qgml3cm7318sn43sp0r6ylpxg0rwj81kycya")))

(define-public crate-combinedfun-0.1.3 (c (n "combinedfun") (v "0.1.3") (h "18hrb43jspa8jj18sfhshpiivhl490b60i3dkl5cdcvxl1si0ncb")))

