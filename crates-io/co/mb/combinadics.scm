(define-module (crates-io co mb combinadics) #:use-module (crates-io))

(define-public crate-combinadics-0.1.0 (c (n "combinadics") (v "0.1.0") (d (list (d (n "binomial-iter") (r "*") (d #t) (k 0)))) (h "19m8gbrm57k4q0jms1anliag38x9m95hz8sc0zc0qhsb5a2i6zk1") (f (quote (("nightly"))))))

