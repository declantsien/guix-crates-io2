(define-module (crates-io co mb combination) #:use-module (crates-io))

(define-public crate-combination-0.1.0 (c (n "combination") (v "0.1.0") (h "0fd18wdbvkldq0gbpwqqvy7fpxialp6p5i0cc67i5qky8y9ji8n8")))

(define-public crate-combination-0.1.1 (c (n "combination") (v "0.1.1") (h "10b468zij4g9pmyy4br5471fh1kc8lzmngl1np1d9fphzi156w6y")))

(define-public crate-combination-0.1.2 (c (n "combination") (v "0.1.2") (h "0jrqr65m3dvnxm61spnq4lzwxsbbkng8axirn44d5zg8y0ljrd6z")))

(define-public crate-combination-0.1.3 (c (n "combination") (v "0.1.3") (h "0k4lav6y3bgniaq1067xqzafakf8zcg7wddii4h1ax7fba3l7xhj")))

(define-public crate-combination-0.1.4 (c (n "combination") (v "0.1.4") (h "0h9f5g6kl330v4hz21ycf4a4km9m4s5sgv5pcp18bzn04r8nzana")))

(define-public crate-combination-0.1.5 (c (n "combination") (v "0.1.5") (h "08k4b2rjhz4rzdf9nwr5xsv2yd12knizz8nilys47rm0y7rxnz1k")))

(define-public crate-combination-0.2.0 (c (n "combination") (v "0.2.0") (h "1xnj30d7zfkzmwzdms7nla7nrrq88cashqabbsh6m7rq7k7vhf21") (f (quote (("v2") ("default" "v2"))))))

(define-public crate-combination-0.2.2 (c (n "combination") (v "0.2.2") (h "1xwjlk409w0n1d4k9ba0a60mmzl650fycnpy1143yx8vqlb6hw8v") (f (quote (("v2") ("default" "v2"))))))

