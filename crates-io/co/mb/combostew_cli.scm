(define-module (crates-io co mb combostew_cli) #:use-module (crates-io))

(define-public crate-combostew_cli-0.1.0 (c (n "combostew_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "combostew") (r "^0.2.0") (d #t) (k 0)))) (h "0777raz25z7v451jxn62h1fdbg6z9mldmrhyragng6zbbp5dgdyh")))

