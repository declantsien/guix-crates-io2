(define-module (crates-io co mb combup) #:use-module (crates-io))

(define-public crate-combup-0.1.0 (c (n "combup") (v "0.1.0") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0lfs4nbdkac9xlbvjbasl2s31jssgzjfikz38jcxaxy2xpym4m2j")))

(define-public crate-combup-0.1.1 (c (n "combup") (v "0.1.1") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0b8k4l858gzrdsm3acqf492w9i0c0glm4m17kbqk0m00kk63qlj2")))

(define-public crate-combup-0.1.2 (c (n "combup") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0875kvbv6515k1s8vgkcyb8j8nhn49qq4msxbwgknba6vxwc1s5m")))

(define-public crate-combup-0.1.3 (c (n "combup") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "06nsrwk2v5lwnwvw8ypw706987agg41idshx9ah6na1wh0xgihf5")))

(define-public crate-combup-0.1.4 (c (n "combup") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "03vd6dj4v7jmk3dfgd8l8hy9rrz5db6caadhz2z3cpian5qgj1ss")))

(define-public crate-combup-0.1.5 (c (n "combup") (v "0.1.5") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0vwpjf7p0h6z8gc2lwrmdpzg49wycyamqdacrap9x2bqbnjpn97i")))

