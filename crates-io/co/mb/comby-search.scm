(define-module (crates-io co mb comby-search) #:use-module (crates-io))

(define-public crate-comby-search-0.1.0 (c (n "comby-search") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0ljsd3ffq2cvga8vjpirl3qjd43acnzs6rz36phrfbs4viq2m6v9")))

(define-public crate-comby-search-0.1.1 (c (n "comby-search") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "05rmdn9qfz6s57i3qdl557k1vc0xc7jnl05kpbpi219bd39ijyf2")))

(define-public crate-comby-search-0.1.2 (c (n "comby-search") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1rz5z0gls9i02xi6w93p6x9ixw9i4d7gzb0zsr8ajd6xahgl5gfp")))

(define-public crate-comby-search-0.2.0 (c (n "comby-search") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0pw60jhfl2xh74sh3plclx0r10syyn7s5vmfh1vr7jvyiarh4dqs")))

(define-public crate-comby-search-0.2.1 (c (n "comby-search") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1bbwbqlv3x5zcv8lshsn7ii5nxaw8scp3jf14pddb9lff8rhkyyw")))

