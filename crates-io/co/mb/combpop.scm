(define-module (crates-io co mb combpop) #:use-module (crates-io))

(define-public crate-combpop-0.0.1 (c (n "combpop") (v "0.0.1") (h "0hj9zzv01cx5xw4423i6kdwpv262racq9ndn7a5awd4iqjs3mjs6")))

(define-public crate-combpop-0.0.2 (c (n "combpop") (v "0.0.2") (h "1vmlza6g04kb1djlxnffdq8slh9dx57n14xwhghc4bdlwxz9maw0")))

(define-public crate-combpop-0.0.3 (c (n "combpop") (v "0.0.3") (h "0jjlaxpc6xwxhx4m8bgflnfjbdgxqkdaqzxs34i65cml2xsbmw76")))

