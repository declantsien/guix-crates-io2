(define-module (crates-io co mb combee) #:use-module (crates-io))

(define-public crate-combee-0.1.0 (c (n "combee") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07cawznnpwd8skh2fvgljiq4h3zc4z0g67hgz3rx5gf4qq6phkpz")))

(define-public crate-combee-0.1.1 (c (n "combee") (v "0.1.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1954mbgbb6f72zr5jz23sf4whsip0lf5v013r5a29dhz2akr700f")))

(define-public crate-combee-0.2.0 (c (n "combee") (v "0.2.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "054md0q5wc4hxghkcrfcsq5x35qdysf5yv5pk8k9akmh81rqyf6d")))

(define-public crate-combee-0.3.0 (c (n "combee") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "parquet") (r "^45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19zr1cp6d72vihfbyqsy7081q8fp37yg1qbb1cilan3scwa7l789")))

(define-public crate-combee-0.4.0 (c (n "combee") (v "0.4.0") (d (list (d (n "arrow2") (r "^0.17.4") (f (quote ("io_parquet"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "parquet") (r "^45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrow") (r "^0.7.1") (f (quote ("arrow2-0-17"))) (d #t) (k 0)) (d (n "time") (r "^0.3.27") (d #t) (k 0)))) (h "0iqia962pa1jmxzvz7jc4xxv8r5px6d9sfi57bzzr2kb64q6xpx0")))

(define-public crate-combee-0.5.0 (c (n "combee") (v "0.5.0") (d (list (d (n "arrow2") (r "^0.17") (f (quote ("io_parquet"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "parquet") (r "^45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrow") (r "^0.7.1") (f (quote ("arrow2-0-17"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "1b21ky0xs81dznba74s50y1r81hyl08rlbz2fch9r5dbz6s4qlvg")))

(define-public crate-combee-0.6.0 (c (n "combee") (v "0.6.0") (d (list (d (n "arrow2") (r "^0.17") (f (quote ("io_parquet"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "parquet") (r "^46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrow") (r "^0.7") (f (quote ("arrow2-0-17"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0q8x442ywrmq1yv3lwvpqcx449kzbsyw32licvdkp97x4b35fm4g")))

