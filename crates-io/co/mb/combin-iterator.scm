(define-module (crates-io co mb combin-iterator) #:use-module (crates-io))

(define-public crate-combin-iterator-0.0.1 (c (n "combin-iterator") (v "0.0.1") (h "1zx2cy7yij4b9hc8gnfsab3jr95cf489w6fbv797b8q6d790i1fp")))

(define-public crate-combin-iterator-0.1.0 (c (n "combin-iterator") (v "0.1.0") (h "18p74sb7b6p8rjbcjpg5hxdzb8va2f0w8j643x5m1n4dh6ny1z6n")))

(define-public crate-combin-iterator-0.2.0 (c (n "combin-iterator") (v "0.2.0") (h "1igy1i86cmfd6bpvjxs9a2qzb3szwayjffd5qjsz1d7kpmw298in")))

(define-public crate-combin-iterator-0.2.1 (c (n "combin-iterator") (v "0.2.1") (h "0069lrw5cmw011vmc9waznd65zsrxfhf5a0l749fzxgpr1qxyjnh")))

(define-public crate-combin-iterator-0.2.2 (c (n "combin-iterator") (v "0.2.2") (h "04mji86l0crxh0shdx2ssifbizyq8l9j3pdjdhba9y0jy1lpc84a")))

