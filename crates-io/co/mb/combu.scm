(define-module (crates-io co mb combu) #:use-module (crates-io))

(define-public crate-combu-0.0.1 (c (n "combu") (v "0.0.1") (h "0xv4h29ynidz1hhqr1j0wlmx4acm3h1p0r32k82f9n6k6rbnzf3m")))

(define-public crate-combu-0.0.2 (c (n "combu") (v "0.0.2") (h "0fm24qlpcj642f02n4a3dq2w5swa49c6rlicch2v6p9cwzjn5n05")))

(define-public crate-combu-0.0.3 (c (n "combu") (v "0.0.3") (h "0k31siqmihn68zxi8gp10ranx3f5c4p3b4jsjy3bdw82n81rl8nc")))

(define-public crate-combu-0.0.4 (c (n "combu") (v "0.0.4") (h "1h6f9gcbi3krf25fbx0nzs0ac32fljdm4rsx6cw7wi7ry0wg3fa2")))

(define-public crate-combu-0.0.5 (c (n "combu") (v "0.0.5") (h "0szyjqj0fwazdffpx1i9h7pb4amd1c4dscahkvmya432i4mpi5lk")))

(define-public crate-combu-0.0.7 (c (n "combu") (v "0.0.7") (h "16pdqpnmh6m8bqsq3q2n54dq5wf9pnrzvxn1p4y7ym32rr5lwp1a") (y #t)))

(define-public crate-combu-0.0.6 (c (n "combu") (v "0.0.6") (h "16lh0qv7zccbaq311nklyyfi9vh47imrwmn57wxsfycjchpck0k9")))

(define-public crate-combu-0.0.8 (c (n "combu") (v "0.0.8") (h "0qy35cwwladndmk4myxcx698wgpg10idi9y8kykykjw1w4qmip3i")))

(define-public crate-combu-0.0.9 (c (n "combu") (v "0.0.9") (h "05m9309a8ai7g0308jxvy8zf0a8mly1v89c56ixi6rd39jys613w")))

(define-public crate-combu-0.0.10 (c (n "combu") (v "0.0.10") (h "120pg47qkzs54nqxc9c9pyp04kjzghn1s326bh7y9cm3h8gxl1zh")))

(define-public crate-combu-0.0.11 (c (n "combu") (v "0.0.11") (h "10a669g802cdfcvv6gdhalmh035yy051fkwncwncrgv0d31q7bks")))

(define-public crate-combu-0.0.12 (c (n "combu") (v "0.0.12") (h "0cb5msk69x512b20r6m9llyvlfdaa22a37b1p11vfbz5w4cv6x3v")))

(define-public crate-combu-0.0.13 (c (n "combu") (v "0.0.13") (h "0zik1vpr2ifdb2amk0hqhw9l6gf8qzfh4cklaywbhz586apqpa73")))

(define-public crate-combu-0.0.14 (c (n "combu") (v "0.0.14") (h "1zr1smqjgan9h2w2ll0qyf07p7fgp0c6pnizp9ibvbzsqjbf33lf")))

(define-public crate-combu-0.1.0 (c (n "combu") (v "0.1.0") (h "1qvyg08cgs3xb2c0rvp988h648yj29f922rhim62a097kwq7asrb")))

(define-public crate-combu-0.1.1 (c (n "combu") (v "0.1.1") (h "10jpgwj28jx2w1j5a6w63z0faakrixsigb02nw3jm10c693bc36b")))

(define-public crate-combu-0.1.2 (c (n "combu") (v "0.1.2") (h "156zxmlidg7hs8i0yh8is9ib4zdyy031a06lwshn8ngny0x0nyz8")))

(define-public crate-combu-0.1.3 (c (n "combu") (v "0.1.3") (h "1pldxakga2yd3g30kh0wqb7mzaa5cl3wxlcsd3s8dbcd6a2522vj")))

(define-public crate-combu-0.1.4 (c (n "combu") (v "0.1.4") (h "1cc34iqpxhj2wkf9cp76vpd754kp8k1vn152g456jv01fc6n7m53")))

(define-public crate-combu-0.1.5 (c (n "combu") (v "0.1.5") (h "11b4bp5b1v8izqj5mrxjx8wl3hvgjxhzn6qwbpydwibxsf9wbq4w")))

(define-public crate-combu-0.1.6 (c (n "combu") (v "0.1.6") (h "1w7dbmwi88i2cvadqrfkv4ns7jsjyg9mh6adpldjb1r8xj48fgmc")))

(define-public crate-combu-0.2.0 (c (n "combu") (v "0.2.0") (h "1kr3h0b7xbgyzyfjbppgczbjn3g4pikw8qfvrnxcrc7c5sp56p40")))

(define-public crate-combu-0.2.2 (c (n "combu") (v "0.2.2") (h "009wsbladf0vqpn1df8l02qnixy215l801avsql0qkn57kw1yhc3")))

(define-public crate-combu-0.2.3 (c (n "combu") (v "0.2.3") (h "0nlglp0jngax0w5nz2hvb6gq2sbqi16zma9f8igkqfqkxiljgg7n")))

(define-public crate-combu-0.2.4 (c (n "combu") (v "0.2.4") (h "13743dphl3s36dqb95i6z3fixg1hv3jj9r3hjrjnlb0bd72q6gdc")))

(define-public crate-combu-0.2.5 (c (n "combu") (v "0.2.5") (h "09cq8v61qwwzsmag0gh647nzvrpvzlwzqvxcg9vsjv3v371189k5")))

(define-public crate-combu-0.2.6 (c (n "combu") (v "0.2.6") (h "1g4c7rdcrwk4rx73abvy051cbh86na64x2a7w3j5mwzh5v5ygs55")))

(define-public crate-combu-0.3.0 (c (n "combu") (v "0.3.0") (h "132inj94hy9b6q30d298x2qsdq90x9188qwzhfrpkdlna7lszx69")))

(define-public crate-combu-0.3.1 (c (n "combu") (v "0.3.1") (h "0w2ghjys66jswgkmb70rrx5h13npv76x3cr073hzrs9n0zbr1675")))

(define-public crate-combu-0.3.2 (c (n "combu") (v "0.3.2") (h "10lys8ncka9jcnz0bpgf45gs8mmxqr4rjgxirw2yi6p69sm5dqw6")))

(define-public crate-combu-0.3.3 (c (n "combu") (v "0.3.3") (h "0mbjl9vnwpy04cazrc4bwlw5dxx0wsisbgz9gyqsdmk55bhpzbcs")))

(define-public crate-combu-0.3.4 (c (n "combu") (v "0.3.4") (h "0zrb041s70r0xw0ynqfvinfdnf7yi9bynpw1v68n2my1v5h8d2ad")))

(define-public crate-combu-0.3.5 (c (n "combu") (v "0.3.5") (h "0as1hyg5n3p0svqp6i07hpvmxg95bm1f6p6zljd0vxvlaflfsi1l")))

(define-public crate-combu-0.3.6 (c (n "combu") (v "0.3.6") (h "0m43c9xiqpxdwy7a4jnkbyvih8kwsk52jbaqbshrcq7vkvd3c32m")))

(define-public crate-combu-0.3.7 (c (n "combu") (v "0.3.7") (h "1qbg7sig8kkazddaxvkg0mavqz2prc43nfhb194p6imkqzpqag8q")))

(define-public crate-combu-0.3.8 (c (n "combu") (v "0.3.8") (h "0pdbjzbbp8j2syk8yjnnqh5s403jq4zz0vh0pw0fwly0kngin5kd")))

(define-public crate-combu-0.4.0 (c (n "combu") (v "0.4.0") (h "1mrjfqipzarvnd9c4mjq5wvd528hcni6zl42h6zbkyipzxr10yr9")))

(define-public crate-combu-0.4.1 (c (n "combu") (v "0.4.1") (h "1whz0sgafq7xm9n1pjf96yqa0cf5b2fabpkcdqvby2fm1ibnjp9m")))

(define-public crate-combu-0.4.2 (c (n "combu") (v "0.4.2") (h "07i85zx1ndvpk4gw3z8di8dlzqv4hg3vzc3mjr8qbg3gppq9g4fj")))

(define-public crate-combu-0.4.3 (c (n "combu") (v "0.4.3") (h "13q7jnq9kli731cy5l0px3iqxp9kslvdrznic1x9g33qfmnb0078")))

(define-public crate-combu-0.4.4 (c (n "combu") (v "0.4.4") (h "1fcpvsyjiy84ai3l6a7cinndfhcdhg3gws2yc1vcv5z3qky71kmb")))

(define-public crate-combu-0.5.0 (c (n "combu") (v "0.5.0") (h "1bbzydjldp69jkavl1hyjc8f230s7mczk9a1z2g3aiajasvrblnq")))

(define-public crate-combu-0.5.1 (c (n "combu") (v "0.5.1") (h "13fc8m249hi08a53d4hswqvhmgl860yw21n2sm8znjq81wrh0qdq")))

(define-public crate-combu-0.5.2 (c (n "combu") (v "0.5.2") (h "0sh3pwnxwpg34j3iydpkm596f418rnyxq9msv0rh7ch0pzck059p")))

(define-public crate-combu-0.5.3 (c (n "combu") (v "0.5.3") (h "0l8r97b9z1r56l42wsdin2waszy4b878dcxwjwv3rv67kvmmv5db")))

(define-public crate-combu-0.6.0 (c (n "combu") (v "0.6.0") (h "09yfy9r4n58c1jzw1ynvhnmi7igl84qz6bxcyw32y7xi6sl0ss3m")))

(define-public crate-combu-0.6.1 (c (n "combu") (v "0.6.1") (h "1f0h64n9yj5mdlsas1qvrr0q11p1dh5wzhzmr03ps3hnm25dv9nz")))

(define-public crate-combu-0.6.2 (c (n "combu") (v "0.6.2") (h "1bk873klhs04fm9l53sv4vl8gvd3wpvdpwghgqbhsb3xikvk9ck8")))

(define-public crate-combu-0.6.3 (c (n "combu") (v "0.6.3") (h "01v2xwcn4hmrbfvx7n98irna871szp58sssrdxp6qsc9nbrfmmxb")))

(define-public crate-combu-0.6.4 (c (n "combu") (v "0.6.4") (h "1pqvfjbp9arcf4n12hf2s52113ls79gd1zmr7jhdws97pdm9r0fq")))

(define-public crate-combu-0.6.5 (c (n "combu") (v "0.6.5") (h "00k8pdq39i4jsbyihp5baqx8wbfnnvyvng5s2f605ilcy5aa60xq")))

(define-public crate-combu-0.6.6 (c (n "combu") (v "0.6.6") (h "1y1ffy2i9zy5zwjyxwx2l6hkm5cybaqj13c0zx159ddaxs0k1x79")))

(define-public crate-combu-0.6.7 (c (n "combu") (v "0.6.7") (h "0ac8a7hv6495jpljss0prjp7cdr8xszml24rmw0rz1dnawqyd09i")))

(define-public crate-combu-0.6.8 (c (n "combu") (v "0.6.8") (h "1mxl4y4jx7ia5arbblkvr5igl6qddccirc40s82lqs1k5g9db3xs")))

(define-public crate-combu-0.7.0 (c (n "combu") (v "0.7.0") (h "16ard02rzcrpijb6nwxwgipvnf7d2rgz2aa6bang62y8589k8jw6")))

(define-public crate-combu-0.7.1 (c (n "combu") (v "0.7.1") (h "01zysagk4j7wqkmnj9ys1sjk4hqsd6aj58q5sfanzcyl25j07mki")))

(define-public crate-combu-0.7.2 (c (n "combu") (v "0.7.2") (h "10v0nhdcj8m773kb358bpa6nw0q1cr5222czl9b0rvgqzcshnai5")))

(define-public crate-combu-0.7.3 (c (n "combu") (v "0.7.3") (h "1rmcr2500vq61vn1vp1y9yp9sn17kcp2nq1ja8wcj5h7dyrrbak9")))

(define-public crate-combu-0.8.0 (c (n "combu") (v "0.8.0") (h "0rwywff6f906pyfjjz6p2mnahszlw13j4g14gxh1wry7hmfy5fqz")))

(define-public crate-combu-0.8.1 (c (n "combu") (v "0.8.1") (h "175zlgmsl0apqhr4nvawr2vw7b8z3pqvlzsnx4r42fnifxam20w8")))

(define-public crate-combu-0.9.0 (c (n "combu") (v "0.9.0") (h "1hy5gr5i7rbxa8fpfka94ybiqlib94amr29dmlqv8ddlyssi1dnl")))

(define-public crate-combu-0.9.1 (c (n "combu") (v "0.9.1") (h "1v213av289630357r0q0vnr3av9mb7f4j0y7r038f26d8zkvviz9")))

(define-public crate-combu-0.10.0 (c (n "combu") (v "0.10.0") (h "1kkpklashdnb3a5sgbmd79kp5xzkac8xm11x52vcwwk9lv4kx58l")))

(define-public crate-combu-0.10.1 (c (n "combu") (v "0.10.1") (h "1p7c0i4ipw7vcs1k2czb2w448byksd3m6asj3rh85wfpclfpr0g2")))

(define-public crate-combu-0.11.0 (c (n "combu") (v "0.11.0") (h "0y1ppgj6p6hzdwwdmfkxjayrfj4wmyqwhwfhhylzyfmvb6hhyw2k")))

(define-public crate-combu-0.11.1 (c (n "combu") (v "0.11.1") (h "164bjmraaz5vnlcgfqs4y4sy1dspyka028jd1i31ir0h4jvswwvp")))

(define-public crate-combu-0.11.2 (c (n "combu") (v "0.11.2") (h "0y278lbpskqi7pyrvh9yiz0c3hpgfjbf5ybmnasqmhgfysbkxmxd")))

(define-public crate-combu-1.0.0 (c (n "combu") (v "1.0.0") (h "0syiklyy0r45v3zbcd9jpid5s8zvv3192i9g973zzc3mfflvkray")))

(define-public crate-combu-1.1.0 (c (n "combu") (v "1.1.0") (h "041nmcq14k80y14wafqrz99z29d51215dl32a816ml0bpwkqpxyx")))

(define-public crate-combu-1.1.1 (c (n "combu") (v "1.1.1") (h "0slc8rkygcp2qzasn09d2q2bd8j8gr936k3lg6pcbz58gr2hiwkf")))

(define-public crate-combu-1.1.2 (c (n "combu") (v "1.1.2") (h "1ms6rqihf7zvjymjaq0xcg4cjhhh05z6gxd8zwhm9gl1ibb6jnfz")))

(define-public crate-combu-1.1.3 (c (n "combu") (v "1.1.3") (h "02h7zp15n6m9jk0y0ws8pxqd8q4h25fjmmfziwr49ak944vk4sn3")))

(define-public crate-combu-1.1.4 (c (n "combu") (v "1.1.4") (h "0ygj26cwbkd4fxijbminczxxrxfh58gciqrrgc0vrb7lw0bmj9xb")))

(define-public crate-combu-1.1.5 (c (n "combu") (v "1.1.5") (h "0bcf17kr6ncqmq7b0dvz2dmgag6qivjmwbzdv5wvvy8x8dy7vcdk")))

(define-public crate-combu-1.1.6 (c (n "combu") (v "1.1.6") (h "1bssysqmkm9zcvhjad82jba7nlvczzkv2gn6dndlll4z3anx8nik")))

(define-public crate-combu-1.1.7 (c (n "combu") (v "1.1.7") (h "06yzp379vl1dbclhm99wnjj6x28g1nlv1d5fj2gvx9mg84hgimg2")))

(define-public crate-combu-1.1.8 (c (n "combu") (v "1.1.8") (h "0a0pqj8m79dx6rircsfkd183px49jzlv18n863mnzdhaczdhacbm")))

(define-public crate-combu-1.1.9 (c (n "combu") (v "1.1.9") (h "1a4yw10mwk1gvvj0x9hcrj8p4nnk0fy2imbd88hhbnv92yp8g0p4")))

(define-public crate-combu-1.1.10 (c (n "combu") (v "1.1.10") (h "1m8l0wxx3g7wvkg4ay2gg9xc5700s249na07c8qkbb04a0s32nzn")))

(define-public crate-combu-1.1.11 (c (n "combu") (v "1.1.11") (h "1igdg1x9r9gkj32rpys6isz5js87h0xwqf8yxqgwsdv1khrq1sbc")))

(define-public crate-combu-1.1.12 (c (n "combu") (v "1.1.12") (h "1i9q1bhn9raq4i3nxwimsww7raavgw1na95d631nwqilc430641b")))

(define-public crate-combu-1.1.13 (c (n "combu") (v "1.1.13") (h "0a6pxdcj642srap248i0wqnz3j200dphd24ca9pymcgl0dvqc1nn")))

(define-public crate-combu-1.1.14 (c (n "combu") (v "1.1.14") (h "1m1dmprx0bfrkr4s7n5vkf6fd93pn3r311y9bhzhwkdgdv1ag378")))

(define-public crate-combu-1.1.15 (c (n "combu") (v "1.1.15") (h "1h0ls4dw66i6jgqj1yckr0zxqmm697252vsp5kbc9786rzjna616")))

(define-public crate-combu-1.1.16 (c (n "combu") (v "1.1.16") (h "1rfffffh77dxqkywsl5bskw42l3ggnvpjg5w7v8f1rs6fnq1l05r")))

(define-public crate-combu-1.1.17 (c (n "combu") (v "1.1.17") (h "1awnqqblm1bg2m3gda9mh9w7mylzckvvlv17f3k2yphr7inaxffh")))

(define-public crate-combu-1.1.18 (c (n "combu") (v "1.1.18") (h "00dlwlln6v5zcgbsrisdhfizamnvmsgnyjn3h96rjgr7k7ms6pz5")))

(define-public crate-combu-1.1.19 (c (n "combu") (v "1.1.19") (h "0nhf5q85c567f4dwgjqfrxals1m93c025sd648mrrxj14jn8s1l7")))

(define-public crate-combu-1.1.20 (c (n "combu") (v "1.1.20") (h "0vv499d2kz4pamsziih5b1cc0a42g18wvvvf0spxvfkdbh07ywkv")))

(define-public crate-combu-1.1.21 (c (n "combu") (v "1.1.21") (h "1r19kjzxg275xi4mdz94z8zamwpd4drhvnm1vfhs0dlx63gw8ax0")))

(define-public crate-combu-1.1.22 (c (n "combu") (v "1.1.22") (h "0b5yw0g1f3ajccxdd3xv8c69484488pb5r1f53168xr68w7fs4c3")))

(define-public crate-combu-1.1.23 (c (n "combu") (v "1.1.23") (h "1k60m2kwjr8y1sf3y4n5i8s4bydspl5v50v88nc0bbhfziylnj08")))

