(define-module (crates-io co mb combine-proc-macro) #:use-module (crates-io))

(define-public crate-combine-proc-macro-0.2.0 (c (n "combine-proc-macro") (v "0.2.0") (d (list (d (n "combine") (r "^3.8.1") (d #t) (k 0)))) (h "0wcp5sjjign2sjba6fa01bc5p6bzjmwrwhb40i6rzksbpq1k2vp1")))

(define-public crate-combine-proc-macro-0.2.1 (c (n "combine-proc-macro") (v "0.2.1") (d (list (d (n "combine") (r "^3.8.1") (d #t) (k 0)))) (h "0kwar7fdvcw0jacgca53vvnidlxddzsx5fsz6k64bcdnzlp261nx")))

(define-public crate-combine-proc-macro-0.3.0 (c (n "combine-proc-macro") (v "0.3.0") (d (list (d (n "combine") (r "^3.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)))) (h "0yi6vbz0fzl19bylcavkaz8iabf1yxbayh495qriyd16d5ajh9g0")))

(define-public crate-combine-proc-macro-0.3.1 (c (n "combine-proc-macro") (v "0.3.1") (d (list (d (n "combine") (r "^3.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)))) (h "06z1ngbzzzvnyyj7mlahy2c410056abmbdpd69csknssi4616vv4")))

