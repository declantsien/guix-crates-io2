(define-module (crates-io co mb combine_traits) #:use-module (crates-io))

(define-public crate-combine_traits-0.1.0 (c (n "combine_traits") (v "0.1.0") (h "1dfqz4a1i8xw9zbydjm9iljp7r7pah1x587f6wanhcxd2mw1qkbz")))

(define-public crate-combine_traits-0.1.1 (c (n "combine_traits") (v "0.1.1") (h "0rc1ly539wfc8y88117y4kda923jm3a2afsss6cb1yv9wx10ijk1")))

(define-public crate-combine_traits-0.1.2 (c (n "combine_traits") (v "0.1.2") (h "06sr64c5xn3ccf98jp5nmxxw0cb3xgds1zbzqhpfh5qn2caxf6yl")))

