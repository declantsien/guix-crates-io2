(define-module (crates-io co mb combine-language) #:use-module (crates-io))

(define-public crate-combine-language-0.5.0 (c (n "combine-language") (v "0.5.0") (d (list (d (n "combine") (r "1.0.*") (d #t) (k 0)))) (h "073a9k5dz6b1h3lh27afrw740s6pbs0dx8a1gkxmlgw9vyq41an7")))

(define-public crate-combine-language-0.6.0 (c (n "combine-language") (v "0.6.0") (d (list (d (n "combine") (r "1.0.*") (d #t) (k 0)))) (h "1y0q9lbc2jvc60s618jn0lf8bp78wnp7hla7cb6icf56bzanj7xp")))

(define-public crate-combine-language-0.7.0 (c (n "combine-language") (v "0.7.0") (d (list (d (n "combine") (r "^1.0.0") (d #t) (k 0)))) (h "1mdvwdglhb3xz0j8bzkjrix6kym4z4jk8ch4h91mmgp2rcrv198l")))

(define-public crate-combine-language-0.7.1 (c (n "combine-language") (v "0.7.1") (d (list (d (n "combine") (r "^1.0.0") (d #t) (k 0)))) (h "1hplf6vwn6q357z0vwzbsa1xmcqzhy79mr3zalhs8z2lsahsq2ps")))

(define-public crate-combine-language-0.8.0 (c (n "combine-language") (v "0.8.0") (d (list (d (n "combine") (r "^1.0.1") (d #t) (k 0)))) (h "1gy0lpn49zyg15fqdam1d14kxix3lfw157bkhq5yqdgj44ljchrb") (f (quote (("test" "range_stream") ("range_stream" "combine/range_stream"))))))

(define-public crate-combine-language-0.9.0 (c (n "combine-language") (v "0.9.0") (d (list (d (n "combine") (r "^1.1.0") (d #t) (k 0)))) (h "1i5gsdxxd88v8v0mal1kvrihdvxgcm4z39ar420nz7y2lvp910vy") (f (quote (("test" "range_stream") ("range_stream" "combine/range_stream"))))))

(define-public crate-combine-language-2.0.0-beta (c (n "combine-language") (v "2.0.0-beta") (d (list (d (n "combine") (r "^2.0.0-beta") (d #t) (k 0)))) (h "18lclvg14nrd5dmx6h250b3q3amsnlm4bd72fvxji1fb11jy7ail")))

(define-public crate-combine-language-1.0.0 (c (n "combine-language") (v "1.0.0") (d (list (d (n "combine") (r "^1.1.0") (d #t) (k 0)))) (h "0j69d5a05wbc615z7k85fkdxzjg7xzlxgkr03q4y3dzqq6pyy2rw")))

(define-public crate-combine-language-2.0.0-beta2 (c (n "combine-language") (v "2.0.0-beta2") (d (list (d (n "combine") (r "^2.0.0-beta") (d #t) (k 0)))) (h "1i7f67qrqcmk88a91bc9ia85i33kpipvvwpjk6gz1drkcw4nrh0c") (y #t)))

(define-public crate-combine-language-2.0.0-beta3 (c (n "combine-language") (v "2.0.0-beta3") (d (list (d (n "combine") (r "^2.0.0-beta2") (d #t) (k 0)))) (h "0kr4vdjrlbgr82krjicipdp6jxf12cfsnpyiiz7ss5023d6lzbzc")))

(define-public crate-combine-language-2.0.0-beta4 (c (n "combine-language") (v "2.0.0-beta4") (d (list (d (n "combine") (r "^2.0.0-beta3") (d #t) (k 0)))) (h "016gpjnksd2pv0raq1kcqf8l7gccfa31fgzkacyvf73faqfwykmq")))

(define-public crate-combine-language-2.0.0 (c (n "combine-language") (v "2.0.0") (d (list (d (n "combine") (r "^2.0.0") (d #t) (k 0)))) (h "19ikqimv9wh2mq05mpwiyyj6r1qp86kcln9vsmi2d27kia9cxqbj")))

(define-public crate-combine-language-3.0.0 (c (n "combine-language") (v "3.0.0") (d (list (d (n "combine") (r "^3.5.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 2)))) (h "1l4h4wha3wbffnf7915lpazd913ny2xvjmyhz60mnxmwc61bxfzn")))

(define-public crate-combine-language-3.0.1 (c (n "combine-language") (v "3.0.1") (d (list (d (n "combine") (r "^3.5.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 2)))) (h "1gd94kq6hfz7h3l6f5adyfbdjl8a3aim1dk9j0c4irl6j8mcycs1")))

(define-public crate-combine-language-3.0.2 (c (n "combine-language") (v "3.0.2") (d (list (d (n "combine") (r "^3.5.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 2)))) (h "1m4mrshh6vwj69bzw8q97g6zn74gmk2pz5zj0kp8rxiz97q3dlvc")))

(define-public crate-combine-language-4.0.0 (c (n "combine-language") (v "4.0.0") (d (list (d (n "combine") (r "^4") (d #t) (k 0)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 2)))) (h "0m5w4g17xx7msm925wwmryd5w2mqss0hafyrmclbbkhm9d4ifix6")))

