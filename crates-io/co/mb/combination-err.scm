(define-module (crates-io co mb combination-err) #:use-module (crates-io))

(define-public crate-combination-err-0.1.1 (c (n "combination-err") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "09li0jvl8pkpz8k9z5j78rfnhki7hfd4dq9jzvd7k2y84l6gfba2")))

(define-public crate-combination-err-0.1.2 (c (n "combination-err") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1w0hqqpan056jjxy8hzv121w2p0dqyqhyb4hihqkf22r43pad5l6")))

