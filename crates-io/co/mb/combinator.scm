(define-module (crates-io co mb combinator) #:use-module (crates-io))

(define-public crate-combinator-0.1.0 (c (n "combinator") (v "0.1.0") (h "1rjq6ykn35jw0pj4dzq65x3fxhbypi82d2s905x3xwzd1dbj3gzp")))

(define-public crate-combinator-0.1.1 (c (n "combinator") (v "0.1.1") (h "0ssglsjbvxf3gsr20jban2aysivxbgbwmwzflaq9i05pkd2vs9i1")))

(define-public crate-combinator-0.1.2 (c (n "combinator") (v "0.1.2") (h "07y4wprh19ppy3s5ay01xdpiacynq4z33lg60i1p60clmj5g63lr")))

