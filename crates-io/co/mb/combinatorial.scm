(define-module (crates-io co mb combinatorial) #:use-module (crates-io))

(define-public crate-combinatorial-0.1.0 (c (n "combinatorial") (v "0.1.0") (h "0pm7saw29hq6w8maam3066lgw2chdpvpzgn2hxgk0r7pbn5hkaza")))

(define-public crate-combinatorial-0.2.0 (c (n "combinatorial") (v "0.2.0") (h "117dlfgcdg08dj5vxx7p0lk4fd8nj66qqmnvmxqlc5sr2bvz5nkv")))

