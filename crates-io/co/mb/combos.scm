(define-module (crates-io co mb combos) #:use-module (crates-io))

(define-public crate-combos-0.1.0 (c (n "combos") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "1d46k7zp90438gg5gmz1z4hk0mrzs0k0hs9gmddd8c462h3d6qa2")))

(define-public crate-combos-0.1.1 (c (n "combos") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "05rn4mwq6js8whixm40h1gif9drnlpskwymkp83q0pcc6vcs4686")))

(define-public crate-combos-0.2.1 (c (n "combos") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "04fj02547vz19dfxldv81z3xbwvgaw923a46cgz7kfvsspbmdymg")))

