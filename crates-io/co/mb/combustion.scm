(define-module (crates-io co mb combustion) #:use-module (crates-io))

(define-public crate-combustion-0.1.0 (c (n "combustion") (v "0.1.0") (d (list (d (n "gray_matter") (r "^0.2") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.5") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "rocket_dyn_templates") (r "^0.1.0-rc.2") (f (quote ("handlebars"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "167xlgnwi9vmaypz14jwb2dld82jgnkjk6vj6vr1wwazhqz9f5rr") (r "1.66.0")))

