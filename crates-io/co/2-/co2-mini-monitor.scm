(define-module (crates-io co #{2-}# co2-mini-monitor) #:use-module (crates-io))

(define-public crate-co2-mini-monitor-0.1.1 (c (n "co2-mini-monitor") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hidapi") (r "^1.2.5") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1fmxkj04pw879k0va87s6cg09kf7v21lb8qnimdaaawk08qbdiri")))

(define-public crate-co2-mini-monitor-0.1.2 (c (n "co2-mini-monitor") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hidapi") (r "^1.2.5") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1y8lw2d5mksb61m4pxmx069z3l34l9jxkj29fanyfakxy0z9q7wc")))

(define-public crate-co2-mini-monitor-0.1.3 (c (n "co2-mini-monitor") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hidapi") (r "^1.2.5") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "02lhywg4bk41lm473v3frcay1gcq9l53kcwsxd3rdqwrpabk9kss")))

