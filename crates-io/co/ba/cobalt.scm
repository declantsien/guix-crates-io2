(define-module (crates-io co ba cobalt) #:use-module (crates-io))

(define-public crate-cobalt-0.1.0 (c (n "cobalt") (v "0.1.0") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0jm8hc8044x4a43y3xkwi7pxnx0plbcqhj16m74z5hf428b5w30c")))

(define-public crate-cobalt-0.1.1 (c (n "cobalt") (v "0.1.1") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "00j6454i0z3gdrih0d8wsmvbyiyks7bi8cajfkmyr4zypiahmikf")))

(define-public crate-cobalt-0.2.0 (c (n "cobalt") (v "0.2.0") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1qywlm2m73hz6bp1i8qaypc6jqlvk589y74s2fx70qr2if4db0fk")))

(define-public crate-cobalt-0.3.0 (c (n "cobalt") (v "0.3.0") (d (list (d (n "clock_ticks") (r "~0.0.6") (d #t) (k 0)) (d (n "rand") (r "~0.3.10") (d #t) (k 0)))) (h "1rgvj1678x1bswgsg7dxjgwrr5r008rhdqkygnq5z72l9754bpld")))

(define-public crate-cobalt-0.4.0 (c (n "cobalt") (v "0.4.0") (d (list (d (n "clock_ticks") (r "~0.0.6") (d #t) (k 0)) (d (n "rand") (r "~0.3.10") (d #t) (k 0)))) (h "0163ga3dzgw2a2jr08zgap5kflc81n0qq1wvkp44bgjdczsr7fkn") (f (quote (("packet_handler_lost") ("packet_handler_compress"))))))

(define-public crate-cobalt-0.5.0 (c (n "cobalt") (v "0.5.0") (d (list (d (n "clock_ticks") (r "~0.0.6") (d #t) (k 0)) (d (n "rand") (r "~0.3.10") (d #t) (k 0)))) (h "02fjph8nw4lg6dw80hxx1sym96zdjiy543m346m50pvr1g23ydqh") (f (quote (("packet_handler_lost") ("packet_handler_compress") ("all" "packet_handler_lost" "packet_handler_compress"))))))

(define-public crate-cobalt-0.5.1 (c (n "cobalt") (v "0.5.1") (d (list (d (n "clock_ticks") (r "~0.0.6") (d #t) (k 0)) (d (n "rand") (r "~0.3.10") (d #t) (k 0)))) (h "1fzqy17bwh05zfvydxw5zbv1npjrj96v1c68q9cdbjqyw4ypskd5") (f (quote (("packet_handler_lost") ("packet_handler_compress") ("all" "packet_handler_lost" "packet_handler_compress"))))))

(define-public crate-cobalt-0.5.2 (c (n "cobalt") (v "0.5.2") (d (list (d (n "clock_ticks") (r "~0.1.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.13") (d #t) (k 0)))) (h "0lzyr2i4v18shyma85lwsrsx6dr1kx7wgj05ny94hgb84nlbfg74") (f (quote (("packet_handler_lost") ("packet_handler_compress") ("all" "packet_handler_lost" "packet_handler_compress"))))))

(define-public crate-cobalt-0.5.3 (c (n "cobalt") (v "0.5.3") (d (list (d (n "clock_ticks") (r "~0.1.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.13") (d #t) (k 0)))) (h "1lxmnpsdz2hnv8gw2v8fibrmnqs0zidgbs10sd8wgvx9b59almfi") (f (quote (("packet_handler_lost") ("packet_handler_compress") ("all" "packet_handler_lost" "packet_handler_compress"))))))

(define-public crate-cobalt-0.7.0 (c (n "cobalt") (v "0.7.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "1abcla1knm1x785im0mcphv5vn0qy11d1kym7iwppzi1avf03gwp") (f (quote (("packet_handler_lost") ("packet_handler_compress") ("all" "packet_handler_lost" "packet_handler_compress"))))))

(define-public crate-cobalt-0.8.0 (c (n "cobalt") (v "0.8.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0f32sdgpfi909b5c7x9vy137i55f4alajzy694jz6w6mzgdibc6l") (f (quote (("packet_handler_lost") ("packet_handler_compress") ("all" "packet_handler_lost" "packet_handler_compress"))))))

(define-public crate-cobalt-0.8.1 (c (n "cobalt") (v "0.8.1") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1rdp7jfmnq2zlwlpgh6i41jjx3sd4z5mrm5hv0hkvlvd98npz8g5") (f (quote (("packet_handler_lost") ("packet_handler_compress") ("all" "packet_handler_lost" "packet_handler_compress"))))))

(define-public crate-cobalt-0.10.0 (c (n "cobalt") (v "0.10.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "15g6r7gn4gl35c5yas8h82xa2hzfxfgb06c5638dmdj6pz7gfdw3") (f (quote (("packet_handler_lost") ("packet_handler_compress") ("all" "packet_handler_lost" "packet_handler_compress"))))))

(define-public crate-cobalt-0.11.0 (c (n "cobalt") (v "0.11.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "17798w87p7s8vml48qa2dac3a50a6v6f9w6jb4fpfz2ns7fj8356") (f (quote (("packet_handler_lost") ("packet_handler_compress") ("all" "packet_handler_lost" "packet_handler_compress"))))))

(define-public crate-cobalt-0.13.0 (c (n "cobalt") (v "0.13.0") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0ianpw15hcdcfj4f1wr4i1k0gdg1bzfc6sybc20s6dbh2bv9wq7p") (f (quote (("packet_handler_lost") ("packet_handler_compress") ("all" "packet_handler_lost" "packet_handler_compress"))))))

(define-public crate-cobalt-0.20.0 (c (n "cobalt") (v "0.20.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0nzrcx71nclw1dmn13cgjydjm9b80gpcxv57l9pq15hzxi25l393")))

(define-public crate-cobalt-0.21.0 (c (n "cobalt") (v "0.21.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0y3dzg8wqvjknwg7b7syp8jy4cxgmbjs5f7f1spbpsmvjxn56hrk")))

(define-public crate-cobalt-0.22.0 (c (n "cobalt") (v "0.22.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0l5i550qnnndpva68rzww32vz0jw698f2ims6nnzm3d9r6zply17")))

