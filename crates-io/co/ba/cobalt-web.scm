(define-module (crates-io co ba cobalt-web) #:use-module (crates-io))

(define-public crate-cobalt-web-0.1.0 (c (n "cobalt-web") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0rp0gpwvwgmgscp1gmnpjgwjw553rf4v104q80zd4hs365mw0q30")))

(define-public crate-cobalt-web-0.2.0 (c (n "cobalt-web") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0im5z21ih1ajiq13jzbz1lwplafa20amsnpd8g36pzykckhqqiwf")))

(define-public crate-cobalt-web-0.3.0 (c (n "cobalt-web") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1djppfsw2ky13d4capgknmzah38pyhaa6f4n0hj5c8dqdzwnbhnp")))

(define-public crate-cobalt-web-0.4.0 (c (n "cobalt-web") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1qzyj4xw3cmp4his87n5vqs38q5kxspd4lfrvc69d91xv0x50j0k")))

(define-public crate-cobalt-web-0.5.0 (c (n "cobalt-web") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0lqs7n0lyvr1a1wajjhf9q5i1xlfxg7rnprjxsccsgazi8him871")))

(define-public crate-cobalt-web-0.6.0 (c (n "cobalt-web") (v "0.6.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0rlsjdm8j62rkjbm0nnwgj9nrl1gj9q1cjxf0x4w7p2sm8l2lxad")))

(define-public crate-cobalt-web-0.7.0 (c (n "cobalt-web") (v "0.7.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "02fk8y4kq82hgpmkla87vyglydk7650kcy0hhn92k00fz38yhmj8")))

(define-public crate-cobalt-web-0.8.0 (c (n "cobalt-web") (v "0.8.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "12spsrm98wr4bwmhyhyb9gg12wg7iczjgp33sf7gm58vk3bb970x")))

