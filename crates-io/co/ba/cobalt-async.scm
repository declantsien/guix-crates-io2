(define-module (crates-io co ba cobalt-async) #:use-module (crates-io))

(define-public crate-cobalt-async-0.1.0 (c (n "cobalt-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1g8j8ydzkq15wi73q8pqczvspjvan4vq2m902yxs7xfyk4jln8pl")))

(define-public crate-cobalt-async-0.2.0 (c (n "cobalt-async") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0y1bzgby8aqjpff9asbna0xfdfrizcqf4vjisqhfz6qypl3q35zf")))

(define-public crate-cobalt-async-0.3.0 (c (n "cobalt-async") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0h9dfwx1w13n3iq3iym4wnka50a2vgy58a19lckyfaim4q6y4fmd")))

(define-public crate-cobalt-async-0.4.0 (c (n "cobalt-async") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.66") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "crc32fast") (r "^1.3.2") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1818jgqpkwxj352gb3vbkphcwb68j5vj33j2f6gsl1r8i5qg562v") (f (quote (("full" "checksum") ("default") ("checksum" "crc32fast" "anyhow"))))))

