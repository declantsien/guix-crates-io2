(define-module (crates-io co in coin_toss) #:use-module (crates-io))

(define-public crate-coin_toss-0.1.0 (c (n "coin_toss") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0j74qwc7d55dv1f0kpn5i3dbfc135x85gii257m7v9z0bg8xb29z")))

(define-public crate-coin_toss-0.2.0 (c (n "coin_toss") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.0") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "14dppvzgmfavx0d93kk125zn86xkqif8mdw952nphbd0i8z22km4")))

