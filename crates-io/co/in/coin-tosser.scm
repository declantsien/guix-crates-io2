(define-module (crates-io co in coin-tosser) #:use-module (crates-io))

(define-public crate-coin-tosser-0.1.0 (c (n "coin-tosser") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0fjmn8jxqkyp274g8aslh510ikl8mr20ss3hm0nh55xaz28qx480")))

(define-public crate-coin-tosser-0.1.1 (c (n "coin-tosser") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0ljvnnfppm125rari2yqjr6hhkgxwqzj0jhpsjmpp4n5x6yv9a6i")))

(define-public crate-coin-tosser-0.1.2 (c (n "coin-tosser") (v "0.1.2") (d (list (d (n "cargo-readme") (r "^3.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1lfn1pvfa72yp9hcvbqbl8a4aabr94m15xf7ijsv8zx1hnfahji5")))

