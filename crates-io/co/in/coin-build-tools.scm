(define-module (crates-io co in coin-build-tools) #:use-module (crates-io))

(define-public crate-coin-build-tools-0.2.0 (c (n "coin-build-tools") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2") (d #t) (k 0)))) (h "0bvf8v78hbz9413z710k23brg097vyr66jc6bbv9i11zcnj8fnqi") (f (quote (("default"))))))

(define-public crate-coin-build-tools-0.2.2 (c (n "coin-build-tools") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2") (d #t) (k 0)))) (h "01135j8yygmdagpjcah39dg12za3yh4d32awi59mfamq81did6kk") (f (quote (("default"))))))

(define-public crate-coin-build-tools-0.2.4 (c (n "coin-build-tools") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2") (d #t) (k 0)))) (h "1wwmmr3b30cgk8bvx7k7rgqlk87vgxvksxzv5b9dqakq00a020dl") (f (quote (("default"))))))

(define-public crate-coin-build-tools-0.2.5 (c (n "coin-build-tools") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2") (d #t) (k 0)))) (h "1bhs8795k1n860jllal87z85fkrbvhfg5rv80ahnik68hk1v3fx5") (f (quote (("default"))))))

(define-public crate-coin-build-tools-0.2.6 (c (n "coin-build-tools") (v "0.2.6") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2") (d #t) (k 0)))) (h "0qjn3z1pvkk7h8nhm8irrpxssnzqhif4a75wbfnywymxcgbmaf3g") (f (quote (("default"))))))

