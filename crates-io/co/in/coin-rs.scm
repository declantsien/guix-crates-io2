(define-module (crates-io co in coin-rs) #:use-module (crates-io))

(define-public crate-coin-rs-0.1.0 (c (n "coin-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m7n84a34rjmcyap7fspymz3iah2345950kb2sysyxjdxahgq2ci")))

(define-public crate-coin-rs-0.2.0 (c (n "coin-rs") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w7hh8mv944nxh3wqzhwdrdbwchxmryy2qn5jg6wjmsscq6svsil")))

(define-public crate-coin-rs-0.3.0 (c (n "coin-rs") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hx1pd6nr9dbxzwzfhn6v7h305achmp7mgnvzg40hklhyyj3927d")))

(define-public crate-coin-rs-0.4.0 (c (n "coin-rs") (v "0.4.0") (d (list (d (n "clap") (r "^4.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17ds2n4cb2hdxqimgbzc5jbfis13siqyrr84wjqwdbx44m2yyj88")))

