(define-module (crates-io co in coin_cbc) #:use-module (crates-io))

(define-public crate-coin_cbc-0.1.0 (c (n "coin_cbc") (v "0.1.0") (d (list (d (n "coin_cbc_sys") (r "^0.1") (d #t) (k 0)))) (h "0zpinava1cah3l24azrdc5hpfkxj8q0401wksqqzikw1683mchqv")))

(define-public crate-coin_cbc-0.1.1 (c (n "coin_cbc") (v "0.1.1") (d (list (d (n "coin_cbc_sys") (r "^0.1") (d #t) (k 0)))) (h "1al55sarhwvv7wwdva7gp4msp03g1mxpvrjj22c3l132h75ljc7p")))

(define-public crate-coin_cbc-0.1.2 (c (n "coin_cbc") (v "0.1.2") (d (list (d (n "coin_cbc_sys") (r "^0.1") (d #t) (k 0)))) (h "1ffkbbrd1h6fzfkrsmkzw795rv4s5cnfyjwwqry3bivrz34iflg5")))

(define-public crate-coin_cbc-0.1.3 (c (n "coin_cbc") (v "0.1.3") (d (list (d (n "coin_cbc_sys") (r "^0.1") (d #t) (k 0)))) (h "0y0malv38sj5piabx7n2hq5n5pi36fspmdjgm19ng8h40vy2xza1")))

(define-public crate-coin_cbc-0.1.4 (c (n "coin_cbc") (v "0.1.4") (d (list (d (n "coin_cbc_sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0c10kil4vjyr10y28qbflxaqn3r44r5x3if1n29lm25i5b7gsbl9") (f (quote (("singlethread-cbc" "lazy_static") ("default" "singlethread-cbc"))))))

(define-public crate-coin_cbc-0.1.5 (c (n "coin_cbc") (v "0.1.5") (d (list (d (n "coin_cbc_sys") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)))) (h "12hfwvdjbbv71vssskxc49hmwyccq7rqnxw7ldl0gzbnjvqhkawp") (f (quote (("singlethread-cbc" "lazy_static") ("default" "singlethread-cbc"))))))

(define-public crate-coin_cbc-0.1.6 (c (n "coin_cbc") (v "0.1.6") (d (list (d (n "coin_cbc_sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1ifr24f02ag31i3zaavxfx0jlp159f7jcaksy291crkw9mdx3m08") (f (quote (("singlethread-cbc" "lazy_static") ("default" "singlethread-cbc") ("cbc-310"))))))

(define-public crate-coin_cbc-0.1.7 (c (n "coin_cbc") (v "0.1.7") (d (list (d (n "coin_cbc_sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1mvf1mmsca1gxkwgfa9nwv0jzp8q20bj99y81gf5lx3v5cjwb1lg") (f (quote (("singlethread-cbc" "lazy_static") ("default" "singlethread-cbc") ("cbc-310"))))))

(define-public crate-coin_cbc-0.1.8 (c (n "coin_cbc") (v "0.1.8") (d (list (d (n "coin_cbc_sys") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)))) (h "102pvkyi1gq39j2wwd69yfk97x2p8gwjljbii9h05bg7s9f080nn") (f (quote (("singlethread-cbc" "lazy_static") ("default" "singlethread-cbc") ("cbc-310"))))))

