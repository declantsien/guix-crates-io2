(define-module (crates-io co in coinbonmin-sys) #:use-module (crates-io))

(define-public crate-coinbonmin-sys-0.1.0 (c (n "coinbonmin-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "bonmin-src") (r "^0.1") (k 0)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)))) (h "1aa2hamzmmim5sp3dv6938ipslzdfx0v0gn4hr2drxhv4p93n345") (f (quote (("default" "bonmin-src/default"))))))

