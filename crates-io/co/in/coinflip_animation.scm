(define-module (crates-io co in coinflip_animation) #:use-module (crates-io))

(define-public crate-coinflip_animation-0.1.0 (c (n "coinflip_animation") (v "0.1.0") (h "1xwxiygkr415m163n80pfnbgx5f5plnl0g2gypvvbk2v5y0rl9x1")))

(define-public crate-coinflip_animation-0.1.1 (c (n "coinflip_animation") (v "0.1.1") (h "1hkfsxsa8xnvqxd1kdaz0m6bw19c8y00j9rsa89bihn3y718zsvl")))

(define-public crate-coinflip_animation-0.1.2 (c (n "coinflip_animation") (v "0.1.2") (h "1yp5xk2jwp1cfjaq3a5qg5i7rwh77bkrc7689zzl551brffgjvjb")))

(define-public crate-coinflip_animation-0.1.3 (c (n "coinflip_animation") (v "0.1.3") (h "1bbhan6lx0f3b94c6w0k3fl8whqdy6nx12js3v901mz93x0pyxi3")))

(define-public crate-coinflip_animation-0.1.4 (c (n "coinflip_animation") (v "0.1.4") (h "06gvjwalwazd1fih9414da4zi35p5zkjyx3qn51lkdb68jlrrk1d")))

(define-public crate-coinflip_animation-0.2.0 (c (n "coinflip_animation") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.4.4") (f (quote ("termination"))) (d #t) (k 0)))) (h "0b6vg084lgf9kxc92c1b4vazqjlacz0g5f1fz37barjdydismv72")))

(define-public crate-coinflip_animation-0.2.1 (c (n "coinflip_animation") (v "0.2.1") (d (list (d (n "ctrlc") (r "^3.4.4") (f (quote ("termination"))) (d #t) (k 0)))) (h "06hnc5m01m2ik9az8xnknbw4vfcx02yyvvqmmbhm08n4h60qq9am")))

