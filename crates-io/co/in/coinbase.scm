(define-module (crates-io co in coinbase) #:use-module (crates-io))

(define-public crate-coinbase-0.1.0 (c (n "coinbase") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.10.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "04128jdhi5m5vh3gms0xj1n107p1wmwkx1x6af529a6wlxzcl6sj")))

