(define-module (crates-io co in coinpaprika-api) #:use-module (crates-io))

(define-public crate-coinpaprika-api-0.1.0 (c (n "coinpaprika-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "reqwest-middleware") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest-retry") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1zlngwqmslarh6mqvl4w9wfpva1s60xq9rfg98bdrsamkhlj00d5")))

