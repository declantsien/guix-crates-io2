(define-module (crates-io co in coinbase-api) #:use-module (crates-io))

(define-public crate-coinbase-api-0.1.0 (c (n "coinbase-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y5rc877nn80kvby2k6aqzs8wfw15iwqdb6b3rghshlgf6s08n59")))

(define-public crate-coinbase-api-0.2.0 (c (n "coinbase-api") (v "0.2.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hmac") (r "^0.6") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "1mmvkqp0yjzfcsfn9hsr29vsilxxg28dv6qya4qa8ys3mvg54sq2")))

(define-public crate-coinbase-api-0.2.1 (c (n "coinbase-api") (v "0.2.1") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hmac") (r "^0.6") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "16c63k16g97jn87jadimiqmlp5w3d6ds52v9blv70344k93blnz2")))

