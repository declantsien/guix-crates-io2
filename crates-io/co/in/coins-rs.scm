(define-module (crates-io co in coins-rs) #:use-module (crates-io))

(define-public crate-coins-rs-0.1.0 (c (n "coins-rs") (v "0.1.0") (d (list (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x8j82x489nnbp815c9cdvgzl82yv22m70spq0bm2ji56gl4x646")))

(define-public crate-coins-rs-0.1.1 (c (n "coins-rs") (v "0.1.1") (d (list (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "091knn9m102k24zgscxx8l0ygw2mrzr9cns5338dzbfff3fhbdvn")))

