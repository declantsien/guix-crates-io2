(define-module (crates-io co in coinbase-bitcoin) #:use-module (crates-io))

(define-public crate-coinbase-bitcoin-0.1.0 (c (n "coinbase-bitcoin") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ihawfxzdpnddpm8mnvqfk74dc5i2pkyw9p28jqvzknwx6zzhn5y")))

(define-public crate-coinbase-bitcoin-0.1.1 (c (n "coinbase-bitcoin") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h9x9k6h19qmnr8vnzmf67skvnrqpsfialnryh8x3x9x6w1afvwb")))

