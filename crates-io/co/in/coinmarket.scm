(define-module (crates-io co in coinmarket) #:use-module (crates-io))

(define-public crate-coinmarket-0.1.0 (c (n "coinmarket") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cyd1ys4maanlza48ng2ajjl92184xf8dzhwwbg2rz4zagdhahnc") (y #t)))

(define-public crate-coinmarket-0.1.2 (c (n "coinmarket") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "119f5gb2bjgzvdfcvcnpcldhkwwi8bmwjxvhgzsz9z268dcrn2ks") (y #t)))

(define-public crate-coinmarket-0.2.0 (c (n "coinmarket") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nc2i43xazc7byxp2pmach1qfcz7cgvyxnswipdlfh8bqyxbx889") (y #t)))

(define-public crate-coinmarket-0.2.102 (c (n "coinmarket") (v "0.2.102") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kc5zn34asd3489l41rabcrpqs4sl24rp3fxn49p1qzxyvbiy8kl")))

(define-public crate-coinmarket-0.3.100 (c (n "coinmarket") (v "0.3.100") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f8c7d9lx27f7qy5rsw8ghj6fm9g27k9g2n17q3r1z2s5j195df8")))

(define-public crate-coinmarket-0.4.100 (c (n "coinmarket") (v "0.4.100") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03dnqqkkmc6zi3xiga4xx8ma98nq6f15lvr2fywja3ymn8zj8r1p") (y #t)))

(define-public crate-coinmarket-0.4.101 (c (n "coinmarket") (v "0.4.101") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03jh0xcm1pf16anhgwxschj4l32jhb8ay5pb1nnf5gyrny9v3zrx")))

