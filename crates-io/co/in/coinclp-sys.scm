(define-module (crates-io co in coinclp-sys) #:use-module (crates-io))

(define-public crate-coinclp-sys-0.2.2 (c (n "coinclp-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "clp-src") (r "^0.2") (f (quote ("clpsolver"))) (d #t) (k 0)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)))) (h "10n115wb0y8japnhhn524fjva5zp40xs1gqgx43nd8fx66dy9191") (f (quote (("default")))) (y #t)))

(define-public crate-coinclp-sys-0.2.3 (c (n "coinclp-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "clp-src") (r "^0.2") (f (quote ("clpsolver"))) (d #t) (k 0)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)))) (h "0jii7jqp69n5s9y7k4lir69p2r0svybaw5r96fxycml66w59d4i9") (f (quote (("default")))) (y #t)))

(define-public crate-coinclp-sys-0.2.4 (c (n "coinclp-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "clp-src") (r "^0.2") (f (quote ("clpsolver"))) (d #t) (k 0)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)))) (h "03rcfxvig54jzc9bs12br8ydbifdwvlj0wfjw6478g25678zf7yw") (f (quote (("default"))))))

(define-public crate-coinclp-sys-0.2.5 (c (n "coinclp-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "clp-src") (r "^0.2") (f (quote ("clpsolver"))) (d #t) (k 0)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)))) (h "1p812bwcwjyqfx9hxlzrn2kv3ybgc1r9rf5zylvzh1i2d8cq8q0v") (f (quote (("default"))))))

