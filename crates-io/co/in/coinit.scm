(define-module (crates-io co in coinit) #:use-module (crates-io))

(define-public crate-coinit-0.1.0 (c (n "coinit") (v "0.1.0") (d (list (d (n "windows") (r "^0.33.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "10dw5vmj7a71524fp11r8y0qxcmbagwpja8k2gjyn2l8iyzm4i8i")))

(define-public crate-coinit-0.1.1 (c (n "coinit") (v "0.1.1") (d (list (d (n "windows") (r "^0.34.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "0i1rkchg35gy57vvycs6swxfj9zxc38jhm2k94nhy66gk6ivply2")))

(define-public crate-coinit-0.1.2 (c (n "coinit") (v "0.1.2") (d (list (d (n "windows") (r "^0.35.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "0jh385vw0kl7wm58lh0jxnai4ns23iip5wp6dj724wk4151h543j")))

(define-public crate-coinit-0.1.3 (c (n "coinit") (v "0.1.3") (d (list (d (n "windows") (r "^0.36.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "159410r2n66bpiibrzbqx8fzpbq8pkrqg0v5mly308wdq60mg4fm")))

(define-public crate-coinit-0.1.4 (c (n "coinit") (v "0.1.4") (d (list (d (n "windows") (r "^0.36.1") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "1lfh5c2200pbd7i44r8aibm82vl7bpjziz1g2p1qindzvwy041l0")))

(define-public crate-coinit-0.1.5 (c (n "coinit") (v "0.1.5") (d (list (d (n "windows") (r "^0.37.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "1f5ji3l3wsa5n47x2b3vfml0lb33s15kn7mq6mz4lqvmrdy01ysl")))

(define-public crate-coinit-0.1.6 (c (n "coinit") (v "0.1.6") (d (list (d (n "windows") (r "^0.38.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "12gr61fn6mh45z21jikc6f9rcgyv1y2xg2q7vbh76l8h48bqbqqc")))

(define-public crate-coinit-0.1.7 (c (n "coinit") (v "0.1.7") (d (list (d (n "windows") (r "^0.39.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "1i6n4aqjqha0zdcnmpn220pn4vx8srsrkak0grsdc8rzk31z0gd2")))

(define-public crate-coinit-0.1.8 (c (n "coinit") (v "0.1.8") (d (list (d (n "windows") (r "^0.40.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "0xqpsr5gpam9wdli2kb89fw95k8p8pzggvwspyzsdqg1nppms0w2")))

(define-public crate-coinit-0.1.9 (c (n "coinit") (v "0.1.9") (d (list (d (n "windows") (r "^0.41.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "0cmjp6ik49kar4wn8fiqdai46r4bkfc2iyzd4nrvn44rkpnvklpj")))

(define-public crate-coinit-0.1.10 (c (n "coinit") (v "0.1.10") (d (list (d (n "windows") (r "^0.42.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "0j44l3g5bxjxa5xg4qr5i4lw45w93y9kr81z44jlkpjzjcn08a7d")))

(define-public crate-coinit-0.1.11 (c (n "coinit") (v "0.1.11") (d (list (d (n "windows") (r "^0.43.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "1sllsx5z22gfsdh8vq56icgnx0zvq1iwz28b6vycnvx2zg6h7lg7")))

(define-public crate-coinit-0.1.12 (c (n "coinit") (v "0.1.12") (d (list (d (n "windows") (r "^0.46.0") (f (quote ("Win32_System_Com"))) (d #t) (k 0)))) (h "0ljra50x78q2i3c4hrjqaz6w3imhjb0ckx9vb2zdr63j6apdczc7")))

