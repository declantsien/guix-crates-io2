(define-module (crates-io co in coinmarketcap) #:use-module (crates-io))

(define-public crate-coinmarketcap-0.1.0 (c (n "coinmarketcap") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1731s00bafjw5apf2hxjiicp9l37svks9psianhnnjsd1kzympjq")))

(define-public crate-coinmarketcap-0.2.0 (c (n "coinmarketcap") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vccjb4v3ii50a2fgbxhrycxaxj2h3p4c899481l5lmm88pkcyfz")))

(define-public crate-coinmarketcap-0.3.0 (c (n "coinmarketcap") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sw7gvwx6yz9qlpf17dcw4xvfmqklca21w31mxs0gkqk0m1gi1wi")))

