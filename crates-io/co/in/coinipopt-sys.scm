(define-module (crates-io co in coinipopt-sys) #:use-module (crates-io))

(define-public crate-coinipopt-sys-0.1.0 (c (n "coinipopt-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)) (d (n "ipopt-src") (r "^0.2") (o #t) (k 0)))) (h "0hdbr3hkmkv7zbfa5crb076z7izahfnvj2yd2vj6a0r7anzsix19") (f (quote (("default" "ipopt-src/default"))))))

