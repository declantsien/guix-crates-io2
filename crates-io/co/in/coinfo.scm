(define-module (crates-io co in coinfo) #:use-module (crates-io))

(define-public crate-coinfo-0.1.0 (c (n "coinfo") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^6.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sbs7234b0ia5l2b1nrfc13zfxj2343w3lk7c07sb8lhxidv5zim")))

(define-public crate-coinfo-0.1.1 (c (n "coinfo") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^6.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "16w72kp1s5nj9xl0agbnfgc61fzb0brac237hgnq2fmklvzlfjp1")))

