(define-module (crates-io co in coinlist) #:use-module (crates-io))

(define-public crate-coinlist-0.1.0 (c (n "coinlist") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "struct-tag") (r "^0.1.5") (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1kcby2kpcacrypk5vpbpw27jcijycxplr34ka616yv4rd686j0d4") (f (quote (("default") ("address32" "struct-tag/address32") ("address20" "struct-tag/address20"))))))

(define-public crate-coinlist-0.2.0 (c (n "coinlist") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("chrono" "url"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "struct-tag") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "06s06cgdamq2cll7n196rk0nfm9h5sc3ppr78qa22zhpqrv3m9w8") (f (quote (("default") ("address32" "struct-tag/address32") ("address20" "struct-tag/address20"))))))

