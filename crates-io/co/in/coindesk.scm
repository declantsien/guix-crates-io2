(define-module (crates-io co in coindesk) #:use-module (crates-io))

(define-public crate-coindesk-0.1.0 (c (n "coindesk") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)))) (h "18w30dq4iq0kqn7a89wf0wh8q5vik7zlr4xgw1kixkiqzp6m7g02")))

