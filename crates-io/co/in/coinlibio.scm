(define-module (crates-io co in coinlibio) #:use-module (crates-io))

(define-public crate-coinlibio-0.1.0 (c (n "coinlibio") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "18b2rndzpg18i5c7vna38i0aflsks36fs2i1bijzj09z2c8awaya")))

(define-public crate-coinlibio-0.1.1 (c (n "coinlibio") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "1imzrnpn6xrk0nhj9n49whw48znnx89paj9937wdmrhybwkcpkal")))

