(define-module (crates-io co in coincbc-sys) #:use-module (crates-io))

(define-public crate-coincbc-sys-0.2.3 (c (n "coincbc-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cbc-src") (r "^0.2") (f (quote ("cbcsolver"))) (d #t) (k 0)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)))) (h "13czxxrhfdsnzdc9c072645h5r240wpg0pw0jk9b9i55hcb3liji") (f (quote (("parallel" "cbc-src/parallel") ("default")))) (y #t)))

(define-public crate-coincbc-sys-0.2.4 (c (n "coincbc-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cbc-src") (r "^0.2") (f (quote ("cbcsolver"))) (d #t) (k 0)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)))) (h "0dh6dx1r4pa71ap80fn364q8alw0p3c1wgjysf8kj1xiifdav0vw") (f (quote (("parallel" "cbc-src/parallel") ("default"))))))

(define-public crate-coincbc-sys-0.2.5 (c (n "coincbc-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cbc-src") (r "^0.2") (f (quote ("cbcsolver"))) (d #t) (k 0)) (d (n "coin-build-tools") (r "^0.2") (d #t) (k 1)))) (h "0iisf3pmq69k2vzqa9i0x763kscdvwml5xxhy2lhmjvzs33w38d5") (f (quote (("parallel" "cbc-src/parallel") ("default"))))))

