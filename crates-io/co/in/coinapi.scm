(define-module (crates-io co in coinapi) #:use-module (crates-io))

(define-public crate-coinapi-0.1.0 (c (n "coinapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1y1mpwdy11196gphndc8zj0y62idsgcmp6gpl8k1da2alph8q409")))

(define-public crate-coinapi-0.1.1 (c (n "coinapi") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0b84ymyr26mv7av21h7d5z60vkmc0a6q6hx5rd8jf2mk7zcmwdqn")))

(define-public crate-coinapi-1.1.1 (c (n "coinapi") (v "1.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1ss0yhc6zj4dbirgx0jpkz57widgnnayn0x5754d60d86iqsshd7")))

(define-public crate-coinapi-1.1.2 (c (n "coinapi") (v "1.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0faylyh1a8fw4wlqyy5q3qi11cyl7fkacrf5lnfphrpp183kdsyv")))

