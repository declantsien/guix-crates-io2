(define-module (crates-io co in coin_cbc_sys) #:use-module (crates-io))

(define-public crate-coin_cbc_sys-0.1.0 (c (n "coin_cbc_sys") (v "0.1.0") (h "0ay0r7sfzwpq7w96zvw8l4d98y3zfp1qb51l5asdicsrcxcni8ql")))

(define-public crate-coin_cbc_sys-0.1.1 (c (n "coin_cbc_sys") (v "0.1.1") (h "1a9g93d25bqgxirdc5pxq5j827fyajkw8wl4ma2xlx7dck0w40j9")))

(define-public crate-coin_cbc_sys-0.1.2 (c (n "coin_cbc_sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0xhgq9x2zqzhmrnzyvancwym9h76yb1yqdk3bki293n3ppw1jmh8")))

