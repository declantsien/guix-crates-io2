(define-module (crates-io co in coinaddress) #:use-module (crates-io))

(define-public crate-coinaddress-1.0.2 (c (n "coinaddress") (v "1.0.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0q825b5api13zy603p3pb8y8fkgmcmcql01vnjqd250v7f66jyp2")))

(define-public crate-coinaddress-1.0.3 (c (n "coinaddress") (v "1.0.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0wl7ynkx7y5hfvx05q40mxf4r875sqnidjgkwarymk5va2lpyz9p")))

(define-public crate-coinaddress-1.1.0 (c (n "coinaddress") (v "1.1.0") (d (list (d (n "num") (r ">= 0.1.3") (d #t) (k 0)))) (h "0xmxkacvy06fab6bsfhr0g13nvjw0jl6r3y9yfznnadzhwrwx8k9")))

(define-public crate-coinaddress-1.1.1 (c (n "coinaddress") (v "1.1.1") (d (list (d (n "num") (r ">= 0.1.3") (d #t) (k 0)))) (h "1z59ijmv9gy1z0k0080sjnr57kdva5qczbi78s60z923rcjd9rp2")))

(define-public crate-coinaddress-1.1.2 (c (n "coinaddress") (v "1.1.2") (d (list (d (n "num") (r ">= 0.1.3") (d #t) (k 0)))) (h "03b2z8ian1d84i80fxjfn5mjby9lsnrqszf7lfd52zma55xfqc16")))

(define-public crate-coinaddress-1.1.3 (c (n "coinaddress") (v "1.1.3") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "num") (r ">= 0.1.3") (d #t) (k 0)) (d (n "sha2") (r "^0.5.1") (d #t) (k 0)))) (h "1ljzdnbprpfyrkv6sbwf5hlk3slri1g6vk21qy5cnckbkab6y8rp")))

