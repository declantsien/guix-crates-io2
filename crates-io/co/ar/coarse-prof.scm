(define-module (crates-io co ar coarse-prof) #:use-module (crates-io))

(define-public crate-coarse-prof-0.1.0 (c (n "coarse-prof") (v "0.1.0") (d (list (d (n "floating-duration") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b5641ma1xflq0v92y8x4dbpbzl3bl6gm8ah5kqpljlrx1dgfmqs")))

(define-public crate-coarse-prof-0.2.0 (c (n "coarse-prof") (v "0.2.0") (d (list (d (n "floating-duration") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f26nl6rk6bxs0ajammm5vc951x9x0zxn4dzr6g5wwy7xcn8frwx") (y #t)))

(define-public crate-coarse-prof-0.2.1 (c (n "coarse-prof") (v "0.2.1") (d (list (d (n "floating-duration") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b8693q4h07sd1n8whp8f7wm5iy4arnhwv6xarr6vmsrzcab0jfp") (y #t)))

(define-public crate-coarse-prof-0.2.2 (c (n "coarse-prof") (v "0.2.2") (d (list (d (n "floating-duration") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1sqrq6kfs2lx63ngp96d66m0pyaw8c8627vyygf1bakv6y6xvwng")))

(define-public crate-coarse-prof-0.2.3 (c (n "coarse-prof") (v "0.2.3") (d (list (d (n "floating-duration") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1cdyzldvrykdm59r2bwhjab4vrlzi3w7q83rmsv6idlsj326y7x8")))

(define-public crate-coarse-prof-0.2.4 (c (n "coarse-prof") (v "0.2.4") (d (list (d (n "floating-duration") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zzpg49vn5acp7qll26503b4sy57n49n9f28cd8i98259yvxralp")))

(define-public crate-coarse-prof-0.2.5 (c (n "coarse-prof") (v "0.2.5") (d (list (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0608hhpn9ibybnvwb77fivvffzm27g3q6agfnvqh2qv7vfv8gy23")))

(define-public crate-coarse-prof-0.2.6 (c (n "coarse-prof") (v "0.2.6") (d (list (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wdmps45wjma5z44929m84cbyclqgnvl58pvv2962iarsjqahzdg")))

