(define-module (crates-io co ar coarsetime-saturated) #:use-module (crates-io))

(define-public crate-coarsetime-saturated-0.1.21 (c (n "coarsetime-saturated") (v "0.1.21") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"wasi\"))") (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "wasi") (r "^0.11.0") (d #t) (t "cfg(target_os = \"wasi\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\"))") (k 0)))) (h "07hhy8mbykfxp5hw0mjmy7ccxvq5jb3waaph062y4jr6ahdq27jc") (f (quote (("nightly")))) (y #t)))

