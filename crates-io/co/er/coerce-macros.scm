(define-module (crates-io co er coerce-macros) #:use-module (crates-io))

(define-public crate-coerce-macros-0.1.0 (c (n "coerce-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yah5rpkx8blmzvczpjfzm7gxshvxffvgq690ljwmghx0kyqd802")))

(define-public crate-coerce-macros-0.2.0 (c (n "coerce-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1srvkqqja8ikmm635xk6xkv96q0cv9ixqf1nwzamaayapb0xk29w")))

