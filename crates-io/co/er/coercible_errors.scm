(define-module (crates-io co er coercible_errors) #:use-module (crates-io))

(define-public crate-coercible_errors-0.1.0 (c (n "coercible_errors") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "1ss2vrra3wllyvmxwvx95l4dw8f6f7f3r683220hb8jfgn0rkmnj") (f (quote (("example_generated") ("default" "error-chain" "example_generated"))))))

(define-public crate-coercible_errors-0.1.1 (c (n "coercible_errors") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "19h1p98yzvgj3mil346fh3rhz0mmp9fsjr35h6rdvb23c4bdrwca") (f (quote (("example_generated") ("default" "error-chain" "example_generated"))))))

(define-public crate-coercible_errors-0.1.2 (c (n "coercible_errors") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "1vqh28rhpd1phgpkm2id5l4gzbdfbm0jx0q6kvsp3pdq5ngi58yf") (f (quote (("example_generated") ("default" "error-chain" "example_generated"))))))

(define-public crate-coercible_errors-0.1.3 (c (n "coercible_errors") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "09m9li20q5n6fqjxqbfcwn0ckvgcf4m94w34ckpwvg9whqj7dz7w") (f (quote (("example_generated") ("default" "error-chain" "example_generated"))))))

(define-public crate-coercible_errors-0.1.4 (c (n "coercible_errors") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "1a782xkc255dawabaxd1cfxhadqsjj6rf0g262z144l75ij4cy5k") (f (quote (("example_generated") ("default" "error-chain" "example_generated"))))))

