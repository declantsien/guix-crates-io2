(define-module (crates-io co er coerce-k8s) #:use-module (crates-io))

(define-public crate-coerce-k8s-0.1.0 (c (n "coerce-k8s") (v "0.1.0") (d (list (d (n "coerce") (r "^0.8.0") (f (quote ("remote"))) (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.16.0") (f (quote ("v1_24" "api"))) (d #t) (k 0)) (d (n "kube") (r "^0.76.0") (f (quote ("client" "rustls-tls"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0x6drvs3vp399xjw3v4v3w4nppz82a19cx0sg6r5sqn8fhzb6k7c")))

(define-public crate-coerce-k8s-0.1.1 (c (n "coerce-k8s") (v "0.1.1") (d (list (d (n "coerce") (r "^0.8.0") (f (quote ("remote"))) (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15.0") (f (quote ("v1_24" "api"))) (d #t) (k 0)) (d (n "kube") (r "^0.74.0") (f (quote ("client" "rustls-tls"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "19fqrq8wg30is3ly4afbw4wgndsmzvlvgm0avrfs49agw8i8llpm")))

(define-public crate-coerce-k8s-0.1.2 (c (n "coerce-k8s") (v "0.1.2") (d (list (d (n "coerce") (r "^0.8.2") (f (quote ("remote"))) (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.15.0") (f (quote ("v1_24" "api"))) (d #t) (k 0)) (d (n "kube") (r "^0.74.0") (f (quote ("client" "rustls-tls"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "13bzpb3x3k6rgfz1lbaybyg0rlsndhvs04vwwymwf3f41aq0jqy1")))

(define-public crate-coerce-k8s-0.1.4 (c (n "coerce-k8s") (v "0.1.4") (d (list (d (n "coerce") (r "^0.8.7") (f (quote ("remote"))) (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17.0") (f (quote ("v1_24" "api"))) (d #t) (k 0)) (d (n "kube") (r "^0.78.0") (f (quote ("client" "rustls-tls"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0z1y85aiwirzk15wq06cc7jdkwl8mrxvfkfqjgqdg7wfy8lfgafi")))

(define-public crate-coerce-k8s-0.1.5 (c (n "coerce-k8s") (v "0.1.5") (d (list (d (n "coerce") (r "^0.8.8") (f (quote ("remote"))) (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17.0") (f (quote ("v1_24" "api"))) (d #t) (k 0)) (d (n "kube") (r "^0.78.0") (f (quote ("client" "rustls-tls"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1vi8ia91798garr7ni3xm2nzpv5dwx1jmib7ixi0pma5mwz71zax")))

(define-public crate-coerce-k8s-0.1.6 (c (n "coerce-k8s") (v "0.1.6") (d (list (d (n "coerce") (r "^0.8.9") (f (quote ("remote"))) (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17.0") (f (quote ("v1_24" "api"))) (d #t) (k 0)) (d (n "kube") (r "^0.78.0") (f (quote ("client" "rustls-tls"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ybcbxx8c9382b6w186vpz419iqla37xjjc5jvmhq22ww4q21bin")))

(define-public crate-coerce-k8s-0.1.7 (c (n "coerce-k8s") (v "0.1.7") (d (list (d (n "coerce") (r "^0.8.10") (f (quote ("remote"))) (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17.0") (f (quote ("v1_24" "api"))) (d #t) (k 0)) (d (n "kube") (r "^0.78.0") (f (quote ("client" "rustls-tls"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "05bw1qy0rh1mc519srf3q8ryd5afl4c5jgza2qarrn97x62i2jvr")))

(define-public crate-coerce-k8s-0.1.8 (c (n "coerce-k8s") (v "0.1.8") (d (list (d (n "coerce") (r "^0.8.11") (f (quote ("remote"))) (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.17.0") (f (quote ("v1_24" "api"))) (d #t) (k 0)) (d (n "kube") (r "^0.78.0") (f (quote ("client" "rustls-tls"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09gh0akaw3d604irb2y3paj9hc9vz7ng3hbjk99w5d2v223s38kv")))

