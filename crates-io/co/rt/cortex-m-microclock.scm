(define-module (crates-io co rt cortex-m-microclock) #:use-module (crates-io))

(define-public crate-cortex-m-microclock-0.1.0 (c (n "cortex-m-microclock") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.7") (f (quote ("critical-section-single-core"))) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (d #t) (k 2)) (d (n "defmt") (r "^0.3.4") (d #t) (k 2)) (d (n "defmt-rtt") (r "^0.4.0") (d #t) (k 2)) (d (n "fugit") (r "^0.3.7") (d #t) (k 0)) (d (n "panic-probe") (r "^0.3") (f (quote ("print-defmt"))) (d #t) (k 2)) (d (n "stm32f1") (r "^0.15.1") (f (quote ("stm32f103"))) (d #t) (k 2)))) (h "1jcdqjh5m76az25xyy3gyal6nr1yj4j7ir2zm4lxgkylxlpqw0p0")))

