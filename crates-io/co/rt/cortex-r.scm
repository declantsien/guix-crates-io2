(define-module (crates-io co rt cortex-r) #:use-module (crates-io))

(define-public crate-cortex-r-0.1.0 (c (n "cortex-r") (v "0.1.0") (h "0hvk30q1lmns994p1a1n383yc0snx6l8q7qpnxsxkc2mbzmim58h")))

(define-public crate-cortex-r-0.2.0 (c (n "cortex-r") (v "0.2.0") (h "10ah0klk7h4hmlfvqfr25w1xrdj7ljv7x2d4k0sks4ac6xmxn23w")))

