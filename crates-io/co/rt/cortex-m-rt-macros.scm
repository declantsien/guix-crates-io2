(define-module (crates-io co rt cortex-m-rt-macros) #:use-module (crates-io))

(define-public crate-cortex-m-rt-macros-0.1.0 (c (n "cortex-m-rt-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.15") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1pzp8mm9yan9bjzyvwd9vllwhq1vcwc0n3w7j8cbsd9ffkc4hg3d")))

(define-public crate-cortex-m-rt-macros-0.1.1 (c (n "cortex-m-rt-macros") (v "0.1.1") (d (list (d (n "cortex-m-rt") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.15") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.14.8") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0kszjq0gqqx5985hxdk5xqbpp2l7k6hr4xa8lmxrijkq9zqnlvpr")))

(define-public crate-cortex-m-rt-macros-0.1.2 (c (n "cortex-m-rt-macros") (v "0.1.2") (d (list (d (n "cortex-m-rt") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.4") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "06kx227089fi0fv9lz04217pxfdhv777hxk3jvxbc3darjnzsf9i")))

(define-public crate-cortex-m-rt-macros-0.1.3 (c (n "cortex-m-rt-macros") (v "0.1.3") (d (list (d (n "cortex-m-rt") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.13") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0adqk120rghlnb8xw8bikbkvm4yqliif3bsja9pp2bfrq1lb2x0l")))

(define-public crate-cortex-m-rt-macros-0.1.4 (c (n "cortex-m-rt-macros") (v "0.1.4") (d (list (d (n "cortex-m-rt") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.13") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "031k1k18idzxrjbxdmq1w3s0hz8gc9fsrrhzyfhyk1zs3v5r9ix6")))

(define-public crate-cortex-m-rt-macros-0.1.5 (c (n "cortex-m-rt-macros") (v "0.1.5") (d (list (d (n "cortex-m-rt") (r "^0.6.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.13") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0pz2mq4r7v3hd2qj56hly3rx5wavbazimvwzayqwrb70fcjnkbnp")))

(define-public crate-cortex-m-rt-macros-0.1.6 (c (n "cortex-m-rt-macros") (v "0.1.6") (d (list (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0dpr80mx9pm26lbbl6q7w87zhbphm13gxb7z9f9c85djxdcw6v9a")))

(define-public crate-cortex-m-rt-macros-0.1.7 (c (n "cortex-m-rt-macros") (v "0.1.7") (d (list (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "13cik0dkwid5vw12l979rk79wn1yk5hvvxmanyrr3rni70rhgcbj")))

(define-public crate-cortex-m-rt-macros-0.1.8 (c (n "cortex-m-rt-macros") (v "0.1.8") (d (list (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0iw66wl78al60j5px2dvqwasrwrvbjgr2la4sdhff1mszcm5c5s7")))

(define-public crate-cortex-m-rt-macros-0.6.15 (c (n "cortex-m-rt-macros") (v "0.6.15") (d (list (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0j8zm3bpmkjxf5npbqk7qn7wkq0r820v08m55y9ga9iy4i9amqy8")))

(define-public crate-cortex-m-rt-macros-0.7.0 (c (n "cortex-m-rt-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1iyki0wq8pj0qbjhw1mbq5njraihhyr7ydcbqzdzwg10dziz7xph")))

