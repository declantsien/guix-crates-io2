(define-module (crates-io co rt cortex) #:use-module (crates-io))

(define-public crate-cortex-0.1.0 (c (n "cortex") (v "0.1.0") (h "1wyq8b80y46dd7zf0mq45ar9cx52rmwddp2z5qyx8h7nk5gh35jv")))

(define-public crate-cortex-0.1.1 (c (n "cortex") (v "0.1.1") (h "0nsyc84in7m9aid4hjvr2bfzd6i3wril0q6r456ws8057k9340fa")))

(define-public crate-cortex-0.1.2 (c (n "cortex") (v "0.1.2") (h "14z87qi8v5vpss5ri19aasm2wahgw5wdvxgnj9s1igc18lds643d")))

(define-public crate-cortex-0.1.3 (c (n "cortex") (v "0.1.3") (h "1akyg7f6g5raybgfxq854xcqwhjqrz1w9zhk4i3r1h7awlpzw430")))

(define-public crate-cortex-0.1.4 (c (n "cortex") (v "0.1.4") (h "1ldmzziyxwm3vf7v38rim2gc6zjgpc785iv37cpjymhk53h9yv18")))

(define-public crate-cortex-0.1.5 (c (n "cortex") (v "0.1.5") (h "142qws4f24qrv7fywd8rj04g026mcffb5ghcgzw5m7zrhzwjqayj")))

