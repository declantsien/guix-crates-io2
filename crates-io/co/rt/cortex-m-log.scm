(define-module (crates-io co rt cortex-m-log) #:use-module (crates-io))

(define-public crate-cortex-m-log-0.1.0 (c (n "cortex-m-log") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "1vazgw38p0mg7yx8rcs1d97hgaimnax87mif09mks87hbs4pwr5n") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log"))))))

(define-public crate-cortex-m-log-0.1.1 (c (n "cortex-m-log") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "1k8frllaqnfdbz9s10f4zlggnj0kkfgcz77mf9kfzssyz8llsak9") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log"))))))

(define-public crate-cortex-m-log-0.2.0 (c (n "cortex-m-log") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "09zv8rxx1kaz3mxdpdds18p8dc36da14v4ixqichf52c6jp2jnwj") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log"))))))

(define-public crate-cortex-m-log-0.3.0 (c (n "cortex-m-log") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "1lgg0jy3hclzx74wnknl24msyzid5lifcz0fxbk426ck29zp3pxg") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

(define-public crate-cortex-m-log-0.3.1 (c (n "cortex-m-log") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "1s65jy0yzddswc70g4h63w41x7c07q7b79b4jh22faax3b6qyrw7") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

(define-public crate-cortex-m-log-0.3.2 (c (n "cortex-m-log") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0zhsl30j4awmj99lnn2855201hg407bh5kamzyj1p7j0pmj49am6") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

(define-public crate-cortex-m-log-0.3.3 (c (n "cortex-m-log") (v "0.3.3") (d (list (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "14plng2wzswabn1p8vn961l72axs54g7pfynz3bp35zbzlfa3fwg") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

(define-public crate-cortex-m-log-0.4.0 (c (n "cortex-m-log") (v "0.4.0") (d (list (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "02i13vnkva2r4grl0hl0dayhc7ykklaai865800n4a9a6skndmk2") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

(define-public crate-cortex-m-log-0.5.0 (c (n "cortex-m-log") (v "0.5.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0zxch0hmg82c3zfldwwb5525i4cn6fzif4y2pyw39n6x6z7n4jjq") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

(define-public crate-cortex-m-log-0.6.0 (c (n "cortex-m-log") (v "0.6.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "1gssjv2pxvw24npy6kp42fydl0v4rgv26wjp01a121a0qg2ifwhn") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

(define-public crate-cortex-m-log-0.6.1 (c (n "cortex-m-log") (v "0.6.1") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0n0lsv095y9cnxgh19alv58w73vqchsvhxhcn0w3s0nicpzam34p") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

(define-public crate-cortex-m-log-0.6.2 (c (n "cortex-m-log") (v "0.6.2") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0jmyz6ivz35rvky0dpza6wr2ah0m6mi6gviz4fbxs0z0n6f9aqqx") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

(define-public crate-cortex-m-log-0.7.0 (c (n "cortex-m-log") (v "0.7.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "1yh6m2jr6k1asadyywqarilnzbivc8v3wmjaabvssd2fmhp2s80f") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

(define-public crate-cortex-m-log-0.8.0 (c (n "cortex-m-log") (v "0.8.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "19xczbmxkn4mnyziva3xxdr4mnqhyynnsfhpbbscbll5zqrn1hl8") (f (quote (("semihosting" "cortex-m-semihosting") ("log-integration" "log") ("itm"))))))

