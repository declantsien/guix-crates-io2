(define-module (crates-io co rt cortex-m-rtfm-macros) #:use-module (crates-io))

(define-public crate-cortex-m-rtfm-macros-0.2.0 (c (n "cortex-m-rtfm-macros") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rtfm-syntax") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0jx8ky4kmcqvyj46ny9rnqa4b8m7fmmfg39wq7cafr6hgspn6dd0")))

(define-public crate-cortex-m-rtfm-macros-0.2.1 (c (n "cortex-m-rtfm-macros") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rtfm-syntax") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "13f5bb0id6xh0vcqrzj0ifk1gwcbdm7s7mv22pld18cs756srs86")))

(define-public crate-cortex-m-rtfm-macros-0.3.0 (c (n "cortex-m-rtfm-macros") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "rtfm-syntax") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "015s3i1jdrgga5ry9lwqsqj40bszx1c5wgw041aby9rvzqdjhf7d")))

(define-public crate-cortex-m-rtfm-macros-0.3.1 (c (n "cortex-m-rtfm-macros") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "rtfm-syntax") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "0wi1y0igzn2n3qsgyvna5y15ibb8n5c4n76zlmjs8i0ks7zhy19z")))

(define-public crate-cortex-m-rtfm-macros-0.3.2 (c (n "cortex-m-rtfm-macros") (v "0.3.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.6") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "rtfm-syntax") (r "^0.3.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "1ns94lil87ra6knsqnqsisff5r95igl0vxmksgnb3lzzdjfgwgn7")))

(define-public crate-cortex-m-rtfm-macros-0.4.0-beta.1 (c (n "cortex-m-rtfm-macros") (v "0.4.0-beta.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.6") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ys6jzaxxbylrxm61v5l1kd59pm4yyybdfm6rj660if6d1ajv9ni") (f (quote (("timer-queue")))) (y #t)))

(define-public crate-cortex-m-rtfm-macros-0.4.0-beta.2 (c (n "cortex-m-rtfm-macros") (v "0.4.0-beta.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.6") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zirdaizc0qcyw8nfr3qw2dj6gfza095s4gzxnh2av7jpxxr1ijd") (f (quote (("timer-queue")))) (y #t)))

(define-public crate-cortex-m-rtfm-macros-0.4.0-beta.3 (c (n "cortex-m-rtfm-macros") (v "0.4.0-beta.3") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.6") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0gcqcr0q3nl8b04w3ymn6pzvng4dakazgxszarhl6n5nzp6hdhm3") (f (quote (("timer-queue")))) (y #t)))

(define-public crate-cortex-m-rtfm-macros-0.4.0 (c (n "cortex-m-rtfm-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0y7scs9abk7rfa85rjyqsw8zz3yz47yqr7cn1bn58sxkmk1vva3f") (f (quote (("timer-queue")))) (y #t)))

(define-public crate-cortex-m-rtfm-macros-0.4.1 (c (n "cortex-m-rtfm-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1shz4r0h66c3f5803hl0i27bp00wf1l23idx702ixbmc10p8pfs0") (f (quote (("timer-queue"))))))

(define-public crate-cortex-m-rtfm-macros-0.4.2 (c (n "cortex-m-rtfm-macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1469swv1y7gqg0vdv6r98rx1mcj7hj571a9n6295wf339ipp6px2") (f (quote (("timer-queue") ("nightly"))))))

(define-public crate-cortex-m-rtfm-macros-0.4.3 (c (n "cortex-m-rtfm-macros") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (k 0)) (d (n "syn") (r "^0.15.23") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0i64b358jkj1j6wwdcmarihmf4nw1rdqfbql8vkyw2c3gvmswp7b") (f (quote (("timer-queue") ("nightly"))))))

(define-public crate-cortex-m-rtfm-macros-0.5.0-beta.1 (c (n "cortex-m-rtfm-macros") (v "0.5.0-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtfm-syntax") (r "^0.4.0-beta.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0qbasfvnl8skspgby4ngfacrazl56bykvrv9mg5zjm5rqh2yrl54") (f (quote (("homogeneous") ("heterogeneous")))) (y #t)))

(define-public crate-cortex-m-rtfm-macros-0.5.0 (c (n "cortex-m-rtfm-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtfm-syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1fvkx8k4g3bqwd3f9z71zai9p239xl1073vlvc59nkilzzv94866") (f (quote (("homogeneous") ("heterogeneous"))))))

(define-public crate-cortex-m-rtfm-macros-0.5.1 (c (n "cortex-m-rtfm-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtfm-syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "124b3m9a6w393q5aj6ziq5dkjf6m35lvyybz58ap16gziz43yjps") (f (quote (("homogeneous") ("heterogeneous"))))))

