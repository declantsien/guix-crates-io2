(define-module (crates-io co rt cortex-mpu) #:use-module (crates-io))

(define-public crate-cortex-mpu-0.1.0 (c (n "cortex-mpu") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.11") (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)))) (h "0lyy40qqmkr9azywzfvdvc2s0ygyfq8j27br1gywx6p64lchcqcr")))

(define-public crate-cortex-mpu-0.2.0 (c (n "cortex-mpu") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4.11") (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)))) (h "10sc1z1qh6k1di12sa6dyl7vh0gn2ilc2rwjcc5j1y75fpq6lkq0")))

(define-public crate-cortex-mpu-0.3.0 (c (n "cortex-mpu") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.4.11") (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)))) (h "0721a8671mxjgfll7py18cyyyyrbjzn59gbjj32r0g8089741kgk")))

(define-public crate-cortex-mpu-0.4.0 (c (n "cortex-mpu") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.4.11") (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)))) (h "145hc5yhk7vy1d004w3cn5miy8msnmaswh0qa3aj7g7wdnx2929v")))

