(define-module (crates-io co rt cortex-m-funnel) #:use-module (crates-io))

(define-public crate-cortex-m-funnel-0.1.0-alpha.1 (c (n "cortex-m-funnel") (v "0.1.0-alpha.1") (d (list (d (n "cortex-m-funnel-macros") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "1fvy838fpzln7p4gdpb90ivv55i9z2z8qqz408zsrici1dykak59") (f (quote (("release_max_level_warn") ("release_max_level_trace") ("release_max_level_off") ("release_max_level_info") ("release_max_level_error") ("release_max_level_debug") ("max_level_warn") ("max_level_trace") ("max_level_off") ("max_level_info") ("max_level_error") ("max_level_debug"))))))

