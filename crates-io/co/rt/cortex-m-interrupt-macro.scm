(define-module (crates-io co rt cortex-m-interrupt-macro) #:use-module (crates-io))

(define-public crate-cortex-m-interrupt-macro-0.1.0 (c (n "cortex-m-interrupt-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1hra6vihjd05q15nww09wjjp8nnbxy65vd2i6nv1brnjgqyicdzp")))

(define-public crate-cortex-m-interrupt-macro-0.2.0 (c (n "cortex-m-interrupt-macro") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "048b9dsndqjn5iiy2bfsp9cyyl98zcm20bmhpxqdxayrnwcg3x1k")))

