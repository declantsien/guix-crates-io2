(define-module (crates-io co rt cortex-m-asm) #:use-module (crates-io))

(define-public crate-cortex-m-asm-0.1.0 (c (n "cortex-m-asm") (v "0.1.0") (d (list (d (n "nightly-crimes") (r "^1.0.2") (d #t) (k 0)))) (h "0lgbmmz7y29ji934n63qahj5c4gx8rkmr535h69dwss4w032fw8p") (y #t)))

(define-public crate-cortex-m-asm-0.1.1 (c (n "cortex-m-asm") (v "0.1.1") (d (list (d (n "nightly-crimes") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "04vh94iafvdqm8g2rkgn5s3pvjljpwyh1arhbbbcrgi6cr8a9m4y") (f (quote (("do-crimes" "nightly-crimes") ("default"))))))

(define-public crate-cortex-m-asm-0.1.2 (c (n "cortex-m-asm") (v "0.1.2") (h "0ril3rcbkxrvgi4dkvag95lk0vkscg7w2jc51lssyi41s052zszq")))

