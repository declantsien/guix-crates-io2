(define-module (crates-io co rt cortex-m-rt-ld) #:use-module (crates-io))

(define-public crate-cortex-m-rt-ld-0.1.0 (c (n "cortex-m-rt-ld") (v "0.1.0") (h "046rym28rfndyi4zbf7yj1spwrk4s84g6njhj8772kc8vk1s6cvy")))

(define-public crate-cortex-m-rt-ld-0.1.1 (c (n "cortex-m-rt-ld") (v "0.1.1") (h "0vaw0cazjp6qy24j9kvfivq238gmxv0ynhzxsqrxpkwvmxsqinpl")))

