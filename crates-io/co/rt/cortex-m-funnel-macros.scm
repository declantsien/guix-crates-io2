(define-module (crates-io co rt cortex-m-funnel-macros) #:use-module (crates-io))

(define-public crate-cortex-m-funnel-macros-0.1.0-alpha.1 (c (n "cortex-m-funnel-macros") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0axrix7r0l8m2fzz1im9w4286cf49cz0iw31qfib5if06kimk4xc")))

