(define-module (crates-io co rt cortex-m-logger) #:use-module (crates-io))

(define-public crate-cortex-m-logger-0.0.0-alpha.0 (c (n "cortex-m-logger") (v "0.0.0-alpha.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (k 0)))) (h "1r980v3xhipdbg293nj72k7lypz7cmrcxjxnra7wqz9jkkwalh62") (f (quote (("default"))))))

(define-public crate-cortex-m-logger-0.0.0-alpha.1 (c (n "cortex-m-logger") (v "0.0.0-alpha.1") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (k 0)))) (h "007iwq63f2np1i5qxwb3i65wx649qagz974pc1xdggglvv745bn4") (f (quote (("semihosting" "cortex-m-semihosting") ("default"))))))

