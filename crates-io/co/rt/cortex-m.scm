(define-module (crates-io co rt cortex-m) #:use-module (crates-io))

(define-public crate-cortex-m-0.1.0 (c (n "cortex-m") (v "0.1.0") (d (list (d (n "volatile-register") (r "^0.1.0") (d #t) (k 0)))) (h "1453b1r30kj27bxigly5rp0fbhnl0i95vak706xj8na96wwqy7sn") (y #t)))

(define-public crate-cortex-m-0.1.1 (c (n "cortex-m") (v "0.1.1") (d (list (d (n "volatile-register") (r "^0.1.0") (d #t) (k 0)))) (h "0x90w3lqwnkvc2gyx26j0vzmx09y23isg41566rjkispdahkdlvr") (y #t)))

(define-public crate-cortex-m-0.1.2 (c (n "cortex-m") (v "0.1.2") (d (list (d (n "volatile-register") (r "^0.1.0") (d #t) (k 0)))) (h "1m7yjnk5b69bdqaqvkjwy1zny9yvy2i7wy73b19p0l35w7y1lh1g")))

(define-public crate-cortex-m-0.1.3 (c (n "cortex-m") (v "0.1.3") (d (list (d (n "volatile-register") (r "^0.1.0") (d #t) (k 0)))) (h "0jhbaxzylnc3d1bj9xs6prr3ckia27ya0dnvbhfz8mdbcrkbp5gm")))

(define-public crate-cortex-m-0.1.4 (c (n "cortex-m") (v "0.1.4") (d (list (d (n "volatile-register") (r "^0.1.0") (d #t) (k 0)))) (h "0h9l66hvflbkw2jryckscmr28khk6h87f9ipwpar20ncq04kb3cw")))

(define-public crate-cortex-m-0.1.5 (c (n "cortex-m") (v "0.1.5") (d (list (d (n "volatile-register") (r "^0.1.0") (d #t) (k 0)))) (h "1cdn8mq37a46sjyinzv8a2nxhdzdfsx6gp67w77gkjhravx0d0b2")))

(define-public crate-cortex-m-0.1.6 (c (n "cortex-m") (v "0.1.6") (d (list (d (n "volatile-register") (r "^0.1.0") (d #t) (k 0)))) (h "0bf68nvh8h0qgrm4h5l7iwl4hqy7hzvs6hmyan7586rrhxpq8dpn")))

(define-public crate-cortex-m-0.2.0 (c (n "cortex-m") (v "0.2.0") (d (list (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1190pifz9sk6sh0njf4wcjscg6vcvvw252h04vk7h5m0bccbx632") (y #t)))

(define-public crate-cortex-m-0.2.1 (c (n "cortex-m") (v "0.2.1") (d (list (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0g8iifc4pravw1zbh16fq8xqna9fb7njga7zif1hl20blifp2ynp") (y #t)))

(define-public crate-cortex-m-0.2.2 (c (n "cortex-m") (v "0.2.2") (d (list (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "05b77h6n7zgyy01h8yzlb2s6iyvzzlq9kjm80rfxs1mip26wj936") (y #t)))

(define-public crate-cortex-m-0.2.3 (c (n "cortex-m") (v "0.2.3") (d (list (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0gpvbkr5jwvq5918qkz83kc5l6qk84ha7v7km37kamzsqzg5q6gz") (y #t)))

(define-public crate-cortex-m-0.2.4 (c (n "cortex-m") (v "0.2.4") (d (list (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0j0mxxmvcnzf551jqysvl6a17ylizr6m5m556sk9g10j48rihkwn") (y #t)))

(define-public crate-cortex-m-0.2.5 (c (n "cortex-m") (v "0.2.5") (d (list (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1nr8arakrxyl573li8l8iyhasiszikdrgff0kf17lb5b73dh5w2n") (y #t)))

(define-public crate-cortex-m-0.2.6 (c (n "cortex-m") (v "0.2.6") (d (list (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1w0bf5w5c47kl8hfjr1i3i4d6ilhhd5dmrfnr4yba4z50pfzblgi")))

(define-public crate-cortex-m-0.2.7 (c (n "cortex-m") (v "0.2.7") (d (list (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0546ppzg66h5n4xdn4dyy9v69jdqb656pffymxzxbrlc6rl46ajl")))

(define-public crate-cortex-m-0.2.8 (c (n "cortex-m") (v "0.2.8") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1s03q4airmi94knd65brkbchbvfy6y093z4icb06jhkw20k5m256") (y #t)))

(define-public crate-cortex-m-0.2.9 (c (n "cortex-m") (v "0.2.9") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0zg7vv3d8jv4acksagf5cmmiyp37fa4qrqvkbdp1l5wzg280b6vg")))

(define-public crate-cortex-m-0.2.10 (c (n "cortex-m") (v "0.2.10") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1cfq6mhzpg4vlzqxbvl6h1q8rbs19n0009qamy833nnck0xjskya")))

(define-public crate-cortex-m-0.2.11 (c (n "cortex-m") (v "0.2.11") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "07x8n5z16krck2vhgicjqgzwn5k3mv913a06297yvv1nmxa268l5")))

(define-public crate-cortex-m-0.3.0 (c (n "cortex-m") (v "0.3.0") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "12nvcbp8q1cn4bb6k9n9aik1p4afy4c1h9ywjpspsawnh22ks130")))

(define-public crate-cortex-m-0.3.1 (c (n "cortex-m") (v "0.3.1") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "13kkhz4hvg2wlv6sm5ai75zxvsp8yr7aca1xdlgch0rlyahkqmad")))

(define-public crate-cortex-m-0.2.12 (c (n "cortex-m") (v "0.2.12") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.1.3") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1ik4rb5vs1i3kyl2n848f7mxnnszl2i0s9j1v6fs7qbhrb6syj98")))

(define-public crate-cortex-m-0.1.7 (c (n "cortex-m") (v "0.1.7") (d (list (d (n "volatile-register") (r "^0.1.0") (d #t) (k 0)))) (h "1p1j539nj5ig9snifiljlf9v038c9mcp4dmnvziarcbq0x6k76yy")))

(define-public crate-cortex-m-0.4.0 (c (n "cortex-m") (v "0.4.0") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0basxcj1zdnpgv0zjpbfbd2hx0yh926sgyvg34j4h5bjd3f93r5i") (f (quote (("cm7-r0p1"))))))

(define-public crate-cortex-m-0.4.1 (c (n "cortex-m") (v "0.4.1") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0q8fzv6dm2g2id8x8cg14hjzn0123x6gx0nrhs5wqz89jvcqi9yy") (f (quote (("cm7-r0p1"))))))

(define-public crate-cortex-m-0.4.2 (c (n "cortex-m") (v "0.4.2") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "02dhcd6ni2sbqqdv29y1fyw0sv4qxjigdhg2prn02pg8miadnwzb") (f (quote (("cm7-r0p1"))))))

(define-public crate-cortex-m-0.4.3 (c (n "cortex-m") (v "0.4.3") (d (list (d (n "aligned") (r "^0.1.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "untagged-option") (r "^0.1.1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1xxpx666mc6mh23f4q65rxizg2yqikmzpgljc5xbwjilhcgdsr18") (f (quote (("cm7-r0p1"))))))

(define-public crate-cortex-m-0.5.0 (c (n "cortex-m") (v "0.5.0") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.10") (d #t) (k 1)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0gp91ap9wvq498r7cxd8649liyp7w7wwy7j0fqk4bji2x7srmyyb") (f (quote (("inline-asm") ("cm7-r0p1"))))))

(define-public crate-cortex-m-0.5.1 (c (n "cortex-m") (v "0.5.1") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.10") (d #t) (k 1)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "01r4k9pp3n91cai42ky5qz8250q5l4f9qw9ikng171jqqcdw9npm") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn") ("cm7-r0p1"))))))

(define-public crate-cortex-m-0.5.2 (c (n "cortex-m") (v "0.5.2") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.10") (d #t) (k 1)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0929rll2f7z7k2bdm2imnnn30vw6drmis2rsw3d8j9llz1ab9klk") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn") ("cm7-r0p1"))))))

(define-public crate-cortex-m-0.5.3 (c (n "cortex-m") (v "0.5.3") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.10") (d #t) (k 1)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1qn5nq4rqg1sgrvksw4i1jmhasqg54mnhhb0yawx76jzz0l6l3x3") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn") ("cm7-r0p1"))))))

(define-public crate-cortex-m-0.5.4 (c (n "cortex-m") (v "0.5.4") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.10") (d #t) (k 1)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "18mqg155wgj8l9lbw8155nsnvg04nxd1wh4v4flsv96f90hhgzm0") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn") ("cm7-r0p1"))))))

(define-public crate-cortex-m-0.1.8 (c (n "cortex-m") (v "0.1.8") (d (list (d (n "volatile-register") (r "^0.1.0") (d #t) (k 0)))) (h "0plq0761s95r7prvbabb5z6i8rhc0bx4b2gsvsvwrwi9k2ddxx9x")))

(define-public crate-cortex-m-0.5.5 (c (n "cortex-m") (v "0.5.5") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0dbcc1rbck7p7n3ihvbrlkpvzvm9alyh0h9djw6fkybqyh14virm") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn") ("cm7-r0p1")))) (y #t)))

(define-public crate-cortex-m-0.5.6 (c (n "cortex-m") (v "0.5.6") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1d4c4pw6xz4wl6ch2b6mrjj4i37r4mw60k3xx9zn9jr8jj1vm9lv") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn") ("cm7-r0p1"))))))

(define-public crate-cortex-m-0.5.7 (c (n "cortex-m") (v "0.5.7") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1dfws66zdzhdh603iy2p60dz3zgm19y6zd0qwkm0x6qybff1jws5") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn") ("cm7-r0p1"))))))

(define-public crate-cortex-m-0.5.8 (c (n "cortex-m") (v "0.5.8") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1ixfmbxz1yn6paj3y7a22xlsw4ji6r3k6d7w8wd7h5n21x51dcns") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn") ("cm7-r0p1"))))))

(define-public crate-cortex-m-0.6.0 (c (n "cortex-m") (v "0.6.0") (d (list (d (n "aligned") (r "^0.3.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0slmw2qd9jajyva9qpccf4fs988gkbdpgjdzd1bbczf5zlcqghgk") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn") ("cm7-r0p1"))))))

(define-public crate-cortex-m-0.5.9 (c (n "cortex-m") (v "0.5.9") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex_m_0_6") (r "^0.6.0") (d #t) (k 0) (p "cortex-m")) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "038hn4jml1m09i5zfgxrqrr3phlr88m0gbm1jggpll7rnja4xvih") (f (quote (("inline-asm") ("const-fn" "bare-metal/const-fn") ("cm7-r0p1")))) (y #t)))

(define-public crate-cortex-m-0.5.10 (c (n "cortex-m") (v "0.5.10") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex_m_0_6") (r "^0.6.0") (d #t) (k 0) (p "cortex-m")) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0m55wyhlykhr0w4ccrbv35c819fv8749hdnyg6ar81l33sd1a2rw") (f (quote (("inline-asm" "cortex_m_0_6/inline-asm") ("const-fn" "bare-metal/const-fn" "cortex_m_0_6/const-fn") ("cm7-r0p1" "cortex_m_0_6/cm7-r0p1"))))))

(define-public crate-cortex-m-0.6.1 (c (n "cortex-m") (v "0.6.1") (d (list (d (n "aligned") (r "^0.3.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1jnv4fj4rddbfxdls3c0pndxsji190bmxrr5sj73ggcv6zya4p8l") (f (quote (("inline-asm") ("const-fn") ("cm7-r0p1")))) (l "cortex-m")))

(define-public crate-cortex-m-0.6.2 (c (n "cortex-m") (v "0.6.2") (d (list (d (n "aligned") (r "^0.3.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0qy73yh0dplzmr0rpi9d9qd3wz2naz74iw760ikrjjfxpcpr8m19") (f (quote (("inline-asm") ("const-fn") ("cm7-r0p1")))) (l "cortex-m")))

(define-public crate-cortex-m-0.6.3 (c (n "cortex-m") (v "0.6.3") (d (list (d (n "aligned") (r "^0.3.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0g673nncjz59cgpyiakd5qr8njc540bd4zvgk16sfscnr4q9ks9b") (f (quote (("inline-asm") ("const-fn") ("cm7-r0p1")))) (l "cortex-m")))

(define-public crate-cortex-m-0.6.4 (c (n "cortex-m") (v "0.6.4") (d (list (d (n "aligned") (r "^0.3.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0qw2ff3lp157510c5c7smnhja1r1hwapzvbx8q6c0dm6zgmazkc8") (f (quote (("inline-asm") ("const-fn") ("cm7-r0p1")))) (l "cortex-m")))

(define-public crate-cortex-m-0.7.0 (c (n "cortex-m") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1381yrdhl2l9nc237qxj7agvc4d5fwm5v0y872855c47yd9i9yxp") (f (quote (("linker-plugin-lto") ("inline-asm") ("cm7-r0p1")))) (l "cortex-m")))

(define-public crate-cortex-m-0.6.5-alpha (c (n "cortex-m") (v "0.6.5-alpha") (d (list (d (n "aligned") (r "^0.3.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "cortex_m_0_7") (r "^0.7.0") (d #t) (k 0) (p "cortex-m")) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0g84x29f3jspsnn992spq8bfld4m4g1f535sbr2gnb5kp3xbvd9g") (f (quote (("inline-asm" "cortex_m_0_7/inline-asm") ("const-fn") ("cm7-r0p1" "cortex_m_0_7/cm7-r0p1"))))))

(define-public crate-cortex-m-0.6.5 (c (n "cortex-m") (v "0.6.5") (d (list (d (n "aligned") (r "^0.3.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "cortex_m_0_7") (r "^0.7.0") (d #t) (k 0) (p "cortex-m")) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0wlkjrf1mpqrdlhxqamjc06gwlzxfi555nbyg8xdlvsim2k35ns8") (f (quote (("inline-asm" "cortex_m_0_7/inline-asm") ("const-fn") ("cm7-r0p1" "cortex_m_0_7/cm7-r0p1"))))))

(define-public crate-cortex-m-0.7.1 (c (n "cortex-m") (v "0.7.1") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0wf7xgf4jbdk4aklkvn08021h0dqz67s862jwifh4mpwpyl5ddx0") (f (quote (("linker-plugin-lto") ("inline-asm") ("cm7-r0p1")))) (l "cortex-m")))

(define-public crate-cortex-m-0.6.6 (c (n "cortex-m") (v "0.6.6") (d (list (d (n "aligned") (r "^0.3.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "cortex_m_0_7") (r "^0.7.0") (d #t) (k 0) (p "cortex-m")) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0wd10rqskjqk2bgj3p9vvlp6synsp3n0y9gnh7hznfhfz5jn35v8") (f (quote (("inline-asm" "cortex_m_0_7/inline-asm") ("const-fn") ("cm7-r0p1" "cortex_m_0_7/cm7-r0p1"))))))

(define-public crate-cortex-m-0.6.7 (c (n "cortex-m") (v "0.6.7") (d (list (d (n "aligned") (r "^0.3.1") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "cortex_m_0_7") (r "^0.7.0") (d #t) (k 0) (p "cortex-m")) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1f8vdz2r053irkhzvjaysh07n0zzs0ac50mmp5in59f60w5k0xch") (f (quote (("inline-asm" "cortex_m_0_7/inline-asm") ("const-fn") ("cm7-r0p1" "cortex_m_0_7/cm7-r0p1"))))))

(define-public crate-cortex-m-0.5.11 (c (n "cortex-m") (v "0.5.11") (d (list (d (n "aligned") (r "^0.2.0") (d #t) (k 0)) (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex_m_0_6") (r "^0.6.0") (d #t) (k 0) (p "cortex-m")) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "08nnq6a0iciyi5hzc07qj3kzq62ihxh8d7fx73vwmjnsyif1m5sr") (f (quote (("inline-asm" "cortex_m_0_6/inline-asm") ("const-fn" "bare-metal/const-fn" "cortex_m_0_6/const-fn") ("cm7-r0p1" "cortex_m_0_6/cm7-r0p1"))))))

(define-public crate-cortex-m-0.7.2 (c (n "cortex-m") (v "0.7.2") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0svdkm1zif1ini87zhyhx754vzzlh9h5fahya7dx08yw3c622fk4") (f (quote (("linker-plugin-lto") ("inline-asm") ("cm7-r0p1")))) (l "cortex-m")))

(define-public crate-cortex-m-0.7.3 (c (n "cortex-m") (v "0.7.3") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1cw3nyip3s81r6qaa3azrb0654jxw465j5fm126fqja48bpikj9a") (f (quote (("linker-plugin-lto") ("inline-asm") ("cm7-r0p1")))) (l "cortex-m")))

(define-public crate-cortex-m-0.7.4 (c (n "cortex-m") (v "0.7.4") (d (list (d (n "bare-metal") (r "^0.2.4") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "09n8wwpvp69rj9fr3qs1wxwcd67af76jbb1l1jx4x8bwhrz9dzrp") (f (quote (("std") ("linker-plugin-lto") ("inline-asm") ("cm7-r0p1" "cm7") ("cm7")))) (l "cortex-m")))

(define-public crate-cortex-m-0.7.5 (c (n "cortex-m") (v "0.7.5") (d (list (d (n "bare-metal") (r "^0.2.4") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "09m17rfib13fvhc2ha1crryfhzgn5ra9xm9ry9slyvx89and886d") (f (quote (("std") ("linker-plugin-lto") ("inline-asm") ("cm7-r0p1" "cm7") ("cm7")))) (l "cortex-m")))

(define-public crate-cortex-m-0.7.6 (c (n "cortex-m") (v "0.7.6") (d (list (d (n "bare-metal") (r "^0.2.4") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1q79rq71mgw1inc7dpmyhczjl7s1171lsp37z4wzvzaqlhlqd1bh") (f (quote (("std") ("linker-plugin-lto") ("inline-asm") ("critical-section-single-core" "critical-section/restore-state-bool") ("cm7-r0p1" "cm7") ("cm7")))) (l "cortex-m")))

(define-public crate-cortex-m-0.7.7 (c (n "cortex-m") (v "0.7.7") (d (list (d (n "bare-metal") (r "^0.2.4") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1fbca698v4gv57mv5fc48jrz8wcy6sv675n6fsrsah4qykc11ilf") (f (quote (("std") ("linker-plugin-lto") ("inline-asm") ("critical-section-single-core" "critical-section/restore-state-bool") ("cm7-r0p1" "cm7") ("cm7")))) (l "cortex-m")))

