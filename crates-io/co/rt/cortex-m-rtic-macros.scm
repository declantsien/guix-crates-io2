(define-module (crates-io co rt cortex-m-rtic-macros) #:use-module (crates-io))

(define-public crate-cortex-m-rtic-macros-0.5.0 (c (n "cortex-m-rtic-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0anjcssc1mpv5ghicw5bwgyyp2q00i6zkalnzndxb07cnc0rxqi9") (f (quote (("homogeneous") ("heterogeneous"))))))

(define-public crate-cortex-m-rtic-macros-0.5.1 (c (n "cortex-m-rtic-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0an5klsax91fn3glqxr80r0mr2bxzd19fi8ys839hyhm06ar975p") (f (quote (("homogeneous") ("heterogeneous"))))))

(define-public crate-cortex-m-rtic-macros-0.5.2 (c (n "cortex-m-rtic-macros") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1iqrn5inxp5x0j06pgsvqabxd6sj9pa7h6p2q0w30dshjm66l6ls") (f (quote (("homogeneous") ("heterogeneous"))))))

(define-public crate-cortex-m-rtic-macros-0.6.0-alpha.0 (c (n "cortex-m-rtic-macros") (v "0.6.0-alpha.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-alpha.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gd1cbmr14gqwmy28i1l7rvc2hxkz7449hs6l4340mg372v98922")))

(define-public crate-cortex-m-rtic-macros-0.6.0-alpha.1 (c (n "cortex-m-rtic-macros") (v "0.6.0-alpha.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-alpha.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1jg4512pn1xi7gc24lb05bzirmgzz9732m4a5cb1a3f6s4zrmmgy")))

(define-public crate-cortex-m-rtic-macros-0.6.0-alpha.2 (c (n "cortex-m-rtic-macros") (v "0.6.0-alpha.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0h34yn0ais4x3h3ym3f0czklxcj4z2vn0h8s5k6kqnsyq6m3yzka")))

(define-public crate-cortex-m-rtic-macros-0.6.0-alpha.4 (c (n "cortex-m-rtic-macros") (v "0.6.0-alpha.4") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-alpha.3") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "134l8zvnrqzwzs5mdak7h60maal6hbb0f8p2z07q3a8mpsj0pkhh")))

(define-public crate-cortex-m-rtic-macros-0.5.3 (c (n "cortex-m-rtic-macros") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0n85hhmval8h2cq6s29i8i2qyf1hllk2fmh3gzk1ansik7d4x1yc") (f (quote (("homogeneous") ("heterogeneous"))))))

(define-public crate-cortex-m-rtic-macros-0.6.0-alpha.5 (c (n "cortex-m-rtic-macros") (v "0.6.0-alpha.5") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-alpha.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1m7pqw9qxb2j217zm5ipsc1xshx02k2xy2qzg339pghl3mxw8dy5")))

(define-public crate-cortex-m-rtic-macros-0.6.0-rc.1 (c (n "cortex-m-rtic-macros") (v "0.6.0-rc.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ffxg4ix0lnv9yry8k8h4aqqp95s8f2k0yp914sqv2xqll20vqks")))

(define-public crate-cortex-m-rtic-macros-0.6.0-rc.2 (c (n "cortex-m-rtic-macros") (v "0.6.0-rc.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0m0c6d6fwk3dx8dqfa0hgf9bq2v6k2x6dn6n7wik29fbxflq8dv8")))

(define-public crate-cortex-m-rtic-macros-0.6.0-rc.3 (c (n "cortex-m-rtic-macros") (v "0.6.0-rc.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0yzrrv01i7j3ac7kj3031yrv73l38ss33sxzdifyb5hpcfddcrdr")))

(define-public crate-cortex-m-rtic-macros-0.6.0-rc.4 (c (n "cortex-m-rtic-macros") (v "0.6.0-rc.4") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14mfdhyq0qa0yq9ka19avcd9vjmy9rngz921krydxs0vjpiwv50p")))

(define-public crate-cortex-m-rtic-macros-1.0.0 (c (n "cortex-m-rtic-macros") (v "1.0.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1y3cgb2aym7bhy3i22ck0add849djpc1pw2q0rydzy4wcg8mb8vv")))

(define-public crate-cortex-m-rtic-macros-1.1.0 (c (n "cortex-m-rtic-macros") (v "1.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07j7y62h9hmgjsk25h6kkwaykwf3c3bxmijfk0qhj4gwq2zx2wbn") (f (quote (("debugprint")))) (y #t)))

(define-public crate-cortex-m-rtic-macros-1.1.1 (c (n "cortex-m-rtic-macros") (v "1.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0yd0v0zaynqxp9qif5sxpbn3nl9gh4v7amwsw3s4l3qfsq7azsqj") (f (quote (("debugprint"))))))

(define-public crate-cortex-m-rtic-macros-1.1.2 (c (n "cortex-m-rtic-macros") (v "1.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0g3pfp7ic94gp6ylvsl4p0ajn0f3g196ywb4bw24qi0fwqvcbkl9") (f (quote (("debugprint"))))))

(define-public crate-cortex-m-rtic-macros-1.1.3 (c (n "cortex-m-rtic-macros") (v "1.1.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0j1mfl90fjkm6vbxjvgdcn09pfvi0cy5fskbj50d4wbla4446pfh") (f (quote (("debugprint"))))))

(define-public crate-cortex-m-rtic-macros-1.1.4 (c (n "cortex-m-rtic-macros") (v "1.1.4") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1q1r3cbhrb530inhl3qs33ibzricv9vm1a4vkzwq4zlx5d0ibi0z") (f (quote (("debugprint"))))))

(define-public crate-cortex-m-rtic-macros-1.1.5 (c (n "cortex-m-rtic-macros") (v "1.1.5") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01w69qcn4mkkyfxfc6mlnmx51rk6jnzyjcxcf37w3gjlxx2rd3ly") (f (quote (("debugprint"))))))

(define-public crate-cortex-m-rtic-macros-1.1.6 (c (n "cortex-m-rtic-macros") (v "1.1.6") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1iylxkmlvfghs32lqnxar8h6q90v1a55qvjj56fpa74hraql1yzf") (f (quote (("debugprint"))))))

