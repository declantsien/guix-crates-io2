(define-module (crates-io co rt cortex-m-async) #:use-module (crates-io))

(define-public crate-cortex-m-async-0.1.0 (c (n "cortex-m-async") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (k 0)))) (h "07b06dmk8gqxl3ni90faa6imqxhpdb2cgnas4q8n424fads88px7")))

