(define-module (crates-io co rt cortex-m-interrupt) #:use-module (crates-io))

(define-public crate-cortex-m-interrupt-0.1.0 (c (n "cortex-m-interrupt") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-interrupt-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "stm32f1xx-hal") (r "^0.9") (f (quote ("stm32f107"))) (d #t) (k 2)))) (h "19vizg3ymkxw6cqfjqdzjcq9h1qd00ixlz7sqhlyh6gz21z5a5lh")))

(define-public crate-cortex-m-interrupt-0.2.0 (c (n "cortex-m-interrupt") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-interrupt-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "stm32f1xx-hal") (r "^0.9") (f (quote ("stm32f107"))) (d #t) (k 2)))) (h "0bvdy2x1s8skh1yscvxn8iz6cpaq88pg4w6ca3nhi5znaxmdpkzf")))

