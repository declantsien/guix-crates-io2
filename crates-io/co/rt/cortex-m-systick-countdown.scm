(define-module (crates-io co rt cortex-m-systick-countdown) #:use-module (crates-io))

(define-public crate-cortex-m-systick-countdown-0.1.0 (c (n "cortex-m-systick-countdown") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.0") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.0") (d #t) (k 2)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0l3nlb0knw2m2n5vg8x1hxbnn3hnj8s48xgr0rpa79gmcxv5mki4")))

