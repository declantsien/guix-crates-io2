(define-module (crates-io co rt cortex-a) #:use-module (crates-io))

(define-public crate-cortex-a-0.1.0 (c (n "cortex-a") (v "0.1.0") (h "1d4l8n8vlf4li588g7lb9gvsng9v30508zl4xkryj0zambi1m5yb")))

(define-public crate-cortex-a-0.1.1 (c (n "cortex-a") (v "0.1.1") (h "13yjrf3wiwjzrva8pvwmdlphn32sz8k4zlm1dk6f7a511vs4w7k0")))

(define-public crate-cortex-a-0.1.2 (c (n "cortex-a") (v "0.1.2") (h "1026v7d6ak5bwx88kdivq0bnafsnjsbl3cqbjw2mlnm463x9yrbs")))

(define-public crate-cortex-a-0.1.3 (c (n "cortex-a") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)))) (h "1zjv3i34sk6i8lcp6qjin5aqr6sdlffgdf7wr47yscb56idgl8x1")))

(define-public crate-cortex-a-1.0.0 (c (n "cortex-a") (v "1.0.0") (d (list (d (n "register") (r "^0.1.1") (d #t) (k 0)))) (h "1ssfnlsdsqs4sd403jw7znndnyjzw5pf3xf0ar488ikdw6zly5r4")))

(define-public crate-cortex-a-1.1.0 (c (n "cortex-a") (v "1.1.0") (d (list (d (n "register") (r "^0.2.0") (d #t) (k 0)))) (h "007x79v69dpgww2ybykaxhygbw22jrpvcjm73nrpnrlipnqjjfl8")))

(define-public crate-cortex-a-2.0.0 (c (n "cortex-a") (v "2.0.0") (d (list (d (n "register") (r "^0.2.0") (d #t) (k 0)))) (h "1i29r7ir4n3ilcqgnsklmgnzhdlf0jyii3vz71vaw7i8qz3zg4hv") (y #t)))

(define-public crate-cortex-a-2.0.1 (c (n "cortex-a") (v "2.0.1") (d (list (d (n "register") (r "^0.2.0") (d #t) (k 0)))) (h "0pv59n57z4mhvbb323w3pc1ck19555z5w355fvh9cnz1lb2d7ddv")))

(define-public crate-cortex-a-2.1.0 (c (n "cortex-a") (v "2.1.0") (d (list (d (n "register") (r "^0.2.0") (d #t) (k 0)))) (h "0chibpr4xi9rq19xr6jkizn4q5jqnr68y9gi4admrcic3w5rq9h9")))

(define-public crate-cortex-a-2.2.0 (c (n "cortex-a") (v "2.2.0") (d (list (d (n "register") (r "^0.2.0") (d #t) (k 0)))) (h "0b6ap5y6872m38da88hb4ivi4qnd20cl64njdj0cy7m65hg4j93f")))

(define-public crate-cortex-a-2.2.1 (c (n "cortex-a") (v "2.2.1") (d (list (d (n "register") (r "^0.2.0") (d #t) (k 0)))) (h "0p9r3mcbx2pccfq6xp0f9zv3hiipl4yf21qdgngbq627ycl3d4gy")))

(define-public crate-cortex-a-2.2.2 (c (n "cortex-a") (v "2.2.2") (d (list (d (n "register") (r "^0.2.0") (d #t) (k 0)))) (h "136hlhadrllg462raw9ci8ddgkbbjwdngybr3nx9jjlbf86ps61b")))

(define-public crate-cortex-a-2.3.0 (c (n "cortex-a") (v "2.3.0") (d (list (d (n "register") (r "^0.3.2") (d #t) (k 0)))) (h "1b3454yi5ml4hrzmxck6m6b3395q69q49k52pflgpfvcvgv0x0d4") (y #t)))

(define-public crate-cortex-a-2.3.1 (c (n "cortex-a") (v "2.3.1") (d (list (d (n "register") (r "^0.3.2") (d #t) (k 0)))) (h "0iifasaja1ryyivs2yjr3x7xpscywsnq4hy777sjh7zkj525qhhj")))

(define-public crate-cortex-a-2.4.0 (c (n "cortex-a") (v "2.4.0") (d (list (d (n "register") (r "^0.3.2") (d #t) (k 0)))) (h "0ls40n55a7b5mxzv68ic8ai0k8bfigdip66zyw92j2kcg2n7p1lp")))

(define-public crate-cortex-a-2.5.0 (c (n "cortex-a") (v "2.5.0") (d (list (d (n "register") (r "^0.3.2") (d #t) (k 0)))) (h "0dzams7fhpjh2bw4aad30849dmfxhlwgbcrj21z4ma2kckslm870")))

(define-public crate-cortex-a-2.6.0 (c (n "cortex-a") (v "2.6.0") (d (list (d (n "register") (r "^0.3.2") (d #t) (k 0)))) (h "1y8bqqizmmqn0i62vaq6m542q0zhxkkmda5idixy89d72nn0041k")))

(define-public crate-cortex-a-2.7.0 (c (n "cortex-a") (v "2.7.0") (d (list (d (n "register") (r "^0.3.3") (d #t) (k 0)))) (h "0l59d9m6x67585a6ndvf2bmfpkbvwsx6qsklfkql8h5p390nrcfb")))

(define-public crate-cortex-a-2.7.1 (c (n "cortex-a") (v "2.7.1") (d (list (d (n "register") (r "0.3.*") (d #t) (k 0)))) (h "0hn987rm8q85a7crhnb770s1g52jsjpw4lns0bflfxbr7jlrfw20")))

(define-public crate-cortex-a-2.8.0 (c (n "cortex-a") (v "2.8.0") (d (list (d (n "register") (r "0.4.*") (d #t) (k 0)))) (h "1d7rpv5ss0kz60vyw3f1mbiiwc3mnss5qs6yfzp8d7jl8m3s8yxd")))

(define-public crate-cortex-a-2.8.1 (c (n "cortex-a") (v "2.8.1") (d (list (d (n "register") (r "0.4.*") (d #t) (k 0)))) (h "1n2hlmi3b27xlcmdm317vlv4lvnzkr5a0gi97myv92mgp82ajda1")))

(define-public crate-cortex-a-2.8.2 (c (n "cortex-a") (v "2.8.2") (d (list (d (n "register") (r "0.4.*") (d #t) (k 0)))) (h "1zfhw5m2nbrk6cm32yfbvbrjs4s86scrkp9l5fjiggyrf592zasa")))

(define-public crate-cortex-a-2.9.0 (c (n "cortex-a") (v "2.9.0") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "0671xdz3wzy92xfl8cixhl9l57akyzrylslzghn36h0r4h97m07x")))

(define-public crate-cortex-a-3.0.0 (c (n "cortex-a") (v "3.0.0") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "1hnb09ms2yhbdr64vfn2s81pyyr75w633yb0lqc5z1cld8anqhpy")))

(define-public crate-cortex-a-3.0.1 (c (n "cortex-a") (v "3.0.1") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "145laz0pflbrlxlqn0did4p7pqr3chwyqhwkf2xwm9h41gfarijz")))

(define-public crate-cortex-a-3.0.2 (c (n "cortex-a") (v "3.0.2") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "1dng5vgcx1fb3fd9ww31lch9x5vijgzgkgmizmf91pnifnmw16ha")))

(define-public crate-cortex-a-3.0.3 (c (n "cortex-a") (v "3.0.3") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "187xy5pqrxk9hqvhbhq8v1iy3j6zn3dgb840904i8kzsvya74in6")))

(define-public crate-cortex-a-3.0.4 (c (n "cortex-a") (v "3.0.4") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "103zaif450ni397j6h079498j6riawz9yfwnijndx8niyh5a88k9")))

(define-public crate-cortex-a-3.0.5 (c (n "cortex-a") (v "3.0.5") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "1xdg8nvq6xximh9dn7g06q3m40d13cy7ygbd3ln49bnysf4dllxd")))

(define-public crate-cortex-a-4.0.0 (c (n "cortex-a") (v "4.0.0") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "1cc6nhwjx8hbysn4syr16cgfn225xa6f57sm06a0k0kvqs71p8pd")))

(define-public crate-cortex-a-4.1.0 (c (n "cortex-a") (v "4.1.0") (d (list (d (n "register") (r "0.5.*") (d #t) (k 0)))) (h "1a6bd50wkawinpss9cys3cfixdvifqsqppp8q01jmq99296532d3")))

(define-public crate-cortex-a-5.0.0 (c (n "cortex-a") (v "5.0.0") (d (list (d (n "register") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1i508vsv9h5614if289jip84lk49k1cpk50fvzk6n7psysqdnvv2")))

(define-public crate-cortex-a-5.0.1 (c (n "cortex-a") (v "5.0.1") (d (list (d (n "register") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1hs0k7rmbxrmp3cdxsdnpil1n4wam6ypazw5qwkgakm35cpsb6kp")))

(define-public crate-cortex-a-5.1.0 (c (n "cortex-a") (v "5.1.0") (d (list (d (n "register") (r "1.*.*") (d #t) (k 0)))) (h "15iqz3x76il4fva5r5lsnn5vv8g69jyz7q7mjvgl0nsg5a3a8dyw")))

(define-public crate-cortex-a-5.1.1 (c (n "cortex-a") (v "5.1.1") (d (list (d (n "register") (r "1.*.*") (d #t) (k 0)))) (h "0az6ryr64anc7xh6gvdv7zqvm100i7drjvsynfk877kcc027b6vw")))

(define-public crate-cortex-a-5.1.2 (c (n "cortex-a") (v "5.1.2") (d (list (d (n "register") (r "1.*.*") (d #t) (k 0)))) (h "07bkzs2j11qnjq8qwhdwlzzaj86qspa7m5ljw3rzzhw35xh62fnk")))

(define-public crate-cortex-a-5.1.3 (c (n "cortex-a") (v "5.1.3") (d (list (d (n "register") (r "1.*.*") (d #t) (k 0)))) (h "159jg0nysm21z5hnl5x03hkwc3s1jcdngmwzwpyrp1sj48rvdlra")))

(define-public crate-cortex-a-5.1.4 (c (n "cortex-a") (v "5.1.4") (d (list (d (n "register") (r "1.*.*") (d #t) (k 0)))) (h "13zf4yipl60gk4psma8j7w2r0nwgyndskjn3ilm9nl9pmj6zhnx9")))

(define-public crate-cortex-a-5.1.5 (c (n "cortex-a") (v "5.1.5") (d (list (d (n "register") (r "1.*.*") (d #t) (k 0)))) (h "12i6ydw169s38ahlmxsgwjd6dhndyi8c51cg4lh6yzickzz18g83")))

(define-public crate-cortex-a-5.1.6 (c (n "cortex-a") (v "5.1.6") (d (list (d (n "register") (r "1.*.*") (d #t) (k 0)))) (h "0a4d4vlnc1yqxjr52zm9grh9xhhfbhqb5m0hm32sz1zbfl4w7vzc")))

(define-public crate-cortex-a-6.0.0 (c (n "cortex-a") (v "6.0.0") (d (list (d (n "tock-registers") (r "0.7.*") (k 0)))) (h "19pb9wn9amp6v9nf9gf5df41wcyxvdx2ycndac206z8i6mxhannj")))

(define-public crate-cortex-a-6.1.0 (c (n "cortex-a") (v "6.1.0") (d (list (d (n "tock-registers") (r "0.7.*") (k 0)))) (h "0cvpbfvzq6k5rkjhi1k5jgd8izyz5hhz42xyraxdvd52hmac77sh")))

(define-public crate-cortex-a-7.0.0 (c (n "cortex-a") (v "7.0.0") (d (list (d (n "tock-registers") (r "0.7.*") (k 0)))) (h "0ksyf003pj1rcqc6xlw3accnbphixky0l36zvycrf7v73c3mszxj")))

(define-public crate-cortex-a-7.0.1 (c (n "cortex-a") (v "7.0.1") (d (list (d (n "tock-registers") (r "0.7.*") (k 0)))) (h "160gxwghixvh2an4v6xihqsx3k6fnq0p4ljd9ryzf66iap85znav")))

(define-public crate-cortex-a-7.1.0 (c (n "cortex-a") (v "7.1.0") (d (list (d (n "tock-registers") (r "0.7.*") (k 0)))) (h "0xdhz0hhr9ghcwa0q4w218chx8db3y1630k25ssrgfsfy1py8ibd")))

(define-public crate-cortex-a-7.2.0 (c (n "cortex-a") (v "7.2.0") (d (list (d (n "tock-registers") (r "0.7.*") (k 0)))) (h "1b9lwzv9hn5zb5zi80piyz71w55g6idqrna3s2r8nd6dbkv93g97")))

(define-public crate-cortex-a-7.3.0 (c (n "cortex-a") (v "7.3.0") (d (list (d (n "tock-registers") (r "0.7.*") (o #t) (k 0)))) (h "18j2bfxp329qjwdy0lqs67j4s47zn18pyb64nyq6my6l7kn80a1l") (f (quote (("nightly" "tock-registers") ("default" "nightly"))))))

(define-public crate-cortex-a-7.3.1 (c (n "cortex-a") (v "7.3.1") (d (list (d (n "tock-registers") (r "0.7.*") (o #t) (k 0)))) (h "0m0vi94acgx073scv8pph4kpl917lksf1qipnr9igk6qjsqwmpnc") (f (quote (("nightly" "tock-registers") ("default" "nightly"))))))

(define-public crate-cortex-a-7.4.0 (c (n "cortex-a") (v "7.4.0") (d (list (d (n "tock-registers") (r "0.7.*") (o #t) (k 0)))) (h "06nmhrsqc342mglvkm06dgzax829j8hamya6zy48wx6n9sbbcpgp") (f (quote (("nightly" "tock-registers") ("default" "nightly"))))))

(define-public crate-cortex-a-7.5.0 (c (n "cortex-a") (v "7.5.0") (d (list (d (n "tock-registers") (r "0.7.*") (o #t) (k 0)))) (h "1rk78k66i2dj04anjq6nhq6l7psji8wmmq0swxj3dbbjhsrgpv5x") (f (quote (("nightly" "tock-registers") ("default" "nightly"))))))

(define-public crate-cortex-a-8.0.0 (c (n "cortex-a") (v "8.0.0") (d (list (d (n "tock-registers") (r "0.8.*") (o #t) (k 0)))) (h "1fr5ih0kqd5q0ncjzknr3phzyc8zaxgxbw4imr8frq54654m5m0c") (f (quote (("nightly" "tock-registers") ("default" "nightly"))))))

(define-public crate-cortex-a-8.1.0 (c (n "cortex-a") (v "8.1.0") (d (list (d (n "tock-registers") (r "0.8.*") (o #t) (k 0)))) (h "0qzql7yhlsj95rn83915ndq19515lpdbz4flidml9080z9kw2jrw") (f (quote (("nightly" "tock-registers") ("default" "nightly"))))))

(define-public crate-cortex-a-8.1.1 (c (n "cortex-a") (v "8.1.1") (d (list (d (n "tock-registers") (r "0.8.*") (o #t) (k 0)))) (h "0a4fw7vaxrzjmyh4v1i4x93lbk3zyagpraf7gi32f0710d8zsml2") (f (quote (("nightly" "tock-registers") ("default" "nightly"))))))

