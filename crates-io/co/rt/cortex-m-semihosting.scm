(define-module (crates-io co rt cortex-m-semihosting) #:use-module (crates-io))

(define-public crate-cortex-m-semihosting-0.1.0 (c (n "cortex-m-semihosting") (v "0.1.0") (h "11s6qpj4xbi5dm36nckkrxcpsw5i2r2dkl7n7zx01kgy4nsiaham")))

(define-public crate-cortex-m-semihosting-0.1.1 (c (n "cortex-m-semihosting") (v "0.1.1") (h "0f764vnvgb8lwz0zxrankrhdz7sji2lhhvw62v0qk5wk8ssj98fa")))

(define-public crate-cortex-m-semihosting-0.1.2 (c (n "cortex-m-semihosting") (v "0.1.2") (h "0wj9kys2gwjqa62vkilrb82hsp159ig2r0rhzgsghaf9hz97xlmq")))

(define-public crate-cortex-m-semihosting-0.1.3 (c (n "cortex-m-semihosting") (v "0.1.3") (h "0pk51fw9ac2n3inwbccdpr2nn6yfxqs7y40rz0vqd03psn7qxa2l")))

(define-public crate-cortex-m-semihosting-0.2.0 (c (n "cortex-m-semihosting") (v "0.2.0") (h "1s1xrq9r542hypbpr3iabbhqs5awz3vb8k6ldcywsk8qii534yja")))

(define-public crate-cortex-m-semihosting-0.2.1 (c (n "cortex-m-semihosting") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.10") (d #t) (k 1)))) (h "0i0yddx1j9s774qs8spjrywhkz3n0hzj33xhvcs1qv7mm92z23al") (f (quote (("inline-asm") ("default" "inline-asm"))))))

(define-public crate-cortex-m-semihosting-0.3.0 (c (n "cortex-m-semihosting") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.10") (d #t) (k 1)))) (h "07h78rgfhfv0ncjnj6chg68sbyc33ni1skczvmk4yz2kdpwhsg8b") (f (quote (("inline-asm"))))))

(define-public crate-cortex-m-semihosting-0.3.1 (c (n "cortex-m-semihosting") (v "0.3.1") (h "1rf9i07ygjbshk2alp41ccdwbcnn9vpjhhshwhfs0503fg26xm2l") (f (quote (("inline-asm"))))))

(define-public crate-cortex-m-semihosting-0.3.2 (c (n "cortex-m-semihosting") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)))) (h "1dd4w9x4pfpny0h9i4zqr91h060gf7aigbbwd6xyhwm7q6z2mp6i") (f (quote (("inline-asm"))))))

(define-public crate-cortex-m-semihosting-0.3.3 (c (n "cortex-m-semihosting") (v "0.3.3") (d (list (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)))) (h "0ihfrhrc4xfnwcjzjvnxz5821blg7xd8vp69lr8i60yiyj33ypqn") (f (quote (("inline-asm"))))))

(define-public crate-cortex-m-semihosting-0.3.4 (c (n "cortex-m-semihosting") (v "0.3.4") (d (list (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)))) (h "12j1nlpfd8yl5443rlbm59vfza4chkfdwzkzmx73ckrldp11gg85") (f (quote (("inline-asm"))))))

(define-public crate-cortex-m-semihosting-0.3.5 (c (n "cortex-m-semihosting") (v "0.3.5") (d (list (d (n "cortex-m") (r ">= 0.5.8, < 0.7") (d #t) (k 0)))) (h "1yicfshkn85lf53q1qmykjghxcwrj13g904kiysn4azfzzng0ghi") (f (quote (("jlink-quirks") ("inline-asm"))))))

(define-public crate-cortex-m-semihosting-0.4.0 (c (n "cortex-m-semihosting") (v "0.4.0") (d (list (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)))) (h "0fagm2sakg39wbz9pzzqr92j9r4047g5lsgkkg4mginb9wycp2ca") (f (quote (("no-semihosting") ("jlink-quirks") ("inline-asm")))) (y #t)))

(define-public crate-cortex-m-semihosting-0.4.1 (c (n "cortex-m-semihosting") (v "0.4.1") (d (list (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)))) (h "0h1khawlaa1gyvmbjjiy7fv4n2w5kw9mb530l7d1qmhgj3fffg52") (f (quote (("no-semihosting") ("jlink-quirks") ("inline-asm")))) (y #t)))

(define-public crate-cortex-m-semihosting-0.3.6 (c (n "cortex-m-semihosting") (v "0.3.6") (d (list (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)))) (h "09jqhansch4yrcin9kqrh8amh9yrh9ga15hpz62kycq67nnysiaa") (f (quote (("no-semihosting") ("jlink-quirks") ("inline-asm")))) (y #t)))

(define-public crate-cortex-m-semihosting-0.3.7 (c (n "cortex-m-semihosting") (v "0.3.7") (d (list (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)))) (h "1g29yz0v24cjidd0ky3z75nrkriq9jb61bhi92macs238p0sdzvb") (f (quote (("no-semihosting") ("jlink-quirks") ("inline-asm"))))))

(define-public crate-cortex-m-semihosting-0.5.0 (c (n "cortex-m-semihosting") (v "0.5.0") (d (list (d (n "cortex-m") (r ">=0.5.8, <0.8") (d #t) (k 0)))) (h "1c4j6nbmh3san5mg7nqiiqbc9lh21rs1wxjbgv3kq0sj0ih38cn2") (f (quote (("no-semihosting") ("jlink-quirks")))) (r "1.59")))

