(define-module (crates-io co ke coke) #:use-module (crates-io))

(define-public crate-coke-0.2.0 (c (n "coke") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mlua") (r "^0.8.3") (f (quote ("lua54" "vendored" "send"))) (d #t) (k 0)))) (h "0z0nxx0kpvckwv7waaa1jrff0gdnhjc4mm3rwk10yxicvjl9wjd5") (y #t)))

(define-public crate-coke-0.1.0 (c (n "coke") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mlua") (r "^0.8.3") (f (quote ("lua54" "vendored" "send"))) (d #t) (k 0)))) (h "1k1jrdhyjiy1wrnm11zdx0vkzafgc0rjx92vnkpm1p9rq0q2pnis") (y #t)))

(define-public crate-coke-0.2.1 (c (n "coke") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "mlua") (r "^0.8.3") (f (quote ("lua54" "vendored" "send"))) (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "01bziqa2navblns33h8q40xmb94iir3x3gwfrklszkb69y45659h") (y #t)))

