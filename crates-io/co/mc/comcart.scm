(define-module (crates-io co mc comcart) #:use-module (crates-io))

(define-public crate-comcart-0.1.0 (c (n "comcart") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "zip") (r "^0.1.16") (d #t) (k 0)))) (h "0p96n6xbrm2fl9dkmdk8h0hhb61z3pc3s92b3q9jyb0jzwyw4j3j")))

