(define-module (crates-io co mc comctl32-sys) #:use-module (crates-io))

(define-public crate-comctl32-sys-0.0.1 (c (n "comctl32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0c66khbdslx582yc36vaaslrm10f1phw4ld5czm353agf8hg7hca")))

(define-public crate-comctl32-sys-0.2.0 (c (n "comctl32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "09diqpyg7zglvxarspyhsj6xzjhanh5ii4lns9brmqh3fhlmj42h")))

