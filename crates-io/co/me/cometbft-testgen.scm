(define-module (crates-io co me cometbft-testgen) #:use-module (crates-io))

(define-public crate-cometbft-testgen-0.1.0-alpha.2 (c (n "cometbft-testgen") (v "0.1.0-alpha.2") (d (list (d (n "cometbft") (r "^0.1.0-alpha.2") (f (quote ("clock"))) (d #t) (k 0)) (d (n "ed25519-consensus") (r "^2") (k 0)) (d (n "gumdrop") (r "^0.8.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "simple-error") (r "^0.3.0") (k 0)) (d (n "tempfile") (r "^3.1.0") (k 0)) (d (n "time") (r "^0.3") (f (quote ("std"))) (k 0) (p "time")))) (h "1cpais9ysgwbxg9m9xahad8rcdip2p1znzpm2ysxh80cy8y62ag6")))

