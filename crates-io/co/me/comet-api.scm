(define-module (crates-io co me comet-api) #:use-module (crates-io))

(define-public crate-comet-api-0.1.0 (c (n "comet-api") (v "0.1.0") (d (list (d (n "comet-gc") (r "^0.1") (d #t) (k 0)) (d (n "mopa") (r "^0.2") (d #t) (k 0)))) (h "16fwvnzvyh7ghqvdld9i5pdmji2wh666gnpxxx6wv0caqg6vvm8l")))

