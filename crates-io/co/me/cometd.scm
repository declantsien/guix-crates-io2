(define-module (crates-io co me cometd) #:use-module (crates-io))

(define-public crate-cometd-0.1.0 (c (n "cometd") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mockito") (r "^0.22.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "0jf44l199sk0dc4hlm5bzcjavg1gqh8mmk8dnk2r445arwf99xw3")))

(define-public crate-cometd-0.1.1 (c (n "cometd") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mockito") (r "^0.22.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "1iq209d54vaf5zkibgss4xrzvcsgmgvsyqpzp17f98hqfmkb9wxj")))

(define-public crate-cometd-0.1.2 (c (n "cometd") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mockito") (r "^0.22.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "0k6ij30fraq3wa97gcmsdyczvjd33ixl5hsmgnfz5m243cq9gmwf")))

