(define-module (crates-io co me cometbft-light-client-verifier) #:use-module (crates-io))

(define-public crate-cometbft-light-client-verifier-0.1.0-alpha.2 (c (n "cometbft-light-client-verifier") (v "0.1.0-alpha.2") (d (list (d (n "cometbft") (r "^0.1.0-alpha.2") (k 0)) (d (n "derive_more") (r "^0.99.5") (f (quote ("display"))) (k 0)) (d (n "flex-error") (r "^0.4.4") (k 0)) (d (n "serde") (r "^1.0.106") (k 0)) (d (n "sha2") (r "^0.10") (k 2)) (d (n "time") (r "^0.3") (k 0)))) (h "1xk8z45p5is7djhkpnnki48xr7n28hgh8zjhknabqgs4vq4310z8") (f (quote (("rust-crypto" "cometbft/rust-crypto") ("default" "rust-crypto" "flex-error/std"))))))

