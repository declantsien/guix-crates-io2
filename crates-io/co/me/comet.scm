(define-module (crates-io co me comet) #:use-module (crates-io))

(define-public crate-comet-1.0.0 (c (n "comet") (v "1.0.0") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)))) (h "1mgpj9rrzxkp2ziwv1m9bms19d0q3qcqr0ka68dij1vgnqz702f7")))

(define-public crate-comet-1.1.0 (c (n "comet") (v "1.1.0") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "notify") (r "^2.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)))) (h "0imyp2njw8dpl7rifk3b82406s95jiapic8xggh3wab1gh6ka6ms")))

(define-public crate-comet-1.1.1 (c (n "comet") (v "1.1.1") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "notify") (r "^2.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)))) (h "1qia0xlxgsjrs62bmc2xjiy92fk60f9jfnhzv0i2cgys5sqi1riz")))

(define-public crate-comet-2.0.0 (c (n "comet") (v "2.0.0") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "notify") (r "^2.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)))) (h "0ms8xq3dirb392rlb3xjmvcj2c3n24idz4lb1jp0di55l4yy2bqk")))

