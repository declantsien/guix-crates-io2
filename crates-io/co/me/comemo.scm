(define-module (crates-io co me comemo) #:use-module (crates-io))

(define-public crate-comemo-0.1.0 (c (n "comemo") (v "0.1.0") (d (list (d (n "comemo-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1l2zwhzyb0vsx470lpqsg9i545gspwl1q4kp239dm7v1948fnb5z")))

(define-public crate-comemo-0.2.0 (c (n "comemo") (v "0.2.0") (d (list (d (n "comemo-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "19ahsdiajmcv398cm315lj9m7mcdmvnkz6bisxfm681h14hjrgr2")))

(define-public crate-comemo-0.2.1 (c (n "comemo") (v "0.2.1") (d (list (d (n "comemo-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "10gjif5dngzv99mwjnznxvnshc4cb8jqmzc8sp0x59x1y3k9dcvh")))

(define-public crate-comemo-0.2.2 (c (n "comemo") (v "0.2.2") (d (list (d (n "comemo-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0grz653cq697h0y7hyl51565y5l89sr8ni9a8p2m26v82bi2790v")))

(define-public crate-comemo-0.3.0 (c (n "comemo") (v "0.3.0") (d (list (d (n "comemo-macros") (r "^0.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "14ng6gqklsy8m9wn6radragn8pazsmn5759mywxb1ddf8bqrg818")))

(define-public crate-comemo-0.3.1 (c (n "comemo") (v "0.3.1") (d (list (d (n "comemo-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "siphasher") (r "^1") (d #t) (k 0)))) (h "1x66czkfc1dwg2d20hryspzqnhkcdqq1i4szdbp041m8ix30amxz")))

(define-public crate-comemo-0.4.0 (c (n "comemo") (v "0.4.0") (d (list (d (n "comemo-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serial_test") (r "^3") (d #t) (k 2)) (d (n "siphasher") (r "^1") (d #t) (k 0)))) (h "14bsiayib4lhz3jrbf1fqh2fpwsm6cii90mifym3jhvji901csfz") (f (quote (("testing") ("default"))))))

