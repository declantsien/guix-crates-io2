(define-module (crates-io co me cometbft-light-client-js) #:use-module (crates-io))

(define-public crate-cometbft-light-client-js-0.1.0-alpha.2 (c (n "cometbft-light-client-js") (v "0.1.0-alpha.2") (d (list (d (n "cometbft") (r "^0.1.0-alpha.2") (k 0)) (d (n "cometbft-light-client-verifier") (r "^0.1.0-alpha.2") (f (quote ("rust-crypto"))) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4.5") (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (f (quote ("serde-serialize"))) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (k 2)))) (h "1jbzm1af6y1rk5gx3w4wdvz4xqgyzpnpf5gap8ijsrvwshiaw9sx") (f (quote (("default" "console_error_panic_hook"))))))

