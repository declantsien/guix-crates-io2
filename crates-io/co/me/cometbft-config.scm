(define-module (crates-io co me cometbft-config) #:use-module (crates-io))

(define-public crate-cometbft-config-0.1.0-alpha.2 (c (n "cometbft-config") (v "0.1.0-alpha.2") (d (list (d (n "cometbft") (r "^0.1.0-alpha.2") (f (quote ("rust-crypto"))) (k 0)) (d (n "flex-error") (r "^0.4.4") (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0zn8qbd9vnrvxn4l3ql0h4jr0da1llvw2vw52fb0vdqr0h3z8f2j") (f (quote (("secp256k1" "cometbft/secp256k1"))))))

