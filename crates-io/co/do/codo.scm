(define-module (crates-io co do codo) #:use-module (crates-io))

(define-public crate-codo-0.0.1 (c (n "codo") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grep") (r "^0.2.12") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "062yjdkjf8qpvbniqkp90vgvb09j3yvs5lm668giviwkylli87nf")))

