(define-module (crates-io co vc covctl) #:use-module (crates-io))

(define-public crate-covctl-0.1.0 (c (n "covctl") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "covalent_class_a") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xlxwc722akgdfxya5pchgqz1wvzyri39x4va9xvb394vg8ryn9z")))

(define-public crate-covctl-0.1.1 (c (n "covctl") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "covalent_class_a") (r "^0.1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h5cjy1s358v16ag6yiva5qmz40n3b6ylz71y2r548al49lrr68d")))

(define-public crate-covctl-0.1.3 (c (n "covctl") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "covalent_class_a") (r "^0.1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bpjsrlr4b54ym7hlyknn15kci82f0pkmh24anych7rhdf5ypqjr")))

