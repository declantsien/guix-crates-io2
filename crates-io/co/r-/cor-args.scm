(define-module (crates-io co r- cor-args) #:use-module (crates-io))

(define-public crate-cor-args-0.1.0 (c (n "cor-args") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("string" "env"))) (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "07zgcahlhnsyj96gj0gyj5zg2bsjrp5ip6527ly96z3pc0xr4ad6")))

(define-public crate-cor-args-0.1.1 (c (n "cor-args") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("string" "env"))) (d #t) (k 0)) (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "1jm8i0vq9fpk83zmwchxqh6jcr1y1kjz0aa9nfy9rsfnyg6im2rz")))

(define-public crate-cor-args-0.2.0 (c (n "cor-args") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("string" "env"))) (o #t) (d #t) (k 0)) (d (n "config") (r "^0.13.3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "1d53lybmjk5pmi1whg90c4rk6apz8lzk8sdp98r5nham4mj5c0rr") (s 2) (e (quote (("config" "dep:config") ("clap" "dep:clap"))))))

