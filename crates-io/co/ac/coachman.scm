(define-module (crates-io co ac coachman) #:use-module (crates-io))

(define-public crate-coachman-0.1.0 (c (n "coachman") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0lsli5lcqpajcd4nfgkmr4xw6zcflrar7b97j3k2n4vpl3d792q4")))

(define-public crate-coachman-0.2.0 (c (n "coachman") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0vl02gw93qzhdw518x1kamydca04cq4mvsfd0bdyr8vv5lz5dyyr")))

(define-public crate-coachman-0.3.0 (c (n "coachman") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "13s725n89av4dbgj8zcpjxbyl7r3qbd1z1cr4mffb81lgb63pj01")))

