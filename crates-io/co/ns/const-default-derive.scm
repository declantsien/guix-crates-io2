(define-module (crates-io co ns const-default-derive) #:use-module (crates-io))

(define-public crate-const-default-derive-0.1.0 (c (n "const-default-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "017sm2b4093n36nk9yjdi86m79wwmkgpn7cxbxp9bfhbaap7b9b7")))

(define-public crate-const-default-derive-0.2.0 (c (n "const-default-derive") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "1nh3iwba073s9vsyhr5ci0pgbnc6zavmfs7za4vj64mqrgc4v08g")))

