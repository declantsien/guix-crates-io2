(define-module (crates-io co ns console-prompt) #:use-module (crates-io))

(define-public crate-console-prompt-0.1.0 (c (n "console-prompt") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "0w7v0swa7qg4x4b26cqiplz2a1yz3i1dizv05b1w8vhl8vl25khc")))

(define-public crate-console-prompt-0.1.1 (c (n "console-prompt") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "1dwssnp4rqgxcmqzjki9ys0mvpdm5dz9c0h4kynqhwl55a72n84c")))

(define-public crate-console-prompt-0.1.2 (c (n "console-prompt") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "0cayrz10v4krlkgnirjhk9ycja2iixif8vakynb3b336kpkanlif")))

(define-public crate-console-prompt-0.1.3 (c (n "console-prompt") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "1kh3z6gqjgyc77br7w9m2y0fpcdsnxxw7va548p0wbzcxga6vzjl")))

(define-public crate-console-prompt-0.1.4 (c (n "console-prompt") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "0ac86swz1bbcyy5m4dbam6sawj4a08wjdks2i4zbh4b6bqmqa7xr")))

(define-public crate-console-prompt-0.1.5 (c (n "console-prompt") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "1xhhpm6i5bmzmg8n841p3yj1wmm8w156qmpkrpf280pdnray5hww")))

