(define-module (crates-io co ns consecuit) #:use-module (crates-io))

(define-public crate-consecuit-0.1.0 (c (n "consecuit") (v "0.1.0") (d (list (d (n "consecuit_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.51") (d #t) (k 0)) (d (n "sealed") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.51") (f (quote ("Document" "DocumentFragment" "Element" "Window" "HtmlElement" "Node" "NodeList" "CssStyleDeclaration" "console"))) (d #t) (k 0)))) (h "0rgzrflf628nbsrfj61iqjwq6p6xy2qx3bkk42n24rmmhbfdavf5")))

(define-public crate-consecuit-0.2.0 (c (n "consecuit") (v "0.2.0") (d (list (d (n "consecuit_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.51") (d #t) (k 0)) (d (n "sealed") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.51") (f (quote ("Document" "DocumentFragment" "Element" "Window" "HtmlElement" "Node" "NodeList" "CssStyleDeclaration" "console"))) (d #t) (k 0)))) (h "08j9ssg6pb4q8r8kmng15wiyqxpz5h1bd7a22d1ll922znsrr0bx")))

