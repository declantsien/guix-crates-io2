(define-module (crates-io co ns consalifold) #:use-module (crates-io))

(define-public crate-consalifold-0.1.0 (c (n "consalifold") (v "0.1.0") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "conshomfold") (r "^0.1") (d #t) (k 0)) (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "07rjaqhlv0hg0i4s12w17zy3izz07h2jgcw9mq0qzmpa30pz49l0")))

(define-public crate-consalifold-0.1.1 (c (n "consalifold") (v "0.1.1") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "conshomfold") (r "^0.1") (d #t) (k 0)) (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1knwrsfvk8adzr1n880xljbvd1rlah3h6752gijddmyad4k4qccv")))

(define-public crate-consalifold-0.1.2 (c (n "consalifold") (v "0.1.2") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "0cr4lx4yrlb7anafn6vafmxdkkp60m4pbyh401k36ahv2h4jp05w")))

(define-public crate-consalifold-0.1.3 (c (n "consalifold") (v "0.1.3") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1j5an4pz66h6f2vwnxg8fb3j09c7a2lgklypf41avxlj2majzn80")))

(define-public crate-consalifold-0.1.4 (c (n "consalifold") (v "0.1.4") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "01g20d4dffx65r8513ycf41azgimkad62r89jl7pv9x3bv8h24dd")))

(define-public crate-consalifold-0.1.5 (c (n "consalifold") (v "0.1.5") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "0p4vn1n1607fi7fypji2z1f7rxjmm5lzwcr10hsyhcyjn2xpgvhk")))

(define-public crate-consalifold-0.1.6 (c (n "consalifold") (v "0.1.6") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "03y1k7glg88cyihckcz870wzify1dyhygzw7lbghsyylmcknzsg9")))

(define-public crate-consalifold-0.1.7 (c (n "consalifold") (v "0.1.7") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "050sdcff44zvy7nm4kryngbi1ffkaa9ckwv9cljcq5939nj94x14")))

(define-public crate-consalifold-0.1.8 (c (n "consalifold") (v "0.1.8") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "12qr9f3ngx9g7fwjxpxb2h9m7rrliywkjd780m2ikw6xsda685pa")))

(define-public crate-consalifold-0.1.9 (c (n "consalifold") (v "0.1.9") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "0lc8ciyvxs6b31y796xl9891zz0606ircxh15l8dkqw1dm565pv8")))

(define-public crate-consalifold-0.1.10 (c (n "consalifold") (v "0.1.10") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1kw24404axq61f7dqks8iyy4qjj322kwsb6g3pgyq249sl86640i")))

(define-public crate-consalifold-0.1.11 (c (n "consalifold") (v "0.1.11") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "06yv0jrs78pm8xcpi6nsx9813v6pds80j0zi7faapwvrj5jxndqd")))

(define-public crate-consalifold-0.1.12 (c (n "consalifold") (v "0.1.12") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "16vdihnr5jba1czdmb09ax8w9nq83kjdrjm73ll8kvdvpf2p8knb")))

(define-public crate-consalifold-0.1.13 (c (n "consalifold") (v "0.1.13") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "0m4gv296xdmz69zwgx5vsgyv3d2l3s4nxwcb8gkfcv2phpm59psx")))

(define-public crate-consalifold-0.1.14 (c (n "consalifold") (v "0.1.14") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1plpgfav8njbf31bz1w1vzc4aadp2ackx32jfpm6245cavmpnknl")))

(define-public crate-consalifold-0.1.15 (c (n "consalifold") (v "0.1.15") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "0qcbbscd03mha2p01cx4mzf413v49mkh9xa13w51c2lyihxnsadb")))

(define-public crate-consalifold-0.1.16 (c (n "consalifold") (v "0.1.16") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0bmbdap5z2bn1q4aiwprmw60z0kg63jadvfg45zakyk2zxila9ay")))

(define-public crate-consalifold-0.1.17 (c (n "consalifold") (v "0.1.17") (d (list (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1gizbxp6qbbx2pal1kjaj1l8qr457023dlkbxr84a8z7l9vx3gr5")))

