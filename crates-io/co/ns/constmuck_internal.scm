(define-module (crates-io co ns constmuck_internal) #:use-module (crates-io))

(define-public crate-constmuck_internal-0.1.0 (c (n "constmuck_internal") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)))) (h "0zxlzwi02f99qxmh8mzkhkbaf6fmc3w2hx4ld35vpd13h86632fq") (f (quote (("debug_checks"))))))

(define-public crate-constmuck_internal-0.1.1 (c (n "constmuck_internal") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)))) (h "07g3ag2aj6gbhjp5927xakz0y5nfmjx1fvq1h7bd4dshlcxyh4cv") (f (quote (("debug_checks"))))))

(define-public crate-constmuck_internal-0.2.0 (c (n "constmuck_internal") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)))) (h "1awlc488i237vprqij7r2nyilzg9b717v1fvqsmdyrvgcmgpz2xr") (f (quote (("debug_checks"))))))

(define-public crate-constmuck_internal-0.3.0 (c (n "constmuck_internal") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "const_panic") (r "^0.1") (o #t) (k 0)))) (h "04xsf3c6c9kp2b45qw66dk43g4zs31szrkiidcybgj62g41ygjqd") (f (quote (("rust_1_57" "const_panic"))))))

(define-public crate-constmuck_internal-1.0.0 (c (n "constmuck_internal") (v "1.0.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "const_panic") (r "^0.2") (k 0)))) (h "0x83vyycippk0bqpngl81had7fn1fmlrhnkjqbprh0zghvwpp4nm") (f (quote (("debug_checks")))) (r "1.65.0")))

