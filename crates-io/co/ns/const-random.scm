(define-module (crates-io co ns const-random) #:use-module (crates-io))

(define-public crate-const-random-0.1.0 (c (n "const-random") (v "0.1.0") (d (list (d (n "const-random-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0mlhlslrvzl6bah9cwd047ias0d45494wih5sn8r3v197gfsivx9")))

(define-public crate-const-random-0.1.1 (c (n "const-random") (v "0.1.1") (d (list (d (n "const-random-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0b0w2l656pm5g32mh5gm6mmilmk92h2z4kgvxbfxaw750d90wp1f")))

(define-public crate-const-random-0.1.2 (c (n "const-random") (v "0.1.2") (d (list (d (n "const-random-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0fhwk29ialybp62bdwxxmkdc682nhjm3ljpbpc8sa41dy1hm40cr")))

(define-public crate-const-random-0.1.3 (c (n "const-random") (v "0.1.3") (d (list (d (n "const-random-macro") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "12g015p666sn4bxq1gz0bhw2gz8w7y850j9ksv743dvhc5n0lhvw")))

(define-public crate-const-random-0.1.4 (c (n "const-random") (v "0.1.4") (d (list (d (n "const-random-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "07flsxmqlzb5vi1m8ms51aab9wsljljv2lrsgjc6fc1cv6c4s3ax")))

(define-public crate-const-random-0.1.5 (c (n "const-random") (v "0.1.5") (d (list (d (n "const-random-macro") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "10ia3cg4qmc7xqzb80lrz6r0h7pkmwj2jg9prpxvypn090j6fxjq")))

(define-public crate-const-random-0.1.6 (c (n "const-random") (v "0.1.6") (d (list (d (n "const-random-macro") (r "^0.1.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1znp5r6qrxs1g406w4mndk7bh3i53hxj0r2m57rl3qv7k261lr3v")))

(define-public crate-const-random-0.1.7 (c (n "const-random") (v "0.1.7") (d (list (d (n "const-random-macro") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0nmx8haq7mjwrhkj6synjpclibkss7i004zwnmpw8j140hwa923g")))

(define-public crate-const-random-0.1.8 (c (n "const-random") (v "0.1.8") (d (list (d (n "const-random-macro") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0sp1ang5sh27fq5b7g9fdwpq4d5s17ymj7khfzax4bbvffngj6ig")))

(define-public crate-const-random-0.1.9 (c (n "const-random") (v "0.1.9") (d (list (d (n "const-random-macro") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "092ksxvadrwa87rq858b1pg4jzhfhqcsaagzr80zgpbah8b5hwa7")))

(define-public crate-const-random-0.1.10 (c (n "const-random") (v "0.1.10") (d (list (d (n "const-random-macro") (r "^0.1.10") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "122cqcvxjnbmm1vl1nnlj7cg4j2f2yvmcwrkmig8an2il12rhx5w") (y #t)))

(define-public crate-const-random-0.1.11 (c (n "const-random") (v "0.1.11") (d (list (d (n "const-random-macro") (r "^0.1.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "00nsjnnix4cbl8zwkdnijvvncvxlhbazf746xlfnxvn25p0q5p02")))

(define-public crate-const-random-0.1.12 (c (n "const-random") (v "0.1.12") (d (list (d (n "const-random-macro") (r "^0.1.12") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1v8p1zbw9dsp2cc81ns1ai43fsf1lfw4qdwz4x3h6n2ifdd46va8")))

(define-public crate-const-random-0.1.13 (c (n "const-random") (v "0.1.13") (d (list (d (n "const-random-macro") (r "^0.1.13") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1i3pmhmmcdw3rr1pv1p9yhm4danm5r156cpy7w30pa0s05fxk47m")))

(define-public crate-const-random-0.1.14 (c (n "const-random") (v "0.1.14") (d (list (d (n "const-random-macro") (r "^0.1.14") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0r5m4sbnrz6hlqsbzmqm2p1xrqz9crcg3bs4fz1ymq0ag69spxxc")))

(define-public crate-const-random-0.1.15 (c (n "const-random") (v "0.1.15") (d (list (d (n "const-random-macro") (r "^0.1.15") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "13n15ji2jpkkymd94wjgfknbhgaz916b1gw2vvhyfv5d5rvpm2in")))

(define-public crate-const-random-0.1.16 (c (n "const-random") (v "0.1.16") (d (list (d (n "const-random-macro") (r "^0.1.16") (d #t) (k 0)))) (h "14smhg7fkjca6kxx89gd2gz1y8d5mdgifg8xsm1fr4kq7nhk5pqi")))

(define-public crate-const-random-0.1.17 (c (n "const-random") (v "0.1.17") (d (list (d (n "const-random-macro") (r "^0.1.16") (d #t) (k 0)))) (h "16i9r34f5lmvrmvm5nsssywyjbg3yrqf2hnhrw5h44n6qb4idbss")))

(define-public crate-const-random-0.1.18 (c (n "const-random") (v "0.1.18") (d (list (d (n "const-random-macro") (r "^0.1.16") (d #t) (k 0)))) (h "0n8kqz3y82ks8znvz1mxn3a9hadca3amzf33gmi6dc3lzs103q47")))

