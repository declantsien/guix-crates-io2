(define-module (crates-io co ns const_env_impl--value) #:use-module (crates-io))

(define-public crate-const_env_impl--value-0.1.2 (c (n "const_env_impl--value") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fmqw62bya56zdvm0kfhq57ljiyh7y0r1dry9p2nccj5jrjy0im0")))

