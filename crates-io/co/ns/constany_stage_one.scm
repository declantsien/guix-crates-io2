(define-module (crates-io co ns constany_stage_one) #:use-module (crates-io))

(define-public crate-constany_stage_one-0.1.0 (c (n "constany_stage_one") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z49n349m3r2js47hvxrrjj2273ydh08cmxfr7j23hmnjpacjm50")))

(define-public crate-constany_stage_one-0.1.1 (c (n "constany_stage_one") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1416adjad5kpa7d3kdpdfda4kjx9whsxqaz9bx4k04pw3h712fz7")))

(define-public crate-constany_stage_one-0.2.0 (c (n "constany_stage_one") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hf8w3v9p1jzxr2kcpciw6ndf23v6skhnxm2my5h412bjfmhpy25")))

