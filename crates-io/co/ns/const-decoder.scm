(define-module (crates-io co ns const-decoder) #:use-module (crates-io))

(define-public crate-const-decoder-0.1.0 (c (n "const-decoder") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "pem") (r "^0.8.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "070r4x3a9ip9d7smgdj6wpalp94509bb4f74is3al6bxysasrdri")))

(define-public crate-const-decoder-0.2.0 (c (n "const-decoder") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "bech32") (r "^0.8.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "pem") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "014nwzlnrrzdfrl4g7ks0y9v1pxzq2gi246hdvmvxaqqjmsqqb9h") (f (quote (("nightly"))))))

(define-public crate-const-decoder-0.3.0 (c (n "const-decoder") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "bech32") (r "^0.9.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "pem") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0vssml5hh1xmn02vaiq3nl0dalsrc5psjgll5sa1bd5i71wwshaj") (r "1.57")))

