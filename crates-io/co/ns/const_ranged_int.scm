(define-module (crates-io co ns const_ranged_int) #:use-module (crates-io))

(define-public crate-const_ranged_int-0.1.0 (c (n "const_ranged_int") (v "0.1.0") (h "1jjd73nasrj672ifygz36hdd84dqyv1k81wwlb1my1x6kwchj3dq") (r "1.70")))

(define-public crate-const_ranged_int-0.1.1 (c (n "const_ranged_int") (v "0.1.1") (h "18fbpcx5h49l9wmy0pi05fi2axi5kbxl9amc56b70xa4ipc913b4") (r "1.70")))

(define-public crate-const_ranged_int-0.1.2 (c (n "const_ranged_int") (v "0.1.2") (h "090jpypn8rx8p4klj3129gbmrz7h50z2fk12c9nckvqs0wcj1aqg") (r "1.70")))

(define-public crate-const_ranged_int-0.1.3 (c (n "const_ranged_int") (v "0.1.3") (h "0bkncmqip4y83hv5pzhc1gq8xmy1ygd63j63x1si1x72fpxj2myv") (r "1.70")))

(define-public crate-const_ranged_int-0.1.4 (c (n "const_ranged_int") (v "0.1.4") (h "06xaq9s435kmw77pr2pyvangl9g601zvrfsazcgvbawp5aaca71c") (r "1.70")))

(define-public crate-const_ranged_int-0.1.5 (c (n "const_ranged_int") (v "0.1.5") (h "191bpk4kk88fmx46g6kjsqmj81k9zan18wgqjbzq34kcsv5qx4iv") (r "1.70")))

(define-public crate-const_ranged_int-0.1.6 (c (n "const_ranged_int") (v "0.1.6") (h "05djcmc5gmpqmls3ir6laak1hkk58n4i5r2ppyfnx674g7ymrnlx") (r "1.70")))

(define-public crate-const_ranged_int-0.1.7 (c (n "const_ranged_int") (v "0.1.7") (h "1l8w5g83600s6sgqprl5f49qfad2mdgk5f25hz58w37wzyn8s4qk") (r "1.70")))

(define-public crate-const_ranged_int-0.1.8 (c (n "const_ranged_int") (v "0.1.8") (h "1sd2ssbsqps3rxmck1lfcqbi5w1bl2apnv32ck7h6x2hvr90vzq8") (r "1.70")))

