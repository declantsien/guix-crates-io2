(define-module (crates-io co ns const-type-layout) #:use-module (crates-io))

(define-public crate-const-type-layout-0.1.0 (c (n "const-type-layout") (v "0.1.0") (d (list (d (n "const-type-layout-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0irrlz2l07k9hrrpnj5gd60x10rq5qzgc626wh1w5c5vl91z3fxi") (r "1.60")))

(define-public crate-const-type-layout-0.2.0 (c (n "const-type-layout") (v "0.2.0") (d (list (d (n "const-type-layout-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yikr17wwhs6w9avkllf21mq09ry6xq2bmqmgwk72gcbhm9im0q0") (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:const-type-layout-derive")))) (r "1.75")))

(define-public crate-const-type-layout-0.2.1 (c (n "const-type-layout") (v "0.2.1") (d (list (d (n "const-type-layout-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nchyczcqj6nfcrim3ykvm4l58jm13nmqyapvgvgsg3wi90s3a7l") (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:const-type-layout-derive")))) (r "1.75")))

(define-public crate-const-type-layout-0.3.0 (c (n "const-type-layout") (v "0.3.0") (d (list (d (n "const-type-layout-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wrpmhn4p565qbchahn78xj737iasfpncc01439a5kzhsaijmrqd") (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:const-type-layout-derive")))) (r "1.78")))

(define-public crate-const-type-layout-0.3.1 (c (n "const-type-layout") (v "0.3.1") (d (list (d (n "const-type-layout-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nj5fcyxah1mi9lijpmnwymfrk3y36as5ayj27xp9gywg6wfxb2x") (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:const-type-layout-derive")))) (r "1.78")))

