(define-module (crates-io co ns console-input) #:use-module (crates-io))

(define-public crate-console-input-0.1.1 (c (n "console-input") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1nl92v67bj3asq27aiqf7279v5yxg0mqs0z828pgsdab4fmbpbjc")))

(define-public crate-console-input-0.1.2 (c (n "console-input") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1jqjghvwdc9qrjq2kfjd11bwpsyskm43clnkcksqwv8x5p0p0hhc")))

