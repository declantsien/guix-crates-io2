(define-module (crates-io co ns constrained_type) #:use-module (crates-io))

(define-public crate-constrained_type-0.1.0 (c (n "constrained_type") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0271vxdjjyx4dbr22hy4vnsklin2j4dqfyvkqb9d1r4zxl4xszh4") (y #t)))

(define-public crate-constrained_type-0.1.1 (c (n "constrained_type") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1iw73mx0kxfd7i2nyrnikqxdbs0a3207w69smhk32w4jw25svb5d") (y #t)))

(define-public crate-constrained_type-0.1.1-alpha.1 (c (n "constrained_type") (v "0.1.1-alpha.1") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "15sakrrwx79g48gj9146zawqpbk8xcj766vbhfm8w2j0i72r12pn") (y #t)))

(define-public crate-constrained_type-0.1.2-alpha.1 (c (n "constrained_type") (v "0.1.2-alpha.1") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "13j10a46p9gaawf6ngpshnxjiy6bpnvjwv2m0m4lhzzpcy737kah") (y #t)))

(define-public crate-constrained_type-0.1.2-alpha.2 (c (n "constrained_type") (v "0.1.2-alpha.2") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0hz3a32aylss23kn7yypxgjaz1gyvbhfjf1xyixl4r300jgjvc05") (y #t)))

(define-public crate-constrained_type-0.1.2-alpha.3 (c (n "constrained_type") (v "0.1.2-alpha.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1wywbican651cf9x4c1sskcgh7m2gc454d0vsf3d08dln3dm7whk") (y #t)))

(define-public crate-constrained_type-0.1.3 (c (n "constrained_type") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1b42v245g46i23vn02c31hdjgm841d6vr62f0d9680x7gj4r7r9k") (y #t)))

(define-public crate-constrained_type-0.2.0 (c (n "constrained_type") (v "0.2.0") (d (list (d (n "fancy-regex") (r "^0.7.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "19rs704g7h6fnhcjxwx64w472d4sgskj2yrfvadb9m6mhjprxbfm")))

(define-public crate-constrained_type-0.2.1 (c (n "constrained_type") (v "0.2.1") (d (list (d (n "fancy-regex") (r "^0.7.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.1") (d #t) (k 0)))) (h "0xlhhcbc9mxymmrhr0cw2bk1b7lgas19kzwfvahdy1ncc8zkvaqf")))

(define-public crate-constrained_type-0.2.2 (c (n "constrained_type") (v "0.2.2") (d (list (d (n "fancy-regex") (r "^0.7.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.1") (d #t) (k 0)))) (h "0hjgbcwn8qswvw71637dnhnb6gqmx6is0z08ac76z2z6ap5fbb17")))

(define-public crate-constrained_type-0.2.3 (c (n "constrained_type") (v "0.2.3") (d (list (d (n "fancy-regex") (r "^0.7.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "zeroize") (r "^1.4.1") (d #t) (k 0)))) (h "0r5zgfdwnfnma4fllax4b175h5a8h6wijq483vw0vpnq52l70pb1")))

(define-public crate-constrained_type-0.2.4 (c (n "constrained_type") (v "0.2.4") (d (list (d (n "fancy-regex") (r "^0.7.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0wmjl2fkg9ilfcxpmmarsrrhnhkbpwxh5azbs32g443z81wblyqp")))

(define-public crate-constrained_type-0.2.5 (c (n "constrained_type") (v "0.2.5") (d (list (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0invfa59b3aqdwsv0w53arhq3my0fmn7jxz0ma2dq75vp2367958")))

