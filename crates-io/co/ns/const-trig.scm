(define-module (crates-io co ns const-trig) #:use-module (crates-io))

(define-public crate-const-trig-0.0.0 (c (n "const-trig") (v "0.0.0") (h "0zsaww6y1q17wj2zhp2sqx2qw5pqhavmxmirg9kpa9x87bnbl4i2")))

(define-public crate-const-trig-0.0.1 (c (n "const-trig") (v "0.0.1") (h "0njkg1n45gvxfnhdzfscvh84788cf2f12bf1xxx0nmlb7g13916r")))

(define-public crate-const-trig-0.0.2 (c (n "const-trig") (v "0.0.2") (h "18dlg6c0rv83lsz2wbdw39cj9qi73hhaya8irqkz1primz0p87wa")))

