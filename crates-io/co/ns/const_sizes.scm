(define-module (crates-io co ns const_sizes) #:use-module (crates-io))

(define-public crate-const_sizes-0.1.0 (c (n "const_sizes") (v "0.1.0") (h "1iqj9qzicxc4i8ml8i8x4lcp6m3rm0b4n3mksi9nclz7kz9jbcvd") (r "1.78")))

(define-public crate-const_sizes-0.1.1 (c (n "const_sizes") (v "0.1.1") (h "0zs3i4vax5qgigls3j2c87dy15ijr7kal88knysbgnvangig6n5z") (r "1.78")))

