(define-module (crates-io co ns const-arithmetic) #:use-module (crates-io))

(define-public crate-const-arithmetic-1.0.0 (c (n "const-arithmetic") (v "1.0.0") (d (list (d (n "const-arith-macros-178") (r "~0.1.0") (d #t) (k 0)))) (h "0l4h15iqv1yphxqxyrrqd2fbxj4fgvra3nn7fs2z0s19r04v9c8q")))

(define-public crate-const-arithmetic-1.0.1 (c (n "const-arithmetic") (v "1.0.1") (d (list (d (n "const-arith-macros-178") (r "~0.1.0") (d #t) (k 0)))) (h "026g3hrnfivi0f68mz4hq8qkr05pixlbdliimyl2fs0alrw1qhgs") (y #t)))

(define-public crate-const-arithmetic-1.0.2 (c (n "const-arithmetic") (v "1.0.2") (d (list (d (n "const-arith-macros-178") (r "~0.1.0") (d #t) (k 0)))) (h "1gv950kviq0fxz16hfn6m9km0sxg6lbz2454w0jx14yp90217pmd")))

(define-public crate-const-arithmetic-1.0.3 (c (n "const-arithmetic") (v "1.0.3") (d (list (d (n "const-arith-macros-178") (r "~0.1.0") (d #t) (k 0)))) (h "03linbm59k4vvibxqbqpcb247zy9qgdcrigidlipvxyn0w14fhrl")))

(define-public crate-const-arithmetic-1.0.4 (c (n "const-arithmetic") (v "1.0.4") (d (list (d (n "const-arith-macros-178") (r "~0.1.2") (d #t) (k 0)))) (h "0bq80sqz6ydfpzzrjpl1xfjbxai6gb10zccv513xscr8jmzb9178")))

