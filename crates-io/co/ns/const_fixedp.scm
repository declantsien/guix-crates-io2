(define-module (crates-io co ns const_fixedp) #:use-module (crates-io))

(define-public crate-const_fixedp-0.1.0 (c (n "const_fixedp") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "003zd4j3r98ly8z6gjp5dpy83fjl9fnsbnbbpdindplpvhs2byk6") (f (quote (("default"))))))

(define-public crate-const_fixedp-0.1.1 (c (n "const_fixedp") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1v67zy79lzw607s3j5fhrfx1ppahnqb1arg71yvjl437qvrkfdx7") (f (quote (("default"))))))

