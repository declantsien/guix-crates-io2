(define-module (crates-io co ns constrained) #:use-module (crates-io))

(define-public crate-constrained-0.1.0 (c (n "constrained") (v "0.1.0") (d (list (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "sprs") (r "^0.3.0") (d #t) (k 0)))) (h "1y4jddx4qsk26xk16ii927c2m1xh2w3jyrph5n9r9aypix8hkhkr")))

