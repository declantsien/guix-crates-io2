(define-module (crates-io co ns const_traits) #:use-module (crates-io))

(define-public crate-const_traits-0.1.0 (c (n "const_traits") (v "0.1.0") (d (list (d (n "const_ops") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0kgvflfvj2q6487jj5775mgjsf80gqv3r47z4gw62s0r824ncfl5") (f (quote (("ops" "const_ops") ("default" "ops"))))))

(define-public crate-const_traits-0.2.0 (c (n "const_traits") (v "0.2.0") (d (list (d (n "const_ops") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1n3g68agvp2gxng79ac8m6s6za246pw57cf4kq1lrkh4bka9c69n") (f (quote (("ops" "const_ops") ("default" "ops"))))))

(define-public crate-const_traits-0.3.0 (c (n "const_traits") (v "0.3.0") (d (list (d (n "const_ops") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1ljs9abkmc1pi5asrscg13nygir02fiv6qb31kghd8ihplx03kj8") (f (quote (("ops" "const_ops") ("default"))))))

