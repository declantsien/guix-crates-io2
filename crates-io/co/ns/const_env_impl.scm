(define-module (crates-io co ns const_env_impl) #:use-module (crates-io))

(define-public crate-const_env_impl-0.1.0 (c (n "const_env_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lq4mb8qsgi7h0c5fsb4zr0arlqvwbv6mjy4vn6h64hjrjjjqr5z")))

(define-public crate-const_env_impl-0.1.1 (c (n "const_env_impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13x54z2r195xbrpydy1vsvlpjc7fdzdsz7012pkj5walp8a6b22s")))

(define-public crate-const_env_impl-0.1.2 (c (n "const_env_impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qidhpxzybqr0i4l1nmn87pjqmnlrm230avhkrcf3da0jwh52krs")))

(define-public crate-const_env_impl-0.1.3 (c (n "const_env_impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1awybgcnclv66hfs6hxv557z995rkpj6v9vshcqmmasmabcv1xi8")))

