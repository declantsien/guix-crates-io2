(define-module (crates-io co ns const_fn) #:use-module (crates-io))

(define-public crate-const_fn-0.1.0 (c (n "const_fn") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0s16gs06dbzcrh9npv5k3ir481zj4gl7micwlv8jjhfi8hw36fsr")))

(define-public crate-const_fn-0.1.1 (c (n "const_fn") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "11wyl71j948j2y9s9f2h1i4gi471agv34n2kwkllsv0bd375y9ng")))

(define-public crate-const_fn-0.1.2 (c (n "const_fn") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full"))) (d #t) (k 0)))) (h "1as14xq5py7gjq2183gsq2sf5nqmj60cnf761mnk1szjffz3qb77")))

(define-public crate-const_fn-0.1.3 (c (n "const_fn") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.9") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "1yifz125c8kxlrymywj9haf1p0synv8bs0vxxdv78ll1ywy6vsb8")))

(define-public crate-const_fn-0.1.4 (c (n "const_fn") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "syn-mid") (r "^0.1.0") (d #t) (k 0)))) (h "0f89rxd88gpqxmg5g9ngxyi4nc5vynr3hhgyci8c0xfhih9k4hpm") (y #t)))

(define-public crate-const_fn-0.1.5 (c (n "const_fn") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.9") (f (quote ("proc-macro" "parsing" "printing" "clone-impls" "full"))) (k 0)))) (h "0hb68wb1sxx27iq9fzp85wn01dlmcmxqbcb54bp6468783dfv0nb")))

(define-public crate-const_fn-0.1.6 (c (n "const_fn") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "syn-mid") (r "^0.2.0") (d #t) (k 0)))) (h "18i5sqd83gmv1wiffykkv811dh9vp4ygs888svdlxd4h16rch09c")))

(define-public crate-const_fn-0.1.7 (c (n "const_fn") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "syn-mid") (r "^0.3.0") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "1166403r1jxb0zdzh2kdwv0bpv4218ch2xisxa60ydjvwa6n307x")))

(define-public crate-const_fn-0.2.0 (c (n "const_fn") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (d #t) (k 0)) (d (n "syn-mid") (r "^0.3.0") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "0hh45d902xlq7s9gxammpis0878pv1s2dk2axapnkixgdi2j4h4i")))

(define-public crate-const_fn-0.2.1 (c (n "const_fn") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn-mid") (r "^0.4") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "1x1hd8hhda06m521pc5xl0nzqf18lzh37rzyi4h8vx24g3hnzbcs")))

(define-public crate-const_fn-0.3.0 (c (n "const_fn") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn-mid") (r "^0.4") (d #t) (k 0)))) (h "0w2l9j7f7lsq81pddb7rpsmkvlvvdhaws119bvfrfg6dhmbhjs2l")))

(define-public crate-const_fn-0.3.1 (c (n "const_fn") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn-mid") (r "^0.5") (d #t) (k 0)))) (h "0cg7vfinw5h3rc2l46p43a2dykjpn8jsgiqhsv3j5qxyg90icqbi")))

(define-public crate-const_fn-0.4.0 (c (n "const_fn") (v "0.4.0") (d (list (d (n "version_check") (r "^0.9.2") (d #t) (k 1)))) (h "0y494xxj0rh2mj9dv425803mw1l84yialz53s5mmxwpl8lhf33v8")))

(define-public crate-const_fn-0.4.1 (c (n "const_fn") (v "0.4.1") (d (list (d (n "version_check") (r "^0.9.2") (d #t) (k 1)))) (h "1ybxy8l9fh69sh5fb9xfs5lhmsdjnbnczgfs3y5c2lw218zvjzbf")))

(define-public crate-const_fn-0.4.2 (c (n "const_fn") (v "0.4.2") (h "1wnhzyrhfcaawnzi172k98cfawwi5zwqql7pg0nz2qlccm6dz46f")))

(define-public crate-const_fn-0.4.3 (c (n "const_fn") (v "0.4.3") (h "1axgb6p2b0mdazi000vf0hd7hky690s052bwnizz3klx09p86y64")))

(define-public crate-const_fn-0.4.4 (c (n "const_fn") (v "0.4.4") (h "09i8lsi55rhrx4jh69h4mmhklvhaijax1ql9z0xnmzdl3arfllfd")))

(define-public crate-const_fn-0.4.5 (c (n "const_fn") (v "0.4.5") (h "19plqg6q4i2a1grphpzl68030sffdq1w8zyigbwjrqj9gzgddf98")))

(define-public crate-const_fn-0.4.6 (c (n "const_fn") (v "0.4.6") (h "0a0azld0qa1qd5z6hrgjbvzk65dh52kfnkg6ry46mkfsn01nhsh7")))

(define-public crate-const_fn-0.4.7 (c (n "const_fn") (v "0.4.7") (h "1v62p2rpbsbzw9xs9yb1jyzv1daygxdlihzgx9vgkqsx950ahba0")))

(define-public crate-const_fn-0.4.8 (c (n "const_fn") (v "0.4.8") (h "1rzn3ifnsgqh0lmzkqgm7jjjzwkykfysnb7gq7w3q2v9sl7zlb7r")))

(define-public crate-const_fn-0.4.9 (c (n "const_fn") (v "0.4.9") (h "0df9fv9jhnh9b4ni3s2fbfcvq77iia4lbb89fklwawbgv2vdrp7v") (r "1.31")))

(define-public crate-const_fn-0.4.10 (c (n "const_fn") (v "0.4.10") (h "038x468fyyyfwxzhg415m860xmaqzxsj4mi0vdv2i210maprygip") (r "1.31")))

