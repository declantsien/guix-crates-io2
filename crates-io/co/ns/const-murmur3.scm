(define-module (crates-io co ns const-murmur3) #:use-module (crates-io))

(define-public crate-const-murmur3-0.1.0 (c (n "const-murmur3") (v "0.1.0") (h "0jr1r5ffv7c9ymlqsfrs7k0p66fcrgcz5rbzb0fvdwpqv0z4n46n")))

(define-public crate-const-murmur3-0.2.1 (c (n "const-murmur3") (v "0.2.1") (h "1qhfy4pk681yhrbw29lxh8f0ir145w0b9jyyk0w05nf2vf8xrw4q")))

