(define-module (crates-io co ns const-utf16) #:use-module (crates-io))

(define-public crate-const-utf16-0.1.0 (c (n "const-utf16") (v "0.1.0") (h "02klbpymd1qbp96l3fgfcicbdh4ds6xj62gami1a7m8m0fm9rva7")))

(define-public crate-const-utf16-0.1.1 (c (n "const-utf16") (v "0.1.1") (h "0nfwz2ir7li9l8xijb44c6gq3qzhpvhwj6m7y2lm05gbf2kj8hv4")))

(define-public crate-const-utf16-0.1.2 (c (n "const-utf16") (v "0.1.2") (h "0cs0kii8fwlzflqgnxiqd0cbgkb098fggvsr1inns51g15kqnmpm")))

(define-public crate-const-utf16-0.2.0 (c (n "const-utf16") (v "0.2.0") (h "1bwdfcb78bavqxssn7m7dbpbkd6h7x91jpk9ry8zgklj9w4aj040")))

(define-public crate-const-utf16-0.2.1 (c (n "const-utf16") (v "0.2.1") (h "11c27pcpnka53bw4y1gmv75hy63vidqg1qivdrs13q2z2smyzzlh")))

