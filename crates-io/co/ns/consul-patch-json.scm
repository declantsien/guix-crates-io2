(define-module (crates-io co ns consul-patch-json) #:use-module (crates-io))

(define-public crate-consul-patch-json-0.1.0 (c (n "consul-patch-json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "consulrs") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07gn1ysdppk7hyf7jn4msjl2rnd6kgrxzh68vr1xaxrk62134fb5")))

(define-public crate-consul-patch-json-0.2.0 (c (n "consul-patch-json") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "consulrs") (r "^0.1.0") (d #t) (k 0)) (d (n "json-patch") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sdhwdr27k6im0d0y7hyzpwf7v9a84ra57fzw4ffk91rlrpn7225")))

(define-public crate-consul-patch-json-0.3.0 (c (n "consul-patch-json") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "consulrs") (r "^0.1.0") (d #t) (k 0)) (d (n "json-patch") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13vcipsswk166p01jsb97m7qsr4225nzy16dz8xqfc5wl8x29bbs")))

