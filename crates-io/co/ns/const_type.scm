(define-module (crates-io co ns const_type) #:use-module (crates-io))

(define-public crate-const_type-0.1.0 (c (n "const_type") (v "0.1.0") (h "1zcxaxxakbrfsl43hs89m00rri651vbvw00cbwbydvm932f45ck6")))

(define-public crate-const_type-0.1.1 (c (n "const_type") (v "0.1.1") (h "0qcww84cny1jar62l3299lxpg87x0ayvl9vn9aigazws4vn1scbk")))

(define-public crate-const_type-0.1.3 (c (n "const_type") (v "0.1.3") (h "0s00i6lfi0mr753yrvqp4s9sa3i40h3c44yh6n7ylikxgc30amny")))

