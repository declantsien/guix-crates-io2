(define-module (crates-io co ns constany_stage_two) #:use-module (crates-io))

(define-public crate-constany_stage_two-0.1.0 (c (n "constany_stage_two") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n88rvdfif668zp6058qal007f5q7jqh6vibzjdpgb1msr4nx3ca")))

(define-public crate-constany_stage_two-0.1.1 (c (n "constany_stage_two") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vdnplnbmv0rwbrj6131fvv67z7qvyszkjp8p40rmxghbfqwqpnr")))

(define-public crate-constany_stage_two-0.2.0 (c (n "constany_stage_two") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "seahash") (r "^4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12z2czby2iw880glqjicgjn4rfnnbxbz8wrbm1y5i3zl6abqf8wl")))

