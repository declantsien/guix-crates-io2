(define-module (crates-io co ns const-identify-derive) #:use-module (crates-io))

(define-public crate-const-identify-derive-0.1.0 (c (n "const-identify-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gsgagix89rsillw67kwx8fimqi9j8d52q0lp8c43jnj41m13q3n")))

(define-public crate-const-identify-derive-0.1.1 (c (n "const-identify-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n098i90q29v9y9vkdq62v3mr8ar1gr3p4iaz07fnxxsb6idk0ah")))

