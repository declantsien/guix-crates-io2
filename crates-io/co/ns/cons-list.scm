(define-module (crates-io co ns cons-list) #:use-module (crates-io))

(define-public crate-cons-list-0.0.1 (c (n "cons-list") (v "0.0.1") (h "16pr65hkgrn649a1kfpnp7am1y109b4yxx7mvvrjcs1xwsvvvyqj")))

(define-public crate-cons-list-0.0.2 (c (n "cons-list") (v "0.0.2") (h "1qlqq95jrpq7jggafqjfh8dqqkcys66c138w27nmsgc800yyj008")))

(define-public crate-cons-list-0.0.3 (c (n "cons-list") (v "0.0.3") (h "0wni5l59c97hf3nj6qlw9f67xkrc92b7jsdd4b8qx8gz3w6sfk5j")))

