(define-module (crates-io co ns constructivism) #:use-module (crates-io))

(define-public crate-constructivism-0.0.1 (c (n "constructivism") (v "0.0.1") (d (list (d (n "constructivism_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "tuple_utils") (r "^0.4.0") (d #t) (k 0)))) (h "1mzyhl4jc78vvbyiil3v4hqmpxkypsyjg6yifwlqh73wgaamdwr2")))

(define-public crate-constructivism-0.0.2 (c (n "constructivism") (v "0.0.2") (d (list (d (n "constructivism_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "tuple_utils") (r "^0.4.0") (d #t) (k 0)))) (h "0jd62l16bdpgaq2sakbzn2ilwnvcxs63w96rjp4qmakvp3kfg30s")))

(define-public crate-constructivism-0.3.0 (c (n "constructivism") (v "0.3.0") (d (list (d (n "constructivism_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "tuple_utils") (r "^0.4.0") (d #t) (k 0)))) (h "1c13bag3gn1j48zg4msx8a2ccdzdzmlcbx28fwg5wvsbmwnbnchn")))

