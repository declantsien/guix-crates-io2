(define-module (crates-io co ns const-field-offset-macro) #:use-module (crates-io))

(define-public crate-const-field-offset-macro-0.1.0 (c (n "const-field-offset-macro") (v "0.1.0") (d (list (d (n "memoffset") (r "^0.5.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lw8rsfk7b5cxc4h71i4xdzhxh5r5nqxvz067harq8jxd0s9rlh6") (f (quote (("field-offset-trait"))))))

(define-public crate-const-field-offset-macro-0.1.1 (c (n "const-field-offset-macro") (v "0.1.1") (d (list (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ag6qmki3rv2412c9x096w0gpb7xxvnahg5bvzlcxw7i5j95rwab") (f (quote (("field-offset-trait"))))))

(define-public crate-const-field-offset-macro-0.1.2 (c (n "const-field-offset-macro") (v "0.1.2") (d (list (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zysjfmgd29x1iiqp9vcfmr6i6y1p8xjkcfsr87hl7vrk8dzgxx7") (f (quote (("field-offset-trait"))))))

(define-public crate-const-field-offset-macro-0.1.3 (c (n "const-field-offset-macro") (v "0.1.3") (d (list (d (n "memoffset") (r "^0.8.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lbgypnhas6m5pv87nn2ipmb0bqbls5rsm33zqxcnfsx33csmajp") (f (quote (("field-offset-trait"))))))

(define-public crate-const-field-offset-macro-0.1.4 (c (n "const-field-offset-macro") (v "0.1.4") (d (list (d (n "memoffset") (r "^0.9.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "135igqj4d9gcxv1pwnrq0j71hhv621h1v4w5xrb9sawqnd0z1hbp") (f (quote (("field-offset-trait"))))))

(define-public crate-const-field-offset-macro-0.1.5 (c (n "const-field-offset-macro") (v "0.1.5") (d (list (d (n "memoffset") (r "^0.9.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "143c8s8siry112y9mx37mkwfnk312bx5l4pa6rjckrp9r6xzb1sk") (f (quote (("field-offset-trait"))))))

