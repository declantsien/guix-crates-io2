(define-module (crates-io co ns const-either) #:use-module (crates-io))

(define-public crate-const-either-0.1.0 (c (n "const-either") (v "0.1.0") (h "1lqq7qhz6qzqaxbpq4nq08w19pcl8cl608is02f6jw34h58i0nvd")))

(define-public crate-const-either-0.1.1 (c (n "const-either") (v "0.1.1") (h "0scrkd5bj30hya1gn8q0ma0afp89bbwnfnyklhnrfzmldnigp7x7")))

