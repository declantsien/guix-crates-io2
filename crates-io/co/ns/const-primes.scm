(define-module (crates-io co ns const-primes) #:use-module (crates-io))

(define-public crate-const-primes-0.1.0 (c (n "const-primes") (v "0.1.0") (h "1a7yar1gak3gb9hsgwf1wy7j9mb494ikzhrr32nqa2nxa1ra3bhx")))

(define-public crate-const-primes-0.1.1 (c (n "const-primes") (v "0.1.1") (h "1qdnhb7a628lagh5zn988m9xbipp98ikw4lcw15igmf9gx06f9ji")))

(define-public crate-const-primes-0.1.2 (c (n "const-primes") (v "0.1.2") (h "1g8vswfr8idpcac2jdjayj3kyzd4fcp6wljb9a43b8511qp1gil3")))

(define-public crate-const-primes-0.1.3 (c (n "const-primes") (v "0.1.3") (h "0vpjzkj8snvdz2zspmq0g9adwssnibisfi1jcmsc0y862cciqkpj")))

(define-public crate-const-primes-0.2.0 (c (n "const-primes") (v "0.2.0") (h "0yflc4ys4y54xkm79f4akqbfgpdyingirc4r2qmyrgh2bpawgn2l")))

(define-public crate-const-primes-0.3.0 (c (n "const-primes") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0dp6rvmly1jvibdp2vhb0pfni8a3yr5nws4q7srbpbv98nh5k6vv")))

(define-public crate-const-primes-0.3.1 (c (n "const-primes") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0hs81m0blnci924prgmwkj42255id3rzs0xk7aizlsf4x3r4cdfm")))

(define-public crate-const-primes-0.3.2 (c (n "const-primes") (v "0.3.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "00rg82bm07fiyif3kispnn33dffkcn10ymbbwchlnqr2kimyccyi")))

(define-public crate-const-primes-0.3.3 (c (n "const-primes") (v "0.3.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0gmvbz1jg4kmf3fq0anrp0ha3fxdgq76qb7v32k55gc6z604yab8")))

(define-public crate-const-primes-0.3.4 (c (n "const-primes") (v "0.3.4") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1nibfr7wxsbhq6yhnz4y2c362h5bmksp8kwl72g82dgibxbkdv3x")))

(define-public crate-const-primes-0.3.5 (c (n "const-primes") (v "0.3.5") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "170v4zdvg2rmgh3vayqb9ylzc0ffxxpxafjnq17mh9lzv8fbllb8")))

(define-public crate-const-primes-0.4.0 (c (n "const-primes") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0wf3gmnfkkjs9hap09vgp6s5lq2lbsaahr8dm8k03sb3cwhh9wmn")))

(define-public crate-const-primes-0.4.1 (c (n "const-primes") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1pl1drmnv534majwxn17spglfcgha579wmyx3psn2k0czrgb11r6")))

(define-public crate-const-primes-0.4.2 (c (n "const-primes") (v "0.4.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1lycswz8i1gg8lpjhslq4g1p34l3wkdzakyfr16iifr7969n3na5")))

(define-public crate-const-primes-0.4.3 (c (n "const-primes") (v "0.4.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1bn8ribykx8smw9xm0c4v9ccg6hxd0d95z3cjd1y51xg7cm9p400")))

(define-public crate-const-primes-0.4.4 (c (n "const-primes") (v "0.4.4") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1q4v6rn7g2y3l5hr4c1a2fvjvhw9qi1ic7zm9znrcycv9kfvlvwf")))

(define-public crate-const-primes-0.4.5 (c (n "const-primes") (v "0.4.5") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1dkl7nyg5kw78hjh83kifdadnp3f1fj26kjj5b1k8k4lcp0j5xbq")))

(define-public crate-const-primes-0.4.6 (c (n "const-primes") (v "0.4.6") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0ihnixjp3ly8bgh10rj6prrdvl3x0f43d5y05d4byb5wzhhhwlys")))

(define-public crate-const-primes-0.4.7 (c (n "const-primes") (v "0.4.7") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "09xwa7hk1s6q09ylkzz69rpw0akh4lhbvs5kbrs74c7xm0s2p9v4")))

(define-public crate-const-primes-0.4.8 (c (n "const-primes") (v "0.4.8") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1fq7npp1hnhx5zr58f4pc7d8m6z2a1y2jxvpdiri8c888mgrn2am")))

(define-public crate-const-primes-0.5.0 (c (n "const-primes") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1999x35qyrmnpyfzh24yvgf78c19vyhjl459k2g6g3c48m13bab0") (f (quote (("std"))))))

(define-public crate-const-primes-0.5.1 (c (n "const-primes") (v "0.5.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "02dsfszfas7sjy3njnp1vpi1hsnf17474rvh0558ghdda30cimd4") (f (quote (("std"))))))

(define-public crate-const-primes-0.6.0 (c (n "const-primes") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0q7h9nla1ggyrmgxqzb2hy9wwr4jmvgh8612xr8lh7gmz1g68i25") (f (quote (("std"))))))

(define-public crate-const-primes-0.6.1 (c (n "const-primes") (v "0.6.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0pywfq2skwjx3sbhqgj9szbaq0mds41ky4ggln8zbpagyhg1xn0s") (f (quote (("std"))))))

(define-public crate-const-primes-0.6.2 (c (n "const-primes") (v "0.6.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0kfxgp76a3fpq9714wir87jprnzkqm5z9dp0q3xjdrivj1bxdwcm") (f (quote (("std"))))))

(define-public crate-const-primes-0.7.0 (c (n "const-primes") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0yy1bp5aiygixrnb8182x1a4gq6ycl3sm3cncsndczcdr8pjdkv6") (f (quote (("std"))))))

(define-public crate-const-primes-0.7.1 (c (n "const-primes") (v "0.7.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "098z2skhhnrccp73dzphwh2i8dl6fqnx5il1as55jk8653dkr9by") (f (quote (("std"))))))

