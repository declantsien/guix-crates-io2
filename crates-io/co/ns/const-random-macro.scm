(define-module (crates-io co ns const-random-macro) #:use-module (crates-io))

(define-public crate-const-random-macro-0.1.0 (c (n "const-random-macro") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1pgsrjc94bipvrng2pvjd5lc6gq98jw6p8nbbblsycjn1xyrgqw9")))

(define-public crate-const-random-macro-0.1.1 (c (n "const-random-macro") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "15zwf0kbhji7sr8gd4ymgcb72xbzdpz9za45i23k44jfz9xwqg8k")))

(define-public crate-const-random-macro-0.1.2 (c (n "const-random-macro") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1a1ay3if6x6nqdpabmjdkvvz642rm8lbgvbix1ykbf3jjaq898lj")))

(define-public crate-const-random-macro-0.1.3 (c (n "const-random-macro") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1iaq26j186m58qvm7x7v7xays0qqgzbkjpikmxg30hacjf1q9wyd")))

(define-public crate-const-random-macro-0.1.4 (c (n "const-random-macro") (v "0.1.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("getrandom"))) (k 0)))) (h "01mmihj6r3x9v38kfmwksgk47zgbgf3q7mbqvm5azrn37gw28bz7")))

(define-public crate-const-random-macro-0.1.5 (c (n "const-random-macro") (v "0.1.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("getrandom"))) (k 0)))) (h "1yd8iqjk4yda1qqr0h6mlf5kml63zixk6j28y0zfzbx0731zk9gc")))

(define-public crate-const-random-macro-0.1.6 (c (n "const-random-macro") (v "0.1.6") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("getrandom"))) (k 0)))) (h "0ncc88bzfkm9y7djnvmiqh35p2cfw1d7z9fm21qn6xrkp09fql67")))

(define-public crate-const-random-macro-0.1.7 (c (n "const-random-macro") (v "0.1.7") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "15jqgqggcxcdk4a1spd5kyryxna7px6iz85qzparxkr3za6q3bak")))

(define-public crate-const-random-macro-0.1.8 (c (n "const-random-macro") (v "0.1.8") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0ykc9riajn6bijvw46092gp18vrbky3y1cjpgjgx57a5xc3cdr15")))

(define-public crate-const-random-macro-0.1.10 (c (n "const-random-macro") (v "0.1.10") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.13") (d #t) (k 0)))) (h "1fmam3c75i3h5xgyq0rxixfj9i8rzj690i5n2h3v2kfav0cdjw8f")))

(define-public crate-const-random-macro-0.1.11 (c (n "const-random-macro") (v "0.1.11") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.13") (d #t) (k 0)))) (h "11b6dwq29x8czvg9g47551zb71pqh6g6f37059n2kaj4jnxpnxgw")))

(define-public crate-const-random-macro-0.1.12 (c (n "const-random-macro") (v "0.1.12") (d (list (d (n "getrandom") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.13") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3"))) (d #t) (k 0)))) (h "1gihwasrzkghphc7rkz454clqav74yf036qh24iwaghfyy7lva29")))

(define-public crate-const-random-macro-0.1.13 (c (n "const-random-macro") (v "0.1.13") (d (list (d (n "getrandom") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.13") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("sha3"))) (d #t) (k 0)))) (h "0h7vvskw1pw5x44sbl74gsi8ydvrj5kaixpjqzxvz8h0s0knwpv1")))

(define-public crate-const-random-macro-0.1.14 (c (n "const-random-macro") (v "0.1.14") (d (list (d (n "getrandom") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.13") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("shake"))) (d #t) (k 0)))) (h "170dqi8a1yv6k7s5i8vy8420dvlcw9dj2giwfwba26h2mfzrar4w")))

(define-public crate-const-random-macro-0.1.15 (c (n "const-random-macro") (v "0.1.15") (d (list (d (n "getrandom") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (f (quote ("race" "alloc"))) (k 0)) (d (n "proc-macro-hack") (r "^0.5.13") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("shake"))) (d #t) (k 0)))) (h "1nqdbkh0zwvh82ypxyzhg82sgq5nvb204pyz22r2sa52qfrnlzcx")))

(define-public crate-const-random-macro-0.1.16 (c (n "const-random-macro") (v "0.1.16") (d (list (d (n "getrandom") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (f (quote ("race" "alloc"))) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("shake"))) (d #t) (k 0)))) (h "03iram4ijjjq9j5a7hbnmdngj8935wbsd0f5bm8yw2hblbr3kn7r")))

