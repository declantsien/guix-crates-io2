(define-module (crates-io co ns const_queue) #:use-module (crates-io))

(define-public crate-const_queue-0.1.0 (c (n "const_queue") (v "0.1.0") (h "0ffvx8yzc5bnbjxicysxf3j39ygxmdqgsq83qr17kv5961984ih2")))

(define-public crate-const_queue-0.1.1 (c (n "const_queue") (v "0.1.1") (h "1sa8cyalwkim26hhjy6zl09z3ifx9myw5b2fgc7pmrybcnaz45g3")))

