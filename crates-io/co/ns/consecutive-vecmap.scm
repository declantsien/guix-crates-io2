(define-module (crates-io co ns consecutive-vecmap) #:use-module (crates-io))

(define-public crate-consecutive-vecmap-0.1.0 (c (n "consecutive-vecmap") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0822cbgvh997ggzjbckb4j32h8rcc7wqi063j3zjswmjgf2myr8n")))

(define-public crate-consecutive-vecmap-0.1.1 (c (n "consecutive-vecmap") (v "0.1.1") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0hd05z9lxx7v67bmy5hh9k5z6xz7153w0yshwg2rz28h1dp7rjkz")))

