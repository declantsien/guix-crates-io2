(define-module (crates-io co ns const-zero) #:use-module (crates-io))

(define-public crate-const-zero-0.1.0 (c (n "const-zero") (v "0.1.0") (h "0dlgw2j4c5zh7wsmzsxa275606g3hbnavlhbdvcc5wm1323ddlx1")))

(define-public crate-const-zero-0.1.1 (c (n "const-zero") (v "0.1.1") (h "1bq6s34a8v01rx6cpy3zslycgssmmasbkgm0blif6vwq4iamdim3")))

