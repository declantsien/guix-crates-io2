(define-module (crates-io co ns const-sha1) #:use-module (crates-io))

(define-public crate-const-sha1-0.1.0 (c (n "const-sha1") (v "0.1.0") (h "134md4n8qw3r6dlwivrbcl97cmszygkzq27d93wp020w582hz3y2")))

(define-public crate-const-sha1-0.1.1 (c (n "const-sha1") (v "0.1.1") (h "1z1wmbcnxlhgzsahqagyx4slg25jbsjyb13b26iym2sa5c957kkz")))

(define-public crate-const-sha1-0.2.0 (c (n "const-sha1") (v "0.2.0") (h "179cgi2m3wj5g80j49pggs95xalc6y1ivvbrv4m82alc3r2vcn7v")))

(define-public crate-const-sha1-0.3.0 (c (n "const-sha1") (v "0.3.0") (h "1zrw1bx35f4pdrrpj8pxa9x37ib3bgr1glp4gacw4lh63qc452hd") (f (quote (("std") ("default" "std"))))))

