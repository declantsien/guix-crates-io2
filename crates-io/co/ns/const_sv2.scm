(define-module (crates-io co ns const_sv2) #:use-module (crates-io))

(define-public crate-const_sv2-0.1.0 (c (n "const_sv2") (v "0.1.0") (h "0dlnkyq3kjf72i8ifsnzzxwf1av0mrgn657r2ajcp6ls5ih7h0r6")))

(define-public crate-const_sv2-0.1.1 (c (n "const_sv2") (v "0.1.1") (h "0s06mv3zp8zmgrh4xh662h5k9a2nwmmhc1s96i1hvl0iddbdi6i5")))

(define-public crate-const_sv2-0.1.3 (c (n "const_sv2") (v "0.1.3") (d (list (d (n "secp256k1") (r "^0.28.2") (f (quote ("hashes" "alloc" "rand" "rand-std"))) (k 0)))) (h "0clwb76l1vcw8f417qg61vm7qpsqmv3yfk4zz234qq1kv9vnrs2j")))

(define-public crate-const_sv2-1.0.0 (c (n "const_sv2") (v "1.0.0") (d (list (d (n "secp256k1") (r "^0.28.2") (f (quote ("hashes" "alloc" "rand" "rand-std"))) (k 0)))) (h "1xvvz801zpas36sjgmqvs1f4g52wnqhxd9mw24gpi2274vrqg1bi")))

