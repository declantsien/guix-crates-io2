(define-module (crates-io co ns consul-kv-trigger) #:use-module (crates-io))

(define-public crate-consul-kv-trigger-0.1.0 (c (n "consul-kv-trigger") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "rs-consul") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry" "env-filter"))) (d #t) (k 2)))) (h "0r5awjn6pndgy5ldpjin5ap8xa2p9wkar1m6zfg3db7fbgimfvya")))

