(define-module (crates-io co ns const_graphs) #:use-module (crates-io))

(define-public crate-const_graphs-0.1.0 (c (n "const_graphs") (v "0.1.0") (h "1im1h49gjqbbkxrnhwmw9n5kiy6ccmcncmc7mpp7xc4bl8c0smkl") (y #t)))

(define-public crate-const_graphs-0.1.1 (c (n "const_graphs") (v "0.1.1") (h "02cd1bm7igjaggfnrgvn1fwzrz1sgcrmpkc52syv5nl0kcgkkih0") (y #t)))

(define-public crate-const_graphs-0.2.0 (c (n "const_graphs") (v "0.2.0") (h "176slckgsfspvv9n35vzak8a5q8872dxlszah0hlm3cj4fvadyra") (y #t)))

(define-public crate-const_graphs-0.2.1 (c (n "const_graphs") (v "0.2.1") (h "11yyk0g5fimzmkjh5jv4zvm43ys3ffs93w6q7iacb9m7wf14a4lv") (y #t)))

(define-public crate-const_graphs-0.2.2 (c (n "const_graphs") (v "0.2.2") (h "1w3ab0qfivbv0bkcnx9a7wr6myys9njn57x5pdii5h4b3qby575x") (f (quote (("std"))))))

