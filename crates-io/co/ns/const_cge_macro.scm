(define-module (crates-io co ns const_cge_macro) #:use-module (crates-io))

(define-public crate-const_cge_macro-0.0.0 (c (n "const_cge_macro") (v "0.0.0") (h "0xjj2cqp4anr53dh83vr9qx3c0wbcqdgabvj7vlk72gx71sbfp00")))

(define-public crate-const_cge_macro-0.1.0 (c (n "const_cge_macro") (v "0.1.0") (d (list (d (n "cge") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pa5r9a9v6aqc887z4v6jdjijqbalas2qaxl24rrr81mfdck8iy4") (y #t)))

(define-public crate-const_cge_macro-0.1.1 (c (n "const_cge_macro") (v "0.1.1") (d (list (d (n "cge") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kb2v18xm9np05843rpw18pp835qi80r1drlw4i0484hi4fq6mkn") (y #t)))

(define-public crate-const_cge_macro-0.2.0 (c (n "const_cge_macro") (v "0.2.0") (d (list (d (n "cge") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15pnbhl6hd5scwspkxbzrcx9ksqx59wrjdh36x1g07mf8ivc9qa7")))

(define-public crate-const_cge_macro-0.3.0 (c (n "const_cge_macro") (v "0.3.0") (d (list (d (n "cge") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ymx3hqgwmhvvlmq5jwi80pqha7c3pslxsy1mgh7365yzidp0sv3") (y #t)))

