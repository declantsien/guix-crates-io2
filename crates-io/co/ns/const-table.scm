(define-module (crates-io co ns const-table) #:use-module (crates-io))

(define-public crate-const-table-0.1.0 (c (n "const-table") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ydfm8al8zzmwfcd9fz9px2wx5wrpyhl6jha93dwhndv0xa0rc01")))

