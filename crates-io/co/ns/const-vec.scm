(define-module (crates-io co ns const-vec) #:use-module (crates-io))

(define-public crate-const-vec-0.1.0 (c (n "const-vec") (v "0.1.0") (h "0w0yi43bjf1qj252sfn44nv735abx0zqc3mksdh7sv04sk96grnd")))

(define-public crate-const-vec-1.0.0 (c (n "const-vec") (v "1.0.0") (h "03rwdcvmy26jmc68yyg9z5m0bkf5ggi7bagd6iyk1vgyp71gh31j")))

(define-public crate-const-vec-1.0.1 (c (n "const-vec") (v "1.0.1") (h "13x9w96n9snlpwgn14ncsafndr11hyy5rcw8q9wfplxamdqp8czq")))

(define-public crate-const-vec-1.1.0 (c (n "const-vec") (v "1.1.0") (h "13g17mf2y34ppwjdlz0wpbzhvc2rdv06pmy8zwfjckh0dcr1nfgx")))

(define-public crate-const-vec-1.1.1 (c (n "const-vec") (v "1.1.1") (h "1r130m74py5g5bkb1jalpj0bvlsy50pg062w3dxmv96slf7955ff")))

