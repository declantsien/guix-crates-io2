(define-module (crates-io co ns consclr) #:use-module (crates-io))

(define-public crate-consclr-0.1.0 (c (n "consclr") (v "0.1.0") (d (list (d (n "cbfr") (r "^0.1.1") (d #t) (k 0)))) (h "0ghg82f33y3hr7q2hkvq3bfv08ksbdd5974mw3p49x0q2s100qn6")))

(define-public crate-consclr-0.1.1 (c (n "consclr") (v "0.1.1") (d (list (d (n "cbfr") (r "^0.1.2") (d #t) (k 0)))) (h "1h1rk6l8fslabc3g9wk3xkp80sgbw9wydxccr4f7hzysl291skc3")))

(define-public crate-consclr-0.2.1 (c (n "consclr") (v "0.2.1") (d (list (d (n "cbfr") (r "^0.1.2") (d #t) (k 0)))) (h "1srw4npxw4lr3gv2l08rs17q8kcg65wacf1k5nzq38yhvix1ld35")))

(define-public crate-consclr-0.2.2 (c (n "consclr") (v "0.2.2") (d (list (d (n "cbfr") (r "^0.1.5") (d #t) (k 0)))) (h "0564xbv1wh8x0mg8hvccz5a0rxmfq7sr78wikvp7c1lsfhl02awn")))

