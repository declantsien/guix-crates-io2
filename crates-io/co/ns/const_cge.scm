(define-module (crates-io co ns const_cge) #:use-module (crates-io))

(define-public crate-const_cge-0.0.0 (c (n "const_cge") (v "0.0.0") (h "10zdbgpgy0w8vav5050n9s3rhgjhxc6b55nyzqhi6a1k578ls6in")))

(define-public crate-const_cge-0.1.0 (c (n "const_cge") (v "0.1.0") (d (list (d (n "cge") (r "^0.0.1") (d #t) (k 2)) (d (n "const_cge_activations") (r "^0.1.0") (d #t) (k 0)) (d (n "const_cge_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1bimdfwsvv9qpqg444q6kp585mhbi5q73kl7jxdn86zjw38j34cx") (y #t)))

(define-public crate-const_cge-0.1.1 (c (n "const_cge") (v "0.1.1") (d (list (d (n "cge") (r "^0.0.1") (d #t) (k 2)) (d (n "const_cge_activations") (r "^0.1.0") (d #t) (k 0)) (d (n "const_cge_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0kssczmb5j5ggn412xlb3q2mhpf0vnjq5i89cdf28hx30p20gmgs") (y #t)))

(define-public crate-const_cge-0.1.2 (c (n "const_cge") (v "0.1.2") (d (list (d (n "cge") (r "^0.0.1") (d #t) (k 2)) (d (n "const_cge_activations") (r "^0.1.0") (d #t) (k 0)) (d (n "const_cge_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1zz48rsqqxi8hmy8pyqx5266z79cl7r05nq9xjj1d77f756mi6bl") (y #t)))

(define-public crate-const_cge-0.2.0 (c (n "const_cge") (v "0.2.0") (d (list (d (n "cge") (r "^0.0.1") (d #t) (k 2)) (d (n "const_cge_macro") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "micromath") (r "^2") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "09naj4yw9slhz49y9zvgflrrzs6b9y3cd7051pn6cjvkcwfqmqgj") (f (quote (("std") ("default" "libm")))) (s 2) (e (quote (("micromath" "dep:micromath" "dep:libm") ("libm" "dep:libm"))))))

(define-public crate-const_cge-0.2.1 (c (n "const_cge") (v "0.2.1") (d (list (d (n "cge") (r "^0.0.1") (d #t) (k 2)) (d (n "const_cge_macro") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "micromath") (r "^2") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "09zssgkjylv165ha3wj2d2x9f0fb4kfqn9dv48cwqfvrpcvk3yji") (f (quote (("std") ("default" "libm")))) (s 2) (e (quote (("micromath" "dep:micromath" "dep:libm") ("libm" "dep:libm"))))))

