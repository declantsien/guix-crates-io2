(define-module (crates-io co ns constduck-procmacro) #:use-module (crates-io))

(define-public crate-constduck-procmacro-0.1.0 (c (n "constduck-procmacro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("derive" "printing"))) (d #t) (k 0)))) (h "0cf4pdiha7rsgggrydjfpdzq1cc4fgzpji77lrmjw3my03m9hszz")))

