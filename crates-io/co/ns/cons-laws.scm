(define-module (crates-io co ns cons-laws) #:use-module (crates-io))

(define-public crate-cons-laws-0.1.0 (c (n "cons-laws") (v "0.1.0") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0clpnvlvc8mah4z5igkkvrqjvgi8ip36bgjcjzpxfj936vakg9yv")))

(define-public crate-cons-laws-0.1.1 (c (n "cons-laws") (v "0.1.1") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "06ar46p6f1c3n2j5s3xl7wpvlnyavhx3nai8x1b0s2jwc1m1446s")))

(define-public crate-cons-laws-0.1.2 (c (n "cons-laws") (v "0.1.2") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "1k6ahb9l3l3kr8g4yg8w010kl36qphy8c6zhcpfhzim1a1spm9kj")))

(define-public crate-cons-laws-0.1.3-alpha.1 (c (n "cons-laws") (v "0.1.3-alpha.1") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0z2cacbai2d9pyxfbcr6s38qni92b7jcirlpdsi22bdcj41mmj1x") (y #t)))

(define-public crate-cons-laws-0.1.3-alpha.2 (c (n "cons-laws") (v "0.1.3-alpha.2") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "07awwkf2j34swn3df1c7z2d8yvnxfsknyswbvywqk7k6bhizc59j") (y #t)))

(define-public crate-cons-laws-0.1.3-alpha.3 (c (n "cons-laws") (v "0.1.3-alpha.3") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "127q5nc57flcp7b1zxx8nxlzg4gx0aima5nwjcpyrwx5lrqxpkp1") (y #t)))

(define-public crate-cons-laws-0.1.3-alpha.4 (c (n "cons-laws") (v "0.1.3-alpha.4") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "1i206nmyx7wm96ll0j5mji14j7mhxg6ssw3w5bfd1d43m62l9bwm") (y #t)))

(define-public crate-cons-laws-0.1.3-alpha.5 (c (n "cons-laws") (v "0.1.3-alpha.5") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0yqyf72gzqpmib3sb82l6ixm7gmlf22mh9gjjxrjxrn6wv1pi7kf") (y #t)))

(define-public crate-cons-laws-0.1.3 (c (n "cons-laws") (v "0.1.3") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "1ldbs3y8cy8anam6rlvsn9jsnr70a3bypf5f4f07jfiqlzkv21ry")))

(define-public crate-cons-laws-0.1.4-alpha.1 (c (n "cons-laws") (v "0.1.4-alpha.1") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "1v4g5z1nn0klwi3y3600c97ygq3qvrn3i99b138jfy88wk4dy963") (f (quote (("ode_solver" "nalgebra" "ode_solvers")))) (y #t)))

(define-public crate-cons-laws-0.1.4-alpha.2 (c (n "cons-laws") (v "0.1.4-alpha.2") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "06qv9jbx53jwcgrz0700gadd7fmyq8ww3ypg2xfmjsaqq2hfmrap") (f (quote (("ode_solver" "nalgebra" "ode_solvers")))) (y #t)))

(define-public crate-cons-laws-0.1.4-alpha.3 (c (n "cons-laws") (v "0.1.4-alpha.3") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "19qzii8l2kw816fza3imzh0l231p60yfay3difv82nc8y346lmy7") (f (quote (("ode_solver" "nalgebra" "ode_solvers")))) (y #t)))

(define-public crate-cons-laws-0.1.4-alpha.4 (c (n "cons-laws") (v "0.1.4-alpha.4") (d (list (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "06fymgp8a03ibsnmp2chymlyrvcj0z9y490183ivf7cy0lx4yiap") (f (quote (("ode_solver" "nalgebra" "ode_solvers")))) (y #t)))

(define-public crate-cons-laws-0.1.4 (c (n "cons-laws") (v "0.1.4") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0l0k6qc8vfs6rp6c7g27m59fzrpx6wsvmn249j9wj01f0jvrshpx") (f (quote (("ode_solver" "nalgebra" "ode_solvers"))))))

(define-public crate-cons-laws-0.1.5 (c (n "cons-laws") (v "0.1.5") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "mathru") (r "^0.9.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "ode_solvers") (r "^0.3.3") (d #t) (k 2)) (d (n "plotters") (r "^0.3.1") (d #t) (k 2)))) (h "0wyl0xsawmni16k4d496qyddcp51m882n8na67bq0myijkap3dkd") (f (quote (("ode_solver" "nalgebra" "ode_solvers"))))))

