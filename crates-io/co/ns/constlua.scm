(define-module (crates-io co ns constlua) #:use-module (crates-io))

(define-public crate-constlua-0.1.0 (c (n "constlua") (v "0.1.0") (d (list (d (n "mlua") (r "^0.7") (f (quote ("lua54" "vendored"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ifnvy0f8jz31lfxjnm2xmfpdgcdmpfjvx8k85af6zpjhrm6y2v7")))

