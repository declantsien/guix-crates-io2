(define-module (crates-io co ns console_tester) #:use-module (crates-io))

(define-public crate-console_tester-0.1.0 (c (n "console_tester") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "terminfo") (r "^0.7.0") (d #t) (k 0)))) (h "0wfrbd3hcjjw1samdlx2c77lywz1p5qz9k3k2gcdf8knpbk12yk8")))

(define-public crate-console_tester-0.1.1 (c (n "console_tester") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "terminfo") (r "^0.7.0") (d #t) (k 0)))) (h "0fvil8jbnpywssykx6mh1h0q2h74zg2nppn7wsp7jar6l6irsbnl")))

(define-public crate-console_tester-0.1.2 (c (n "console_tester") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "terminfo") (r "^0.7.0") (d #t) (k 0)))) (h "1wy7pzay7xdp65hvlwayr1ax2zxpv58b2dzvgf9x2vziqqbsh6xb")))

(define-public crate-console_tester-0.1.3 (c (n "console_tester") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "terminfo") (r "^0.7.0") (d #t) (k 0)))) (h "1w9r0sf9fvsqpavljw7mcy75lqnsqdfvrh7263avnnsvak4hjlxi")))

