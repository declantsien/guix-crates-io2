(define-module (crates-io co ns constant-cstr) #:use-module (crates-io))

(define-public crate-constant-cstr-0.1.0 (c (n "constant-cstr") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "13hp4b030k3vjr3l7jpxc7fsg829y8770iadv9yvyygisygfg3ss")))

(define-public crate-constant-cstr-0.1.1 (c (n "constant-cstr") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "153531zp4l9h2wmbjragvgpdn25zj12q47an90fafk31yzw158a0")))

