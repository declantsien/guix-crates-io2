(define-module (crates-io co ns console_error_panic_hook) #:use-module (crates-io))

(define-public crate-console_error_panic_hook-0.1.0 (c (n "console_error_panic_hook") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.10") (d #t) (k 0)))) (h "0z1kiagaf1aaki46g8j6c1msgpvrd59ayblhkdpbbx63g254rq0v")))

(define-public crate-console_error_panic_hook-0.1.1 (c (n "console_error_panic_hook") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.10") (d #t) (k 0)))) (h "1vk4zcgxl4ax76v118gz827nzynnyfzkxznj3biabfpi71g8r13d")))

(define-public crate-console_error_panic_hook-0.1.2 (c (n "console_error_panic_hook") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.10") (d #t) (k 0)))) (h "0hbgmhb75s1zcrd6h0mr0w6wwp6v01cwqyqz1zwqra0i868llq1r")))

(define-public crate-console_error_panic_hook-0.1.3 (c (n "console_error_panic_hook") (v "0.1.3") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.12") (d #t) (k 0)))) (h "0svjs47rwrcfy6ljgrijjzarywmj0vs5sry8l0f8yhjwfwink21m")))

(define-public crate-console_error_panic_hook-0.1.4 (c (n "console_error_panic_hook") (v "0.1.4") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.16") (d #t) (k 0)))) (h "1pdbajdpfjicdx1rl5v3qhzc9i3czmrvpz0gdbylhbxd247s361p")))

(define-public crate-console_error_panic_hook-0.1.5 (c (n "console_error_panic_hook") (v "0.1.5") (d (list (d (n "cfg-if") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.20") (d #t) (k 0)))) (h "0bknjm972nwrnlnzzbrv2pipa0i7my01gwyad85cckj7jk0d4pbc")))

(define-public crate-console_error_panic_hook-0.1.6 (c (n "console_error_panic_hook") (v "0.1.6") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.37") (d #t) (k 0)))) (h "04d2narcrzk9bnddz17rr2l819l82pr0h6d98s2w9q236n87dndq")))

(define-public crate-console_error_panic_hook-0.1.7 (c (n "console_error_panic_hook") (v "0.1.7") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.37") (d #t) (k 0)))) (h "1g5v8s0ndycc10mdn6igy914k645pgpcl8vjpz6nvxkhyirynsm0")))

