(define-module (crates-io co ns const-sort) #:use-module (crates-io))

(define-public crate-const-sort-0.1.0 (c (n "const-sort") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "subtle") (r "^1.0.0") (d #t) (k 0)))) (h "0d2y1w89cvf937pdl40yk4ld3c75yz62l8xl0pbm397rn5b91apx")))

(define-public crate-const-sort-0.1.1 (c (n "const-sort") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "subtle") (r "^1.0.0") (d #t) (k 0)))) (h "0q48q1f4qjyaswjvizqca9lx0xqhb5jlr6v0a4hinpzgq3n58zlf")))

