(define-module (crates-io co ns console-web) #:use-module (crates-io))

(define-public crate-console-web-0.1.0 (c (n "console-web") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "03ax09109xc2az4llpzjjyvsf7alr8h4fx2rvzksm4vcamjv1if8")))

(define-public crate-console-web-0.1.1 (c (n "console-web") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "0dn3zipsr1ya1qasrk2h6xwy8vi1k59db53svh7l98y05gd5q37h")))

(define-public crate-console-web-0.1.2 (c (n "console-web") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "10yxjqvxdwsp8np92x3gd2pmhv50paaymlnclngwp6i3rvxy3v4x")))

