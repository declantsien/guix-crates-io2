(define-module (crates-io co ns constaes) #:use-module (crates-io))

(define-public crate-constaes-0.1.0 (c (n "constaes") (v "0.1.0") (h "0ja67l76lk4iggndri8y8j2zdmzxprx288ajmk3ri9awjinhbw4h")))

(define-public crate-constaes-0.1.1 (c (n "constaes") (v "0.1.1") (h "1z9dxhhz20nxcf85l6551cxw3132ya74dvmqjq64q6dx4p9bz1rw")))

