(define-module (crates-io co ns console_engine) #:use-module (crates-io))

(define-public crate-console_engine-0.1.1 (c (n "console_engine") (v "0.1.1") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0fdn20x7ddrn4v8yh7hji1xw8xyi1ixzi61rig05jh21l7xbra5f")))

(define-public crate-console_engine-0.1.2 (c (n "console_engine") (v "0.1.2") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0ji3da7g2nvr82ja2cq3043r2fdwnkx2w28z4ssrc1wq24fv942v")))

(define-public crate-console_engine-0.1.3 (c (n "console_engine") (v "0.1.3") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1rq4an2q5hskdyv4i70r1wfj2sphf9q95zv18vx5z74apm132vnm")))

(define-public crate-console_engine-0.1.4 (c (n "console_engine") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0ah9i745qbcdm4kw95nqz26aq5vnj0xsnbkibgjqqh5bd91q0206")))

(define-public crate-console_engine-0.1.5 (c (n "console_engine") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "01ajx69qa50azdg9qlnpa6q3qssa5vr942yalh5ykfjh8kncdciz")))

(define-public crate-console_engine-0.2.0 (c (n "console_engine") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1iwsvizp0gx2c5ciia5diq6bd7jfzfimzq5nk23lrp7y4ha39vhx")))

(define-public crate-console_engine-0.2.1 (c (n "console_engine") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1ra4iwgvlkqfg3s0yij9gzj8379h8a3pmi2m5418ncg2xnrd4clq")))

(define-public crate-console_engine-0.3.0 (c (n "console_engine") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1n09x9wpk8i9915j6ivkg9897322j643k1scija3mnlfzfk89mp2")))

(define-public crate-console_engine-0.4.0 (c (n "console_engine") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0d1p4fy4anh08jmznvy75xwjqygn1j1vpc02m513prkkk2ppmqky")))

(define-public crate-console_engine-0.4.1 (c (n "console_engine") (v "0.4.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1yavkns3f83fgbafcm6p7bq74aw8hbwnwh5ivxn5z202q9rq3znz")))

(define-public crate-console_engine-0.5.0 (c (n "console_engine") (v "0.5.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1hs8jrj325f9wxn58g4xcl6rxrqd29vn0vmacwpy80pgxsr3llld")))

(define-public crate-console_engine-0.6.0 (c (n "console_engine") (v "0.6.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1h4nl9y4h53bzi101iwv22ypydsw94ccjya58mrwnw8n9kl5g003")))

(define-public crate-console_engine-0.6.1 (c (n "console_engine") (v "0.6.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "194drdbgb1s07kj5fjrr7xskn9pkr2zhf8dx5jbmw0m3whivq15p")))

(define-public crate-console_engine-0.6.2 (c (n "console_engine") (v "0.6.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "14236i6qvl36kl7cza3v0pk3sp98sq9nh5hagvrkxpmxlwpfgxqy")))

(define-public crate-console_engine-0.7.0 (c (n "console_engine") (v "0.7.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "01wgsr60cvnpsddx94xb8s3h0id0px8lb6zqnzrika1lq5yxdncy")))

(define-public crate-console_engine-1.0.0 (c (n "console_engine") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1n83wf3cwm0c3igq6cl8523cf96344wd47qrvclaar8y4lv77car")))

(define-public crate-console_engine-1.0.1 (c (n "console_engine") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0jhfmgg1x3bshrwpsiflqiid8ks3mlr8ja9x75zcy4x3j0a9zkq8")))

(define-public crate-console_engine-1.0.2 (c (n "console_engine") (v "1.0.2") (d (list (d (n "crossterm") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "16cyvlv007kc5whbzvyv9l977cyra1vgc6rmpx61jqp8bmqa6gsg")))

(define-public crate-console_engine-1.0.3 (c (n "console_engine") (v "1.0.3") (d (list (d (n "crossterm") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1v7dmf32614svvf55pmi9xfrb3bmakwxgn41zpxnlfdgzi8f2bv4")))

(define-public crate-console_engine-1.0.4 (c (n "console_engine") (v "1.0.4") (d (list (d (n "crossterm") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0pnc8vwflbyh19sjg8yz18s1xnfh1c5br77gdz1p9cj3gsq05b22")))

(define-public crate-console_engine-1.0.5 (c (n "console_engine") (v "1.0.5") (d (list (d (n "crossterm") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0qz5izwm43zflr32rj1yh8d1w93lxkn2fq3fq9nr9jwnxac9mm2d")))

(define-public crate-console_engine-1.1.0 (c (n "console_engine") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0aacajimzxnvsi23bbi8imad18rsybfbq4ddjkrql1rajsyhgds3")))

(define-public crate-console_engine-1.2.0 (c (n "console_engine") (v "1.2.0") (d (list (d (n "crossterm") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0s6gdhg7z2nhvc37qc0dnhbikibwaix3pavgs1s7m3iyh1crqv7b")))

(define-public crate-console_engine-1.3.0 (c (n "console_engine") (v "1.3.0") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "12gwnzj3zv3wymhamqnbdyw2ahk5f8b5v48qi9wlw4f80drv404s")))

(define-public crate-console_engine-1.4.0 (c (n "console_engine") (v "1.4.0") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1k42svvybsnlhsb0iwnqj68i0jnl2hl85hgh9fvwf89yrdiz79nf")))

(define-public crate-console_engine-1.4.1 (c (n "console_engine") (v "1.4.1") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0jyxd76bjf6a9bxswf09yswpa7w2xcjy2gw1i8k1hpr882bf1p6a")))

(define-public crate-console_engine-1.4.2 (c (n "console_engine") (v "1.4.2") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "034jdv3fzydvci2dznsgk11ipnbz5fvlyixqgnbmgaicqvnzvbvp")))

(define-public crate-console_engine-1.5.0 (c (n "console_engine") (v "1.5.0") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0w64wsfv68ic2lqp4n0ydr690qwgd6bma85fmmanc0f9s3mpn1cy")))

(define-public crate-console_engine-1.5.1 (c (n "console_engine") (v "1.5.1") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0l8abqyi5p5ykxm32i8g09z4b1asj3w46zm761i47ymyl7lwxi9j")))

(define-public crate-console_engine-2.0.1 (c (n "console_engine") (v "2.0.1") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0w8zr8zn912ywp6308yws7mgaf6pcwhv8s3fv7b6kvpk79igj1i8")))

(define-public crate-console_engine-2.0.2 (c (n "console_engine") (v "2.0.2") (d (list (d (n "crossterm") (r "^0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "060vh9dq2fa43d71zsxqf9mgb6v1vd37kb3a7bdn5fc6aqi59q6r")))

(define-public crate-console_engine-2.1.0 (c (n "console_engine") (v "2.1.0") (d (list (d (n "crossterm") (r "^0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1ygsfb6mxqq19lrjxnk9xi0n70f0phn226zavmv86n5sp4w84nlh") (f (quote (("event") ("default"))))))

(define-public crate-console_engine-2.2.0 (c (n "console_engine") (v "2.2.0") (d (list (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "12qxja0igcbx7mnmwd0fjdapqldqfw0kj78fpc3dhqlb04008dy7") (f (quote (("event") ("default"))))))

(define-public crate-console_engine-2.3.0 (c (n "console_engine") (v "2.3.0") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1xjdv595nxci55ddpp653kb9xvqvgkcljylwpvppm3jyy1z4qmw6") (f (quote (("event") ("default"))))))

(define-public crate-console_engine-2.4.0 (c (n "console_engine") (v "2.4.0") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "04sfnp3bkw3si6h0n2dgb8ifkdv1rrywykr985750bvwdgx3bi0c") (f (quote (("form" "event") ("event") ("default"))))))

(define-public crate-console_engine-2.5.0 (c (n "console_engine") (v "2.5.0") (d (list (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1ddp8q3h0w4gkvjlwimqwpl58gzgvzrghd1lvw4z2q76kn1nla6g") (f (quote (("form" "event") ("event") ("default"))))))

(define-public crate-console_engine-2.5.1 (c (n "console_engine") (v "2.5.1") (d (list (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1sqb8q3yfm5jsk3hpxd7fpkpamixf2ls2xsr35ycfxi8184i1g4l") (f (quote (("form" "event") ("event") ("default"))))))

(define-public crate-console_engine-2.6.0 (c (n "console_engine") (v "2.6.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "090yh72s141jxsa2p46pscwfbk35dhd1y5jffr8q5aykbxhj6mxh") (f (quote (("form" "event") ("event") ("default"))))))

(define-public crate-console_engine-2.6.1 (c (n "console_engine") (v "2.6.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0gzcdwy4i8as0ln5k975hx0y0dfc5bjg9xk4dp6sjx3z4lnh7kql") (f (quote (("form" "event") ("event") ("default"))))))

