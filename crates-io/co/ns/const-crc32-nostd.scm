(define-module (crates-io co ns const-crc32-nostd) #:use-module (crates-io))

(define-public crate-const-crc32-nostd-1.3.1 (c (n "const-crc32-nostd") (v "1.3.1") (d (list (d (n "crc32fast") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0gc21wzkz0sf5iamaab0d9vlbsjvmkmak2np4gfi2nz9f0qw92l0")))

