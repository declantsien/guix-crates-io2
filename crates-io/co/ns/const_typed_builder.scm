(define-module (crates-io co ns const_typed_builder) #:use-module (crates-io))

(define-public crate-const_typed_builder-0.1.0 (c (n "const_typed_builder") (v "0.1.0") (d (list (d (n "const_typed_builder_derive") (r "=0.1.0") (d #t) (k 0)))) (h "1cgm2qphmdrml0549pp5977vhh4v53zds0b871ag8805s9bij51k")))

(define-public crate-const_typed_builder-0.1.1 (c (n "const_typed_builder") (v "0.1.1") (d (list (d (n "const_typed_builder_derive") (r "=0.1.1") (d #t) (k 0)))) (h "16pxicviskm1ssyg2wj321qyjwz0ccg3b28c0v37ayys4v5dgrk2")))

(define-public crate-const_typed_builder-0.2.0 (c (n "const_typed_builder") (v "0.2.0") (d (list (d (n "const_typed_builder_derive") (r "=0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.84") (d #t) (k 2)))) (h "02ankyz07gl3v1whf9cq3dha71laf2agy0j0c463v065s2zlqnfa")))

(define-public crate-const_typed_builder-0.3.0 (c (n "const_typed_builder") (v "0.3.0") (d (list (d (n "const_typed_builder_derive") (r "=0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.84") (d #t) (k 2)))) (h "1bgssj85lp8097wy31fa4kggn56a29dxjycpvxvjbawwvsi3y5ng")))

