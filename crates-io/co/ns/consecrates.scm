(define-module (crates-io co ns consecrates) #:use-module (crates-io))

(define-public crate-consecrates-0.1.0 (c (n "consecrates") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "http_req") (r "^0.7.2") (f (quote ("rust-tls"))) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "1x3cqihkr358fi7x85jbzkj1i8wmfh1gx8z3f7nlxxyknzxwx3x1")))

(define-public crate-consecrates-0.1.1 (c (n "consecrates") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "http_req") (r "^0.7.2") (f (quote ("rust-tls"))) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "0d1vhql2k76h8463sijx7j93n88bxic6igkv1dzxiv6hg0pzk5rr")))

