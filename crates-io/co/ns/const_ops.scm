(define-module (crates-io co ns const_ops) #:use-module (crates-io))

(define-public crate-const_ops-0.1.0 (c (n "const_ops") (v "0.1.0") (h "058kg4sxx08va9vw7ivmbd892wrcjbpbf1yc082cdzpbq7sywinm")))

(define-public crate-const_ops-0.2.0 (c (n "const_ops") (v "0.2.0") (h "168ddmdp7bnc2amb5hn3qqjj1ly1mhp1f5fsh50vw62l60rn5as0")))

(define-public crate-const_ops-0.2.1 (c (n "const_ops") (v "0.2.1") (h "0ahad7va3z3pkddx87wjdlxq5vzgdxb951nf4kzsdlvccp3p3hhw")))

(define-public crate-const_ops-0.2.2 (c (n "const_ops") (v "0.2.2") (h "1ckyw2drzgjd6zbzfcn0zqad04as7q2vcbk419wrqy367f70yr3m")))

