(define-module (crates-io co ns console-codes) #:use-module (crates-io))

(define-public crate-console-codes-0.1.0 (c (n "console-codes") (v "0.1.0") (h "1njn21gaaq6n3j5q77gc0irwgqsjfgh271vkm1x4m458cnsgpj9k")))

(define-public crate-console-codes-0.1.1 (c (n "console-codes") (v "0.1.1") (h "0vxmfd2j0862jyxjic84npmgmpvny15nkgpapn76y0sk7xc5f5v9")))

(define-public crate-console-codes-0.1.2 (c (n "console-codes") (v "0.1.2") (h "17bzbs8m1mxlb4wnwl3y40vai8v4mkknn7llfxf0wy2m7vj679fj")))

