(define-module (crates-io co ns const-alg) #:use-module (crates-io))

(define-public crate-const-alg-0.1.0 (c (n "const-alg") (v "0.1.0") (d (list (d (n "array-vec") (r "^0.1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0z6lc0r8iykxjwr7sh5iaw8ls7qy6xhdgavl98b86pz0q61awk5y")))

(define-public crate-const-alg-0.1.1 (c (n "const-alg") (v "0.1.1") (d (list (d (n "array-vec") (r "^0.1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "12imk568879yzjrnvkn4gyf5gzdmwhhqi5k7pm9nbfcz54dm3zkc")))

