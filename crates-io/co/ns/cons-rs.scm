(define-module (crates-io co ns cons-rs) #:use-module (crates-io))

(define-public crate-cons-rs-0.1.0 (c (n "cons-rs") (v "0.1.0") (h "0qmgxn0shbkavwqnd1fm3ljy70lnk3cyk12b8hml1piwr9iinq5q")))

(define-public crate-cons-rs-0.2.0 (c (n "cons-rs") (v "0.2.0") (h "1im5n0vja3llccmsz5apzhf8m9blg6snn9m44hmai5hcac4iwnrd")))

(define-public crate-cons-rs-0.2.1 (c (n "cons-rs") (v "0.2.1") (h "0q2d5pd6mad9xqs515lynq2xrdgr91nk9phdihxy2dl4vp6nzxa6")))

(define-public crate-cons-rs-0.2.2 (c (n "cons-rs") (v "0.2.2") (h "1yzyki9qwq48wghq0hg6rmnlv5nfjsrqwkvy272sl6srn92izrq7")))

(define-public crate-cons-rs-0.2.3 (c (n "cons-rs") (v "0.2.3") (h "037ckg5wdzl367dbgccf61fvjd4vw0n0h2cpllravz21a6rdh12n")))

(define-public crate-cons-rs-0.2.4 (c (n "cons-rs") (v "0.2.4") (h "18w5iw8sm2dsyv5igji0c8bpq4plnrjj8qmma1rh6s0b5y4ic7fl")))

(define-public crate-cons-rs-0.2.5 (c (n "cons-rs") (v "0.2.5") (h "1pd141dv6g3d3qpwmf5zdksvlcxfv9jj79y26bk94lrjlm1va1ql")))

(define-public crate-cons-rs-0.2.6 (c (n "cons-rs") (v "0.2.6") (h "1p22p0w2dfb80pn8zx9mpi3kiy76zanmnmjzlkkmv25f80lhgng2") (y #t)))

(define-public crate-cons-rs-0.3.0 (c (n "cons-rs") (v "0.3.0") (h "181psxrv4r639sxb65xj3r6yvdcp6jazdjmbabldqsl0p36iqcbf")))

(define-public crate-cons-rs-0.3.1 (c (n "cons-rs") (v "0.3.1") (h "1xgaclwvg3dhzq14dy8m84b4hfnk88yxjx6i4kbdhajimjnsrpf4")))

(define-public crate-cons-rs-0.3.2 (c (n "cons-rs") (v "0.3.2") (h "0mglw3gpq7hwfpdc26fm6zlzjhqhbgqaf51c2jy0d26wrbgcnayp")))

(define-public crate-cons-rs-0.4.0 (c (n "cons-rs") (v "0.4.0") (h "1spwjhb2irg95j2rcy7q4pwh2c9kyhim87sf7y22l4j7vl9p7lxp")))

(define-public crate-cons-rs-0.4.1 (c (n "cons-rs") (v "0.4.1") (h "1d13bi1zn7k0hjfkq6pv4iqs5gkybpmi2mzkwk758zsrjhj4r6v2")))

(define-public crate-cons-rs-0.5.0 (c (n "cons-rs") (v "0.5.0") (h "142bs53ms5amx2x5fm182bfia2wwkcr5bf211slbphxapzvvh6nd")))

(define-public crate-cons-rs-0.6.0 (c (n "cons-rs") (v "0.6.0") (h "0rzrrqxbxy9h6bj8xpaxxqxzpjha42w7r8sqgn715ig8w3gc0s9b")))

(define-public crate-cons-rs-0.6.1 (c (n "cons-rs") (v "0.6.1") (h "13gmwl241k1babqf90wg64pw0rf236qfll0n7d9g8956p3mcwx10")))

(define-public crate-cons-rs-0.6.2 (c (n "cons-rs") (v "0.6.2") (h "157zrx7h4ii97hfdmvzsbm84l92pipgk5wi04n04ln024y59b9bf")))

(define-public crate-cons-rs-0.6.3 (c (n "cons-rs") (v "0.6.3") (h "0r2f59scv694v2ky0ww9jq247xkz1xvpa4i6dvj4841nmwnb5vsp")))

(define-public crate-cons-rs-0.6.4 (c (n "cons-rs") (v "0.6.4") (h "1w3wl8z4bjx7rql3xwcqpyalq5c3z5qrqwsd634j75b5nw6pv37z")))

(define-public crate-cons-rs-0.6.5 (c (n "cons-rs") (v "0.6.5") (h "0wisbqk5l1zsw2rf7ihpqmv817fp67gv78c55gwq710zqrp7v06m")))

(define-public crate-cons-rs-0.6.6 (c (n "cons-rs") (v "0.6.6") (h "1piw3djn160ppjir702rmvq299gliablb40dkdf05wf96n2idyvp")))

(define-public crate-cons-rs-0.6.7 (c (n "cons-rs") (v "0.6.7") (h "0gzggynafw26bjhkfh9bq3smxz7k5s011y8zzn8gnrm5mx09lw8v")))

(define-public crate-cons-rs-0.6.8 (c (n "cons-rs") (v "0.6.8") (h "14ddbz5nj47zlg38783lilkvxw4j6ikj69852r9rwflh7pgxz4mf")))

(define-public crate-cons-rs-0.7.0 (c (n "cons-rs") (v "0.7.0") (h "1bm9bklqs6jk9ppbxxxvfkb302b7kiipdafc1vcqx1varahnrl4s") (f (quote (("immutable"))))))

(define-public crate-cons-rs-0.7.1 (c (n "cons-rs") (v "0.7.1") (h "14awp3r5mx2kpz86n3fnzcs0szay94hpcbk17x48waa5h96gsci6") (f (quote (("immutable"))))))

(define-public crate-cons-rs-0.7.2 (c (n "cons-rs") (v "0.7.2") (h "1lbxgpxdfmphqkddikn6nk0v5hz49fwmldk8sy0gi3jgq4z7836s") (f (quote (("immutable"))))))

(define-public crate-cons-rs-0.7.3 (c (n "cons-rs") (v "0.7.3") (h "1cnjbma784raldmhyqqgk4ada031vgclpc42dzrwzm4s6393bg5m") (f (quote (("immutable"))))))

