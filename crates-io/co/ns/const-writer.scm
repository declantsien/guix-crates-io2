(define-module (crates-io co ns const-writer) #:use-module (crates-io))

(define-public crate-const-writer-0.1.0 (c (n "const-writer") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 2)))) (h "1jmn32wm13m14mil0dbqf4721hrjcgm8ax5i9jl30p65mkpdx8b2") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-const-writer-0.1.1 (c (n "const-writer") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 2)))) (h "1661apxa4zl5z1x8ykw5iq1g9n7f2mh2x4mjg83dx59rplrqzfm6") (f (quote (("std") ("default" "std") ("alloc"))))))

