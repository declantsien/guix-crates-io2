(define-module (crates-io co ns const_panic) #:use-module (crates-io))

(define-public crate-const_panic-0.0.0 (c (n "const_panic") (v "0.0.0") (h "04vlxq82biiwkrxz2i4gawd62c9vwlw398dgzcz97152b21nnfpy")))

(define-public crate-const_panic-0.1.0 (c (n "const_panic") (v "0.1.0") (h "1qaacddf3psnwapkjsrhqw6np03fj475shm7bbn1xdmgvp27r9wd") (f (quote (("test") ("non_basic") ("docsrs") ("default" "non_basic")))) (r "1.57")))

(define-public crate-const_panic-0.1.1 (c (n "const_panic") (v "0.1.1") (h "0yipbwhbync65kfa72gqfp8w2v6h3sm1ddz0b1j2hvz5h78qv5ia") (f (quote (("test") ("non_basic") ("docsrs") ("default" "non_basic")))) (r "1.57")))

(define-public crate-const_panic-0.2.0 (c (n "const_panic") (v "0.2.0") (d (list (d (n "const_panic_proc_macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (k 2)))) (h "191igpn13z7dqprfx1rfg3vdqp47bi45qpsmv996njz48diasks7") (f (quote (("test") ("non_basic") ("docsrs") ("derive" "const_panic_proc_macros") ("default" "non_basic")))) (r "1.57")))

(define-public crate-const_panic-0.2.1 (c (n "const_panic") (v "0.2.1") (d (list (d (n "const_panic_proc_macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (k 2)))) (h "1h9gsal5mccxgmqam0s34qzq1kjizv42wksr3gwawnq56j06iidg") (f (quote (("test") ("non_basic") ("docsrs") ("derive" "const_panic_proc_macros") ("default" "non_basic")))) (r "1.57")))

(define-public crate-const_panic-0.2.2 (c (n "const_panic") (v "0.2.2") (d (list (d (n "const_panic_proc_macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (k 2)))) (h "02jz9pjvgn17gxrl8k995qnqh19qdf3n1hrr8c9fnzn7w35dq7k7") (f (quote (("test") ("non_basic") ("docsrs") ("derive" "const_panic_proc_macros") ("default" "non_basic")))) (r "1.57")))

(define-public crate-const_panic-0.2.3 (c (n "const_panic") (v "0.2.3") (d (list (d (n "const_panic_proc_macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (k 2)))) (h "0lxdj76p1k5kzdsgxdgh89rxr3zdz162yv95v12y0grkk778f4xq") (f (quote (("test") ("non_basic") ("docsrs") ("derive" "const_panic_proc_macros") ("default" "non_basic")))) (r "1.57")))

(define-public crate-const_panic-0.2.4 (c (n "const_panic") (v "0.2.4") (d (list (d (n "const_panic_proc_macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (k 2)))) (h "17zscycyhkrlyi66681np4lc54lqxkv12a1bkg347r4h3vj5h0ww") (f (quote (("test") ("non_basic") ("docsrs") ("derive" "const_panic_proc_macros") ("default" "non_basic")))) (r "1.57")))

(define-public crate-const_panic-0.2.5 (c (n "const_panic") (v "0.2.5") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "const_panic_proc_macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (k 2)))) (h "185sksa1j812qf336lrhvr83qrwyxx57w2x865mlmf52g3nsxzkm") (f (quote (("test") ("rust_1_64") ("non_basic") ("docsrs") ("derive" "const_panic_proc_macros") ("default" "non_basic")))) (r "1.57")))

(define-public crate-const_panic-0.2.6 (c (n "const_panic") (v "0.2.6") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "const_panic_proc_macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (k 2)))) (h "1pb9p880fcib70yqlad9p52x8sz4lgd02i42pdkgpr7f4f1v5lly") (f (quote (("test") ("rust_1_64") ("non_basic") ("docsrs") ("derive" "const_panic_proc_macros") ("default" "non_basic")))) (r "1.57")))

(define-public crate-const_panic-0.2.7 (c (n "const_panic") (v "0.2.7") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "const_panic_proc_macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "konst_kernel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (k 2)))) (h "1gzzpbvp7bjfh8hrks2gs6yc6h7cr0sxv79a2arikjl53dbaxfjq") (f (quote (("test") ("rust_1_64") ("non_basic" "konst_kernel") ("docsrs") ("derive" "const_panic_proc_macros" "non_basic") ("default" "non_basic")))) (r "1.57")))

(define-public crate-const_panic-0.2.8 (c (n "const_panic") (v "0.2.8") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "const_panic_proc_macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (k 2)) (d (n "typewit") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "16w72mnzjqgwfhlq8cqm6xhd2n6lc1wan08987izv1pcxhwz4lb0") (f (quote (("test") ("rust_1_64") ("non_basic" "typewit") ("docsrs") ("derive" "const_panic_proc_macros" "non_basic") ("default" "non_basic")))) (r "1.57")))

