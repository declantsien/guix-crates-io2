(define-module (crates-io co ns const-identify) #:use-module (crates-io))

(define-public crate-const-identify-0.1.0 (c (n "const-identify") (v "0.1.0") (d (list (d (n "const-fnv1a-hash") (r "^1.1") (d #t) (k 0)) (d (n "const-identify-derive") (r "^0.1") (d #t) (k 0)))) (h "0v80c8la2cyvj8fyxr4b90k1097b4p7w25dwbf07vhzx18pqr4zk")))

(define-public crate-const-identify-0.1.1 (c (n "const-identify") (v "0.1.1") (d (list (d (n "const-fnv1a-hash") (r "^1.1") (d #t) (k 0)) (d (n "const-identify-derive") (r "^0.1") (d #t) (k 0)))) (h "1mbjimyxqdsb4hra0gjz75ynvy962wcz05yi5x813giska7p8bw7")))

