(define-module (crates-io co ns const-oid) #:use-module (crates-io))

(define-public crate-const-oid-0.1.0 (c (n "const-oid") (v "0.1.0") (h "02rd4l3cgym9b2j433478pjgsnyahv3g4sld4236x949f8midnd2")))

(define-public crate-const-oid-0.2.0 (c (n "const-oid") (v "0.2.0") (h "0zy1grnws52d4ffr43h39f0svibmcwarl15r1krk4hhbmdvkzhvg")))

(define-public crate-const-oid-0.3.0 (c (n "const-oid") (v "0.3.0") (h "1dmvwm6m6gqswhkzhz6cjss76yxdn8f6m6bqgfxh2czb5xsr5wbs") (f (quote (("std")))) (y #t)))

(define-public crate-const-oid-0.3.1 (c (n "const-oid") (v "0.3.1") (h "0qmdcp741lf9a8bfgixl2s8x82pvyc15dimlxaj1pzxkcs62wgwb") (f (quote (("std"))))))

(define-public crate-const-oid-0.3.2 (c (n "const-oid") (v "0.3.2") (h "1m44gs2g4agnxj7bxw59whlcpyhvlscs3mhb2965l98p9hhk1yml") (f (quote (("std"))))))

(define-public crate-const-oid-0.3.3 (c (n "const-oid") (v "0.3.3") (h "13qrlkw7q28p8fa906aqxyxqy4shf0rb5f2wna8sbfqfa5cfn64p") (f (quote (("std"))))))

(define-public crate-const-oid-0.3.4 (c (n "const-oid") (v "0.3.4") (h "1w23m8y8hmxgiip8hamg2qb907d1vap68frims86jmgwrf71xww1") (f (quote (("std"))))))

(define-public crate-const-oid-0.3.5 (c (n "const-oid") (v "0.3.5") (h "1vgghdnikn0lh82dd1gabqpr29h4k7nv06s3p9mv8l2yxxbkh54f") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-const-oid-0.4.0 (c (n "const-oid") (v "0.4.0") (h "1rx10d5c4fx75wd305a97kcnwvbhigrm84bvaqcd0rq2n3d4d6q6") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-const-oid-0.4.1 (c (n "const-oid") (v "0.4.1") (h "1sg3fjihmfhx7bw5f4pcz7h9nkbsg7f5w2lh0fvgnw89nyb2gn65") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-const-oid-0.4.2 (c (n "const-oid") (v "0.4.2") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "09ws8j7k6bk7hn30whwpsarq1a79xqjfg27k4gvc1v9vmdgdvs57") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-const-oid-0.4.3 (c (n "const-oid") (v "0.4.3") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "10xg8lqni03r4059f12fzhhd9xjgx6isiyzasp7vg0y7p9avq9zv") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-const-oid-0.4.4 (c (n "const-oid") (v "0.4.4") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0s39fqbh0lhv6vml8aa8yvpsr2aihl528mw4h74a711nax0nzgkq") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-const-oid-0.4.5 (c (n "const-oid") (v "0.4.5") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0pqd9jyc6dxm4c4x41v604b7wsrcv1mkm3kj6a9y9irjd7dn8swz") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-const-oid-0.5.0 (c (n "const-oid") (v "0.5.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0vbnx5d37hkchbs0fx35yxhn6xb8bj1agkjy4g7b8gm9fbwsmf3m") (f (quote (("std"))))))

(define-public crate-const-oid-0.5.1 (c (n "const-oid") (v "0.5.1") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0badlbgs82wjj6ihawf6qhqv3q63h55xiqg1l0bc9wy3jflpnjdg") (f (quote (("std"))))))

(define-public crate-const-oid-0.5.2 (c (n "const-oid") (v "0.5.2") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1nmfq96n085klj3ahxg2w76h5khzglinibq4g1fag27pagyci6r7") (f (quote (("std"))))))

(define-public crate-const-oid-0.6.0 (c (n "const-oid") (v "0.6.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "08hga67vqmpd00469ix2v2r9vmcm7c16q0lmss8l46x43q1jzhs4") (f (quote (("std")))) (y #t)))

(define-public crate-const-oid-0.6.1 (c (n "const-oid") (v "0.6.1") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1pv40h7rmiwl3k0hpqh2g9ps9h9hqimscl02yh0621a4cxfl3azx") (f (quote (("std")))) (y #t)))

(define-public crate-const-oid-0.6.2 (c (n "const-oid") (v "0.6.2") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "12vv7csqqjj0x1l5mf51lgqiw76k5c3mb1yzfhfcqysks2j2lvwx") (f (quote (("std"))))))

(define-public crate-const-oid-0.7.0 (c (n "const-oid") (v "0.7.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0q4zpwx237xip13bwp209ggbryxzgka024vfwqhira5g8j7pamfk") (f (quote (("std")))) (y #t) (r "1.56")))

(define-public crate-const-oid-0.7.1 (c (n "const-oid") (v "0.7.1") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1wwl3cncd8p2fa54vzmghflh4nh9ml02xfbv38nf5ziifh28riz4") (f (quote (("std")))) (r "1.56")))

(define-public crate-const-oid-0.8.0 (c (n "const-oid") (v "0.8.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1k3l6k8j8np19ni5sv9a71czwznsv74cwn0jahi68pzbn15pnkaf") (f (quote (("std")))) (r "1.57")))

(define-public crate-const-oid-0.9.0 (c (n "const-oid") (v "0.9.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0q8n1zsa73130hxa2w88qw36g8nprz21j52abpva3khm59a26bkj") (f (quote (("std") ("db")))) (r "1.57")))

(define-public crate-const-oid-0.9.1 (c (n "const-oid") (v "0.9.1") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0fyxvwnl3x6bxhy08a3g4ryf8mky6wnhwd6ll4g6mjxgfnk1ihyf") (f (quote (("std") ("db")))) (r "1.57")))

(define-public crate-const-oid-0.9.2 (c (n "const-oid") (v "0.9.2") (d (list (d (n "arbitrary") (r "^1.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "04xr993r37hd3kcwzll34kpihxrxj7yhr7fawgir8gs80wyby3sj") (f (quote (("std") ("db")))) (r "1.57")))

(define-public crate-const-oid-0.9.3 (c (n "const-oid") (v "0.9.3") (d (list (d (n "arbitrary") (r "^1.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1g66wq9w41j70p3akd4gph5z1nb8bzrm6489d5ln652ljdbxyh33") (f (quote (("std") ("db")))) (r "1.57")))

(define-public crate-const-oid-0.9.4 (c (n "const-oid") (v "0.9.4") (d (list (d (n "arbitrary") (r "^1.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0iv7idvmif4bfzf3q7xrj9mrgp522zj2fqngbw3hyd4fdbkccnvr") (f (quote (("std") ("db")))) (r "1.57")))

(define-public crate-const-oid-0.9.5 (c (n "const-oid") (v "0.9.5") (d (list (d (n "arbitrary") (r "^1.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0vxb4d25mgk8y0phay7j078limx2553716ixsr1x5605k31j5h98") (f (quote (("std") ("db")))) (r "1.57")))

(define-public crate-const-oid-0.10.0-pre.0 (c (n "const-oid") (v "0.10.0-pre.0") (d (list (d (n "arbitrary") (r "^1.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "11nv3j5yf26jymmnvwvh5my8kzkdgw8685pn6mzwwsk8i6x9a5k6") (f (quote (("std") ("db")))) (r "1.60")))

(define-public crate-const-oid-0.9.6 (c (n "const-oid") (v "0.9.6") (d (list (d (n "arbitrary") (r "^1.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1y0jnqaq7p2wvspnx7qj76m7hjcqpz73qzvr9l2p9n2s51vr6if2") (f (quote (("std") ("db")))) (r "1.57")))

(define-public crate-const-oid-0.10.0-pre.1 (c (n "const-oid") (v "0.10.0-pre.1") (d (list (d (n "arbitrary") (r "^1.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "0yhlgrpi9z2nglg7sk0g7m4id6hwpb9y9b222girg4rswfhnn7w4") (f (quote (("std") ("db")))) (r "1.60")))

(define-public crate-const-oid-0.10.0-pre.2 (c (n "const-oid") (v "0.10.0-pre.2") (d (list (d (n "arbitrary") (r "^1.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "1sp34xmmga9yrgz3lwn2nn46lphn2lxz3ra6jnqad2q94wm3bqzp") (f (quote (("std") ("db")))) (r "1.71")))

