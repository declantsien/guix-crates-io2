(define-module (crates-io co ns const_strum) #:use-module (crates-io))

(define-public crate-const_strum-0.1.0 (c (n "const_strum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "05j9wqdwrbvhs593bn0mvk9lm79f4nlnqav8zzq5ji53581xq6x9")))

(define-public crate-const_strum-0.1.1 (c (n "const_strum") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "09bn0psm03mm5c3dcnb6gn3q6biywnrnw3yj9wgkaanbb971izgm")))

