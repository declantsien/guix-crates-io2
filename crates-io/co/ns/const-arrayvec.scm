(define-module (crates-io co ns const-arrayvec) #:use-module (crates-io))

(define-public crate-const-arrayvec-0.1.0 (c (n "const-arrayvec") (v "0.1.0") (h "1596c1qmbn9ii5dyzfnaj6l5p84qq21n7pr5a66agv756rdia0vl")))

(define-public crate-const-arrayvec-0.2.0 (c (n "const-arrayvec") (v "0.2.0") (h "1vfavjbfhdc3mgkbk1hvhp865l9ckapvbq9izv83rd2niv35b6a9")))

(define-public crate-const-arrayvec-0.2.1 (c (n "const-arrayvec") (v "0.2.1") (h "0a82660walrag1hxm78qcqx0cq12s6044rl4h7h2pa3baj4si8px")))

