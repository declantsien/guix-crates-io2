(define-module (crates-io co ns const-cstr) #:use-module (crates-io))

(define-public crate-const-cstr-0.1.0 (c (n "const-cstr") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1pd7pkx25h3kw8991vip286lnvay14amqsmfyyn6d8dkfzcg86h3")))

(define-public crate-const-cstr-0.2.0 (c (n "const-cstr") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "17mi3ak9am1xg4c0qh2jmyz8hlh6ba8h6c8g0pr74l5x3sskff55")))

(define-public crate-const-cstr-0.2.1 (c (n "const-cstr") (v "0.2.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1lj4pk3z79vszcxzrd16yd0r2s0nyqfzlmrjsj1mm73zk8vhp59f")))

(define-public crate-const-cstr-0.3.0 (c (n "const-cstr") (v "0.3.0") (h "19ij6m8s16d0i7vma535l7w4x8bcanjcxs7c6n7sci86ydghnggd")))

