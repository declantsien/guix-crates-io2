(define-module (crates-io co ns consistenttime) #:use-module (crates-io))

(define-public crate-consistenttime-0.0.1 (c (n "consistenttime") (v "0.0.1") (h "15fmw7fna9kwrdc6448al4lxljnxxk4sgmfiza6dkvvqrk493gxv")))

(define-public crate-consistenttime-0.0.2 (c (n "consistenttime") (v "0.0.2") (h "0nsyk7ppwk4mlmmf04xmvcn8qlmfs89ca3s39bfhjman2hrr4rhz")))

(define-public crate-consistenttime-0.1.0 (c (n "consistenttime") (v "0.1.0") (h "1q0w8jy3jcs4ww0cn03d6rgahfynx1iaybhz584vdzij3m0ryx50")))

(define-public crate-consistenttime-0.2.0 (c (n "consistenttime") (v "0.2.0") (h "00pmmhpins0lv946fr7n7amm464jg2iicn6wi0manpiqmbfrn1va")))

