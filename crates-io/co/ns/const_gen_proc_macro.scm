(define-module (crates-io co ns const_gen_proc_macro) #:use-module (crates-io))

(define-public crate-const_gen_proc_macro-0.1.0 (c (n "const_gen_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1m2b3q760kdc3sdvhlvksihglnp1dpid7dcvw8jn1l8yw63yyz56") (f (quote (("proc_macro_expand")))) (y #t)))

(define-public crate-const_gen_proc_macro-0.1.1 (c (n "const_gen_proc_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0add84d9d4qvwbcb6qyshp53s4m8zhykqxvx3qbrzv5amz9340yr") (f (quote (("proc_macro_expand"))))))

