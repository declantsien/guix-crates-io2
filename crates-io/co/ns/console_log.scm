(define-module (crates-io co ns console_log) #:use-module (crates-io))

(define-public crate-console_log-0.1.0 (c (n "console_log") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "1g0z0cc5fmvhwqps0zmsqcfay5zhh4008mg0haiaw9q339q0a4mj")))

(define-public crate-console_log-0.1.1 (c (n "console_log") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "154agy0v2bik0jrsnfwcm4ck8g5r7k7br5c1kvgfmw3ikf5ny2b3")))

(define-public crate-console_log-0.1.2 (c (n "console_log") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "0j1wd2885m3245bhsb2qjvw08lzplbi1rgg2v3yv0hbljk972y0y")))

(define-public crate-console_log-0.2.0 (c (n "console_log") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "150li8pznpfpn4q0f7g9jwq2hnd5wik0w8378zaa1wffc5ckf6jh") (f (quote (("default") ("color" "wasm-bindgen"))))))

(define-public crate-console_log-0.2.1 (c (n "console_log") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "00sws6jj9x47k5xhbh44xcq58rkcj5jxc4p4dsil9488sikx12l7") (f (quote (("default") ("color" "wasm-bindgen"))))))

(define-public crate-console_log-0.2.2 (c (n "console_log") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "17s9awnafrz3cb2k4i9brvi8f2bqzcd5lk80ia5qf0c5bvv757z8") (f (quote (("default") ("color" "wasm-bindgen"))))))

(define-public crate-console_log-1.0.0 (c (n "console_log") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "03rwzvpg384y68j6hxm4h1bhzi7xcc5jdari8hxlvgzdwi0fv2my") (f (quote (("default") ("color" "wasm-bindgen"))))))

