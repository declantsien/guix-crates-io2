(define-module (crates-io co ns const_str_to_pubkey) #:use-module (crates-io))

(define-public crate-const_str_to_pubkey-0.1.0 (c (n "const_str_to_pubkey") (v "0.1.0") (d (list (d (n "solana-program") (r "^1.18.4") (d #t) (k 0)))) (h "0acn8kfp0q99lzv6nz9jsbc3bfwys052y40bh0q9zw0wgw230qxy")))

(define-public crate-const_str_to_pubkey-0.1.1 (c (n "const_str_to_pubkey") (v "0.1.1") (d (list (d (n "solana-program") (r "^1.18.4") (d #t) (k 0)))) (h "047xylir6nxrcn2ibsyw7n1rqgq2vhzm1midkqfk3ysmwwa476i7")))

