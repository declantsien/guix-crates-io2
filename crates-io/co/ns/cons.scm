(define-module (crates-io co ns cons) #:use-module (crates-io))

(define-public crate-cons-0.0.1 (c (n "cons") (v "0.0.1") (h "05hqm3lgcp7kh8zylr24bf0d4cwcz9bdmsvsm7lfqxh2pcib1j6r") (y #t)))

(define-public crate-cons-0.0.3 (c (n "cons") (v "0.0.3") (d (list (d (n "cons-list") (r "^0.0.3") (d #t) (k 0)))) (h "0xfprdgfvfq4br7mpfscig22jdr7ny3byv6fdxspkrgyb0kjnlly") (y #t)))

(define-public crate-cons-0.0.4 (c (n "cons") (v "0.0.4") (h "0xs3j8bad03cjb4p2m3f2dxmimw5rbrki9pj66cgwn71by210pp4")))

(define-public crate-cons-0.1.0 (c (n "cons") (v "0.1.0") (d (list (d (n "ahash") (r "^0.4.5") (o #t) (k 0)) (d (n "elysees") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "flurry") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0bwqqs3wfvkr054rqd0xd8ygll1gnigfa23j0ycz73pd9y0rjjgv") (f (quote (("flurry-cons" "flurry" "ahash"))))))

