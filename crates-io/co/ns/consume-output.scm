(define-module (crates-io co ns consume-output) #:use-module (crates-io))

(define-public crate-consume-output-0.1.0 (c (n "consume-output") (v "0.1.0") (h "006yff1lw2pjc643izjarbv5l54r79rk0061lpr5026z7cagqkqn")))

(define-public crate-consume-output-0.1.1 (c (n "consume-output") (v "0.1.1") (h "0izy89gb4y0kw2cgkfvlicxzivvsi35b86qbwg7l4n0bc6mxb2mx")))

