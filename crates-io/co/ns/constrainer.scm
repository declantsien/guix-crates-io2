(define-module (crates-io co ns constrainer) #:use-module (crates-io))

(define-public crate-constrainer-0.0.1 (c (n "constrainer") (v "0.0.1") (d (list (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dlxhf06xvdbdky446jbziykfj7myq3vblwazij80969mpz97jqk")))

(define-public crate-constrainer-0.0.2 (c (n "constrainer") (v "0.0.2") (d (list (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dmhdq5nvz64mm11163j1sfh0pm7zpk6pxzv4wi6c62qk8pzfmcf")))

