(define-module (crates-io co ns consistent_hash) #:use-module (crates-io))

(define-public crate-consistent_hash-0.1.0 (c (n "consistent_hash") (v "0.1.0") (d (list (d (n "clap") (r "^2.20") (d #t) (k 2)) (d (n "siphasher") (r "^0.1") (d #t) (k 0)) (d (n "splay_tree") (r "^0.1") (d #t) (k 0)))) (h "0aymbjg3bjg2cr5ihx175chnirjplqhs6b0fwr04a425pjfhwmqh")))

(define-public crate-consistent_hash-0.1.1 (c (n "consistent_hash") (v "0.1.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 2)) (d (n "siphasher") (r "^0.1") (d #t) (k 0)) (d (n "splay_tree") (r "^0.1") (d #t) (k 0)))) (h "0invip48icy35svwnkiq9565d81f55wl582kyh5pfibq67kflzvm")))

(define-public crate-consistent_hash-0.1.2 (c (n "consistent_hash") (v "0.1.2") (d (list (d (n "clap") (r "^2.20") (d #t) (k 2)) (d (n "siphasher") (r "^0.1") (d #t) (k 0)) (d (n "splay_tree") (r "^0.1") (d #t) (k 0)))) (h "1qkac8bl294s3bjcvx4iwlrl0g4xhmf89aa6lz6dp0v8iq6n6wj5")))

(define-public crate-consistent_hash-0.1.3 (c (n "consistent_hash") (v "0.1.3") (d (list (d (n "clap") (r "^2.20") (d #t) (k 2)) (d (n "siphasher") (r "^0.1") (d #t) (k 0)) (d (n "splay_tree") (r "^0.1") (d #t) (k 0)))) (h "0d5g6p7ywv3f6sapy7ykp2r4n1g34x9llka74f6fj9ngxcxjq0z0")))

(define-public crate-consistent_hash-0.1.4 (c (n "consistent_hash") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "siphasher") (r "^0.1") (d #t) (k 0)) (d (n "splay_tree") (r "^0.2") (d #t) (k 0)))) (h "06yxxb004wxpagca6p58c80c11v5vkmi39i4q2v1z43k6l2y9pgm")))

