(define-module (crates-io co ns const_unit_poc) #:use-module (crates-io))

(define-public crate-const_unit_poc-1.0.0 (c (n "const_unit_poc") (v "1.0.0") (h "1vbzv64yxpkr44yask3ba6p1qggdxpx1wm5r9wjvsd51ddzrkmq0") (f (quote (("non_ascii") ("default"))))))

(define-public crate-const_unit_poc-1.1.0 (c (n "const_unit_poc") (v "1.1.0") (h "1zcgaplqd9qn2bq1py3w9bbdg84qca3bi0n7ffsry2wcx1lrba1k") (f (quote (("non_ascii") ("default"))))))

(define-public crate-const_unit_poc-1.1.1 (c (n "const_unit_poc") (v "1.1.1") (h "0waw0z9az4a4x46fw2ix1f8h4ds130vk6cj1yavz87z4718bh81c") (f (quote (("non_ascii") ("default"))))))

(define-public crate-const_unit_poc-1.1.2 (c (n "const_unit_poc") (v "1.1.2") (h "04w80jylz20qs3rmqafag2b3x95x8fbfsl97yd0jq0sd02ik5xgc") (f (quote (("non_ascii") ("default"))))))

(define-public crate-const_unit_poc-1.1.3 (c (n "const_unit_poc") (v "1.1.3") (h "1628lddms6ks47r4my8dsfz7bg5wfckhrxa94a89q3v30sfi67zl") (f (quote (("non_ascii") ("default"))))))

