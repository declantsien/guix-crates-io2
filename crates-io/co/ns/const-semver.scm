(define-module (crates-io co ns const-semver) #:use-module (crates-io))

(define-public crate-const-semver-0.1.0 (c (n "const-semver") (v "0.1.0") (d (list (d (n "semver") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "13032p9dnywli1d5sgf21j71c75sv9alx9r76l39y3d9lqw5nci8") (f (quote (("macros") ("default" "macros" "semver" "serde")))) (y #t)))

(define-public crate-const-semver-0.1.1 (c (n "const-semver") (v "0.1.1") (d (list (d (n "semver") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0hpmfkyi18x03birf9q7wwm4hxg8ngk91iwckknql30hrlf88wkw") (f (quote (("macros") ("default" "macros" "semver" "serde")))) (y #t)))

(define-public crate-const-semver-0.1.2 (c (n "const-semver") (v "0.1.2") (d (list (d (n "semver") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0pav0nda0070fqil3m9hzdwqhldpz1wgp0nyy0n50gcxr72xplj5") (f (quote (("macros") ("default" "macros" "semver" "serde")))) (y #t)))

(define-public crate-const-semver-0.1.3 (c (n "const-semver") (v "0.1.3") (d (list (d (n "semver") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0nb1rcxi7ms9hjqi96m0ka0nf9in2fy5lr4s2f1fvbia9z3mr995") (f (quote (("macros") ("default" "macros" "semver" "serde")))) (y #t)))

(define-public crate-const-semver-0.1.4 (c (n "const-semver") (v "0.1.4") (d (list (d (n "semver") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1xvqwyxg52aaivb73n01wwziyh1591498a6rs0drbbld7bdlv4n7") (f (quote (("macros") ("default" "macros" "semver" "serde"))))))

