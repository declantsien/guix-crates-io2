(define-module (crates-io co ns constant) #:use-module (crates-io))

(define-public crate-constant-0.0.1 (c (n "constant") (v "0.0.1") (h "0xzcqck6msyfih966vlig0fw416a3w3fz4cqn6a2xwg2n7257f7s") (y #t)))

(define-public crate-constant-0.1.0 (c (n "constant") (v "0.1.0") (d (list (d (n "const_internals") (r "^0.1.0") (d #t) (k 0)))) (h "1qbl1qdlxnv8l6rhvzpwbivv2k5cqdqa8xivfzg2fq02yds4dzd6")))

(define-public crate-constant-0.2.0 (c (n "constant") (v "0.2.0") (d (list (d (n "const_internals") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "const_internals") (r "^0.2.0") (d #t) (k 2)))) (h "013608lvxl0n7c60624sfjjrw8rq56hm8224s81k32y8chsy6sz6") (f (quote (("default" "const_internals"))))))

