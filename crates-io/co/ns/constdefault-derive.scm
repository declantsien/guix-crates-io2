(define-module (crates-io co ns constdefault-derive) #:use-module (crates-io))

(define-public crate-constdefault-derive-0.2.0 (c (n "constdefault-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "0fypld1b34nammzl2m447n0pzmg3h1006ch4xg16c0rdrhldnwk1")))

(define-public crate-constdefault-derive-0.2.1 (c (n "constdefault-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "proc-macro" "printing"))) (k 0)))) (h "0v85hbwmrkhjr8l0hhvz1hvkjz6g6ppr33hp859dzjnvrqrc6hb0")))

