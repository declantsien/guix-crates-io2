(define-module (crates-io co ns const-buffer) #:use-module (crates-io))

(define-public crate-const-buffer-0.1.0 (c (n "const-buffer") (v "0.1.0") (h "1s95axwzr37db2q2ma12948cnzrpxa0ilk6mx3rvmcw51zs68gjb")))

(define-public crate-const-buffer-0.1.1 (c (n "const-buffer") (v "0.1.1") (h "10zl02l0jaqxifxc4ymkdcgfkx1wl52yf50n7cla80hq6wxmvqp3")))

(define-public crate-const-buffer-0.1.2 (c (n "const-buffer") (v "0.1.2") (h "1s05j879f52wkdby4z2bc5jdja1pwpk38j6mh8fh79q1xb5ba5y5")))

