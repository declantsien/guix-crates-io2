(define-module (crates-io co ns const_soft_float) #:use-module (crates-io))

(define-public crate-const_soft_float-0.1.0 (c (n "const_soft_float") (v "0.1.0") (h "1dwhy977asi6i14hmya2j6rvpcfzsy0vf2yz1lixw00a4mmd6asg") (f (quote (("no_std") ("const_trait_impl") ("const_mut_refs"))))))

(define-public crate-const_soft_float-0.1.1 (c (n "const_soft_float") (v "0.1.1") (h "0k276l5qdcivgvgcx9w2h6lim5anx7j5dc4c9rhig0y5wdrk42vh") (f (quote (("no_std") ("const_trait_impl") ("const_mut_refs"))))))

(define-public crate-const_soft_float-0.1.2 (c (n "const_soft_float") (v "0.1.2") (h "116cr6is3rnpynz6h2c1dsr1rihcinkmraal35s1i1qyqwp4llsv") (f (quote (("no_std") ("const_trait_impl") ("const_mut_refs"))))))

(define-public crate-const_soft_float-0.1.3 (c (n "const_soft_float") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)))) (h "1nlhmvc8r1z8z5rxqbcmmk82w0cib4p5k0npz4hdlj4wrwaa60a0") (f (quote (("no_std") ("const_trait_impl") ("const_mut_refs"))))))

(define-public crate-const_soft_float-0.1.4 (c (n "const_soft_float") (v "0.1.4") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)))) (h "1zrxrj3qy75bzkq3f4w7bcpipksi5rhxpcwbwr9x8kpgcjm1rjl7") (f (quote (("no_std") ("const_trait_impl") ("const_mut_refs"))))))

