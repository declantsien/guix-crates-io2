(define-module (crates-io co ns consume_on_drop) #:use-module (crates-io))

(define-public crate-consume_on_drop-0.1.0 (c (n "consume_on_drop") (v "0.1.0") (h "10x0sxax78pbl8m7vgb09a3yb2p8420dim3kgflcn654m714qghh")))

(define-public crate-consume_on_drop-0.1.1 (c (n "consume_on_drop") (v "0.1.1") (h "13mqnwy0icmqjfj8xqfkb15kzc4fibsypf1j43izkff6ma31bna7")))

