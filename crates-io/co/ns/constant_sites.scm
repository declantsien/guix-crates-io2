(define-module (crates-io co ns constant_sites) #:use-module (crates-io))

(define-public crate-constant_sites-0.1.0 (c (n "constant_sites") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.12") (d #t) (k 0)) (d (n "seq_io") (r "^0.3.0") (d #t) (k 0)))) (h "0w4iz7iq03kc41j2f0yklcaarivksihqzjfhyh17dn6k1v8177hk") (y #t)))

