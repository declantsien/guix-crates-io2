(define-module (crates-io co ns const-array-attrs) #:use-module (crates-io))

(define-public crate-const-array-attrs-0.0.1 (c (n "const-array-attrs") (v "0.0.1") (d (list (d (n "disuse") (r "^0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "transition-table") (r "^0.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "01z2a6mw773c9qym7k8x3f75f0hs25gdr94r1qafi1w46vvmc9m1")))

(define-public crate-const-array-attrs-0.0.2 (c (n "const-array-attrs") (v "0.0.2") (d (list (d (n "disuse") (r "^0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "transition-table") (r "^0.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0f5hvbcdhbid9zr94rp9zgpvycn639dal5cs6p158rbw2jq5pi32")))

(define-public crate-const-array-attrs-0.0.3 (c (n "const-array-attrs") (v "0.0.3") (d (list (d (n "disuse") (r "^0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "transition-table") (r "^0.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1q3qxr1592il6krfr4qh16vza75si6c5awjjniwziy8cxbgf1s7c")))

