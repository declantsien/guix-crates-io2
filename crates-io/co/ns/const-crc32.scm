(define-module (crates-io co ns const-crc32) #:use-module (crates-io))

(define-public crate-const-crc32-1.0.0 (c (n "const-crc32") (v "1.0.0") (d (list (d (n "crc32fast") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "06bcnxz8xyjq4n2wf9s2v830mb1yckxk54xwwxfgql68w9rbjc3v")))

(define-public crate-const-crc32-1.0.1 (c (n "const-crc32") (v "1.0.1") (d (list (d (n "crc32fast") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1vijpgw6dllgw6mgdgks6qwsqf65ik6ylflcjv13x1jnnd19lili")))

(define-public crate-const-crc32-1.1.0 (c (n "const-crc32") (v "1.1.0") (d (list (d (n "crc32fast") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1zij9xyclz6lnkb7q43p5qvbc6r74pav4s5q518dg5rb3bynq79i")))

(define-public crate-const-crc32-1.2.0 (c (n "const-crc32") (v "1.2.0") (d (list (d (n "crc32fast") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0vnnq819r2dxbbkc4vimksmffwfp8wsmpg47zwbd044sf8hdmvi3")))

(define-public crate-const-crc32-1.3.0-rc.1 (c (n "const-crc32") (v "1.3.0-rc.1") (d (list (d (n "crc32fast") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1aiipk4ckjfzy780wqsa367ffmqf1s1p8kw2lc9wvjb6177h7d50")))

(define-public crate-const-crc32-1.3.0 (c (n "const-crc32") (v "1.3.0") (d (list (d (n "crc32fast") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "185r2m3fzlbbk63wsn3cabyjq1dc0i3zgxj6pwwv7rbh5ma3zlb8")))

