(define-module (crates-io co ns console-games) #:use-module (crates-io))

(define-public crate-console-games-0.1.0 (c (n "console-games") (v "0.1.0") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)))) (h "0kdll3sx29mr2l9wwd2p03fz1cvh4rr8rm0x7bxd8jzgl4d2wlsl")))

(define-public crate-console-games-0.1.1 (c (n "console-games") (v "0.1.1") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)))) (h "1hgvkhzlbhwwhc7wzpawbxd879ay2h3i1pbj1gdnc12sg171hj6z")))

(define-public crate-console-games-0.1.2 (c (n "console-games") (v "0.1.2") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0smlvx7vhn903pki1m0b18f6xr3yw087x4npps8dvrjpj28x07rk")))

(define-public crate-console-games-0.1.3 (c (n "console-games") (v "0.1.3") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c2ivsi20xqh37iyn5k4r6paja47xli0w01rrg1h96w1p05qwxha")))

(define-public crate-console-games-0.1.4 (c (n "console-games") (v "0.1.4") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0by5x0cdn4inr104sghivn1j34mlkglg9gfkp64mh0qdzvcinsp3")))

(define-public crate-console-games-0.1.5 (c (n "console-games") (v "0.1.5") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pb8q2rw2iq51c8952snr2m3n6pr0aqx0gkm7554ink7gf5pd2dw")))

(define-public crate-console-games-0.1.6 (c (n "console-games") (v "0.1.6") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0q68d11h5vyplwf1cjm0460ckwipp18v98ax8i4rv47zvdls3xxj")))

(define-public crate-console-games-0.1.7 (c (n "console-games") (v "0.1.7") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xjwpgmhffjapap16911hnghdc1g454wfwnbikv14ca86isjjq33")))

(define-public crate-console-games-0.1.8 (c (n "console-games") (v "0.1.8") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vdvqnrm0riykvdr9qqdnggc1nslkmjj3pix76pvhbr8dh5ccabd")))

(define-public crate-console-games-0.1.9 (c (n "console-games") (v "0.1.9") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14awrvm11sjd5vnvwrz3q4i4rxhci0x6s3xaj2hp35x0b7djzibj")))

(define-public crate-console-games-0.1.10 (c (n "console-games") (v "0.1.10") (d (list (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rcd8nxjn62pj70xcf4zgp9vs5f7n7bqs9r9h6gw60ihnnxrikks")))

(define-public crate-console-games-0.1.11 (c (n "console-games") (v "0.1.11") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m2589kcg53lxj0vrl32bdkp3f6ij0z7p0sdjys5jhdjshxq2zv5")))

(define-public crate-console-games-0.1.12 (c (n "console-games") (v "0.1.12") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18a7d6x1q9yfnvavab03vi7xyba8lygjm5n3yjy8zxvyb5i8nzaf")))

(define-public crate-console-games-1.0.0 (c (n "console-games") (v "1.0.0") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b3w7dvvh350dlv8n9m0svfljx10h22ij6060kci2vsrfz8hlrpx")))

(define-public crate-console-games-1.0.1 (c (n "console-games") (v "1.0.1") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1mywkw6zq0w9csbvs75mlmsmyafzw0d0pcsxkgazq5ag0ilhg21x")))

(define-public crate-console-games-1.0.2 (c (n "console-games") (v "1.0.2") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0z2f3bw1dm3nki6m21dbqpkgb9y7nlvabvksh3j4gx8kybplnp4b")))

(define-public crate-console-games-1.1.0 (c (n "console-games") (v "1.1.0") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-number") (r "^0.1.8") (d #t) (k 0)))) (h "08rv20wcgkf05qfy8y2zw4z1j13b4sp6vjizpqx5ffx1kb802xqi")))

(define-public crate-console-games-1.1.1 (c (n "console-games") (v "1.1.1") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-number") (r "^0.1.8") (d #t) (k 0)))) (h "1nxncp0az1wwv7f054icly1wpqd9fywdwzwzc4gdq6icf5s8lbpq")))

(define-public crate-console-games-1.1.2 (c (n "console-games") (v "1.1.2") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-number") (r "^0.1.8") (d #t) (k 0)))) (h "1mw1bvpqsg236lzd6mczg5p6ld5zghbddxgvxmyg1gym4dq2k3cs")))

(define-public crate-console-games-1.1.3 (c (n "console-games") (v "1.1.3") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-number") (r "^0.1.8") (d #t) (k 0)))) (h "17ki9xc4f765pwsi5bcqvdffih7hx45xf1cxkkjjw0qkdpbsrc0n")))

(define-public crate-console-games-1.1.4 (c (n "console-games") (v "1.1.4") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-number") (r "^0.1.8") (d #t) (k 0)))) (h "169dj1zf54dzbr23ir0xl0jiqllmsqzm1rqqlxsphd47xcr2k4pw")))

(define-public crate-console-games-1.1.6 (c (n "console-games") (v "1.1.6") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-number") (r "^0.1.8") (d #t) (k 0)))) (h "1zhmhp1nnisb43wf6r1bzms5pbig0dbswr6kgl3wwcxbg8g4zln6")))

(define-public crate-console-games-1.1.7 (c (n "console-games") (v "1.1.7") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "eff-wordlist") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-number") (r "^0.1.8") (d #t) (k 0)))) (h "0cgzfax3sx2vnwq1d67frmsrb8qyymwba7k1wqdi05b5iybm2dkh")))

