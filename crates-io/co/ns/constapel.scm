(define-module (crates-io co ns constapel) #:use-module (crates-io))

(define-public crate-constapel-0.1.0 (c (n "constapel") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0kr1ysv75m7lbbwppyp45a1ydi2jlbg4yxf72gbzab67w78pzajq")))

(define-public crate-constapel-0.1.1 (c (n "constapel") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "03sr8h3hx7mfadjsish8h2c8ny2i50bf4myxvm0wcx3jzp6lbc2i")))

