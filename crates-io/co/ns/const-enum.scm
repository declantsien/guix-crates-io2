(define-module (crates-io co ns const-enum) #:use-module (crates-io))

(define-public crate-const-enum-0.1.0 (c (n "const-enum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15xk9fz8dz3xc0qklgjxiw13cjh6iq0ifry0qi00fzif0xslqlw2")))

