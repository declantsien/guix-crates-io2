(define-module (crates-io co ns const_internals) #:use-module (crates-io))

(define-public crate-const_internals-0.0.1 (c (n "const_internals") (v "0.0.1") (h "1ap1s5j47rnj8rgh29xs1zygv3sb5b8i34plwcszr091p8ih324g")))

(define-public crate-const_internals-0.1.0 (c (n "const_internals") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "14wmymch5in5z7adaq8jkr76lxxmk53wri53y56wlpafjvmyv9fa")))

(define-public crate-const_internals-0.2.0 (c (n "const_internals") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (d #t) (k 0)))) (h "1yix6g8g2r27qx7p1hxqz6riyh0bh3kmk8gj8n7w2r708fn55sjb")))

