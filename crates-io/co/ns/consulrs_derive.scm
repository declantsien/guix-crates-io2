(define-module (crates-io co ns consulrs_derive) #:use-module (crates-io))

(define-public crate-consulrs_derive-0.1.0 (c (n "consulrs_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.5") (d #t) (k 0)))) (h "0qjiqpa8vs201y0vl22vkqgknf8nj14w73kx78ibiyz62g6aydni")))

