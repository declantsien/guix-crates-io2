(define-module (crates-io co ns const-anonymous-functions) #:use-module (crates-io))

(define-public crate-const-anonymous-functions-1.0.0 (c (n "const-anonymous-functions") (v "1.0.0") (h "1fl9ax0bng8gj8hjq154pph6a7lwqsvbysn78761gv8pl5nkkywz")))

(define-public crate-const-anonymous-functions-1.1.0 (c (n "const-anonymous-functions") (v "1.1.0") (h "11s3ps2mj8f4lybgxd7biqw1rm9h456hp5rgghkvavj1pvc739ps")))

