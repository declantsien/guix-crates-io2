(define-module (crates-io co ns consul) #:use-module (crates-io))

(define-public crate-consul-0.0.1 (c (n "consul") (v "0.0.1") (d (list (d (n "curl") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0fnvpws5cn582z1kv0kkb3viy6n0fn5yag8w2wxc90i9h7qxiakf")))

(define-public crate-consul-0.0.2 (c (n "consul") (v "0.0.2") (d (list (d (n "curl") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0al0iznvp4ni5n3msk9q1qjcvqgw3nng9qh3wrkf3r3h88fr9vkc")))

(define-public crate-consul-0.0.3 (c (n "consul") (v "0.0.3") (d (list (d (n "curl") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "05mi1c8i2bqg8waffyq5a93siygvbach38vjq8q1j2ndzrl91gbz")))

(define-public crate-consul-0.0.4 (c (n "consul") (v "0.0.4") (d (list (d (n "curl") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "16b04ksqi7psnrjx68njwamc2npnfb2fhxjdxbcigw8nbql4qs07")))

(define-public crate-consul-0.0.5 (c (n "consul") (v "0.0.5") (d (list (d (n "curl") (r "^0.2.16") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0mpc4y1ay88wadhv8jrzza7hy3hx8xq38cd9z599jfprj3mj9q5f")))

(define-public crate-consul-0.0.6 (c (n "consul") (v "0.0.6") (d (list (d (n "curl") (r "^0.2.19") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0625cnw2rjzrzvh4g5d9vcwk56p6k2lmr923nls2g8s082n12ij1")))

(define-public crate-consul-0.1.0 (c (n "consul") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16y2pp56v8anvha9z4aamn7zyzca96pvs8v8jr3c0i6d985njh9a")))

(define-public crate-consul-0.2.0 (c (n "consul") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-openssl") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "150phnlijr7w60vh1gh7as2sl6jxqn0vphgbvx1w5v3162036g0m")))

(define-public crate-consul-0.3.0 (c (n "consul") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1410va9v60y9rw3zx5f6rdaywnm70y2g2wmsvicnj8wiff79z6kx")))

(define-public crate-consul-0.4.1 (c (n "consul") (v "0.4.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1rk6i4qfizkfj0sxgjx11bza4d83k96ghlknbshpf63i1cf29g73")))

(define-public crate-consul-0.4.2 (c (n "consul") (v "0.4.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rstest") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0nldsmq57d74rjrkg075vlhdl6qnhfrk73hhk8s3895ip5sfzn1m")))

