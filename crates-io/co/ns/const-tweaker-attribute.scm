(define-module (crates-io co ns const-tweaker-attribute) #:use-module (crates-io))

(define-public crate-const-tweaker-attribute-0.1.0 (c (n "const-tweaker-attribute") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0shgqi40yi97kmw57pw7bqqbv3pdyg1n42i3glhxnvg0gcszy8iw")))

(define-public crate-const-tweaker-attribute-0.1.1 (c (n "const-tweaker-attribute") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1klqxmig12rxpw4bza3575jrx0d7w0bmqp6fjp0gwck65q0agi46")))

(define-public crate-const-tweaker-attribute-0.2.0 (c (n "const-tweaker-attribute") (v "0.2.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "19kdkynyinyp1jzlm8rraik2iz22yr2iqfh95jd200fs9kvn3lf4")))

(define-public crate-const-tweaker-attribute-0.3.0 (c (n "const-tweaker-attribute") (v "0.3.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1z41w01r973hbmn7r0pi9pv17g8jwigi8qq37yly2hcpslrz5kjm")))

(define-public crate-const-tweaker-attribute-0.4.0 (c (n "const-tweaker-attribute") (v "0.4.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1lf43pgd2j2b46akgxggyzw2nh1jrygwkzrrn76mk57cx4ka7fg4")))

(define-public crate-const-tweaker-attribute-0.4.1 (c (n "const-tweaker-attribute") (v "0.4.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1fpi3h7qmhr8qpdfdi93l0s630l4zfadwx62r14vjfxxzs7x4hqs")))

(define-public crate-const-tweaker-attribute-0.5.0 (c (n "const-tweaker-attribute") (v "0.5.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1n60l09hb0h2hiz2csvvbn3az0qzmaqfyfhbih60q47ny0w9ww1d")))

