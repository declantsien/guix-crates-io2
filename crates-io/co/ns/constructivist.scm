(define-module (crates-io co ns constructivist) #:use-module (crates-io))

(define-public crate-constructivist-0.0.1 (c (n "constructivist") (v "0.0.1") (d (list (d (n "constructivism_macro_gen") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0afw32v7hgw70k2hb8bybl10yqlhv47d7b61pvmbhkxkyalv5b9p")))

(define-public crate-constructivist-0.3.0 (c (n "constructivist") (v "0.3.0") (d (list (d (n "constructivism_macro_gen") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1rawkb99p3ja3rmsqkil79ig7blgn5b3zwaz6pay6dhsnra4f8wb")))

