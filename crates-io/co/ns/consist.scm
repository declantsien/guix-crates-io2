(define-module (crates-io co ns consist) #:use-module (crates-io))

(define-public crate-consist-0.1.0 (c (n "consist") (v "0.1.0") (h "0v8d1iqs5rbr76xxyqiwsggalqp9i7g67bnsvxmlzqpwzy3j4vyw")))

(define-public crate-consist-0.1.1 (c (n "consist") (v "0.1.1") (h "0ycfnaijcj9i7mwp9ddppsxz5rsr8ky07y52z8zqnl41vqpjzz02")))

(define-public crate-consist-0.2.0 (c (n "consist") (v "0.2.0") (h "0iabl2pcjmbjr2fh17sa3xcylkjgm6cw6lva30gf0y9qhlxwfrgh")))

(define-public crate-consist-0.3.0 (c (n "consist") (v "0.3.0") (d (list (d (n "crc") (r "^1.3.0") (d #t) (k 0)))) (h "0dizf8hwnkhizpp7ww44p14ax3rawn3dazc1303a7f3pl204s8c4")))

(define-public crate-consist-0.3.1 (c (n "consist") (v "0.3.1") (d (list (d (n "crc") (r "^1.3.0") (d #t) (k 0)))) (h "15kzpd28p405pvfwdmk3z1msrvpqkpw3n8vdmngyqg5pkvx61sqk") (y #t)))

(define-public crate-consist-0.3.2 (c (n "consist") (v "0.3.2") (d (list (d (n "crc") (r "^1.3.0") (d #t) (k 0)))) (h "1av63lz1vs3af5qaapzv7j2a78pdr805jzw4jglssp0ifzlmmavm")))

