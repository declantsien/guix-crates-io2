(define-module (crates-io co ns constdb) #:use-module (crates-io))

(define-public crate-constdb-0.1.0 (c (n "constdb") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "0xdshk6h2xlhg6szxxj4g2kd0x7xqfvl3dkf3p1nas0l57m540d1")))

(define-public crate-constdb-0.2.0 (c (n "constdb") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "069bkkazmgcnmhdc7s989c3brq66g82q9mfff32gnxh04x92iajp")))

(define-public crate-constdb-0.2.1 (c (n "constdb") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "0sywmfyr46hvcsyisyaapm1s5adf49w3gd3m2zzfijflpmga22ik")))

(define-public crate-constdb-0.2.2 (c (n "constdb") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "19avpicdsf9bnis286a55bsag067agcvskqa7p0iy6qz5pvxgsyj")))

(define-public crate-constdb-0.4.0 (c (n "constdb") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "1hwnqm0cnscmqjk5lkzy7bgnymnhjvvgqg0bh2d2n748dv2zi6kp") (y #t)))

(define-public crate-constdb-0.4.1 (c (n "constdb") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "155kzcszrkdxj9rcib7rlpm8vpswks4r2j75wk29kdx2ksx6g4ff")))

(define-public crate-constdb-0.2.3 (c (n "constdb") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.0") (d #t) (k 2)))) (h "0wrjf3qakasglsqgvkyjrc50xqpfgxap67hywzhdzgfgxdjd0mnb")))

