(define-module (crates-io co ns constructivism_macro) #:use-module (crates-io))

(define-public crate-constructivism_macro-0.0.1 (c (n "constructivism_macro") (v "0.0.1") (d (list (d (n "constructivist") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1qss48dyl8zjrk0skdgf03mmd6cwjxr6817110bf4cz3awhjl7c5")))

(define-public crate-constructivism_macro-0.3.0 (c (n "constructivism_macro") (v "0.3.0") (d (list (d (n "constructivist") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0lax1iy9nwxxid4vpf8zx5z5wzx7hvmhwczqcb1xl9lcy6mfgmas")))

