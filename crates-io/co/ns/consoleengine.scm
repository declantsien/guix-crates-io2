(define-module (crates-io co ns consoleengine) #:use-module (crates-io))

(define-public crate-ConsoleEngine-0.0.1 (c (n "ConsoleEngine") (v "0.0.1") (h "139qm6dv15jfv9i7c239r1n8g0y7f784iqh9yflz5dlljqvgvwpv")))

(define-public crate-ConsoleEngine-0.0.2 (c (n "ConsoleEngine") (v "0.0.2") (h "0zymnh353kpsf1c8sycvx55n6rd751272i1qyxbf95qfndvb5905")))

(define-public crate-ConsoleEngine-0.0.3 (c (n "ConsoleEngine") (v "0.0.3") (h "08xp7rd7zw6sk89cvx5ipsyp865ddqiq47m80npv1diy9smm66jg")))

(define-public crate-ConsoleEngine-0.0.4 (c (n "ConsoleEngine") (v "0.0.4") (h "1lxx8cwwjvyl2r3yd2s4rdjxi9xniq2xwl4jp52rpnlvd27m5i1z")))

(define-public crate-ConsoleEngine-0.0.5 (c (n "ConsoleEngine") (v "0.0.5") (h "0hiyxm5hx8cwpwxpl6j0y6iqjnfacfb3adaph4g4fzv81shf8y8d")))

(define-public crate-ConsoleEngine-0.0.6 (c (n "ConsoleEngine") (v "0.0.6") (h "0gn522ysxdg1qx7ldl75x4kmm1c7nwig43wn3r6xy7m6fvfp30g5")))

(define-public crate-ConsoleEngine-0.0.7 (c (n "ConsoleEngine") (v "0.0.7") (h "1xz1xzhlya0d5677kwmjdrij91vq2px1sgikikvyqkwrvii30hcg")))

