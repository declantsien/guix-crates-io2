(define-module (crates-io co ns const-enum-tools) #:use-module (crates-io))

(define-public crate-const-enum-tools-0.1.0 (c (n "const-enum-tools") (v "0.1.0") (h "0qvkihwbnmdvci3kcqffnsz7ll8xgn2v2ks6zmrkf6c4j2ccjixf")))

(define-public crate-const-enum-tools-0.2.0 (c (n "const-enum-tools") (v "0.2.0") (h "1lcj7mpnl7vxialhi26c100zdxb406hinmal25y7sazxmsf4dd4n")))

