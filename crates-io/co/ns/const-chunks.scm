(define-module (crates-io co ns const-chunks) #:use-module (crates-io))

(define-public crate-const-chunks-0.1.0 (c (n "const-chunks") (v "0.1.0") (h "1xzpq1qwzmfqm5w247j1jynhin8lx8zybdn7vv949d03813kapvh") (r "1.65.0")))

(define-public crate-const-chunks-0.1.1 (c (n "const-chunks") (v "0.1.1") (h "1xyxrc7m8j2sm3w7b81074gzkj0g5cx2kqfirh475lqxjfhyrhl8") (r "1.65.0")))

(define-public crate-const-chunks-0.2.0 (c (n "const-chunks") (v "0.2.0") (h "1cd9hfkpwmqvlr3688blbrdmiz0nb5lp6c23wsh3cxsznjag37kz") (r "1.65.0")))

(define-public crate-const-chunks-0.2.1 (c (n "const-chunks") (v "0.2.1") (h "1npwmszkfisvx1ayyyl23rg1rxkzirrm8b3jqcvca3iba4h1m064") (r "1.65.0")))

(define-public crate-const-chunks-0.2.2 (c (n "const-chunks") (v "0.2.2") (h "15xl88h4d1fb2bkjbbf83f7z081a2mkg07napn37q249sva025s4") (r "1.65.0")))

(define-public crate-const-chunks-0.3.0 (c (n "const-chunks") (v "0.3.0") (h "0vf7s3fhw5ykylylpswh20hq8c5m6bfvpqp930pp53pwgs2kvmlj") (r "1.65.0")))

