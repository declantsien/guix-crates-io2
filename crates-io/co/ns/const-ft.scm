(define-module (crates-io co ns const-ft) #:use-module (crates-io))

(define-public crate-const-ft-0.1.0 (c (n "const-ft") (v "0.1.0") (h "1sypzkian73j7qfn4ynzwxaky3j6c581r6xrgbmrv7bjcznnnz5d") (f (quote (("default") ("const_fn"))))))

(define-public crate-const-ft-0.1.1 (c (n "const-ft") (v "0.1.1") (h "0v65qnvqfzmadg8ahb3bd1jflcjyg5qkkyss448000jhn8a82m8k") (f (quote (("default") ("const_fn"))))))

(define-public crate-const-ft-0.1.2 (c (n "const-ft") (v "0.1.2") (h "1zar5s0ccbjginx2mp8xba2lp6cm21gb068fwpswczchrvmml3wr") (f (quote (("default") ("const_fn"))))))

