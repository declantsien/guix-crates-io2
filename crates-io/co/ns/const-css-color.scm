(define-module (crates-io co ns const-css-color) #:use-module (crates-io))

(define-public crate-const-css-color-0.1.0 (c (n "const-css-color") (v "0.1.0") (d (list (d (n "konst") (r "^0.2.13") (d #t) (k 0)))) (h "18aqbvbbgdw9xipb69vmz8qfssv7dqks7m1fbs50k430nz59w2vr") (f (quote (("default")))) (y #t)))

(define-public crate-const-css-color-0.1.1 (c (n "const-css-color") (v "0.1.1") (h "129yr69vzcfxpxwmcwjpzyg9fq0y378dqmr315814s51rgskdy7r")))

