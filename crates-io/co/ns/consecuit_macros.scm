(define-module (crates-io co ns consecuit_macros) #:use-module (crates-io))

(define-public crate-consecuit_macros-0.1.0 (c (n "consecuit_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8.0") (d #t) (k 0)))) (h "0jhgy5yifrywq019zj7x6mx7b12s88cf4dxgp4y8arj348mxvx4x")))

