(define-module (crates-io co ns const_sort_rs) #:use-module (crates-io))

(define-public crate-const_sort_rs-0.1.0 (c (n "const_sort_rs") (v "0.1.0") (d (list (d (n "const_closure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "15fnm82yn728rq1ig4g89wv1xcj6dmdy8qw95iljy7ga52zf6mka")))

(define-public crate-const_sort_rs-0.2.0 (c (n "const_sort_rs") (v "0.2.0") (d (list (d (n "const_closure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "01pr4r54d3ax1q8pah4fzkx892bcs0pjp63jm44zgk2ddbsnn42z")))

(define-public crate-const_sort_rs-0.2.1 (c (n "const_sort_rs") (v "0.2.1") (d (list (d (n "const_closure") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "17m7c1kxgx83xkf5bkysn3pz0iz16wy38nda4zflwnszpzn2c60h")))

(define-public crate-const_sort_rs-0.3.0 (c (n "const_sort_rs") (v "0.3.0") (d (list (d (n "const_closure") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18inqvk04dd3jfbrp3i1lzysqpf2m77hlmpcpq88xas13h1i41j1")))

(define-public crate-const_sort_rs-0.3.1 (c (n "const_sort_rs") (v "0.3.1") (d (list (d (n "const_closure") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1wi9qv06284cna5v6z08q67nfwr1vgs01z123b98ldhwz9q40xkp")))

(define-public crate-const_sort_rs-0.3.2 (c (n "const_sort_rs") (v "0.3.2") (d (list (d (n "const_closure") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ga11l9gaw28y92jfdn4l62d2rf47161092xdyb7hhplvj4gyqv9")))

(define-public crate-const_sort_rs-0.3.3 (c (n "const_sort_rs") (v "0.3.3") (d (list (d (n "const_closure") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1f8wmsqr5nj9zn8al5sqyh4458ig17hy7yfmzbfb6nvg34gf0r9x")))

