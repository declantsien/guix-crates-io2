(define-module (crates-io co ns const_env) #:use-module (crates-io))

(define-public crate-const_env-0.1.0 (c (n "const_env") (v "0.1.0") (d (list (d (n "const_env_impl") (r "^0.1") (d #t) (k 0)))) (h "0r18651a2l0kxkfc2lwa561a3sp3qfqa64ryrwc0pdgvcw3rhhai")))

(define-public crate-const_env-0.1.1 (c (n "const_env") (v "0.1.1") (d (list (d (n "const_env_impl") (r "= 0.1.1") (d #t) (k 0)))) (h "0975grimfn4kiib599is192q80r50pp9ksajg6s32yy181wlw3fb")))

(define-public crate-const_env-0.1.2 (c (n "const_env") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.3") (f (quote ("stable"))) (d #t) (k 2)) (d (n "const_env_impl") (r "= 0.1.2") (d #t) (k 0)))) (h "0zk2z6d6w846z6xnk23jzs0rgy6qzydbsaipvak8qfg3qrr4z7iy")))

