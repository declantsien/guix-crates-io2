(define-module (crates-io co ns const-if) #:use-module (crates-io))

(define-public crate-const-if-0.1.0 (c (n "const-if") (v "0.1.0") (h "0kd1q1piy2x1knamyn5hw3v0f17lxp2klziqnpq2j1z9xcndly5y")))

(define-public crate-const-if-0.1.1 (c (n "const-if") (v "0.1.1") (h "0cg5mjgfn2g40h8rqb5fdr1pwyym13wmjdw1sw9c5apy8scc0348")))

(define-public crate-const-if-0.1.2 (c (n "const-if") (v "0.1.2") (h "1icx46f3dqbagjwszn16p67paj0nazjrgii88h2jm2km92wp5vy9")))

(define-public crate-const-if-0.1.3 (c (n "const-if") (v "0.1.3") (h "07giavp52gfpx56896w7dy1dpgx80m336i6xwnn0d5yc4r2pn6ci")))

