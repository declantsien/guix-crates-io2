(define-module (crates-io co ns const-layout) #:use-module (crates-io))

(define-public crate-const-layout-0.1.0 (c (n "const-layout") (v "0.1.0") (h "0q5m5w9xg0bafn63s975wnkhkc3iga02rglsm015j906aa5mms7v") (y #t)))

(define-public crate-const-layout-0.1.1 (c (n "const-layout") (v "0.1.1") (h "1rwyw6kkx070zs73hmhy5b8994r8i1yw8absz8f3afc1pa92xqxl") (y #t)))

