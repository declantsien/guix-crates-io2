(define-module (crates-io co ns constptr) #:use-module (crates-io))

(define-public crate-constptr-0.1.0 (c (n "constptr") (v "0.1.0") (h "00z968i2zdpiivx8yglysvf0ykkckif2wwjm5gbz1mzv9r29g8s3")))

(define-public crate-constptr-0.2.0 (c (n "constptr") (v "0.2.0") (h "0zhnnng0s0np4jgmz1lsk6gi3jlxpzi3qbq3xbmlyzfmqdp82swz") (f (quote (("std") ("default" "std"))))))

