(define-module (crates-io co ns conshash) #:use-module (crates-io))

(define-public crate-conshash-0.1.0 (c (n "conshash") (v "0.1.0") (h "0dfrx97bpdnb3hs6m208q7wysnji9qrn8asjc3487cb3p9zfvdfd")))

(define-public crate-conshash-0.1.1 (c (n "conshash") (v "0.1.1") (h "18502wdpx3jvh5x2b1syyrp4953yxnx93sppgw8apyg6p0q4784x")))

(define-public crate-conshash-0.1.2 (c (n "conshash") (v "0.1.2") (h "0x36b0k2rr5b7fmhq2yyfaasgqd9im9rdpz6qsayi2i26pb2h27f")))

(define-public crate-conshash-0.1.3 (c (n "conshash") (v "0.1.3") (h "1648xhyghkh71d14gwipah4p1jv1v3nn5m9aw16shji999pnmi1g")))

(define-public crate-conshash-0.1.4 (c (n "conshash") (v "0.1.4") (h "1xdq1s874iql036pd3fdj6ybmy66kix5q1av2r8r5q1c4y6icyxr")))

