(define-module (crates-io co ns const-enum-tools-derive) #:use-module (crates-io))

(define-public crate-const-enum-tools-derive-0.1.0 (c (n "const-enum-tools-derive") (v "0.1.0") (d (list (d (n "const-enum-tools") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gi5l70wmlk1cfycqwj46ndb9p54lan67jb27jkzfzl53lm1481d")))

(define-public crate-const-enum-tools-derive-0.2.0 (c (n "const-enum-tools-derive") (v "0.2.0") (d (list (d (n "const-enum-tools") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f70v94vn9wr59cp88lvfnhp66h3srn5mfvqyirmmm870mrb76dm")))

(define-public crate-const-enum-tools-derive-0.2.1 (c (n "const-enum-tools-derive") (v "0.2.1") (d (list (d (n "const-enum-tools") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "056av9md6d861r606ksj4frfx7q3sfjh7lpw3mn0qng9jjmxk7dv")))

(define-public crate-const-enum-tools-derive-0.2.2 (c (n "const-enum-tools-derive") (v "0.2.2") (d (list (d (n "const-enum-tools") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ww158a2hkdi2n9z9pl73zqbyasf3n2c3zw0lzdlkwwnvl7762b1")))

(define-public crate-const-enum-tools-derive-0.3.2 (c (n "const-enum-tools-derive") (v "0.3.2") (d (list (d (n "const-enum-tools") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f5g7zx8l619kiyj11dym4zlzbq11fjwq6pxnisv8qnxiwd5830b")))

(define-public crate-const-enum-tools-derive-0.4.2 (c (n "const-enum-tools-derive") (v "0.4.2") (d (list (d (n "const-enum-tools") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zykjnyma7r1k230mz8p96lbnshig7rksypx052pc9rxic78pzq3")))

(define-public crate-const-enum-tools-derive-0.4.3 (c (n "const-enum-tools-derive") (v "0.4.3") (d (list (d (n "const-enum-tools") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y341gnf54v8vk3h29alk1r778fx7k4by34nbhgkbk7gaznzsdg8")))

