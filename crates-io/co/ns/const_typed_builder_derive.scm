(define-module (crates-io co ns const_typed_builder_derive) #:use-module (crates-io))

(define-public crate-const_typed_builder_derive-0.1.0 (c (n "const_typed_builder_derive") (v "0.1.0") (d (list (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12n4lj3ycmy7a0a1pygkp3ni57zx7wvw9r06zxaym1vifp8yhr0w")))

(define-public crate-const_typed_builder_derive-0.1.1 (c (n "const_typed_builder_derive") (v "0.1.1") (d (list (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01vlkar4q5h69bl9cdbwali363why08rk9s79ps5vaqvaqp66m2v")))

(define-public crate-const_typed_builder_derive-0.2.0 (c (n "const_typed_builder_derive") (v "0.2.0") (d (list (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12wz1kzk0fgmz70kd6rg68rncbfq0s3rxg6d3lqjxxxjcsay6n7y")))

(define-public crate-const_typed_builder_derive-0.3.0 (c (n "const_typed_builder_derive") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y8l7xgcnih62jfym25swrpj33gk4ifgwkq63adkvd5k34ah1bw1")))

