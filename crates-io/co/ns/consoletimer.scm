(define-module (crates-io co ns consoletimer) #:use-module (crates-io))

(define-public crate-consoletimer-0.1.1 (c (n "consoletimer") (v "0.1.1") (d (list (d (n "eventual") (r "^0.1.7") (d #t) (k 0)) (d (n "ncurses") (r "^5.97.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "05xmg1c68s0yxn2191mdxqkcdy9y3fpjha9bmwwy80frbxwcbbdv")))

(define-public crate-consoletimer-0.3.0 (c (n "consoletimer") (v "0.3.0") (d (list (d (n "eventual") (r "^0.1.7") (d #t) (k 0)) (d (n "ncurses") (r "^5.97.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1m2jxx0dxwccax35lw5q70yg0sbapwdrm6j2qxvszmqdalcjc4l0")))

(define-public crate-consoletimer-0.4.0 (c (n "consoletimer") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eventual") (r "^0.1.7") (d #t) (k 0)) (d (n "ncurses") (r "^5.97.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "0c0455svxys0wy3vr2vfn0xl28z5frq12r1lq4k25yqdfgvsfqdc")))

(define-public crate-consoletimer-0.4.1 (c (n "consoletimer") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eventual") (r "^0.1.7") (d #t) (k 0)) (d (n "ncurses") (r "^5.97.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1n3d2dhjh6lwkrvlm1kakcc39gyx1pk24b2gdif36y4ymqqfhdaj")))

