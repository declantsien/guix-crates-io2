(define-module (crates-io co ns console_color) #:use-module (crates-io))

(define-public crate-console_color-0.1.0 (c (n "console_color") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "wincon" "consoleapi" "processenv" "winnt" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "137a923318ywa2k7biyn34l7y563r04wf6q0796mh1wjfryr9yz3") (y #t)))

(define-public crate-console_color-0.1.1 (c (n "console_color") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "wincon" "consoleapi" "processenv" "winnt" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1c6gg2j0c8gzivnkx6qxjrkspbabmak9pnnz0j97cxi76n3kp9sa") (y #t)))

