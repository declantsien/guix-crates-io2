(define-module (crates-io co ns constexpr) #:use-module (crates-io))

(define-public crate-constexpr-0.0.1-alpha1 (c (n "constexpr") (v "0.0.1-alpha1") (d (list (d (n "constexpr-macros") (r "^0.0.1-alpha1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)))) (h "0q0i42wv6jvfpi9r4a3y9z54fnkih2wr892dml2fxv10df3lrmwy")))

