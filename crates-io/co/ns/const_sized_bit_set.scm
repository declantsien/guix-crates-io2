(define-module (crates-io co ns const_sized_bit_set) #:use-module (crates-io))

(define-public crate-const_sized_bit_set-0.1.0 (c (n "const_sized_bit_set") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "iai-callgrind") (r "^0.10.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_arrays") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0zb2dm07gzgsgypcqqbixdpx0mqm8a1ijwniv6ckm71c0nbsxc7h") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_arrays"))))))

