(define-module (crates-io co ns const_guards) #:use-module (crates-io))

(define-public crate-const_guards-0.1.1 (c (n "const_guards") (v "0.1.1") (d (list (d (n "const_guards_attribute") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)))) (h "1zp395pg8z3n57bkqbc82n7h60k6pbqx2kh9ivyfk6822hcyv0f2")))

(define-public crate-const_guards-0.1.2 (c (n "const_guards") (v "0.1.2") (d (list (d (n "const_guards_attribute") (r "^0.1.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)))) (h "13m1g8k9hf88gblxia79c0xidwzh25my88ynjy2x0wbi8j27k2mj")))

(define-public crate-const_guards-0.1.3 (c (n "const_guards") (v "0.1.3") (d (list (d (n "const_guards_attribute") (r "^0.1.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)))) (h "0l83f16l1iwmiiqs415vlifw73ya3v5b4rwqg5n03fv30x2sggii")))

