(define-module (crates-io co ns const-assert) #:use-module (crates-io))

(define-public crate-const-assert-1.0.0 (c (n "const-assert") (v "1.0.0") (h "1hr9ysbvl49pkjmyjybc7bk7wb2swwj2pwjkff48nn4zq9b1vhl0")))

(define-public crate-const-assert-1.0.1 (c (n "const-assert") (v "1.0.1") (h "11nxhyndgl08cxkk86jrrdxyyvg3pd70yvnvr98skcw162sqsafq")))

