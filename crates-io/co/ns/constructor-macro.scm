(define-module (crates-io co ns constructor-macro) #:use-module (crates-io))

(define-public crate-constructor-macro-0.2.0 (c (n "constructor-macro") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0cgmkbxjznniq89jm3lm3ksxy11gvqp4wpj4fh6s6rbp33kmrfd5")))

(define-public crate-constructor-macro-0.3.0 (c (n "constructor-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0shdkkan0fhzl3w9yqlmq0y015pfpk8xj1l5z79c6csr3iwpb121")))

(define-public crate-constructor-macro-0.4.0 (c (n "constructor-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0p37if1f540y13yyaal85ncz89rxg7hmka0164anbdjwfrbl3c6g")))

(define-public crate-constructor-macro-0.4.1 (c (n "constructor-macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0d0mz0i3xxq7w7fix2jwng71p4pwq16aq553zlgi4llhc4d17rp3")))

(define-public crate-constructor-macro-0.4.2 (c (n "constructor-macro") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0wl6s4d4kmcp9643qjhyjsmshrv41zqinzzkzqa455sjd82kpd8r")))

