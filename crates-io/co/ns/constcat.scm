(define-module (crates-io co ns constcat) #:use-module (crates-io))

(define-public crate-constcat-0.1.0 (c (n "constcat") (v "0.1.0") (h "1a8yp0i44hrl59k90kzkx2qdiv30h621cjvzzfnplsy4340asni2")))

(define-public crate-constcat-0.1.1 (c (n "constcat") (v "0.1.1") (h "16ky80qiv0c5cvl8037swbwch90mh69p0xk7xhvdwky9457dpbcv")))

(define-public crate-constcat-0.2.0 (c (n "constcat") (v "0.2.0") (h "07abb37z4s44m8jakp5dfs22malsra1k3h2wffzkaj6ighi4l0b8")))

(define-public crate-constcat-0.3.0 (c (n "constcat") (v "0.3.0") (h "0hd2q45rf8br3p7gpq8hx635jn3ngxqcfag51sl4y6w3rz2d0wpj") (f (quote (("default" "bytes") ("bytes"))))))

(define-public crate-constcat-0.3.1 (c (n "constcat") (v "0.3.1") (h "021d517fsfb86zrmzrbigjfimb9cq5x25bvsp6zpr22rwsp3aznd") (f (quote (("default" "bytes") ("bytes"))))))

(define-public crate-constcat-0.4.0 (c (n "constcat") (v "0.4.0") (h "07yy52cpwhi53aamxvx9qkjc5lsx79gyrh75bd3rhj5a9d2zlnrj") (f (quote (("_bytes"))))))

(define-public crate-constcat-0.4.1 (c (n "constcat") (v "0.4.1") (h "1l26h87jqarjd0vf5ls6103jpy7gfg4p5f0wgf1hadzqgv2x0p3x") (f (quote (("_bytes"))))))

(define-public crate-constcat-0.5.0 (c (n "constcat") (v "0.5.0") (h "0l9ns1ms9bh95hmy25v8z0bn4a0a1jc6455h5l4mp5dii7wmlbhz") (f (quote (("_bytes"))))))

