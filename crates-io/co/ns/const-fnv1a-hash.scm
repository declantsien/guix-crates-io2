(define-module (crates-io co ns const-fnv1a-hash) #:use-module (crates-io))

(define-public crate-const-fnv1a-hash-1.0.0 (c (n "const-fnv1a-hash") (v "1.0.0") (h "1a2dvanj64hlz233jidab686i1lgnfljv99xb8b6062vh3m769k5")))

(define-public crate-const-fnv1a-hash-1.0.1 (c (n "const-fnv1a-hash") (v "1.0.1") (h "0v8993l0rjysrym08rlr0bdjbrksxni1cq1i7r28h8s7qv7z7ifs")))

(define-public crate-const-fnv1a-hash-1.1.0 (c (n "const-fnv1a-hash") (v "1.1.0") (h "1jicrrr85ckvy8scp4qmxj37r19ajjrichz3g6xbw4m842hkxc9j")))

