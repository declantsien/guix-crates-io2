(define-module (crates-io co ns constime) #:use-module (crates-io))

(define-public crate-constime-0.1.0 (c (n "constime") (v "0.1.0") (h "0w3mva66c8zkl23m7r3hj6lackbdsnimh04xkkpcnl92gvmlwh95")))

(define-public crate-constime-0.2.0 (c (n "constime") (v "0.2.0") (d (list (d (n "ureq") (r "^2.6.1") (d #t) (k 2)))) (h "1vpvfa7gydxzz018g61afqbc4ccy8axbw1idm81wpicf4bfkiw2q")))

(define-public crate-constime-0.2.1 (c (n "constime") (v "0.2.1") (d (list (d (n "ureq") (r "^2.6.1") (d #t) (k 1)))) (h "1xf0pmw7fkq6gnmjh93aar9cx69pm62r5i83mbh46grhcvwvf19l")))

(define-public crate-constime-0.2.2 (c (n "constime") (v "0.2.2") (d (list (d (n "ureq") (r "^2.6.1") (d #t) (k 2)))) (h "0n6sm3z3jq92j744w8xvwf8mhrzm84ga2gfcm3ijncxxz0diib1k")))

