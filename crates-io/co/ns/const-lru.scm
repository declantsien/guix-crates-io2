(define-module (crates-io co ns const-lru) #:use-module (crates-io))

(define-public crate-const-lru-0.1.0 (c (n "const-lru") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r ">=0.2") (k 0)))) (h "1l9fzj20lfq4cwmqbs5i8d9dirnga1sszff2f9z957nyh4l30anm")))

(define-public crate-const-lru-0.2.0 (c (n "const-lru") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r ">=0.2") (k 0)))) (h "0paf7y047rbl5agj9s5vphabi12cy90gnjph35v47yc83w6792g9")))

(define-public crate-const-lru-0.2.1 (c (n "const-lru") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r ">=0.2") (k 0)))) (h "1q1smj9pbqly4h0x1q9yxdykv3r7ay9z629rzkh55y80sgwxy15i")))

(define-public crate-const-lru-0.2.2 (c (n "const-lru") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r ">=0.2") (k 0)))) (h "0a67x0vvlxa3sp1fr6vq8zhc2c98k6b6wrx7a3qgjzyw7hhz4x1h")))

(define-public crate-const-lru-1.0.0 (c (n "const-lru") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-traits") (r ">=0.2") (k 0)))) (h "1l1akibygrlixk6687nj4w04hi0qzpdpl93gwr2fw3wp4mw1fvnz")))

