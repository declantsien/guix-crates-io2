(define-module (crates-io co ns console-listener) #:use-module (crates-io))

(define-public crate-console-listener-0.1.0 (c (n "console-listener") (v "0.1.0") (h "0hzpnii489g2nxcrb5pc0cwa2g430gw3vrs6wadcpzda8r1scfnd")))

(define-public crate-console-listener-0.1.1 (c (n "console-listener") (v "0.1.1") (h "0zhpy15plxxz2lp879xn0l5qycv4gf28idlqmb4k008himsn2haw")))

(define-public crate-console-listener-0.1.2 (c (n "console-listener") (v "0.1.2") (h "1njqhlnb1y9qgqigqxpija3bg7l61x4xhyf4abhalh9gjz5w8zal")))

(define-public crate-console-listener-0.1.3 (c (n "console-listener") (v "0.1.3") (h "1995j1zhyv3g196w8pa5s2149lp0hspnwwzk9d1vbkv5fnd01s62")))

(define-public crate-console-listener-0.1.4 (c (n "console-listener") (v "0.1.4") (h "1jv804vz46lzw6hldw4y6h0ixpqdkx7x7z3hrxkch6vx02yxf6m2")))

(define-public crate-console-listener-0.2.0 (c (n "console-listener") (v "0.2.0") (h "01vfqsin2zmg814nd5srv7nlm6ay743k8xkdqc3ra2ai0lj1b61h")))

