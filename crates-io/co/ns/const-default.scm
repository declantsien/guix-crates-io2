(define-module (crates-io co ns const-default) #:use-module (crates-io))

(define-public crate-const-default-0.1.0 (c (n "const-default") (v "0.1.0") (d (list (d (n "const-default-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "12r2ky3nx01l6wsm2pxvk1gwmz8lcfj51wg71yvinddsa7s897fm") (f (quote (("unstable") ("std" "alloc") ("derive" "const-default-derive") ("alloc"))))))

(define-public crate-const-default-0.2.0 (c (n "const-default") (v "0.2.0") (d (list (d (n "const-default-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1ray0pryxr4kvxibm4qfdpv34367pzdwvwhfj0jcbyvf8r3q77is") (f (quote (("unstable") ("std" "alloc") ("derive" "const-default-derive") ("alloc"))))))

(define-public crate-const-default-0.3.0 (c (n "const-default") (v "0.3.0") (d (list (d (n "const-default-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0irrnlpwz8f4jyxvdv3c1ls0myjv4s5znp6lm7nb8j9vih2md4mx") (f (quote (("unstable") ("std" "alloc") ("enable-atomics") ("derive" "const-default-derive") ("default" "enable-atomics") ("alloc"))))))

(define-public crate-const-default-0.3.1 (c (n "const-default") (v "0.3.1") (d (list (d (n "const-default-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1krniizsgwbdxk63w38140kbk92k6w5sbm5di57d54w7v86w1630") (f (quote (("unstable") ("std" "alloc") ("enable-atomics") ("derive" "const-default-derive") ("default" "enable-atomics") ("alloc"))))))

(define-public crate-const-default-1.0.0-beta1 (c (n "const-default") (v "1.0.0-beta1") (d (list (d (n "const-default-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0x7aq12hnh9pcxknm9027gz7xbxa4dxw8qi1l06iasxhmiig05yl") (f (quote (("unstable-docs") ("unstable") ("std" "alloc") ("enable-atomics") ("derive" "const-default-derive") ("default" "enable-atomics") ("alloc"))))))

(define-public crate-const-default-1.0.0 (c (n "const-default") (v "1.0.0") (d (list (d (n "const-default-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1apcnxfrz5xsfxaxbv1n9c5sdfqlmrk81v0q29z5amflfqgnsf8b") (f (quote (("unstable-docs") ("unstable") ("std" "alloc") ("enable-atomics") ("derive" "const-default-derive") ("default" "enable-atomics") ("alloc"))))))

