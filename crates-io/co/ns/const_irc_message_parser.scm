(define-module (crates-io co ns const_irc_message_parser) #:use-module (crates-io))

(define-public crate-const_irc_message_parser-0.1.0 (c (n "const_irc_message_parser") (v "0.1.0") (h "1vrmk0a5cbd90pxshi5ajm83m0rp18kryv3aw7ij6krd24lfgfym")))

(define-public crate-const_irc_message_parser-0.1.1 (c (n "const_irc_message_parser") (v "0.1.1") (h "0dd35cfrxr36zppc9g51mazrh4f2gzb635cm9fq5p0icy742r74m")))

(define-public crate-const_irc_message_parser-0.2.0 (c (n "const_irc_message_parser") (v "0.2.0") (h "16ads9s771lc8lwdg020qdbnj33jhh23rdhg239c6dsq7mzmba2g")))

(define-public crate-const_irc_message_parser-0.3.0 (c (n "const_irc_message_parser") (v "0.3.0") (h "1wwj9bqlfq2vbsm1rbaik6p4w8lg59ss1k8wyzj5c8h90775fcdc") (r "1.71")))

(define-public crate-const_irc_message_parser-0.4.0 (c (n "const_irc_message_parser") (v "0.4.0") (h "0j0qzhhla639398kcjxjp7iaba9sq410ni4lszbzlgqyl34721dd") (r "1.71")))

(define-public crate-const_irc_message_parser-0.5.0 (c (n "const_irc_message_parser") (v "0.5.0") (h "1x5fn6y5g2vkmcppkxsnwbbqknxkhcfzvn1w3yhb2k925ny5f7z3") (r "1.71")))

(define-public crate-const_irc_message_parser-0.6.0 (c (n "const_irc_message_parser") (v "0.6.0") (h "1y7qcdq6bharbmw0rcdmnfp01ymdhpfxj53v9cpjpdjawrxh0mil") (r "1.71")))

(define-public crate-const_irc_message_parser-0.7.0 (c (n "const_irc_message_parser") (v "0.7.0") (h "0b5yxjwyznd8glg2k27hk7nqls9q84faqbvm6iclfkcahy08fm9y") (r "1.71")))

(define-public crate-const_irc_message_parser-0.7.1 (c (n "const_irc_message_parser") (v "0.7.1") (h "02r4nd9b0xs131307f279c9dp136zh44i5xfm2xwgij6k2y8pbs7") (r "1.71")))

