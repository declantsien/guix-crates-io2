(define-module (crates-io co ns const-gen-derive) #:use-module (crates-io))

(define-public crate-const-gen-derive-0.1.0 (c (n "const-gen-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08ja31wcdplkkjrhw701c2rzkqkli5b5pvrwxdm6ysxbp1pg3g6r")))

(define-public crate-const-gen-derive-0.1.1 (c (n "const-gen-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "166ai7av2b1a8nfqk0gmz07x8f7rm3apa0bgqh6qmwsifrildp8s")))

(define-public crate-const-gen-derive-0.1.2 (c (n "const-gen-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vabpyaw98wln4z08zf0czbpfbf8lqxva5ys00jh0rcdzlv4s5l7")))

(define-public crate-const-gen-derive-0.2.0 (c (n "const-gen-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05whjbclxr4w628wck2f2qqrpk40bik67bda8lsckvqnqdcl1yly")))

(define-public crate-const-gen-derive-0.2.2 (c (n "const-gen-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1js02x7vsy5199kqpr1xr6mndlhmaa0hpz6d0x9npvgkb6x44a63")))

(define-public crate-const-gen-derive-0.4.0 (c (n "const-gen-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vv23095jg3yb8kjp7yv2q7g58v05j2vf8vbgbjy5zjpq8hn7m3y")))

(define-public crate-const-gen-derive-0.5.0 (c (n "const-gen-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jvz39cgjs4h2ld04bfckrgbfa8nnq98q7j9g431qfkk3qp4w2wj")))

(define-public crate-const-gen-derive-1.0.0 (c (n "const-gen-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jqyfnnrlrw5dy37w50rhfv39qdmyfhgmpf425ssvxrkcgagyxvc")))

(define-public crate-const-gen-derive-1.1.0 (c (n "const-gen-derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ph0vp5g478fmrvwgnm1g1nvpmsww90s3396m0ayydqcpwp0k5ad")))

(define-public crate-const-gen-derive-1.1.1 (c (n "const-gen-derive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "044m9j1mk3cwkbmfl7k42cwlk9h6czdx0zmprh4jnxgp5lf0js04")))

(define-public crate-const-gen-derive-1.1.2 (c (n "const-gen-derive") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1apysxljkz7jxnrj2wg26m095fbfni04qrmhvlf3xx4y3v3s1ivk")))

(define-public crate-const-gen-derive-1.1.3 (c (n "const-gen-derive") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03fh02kbza1ah8pinhajpwrcz32hyswjwy9xxhg00yvy1ahfgp01") (y #t)))

(define-public crate-const-gen-derive-1.1.4 (c (n "const-gen-derive") (v "1.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19sfhrx3m257pgb31jf8jl7vs4x1hzhd3w1n0yh7kwsd2gfc8xhk")))

(define-public crate-const-gen-derive-1.1.5 (c (n "const-gen-derive") (v "1.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "007idypaasvgckhf8g4z24yfvdbakfphflqpi5kxfxq57bss5cra")))

