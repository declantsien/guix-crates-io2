(define-module (crates-io co ns const_cmp) #:use-module (crates-io))

(define-public crate-const_cmp-0.0.0 (c (n "const_cmp") (v "0.0.0") (h "1b28y2pa6pzawn79ifmpcdi4rfx1vwj99h46mrh0qnbcczkdk3b2") (f (quote (("str" "polymorphism") ("slice" "str" "polymorphism") ("range" "polymorphism") ("primitives" "polymorphism") ("polymorphism") ("other" "polymorphism") ("option" "polymorphism") ("nonzero" "polymorphism") ("default" "slice" "nonzero" "option" "range" "primitives" "other" "polymorphism") ("const_generics"))))))

