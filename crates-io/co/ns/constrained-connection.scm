(define-module (crates-io co ns constrained-connection) #:use-module (crates-io))

(define-public crate-constrained-connection-0.1.0 (c (n "constrained-connection") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1c56jyfbpy9j6dgab18di2vk2wpkkirw1qn63pvfr8l6b3jmnhgk")))

