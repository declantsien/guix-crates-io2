(define-module (crates-io co ns const_panic_proc_macros) #:use-module (crates-io))

(define-public crate-const_panic_proc_macros-0.2.0 (c (n "const_panic_proc_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1addx3a8vi02cdak3ygrqivv02jj73251h85x49aic78yznrhlrr")))

