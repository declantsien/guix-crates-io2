(define-module (crates-io co ns consalign) #:use-module (crates-io))

(define-public crate-consalign-0.1.0 (c (n "consalign") (v "0.1.0") (d (list (d (n "consprob-trained") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "06j17kbdvnd21xwbvij5z2qx0mh596lzyf4y0gszlwmckzh36wrh")))

(define-public crate-consalign-0.1.1 (c (n "consalign") (v "0.1.1") (d (list (d (n "consprob-trained") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "03li61v2aj7yw759wvhbsnzx9j5701kldh1q19qcihyzc5s1qlyh")))

(define-public crate-consalign-0.1.2 (c (n "consalign") (v "0.1.2") (d (list (d (n "consprob-trained") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0z389bfygpbbw2l0mn98mqr28x0bbxx0z6mswmv3y6qf875mgdsa")))

(define-public crate-consalign-0.1.3 (c (n "consalign") (v "0.1.3") (d (list (d (n "consprob-trained") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0dblr4ansfrb7w14jiar6iij8dn3mkap7qawlplxgw9xybcrl9f0")))

(define-public crate-consalign-0.1.4 (c (n "consalign") (v "0.1.4") (d (list (d (n "consprob-trained") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "1lh7i07vrcmmr8d6g3rg5j3kmxqcbnnf3rcmca02rjd9y6j4ax07")))

(define-public crate-consalign-0.1.5 (c (n "consalign") (v "0.1.5") (d (list (d (n "consprob-trained") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0zr6jpy3awjzqir8whl7ynclzd9d7yk0cjzqfjpcn4mjp74374h2")))

(define-public crate-consalign-0.1.6 (c (n "consalign") (v "0.1.6") (d (list (d (n "consprob-trained") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "1ab50m3npmzirqwfvl8m5mhmfi2ckzrvbmn4vp4vv30f425gvxxz")))

(define-public crate-consalign-0.1.7 (c (n "consalign") (v "0.1.7") (d (list (d (n "consprob-trained") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "1jc66grif7c21dj6c1wxn0v5z3rhzkzr2fbjvyqamc7dclz8y2gv")))

(define-public crate-consalign-0.1.8 (c (n "consalign") (v "0.1.8") (d (list (d (n "consprob-trained") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0fr8clpkpkqy24myrsss8rx1zc3gl8r8167f45clbzg3ggskpsga")))

(define-public crate-consalign-0.1.9 (c (n "consalign") (v "0.1.9") (d (list (d (n "consprob-trained") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0j74l953r0ian3r0vj5fmwjz58fd3hjhsrqfs3157cs4nj5pbl9c")))

