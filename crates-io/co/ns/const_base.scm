(define-module (crates-io co ns const_base) #:use-module (crates-io))

(define-public crate-const_base-0.1.0 (c (n "const_base") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (k 2)) (d (n "itertools") (r "^0.10") (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)))) (h "1smcvzn18lxfy15grgxfy1w8jcgmsz4wd5py5v9g9x3i0f2dhscg")))

(define-public crate-const_base-0.1.1 (c (n "const_base") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (k 2)) (d (n "data-encoding") (r "^2.3.2") (k 2)) (d (n "itertools") (r "^0.10") (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)))) (h "1qd371nn68nj976dw7vgjv6c6n5nb0y7gcqpqj29mfnx7qdihm70")))

(define-public crate-const_base-0.1.2 (c (n "const_base") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (k 2)) (d (n "data-encoding") (r "^2.3.2") (k 2)) (d (n "itertools") (r "^0.10") (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)))) (h "1z59lh6466dszv9iby6vl5s86r559w48k88l9dfkf3183blx558m")))

(define-public crate-const_base-0.2.0 (c (n "const_base") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (k 2)) (d (n "const_panic") (r "^0.2.7") (f (quote ("rust_1_64"))) (k 0)) (d (n "data-encoding") (r "^2.3.2") (k 2)) (d (n "itertools") (r "^0.10") (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1d5xd5pjbbqdizrngz7br08gr96081wjay0bbn1avlbixdz2jjxn") (f (quote (("__test")))) (r "1.64.0")))

