(define-module (crates-io co ns const_num_bigint) #:use-module (crates-io))

(define-public crate-const_num_bigint-0.1.0 (c (n "const_num_bigint") (v "0.1.0") (d (list (d (n "const_std_vec") (r "^0.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "01mcj87iy0rdjmh5pgdkakw2c9zpdxby41c8nnl3nyjb3nrlg503")))

(define-public crate-const_num_bigint-0.2.0 (c (n "const_num_bigint") (v "0.2.0") (d (list (d (n "const_num_bigint_derive") (r "^0.1") (d #t) (k 0)) (d (n "const_std_vec") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0r8kmg905yypks04gnx664s135akr53v5ghpqpvdk7bvqpg0c0kr")))

(define-public crate-const_num_bigint-0.2.1 (c (n "const_num_bigint") (v "0.2.1") (d (list (d (n "const_num_bigint_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "const_std_vec") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)))) (h "0lcjrz4cq2jr4ypg1x2s60hljn0x06g55g04cx2f4vjlbnnf1njx")))

