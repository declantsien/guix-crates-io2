(define-module (crates-io co ns const-units) #:use-module (crates-io))

(define-public crate-const-units-0.1.0 (c (n "const-units") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "17i8an8l8k8xlfvaffv64a3f0iyks49v2bdm3k1b83h6ra2gn2ml")))

(define-public crate-const-units-0.1.1 (c (n "const-units") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1zkkx9x6wfh4h6cdhm734130lafpf94lwnavq6kkx3574166x5fv")))

(define-public crate-const-units-0.1.2 (c (n "const-units") (v "0.1.2") (d (list (d (n "const-str") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "033zl30kabddq2z2ifif3g1bak0wa0yq7c7kf5dwqdi5hshpzw0k") (f (quote (("std") ("dyn") ("default" "std" "const") ("const"))))))

(define-public crate-const-units-0.1.3 (c (n "const-units") (v "0.1.3") (d (list (d (n "const-str") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "121ilpk38spim9ms4mpgdwl2hbqj7zcaqlqlgsc33a7l5kb39402") (f (quote (("std") ("dyn") ("default" "std" "const") ("const"))))))

