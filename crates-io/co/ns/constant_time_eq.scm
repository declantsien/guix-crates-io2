(define-module (crates-io co ns constant_time_eq) #:use-module (crates-io))

(define-public crate-constant_time_eq-0.1.0 (c (n "constant_time_eq") (v "0.1.0") (h "1w74kgfagf5l6z7d0lah8jl81lh5k09z0p91wjgjd802qwwd6lmn")))

(define-public crate-constant_time_eq-0.1.1 (c (n "constant_time_eq") (v "0.1.1") (h "1wza22bxwiv1nj3xiq4vxmj1cc4ds1pg7bw5plax1y271phddsgv") (y #t)))

(define-public crate-constant_time_eq-0.1.2 (c (n "constant_time_eq") (v "0.1.2") (h "08qkfbc13mhh3zriri4sv4j9k4fbkcwgz9zrcbv1qvqgkyavgp07")))

(define-public crate-constant_time_eq-0.1.3 (c (n "constant_time_eq") (v "0.1.3") (h "17janp8n9dd6kjbbgqiayrh9fw81v4cq9rz04926s5nf4pi15w4g")))

(define-public crate-constant_time_eq-0.1.4 (c (n "constant_time_eq") (v "0.1.4") (h "083icpr9xb72rrdxw3p4068dcspn6ai22jy7rhl2a8grfz448nlr")))

(define-public crate-constant_time_eq-0.1.5 (c (n "constant_time_eq") (v "0.1.5") (h "1g3vp04qzmk6cpzrd19yci6a95m7ap6wy7wkwgiy2pjklklrfl14")))

(define-public crate-constant_time_eq-0.2.0 (c (n "constant_time_eq") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("cargo_bench_support" "html_reports"))) (d #t) (k 2)))) (h "0g7qvimfv2lrxxqrxa3cmh5mywv1ljwlis59jrx3zf29x8zaqcjx")))

(define-public crate-constant_time_eq-0.2.1 (c (n "constant_time_eq") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("cargo_bench_support" "html_reports"))) (d #t) (k 2)))) (h "0nn6nc5h5klwkxhf8y44x59p9x78cnqivz3bxckqq94igqxaa16z")))

(define-public crate-constant_time_eq-0.2.2 (c (n "constant_time_eq") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("cargo_bench_support" "html_reports"))) (d #t) (k 2)))) (h "1lliwqh3mk8f710bidcbkrcy88vbjxvm6fry8mhqx4b10dbslc8f") (r "1.59")))

(define-public crate-constant_time_eq-0.2.3 (c (n "constant_time_eq") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (f (quote ("cargo_bench_support" "html_reports"))) (d #t) (k 2)))) (h "0dwk8gz4rd8aywz3dy4rx82rl9pbap4vlji07mli13k989b575lb") (r "1.59")))

(define-public crate-constant_time_eq-0.2.4 (c (n "constant_time_eq") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (f (quote ("cargo_bench_support" "html_reports"))) (d #t) (k 2)))) (h "0ycj3vn8g9lnkzv8wajz0r9rc2xgixs8j3pb0ivb7isxyv0qbbgk") (r "1.59")))

(define-public crate-constant_time_eq-0.2.5 (c (n "constant_time_eq") (v "0.2.5") (d (list (d (n "criterion") (r "^0.4") (f (quote ("cargo_bench_support" "html_reports"))) (d #t) (k 2)))) (h "0sy7bs12dfa2d5hw7759b0mvjqcs85giajg4qyg39xq8a1s8wh8k") (r "1.59")))

(define-public crate-constant_time_eq-0.2.6 (c (n "constant_time_eq") (v "0.2.6") (d (list (d (n "count_instructions") (r "^0.1.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("cargo_bench_support" "html_reports"))) (d #t) (k 2)))) (h "1mhshq5qzdbqq5d8sp695kf5rnh47kydqlsv87kpg0r89l53r991") (f (quote (("count_instructions_test")))) (r "1.59")))

(define-public crate-constant_time_eq-0.3.0 (c (n "constant_time_eq") (v "0.3.0") (d (list (d (n "count_instructions") (r "^0.1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("cargo_bench_support" "html_reports"))) (d #t) (k 2)))) (h "1hl0y8frzlhpr58rh8rlg4bm53ax09ikj2i5fk7gpyphvhq4s57p") (f (quote (("count_instructions_test")))) (r "1.66.0")))

