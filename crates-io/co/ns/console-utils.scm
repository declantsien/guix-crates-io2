(define-module (crates-io co ns console-utils) #:use-module (crates-io))

(define-public crate-console-utils-1.0.0 (c (n "console-utils") (v "1.0.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0w688c12nk3yzyiip0pv5iq1rp1f532d83gbgr7rpcsl6fbc1z64")))

(define-public crate-console-utils-1.0.1 (c (n "console-utils") (v "1.0.1") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0h1w4cggyq1d21crni79fxb5sws6h1ivdyfqn0gg4q6a657hrly0")))

(define-public crate-console-utils-1.0.2 (c (n "console-utils") (v "1.0.2") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0l76l9y2388ayx3vwcsmb0mp0724awhxzxs2l3645g2ggj1zbfix")))

(define-public crate-console-utils-1.0.3 (c (n "console-utils") (v "1.0.3") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0z1jhmd73rpvcgw5s222jwk0insnrp37v28y32ls07yy6s00bv72")))

(define-public crate-console-utils-1.1.4 (c (n "console-utils") (v "1.1.4") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "15ci07yg60lg424wk62fwm0gxh8x5hjg7l2fds2nkgr116fqzaxc")))

(define-public crate-console-utils-1.1.5 (c (n "console-utils") (v "1.1.5") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "156wvqizwnkh0ybnlb5z5fh5rc6b0sx8yjyzal37s86r8bg57dmn")))

(define-public crate-console-utils-1.1.6 (c (n "console-utils") (v "1.1.6") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0n3p6rkjikv2zhfjxzgcrzap3g45gs2f12q6wv3dms93fazp1gp1")))

(define-public crate-console-utils-1.1.7 (c (n "console-utils") (v "1.1.7") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "1hs8x2slf01s7f8n3xdfbhbs1mr79rvb1qli4lwzs9mb9rq0bdml")))

(define-public crate-console-utils-1.1.8 (c (n "console-utils") (v "1.1.8") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0cx4i6sh4jh9vhi060idlarj07ryngc4h6q36w3hvc84z797f923")))

(define-public crate-console-utils-1.1.9 (c (n "console-utils") (v "1.1.9") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "05cbm17cvj9sb1hmq2qnzq6kl43bd16fmykc5kkc5vbkm3l8bjfg")))

(define-public crate-console-utils-1.1.10 (c (n "console-utils") (v "1.1.10") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "07d8wi8pp05q5w6qi8fm2m0xbj1g892cghzdknjnnl2i32rwafbg")))

(define-public crate-console-utils-1.2.0 (c (n "console-utils") (v "1.2.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "1dz742kzhn0wy4rwwxphdw0q4f5ikw2sbyjrd2m7zsyhfhhfa16i")))

(define-public crate-console-utils-1.2.1 (c (n "console-utils") (v "1.2.1") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "1r6dra29cvcxvbcdgv8gvjpmgg3hiyqh6pflrkm72nk3j1k8vj72") (y #t)))

(define-public crate-console-utils-1.2.2 (c (n "console-utils") (v "1.2.2") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "03hfjb8jxgrz5w0w0lmc34wkmxmlph4yyyzsdk1l5gp1fvpkd409") (y #t)))

(define-public crate-console-utils-1.2.3 (c (n "console-utils") (v "1.2.3") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "1ymcm24rzslrcbsna4lxg9ylra0mziz1ik1djqv2rbmqqws7pjha") (y #t)))

(define-public crate-console-utils-1.2.4 (c (n "console-utils") (v "1.2.4") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "1a5ar53crhnjpadvjwphn05skf6ibsgqvn2i1a3dqni9wx70iivc")))

(define-public crate-console-utils-1.3.0 (c (n "console-utils") (v "1.3.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "1jlj9y8g2zyyyi3q1cnn82n3h6z9g5hkchm3nl1l0dji1v5zxrvb")))

(define-public crate-console-utils-1.4.0 (c (n "console-utils") (v "1.4.0") (d (list (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0xwd88w8y2b7c4frqanq53q5b7ngc8ybbvw0wwd7wn5cvql8ifis")))

(define-public crate-console-utils-1.5.0 (c (n "console-utils") (v "1.5.0") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wavm0wfbs7mqdwqznqk7rn4rsa3dna7f9l836knwcr9hncr0kj8") (y #t)))

(define-public crate-console-utils-1.5.1 (c (n "console-utils") (v "1.5.1") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "080ckz168vg94k20h272f9b1n8xbk1xhnqi6rvd6haa8xhsdzp9m") (y #t)))

(define-public crate-console-utils-1.5.2 (c (n "console-utils") (v "1.5.2") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0brd9p6fg6f97xfpj53cv4ls9r53vzg28grxisc80phhrh31a4sa") (y #t)))

(define-public crate-console-utils-1.5.3 (c (n "console-utils") (v "1.5.3") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_System_SystemServices" "Win32_UI" "Win32_UI_Input" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0g486xbrh6kfqj5cdrnw237n8h34x2zhc6gsahyxm9ri44h1v9s7") (y #t)))

(define-public crate-console-utils-1.5.4 (c (n "console-utils") (v "1.5.4") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_System_SystemServices" "Win32_UI" "Win32_UI_Input" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0wcavzzfp1g3f3mbfqjxglxw5z7pd13sp4s01w3b3m9m7cvkrqbi") (y #t)))

(define-public crate-console-utils-1.5.5 (c (n "console-utils") (v "1.5.5") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_System_SystemServices" "Win32_UI" "Win32_UI_Input" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0np5dk0lnlsq1xw06fxyk7v072gvnqbpbhmi37q2vv2b6p5ygnkj") (y #t)))

(define-public crate-console-utils-1.5.6 (c (n "console-utils") (v "1.5.6") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_UI" "Win32_UI_Input" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1js09qi6nlv2dzxwddmizdnjjcswydwqp8kppbmjv31v0q29r2fz")))

(define-public crate-console-utils-1.5.7 (c (n "console-utils") (v "1.5.7") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1apzcr4vw93v16srma6khnpz4gddgj9889hhj3b5lcn2kicrgm5i")))

(define-public crate-console-utils-1.5.8 (c (n "console-utils") (v "1.5.8") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0z307ra0f33krw7j9hhyk46fjhnnr40vbarvr0md5xl7whv7hd68")))

(define-public crate-console-utils-1.5.9 (c (n "console-utils") (v "1.5.9") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_UI_Input_KeyboardAndMouse"))) (t "cfg(windows)") (k 0)))) (h "1lmlbahq9676x041p4rxbi475yx8353gbw0fj19l2jc9w6p8q110")))

(define-public crate-console-utils-1.6.0 (c (n "console-utils") (v "1.6.0") (d (list (d (n "libc") (r "^0.2.150") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_UI_Input_KeyboardAndMouse"))) (t "cfg(windows)") (k 0)))) (h "1kq4dlzf9spwy0zmdmzfbxrznlv0q0liqi2yawzjz7hppmgdkbcc")))

