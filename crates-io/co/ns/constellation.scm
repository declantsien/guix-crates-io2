(define-module (crates-io co ns constellation) #:use-module (crates-io))

(define-public crate-constellation-0.1.0 (c (n "constellation") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.3.20") (d #t) (k 0)) (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.6.0") (d #t) (k 0)) (d (n "tuple_utils") (r "^0.2") (d #t) (k 0)))) (h "08q7brm6i2akl198srfk8m0a57wpwkvlsfi7skdi9arzhj13qi2p")))

(define-public crate-constellation-0.2.0 (c (n "constellation") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.3.20") (d #t) (k 0)) (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "rayon") (r "^0.6.0") (d #t) (k 0)) (d (n "tuple_utils") (r "^0.2") (d #t) (k 0)))) (h "03hbzgyipjbc7zk8figaibh3dg5rrq942646jcpz27jr98q4jjhm")))

