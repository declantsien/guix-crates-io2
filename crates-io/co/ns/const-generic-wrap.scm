(define-module (crates-io co ns const-generic-wrap) #:use-module (crates-io))

(define-public crate-const-generic-wrap-0.1.0 (c (n "const-generic-wrap") (v "0.1.0") (d (list (d (n "typenum") (r "^1.13.0") (o #t) (d #t) (k 0)))) (h "00bjb1hvmnzamzswk6qiplhnvg9qmijrjcirlk8kb2v79phlmwf4") (f (quote (("unstable") ("default"))))))

(define-public crate-const-generic-wrap-0.2.0 (c (n "const-generic-wrap") (v "0.2.0") (d (list (d (n "typenum") (r "^1.13.0") (o #t) (d #t) (k 0)))) (h "01zx5sfzz6mildn9vamcil0j2knp9akn3svifwa6ifk72bivvb2q") (f (quote (("unstable") ("default")))) (y #t)))

(define-public crate-const-generic-wrap-0.3.0 (c (n "const-generic-wrap") (v "0.3.0") (d (list (d (n "typenum") (r "^1.13.0") (o #t) (d #t) (k 0)))) (h "0hsrdqmkaib41afvjir0x6pm9q9y7cwyckhfyng777b06y9dv9pb") (f (quote (("unstable") ("default"))))))

