(define-module (crates-io co ns const_for) #:use-module (crates-io))

(define-public crate-const_for-0.1.0 (c (n "const_for") (v "0.1.0") (h "06p3brvn16sbhkzf1n62248b1g65cpgqh72fa0q6ks73cd1kchk0")))

(define-public crate-const_for-0.1.1 (c (n "const_for") (v "0.1.1") (h "13pdx6l4z48zwrx9hfbsv24ss0amvyp6gi77m5p2l5pjc5v8d317")))

(define-public crate-const_for-0.1.2 (c (n "const_for") (v "0.1.2") (h "0wvjv1i0bc9p3vs40kvs1wkfvyc9y1j10i8xa4kdjdsh3793vr10")))

(define-public crate-const_for-0.1.3 (c (n "const_for") (v "0.1.3") (h "0n7ricjfgh78q6h4jrz2smj8dnq6ixzci5qqf601pszi61sxql2z")))

(define-public crate-const_for-0.1.4 (c (n "const_for") (v "0.1.4") (h "1j01929kmck9lfk9s8c9343x2ivgw65pnah65i1gdfnir1a6m3wb")))

