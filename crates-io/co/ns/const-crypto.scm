(define-module (crates-io co ns const-crypto) #:use-module (crates-io))

(define-public crate-const-crypto-0.1.0 (c (n "const-crypto") (v "0.1.0") (d (list (d (n "bs58") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^4.1.2") (d #t) (k 2)) (d (n "keccak-const") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sha2-const-stable") (r "^0.1.0") (d #t) (k 0)))) (h "0b7jjnclyigxhw8dkws1kq1a51m8ih1qfqvxga3z2f92k76kmihr")))

(define-public crate-const-crypto-0.2.0 (c (n "const-crypto") (v "0.2.0") (d (list (d (n "bs58") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^4.1.2") (d #t) (k 2)) (d (n "keccak-const") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sha2-const-stable") (r "^0.1.0") (d #t) (k 0)))) (h "0g4pm14zaxd8m1w2w2hnpldr9pjl0hnj8jjvfp46qji0lvx6alvb")))

