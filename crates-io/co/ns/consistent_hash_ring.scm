(define-module (crates-io co ns consistent_hash_ring) #:use-module (crates-io))

(define-public crate-consistent_hash_ring-0.1.0 (c (n "consistent_hash_ring") (v "0.1.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "0jdd1caz9vjjsd4wh2hyb3z78ddz86l5vvb0lrvqjcvjcphl63cz")))

(define-public crate-consistent_hash_ring-0.2.0 (c (n "consistent_hash_ring") (v "0.2.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "00c8bw9cw4n8i991yvardv1v1823q6q9f9x362cwzrax6hn5g74s")))

(define-public crate-consistent_hash_ring-0.2.1 (c (n "consistent_hash_ring") (v "0.2.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "0wyaaqmcmmqwc0jbqxh8pyjgmfqv1ppjrf0zq9yvmgbdgm25s1g3")))

(define-public crate-consistent_hash_ring-0.2.2 (c (n "consistent_hash_ring") (v "0.2.2") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "1jq8i8hj9mj7v60bs5d9r7ma7l8z383dza4rcw05chv19afc1010")))

(define-public crate-consistent_hash_ring-0.3.0 (c (n "consistent_hash_ring") (v "0.3.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "0qbhsrwq50rqg1jl389ix3db9yysj1gs15pz4zrg2bdp2gmscizl")))

(define-public crate-consistent_hash_ring-0.3.1 (c (n "consistent_hash_ring") (v "0.3.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "0nigqjq3mi9h29v58j23xp0jcwsdb0v5v2kk62593rzg7v48rhlv")))

(define-public crate-consistent_hash_ring-0.3.2 (c (n "consistent_hash_ring") (v "0.3.2") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "1rz7m4q2ip6i6fi0gskxs6l2pc275zsacwmm4rcdyqjj5kwjhbqk")))

(define-public crate-consistent_hash_ring-0.3.3 (c (n "consistent_hash_ring") (v "0.3.3") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "0cb9qi7w5lvqbvn42c2pvx6hpwn33fnbh3hhqgpv6gip1kqla3c0")))

(define-public crate-consistent_hash_ring-0.3.4 (c (n "consistent_hash_ring") (v "0.3.4") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)))) (h "0cc1h9175pqvmsk2zr3169nvcbg8y79plhgjsxb9jfv97r3hr3nj")))

(define-public crate-consistent_hash_ring-0.4.0 (c (n "consistent_hash_ring") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "0jy7k6fl384hlk3nv5ms2bhhxpnrixg5azaggi37qbnnbwvssc17")))

(define-public crate-consistent_hash_ring-0.4.1 (c (n "consistent_hash_ring") (v "0.4.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "0a3rll58yycw9srcqkjb4zqmfv3y0gajhv80h27kp8xf2a963hw0")))

(define-public crate-consistent_hash_ring-0.5.0 (c (n "consistent_hash_ring") (v "0.5.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "08z6mfhhb981b7z0vrjwfsk7lhng2q0hagpz208ksqyr01hwrii6")))

(define-public crate-consistent_hash_ring-0.5.1 (c (n "consistent_hash_ring") (v "0.5.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "00rp1l7c1gs6db2h24kv5fbis90drc4anygr52xxfbjklaxyqy6j")))

(define-public crate-consistent_hash_ring-0.5.2 (c (n "consistent_hash_ring") (v "0.5.2") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "1gjd41zsy7srninihscn30y6pfw5y65p4q58666ajd2hvqndpi8x")))

(define-public crate-consistent_hash_ring-0.5.3 (c (n "consistent_hash_ring") (v "0.5.3") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "0g4kjxlz0fvpwlj9jkbjrjqbv0ya9bk7dl9mjbkjvgw1kki3l2qw")))

(define-public crate-consistent_hash_ring-0.5.4 (c (n "consistent_hash_ring") (v "0.5.4") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "1ng5hs7hrs6l58nx09whdw6354019m8261dblsnw7j4svdz30nvx")))

(define-public crate-consistent_hash_ring-0.5.5 (c (n "consistent_hash_ring") (v "0.5.5") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "0pj2i55chy2yrcma97sjf79gqazwx9kib7j18k0l6ylywv34p68f")))

(define-public crate-consistent_hash_ring-0.5.6 (c (n "consistent_hash_ring") (v "0.5.6") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "0x3409zc1r9fgn93q8rhgkm88mbj3w68c2g19sdwp2v2n3dmblfh")))

(define-public crate-consistent_hash_ring-0.5.7 (c (n "consistent_hash_ring") (v "0.5.7") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "1yr6p80p41psfvnzalllz9507yg578r8y3vbz036lya33is74src")))

(define-public crate-consistent_hash_ring-0.5.8 (c (n "consistent_hash_ring") (v "0.5.8") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "1ljzw411705ahqnxn3d4zj1nc85w7scqi9xqbysp84b4cmy465h2")))

(define-public crate-consistent_hash_ring-0.6.0 (c (n "consistent_hash_ring") (v "0.6.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "1zxz5rzbdd5iwr1qg26y6fg2ix1vdzfhap4b0q5nq5xkkrd6q24v")))

(define-public crate-consistent_hash_ring-0.6.1 (c (n "consistent_hash_ring") (v "0.6.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "1pc8fki7fyi7vi0v9hk5lpnhv5740gk418yrvmymw5zzm5mx424h")))

(define-public crate-consistent_hash_ring-0.7.0 (c (n "consistent_hash_ring") (v "0.7.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "0rqvc1a6rf72jcib8zaf06j89m3kbsvml6wrsjv440k57kjksbq9")))

(define-public crate-consistent_hash_ring-0.7.1 (c (n "consistent_hash_ring") (v "0.7.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "0s2m7vnsbm88gv4i3vjcvvjwqb7jnnv3nygazzappaszn978aydh")))

(define-public crate-consistent_hash_ring-0.7.2 (c (n "consistent_hash_ring") (v "0.7.2") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "1z8s8yki4iip083cgfpic8lh3sbca2yr9vjfn99nzqjj7mr78n7c")))

(define-public crate-consistent_hash_ring-0.8.0 (c (n "consistent_hash_ring") (v "0.8.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "1wjvngvda77njb2n6bygnk3ncglpna81f7v3mlrkgwlc4g9cqy34")))

