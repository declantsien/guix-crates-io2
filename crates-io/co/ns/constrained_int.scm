(define-module (crates-io co ns constrained_int) #:use-module (crates-io))

(define-public crate-constrained_int-0.1.0 (c (n "constrained_int") (v "0.1.0") (d (list (d (n "const_guards") (r "^0.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "1cdkjn42ga6rkpd4cy17gcrw69ml5r8n42yrdjyj8l1ad2xy7703") (y #t) (s 2) (e (quote (("std" "dep:thiserror"))))))

(define-public crate-constrained_int-0.1.1 (c (n "constrained_int") (v "0.1.1") (d (list (d (n "const_guards") (r "^0.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "03k4q8snjvwy513jxzr60qbibsp6xhdcc297cav1hxrmpp3f1ddm") (y #t) (s 2) (e (quote (("std" "dep:thiserror"))))))

(define-public crate-constrained_int-0.1.2 (c (n "constrained_int") (v "0.1.2") (d (list (d (n "const_guards") (r "^0.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "06a1sbw4w4gmmfjhhq025r1ncpa93qwz873niz7alzbl40gh90i9") (f (quote (("std")))) (y #t)))

(define-public crate-constrained_int-0.2.0 (c (n "constrained_int") (v "0.2.0") (d (list (d (n "const_guards") (r "^0.1.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0l9v9xswcs17bli58ciibwx4g1dr4qirrj0843vx98gfvf4w6q0d") (f (quote (("std"))))))

(define-public crate-constrained_int-0.2.1 (c (n "constrained_int") (v "0.2.1") (d (list (d (n "const_guards") (r "^0.1.3") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "04y2bcyky6al04lnj814y2g0chr36wrl911va756v7i3xszh0f3y") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-constrained_int-0.2.2 (c (n "constrained_int") (v "0.2.2") (d (list (d (n "const_guards") (r "^0.1.3") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "07l6l716fk2jqk2m01lmd6ab487zdjd8zcqpp8pp3gy8f4mpqh6h") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-constrained_int-0.2.3 (c (n "constrained_int") (v "0.2.3") (d (list (d (n "const_guards") (r "^0.1.3") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0bjvfrcjk4w7nqfx4nddrfhp48h6x33wm6x2r4mg17vpljjdrzgi") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

