(define-module (crates-io co ns constant_sandbox) #:use-module (crates-io))

(define-public crate-constant_sandbox-0.1.0 (c (n "constant_sandbox") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lib-ruby-parser") (r "^3.0.0-4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0kl6v95liirhxbwpiip47w3f2bxr70dl8dshg99y813nvg15r424")))

