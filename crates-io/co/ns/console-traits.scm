(define-module (crates-io co ns console-traits) #:use-module (crates-io))

(define-public crate-console-traits-0.1.0 (c (n "console-traits") (v "0.1.0") (h "10m9qhs3x5xrf8pv6pmphq4ijifvh3zygiiqzyfkq2aj396mqa4g")))

(define-public crate-console-traits-0.2.0 (c (n "console-traits") (v "0.2.0") (h "10y689y2mqw8brs4kvi80is5hbqbs4jvzhvdsk8fark9srcwzfdw")))

(define-public crate-console-traits-0.3.0 (c (n "console-traits") (v "0.3.0") (h "1infl9rn7saj0azqdb6g1y05qpsx7q7ir418ciysxxy3sp8v64gp")))

