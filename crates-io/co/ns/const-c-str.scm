(define-module (crates-io co ns const-c-str) #:use-module (crates-io))

(define-public crate-const-c-str-0.1.0 (c (n "const-c-str") (v "0.1.0") (d (list (d (n "const-c-str-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1g58hh11bvlp735y5sifnzd2pjdn375y2cvq1apbf7frg2sai8k0")))

(define-public crate-const-c-str-0.1.1 (c (n "const-c-str") (v "0.1.1") (d (list (d (n "const-c-str-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "07l71dhapswz6a93jw9lh9a5p7b9kffn502pshgc88y4xjwz31rk")))

