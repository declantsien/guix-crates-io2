(define-module (crates-io co ns consio) #:use-module (crates-io))

(define-public crate-consio-0.1.0 (c (n "consio") (v "0.1.0") (h "0vpav99gh53gg0450ipza0p4pmdi9p1x1vq1x9798ni74331dx2i") (y #t)))

(define-public crate-consio-0.1.1 (c (n "consio") (v "0.1.1") (h "1j9hx12xq5d5gr69ssw6pdc2nn33arw34wkjwh7rcd4w82xkgakf") (y #t)))

(define-public crate-consio-0.1.2 (c (n "consio") (v "0.1.2") (h "0wxivh8ga3b6bgxa0g80sbxzf88n67d2j89n57qa77dan86nvals")))

