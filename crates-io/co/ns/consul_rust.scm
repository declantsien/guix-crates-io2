(define-module (crates-io co ns consul_rust) #:use-module (crates-io))

(define-public crate-consul_rust-0.1.0 (c (n "consul_rust") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "06cnpf64xs35rh6lizym9jfv5m1v3xjvn2phspwb6ks09f5qrw40")))

