(define-module (crates-io co ns consume) #:use-module (crates-io))

(define-public crate-consume-1.0.0 (c (n "consume") (v "1.0.0") (h "1wsai5lcwh00n7v30bkh79xdryj4pj1gqv2cxwd915rk6j1lmk2l")))

(define-public crate-consume-1.0.1 (c (n "consume") (v "1.0.1") (h "0k5dh4hykrnfwwiw27aizqf1wskmpv2274cy3sjjvk1mvxbi4v4v")))

