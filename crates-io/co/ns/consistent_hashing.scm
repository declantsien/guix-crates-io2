(define-module (crates-io co ns consistent_hashing) #:use-module (crates-io))

(define-public crate-consistent_hashing-0.1.0 (c (n "consistent_hashing") (v "0.1.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0bzjlsl7v7dff93r4d3wkz773bkbbs4pz88lwnk5nx7g3aknz007")))

(define-public crate-consistent_hashing-0.2.0 (c (n "consistent_hashing") (v "0.2.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0rik0cfqpdbhhxxccy2qkj343zxrbi1jyb8yp0hb5cbmlrdaxv20")))

(define-public crate-consistent_hashing-0.3.0 (c (n "consistent_hashing") (v "0.3.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1diimw6inysvsvjg64wzqwkzwpp5h0s6iy6nrs31a6cqykfd8f79")))

(define-public crate-consistent_hashing-0.3.1 (c (n "consistent_hashing") (v "0.3.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ha1ah0mad7kp9hfnhaw7h7m6rllwhdlislnhd81f1hzsblay9yl")))

(define-public crate-consistent_hashing-0.4.0 (c (n "consistent_hashing") (v "0.4.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0iv3cpgb71mlaqkyzk3bnc98vid9vnivlp6ysgdsxfwi3j88n29b")))

(define-public crate-consistent_hashing-0.4.1 (c (n "consistent_hashing") (v "0.4.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1jnbcfxz96jzznfp29g8x59vfx4q2f21517xwwiq5aa41dy76mmw")))

(define-public crate-consistent_hashing-0.5.0 (c (n "consistent_hashing") (v "0.5.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1b0vkwkk13il530kg6p0phs03p6yxdd08fy6350f7w7pxak207pa")))

