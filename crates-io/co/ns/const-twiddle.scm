(define-module (crates-io co ns const-twiddle) #:use-module (crates-io))

(define-public crate-const-twiddle-0.0.1 (c (n "const-twiddle") (v "0.0.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0137n0mg3gvx8a8w8d17jz4zrawnsffnvly92f5hrx5zhi5p6dpd") (y #t)))

(define-public crate-const-twiddle-0.0.2 (c (n "const-twiddle") (v "0.0.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1a3snm1wxmjpjan5cc4bl18vgagybg87lwsy1cvv3hq8kr286kqq") (y #t)))

(define-public crate-const-twiddle-0.0.3 (c (n "const-twiddle") (v "0.0.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0hy3y3brq30j3ngrdm7iv5c6f2ilsk4dp27mvzj4axabk18336p0") (y #t)))

(define-public crate-const-twiddle-0.0.4 (c (n "const-twiddle") (v "0.0.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0giqfcrdrd0ijsql1rpb8xjbfqz4sr6ndy26k9dyv0l301a6pyfz") (y #t)))

(define-public crate-const-twiddle-0.0.5 (c (n "const-twiddle") (v "0.0.5") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0padc8i6q49hrlb23i2ssjb9d6m48y8lixpmmmx04q8lk5qpisx0") (y #t)))

(define-public crate-const-twiddle-0.1.0 (c (n "const-twiddle") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0391vrl4wkvlkh63b4a2j0raqfsnza5lyravzia7vx7h7sd7f002") (y #t)))

(define-public crate-const-twiddle-0.1.1 (c (n "const-twiddle") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1rxk51hjy838rn22qspfvzr2v6q8wahal4k2zfnjavfi7m83n87p") (y #t)))

