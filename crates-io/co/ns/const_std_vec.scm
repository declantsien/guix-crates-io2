(define-module (crates-io co ns const_std_vec) #:use-module (crates-io))

(define-public crate-const_std_vec-0.1.0 (c (n "const_std_vec") (v "0.1.0") (h "01kk14lfa0svmn9sw4h0n0z26nvb4h8jkkxlqkak38mi54pk8irn")))

(define-public crate-const_std_vec-0.2.0 (c (n "const_std_vec") (v "0.2.0") (h "0wpi6gfnkan9xif7wqpj9cb79asvryq1s6wrs448cyll3dqa9rlf")))

