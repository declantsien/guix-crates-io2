(define-module (crates-io co ns constexpr-macros) #:use-module (crates-io))

(define-public crate-constexpr-macros-0.0.1-alpha (c (n "constexpr-macros") (v "0.0.1-alpha") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)))) (h "1nnsq4nsj2zc9zavw0cbv1wcvzi3kfihmzxrbfzy2iksdgga97pg")))

(define-public crate-constexpr-macros-0.0.1-alpha1 (c (n "constexpr-macros") (v "0.0.1-alpha1") (d (list (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)))) (h "0rbppi184q85vri6z4n9jny8nijss70im5xx69y580551ddjv2qg")))

