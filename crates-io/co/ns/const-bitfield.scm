(define-module (crates-io co ns const-bitfield) #:use-module (crates-io))

(define-public crate-const-bitfield-0.1.0 (c (n "const-bitfield") (v "0.1.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "08whmfsgbjdpcmw72xp8kq53v9fn9i9af63sr4dcdagspyb36v0y")))

(define-public crate-const-bitfield-0.2.0 (c (n "const-bitfield") (v "0.2.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0bdaay67504qq0prwy9jnwak9rn794rdrsv3lhgmk194y09ci708")))

(define-public crate-const-bitfield-0.2.1 (c (n "const-bitfield") (v "0.2.1") (d (list (d (n "const-enum") (r "^0.1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "087s53k4nmh7vrr011zrybv27ayw22cy7pvsg95jcdksc2h9jc7n")))

(define-public crate-const-bitfield-0.2.2 (c (n "const-bitfield") (v "0.2.2") (d (list (d (n "const-enum") (r "^0.1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ax4riia9gr4h8s6zmgkamx5z3pgjrs4skiyddxhy96qbmq8k6xb")))

