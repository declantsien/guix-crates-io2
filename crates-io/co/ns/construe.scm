(define-module (crates-io co ns construe) #:use-module (crates-io))

(define-public crate-construe-0.0.1 (c (n "construe") (v "0.0.1") (h "0vfippxc254jsy92bads0cmr5qhdpr49d8m4z65dvvs15fj6wyvd")))

(define-public crate-construe-0.0.2 (c (n "construe") (v "0.0.2") (h "0vl79vlbv0y6xp8lpdxshshws2nawjjg4zam05yhbraj8zm6w0ih")))

(define-public crate-construe-0.0.3 (c (n "construe") (v "0.0.3") (h "0y1jv3cd7i6j78ihzg4qrf19s6n0faawlw0lkmqfhybgiiqz8a2w")))

