(define-module (crates-io co ns conservator) #:use-module (crates-io))

(define-public crate-conservator-0.1.0 (c (n "conservator") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "conservator_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono" "bigdecimal"))) (d #t) (k 0)))) (h "1zldncs8mzhlyff548s8r5vxn1vh418lcwjjfv3r5arwvbdsivgg")))

(define-public crate-conservator-0.1.1 (c (n "conservator") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "conservator_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono" "bigdecimal"))) (d #t) (k 0)))) (h "0mg31c93vy9vi5l15qld1lg14y8vkwbklm0fqpj2gss952jjy7md")))

(define-public crate-conservator-0.1.2 (c (n "conservator") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "conservator_macro") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono" "bigdecimal"))) (d #t) (k 0)))) (h "00690yyg8vr5641j6r7hmwib5xbfdq65jh24lq96zr8syn60fa1j")))

(define-public crate-conservator-0.1.3 (c (n "conservator") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "conservator_macro") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono" "bigdecimal"))) (d #t) (k 0)))) (h "0p3qayy0dzw8j8p1xbbgjj1if74cmarlvz5k1njb9qbwghds6kcp")))

(define-public crate-conservator-0.1.4 (c (n "conservator") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "conservator_macro") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono" "bigdecimal"))) (d #t) (k 0)))) (h "02c6hsd6yxaiqf0lp1hfsbnn2aid26nj4hd8y4rcnxhj65mrygd9")))

(define-public crate-conservator-0.1.5 (c (n "conservator") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "conservator_macro") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono" "bigdecimal"))) (d #t) (k 0)))) (h "1hb5xc8ij6bf6wl3j7pw71p2sd2jgg9cq1qfj0c958sphwm0rn83")))

(define-public crate-conservator-0.1.6 (c (n "conservator") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "conservator_macro") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono" "bigdecimal"))) (d #t) (k 0)))) (h "1vjmhp9z4wran4ajn49651gpndh34czzpydni932ik7z79ic6yrc")))

(define-public crate-conservator-0.1.7 (c (n "conservator") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "conservator_macro") (r "^0.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "postgres" "chrono" "bigdecimal"))) (d #t) (k 0)))) (h "1xwdyh2zf1phvg29kgcxx76r5isy7xz60sw2vcmjwmc409pqv693")))

