(define-module (crates-io co ns const-arith-macros-178) #:use-module (crates-io))

(define-public crate-const-arith-macros-178-0.1.0 (c (n "const-arith-macros-178") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zs739kaiq46vs2zcq4fahdy3qm40h8bjyzqg5n7h2m80f063mh7")))

(define-public crate-const-arith-macros-178-0.1.1 (c (n "const-arith-macros-178") (v "0.1.1") (d (list (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16w6fplgxrh36snrx1sanpazn9kk79gdgfa8vpghqhf01zlj2zwp")))

(define-public crate-const-arith-macros-178-0.1.2 (c (n "const-arith-macros-178") (v "0.1.2") (d (list (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15bgasi1y59py8g31xslrhd6hivynixrsv7hlixwc8jhjpbz8lzs")))

