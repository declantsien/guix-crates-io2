(define-module (crates-io co ns const-field-offset) #:use-module (crates-io))

(define-public crate-const-field-offset-0.1.0 (c (n "const-field-offset") (v "0.1.0") (d (list (d (n "const-field-offset-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "memoffset") (r "^0.5.4") (d #t) (k 2)))) (h "07sg63djql9lxxqkg59h2sh62k169nvin9z5mils1wbkalrpdllv") (f (quote (("field-offset-trait" "const-field-offset-macro/field-offset-trait"))))))

(define-public crate-const-field-offset-0.1.1 (c (n "const-field-offset") (v "0.1.1") (d (list (d (n "const-field-offset-macro") (r "=0.1.1") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)))) (h "1yvi0mw64r0n531gg7w8fv5l4akf4bmplhwva9s7y79jv0dbs9xs") (f (quote (("field-offset-trait" "const-field-offset-macro/field-offset-trait"))))))

(define-public crate-const-field-offset-0.1.2 (c (n "const-field-offset") (v "0.1.2") (d (list (d (n "const-field-offset-macro") (r "=0.1.2") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)))) (h "1l7vc5l7k9qnwb9y18kcays5cg1pda5lryr3wkb0j0cf5ky40vad") (f (quote (("field-offset-trait" "const-field-offset-macro/field-offset-trait"))))))

(define-public crate-const-field-offset-0.1.3 (c (n "const-field-offset") (v "0.1.3") (d (list (d (n "const-field-offset-macro") (r "=0.1.3") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.8.0") (d #t) (k 2)))) (h "1zcdxzs2ksxyv1xhagxmlv9ysximz4ypqwrbazfcsqzl2rglc133") (f (quote (("field-offset-trait" "const-field-offset-macro/field-offset-trait"))))))

(define-public crate-const-field-offset-0.1.4 (c (n "const-field-offset") (v "0.1.4") (d (list (d (n "const-field-offset-macro") (r "=0.1.4") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.9.0") (d #t) (k 2)))) (h "05qs5rjr0vqc9jxfgavriqa57rwcm5rp5s35a9f4gzmavv9ryagf") (f (quote (("field-offset-trait" "const-field-offset-macro/field-offset-trait"))))))

(define-public crate-const-field-offset-0.1.5 (c (n "const-field-offset") (v "0.1.5") (d (list (d (n "const-field-offset-macro") (r "=0.1.5") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.2") (d #t) (k 0)) (d (n "memoffset") (r "^0.9.0") (d #t) (k 2)))) (h "0m82mwfw9i8wdvv166a4kaqqd7p15r3kq23knna5l6r1l56dxz4i") (f (quote (("field-offset-trait" "const-field-offset-macro/field-offset-trait"))))))

