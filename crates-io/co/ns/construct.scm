(define-module (crates-io co ns construct) #:use-module (crates-io))

(define-public crate-construct-0.0.1 (c (n "construct") (v "0.0.1") (h "1agra6h7804yc8nvjfv2i2rdka0yid1l1xw3d2jdgjrlckrrc77y")))

(define-public crate-construct-0.0.2 (c (n "construct") (v "0.0.2") (h "0gh4ssyivqhbhy61rc9p1nj7r91rdcpaq8idap328fzwslsqdjp2")))

(define-public crate-construct-0.0.3 (c (n "construct") (v "0.0.3") (h "07whv762b91y8kr1kmfmygdbpzri61dlbf28722z7qlcih9g7bil")))

