(define-module (crates-io co ns const-c-str-impl) #:use-module (crates-io))

(define-public crate-const-c-str-impl-0.1.0 (c (n "const-c-str-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0ydlzgiw12lrinx5lq20d1nbwns9wvzc6p41z2fybaj9ngi9y9s5")))

