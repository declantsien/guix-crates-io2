(define-module (crates-io co ns consistency) #:use-module (crates-io))

(define-public crate-consistency-1.0.0-pre.0 (c (n "consistency") (v "1.0.0-pre.0") (d (list (d (n "clippy") (r "^0.0.70") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0c4fnl2yk24kf59gg9qy97kcgkd3w10wgs0sdhmnvwzixqvw5rmq")))

