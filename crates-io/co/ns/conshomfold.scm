(define-module (crates-io co ns conshomfold) #:use-module (crates-io))

(define-public crate-conshomfold-0.1.0 (c (n "conshomfold") (v "0.1.0") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1if7sd5jpi9rgldiapas51qxl2dipdy2wdcmmw30ad7i1fhmmq9w")))

(define-public crate-conshomfold-0.1.1 (c (n "conshomfold") (v "0.1.1") (d (list (d (n "bio") (r "^0.20") (d #t) (k 0)) (d (n "consprob") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1gjmg3akgp9xqn17g2gm138367mfp3w7f55nlnr3iiljrwk04iik")))

