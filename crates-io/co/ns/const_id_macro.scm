(define-module (crates-io co ns const_id_macro) #:use-module (crates-io))

(define-public crate-const_id_macro-0.1.0 (c (n "const_id_macro") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "0x5i5m6sbrh50csdxz9rpns1dscdsk1x0azg09yq3hx95mf5rpxj") (y #t)))

