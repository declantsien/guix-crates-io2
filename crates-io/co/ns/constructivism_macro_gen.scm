(define-module (crates-io co ns constructivism_macro_gen) #:use-module (crates-io))

(define-public crate-constructivism_macro_gen-0.1.0 (c (n "constructivism_macro_gen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "03ga4njfwigx04v6igxnbvxk3xsn8b8cr6swyblxi8yzcb6j8cyh")))

(define-public crate-constructivism_macro_gen-0.3.0 (c (n "constructivism_macro_gen") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "08mympmwn0qf23yqhzhi3v8v4hqmlpsmirjkp1mssj9c65nbz6bd")))

