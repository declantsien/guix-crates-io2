(define-module (crates-io co ns constdefault) #:use-module (crates-io))

(define-public crate-constdefault-1.0.1 (c (n "constdefault") (v "1.0.1") (d (list (d (n "constdefault-derive") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0w4k5wsii1pizfi0dxwrgzcl8bnjjrh0hpw242zsvw80jkqglczv") (f (quote (("unstable-docs") ("unstable") ("std" "alloc") ("enable-atomics") ("derive" "constdefault-derive") ("default" "enable-atomics") ("alloc"))))))

