(define-module (crates-io co ns const_env--value) #:use-module (crates-io))

(define-public crate-const_env--value-0.1.2 (c (n "const_env--value") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.5") (f (quote ("stable"))) (d #t) (k 2)) (d (n "const_env_impl--value") (r "=0.1.2") (d #t) (k 0)))) (h "0ia62jvhpw5lpq9xc7gh9by9vd1ghiqsxf7yyddhl1cma0x51phs")))

