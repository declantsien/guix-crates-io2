(define-module (crates-io co ns consistent-rs) #:use-module (crates-io))

(define-public crate-consistent-rs-0.1.0 (c (n "consistent-rs") (v "0.1.0") (d (list (d (n "crc") (r "^1.4.0") (d #t) (k 0)))) (h "10gfc3f4d5prh5mp2d7yraziwflqmspy1kr3i1k3cag3l8izl27c")))

(define-public crate-consistent-rs-0.1.1 (c (n "consistent-rs") (v "0.1.1") (d (list (d (n "crc") (r "^1.4.0") (d #t) (k 0)))) (h "074lqh15fi5gds3vy7nklhyk3q2xqm8b06r5zi3sj7csc99ya1l3")))

