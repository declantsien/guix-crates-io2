(define-module (crates-io co ns constgebra) #:use-module (crates-io))

(define-public crate-constgebra-0.1.0 (c (n "constgebra") (v "0.1.0") (d (list (d (n "const_soft_float") (r "^0.1.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0li3rrkcl13jdzzmfzd1i4xmpsfb8syhp2wj0fzffnh81cwk373g")))

(define-public crate-constgebra-0.1.1 (c (n "constgebra") (v "0.1.1") (d (list (d (n "const_soft_float") (r "^0.1.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0n9a40zgap52gg82pffswhv62zx07n6kw1j0krp0wsb72sw54cif")))

(define-public crate-constgebra-0.1.2 (c (n "constgebra") (v "0.1.2") (d (list (d (n "const_soft_float") (r "^0.1.2") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "132iz4x9qhd6mkr5jkyfl2vhna7yawkyzb14msjhc44k99ch5va5")))

(define-public crate-constgebra-0.1.3 (c (n "constgebra") (v "0.1.3") (d (list (d (n "const_soft_float") (r "^0.1.2") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0844a7qg6kx04hsl035lfa3drk8w9z77ib213vydmrjh8n33xlpd")))

(define-public crate-constgebra-0.1.4 (c (n "constgebra") (v "0.1.4") (d (list (d (n "const_soft_float") (r "^0.1.2") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1g2gfszm0jlwmg7xfzch1fb5qxisi741023cmii8d9j9b2vgkap1")))

