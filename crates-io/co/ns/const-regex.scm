(define-module (crates-io co ns const-regex) #:use-module (crates-io))

(define-public crate-const-regex-0.1.0 (c (n "const-regex") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex-automata") (r "^0.1.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("full"))) (d #t) (k 0)))) (h "0adiyaanfik44g405qwb5yqb3sv29fjaas0yfc94wsr9nb75cry4")))

