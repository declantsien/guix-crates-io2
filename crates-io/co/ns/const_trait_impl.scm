(define-module (crates-io co ns const_trait_impl) #:use-module (crates-io))

(define-public crate-const_trait_impl-0.1.0 (c (n "const_trait_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1jczwaklvk6ndvysc6rg5y2sz5w0d7v6k27sj2an0qs83wp8rsim") (r "1.56.1")))

(define-public crate-const_trait_impl-0.1.1 (c (n "const_trait_impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "02ckj26zmdy9hqsass4a2ck928pwrp43ahi154009a74ch6z7nh0") (r "1.56.1")))

(define-public crate-const_trait_impl-0.1.2 (c (n "const_trait_impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0k8h3lh506rvh535ngmgdc8hvgivxgw3cr2j6wsi3ihbzb0m8qbj") (r "1.56.1")))

(define-public crate-const_trait_impl-0.1.3 (c (n "const_trait_impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0fpxlxwar1iajhsx3g3kqvq449dmgr0lcqagz3incaszvgvj2j7l") (r "1.56.1")))

