(define-module (crates-io co ns const_closure) #:use-module (crates-io))

(define-public crate-const_closure-0.1.0 (c (n "const_closure") (v "0.1.0") (h "067v7gg6nvl66s11rvvifjwr1w5rvlg8nzrkvqxcgw2bs9ccfwx7")))

(define-public crate-const_closure-0.1.1 (c (n "const_closure") (v "0.1.1") (h "1rq8y6ki33wckkx52whp2l2jk4iski4bqzc1yrqmsbkbrx53gq5v")))

(define-public crate-const_closure-0.2.0 (c (n "const_closure") (v "0.2.0") (h "03qnypj7y3biifanvss4r56amn7x4k30p58ykkv9w3mm583zxjix")))

(define-public crate-const_closure-0.2.1 (c (n "const_closure") (v "0.2.1") (h "1jc7dlx3nz1nkn68wbpb47xfshcfgkh1az96d23ijfz6mb2n16kj")))

(define-public crate-const_closure-0.2.2 (c (n "const_closure") (v "0.2.2") (h "1raybyc6kj3xh6yl7ad4d3pdn8hygjhxl49q7qhirybwpjcf3zyw")))

(define-public crate-const_closure-0.2.3 (c (n "const_closure") (v "0.2.3") (h "1v4378k3ic6l9v68wyyp1rbgknjzr9f2434ampx7r2qprsczv0vk")))

(define-public crate-const_closure-0.3.0 (c (n "const_closure") (v "0.3.0") (h "14g5fdznlll7m6qqbwq8yybgahph5cgcpv3zadynj7j3998ddkg1")))

