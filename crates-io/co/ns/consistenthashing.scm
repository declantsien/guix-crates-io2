(define-module (crates-io co ns consistenthashing) #:use-module (crates-io))

(define-public crate-ConsistentHashing-0.1.0 (c (n "ConsistentHashing") (v "0.1.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "092h1wnvc0s5rnivm9nji71464lam3rcx3n3z38zjwzjida805pm") (y #t)))

