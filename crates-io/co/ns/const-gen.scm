(define-module (crates-io co ns const-gen) #:use-module (crates-io))

(define-public crate-const-gen-0.1.0 (c (n "const-gen") (v "0.1.0") (d (list (d (n "const-gen-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "10lgvrrn3xajjdlp8mkjmb6wqhvfwwrd7f9022rrf0xmqrxyf392") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.1.1 (c (n "const-gen") (v "0.1.1") (d (list (d (n "const-gen-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0il7i6kgpgwxh11h1ffw4ivm1i6qi27bqh1hsgqlmh756daiwgds") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.1.2 (c (n "const-gen") (v "0.1.2") (d (list (d (n "const-gen-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1fabkc8wmdv22022qspwx2ml7kgbwkxh51d9y7m2smy23h1csfq5") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.1.3 (c (n "const-gen") (v "0.1.3") (d (list (d (n "const-gen-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ikqbzd0diyqhpwvfvx8ahi6ahmw53hph4wb6yx8hjffa3zx0r2a") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.1.4 (c (n "const-gen") (v "0.1.4") (d (list (d (n "const-gen-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1w7w03v68jn4wp8mbgiq51pc400vrjfxd2gk3ffdayz7wy1lwmwx") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.0 (c (n "const-gen") (v "0.2.0") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (o #t) (d #t) (k 0)))) (h "0amxqjyrsyf9g8063ahvcv810ccf9ca1v845ykkck9xmc9z67fx2") (f (quote (("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.1 (c (n "const-gen") (v "0.2.1") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0mm571wnlf9d74i6xwa3hmxm41p0q9m88sdcs9rrrabyvgasn9r0") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.2 (c (n "const-gen") (v "0.2.2") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1xxpwxczk69nhdf1bfq5ijsyidvk3hhq61g21bi4jybw88av4xpg") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.3 (c (n "const-gen") (v "0.2.3") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1wzzlfi0k8j9ad0lj07y5aixnvdsvwl33350grzsqxjpwac14xmi") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.4 (c (n "const-gen") (v "0.2.4") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1v6vpblhz2y5vgc2hsy6f8dkiv5i8ap0jg8kz0izyrbmih5zaj32") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.5 (c (n "const-gen") (v "0.2.5") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "08ycx7dv70vqrxkgi2hxxl8vzcw3ijbb20mdg2pgvp5119559g6j") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.6 (c (n "const-gen") (v "0.2.6") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "049jnnvklnzbki3aibgg5v21c32nbj97az2q1pxaydivk00n6vch") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.7 (c (n "const-gen") (v "0.2.7") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "05gp46mizvv9gla9l8grw1jngk4hpjc3zma25660f2cbnka9wl3n") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.8 (c (n "const-gen") (v "0.2.8") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0p7xhfjvg93zcn9xk69npdr5lissgfmqlvqc12kl7fvndw4nfazs") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.9 (c (n "const-gen") (v "0.2.9") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1k1hh0gd8rcl655a8jwhbl5yp6gip0rf25bv0lsv7wlnhi8vww0l") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.10 (c (n "const-gen") (v "0.2.10") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0152jb6hqmq52ag883as1kxifwvfgm605r6nd4afrlggalymzywi") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.2.11 (c (n "const-gen") (v "0.2.11") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1c7zxvp1zwwjikmfq5w2d0zzc1xayqhqbq5pjg2wcmavgna7zwzs") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.3.0 (c (n "const-gen") (v "0.3.0") (d (list (d (n "const-gen-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1n74n1ippwbm5alsqh8hxk6srqniir5rdpq09wjpasx1vli7kdiy") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.3.1 (c (n "const-gen") (v "0.3.1") (d (list (d (n "const-gen-derive") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "02pxk5wi1i44x8alk9b0ilxs7k874ynqzm2ar99wd9caszlablqi") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.4.0 (c (n "const-gen") (v "0.4.0") (d (list (d (n "const-gen-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "15wpr4f10p0pral9xd3b8kdqws0pyjdp7vdfmnk4dz7brxgac7lk") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.4.1 (c (n "const-gen") (v "0.4.1") (d (list (d (n "const-gen-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0sdpr4khyjnw6d7pr4k7kszqn3n8707l567k9v0nhfj6zp0s89hs") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.4.2 (c (n "const-gen") (v "0.4.2") (d (list (d (n "const-gen-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0wif65rrcfbww4q2nbi8y8zrkdxzqv57gdfdm626s10l13igmb8p") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.4.3 (c (n "const-gen") (v "0.4.3") (d (list (d (n "const-gen-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0ykw1dg4j5wlwmypwfyfgsl4m5sl6si0b2bwks7q2c7gw60n2kfa") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.4.4 (c (n "const-gen") (v "0.4.4") (d (list (d (n "const-gen-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "12jyr0fm6p81aj5x4nz0jsw6dqyysj009qav5q2n0h3md1vprz7i") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.4.5 (c (n "const-gen") (v "0.4.5") (d (list (d (n "const-gen-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0gdwmv66wmdqzh9lzm52m3jd4lxamsxkiq8xg22i1iy9k5da0w4z") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.4.6 (c (n "const-gen") (v "0.4.6") (d (list (d (n "const-gen-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0gibn1vmn75rn4806vy8z462isncq1y6fx9y0qq71qmaa2shzm3p") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-0.4.7 (c (n "const-gen") (v "0.4.7") (d (list (d (n "const-gen-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1fa4v4gv95qlp26whc7m2cgv9c8c6jj5spp1is80f04m6clji4p2") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-1.0.0 (c (n "const-gen") (v "1.0.0") (d (list (d (n "const-gen-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1cpzs7w1a9d0dj4vzrry1kc5z6l1wynmypxyv97jzkcs2i0bnr1v") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-1.1.0 (c (n "const-gen") (v "1.1.0") (d (list (d (n "const-gen-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1dmavccvrld9hwz728yg7z3r8h6pysh1b60dxfxb0zs2hf9flhdh") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-1.2.0 (c (n "const-gen") (v "1.2.0") (d (list (d (n "const-gen-derive") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "17qq0r41spz3nfkrvw35d2zbvdqwh1ccjvsbzq0vyi7qbxrwijgg") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-1.3.0 (c (n "const-gen") (v "1.3.0") (d (list (d (n "const-gen-derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1gyx133zxayqkcp2vi605a17pydfc7p3q9lj6xfw66zahlrl0vap") (f (quote (("phf") ("derive" "const-gen-derive") ("default" "derive" "phf"))))))

(define-public crate-const-gen-1.4.0 (c (n "const-gen") (v "1.4.0") (d (list (d (n "const-gen-derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "153j80zjvlljssmysza46jl06q54nhxzyn80js5ci7vjxh1l6l81") (f (quote (("std") ("phf" "std") ("derive" "const-gen-derive") ("default" "std" "derive" "phf"))))))

(define-public crate-const-gen-1.4.1 (c (n "const-gen") (v "1.4.1") (d (list (d (n "const-gen-derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1kx44rxpmsrmw3smk7kxzzk16jvhpjd0yj7xwk6vxb9c86xarbrc") (f (quote (("std") ("phf" "std") ("derive" "const-gen-derive") ("default" "std" "derive" "phf"))))))

(define-public crate-const-gen-1.5.0 (c (n "const-gen") (v "1.5.0") (d (list (d (n "const-gen-derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "146m0368nyjk29vr2563cr2x4k04bfsfmx524afklqajgnr1sxcr") (f (quote (("std") ("phf" "std") ("derive" "const-gen-derive") ("default" "std" "derive" "phf"))))))

(define-public crate-const-gen-1.5.1 (c (n "const-gen") (v "1.5.1") (d (list (d (n "const-gen-derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1k2albrf00n0hblyz51n1qa3jgwglynpknax89riqwsvm4k6b62y") (f (quote (("std") ("phf" "std") ("derive" "const-gen-derive") ("default" "std" "derive" "phf"))))))

(define-public crate-const-gen-1.6.0 (c (n "const-gen") (v "1.6.0") (d (list (d (n "const-gen-derive") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "0z1ml7i0dlpfcba731va6p7dzf52qbl52qg7jpzdbps2zhhrqr0f") (f (quote (("std") ("phf" "std") ("derive" "const-gen-derive") ("default" "std" "derive" "phf"))))))

(define-public crate-const-gen-1.6.1 (c (n "const-gen") (v "1.6.1") (d (list (d (n "const-gen-derive") (r "^1.1.4") (o #t) (d #t) (k 0)))) (h "0hi65sm4s6wxyx0xislz87ifaj7hz85swficaksza0rqxi2g227n") (f (quote (("std") ("phf" "std") ("derive" "const-gen-derive") ("default" "std" "derive" "phf"))))))

(define-public crate-const-gen-1.6.2 (c (n "const-gen") (v "1.6.2") (d (list (d (n "const-gen-derive") (r "^1.1.4") (o #t) (d #t) (k 0)))) (h "10c0471q32f8p4cx2p7bmmlbia3jl144hnxg8klq8ggqk75h4x7c") (f (quote (("std") ("phf" "std") ("derive" "const-gen-derive") ("default" "std" "derive" "phf"))))))

(define-public crate-const-gen-1.6.3 (c (n "const-gen") (v "1.6.3") (d (list (d (n "const-gen-derive") (r "^1.1.5") (o #t) (d #t) (k 0)))) (h "0vl389360y0q0p8hld934b46c3djv0wv3rwx2i4zz68m8p4d61fc") (f (quote (("std") ("phf" "std") ("derive" "const-gen-derive") ("default" "std" "derive" "phf")))) (y #t)))

(define-public crate-const-gen-1.6.4 (c (n "const-gen") (v "1.6.4") (d (list (d (n "const-gen-derive") (r "^1.1.5") (o #t) (d #t) (k 0)))) (h "11qfld6s9ln3np4fsljimc7qsm8v4w3ywwjyk4sn66a6impl9z7v") (f (quote (("std") ("phf" "std") ("derive" "const-gen-derive") ("default" "std" "derive" "phf"))))))

