(define-module (crates-io co ns const-type-layout-derive) #:use-module (crates-io))

(define-public crate-const-type-layout-derive-0.1.0 (c (n "const-type-layout-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pa0jpslh6mk4zdnnw7gz0ww7jw2jk9r4vlcnzkr9gvb84s7k63s") (r "1.60")))

(define-public crate-const-type-layout-derive-0.2.0 (c (n "const-type-layout-derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kjdqzgcp3m4cmp86v10v5ppvf6x9h7vx6lafnwsglnq7gr0dxfc") (r "1.75")))

(define-public crate-const-type-layout-derive-0.3.0 (c (n "const-type-layout-derive") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "147wb5rjqczc04cc7bd1asmgl29c23glfk546ypmzw7hdm4rygm5") (r "1.78")))

