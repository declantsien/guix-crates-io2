(define-module (crates-io co ns console_ui_engine_null) #:use-module (crates-io))

(define-public crate-console_ui_engine_null-0.1.0 (c (n "console_ui_engine_null") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.13.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17k10z71p5303j07ha274n94cb05hjc2rsg144kw2bwyxi29mnpp")))

