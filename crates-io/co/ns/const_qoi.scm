(define-module (crates-io co ns const_qoi) #:use-module (crates-io))

(define-public crate-const_qoi-1.0.0 (c (n "const_qoi") (v "1.0.0") (h "142y7cz4aqyvx2y0lygb78fb8vby75n45hikmzp00czig04jbddr") (r "1.78")))

(define-public crate-const_qoi-1.0.1 (c (n "const_qoi") (v "1.0.1") (h "0qxj9awx185agyh5ih1z4pxbydyv7rcclrn05w6wnkg1v5jvqwld") (r "1.78")))

