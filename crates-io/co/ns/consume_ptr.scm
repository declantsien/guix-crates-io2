(define-module (crates-io co ns consume_ptr) #:use-module (crates-io))

(define-public crate-consume_ptr-0.1.0 (c (n "consume_ptr") (v "0.1.0") (h "0x7gzwpans4nj9l4nl1wf6rk1nnbg4pack14k6a3p7hw4bg7z6yx")))

(define-public crate-consume_ptr-0.2.0 (c (n "consume_ptr") (v "0.2.0") (h "0prxxi54g62rap262xq8lmfyh72nj8qzf5iw11csw6y9i85wymiv")))

(define-public crate-consume_ptr-0.2.1 (c (n "consume_ptr") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "0n4ygzjjqjyzkqq6zzivrpx9af11ssyq09lxgkxd3r622k3cpzhw")))

(define-public crate-consume_ptr-0.2.2 (c (n "consume_ptr") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "15yfbkqh1riqfzh7zfqhpzwyfm0clgb63ds80f93g7l493d9bgw4")))

(define-public crate-consume_ptr-0.3.0 (c (n "consume_ptr") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "0291z8syfawp88iqrq7pbdb0wg41y1sj12vl2s4rw1wycj1zs8b6")))

