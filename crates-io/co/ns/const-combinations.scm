(define-module (crates-io co ns const-combinations) #:use-module (crates-io))

(define-public crate-const-combinations-1.0.0 (c (n "const-combinations") (v "1.0.0") (h "1hbjcrq5i2099ccq0hk37i6syagdh624jsds6h6vlg93khk78r0f")))

(define-public crate-const-combinations-2.0.0 (c (n "const-combinations") (v "2.0.0") (h "1hnyg0fjmmy2cfa5zbvlb8zx9d1l5acm5nj4gj3fvbcr1y8ng8q4")))

(define-public crate-const-combinations-2.0.2 (c (n "const-combinations") (v "2.0.2") (h "0cx4ypi2y20rqm9i4qbw5a3mwn6lwv677rmc14wnbfqxc7alvrx2")))

(define-public crate-const-combinations-2.0.3 (c (n "const-combinations") (v "2.0.3") (h "18asvbpxxg972cmgklfr33fkcghjxvk6lbjprkm2y20j3zj11xmv")))

