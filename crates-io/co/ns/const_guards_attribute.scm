(define-module (crates-io co ns const_guards_attribute) #:use-module (crates-io))

(define-public crate-const_guards_attribute-0.1.0 (c (n "const_guards_attribute") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "0svcip6p8f4pfnib26hzj4mwc4qb6xm0xxb4kbc4mcbf8yaa3cny")))

(define-public crate-const_guards_attribute-0.1.1 (c (n "const_guards_attribute") (v "0.1.1") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "04dl5ks48aiaka7czhcqiv6hplxi8l9icjfsah7bb9l7i281i7ga")))

(define-public crate-const_guards_attribute-0.1.2 (c (n "const_guards_attribute") (v "0.1.2") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "1nfrav1wkwa4714wgqdzcni8ccc6k5cqsz31y00yvg3jsplk8v7g")))

(define-public crate-const_guards_attribute-0.1.3 (c (n "const_guards_attribute") (v "0.1.3") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full"))) (d #t) (k 0)))) (h "110g0kx8lwbp8h0nvpa6i8icj7c0g761ipqyvy1ap7a9jgrbr8xi")))

