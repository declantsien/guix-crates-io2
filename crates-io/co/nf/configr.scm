(define-module (crates-io co nf configr) #:use-module (crates-io))

(define-public crate-configr-0.5.0 (c (n "configr") (v "0.5.0") (d (list (d (n "configr_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "18gp8wgyrn4vfg9p2yzxb3r7rbv1nlwjq3ykisiw6jnkj7w6lkmn")))

(define-public crate-configr-0.5.1 (c (n "configr") (v "0.5.1") (d (list (d (n "configr_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1d77ys9jxhcsi6f3dsrkqzrwsc79534534pkjr3r570d5zhk50zp")))

(define-public crate-configr-0.5.2 (c (n "configr") (v "0.5.2") (d (list (d (n "configr_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1izhss7yz06r28fjab5zr4p4m2ap1yhk048c23j8ayw6zwkvj9v3")))

(define-public crate-configr-0.6.0 (c (n "configr") (v "0.6.0") (d (list (d (n "configr_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0lc2sjfgq6apa79i04szy2q7rzp5apvh81mspq8344gn36hzq1y6")))

(define-public crate-configr-0.6.5 (c (n "configr") (v "0.6.5") (d (list (d (n "configr_derive") (r "^0.6.5") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1rrzxk31s98klr1gaq75p9gv8r0r86zm8wrhbfw3zm0d0b6d4q5f")))

(define-public crate-configr-0.6.6 (c (n "configr") (v "0.6.6") (d (list (d (n "configr_derive") (r "^0.6.6") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1mr16m9ayc64j6vzra0jy7w3zj3wfymp0xdmdahvw00fzcg0r1ca")))

(define-public crate-configr-0.7.0 (c (n "configr") (v "0.7.0") (d (list (d (n "configr_derive") (r "^0.6.6") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1dhyf5bkxlmgl9zhsdi47lqmi4fb2cics7i21sxp7xxl0vx9xr7p")))

(define-public crate-configr-0.8.0 (c (n "configr") (v "0.8.0") (d (list (d (n "configr_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0dili6q0bqqxf74x4d6xqb0mqj5m6d7rxlp2j8fmp1b0hw8p1iff")))

