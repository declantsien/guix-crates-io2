(define-module (crates-io co nf confgr_derive) #:use-module (crates-io))

(define-public crate-confgr_derive-0.1.0 (c (n "confgr_derive") (v "0.1.0") (d (list (d (n "confgr_core") (r "^0.1.0") (d #t) (k 0)) (d (n "config") (r "^0.14.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "136lpkaar03z1zbvn63hy62qcds5a8698v5n1b6277z6mrznvx40")))

(define-public crate-confgr_derive-0.1.1 (c (n "confgr_derive") (v "0.1.1") (d (list (d (n "confgr_core") (r "^0.1.1") (d #t) (k 0)) (d (n "config") (r "^0.14.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1hyicq3klxb18a86yds903kk3c74w4vdmr9a8z5fjdbd1p3nwkvq")))

(define-public crate-confgr_derive-0.2.0 (c (n "confgr_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1gxa3zjma5bcrw64yg5jql0qr6jnybcpp98s6640rnb7ll1kq2dj")))

(define-public crate-confgr_derive-0.2.1 (c (n "confgr_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1b8dn539vxwpqyxs6db3yv9dmw93f6bncijaa06kyicj8ijsr7sg")))

