(define-module (crates-io co nf confirm-rs) #:use-module (crates-io))

(define-public crate-confirm-rs-0.1.0 (c (n "confirm-rs") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "getch") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "106i363y3vj8kfgw5qdjakhqscmmk1fvn6vkml60g3dy72g2qjbm")))

(define-public crate-confirm-rs-0.1.1 (c (n "confirm-rs") (v "0.1.1") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "getch") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "00w9hqywqgcqmmzdc2gwyyqskz7i3q6d7fmlg8jly6bqzknk2bkz")))

(define-public crate-confirm-rs-0.1.2 (c (n "confirm-rs") (v "0.1.2") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "getch") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "12l0m5bzv1bl9i38lz2c40a43rh8x3rpjmpf7k1n1s3zp0ig778v")))

(define-public crate-confirm-rs-1.0.0 (c (n "confirm-rs") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "getch") (r "^0.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "12x45lib62fr47b557q8sisc6c3jb29vsijr0zxanaj1c3d5kcif")))

(define-public crate-confirm-rs-1.1.0 (c (n "confirm-rs") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "getch") (r "^0.3.1") (d #t) (k 0)))) (h "0zikrp503f57n6qrsf11pf6z9ril78xd7bghl33pqy77jd9az3nc")))

