(define-module (crates-io co nf config-manager-proc) #:use-module (crates-io))

(define-public crate-config-manager-proc-0.1.0 (c (n "config-manager-proc") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "07qrixv4vd15k5ky7ch8f09jq1cv13vmk6fhq549nli3nfx5ibzj")))

(define-public crate-config-manager-proc-0.1.1 (c (n "config-manager-proc") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "0fjximpgl2mhywwyv9j0ygraz4qacnib1341vp705vm12w6wv04m")))

(define-public crate-config-manager-proc-0.2.0 (c (n "config-manager-proc") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "1060piz5k70hzjgzr9a73bs8mdkigjffs4x2sc4qhw3yjm9di8pc")))

