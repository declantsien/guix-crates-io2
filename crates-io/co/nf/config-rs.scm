(define-module (crates-io co nf config-rs) #:use-module (crates-io))

(define-public crate-config-rs-0.1.0 (c (n "config-rs") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "104j27d4z0gdl3k4v7vqsnf2d2cmj28kgpnhvdrw6fmivps8n4fj")))

(define-public crate-config-rs-0.1.1 (c (n "config-rs") (v "0.1.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0cqbiph5hbc6lm76masvy6n79lyzm4ba5bwzcd15fjkkcp08rj0j")))

(define-public crate-config-rs-0.1.2 (c (n "config-rs") (v "0.1.2") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1dpagaqn8zy9lpbqr0n51fymv7s5cgh2zjxqq9dalh5jzgkqlgxs") (f (quote (("ini") ("default" "ini"))))))

(define-public crate-config-rs-0.1.3 (c (n "config-rs") (v "0.1.3") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "06v02c81cdr6dj3y893caqbjawp8zrd8zq01gjmdb7gpqarlg720") (f (quote (("ini") ("default" "ini" "conf") ("conf"))))))

