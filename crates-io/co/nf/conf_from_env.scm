(define-module (crates-io co nf conf_from_env) #:use-module (crates-io))

(define-public crate-conf_from_env-0.1.0 (c (n "conf_from_env") (v "0.1.0") (d (list (d (n "cfe_progmacro") (r "^0.1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (d #t) (k 0)))) (h "0sli7pqw5g6160ll4xkhpd6ryrxi3pqm2nl1lkj6ra1afdk7llc3")))

