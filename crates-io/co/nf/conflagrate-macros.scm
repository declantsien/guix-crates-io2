(define-module (crates-io co nf conflagrate-macros) #:use-module (crates-io))

(define-public crate-conflagrate-macros-0.0.1 (c (n "conflagrate-macros") (v "0.0.1") (d (list (d (n "dot-structures") (r ">=0.1.0") (d #t) (k 0)) (d (n "graphviz-rust") (r ">=0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h2i4bc27m689yndlvhjgcm7yc7dmny6yghnzpzis8bqjzz9ib1f")))

(define-public crate-conflagrate-macros-0.0.2 (c (n "conflagrate-macros") (v "0.0.2") (d (list (d (n "dot-structures") (r ">=0.1.0") (d #t) (k 0)) (d (n "graphviz-rust") (r ">=0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11sl8i1a8c4jwmklvs5n6p5dm0khw64bx6bc3828yipl0qn2d7zq")))

(define-public crate-conflagrate-macros-0.0.3 (c (n "conflagrate-macros") (v "0.0.3") (d (list (d (n "dot-structures") (r ">=0.1.0") (d #t) (k 0)) (d (n "graphviz-rust") (r ">=0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d5c6rfid0qqskh38sddlcx2yhb9agq8qla725db0wm1lb7x8yfk")))

(define-public crate-conflagrate-macros-0.1.0 (c (n "conflagrate-macros") (v "0.1.0") (d (list (d (n "dot-structures") (r ">=0.1.0") (d #t) (k 0)) (d (n "graphviz-rust") (r ">=0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l3yksapk7wa7hk406nq2ixaqcy85mwywk4bc0nvzawrknwpn2kb")))

