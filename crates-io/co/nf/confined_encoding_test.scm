(define-module (crates-io co nf confined_encoding_test) #:use-module (crates-io))

(define-public crate-confined_encoding_test-0.9.0-alpha.1 (c (n "confined_encoding_test") (v "0.9.0-alpha.1") (d (list (d (n "amplify") (r "^3.13.0") (d #t) (k 0)) (d (n "confined_encoding") (r "^0.9.0-alpha.1") (d #t) (k 0)))) (h "0zbi6k0103fr3v89rbkgfjl0fbllkygjl5m51lal26kzsa9gcnl0") (r "1.59.0")))

