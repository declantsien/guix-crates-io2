(define-module (crates-io co nf confusion_matrix) #:use-module (crates-io))

(define-public crate-confusion_matrix-1.0.0 (c (n "confusion_matrix") (v "1.0.0") (h "07qw0max0w623wmqn7f0gmdjzx2h3ifrkh3is2zqlxmfjzi20qzi")))

(define-public crate-confusion_matrix-1.0.1 (c (n "confusion_matrix") (v "1.0.1") (h "1xzy1j8y2fsbvy4j52z5k2b45z8yq9ifln2xamfhllb450p93c4k")))

(define-public crate-confusion_matrix-1.1.0 (c (n "confusion_matrix") (v "1.1.0") (h "1q8qrphmpz6f41dqpalrzs6rkmd47vl3d203vj8yhzrwwcr8my6k")))

