(define-module (crates-io co nf configurable) #:use-module (crates-io))

(define-public crate-configurable-0.3.3 (c (n "configurable") (v "0.3.3") (d (list (d (n "directories") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1x8hyqsh4b3amf0dq4mb9xq2aahg7c2w7ip4b5n0k46l9p6v8bis")))

(define-public crate-configurable-0.3.4 (c (n "configurable") (v "0.3.4") (d (list (d (n "directories") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1n62kddfqcsa6i74p71gzkwjqm6mp4d3rcmvymx32q6kzqdsys30")))

(define-public crate-configurable-0.3.5 (c (n "configurable") (v "0.3.5") (d (list (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1w0nwx81mg0hwksfaxg7xzqga4qzqy1jq1702211xivm1kg86j53")))

