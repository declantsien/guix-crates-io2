(define-module (crates-io co nf conf_json) #:use-module (crates-io))

(define-public crate-conf_json-0.1.2 (c (n "conf_json") (v "0.1.2") (h "0hx18kb0i8s81nyqn3sdkpnwnnw5bdg2schqhgn7mhbg3pcc7gk2")))

(define-public crate-conf_json-0.1.3 (c (n "conf_json") (v "0.1.3") (h "1gqjjwx6lnykp9s5sdja0f0hhgj1q0f3y4hwqfzsdfl57pm9whjd")))

(define-public crate-conf_json-0.1.4 (c (n "conf_json") (v "0.1.4") (h "0qwlq4by9y0v27rv06lp4hfw1gmdk0aapasjhkhihsap07hc29al")))

