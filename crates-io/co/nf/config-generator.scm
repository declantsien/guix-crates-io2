(define-module (crates-io co nf config-generator) #:use-module (crates-io))

(define-public crate-config-generator-0.0.1 (c (n "config-generator") (v "0.0.1") (d (list (d (n "config-gen-macro-impl") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "0mj42ydr8m70fkx5nyhy4774qk3qm8ii81rsx3cqwhnwk1p517pk")))

(define-public crate-config-generator-0.1.0 (c (n "config-generator") (v "0.1.0") (d (list (d (n "config-gen-macro-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "1az8q3kx81y8hnwh3sb0vskxlccc3mpbkv5i69ywkg7cdim6sfap")))

(define-public crate-config-generator-0.1.1 (c (n "config-generator") (v "0.1.1") (d (list (d (n "config-gen-macro-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "0npxjcih5ap11wyyxfmbx7ib31k9hqnlfmz6barz0iwxz7xh9pbi")))

(define-public crate-config-generator-0.1.2 (c (n "config-generator") (v "0.1.2") (d (list (d (n "config-gen-macro-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "1iipw8sgrah7w9bds7jlmqrbx4bqx6zy8l5knszxj0wma22klvbm")))

(define-public crate-config-generator-0.1.3 (c (n "config-generator") (v "0.1.3") (d (list (d (n "config-gen-macro-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "128k360jjs8hp19h7dkm4imz8pqr4mjfahcrkqr6fv4865lfpz1w") (f (quote (("load_toml" "config-gen-macro-impl/load_toml") ("default" "load_toml"))))))

