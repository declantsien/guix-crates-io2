(define-module (crates-io co nf confpiler) #:use-module (crates-io))

(define-public crate-confpiler-0.1.0 (c (n "confpiler") (v "0.1.0") (d (list (d (n "config") (r "^0.12") (d #t) (k 0)))) (h "14lx87rf42w53ssblpm3c5piga2wsz4pjl8a7mhycjiy94m159mp")))

(define-public crate-confpiler-0.2.0 (c (n "confpiler") (v "0.2.0") (d (list (d (n "config") (r "^0.12") (d #t) (k 0)))) (h "0vwb5y2r4d62znakxm54jv1dypzj9vg8hprf3bw9w7rq2fwryaq1")))

(define-public crate-confpiler-0.2.1 (c (n "confpiler") (v "0.2.1") (d (list (d (n "config") (r "^0.12") (d #t) (k 0)))) (h "1zrdmm3ywi98035k6cvqhk7frlknk655ky68ffr6a7pxks16s4wg")))

(define-public crate-confpiler-0.2.2 (c (n "confpiler") (v "0.2.2") (d (list (d (n "config") (r "~0.13.0") (d #t) (k 0)))) (h "1dmybrshcpz82qkb71scsxc9pcwi7mjh3m82534m7q2s2g58x5xh") (r "1.58")))

