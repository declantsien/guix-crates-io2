(define-module (crates-io co nf confium) #:use-module (crates-io))

(define-public crate-confium-0.1.1 (c (n "confium") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 0)) (d (n "slog-stdlog") (r "^4.1") (d #t) (k 0)) (d (n "slog-term") (r "^2.6") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1z8knnqsvvznank124c0y08d71gsrwyjz3r56wixvnz1sjzrn9bv")))

(define-public crate-confium-0.1.2 (c (n "confium") (v "0.1.2") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "slog") (r "^2.5") (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 0)) (d (n "slog-stdlog") (r "^4.1") (d #t) (k 0)) (d (n "slog-term") (r "^2.6") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "04zdzh00frnvg62mzamy9i13i40fxnxzvr800xw0q5d5x50bhw0b")))

