(define-module (crates-io co nf config-tree) #:use-module (crates-io))

(define-public crate-config-tree-0.1.1 (c (n "config-tree") (v "0.1.1") (h "02b2lg2aiz4nmnqvd8bb7aa67ssnwzg72xb0q1v36r3li44vaj7b")))

(define-public crate-config-tree-0.1.2 (c (n "config-tree") (v "0.1.2") (h "0bwayv7hv24drp0x2lvfi0g2cmiigfanjbpyglwpbpsp7i4k4ha5")))

