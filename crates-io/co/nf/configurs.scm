(define-module (crates-io co nf configurs) #:use-module (crates-io))

(define-public crate-configurs-0.1.0 (c (n "configurs") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0z15331qpp8sxp57pw6wmsi2693mxb36a96r7f2jyl233z77s49p")))

(define-public crate-configurs-0.1.1 (c (n "configurs") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "1alkfi05f9mjd3ymwz87fk70s80ahycb07mngbkaci5sjj426yfn")))

