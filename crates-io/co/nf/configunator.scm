(define-module (crates-io co nf configunator) #:use-module (crates-io))

(define-public crate-configunator-0.1.0 (c (n "configunator") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "08qb4wlbkrrvwbars31q3bvff2bv7vjxzp8yf2qp3m4qipxjqmdn")))

(define-public crate-configunator-0.2.0 (c (n "configunator") (v "0.2.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0f58yf005nnxzr33k44ygcpjh74115hkryqylpx32i72fn631c8f")))

