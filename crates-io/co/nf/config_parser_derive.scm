(define-module (crates-io co nf config_parser_derive) #:use-module (crates-io))

(define-public crate-config_parser_derive-0.1.0 (c (n "config_parser_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0bnc0k6p90swgz4daw89wpb0p00z244fplc513ibbbp7dmjjrhsj")))

(define-public crate-config_parser_derive-0.1.1 (c (n "config_parser_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "0dhkg7i7hk70w0h3z2051y2npsxcg84rjrpj0qq5z26ip2kbr73b")))

(define-public crate-config_parser_derive-0.1.2 (c (n "config_parser_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0vl4wv4mhpsq9r4i3q5ml7s4fnp2clcvdb2kviilbds7f92dl5h6")))

(define-public crate-config_parser_derive-0.1.3 (c (n "config_parser_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1vxbzw1s7bkd604fcrs3rf0iyqvmgcyk3dg3hvilp0hgcyri0mw5")))

