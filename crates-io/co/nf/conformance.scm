(define-module (crates-io co nf conformance) #:use-module (crates-io))

(define-public crate-conformance-0.1.0 (c (n "conformance") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0ah38hy0scv1gj281n23gs9n4kqs6s6hiszqk015v5v2xs5rd0jr")))

(define-public crate-conformance-0.2.0 (c (n "conformance") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dkqwqnckz2j0xvabrlls0py43py6w45r6nbw2yfccazs9dfllr5")))

