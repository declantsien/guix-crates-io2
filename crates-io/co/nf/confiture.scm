(define-module (crates-io co nf confiture) #:use-module (crates-io))

(define-public crate-confiture-0.1.0 (c (n "confiture") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "17kfkfk021jzz13isnwjkvvhz4agandmsdf09scq3fj2ad5w2n68")))

(define-public crate-confiture-0.1.1 (c (n "confiture") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0v61rsx9iz5alvv98jfm8jqfqvd30qc8j7piiyb60rnr1fk8852a")))

(define-public crate-confiture-0.1.2 (c (n "confiture") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "19416l349ydpgjlsvrm0clkfdlyh796nwc65lv9smliajvrx9lx1")))

(define-public crate-confiture-0.1.3 (c (n "confiture") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "19dy443dvbky280vr243z4163z0ab79ljqc1afdb9f45an7kwixv")))

(define-public crate-confiture-0.1.4 (c (n "confiture") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "124sfhvdxixl20rn6j777s221pwafyrzv87d8c63za86wx78c4fa")))

