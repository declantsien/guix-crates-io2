(define-module (crates-io co nf confluence) #:use-module (crates-io))

(define-public crate-confluence-0.1.0 (c (n "confluence") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.3.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.2") (d #t) (k 0)))) (h "1bq432fs9ny0f7rfbw0nmjqjzr5bg4jw6lpj5c2r9bn2pm3pk2yv")))

(define-public crate-confluence-0.1.1 (c (n "confluence") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.3.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.2") (d #t) (k 0)))) (h "1w3rbxkx64gyi26w5q42njwc3mph97m7nb2yip519aw9rm1jzycf")))

(define-public crate-confluence-0.2.0 (c (n "confluence") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.3.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.2") (d #t) (k 0)))) (h "1gx7a7nhzqrkly0pcwnz498lzhbm5xwscqx058y68mdfs4zmzbgr")))

(define-public crate-confluence-0.3.0 (c (n "confluence") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)) (d (n "xmltree") (r "^0.2") (d #t) (k 0)))) (h "0p4zhq2lxs7kz13c1bnr14j007k7fx9sw8cwxy4wzb9kgnhzjmg4")))

(define-public crate-confluence-0.4.0 (c (n "confluence") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "045ay4d41i0inwkc3ina6q1mr71imjpwa68958iirw3lg8l88812")))

(define-public crate-confluence-0.4.1 (c (n "confluence") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "xml-rs") (r "^0.7") (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "02yxngdhr2aiyyilc1q0v3n870pxq979gqxpx97r8k0bl6dg4bwi")))

