(define-module (crates-io co nf config-it-macros) #:use-module (crates-io))

(define-public crate-config-it-macros-0.0.1 (c (n "config-it-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1lw3y04msvzcrazv61r9glr6mx0ba1lmcqgihrkx15ypd6n7n7yv")))

(define-public crate-config-it-macros-0.0.2 (c (n "config-it-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1b8q9ah79jgs9wxvkv7h77318skcb9igk3q70hqp85ckd50rj4ks")))

(define-public crate-config-it-macros-0.1.0 (c (n "config-it-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0lh7bv5z2g1vwrqss3vfspnaq8a8342k1nxrkswmhnqaq4dq47a6")))

(define-public crate-config-it-macros-0.1.2 (c (n "config-it-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0flm3gcsk592k1aqj258filkivv67gc20qmb749r8mi64xmnh62v")))

(define-public crate-config-it-macros-0.2.0 (c (n "config-it-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "16dl2a4yljdd6f8kl5f3q6c589lvi7i3l0xhl1pwnbwxvkwhwi0g")))

(define-public crate-config-it-macros-0.3.0 (c (n "config-it-macros") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0m3c8ndr8zrchafv19mn07l78d2473fynllgna6z1zwpj2finxaw") (f (quote (("nocfg") ("more_attr") ("default" "more_attr" "nocfg"))))))

(define-public crate-config-it-macros-0.3.1 (c (n "config-it-macros") (v "0.3.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0nvmm2lga1c07x1xvdv18gbcw7nf9d0xxvsdyhj1bxn6vxdx1vn9") (f (quote (("nocfg") ("more_attr") ("default" "more_attr" "nocfg"))))))

(define-public crate-config-it-macros-0.3.2 (c (n "config-it-macros") (v "0.3.2") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "125n7jy1srl7cm7xwdj77va7ka5yx3qifd9zd5m085x6x1flxc6w") (f (quote (("nocfg") ("more_attr") ("default" "more_attr" "nocfg"))))))

(define-public crate-config-it-macros-0.4.0 (c (n "config-it-macros") (v "0.4.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jzykng8mshj4rx7046p909hm60vxdmzd4q1bg7zvldljbzys73j") (f (quote (("nocfg") ("more_attr") ("default" "more_attr" "nocfg"))))))

(define-public crate-config-it-macros-0.5.0 (c (n "config-it-macros") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1y2l3cncgvhcxjaafmcpgkpkm920bl7ipnhb8g80z61ff9k1qqw6") (f (quote (("jsonschema"))))))

(define-public crate-config-it-macros-0.5.1 (c (n "config-it-macros") (v "0.5.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0mjalw7d36izvzclnlkxjw923p595bg96cpfksw6lcn4fsamz756") (f (quote (("jsonschema"))))))

(define-public crate-config-it-macros-0.5.2 (c (n "config-it-macros") (v "0.5.2") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "18nlq8sldy0y4y163njakmrflyzcnzy94jdwn45632916z40fi35") (f (quote (("jsonschema"))))))

(define-public crate-config-it-macros-0.5.3 (c (n "config-it-macros") (v "0.5.3") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1izqc6vnrgzwzp4msrpi22fxgicm09lvkgaw6j1l3m7g71ms2chv") (f (quote (("jsonschema"))))))

(define-public crate-config-it-macros-0.5.4 (c (n "config-it-macros") (v "0.5.4") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "15i303swn38zkx2vs7xhfhbxg8kb2i4ci2z204ywppf88n073qsb") (f (quote (("jsonschema"))))))

