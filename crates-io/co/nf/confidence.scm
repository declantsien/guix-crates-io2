(define-module (crates-io co nf confidence) #:use-module (crates-io))

(define-public crate-confidence-1.0.0 (c (n "confidence") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "same-file") (r "^1.0.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "14a3vdrd8hiw77ig56x3zlknzk3pjaa5yij45fqlaxh2s3czqwah")))

