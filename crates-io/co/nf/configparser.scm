(define-module (crates-io co nf configparser) #:use-module (crates-io))

(define-public crate-configparser-0.1.0 (c (n "configparser") (v "0.1.0") (h "0yaf490796ivr134w3ymgda0gzyrddfgj938j6wazsrkvh3kr5xx") (y #t)))

(define-public crate-configparser-0.1.1 (c (n "configparser") (v "0.1.1") (h "17bh3n5l33r9j548vkwy2mslr900ri82jfpcjzi97mbwqpb7fxia")))

(define-public crate-configparser-0.2.0 (c (n "configparser") (v "0.2.0") (h "00chyxhs7kgrqg55cb086kgin16pgl0lz6y25bx6p3pm8h66a4rf")))

(define-public crate-configparser-0.2.1 (c (n "configparser") (v "0.2.1") (h "1h8m8fbffg8k8zwhx87d1b5z8rflfavz1qk0llxqzaw33h7ifp1g")))

(define-public crate-configparser-0.2.2 (c (n "configparser") (v "0.2.2") (h "0lpk7fvqa21kfbyn3xx38fp4m3rwnif7ch6myih4bvwf5gqd68z6")))

(define-public crate-configparser-0.3.0 (c (n "configparser") (v "0.3.0") (h "0ip7j0z5sdfv4i8r0nwdpsall557wgzd5q2gxvbv7acpj3qgyrpd")))

(define-public crate-configparser-0.3.1 (c (n "configparser") (v "0.3.1") (h "1pdx67z1a9rmaq22rzbyxkasxma86bigyrqfj59a33ixifm9sjlg")))

(define-public crate-configparser-0.4.0 (c (n "configparser") (v "0.4.0") (h "15v7vsq4k9z4sg43shhs7kg82l3s38mnkfgl7ggs87r3kif11m76")))

(define-public crate-configparser-0.4.1 (c (n "configparser") (v "0.4.1") (h "0fb3hb5w5lv7jcijl5d3wdhfsbbasiwhkrb58147r27wya7snxrp")))

(define-public crate-configparser-0.5.0 (c (n "configparser") (v "0.5.0") (h "173xbhm7dx264fcaazyairlvac74xwcf95r9pbkgigv1hh9vp0wq") (y #t)))

(define-public crate-configparser-0.5.1 (c (n "configparser") (v "0.5.1") (h "0fvj3lspqdhd6hp78ykz695lhnv9szpl2v809ivpin1acv1ha4h8")))

(define-public crate-configparser-0.6.0 (c (n "configparser") (v "0.6.0") (h "08lszbpl6cwjnb53h1rqglgh6lzf1lwp74cgxynlpbaa6mn2mk00")))

(define-public crate-configparser-0.6.1 (c (n "configparser") (v "0.6.1") (h "1h92lc3245c41lx5nglgvpfvx5x09dmzixrmsl17nhzgwwpcdl2p") (y #t)))

(define-public crate-configparser-0.6.2 (c (n "configparser") (v "0.6.2") (h "1x87papj6a0rxn65vcby03qz2gpymwq3q6fl3n0zl9rcsrs5qvc9")))

(define-public crate-configparser-0.7.0 (c (n "configparser") (v "0.7.0") (h "1lxavs0c7mjfjrd64lhkspzf70g5rd52i4p8p31ha2ziyq8wm6pc")))

(define-public crate-configparser-0.7.1 (c (n "configparser") (v "0.7.1") (h "049pjvl20kl2mx0m1cnml6b1xq8hn2fsrwf2rs6xr4y2mix1qg72")))

(define-public crate-configparser-0.8.0 (c (n "configparser") (v "0.8.0") (h "0ahaaywdc8kbkx5xqcvkvjbr3yv5lwgwlvqa9hlpg1rcwkl8kca2")))

(define-public crate-configparser-0.8.1 (c (n "configparser") (v "0.8.1") (h "0wvbfrgxzpnjh3mxy8b4lhllljkl9ya8q52vsfvv0diynfq90cya")))

(define-public crate-configparser-0.9.0 (c (n "configparser") (v "0.9.0") (h "1zwc3xr5cf0namzv6kv4162zfi2dvwyppr4hzj42y2dk9xc980xk")))

(define-public crate-configparser-0.9.1 (c (n "configparser") (v "0.9.1") (h "1n427l55z81m6qcw587xdiw1cw6wxn7xg24f9pmx1brvm2yq8dyx")))

(define-public crate-configparser-0.9.2 (c (n "configparser") (v "0.9.2") (h "1g33bjb7l4h0fgd2bshq5x9hny2vvk9d6c9b6fkniyzdi3bd9ial")))

(define-public crate-configparser-0.10.0 (c (n "configparser") (v "0.10.0") (h "043ac66390n98w2fi408018sj5szi89sgzmfnhp78h08jb8zc8fc")))

(define-public crate-configparser-0.11.0 (c (n "configparser") (v "0.11.0") (h "1c505af52k7ai52fkcnbb0aynyzgf9r0gy4jdwh4ib9bgpn1jfsx")))

(define-public crate-configparser-0.11.1 (c (n "configparser") (v "0.11.1") (h "1bc7n7lih63sn1r6dcch4cna1fdzzqssxxwyczdrxrsa437sky15")))

(define-public crate-configparser-0.12.0 (c (n "configparser") (v "0.12.0") (h "0xf73pfbszn91c47aqpx4mwa7nnnbnac4gdr7i7c06f4vhaksdmn")))

(define-public crate-configparser-0.13.0 (c (n "configparser") (v "0.13.0") (h "1ww707l42aa9fzn2j5i4qzhwrjyzn3gf03nl012hziky14lm9r0h")))

(define-public crate-configparser-0.13.1 (c (n "configparser") (v "0.13.1") (h "0yrki3w1bdvgcf525qhszl13cacksbb6x68fc64wrcnlqj0jgj6s") (y #t)))

(define-public crate-configparser-0.13.2 (c (n "configparser") (v "0.13.2") (h "0s6wl6bqaix05l6l698rvvmkvhg1f7j471biajgnvkcg3x502swf")))

(define-public crate-configparser-1.0.0 (c (n "configparser") (v "1.0.0") (h "19s7rcq71rry8zgy2x3pn1c9m11gbx3a3yxx8kj7knnilz6ps7gy")))

(define-public crate-configparser-2.0.0 (c (n "configparser") (v "2.0.0") (h "1qdaggalm3js86s2i11pjjcndrrvpk0pw1lfvmv7v25z3y66sqg2")))

(define-public crate-configparser-2.0.1 (c (n "configparser") (v "2.0.1") (h "1zjghi586ksaxf64sv1xixdj4ckpb663ny3dxw4vhidydpbkkbaa")))

(define-public crate-configparser-2.1.0 (c (n "configparser") (v "2.1.0") (h "13zcjw2jlcc73fd1lcjmb2vzh3ckankin490m24xa96i2vj1w87p")))

(define-public crate-configparser-3.0.0 (c (n "configparser") (v "3.0.0") (d (list (d (n "indexmap") (r "^1.7") (o #t) (d #t) (k 0)))) (h "00n2l9jb3yinp0lar26imvqag5667cfbgia7rw988yikk2jix0h6")))

(define-public crate-configparser-3.0.1 (c (n "configparser") (v "3.0.1") (d (list (d (n "indexmap") (r "^1.9.1") (o #t) (d #t) (k 0)))) (h "066bdscah69mdj3dpx8cs1wf9blqs6plqqjl36jkd1489c6xxjv5")))

(define-public crate-configparser-3.0.2 (c (n "configparser") (v "3.0.2") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (o #t) (d #t) (k 0)))) (h "0ypq3phwrlx2c7agdj1rlivkhsk9k795jb30j58azvw7lp8xjn2l")))

(define-public crate-configparser-3.0.3 (c (n "configparser") (v "3.0.3") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (o #t) (d #t) (k 0)))) (h "0dwjni8z9v26ysn7yqw3ickvqbrwjd0cv1ag20manlia990nxrg0")))

(define-public crate-configparser-3.0.4 (c (n "configparser") (v "3.0.4") (d (list (d (n "indexmap") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1202hawv78c3w2j8s44jjcflnpvkwgv3l1irafl7f0smivdd7ijf") (f (quote (("async-std" "tokio"))))))

