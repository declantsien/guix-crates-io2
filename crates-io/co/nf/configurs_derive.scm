(define-module (crates-io co nf configurs_derive) #:use-module (crates-io))

(define-public crate-configurs_derive-0.1.0 (c (n "configurs_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0fj1ah7qlgmh6s13rgjdq0x99db69g6jx1yk6jp3nsynnc4s1nr5") (f (quote (("file_loader" "serde" "serde_derive" "toml"))))))

