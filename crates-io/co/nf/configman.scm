(define-module (crates-io co nf configman) #:use-module (crates-io))

(define-public crate-configman-0.3.0 (c (n "configman") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "fmtlog") (r "^0.1.4") (f (quote ("colored"))) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0jjvi8p99ir4xhm3wyc839sfg5r0agkclym8a5in7hr61g4833v0")))

(define-public crate-configman-0.4.0 (c (n "configman") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "fmtlog") (r "^0.1.4") (f (quote ("colored"))) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1vvkbr19v4wzywd29hdpa60h6pxfjsjfgak7wddbxfdgi2f8f7v9")))

(define-public crate-configman-0.4.1 (c (n "configman") (v "0.4.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "fmtlog") (r "^0.1.4") (f (quote ("colored"))) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1x9r3sw87q265fl4psdvy8pw63ik95p22rzvdia00k920w0adxbi")))

