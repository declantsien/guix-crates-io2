(define-module (crates-io co nf configuration) #:use-module (crates-io))

(define-public crate-configuration-0.0.1 (c (n "configuration") (v "0.0.1") (h "15ab83i0di7vgrvrk51h1byxx86mq63sc7r0kkjy3qcqhrl06pib")))

(define-public crate-configuration-0.1.0 (c (n "configuration") (v "0.1.0") (d (list (d (n "options") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1l26rcjgj2wl3qp5r9qd7ynv9qcnv090rsk1k2f7j07p20hylw4d") (f (quote (("default" "toml"))))))

(define-public crate-configuration-0.1.1 (c (n "configuration") (v "0.1.1") (d (list (d (n "options") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (o #t) (k 0)))) (h "15jrwc1ylpar7d2z24f8v6ib76rd5k81c8v3vmysrs4bd3iy0w5q") (f (quote (("default" "toml"))))))

(define-public crate-configuration-0.2.0 (c (n "configuration") (v "0.2.0") (d (list (d (n "options") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.1") (o #t) (k 0)))) (h "1dg6c6418mjy3n9zggbfrmira8pgvp7z6ap1ib32lcz0mg936w7k") (f (quote (("default" "toml"))))))

(define-public crate-configuration-0.3.0 (c (n "configuration") (v "0.3.0") (d (list (d (n "options") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.1") (o #t) (k 0)))) (h "1sj5ra3m839mh5847fmq6r6inb6d5y9sh8ncafy0w6qz1bh5bgam") (f (quote (("unstable") ("default" "toml"))))))

(define-public crate-configuration-0.4.0 (c (n "configuration") (v "0.4.0") (d (list (d (n "options") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.1") (o #t) (k 0)))) (h "19vs1zsg396kfv40f4zcb471cmk3mdnsqjz2g6m6q0ifvlnlhwgk") (f (quote (("unstable") ("default" "toml"))))))

(define-public crate-configuration-0.5.0 (c (n "configuration") (v "0.5.0") (d (list (d (n "options") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.1") (o #t) (k 0)))) (h "1v32f314igchcpapasa042k29f79iky7qdxzxzab0hcfz9dkqv9j") (f (quote (("unstable") ("default" "toml"))))))

(define-public crate-configuration-0.6.0 (c (n "configuration") (v "0.6.0") (d (list (d (n "options") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.1") (o #t) (k 0)))) (h "0gpp0r39355vhgby5dj73izx61s4wl6hdkzwcm2az159kiylr6jl") (f (quote (("unstable") ("default" "toml"))))))

(define-public crate-configuration-0.7.0 (c (n "configuration") (v "0.7.0") (d (list (d (n "options") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.1") (o #t) (k 0)))) (h "0xhw23v8fsmdlbvd2lgsgwprz85ga8z5gaf1r17ibyhlylhfc3mm") (f (quote (("default" "toml"))))))

(define-public crate-configuration-0.7.1 (c (n "configuration") (v "0.7.1") (d (list (d (n "options") (r "^0.5") (d #t) (k 0)) (d (n "toml") (r "^0.1") (o #t) (k 0)))) (h "1q65azlvf1bfr84llblnlqm70mw0pvm0gy1py7zck0qj1bpvqprh") (f (quote (("default" "toml"))))))

(define-public crate-configuration-0.7.2 (c (n "configuration") (v "0.7.2") (d (list (d (n "options") (r "^0.5") (d #t) (k 0)) (d (n "toml") (r "^0.1") (o #t) (k 0)))) (h "1qsq8qjlqildldm8y52kdhm8lgjz64n3s7gmr24hzb3zs4aq0m6b") (f (quote (("default" "toml"))))))

