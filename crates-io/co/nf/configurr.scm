(define-module (crates-io co nf configurr) #:use-module (crates-io))

(define-public crate-configurr-1.0.0 (c (n "configurr") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.18.0") (f (quote ("v1_26"))) (d #t) (k 0)) (d (n "kube") (r "^0.84.0") (f (quote ("runtime" "derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "really-notify") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08zmw1xzx9cljqafwyx4g7xx4fya8d4xnm2d8a074zh66i5w5nk7")))

