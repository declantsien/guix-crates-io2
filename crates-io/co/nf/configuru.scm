(define-module (crates-io co nf configuru) #:use-module (crates-io))

(define-public crate-configuru-0.1.0 (c (n "configuru") (v "0.1.0") (d (list (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "0gy7lzps98d9h68jr8dvffwh68wdv5fypmnbg2qmaqzv5qwjygck")))

(define-public crate-configuru-0.1.1 (c (n "configuru") (v "0.1.1") (d (list (d (n "displaydoc") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "1f7fvv3hs4d8jx9w8vpqk0njja02ivznns965q96qrlq2zwywwws")))

