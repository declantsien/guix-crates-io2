(define-module (crates-io co nf config2args) #:use-module (crates-io))

(define-public crate-config2args-0.1.0 (c (n "config2args") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (f (quote ("preserve_order"))) (k 0)))) (h "1h5ypv1wypq1py0njxcvzp35wkc4d9fi4769bajm72hp32y4dygy")))

(define-public crate-config2args-0.1.1 (c (n "config2args") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (f (quote ("preserve_order"))) (k 0)))) (h "12nks35z3xcm0pachfmj0sqrhj28h1qyjv1qafcrkqwwh1f8wqhk")))

(define-public crate-config2args-0.2.0 (c (n "config2args") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (f (quote ("preserve_order"))) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "06fx6inldfkds8iz26qq41kw5dymqk9349vg1a75zvagca78f1fp")))

