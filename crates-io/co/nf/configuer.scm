(define-module (crates-io co nf configuer) #:use-module (crates-io))

(define-public crate-configuer-0.1.0 (c (n "configuer") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fffdsp8calwibkk5hyy6d97mgir4sc750qkdxfnmq9svrwxakh5") (f (quote (("default" "dirs"))))))

(define-public crate-configuer-0.1.1 (c (n "configuer") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "09pf0n5w71hjmf3zf2qgilwmp19b10iajhvdw1jfv39cn6wx8dfc") (f (quote (("default" "dirs"))))))

(define-public crate-configuer-0.1.11 (c (n "configuer") (v "0.1.11") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yy87r6hncjzhwx9zjm75p12vfyj8fva2jk6wzahknwcfd7dsjgl") (f (quote (("default" "dirs"))))))

(define-public crate-configuer-0.1.12 (c (n "configuer") (v "0.1.12") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "071rppskhwi6ldw0bysqh3fa8hw1bsn7aj0dchcsss7b3wdzivhb") (f (quote (("default" "dirs-next"))))))

