(define-module (crates-io co nf configr_derive) #:use-module (crates-io))

(define-public crate-configr_derive-0.5.0 (c (n "configr_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sbgvyya2my8v4hy9ia4px7k9b92xrcgp07lx24bxf7yzf4l27zk")))

(define-public crate-configr_derive-0.6.0 (c (n "configr_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "164jf426lqnlyvk12wvzi2pn3kq0aldq0d5ad7pqbz2a4k737qy0")))

(define-public crate-configr_derive-0.6.5 (c (n "configr_derive") (v "0.6.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cd0i4ipcyp5k3qjxb0pxjvblk7n78565idif9ci8b518wxanxmz")))

(define-public crate-configr_derive-0.6.6 (c (n "configr_derive") (v "0.6.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lwd3wq5ipv2n3cwinmw7pyz1x180d3376s522dnl2px2r0bna7d")))

(define-public crate-configr_derive-0.8.0 (c (n "configr_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02yc45ws9n2x2l8pa1wq8c6a8xa8dmhx8riw7labibd5132cnri8")))

