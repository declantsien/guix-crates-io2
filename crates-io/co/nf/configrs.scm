(define-module (crates-io co nf configrs) #:use-module (crates-io))

(define-public crate-configrs-0.0.0 (c (n "configrs") (v "0.0.0") (h "05jai95gx7r8n5s5s2rfc8ygwhy4mn5dpys7fbrqk8c01r0dsasp")))

(define-public crate-configrs-0.1.0 (c (n "configrs") (v "0.1.0") (d (list (d (n "env-file-reader") (r "0.3.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "0.9.*") (d #t) (k 0)) (d (n "toml") (r "0.8.*") (d #t) (k 0)))) (h "0d1xy7cb5l9cwy29k6c8a41abpgrdbmaxxwkwx9x26aa0qg0wg9y")))

(define-public crate-configrs-0.1.1 (c (n "configrs") (v "0.1.1") (d (list (d (n "env-file-reader") (r "0.3.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "0.9.*") (d #t) (k 0)) (d (n "toml") (r "0.8.*") (d #t) (k 0)))) (h "1rnzpcp30lvax7154zn1zvxpg48ygn7045ffh3fv75cliz0pkb1i")))

(define-public crate-configrs-0.1.2 (c (n "configrs") (v "0.1.2") (d (list (d (n "env-file-reader") (r "0.3.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "0.9.*") (d #t) (k 0)) (d (n "toml") (r "0.8.*") (d #t) (k 0)))) (h "0q44nhgrrlfb1fr0r1h3xkfyx6va92li63zgm5pind6ls2y9aqzk")))

(define-public crate-configrs-0.1.3 (c (n "configrs") (v "0.1.3") (d (list (d (n "env-file-reader") (r "0.3.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "0.9.*") (d #t) (k 0)) (d (n "toml") (r "0.8.*") (d #t) (k 0)))) (h "1sc31p5vb2bi7yixvklj9fc3jkwgi5zp93pbrzhq2s8x0baw501j")))

(define-public crate-configrs-0.1.4 (c (n "configrs") (v "0.1.4") (d (list (d (n "env-file-reader") (r "0.3.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "0.9.*") (d #t) (k 0)) (d (n "toml") (r "0.8.*") (d #t) (k 0)))) (h "0610mwls5pkfyyjk7155rm2b003z8a8bjclcgvjahfbqa89r6pjh")))

