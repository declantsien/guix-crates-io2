(define-module (crates-io co nf config-gen-macro-impl) #:use-module (crates-io))

(define-public crate-config-gen-macro-impl-0.0.1 (c (n "config-gen-macro-impl") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "199d332132y9mh9kv7qh7rmfh44adhbyhazmi8xwvnzjkhv0hrmx")))

(define-public crate-config-gen-macro-impl-0.1.0 (c (n "config-gen-macro-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "0jgxhpw8qvj5xlynsr1dqhgrc0i7x1pjrrvlgmbzdmidyk9r2c38")))

(define-public crate-config-gen-macro-impl-0.1.1 (c (n "config-gen-macro-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "09fhbw992bc6j8i240xq5dvci8iwi20s0hbddyx84zyh8h02gjgg")))

(define-public crate-config-gen-macro-impl-0.1.2 (c (n "config-gen-macro-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "0vfsj53l1gw34mckfx9fzdf82hgrqbc4kpcbz4hkrvw86z2p0djn")))

(define-public crate-config-gen-macro-impl-0.1.3 (c (n "config-gen-macro-impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 2)))) (h "18n1xgfviyz1vz40l1dawjawxxdh731wmsj3amqqjvddw16bvw9g") (f (quote (("load_toml"))))))

