(define-module (crates-io co nf config-diag) #:use-module (crates-io))

(define-public crate-config-diag-1.0.0 (c (n "config-diag") (v "1.0.0") (h "0rb937x2bczrj5ny1572xcp6hlby74psivq3g3zakanr1aw7226k")))

(define-public crate-config-diag-1.0.1 (c (n "config-diag") (v "1.0.1") (h "1f4s6npzkx0205mqpqjs6820mrj5n3m9xaqh48dh2rnlg33yhnfh")))

(define-public crate-config-diag-1.0.2 (c (n "config-diag") (v "1.0.2") (h "1ymdzapwhichv3dgg1m784ks6sblifkvanflia490qlywfyhdjxf")))

