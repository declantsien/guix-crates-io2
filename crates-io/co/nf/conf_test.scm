(define-module (crates-io co nf conf_test) #:use-module (crates-io))

(define-public crate-conf_test-0.1.0 (c (n "conf_test") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "12w80fy1hfrbbfp5j0dgnjsp72s8f1hlyylsmn7q4k2b4zrbmf51") (y #t)))

(define-public crate-conf_test-0.1.1 (c (n "conf_test") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "19ziaqygwba09axi9kvznivsnlkv44dqrpagz4ap9c1j0xsi59jf") (y #t)))

(define-public crate-conf_test-0.1.2 (c (n "conf_test") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "0shk1rqzwl0jhh1kxiha294xgxvl17ff6hbjdh869jajp4f24w3p")))

(define-public crate-conf_test-0.2.0 (c (n "conf_test") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "19pd0bc8vv0kyzlh9v5rsjjsm5279kxql4qi3psmqnr2ciy8svhw")))

(define-public crate-conf_test-0.2.1 (c (n "conf_test") (v "0.2.1") (d (list (d (n "cargo_metadata") (r ">=0.13, <=0.15") (d #t) (k 0)))) (h "0b6i0z46qm28blcvdgg6hb19d72w8cp95jb4ispqk9pc44644s0a")))

(define-public crate-conf_test-0.3.0 (c (n "conf_test") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)))) (h "0pmj3xacj027q24vjndf21xq482k6769xafaxbwzw146wmp3jazc")))

(define-public crate-conf_test-0.3.1 (c (n "conf_test") (v "0.3.1") (d (list (d (n "cargo_metadata") (r ">=0.13, <=0.16") (d #t) (k 0)))) (h "14mdsnixh3280jbaiffvxhysjsyjcz3zxhpziqdm6ckg091z0si8")))

(define-public crate-conf_test-0.3.2 (c (n "conf_test") (v "0.3.2") (d (list (d (n "cargo_metadata") (r ">=0.13, <=0.16") (d #t) (k 0)))) (h "10j115c36a0rzf1pfn3gimk4py1qy59zkaccgav3rhhaisgbhgb3")))

(define-public crate-conf_test-0.4.0 (c (n "conf_test") (v "0.4.0") (d (list (d (n "cargo_metadata") (r ">=0.13, <=0.16") (d #t) (k 0)))) (h "0mknassb01c6xj5svpckrgq16z4af2pvivbqxd8hl54577nh0vx1")))

(define-public crate-conf_test-0.5.0 (c (n "conf_test") (v "0.5.0") (d (list (d (n "cargo_metadata") (r ">=0.13, <=0.16") (d #t) (k 0)))) (h "1f2vw7wqc86jk0600k4rkr6j3pjy076x16ljvi8djjj49f73bs58")))

