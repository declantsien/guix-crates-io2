(define-module (crates-io co nf configured) #:use-module (crates-io))

(define-public crate-configured-0.2.0 (c (n "configured") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "config") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0chhcfxpnffzf78azm7fgry4q4flba21p2bm78ilkjyivqrirrsj") (r "1.61")))

(define-public crate-configured-0.3.0 (c (n "configured") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "config") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1vnzdjr1pmvx4zlzchl96qry6v25f9n1k23n3gp7r4ang7clqbra") (r "1.61")))

(define-public crate-configured-0.4.0 (c (n "configured") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "config") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0wdyyqvhsxv2hs4lv37p62slmzag9jxvq0dy0k8yf33i6r9b2nz6")))

(define-public crate-configured-0.5.0 (c (n "configured") (v "0.5.0") (d (list (d (n "config") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gsp7p8rsl8x9d10s3dh6ibxd0695jh0rcmksvbircai4rzj4nzp")))

(define-public crate-configured-0.6.0 (c (n "configured") (v "0.6.0") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "120y8knm2bh7q0xq4kfvxs7wnwv6c7jmksjs9svq2qk1yv58v13x")))

(define-public crate-configured-0.6.2 (c (n "configured") (v "0.6.2") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1swisg1wxz8v17kklhsyf3cyfrdg00zhr8rgpmky031m72i5clj8")))

(define-public crate-configured-0.7.0 (c (n "configured") (v "0.7.0") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bfnh4dj5wccb0hak0wvn4vjk08l2lzm0ly233kn9nmcvaq7wpc8")))

(define-public crate-configured-0.7.1 (c (n "configured") (v "0.7.1") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kxkigzialsvcnl0pd95gizjjh587x90la1x5qa722rsvp4pq4ba")))

