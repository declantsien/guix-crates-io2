(define-module (crates-io co nf confyg) #:use-module (crates-io))

(define-public crate-confyg-0.0.0 (c (n "confyg") (v "0.0.0") (d (list (d (n "config") (r "^0.13.2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1fwrh17yrkmxzas7arr6qgz38hd5rywy3271cmakwm8klnaq8qb1")))

(define-public crate-confyg-0.1.0 (c (n "confyg") (v "0.1.0") (d (list (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "05lyv2af6ajr4wav03iid8am2w5xyq2z5ggmnrp1b631rwqjsjv1")))

(define-public crate-confyg-0.1.1 (c (n "confyg") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1n3cnf41pnj2m7ixyr3cr423m1rm52lqz0lb4lfn282rpc9800w0")))

(define-public crate-confyg-0.1.2 (c (n "confyg") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0x1i3znlw8lgyfc0x8dd3jgmzn9lc3h68l100fp05dyyc4l7hp4j")))

(define-public crate-confyg-0.1.3 (c (n "confyg") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0qpqlvqbvji18j89spilhysy0nzxnzd2l5mzwiyr5g6cp1fjab0f")))

(define-public crate-confyg-0.1.4 (c (n "confyg") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0vrjczqsk8kwba9b372aih4lxdrgzp8xqir109rfxqgy5bmj15y9")))

(define-public crate-confyg-0.2.0 (c (n "confyg") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0p2hcjfwx4bmprv7sz96azp3x59aznkb18la0nb2xdjf1wnvh95g")))

