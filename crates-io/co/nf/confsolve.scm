(define-module (crates-io co nf confsolve) #:use-module (crates-io))

(define-public crate-confsolve-0.1.0 (c (n "confsolve") (v "0.1.0") (h "05bn3brgyankcv56lmf8ig5h6z8smp3a366pk22hbk4l8nwfsajg")))

(define-public crate-confsolve-0.1.1 (c (n "confsolve") (v "0.1.1") (h "14ljh0dxi11760sylgd2s2qys6d269gbq01y7r0m2i099zck7awf")))

(define-public crate-confsolve-0.1.2 (c (n "confsolve") (v "0.1.2") (h "09qz76j1pxkg0f2qrcinskbixgj3nhj711m6l8qjr9ilfw11vghk")))

(define-public crate-confsolve-0.1.3 (c (n "confsolve") (v "0.1.3") (h "1qp8gwwxp1zk8m1nx3yfzgdwp4vp3qqan3370xrn9dw19whf45qn")))

(define-public crate-confsolve-0.1.4 (c (n "confsolve") (v "0.1.4") (h "1xjcvslk6pz6fil21jh52dgr9whgga0hjbs3b8rrsjkg8bafl86j")))

(define-public crate-confsolve-0.1.5 (c (n "confsolve") (v "0.1.5") (d (list (d (n "term") (r "~0.2.2") (d #t) (k 0)))) (h "1f8yzrl5rvzv757ad9a53dw0axlf2bjhlfx1b35im31ximqdkxn3")))

(define-public crate-confsolve-0.2.0 (c (n "confsolve") (v "0.2.0") (h "1rpr7qb8q8wks7974qbgp7b6s5bzaikwv6kj8zla9ifqhg1gs22j")))

(define-public crate-confsolve-0.2.1 (c (n "confsolve") (v "0.2.1") (h "0kwipjdy1jkvd5pdfnffbsbbnzvylq32qj2l2d55kpy16z7wrkmv")))

(define-public crate-confsolve-0.3.0 (c (n "confsolve") (v "0.3.0") (h "1lxkl6y7x8qygn4hvbb1gx542h3h1gfas4mm6yap05qkqajq7378")))

