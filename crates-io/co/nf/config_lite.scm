(define-module (crates-io co nf config_lite) #:use-module (crates-io))

(define-public crate-config_lite-1.0.0-beta (c (n "config_lite") (v "1.0.0-beta") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0mvy7kra2dwc1i71j552ak3rn2vl78hdb579qmbygrgxh4rqy323")))

(define-public crate-config_lite-2.0.0-beta (c (n "config_lite") (v "2.0.0-beta") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1j0mz62aqcrjfxq5vvvn13zpha1ajlziwj0hxb4sj3pfw0cwp9r5")))

(define-public crate-config_lite-2.1.0-beta (c (n "config_lite") (v "2.1.0-beta") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1prd7awq2rzrkjllib2h79wpkb8mqrpw920j5dlcn924shi28p5h")))

