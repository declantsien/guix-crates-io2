(define-module (crates-io co nf confu) #:use-module (crates-io))

(define-public crate-confu-0.1.0 (c (n "confu") (v "0.1.0") (d (list (d (n "confu_derive") (r "=0.1.0") (d #t) (k 0)))) (h "1ri7a84xrxzchc81qb6baci4bx6jw7iyszi1xkpgmwbcfhkyj000")))

(define-public crate-confu-0.1.1 (c (n "confu") (v "0.1.1") (d (list (d (n "confu_derive") (r "=0.1.1") (d #t) (k 0)))) (h "12g0p72lsjqyb69wdbakfxsdpc7i56si5iqllgaa011y2jgdmhy3")))

(define-public crate-confu-0.1.2 (c (n "confu") (v "0.1.2") (d (list (d (n "confu_derive") (r "=0.1.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0iyfk94ghz8djg1s8jig14cvzj8cx39n7fhl2jcfgizd53wsf65l")))

(define-public crate-confu-0.1.3 (c (n "confu") (v "0.1.3") (d (list (d (n "confu_derive") (r "=0.1.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1jwv0djpvan80q35qjb1p7w0lj5dna283q748c884w3g58jfqpng")))

