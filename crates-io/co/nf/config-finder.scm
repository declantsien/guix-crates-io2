(define-module (crates-io co nf config-finder) #:use-module (crates-io))

(define-public crate-config-finder-0.1.0 (c (n "config-finder") (v "0.1.0") (d (list (d (n "dirs-sys") (r "^0.3.7") (d #t) (k 0)))) (h "1g6ncm30806gfiqxhgizfk73ygbjs08l4gf13n70a3dm7lx4bg6v") (y #t) (r "1.56.0")))

(define-public crate-config-finder-0.1.1 (c (n "config-finder") (v "0.1.1") (d (list (d (n "dirs-sys") (r "^0.3.7") (d #t) (k 0)))) (h "048n82jinz42w3lp8nizwn51irdw3z984fbpjjixdw77gy4fasjf") (y #t) (r "1.56.0")))

(define-public crate-config-finder-0.1.2 (c (n "config-finder") (v "0.1.2") (d (list (d (n "dirs-sys") (r "^0.3.7") (d #t) (k 0)))) (h "01ryhg4zizy2v2r746pnq56pm6gbh2hzv10yq3g0d97xgy6yv7jw") (r "1.56.0")))

