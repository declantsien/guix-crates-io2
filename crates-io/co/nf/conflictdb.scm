(define-module (crates-io co nf conflictdb) #:use-module (crates-io))

(define-public crate-conflictdb-0.1.0 (c (n "conflictdb") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1yz2xnsq5ga5qlkxly2m2h3v5r7qgyzqb2fs4bcnb9snl142k198")))

