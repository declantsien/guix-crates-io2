(define-module (crates-io co nf confindent) #:use-module (crates-io))

(define-public crate-confindent-0.1.0 (c (n "confindent") (v "0.1.0") (h "1s6ivpzgaz1p8xrl1dpirvxcm5ww4q8xdf0zh1ajhiygm5sdkcjd")))

(define-public crate-confindent-0.2.0 (c (n "confindent") (v "0.2.0") (h "00ijhkbgrbpcb5b3q5n1adh6hxw0575z6rdgmmkvjnffhd9w40xi")))

(define-public crate-confindent-1.0.0 (c (n "confindent") (v "1.0.0") (h "00053y9xls2pq9sn09syf0pszkpxc2q387w4llhjmz92vrrbh1zh")))

(define-public crate-confindent-1.1.0 (c (n "confindent") (v "1.1.0") (h "0bbm68zidb8a39z07ckc8bdp7an5ip2xajnxarxfah0g516g9g0f")))

(define-public crate-confindent-0.3.1 (c (n "confindent") (v "0.3.1") (h "1z2jpxnvl7pp3g1m0pqsfcf6ybvd3gjx887lyd0sszbclkx6j00s")))

(define-public crate-confindent-1.1.1 (c (n "confindent") (v "1.1.1") (h "16m25agair6a336ph9ld7dv12sddpbfks7ly8p3imjki7cmqr4q6")))

(define-public crate-confindent-2.0.0 (c (n "confindent") (v "2.0.0") (h "055m1iadvka8m2fsxdsydgciychkmh4m2nmarlhmzjv7d39ngnaw")))

(define-public crate-confindent-2.0.1 (c (n "confindent") (v "2.0.1") (h "17icprb3apq12ws9cs28s0285gkvdfh4kgkkj03wk4ba73a75phv")))

(define-public crate-confindent-2.1.0 (c (n "confindent") (v "2.1.0") (h "00hmmaczfcih2j76wh93vazxxblb5pgy4hklvcfvw06w39c3rq0h")))

(define-public crate-confindent-2.2.0 (c (n "confindent") (v "2.2.0") (h "1c9zynrc1wv6ik70pf9lg2mjlp979bvq7c51n3sm7dp0v37nx865")))

