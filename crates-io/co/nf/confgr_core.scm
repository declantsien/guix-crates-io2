(define-module (crates-io co nf confgr_core) #:use-module (crates-io))

(define-public crate-confgr_core-0.1.0 (c (n "confgr_core") (v "0.1.0") (h "078zs32gwnfldlc74qb2zirrwk4z4dingv2hca8yl5inkd6bcy3g")))

(define-public crate-confgr_core-0.1.1 (c (n "confgr_core") (v "0.1.1") (h "0bchs96jczilpgs6zgmi1j3fsn8gxlhsmhpzxs690z185bzp4l6i")))

(define-public crate-confgr_core-0.2.0 (c (n "confgr_core") (v "0.2.0") (d (list (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1qnl2a54zwh9p5ig09dcmkfzwwva236bc4iv6mqd28n2rxw8ay87")))

(define-public crate-confgr_core-0.2.1 (c (n "confgr_core") (v "0.2.1") (d (list (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "183fia9p18nr2csn2jn72rhvd7r7s36b29bcxjvgmngm79bhzggk")))

