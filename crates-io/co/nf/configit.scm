(define-module (crates-io co nf configit) #:use-module (crates-io))

(define-public crate-configit-0.1.0 (c (n "configit") (v "0.1.0") (h "0xxaxkanid208zvay7s1hn8vqd0iadjlq1znqpz6q12731a9fy31")))

(define-public crate-configit-0.1.1 (c (n "configit") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0bhx7aknr0ls9rac58pikycdjaigd5mr978i17s0zc7llka0mir3") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("default" "toml_conf"))))))

(define-public crate-configit-0.1.2 (c (n "configit") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0zimk0f25l11mf0wdh9jq7dk1dndchfy6gs1b372x28jln8qznzb") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("default" "toml_conf"))))))

(define-public crate-configit-0.1.3 (c (n "configit") (v "0.1.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1v7m4k93if2n8011s2lpz2n26s5v1rw7lrg3j2habbk25d26wijp") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("default" "toml_conf"))))))

(define-public crate-configit-0.1.4 (c (n "configit") (v "0.1.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1rgr45k2lcazlxzzpk1iah5b7i320rkw6r9pkibc7km4y7sq8xh0") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("default" "toml_conf"))))))

(define-public crate-configit-0.2.0 (c (n "configit") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "005zmnwsx94lmqvq4ly8w7yw0669dkf80dbpcf38s5n320igya68") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("default" "toml_conf"))))))

(define-public crate-configit-0.3.0 (c (n "configit") (v "0.3.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1j0qqy55d4lsy7k4744yavwga051ih7qvpfw7m9gaz768kbma54n") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("default" "toml_conf"))))))

(define-public crate-configit-0.3.1 (c (n "configit") (v "0.3.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1zdbak040d6qz7zf8jjqpg04zq5xls196cibag26x35fad9d0h11") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("default" "toml_conf"))))))

