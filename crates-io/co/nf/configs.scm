(define-module (crates-io co nf configs) #:use-module (crates-io))

(define-public crate-configs-0.1.0 (c (n "configs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 0)))) (h "1sh71azhlxi85jrh7d8v3mfwn9mbw10jybgvc5mh5y03h0xysbd6")))

(define-public crate-configs-0.1.1 (c (n "configs") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 0)))) (h "1ljfhf92c5hcyg5i1lq43l4v7idyknbkcjq3dm7lvq8k8a505ppa")))

