(define-module (crates-io co nf configuro) #:use-module (crates-io))

(define-public crate-configuro-0.1.0 (c (n "configuro") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1qy0ak4gnhcmwh5djjww2za5cjsylzc4ln8xdqpnnranmlg37vjg")))

