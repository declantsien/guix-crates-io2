(define-module (crates-io co nf config-loader) #:use-module (crates-io))

(define-public crate-config-loader-1.1.0 (c (n "config-loader") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0m8qd2dni491zb89w5ijk7v6j96n3x9z84qljbna337rj2sxrifs")))

(define-public crate-config-loader-1.1.1 (c (n "config-loader") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0vpw10pwz8k5xqs8j65qjsyvc2fc1zdb5w820f5407ylf6g4jk1d") (y #t)))

(define-public crate-config-loader-1.1.2 (c (n "config-loader") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0qf7ynsqgh3hg08a0q4hifp0jl70hwxi1qasrk0lgi45vna3gwph")))

(define-public crate-config-loader-2.2.3 (c (n "config-loader") (v "2.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.5") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0zc74j529a23fjxqbyfy62awf84dq2z3kxjm48yclx0yvsmkm4xj")))

