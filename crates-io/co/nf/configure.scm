(define-module (crates-io co nf configure) #:use-module (crates-io))

(define-public crate-configure-0.0.1 (c (n "configure") (v "0.0.1") (d (list (d (n "erased-serde") (r "^0.3.3") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 2)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0frggdzg6hk685xk6c30ddngpji36w0j44s0yhyaks9gxa7svfkk")))

(define-public crate-configure-0.1.0 (c (n "configure") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.3.3") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 2)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1152nb53kl9xl0f2day63gg14vm2xlijby9pqqlhvp95fgq2smig")))

(define-public crate-configure-0.1.1 (c (n "configure") (v "0.1.1") (d (list (d (n "configure_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3.3") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 2)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0h96h9apfmz47q8ik6270y61qzy39bh7sfas7jp95c2v2d4783qq")))

