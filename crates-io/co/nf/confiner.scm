(define-module (crates-io co nf confiner) #:use-module (crates-io))

(define-public crate-confiner-0.2.0 (c (n "confiner") (v "0.2.0") (d (list (d (n "peg") (r "^0.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "100nx2lb5vwfb00cgykbc9jc0ciw01z2zraln4m9vfpb78ag4iab")))

(define-public crate-confiner-0.2.1 (c (n "confiner") (v "0.2.1") (d (list (d (n "peg") (r "^0.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vxvni364pi59zgxdifw2vlc4rq5zlnff4fp6g58d6bxisjr5dc3")))

