(define-module (crates-io co nf conform) #:use-module (crates-io))

(define-public crate-conform-0.0.1 (c (n "conform") (v "0.0.1") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)))) (h "0731mghww79apsas6d5696nfpcs8ijwv3i3yr5pfg1xd1wkiw9rf")))

(define-public crate-conform-0.0.2 (c (n "conform") (v "0.0.2") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)))) (h "1cdwpv6b4ajmb8ww5pgvynjdff63mwz91d1hz30lmsxvicgcvlw8")))

