(define-module (crates-io co nf config-parser) #:use-module (crates-io))

(define-public crate-config-parser-0.1.0 (c (n "config-parser") (v "0.1.0") (h "0lqd43frsx3w0xffjg7d9lyyglh006wpw066v5ylv45w2li7c3km")))

(define-public crate-config-parser-0.1.1 (c (n "config-parser") (v "0.1.1") (h "1z9n10cqkq97i1s2h5mkhqzpq3qlx691k2gkr9ivwxx53pbr9sqd")))

(define-public crate-config-parser-0.1.2 (c (n "config-parser") (v "0.1.2") (h "0p7bsqz6bz8vjaw0ngq6rlk17vy1jwn62f5762dbzcrj2jga4z22")))

