(define-module (crates-io co nf config-secret) #:use-module (crates-io))

(define-public crate-config-secret-0.1.0 (c (n "config-secret") (v "0.1.0") (d (list (d (n "config") (r ">=0.13") (k 0)) (d (n "config") (r "^0.13") (f (quote ("json" "yaml"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "temp-env") (r "^0.3") (d #t) (k 2)))) (h "0dynilv9phq7lwsrg8r4ks3j22j8is5w0qs7ivafb16j72rc1smm")))

