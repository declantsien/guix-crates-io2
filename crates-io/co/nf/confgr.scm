(define-module (crates-io co nf confgr) #:use-module (crates-io))

(define-public crate-confgr-0.1.0 (c (n "confgr") (v "0.1.0") (d (list (d (n "confgr_core") (r "^0.1.0") (d #t) (k 0)) (d (n "confgr_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "config") (r "^0.14.0") (f (quote ("toml"))) (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "smart-default") (r "^0.7.1") (d #t) (k 2)))) (h "1z7rk2gw9kzq18zb0zwq0wwb9ykask5zl719z36c1yjwa9lpi72v")))

(define-public crate-confgr-0.1.1 (c (n "confgr") (v "0.1.1") (d (list (d (n "confgr_core") (r "^0.1.1") (d #t) (k 0)) (d (n "confgr_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "config") (r "^0.14.0") (f (quote ("toml"))) (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 2)) (d (n "smart-default") (r "^0.7.1") (d #t) (k 2)))) (h "0d4pvklwxpiikclk8dmrpv341zipl5pzac7d553f7knz8cz2krds")))

(define-public crate-confgr-0.2.0 (c (n "confgr") (v "0.2.0") (d (list (d (n "confgr_core") (r "^0.2.0") (d #t) (k 0)) (d (n "confgr_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 2)) (d (n "smart-default") (r "^0.7.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "0h96gpr4cpznhizgw6x7szb65cb9lmgjh2ki6wapjvjdl5cichlp")))

(define-public crate-confgr-0.2.1 (c (n "confgr") (v "0.2.1") (d (list (d (n "confgr_core") (r "^0.2.1") (d #t) (k 0)) (d (n "confgr_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 2)) (d (n "smart-default") (r "^0.7.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "0kw1pfwginxn1z2nwfl987nf871m52yl85y9apk26235kvlyzx9q")))

