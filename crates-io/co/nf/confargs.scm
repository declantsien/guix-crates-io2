(define-module (crates-io co nf confargs) #:use-module (crates-io))

(define-public crate-confargs-0.1.0 (c (n "confargs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^3.2.3") (f (quote ("derive" "std"))) (k 2)) (d (n "tempfile") (r "^3.3.0") (k 2)) (d (n "toml") (r "^0.5.9") (k 0)))) (h "1wnh69mzbsvziwjnnrcv38imja8fl7240lprz3pkmw718w0mk1i8")))

(define-public crate-confargs-0.1.1 (c (n "confargs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^3.2.3") (f (quote ("derive" "std"))) (k 2)) (d (n "tempfile") (r "^3.3.0") (k 2)) (d (n "toml") (r "^0.5.9") (k 0)))) (h "0pvm98khq8jkq9n06cf65gb36zbzlsfa47gjz0rc5v34rl80v97b")))

(define-public crate-confargs-0.1.2 (c (n "confargs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.57") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^3.2.3") (f (quote ("derive" "std"))) (k 2)) (d (n "tempfile") (r "^3.3.0") (k 2)) (d (n "toml") (r "^0.5.9") (k 0)))) (h "0qnfw9f4b5v76hqkqbyrm0z49jixpqrwb4hnn9svs288v465f7wk")))

(define-public crate-confargs-0.1.3 (c (n "confargs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.57") (f (quote ("std"))) (k 0)) (d (n "clap") (r "^3.2.3") (f (quote ("derive" "std"))) (k 2)) (d (n "tempfile") (r "^3.3.0") (k 2)) (d (n "toml") (r "^0.5.9") (k 0)))) (h "0yjqcvvhms3khbrw5955j4qd0lq1d7jhrscmn2fd63gp4qw659qf")))

