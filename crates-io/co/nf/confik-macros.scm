(define-module (crates-io co nf confik-macros) #:use-module (crates-io))

(define-public crate-confik-macros-0.0.1 (c (n "confik-macros") (v "0.0.1") (h "0kzq7kn5f82vs56x87p8ic28p5alnj3d4lkry4vyrlqd3ndk0px5")))

(define-public crate-confik-macros-0.7.0 (c (n "confik-macros") (v "0.7.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rzzfnl5iylfbjv03i4fp8vm3xjzb0s5xpr3xdb55bfwch8ix5vj") (r "1.66")))

(define-public crate-confik-macros-0.8.0 (c (n "confik-macros") (v "0.8.0") (d (list (d (n "darling") (r "^0.20.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04j6wq2fc2fbdyq1r68qrqmbnvgs00plxw1y4dqzzkmqf3s89ssl") (r "1.66")))

(define-public crate-confik-macros-0.9.0 (c (n "confik-macros") (v "0.9.0") (d (list (d (n "darling") (r "^0.20.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xpgbw57gvhcmv7v4flh0w4g3c15l36bzs9q8yn5wdh057mlbg2d") (r "1.66")))

(define-public crate-confik-macros-0.10.0 (c (n "confik-macros") (v "0.10.0") (d (list (d (n "darling") (r "^0.20.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "19rgin8lrj9dipiywgw9klncvk5sj6xnn7v2hnygk8arhwsfz6hs") (r "1.66")))

(define-public crate-confik-macros-0.11.0 (c (n "confik-macros") (v "0.11.0") (d (list (d (n "darling") (r "^0.20.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kqgz54135x254453qgs3wpya66w5svh96w3nid31al633rqkpyn") (r "1.66")))

(define-public crate-confik-macros-0.11.1 (c (n "confik-macros") (v "0.11.1") (d (list (d (n "darling") (r "^0.20.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0d5xisl4k5v29ma86y682cjcbzri5ywb8ir8i267hv5vrxk5aw6p") (r "1.67")))

