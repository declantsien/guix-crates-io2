(define-module (crates-io co nf confu_derive) #:use-module (crates-io))

(define-public crate-confu_derive-0.1.0 (c (n "confu_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1pdwks9a55pqsf0mw5nijylmd08wgz4qh2cj18qznaxjdxa99cjk")))

(define-public crate-confu_derive-0.1.1 (c (n "confu_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1j1vj0lmxw4cxmq7riibfp9cdhppsyvs3kip5d1sm5i2czssx36k")))

(define-public crate-confu_derive-0.1.2 (c (n "confu_derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0752lgwb6xsvnyfr746hw8s2f0j0knkh8fgqnf15j5y37qjv9hi9")))

(define-public crate-confu_derive-0.1.3 (c (n "confu_derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0q9grnnzkjk0hcrjsbwy747cwdsrhmq8fqclc4j08d3k6drd9rk7")))

