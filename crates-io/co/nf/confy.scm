(define-module (crates-io co nf confy) #:use-module (crates-io))

(define-public crate-confy-0.1.0 (c (n "confy") (v "0.1.0") (d (list (d (n "toml") (r "^0.1.27") (d #t) (k 0)))) (h "14qpw27pbpm7f5r00mz07l9ylik63xqdg80zigapa2p4mpc9i0j6")))

(define-public crate-confy-0.2.0 (c (n "confy") (v "0.2.0") (d (list (d (n "toml") (r "^0.1.27") (d #t) (k 0)))) (h "0igv37lyll1y2svg8xad450v8zvbqlknp71d9aprxjh2p2gf7s5l")))

(define-public crate-confy-0.3.0 (c (n "confy") (v "0.3.0") (d (list (d (n "directories") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1v06bmx6wfa4cwqc2ph4zwlm6jy460gyz81xs2x7syx6g3wbaa2g") (f (quote (("sd" "serde_derive"))))))

(define-public crate-confy-0.3.1 (c (n "confy") (v "0.3.1") (d (list (d (n "directories") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1spddm0kpirn94m0jw25c7gy5llq2cp4knffqz92fzxf1p6h052b") (f (quote (("sd" "serde_derive"))))))

(define-public crate-confy-0.4.0 (c (n "confy") (v "0.4.0") (d (list (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "15v6g3k1jj5g966c2nlc5klim06yl287zw9i1y54ks790h14f4r9")))

(define-public crate-confy-0.5.0 (c (n "confy") (v "0.5.0") (d (list (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0xc2nfv51knzq9iw2n3k12w7109injs2pl8pc1q8bm4sbl3f6sah") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("default" "toml_conf"))))))

(define-public crate-confy-0.5.1 (c (n "confy") (v "0.5.1") (d (list (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0b1d1ckqm8lx50phsk4b0vlflpipvrzz799i36mcyp8l6p5nhxp3") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("ron_conf" "ron") ("default" "toml_conf"))))))

(define-public crate-confy-0.6.0 (c (n "confy") (v "0.6.0") (d (list (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (o #t) (d #t) (k 0)))) (h "19a8n8szrdx3cc8hmbxafxsx4c8888if4a2cha9l1vf6fp29dlhm") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("ron_conf" "ron") ("default" "toml_conf"))))))

(define-public crate-confy-0.6.1 (c (n "confy") (v "0.6.1") (d (list (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (o #t) (d #t) (k 0)))) (h "18795hfza7g0jwbvldl5rammmk3jdaxq5b6w9b1pvw3h130g9ca5") (f (quote (("yaml_conf" "serde_yaml") ("toml_conf" "toml") ("ron_conf" "ron") ("default" "toml_conf"))))))

