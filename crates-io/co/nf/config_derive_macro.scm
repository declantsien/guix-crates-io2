(define-module (crates-io co nf config_derive_macro) #:use-module (crates-io))

(define-public crate-config_derive_macro-0.1.0 (c (n "config_derive_macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h7nkmab0bf7vlwqjg78gmggalknyf0r7nnlxn11xmy3yzpmsq4h") (y #t)))

