(define-module (crates-io co nf config_file_handler) #:use-module (crates-io))

(define-public crate-config_file_handler-0.0.1 (c (n "config_file_handler") (v "0.0.1") (d (list (d (n "cbor") (r "~0.3.16") (d #t) (k 0)) (d (n "memmap") (r "~0.2.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.16") (d #t) (k 0)))) (h "1r883dgsv2dqqdbp027aj3rdcg5m612hi43kcgzvhxc1qsd888r2")))

(define-public crate-config_file_handler-0.1.0 (c (n "config_file_handler") (v "0.1.0") (d (list (d (n "clippy") (r "~0.0.45") (o #t) (d #t) (k 0)) (d (n "memmap") (r "~0.2.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.18") (d #t) (k 0)))) (h "0rgnilcqbrgjr6smg15fjih5c7fzvnyrswk1knkwcp8j70v2agqm")))

(define-public crate-config_file_handler-0.2.0 (c (n "config_file_handler") (v "0.2.0") (d (list (d (n "clippy") (r "~0.0.46") (o #t) (d #t) (k 0)) (d (n "memmap") (r "~0.2.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.18") (d #t) (k 0)))) (h "03hn9bkyiwks1vr9vsm2nb1w6979nyp049hfns54x3ggfiy1g54a")))

(define-public crate-config_file_handler-0.2.1 (c (n "config_file_handler") (v "0.2.1") (d (list (d (n "clippy") (r "~0.0.46") (o #t) (d #t) (k 0)) (d (n "fs2") (r "~0.2.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.18") (d #t) (k 0)))) (h "0v9a0883jmkc2bqa4a9qmpgr54xfcaj580xj1zi6w7pmxmhph3qn")))

(define-public crate-config_file_handler-0.3.0 (c (n "config_file_handler") (v "0.3.0") (d (list (d (n "clippy") (r "~0.0.55") (o #t) (d #t) (k 0)) (d (n "fs2") (r "~0.2.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.18") (d #t) (k 0)))) (h "032rdnzskgxkmn5pbrc3axdhrcmf103wgazl9pvbacayipgpws20")))

(define-public crate-config_file_handler-0.3.1 (c (n "config_file_handler") (v "0.3.1") (d (list (d (n "clippy") (r "~0.0.55") (o #t) (d #t) (k 0)) (d (n "fs2") (r "~0.2.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.18") (d #t) (k 0)))) (h "1w03amczfk99xw58yhbqqp8fnmknn1asc1km917ry76vy06ws5f2")))

(define-public crate-config_file_handler-0.4.0 (c (n "config_file_handler") (v "0.4.0") (d (list (d (n "clippy") (r "~0.0.80") (o #t) (d #t) (k 0)) (d (n "fs2") (r "^0.2.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0ds6cpbq9akq4lqw7qz4ny8ckidn5kz5bc0lbl10sbigaxd4vy5l")))

(define-public crate-config_file_handler-0.5.0 (c (n "config_file_handler") (v "0.5.0") (d (list (d (n "fs2") (r "^0.2.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1amrzq5kmzxjqgs5f9rw0chcza4h34qgc9d8nh86w9r2jrs9r5gs")))

(define-public crate-config_file_handler-0.6.0 (c (n "config_file_handler") (v "0.6.0") (d (list (d (n "fs2") (r "^0.2.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "~0.9.11") (d #t) (k 0)) (d (n "serde_json") (r "~0.9.9") (d #t) (k 0)))) (h "1mas21jj5jcvf2vavdcq4hhl266f025869kbfr5lh5cffcln6msy")))

(define-public crate-config_file_handler-0.7.0 (c (n "config_file_handler") (v "0.7.0") (d (list (d (n "fs2") (r "^0.2.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.1") (d #t) (k 0)))) (h "1gvbbi5p2kz573ca6wb4bk0n28zw0vig1k1433sabjyif7lkc7al")))

(define-public crate-config_file_handler-0.8.0 (c (n "config_file_handler") (v "0.8.0") (d (list (d (n "fs2") (r "~0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "~1.2.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.10") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.2") (d #t) (k 0)))) (h "0nsq9vsdndl8dbzxa5mkl8ydabsghw1q5jzws0hja367y8pqbm6x") (y #t)))

(define-public crate-config_file_handler-0.8.1 (c (n "config_file_handler") (v "0.8.1") (d (list (d (n "fs2") (r "~0.4.2") (d #t) (k 0)) (d (n "quick-error") (r "~1.2.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.2") (d #t) (k 0)))) (h "0dzlc1ciaz03ldjz4f1jnz73kg7fbx81g36yydg9iwmzwfmms4l5")))

(define-public crate-config_file_handler-0.8.2 (c (n "config_file_handler") (v "0.8.2") (d (list (d (n "fs2") (r "~0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "quick-error") (r "~1.2.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.2") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1fmq5qqp7yxq63mhkcpa2rk70ia9z2nq5xg41qsrvhswlji9d9ks")))

(define-public crate-config_file_handler-0.8.3 (c (n "config_file_handler") (v "0.8.3") (d (list (d (n "fs2") (r "~0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "quick-error") (r "~1.2.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.2") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "0g0q5x71iawnizd262nfxakhsgisjw7s3cx8p1fh4dqc3nq6dkza")))

(define-public crate-config_file_handler-0.9.0 (c (n "config_file_handler") (v "0.9.0") (d (list (d (n "fs2") (r "~0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "quick-error") (r "~1.2.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.25") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.8") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1ylasz72y6z50pckdk40n1i32sp7911np1hs4kcjpwfy5pkcp2qq")))

(define-public crate-config_file_handler-0.10.0 (c (n "config_file_handler") (v "0.10.0") (d (list (d (n "fs2") (r "~0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "quick-error") (r "~1.2.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.9") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "1049bm680nz141wis2xcasq0hjk9b3fsqhbxc2bqmsc5i8r8mygc")))

(define-public crate-config_file_handler-0.11.0 (c (n "config_file_handler") (v "0.11.0") (d (list (d (n "fs2") (r "~0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "quick-error") (r "~1.2.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.9") (d #t) (k 0)) (d (n "unwrap") (r "~1.2.1") (d #t) (k 0)))) (h "1i7g0jfxfs985ffxin1xbd7isvifpv1hz9l11hf59p9d1xmk60q1")))

