(define-module (crates-io co nf configure_me) #:use-module (crates-io))

(define-public crate-configure_me-0.1.0 (c (n "configure_me") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "= 0.4.5") (d #t) (k 0)))) (h "1r9a507rvzm0zvsp0nqr5xkdvwj35jjgh93bicyn7za00fa8nasd")))

(define-public crate-configure_me-0.1.1 (c (n "configure_me") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "= 0.4.5") (d #t) (k 0)))) (h "0a8an12n0njfxfrl7dzmphwrpa5rjn9axp4cp5bdgrm2xxz2sx05")))

(define-public crate-configure_me-0.2.0 (c (n "configure_me") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "= 0.4.5") (d #t) (k 0)))) (h "05bf5pp3wp5nvi8zmqbzz3cf7v33sbv6lz404k4cxa57ycmv6cwg")))

(define-public crate-configure_me-0.2.1 (c (n "configure_me") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "= 0.4.5") (d #t) (k 0)))) (h "1c4v6i4jpnjzyckmhgf97mcalx9adjni1swks4hpndw7g7zidqbl")))

(define-public crate-configure_me-0.2.2 (c (n "configure_me") (v "0.2.2") (d (list (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "= 0.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "0hi028dr0p2yk9hycsgxai473psh39andnz9wxa5q2rjwqzkgfc2")))

(define-public crate-configure_me-0.2.3 (c (n "configure_me") (v "0.2.3") (d (list (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "= 0.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2") (d #t) (k 0)))) (h "0gpxy3z9k4wwcjdmcbw1rss564i5a3rjrv8cafdgfv7qazqnlnvp")))

(define-public crate-configure_me-0.3.0 (c (n "configure_me") (v "0.3.0") (d (list (d (n "parse_arg") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "1chw0kjplkk2b7s9qsvfrc66dn2ip74yfq4kp2bam14vh35rl6cf")))

(define-public crate-configure_me-0.3.1 (c (n "configure_me") (v "0.3.1") (d (list (d (n "parse_arg") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "0pj4ksraifrzfgjbc5alf4qf7znw879gbakm8grfnyfkkdgmg77x")))

(define-public crate-configure_me-0.3.2 (c (n "configure_me") (v "0.3.2") (d (list (d (n "parse_arg") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "1cd63liiiz6zl4mkp8ikpngs4iwyb6qlq1zd92ir35r1sracmg09")))

(define-public crate-configure_me-0.3.3 (c (n "configure_me") (v "0.3.3") (d (list (d (n "parse_arg") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "111vblnqk5xwhr1cdzs78jqs6iimwkv1r1ymv48d9xdzlrb5v3j7")))

(define-public crate-configure_me-0.3.4 (c (n "configure_me") (v "0.3.4") (d (list (d (n "parse_arg") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)))) (h "1yx96crblnmaw094vvz2hsxbd2zdbp958p7g7w81p4ljc5462x8p")))

(define-public crate-configure_me-0.4.0 (c (n "configure_me") (v "0.4.0") (d (list (d (n "parse_arg") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "17dk22avjdw9gz4fikfqflygnbnldkqxvs7yv9dqa9nrxayiyg6h")))

