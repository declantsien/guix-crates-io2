(define-module (crates-io co nf configstore) #:use-module (crates-io))

(define-public crate-configstore-0.1.0 (c (n "configstore") (v "0.1.0") (d (list (d (n "platform-dirs") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1b65pvanfda9w6ga12ykjq2lhrzpk5nnghmsphidbcn3cnpx9wyc")))

(define-public crate-configstore-0.1.1 (c (n "configstore") (v "0.1.1") (d (list (d (n "platform-dirs") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1q16z8979aajh7dcnywqrggb6rfal1in5qlr0nid5rcgrwqm4cpv")))

(define-public crate-configstore-0.1.2 (c (n "configstore") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "102fxk1lgdz1gaynh61plfff9hpdv2c7sx5s3wsx7nva3q0rlqx7")))

(define-public crate-configstore-0.1.3 (c (n "configstore") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "platform-dirs") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1a1y6h2hg1bbjqn9q8rcafwisik9kxzp1f2wy5vqc11rfs0m369l")))

