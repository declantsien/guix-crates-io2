(define-module (crates-io co nf conflagrate) #:use-module (crates-io))

(define-public crate-conflagrate-0.0.1 (c (n "conflagrate") (v "0.0.1") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r ">=0.1.52") (d #t) (k 0)) (d (n "conflagrate-macros") (r "=0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "19bq5dw7929gma2ra57q1ypfhpbsrdzzqlrlv14xqln801vjhfaa")))

(define-public crate-conflagrate-0.0.2 (c (n "conflagrate") (v "0.0.2") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r ">=0.1.52") (d #t) (k 0)) (d (n "conflagrate-macros") (r "=0.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "0skbfkj1554ykcvj28fvndvf6w75ywmvqgqpjcvr6gd8xira9nxr")))

(define-public crate-conflagrate-0.0.3 (c (n "conflagrate") (v "0.0.3") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r ">=0.1.52") (d #t) (k 0)) (d (n "conflagrate-macros") (r "=0.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "1gxvggwy8ac50zjrwnjjhy6rl99qp2z0i0l9dimv2g5hjw25jp3c")))

(define-public crate-conflagrate-0.1.0 (c (n "conflagrate") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-trait") (r ">=0.1.52") (d #t) (k 0)) (d (n "conflagrate-macros") (r "=0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "00aiblch6j4ik41v3qf5yd74z4pnpxl5gbbvz5issvld13fcz09m")))

