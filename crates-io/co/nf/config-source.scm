(define-module (crates-io co nf config-source) #:use-module (crates-io))

(define-public crate-config-source-0.1.0 (c (n "config-source") (v "0.1.0") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)))) (h "1a6fmbpfpj8hqk13gljkxnk30gq9jxsz9gifdja2g3pdq7dbl45g")))

(define-public crate-config-source-0.1.1 (c (n "config-source") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "config") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)))) (h "0wh7h2qf7iki909y2lc609rq8m2vr8ga2kal137sbz06fqkl4gv9")))

