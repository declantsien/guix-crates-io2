(define-module (crates-io co nf conf-embed) #:use-module (crates-io))

(define-public crate-conf-embed-0.1.0 (c (n "conf-embed") (v "0.1.0") (h "1fz0kmh0dy8828qnxv7kvx4s12a68jys0d44nb9as7drys3m0f93")))

(define-public crate-conf-embed-0.1.1 (c (n "conf-embed") (v "0.1.1") (h "1zli144caj77nda8q3qvz1j1y8awxnlz2qviii7symnky4shs9ah")))

(define-public crate-conf-embed-0.1.2 (c (n "conf-embed") (v "0.1.2") (h "1lal0n37qc6pcnd543sg6drf2vs3b6c2a9yjfcbr6flds9jpn12g")))

(define-public crate-conf-embed-0.1.3 (c (n "conf-embed") (v "0.1.3") (h "1rg9i8f4m90s1xb42xwjkvk6lzn5y521218iv1g7q3kh5s6sw0di")))

