(define-module (crates-io co nf confique-macro) #:use-module (crates-io))

(define-public crate-confique-macro-0.0.1 (c (n "confique-macro") (v "0.0.1") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g5kgi5156r6phhdlk9avs118gsx8lz95hif2vfh0y1mi1ci64wq")))

(define-public crate-confique-macro-0.0.2 (c (n "confique-macro") (v "0.0.2") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kfsjabsgaibmxr47027rqqpzip4hsbiiwnqfmjfdr2n2qphsrwh")))

(define-public crate-confique-macro-0.0.3 (c (n "confique-macro") (v "0.0.3") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14zl0i557di3rbnlj4ncr7fq9k35swn912rw41zcrxbkcj3znjza")))

(define-public crate-confique-macro-0.0.4 (c (n "confique-macro") (v "0.0.4") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09rfl9zryqay0qhzcgfrfrj1qlxpd3h3ln4w0nv9k3hgzhnh5i8m")))

(define-public crate-confique-macro-0.0.5 (c (n "confique-macro") (v "0.0.5") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l3b57qav06cgghahsdykigna9d4lzrmk2b60rxh1017gyrr5rsg")))

(define-public crate-confique-macro-0.0.6 (c (n "confique-macro") (v "0.0.6") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1598bdp8pbwba2ni2gd084zfdqnmyf9xdxv6cr10l6alxh5hvlqj")))

(define-public crate-confique-macro-0.0.7 (c (n "confique-macro") (v "0.0.7") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mycjispf18bw3amgkcx1298bvk2i2g2z0fv5fic10a10yqyjkhv")))

(define-public crate-confique-macro-0.0.8 (c (n "confique-macro") (v "0.0.8") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kj82kv801xhdndqns6dc6wzlsy6445m59bd0018nvgxkybv81bk")))

(define-public crate-confique-macro-0.0.9 (c (n "confique-macro") (v "0.0.9") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zmyvakm1f2qllpmvg52dgy6287s7b6020m9aiq2kidkmbdfy89q")))

