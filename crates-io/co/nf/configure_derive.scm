(define-module (crates-io co nf configure_derive) #:use-module (crates-io))

(define-public crate-configure_derive-0.0.1 (c (n "configure_derive") (v "0.0.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1s81814n4iw9vz83fzxkzr0ly6q97smxs1bad53w0q19w625bfm4")))

(define-public crate-configure_derive-0.1.0 (c (n "configure_derive") (v "0.1.0") (d (list (d (n "configure") (r "^0.1.0") (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0hd1nny9kz68j6p40ivm8pc2bzq7jskilmb2k6y1aywz4kw0sdkn")))

(define-public crate-configure_derive-0.1.1 (c (n "configure_derive") (v "0.1.1") (d (list (d (n "configure") (r "^0.1.0") (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.21") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.21") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1qrhsf63j1ybj6baigwlvsdisqk6cgl1qydb6sk3xkjhm319ql9j")))

