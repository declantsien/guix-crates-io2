(define-module (crates-io co nf confined_encoding_derive) #:use-module (crates-io))

(define-public crate-confined_encoding_derive-0.8.0 (c (n "confined_encoding_derive") (v "0.8.0") (d (list (d (n "amplify") (r "^3.13.0") (d #t) (k 2)) (d (n "amplify_syn") (r "^1.1.6") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.9.0") (d #t) (k 2)) (d (n "encoding_derive_helpers") (r "^0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1f985sqb1xqxiwlpj4bl0vg29dbddjbbvnwrvvp3s7p7placafxi") (r "1.59.0")))

(define-public crate-confined_encoding_derive-0.9.0-alpha.1 (c (n "confined_encoding_derive") (v "0.9.0-alpha.1") (d (list (d (n "amplify") (r "^3.13.0") (d #t) (k 2)) (d (n "amplify_syn") (r "^1.1.6") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.9.0") (d #t) (k 2)) (d (n "encoding_derive_helpers") (r "^0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "093s81rb16alh6ifpmmv8hcdxmbs2kaby9mf9c8aanah1ybkyidp") (r "1.59.0")))

(define-public crate-confined_encoding_derive-0.9.0-alpha.2 (c (n "confined_encoding_derive") (v "0.9.0-alpha.2") (d (list (d (n "amplify") (r "^4.0.0-alpha.3") (d #t) (k 2)) (d (n "amplify_syn") (r "^1.1.6") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.9.0") (d #t) (k 2)) (d (n "encoding_derive_helpers") (r "^2.0.0-alpha.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0yw1x96fg7nfyx8c47dimvjvzinb6ihg65p7sjr6gnil2ccf97ym") (r "1.59.0")))

