(define-module (crates-io co nf config_parser2) #:use-module (crates-io))

(define-public crate-config_parser2-0.1.0 (c (n "config_parser2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "config_parser_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "14g37im58dml2f4i0m7s23km93nplbpmidxydwqsq5asaidh7dkn")))

(define-public crate-config_parser2-0.1.1 (c (n "config_parser2") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "config_parser_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0zb1si27vgwc3snsjcl8swwn4y3xrmmcdxwg5s0by68g6wky5109")))

(define-public crate-config_parser2-0.1.2 (c (n "config_parser2") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "config_parser_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0wcczw3kx8c76xbq4fiyz3dgqyypdvwgrr67vnvfg1frl210rm2s")))

(define-public crate-config_parser2-0.1.3 (c (n "config_parser2") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "config_parser_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1p331ih3xwhqmbfg06lfgiilrcf65hbfrmcir24myg3r2345j25b")))

(define-public crate-config_parser2-0.1.4 (c (n "config_parser2") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "config_parser_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (d #t) (k 0)))) (h "127agff0xbhk4c5x88i6nqjxpl83h9v825l7as59mdassfmf8hpa")))

(define-public crate-config_parser2-0.1.5 (c (n "config_parser2") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "config_parser_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.1") (d #t) (k 0)))) (h "09v3n58gr5mgrx5jmak09wrn11cdgd8ivmcnjqk8xwml1anhx1hr")))

