(define-module (crates-io co nf conflag) #:use-module (crates-io))

(define-public crate-conflag-0.1.0 (c (n "conflag") (v "0.1.0") (d (list (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z76l0z11ngg7qqb2y440cpwy77w8xy5y7rmysnbn376bskqwzbx") (s 2) (e (quote (("serde" "dep:serde") ("python" "dep:pyo3"))))))

(define-public crate-conflag-0.1.1 (c (n "conflag") (v "0.1.1") (d (list (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14rn2lg9lfdckj84vcznk74j9mzjdnl49ix0v0q0sjiw8p93z2jm") (s 2) (e (quote (("serde" "dep:serde") ("python" "dep:pyo3"))))))

