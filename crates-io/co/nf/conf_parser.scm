(define-module (crates-io co nf conf_parser) #:use-module (crates-io))

(define-public crate-conf_parser-0.1.0 (c (n "conf_parser") (v "0.1.0") (h "149xwkc1zljr9rz4c8lp58mkbx67g82hwiwb689wc1fdh29nkgbh")))

(define-public crate-conf_parser-0.1.1 (c (n "conf_parser") (v "0.1.1") (h "1p6pgz70nmlgvfxip72hw8d51cps7m3m11dv0vqk21c099a9kssx")))

(define-public crate-conf_parser-0.1.2 (c (n "conf_parser") (v "0.1.2") (h "14gmbk8rxc1lyj2hr506bnapl2qjib8vnlkayixfnzqvyym5z7lk")))

(define-public crate-conf_parser-0.1.3 (c (n "conf_parser") (v "0.1.3") (h "0gv4jqgf75a0icgsf0k72xqb048brkacpbyb0kxkdl6wk91b4h6p")))

(define-public crate-conf_parser-0.1.4 (c (n "conf_parser") (v "0.1.4") (h "1jixnxy0br8a4hgn5kk3hsvi1h681b8x7lah608x34z48npw8ijx")))

(define-public crate-conf_parser-0.1.5 (c (n "conf_parser") (v "0.1.5") (h "03gsvi9j2hbfz6cl2r74ljykir30kvgvq0w2vlckw9pzwc7a2pb3")))

(define-public crate-conf_parser-0.1.6 (c (n "conf_parser") (v "0.1.6") (h "1arcc6sa4ai4yslmmbf2iq9jjiibsam1h6ss211bcf25rg2sl5jl")))

(define-public crate-conf_parser-0.1.7 (c (n "conf_parser") (v "0.1.7") (h "1r1xp64mfmq1sx807zz6va5crn0drw9id3cyrinh4s1a4qic2zjy")))

(define-public crate-conf_parser-0.2.0 (c (n "conf_parser") (v "0.2.0") (h "00k9bfjv5k61aa30di98h1604g547scfkb02hsw3ldix8yqcw4m5")))

