(define-module (crates-io co po copo) #:use-module (crates-io))

(define-public crate-copo-0.1.0 (c (n "copo") (v "0.1.0") (d (list (d (n "console") (r "^0.11.3") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "notifica") (r "^1.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "16zlff2gx5065a816cffpmplykz1iw2a317i9x4yhxg7amz83h98")))

(define-public crate-copo-0.1.1 (c (n "copo") (v "0.1.1") (d (list (d (n "console") (r "^0.11.3") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "notifica") (r "^1.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "11j109377igakp6vs4hq6q4p4z7plvxydxa3kyhif4k3k20lz9if")))

(define-public crate-copo-0.1.2 (c (n "copo") (v "0.1.2") (d (list (d (n "console") (r "^0.11.3") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1siadzsssy4aikfgj8y5rlikwhvqihzkc1rwh09f2k0d5apr6l1b")))

