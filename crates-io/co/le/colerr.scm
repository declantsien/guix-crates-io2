(define-module (crates-io co le colerr) #:use-module (crates-io))

(define-public crate-colerr-1.0.0 (c (n "colerr") (v "1.0.0") (d (list (d (n "docopt") (r "^0.6.18") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "mioco") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "03zsjlcs500l4n40vzjgvzwjldm3j53k1nhns4pasd579yqf70q4")))

