(define-module (crates-io co l_ col_proc_macros_impl) #:use-module (crates-io))

(define-public crate-col_proc_macros_impl-0.1.0 (c (n "col_proc_macros_impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1g6rq78nivd28x8skhjgwmm936ccdvi7rl9jf2whh8hqdvpxj5xd")))

(define-public crate-col_proc_macros_impl-0.1.2 (c (n "col_proc_macros_impl") (v "0.1.2") (d (list (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1k3b9h5kx6jmdgia45537j2n9qfxdc0g0bbm25b7wwbqy76gp6xj")))

