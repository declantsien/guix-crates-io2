(define-module (crates-io co l_ col_proc_macros) #:use-module (crates-io))

(define-public crate-col_proc_macros-0.1.0 (c (n "col_proc_macros") (v "0.1.0") (d (list (d (n "col_proc_macros_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro-nested") (r "^0.1.4") (d #t) (k 0)))) (h "0i8lk3jk6yc210mypx3z9xra3xyfpjm1r5fk0gp1fw50x43rxclr")))

(define-public crate-col_proc_macros-0.1.1 (c (n "col_proc_macros") (v "0.1.1") (d (list (d (n "col_proc_macros_impl") (r "^0") (d #t) (k 0)))) (h "070vdhcyl6p9jgcmdpd0xd2qn99q73gpygnqh6qqmayii9d3jw4j")))

