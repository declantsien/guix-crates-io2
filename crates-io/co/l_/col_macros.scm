(define-module (crates-io co l_ col_macros) #:use-module (crates-io))

(define-public crate-col_macros-0.1.0 (c (n "col_macros") (v "0.1.0") (h "0n8dr6nybb8q86k3aqd4y2j76yv1bmi1y48xkz31b5nkjbq5zcyl") (y #t)))

(define-public crate-col_macros-0.1.1 (c (n "col_macros") (v "0.1.1") (d (list (d (n "high_mem_utils") (r "^0.1.1") (d #t) (k 0)))) (h "0n8v7ykf4xy2pi5576nmxv552mwazwa3lfflkpj0pwbl21gq236b") (y #t)))

(define-public crate-col_macros-0.1.2 (c (n "col_macros") (v "0.1.2") (d (list (d (n "high_mem_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.0") (d #t) (k 0)))) (h "0qsh2myrbc79l65qbbn79z6xh7qq8v0c46q9hmg9r383cg1cv66x") (y #t)))

(define-public crate-col_macros-0.1.3 (c (n "col_macros") (v "0.1.3") (d (list (d (n "high_mem_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.0") (d #t) (k 0)))) (h "0g0clbipqd554q8kdjjcjbkj9mg6fvpzyqr0bjvwb0b694br425f") (y #t)))

(define-public crate-col_macros-0.1.4 (c (n "col_macros") (v "0.1.4") (d (list (d (n "compile_ops") (r "^0.1.1") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.1") (d #t) (k 0)))) (h "0x1pmh9qik1rd7v9v29fqh1c27iidq2w3n5cxqc9f8drzjpk4qm6") (y #t)))

(define-public crate-col_macros-0.1.5 (c (n "col_macros") (v "0.1.5") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.2") (d #t) (k 0)))) (h "180xq1fzhxyffz7cmpf48417c0pvndxk37hqqkg7w81h2a7hq45i") (y #t)))

(define-public crate-col_macros-0.1.6 (c (n "col_macros") (v "0.1.6") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.2") (d #t) (k 0)))) (h "0b7q6qcivfdz9wvpry4mfavbdhn49ai0sfi89db586lgs87bz6yc") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.1.62 (c (n "col_macros") (v "0.1.62") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.2") (d #t) (k 0)))) (h "0lf6wjssy58l7hrrd9p8xhw39jbq0qkbah2mjjidwxz7cib0j0wy") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.1.64 (c (n "col_macros") (v "0.1.64") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.2") (d #t) (k 0)))) (h "1zm8swq1avs7s2qfym6s5y05xiislzp4rr45ppgvwai2hk2nq47a") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.1.68 (c (n "col_macros") (v "0.1.68") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.2") (d #t) (k 0)))) (h "1r48mq465bhz52mlm737j8ab1wcdf47cblbi3i2qqifc4yqbki0r") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.1.7 (c (n "col_macros") (v "0.1.7") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.2") (d #t) (k 0)))) (h "1pg7qmabnwh5072d4z535lxgv6r290apkrc9bfqarbldjx8fhsmp") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.1.8 (c (n "col_macros") (v "0.1.8") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.2") (d #t) (k 0)))) (h "0fd1289r79aq24n73q2xn2fsiv66bsb4p57h4ydmdpfmc5l6gckj") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.1.81 (c (n "col_macros") (v "0.1.81") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.3") (d #t) (k 0)))) (h "0ck0d91vpzqnk0q2mwriz12lx1n8zxjwd738hvyk045w1j0grwbb") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.1.9 (c (n "col_macros") (v "0.1.9") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.4") (d #t) (k 0)))) (h "09v7xr0i5c62irbirkws1yrs9lcz41cjacdsn97y2dc0ywwfnmwc") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.1.91 (c (n "col_macros") (v "0.1.91") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.4") (d #t) (k 0)))) (h "04x71r4bh4cm0x15xcly5qqb3w6kyysbjbrbdz2wk2saxrfc7nvm") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.1.93 (c (n "col_macros") (v "0.1.93") (d (list (d (n "compile_ops") (r "^0.1.2") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2.2") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1.4") (d #t) (k 0)))) (h "0mbisppywra04sw2xjfhl648z6arpvz9sbrv2hrzzbf1ggr1y6mm") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.2.0 (c (n "col_macros") (v "0.2.0") (d (list (d (n "compile_ops") (r "^0.1") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1") (d #t) (k 0)))) (h "1jpsqc96hw4lkwxfjynx8mfj5fv74mjyqmwfprxnlshzixkj2d0h") (f (quote (("owned_slice")))) (y #t)))

(define-public crate-col_macros-0.2.2 (c (n "col_macros") (v "0.2.2") (d (list (d (n "compile_ops") (r "^0.1") (d #t) (k 0)) (d (n "high_mem_utils") (r "^0.2") (d #t) (k 0)) (d (n "macro_helper") (r "^0.1") (d #t) (k 0)))) (h "0szq36ygj1xca6xrrk8ba73186yk6piqwr6d8i4sym82ry04sljb")))

