(define-module (crates-io co ds codspeed) #:use-module (crates-io))

(define-public crate-codspeed-0.1.0 (c (n "codspeed") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1h43k4gmljqndlg3w2q6z9gwkzadkp5yrip9pmmi24vfjbilkqcv") (y #t)))

(define-public crate-codspeed-1.0.0 (c (n "codspeed") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "024d3ds33s3b8cag4bxqkaxpxk4pgy585xwb0sbvphqvgxf23x54")))

(define-public crate-codspeed-1.0.1 (c (n "codspeed") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14ncr4ki1xarh58wkf95lz8bjk9vhqppbwyll90f53pk07294433")))

(define-public crate-codspeed-1.1.0 (c (n "codspeed") (v "1.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zia6l51m2wc982cg65516f3mdk0cxdl6qbz1pp577x5aa3dahi0")))

(define-public crate-codspeed-2.0.0 (c (n "codspeed") (v "2.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a5w7pisjw1383hp198kkhdnkhbjj191zhw02jxp43s64dza8xgw")))

(define-public crate-codspeed-2.1.0 (c (n "codspeed") (v "2.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0asmn5zrgw6m3liagq3qxyw189jwvlai286anlwdqsa9xpxw5vjs")))

(define-public crate-codspeed-2.2.0 (c (n "codspeed") (v "2.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0mwzrads0adb58yjdbppx3pxfgpw7rdpr4rann2rkw8hdi0khclv")))

(define-public crate-codspeed-2.3.0 (c (n "codspeed") (v "2.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0i6ki1j91g77b36823dkj6qic3vfs33pi6k4lg3xfbfxxv8wr06n")))

(define-public crate-codspeed-2.3.1 (c (n "codspeed") (v "2.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0jqpbfl93wjw7bmxzxazz9qp0ym2nmbbvpnm7fmn0953y6h172wi")))

(define-public crate-codspeed-2.3.2 (c (n "codspeed") (v "2.3.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0nzn6x0x7l80q36n4fhyii08fnmlmdiydhn28dmjvx27vk3lydjp")))

(define-public crate-codspeed-2.3.3 (c (n "codspeed") (v "2.3.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "16s95jrih19llp4m8rgmymvan0misfcnzc8gb57ynm35rd6spd0f")))

(define-public crate-codspeed-2.4.0 (c (n "codspeed") (v "2.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0ikj07wl9g7az7mnlarmsyjh6a77vm4l2lmwbsbx2h85m9bb11ab")))

(define-public crate-codspeed-2.4.1 (c (n "codspeed") (v "2.4.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0zgljccdn4apn25sx3zl5mjg736r0k4hs5pvnqjxxy4fd676snk5")))

(define-public crate-codspeed-2.5.0 (c (n "codspeed") (v "2.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0vbal2qg9m4l9b05py8l2zzas8hm9375fyydjq2vjg661zp1cpvk")))

(define-public crate-codspeed-2.5.1 (c (n "codspeed") (v "2.5.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "1fnkjyv07xshf1ask9ypzrf31b4bc5brs1wyxffmaxi5js08rprn")))

(define-public crate-codspeed-2.6.0 (c (n "codspeed") (v "2.6.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "129062464gv2c3kbfyjcvx1faqngbpavvz5k3s98n670934ll41s")))

