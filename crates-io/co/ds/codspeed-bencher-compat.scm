(define-module (crates-io co ds codspeed-bencher-compat) #:use-module (crates-io))

(define-public crate-codspeed-bencher-compat-0.1.0 (c (n "codspeed-bencher-compat") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1lbawyvmkmagrapn26qdvmvavcxbw63mnj2yl9w6zyprdwv8a2hd") (f (quote (("codspeed")))) (y #t)))

(define-public crate-codspeed-bencher-compat-0.1.1 (c (n "codspeed-bencher-compat") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)))) (h "05rjpvbibjw5x1rj5njmbh0k8zc2dj7z4rgaygg58bc7d5mgrc3z") (f (quote (("codspeed")))) (y #t)))

(define-public crate-codspeed-bencher-compat-1.0.0 (c (n "codspeed-bencher-compat") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "^1.0.0") (d #t) (k 0)))) (h "153y3x6drxm9xs3cnbb9shsjg0b3s87aa2wfvhimp50dcd29mv9a")))

(define-public crate-codspeed-bencher-compat-1.0.1 (c (n "codspeed-bencher-compat") (v "1.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "^1.0.0") (d #t) (k 0)))) (h "1pqljhvzrx8jxdqnxfpk8c5nbci9iasq9k1myp5bs38d56z4qkiq")))

(define-public crate-codspeed-bencher-compat-1.1.0 (c (n "codspeed-bencher-compat") (v "1.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "^1.0.0") (d #t) (k 0)))) (h "09918b6ygg3wrs8hcs5sn19plngnw6892fh4mb3z2hdxvghmda1c")))

(define-public crate-codspeed-bencher-compat-2.0.0 (c (n "codspeed-bencher-compat") (v "2.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.0.0") (d #t) (k 0)))) (h "0hpjqnv5zmxf7knygzm7cp4rkm4yj70ydcw6v545cdfh7rpwsm86")))

(define-public crate-codspeed-bencher-compat-2.1.0 (c (n "codspeed-bencher-compat") (v "2.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.1.0") (d #t) (k 0)))) (h "1sys8dyrsiamv906cypykcq2vzly4ww61lxgpbbgvgnj1fzp7i6s")))

(define-public crate-codspeed-bencher-compat-2.2.0 (c (n "codspeed-bencher-compat") (v "2.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.2.0") (d #t) (k 0)))) (h "0y4a9pggfkwsyna08yamz4v7ii16fsb8y2ly3ryq3kw0wnri6kis")))

(define-public crate-codspeed-bencher-compat-2.3.0 (c (n "codspeed-bencher-compat") (v "2.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.3.0") (d #t) (k 0)))) (h "1hbgkgdj8kybavjk9cmi4ywghr78fxip974745mvbnm7lxri0ng3")))

(define-public crate-codspeed-bencher-compat-2.3.1 (c (n "codspeed-bencher-compat") (v "2.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.3.1") (d #t) (k 0)))) (h "0iqsyjqk7p5g8ac6laph8w179wdh15jjmm0snnya96sr878z83qk")))

(define-public crate-codspeed-bencher-compat-2.3.2 (c (n "codspeed-bencher-compat") (v "2.3.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.3.2") (d #t) (k 0)))) (h "1vvkga7wc9rlqfrqi5640zcgv2arjnf3xrydhzfypnqlg048wslw")))

(define-public crate-codspeed-bencher-compat-2.3.3 (c (n "codspeed-bencher-compat") (v "2.3.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.3.3") (d #t) (k 0)))) (h "17nmj0yzyyv6450yshs1f2qa0n7d9wwzkcc5ixjkiy63dww2ivpy")))

(define-public crate-codspeed-bencher-compat-2.4.0 (c (n "codspeed-bencher-compat") (v "2.4.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.4.0") (d #t) (k 0)))) (h "172c4xkvwbpvjhrx0w6lc4fa250cxdi8fmfvpv6mffghcxaqpr6h")))

(define-public crate-codspeed-bencher-compat-2.4.1 (c (n "codspeed-bencher-compat") (v "2.4.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.4.1") (d #t) (k 0)))) (h "1y7gch3ycl0x2fg54v7yqfy7nq7ik90aipb3pdjvnjkai10bnvj8")))

(define-public crate-codspeed-bencher-compat-2.5.0 (c (n "codspeed-bencher-compat") (v "2.5.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.5.0") (d #t) (k 0)))) (h "0ycfsqr2i2sr8mlzz69bfmkr0xx4kg7x9fj8pbr7x2jmb1yml6z6")))

(define-public crate-codspeed-bencher-compat-2.5.1 (c (n "codspeed-bencher-compat") (v "2.5.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.5.1") (d #t) (k 0)))) (h "0g1zh3a6iszms7x2nim2g1c0yayr697hqdw9qgzzpgnfvbn1i635")))

(define-public crate-codspeed-bencher-compat-2.6.0 (c (n "codspeed-bencher-compat") (v "2.6.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "codspeed") (r "=2.6.0") (d #t) (k 0)))) (h "0wdn2z1mn0wvp2gi9yd3i4gsdpglkyipzh4ry6h06ik3l97aiayf")))

