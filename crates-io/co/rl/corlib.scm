(define-module (crates-io co rl corlib) #:use-module (crates-io))

(define-public crate-corlib-0.1.0 (c (n "corlib") (v "0.1.0") (d (list (d (n "delegate") (r "0.*") (d #t) (k 0)) (d (n "paste") (r "1.0.*") (d #t) (k 0)))) (h "055vh4fv8vbdqraq0xpxr8pkx2a6pg7i2apvlpkxbsmbd0240lrb")))

(define-public crate-corlib-0.2.0 (c (n "corlib") (v "0.2.0") (d (list (d (n "delegate") (r "0.*") (d #t) (k 0)) (d (n "paste") (r "1.0.*") (d #t) (k 0)))) (h "137g6r1bmy5jyxq6zxa1cwkykp9vickqd4zjm107wb1pc7agkfvn")))

