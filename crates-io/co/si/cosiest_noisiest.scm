(define-module (crates-io co si cosiest_noisiest) #:use-module (crates-io))

(define-public crate-cosiest_noisiest-0.1.0 (c (n "cosiest_noisiest") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_chacha") (r "^0.3") (k 0)) (d (n "splines") (r "^4.3") (d #t) (k 0)))) (h "1s20yn2mzqk31aqmzs3gqxcp3p9xghidw5991sqlik9prfcm2pqh") (f (quote (("f32"))))))

(define-public crate-cosiest_noisiest-0.1.1 (c (n "cosiest_noisiest") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "num-integer") (r "^0.1") (k 0)) (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_chacha") (r "^0.3") (k 0)) (d (n "splines") (r "^4.3") (d #t) (k 0)))) (h "0g7ic2hl008k0ksflaqffiq9lplg06mqbn8bc19dq29xf5c4khy6") (f (quote (("f32"))))))

