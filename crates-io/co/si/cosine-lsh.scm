(define-module (crates-io co si cosine-lsh) #:use-module (crates-io))

(define-public crate-cosine-lsh-0.1.0 (c (n "cosine-lsh") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1p8k2jgwbfnyi11pphazkd5fdpvz92rb5shcd7pdb0fmy0687pg3")))

(define-public crate-cosine-lsh-0.1.1 (c (n "cosine-lsh") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "13rw9dlb9rfh9c645f22456c6hhji052nc0b5ypjh8qgs555clcv")))

