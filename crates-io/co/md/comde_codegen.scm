(define-module (crates-io co md comde_codegen) #:use-module (crates-io))

(define-public crate-comde_codegen-0.0.0 (c (n "comde_codegen") (v "0.0.0") (d (list (d (n "byte_string") (r "^1.0.0") (d #t) (k 0)) (d (n "comde") (r "^0.0.0") (d #t) (k 0)) (d (n "delegate") (r "^0.2.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_shared") (r "^0.7.24") (d #t) (k 0)))) (h "1mhk9rkv11q35m7yxravdc8jn146wdnsmzkcrs0dzy8gz9z1ylsx") (f (quote (("zstandard" "comde/zstandard") ("xz" "comde/xz") ("snappy" "comde/snappy") ("deflate" "comde/deflate"))))))

(define-public crate-comde_codegen-0.1.0 (c (n "comde_codegen") (v "0.1.0") (d (list (d (n "byte_string") (r "^1.0.0") (d #t) (k 0)) (d (n "comde") (r "^0.1.0") (d #t) (k 0)) (d (n "delegate") (r "^0.2.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_shared") (r "^0.7.24") (d #t) (k 0)))) (h "1vkbfgwyxpncv1w1mk85w6d4sz8244c0abhmbm1axjyibif20mzc") (f (quote (("zstandard" "comde/zstandard") ("xz" "comde/xz") ("snappy" "comde/snappy") ("deflate" "comde/deflate"))))))

