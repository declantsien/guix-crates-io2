(define-module (crates-io co md comdlg32-sys) #:use-module (crates-io))

(define-public crate-comdlg32-sys-0.0.1 (c (n "comdlg32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0wbqmhg83cxzbcsy4n878cymh9dm7lqhsfpwgwlhv83kddc70qq1")))

(define-public crate-comdlg32-sys-0.2.0 (c (n "comdlg32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0adma1vz4fic8jsz6ar8cwligbm3ljr01z5vcslbczdc3gvh2r38")))

