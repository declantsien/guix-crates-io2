(define-module (crates-io co d- cod-render) #:use-module (crates-io))

(define-public crate-cod-render-0.1.0 (c (n "cod-render") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (f (quote ("editor" "date"))) (d #t) (k 0)) (d (n "term-table") (r "^1.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("time" "macros"))) (d #t) (k 0)))) (h "0ah4vscpwn90awbzy1pd5x5fmbvfd86nzaidiqn6b3ghjvv9sbpr")))

(define-public crate-cod-render-0.2.0 (c (n "cod-render") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (f (quote ("editor" "date"))) (d #t) (k 0)) (d (n "term-table") (r "^1.3.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("time" "macros"))) (d #t) (k 0)))) (h "1qqqcvf4954773bdkrc6a22v8z792i06nn507swx7rm44pd516zk")))

