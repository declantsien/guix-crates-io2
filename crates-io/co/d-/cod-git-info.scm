(define-module (crates-io co d- cod-git-info) #:use-module (crates-io))

(define-public crate-cod-git-info-0.1.0 (c (n "cod-git-info") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)))) (h "0z48k0h977czvajm1ja5ywg5qwg7mjxfng08i9py78iap3pdcixd")))

(define-public crate-cod-git-info-0.2.0 (c (n "cod-git-info") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)))) (h "04z1ccw5l7b0gh6liak4sl94gckksdqm9rv1a38w4baxzb4ijsci")))

