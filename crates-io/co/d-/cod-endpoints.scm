(define-module (crates-io co d- cod-endpoints) #:use-module (crates-io))

(define-public crate-cod-endpoints-0.1.0 (c (n "cod-endpoints") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "cod-git-info") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)))) (h "08bzh9ahk8hcm7kdycjdll685fc729mq3kzsvkqixmi79akjkn8s")))

(define-public crate-cod-endpoints-0.2.0 (c (n "cod-endpoints") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "cod-git-info") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)))) (h "00n7w3vl1awb6199fzfm6z9nzais7p1zmk2lqm6slrj675mknw80")))

(define-public crate-cod-endpoints-0.2.1 (c (n "cod-endpoints") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "cod-git-info") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)))) (h "1qjnjg4r0fg89mfzijiy1j20mlnzi1ig5bipx9qzx41902g19g18")))

