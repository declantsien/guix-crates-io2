(define-module (crates-io co d- cod-bin) #:use-module (crates-io))

(define-public crate-cod-bin-0.1.0 (c (n "cod-bin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cod-actions") (r "^0.1.0") (d #t) (k 0)) (d (n "cod-cli") (r "^0.1.0") (d #t) (k 0)) (d (n "cod-client") (r "^0.1.0") (d #t) (k 0)) (d (n "cod-types") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1na2p9mdgik5fvk1sfp599nkamid4nxncx9pr1xf7x4igwy62prf")))

(define-public crate-cod-bin-0.2.0 (c (n "cod-bin") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cod-actions") (r "^0.2.0") (d #t) (k 0)) (d (n "cod-cli") (r "^0.2.0") (d #t) (k 0)) (d (n "cod-client") (r "^0.2.0") (d #t) (k 0)) (d (n "cod-types") (r "^0.2.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1fiank780nav9007bdwp9fz0pdpwqxy5643s64spvvwsq9sya79q")))

