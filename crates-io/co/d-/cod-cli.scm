(define-module (crates-io co d- cod-cli) #:use-module (crates-io))

(define-public crate-cod-cli-0.1.0 (c (n "cod-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.1") (d #t) (k 0)) (d (n "cod-types") (r "^0.1.0") (d #t) (k 0)))) (h "1z6vdx5xdb4sm8ajnh4rnjw9vhvfrra11k6y1nraf4s8sw1limj7")))

(define-public crate-cod-cli-0.2.0 (c (n "cod-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.1") (d #t) (k 0)) (d (n "cod-types") (r "^0.2.1") (d #t) (k 0)))) (h "1g4hb9x0s57mab8hs5q1mdzgjryh4x6i2z9c7biwgylibiwnfcnn")))

