(define-module (crates-io co d- cod-paths) #:use-module (crates-io))

(define-public crate-cod-paths-0.1.0 (c (n "cod-paths") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "1h5y1p9x5z5mnabg8c3hi0j3704swgm627qwsc3dhvcs80masdw7")))

(define-public crate-cod-paths-0.2.0 (c (n "cod-paths") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "17flngqyffnifgfyz1w7f45fzj0n8s8dc224d6kwscq8qqdlh2qh")))

