(define-module (crates-io co di codice_fiscale_rs) #:use-module (crates-io))

(define-public crate-codice_fiscale_rs-0.1.0 (c (n "codice_fiscale_rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "01g2p70rw5dh616s7wvf0wqzxvpq78qlhhkvxlr7b06d4z93968m")))

(define-public crate-codice_fiscale_rs-0.1.1 (c (n "codice_fiscale_rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1va08nmq8s3vhx78j4v3m35v719605ra4gqrckwplc61sajyxwj7")))

(define-public crate-codice_fiscale_rs-0.1.2 (c (n "codice_fiscale_rs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0cnf6cqqjn3qkkpvfrl5575xayimh82kl3847239hpv7sp3x7zp5")))

(define-public crate-codice_fiscale_rs-0.1.3 (c (n "codice_fiscale_rs") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1r70kd43cmn4n2gck1f67jfzmzrdrw7ig55mynzkqf3kz55jmk4i")))

(define-public crate-codice_fiscale_rs-0.2.0 (c (n "codice_fiscale_rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1y5v1iiqv6jsp40gp8y59nrycvjfxyqsdaiyziw2fmc9898p40wg")))

(define-public crate-codice_fiscale_rs-0.2.1 (c (n "codice_fiscale_rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "01bf8q071fbx4cs9ianch660ildxy1qadf3y03zvlc5ppkh1h6gi")))

