(define-module (crates-io co di coding_pk) #:use-module (crates-io))

(define-public crate-coding_pk-0.0.0 (c (n "coding_pk") (v "0.0.0") (h "1i91z1iwb3mz12x8irc1rh0ll1vgsdbb9aj04bb2bp30vgnxb7vm")))

(define-public crate-coding_pk-0.1.0 (c (n "coding_pk") (v "0.1.0") (h "0qkdqbrmpy5ijx86xljp3m6cszidz46ghz2zlkckynr778ppxa6p")))

(define-public crate-coding_pk-0.2.0 (c (n "coding_pk") (v "0.2.0") (h "16xhwzxx9k7gchj8s8msk19da8wv12cjib7n5cmgjdi2ahwlk6im")))

(define-public crate-coding_pk-0.3.0 (c (n "coding_pk") (v "0.3.0") (h "0ckdnsyd9r05wjcx7pxl05974m9vgp2cy25bxwp0jpk4myd4c2zg")))

(define-public crate-coding_pk-0.4.0 (c (n "coding_pk") (v "0.4.0") (h "1853sla3nynaprxmvll9pzbyvl3mcrdkwxd591ngcga2fcm0zgb0")))

(define-public crate-coding_pk-0.5.0 (c (n "coding_pk") (v "0.5.0") (h "0x6i239wxqk2k9py0mz7gsa9l0jqkmj5x2pyy1hhlaba65dai5yk")))

(define-public crate-coding_pk-0.5.1 (c (n "coding_pk") (v "0.5.1") (h "1pv75yxcb0qsv914m781dmsh5bi05dizxr3h7cvjibm07daxl87v")))

(define-public crate-coding_pk-0.6.0 (c (n "coding_pk") (v "0.6.0") (h "0rh94lxn3pn9qsrv0k6xqp9xcfi5zjja0qz183s04l505i7i7g40")))

(define-public crate-coding_pk-0.7.0 (c (n "coding_pk") (v "0.7.0") (h "1mg2zyigwj7mlmf26dgy13xcs7sh6h217244zr2i961z1rfq2zzy")))

(define-public crate-coding_pk-0.8.0 (c (n "coding_pk") (v "0.8.0") (h "13mnrxcbp27cj72ag1zqyrj1s74biyfwhi5bx8nxjhpclwmv3ijq")))

(define-public crate-coding_pk-0.9.0 (c (n "coding_pk") (v "0.9.0") (h "0pfwr84yzk30nlcbj68wjxqaf6svag4yi57r0h2zjphz06v5ml6v")))

(define-public crate-coding_pk-0.10.0 (c (n "coding_pk") (v "0.10.0") (h "17fj86sw9yccr28sh9m0b8l6fbh0qs8gmxvf5syhkwcpfz9v7vzg")))

(define-public crate-coding_pk-0.11.0 (c (n "coding_pk") (v "0.11.0") (h "10cs5x4qv7bh0l7gixiy0s0arypaa5mbn9ix6306rcmpnfkk5a93")))

(define-public crate-coding_pk-0.11.1 (c (n "coding_pk") (v "0.11.1") (h "0xh67kib5ypwc79mgp1y4v6cfpnr5mpkrmmkyl72ypnkzrk54adf")))

(define-public crate-coding_pk-0.11.2 (c (n "coding_pk") (v "0.11.2") (h "1avnwdrdvqfx0wi58dsn5681jyk8yr04sqkjmgvm0smrz51bk6zi")))

(define-public crate-coding_pk-0.12.0 (c (n "coding_pk") (v "0.12.0") (h "1yspn1fkdbjsflr8yl8fr4w3xbd2zibbnh522r84fsjcly7db330")))

(define-public crate-coding_pk-0.13.0 (c (n "coding_pk") (v "0.13.0") (h "0zyxyrrv2i0lc7izfhqdj8dnfssc7r44pnshyy8vipi1v4kwd9xw")))

