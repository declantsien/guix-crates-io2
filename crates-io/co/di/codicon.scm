(define-module (crates-io co di codicon) #:use-module (crates-io))

(define-public crate-codicon-0.1.0 (c (n "codicon") (v "0.1.0") (h "1d5vl7n3r9fy9z2zqawp2lzifsgv0qqzn1wm57wi3xhjv8jnd6a9")))

(define-public crate-codicon-1.0.0 (c (n "codicon") (v "1.0.0") (h "17jxrhcdm5n24mx7vwa5jpfpcr93bqawwhdz2lafxqmj4ljhamq6")))

(define-public crate-codicon-2.0.0 (c (n "codicon") (v "2.0.0") (h "0lx0fnzzfy9vjkbr21wc4v3sxsvq1nmcsapx9mrzisw8lcjnzgv2")))

(define-public crate-codicon-2.1.0 (c (n "codicon") (v "2.1.0") (h "1y0l78lh6nv47waviad1nsxb9s03c1inmkbh0m9vaf0sbbgipj0w")))

(define-public crate-codicon-3.0.0 (c (n "codicon") (v "3.0.0") (h "0qfyx6s7911b2z435p3793xd0m38hdpmk0czl44nygakyf0005qj")))

