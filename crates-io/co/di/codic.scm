(define-module (crates-io co di codic) #:use-module (crates-io))

(define-public crate-codic-0.1.0 (c (n "codic") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fipjmrdkhkyixx7aabx03r5a2fc042bqivigf1ymlyvh8hd3w01")))

(define-public crate-codic-0.2.0 (c (n "codic") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0qj9njhsdi8vz3lvf1lmnfpd1c6wbks43bbnb7cg484cbs0rwrhv")))

