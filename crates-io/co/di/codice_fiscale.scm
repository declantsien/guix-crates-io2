(define-module (crates-io co di codice_fiscale) #:use-module (crates-io))

(define-public crate-codice_fiscale-0.1.0 (c (n "codice_fiscale") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1z76r7sd0fs9i724x1hmx5bag7gjz9xwlxrkl1ggmclnpg4ia31v") (y #t)))

(define-public crate-codice_fiscale-0.1.1 (c (n "codice_fiscale") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "16mcv2g2grsbg7j6n5zkhfwaq138pgw6ykjka01fq9lx86640czr") (y #t)))

(define-public crate-codice_fiscale-0.2.0 (c (n "codice_fiscale") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.11") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "16h27d4b7i0f0w0n4lpy6z1libgy6fngjhmmmcqi5ainjqrydnrk")))

(define-public crate-codice_fiscale-0.3.0 (c (n "codice_fiscale") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1bh3199b4pklippj8s71vrcryzdfyd4in8k27l17lwjkgs4yb46h")))

(define-public crate-codice_fiscale-0.3.1 (c (n "codice_fiscale") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1ak0gbq44r662h23gz1ys35xwwl03kw0y5h18v8ih6l67qsmgn7b")))

(define-public crate-codice_fiscale-0.4.0 (c (n "codice_fiscale") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("parsing" "macros"))) (d #t) (k 0)))) (h "0z2ajsr51dkvq1bnfxsbsh6rhszwqs6siyw9sgq1vczy570wpq5w")))

(define-public crate-codice_fiscale-0.4.1 (c (n "codice_fiscale") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("parsing" "macros"))) (d #t) (k 0)))) (h "0vghwcbfkg9m84jq2apsb5s5mms63g4mv46ir8hgmypxcl3c7mmm")))

