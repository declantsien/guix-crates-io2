(define-module (crates-io co ap coap-message) #:use-module (crates-io))

(define-public crate-coap-message-0.0.1 (c (n "coap-message") (v "0.0.1") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "0azbdnqmc95wc15pg668szich14057prflisv9kxxi17fnal4ijn") (f (quote (("typenum_test" "typenum"))))))

(define-public crate-coap-message-0.0.2 (c (n "coap-message") (v "0.0.2") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "1gn3jmmaywjn5cmiq10mzi9f9c246bc1vwwh3ycjcd5z5mixhyx0") (f (quote (("typenum_test" "typenum"))))))

(define-public crate-coap-message-0.0.3 (c (n "coap-message") (v "0.0.3") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "0p91r27rbs4rk2qrxzwjxai9h5wa89hckbi2afv6xbkgi8klrb3i") (f (quote (("typenum_test" "typenum"))))))

(define-public crate-coap-message-0.1.0 (c (n "coap-message") (v "0.1.0") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "1i1z7dl8zjz7vgv6mcl3qmqsbvdcjaxd641saxkwj8qykvyv2g8z") (f (quote (("typenum_test" "typenum") ("alloc"))))))

(define-public crate-coap-message-0.1.1 (c (n "coap-message") (v "0.1.1") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "0m5bkiglzwv9x8hlfv0p6wmgbqfn73db0f89siq20kqs2h3ccwmr") (f (quote (("typenum_test" "typenum") ("alloc"))))))

(define-public crate-coap-message-0.2.0-alpha.0 (c (n "coap-message") (v "0.2.0-alpha.0") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "1kca49xnasp5mw1lhb2vb812cxxf6ilnysvi9ksklsywn56d3ng0") (f (quote (("typenum_test" "typenum") ("alloc"))))))

(define-public crate-coap-message-0.2.0 (c (n "coap-message") (v "0.2.0") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "1706dlm0vnzbk30dmg7cf1ml5m7knhd061pcyqzd3fh5z4jq3v2y") (f (quote (("typenum_test" "typenum") ("alloc"))))))

(define-public crate-coap-message-0.2.1 (c (n "coap-message") (v "0.2.1") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "00xgzfzhpgwn8ps458bdfwsganybdp1sagf3l4jd7p53kpk18i8w") (f (quote (("typenum_test" "typenum") ("alloc"))))))

(define-public crate-coap-message-0.2.2 (c (n "coap-message") (v "0.2.2") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "1y1xhd2qg516gzw6jpgz3q52bknff7bjdjgx3xb08vsfbnd53i0j") (f (quote (("typenum_test" "typenum") ("alloc"))))))

(define-public crate-coap-message-0.2.3-pre1 (c (n "coap-message") (v "0.2.3-pre1") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "132xlk4qmrhpyawi9f1d23ravs00w3psra2xp3fih0d08xhnana1") (f (quote (("typenum_test" "typenum") ("alloc")))) (r "1.65")))

(define-public crate-coap-message-0.2.3 (c (n "coap-message") (v "0.2.3") (d (list (d (n "typenum") (r "^1") (o #t) (d #t) (k 0)))) (h "1v2q5dnlx8c3880xliga929znn1zcxphakysiysi1sbbbzhqij3w") (f (quote (("typenum_test" "typenum") ("alloc")))) (r "1.65")))

(define-public crate-coap-message-0.3.0-alpha.1 (c (n "coap-message") (v "0.3.0-alpha.1") (d (list (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0y4qnywgfiw5dyndyk8b5h9li2qyp5j5nmn77c5y0nn5g94pq8sw") (r "1.65")))

(define-public crate-coap-message-0.3.0-alpha.2 (c (n "coap-message") (v "0.3.0-alpha.2") (d (list (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1ndry0hsdrbyz2dqsbhw0w1wr2xfby9cb8ixabxjajrc75q77ph9") (r "1.65")))

(define-public crate-coap-message-0.3.0 (c (n "coap-message") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0lfrvdp11aarwx9acfd42n79hpmhiij7r2lyi0npah03z82psmya") (r "1.65")))

(define-public crate-coap-message-0.3.1 (c (n "coap-message") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2.16") (k 0)))) (h "0bxhgnjx8lh794d48dxncxxybjmhj72pxss8mz7xzbz608nph65f") (r "1.65")))

(define-public crate-coap-message-0.3.2 (c (n "coap-message") (v "0.3.2") (d (list (d (n "num-traits") (r "^0.2.16") (k 0)))) (h "0r85qqmkz0gkbsa3drc914ah7rx6b2f6rnjlyky5sk2zlf36s4ky") (r "1.65")))

