(define-module (crates-io co ap coap_rs) #:use-module (crates-io))

(define-public crate-coap_rs-0.1.0 (c (n "coap_rs") (v "0.1.0") (h "1n99y4avljzh55ib6qrlnhqgpwidpah77qfxjyjna9v7yn75s5nl")))

(define-public crate-coap_rs-0.1.1 (c (n "coap_rs") (v "0.1.1") (h "19dpkhnd545w4l2228xac3aw3h8npjh2qij6ff2bcafjl1skkn60")))

