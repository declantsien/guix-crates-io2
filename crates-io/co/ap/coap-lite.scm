(define-module (crates-io co ap coap-lite) #:use-module (crates-io))

(define-public crate-coap-lite-0.1.0 (c (n "coap-lite") (v "0.1.0") (h "02gxg0pk5w2sv0265kscv1r6x5jpv29ii89hm9d3mn7wi3wlv30a")))

(define-public crate-coap-lite-0.2.0 (c (n "coap-lite") (v "0.2.0") (h "0l45qkc7xvghkydfv3y874h4bdk6gzvh9asd8v7hal4sira2m9ar") (f (quote (("std") ("default" "std"))))))

(define-public crate-coap-lite-0.3.0 (c (n "coap-lite") (v "0.3.0") (h "0alza78klh3lipywb4vlllishj7jlf6qggk2hxfk9l87qzlf1jc2") (f (quote (("std") ("default" "std"))))))

(define-public crate-coap-lite-0.3.1 (c (n "coap-lite") (v "0.3.1") (h "0a4g0pygdm7pqkcifr2hf82rphng0nywdi6508wkijhmvzdijj5d") (f (quote (("std") ("default" "std"))))))

(define-public crate-coap-lite-0.3.2 (c (n "coap-lite") (v "0.3.2") (h "0m8gpcavqw20xa9i1xy9pnbrlzmns5lk3j85l6zxhli6cyrvxkvf") (f (quote (("std") ("default" "std"))))))

(define-public crate-coap-lite-0.3.3 (c (n "coap-lite") (v "0.3.3") (h "0yaim7x9816x5f9dviqvgn4y35kcrsnzalyfcyqbp7j9fa2a27b3") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-coap-lite-0.3.4 (c (n "coap-lite") (v "0.3.4") (h "0hid2l52pqilbqj58xxmh1hv39j8jp6nwq0mdk4m52rdjh80mp1x") (f (quote (("std") ("default" "std"))))))

(define-public crate-coap-lite-0.3.5 (c (n "coap-lite") (v "0.3.5") (h "05mkcra6filw2q6q01d3hs26ccl29rpxj9gjqbjqhbs602ab5pqd") (f (quote (("std") ("default" "std"))))))

(define-public crate-coap-lite-0.4.0 (c (n "coap-lite") (v "0.4.0") (d (list (d (n "coap-handler") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "coap-numbers") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("alloc"))) (o #t) (k 0)))) (h "1hsf2jqgqh2i51cj33rm7zi5xfdd37cvd466048b8ycmdybdvdxv") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler" "serde" "coap-numbers") ("default" "std"))))))

(define-public crate-coap-lite-0.4.1 (c (n "coap-lite") (v "0.4.1") (d (list (d (n "coap-handler") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "coap-numbers") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0fz0lk9akfzbj0zdhi03cp5xs9gz04gbrng30plp0fwinx18cpni") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler" "serde" "coap-numbers") ("default" "std"))))))

(define-public crate-coap-lite-0.5.0 (c (n "coap-lite") (v "0.5.0") (d (list (d (n "coap-handler") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "coap-numbers") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("alloc"))) (o #t) (k 0)))) (h "1vpl0w3djd1bl5gjnv6hg3yyd6ri3kvrrcz4xgfajwd0fk9pdb7d") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler" "serde" "coap-numbers") ("default" "std"))))))

(define-public crate-coap-lite-0.5.1 (c (n "coap-lite") (v "0.5.1") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)))) (h "0l2kdzbwa0pprz6gpf32kv62y63s9672g05wb1mbg9p6jc5grsn7") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.6.0 (c (n "coap-lite") (v "0.6.0") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)))) (h "0x030i579xa8rx61wkgbq4g2q97cn4njsqzs24slcqbv8ssxis53") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.7.0 (c (n "coap-lite") (v "0.7.0") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (k 0)))) (h "0l6riy4hxz3by02x33mshl3dm4sf8pajpx9zy47vjfzwx5pm4499") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.8.0 (c (n "coap-lite") (v "0.8.0") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (k 0)))) (h "0livr34i847na6ypykpg82xs0d5qaavyjvmyplami3wvl5x82c2y") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.8.1 (c (n "coap-lite") (v "0.8.1") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (k 0)))) (h "1b8kvcp1p1xk6knd8bb4d6002fvx8sig1i4r73qvjsp27pgx388z") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.9.0 (c (n "coap-lite") (v "0.9.0") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (k 0)) (d (n "lru_time_cache") (r "^0.11.11") (d #t) (k 0)))) (h "18mm14j8xm2nc03bb680f01a8i32srp5ki0yq06jvvvvzs23i7g3") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.9.1 (c (n "coap-lite") (v "0.9.1") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (k 0)) (d (n "lru_time_cache") (r "^0.11.11") (d #t) (k 0)))) (h "1lrzjhnizia4ikqbs1p2mm0hz06q7p5khliwb4a7xvrwskm5jj1v") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.10.0 (c (n "coap-lite") (v "0.10.0") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (k 0)) (d (n "lru_time_cache") (r "^0.11.11") (d #t) (k 0)))) (h "0q9rrdsspp28bbalgxvcj0xsn551hcsf5wb1kjnlcwng4dq7q3h6") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.11.0 (c (n "coap-lite") (v "0.11.0") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (k 0)) (d (n "lru_time_cache") (r "^0.11.11") (d #t) (k 0)))) (h "0gg931d7gk30i2ankazzsxnsy30c1qkpi60sr590zfsrrag6n7rv") (f (quote (("with-coap-message" "coap-message") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.11.1 (c (n "coap-lite") (v "0.11.1") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (k 0)) (d (n "lru_time_cache") (r "^0.11.11") (d #t) (k 0)))) (h "0fkc6zvq3xv1ybycjy4racq2lsw3a9lqgvlgripwh9pnd1bkwbps") (f (quote (("with-coap-message" "coap-message") ("udp") ("std") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.11.2 (c (n "coap-lite") (v "0.11.2") (d (list (d (n "coap-handler") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (k 0)) (d (n "lru_time_cache") (r "^0.11.11") (o #t) (d #t) (k 0)))) (h "1igizl2inp5bb4rc0zcpypzm29mpynx91sw4nwp5pc8asiml7h1i") (f (quote (("with-coap-message" "coap-message") ("udp") ("std" "lru_time_cache") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.11.3 (c (n "coap-lite") (v "0.11.3") (d (list (d (n "coap-handler") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "coap-message") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (o #t) (k 0)) (d (n "lru_time_cache") (r "^0.11.11") (o #t) (d #t) (k 0)))) (h "0ygpj7h3lzs7809aljj0xb7y95vdwgdqahl24kmi5ksnqz5i3ji5") (f (quote (("with-coap-message" "coap-message") ("udp") ("std" "lru_time_cache") ("example-server_coaphandler" "with-coap-message" "coap-handler") ("default" "std"))))))

(define-public crate-coap-lite-0.11.4 (c (n "coap-lite") (v "0.11.4") (d (list (d (n "coap-handler") (r "^0.2.0") (d #t) (k 2)) (d (n "coap-handler-implementations") (r "^0.5.0") (d #t) (k 2)) (d (n "coap-message") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-message-0-3") (r "^0.3") (d #t) (k 0) (p "coap-message")) (d (n "log") (r "^0.4.19") (o #t) (k 0)) (d (n "lru_time_cache") (r "^0.11.11") (o #t) (d #t) (k 0)))) (h "054ij5zn4mzahhxqdj8461i170i6r9i2hrlr85x1shjy1mggbpm3") (f (quote (("with-coap-message") ("udp") ("std" "lru_time_cache") ("example-server_coaphandler") ("default" "std"))))))

(define-public crate-coap-lite-0.11.5 (c (n "coap-lite") (v "0.11.5") (d (list (d (n "coap-handler") (r "^0.2.0") (d #t) (k 2)) (d (n "coap-handler-implementations") (r "^0.5.0") (d #t) (k 2)) (d (n "coap-message") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-message-0-3") (r "^0.3") (d #t) (k 0) (p "coap-message")) (d (n "log") (r "^0.4.19") (o #t) (k 0)) (d (n "lru_time_cache") (r "^0.11.11") (o #t) (d #t) (k 0)))) (h "19l31d1w6vjiplc6giia88wafjdgz063k06rnz2541k5rgxz8whx") (f (quote (("with-coap-message") ("udp") ("std" "lru_time_cache") ("example-server_coaphandler") ("default" "std"))))))

(define-public crate-coap-lite-0.12.0 (c (n "coap-lite") (v "0.12.0") (d (list (d (n "coap-handler") (r "^0.2.0") (d #t) (k 2)) (d (n "coap-handler-implementations") (r "^0.5.0") (d #t) (k 2)) (d (n "coap-message") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-message-0-3") (r "^0.3") (d #t) (k 0) (p "coap-message")) (d (n "log") (r "^0.4.19") (o #t) (k 0)) (d (n "lru_time_cache") (r "^0.11.11") (o #t) (d #t) (k 0)))) (h "0hfjjsdh2lvvkl58f255f06mh1xvrx8bdmz5qhjv2igp4vwh843x") (f (quote (("udp") ("std" "lru_time_cache") ("default" "std"))))))

