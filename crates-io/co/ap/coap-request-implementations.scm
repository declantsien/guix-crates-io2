(define-module (crates-io co ap coap-request-implementations) #:use-module (crates-io))

(define-public crate-coap-request-implementations-0.1.0-alpha.1 (c (n "coap-request-implementations") (v "0.1.0-alpha.1") (d (list (d (n "coap-handler-implementations") (r "^0.4.2") (d #t) (k 0)) (d (n "coap-message") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-request") (r "^0.1.0") (d #t) (k 0)))) (h "12yb2iv33w6dkk3sc6z4wxmzaasdvragv3xp9y2ng4n0w5k02a8g") (r "1.75")))

(define-public crate-coap-request-implementations-0.1.0-alpha.2 (c (n "coap-request-implementations") (v "0.1.0-alpha.2") (d (list (d (n "coap-handler-implementations") (r "^0.4.2") (d #t) (k 0)) (d (n "coap-message") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-request") (r "^0.1.0") (d #t) (k 0)))) (h "1s5r7qdjd79jvcbqgf1cqh0drpsl5ck568kj1h8s4b9mh8i63wjf") (r "1.75")))

(define-public crate-coap-request-implementations-0.1.0-alpha.3 (c (n "coap-request-implementations") (v "0.1.0-alpha.3") (d (list (d (n "coap-message") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "coap-message-utils") (r "^0.3.0") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-request") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "0k74cqqc757nzwryg1md6awcc5ci53fzisa48npf95x8vcabd4rn") (r "1.75")))

(define-public crate-coap-request-implementations-0.1.0-alpha.4 (c (n "coap-request-implementations") (v "0.1.0-alpha.4") (d (list (d (n "coap-message") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "coap-message-utils") (r "^0.3.0") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-request") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "07w5c1lk9m5cxa8lv9yysbkz7ccgy7plcxcanp1ffbyk02zc65fc") (r "1.75")))

