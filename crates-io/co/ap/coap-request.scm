(define-module (crates-io co ap coap-request) #:use-module (crates-io))

(define-public crate-coap-request-0.1.0 (c (n "coap-request") (v "0.1.0") (d (list (d (n "coap-message") (r "^0.2") (d #t) (k 0)))) (h "17dfb9flw0s987xv19mdhpd379fynh429b4pnfrj9mqmqrl3xg1j") (r "1.75")))

(define-public crate-coap-request-0.2.0-alpha.1 (c (n "coap-request") (v "0.2.0-alpha.1") (d (list (d (n "coap-message") (r "^0.3.0-alpha.1") (d #t) (k 0)))) (h "0gaxyi5a4zm6y3frhfdg42h1g070p80ffbqmpi4210nyb77awm1z") (r "1.75")))

(define-public crate-coap-request-0.2.0-alpha.2 (c (n "coap-request") (v "0.2.0-alpha.2") (d (list (d (n "coap-message") (r "^0.3.0-alpha.2") (d #t) (k 0)))) (h "0v6is82f87smhj156yr46mim1zjn05lvv316y3l6gf29gna6vmsq") (r "1.75")))

