(define-module (crates-io co ap coap-scroll-ring-server) #:use-module (crates-io))

(define-public crate-coap-scroll-ring-server-0.1.0 (c (n "coap-scroll-ring-server") (v "0.1.0") (d (list (d (n "coap-handler") (r "^0.1.5") (d #t) (k 0)) (d (n "coap-handler-implementations") (r "^0.4.0") (d #t) (k 0)) (d (n "coap-message") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2.2") (d #t) (k 0)) (d (n "scroll-ring") (r "^0.1.1") (d #t) (k 0)))) (h "0k53nx4c7hlkni3q6s1z0yrxmkvzi4jnv57sh7r6jnpg2fl5g6s9") (r "1.65")))

(define-public crate-coap-scroll-ring-server-0.2.0 (c (n "coap-scroll-ring-server") (v "0.2.0") (d (list (d (n "coap-handler") (r "^0.2.0") (d #t) (k 0)) (d (n "coap-handler-implementations") (r "^0.5.0") (d #t) (k 0)) (d (n "coap-message") (r "^0.3.0") (d #t) (k 0)) (d (n "coap-message-utils") (r "^0.3.0") (f (quote ("error_max_age"))) (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2.2") (d #t) (k 0)) (d (n "scroll-ring") (r "^0.1.1") (d #t) (k 0)))) (h "183qjz5qwjlk9ibyc9q93y92a7s0pj4vxn7w4yahfr53r8j1f8jl") (r "1.65")))

