(define-module (crates-io co ap coap-gatt-utils) #:use-module (crates-io))

(define-public crate-coap-gatt-utils-0.1.0 (c (n "coap-gatt-utils") (v "0.1.0") (d (list (d (n "coap-message") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-message-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (d #t) (k 0)))) (h "0b05d8r32wsxd7l7v73qgszyssa5rswf5437rclmbasl2m31g80i")))

