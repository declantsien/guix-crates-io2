(define-module (crates-io co ap coap-message-utils) #:use-module (crates-io))

(define-public crate-coap-message-utils-0.0.1 (c (n "coap-message-utils") (v "0.0.1") (d (list (d (n "coap-message") (r "^0.1") (d #t) (k 0)))) (h "0viss049jcjp0x9yckxq2zki5vkicp4qayprvmxgqvrksd4v9ql1")))

(define-public crate-coap-message-utils-0.0.2 (c (n "coap-message-utils") (v "0.0.2") (d (list (d (n "coap-message") (r "^0.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "1gs06yhg77z63nxhbdjqi7icqgb8pkwj75cl8x2bpm62pjwncfkm")))

(define-public crate-coap-message-utils-0.1.0 (c (n "coap-message-utils") (v "0.1.0") (d (list (d (n "coap-message") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "0mqj7m6wa4h03cml1sr7b5lw4r2b87amxwan7p94bwbabbk19n96")))

(define-public crate-coap-message-utils-0.1.1 (c (n "coap-message-utils") (v "0.1.1") (d (list (d (n "coap-message") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "00n0rpqdngp7n9cmwh4mlaq5i8cw61x2pypsk1z02l1q02ajcmxv")))

(define-public crate-coap-message-utils-0.1.2 (c (n "coap-message-utils") (v "0.1.2") (d (list (d (n "coap-message") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "0gh30bqy6np5n89skm2a74gh24y4cciz9qmb52g8yr0ja8gpq4bn")))

(define-public crate-coap-message-utils-0.2.0 (c (n "coap-message-utils") (v "0.2.0") (d (list (d (n "coap-message") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "16ggbrbrn8v8vza1fja084p610cn22snj88qxyaxdyr44b88wx4m") (r "1.65")))

(define-public crate-coap-message-utils-0.2.1 (c (n "coap-message-utils") (v "0.2.1") (d (list (d (n "coap-message") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "06gshdjch30k8ljs5132zlfxcsqx7qapypmbinxqg0fl2fj8xzh2") (r "1.65")))

(define-public crate-coap-message-utils-0.2.2 (c (n "coap-message-utils") (v "0.2.2") (d (list (d (n "coap-message") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "108r9qvqzdlxmx0a1lffzvcnc7v2l69rdwnpkr98p3smf8d290xg") (r "1.65")))

(define-public crate-coap-message-utils-0.3.0-alpha.1 (c (n "coap-message-utils") (v "0.3.0-alpha.1") (d (list (d (n "coap-message") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "0h5m53rjhqbkvjmjxkk8abwrl49i9vpb7vz98qjlrk8p9yranzn9") (r "1.65")))

(define-public crate-coap-message-utils-0.3.0 (c (n "coap-message-utils") (v "0.3.0") (d (list (d (n "cbor-diag") (r "^0.1.12") (d #t) (k 2)) (d (n "coap-message") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (k 0)))) (h "1b8j9ww1j2nz464a839g55glwb97lfmkwwin40pizya0kv29z53v") (f (quote (("error_unprocessed_coap_option") ("error_request_body_error_position") ("error_max_age") ("_nightly_docs")))) (r "1.75")))

(define-public crate-coap-message-utils-0.3.1 (c (n "coap-message-utils") (v "0.3.1") (d (list (d (n "cbor-diag") (r "^0.1.12") (d #t) (k 2)) (d (n "coap-message") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2.3") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (k 0)))) (h "0jzwrp88w0098l3gv8ffaic6cjb832cns8598wxfl5zdikq4kfrf") (f (quote (("error_unprocessed_coap_option") ("error_request_body_error_position") ("error_max_age") ("_nightly_docs")))) (r "1.75")))

(define-public crate-coap-message-utils-0.3.2 (c (n "coap-message-utils") (v "0.3.2") (d (list (d (n "cbor-diag") (r "^0.1.12") (d #t) (k 2)) (d (n "coap-message") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2.3") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (k 0)))) (h "1sil4ypy5q48m0wycvkkk73yp9lg9v30r3gxmq8w5g7sdlw1wz6m") (f (quote (("error_unprocessed_coap_option") ("error_request_body_error_position") ("error_max_age") ("_nightly_docs")))) (r "1.75")))

(define-public crate-coap-message-utils-0.3.3 (c (n "coap-message-utils") (v "0.3.3") (d (list (d (n "cbor-diag") (r "^0.1.12") (d #t) (k 2)) (d (n "coap-message") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2.3") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "minicbor") (r "^0.19") (k 0)))) (h "1rs5s6bgwk2050i07g524k7b171l7z4wfsygssng7f8cp9vsd5iw") (f (quote (("error_unprocessed_coap_option") ("error_title") ("error_request_body_error_position") ("error_max_age") ("_nightly_docs")))) (r "1.75")))

