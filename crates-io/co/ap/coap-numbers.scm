(define-module (crates-io co ap coap-numbers) #:use-module (crates-io))

(define-public crate-coap-numbers-0.1.0 (c (n "coap-numbers") (v "0.1.0") (h "1abbr5clb6g3xpfnqcicyn05myf0pnzns9y6gwpyjaf0kaz1xnxn") (f (quote (("std") ("default" "std"))))))

(define-public crate-coap-numbers-0.1.1 (c (n "coap-numbers") (v "0.1.1") (h "0sjh8j2mzgqvvnyhfcspik2v6avf7a3mrwzi3c1f54fb2h5w8ai8") (f (quote (("std") ("default" "std"))))))

(define-public crate-coap-numbers-0.1.2 (c (n "coap-numbers") (v "0.1.2") (h "0shdd569jb55qdjpvj02lws1aazb2ijdjrvgwaryd60s9yx9nv44") (f (quote (("std") ("default" "std"))))))

(define-public crate-coap-numbers-0.2.0 (c (n "coap-numbers") (v "0.2.0") (h "1s826kf40falhhkqi5mlcdz80z6lmfa15kbg4ss5z6xcc7dkahcd") (f (quote (("alloc"))))))

(define-public crate-coap-numbers-0.2.1 (c (n "coap-numbers") (v "0.2.1") (h "1pgh4lv5q8mjiws0irzm72377365k0mxha9wb9w5nbacijfiyil9") (f (quote (("nightly_docs") ("alloc"))))))

(define-public crate-coap-numbers-0.2.2 (c (n "coap-numbers") (v "0.2.2") (h "0wqqq8w3pbnfc9xs7vkf4qk0szziazk4w6vh2np2sz9grcd8f1iy") (f (quote (("nightly_docs") ("alloc"))))))

(define-public crate-coap-numbers-0.2.3 (c (n "coap-numbers") (v "0.2.3") (h "1b0i6k0spydl8zkhzan98n9hgwy34awpl08w50fc2yqhcd2vwrp7") (f (quote (("nightly_docs") ("alloc"))))))

(define-public crate-coap-numbers-0.2.4 (c (n "coap-numbers") (v "0.2.4") (h "1yd5j2f4bm24qdis0g1v18h0wwqa403n3mkks48jrawk8disay3d") (f (quote (("nightly_docs") ("alloc"))))))

