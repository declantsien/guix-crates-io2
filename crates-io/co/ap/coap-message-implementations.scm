(define-module (crates-io co ap coap-message-implementations) #:use-module (crates-io))

(define-public crate-coap-message-implementations-0.1.0 (c (n "coap-message-implementations") (v "0.1.0") (d (list (d (n "coap-message") (r "^0.3.0") (d #t) (k 0)) (d (n "coap-message-utils") (r "^0.3.0") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "0i9npij7jwp2f2qh0gfy9kcn99bzmndlhmpmhi8n6lkx8kl3la9l") (r "1.65")))

(define-public crate-coap-message-implementations-0.1.1 (c (n "coap-message-implementations") (v "0.1.1") (d (list (d (n "coap-message") (r "^0.3.0") (d #t) (k 0)) (d (n "coap-message-utils") (r "^0.3.0") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "1kb12f698n8ip2k7hcy4iflhfjq3039hinb8xdw7wxvi0iwimcb5") (f (quote (("alloc") ("_nightly_docs")))) (r "1.65")))

(define-public crate-coap-message-implementations-0.1.2 (c (n "coap-message-implementations") (v "0.1.2") (d (list (d (n "coap-message") (r "^0.3.2") (d #t) (k 0)) (d (n "coap-message-utils") (r "^0.3.0") (d #t) (k 0)) (d (n "coap-numbers") (r "^0.2") (d #t) (k 0)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "0i1n7fsd814xfl0p192n21713njhd5di4mrv900dr1wz2494g7b2") (f (quote (("downcast") ("alloc") ("_nightly_docs")))) (r "1.65")))

