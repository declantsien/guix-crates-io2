(define-module (crates-io co nt continent-code) #:use-module (crates-io))

(define-public crate-continent-code-0.0.0 (c (n "continent-code") (v "0.0.0") (h "1zskjwmp9kcpp1rbbkpcmb5x9c647k4aj6jpm9m32ycfv3k010mx")))

(define-public crate-continent-code-0.1.0 (c (n "continent-code") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "09lk1b44zdljgm9ax8f8mhas91r78w8xc736rk3jpxka50va71a0") (f (quote (("std") ("default" "std"))))))

(define-public crate-continent-code-0.2.0 (c (n "continent-code") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "impl-macros") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1k1k5ya0wlg131145p513k6j9n1j5y58spn3qla2k54fzcmwy728") (f (quote (("std") ("default" "std"))))))

(define-public crate-continent-code-0.3.0 (c (n "continent-code") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "impl-macros") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1qm3fz962g15ajv0ksks62mydyx44x2n7fmsnm8qchs6zwhvrdyg") (f (quote (("std") ("default" "std"))))))

(define-public crate-continent-code-0.4.0 (c (n "continent-code") (v "0.4.0") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "impl-macros") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1nh6qxz5sq1d5jyl9vslz5nfw2h8s0hwf37c7p8p2y98d89817nm") (f (quote (("std") ("default" "std"))))))

