(define-module (crates-io co nt contrail-derive) #:use-module (crates-io))

(define-public crate-contrail-derive-0.0.0 (c (n "contrail-derive") (v "0.0.0") (d (list (d (n "contrail") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "14hjqrgd8hh533zibs8nyj636bjy28hjsphqcfrbhnf8i9v0dis6")))

(define-public crate-contrail-derive-0.0.1 (c (n "contrail-derive") (v "0.0.1") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "contrail") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xapqhjrywp254bg1rcvfgjxb8xdyxgfvncazxf9fp9x8l3hz9py")))

(define-public crate-contrail-derive-0.0.2 (c (n "contrail-derive") (v "0.0.2") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "contrail") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h6bgiapcaaa53c7y63ksiyxj4j1nf64bs0dz3r9xl8pvp6qvvxw")))

(define-public crate-contrail-derive-0.0.3 (c (n "contrail-derive") (v "0.0.3") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "contrail") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m2zl5b4ibpbpg91pi08295ymqxw5gy5jxrhvldjgscw83dg301y")))

(define-public crate-contrail-derive-0.1.0 (c (n "contrail-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "12zidrl06bxr5nm18qiis8lmafi68c4yqihl8kzd3nkqahxf78wa")))

(define-public crate-contrail-derive-0.2.0 (c (n "contrail-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1sbvk7bm4fbgzmwm2kkrmxqpjd10argx15fpyqcpij2ck9dwyn7c")))

