(define-module (crates-io co nt contract-verification-migrator) #:use-module (crates-io))

(define-public crate-contract-verification-migrator-0.1.0 (c (n "contract-verification-migrator") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "eyre") (r "^0.6.11") (d #t) (k 0)) (d (n "foundry-block-explorers") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c5ibp1zzh2x9p3n5y2ixyayn109rf3x3fivj442m9hxb97ix66p")))

