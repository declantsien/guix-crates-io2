(define-module (crates-io co nt continent) #:use-module (crates-io))

(define-public crate-continent-0.0.0 (c (n "continent") (v "0.0.0") (d (list (d (n "shape-core") (r "^0.1.14") (d #t) (k 0)) (d (n "shape-triangulation") (r "^0.1.1") (f (quote ("rand"))) (d #t) (k 0)))) (h "1x3h94phvg46p965cyllxazjr2scxqjkzj1ifl87yc3nm9qzjb82") (f (quote (("default"))))))

