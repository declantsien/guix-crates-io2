(define-module (crates-io co nt content-tree) #:use-module (crates-io))

(define-public crate-content-tree-0.1.0 (c (n "content-tree") (v "0.1.0") (d (list (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rle") (r "^0.1") (f (quote ("smallvec"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (f (quote ("union"))) (d #t) (k 0)))) (h "1jh4kzpafsnx447sgnj65wcs4b25v15d7y6a35dwzdk2ldj4s8vw")))

(define-public crate-content-tree-0.2.0 (c (n "content-tree") (v "0.2.0") (d (list (d (n "humansize") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rle") (r "^0.2.0") (f (quote ("smallvec"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (f (quote ("union"))) (d #t) (k 0)))) (h "1rdy4kkyl73l3wpanncp0vwarqx4hc112ax1606k2h6szyw06a77")))

