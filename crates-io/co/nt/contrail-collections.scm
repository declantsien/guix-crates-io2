(define-module (crates-io co nt contrail-collections) #:use-module (crates-io))

(define-public crate-contrail-collections-0.0.0 (c (n "contrail-collections") (v "0.0.0") (d (list (d (n "contrail") (r "^0") (d #t) (k 0)))) (h "19522lhlnvmbbxrgrwagx0vxv8njnvj0b17y040grlfsw3f6zr7a")))

(define-public crate-contrail-collections-0.0.1 (c (n "contrail-collections") (v "0.0.1") (d (list (d (n "contrail") (r "^0.0") (d #t) (k 0)))) (h "08v5ipzmwkkkvkcb68k3zm88vd6rsil98k4sddksdrwzgvpkrxy5")))

(define-public crate-contrail-collections-0.1.0 (c (n "contrail-collections") (v "0.1.0") (d (list (d (n "contrail") (r "^0.1") (d #t) (k 0)))) (h "1iwpnjgjn7vp5dg5g7qi3758lk637mdqwm5ag1i6f7iyyyjj2w3g")))

(define-public crate-contrail-collections-0.1.1 (c (n "contrail-collections") (v "0.1.1") (d (list (d (n "contrail") (r "^0.1") (d #t) (k 0)))) (h "0hr9cswdndx5glbdgwdj2mccrjwjlfr48fhk1lrkmx9fckziai4a")))

(define-public crate-contrail-collections-0.1.2 (c (n "contrail-collections") (v "0.1.2") (d (list (d (n "contrail") (r "^0.1") (d #t) (k 0)))) (h "0s58rm8nwg83xd8hs64wlkrmyw9mdik73cwz739l7frhf6kdpidy")))

(define-public crate-contrail-collections-0.2.0 (c (n "contrail-collections") (v "0.2.0") (d (list (d (n "contrail") (r "^0.2") (d #t) (k 0)))) (h "06ccrq3qgh8n2pc3brlc74zd7bb5a4y6pvxv3i2r0gq1gjyvs99j")))

(define-public crate-contrail-collections-0.2.1 (c (n "contrail-collections") (v "0.2.1") (d (list (d (n "contrail") (r "^0.2") (d #t) (k 0)))) (h "1h1831yjz1g5h4ww5x4r2v6am6xsk45inb4gjc8akhk56bdhml7l")))

(define-public crate-contrail-collections-0.3.0 (c (n "contrail-collections") (v "0.3.0") (d (list (d (n "contrail") (r "^0.3") (d #t) (k 0)))) (h "1rzcf1gkybfnpmw40dj78xq6b9fm7p2rgdfqiplmhla41z7xvqki")))

