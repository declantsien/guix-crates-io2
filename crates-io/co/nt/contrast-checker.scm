(define-module (crates-io co nt contrast-checker) #:use-module (crates-io))

(define-public crate-contrast-checker-0.1.0 (c (n "contrast-checker") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "contrast") (r "^0.1.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rust-hsluv") (r "^0.1.4") (d #t) (k 0)))) (h "0zdqkv9dvddk2j34y1jvll29qn0ah7nwgqy6kv860xihfm6vwp6x")))

(define-public crate-contrast-checker-0.1.1 (c (n "contrast-checker") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "contrast") (r "^0.1.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)) (d (n "rust-hsluv") (r "^0.1.4") (d #t) (k 0)))) (h "02l83jvp06xjsjs1syq5fjy6i0w7pkghhmy06iairqydgqmbnpvq")))

