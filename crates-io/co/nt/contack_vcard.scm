(define-module (crates-io co nt contack_vcard) #:use-module (crates-io))

(define-public crate-contack_vcard-0.1.1 (c (n "contack_vcard") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "contack") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "vcard") (r "^0.4.7") (d #t) (k 0)))) (h "0qq2igidc7jbxsv5jdcs10z42ykg7bcgvjgkmwf85786514vdg03") (y #t)))

(define-public crate-contack_vcard-0.1.2 (c (n "contack_vcard") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "contack") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "vcard") (r "^0.4.7") (d #t) (k 0)))) (h "0zsqhays6y9frhhp89ydiknifh85kmy6b668f8qrhjhri5n18cgf") (y #t)))

(define-public crate-contack_vcard-0.1.6 (c (n "contack_vcard") (v "0.1.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "contack") (r "^0.1.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "vcard") (r "^0.4.7") (d #t) (k 0)))) (h "1kl2653ikpm1g6m7d5a6yc6dd8cfvkx3n33m3xic9rsbvs0brp44") (y #t)))

(define-public crate-contack_vcard-0.1.7 (c (n "contack_vcard") (v "0.1.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "contack") (r "^0.1.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "vcard") (r "^0.4.7") (d #t) (k 0)))) (h "05c8bl329aasm1iy49fx5gclznvnj0zyamyh7lscxs770f0plvcv") (y #t)))

