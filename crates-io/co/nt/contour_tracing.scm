(define-module (crates-io co nt contour_tracing) #:use-module (crates-io))

(define-public crate-contour_tracing-1.0.0 (c (n "contour_tracing") (v "1.0.0") (h "1wrgqfqi87h3d530vmgla6d1kz39khly7qxn09lpagnvm5j5j9jq") (y #t)))

(define-public crate-contour_tracing-1.0.1 (c (n "contour_tracing") (v "1.0.1") (h "1hyrdqshhwiyb6pp1nnr2gxpisxaxdwkp3z5cibys1xqybdh3b94") (y #t)))

(define-public crate-contour_tracing-1.0.2 (c (n "contour_tracing") (v "1.0.2") (h "04qi800yw534im76gcb6aki5jb43mrkafcaclx9k4sp8pwrs4hr4") (y #t)))

(define-public crate-contour_tracing-1.0.3 (c (n "contour_tracing") (v "1.0.3") (h "09w0vd27hk30msyk5pg5z3b285fdbkbjwmih74l7wap2mp4lhlni") (y #t)))

(define-public crate-contour_tracing-1.0.4 (c (n "contour_tracing") (v "1.0.4") (d (list (d (n "image") (r "^0.23.14") (o #t) (d #t) (k 0)))) (h "144p8yqc2f4fvpf7bf7vvhavnhca9vn606mnjaxlwvqjzg1jaxyb") (y #t) (r "1.56")))

(define-public crate-contour_tracing-1.0.5 (c (n "contour_tracing") (v "1.0.5") (d (list (d (n "image") (r "^0.23.14") (o #t) (d #t) (k 0)))) (h "1hrhhzi38ajfpc94sj3wqbsa37arhvpqaar656nmsbzzvpw4fipg") (y #t) (r "1.56")))

(define-public crate-contour_tracing-1.0.6 (c (n "contour_tracing") (v "1.0.6") (d (list (d (n "image") (r "^0.23.14") (o #t) (d #t) (k 0)))) (h "167mwvzlk72vckrjdwhdf2cy5hxskllcb8rn3d7r22rai7283xj9") (y #t) (r "1.56")))

(define-public crate-contour_tracing-1.0.7 (c (n "contour_tracing") (v "1.0.7") (d (list (d (n "image") (r "^0.23.14") (o #t) (d #t) (k 0)))) (h "0fk6sqn06q75r8qw7wzry63psxr274f3lsnfbn70sgbaqm78c3jb") (y #t) (r "1.56")))

(define-public crate-contour_tracing-1.0.8 (c (n "contour_tracing") (v "1.0.8") (d (list (d (n "image") (r "^0.23.14") (o #t) (d #t) (k 0)))) (h "1kkgh0p9b0lrp1lv32355y55385ll9wwfax8l7hh0wfyi6557ns6") (y #t) (r "1.56")))

(define-public crate-contour_tracing-1.0.9 (c (n "contour_tracing") (v "1.0.9") (d (list (d (n "image") (r "^0.23.14") (o #t) (d #t) (k 0)))) (h "05spxv9lwgc9y5lcab1l57cwpiyl1rgc7qwdp9z0k2nxg337dnks") (f (quote (("default") ("array")))) (y #t) (r "1.56")))

(define-public crate-contour_tracing-1.0.10 (c (n "contour_tracing") (v "1.0.10") (d (list (d (n "image") (r "^0.23.14") (o #t) (d #t) (k 0)))) (h "0k2fd70mlr3kcfv8prsrwr8s3324m1nb58anf7n8lscg5r9dh7ss") (f (quote (("default") ("array")))) (y #t) (r "1.56")))

(define-public crate-contour_tracing-1.0.11 (c (n "contour_tracing") (v "1.0.11") (d (list (d (n "image") (r "^0.24.0") (o #t) (d #t) (k 0)))) (h "1bd0cifrb2rsyv1xvd835wrn6s9b3vydhkhya4534hi6s5xhzw11") (f (quote (("default") ("array")))) (y #t) (r "1.58.1")))

(define-public crate-contour_tracing-1.0.12 (c (n "contour_tracing") (v "1.0.12") (d (list (d (n "image") (r "^0.24.1") (o #t) (d #t) (k 0)))) (h "0djrc8333j6jb9s94xa0a35x7dygk2nfh2k99fy9w17lnirfzvy9") (f (quote (("default") ("array")))) (r "1.58.1")))

