(define-module (crates-io co nt conta) #:use-module (crates-io))

(define-public crate-conta-0.0.0 (c (n "conta") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.4") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1mqy30zghxdvwgr5p27h47in3k0db1cf5hm4jf1laf7qrf76v2bv")))

(define-public crate-conta-0.0.1 (c (n "conta") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "ccli") (r "^0.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking" "json" "default-tls"))) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "1k82vvxkmrg5hnm2ciyhz12vcs0n63s50llsfzmas7y2257gdzzj")))

