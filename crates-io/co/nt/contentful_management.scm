(define-module (crates-io co nt contentful_management) #:use-module (crates-io))

(define-public crate-contentful_management-0.1.0 (c (n "contentful_management") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05isq6zzjca894iwr43dyw9imp6maq1z0p0cn2hv09dbrqc5gc03") (y #t)))

(define-public crate-contentful_management-0.1.1 (c (n "contentful_management") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v89k3drgcmynp076j5gbggqxfs9m39i977x7c7qdrn5ij9dfr9a")))

(define-public crate-contentful_management-0.1.2 (c (n "contentful_management") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dybaqf36b70m4jdv0yfswnmf7g5lyl95p8mx8w586q3ipjr05ah")))

