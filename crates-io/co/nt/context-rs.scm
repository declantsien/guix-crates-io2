(define-module (crates-io co nt context-rs) #:use-module (crates-io))

(define-public crate-context-rs-0.1.0 (c (n "context-rs") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 2)))) (h "0fgvhs97hz5ywcyazi8v4n056xp9hp2il0kxig222fr1x3mvvvn1")))

(define-public crate-context-rs-0.2.0 (c (n "context-rs") (v "0.2.0") (d (list (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "04m08g9l4xsrwnfxjx0vicy0dn1cyhxg9hjxwd0i0yjfb7afwwj0") (y #t)))

(define-public crate-context-rs-0.2.1 (c (n "context-rs") (v "0.2.1") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1yx027zbbxbcmqdaw0j5bjm7z29gvrgvjkxzjwihizm0r0ph4laa") (f (quote (("time" "std" "tokio/time") ("std" "tokio/sync") ("default" "std")))) (y #t)))

(define-public crate-context-rs-0.2.2 (c (n "context-rs") (v "0.2.2") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1a3rasih69b4lck95sv3ids1qam16lvl1gfkspb8bwfhf3p5nvlb") (f (quote (("time" "std" "tokio/time") ("std" "tokio/sync") ("default" "std"))))))

