(define-module (crates-io co nt control-code) #:use-module (crates-io))

(define-public crate-control-code-0.1.0 (c (n "control-code") (v "0.1.0") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0k0d2my66jirdhjlwylwap95jh51ma8h9vh1znn0rgwjwy750sgp")))

(define-public crate-control-code-0.1.1 (c (n "control-code") (v "0.1.1") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0gwylcdwm8gvydvhs7nmr7sp9kdzkppd82wp5xrvhzl97zv1mfxk")))

(define-public crate-control-code-0.1.2 (c (n "control-code") (v "0.1.2") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "08z6p3ibqrbx8xy7v8crrxgywisinfybp2jlakh16zsfipzyfw11")))

(define-public crate-control-code-0.2.0 (c (n "control-code") (v "0.2.0") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0si07q6ihgyk0b7nfa51ngl88xns4b0mnwd6qzjmgjp2w2y51z24")))

(define-public crate-control-code-0.2.1 (c (n "control-code") (v "0.2.1") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0c4vmi7778jzvhvgkfgsglwl2ykhgmjvkmmhk4rl9nz3j068x4fs")))

(define-public crate-control-code-0.2.2 (c (n "control-code") (v "0.2.2") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0ggrx7bkwsiq5ypa554rf3z820vhgq67lkmywfx09ran9nj2s5ls")))

(define-public crate-control-code-0.2.3 (c (n "control-code") (v "0.2.3") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "10nsnl9bb3fx5zxnryxlc3hbjdbrzwghp8apxxdgcfih1j1kxrl5")))

(define-public crate-control-code-0.2.4 (c (n "control-code") (v "0.2.4") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "1rhrfw837f3mmapg2cqhdq7g6r8dmvdwi7bpvg222b0wxhcdys2k")))

(define-public crate-control-code-0.2.5 (c (n "control-code") (v "0.2.5") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0ibszzy9s4gh13a9lgh8bgkfkfif5vcxgjvakncp3w1p72yism1h")))

(define-public crate-control-code-0.3.0 (c (n "control-code") (v "0.3.0") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "1chhbwi7d0q986w09kj0g8z2z983ala7n7c4knjm276il6044r0p") (y #t)))

(define-public crate-control-code-0.2.6 (c (n "control-code") (v "0.2.6") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "1z54raw9d7i0zk1byaw9fggl96252rr6qb11ssc7i1bd8vnhfqqg")))

(define-public crate-control-code-0.2.7 (c (n "control-code") (v "0.2.7") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "015vfkk03kl8azlk5m7q8f5fqg4v84whdpwkmnaq220fcmmlb4pj")))

(define-public crate-control-code-0.2.8 (c (n "control-code") (v "0.2.8") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "09sjq9xxwxr6g4m3zywk544aq6s7fvjhlgnm7hvy7jv08jcqijqy")))

(define-public crate-control-code-0.2.9 (c (n "control-code") (v "0.2.9") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "1lwz3nvzb3db3b5xi4j0190vv0mwh8d00mz8f881va4vh4j1hhlc")))

(define-public crate-control-code-0.2.10 (c (n "control-code") (v "0.2.10") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0pjw9wh1zj1mcs4hx3vhdjr7317hf7k442f3miw2kdvmpvjc3nsy")))

(define-public crate-control-code-0.3.1 (c (n "control-code") (v "0.3.1") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0kq06ykpwqnpcwh6aaqa7zg0n4sxkdhpi6kz2c563ysq4xkf5mjz")))

(define-public crate-control-code-0.3.2 (c (n "control-code") (v "0.3.2") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "03wcz44a9qxx48ly7gvih84h9n27j4kg1v438hjx0kwhayw9smm5")))

(define-public crate-control-code-0.3.3 (c (n "control-code") (v "0.3.3") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0p8sa7ij1aj0k1v3xd4hs42jjn0j1hz2x92s89ydjwnnpjc9f4aa")))

(define-public crate-control-code-0.3.4 (c (n "control-code") (v "0.3.4") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "0ka60vsv7b94vmk7vq8jrcklnshlwvcgd1lf5mq7z240ks5hviiz")))

(define-public crate-control-code-0.3.5 (c (n "control-code") (v "0.3.5") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "1m8i428m6jhhvxn3dd6ghckbg6waccm1dhbsh4zrglwh2jr50928")))

(define-public crate-control-code-0.4.0 (c (n "control-code") (v "0.4.0") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)))) (h "13sbfabqy3cbm81y8j96rm13ya19603ma4jighz1zqasi6gjvpsb")))

(define-public crate-control-code-0.4.1 (c (n "control-code") (v "0.4.1") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1jv18564qdc307sbkwlzx88j58b1z20c9dp1flbs2pqbcrfla7vg")))

(define-public crate-control-code-0.4.2 (c (n "control-code") (v "0.4.2") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "0xf6baqgmm2kjdvg7wyn1qyvycpphqy6wrfhz6sy9wl64n14y5cy")))

(define-public crate-control-code-0.4.3 (c (n "control-code") (v "0.4.3") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1f9ny9v2wmbka59i89qppsw4843v9fb8frsl8gsy5a0j05xhas25")))

(define-public crate-control-code-0.4.4 (c (n "control-code") (v "0.4.4") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1674l1d7klgiixdbrmwmyk6q37vcr3f5j56nm3j6fj74mnkzgbnm")))

(define-public crate-control-code-0.4.5 (c (n "control-code") (v "0.4.5") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "056m3bsgm0psrrg8g2b3y3fjhvn0ryzmnfvvmm74nwqjwzbp8yq9")))

(define-public crate-control-code-0.4.6 (c (n "control-code") (v "0.4.6") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1rb2m97wra8mqk2b8h52y3r66d2hpr4sk3mj09f5ay13qlgy537q")))

(define-public crate-control-code-0.4.7 (c (n "control-code") (v "0.4.7") (d (list (d (n "nom") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1jvhhp3z79brnjpcknivpi61zw6xzl5z7sa9g91k8wz29axihmcs")))

(define-public crate-control-code-0.4.8 (c (n "control-code") (v "0.4.8") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "04fzj5jhg95hqcckf1bznniccx45nzsd9qc1dhk74yc7baxmlz0f")))

(define-public crate-control-code-0.4.9 (c (n "control-code") (v "0.4.9") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1hyx1yjhrv4lafabyh9zl7yjnv3f1ixql8rykls6543xmikkf4dw")))

(define-public crate-control-code-0.4.10 (c (n "control-code") (v "0.4.10") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1wxwj7b76i3b7j70yvzm4vaz91gj52vgp2dvvwcd6ni0fwiszii3")))

(define-public crate-control-code-0.5.0 (c (n "control-code") (v "0.5.0") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "152m2gpk35hp40hhw9np9cgxnzmdjkivxz6wriw0y319p6igvlcj")))

(define-public crate-control-code-0.5.1 (c (n "control-code") (v "0.5.1") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1dk7rgklcbgcddwhhkdlg8hchy7kklb87c2qq3qgwg0ki7x4c9ap")))

(define-public crate-control-code-0.5.2 (c (n "control-code") (v "0.5.2") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1a4f4k0py8l8wi051wmk75vsrxlbl1b5ammsxvacsdj8bz1b91ws")))

(define-public crate-control-code-0.5.3 (c (n "control-code") (v "0.5.3") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "16mvsqjj9yjy2lbycicbgf61ic1v6h55p0hnmdwxd8b7qa58y1a0")))

(define-public crate-control-code-0.5.4 (c (n "control-code") (v "0.5.4") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1inrysnz8dxsvnls7gy69j1vw0n2m8za552fsm362bw4dwlqvv3d")))

(define-public crate-control-code-0.5.5 (c (n "control-code") (v "0.5.5") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "0jpyx5s5phy85srq6fcmb316z0swnp4g6brssfmnm5xvh3wlhshp")))

(define-public crate-control-code-0.5.6 (c (n "control-code") (v "0.5.6") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1hbf4h80wxj54c700f8x5kv1cj3dcz3nxhlxybac2pc6l94dbsqw")))

(define-public crate-control-code-0.5.7 (c (n "control-code") (v "0.5.7") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "0gvg348z5zlhyp346xav6q17ij66wh0spjx2xdaw154qnzsf1sh5")))

(define-public crate-control-code-0.6.0 (c (n "control-code") (v "0.6.0") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "0f3znypjrp474bmwvjc5w1jkds89mk5r8zmh13wm8ss0kvvy9b3z") (y #t)))

(define-public crate-control-code-0.6.1 (c (n "control-code") (v "0.6.1") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.2") (d #t) (k 0)))) (h "1vhd0shqj12qlq3f8vrwmc6n65icvy8cxzsndnddbmk8cx3fwlnp")))

(define-public crate-control-code-0.6.2 (c (n "control-code") (v "0.6.2") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)))) (h "1162nvi13952hnjkrjnar0l6n65hvzy0fwls2j7hz3hc4h8810dj")))

(define-public crate-control-code-0.6.3 (c (n "control-code") (v "0.6.3") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)))) (h "12biqlk2fzms36vqar77p7xhaax6ygd4qgizdwz7ygravs147k56")))

(define-public crate-control-code-0.7.0 (c (n "control-code") (v "0.7.0") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.3") (d #t) (k 0)))) (h "0jzk2fc2n0w72mikwdrpyadcvm74xj8a9m6mxzv175iwm4b04h1k")))

(define-public crate-control-code-0.7.1 (c (n "control-code") (v "0.7.1") (d (list (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)))) (h "0gv0kg3ayph69zyx5y3gq77hb1nmxgy4x6bp816w0fd8gm9sr4fd")))

