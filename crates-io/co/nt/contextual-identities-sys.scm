(define-module (crates-io co nt contextual-identities-sys) #:use-module (crates-io))

(define-public crate-contextual-identities-sys-0.1.0 (c (n "contextual-identities-sys") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.44") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.67") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.17") (d #t) (k 0)))) (h "1pm0xmf3jz0ky1nn703ks39lvp98cldbz6dnrvl4c4bmrvyfx33m") (y #t)))

(define-public crate-contextual-identities-sys-0.1.1 (c (n "contextual-identities-sys") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.44") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.67") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.17") (d #t) (k 0)))) (h "0xwfz3a8ig8a3x8xrllzy8rfl0x4h3zlg4w476ni2scj2vbgg6np") (y #t)))

