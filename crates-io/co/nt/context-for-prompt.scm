(define-module (crates-io co nt context-for-prompt) #:use-module (crates-io))

(define-public crate-context-for-prompt-0.1.0 (c (n "context-for-prompt") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1zyf578520nzrk6sa0fzaii3drradbhmpwax8by15xigk549yxgq")))

