(define-module (crates-io co nt containers-rs) #:use-module (crates-io))

(define-public crate-containers-rs-0.1.0 (c (n "containers-rs") (v "0.1.0") (h "0r0y51vm922sn87bvc9nrych3mxi7wrp2w6fy143zf7x6hl9kxfv")))

(define-public crate-containers-rs-0.1.1 (c (n "containers-rs") (v "0.1.1") (h "1wq4h9bf85hiczd3gx2sd1f4ds1jpb56x4gka9i0zw5w19lh9z6h")))

(define-public crate-containers-rs-0.1.2 (c (n "containers-rs") (v "0.1.2") (h "1rgbrlh9hx1xx4bdng2vh6zz1lm2fsynrfff6l5qfwzilhhdblwy")))

(define-public crate-containers-rs-0.1.3 (c (n "containers-rs") (v "0.1.3") (h "1y89jd2dk8syyp92q8zj3bid3k2hlf1x89ldz5m0z5mvlcqbqj7c")))

(define-public crate-containers-rs-0.1.4 (c (n "containers-rs") (v "0.1.4") (h "0grqfqn0p2yhrybwz2ni52ql8izg084vn6zs2y6mrnv8j2h5wf3a")))

(define-public crate-containers-rs-0.1.5 (c (n "containers-rs") (v "0.1.5") (h "11jvfrqmsg88762qx3v7vcfi187a0rpbn86g7ih3g4p44r6slsll")))

(define-public crate-containers-rs-0.1.6 (c (n "containers-rs") (v "0.1.6") (h "193wg5z7vwsbydjx8n9b23lcy28dg5qjw4i7xwhgfqc5b3731pg7")))

(define-public crate-containers-rs-0.1.7 (c (n "containers-rs") (v "0.1.7") (h "1rgcraxacrxj4ndc9kw841yyra8sqzjch4xwmq3901v49m9lhqkz")))

(define-public crate-containers-rs-0.1.8 (c (n "containers-rs") (v "0.1.8") (h "1j7kvvcfs5xd3vfgnp5k8sb5dlm554x35d4lmvr0742kp4xvimzy")))

(define-public crate-containers-rs-0.1.9 (c (n "containers-rs") (v "0.1.9") (h "094n4cd19zy322fi028l4m4308hq4shydgq7a3dga0j1c3b2251b")))

(define-public crate-containers-rs-0.2.0 (c (n "containers-rs") (v "0.2.0") (h "1f7npsxkz8sqrn6ccfp146b17h2lav85mrvyv60fn2hsdnprm3mc")))

(define-public crate-containers-rs-0.3.0 (c (n "containers-rs") (v "0.3.0") (h "05r4znx791b0jkvi5l6fsjisccmy9d9cjviy188yxrm2xqx9wqp9")))

(define-public crate-containers-rs-0.4.0 (c (n "containers-rs") (v "0.4.0") (h "1jgnzrflx827jrqmn79wmfhl31xv39iyagiidn0mw12l648g4f3y")))

(define-public crate-containers-rs-0.5.0 (c (n "containers-rs") (v "0.5.0") (h "1cssdpisfsfm7984j1bxf2pdrfcgf2abl5fpy95vrnqc3v67kvk2")))

(define-public crate-containers-rs-0.5.1 (c (n "containers-rs") (v "0.5.1") (h "0s4kaqqxsmn3x0brj93bd0n0jssgn1yb15vay1wzpvxf3knvl5aw")))

