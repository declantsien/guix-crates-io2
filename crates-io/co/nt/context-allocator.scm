(define-module (crates-io co nt context-allocator) #:use-module (crates-io))

(define-public crate-context-allocator-0.1.0 (c (n "context-allocator") (v "0.1.0") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "0kh4qqc0h7c24rp6nqfsa3fp1khr0fgfw5wzsiy1i584d5vaq2ns")))

(define-public crate-context-allocator-0.1.1 (c (n "context-allocator") (v "0.1.1") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1s19gc4hai6n81jlv5mas8q95grwvfv0x7pfz8a0fbiidxqks4qr")))

(define-public crate-context-allocator-0.1.2 (c (n "context-allocator") (v "0.1.2") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "0qbqzlp154hhsa2lxpb1s1cczzgsf4xp2l8gqvndyx1bkr4fwwgk")))

(define-public crate-context-allocator-0.1.3 (c (n "context-allocator") (v "0.1.3") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1fmm95k0xd37d2fx6hkzjdbsv5qwk1lf46fy6j8wpbnyb22n533k")))

(define-public crate-context-allocator-0.1.4 (c (n "context-allocator") (v "0.1.4") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1i7y1jqlgngwnp8psb9xak8ax1nqb8haddhfg2p9kbajrvd0i525")))

(define-public crate-context-allocator-0.1.5 (c (n "context-allocator") (v "0.1.5") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "0z070xcqqhigqr9vsid4qj2jjc84wn4pwcvjnrq9siw28v2993l0")))

(define-public crate-context-allocator-0.1.6 (c (n "context-allocator") (v "0.1.6") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1h2lxi09ilwjkgc9vyky6b1h5dc9j8jrryi3hrx2ry86v3acc3n1")))

(define-public crate-context-allocator-0.1.7 (c (n "context-allocator") (v "0.1.7") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "08cma6077dxl1lkpfcndp2vlvqrixw8iiwpdlrfvwldq59334v4g")))

(define-public crate-context-allocator-0.2.0 (c (n "context-allocator") (v "0.2.0") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "1pk6098whip925h6b13r15s09wr9km1kjm0vzszmcpi48dd66psh")))

(define-public crate-context-allocator-0.2.1 (c (n "context-allocator") (v "0.2.1") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "158k7mcgr1l6m72kfvhvjr2wsdby30b0lkc308zh0y384srzz68z")))

(define-public crate-context-allocator-0.2.2 (c (n "context-allocator") (v "0.2.2") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "0ma3x5c0wh1pznmil3z426rqd4xn5wxnrm9j1k9zk6s0vn9165z9")))

(define-public crate-context-allocator-0.2.3 (c (n "context-allocator") (v "0.2.3") (d (list (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(unix)") (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (t "cfg(any(target_os = \"android\", target_os = \"linux\"))") (k 0)))) (h "0dk0hm60gbz6f5v6lk90pg5lxx3kjikks4h4cj6c08a2fch85s5k")))

