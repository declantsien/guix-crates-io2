(define-module (crates-io co nt contest-algorithms) #:use-module (crates-io))

(define-public crate-contest-algorithms-0.1.0 (c (n "contest-algorithms") (v "0.1.0") (d (list (d (n "bit-vec") (r "0.6.*") (d #t) (k 0)))) (h "0vc65mj3v48m8q7j31dscc53yqc2wk899mggnjpvikv6rxqlmpy4")))

(define-public crate-contest-algorithms-0.1.1 (c (n "contest-algorithms") (v "0.1.1") (d (list (d (n "bit-vec") (r "0.6.*") (d #t) (k 0)))) (h "01fnkf9yi26ahv3ajx4px3h2izswk004w01cdi6zyzkixsylysvb")))

(define-public crate-contest-algorithms-0.1.2 (c (n "contest-algorithms") (v "0.1.2") (d (list (d (n "bit-vec") (r "0.6.*") (d #t) (k 0)))) (h "14z0m5k1qcaknvvxnvbzw4yq1z959imas9n9m2jdlypr2yrhx3k2")))

(define-public crate-contest-algorithms-0.1.3 (c (n "contest-algorithms") (v "0.1.3") (d (list (d (n "bit-vec") (r "0.6.*") (d #t) (k 0)))) (h "0cawrzly68pyrvhmmhr12g3ja99ligw36v1q7v437zfghvcxliyq")))

(define-public crate-contest-algorithms-0.1.4 (c (n "contest-algorithms") (v "0.1.4") (h "118csd3pkxr8jyyr70sirhfp50da219669dz1qg23ny0hlmq3l8l")))

(define-public crate-contest-algorithms-0.2.0 (c (n "contest-algorithms") (v "0.2.0") (h "0ff4ysybv0ddyqm9cclhj3zg8vyqv8alwjwz2p0y7j68wh9f9awc")))

(define-public crate-contest-algorithms-0.2.1 (c (n "contest-algorithms") (v "0.2.1") (h "1l0b0li32d446vhvxbrmvg6c2fflia2kmbiy7w2ads8xjzcbaa1x")))

(define-public crate-contest-algorithms-0.3.0 (c (n "contest-algorithms") (v "0.3.0") (h "1264b13vjwxd02zgw7qihbm1qjpw2mpaj1fybfiwq93q8bp2kwck")))

