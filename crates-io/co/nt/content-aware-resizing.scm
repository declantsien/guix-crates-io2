(define-module (crates-io co nt content-aware-resizing) #:use-module (crates-io))

(define-public crate-content-aware-resizing-0.1.0 (c (n "content-aware-resizing") (v "0.1.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "image_energy") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0cirx7f8z2v5zykpjd463x80hccsdmk1sfkivd0hi0gigx63avbl")))

