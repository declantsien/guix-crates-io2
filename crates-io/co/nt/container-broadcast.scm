(define-module (crates-io co nt container-broadcast) #:use-module (crates-io))

(define-public crate-container-broadcast-0.0.1 (c (n "container-broadcast") (v "0.0.1") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "1bq5mggrdjdqd8vb9cq8qksd48888ri6v7079p8383d9by5akjib") (y #t)))

(define-public crate-container-broadcast-0.0.2 (c (n "container-broadcast") (v "0.0.2") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "1gzcr3cyahh7ay2b59a3bdz6z3db5zrqvp55z5d6w21p4avnv26a") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-container-broadcast-0.0.3 (c (n "container-broadcast") (v "0.0.3") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "1qakqmxjcnqycsy213j0s2lx2anlsfcajd5c9xccycqnb87c2647") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-container-broadcast-0.0.4 (c (n "container-broadcast") (v "0.0.4") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "1rg0fmmd4jn1pzx4cg7gd1mpvbhxwfzvc64zhy1fpzn18jyhn0hd") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-container-broadcast-0.0.5 (c (n "container-broadcast") (v "0.0.5") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "10mj2hzy5qivdk70i8q8ji2br89w9jhn2jhxn0mqkx8nqhy20awc") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-container-broadcast-0.0.6 (c (n "container-broadcast") (v "0.0.6") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "0f2fdlsczzn240m08hifrrmrdvqb6462i3jk28jpjq6fh89gc38k") (f (quote (("std") ("nightly-features") ("default" "std")))) (y #t)))

(define-public crate-container-broadcast-0.0.7 (c (n "container-broadcast") (v "0.0.7") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "1mzprn1iqy0alsrjgzzp0sm1la2jl5mc8xxgwj9hlbdyyix42kkr") (f (quote (("std") ("nightly-features") ("default" "std"))))))

(define-public crate-container-broadcast-0.0.8 (c (n "container-broadcast") (v "0.0.8") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)))) (h "08pdmfbqgcjah0r666bhkphbyyzrvwg0c8irijg7a38lv23ah7rj") (f (quote (("std") ("nightly-features") ("default" "std"))))))

