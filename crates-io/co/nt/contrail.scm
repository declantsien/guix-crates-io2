(define-module (crates-io co nt contrail) #:use-module (crates-io))

(define-public crate-contrail-0.0.0 (c (n "contrail") (v "0.0.0") (d (list (d (n "downcast-rs") (r "^1.0.3") (d #t) (k 0)))) (h "1h3gaqzq9c4vsylmv2c37sklr5sdyyy11xbhgixl0h1mh02hr877")))

(define-public crate-contrail-0.0.1 (c (n "contrail") (v "0.0.1") (d (list (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "0air8psh8xjhv0hb8mpicgc6rzh1v0y7dpylc5b8anaaqs76w083")))

(define-public crate-contrail-0.0.2 (c (n "contrail") (v "0.0.2") (d (list (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "0kh9asav26qf6w48fb2m3ab9gcviky1sjgppmxabl8d7d9pb9b3i")))

(define-public crate-contrail-0.0.3 (c (n "contrail") (v "0.0.3") (d (list (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "037bjdfl7nic2spa8xm0cmv0xvdxk6nzsrpi9n63gl4cn2xad6m9")))

(define-public crate-contrail-0.0.4 (c (n "contrail") (v "0.0.4") (d (list (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "16frpf8fgsybdgs17v4ls10i98cpw7mb1rvww4ln1fhymw47hcwv")))

(define-public crate-contrail-0.0.5 (c (n "contrail") (v "0.0.5") (d (list (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "1k3536hiq8gc9j76p42cn3l9yxmldcwj8hr2h5109yk6fhs13krg")))

(define-public crate-contrail-0.0.6 (c (n "contrail") (v "0.0.6") (d (list (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "1aq7lxdhfz02c8m95iccfmdgk453fjnivzwiybg56y5awf5hx0rv")))

(define-public crate-contrail-0.1.0 (c (n "contrail") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "0gxa6q8if28585kwbl15igg6pc6f3fkn9wa3jl9528rf75ykxcly")))

(define-public crate-contrail-0.1.1 (c (n "contrail") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.5.0") (d #t) (k 2)))) (h "1qx2n26va5y3x8b3f9xi4pca8wha83k349p5b3l32ywbph889nsx")))

(define-public crate-contrail-0.1.2 (c (n "contrail") (v "0.1.2") (d (list (d (n "contrail-derive") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "10bz6m2890vsbgs0j5iqcw4z1kr6079lrzq4snsxyhv4mzl7f2hq")))

(define-public crate-contrail-0.1.3 (c (n "contrail") (v "0.1.3") (d (list (d (n "contrail-derive") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "1ifxa0cr64h24msacdmd6n34633np5swih53la13ln9k2lbf77m6")))

(define-public crate-contrail-0.1.4 (c (n "contrail") (v "0.1.4") (d (list (d (n "contrail-derive") (r "^0.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "124174iis9chwfk0vk5nyjlvviakl98r2lpc9byjaxh317z80c7g")))

(define-public crate-contrail-0.1.5 (c (n "contrail") (v "0.1.5") (d (list (d (n "contrail-derive") (r "^0.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "059s7h5p901iivq8zc3wxirwfm14vrbqf4qqih3ls3i5j07m7icz")))

(define-public crate-contrail-0.2.0 (c (n "contrail") (v "0.2.0") (d (list (d (n "contrail-derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.7") (d #t) (k 2)))) (h "1xsaxwxxxnzg6rl6pdagv39j68dvgr3xqr9j95s9wgnc4nbx2qns")))

(define-public crate-contrail-0.2.1 (c (n "contrail") (v "0.2.1") (d (list (d (n "contrail-derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0q67kjki35id1xhsd6lyd7rjl4hj1rvr2lsiixxv3b9ggpacpd45")))

(define-public crate-contrail-0.3.0 (c (n "contrail") (v "0.3.0") (d (list (d (n "contrail-derive") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0phx2w5p5g2034n66cf1jj7bnn10dsaaf5lzvl8swn8y1xyxmggz")))

