(define-module (crates-io co nt contraband_diesel) #:use-module (crates-io))

(define-public crate-contraband_diesel-0.1.0 (c (n "contraband_diesel") (v "0.1.0") (d (list (d (n "contraband") (r "^0.1.0") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres" "sqlite" "r2d2" "uuid"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)))) (h "09py5fizbglqyj44c3gvds07wlccd7bi9p1lm0nbc5b38pa17y2c")))

