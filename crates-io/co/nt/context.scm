(define-module (crates-io co nt context) #:use-module (crates-io))

(define-public crate-context-0.1.0 (c (n "context") (v "0.1.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)))) (h "1zkjsc5lp2fapfcz6yliv7704mhpm43z0bc6i2kbq849lhvgl20y")))

(define-public crate-context-0.1.1 (c (n "context") (v "0.1.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)))) (h "1ba9mz2f720063vg492zdw5jscm874vdlgjz3yi9vi9rcay3rm5n")))

(define-public crate-context-0.1.2 (c (n "context") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.12") (d #t) (k 1)) (d (n "log") (r "^0.3.1") (d #t) (k 1)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)))) (h "12v2b4p09yj1jdawd5z3kc5zxpy2spnqc6rcpy1siq8rhwj00x0g")))

(define-public crate-context-0.1.3 (c (n "context") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3.12") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 1)) (d (n "memmap") (r "^0.2") (d #t) (k 0)))) (h "017gxdl0khmy87c3xq590m68klla9h91lzmbcj6kxjwg2is94787")))

(define-public crate-context-0.1.4 (c (n "context") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3.12") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 1)) (d (n "memmap") (r "^0.2") (d #t) (k 0)))) (h "1kjz2rbi1k96kc7jdvvlgmwhp9f1pzw7qqam2569ci9jhsmg9mjb")))

(define-public crate-context-0.2.1 (c (n "context") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3.12") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 1)) (d (n "memmap") (r "^0.2") (d #t) (k 0)))) (h "0sk7hg76kszjmlbx0yv9v5yzi7w9a3wkhn03ci0b2rvfsyy3l7vd")))

(define-public crate-context-1.0.0 (c (n "context") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1h5pvd8cm7nl2f4fnnv5ba1v1n9pzlbx42540w0dwmp00snrn1rj") (f (quote (("nightly"))))))

(define-public crate-context-1.0.1 (c (n "context") (v "1.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "13mdqbbx4wc2imql44y4y2qnx7a97zkkxy8ii407h3ay77cg94kh") (f (quote (("nightly"))))))

(define-public crate-context-2.0.0 (c (n "context") (v "2.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "17la8l82j9x6lvb1ls5g8vxnm3gnhyky787vwlwga15gi5v6m1b0") (f (quote (("nightly"))))))

(define-public crate-context-2.0.1 (c (n "context") (v "2.0.1") (d (list (d (n "cc") (r "~1") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "042i2ibims7yj5qv8wbl412p7gb81pb10s0zi75isr1wr10hk8in") (f (quote (("nightly"))))))

(define-public crate-context-2.1.0 (c (n "context") (v "2.1.0") (d (list (d (n "cc") (r "~1") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1cpc86cz0c6iam4w9c4hwqfc3bkdhh3bqd5jh9q23619c1rabhsl") (f (quote (("nightly"))))))

(define-public crate-context-3.0.0 (c (n "context") (v "3.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "cc") (r "~1") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "winnt" "memoryapi" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1s30bxk0im87qb9rpw0frxkndj3l3bfn1hqyqjvmmrznjyai9pqa") (f (quote (("nightly")))) (l "boost_context")))

