(define-module (crates-io co nt contract-analyze) #:use-module (crates-io))

(define-public crate-contract-analyze-4.0.0-rc (c (n "contract-analyze") (v "4.0.0-rc") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "contract-metadata") (r "^4.0.0-rc") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45.0") (f (quote ("sign_ext"))) (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 2)))) (h "0pyc68drv04iyxnbvzvjzwafadai5mpmbz8glmigib6apj5cr873")))

(define-public crate-contract-analyze-4.0.0-rc.1 (c (n "contract-analyze") (v "4.0.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "contract-metadata") (r "^4.0.0-rc.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 2)))) (h "11ljvax6sgbx0bd6qpzfhs7sfwlnb7vvx4a11zcr1c6mm2q9bvfh")))

(define-public crate-contract-analyze-4.0.0-rc.2 (c (n "contract-analyze") (v "4.0.0-rc.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "contract-metadata") (r "^4.0.0-rc.2") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 2)))) (h "0m35hjs3vzcm1f57cjk9i6sk707vc60xm3jv4i7sdbc76gwfp4v6")))

(define-public crate-contract-analyze-4.0.0-rc.3 (c (n "contract-analyze") (v "4.0.0-rc.3") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "contract-metadata") (r "^4.0.0-rc.3") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 2)))) (h "0wh2rpn9q6r25p29n8x2s607zjisw3m2ff92qyihlxg8yl6im9g1")))

(define-public crate-contract-analyze-4.0.0-rc.4 (c (n "contract-analyze") (v "4.0.0-rc.4") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "contract-metadata") (r "^4.0.0-rc.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 2)))) (h "0yanp9w78g3n62fha86ccfjw433wfvfn6lhyfni8myi2wkpkxwlg")))

(define-public crate-contract-analyze-4.0.0 (c (n "contract-analyze") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "contract-metadata") (r "^4.0.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 2)))) (h "1i653l8739m60z71iwfh0xygawbr4kx78fwby4l9w85x8h6ikq6v")))

(define-public crate-contract-analyze-4.0.1 (c (n "contract-analyze") (v "4.0.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "contract-metadata") (r "^4.0.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 2)))) (h "01x4nchvvh3vk2g1ynqln782kd7ylqn43yj4qqcc5rx7imr3args")))

(define-public crate-contract-analyze-4.0.2 (c (n "contract-analyze") (v "4.0.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "contract-metadata") (r "^4.0.2") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 2)))) (h "0gd18akxxkxna12l96m2g3v82ag212ylsmmsvg8jyfd5lc0diq4q")))

(define-public crate-contract-analyze-4.1.0 (c (n "contract-analyze") (v "4.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "contract-metadata") (r "^4.1.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 2)))) (h "1i2vhq3pryiylfxlmmajpi7rhcjxzfwr2jxmd3nv6f4n3jzlk32l")))

(define-public crate-contract-analyze-4.1.1 (c (n "contract-analyze") (v "4.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "contract-metadata") (r "^4.1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.45.0") (d #t) (k 0)) (d (n "wabt") (r "^0.10.0") (d #t) (k 2)))) (h "03pcm08gvh6svvqlq0i5297pgld70lmwpxbgn55wkvcdqf641zsf")))

