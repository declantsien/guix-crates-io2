(define-module (crates-io co nt contriview) #:use-module (crates-io))

(define-public crate-contriview-0.1.0 (c (n "contriview") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "scraper") (r "^0.9") (d #t) (k 0)))) (h "023i0r9zr3ddgz98x4lmha0xs01f6z0v82i5zmw2pj3hrkdrs21w")))

(define-public crate-contriview-0.2.0 (c (n "contriview") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "scraper") (r "^0.9") (d #t) (k 0)))) (h "0xmbyyr5h7chx60fg8h2qihz2rb2yrz7afykhq0d3x4141m3hs5m")))

(define-public crate-contriview-0.2.1 (c (n "contriview") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)))) (h "0m6326ri3jcp5h2fqwbxl4lqasa6bwj1lw80x4m7qyhdbccvnglb")))

