(define-module (crates-io co nt conthir-hir) #:use-module (crates-io))

(define-public crate-conthir-hir-0.0.1 (c (n "conthir-hir") (v "0.0.1") (d (list (d (n "built") (r "^0.7.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v8"))) (d #t) (k 0)))) (h "08cwrl6hwc8i4i1qq1c6ams9vkarkrbnknjpwrnrcbw9sg2gik76") (y #t) (r "1.76")))

