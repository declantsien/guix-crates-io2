(define-module (crates-io co nt context-mapper) #:use-module (crates-io))

(define-public crate-context-mapper-0.1.0 (c (n "context-mapper") (v "0.1.0") (d (list (d (n "context-mapper-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0mhlhaa61d3vwdyjkxhkck850s9r44n183n4h9gsp432w6px4qn2")))

(define-public crate-context-mapper-0.1.1 (c (n "context-mapper") (v "0.1.1") (d (list (d (n "context-mapper-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1yy1yrjrvlaa42xkqvm5896h1wrs31spyhb224xrh93ibd2vxfrp")))

