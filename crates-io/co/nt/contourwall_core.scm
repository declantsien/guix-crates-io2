(define-module (crates-io co nt contourwall_core) #:use-module (crates-io))

(define-public crate-contourwall_core-0.1.0 (c (n "contourwall_core") (v "0.1.0") (d (list (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "0czhcg39kpaiylb54agv8bcb206rqxnq0ifhzb908f6048p1gghi")))

(define-public crate-contourwall_core-0.2.0 (c (n "contourwall_core") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (d #t) (k 0)))) (h "03d21l0ran69qiav7zfsp28q4qp9flf9s352vx3szgw1q9k5db70") (r "1.70")))

