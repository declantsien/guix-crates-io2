(define-module (crates-io co nt content-blocker) #:use-module (crates-io))

(define-public crate-content-blocker-0.1.0 (c (n "content-blocker") (v "0.1.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)))) (h "1cqrvsh3fdvja7lml49f8pfsfksmhyzysczl52xj8cnp43w7nyaf")))

(define-public crate-content-blocker-0.2.0 (c (n "content-blocker") (v "0.2.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0c9q1hrvc3apna1k0z26z1xdaxvphq0swpgrqq4k85npvrngc549")))

(define-public crate-content-blocker-0.2.1 (c (n "content-blocker") (v "0.2.1") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1wdpjsdy7629m16hrrvmb8h6jbmy29zcyiqdzfxh6mahh75qc38v")))

(define-public crate-content-blocker-0.2.2 (c (n "content-blocker") (v "0.2.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1xpphyqk66nmkhfkpvabrgnf922ggmbswffjkpss3kp3phrxb4sq")))

(define-public crate-content-blocker-0.2.3 (c (n "content-blocker") (v "0.2.3") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0m61m6n9qbhakm63ivgz956c74yqwkfi2n2wq3g5p6i1dvqb5ps4")))

