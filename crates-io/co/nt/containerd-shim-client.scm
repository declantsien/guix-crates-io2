(define-module (crates-io co nt containerd-shim-client) #:use-module (crates-io))

(define-public crate-containerd-shim-client-0.1.0 (c (n "containerd-shim-client") (v "0.1.0") (d (list (d (n "protobuf") (r "^2.23.0") (d #t) (k 0)) (d (n "ttrpc") (r "^0.5.2") (d #t) (k 0)) (d (n "ttrpc-codegen") (r "^0.2") (d #t) (k 1)))) (h "0bpfb903vcab1w91wwrjgikwi3aq4974fc31fr3bdpxzmy6dpzf1") (y #t)))

(define-public crate-containerd-shim-client-0.1.1 (c (n "containerd-shim-client") (v "0.1.1") (d (list (d (n "protobuf") (r "^2.23.0") (d #t) (k 0)) (d (n "ttrpc") (r "^0.5.2") (d #t) (k 0)) (d (n "ttrpc-codegen") (r "^0.2") (d #t) (k 1)))) (h "0gc7vrh9xhn03jw01hvmbzb9z120sh6i8ncgcbn560cjzsk2n2gr") (f (quote (("generate_bindings")))) (y #t)))

(define-public crate-containerd-shim-client-0.1.2 (c (n "containerd-shim-client") (v "0.1.2") (d (list (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "protobuf") (r "^2.23.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.16") (f (quote ("stderr"))) (k 2)) (d (n "ttrpc") (r "^0.5.2") (d #t) (k 0)) (d (n "ttrpc-codegen") (r "^0.3.0") (o #t) (d #t) (k 1)))) (h "0rhb211yizlflc019wh4w0v8qjwllg1z16n44bkm49xhn8prj1s6") (f (quote (("generate_bindings" "ttrpc-codegen")))) (y #t)))

