(define-module (crates-io co nt containerof) #:use-module (crates-io))

(define-public crate-containerof-0.0.2 (c (n "containerof") (v "0.0.2") (h "0j4hc29z00ac541sh631m13834h3fqvb7p2l3mz3av2bqsckr0kl")))

(define-public crate-containerof-0.1.0 (c (n "containerof") (v "0.1.0") (h "0pqcq8407n8nai05q2mnh69qjpcy03dfywwzlxscr4xj2hd4119f")))

(define-public crate-containerof-0.2.0 (c (n "containerof") (v "0.2.0") (h "0h1rmxx2sbzqd2cmxgxqnfasn7lxnz0k6f8xr6jz474001nqji69")))

(define-public crate-containerof-0.2.1 (c (n "containerof") (v "0.2.1") (h "04764swx0pwdbsifvliyjx76xi2fsxd0ibmgcb53k8l5m8glc38w")))

(define-public crate-containerof-0.2.2 (c (n "containerof") (v "0.2.2") (h "1pqc4kgsmggriiv2kwsjp5j0lnw617ffsp2yrd20cn4y0xw7pdvd")))

(define-public crate-containerof-0.3.0 (c (n "containerof") (v "0.3.0") (d (list (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "140gp47d7ys0srfrrcrzhqb47vhh85xynv8lmhnj2ikr61j8gi5s")))

