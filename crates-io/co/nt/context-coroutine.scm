(define-module (crates-io co nt context-coroutine) #:use-module (crates-io))

(define-public crate-context-coroutine-2.1.0 (c (n "context-coroutine") (v "2.1.0") (d (list (d (n "context") (r "^2.1.0") (d #t) (k 0)))) (h "1wc9hz07hl60bm68rmj916s0arfayzlfsg8q1jqppvq04h52dvvb")))

(define-public crate-context-coroutine-3.0.0 (c (n "context-coroutine") (v "3.0.0") (d (list (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "libc-extra") (r "^0.3.2") (d #t) (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)))) (h "0k7jnxl8mqkl01z841dc305qn028n0d0shx4liv9ka1h5n9b1g7f")))

(define-public crate-context-coroutine-3.1.0 (c (n "context-coroutine") (v "3.1.0") (d (list (d (n "context-allocator") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "libc-extra") (r "^0.3.2") (d #t) (k 0)) (d (n "likely") (r "^0.1.0") (d #t) (k 0)))) (h "0xaa9v3yc8rs1344mlf925sfr5840s8x92m4rg3ss0cgff16zp32")))

