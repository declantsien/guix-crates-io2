(define-module (crates-io co nt content_distribution_network) #:use-module (crates-io))

(define-public crate-content_distribution_network-0.0.1 (c (n "content_distribution_network") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0gq4788acsr2xxhf6f75121fz78w0k73f94klm1ssafgjxzb0bkr")))

(define-public crate-content_distribution_network-0.0.2 (c (n "content_distribution_network") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)))) (h "14gyfccph19ic8zn5rxwm3kr7vvb6m9qv1wpf86l6jgqrcr9ljhg")))

