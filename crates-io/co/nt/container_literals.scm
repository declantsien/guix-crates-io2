(define-module (crates-io co nt container_literals) #:use-module (crates-io))

(define-public crate-container_literals-0.1.0 (c (n "container_literals") (v "0.1.0") (h "1qals11cia0c6w634mrqzh2nms9vk66gjzjr6iz7zxwsdl7hda4g")))

(define-public crate-container_literals-0.1.1 (c (n "container_literals") (v "0.1.1") (h "02zfx0cray6xvb75kdgyqsnsflqzas2wd3py4gslnmn76w6jzgfd")))

(define-public crate-container_literals-0.1.2 (c (n "container_literals") (v "0.1.2") (h "0r3cwm14sxgrkbdzhcjzydrv2zrn81l6qbx10zl9c13a4gif8zd6")))

