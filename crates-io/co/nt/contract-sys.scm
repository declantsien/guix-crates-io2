(define-module (crates-io co nt contract-sys) #:use-module (crates-io))

(define-public crate-contract-sys-0.1.0 (c (n "contract-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1vbgxbq544bqfnirz9faa96hl7j2641w1a2is49l5x2jkgim64nz") (l "libcontract")))

(define-public crate-contract-sys-0.1.1 (c (n "contract-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)))) (h "1v7by83d1nrm44dc886f06x135wk8pgmr7446ysn01i0f85lqkdy") (l "libcontract")))

