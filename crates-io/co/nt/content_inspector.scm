(define-module (crates-io co nt content_inspector) #:use-module (crates-io))

(define-public crate-content_inspector-0.2.0 (c (n "content_inspector") (v "0.2.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "16wbb70yfgj8gr8cbk8da0bb3v4h2rrbnn9bf9ckrybicdchsynp")))

(define-public crate-content_inspector-0.2.1 (c (n "content_inspector") (v "0.2.1") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "0qcq99wjsm5s2yr17z158qn9manjkfhw9hinmnwvj5j6zvdzc117")))

(define-public crate-content_inspector-0.2.2 (c (n "content_inspector") (v "0.2.2") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "0sdn84k3vc3pxawzvx3k5q9nhydcbz3ph3daig4272h3n1zf20nr")))

(define-public crate-content_inspector-0.2.3 (c (n "content_inspector") (v "0.2.3") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "1z7scd2rhwjnhbfmvxv9z047wcq0i0siqi67l53afn3wxlfrfnzi")))

(define-public crate-content_inspector-0.2.4 (c (n "content_inspector") (v "0.2.4") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "0f1gwv4axxw9wck4a4jxlkm7xjjakb3616isll2k0s4chmpadgdp")))

