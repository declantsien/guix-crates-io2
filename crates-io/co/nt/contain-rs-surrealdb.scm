(define-module (crates-io co nt contain-rs-surrealdb) #:use-module (crates-io))

(define-public crate-contain-rs-surrealdb-0.2.0-alpha.3 (c (n "contain-rs-surrealdb") (v "0.2.0-alpha.3") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "02sm1c0zm8vk6n7pmsqamqhm0ff61n0qdbyvqrfvccxbhm8zaqsq")))

(define-public crate-contain-rs-surrealdb-0.2.0-alpha.4 (c (n "contain-rs-surrealdb") (v "0.2.0-alpha.4") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "16jn1ycknm2vd1ly73amh6j9n5jpb0pkl31qbsxfn53ikcs1ja9g")))

(define-public crate-contain-rs-surrealdb-0.2.0-alpha.5 (c (n "contain-rs-surrealdb") (v "0.2.0-alpha.5") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1bdmckhmakz0v66fb43pa7yr9xs3h8jpy8yanmwr0wy8bw4qj2xx")))

(define-public crate-contain-rs-surrealdb-0.2.0-alpha.6 (c (n "contain-rs-surrealdb") (v "0.2.0-alpha.6") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0sbs71n7vdnczd1adbf7ph2gcdy1bg8rkjcd8c7vhd86wfc5hbza")))

(define-public crate-contain-rs-surrealdb-0.2.0-alpha.9 (c (n "contain-rs-surrealdb") (v "0.2.0-alpha.9") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1jya93fhkl9m8ifrljvy1hszlfdnrbx1bq15diy8c0m9fyrjz7k8")))

(define-public crate-contain-rs-surrealdb-0.2.0 (c (n "contain-rs-surrealdb") (v "0.2.0") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1pbi45wfa836d1p2y9by264r56wllj6n5vqrcbvmjwz50hvh5449")))

(define-public crate-contain-rs-surrealdb-0.2.1 (c (n "contain-rs-surrealdb") (v "0.2.1") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1mvhz3qpvxf1wfyd0aza4k84q3ikpb9s4nbd6aha5fd7xb9fmrha")))

(define-public crate-contain-rs-surrealdb-0.2.2 (c (n "contain-rs-surrealdb") (v "0.2.2") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1nskwkrr8l0qvk61r8p6iki6z2y4narchcc687laj9lxcka33w2i")))

(define-public crate-contain-rs-surrealdb-0.2.3 (c (n "contain-rs-surrealdb") (v "0.2.3") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "138iw2nsq1nsf3iv7lpwsx4b0p1dhvxw3mqyqd33smj3367060s1")))

