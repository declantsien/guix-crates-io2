(define-module (crates-io co nt containerd-shim-logging) #:use-module (crates-io))

(define-public crate-containerd-shim-logging-0.1.0 (c (n "containerd-shim-logging") (v "0.1.0") (d (list (d (n "systemd") (r "^0.9.0") (f (quote ("journal"))) (t "cfg(target_os = \"linux\")") (k 2)))) (h "0jy68ch6nbbp9n58spy6jdnwgvjq696h36r6hib6w8srp42npnbf")))

(define-public crate-containerd-shim-logging-0.1.1 (c (n "containerd-shim-logging") (v "0.1.1") (h "1rc14axfkrqwpq1anxh2zqcvmq5b3yy23z6xir8l7qdcaszcs6p4")))

