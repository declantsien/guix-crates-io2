(define-module (crates-io co nt contraband_codegen) #:use-module (crates-io))

(define-public crate-contraband_codegen-0.1.0 (c (n "contraband_codegen") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.19") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0ip2q8w4amkfac7r3pr2cy8l8mxzpg78gzp5zdzqz00km93fdcsr")))

(define-public crate-contraband_codegen-0.1.1 (c (n "contraband_codegen") (v "0.1.1") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.19") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0v0z4j04vbs1sglrkgnay2rrh1k1r47bg2203as9q42wannzscb6")))

(define-public crate-contraband_codegen-0.1.2 (c (n "contraband_codegen") (v "0.1.2") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.19") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0q0hwzb086p0bfnp8l96kv3y4d84yfgk7qixz5czsza8vnyxw8y3")))

