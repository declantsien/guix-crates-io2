(define-module (crates-io co nt container-run) #:use-module (crates-io))

(define-public crate-container-run-0.3.0 (c (n "container-run") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-log") (r "^1.3.2") (d #t) (k 0)))) (h "1qa0hhimsvrfij5plxd4z2mqkw0j04c9z5fhlp0b2dddmn0wvxqz")))

(define-public crate-container-run-0.3.1 (c (n "container-run") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-log") (r "^1.3.2") (d #t) (k 0)))) (h "1fjyckcnz8s0sgk7jwr8xww7n7dblbmzpmznfpk73f1jx8cmc4sf")))

(define-public crate-container-run-0.3.2 (c (n "container-run") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-log") (r "^1.3.2") (d #t) (k 0)))) (h "01wglwjrvdp5k94n92h4q6llhfassgxvr3rfi2sxk35ixz8cm4a6")))

(define-public crate-container-run-0.3.3 (c (n "container-run") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-log") (r "^1.3.2") (d #t) (k 0)))) (h "0dsgzvbdmjhsrpsyhqjk3fdzkl5dpkqxblp4dlwggpx7d6ys1p3n")))

(define-public crate-container-run-0.3.4 (c (n "container-run") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-log") (r "^1.3.2") (d #t) (k 0)))) (h "1hhihkhrpvn420mvvrsny6shhy1370f4g4n8p6vn04ifwl2h86ld")))

(define-public crate-container-run-0.3.5 (c (n "container-run") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-log") (r "^1.3.2") (d #t) (k 0)))) (h "00709f46pq435ra77zkjwa4rmm6kv2ng0dvrzi3lljmgv18wsil5")))

(define-public crate-container-run-0.4.0 (c (n "container-run") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-log") (r "^1.3.2") (d #t) (k 0)))) (h "1s8k54a0fi95gcq7lqwljvrf9c2f0qchb2zyr35i8q3vjxclqqc9")))

(define-public crate-container-run-0.4.1 (c (n "container-run") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-log") (r "^1.3.2") (d #t) (k 0)))) (h "17fa9ijy28sgbs8c5g6npzkx6liqcrz5fl47ndw9fm20lpr51m68")))

(define-public crate-container-run-0.5.0 (c (n "container-run") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple-log") (r "^1.3.2") (d #t) (k 0)))) (h "1hnc0cny66kklrbssjjss6w57s67v5xn0sds6k0h2b6bmcgvc3xr")))

(define-public crate-container-run-0.6.0 (c (n "container-run") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "simple-log") (r "^1.3.2") (d #t) (k 0)))) (h "0caxlxph9ysmf2p4p4qj0qnk0j1ds6b879jj33qp8dsra6lbysqq")))

