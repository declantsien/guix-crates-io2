(define-module (crates-io co nt contractdiff) #:use-module (crates-io))

(define-public crate-contractdiff-1.0.0 (c (n "contractdiff") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "web3") (r "^0.18.0") (d #t) (k 0)))) (h "0ixfyc1rhak7x05ys6ywlc7f97illb0abnfknj15l9priqimh4l8")))

