(define-module (crates-io co nt container-image-ref) #:use-module (crates-io))

(define-public crate-container-image-ref-0.1.0 (c (n "container-image-ref") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0sl90wkaskjm4dbsl65gzsbi3m456ls80y35hivgx5sc6g6ywzsn")))

