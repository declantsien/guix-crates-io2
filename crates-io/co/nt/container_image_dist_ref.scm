(define-module (crates-io co nt container_image_dist_ref) #:use-module (crates-io))

(define-public crate-container_image_dist_ref-0.1.0 (c (n "container_image_dist_ref") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1m18q0pbxdym6m18f7wysnlzikwnqcig14rrwv3766qz3cy9ixwl")))

(define-public crate-container_image_dist_ref-0.1.1 (c (n "container_image_dist_ref") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1wfsiczbawaphlibd9a6sw7b688r1ap1hpm690kl9llxgdnlc9p1")))

(define-public crate-container_image_dist_ref-0.1.2 (c (n "container_image_dist_ref") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "07a5saxf7zw3r7na29adzq4g3kmv2k10kz9rvzwlm4c0p7x3wqag")))

(define-public crate-container_image_dist_ref-0.1.3 (c (n "container_image_dist_ref") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "07kgpzxzy1lxgw7k572cllqw0cn8w5sg5ds2y2hkqb98zh5aa7zq")))

(define-public crate-container_image_dist_ref-0.1.4 (c (n "container_image_dist_ref") (v "0.1.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0ccikv8c32wrkyjy01nj6rpfc2ci0ab5m65iszjp952ma8idx1xx")))

(define-public crate-container_image_dist_ref-0.2.0 (c (n "container_image_dist_ref") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1vc375vqd58h1wiqhjb4xhvsyzsyj5nm3h54zv77bym92f4103aw")))

(define-public crate-container_image_dist_ref-0.3.0 (c (n "container_image_dist_ref") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1nz8kcmfimrc9a8zb70adlmpb01c7iw4gcc0nkjmx72qr9na7mil")))

