(define-module (crates-io co nt contained-actors) #:use-module (crates-io))

(define-public crate-contained-actors-0.1.3 (c (n "contained-actors") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "decanter") (r "^0.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "scsys") (r "^0.1.41") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "smart-default") (r "^0.6") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "152cgfs3695k80x0a6z2yflcw8cdlhf2cnai6mwiqy5h31yy5h7k")))

