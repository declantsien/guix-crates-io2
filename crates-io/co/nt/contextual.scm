(define-module (crates-io co nt contextual) #:use-module (crates-io))

(define-public crate-contextual-0.1.0 (c (n "contextual") (v "0.1.0") (h "1x1clcfj6jrg2idrhbg3y3wyk7v5f64kmmipwmvk6gjhs0vs2vxn")))

(define-public crate-contextual-0.1.1 (c (n "contextual") (v "0.1.1") (h "0aawb4fpyclnvhlxlnwa1gx9p9hqq1zpq9j56zjk9s9cfpisim8h")))

(define-public crate-contextual-0.1.2 (c (n "contextual") (v "0.1.2") (h "0rxykyfrccdmfdxsw3pwd05bb1a8f9mbmxnfqc8rm4gr2adzwaix")))

(define-public crate-contextual-0.1.3 (c (n "contextual") (v "0.1.3") (h "0l11h1ai9d7crz88vrjqx7w45q8rmm1486b018qafzz53dy58rri")))

(define-public crate-contextual-0.1.4 (c (n "contextual") (v "0.1.4") (h "1iyn6id1jlfydf625zfmj9bx9s737fh8350rkw686v4l9annv4af")))

(define-public crate-contextual-0.1.5 (c (n "contextual") (v "0.1.5") (h "08ja6wsqzj9jb1z38imgq158rxzsjaflv17yxz91cy1hppfmszj7")))

(define-public crate-contextual-0.1.6 (c (n "contextual") (v "0.1.6") (h "0sizy2zchi2h5vzasxaag59k46dvxjsh9gknx6i8b7ni4krp3jh5")))

