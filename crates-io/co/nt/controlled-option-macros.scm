(define-module (crates-io co nt controlled-option-macros) #:use-module (crates-io))

(define-public crate-controlled-option-macros-0.1.0 (c (n "controlled-option-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1az8vqhrhjsbfxlyfxpx707si0gj1zmxcz83xvflrj54pyj31iip")))

(define-public crate-controlled-option-macros-0.2.0 (c (n "controlled-option-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "016xa8929qc15xd9h8m67p0r4jpxrax42ql1rrw12444gjdnh7f9") (y #t)))

(define-public crate-controlled-option-macros-0.2.1 (c (n "controlled-option-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c9pmisjbm4abhd3z1lkb7qbkk0vh46qaraqzdjp7q5nh6i121ph")))

(define-public crate-controlled-option-macros-0.2.2 (c (n "controlled-option-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1yzvgi8hfxczwvd533rq2lp5rpdfnxcjbnihw4rn6ibcb8jj8l1h")))

