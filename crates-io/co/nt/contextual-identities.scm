(define-module (crates-io co nt contextual-identities) #:use-module (crates-io))

(define-public crate-contextual-identities-0.1.0 (c (n "contextual-identities") (v "0.1.0") (d (list (d (n "contextual-identities-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.67") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.17") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "173g6wxa3axla42364cw7yn6w84m6asn2kq6wg9hjyda40f3z5la") (y #t)))

