(define-module (crates-io co nt contrive) #:use-module (crates-io))

(define-public crate-contrive-0.1.0 (c (n "contrive") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "17rm4q92yvzdjw2qfz1j4js8qc1m056c9z1n83mz4l9zfrsp4fbb")))

(define-public crate-contrive-0.1.1 (c (n "contrive") (v "0.1.1") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0c98ws4dr8hivy53abpq6kf9yhw863svh6z1mncljm1xczvkdf42")))

