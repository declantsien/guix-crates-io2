(define-module (crates-io co nt contrast) #:use-module (crates-io))

(define-public crate-contrast-0.1.0 (c (n "contrast") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1c8fab7gq01b1yycybv3s9zd9dnmg8d3bapd48sw0nccba4fdqp9")))

