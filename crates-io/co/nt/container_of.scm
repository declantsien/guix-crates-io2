(define-module (crates-io co nt container_of) #:use-module (crates-io))

(define-public crate-container_of-0.1.0 (c (n "container_of") (v "0.1.0") (h "151w4kr4gi9yvpabmxkacs1qdd030xp9wl1gj161f1sgd5h986m3")))

(define-public crate-container_of-0.5.0 (c (n "container_of") (v "0.5.0") (d (list (d (n "memoffset") (r "^0.6.5") (d #t) (k 0)))) (h "15fivrkimyi29rvm387jqjb7sad6sd6c7ra9sad35krnz4169bn1") (y #t)))

(define-public crate-container_of-0.5.1 (c (n "container_of") (v "0.5.1") (d (list (d (n "memoffset") (r "^0.6") (d #t) (k 0)))) (h "0as7g6gspvdbp4vl1a1834pzh481x9jp4clfgyl6c7vnhvmvpxc9")))

