(define-module (crates-io co nt contain) #:use-module (crates-io))

(define-public crate-contain-0.1.0 (c (n "contain") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0ay2fhlb8paqqpvjcf3pgk79r3qbbqzn8lr7rpxr4baxvqdrfkvr") (y #t)))

(define-public crate-contain-0.2.0 (c (n "contain") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0wz2s858p12hnxp1prsp7i2a7qghvhaa26ps842whnkb3y0zpv3q") (y #t)))

(define-public crate-contain-0.2.1 (c (n "contain") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "17l3860csasj91b2hyad13rx9hff6j5qgg39pffjbrp1m89z5jja") (y #t)))

(define-public crate-contain-0.3.0 (c (n "contain") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "13n9fw6mj9wi4hl3wa3rbg4dv25agrnywan9y4f7z248hy8w78ik")))

(define-public crate-contain-0.3.1 (c (n "contain") (v "0.3.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "00wlzwaqgjp8xkxbb6dbqx0w53pg58zka3d98d1r9vw41980d3nx")))

(define-public crate-contain-0.4.0 (c (n "contain") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1w8a8b6l20x8npkb7lw2j3034k5qhvq50srz4hfqlqszgh1a7706")))

