(define-module (crates-io co nt contain-rs-nginx) #:use-module (crates-io))

(define-public crate-contain-rs-nginx-0.2.0-alpha.3 (c (n "contain-rs-nginx") (v "0.2.0-alpha.3") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "038i1c40wgxn41b4r042xw6xpfl152578n74rjzm596a9crdc52v")))

(define-public crate-contain-rs-nginx-0.2.0-alpha.4 (c (n "contain-rs-nginx") (v "0.2.0-alpha.4") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "159l6jyk8ym98qwip9xy8dr25arbcf9xvn9xrijd2r3m5i7096v1")))

(define-public crate-contain-rs-nginx-0.2.0-alpha.5 (c (n "contain-rs-nginx") (v "0.2.0-alpha.5") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "0n5728j6vyr7vkq2bg9vfvs4747ylyia0p4nhc790w142jkfwdg9")))

(define-public crate-contain-rs-nginx-0.2.0-alpha.6 (c (n "contain-rs-nginx") (v "0.2.0-alpha.6") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "1sadl7nzq9ki0wbb8y15d77r094xf0q5q67n6wcpkx9jvl38dwqq")))

(define-public crate-contain-rs-nginx-0.2.0-alpha.7 (c (n "contain-rs-nginx") (v "0.2.0-alpha.7") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "0m6x1s5s7sw0lwagkzz08yk0hbkdc421zlska50hdpr5gvvq0dk3")))

(define-public crate-contain-rs-nginx-0.2.0-alpha.8 (c (n "contain-rs-nginx") (v "0.2.0-alpha.8") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "0mcdvycs6rh13qm55zlkqy3n1f0l0x97vpvqx6hvpqr7yz1cqhjp")))

(define-public crate-contain-rs-nginx-0.2.0-alpha.9 (c (n "contain-rs-nginx") (v "0.2.0-alpha.9") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "1wab3mwp54hz3maqlw5bjmg748anz5cv7sndj8zab55hq0flk1r0")))

(define-public crate-contain-rs-nginx-0.2.0 (c (n "contain-rs-nginx") (v "0.2.0") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "09nid4vscc01q41986srsii9j3pj6ag8307n0xq7sqj1lpfp5ry3")))

(define-public crate-contain-rs-nginx-0.2.1 (c (n "contain-rs-nginx") (v "0.2.1") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "014sz2fian8nxcks6w6zlmghqwl0sgbqy6s40310a60yfqalv63r")))

(define-public crate-contain-rs-nginx-0.2.2 (c (n "contain-rs-nginx") (v "0.2.2") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "10bm8xhxw0fjp3ifvmarg7a7751lb49a20vqnfxf3njrjdghwz9m")))

(define-public crate-contain-rs-nginx-0.2.3 (c (n "contain-rs-nginx") (v "0.2.3") (d (list (d (n "contain-rs") (r "^0.2.0-alpha.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)))) (h "0rawrna13b1rqk1kv1hlzhk24xf3cd2yrxyxc31pm2zr7p8ac3x7")))

