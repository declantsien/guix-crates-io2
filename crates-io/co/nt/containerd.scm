(define-module (crates-io co nt containerd) #:use-module (crates-io))

(define-public crate-containerd-0.1.0 (c (n "containerd") (v "0.1.0") (d (list (d (n "prost") (r "~0.9") (d #t) (k 0)) (d (n "prost-types") (r "~0.9") (d #t) (k 0)) (d (n "tokio") (r "~1.14") (f (quote ("rt-multi-thread" "time" "fs" "macros" "net"))) (d #t) (k 0)) (d (n "tonic") (r "~0.6") (d #t) (k 0)) (d (n "tonic-build") (r "~0.6") (f (quote ("prost"))) (d #t) (k 1)) (d (n "tower") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "~2.3") (d #t) (k 1)))) (h "1vbf95qxdb73kr6ri58kaqqlh37faqgh81ldi9a1fck5j64lfjqp")))

