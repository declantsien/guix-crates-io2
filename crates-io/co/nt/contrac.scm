(define-module (crates-io co nt contrac) #:use-module (crates-io))

(define-public crate-contrac-0.5.0 (c (n "contrac") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "embed-resource") (r "^1.3") (d #t) (k 1)) (d (n "native-windows-derive") (r "^1.0.2") (d #t) (k 0)) (d (n "native-windows-gui") (r "^1.0.6") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)) (d (n "winping") (r "^0.10.1") (d #t) (k 0)))) (h "0xc89dy680hmilsy59x8b4ixxx9n4ch84wkbfaymg0kqjkl54gig")))

