(define-module (crates-io co nt continuation) #:use-module (crates-io))

(define-public crate-continuation-0.1.0 (c (n "continuation") (v "0.1.0") (h "1ip2lf5wgxvq0lximgi8cb45gqjm7kywjck3sqj8mlb01yj9plxq")))

(define-public crate-continuation-0.1.1 (c (n "continuation") (v "0.1.1") (h "1qh9y39g2dbxj9gmsyg1gsn42cdivrg0wkn226wdb20qci8574zh")))

