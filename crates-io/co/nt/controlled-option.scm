(define-module (crates-io co nt controlled-option) #:use-module (crates-io))

(define-public crate-controlled-option-0.1.0 (c (n "controlled-option") (v "0.1.0") (h "19gmvc06d6q47942fs2m53pfc7mrixzyrjcd6jhw5d3359xl409f")))

(define-public crate-controlled-option-0.2.0 (c (n "controlled-option") (v "0.2.0") (d (list (d (n "controlled-option-macros") (r "=0.1.0") (d #t) (k 0)))) (h "1hasdy7ws8jn68vsjp8nbnmanb2arw3ljv3cb45jm0cqwx5841b4")))

(define-public crate-controlled-option-0.3.0 (c (n "controlled-option") (v "0.3.0") (d (list (d (n "controlled-option-macros") (r "=0.2.1") (d #t) (k 0)))) (h "009jh27nmf8chi0h489bk1z1jli47ykf2vf0ysx0ilphy6bzvhv5")))

(define-public crate-controlled-option-0.4.0 (c (n "controlled-option") (v "0.4.0") (d (list (d (n "controlled-option-macros") (r "=0.2.1") (d #t) (k 0)))) (h "1pa753z9dksrdn56in4cdpragmcjvkyha88jyp5sbyf07sngxq6x")))

(define-public crate-controlled-option-0.4.1 (c (n "controlled-option") (v "0.4.1") (d (list (d (n "controlled-option-macros") (r "=0.2.2") (d #t) (k 0)))) (h "1b63p80rzx71f064dwg6gy9g71n3lcqazc5880gmf4aln9fwkawm")))

