(define-module (crates-io co nt contra) #:use-module (crates-io))

(define-public crate-contra-1.0.0 (c (n "contra") (v "1.0.0") (d (list (d (n "lib-contra") (r "^1.0") (d #t) (k 0)) (d (n "proc-contra") (r "^1.0") (d #t) (k 0)))) (h "1ik5xmicr6zhlbdlqlyvw6sd0p0d9df0n1hyv01s902g6dlllmax")))

(define-public crate-contra-1.0.1 (c (n "contra") (v "1.0.1") (d (list (d (n "lib-contra") (r "^1.0") (d #t) (k 0)) (d (n "proc-contra") (r "^1.0") (d #t) (k 0)))) (h "0jvr82wrhy4891y7j82a0k7wmws8mqkv9m2pzpgis3n08f2g00bb")))

(define-public crate-contra-1.0.2 (c (n "contra") (v "1.0.2") (d (list (d (n "lib-contra") (r "^1.0") (d #t) (k 0)) (d (n "proc-contra") (r "^1.0.1") (d #t) (k 0)))) (h "0841gq489vbiwnlihm5clls604jjnyjirhiin6rlykb75jwmm1jg")))

(define-public crate-contra-1.0.3 (c (n "contra") (v "1.0.3") (d (list (d (n "lib-contra") (r "^1.0") (d #t) (k 0)) (d (n "proc-contra") (r "^1.0.1") (d #t) (k 0)))) (h "1sk7769lwfxc3n3p50albwjr6vvykb065d6rl9hdfmmy1q1djfng")))

(define-public crate-contra-1.0.4 (c (n "contra") (v "1.0.4") (d (list (d (n "lib-contra") (r "^1.0") (d #t) (k 0)) (d (n "proc-contra") (r "^1.0.1") (d #t) (k 0)))) (h "1mckkzh3i7k73d01xl9jmyx4089rif9d8ax7jsinbnz1akfr7qf7")))

(define-public crate-contra-1.0.5 (c (n "contra") (v "1.0.5") (d (list (d (n "lib-contra") (r "^1.0") (d #t) (k 0)) (d (n "proc-contra") (r "^1.0.1") (d #t) (k 0)))) (h "0dv78k87mmpckg9ir66xnylqnp0xiy6hw2nbz73w9cj2bg71blbp")))

(define-public crate-contra-2.0.2 (c (n "contra") (v "2.0.2") (d (list (d (n "lib-contra") (r "^2.0.1") (d #t) (k 0)) (d (n "proc-contra") (r "^2.0.1") (d #t) (k 0)))) (h "0wiynqqd4zn1lp3ccn7iddjnkhqnfnabgp6zk9g8xq1246sz0wzq")))

(define-public crate-contra-2.0.5 (c (n "contra") (v "2.0.5") (d (list (d (n "lib-contra") (r "^2.0.3") (d #t) (k 0)) (d (n "proc-contra") (r "^2.0.3") (d #t) (k 0)))) (h "1hdv5yhaifnizny4pvqy6pc5fxmxpwgg5ny9mi5qgvz03iy5266x")))

(define-public crate-contra-2.0.8 (c (n "contra") (v "2.0.8") (d (list (d (n "lib-contra") (r "^2.0.8") (d #t) (k 0)) (d (n "proc-contra") (r "^2.0.8") (d #t) (k 0)))) (h "0h7gjsmcp8bdhabqxxxjv2smhp461sdwg00gcy7b7ypgx5f7kvvg")))

(define-public crate-contra-2.1.1 (c (n "contra") (v "2.1.1") (d (list (d (n "lib-contra") (r "^2.1.1") (d #t) (k 0)) (d (n "proc-contra") (r "^2.1.1") (d #t) (k 0)))) (h "0n52645801b3nxwl1zrr3bkm82b8afpbk9q9pyf4p7fn8m3xqi32")))

(define-public crate-contra-4.0.0 (c (n "contra") (v "4.0.0") (d (list (d (n "lib-contra") (r "^4.0.0") (d #t) (k 0)) (d (n "proc-contra") (r "^4.0.0") (d #t) (k 0)))) (h "0j7g9nbnvp73f3xcc680xm9j2hc3pgsyv3gbr7cd26wkbdabx089")))

(define-public crate-contra-4.2.0 (c (n "contra") (v "4.2.0") (d (list (d (n "lib-contra") (r "^4.0.0") (d #t) (k 0)) (d (n "proc-contra") (r "^4.0.0") (d #t) (k 0)))) (h "0h9v6p967z9fqy442s02pr9jyc818j804pngfcwqdl7ch6fqm56v")))

(define-public crate-contra-4.8.0 (c (n "contra") (v "4.8.0") (d (list (d (n "lib-contra") (r "=4.8.0") (d #t) (k 0)) (d (n "proc-contra") (r "=4.8.0") (d #t) (k 0)))) (h "0k6zq4zn7bsi0kdyk1rbqar84pjj5bxjjapx23xhi9s8igbgic5z")))

(define-public crate-contra-4.9.0 (c (n "contra") (v "4.9.0") (d (list (d (n "lib-contra") (r "=4.9.0") (d #t) (k 0)) (d (n "proc-contra") (r "=4.9.0") (d #t) (k 0)))) (h "14hys6pfnh5dbvjk3hkcm9ql11j2fik20byd5j1p4qrhqmv9zgxh")))

(define-public crate-contra-4.10.0 (c (n "contra") (v "4.10.0") (d (list (d (n "lib-contra") (r "=4.10.0") (d #t) (k 0)) (d (n "proc-contra") (r "=4.10.0") (d #t) (k 0)))) (h "1kqv0csjh3h1pjgbfwb5z4z6srk84mmx9amwqf9aqlxdn5fzk00x")))

(define-public crate-contra-4.11.0 (c (n "contra") (v "4.11.0") (d (list (d (n "lib-contra") (r "=4.11.0") (d #t) (k 0)) (d (n "proc-contra") (r "=4.11.0") (d #t) (k 0)))) (h "051imi2a3riw72r4anqc414a94liw8iamrymgqka40bdv734xdzk")))

(define-public crate-contra-4.12.0 (c (n "contra") (v "4.12.0") (d (list (d (n "lib-contra") (r "=4.12.0") (d #t) (k 0)) (d (n "proc-contra") (r "=4.12.0") (d #t) (k 0)))) (h "0z8rilkjylrbia3y9k7b9r981wrxbvqlpj3ypdkjh78i9gg22klj")))

(define-public crate-contra-4.13.0 (c (n "contra") (v "4.13.0") (d (list (d (n "lib-contra") (r "=4.13.0") (d #t) (k 0)) (d (n "proc-contra") (r "=4.13.0") (d #t) (k 0)))) (h "0vplvvlnkikdvr8jrkn5yj523n2ha6brcdq4zcxnjzrka22awlgd")))

(define-public crate-contra-4.13.1 (c (n "contra") (v "4.13.1") (d (list (d (n "lib-contra") (r "=4.13.1") (d #t) (k 0)) (d (n "proc-contra") (r "=4.13.1") (d #t) (k 0)))) (h "0d7k41z7ahbz5dnphcv2m5crx29zcnddjhw3kfd75x1q7jqqij69")))

(define-public crate-contra-4.13.2 (c (n "contra") (v "4.13.2") (d (list (d (n "lib-contra") (r "=4.13.2") (d #t) (k 0)) (d (n "proc-contra") (r "=4.13.2") (d #t) (k 0)))) (h "0c0r5v1r447qincfc89vrylh207xfb80n8ys7vz8g7xb6yzhi76q")))

(define-public crate-contra-4.13.3 (c (n "contra") (v "4.13.3") (d (list (d (n "lib-contra") (r "=4.13.3") (d #t) (k 0)) (d (n "proc-contra") (r "=4.13.3") (d #t) (k 0)))) (h "1aiwwsyv5qv0y05qlfs8k26nlg6lbqqvf4qrpgrn9jhv4092jba0")))

(define-public crate-contra-4.13.4 (c (n "contra") (v "4.13.4") (d (list (d (n "lib-contra") (r "=4.13.4") (d #t) (k 0)) (d (n "proc-contra") (r "=4.13.4") (d #t) (k 0)))) (h "1zcjjqbkm4m57fyjdr1yd168sla98di18zak59aqjkdaqyv1mk7z")))

(define-public crate-contra-4.13.5 (c (n "contra") (v "4.13.5") (d (list (d (n "lib-contra") (r "=4.13.5") (d #t) (k 0)) (d (n "proc-contra") (r "=4.13.5") (d #t) (k 0)))) (h "1kkhppkv6gv0ax2lbsdsq2da518sw0gkbddjsb4y3wn1603f3nm7")))

(define-public crate-contra-4.14.0 (c (n "contra") (v "4.14.0") (d (list (d (n "lib-contra") (r "=4.14.0") (d #t) (k 0)) (d (n "proc-contra") (r "=4.14.0") (d #t) (k 0)))) (h "0lgwf1ny1da6ln8zxfs7yirp1qwzbcmd526jal8f322bmnr0861a")))

(define-public crate-contra-4.15.0 (c (n "contra") (v "4.15.0") (d (list (d (n "lib-contra") (r "=4.15.0") (d #t) (k 0)) (d (n "proc-contra") (r "=4.15.0") (d #t) (k 0)))) (h "01wjn2q1q5dr97vlj7pz22lq3g6srlp3k2029i17vncxnispwg3i")))

(define-public crate-contra-5.0.0 (c (n "contra") (v "5.0.0") (d (list (d (n "lib-contra") (r "=5.0.0") (d #t) (k 0)) (d (n "proc-contra") (r "=5.0.0") (d #t) (k 0)))) (h "0x0pkp1a9aj165xjklkp6l0dghy1c1w16ysi2rjn24f5gylrjkn3")))

(define-public crate-contra-5.0.1 (c (n "contra") (v "5.0.1") (d (list (d (n "lib-contra") (r "=5.0.1") (d #t) (k 0)) (d (n "proc-contra") (r "=5.0.1") (d #t) (k 0)))) (h "11pgxsvgsh5gsynm6w2w7h2gfc0az6zsm0n7qj61n7c2prkvzqdm")))

(define-public crate-contra-5.0.2 (c (n "contra") (v "5.0.2") (d (list (d (n "lib-contra") (r "=5.0.2") (d #t) (k 0)) (d (n "proc-contra") (r "=5.0.2") (d #t) (k 0)))) (h "0qcp5sx18as4w3kbqvf7wxmwjvh8d2az85sg4nf9apvl15j9rr5b")))

