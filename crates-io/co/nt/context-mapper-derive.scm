(define-module (crates-io co nt context-mapper-derive) #:use-module (crates-io))

(define-public crate-context-mapper-derive-0.1.0 (c (n "context-mapper-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14fxjhgsy0db4cbv8mkz7091vrkn8ymsl1fnbnhhh199bsgsalyw")))

