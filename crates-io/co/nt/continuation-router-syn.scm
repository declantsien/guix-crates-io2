(define-module (crates-io co nt continuation-router-syn) #:use-module (crates-io))

(define-public crate-continuation-router-syn-1.0.0 (c (n "continuation-router-syn") (v "1.0.0") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v0lfbhikn4fr6dz57zafmzpd951m9w7lnwvf8mqqp106f7vdxfg")))

(define-public crate-continuation-router-syn-1.0.6 (c (n "continuation-router-syn") (v "1.0.6") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00aapa4yyl50qkpg9wrlzlw66l7vzxmymprw16653apzh1szqy1v")))

(define-public crate-continuation-router-syn-1.0.7 (c (n "continuation-router-syn") (v "1.0.7") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x59dxjp9fpim31hidhq05ibh205w7y391hr52z1q5rixnw6z2cw")))

(define-public crate-continuation-router-syn-1.0.9 (c (n "continuation-router-syn") (v "1.0.9") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11hi9ibqjpgc0kd37w6pdn2hwzllqcq2zxpzw773nb6ir8rjnq86")))

(define-public crate-continuation-router-syn-1.0.10 (c (n "continuation-router-syn") (v "1.0.10") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qhbdv7zl797kfqcvnqk0mvx2aiz13f5fl8bbl39piza32y9pkp7")))

(define-public crate-continuation-router-syn-1.0.11 (c (n "continuation-router-syn") (v "1.0.11") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w3b4p7skrlkcdslsmch79lbjqi1n1r4zfq8ia6ln16m7cnf3d1l")))

(define-public crate-continuation-router-syn-1.0.12 (c (n "continuation-router-syn") (v "1.0.12") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "147f01fk3hsrf9ar3a30614b02c3p624yblpl38cj3s2px0941kc")))

(define-public crate-continuation-router-syn-1.1.0 (c (n "continuation-router-syn") (v "1.1.0") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "082vmi9qbkhpg4dxns2gyc2g0ncllv80fm351xlcdyls36zsv368")))

(define-public crate-continuation-router-syn-1.1.1 (c (n "continuation-router-syn") (v "1.1.1") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rcpcch5qmn17js1dpjpsbr58pcp86zjlnpb2l1pnxpani1gyhjp")))

(define-public crate-continuation-router-syn-1.1.2 (c (n "continuation-router-syn") (v "1.1.2") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mcyylp6xh3h26c095qgd0gz3fb7h08wq5z3vsfdasiw8jq8s906")))

