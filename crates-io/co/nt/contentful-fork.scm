(define-module (crates-io co nt contentful-fork) #:use-module (crates-io))

(define-public crate-contentful-fork-0.0.1 (c (n "contentful-fork") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 2)))) (h "17gp5s3wx87h6f5rj48gvdyi5vga8cdazrm4jsmryv2k35y9nc7z")))

