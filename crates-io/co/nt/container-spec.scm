(define-module (crates-io co nt container-spec) #:use-module (crates-io))

(define-public crate-container-spec-1.0.1 (c (n "container-spec") (v "1.0.1") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d6byyxjb1p55fn3gkxqzhr9zhb888mg959z5vifwxqhb1hsxv8a")))

