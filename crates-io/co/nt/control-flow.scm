(define-module (crates-io co nt control-flow) #:use-module (crates-io))

(define-public crate-control-flow-0.1.0 (c (n "control-flow") (v "0.1.0") (h "1r1gh5gark0l9f0v0w0x7ykjvd5kvxvbb4pzavnix5ilj2878kwz")))

(define-public crate-control-flow-0.1.1 (c (n "control-flow") (v "0.1.1") (h "080ay1mspzm2hyjirx9yj1waiszb0rwnvpbmdcp0cpcx7i17x2y8")))

