(define-module (crates-io co nt context_bind) #:use-module (crates-io))

(define-public crate-context_bind-0.0.1 (c (n "context_bind") (v "0.0.1") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)))) (h "01cbi5pl4ramrpswg0jnp61bsk3h75pv91whin3kx074cy9f0v4r")))

(define-public crate-context_bind-0.0.2 (c (n "context_bind") (v "0.0.2") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)))) (h "1qc1ma1a2chk0yw8108vmy3kfm6yrjhkcwxbdb2ns3zizp5i1s8l")))

