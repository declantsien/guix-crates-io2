(define-module (crates-io co nt content) #:use-module (crates-io))

(define-public crate-content-0.1.0 (c (n "content") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 2)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1mqbj1apjpfr0385h9l5vzxr2wdrv6vx42wg69rhxcrxqz4vfsb1")))

(define-public crate-content-0.2.0 (c (n "content") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 2)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "06qn571mlffp4956bz1mgfbmpislx612nnbmdnvjyini4skdqfnw")))

(define-public crate-content-0.3.0 (c (n "content") (v "0.3.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 2)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1hgv3jljph640kx0n9axihfzvjrpr8wl9zqm5zh9aca318gsb6cv")))

(define-public crate-content-0.6.0 (c (n "content") (v "0.6.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1881xp70mrrgdm7gvsd40r0fbmdla731fz6c7jqy7bbwbms867pq")))

