(define-module (crates-io co nt controlmap-parser) #:use-module (crates-io))

(define-public crate-controlmap-parser-0.1.0 (c (n "controlmap-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0dnymdlwb0q7adkv0hjbgzsrvz6c98n2yyiaqnqwk7n3654gg0r3")))

(define-public crate-controlmap-parser-0.2.0 (c (n "controlmap-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0fyw6bww5n1ak8bi019wjacc9yypya2ni6ckaflw9izspj4jkb6z")))

(define-public crate-controlmap-parser-0.3.0 (c (n "controlmap-parser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-appender") (r "^0.2.3") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "0a237r0lw2rihb18hj2kqwvin5cgppgx2r9gz4bq9yh58qjh3374") (f (quote (("serde"))))))

