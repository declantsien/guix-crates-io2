(define-module (crates-io co nt container-pid) #:use-module (crates-io))

(define-public crate-container-pid-0.1.0 (c (n "container-pid") (v "0.1.0") (d (list (d (n "libc") (r "~0") (d #t) (k 0)) (d (n "simple-error") (r "~0") (d #t) (k 0)))) (h "0mqchmx7mfh79hpcqskm4cj8jhizs3x3m8qbw46mzl6n2l47nclw")))

(define-public crate-container-pid-0.2.0 (c (n "container-pid") (v "0.2.0") (d (list (d (n "libc") (r "~0") (d #t) (k 0)) (d (n "simple-error") (r "~0") (d #t) (k 0)))) (h "0xjfxq5yn46z06s1m2bgkfskq45dvm7hqw4zih37l8z80g6dmlb8")))

