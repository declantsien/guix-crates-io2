(define-module (crates-io co nt contructor-derive) #:use-module (crates-io))

(define-public crate-contructor-derive-0.1.0 (c (n "contructor-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06ya6f4v0frrgbh8203nm53kxk8msrlkmpzbw8qjd66ilvciyb4g")))

(define-public crate-contructor-derive-0.1.1 (c (n "contructor-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lryqzly0n9sj0jhpifghdcclyx4d70ds622bak2x8i8c71zf1x6")))

