(define-module (crates-io co nt controlgroup) #:use-module (crates-io))

(define-public crate-controlgroup-0.2.0 (c (n "controlgroup") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.10.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "0d3sikplxw7jrrs2kglil2dm03yiyylbc3003ay1hnx5yf5x4gqx")))

(define-public crate-controlgroup-0.2.1 (c (n "controlgroup") (v "0.2.1") (d (list (d (n "num_cpus") (r "^1.10.1") (d #t) (k 2)))) (h "000qjl31g1wzhj2yj324nzqfba33gvrr13k9qm9z0k090gk5f3r7")))

(define-public crate-controlgroup-0.3.0 (c (n "controlgroup") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1.11.1") (d #t) (k 2)))) (h "0pq599l9zlgm7rl2jl6v8hfbla5ybx1qbj0lfmswclvmcbzv4jgr")))

