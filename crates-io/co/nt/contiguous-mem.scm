(define-module (crates-io co nt contiguous-mem) #:use-module (crates-io))

(define-public crate-contiguous-mem-0.1.0 (c (n "contiguous-mem") (v "0.1.0") (d (list (d (n "portable-atomic") (r "^1") (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0d243r3xnpj992mkp0if2kx811mfrp97zdxgrn1x9kqibw20i6lw") (f (quote (("std" "portable-atomic/std") ("default" "std") ("debug")))) (s 2) (e (quote (("no_std" "dep:spin"))))))

(define-public crate-contiguous-mem-0.1.1 (c (n "contiguous-mem") (v "0.1.1") (d (list (d (n "portable-atomic") (r "^1") (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1ixqy17b9aanzgmhvnfh9mfjd0xamr69dqa0lqac9zkjhf8lzly0") (f (quote (("std" "portable-atomic/std") ("ptr_metadata") ("default" "std" "ptr_metadata") ("debug")))) (y #t) (s 2) (e (quote (("no_std" "dep:spin"))))))

(define-public crate-contiguous-mem-0.1.2 (c (n "contiguous-mem") (v "0.1.2") (d (list (d (n "portable-atomic") (r "^1") (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "09c640nl4b5wzldgg6mk6cm7ij37kazh6d3bi3rzkpb1bzdqg2ix") (f (quote (("std" "portable-atomic/std") ("ptr_metadata") ("default" "std") ("debug")))) (s 2) (e (quote (("no_std" "dep:spin"))))))

(define-public crate-contiguous-mem-0.2.0 (c (n "contiguous-mem") (v "0.2.0") (d (list (d (n "portable-atomic") (r "^1") (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0454rblki7kcs3ms1d9r5cfnhsdbgj02p2x91jaflfgi1qb6szlg") (f (quote (("std" "portable-atomic/std") ("ptr_metadata") ("leak_data") ("error_in_core") ("default" "std" "leak_data") ("debug")))) (s 2) (e (quote (("no_std" "dep:spin"))))))

(define-public crate-contiguous-mem-0.3.0 (c (n "contiguous-mem") (v "0.3.0") (d (list (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "09laik4vczpn521zpkgil337ps0gsirrbszn4kl88awd896cnsgw") (f (quote (("std") ("ptr_metadata") ("leak_data") ("error_in_core") ("default" "std" "leak_data") ("debug")))) (s 2) (e (quote (("no_std" "dep:portable-atomic" "dep:spin"))))))

(define-public crate-contiguous-mem-0.3.1 (c (n "contiguous-mem") (v "0.3.1") (d (list (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1n8p4grjpz65hb9rq5apsv9gnr2h4c13q03rd99vvfk9l8b3q7zl") (f (quote (("std") ("ptr_metadata") ("leak_data") ("error_in_core") ("default" "std" "leak_data") ("debug")))) (s 2) (e (quote (("no_std" "dep:portable-atomic" "dep:spin"))))))

(define-public crate-contiguous-mem-0.4.0 (c (n "contiguous-mem") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 2)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "11686vms3mm8044sbvqnc2a9kjnn2270xpwyrnng0syw5ngn5n3w") (f (quote (("ptr_metadata") ("leak_data") ("error_in_core") ("default" "leak_data") ("debug")))) (s 2) (e (quote (("no_std" "dep:portable-atomic" "dep:spin"))))))

(define-public crate-contiguous-mem-0.4.1 (c (n "contiguous-mem") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 2)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "044g00ar5p2pms5h58fp1dci0ywpc1dp5q855gfsaqnplkhrxhhc") (f (quote (("ptr_metadata") ("leak_data") ("error_in_core") ("default" "leak_data") ("debug")))) (s 2) (e (quote (("no_std" "dep:portable-atomic" "dep:spin"))))))

(define-public crate-contiguous-mem-0.4.2 (c (n "contiguous-mem") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 2)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1rc9g2gq848f3azmmimm2pprh0166c5bk0l2czzdz777cpr2y4rd") (f (quote (("ptr_metadata") ("error_in_core") ("default") ("debug")))) (s 2) (e (quote (("no_std" "dep:portable-atomic" "dep:spin"))))))

