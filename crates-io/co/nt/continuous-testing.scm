(define-module (crates-io co nt continuous-testing) #:use-module (crates-io))

(define-public crate-continuous-testing-0.0.1 (c (n "continuous-testing") (v "0.0.1") (h "1bn9315cqqbmqn8x40q701kwxhm6qjjm5fz7504s3x836wa9zd36")))

(define-public crate-continuous-testing-0.0.2 (c (n "continuous-testing") (v "0.0.2") (h "1yjl004zhmxjmd9qjq6rz95928194vxp2dlyw17hi5fdj5c63zm9")))

(define-public crate-continuous-testing-0.0.3 (c (n "continuous-testing") (v "0.0.3") (h "12dqvhvy0nrp8cy5v2hjx9jb234vx98f8h6rjal7r3f44h271648")))

(define-public crate-continuous-testing-0.0.4 (c (n "continuous-testing") (v "0.0.4") (h "14bg75pb56yni9ldnyqxgb6jlc3ybh7mda0zxdkwd3pazdk136xg")))

(define-public crate-continuous-testing-0.1.0 (c (n "continuous-testing") (v "0.1.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "076s8pa1wlax5yl33ha74v0phy9fzavaj08vncyk98wahbk1mkxn")))

(define-public crate-continuous-testing-0.1.1 (c (n "continuous-testing") (v "0.1.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0xw6x51i3z046x1la2dmg5d1xs0rxva7f76pf4194nsjkg5cdbry")))

(define-public crate-continuous-testing-0.1.2 (c (n "continuous-testing") (v "0.1.2") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "1zrqpjsr15wf1jvyn245rx524axa9xqikhf3kynrz727jlm71d4c")))

(define-public crate-continuous-testing-0.1.3 (c (n "continuous-testing") (v "0.1.3") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0jys65kn4gkhxvf3lv2ac8z50sha41d1qsnmba0dsz0sqk3z2ajj")))

(define-public crate-continuous-testing-0.1.4 (c (n "continuous-testing") (v "0.1.4") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)))) (h "0sd80a6dr4a6fs2azra6yxw920392vwv84glbd5cs3g297sf5v7b")))

(define-public crate-continuous-testing-0.2.0 (c (n "continuous-testing") (v "0.2.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "eywa") (r "^0.2.0") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)))) (h "16y7w462jvq1nc00gnk9pphhxwlams08cyyldhw87b7krd3361kc")))

(define-public crate-continuous-testing-0.2.1 (c (n "continuous-testing") (v "0.2.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "eywa") (r "^0.2.0") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)))) (h "1xv00hv6j2w3hchj8jdk7q7v79681b0bhbvbx50vjhkfjl2jycvx")))

(define-public crate-continuous-testing-0.2.2 (c (n "continuous-testing") (v "0.2.2") (d (list (d (n "eywa") (r "^0.2.0") (d #t) (k 0)))) (h "0iwvcpgvl506jz13i0372vj0rwm815cf69kdqf3z8m8l4nigpgrd")))

(define-public crate-continuous-testing-0.2.4 (c (n "continuous-testing") (v "0.2.4") (d (list (d (n "eywa") (r "^0.2.0") (d #t) (k 0)))) (h "00lv9miqk476xyqdzb6c2hwbbbwkwjkb9wj9p60mf97nla2kvlww")))

(define-public crate-continuous-testing-0.3.0 (c (n "continuous-testing") (v "0.3.0") (d (list (d (n "eywa") (r "^0.2.0") (d #t) (k 0)))) (h "12slsqwb1fi49dagyykqgyv2q31bgniww04y4l2qh2qkrcv2zdr6")))

(define-public crate-continuous-testing-0.3.1 (c (n "continuous-testing") (v "0.3.1") (d (list (d (n "eywa") (r "^0.2.0") (d #t) (k 0)))) (h "0rv5bxqdplhf45sqgnz7ns90lvsgvcapzg1nr3jz2y3dv8yi8dqa")))

(define-public crate-continuous-testing-0.3.3 (c (n "continuous-testing") (v "0.3.3") (d (list (d (n "eywa") (r "^0.2.0") (d #t) (k 0)))) (h "0havk68r95lnba46d4acixka4vykyj7x8zpwp1qa24dxr68j3kxv")))

(define-public crate-continuous-testing-0.3.4 (c (n "continuous-testing") (v "0.3.4") (d (list (d (n "eywa") (r "^0.2.0") (d #t) (k 0)))) (h "1n6qjkjrsjfkl8py2m52azihgmzqr7nwjg1mxsw1xh1dh5n9ivpk")))

(define-public crate-continuous-testing-0.3.5 (c (n "continuous-testing") (v "0.3.5") (d (list (d (n "eywa") (r "^0.2.0") (d #t) (k 0)))) (h "0b21zzf2kssfl7mhhdrdnm9nbm7zdmi310andc1r0qkvcb1bn3w1")))

(define-public crate-continuous-testing-0.3.6 (c (n "continuous-testing") (v "0.3.6") (d (list (d (n "eywa") (r "^0.2.0") (d #t) (k 0)))) (h "1w46r627ny2s383izpsl6d79yp49h0wsaqa4wssz0sbssx0hmdzs")))

(define-public crate-continuous-testing-0.3.8 (c (n "continuous-testing") (v "0.3.8") (d (list (d (n "eywa") (r "^0.2.0") (d #t) (k 0)))) (h "047mj3fbd5z05z4nb0y9dw21dvj798yfpc1x7dan2m2nprsmmcwa")))

(define-public crate-continuous-testing-0.4.0 (c (n "continuous-testing") (v "0.4.0") (h "0abwgpg65l642l5d5iwana4ymk9lziyrcrvdi52m29vk45fz1cbm")))

(define-public crate-continuous-testing-0.4.1 (c (n "continuous-testing") (v "0.4.1") (h "1fv7apqj3i6a45sqpss8y159ycal28zg1fgf0v46rbcdkfqc689r")))

(define-public crate-continuous-testing-1.0.0 (c (n "continuous-testing") (v "1.0.0") (h "0226rk521917nj08g3hfqdj0l9h4fzs4k0vj71qfrfhznlc51jxz")))

(define-public crate-continuous-testing-1.0.1 (c (n "continuous-testing") (v "1.0.1") (h "1svp2j79wbkldrviwj2rnrsx64d474b34r5da6vl6b4jg39ip7s6")))

(define-public crate-continuous-testing-1.1.0 (c (n "continuous-testing") (v "1.1.0") (h "0zb3m3kgj0f5ynmwm7ms668sidsbw9i2z46ig76i87ywjan9lncy")))

(define-public crate-continuous-testing-1.1.1 (c (n "continuous-testing") (v "1.1.1") (h "143z18sf22hw7cbv6p9qd8jjslvflw3gg4732i2s014cr3cgwqif")))

(define-public crate-continuous-testing-1.1.2 (c (n "continuous-testing") (v "1.1.2") (h "1ydzigi0l615vq3ha0kr79cpfqq892x15imbf6b3558rmg1js9lk")))

(define-public crate-continuous-testing-1.1.3 (c (n "continuous-testing") (v "1.1.3") (h "0raz5bmpzj2161800k84xlhxii9s9r833lyw1c1gn6anyv4ba68h")))

(define-public crate-continuous-testing-2.0.0 (c (n "continuous-testing") (v "2.0.0") (d (list (d (n "marked-yaml") (r "^0.2.2") (d #t) (k 0)) (d (n "notifme") (r "^0.0.2") (d #t) (k 0)))) (h "0dfl6n2pk6g4fw946dv25v390vv2g36qxsngcbzhk3f4f5szaifm")))

(define-public crate-continuous-testing-2.0.1 (c (n "continuous-testing") (v "2.0.1") (d (list (d (n "marked-yaml") (r "^0.2.2") (d #t) (k 0)) (d (n "notifme") (r "^0.0.2") (d #t) (k 0)))) (h "193qy02rvwzikaxa13amjwhp8m180fn9398i6xx1sbvc7gw8mdar")))

