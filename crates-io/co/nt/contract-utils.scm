(define-module (crates-io co nt contract-utils) #:use-module (crates-io))

(define-public crate-contract-utils-0.1.0 (c (n "contract-utils") (v "0.1.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "1srsgx548rl4l0dyvj2vmvyzxmiickwj95h6h6zdnk9npxgcsg6z")))

