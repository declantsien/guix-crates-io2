(define-module (crates-io co ok cookiecutter) #:use-module (crates-io))

(define-public crate-cookiecutter-0.1.0 (c (n "cookiecutter") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sqlite") (r "^0.26.0") (d #t) (k 0)))) (h "1jh25r8x65iah2p1zgrhzsiwxvxhb4jrwyvwac546hmw9g6ydfpz")))

