(define-module (crates-io co ok cookies-rs) #:use-module (crates-io))

(define-public crate-cookies-rs-0.1.0 (c (n "cookies-rs") (v "0.1.0") (d (list (d (n "cookie") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "0fzqanynj8g0g9587m94y4h50psshcgb1dpr37bbcsrabi928jy9")))

(define-public crate-cookies-rs-0.1.1 (c (n "cookies-rs") (v "0.1.1") (d (list (d (n "cookie") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "0jzbglx00x8yhqz43hw178q3yv22yykbb4yjifalra0jqs6ggx9l")))

