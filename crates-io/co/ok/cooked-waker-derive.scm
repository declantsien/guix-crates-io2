(define-module (crates-io co ok cooked-waker-derive) #:use-module (crates-io))

(define-public crate-cooked-waker-derive-1.0.0 (c (n "cooked-waker-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "1lvcw7k5khwq9wcsfmbb2yqfc8wz44z4la2mrawkzawn3n0iv39a")))

(define-public crate-cooked-waker-derive-1.0.1 (c (n "cooked-waker-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "0x4xpqh7xnrba69asrfif3p8nnfyghlp3k98fgm57zx80hmx1pkl")))

