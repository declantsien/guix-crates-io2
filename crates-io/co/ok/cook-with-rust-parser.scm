(define-module (crates-io co ok cook-with-rust-parser) #:use-module (crates-io))

(define-public crate-cook-with-rust-parser-0.0.3 (c (n "cook-with-rust-parser") (v "0.0.3") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1g4fr7ygvrh7x26hl1c8y1d4sdfzkfg0hvzy9mgwfb9hd1v126a5")))

(define-public crate-cook-with-rust-parser-0.0.4 (c (n "cook-with-rust-parser") (v "0.0.4") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0ixy7257dprpc8jlrkv71dnpm0pa0kky5189q0mfvdwj167mnyfn")))

(define-public crate-cook-with-rust-parser-0.0.5 (c (n "cook-with-rust-parser") (v "0.0.5") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1xqglfnd348qx2xfxlrlvnwq64z94liym7dgc6i167rzh0piynkq") (f (quote (("wasm" "uuid/wasm-bindgen"))))))

