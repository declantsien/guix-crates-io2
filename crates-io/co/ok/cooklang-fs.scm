(define-module (crates-io co ok cooklang-fs) #:use-module (crates-io))

(define-public crate-cooklang-fs-0.1.0 (c (n "cooklang-fs") (v "0.1.0") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0dvwahimq58r6qln3rlbb1x3456wlq5kjgkxf3j1ajhla6rgyy65")))

(define-public crate-cooklang-fs-0.1.1 (c (n "cooklang-fs") (v "0.1.1") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1l9nn3sflqiqkm65ndgcq22d4pmlzqs4gay43ikqxk2ccdg3r43s")))

(define-public crate-cooklang-fs-0.2.0 (c (n "cooklang-fs") (v "0.2.0") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "085ka6bn4lrrlawmspc0kxclbr8ymgbggvz1qv0snfhmmfpc03bk")))

(define-public crate-cooklang-fs-0.2.1 (c (n "cooklang-fs") (v "0.2.1") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0s0h85wharrqv3pna0yjchy30yzh65jj84vw66yf0y6s7wj51f8y")))

(define-public crate-cooklang-fs-0.3.0 (c (n "cooklang-fs") (v "0.3.0") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0c5mgayp1fwzx8p28vxc35d62i2541pmk9iq4fm7s05qfa5dflxw")))

(define-public crate-cooklang-fs-0.3.1 (c (n "cooklang-fs") (v "0.3.1") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "03an60wsnrg618d77fawva8ah0g4rfqw3adml4v1m3gf4shm6saw")))

(define-public crate-cooklang-fs-0.5.0 (c (n "cooklang-fs") (v "0.5.0") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "18gqmfsdq15wwx8wnx4bxgwhask6jnx8f2pgq2kqzsvsk4lkpc2z")))

(define-public crate-cooklang-fs-0.7.0 (c (n "cooklang-fs") (v "0.7.0") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "03mj3jv9lm20d3nng1ghd2s8bcgiap448345nhqqwl3v7j42k9rb")))

(define-public crate-cooklang-fs-0.8.0 (c (n "cooklang-fs") (v "0.8.0") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0yh24dgz815vpmsrm3abz6g0w9334kq8wwsp0p1jb1ib8y7jjm9h")))

(define-public crate-cooklang-fs-0.9.0 (c (n "cooklang-fs") (v "0.9.0") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1flc27ki437rq11qmal53mfnh39qig798a3fp2v5lvw5d6dmdamp")))

(define-public crate-cooklang-fs-0.12.0 (c (n "cooklang-fs") (v "0.12.0") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0cgakxq425z2lqhw0v225pixmg11qb449nlv8sa9grmnirnk23y3")))

(define-public crate-cooklang-fs-0.13.0 (c (n "cooklang-fs") (v "0.13.0") (d (list (d (n "camino") (r "^1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "cooklang") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "123i1mvbl1p1w25wh65vvz8npschcswjm95whagaigzbi5krh7sh")))

