(define-module (crates-io co ok cook-with-rust) #:use-module (crates-io))

(define-public crate-cook-with-rust-0.0.1 (c (n "cook-with-rust") (v "0.0.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1m9aij4wdbzlf1b4qy00pxszdagki2d783ipq5zxav4x6mis9gq1")))

(define-public crate-cook-with-rust-0.0.2 (c (n "cook-with-rust") (v "0.0.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0d1cg7xw9sw98qi2nsxrhzpchnln16xwifimd5mgl82irflh73i3")))

