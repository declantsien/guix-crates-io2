(define-module (crates-io co ok cookbook_data_parser) #:use-module (crates-io))

(define-public crate-cookbook_data_parser-0.1.0 (c (n "cookbook_data_parser") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "1fqlahbv7a76lgk6vnwkk28wax900lnj13b1by1iby8ha7fj1rnm")))

