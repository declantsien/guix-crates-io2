(define-module (crates-io co ok cookbook_src_generator) #:use-module (crates-io))

(define-public crate-cookbook_src_generator-0.1.0 (c (n "cookbook_src_generator") (v "0.1.0") (d (list (d (n "cookbook_data_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "0yyjdi7yd0ppfv0dim3k0maj0zzkbirf91z8v7sx9wxrg5g2x4xa")))

