(define-module (crates-io co ok cooks) #:use-module (crates-io))

(define-public crate-cooks-0.0.1 (c (n "cooks") (v "0.0.1") (h "1lchcrjp50x2fpxkfj7vcvgifix0slyrgsz562rdf09ds8q7b4pi") (y #t)))

(define-public crate-cooks-0.0.2 (c (n "cooks") (v "0.0.2") (h "19gp587wqi2sb0y9s5fbi4v0skgybbdivnwnbi35d7knrmr8y97h") (y #t)))

(define-public crate-cooks-0.0.3 (c (n "cooks") (v "0.0.3") (h "036l655qjfc9pykjc1gqpy0g05svl16nb5j8xfy9f7ik4njfrrjc") (y #t)))

