(define-module (crates-io co ok cookie-factory) #:use-module (crates-io))

(define-public crate-cookie-factory-0.1.0 (c (n "cookie-factory") (v "0.1.0") (h "026ffbv8pjxafx6a2sf3zcgmrddi9r3g55bbav5vx6mqhpn7y2lh")))

(define-public crate-cookie-factory-0.2.0 (c (n "cookie-factory") (v "0.2.0") (h "0fk2qq3byj76nldwk2b7728p5lb5qxnp8acpc1wdyqgqjg9bpp7b")))

(define-public crate-cookie-factory-0.2.1 (c (n "cookie-factory") (v "0.2.1") (h "06m1pq53bh74aw7c9jzajydid3jmv5l9p83yp07hnccb6l5rlapl")))

(define-public crate-cookie-factory-0.2.2 (c (n "cookie-factory") (v "0.2.2") (h "07izhcrbqkiksx1c6a32dynrv2siwbys0cqjqkgrprl7716m0jrz")))

(define-public crate-cookie-factory-0.2.3 (c (n "cookie-factory") (v "0.2.3") (h "1az64ig1x37bzd4xc1hyxf8wrz4rc4q8zy9w5iwcz1kca06zhyci")))

(define-public crate-cookie-factory-0.2.4 (c (n "cookie-factory") (v "0.2.4") (h "01qjg6dz24ybm82xfsj0p8lzj9xyhxndfgasj5jaricw17w7k94q")))

(define-public crate-cookie-factory-0.3.0-alpha1 (c (n "cookie-factory") (v "0.3.0-alpha1") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "0ln7ycv5kqgpls9amiqqwvw37rcc51rfh8dqw1ds2gl9s0jplhgn")))

(define-public crate-cookie-factory-0.3.0-alpha2 (c (n "cookie-factory") (v "0.3.0-alpha2") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1ppqlw4g19bw6zr8p8r36jcg3iwjk3bwdc0g01jccnb8f2bgk7h8")))

(define-public crate-cookie-factory-0.3.0-alpha3 (c (n "cookie-factory") (v "0.3.0-alpha3") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1cdcnfvgwwar1r0mqzg0lh8gqn4c26qgicyhcwnfg9g0wr4qf2cs")))

(define-public crate-cookie-factory-0.3.0-beta1 (c (n "cookie-factory") (v "0.3.0-beta1") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1gqc4p7dg0xv2j9v8azswg8cpab78cwbr7gin3bz9y6x9izsfvdc")))

(define-public crate-cookie-factory-0.3.0-beta2 (c (n "cookie-factory") (v "0.3.0-beta2") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1mfxiawbl0b56rxmck6r13mzzmx15dqpkd0r5paxah56zq1hb8xi")))

(define-public crate-cookie-factory-0.3.0-beta3 (c (n "cookie-factory") (v "0.3.0-beta3") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1bmc8mvcvq2f61ca10jf0dqjhh0iryznskz6rgj3w7fdivrzkg3m") (f (quote (("std") ("default" "std"))))))

(define-public crate-cookie-factory-0.3.0-beta4 (c (n "cookie-factory") (v "0.3.0-beta4") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "02bf5njgfdfrmqd89lpqbhkbn9qwgdqdjlnbpmxa8swf752vmxp8") (f (quote (("std") ("default" "std"))))))

(define-public crate-cookie-factory-0.3.0-beta5 (c (n "cookie-factory") (v "0.3.0-beta5") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "01cc18szyy0wvy2c3db0lfjah1fs4fjrsgwrj2yh3anl1j8layvx") (f (quote (("std") ("default" "std"))))))

(define-public crate-cookie-factory-0.3.0 (c (n "cookie-factory") (v "0.3.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "1sagdndr0b1jz2n85xmsa6qyldr8759jra4ixnigzxdqm7w82asz") (f (quote (("std") ("default" "std"))))))

(define-public crate-cookie-factory-0.3.1 (c (n "cookie-factory") (v "0.3.1") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "079qy27w42dz2mj4q6pb9zbi15mvcxb464j8an8vh31g3mc1pwj1") (f (quote (("std") ("default" "std"))))))

(define-public crate-cookie-factory-0.3.2 (c (n "cookie-factory") (v "0.3.2") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "0sqjmw85ckqhppff6gjwmvjpkii35441a51xx7cv0ih3jy2fjv9r") (f (quote (("std") ("default" "std"))))))

(define-public crate-cookie-factory-0.3.3 (c (n "cookie-factory") (v "0.3.3") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.16") (o #t) (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)))) (h "18mka6fk3843qq3jw1fdfvzyv05kx7kcmirfbs2vg2kbw9qzm1cq") (f (quote (("std") ("default" "std" "async") ("async" "futures"))))))

