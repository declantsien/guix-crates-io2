(define-module (crates-io co ok cook-markdown) #:use-module (crates-io))

(define-public crate-cook-markdown-0.0.1 (c (n "cook-markdown") (v "0.0.1") (d (list (d (n "cook-with-rust-parser") (r "^0.0.4") (d #t) (k 0)))) (h "10i7pcsqzjvbh5yysw4n011a36hh3kwpsycq17z94w5dsyd40ial")))

(define-public crate-cook-markdown-0.0.2 (c (n "cook-markdown") (v "0.0.2") (d (list (d (n "cook-with-rust-parser") (r "^0.0.5") (d #t) (k 0)))) (h "0lny2i5n5ascgvr99l26dlggg5gbfdvmyd0szqkj2fqsljwbmx3p") (f (quote (("wasm" "cook-with-rust-parser/wasm"))))))

