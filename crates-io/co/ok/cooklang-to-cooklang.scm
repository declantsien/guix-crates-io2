(define-module (crates-io co ok cooklang-to-cooklang) #:use-module (crates-io))

(define-public crate-cooklang-to-cooklang-0.1.0 (c (n "cooklang-to-cooklang") (v "0.1.0") (d (list (d (n "cooklang") (r "^0.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0mzni96k52hv4c2n8pa48ary07z68yqf2a343lls5yzdd8qygyv6")))

(define-public crate-cooklang-to-cooklang-0.1.1 (c (n "cooklang-to-cooklang") (v "0.1.1") (d (list (d (n "cooklang") (r "^0.1.1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1655fddcak9vqgrbgsjhgd1f6qjrcrhrfsv9ma8w5h4xm5jqzkjx")))

(define-public crate-cooklang-to-cooklang-0.2.0 (c (n "cooklang-to-cooklang") (v "0.2.0") (d (list (d (n "cooklang") (r "^0.2") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "16p7a8rp3ycnv6n20779a9p8jdd7vlc0v1s1wfyc2yizi7gvpzkk")))

(define-public crate-cooklang-to-cooklang-0.3.0 (c (n "cooklang-to-cooklang") (v "0.3.0") (d (list (d (n "cooklang") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1xxvy0hvvajdn8xs1k090j3bagivr8fvhm3dhaywpqxy8bq1r7s0")))

(define-public crate-cooklang-to-cooklang-0.5.0 (c (n "cooklang-to-cooklang") (v "0.5.0") (d (list (d (n "cooklang") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0kkvxwi1g929lfs1v5a8cp1d6fya33axpl32sysfvsyycwqkbjzn")))

(define-public crate-cooklang-to-cooklang-0.7.0 (c (n "cooklang-to-cooklang") (v "0.7.0") (d (list (d (n "cooklang") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1naxdbgvap5bqhzn521qw2z247sarzxpnycchck41zyrpayagx8w")))

(define-public crate-cooklang-to-cooklang-0.8.0 (c (n "cooklang-to-cooklang") (v "0.8.0") (d (list (d (n "cooklang") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0al2k1bmh63d5g9ajmy1z7g2cvdhkn1nfp28k0afgij1n4j5np7m")))

(define-public crate-cooklang-to-cooklang-0.9.0 (c (n "cooklang-to-cooklang") (v "0.9.0") (d (list (d (n "cooklang") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0pz19gfcwrrpshccc2b60hav58qqgafq031mdljljg66dsl5y4mm")))

(define-public crate-cooklang-to-cooklang-0.12.0 (c (n "cooklang-to-cooklang") (v "0.12.0") (d (list (d (n "cooklang") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1f43v63rirzrrskgzl9gzm60550ig5y52fi6k8gqg40vnqjxblal")))

(define-public crate-cooklang-to-cooklang-0.13.0 (c (n "cooklang-to-cooklang") (v "0.13.0") (d (list (d (n "cooklang") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0zym7ngb5by180djin8z0w7xc2hkf4j7k8vp5gw50nkrxffvm442")))

