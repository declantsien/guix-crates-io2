(define-module (crates-io co ok cooked-waker) #:use-module (crates-io))

(define-public crate-cooked-waker-1.0.0 (c (n "cooked-waker") (v "1.0.0") (d (list (d (n "cooked-waker-derive") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "stowaway") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "089br3ly6qdp715c8vnqiscxnlfrs6gbgkvf2rflyh5a3zk5f1q8") (f (quote (("derive" "stowaway" "cooked-waker-derive") ("default" "derive")))) (y #t)))

(define-public crate-cooked-waker-1.0.2 (c (n "cooked-waker") (v "1.0.2") (d (list (d (n "cooked-waker-derive") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "stowaway") (r "^1.1.2") (o #t) (d #t) (k 0)))) (h "050rhfd86bnk86q11qaig64xcpzqazyxf3sby7d662v8w25jw0xh") (f (quote (("derive" "stowaway" "cooked-waker-derive") ("default" "derive")))) (y #t)))

(define-public crate-cooked-waker-2.0.0 (c (n "cooked-waker") (v "2.0.0") (d (list (d (n "stowaway") (r "^1.1.2") (d #t) (k 0)))) (h "0cb6h4hfrq4nxr48wc2zg0nh5fzrfhw34lg99mcdsn4dc18dhd0v") (y #t)))

(define-public crate-cooked-waker-3.0.0 (c (n "cooked-waker") (v "3.0.0") (d (list (d (n "stowaway") (r "^2.0.0") (d #t) (k 0)))) (h "0g9s1h763b4wh3q6dfbvlmlxc3hbr11d5vmmjssrx25sk0iafsyx") (y #t)))

(define-public crate-cooked-waker-4.0.0 (c (n "cooked-waker") (v "4.0.0") (h "05ncbnyi55k2rs6bvwcl328s91sbfzi02mr3ja06yhxbdwch70ay")))

(define-public crate-cooked-waker-5.0.0 (c (n "cooked-waker") (v "5.0.0") (h "0vs07c0am50gxzxz593sqb42jk7xs1fjs992dfydllkhcxfyayql")))

