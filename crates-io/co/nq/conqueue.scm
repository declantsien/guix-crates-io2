(define-module (crates-io co nq conqueue) #:use-module (crates-io))

(define-public crate-conqueue-0.1.0 (c (n "conqueue") (v "0.1.0") (h "0r6vhdlfgvjs4dcrpr07zjraxi0gz5czbf7jqih49ra8zz34lyvf")))

(define-public crate-conqueue-0.1.1 (c (n "conqueue") (v "0.1.1") (h "17z4r1fjxqca95zh8f6j9aws6jf6727zz4hac7pw7r5qc726y6nk")))

(define-public crate-conqueue-0.2.0 (c (n "conqueue") (v "0.2.0") (h "0b1rw0rc5bsc5cmgvqpa77l0fajrr3mfs5zsdqmj511a1kmn08av")))

(define-public crate-conqueue-0.2.1 (c (n "conqueue") (v "0.2.1") (h "1fmpzl35rp5naq2l4rcafn0yjy9chn77zpz4bdd6z8f73dj4s7kw")))

(define-public crate-conqueue-0.3.0 (c (n "conqueue") (v "0.3.0") (h "0qsyisjkcf1hmffvy5jn1vzj4wwa7gq6qy9rh93dv22j6y5j0saj")))

(define-public crate-conqueue-0.4.0 (c (n "conqueue") (v "0.4.0") (h "0rkv88k094b49xmbhm2snh4fxnjpi80nbyll9fbd75bbg5n31i7a")))

