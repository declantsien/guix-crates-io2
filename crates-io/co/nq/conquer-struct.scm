(define-module (crates-io co nq conquer-struct) #:use-module (crates-io))

(define-public crate-conquer-struct-0.1.0 (c (n "conquer-struct") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1xann39ymfbvnfkxwyklsbc2kl49rh0bry7a1y8gnmrvp24gwcw5")))

