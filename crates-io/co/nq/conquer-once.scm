(define-module (crates-io co nq conquer-once) #:use-module (crates-io))

(define-public crate-conquer-once-0.1.0 (c (n "conquer-once") (v "0.1.0") (d (list (d (n "conquer-util") (r "^0.1.0") (f (quote ("back-off"))) (d #t) (k 0)))) (h "11rsapwn6yk1b22v5kmhflbjfkvscspv5dir762argz19sb5cgmf") (f (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.1.1 (c (n "conquer-once") (v "0.1.1") (d (list (d (n "conquer-util") (r "^0.1.0") (f (quote ("back-off"))) (d #t) (k 0)))) (h "129g2h6kq1nvdpkf0hr7lr61q1ijizbjz9w4c030q3c2fs9a45ff") (f (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.1.2 (c (n "conquer-once") (v "0.1.2") (d (list (d (n "conquer-util") (r "^0.2.0") (f (quote ("back-off"))) (d #t) (k 0)))) (h "1pp22dkirj8h3w5cpdswy1nsg8iqyvd0vcxwvb9vxlwl8aa4xyhn") (f (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.2.0 (c (n "conquer-once") (v "0.2.0") (d (list (d (n "conquer-util") (r "^0.2.0") (f (quote ("back-off"))) (k 0)))) (h "13zrnhly3f836az11rjv0kfzg60pmy9874aa9vbwm3jl19h48xkg") (f (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.2.1 (c (n "conquer-once") (v "0.2.1") (d (list (d (n "conquer-util") (r "^0.2.0") (f (quote ("back-off"))) (k 0)))) (h "18zgz5wzbkyjybpp1qbmi6p30253ws4x62chmvxicrs6d7xi5swn") (f (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.3.0 (c (n "conquer-once") (v "0.3.0") (d (list (d (n "conquer-util") (r "^0.3.0") (f (quote ("back-off"))) (k 0)))) (h "1x1ahqj25c9jrvwmyxd0z70868x9bvvki14l788lk51y592slxnk") (f (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.3.1 (c (n "conquer-once") (v "0.3.1") (d (list (d (n "conquer-util") (r ">=0.3.0, <0.4.0") (f (quote ("back-off"))) (k 0)))) (h "053ji063gxirhl1iwlsci48bbxa8q7k0r1a83gxfw8wk14i57d2w") (f (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.3.2 (c (n "conquer-once") (v "0.3.2") (d (list (d (n "conquer-util") (r "^0.3.0") (f (quote ("back-off"))) (k 0)))) (h "12lixkr25rw437wjlkfssdbh5lvynscqk26cw8gnv7x6fnbklvbw") (f (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.4.0 (c (n "conquer-once") (v "0.4.0") (d (list (d (n "conquer-util") (r "^0.3.0") (f (quote ("back-off"))) (k 0)))) (h "1d30445kpsvnp6c1vvpz1pka71ls0ql2aw8kr8v9y9hg3i28l02x") (f (quote (("std") ("default" "std"))))))

