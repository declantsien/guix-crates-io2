(define-module (crates-io co nq conquer-util) #:use-module (crates-io))

(define-public crate-conquer-util-0.1.0 (c (n "conquer-util") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (o #t) (k 0)))) (h "1bdcv22fzry3qf8n75nk0803hlv2acr4577ykmjr0ji1rwcgx4n8") (f (quote (("tls") ("std") ("default" "std") ("back-off" "rand") ("alloc") ("align"))))))

(define-public crate-conquer-util-0.2.0 (c (n "conquer-util") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (o #t) (k 0)))) (h "0ahqn6ari4v51xla1rqa6zppnihxm0gkl427ql8x6sf35i3v4kv5") (f (quote (("tls") ("std") ("random" "back-off" "rand") ("default" "std") ("back-off") ("alloc") ("align"))))))

(define-public crate-conquer-util-0.3.0 (c (n "conquer-util") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (o #t) (k 0)))) (h "10imvg3jaz5x6w11g5bfwnj0q2vp06jcvzkxyf0b64vbhkwfwqz7") (f (quote (("tls" "alloc") ("std") ("random" "back-off" "rand") ("default" "std") ("back-off") ("alloc") ("align"))))))

