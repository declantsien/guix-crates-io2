(define-module (crates-io co nq conque) #:use-module (crates-io))

(define-public crate-conque-0.0.0 (c (n "conque") (v "0.0.0") (h "1l8dcv4f7hmqrlrmq6ljy3qgrydf59b9kc2spnqvvp4ip37a1rwd") (y #t)))

(define-public crate-conque-0.0.0-alpha (c (n "conque") (v "0.0.0-alpha") (h "0bs653yzn3r84hxqsnvciyszlyqlcb3lvlgj8rh2s65rnx0cxnkl")))

