(define-module (crates-io co rr corrosion) #:use-module (crates-io))

(define-public crate-corrosion-0.0.1 (c (n "corrosion") (v "0.0.1") (h "1adx0xljnixy6givgbnfizlq4vxqfzppk74rvndn9d4p30miig3c")))

(define-public crate-corrosion-0.0.2 (c (n "corrosion") (v "0.0.2") (h "0p14xaxbi4bliv8s5w0dl26xjz3qzv9a6ax4mw18fa6wpcq0vv0z")))

