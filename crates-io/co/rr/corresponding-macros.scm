(define-module (crates-io co rr corresponding-macros) #:use-module (crates-io))

(define-public crate-corresponding-macros-0.1.0 (c (n "corresponding-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vi3i3s8rjvxrq4vjlf02z8n7a98clk1fl8l948b2ppw88r5qr7c")))

(define-public crate-corresponding-macros-0.1.1 (c (n "corresponding-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vnsmifh3vy2vl8q7f9zagdjfmxhm76j23gdb3zp4kdhrv6zn5r4")))

(define-public crate-corresponding-macros-0.1.2 (c (n "corresponding-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jlq2hyq699w37v5f9fx5b4a5fikgjwv8zfy0fla82zynbxqbgwv")))

(define-public crate-corresponding-macros-0.1.3 (c (n "corresponding-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a4lmry5j60gb95cqqrn85iva1x0iff6lsqg1sajaajz5cnvdkzq")))

(define-public crate-corresponding-macros-0.1.4 (c (n "corresponding-macros") (v "0.1.4") (d (list (d (n "corresponding") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cj5hm39w44hxmr9pkkw83l6mxvrw6amc55iv7z0gq5y971ymqg9")))

(define-public crate-corresponding-macros-0.1.6 (c (n "corresponding-macros") (v "0.1.6") (d (list (d (n "corresponding") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jvf9sgw4c2llm4f102li2x4bmvqcyswwlxl65j1g42i85mvpghk")))

(define-public crate-corresponding-macros-0.1.7 (c (n "corresponding-macros") (v "0.1.7") (d (list (d (n "corresponding") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05razhq0y597ibd5p007s28dj6pz95ggm00bkx1wgakkxivgck4q")))

(define-public crate-corresponding-macros-0.1.8 (c (n "corresponding-macros") (v "0.1.8") (d (list (d (n "corresponding") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0shnjihffxpb4w5fm38nyqpsss9wb8rcbsdj2synrjf79zwfl2i6")))

