(define-module (crates-io co rr corruption) #:use-module (crates-io))

(define-public crate-corruption-0.1.0 (c (n "corruption") (v "0.1.0") (d (list (d (n "handlebars") (r "^0.20.5") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "mildew") (r "^0.1.2") (d #t) (k 0)))) (h "1yb7nxq6zdwq6mzl4qh42xsy2xl5m1jzp8ginxmihqjdnfnc307n")))

(define-public crate-corruption-0.1.1 (c (n "corruption") (v "0.1.1") (d (list (d (n "handlebars") (r "^0.20.5") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "mildew") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0c04qqsgx8h60jz1ikjz3vxcggyykim8rmwan1py471b9b01nm0m")))

(define-public crate-corruption-0.1.2 (c (n "corruption") (v "0.1.2") (d (list (d (n "handlebars") (r "^0.20.5") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "mildew") (r "^0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1zz8216s1ykjv1i8fivkr2cq87v4zf0pc21wlfymbbiz12q7fk7m")))

