(define-module (crates-io co rr correct) #:use-module (crates-io))

(define-public crate-correct-0.1.0 (c (n "correct") (v "0.1.0") (h "1xm2zmf5cw8pa9hch6vd1wvaawfra6ybfzraaxcvsp5a5i6f5h8a")))

(define-public crate-correct-0.1.1 (c (n "correct") (v "0.1.1") (h "0rrri7rgiyb5nl8m8w48j6mmlahfkr38zi0676bksd4pvhisbgpq")))

