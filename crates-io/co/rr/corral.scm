(define-module (crates-io co rr corral) #:use-module (crates-io))

(define-public crate-corral-0.1.0 (c (n "corral") (v "0.1.0") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iva57fq4b6yjkwjz8ivvbwbz6sbbsxzmdpyqcarrl6bw1lzsl9k")))

(define-public crate-corral-0.1.1 (c (n "corral") (v "0.1.1") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iaavvk06pm71jw98zpgzv2806pm932a226m5v7j8144f07c6kcz")))

(define-public crate-corral-0.1.3 (c (n "corral") (v "0.1.3") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "09r3jlrhn4zk2w9cmkc7gq8psbb4adhzamdk52md8g75pazmk20w")))

(define-public crate-corral-0.1.4 (c (n "corral") (v "0.1.4") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "06lnlkkfk2d19zyiiq23sz5kfa4lxn0djg4slhp57rhi70v6ahha")))

(define-public crate-corral-0.1.5 (c (n "corral") (v "0.1.5") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "0mjaziszxc76ky3myk9bdjlbdkswj3v5n82s7j63napimafz2xsm")))

