(define-module (crates-io co rr corroded_drone) #:use-module (crates-io))

(define-public crate-corroded_drone-0.1.0 (c (n "corroded_drone") (v "0.1.0") (h "0hz779ghf6dqi0a3w14j5ak4d9mgp3dgi77i3cwaq2qfh9xkng4f")))

(define-public crate-corroded_drone-0.1.1 (c (n "corroded_drone") (v "0.1.1") (h "1m241wrzf1ahvgww2pvj13viq7yx4b6b8m71f99yvagkyk83lc6j")))

(define-public crate-corroded_drone-0.1.2 (c (n "corroded_drone") (v "0.1.2") (h "0vjb7x208x4xqn559p1idjmi9qgl3a80hajc22xnaj5fjhwxnc0l")))

(define-public crate-corroded_drone-0.1.3 (c (n "corroded_drone") (v "0.1.3") (h "032dpg9xk6kh6y5m1nzgvv3jvwfrxp2zfrkrqdlzhag8izc9lp3i")))

(define-public crate-corroded_drone-0.1.4 (c (n "corroded_drone") (v "0.1.4") (h "0xw1lc2w1kfddafvxhbbg74byaz7f8qj56sncp1hnlc2pqh6h335")))

(define-public crate-corroded_drone-0.1.5 (c (n "corroded_drone") (v "0.1.5") (h "0wqlvj3x2x75vb3akihn7yc5djyh2fgl9fkqv6ahvzgngw2j9hj6")))

(define-public crate-corroded_drone-0.1.6 (c (n "corroded_drone") (v "0.1.6") (h "0vxhhn81s9figvadjff0jcjwr1w9zpcxpggi38lzc96m5cr5fgfk")))

(define-public crate-corroded_drone-0.1.7 (c (n "corroded_drone") (v "0.1.7") (h "02lsaxyf69zn8p4xwskpsbfzsiniyx5d0gb829g56figj1k4nzcd")))

