(define-module (crates-io co rr corresponding) #:use-module (crates-io))

(define-public crate-corresponding-0.1.1 (c (n "corresponding") (v "0.1.1") (d (list (d (n "corresponding-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1clkkpk6mm7q5zmfmwb57kwmx3grynpwn9qnz3ggd79wdapd0474")))

(define-public crate-corresponding-0.1.2 (c (n "corresponding") (v "0.1.2") (d (list (d (n "corresponding-macros") (r "^0.1") (d #t) (k 0)))) (h "0plflqzlzrm7xgizwxjbi305h721cqsrcryyk61r6pyfqg169s39")))

(define-public crate-corresponding-0.1.3 (c (n "corresponding") (v "0.1.3") (d (list (d (n "corresponding-macros") (r "^0.1") (d #t) (k 0)))) (h "1i26xlj8zqqdvhzq1i4c88frplb6cbvwcnhrls74bz4s9nys88kr")))

(define-public crate-corresponding-0.1.4 (c (n "corresponding") (v "0.1.4") (d (list (d (n "corresponding-macros") (r "^0.1") (d #t) (k 0)))) (h "02gyvkpzj0fykwig5x1dxdgy59kcxl04yy4l6vvxaga0yvbb5wsv")))

(define-public crate-corresponding-0.1.5 (c (n "corresponding") (v "0.1.5") (d (list (d (n "corresponding-macros") (r "^0.1") (d #t) (k 0)))) (h "1n2bqz7ddn9j4ggf0in0ha1nbn375dj0np28l8zjsyc2jcqa8j4m")))

(define-public crate-corresponding-0.1.6 (c (n "corresponding") (v "0.1.6") (d (list (d (n "corresponding-macros") (r "^0.1") (d #t) (k 0)))) (h "1g2zx9bi6prix6qigw30v26djkz5w34jcly8v4pi4b3i5gv4h8zg")))

(define-public crate-corresponding-0.1.7 (c (n "corresponding") (v "0.1.7") (d (list (d (n "corresponding-macros") (r "^0.1") (d #t) (k 0)))) (h "0mwsxqjih8v4msyifk888rmhmznvr67f2r2mfvvk23xx23yvp3pk")))

(define-public crate-corresponding-0.1.8 (c (n "corresponding") (v "0.1.8") (d (list (d (n "corresponding-macros") (r "^0.1") (d #t) (k 0)))) (h "0nyk4g69zba8j6p0v50ilp7b84nicq2vzmh6l6rjkllsmymiqkqz")))

