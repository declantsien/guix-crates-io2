(define-module (crates-io co rr correct_word) #:use-module (crates-io))

(define-public crate-correct_word-0.1.0 (c (n "correct_word") (v "0.1.0") (h "1arvpv7z94md3ay6lcrsbz169ldinfxgm1847zxzkbycl8zzw6ma")))

(define-public crate-correct_word-0.1.1 (c (n "correct_word") (v "0.1.1") (h "1pn3v5cfh0z1fm4qj9ysjgv3bg8dllcr9h7li1v9jadyz1z49xqw")))

(define-public crate-correct_word-0.1.2 (c (n "correct_word") (v "0.1.2") (h "1lfjr6npmnnc0klgqv0ml6jcn5z195a2axczf33bb7qsqqwy5lk9")))

