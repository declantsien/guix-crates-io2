(define-module (crates-io co rr corrupt) #:use-module (crates-io))

(define-public crate-corrupt-0.1.0 (c (n "corrupt") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dbns294wqbb71bhgdi7bq3p4q103fgjd1zdfcxhh4dyl80gll1n")))

