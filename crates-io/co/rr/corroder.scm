(define-module (crates-io co rr corroder) #:use-module (crates-io))

(define-public crate-corroder-0.1.0 (c (n "corroder") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^0.2.2") (d #t) (k 0)) (d (n "assert_matches") (r "^1.0.0") (d #t) (k 0)) (d (n "assert_ne") (r "^0.2.4") (d #t) (k 0)))) (h "1y40zj2q5nqc9w9iy5icxs3slf6fqg2a6yj1a3vqy1j442z3p4jx")))

