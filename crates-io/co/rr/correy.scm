(define-module (crates-io co rr correy) #:use-module (crates-io))

(define-public crate-correy-0.1.0 (c (n "correy") (v "0.1.0") (h "0wdwzvyrcy0i49y5hbf8zllm57pb34qyp2sgayxi5rxkvhz21hd5")))

(define-public crate-correy-0.1.1 (c (n "correy") (v "0.1.1") (h "1gqjhvmi2rv9nqc9wng5rhrspvz1ayj1yqwsh1ggbfhvvd1j544k")))

(define-public crate-correy-0.1.2 (c (n "correy") (v "0.1.2") (h "09j29fggqz8f64ccg0df8m77aj8macr3v7m05yhxawmihsp5if5r")))

