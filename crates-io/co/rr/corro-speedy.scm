(define-module (crates-io co rr corro-speedy) #:use-module (crates-io))

(define-public crate-corro-speedy-0.8.7 (c (n "corro-speedy") (v "0.8.7") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "glam") (r ">=0.15, <=0.24") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1") (o #t) (d #t) (k 0)) (d (n "memoffset") (r "^0.8") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "regex") (r "^1") (o #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "speedy-derive") (r "=0.8.6") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "uuid") (r "^1") (o #t) (k 0)))) (h "1fnsiwkm59ylsprjrzizkmcrafrsicb6aw2y6yh8bdwj5798vg6w") (f (quote (("external_doc") ("default" "speedy-derive"))))))

