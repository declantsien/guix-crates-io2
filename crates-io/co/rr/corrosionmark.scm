(define-module (crates-io co rr corrosionmark) #:use-module (crates-io))

(define-public crate-CorrosionMark-0.0.0 (c (n "CorrosionMark") (v "0.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1pninx385lbfqpq46hzny83zcrc1n8das2m3xd321v10lnara816")))

(define-public crate-CorrosionMark-0.0.1 (c (n "CorrosionMark") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "0a5680kndrjk8h0qzv8hkwzx2k6rv62pypc2w3xsmscl2vvbcz08")))

(define-public crate-CorrosionMark-0.1.0 (c (n "CorrosionMark") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fk05fqpr9v74bfhn5qci5ll05d8kqmnc3dyncb29arbi3g40g31")))

(define-public crate-CorrosionMark-0.1.1 (c (n "CorrosionMark") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lv1fl3sf1h85j1h9pfha3ja5djrwmja5jv8vn2b9gc396y66ga5")))

