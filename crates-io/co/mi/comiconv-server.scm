(define-module (crates-io co mi comiconv-server) #:use-module (crates-io))

(define-public crate-comiconv-server-0.1.0 (c (n "comiconv-server") (v "0.1.0") (d (list (d (n "comiconv") (r "^0.2.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0bar74cr39jxz18gpc1x3p1m6w3qrg8lvn1hknyirkd75mmcilqn")))

(define-public crate-comiconv-server-0.1.1 (c (n "comiconv-server") (v "0.1.1") (d (list (d (n "comiconv") (r "^0.2.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0c1ga9lv8gzmczicxrgss70xh860qicclp211g2x2kj9xra6c591")))

(define-public crate-comiconv-server-0.1.2 (c (n "comiconv-server") (v "0.1.2") (d (list (d (n "comiconv") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0avha7rw22s5lnlzn9vndyiqjlxk20q9fsam3mx1y7fa5g5wahhh")))

