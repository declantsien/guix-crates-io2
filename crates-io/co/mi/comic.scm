(define-module (crates-io co mi comic) #:use-module (crates-io))

(define-public crate-comic-0.1.0 (c (n "comic") (v "0.1.0") (d (list (d (n "domafic") (r "^0.1") (d #t) (k 0)) (d (n "memenhancer") (r "^0.1") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "svgbob") (r "^0.1") (d #t) (k 0)))) (h "0gqmfxl2041b0xzjldmxxyz6lv2ks4iwn04icc3ffd040nnrmqax")))

