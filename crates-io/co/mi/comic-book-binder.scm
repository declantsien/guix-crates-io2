(define-module (crates-io co mi comic-book-binder) #:use-module (crates-io))

(define-public crate-comic-book-binder-0.1.0 (c (n "comic-book-binder") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0khvp4b6wf3szaf5frwywm4mfqbvaic0akpsb83s05y39fwdkh3n")))

