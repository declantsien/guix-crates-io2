(define-module (crates-io co pt coptic) #:use-module (crates-io))

(define-public crate-coptic-0.1.0 (c (n "coptic") (v "0.1.0") (h "1wcxy4wfacvhzzsqlvq9g34amzcww7bfgpmcs1mm4as3zldm9z0q")))

(define-public crate-coptic-0.1.1 (c (n "coptic") (v "0.1.1") (h "16l8j0ciziqld5ibfz83y9r4kpqaqcxdygfh5s3ixhwgnzwyyb2g")))

