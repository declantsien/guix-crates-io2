(define-module (crates-io co pt copt) #:use-module (crates-io))

(define-public crate-copt-0.1.0 (c (n "copt") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("codec"))) (d #t) (k 0)) (d (n "tpkt") (r "^0.1.0") (d #t) (k 0)))) (h "01x4fff09sfzvr3il234agp0vfym4sryh8v9fvs41p8a8w88f7p7")))

