(define-module (crates-io co wo cowonverter) #:use-module (crates-io))

(define-public crate-cowonverter-1.0.0 (c (n "cowonverter") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0d1bzlfn3nnvjm199s5nz4na037zx2fqhg2h2w5aa1jirbr62qnf") (y #t)))

(define-public crate-cowonverter-2.0.0 (c (n "cowonverter") (v "2.0.0") (h "1wy5vqc60vahchh437byr4vfpl5v9m5jh913l9fhnfds9i81c0y9")))

(define-public crate-cowonverter-2.1.0 (c (n "cowonverter") (v "2.1.0") (h "1kc68y300bsrmw81pnd8qgmr4cx7kx2j16d2jqiivzrccz85y7hb")))

(define-public crate-cowonverter-2.2.0 (c (n "cowonverter") (v "2.2.0") (h "00415lvi982zqrsdijj7b3ch0ngsw88jsnk5fp3lc8klq3zi188z")))

(define-public crate-cowonverter-2.3.0 (c (n "cowonverter") (v "2.3.0") (h "1ca94p5gncw6fv7z0h8mr3fqinnpcz6qdcx8vl3c81ml01fa5n59")))

(define-public crate-cowonverter-2.4.0 (c (n "cowonverter") (v "2.4.0") (h "1gl99pwl4gcn6ma4m1572j47sd422y6p6sh5w048083sq9rfi1k9")))

(define-public crate-cowonverter-2.5.0 (c (n "cowonverter") (v "2.5.0") (h "1v5y3r9pqznhjmw77rbgx17mvqw4fzcpiknm6drrbjm0j4lv0wsi")))

