(define-module (crates-io co ol cool-id-generator) #:use-module (crates-io))

(define-public crate-cool-id-generator-0.1.1 (c (n "cool-id-generator") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "08hm7a892asx0ay5ynxr9ww78dqys5p75jj2yqrmdbfhdg7jnq5q")))

(define-public crate-cool-id-generator-0.1.2 (c (n "cool-id-generator") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0f8rk63clz9b848bidbls1ns1znv1yq3jdcpfifnphvv2j89i448")))

(define-public crate-cool-id-generator-0.2.0 (c (n "cool-id-generator") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "02bd8d7abq5rv7qn0b191nhjgg8fj4gjg711qkd1w05hkfj77lrh")))

(define-public crate-cool-id-generator-1.0.0 (c (n "cool-id-generator") (v "1.0.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0vrdjf5v4d2nji1cmqmfsjj7d3pvvj2gx1y18m8blcpbyrvv2l9h")))

(define-public crate-cool-id-generator-1.0.1 (c (n "cool-id-generator") (v "1.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0gdly1axjqzskgxizcqz3b74m0c2i509p2sq4p08n4kin67zhv4l")))

