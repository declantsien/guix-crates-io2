(define-module (crates-io co ol coolprop-rs) #:use-module (crates-io))

(define-public crate-coolprop-rs-0.1.0 (c (n "coolprop-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "1r0wfsd8lhbrpvr6lr2w5jvw1awdwycnhy2h2dzzhdyhcbyh7knd")))

(define-public crate-coolprop-rs-0.2.0 (c (n "coolprop-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "1b6b0p79995qc5grmgybdass34ppq6k1l409394p312nd3chxq1r")))

