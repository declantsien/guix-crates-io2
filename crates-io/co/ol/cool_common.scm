(define-module (crates-io co ol cool_common) #:use-module (crates-io))

(define-public crate-cool_common-0.1.0 (c (n "cool_common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("json" "env-filter"))) (d #t) (k 0)))) (h "1x1phr8xdxb53gvxmmcp9w2y358xlj2csmin774vzd22rssrzbfp")))

