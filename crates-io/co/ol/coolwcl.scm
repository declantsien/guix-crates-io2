(define-module (crates-io co ol coolwcl) #:use-module (crates-io))

(define-public crate-coolwcl-1.0.0 (c (n "coolwcl") (v "1.0.0") (h "0pdyjzgmckhdsixqpn4pp4xcahwc52nijw9j4fgs8inrvq0f6abf")))

(define-public crate-coolwcl-1.0.1 (c (n "coolwcl") (v "1.0.1") (h "15x9lmh86ir17289gfylf6c5mjalf39f7kbwi3fqgay2a7wxa4nq")))

