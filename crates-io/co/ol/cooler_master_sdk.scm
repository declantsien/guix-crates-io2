(define-module (crates-io co ol cooler_master_sdk) #:use-module (crates-io))

(define-public crate-cooler_master_sdk-0.1.0 (c (n "cooler_master_sdk") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0y6fjsaaba3k7nxw2ak7k6b0dp3l7x5q2k2nr6rx9pzn2w8b7wib") (l "SDKDLL")))

(define-public crate-cooler_master_sdk-0.1.1 (c (n "cooler_master_sdk") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "08isv23b1yjp75zn95an27zlyl2msn649rrxpl0932j1faq942j2") (l "SDKDLL")))

(define-public crate-cooler_master_sdk-0.1.2 (c (n "cooler_master_sdk") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0zfgqskd7vslj3s5012w47d82a0dbw6kca2drd61jlrlrr9g2zc0") (l "SDKDLL")))

(define-public crate-cooler_master_sdk-0.1.3 (c (n "cooler_master_sdk") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1aqh424vgc9qxipw154i64ndq6lnhgvlj2xk54lm0yq7kx6m32f2") (l "SDKDLL")))

