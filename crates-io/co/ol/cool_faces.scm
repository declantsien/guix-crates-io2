(define-module (crates-io co ol cool_faces) #:use-module (crates-io))

(define-public crate-cool_faces-0.1.1 (c (n "cool_faces") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0y878hkj5yl6ww7qjh548r54p8wl753000mvv44acjspnymx4q64")))

(define-public crate-cool_faces-0.1.2 (c (n "cool_faces") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0qybrkvsnb3pqbmhx3dadmbmbi87nnm09z4wh6gbq037hf25sy08")))

(define-public crate-cool_faces-0.1.3 (c (n "cool_faces") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0agkc4vr6misb49a4yw1nx9vrdbpr1msi0v1ibq5v2x4fbhdavz3")))

(define-public crate-cool_faces-0.1.4 (c (n "cool_faces") (v "0.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0r9v1sds0npzypd2lwllrb6kz5rp8qafrx3mbif2yz6n517kd5bq")))

