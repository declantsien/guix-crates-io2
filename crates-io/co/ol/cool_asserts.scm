(define-module (crates-io co ol cool_asserts) #:use-module (crates-io))

(define-public crate-cool_asserts-1.0.0 (c (n "cool_asserts") (v "1.0.0") (h "0hzc8pcn743rh3b6v1f2y17iyfmbcg0fpzdx0jbazxqwm6p14ygq")))

(define-public crate-cool_asserts-1.0.1 (c (n "cool_asserts") (v "1.0.1") (h "1x0pgy89lmxj8kx51cqijckrh7cj9rnvs2n18g9hsq0ns40jgzi8")))

(define-public crate-cool_asserts-1.0.2 (c (n "cool_asserts") (v "1.0.2") (h "164rc9317k43r1saiibghj0fg8zqgz3xv61bk5fbby0vmwizqpmp")))

(define-public crate-cool_asserts-1.0.3 (c (n "cool_asserts") (v "1.0.3") (h "0xliacsxp5kdjyh0byd74b5cmf2ff3dckpb16g6x2fp73mhxkwf3")))

(define-public crate-cool_asserts-1.1.0 (c (n "cool_asserts") (v "1.1.0") (h "11k4bwj7srj0klj5akzcvpc3k7sxv7cxfskr6qqi1n2jzdfw1srq")))

(define-public crate-cool_asserts-1.1.1 (c (n "cool_asserts") (v "1.1.1") (d (list (d (n "rustversion") (r "^1.0.5") (d #t) (k 2)))) (h "0hy4rqg4fhncwl4aqz3xiiks92w32cix24gm64694abxpnf0r9zy")))

(define-public crate-cool_asserts-2.0.0 (c (n "cool_asserts") (v "2.0.0") (d (list (d (n "indent_write") (r "^2.2.0") (k 0)))) (h "01973d1ihn6x5m1506v69rcrknypb3vfsig42k6rdwn104429m0z")))

(define-public crate-cool_asserts-2.0.1 (c (n "cool_asserts") (v "2.0.1") (d (list (d (n "indent_write") (r "^2.2.0") (k 0)))) (h "1gl6sf8q84kf23smvfwnh9wq2ai2g5pg1hkrscbkw8mmayybwhw7")))

(define-public crate-cool_asserts-2.0.2 (c (n "cool_asserts") (v "2.0.2") (d (list (d (n "indent_write") (r "^2.2.0") (k 0)))) (h "1rbisrqx1lpimr0vshn4fgarrdr3y200jlnhjx1a10lkj6m7pqy6")))

(define-public crate-cool_asserts-2.0.3 (c (n "cool_asserts") (v "2.0.3") (d (list (d (n "indent_write") (r "^2.2.0") (k 0)))) (h "1v18dg7ifx41k2f82j3gsnpm1fg9wk5s4zv7sf42c7pnad72b7zf")))

