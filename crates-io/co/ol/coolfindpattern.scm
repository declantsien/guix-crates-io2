(define-module (crates-io co ol coolfindpattern) #:use-module (crates-io))

(define-public crate-coolfindpattern-0.1.0 (c (n "coolfindpattern") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "16rv3hvjc2m5npsqicn1iqzna4mk5p3pf87qfddxr8lfk6hqpbps")))

(define-public crate-coolfindpattern-0.1.1 (c (n "coolfindpattern") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1g0z3dz4j7s80c3fbipxx0l3486axwlyzpv6qsczkzkn029knv51")))

(define-public crate-coolfindpattern-0.1.2 (c (n "coolfindpattern") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0fp2w3hw1qxgjpwx114w93335yalqhjgq7xcksbjacmywmn7ppnx")))

(define-public crate-coolfindpattern-0.1.3 (c (n "coolfindpattern") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1hxc74rx6hyvbisax06xw805acp2g947g2sm9kl9h0m7097jbfhm")))

(define-public crate-coolfindpattern-0.1.4 (c (n "coolfindpattern") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1iw2fvqkrr5wd5g3r4s625amrzm6yjhycy0qpq4xmlwhqpa5mx39")))

(define-public crate-coolfindpattern-0.1.5 (c (n "coolfindpattern") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0jlqmzvdj0sa857nbc8mmcwl7cbkj8133j9nvzr3swwxzgyqrwx3")))

(define-public crate-coolfindpattern-0.1.6 (c (n "coolfindpattern") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1433df3kk4vnlbrcmbsfnj0n5rhh2kh82c7k7gkbqa0cbh78wg80")))

