(define-module (crates-io co ol coolbox) #:use-module (crates-io))

(define-public crate-coolbox-0.1.0 (c (n "coolbox") (v "0.1.0") (h "0d6yjaiap9gvpgs0iyc08rwf8289qcbh7b37fbbszilcja9kxj08") (y #t)))

(define-public crate-coolbox-0.1.1 (c (n "coolbox") (v "0.1.1") (h "0v18ynb871xx9kq4hrxm9lnqw8wgia18vsy66afawbcg5rqhn8i8") (y #t)))

(define-public crate-coolbox-0.1.2 (c (n "coolbox") (v "0.1.2") (h "0452id19i3ird676599mh3fq6sf2hm6vp0r2q3xl7a59mgphjwx6") (y #t)))

(define-public crate-coolbox-0.1.21 (c (n "coolbox") (v "0.1.21") (h "135fphrr81gxh7vxhld4yx16d41i07l55ylclzv9bxgrdz5vqrxq") (y #t)))

