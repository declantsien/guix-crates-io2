(define-module (crates-io co ol coolor) #:use-module (crates-io))

(define-public crate-coolor-0.1.0 (c (n "coolor") (v "0.1.0") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 2)))) (h "12f85hgvsj29nsf4rdf89yy3iifz1bjvkrx9v8w67cav2yn81a5d")))

(define-public crate-coolor-0.1.1 (c (n "coolor") (v "0.1.1") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 2)))) (h "1gdv2dv4nlwlh3qsvyj53ga8gx9f3f6jd5m3iwrkdlg6xllich5d")))

(define-public crate-coolor-0.2.0 (c (n "coolor") (v "0.2.0") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1mzn376s3x5cpgc65a8vxcsrsn8kqxb5236a3w4mq7gs6vh6ycrj")))

(define-public crate-coolor-0.2.1 (c (n "coolor") (v "0.2.1") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0niyq9v7dmclp7k1mxlzm3jad643yn5m6li3v74l8s8j7w86bhwg")))

(define-public crate-coolor-0.3.0 (c (n "coolor") (v "0.3.0") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1x238912xl7d9i0vfcrc4p6a3ab5alhfcly9nkyybnk7a511zbmb") (f (quote (("default"))))))

(define-public crate-coolor-0.3.1 (c (n "coolor") (v "0.3.1") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "04q0jln58x1jaicvmzbipqqlwcclaginf3w3zbn2x0ax17plb1mp") (f (quote (("default"))))))

(define-public crate-coolor-0.4.0 (c (n "coolor") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.22.1") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0pn2l6an67jrhj1b8gg7si9r7wv26kr3ywnbxf8bhlwdwd9fpsxa") (f (quote (("default"))))))

(define-public crate-coolor-0.5.0 (c (n "coolor") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.23.1") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1bdaqnbpgnr19ma0wi0bkf9smz938ffq0753c662znd0bj07lkdg") (f (quote (("default"))))))

(define-public crate-coolor-0.5.1 (c (n "coolor") (v "0.5.1") (d (list (d (n "crossterm") (r ">=0.23.2") (o #t) (d #t) (k 0)) (d (n "crossterm") (r ">=0.23.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "11pvplrm51xjr0mmhwrjaif6vd7p2xpmdbwd0rbx2kr4iq4c4smd") (f (quote (("default"))))))

(define-public crate-coolor-0.6.0 (c (n "coolor") (v "0.6.0") (d (list (d (n "ansi_colours") (r "^1.2.2") (d #t) (k 0)) (d (n "crossterm") (r ">=0.23.2") (o #t) (d #t) (k 0)) (d (n "crossterm") (r ">=0.23.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "08sy1pbmi8gfwy1whkrxdpbcyf7p2n72gh6afrdcz90685aiscsi") (f (quote (("default"))))))

(define-public crate-coolor-0.6.1 (c (n "coolor") (v "0.6.1") (d (list (d (n "ansi_colours") (r "^1.2.2") (d #t) (k 0)) (d (n "crossterm") (r ">=0.23.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "10li2rlf8dac7q2hcqz0qmiprw3kxr6q5w781in1w1dv4wwn8ngv") (f (quote (("default"))))))

(define-public crate-coolor-0.6.2 (c (n "coolor") (v "0.6.2") (d (list (d (n "ansi_colours") (r "^1.2.2") (d #t) (k 0)) (d (n "crossterm") (r ">=0.23.2, <=0.25.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1yr00vw5z9v2w78xqff77ss9xww7ibdyn45agg0pppjx0y44386h") (f (quote (("default"))))))

(define-public crate-coolor-0.7.0 (c (n "coolor") (v "0.7.0") (d (list (d (n "ansi_colours") (r "^1.2.2") (d #t) (k 0)) (d (n "crossterm") (r "=0.23.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "11jyy1pf0l4yrsy38y4flmq5qdzwwlr0bn5pa5z6q8jwrqxkygfc") (f (quote (("default"))))))

(define-public crate-coolor-0.8.0 (c (n "coolor") (v "0.8.0") (d (list (d (n "crossterm") (r "=0.23.2") (o #t) (d #t) (k 0)) (d (n "glassbench") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (d #t) (k 2)))) (h "1sjrs0k2ng69hilqrrkfayr2rjpwa6b6krp11y3da64zj10kxklm") (f (quote (("default"))))))

(define-public crate-coolor-0.9.0 (c (n "coolor") (v "0.9.0") (d (list (d (n "crossterm") (r "^0.27.0") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (d #t) (k 2)))) (h "0yz2glmcnjlllchbgj96kkyj3d69jijqrzz1xsmidfbz4ivkks9p") (f (quote (("default"))))))

