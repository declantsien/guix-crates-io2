(define-module (crates-io co un country-parser) #:use-module (crates-io))

(define-public crate-country-parser-0.1.0 (c (n "country-parser") (v "0.1.0") (d (list (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "169xx4md8wcpmj1diwq788g9hdkfsdi3vsshqk6kbxlnj0d6cykp")))

(define-public crate-country-parser-0.1.1 (c (n "country-parser") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)))) (h "0q3vy58axgzfylj6qa7r9dhrp504sa2vzff6cz7cn1c162l27vhx")))

