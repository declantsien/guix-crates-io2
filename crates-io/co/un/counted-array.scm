(define-module (crates-io co un counted-array) #:use-module (crates-io))

(define-public crate-counted-array-0.1.0 (c (n "counted-array") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "13vq7c6x64aacy7zf6zsinrbck9xqc3rlhvcpsvdahgp798q2473") (f (quote (("nightly" "lazy_static/nightly"))))))

(define-public crate-counted-array-0.1.1 (c (n "counted-array") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "16vb983i0rqvd5c5jnwllkdpkmww2wb4a4gv5lllz8rshn9f49nq") (f (quote (("nightly" "lazy_static/nightly"))))))

(define-public crate-counted-array-0.1.2 (c (n "counted-array") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "1zj8yr39pb0q2v4gskbcr9rs3lh90xrpn4p0nqh0k2aw2x9qqkrq") (f (quote (("nightly" "lazy_static/nightly"))))))

