(define-module (crates-io co un counting-macros) #:use-module (crates-io))

(define-public crate-counting-macros-0.1.0 (c (n "counting-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fbcxd10yzkmj8hf7mx7i1s4g71rn5bp4rj8p9d4i866kp21gf2r")))

(define-public crate-counting-macros-0.2.0 (c (n "counting-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1awnrk312n06cxih3g00q53nlw71avs7fwnwkvhz17hmdahah05m")))

