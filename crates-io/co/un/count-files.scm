(define-module (crates-io co un count-files) #:use-module (crates-io))

(define-public crate-count-files-0.1.0 (c (n "count-files") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^5.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "1i8zv532i0c9rrpm0dn23csizv4dyl6mhys0axrnclq4417clxbk")))

(define-public crate-count-files-0.2.0 (c (n "count-files") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^5.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "0wckwvxlzkm9h6234pmv1m1z0bsskbiv9aa0radwh8iw5jfi5qx7")))

