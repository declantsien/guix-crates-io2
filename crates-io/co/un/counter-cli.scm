(define-module (crates-io co un counter-cli) #:use-module (crates-io))

(define-public crate-counter-cli-0.4.0 (c (n "counter-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0s73sp22j77ky9r7j81scd6a094jw4rdkjlzm1b1hxxvn043jp8i")))

(define-public crate-counter-cli-0.5.0 (c (n "counter-cli") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0jzgs06vlvsaxr3sfs30vxsbsnxp5arm4xk358v5rfsia3kprnij")))

(define-public crate-counter-cli-0.5.1 (c (n "counter-cli") (v "0.5.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0mwps3jrxdv8fln9n87flqa5ixq02mqagb3dicq4in55m0mvwb9y")))

(define-public crate-counter-cli-0.5.2 (c (n "counter-cli") (v "0.5.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "05mbs0wfmz4cy2rlwvdv2vv4584sl0dpjy3pchdyqkj8zbncsl68")))

(define-public crate-counter-cli-0.5.3 (c (n "counter-cli") (v "0.5.3") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0yhj996a3lvqh6wyg40yakgs9kg10q9yfm60126i7rgx3n4ckn4p")))

