(define-module (crates-io co un countdown) #:use-module (crates-io))

(define-public crate-countdown-0.1.0 (c (n "countdown") (v "0.1.0") (h "04vvk8fjigl9jixc02v8x1pi86zf7mg4npn1jcw533223nb3sk51")))

(define-public crate-countdown-0.1.1 (c (n "countdown") (v "0.1.1") (h "1i2f4hxvs9m9pyxhqdq4b50w5mm9cypin630hsamq7ykqda99rkx")))

(define-public crate-countdown-0.1.2 (c (n "countdown") (v "0.1.2") (h "0jx4q622rhk4rix3g5xzvx3ha8mlbf5khnhwmiiir7qzi6pzs07d")))

