(define-module (crates-io co un count-tys) #:use-module (crates-io))

(define-public crate-count-tys-0.1.0 (c (n "count-tys") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "1jdpbnsa8zfibniqayzsvwq1k2y5pnhd244yb460p7689mksz0q3")))

(define-public crate-count-tys-0.1.1 (c (n "count-tys") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.15") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "0kqy94x7rblljazdwndl968b3nqsciypi3gnv8lhzvqw6g7ndxry")))

(define-public crate-count-tys-0.1.2 (c (n "count-tys") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5.15") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "1h48n4r9wljsr4zw47ngdfy59mwqkn0c1l58yjwwrjym5mnz6hff")))

