(define-module (crates-io co un countries-tools) #:use-module (crates-io))

(define-public crate-countries-tools-0.1.0 (c (n "countries-tools") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "parse-display") (r "^0.8") (d #t) (k 0)))) (h "0s3d7ssci6w01x4a10l8hjpg4s1q97spx0ygrjp48705bqfklv09") (f (quote (("short-names") ("default" "short-names"))))))

(define-public crate-countries-tools-0.2.0 (c (n "countries-tools") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "parse-display") (r "^0.8") (d #t) (k 0)))) (h "03a8zzq89z7rcz868x2q0m5rl5g5i9nij0f234njnk85jsfbg976") (f (quote (("short-names") ("default" "short-names"))))))

