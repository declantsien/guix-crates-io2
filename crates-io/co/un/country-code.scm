(define-module (crates-io co un country-code) #:use-module (crates-io))

(define-public crate-country-code-0.0.0 (c (n "country-code") (v "0.0.0") (h "0kbzpq1l90rqiwkqhzi9aj9s2rajzjch3dajrbqnfbw9lpkir9kg")))

(define-public crate-country-code-0.1.0 (c (n "country-code") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "08pvl06yyfkfkwn08w8nwdjlvxjkz5prb7gjn7cdqaihc0fqp7mc") (f (quote (("std") ("default" "std"))))))

(define-public crate-country-code-0.2.0 (c (n "country-code") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "impl-macros") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1j0lwp9gz7dxfwgbrvj29hcpn8wdq84zizmgs8frbk1q380x06jq") (f (quote (("std") ("default" "std"))))))

(define-public crate-country-code-0.2.1 (c (n "country-code") (v "0.2.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "impl-macros") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gh701xgykfwz0xx8pf1h0jlmhj3sd5fgyaqhj72qlksmkvgxbdn") (f (quote (("std") ("default" "std"))))))

(define-public crate-country-code-0.3.0 (c (n "country-code") (v "0.3.0") (d (list (d (n "impl-macros") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1j5qmrwhwsspy1izbp85204msls9i5qibvkypgin2qi7rr3nl4v6") (f (quote (("std") ("default" "std"))))))

