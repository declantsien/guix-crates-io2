(define-module (crates-io co un counter-fpy) #:use-module (crates-io))

(define-public crate-counter-fpy-0.1.0 (c (n "counter-fpy") (v "0.1.0") (h "08bl51glkbla8l0ibr376f8v1h79afbc7s5bjhsiqmsf71202f70") (r "1.74.0")))

(define-public crate-counter-fpy-0.1.2 (c (n "counter-fpy") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0cyh7h558f8v9jmbv66f97s2vkpa11jm07ryyv2zj1ry8idry51b") (r "1.74.0")))

