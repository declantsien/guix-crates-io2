(define-module (crates-io co un count-digits) #:use-module (crates-io))

(define-public crate-count-digits-0.1.0 (c (n "count-digits") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "021mqajha8216db2c08pw81zh7sxndsrgiqq4d9bk2gq9yga46jq") (r "1.67.1")))

(define-public crate-count-digits-0.2.0 (c (n "count-digits") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "1dxl3k5bgm92fr62zinhqrsr6zxdkc2vj9n1l5gy77y6yai3084n") (r "1.71.1")))

(define-public crate-count-digits-0.2.1 (c (n "count-digits") (v "0.2.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "1gh4rjryaj7hxhy7si11vjva0x535mcj1pk1rr1imlpz4i9n8c3v") (r "1.71.1")))

(define-public crate-count-digits-0.2.2 (c (n "count-digits") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "0qpxsynvv952lg8dgd1k2mccll31n6vxgzn2vjzbncdxy7g265bl") (r "1.71.1")))

(define-public crate-count-digits-0.2.3 (c (n "count-digits") (v "0.2.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "0cy7ys8syy8kfaify78kzrmr650p3j51vc5q4h7kmr0i90is84mn") (r "1.71.1")))

(define-public crate-count-digits-0.2.4 (c (n "count-digits") (v "0.2.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "063fnfign680zvjl4q95h4wgl3qfbj90agixkm7i9lbxzrdnjwcp") (r "1.71.1")))

(define-public crate-count-digits-0.2.5 (c (n "count-digits") (v "0.2.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "1dcpdqzmd7qdd3pap57qgr21fbsydkjydwj4qa8rclim2yrnjnqn") (r "1.71.1")))

(define-public crate-count-digits-0.2.6 (c (n "count-digits") (v "0.2.6") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "0pi3j7c4qp4gdsidgirk3k7zb2gd7rm0d1fjh153pzm1w731zr39") (r "1.71.1")))

(define-public crate-count-digits-0.3.0 (c (n "count-digits") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "06zcd3xnmbw3acqv2xz8c020a7kxv4gl5vzakwmzjw18nl62k53f") (r "1.71.1")))

(define-public crate-count-digits-0.3.1 (c (n "count-digits") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "12qm5ak1mg7ay6pvx4is5w9rlqgb91fxsw2gn6jkgkjy5fwpmhsk") (r "1.71.1")))

(define-public crate-count-digits-0.4.0 (c (n "count-digits") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "1ggcwrzplc1hbnb5irq2f8gq0ngk0h6xmakkc9m4c9w1rsmbmazn") (r "1.71.1")))

(define-public crate-count-digits-0.5.0 (c (n "count-digits") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "0rsbi15ais7ak17zrrb7vif3l6y3qjd57km78rhk82lxxf0g4b1s") (r "1.71.1")))

(define-public crate-count-digits-0.5.1 (c (n "count-digits") (v "0.5.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "19ws5r89cyv7rxm8bwzr9lqjj4zxk52symsrbrz9sk6ndl4g9ixr") (r "1.71.1")))

