(define-module (crates-io co un counting_sort) #:use-module (crates-io))

(define-public crate-counting_sort-1.0.0 (c (n "counting_sort") (v "1.0.0") (d (list (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)))) (h "03za18fh24py5hb5ajf3k1jqjk4ja8hh0ljd1hpq33y0iy6a6mrw")))

(define-public crate-counting_sort-1.0.1 (c (n "counting_sort") (v "1.0.1") (d (list (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)))) (h "0rzai1faxpawqs6frrwk887ndh4a389b83cfbk1inqdky0gia6ja")))

(define-public crate-counting_sort-1.0.2 (c (n "counting_sort") (v "1.0.2") (d (list (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)))) (h "1wdfh9pxiipliwpdqlvs0wyr34ihns5xzng7spxv6s075i1cazd2")))

(define-public crate-counting_sort-1.0.3 (c (n "counting_sort") (v "1.0.3") (d (list (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)))) (h "12sknwvz3iv61xa85vhvw164pyfm3zf4md5g12i0vwarzgl20y99")))

(define-public crate-counting_sort-1.0.4 (c (n "counting_sort") (v "1.0.4") (d (list (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)))) (h "0wcwqd96szrcyadv347xf8a5p25d5smyiwxf8w6vmyr6b5dzhdcr")))

(define-public crate-counting_sort-1.0.5 (c (n "counting_sort") (v "1.0.5") (d (list (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)))) (h "0w2gpinh709wvcsd16xk7wf872jf861ypk3x9cyzapva6ys2946s")))

(define-public crate-counting_sort-1.0.6 (c (n "counting_sort") (v "1.0.6") (d (list (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)))) (h "11gp6if173yzsgynhmshayna4mph487ivcgzxbdrnm5nnrmxj678")))

(define-public crate-counting_sort-1.0.7 (c (n "counting_sort") (v "1.0.7") (d (list (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)))) (h "1i03f965f3zyi0snwsbmls9an07zyxnhzwy4x56yndibxs6i86sc")))

(define-public crate-counting_sort-1.0.8 (c (n "counting_sort") (v "1.0.8") (d (list (d (n "oorandom") (r ">=11.1.0, <12.0.0") (d #t) (k 2)))) (h "0nk642yfrrablb4hjmg3mgpc6rn26p1rhwrkq77ik6awam93np8j")))

(define-public crate-counting_sort-1.0.9 (c (n "counting_sort") (v "1.0.9") (d (list (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)))) (h "0yl92kh5dsskk31mzyh0i0hs7inbnvi6b5ixgrwacyb4mw2zggiy")))

(define-public crate-counting_sort-1.0.10 (c (n "counting_sort") (v "1.0.10") (d (list (d (n "oorandom") (r "^11.1.0") (d #t) (k 2)))) (h "1bs2n7xbd0mmrqgflar6jkcmylgrv0cdvy50dmd2cml69ighvsid")))

