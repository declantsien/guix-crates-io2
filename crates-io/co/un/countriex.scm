(define-module (crates-io co un countriex) #:use-module (crates-io))

(define-public crate-countriex-0.1.0 (c (n "countriex") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m923xyyrz9f119rndr231f7sp94sjwnbxn7izsy1y8pxnfx44ic")))

(define-public crate-countriex-0.1.1 (c (n "countriex") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0m13lhf1zc8mn8s9rs9afy3zgcvrqm70ri1qxd2qpnz6dbblzjz0")))

