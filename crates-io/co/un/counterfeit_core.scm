(define-module (crates-io co un counterfeit_core) #:use-module (crates-io))

(define-public crate-counterfeit_core-0.2.0 (c (n "counterfeit_core") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0bh81s378mg0sqfgg18x9bllmah64cpwn1vi8a164izn8iqkc72a")))

(define-public crate-counterfeit_core-0.2.1 (c (n "counterfeit_core") (v "0.2.1") (d (list (d (n "hyper") (r "^0.14.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1xw178i84n5jmpfimrixk51ghs5bv42kg87ldvlwcxj2qz6rfnxz")))

