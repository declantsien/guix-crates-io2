(define-module (crates-io co un count-zeroes) #:use-module (crates-io))

(define-public crate-count-zeroes-0.1.0 (c (n "count-zeroes") (v "0.1.0") (h "0lk35r4448n09hzff77sh6f3mjhpa6y4y9naycm2hddy420irlni")))

(define-public crate-count-zeroes-0.2.0 (c (n "count-zeroes") (v "0.2.0") (h "1jiksa6mxjad1afr36ziq7jvr9hks546bd863950bkfcvgiqf3y8")))

(define-public crate-count-zeroes-0.2.1 (c (n "count-zeroes") (v "0.2.1") (h "0wn1lyscig2v52anwz1d4xfdfhfh8ibh6q6rkm9msy3gjlfv2cx2")))

