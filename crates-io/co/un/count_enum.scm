(define-module (crates-io co un count_enum) #:use-module (crates-io))

(define-public crate-count_enum-0.1.0 (c (n "count_enum") (v "0.1.0") (d (list (d (n "algtype") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.4.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.4.0") (d #t) (k 2)))) (h "06cgn79378a1ldmjfxcj1shc27wgcd0hga68w5sva947jgh9gsh7")))

