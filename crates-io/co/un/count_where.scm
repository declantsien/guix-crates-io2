(define-module (crates-io co un count_where) #:use-module (crates-io))

(define-public crate-count_where-0.1.0 (c (n "count_where") (v "0.1.0") (h "009j3niynzqwanhy8lfws1c36ngyr5hj39nn76s7976kg3wjfvyr")))

(define-public crate-count_where-0.1.1 (c (n "count_where") (v "0.1.1") (h "0wh39s8cmd2gr742zz31k6zwaqigizn2chv51am4h94x345p26wi")))

