(define-module (crates-io co un council) #:use-module (crates-io))

(define-public crate-council-0.0.0 (c (n "council") (v "0.0.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.39") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1xhn3flpi7avpvp0r7mby05aqn52x47i966kyxi71yv4968s9lix")))

