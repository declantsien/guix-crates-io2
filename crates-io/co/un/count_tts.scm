(define-module (crates-io co un count_tts) #:use-module (crates-io))

(define-public crate-count_tts-0.1.0 (c (n "count_tts") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0whcjplry6zns8d19b03i2g09v7p1vaaziaxhqzyb6rpkrlid385")))

(define-public crate-count_tts-0.2.0 (c (n "count_tts") (v "0.2.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1chblk2i4w4dq7dfmnb84ffn40mbwham97y3bx87ycmh8z5qi6w0")))

