(define-module (crates-io co un countrs) #:use-module (crates-io))

(define-public crate-countrs-0.1.0 (c (n "countrs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)))) (h "0k46rikzin304m9pk27b8jwxhi137gl9w5b1prcv81mjs5ykcsfr") (f (quote (("types" "chrono") ("default" "types" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-countrs-0.1.1 (c (n "countrs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)))) (h "12mcv67d3bw40cywx9q5dvxpp0v7rm1jzka3hpna29yws65mw3iz") (f (quote (("types" "chrono") ("default" "types" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-countrs-0.1.2 (c (n "countrs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)))) (h "1vf2q6f3hmqy2kixj40wk8sd2ba60j9xgyd9wpj2g11m2zxxl2r9") (f (quote (("types" "chrono") ("default" "types" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-countrs-0.1.3 (c (n "countrs") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)))) (h "1kk9i7ns4ggfxg465mjg9blqcg4cdk1vi49g1vc1clpghw3yga0q") (f (quote (("types" "chrono") ("default" "types" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-countrs-0.1.4 (c (n "countrs") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)))) (h "0w06kpqslfmjgldjs6wv87m10di5ch95zd6r14r9mvdwk53kyapw") (f (quote (("types" "chrono") ("default" "types" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-countrs-0.1.5 (c (n "countrs") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)))) (h "0f780ivykxr6rl6p8gri4kx669yq5p0cjsnv0d6nkf4f7sh3qags") (f (quote (("types" "chrono") ("default" "types" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

(define-public crate-countrs-0.1.6 (c (n "countrs") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)))) (h "1b2mqqm3xdw84k8kpqn47ng5k3f90mr45vpjn0nn12kwspirva22") (f (quote (("types" "chrono") ("default" "types" "chrono")))) (s 2) (e (quote (("chrono" "dep:chrono"))))))

