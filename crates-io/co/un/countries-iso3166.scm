(define-module (crates-io co un countries-iso3166) #:use-module (crates-io))

(define-public crate-countries-iso3166-1.0.0 (c (n "countries-iso3166") (v "1.0.0") (h "0vhankg8j9ja0ssn0rdribiw7k07763cz8jnxnwzkn5yc87vhwb2")))

(define-public crate-countries-iso3166-1.0.1 (c (n "countries-iso3166") (v "1.0.1") (h "15bwwkyra68zlvbgvm6xsl346s954w98895xpirj7yqaa827zv7m")))

(define-public crate-countries-iso3166-1.0.2 (c (n "countries-iso3166") (v "1.0.2") (h "1wdgw5kdgsyv8n057043px7h3h3i2frjxkdzkq0bgjaj62d72ahr")))

