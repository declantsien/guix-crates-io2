(define-module (crates-io co un countdown_latch) #:use-module (crates-io))

(define-public crate-countdown_latch-0.1.0 (c (n "countdown_latch") (v "0.1.0") (h "01lpfawv95sv8qgzz4b8jll5ax37f5wd938c1rzhzz7yac63c467") (y #t)))

(define-public crate-countdown_latch-0.1.1 (c (n "countdown_latch") (v "0.1.1") (h "0vxm6nqnkzvrdiw2ynai54j2by7lf4rhf7i43y5wy167dbcmzy7f")))

