(define-module (crates-io co un countmap) #:use-module (crates-io))

(define-public crate-countmap-0.1.1 (c (n "countmap") (v "0.1.1") (h "1b2qmvkr1w2kpx2h25j5l3jyqk3gnr47w2vwnh1jf8zgji2gcqy5")))

(define-public crate-countmap-0.2.0 (c (n "countmap") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "19n0fb6g08gfbqjngvrhq8hg6lz4nq54h0k5h83mcn5gqh1s9why")))

