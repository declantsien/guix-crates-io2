(define-module (crates-io co un counts) #:use-module (crates-io))

(define-public crate-counts-0.1.0 (c (n "counts") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "14h5f71mzkhxxgfjasvln3frnj1daa5ydgv0q5r15n2daf6f20y5")))

(define-public crate-counts-0.1.1 (c (n "counts") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "08vc1yns9r6qw2n7f7gx6a9grkhxry8ma4nrylmvvycqf2zf3n2n")))

(define-public crate-counts-0.1.2 (c (n "counts") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1wj8z588ycxaa4920r28l1brd836ar18ip73x0pp5i8lj3xwk0jp")))

(define-public crate-counts-0.2.0 (c (n "counts") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1bgdzx9gxfaw6a2wi9wzn8v7y23smr1723905kkzlr75imrmsnlh")))

(define-public crate-counts-1.0.0 (c (n "counts") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex-lite") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "16kqnhvjbpb56f2cy9hcjqj58w99xqw91580hbc84fmdlm7bc53n")))

(define-public crate-counts-1.0.1 (c (n "counts") (v "1.0.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "regex-lite") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "04yqjjyxkd2bm3f14l1pm4my9dpi4wsczqj427szv7pipvvxzmci")))

(define-public crate-counts-1.0.2 (c (n "counts") (v "1.0.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "regex-lite") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1wryi6z9jjpdlah41krh0h9nnbgzjz69f4caj6sk3dxi3mqan4ay")))

(define-public crate-counts-1.0.3 (c (n "counts") (v "1.0.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "regex-lite") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0fyv8sh4p9k9x4s4f6yd11xysadkisi9czza3gxqj4fx75gg5wfq") (r "1.59")))

(define-public crate-counts-1.0.4 (c (n "counts") (v "1.0.4") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "regex-lite") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "19xbfn122lzpsz55b0qqyy9pywnhb39zr7bdi45dsd3pppn29c24") (r "1.59")))

