(define-module (crates-io co un counterfeit) #:use-module (crates-io))

(define-public crate-counterfeit-0.1.0 (c (n "counterfeit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "04k4zwzla76s3ps0fkhqs5wk8gsrsadn0sdh9lcnvyi9nn4dylad")))

(define-public crate-counterfeit-0.2.0 (c (n "counterfeit") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "counterfeit_core") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "08xsdfg83xjhs32l2x36f3mn7nlml9pdaxl01kymm24x0v7y5pva")))

(define-public crate-counterfeit-0.2.1 (c (n "counterfeit") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "counterfeit_core") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14.1") (f (quote ("server" "http1" "tcp"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jzmam9zhf9gz4bglxvkn49p4hzqsyhf8sgp8gsyvfg2apivrsvb")))

(define-public crate-counterfeit-0.2.2 (c (n "counterfeit") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "counterfeit_core") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14.1") (f (quote ("server" "http1" "tcp"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zq0796zah7gknllq49drbqhq80vxzcsak8g35wifvpgv9653g4x")))

