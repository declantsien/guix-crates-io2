(define-module (crates-io co un countrycode) #:use-module (crates-io))

(define-public crate-countrycode-0.1.0 (c (n "countrycode") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)))) (h "1rm5jvb84khypxdaslnmnbnxp7p78yi762na5w4p6ss54q3maywh") (f (quote (("default" "once_cell"))))))

(define-public crate-countrycode-0.1.1 (c (n "countrycode") (v "0.1.1") (d (list (d (n "chrono-tz") (r "^0.6") (k 0)) (d (n "country-code") (r "^0.2") (f (quote ("std" "serde"))) (k 0)) (d (n "csv") (r "^1.1") (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-field-with") (r "^0.1.2") (f (quote ("std"))) (k 0)))) (h "1bd6r1xq13g317wvhqkmx65kn4wqkpi9b9y1l5kz2nyx8vvmrm24") (f (quote (("default" "once_cell"))))))

(define-public crate-countrycode-0.1.2 (c (n "countrycode") (v "0.1.2") (d (list (d (n "chrono-tz") (r "^0.6") (k 0)) (d (n "country-code") (r "^0.2") (f (quote ("std" "serde"))) (k 0)) (d (n "csv") (r "^1.1") (k 0)) (d (n "once_cell") (r "^1.10") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-field-with") (r "^0.1.2") (f (quote ("std"))) (k 0)))) (h "0xlhb83yr8z4m0qharda28wnd4jkqvj336hbc7mp2188s159a28j") (f (quote (("default" "once_cell"))))))

(define-public crate-countrycode-0.2.0 (c (n "countrycode") (v "0.2.0") (d (list (d (n "chrono-tz") (r "^0.8") (k 0)) (d (n "country-code") (r "^0.3") (f (quote ("std" "serde"))) (k 0)) (d (n "csv") (r "^1") (k 0)) (d (n "once_cell") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-field-with") (r "^0.1.2") (f (quote ("std"))) (k 0)))) (h "1a6dm9vipcanfqis32xfbxivj1rig1in9p40b0kjmr16c1s3z17w") (f (quote (("default" "once_cell"))))))

