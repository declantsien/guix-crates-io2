(define-module (crates-io co un count_cache) #:use-module (crates-io))

(define-public crate-count_cache-0.1.0 (c (n "count_cache") (v "0.1.0") (h "18n62al9cy53x7awlmivld4cjbvkhqpsl605dnlyd5knk554fidd")))

(define-public crate-count_cache-0.1.1 (c (n "count_cache") (v "0.1.1") (h "17hx0j4yaavddw6d5i9sanivg25xrdyzz6mfnzhd8pzr30whicch")))

