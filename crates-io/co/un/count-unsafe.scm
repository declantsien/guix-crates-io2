(define-module (crates-io co un count-unsafe) #:use-module (crates-io))

(define-public crate-count-unsafe-0.1.0 (c (n "count-unsafe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-geiger-serde") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "geiger") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0594z3gxsw30z61l5msaqynzwi2qgh5wjml1jzbd3sdyp1xav0sg")))

(define-public crate-count-unsafe-0.1.1 (c (n "count-unsafe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-geiger-serde") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "geiger") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0irg0himq67f6pg6abbf90ky928kxcljr3sia95fkiy4nplsq776")))

