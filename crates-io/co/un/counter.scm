(define-module (crates-io co un counter) #:use-module (crates-io))

(define-public crate-counter-0.1.0 (c (n "counter") (v "0.1.0") (h "0bj6jaq5hc6lba7w3vrc4sardpf6wfdlm138g8r9qm4gl0glgqcl")))

(define-public crate-counter-0.2.0 (c (n "counter") (v "0.2.0") (h "1abmn5dax8prc4mnjgrl0lb47k1banbdah6c3rxdfljpsnsxmxvs")))

(define-public crate-counter-0.2.1 (c (n "counter") (v "0.2.1") (h "0ccrbb5sn3gq7gyvjj85rkr32i7mgvn9iq73yxh4vz0kg90mxji2")))

(define-public crate-counter-0.3.0 (c (n "counter") (v "0.3.0") (h "1daywxsy36dja128z9bza3qxw0n9zfsbp9igvvwdhmrjjx9pg20n")))

(define-public crate-counter-0.3.1 (c (n "counter") (v "0.3.1") (h "05c4zvqa7zak91n6lkci8vvm28zwa6qdhdf06bx2c0j0h6jq763r")))

(define-public crate-counter-0.3.2 (c (n "counter") (v "0.3.2") (h "1qb756h6l7vpsb4955q3sj42glzjr87j8c33vm8kasq0mg0kchx2")))

(define-public crate-counter-0.3.3 (c (n "counter") (v "0.3.3") (h "179m7r7a4dw8p7p79m1lf3a10zqdn1ccjnyzyf4pygrydwig73ys")))

(define-public crate-counter-0.4.0 (c (n "counter") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1lmisfvp1hhjn30nmhlczr066m25sx8s1q7p6ipir8pnjzyqh1ha")))

(define-public crate-counter-0.4.1 (c (n "counter") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0pzg2p1pa2b0j8bd80njp6dw3b52crjn6kxmy1440cp83l8w7blj")))

(define-public crate-counter-0.4.2 (c (n "counter") (v "0.4.2") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0xf5hljnxrmw3lmx15wsjr806y7wb37pwyga26wl34wjnml9wyn1")))

(define-public crate-counter-0.4.3 (c (n "counter") (v "0.4.3") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0q5kdjdj3rg37h0lw0s45ajliklpcyrqjqi88y0glr690b76cjyq")))

(define-public crate-counter-0.5.0 (c (n "counter") (v "0.5.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0x0qhw0dxx8q0vkzlpb31715xhy0mwjd6xfl97iw0820y273hx29")))

(define-public crate-counter-0.5.1 (c (n "counter") (v "0.5.1") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0r6p0zqr7q11gkc17lz7rrh59wwfhi7qmk4xdj7vpdw5h8kp5s51")))

(define-public crate-counter-0.5.2 (c (n "counter") (v "0.5.2") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1zj5fl71l8y8zlyfhd5y8c5lnzs0ifyacblhfp1pj9y5rxip73v9")))

(define-public crate-counter-0.5.3 (c (n "counter") (v "0.5.3") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1vc53av7pd3yc5rd752ixkjw0wzg29nynibg647iwikvkq4971w7")))

(define-public crate-counter-0.5.4 (c (n "counter") (v "0.5.4") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0xmi354b2nlkp0k1mc823vgkd1vnbpm1r9dfsk61r8v57scy563p")))

(define-public crate-counter-0.5.5 (c (n "counter") (v "0.5.5") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "11zzy6i69nfn51dryl0cvc8mgxbz09i4171nnybkvd80rmx5vc33")))

(define-public crate-counter-0.5.6 (c (n "counter") (v "0.5.6") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vwmgmkm1ampy48gvadvcdd2pz7rlsv340wn22b32q53263qsf28")))

(define-public crate-counter-0.5.7 (c (n "counter") (v "0.5.7") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1kfw3l651rixf1y6hnc3r9sj0m2ig5zvpkzzsdpzaj4kk5k8wi9d")))

