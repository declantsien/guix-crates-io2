(define-module (crates-io co un count-min-sketch) #:use-module (crates-io))

(define-public crate-count-min-sketch-0.1.0 (c (n "count-min-sketch") (v "0.1.0") (d (list (d (n "clippy") (r ">= 0.0.87") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)))) (h "0y3sbgl71x6wl5k7if3mliwpwkyvzycw12vq6r8ipqfp6nmkvckh") (f (quote (("nightly") ("default"))))))

(define-public crate-count-min-sketch-0.1.1 (c (n "count-min-sketch") (v "0.1.1") (d (list (d (n "clippy") (r ">= 0.0.87") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)))) (h "1sqbdgvkg28rj6m5wvdnh349znq6c714f84qhyi9fcy2adrgnf2a") (f (quote (("nightly") ("default"))))))

(define-public crate-count-min-sketch-0.1.2 (c (n "count-min-sketch") (v "0.1.2") (d (list (d (n "clippy") (r ">= 0.0.87") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "siphasher") (r "~0.1") (d #t) (k 0)))) (h "0fs7lh7wm3x56v7kh9ibkhaim4myvdx2mz46jypf5jlfvqlw2lik") (f (quote (("nightly") ("default"))))))

(define-public crate-count-min-sketch-0.1.3 (c (n "count-min-sketch") (v "0.1.3") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0.3.15") (d #t) (k 0)) (d (n "siphasher") (r "~0.1") (d #t) (k 0)))) (h "1ximx63vxslg09fk2fw03y5ws0q0n32n3ssx87gzv52baghsyzmp") (f (quote (("nightly") ("default"))))))

(define-public crate-count-min-sketch-0.1.4 (c (n "count-min-sketch") (v "0.1.4") (d (list (d (n "clippy") (r ">= 0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0.4") (d #t) (k 0)) (d (n "siphasher") (r "~0.2") (d #t) (k 0)))) (h "13zwkka69n3dzg6c3i92s1czdbp7mc0fqmp8fnby192bwrdwx648") (f (quote (("nightly") ("default"))))))

(define-public crate-count-min-sketch-0.1.5 (c (n "count-min-sketch") (v "0.1.5") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1fmgfvvaf6g2f67lqnp3zd1a53wr266ww540pqq1syxw9n5n9iwl") (f (quote (("nightly") ("default"))))))

(define-public crate-count-min-sketch-0.1.6 (c (n "count-min-sketch") (v "0.1.6") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "0cwb9g2x5j5pi45k9rs5qjn2k1wzx4lzqch07w21nja0n93hgqbq") (f (quote (("nightly") ("default"))))))

(define-public crate-count-min-sketch-0.1.7 (c (n "count-min-sketch") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "1izc1z1drnj58md6cjbx2n3sy22p449bcy0dlafr8s3v1pirycfa")))

