(define-module (crates-io co un count-write) #:use-module (crates-io))

(define-public crate-count-write-0.1.0 (c (n "count-write") (v "0.1.0") (d (list (d (n "futures-io-preview") (r "^0.3.0-alpha.19") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.2.0-alpha.6") (o #t) (d #t) (k 0)))) (h "11bswmgr81s3jagdci1pr6qh9vnz9zsbbf2dqpi260daa2mhgmff") (f (quote (("tokio" "tokio-io") ("futures" "futures-io-preview"))))))

