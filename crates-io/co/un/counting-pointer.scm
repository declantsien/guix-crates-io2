(define-module (crates-io co un counting-pointer) #:use-module (crates-io))

(define-public crate-counting-pointer-0.1.0 (c (n "counting-pointer") (v "0.1.0") (d (list (d (n "gharial") (r "^0.3") (d #t) (k 2)))) (h "1q1lxfqxgywanx7l59p2rha8dszkghk3v21nfzfls3agy7zh3fyf")))

(define-public crate-counting-pointer-0.2.0 (c (n "counting-pointer") (v "0.2.0") (d (list (d (n "gharial") (r "^0.3") (d #t) (k 2)))) (h "07vax20bkx22pxv0jpiik4mgdx782bhgi0cx119xkvqp0ik58qwz")))

