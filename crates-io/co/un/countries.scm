(define-module (crates-io co un countries) #:use-module (crates-io))

(define-public crate-countries-0.1.3 (c (n "countries") (v "0.1.3") (d (list (d (n "async-graphql") (r "^4") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1ab2gnpjrf2mjj0d08ags10nqy2jzldv0whzn30bfwi76fbfdkfq") (f (quote (("std" "alloc") ("flags" "bitflags") ("default" "std") ("alloc"))))))

