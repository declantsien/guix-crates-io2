(define-module (crates-io co un countryip) #:use-module (crates-io))

(define-public crate-countryip-0.1.0 (c (n "countryip") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (f (quote ("default"))) (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parallel-stream") (r "^2.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("default"))) (d #t) (k 0)) (d (n "rust_iso3166") (r "^0.1.7") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (f (quote ("paris"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "0yh3jfvzb3a953skz0281fjydgc6cq4gg8bkixsmgaf57d5qn2ia")))

