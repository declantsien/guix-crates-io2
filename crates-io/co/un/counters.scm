(define-module (crates-io co un counters) #:use-module (crates-io))

(define-public crate-counters-0.1.0 (c (n "counters") (v "0.1.0") (h "111b84avqbrk5r8567d7rxjgkv718c5zmx7f4rpy1z7x0pcn84ki")))

(define-public crate-counters-0.2.0 (c (n "counters") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0d9x1rp75i3hpfq12ymjjh3sbxnmb66mxj74gfymn0hcmv8jh264") (f (quote (("serialization" "serde") ("noop"))))))

(define-public crate-counters-0.3.0 (c (n "counters") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1vnjs5a5mgb281a4jgzbk1af6iw9mn9ijir1362h2pfn17wny5z0") (f (quote (("serialization" "serde") ("noop"))))))

(define-public crate-counters-0.4.0 (c (n "counters") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0bng24bjnc9ahcmm2m2jz1v6rjmwyqi9hncvkh4k5n6nf5mvi2lp") (f (quote (("serialization" "serde") ("noop"))))))

(define-public crate-counters-0.5.0 (c (n "counters") (v "0.5.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1spra2r903i195iq7r5lsbxaghajx7bi50jpdrv7amgjb30rwyf1") (f (quote (("dummy_serialization" "serde") ("debug_counters"))))))

