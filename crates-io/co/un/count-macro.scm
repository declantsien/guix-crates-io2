(define-module (crates-io co un count-macro) #:use-module (crates-io))

(define-public crate-count-macro-0.1.0 (c (n "count-macro") (v "0.1.0") (h "0brnfbqry3hxq5n8xv5jz05xwi782n3xfkmi57mrpkzpcfp33p2h")))

(define-public crate-count-macro-0.1.1 (c (n "count-macro") (v "0.1.1") (h "0lhyjjifm3s1prjhcqzqxvbs9bc44g3rqbxy6cy0a2c97rgflg4s")))

(define-public crate-count-macro-0.2.0 (c (n "count-macro") (v "0.2.0") (h "036k8sjpg0ffzgs6qijcpdx09sri6m2sbfdk5ld8l2jvzzh9zzlp")))

(define-public crate-count-macro-0.2.1 (c (n "count-macro") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0pi3vyzahcg0w5zw9l5zns2v611csxbsmqr2p7nrw7w6fj1148b0")))

(define-public crate-count-macro-0.2.2 (c (n "count-macro") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "047r9xlinwj1xmsxpjfl04plq5a2w2bmk4rhrclb39xv3riwbfmg")))

