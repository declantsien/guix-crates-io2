(define-module (crates-io co un count_instructions) #:use-module (crates-io))

(define-public crate-count_instructions-0.1.0 (c (n "count_instructions") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustix") (r "^0.37.0") (f (quote ("process" "thread"))) (d #t) (t "cfg(unix)") (k 0)))) (h "008xzrw6h81g4qxwfacpsbxb7gxhpc3mhiw9zp63y702z93bd5hf")))

(define-public crate-count_instructions-0.1.1 (c (n "count_instructions") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustix") (r "^0.37.0") (f (quote ("process" "thread"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1agzkjpzh3c62rc75w0aw6qaiacv7i0spzs50agr36s4wskc6i3p")))

(define-public crate-count_instructions-0.1.2 (c (n "count_instructions") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustix") (r "^0.37.0") (f (quote ("process" "thread"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1jg84w07p0kdmfx09s438pxy4h5v320j1nbj0xpbhi5qpvfhwsp6")))

(define-public crate-count_instructions-0.1.3 (c (n "count_instructions") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustix") (r "^0.37.0") (f (quote ("process" "thread"))) (d #t) (t "cfg(unix)") (k 0)))) (h "03xlagq6lkw6f6x52cy3xpdik75r93805xdpzx3k0z1cly0dchck")))

(define-public crate-count_instructions-0.1.4 (c (n "count_instructions") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustix") (r "^0.38.0") (f (quote ("pipe" "process" "thread"))) (d #t) (t "cfg(unix)") (k 0)))) (h "0ps0254pvx3nmnxs2v60kv7fqayh82r3jqypb4l3ql3i7s3rzr1n")))

