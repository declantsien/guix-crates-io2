(define-module (crates-io co un country-emoji) #:use-module (crates-io))

(define-public crate-country-emoji-0.1.0 (c (n "country-emoji") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)))) (h "1lqnx8qfw2xf4vr7ylq1cdl8i88lpvxar0qr5xjfx3wd8h5kakr9")))

(define-public crate-country-emoji-0.1.1 (c (n "country-emoji") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)))) (h "0dyymby97ygdcx5aclia3pl5cf5lsjlnzfxc1adrjvk2dazy76az")))

(define-public crate-country-emoji-0.1.2 (c (n "country-emoji") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)))) (h "0qx24bi8dnawzlmwzzid584a4gfms95a3j8sx7h0j0p1l59r1a3z")))

(define-public crate-country-emoji-0.2.0 (c (n "country-emoji") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)))) (h "00ighxr4rxlr316lbac45mbwvm0kh6b70bp5yzla6cylbz0x6gnr")))

