(define-module (crates-io co un countable) #:use-module (crates-io))

(define-public crate-countable-0.0.0 (c (n "countable") (v "0.0.0") (h "1p04gq1nrkw8cymp9j5r7hvz0s8ad629a6wxpfdln7mbxr5y04mc")))

(define-public crate-countable-0.1.0 (c (n "countable") (v "0.1.0") (d (list (d (n "either") (r "^1.5") (k 0)) (d (n "itertools") (r "^0.7.11") (k 0)) (d (n "void") (r "^1") (k 0)))) (h "1jgxn3bb1gxgmr3s3ny8041sjzsjnrdq3l8b1nxd75vrf0vz8g7s")))

