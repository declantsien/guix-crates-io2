(define-module (crates-io co un count-non-zero) #:use-module (crates-io))

(define-public crate-count-non-zero-0.1.0 (c (n "count-non-zero") (v "0.1.0") (h "1bh8haijk4189ncysr90g9dd1i5kf79ny5pgq3n82hv66qd5c0sl") (y #t)))

(define-public crate-count-non-zero-0.2.0 (c (n "count-non-zero") (v "0.2.0") (h "0n4h007cj7vyikrgajj2ghazn1pvj2w8ap66yz88qf2s68s940nf") (y #t)))

