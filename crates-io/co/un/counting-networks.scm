(define-module (crates-io co un counting-networks) #:use-module (crates-io))

(define-public crate-counting-networks-0.1.0 (c (n "counting-networks") (v "0.1.0") (h "0xbkh9v24jkc8nqk0iwsk6rxpb7hz4nacpc9p9fvb2maibyvp6gj")))

(define-public crate-counting-networks-0.1.1 (c (n "counting-networks") (v "0.1.1") (h "0982rz7ppc20agvd57f3q6yhp6ms7r513fy4db96vamzwi2igc6p")))

(define-public crate-counting-networks-0.1.2 (c (n "counting-networks") (v "0.1.2") (h "0354cpvpcbmclqw0qi6fk0vlsa6xq5r2jjgalmi1869barm6mjcr")))

(define-public crate-counting-networks-0.1.3 (c (n "counting-networks") (v "0.1.3") (h "06cjd1mbf1ap63pfhxldbkw7hmv0kv03krwb59v31xm7nxzl4bar")))

