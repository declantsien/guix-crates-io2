(define-module (crates-io co un count_sort) #:use-module (crates-io))

(define-public crate-count_sort-0.1.0 (c (n "count_sort") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0fazwm6gh1skzjc0290xqixaavzlbll4rcfp3m492bc474y167g8")))

(define-public crate-count_sort-0.1.1 (c (n "count_sort") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0m6w1ib74a3w5rqlp7s6w0f67mnm9xxywmj3lg9hf6zlxyshpcsa")))

(define-public crate-count_sort-0.1.2 (c (n "count_sort") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1yd48vj0pgkwii9blaw0384f0la141mkd7bqzbxx3zdy0z3xawh6")))

(define-public crate-count_sort-0.1.3 (c (n "count_sort") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "13nna5lmpwj0kgalsnn17gspkqdqjn43k0a7pqwys7filn64525y")))

(define-public crate-count_sort-0.1.4 (c (n "count_sort") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1sqw7gph5x99iwgrphbkjbj1x0zsarsxnlfv9y87rdxhqs8rm0c4")))

(define-public crate-count_sort-0.1.5 (c (n "count_sort") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "dmsort") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0c18aiijl0fqyp2bcp0jj575yd3ka490x7zcy20kpn0ff94mx7x9")))

(define-public crate-count_sort-0.1.6 (c (n "count_sort") (v "0.1.6") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "dmsort") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1qx1gnpkgrsdizs2zi730hh7w01gwbvli5pnk1f0iwx1x7pk1gfi")))

(define-public crate-count_sort-0.1.7 (c (n "count_sort") (v "0.1.7") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "dmsort") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "19y0mnxp2qarz7rhlzar89s851fna33gq2vx534a1m9h3y1gxcb0")))

(define-public crate-count_sort-0.1.8 (c (n "count_sort") (v "0.1.8") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "dmsort") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1cg05qb9paih1pswad52swcmlx65di6gkrbfzzpbx58lh6dpcasl")))

(define-public crate-count_sort-0.2.0 (c (n "count_sort") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "dmsort") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1c4l3zynlprvbxqkbj3q2nqifw7khs4j42y2hf2br4b25pnjb2ar")))

(define-public crate-count_sort-0.3.0 (c (n "count_sort") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "dmsort") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "14cwzj0jlz30yvzp008441pkhhrybccxpm9r4sxliajwcxwglg98")))

