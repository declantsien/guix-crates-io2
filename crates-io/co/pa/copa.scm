(define-module (crates-io co pa copa) #:use-module (crates-io))

(define-public crate-copa-0.12.0 (c (n "copa") (v "0.12.0") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.18") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "1mk5ag63gvl26j91z9a62y967hnw3in561pg7hsqzczlqy6wfzxs") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.62.1")))

(define-public crate-copa-0.0.18 (c (n "copa") (v "0.0.18") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.18") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "00y31n6dpljahx3xa0wczwm4p1h27wmjlrznjy95g0vjlxzql7wp") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.62.1")))

(define-public crate-copa-0.0.19 (c (n "copa") (v "0.0.19") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.19") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "0p4z75jh2di7firn7ickx1z1cfkmm1fv96jkbf0k5wr0498qs8ys") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.62.1")))

(define-public crate-copa-0.0.20 (c (n "copa") (v "0.0.20") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.20") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "0wcq58jx5qih37aqh8k4bmvp3smq4kjhf38akc22ln4jvnz5sp61") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.72.1")))

(define-public crate-copa-0.0.21 (c (n "copa") (v "0.0.21") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.21") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "1daizzvnjl3pzgla4ixn2cgaixa6xyq3vvsk2is8p0dkqyy3k441") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.72.1")))

(define-public crate-copa-0.0.22 (c (n "copa") (v "0.0.22") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.22") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "1zgbzywnl6ng5b8v51dlhg7k2vbixhig25v8abd327iki6azn62n") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.72.1")))

(define-public crate-copa-0.0.23 (c (n "copa") (v "0.0.23") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.23") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "15g4kxa8rsnzpk9py4adm057r3r5bhx52zm3fy9jll198v0k85c4") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.24 (c (n "copa") (v "0.0.24") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.24") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "1qbc0pxf69rg9hg6aga5p47vxai5iqyf7hfxwxby4kybmr10kdp6") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.25 (c (n "copa") (v "0.0.25") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.25") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "1kknis3b4s1314wmysg55mrial582zj15zrxp52pln3kdfadki4y") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.26 (c (n "copa") (v "0.0.26") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.26") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "0662r36c70mmfzighmzx5ja91lgp5fb59gyivrqkawhqypz4k7h8") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.27 (c (n "copa") (v "0.0.27") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.27") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "0dfds7vgma9gwqp22sh19h3k314a4i44bx2x8dyfnvrwj31pqxvw") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.28 (c (n "copa") (v "0.0.28") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.28") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "1jl12kyhv94ynff97jl2vy253pzzlcc87nj9fiv5228pdfjk56np") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.29 (c (n "copa") (v "0.0.29") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.29") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "10kdx1wss87g8kyjhm51r5wq5yr1qcqq1xnr348083xwg3dy8d3v") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.30 (c (n "copa") (v "0.0.30") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "0baczkyv1k1fgpclnzjmkig6y9m0cscdq83z65k9rs94vvn3gdl7") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.31 (c (n "copa") (v "0.0.31") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.31") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "1mrfpplfxqmbdv9h0wpp4inc1w2z4367a50gzd0cxxddq92vxd45") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.32 (c (n "copa") (v "0.0.32") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.32") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "1k21kfhcpvsakfkdibqknspp3c55p4d7xpqbjysb70hldvffp63s") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.33 (c (n "copa") (v "0.0.33") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.33") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "11mcis3i9kx0fqv74ydri3cqvy16qdld49faimc8xff5kb9kzg8f") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.34 (c (n "copa") (v "0.0.34") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.34") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "0jcs8znnkh89hhn1hxi404wq07prrsp6kbhzd8jf8v7z0sai7d1h") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.35 (c (n "copa") (v "0.0.35") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.35") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "01gcvsswhwldzb9lcs36cp40i579d9nzx0rhg6ng64bykpx34p1k") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.36 (c (n "copa") (v "0.0.36") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.36") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "12c36lb9f51ri8fgjljkik1jrsqqm438x033lpy9awcpd54rm7dc") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.37 (c (n "copa") (v "0.0.37") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.37") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "18qw54b34z97wkc995vwck13lk9yxydj8k4x6z9nsm1w03h2iz7f") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.38 (c (n "copa") (v "0.0.38") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.38") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "0ipyi56gyyflv93wry7hpfkk3zmp5qf6p26cwcjimi217s9al1qx") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

(define-public crate-copa-0.0.39 (c (n "copa") (v "0.0.39") (d (list (d (n "arrayvec") (r "^0.7.4") (o #t) (k 0)) (d (n "rio-proc-macros") (r "^0.0.39") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 2)))) (h "1527f5nxrh8qi9jq27y8a8y6zvklasi7cpq2ia78m69djcrkzbdr") (f (quote (("no_std" "arrayvec") ("nightly") ("default" "no_std")))) (r "1.73.0")))

