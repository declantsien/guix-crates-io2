(define-module (crates-io co gs cogset) #:use-module (crates-io))

(define-public crate-cogset-0.1.0 (c (n "cogset") (v "0.1.0") (h "1bizypdi09qj7vpaj711rsvd163cb5gczv9jxxk2aa7mj0labcgd")))

(define-public crate-cogset-0.1.1 (c (n "cogset") (v "0.1.1") (h "1yq6bi5slvh85m0cyswwlm56x72ii44ahx0k2mf3ly08fv1jgjyj")))

(define-public crate-cogset-0.2.0 (c (n "cogset") (v "0.2.0") (d (list (d (n "order-stat") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "11ph02w0b78qlmm3hxhkdcrzh8ix4pl6sfia9q5980lwjhdwcakz") (f (quote (("unstable"))))))

