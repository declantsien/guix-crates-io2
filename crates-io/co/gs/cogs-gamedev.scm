(define-module (crates-io co gs cogs-gamedev) #:use-module (crates-io))

(define-public crate-cogs-gamedev-0.1.0 (c (n "cogs-gamedev") (v "0.1.0") (d (list (d (n "enum-map") (r "^0.6.4") (d #t) (k 0)))) (h "07dc41sk10lir3s3nl9vianfvh4m8xp3dd1nddqzkzx9nkj6bc23")))

(define-public crate-cogs-gamedev-0.1.1 (c (n "cogs-gamedev") (v "0.1.1") (d (list (d (n "enum-map") (r "^0.6.4") (d #t) (k 0)))) (h "1lb5z5zyp10xw4sa8nfg44y7p4phrnxd9mf7s4sgii6fw5lv6jvl")))

(define-public crate-cogs-gamedev-0.1.2 (c (n "cogs-gamedev") (v "0.1.2") (d (list (d (n "enum-map") (r "^0.6.4") (d #t) (k 0)))) (h "0x8ffz74qgcjhyh3rs50m0q0x2ml0rsr86kwrcb83smbwn4lybrq")))

(define-public crate-cogs-gamedev-0.1.3 (c (n "cogs-gamedev") (v "0.1.3") (d (list (d (n "enum-map") (r "^0.6.4") (d #t) (k 0)))) (h "048fh14wcj26w4ih8fn80acg66w8q4aqjgvrxib2nbl6jlpai9gj")))

(define-public crate-cogs-gamedev-0.1.4 (c (n "cogs-gamedev") (v "0.1.4") (d (list (d (n "enum-map") (r "^0.6.4") (d #t) (k 0)))) (h "02cwyxclx9j6wh8xsbh4ly2faq5npichc03p7w5qq74a66mvl5d4")))

(define-public crate-cogs-gamedev-0.1.5 (c (n "cogs-gamedev") (v "0.1.5") (d (list (d (n "enum-map") (r "^0.6.4") (d #t) (k 0)))) (h "077v5ynixwwh6pv1hmclrcwiph311mn5ksicbl6cmv7qv7f2bx9r")))

(define-public crate-cogs-gamedev-0.1.6 (c (n "cogs-gamedev") (v "0.1.6") (d (list (d (n "enum-map") (r "^0.6.4") (d #t) (k 0)))) (h "0yzza7dzhbz263j9rgyj1if80f36rkvbr29by1z1wpi6adbj2yll")))

(define-public crate-cogs-gamedev-0.1.7 (c (n "cogs-gamedev") (v "0.1.7") (d (list (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)))) (h "00va15zr5zv1h8ix56rivfa3h5hnk48z5spin1m12bdv8g374x5m")))

(define-public crate-cogs-gamedev-0.1.8 (c (n "cogs-gamedev") (v "0.1.8") (d (list (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)))) (h "0ysaml8x27fnj9ppgq3vs9fz16ck0wx4kds0v02b0h0cf6xglqfz")))

(define-public crate-cogs-gamedev-0.1.9 (c (n "cogs-gamedev") (v "0.1.9") (d (list (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)))) (h "0iiayxf7gnmi2kzpjdm6iw8li4wi9r13x9c9xh0j0fs6yb681388")))

(define-public crate-cogs-gamedev-0.1.10 (c (n "cogs-gamedev") (v "0.1.10") (d (list (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19myhcyha0qii2cay3g2bddwdvfcsagy3npbf225vnka4gv65l5b") (f (quote (("default"))))))

(define-public crate-cogs-gamedev-0.1.11 (c (n "cogs-gamedev") (v "0.1.11") (d (list (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14ri9fhhjlc5cbl1s41v0718ky5yvhy8w996fdkfnb96nmv9qqph") (f (quote (("default"))))))

(define-public crate-cogs-gamedev-0.1.12 (c (n "cogs-gamedev") (v "0.1.12") (d (list (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1db6yy4mqn5zpgcdp2yykrvcdgwamwsg63zc4423i6fa850w54n6") (f (quote (("default"))))))

(define-public crate-cogs-gamedev-0.2.0 (c (n "cogs-gamedev") (v "0.2.0") (d (list (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05cvbi6i0fcv9xnq542wd8lpnvz9zxh1iin3fr5rl45mrb9dqnqb") (f (quote (("default"))))))

(define-public crate-cogs-gamedev-0.3.0 (c (n "cogs-gamedev") (v "0.3.0") (d (list (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11dvvcd845plsbv52haha7nhcpn6by2fsic1442g3lavqzfi0fbp") (f (quote (("default"))))))

(define-public crate-cogs-gamedev-0.4.0 (c (n "cogs-gamedev") (v "0.4.0") (d (list (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13kyrn3kh08ckvk0jp5m7qh5ri9w6m5ncd752xzchw836nys4hf6") (f (quote (("default"))))))

(define-public crate-cogs-gamedev-0.4.1 (c (n "cogs-gamedev") (v "0.4.1") (d (list (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0v05yaqc7kj4wvcqwb9j5q69mm4k138k541cbqylxn3larbvwa5n") (f (quote (("default"))))))

(define-public crate-cogs-gamedev-0.5.0 (c (n "cogs-gamedev") (v "0.5.0") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16iscmf787zg7dld8kbv3v80ic37cynx5az65bp4cx5xdgzaa8j4") (f (quote (("default"))))))

(define-public crate-cogs-gamedev-0.6.0 (c (n "cogs-gamedev") (v "0.6.0") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0yhdxjx0c6i5dk9lvkgnqwhvmh24d2zlzzablvsq72c0i6anyncr") (f (quote (("default"))))))

(define-public crate-cogs-gamedev-0.7.0 (c (n "cogs-gamedev") (v "0.7.0") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lwm3h310alg2v8wdcxnn5b7j728lf7nmbqxs7aw09ll06bhlxlj") (f (quote (("default"))))))

(define-public crate-cogs-gamedev-0.8.0 (c (n "cogs-gamedev") (v "0.8.0") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 0)) (d (n "enum-map") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1q0k1m5xs5qj20zbaj6wx419kpa2l9sfvfbrdbrwkymjvl23yc7a") (f (quote (("default"))))))

