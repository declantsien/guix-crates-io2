(define-module (crates-io co gs cogs) #:use-module (crates-io))

(define-public crate-cogs-0.1.0 (c (n "cogs") (v "0.1.0") (h "0avvcnnagrrmxi2wzqsj62dw0r1l6nhgzvxsps57xh8n52vx3rd5")))

(define-public crate-cogs-0.0.1 (c (n "cogs") (v "0.0.1") (h "049wp0qg7ma3wmw07brqya6pxsbkx9jk22zlm5mqin1r8fhj4r6z")))

(define-public crate-cogs-0.0.4 (c (n "cogs") (v "0.0.4") (d (list (d (n "clap") (r "^2.21.2") (d #t) (k 0)) (d (n "elementtree") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "0g7njh30kig2g3b9li8dg7w4g9kv43a4fxwaclg1aprl5kj98g9m")))

(define-public crate-cogs-0.1.1 (c (n "cogs") (v "0.1.1") (d (list (d (n "clap") (r "^2.21.2") (d #t) (k 0)) (d (n "elementtree") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "1p3pfr92ff75i9sbswgkc4xyikncqklndqhh02ddmp3spragx90i")))

