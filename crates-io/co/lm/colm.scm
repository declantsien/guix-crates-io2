(define-module (crates-io co lm colm) #:use-module (crates-io))

(define-public crate-colm-0.1.0 (c (n "colm") (v "0.1.0") (d (list (d (n "aesni") (r "^0.2") (d #t) (k 2)) (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 2)) (d (n "subtle") (r "^0.3") (k 0)))) (h "17mvgk1wjfsi822wm44rwndh1fg9gyyw8c2gyyxmiql707xxiw72") (f (quote (("x32") ("x16") ("default" "x16"))))))

