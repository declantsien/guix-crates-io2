(define-module (crates-io co lm colmac) #:use-module (crates-io))

(define-public crate-colmac-0.1.0 (c (n "colmac") (v "0.1.0") (h "01qwkr4kzxr6xqw5ls4f4vhmnzi1c8d986a3vpz0jnv1i42r8nim")))

(define-public crate-colmac-0.1.1 (c (n "colmac") (v "0.1.1") (h "19dwcyawndxdfkzbn9zg7767gcipn312l5c9pvhdinzb45yf790x")))

(define-public crate-colmac-0.1.2 (c (n "colmac") (v "0.1.2") (h "0f77a1cb646wvxil2a0miy51v84l3mjbg3pvj29jp7dk3kv0p8h8")))

