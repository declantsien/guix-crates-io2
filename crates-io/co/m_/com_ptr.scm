(define-module (crates-io co m_ com_ptr) #:use-module (crates-io))

(define-public crate-com_ptr-0.1.0 (c (n "com_ptr") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.7") (f (quote ("combaseapi" "unknwnbase"))) (d #t) (t "cfg(not(test))") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("combaseapi" "unknwnbase" "objbase" "wincodec"))) (d #t) (t "cfg(test)") (k 0)))) (h "1m3by6r4v59rva3aa4v1y3l9678q6k7ghpiczs3cvwm8kyrcc1r3")))

(define-public crate-com_ptr-0.1.1 (c (n "com_ptr") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.7") (f (quote ("combaseapi" "unknwnbase"))) (d #t) (t "cfg(not(test))") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("combaseapi" "unknwnbase" "objbase" "wincodec"))) (d #t) (t "cfg(test)") (k 0)))) (h "031pr3p6yg3w85fqbpl52vhd87f4ws8fgs5b7bf4hm273pfznnp7")))

(define-public crate-com_ptr-0.1.2 (c (n "com_ptr") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.7") (f (quote ("combaseapi" "unknwnbase"))) (d #t) (t "cfg(not(test))") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("combaseapi" "unknwnbase" "objbase" "wincodec"))) (d #t) (t "cfg(test)") (k 0)))) (h "1qyw4y6gchpdav4bmy61qxq609h6cyhqwqrzb601hbhjcmzvhi7w")))

(define-public crate-com_ptr-0.1.3 (c (n "com_ptr") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("combaseapi" "unknwnbase"))) (d #t) (t "cfg(not(test))") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("combaseapi" "unknwnbase" "objbase" "wincodec"))) (d #t) (t "cfg(test)") (k 0)))) (h "0bfpd629f9a2bbmq3xhbbcc4zy6gj5pyqa0xzjrrldk2gpni0yfd")))

(define-public crate-com_ptr-0.2.0 (c (n "com_ptr") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("combaseapi" "unknwnbase" "winbase"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("combaseapi" "unknwnbase" "objbase" "wincodec" "winbase"))) (d #t) (k 2)))) (h "096rcql4dzpblkpw14mn8950ijix00i1s3wsf269l6a850dqk336")))

(define-public crate-com_ptr-0.2.1 (c (n "com_ptr") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("combaseapi" "unknwnbase" "winbase"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("combaseapi" "unknwnbase" "objbase" "wincodec" "winbase"))) (d #t) (k 2)))) (h "0rpfk23lnn905z7gc6vdi12kbb0yzc9yk2q7187vgan3mqgw4h81")))

