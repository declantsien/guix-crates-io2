(define-module (crates-io co m_ com_macros) #:use-module (crates-io))

(define-public crate-com_macros-0.1.0 (c (n "com_macros") (v "0.1.0") (d (list (d (n "com_macros_support") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.13") (d #t) (k 2)))) (h "1k2ax1v1bgsvgfgyb1ni78zr7dh17ramd34y99jyxfw07vmlfgag")))

(define-public crate-com_macros-0.2.0 (c (n "com_macros") (v "0.2.0") (d (list (d (n "com_macros_support") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.13") (d #t) (k 2)))) (h "1hghsfldbra8l7ivysldpahb5g0fhsw5704yr3f8v9py89cb01kn")))

(define-public crate-com_macros-0.3.0 (c (n "com_macros") (v "0.3.0") (d (list (d (n "com_macros_support") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "055ba3w6czasxmsd6rv5shva2v6gkznwpw8bn9dmaax5za4qyf3n")))

(define-public crate-com_macros-0.4.0 (c (n "com_macros") (v "0.4.0") (d (list (d (n "com_macros_support") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wxc3qn5jy1w8dafzihxsiqy334rhwacjd6vk3fdqwznmcf3z55q")))

(define-public crate-com_macros-0.5.0 (c (n "com_macros") (v "0.5.0") (d (list (d (n "com_macros_support") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09r3ia69pxpjb402hb8wsp4smszqyv3v926f2v28c3yl8z7zjaj4")))

(define-public crate-com_macros-0.6.0 (c (n "com_macros") (v "0.6.0") (d (list (d (n "com_macros_support") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "198k9fqd9rnpv3x6pxav6g636gl6m30iyqx63r4cfs56h0sqhxfk")))

