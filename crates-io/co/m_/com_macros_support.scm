(define-module (crates-io co m_ com_macros_support) #:use-module (crates-io))

(define-public crate-com_macros_support-0.1.0 (c (n "com_macros_support") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "12i0vdxhrp9fvll2i81dqyr5hzd770yljbn8wy5c1ln5pgh03vfy")))

(define-public crate-com_macros_support-0.2.0 (c (n "com_macros_support") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1nh7w4920k5vbz2qhz59ymm5rrhnjiz5sics60787j2a1z9adscp")))

(define-public crate-com_macros_support-0.3.0 (c (n "com_macros_support") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cimajn746d4b70vzfgnyrpfb7ncbhsj27pi9yq1rmf548isk5as")))

(define-public crate-com_macros_support-0.4.0 (c (n "com_macros_support") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cnr29jvfs0yy7grpnspgyc8w5l18jd2n4dycw2p118a6vs637iq")))

(define-public crate-com_macros_support-0.5.0 (c (n "com_macros_support") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wn16cdyx9ijn6ylg0kwia8p1mnfxhfz0b59wg400p40ra3ia05p")))

(define-public crate-com_macros_support-0.6.0 (c (n "com_macros_support") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "033ix2k6j0930b0gpm77r2zc2d4f5fvpqbbr8ib6sad9hw89m2dd")))

