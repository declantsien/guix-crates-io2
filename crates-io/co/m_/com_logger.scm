(define-module (crates-io co m_ com_logger) #:use-module (crates-io))

(define-public crate-com_logger-0.1.0 (c (n "com_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "uart_16550") (r "^0.2") (d #t) (k 0)))) (h "078sy6xb0fl2rd22s67wircx4x1sqh5w77dpm11pdqd0hm9my5pf") (f (quote (("readme") ("default"))))))

(define-public crate-com_logger-0.1.1 (c (n "com_logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "uart_16550") (r "^0.2") (d #t) (k 0)))) (h "1hh7zqz7k2hjbxbbq4ci61j16hx4csim2fxxkwycc3pp0zqm4p0d")))

