(define-module (crates-io co rk corkscrew-rs) #:use-module (crates-io))

(define-public crate-corkscrew-rs-0.1.0 (c (n "corkscrew-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.16") (d #t) (k 0)))) (h "0qc5gmb411ps83355ijs0b395bjzv4k2m6fs2l0vrkxn6y0802dp")))

