(define-module (crates-io co rn corner-calculator) #:use-module (crates-io))

(define-public crate-corner-calculator-0.1.0 (c (n "corner-calculator") (v "0.1.0") (h "1l24bfz3gvd1g7i479ps4r639dbphfzydm7rcbvagqb2zkkfz2p3") (y #t)))

(define-public crate-corner-calculator-0.1.1 (c (n "corner-calculator") (v "0.1.1") (h "03smv2q9yniqarwgr68i3ygwf2blx42dk5galzn0ykfkls8lqn2i") (y #t)))

(define-public crate-corner-calculator-0.1.2 (c (n "corner-calculator") (v "0.1.2") (h "1r336r8hps02s56ixxkmrp9kgw6cimhhjma1q9g7ains2cb58i8y") (y #t)))

(define-public crate-corner-calculator-0.1.3 (c (n "corner-calculator") (v "0.1.3") (h "1j9mpxwhb73pgjad90fi5sr7r2dxwfk43i9y5n817pcij6nvcbhi")))

