(define-module (crates-io co rn cornucopia_sync) #:use-module (crates-io))

(define-public crate-cornucopia_sync-0.3.0 (c (n "cornucopia_sync") (v "0.3.0") (d (list (d (n "cornucopia_client_core") (r "^0.3.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19.3") (d #t) (k 0)))) (h "1n1q7bcqims3h14rwlf909s6kfsx3wr43acd0g9w2q3iqpvbijh6")))

(define-public crate-cornucopia_sync-0.3.1 (c (n "cornucopia_sync") (v "0.3.1") (d (list (d (n "cornucopia_client_core") (r "^0.3.1") (d #t) (k 0)) (d (n "postgres") (r "^0.19.3") (d #t) (k 0)))) (h "1i57q68ms4adj9n1jnxhvq3qygw3iinss46g1m3dwwqjqz2kmj32")))

(define-public crate-cornucopia_sync-0.4.0 (c (n "cornucopia_sync") (v "0.4.0") (d (list (d (n "cornucopia_client_core") (r "^0.4.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19.4") (d #t) (k 0)))) (h "03nbzfg52x2r26rsjrcjcb0vy5rxws76y78s6h1dvz7qyb57y76s") (f (quote (("with-serde_json-1" "cornucopia_client_core/with-serde_json-1"))))))

