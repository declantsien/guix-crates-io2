(define-module (crates-io co rn cornucopia_client_core) #:use-module (crates-io))

(define-public crate-cornucopia_client_core-0.3.0 (c (n "cornucopia_client_core") (v "0.3.0") (d (list (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)) (d (n "postgres-protocol") (r "^0.6.4") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.3") (d #t) (k 0)))) (h "1iykb98i3v9hph5skdg1mxa4vp6vlg6bh8j50mwranfm17521nym")))

(define-public crate-cornucopia_client_core-0.3.1 (c (n "cornucopia_client_core") (v "0.3.1") (d (list (d (n "fallible-iterator") (r "^0.2") (d #t) (k 0)) (d (n "postgres-protocol") (r "^0.6.4") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.3") (d #t) (k 0)))) (h "0hlxbgsybi760wxyhand94kbpdfhf5vy13b1a12xqiplmxp34wyr")))

(define-public crate-cornucopia_client_core-0.4.0 (c (n "cornucopia_client_core") (v "0.4.0") (d (list (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "postgres-protocol") (r "^0.6.4") (d #t) (k 0)) (d (n "postgres-types") (r "^0.2.4") (d #t) (k 0)) (d (n "serde-1") (r "^1.0.147") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_json-1") (r "^1.0.87") (o #t) (d #t) (k 0) (p "serde_json")))) (h "061gipgpbq6dsr3rzwx0zqpi3p688hzrylwr4krm9jhp34kqg84m") (f (quote (("with-serde_json-1" "postgres-types/with-serde_json-1" "serde-1" "serde_json-1"))))))

