(define-module (crates-io co rn cornflakes_datasize_macro) #:use-module (crates-io))

(define-public crate-cornflakes_datasize_macro-0.0.1 (c (n "cornflakes_datasize_macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "092aqvjz3v622xlpfczi0bw32nv9gms16kfm71vi0z4fjk0kli3k")))

