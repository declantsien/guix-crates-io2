(define-module (crates-io co rn cornflakes) #:use-module (crates-io))

(define-public crate-cornflakes-0.0.0-dev (c (n "cornflakes") (v "0.0.0-dev") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cvgbrgnc6l2yz0haj166a0yc88f7as3m6pcbba1m9669wb4p0q7")))

(define-public crate-cornflakes-0.0.0 (c (n "cornflakes") (v "0.0.0") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)))) (h "18lj11zw138rck70v65yazgl1wzwafw8zf7ix9z0qzlg30dy1x3j")))

(define-public crate-cornflakes-0.0.1 (c (n "cornflakes") (v "0.0.1") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d9p6dsrdiwrc1g5dqi7vx2np9vrsvjcxyxjvy48bgnbymvj0q5p")))

(define-public crate-cornflakes-0.0.2 (c (n "cornflakes") (v "0.0.2") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "cornflakes_datasize_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bh9laa2n87y0j9kp8fs5h938gnrln4zl315nm0h94qd12m24r7n")))

(define-public crate-cornflakes-0.0.3 (c (n "cornflakes") (v "0.0.3") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "cornflakes_datasize_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cp21nncska319bi8rh6vasyn5a6ab3ppgfg6wmvwd0dky9h6hb9")))

(define-public crate-cornflakes-0.0.4 (c (n "cornflakes") (v "0.0.4") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "cornflakes_datasize_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hixy2bb726y7dchp7r2zg0583lifr7ym0g4p7gii9hxk7v4kxyq")))

(define-public crate-cornflakes-0.0.5 (c (n "cornflakes") (v "0.0.5") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "cornflakes_datasize_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sgddw8g3s5ayf3ppcfv8l7w7nl35aamjwiynqda3cnawjbbj1n7")))

(define-public crate-cornflakes-0.0.6 (c (n "cornflakes") (v "0.0.6") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "cornflakes_datasize_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "192wlsqfaj97si7dn47g3h40s3y39n67hckl6jcbdjg0mikba15g")))

(define-public crate-cornflakes-0.0.7 (c (n "cornflakes") (v "0.0.7") (d (list (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "cornflakes_datasize_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iqyniarxzm0a9xh0v82jcip225g7lb07yw191b73sw9ailkx61p")))

