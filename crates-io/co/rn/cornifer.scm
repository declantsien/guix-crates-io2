(define-module (crates-io co rn cornifer) #:use-module (crates-io))

(define-public crate-cornifer-0.0.1 (c (n "cornifer") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled" "blob"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "00sirk8f0aqryhqdr5x1f49iqmm0lnk6xrk7mq6p4354ry6mfq9i")))

