(define-module (crates-io co rn cornerstore) #:use-module (crates-io))

(define-public crate-cornerstore-0.1.0 (c (n "cornerstore") (v "0.1.0") (d (list (d (n "bustle") (r "^0.4.2") (d #t) (k 2)) (d (n "fxhash") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "jemallocator") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "1gw1gmgxwmgxyiy7qwvhv5xb6vh5wbp9nw0yg5dixi07ci45bgid") (f (quote (("safe-input" "fxhash"))))))

