(define-module (crates-io co rn corn) #:use-module (crates-io))

(define-public crate-corn-0.1.0 (c (n "corn") (v "0.1.0") (h "0q0v21ylsn6ja244qi98xcdy2kr237q1v4m96vvcqhygnx65kgjl")))

(define-public crate-corn-0.1.1 (c (n "corn") (v "0.1.1") (h "139j7fz94ic96bjfh09ijf0b17zm0hrdpi4a7xfjgk5z4cq97xzm")))

