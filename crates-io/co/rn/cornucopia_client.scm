(define-module (crates-io co rn cornucopia_client) #:use-module (crates-io))

(define-public crate-cornucopia_client-0.1.0 (c (n "cornucopia_client") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "deadpool-postgres") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.5") (d #t) (k 0)))) (h "1047rpp09yd6qx11xiw3y5gsaw0mkhnnppxmq29ns9y0zdhhvj2m")))

(define-public crate-cornucopia_client-0.2.0 (c (n "cornucopia_client") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "deadpool-postgres") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.5") (d #t) (k 0)))) (h "1v3wrl8wmhsppn628q8788gsj10fcffk56my1b0fxsv4zidpqxpa") (y #t)))

(define-public crate-cornucopia_client-0.2.1 (c (n "cornucopia_client") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "deadpool-postgres") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.6") (d #t) (k 0)))) (h "1ph02y51ywv3vqzkhf6jmw9q065hl6najmfkpdja0d5w42lnj9ds") (y #t)))

(define-public crate-cornucopia_client-0.2.2 (c (n "cornucopia_client") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "deadpool-postgres") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.6") (d #t) (k 0)))) (h "1y3s1m8hdi9d5xx654h8f0wph02pm723snsyc3hyqwyaysdq9vs8")))

(define-public crate-cornucopia_client-0.2.3 (c (n "cornucopia_client") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "deadpool-postgres") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.6") (d #t) (k 0)))) (h "1fvqwc92narbg4vfnbmsmc0z9b717crjbhk40c7r4j3wvq7j6fh0")))

