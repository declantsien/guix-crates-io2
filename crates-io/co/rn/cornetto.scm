(define-module (crates-io co rn cornetto) #:use-module (crates-io))

(define-public crate-cornetto-0.1.0 (c (n "cornetto") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0gy6dy132y3wlg5zlmsri18f96wia72rihj47w0xxnc982hc4irs")))

