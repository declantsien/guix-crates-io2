(define-module (crates-io co rn cornerstone) #:use-module (crates-io))

(define-public crate-cornerstone-0.1.0 (c (n "cornerstone") (v "0.1.0") (h "1idpf3b9f7m58krkd30wd8q3pd9ww96xqgg3vjngs0hqyhzbwff2")))

(define-public crate-cornerstone-0.1.1 (c (n "cornerstone") (v "0.1.1") (h "13mi2hdz1alfcqwzi6af7sa0vdirgnayvnhkzilvr4bl2dgklhph")))

(define-public crate-cornerstone-0.1.2 (c (n "cornerstone") (v "0.1.2") (h "0wc0f7amm57630bldv8ckzpbbm1wcjr5fcw06r5l72hwvzk6rs0d")))

(define-public crate-cornerstone-0.2.0 (c (n "cornerstone") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "19f3kkfrrpmmp9p3nj71ax1a5qcs5v1gqf8rxq30bdqnrbjvgl1i")))

(define-public crate-cornerstone-0.3.0 (c (n "cornerstone") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "10y20x4xd8ppma4ii192nma9g7gxb81v99an298g4hhz7y934vfb")))

(define-public crate-cornerstone-0.3.1 (c (n "cornerstone") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("fmt" "env-filter" "std" "registry"))) (d #t) (k 0)))) (h "1638r2xhk2sk7i7c3la57qx8r1fc9p6miglhbmsw09qnjir9i2lr")))

