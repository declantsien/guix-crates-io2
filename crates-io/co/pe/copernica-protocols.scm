(define-module (crates-io co pe copernica-protocols) #:use-module (crates-io))

(define-public crate-copernica-protocols-0.1.0 (c (n "copernica-protocols") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "copernica-common") (r "^0.1.0") (d #t) (k 0)) (d (n "copernica-packets") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "090c88nry55fn2nzz4nklcqjkrjbxlqvm1ffsszm3vbv6yz919an")))

