(define-module (crates-io co pe copernica-packets) #:use-module (crates-io))

(define-public crate-copernica-packets-0.1.0 (c (n "copernica-packets") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bech32") (r "^0.7.2") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "copernica-common") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "keynesis") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1xgxz8x3rwf580qfrrkpg3id8zvv5d0yscss8hj8wzms6wsqwcvi")))

