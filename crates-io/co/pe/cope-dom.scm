(define-module (crates-io co pe cope-dom) #:use-module (crates-io))

(define-public crate-cope-dom-0.0.1 (c (n "cope-dom") (v "0.0.1") (d (list (d (n "cope") (r "^0.0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.18") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.45") (f (quote ("Document" "Element" "HtmlButtonElement" "Text" "Window"))) (d #t) (k 0)))) (h "0jynjp81s9bn29lgpxnziwnlzp21j37hcz5fx482q9j57bganpwb") (f (quote (("strict"))))))

