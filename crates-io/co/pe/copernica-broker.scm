(define-module (crates-io co pe copernica-broker) #:use-module (crates-io))

(define-public crate-copernica-broker-0.1.0 (c (n "copernica-broker") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "copernica-common") (r "^0.1.0") (d #t) (k 0)) (d (n "copernica-packets") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "uluru") (r "^2.0.0") (d #t) (k 0)))) (h "1r18l2nkfan2f1pbrpqw4n25ayhqaajwspf9gyphdldn2gz63rsy")))

