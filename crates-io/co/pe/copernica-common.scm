(define-module (crates-io co pe copernica-common) #:use-module (crates-io))

(define-public crate-copernica-common-0.1.0 (c (n "copernica-common") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "fern") (r "^0.5.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1yj1ck15x0amkahrrbvpagdmv7pfsygbh1ma75lnxx96987acf6w")))

