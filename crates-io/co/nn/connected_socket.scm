(define-module (crates-io co nn connected_socket) #:use-module (crates-io))

(define-public crate-connected_socket-0.0.1 (c (n "connected_socket") (v "0.0.1") (h "0i6k8nvh4i95ya7mzjn4zs8ifs7x440zh8jm990033gxq6anfi2c")))

(define-public crate-connected_socket-0.0.2 (c (n "connected_socket") (v "0.0.2") (h "1yas7bgg571jnwrjah016py4zrq2283945z86crvzkqnviyxac6v")))

(define-public crate-connected_socket-0.0.3 (c (n "connected_socket") (v "0.0.3") (h "1kgxnsddvwjrf7zi86d4iksm3vv6d7k55xkf5bz26jwd5sq86hc9")))

(define-public crate-connected_socket-0.0.4 (c (n "connected_socket") (v "0.0.4") (h "1hb3c1kq4as6vybvlrw24r5990531vl1b6i4d7kwfqc1nv8ahnkm")))

(define-public crate-connected_socket-0.0.5 (c (n "connected_socket") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0xnkr1bigzx1a0s27mcjpdjcpshk8kvnxx1cfv4ygd5387r427q0")))

(define-public crate-connected_socket-0.0.6 (c (n "connected_socket") (v "0.0.6") (d (list (d (n "byteorder") (r "^0.3.9") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0y2vfx7q5jjn0pzbgyp76l53z21234wg497rbhzv8p3k87fqp93l")))

