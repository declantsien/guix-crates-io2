(define-module (crates-io co nn connect4-coms) #:use-module (crates-io))

(define-public crate-connect4-coms-0.1.0 (c (n "connect4-coms") (v "0.1.0") (d (list (d (n "connect4-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hs9bcym7b8mw811vb725pcwcx1x45si7lxyfmphx5rficsb4lbv")))

