(define-module (crates-io co nn connect_four) #:use-module (crates-io))

(define-public crate-connect_four-0.1.0 (c (n "connect_four") (v "0.1.0") (d (list (d (n "log-update") (r "^0.1.0") (d #t) (k 0)))) (h "0xqzflkyxb43xckgq4n6im9zzz4zmgxcwi81p7qaid0lq73rwr98")))

(define-public crate-connect_four-0.1.1 (c (n "connect_four") (v "0.1.1") (d (list (d (n "log-update") (r "^0.1.0") (d #t) (k 0)))) (h "04acvqpndpcirrm050bpzpnww2j6ypgxc80p5z38a28ivdjqxacv")))

(define-public crate-connect_four-0.1.2 (c (n "connect_four") (v "0.1.2") (d (list (d (n "log-update") (r "^0.1.0") (d #t) (k 0)))) (h "09s1sbw6amga1x5m5sgxfxxxvhrhfa1lri64f6yi1cn93iy0x2cg")))

(define-public crate-connect_four-0.1.3 (c (n "connect_four") (v "0.1.3") (d (list (d (n "log-update") (r "^0.1.0") (d #t) (k 0)))) (h "12a99zw23kr0d75cyjs9s3bya25b1gfvhf34nsa89z1bg50cif9s")))

(define-public crate-connect_four-0.1.4 (c (n "connect_four") (v "0.1.4") (d (list (d (n "log-update") (r "^0.1.0") (d #t) (k 0)))) (h "0l3wgpa2rw9gy5i9jbznc88wm1n2d6v89s7zbkys2n2drzb9q51b")))

(define-public crate-connect_four-0.1.5 (c (n "connect_four") (v "0.1.5") (d (list (d (n "log-update") (r "^0.1.0") (d #t) (k 0)))) (h "0nsi7zaihxnjvksf0cdq3p5p87lg2v11kn8700p2wfc387w13hjg")))

(define-public crate-connect_four-0.1.6 (c (n "connect_four") (v "0.1.6") (d (list (d (n "log-update") (r "^0.1.0") (d #t) (k 0)))) (h "0kf8jnysw2bac513ripa2zrgs09qlla59l4ribr6wm5hbk54npis")))

(define-public crate-connect_four-0.1.7 (c (n "connect_four") (v "0.1.7") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "05dnz39fqjylb3bz4y756w0lffpziynbc7lksxayfsd8z34kq4jr")))

(define-public crate-connect_four-0.1.8 (c (n "connect_four") (v "0.1.8") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1fhn75vfbn6yqmc0cclk4znfxym8n8biasq5g9pyg1n9a47iqimk")))

