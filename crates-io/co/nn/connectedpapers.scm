(define-module (crates-io co nn connectedpapers) #:use-module (crates-io))

(define-public crate-connectedpapers-0.1.0 (c (n "connectedpapers") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12srs2fsrfrfif5w1gp0gbikbwqaijcrpqgjjvdvqi81vp9r0f8s")))

