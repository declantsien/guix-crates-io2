(define-module (crates-io co nn connect4-server) #:use-module (crates-io))

(define-public crate-connect4-server-0.1.0 (c (n "connect4-server") (v "0.1.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "connect4-coms") (r "^0.1.0") (d #t) (k 0)) (d (n "connect4-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.1.0") (d #t) (k 0)) (d (n "mongodb") (r "^0.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.4") (k 0)) (d (n "rocket_contrib") (r "^0.4.4") (f (quote ("handlebars_templates" "tera_templates" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "04vq3fqapznhxy4wzax8y9dfbwp08w4hkflqi8ad7p58y2lnv6wc")))

