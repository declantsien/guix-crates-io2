(define-module (crates-io co nn connection-string-parser) #:use-module (crates-io))

(define-public crate-connection-string-parser-0.1.0 (c (n "connection-string-parser") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "01vmmd5qi8r933crn112csh32bdn2xry25d3jj33p7az3ywai95m")))

(define-public crate-connection-string-parser-0.2.0 (c (n "connection-string-parser") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0cja4d6g2knjw654cig08lyfzwyj1pgmyclbndb98x1ir0jc5jmn")))

(define-public crate-connection-string-parser-0.2.1 (c (n "connection-string-parser") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0zn0m8l86mlwzc017hsvj041ri9ix7c31aclbgf58g94w4zzl99z")))

(define-public crate-connection-string-parser-0.2.2 (c (n "connection-string-parser") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1w542p1v075araga8pf5iwhdlkb25i80nl7pphi1slnjxrqnzzzi")))

(define-public crate-connection-string-parser-0.2.3 (c (n "connection-string-parser") (v "0.2.3") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1mkzhfx7j9y2mbiblglrvcqpdj1fx7rymig63flv5pp4bnx04cv7")))

(define-public crate-connection-string-parser-0.2.4 (c (n "connection-string-parser") (v "0.2.4") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "19i0gvc4mlhn33ihm50vkfir9v7pl606pvh5gs0hpjh9slxfrr42")))

(define-public crate-connection-string-parser-0.2.5 (c (n "connection-string-parser") (v "0.2.5") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0cfiymy4yq1liyzdvfgcpals04pvw74z3cycgkqj7kb4rhwhng63")))

(define-public crate-connection-string-parser-0.3.0 (c (n "connection-string-parser") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "09f77zdzp8j6hclqjbxkcs10y3lcrf5wq6q3m1kq3hdck3lk16af")))

(define-public crate-connection-string-parser-0.3.1 (c (n "connection-string-parser") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0r3k8hba7wdn3bvxrx0vawsh0w5avn26lasdwjxh3vgxal10sgdi")))

(define-public crate-connection-string-parser-0.3.2 (c (n "connection-string-parser") (v "0.3.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0a2sinq2g25jx4f8a07jmxx0yxqfp43w9iiq1dqxlvvzrrcaxgz9")))

(define-public crate-connection-string-parser-0.3.3 (c (n "connection-string-parser") (v "0.3.3") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0blcnv8kxkvc4vdrsjpgr48ys1n19k6ak69iljfc2kswgaxql3ls")))

