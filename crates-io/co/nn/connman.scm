(define-module (crates-io co nn connman) #:use-module (crates-io))

(define-public crate-connman-0.1.1 (c (n "connman") (v "0.1.1") (d (list (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0rc8x9bc6zm817sla04xc6nxbz1d9qx8n2lgwbzinyk2anygbgh8")))

(define-public crate-connman-0.1.2 (c (n "connman") (v "0.1.2") (d (list (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0fdg3msv9jf9v76pzk648ydccbwsjjwbfqqybby0b55afl7dwrk4") (f (quote (("introspection" "xml-rs") ("default"))))))

(define-public crate-connman-0.1.3 (c (n "connman") (v "0.1.3") (d (list (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "dbus-tokio") (r "^0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1hqpkb9g1cm2dw2q8sc0mibzwzxmn9b93pklibnxb8fjs4g70h7z") (f (quote (("introspection" "xml-rs") ("default"))))))

