(define-module (crates-io co nn connect4-lib) #:use-module (crates-io))

(define-public crate-connect4-lib-0.1.0 (c (n "connect4-lib") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kv89fpjvys92ba2667bvi4gzzi8ibd48s39xqf760s9cpf7bi94")))

