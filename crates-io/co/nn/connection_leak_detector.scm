(define-module (crates-io co nn connection_leak_detector) #:use-module (crates-io))

(define-public crate-connection_leak_detector-0.1.0 (c (n "connection_leak_detector") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.12") (d #t) (k 2)) (d (n "enum-iterator") (r "^0.7") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "0k1x9zrginfb4q09pisalzyhhfagdc0cg47a7zasy1c1x54xwx39")))

(define-public crate-connection_leak_detector-0.1.1 (c (n "connection_leak_detector") (v "0.1.1") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.12") (d #t) (k 2)) (d (n "enum-iterator") (r "^0.7") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "15m0ig67rx6yqm5r5x6aa838qw5djq30yfmvlmvx9cqhfk4avkwa")))

(define-public crate-connection_leak_detector-0.1.2 (c (n "connection_leak_detector") (v "0.1.2") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.12") (d #t) (k 2)) (d (n "enum-iterator") (r "^0.7") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1axqaj03ws6k5wczysp2xwxak43vm4psiksw1q1w2l3xk51bxi4v")))

(define-public crate-connection_leak_detector-0.1.3 (c (n "connection_leak_detector") (v "0.1.3") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.12") (d #t) (k 2)) (d (n "enum-iterator") (r "^0.7") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1i098fb6s0c2ildl7v0c3vvg1qgn1al5i22jkfaaa2cnz71s6qba")))

(define-public crate-connection_leak_detector-0.1.4 (c (n "connection_leak_detector") (v "0.1.4") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.12") (d #t) (k 2)) (d (n "enum-iterator") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "190nnb9d8f2majh3hfs1fmqhkjn7rp8xi49lwb8s47qciy6ixl9n")))

(define-public crate-connection_leak_detector-0.1.5 (c (n "connection_leak_detector") (v "0.1.5") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.12") (d #t) (k 2)) (d (n "enum-iterator") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "0pp0kbh5k7fljzjw6qx6vpv102h63mlsnrwsj4cnr00ka2rwap7m")))

(define-public crate-connection_leak_detector-0.1.6 (c (n "connection_leak_detector") (v "0.1.6") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.12") (d #t) (k 2)) (d (n "enum-iterator") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "0s677x3c7200lna9d1k9q22v6irwrbk97yfw7rivimaajf7fwylv")))

