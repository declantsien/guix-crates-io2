(define-module (crates-io co nn connpass) #:use-module (crates-io))

(define-public crate-connpass-0.0.1 (c (n "connpass") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lv402zili8v12p24frls087sylhskzs55ywzd03xg3r9pi3bdpi")))

(define-public crate-connpass-0.1.0 (c (n "connpass") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qwr38m0g307vcs6air3d1y8wjkvdkc4l24nab0zdm3skisrd35n")))

