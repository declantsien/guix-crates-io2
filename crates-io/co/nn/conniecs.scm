(define-module (crates-io co nn conniecs) #:use-module (crates-io))

(define-public crate-conniecs-0.1.0 (c (n "conniecs") (v "0.1.0") (d (list (d (n "conniecs-derive") (r "^0.1.0") (d #t) (k 1)) (d (n "conniecs-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "index-pool") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)) (d (n "vec_map") (r "^0.8.0") (d #t) (k 0)))) (h "1ahh8f247l4n7xgxnbahmd6yhqhyr8fv48wdcvdh8j53wy2sibhj")))

(define-public crate-conniecs-0.1.1 (c (n "conniecs") (v "0.1.1") (d (list (d (n "conniecs-derive") (r "^0.1.1") (d #t) (k 1)) (d (n "conniecs-derive") (r "^0.1.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "index-pool") (r "^1.0.1") (d #t) (k 0)) (d (n "odds") (r "^0.2.25") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)) (d (n "vec_map") (r "^0.8.0") (d #t) (k 0)))) (h "1rw1sckwnv6l3d8h8a1qymq48rxzirmhagv4h6pxksc8ilffmqjk") (f (quote (("coroutines" "odds"))))))

(define-public crate-conniecs-0.1.2 (c (n "conniecs") (v "0.1.2") (d (list (d (n "conniecs-derive") (r "^0.1.2") (d #t) (k 1)) (d (n "conniecs-derive") (r "^0.1.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "index-pool") (r "^1.0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)) (d (n "vec_map") (r "^0.8.0") (d #t) (k 0)))) (h "0cyg2wa3y4z9khnqg6m7p6hprjjcqmca1fjvlizqyyczqpdplzgq")))

(define-public crate-conniecs-0.1.3 (c (n "conniecs") (v "0.1.3") (d (list (d (n "conniecs-derive") (r "^0.1.2") (d #t) (k 1)) (d (n "conniecs-derive") (r "^0.1.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "free-ranges") (r "^1.0") (d #t) (k 0)) (d (n "index-pool") (r "^1.0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)) (d (n "vec_map") (r "^0.8.0") (d #t) (k 0)))) (h "055i3wz10r252wxirfswapl6ccav7kdgvzi4mn855dilnhf2ysy6")))

(define-public crate-conniecs-0.1.4 (c (n "conniecs") (v "0.1.4") (d (list (d (n "conniecs-derive") (r "^0.1.2") (d #t) (k 1)) (d (n "conniecs-derive") (r "^0.1.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "free-ranges") (r "^1.0") (d #t) (k 0)) (d (n "index-pool") (r "^1.0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)) (d (n "vec_map") (r "^0.8.0") (d #t) (k 0)))) (h "05srf8y9n1w0h1ppw6kf70b6dvwzz5g1gwh64wvsp5ma4pmgiddp")))

(define-public crate-conniecs-0.2.0 (c (n "conniecs") (v "0.2.0") (d (list (d (n "conniecs-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "free-ranges") (r "^1.0") (d #t) (k 0)) (d (n "index-pool") (r "^1.0.5") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)) (d (n "vec_map") (r "^0.8.0") (d #t) (k 0)))) (h "0s56w1dmdr38xf56a7pi1ry5r42mj9d4898r65sj7rw36xkp2zw7")))

