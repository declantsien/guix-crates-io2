(define-module (crates-io co nn connection) #:use-module (crates-io))

(define-public crate-connection-0.1.0 (c (n "connection") (v "0.1.0") (h "1gpaya60s2v7v3ds4rhcf6ps1jqwfwz4jb10vilw58xzw6lnmqwz")))

(define-public crate-connection-0.2.0 (c (n "connection") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0hmg4pca30hfmb7x0kjlvlm9b2fzqjzax6wpcx9l3sqnq4k7llk3")))

(define-public crate-connection-0.2.1 (c (n "connection") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "160biq80qg97zz658wjmbl8znssw7m7gsr1x5dnwkjyaxicdk6sf")))

(define-public crate-connection-0.2.3 (c (n "connection") (v "0.2.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "11vspcbfjdd9qpx9pfhjrfhrj29k4kzkycb3mpd0l99jls1pfd6l")))

(define-public crate-connection-0.2.4 (c (n "connection") (v "0.2.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "156nngxcnk55p6bqrzsbz135jpf2b6isk0185jpxgbrij8qwyv8q")))

(define-public crate-connection-0.2.5 (c (n "connection") (v "0.2.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "14dm46i42k4cxjycl0sr8lh4j5jkyazxkrzclc8lrmpl7srfnvn5")))

