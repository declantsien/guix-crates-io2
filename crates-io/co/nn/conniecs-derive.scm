(define-module (crates-io co nn conniecs-derive) #:use-module (crates-io))

(define-public crate-conniecs-derive-0.1.0 (c (n "conniecs-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0k4r4n931cq6d9kw7racd4kxy4hfyx5ywi91rv8cdmcbw5g3ahwf")))

(define-public crate-conniecs-derive-0.1.1 (c (n "conniecs-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "06xjqx92yy3g2vfhhg67bc55a1y73pncnz51x5qpfwxiaml2dih6")))

(define-public crate-conniecs-derive-0.1.2 (c (n "conniecs-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1qm6bak0nwzc65d60hkwgh57d2chrkkxpxq5gcqv18qm79mvz1s7")))

(define-public crate-conniecs-derive-0.2.0 (c (n "conniecs-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1qmz1gfird8da9nbiqk7fxbzfjkd9mj0yq76m2g9603wbnyvq2cl")))

