(define-module (crates-io co nn connpass-rs) #:use-module (crates-io))

(define-public crate-connpass-rs-0.1.0 (c (n "connpass-rs") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.5") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fw5r918gkz78sx056raklwybrygz26a9rym3db1kwp4rg8ky1ws") (f (quote (("blocking" "reqwest/blocking"))))))

