(define-module (crates-io co nn connx) #:use-module (crates-io))

(define-public crate-connx-0.1.0 (c (n "connx") (v "0.1.0") (h "0li3j2s6bcm6z5ybk5c38f8qya5mvrj8yj291rn6blxnddyaxbqi")))

(define-public crate-connx-0.1.1 (c (n "connx") (v "0.1.1") (h "1wvzcg4f2w9rrgvbk8ip5bk5ijm2257dh34lfp206jyz1ahn400g")))

