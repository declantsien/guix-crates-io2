(define-module (crates-io co nn connect-4-bot) #:use-module (crates-io))

(define-public crate-connect-4-bot-0.1.0 (c (n "connect-4-bot") (v "0.1.0") (h "0p3cppfpz5gzvplinkxx58xs2b040avivwbsh5i4kcf6mgidinxx") (y #t)))

(define-public crate-connect-4-bot-1.0.0 (c (n "connect-4-bot") (v "1.0.0") (h "10i8wvhc1q0avmm5qvia85d7wsx4f9rdq1bq79vg5ly3hlpm0mgz") (y #t)))

