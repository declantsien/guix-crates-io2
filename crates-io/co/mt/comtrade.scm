(define-module (crates-io co mt comtrade) #:use-module (crates-io))

(define-public crate-comtrade-0.2.1 (c (n "comtrade") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0hcxsda1l5125ahjpksc6xr2dnvlf7lxh6fnpcp5hv9ms6l0xfdg")))

