(define-module (crates-io co mt comtesse) #:use-module (crates-io))

(define-public crate-comtesse-0.1.0 (c (n "comtesse") (v "0.1.0") (h "0hhy2474f2qrsrbchwmwzcdzkl337mj4viaq8bmkj50kc7ch3836")))

(define-public crate-comtesse-0.1.1 (c (n "comtesse") (v "0.1.1") (h "0dbmi64hj5c11cdich7y3y9dvpyl10l8rnmvrvnjjx0bhlm541k9")))

(define-public crate-comtesse-0.2.0 (c (n "comtesse") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "081yyziahs6m8b7rwgfwzgq3jw623gc1q9i116g8ylfxg779xxb9")))

(define-public crate-comtesse-0.2.1 (c (n "comtesse") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "038c6nykfx8l8987p1a9ayhsqfh2ddhj1vj5cs92j9yvjrwils8q")))

(define-public crate-comtesse-0.2.2 (c (n "comtesse") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "14gbgpvz5k70yyp8l6av1hq3n9mw05mrvn3kw2041g9y2i1jjg5y")))

(define-public crate-comtesse-0.2.3 (c (n "comtesse") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0y6sw982j7a2v4h0n9zdcxb1rmlwdgd4kj0x5apq197hxiwxgmlw")))

