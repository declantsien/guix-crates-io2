(define-module (crates-io co mt comtrade-rs) #:use-module (crates-io))

(define-public crate-comtrade-rs-0.2.0 (c (n "comtrade-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0pillkl98frjfm3srcaa5pb0cxjqb1awqgy33x3i88p37lz7lhqm") (y #t)))

