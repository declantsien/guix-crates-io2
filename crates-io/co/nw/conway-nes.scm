(define-module (crates-io co nw conway-nes) #:use-module (crates-io))

(define-public crate-conway-nes-0.1.0 (c (n "conway-nes") (v "0.1.0") (d (list (d (n "ines") (r "^0.1") (d #t) (k 0)) (d (n "mos6502_assembler") (r "^0.1") (d #t) (k 0)) (d (n "mos6502_model") (r "^0.1") (d #t) (k 0)))) (h "1nflg60r3sy7qi3gq4vmkik699a8v894pkfj59n0jpzfs935rrbp")))

(define-public crate-conway-nes-0.1.1 (c (n "conway-nes") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "ines") (r "^0.2") (d #t) (k 0)) (d (n "mos6502_assembler") (r "^0.2") (d #t) (k 0)) (d (n "mos6502_model") (r "^0.2") (d #t) (k 0)))) (h "16mpwhzn9gv5a7kidmm7m3l6x86cip2zxr1p066iqbjgarx7wx9l")))

