(define-module (crates-io co nw conway) #:use-module (crates-io))

(define-public crate-conway-0.1.2 (c (n "conway") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "09w3srk6rpnlpa3ab43a8d85q53fsqlsmd4jqm8591hicjxl27d8")))

(define-public crate-conway-0.1.3 (c (n "conway") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "05kgfbkhzm9jggvvzxxkyc50wn0wq4v05cvq7659qfh5990l9m6r")))

(define-public crate-conway-0.1.4 (c (n "conway") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "07zy0ly7wjhbgaakr7wqmnz0ggcy04qn2073r97wj5v8f353ssry")))

(define-public crate-conway-0.1.5 (c (n "conway") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "17611h3wjcpk59n887b87jww0cwnmaa1i1n9v29m2dx1qvyid6k0")))

(define-public crate-conway-0.2.0 (c (n "conway") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0k2j8xjy6jaavdmrqr7i8fymvajcm41gxy71z26x6136a2l5c2l7")))

(define-public crate-conway-0.3.0 (c (n "conway") (v "0.3.0") (d (list (d (n "custom_error") (r "^1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1rqcg9rcjja586q8fd1irxvsa72q5qarb1sy2s9n6vh4das97b1i")))

