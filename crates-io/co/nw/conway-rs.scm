(define-module (crates-io co nw conway-rs) #:use-module (crates-io))

(define-public crate-conway-rs-0.1.0 (c (n "conway-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0hszxx12jpa7xwgz30fq4rvdnnyxim27rb0kbjzh293z3xlfdksc")))

(define-public crate-conway-rs-0.1.1 (c (n "conway-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0lr21j599yxmq7z0c5lhwr2vn211xpw8agy8j8i9c4a47crvq6xs")))

(define-public crate-conway-rs-0.1.2 (c (n "conway-rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "13859qjgmbri98ky42g6qgambiwvq49423a6hsxkvhjvj6s9z9l8")))

