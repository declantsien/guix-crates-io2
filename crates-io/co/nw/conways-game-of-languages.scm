(define-module (crates-io co nw conways-game-of-languages) #:use-module (crates-io))

(define-public crate-conways-game-of-languages-0.1.0 (c (n "conways-game-of-languages") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p4bkfj5vrwpvr6q4dy3zpkk5r2864axs2vs2vdk1z3x4wkz3hrq")))

(define-public crate-conways-game-of-languages-0.2.0 (c (n "conways-game-of-languages") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0inh1rhzpmkkida5pm7s6r5zlm1fq356x0v9n1ysy5zi16n4pd7c")))

