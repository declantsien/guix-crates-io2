(define-module (crates-io co nc concise-scanf-like-input) #:use-module (crates-io))

(define-public crate-concise-scanf-like-input-0.1.0 (c (n "concise-scanf-like-input") (v "0.1.0") (h "1z6h83zmskb5wjrws260npwngb2k688p6r4zkxpsng2waj9a4rsf")))

(define-public crate-concise-scanf-like-input-0.1.1 (c (n "concise-scanf-like-input") (v "0.1.1") (h "0426xhwa5qk4bkpnk4s3fwy2kr3kb4m95n4yjh3wbrl7yivw17bf")))

