(define-module (crates-io co nc concmap) #:use-module (crates-io))

(define-public crate-concmap-0.1.0 (c (n "concmap") (v "0.1.0") (h "02riczn2sr8h3nr057knb5v335qjlaph9bcm3r7p3vdcfd11lydk") (y #t)))

(define-public crate-concmap-0.0.0 (c (n "concmap") (v "0.0.0") (h "1sixhzfhsj8a03v1y7m71plrnfaal5021kvxqhxdm5rz33mji77c")))

