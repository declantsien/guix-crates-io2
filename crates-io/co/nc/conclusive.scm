(define-module (crates-io co nc conclusive) #:use-module (crates-io))

(define-public crate-conclusive-0.1.0 (c (n "conclusive") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tui") (r "^0.14") (d #t) (k 0)))) (h "1bgggd12h1cvjfgsp7vd71l4dhsj9m2ghag1dck3rcqfn9jjxwqv")))

(define-public crate-conclusive-1.0.0 (c (n "conclusive") (v "1.0.0") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tui") (r "^0.18") (f (quote ("termion"))) (k 0)))) (h "1dx36m1n38vl3622f9z7i6scp807amy98aja83hx39jsji8cp46j")))

