(define-module (crates-io co nc concrete-npe) #:use-module (crates-io))

(define-public crate-concrete-npe-0.1.6 (c (n "concrete-npe") (v "0.1.6") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "03zskinqzbi3w7vs7rsbnp505gaskbzmdny25370jfamfy6g2qhy")))

(define-public crate-concrete-npe-0.1.7 (c (n "concrete-npe") (v "0.1.7") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "10624vab9sf685hk2lxdpdrv14y3v8c53nh9sc6h6bxxaszx66kh")))

(define-public crate-concrete-npe-0.1.8 (c (n "concrete-npe") (v "0.1.8") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "1wal5f7by123d99gh3p6fxyf9bnlc28nay33if3phyq88gwpv8ii")))

(define-public crate-concrete-npe-0.1.9 (c (n "concrete-npe") (v "0.1.9") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "02rcy5rawlxd3q5flglmn714h235r6gana7asygl3vdnvzzz04lq")))

(define-public crate-concrete-npe-0.2.0 (c (n "concrete-npe") (v "0.2.0") (d (list (d (n "concrete-commons") (r "=0.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0wbpbndkjyqdiblzivpd5bgjy4rs517f4sp5ny7fs15y7rqc1g4j")))

(define-public crate-concrete-npe-0.2.1 (c (n "concrete-npe") (v "0.2.1") (d (list (d (n "concrete-commons") (r "=0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0m5rzcp6fxc657d226ang55sp96axiw4abngpjwy7ja4wkfmjzyx")))

(define-public crate-concrete-npe-0.2.2 (c (n "concrete-npe") (v "0.2.2") (d (list (d (n "concrete-commons") (r "=0.2.1") (d #t) (k 0)))) (h "0kjfiak59pmnzk94l3i5hs64ag10lqgr4c1710i7sc5q3hm6np8v")))

(define-public crate-concrete-npe-0.3.0 (c (n "concrete-npe") (v "0.3.0") (d (list (d (n "concrete-core") (r "^1.0.0") (d #t) (k 0)))) (h "1qxfxpl3yp0705pi06fmhp7ppf1djx2pybim1m6b1qdc2y99wszi")))

