(define-module (crates-io co nc concat-kdf) #:use-module (crates-io))

(define-public crate-concat-kdf-0.1.0 (c (n "concat-kdf") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "digest") (r "^0.10.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10.1") (k 2)))) (h "05l8xj91phnjcdi1p2gnh1qqwfvfbx58im9d17i3pa164hjw2wid") (f (quote (("std"))))))

