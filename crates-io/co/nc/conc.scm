(define-module (crates-io co nc conc) #:use-module (crates-io))

(define-public crate-conc-0.1.2 (c (n "conc") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0cnx6kybhnhhimqd7cjj091gvs01ikkwpiksmgkbrck39sqi17jf")))

(define-public crate-conc-0.2.0 (c (n "conc") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "1pz38987p68cmff68c99289vgy62wcf8hdm769mcswr69vci8i17") (f (quote (("debug-tools" "backtrace"))))))

(define-public crate-conc-0.2.1 (c (n "conc") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "0pzm6qpmgbg1f7v1gj02jvmaxv5xwngsi6cblpw5xyfwbcl1zdz3") (f (quote (("debug-tools" "backtrace"))))))

(define-public crate-conc-0.2.3 (c (n "conc") (v "0.2.3") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "1j8w66d83wzx9djpxqz5p75a61wrzxw1jrwjfk3bp9nn23y97ldc") (f (quote (("debug-tools" "backtrace"))))))

(define-public crate-conc-0.3.0 (c (n "conc") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "05pyj3hnbqshahf8na3mk1nrihkqcxqsixsrjbbh3njxgmmb04ki") (f (quote (("debug-tools" "backtrace"))))))

(define-public crate-conc-0.4.0 (c (n "conc") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "0g8h3zbhc4jwg1817f798xpwxy3x8530qql01q53s49dl2irm8sh") (f (quote (("debug-tools" "backtrace"))))))

(define-public crate-conc-0.4.2 (c (n "conc") (v "0.4.2") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "1604d2fcb5apbgrfvpdkvnhscqkjnp0wr3hmkfbfc0z23sv2b521") (f (quote (("debug-tools" "backtrace"))))))

(define-public crate-conc-0.5.0 (c (n "conc") (v "0.5.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "1wfwqybbfjn03jnw81m3wkv0l9z1ykfs6hb7l20nkka9mzccm361") (f (quote (("debug-tools" "backtrace"))))))

