(define-module (crates-io co nc concat-string) #:use-module (crates-io))

(define-public crate-concat-string-1.0.0 (c (n "concat-string") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "17ds458x8a5a7rv1fdn2qk8hkz2h2advrirsipi21jqb6316pzm9")))

(define-public crate-concat-string-1.0.1 (c (n "concat-string") (v "1.0.1") (h "02c6hfxsvs1ff2j58f3qzr526w1yg8d2nf6yyjv81ixgbz5vwfbl")))

