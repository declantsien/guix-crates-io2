(define-module (crates-io co nc concordium-wasm) #:use-module (crates-io))

(define-public crate-concordium-wasm-1.0.0 (c (n "concordium-wasm") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "concordium-contracts-common") (r "^5") (f (quote ("derive-serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "1h79aaxwkp7y10sx7a84mc6vfxy70yz1h9hinlly2p0ha2zin022") (f (quote (("fuzz-coverage"))))))

(define-public crate-concordium-wasm-1.1.0 (c (n "concordium-wasm") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "concordium-contracts-common") (r "^6") (f (quote ("derive-serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "1nvlhih32carwa76hxkbgzy9ylphzp3pz3nxmdjpzm3z4lv2zmh6") (f (quote (("fuzz-coverage"))))))

(define-public crate-concordium-wasm-2.0.0 (c (n "concordium-wasm") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "concordium-contracts-common") (r "^7") (f (quote ("derive-serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)))) (h "0qwixiywfpfsjvab0g6bjisb7z7w94vjp4lk7qypmglgnvswbdh4") (f (quote (("fuzz-coverage"))))))

(define-public crate-concordium-wasm-3.0.0 (c (n "concordium-wasm") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "concordium-contracts-common") (r "^8") (f (quote ("derive-serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.6") (d #t) (k 0)))) (h "0hn45i2jpi20742w0z25dkncnggly7c7n85n5n3c98sl6a84fcaw") (f (quote (("fuzz-coverage"))))))

(define-public crate-concordium-wasm-4.0.0 (c (n "concordium-wasm") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "concordium-contracts-common") (r "^9") (f (quote ("derive-serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.6") (d #t) (k 0)))) (h "1mv7bk97c6ypkjcf83dfv7g7ga0x7kjd2pmgkqa3xcp1dbpq3996") (f (quote (("fuzz-coverage"))))))

