(define-module (crates-io co nc concurrency) #:use-module (crates-io))

(define-public crate-concurrency-0.1.1 (c (n "concurrency") (v "0.1.1") (h "0lqbar3qa9rgpamp3lcfs8bjhqbm574s9cgcvz2dfpgwzyrninax")))

(define-public crate-concurrency-0.1.2 (c (n "concurrency") (v "0.1.2") (h "0fq8l3ihfa5wn02hqkv38qjnlzdawphb424kpmvz0xar0rwdbxzh")))

(define-public crate-concurrency-0.1.3 (c (n "concurrency") (v "0.1.3") (h "15yxwyr10spx3qxd33baynj85ah013ax6qd91vfyrhkw0j51iw0g")))

(define-public crate-concurrency-4.2.0 (c (n "concurrency") (v "4.2.0") (h "090k0aac6swmgbg8qnr8x84in2xzi6rc644d32nqcdp4mpg7q8xs") (y #t)))

(define-public crate-concurrency-4.2.1 (c (n "concurrency") (v "4.2.1") (h "06cmgzwzqia1wwgkpssvr4pz4pshshhm2snfl2574pnkbkqqhr32") (y #t)))

(define-public crate-concurrency-4.20.1 (c (n "concurrency") (v "4.20.1") (h "0j1bm366k5b869papq6wwigqpgh3crnbsz0fn58d07mimaxby4b5")))

