(define-module (crates-io co nc concat_const) #:use-module (crates-io))

(define-public crate-concat_const-0.1.0 (c (n "concat_const") (v "0.1.0") (h "1acg10fxc2s1h7874v4akgjfpi6faqk29v71rvr6ixvxazr00n0s")))

(define-public crate-concat_const-0.1.1 (c (n "concat_const") (v "0.1.1") (h "1wsjd1cfzz28lnsmm13gb9jza83nq012zd10ayygwx4afwisw6iv") (y #t)))

(define-public crate-concat_const-0.1.2 (c (n "concat_const") (v "0.1.2") (h "1gnw2acq7dmi9j1afnbz4gbs1q538svwbklprpbx264hn9cif0iy")))

(define-public crate-concat_const-0.1.3 (c (n "concat_const") (v "0.1.3") (h "0hlcky6alywsx0ryfgph0y43sybg5nf9pdpjyk1iyxf2zyn2npqh")))

