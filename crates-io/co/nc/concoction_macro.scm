(define-module (crates-io co nc concoction_macro) #:use-module (crates-io))

(define-public crate-concoction_macro-0.1.0 (c (n "concoction_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "19b68lfjzccij7rk05h3cf041jdaza7nkqf2h9rivg6z9gp0d125")))

(define-public crate-concoction_macro-0.2.0 (c (n "concoction_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "0nzbb2syczhqqa6z4db606ldr3fhsir11bamchid3z4hkl5rwgn0")))

