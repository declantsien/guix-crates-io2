(define-module (crates-io co nc conclave-room-net) #:use-module (crates-io))

(define-public crate-conclave-room-net-0.0.1 (c (n "conclave-room-net") (v "0.0.1") (d (list (d (n "conclave-room") (r "^0.0.2-pre01") (d #t) (k 0)) (d (n "conclave-room-serialize") (r "^0.0.2-pre01") (d #t) (k 0)))) (h "1273sxbbhkq9hh7kaihz73zw8syi1xddc1s4rcwkia8s6yq71zhg")))

(define-public crate-conclave-room-net-0.0.2-pre01 (c (n "conclave-room-net") (v "0.0.2-pre01") (d (list (d (n "conclave-room") (r "^0.0.2-pre02") (d #t) (k 0)) (d (n "conclave-room-serialize") (r "^0.0.2-pre02") (d #t) (k 0)))) (h "14p85l23jzv9chahkmq4jrm60vc7skb221qip1azpciy4bsx30bi")))

