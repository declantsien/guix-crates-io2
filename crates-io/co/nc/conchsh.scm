(define-module (crates-io co nc conchsh) #:use-module (crates-io))

(define-public crate-conchsh-0.1.0 (c (n "conchsh") (v "0.1.0") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0kkkrmzy6k66jv90wm4d4w3phdf9y8dag4i2vv59w5292kd0jm6x") (f (quote (("build_deps" "man"))))))

(define-public crate-conchsh-0.1.1 (c (n "conchsh") (v "0.1.1") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1wsxhn4i6avw5l3qla2kiym080r0wizgfpw51p919bcgq87yzckn") (f (quote (("build_deps" "man"))))))

