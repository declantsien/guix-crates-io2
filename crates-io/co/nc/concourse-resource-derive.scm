(define-module (crates-io co nc concourse-resource-derive) #:use-module (crates-io))

(define-public crate-concourse-resource-derive-0.1.0 (c (n "concourse-resource-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1na1xlh5xpy5qajk89cy8bx741yvwjkdc69symrcwglb01rgc8vg")))

(define-public crate-concourse-resource-derive-0.2.0 (c (n "concourse-resource-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1028xv0jw1pkmnrjr9sc8fipxpmkxnvm84whyzg2sq524jb543jx")))

