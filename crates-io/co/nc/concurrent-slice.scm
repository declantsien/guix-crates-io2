(define-module (crates-io co nc concurrent-slice) #:use-module (crates-io))

(define-public crate-concurrent-slice-0.1.0 (c (n "concurrent-slice") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)))) (h "048h3p1awa0znjh8blr2z6sdzcgyg0c3qrinf1wcy0d7rd2sgvr6")))

