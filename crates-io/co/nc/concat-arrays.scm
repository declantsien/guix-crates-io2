(define-module (crates-io co nc concat-arrays) #:use-module (crates-io))

(define-public crate-concat-arrays-0.1.0 (c (n "concat-arrays") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1q4pxsnrgzd6l027y1b7n7594286dgrcqc48y5la9dsxdrvpppx6") (y #t)))

(define-public crate-concat-arrays-0.1.1 (c (n "concat-arrays") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "0m7y83lfsm2syds26ibmr0gcw6gnq4mhks2yvlj0jv3fn9r1w7yf") (y #t)))

(define-public crate-concat-arrays-0.1.2 (c (n "concat-arrays") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0irpvjsws7i40nx1ywxsmqw1bx3v4jq66x7vg95y70mk9s11bxqx")))

