(define-module (crates-io co nc conclave-serialize) #:use-module (crates-io))

(define-public crate-conclave-serialize-0.0.1 (c (n "conclave-serialize") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "conclave-room-serialize") (r "^0.0.2-pre02") (d #t) (k 0)) (d (n "conclave-types") (r "^0.0.1") (d #t) (k 0)))) (h "0nmka09p61nm2x6sx6f8q4agn81cb33rfi9vxqnxhwpyygznbnfl")))

