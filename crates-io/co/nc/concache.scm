(define-module (crates-io co nc concache) #:use-module (crates-io))

(define-public crate-concache-0.2.0 (c (n "concache") (v "0.2.0") (d (list (d (n "chashmap") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.20.3") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)) (d (n "zipf") (r "^4.0.0") (o #t) (d #t) (k 0)))) (h "1cm64qd42dfnad53y4cls2975pda2p9w6687gj1z4jwgcj41l59f") (f (quote (("bench" "clap" "zipf" "chashmap" "rand"))))))

(define-public crate-concache-0.2.1 (c (n "concache") (v "0.2.1") (d (list (d (n "chashmap") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.20.3") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)) (d (n "zipf") (r "^4.0.0") (o #t) (d #t) (k 0)))) (h "05m514lwvln6qnnn8iih1cr8pvfb4k7zxwy0pc0m92d9ab576if9") (f (quote (("bench" "clap" "zipf" "chashmap" "rand"))))))

