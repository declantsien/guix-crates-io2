(define-module (crates-io co nc concolor-override) #:use-module (crates-io))

(define-public crate-concolor-override-1.0.0 (c (n "concolor-override") (v "1.0.0") (h "0zq3qvpb5ly4r1vqpw4p6lksmhf0qa2d613aafq2zicdjyhx8md8") (r "1.60.0")))

(define-public crate-concolor-override-1.1.0 (c (n "concolor-override") (v "1.1.0") (d (list (d (n "colorchoice") (r "^1.0.0") (d #t) (k 0)))) (h "140yb63zrbdvxvy3zaqvldgrjh5rm9qijy14w0imk7g3i1995920") (r "1.64.0")))

