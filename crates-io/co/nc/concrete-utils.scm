(define-module (crates-io co nc concrete-utils) #:use-module (crates-io))

(define-public crate-concrete-utils-0.1.0 (c (n "concrete-utils") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "0dlz19n56anq5fjc4dgq43ryviwcdzqal1rpkfl64xaixy33q8cv")))

(define-public crate-concrete-utils-0.2.0 (c (n "concrete-utils") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1cfvksmyin01qplgj76mwkmzhl0hf50565j03b9apgjwkpbqivsm")))

