(define-module (crates-io co nc concoct-web) #:use-module (crates-io))

(define-public crate-concoct-web-0.1.0-alpha.1 (c (n "concoct-web") (v "0.1.0-alpha.1") (d (list (d (n "concoct") (r "^0.17.0-alpha.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.66") (f (quote ("Document" "Event" "HtmlElement" "HtmlCollection" "Text" "Window"))) (d #t) (k 0)))) (h "1sdp7xxzw84xjzhniigv3719zy4vj5b6xd23c7j3xg33f6npapb5")))

