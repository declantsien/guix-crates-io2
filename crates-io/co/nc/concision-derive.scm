(define-module (crates-io co nc concision-derive) #:use-module (crates-io))

(define-public crate-concision-derive-0.1.6 (c (n "concision-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0b5b0bf9wd7ilsgwl4yml36sqpcjj399l7akrq7q425pbj8y4m6d") (f (quote (("default"))))))

(define-public crate-concision-derive-0.1.7 (c (n "concision-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "12szh0lbwnrns03fqg41sjmcjq8rjjrm6r2j00mvigrazizdj078") (f (quote (("default"))))))

(define-public crate-concision-derive-0.1.8 (c (n "concision-derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "182256h5kkxpqm032wly0vgarv64s1x6j2q2c8b0farv1r7lk5vc") (f (quote (("default"))))))

(define-public crate-concision-derive-0.1.9 (c (n "concision-derive") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0b49lxpdjjbg0r57dlxj47yd02825rn1w1xd7kh9ryxgh7vww67y")))

(define-public crate-concision-derive-0.1.10 (c (n "concision-derive") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0jz97g57j6nm8sd5ll89854h4g2vsk0lldzgyxvrlqqb6viak4r0")))

(define-public crate-concision-derive-0.1.11 (c (n "concision-derive") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0i4jihb8qxf6kfl5bx5j337z5x4rm5x4wjfs3jrip6hf1iagyjbf")))

(define-public crate-concision-derive-0.1.12 (c (n "concision-derive") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "103m3y1i2icjjf6ykmq58kdqjzn8vyjgwrarv0jyg1d7qfzd3wyb") (f (quote (("default"))))))

(define-public crate-concision-derive-0.1.13 (c (n "concision-derive") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1j8fnb7i3ljjkmhk2yzn66mnbz6gwf6rbxjpbx345vm1y9zr8a13") (f (quote (("default"))))))

(define-public crate-concision-derive-0.1.14 (c (n "concision-derive") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1jxkf2xdqmk6hsncyj9yzb8s83fkhg4bq4wgzaa87q6xfjafqj0j") (f (quote (("wasm") ("wasi") ("std") ("default" "std") ("alloc"))))))

