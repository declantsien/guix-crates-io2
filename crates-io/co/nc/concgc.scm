(define-module (crates-io co nc concgc) #:use-module (crates-io))

(define-public crate-concgc-0.1.0 (c (n "concgc") (v "0.1.0") (h "16ps6jbm571qa02avnwqp1c2caqxhdavpf0f1m720wkg9q9qz6k9")))

(define-public crate-concgc-0.1.1 (c (n "concgc") (v "0.1.1") (d (list (d (n "derive_deref") (r "^1.1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "0qyqsklbisgbjagfmzwd2lnrrsiipy52v6dlq9v1xzsly5dp00y2")))

