(define-module (crates-io co nc concurrent-biproxy) #:use-module (crates-io))

(define-public crate-concurrent-biproxy-0.1.0 (c (n "concurrent-biproxy") (v "0.1.0") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "logs") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14r41vgmli6f6pnj60zfdf3134a49cijxlgdm2wrm78pk2aickgv")))

