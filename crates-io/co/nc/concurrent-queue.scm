(define-module (crates-io co nc concurrent-queue) #:use-module (crates-io))

(define-public crate-concurrent-queue-0.0.1 (c (n "concurrent-queue") (v "0.0.1") (h "1ll9q3sc5ad1i45jf2z4ijvpcw0rpbv6qlag9kfgp31y0ry63lsr")))

(define-public crate-concurrent-queue-1.0.0 (c (n "concurrent-queue") (v "1.0.0") (d (list (d (n "cache-padded") (r "^1.0.0") (d #t) (k 0)) (d (n "easy-parallel") (r "^2.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.0.0") (d #t) (k 2)))) (h "01814yyawaadj0gwd2sb9a2k4wl1yly2rnikjgchsnxisvc0mp0m")))

(define-public crate-concurrent-queue-1.1.0 (c (n "concurrent-queue") (v "1.1.0") (d (list (d (n "cache-padded") (r "^1.0.0") (d #t) (k 0)) (d (n "easy-parallel") (r "^2.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.0.0") (d #t) (k 2)))) (h "1v76yglcahm0mm7x6dhxwxfdfjanxp61998n1mapf41d7wq5x3lx")))

(define-public crate-concurrent-queue-1.1.1 (c (n "concurrent-queue") (v "1.1.1") (d (list (d (n "cache-padded") (r "^1.0.0") (d #t) (k 0)) (d (n "easy-parallel") (r "^2.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.0.0") (d #t) (k 2)))) (h "1060jsmr1p5nw1sb9jhx8wa6z1zprgrrsdvwp2g8jb8zysphcg7q")))

(define-public crate-concurrent-queue-1.1.2 (c (n "concurrent-queue") (v "1.1.2") (d (list (d (n "cache-padded") (r "^1.0.0") (d #t) (k 0)) (d (n "easy-parallel") (r "^2.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.0.0") (d #t) (k 2)))) (h "1m461xj5b7jcl8gw0z7f2dpv0z818rk27g1h5hiyz5sdnydi70hm")))

(define-public crate-concurrent-queue-1.2.0 (c (n "concurrent-queue") (v "1.2.0") (d (list (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.3.3") (d #t) (k 2)))) (h "1qbq1s4vnvrnlaqbqry7q3vqdy8qagq43nnf1b3llc2lh5y435p2")))

(define-public crate-concurrent-queue-1.2.1 (c (n "concurrent-queue") (v "1.2.1") (d (list (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.3.3") (d #t) (k 2)))) (h "1l1w6cjgwnh4gyfyqi3mq4b75500nd2jlss4p62akgifk5k9rzrr")))

(define-public crate-concurrent-queue-1.2.2 (c (n "concurrent-queue") (v "1.2.2") (d (list (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.3.3") (d #t) (k 2)))) (h "18w6hblcjjk9d0my3657ra1zdj79gwfjmzvc0b3985g01dahgv9h")))

(define-public crate-concurrent-queue-1.2.3 (c (n "concurrent-queue") (v "1.2.3") (d (list (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.3.3") (d #t) (k 2)))) (h "0mw18z2n5z118qs7fw7yizfjfxcrg8gb7vf2fd7zlwiccf9pg0l3") (y #t) (r "1.38")))

(define-public crate-concurrent-queue-1.2.4 (c (n "concurrent-queue") (v "1.2.4") (d (list (d (n "cache-padded") (r "^1.1.1") (d #t) (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.3.3") (d #t) (k 2)))) (h "176v15an6f686c9m5br57al23d7z3xzm3542walnwsdm9aj80ixg") (r "1.38")))

(define-public crate-concurrent-queue-2.0.0 (c (n "concurrent-queue") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.11") (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.3.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(loom)") (k 0)) (d (n "portable-atomic") (r "^0.3") (o #t) (k 0)))) (h "02rrliwnayijc47m7mrdww9fv9z26l8d9bp7wh8cdqw6vilyyyxx") (f (quote (("std") ("default" "std")))) (r "1.38")))

(define-public crate-concurrent-queue-2.1.0 (c (n "concurrent-queue") (v "2.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.11") (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.3.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(loom)") (k 0)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)))) (h "13iy6879mpipcrwvn1vc62nimsqvbvag8kdig01bg0qphfdq6y62") (f (quote (("std") ("default" "std")))) (r "1.38")))

(define-public crate-concurrent-queue-2.2.0 (c (n "concurrent-queue") (v "2.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.11") (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.3.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (o #t) (d #t) (t "cfg(loom)") (k 0)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)))) (h "0z0bnpgcblhrms6gph7x78yplj3qmlr5mvl38v9641zsxiqngv32") (f (quote (("std") ("default" "std")))) (r "1.38")))

(define-public crate-concurrent-queue-2.3.0 (c (n "concurrent-queue") (v "2.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.11") (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "loom") (r "^0.7") (o #t) (d #t) (t "cfg(loom)") (k 0)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)))) (h "006lfgl3hn38pxgkafsrymkxalmvhlb8m5dh9583c4jglnaacmzh") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-concurrent-queue-2.4.0 (c (n "concurrent-queue") (v "2.4.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.11") (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "loom") (r "^0.7") (o #t) (d #t) (t "cfg(loom)") (k 0)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)))) (h "0qvk23ynj311adb4z7v89wk3bs65blps4n24q8rgl23vjk6lhq6i") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-concurrent-queue-2.5.0 (c (n "concurrent-queue") (v "2.5.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("cargo_bench_support"))) (k 2)) (d (n "crossbeam-utils") (r "^0.8.11") (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "loom") (r "^0.7") (o #t) (d #t) (t "cfg(loom)") (k 0)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_family = \"wasm\")") (k 2)))) (h "0wrr3mzq2ijdkxwndhf79k952cp4zkz35ray8hvsxl96xrx1k82c") (f (quote (("std") ("default" "std")))) (r "1.60")))

