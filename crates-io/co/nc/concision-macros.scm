(define-module (crates-io co nc concision-macros) #:use-module (crates-io))

(define-public crate-concision-macros-0.1.6 (c (n "concision-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0bf0zdzbm0xj17i91zfh9xrpaapa6c0maikjlqdsa55zylc4p1l7") (f (quote (("default"))))))

(define-public crate-concision-macros-0.1.7 (c (n "concision-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0g6b123gmmjns1zqpalhfwdlsf8i8r3mnvlrhbxl25zb7mz1p7c5") (f (quote (("default"))))))

(define-public crate-concision-macros-0.1.8 (c (n "concision-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "03di4jha1p9yc7pwv907m50a3hrk8cm09379g5xkipsjc81xkcyk") (f (quote (("default"))))))

(define-public crate-concision-macros-0.1.9 (c (n "concision-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1vzqbylkm376rqzzw549f8ssfqll147whkyyiq5ri5fhx0wlvkf8")))

(define-public crate-concision-macros-0.1.10 (c (n "concision-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1ha7yzj4wxi1c0fghm0drwcvay577lbq0rjkyqg43i0nka0wgnkl")))

(define-public crate-concision-macros-0.1.11 (c (n "concision-macros") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0m49g81vj0943pfp3mxbl34lmrhpc6999923xab6b46l8nak9hv4")))

(define-public crate-concision-macros-0.1.12 (c (n "concision-macros") (v "0.1.12") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0n4avjvxsb7y9z6rqnq8ggkb2hihg2ha3nnds2mp7l0f2imf24ns") (f (quote (("default"))))))

(define-public crate-concision-macros-0.1.13 (c (n "concision-macros") (v "0.1.13") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "15s9ia57p0l2fk2x7ldywj9cx16g3npaxpqwzqplzs66qbp2hn06") (f (quote (("default"))))))

(define-public crate-concision-macros-0.1.14 (c (n "concision-macros") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1knrhngyafh8c40gwivyy1v6i4ra7w7ibbxbc3jmgl84lbgi2x2x") (f (quote (("wasm") ("wasi") ("std") ("default" "std") ("alloc"))))))

