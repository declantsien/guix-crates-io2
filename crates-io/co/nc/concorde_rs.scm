(define-module (crates-io co nc concorde_rs) #:use-module (crates-io))

(define-public crate-concorde_rs-0.1.0 (c (n "concorde_rs") (v "0.1.0") (h "0kcnc4cn9x8q7jpgifvbgaprf95z7vncd8561zp6z164vrcr72iz") (y #t)))

(define-public crate-concorde_rs-0.1.1 (c (n "concorde_rs") (v "0.1.1") (h "0n6x1b7k1x4hhbcb9yczl5awaq0k16w8xzlnwy036jghf4s8zsja")))

