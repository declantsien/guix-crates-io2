(define-module (crates-io co nc concat-util) #:use-module (crates-io))

(define-public crate-concat-util-0.1.0 (c (n "concat-util") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0hidh2b8kpmlvlvlnhniancwzhg4q9ci3vsyasvq8vjkdfyg2zn1")))

(define-public crate-concat-util-0.2.0 (c (n "concat-util") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0nbqlhz0146f8j3dzcdlchzblgz3x7ipibp5qkvavf56gg5c5k35")))

