(define-module (crates-io co nc concordium-cis2) #:use-module (crates-io))

(define-public crate-concordium-cis2-1.0.0 (c (n "concordium-cis2") (v "1.0.0") (d (list (d (n "concordium-std") (r "^3.1") (k 0)))) (h "1b1jrikkj4i3ws6gfksdqkw71acb8cs230721pldx3f1sgb2wybw") (f (quote (("std" "concordium-std/std") ("default" "std"))))))

(define-public crate-concordium-cis2-1.1.0 (c (n "concordium-cis2") (v "1.1.0") (d (list (d (n "concordium-std") (r "^4") (k 0)))) (h "0bakk75y0s2cmyg3w4p1g3v41n0sgz4wi8y0pz6g6nljflbann1r") (f (quote (("std" "concordium-std/std") ("default" "std"))))))

(define-public crate-concordium-cis2-1.2.0 (c (n "concordium-cis2") (v "1.2.0") (d (list (d (n "concordium-std") (r "^4") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)))) (h "054k4jw74hsgaf7mv7vgw5h51v3027cacp9s39rp9pfnavxn6v9w") (f (quote (("u256_amount") ("std" "concordium-std/std") ("default" "std"))))))

(define-public crate-concordium-cis2-2.0.0 (c (n "concordium-cis2") (v "2.0.0") (d (list (d (n "concordium-std") (r "^5") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)))) (h "1gahis4i21h76i56ylrqarx99zh0zaa4lm0ym3jg91bnjkd6b5f7") (f (quote (("u256_amount") ("std" "concordium-std/std") ("default" "std"))))))

(define-public crate-concordium-cis2-3.0.0 (c (n "concordium-cis2") (v "3.0.0") (d (list (d (n "concordium-std") (r "^6") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)))) (h "0hgc6fj6scwn4bw9i25la4j2crpcdx9b24h73jkzld9nhgqy1mm2") (f (quote (("u256_amount") ("std" "concordium-std/std") ("default" "std"))))))

(define-public crate-concordium-cis2-3.1.0 (c (n "concordium-cis2") (v "3.1.0") (d (list (d (n "concordium-std") (r "^6.2") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)))) (h "1wfnkwl1b2kdipaqdb333s5z3jk0hw65l6himgca71avvmd29yw0") (f (quote (("u256_amount") ("std" "concordium-std/std") ("default" "std"))))))

(define-public crate-concordium-cis2-4.0.0 (c (n "concordium-cis2") (v "4.0.0") (d (list (d (n "concordium-std") (r "^7.0") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)))) (h "0jaspaj93y0zghjymaxybxwf6934rakabr4wbvbcbxpm15kym2pf") (f (quote (("u256_amount") ("std" "concordium-std/std") ("default" "std"))))))

(define-public crate-concordium-cis2-5.0.0 (c (n "concordium-cis2") (v "5.0.0") (d (list (d (n "concordium-std") (r "^8.0") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)))) (h "00bbvb28way5fp7m42sf84y5mbskdakbimvh45ry4hmgsawvmpnx") (f (quote (("u256_amount") ("std" "concordium-std/std") ("default" "std"))))))

(define-public crate-concordium-cis2-5.1.0 (c (n "concordium-cis2") (v "5.1.0") (d (list (d (n "concordium-std") (r "^8.0") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)))) (h "1h6q8pxf93c2w4wkbm6vdg03j02y4d7lm54ycv1g4nrfbqv5dif6") (f (quote (("u256_amount") ("std" "concordium-std/std") ("default" "std"))))))

(define-public crate-concordium-cis2-6.0.0 (c (n "concordium-cis2") (v "6.0.0") (d (list (d (n "concordium-std") (r "^9.0") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)))) (h "1m7lldj8j8lgqvgmwckarzcdfydskn8h8b0vrbyg3c13smffwq8q") (f (quote (("u256_amount") ("std" "concordium-std/std") ("default" "std"))))))

(define-public crate-concordium-cis2-6.1.0 (c (n "concordium-cis2") (v "6.1.0") (d (list (d (n "concordium-std") (r "^10.0") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)))) (h "0yb3q7zxrw3k5rbhny66gbswdmncjrhl4hrzg84qm375fw12487r") (f (quote (("u256_amount") ("std" "concordium-std/std") ("default" "std"))))))

