(define-module (crates-io co nc conclave-room-serialize) #:use-module (crates-io))

(define-public crate-conclave-room-serialize-0.0.1 (c (n "conclave-room-serialize") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "conclave-room") (r "^0.0.1") (d #t) (k 0)))) (h "00cxfvfp7krz6l3klwgx23ydk9qbvp70ff5rgqlxzj494ydmmil2")))

(define-public crate-conclave-room-serialize-0.0.2-pre01 (c (n "conclave-room-serialize") (v "0.0.2-pre01") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "conclave-room") (r "^0.0.2-pre01") (d #t) (k 0)))) (h "0djfs7w4nwwrzhir6k83091pnygv0rqdblk29cbry1s7l7hwf55c")))

(define-public crate-conclave-room-serialize-0.0.2-pre02 (c (n "conclave-room-serialize") (v "0.0.2-pre02") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "conclave-room") (r "^0.0.2-pre02") (d #t) (k 0)))) (h "1f3kp16lk4inmv5j0xf6mmvfvjnfggchjkkbna183k5xcs690079")))

(define-public crate-conclave-room-serialize-0.0.2-pre03 (c (n "conclave-room-serialize") (v "0.0.2-pre03") (d (list (d (n "conclave-room") (r "^0.0.2-pre02") (d #t) (k 0)) (d (n "flood-rs") (r "^0.0.3") (d #t) (k 0)))) (h "0qli48v174b1s4xhag95w2lg85x722vkp4452m2npfv8g9wkc8s9")))

