(define-module (crates-io co nc concordium-std-derive) #:use-module (crates-io))

(define-public crate-concordium-std-derive-0.1.0 (c (n "concordium-std-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h69pj2yp3dpfiasghkswqk7piqfqaszkj1yrl7fsxriw8ilpxdd") (y #t)))

(define-public crate-concordium-std-derive-0.2.0 (c (n "concordium-std-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06m1fv52lfbsi8i4dz2nnkfhf9vvn7c0sxrjbf04bl2lcimgbccy") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-0.3.0 (c (n "concordium-std-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jn2divlqijwnf32mdj1xi1r67lsijga8dq4zwayv43kfs667ygb") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-0.4.0 (c (n "concordium-std-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ldqdj0slm82a0a9fj2piwwprf1ckcrw9lk63361x8gsll06m8d4") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-0.5.0 (c (n "concordium-std-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yz7kyn3ag2z18jn7k1pzwjxx791svw3gd1pgsc5nkh3vlnb2wwn") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-1.0.0 (c (n "concordium-std-derive") (v "1.0.0") (d (list (d (n "concordium-contracts-common") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bxah5rawsxv0qlb9a9xvpgrd7s5jangqdfi78xvn4ljya2vsl91") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-2.0.0 (c (n "concordium-std-derive") (v "2.0.0") (d (list (d (n "concordium-contracts-common") (r "^2.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03gd1sgqprqc5mrs6gmjq0j6nlganna9l24sk1fs49gh9aj7fwc9") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-3.0.0 (c (n "concordium-std-derive") (v "3.0.0") (d (list (d (n "concordium-contracts-common") (r "^3.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i3dwxk45d2i2mc3z2x35cds9p4p0z1g0q9y1m3g3d5qvkc45bpm") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-3.1.0 (c (n "concordium-std-derive") (v "3.1.0") (d (list (d (n "concordium-contracts-common") (r "^3.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xjsz2knz5fwn3pz6zij8q7s7bjydzr0pqjbbmi8sddnz35ygzvz") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-4.0.0 (c (n "concordium-std-derive") (v "4.0.0") (d (list (d (n "concordium-contracts-common") (r "^4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "070rfj8dzscn81mf2aww6771n4gzymvpwjxy3ilwcxl2j6lnqfxa") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-4.1.0 (c (n "concordium-std-derive") (v "4.1.0") (d (list (d (n "concordium-contracts-common") (r "^4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kxindljvkhxvc12wzicga6srzg1dmq2726ikrm6pxmnxaap4m3n") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-5.0.0 (c (n "concordium-std-derive") (v "5.0.0") (d (list (d (n "concordium-contracts-common") (r "^5") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vr2f8lrw06sz9g1p2wznrrhr0xlr2dmv8843hm29jq2n1v3wxa2") (f (quote (("wasm-test") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-5.1.0 (c (n "concordium-std-derive") (v "5.1.0") (d (list (d (n "concordium-contracts-common") (r "^5") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nsdpzr6j131m03wm8idz0va98n5jsp0byjk1qa71kcdvj2wz38a") (f (quote (("wasm-test") ("concordium-quickcheck") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-5.2.0 (c (n "concordium-std-derive") (v "5.2.0") (d (list (d (n "concordium-contracts-common") (r "^6.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c90h18nzscgw06c7lb5kq8ksf8xxqn4iq7l6rckazb6zk7ircqg") (f (quote (("wasm-test") ("concordium-quickcheck") ("build-schema")))) (y #t)))

(define-public crate-concordium-std-derive-6.0.0 (c (n "concordium-std-derive") (v "6.0.0") (d (list (d (n "concordium-contracts-common") (r "^9") (f (quote ("derive-serde"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qmwrr4w411skhkrfz3c95ljsfnpiyrajjvz76nf59ga484f9g1l") (r "1.73")))

