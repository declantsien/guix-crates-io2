(define-module (crates-io co nc concat-idents) #:use-module (crates-io))

(define-public crate-concat-idents-1.0.0 (c (n "concat-idents") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.30") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.31") (d #t) (k 2)))) (h "0npnaizsklnzc88vnclx6viaz9nwzccmzg1093vr34gwz9bbk86m")))

(define-public crate-concat-idents-1.1.0 (c (n "concat-idents") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.38") (d #t) (k 2)))) (h "18y0gvzi9pyzs7jp7mlfvss2gjy5qv95xv9jr6niq0sjz2myawhm")))

(define-public crate-concat-idents-1.1.1 (c (n "concat-idents") (v "1.1.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.38") (d #t) (k 2)))) (h "18hbp9shidxvbl4s9y6a456fy74qvvlnf7wnay9vlj9mn7nqvmvw")))

(define-public crate-concat-idents-1.1.2 (c (n "concat-idents") (v "1.1.2") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.38") (d #t) (k 2)))) (h "0v7plp71q1khfcpb4xhnldz127cdpfyivm5w33dq06nrwvzbla9z")))

(define-public crate-concat-idents-1.1.3 (c (n "concat-idents") (v "1.1.3") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0bqdl6vml2ksyz6hc4xlpw3iqaagxs7xn0xinwa51ms80a390vsb")))

(define-public crate-concat-idents-1.1.4 (c (n "concat-idents") (v "1.1.4") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "0cp80xzafssrvfwg8v3l7y0qggmcbxmlk9whwlc7v2fyyzcy3q0g")))

(define-public crate-concat-idents-1.1.5 (c (n "concat-idents") (v "1.1.5") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "0k8d0cww6d0p8qllp420mxqd3aislq50dbfhv4vxhrr23y8r0sgp")))

