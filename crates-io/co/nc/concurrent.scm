(define-module (crates-io co nc concurrent) #:use-module (crates-io))

(define-public crate-concurrent-0.1.0 (c (n "concurrent") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1pvfls6ri0hb9s3pqc8z7h5fljfv7bymi4i44abygkz8wbl37i9d")))

(define-public crate-concurrent-0.1.1 (c (n "concurrent") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0dggb9xq9qpsl5czrgdsh524h1a5pdnj2nm5kkpadqjnrxsybx49")))

(define-public crate-concurrent-0.1.2 (c (n "concurrent") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0afs87cs6a78dl5g4y09hz2rhnj15c5gdpcg0dgawj8236rk0c15")))

(define-public crate-concurrent-0.2.1 (c (n "concurrent") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "13sfgqa4gplwvhpcp13wkm87kb37xhw1n2jdx8d4j59dz91fdidw") (f (quote (("debug-tools" "backtrace"))))))

(define-public crate-concurrent-0.2.2 (c (n "concurrent") (v "0.2.2") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "spin") (r "^0.4") (d #t) (k 0)))) (h "1lgkrdvv3v8bkanas7sgq0m4fm1fgwqxar59d6j4yjja9411rzdc") (f (quote (("debug-tools" "backtrace"))))))

