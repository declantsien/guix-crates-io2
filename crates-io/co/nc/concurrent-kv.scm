(define-module (crates-io co nc concurrent-kv) #:use-module (crates-io))

(define-public crate-concurrent-kv-0.2.0 (c (n "concurrent-kv") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)))) (h "0i0b5y114b1h3xvpm8fcf68l7sx1dh9hbp46l00w4q0q7cxhv4n6")))

(define-public crate-concurrent-kv-0.2.1 (c (n "concurrent-kv") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)))) (h "0y5hhkj3vm4bi5ag5g60z7vbpvglhy9hlbnwyxynjfxbvcgfv43r")))

