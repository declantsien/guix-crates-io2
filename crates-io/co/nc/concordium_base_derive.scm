(define-module (crates-io co nc concordium_base_derive) #:use-module (crates-io))

(define-public crate-concordium_base_derive-1.0.0 (c (n "concordium_base_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1v8bclwzb2j933fzmhxss37qb1kvhkgh7ilhz3lx6n2dlvlm98dr")))

