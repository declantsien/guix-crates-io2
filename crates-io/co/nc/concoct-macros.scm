(define-module (crates-io co nc concoct-macros) #:use-module (crates-io))

(define-public crate-concoct-macros-0.1.0 (c (n "concoct-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "055q09jx243h5pwl2iyzbzqshm9ifzd9ij1m53m4891xi7p8dz36")))

(define-public crate-concoct-macros-0.1.0-alpha.1 (c (n "concoct-macros") (v "0.1.0-alpha.1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1w6fg9y9mlrcrx2272v0b13gjbmxc4q2alrc2d3pl0w56v34b40n")))

(define-public crate-concoct-macros-0.2.0-alpha.1 (c (n "concoct-macros") (v "0.2.0-alpha.1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0im6srzq4wk58hkmyzn3k4f9miczjfj5kvjj1nd2dwg176zb9i4f")))

(define-public crate-concoct-macros-0.2.0-alpha.2 (c (n "concoct-macros") (v "0.2.0-alpha.2") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0shh3566mlfi87b991vpp4gjgbnvz2qjkgsbxb6dr8y77g6gg299")))

(define-public crate-concoct-macros-0.2.0-alpha.3 (c (n "concoct-macros") (v "0.2.0-alpha.3") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "074q86v0pff02ybnf09wsbwsvmpvwmz80cwlnznsyr1wg1hzx9d9")))

(define-public crate-concoct-macros-0.2.0 (c (n "concoct-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0fc90jsl941i86628laxy467nx7015hmcyzkavlnn1xg08gchpwb")))

