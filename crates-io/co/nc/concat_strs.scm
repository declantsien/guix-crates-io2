(define-module (crates-io co nc concat_strs) #:use-module (crates-io))

(define-public crate-concat_strs-1.0.0 (c (n "concat_strs") (v "1.0.0") (d (list (d (n "concat_strs_impl") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0ffszajw9mgc6fx1nyif4lsk9gsxja86hirg3dnqwdx956vkw02k")))

(define-public crate-concat_strs-1.0.2 (c (n "concat_strs") (v "1.0.2") (d (list (d (n "concat_strs_impl") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1pj6shwyxhq3sckw9nywvhdy1i40kvd8y5m2a3sgfmgjik7skc9v")))

