(define-module (crates-io co nc concat_tuple) #:use-module (crates-io))

(define-public crate-concat_tuple-0.1.0 (c (n "concat_tuple") (v "0.1.0") (h "1macmc5z9zmy2k92pywkagzdnp0zdwfbxsvgrv2dyz6xyrx7398g")))

(define-public crate-concat_tuple-0.1.1 (c (n "concat_tuple") (v "0.1.1") (h "12z4489fabbva9x057is78cycczdh33f8870jz1866rn9cypxjv3")))

