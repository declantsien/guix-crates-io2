(define-module (crates-io co nc concur) #:use-module (crates-io))

(define-public crate-concur-0.1.0 (c (n "concur") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0702392md5wsax8z2c505w7025pxvwds0c67n68szi86lkr5013r")))

(define-public crate-concur-0.1.1 (c (n "concur") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "17kaq5y61bx6zzlg3v9va8ccdnzbllh9xpjplvvnsd2r0gbi0758")))

(define-public crate-concur-0.1.2 (c (n "concur") (v "0.1.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1m7di3bair0mbw8i6vxp9rjrr64x4fk4miqwybycz6ydqj6a6zdk")))

(define-public crate-concur-0.1.3 (c (n "concur") (v "0.1.3") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0vlmb3w2cr0ar5a8by68aw2qbbf0p7i4l5p0bn2j276hvvhlhw83")))

(define-public crate-concur-0.1.4 (c (n "concur") (v "0.1.4") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0mya0sr92v0h6qj8s6lzaiw2cis9zwwwrbfcr5pcm9bj3bkxc0kp")))

