(define-module (crates-io co nc concurrent_prime_sieve) #:use-module (crates-io))

(define-public crate-concurrent_prime_sieve-0.1.1 (c (n "concurrent_prime_sieve") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "1wsd01r6cyvv82r518f2rpwjnzdz062dg0xq1srrkafxghs5xa8i")))

(define-public crate-concurrent_prime_sieve-0.2.0 (c (n "concurrent_prime_sieve") (v "0.2.0") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "1lcrj8731hcm1l3i9s0cssaz8cd4w5ajgaiqsqnimv0m0zzb10gd")))

(define-public crate-concurrent_prime_sieve-0.2.1 (c (n "concurrent_prime_sieve") (v "0.2.1") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "1i0ffh3sb8sndg5m98h0p1m2vbr67mghzixinbxgwrcp7pinyw9h")))

(define-public crate-concurrent_prime_sieve-0.2.2 (c (n "concurrent_prime_sieve") (v "0.2.2") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "1lb43xmbw8gdpvpw8ixllg5g4ns6yg92lsb0chqijpvgw593c6i4")))

(define-public crate-concurrent_prime_sieve-0.2.3 (c (n "concurrent_prime_sieve") (v "0.2.3") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "0l0ddk5lbyj6yr0sq45wrdi8m6aba23wbp85kyac48kw68nzf41p")))

(define-public crate-concurrent_prime_sieve-0.2.4 (c (n "concurrent_prime_sieve") (v "0.2.4") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1k9i0hriad483sndc9irgdwbhzqkw81hszyph797qh7ml1xz4avg")))

(define-public crate-concurrent_prime_sieve-0.2.5 (c (n "concurrent_prime_sieve") (v "0.2.5") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.2.6") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1an423c0r95n9n0mwv2z59mv2416dj8rr7phlgnqmbab4bs8sgms")))

(define-public crate-concurrent_prime_sieve-0.3.0 (c (n "concurrent_prime_sieve") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.2.6") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1czhglkkk5hc493ir8b60c4q53m63583vkpgb5g49wrzcwcy32j6") (y #t)))

(define-public crate-concurrent_prime_sieve-0.3.1 (c (n "concurrent_prime_sieve") (v "0.3.1") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.2.6") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "14azm6c7805rn1cb5aaip27hh58f9xak3zvr9m3kbahbmn4rglrk")))

(define-public crate-concurrent_prime_sieve-0.3.2 (c (n "concurrent_prime_sieve") (v "0.3.2") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.2.6") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1yqqqgvgbf5dmc6wv9hj94b7fvnp6mwk5c526cic8nl02pksx99m")))

(define-public crate-concurrent_prime_sieve-0.3.3 (c (n "concurrent_prime_sieve") (v "0.3.3") (d (list (d (n "num_cpus") (r "^1.2.1") (d #t) (k 0)) (d (n "primal-sieve") (r "^0.2.6") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 2)))) (h "1jw0x25m3jc8mzcwwlvv1cqg650v9cbzwrsaikmy541hvhnnnvmv")))

