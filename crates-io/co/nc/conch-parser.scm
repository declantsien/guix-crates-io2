(define-module (crates-io co nc conch-parser) #:use-module (crates-io))

(define-public crate-conch-parser-0.0.0 (c (n "conch-parser") (v "0.0.0") (h "0zaq5har029an6xl45d5qyw7kajni4w6spzy8bsgv2jnppsnjz1g") (y #t)))

(define-public crate-conch-parser-0.1.0 (c (n "conch-parser") (v "0.1.0") (d (list (d (n "clippy") (r "~0.0.80") (o #t) (d #t) (k 0)) (d (n "owned_chars") (r "^0.2.1") (d #t) (k 2)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1jd3xcqhk585b6ib9ls9fs5wbj7vg7i2qkq9dh9g4yarz5y60sia") (f (quote (("nightly"))))))

(define-public crate-conch-parser-0.1.1 (c (n "conch-parser") (v "0.1.1") (d (list (d (n "owned_chars") (r "^0.2.1") (d #t) (k 2)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "0hasf1w55d4ljh9fgm79b5ap2r0n76fj2k4si9yhmy6zgvm1rxms") (f (quote (("nightly") ("clippy"))))))

