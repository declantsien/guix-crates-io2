(define-module (crates-io co nc conciliator) #:use-module (crates-io))

(define-public crate-conciliator-0.1.0 (c (n "conciliator") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)))) (h "04rm7x3gv1crjwsx6zz8gba99fl72d3wy4q3vfixmg8l1zr0kb68")))

(define-public crate-conciliator-0.2.0 (c (n "conciliator") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "10w2jfiba8b253ys7rvbi70h5ynhpzi1w9m2jczlg996s6acgs11")))

(define-public crate-conciliator-0.3.0 (c (n "conciliator") (v "0.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1jisk1j8w8clzs5jf4g2qwfq6vs5s0ax826fnjxnc8c73nbqd52x")))

(define-public crate-conciliator-0.3.1 (c (n "conciliator") (v "0.3.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1y618vvf27lxwvs57rml3gp9190sfihrivpwymbcya962yan9pdf")))

(define-public crate-conciliator-0.3.2 (c (n "conciliator") (v "0.3.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0nmjhskr9qkzshs2z7jfd2m4zks3zcxlrlwdn1rqj0rl51b0pf4f") (f (quote (("test_capture"))))))

(define-public crate-conciliator-0.3.3 (c (n "conciliator") (v "0.3.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "00dxd3xn7f7ks5xgvwwc1zss02xr8n5xwds45anplcqlxv9cqn9s") (f (quote (("test_capture"))))))

(define-public crate-conciliator-0.3.4 (c (n "conciliator") (v "0.3.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0gydym5zvw155aksg2cjl32j5327akj8rsdrr3ib9mckxvnnzh62") (f (quote (("test_capture"))))))

(define-public crate-conciliator-0.3.5 (c (n "conciliator") (v "0.3.5") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1yvry57m1kp67ggi6cyz9jd62j8acssgqdhgxqx8i65nyjd6lg7v") (f (quote (("test_capture"))))))

(define-public crate-conciliator-0.3.6 (c (n "conciliator") (v "0.3.6") (d (list (d (n "ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("rt" "sync" "time" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0lgcqm1c7yqfc8ic5zrnq5rswgd51cwpvppf50j4mp32g1xfrcka") (f (quote (("test_capture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-conciliator-0.3.7 (c (n "conciliator") (v "0.3.7") (d (list (d (n "ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("rt" "sync" "time" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0r5bp5hnjrbv4x3gnripnwpdlb42ikx5rip12cnkj6kkmg8z1dgi") (f (quote (("test_capture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-conciliator-0.3.8 (c (n "conciliator") (v "0.3.8") (d (list (d (n "ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("rt" "sync" "time" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0lndnby9i9263ylhhjyldjgpv6qlwxzx4cm181r3qb9lh0s0xsj0") (f (quote (("test_capture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-conciliator-0.3.9 (c (n "conciliator") (v "0.3.9") (d (list (d (n "ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("rt" "sync" "time" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "01zf4vwzazg0l00n7g7xn3vk6g7x3992hzkg534m0prfrrinyw1x") (f (quote (("test_capture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

