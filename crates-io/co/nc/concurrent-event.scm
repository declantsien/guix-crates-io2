(define-module (crates-io co nc concurrent-event) #:use-module (crates-io))

(define-public crate-concurrent-event-0.1.0 (c (n "concurrent-event") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0rzqkmr5i2imhq192q00nkfxhhfs00qghcx5iam7778acqy16zc7")))

