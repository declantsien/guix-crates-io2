(define-module (crates-io co nc concurrent_log) #:use-module (crates-io))

(define-public crate-concurrent_log-0.1.0 (c (n "concurrent_log") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0wnqr0riq9nck2azrz0yg6g4w2ksiy7ms2xgd820k6nr07l5hps2")))

(define-public crate-concurrent_log-0.2.0 (c (n "concurrent_log") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0cxk73paclzvgy8k0kxg55x55fidpmc1aq64x1vrhy44627h3fsp")))

(define-public crate-concurrent_log-0.2.1 (c (n "concurrent_log") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1jnsdf5fbrzmk5xfzjlh4040r749mf73fwpq3fypcyhf9bqs8nyk")))

(define-public crate-concurrent_log-0.2.2 (c (n "concurrent_log") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1sww2l24wrlbllflcf26jlvripyh49gw0krk36495k6vjcm63npn")))

(define-public crate-concurrent_log-0.2.3 (c (n "concurrent_log") (v "0.2.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1ji9l7368kv8ja3xz05lzb8y17dcy8ww28di01zyxdvmqr46w0bi")))

(define-public crate-concurrent_log-0.2.4 (c (n "concurrent_log") (v "0.2.4") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "177fqq0v3paqb8qz083qgnsclm6y1k9q154f77r3xqza9c38xqvx")))

