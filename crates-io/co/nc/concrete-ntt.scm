(define-module (crates-io co nc concrete-ntt) #:use-module (crates-io))

(define-public crate-concrete-ntt-0.1.0 (c (n "concrete-ntt") (v "0.1.0") (d (list (d (n "aligned-vec") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "pulp") (r "^0.11") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1yv2xdpgmajhnr53zwv6h59fb6crrckpazlz6xq43v414dw2vbmf") (f (quote (("std" "pulp/std" "aligned-vec/std") ("nightly" "pulp/nightly") ("default" "std")))) (r "1.67")))

(define-public crate-concrete-ntt-0.1.1 (c (n "concrete-ntt") (v "0.1.1") (d (list (d (n "aligned-vec") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "pulp") (r "^0.11") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "1mv4rlcfb9p466049wk6pjjmm4isyazsi84iqq2k49szfs1warqi") (f (quote (("std" "pulp/std" "aligned-vec/std") ("nightly" "pulp/nightly") ("default" "std")))) (r "1.67")))

(define-public crate-concrete-ntt-0.1.2 (c (n "concrete-ntt") (v "0.1.2") (d (list (d (n "aligned-vec") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "pulp") (r "^0.18.8") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "17llbxiqsi18qbglihzykzb71nykpg1fcahjk7h6kq2xplyn9x5l") (f (quote (("std" "pulp/std" "aligned-vec/std") ("nightly" "pulp/nightly") ("default" "std")))) (r "1.67")))

