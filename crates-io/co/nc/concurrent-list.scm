(define-module (crates-io co nc concurrent-list) #:use-module (crates-io))

(define-public crate-concurrent-list-0.0.0 (c (n "concurrent-list") (v "0.0.0") (h "0p2xapkxb756930b0zbx6aiaalms698s0ax3sbi50wxl8ji25hi3") (y #t) (r "1.56")))

(define-public crate-concurrent-list-0.0.1 (c (n "concurrent-list") (v "0.0.1") (h "0my29ypbi2xmv6ih0w9cr8pbn0k0rz9gql22m2kd7zszxlm5h0kf") (r "1.56")))

(define-public crate-concurrent-list-0.0.2 (c (n "concurrent-list") (v "0.0.2") (h "1m9ifx7pzmkxd27qnx5w2cjzwx1hii55iy9hqf2prq841vkncvgn") (r "1.56")))

