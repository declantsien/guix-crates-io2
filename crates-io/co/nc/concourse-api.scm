(define-module (crates-io co nc concourse-api) #:use-module (crates-io))

(define-public crate-concourse-api-0.0.0 (c (n "concourse-api") (v "0.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.10") (d #t) (k 1)) (d (n "located_yaml") (r "^0.2") (d #t) (k 1)) (d (n "voca_rs") (r "^1") (d #t) (k 1)))) (h "07py1pgl7f3az7kzc3xmvaaplgnllhva3ys0rdwbjjif03djhdcs")))

(define-public crate-concourse-api-0.0.1 (c (n "concourse-api") (v "0.0.1") (d (list (d (n "indexmap") (r "^1") (d #t) (k 1)) (d (n "itertools") (r "^0.10") (d #t) (k 1)) (d (n "located_yaml") (r "^0.2") (d #t) (k 1)) (d (n "voca_rs") (r "^1") (d #t) (k 1)))) (h "1xfywaasvzc4sqz6kqcd3gddj2k2ss4ixcs38szls1zykp1s25z0")))

