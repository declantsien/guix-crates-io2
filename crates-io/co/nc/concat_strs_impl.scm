(define-module (crates-io co nc concat_strs_impl) #:use-module (crates-io))

(define-public crate-concat_strs_impl-1.0.0 (c (n "concat_strs_impl") (v "1.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.15") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z6swp48wg9nvam1blxwgpzilh4691a4f1bvgj8xdvfhg9858ncc")))

(define-public crate-concat_strs_impl-1.0.1 (c (n "concat_strs_impl") (v "1.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.15") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wcjbsanvlibdxlvk47amy38yl8hqx8sz7da4h3h0g9vz5k7lyyf")))

