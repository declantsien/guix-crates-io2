(define-module (crates-io co nc concrete-commons) #:use-module (crates-io))

(define-public crate-concrete-commons-0.1.0 (c (n "concrete-commons") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j69a22x85j18sz1y8r60260cf3lzyrp8n49j1d964i9qxid4q2m")))

(define-public crate-concrete-commons-0.1.1 (c (n "concrete-commons") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bcicacnn0cb3v73h506vkm51bxpczmsh41yn52h0ykf5pqmigms")))

(define-public crate-concrete-commons-0.1.2 (c (n "concrete-commons") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gjj00ia55lcrijx3nafh8iwjv81srnaxy36mzbqc20nwadmm7ka")))

(define-public crate-concrete-commons-0.2.0 (c (n "concrete-commons") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1m29ij97ilhy4psa729wf4fjq7ak3zx6jh9wpbirwikdiqlhsbfb") (f (quote (("serde_serialize" "serde" "serde/derive"))))))

(define-public crate-concrete-commons-0.2.1 (c (n "concrete-commons") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1k1rsb7q3330mk32fw6llcgdplmakjf2801lzdcg8dwj386vkl0j") (f (quote (("serde_serialize" "serde" "serde/derive"))))))

