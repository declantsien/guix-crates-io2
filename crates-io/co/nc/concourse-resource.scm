(define-module (crates-io co nc concourse-resource) #:use-module (crates-io))

(define-public crate-concourse-resource-0.1.0 (c (n "concourse-resource") (v "0.1.0") (d (list (d (n "concourse-resource-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0da0ff8zamksdnslx00ahaxmn616fr296bgyhkhnrz871ng8fss4")))

(define-public crate-concourse-resource-0.2.0 (c (n "concourse-resource") (v "0.2.0") (d (list (d (n "concourse-resource-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pkmc7m1wx4n6mq4w84a2zg76mm5bna8hwczlpirkwjgp1h10s3k")))

(define-public crate-concourse-resource-0.2.1 (c (n "concourse-resource") (v "0.2.1") (d (list (d (n "concourse-resource-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ri59727qr8g79yvxflhm5fc8rwmb98dadcnvkv5zx19j90q3n9g")))

(define-public crate-concourse-resource-0.3.0 (c (n "concourse-resource") (v "0.3.0") (d (list (d (n "concourse-resource-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (f (quote ("json"))) (d #t) (k 2)))) (h "1j581v61yaky4j8px9a3ixzmqsyar5lsjka34rrhk5b4yzwg5lm8")))

