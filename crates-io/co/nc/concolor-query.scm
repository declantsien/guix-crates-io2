(define-module (crates-io co nc concolor-query) #:use-module (crates-io))

(define-public crate-concolor-query-0.0.1 (c (n "concolor-query") (v "0.0.1") (h "18sk4s6c9a3j6y647qlsr87kb2prldk2b6hil5swimaqmy8yfvaj") (f (quote (("windows"))))))

(define-public crate-concolor-query-0.0.2 (c (n "concolor-query") (v "0.0.2") (h "1vclx2mgyzing1nbvshgrskzmg8sqkb6v7abaqzkym2m1v6ml6gq") (f (quote (("windows"))))))

(define-public crate-concolor-query-0.0.3 (c (n "concolor-query") (v "0.0.3") (h "050dsml7jrz3nyq3mgc0ypp47sxa1azbc3afys2ajl9mclh2yc3x") (f (quote (("windows"))))))

(define-public crate-concolor-query-0.0.4 (c (n "concolor-query") (v "0.0.4") (h "1isbqpyiwblp0rglnaqzai5hav23095s82mwgi09v3xcck4rq5dd") (f (quote (("windows"))))))

(define-public crate-concolor-query-0.0.5 (c (n "concolor-query") (v "0.0.5") (h "0jcll1lnnkbdr6xcgppf6dr0ra9rxcp78xr1zlrvba03zkk7yhfn") (f (quote (("windows"))))))

(define-public crate-concolor-query-0.1.0 (c (n "concolor-query") (v "0.1.0") (h "05ykng7pqhm7840yh07r27p90flwrrmwlk32wxbgdp6mncs0gac2") (f (quote (("windows")))) (r "1.60.0")))

(define-public crate-concolor-query-0.2.0 (c (n "concolor-query") (v "0.2.0") (d (list (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zgzjvhcqw2lx6ndxbba27q2bjs7b2yynwc4l2j8imbdz2wp1kbi") (r "1.60.0")))

(define-public crate-concolor-query-0.2.1 (c (n "concolor-query") (v "0.2.1") (d (list (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0s9mlb22a1z48r1p44bbsdn68nl74qabxpmx5039afy7zp9jf5f8") (r "1.60.0")))

(define-public crate-concolor-query-0.3.0 (c (n "concolor-query") (v "0.3.0") (d (list (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lx1i2v9m8kkycs9s4sg2xpvxcphvh665n5svqhbhia9ll6v8kjk") (r "1.60.0")))

(define-public crate-concolor-query-0.3.1 (c (n "concolor-query") (v "0.3.1") (d (list (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zcpnrzbr22zwdyc1h6ig2dzzwf34zgc9nib9ly2b1qkdvvifr72") (r "1.60.0")))

(define-public crate-concolor-query-0.3.2 (c (n "concolor-query") (v "0.3.2") (d (list (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rkps2wjibh89gqpa4d3iynrsf884a7kdhpaikf0vjr44nncc82b") (r "1.60.0")))

(define-public crate-concolor-query-0.3.3 (c (n "concolor-query") (v "0.3.3") (d (list (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Console" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ks4g514kx31nnv3bxa7cj5xgg6vwkljn8a001njxjnpqd91vlc8") (r "1.60.0")))

