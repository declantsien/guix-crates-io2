(define-module (crates-io co nc concordium-contracts-common-derive) #:use-module (crates-io))

(define-public crate-concordium-contracts-common-derive-1.0.0 (c (n "concordium-contracts-common-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "095zgb2jvcb29jqfm6q3vzgkkygkpwp0x99sjrfdsxx26lxj55ql") (f (quote (("sdk"))))))

(define-public crate-concordium-contracts-common-derive-1.0.1 (c (n "concordium-contracts-common-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10n06yq2j68k72m42sjka1z7nfvjw77apr4qz2sk79gz93cl7jl4") (f (quote (("sdk"))))))

(define-public crate-concordium-contracts-common-derive-2.0.0 (c (n "concordium-contracts-common-derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s3wp79jrpghhafx7si3xdkxjf88d4490a33vcdd4nxrg9k3hja3") (r "1.60")))

(define-public crate-concordium-contracts-common-derive-3.0.0 (c (n "concordium-contracts-common-derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mgxfyyfcabd71j05bvkckixlxldln6cqv7g30j7dksz7c39qkyk") (f (quote (("wasm-test") ("concordium-quickcheck") ("build-schema")))) (r "1.65")))

(define-public crate-concordium-contracts-common-derive-4.0.0 (c (n "concordium-contracts-common-derive") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ri6qd813z7k4yb1mvbxjasc72q0sn8c2m489630m30d0m5w7z2g") (f (quote (("wasm-test") ("concordium-quickcheck") ("build-schema")))) (r "1.65")))

(define-public crate-concordium-contracts-common-derive-4.0.1 (c (n "concordium-contracts-common-derive") (v "4.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lmf4lp57wiwizc5maz34icgccfax71sq47cb0b6nqg6xgrv578g") (f (quote (("wasm-test") ("concordium-quickcheck") ("build-schema")))) (r "1.65")))

(define-public crate-concordium-contracts-common-derive-4.1.0 (c (n "concordium-contracts-common-derive") (v "4.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1753gzq3vd48b0wfj5h3mhcf11vf9f3inv4ppqrq3hgkmkzq4d7f") (f (quote (("wasm-test") ("concordium-quickcheck") ("build-schema")))) (r "1.73")))

