(define-module (crates-io co nc concat-in-place) #:use-module (crates-io))

(define-public crate-concat-in-place-1.0.0 (c (n "concat-in-place") (v "1.0.0") (h "01d3bi8ygb1km26v8y2wk2bkbmjn7akksm4ic1ipf9lvidwbsk0q")))

(define-public crate-concat-in-place-1.1.0 (c (n "concat-in-place") (v "1.1.0") (h "0c5l6syg7r0xzmbsycah2zl7f4bzhcmk3l1ad55hqvnjcnx0vf65")))

