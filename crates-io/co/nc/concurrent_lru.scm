(define-module (crates-io co nc concurrent_lru) #:use-module (crates-io))

(define-public crate-concurrent_lru-0.1.0 (c (n "concurrent_lru") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0409h8kchgdscs70sk3aizsc1ar1p0ys3y85ygvcgqrdfpnsnn7q")))

(define-public crate-concurrent_lru-0.2.0 (c (n "concurrent_lru") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0kif9k7qjqx14hfnfqw591yhz2afvc375qj08nifhx7p2armrsvz")))

