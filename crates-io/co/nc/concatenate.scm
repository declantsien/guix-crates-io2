(define-module (crates-io co nc concatenate) #:use-module (crates-io))

(define-public crate-concatenate-1.0.0 (c (n "concatenate") (v "1.0.0") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "14kwdr6lni8vcp5s3fv7kyvwvlf1wm6f80qcjmd8g2yj8917km1a")))

(define-public crate-concatenate-1.0.1 (c (n "concatenate") (v "1.0.1") (d (list (d (n "structopt") (r ">= 0.3.9") (d #t) (k 0)))) (h "16yiny72kxb4b4q35y94hf3sv3yvg1xbm5abbkv4frwax6w7zllx")))

