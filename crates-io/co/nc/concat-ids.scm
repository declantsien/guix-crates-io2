(define-module (crates-io co nc concat-ids) #:use-module (crates-io))

(define-public crate-concat-ids-0.1.0 (c (n "concat-ids") (v "0.1.0") (d (list (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "0yci5a6b7rbpailx5x2l96bp3ck32iaadac0x32kbqq4pcks2vzq")))

(define-public crate-concat-ids-0.1.1 (c (n "concat-ids") (v "0.1.1") (d (list (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1g2rwv8rr05nfk01h29iwzm2zq44fpvy55m03i5xcnbh02xfixg3")))

