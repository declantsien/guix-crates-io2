(define-module (crates-io co nc concatsql_macro) #:use-module (crates-io))

(define-public crate-concatsql_macro-0.1.0 (c (n "concatsql_macro") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "concatsql") (r "^0.4") (d #t) (k 2)))) (h "06a1l9rc7jcwhs698fyk9wyr8ri9vi2zpxfq1plwzkpd6n3n217h")))

