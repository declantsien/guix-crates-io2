(define-module (crates-io co nc concrete-fftw-sys) #:use-module (crates-io))

(define-public crate-concrete-fftw-sys-0.1.0 (c (n "concrete-fftw-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "fs_extra") (r "~1.2.0") (d #t) (k 1)) (d (n "intel-mkl-src") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)))) (h "06qcdcw9xq5nzw2vdpq1lqfkxk4h03k066wpadf7v25ak5ldrk1h") (f (quote (("mkl" "intel-mkl-src"))))))

(define-public crate-concrete-fftw-sys-0.1.1 (c (n "concrete-fftw-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "fs_extra") (r "~1.2.0") (d #t) (k 1)) (d (n "intel-mkl-src") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)))) (h "140zgdkbafc2vqv4cahhvn5ginkss63df7hnba74dqqxjfn6b7i9") (f (quote (("mkl" "intel-mkl-src"))))))

(define-public crate-concrete-fftw-sys-0.1.2 (c (n "concrete-fftw-sys") (v "0.1.2") (d (list (d (n "fs_extra") (r "~1.2.0") (d #t) (k 1)) (d (n "intel-mkl-src") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)))) (h "1fywwvqb6y6ajq8h9sgacqz7c2fngrji70n1iq1nsliwc1f1b208") (f (quote (("mkl" "intel-mkl-src"))))))

(define-public crate-concrete-fftw-sys-0.1.3 (c (n "concrete-fftw-sys") (v "0.1.3") (d (list (d (n "fs_extra") (r "~1.2.0") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)))) (h "1i836id3w0qh017jyhjxa9pv841ckjqamfaa494qnv805x5rmsww")))

(define-public crate-concrete-fftw-sys-0.1.4 (c (n "concrete-fftw-sys") (v "0.1.4") (d (list (d (n "fs_extra") (r "~1.2.0") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)))) (h "0m7v7v44gywddi8kq0ii6h6d4c0fgln5wr2xkpfg6dl59db92kj7")))

