(define-module (crates-io co nc conclave-room) #:use-module (crates-io))

(define-public crate-conclave-room-0.0.1 (c (n "conclave-room") (v "0.0.1") (h "1z9qb7y0k2rg5iig9wcidlmlxc9p5c0gwr8vyjv0l8fhww00c81g")))

(define-public crate-conclave-room-0.0.2-pre01 (c (n "conclave-room") (v "0.0.2-pre01") (h "0akx23j0802ywzq888c11dq9lnvi0298nh5vbjqyi25g2aamz2a0")))

(define-public crate-conclave-room-0.0.2-pre02 (c (n "conclave-room") (v "0.0.2-pre02") (h "0rbvw21a0j0psd3s8i20jx9ndw8rmv2qfqc0f6jk1zr69qchp99a")))

