(define-module (crates-io co nc concurrent-round-robin) #:use-module (crates-io))

(define-public crate-concurrent-round-robin-0.1.0 (c (n "concurrent-round-robin") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dashmap") (r "^5.1") (f (quote ("raw-api"))) (d #t) (k 0)))) (h "0h48mjrz49m88a1ayv2agsg7rpwff739bmmwjgzxxz3v83vmhv8y")))

