(define-module (crates-io co nc concat-with) #:use-module (crates-io))

(define-public crate-concat-with-0.1.0 (c (n "concat-with") (v "0.1.0") (h "11rgx3s8k07akqcybi0h7l668ib7zmikkcrjmpd4k5zssk8rrr61")))

(define-public crate-concat-with-0.2.0 (c (n "concat-with") (v "0.2.0") (h "005sxibzsd42j4dhp7b3mvrjpskq7dpyyi2zj9f2gq8c6pmab0n1")))

(define-public crate-concat-with-0.2.1 (c (n "concat-with") (v "0.2.1") (h "0rk57i46gdabccsvr7wclhsckzng0wqf1nz15prrmghkq5b6243x")))

(define-public crate-concat-with-0.2.2 (c (n "concat-with") (v "0.2.2") (h "15n7z8i5rkfw97rr1nmlpcr1wz5zgx6kayf8m5aap8pvvspz5ga8") (y #t)))

(define-public crate-concat-with-0.2.3 (c (n "concat-with") (v "0.2.3") (h "0rq28a3q2bdgj6lks82d68iari1ipds2ylz9q44h5hrm112hsd28")))

(define-public crate-concat-with-0.2.4 (c (n "concat-with") (v "0.2.4") (h "0g95hnk71hbygfvd63qwdrq2l0qzgkvsf2ckkp7msmi3qblw9c32")))

(define-public crate-concat-with-0.2.5 (c (n "concat-with") (v "0.2.5") (h "0mdd8h2fjb5fi8qir6mnyh1j99ql71fymh3809mdvggrpcv5kr1a")))

(define-public crate-concat-with-0.2.6 (c (n "concat-with") (v "0.2.6") (h "1vcgw9d3m2a5j74l94j7g4m7vm2dp7xbkavkn2jlvwy91cp5pq41")))

(define-public crate-concat-with-0.2.7 (c (n "concat-with") (v "0.2.7") (h "0zp0ilyy0x0q108ly2y8i2dqqfrznwdmqffqz3x74qhczh6lga9g")))

(define-public crate-concat-with-0.2.8 (c (n "concat-with") (v "0.2.8") (h "0zyhnaq253af86fa9bfzmhg17f1a77sf4xnmprbssqzxpffkxchb")))

(define-public crate-concat-with-0.2.9 (c (n "concat-with") (v "0.2.9") (h "0krfqby940vpza7df2r08gahk22r7a569xgmwwp46pgnrp4pylj5") (r "1.56")))

