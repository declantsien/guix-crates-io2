(define-module (crates-io co te cote-derive) #:use-module (crates-io))

(define-public crate-cote-derive-0.1.0 (c (n "cote-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jpvn6a6xpk4nffla3q7rrw0d07z8smjxbl63krql05146pyqpii") (y #t)))

(define-public crate-cote-derive-0.1.11 (c (n "cote-derive") (v "0.1.11") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "09isc4qx47psrq22l20i7m8qxzs74bd9pf2bs90lnq16sk4l7hds") (y #t)))

(define-public crate-cote-derive-0.1.12 (c (n "cote-derive") (v "0.1.12") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1jfzxianzbng2j7nmwvbiirsp53zj9jz7yc34halwp185byzvf2q") (y #t)))

(define-public crate-cote-derive-0.2.1 (c (n "cote-derive") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "152x23k3w8dvf2dffnwsn9ylq9i5z3xdx4862fwvsbwk7k95zkyd") (y #t)))

(define-public crate-cote-derive-0.2.2 (c (n "cote-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1n5f0s2scq4wyviiihabaisymjyc6zcgnjfp8i9k9i3g3l8k746r") (y #t)))

(define-public crate-cote-derive-0.2.3 (c (n "cote-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0nvvhgalkj7m2wpfp3p9vsfx8wcnj72qhngyv94ilf75wdgc4s1m")))

(define-public crate-cote-derive-0.3.0 (c (n "cote-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "16g9mffdxrp7rk4xrphjpxwfs0dn04y0nqn9c57cba8svlpg8fqs")))

