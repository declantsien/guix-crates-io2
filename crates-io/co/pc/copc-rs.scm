(define-module (crates-io co pc copc-rs) #:use-module (crates-io))

(define-public crate-copc-rs-0.1.0 (c (n "copc-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "las") (r "^0.7.7") (d #t) (k 0)) (d (n "laz") (r "^0.6.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1ryfsiazrl2xbaq355vm1m4rzmgci0q40g0d129a6qn8kwcb805i")))

(define-public crate-copc-rs-0.2.0 (c (n "copc-rs") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "las") (r "^0.7.7") (d #t) (k 0)) (d (n "laz") (r "^0.6.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "066m408n2mnqgzp1dhz5akggb11k4r1d6svyw3zyq5q7qpwcsiqc")))

(define-public crate-copc-rs-0.3.0 (c (n "copc-rs") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "http-range-client") (r "^0.7.0") (d #t) (k 2)) (d (n "las") (r "^0.8.1") (d #t) (k 0)) (d (n "laz") (r "^0.8.2") (d #t) (k 0)))) (h "1w3659irv7mjmxdd1a88rwvwnaynrkc536j35rcg9bh6phhwl0nh")))

