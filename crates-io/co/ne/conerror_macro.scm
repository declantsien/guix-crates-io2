(define-module (crates-io co ne conerror_macro) #:use-module (crates-io))

(define-public crate-conerror_macro-0.1.0 (c (n "conerror_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1g83l9lvvcxfsh6fh7yl1gap4w7wxv9lczv242yb9ipbaipmsmyw")))

(define-public crate-conerror_macro-0.1.1 (c (n "conerror_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1yg1nxnm83myblw8crhq9qb95n2lg7r7b9mggwd7syd5ci6dk3rl")))

(define-public crate-conerror_macro-0.1.2 (c (n "conerror_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1394xigjbcklybf6jlaq7ava511k3z1gcyri3fss6s11f4w0g803")))

(define-public crate-conerror_macro-0.1.3 (c (n "conerror_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1dpg72bkiidnfldi8q26g7wgbdmjv19a4m4jq1avlwgmgksdfh2q")))

(define-public crate-conerror_macro-0.1.4 (c (n "conerror_macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "026r6jdsn28jp6v00rzw7p48x03nvdda8xyfqgkpmfakr4pp1j30")))

(define-public crate-conerror_macro-0.1.5 (c (n "conerror_macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1kraljc7ibjnidx9fg5bp322aj98wdyvs2zms0268m0ssn27ixnx")))

