(define-module (crates-io co ne conerror) #:use-module (crates-io))

(define-public crate-conerror-0.1.0 (c (n "conerror") (v "0.1.0") (d (list (d (n "conerror_macro") (r "=0.1.0") (d #t) (k 0)))) (h "1p8myrq7b4dz1x3552bgbwk6jcsr0809zsij9hgq842x4mriqzq5")))

(define-public crate-conerror-0.1.1 (c (n "conerror") (v "0.1.1") (d (list (d (n "conerror_macro") (r "=0.1.0") (d #t) (k 0)))) (h "0ilbb81rsswma0lif3jpfp146qrblmq7dkchkv5rg75d48m2z3mw")))

(define-public crate-conerror-0.1.2 (c (n "conerror") (v "0.1.2") (d (list (d (n "conerror_macro") (r "=0.1.0") (d #t) (k 0)))) (h "1qsdrfyd1jraimpwv1x0mx0xjl1xjbc1vhsavs387xmfsxyld8sj") (y #t)))

(define-public crate-conerror-0.1.3 (c (n "conerror") (v "0.1.3") (d (list (d (n "conerror_macro") (r "=0.1.1") (d #t) (k 0)))) (h "1z49xvidahip6yis7a3pkca623fb2fpk9c5lbzmi3w70snhgfva6") (y #t)))

(define-public crate-conerror-0.1.4 (c (n "conerror") (v "0.1.4") (d (list (d (n "conerror_macro") (r "=0.1.2") (d #t) (k 0)))) (h "17lnzxjaaawj8iyby5iqyhmd776iydf0alih1k8rd5rh63aiv1i5")))

(define-public crate-conerror-0.1.5 (c (n "conerror") (v "0.1.5") (d (list (d (n "conerror_macro") (r "=0.1.3") (d #t) (k 0)))) (h "14ikavc60xgljkls98n1dl9vhm4cm8x3v78aj8r8fayqphqq90bh")))

(define-public crate-conerror-0.1.6 (c (n "conerror") (v "0.1.6") (d (list (d (n "conerror_macro") (r "=0.1.4") (d #t) (k 0)))) (h "1cspb9pxm13p242kq5w885j03jsl7apcc8mpy371w0xi1p41q5gd")))

(define-public crate-conerror-0.1.7 (c (n "conerror") (v "0.1.7") (d (list (d (n "conerror_macro") (r "=0.1.4") (d #t) (k 0)))) (h "1sn7pigbca5ff00hnfl87xjfkcc9nspb48hgsybshskxafzr7sya")))

(define-public crate-conerror-0.1.8 (c (n "conerror") (v "0.1.8") (d (list (d (n "conerror_macro") (r "=0.1.5") (d #t) (k 0)))) (h "19vrgrvdl768k50ixfkmqr787dc2y03q9hy6ncp57k8b1ca2aqvj")))

