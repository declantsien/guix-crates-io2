(define-module (crates-io co ne cone) #:use-module (crates-io))

(define-public crate-cone-0.1.0 (c (n "cone") (v "0.1.0") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)))) (h "04chl0167011kd0rnpnnlhpzmxvx5vakwq40xabzqzzs420fdl1i")))

(define-public crate-cone-0.1.1 (c (n "cone") (v "0.1.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)))) (h "09xhbywp63bv6jkyr2lfh7ar0gl2hp56szhz9l7pv0fqg4h2sfik")))

(define-public crate-cone-0.1.2 (c (n "cone") (v "0.1.2") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)))) (h "0ywfp8ykpy839ff9jyyij1f4cycj2r7vd6x6zza5rcip2h9i1rzf")))

(define-public crate-cone-0.1.3 (c (n "cone") (v "0.1.3") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)))) (h "0yxibj1jni76gk8hp2gcyhy8hb22264fh08q1lrpq7swcxcqy9lx")))

(define-public crate-cone-0.1.4 (c (n "cone") (v "0.1.4") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)))) (h "0gzaxq3719bwq5kwnsbkmwl746mvimm6413bshr891ps41x9bgn9")))

(define-public crate-cone-0.1.5 (c (n "cone") (v "0.1.5") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)))) (h "0w1kdlzqgxyk6r58qq7pyk4yy0y7azqk291azm8cxg1yl69qis82")))

(define-public crate-cone-0.1.6 (c (n "cone") (v "0.1.6") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glutin") (r "^0.24") (d #t) (k 0)) (d (n "png") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gs1xmqmggjq3qnqfs3f51xgfk2pvk3q3452bygzrs9ycinbqr78") (f (quote (("serde1" "serde") ("default"))))))

