(define-module (crates-io co ne coneman-cli) #:use-module (crates-io))

(define-public crate-coneman-cli-0.1.0 (c (n "coneman-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "08xfm9n215w8m1wg6jnjfghn6livpavqrvg61sa3zpz7n3ssykjd")))

(define-public crate-coneman-cli-0.1.1 (c (n "coneman-cli") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1gjlr1rngik3mw2s6xsv5bp2cbpw3pylrp27ky4s5a4s0w4qpla2")))

