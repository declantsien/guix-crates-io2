(define-module (crates-io co ne coneman) #:use-module (crates-io))

(define-public crate-coneman-0.1.1 (c (n "coneman") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "00fl0z8xpprsfzi1dbh9qpdymya9bd5877irr77gds0vgidvn4cp")))

(define-public crate-coneman-0.1.2 (c (n "coneman") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "13fcjhb1bavqnyibxhrb3qd82jisrxa5v357vskkl6x0qpg8hkl2")))

