(define-module (crates-io co ro corollary) #:use-module (crates-io))

(define-public crate-corollary-0.0.1 (c (n "corollary") (v "0.0.1") (h "1784yx6hzxj572pvhsrw3q9h3501i6h54vrx0fv12d0c1fy2aslv")))

(define-public crate-corollary-0.3.0 (c (n "corollary") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "errln") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "iflet") (r "^0.1.0") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.12.5") (d #t) (k 0)) (d (n "maplit") (r "^0.1.4") (d #t) (k 0)) (d (n "matches") (r "^0.1.4") (d #t) (k 0)) (d (n "parser-haskell") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1w2c32i1p8s81qs4r2yazq4bghsdaks7vww928204iqlxwspwyiz")))

