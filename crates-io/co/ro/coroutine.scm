(define-module (crates-io co ro coroutine) #:use-module (crates-io))

(define-public crate-coroutine-0.1.0 (c (n "coroutine") (v "0.1.0") (d (list (d (n "deque") (r "*") (d #t) (k 0)) (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 1)) (d (n "num_cpus") (r "*") (d #t) (k 2)))) (h "013mzpnc3idpbm57yk6bv35ssh0pgazldqv5gqhn8xsl7xfa68cy")))

(define-public crate-coroutine-0.1.1 (c (n "coroutine") (v "0.1.1") (d (list (d (n "deque") (r "*") (d #t) (k 0)) (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 1)) (d (n "num_cpus") (r "*") (d #t) (k 2)))) (h "10r7rhps515lg78yywkfd6gqp71bgy4wlfksanbx1zy4idcksdv2")))

(define-public crate-coroutine-0.1.2 (c (n "coroutine") (v "0.1.2") (d (list (d (n "deque") (r "*") (d #t) (k 0)) (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 1)) (d (n "num_cpus") (r "*") (d #t) (k 2)))) (h "040svyn8i48kazlifi0yr90w6vgr9bk4gg6nv8v875imjj3ph9jn")))

(define-public crate-coroutine-0.1.5 (c (n "coroutine") (v "0.1.5") (d (list (d (n "deque") (r "*") (d #t) (k 0)) (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 1)) (d (n "num_cpus") (r "*") (d #t) (k 2)))) (h "1s3dvcmdb408hpcasqhdb8mxp6xpg19gf1is42239fn4c3zgxicx")))

(define-public crate-coroutine-0.1.6 (c (n "coroutine") (v "0.1.6") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 2)))) (h "1a9jxjc3nv530hl6qpm7fp6lif3b51wy9b2q3dpjwga960z2msy2") (f (quote (("enable-stack-recycle") ("default" "enable-stack-recycle"))))))

(define-public crate-coroutine-0.1.7 (c (n "coroutine") (v "0.1.7") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 2)))) (h "13j6cl3k8rmiri1ydp6w91yb119jpwdfzl74d09n1zc8g2pxqm34")))

(define-public crate-coroutine-0.2.0 (c (n "coroutine") (v "0.2.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 2)) (d (n "spin") (r "*") (o #t) (d #t) (k 0)))) (h "1bv8gfjn60qfhdzic03rhqf1pwl02spfmr2xlqw9gcnbq8dwf32v") (f (quote (("enable-clonable-handle" "spin") ("default" "enable-clonable-handle"))))))

(define-public crate-coroutine-0.3.1 (c (n "coroutine") (v "0.3.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 2)) (d (n "spin") (r "*") (o #t) (d #t) (k 0)))) (h "03578n3nfad1mvm9gq19bxq3ky55xhi95s5vk7q1l8571iqyzdgd") (f (quote (("enable-clonable-handle" "spin") ("default" "enable-clonable-handle"))))))

(define-public crate-coroutine-0.3.2 (c (n "coroutine") (v "0.3.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 2)) (d (n "spin") (r "*") (o #t) (d #t) (k 0)))) (h "0whki9bw1zjpnp03b30wx792zc53yml6jr7140k00phfc9pcjci6") (f (quote (("enable-clonable-handle" "spin") ("default" "enable-clonable-handle"))))))

(define-public crate-coroutine-0.5.0 (c (n "coroutine") (v "0.5.0") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 1)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 2)))) (h "1dapsd187rp34gh6pbjhf3jzj6ic59h80rcp01v32p00y4l41vai")))

(define-public crate-coroutine-0.6.0 (c (n "coroutine") (v "0.6.0") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.26") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 1)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 2)))) (h "11gq2l115jfbpqmqfcgvp96ss4cn37zhg61hndy53fcfvs52kj5l")))

(define-public crate-coroutine-0.7.0 (c (n "coroutine") (v "0.7.0") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 1)) (d (n "num_cpus") (r "^1.3") (d #t) (k 2)))) (h "1l021nbj05ss5x152kw8p7g7dy6pwxnrn3f6dx2lmy4mq76p8lxh")))

(define-public crate-coroutine-0.7.1 (c (n "coroutine") (v "0.7.1") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 1)) (d (n "num_cpus") (r "^1.3") (d #t) (k 2)))) (h "1230ycb0da2c1rziwc4n2ajm0qpg0in25rjam3dnsfrkpnq7zc9n")))

(define-public crate-coroutine-0.8.0 (c (n "coroutine") (v "0.8.0") (d (list (d (n "context") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 1)))) (h "0yzk14saj9i27fg2436c2p4sn9ak0dch90avgrpybr9b9r4g4n6q")))

