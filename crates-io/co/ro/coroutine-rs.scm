(define-module (crates-io co ro coroutine-rs) #:use-module (crates-io))

(define-public crate-coroutine-rs-0.0.1 (c (n "coroutine-rs") (v "0.0.1") (d (list (d (n "deque") (r "*") (d #t) (k 0)) (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "log") (r "*") (d #t) (k 1)) (d (n "mmap") (r "*") (d #t) (k 0)) (d (n "num_cpus") (r "*") (d #t) (k 1)) (d (n "num_cpus") (r "*") (d #t) (k 2)))) (h "0nxz105yl82ks96vfihv2bk3ddrqymqdpbxrqg3kxmwfbbpmj673") (y #t)))

