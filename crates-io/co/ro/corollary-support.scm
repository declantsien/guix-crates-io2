(define-module (crates-io co ro corollary-support) #:use-module (crates-io))

(define-public crate-corollary-support-0.1.0 (c (n "corollary-support") (v "0.1.0") (h "1gdgrrxjldzxbhsw8sjzr7g1yv6g14awnjr5blny5gp72w2nz8pv")))

(define-public crate-corollary-support-0.2.0 (c (n "corollary-support") (v "0.2.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "00fi2v5bmv61168qh8zric9rhp1qwwkss7wwipg9bhxzf7h4rlyj")))

