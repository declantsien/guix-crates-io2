(define-module (crates-io co ro coronavirus) #:use-module (crates-io))

(define-public crate-coronavirus-0.1.0 (c (n "coronavirus") (v "0.1.0") (h "0x17a0zqi89dfhmniqdypcfyvjvi447xlnj873kwmlifb190inx6")))

(define-public crate-coronavirus-0.1.1 (c (n "coronavirus") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "curl") (r "^0.4.26") (d #t) (k 0)))) (h "1yd00024qnpd2yi5iwjkksxz7i9r2yxdr7z9ihp6gh2ard3caiaj")))

(define-public crate-coronavirus-0.1.2 (c (n "coronavirus") (v "0.1.2") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "curl") (r "^0.4.26") (d #t) (k 0)))) (h "06gj0rv614ghz8k0bcqn7d4444zppd45mxh3i4gg3hs3cln3fpim")))

