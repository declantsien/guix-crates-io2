(define-module (crates-io co ro coroutines) #:use-module (crates-io))

(define-public crate-coroutines-0.1.0 (c (n "coroutines") (v "0.1.0") (h "0chp3sdsgdl6vsbwzk1gi3yl1raqw91qsm12qydv4562vzihb7av")))

(define-public crate-coroutines-0.1.1 (c (n "coroutines") (v "0.1.1") (d (list (d (n "spin") (r "^0.4") (k 0)))) (h "04i6wqw996xmkqcq1d8wp1lk7z15wq1bb7rcz0a22i1bqxqkc398")))

(define-public crate-coroutines-0.1.2 (c (n "coroutines") (v "0.1.2") (d (list (d (n "spin") (r "^0.4") (k 0)))) (h "0s84ab5w16b9728322kgwlfwzak52a3038qh4zk5g4xiyps320xw")))

(define-public crate-coroutines-0.2.0 (c (n "coroutines") (v "0.2.0") (d (list (d (n "spin") (r "^0.4") (k 0)))) (h "0xzar40abdcly0773s4096zn9gr0l0aj2njjnb3ypdhj1l6f7p9v")))

