(define-module (crates-io co st cost) #:use-module (crates-io))

(define-public crate-cost-0.1.0 (c (n "cost") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "12r23lk0yncjrk0m624j6vm3izsmp9hw1ws7q6akjbv1aj23qva1")))

(define-public crate-cost-0.1.1 (c (n "cost") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1yz742sf5gy9bsnl51wic07z3bvdyhl1flw54j14vqh0a6lhmh1v")))

