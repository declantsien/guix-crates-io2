(define-module (crates-io co ll collada) #:use-module (crates-io))

(define-public crate-collada-0.0.1 (c (n "collada") (v "0.0.1") (d (list (d (n "RustyXML") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quaternion") (r "*") (d #t) (k 0)) (d (n "vecmath") (r "*") (d #t) (k 0)))) (h "0h1yjbqylyv5i53abs7my629f5r5k0z2d9v0dqa7dxax4f94fcz2")))

(define-public crate-collada-0.1.0 (c (n "collada") (v "0.1.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quaternion") (r "^0.1.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.1.1") (d #t) (k 0)))) (h "0sb7ighcwggfxr3s7i4sx4g53zvfw44rw3ma64x8gdaljz5aaagp")))

(define-public crate-collada-0.2.0 (c (n "collada") (v "0.2.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "07zxmb31rcs9ck0l5v9l6ksp6r4s85aq6gxl7l5rxqz8viy4s1rv")))

(define-public crate-collada-0.3.0 (c (n "collada") (v "0.3.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "0hg51dmlvvg4wf5277rnq9j8n85r1yxp5mgx8krfrffavadhdisj")))

(define-public crate-collada-0.4.0 (c (n "collada") (v "0.4.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quaternion") (r "^0.2.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.2.0") (d #t) (k 0)))) (h "15wjh8vv9pqzqy63ipd9y6cz0vcrax58lky16pxm2i9pndyd8n2v")))

(define-public crate-collada-0.5.0 (c (n "collada") (v "0.5.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "1bx3iyj76jdjp49ani5vzhzf7lid2mwgigifiyxxrs2l7i14np39")))

(define-public crate-collada-0.6.0 (c (n "collada") (v "0.6.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "14yyznzzip711h15j25xdpl1isn5swy40i27731694fsdd7z353z")))

(define-public crate-collada-0.7.0 (c (n "collada") (v "0.7.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "08fi37106qipq7abbvvsd1128wiqfwszv20pb0cnh5xk31vym743")))

(define-public crate-collada-0.8.0 (c (n "collada") (v "0.8.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "11ck0k37wbdbzb6b7q643yam2369n6xdjxhicgplbmkfxxgqq9gz")))

(define-public crate-collada-0.9.0 (c (n "collada") (v "0.9.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0c9szzdqxy709p3p78azfqkfgnznqabg5xmxlqb7sslfgbk0jmp8")))

(define-public crate-collada-0.10.0 (c (n "collada") (v "0.10.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^0.3.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0b6zxkiqs1kazwwgzrjrf877hr15ksn1i2n5dn92lnwzkv3km6nh")))

(define-public crate-collada-0.11.0 (c (n "collada") (v "0.11.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "1k52qj4xlgyvj7izmyln1hqlh7clzhhhiglfiglhmpzmicymi6cm")))

(define-public crate-collada-0.12.0 (c (n "collada") (v "0.12.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "1pgvv0d5k95vdhlmviv29ij0m4maj0g2qpcqz2a859dxyszhd4l1")))

(define-public crate-collada-0.12.1 (c (n "collada") (v "0.12.1") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "180mblk2lchyc9lwhl1nkqxxsn1x1x88d4h3sjvgjllsj1jjrkwv")))

(define-public crate-collada-0.13.0 (c (n "collada") (v "0.13.0") (d (list (d (n "RustyXML") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "0k81fvn1rq79s34ckmgzrlbqnc9cdw2hkhvqhi5qn9qbk6iff533")))

(define-public crate-collada-0.14.0 (c (n "collada") (v "0.14.0") (d (list (d (n "RustyXML") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^0.4.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "0p9b5ajdmlj24v0rkrng5r7pyxbgwfrnni1b3ljh0w33l12mjlnn")))

(define-public crate-collada-0.15.0 (c (n "collada") (v "0.15.0") (d (list (d (n "RustyXML") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quaternion") (r "^1.0.0") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "014qq1aq6h8bdfkk1n31yirjj432wvy450h0fd2ny73dyc66vzhb")))

