(define-module (crates-io co ll collections-rs) #:use-module (crates-io))

(define-public crate-collections-rs-0.1.0 (c (n "collections-rs") (v "0.1.0") (h "1hb42ippkj9wdnhnx7vxx6vir5hkny7hs69dhabnbn9g577lss5v")))

(define-public crate-collections-rs-0.2.0 (c (n "collections-rs") (v "0.2.0") (h "1wxaqq6i4xh5hc18qk5bnibkmcbgwc6pi8pvln2zi6l3djmd8xih")))

(define-public crate-collections-rs-0.2.1 (c (n "collections-rs") (v "0.2.1") (h "1y90mx0q8p4abbgkpdqxs2927m7w0j2wzqylfiips00654qh736d")))

(define-public crate-collections-rs-0.3.0 (c (n "collections-rs") (v "0.3.0") (h "1p8ih89jgbwp11952kz56xpvfmsr1dc7pvxwz0xx267s8xvng19h")))

(define-public crate-collections-rs-0.3.1 (c (n "collections-rs") (v "0.3.1") (h "1lhzccb4xygd9mv4bca5f1340warxjl0vai8ck0x4a453nvqcrly")))

