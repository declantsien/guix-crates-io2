(define-module (crates-io co ll collide-sphere) #:use-module (crates-io))

(define-public crate-collide-sphere-0.1.0 (c (n "collide-sphere") (v "0.1.0") (d (list (d (n "collide") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vector-space") (r "^0.1.0") (d #t) (k 0)))) (h "0m5yylbx7fv270n8fagv8n3dfp4cdi8sqx8n6pk6grnl3f08hnnv")))

