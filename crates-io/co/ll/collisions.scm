(define-module (crates-io co ll collisions) #:use-module (crates-io))

(define-public crate-collisions-0.1.0 (c (n "collisions") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)))) (h "0zz0l0lk414c2pps2kmr5435mcky1x0ny0z8f4sz98nmhrfill91")))

(define-public crate-collisions-0.1.1 (c (n "collisions") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)))) (h "0a4548skbi8sm3lz88vc8h1hv1dcrhn8xbvxykhdybzji8avl4xg")))

(define-public crate-collisions-0.1.2 (c (n "collisions") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)))) (h "00rw6zy439awif593ah6ihlpya7ywlri0hyw544453dby3fhl5rb")))

(define-public crate-collisions-0.1.3 (c (n "collisions") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)))) (h "0jkdjbf9788wqyhgd1icsfcyk0sq37dwlv0pj7b1a9kgmxar97d1")))

