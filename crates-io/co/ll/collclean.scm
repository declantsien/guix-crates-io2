(define-module (crates-io co ll collclean) #:use-module (crates-io))

(define-public crate-collclean-0.1.0 (c (n "collclean") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)))) (h "0gg6zwpm32kzvc59yf6hg5rfx05q6rz0crnzf8cd1xvnxd87ag4z")))

(define-public crate-collclean-0.2.0 (c (n "collclean") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)))) (h "0s5xny4hzan99qknwa2flcr6m6p799z9m8nrj48ybd6xjsg0klvf")))

(define-public crate-collclean-0.2.1 (c (n "collclean") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)))) (h "1fyw4m0xdn97rkg53annz6svjh9n5wpnaac9izyjh9l3djpj3rd1")))

(define-public crate-collclean-0.3.0 (c (n "collclean") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0l4sx3gy8ix29qm8lb3946vr083hq9r1yw7zkq84jss958pisb3c")))

(define-public crate-collclean-0.4.0 (c (n "collclean") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0qd2ihlv3739n1s5griw5n6vk4qr2ghv178blc5hgypray2pig7i")))

(define-public crate-collclean-0.4.1 (c (n "collclean") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "08vjbfv7gm9p3axd9iy36x03vfxps5x77hkdxvw1ilyi8mq0vg37")))

