(define-module (crates-io co ll collectxyz) #:use-module (crates-io))

(define-public crate-collectxyz-0.1.0 (c (n "collectxyz") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw721") (r "=0.9.1") (d #t) (k 0)) (d (n "cw721-base") (r "=0.9.1") (f (quote ("library"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "0d1af0br7jw8nwivaysi4vd7nb79z0hl84m2ys6q53c47vl3viyn") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-collectxyz-0.2.1 (c (n "collectxyz") (v "0.2.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw721") (r "=0.9.1") (d #t) (k 0)) (d (n "cw721-base") (r "=0.9.1") (f (quote ("library"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "1a5m04sd8hczkl1vw41dp5lbnp9v05s4hrvj5ks3fzv0g922mj7h") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-collectxyz-0.2.2 (c (n "collectxyz") (v "0.2.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw721") (r "=0.9.1") (d #t) (k 0)) (d (n "cw721-base") (r "=0.9.1") (f (quote ("library"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)))) (h "1j6fxn0fmfkvqm64sv327rk5j7y16sk6qxnz0hp2gf1wny2p6m59") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

