(define-module (crates-io co ll collection_macros) #:use-module (crates-io))

(define-public crate-collection_macros-0.1.0 (c (n "collection_macros") (v "0.1.0") (h "0qqf848vf9s0js1lg185w5p5alig8fa3p94237sf2h0bfazg56qr") (f (quote (("nightly"))))))

(define-public crate-collection_macros-0.1.2 (c (n "collection_macros") (v "0.1.2") (h "0dm4032h02b92wik728b9vlvbi841kbgrd9km4ybcpfjs4sfqwwz") (f (quote (("nightly"))))))

(define-public crate-collection_macros-0.2.0 (c (n "collection_macros") (v "0.2.0") (h "0yl2zcqhmxg71xgs0k90b9p5lmpwnhrghn0nlr960c2ylzk81cah")))

