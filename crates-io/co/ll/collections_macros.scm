(define-module (crates-io co ll collections_macros) #:use-module (crates-io))

(define-public crate-collections_macros-0.1.0 (c (n "collections_macros") (v "0.1.0") (h "089gfy5xpkzz32z0z3ijansdsx6s87lyqwrp9064a4261gyliq3h")))

(define-public crate-collections_macros-1.0.1 (c (n "collections_macros") (v "1.0.1") (h "1as2hrvss0hhzk1ys1qxgbhs0z6q75dhz8pzn22k1c5h513jsxz1")))

