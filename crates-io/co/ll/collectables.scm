(define-module (crates-io co ll collectables) #:use-module (crates-io))

(define-public crate-collectables-2.0.0 (c (n "collectables") (v "2.0.0") (d (list (d (n "sixarm_assert") (r "^1") (d #t) (k 0)))) (h "0b7nlx9npfmn5m64002s1d01x5ab3d05kisjdp8w3kjky97aqj8c")))

(define-public crate-collectables-2.0.1 (c (n "collectables") (v "2.0.1") (d (list (d (n "assertables") (r "^3") (d #t) (k 0)))) (h "15k8p168wy2l563lskw4p1n172mabmzjxn1w9k3qagcjrh3wzjc4")))

