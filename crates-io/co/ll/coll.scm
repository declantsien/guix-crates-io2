(define-module (crates-io co ll coll) #:use-module (crates-io))

(define-public crate-coll-0.1.0 (c (n "coll") (v "0.1.0") (h "1gdhfc3x547lwvzjfax42l8il9yg463av8knmwkzjp9mjwrc11z2") (y #t)))

(define-public crate-coll-0.1.1 (c (n "coll") (v "0.1.1") (h "09hyyl7fsqfw9a28yhvvh457c88ywqj2ydzlv83xmaflnj0bbrwa") (y #t)))

(define-public crate-coll-0.1.2 (c (n "coll") (v "0.1.2") (h "0yq7159dd4vascm6crjlrxbgpyylzzlafv2wncrmcwbwfkkcs14i") (y #t)))

(define-public crate-coll-0.1.3 (c (n "coll") (v "0.1.3") (h "1ljm3f5nc4wp14vsajicidxa3arxv3mb5ixrmm8asxk0mq0sr88v") (y #t)))

