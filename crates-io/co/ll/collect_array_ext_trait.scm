(define-module (crates-io co ll collect_array_ext_trait) #:use-module (crates-io))

(define-public crate-collect_array_ext_trait-0.1.0 (c (n "collect_array_ext_trait") (v "0.1.0") (h "1myw5v5lvh4fpkawz3apmd6wj6rxzn2dc0srbfsgw49mihn4aj65")))

(define-public crate-collect_array_ext_trait-0.2.0 (c (n "collect_array_ext_trait") (v "0.2.0") (h "15zlpbgbpd0pvqqcji6b6lhzn1xsk06vbdxay3wsn4yy60rfi8hv")))

