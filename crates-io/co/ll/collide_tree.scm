(define-module (crates-io co ll collide_tree) #:use-module (crates-io))

(define-public crate-collide_tree-0.1.0 (c (n "collide_tree") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1z7pn6xinj5sgz2qy4mh4c70prd2ivybalg1qzcrn58frw1si8lb")))

(define-public crate-collide_tree-0.1.1 (c (n "collide_tree") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "16r1lq0sgnsbgdaxhq0vjkac7mji9xx75kid6a2jszcl7v8zrqjr")))

