(define-module (crates-io co ll colloquy) #:use-module (crates-io))

(define-public crate-colloquy-0.1.0 (c (n "colloquy") (v "0.1.0") (d (list (d (n "clap") (r ">= 2.33.0") (f (quote ("yaml" "color" "suggestions"))) (d #t) (k 0)) (d (n "notcurses") (r ">= 1.2.0") (d #t) (k 0)))) (h "01ya1j0sbi4cgga78y0swapw502y4q39fhc0gsa71gn2cbixq87c")))

(define-public crate-colloquy-0.1.1 (c (n "colloquy") (v "0.1.1") (d (list (d (n "clap") (r ">= 2.33.0") (f (quote ("yaml" "color" "suggestions"))) (k 0)) (d (n "libc") (r ">= 0.2.66") (d #t) (k 0)) (d (n "libnotcurses-sys") (r ">= 1.2.5") (d #t) (k 0)) (d (n "notcurses") (r ">= 1.2.5") (d #t) (k 0)))) (h "1yd6jzr0jk0b2kscbmhpwlis50qhfisa4i5ppy4p7ccxkv1xb12c")))

(define-public crate-colloquy-0.1.3 (c (n "colloquy") (v "0.1.3") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "clap") (r ">=2.33.0") (f (quote ("yaml" "color" "suggestions"))) (k 0)) (d (n "libc") (r ">=0.2.66") (d #t) (k 0)) (d (n "libnotcurses-sys") (r ">=1.5.1") (d #t) (k 0)) (d (n "notcurses") (r ">=1.5.1") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "0r9pyp6qqgd1nfk7aklqch2pxfzhs2r33yg1m132mnjr9da895bz")))

(define-public crate-colloquy-0.1.4 (c (n "colloquy") (v "0.1.4") (d (list (d (n "all_asserts") (r ">=1.0.0") (d #t) (k 2)) (d (n "clap") (r ">=2.33.0") (f (quote ("yaml" "color" "suggestions"))) (k 0)) (d (n "libc") (r ">=0.2.66") (d #t) (k 0)) (d (n "libnotcurses-sys") (r ">=1.6.6") (d #t) (k 0)) (d (n "notcurses") (r ">=1.6.6") (d #t) (k 0)) (d (n "serial_test") (r ">=0.4.0") (d #t) (k 2)) (d (n "serial_test_derive") (r ">=0.4.0") (d #t) (k 2)))) (h "1p0lx5x9klbsj8p2z90asmx1kbiqilhmzahlas783a2xrzm6i5a8")))

