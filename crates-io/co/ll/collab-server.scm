(define-module (crates-io co ll collab-server) #:use-module (crates-io))

(define-public crate-collab-server-0.0.7 (c (n "collab-server") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-compat") (r "^0.2") (d #t) (k 0)) (d (n "common") (r "^0.0.7") (f (quote ("__server"))) (d #t) (k 0) (p "collab-common")) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-rustls") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("macros" "net" "sync" "time"))) (d #t) (k 0)))) (h "19h5d8xaqsff2r6jm8s8hqhv2qq9w84i5cb1mziy5s0m0dqg9wpy") (f (quote (("__tests" "common/__tests"))))))

