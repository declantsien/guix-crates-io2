(define-module (crates-io co ll collections-more) #:use-module (crates-io))

(define-public crate-collections-more-0.1.0 (c (n "collections-more") (v "0.1.0") (h "0kh6zh45farxn3hipdmf55gd1404a3fdp5pink77ak9a7d5rl3dc")))

(define-public crate-collections-more-0.1.1 (c (n "collections-more") (v "0.1.1") (h "01vi0fgxnjwpm2gprk36lqjldkvad6201pf5vflvbwsqpr1xwxia")))

(define-public crate-collections-more-0.1.2 (c (n "collections-more") (v "0.1.2") (h "1lr0frdmgambz8fnhj2vfp7n3yfzvkw7hl9nqdnrv1scjz5ji332")))

(define-public crate-collections-more-0.1.3 (c (n "collections-more") (v "0.1.3") (h "1g8ym32l5i0yng5gccq4a9avy3ppfdlacwfy2ggn2s3i91jvly2j")))

(define-public crate-collections-more-0.1.4 (c (n "collections-more") (v "0.1.4") (h "1jclrmr2glc9k8h0d0kppiall05nbl9g62aqylbzzg9wvp0265ad")))

