(define-module (crates-io co ll collab) #:use-module (crates-io))

(define-public crate-collab-0.0.7 (c (n "collab") (v "0.0.7") (d (list (d (n "async-net") (r "^2.0") (d #t) (k 0)) (d (n "cola") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "common") (r "^0.0.7") (f (quote ("__client"))) (d #t) (k 0) (p "collab-common")) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-rustls") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("macros" "sync"))) (d #t) (k 0)))) (h "1b30pw03rd299vhkxyb9hxqj201rpfcbnxcy3kjifjvnlvfw4nb4") (f (quote (("__tests" "common/__tests"))))))

