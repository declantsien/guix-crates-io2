(define-module (crates-io co ll collision-detection) #:use-module (crates-io))

(define-public crate-collision-detection-0.2.0 (c (n "collision-detection") (v "0.2.0") (d (list (d (n "array-linked-list") (r "^0.1.0") (d #t) (k 0)) (d (n "collide") (r "^0.2") (d #t) (k 0)) (d (n "vector-space") (r "^0.3") (d #t) (k 0)))) (h "0m8i60hxq86rdlj6q6d0ii3gjcxcr1asqnqadfhhfs6aixsxyp7l")))

