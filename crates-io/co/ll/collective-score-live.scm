(define-module (crates-io co ll collective-score-live) #:use-module (crates-io))

(define-public crate-collective-score-live-0.0.7 (c (n "collective-score-live") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.25") (f (quote ("rustls-tls" "blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jiza61pgbzxrq91al7hrl0kdcz8dlw8989d7mq3dks2wy0wph5y")))

