(define-module (crates-io co ll collect) #:use-module (crates-io))

(define-public crate-collect-0.0.3 (c (n "collect") (v "0.0.3") (h "1j5ikx8gm09pc6w3n0327m8k9hbn407hiarah6bbwn7aybh9dj0g")))

(define-public crate-collect-0.0.4 (c (n "collect") (v "0.0.4") (d (list (d (n "traverse") (r "^0.0.1") (d #t) (k 0)))) (h "0yzjmqx25g1b0mjlnpi175mnsjj70fsdnv0j06b196f4kxmi3vw6")))

(define-public crate-collect-0.0.5 (c (n "collect") (v "0.0.5") (d (list (d (n "traverse") (r "^0.0.2") (d #t) (k 0)))) (h "1i1pymcimgl0y67dcg0mj2fiwak81bjrm632jx13ya8kxcbwbz6j")))

(define-public crate-collect-0.0.6 (c (n "collect") (v "0.0.6") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "0mfplhhynisfs2l5dihdaf5vchpi8ygkqcnbrffmady2vv54yx1p")))

(define-public crate-collect-0.0.7 (c (n "collect") (v "0.0.7") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "0ysb41dl104r6c1y49g2wxv23xgsm28s5p8cv0z9yv90v9a4yg4q")))

(define-public crate-collect-0.0.8 (c (n "collect") (v "0.0.8") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "02q31yr5va8rv6nv92k0n9aks1i5ppgpx78k33sx0yrnfmjxzavb")))

(define-public crate-collect-0.0.9 (c (n "collect") (v "0.0.9") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "0dinkbfmpwrlmlc3ysgxj61chyyf4kz272cxhablqp7ghl2n4a4s")))

(define-public crate-collect-0.0.10 (c (n "collect") (v "0.0.10") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "04s6dq84xlygwi1b96l7pxmhb07h3cw17aiircyq8x4whb40x32y")))

(define-public crate-collect-0.0.11 (c (n "collect") (v "0.0.11") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "0a3hsw211b3kxk6ck0i893bcy0w1z9rjlj9xx7m1l48j7l7jqabj")))

(define-public crate-collect-0.0.12 (c (n "collect") (v "0.0.12") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "1935z1ynbai233sgghzxfg7kn7d9r4h0ii9mn3jcwp7w8bfr056y")))

(define-public crate-collect-0.0.13 (c (n "collect") (v "0.0.13") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "103c14q9xla38v8hi4jg8fnvr8d4dv621bjpa4z3zflh540x19z3")))

(define-public crate-collect-0.0.14 (c (n "collect") (v "0.0.14") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "1jpchhya392rq49im172vslkvnkz02v9jp5pl3vi1rg3ramn005n")))

(define-public crate-collect-0.0.15 (c (n "collect") (v "0.0.15") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "0jb60pyi4xbyg985qhzlfd5qvcja3k3j5fd8hh0zzzn7yibdi1gx")))

(define-public crate-collect-0.0.16 (c (n "collect") (v "0.0.16") (d (list (d (n "traverse") (r "*") (d #t) (k 0)))) (h "0zswv6b39ysknd00qzv6rvm36023zr6czhnvzzqqaz56jgwakw7g")))

(define-public crate-collect-0.0.17 (c (n "collect") (v "0.0.17") (d (list (d (n "traverse") (r "*") (o #t) (d #t) (k 0)))) (h "0spkzda364bpxhn1k48x37cvw30pw0715hkra7fz342ijm8jgf98") (f (quote (("trie_map") ("tree_map" "compare") ("string_joiner") ("proto") ("ordered_iter") ("lru_cache") ("linked_hash_map") ("interval_heap" "compare") ("immut_slist") ("enum_set") ("default" "blist" "enum_set" "immut_slist" "interval_heap" "linked_hash_map" "lru_cache" "tree_map" "trie_map" "compare" "proto" "string_joiner" "ordered_iter") ("compare") ("blist" "proto"))))))

(define-public crate-collect-0.0.18 (c (n "collect") (v "0.0.18") (d (list (d (n "traverse") (r "*") (o #t) (d #t) (k 0)))) (h "17p1kgcj174msx8c3r4wwbp3w9jrsvrhc3z089rpxs3sv6n1p0yj") (f (quote (("trie_map") ("tree_map" "compare") ("string_joiner") ("proto") ("ordered_iter") ("lru_cache" "linked_hash_map") ("linked_hash_map") ("interval_heap" "compare") ("immut_slist") ("enum_set") ("default" "blist" "enum_set" "immut_slist" "interval_heap" "linked_hash_map" "lru_cache" "tree_map" "trie_map" "compare" "proto" "string_joiner" "ordered_iter") ("compare") ("blist" "proto"))))))

(define-public crate-collect-0.0.19 (c (n "collect") (v "0.0.19") (d (list (d (n "traverse") (r "*") (o #t) (d #t) (k 0)))) (h "0fv5ym0p0wifgv3wdrrna63p65187iq2wjjl8600a8acls4wpzgv") (f (quote (("trie_map") ("tree_map" "compare") ("string_joiner") ("proto") ("ordered_iter") ("lru_cache" "linked_hash_map") ("linked_hash_map") ("interval_heap" "compare") ("immut_slist") ("enum_set") ("default" "blist" "enum_set" "immut_slist" "interval_heap" "linked_hash_map" "lru_cache" "tree_map" "trie_map" "compare" "proto" "string_joiner" "ordered_iter") ("compare") ("blist" "proto"))))))

(define-public crate-collect-0.0.20 (c (n "collect") (v "0.0.20") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "traverse") (r "*") (o #t) (d #t) (k 0)))) (h "0xr9skg474myx335n36bf5a8dyf0l7qdwgxrvl6hcs9i7fg9qpr4") (f (quote (("trie_map") ("tree_map" "compare") ("string_joiner") ("proto") ("ordered_iter") ("lru_cache" "linked_hash_map") ("linked_hash_map") ("interval_heap" "compare") ("immut_slist") ("enum_set") ("default" "blist" "enum_set" "immut_slist" "interval_heap" "linked_hash_map" "lru_cache" "tree_map" "trie_map" "compare" "proto" "string_joiner" "ordered_iter") ("compare") ("blist" "proto"))))))

(define-public crate-collect-0.0.21 (c (n "collect") (v "0.0.21") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "traverse") (r "*") (o #t) (d #t) (k 0)))) (h "1d99ac3q93v1v7m52dnq4w8kwg2yfribq4gaq3m74kgf4dch6j44") (f (quote (("trie_map") ("tree_map" "compare") ("string_joiner") ("proto") ("ordered_iter") ("lru_cache" "linked_hash_map") ("linked_hash_map") ("interval_heap" "compare") ("immut_slist") ("enum_set") ("default" "blist" "enum_set" "immut_slist" "interval_heap" "linked_hash_map" "lru_cache" "tree_map" "trie_map" "compare" "proto" "string_joiner" "ordered_iter") ("compare") ("blist" "proto"))))))

(define-public crate-collect-0.0.22 (c (n "collect") (v "0.0.22") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "traverse") (r "*") (o #t) (d #t) (k 0)))) (h "14kg5a7zbmbgd87hvi2p5pc9sqqm8r1llpvpfhaly30szs7g0by8") (f (quote (("trie_map") ("tree_map" "compare") ("string_joiner") ("proto") ("ordered_iter") ("lru_cache" "linked_hash_map") ("linked_hash_map") ("interval_heap" "compare") ("immut_slist") ("enum_set") ("default" "blist" "enum_set" "immut_slist" "interval_heap" "linked_hash_map" "lru_cache" "tree_map" "trie_map" "compare" "proto" "string_joiner" "ordered_iter") ("compare") ("blist" "proto"))))))

(define-public crate-collect-0.0.23 (c (n "collect") (v "0.0.23") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "traverse") (r "*") (o #t) (d #t) (k 0)))) (h "0dpsc7wwv5x4spwy5kbkhizn1vg1sm38a78sk850qgghcdb6r1r8") (f (quote (("trie_map") ("tree_map" "compare") ("string_joiner") ("proto") ("ordered_iter") ("lru_cache" "linked_hash_map") ("linked_hash_map") ("interval_heap" "compare") ("immut_slist") ("enum_set") ("default" "blist" "enum_set" "immut_slist" "interval_heap" "linked_hash_map" "lru_cache" "tree_map" "trie_map" "compare" "proto" "string_joiner" "ordered_iter") ("compare") ("blist" "proto"))))))

(define-public crate-collect-0.0.24 (c (n "collect") (v "0.0.24") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "threadpool") (r "*") (d #t) (k 2)) (d (n "traverse") (r "*") (o #t) (d #t) (k 0)))) (h "175jckjkbrh749kxxchgfgf8ihkbv1m0kwb6q5bgmcm3z8sld2as") (f (quote (("trie_map") ("tree_map" "compare") ("string_joiner") ("proto") ("ordered_iter") ("lru_cache" "linked_hash_map") ("linked_hash_map") ("interval_heap" "compare") ("immut_slist") ("enum_set") ("default" "blist" "enum_set" "immut_slist" "interval_heap" "linked_hash_map" "lru_cache" "tree_map" "trie_map" "compare" "proto" "string_joiner" "ordered_iter") ("compare") ("blist" "proto"))))))

(define-public crate-collect-0.0.25 (c (n "collect") (v "0.0.25") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "threadpool") (r "*") (d #t) (k 2)) (d (n "traverse") (r "*") (o #t) (d #t) (k 0)))) (h "1sk8lakp7nb8l1brix5c72p4dwba85p0pbjvvnlgjdvxb5rd89m4") (f (quote (("trie_map") ("tree_map" "compare") ("string_joiner") ("proto") ("ordered_iter") ("lru_cache" "linked_hash_map") ("linked_hash_map") ("interval_heap" "compare") ("immut_slist") ("enum_set") ("default" "blist" "enum_set" "immut_slist" "interval_heap" "linked_hash_map" "lru_cache" "tree_map" "trie_map" "compare" "proto" "string_joiner" "ordered_iter") ("compare") ("blist" "proto"))))))

(define-public crate-collect-0.0.26 (c (n "collect") (v "0.0.26") (d (list (d (n "compare") (r "*") (o #t) (d #t) (k 0)) (d (n "ordered_iter") (r "*") (o #t) (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "threadpool") (r "*") (d #t) (k 2)) (d (n "traverse") (r "*") (o #t) (d #t) (k 0)))) (h "146j6w0z4wns5aq242mqb4mgpi40sia49qdsx6kbiaajf6drml84") (f (quote (("tree_map" "compare") ("string_joiner") ("proto") ("lru_cache" "linked_hash_map") ("linked_hash_map") ("interval_heap" "compare") ("immut_slist") ("enum_set") ("default" "blist" "enum_set" "immut_slist" "interval_heap" "linked_hash_map" "lru_cache" "tree_map" "proto" "string_joiner" "ordered_iter") ("blist" "proto" "traverse"))))))

