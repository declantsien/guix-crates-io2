(define-module (crates-io co ll collection) #:use-module (crates-io))

(define-public crate-collection-0.1.0 (c (n "collection") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "seahash") (r "^3.0.4") (d #t) (k 0)) (d (n "trait-group") (r "^0.1.0") (d #t) (k 0)))) (h "0mng51i57icl1rg89c7x6libnrwc70q0550iij27slfm3ng7v6a8")))

(define-public crate-collection-0.1.1 (c (n "collection") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "seahash") (r "^3.0.4") (d #t) (k 0)) (d (n "trait-group") (r "^0.1.0") (d #t) (k 0)))) (h "0nw94mc71ad8na32xyqq427vjp0l9wh34yrlpgwwrkahgcgb1b9q")))

