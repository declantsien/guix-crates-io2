(define-module (crates-io co ll collenchyma-blas) #:use-module (crates-io))

(define-public crate-collenchyma-blas-0.0.1 (c (n "collenchyma-blas") (v "0.0.1") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)))) (h "1r1icxqd45rypmg5pc2l7kkfxxa1s9cyf1kxlzjzfrsc9pz8qwrc") (f (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-collenchyma-blas-0.1.0 (c (n "collenchyma-blas") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.6") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rblas") (r "^0.0.11") (o #t) (d #t) (k 0)))) (h "0xcyja3mfw6jgp2nz1qcg94fxzydgmi09j9q35qrv103yv7xdfwg") (f (quote (("travis" "native") ("opencl" "collenchyma/opencl") ("native" "collenchyma/native" "rblas") ("lint" "clippy") ("dev") ("default" "native" "cuda" "opencl") ("cuda" "collenchyma/cuda"))))))

(define-public crate-collenchyma-blas-0.2.0 (c (n "collenchyma-blas") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)) (d (n "collenchyma") (r "^0.0.8") (k 0)) (d (n "cublas") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rblas") (r "^0.0.11") (o #t) (d #t) (k 0)))) (h "1x1nk7qsx6aq1phmgdfx1mxkxa82lp6ak0ryggm1gyply2sb7xcn") (f (quote (("travis" "native") ("opencl" "collenchyma/opencl") ("native" "collenchyma/native" "rblas") ("lint" "clippy") ("dev") ("default" "native" "cuda" "opencl") ("cuda" "collenchyma/cuda" "cublas"))))))

