(define-module (crates-io co ll collide) #:use-module (crates-io))

(define-public crate-collide-0.1.0 (c (n "collide") (v "0.1.0") (d (list (d (n "vector-space") (r "^0.1.0") (d #t) (k 0)))) (h "1fkj4wslsd35sr4mfhyymwh5kmvpd6n7dl8z3kz7dv0sx1fnkq27")))

(define-public crate-collide-0.2.0 (c (n "collide") (v "0.2.0") (d (list (d (n "vector-space") (r "^0.3") (d #t) (k 0)))) (h "0nfgc6ryaqf7wbblrgkg91ddpmkbvlrbhh4hwwqv5mmf9yxc2mfc")))

