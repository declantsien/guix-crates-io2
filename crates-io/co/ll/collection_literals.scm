(define-module (crates-io co ll collection_literals) #:use-module (crates-io))

(define-public crate-collection_literals-1.0.0 (c (n "collection_literals") (v "1.0.0") (h "0xzsaf2xrsl5k8p1acxk0055xfm7zcky078l22ds9hpnch8hdmar") (y #t)))

(define-public crate-collection_literals-1.0.0-1 (c (n "collection_literals") (v "1.0.0-1") (h "0b10hf6wrbizviq7449hkf5b3kbgmq7k74m56sl4j71yrwwdz1nn") (y #t)))

(define-public crate-collection_literals-1.0.0-2 (c (n "collection_literals") (v "1.0.0-2") (h "1vq025p4brmsnyivmwry7gfzik90fnvv0xqavixw9c6idavyi3vl") (y #t)))

(define-public crate-collection_literals-1.0.0-rc1 (c (n "collection_literals") (v "1.0.0-rc1") (h "1irhjcjh6ijx58rxn3zjpf86dqg34qbxfqcq3k2hnvf6mmxl8c9s") (y #t)))

(define-public crate-collection_literals-1.0.1 (c (n "collection_literals") (v "1.0.1") (h "0wg2q6vgjpcxpg60drwn480zqq7wf0wz1i223isxwrkp6sccwv8q")))

