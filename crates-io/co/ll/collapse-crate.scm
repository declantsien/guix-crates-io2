(define-module (crates-io co ll collapse-crate) #:use-module (crates-io))

(define-public crate-collapse-crate-0.1.0 (c (n "collapse-crate") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0lnr80yz2718z1wc4j2pjfmdas8dsx6izkbaw0vbrd381asw0617")))

