(define-module (crates-io co ll collate) #:use-module (crates-io))

(define-public crate-collate-0.1.0 (c (n "collate") (v "0.1.0") (h "04zi5kbn89cprvbxizclcq5x8kw987g45qz4zhlwfdh5r8i0fnl3")))

(define-public crate-collate-0.1.1 (c (n "collate") (v "0.1.1") (h "19hsylc7f09h6vbn56gd83fg5b39wwbcmp42d04kq9n3i9jg65l9")))

(define-public crate-collate-0.1.2 (c (n "collate") (v "0.1.2") (h "0wns3qh6d907p6dhrhhcqhs37vx1lz7m366q187xdra308mjp1my")))

(define-public crate-collate-0.1.3 (c (n "collate") (v "0.1.3") (h "00q84rvmzj638qzy0934p2a4a9b1vhhhbfh6v1y0r35cda9n0b0c")))

(define-public crate-collate-0.1.4 (c (n "collate") (v "0.1.4") (h "0dbsfj44fy0mvw6zq22jrxnlnivgr3aibda18bhzh6328640fjja")))

(define-public crate-collate-0.1.5 (c (n "collate") (v "0.1.5") (h "0mzcds9ynfkcmhlvx9nl9cjkg4nz9z4pdkmqah4hyli80d7acz95")))

(define-public crate-collate-0.1.6 (c (n "collate") (v "0.1.6") (h "054az14ddbn69hifdfa4vbgrpaas4xxsqqwy80yxzd8xsb19jdvv")))

(define-public crate-collate-0.1.7 (c (n "collate") (v "0.1.7") (h "1lfxwn6zvfng8nxh3pahi249lqw9lrf8bqczp0zppan6wrpr0l6b")))

(define-public crate-collate-0.1.8 (c (n "collate") (v "0.1.8") (h "0nawwq8385a397j96zifb0mmd9w8dam1z5h4zvz2rzql7qgn3lh7")))

(define-public crate-collate-0.1.9 (c (n "collate") (v "0.1.9") (h "16vqjpzbdxiv8481xvgylljdfsl6r7pync2b7b46lchdv48q0aca")))

(define-public crate-collate-0.1.10 (c (n "collate") (v "0.1.10") (h "0jpqk3di25q2ib51hfih0yqaycw99jw9p9svq1m8fjgrwsh1k5wn")))

(define-public crate-collate-0.2.0 (c (n "collate") (v "0.2.0") (d (list (d (n "num-complex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "00gssagh5m34vpkh5vf9yyyzd8pbnzq4dvawj08k2if1h6h38bkb") (f (quote (("complex" "num-complex")))) (y #t)))

(define-public crate-collate-0.2.1 (c (n "collate") (v "0.2.1") (d (list (d (n "num-complex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "12dp57k64p9czaspv5r4z6p8522rsmvvgyqy70g2xg059q4gvnh0") (f (quote (("complex" "num-complex"))))))

(define-public crate-collate-0.2.2 (c (n "collate") (v "0.2.2") (d (list (d (n "num-complex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0b81skj79i62nwsycpayz0zlcyf2lamd9cn5z8gijhnlp78z8lmh") (f (quote (("complex" "num-complex"))))))

(define-public crate-collate-0.2.3 (c (n "collate") (v "0.2.3") (d (list (d (n "num-complex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1jggrlvnaj7nnzgif0ljsrp5xx9frr7nyay23432n7kq3zbcqyfa") (f (quote (("complex" "num-complex"))))))

(define-public crate-collate-0.2.4 (c (n "collate") (v "0.2.4") (d (list (d (n "num-complex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1fr5kvmhcgb2rl2k9448hypzpbidpgmhqysszl7k7ykk5zsiwl6s") (f (quote (("complex" "num-complex"))))))

(define-public crate-collate-0.3.0 (c (n "collate") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.26") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1zqx7fa9ydpb4sb89gg4cldx887bxwfb7vj0rf81yzxq152ndg9c") (f (quote (("stream" "futures" "pin-project"))))))

(define-public crate-collate-0.4.0 (c (n "collate") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "06gb11cma8kcc68ryqinxvp4mgj7fk6q194qip55j2b5c2a7s3ay") (f (quote (("stream" "futures" "pin-project"))))))

(define-public crate-collate-0.4.1 (c (n "collate") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "19wgvd19va8d8jrsbmpz2hwriy3gmq7966brr2q2xff17y1w4wd6") (f (quote (("stream" "futures" "pin-project"))))))

