(define-module (crates-io co ll collatz_conjecture) #:use-module (crates-io))

(define-public crate-collatz_conjecture-0.1.0 (c (n "collatz_conjecture") (v "0.1.0") (d (list (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "1yiz1wm8b0fisf1hiz10lj0szfya7via0kr81h4s8gq7vl3wxxd5")))

(define-public crate-collatz_conjecture-0.1.1 (c (n "collatz_conjecture") (v "0.1.1") (d (list (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "0h383sp3k8mfb7zfkplczayyi1bbhhm9cdgrz8lyy6q94s51pxpm")))

(define-public crate-collatz_conjecture-0.1.2 (c (n "collatz_conjecture") (v "0.1.2") (d (list (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "0j2g6ni5mqp4ynkx4k110xg5zzhia7jhdimsw9mvlhrzqskalkmz")))

