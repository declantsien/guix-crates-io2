(define-module (crates-io co ll collidea) #:use-module (crates-io))

(define-public crate-collidea-0.1.0 (c (n "collidea") (v "0.1.0") (d (list (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "01l0s0hw9zk6ryskzm07bbl9nsd7ylp1wf6a3fc60r6mz72myslh")))

(define-public crate-collidea-0.1.1 (c (n "collidea") (v "0.1.1") (d (list (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "1jbh8ibzfhadys2fjg3j7lrs9pfzhcighh7s57cal876faa0s8p8")))

(define-public crate-collidea-0.1.2 (c (n "collidea") (v "0.1.2") (d (list (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "0wih698562pa1x89xlgp847247i490m7pwyvkah6nd5ndr5lcvnr")))

