(define-module (crates-io co ll collectfiles) #:use-module (crates-io))

(define-public crate-collectfiles-1.0.0 (c (n "collectfiles") (v "1.0.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1l7y9p102g65xlcz0kli117pg9n3naknwb4p3x32vpfxgdz434y1")))

(define-public crate-collectfiles-1.1.0 (c (n "collectfiles") (v "1.1.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "10rz633ra5h5wckfvlwkbl2kp2ssj2yf9nmja2aai22gahslsfqn")))

