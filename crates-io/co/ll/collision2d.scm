(define-module (crates-io co ll collision2d) #:use-module (crates-io))

(define-public crate-collision2d-0.1.0 (c (n "collision2d") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.8") (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "0348j2lj7lvayyk1grl8shbjjj5v5gpq490705xl07lwi851bihw") (f (quote (("std") ("default" "std") ("aabb")))) (s 2) (e (quote (("libm" "dep:libm")))) (r "1.74")))

(define-public crate-collision2d-0.1.1 (c (n "collision2d") (v "0.1.1") (d (list (d (n "libm") (r "^0.2.8") (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "0fiv1hqh9j1ghhl1v5iza24sbgwjwdyqiv544fkf81g4qb94qci4") (f (quote (("std") ("default" "std") ("aabb")))) (s 2) (e (quote (("libm" "dep:libm")))) (r "1.74")))

(define-public crate-collision2d-0.1.2 (c (n "collision2d") (v "0.1.2") (d (list (d (n "libm") (r "^0.2.8") (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "091phsd4i9dnjpcpk386a7j0r3m78ny1v5ifmnrhk5frx7c7n6sq") (f (quote (("std") ("default" "std") ("aabb")))) (s 2) (e (quote (("libm" "dep:libm")))) (r "1.74")))

(define-public crate-collision2d-0.2.0 (c (n "collision2d") (v "0.2.0") (d (list (d (n "libm") (r "^0.2.8") (o #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "11zv61wl7mvvda57gp34n0wk6c3lvyiy3sgwz74zvdhdl8hsy52b") (f (quote (("std") ("default" "std") ("aabb")))) (s 2) (e (quote (("libm" "dep:libm")))) (r "1.74")))

