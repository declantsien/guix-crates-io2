(define-module (crates-io co ll collectable) #:use-module (crates-io))

(define-public crate-collectable-0.0.1 (c (n "collectable") (v "0.0.1") (h "0fzf64ifbdq5ng8gr9m504rr985011vq2j48yhkjglzc74g7bzgw") (f (quote (("alloc"))))))

(define-public crate-collectable-0.0.2 (c (n "collectable") (v "0.0.2") (h "0np47fw24sjq9r1hdw3xpcbnzh04ij6k1m3x7kjh35i0mnxdvaq8") (f (quote (("alloc"))))))

