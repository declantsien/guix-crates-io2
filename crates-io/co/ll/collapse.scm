(define-module (crates-io co ll collapse) #:use-module (crates-io))

(define-public crate-collapse-0.1.0 (c (n "collapse") (v "0.1.0") (h "1660kbq3g02zy5zpwfalfb7bzb5fygfliiz8kp9y5ppmwhgwbhyv")))

(define-public crate-collapse-0.1.1 (c (n "collapse") (v "0.1.1") (h "1c559zks0daxlrvyzr33j18sfwldk3xi4rcbrnp0bwdnrqq2p12d")))

(define-public crate-collapse-0.1.2 (c (n "collapse") (v "0.1.2") (h "0nhkrkgpsnii5yvgb76515g9rp14nxizaqsvsz46sfvcshydca5s")))

