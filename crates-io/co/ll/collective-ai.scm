(define-module (crates-io co ll collective-ai) #:use-module (crates-io))

(define-public crate-collective-ai-0.1.0 (c (n "collective-ai") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-openai") (r "^1.0.1") (d #t) (k 0)))) (h "1r3kdshmaw13kmj251cviflkyad1p61zcgcjr56x1a3lk4iy5zmh")))

