(define-module (crates-io co ll collective-utils) #:use-module (crates-io))

(define-public crate-collective-utils-1.0.0 (c (n "collective-utils") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.12") (d #t) (k 0)))) (h "1ckfa5f519a2zsypwg95kv9f90lchxyk3msfg49pq142ayx22881")))

