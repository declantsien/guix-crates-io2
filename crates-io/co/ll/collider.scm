(define-module (crates-io co ll collider) #:use-module (crates-io))

(define-public crate-collider-0.1.0 (c (n "collider") (v "0.1.0") (h "0gc76vi5iz21hwfsym9j351gcpn99fcla51hrm2501sns4if7xhz")))

(define-public crate-collider-0.1.1 (c (n "collider") (v "0.1.1") (h "11bmgcii3gmvlf5xsnghnhb1c8xcp714a3srxx7cl0sgagmnzyyp")))

(define-public crate-collider-0.1.2 (c (n "collider") (v "0.1.2") (d (list (d (n "noisy_float") (r "^0.1.1") (d #t) (k 0)))) (h "0dxn88ggg2b0v3j61dah2qzi2w9b3nwaxsvs092558ha7dgvgsdx") (y #t)))

(define-public crate-collider-0.1.3 (c (n "collider") (v "0.1.3") (d (list (d (n "noisy_float") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0yw0gdkwnmc1wahs6wzpi3ygqvvmkgj3psz63shk95bj7s6y6w7w") (f (quote (("noisy-floats" "noisy_float"))))))

(define-public crate-collider-0.1.4 (c (n "collider") (v "0.1.4") (d (list (d (n "noisy_float") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1cv4shwxnrifc8cabz9zgfda05kbkqx4w1xnzxjz6mjimdhv3alv") (f (quote (("noisy-floats" "noisy_float"))))))

(define-public crate-collider-0.2.0 (c (n "collider") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "01c9s1fsz30brf1dzxblh0gwh1gcd5aar8pbgd7p5w5c2kz8dlz5") (f (quote (("noisy-floats" "noisy_float"))))))

(define-public crate-collider-0.2.1 (c (n "collider") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1zjhsh6rj3bw3506phxq12d2q1jc96snv20vwpqk21n51vwpkr1v") (f (quote (("noisy-floats" "noisy_float"))))))

(define-public crate-collider-0.3.0 (c (n "collider") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)))) (h "0yqdakmyb5cj86sj4j7rj3ddsckwqnx451mvkvbg4mky4cy14jmg")))

(define-public crate-collider-0.3.1 (c (n "collider") (v "0.3.1") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)))) (h "06kf0qfwzpkj5b0xhw3jzzc9ia8z7pa41bimwcawmagwkq9n455s")))

