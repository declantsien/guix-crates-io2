(define-module (crates-io co ll collatz_length) #:use-module (crates-io))

(define-public crate-collatz_length-0.1.0 (c (n "collatz_length") (v "0.1.0") (h "1qfwq19za6yh57b5ryfrwgbak3dyfih7hjv4by3b6ryx4mcmrfy3")))

(define-public crate-collatz_length-0.1.1 (c (n "collatz_length") (v "0.1.1") (h "1vri6y185aazwkv04xl4hcwrzsh3w9184s9bza77y6v15j8lb0my")))

