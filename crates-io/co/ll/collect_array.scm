(define-module (crates-io co ll collect_array) #:use-module (crates-io))

(define-public crate-collect_array-0.1.0 (c (n "collect_array") (v "0.1.0") (h "0rrf6fha2kr0554vy5qwd6fpi3am6gc2j6lsx0hbb3wr1dc6f4h5")))

(define-public crate-collect_array-0.1.1 (c (n "collect_array") (v "0.1.1") (h "1wzf9lx5rfjfim0m85x29csbqw280mz74hifnqmmbfjh4axxy1wl")))

(define-public crate-collect_array-0.1.2 (c (n "collect_array") (v "0.1.2") (h "11z8qv7qxl1vyvs8r7sy490piiwh5zz0kb0z9b4sk38g4qir8ysa")))

(define-public crate-collect_array-0.1.3 (c (n "collect_array") (v "0.1.3") (h "1c8a203i08cshks3bwpvbrj6kqclf6w5hf9q12x90mzahwx317k5")))

