(define-module (crates-io co ll collatz) #:use-module (crates-io))

(define-public crate-collatz-0.1.0 (c (n "collatz") (v "0.1.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0zjx25y2rqyd7gi811q4x0yg0i2fidb698yysb0ysxx3cl5zpzgr")))

(define-public crate-collatz-0.1.1 (c (n "collatz") (v "0.1.1") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1580n4i1ma74z309v3x5ksn9qmnjdf5bz4jpgi9xaw7d2xyzb451")))

(define-public crate-collatz-0.2.0 (c (n "collatz") (v "0.2.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0vw6hh82dpaxfc6svb659chpc00kcq1njdc2w9x1d9ywjqsd3vn7")))

(define-public crate-collatz-0.3.0 (c (n "collatz") (v "0.3.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0gk6illngi3n5pnlnqjd1bh0dwd11qxjiz32l5k5hw1iy8br5m9i")))

(define-public crate-collatz-0.3.1 (c (n "collatz") (v "0.3.1") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "textplots") (r "^0.8.0") (d #t) (k 2)))) (h "074873hy0b0fjz892grq0qp2dqs4ynmrksx442rxjjfpkkyy8zh9")))

(define-public crate-collatz-0.3.2 (c (n "collatz") (v "0.3.2") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "textplots") (r "^0.8.0") (d #t) (k 2)))) (h "1x469k1vdym9drzlv57h7g2aqp4cm9k3pwnilrwngndbs65wvk3l")))

(define-public crate-collatz-0.4.0 (c (n "collatz") (v "0.4.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "textplots") (r "^0.8") (d #t) (k 2)))) (h "0hd0acmgz8f2dvbrxbxg5pkjck1zg8fzl3g4la65xb8p5iycz65x")))

(define-public crate-collatz-0.5.0 (c (n "collatz") (v "0.5.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)) (d (n "textplots") (r "^0.8") (d #t) (k 2)))) (h "119df6ksrnscrk156b77lqapidxz43520cv4p13r6bwhbcb4lf2f")))

(define-public crate-collatz-0.5.1 (c (n "collatz") (v "0.5.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "textplots") (r "^0.8") (d #t) (k 2)))) (h "0fhy4h3nxbdxy43y5wbalack05arvl2fcq8dvddjblbcq9kn9h49")))

