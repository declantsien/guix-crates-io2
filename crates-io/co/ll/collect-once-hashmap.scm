(define-module (crates-io co ll collect-once-hashmap) #:use-module (crates-io))

(define-public crate-collect-once-hashmap-0.1.0 (c (n "collect-once-hashmap") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0syh0h1h45bms415r9l6jc4sf32pxqf123470qvfna4cvbylvjpb")))

(define-public crate-collect-once-hashmap-0.2.0 (c (n "collect-once-hashmap") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bdkik50cwgn0mkyq3wi3nd3svg4vfwy78myxg0lqcpamkliymc1")))

