(define-module (crates-io co ll collection_traits) #:use-module (crates-io))

(define-public crate-collection_traits-0.0.0 (c (n "collection_traits") (v "0.0.0") (h "0v2akmk4qfngm7p8zwrv2ii37a23r8nxgm8p4icy54xlz74g2ipk")))

(define-public crate-collection_traits-0.0.1 (c (n "collection_traits") (v "0.0.1") (d (list (d (n "smallvec") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (o #t) (d #t) (k 0)))) (h "15amy6mqfym3i2j7slzc7dg570i8dppgjp45gnz76kbvc64qqvs6")))

(define-public crate-collection_traits-0.0.2 (c (n "collection_traits") (v "0.0.2") (d (list (d (n "smallvec") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1vfkx8bry1qybj20q3vnz18v5fgdz6pgwvadpzaxn5p2npkyk6p0")))

