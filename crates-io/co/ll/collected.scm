(define-module (crates-io co ll collected) #:use-module (crates-io))

(define-public crate-collected-0.1.0 (c (n "collected") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "1as6az6l6zaj81vhc9w3l6si3yx945xfb2lnbgqx3zm7by30gmrq")))

(define-public crate-collected-0.1.1 (c (n "collected") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "106w4ixngljqpjciynmb8c07938f302g88d764bfrkj2s1wqbd6k")))

(define-public crate-collected-0.1.2 (c (n "collected") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "1cwvzgvvpvv17qnryz7pny3qcl8k2ckbnlc3hjzd7lk3w46x7d9y")))

(define-public crate-collected-0.2.0 (c (n "collected") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1044hm5hdgj0v90r68a83vsxqwjk1wr0xng8dyz9dc6w2b0jfns3")))

(define-public crate-collected-0.3.0 (c (n "collected") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.6.2") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "min-max-heap") (r "^1.3.0") (d #t) (k 0)))) (h "197mwiv7hz32axsnc69j21g8qldw70hbksrdv2sdxvv50yg2n310") (f (quote (("unstable") ("default" "indexmap"))))))

(define-public crate-collected-0.4.0 (c (n "collected") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "min-max-heap") (r "^1.3.0") (d #t) (k 0)))) (h "0fyrmssvpzn73a0fi5dvcmpjvvvpq8khmg57g5vnmfhgq1vmvnm8") (f (quote (("unstable") ("default" "indexmap"))))))

