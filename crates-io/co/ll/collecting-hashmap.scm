(define-module (crates-io co ll collecting-hashmap) #:use-module (crates-io))

(define-public crate-collecting-hashmap-0.1.0 (c (n "collecting-hashmap") (v "0.1.0") (h "0p4r6i3klv6n85xihmflrfabjk46aqdvsxw4385k41bfidadjwk4")))

(define-public crate-collecting-hashmap-0.2.0 (c (n "collecting-hashmap") (v "0.2.0") (h "1acfq36gpy6v64538kl62sxd0ky0fyr0w9gljd9qm4hk945iyd26")))

