(define-module (crates-io co t_ cot_publisher) #:use-module (crates-io))

(define-public crate-cot_publisher-1.0.0-rc.1 (c (n "cot_publisher") (v "1.0.0-rc.1") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "varint-rs") (r "^2.2") (d #t) (k 0)))) (h "0vdqlfkbh4lmk4d99y81hqqa8zwlxjzysbyjfci29a43wb0ws597")))

