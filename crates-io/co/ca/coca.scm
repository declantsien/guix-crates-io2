(define-module (crates-io co ca coca) #:use-module (crates-io))

(define-public crate-coca-0.1.0 (c (n "coca") (v "0.1.0") (h "0dh3sa1qq4nzaiml343pmhnmfvmg3k9h7qxgpjrd11ljn1pin7r4") (f (quote (("profile") ("nightly") ("default") ("alloc"))))))

(define-public crate-coca-0.2.0 (c (n "coca") (v "0.2.0") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)))) (h "1rczr4f3cj1ymm13aqwsx3p979szbc5arcawsgx11466ad1zba4g") (f (quote (("profile") ("nightly") ("default") ("alloc"))))))

(define-public crate-coca-0.3.0 (c (n "coca") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1") (k 2)))) (h "048ygpagm2y74cdmmavkz1w5wxyy7y0zhvzpkjshkaz0kqvj6dgb") (f (quote (("unstable") ("profile") ("default") ("alloc"))))))

