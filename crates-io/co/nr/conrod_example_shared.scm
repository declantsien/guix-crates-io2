(define-module (crates-io co nr conrod_example_shared) #:use-module (crates-io))

(define-public crate-conrod_example_shared-0.62.0 (c (n "conrod_example_shared") (v "0.62.0") (d (list (d (n "conrod_core") (r "^0.62") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1wsq4a444nq0828l6r5dss2a6ilmx1fwzvnjk063xrxza1ghzfmi")))

(define-public crate-conrod_example_shared-0.63.0 (c (n "conrod_example_shared") (v "0.63.0") (d (list (d (n "conrod_core") (r "^0.63") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1w91vgvfnrzvc5d7hhxmzcixhlkkk94xywklr4cv0211ywr9r04v")))

(define-public crate-conrod_example_shared-0.64.0 (c (n "conrod_example_shared") (v "0.64.0") (d (list (d (n "conrod_core") (r "^0.64") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0xlp24bjh51x5zxnbwhwqwczp3d5q1y6fkvx4kj6g47sfjf15h9h")))

(define-public crate-conrod_example_shared-0.65.0 (c (n "conrod_example_shared") (v "0.65.0") (d (list (d (n "conrod_core") (r "^0.65") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1n0xvvvb5d8hyzv9f40bdsjmlf29gh8fxa12s0v9lpr6s40zygn7")))

(define-public crate-conrod_example_shared-0.66.0 (c (n "conrod_example_shared") (v "0.66.0") (d (list (d (n "conrod_core") (r "^0.66") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1yvxfsmpiz1lqsqf4d583n734484g94bwd61pj5512q8agd9azhw")))

(define-public crate-conrod_example_shared-0.67.0 (c (n "conrod_example_shared") (v "0.67.0") (d (list (d (n "conrod_core") (r "^0.67") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "18fh8gb1kxwbyr2ksi5dsh2zpblr6rvc7f7wd0nc1psi2r2whzz8")))

(define-public crate-conrod_example_shared-0.68.0 (c (n "conrod_example_shared") (v "0.68.0") (d (list (d (n "conrod_core") (r "^0.68") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1ax2ckm01xw4h5qr11zkq66yh83ai0yw6hmi2b7xcy540ip1b3r0")))

(define-public crate-conrod_example_shared-0.69.0 (c (n "conrod_example_shared") (v "0.69.0") (d (list (d (n "conrod_core") (r "^0.69") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1xckh7kkkg684pr0r1kwpg4gh5kg2xdk0grabhn0qm517j11zc22")))

(define-public crate-conrod_example_shared-0.70.0 (c (n "conrod_example_shared") (v "0.70.0") (d (list (d (n "conrod_core") (r "^0.70") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0c6j0ss8zaisdg6rb15ckpa6d2q0rqacd1b4m2anbajhjxbwcl30")))

(define-public crate-conrod_example_shared-0.71.0 (c (n "conrod_example_shared") (v "0.71.0") (d (list (d (n "conrod_core") (r "^0.71") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1ch5idac7rvxlg49zvjir675vhb5sa3rcjjn8fzdilx9qwkyk7dc")))

(define-public crate-conrod_example_shared-0.72.0 (c (n "conrod_example_shared") (v "0.72.0") (d (list (d (n "conrod_core") (r "^0.72") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "03idwr473an0rh10bcmqhrlx9h8d36l72viz36zzfflnjw11iv4q")))

(define-public crate-conrod_example_shared-0.73.0 (c (n "conrod_example_shared") (v "0.73.0") (d (list (d (n "conrod_core") (r "^0.73") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0hf2i0ys0ib7j1ij7qgwx21rkpy77ckvk5mh5w9f2nlp55gqlnfb")))

(define-public crate-conrod_example_shared-0.74.0 (c (n "conrod_example_shared") (v "0.74.0") (d (list (d (n "conrod_core") (r "^0.74") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "130ql81gpz38by0i1frpwscnxx8pri03qs9x11p906l91fh310nq")))

(define-public crate-conrod_example_shared-0.75.0 (c (n "conrod_example_shared") (v "0.75.0") (d (list (d (n "conrod_core") (r "^0.75") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1pcjsd4qrpnzs9cv8m748hpgh71qawwcqjs2nikx09l3g3d4fh67")))

(define-public crate-conrod_example_shared-0.76.0 (c (n "conrod_example_shared") (v "0.76.0") (d (list (d (n "conrod_core") (r "^0.76") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "065j7d8kmld7192yd9n6ny18f34pqrb2mspxbmf5r363mddx96qh")))

(define-public crate-conrod_example_shared-0.76.1 (c (n "conrod_example_shared") (v "0.76.1") (d (list (d (n "conrod_core") (r "^0.76") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0xh126ffc3adi1zsc3cgxplw9c5z5b590x0jysd941vw3hcsz0nc")))

