(define-module (crates-io co nr conrod_prompt) #:use-module (crates-io))

(define-public crate-conrod_prompt-0.1.0 (c (n "conrod_prompt") (v "0.1.0") (d (list (d (n "conrod_core") (r "^0.66") (d #t) (k 0)) (d (n "conrod_derive") (r "^0.66") (d #t) (k 0)))) (h "00zy1r1jidh5kq09y12r0dviig9rdihgybxq2cv3i5cq24rc0bff")))

(define-public crate-conrod_prompt-0.1.1 (c (n "conrod_prompt") (v "0.1.1") (d (list (d (n "conrod_core") (r "^0.66") (d #t) (k 0)) (d (n "conrod_derive") (r "^0.66") (d #t) (k 0)) (d (n "conrod_glium") (r "^0.66") (d #t) (k 2)) (d (n "conrod_winit") (r "^0.66") (d #t) (k 2)) (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "glium") (r "^0.24") (d #t) (k 2)) (d (n "image") (r "^0.21") (d #t) (k 2)) (d (n "winit") (r "^0.19") (d #t) (k 2)))) (h "114q0vs0c1xmbjwmw59wbqkbal8ym8c985jfsj57m2aj72ajagi7")))

(define-public crate-conrod_prompt-0.2.0 (c (n "conrod_prompt") (v "0.2.0") (d (list (d (n "conrod_core") (r "^0.68") (d #t) (k 0)) (d (n "conrod_derive") (r "^0.68") (d #t) (k 0)) (d (n "conrod_glium") (r "^0.68") (d #t) (k 2)) (d (n "conrod_winit") (r "^0.68") (d #t) (k 2)) (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "glium") (r "^0.24") (d #t) (k 2)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "winit") (r "^0.19") (d #t) (k 2)))) (h "17icar87k4sy54rlj195vm9jifmv4agl8y0p2qr77467j271fg1w")))

(define-public crate-conrod_prompt-0.3.0 (c (n "conrod_prompt") (v "0.3.0") (d (list (d (n "conrod_core") (r "^0.68") (d #t) (k 0)) (d (n "conrod_derive") (r "^0.68") (d #t) (k 0)) (d (n "conrod_glium") (r "^0.68") (d #t) (k 2)) (d (n "conrod_winit") (r "^0.68") (d #t) (k 2)) (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "glium") (r "^0.24") (d #t) (k 2)) (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "winit") (r "^0.19") (d #t) (k 2)))) (h "0qj4zd45ib5dbpnwbycr13a36lsqjx27xb47nnalxj3n2p72g636")))

(define-public crate-conrod_prompt-0.4.1 (c (n "conrod_prompt") (v "0.4.1") (d (list (d (n "conrod_core") (r "^0.73") (d #t) (k 0)) (d (n "conrod_derive") (r "^0.73") (d #t) (k 0)) (d (n "conrod_glium") (r "^0.73") (d #t) (k 2)) (d (n "conrod_winit") (r "^0.73") (d #t) (k 2)) (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "glium") (r "^0.28") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "winit") (r "^0.23") (d #t) (k 2)))) (h "008dwxfdxylh5h9yl64l88cjf2v9ydwjr8gslb0x232rids9wyz0")))

