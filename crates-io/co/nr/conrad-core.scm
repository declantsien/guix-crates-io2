(define-module (crates-io co nr conrad-core) #:use-module (crates-io))

(define-public crate-conrad-core-0.2.0 (c (n "conrad-core") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.17") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "scrypt") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("macros" "rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "153gc0fv14w3xpl8nafpaprm8zfnm3vb653zmk48qwqqrnvx1yc5")))

