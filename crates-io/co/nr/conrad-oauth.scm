(define-module (crates-io co nr conrad-oauth) #:use-module (crates-io))

(define-public crate-conrad-oauth-0.2.0 (c (n "conrad-oauth") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "conrad-core") (r "^0.2.0") (d #t) (k 0)) (d (n "oauth2") (r "^4.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18gz9ni4i6ypb4a2w6x6zw57ng2rf32cmzmmq1i2sn9yz590jam4")))

