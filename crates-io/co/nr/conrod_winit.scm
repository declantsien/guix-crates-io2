(define-module (crates-io co nr conrod_winit) #:use-module (crates-io))

(define-public crate-conrod_winit-0.62.0 (c (n "conrod_winit") (v "0.62.0") (d (list (d (n "conrod_core") (r "^0.62") (d #t) (k 0)) (d (n "winit") (r "^0.18") (d #t) (k 0)))) (h "1p6q3b5gmlbgcr60gp7za7llvvwvb222k5kd6rlhw6a98p4pzqyf")))

(define-public crate-conrod_winit-0.63.0 (c (n "conrod_winit") (v "0.63.0") (d (list (d (n "conrod_core") (r "^0.63") (d #t) (k 0)) (d (n "winit") (r "^0.18") (d #t) (k 0)))) (h "1jsss25rv6ac9jxm0qx8jmqr038xy8c9vhg9awi0qamyrhlmz5xm")))

(define-public crate-conrod_winit-0.64.0 (c (n "conrod_winit") (v "0.64.0") (h "0rrddcz2ysmhjznzzh09gxl4kixnqf10p1xgddgba6x5r4ylyccc")))

(define-public crate-conrod_winit-0.65.0 (c (n "conrod_winit") (v "0.65.0") (h "10g54p6agadx2w35z64ih55sn06769c5nvccdr837n7rfpp72jpj")))

(define-public crate-conrod_winit-0.66.0 (c (n "conrod_winit") (v "0.66.0") (h "1aj2l3qx999c1h8pmdmrmya4vy62c9br8vaiz94xvhsjpaspfppm")))

(define-public crate-conrod_winit-0.67.0 (c (n "conrod_winit") (v "0.67.0") (h "0lnp6pdka3y2c1hx16zminq4zvscmz709hi13g8hgi03fnn32cvs")))

(define-public crate-conrod_winit-0.68.0 (c (n "conrod_winit") (v "0.68.0") (h "092xswhh0b975czki2qs68w66473dwiqivbw79v3v8g21jv5v7hk")))

(define-public crate-conrod_winit-0.69.0 (c (n "conrod_winit") (v "0.69.0") (h "0yikndzznslwj10ya1m4nxx0rddgc8jk5f036d9qcgrr2jr8imgg")))

(define-public crate-conrod_winit-0.70.0 (c (n "conrod_winit") (v "0.70.0") (h "1flg3chc587hvg7kqg5z426gbg5q5wr6ybp3cl2m9m6k1iiaa4jd")))

(define-public crate-conrod_winit-0.71.0 (c (n "conrod_winit") (v "0.71.0") (h "055mxi60f07xhvfs90z64r230d0xccslrgcp92xmr5j08jk69gnz")))

(define-public crate-conrod_winit-0.72.0 (c (n "conrod_winit") (v "0.72.0") (h "1ygj0gk5475ri4yq2yjqjwcd4hjj4lnxal93b6vhs0jashb4qifi")))

(define-public crate-conrod_winit-0.73.0 (c (n "conrod_winit") (v "0.73.0") (h "0vlbmkz8dmpg38yvbdcfr4jfzdmbl3107p5891grm6aa2fp6i9li")))

(define-public crate-conrod_winit-0.74.0 (c (n "conrod_winit") (v "0.74.0") (h "1dgdhnyaqpyrhs53mj1zf7lk1x5cicw4mhd1kzh8spsxkx3mkvzj")))

(define-public crate-conrod_winit-0.75.0 (c (n "conrod_winit") (v "0.75.0") (h "0y3a0d1zpvgqqp07ra02gi3yvxpdn8jc21pr4g0kvd2kf9pq1zi2")))

(define-public crate-conrod_winit-0.76.0 (c (n "conrod_winit") (v "0.76.0") (h "10g7sxzsb0c2kvlrch305xqha1x4a8kyj44pnqir76k1jdj0xa4b")))

(define-public crate-conrod_winit-0.76.1 (c (n "conrod_winit") (v "0.76.1") (h "1l7hsq1azgfssb5l02gglvc3s8cnz34scyxsad354954r4qcdjz4")))

