(define-module (crates-io co nr conrad-tokens) #:use-module (crates-io))

(define-public crate-conrad-tokens-0.2.0 (c (n "conrad-tokens") (v "0.2.0") (d (list (d (n "conrad-core") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0aa1lj1rccsvc17z79yz0nlvpkg0jyn36kyjkk8hiigs56n7g5v9")))

