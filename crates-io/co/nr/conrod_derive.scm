(define-module (crates-io co nr conrod_derive) #:use-module (crates-io))

(define-public crate-conrod_derive-0.1.0 (c (n "conrod_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0kp9ppm7rmisn1k4pxnbggnnwxsacvr4f5h0bqz9czqirx7qp3y2")))

(define-public crate-conrod_derive-0.1.1 (c (n "conrod_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1n6sx3a53lfmy5mw1rd44gbv1f85r556dxl7dx26qwdjvz7w2b9y")))

(define-public crate-conrod_derive-0.1.2 (c (n "conrod_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "11yfc1s5pgdavs11v58gdsa5392c63fzbjmn1kv9wkpvqqdw3dsk")))

(define-public crate-conrod_derive-0.62.0 (c (n "conrod_derive") (v "0.62.0") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1rjl74dki5p2gg6yfx1yi97w4if4n5ic7arbnwy4gpp0ygwzn5i2")))

(define-public crate-conrod_derive-0.63.0 (c (n "conrod_derive") (v "0.63.0") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ifv9mgrj61vs57aw51filjy9938an65xhbvxhad2ph16jfrip5a")))

(define-public crate-conrod_derive-0.64.0 (c (n "conrod_derive") (v "0.64.0") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0q3z799nnkj6hcs50lhxviwf5ki64r3r4h1palyiamk7s4kfi8vc")))

(define-public crate-conrod_derive-0.65.0 (c (n "conrod_derive") (v "0.65.0") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ixfzild9zd060fmjp2kpf7g2f62mffr8b4gp8v7ks6761s06jrz")))

(define-public crate-conrod_derive-0.66.0 (c (n "conrod_derive") (v "0.66.0") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1z9l33v3r8f7c6gjykwscv2iyz67hd0nfxmlw1gkhab7g85q4phf")))

(define-public crate-conrod_derive-0.67.0 (c (n "conrod_derive") (v "0.67.0") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0g97flm31fgvdfcrfisjlyzf4jk806yiz41vvh0gx857v4ncs60i")))

(define-public crate-conrod_derive-0.68.0 (c (n "conrod_derive") (v "0.68.0") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "17jvys26r74nc8d65na298dci0sqpkjdn105l37icl3a8z06l0km")))

(define-public crate-conrod_derive-0.69.0 (c (n "conrod_derive") (v "0.69.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qfmfgyyc2q7vviybw7z71yc26kvwwhavl50pr3449ci94kvribd")))

(define-public crate-conrod_derive-0.70.0 (c (n "conrod_derive") (v "0.70.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0a37azjkjchn7q6jpixgkdib1n8qa79i6c8ibcsqbpfza4mqqq7k")))

(define-public crate-conrod_derive-0.71.0 (c (n "conrod_derive") (v "0.71.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ph88fb9wgx8b6i92bws6z3nfznspb70y4ww8dhn9x1mkjqk9c7p")))

(define-public crate-conrod_derive-0.72.0 (c (n "conrod_derive") (v "0.72.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1qpnwp0zpgxxlcs1gzndjxp9vqwr0mwl4rhhnlb2xwb01qay1qga")))

(define-public crate-conrod_derive-0.73.0 (c (n "conrod_derive") (v "0.73.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1dwcpl54823z1ap98a5m2cq7gka9kqd83nd717da0wgxjzpbdr72")))

(define-public crate-conrod_derive-0.74.0 (c (n "conrod_derive") (v "0.74.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1l0nawd4jgrsrxy45dyyg0yjv4b1z9dwxv9qawm565nqlzphg5z1")))

(define-public crate-conrod_derive-0.75.0 (c (n "conrod_derive") (v "0.75.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "17as480cl6xr8p7an1r6fd0zsg7qhg9xxbwch5pc5ml3vp6vi85x")))

(define-public crate-conrod_derive-0.76.0 (c (n "conrod_derive") (v "0.76.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0byxmnxmvzkinxddi6nks03l78i7hqwdqy0nbqnnaaypid6zrv8s")))

(define-public crate-conrod_derive-0.76.1 (c (n "conrod_derive") (v "0.76.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "04g50ypikjn45cspb4ahaavi2p7kwdnrny6lvis8m09p9z8fk9gd")))

