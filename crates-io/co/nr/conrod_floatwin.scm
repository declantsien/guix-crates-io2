(define-module (crates-io co nr conrod_floatwin) #:use-module (crates-io))

(define-public crate-conrod_floatwin-0.0.1 (c (n "conrod_floatwin") (v "0.0.1") (d (list (d (n "conrod_core") (r ">=0.69") (d #t) (k 0)) (d (n "conrod_core") (r "^0.70") (d #t) (k 2)) (d (n "conrod_glium") (r "^0.70") (d #t) (k 2)) (d (n "conrod_winit") (r "^0.70") (d #t) (k 2)) (d (n "glium") (r "^0.24") (d #t) (k 2)) (d (n "winit") (r "^0.19") (d #t) (k 2)))) (h "04vza3xzhgk0zmymf1gaiwx1z7m1nq570dy844cssl826mxjrgma")))

(define-public crate-conrod_floatwin-0.0.2 (c (n "conrod_floatwin") (v "0.0.2") (d (list (d (n "conrod_core") (r ">=0.69") (d #t) (k 0)) (d (n "conrod_core") (r "^0.70") (d #t) (k 2)) (d (n "conrod_glium") (r "^0.70") (d #t) (k 2)) (d (n "conrod_winit") (r "^0.70") (d #t) (k 2)) (d (n "glium") (r "^0.24") (d #t) (k 2)) (d (n "winit") (r "^0.19") (d #t) (k 2)))) (h "1p22jvgk21zx8s27hfxkl13c8cbdp58c6rg2rhkhjghdwlkk165v")))

