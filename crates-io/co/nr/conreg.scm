(define-module (crates-io co nr conreg) #:use-module (crates-io))

(define-public crate-conreg-0.1.0 (c (n "conreg") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.8.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("std" "libm"))) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "096bljsl5a5ndj0h1392fqqlk7iv7xrymqwmv0l82ix3igmb3cq6") (f (quote (("std") ("default" "std"))))))

