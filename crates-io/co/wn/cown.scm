(define-module (crates-io co wn cown) #:use-module (crates-io))

(define-public crate-cown-0.1.0 (c (n "cown") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "0447i1fq4wdcrglmibs82jk76wsv0g9xcskgsnq275aa1bwi08iq")))

(define-public crate-cown-0.1.1 (c (n "cown") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "1yfi449iydhql23fsxygdw275p8fj3mymcwpj95fl168yyaq2y25")))

(define-public crate-cown-0.1.2 (c (n "cown") (v "0.1.2") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "04ga3nnjxd7fm60ggbnqcayrywdi1syp0n668d5glfhxq7lnaclz")))

