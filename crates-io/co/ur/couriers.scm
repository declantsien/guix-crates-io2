(define-module (crates-io co ur couriers) #:use-module (crates-io))

(define-public crate-couriers-0.0.0 (c (n "couriers") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("std" "derive" "env"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0kylw7lxzm0gcmbia8lzfbdjbikc0159cd2m560802x8ffqhz949")))

(define-public crate-couriers-0.0.0-unreleased+2 (c (n "couriers") (v "0.0.0-unreleased+2") (h "08bi6fxv7akp6lrnsflsqhnhm8l8ya4xv37xllqsldfa5ag2qc1c")))

