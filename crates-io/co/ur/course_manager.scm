(define-module (crates-io co ur course_manager) #:use-module (crates-io))

(define-public crate-course_manager-0.1.0 (c (n "course_manager") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1cm7lxpfz5niz2vx2z9h1r8h3v8f20103d7ag29d5fqlwzkpga85")))

(define-public crate-course_manager-0.1.1 (c (n "course_manager") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0xrm5p1azxkggzjp2i1vy7gh9v19mb5j4rpwiphcs0wch4ah720n")))

(define-public crate-course_manager-0.1.2 (c (n "course_manager") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1wvxrhhx43blpz00brzsc17dg5mfyfsqwyhgaac2912b69s7mzz3")))

(define-public crate-course_manager-0.1.3 (c (n "course_manager") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "homedir") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1lyv3lm02abjcy6hfbd534lvhxgd4hqb2vv4vghny7v6cvccydvz")))

