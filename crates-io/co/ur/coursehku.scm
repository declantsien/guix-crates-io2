(define-module (crates-io co ur coursehku) #:use-module (crates-io))

(define-public crate-coursehku-0.1.0 (c (n "coursehku") (v "0.1.0") (d (list (d (n "polars") (r "^0.36.2") (f (quote ("lazy" "strings" "lazy_regex"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "16k2aahcav4fgc37khi58ir4588imd2c1khn1prl053mzm0bpacw")))

(define-public crate-coursehku-0.2.0 (c (n "coursehku") (v "0.2.0") (d (list (d (n "polars") (r "^0.36.2") (f (quote ("lazy" "strings" "lazy_regex"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n75m0imx6sv1cjvp66nifp60jzbwbvfmsffak83qvydr19cf10a")))

(define-public crate-coursehku-0.3.0 (c (n "coursehku") (v "0.3.0") (d (list (d (n "polars") (r "^0.36.2") (f (quote ("lazy" "strings" "lazy_regex"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "00bkl2ci1j8hzkk8lslxrsbxv9gyap3lx62rh7r78pfxmzjahcvn")))

(define-public crate-coursehku-1.0.0 (c (n "coursehku") (v "1.0.0") (d (list (d (n "polars") (r "^0.36.2") (f (quote ("lazy" "strings" "lazy_regex"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d2jrflia3v8by44gn2jqsgsm7l0cwvwrnzzlb2s7a4zg68j6qdc")))

