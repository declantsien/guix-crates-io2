(define-module (crates-io co ur courgette) #:use-module (crates-io))

(define-public crate-courgette-0.1.0 (c (n "courgette") (v "0.1.0") (h "14qwjxdi7j8axpm24vz0x9g3q6c7d99fxmn8hld7r3x6ixm161x5")))

(define-public crate-courgette-0.1.1 (c (n "courgette") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1gxjipmk4illjmb3lgq8i13mwhb26a3qlp19xc3zfdwgnvb7yxh5")))

