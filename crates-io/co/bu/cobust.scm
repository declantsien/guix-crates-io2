(define-module (crates-io co bu cobust) #:use-module (crates-io))

(define-public crate-cobust-0.1.0 (c (n "cobust") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("cookies"))) (d #t) (k 2)) (d (n "retryiter") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "14anj8hnkgil019l9l58f6svhl7himfnsfa0i0mld2z6l2nwh2as")))

(define-public crate-cobust-0.1.1 (c (n "cobust") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("cookies"))) (d #t) (k 2)) (d (n "retryiter") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0jhg81n9r80aa46k71pa66rz0p4cyhc54dxgyrix97a6q9gjlzkg")))

