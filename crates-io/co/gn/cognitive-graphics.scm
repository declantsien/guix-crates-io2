(define-module (crates-io co gn cognitive-graphics) #:use-module (crates-io))

(define-public crate-cognitive-graphics-0.0.1 (c (n "cognitive-graphics") (v "0.0.1") (d (list (d (n "egl") (r "^0.2.6") (d #t) (k 0)) (d (n "gbm-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "118a0i7acqhfbj46ciinq2qxc1b72b2byxrshqrfn2kd63qs4hqx")))

(define-public crate-cognitive-graphics-0.1.0 (c (n "cognitive-graphics") (v "0.1.0") (d (list (d (n "egl") (r "^0.2") (d #t) (k 0)) (d (n "gbm-rs") (r "^0.2") (d #t) (k 0)) (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11ffslhnw60kiwx2mrxmdd7jph0418l0ivhdkcsw7fcl18h44iac")))

