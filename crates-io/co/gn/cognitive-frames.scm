(define-module (crates-io co gn cognitive-frames) #:use-module (crates-io))

(define-public crate-cognitive-frames-0.0.1 (c (n "cognitive-frames") (v "0.0.1") (d (list (d (n "cognitive-qualia") (r "^0.0.3") (d #t) (k 0)) (d (n "cognitive-qualia") (r "^0.0.3") (f (quote ("testing"))) (d #t) (k 2)))) (h "04qvymx45wzq141nq2lmv3hndk0nv6q4bzfqc1n4qsvhr7cnvrn1") (f (quote (("testing"))))))

(define-public crate-cognitive-frames-0.1.0 (c (n "cognitive-frames") (v "0.1.0") (d (list (d (n "cognitive-qualia") (r "^0.1") (d #t) (k 0)) (d (n "cognitive-qualia") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)))) (h "0h2s9grbbhz5hkg8ffd4c8vhq56zz2j2gbgz6ky5p7pvvdlwwas0") (f (quote (("testing"))))))

