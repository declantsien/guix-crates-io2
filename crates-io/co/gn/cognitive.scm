(define-module (crates-io co gn cognitive) #:use-module (crates-io))

(define-public crate-cognitive-0.0.1 (c (n "cognitive") (v "0.0.1") (d (list (d (n "dharma") (r "^0.0.1") (d #t) (k 0)) (d (n "timber") (r "^0.0.1") (d #t) (k 0)))) (h "094yhrhryhpd7bljdlb1izk12pjpi1nzpkbrzavv5kmj5422b67b")))

(define-public crate-cognitive-0.1.0 (c (n "cognitive") (v "0.1.0") (d (list (d (n "cognitive-device-manager") (r "^0.1") (d #t) (k 0)) (d (n "cognitive-graphics") (r "^0.1") (d #t) (k 0)) (d (n "cognitive-inputs") (r "^0.1") (d #t) (k 0)) (d (n "cognitive-outputs") (r "^0.1") (d #t) (k 0)) (d (n "cognitive-qualia") (r "^0.1") (d #t) (k 0)) (d (n "cognitive-renderer-gl") (r "^0.1") (d #t) (k 0)) (d (n "dharma") (r "^0.1") (d #t) (k 0)) (d (n "timber") (r "^0.1") (d #t) (k 0)))) (h "03rkyvlvkgb18lj62ihbi7zqla81swkjyg329bh0pvvbzf81d5bc")))

