(define-module (crates-io co gn cogno) #:use-module (crates-io))

(define-public crate-cogno-0.1.0 (c (n "cogno") (v "0.1.0") (d (list (d (n "cogno-attr") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0i824kzhxgvbx3vnrapddr4245hqxcf092lpz6g4llvi6ayyfx2c") (s 2) (e (quote (("console" "dep:colored"))))))

