(define-module (crates-io co gn cognito_user_reader_lib) #:use-module (crates-io))

(define-public crate-cognito_user_reader_lib-0.5.0 (c (n "cognito_user_reader_lib") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "console") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0yi4xq4xa47rwfhpcr56sf6chx0wykpwfxqvava930199d3vs290") (y #t)))

