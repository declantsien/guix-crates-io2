(define-module (crates-io co gn cognitive-aesthetics) #:use-module (crates-io))

(define-public crate-cognitive-aesthetics-0.0.1 (c (n "cognitive-aesthetics") (v "0.0.1") (d (list (d (n "cognitive-qualia") (r "^0.0.3") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)) (d (n "timber") (r "^0.0.1") (d #t) (k 0)))) (h "05459c9lsdb4xjqj6dl94amll6idggqhkqnqgkghz2npz440nbm6")))

(define-public crate-cognitive-aesthetics-0.1.0 (c (n "cognitive-aesthetics") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "cognitive-qualia") (r "^0.1") (d #t) (k 0)) (d (n "font-loader") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.12") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "timber") (r "^0.1") (d #t) (k 0)))) (h "17560fbshwlzlyar9yhq41w9i1w67k05skqrqysizplhf00l7vfd")))

