(define-module (crates-io co gn cognitive-services-speech-sdk-rs) #:use-module (crates-io))

(define-public crate-cognitive-services-speech-sdk-rs-0.1.0 (c (n "cognitive-services-speech-sdk-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r81cg2qripznnwsh620g0fvdjgsv06hfyyh2nmjqfn8yrf6ycyv")))

(define-public crate-cognitive-services-speech-sdk-rs-0.1.1 (c (n "cognitive-services-speech-sdk-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0az96y7z87dfdgpisfg9qnnk8frgwv286q653b9qxm847b2jx9q6")))

(define-public crate-cognitive-services-speech-sdk-rs-0.1.2 (c (n "cognitive-services-speech-sdk-rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00zc7fawxlkinahmjcvaqmc4gd8hh00i0fasv58xp4l6cxv75zg5")))

(define-public crate-cognitive-services-speech-sdk-rs-0.1.3 (c (n "cognitive-services-speech-sdk-rs") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1209qvbphabhf472rr6xl74rsg0ckps949h8g0c2fxbyvw6iw9i2") (y #t)))

(define-public crate-cognitive-services-speech-sdk-rs-0.1.4 (c (n "cognitive-services-speech-sdk-rs") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "056vfabhpl6x59qmgiibpfxzhs39lwr26iak9n07qdvg73raw4a6")))

(define-public crate-cognitive-services-speech-sdk-rs-0.2.0 (c (n "cognitive-services-speech-sdk-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rc5545kp8w6lr4mg0kx3pvdshfkdqsgnfw0d62xd5q4yy9ag6cp")))

(define-public crate-cognitive-services-speech-sdk-rs-0.2.1 (c (n "cognitive-services-speech-sdk-rs") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qwab1pyysq5dlafhyi6chbnyn4gd947aaapy0kg2fk5fzbpz5kj")))

(define-public crate-cognitive-services-speech-sdk-rs-0.2.2 (c (n "cognitive-services-speech-sdk-rs") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15wygdyv2j5jc29ph2wh8ahb3p0ffwalclc04sd73za3202pk46f")))

(define-public crate-cognitive-services-speech-sdk-rs-0.2.3 (c (n "cognitive-services-speech-sdk-rs") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17asbmgal8amzprqdy3ng4srbsh97v748gszvn3fan36pr0jrbiz")))

(define-public crate-cognitive-services-speech-sdk-rs-0.3.0 (c (n "cognitive-services-speech-sdk-rs") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18ai656zcq8jhdq1rf1r5fwzk4yzx1a0l1gaw5jg3bcma1gm0r9f")))

(define-public crate-cognitive-services-speech-sdk-rs-0.3.1 (c (n "cognitive-services-speech-sdk-rs") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 1)))) (h "01qfli7x9hg0l9k1md30chjc05dd9x8qrihr7081j4j7a70f1cy2")))

(define-public crate-cognitive-services-speech-sdk-rs-1.0.0 (c (n "cognitive-services-speech-sdk-rs") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 1)))) (h "13hakfxf7prch40lrnb3041c0rl5i08asfa2sy79vfshb8y8kr2w")))

(define-public crate-cognitive-services-speech-sdk-rs-1.0.1 (c (n "cognitive-services-speech-sdk-rs") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 1)))) (h "14k7c784kz6wadav17nr1ph1bfl3zxfib05kkcq4n5hafyg053vs")))

(define-public crate-cognitive-services-speech-sdk-rs-1.0.2 (c (n "cognitive-services-speech-sdk-rs") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 1)))) (h "1r88c7agk00kl3nqa5583v3bvl58zwsqa187snbn7xzsyak1yglv")))

