(define-module (crates-io co gn cogno-attr) #:use-module (crates-io))

(define-public crate-cogno-attr-0.1.0 (c (n "cogno-attr") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08n8nl1mpil6fv60v886ip6hchc266ggy53v8j1rrqrf1a2k147d")))

