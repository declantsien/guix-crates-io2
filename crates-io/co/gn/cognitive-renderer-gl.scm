(define-module (crates-io co gn cognitive-renderer-gl) #:use-module (crates-io))

(define-public crate-cognitive-renderer-gl-0.0.1 (c (n "cognitive-renderer-gl") (v "0.0.1") (d (list (d (n "cognitive-graphics") (r "^0.0.1") (d #t) (k 0)) (d (n "cognitive-qualia") (r "^0.0.1") (d #t) (k 0)) (d (n "egl") (r "^0.2.6") (d #t) (k 0)) (d (n "gl") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "timber") (r "^0.0.1") (d #t) (k 0)))) (h "1ic5p71dxgm4pba4fw4q5v0hh1jblsfxskp290008i4d0f58j09v")))

(define-public crate-cognitive-renderer-gl-0.0.2 (c (n "cognitive-renderer-gl") (v "0.0.2") (d (list (d (n "cognitive-graphics") (r "^0.0.1") (d #t) (k 0)) (d (n "cognitive-qualia") (r "^0.0.3") (d #t) (k 0)) (d (n "egl") (r "^0.2.6") (d #t) (k 0)) (d (n "gl") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "timber") (r "^0.0.1") (d #t) (k 0)))) (h "1yyrp7fp8lkyx95q9nl3q0am7ikgbpb3nxk20br42s1rz35ksw7s")))

(define-public crate-cognitive-renderer-gl-0.1.0 (c (n "cognitive-renderer-gl") (v "0.1.0") (d (list (d (n "cognitive-graphics") (r "^0.1") (d #t) (k 0)) (d (n "cognitive-qualia") (r "^0.1") (d #t) (k 0)) (d (n "egl") (r "^0.2") (d #t) (k 0)) (d (n "gl") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "timber") (r "^0.1") (d #t) (k 0)))) (h "0kz17zjxv70yicwx1yx3fycmdsh775ck3ya2km5nf3a383xg2p4w")))

