(define-module (crates-io co v- cov-mark) #:use-module (crates-io))

(define-public crate-cov-mark-1.0.0 (c (n "cov-mark") (v "1.0.0") (h "0ghfkxzmm2cwb1215m6f7815mnk2sd9ynjavlb7pikm7h410myg6")))

(define-public crate-cov-mark-1.1.0 (c (n "cov-mark") (v "1.1.0") (h "1wv75ylrai556m388a40d50fxiyacmvm6qqz6va6qf1q04z3vylz") (f (quote (("thread-local"))))))

(define-public crate-cov-mark-2.0.0-pre.1 (c (n "cov-mark") (v "2.0.0-pre.1") (h "0jj4yz70k31ax1n3s7iyjv1k5yzrm4hkibrazqciycyrdgvxhj0d") (f (quote (("enable") ("default" "enable"))))))

