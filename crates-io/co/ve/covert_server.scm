(define-module (crates-io co ve covert_server) #:use-module (crates-io))

(define-public crate-covert_server-0.1.0 (c (n "covert_server") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "03ag73s93l0yyvsdlsp0zlwca2yfrf4bshwnli28sfdclg0b7ywx")))

(define-public crate-covert_server-0.1.1 (c (n "covert_server") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1lhp0q0cjhmma714a8ffqp6a914s5ijizywqkqjd3vvcm4chpya0")))

(define-public crate-covert_server-0.1.2 (c (n "covert_server") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "0azbb538vdqgb85a9da07jm2chkc4fm6qz2mjqz0fspnbzdibxfy")))

(define-public crate-covert_server-0.1.3 (c (n "covert_server") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 0)))) (h "15viqpbz2cyqfr1p9bc1fh10sgl9a48ywx9dw3wsm4mr9wff9rd3")))

(define-public crate-covert_server-0.1.4 (c (n "covert_server") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)))) (h "1gch094scmlz80cw66csc0gsi7aqkldgln22rvlshdmm9d0fppzv")))

