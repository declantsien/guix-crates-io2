(define-module (crates-io co ve cove) #:use-module (crates-io))

(define-public crate-cove-0.1.0 (c (n "cove") (v "0.1.0") (d (list (d (n "cove-num") (r "^0") (d #t) (k 0)))) (h "1if3cb9p30vj1xiba5x585qgy54hdb0c1bpc1np9lzqwl324lfz1") (y #t)))

(define-public crate-cove-0.2.0 (c (n "cove") (v "0.2.0") (h "13lpkzls4l5zv6m7ffqrnspa1dvamhg20dr2xyrf7h70yn1vamri")))

(define-public crate-cove-0.3.0 (c (n "cove") (v "0.3.0") (h "1mmg4mqkchxryrll2i2k7d6lqzsn133nyqljqvxwliwjy4r97js8") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cove-0.4.0 (c (n "cove") (v "0.4.0") (h "0bylgc3nr8yrxmy019h285axhpr8r7cjy6g725h09k9pm5zdhhl1") (f (quote (("std") ("default" "std"))))))

