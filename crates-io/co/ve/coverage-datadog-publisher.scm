(define-module (crates-io co ve coverage-datadog-publisher) #:use-module (crates-io))

(define-public crate-coverage-datadog-publisher-0.1.0 (c (n "coverage-datadog-publisher") (v "0.1.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "datadog-client") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "13ms05wi00wm08wpzx0b866b80r817z39dvzwp8d58a0dj88v54w")))

(define-public crate-coverage-datadog-publisher-0.2.0 (c (n "coverage-datadog-publisher") (v "0.2.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "datadog-client") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "05xh9z5iqr2jygl3x3bq0zsfblk9qr4i4xp11km84qf7vhpx7slq")))

