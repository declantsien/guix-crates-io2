(define-module (crates-io co ve covers) #:use-module (crates-io))

(define-public crate-covers-0.1.0-beta.1 (c (n "covers") (v "0.1.0-beta.1") (h "0yp7dawbjh5q3pks52d2987yyb5zax3ca4j1zymfpv1pvjhamyna") (f (quote (("default" "_") ("_orig_") ("__") ("_"))))))

(define-public crate-covers-0.1.0-beta.2 (c (n "covers") (v "0.1.0-beta.2") (h "13ni29a9lkm3dlqwbbmlnzi07fcg761r1zgyzb0slvyk004hpv69") (f (quote (("default" "_") ("_orig_") ("__") ("_"))))))

(define-public crate-covers-0.1.0-beta.3 (c (n "covers") (v "0.1.0-beta.3") (h "0v57sbi24j20dz231r1qvjjw54087763fjxhljn5fybfbi68vql1") (f (quote (("default") ("_orig_") ("__"))))))

(define-public crate-covers-0.1.0-beta.4 (c (n "covers") (v "0.1.0-beta.4") (h "0rpvcgyhsiqfmaqf1nxbfpvap03s0yhbqvxaskgbwb9y9n7kfhz0") (f (quote (("default") ("_orig_") ("__"))))))

(define-public crate-covers-0.1.0-beta.5 (c (n "covers") (v "0.1.0-beta.5") (h "0lcpzk8qxsjap4q7a8nr2g6mwqfc5dns8vrqr6k8j0bnxlws42f4") (f (quote (("default") ("_orig_") ("__"))))))

(define-public crate-covers-0.1.0-beta.6 (c (n "covers") (v "0.1.0-beta.6") (h "15ka4k5q15ns957py57kvvs00y6v4paak9xmq28934lhbr1z1sp7") (f (quote (("no-pub") ("default") ("_orig_") ("__"))))))

(define-public crate-covers-0.1.0-beta.7 (c (n "covers") (v "0.1.0-beta.7") (h "15wjgl5k9zbf99md3fivgy7q1g8ha4sbayfw4n332va6zc91wzhd") (f (quote (("no-pub") ("default") ("_orig_") ("__"))))))

