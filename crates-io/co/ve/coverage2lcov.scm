(define-module (crates-io co ve coverage2lcov) #:use-module (crates-io))

(define-public crate-coverage2lcov-0.1.0 (c (n "coverage2lcov") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0wz6mv9zkmxgk0wy5kqawvmjb31g35lmpwsv5015rjq25p6l9915")))

