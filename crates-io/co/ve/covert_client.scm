(define-module (crates-io co ve covert_client) #:use-module (crates-io))

(define-public crate-covert_client-0.1.0 (c (n "covert_client") (v "0.1.0") (d (list (d (n "windows") (r "^0.38.0") (f (quote ("Win32_System_Threading" "Win32_Foundation" "Win32_Security" "Win32_System_Memory" "Win32_System_Pipes" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (k 0)))) (h "1baklby04wnqyzi9gcdq0iy38wxd6vhvbyplvki561myfaz1271r")))

(define-public crate-covert_client-0.1.1 (c (n "covert_client") (v "0.1.1") (d (list (d (n "windows") (r "^0.38.0") (f (quote ("Win32_System_Threading" "Win32_Foundation" "Win32_Security" "Win32_System_Memory" "Win32_System_Pipes" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (k 0)))) (h "055ldc83rawvnmbws4qwcs0qr8cz9afjcjpcinxq25j469v6hk64")))

(define-public crate-covert_client-0.1.2 (c (n "covert_client") (v "0.1.2") (d (list (d (n "windows") (r "^0.38.0") (f (quote ("Win32_System_Threading" "Win32_Foundation" "Win32_Security" "Win32_System_Memory" "Win32_System_Pipes" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (k 0)))) (h "11jxnxbipsrz36g8sbwk38m6l1a1hx5jk6ld019b9gn5x3dbyvc9")))

(define-public crate-covert_client-0.1.3 (c (n "covert_client") (v "0.1.3") (d (list (d (n "windows") (r "^0.38.0") (f (quote ("Win32_System_Threading" "Win32_Foundation" "Win32_Security" "Win32_System_Memory" "Win32_System_Pipes" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (k 0)))) (h "0s593ij7qc1jszsah4ixwknr18y0jjzrw3jdazbv8mzxkh4snx8p")))

(define-public crate-covert_client-0.1.4 (c (n "covert_client") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_System_Threading" "Win32_Foundation" "Win32_Security" "Win32_System_Memory" "Win32_System_Pipes" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (k 0)))) (h "034fldsfq4xz5ibyikacxd86p317bzd9n7hf0zzrnrinpcb91xmx")))

(define-public crate-covert_client-0.1.5 (c (n "covert_client") (v "0.1.5") (d (list (d (n "windows") (r "^0.39.0") (f (quote ("Win32_System_Threading" "Win32_Foundation" "Win32_Security" "Win32_System_Memory" "Win32_System_Pipes" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (k 0)))) (h "0bvnwaxj53vfk6hhqgijhmwi84r48q5sr37x75nhs4lh81x3i7pw")))

