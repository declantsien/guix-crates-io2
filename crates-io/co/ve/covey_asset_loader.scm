(define-module (crates-io co ve covey_asset_loader) #:use-module (crates-io))

(define-public crate-covey_asset_loader-0.1.0 (c (n "covey_asset_loader") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 2)) (d (n "covey_asset_loader_macros") (r "^0.1.0") (d #t) (k 0)))) (h "169wakzw7z4cyv7zs08nd2609jxw8hfrpc6hj0hvlg3gxb4xyv5m") (y #t)))

(define-public crate-covey_asset_loader-0.1.1 (c (n "covey_asset_loader") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset"))) (k 0)) (d (n "bevy") (r "^0.10") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.18.3") (d #t) (k 2)) (d (n "covey_asset_loader_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1c6xy16qza56mb2bi4ir42dy06p88aj8zisng5q379iwybhqly7c")))

