(define-module (crates-io co ve covey_asset_loader_macros) #:use-module (crates-io))

(define-public crate-covey_asset_loader_macros-0.1.0 (c (n "covey_asset_loader_macros") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset"))) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0cf8rpd70cx0xn26hnwjvfaxc81i4hmfwx8pyhndgwx1ck4mnsv1")))

