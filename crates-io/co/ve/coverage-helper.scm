(define-module (crates-io co ve coverage-helper) #:use-module (crates-io))

(define-public crate-coverage-helper-0.1.0 (c (n "coverage-helper") (v "0.1.0") (h "1zpk3k8r7rzc84xp45qww0kix7s9iv92xajndi5c3wvw8w753nx9") (r "1.38")))

(define-public crate-coverage-helper-0.1.1 (c (n "coverage-helper") (v "0.1.1") (h "1jmm45536nnwgpbgabnngr21n7sjmga0bm4yzaflx925xfll3m0g") (r "1.38")))

(define-public crate-coverage-helper-0.2.0 (c (n "coverage-helper") (v "0.2.0") (h "1bywamhfv7ikdq1b16b2v41yj14pgxkzg57fq6bddjdziag3jdih") (r "1.38")))

(define-public crate-coverage-helper-0.2.1 (c (n "coverage-helper") (v "0.2.1") (h "1dsir9khff1j8i8hmmc23qdgdkc2y12yjkb96vx241v1vnkafswv") (r "1.38")))

(define-public crate-coverage-helper-0.2.2 (c (n "coverage-helper") (v "0.2.2") (h "0knim97n8v0yhn82rm4dvn0gds4fbwzx6f2yjdsiwgdv2wbmax41") (r "1.38")))

