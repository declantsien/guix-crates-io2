(define-module (crates-io co ve covert-sdk) #:use-module (crates-io))

(define-public crate-covert-sdk-0.1.1 (c (n "covert-sdk") (v "0.1.1") (d (list (d (n "covert-types") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0a0q8kasdams5blz0h7pq6nifcyiv3jmaciknwcnszy8g40rh2gm")))

(define-public crate-covert-sdk-0.1.2 (c (n "covert-sdk") (v "0.1.2") (d (list (d (n "covert-types") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1jy2398cdb6ffhahn6kh30m6s5am5kpnslqikr1mqlng3sk4vs7q")))

(define-public crate-covert-sdk-0.1.3 (c (n "covert-sdk") (v "0.1.3") (d (list (d (n "covert-types") (r "^0.1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1az6v8jgij6k8078f5qxkjg3ymxgsq9mn4z6ia77fsld7sikpv6q")))

