(define-module (crates-io co ve covert_common) #:use-module (crates-io))

(define-public crate-covert_common-0.1.0 (c (n "covert_common") (v "0.1.0") (d (list (d (n "bincode_aes") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "075jnc4kzjvglyqmpl6fmmka4s2k8f53jzczk09ciy75yvc88f2x")))

(define-public crate-covert_common-0.1.1 (c (n "covert_common") (v "0.1.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "cipher") (r "^0.4.3") (f (quote ("block-padding" "alloc"))) (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "02n90q6x41dkyjc54q35vgznn8384bl3kqmlfkjiw5lqd433nw9r")))

(define-public crate-covert_common-0.1.2 (c (n "covert_common") (v "0.1.2") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "cipher") (r "^0.4.3") (f (quote ("block-padding" "alloc"))) (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "053m2wq857d6whrw91jf68a4hz7zr88fqlv1p9dw2bhy7qqnw14z")))

(define-public crate-covert_common-0.1.3 (c (n "covert_common") (v "0.1.3") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "cipher") (r "^0.4.3") (f (quote ("block-padding" "alloc"))) (d #t) (k 0)) (d (n "crc32fast") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q4ssr70bv5wikw73mvs5f18w99yj6gvv84c9y9ss8bnfwz7cjcc")))

