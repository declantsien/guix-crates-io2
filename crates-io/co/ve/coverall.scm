(define-module (crates-io co ve coverall) #:use-module (crates-io))

(define-public crate-coverall-0.12.0 (c (n "coverall") (v "0.12.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uniffi") (r "^0.12.0") (f (quote ("builtin-bindgen"))) (d #t) (k 0)) (d (n "uniffi_build") (r "^0.12.0") (f (quote ("builtin-bindgen"))) (d #t) (k 1)) (d (n "uniffi_macros") (r "^0.12.0") (d #t) (k 0)))) (h "10dca7m5ya8nq72kz2qzy00v0v20m523syhdi15a4wqywv1y2vjc")))

