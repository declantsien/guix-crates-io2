(define-module (crates-io co ve coverage-prepare) #:use-module (crates-io))

(define-public crate-coverage-prepare-0.1.0 (c (n "coverage-prepare") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)))) (h "1i6p8v0fni34fdz6fq39b4h8jhnf7kxgrpa5hsxf5f9ksf1ylwyl")))

(define-public crate-coverage-prepare-0.2.0 (c (n "coverage-prepare") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "11wvqnp7dw5gx8zqc5pbrzvv391bq0ma8j5gv1qh030n1qdjf4kj")))

(define-public crate-coverage-prepare-0.3.0 (c (n "coverage-prepare") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "1ilrdnd1z87s255fifikpbbbbjs45ipb6w3503wsx0lpn5xnhan2")))

(define-public crate-coverage-prepare-0.3.1 (c (n "coverage-prepare") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "084rq1rf3d9kiddh0yqkp31xlc56d9mbwzvy2bhy999n0slk59ka")))

