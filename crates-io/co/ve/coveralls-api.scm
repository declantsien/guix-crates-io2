(define-module (crates-io co ve coveralls-api) #:use-module (crates-io))

(define-public crate-coveralls-api-0.1.0 (c (n "coveralls-api") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "18iwmrvqk6akq74pdij1yk7c4sva4xv5kq502k5jg280r78i437q")))

(define-public crate-coveralls-api-0.1.1 (c (n "coveralls-api") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "16yc6lmadzmpmwi4vi4dfdmjlk18n1jsbf2bpr1yxdra9gj7glwr")))

(define-public crate-coveralls-api-0.2.0 (c (n "coveralls-api") (v "0.2.0") (d (list (d (n "curl") (r "^0.4.6") (d #t) (k 0)) (d (n "deflate") (r "^0.7.11") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0iffxpn67igmhbakplbzcxvzy4m5y7xvjannyl8riyp9j90jd60a")))

(define-public crate-coveralls-api-0.3.0 (c (n "coveralls-api") (v "0.3.0") (d (list (d (n "curl") (r "^0.4.6") (d #t) (k 0)) (d (n "deflate") (r "^0.7.11") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0liyp2zwagsawb4wgq5jx73kdsxnwpxwwdmgkz15pk7c6vadbi3m")))

(define-public crate-coveralls-api-0.3.1 (c (n "coveralls-api") (v "0.3.1") (d (list (d (n "curl") (r "^0.4.6") (d #t) (k 0)) (d (n "deflate") (r "^0.7.11") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1yfasfnm0jlwl11gr3g8d5f4xh7v01fmz0bza9l9zm2hb8754kbz")))

(define-public crate-coveralls-api-0.3.2 (c (n "coveralls-api") (v "0.3.2") (d (list (d (n "curl") (r "^0.4.6") (d #t) (k 0)) (d (n "deflate") (r "^0.7.11") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "16b16290llnnl13gv8iad0n98p5r8ilvv02yk22gc9k0klsa72ph")))

(define-public crate-coveralls-api-0.3.3 (c (n "coveralls-api") (v "0.3.3") (d (list (d (n "curl") (r "^0.4.6") (d #t) (k 0)) (d (n "deflate") (r "^0.7.11") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0h6nrawp4hvdsb5jwiajrnx2f1fywwj932ga8z4cw5rgbciifzqs")))

(define-public crate-coveralls-api-0.4.0 (c (n "coveralls-api") (v "0.4.0") (d (list (d (n "curl") (r "^0.4.6") (d #t) (k 0)) (d (n "deflate") (r "^0.7.11") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "092vmwrhbawvfc26rc6ml22xw2d01n5l32gcbcj7224dg007y1q8")))

(define-public crate-coveralls-api-0.5.0 (c (n "coveralls-api") (v "0.5.0") (d (list (d (n "curl") (r "^0.4.6") (d #t) (k 0)) (d (n "deflate") (r "^0.8.2") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0z8mwsr47z5fcbqxzcb5mprzzb7f5wy0qkkfkwibgrk4bpr5xi4l")))

(define-public crate-coveralls-api-0.6.0 (c (n "coveralls-api") (v "0.6.0") (d (list (d (n "deflate") (r "^1.0.0") (f (quote ("gzip"))) (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0ris2b2lzg3wywvdxnbvhj170cg7kr4ll8r3i5kamh4bzvn6az70")))

