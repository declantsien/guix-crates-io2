(define-module (crates-io co -a co-authors) #:use-module (crates-io))

(define-public crate-co-authors-0.0.1 (c (n "co-authors") (v "0.0.1") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "00109azsma00zxqbr7j5cwcy1b7zc7xwpb83ysmchhq3198d87fn")))

(define-public crate-co-authors-0.1.0 (c (n "co-authors") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "0vsplzmcm29c7c5gh7rr2gqxpsv0rn70bi6zpyr1j56lly1bqwkc")))

