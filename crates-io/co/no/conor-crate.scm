(define-module (crates-io co no conor-crate) #:use-module (crates-io))

(define-public crate-conor-crate-0.1.0 (c (n "conor-crate") (v "0.1.0") (h "0jh3i6fj8pyivlnq9v4q2j5liqpj0sbrhw74z0lifrs7ybr779in")))

(define-public crate-conor-crate-0.1.1 (c (n "conor-crate") (v "0.1.1") (h "0gyicv9qlzwrh0vwazlh5d81dlnlklj82n9qkwafhnk6c8d6ngi8")))

(define-public crate-conor-crate-0.1.4 (c (n "conor-crate") (v "0.1.4") (h "1x7cnby3swkz663y77x232hnvg6phcxc4265hklimff1k4ggnnzq")))

