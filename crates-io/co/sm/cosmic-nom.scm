(define-module (crates-io co sm cosmic-nom) #:use-module (crates-io))

(define-public crate-cosmic-nom-0.3.0-alpha2 (c (n "cosmic-nom") (v "0.3.0-alpha2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0yyyyhggjklzpjf1ya9km6r95zxrpzcbyj7csxgx6i0nid9993m4")))

(define-public crate-cosmic-nom-0.3.0-alpha3 (c (n "cosmic-nom") (v "0.3.0-alpha3") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1zmwlrn3yxy9sll2p8yfm97pxvirp2732z8bg1s0wfjjkvxw0qn9")))

(define-public crate-cosmic-nom-0.3.0-alpha4 (c (n "cosmic-nom") (v "0.3.0-alpha4") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "14r04ris1m6qv2asylkprwgrlnmi8vprfw9r14iq2hsixpxfl3g8")))

(define-public crate-cosmic-nom-0.3.0-alpha5 (c (n "cosmic-nom") (v "0.3.0-alpha5") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0vm8v75gwhdkrxkvipdypxqqjy67aildc33fd74043yx7p7z77x0")))

(define-public crate-cosmic-nom-0.3.0-alpha6 (c (n "cosmic-nom") (v "0.3.0-alpha6") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "09kzvfzfphr9wz8mqdgzhslq342zmq9l2i2shrn0js7wa7hss86c")))

(define-public crate-cosmic-nom-0.3.0-alpha7 (c (n "cosmic-nom") (v "0.3.0-alpha7") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1ia39lkrfhzwsynmg00hg3dzyvhj7zgf4chb40gjr83hm4472yzs")))

(define-public crate-cosmic-nom-0.3.0-alpha8 (c (n "cosmic-nom") (v "0.3.0-alpha8") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1iw9j0dr0mb6lgy8ljlmals11a83wr3kg5if20sw9arbvs849v8d")))

(define-public crate-cosmic-nom-0.3.0-alpha9 (c (n "cosmic-nom") (v "0.3.0-alpha9") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0g97w6iyqza0s2xyn86jk7bza311wiqhmy6syg7zyj068lx93zim")))

(define-public crate-cosmic-nom-0.3.0 (c (n "cosmic-nom") (v "0.3.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "116znk91k59qzvy5zp12583aayg7f9ch32v1ys68mbgjb8cs2lg9")))

(define-public crate-cosmic-nom-0.3.1-alpha (c (n "cosmic-nom") (v "0.3.1-alpha") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "16rsn6cwfn6d6khq2h8v2giz03vvvf0ig638g3fsys0lmds87ij1")))

(define-public crate-cosmic-nom-0.3.1 (c (n "cosmic-nom") (v "0.3.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1grik333mpfbmpbpg33ajqmykv6jdnj64bkcd7719midywrl0i15")))

(define-public crate-cosmic-nom-0.3.2 (c (n "cosmic-nom") (v "0.3.2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1i2d5i9yx371qiy3rwwkz3s5wb7v1k4sdginbd6xn63lm1g6mavy")))

(define-public crate-cosmic-nom-0.3.3 (c (n "cosmic-nom") (v "0.3.3") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "132pcjgwki93znk02f65f6bxpkkj4fphi3gz0s2ldgc3kkl8rzdi")))

(define-public crate-cosmic-nom-0.3.4 (c (n "cosmic-nom") (v "0.3.4") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "00yg1lmwqqj3gc96yfd5gd0yqwzb616gybjq0c8ri143pb7wpqlk")))

(define-public crate-cosmic-nom-0.3.6-rc1 (c (n "cosmic-nom") (v "0.3.6-rc1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1hc65xjwsbf70ml9k0ph1a9k6as7l9cjjljb8bbg4gjrsxjialwr")))

(define-public crate-cosmic-nom-0.3.6-rc2 (c (n "cosmic-nom") (v "0.3.6-rc2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0nw64xbgmwipv1ingxcq5yinm0g3kszz20wxbzypilfy67gz1f0c")))

(define-public crate-cosmic-nom-0.3.6-rc3 (c (n "cosmic-nom") (v "0.3.6-rc3") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1zfaj0qcqx1b082gcb5i01jsi9lz82lqfsqcbqpa7r9xh9fizqy4")))

(define-public crate-cosmic-nom-0.3.6-rc4 (c (n "cosmic-nom") (v "0.3.6-rc4") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "05qsm1jgzad2z09sljgza3946d753ydymr82srg9lqqh1q7zmf5s")))

(define-public crate-cosmic-nom-0.3.6-rc5 (c (n "cosmic-nom") (v "0.3.6-rc5") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "165h5168298c8rc756361mpy2xsp9bpn040xwdfqijr6yjss9jhr")))

(define-public crate-cosmic-nom-0.3.6 (c (n "cosmic-nom") (v "0.3.6") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1i04rf9dl9zzvncph1l1gvmhamwbvwapb6vdszw5pxb3wcl5g8v5")))

