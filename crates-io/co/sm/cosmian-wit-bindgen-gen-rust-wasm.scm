(define-module (crates-io co sm cosmian-wit-bindgen-gen-rust-wasm) #:use-module (crates-io))

(define-public crate-cosmian-wit-bindgen-gen-rust-wasm-0.1.0 (c (n "cosmian-wit-bindgen-gen-rust-wasm") (v "0.1.0") (d (list (d (n "cosmian-wit-bindgen-gen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "cosmian-wit-bindgen-gen-rust") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)))) (h "0b44gil8jncajrfgyvwi4xxcgljv46p6gmzic85bjlw35jpbhiaf") (f (quote (("witx-compat" "cosmian-wit-bindgen-gen-core/witx-compat"))))))

(define-public crate-cosmian-wit-bindgen-gen-rust-wasm-0.1.1 (c (n "cosmian-wit-bindgen-gen-rust-wasm") (v "0.1.1") (d (list (d (n "cosmian-wit-bindgen-gen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "cosmian-wit-bindgen-gen-rust") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)))) (h "1mjsa23p3nxxnzc5fksdgv7xb52clmayidn83s4dgzmfwjabk3ic") (f (quote (("witx-compat" "cosmian-wit-bindgen-gen-core/witx-compat"))))))

