(define-module (crates-io co sm cosmos_gravity_proto) #:use-module (crates-io))

(define-public crate-cosmos_gravity_proto-0.1.0 (c (n "cosmos_gravity_proto") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "cosmos-sdk-proto") (r "^0.6.3") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)))) (h "01v46hnwr5ry0vyb322hdj4mm82pzjp18mqgaspj07mbryzv7jrn")))

