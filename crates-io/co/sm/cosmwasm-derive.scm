(define-module (crates-io co sm cosmwasm-derive) #:use-module (crates-io))

(define-public crate-cosmwasm-derive-0.13.1 (c (n "cosmwasm-derive") (v "0.13.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y1vpn7hc1grgpqaiwvxbf73zj9aprln3c5ds89hb99vpcwz4fsn") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.13.2 (c (n "cosmwasm-derive") (v "0.13.2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "128gns8xb4bn6kryjrkqx2pr3w4v5p30x80d4sm05r741xx1jw6y") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.14.0-alpha1 (c (n "cosmwasm-derive") (v "0.14.0-alpha1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bdi1f0h2n6sd5h26593prks892wzpa4abdibsny11qr4ph0dxy6") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.14.0-alpha2 (c (n "cosmwasm-derive") (v "0.14.0-alpha2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f3zxzlpsb93chc0yms2mj4ip0g10qvwrq4m6pc72wz6r42sqrvn") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.14.0-beta1 (c (n "cosmwasm-derive") (v "0.14.0-beta1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rxgw9v79cxkknlg23s5lsgrprpz17gbxbcssx95sag3z6vqx74y") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.14.0-beta2 (c (n "cosmwasm-derive") (v "0.14.0-beta2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b2gpw7bzwqij2q9bf7ccrv8sw29jwp7wk35shscsln8bcshxwi6") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.14.0-beta3 (c (n "cosmwasm-derive") (v "0.14.0-beta3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ywwkgh12xcdiwqnzmkbzygngvm4iw5sl6hdbp4c77h1wa2m77s9") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.14.0-beta4 (c (n "cosmwasm-derive") (v "0.14.0-beta4") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k1g6phfx7xk1gnm3f6h81vm1z9dxa00ib8v99nn7w1xbpicn4qs") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.14.0-beta5 (c (n "cosmwasm-derive") (v "0.14.0-beta5") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10dbmp2s35a500hj84dvpk8vjlgsd3ncn45a0c0v5a6jwn9kjawl") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.14.0-rc1 (c (n "cosmwasm-derive") (v "0.14.0-rc1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "006lf3bbbmnddw11hr9j67yrj8zm7c291n4vw3nxswg5z0qlpns6") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.14.0 (c (n "cosmwasm-derive") (v "0.14.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qs550is831018nzrnc832nxq0mia24c5bflqs318x2727i2vrnl") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.14.1 (c (n "cosmwasm-derive") (v "0.14.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "113vl9g55n0jjsv8jk6gn1w8hf86xxi84rsjgxd7jhsqjgdg72v2") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.15.0-alpha1 (c (n "cosmwasm-derive") (v "0.15.0-alpha1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ba6vclv2cg7awzy3x2mp6ch62k02yi7ycqdd1iwpb5j5pk9d31s") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.15.0-alpha2 (c (n "cosmwasm-derive") (v "0.15.0-alpha2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gn40gwmwl546lf7v79nh9rcxibfacxvscvcr604qrfzsg20464v") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.15.0-alpha3 (c (n "cosmwasm-derive") (v "0.15.0-alpha3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mrsjgng64p7yz901pq4f4gqcfhziq8wjla08vh0wfqaz29bk6q3") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.15.0 (c (n "cosmwasm-derive") (v "0.15.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08p5mrg1dxdqx83jfrxsqarhg07pzfsribphgr8hqfdsfymd9qpm") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.0-rc1 (c (n "cosmwasm-derive") (v "0.16.0-rc1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lkznrdbzmbg6nn96qisq399qvyk2wdy6cphx9768w93ahrx80g0") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.0-rc2 (c (n "cosmwasm-derive") (v "0.16.0-rc2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09caijnjr0fhazfvlkgprmq5s46c3d0p71acmr81p9rh0aazhrqf") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.15.1 (c (n "cosmwasm-derive") (v "0.15.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rviymny3az5rylvj8vb6sdr5cqs22i9x2s5gkw2vfbjjh5cxqrw") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.15.2 (c (n "cosmwasm-derive") (v "0.15.2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s6mp61ggqgxxjag3vf7d8777mr2rkfdqsx2bn6b7m76mq95p9vj") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.0-rc3 (c (n "cosmwasm-derive") (v "0.16.0-rc3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dy7frjy3yg0w0r2lm6n3fzyjhym286xcarcs6cnpc79n1zwdb22") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.0-rc4 (c (n "cosmwasm-derive") (v "0.16.0-rc4") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13kw25n8zqk7i7wkha3a83sfsmy0ak8say0b1zqn7q1s0wzs27g4") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.0-rc5 (c (n "cosmwasm-derive") (v "0.16.0-rc5") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qv5pbn54y55a5znm3d62nw5mbfamlmif1f6ay4625q186d9vqq2") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.0-rc6 (c (n "cosmwasm-derive") (v "0.16.0-rc6") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05v2zafi3d2v1hpdswy1c1hvzfdh7r32g70rd21zacmjmzgl63nx") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.0 (c (n "cosmwasm-derive") (v "0.16.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18n52r837affplwn5ks0zcmqlwyyxi5pqiyzzk21al4n0zznsvlz") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.1 (c (n "cosmwasm-derive") (v "0.16.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nswjsp6fwiw1fnkam527bp3yivr24m1v46bh3k7vlxp4gqass5y") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.2 (c (n "cosmwasm-derive") (v "0.16.2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1593asgg0xwq9k1yaxhk67ck7jmccy0220akkgcaa2dbnha7mhas") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.0-soon (c (n "cosmwasm-derive") (v "1.0.0-soon") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16rnisyygkg84b4czcv9xw2gmw3z81nsy7bfkn24ajc5bq6lz9hm") (f (quote (("default")))) (y #t)))

(define-public crate-cosmwasm-derive-1.0.0-soon2 (c (n "cosmwasm-derive") (v "1.0.0-soon2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z0lvvqpvpld3i7c88s6vsc02m3xfc0ys8cmhjlf7nj0z4zan709") (f (quote (("default")))) (y #t)))

(define-public crate-cosmwasm-derive-1.0.0-beta (c (n "cosmwasm-derive") (v "1.0.0-beta") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qcdaidclgnwym2gd4b7nxiwnzkviqz0kz60mxxh9kszs2cmnv7b") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.0-beta2 (c (n "cosmwasm-derive") (v "1.0.0-beta2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1llyq0hwj4g000ir86p9rq3wsqs59csydp54j1lgd8i7yipimbdb") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.0-beta3 (c (n "cosmwasm-derive") (v "1.0.0-beta3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bpgzvpfxi1k2mnl1yw58vcqvwnib4q4r0nqqww31whkzsr16rw6") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.3 (c (n "cosmwasm-derive") (v "0.16.3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vy04bss2jmxwifdcphj7dzkbf73j866iq7k9yq71pw0q0iljky0") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.0-beta4 (c (n "cosmwasm-derive") (v "1.0.0-beta4") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bhc3gkkgkhmiv0xw5sf6sm796s2vvq2p3ny0d33kjvyazpynaw3") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.0-beta5 (c (n "cosmwasm-derive") (v "1.0.0-beta5") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04r600q6hh31573k6b703h90088x9glz634xj9kmh24x9nn68hx1") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.4 (c (n "cosmwasm-derive") (v "0.16.4") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qg4gz28xz54vwq08g7m5zcbchcsb29fvzzarhwh73pgzfyk1ql5") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.5 (c (n "cosmwasm-derive") (v "0.16.5") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "042ndbabxk3x11i7znj5qxiq1mwg9z0pdyd55y1ix35rzpsyjhis") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.0-beta6 (c (n "cosmwasm-derive") (v "1.0.0-beta6") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "162m1gc6sxicaf0hbahr8ma6028lk2sdsg51b32vrf35y480ykzy") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.6 (c (n "cosmwasm-derive") (v "0.16.6") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wd7bm6fscglgg4jmqihmdjnmxp5zh3k69nfpwp1rikx5vgnv344") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.0-beta7 (c (n "cosmwasm-derive") (v "1.0.0-beta7") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i9vzqpxqpzva2hs6idhiyz7x3inlx5822ixcs70iq5vipgzr2gs") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.0-beta8 (c (n "cosmwasm-derive") (v "1.0.0-beta8") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cxp9wc34lkpsxfhk701j1ldns37g0h3xxiki03gdrbbv3ajmg2q") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-0.16.7 (c (n "cosmwasm-derive") (v "0.16.7") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17cdq7k71nv9b8cpdabghb8np8076k9zhr3f8003m95hbsnrpbqg") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.0-rc.0 (c (n "cosmwasm-derive") (v "1.0.0-rc.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ldwnj88r98gclzl17g49c3rj7az8cjmbd7mrgdcbfygbqq41ciq") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.0 (c (n "cosmwasm-derive") (v "1.0.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1941y2gagsz8gdynz64w0q8qvdn9nqd76vkbwh03wahac8kyadjb") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.0-rc.1 (c (n "cosmwasm-derive") (v "1.1.0-rc.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15qf8aszjf2cf7vq48vlpaa07a5aq07rzkhnnxgcf2pmhxgnn6bk") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.0-rc.2 (c (n "cosmwasm-derive") (v "1.1.0-rc.2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ly11mz6h5vpfzhym733fp4cvgxcxg15vb022nqyprgnbwns27rh") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.0 (c (n "cosmwasm-derive") (v "1.1.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1553zfnllanfhds49k15ifkjhxc048mzl911ksgix0w5mpifnlwh") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.1 (c (n "cosmwasm-derive") (v "1.1.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03gir6nidlkglfdamfwadx0da5ah418hfs3y7cbxcy4z7013mbqg") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.2 (c (n "cosmwasm-derive") (v "1.1.2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0brl4r56vd91lyyincby2z1k0jwvp8x2v3b5dxiy9lqnldv4c6zq") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.3 (c (n "cosmwasm-derive") (v "1.1.3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0avlf08914gz3mscpgjkkwfk2qcmdf4b049riy8vzz7zza88gl0n") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.4 (c (n "cosmwasm-derive") (v "1.1.4") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08gslm00wwl8i58qpkvgpkr50s2579mpmzldam65ahpjac3ha90a") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.5 (c (n "cosmwasm-derive") (v "1.1.5") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hzf16vkgz3bbcfa6kkf0mvixppy71i69lzvr1lcwjwag97rzdlf") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.6 (c (n "cosmwasm-derive") (v "1.1.6") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pxm0ryq3mi7l9ssl82l1q74kasgy8mxfgsrdg6p7lbsz1n72lm5") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.7 (c (n "cosmwasm-derive") (v "1.1.7") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jni8yywxs9y46rpgb6qzdzfbv3ww8wbwg9hf1dy6pw7x1fq4ak2") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.8 (c (n "cosmwasm-derive") (v "1.1.8") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ci7gwjybsv5y0z469n1yxb68ib6ss2xv7mkk8qq2s9nrj4v4s45") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.9 (c (n "cosmwasm-derive") (v "1.1.9") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0375494cqyfbs3jkzzcpgvvvsc82vzbi12qd8jkbypvy3zak1jkg") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.0-beta.0 (c (n "cosmwasm-derive") (v "1.2.0-beta.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h9x6l11w8dwxadl8q9209yb1l5rydbd5hpsr773fwaiw39nwi0a") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.0-beta.1 (c (n "cosmwasm-derive") (v "1.2.0-beta.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0va9l9p0na93h0aqqjvlzhsikbygp7zbq9hbn7x7r7c088ypqzfs") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.0-rc.1 (c (n "cosmwasm-derive") (v "1.2.0-rc.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18m6wkbjw2jjffh7x9p3axkkga884arx5ff9fd87drax62aajvgl") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.0 (c (n "cosmwasm-derive") (v "1.2.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mkdsl4h5svipzqj1plbgaz4x09jhy4zygfislzs40nn0bi7hsp2") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.1 (c (n "cosmwasm-derive") (v "1.2.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05a17vfs2s8f2x5y2bqxcm9dnl84z51d1lz40a20kl76j6wfxaym") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.2 (c (n "cosmwasm-derive") (v "1.2.2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0brr1rc5bj30ky21zx1s39bw4md53l6ijmf5wd2b06qrg9nmz2gi") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.3 (c (n "cosmwasm-derive") (v "1.2.3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b8sph1vrl1gq5v6z2m30p8c5zwq5s22gkrjlv891vqq19qlzrn2") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.4 (c (n "cosmwasm-derive") (v "1.2.4") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14nmqr7x4ikzqjah66d3nm7wbddi6gdpzikd2bcd4r31fgvpml0x") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.1.10 (c (n "cosmwasm-derive") (v "1.1.10") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bid5i4hbmq218bkzpqy7bhzxvkipkqsaxnni9pr9sgw64iwxj3w") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.0.1 (c (n "cosmwasm-derive") (v "1.0.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wvhvf56jghr8js6v5r99jcvp8lv1glhzzrkbbf7k1nv0z0a5ni5") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.5 (c (n "cosmwasm-derive") (v "1.2.5") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wy7w2sxmb3lianmsxma8a344rn92f2zwr16gdgjkixza477z7qw") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.6 (c (n "cosmwasm-derive") (v "1.2.6") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "001mkb90yzlpnxcpk11dx7jlgxnmh47j3d7bgg8hvh4jiiwy4zis") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.7 (c (n "cosmwasm-derive") (v "1.2.7") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a5h2l2g59793vpm1ka1ck3z9bzri2cnmm9llbhxm8s1kzh2ryx0") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.3.0-rc.0 (c (n "cosmwasm-derive") (v "1.3.0-rc.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09zvy3n0a7ak68i27zprc02pvrwzc2kyf97dxyk2k8l0s45nv9av") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.3.0 (c (n "cosmwasm-derive") (v "1.3.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11153xjqm23glcnzh0jswmccp5iwkfz1gz2834i42ywxq3rn3hyy") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.3.1 (c (n "cosmwasm-derive") (v "1.3.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kiig0s003lsjflbw3b2ysirp6a69rzk9kz0b9186lnaraivcv99") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.3.2 (c (n "cosmwasm-derive") (v "1.3.2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00z3rzzyhzrcg830f45x05k9c2dfjyph3xlnzcd30allqqq45c83") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.3.3 (c (n "cosmwasm-derive") (v "1.3.3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p1hgg3kbyhmsbhiaqavq73la94c8m8hmkbhyzbcdj578m5v9s3w") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.4.0-beta.1 (c (n "cosmwasm-derive") (v "1.4.0-beta.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ljdm1isd68a1rfj373ayna3fg6xnz75glw4mkm78gjgxyzn83x3") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.4.0-rc.1 (c (n "cosmwasm-derive") (v "1.4.0-rc.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "010w5dy7206rkx3hj64agnm41q24ywgb6m18s852f79gsaa82s9y") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.4.0 (c (n "cosmwasm-derive") (v "1.4.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xdapi9lblnnmc2si6a2i891xwwwh0yw01rbkn2443pnjb92sgf7") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.4.1 (c (n "cosmwasm-derive") (v "1.4.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nq551bk1n5j80qw436l76jb89ifsxzkidhx45mpvabf90j986bf") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.5.0-rc.0 (c (n "cosmwasm-derive") (v "1.5.0-rc.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hfb0y3rq21bdvb517jmap1ywzpxbclfjq62cacwgyqd8hvq1lb8") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.5.0 (c (n "cosmwasm-derive") (v "1.5.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11wn4h3cgqwc9y5m9i3fbn5i0fp9c40fsmcd040dxvz6ca8kx9zy") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-2.0.0-beta.0 (c (n "cosmwasm-derive") (v "2.0.0-beta.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5dxpi2jb8s13lksdv44vwq4vyp99yxikgm83gljgfv8113ly9k") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.5.1 (c (n "cosmwasm-derive") (v "1.5.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1czgk09fxf1pqb27syk7lajkr1ig7z61b84bsayyl1l4frdn6rfa") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.4.2 (c (n "cosmwasm-derive") (v "1.4.2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w4hbcjlpa2c6zzy3bz70fvzd7vyiwgkpblkawx9yfv36i3l42nv") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.3.4 (c (n "cosmwasm-derive") (v "1.3.4") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05763wiv8pixz4rx01sf2i92pmqfwn1bflsc0c5x92vz4rzkw65h") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.2.8 (c (n "cosmwasm-derive") (v "1.2.8") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11kiwdbilyrkl2q16n14j3asdglvhhkxgsx8zlr100lh3daf9q1g") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.5.2 (c (n "cosmwasm-derive") (v "1.5.2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j9fggji8527qclp47md3qd049c3nxc9mbafsk3bwjix5y2yras0") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.4.3 (c (n "cosmwasm-derive") (v "1.4.3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rysjrgr3a241p6spkxnf24q0ilig1lp8s1b7fcmlmiwx4fw38np") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-2.0.0-beta.1 (c (n "cosmwasm-derive") (v "2.0.0-beta.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14ncbfhj12wm21zr2m2fzk83iidd9l2iiv975l37q4rfpjmimvc3") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.5.3 (c (n "cosmwasm-derive") (v "1.5.3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g4b105nz1sxxg568yf50jh9g1kaxfyyrdaj3jqxlfxx63ip4pmw") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-2.0.0-rc.0 (c (n "cosmwasm-derive") (v "2.0.0-rc.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nim0b3324f33chl8xsis3b668s2rp2r2rdcr2vn39kq6l6ngnr9") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-2.0.0-rc.1 (c (n "cosmwasm-derive") (v "2.0.0-rc.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jxzz4qvggzl2scjyzzjrfw6fzkfarz6hrjmsmfi5dnwgljafdkl") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-2.0.0 (c (n "cosmwasm-derive") (v "2.0.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01mxzglnya8kd7a40ph289vmm25v4hh0g3syqx3bzazy8m0994nn") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-2.0.1 (c (n "cosmwasm-derive") (v "2.0.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q4shhs72gz743p7g377jf0f1bv3zw8k5nrhi2vk7w801d4aw0fv") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-2.0.2 (c (n "cosmwasm-derive") (v "2.0.2") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sbpismhv75jvc2b7ga48bbydj7l0ffxkj22h7h2wjx2q67r3c5q") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.5.4 (c (n "cosmwasm-derive") (v "1.5.4") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wx3d3ds9m5fkp0f8kykcl6zc9504m88jjk6xq4yh01v1k8wx1n5") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.4.4 (c (n "cosmwasm-derive") (v "1.4.4") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ax9dc7i0n62xzid4rhgs9hvyya32id9h3m6gxcfz1qzh63dn254") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-2.0.3 (c (n "cosmwasm-derive") (v "2.0.3") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n7fhv05lm1ajijgsgjmr7gq4gx2nfidas09bm95bwzxnr6hj9ci") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.5.5 (c (n "cosmwasm-derive") (v "1.5.5") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fjgmagkw2r1sqn77a2q2yshf41d4srrs39hizh25h9ilbkrhbi4") (f (quote (("default"))))))

(define-public crate-cosmwasm-derive-1.4.5 (c (n "cosmwasm-derive") (v "1.4.5") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xavcr5c8g5cwqgqad1yvibx9kinnkl0shnvsghxncdxq6mkzl1f") (f (quote (("default"))))))

