(define-module (crates-io co sm cosmos) #:use-module (crates-io))

(define-public crate-cosmos-0.0.1 (c (n "cosmos") (v "0.0.1") (d (list (d (n "hyper") (r "^0.3.7") (d #t) (k 0)))) (h "0rca2y7j25kszwrdakgfb6mh5v4r078nqhrf2bxpzf8lsz22nnsv")))

(define-public crate-cosmos-0.0.2 (c (n "cosmos") (v "0.0.2") (d (list (d (n "hyper") (r "^0.3.8") (d #t) (k 0)))) (h "0zy9qxabyzsx7xx3b2f2a62b594am5h9jvz1sggcsf0pm1rm823x")))

(define-public crate-cosmos-0.0.3 (c (n "cosmos") (v "0.0.3") (d (list (d (n "curl") (r "^0.2.7") (d #t) (k 0)))) (h "14kfmcmx3fzrfx3k8i3x9knm4f8rkfr00n1crnpxvkzkkfmhk56z")))

(define-public crate-cosmos-0.0.4 (c (n "cosmos") (v "0.0.4") (d (list (d (n "curl") (r "^0.2.7") (d #t) (k 0)))) (h "173m2jghhr0hn6j9sp0qrf7yrmxa6c3bak8hzyyr1vq0bnb7iasv")))

(define-public crate-cosmos-0.0.5 (c (n "cosmos") (v "0.0.5") (d (list (d (n "curl") (r "^0.2.7") (d #t) (k 0)))) (h "0r6fg2qyblqixzn6wcw30wzx1hgjw2kpwbakbacfd6iffzrvck9i")))

(define-public crate-cosmos-0.0.6 (c (n "cosmos") (v "0.0.6") (d (list (d (n "curl") (r "^0.2.7") (d #t) (k 0)))) (h "1c7lg6l88lpapirq3qgrz2wad8pknhh2az6rs37qn3h8g2c8763v")))

(define-public crate-cosmos-0.0.7 (c (n "cosmos") (v "0.0.7") (d (list (d (n "curl") (r "^0.2.8") (d #t) (k 0)))) (h "10cavns9iywrngrg4blzh0pgkrhrhaqp2hx0q7rch0z91d4mbf1l")))

(define-public crate-cosmos-0.0.8 (c (n "cosmos") (v "0.0.8") (d (list (d (n "request") (r "^0.0.1") (d #t) (k 0)))) (h "08c2d45nsflxz6yvcrhjcz669q4jnj59p7py88k75h1l7g0sphyq")))

(define-public crate-cosmos-0.0.9 (c (n "cosmos") (v "0.0.9") (d (list (d (n "request") (r "^0.0.2") (d #t) (k 0)))) (h "1f39vjr2bb8bd751xbqgh2fgb98ksq11j4rch7k1ixyx813xcmhd")))

(define-public crate-cosmos-0.0.10 (c (n "cosmos") (v "0.0.10") (d (list (d (n "request") (r "^0.0.2") (d #t) (k 0)))) (h "1w9asv9m25964f0x0dxvc3kf6fhprs74ws7dkzzpsxizpq73n8w6")))

(define-public crate-cosmos-0.0.11 (c (n "cosmos") (v "0.0.11") (d (list (d (n "request") (r "^0.0.3") (d #t) (k 0)))) (h "0sbi93kdm4hbzfzj6m2nqnr6pqx0v8hrh1n6cslsvryhpmdryl6l")))

(define-public crate-cosmos-0.0.12 (c (n "cosmos") (v "0.0.12") (d (list (d (n "request") (r "^0.0.4") (d #t) (k 0)))) (h "1jp3m486d769swzgc334x53y1vjypkkhra7xy5y7kakgg0wlgcy1")))

(define-public crate-cosmos-0.0.13 (c (n "cosmos") (v "0.0.13") (d (list (d (n "request") (r "^0.0.5") (d #t) (k 0)))) (h "1l3xm5q1mf3m1r13ylh7x6ngnsa07jcky495m4yh9y063k9385na")))

(define-public crate-cosmos-0.0.14 (c (n "cosmos") (v "0.0.14") (d (list (d (n "request") (r "^0.0.6") (d #t) (k 0)))) (h "0zc0kv9bmcn8r2wg1ha42nhc806hkj0z95m5lvfd5qz3841fjlgw")))

(define-public crate-cosmos-0.0.15 (c (n "cosmos") (v "0.0.15") (d (list (d (n "request") (r "^0.0.7") (d #t) (k 0)))) (h "0cg9p0pafnhmkx57p504wih0s6imsi5vkf4w4y42kaix9bghgk05")))

(define-public crate-cosmos-0.0.16 (c (n "cosmos") (v "0.0.16") (d (list (d (n "request") (r "^0.0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "1ybm46mj01j0c455nc5slfwqiw4x4v36rd8qxjbgs33nywi799mb")))

(define-public crate-cosmos-0.0.17 (c (n "cosmos") (v "0.0.17") (d (list (d (n "request") (r "^0.0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0fwflam3r9kw52293j0zghms4jyxfwh119xqlfskvvkwajdpd7wp")))

