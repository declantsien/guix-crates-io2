(define-module (crates-io co sm cosmian-wit-parser) #:use-module (crates-io))

(define-public crate-cosmian-wit-parser-0.1.0 (c (n "cosmian-wit-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "id-arena") (r "^2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "wast") (r "^33") (o #t) (k 0)) (d (n "witx") (r "^0.8.8") (o #t) (d #t) (k 0)))) (h "0hqz04fz7bajb3gx74a3jyif5cm199015p5ff6wd79wvm4xdb23g") (f (quote (("witx-compat" "witx" "wast"))))))

