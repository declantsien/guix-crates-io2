(define-module (crates-io co sm cosmic-time) #:use-module (crates-io))

(define-public crate-cosmic-time-0.1.0 (c (n "cosmic-time") (v "0.1.0") (d (list (d (n "iced") (r "^0.8.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "iced_core") (r "^0.8.1") (d #t) (k 0)) (d (n "iced_futures") (r "^0.6.0") (d #t) (k 0)) (d (n "iced_native") (r "^0.9.1") (d #t) (k 0)) (d (n "iced_style") (r "^0.7.0") (d #t) (k 0)))) (h "0ws8nin6jhz0xv4zgp2qjncfn6h6vslgxm5pj73sddnyacrxn8sh")))

(define-public crate-cosmic-time-0.1.1 (c (n "cosmic-time") (v "0.1.1") (d (list (d (n "iced") (r "^0.8.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "iced_core") (r "^0.8.1") (d #t) (k 0)) (d (n "iced_futures") (r "^0.6.0") (d #t) (k 0)) (d (n "iced_native") (r "^0.9.1") (d #t) (k 0)) (d (n "iced_style") (r "^0.7.0") (d #t) (k 0)))) (h "01dis61d24n9801cmrjhjvbiabczfa815gba3l1n0nl3d4wr3nyv")))

(define-public crate-cosmic-time-0.1.2 (c (n "cosmic-time") (v "0.1.2") (d (list (d (n "iced") (r "^0.8.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "iced_core") (r "^0.8.1") (d #t) (k 0)) (d (n "iced_futures") (r "^0.6.0") (d #t) (k 0)) (d (n "iced_native") (r "^0.9.1") (d #t) (k 0)) (d (n "iced_style") (r "^0.7.0") (d #t) (k 0)))) (h "16gqi49fbrnz61vlsp8zc4s5842kdjvx2bj9lpmaxw6g0n13p7n2")))

(define-public crate-cosmic-time-0.2.0 (c (n "cosmic-time") (v "0.2.0") (d (list (d (n "iced") (r "^0.9.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "iced_core") (r "^0.9.0") (d #t) (k 0)) (d (n "iced_futures") (r "^0.6.0") (d #t) (k 0)) (d (n "iced_native") (r "^0.10.1") (d #t) (k 0)) (d (n "iced_style") (r "^0.8.0") (d #t) (k 0)))) (h "04cc93iq3hnma0l0sdlk76klrzw3c2dads7x77nqj8qmky4996r6")))

