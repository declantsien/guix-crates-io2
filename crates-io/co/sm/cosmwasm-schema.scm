(define-module (crates-io co sm cosmwasm-schema) #:use-module (crates-io))

(define-public crate-cosmwasm-schema-0.8.0-alpha1 (c (n "cosmwasm-schema") (v "0.8.0-alpha1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w08pf3ysdpy8cc62y3s85zcbr7bhvr8ix0rvigl02fihgvi6aaj")))

(define-public crate-cosmwasm-schema-0.8.0-alpha2 (c (n "cosmwasm-schema") (v "0.8.0-alpha2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "029kqgc7j58imcisphb83d2b9w61iq376ww5v7b1ji1j22kvjfxv")))

(define-public crate-cosmwasm-schema-0.8.0-alpha3 (c (n "cosmwasm-schema") (v "0.8.0-alpha3") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "023l7dr3cfnhqbg7lvnh4llc5knarfrii0137ilsw5qz9k734ip8")))

(define-public crate-cosmwasm-schema-0.8.0-rc1 (c (n "cosmwasm-schema") (v "0.8.0-rc1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "133577p946zr8gcc0nv4hijn5imq15njzhix0xr88xpa64wgpsrg")))

(define-public crate-cosmwasm-schema-0.8.0 (c (n "cosmwasm-schema") (v "0.8.0") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15k2ym85izrq6klpy9da3ddp91qzwfa7yqfizjf72ysawqn5i63x")))

(define-public crate-cosmwasm-schema-0.8.1 (c (n "cosmwasm-schema") (v "0.8.1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q9lxqf23n3jn1j06gynzc2v9ya0mr0xd7p4xb75zaxb2bclnp5z")))

(define-public crate-cosmwasm-schema-0.9.0-alpha2 (c (n "cosmwasm-schema") (v "0.9.0-alpha2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "183zx42ya0qws63rwhvcg33nqsl3vkzi255kg0ard2wwdkgi12am")))

(define-public crate-cosmwasm-schema-0.9.0-alpha3 (c (n "cosmwasm-schema") (v "0.9.0-alpha3") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b7g72il2n4q2yfkp4h1nbigf5xhl93j0fxzr9girccq4av3qgf7")))

(define-public crate-cosmwasm-schema-0.9.0 (c (n "cosmwasm-schema") (v "0.9.0") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vs71mqvmzhmn5vyjzipvxw6p91l96w9x99p3439nwrxzq75r03q") (y #t)))

(define-public crate-cosmwasm-schema-0.9.1 (c (n "cosmwasm-schema") (v "0.9.1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04nvlgwr9xjs4f44djyfj2vj6ypqhdw043s37829d6mb9pjfv48h")))

(define-public crate-cosmwasm-schema-0.9.2 (c (n "cosmwasm-schema") (v "0.9.2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09zsbl4vryvv4ahnv00w93sf61l5d3wvsxqm60c7k2bh01n2mzm3")))

(define-public crate-cosmwasm-schema-0.9.3 (c (n "cosmwasm-schema") (v "0.9.3") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lw85wbzm70m194n02ww8m0l6pk8adqsipqn9fjzjzf4zrdxbcvx")))

(define-public crate-cosmwasm-schema-0.9.4 (c (n "cosmwasm-schema") (v "0.9.4") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x2kpfpr2xkqr13qqglkaaaaakv40ssshy02rzvw22a9bv5vyjmq")))

(define-public crate-cosmwasm-schema-0.10.0-alpha1 (c (n "cosmwasm-schema") (v "0.10.0-alpha1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01w4bac6v6kxis9dd6yjmw1nqghikbnkwr7xynsyc0jv7zqcj7yn")))

(define-public crate-cosmwasm-schema-0.10.0-alpha2 (c (n "cosmwasm-schema") (v "0.10.0-alpha2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cjpilg6mw4cpyfj1safhzhl7dzamzhb2vf60l95anbm5jfn47hk")))

(define-public crate-cosmwasm-schema-0.10.0-alpha3 (c (n "cosmwasm-schema") (v "0.10.0-alpha3") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07mspnlr6hhjdhkwp1sf02m9awp8v8ijs7p6jnc4a26mh8w3x9k7")))

(define-public crate-cosmwasm-schema-0.10.0 (c (n "cosmwasm-schema") (v "0.10.0") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1718yjr90g3w2inj8pkcb2yzxialmrl4fvb0fsjdqhpmzdidv4hm")))

(define-public crate-cosmwasm-schema-0.10.1 (c (n "cosmwasm-schema") (v "0.10.1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pr99ijcn27ilkkcivirwbf0pf1fkshq976h548s5slpl5p5i5i1")))

(define-public crate-cosmwasm-schema-0.11.0-alpha1 (c (n "cosmwasm-schema") (v "0.11.0-alpha1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cpbisbc3n68ak2m3ijpp4fgsbf6fc9dr770hzphxwpn9fr610ad")))

(define-public crate-cosmwasm-schema-0.11.0-alpha2 (c (n "cosmwasm-schema") (v "0.11.0-alpha2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gn5wds62z587j396bvi031wnnwazqqiwpmg4l4m83f8nly9n208")))

(define-public crate-cosmwasm-schema-0.11.0-alpha3 (c (n "cosmwasm-schema") (v "0.11.0-alpha3") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g3xqrnr7z1svsgqrh8hq3zr152i0hs1cn3djfr934xjbpsq8p8f")))

(define-public crate-cosmwasm-schema-0.11.0-alpha4 (c (n "cosmwasm-schema") (v "0.11.0-alpha4") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fs1qrwpkjhgivlcd45csipzcks682wm7411qmcnbhgmf06wlp40")))

(define-public crate-cosmwasm-schema-0.11.0 (c (n "cosmwasm-schema") (v "0.11.0") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "020jg38walymwk8dfjcyrp0hdpj0xbnxqzznwcsk1jdfr1piy6i5")))

(define-public crate-cosmwasm-schema-0.11.1 (c (n "cosmwasm-schema") (v "0.11.1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "009ac6msnlziz965x2vrm0jfh20zz1w5nqvdniny69996ilc0s3p")))

(define-public crate-cosmwasm-schema-0.11.2 (c (n "cosmwasm-schema") (v "0.11.2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ppp5sm5wpgf5189gx9y13djmzvzv2xs6iyjkra3jqqdhdaadw6x")))

(define-public crate-cosmwasm-schema-0.12.0-alpha1 (c (n "cosmwasm-schema") (v "0.12.0-alpha1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g1bgvahjwz5iil1rsdc4lq4hl9ggv882snhj0qyi1xfqfdy6wzv")))

(define-public crate-cosmwasm-schema-0.12.0-alpha2 (c (n "cosmwasm-schema") (v "0.12.0-alpha2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hzkyk1h2x70y2qhsr6pi94vfvy6nzwr87l244k78hrm73aj968z")))

(define-public crate-cosmwasm-schema-0.12.0-alpha3 (c (n "cosmwasm-schema") (v "0.12.0-alpha3") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dz0llb1ix17n3qfcja0bc398wi6s3in9k63wpwgqagm1276s2a2")))

(define-public crate-cosmwasm-schema-0.12.0 (c (n "cosmwasm-schema") (v "0.12.0") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14nhn9skxj3xsj31pf8v6zfls55mc1y65vgbjijqa3zv1bahd8iz")))

(define-public crate-cosmwasm-schema-0.12.1 (c (n "cosmwasm-schema") (v "0.12.1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ppj253wvqrg5grr8mvwdiddkzdmiwfz9xjla0dxh1ywln9hnmh1")))

(define-public crate-cosmwasm-schema-0.12.2 (c (n "cosmwasm-schema") (v "0.12.2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16y8ji0za6diqnig0rb26j6xba5im041m83kdlqzgsqbdxq48ppv")))

(define-public crate-cosmwasm-schema-0.13.0-alpha1 (c (n "cosmwasm-schema") (v "0.13.0-alpha1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0907bwpaz6qk8c0lpvblhsl7ydmkcgxrvcl6rhp4g30g41xlihmn")))

(define-public crate-cosmwasm-schema-0.13.0-alpha2 (c (n "cosmwasm-schema") (v "0.13.0-alpha2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mha0b0sbrkfm8xkckgb0n2da8fba5k7r8vixc2l8rrpbj122j4b")))

(define-public crate-cosmwasm-schema-0.13.0 (c (n "cosmwasm-schema") (v "0.13.0") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "049l8hlpf8yb53r5593fwjnl6q46sw88zj1kh4arfkg7lrf44xn6")))

(define-public crate-cosmwasm-schema-0.13.1 (c (n "cosmwasm-schema") (v "0.13.1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w6ndy3kkwf0vrvb23s9f1dsfw25ncxb1swd3ay3nkkk9kp4prd3")))

(define-public crate-cosmwasm-schema-0.13.2 (c (n "cosmwasm-schema") (v "0.13.2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s1cmf5hhvbzjh2ik4ifbb6vsd4bf99g3q703gdczr9rr30md2x9")))

(define-public crate-cosmwasm-schema-0.14.0-alpha1 (c (n "cosmwasm-schema") (v "0.14.0-alpha1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05qq9alphq5pb0xwkpc2ij7x5d75k5j5xrx0mjj98k0p9hcwvdsn")))

(define-public crate-cosmwasm-schema-0.14.0-alpha2 (c (n "cosmwasm-schema") (v "0.14.0-alpha2") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jd4mkng5qn8dlpn649dribijp8hff2px5anfc505d5g8b73zfzv")))

(define-public crate-cosmwasm-schema-0.14.0-beta1 (c (n "cosmwasm-schema") (v "0.14.0-beta1") (d (list (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "165d3fjfrqsgchzmpcz4afqfq07p2w06p9xjbgcvcnz8d2fivpm0")))

(define-public crate-cosmwasm-schema-0.14.0-beta2 (c (n "cosmwasm-schema") (v "0.14.0-beta2") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mvzz8166gh3k4lqk3x8a6zd535aws6mjz9fqv1gcm3wvw2qz9db")))

(define-public crate-cosmwasm-schema-0.14.0-beta3 (c (n "cosmwasm-schema") (v "0.14.0-beta3") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ppvpdpy7jawl950ad84ksr3p8m6hc0hk240w1xq9r8mr10q27mb")))

(define-public crate-cosmwasm-schema-0.14.0-beta4 (c (n "cosmwasm-schema") (v "0.14.0-beta4") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a1daypsc194varzyalvc3hy632b7mbmi41hdgblblsqj6wpy1vg")))

(define-public crate-cosmwasm-schema-0.14.0-beta5 (c (n "cosmwasm-schema") (v "0.14.0-beta5") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i55csiqx1p642wa29fxv7cl12yplj04rjxybgga5snd3p93wai9")))

(define-public crate-cosmwasm-schema-0.14.0-rc1 (c (n "cosmwasm-schema") (v "0.14.0-rc1") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r7ia4418x6xm6zq38ic2zcf6fbj40b3h3j08101dcl9n62bdy62")))

(define-public crate-cosmwasm-schema-0.14.0 (c (n "cosmwasm-schema") (v "0.14.0") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qdsdzrigkgli4w7ghpr4i2cipvwhmjjlca3z5b7nk9vrajadafn")))

(define-public crate-cosmwasm-schema-0.14.1 (c (n "cosmwasm-schema") (v "0.14.1") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w58w4i3qdq2znwwlir32pqbiprnjybjpzr3g7dp2djqkgn9w584")))

(define-public crate-cosmwasm-schema-0.15.0-alpha1 (c (n "cosmwasm-schema") (v "0.15.0-alpha1") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01ga9fghri85mq342b0llq71gsbihabkimbdxjgw2m7fmr49dfi8")))

(define-public crate-cosmwasm-schema-0.15.0-alpha2 (c (n "cosmwasm-schema") (v "0.15.0-alpha2") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hjwj593r1bhpvi3y7jw0zdq0jadrhrg3anlb7hjkgrw3jla5l7j")))

(define-public crate-cosmwasm-schema-0.15.0-alpha3 (c (n "cosmwasm-schema") (v "0.15.0-alpha3") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z61d3g80r2frf0f3dy227sizg0sbvw33fq20n56x0yi0cr2lflm")))

(define-public crate-cosmwasm-schema-0.15.0 (c (n "cosmwasm-schema") (v "0.15.0") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hrsgfqgpvm4z6ab218w5qnmqjvbgzd0xc4cl54axa8r0gnyhsnn")))

(define-public crate-cosmwasm-schema-0.16.0-rc1 (c (n "cosmwasm-schema") (v "0.16.0-rc1") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "077g56wzaqm3xv808f9dc92rb40m9plvrg1vcvwaxzb4j9hil4qq")))

(define-public crate-cosmwasm-schema-0.16.0-rc2 (c (n "cosmwasm-schema") (v "0.16.0-rc2") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pqbyy4n0ahi80gzdfr6v6ji6kmdzhm4c5dfjfyf6a7l0w5w80lq")))

(define-public crate-cosmwasm-schema-0.15.1 (c (n "cosmwasm-schema") (v "0.15.1") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1amm4pn2ry2375pl48iyyhqi9h85s16ln9qd2m3wq7rkc603f58f")))

(define-public crate-cosmwasm-schema-0.15.2 (c (n "cosmwasm-schema") (v "0.15.2") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rfh8yhq5q7kis6qrjyispkgklj1yb5k9w46b2c5ji10w2qwh6lb")))

(define-public crate-cosmwasm-schema-0.16.0-rc3 (c (n "cosmwasm-schema") (v "0.16.0-rc3") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00502p9sci36x5sywkwbcmhqljfhc5pw106pgk3jwsmil3kkzja4")))

(define-public crate-cosmwasm-schema-0.16.0-rc4 (c (n "cosmwasm-schema") (v "0.16.0-rc4") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qmabfdhp9s2229mblfyc7lpbimv1fa44bnp3sdg2jjxmvxxd3si")))

(define-public crate-cosmwasm-schema-0.16.0-rc5 (c (n "cosmwasm-schema") (v "0.16.0-rc5") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mp325hdy41a1gwjdkldypz46k978f15ra1gf76hkdjd7nzj97pd")))

(define-public crate-cosmwasm-schema-0.16.0-rc6 (c (n "cosmwasm-schema") (v "0.16.0-rc6") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lmnrgzqd3shih9bb159smxq3mxf4acp6xkxrqa933w5ynwhz0ch")))

(define-public crate-cosmwasm-schema-0.16.0 (c (n "cosmwasm-schema") (v "0.16.0") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p7nv9x2nrdvs61nssmgqb5q07mmvhxc8ybn9bng3rw8l3pa36xc")))

(define-public crate-cosmwasm-schema-0.16.1 (c (n "cosmwasm-schema") (v "0.16.1") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xx6149nlc917nvpf1q302jcan75bhdjqlv33c77m3m2prh32kjk")))

(define-public crate-cosmwasm-schema-0.16.2 (c (n "cosmwasm-schema") (v "0.16.2") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w0b3yk9dpprz6wl494dmahsw68vlvw5dsn002j0x5vc32y30j60")))

(define-public crate-cosmwasm-schema-1.0.0-soon (c (n "cosmwasm-schema") (v "1.0.0-soon") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q17x702yjkgnhk8fwwwga9dhljm7qwjy0v5kkcn7qr2xka5i5fq") (y #t)))

(define-public crate-cosmwasm-schema-1.0.0-soon2 (c (n "cosmwasm-schema") (v "1.0.0-soon2") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1546ga2fcr2xdm26g934wkppxrlw7q6w0fmg4g3qbg36zc687jg0") (y #t)))

(define-public crate-cosmwasm-schema-1.0.0-beta (c (n "cosmwasm-schema") (v "1.0.0-beta") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c6c67fp1w2fg8j8njhs6gih480chzs1m4yv7pfsr51zjgvb7r12")))

(define-public crate-cosmwasm-schema-1.0.0-beta2 (c (n "cosmwasm-schema") (v "1.0.0-beta2") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l3gm8hhlybzbc7zr6jk6apfa1dy9ps4vhkcvdcq6gzy8nfv2lpy")))

(define-public crate-cosmwasm-schema-1.0.0-beta3 (c (n "cosmwasm-schema") (v "1.0.0-beta3") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h24gsgwpllamxry403rz0za81r1lsa29lmyq9lk56n0cf1952w1")))

(define-public crate-cosmwasm-schema-0.16.3 (c (n "cosmwasm-schema") (v "0.16.3") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xrns5wfl90jhjibvr2ry3sjwajs1f2h1z5rv3aggh77lvcbs4dy")))

(define-public crate-cosmwasm-schema-1.0.0-beta4 (c (n "cosmwasm-schema") (v "1.0.0-beta4") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x8zflr1z0j0pv7v3qisz42xg7576mwsg0yvb56c6cx9li4rgpp5")))

(define-public crate-cosmwasm-schema-1.0.0-beta5 (c (n "cosmwasm-schema") (v "1.0.0-beta5") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07cj0i612bykh3bld9jlahfxr87xdf3hbvcniqs0sqj2gjw42a9v")))

(define-public crate-cosmwasm-schema-0.16.4 (c (n "cosmwasm-schema") (v "0.16.4") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w9n79c110wv7pgrbww4f1kxrvc37ksid9zpyf7xazbiphaivpdx")))

(define-public crate-cosmwasm-schema-0.16.5 (c (n "cosmwasm-schema") (v "0.16.5") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i5yd0apmp7iysbjc1lvr2ql92a2dkhfxg43m31f2i732p736xvm")))

(define-public crate-cosmwasm-schema-1.0.0-beta6 (c (n "cosmwasm-schema") (v "1.0.0-beta6") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1213h9ng8lr167d7hzsspabvg3h8kl268pw4i4vlw5cfkam34x29")))

(define-public crate-cosmwasm-schema-0.16.6 (c (n "cosmwasm-schema") (v "0.16.6") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vffsb19xb9mzdakv00gi0fka1mzqi57lp0b4zk6bay5mhzcbbwh")))

(define-public crate-cosmwasm-schema-1.0.0-beta7 (c (n "cosmwasm-schema") (v "1.0.0-beta7") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nv3r7fkqgq7j9r0ylx826aqsg0q13imjsdhdimhn6djwxk9ixv3")))

(define-public crate-cosmwasm-schema-1.0.0-beta8 (c (n "cosmwasm-schema") (v "1.0.0-beta8") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rzd2rj0q3m1kaismqzr2jfgpxlbgb3z47hp4hxn3n37ashgcx8d")))

(define-public crate-cosmwasm-schema-0.16.7 (c (n "cosmwasm-schema") (v "0.16.7") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "044cvh3aclvb0fv7hbp4d6ldpzd90xa6w143bq9wamfqp7pvs8dh")))

(define-public crate-cosmwasm-schema-1.0.0-rc.0 (c (n "cosmwasm-schema") (v "1.0.0-rc.0") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jna9qll9dfay3k6wk1g9vwbnxbk5yyzv9kxaka69g17xn7nb58g")))

(define-public crate-cosmwasm-schema-1.0.0 (c (n "cosmwasm-schema") (v "1.0.0") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nk17bm7n5s8qwd3kjan1m5dv0gzl4ivf4l80si4f6i3mnxq0bkp")))

(define-public crate-cosmwasm-schema-1.1.0-rc.1 (c (n "cosmwasm-schema") (v "1.1.0-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.0-rc.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1d7l9kc7rmk5d2y518mgwal15gws6gvj875z97l3c8s9zn008hiy")))

(define-public crate-cosmwasm-schema-1.1.0-rc.2 (c (n "cosmwasm-schema") (v "1.1.0-rc.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.0-rc.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "069jmxw78gqh554pzk940c4kaxpcz4c0ym5jjdqghwsh8i592g0b")))

(define-public crate-cosmwasm-schema-1.1.0 (c (n "cosmwasm-schema") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0n6lw5jxab2dwk1r7zwypg2pbycfq5djpk04lcpg1358k332yx4c")))

(define-public crate-cosmwasm-schema-1.1.1 (c (n "cosmwasm-schema") (v "1.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0njy4rjbl4mmypaxj1kdgsqf0h4nfsw7j98ar458nhahx6g3xlfm")))

(define-public crate-cosmwasm-schema-1.1.2 (c (n "cosmwasm-schema") (v "1.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x8sggqw32rgnwl28wz0f9smww3xv837xfzqa69djzwqgh77p4c8")))

(define-public crate-cosmwasm-schema-1.1.3 (c (n "cosmwasm-schema") (v "1.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0as1i0filqcw7fyml9mshylzd3bsigz8y1m3j41ah7f5z8chkhg6")))

(define-public crate-cosmwasm-schema-1.1.4 (c (n "cosmwasm-schema") (v "1.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mi9zmbhkhpwpyclr39zhld6wign8zkcn9dq0sj8g6p5p5bzxrlw")))

(define-public crate-cosmwasm-schema-1.1.5 (c (n "cosmwasm-schema") (v "1.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14j2ab48iy1fmsspmxb8zmsjz5hfjc79j72b6mmb4lkikbmwy9x2")))

(define-public crate-cosmwasm-schema-1.1.6 (c (n "cosmwasm-schema") (v "1.1.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "078yy337yxq0ad86nkyix80lsyiqrkg0dzxq7zaab72q57b2bh4q") (y #t)))

(define-public crate-cosmwasm-schema-1.1.7 (c (n "cosmwasm-schema") (v "1.1.7") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.7") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "12dl94pfyqg36swy4kmy695jil023iqpd545f8f4v9d760x6k7h6") (y #t)))

(define-public crate-cosmwasm-schema-1.1.8 (c (n "cosmwasm-schema") (v "1.1.8") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "0ccf61b1rfh3ykvrxzpf08x9kcwfkcqxw0hcmlzlnism81qfr26z")))

(define-public crate-cosmwasm-schema-1.1.9 (c (n "cosmwasm-schema") (v "1.1.9") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.9") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "06dnrn7hy6cr4rkzgbpdfazps1rwahrfi91wg7mngf63w9qmj4q4")))

(define-public crate-cosmwasm-schema-1.2.0-beta.0 (c (n "cosmwasm-schema") (v "1.2.0-beta.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.0-beta.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.0-beta.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "1b7j012n959dfqansyfkpxdlllybl9lyc6050gc7y5skiq4lghab")))

(define-public crate-cosmwasm-schema-1.2.0-beta.1 (c (n "cosmwasm-schema") (v "1.2.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.0-beta.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.0-beta.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0gw38nnspazasg484ylr19ij57hdsbb8xhkh2ybi0rxsrvcbzrmx")))

(define-public crate-cosmwasm-schema-1.2.0-rc.1 (c (n "cosmwasm-schema") (v "1.2.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.0-rc.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.0-rc.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1rp3y6cl82ny0waki3853ipn67irpg3x15h9bbdlwzd7d4wvfanh")))

(define-public crate-cosmwasm-schema-1.2.0 (c (n "cosmwasm-schema") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0qy87bz5jpz5x263hfjhlhl9h8qg86cskh10i3prljvg1kxgqg8s")))

(define-public crate-cosmwasm-schema-1.2.1 (c (n "cosmwasm-schema") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1yc3qj1w9brchgrwwl67dqlqy0w7ix1lcv321bylhrnz8dlf664i")))

(define-public crate-cosmwasm-schema-1.2.2 (c (n "cosmwasm-schema") (v "1.2.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.2") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0bndbrmb9ijyw5g8z6jarzfz4z731qvrzvrfmr02j5ng5ihrzfdi")))

(define-public crate-cosmwasm-schema-1.2.3 (c (n "cosmwasm-schema") (v "1.2.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.3") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1pg5pw13gjis8y714jzdkdwk7777wbiww83vsm69w6wp7bid4npy")))

(define-public crate-cosmwasm-schema-1.2.4 (c (n "cosmwasm-schema") (v "1.2.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.4") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "12vrn56dxbd438l8grw09r9zr1546rwd3i0dkmjr2nj7rv6r57iy")))

(define-public crate-cosmwasm-schema-1.1.10 (c (n "cosmwasm-schema") (v "1.1.10") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.1.10") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "1qj5m9fpfrkrzyskkwciwww2znw607dpsxlic60f8ja8zdcmxbp8")))

(define-public crate-cosmwasm-schema-1.0.1 (c (n "cosmwasm-schema") (v "1.0.1") (d (list (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "055b7bd5gnx3jy9b1mjysd9cng9fv9jgcs85qfmmcjfzz2jcxnl7")))

(define-public crate-cosmwasm-schema-1.2.5 (c (n "cosmwasm-schema") (v "1.2.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.5") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1z7rkq9gcqncvx7k6s7hg45r5hx7w44spfr47ylfw602c5iv600g")))

(define-public crate-cosmwasm-schema-1.2.6 (c (n "cosmwasm-schema") (v "1.2.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.6") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.6") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0hgxq8rvlrkv6qqa1nfxy1w26sybnwxz0rw1vdh8p83i2rpwlyj0")))

(define-public crate-cosmwasm-schema-1.2.7 (c (n "cosmwasm-schema") (v "1.2.7") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.7") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.7") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "05f8jb973yllarm0b6fq5imdnwf8p60kqxili7dk2lxfxwf5s3i3")))

(define-public crate-cosmwasm-schema-1.3.0-rc.0 (c (n "cosmwasm-schema") (v "1.3.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.3.0-rc.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.0-rc.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "10w59c4k3hl14pva6w0lk5sv4rm7rsgia4fj0822ia84gbfspm5h")))

(define-public crate-cosmwasm-schema-1.3.0 (c (n "cosmwasm-schema") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.3.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1carx7q36a4w5qc9yzh397d7fby2j6pf5wkpvk6mkvsqcyvjysxv")))

(define-public crate-cosmwasm-schema-1.3.1 (c (n "cosmwasm-schema") (v "1.3.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.3.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0bm7pb87r2716sfnvsiqgxvk4cv1yrzlm4ax5fsyb2d0jzh3ghv3")))

(define-public crate-cosmwasm-schema-1.3.2 (c (n "cosmwasm-schema") (v "1.3.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.3.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.2") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1snv4h89gbswpnzid2dnilz09k4dh646sfvppm2lnz8vjkavs9qh")))

(define-public crate-cosmwasm-schema-1.3.3 (c (n "cosmwasm-schema") (v "1.3.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.3.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.3") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "17cxiiq0bmrq9k946c4w56i7f21q01bhd2hdan4n7qqy82h2y8lr")))

(define-public crate-cosmwasm-schema-1.4.0-beta.1 (c (n "cosmwasm-schema") (v "1.4.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.4.0-beta.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0-beta.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1ci5n90nfffvws4h1n2aq1y5b7krrvawvrb82pkkfj1s1z52ml7g")))

(define-public crate-cosmwasm-schema-1.4.0-rc.1 (c (n "cosmwasm-schema") (v "1.4.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.4.0-rc.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0-rc.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1anfy26gl1p6k61pb2y29b9m6qvj1mzn3fwxxgvqrw9q36yyjap8")))

(define-public crate-cosmwasm-schema-1.4.0 (c (n "cosmwasm-schema") (v "1.4.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1mcjxkarz4gbiayj20py411i68mxyq202iycynd9hcq40844mqvc")))

(define-public crate-cosmwasm-schema-1.4.1 (c (n "cosmwasm-schema") (v "1.4.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.4.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0d6w2x2ki47w5ccypxw79hn9arncsb81jmsi65nvvsn4q6lq7xpy")))

(define-public crate-cosmwasm-schema-1.5.0-rc.0 (c (n "cosmwasm-schema") (v "1.5.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.5.0-rc.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0-rc.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0lwxzm541x5gp9fk765bpc35ylyy9c04dlmvn4sf4g7137f86drm")))

(define-public crate-cosmwasm-schema-1.5.0 (c (n "cosmwasm-schema") (v "1.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.5.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0miqcxplfpjyzi93cblfiipgdhlc0kn9wrbr6nsbcii9byjixx0d")))

(define-public crate-cosmwasm-schema-2.0.0-beta.0 (c (n "cosmwasm-schema") (v "2.0.0-beta.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=2.0.0-beta.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0-beta.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "00bc9j2af03jd7dj23wzkn7ca5g6f95ks515z29ymcd1waz7s4ij")))

(define-public crate-cosmwasm-schema-1.5.1 (c (n "cosmwasm-schema") (v "1.5.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.5.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1mql33xf639v5ishkk5zms1fcgvrf889r7gxjlj5gbdwdp7adqkp")))

(define-public crate-cosmwasm-schema-1.4.2 (c (n "cosmwasm-schema") (v "1.4.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.4.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.2") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "07w2m97q8sinwm0n7r42g70z6kdaz7yljxhq7qbn10zmakqiah6a")))

(define-public crate-cosmwasm-schema-1.3.4 (c (n "cosmwasm-schema") (v "1.3.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.3.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.4") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "07s3mh9biy627v30r48ki7gsan8pvilznsh4r6nzcy40f1nz6db7")))

(define-public crate-cosmwasm-schema-1.2.8 (c (n "cosmwasm-schema") (v "1.2.8") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.2.8") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.8") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "095z04bcq5bkd4w07akdc67m6yg7hw25s46i6nyjmfp0l6q4v1p0")))

(define-public crate-cosmwasm-schema-1.5.2 (c (n "cosmwasm-schema") (v "1.5.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.5.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.2") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1afj95wjacxh6fn3prgz3h943fz7gajjl1mfbdbwagg9pxgj2rmi")))

(define-public crate-cosmwasm-schema-1.4.3 (c (n "cosmwasm-schema") (v "1.4.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.4.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.3") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1bv84jdkv65zdlcnagd7hqvxggfdj4i3v31wzb2d0cdq3ybjxbhi")))

(define-public crate-cosmwasm-schema-2.0.0-beta.1 (c (n "cosmwasm-schema") (v "2.0.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=2.0.0-beta.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0-beta.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "043w4qwkq9l46w9xjc33vmpv7zzbl4w4vd9gkwl9cmr51sq25dic")))

(define-public crate-cosmwasm-schema-1.5.3 (c (n "cosmwasm-schema") (v "1.5.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.5.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.3") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "044hsv6dzh8b6dym6f5zfznq79nilpydzxc2cn5hx9p26qhklgmc")))

(define-public crate-cosmwasm-schema-2.0.0-rc.0 (c (n "cosmwasm-schema") (v "2.0.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=2.0.0-rc.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0-rc.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0p1ilw7rjhpcxgx3nmpxr76dcqjq2fg8y7vqy0v1hcmffg4gj36z")))

(define-public crate-cosmwasm-schema-2.0.0-rc.1 (c (n "cosmwasm-schema") (v "2.0.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=2.0.0-rc.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0-rc.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0f5x2dhb5h988ipdfyawm9drsmspscwiq26y4s06xp78pggc2xvq")))

(define-public crate-cosmwasm-schema-2.0.0 (c (n "cosmwasm-schema") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1ii0li5c94qqz5xgm7sx45mx14dswzl7y0y83yy4ncwypad0jz56")))

(define-public crate-cosmwasm-schema-2.0.1 (c (n "cosmwasm-schema") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=2.0.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1dbpfpzsxqmlsx2pn68zlc3j60v5n4x1j210i2flwas52c6m6vny")))

(define-public crate-cosmwasm-schema-2.0.2 (c (n "cosmwasm-schema") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=2.0.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.2") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0fskbndpmpx4g63m0giw2fpk8m3wkaspvmiqz7rg4rya01cdpnr6")))

(define-public crate-cosmwasm-schema-1.5.4 (c (n "cosmwasm-schema") (v "1.5.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.5.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.4") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1d0palx9132gy87krsq8daajmrq2gpag9xjzjlqw316k4x48frw4")))

(define-public crate-cosmwasm-schema-1.4.4 (c (n "cosmwasm-schema") (v "1.4.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.4.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.4") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "067wamx09bgk8g659spi7jf5b1dm0f2zz7w7fhaxlny3cq0pf981")))

(define-public crate-cosmwasm-schema-2.0.3 (c (n "cosmwasm-schema") (v "2.0.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=2.0.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.3") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0plym54yblcijdishm7k8vfr24wzza0a3p995pyj0nss2yhxw0yl")))

(define-public crate-cosmwasm-schema-1.5.5 (c (n "cosmwasm-schema") (v "1.5.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.5.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.5") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0agrlbr1a0jnl9i694635gncwpjszip33mz05z1d2ah9arhh6ybq")))

(define-public crate-cosmwasm-schema-1.4.5 (c (n "cosmwasm-schema") (v "1.4.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "cosmwasm-schema-derive") (r "=1.4.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.5") (d #t) (k 2)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "15j7cyg3w56qg5634jvr48w9i4sw8ncbxqqmrcc03m83qx0vrii6")))

