(define-module (crates-io co sm cosmwasm-ica) #:use-module (crates-io))

(define-public crate-cosmwasm-ica-0.1.0 (c (n "cosmwasm-ica") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.4") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "00c4hhgrylp4gcijkxs5zx3byp1rz46mdq9v7gbja8vg16gspkpf")))

