(define-module (crates-io co sm cosmian-wit-bindgen-gen-core) #:use-module (crates-io))

(define-public crate-cosmian-wit-bindgen-gen-core-0.1.0 (c (n "cosmian-wit-bindgen-gen-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cosmian-wit-parser") (r "^0.1.0") (d #t) (k 0)))) (h "1hhd068rnxhsxf75p7bflscxr0fhih48qnsqmvb14cna8zchjnna") (f (quote (("witx-compat" "cosmian-wit-parser/witx-compat"))))))

