(define-module (crates-io co sm cosmocalc) #:use-module (crates-io))

(define-public crate-cosmocalc-0.1.0 (c (n "cosmocalc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "0ga6flx5aplihabjz153rq9vdsc65b4piyn9a1hhaswihaxj2zms")))

(define-public crate-cosmocalc-0.2.0 (c (n "cosmocalc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "17zwsph9vrjdxpncs7rnhnyvr4a9kddmibxxxg46vk6i4wc1mpyb")))

