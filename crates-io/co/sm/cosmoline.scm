(define-module (crates-io co sm cosmoline) #:use-module (crates-io))

(define-public crate-cosmoline-0.1.1 (c (n "cosmoline") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "handlebars") (r "^4.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1534kjgl80fnblpgvqlf09icis2jixn2myjmw1hlw2s5hkyaqxdx")))

