(define-module (crates-io co sm cosmos-sdk-proto-nymlab) #:use-module (crates-io))

(define-public crate-cosmos-sdk-proto-nymlab-0.19.1 (c (n "cosmos-sdk-proto-nymlab") (v "0.19.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tendermint-proto") (r "^0.32") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (f (quote ("codegen" "prost"))) (o #t) (k 0)))) (h "1k3c0vny9mpf1kni9yycadpvv9xiy1apvzrzk54qzcw7ab0jf8i0") (f (quote (("grpc-transport" "grpc" "tonic/transport") ("grpc" "tonic") ("default" "grpc-transport") ("cosmwasm")))) (r "1.67")))

(define-public crate-cosmos-sdk-proto-nymlab-0.19.2 (c (n "cosmos-sdk-proto-nymlab") (v "0.19.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tendermint-proto") (r "^0.32") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (f (quote ("codegen" "prost"))) (o #t) (k 0)))) (h "16chbykh8811a4jsnbp7b137d4p1ldsjlh10dx26f5cik5jdrcm9") (f (quote (("grpc-transport" "grpc" "tonic/transport") ("grpc" "tonic") ("default" "grpc-transport") ("cosmwasm")))) (r "1.67")))

