(define-module (crates-io co sm cosmwasm-storage) #:use-module (crates-io))

(define-public crate-cosmwasm-storage-0.8.0-alpha1 (c (n "cosmwasm-storage") (v "0.8.0-alpha1") (d (list (d (n "cosmwasm-std") (r "^0.8.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0fvr5zw81f0ygbyzxy4zqdh5b3cwfm69lbvdlqf250pv0a3djz4n") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.8.0-alpha2 (c (n "cosmwasm-storage") (v "0.8.0-alpha2") (d (list (d (n "cosmwasm-std") (r "^0.8.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0rjmrb2x4ci2x9iskfgwxcswiqxn08qkfx3yh0ld4r621y7sx98d") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.8.0-alpha3 (c (n "cosmwasm-storage") (v "0.8.0-alpha3") (d (list (d (n "cosmwasm-std") (r "^0.8.0-alpha3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "1y595pnq580y0b702cw2zsl52nnhlxbhbhflbrkdmi4260xg4ici") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.8.0-rc1 (c (n "cosmwasm-storage") (v "0.8.0-rc1") (d (list (d (n "cosmwasm-std") (r "^0.8.0-rc1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0kiympily9xkz9hcv8h538kcbc4ajbkxria5rfm4lm4d996smi64") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.8.0 (c (n "cosmwasm-storage") (v "0.8.0") (d (list (d (n "cosmwasm-std") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "1q2gzvvca3x4qncwhn2x0rk39wm1dq2mj5xyycpd3vlkgj45xxz0") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.8.1 (c (n "cosmwasm-storage") (v "0.8.1") (d (list (d (n "cosmwasm-std") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0grsv778jhz8g3xjj2cd0qj5daq09q2f51xcg6mm45r25f7iglhs") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.9.0-alpha2 (c (n "cosmwasm-storage") (v "0.9.0-alpha2") (d (list (d (n "cosmwasm-std") (r "^0.9.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "1f7lvabjr3fxl7d7rc83dgw1lgvgjh181lb0a44b73lj8dw639ah") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.9.0-alpha3 (c (n "cosmwasm-storage") (v "0.9.0-alpha3") (d (list (d (n "cosmwasm-std") (r "^0.9.0-alpha3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "1cygbxmwg6lymmjigjrnyymyklpq667gdzqi8a9kb05q0l5dzzqw") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.9.0 (c (n "cosmwasm-storage") (v "0.9.0") (d (list (d (n "cosmwasm-std") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "103dznfhpl94sw6zz71b58jgfkqbwqf2k3h5nlwf011mb4qjb2dr") (f (quote (("iterator" "cosmwasm-std/iterator")))) (y #t)))

(define-public crate-cosmwasm-storage-0.9.1 (c (n "cosmwasm-storage") (v "0.9.1") (d (list (d (n "cosmwasm-std") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "1jxvjgah0ggn00cz7vwdy8hrgdyw6dvm08ci766admawaq59vdds") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.9.2 (c (n "cosmwasm-storage") (v "0.9.2") (d (list (d (n "cosmwasm-std") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "12v5yvqc6rqb11ya917pk7df8b0rxwkxj7iqvgv9gbv6d04w32yc") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.9.3 (c (n "cosmwasm-storage") (v "0.9.3") (d (list (d (n "cosmwasm-std") (r "^0.9.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0hnkpgbhx5hxswcax4myrkj2xccly003wpzbs0ngha8m1h6r5sgm") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.9.4 (c (n "cosmwasm-storage") (v "0.9.4") (d (list (d (n "cosmwasm-std") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "1as3drkg9d26n1kr51yc6if50pdg9bdrf9632nl42q3g2b5bddnd") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.10.0-alpha1 (c (n "cosmwasm-storage") (v "0.10.0-alpha1") (d (list (d (n "cosmwasm-std") (r "^0.10.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0fmi03qmd046jl2jm0adhzhbxihgybs07a7zr4a5jx2nx0zq1vqd") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.10.0-alpha2 (c (n "cosmwasm-storage") (v "0.10.0-alpha2") (d (list (d (n "cosmwasm-std") (r "^0.10.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0ac4iaw2lmcich09r7zffzzfcsv9p14qjyc0jg0551gd1hjigdaf") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.10.0-alpha3 (c (n "cosmwasm-storage") (v "0.10.0-alpha3") (d (list (d (n "cosmwasm-std") (r "^0.10.0-alpha3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "10x2j36n2gbd8z1m2fw85cnnmgli9bh9b3p114d0i2nkvn4adm46") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.10.0 (c (n "cosmwasm-storage") (v "0.10.0") (d (list (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "14qb6yqp723135diivhpqzsbwpdvc4hn0gsr5mxy8dlr60kzkn14") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.10.1 (c (n "cosmwasm-storage") (v "0.10.1") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0y8kmj5g003fk9zhx3mb6843ckcc6ifw5kirfrmyhdp65hd560z1") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.11.0-alpha1 (c (n "cosmwasm-storage") (v "0.11.0-alpha1") (d (list (d (n "cosmwasm-std") (r "^0.11.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "1xvrz81b49r6si9zh8jqs4gs3ap2gmivfnv7vf85qcmlimvfxli8") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.11.0-alpha2 (c (n "cosmwasm-storage") (v "0.11.0-alpha2") (d (list (d (n "cosmwasm-std") (r "^0.11.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0va2a4hqfv563mc4mx56d582d6531m515mcs6adhk3ys8yg8sfda") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.11.0-alpha3 (c (n "cosmwasm-storage") (v "0.11.0-alpha3") (d (list (d (n "cosmwasm-std") (r "^0.11.0-alpha3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0zva3qn2r0pdq84lmv528b5zdwwcv0a4v3ay0iplmnpcmp3x123f") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.11.0-alpha4 (c (n "cosmwasm-storage") (v "0.11.0-alpha4") (d (list (d (n "cosmwasm-std") (r "^0.11.0-alpha4") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "1vlipjbf9bfg2l0hvcl72vyqygs3izpjpyp2n6miiqflqpf3gah7") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.11.0 (c (n "cosmwasm-storage") (v "0.11.0") (d (list (d (n "cosmwasm-std") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "04n81mwnm884qiz4m4xkzk9403gvbdvirw0p8m1npwqxb41az9x5") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.11.1 (c (n "cosmwasm-storage") (v "0.11.1") (d (list (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "0blp39xq8dbycazd26gqgl6p5v3f1qabn7xa0h9l65bn80n0hq19") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.11.2 (c (n "cosmwasm-storage") (v "0.11.2") (d (list (d (n "cosmwasm-std") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 2)))) (h "06b442dvp1m2l3hv5zcxilcx9vcz296icv7v93abilahsnzh3b13") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.12.0-alpha1 (c (n "cosmwasm-storage") (v "0.12.0-alpha1") (d (list (d (n "cosmwasm-std") (r "^0.12.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1x9cg0i0zi31shijdmi3l66aa2bxi84vxgj60al2c64jdkllhds4") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.12.0-alpha2 (c (n "cosmwasm-storage") (v "0.12.0-alpha2") (d (list (d (n "cosmwasm-std") (r "^0.12.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0m67mgi9jy2lpq74f3blza1hh7z0z1k6d2bq3x6px2iqsfw8a6x8") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.12.0-alpha3 (c (n "cosmwasm-storage") (v "0.12.0-alpha3") (d (list (d (n "cosmwasm-std") (r "^0.12.0-alpha3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1q6ywskx581vrx7i9fixpcxxnvvwhbdspp0lp567098x8drx4wb6") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.12.0 (c (n "cosmwasm-storage") (v "0.12.0") (d (list (d (n "cosmwasm-std") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "157w4jpf9b7l6mzvcf2zj5qzgpxsciifn3ljkcsbd2ixl6cx2mdk") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.12.1 (c (n "cosmwasm-storage") (v "0.12.1") (d (list (d (n "cosmwasm-std") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1hg3sy531wsgxny68k0qxzgm3mzcrgm3wz304drkc21zr2nbhk51") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.12.2 (c (n "cosmwasm-storage") (v "0.12.2") (d (list (d (n "cosmwasm-std") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1qhxh9470d9ygw13j9xn8yks3ysa56cl8l39ckcz5r292vjv13yi") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.13.0-alpha1 (c (n "cosmwasm-storage") (v "0.13.0-alpha1") (d (list (d (n "cosmwasm-std") (r "^0.13.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0pz6ysmz1gd79598rssyw4p03rr8mqglhk1bk4k5sc58zzj6f1p8") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.13.0-alpha2 (c (n "cosmwasm-storage") (v "0.13.0-alpha2") (d (list (d (n "cosmwasm-std") (r "^0.13.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1srbbndgkvarbhjjrs4dfl7f64p0b1jjvvgnflls26bssmwhi99v") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.13.0 (c (n "cosmwasm-storage") (v "0.13.0") (d (list (d (n "cosmwasm-std") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1qpfmi8p8gafqygm4v826qjas0aaa3xc0pz115pa4hn54pippj5l") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.13.1 (c (n "cosmwasm-storage") (v "0.13.1") (d (list (d (n "cosmwasm-std") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "083r52qb4sf0mvvyjxsk5v36c90fjjzirnpcz28xjcghc4mmhlbh") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.13.2 (c (n "cosmwasm-storage") (v "0.13.2") (d (list (d (n "cosmwasm-std") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "01044i8zy8hbfgn833q8pv3hprkqc7v8l0dh08f9470qqgvbbkg2") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.14.0-alpha1 (c (n "cosmwasm-storage") (v "0.14.0-alpha1") (d (list (d (n "cosmwasm-std") (r "^0.14.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1s3cxhqg5vdvp63ilmdh6x13gi817c6n7nlyziadmvm13gibcaa2") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.14.0-alpha2 (c (n "cosmwasm-storage") (v "0.14.0-alpha2") (d (list (d (n "cosmwasm-std") (r "^0.14.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0mpary8w319aiygi62b679dr4y3djzx1ys8xgb76am75lyga4h8z") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.14.0-beta1 (c (n "cosmwasm-storage") (v "0.14.0-beta1") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0ygxbfip5d8rkxmllvz0cbf4fzyzzx5xa2wfbpb8b183q8v8sb7w") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.14.0-beta2 (c (n "cosmwasm-storage") (v "0.14.0-beta2") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0qa3867k7yh4m5iknwfknsyp3jyq5pz6qz8dvsvpgk93gvm81qj3") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.14.0-beta3 (c (n "cosmwasm-storage") (v "0.14.0-beta3") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1k2pnarw7awzcha5waz4y2svyx77jfqmsigy8janhjil97086dcp") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.14.0-beta4 (c (n "cosmwasm-storage") (v "0.14.0-beta4") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta4") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "07wddsff6k8avlvll3r3q38r0j77j6ilq1g8g8x8nqrhfd5q60cz") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.14.0-beta5 (c (n "cosmwasm-storage") (v "0.14.0-beta5") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta5") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1yrcyyr65jfkwf996p0nxkhw6fiwqh2iskklm6jdwl4jnbbihxyn") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.14.0-rc1 (c (n "cosmwasm-storage") (v "0.14.0-rc1") (d (list (d (n "cosmwasm-std") (r "^0.14.0-rc1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1vzp41gj1r5iwhnnvvaky7gv2nvzjnaxgdnz474i7rhry1a79qp3") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.14.0 (c (n "cosmwasm-storage") (v "0.14.0") (d (list (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1f64yw6c9slnqz4bgb7vr2njmqb7pikns2af4b75nyzg4k1kvc0k") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.14.1 (c (n "cosmwasm-storage") (v "0.14.1") (d (list (d (n "cosmwasm-std") (r "^0.14.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "03hfmk1cdp0mw6rxi1xl4y8x20l7vxkxz3mijlsi47k5q80g5azl") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.15.0-alpha1 (c (n "cosmwasm-storage") (v "0.15.0-alpha1") (d (list (d (n "cosmwasm-std") (r "^0.15.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1wfnkqwfd5irg9hn4bbkfndw0vp68fvp8fiqbrlrk2khaiz84mng") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.15.0-alpha2 (c (n "cosmwasm-storage") (v "0.15.0-alpha2") (d (list (d (n "cosmwasm-std") (r "^0.15.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "19xcfzkmkmk74ykkzr94kww7648wvhz9f2sgsaj8rb9yzjbm5axk") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.15.0-alpha3 (c (n "cosmwasm-storage") (v "0.15.0-alpha3") (d (list (d (n "cosmwasm-std") (r "^0.15.0-alpha3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "03qck7ljf9fhd7901w4qbiqxp25zf8kilnjicfji1byqn5knzf89") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.15.0 (c (n "cosmwasm-storage") (v "0.15.0") (d (list (d (n "cosmwasm-std") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0xjxx35zzn2kyk1cqy0miqznwpwah5malgp9argxkdil8kyg95zj") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.16.0-rc1 (c (n "cosmwasm-storage") (v "0.16.0-rc1") (d (list (d (n "cosmwasm-std") (r "^0.16.0-rc1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1x4lvbv1x838846a34jc7dgm69pdq89myb3vgq82y0jjjsz79y3g") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.0-rc2 (c (n "cosmwasm-storage") (v "0.16.0-rc2") (d (list (d (n "cosmwasm-std") (r "^0.16.0-rc2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "00vfl9zk17z9jkdf18zbvm1mj1skfx6vd235bbh9487vk5kz9y4v") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.15.1 (c (n "cosmwasm-storage") (v "0.15.1") (d (list (d (n "cosmwasm-std") (r "^0.15.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1r2l1wv6a3wrappi2ydy7a9l7wg1106069iaay49305fxrss1188") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.15.2 (c (n "cosmwasm-storage") (v "0.15.2") (d (list (d (n "cosmwasm-std") (r "^0.15.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0vyvhq4lndsd3794p7x1j18309q38ki33d98wcsr8xilzh7b8mm2") (f (quote (("iterator" "cosmwasm-std/iterator"))))))

(define-public crate-cosmwasm-storage-0.16.0-rc3 (c (n "cosmwasm-storage") (v "0.16.0-rc3") (d (list (d (n "cosmwasm-std") (r "^0.16.0-rc3") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1bcriiqvbgrrvlx245bk5la4b7alj0b0nshk9vxflc8cxw9c3zvj") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.0-rc4 (c (n "cosmwasm-storage") (v "0.16.0-rc4") (d (list (d (n "cosmwasm-std") (r "^0.16.0-rc4") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0sjd5jpqdw5frzjpln6mjfp8m528181iv1v4g8q1ig0fadqql1an") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.0-rc5 (c (n "cosmwasm-storage") (v "0.16.0-rc5") (d (list (d (n "cosmwasm-std") (r "^0.16.0-rc5") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0ia5gzinb0371hnish79mz3h9lj49508ng4v0x08z8l23zbmi5lw") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.0-rc6 (c (n "cosmwasm-storage") (v "0.16.0-rc6") (d (list (d (n "cosmwasm-std") (r "^0.16.0-rc6") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0af0nb4xhg8gq9l36ikyal74yyg5dxwd7hhjzqffqlf672336ddz") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.0 (c (n "cosmwasm-storage") (v "0.16.0") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1k207vcamd6ql7kpbwcxbddhrmriqs04zqrvramha6wp9zqm6q72") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.1 (c (n "cosmwasm-storage") (v "0.16.1") (d (list (d (n "cosmwasm-std") (r "^0.16.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1ipiclj8kqa6f7f5g6nrvzsnipjgpjz1ympm9hl56r1iiq2hxrs6") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.2 (c (n "cosmwasm-storage") (v "0.16.2") (d (list (d (n "cosmwasm-std") (r "^0.16.2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "19x650h55q8h2qag855dj4a4ppnygba4lrw99mgmq5g7w3c74d1f") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.0-soon (c (n "cosmwasm-storage") (v "1.0.0-soon") (d (list (d (n "cosmwasm-std") (r "^1.0.0-soon") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "03zlsif7in5rqjf2vjnhwib46jr6fx8wx8njhr3lpc986fs0ijhv") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator")))) (y #t)))

(define-public crate-cosmwasm-storage-1.0.0-soon2 (c (n "cosmwasm-storage") (v "1.0.0-soon2") (d (list (d (n "cosmwasm-std") (r "^1.0.0-soon2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1dlkhxz4y6jw0i0y3b3c5kvk09477g28aa1zyhyvq01ng7aq5wqd") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator")))) (y #t)))

(define-public crate-cosmwasm-storage-1.0.0-beta (c (n "cosmwasm-storage") (v "1.0.0-beta") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "109v1kpllh53hphrhv0a8hfg56sah88rfwz6yqv6kr1bsixgjp36") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.0-beta2 (c (n "cosmwasm-storage") (v "1.0.0-beta2") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0zxg31j3964vwnqarz2dnscylgn256d2x810hmkdz1jg7gz4wfyg") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.0-beta3 (c (n "cosmwasm-storage") (v "1.0.0-beta3") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta3") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0jgpa8pj593pppd4g4jq8kh8vbv6sjw028r0s96dbzk41mgyb95l") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.3 (c (n "cosmwasm-storage") (v "0.16.3") (d (list (d (n "cosmwasm-std") (r "^0.16.3") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1raw1qzrv3pxwg2mwkrp2k3xfrhl56m79q1mx5jcyjqhnx6cck4b") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.0-beta4 (c (n "cosmwasm-storage") (v "1.0.0-beta4") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta4") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0plwvapnkza9yw96ln4rc3kra41kqmjhnzak083m3944snzw0wpl") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.0-beta5 (c (n "cosmwasm-storage") (v "1.0.0-beta5") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta5") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "137nmplhb1kypd9js3mpjrkl4jsz9v73kwcl8pxfyd4ris6k7r77") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.4 (c (n "cosmwasm-storage") (v "0.16.4") (d (list (d (n "cosmwasm-std") (r "^0.16.4") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "00i5pc6m2aw47azgzlpqk7bcikdg0lrfiiadyzkjycx04v2lksbb") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.5 (c (n "cosmwasm-storage") (v "0.16.5") (d (list (d (n "cosmwasm-std") (r "^0.16.5") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1slr80zg7j3fnmivfrnb9hp69h5dx2d2r0v1n4lcp5bvwfdhbk8v") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.0-beta6 (c (n "cosmwasm-storage") (v "1.0.0-beta6") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta6") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0vnb9m3jy9gji1g224f2fjrkjsy83mj31y7q64c9x834iz1b4i99") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.6 (c (n "cosmwasm-storage") (v "0.16.6") (d (list (d (n "cosmwasm-std") (r "^0.16.6") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0m8ismzm9inbd0cf63b5v5brqz9k4l8l50669fad513bll0727nv") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.0-beta7 (c (n "cosmwasm-storage") (v "1.0.0-beta7") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta7") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "18j16kj1niazb5rlaxmwzp9q6qy07zdd2qlf93rahjl2kh4mdy07") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.0-beta8 (c (n "cosmwasm-storage") (v "1.0.0-beta8") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta8") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "16vjsfibfy0dii9zg8fbw1y001q7qb5mx4iaqg8ffflxikyyjjqw") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-0.16.7 (c (n "cosmwasm-storage") (v "0.16.7") (d (list (d (n "cosmwasm-std") (r "^0.16.7") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1v9cm657ngk022njbr5wvris4jv6kj568qlpsrgbp6n4lrmx4hy2") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.0-rc.0 (c (n "cosmwasm-storage") (v "1.0.0-rc.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-rc.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1lb6m2izgi36zbwldw967il8j826blq4iwhjmzhi77jy9vm6hjrx") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.0 (c (n "cosmwasm-storage") (v "1.0.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1d99j9pw3196bvxy6jrvc26zgjmv8l6h84dd9lq5vl84ffq0716i") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.0-rc.1 (c (n "cosmwasm-storage") (v "1.1.0-rc.1") (d (list (d (n "cosmwasm-std") (r "^1.1.0-rc.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "005s2z4m8rfb0919pxy0x7qrdvkczx7h4cgla3ia5bpymw1wvwww") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.0-rc.2 (c (n "cosmwasm-storage") (v "1.1.0-rc.2") (d (list (d (n "cosmwasm-std") (r "^1.1.0-rc.2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1zdb8pj4g5p25mqf3k1rx3rhnwwfxyjvy8apgjlsb94kfxs5aj5f") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.0 (c (n "cosmwasm-storage") (v "1.1.0") (d (list (d (n "cosmwasm-std") (r "^1.1.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "08ivpr71k8j1jpcryiy2cf1c03fh6xy61f99fg6bgix41chxa19j") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.1 (c (n "cosmwasm-storage") (v "1.1.1") (d (list (d (n "cosmwasm-std") (r "^1.1.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "02jayk7z0pc45hq9wv78mzx5qyqrp2mb7wrivlq3y1bcsh6qh97h") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.2 (c (n "cosmwasm-storage") (v "1.1.2") (d (list (d (n "cosmwasm-std") (r "^1.1.2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0hgnmqnimvv7z809hn4x7bljm0xqrfbkw7w5fdvfn2cbk8ibijfd") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.3 (c (n "cosmwasm-storage") (v "1.1.3") (d (list (d (n "cosmwasm-std") (r "^1.1.3") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1zy9ngyf5l5b7zy0vx1mvvp2ipjb58h88wdzpci4jixwpz2bnl97") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.4 (c (n "cosmwasm-storage") (v "1.1.4") (d (list (d (n "cosmwasm-std") (r "^1.1.4") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0x2k6if9yskz201b1pwr5731mm1xd102nnqgbc8zf3f9qv73bqbc") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.5 (c (n "cosmwasm-storage") (v "1.1.5") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0ljqwb38x29wzfgy5d53fjb94m83z0m7jk9f7qkg0nnihzpwy7xn") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.6 (c (n "cosmwasm-storage") (v "1.1.6") (d (list (d (n "cosmwasm-std") (r "^1.1.6") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1cxnriibjhjsvx0yrwf8zbny8pawnicz1sg7qiz9bx3m5p6gzr5i") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.7 (c (n "cosmwasm-storage") (v "1.1.7") (d (list (d (n "cosmwasm-std") (r "^1.1.7") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "03zdk6fnpplbbm08y96y5kkmpf60bqaw3q297wwq27nsf4hcary0") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.8 (c (n "cosmwasm-storage") (v "1.1.8") (d (list (d (n "cosmwasm-std") (r "^1.1.8") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "18rwxkn059h8d10qhjh1yj88h7db3rh83ggqf81hgs84h97fii5n") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.9 (c (n "cosmwasm-storage") (v "1.1.9") (d (list (d (n "cosmwasm-std") (r "^1.1.9") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0xlnrw0zn21nq16wzsydk9g8xc0ipyyhwrgfw9dlv48jakwc6qli") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.0-beta.0 (c (n "cosmwasm-storage") (v "1.2.0-beta.0") (d (list (d (n "cosmwasm-std") (r "^1.2.0-beta.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1l19s9imysw3q6iyz1k3gnfwwklfvly0w673wwjymq3jr3hdn1y4") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.0-beta.1 (c (n "cosmwasm-storage") (v "1.2.0-beta.1") (d (list (d (n "cosmwasm-std") (r "^1.2.0-beta.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1mbxa58wid207s44y4nvf18p95id2bxvz1qay05irwqmh10p8fak") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.0-rc.1 (c (n "cosmwasm-storage") (v "1.2.0-rc.1") (d (list (d (n "cosmwasm-std") (r "^1.2.0-rc.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1pdad584w074k0nnqhhlc22gnrk9yriwma4lay3lssmq7s6majiv") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.0 (c (n "cosmwasm-storage") (v "1.2.0") (d (list (d (n "cosmwasm-std") (r "^1.2.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1md7cjkqfajkwqa0vvai1harca90mgdcig0bys4s6vvvay09h6y9") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.1 (c (n "cosmwasm-storage") (v "1.2.1") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1z3xk4rhmjh73hr6f8zwm38dg2yqlcghz58n2ppm9rbrcjfckd0q") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.2 (c (n "cosmwasm-storage") (v "1.2.2") (d (list (d (n "cosmwasm-std") (r "^1.2.2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "106z0rkk87ym90hidl5jmc95gkm6babqqsgy5cbhz27gjnc9nway") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.3 (c (n "cosmwasm-storage") (v "1.2.3") (d (list (d (n "cosmwasm-std") (r "^1.2.3") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "155xzc1910l9sdcsvi21m5aipfghp3p6q5i36dgc86mw11jc76v3") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.4 (c (n "cosmwasm-storage") (v "1.2.4") (d (list (d (n "cosmwasm-std") (r "^1.2.4") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "01z8d76r52qdw06zghh1vxxqikks15d94j1jyycakhq4a95r9xw8") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.1.10 (c (n "cosmwasm-storage") (v "1.1.10") (d (list (d (n "cosmwasm-std") (r "^1.1.10") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0gab72h0r67in00xz1wkhgcbmkvdi8nzygx5aybcsbm44cbxqwkc") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.0.1 (c (n "cosmwasm-storage") (v "1.0.1") (d (list (d (n "cosmwasm-std") (r "^1.0.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1z8s5x6myvl9m3fw1iqad4wd6rvf9x9qd2rpx8573g27smh9g27p") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.5 (c (n "cosmwasm-storage") (v "1.2.5") (d (list (d (n "cosmwasm-std") (r "^1.2.5") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0k8nf889ghjfhhck9d66iyyywzz766vvywsv7f4fvxa8mhx7lwx3") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.6 (c (n "cosmwasm-storage") (v "1.2.6") (d (list (d (n "cosmwasm-std") (r "^1.2.6") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0vms9p9jz5fm0y9v0dwk634k4mrwqkhqaxjd30hgv1l9yp2237mg") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.7 (c (n "cosmwasm-storage") (v "1.2.7") (d (list (d (n "cosmwasm-std") (r "^1.2.7") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "12ab5jiqqr5rgdmnc57g7jknqqy52rjl3cwrzqwnwxxq9ll1sq78") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.3.0-rc.0 (c (n "cosmwasm-storage") (v "1.3.0-rc.0") (d (list (d (n "cosmwasm-std") (r "^1.3.0-rc.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0vwmja19wn25bdwmgj47hamddqy461j761hdhwl2dq7lyvxc7fxa") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.3.0 (c (n "cosmwasm-storage") (v "1.3.0") (d (list (d (n "cosmwasm-std") (r "^1.3.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "07i16qgzbqdz9aqcc4qhrgc1pldfzxa0i70irxms026wg6p8rpks") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.3.1 (c (n "cosmwasm-storage") (v "1.3.1") (d (list (d (n "cosmwasm-std") (r "^1.3.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "189a5palzpqlnk9zs9vphmkqg7b8lcm9jb9xpwcmx4dsf3fss2l0") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.3.2 (c (n "cosmwasm-storage") (v "1.3.2") (d (list (d (n "cosmwasm-std") (r "^1.3.2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1dw6i07x30j9hny1shv1rc4ml36pdvslmqvdjpzx1mmq9j7lx1c1") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.3.3 (c (n "cosmwasm-storage") (v "1.3.3") (d (list (d (n "cosmwasm-std") (r "^1.3.3") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0j470k72xv1ndgwb1gmncprdj10dia840pmpmkrh5xbqdgay0axm") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.4.0-beta.1 (c (n "cosmwasm-storage") (v "1.4.0-beta.1") (d (list (d (n "cosmwasm-std") (r "^1.4.0-beta.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1z7ivc1ajrk4jna11n9c5adwc1qx3syv9h7ah3axa6flbzy0mbrn") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.4.0-rc.1 (c (n "cosmwasm-storage") (v "1.4.0-rc.1") (d (list (d (n "cosmwasm-std") (r "^1.4.0-rc.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "018c99zlkjm5djsg4j5gqsz818fm0hjvcfjhz6igxx4m9px2dv76") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.4.0 (c (n "cosmwasm-storage") (v "1.4.0") (d (list (d (n "cosmwasm-std") (r "^1.4.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0678syy12bh95fcvnm486gxdvj2wqygdj8nm6ccrg7kwmpy4sm5b") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.4.1 (c (n "cosmwasm-storage") (v "1.4.1") (d (list (d (n "cosmwasm-std") (r "^1.4.1") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "002giab4bjfmnhpis8h137i5q6i3kj1ixri1h55zxvqgsdscb8al") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.5.0-rc.0 (c (n "cosmwasm-storage") (v "1.5.0-rc.0") (d (list (d (n "cosmwasm-std") (r "^1.5.0-rc.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1b69jbr32jncjqrmaaapzdccl93c6rms4rcdlm1z06fz30bzz4g4") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.5.0 (c (n "cosmwasm-storage") (v "1.5.0") (d (list (d (n "cosmwasm-std") (r "^1.5.0") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "191jh1c0rdiz3s0vrqn6rv4psb8xsmrd2nfzhmngbs035bkllaxx") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.4.2 (c (n "cosmwasm-storage") (v "1.4.2") (d (list (d (n "cosmwasm-std") (r "^1.4.2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "0brwmcarrj9lj2b8b8hyd35apmq2c7fdg8pw5w9vn3z0hwhyj70q") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.3.4 (c (n "cosmwasm-storage") (v "1.3.4") (d (list (d (n "cosmwasm-std") (r "^1.3.4") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1wsgi40aaw5fhcyb7vi65gic1s26hirc1c2xxafz1bh6c1vccgzp") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.2.8 (c (n "cosmwasm-storage") (v "1.2.8") (d (list (d (n "cosmwasm-std") (r "^1.2.8") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1zpj0dzmmli7672gdqqbvry8ph1ri2f7jigpm5y4rhjppdxrk8nh") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.5.2 (c (n "cosmwasm-storage") (v "1.5.2") (d (list (d (n "cosmwasm-std") (r "^1.5.2") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "1xilxybi39x42xkpdad8ljnh6sakpr7ridgjvv77nx84vfwjmpk6") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

(define-public crate-cosmwasm-storage-1.4.3 (c (n "cosmwasm-storage") (v "1.4.3") (d (list (d (n "cosmwasm-std") (r "^1.4.3") (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive" "alloc"))) (k 0)))) (h "063a2baxzyh0cylwnvqwca93dla9ifrphqq39j6qv2v82abhfdc9") (f (quote (("iterator" "cosmwasm-std/iterator") ("default" "iterator"))))))

