(define-module (crates-io co sm cosmwasm-rbac) #:use-module (crates-io))

(define-public crate-cosmwasm-rbac-0.1.1 (c (n "cosmwasm-rbac") (v "0.1.1") (d (list (d (n "cosmwasm-std") (r "^0.16.2") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gv2lbmmi7igd2i43jvmj7n2vifvcyrcbj6lb3kq2k70m7hhbdvp")))

(define-public crate-cosmwasm-rbac-0.1.2 (c (n "cosmwasm-rbac") (v "0.1.2") (d (list (d (n "cosmwasm-std") (r "^0.16.2") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13lx78qxljx4j1dhbljlg323vjjkim6pq6dvlji8igbshqxrid9s")))

(define-public crate-cosmwasm-rbac-0.1.3 (c (n "cosmwasm-rbac") (v "0.1.3") (d (list (d (n "cosmwasm-std") (r "^0.16.2") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0saa2kia1i0sdafiiqv1a9n5rd6n05m11fxsgvyk7q6340kyi19i")))

(define-public crate-cosmwasm-rbac-0.1.4 (c (n "cosmwasm-rbac") (v "0.1.4") (d (list (d (n "cosmwasm-std") (r "^0.16.2") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04qf175xx4z56b2pj0l941sh9ljsd5jv08gbr6lib7mkzr0qb9gm")))

(define-public crate-cosmwasm-rbac-0.1.5 (c (n "cosmwasm-rbac") (v "0.1.5") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1di3n5ky89r4gfkilws1pygzdqli94l254cbnxa9ziqckpkl6yrv")))

