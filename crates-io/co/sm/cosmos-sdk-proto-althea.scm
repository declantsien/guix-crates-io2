(define-module (crates-io co sm cosmos-sdk-proto-althea) #:use-module (crates-io))

(define-public crate-cosmos-sdk-proto-althea-0.9.0 (c (n "cosmos-sdk-proto-althea") (v "0.9.0") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0ahpfqwxpvr00828qcccsyqsm5qpspdbmvfj5ifyg2bb9wpv38v4") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.9.1 (c (n "cosmos-sdk-proto-althea") (v "0.9.1") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1m7k2px2vgj9rs0hvzmqagcjrd4hrfrmi66flrzcngzyfazc7i89") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.9.2 (c (n "cosmos-sdk-proto-althea") (v "0.9.2") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0nd7f679sij4wcr5fn3pcwkjj84l0j8bnvv7c5gp058f3b4mcvk5") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.10.0 (c (n "cosmos-sdk-proto-althea") (v "0.10.0") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1n0k61hhhsr2hvdzrmnzqw7d61vwwm2my7hilckimlzl8a4v729p") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.11.0 (c (n "cosmos-sdk-proto-althea") (v "0.11.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1bsvqi8c9xp4bydgibklirljcpxi0c4wicxg04k0kih1c3jlvldw") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.12.0 (c (n "cosmos-sdk-proto-althea") (v "0.12.0") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1ib47hxm03i8pa3bqbm2dn41qvqc07s4fdhizklzm2dh4yqlzj88") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.12.1 (c (n "cosmos-sdk-proto-althea") (v "0.12.1") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1vcmq6bjfysgshxifnm8d89ymivplkymd7qbvbw6y23xyg6q770q") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.12.2 (c (n "cosmos-sdk-proto-althea") (v "0.12.2") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0fbfcimm42j9fh2pzqcammn0swffi2abjrw2qvyin4k18jdv85rx") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.12.3 (c (n "cosmos-sdk-proto-althea") (v "0.12.3") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0ljik7i13mdkr5bkk9slzaxw76k5yv734cnifbcaslm6d6xid56b") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.12.4 (c (n "cosmos-sdk-proto-althea") (v "0.12.4") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0q8d6lszz0snsxspkxzchjhxv5rb5by2jnz2syj9557l84qr193j") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm") ("bech32ibc")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.13.0 (c (n "cosmos-sdk-proto-althea") (v "0.13.0") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (o #t) (d #t) (k 0)))) (h "13id8wjfs0qcp8sz3w57255iwnrnyr0a4g61fmml84cziw5rnw92") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.13.1 (c (n "cosmos-sdk-proto-althea") (v "0.13.1") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1xlnkfp4kddbxgh33d3l5876q67azyfms22m8fbidi3xdvfcb3kc") (f (quote (("grpc" "tonic") ("default" "grpc") ("cosmwasm")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.13.2 (c (n "cosmos-sdk-proto-althea") (v "0.13.2") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0wnrnblfz9w3ywnyvsih7fgbrdrzww0sl5jh5xvmdfc6v34a1bkb") (f (quote (("grpc" "tonic") ("ethermint") ("default" "grpc") ("cosmwasm") ("bech32ibc")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.13.3 (c (n "cosmos-sdk-proto-althea") (v "0.13.3") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (o #t) (d #t) (k 0)))) (h "08342ipp10fhsqiw85x6gxy6b4ph4h96xhgr88c65v0y2c4c92kb") (f (quote (("grpc" "tonic") ("ethermint") ("default" "grpc") ("cosmwasm") ("bech32ibc")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.14.0 (c (n "cosmos-sdk-proto-althea") (v "0.14.0") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0n5bmrz4d1zalqwjg3j3l1nbgcz1pa2qaq21afhjkyk7bnjbgvlq") (f (quote (("grpc" "tonic") ("ethermint") ("default" "grpc") ("cosmwasm") ("bech32ibc")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.14.1 (c (n "cosmos-sdk-proto-althea") (v "0.14.1") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0frw4yzk6085y7pg0nz9s9irb0c84vkvacs3mlwzbc5yqq8lwi7w") (f (quote (("grpc" "tonic") ("ethermint") ("default" "grpc") ("cosmwasm") ("bech32ibc")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.14.2 (c (n "cosmos-sdk-proto-althea") (v "0.14.2") (d (list (d (n "prost") (r "^0.10") (d #t) (k 0)) (d (n "prost-types") (r "^0.10") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0v0hnsffipxa5j9n8k25vsx0jl32lhs8wsfjd2ciwgpxrkxxhik9") (f (quote (("grpc" "tonic") ("ethermint") ("default" "grpc") ("cosmwasm") ("bech32ibc")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.15.0 (c (n "cosmos-sdk-proto-althea") (v "0.15.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1vgm42ghdnzafcisjziikk38z0i1f1iyr4r1xmz5p091bzhkrcf3") (f (quote (("grpc" "tonic") ("ethermint") ("default" "grpc") ("cosmwasm") ("bech32ibc")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.16.0 (c (n "cosmos-sdk-proto-althea") (v "0.16.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0gbqcan5iaqsww6gj5sc6kyzfj3nngdfnv0jmim68879xw2jdas6") (f (quote (("grpc" "tonic") ("ethermint") ("default" "grpc") ("cosmwasm") ("bech32ibc")))) (r "1.56")))

(define-public crate-cosmos-sdk-proto-althea-0.16.1 (c (n "cosmos-sdk-proto-althea") (v "0.16.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0zy280d885gcssgq2pqaxwa8l6djs277m9x2fsvfrxgdyjar0x02") (f (quote (("grpc" "tonic") ("ethermint") ("default" "grpc") ("cosmwasm") ("bech32ibc")))) (r "1.56")))

