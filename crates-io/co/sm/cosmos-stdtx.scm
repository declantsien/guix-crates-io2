(define-module (crates-io co sm cosmos-stdtx) #:use-module (crates-io))

(define-public crate-cosmos-stdtx-0.0.1 (c (n "cosmos-stdtx") (v "0.0.1") (d (list (d (n "anomaly") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1d0qdvdvcz4lah17ny8qk280f4vrq0gbagzv1b3zz7mlblk02z69") (y #t)))

