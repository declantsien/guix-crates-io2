(define-module (crates-io co sm cosmian_ffi) #:use-module (crates-io))

(define-public crate-cosmian_ffi-1.0.0 (c (n "cosmian_ffi") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1pdfn8b6pmmx910nqg095xpk62i1m2g1fs5jw708wf2z5p1jw0yl")))

(define-public crate-cosmian_ffi-1.0.1 (c (n "cosmian_ffi") (v "1.0.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "13ih0mjjsk1qpcnfzgsa9yy4afz6f29km3s5xki2645r0sprmxpw")))

