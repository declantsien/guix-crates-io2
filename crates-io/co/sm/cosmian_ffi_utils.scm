(define-module (crates-io co sm cosmian_ffi_utils) #:use-module (crates-io))

(define-public crate-cosmian_ffi_utils-0.1.0 (c (n "cosmian_ffi_utils") (v "0.1.0") (h "1d4wp39y3wncmsjn2lp143zaca16sb5zmvw3c68pxng5h703axni")))

(define-public crate-cosmian_ffi_utils-0.1.1 (c (n "cosmian_ffi_utils") (v "0.1.1") (h "01rcmvczcq8h49bpfhz8x65az1rvnhpm3k1cwal615v7gvzidlir")))

(define-public crate-cosmian_ffi_utils-0.1.2 (c (n "cosmian_ffi_utils") (v "0.1.2") (h "1jx7nvacixj2qj6m8v5rx87fi03qwxwl33qq5lbl3dp8nh4wpnw7")))

(define-public crate-cosmian_ffi_utils-0.1.3 (c (n "cosmian_ffi_utils") (v "0.1.3") (h "1xdk7w69m16x7rybha4l4fg8a8z6kkmqkq3f5pxrv6i9nnxpkv6p")))

(define-public crate-cosmian_ffi_utils-0.1.4 (c (n "cosmian_ffi_utils") (v "0.1.4") (h "02pxbb1l1rdfyip7cn9d4l0rv4l6pz7ff0p123as55bh2bkmm00n")))

