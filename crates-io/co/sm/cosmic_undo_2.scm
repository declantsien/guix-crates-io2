(define-module (crates-io co sm cosmic_undo_2) #:use-module (crates-io))

(define-public crate-cosmic_undo_2-0.2.0 (c (n "cosmic_undo_2") (v "0.2.0") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1zhswkplbzg2rswfg17dykj4paksvfad8aw82wv69m0mm6mj7ldg") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.64")))

