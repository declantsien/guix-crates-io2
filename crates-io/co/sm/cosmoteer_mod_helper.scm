(define-module (crates-io co sm cosmoteer_mod_helper) #:use-module (crates-io))

(define-public crate-cosmoteer_mod_helper-1.0.0 (c (n "cosmoteer_mod_helper") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)))) (h "1by9daz9ff34rjwpvvkvkjy2hbv2flrh74wrxhhnpza9gi333vsr")))

(define-public crate-cosmoteer_mod_helper-1.0.1 (c (n "cosmoteer_mod_helper") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)))) (h "0sy3qs8mz7ymgqzbhvghvl89fhi3603m7j9dmz9lhd1r3f0hggzg")))

(define-public crate-cosmoteer_mod_helper-1.0.2 (c (n "cosmoteer_mod_helper") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)))) (h "0dxvds6bszi7576dmaibs94xw8zxj4nfzp74lh95mn0phcwl8yvv")))

