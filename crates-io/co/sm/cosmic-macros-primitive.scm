(define-module (crates-io co sm cosmic-macros-primitive) #:use-module (crates-io))

(define-public crate-cosmic-macros-primitive-0.3.0-alpha3 (c (n "cosmic-macros-primitive") (v "0.3.0-alpha3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pkw722aglg4zmyy8qhvrjw6rwd06v6qhfyn62zf8akmgzdhycp0")))

(define-public crate-cosmic-macros-primitive-0.3.0-alpha4 (c (n "cosmic-macros-primitive") (v "0.3.0-alpha4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kcdip27ykkkzsl64hkddb123wc51w60d9viwi5vllhsibd4wz95")))

(define-public crate-cosmic-macros-primitive-0.3.0-alpha5 (c (n "cosmic-macros-primitive") (v "0.3.0-alpha5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11zlkrgcg7cv5q7idbh9v8c4fzkyhrqzrj8i3z890w6qy5c8ajmz")))

(define-public crate-cosmic-macros-primitive-0.3.0-alpha6 (c (n "cosmic-macros-primitive") (v "0.3.0-alpha6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s27rxmyyjqlch81wyd12wz3nh2zrajapc3p18bmsj0n0zm23021")))

(define-public crate-cosmic-macros-primitive-0.3.0-alpha7 (c (n "cosmic-macros-primitive") (v "0.3.0-alpha7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z3j27j58hk610lsm5r1l5ssv2lvv8l8hwp4xylj20y45fsg3kkh")))

(define-public crate-cosmic-macros-primitive-0.3.0-alpha8 (c (n "cosmic-macros-primitive") (v "0.3.0-alpha8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bhbhh7b2i8jhr13sn2971af9jspcm66q8cw5fhm01nyqlvrrgnv")))

(define-public crate-cosmic-macros-primitive-0.3.0-alpha9 (c (n "cosmic-macros-primitive") (v "0.3.0-alpha9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1655wjwlsp5xy1k92g5c3c4acj4x7kvk90dnl1lmsyrvp1z0224f")))

(define-public crate-cosmic-macros-primitive-0.3.0 (c (n "cosmic-macros-primitive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "196nggpgrq3glxawmad1x24zb29kshcgqz8n1bljcbkqrajmsvpv")))

(define-public crate-cosmic-macros-primitive-0.3.1-alpha (c (n "cosmic-macros-primitive") (v "0.3.1-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kx6k29dp91snzf4f53ncv28r129ciy011wpcmaz5ms9ghgvgljs")))

(define-public crate-cosmic-macros-primitive-0.3.1 (c (n "cosmic-macros-primitive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n5dwbh63kz36p6z8563g80s0pjxxzrq8j3jzfswfjw52v3lg81c")))

(define-public crate-cosmic-macros-primitive-0.3.2 (c (n "cosmic-macros-primitive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fjqwfzppg7f74ffnnqkdi3pbhin9d7zx8khaqq27immf8kkkb8q")))

(define-public crate-cosmic-macros-primitive-0.3.3 (c (n "cosmic-macros-primitive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y34p0sqpl67yjx0ih2lgcig8mkw3vc04qnsz13xid9v591rkqns")))

(define-public crate-cosmic-macros-primitive-0.3.4 (c (n "cosmic-macros-primitive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10mk3sf6p16jnpfph3mjwifj5p714h2vw3jmf2sy9964l04f8fl0")))

(define-public crate-cosmic-macros-primitive-0.3.6-rc1 (c (n "cosmic-macros-primitive") (v "0.3.6-rc1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sdas46724abmjsczr0y5rif9cdg3csfcgc06aapx639ia8jh0nc")))

(define-public crate-cosmic-macros-primitive-0.3.6-rc2 (c (n "cosmic-macros-primitive") (v "0.3.6-rc2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11b5mypswqglvq2z6ypyw6i4k30iz3vaj7f56fcrl90zpp53y57d")))

(define-public crate-cosmic-macros-primitive-0.3.6-rc3 (c (n "cosmic-macros-primitive") (v "0.3.6-rc3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bl37a6638kmggn039m54v80jcld19wdxh2870j4h3xahsq7sdwb")))

(define-public crate-cosmic-macros-primitive-0.3.6-rc4 (c (n "cosmic-macros-primitive") (v "0.3.6-rc4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fgfaz88j7xxwyrpknlw3jfaraklkv8f6yl3pa0wa9v74ywnzjgl")))

(define-public crate-cosmic-macros-primitive-0.3.6-rc5 (c (n "cosmic-macros-primitive") (v "0.3.6-rc5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gywnypksaryyv8sad6v40yms4nz20mifcyn9gjjsaxqc66qdfhj")))

(define-public crate-cosmic-macros-primitive-0.3.6 (c (n "cosmic-macros-primitive") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07vrgbnk051srj78qikq9b9bm90rja7mlvz83mbjfdi2909fhklx")))

