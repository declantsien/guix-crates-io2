(define-module (crates-io co sm cosmian-wit-bindgen-rust-impl) #:use-module (crates-io))

(define-public crate-cosmian-wit-bindgen-rust-impl-0.1.0 (c (n "cosmian-wit-bindgen-rust-impl") (v "0.1.0") (d (list (d (n "cosmian-wit-bindgen-gen-core") (r "^0.1") (d #t) (k 0)) (d (n "cosmian-wit-bindgen-gen-rust-wasm") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a8kx2pi5yn56xjk66mi6sjply2lci31vb1bndl5dxp81d7mmbax") (f (quote (("witx-compat" "cosmian-wit-bindgen-gen-core/witx-compat"))))))

(define-public crate-cosmian-wit-bindgen-rust-impl-0.1.1 (c (n "cosmian-wit-bindgen-rust-impl") (v "0.1.1") (d (list (d (n "cosmian-wit-bindgen-gen-core") (r "^0.1") (d #t) (k 0)) (d (n "cosmian-wit-bindgen-gen-rust-wasm") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d9csdmfgb6f1ik0mka3abgssc5a6bkg8hipifvx3qjkwml6jv41") (f (quote (("witx-compat" "cosmian-wit-bindgen-gen-core/witx-compat"))))))

