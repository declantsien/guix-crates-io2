(define-module (crates-io co sm cosmian-wit-bindgen-rust) #:use-module (crates-io))

(define-public crate-cosmian-wit-bindgen-rust-0.1.0 (c (n "cosmian-wit-bindgen-rust") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cosmian-wit-bindgen-rust-impl") (r "^0.1.0") (d #t) (k 0)))) (h "1dyi295377vr9sg5samic9dzygn2c9aawd6iwpfmgh0zjgsdc25x") (f (quote (("witx-compat" "cosmian-wit-bindgen-rust-impl/witx-compat"))))))

(define-public crate-cosmian-wit-bindgen-rust-0.1.1 (c (n "cosmian-wit-bindgen-rust") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cosmian-wit-bindgen-rust-impl") (r "^0.1.1") (d #t) (k 0)))) (h "1rma15n1gbzn5a94y0m8s6gd6p1qfb78cpyzg4q6vlljkj2iqqig") (f (quote (("witx-compat" "cosmian-wit-bindgen-rust-impl/witx-compat"))))))

