(define-module (crates-io co sm cosmicbox) #:use-module (crates-io))

(define-public crate-cosmicbox-0.5.0 (c (n "cosmicbox") (v "0.5.0") (d (list (d (n "hidapi") (r "^0.5") (d #t) (k 0)))) (h "1q9clwrxr3v1dblwridfi1fm1zz9xzfgzq3vka2pcb4xjcnk2w44")))

(define-public crate-cosmicbox-0.5.1 (c (n "cosmicbox") (v "0.5.1") (d (list (d (n "hidapi") (r "^0.5") (d #t) (k 0)))) (h "0d0x5mash3a6qdqcllf2clrqz8agix974p15vswrsws2ik6svir3")))

