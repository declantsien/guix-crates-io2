(define-module (crates-io co sm cosmwasm-contract-migratable-std) #:use-module (crates-io))

(define-public crate-cosmwasm-contract-migratable-std-0.1.0 (c (n "cosmwasm-contract-migratable-std") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "secret-toolkit") (r "^0.7.0") (f (quote ("permit" "serialization"))) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)) (d (n "serde-cw-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.10") (d #t) (k 0)))) (h "0cad13ib79li3qqf0inm2kkcwdvxwkfvml6vzr14ld5q3ppcs7fp") (f (quote (("default") ("backtraces" "cosmwasm-std/backtraces"))))))

