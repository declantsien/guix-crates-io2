(define-module (crates-io co sm cosmian_std) #:use-module (crates-io))

(define-public crate-cosmian_std-0.1.0 (c (n "cosmian_std") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.5.2") (o #t) (d #t) (k 0)) (d (n "scale") (r "^0.1.1") (d #t) (k 0) (p "scale-core")) (d (n "scale_std") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (o #t) (d #t) (k 0)))) (h "092abk2ghbz11p5yggy2y6y8yq37q3zmhj83vfzaw6fyb8dfy2ml") (f (quote (("emulate" "scale/emulate" "scale_std/emulate" "serde_json" "once_cell"))))))

(define-public crate-cosmian_std-0.1.1 (c (n "cosmian_std") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.5.2") (o #t) (d #t) (k 0)) (d (n "scale") (r "^0.1.1") (d #t) (k 0) (p "scale-core")) (d (n "scale_std") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (o #t) (d #t) (k 0)))) (h "0a1056l6rc4fhm977m0c0qwza5ikm0hidq9sk8xnkz70fl8dl5jh") (f (quote (("emulate" "scale/emulate" "scale_std/emulate" "serde_json" "once_cell"))))))

(define-public crate-cosmian_std-0.1.2 (c (n "cosmian_std") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.5.2") (o #t) (d #t) (k 0)) (d (n "scale") (r "^0.1.1") (d #t) (k 0) (p "scale-core")) (d (n "scale_std") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (o #t) (d #t) (k 0)))) (h "1k99jdhw46qip6c1lng676rk693rcg3i9nf4fqbk75hd31cd2zia") (f (quote (("emulate" "scale/emulate" "scale_std/emulate" "serde_json" "once_cell"))))))

(define-public crate-cosmian_std-0.1.3 (c (n "cosmian_std") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.5.2") (o #t) (d #t) (k 0)) (d (n "scale") (r "^0.1.1") (d #t) (k 0) (p "scale-core")) (d (n "scale_std") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (o #t) (d #t) (k 0)))) (h "0h18q5v10vvliq891dczs5r2i94yw9dylwg86ad60swwqxwvmmrq") (f (quote (("emulate" "scale/emulate" "scale_std/emulate" "serde_json" "once_cell"))))))

(define-public crate-cosmian_std-0.1.4 (c (n "cosmian_std") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.5.2") (o #t) (d #t) (k 0)) (d (n "scale") (r "^0.1") (d #t) (k 0) (p "scale-core")) (d (n "scale_std") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (o #t) (d #t) (k 0)))) (h "1b0yrxwa7dgg5ik6zs2h27g9l82wwagmcwv1jih8sv6c8hd6y8v5") (f (quote (("emulate" "scale/emulate" "scale_std/emulate" "serde_json" "once_cell"))))))

