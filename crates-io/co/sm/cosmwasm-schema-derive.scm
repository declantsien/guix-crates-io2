(define-module (crates-io co sm cosmwasm-schema-derive) #:use-module (crates-io))

(define-public crate-cosmwasm-schema-derive-1.1.0-rc.1 (c (n "cosmwasm-schema-derive") (v "1.1.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "03c5bs8adbcnipq6hz40wqg4sacdknii1i93yrg1yzlqhmzmliyg")))

(define-public crate-cosmwasm-schema-derive-1.1.0-rc.2 (c (n "cosmwasm-schema-derive") (v "1.1.0-rc.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "17b82grvsv8k031allw8ijg436vf3yc0q4hpaiwix5xhssgin5ag")))

(define-public crate-cosmwasm-schema-derive-1.1.0 (c (n "cosmwasm-schema-derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "15p960bi687qvadi4nb8kk4jvg2c9x60dwrsvl4xs6p1gg0c99w8")))

(define-public crate-cosmwasm-schema-derive-1.1.1 (c (n "cosmwasm-schema-derive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1pv1b0593mhkmrlsfv3rhfp3dkd5nsdgwcq67n0szdld6d6y1mbk")))

(define-public crate-cosmwasm-schema-derive-1.1.2 (c (n "cosmwasm-schema-derive") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1d654i65qy8y1wazrmd5a15na06qz2z6jzmdkm9300dwp2ai9g83")))

(define-public crate-cosmwasm-schema-derive-1.1.3 (c (n "cosmwasm-schema-derive") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "070nw0d3w8h5i07f1h63il8wdl224ivagnc70krvycmnidhl6d0q")))

(define-public crate-cosmwasm-schema-derive-1.1.4 (c (n "cosmwasm-schema-derive") (v "1.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "016s8sm7nk3ap8mq66cad7sbkril6vzz1nn9fb98wyjm1g4wzs32")))

(define-public crate-cosmwasm-schema-derive-1.1.5 (c (n "cosmwasm-schema-derive") (v "1.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "051sbwlvsan453v1na3gv0j54cjm29qp7s4iyxkxww7qxr1cn9in")))

(define-public crate-cosmwasm-schema-derive-1.1.6 (c (n "cosmwasm-schema-derive") (v "1.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1cnww52rfmi2rmlbab5w54v74cwkfnk6maqfnfhj86l649fsc2bg") (y #t)))

(define-public crate-cosmwasm-schema-derive-1.1.7 (c (n "cosmwasm-schema-derive") (v "1.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1hnzah42328z107xwrsmwqwwbyn9pznn6gqdrx4a5pxwzly1y1sz") (y #t)))

(define-public crate-cosmwasm-schema-derive-1.1.8 (c (n "cosmwasm-schema-derive") (v "1.1.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0aia3zcaafpn0nlcdg8xljm02wnhrxxv214wyrwmx4jrg6q78r6p")))

(define-public crate-cosmwasm-schema-derive-1.1.9 (c (n "cosmwasm-schema-derive") (v "1.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1855rq3plm2izfv1anz6mvl5j935qjsz0dda2q84ibhkd98qyv50")))

(define-public crate-cosmwasm-schema-derive-1.2.0-beta.0 (c (n "cosmwasm-schema-derive") (v "1.2.0-beta.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "08vs9w13a3gfji2kfdpj8qq98s6ilcw60n978x2q6pn682inkqpl")))

(define-public crate-cosmwasm-schema-derive-1.2.0-beta.1 (c (n "cosmwasm-schema-derive") (v "1.2.0-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0xb4f9g3vhjqrvhkjg238ml1nv7f483mpgb6pr9kbh11lj6rm5lv")))

(define-public crate-cosmwasm-schema-derive-1.2.0-rc.1 (c (n "cosmwasm-schema-derive") (v "1.2.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1nbca7ba1iq8h9gywnxksww96gvs2m2nvxcvghhik188lmqipcw8")))

(define-public crate-cosmwasm-schema-derive-1.2.0 (c (n "cosmwasm-schema-derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0f8xfk82bfgkqpyismvmw651f0jqf23hdkxb18mbkxgfcf0d9771")))

(define-public crate-cosmwasm-schema-derive-1.2.1 (c (n "cosmwasm-schema-derive") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1wr44cs77gpba1crgvvshk996gw98xjkj3wvnrnf855cajcgrmkq")))

(define-public crate-cosmwasm-schema-derive-1.2.2 (c (n "cosmwasm-schema-derive") (v "1.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0jafn7nqpbvy73fhqvp2qhgjbpvx07d77xmb6v1nkjrk22kfwb59")))

(define-public crate-cosmwasm-schema-derive-1.2.3 (c (n "cosmwasm-schema-derive") (v "1.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1f2m4qmfwg4kq1vxmdp3kld4kwk32q7jhh1bb5m726mvm5cx29i9")))

(define-public crate-cosmwasm-schema-derive-1.2.4 (c (n "cosmwasm-schema-derive") (v "1.2.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0j2hlhi16xzn2avid2skkyr0ci1xkv9kx7i7j04v1c5y7nn1ns39")))

(define-public crate-cosmwasm-schema-derive-1.1.10 (c (n "cosmwasm-schema-derive") (v "1.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0vzcj5l0am2lmdx8r8mi2hsw1aydjsmdjl9969scwygfd7izlsgy")))

(define-public crate-cosmwasm-schema-derive-1.2.5 (c (n "cosmwasm-schema-derive") (v "1.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0p3qj9q3nskwqqxldpb7zqzs6yj952bxpqn9fnr34bc2n84zjf5f")))

(define-public crate-cosmwasm-schema-derive-1.2.6 (c (n "cosmwasm-schema-derive") (v "1.2.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1cylzh5lp2rsqaz8x7q7z6k273im4qv040q3q4izjzyjiw5y1lg6")))

(define-public crate-cosmwasm-schema-derive-1.2.7 (c (n "cosmwasm-schema-derive") (v "1.2.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1f5wsjvq7ivr7dz8izmhay8rphn94awnr7nng60b4v204dydznj3")))

(define-public crate-cosmwasm-schema-derive-1.3.0-rc.0 (c (n "cosmwasm-schema-derive") (v "1.3.0-rc.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "18mh10ji6nr2nap9lrw3463m8fg3jc84d11asdbfnnsf3wln3d2x")))

(define-public crate-cosmwasm-schema-derive-1.3.0 (c (n "cosmwasm-schema-derive") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0nf69fbdkv15m2777bisz96k35c44mriawfkdyd1r7qd5x13kyib")))

(define-public crate-cosmwasm-schema-derive-1.3.1 (c (n "cosmwasm-schema-derive") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0s8ils2xh7a9b8cj9rnh1hh5bmya1y8hhn16q34qybbnq7kwjv3n")))

(define-public crate-cosmwasm-schema-derive-1.3.2 (c (n "cosmwasm-schema-derive") (v "1.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1mswv487180pgcsipn99s08r6pnkpq1kfhnpz1aiz6jswvv6kq5n")))

(define-public crate-cosmwasm-schema-derive-1.3.3 (c (n "cosmwasm-schema-derive") (v "1.3.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1yyl8cic5n1pd5lpjn4bcl7wp4z57sql1cl66ig8xvw5wpwylx2b")))

(define-public crate-cosmwasm-schema-derive-1.4.0-beta.1 (c (n "cosmwasm-schema-derive") (v "1.4.0-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "16mxnysw69yxgds9y9zyv6z3ygaqqwaky373zbddh7n6rhbvpfzh")))

(define-public crate-cosmwasm-schema-derive-1.4.0-rc.1 (c (n "cosmwasm-schema-derive") (v "1.4.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1961jwn6i638vcb9d3yj95n63qnfgqzr4mag0swf86900bl0mdz0")))

(define-public crate-cosmwasm-schema-derive-1.4.0 (c (n "cosmwasm-schema-derive") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1xwn7ddlkk6nadgqpj4rv4dc42g0mlljb2ln3zm6vpd7h73lwscn")))

(define-public crate-cosmwasm-schema-derive-1.4.1 (c (n "cosmwasm-schema-derive") (v "1.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1n3j0zlcpf8myhxrv5155i4jwg5cnd9yp4w32w4c1h5lrdd04rwm")))

(define-public crate-cosmwasm-schema-derive-1.5.0-rc.0 (c (n "cosmwasm-schema-derive") (v "1.5.0-rc.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0fcvjyz1401k2ifd1w9sdwhisa8kwgiiwcb0451vvbrazv45slha")))

(define-public crate-cosmwasm-schema-derive-1.5.0 (c (n "cosmwasm-schema-derive") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1acbdji143p17bwaaca9916xvl52ak9lscqvjnm6i4qvrs99wq23")))

(define-public crate-cosmwasm-schema-derive-2.0.0-beta.0 (c (n "cosmwasm-schema-derive") (v "2.0.0-beta.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1hh8a72air4dqrwygzzs1zb58n1y86kp288l2s3ikj79nvxc53v5")))

(define-public crate-cosmwasm-schema-derive-1.5.1 (c (n "cosmwasm-schema-derive") (v "1.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1w3khq2ng2fvqhip9bz5kdmwzk0310vmajknbi9ivzbkdpf0hi4h")))

(define-public crate-cosmwasm-schema-derive-1.4.2 (c (n "cosmwasm-schema-derive") (v "1.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1rz7jmp2pgax2jn80fc45131hlwv8a26w418b820sjl1rcgrvfk1")))

(define-public crate-cosmwasm-schema-derive-1.3.4 (c (n "cosmwasm-schema-derive") (v "1.3.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "16qw7zhyxscm3k2sqq680mm4j8khl20dl16v4s39ahlfwsbxjjxc")))

(define-public crate-cosmwasm-schema-derive-1.2.8 (c (n "cosmwasm-schema-derive") (v "1.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "17s7avxh1x1fx4ld9sv0smly3brydf89gzzxcgni4xlhzy3gdl1c")))

(define-public crate-cosmwasm-schema-derive-1.5.2 (c (n "cosmwasm-schema-derive") (v "1.5.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1a4lm4i903xwk4jfk6qys94baw7nq85mkjmpcvdx2adv4272zwcb")))

(define-public crate-cosmwasm-schema-derive-1.4.3 (c (n "cosmwasm-schema-derive") (v "1.4.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0gc59wr3cq1drhsmjzn19gxrx2viv8bjr5wv5yy8anw2s1zwbb8c")))

(define-public crate-cosmwasm-schema-derive-2.0.0-beta.1 (c (n "cosmwasm-schema-derive") (v "2.0.0-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "10g9kwdyxipzl9wrswdw8l4id88hmlii6l5dg8dvyamacv0wpwa4")))

(define-public crate-cosmwasm-schema-derive-1.5.3 (c (n "cosmwasm-schema-derive") (v "1.5.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1cgba84x452ng6wf1ilwafxhvqlyn0p4l2zfs4dxd7mxlsz07n7m")))

(define-public crate-cosmwasm-schema-derive-2.0.0-rc.0 (c (n "cosmwasm-schema-derive") (v "2.0.0-rc.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1zbk7gk16b3jjkwcrafc2nmxpmhirf14a7zf12cp9ikq7ccaiif8")))

(define-public crate-cosmwasm-schema-derive-2.0.0-rc.1 (c (n "cosmwasm-schema-derive") (v "2.0.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1vc53va1g578sqb9wqfbmwgd4dfy9gpz5kvn2nbi9b2n1ipm6lgs")))

(define-public crate-cosmwasm-schema-derive-2.0.0 (c (n "cosmwasm-schema-derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0gfyp16fymfb67kzrp4y5rjja9pd1w5dn7rnhgsnps7nd28v0awa")))

(define-public crate-cosmwasm-schema-derive-2.0.1 (c (n "cosmwasm-schema-derive") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0qy6cyqs50i56r7qcyszq3pi36ma28yk752wiy77p078i9x0vvy0")))

(define-public crate-cosmwasm-schema-derive-2.0.2 (c (n "cosmwasm-schema-derive") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1pvg66hff72bbdpv98sp24zibz2irymxvs5dlszqws7p577lgvnq")))

(define-public crate-cosmwasm-schema-derive-1.5.4 (c (n "cosmwasm-schema-derive") (v "1.5.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "14vk6f13cv0igzgn5jsqa2ncza8znban9rb5yjp242f8ibcqbnzn")))

(define-public crate-cosmwasm-schema-derive-1.4.4 (c (n "cosmwasm-schema-derive") (v "1.4.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0rphhk9q71vyk6iia70x3xp4la39sm183a7v3cb2hyj165lbxr70")))

(define-public crate-cosmwasm-schema-derive-2.0.3 (c (n "cosmwasm-schema-derive") (v "2.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1056k468355f7704jvjcl8s2mgh2x24lcmg2sbg8047970q1bhz3")))

(define-public crate-cosmwasm-schema-derive-1.5.5 (c (n "cosmwasm-schema-derive") (v "1.5.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1dicp1xvsaib296sp5n3d06sq18s7d0hvbj5hizk50zwzdapid8b")))

(define-public crate-cosmwasm-schema-derive-1.4.5 (c (n "cosmwasm-schema-derive") (v "1.4.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "01xs013xkv4kcvs1izf7saxvla6phzcy4pndj8c12wx5imjjdcff")))

