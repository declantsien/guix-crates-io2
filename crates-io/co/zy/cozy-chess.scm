(define-module (crates-io co zy cozy-chess) #:use-module (crates-io))

(define-public crate-cozy-chess-0.1.0 (c (n "cozy-chess") (v "0.1.0") (d (list (d (n "cozy-chess-types") (r "^0.1") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0vr2gi4ldma9cr8bkc4y06is9zbviszqkgh02w4wrcvhqvavz922")))

(define-public crate-cozy-chess-0.1.1 (c (n "cozy-chess") (v "0.1.1") (d (list (d (n "cozy-chess-types") (r "^0.1") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "13pwng67yxa700jmlvri9nc8lzgajsy31xdr8rx09s7fadan7p1b")))

(define-public crate-cozy-chess-0.1.2 (c (n "cozy-chess") (v "0.1.2") (d (list (d (n "cozy-chess-types") (r "^0.1") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.1") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "00zpanzc4gis24xd5nzd54rkwjhgs2vg4f27z65vhj8ax3470i4q")))

(define-public crate-cozy-chess-0.1.3 (c (n "cozy-chess") (v "0.1.3") (d (list (d (n "cozy-chess-types") (r "^0.1.2") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.1.2") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "09zmy1rzv2562n6gj517842zxiilzwgi2f6ypfwd3xxxzfg264i2")))

(define-public crate-cozy-chess-0.1.4 (c (n "cozy-chess") (v "0.1.4") (d (list (d (n "cozy-chess-types") (r "^0.1.3") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.1.3") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0yrmhzl4pqh3sjkvn5rq3681j9zhpy0j46kf6km075lyj1ys6hgw")))

(define-public crate-cozy-chess-0.2.0 (c (n "cozy-chess") (v "0.2.0") (d (list (d (n "cozy-chess-types") (r "^0.1.4") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.1.4") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0bw6hf6x150mqm95salnnp2ww9rv67n89nx168v4j6phra164cra")))

(define-public crate-cozy-chess-0.2.1 (c (n "cozy-chess") (v "0.2.1") (d (list (d (n "cozy-chess-types") (r "^0.1.4") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.1.4") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1mrb2qcsqgidrvqrv2ld4bdn6jmjbw4mbyz5fqzgdijp5w0z7mkj")))

(define-public crate-cozy-chess-0.2.2 (c (n "cozy-chess") (v "0.2.2") (d (list (d (n "cozy-chess-types") (r "^0.1.4") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.1.4") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0fbz5hvcqv2mmi6hgrr59n7pi6dx6p39q4j9bxv974f4wahgqk1d")))

(define-public crate-cozy-chess-0.3.0 (c (n "cozy-chess") (v "0.3.0") (d (list (d (n "cozy-chess-types") (r "^0.2.1") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.2.1") (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0p5a2b6anvm6gy947cvhmagygf14nsj1q1mw6aandcx9rkp7fwmm") (f (quote (("std" "cozy-chess-types/std") ("pext" "cozy-chess-types/pext"))))))

(define-public crate-cozy-chess-0.3.1 (c (n "cozy-chess") (v "0.3.1") (d (list (d (n "cozy-chess-types") (r "^0.2.1") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.2.1") (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1ffkw62jg29ryvgjzwc8bbbibp53ry9slqg381y57a5igvd777r5") (f (quote (("std" "cozy-chess-types/std") ("pext" "cozy-chess-types/pext"))))))

(define-public crate-cozy-chess-0.3.2 (c (n "cozy-chess") (v "0.3.2") (d (list (d (n "cozy-chess-types") (r "^0.2.1") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.2.1") (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "01m3pzw592aqybklfvz54hfjr8ac817yx08gmwch1cb6pv6jvaph") (f (quote (("std" "cozy-chess-types/std") ("pext" "cozy-chess-types/pext"))))))

(define-public crate-cozy-chess-0.3.3 (c (n "cozy-chess") (v "0.3.3") (d (list (d (n "cozy-chess-types") (r "^0.2.1") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.2.1") (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1kq8aa34q8kixwrrsdc14mrxkd6mv76mf96w7rwflacr1fxxggfp") (f (quote (("std" "cozy-chess-types/std") ("pext" "cozy-chess-types/pext"))))))

(define-public crate-cozy-chess-0.3.4 (c (n "cozy-chess") (v "0.3.4") (d (list (d (n "cozy-chess-types") (r "^0.2.2") (d #t) (k 0)) (d (n "cozy-chess-types") (r "^0.2.2") (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0s7zmb45yk8xaj957sayrdhy16r17n5jrh17xs2lzz2n20v9l0i7") (f (quote (("std" "cozy-chess-types/std") ("pext" "cozy-chess-types/pext"))))))

