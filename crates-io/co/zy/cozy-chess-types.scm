(define-module (crates-io co zy cozy-chess-types) #:use-module (crates-io))

(define-public crate-cozy-chess-types-0.1.0 (c (n "cozy-chess-types") (v "0.1.0") (h "06iwy5fa18d2am52gwdxbwj009k5cycazk70q97pmf2rir5d24gx")))

(define-public crate-cozy-chess-types-0.1.1 (c (n "cozy-chess-types") (v "0.1.1") (h "0gmcxi4b09h2427hjgwhqysn217wam16lpc5byzcwq1g3dy73f3m")))

(define-public crate-cozy-chess-types-0.1.2 (c (n "cozy-chess-types") (v "0.1.2") (h "0aj87ghc5qvhylxh5y3zcswrv195gcabmfn8dmqjzn5hvyj131m2")))

(define-public crate-cozy-chess-types-0.1.3 (c (n "cozy-chess-types") (v "0.1.3") (h "0w515dp2rjryi9f390j3n5w0y9fz7c2k7zs1hsxbqibzz21vgspq")))

(define-public crate-cozy-chess-types-0.1.4 (c (n "cozy-chess-types") (v "0.1.4") (h "00xr3a1izarbcs8j23cmmbsxwdj8n7d3qjc2s5l5kv51ww44cz44")))

(define-public crate-cozy-chess-types-0.2.0 (c (n "cozy-chess-types") (v "0.2.0") (h "1pv3zma2zsvaywvfn2bahkhb389651qq5z8kwhkm6iixas9by87m") (f (quote (("std") ("pext"))))))

(define-public crate-cozy-chess-types-0.2.1 (c (n "cozy-chess-types") (v "0.2.1") (h "04hyg95iv7g7hls3xnpz833mj5m6r5rz8chp61fp1f2rcry5ajkg") (f (quote (("std") ("pext"))))))

(define-public crate-cozy-chess-types-0.2.2 (c (n "cozy-chess-types") (v "0.2.2") (h "1safwz7b39x6gmp6a51afkpajqixs9d6d01chlssyiskfw24hsr7") (f (quote (("std") ("pext"))))))

