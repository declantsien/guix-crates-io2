(define-module (crates-io co co cocoa-colors) #:use-module (crates-io))

(define-public crate-cocoa-colors-0.1.0 (c (n "cocoa-colors") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "00kl6ds96pr6wq643js4bnp257drphliar6pn3hasiggxxracdjc")))

(define-public crate-cocoa-colors-0.1.1 (c (n "cocoa-colors") (v "0.1.1") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 1)))) (h "0cyvh6r6abnwv81nqw9j3i9cll7y3f9sf33qf8hqf1ir9zx81h1x")))

(define-public crate-cocoa-colors-0.1.2 (c (n "cocoa-colors") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "15jj9m3wixwi57k1438r6njphcw02iryhz8dq85rwd3g80wk9z57")))

