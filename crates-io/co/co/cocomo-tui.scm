(define-module (crates-io co co cocomo-tui) #:use-module (crates-io))

(define-public crate-cocomo-tui-0.0.1 (c (n "cocomo-tui") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "cocomo-core") (r "^0.0.1") (d #t) (k 0)))) (h "1sq91l9acghkyyx2jvpc889870p5k87vd85168zn8k5kcvg8y0vv")))

