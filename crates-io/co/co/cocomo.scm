(define-module (crates-io co co cocomo) #:use-module (crates-io))

(define-public crate-cocomo-0.1.0 (c (n "cocomo") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "0j1b2ypv7jpjdkhx6rgpwdfm53fpbhfsbdlfhkpbi0ds6cjp4127")))

(define-public crate-cocomo-0.1.1 (c (n "cocomo") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "001dh4lq32ml2ww86b164pp0lxxihy7x7f7fc9fbnr2gi60lbs17")))

(define-public crate-cocomo-0.2.0 (c (n "cocomo") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "0kws86l75kjjxagij16jfvvpgwin2lfwigm2vjhlh7rwb5jnxgsq")))

(define-public crate-cocomo-0.2.1 (c (n "cocomo") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "0yrfv0zy559qzcwq3w790ra85gmwrixbs0qn2m71a7mxi58zmiky")))

(define-public crate-cocomo-0.3.0 (c (n "cocomo") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "05g9knfj8zfmsfvdhwd8hbss7df653a60ydrna8hkaxmaff6rfjv")))

(define-public crate-cocomo-0.3.1 (c (n "cocomo") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "0mmvxb6dcf2zc890kp9k9wys07zvi1yk2gm865lmk7kxyys7j61v")))

(define-public crate-cocomo-0.3.2 (c (n "cocomo") (v "0.3.2") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "1kn2vf6cjg0bqrkbjcxn1vjmn12sqhf9ipv6ss73039y5fd5al2y")))

(define-public crate-cocomo-0.3.3 (c (n "cocomo") (v "0.3.3") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "104cj5f089prbg42g92wbsi9mks5nfx0rq95nrm131rjjgl0ygxd")))

(define-public crate-cocomo-0.4.0 (c (n "cocomo") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "0gar4y7qh6zzn3hnmp45lrfns390rbw1adwwyzrpvy706awmbh9f")))

(define-public crate-cocomo-0.4.1 (c (n "cocomo") (v "0.4.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "0mnirar2c0f5pfrd4869bsa80nawzvyi5vfdx01dayg7cz1q05zy")))

(define-public crate-cocomo-0.5.0 (c (n "cocomo") (v "0.5.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "1nni9az3zr1v6sh632ajrmwsif57qm72r2cn0hrnni1rkbhzki0h")))

(define-public crate-cocomo-0.6.0 (c (n "cocomo") (v "0.6.0") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "format_num") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "0kgv911hy63781n1i0i12z6dr9qbgi6ay8j1l1545zi9lxc0j1yd")))

(define-public crate-cocomo-0.7.0 (c (n "cocomo") (v "0.7.0") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "format_num") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "1rgxqd9zn335afky7fr7vjpa7vjlsgfg5fc8n444md8qh5pcajjc")))

(define-public crate-cocomo-0.7.1 (c (n "cocomo") (v "0.7.1") (d (list (d (n "clap") (r "^4.3.8") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "format_num") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokei") (r "^12.1.2") (d #t) (k 0)))) (h "1arvyna8akylyi05y7dj53mw25zhypixzprzrnjlglj8kvf18azm")))

