(define-module (crates-io co co cocoa-utils) #:use-module (crates-io))

(define-public crate-cocoa-utils-0.1.0 (c (n "cocoa-utils") (v "0.1.0") (d (list (d (n "cocoa") (r "^0.24.0") (d #t) (k 0)) (d (n "cocoa-utils-procmacro") (r "^0.1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9.3") (d #t) (k 0)) (d (n "core-graphics") (r "^0.22.3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.136") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)))) (h "03zmxn8da4wf2m78c434mgn60drxqfs22zrmnw8pd9q9n04az5k7")))

