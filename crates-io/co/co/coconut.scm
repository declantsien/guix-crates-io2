(define-module (crates-io co co coconut) #:use-module (crates-io))

(define-public crate-coconut-0.1.0 (c (n "coconut") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 0)))) (h "1ilhf3wk7x3y7k53fsiygyxqyrjc7i787nq61qb3gxir46snmdy4")))

