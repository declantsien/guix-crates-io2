(define-module (crates-io co co coco-rs) #:use-module (crates-io))

(define-public crate-coco-rs-0.1.0 (c (n "coco-rs") (v "0.1.0") (d (list (d (n "coco-sys") (r "^0.1") (d #t) (k 0)))) (h "1bwhd1sdha1q1hrj3aiszi8qpwdqhcpqp7dng33ignjgca9wrqix")))

(define-public crate-coco-rs-0.2.0 (c (n "coco-rs") (v "0.2.0") (d (list (d (n "coco-sys") (r "^0.2") (d #t) (k 0)))) (h "0bxpgwyk1x83827vr8x2ynhsw2xp8ccihd85y8s7za96amqmn52h")))

(define-public crate-coco-rs-0.3.0 (c (n "coco-rs") (v "0.3.0") (d (list (d (n "coco-sys") (r "^0.3") (d #t) (k 0)))) (h "0jwi5hyjziszdn2j3phva35fidds9rfb8wgffji81mvfqp7a5ad0")))

(define-public crate-coco-rs-0.4.0 (c (n "coco-rs") (v "0.4.0") (d (list (d (n "coco-sys") (r "^0.3") (d #t) (k 0)))) (h "0m37i5j8bvsdbf0bz21gqvlnyghdjmaqqrvr9v5jfvmp7izclsam")))

(define-public crate-coco-rs-0.5.0 (c (n "coco-rs") (v "0.5.0") (d (list (d (n "coco-sys") (r "^0.4") (d #t) (k 0)))) (h "0wkb185cpnrhb4ww9zgri1322fdjzzvsgmjxpd0vvvvjihrgrin2")))

(define-public crate-coco-rs-0.6.0 (c (n "coco-rs") (v "0.6.0") (d (list (d (n "coco-sys") (r "^0.6") (d #t) (k 0)))) (h "10hrfl4wkgcxwz89q4kxqgsfyq490iz6h68lxczwiccbahdqd051")))

(define-public crate-coco-rs-0.7.0 (c (n "coco-rs") (v "0.7.0") (d (list (d (n "coco-sys") (r "^0.6") (d #t) (k 0)))) (h "1ab1xg54rd0zjzfj6m66cdcwlzbhb05k5995g1iyxq28yvh5jzpl")))

