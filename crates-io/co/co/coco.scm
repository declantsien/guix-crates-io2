(define-module (crates-io co co coco) #:use-module (crates-io))

(define-public crate-coco-0.1.0 (c (n "coco") (v "0.1.0") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.3") (d #t) (k 0)))) (h "01khydlbpxma2cih7r0pnjlm168ww4jyhmbsmhqs176w18x5binl") (f (quote (("strict_gc") ("internals"))))))

(define-public crate-coco-0.1.1 (c (n "coco") (v "0.1.1") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.3") (d #t) (k 0)))) (h "1gcibyl09dgms73jwfyyxbij6ba7ic5m9xgbcxy1rqxppvsnjqf0") (f (quote (("strict_gc") ("internals"))))))

(define-public crate-coco-0.2.0 (c (n "coco") (v "0.2.0") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.3") (d #t) (k 0)))) (h "1lzwfpjhc861vlwh7c07fq4syqqfkah5wmy1vlyysc89b077cfy2") (f (quote (("strict_gc") ("internals"))))))

(define-public crate-coco-0.2.1 (c (n "coco") (v "0.2.1") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.3") (d #t) (k 0)))) (h "1yp1x1zcp6ii7l1s4hg2dw465sc303xk8303hcvxbrak130kdhnx") (f (quote (("strict_gc") ("internals"))))))

(define-public crate-coco-0.3.0 (c (n "coco") (v "0.3.0") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.3") (d #t) (k 0)))) (h "186sw72sllxghrdbxbx7a245h20c27xiw279qrikvsx0sfzajxwk") (f (quote (("strict_gc") ("internals"))))))

(define-public crate-coco-0.3.1 (c (n "coco") (v "0.3.1") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.3") (d #t) (k 0)))) (h "1a1ji8llsykzgjy9hbc6xijrxc3drgaw8500xkb2izgm35nk71wa") (f (quote (("strict_gc") ("internals"))))))

(define-public crate-coco-0.3.2 (c (n "coco") (v "0.3.2") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.3") (d #t) (k 0)))) (h "09n1fm5yvbv7w59zw8kxr6ck5cclgmj1391447fyj4bk82n1skgf") (f (quote (("strict_gc") ("internals"))))))

(define-public crate-coco-0.3.3 (c (n "coco") (v "0.3.3") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.3") (d #t) (k 0)))) (h "1xwbsd20hgs4l0dg40sk410m7zbn8zh7p2c1v1i8rjpm7z1jh3gy") (f (quote (("strict_gc") ("internals"))))))

(define-public crate-coco-0.3.4 (c (n "coco") (v "0.3.4") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scopeguard") (r "^0.3") (d #t) (k 0)))) (h "1slbqinnhsqa3p3x92bpmb96jx359h2kk706slsvnk1mr7wyxm84") (f (quote (("strict_gc") ("internals"))))))

