(define-module (crates-io co co coconut_sig) #:use-module (crates-io))

(define-public crate-coconut_sig-0.2.2 (c (n "coconut_sig") (v "0.2.2") (d (list (d (n "amcl_wrapper") (r "^0.2.3") (f (quote ("bls381"))) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "ps_sig") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "secret_sharing") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1x30c7nlwhqkkm7ghfjj29dqlp8hmxfdnnsyy96621rn1gndryal") (f (quote (("default" "SignatureG2") ("SignatureG2") ("SignatureG1"))))))

