(define-module (crates-io co co cocoa-purity) #:use-module (crates-io))

(define-public crate-cocoa-purity-0.1.0 (c (n "cocoa-purity") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1l9g5zpd1nvfawj6hpbw1bg2560whbidsdwhnpmklqbxny37bsp3") (f (quote (("map_enum") ("default" "map_enum"))))))

(define-public crate-cocoa-purity-0.1.1 (c (n "cocoa-purity") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "00622vinyqvb742n5rfa9ay6gsgi9wcimnbf1pwqn6j05hyh1h4c") (f (quote (("map_enum") ("default" "map_enum"))))))

