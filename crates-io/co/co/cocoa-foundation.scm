(define-module (crates-io co co cocoa-foundation) #:use-module (crates-io))

(define-public crate-cocoa-foundation-0.1.0 (c (n "cocoa-foundation") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics-types") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.3") (d #t) (k 0)))) (h "0633ipbd28z35rsdmsl505f1aasrjsrrnirs826aa32nbnv4kpks")))

(define-public crate-cocoa-foundation-0.1.1 (c (n "cocoa-foundation") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics-types") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.3") (d #t) (k 0)))) (h "1mjhas5v0wpnhrf7h6j6fhixn21dl7my8g22b0y6xxc6q8vkh7ck")))

(define-public crate-cocoa-foundation-0.1.2 (c (n "cocoa-foundation") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics-types") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.3") (d #t) (k 0)))) (h "1xwk1khdyqw3dwsl15vr8p86shdcn544fr60ass8biz4nb5k8qlc")))

