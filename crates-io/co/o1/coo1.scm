(define-module (crates-io co o1 coo1) #:use-module (crates-io))

(define-public crate-coo1-0.1.0 (c (n "coo1") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.2.5") (d #t) (k 0)))) (h "18g3kx6jvgvxz2akarpd4klwfrmi4h9wvygzmf9iqmv55wxb3fwr")))

