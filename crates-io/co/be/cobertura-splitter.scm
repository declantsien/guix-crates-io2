(define-module (crates-io co be cobertura-splitter) #:use-module (crates-io))

(define-public crate-cobertura-splitter-0.1.0 (c (n "cobertura-splitter") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)))) (h "1ijkp6h6z5f9gkmpllppql6yjv9lfsf61a1rl6gyn0kdgf4y009d")))

(define-public crate-cobertura-splitter-0.2.0 (c (n "cobertura-splitter") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)))) (h "066688v8aqw0ip0svaxlrb1pwjx4rv7mb9q237n3hclc1jr547kf")))

