(define-module (crates-io co ps copstr) #:use-module (crates-io))

(define-public crate-copstr-0.0.1 (c (n "copstr") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)))) (h "1l1jwqn8kgn9800akykhdmai8j3f1cwdvigqgvrf5iyvq3bfaq5f")))

(define-public crate-copstr-0.0.2 (c (n "copstr") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 2)))) (h "16fd8j1pnirgskkw991x2ff0szr4khbc1hqxvbb9bq0ngimnccc9")))

(define-public crate-copstr-0.0.3 (c (n "copstr") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 2)))) (h "1caybr7zzvswfjlhg1izrrgspf2p9vpydrqyc6cc0hvgppsxl27v")))

(define-public crate-copstr-0.0.4 (c (n "copstr") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)))) (h "13dqkfwsbqm86kdnc2nh38f5x161l974abxblhfvw3c7p0vwwsdg")))

(define-public crate-copstr-0.0.5 (c (n "copstr") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)))) (h "1l2mfafhvi64bf7g22a8wmmk5ayklvcysl9nxl91lwxsjbxi86kr")))

(define-public crate-copstr-0.1.0 (c (n "copstr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)))) (h "1ddn48fl159yc25174ixfkrzqldldp28ap6czqxa72kccicz83sc")))

(define-public crate-copstr-0.1.1 (c (n "copstr") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)))) (h "1xqhnjjhbnppbxx3qcybwqhk04sabfkkdbh5p0dqvxr83c3pp1xs")))

(define-public crate-copstr-0.1.2 (c (n "copstr") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 2)))) (h "0dnkjbdbhfinw8bj6iw807719irkzc11p1iilkdv5pkzarfwlvvl")))

