(define-module (crates-io co pr coproduct) #:use-module (crates-io))

(define-public crate-coproduct-0.1.0 (c (n "coproduct") (v "0.1.0") (d (list (d (n "frunk") (r "^0.4") (d #t) (k 0)))) (h "131qz0vik0gdmpgn14rgw4hkphp4fn0mzsj8ck5fajgd9s15pbx4")))

(define-public crate-coproduct-0.1.1 (c (n "coproduct") (v "0.1.1") (d (list (d (n "frunk") (r "^0.4") (d #t) (k 0)))) (h "17ngms41ybf6hz35hq1qgam10zh2dbcyr5c967xldiimnkvyzb25")))

(define-public crate-coproduct-0.2.0 (c (n "coproduct") (v "0.2.0") (h "12y7yp21r4sx4h9jzmw67ddjqai8qwg250mfnv0psrcai2mla6fy")))

(define-public crate-coproduct-0.3.0 (c (n "coproduct") (v "0.3.0") (d (list (d (n "coproduct-idtype-macro") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0clsviw6c3ay7lh5klnrjjc17r9hwymq6jjvwd1hcjk5gjgjp7zy")))

(define-public crate-coproduct-0.4.0 (c (n "coproduct") (v "0.4.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0m3wdv3pcrfh3vbvr2lk6mprs03c8wh3xz17l1lp139szidxrxlv") (f (quote (("type_inequality_hack"))))))

(define-public crate-coproduct-0.4.1 (c (n "coproduct") (v "0.4.1") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0dc7bpl533j3mmmgd0j9vq9icgr7pcjq59b895adrlqxrcvm6syw") (f (quote (("type_inequality_hack"))))))

