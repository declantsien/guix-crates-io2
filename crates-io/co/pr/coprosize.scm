(define-module (crates-io co pr coprosize) #:use-module (crates-io))

(define-public crate-coprosize-0.0.0 (c (n "coprosize") (v "0.0.0") (h "02fvs4azclv8blakzflngp4ywykg310nk1ci4anjjy8775fw8g0z") (y #t)))

(define-public crate-coprosize-1.0.0-alpha.1 (c (n "coprosize") (v "1.0.0-alpha.1") (h "0s3a6k4syffj05i6drxvix6w34b6zia65zpg6nn8bwr87jkkw6pz") (y #t)))

(define-public crate-coprosize-1.0.0-alpha.2 (c (n "coprosize") (v "1.0.0-alpha.2") (h "0r0cdgvgn0vwdai58x839wxwlz6xrf9gynwccwvvnsx700cd4z10") (y #t)))

(define-public crate-coprosize-1.0.0-alpha.3 (c (n "coprosize") (v "1.0.0-alpha.3") (h "1g1r07wplcgl6v16lbfi8559w5m65wvv850g0mhs8q56fncmmnc6") (y #t)))

(define-public crate-coprosize-1.0.0-alpha.4 (c (n "coprosize") (v "1.0.0-alpha.4") (h "0579yqc53jak0rwvgv6p696vqyrfjjb2vzv1vgqnrxd26vvwd8v5") (y #t)))

(define-public crate-coprosize-1.0.0-alpha.5 (c (n "coprosize") (v "1.0.0-alpha.5") (h "0fzigf64qrllfsrxy4ciijvwb1aib0yjbny9nz49i19m04zm8ymj") (y #t)))

(define-public crate-coprosize-1.0.0-alpha.6 (c (n "coprosize") (v "1.0.0-alpha.6") (h "0hjbj7f8ni4n9i7lpc3yh7gli8mjzfbln9nfi450c2075sda95ji") (y #t)))

(define-public crate-coprosize-1.0.0-alpha.7 (c (n "coprosize") (v "1.0.0-alpha.7") (h "1b7lin3hb1bzmckyv8msalgyr2b5ajz79az8qvgv9zvb6vkk3gp0") (y #t)))

(define-public crate-coprosize-1.0.0-alpha.8 (c (n "coprosize") (v "1.0.0-alpha.8") (h "00x6fa4iqfw3kvbk116c9p2liw7d4dahsnx33x03ar5rv9jf7jqy") (y #t)))

(define-public crate-coprosize-1.0.0-alpha.9 (c (n "coprosize") (v "1.0.0-alpha.9") (h "05m0gq19vc91ln3al371jnyjbar451q5plangjhqqhb16llfn2j7") (y #t)))

(define-public crate-coprosize-1.0.0-beta (c (n "coprosize") (v "1.0.0-beta") (h "1cqb58xjhazns0f6cjdgsswqycj27yd9i4wk8pnv5c6v9vc3kf6l") (y #t)))

(define-public crate-coprosize-1.0.0 (c (n "coprosize") (v "1.0.0") (h "0ijfmajn36mb34f2znn7ipm716dfr46dd5wldy721nhq56fdbhk1")))

(define-public crate-coprosize-1.0.1 (c (n "coprosize") (v "1.0.1") (h "055nkc2jpjsdkns8m1235lfa5w8765sr377ip491xl8lcfrndprl")))

(define-public crate-coprosize-1.0.2 (c (n "coprosize") (v "1.0.2") (h "0fydmpd3s025r8a04rhfsrn8bcjb96hf8ab0fdl0rc581nynj710")))

(define-public crate-coprosize-1.0.3 (c (n "coprosize") (v "1.0.3") (h "02frwmvh8dfwgidss4280y5h0c5bvhaxq2wbir8yx619swbbxh84")))

(define-public crate-coprosize-1.0.4 (c (n "coprosize") (v "1.0.4") (h "06xl5794k9g2r02a60iwrpzrlxmv9naxaspr5wf3kxl6nv0ak0i7")))

