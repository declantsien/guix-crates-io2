(define-module (crates-io co ls colsol) #:use-module (crates-io))

(define-public crate-colsol-0.1.0 (c (n "colsol") (v "0.1.0") (h "1cqzc1xhblcbyb1yxzw26ryf91lq93sb7ycz3bjksmz871bv967x") (y #t)))

(define-public crate-colsol-0.1.1 (c (n "colsol") (v "0.1.1") (h "12510c1pjf716jm0jn8n3g4jvi39bj6sfmfpbqz34s7gp3vcw6lx") (y #t)))

(define-public crate-colsol-0.1.2 (c (n "colsol") (v "0.1.2") (h "0qqp10z97ydn2gc114bynp70p65ln34xzagiyp7lwkwi4xr3ping") (y #t)))

(define-public crate-colsol-0.1.3 (c (n "colsol") (v "0.1.3") (h "1vx83mmfa1dwhxx1395vlsr0cv684bw0r0g40x5dkssbfrbwzg14") (y #t)))

(define-public crate-colsol-0.1.4 (c (n "colsol") (v "0.1.4") (h "0b8bzpw7czncy61p56nz34v1i95h2i0ms3bbw7x1lz6kiqwx3jj1") (y #t)))

(define-public crate-colsol-1.0.0 (c (n "colsol") (v "1.0.0") (h "1cfw8g92pys3qnz1sfshghvzkxa75lynh04h6h3iaw5500rlc402")))

