(define-module (crates-io co qu coqui-stt-sys) #:use-module (crates-io))

(define-public crate-coqui-stt-sys-1.1.0 (c (n "coqui-stt-sys") (v "1.1.0") (h "1bkva100z9w0vida2658qwlq3wwbhfxz7k6sw5dcph6ddqzj3syc")))

(define-public crate-coqui-stt-sys-1.1.1 (c (n "coqui-stt-sys") (v "1.1.1") (h "05sg3qzzv3jk1yn6gy227phb0ss3658c5lplbzadd63dbg25qmpc")))

(define-public crate-coqui-stt-sys-1.3.0 (c (n "coqui-stt-sys") (v "1.3.0") (h "19fy4qwys94n6q8j3n1lgsmqgz2xb76c79lk1f0bqglz3kgxviwm")))

(define-public crate-coqui-stt-sys-1.3.1 (c (n "coqui-stt-sys") (v "1.3.1") (h "1scva70w1giww3110rw35887zbsavv3z09n3liyg9jnrazslx8jx")))

