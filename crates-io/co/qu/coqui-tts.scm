(define-module (crates-io co qu coqui-tts) #:use-module (crates-io))

(define-public crate-coqui-tts-0.0.0 (c (n "coqui-tts") (v "0.0.0") (h "19pvwba35pzissqnx34ij7i5amb7fcl8nd19jhd5is4xwn445lsy")))

(define-public crate-coqui-tts-0.1.0 (c (n "coqui-tts") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.16.5") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "196adnf6kk27b0b4zl53jx8bzqp14isr5akxfdlc3k7zc93z9kha")))

(define-public crate-coqui-tts-0.2.0 (c (n "coqui-tts") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.18.2") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "02s1xxi8fvbck5rzmm4yh09rcvvlq29h06s4zcjwc864cd7dxw2z")))

