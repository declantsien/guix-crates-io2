(define-module (crates-io co rg corg) #:use-module (crates-io))

(define-public crate-corg-0.1.0 (c (n "corg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (o #t) (d #t) (k 0)) (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "shlex") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "001jwqi3qji3gfr098hqlm3gapkasglyc46p0bbnpzaifny7jv2l") (f (quote (("cli" "anyhow" "difference" "clap" "colored" "checksum") ("checksum" "md5"))))))

