(define-module (crates-io co rg corgi) #:use-module (crates-io))

(define-public crate-corgi-0.1.0 (c (n "corgi") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1l4phpcisi6gpbdl25bn7x60jj14426bbfz6ng7g5r44x08530b4") (f (quote (("f32"))))))

(define-public crate-corgi-0.2.0 (c (n "corgi") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "18g403h00x93bwyfbr8ykga3y33pvdmw0rngqpblnjz1dr44s8pp") (f (quote (("f32")))) (y #t)))

(define-public crate-corgi-0.2.1 (c (n "corgi") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0z76d0yabjldicqbkjn8gccvnbl54zk1gflbivxwzrl10mxkgnay") (f (quote (("f32"))))))

(define-public crate-corgi-0.3.0 (c (n "corgi") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "11kcr15c93x36zj9y0vhspmgf9lh3bbr2c3n9hw11wdaywhbqy2d") (f (quote (("f32"))))))

(define-public crate-corgi-0.4.0 (c (n "corgi") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0lb52wxciv81rga1nyr7q481hyc5vg95fjap5qfwzj4fcn3inq2b") (f (quote (("f32"))))))

(define-public crate-corgi-0.5.0 (c (n "corgi") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "165vsmajv5ka72nshbwh362dgq87x4a8wmd1jjy88yi9zpps35vb") (f (quote (("f32") ("blas")))) (y #t)))

(define-public crate-corgi-0.5.1 (c (n "corgi") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0ysvjh6mwwcp329zcdqh32w38ps78knwzs7nz232y31l5d5v7nca") (f (quote (("f32") ("blas")))) (y #t)))

(define-public crate-corgi-0.5.2 (c (n "corgi") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0h0c6jgh46nbjn1drc8n2by7f43gg4r2vza6gimbd3pxcvcs9rvc") (f (quote (("f32") ("blas"))))))

(define-public crate-corgi-0.6.0 (c (n "corgi") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "09fp79k0ysqvch85dn9vvpk4pv50ibypbzhr3x3k03b0zk212v7p") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.7.0 (c (n "corgi") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0n14l5alb2k1jgp7dlzdxdhdlwz2hs3y3vqds2s4iavg0i5ckhgb") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.7.1 (c (n "corgi") (v "0.7.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1v8rkplq5ipq1rixjszxr8c34jxf8k7mqzn5snnz90y78ywb91li") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.7.2 (c (n "corgi") (v "0.7.2") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "145x2z93mczc354nhjl01dnyg7yw62wg3lhh4gm577ngyp75zpqz") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.7.3 (c (n "corgi") (v "0.7.3") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1kra1jfyjprx2mhha66m33ayrkl4cpv90lf30nx1sf3imzdyvqd1") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.8.0 (c (n "corgi") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "09v5jjbri69kdf08gsh020r3mkai8ck22l4sp0qj55farnfpfmg9") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.8.1 (c (n "corgi") (v "0.8.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1j039nwfavc5w8xvnhhlv8jj595anq7w03wk6qqk6n03aggc8msa") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.8.2 (c (n "corgi") (v "0.8.2") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1p9jwkqby407q6l98jbg0n1n3djxk547jb3aj3mhw4qg8sp0l9vj") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.8.3 (c (n "corgi") (v "0.8.3") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1azk90dkijsj6vbr0ck7kbmcs2bbaw0l4br7ahhl47bd909pic3c") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.8.4 (c (n "corgi") (v "0.8.4") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1d8v36b73bzw4m0q796nfqgsisaddkpncyvzbcs0mmb89jj99qfm") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.8.5 (c (n "corgi") (v "0.8.5") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0yydzk2hp96173qij0rvnm78a63qgz80w2rr6mg8hns561zhqlzp") (f (quote (("f32") ("blas" "libc")))) (y #t)))

(define-public crate-corgi-0.8.6 (c (n "corgi") (v "0.8.6") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1akq2g1pcgq5j4ps6f4hb2yl70wbsdr5dl5qy0vpilyhyi12n464") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.8.7 (c (n "corgi") (v "0.8.7") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "07zix67nrbq6n9pb99jp65yys6na2h61558jqw0pv1r7c973f246") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.8.8 (c (n "corgi") (v "0.8.8") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "018rl01h704gkvhdf4pla4y8qgki20lks2hra9113n1sh5y18j19") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.9.0 (c (n "corgi") (v "0.9.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1d8iyyl1y3dsdinccjrqh64a7aw7v7w3janrpx27djmwhf1bw6hn") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.9.1 (c (n "corgi") (v "0.9.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0a6k7khs53gnyhs84my85ma43020501pmirfdvpmqg6q44hzf9ih") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.9.2 (c (n "corgi") (v "0.9.2") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "07ppzqk7jzr7jbpvqhml929pdf3v7y8712g0yn1ks87wbkx9w4z5") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.9.3 (c (n "corgi") (v "0.9.3") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1c0s4ixyqqx757ab0wh4mw83hacg830j9q6q561zh97nb8h59hzb") (f (quote (("f32") ("blas" "libc")))) (y #t)))

(define-public crate-corgi-0.9.4 (c (n "corgi") (v "0.9.4") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "11xsx19q0r5slpbdwwkhrz1swbmr8sf88da8jwcx0qriavdv4mr3") (f (quote (("f32") ("blas" "libc"))))))

(define-public crate-corgi-0.9.5 (c (n "corgi") (v "0.9.5") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "blas-src") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "cblas-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1h0wsmz1zxwdqyhdmvvd8z8dy3jmd5ysf700v75w735rw7klwrh3") (f (quote (("openblas" "blas" "cblas-sys" "blas-src/openblas") ("netlib" "blas" "cblas-sys" "blas-src/netlib") ("f32") ("blas"))))))

(define-public crate-corgi-0.9.6 (c (n "corgi") (v "0.9.6") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "blas-src") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "cblas-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1pab1a30v6yjpg70w6mgqmghscr80z9wnw7l2rjy36rl9a8xnj0a") (f (quote (("openblas" "blas" "cblas-sys" "blas-src/openblas") ("netlib" "blas" "cblas-sys" "blas-src/netlib") ("f32") ("blas"))))))

(define-public crate-corgi-0.9.7 (c (n "corgi") (v "0.9.7") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "cblas-sys") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.26") (k 0)) (d (n "netlib-src") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "openblas-src") (r "^0.10.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "173mihw1pnhll519f7gjxm7zrp29pmjdp15aarasz5j52jqzbvpg") (f (quote (("openblas" "blas" "cblas-sys" "openblas-src/static") ("netlib" "blas" "cblas-sys" "netlib-src/static") ("f32") ("blas"))))))

(define-public crate-corgi-0.9.8 (c (n "corgi") (v "0.9.8") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "cblas-sys") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.26") (k 0)) (d (n "netlib-src") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "openblas-src") (r "^0.10.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "16crwr8kw6dwzm44n8f5av4ps1bhi93a21vhvv3djzjkvm0gszxy") (f (quote (("openblas" "blas" "cblas-sys" "openblas-src/static") ("netlib" "blas" "cblas-sys" "netlib-src/static") ("f32") ("blas"))))))

(define-public crate-corgi-0.9.9 (c (n "corgi") (v "0.9.9") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 0)) (d (n "cblas-sys") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.26") (k 0)) (d (n "netlib-src") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "openblas-src") (r "^0.10.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "191yjms2qc7bybwalxzg8z8l20f3xgqb6kpfbpkr6zmr8ycaliqz") (f (quote (("openblas" "blas" "cblas-sys" "openblas-src/static") ("netlib" "blas" "cblas-sys" "netlib-src/static") ("f32") ("blas"))))))

