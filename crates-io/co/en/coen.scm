(define-module (crates-io co en coen) #:use-module (crates-io))

(define-public crate-coen-0.1.0 (c (n "coen") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p6wpia1l6l1da4akhmxh13g76q2dc3wbklscdp70aphhyaqis4z")))

(define-public crate-coen-0.1.1 (c (n "coen") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1n12jm5xvnxigd4da4bb0xyjyar7fb6wzwkrsz6x8v064nds288l")))

(define-public crate-coen-0.1.2-alpha (c (n "coen") (v "0.1.2-alpha") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1dzkhmg8i8prjyd9rsdgb4h9dv84dqb069zq5xi9r2ddfydvxf46")))

