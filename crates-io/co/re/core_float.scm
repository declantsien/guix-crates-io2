(define-module (crates-io co re core_float) #:use-module (crates-io))

(define-public crate-core_float-0.1.0 (c (n "core_float") (v "0.1.0") (h "0s2an4rqkv5gh52hciih3cykgc34m0gj438qz5mg4x3ixfi3463f") (y #t)))

(define-public crate-core_float-0.1.1 (c (n "core_float") (v "0.1.1") (h "1qbgzzbnmic0y6xp9yb25lpah735h04z4ba31pfvxrk9mpz0bv6z")))

(define-public crate-core_float-0.1.2 (c (n "core_float") (v "0.1.2") (h "1rb2s1ahybww80mhc598g02q22q5ss2ab6cpyw3c03yshx0b4vpf")))

(define-public crate-core_float-0.2.0 (c (n "core_float") (v "0.2.0") (h "14vyh16ylylr0r7cl02kana7q0zrdr47l8nnn2g3gfm1660mfn9d")))

(define-public crate-core_float-0.3.0 (c (n "core_float") (v "0.3.0") (h "0rid08kgh9avqpiihz6jckg917d293h7kqix7w2r6c4a67hzpkh9")))

(define-public crate-core_float-0.3.1 (c (n "core_float") (v "0.3.1") (h "0vsx0yynssr65y726ggbf9yi3kjf38dka1z00ylg4nxfw79w9pl7")))

