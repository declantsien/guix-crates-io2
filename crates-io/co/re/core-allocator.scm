(define-module (crates-io co re core-allocator) #:use-module (crates-io))

(define-public crate-core-allocator-0.1.0 (c (n "core-allocator") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1xx82hpnl5yq1j8w5hdqcamny6a6nha954305vxza6da5wxr8rkw")))

(define-public crate-core-allocator-0.1.1 (c (n "core-allocator") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1zq6azysvsq6b8iifc2zdzzmjwsvbihhshypc9fw30shrxb4b1p8")))

(define-public crate-core-allocator-0.1.2 (c (n "core-allocator") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1xr32qaxdh40wczz6h8xk4y5s3k39gypdhwx4rpxpmjn5h5y2vfw")))

(define-public crate-core-allocator-0.1.3 (c (n "core-allocator") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "023j7d71shmfv7qamwpmbyk37hj1djpc2jmjpcl60yaivnvm427r")))

(define-public crate-core-allocator-0.1.4 (c (n "core-allocator") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1xm4hdfzkv1wxpms93v5qqssjw6pir28gzc89zi0vq4wvlwnwwds")))

(define-public crate-core-allocator-0.1.5 (c (n "core-allocator") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "001fv88pfab5npwwqi7721bj8x3gi3j4adhp6hxjkdgmzk7j90xh")))

(define-public crate-core-allocator-0.1.6 (c (n "core-allocator") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1cwilgw8fqdx8l5pbq7l9dyzgzwadfxwhrhnpn9lh8bca3kq8dix")))

(define-public crate-core-allocator-0.1.7 (c (n "core-allocator") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc2") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0diqv1pkd62b9dfwpkq0mr3vbmglgcz615jvflfbnvyxw0ww0blk")))

(define-public crate-core-allocator-0.1.8 (c (n "core-allocator") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc2") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0g22yk7z4321v05gpbyixvmdggyzrc6h03crk8k006ppnn6lypdk") (f (quote (("default" "hwloc2"))))))

(define-public crate-core-allocator-0.2.0 (c (n "core-allocator") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc2") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1fb4g2qkv2g6arbxc7ciazm82b1kw08p9497x9yaxr1jxkajwhr5") (f (quote (("default" "hwloc2"))))))

(define-public crate-core-allocator-0.2.1 (c (n "core-allocator") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc2") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0jx82abjhcvpg1k3fy8d1rr65cn5d06hq1j1rqjzdiwnn0l70wkx") (f (quote (("default" "hwloc2"))))))

(define-public crate-core-allocator-0.2.2 (c (n "core-allocator") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hwloc2") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1qbb6m5asjs2har1dgmf6xd6a20jmz2cgk6g4s18hfbw3zwv1dfs") (f (quote (("default" "hwloc2"))))))

