(define-module (crates-io co re coredump) #:use-module (crates-io))

(define-public crate-coredump-0.1.0 (c (n "coredump") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xxmnc2mcrmc4qbv9h7s43faqhbbxb7r6kb66wlrwbvl5z3nz0dh")))

(define-public crate-coredump-0.1.1 (c (n "coredump") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c57yrnxs7gpnax4h5rbmypmxn1a8qm1pdinddzmyz6lx1h9j1y4")))

(define-public crate-coredump-0.1.2 (c (n "coredump") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ddkrv2gdlnvlvbndsfqysyzkyndplhq5j4vc9d3fb6fcnl2rcf7")))

