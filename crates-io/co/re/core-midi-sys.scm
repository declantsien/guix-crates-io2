(define-module (crates-io co re core-midi-sys) #:use-module (crates-io))

(define-public crate-core-midi-sys-0.1.0 (c (n "core-midi-sys") (v "0.1.0") (d (list (d (n "core-foundation-sys") (r "^0.2.2") (d #t) (k 0)))) (h "0fwigp9caf08k7byx4xgfbqc0ig8fjj54vpv223lb9f8kqhff2m7")))

(define-public crate-core-midi-sys-0.1.1 (c (n "core-midi-sys") (v "0.1.1") (d (list (d (n "core-foundation-sys") (r "^0.2.2") (d #t) (k 0)))) (h "1ja5vphb81kwmxs23qg7jfyd9n68y3wzxfqzfc749cg7c46wgfc0")))

