(define-module (crates-io co re core-error) #:use-module (crates-io))

(define-public crate-core-error-0.0.0 (c (n "core-error") (v "0.0.0") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "13wvc7lcpi7f6rr0racns4l52gzpix4xhih6qns30hmn5sbv5kgg") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-core-error-0.0.1-rc1 (c (n "core-error") (v "0.0.1-rc1") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0pzbild9wwp15f2plz0frkdsq39fji0wj0pzxpnim0dpkw5p77cl") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-core-error-0.0.1-rc2 (c (n "core-error") (v "0.0.1-rc2") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1im8rrkj0d8aiq8r0agia80f44dvw80mkds5y19h59z5839rjja7") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-core-error-0.0.1-rc3 (c (n "core-error") (v "0.0.1-rc3") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0ibjyk7f1asqp54jwdh5vdsybjb572xkrmbnwljg5r17d2dcdl0x") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-core-error-0.0.1-rc4 (c (n "core-error") (v "0.0.1-rc4") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0xzr47ym8rn2hv6bns3pd7n3zh27yqcy27hz5xgwzsvl3svbqlbq") (f (quote (("std") ("default") ("alloc"))))))

