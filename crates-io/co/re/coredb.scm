(define-module (crates-io co re coredb) #:use-module (crates-io))

(define-public crate-coredb-15.1.0-coredb.1 (c (n "coredb") (v "15.1.0-coredb.1") (h "0jlk1zycl860vxnvl9rs3izc929mmcsl5zkxrgf9ksyfksz58kb0")))

(define-public crate-coredb-15.1.0-coredb.2 (c (n "coredb") (v "15.1.0-coredb.2") (h "05ysgpllp4hlzb9w43zky7yjkcwgc59lqrrsvxiiv37jzlrn7y0f")))

(define-public crate-coredb-15.1.0-coredb.3 (c (n "coredb") (v "15.1.0-coredb.3") (h "0r5higjg6j9adc8jjwfxygkcnlg7al650pcl5y2xkbly9wnpzdgm")))

(define-public crate-coredb-15.1.0-coredb.4 (c (n "coredb") (v "15.1.0-coredb.4") (h "1xd9pl3spxxi37rb6kxdxl5da6cdy28vgfl2xakvh07s7gfahpbj")))

(define-public crate-coredb-15.1.0-coredb.5 (c (n "coredb") (v "15.1.0-coredb.5") (h "1ac2ikylmzqbsmgxiygl1frlsab8x06hj6a44jmgibf7v5cn25if")))

(define-public crate-coredb-15.1.0-coredb.6 (c (n "coredb") (v "15.1.0-coredb.6") (h "0aaayy0ql6bljjaahyf56l5knajxpi02i1c2yls8xkmb72a965ix")))

(define-public crate-coredb-15.1.0-coredb.7 (c (n "coredb") (v "15.1.0-coredb.7") (h "1khzw2ks1smfj72rzpgkwq9lq096kzdikck5gn66bgj3hzmqbfb9")))

(define-public crate-coredb-15.1.0-coredb.8 (c (n "coredb") (v "15.1.0-coredb.8") (h "0d5ysg43f5jrg03slz4432k3gbwvdvmc26az4wn6p8235aij0gha")))

(define-public crate-coredb-15.1.0-coredb.9 (c (n "coredb") (v "15.1.0-coredb.9") (h "0cn2hyh23n9vkl47azfhqw7kd8zsm2n3xc1h574n5sy23z0r4fpm")))

(define-public crate-coredb-15.1.0-coredb.10 (c (n "coredb") (v "15.1.0-coredb.10") (h "0x5yxm8l2n3ywxcdg51q8p9jjsfydrxxvx1g9sbc6n9f92y2f6sk")))

(define-public crate-coredb-15.1.0-coredb.11 (c (n "coredb") (v "15.1.0-coredb.11") (h "01d35c129wd5yb1cdr0blq128zw18kyibj20f45gjz3air015c2j")))

(define-public crate-coredb-15.2.0-coredb.1 (c (n "coredb") (v "15.2.0-coredb.1") (h "0c19257vxbr00kbj5bdww8nqgps6v8p2m66dz2m2vlhbhdj6yc3r")))

(define-public crate-coredb-15.2.0-coredb.2 (c (n "coredb") (v "15.2.0-coredb.2") (h "0q0556h5ac6hrwl9hq9wdpmh9ps1hw9js9dgc5nqc9xgckafx3bx")))

(define-public crate-coredb-15.2.0-coredb.3 (c (n "coredb") (v "15.2.0-coredb.3") (h "0a57p5d9sd9fa6kn018vh4qj57rfsa0kn8isdq3cc0xlb6a4awg8")))

(define-public crate-coredb-15.2.0-coredb.4 (c (n "coredb") (v "15.2.0-coredb.4") (h "0acdikfxj323wjcw6vqddim7zmimzs24hdpg3zhsvdms5d2fb8rd")))

