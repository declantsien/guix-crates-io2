(define-module (crates-io co re coredb-cli) #:use-module (crates-io))

(define-public crate-coredb-cli-0.1.0 (c (n "coredb-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "1agcg76915kni6azrjqc8f33hsnj0xhr7f9kn2jq3i7d9wffgsn5")))

(define-public crate-coredb-cli-2023.1.23 (c (n "coredb-cli") (v "2023.1.23") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "1ff2apx9qfnka49j05av40nksq831hf8ks9hwnbwwzg1mavkas3h") (y #t)))

(define-public crate-coredb-cli-0.2.0-rc (c (n "coredb-cli") (v "0.2.0-rc") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "0w8kp9i2bbjwi3lknz63fwrkjmcnvk5n4dkqwayk0rwmj0g4hb7g") (y #t)))

(define-public crate-coredb-cli-0.2.0-rc-45a3ee8 (c (n "coredb-cli") (v "0.2.0-rc-45a3ee8") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "0qmzvcasl7r4vq83rgrb86hvl24pq3haaslsid5lc58i681ss60y")))

(define-public crate-coredb-cli-0.2.0-rc-94d0041 (c (n "coredb-cli") (v "0.2.0-rc-94d0041") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "1g7l1f948vk956dmqa2q7w77wgqnmzfx01j7b7g788csrdpm6nww")))

(define-public crate-coredb-cli-2023.1.27 (c (n "coredb-cli") (v "2023.1.27") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "0chvjgpq024lykhz9vw5spajy75fjpnn35a4v3y3y8rpf4b3kr28") (y #t)))

(define-public crate-coredb-cli-0.2.0-rc-3c17358 (c (n "coredb-cli") (v "0.2.0-rc-3c17358") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "1wynh7w506y5jbf5yrjx7ap7nwgwilxyn10b7a984bzpv729adkl")))

(define-public crate-coredb-cli-0.2.0-rc-916b4e3 (c (n "coredb-cli") (v "0.2.0-rc-916b4e3") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "1bn9bs1x27pccq3byb9bmvjakadhxfp54qpcfq0nngxhs7nnyaw9")))

(define-public crate-coredb-cli-0.2.0-alpha (c (n "coredb-cli") (v "0.2.0-alpha") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "12v1q82j9nxw0lmgqlxjcpk33sp1ih5f3m74wa34ap003sly7gjc")))

(define-public crate-coredb-cli-2023.2.15 (c (n "coredb-cli") (v "2023.2.15") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "1zj9sd1cbv312jdq96m7fzfr8dy20svsyzwayslj6vm099jl65im") (y #t)))

(define-public crate-coredb-cli-2023.3.6 (c (n "coredb-cli") (v "2023.3.6") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "1ywqrhir02a480dlzc9s6k9nc8ldkhsy1r6w9xyffnqgs1sgi0l1") (y #t)))

(define-public crate-coredb-cli-2023.3.9 (c (n "coredb-cli") (v "2023.3.9") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "18b1099br5x1anh728vc06mc4bbi5f209gknkxg0aq15qc3rmv73") (y #t)))

(define-public crate-coredb-cli-2023.3.9+1 (c (n "coredb-cli") (v "2023.3.9+1") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "1hgydpkmkibcmh673di6a9fn6xp08d0qxzqcjzb1gq8bxc9a2syd") (y #t)))

(define-public crate-coredb-cli-0.2.0 (c (n "coredb-cli") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "07zcclx76dradnmcnxmnysylxdvd6yc6baf06kg1wm9lp87rgw0n")))

(define-public crate-coredb-cli-0.2.1 (c (n "coredb-cli") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "0habj5c1i0b1k1k3yv8my0ykb8jfjwyb5fh88ldi6qdnn5jws8jc")))

(define-public crate-coredb-cli-0.2.2 (c (n "coredb-cli") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "1sr5rv8fhwgdzbdhpxcpl82vw7yvfyz93vgzzk1b0qnnybcyzkba")))

(define-public crate-coredb-cli-0.2.3 (c (n "coredb-cli") (v "0.2.3") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "0k040cg7b21ygzczvgwa06zi8wncd0sixjqdch3m45262c9820p3")))

