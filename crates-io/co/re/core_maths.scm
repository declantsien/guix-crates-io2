(define-module (crates-io co re core_maths) #:use-module (crates-io))

(define-public crate-core_maths-0.0.0 (c (n "core_maths") (v "0.0.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "15pg2bdwy23188vai61b8ngj7y2lhrmwns7nmcf3jwmnkdbmf7jc")))

(define-public crate-core_maths-0.1.0 (c (n "core_maths") (v "0.1.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "18q9fwy80lk1lccam375skmsslryik00zkhsl850pidqrh2jbc73")))

