(define-module (crates-io co re coreplus) #:use-module (crates-io))

(define-public crate-coreplus-0.1.0 (c (n "coreplus") (v "0.1.0") (h "1ilhnxqhp68i1r7ww0nkadxghq1q7bi2nd38dn9sqz20a9clsl21") (f (quote (("unstable") ("std")))) (y #t)))

(define-public crate-coreplus-0.1.1 (c (n "coreplus") (v "0.1.1") (h "07c67wmvv6cxj8gy08qfjd68mshh6ahyfgqlks77qsx89fiis600") (f (quote (("unstable") ("std")))) (y #t)))

(define-public crate-coreplus-0.1.2 (c (n "coreplus") (v "0.1.2") (h "0fjcq511gsf98apys4cns0v7yk4zy6wahcpk1r8b1g3d82zqh9wq") (f (quote (("unstable") ("std"))))))

(define-public crate-coreplus-0.2.0 (c (n "coreplus") (v "0.2.0") (h "06d9dyziqz0vngl4d9lh9l8n69fgaqj54npdhrmiq42j99fiarwg") (f (quote (("std") ("default" "std"))))))

(define-public crate-coreplus-0.2.1 (c (n "coreplus") (v "0.2.1") (h "1vjszcfv55jzkpq1j2cnjw637g4nqnpdx0phjsf7wxi349983gcd") (f (quote (("std") ("default" "std"))))))

