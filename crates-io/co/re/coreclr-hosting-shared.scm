(define-module (crates-io co re coreclr-hosting-shared) #:use-module (crates-io))

(define-public crate-coreclr-hosting-shared-0.1.0 (c (n "coreclr-hosting-shared") (v "0.1.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "09s946ximmli0h5zpl1aam8b7mdi8ffgsfhy9m2js5idiabfha8l")))

(define-public crate-coreclr-hosting-shared-0.1.1 (c (n "coreclr-hosting-shared") (v "0.1.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "01ai9pbybx0wl7pz355zlzgl7l59c3prb1j1a8siw3rszp91rsvy")))

(define-public crate-coreclr-hosting-shared-0.1.2 (c (n "coreclr-hosting-shared") (v "0.1.2") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1igzc3lan2wg01f7m3xrw83i1n4bjak32hk5z1k3qq1bp0wksd5w")))

(define-public crate-coreclr-hosting-shared-0.1.3 (c (n "coreclr-hosting-shared") (v "0.1.3") (d (list (d (n "cty") (r "^0.2") (d #t) (t "cfg(not(std))") (k 0)))) (h "144l3nbwl7yl933ysrammn5a2mbabxzrq0cdd6igrnnh8midwf16")))

(define-public crate-coreclr-hosting-shared-0.1.4 (c (n "coreclr-hosting-shared") (v "0.1.4") (d (list (d (n "cty") (r "^0.2") (d #t) (t "cfg(not(std))") (k 0)))) (h "0ixqhqkmb6ghymmrk526m9yziz947d43qdzrzj4rbgz6nz56nwgf")))

