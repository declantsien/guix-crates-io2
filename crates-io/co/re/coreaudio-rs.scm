(define-module (crates-io co re coreaudio-rs) #:use-module (crates-io))

(define-public crate-coreaudio-rs-0.0.1 (c (n "coreaudio-rs") (v "0.0.1") (d (list (d (n "coreaudio-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0s7ccva63xjrxfiibhp0g42ppsk3ycpqrnax79y002i825kxywvc")))

(define-public crate-coreaudio-rs-0.0.2 (c (n "coreaudio-rs") (v "0.0.2") (d (list (d (n "coreaudio-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "16hiiw65hyw2yw21kjqyzmj01j6gfr6jns5na9r667yvfgiv109f")))

(define-public crate-coreaudio-rs-0.0.3 (c (n "coreaudio-rs") (v "0.0.3") (d (list (d (n "coreaudio-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1n7sh5q3v9kjyx5qz614plrbvm4dwzgv2q6mk7fhacy1zj2hzfm3")))

(define-public crate-coreaudio-rs-0.0.4 (c (n "coreaudio-rs") (v "0.0.4") (d (list (d (n "coreaudio-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "00d6rw8x2pz8aca31mj4x8xfs1jmy1f45r8hqjx5mcw0nfdw6drn")))

(define-public crate-coreaudio-rs-0.0.5 (c (n "coreaudio-rs") (v "0.0.5") (d (list (d (n "coreaudio-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1b5fxxd08g1l00nkszx69rwadciz818wlnkpaqvpbsvfxhqcamxd")))

(define-public crate-coreaudio-rs-0.0.6 (c (n "coreaudio-rs") (v "0.0.6") (d (list (d (n "coreaudio-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 2)))) (h "19ywjvj3aq6zs1159v5lgnq62zi65dg1p9xzg5nb50v7b85ji85r")))

(define-public crate-coreaudio-rs-0.1.1 (c (n "coreaudio-rs") (v "0.1.1") (d (list (d (n "coreaudio-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 2)))) (h "19cba73mcyr60xfrbr7bvlqz1j8lvfa234snjw8qnrid5x5yj5dc")))

(define-public crate-coreaudio-rs-0.2.0 (c (n "coreaudio-rs") (v "0.2.0") (d (list (d (n "coreaudio-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 2)))) (h "0i2xxcllz6si4z53kawial3799y01spmqpb0ir125ra6zkq58s36")))

(define-public crate-coreaudio-rs-0.2.1 (c (n "coreaudio-rs") (v "0.2.1") (d (list (d (n "coreaudio-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 2)))) (h "1drs31j1k6hszjncvvvnf8y025yn8l7q94fdbp64gzpl1p16bgwm")))

(define-public crate-coreaudio-rs-0.2.2 (c (n "coreaudio-rs") (v "0.2.2") (d (list (d (n "coreaudio-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 2)))) (h "1hmbvy9qzppwqnpxs178pj3frmipac6i4dhk60gg3992hhy5i17n")))

(define-public crate-coreaudio-rs-0.3.0 (c (n "coreaudio-rs") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 2)))) (h "1ny4wfrppwzdf9frny9jv002szk6318qm5q2q6cmr1zis34nlari")))

(define-public crate-coreaudio-rs-0.4.0 (c (n "coreaudio-rs") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 2)))) (h "06if23g790rd7xhs9blgd2c4ci10pxqbk50bv5airjmr26dym30i")))

(define-public crate-coreaudio-rs-0.5.0 (c (n "coreaudio-rs") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (d #t) (k 0)))) (h "1njbj52xr8l96b9iy1g7xxmr6k2wjv766b0gm39w27rqnhfdv5vz")))

(define-public crate-coreaudio-rs-0.6.0 (c (n "coreaudio-rs") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zsgg4l7dpac0mxk27m0f0crd2a1m003qnddk261ys26k6i3yqvs")))

(define-public crate-coreaudio-rs-0.7.0 (c (n "coreaudio-rs") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1skm2sljx5rh2xqx6dnqajqlyyn0nzy1lj2b07wng69p4lkl7v12")))

(define-public crate-coreaudio-rs-0.8.0 (c (n "coreaudio-rs") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.2") (d #t) (k 0)))) (h "109pbc12n2r0cjclg96rimj6gw08aa73p4kxydar773qjzawb8vm")))

(define-public crate-coreaudio-rs-0.8.1 (c (n "coreaudio-rs") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.2") (k 0)))) (h "0a7lhvvm45l258b5nvlirlj91js4zg6ai06i9b72dsx4hxzrvr58") (f (quote (("open_al" "coreaudio-sys/open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi" "coreaudio-sys/core_midi") ("core_audio" "coreaudio-sys/core_audio") ("audio_unit" "coreaudio-sys/audio_unit") ("audio_toolbox" "coreaudio-sys/audio_toolbox"))))))

(define-public crate-coreaudio-rs-0.9.0 (c (n "coreaudio-rs") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.2") (k 0)))) (h "16i0hfwp97f7s2ci9m1hq9a2cp6q84a58clhgazdhdhips75chh5") (f (quote (("open_al" "coreaudio-sys/open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi" "coreaudio-sys/core_midi") ("core_audio" "coreaudio-sys/core_audio") ("audio_unit" "coreaudio-sys/audio_unit") ("audio_toolbox" "coreaudio-sys/audio_toolbox"))))))

(define-public crate-coreaudio-rs-0.9.1 (c (n "coreaudio-rs") (v "0.9.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.2") (k 0)))) (h "14g4yqsbhif2bqdk4qk0lixfy78gl1p8lrl122qyklysclcpcagj") (f (quote (("open_al" "coreaudio-sys/open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi" "coreaudio-sys/core_midi") ("core_audio" "coreaudio-sys/core_audio") ("audio_unit" "coreaudio-sys/audio_unit") ("audio_toolbox" "coreaudio-sys/audio_toolbox"))))))

(define-public crate-coreaudio-rs-0.10.0 (c (n "coreaudio-rs") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.2") (k 0)))) (h "125d4zr3n363ybga4629p41ym7iqjfb2alnwrc1zj7zyxch4p28i") (f (quote (("open_al" "coreaudio-sys/open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi" "coreaudio-sys/core_midi") ("core_audio" "coreaudio-sys/core_audio") ("audio_unit" "coreaudio-sys/audio_unit") ("audio_toolbox" "coreaudio-sys/audio_toolbox"))))))

(define-public crate-coreaudio-rs-0.11.0 (c (n "coreaudio-rs") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.2") (k 0)))) (h "07zksjnsz90m6lkzi1l09jgl36h0q34qlppc3raf4w5cw7a5xz5a") (f (quote (("open_al" "coreaudio-sys/open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi" "coreaudio-sys/core_midi") ("core_audio" "coreaudio-sys/core_audio") ("audio_unit" "coreaudio-sys/audio_unit") ("audio_toolbox" "coreaudio-sys/audio_toolbox"))))))

(define-public crate-coreaudio-rs-0.11.1 (c (n "coreaudio-rs") (v "0.11.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.2") (k 0)))) (h "0h24qz1a99z2w7ya158cbnrw193jbk5wsw1isp17qnak2pj1jdsx") (f (quote (("open_al" "coreaudio-sys/open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi" "coreaudio-sys/core_midi") ("core_audio" "coreaudio-sys/core_audio") ("audio_unit" "coreaudio-sys/audio_unit") ("audio_toolbox" "coreaudio-sys/audio_toolbox"))))))

(define-public crate-coreaudio-rs-0.11.2 (c (n "coreaudio-rs") (v "0.11.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.2") (k 0)))) (h "1zxjf71jyrglmkbn505lywl3q4679gwmv4a8jr0rc6avg78y45yb") (f (quote (("open_al" "coreaudio-sys/open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi" "coreaudio-sys/core_midi") ("core_audio" "coreaudio-sys/core_audio") ("audio_unit" "coreaudio-sys/audio_unit") ("audio_toolbox" "coreaudio-sys/audio_toolbox"))))))

(define-public crate-coreaudio-rs-0.11.3 (c (n "coreaudio-rs") (v "0.11.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8.3") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.2") (k 0)))) (h "1kmssby4rqhv2iq1a8zmaav5p3bl40qs0wah9zv65ikr5lbpf41j") (f (quote (("open_al" "coreaudio-sys/open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi" "coreaudio-sys/core_midi") ("core_audio" "coreaudio-sys/core_audio") ("audio_unit" "coreaudio-sys/audio_unit") ("audio_toolbox" "coreaudio-sys/audio_toolbox"))))))

(define-public crate-coreaudio-rs-0.12.0 (c (n "coreaudio-rs") (v "0.12.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8.3") (d #t) (k 0)) (d (n "coreaudio-sys") (r "^0.2") (k 0)))) (h "1nqa3qz7rks0ss34jky3k8r63hq2wm4vn584jn9k39bwwdpvxd8z") (f (quote (("open_al" "coreaudio-sys/open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi" "coreaudio-sys/core_midi") ("core_audio" "coreaudio-sys/core_audio") ("audio_unit" "coreaudio-sys/audio_unit") ("audio_toolbox" "coreaudio-sys/audio_toolbox"))))))

