(define-module (crates-io co re coreutils_module_core) #:use-module (crates-io))

(define-public crate-coreutils_module_core-0.2.1 (c (n "coreutils_module_core") (v "0.2.1") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "coreutils_json") (r "^0.2.1") (d #t) (k 0)) (d (n "coreutils_jwt") (r "^0.2.1") (d #t) (k 0)) (d (n "coreutils_logger") (r "^0.2.1") (d #t) (k 0)) (d (n "coreutils_module") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ws1wdxgi60i0y0b18db4n210rhsjdikc0qfajkj0sm6g24zwwx6")))

(define-public crate-coreutils_module_core-0.3.0 (c (n "coreutils_module_core") (v "0.3.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "coreutils_json") (r "^0.3.0") (d #t) (k 0)) (d (n "coreutils_jwt") (r "^0.3.0") (d #t) (k 0)) (d (n "coreutils_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "coreutils_module") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0s9x7wmw4h1gmd1mpl2c5fqwln9lfpbqmcfkfff44fpj5fq90lk2")))

(define-public crate-coreutils_module_core-0.4.0 (c (n "coreutils_module_core") (v "0.4.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "coreutils_json") (r "^0.4.0") (d #t) (k 0)) (d (n "coreutils_jwt") (r "^0.4.0") (d #t) (k 0)) (d (n "coreutils_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "coreutils_module") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10mgjc5m41nwhkyzlkfap8hc4fx9m049vwl5l15zdl1898f99274")))

