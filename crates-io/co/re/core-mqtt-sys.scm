(define-module (crates-io co re core-mqtt-sys) #:use-module (crates-io))

(define-public crate-core-mqtt-sys-0.1.0 (c (n "core-mqtt-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "1ig6d9cvpd8qf8fl4xd63frqfaa3lh1zayb2krxq9fznlyg6kirx")))

(define-public crate-core-mqtt-sys-0.1.1 (c (n "core-mqtt-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "1qybbfc6cz95v6jklv7layxl9cj8la90j25d4zayzlpbpxl7sayk")))

