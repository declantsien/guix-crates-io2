(define-module (crates-io co re coreaudio-sys) #:use-module (crates-io))

(define-public crate-coreaudio-sys-0.0.1 (c (n "coreaudio-sys") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1rfl7a50pfxpk8gy4ih92lksxx3vj9pz0xq2ysvplrgaa39zdkjd")))

(define-public crate-coreaudio-sys-0.0.2 (c (n "coreaudio-sys") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "189agplfzr6wg0i46rn1sdrqh24xkh3hs21qbizah6q0ykgvprxq")))

(define-public crate-coreaudio-sys-0.1.0 (c (n "coreaudio-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "12vz8jj9qidnvfr53c2x2c3fr87657ln58zz06fl6imjfk4wkrkc")))

(define-public crate-coreaudio-sys-0.1.1 (c (n "coreaudio-sys") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0vlaalzr46fj3j31jgibqzdf1v936schq82jrq7272h26rwm0ixm")))

(define-public crate-coreaudio-sys-0.1.2 (c (n "coreaudio-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)))) (h "0i6dh6sz1f4xrs3caq7sg20cgw7wsqk6zbqin96d2k1acabih8ri")))

(define-public crate-coreaudio-sys-0.2.0 (c (n "coreaudio-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)))) (h "19paxwdl95cqvzmqp8mi6d2mc35hmp5ry4wgfjbfkipv9scbs51m") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.1 (c (n "coreaudio-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)))) (h "08gfs49bwab2dj6d5czhxq2gww088n4jxj9m4gjkmbqqmq3jy0a7") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.2 (c (n "coreaudio-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)))) (h "0az89mdwavp6jqyrlqgc9hw4ya09q5qsb50vwdhz9cfmb2zvmzbq") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.3 (c (n "coreaudio-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.51") (k 1)))) (h "1kl2p41grd3xrb0p4zayb9a16h52zhlqngj4819vbk67q5a5k3vy") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.4 (c (n "coreaudio-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("runtime"))) (k 1)))) (h "0b2hh7an7q02h6jdyibijkak8as60cxsqxrhl28apzrkbhb1q7z8") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.5 (c (n "coreaudio-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("runtime"))) (k 1)))) (h "1m3i29bh1xvfxh78px2hz0yal678p5nj7jayin91w4w9w3k0wmyn") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.6 (c (n "coreaudio-sys") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("runtime"))) (k 1)))) (h "1mx89ynkf2ds1n43hdd6radg2660gp27dw2l90vkqk4zybq3vxqp") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.7 (c (n "coreaudio-sys") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.56") (f (quote ("runtime"))) (k 1)))) (h "006vl52j21jflc18y1p4qcibkzqswgj5iiqsr664fp83l7gwqkgm") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.8 (c (n "coreaudio-sys") (v "0.2.8") (d (list (d (n "bindgen") (r "^0.56") (f (quote ("runtime"))) (k 1)))) (h "1yiipfq8gni2fkh62kzzprqgnfb77046d392p9mb82bapr3k6zib") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.9 (c (n "coreaudio-sys") (v "0.2.9") (d (list (d (n "bindgen") (r "^0.56") (f (quote ("runtime"))) (k 1)))) (h "12r4icmi931jp6dvaf22499r8fqnq7ldy4n0ckq1b35xknjpjina") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.10 (c (n "coreaudio-sys") (v "0.2.10") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)))) (h "1rjppvvv1j6wbsjw48mrsa5m3z818l5x8f3x0xrp03b3h16l9zrx") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.11 (c (n "coreaudio-sys") (v "0.2.11") (d (list (d (n "bindgen") (r "^0.61") (f (quote ("runtime"))) (k 1)))) (h "1yvqsz7h0hknj0f25074zrmy5hb9diqaj0cyqbngw9409fwl950s") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.12 (c (n "coreaudio-sys") (v "0.2.12") (d (list (d (n "bindgen") (r "^0.64") (f (quote ("runtime"))) (k 1)))) (h "091b4sq3kl8n4dy86l4mxq9vjzsn8w8b51xzfcpxwjkciqjv4d7h") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.13 (c (n "coreaudio-sys") (v "0.2.13") (d (list (d (n "bindgen") (r "^0.68") (f (quote ("runtime"))) (k 1)))) (h "0b9kqjza9dp3vcwaa2jgw75qgymb5q0fm64qdciwwkfiv9dqwiyq") (f (quote (("open_al") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.14 (c (n "coreaudio-sys") (v "0.2.14") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (k 1)))) (h "0a50b29nvilvlqza43ln78yyll3x2ba37n4acf701pm9h2xhw4pk") (f (quote (("open_al") ("io_kit_audio") ("default" "audio_toolbox" "audio_unit" "core_audio" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox"))))))

(define-public crate-coreaudio-sys-0.2.15 (c (n "coreaudio-sys") (v "0.2.15") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (k 1)))) (h "1agmf1idf5m08rgkvsdxqni985acmrs629xzlpqgazq54x85h0bz") (f (quote (("open_al") ("io_kit_audio") ("default" "audio_toolbox" "audio_unit" "core_audio" "audio_server_plugin" "open_al" "core_midi") ("core_midi") ("core_audio") ("audio_unit") ("audio_toolbox") ("audio_server_plugin"))))))

