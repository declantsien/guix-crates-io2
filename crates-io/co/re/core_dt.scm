(define-module (crates-io co re core_dt) #:use-module (crates-io))

(define-public crate-core_dt-0.1.0 (c (n "core_dt") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (f (quote ("std"))) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "06ma1zl12ln75aa5c6192qk4jm0px0akwmq4c56hd141d842q0qh")))

