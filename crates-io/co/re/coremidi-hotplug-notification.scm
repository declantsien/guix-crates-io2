(define-module (crates-io co re coremidi-hotplug-notification) #:use-module (crates-io))

(define-public crate-coremidi-hotplug-notification-0.1.0 (c (n "coremidi-hotplug-notification") (v "0.1.0") (d (list (d (n "core-foundation") (r "^0.9.4") (d #t) (k 0)) (d (n "coremidi") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1lp21apn6gdic4cqq4crzpzjkd00y3ijqv22sv6a0bdp3wrjylbf")))

(define-public crate-coremidi-hotplug-notification-0.1.1 (c (n "coremidi-hotplug-notification") (v "0.1.1") (d (list (d (n "core-foundation") (r "^0.9.4") (d #t) (k 0)) (d (n "coremidi") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0jnsbayj01q5h3abhw8373krbpzwfrm2wwz8af3662f0ls7smwp0")))

(define-public crate-coremidi-hotplug-notification-0.1.3 (c (n "coremidi-hotplug-notification") (v "0.1.3") (d (list (d (n "core-foundation") (r "^0.9.4") (d #t) (k 0)) (d (n "coremidi") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "02zb6j964frnspl0i4xzss59mwfbcmprkylnh1krfw9ldxbgqklq")))

