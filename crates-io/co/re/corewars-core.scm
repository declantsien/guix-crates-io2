(define-module (crates-io co re corewars-core) #:use-module (crates-io))

(define-public crate-corewars-core-0.0.0 (c (n "corewars-core") (v "0.0.0") (h "0hkzj0p9a0h8lk0wvhl585wcy6i4mb2lx9wr4nfsfzpkzf0wcz9p")))

(define-public crate-corewars-core-0.2.0 (c (n "corewars-core") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "08qzw4xbm3g12rr3xf7mxgm3ry7795km6frfcly0skbg8ixw5426")))

