(define-module (crates-io co re core-futures-tls) #:use-module (crates-io))

(define-public crate-core-futures-tls-0.1.0 (c (n "core-futures-tls") (v "0.1.0") (h "08h2bi1486vvdsq5axs6832jgwx88s61ddaqg0xc5k37zsggblli")))

(define-public crate-core-futures-tls-0.1.1 (c (n "core-futures-tls") (v "0.1.1") (h "0fkr3hw3fbphas5bzf9b1lj0l9rx3qik8y98klpq9ljykdpxs1q4")))

