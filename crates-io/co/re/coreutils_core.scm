(define-module (crates-io co re coreutils_core) #:use-module (crates-io))

(define-public crate-coreutils_core-0.1.0 (c (n "coreutils_core") (v "0.1.0") (d (list (d (n "bstr") (r "~0.2.11") (d #t) (k 0)) (d (n "libc") (r "~0.2.67") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "time") (r "~0.2.8") (d #t) (k 0)))) (h "183k0lwsjdmsgis224cf3la6xzsgw4xqx1m3ydsyvzymcwqh36sn")))

(define-public crate-coreutils_core-0.1.1 (c (n "coreutils_core") (v "0.1.1") (d (list (d (n "bstr") (r "~0.2.11") (d #t) (k 0)) (d (n "libc") (r "~0.2.67") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "time") (r "~0.2.8") (d #t) (k 0)))) (h "0fkgwqvnmsjn2lfd6w5bdcjj6h57vkqbagsac86jhcdxa99h1n69")))

