(define-module (crates-io co re coreemu) #:use-module (crates-io))

(define-public crate-coreemu-0.1.0 (c (n "coreemu") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.15") (d #t) (k 0)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.5.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (d #t) (k 1)))) (h "14grpib7x5yz0rgqjd2a69v0b6njll3cj1yrpl6iccgkv9a4mpph")))

(define-public crate-coreemu-0.1.1 (c (n "coreemu") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.15") (d #t) (k 0)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.5.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (d #t) (k 1)))) (h "1mii91csw8kqqr4rw7g7l913zmza7k5imsw3by3lvzffpbqck0wx")))

(define-public crate-coreemu-0.1.2 (c (n "coreemu") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.15") (d #t) (k 0)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.5.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (d #t) (k 1)))) (h "0qryljcgf4138jsgqnz42565zqvd7abdz3j22cn52k87dh6in911")))

(define-public crate-coreemu-0.1.3 (c (n "coreemu") (v "0.1.3") (d (list (d (n "futures-util") (r "^0.3.15") (d #t) (k 0)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.5.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5") (d #t) (k 1)))) (h "0y17i9qzjhhaz7kcs6sghczi6dzgd3jy99il7gm5pr0q09d8xmr4")))

(define-public crate-coreemu-0.1.4 (c (n "coreemu") (v "0.1.4") (d (list (d (n "futures-util") (r "^0.3.15") (d #t) (k 0)) (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (f (quote ("channel" "codegen" "prost" "transport"))) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "138d7ckj0yyyn9m9drd3n21836y2yjg0gjykcfmb5x6c4pv5sldz")))

(define-public crate-coreemu-0.1.5 (c (n "coreemu") (v "0.1.5") (d (list (d (n "futures-util") (r "^0.3.15") (d #t) (k 0)) (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (f (quote ("channel" "codegen" "prost" "transport"))) (k 0)) (d (n "tonic-build") (r "^0.7") (d #t) (k 1)))) (h "1xlywmfkmjahdhkd2rw0gk7s328iy1sagb4xb42nrvgl2ykcal2n")))

