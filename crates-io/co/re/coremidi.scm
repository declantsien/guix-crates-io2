(define-module (crates-io co re coremidi) #:use-module (crates-io))

(define-public crate-coremidi-0.0.1 (c (n "coremidi") (v "0.0.1") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.2") (d #t) (k 0)) (d (n "coremidi-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "11qxga7drlhnm43a91x6h3cwlcazg72rr5wxsaa1y0qfjp1sr2q0")))

(define-public crate-coremidi-0.0.2 (c (n "coremidi") (v "0.0.2") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.2") (d #t) (k 0)) (d (n "coremidi-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1y5sj8kmqlp311rncscdrkw8gvdwb1j76rayx329lazz9ianpgb5")))

(define-public crate-coremidi-0.1.0 (c (n "coremidi") (v "0.1.0") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.2") (d #t) (k 0)) (d (n "coremidi-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1avzy0xh74cpi946i3biibkifa6x5xcq7a9hlnlg7rbw4v3jnsaz")))

(define-public crate-coremidi-0.2.0 (c (n "coremidi") (v "0.2.0") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.2") (d #t) (k 0)) (d (n "coremidi-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0fyz5b1aw1dzadf5plkgbx1zyx5aspi3p8mg689c28gwis5xia1k")))

(define-public crate-coremidi-0.3.0 (c (n "coremidi") (v "0.3.0") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.2") (d #t) (k 0)) (d (n "coremidi-sys") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1vfbg5swa68cxlyrklh8xpbyqdxzcn1rk0a2d4n11w7466z6ncik")))

(define-public crate-coremidi-0.3.1 (c (n "coremidi") (v "0.3.1") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.2") (d #t) (k 0)) (d (n "coremidi-sys") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "05684hzp3clncb203rzw9773iq0vf958wk1xhhmsk8mkg538saxf")))

(define-public crate-coremidi-0.4.0 (c (n "coremidi") (v "0.4.0") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.2") (d #t) (k 0)) (d (n "coremidi-sys") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0wridj9j8nn60rg5b81pwav2q8mb25bdijlibynjn62gagjjvycr")))

(define-public crate-coremidi-0.5.0 (c (n "coremidi") (v "0.5.0") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.2") (d #t) (k 0)) (d (n "coremidi-sys") (r "^2.0") (d #t) (k 0)))) (h "0h6s6scb119hj3hqqn28yx4f3hw98cl7ilz84wja8d0fg4b62ax9")))

(define-public crate-coremidi-0.6.0 (c (n "coremidi") (v "0.6.0") (d (list (d (n "core-foundation") (r "^0.9.1") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8.2") (d #t) (k 0)) (d (n "coremidi-sys") (r "^3.0.1") (d #t) (k 0)))) (h "1x9lz9w360kkyvszyr3lbw3ja0b7vvkbjz5p112j0rwa0754fy0s")))

(define-public crate-coremidi-0.7.0 (c (n "coremidi") (v "0.7.0") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9.3") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8.3") (d #t) (k 0)) (d (n "coremidi-sys") (r "^3.1.0") (d #t) (k 0)))) (h "1xszhv5vl85mqf15ynmz0d2krmxmg8r250ppxh6dk8xvi38zjwg8")))

(define-public crate-coremidi-0.8.0 (c (n "coremidi") (v "0.8.0") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9.3") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8.4") (d #t) (k 0)) (d (n "coremidi-sys") (r "^3.1.0") (d #t) (k 0)))) (h "0c6abi52gza20sccpl6fdph7a3vkrarsm1khg6fd5c581vhv6kln")))

