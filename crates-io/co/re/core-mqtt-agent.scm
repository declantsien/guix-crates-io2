(define-module (crates-io co re core-mqtt-agent) #:use-module (crates-io))

(define-public crate-core-mqtt-agent-0.1.0 (c (n "core-mqtt-agent") (v "0.1.0") (d (list (d (n "core-mqtt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "01hlkf5k6xbcszzwrz2kf5ij23y4aac3hdc0panarsvxvc30zs27")))

(define-public crate-core-mqtt-agent-0.1.1 (c (n "core-mqtt-agent") (v "0.1.1") (d (list (d (n "core-mqtt-sys") (r "^0.1.0") (d #t) (k 0)))) (h "17am4lhcyphrbgshz7gm4j05fpldbzhdd161sliwqhrsvxxalrjk")))

