(define-module (crates-io co re coreutils_logger) #:use-module (crates-io))

(define-public crate-coreutils_logger-0.1.0 (c (n "coreutils_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0m49l35f28h65f23ink8sgaafh9cw6vw3d5290i9cd9wnbyb7gvj")))

(define-public crate-coreutils_logger-0.2.0 (c (n "coreutils_logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1v4hzhw1gl8lq4fx334yf00a596795rgmh83mhw7x4min9zwp54v")))

(define-public crate-coreutils_logger-0.2.1 (c (n "coreutils_logger") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0g8aqzzslgq6ikmlybczb1p4dgah88yzcl01bakm1lk54knmws63")))

(define-public crate-coreutils_logger-0.3.0 (c (n "coreutils_logger") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "045m35fx90k8ywhyimcgia4y0c4n8c6gfcygbjwy3d6ncz2bnvcw")))

(define-public crate-coreutils_logger-0.4.0 (c (n "coreutils_logger") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1akifs1lg02x6wgsmsn9qqq9j7gp6rmfpfg9hbw8x34fahx0rv75")))

(define-public crate-coreutils_logger-0.5.0 (c (n "coreutils_logger") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "err-derive") (r "^0.1") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1ahgmm64iaq763ryh0wvr2xgrfzmz1w7264yxqbvp304r2r3qby9")))

(define-public crate-coreutils_logger-0.5.1 (c (n "coreutils_logger") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "err-derive") (r "^0.1") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0ab5x13ni3gvqis0x7qniqqvp05g4aq38p8dnn33frh5iz7c13wx")))

