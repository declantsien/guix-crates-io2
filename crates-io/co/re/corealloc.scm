(define-module (crates-io co re corealloc) #:use-module (crates-io))

(define-public crate-corealloc-0.1.0 (c (n "corealloc") (v "0.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)))) (h "0vrgv8csznf49yv8qii970kfg7vfhm7pr63bwk9v6n1l5l451h6z")))

(define-public crate-corealloc-0.2.0 (c (n "corealloc") (v "0.2.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)))) (h "0mc71isp4q76hb93s4azs578pjwqws2f0kwh874lq4dab72lsvqv")))

(define-public crate-corealloc-0.3.0 (c (n "corealloc") (v "0.3.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qh48r11kl4xdnkyqbk3y15nb5mgin6kzh32blqm6hdr83fm5ba2")))

(define-public crate-corealloc-0.4.0 (c (n "corealloc") (v "0.4.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "hwloc") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1g2db0d3cp6xpp39fc7s9d6dx0z4nkml87j1g8hnckgwa805v4if")))

(define-public crate-corealloc-0.5.0 (c (n "corealloc") (v "0.5.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "hwloc2") (r "^2.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x23c8qwjk64i9p2f40rin4gw903nqlbyapic25qrf8q0fpaggbx")))

(define-public crate-corealloc-0.6.0 (c (n "corealloc") (v "0.6.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "hwloc2") (r "^2.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kpxccdlbh66cd3q2s8dlci40bbpm096d23vkaj0dvq462s08cz0")))

