(define-module (crates-io co re coremotion-rs) #:use-module (crates-io))

(define-public crate-coremotion-rs-0.0.1 (c (n "coremotion-rs") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "block") (r "^0.1.6") (d #t) (t "cfg(target_os = \"ios\")") (k 0)) (d (n "clang-sys") (r "^1.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"ios\")") (k 0)) (d (n "oslog") (r "^0.2.0") (d #t) (k 2)))) (h "13mynifl33q6r69qkaz89kfslqwb2c08ia0a95il3gfxg3rcnh7s")))

(define-public crate-coremotion-rs-0.0.2 (c (n "coremotion-rs") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "block") (r "^0.1.6") (d #t) (t "cfg(target_os = \"ios\")") (k 0)) (d (n "clang-sys") (r "^1.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"ios\")") (k 0)) (d (n "oslog") (r "^0.2.0") (d #t) (k 2)))) (h "0k0xrj1fsqpkykmc7hrda1yvf2664x2ms1a6f0m36zidvxay1yv0")))

