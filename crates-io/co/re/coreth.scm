(define-module (crates-io co re coreth) #:use-module (crates-io))

(define-public crate-coreth-0.0.0 (c (n "coreth") (v "0.0.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "10qrs7n6rpna94zi8r68jq44098wfcy8c1vyfr7l8grv27ww53hd") (r "1.62")))

