(define-module (crates-io co re corewlan-sys) #:use-module (crates-io))

(define-public crate-corewlan-sys-0.1.0 (c (n "corewlan-sys") (v "0.1.0") (d (list (d (n "objc") (r "^0.2.7") (d #t) (k 0)))) (h "0k0xmik283fppflpcnr6zvv30w3b7np99afh0hsk32pj2ajfcb62") (l "CoreWLAN")))

(define-public crate-corewlan-sys-0.1.1 (c (n "corewlan-sys") (v "0.1.1") (d (list (d (n "objc") (r "^0.2.7") (d #t) (k 0)))) (h "1rmxbr8c91nr3jghzpy18f8zp1bpxp4blqgf9xplxkn2n23a07wh") (l "CoreWLAN")))

