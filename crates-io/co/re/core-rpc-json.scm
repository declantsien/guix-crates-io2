(define-module (crates-io co re core-rpc-json) #:use-module (crates-io))

(define-public crate-core-rpc-json-0.14.0 (c (n "core-rpc-json") (v "0.14.0") (d (list (d (n "bitcoin") (r "^0.27") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ynl0ahnygving8c2j151pfy69zigq7mf5y7lm1csbpad6cjlfs4") (y #t)))

(define-public crate-core-rpc-json-0.15.0 (c (n "core-rpc-json") (v "0.15.0") (d (list (d (n "bitcoin") (r "^0.27") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0z8nxm8w3nab22bmqmj6mgb144ri9l0h7qy9b39gw56ikkis95mf") (y #t)))

(define-public crate-core-rpc-json-0.17.0 (c (n "core-rpc-json") (v "0.17.0") (d (list (d (n "bitcoin") (r "^0.30.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0h9svvdwsd2jrpx5lqzixlafrkzzvyi8q78vfdj1rww3kbnrh62q")))

