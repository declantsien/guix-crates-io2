(define-module (crates-io co re core-isa-parser) #:use-module (crates-io))

(define-public crate-core-isa-parser-0.1.0 (c (n "core-isa-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "00ljba2gp4j6dxajw1nyl7qz9znngfkmvmmccm9qivkjwpmaykp5")))

(define-public crate-core-isa-parser-0.2.0 (c (n "core-isa-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.0") (d #t) (k 0)))) (h "10q0k84883r8fl7m0igx7ff7vyn7lpjw4d939gjp4n3k9gjriv13")))

