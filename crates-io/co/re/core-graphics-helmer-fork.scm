(define-module (crates-io co re core-graphics-helmer-fork) #:use-module (crates-io))

(define-public crate-core-graphics-helmer-fork-0.24.0 (c (n "core-graphics-helmer-fork") (v "0.24.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9.4") (k 0)) (d (n "core-graphics-types") (r "^0.1.3") (k 0)) (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mcwisxgzs287vbj76qhm0vr616dgv79k403lqvx9xp998sprsrj") (f (quote (("link" "core-foundation/link" "core-graphics-types/link") ("highsierra") ("elcapitan") ("default" "link"))))))

