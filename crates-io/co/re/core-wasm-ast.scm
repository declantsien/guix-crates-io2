(define-module (crates-io co re core-wasm-ast) #:use-module (crates-io))

(define-public crate-core-wasm-ast-0.1.0 (c (n "core-wasm-ast") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1dm98wdrxd3vwcmp2m298fpskl4w2i7qnkf7npd1hsrbghmbfd36")))

(define-public crate-core-wasm-ast-0.1.7 (c (n "core-wasm-ast") (v "0.1.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0hxicq4m0r5337v0ydzj3agjglhyk4cmcp3fj7lqlrvy2pzqw8hw")))

(define-public crate-core-wasm-ast-0.1.8 (c (n "core-wasm-ast") (v "0.1.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "06dv65makibskllj0ddvniv3acbz652isqrrc8nvaf8gdcb73j07")))

(define-public crate-core-wasm-ast-0.1.9 (c (n "core-wasm-ast") (v "0.1.9") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "180mkg9xhkmh68zc2pa4wybzzh077sf2yiv3ra7ma2wjhwzwpmiq")))

(define-public crate-core-wasm-ast-0.1.10 (c (n "core-wasm-ast") (v "0.1.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.10") (d #t) (k 0)))) (h "14zbk8srwm4lwnidd16g8v0jz1isk6dyxl3mq1yprz38jjfvbqbd")))

(define-public crate-core-wasm-ast-0.1.11 (c (n "core-wasm-ast") (v "0.1.11") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.11") (d #t) (k 0)))) (h "0lk6vaxrj2600pm242jya52g3ca38kzsn6qa6gpvcwfpf5ncxk92")))

(define-public crate-core-wasm-ast-0.1.12 (c (n "core-wasm-ast") (v "0.1.12") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.12") (d #t) (k 0)))) (h "1klh20fi34sw5vkz1gxj4dmc941s2yipydkf5155rwk64y78lzsj")))

(define-public crate-core-wasm-ast-0.1.13 (c (n "core-wasm-ast") (v "0.1.13") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.13") (d #t) (k 0)))) (h "0nk1bm25s08qzclp6mwhvimh23mwsgqgrp4k8pbjkb51p58dd07k")))

(define-public crate-core-wasm-ast-0.1.14 (c (n "core-wasm-ast") (v "0.1.14") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.14") (d #t) (k 0)))) (h "0a7fb2890k2z4qqv4afg2mqvhfapqna3iplqjg1sl0lrc7bisihi")))

(define-public crate-core-wasm-ast-0.1.15 (c (n "core-wasm-ast") (v "0.1.15") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.15") (d #t) (k 0)))) (h "1drcmf0aw1zd8rkrlvd27y49zf96csn8ynm28qibm66nn2z91gfh")))

(define-public crate-core-wasm-ast-0.1.16 (c (n "core-wasm-ast") (v "0.1.16") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.16") (d #t) (k 0)))) (h "1cg7slq56a7ifff919w10w30if51pvnsn5qxvi1m387r03yzifal")))

(define-public crate-core-wasm-ast-0.1.17 (c (n "core-wasm-ast") (v "0.1.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.17") (d #t) (k 0)))) (h "0qibbjbhqhs484n666i4adharnayp0y1h4rs9ayiy88r7j6l8g2y")))

(define-public crate-core-wasm-ast-0.1.18 (c (n "core-wasm-ast") (v "0.1.18") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.18") (d #t) (k 0)))) (h "0pm1ccpg6m68vlj2vin8sli4vidagfijb6kpxbf5rq7vqfxk5y5r")))

(define-public crate-core-wasm-ast-0.1.19 (c (n "core-wasm-ast") (v "0.1.19") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.19") (d #t) (k 0)))) (h "0mzxfnzwynhg34rnl91bsqflybyc3fzz4xdcbp2j8k12l563qfmm")))

(define-public crate-core-wasm-ast-0.1.20 (c (n "core-wasm-ast") (v "0.1.20") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.20") (d #t) (k 0)))) (h "0bwhx9b0vqkfnwd74awil3l0lplhgdxz63nvxnj5m1x9xjl1cx61")))

(define-public crate-core-wasm-ast-0.1.21 (c (n "core-wasm-ast") (v "0.1.21") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.21") (d #t) (k 0)))) (h "1p981kmv4wz1rbgjpcsinc99my431piimsjhv72qk7k9mljnpqwr")))

(define-public crate-core-wasm-ast-0.1.22 (c (n "core-wasm-ast") (v "0.1.22") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.22") (d #t) (k 0)))) (h "1118jr1avd271ig6hk0sh75i09cc37q8vjbhlnpzj8j8krsb1kg8")))

