(define-module (crates-io co re coremedia-sys) #:use-module (crates-io))

(define-public crate-coremedia-sys-0.1.0 (c (n "coremedia-sys") (v "0.1.0") (d (list (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07xs59gww38kv119gaqy1kpib1p7d44s4k37xryg0d0vpcfdi6g2") (y #t)))

(define-public crate-coremedia-sys-0.0.1 (c (n "coremedia-sys") (v "0.0.1") (d (list (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a6209cr3mhcs9ajaakspp2dpl3asb8fpryb71h85d94rj5b38fz") (y #t)))

(define-public crate-coremedia-sys-0.0.2 (c (n "coremedia-sys") (v "0.0.2") (d (list (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15zkwamv19bxfj2yk0hvjzwfsvmrp81a5h6fl72cwf461blh5gzx")))

