(define-module (crates-io co re corewa_rs) #:use-module (crates-io))

(define-public crate-corewa_rs-0.0.1 (c (n "corewa_rs") (v "0.0.1") (h "1wd3yibkrxar6y1arxk6w7l66ra699ins52xx10pgcyf7sjhp3za")))

(define-public crate-corewa_rs-0.0.2 (c (n "corewa_rs") (v "0.0.2") (h "146hbfvfk76555f5xj0fqq9mcfgl6xjajrwpv6chs68f6q1wx4a2")))

