(define-module (crates-io co re coreutils_module) #:use-module (crates-io))

(define-public crate-coreutils_module-0.1.0 (c (n "coreutils_module") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1y5rqayq6sb69gssjr8dxgq0jblhz2b2zdd8r5jx39s8jws49n4a")))

(define-public crate-coreutils_module-0.2.0 (c (n "coreutils_module") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16sxyld1mx5pidsh77178j5qj26g411il9996qwbnl5rhmg6m8gz")))

(define-public crate-coreutils_module-0.2.1 (c (n "coreutils_module") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1q8x9211zfmc3ykb61pbhl6hhhj54yhhw8g2ir5whgn7fywyz3qq")))

(define-public crate-coreutils_module-0.3.0 (c (n "coreutils_module") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0j6xz67cskidrbqkcb9nh8dli3kyrzmksmcd6mcfy8h9byfhf5j1")))

(define-public crate-coreutils_module-0.4.0 (c (n "coreutils_module") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "119m0pw9y9gv498xd4igf0r00ycps3vngclkn0yadi5xqmxki141")))

(define-public crate-coreutils_module-0.5.0 (c (n "coreutils_module") (v "0.5.0") (d (list (d (n "err-derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "12vq5brjqndphas7dpfw3dlvq3zzbfj2qxhb4y9br785aab4py3m")))

(define-public crate-coreutils_module-0.5.1 (c (n "coreutils_module") (v "0.5.1") (d (list (d (n "err-derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0s31brld8m504hic1hk4y6lici7ljnlhn7j199b7yv342crsfjgb")))

