(define-module (crates-io co re core-media-sys) #:use-module (crates-io))

(define-public crate-core-media-sys-0.1.0 (c (n "core-media-sys") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s6kiwyamabwlnx0r91jbxg3kwkgb2frknlcgghjvpm729yr1klv") (f (quote (("default"))))))

(define-public crate-core-media-sys-0.1.1 (c (n "core-media-sys") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pjq4sm0yrv0k8lcsswr7jarqdd4y09j5nvis0ayrzwkg4xbm72k") (f (quote (("default"))))))

(define-public crate-core-media-sys-0.1.2 (c (n "core-media-sys") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1w9ikymxcrq5w0www2qa2ckna2slq644ga36fxmd07zmbgyg6fr7") (f (quote (("default"))))))

