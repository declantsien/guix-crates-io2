(define-module (crates-io co re core-nightly) #:use-module (crates-io))

(define-public crate-core-nightly-0.0.0-20141227 (c (n "core-nightly") (v "0.0.0-20141227") (h "1h7c114ixjr2s62xyys75gsa85gnmbn0wip7mgqw9lqc207kb6pl")))

(define-public crate-core-nightly-2014.12.27 (c (n "core-nightly") (v "2014.12.27") (h "07pwmhfs5dbdv6drnzxrvdgjsha2paw1jhgrbgqvdnr1ldk1l3ap")))

(define-public crate-core-nightly-2014.12.29 (c (n "core-nightly") (v "2014.12.29") (h "1gcp3p5jjfgx4lj6a3xc2kb9k619n4rrxjdf08brnl6mn2q5y1b9")))

(define-public crate-core-nightly-2015.1.2 (c (n "core-nightly") (v "2015.1.2") (h "0swqcjck6g9gvswm2lhmd9kmnp1q1miyl4ys8pf5z0kb93vg6vpd")))

(define-public crate-core-nightly-2015.1.5 (c (n "core-nightly") (v "2015.1.5") (h "0yrgyvl4p3nmlabfxnk39g33gdfks3g9fn4p2bpzizrcd8v7ybx9")))

(define-public crate-core-nightly-2015.1.7 (c (n "core-nightly") (v "2015.1.7") (h "1fvhw63lh51ajllindiig1sqlyiry87szq3wmja7cad17nzpws2k")))

