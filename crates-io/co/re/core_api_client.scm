(define-module (crates-io co re core_api_client) #:use-module (crates-io))

(define-public crate-core_api_client-0.1.0 (c (n "core_api_client") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.11") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0iw9ld51d49r80d26i8w0a271l53nj8bc2bv8bd7i7kfcj4igysh")))

