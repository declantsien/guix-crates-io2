(define-module (crates-io co re corex) #:use-module (crates-io))

(define-public crate-corex-0.1.0 (c (n "corex") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.7") (d #t) (k 0)) (d (n "modelx") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0v6wmgjfc7hp2fdsvv5v0x1jfvz9z7ridcmi4s5lwxnzd6asip39")))

