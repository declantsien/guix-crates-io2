(define-module (crates-io co re core_io) #:use-module (crates-io))

(define-public crate-core_io-0.0.20160707 (c (n "core_io") (v "0.0.20160707") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1lxaac8wsc9ldqs5zxw16p0lc40p332p33bv60yghwh95hkm98lm")))

(define-public crate-core_io-0.0.20160708 (c (n "core_io") (v "0.0.20160708") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1bkb6dmjznf52radhw8fkb4mh9pk9dvc5nqj0w0c3al9n1yr0ysa")))

(define-public crate-core_io-0.1.20160710 (c (n "core_io") (v "0.1.20160710") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0kl1k6mraqh4gjdakgqpz8x3v4nr21qb7rv15dr95jwngh91452l") (f (quote (("collections" "alloc") ("alloc"))))))

(define-public crate-core_io-0.1.20161011 (c (n "core_io") (v "0.1.20161011") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0z5fq5wp1njmxc70c018083llrqaha2a7vm1jlyd9f25zg9zqlya") (f (quote (("collections" "alloc") ("alloc"))))))

(define-public crate-core_io-0.1.20161028 (c (n "core_io") (v "0.1.20161028") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "13qnscv9bnkah2nfa4bszrb2qn3bmk5r77xsrj9q9zz899a3vwjd") (f (quote (("collections" "alloc") ("alloc"))))))

(define-public crate-core_io-0.1.20170409 (c (n "core_io") (v "0.1.20170409") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0fzlk485vs1njjnikg50dy5jarm53i6pgqzqrkrw4a3lx5b7vzy7") (f (quote (("collections" "alloc") ("alloc")))) (y #t)))

(define-public crate-core_io-0.1.20170408 (c (n "core_io") (v "0.1.20170408") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1p6xr9s94j0fai3a07rls3jspfyg2wzlzdlfrjaq5v085y68mmim") (f (quote (("collections" "alloc") ("alloc"))))))

(define-public crate-core_io-0.1.20170615 (c (n "core_io") (v "0.1.20170615") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0rmcp9w8yq3nvcvcvzjpd9hbhg1kggazc2qmpgmjdr301rp8l4bl") (f (quote (("collections" "alloc") ("alloc"))))))

(define-public crate-core_io-0.1.20180307 (c (n "core_io") (v "0.1.20180307") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1k4pr73ig0ds8nvmyfwxwxnnawc99py1yir8mk99nljpshxgrhjj") (f (quote (("collections" "alloc") ("alloc"))))))

(define-public crate-core_io-0.1.20190427 (c (n "core_io") (v "0.1.20190427") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0yg2lqnaz0jcpf9kfx850nrqfx229acnhbnlbdy321hkv8s2ffhi") (f (quote (("collections" "alloc") ("alloc")))) (y #t)))

(define-public crate-core_io-0.1.20190701 (c (n "core_io") (v "0.1.20190701") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "12hps4nhgsaxfl33wm7biv394ii68bl5d4ihjn5ylcy24nr4afxv") (f (quote (("collections" "alloc") ("alloc"))))))

(define-public crate-core_io-0.1.20210325 (c (n "core_io") (v "0.1.20210325") (d (list (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1pq7hng8llgy8z76wn0y01pnyfimklwkld2dxfgwg318chh97y4p") (f (quote (("collections" "alloc") ("alloc"))))))

