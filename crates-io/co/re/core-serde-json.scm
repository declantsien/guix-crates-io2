(define-module (crates-io co re core-serde-json) #:use-module (crates-io))

(define-public crate-core-serde-json-0.1.0 (c (n "core-serde-json") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)))) (h "130q6ra0i8ag6p4q3npqshh30aqjz4ggnf38zfy7x58ij889dkch") (f (quote (("std" "serde/std"))))))

