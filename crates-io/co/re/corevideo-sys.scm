(define-module (crates-io co re corevideo-sys) #:use-module (crates-io))

(define-public crate-corevideo-sys-0.0.1 (c (n "corevideo-sys") (v "0.0.1") (d (list (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "coremedia-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dpv9ffvir2skahql8vyb3m0v42q5dqn2a1j3s1w0f4g9vdzr813") (y #t)))

