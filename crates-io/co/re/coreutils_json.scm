(define-module (crates-io co re coreutils_json) #:use-module (crates-io))

(define-public crate-coreutils_json-0.1.0 (c (n "coreutils_json") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wwsfsbmirbhkf4hk1hgxqyv0a0087d5gfs0dn1acyx08k5n99ni")))

(define-public crate-coreutils_json-0.2.0 (c (n "coreutils_json") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03japciyzjb6fkzxdmqvr1qh5f67kdlm8f2xfisqz19iznrgc75s")))

(define-public crate-coreutils_json-0.2.1 (c (n "coreutils_json") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s68g39cnb55ni58906xng9wifpxfjs38c37bg3f400j3cch6szb")))

(define-public crate-coreutils_json-0.3.0 (c (n "coreutils_json") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02si30qmaviy2sz26r09wlfjrwhmfwmbj64v6yz1m1mr3jjqmvdr")))

(define-public crate-coreutils_json-0.4.0 (c (n "coreutils_json") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gir024ipm4k3lmw68yq7hd84d933hfz2bb8q5ypd7ay2k5kkrah")))

(define-public crate-coreutils_json-0.5.0 (c (n "coreutils_json") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lk956bjpx9857pydhvs49lqsjhynqsv8vvqk2qxcbbrxillmfqy")))

(define-public crate-coreutils_json-0.5.1 (c (n "coreutils_json") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wy04fv6rjc5fmjf84i825lkm271xbzbda1j3n3kr2r5bvfzwjlk")))

