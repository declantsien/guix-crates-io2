(define-module (crates-io co re coredump-to-stack) #:use-module (crates-io))

(define-public crate-coredump-to-stack-0.1.16 (c (n "coredump-to-stack") (v "0.1.16") (d (list (d (n "core-wasm-ast") (r "^0.1.16") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.16") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.16") (d #t) (k 0)))) (h "0ki70i1hnxc5y3nk353fvcaqs2rxmcvr9c4pj1ljjv7fg94ndcpw")))

(define-public crate-coredump-to-stack-0.1.17 (c (n "coredump-to-stack") (v "0.1.17") (d (list (d (n "core-wasm-ast") (r "^0.1.17") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.17") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.17") (d #t) (k 0)))) (h "0y2hdp0iimv1apz50yadb9q9kn6vlacmx7znrgpc24glhcz8zw2y")))

(define-public crate-coredump-to-stack-0.1.18 (c (n "coredump-to-stack") (v "0.1.18") (d (list (d (n "core-wasm-ast") (r "^0.1.18") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.18") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.18") (d #t) (k 0)))) (h "03i4ffm1ma65bnyymcm8c12xywb3jz3jrn80mjdprhzlw62xcbkx")))

(define-public crate-coredump-to-stack-0.1.19 (c (n "coredump-to-stack") (v "0.1.19") (d (list (d (n "core-wasm-ast") (r "^0.1.19") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.19") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.19") (d #t) (k 0)))) (h "03i5f9dywkwliqrnayg79fl40p8y7qa91vdpzvigwk7q9cpwfjm5")))

(define-public crate-coredump-to-stack-0.1.20 (c (n "coredump-to-stack") (v "0.1.20") (d (list (d (n "core-wasm-ast") (r "^0.1.20") (d #t) (k 0)) (d (n "object") (r "^0.29.0") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.23") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.20") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.20") (d #t) (k 0)) (d (n "wasmgdb_ddbug_parser") (r "^0.3.2") (d #t) (k 0)))) (h "0wyah3apsldqsbika2y1cliwvbrbls8g3g2fc2rf0k9am9sjn2yl")))

(define-public crate-coredump-to-stack-0.1.21 (c (n "coredump-to-stack") (v "0.1.21") (d (list (d (n "core-wasm-ast") (r "^0.1.21") (d #t) (k 0)) (d (n "object") (r "^0.29.0") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.23") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.21") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.21") (d #t) (k 0)) (d (n "wasmgdb_ddbug_parser") (r "^0.3.2") (d #t) (k 0)))) (h "04c74ixfz1wxdqib04p6ngb8ik8qacyksfr8w5n5511b9az9v1a7")))

(define-public crate-coredump-to-stack-0.1.22 (c (n "coredump-to-stack") (v "0.1.22") (d (list (d (n "core-wasm-ast") (r "^0.1.22") (d #t) (k 0)) (d (n "object") (r "^0.29.0") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.23") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.22") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.22") (d #t) (k 0)) (d (n "wasmgdb_ddbug_parser") (r "^0.3.2") (d #t) (k 0)))) (h "1gyvcdq8pj4rn9fci183jkzbynbcx1s7phgp1yh3h178i9xh95k2")))

