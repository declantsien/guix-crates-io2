(define-module (crates-io co re core-graphics-types) #:use-module (crates-io))

(define-public crate-core-graphics-types-0.1.0 (c (n "core-graphics-types") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g79fr9z470s6rgng1jkxyjqbbhfi2ivlzbhjs11g94kj18msbz9")))

(define-public crate-core-graphics-types-0.1.1 (c (n "core-graphics-types") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12vqf0n5mjjcqjksdd82n2zh8hfda2zpiiqsr522c2266j5vcs1s")))

(define-public crate-core-graphics-types-0.1.2 (c (n "core-graphics-types") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0csf9wfxcl6wz2j8fbsmsgx3v2hw86hh74gjiwfnr61223a45c9b")))

(define-public crate-core-graphics-types-0.1.3 (c (n "core-graphics-types") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.9.4") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bxg8nxc8fk4kxnqyanhf36wq0zrjr552c58qy6733zn2ihhwfa5") (f (quote (("link" "core-foundation/link") ("default" "link"))))))

