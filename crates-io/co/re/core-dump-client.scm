(define-module (crates-io co re core-dump-client) #:use-module (crates-io))

(define-public crate-core-dump-client-0.1.0 (c (n "core-dump-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "12jljw55kg3sridayi6azgr9asz6nwsh4i78ciyh0kd5aywf38xb")))

(define-public crate-core-dump-client-0.2.0 (c (n "core-dump-client") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0z3dzy541kaxfjzykcszzw6z1i1qp2n2gbalwx11yi4amjr76i60")))

(define-public crate-core-dump-client-1.0.0 (c (n "core-dump-client") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0p9a0sysi7v7sxwlpf34wxw72z057zzpmjr0yg75k4mxzwpjz05q")))

