(define-module (crates-io co re corewars-parser) #:use-module (crates-io))

(define-public crate-corewars-parser-0.0.0 (c (n "corewars-parser") (v "0.0.0") (h "0jqj5vcn3r9qjd44m39kzqxxkw0n8iw52p3hraz9si38jsvjg4ai")))

(define-public crate-corewars-parser-0.2.0 (c (n "corewars-parser") (v "0.2.0") (d (list (d (n "corewars-core") (r "=0.2.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "normalize-line-endings") (r "^0.3.0") (d #t) (k 2)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 2)) (d (n "textwrap-macros") (r "^0.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0130ihwagpvbl46sjjza0h79rfm5ss985gq74gv2raclbl7vfyr1")))

