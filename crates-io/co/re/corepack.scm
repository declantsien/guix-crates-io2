(define-module (crates-io co re corepack) #:use-module (crates-io))

(define-public crate-corepack-0.1.1 (c (n "corepack") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "serde") (r "^0.8") (f (quote ("collections"))) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)))) (h "046mbi2b6fhjxv7mjmaa2sjph8516x51ca0zxgdp0dwkmbndhjj3") (f (quote (("std" "serde/std") ("default"))))))

(define-public crate-corepack-0.2.0 (c (n "corepack") (v "0.2.0") (d (list (d (n "byteorder") (r "~1.0") (k 0)) (d (n "serde") (r "~1.0.5") (k 0)) (d (n "serde_derive") (r "~1.0.3") (d #t) (k 2)))) (h "1xj27bvayya86lvwjhl6j0c1aybf75g66bybm35ra6921h58g4zm") (f (quote (("std" "serde/std") ("default" "std") ("collections" "serde/collections"))))))

(define-public crate-corepack-0.2.1 (c (n "corepack") (v "0.2.1") (d (list (d (n "byteorder") (r "~1.0") (k 0)) (d (n "serde") (r "~1.0.7") (k 0)) (d (n "serde_derive") (r "~1.0.7") (d #t) (k 2)))) (h "14f4isl46xx0vh26z8lkl3s8k0p4j8pxqyqhr2vx094yjkvly38a") (f (quote (("std" "serde/std") ("default" "std") ("collections" "serde/collections"))))))

(define-public crate-corepack-0.3.0 (c (n "corepack") (v "0.3.0") (d (list (d (n "byteorder") (r "~1.0") (k 0)) (d (n "serde") (r "~1.0.10") (k 0)) (d (n "serde_derive") (r "~1.0.10") (d #t) (k 2)))) (h "1x88qvba438irm5mddmgz4zmn3ld3z248s4nfx7kwxgx2klm6wr9") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-corepack-0.3.1 (c (n "corepack") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "serde") (r "~1.0.10") (k 0)) (d (n "serde_derive") (r "~1.0.10") (d #t) (k 2)))) (h "1xl0rmqydrawlbvm22r2fkn7lsdiq9xv555kj5brcal4yzi4rs9i") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-corepack-0.4.0 (c (n "corepack") (v "0.4.0") (d (list (d (n "byteorder") (r "~1.2.3") (k 0)) (d (n "serde") (r "~1.0.10") (k 0)) (d (n "serde_derive") (r "~1.0.10") (d #t) (k 2)))) (h "19bmcwhd83h3k873jshgpv7d6h0crp6p0v53s6a0qzc1slgyc3va") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-corepack-0.4.1 (c (n "corepack") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0qkxb9qn5cy5xd7klmqiy70ywdai0aqjsqgpcyw58kficww63sb6") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

