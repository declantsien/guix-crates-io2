(define-module (crates-io co re core_extensions_proc_macros) #:use-module (crates-io))

(define-public crate-core_extensions_proc_macros-1.0.0 (c (n "core_extensions_proc_macros") (v "1.0.0") (h "0zd57nxsapyjh6y3drz06fag3fsyqsrxdzyprxc2r442cslacsna")))

(define-public crate-core_extensions_proc_macros-1.1.0 (c (n "core_extensions_proc_macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 2)))) (h "0dvlgsksa5lac89c5519kvfsp2qgsh4jic8c8xlpgh03yci08d0l")))

(define-public crate-core_extensions_proc_macros-1.1.1 (c (n "core_extensions_proc_macros") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 2)))) (h "1k9q3ycjkzd4xwdi7j6hiq5bpzvgjk1gxbqm7ki1bmz4sv1a0g5v")))

(define-public crate-core_extensions_proc_macros-1.2.0 (c (n "core_extensions_proc_macros") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 2)))) (h "16rnxqjb54vmij6sgn1plq23axbb4wj6mgqhm9r2da7j697vl8mz") (f (quote (("macro_utils") ("item_parsing"))))))

(define-public crate-core_extensions_proc_macros-1.3.0 (c (n "core_extensions_proc_macros") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 2)))) (h "0mq58pcyfmhj02f0nqjpsrxhxfxs0divzjy1qzddl5gn2q575ac0") (f (quote (("macro_utils") ("item_parsing"))))))

(define-public crate-core_extensions_proc_macros-1.4.0 (c (n "core_extensions_proc_macros") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 2)))) (h "1jp8dkv93p9hmhkqci50g67casbxda1zw05pdc9vaxpiy49daiqc") (f (quote (("rust_1_45") ("macro_utils") ("item_parsing" "macro_utils"))))))

(define-public crate-core_extensions_proc_macros-1.4.1 (c (n "core_extensions_proc_macros") (v "1.4.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 2)))) (h "0583f16afz0blfh7q1v94d5dfpy8hin3qcwpb3y6qpvn0d9p8r21") (f (quote (("rust_1_45") ("macro_utils") ("item_parsing" "macro_utils"))))))

(define-public crate-core_extensions_proc_macros-1.5.0 (c (n "core_extensions_proc_macros") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 2)) (d (n "quote") (r "^1.0.9") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1ssp5ll2i4njb57kdjvd6p8yn9r94slzc7fsgbkzdwq7x1y8kj60") (f (quote (("rust_1_45") ("macro_utils") ("item_parsing" "macro_utils") ("derive" "syn" "quote" "proc-macro2"))))))

(define-public crate-core_extensions_proc_macros-1.5.1 (c (n "core_extensions_proc_macros") (v "1.5.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 2)) (d (n "quote") (r "^1.0.9") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1dmqnp3x6yrh880vlbaznqchajk5i8jhxwdr4i0hawzm2cashj63") (f (quote (("rust_1_45") ("macro_utils") ("item_parsing" "macro_utils") ("derive" "syn" "quote" "proc-macro2"))))))

(define-public crate-core_extensions_proc_macros-1.5.3 (c (n "core_extensions_proc_macros") (v "1.5.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 2)) (d (n "quote") (r "^1.0.9") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "19k11haw8s00zxxignjmw0ian0q85r9grhbvr153nvlbs8cv5wv9") (f (quote (("rust_1_45") ("macro_utils") ("item_parsing" "macro_utils") ("derive" "syn" "quote" "proc-macro2"))))))

