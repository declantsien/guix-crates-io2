(define-module (crates-io co re core-midi) #:use-module (crates-io))

(define-public crate-core-midi-0.1.0 (c (n "core-midi") (v "0.1.0") (d (list (d (n "core-foundation") (r "^0.2.2") (d #t) (k 0)) (d (n "core-midi-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1bzw8qx3rc8mb6hc0b4jvw9cgvdsn9iik3jcw970pj3x7pag98fs")))

(define-public crate-core-midi-0.1.1 (c (n "core-midi") (v "0.1.1") (d (list (d (n "core-foundation") (r "^0.2.2") (d #t) (k 0)) (d (n "core-midi-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "07a96bnyldk5gh6b1pqanv5kiwmzayzflp0wfc47xr4d9s97ddqp")))

