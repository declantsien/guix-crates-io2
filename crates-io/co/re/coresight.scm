(define-module (crates-io co re coresight) #:use-module (crates-io))

(define-public crate-coresight-0.1.0 (c (n "coresight") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "jep106") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0hcpjpkyn449l2xfd57wfrqmsg9a5d0qgjabiy5spax8zlvkxvf2") (y #t)))

