(define-module (crates-io co re core-rpc) #:use-module (crates-io))

(define-public crate-core-rpc-0.14.0 (c (n "core-rpc") (v "0.14.0") (d (list (d (n "core-rpc-json") (r "^0.14.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jjhvrvngxiifffx2pdqlpr3cpyiq6hrvq10ljia5n44p3q8a5zc") (y #t)))

(define-public crate-core-rpc-0.15.0 (c (n "core-rpc") (v "0.15.0") (d (list (d (n "core-rpc-json") (r "^0.15.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0mswp1fwbazi9f96mwy12bvqd9rrzrgsb8hj7px9n10n6fcf5z38") (y #t)))

(define-public crate-core-rpc-0.17.0 (c (n "core-rpc") (v "0.17.0") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "core-rpc-json") (r "^0.17.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nr3bf31s1kri8lxijiwxkslzkddmn8z3np1srw2f75pw5wp1mr6")))

