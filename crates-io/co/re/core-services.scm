(define-module (crates-io co re core-services) #:use-module (crates-io))

(define-public crate-core-services-0.1.0 (c (n "core-services") (v "0.1.0") (d (list (d (n "core-foundation") (r "^0.7.0") (d #t) (k 0)))) (h "0w7svpkk3ld2kb092fp3c5w64834fp2yzdsdh5kf1mmgynjkzwkn")))

(define-public crate-core-services-0.1.1 (c (n "core-services") (v "0.1.1") (d (list (d (n "core-foundation") (r "^0.7.0") (d #t) (k 0)))) (h "1lbpz2dgs1ppq3yjmg4hs5h4yirffqkydabi50h37il22c1vdz5m")))

(define-public crate-core-services-0.2.0 (c (n "core-services") (v "0.2.0") (d (list (d (n "core-foundation") (r "^0.9.1") (d #t) (k 0)))) (h "0ixkz88vl2am2pmd1jpak23frigck6az91k0pxc0isfab2wl9csi")))

(define-public crate-core-services-0.2.1 (c (n "core-services") (v "0.2.1") (d (list (d (n "core-foundation") (r "^0.9.1") (d #t) (k 0)))) (h "1zf0wcm7rkp4c265zvn7q8h7iv14as3msb3lmzmm09ajvf0pwmlj")))

