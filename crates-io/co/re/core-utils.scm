(define-module (crates-io co re core-utils) #:use-module (crates-io))

(define-public crate-core-utils-0.1.0 (c (n "core-utils") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.9") (d #t) (k 0)))) (h "043szljm80jh229f8lsglz76fsq5jkrjiyxhnsmw54m7p8iagnvr")))

(define-public crate-core-utils-0.1.1 (c (n "core-utils") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.5.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.11") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.5") (d #t) (k 0)))) (h "1ia791yhbv8gp39v77nvp4nvddq2fmr0qv8z357kw4msgldhwqyy")))

(define-public crate-core-utils-0.1.2 (c (n "core-utils") (v "0.1.2") (d (list (d (n "num_cpus") (r "^0.2.6") (d #t) (k 0)) (d (n "pgetopts") (r "^0.1.2") (d #t) (k 0)) (d (n "rpf") (r "^0.1.3") (d #t) (k 0)))) (h "1ml4pr38vckyd2nx6vj5mb7jhnvqjzcqapq7wincpjwkbkx2ac19")))

