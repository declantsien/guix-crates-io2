(define-module (crates-io co re corefoundation-sys) #:use-module (crates-io))

(define-public crate-CoreFoundation-sys-0.1.0 (c (n "CoreFoundation-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0hyhggncsbfxzd6sqn6dx032lrk32yfh03j352xzcf2q745qxm7s")))

(define-public crate-CoreFoundation-sys-0.1.1 (c (n "CoreFoundation-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "0mw9jzlirq5hia9dmdhl2xr4sn63grfdli4ficl88v8ny6z634lg")))

(define-public crate-CoreFoundation-sys-0.1.2 (c (n "CoreFoundation-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "07xn5ww5bz8d1vcxka1gp6pj5clx7i4hxksw8vrbxik6lkcqf6z5")))

(define-public crate-CoreFoundation-sys-0.1.3 (c (n "CoreFoundation-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jbblw7r55sm302rvmr1y96hn59d46dy461r54hmj80h4a2gj82s")))

(define-public crate-CoreFoundation-sys-0.1.4 (c (n "CoreFoundation-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mach") (r "^0.1.0") (d #t) (k 0)))) (h "02y2lpp1n52mbzwbbvqbd2jp7nb4x782hiyqi2fx865idng8isfh")))

