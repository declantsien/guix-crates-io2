(define-module (crates-io co re core2) #:use-module (crates-io))

(define-public crate-core2-0.0.0 (c (n "core2") (v "0.0.0") (h "1pbsly2rimpj6xv9b9rv2fgp1v6dkrfqd5nbal53vimydpkikh0j")))

(define-public crate-core2-0.3.0-alpha.1 (c (n "core2") (v "0.3.0-alpha.1") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "0vix9s57y9v24nzdlydywvcayyxblzj49nq6khbnx93x2rs5pw52") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-core2-0.3.0 (c (n "core2") (v "0.3.0") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "0ypj57wx9gfdpzxsqkm15xpwrgdsfdsncj78rr88npmzzzsi36if") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-core2-0.3.1 (c (n "core2") (v "0.3.1") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "145lr8jvn85bywf5kaf1jk9yjmiihv4mrp96cl0rsryks0daxknv") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-core2-0.3.2 (c (n "core2") (v "0.3.2") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "1xm5np2wjjak1v9y86mqkbb972a750a5cnbg26m29l9ymlnjvw9c") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-core2-0.3.3 (c (n "core2") (v "0.3.3") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "1wzzy5iazdk5caadxvjfwrd312rbg7a55a1zpmsdrhk3kfpa77r3") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-core2-0.4.0 (c (n "core2") (v "0.4.0") (d (list (d (n "memchr") (r "^2") (k 0)))) (h "01f5xv0kf3ds3xm7byg78hycbanb8zlpvsfv4j47y46n3bpsg6xl") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

