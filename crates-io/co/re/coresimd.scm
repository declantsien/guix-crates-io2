(define-module (crates-io co re coresimd) #:use-module (crates-io))

(define-public crate-coresimd-0.0.4 (c (n "coresimd") (v "0.0.4") (d (list (d (n "cupid") (r "^0.5.0") (d #t) (k 2)))) (h "16va1x4vd6zg3fm3fw1xqvwhmndw7kpdq97455frphixwj0r9br9") (f (quote (("strict") ("intel_sde"))))))

(define-public crate-coresimd-0.1.0 (c (n "coresimd") (v "0.1.0") (h "0zbwpv13hlxkjwrspgnbjqraivqrfm00qwn1d45pavw18flysv9k") (f (quote (("wasm_simd128") ("strict") ("intel_sde"))))))

(define-public crate-coresimd-0.1.1 (c (n "coresimd") (v "0.1.1") (d (list (d (n "wasm-bindgen-test") (r "= 0.2.19") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "0xd2c2w110pzn1qj52jb9i7ysc15svd5gc6cv8512rzz9jgpvz4p") (f (quote (("wasm_simd128") ("strict") ("intel_sde"))))))

(define-public crate-coresimd-0.1.2 (c (n "coresimd") (v "0.1.2") (d (list (d (n "wasm-bindgen-test") (r "= 0.2.19") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "0xrgjj6fp6nwwak6j3zblhnfb0v1w51mkirs4dcwv99a2zbm70yr") (f (quote (("wasm_simd128") ("strict") ("intel_sde"))))))

