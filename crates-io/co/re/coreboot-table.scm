(define-module (crates-io co re coreboot-table) #:use-module (crates-io))

(define-public crate-coreboot-table-0.1.0 (c (n "coreboot-table") (v "0.1.0") (h "1rcf2i5clkh2ygkdcb6rwvkf7nwvjyij8bkhq93hpx82lxdclfrf")))

(define-public crate-coreboot-table-0.1.1 (c (n "coreboot-table") (v "0.1.1") (h "129p0df265wdycx5wbgb18g9sqbhr0r0mzm3ybg6rm1as1qapnn4")))

(define-public crate-coreboot-table-0.1.2 (c (n "coreboot-table") (v "0.1.2") (h "0xjzn319h7l6qyxc5zar0xlb6gv8j783scmdsk5dys88abis9iks")))

(define-public crate-coreboot-table-0.1.3 (c (n "coreboot-table") (v "0.1.3") (h "1qajfc21h6svrvampii1p1qa00v6qajyd374n82m3k7g43s4iyvi")))

(define-public crate-coreboot-table-0.1.4 (c (n "coreboot-table") (v "0.1.4") (h "00h7wwra4p0s4h3ml9mrf21ycy6wji47wbinlhg5fh6kgak8863j")))

(define-public crate-coreboot-table-0.1.5 (c (n "coreboot-table") (v "0.1.5") (h "1w1kf093yr5q8g5yynigr9da469m5zri8ahqm85hn6y2plc8hz90")))

(define-public crate-coreboot-table-0.1.6 (c (n "coreboot-table") (v "0.1.6") (h "01bhban78m9jx2w40v9j8d9pnj49w57ksbfkmkcfj26ir4z27925")))

