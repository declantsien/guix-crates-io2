(define-module (crates-io co re core-foundation-sys) #:use-module (crates-io))

(define-public crate-core-foundation-sys-0.1.0 (c (n "core-foundation-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0dslx71mldslywa0rllp0hhslds4pb9rv7lrv5ldvvs59r409j3a")))

(define-public crate-core-foundation-sys-0.2.0 (c (n "core-foundation-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pamc5vyv0mbn7383hg4mncd23gyjnck0hc9g9p36q2vgwj2c59h")))

(define-public crate-core-foundation-sys-0.2.1 (c (n "core-foundation-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k85ldvvv6mw0ax5mwkq1h1yyff2srl4r2hw6kz2p6y4nflyvl7c")))

(define-public crate-core-foundation-sys-0.2.2 (c (n "core-foundation-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ibl8p7c1wq86fvs3yha51qz0ijgnr7zx51pqs8m6jjhvi4d5vh5")))

(define-public crate-core-foundation-sys-0.2.3 (c (n "core-foundation-sys") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13f7f3kblyj6yxcxm74yg84vj9ahaprlc1vgblagmj6bzmzmsnh6")))

(define-public crate-core-foundation-sys-0.3.0 (c (n "core-foundation-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n5kyxrqfksaicz8xfradlz9a0pya8rxih3gmqlmshhksnnnmss4")))

(define-public crate-core-foundation-sys-0.3.1 (c (n "core-foundation-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0956j1zfdycmchhkym8q73azmlbib7r770qlk3pybqfklmm5l4a1")))

(define-public crate-core-foundation-sys-0.4.0 (c (n "core-foundation-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0568p3m06pf1pr8snrrisfddhpa95d9sz8xq9wdldlczdn46mffn")))

(define-public crate-core-foundation-sys-0.4.1 (c (n "core-foundation-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qvcd3zaff6qyl9szhs7m1dm37d3xlalsfikc2v36jk7v84z5xak")))

(define-public crate-core-foundation-sys-0.4.2 (c (n "core-foundation-sys") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "143pyaiqhh5r4qanlmrqg1mvkmh41n49d6qw30yg3ag6nqan35cc")))

(define-public crate-core-foundation-sys-0.4.3 (c (n "core-foundation-sys") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "176qvqblhnvw2qjm03c1kvr7k7w4qhhkb2089fabvqmbc5gpb6yy") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.4.4 (c (n "core-foundation-sys") (v "0.4.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11rkk9dzigrlvm8h8njrg151wazf8jqzjqqwrzbnygk6rgbb77xw") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.4.5 (c (n "core-foundation-sys") (v "0.4.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1igigl58hmnbkmrdc83glhbbfgz3fixx6x4zlj9n2dc52abxyd6p") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.4.6 (c (n "core-foundation-sys") (v "0.4.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mdwish2fvb0y4kbcrf4v1gf9s2fklp6g5ci2yl9fr1f3919a88m") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.5.0 (c (n "core-foundation-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n91l5ah4s656k5fjimlpz56gd92b09qn0ixdm282l25si0ha0z2") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.5.1 (c (n "core-foundation-sys") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yiyi30bnlnh29i21gp5f411b4qaj05vc8zp8j1y9b0khqg2fv3i") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.6.0 (c (n "core-foundation-sys") (v "0.6.0") (h "0nigyncd4l5y1vjq18cryy5z0fssgmsqswwqz7hygxyw1p73r9dj") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.6.1 (c (n "core-foundation-sys") (v "0.6.1") (h "1y91azxyga02l181dqynb2x1nqykbjxrc0kxv618pkyrpp6ibyx3") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.6.2 (c (n "core-foundation-sys") (v "0.6.2") (h "0fzsw1j9g1x598yhwklg59l15hwzc0pyvs01w9fg2kin4598mjp7") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.7.0 (c (n "core-foundation-sys") (v "0.7.0") (h "1b5qfnnmg49sawwfsb0c0wbj81bqi7h7lh68pmhbidf0jjs1m9xk") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.7.1 (c (n "core-foundation-sys") (v "0.7.1") (h "0g2w2p09dsc9jdhid8p65am6xv0j2byfxlj1ibvm9i6a1bpcvb4h") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support")))) (y #t)))

(define-public crate-core-foundation-sys-0.7.2 (c (n "core-foundation-sys") (v "0.7.2") (h "1ghrg46h4ci306agr2vwm28w6gb5l455nzp61y2zkhwfs49p4nis") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support")))) (y #t)))

(define-public crate-core-foundation-sys-0.8.0 (c (n "core-foundation-sys") (v "0.8.0") (h "1ijmbl295j3w92gmvy1jqvx159x6c7rr2h6biz6s600pjhhzl8cs") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.8.1 (c (n "core-foundation-sys") (v "0.8.1") (h "14rk4fn4nmigp096s97sxd30syp4mbh2jbikr4vkiph18rg3pby0") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.8.2 (c (n "core-foundation-sys") (v "0.8.2") (h "06wq7yb7mlkc4h2kbc0yjfi0xv44z4snzdpr7c1l0zm4hi91n8pa") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.8.3 (c (n "core-foundation-sys") (v "0.8.3") (h "1p5r2wckarkpkyc4z83q08dwpvcafrb1h6fxfa3qnikh8szww9sq") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.8.4 (c (n "core-foundation-sys") (v "0.8.4") (h "1yhf471qj6snnm2mcswai47vsbc9w30y4abmdp4crb4av87sb5p4") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support"))))))

(define-public crate-core-foundation-sys-0.8.5 (c (n "core-foundation-sys") (v "0.8.5") (h "0d1l42rzq2fy0bnhxkk4iaywlkqrznimd41ny8fidzsy9v9zb1d3") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support")))) (y #t)))

(define-public crate-core-foundation-sys-0.8.6 (c (n "core-foundation-sys") (v "0.8.6") (h "13w6sdf06r0hn7bx2b45zxsg1mm2phz34jikm6xc5qrbr6djpsh6") (f (quote (("mac_os_10_8_features") ("mac_os_10_7_support") ("link") ("default" "link"))))))

