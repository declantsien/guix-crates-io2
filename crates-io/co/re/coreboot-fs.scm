(define-module (crates-io co re coreboot-fs) #:use-module (crates-io))

(define-public crate-coreboot-fs-0.1.0 (c (n "coreboot-fs") (v "0.1.0") (d (list (d (n "plain") (r "^0.2.3") (d #t) (k 0)))) (h "06yg4xi8ip3yl865iq2yaw4b7zyp8lb4mjn4zb0pcj7hhw54xv1c")))

(define-public crate-coreboot-fs-0.1.1 (c (n "coreboot-fs") (v "0.1.1") (d (list (d (n "plain") (r "^0.2.3") (d #t) (k 0)))) (h "0mdwkmh08zxf298r9iskpd369jm2nn9vx2j9yy2zmarkjyn9m2fi")))

