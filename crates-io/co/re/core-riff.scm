(define-module (crates-io co re core-riff) #:use-module (crates-io))

(define-public crate-core-riff-0.1.0 (c (n "core-riff") (v "0.1.0") (d (list (d (n "loam-sdk") (r "^0.6.5") (f (quote ("loam-soroban-sdk"))) (d #t) (k 0)) (d (n "loam-sdk") (r "^0.6.5") (f (quote ("soroban-sdk-testutils"))) (d #t) (k 2)) (d (n "smartdeploy-sdk") (r "^0.1.0") (d #t) (k 0)))) (h "0brhh6dvaq1689cv6hnvzwv1s3wfjbqic21xmfjihh9fbk1b62n0")))

