(define-module (crates-io co re core-audio-types) #:use-module (crates-io))

(define-public crate-core-audio-types-0.1.0 (c (n "core-audio-types") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "162nwf99y8cma9zrnhbvx0vp2g6yq2brxvv67ysra5x8p0f3yz5s") (y #t)))

(define-public crate-core-audio-types-0.1.1 (c (n "core-audio-types") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kifx2fzbyhpjcwkkw81v0l67lcyd84ja5qy57j1k12b7kqrdxpr")))

(define-public crate-core-audio-types-0.1.2 (c (n "core-audio-types") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ddw4lxxi39gwpr7qs9l5yxa3zqdkl2fm8rig4mp8hmv2l3yyg4i")))

(define-public crate-core-audio-types-0.1.3 (c (n "core-audio-types") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.8") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12wkc8ll3isgj8889p0ixan8fl1lv91133ya97r15p1bd8q9yzr2") (f (quote (("link" "core-foundation-sys/link") ("default" "link"))))))

