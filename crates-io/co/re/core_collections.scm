(define-module (crates-io co re core_collections) #:use-module (crates-io))

(define-public crate-core_collections-0.1.0 (c (n "core_collections") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1cm4gvni3vzb6wx5izy2jpx28s2n6v6i5wiwjyxzsnpp84yrlw6k") (f (quote (("rdrand"))))))

(define-public crate-core_collections-0.1.1 (c (n "core_collections") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ssgvvdfxk9ppacxcvx03s27i6pqczyv4yis4ws0w9b9s2hzsdw8") (f (quote (("rdrand"))))))

(define-public crate-core_collections-0.2.20160707 (c (n "core_collections") (v "0.2.20160707") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "105v9hk2ddkvdgzc0zilwy8cp3mcwqn09kdlmbkgflnllrv96yji")))

(define-public crate-core_collections-0.2.20160708 (c (n "core_collections") (v "0.2.20160708") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "19pb94pcaz2npai6khw5arnlcnmaa0zpjzimqnjh1hdkq1dzadqk")))

(define-public crate-core_collections-0.3.20160708 (c (n "core_collections") (v "0.3.20160708") (d (list (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0rymb5bbv582h8d8d47lqlcjryi3nmzpcl495gxgmr6rgnxs4p85")))

(define-public crate-core_collections-0.3.20161028 (c (n "core_collections") (v "0.3.20161028") (d (list (d (n "rand") (r "^0.3") (o #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "0s153j6d03rim73ypp4gnh7sljxz5c58djm8h6hj2k3xmyck1z5v")))

(define-public crate-core_collections-0.3.20170409 (c (n "core_collections") (v "0.3.20170409") (d (list (d (n "rand") (r "^0.3") (o #t) (k 0)) (d (n "rustc_version") (r "^0.1.7") (d #t) (k 1)))) (h "1w4fvv0qvj4zpb8zr6hy6grnqr7z7x8rby1zdwlv53hbfgky171i")))

