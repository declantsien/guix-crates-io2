(define-module (crates-io co re coremidi-sys) #:use-module (crates-io))

(define-public crate-coremidi-sys-1.0.0 (c (n "coremidi-sys") (v "1.0.0") (d (list (d (n "core-foundation-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fdda3v98kk4xalr61xkhxkgp23zaydbfaj38yj3z9vy6hmlpqqi")))

(define-public crate-coremidi-sys-2.0.0 (c (n "coremidi-sys") (v "2.0.0") (d (list (d (n "core-foundation-sys") (r "^0.2") (d #t) (k 0)))) (h "0cqa5il731gbp282b8p5wcg1b8620sagif8cagm6c9hbxrpdnbic")))

(define-public crate-coremidi-sys-2.0.1 (c (n "coremidi-sys") (v "2.0.1") (d (list (d (n "core-foundation-sys") (r "^0.2") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "19j11b99bgxhb2cp64yjvh2jw5d37qa39csjndl999iwrqn5x0fa")))

(define-public crate-coremidi-sys-2.0.2 (c (n "coremidi-sys") (v "2.0.2") (d (list (d (n "core-foundation-sys") (r "^0.2") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "0ppr89qn3mwr5xbzhix1byhp9xb4cyzwj6pz77axqc5vrqkmiw07")))

(define-public crate-coremidi-sys-3.0.0 (c (n "coremidi-sys") (v "3.0.0") (d (list (d (n "core-foundation-sys") (r "^0.8.2") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "1wv8d118k7dympc4spnqhhsf917hi91qqwm98irg6szsq14bx8c9") (y #t)))

(define-public crate-coremidi-sys-3.0.1 (c (n "coremidi-sys") (v "3.0.1") (d (list (d (n "core-foundation-sys") (r "^0.8.2") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "03gjk4406mkbfyxgq0mx5ddbsixhl0pnn38ip8my8jv3dzc795ml")))

(define-public crate-coremidi-sys-3.1.0 (c (n "coremidi-sys") (v "3.1.0") (d (list (d (n "core-foundation-sys") (r "^0.8.3") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)))) (h "0v0i34069pbpqa136hh04qb1xmw1gz4y8xxbpc5d9clp1knxx9kr")))

