(define-module (crates-io co re coreos-stream-metadata) #:use-module (crates-io))

(define-public crate-coreos-stream-metadata-0.1.0 (c (n "coreos-stream-metadata") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r ">=0.20, <0.25") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.20, <0.25") (d #t) (k 0)))) (h "0q9bwzbgsxpdn448xmvgczi1zj45cbvcb1a4919g6mz7wz95hm3s") (r "1.58.0")))

