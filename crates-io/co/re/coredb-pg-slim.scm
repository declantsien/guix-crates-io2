(define-module (crates-io co re coredb-pg-slim) #:use-module (crates-io))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.1 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.1") (h "1qjzdx0nacnshdrs7bj1h15n8a0rrl9clf7l7787yy496mxpvakw") (y #t)))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.3 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.3") (h "1byb0v9j0hg7h4fr4aibnnsi33lqkdr079yb0bqzyl1c41m0l6dq") (y #t)))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.4 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.4") (h "10cx2ajkrf53s78fiff9p0yb26hyjbal19il5f6xsalbdqvx5q1b") (y #t)))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.5 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.5") (h "0y675y9v35zv6dn0fpp63sfzdz3z28hpk92jwq6wpincj2990lim") (y #t)))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.6 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.6") (h "143py8xrbcb5rhzapqkmfm7n1s65v8fnvvm6ipv8a17118fwh6m2") (y #t)))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.7 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.7") (h "0p3d56q5xv5zwdkzjfybcgbab4a2phf32vin1443yx07580gff1d") (y #t)))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.8 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.8") (h "1wzbpb4jja4cs68g1jhmr8z20r07d7sk1arw03aykvfmp41yqrcr") (y #t)))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.9 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.9") (h "00b4113ay8qw599pxpniyb3zff2jmb3lxvjfa0rl5dxmz2adcfff") (y #t)))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.10 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.10") (h "0ybv8wvyw5mvl3zardks76kl9zgb1h9aj7qw7grsfhpbrfydqhay") (y #t)))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.13 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.13") (h "0qajr0lx1gn2jqk23s4fpafi0385vh7n6cm4cr26d8c68083w9h6") (y #t)))

(define-public crate-coredb-pg-slim-15.2.0-coredb-pg-slim.14 (c (n "coredb-pg-slim") (v "15.2.0-coredb-pg-slim.14") (h "00m71sphgfs8lkfj2d28q8vsa4mvzk5995qx39g5gfx6whz8ybsi") (y #t)))

