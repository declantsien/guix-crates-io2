(define-module (crates-io co re core_arch) #:use-module (crates-io))

(define-public crate-core_arch-0.1.0 (c (n "core_arch") (v "0.1.0") (d (list (d (n "wasm-bindgen-test") (r "= 0.2.19") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "11ahjqcryxyf1wxvr2m6lsq85dmaqf4xcbgwyhbyaadan5xi4ycv") (f (quote (("strict"))))))

(define-public crate-core_arch-0.1.1 (c (n "core_arch") (v "0.1.1") (d (list (d (n "wasm-bindgen-test") (r "= 0.2.19") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "1hkq1x4962qxiygk2j53iwkb9q79k7dag21sjw5wc9pxqlhmvicw") (f (quote (("strict"))))))

(define-public crate-core_arch-0.1.2 (c (n "core_arch") (v "0.1.2") (d (list (d (n "wasm-bindgen-test") (r "= 0.2.19") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "1s80nnh66nyabp5jwh688kcdff07jdb50ahk4cd9wdp42vqrsdqp") (f (quote (("strict"))))))

(define-public crate-core_arch-0.1.3 (c (n "core_arch") (v "0.1.3") (d (list (d (n "wasm-bindgen-test") (r "= 0.2.19") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "0gdk3n8lr7b945d6gk73az0v7bvsyhb6lvcx5qsgg7rv8psvc1im") (f (quote (("strict"))))))

(define-public crate-core_arch-0.1.4 (c (n "core_arch") (v "0.1.4") (h "1wd16qj9fk079zg7gvz9b7zrypk381nq9hsr4gy9k55yjjjh4hfv")))

(define-public crate-core_arch-0.1.5 (c (n "core_arch") (v "0.1.5") (d (list (d (n "wasm-bindgen-test") (r "= 0.2.19") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "04vdvr9vj0f1cv2p54nsszmrrk9w1js4c0z4i0bdlajl1lydslim")))

