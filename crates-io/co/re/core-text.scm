(define-module (crates-io co re core-text) #:use-module (crates-io))

(define-public crate-core-text-0.0.1 (c (n "core-text") (v "0.0.1") (h "0mycapz9y88jpxz8lv5d5zjhjzvafb93lf9z33ni7nmc8wq8ngqf")))

(define-public crate-core-text-0.0.2 (c (n "core-text") (v "0.0.2") (h "0qi5h59hgg0g4sfwc9ff54wi0cp0mgbblqikjw30jpgkkpb0wmnw")))

(define-public crate-core-text-1.0.0 (c (n "core-text") (v "1.0.0") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.2") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "009px6c7g209ymfxlzd9hr1mgz6kj10pag2gc0v6b38gmjmxdmj1")))

(define-public crate-core-text-1.1.0 (c (n "core-text") (v "1.1.0") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r ">= 0.2, < 0.4") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1z3b8shw1mvckb2pjc4qnfjlzly8fpvynch1gf1wpya3p2fz1k3d")))

(define-public crate-core-text-1.1.1 (c (n "core-text") (v "1.1.1") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r ">= 0.2, < 0.4") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0y18rf9f67vjhga2cbccnbp62arj1v2llxi8hxj2l970p7xg7m4l")))

(define-public crate-core-text-2.0.0 (c (n "core-text") (v "2.0.0") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.4") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "166w4hqjm1x00nzs5hzr5rxbwrs6d5jnv2wm1qkmx3imm0ibzsrd")))

(define-public crate-core-text-2.0.1 (c (n "core-text") (v "2.0.1") (d (list (d (n "core-foundation") (r "^0.2") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.5") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "037d469qqgiwaac0bl778wygv8z1qgzm2sb37yykpidncl0s8bv4")))

(define-public crate-core-text-3.0.0 (c (n "core-text") (v "3.0.0") (d (list (d (n "core-foundation") (r "^0.3") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.6") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "174n13x8qlyw1wzahr18ld2na2xkligvqbb1r8ajnqhylicz80wp")))

(define-public crate-core-text-4.0.0 (c (n "core-text") (v "4.0.0") (d (list (d (n "core-foundation") (r "^0.3") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.7") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pfhmac48kyijv0jjkd3yx8b9xsxqpw48iq7iri1gxqhd9hik5qf")))

(define-public crate-core-text-5.0.0 (c (n "core-text") (v "5.0.0") (d (list (d (n "core-foundation") (r "^0.3") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.8") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d7wi1w4i1l2wwq5m92s50mih145ykgas8h8q2v4zffcpmx2mfkl")))

(define-public crate-core-text-5.0.1 (c (n "core-text") (v "5.0.1") (d (list (d (n "core-foundation") (r "^0.3") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.8") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g604adqir8gphyp523cgky06wz6ay1673r6j8cqhswd80bswhkw") (f (quote (("lion") ("default"))))))

(define-public crate-core-text-6.0.0 (c (n "core-text") (v "6.0.0") (d (list (d (n "core-foundation") (r "^0.3") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.8") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "011d4b7iyy2k49jdxaj92wvp4a128x4879aj3fy529qlkz9avj4y") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-6.1.0 (c (n "core-text") (v "6.1.0") (d (list (d (n "core-foundation") (r "^0.3") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.8") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rjx9s6n2rlk667y97yfzcmwkghl2db8xx0zq4b10600xpcidkhn") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-6.1.1 (c (n "core-text") (v "6.1.1") (d (list (d (n "core-foundation") (r "^0.4") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.9") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "027js5dqxw9nqs7rik7ppmq676lc6n80xwdp9341z463ix8a31hg") (f (quote (("mountainlion") ("default" "mountainlion")))) (y #t)))

(define-public crate-core-text-6.1.2 (c (n "core-text") (v "6.1.2") (d (list (d (n "core-foundation") (r "^0.3") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.8") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x9mv4d5y2dglvdrgq90f2vynwr0w03xdzikcq4spv2yp4mr63vg") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-7.0.0 (c (n "core-text") (v "7.0.0") (d (list (d (n "core-foundation") (r "^0.4") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "^0.9") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0phzinj5wh2xa0lqkskxd30y2j1ss3p3cgpjd9g0xdzsg7vvw8ra") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-8.0.0 (c (n "core-text") (v "8.0.0") (d (list (d (n "core-foundation") (r "^0.4") (d #t) (k 0)) (d (n "core-graphics") (r "^0.12.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z26s0cd0213mwvminw9i2k8znxa4z74m4bapx3lpp6idmsj7bdw") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-9.0.0 (c (n "core-text") (v "9.0.0") (d (list (d (n "core-foundation") (r "^0.5") (d #t) (k 0)) (d (n "core-graphics") (r "^0.13") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zz08rb1aax31w27ncjx88dv1l7pckm0l9zdc74l6a8i3xf7lwrc") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-9.1.0 (c (n "core-text") (v "9.1.0") (d (list (d (n "core-foundation") (r "^0.5") (d #t) (k 0)) (d (n "core-graphics") (r "^0.13") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mlsr8x8zl9pc2gp1ad90hlm1a0ngm2ahhywhryb9q2ryh0gj5pl") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-9.2.0 (c (n "core-text") (v "9.2.0") (d (list (d (n "core-foundation") (r "^0.5") (d #t) (k 0)) (d (n "core-graphics") (r "^0.13") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j2pdsvllng84s5aapa0w1735wlip3xswyfi24rw5l43fb1q3m9b") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-10.0.0 (c (n "core-text") (v "10.0.0") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "core-graphics") (r "^0.14") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14dc8064m9hb6kpjn835rk3vwlkb821mjgwa0p6yam1rfzzrpxc1") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-10.0.1 (c (n "core-text") (v "10.0.1") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "core-graphics") (r "^0.16") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x0y35m3z35ri0hh07pp3zhlpm2kwzcbj1xwkm4wnsky0c3zjsif") (f (quote (("mountainlion") ("default" "mountainlion")))) (y #t)))

(define-public crate-core-text-11.0.0 (c (n "core-text") (v "11.0.0") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "core-graphics") (r "^0.16") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s2rz8fgfr5cj0k9b7q3q7qndb79ai2i5n9nrrvccva9ja5g6zqm") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-13.0.0 (c (n "core-text") (v "13.0.0") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "core-graphics") (r "^0.17") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bjhy2vnzmh3190hd8jzwk9as1rqwb64q2s2mxhp4fgjsr869x7k") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-13.1.0 (c (n "core-text") (v "13.1.0") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "core-graphics") (r "^0.17") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n4yg4nxga3lxb2zx6aw62z082g5dj1if42mp28fwg15ph3mjw56") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-13.1.1 (c (n "core-text") (v "13.1.1") (d (list (d (n "core-foundation") (r "^0.6.2") (d #t) (k 0)) (d (n "core-graphics") (r "^0.17") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xvd1snfqpz8kg1nv4nrsp475d52v23lf3nr35rivdd4pajfw43j") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-13.2.0 (c (n "core-text") (v "13.2.0") (d (list (d (n "core-foundation") (r "^0.6.2") (d #t) (k 0)) (d (n "core-graphics") (r "^0.17") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sg60zkqnw1b5i23b1h2dy65pzlmfh2g7vw8vmlrjm0fwnsp4nnr") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-13.3.0 (c (n "core-text") (v "13.3.0") (d (list (d (n "core-foundation") (r "^0.6.2") (d #t) (k 0)) (d (n "core-graphics") (r "^0.17") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h56y8a71dv5yqi4ahhqprxxk4azfzxji5mll805dj8lnd1l4s0j") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-13.3.1 (c (n "core-text") (v "13.3.1") (d (list (d (n "core-foundation") (r "^0.6.2") (d #t) (k 0)) (d (n "core-graphics") (r "^0.18") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0in8jwv1183n529c75ky2y1a80ahr2ixjc5j7gllb9znxkx79gld") (f (quote (("mountainlion") ("default" "mountainlion")))) (y #t)))

(define-public crate-core-text-13.3.2 (c (n "core-text") (v "13.3.2") (d (list (d (n "core-foundation") (r "^0.6.2") (d #t) (k 0)) (d (n "core-graphics") (r "^0.17") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v9lxn277p39cf81pb45r7k0lzf17pwgd5cpry1c04ajv556b16v") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-15.0.0 (c (n "core-text") (v "15.0.0") (d (list (d (n "core-foundation") (r "^0.7") (d #t) (k 0)) (d (n "core-graphics") (r "^0.19") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07d6lbxrhzicmh3c6iyl0ky4mih8c3dlzylqngrbjpdxz38ky6qk") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-16.0.0 (c (n "core-text") (v "16.0.0") (d (list (d (n "core-foundation") (r "^0.7") (d #t) (k 0)) (d (n "core-graphics") (r "^0.19.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1467l9l0sylc86m6dsmsq3pwsl53ybz2f8p6vxfa48wsx9zr3mcn") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-17.0.0 (c (n "core-text") (v "17.0.0") (d (list (d (n "core-foundation") (r "^0.8") (d #t) (k 0)) (d (n "core-graphics") (r "^0.20.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g8pax73q6gd1w49mmc1cdgsb6cvmmb77vrq9nyifd1nxmqigy2z") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-18.0.0 (c (n "core-text") (v "18.0.0") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics") (r "^0.21.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0daccsdxnvympmfjrchcghlwhlksw6v8kycf6rwy6cf2cgm6knj5") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-18.0.1 (c (n "core-text") (v "18.0.1") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics") (r "^0.21.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "171yvki91nfd5pjxf8xijaidn3g1vwirayd0xv2byyhx80yd0l8j") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-19.0.0 (c (n "core-text") (v "19.0.0") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics") (r "^0.22.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qm4r43r5misacwnyzvz0fc57p6d3vdwsx3izrbjdrqimx8axpq4") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-18.1.0 (c (n "core-text") (v "18.1.0") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics") (r "^0.21.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0c71kqwhmjqm4v527kg3m2rb1jlz4jsdbva9dsfj5rkjqqh4k9cn") (f (quote (("mountainlion") ("default" "mountainlion")))) (y #t)))

(define-public crate-core-text-19.1.0 (c (n "core-text") (v "19.1.0") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics") (r "^0.22.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q1sr55v8zq82y0dwnwwksz1radh515i0a45nbsda3w2idpg9iyj") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-19.2.0 (c (n "core-text") (v "19.2.0") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics") (r "^0.22.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09aa9gfw0zvjwncskr721ljnyj2vfsxbz2lgl7piqz70cvd4mmwr") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-20.0.0 (c (n "core-text") (v "20.0.0") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics") (r "^0.23.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hl7g4m60w6v7511xflb0fr74njmc4spaqqs7ralfqlxk0aks202") (f (quote (("mountainlion") ("default" "mountainlion"))))))

(define-public crate-core-text-20.1.0 (c (n "core-text") (v "20.1.0") (d (list (d (n "core-foundation") (r "^0.9") (d #t) (k 0)) (d (n "core-graphics") (r "^0.23.0") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mffma8w0ic11ydv6zclamw4dslzmsych1fwz14msih8bh5pkln9") (f (quote (("mountainlion") ("default" "mountainlion"))))))

