(define-module (crates-io co re coreutils_auth) #:use-module (crates-io))

(define-public crate-coreutils_auth-0.1.0 (c (n "coreutils_auth") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vjssavzbnjhppx172j51lg8bqslx759d7gl55547lyc7lfmrhwr")))

(define-public crate-coreutils_auth-0.2.0 (c (n "coreutils_auth") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mwz42873yypjbw200j23a9kz21g53qhx7ih99q2l86v0m864ljb")))

(define-public crate-coreutils_auth-0.2.1 (c (n "coreutils_auth") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h5apchyfgjl28cf3516y2plsqby3cdmipjw2dkw00gcwaak3vzr")))

(define-public crate-coreutils_auth-0.3.0 (c (n "coreutils_auth") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wq71ci1c6gvvmavycbpwrmm16ah9dy9z6lkxdns1lx5jmgjwd64")))

(define-public crate-coreutils_auth-0.4.0 (c (n "coreutils_auth") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08qsi8rwx94ab1b3c0sc574zyk572f3jy9bcj3y0pjv6wjd75x4r")))

(define-public crate-coreutils_auth-0.5.0 (c (n "coreutils_auth") (v "0.5.0") (d (list (d (n "err-derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "195762jmghjf51yhc7gcl4w109722yncyqcir1qnk5zqqvl9xf2n")))

(define-public crate-coreutils_auth-0.5.1 (c (n "coreutils_auth") (v "0.5.1") (d (list (d (n "err-derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h3fqk3m4310sv4lqdjwpb1n25wim636ykx7r63pz0c0hamg5b8s")))

