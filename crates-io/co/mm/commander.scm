(define-module (crates-io co mm commander) #:use-module (crates-io))

(define-public crate-commander-0.1.0 (c (n "commander") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 1)))) (h "0b260arzbb94jizmyliy9sl6m393jdgiqdhcf7rxzaxsbkaz01hl")))

(define-public crate-commander-0.1.1 (c (n "commander") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 1)))) (h "1c7bg7lnac2m3y1902bsl451djgkpwdgswfpxr4vlrximjb15hyr")))

(define-public crate-commander-0.1.2 (c (n "commander") (v "0.1.2") (d (list (d (n "time") (r "^0.1") (d #t) (k 1)))) (h "1y2hdxvrlx3i1688cs74p4pbwpkyb7ylq6ags8g9301wn2hlzz9s")))

(define-public crate-commander-0.1.3 (c (n "commander") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)))) (h "1v7f6gyahx0vcy22mcqysj6m4b84wamqwfz2g6xvwygqvmch8agj")))

(define-public crate-commander-0.1.4 (c (n "commander") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)))) (h "1xvp02g5lsg5yjm853ychrmgsribp9bzqpgy94pg3w74vssq8r23")))

(define-public crate-commander-0.1.5 (c (n "commander") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)))) (h "1db0nhl5qfp70522f4jzwndgiqi1hc8skzl3hm15zgqnd93zhlkv")))

