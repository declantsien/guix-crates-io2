(define-module (crates-io co mm commit_message) #:use-module (crates-io))

(define-public crate-commit_message-0.1.0 (c (n "commit_message") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05dm0kz41c3iab9hl72nckdcpx082xv883f283m0hjn9jwl1birx")))

(define-public crate-commit_message-0.1.1 (c (n "commit_message") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "171bvnh4v8nwaynxlmsfiy1w57q4gqpaq54sb9f6v5dhp4mnk8km")))

