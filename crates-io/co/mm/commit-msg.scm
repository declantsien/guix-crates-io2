(define-module (crates-io co mm commit-msg) #:use-module (crates-io))

(define-public crate-commit-msg-0.0.1 (c (n "commit-msg") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rust_util") (r "^0.6") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0lsv1a0ls3y58bfbbbdmnafmg6vl8xx8i2dfz4891mhn6h7fdghw")))

