(define-module (crates-io co mm command_line_calculator) #:use-module (crates-io))

(define-public crate-command_line_calculator-1.0.0 (c (n "command_line_calculator") (v "1.0.0") (h "1233gqshghc062b8w4hd5qb36z2ykwvciq9skgz446rnf3a5n5lg")))

(define-public crate-command_line_calculator-1.0.1 (c (n "command_line_calculator") (v "1.0.1") (h "0r6011sls7mf99kimrw9kkgfmmf4cfl5qysxfpxlra45v7dwqb3g")))

(define-public crate-command_line_calculator-1.0.2 (c (n "command_line_calculator") (v "1.0.2") (h "0ipycrinx5860x12y7qjk45v3h5izh0hnidlimfy5y86dmyl1c3l")))

