(define-module (crates-io co mm commands) #:use-module (crates-io))

(define-public crate-commands-0.0.1 (c (n "commands") (v "0.0.1") (h "0vf85br0yic89zz050xcs0603wvwkib44bjqrz0csam85pw9gyyz")))

(define-public crate-commands-0.0.2 (c (n "commands") (v "0.0.2") (h "0qr3zpic9vna6zcvxi09mzmns3ilgyghnhch970fgq1pjfy3gxl0")))

(define-public crate-commands-0.0.3 (c (n "commands") (v "0.0.3") (d (list (d (n "readline") (r "^0.0.12") (d #t) (k 2)))) (h "12xxdjcaln4fsh1jd7k6sfyra3l3wln4lqdalc914vky4x9bhyvn")))

(define-public crate-commands-0.0.4 (c (n "commands") (v "0.0.4") (d (list (d (n "linefeed") (r "^0.1.3") (d #t) (k 2)) (d (n "readline") (r "^0.0.12") (d #t) (k 2)) (d (n "rustyline") (r "^0.2.3") (d #t) (k 2)))) (h "0amls420vj6dl90pgy7vqs9d1vqcai5aryjlfla9y79afkl5k637")))

(define-public crate-commands-0.0.5 (c (n "commands") (v "0.0.5") (d (list (d (n "linefeed") (r "^0.2") (d #t) (k 2)) (d (n "rustyline") (r "^1") (d #t) (k 2)))) (h "08s1dznaznwmfv5ahcrx0ak96mx1vm5gp51av2li3gwvfswjd0k8")))

