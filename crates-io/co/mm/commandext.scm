(define-module (crates-io co mm commandext) #:use-module (crates-io))

(define-public crate-commandext-0.0.1 (c (n "commandext") (v "0.0.1") (h "1kmjz0afcn7hwqbchk02jdhcqkdzf77whlk6pchc8ra0hbfng33q")))

(define-public crate-commandext-0.0.2 (c (n "commandext") (v "0.0.2") (h "05lh40ircw847l64fqy9r3hyk1r1kv0iwk92sfx2sbll3bwxrshr")))

(define-public crate-commandext-0.0.3 (c (n "commandext") (v "0.0.3") (h "1fn4vgzkd3y0f82b5y75kym0i8figmqs2xr2xicj2pg7lf644jxy")))

(define-public crate-commandext-0.0.4 (c (n "commandext") (v "0.0.4") (h "13prhr3wm6d797p5b8p5x82f9n6hjp34kznp6anhnw711z4p27g6")))

(define-public crate-commandext-0.0.5 (c (n "commandext") (v "0.0.5") (h "0hbm040k8dpa96y61jjw9b2ivzfka7q380vcfv4zy7vx10ls7mi1")))

(define-public crate-commandext-0.0.6 (c (n "commandext") (v "0.0.6") (h "06q6azy522aaqdqylfxriyzkc8dlqaqzlm4s8fln1g6lwv3szymx")))

(define-public crate-commandext-0.0.7 (c (n "commandext") (v "0.0.7") (d (list (d (n "clippy") (r "~0.0.30") (o #t) (d #t) (k 0)) (d (n "sodium-sys") (r "~0.0.4") (d #t) (k 2)))) (h "12107jyvchrjn2x9q6k9594kw1q0370qar74zgvdg3cgqii8v60f") (f (quote (("default"))))))

(define-public crate-commandext-0.1.0 (c (n "commandext") (v "0.1.0") (d (list (d (n "clippy") (r "~0.0.30") (o #t) (d #t) (k 0)) (d (n "sodium-sys") (r "~0.0.4") (d #t) (k 2)))) (h "01p3czly3c93ya6la1z3l95ry89c47dmrn3g6kbwb59l9q8xf7ck") (f (quote (("default"))))))

