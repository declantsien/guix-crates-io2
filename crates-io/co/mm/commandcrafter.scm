(define-module (crates-io co mm commandcrafter) #:use-module (crates-io))

(define-public crate-commandcrafter-0.2.0 (c (n "commandcrafter") (v "0.2.0") (h "0p9f42r9a7j4j33ddbwjs9hg16a144yajj4hnn7rd23ynsvj8zdm")))

(define-public crate-commandcrafter-0.2.1 (c (n "commandcrafter") (v "0.2.1") (h "0frgqlwwhdrxxi43j7zgnxhdlgdm2zab7fm1cdqw0764dpk4bajs")))

(define-public crate-commandcrafter-0.2.2 (c (n "commandcrafter") (v "0.2.2") (h "08cjcp4xm6rq0djy6r622656rdd6dw036g99aq13vd9ryszb1r5z")))

(define-public crate-commandcrafter-0.3.0 (c (n "commandcrafter") (v "0.3.0") (h "00chx7w0b4aqgmnj862bnxnx7k3hh8q8mz10ggdc3bvb2xymqi4a")))

(define-public crate-commandcrafter-0.3.2 (c (n "commandcrafter") (v "0.3.2") (h "04g6z219534ms042b8mpqxhp2gj2narxz3pwjaj4cvr76a6d4jbm")))

(define-public crate-commandcrafter-0.3.3 (c (n "commandcrafter") (v "0.3.3") (h "099id5zahk2myv5zxmillg5rll5fa82aiikzs623iz4nfrbwzzw5")))

(define-public crate-commandcrafter-0.4.0 (c (n "commandcrafter") (v "0.4.0") (h "141wzrxsccp47lrbi4jcfj6pvl4lr89aal9brxzaaig0mkzhxdc8")))

(define-public crate-commandcrafter-0.4.1 (c (n "commandcrafter") (v "0.4.1") (h "1913scbi90s4jn1sn9v0p5rgsyp6a25bf4djz3s5ai40apmp75xx")))

(define-public crate-commandcrafter-0.4.2 (c (n "commandcrafter") (v "0.4.2") (h "1rmz9b1zz0xa7q45yyw0il1hc2rdxya8k9kmlkj3b7vwpxp43cky")))

