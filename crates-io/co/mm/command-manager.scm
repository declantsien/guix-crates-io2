(define-module (crates-io co mm command-manager) #:use-module (crates-io))

(define-public crate-command-manager-0.0.1 (c (n "command-manager") (v "0.0.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0a511whs5rn1sv6gz534ppj1r7zwj058s6s1gq93x0rinqid0zpy") (r "1.62")))

(define-public crate-command-manager-0.0.2 (c (n "command-manager") (v "0.0.2") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "0j182ljhfp1lrmn7bic4bdkfl77izp25zacm28a7s9hbw3wmi844") (r "1.69")))

(define-public crate-command-manager-0.0.3 (c (n "command-manager") (v "0.0.3") (d (list (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "19r555vfky2m24d660mwzxxbwns1k26laj6mncwcazcw765d6x6d") (r "1.69")))

