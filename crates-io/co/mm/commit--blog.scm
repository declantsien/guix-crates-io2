(define-module (crates-io co mm commit--blog) #:use-module (crates-io))

(define-public crate-commit--blog-0.1.0 (c (n "commit--blog") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "httparse") (r "^1.5") (d #t) (k 0)) (d (n "keyring") (r "^1.0") (d #t) (k 0)) (d (n "oauth2") (r "^4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "1syqjwqbxrav7b8cwmxyqpfyh4hs7hzg3706f8hgpcsxbp9biv38") (y #t)))

