(define-module (crates-io co mm command-extra) #:use-module (crates-io))

(define-public crate-command-extra-0.0.0 (c (n "command-extra") (v "0.0.0") (h "1fjq85n1s0i6fs6x0zj3wqhmq8bj4da9f82q0kf630qhn7n2b990")))

(define-public crate-command-extra-0.1.0 (c (n "command-extra") (v "0.1.0") (h "0hsg2bnmlmcmll55dvmjzb55ppkllf3fai2b29yp9pgb52484n8g")))

(define-public crate-command-extra-1.0.0 (c (n "command-extra") (v "1.0.0") (h "0b9x36rmijm4swcbnkd23i3qyddc08zxsp3cbfbqsaizx8rgxd5i")))

