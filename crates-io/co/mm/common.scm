(define-module (crates-io co mm common) #:use-module (crates-io))

(define-public crate-common-0.1.0 (c (n "common") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bs58") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "107126xdxpbd9p7hf5485gw5262sym3dz82qk224x7y5qysh0lp0")))

