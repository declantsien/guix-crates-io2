(define-module (crates-io co mm commit_info) #:use-module (crates-io))

(define-public crate-commit_info-0.1.0 (c (n "commit_info") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cmd_lib") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "147wk34x1j989awq9c79q60687zly160yycynklwshjl7bfrlhyw")))

(define-public crate-commit_info-0.1.1 (c (n "commit_info") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cmd_lib") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "14m20b5m7zfs36mfsdkqna33cx7q9x6kgcwa4y32xkwmcxsxzds1")))

