(define-module (crates-io co mm common-tree) #:use-module (crates-io))

(define-public crate-common-tree-0.1.0 (c (n "common-tree") (v "0.1.0") (h "029vcs1f7q2caigg21vzwlzpibj6x20vdys8pb7nlydks8mz1xyv")))

(define-public crate-common-tree-0.1.1 (c (n "common-tree") (v "0.1.1") (h "1y7ybmm8nwahlwwmc2d0r70bsm0v12i0ny9187fh4nmk0r58kv44")))

(define-public crate-common-tree-0.2.0 (c (n "common-tree") (v "0.2.0") (h "1y2rgmfwsyf45w10fm3ghc52ph6flgy2yk4spgdjz9xl3z4pfw1s")))

