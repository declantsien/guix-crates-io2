(define-module (crates-io co mm comment-strip) #:use-module (crates-io))

(define-public crate-comment-strip-0.1.1 (c (n "comment-strip") (v "0.1.1") (d (list (d (n "clap") (r "^2.25.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "0y702b8mwklnkwjpd2prqibha3kszxshdxcn9cnpgkmvy2i4pniv")))

(define-public crate-comment-strip-0.1.2 (c (n "comment-strip") (v "0.1.2") (d (list (d (n "clap") (r "^2.25.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "154x29g0rnf2lfjidrxwfzshv4bhwklzmlcqawbx6adbmwza9hcd")))

(define-public crate-comment-strip-0.1.3 (c (n "comment-strip") (v "0.1.3") (d (list (d (n "clap") (r "^2.25.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "09f70v422r2w43862qvhx4h8sw110000h4dlafsyghry4h6ckgvd")))

