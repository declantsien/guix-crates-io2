(define-module (crates-io co mm command_attr) #:use-module (crates-io))

(define-public crate-command_attr-0.1.0-rc (c (n "command_attr") (v "0.1.0-rc") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0jlcfbcz8wnlhvipnr8a3s90wpf226as0nzgfm1s88c73irw85n2")))

(define-public crate-command_attr-0.1.0 (c (n "command_attr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1csdm9mlagd3bllj0rpzmschvn175kmw124czgd96y9ddssnx895")))

(define-public crate-command_attr-0.1.1 (c (n "command_attr") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "02wrmf3fk2nxdn7crzh2kii9zg5ds1cz4fjkj2xnmxjmvqcfw1jl")))

(define-public crate-command_attr-0.1.2 (c (n "command_attr") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1nd9z3irmw0jb8n8acvfy0y432j684pv1x2mbz2gysh16xz8xbv8")))

(define-public crate-command_attr-0.1.3 (c (n "command_attr") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1nnpl7y38r8g73w76w8phy56cf06qrmd3azh6gndf2dxnk4brbmj")))

(define-public crate-command_attr-0.1.4 (c (n "command_attr") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "18fa3hqbblgaf0xxszlh6ppxjq7cb0w5vj2bsgicx430fdni76f8")))

(define-public crate-command_attr-0.1.5 (c (n "command_attr") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0pbd5p6h518j9nyw033x168xn394vp048kpxjpngwl49p481wcsy")))

(define-public crate-command_attr-0.1.6 (c (n "command_attr") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0bylj95xmwi45qd3h6jp5d7k4l9s0nc7vqlck13dsg8y29k4cq77")))

(define-public crate-command_attr-0.1.7 (c (n "command_attr") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0dmk90g5qix8qbxnbz2rr0n259rjkymk1bhq95nda2iydla9h45n") (y #t)))

(define-public crate-command_attr-0.2.0 (c (n "command_attr") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "10d371imgr15aprcaq2kf6dvdcqp4jdxqgfrg5ihp21xz5an2zf2")))

(define-public crate-command_attr-0.3.0-rc.0 (c (n "command_attr") (v "0.3.0-rc.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0fxzza3nzcis3ldvrkbg4hpgwa1psdzdx1fz5nr43fpw81my0gh9")))

(define-public crate-command_attr-0.3.0-rc.1 (c (n "command_attr") (v "0.3.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0kdb8c00ipg3bxyp17v2xxkx44dvca5694ga3kk3vwda5nmxlf65")))

(define-public crate-command_attr-0.3.0-rc.2 (c (n "command_attr") (v "0.3.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0zdf164x0ih41vrrl83x71afzc4b3kwiabrn4hs5ir75i477fib7")))

(define-public crate-command_attr-0.3.0-rc.3 (c (n "command_attr") (v "0.3.0-rc.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1p6l538rvz134cvvgb4hqf18ssibpqqd557h5m2qnh32hlaq9syc")))

(define-public crate-command_attr-0.3.0-rc.4 (c (n "command_attr") (v "0.3.0-rc.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1h63xv3brvhp2j2qvajm14lydy59fg5whldgykadhkaf30x37x50")))

(define-public crate-command_attr-0.3.0 (c (n "command_attr") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0a56d4g4rfn0xpsjlljn3psl7scr2a545z420ka1f43kk2aqncaq")))

(define-public crate-command_attr-0.3.1 (c (n "command_attr") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "00cx52qns2c9p4i9xaxxhpq1yb1prmyf3ci1knwdjkaa00lxmj4d")))

(define-public crate-command_attr-0.3.2 (c (n "command_attr") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0pi0kivmw63999q5c1hk9qh0vkjxw5xfibyn29j4w612kr0zhjrh")))

(define-public crate-command_attr-0.3.3 (c (n "command_attr") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1iy7v7gh2jjz4xbzbr9m4ncmg5rw326d8bv3kmmh72pzwi3b2m7g")))

(define-public crate-command_attr-0.3.4 (c (n "command_attr") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0xdl56av94d0j0q8k7f7pja8i6mfbwbbpmlyhdj20vmadhj9qiyk")))

(define-public crate-command_attr-0.3.5 (c (n "command_attr") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0ny65h1qd12xlrcld26vjgjx4h5w4gdshv7fl87g8s6ykyvg1qcg")))

(define-public crate-command_attr-0.3.6 (c (n "command_attr") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1l69gkh325xxfklm7frrki0rxm31hbn2ijrh1x4mpdr35gnk417x")))

(define-public crate-command_attr-0.3.7 (c (n "command_attr") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "07fqbqw9q21jfxn76l0410nkg75dvf2ckf180vy1xjw5yrk3cv4a")))

(define-public crate-command_attr-0.3.8 (c (n "command_attr") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0k3ll77kchpv8ifkww85xsapwrw8psib3xgr3f17gdfhz4hs0x4y")))

(define-public crate-command_attr-0.4.0 (c (n "command_attr") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1p26zjpj8h1p2cf5hw08azri209r3z8a0l5741pzwpksb0rbaql8")))

(define-public crate-command_attr-0.4.1 (c (n "command_attr") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "16jxl0v2zalmzb20c1nabcwa9acsgaf633zf9bhhw59ifx79v6ad")))

(define-public crate-command_attr-0.4.2 (c (n "command_attr") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1r4qs6a056xb46ajivlk1l9dcq8sglab9cilki6ds1lqkg8qgdq7")))

(define-public crate-command_attr-0.5.0 (c (n "command_attr") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "089ndirkp8wq0c1s9gczq279lm9bs5i3np3g13619q4991c9fwfn")))

(define-public crate-command_attr-0.5.1 (c (n "command_attr") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1abhy1fnc68zgmaa6lk4gppr61xfwvp61jvx9xxnn1ifl22qrw1j")))

