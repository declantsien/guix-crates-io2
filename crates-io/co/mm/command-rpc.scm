(define-module (crates-io co mm command-rpc) #:use-module (crates-io))

(define-public crate-command-rpc-0.1.0 (c (n "command-rpc") (v "0.1.0") (h "01ciqk6yxivn6xn65p7nba0lnkwzdiwaqdwngqr2360yprcb59pg")))

(define-public crate-command-rpc-0.1.1 (c (n "command-rpc") (v "0.1.1") (h "0izc0fs5a1mi6684ffbdgjsnms01phxz6j20k3nwsanyfrdjd5xs")))

(define-public crate-command-rpc-0.1.2 (c (n "command-rpc") (v "0.1.2") (h "066ppyzf1mp06j8mb9nzbix71nkwvin81fci3a6ghs2jbx99zlxs")))

(define-public crate-command-rpc-0.1.3 (c (n "command-rpc") (v "0.1.3") (h "09wh69rrsfxrmsqy2d72zyskwdw4asa1z04zfdvprid8l1p2s9mz")))

(define-public crate-command-rpc-0.1.4 (c (n "command-rpc") (v "0.1.4") (h "0ylb90gg7gdbfg4sjq9njf1qj4lnggrk36hzpycfpsz4d0r7qvss")))

(define-public crate-command-rpc-0.1.5 (c (n "command-rpc") (v "0.1.5") (h "12kwmpfd5x6h1vaw8aggn0z8sx38r3wbk8y34whzn822ypirzz2v")))

(define-public crate-command-rpc-0.1.6 (c (n "command-rpc") (v "0.1.6") (h "03lv1ir4xqwszk4m4avs4k1qssxc3rxjrh1sjcaa1s5ivbch4z5q")))

(define-public crate-command-rpc-0.1.7 (c (n "command-rpc") (v "0.1.7") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1c7aqpk48p8w47w6fn21vzpgr2dv55a8i50ck9z5mi5fkyr9jqrn")))

(define-public crate-command-rpc-0.1.8 (c (n "command-rpc") (v "0.1.8") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "16s8bg501339w1gfs60rr2nrvpzv3klc9ka5kllfsz6iicnbgnif")))

(define-public crate-command-rpc-0.1.9 (c (n "command-rpc") (v "0.1.9") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "10jrjdf31r9vcgqc2yv6sqz82qqqbd66z1n5vpa6ij1czhph89p3")))

(define-public crate-command-rpc-0.1.10 (c (n "command-rpc") (v "0.1.10") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "05c812vrpcppzldahgg67w5s6i12xfb3vkli5kq77fjr7jpyb398")))

(define-public crate-command-rpc-0.1.11 (c (n "command-rpc") (v "0.1.11") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0v3wimv6rghr4k72n2fr9m321lj8jaxwxizrj6kmnwia38rfsxca")))

(define-public crate-command-rpc-0.1.12 (c (n "command-rpc") (v "0.1.12") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1g5x5pl2ffj538n28msxsq5c1lyaz108l4jjx20f0kvnsbyghpby")))

