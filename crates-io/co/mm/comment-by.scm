(define-module (crates-io co mm comment-by) #:use-module (crates-io))

(define-public crate-comment-by-0.1.0 (c (n "comment-by") (v "0.1.0") (d (list (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1dqq1fg77hwnbsqc3qq8l1ibscc28skzqz2yjqi9jpszklmna0z5") (y #t)))

(define-public crate-comment-by-0.1.1 (c (n "comment-by") (v "0.1.1") (d (list (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "09xml65j0chy8782nfbk7p24pbyirvknkrzr188sqg40h09prqsi") (y #t)))

(define-public crate-comment-by-0.1.2 (c (n "comment-by") (v "0.1.2") (d (list (d (n "encoding_rs_io") (r "^0.1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "00prznwgk6r92yrra11c5zp9rvpvqnxkr86d02amqwfp4r7xbvwv")))

