(define-module (crates-io co mm committer) #:use-module (crates-io))

(define-public crate-committer-0.1.0 (c (n "committer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (f (quote ("editor"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "0hfyi6zhwnn8bbma1mnpd4pchkhma9zw1xykfg3dbly4i806vzar")))

(define-public crate-committer-0.1.1 (c (n "committer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (f (quote ("editor"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "1k0q5a83s89b8phjchs200yixfghr6dv2smd217zr859z0aw3svd")))

(define-public crate-committer-0.2.0 (c (n "committer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (f (quote ("editor"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "149glg71kbp82cxmqjhx72ds3hrgs02yc3a6nzmrxrqrjc44mz3i")))

(define-public crate-committer-0.3.0 (c (n "committer") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.7") (f (quote ("editor"))) (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ihabyxk2yhmx2mz8pc8pvblrl65p3dy6fj8v0hjdad91dcp3lzf")))

(define-public crate-committer-0.4.0 (c (n "committer") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.7") (f (quote ("editor"))) (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wq602hikclsgvdzdwwhf4593fw5i633jv9c2xvnw26qzhd5m592")))

