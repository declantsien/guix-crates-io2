(define-module (crates-io co mm commonregex) #:use-module (crates-io))

(define-public crate-commonregex-0.1.0 (c (n "commonregex") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15h1disk9rw7wwvk1ns1fawxsz5hcma69ikbwvjpkk312f53s80r") (y #t)))

(define-public crate-commonregex-0.1.1 (c (n "commonregex") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "160sp3x1f2v4b95j3hf8s27j7flhxahq41wlmrxaqqpw3p99fqmc") (y #t)))

(define-public crate-commonregex-0.1.2 (c (n "commonregex") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xply1fnvbjb7pwl24w5g2dm897prb0kmp816dmqdc75vcm81i36") (y #t)))

(define-public crate-commonregex-0.1.3 (c (n "commonregex") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02jn9x4hcqb4phirczsmw7nzvi7xb6i4kn1c2v62akbfvpk02q3p") (y #t)))

(define-public crate-commonregex-0.1.4 (c (n "commonregex") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0s9g96nka908265kslc0mj7pq75m8rr9yzbsviyyk1bkajb16jv2") (y #t)))

(define-public crate-commonregex-0.2.0 (c (n "commonregex") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10bgwf73hsfp2vbp1vcs75kbigyvypfpxslbk99b6fh70gqali5p")))

