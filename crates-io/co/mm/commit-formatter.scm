(define-module (crates-io co mm commit-formatter) #:use-module (crates-io))

(define-public crate-commit-formatter-0.1.0 (c (n "commit-formatter") (v "0.1.0") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)))) (h "1ic7dycd7bwqskixqvp8m4nk934xvgwihs6qr04mrfmdariyr4sd")))

(define-public crate-commit-formatter-0.1.1 (c (n "commit-formatter") (v "0.1.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)))) (h "088q83afszdxvg5279xn14k5nkz7j9crxy0c6lg4w9avw2h6wyfa")))

(define-public crate-commit-formatter-0.2.0 (c (n "commit-formatter") (v "0.2.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "138qv74gnrv9spzzskqf2s984lmiil9mxjww3xw851gm4l38v66v")))

(define-public crate-commit-formatter-0.2.1 (c (n "commit-formatter") (v "0.2.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1846fg743qp50h27yxhm51ddzrzxqjcqkdznpnigxdah1xa11mn1")))

