(define-module (crates-io co mm commandlines) #:use-module (crates-io))

(define-public crate-commandlines-0.0.2 (c (n "commandlines") (v "0.0.2") (h "04pwrrh1zb65dxrwb6c6kd86431vg68jzfzvggn5iyc4ax5758zr")))

(define-public crate-commandlines-0.1.0 (c (n "commandlines") (v "0.1.0") (h "1b3cbb69wx0rjr4xnvwns92d5nsy81l22ldwxvkyd1254a8q177k")))

(define-public crate-commandlines-0.2.0 (c (n "commandlines") (v "0.2.0") (h "04ad01zwx0w07a3dcsrr94sap0h6vm7g5b8x5d2v0iz8abvxrbq6")))

(define-public crate-commandlines-0.2.1 (c (n "commandlines") (v "0.2.1") (h "1q23j0016w0dlgzwma16nah7b5ka803xyysq0byrbqbxmqnb8ncr")))

(define-public crate-commandlines-0.3.0 (c (n "commandlines") (v "0.3.0") (h "0m871dh1i0yxjnkpv0ba13vs8p8gjjkmpb0zwiplqpfdm8v5f40p")))

(define-public crate-commandlines-0.4.0 (c (n "commandlines") (v "0.4.0") (h "0i0sajpinv7vpmmx5sis4jw529qdjbia7cv0vysy0cp6g38zpp4x")))

(define-public crate-commandlines-0.5.0 (c (n "commandlines") (v "0.5.0") (h "0cpp2b85i8q57fgrgnbgq149klgijdcxmfmzvqvh1v9bb50fja0m")))

(define-public crate-commandlines-0.5.1 (c (n "commandlines") (v "0.5.1") (h "1954i2x515y6k83218d64nzdga8m3bf1zppzw3km4kizsll62gfd")))

(define-public crate-commandlines-0.5.2 (c (n "commandlines") (v "0.5.2") (h "1xncpsylrk53xhvczdbgcpjwifyzlj9wz4d67rwcl62md7vvmfxq")))

(define-public crate-commandlines-0.6.0 (c (n "commandlines") (v "0.6.0") (h "0yrgsjs4m69krd2b7fl44nkb0y4d333r02ymkk5zvwnpzp49ghjz")))

(define-public crate-commandlines-0.7.0 (c (n "commandlines") (v "0.7.0") (h "0zmfs825m13hk4qpnn32d2cwakw65g7p20ymaqq45i0k57zpnpbn")))

(define-public crate-commandlines-0.8.0 (c (n "commandlines") (v "0.8.0") (h "1gsh8ivqw9an6q7q0jbzxxjhhzl1rr16ljc8rd5rrd2ix1f81blv")))

