(define-module (crates-io co mm commonplace) #:use-module (crates-io))

(define-public crate-commonplace-0.1.0 (c (n "commonplace") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c61gmzxz7zx81x00610vhjbyz39lbmx0p0hda5051f7qa9xinys")))

