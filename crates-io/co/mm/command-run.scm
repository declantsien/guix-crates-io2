(define-module (crates-io co mm command-run) #:use-module (crates-io))

(define-public crate-command-run-0.9.0 (c (n "command-run") (v "0.9.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0ln095yi96yy2cvx582lkf4zw7las0krrxzivcnhsd7l38habxr5") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-0.10.0 (c (n "command-run") (v "0.10.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1j0w1wfscn4ffwwk09764fnc27saw5x77kw7092z4yanf5akhd61") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-0.11.0 (c (n "command-run") (v "0.11.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "11491y5kv364s8s77zw56890w1dk2sawqwjcfra8gp4f0gvmcy5k") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-0.12.0 (c (n "command-run") (v "0.12.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0lvvvjw551glsy8f2bq5wd6wc6sgf6y0ndk717zbqyf9lxjkmak7") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-0.12.1 (c (n "command-run") (v "0.12.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0yn9zw359wwdljjdxk84mh95asrnwra1ngwhkpg9fl9hrjx4nqc8") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-0.12.2 (c (n "command-run") (v "0.12.2") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1vzvxl25d8qrr96xdfx46i5fg1vxa5xx6k03h8cvnv9az3lk82i0") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-0.12.3 (c (n "command-run") (v "0.12.3") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0r9skl1vryrga5q570blgh46zm6wsyx4734dvqa8mfa6cg51habn") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-0.13.0 (c (n "command-run") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 2)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1bp7q1x44hxy5dp2w04zy8f49kk00qhl81msxp9xbkdv12qx32cx") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-1.0.0 (c (n "command-run") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 2)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1m8m877qj1q8l5q94g4z71mpvm4n82wwbaq7162m8wfkkpwr4w35") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-1.0.1 (c (n "command-run") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 2)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "083w66rvzaj6jb8dgfa40dyxjv8qqrbjv26nc1d2dr7ykf35rajh") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-1.0.2 (c (n "command-run") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 2)) (d (n "os_pipe") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "024bpbrx4bxzdn79cb11my7dfalc6nvk7q1bdfmlcjhxws7iq9mr") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-1.0.3 (c (n "command-run") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "os_pipe") (r "^0.9.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "124av8z6d5zpzdvvn6iisc98mlq37jjklp9zgsbb48s2jvhl6bkp") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-1.1.0 (c (n "command-run") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.46") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "os_pipe") (r "^0.9.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1m11xvmdzs3hamyhih8lvfsynk9byb02361apr4g08vdynifyj2z") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-1.1.1 (c (n "command-run") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "os_pipe") (r "^0.9.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0vmb88k4gz65hpqsjj9mvingdc7l0h4wpa7i2ijzcdbps4kgkngz") (f (quote (("logging" "log") ("default" "logging"))))))

(define-public crate-command-run-1.1.2 (c (n "command-run") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "os_pipe") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1arc70js5fa0kasakgix6l1ygry9iw94n95h4vsfsqrjbk6lxcp2") (f (quote (("logging" "log") ("default" "logging"))))))

