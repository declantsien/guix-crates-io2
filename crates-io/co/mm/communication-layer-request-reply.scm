(define-module (crates-io co mm communication-layer-request-reply) #:use-module (crates-io))

(define-public crate-communication-layer-request-reply-0.2.0-rc-6 (c (n "communication-layer-request-reply") (v "0.2.0-rc-6") (h "117xpkib06p13546xizr54n3fxkmj0zi90grdgqp62wpnmp5qp0p")))

(define-public crate-communication-layer-request-reply-0.2.0-rc-7 (c (n "communication-layer-request-reply") (v "0.2.0-rc-7") (h "181xyyd0slc8cn90mq73gwb9ygja20f9q29h9pqvl5vv9f7r308b")))

(define-public crate-communication-layer-request-reply-0.2.0-rc-8 (c (n "communication-layer-request-reply") (v "0.2.0-rc-8") (h "1b5s5h4w2jv7sd0jl6hsm8yfpywz29x53ibp7mzinza9j18xpmwd")))

(define-public crate-communication-layer-request-reply-0.2.0-rc-9 (c (n "communication-layer-request-reply") (v "0.2.0-rc-9") (h "0y9kwv82vrqb5729lyg91sdrvrjf4rf10apkrvzcs3p9x32ih5jy")))

(define-public crate-communication-layer-request-reply-0.2.0-rc-10 (c (n "communication-layer-request-reply") (v "0.2.0-rc-10") (h "072pk627gp7xb3xbgwiq3jhixaxyrsp4pj6nla2sg931753n86rn")))

(define-public crate-communication-layer-request-reply-0.2.0-rc-11 (c (n "communication-layer-request-reply") (v "0.2.0-rc-11") (h "0x1653ln371k6v2fqci8c6ygi78yy7yxvvah7wgj5wnz17dj54fk")))

(define-public crate-communication-layer-request-reply-0.2.1-rc (c (n "communication-layer-request-reply") (v "0.2.1-rc") (h "0lg27s32d0xvpshc78bzvmqxgf47y4wi97ijg92ckk6cc5hx95pz")))

(define-public crate-communication-layer-request-reply-0.2.1 (c (n "communication-layer-request-reply") (v "0.2.1") (h "1lqs0hllpbkn5c0g159q2xkqai347b3mbcmd3dywia3l2cqy9c2f")))

(define-public crate-communication-layer-request-reply-0.2.2-rc (c (n "communication-layer-request-reply") (v "0.2.2-rc") (h "0r8b1qmlg0y0wbqm38f3bl01s1d2ixpzlvgl3i9k60dc5csx2w2m")))

(define-public crate-communication-layer-request-reply-0.2.2-rc-2 (c (n "communication-layer-request-reply") (v "0.2.2-rc-2") (h "05asfvg2d4kl1kq9ax3ixlpc3ggrjv7418q9hcv7rjqgkzn42wqg")))

(define-public crate-communication-layer-request-reply-0.2.2 (c (n "communication-layer-request-reply") (v "0.2.2") (h "127ayiqsza7cj7rfg998g6qmfbbw1xmf6i0x0q14gan0jmiqqa83")))

(define-public crate-communication-layer-request-reply-0.2.3-rc (c (n "communication-layer-request-reply") (v "0.2.3-rc") (h "1j1nk0n2al8zfla9ir6iqad4r1lybx26qnw8kq3a3i2l55gmn637")))

(define-public crate-communication-layer-request-reply-0.2.3-rc2 (c (n "communication-layer-request-reply") (v "0.2.3-rc2") (h "1aar9w35jydxi26zhwd5hcwy25azn7pmqgpw0zmhnljl77hah7pk")))

(define-public crate-communication-layer-request-reply-0.2.3-rc4 (c (n "communication-layer-request-reply") (v "0.2.3-rc4") (h "1q24pdngb0nja0lrb2r0ywd2h5mdam3arsddic4g3f3lvf9162bi")))

(define-public crate-communication-layer-request-reply-0.2.3-rc5 (c (n "communication-layer-request-reply") (v "0.2.3-rc5") (h "1zn2dcich6b6f58881l2f79ssiq2lhf0ixyphyhnjgwmwnyggbl5")))

(define-public crate-communication-layer-request-reply-0.2.3-rc6 (c (n "communication-layer-request-reply") (v "0.2.3-rc6") (h "0yc5vg8z6xpnqi4zsd2kqz2x8y2z0r22j9gdikdhpzmn62khlg7y")))

(define-public crate-communication-layer-request-reply-0.2.3 (c (n "communication-layer-request-reply") (v "0.2.3") (h "0h9ngpkx36x1gh74gnyi7qmr5izhpci5sflr5ynbsv04fv1crdgy")))

(define-public crate-communication-layer-request-reply-0.2.4-rc3 (c (n "communication-layer-request-reply") (v "0.2.4-rc3") (h "0agjn2bxqbmv8bfj4w45fx3j7h3xnngzp8bs4qraxnp1ccxk536j")))

(define-public crate-communication-layer-request-reply-0.2.4 (c (n "communication-layer-request-reply") (v "0.2.4") (h "0fycliby9whi2n6q6ki7wlga6hygvnqln8ai8ca73mvyv0ls45ph")))

(define-public crate-communication-layer-request-reply-0.2.5-alpha.1 (c (n "communication-layer-request-reply") (v "0.2.5-alpha.1") (h "1q68maxikp7ny5ngy0g04myy33pl46308753sp7pahfny8ps4n6g")))

(define-public crate-communication-layer-request-reply-0.2.5 (c (n "communication-layer-request-reply") (v "0.2.5") (h "0y36xisky63xbdchlal862ilzv4jshd1hsp3s93mjl7k1nhc4bcs")))

(define-public crate-communication-layer-request-reply-0.2.6 (c (n "communication-layer-request-reply") (v "0.2.6") (h "03lbxdbv7zdqw5xdnk4hrk7gfqqa7hq427ngl6dbj1cd0gsa4g8s")))

(define-public crate-communication-layer-request-reply-0.3.0-rc (c (n "communication-layer-request-reply") (v "0.3.0-rc") (h "019l48d2nf9fvlkkq55r6gfp0acr7f5plw0lfg3bk1jy5xrv8hfn")))

(define-public crate-communication-layer-request-reply-0.3.0-rc2 (c (n "communication-layer-request-reply") (v "0.3.0-rc2") (h "0nr2vfcqzap5qnh1p7xwr31ky5nckb12b9mb8qmbly5j2b6yf7a8")))

(define-public crate-communication-layer-request-reply-0.3.0-rc3 (c (n "communication-layer-request-reply") (v "0.3.0-rc3") (h "10mmx092jxarjyqq9f36s7v0bbbaxd2596cqwkmqjn2s675v0fvm")))

(define-public crate-communication-layer-request-reply-0.3.0-rc4 (c (n "communication-layer-request-reply") (v "0.3.0-rc4") (h "0v1hfr3qznc1d23y5nl30clf925m97dwn24d4s0m5n5a4a331gvd")))

(define-public crate-communication-layer-request-reply-0.3.0 (c (n "communication-layer-request-reply") (v "0.3.0") (h "06jp4g3f9bi0mj9ymqrrifmxlgg8wfjy67n2zx3qhq48qgqixy9i")))

(define-public crate-communication-layer-request-reply-0.3.1-rc2 (c (n "communication-layer-request-reply") (v "0.3.1-rc2") (h "14jid7ny1gxv64rbq0mh4dr76jv3b59cl699z8rvw0agnxd3kir8")))

(define-public crate-communication-layer-request-reply-0.3.1-rc3 (c (n "communication-layer-request-reply") (v "0.3.1-rc3") (h "1azliglbxbjh3vrahs82ca0m4mlrfzgv9vlklrapjmh0nx5k0cw7")))

(define-public crate-communication-layer-request-reply-0.3.1-rc4 (c (n "communication-layer-request-reply") (v "0.3.1-rc4") (h "0vv6zkkpn9g6244h4dviyz53z6dlivlh58w7l6yg59vxsghf27xv")))

(define-public crate-communication-layer-request-reply-0.3.1-rc5 (c (n "communication-layer-request-reply") (v "0.3.1-rc5") (h "1ibfrcjxig18sicz08zhyabrri9a28m4vwmf9mzjb47rch4wv7wg")))

(define-public crate-communication-layer-request-reply-0.3.1-rc6 (c (n "communication-layer-request-reply") (v "0.3.1-rc6") (h "04jrddg7j0941a7azl9j76sjc1cvhy9b67a03gdy4addi3pa8wnv")))

(define-public crate-communication-layer-request-reply-0.3.1 (c (n "communication-layer-request-reply") (v "0.3.1") (h "09xpp9wl5zricwhw70j37j6x9y6vrhv8v2bl4rbl9k2n97zyz4vw")))

(define-public crate-communication-layer-request-reply-0.3.2-rc1 (c (n "communication-layer-request-reply") (v "0.3.2-rc1") (h "0fjc1zk7r6rxgy9a2p2ckxha9fa113vy2dyr2lq1hi9h9nir070g")))

(define-public crate-communication-layer-request-reply-0.3.2-rc2 (c (n "communication-layer-request-reply") (v "0.3.2-rc2") (h "13wb77xl7yl5f6811jqk4abkkgh91hwdra09c9jwpblhg7ajyk7v")))

(define-public crate-communication-layer-request-reply-0.3.2 (c (n "communication-layer-request-reply") (v "0.3.2") (h "127yx8yb5k51r7zb1igw1hlw7fixbnazwd5vgsdr4y0lrw0cymw9")))

(define-public crate-communication-layer-request-reply-0.3.3-rc1 (c (n "communication-layer-request-reply") (v "0.3.3-rc1") (h "1a24mfdxx7iiyhb81096kh7nqrgdzxmgs7yl9ixir5n79m697r52")))

(define-public crate-communication-layer-request-reply-0.3.3 (c (n "communication-layer-request-reply") (v "0.3.3") (h "09mh76hm2zr9ynkwh00987pxgrnhfmzswwzc8s1sqn19iysq851n")))

(define-public crate-communication-layer-request-reply-0.3.4-rc1 (c (n "communication-layer-request-reply") (v "0.3.4-rc1") (h "1zpvqia3j1q3x079zarwpkrpqpcdardwg67aqb5psypgmj4cpsh2")))

(define-public crate-communication-layer-request-reply-0.3.4-rc2 (c (n "communication-layer-request-reply") (v "0.3.4-rc2") (h "1pnf2krdm24ssv8d581cl5jcrl5pxs6na49z4j3v7xh3a5w39rmy")))

(define-public crate-communication-layer-request-reply-0.3.4 (c (n "communication-layer-request-reply") (v "0.3.4") (h "1j503d21h99wvy2dmg0prv9wcn1fi20w730350v1q7xp3znd90sg")))

