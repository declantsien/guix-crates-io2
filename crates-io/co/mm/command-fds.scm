(define-module (crates-io co mm command-fds) #:use-module (crates-io))

(define-public crate-command-fds-0.1.0 (c (n "command-fds") (v "0.1.0") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ckwh2mydja8ljab2danj6hwiinbsfwqy9krbzyj2xxzsdh97383")))

(define-public crate-command-fds-0.2.0 (c (n "command-fds") (v "0.2.0") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1l8xyznlc9q96snq6g4bgz4kqr66vhaxivy8myc57g8pjhzwnkyx")))

(define-public crate-command-fds-0.2.1 (c (n "command-fds") (v "0.2.1") (d (list (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "04vhwmv3lk9zy5k7kqk2kfvjg0krqc5hhilp3f374hq5bnpa4kyf")))

(define-public crate-command-fds-0.2.2 (c (n "command-fds") (v "0.2.2") (d (list (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio-crate") (r "^1.0") (f (quote ("process"))) (o #t) (k 0) (p "tokio")))) (h "0s7g6c4br7wi46dcczhlcrhb1yd9qn440h0c7pmx00x4428q3k1m") (f (quote (("tokio" "tokio-crate") ("default"))))))

(define-public crate-command-fds-0.2.3 (c (n "command-fds") (v "0.2.3") (d (list (d (n "nix") (r "^0.27.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio-crate") (r "^1.32.0") (f (quote ("process"))) (o #t) (k 0) (p "tokio")))) (h "00av3dp5yhcd2vnr80lrgrnbxg9rqxhwa3kd573a7g7pak4z747i") (f (quote (("tokio" "tokio-crate") ("default"))))))

(define-public crate-command-fds-0.3.0 (c (n "command-fds") (v "0.3.0") (d (list (d (n "nix") (r "^0.27.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio-crate") (r "^1.32.0") (f (quote ("process"))) (o #t) (k 0) (p "tokio")))) (h "1xxd7cfjf6wc67444h67dimbl2p0xw6b95r930dp7wwb6z8ipcbv") (f (quote (("tokio" "tokio-crate") ("default"))))))

