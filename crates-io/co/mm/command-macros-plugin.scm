(define-module (crates-io co mm command-macros-plugin) #:use-module (crates-io))

(define-public crate-command-macros-plugin-0.2.0 (c (n "command-macros-plugin") (v "0.2.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "1y9f2jkx9f8dhzzm068mv3w4pbkhfr560jqpr0bszmlar5wnm52f")))

(define-public crate-command-macros-plugin-0.2.1 (c (n "command-macros-plugin") (v "0.2.1") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "1a66yxjhjg3ysyx2cnh7gm3mqan5w3qichvdj56ad8g686nndm9a")))

(define-public crate-command-macros-plugin-0.2.2 (c (n "command-macros-plugin") (v "0.2.2") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "1qlnxh4dgmy5gixdy8j9i8rn0n355vl0qq8rnb0y4nl8g7xvjjid")))

(define-public crate-command-macros-plugin-0.2.3 (c (n "command-macros-plugin") (v "0.2.3") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "09k7i6mxd2aln5afl32il2p4rfnw0zqxlx1aw4wd05ipm3a7xxqm")))

(define-public crate-command-macros-plugin-0.2.4 (c (n "command-macros-plugin") (v "0.2.4") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "0scf86k5dsdbhjxfl79p9l5v78rc5k6vy3xkm1ybwr516l6m2n4f")))

(define-public crate-command-macros-plugin-0.2.5 (c (n "command-macros-plugin") (v "0.2.5") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "0zfc9y4c70ls4fyhpx3jjr6jp4nni08zp3rkl1f09w6v0ypyni68")))

(define-public crate-command-macros-plugin-0.2.6 (c (n "command-macros-plugin") (v "0.2.6") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "1nj82bvj9iv25cn54nqx7ifym7y1adr56h2pr7s5dh4ivcx5wr2v") (y #t)))

(define-public crate-command-macros-plugin-0.2.7 (c (n "command-macros-plugin") (v "0.2.7") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)))) (h "05yix76sq809pp48xs9jbgcn2ikk3iyn53xf3mf97vrxvzgv45xz")))

