(define-module (crates-io co mm commr) #:use-module (crates-io))

(define-public crate-commr-0.1.0 (c (n "commr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "cargo-release") (r "^0.25.6") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nk2nylq1pv484wxb96gn6qzjpg5f57z7rsibyvqawmfxfb83109")))

