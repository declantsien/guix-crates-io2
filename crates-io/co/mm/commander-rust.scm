(define-module (crates-io co mm commander-rust) #:use-module (crates-io))

(define-public crate-commander-rust-1.0.1 (c (n "commander-rust") (v "1.0.1") (d (list (d (n "commander-core") (r "^1.0.0") (d #t) (k 0)) (d (n "commander-macros") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1dqh099x3038srjmq7n6k1zchnny8gwq096ndgqfqcavrl3kgxha")))

(define-public crate-commander-rust-1.1.2 (c (n "commander-rust") (v "1.1.2") (d (list (d (n "commander-core") (r "^1.1.1") (d #t) (k 0)) (d (n "commander-macros") (r "^1.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "04rrisa9nnki029kh8yiasrrbpl3yz7dcni3lx2vvj878hs5n37f")))

(define-public crate-commander-rust-1.1.3 (c (n "commander-rust") (v "1.1.3") (d (list (d (n "commander-core") (r "^1.1.3") (d #t) (k 0)) (d (n "commander-macros") (r "^1.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0hz09111mijflvpxk8pxip5xr2xy6fxnmwvyrhpgzckvxdmyy7gz")))

(define-public crate-commander-rust-1.1.4 (c (n "commander-rust") (v "1.1.4") (d (list (d (n "commander-core") (r "^1.1.3") (d #t) (k 0)) (d (n "commander-macros") (r "^1.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1ga4yxpn4rxg0a585s5afakxkyiq04mk6ymny7bw19v2p27wda8q")))

(define-public crate-commander-rust-1.1.5 (c (n "commander-rust") (v "1.1.5") (d (list (d (n "commander-core") (r "^1.1.4") (d #t) (k 0)) (d (n "commander-macros") (r "^1.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1wly08f7acqgpc3gwaxlfscb1mm2bdyk5zzpbf1h0rpf4q6y1igh")))

(define-public crate-commander-rust-1.2.0 (c (n "commander-rust") (v "1.2.0") (d (list (d (n "commander-core") (r "^1.2.0") (d #t) (k 0)) (d (n "commander-macros") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "17xcn224av7bax2y0s77ycrqpqya1vx1bhhmjsfjzd73mhw4wgwa")))

(define-public crate-commander-rust-1.2.1 (c (n "commander-rust") (v "1.2.1") (d (list (d (n "commander-core") (r "^1.2.0") (d #t) (k 0)) (d (n "commander-macros") (r "^1.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "191xzgq0ghvlmabgzagbfsx3lqbzf2fy4s4ax770jsr1gpn14h8h")))

