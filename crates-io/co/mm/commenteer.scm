(define-module (crates-io co mm commenteer) #:use-module (crates-io))

(define-public crate-commenteer-0.2.0 (c (n "commenteer") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "01mxn12m85fr8zpxnxrr08byvc0vy7ifvi03npnw5cfp30xdbscf")))

(define-public crate-commenteer-0.2.0-1 (c (n "commenteer") (v "0.2.0-1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "06s71mjmxzn0wn07m83s5y663fzsjdm0hddmhhv48iiy2ffpkj50")))

(define-public crate-commenteer-0.2.1 (c (n "commenteer") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "0z9jlmwzykmlflnv3gzaxll9pz19kprjsk9g96wyyxbm03nmkn44")))

