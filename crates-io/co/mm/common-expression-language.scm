(define-module (crates-io co mm common-expression-language) #:use-module (crates-io))

(define-public crate-common-expression-language-0.1.0 (c (n "common-expression-language") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lalrpop") (r "^0.19.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xqzdsd4sk85kv7dmk6axh9rcxx1sk544abrzjg95c2sri50b3af")))

