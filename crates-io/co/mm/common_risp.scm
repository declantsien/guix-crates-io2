(define-module (crates-io co mm common_risp) #:use-module (crates-io))

(define-public crate-common_risp-0.0.0 (c (n "common_risp") (v "0.0.0") (h "13xbdlg3q2gcd2i7idvf7fdqqjf8917swspgpmlz8813ixml3gmv")))

(define-public crate-common_risp-0.1.0 (c (n "common_risp") (v "0.1.0") (d (list (d (n "risp_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0ww6fgh2y9d95nridjnnsdqdsqgn74c6648d5lhyxfrgq2ashpgp")))

(define-public crate-common_risp-0.1.1 (c (n "common_risp") (v "0.1.1") (d (list (d (n "risp_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0f3la8ykbs0kw9g9rj2s0k7psvapbwhrinlivcivqvzhzfihvxch")))

