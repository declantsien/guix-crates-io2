(define-module (crates-io co mm command-macros) #:use-module (crates-io))

(define-public crate-command-macros-0.1.0 (c (n "command-macros") (v "0.1.0") (h "076j34v5n1zmqa8jynpvl1xs0cq2yhccxsjahx6v11h87418jgja") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.1.1 (c (n "command-macros") (v "0.1.1") (h "1v8dcfzvss7w9cnawpal571qxixrd6qy3gqqsmhi2gva69nxx67x") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.1.2 (c (n "command-macros") (v "0.1.2") (h "1wx184d8r3qa5rnq0fb6xmd1f6jjxv87wcy7kk9g51n0sb40idlf") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.1.3 (c (n "command-macros") (v "0.1.3") (h "0nz8lksgz99qmwf2ndriidsngm63nr9x7ihbrk97n8zrya89m2q2") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.1.4 (c (n "command-macros") (v "0.1.4") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "1z6g849ab1hjm2qxza19xj38l5fh3qp75mnab1nbzqnzy3hpyq2y") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.1.5 (c (n "command-macros") (v "0.1.5") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "0dwyid4ygw21bk3fnjnha9zib4r2vw92h6lq7f5cbbym491z487d") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.1.6 (c (n "command-macros") (v "0.1.6") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "1ivi0zwxs9cd0ks8qrs5j6i5qill2rhwimlk7zfv31w8ksivygc2") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.1.7 (c (n "command-macros") (v "0.1.7") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "0qlmk53nfn1li15pjgccg91bsy4yr0imp0qm9457rzg0ijr7if85") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.1.8 (c (n "command-macros") (v "0.1.8") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "0hl91kmkmirzg63l4qckb36m1x5nqspxx1wl1b8acpxdaw15v48y") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.1.9 (c (n "command-macros") (v "0.1.9") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "1lvn2kzwmvigw27ynadabyh0z9awq9iypzpaiz20l36jk9j0n114") (f (quote (("nightly")))) (y #t)))

(define-public crate-command-macros-0.1.10 (c (n "command-macros") (v "0.1.10") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "15h82hmb1flwazj15zmgwlq0j9sc4swdx36qnvxl5rihr6ifmb1x") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.1.11 (c (n "command-macros") (v "0.1.11") (d (list (d (n "rustc_version") (r "^0.1") (d #t) (k 1)))) (h "1cx6xl3ifc6hx6sn841lb6ic5xz6p4c5i7bbgj9d585gg91d9ih1") (f (quote (("nightly"))))))

(define-public crate-command-macros-0.2.0 (c (n "command-macros") (v "0.2.0") (d (list (d (n "command-macros-plugin") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0vdldc8vgqwdkspqdd4cwdmqn28f7w5afwhipnvrgwmziv8g2mqy") (f (quote (("nightly" "command-macros-plugin"))))))

(define-public crate-command-macros-0.2.1 (c (n "command-macros") (v "0.2.1") (d (list (d (n "command-macros-plugin") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0zsi6d5vv82aqjyhx2sy008hhb5nz5h0cnx2vpqzwlwzc4jw06ad") (f (quote (("nightly" "command-macros-plugin") ("dox"))))))

(define-public crate-command-macros-0.2.3 (c (n "command-macros") (v "0.2.3") (d (list (d (n "command-macros-plugin") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0aw8jm2qsgaw3if1m9m4cvg80h4mbm2inc15h9j2l6wdw1s6jkmr") (f (quote (("nightly" "command-macros-plugin") ("dox"))))))

(define-public crate-command-macros-0.2.4 (c (n "command-macros") (v "0.2.4") (d (list (d (n "command-macros-plugin") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "0hgv01vg02kx45j76xfh8x00jnpfd0z718m47yrrcmh15dhlb9yw") (f (quote (("nightly" "command-macros-plugin") ("dox"))))))

(define-public crate-command-macros-0.2.5 (c (n "command-macros") (v "0.2.5") (d (list (d (n "command-macros-plugin") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1dr30gwm4qrcvkvxc0jbjd2h40iyp98zyyjyg0ksgmzxnpggigry") (f (quote (("nightly" "command-macros-plugin") ("dox"))))))

(define-public crate-command-macros-0.2.6 (c (n "command-macros") (v "0.2.6") (d (list (d (n "command-macros-plugin") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0932qld1xsmwf34qvmmznbd4iyp315jf289j268wrivakrqapncg") (f (quote (("nightly" "command-macros-plugin") ("dox"))))))

(define-public crate-command-macros-0.2.7 (c (n "command-macros") (v "0.2.7") (d (list (d (n "command-macros-plugin") (r "^0.2.5") (o #t) (d #t) (k 0)))) (h "0ydz4faqz670yivfcvwbv5qbcs81ih8vybvwahmil1rx7awpw710") (f (quote (("nightly" "command-macros-plugin") ("dox"))))))

(define-public crate-command-macros-0.2.8 (c (n "command-macros") (v "0.2.8") (d (list (d (n "command-macros-plugin") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "09ljrmx26bxmybpvc81v3b31z92llccvybchbi9v285awqkr6k5m") (f (quote (("nightly" "command-macros-plugin") ("dox")))) (y #t)))

(define-public crate-command-macros-0.2.9 (c (n "command-macros") (v "0.2.9") (d (list (d (n "command-macros-plugin") (r "^0.2.7") (o #t) (d #t) (k 0)))) (h "0iycraxgyindrcf07d0ggca1hsjvbjh630wjhv73mvwp61dpcsi4") (f (quote (("nightly" "command-macros-plugin") ("dox"))))))

