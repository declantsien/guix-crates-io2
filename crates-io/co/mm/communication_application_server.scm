(define-module (crates-io co mm communication_application_server) #:use-module (crates-io))

(define-public crate-communication_application_server-0.1.0 (c (n "communication_application_server") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0bvxx0xn72amy8sb7hdcljqlj016h31f2wvxj5xshwy7pwv8ml0n") (y #t)))

