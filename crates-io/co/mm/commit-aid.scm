(define-module (crates-io co mm commit-aid) #:use-module (crates-io))

(define-public crate-commit-aid-0.1.0 (c (n "commit-aid") (v "0.1.0") (d (list (d (n "appendlist") (r "=1.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "0k6xp9xrdgjqlqd4q68scl2sccfbjkcjxwh3sxlamy0g1gf24v1k")))

