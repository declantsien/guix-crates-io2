(define-module (crates-io co mm commandspec) #:use-module (crates-io))

(define-public crate-commandspec-0.2.0 (c (n "commandspec") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0gwl62vaf79c87jvk5sbhyf5n9ipgc4b0wy7vx3p3wlp7mry8i3z")))

(define-public crate-commandspec-0.3.0 (c (n "commandspec") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0zb764yc996bpbhm279qr8585p1i67ryx1fr3qxwnmgjkj472m2r")))

(define-public crate-commandspec-0.4.0 (c (n "commandspec") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0alvdabm5rxgwg601wbpmd4n7aiikfm3zpzpjp7f8qnz6fcijfrm")))

(define-public crate-commandspec-0.5.0 (c (n "commandspec") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1ilcfsxjc282hmi3vmi2glb12mc5h4wgw37hgp5lkabmjjnx7xri")))

(define-public crate-commandspec-0.5.1 (c (n "commandspec") (v "0.5.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0hf534bgs9nyj8zjc4k2yyv2k0k26lq7f4nvhbhqq299qyx2mk6y")))

(define-public crate-commandspec-0.6.0 (c (n "commandspec") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0cj3qkvajvb5v9crwlybsw6l5gkz1nz0kgb2ndzikik81yiqzy3v")))

(define-public crate-commandspec-0.6.1 (c (n "commandspec") (v "0.6.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "14ww1w4s5jzqisv3skaak21vs2070h1v1pjb9y00m3rillqdrrpp")))

(define-public crate-commandspec-0.7.0 (c (n "commandspec") (v "0.7.0") (d (list (d (n "ctrlc") (r "^3.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1rz5hi2b8wgqfgb9n1zhycbj44xjmpcfzi1ah68cd0bq112p3pyw")))

(define-public crate-commandspec-0.8.0 (c (n "commandspec") (v "0.8.0") (d (list (d (n "ctrlc") (r "^3.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "0kc5a1s5hqbd5g6c6i0gsnglj9zfcmf7pvhpzwa8d7s96d3hmpd0")))

(define-public crate-commandspec-0.9.0 (c (n "commandspec") (v "0.9.0") (d (list (d (n "ctrlc") (r "^3.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1lfkw33gny0mqqbpf916lkisy87w7f301myanwah9faksgihsjfx")))

(define-public crate-commandspec-0.10.0 (c (n "commandspec") (v "0.10.0") (d (list (d (n "ctrlc") (r "^3.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1a97x3radgrmfy0jq63qh6ss9xainqrbxxl8qscy1g4p64l45sgm")))

(define-public crate-commandspec-0.11.0 (c (n "commandspec") (v "0.11.0") (d (list (d (n "ctrlc") (r "^3.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)))) (h "1s0gq9hc58dgl5vlfaniz0z1pjs3wnj7bqi61zl0b8y53wrw4vbz")))

(define-public crate-commandspec-0.12.0 (c (n "commandspec") (v "0.12.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0v18sw3ipj0j7wjq5mpvcqwkyin1bpxpxch0y9jhw0qyxds0klgs") (y #t)))

(define-public crate-commandspec-0.12.1 (c (n "commandspec") (v "0.12.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "09k01lqbsaxywlfkrh9sfz0arr91jx391s1xsa1xambwnsi2z5c1") (y #t)))

(define-public crate-commandspec-0.12.2 (c (n "commandspec") (v "0.12.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1agw0q5qn7869mjs4342wdq1456blry7ka8iihsargcxfc84h5d3")))

