(define-module (crates-io co mm comma_serde_urlencoded) #:use-module (crates-io))

(define-public crate-comma_serde_urlencoded-0.7.2 (c (n "comma_serde_urlencoded") (v "0.7.2") (d (list (d (n "form_urlencoded") (r "^1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0qcd9jad8k8mz3x13x3nvc93pnd71qp55l22ar6w8z09lvacf1ph")))

(define-public crate-comma_serde_urlencoded-0.7.3 (c (n "comma_serde_urlencoded") (v "0.7.3") (d (list (d (n "form_urlencoded") (r "^1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1inz31b5wlqbiirw9ak7byb3n6asxig36j4zxqlzxad0kha1haag")))

(define-public crate-comma_serde_urlencoded-0.7.4 (c (n "comma_serde_urlencoded") (v "0.7.4") (d (list (d (n "form_urlencoded") (r "^1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0xaq8ljyfrc3011khvbp2yv359anbq4dk8d4902aqmd0h86k6qmp")))

(define-public crate-comma_serde_urlencoded-0.7.5 (c (n "comma_serde_urlencoded") (v "0.7.5") (d (list (d (n "form_urlencoded") (r "^1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1alxfrcx7g9wbm4vlfhqivcfkgc16847jxl0bgad23i0a4a2pw5j")))

(define-public crate-comma_serde_urlencoded-0.7.6 (c (n "comma_serde_urlencoded") (v "0.7.6") (d (list (d (n "form_urlencoded") (r "^1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0iiw5cmcix2c9nmxwfjrryd32j34simqw3am114zr8iwv3ybf07m")))

(define-public crate-comma_serde_urlencoded-0.8.0 (c (n "comma_serde_urlencoded") (v "0.8.0") (d (list (d (n "form_urlencoded") (r "^1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "11zn10710x27y9jymxbvx7vy4frznxzjv94v8zlzkb68ya521blj")))

(define-public crate-comma_serde_urlencoded-0.8.1 (c (n "comma_serde_urlencoded") (v "0.8.1") (d (list (d (n "form_urlencoded") (r "^1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1bsrysfznqlnj4i8aqjqd2azkad0icrwz794h8pv3pwdxqrv1qvl")))

