(define-module (crates-io co mm commandblock) #:use-module (crates-io))

(define-public crate-commandblock-0.1.0 (c (n "commandblock") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)))) (h "15nad30n96bs300yp7ac37c7lka81pwq03pc4q7z34acgll6r4c0") (f (quote (("serde") ("java_edition") ("default" "java_edition") ("bedrock_edition")))) (y #t)))

(define-public crate-commandblock-0.1.1 (c (n "commandblock") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)))) (h "17a8lyadri828lc68cb9qj5s7jkpqir17k0ywhn3bfc2la1b233i") (f (quote (("serde"))))))

(define-public crate-commandblock-0.2.0 (c (n "commandblock") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)))) (h "12xywfgxn7vwkczlqmb2hhf6ba73klhs2rcwy0ir8j9c9izb4szj") (f (quote (("serde"))))))

(define-public crate-commandblock-0.3.0 (c (n "commandblock") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0rc2yqr0c9mr0h6rmpqcihx3d95h8w89hyhq3l38d8c5dkcr8zh4") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-commandblock-0.4.0 (c (n "commandblock") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "04r68qzcyp5rv854r8xqg8609il6gmxqricyskj79syiydhidifd") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-commandblock-0.4.1 (c (n "commandblock") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1200fcilzj514vch3c07mm3dp4pswv5s9znjq7177fypm93sx0b5") (s 2) (e (quote (("serde" "dep:serde"))))))

