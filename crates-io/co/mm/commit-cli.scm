(define-module (crates-io co mm commit-cli) #:use-module (crates-io))

(define-public crate-commit-cli-0.1.0 (c (n "commit-cli") (v "0.1.0") (d (list (d (n "appendlist") (r "=1.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "0665kdvz8x7vzjcdsnnl8sqp7760ygm1ksfg3d6wmx8biihrp94a")))

