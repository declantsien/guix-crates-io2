(define-module (crates-io co mm comma) #:use-module (crates-io))

(define-public crate-comma-0.1.0 (c (n "comma") (v "0.1.0") (h "1jr8c5xj57jvvlivq5jmxcb0aax4z9j98b0c8k4id71kilr34p31")))

(define-public crate-comma-0.1.1 (c (n "comma") (v "0.1.1") (h "09zvxldmpmrvs7r9ad1iirvvzg4sj6pmb53hps1wrg5hbxc4vgcx")))

(define-public crate-comma-0.1.2 (c (n "comma") (v "0.1.2") (h "1rcjyqswg9nvj442wlzmn59vmyfslylngmqb8w7r3zigad8parwn")))

(define-public crate-comma-1.0.0 (c (n "comma") (v "1.0.0") (h "0dfk3ybjspncy93crzyv5jdk06llckzrg9cy96arx7sf3d3p5djm")))

