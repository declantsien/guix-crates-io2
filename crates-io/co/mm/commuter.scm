(define-module (crates-io co mm commuter) #:use-module (crates-io))

(define-public crate-commuter-0.1.0 (c (n "commuter") (v "0.1.0") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0vnwb0zfg004kbbzpry461h3pw0rv0mfbkxg1jx98gi3k45k3cf0")))

(define-public crate-commuter-0.1.1 (c (n "commuter") (v "0.1.1") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1vv6sxsld6fm8gjfkidky4q05w0glm6xskzk9adjgsx5rclbylpm")))

(define-public crate-commuter-0.1.2 (c (n "commuter") (v "0.1.2") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "16irrja3ihvhl3k9g8pfqr3qxb0a0vq3l01rbbyjsaksqvb4fhb3")))

(define-public crate-commuter-0.1.3 (c (n "commuter") (v "0.1.3") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "066l8n8bxq22azsmdcdqb96iy6nry6slai4b0mg3f3ywpkkvqwzw") (y #t)))

(define-public crate-commuter-0.1.4 (c (n "commuter") (v "0.1.4") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "18ji8mhlycvp52p9hjh6234c8wcfgaqi80davyp64bw896y8c4f8") (y #t)))

(define-public crate-commuter-0.1.5 (c (n "commuter") (v "0.1.5") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "0m6abbqiq45q0yacmdjchs4s080m5qwhpav20y9ikq1gpq1z96k8") (y #t)))

(define-public crate-commuter-0.1.6 (c (n "commuter") (v "0.1.6") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1zxq99fdv7324fpzwl9cyqylac1c9c2shzd944pwpa0nrkbdlkbl")))

(define-public crate-commuter-0.1.7 (c (n "commuter") (v "0.1.7") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1wq5v8drifq9kbkmvhka53gnnmf9wjbiprrk8yc0bdarwb4k8xiz")))

(define-public crate-commuter-0.1.8 (c (n "commuter") (v "0.1.8") (d (list (d (n "dyn-clonable") (r "^0.9.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1dyn7bxlhwpdb7c8ampfm1mkb0ab4al0w5rwyb748fgkc58gsbk9")))

