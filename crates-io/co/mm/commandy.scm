(define-module (crates-io co mm commandy) #:use-module (crates-io))

(define-public crate-commandy-0.1.0 (c (n "commandy") (v "0.1.0") (d (list (d (n "buildinfy") (r "^0.1.0") (d #t) (k 0)) (d (n "commandy_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1qzhs48vkadw1mc0f8nzna0hqr78n4fmclhqvys6889djxa1dx1n")))

(define-public crate-commandy-0.1.1 (c (n "commandy") (v "0.1.1") (d (list (d (n "buildinfy") (r "^0.1") (d #t) (k 0)) (d (n "commandy_macros") (r "^0.1") (d #t) (k 0)))) (h "0gfrgc181j8m9yqsdymvfjhbcfm52w04ya7d100cns1ybawy1q5s")))

(define-public crate-commandy-0.1.2 (c (n "commandy") (v "0.1.2") (d (list (d (n "buildinfy") (r "^0.1") (d #t) (k 0)) (d (n "commandy_macros") (r "^0.1") (d #t) (k 0)))) (h "1srn6p1wpzkdfk5kaj8q0fqw0lx523xqj6v1vpjh0y4hv6c6k88f")))

(define-public crate-commandy-0.2.0 (c (n "commandy") (v "0.2.0") (d (list (d (n "buildinfy") (r "^0.1") (d #t) (k 0)) (d (n "commandy_macros") (r "^0.2") (d #t) (k 0)))) (h "1iyz5bcjqw3g6nlc3kfix9l0rv9gpyw2a4ijzqbb0dqjz135kx9s")))

(define-public crate-commandy-0.2.1 (c (n "commandy") (v "0.2.1") (d (list (d (n "buildinfy") (r "^0.1") (d #t) (k 0)) (d (n "commandy_macros") (r "^0.2.1") (d #t) (k 0)))) (h "0h49fic1lky8ifkzblghf90s9z3a26iqpgly32a44yk75733ns1f")))

(define-public crate-commandy-0.2.2 (c (n "commandy") (v "0.2.2") (d (list (d (n "buildinfy") (r "^0.1") (d #t) (k 0)) (d (n "commandy_macros") (r "^0.2.2") (d #t) (k 0)))) (h "1wslfyfzwqdc7r54nciy0jds5p42391fqhxvw6pg5q6j6z2yl2k0")))

