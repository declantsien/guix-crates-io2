(define-module (crates-io co mm command-rusage) #:use-module (crates-io))

(define-public crate-command-rusage-1.0.0 (c (n "command-rusage") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "1pf4nplz03p2j79a8ss6kizqx7bzjpbsvsnw4z5ghwlhzpig2qvz")))

(define-public crate-command-rusage-1.0.1 (c (n "command-rusage") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "05rwiajmckglsa7mwpylx94ydzr0m4vv29j4xi94ccq5lrnpj6ly")))

