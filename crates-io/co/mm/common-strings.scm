(define-module (crates-io co mm common-strings) #:use-module (crates-io))

(define-public crate-common-strings-0.1.0 (c (n "common-strings") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fxbkyk532p2i0rklmfahval67hf1qc99hlg0bcn0g67i1g5cw03") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-common-strings-0.1.1 (c (n "common-strings") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11xfac93svngvj7rn1r4j3z0j6szmx9bkwbm838l08hl7cczl0yq") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-common-strings-0.1.2 (c (n "common-strings") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1f73wpcp91rnv2fsg530aagd74dyzd2lgkh633wz5g4s0scwav7d") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

