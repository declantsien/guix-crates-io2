(define-module (crates-io co mm common-utils) #:use-module (crates-io))

(define-public crate-common-utils-0.1.0 (c (n "common-utils") (v "0.1.0") (h "1x9081ywwahbiai5k6vnhgdgw07i2i6wb2c9bvsdcpd7ilg1y8pb") (y #t)))

(define-public crate-common-utils-0.1.1 (c (n "common-utils") (v "0.1.1") (h "1i2hsqsm0safs5ldx99jp3saa3wjjjcq6yz51nh2rqmyai3g4yfr")))

(define-public crate-common-utils-0.1.2 (c (n "common-utils") (v "0.1.2") (h "05whw2c2yzlg1gnnh5yr7lnvd3lm8hj3215s92vf5pa7cxw4bpbs")))

