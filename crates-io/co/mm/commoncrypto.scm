(define-module (crates-io co mm commoncrypto) #:use-module (crates-io))

(define-public crate-commoncrypto-0.1.0 (c (n "commoncrypto") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "commoncrypto-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 2)))) (h "1zv7y24jjaqxng9ib275ngqcq9935haj3qczgmikcld4fsgxxyy9") (f (quote (("lint" "clippy"))))))

(define-public crate-commoncrypto-0.2.0 (c (n "commoncrypto") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "commoncrypto-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 2)))) (h "01whnqcziclsj1gwavvqhrw2r5cmwh00j2fbc56iwnm2ddcahmnh") (f (quote (("lint" "clippy"))))))

