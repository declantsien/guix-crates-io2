(define-module (crates-io co mm commons) #:use-module (crates-io))

(define-public crate-commons-0.1.0 (c (n "commons") (v "0.1.0") (h "0x5rrpl7mh2glpsw6ic6cyrrl96fycgmyxginpi537wdi5qwr7l1") (f (quote (("sort") ("default" "collections" "sort") ("collections")))) (y #t)))

(define-public crate-commons-0.2.0 (c (n "commons") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "00aqv9slikscbpxf32mc4ghflxgvw37gv15yqjrfvxpchnniryv0") (f (quote (("sort") ("default" "collections" "sort") ("collections")))) (y #t)))

(define-public crate-commons-0.3.0 (c (n "commons") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1sx422rbvzwvj7b6zxgm0n91hv7xps3wbrvwhz3axj6yxg3j40fq") (f (quote (("sort") ("math") ("default" "collections" "sort" "math") ("collections")))) (y #t)))

