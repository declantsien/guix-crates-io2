(define-module (crates-io co mm communication-layer-pub-sub) #:use-module (crates-io))

(define-public crate-communication-layer-pub-sub-0.2.0-rc-6 (c (n "communication-layer-pub-sub") (v "0.2.0-rc-6") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1kyggind9gnhlikla59048lkjh21g5i3ddgvl6sb5mln8idq8j8s") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.0-rc-7 (c (n "communication-layer-pub-sub") (v "0.2.0-rc-7") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0zvs60dkjbw0jl4bplkf65gjh6s8ci0x525k9lmc8yyv8m7xjsbx") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.0-rc-8 (c (n "communication-layer-pub-sub") (v "0.2.0-rc-8") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "05vkh5ikvphjw8zabhg98wf75adx4xky3d6j1w6dwr1jmwwv7f3r") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.0-rc-9 (c (n "communication-layer-pub-sub") (v "0.2.0-rc-9") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "12qkbqfvwnmrncq7627z9f6mscv4071vjaqvrnlgcd9qlcisac2w") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.0-rc-10 (c (n "communication-layer-pub-sub") (v "0.2.0-rc-10") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0fv3d5kaywwdzqrx7gmlds0mm0jw6584xgy27pxrlsrhs911miv1") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.0-rc-11 (c (n "communication-layer-pub-sub") (v "0.2.0-rc-11") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0vhczvx5bgxcv1xxa3jiakf9r86klfg1rwmxmax6p91hx8qnjfm0") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.1-rc (c (n "communication-layer-pub-sub") (v "0.2.1-rc") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0i5gz3gajypcnzkw58mr4d3i4867cnl47pzdr2lacdvll2fh2i3r") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.1 (c (n "communication-layer-pub-sub") (v "0.2.1") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1z2lbpv9pgyws6234qw9hai6nxv5zylb00p8zvkviqd9ja358i5c") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.2-rc (c (n "communication-layer-pub-sub") (v "0.2.2-rc") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "01985c5w573fnwzc62krilckkyldradxk48inphmmnsqz1zl669r") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.2-rc-2 (c (n "communication-layer-pub-sub") (v "0.2.2-rc-2") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0v225vl8a9s8i38s4gry2blrjkyzqd2s15zjfqfgnhskjsawggsk") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.2 (c (n "communication-layer-pub-sub") (v "0.2.2") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1b5drbq3jdq77d3kx4ww43ywzsc032112p0jhvy4j8p4mjzx12ws") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.3-rc (c (n "communication-layer-pub-sub") (v "0.2.3-rc") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0h9nj48pnxa9qimv4spqs0383dyxk6a1fbgfcjvvkgzg61nasf5w") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.3-rc2 (c (n "communication-layer-pub-sub") (v "0.2.3-rc2") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0h209v33rb1bvqn1gqb8x731km1maib2ry70jj2w42qvh1hzfmbb") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.3-rc4 (c (n "communication-layer-pub-sub") (v "0.2.3-rc4") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "03grz5nfjfqjg7gqmj2jl865pxcm13a930j8fqzwwzzhnqn3q6gi") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.3-rc5 (c (n "communication-layer-pub-sub") (v "0.2.3-rc5") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0rnxadn6chgfdvzn08gdz36sp2ybpqr887g56sy2ync36hl5c4sd") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.3-rc6 (c (n "communication-layer-pub-sub") (v "0.2.3-rc6") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "158158ivb03qfv9ij6rkb46l1gw67sdzxr2ccyba4bp5k6j5xa6z") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.3 (c (n "communication-layer-pub-sub") (v "0.2.3") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0y7c1c8xp1r4cp3zm85k3rc7n2a068wkblaznls819xahm8mw0sw") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.4-rc3 (c (n "communication-layer-pub-sub") (v "0.2.4-rc3") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0q5npkabxazmqxy6xn7kvsd1zp6cq6jyr4bb560iy1glnsl4lgcf") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.4 (c (n "communication-layer-pub-sub") (v "0.2.4") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1pwn1xss7h24395gs6g0f3qj0wkcfbjxw6lr0krjp937gsl7agsg") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.5-alpha.1 (c (n "communication-layer-pub-sub") (v "0.2.5-alpha.1") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1qv05isjd9lfxyj9knc3iy4fmcdq61bvz6rxy1jx4nmsv8c6gz60") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.5 (c (n "communication-layer-pub-sub") (v "0.2.5") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "08ciicyh2dwwmwvbylizb6dkm2radb64s1gxn1fc8dql3l9qm2iw") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.2.6 (c (n "communication-layer-pub-sub") (v "0.2.6") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0d6gc3cfdcj3i4w95xwylzxwnh0llb6p4lw6a9bf63q8ka50h6hq") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.0-rc (c (n "communication-layer-pub-sub") (v "0.3.0-rc") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1s8ci9rbcalcza97yfn7v5g06hnivrf4ck1771nna07q2ms46vha") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.0-rc2 (c (n "communication-layer-pub-sub") (v "0.3.0-rc2") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0b2xh7af5im6kgsgy24ai0wfwl9kbjghwicc6sfwnz3vffyv3258") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.0-rc3 (c (n "communication-layer-pub-sub") (v "0.3.0-rc3") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "15c52mmli39zj1mybnlxkpdvvvvshrbrnrzxghv707ly23gsij2k") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.0-rc4 (c (n "communication-layer-pub-sub") (v "0.3.0-rc4") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0c0m3rgysl5rx0fl0mf3msp5w0yc9jl6az2a0fbd8wqb694ndh6n") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.0 (c (n "communication-layer-pub-sub") (v "0.3.0") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1z84y3g2ma20nlz9izb2rpj2fhf4k8i3pf2lwmv5bkg1vsigl572") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.1-rc2 (c (n "communication-layer-pub-sub") (v "0.3.1-rc2") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0i7x481pa5xi5niy652hfvv89q5znr0x24dd3jczdyp8iani8hl9") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.1-rc3 (c (n "communication-layer-pub-sub") (v "0.3.1-rc3") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0xwlqhkdjlzdl2pkfwsah9c3mwp74iz43hj0iswr8b092am5v3hd") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.1-rc4 (c (n "communication-layer-pub-sub") (v "0.3.1-rc4") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1a21bwxbsp42n580gf4dhwy7qq9a98hgmkk6swirsg9wshic1a25") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.1-rc5 (c (n "communication-layer-pub-sub") (v "0.3.1-rc5") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0ffhry8436hrl4bc3d481388y5rc9i3dvp3zx0lh934vazq2i0s2") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.1-rc6 (c (n "communication-layer-pub-sub") (v "0.3.1-rc6") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0dxhg9iwqs3aggyid2kz9w1z3y28w99d7lm1k5jimgm9ghv4jmm7") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.1 (c (n "communication-layer-pub-sub") (v "0.3.1") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0l7wnwd5vzcnaidyip4mipbyf3lvjxm26xss2b1p1s92vz2d3shj") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.2-rc1 (c (n "communication-layer-pub-sub") (v "0.3.2-rc1") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1al0dag2lp0hsjiq429yag3l4kbvcrvzbn61icjqyz4j3krgdm5n") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.2-rc2 (c (n "communication-layer-pub-sub") (v "0.3.2-rc2") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0yr9kcbgj5gzgyskjif73kksia8jhdy9k6jsd0plqjzwmpxqckzm") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.2 (c (n "communication-layer-pub-sub") (v "0.3.2") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1rijfnhfylzvw98dlnzlzl4zza4c97nlsnfwbchs6n3jdvdznvyv") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.3-rc1 (c (n "communication-layer-pub-sub") (v "0.3.3-rc1") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1q9d0njbif3f75m9zpa2brcyg5nmqlnb12zkx3fwc4nkffgp0y6b") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.3 (c (n "communication-layer-pub-sub") (v "0.3.3") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1b890nswwf1wmkhm4ijk3mw0lr7vfkw1fcj0n79x958l20p01wk7") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.4-rc1 (c (n "communication-layer-pub-sub") (v "0.3.4-rc1") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1dynhm2anndxxaazqz86x2gnnqb2qg6aaw35rfjiskiamg1qi74g") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.4-rc2 (c (n "communication-layer-pub-sub") (v "0.3.4-rc2") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "1hpa7d729wyw1xhfbka3yxgddhn8ifwchl1rf0zg6dmkfxrb7lsr") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

(define-public crate-communication-layer-pub-sub-0.3.4 (c (n "communication-layer-pub-sub") (v "0.3.4") (d (list (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "zenoh") (r "^0.7.0-rc") (f (quote ("transport_tcp"))) (o #t) (d #t) (k 0)))) (h "0y2l7p48847ksgsw79ilxp9glgzjxrlghqaldkqxf5xpi7pi6fxm") (f (quote (("default" "zenoh")))) (s 2) (e (quote (("zenoh" "dep:zenoh"))))))

