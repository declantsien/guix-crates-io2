(define-module (crates-io co mm commandstream) #:use-module (crates-io))

(define-public crate-commandstream-0.1.0 (c (n "commandstream") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.11") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "ttycommand") (r "^0.1.0") (d #t) (k 0)))) (h "1a27m31a96irxgjlgsxw41374fr5gdq36xcn3d2xdz11js8kp5fr")))

(define-public crate-commandstream-0.2.0 (c (n "commandstream") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.11") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "ttycommand") (r "^0.1.0") (d #t) (k 0)))) (h "03xb40wzrf13syapc7g90wjpixma4k2fzzw1ilwzw8mw4bnrf5ki")))

