(define-module (crates-io co mm commitguard) #:use-module (crates-io))

(define-public crate-commitguard-0.1.0 (c (n "commitguard") (v "0.1.0") (d (list (d (n "config") (r "^0.13.4") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "miette") (r "^5.10.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gsrphj2n8h55mcwx17jxv6i7qn8vpdmvmf2h68qk23j6vlps9hf")))

(define-public crate-commitguard-0.2.0 (c (n "commitguard") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "config") (r "^0.13.4") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "miette") (r "^5.10.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yd1y17j9dwwnw8as8q6jh2f8xcljak8h50bpyw2ilx5p6dqqlhz")))

