(define-module (crates-io co mm common_macros) #:use-module (crates-io))

(define-public crate-common_macros-0.0.1-wip (c (n "common_macros") (v "0.0.1-wip") (h "0z0n82rp0rvipknal21iwx3qhl957yp8dabm5y20y4nv6bms69gj")))

(define-public crate-common_macros-0.1.0 (c (n "common_macros") (v "0.1.0") (h "0sacsglyli6hv83z32x94mj2df1pk60rfcicrk3pdx41vqspmgry")))

(define-public crate-common_macros-0.1.1 (c (n "common_macros") (v "0.1.1") (h "0dfxc8hn9vam5gdzbwqhjdpa35jd6qrdpy8a1zv3mp77f6fdbxpk")))

