(define-module (crates-io co mm comment_commander) #:use-module (crates-io))

(define-public crate-comment_commander-0.0.0 (c (n "comment_commander") (v "0.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0bcf789cdn75jfb2awsmlg8x9wc7m6s1q1n2ic8q6704c21zh8jl")))

