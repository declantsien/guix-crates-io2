(define-module (crates-io co mm command-engine) #:use-module (crates-io))

(define-public crate-command-engine-1.0.0-rc.1 (c (n "command-engine") (v "1.0.0-rc.1") (d (list (d (n "tokio") (r "1.*") (f (quote ("rt-multi-thread" "sync"))) (o #t) (d #t) (k 0)))) (h "1s35p77mv9mglngc6jqkf9cajplinrvspsggvr84qby73hrglbnx") (f (quote (("engine") ("default" "engine") ("async" "tokio"))))))

(define-public crate-command-engine-1.0.0 (c (n "command-engine") (v "1.0.0") (d (list (d (n "tokio") (r "1.*") (f (quote ("rt-multi-thread" "sync"))) (o #t) (d #t) (k 0)))) (h "1449d5h2qs52frizvxgd70s9fx4i39y9y18x2g01h7b8jqfwdrz5") (f (quote (("engine") ("default" "engine") ("async" "tokio"))))))

