(define-module (crates-io co mm commot) #:use-module (crates-io))

(define-public crate-commot-0.1.0 (c (n "commot") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "term-table") (r "^1.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1kvazpkzxh85vy3k4xayaq1605dg3nn5aq0zdpxi0wkpf3dj5299")))

(define-public crate-commot-0.1.1 (c (n "commot") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "term-table") (r "^1.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xgavammsh9g126r2lgm32y1qh1igas4vxxf6qmb1xhsmvi1kjyk")))

