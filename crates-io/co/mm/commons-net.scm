(define-module (crates-io co mm commons-net) #:use-module (crates-io))

(define-public crate-commons-net-0.1.0 (c (n "commons-net") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tokio-rustls") (r "^0.23.3") (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.1") (f (quote ("io"))) (d #t) (k 2)))) (h "00brqlygpd6mqn9an3935fzymfkkrkz373jg556dr95vr49krng2") (f (quote (("ftps" "tokio-rustls"))))))

