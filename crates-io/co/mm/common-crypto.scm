(define-module (crates-io co mm common-crypto) #:use-module (crates-io))

(define-public crate-common-crypto-0.1.0 (c (n "common-crypto") (v "0.1.0") (h "127ryy9ybqq05kpkn662vrj6r1mjc6nglkgh6jqxzpczrgfb524m")))

(define-public crate-common-crypto-0.2.0 (c (n "common-crypto") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1v3j83as8367zz8smc74273y3i47flfydjxjmyqi9pv1mjndkwrg")))

(define-public crate-common-crypto-0.2.1 (c (n "common-crypto") (v "0.2.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "06fg5pv3vah2pn4pzid4s2cqqr9mnh736fzsdix26v56vcyzchf5")))

(define-public crate-common-crypto-0.2.2 (c (n "common-crypto") (v "0.2.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1kmi6ii3i6a228h0ln9fpy9q9qgvwnpqkv1lxzdw1bfpi4jflw3r")))

(define-public crate-common-crypto-0.2.3 (c (n "common-crypto") (v "0.2.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1hrs2z5k43ysswr0lh5ang39q1rv7j6sw7j612r2ppnj7db3bgfk")))

(define-public crate-common-crypto-0.3.0 (c (n "common-crypto") (v "0.3.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "16p15d24bdlfz5d3q9bn3063splc0d5rzavrfib8mymv8pflq1kf")))

