(define-module (crates-io co mm comment) #:use-module (crates-io))

(define-public crate-comment-0.1.0 (c (n "comment") (v "0.1.0") (d (list (d (n "comment-strip") (r "^0.1.3") (d #t) (k 0)))) (h "1dr8dif37llm6477h7g0xnycan3is73rn1wnxn0k4lar3zf3lz7g")))

(define-public crate-comment-0.1.1 (c (n "comment") (v "0.1.1") (d (list (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "1ma86q9yk8ka8crpcna61pqngjijk4sxkmhakbngp9l0knnbwv1z")))

