(define-module (crates-io co mm common_failures) #:use-module (crates-io))

(define-public crate-common_failures-0.1.0 (c (n "common_failures") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0qny0440pp1ddqhrv5bpqs5naa7cv5f15v43vmmgknwd464fnyaj")))

(define-public crate-common_failures-0.1.1 (c (n "common_failures") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1yyqh17z3lv6lvy74f93y3880vvi24awa1ly0iz8p660g98znjn8")))

(define-public crate-common_failures-0.2.0 (c (n "common_failures") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1zwnkm47agcnwkzlmqk6f3qmy2f5fjnkly71zrivrjk54p1zmsrk")))

