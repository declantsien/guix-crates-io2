(define-module (crates-io co mm commandy_macros) #:use-module (crates-io))

(define-public crate-commandy_macros-0.1.0 (c (n "commandy_macros") (v "0.1.0") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "1dyl0z52gw0dcyf2w0q52kd62xyfw38s3mks82hiv529ijjdgy64")))

(define-public crate-commandy_macros-0.1.1 (c (n "commandy_macros") (v "0.1.1") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17sbs3jzk58pggr4fjdqq5aynwhqvf3i2kgvdmybnjz0xjgjhh6g")))

(define-public crate-commandy_macros-0.1.2 (c (n "commandy_macros") (v "0.1.2") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19i950rschl0gnm2nbbnk1rmxjxl03k76k2zx1j4rnda57iarvrn")))

(define-public crate-commandy_macros-0.2.0 (c (n "commandy_macros") (v "0.2.0") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "031d5idswv33bm422r6mpjgc9a4vlx75xr1jc142qscvyhb0hzjp")))

(define-public crate-commandy_macros-0.2.1 (c (n "commandy_macros") (v "0.2.1") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12lqhc42g0ni7yx5a4420kk2066p9xl2f44zgvk0nza6mydp7ppa")))

(define-public crate-commandy_macros-0.2.2 (c (n "commandy_macros") (v "0.2.2") (d (list (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0625r9iilpk9m0ba7nixkipfdvjfyz20rra5h4y2f2nvck221ab8")))

