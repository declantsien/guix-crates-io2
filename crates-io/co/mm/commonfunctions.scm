(define-module (crates-io co mm commonfunctions) #:use-module (crates-io))

(define-public crate-commonfunctions-0.1.0 (c (n "commonfunctions") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gxs5sw68d519ijj19jcqxyq44a0wxn5wg3vls0qariialcxc0k2")))

