(define-module (crates-io co mm commcid) #:use-module (crates-io))

(define-public crate-commcid-0.1.0 (c (n "commcid") (v "0.1.0") (d (list (d (n "cid") (r "^0.2") (d #t) (k 0) (p "forest_cid")) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0m2f9b7lz7hg0acwnc8blgy81f3cmf0ghbhya2z7h2i6halrzhrn")))

(define-public crate-commcid-0.1.1 (c (n "commcid") (v "0.1.1") (d (list (d (n "cid") (r "^0.3") (d #t) (k 0) (p "forest_cid")) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "12yrpxypj9mxq7ay0pnnxgd6dlgh00bzcbbz44wgz4m4y8sncs3y")))

