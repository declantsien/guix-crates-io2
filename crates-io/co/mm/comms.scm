(define-module (crates-io co mm comms) #:use-module (crates-io))

(define-public crate-comms-0.1.4 (c (n "comms") (v "0.1.4") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "12znf7f5lxz3sfks8h6c8ip3czn0jmzgfbbf50i7im3qap97s02m") (f (quote (("dev" "clippy") ("default"))))))

