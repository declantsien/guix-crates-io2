(define-module (crates-io co mm common-testing) #:use-module (crates-io))

(define-public crate-common-testing-0.1.0 (c (n "common-testing") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (f (quote ("std" "alloc"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("attributes"))) (k 0)))) (h "1907mxlmqblr7w8z0w1qwlgn3yir8s56k3yaqaci9sn9iynjcsjp")))

(define-public crate-common-testing-0.2.0 (c (n "common-testing") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (f (quote ("std" "alloc"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("attributes"))) (k 0)))) (h "0f51bynr802r6hqnsqd2856ar0dcjggiw96za6w0b3frr6ws2vwi")))

(define-public crate-common-testing-1.0.0 (c (n "common-testing") (v "1.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (f (quote ("std" "alloc"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("attributes"))) (k 0)))) (h "05j6jm2xls845srwaz14znrg85zpvwmrb3p6wm8p8kiab6835shg")))

(define-public crate-common-testing-1.1.0 (c (n "common-testing") (v "1.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (f (quote ("std" "alloc"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("attributes"))) (k 0)))) (h "1ynh0w2vpaz365ph497npc3dv4ydn1dxb5g79dhs630y4d323agx")))

(define-public crate-common-testing-1.1.1 (c (n "common-testing") (v "1.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (f (quote ("std" "alloc"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("attributes"))) (k 0)))) (h "1sww7c4qscc45vr6j7qdsj0qd3v3s4vwl9rnsvaqd5125ngmvngl")))

