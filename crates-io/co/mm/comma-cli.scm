(define-module (crates-io co mm comma-cli) #:use-module (crates-io))

(define-public crate-comma-cli-0.1.0 (c (n "comma-cli") (v "0.1.0") (h "0kmg8h7wp67nsv1yq5cxc1qbdazxbgpzkn86jwy3pvd8sgv4bpwl") (y #t)))

(define-public crate-comma-cli-0.2.0 (c (n "comma-cli") (v "0.2.0") (h "14pnpln39ilc0avkgar40pvpv4nq9ni2dyz1vkn5mc4sq5hk9pmj")))

