(define-module (crates-io co mm community-id) #:use-module (crates-io))

(define-public crate-community-id-0.1.0 (c (n "community-id") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "063mzyd2qgc5p17aha0z9zfzh8pdwxwm61523r63pa71l95zw2x1")))

(define-public crate-community-id-0.1.1 (c (n "community-id") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "0cbdf5cqs58s2r1xkrxh2a6nd0fdnfssqmcx9d1323s93ri4m5sj")))

(define-public crate-community-id-0.1.2 (c (n "community-id") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "169nr5z0jiry5s6afrxvirrb1fdja2yi99bmldqq4slmqbcgjxnc")))

(define-public crate-community-id-0.2.0 (c (n "community-id") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "0xh30c3kjgkklm4cdd8xvrhgdj0sm689chy8gx1bibjs66qpigyd")))

(define-public crate-community-id-0.2.1 (c (n "community-id") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "1achbk5yg6d7bljv5k8r87x52bki5xalfbjfcf3vy8qq85cf35ra")))

(define-public crate-community-id-0.2.2 (c (n "community-id") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)))) (h "1b83k1gqx2nwagqlicxzyzryq4wrm697fhhy737p8jf075lgjsjg")))

