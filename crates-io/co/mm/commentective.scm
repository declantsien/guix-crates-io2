(define-module (crates-io co mm commentective) #:use-module (crates-io))

(define-public crate-commentective-0.1.0 (c (n "commentective") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "1yg3s0h851jblq15pivfvsjd0k3k2gpr30c6v5whpf1cdhps8qa7")))

(define-public crate-commentective-0.2.0 (c (n "commentective") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)))) (h "0vh2111vd92xlvbr3d42yfp6bw23hjk3qmp5ksslyr64gzkdf760")))

(define-public crate-commentective-0.3.0 (c (n "commentective") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)))) (h "0r0l29rf7y7v1kyc6lqngjkpnjfi04xh3m920khfirylgif9r8bg")))

(define-public crate-commentective-0.4.0 (c (n "commentective") (v "0.4.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "149y7pp8xjjphvvxbc3jrca38mg62w7jjk36hxyr4mj709n4c032")))

(define-public crate-commentective-0.5.0 (c (n "commentective") (v "0.5.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1fz8l0ylsggfizxj5lsglkdbrkaxy20cr3j59wsjpnpxias4i5yx")))

(define-public crate-commentective-0.6.0 (c (n "commentective") (v "0.6.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1gqcsx4j1gjhiaf5man5z8d5ck48il0fyx2b76aj8x7qawxaizaz")))

(define-public crate-commentective-0.7.0 (c (n "commentective") (v "0.7.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "rayon") (r "^1.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1xjsz7lp97i2ny7sgxwvbby4009fbhy3p0kbzyz1sjxp1x710sap")))

(define-public crate-commentective-0.8.0 (c (n "commentective") (v "0.8.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "rayon") (r "^1.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0krycqbafy6z84ini89mnnxi194yxh5y345jim9b97lhjcjbp7dv")))

(define-public crate-commentective-0.8.1 (c (n "commentective") (v "0.8.1") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0yg0fdc9841g1imf0983hgz8p0y62l7kn1d5s5vjba35005snjiw")))

