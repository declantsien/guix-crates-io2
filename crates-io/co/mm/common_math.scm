(define-module (crates-io co mm common_math) #:use-module (crates-io))

(define-public crate-common_math-0.1.0 (c (n "common_math") (v "0.1.0") (h "1qpx6h8zqc2kscwaimy21m09918wdv6k4nr4lzslgd14azz20p11")))

(define-public crate-common_math-0.1.1 (c (n "common_math") (v "0.1.1") (h "0w9415hvg785lvpjldxmqxm8xl1lnz6l53p91h72ypvlmv5nfyzn")))

(define-public crate-common_math-0.2.0 (c (n "common_math") (v "0.2.0") (h "0fsh0wbzi5hhbcf55kkf582a3p4vxan6sxpcnw2bmvjv0116psls")))

(define-public crate-common_math-0.2.1 (c (n "common_math") (v "0.2.1") (h "1vks5lnlfdwq1v3hvgi4cz78dj5hz8zx7g7261lyf3vfkm72c0lm")))

(define-public crate-common_math-0.2.2 (c (n "common_math") (v "0.2.2") (h "0adzc4cjxx4sjnhpwnzhk8b7d9sp5r64vrk7zpwmz7l8fqqj9al9")))

(define-public crate-common_math-0.2.3 (c (n "common_math") (v "0.2.3") (h "1icppl9qdhyss8y3hkdwlqpbnbcs3zi4y08mp26qk4yaridf7a7j")))

(define-public crate-common_math-0.2.4 (c (n "common_math") (v "0.2.4") (h "10dvz8ap35xcv3l5xwv3rl4w8iy5ylixrh6v6k9mn0a1phsw6i45")))

(define-public crate-common_math-0.3.0 (c (n "common_math") (v "0.3.0") (h "1qxnncixcsf30jfjy41syq1ljwjf08y183jk2jllancg6bbw5437")))

