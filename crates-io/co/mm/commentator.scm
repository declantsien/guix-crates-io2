(define-module (crates-io co mm commentator) #:use-module (crates-io))

(define-public crate-commentator-0.0.1 (c (n "commentator") (v "0.0.1") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1gl0n0n7l27a5a7qm4i7dlqhbl4wlnggcipm86jwzl653sr4k5yk")))

(define-public crate-commentator-0.1.0 (c (n "commentator") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1mcwnn2855y171f913kpiyga8xa1c9yispq1b6hn2shsrzvzv1j4") (f (quote (("feat-bin" "json" "argparse"))))))

(define-public crate-commentator-0.2.3 (c (n "commentator") (v "0.2.3") (d (list (d (n "argparse") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0ir7smzjj5nlkczpin5k0pl693g0bd4h79v26vswq2wwpp9967xi") (f (quote (("feat-bin" "json" "argparse"))))))

