(define-module (crates-io co mm command-error) #:use-module (crates-io))

(define-public crate-command-error-0.2.0 (c (n "command-error") (v "0.2.0") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shell-words") (r "^1") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0") (o #t) (d #t) (k 0)) (d (n "utf8-command") (r "^1") (o #t) (d #t) (k 0)))) (h "0xlzqhy0k26xwx8nfca7x91xb5myf4x5npzlc3ay9rnlcysbp632") (f (quote (("default" "utf8-command" "shell-words" "tracing"))))))

(define-public crate-command-error-0.2.1 (c (n "command-error") (v "0.2.1") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shell-words") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0") (o #t) (d #t) (k 0)) (d (n "utf8-command") (r "^1") (d #t) (k 0)))) (h "08n569jzj4dh9yl3n8aazz4yxr74qm23jzvg92616sh0a57qy70c")))

(define-public crate-command-error-0.3.0 (c (n "command-error") (v "0.3.0") (d (list (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shell-words") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0") (o #t) (d #t) (k 0)) (d (n "utf8-command") (r "^1") (d #t) (k 0)))) (h "1n3qlkhqrbs0ds84jp4xckj3g744dsdx8ga8857f4xly72n55rc7")))

(define-public crate-command-error-0.4.0 (c (n "command-error") (v "0.4.0") (d (list (d (n "dyn-clone") (r "^1.0.17") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "shell-words") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "tracing") (r "^0") (o #t) (d #t) (k 0)) (d (n "utf8-command") (r "^1") (d #t) (k 0)))) (h "084h5rkfbd1nckj6vvq3h2nh8myjw4dzx62kjpsgfjxs7m9gnfi7")))

