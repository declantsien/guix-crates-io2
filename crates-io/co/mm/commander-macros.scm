(define-module (crates-io co mm commander-macros) #:use-module (crates-io))

(define-public crate-commander-macros-1.0.1 (c (n "commander-macros") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1fsvlvfzymj9hhzjb8r5l641jdrarzlc5x201r4rn6g1nl0nn45x") (f (quote (("en") ("default" "en") ("cn"))))))

(define-public crate-commander-macros-1.1.1 (c (n "commander-macros") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1gb441ljq2p5772yz663yl6kwnhk21nrnn6jy9bllwfmm33kkk13")))

(define-public crate-commander-macros-1.1.2 (c (n "commander-macros") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1and1yj97qi9qibqqmrnwkji2ql4qqx9pnzzbkc43hcwhvkwj16d")))

(define-public crate-commander-macros-1.1.3 (c (n "commander-macros") (v "1.1.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0yi626gqyqx75bi9vbk3vfj33akfxaqqj3922cmndcpn3sqivjp1")))

(define-public crate-commander-macros-1.1.4 (c (n "commander-macros") (v "1.1.4") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0i4041wwp0q6bgj8wk0jc67j0nidwbim7v3ispmsyrwnxw063snb")))

(define-public crate-commander-macros-1.2.0 (c (n "commander-macros") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0s4hilzm3arps0k6pnk7imrd8asn534y1iz5q3b83dfcn222avgj")))

