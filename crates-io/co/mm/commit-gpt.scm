(define-module (crates-io co mm commit-gpt) #:use-module (crates-io))

(define-public crate-commit-gpt-1.0.0 (c (n "commit-gpt") (v "1.0.0") (d (list (d (n "async-openai") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cv40ji2w1613f5mrm37z09sqqmlv3mbcid5f3g61awhyl9xjwkb")))

(define-public crate-commit-gpt-1.0.1 (c (n "commit-gpt") (v "1.0.1") (d (list (d (n "async-openai") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0aqrgbsj1z7i6s7kscc8rb1s55kl6rk80vvj2b0960prfm3y6c8w")))

