(define-module (crates-io co mm commoncrypto-sys) #:use-module (crates-io))

(define-public crate-commoncrypto-sys-0.1.0 (c (n "commoncrypto-sys") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sk5y99j3h9hhpir7xng8pgy0ill44ncgppdvs43arl1g1xdr0cc") (f (quote (("lint" "clippy"))))))

(define-public crate-commoncrypto-sys-0.2.0 (c (n "commoncrypto-sys") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ql381ziqh594a7z6m9bvs583lkrhbynk02pmbgp7aj7czs39v8z") (f (quote (("lint" "clippy"))))))

