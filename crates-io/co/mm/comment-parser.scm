(define-module (crates-io co mm comment-parser) #:use-module (crates-io))

(define-public crate-comment-parser-0.1.0 (c (n "comment-parser") (v "0.1.0") (d (list (d (n "detect-lang") (r "^0.1") (d #t) (k 0)) (d (n "line-span") (r "^0.1") (d #t) (k 0)))) (h "1q70qx0j73rd18gwgcv5avyz7ckg6fj7ww7j9gw1djgzrpq8xyw4")))

