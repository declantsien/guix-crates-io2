(define-module (crates-io co rd cordic) #:use-module (crates-io))

(define-public crate-cordic-0.1.0 (c (n "cordic") (v "0.1.0") (d (list (d (n "fixed") (r "^0.5") (d #t) (k 0)))) (h "1s2xk6bb7zj489hn6i5sa4871cvrqqb5wlg1wi5knyq69dwkrgsh") (y #t)))

(define-public crate-cordic-0.1.1 (c (n "cordic") (v "0.1.1") (d (list (d (n "fixed") (r "^0.5") (d #t) (k 0)))) (h "12gn7fsah45vq046fxj8l9rk9sm39bg4kbyy8kdhw1yixjk6pjlv") (y #t)))

(define-public crate-cordic-0.1.2 (c (n "cordic") (v "0.1.2") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)))) (h "1z1l4xb4hq9603rb0q9fcdj0piq0cmm45igvqa76vm5gxz6lsx7j") (y #t)))

(define-public crate-cordic-0.1.3 (c (n "cordic") (v "0.1.3") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)))) (h "1zk2aqhqg60lr03jmqj1jk61s5xj7vfhcmbsgz7r3grafqb5zyba") (y #t)))

(define-public crate-cordic-0.1.4 (c (n "cordic") (v "0.1.4") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)))) (h "13zvqn6c8d8lp18p9ik10q100wfsyv2m2n4fca16laq3yw7r231m")))

(define-public crate-cordic-0.1.5 (c (n "cordic") (v "0.1.5") (d (list (d (n "fixed") (r "^1") (d #t) (k 0)))) (h "1l0jfhm6kynv61bp9ncmi25bdib40d9pfcajl1gwkidqq1va3l0f")))

