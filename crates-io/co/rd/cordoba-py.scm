(define-module (crates-io co rd cordoba-py) #:use-module (crates-io))

(define-public crate-cordoba-py-0.3.1 (c (n "cordoba-py") (v "0.3.1") (d (list (d (n "cordoba") (r "^0.3.1") (f (quote ("python"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "= 0.6.0-alpha.2") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0r12kh9c0simg6bwal9k4pwj2b6ryrygn8xjgf0yh2ygbpxn5174")))

