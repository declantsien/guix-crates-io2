(define-module (crates-io co rd cordon) #:use-module (crates-io))

(define-public crate-cordon-0.1.0 (c (n "cordon") (v "0.1.0") (h "07fab9cz4q8zv9ad32pl7a0sga0rv5z4wsgqxn44shz107fb6wck")))

(define-public crate-cordon-0.2.0 (c (n "cordon") (v "0.2.0") (d (list (d (n "alloc_counter") (r "^0.0.4") (d #t) (k 0)) (d (n "c_str_macro") (r "^1.0.3") (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "eyre") (r "^0.6.9") (d #t) (k 2)) (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1l6kvpg3qcr28r5ndrzlgnmfhrkngznjm3xrrkskvzxlbyk0525c")))

(define-public crate-cordon-0.3.0 (c (n "cordon") (v "0.3.0") (d (list (d (n "alloc_counter") (r "^0.0.4") (d #t) (k 0)) (d (n "c_str_macro") (r "^1.0.3") (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "eyre") (r "^0.6.9") (d #t) (k 2)) (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "12443vqvvyl0xgxkqkbsxjm7s1lx2rdfz0z6629n8wniq0s1gjgv")))

