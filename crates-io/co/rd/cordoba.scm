(define-module (crates-io co rd cordoba) #:use-module (crates-io))

(define-public crate-cordoba-0.1.0 (c (n "cordoba") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.31") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (o #t) (d #t) (k 0)))) (h "1m7aa04s1gns1848xdjdawy40s1vw23sf0xqhs36bgxxmx76bpgy") (f (quote (("default" "build-binary") ("build-binary" "clap" "memmap"))))))

(define-public crate-cordoba-0.2.0 (c (n "cordoba") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.31") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0y20wfjnxi9bh596v04j3jmdssnjib43dkmpq6rm3332vwj2yx5y") (f (quote (("default" "build-binary") ("build-binary" "clap" "memmap"))))))

(define-public crate-cordoba-0.3.0 (c (n "cordoba") (v "0.3.0") (d (list (d (n "clap") (r "^2.31") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.6.0-alpha") (o #t) (d #t) (k 0)))) (h "01y69ikjsis19qhbaw2289247ggd2qr4jqyygl4rm61446mkjsii") (f (quote (("std") ("python" "pyo3") ("default" "build-binary" "std") ("build-binary" "clap" "memmap"))))))

(define-public crate-cordoba-0.3.1 (c (n "cordoba") (v "0.3.1") (d (list (d (n "clap") (r "^2.31") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "= 0.6.0-alpha.2") (o #t) (d #t) (k 0)))) (h "0bgn3wj3330fjc1q78d1dar7dwbhhq53iidq4c3nclz0k4j69shw") (f (quote (("std") ("python" "pyo3") ("default" "build-binary" "std") ("build-binary" "clap" "memmap"))))))

(define-public crate-cordoba-0.3.2 (c (n "cordoba") (v "0.3.2") (d (list (d (n "clap") (r "^2.31") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "= 0.9.0-alpha.1") (o #t) (d #t) (k 0)))) (h "1z2hchv1lawb7mr2kd81ijqs194y970xk7rka82g8lxnaj194348") (f (quote (("std") ("python" "pyo3") ("default" "build-binary" "std") ("build-binary" "clap" "memmap"))))))

(define-public crate-cordoba-0.3.3 (c (n "cordoba") (v "0.3.3") (d (list (d (n "clap") (r "^2.31") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "= 0.9.0-alpha.1") (o #t) (d #t) (k 0)))) (h "0im0444anfsnblwx2j6wlwqznhn0mh09qddqnx9mclc8a5ixik0q") (f (quote (("std") ("python" "pyo3") ("default" "build-binary" "std") ("build-binary" "clap" "memmap"))))))

(define-public crate-cordoba-0.3.4 (c (n "cordoba") (v "0.3.4") (d (list (d (n "clap") (r "^2.31") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.11") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0jmy8x8sixrwjzay8l3yay4c5jfnk06sjdf9fc0s2mn2rc3p8pf5") (f (quote (("std") ("python" "pyo3" "memmap") ("default" "build-binary" "std") ("build-binary" "clap" "memmap"))))))

