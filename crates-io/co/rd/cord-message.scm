(define-module (crates-io co rd cord-message) #:use-module (crates-io))

(define-public crate-cord-message-0.1.0 (c (n "cord-message") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)))) (h "1l5vd3kvd59c8399q99s8kd56x24zrpq96x812vcbfh8n9z7wa27")))

(define-public crate-cord-message-0.2.0 (c (n "cord-message") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "11jzvf979ppx7lr902yyl0m68dv3pblk3d0aqcyj9p6ii7nyvyh2")))

(define-public crate-cord-message-0.3.0 (c (n "cord-message") (v "0.3.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "1iax98m4r0vx8vfmah1d6h8858ni0l9p92b6bznc1gkzn66hdajf")))

