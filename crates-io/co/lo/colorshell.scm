(define-module (crates-io co lo colorshell) #:use-module (crates-io))

(define-public crate-ColorShell-1.0.0 (c (n "ColorShell") (v "1.0.0") (h "04ixrxsr3cly7dspphqx067f8bg22r035hhb56w0by21nilyn7rv")))

(define-public crate-ColorShell-1.0.3 (c (n "ColorShell") (v "1.0.3") (h "1vi6kxyaqrcf6k2kw2qa25kladlfmhi4ll40c04nsin6hrw02zpz")))

(define-public crate-ColorShell-1.0.4 (c (n "ColorShell") (v "1.0.4") (h "1xm4lvzvnnzxzlfvpj7795j744hciprwrj3zf29f2by2bmracs16")))

(define-public crate-ColorShell-1.0.5 (c (n "ColorShell") (v "1.0.5") (h "1kqwcc40x2ampq1kydqzdjg7ia1ffarxp6vivpkgnhc0wp19aasy")))

(define-public crate-ColorShell-1.0.6 (c (n "ColorShell") (v "1.0.6") (h "0f77dy7x5vl7y0y94xmacqknkbdm69rsnh1zq7nd0hhf7fb5hf1r")))

(define-public crate-ColorShell-1.0.7 (c (n "ColorShell") (v "1.0.7") (h "0lck1qf1xncwmjg7g05pv760yfil89sfsgyy8869qx87fmk2k6gx")))

(define-public crate-ColorShell-1.0.8 (c (n "ColorShell") (v "1.0.8") (h "00l3nsagix0j293cgnz3b7cx7dvaji6yb19l694vr27zwprgn7qi")))

(define-public crate-ColorShell-1.0.9 (c (n "ColorShell") (v "1.0.9") (h "1s0yy58wvs5wbz6hjz2l8n1xxdr7kmmc3lqbgqh2fs638pm92l43")))

(define-public crate-ColorShell-1.1.0 (c (n "ColorShell") (v "1.1.0") (h "12hj3pr8qlrrd600c7qcyj2cb4pbsj3nqm5vl3nv3fby5f1wll2b")))

(define-public crate-ColorShell-1.1.1 (c (n "ColorShell") (v "1.1.1") (h "1rcc9hbfq28p9sfnaxd5m0n4yj8kyn2plj0qrn5pnzrzw5ryp1bg")))

(define-public crate-ColorShell-1.1.2 (c (n "ColorShell") (v "1.1.2") (h "0hw0fm1cnka73hm94l9rbx4npjzpk0683dlg9akqndqbarfys7hs")))

(define-public crate-ColorShell-1.1.3 (c (n "ColorShell") (v "1.1.3") (h "1jndm1mxq8i5y8zyafhfw5vk0gym6vyip6c5s1k755h74p00lmgw")))

(define-public crate-ColorShell-1.1.4 (c (n "ColorShell") (v "1.1.4") (h "13mm900svlfnl510jn267cagv1i4346ymwbkk8pqlad10l914nq1")))

(define-public crate-ColorShell-1.1.5 (c (n "ColorShell") (v "1.1.5") (h "02bd87wgfqd3y1567cgzk6xllfsab0jlvvg5f4ryjkcrk713cvsb")))

