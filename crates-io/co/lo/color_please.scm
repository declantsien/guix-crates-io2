(define-module (crates-io co lo color_please) #:use-module (crates-io))

(define-public crate-color_please-0.1.0 (c (n "color_please") (v "0.1.0") (h "0lga5iyg0bzl1wy3cqcywld6s9afgpypkfv18nhrmn8czw3r03wb")))

(define-public crate-color_please-0.1.1 (c (n "color_please") (v "0.1.1") (h "095vvv8p04yja9szpqz8hh3mw9pdpv239dxq5nazmj31mvnpcivz")))

(define-public crate-color_please-0.2.0 (c (n "color_please") (v "0.2.0") (h "1xvcmr8c90lnsghyjpw990mljs36fm2jlbyd4hj24yk6wyja0q3x")))

(define-public crate-color_please-0.2.1 (c (n "color_please") (v "0.2.1") (h "0faqhyl13yx8khwp8rpf0y38nq2mabc4khwpnrndphalj3ikqwp9")))

