(define-module (crates-io co lo colorbrewer) #:use-module (crates-io))

(define-public crate-colorbrewer-0.1.0 (c (n "colorbrewer") (v "0.1.0") (h "1frri1zlzfnzs1w46a0pd9mdklmrzd455sfdyw4frhhd0963q144")))

(define-public crate-colorbrewer-0.2.0 (c (n "colorbrewer") (v "0.2.0") (d (list (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "19hr75k7ai36n6y62rs09b9gprh1hvqqgvad28p25g4p5l0yansj")))

