(define-module (crates-io co lo color-rs) #:use-module (crates-io))

(define-public crate-color-rs-0.1.0 (c (n "color-rs") (v "0.1.0") (d (list (d (n "angle") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1swbwxf9091xxwp16hsy7kgcg8spgyf5fzkk85ijjcl8bxrsr15j")))

(define-public crate-color-rs-0.2.0 (c (n "color-rs") (v "0.2.0") (d (list (d (n "angle") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0m6y537lff9pdj7ds1kid9xvaybbcy9kx0w9h2fkhwkfcdxg2iky")))

(define-public crate-color-rs-0.3.0 (c (n "color-rs") (v "0.3.0") (d (list (d (n "angle") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1na9cszhdbdz4arzp92bgfwh9ack0mk4xbl3pclsv1bjnf626lfw")))

(define-public crate-color-rs-0.4.0 (c (n "color-rs") (v "0.4.0") (d (list (d (n "angle") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0hsaqc0akhn1jvs5asmvg4fq1dvhwf62b5ni2yba2xg259gwk24j")))

(define-public crate-color-rs-0.5.0 (c (n "color-rs") (v "0.5.0") (d (list (d (n "angle") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0pwal5r7hdk2xg045vnaflbq478zs0yadbll5smr3cxhz04aviq0")))

(define-public crate-color-rs-0.6.0 (c (n "color-rs") (v "0.6.0") (d (list (d (n "angle") (r "^0.4") (d #t) (k 0)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "kmeans_colors") (r "^0.3.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0yrasagyp4a5ws60fkgh2d6gd0pybl1w8cri0662l7pn55pzy15q") (f (quote (("kmeans" "kmeans_colors" "rand") ("default"))))))

(define-public crate-color-rs-0.6.1 (c (n "color-rs") (v "0.6.1") (d (list (d (n "angle") (r "^0.4") (d #t) (k 0)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "kmeans_colors") (r "^0.3.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0w7wg7kg3lqfkn8kql2ijsr43f5595plhipjp7h8jrgld2bkvx2r") (f (quote (("kmeans" "kmeans_colors" "rand") ("default"))))))

(define-public crate-color-rs-0.7.0 (c (n "color-rs") (v "0.7.0") (d (list (d (n "angle") (r "^0.5") (d #t) (k 0)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "kmeans_colors") (r "^0.5") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0kqg9a1hgg1nvqhh7wy4k6b4dbilrz5d5la2h1wwp5wvy0dipbs8") (f (quote (("kmeans" "kmeans_colors" "rand") ("default"))))))

(define-public crate-color-rs-0.7.1 (c (n "color-rs") (v "0.7.1") (d (list (d (n "angle") (r "^0.5") (d #t) (k 0)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "kmeans_colors") (r "^0.5") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "025q6ij513ynx00qiq4233bb722wdzfz5l8mdmm0sdc0inhwvmh5") (f (quote (("kmeans" "kmeans_colors" "rand") ("default"))))))

(define-public crate-color-rs-0.8.0 (c (n "color-rs") (v "0.8.0") (d (list (d (n "angle") (r "^0.5") (d #t) (k 0)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "kmeans_colors") (r "^0.5") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1y49kprk16g422ncg9nr0i747byj36zwrz5r9mhj6sznh65w259l") (f (quote (("kmeans" "kmeans_colors" "rand") ("default"))))))

