(define-module (crates-io co lo colored_json) #:use-module (crates-io))

(define-public crate-colored_json-0.1.0 (c (n "colored_json") (v "0.1.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lbzs07ayx3czfik3w3511r0szl2617rjqgs9hhs5z3r3n96n0m6")))

(define-public crate-colored_json-0.2.0 (c (n "colored_json") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "085p9f3lb0r504kll18jzfkzchkvijg0c7vk50vmvzf5fyppja1l")))

(define-public crate-colored_json-0.2.1 (c (n "colored_json") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01kcfxdj8pvz5pql9yfhpnzaf03q5a9cl0rd3cmwprx9a9safx8h")))

(define-public crate-colored_json-0.2.2 (c (n "colored_json") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hrwrnyv6j23fgxljqbn3jbqldrj6d5d50ykhvsmp6g9zhb5giai")))

(define-public crate-colored_json-0.3.0 (c (n "colored_json") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bfnnnr42mbvlhcxxsdrybslh3w8idnjzy7c0jipbvpd2rk0phxp")))

(define-public crate-colored_json-0.4.0 (c (n "colored_json") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xvrjrwzrcl1747prvfgfv00wg0assakshrd59bdqxcm04xsayjv")))

(define-public crate-colored_json-0.5.0 (c (n "colored_json") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "00l2svdaddf84fpbv4x1pip12w6bcwlincrbjdc3qp4mx18m59l1")))

(define-public crate-colored_json-0.6.0 (c (n "colored_json") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08sb9y7p4j5w5rl18vdfrj80if4zi9gjcf0hc8fy2lkzir00k6dl")))

(define-public crate-colored_json-0.6.1 (c (n "colored_json") (v "0.6.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1y1cjhc039lmw2l6q9lgbg39hwpn3aal67ndbh01bp1rw61ygaxq")))

(define-public crate-colored_json-0.6.2 (c (n "colored_json") (v "0.6.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17wbf8rqxaw0mxfrwvg3qn7n7p8p2d1gfdv8bsvf6dxysxh1f685")))

(define-public crate-colored_json-1.0.0 (c (n "colored_json") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kspb7irddqqcbgdc5kyy2p5nrbn30x8rjmnwm52iqqzz987ga0i")))

(define-public crate-colored_json-2.0.0 (c (n "colored_json") (v "2.0.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wlh0p03nvfnmqlli9zh1m8a553g5zna8impkyfidmhvmdr3gg9y")))

(define-public crate-colored_json-2.1.0 (c (n "colored_json") (v "2.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0r0sfwhwmzmfd54cr373j8x88x9ch2ky6016ghxj0vh19nsjxlqz")))

(define-public crate-colored_json-3.0.0 (c (n "colored_json") (v "3.0.0") (d (list (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "051526pi05gh2bykc2fycn1pxcdzd941w724pp94c5z80lclwxfw")))

(define-public crate-colored_json-3.0.1 (c (n "colored_json") (v "3.0.1") (d (list (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1szxzjn1p39xj4bqvrjlkm49vvljdv80hz60sh44a6c4pg6iack3")))

(define-public crate-colored_json-3.1.0 (c (n "colored_json") (v "3.1.0") (d (list (d (n "is-terminal") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1sash81my29b0dqaxf95iy19gifqzb0pgmfiqdgizknzkhjwswsk")))

(define-public crate-colored_json-3.2.0 (c (n "colored_json") (v "3.2.0") (d (list (d (n "is-terminal") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "14ba6rg1habjv4z0hk94qx744pgnxvpg53aim6zm8vkgp3k9rjvl")))

(define-public crate-colored_json-3.3.0 (c (n "colored_json") (v "3.3.0") (d (list (d (n "is-terminal") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1ckx22mncjllhijw6r44w9djibakllnk15w9mwxj7nvklz8awj29") (y #t)))

(define-public crate-colored_json-4.0.0 (c (n "colored_json") (v "4.0.0") (d (list (d (n "is-terminal") (r "^0.4.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1a7r82ymgjz1vv3j4ydwc9bbxqzak3nzid4hkbrbxzx3f99kl836")))

(define-public crate-colored_json-4.1.0 (c (n "colored_json") (v "4.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "11vsvkfr0d9bhskpc9jqnrzrglwawjsg1vsf93k7bsngylnz7kvr") (r "1.70.0")))

(define-public crate-colored_json-5.0.0 (c (n "colored_json") (v "5.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "yansi") (r "^1") (d #t) (k 0)))) (h "1dirw20q0d3i9zyayc4jp90731d0fa8hj67xb7iy7y26p2hq0ng3") (r "1.70.0")))

