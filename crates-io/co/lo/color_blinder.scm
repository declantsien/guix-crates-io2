(define-module (crates-io co lo color_blinder) #:use-module (crates-io))

(define-public crate-color_blinder-0.1.0 (c (n "color_blinder") (v "0.1.0") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0vsi1xg75b10krq5p54jfg8izckxfg0rwjxlxhqk68l8rrfzc5rn")))

(define-public crate-color_blinder-0.2.0 (c (n "color_blinder") (v "0.2.0") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "084gcnkjhj8jd5fvl02v769x72jlk3qwjsq76qxl0cw212h7p2z9")))

(define-public crate-color_blinder-0.2.1 (c (n "color_blinder") (v "0.2.1") (d (list (d (n "image") (r "^0.22.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "06hcny3j1w3lq48s7hnphdhfhxl9wwbhqjhk25f9d3pwmjn45kkx")))

(define-public crate-color_blinder-0.3.0 (c (n "color_blinder") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rusttype") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0rmrbav130vfygfvvj0q0jcqv9xwkfghjymhalbdxc1a8d5n1py7") (f (quote (("labels" "rusttype") ("default" "labels"))))))

(define-public crate-color_blinder-0.3.1 (c (n "color_blinder") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rusttype") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1i6fxxb5ag6n0636p4hx8n38v2afh0m41pcxlalrx7xwhrq197vm") (f (quote (("labels" "rusttype") ("default" "labels"))))))

(define-public crate-color_blinder-0.3.2 (c (n "color_blinder") (v "0.3.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0irxp4cwa96v2grj6950khwv696vp960pw9704giq673bqqx3alh") (f (quote (("labels" "rusttype") ("default" "labels"))))))

(define-public crate-color_blinder-1.0.0 (c (n "color_blinder") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 0)))) (h "19i0zg99yv88wbxazd2jgbpzbfcw3n38wsq01xkzxas40vd4zcya") (f (quote (("labels" "rusttype") ("default" "labels"))))))

(define-public crate-color_blinder-1.0.2 (c (n "color_blinder") (v "1.0.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 0)))) (h "0vmlzjwxm49kxbgb6x82hrs619s3zzmm8k9f9gx0wm309rwq4zj6") (f (quote (("labels" "rusttype") ("default" "labels"))))))

