(define-module (crates-io co lo colo) #:use-module (crates-io))

(define-public crate-colo-0.1.0 (c (n "colo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "color_space") (r "=0.5.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.1") (d #t) (k 0)))) (h "0pw4nnmli8l0vc85q3l1qyarpl8xs6a21xd8pgc2wvdf2qb553jr")))

(define-public crate-colo-0.2.0 (c (n "colo") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "color_space") (r "=0.5.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.1") (d #t) (k 0)))) (h "16kfginh58pcahlg34pk6gszblp3lzvybk9xqklgl9lpvdfd5w9v") (y #t)))

(define-public crate-colo-0.2.1 (c (n "colo") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "color_space") (r "=0.5.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.1") (d #t) (k 0)))) (h "0lx6jl4k18kdi1svs0bawviyq1g4krxwx88k4iq5fa1sghw4nbdv")))

(define-public crate-colo-0.2.2 (c (n "colo") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "color_space") (r "^0.5.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.1") (d #t) (k 0)))) (h "0yd8dz4c6q9hdlx280qi658xwhjm5hpgwra08ysmi4rl93p48dgi")))

(define-public crate-colo-0.3.0 (c (n "colo") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "color_space") (r "^0.5.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zv0bq86bf04gqv292ssljhr61aqqg8fippcf9865yi2r92cc85k")))

(define-public crate-colo-0.3.1 (c (n "colo") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "color_space") (r "^0.5.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x36hr3j2g52f9dibv0hplgn4l519maf6zwqpi70c7gwdgp0mkal")))

(define-public crate-colo-0.3.2 (c (n "colo") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "color_space") (r "^0.5.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.4") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x8pzxc8wmp6slpw0fyrva502q1n7zcc75j1s8klqmkbddpa24s0")))

(define-public crate-colo-0.4.0 (c (n "colo") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "color_space") (r "^0.5.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.13") (d #t) (k 0)) (d (n "fastrand") (r "^1.4") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1487p2xfk6xcp3acy638bpgjsch3q45cm1cx886k62pmajhxsr99")))

(define-public crate-colo-0.4.1 (c (n "colo") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "color_space") (r "^0.5.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.13") (d #t) (k 0)) (d (n "fastrand") (r "^1.4") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "196jxcc8nv23kk24d81z4hikn9q0k8b7r34i2c63hfgismbyqi25")))

