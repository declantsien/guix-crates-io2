(define-module (crates-io co lo color_quant) #:use-module (crates-io))

(define-public crate-color_quant-1.0.0 (c (n "color_quant") (v "1.0.0") (h "03bvfrblk86qhn72sr41jcd5l0xgrydqv5kjvy5d50rdyi5gqxd4")))

(define-public crate-color_quant-1.0.1 (c (n "color_quant") (v "1.0.1") (h "1ga56jrafnjm80903nnqjkyii4bwd6a7visxh0g8hgi6cmrvbfqd")))

(define-public crate-color_quant-1.1.0 (c (n "color_quant") (v "1.1.0") (h "12q1n427h2bbmmm1mnglr57jaz2dj9apk0plcxw7nwqiai7qjyrx")))

