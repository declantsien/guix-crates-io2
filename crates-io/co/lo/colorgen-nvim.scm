(define-module (crates-io co lo colorgen-nvim) #:use-module (crates-io))

(define-public crate-colorgen-nvim-0.1.0 (c (n "colorgen-nvim") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "14y8vqiagljgybqbxbv65xy4zlj8ghz5lc81c18amfgfva3xc5j8")))

(define-public crate-colorgen-nvim-0.2.0 (c (n "colorgen-nvim") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0sbnr6yngazyn4sss2f35nbval8rnvva6b07wi4zmsdd9hf3jiwh")))

(define-public crate-colorgen-nvim-0.3.0 (c (n "colorgen-nvim") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "11ffmpy8xsg4sdmbdzsbndzrzw3nilhfjx8z77whjcgn1i1g69ay")))

