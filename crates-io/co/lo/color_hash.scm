(define-module (crates-io co lo color_hash) #:use-module (crates-io))

(define-public crate-color_hash-0.1.0 (c (n "color_hash") (v "0.1.0") (d (list (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "1q1g9ys06a7c97x6wzy1wrmm0a1d350ycyihyk56b70kl05kgkad")))

