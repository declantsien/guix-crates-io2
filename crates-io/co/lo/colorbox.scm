(define-module (crates-io co lo colorbox) #:use-module (crates-io))

(define-public crate-colorbox-0.1.0 (c (n "colorbox") (v "0.1.0") (h "1lc4q3dfn180gbkg1n7qciikcxpbngjqh2vclwrs8yqj18mzyj62")))

(define-public crate-colorbox-0.2.0 (c (n "colorbox") (v "0.2.0") (h "0gkx7pvmgck4gqgn8iwdym2rnn3kqdr5vr4yk47jlv080s65s9xl")))

(define-public crate-colorbox-0.3.0 (c (n "colorbox") (v "0.3.0") (h "1z2ns7n06d79495cr0fhqa5nz03kbrrngx2lfsan15q0c5axa9rx")))

(define-public crate-colorbox-0.4.0 (c (n "colorbox") (v "0.4.0") (h "10b8j17lxzfaqsj65dapxc3ngfk6a7qr8fbiarjgizxlvk4jih7w")))

