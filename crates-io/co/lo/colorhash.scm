(define-module (crates-io co lo colorhash) #:use-module (crates-io))

(define-public crate-colorhash-0.1.0 (c (n "colorhash") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "colorsys") (r "^0.6.7") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 0)))) (h "03nl12wk52fgh3kaam598pzydlh5dnkdbnsl44xks2wgsr5vwzdb")))

(define-public crate-colorhash-0.1.1 (c (n "colorhash") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "colorsys") (r "^0.6.7") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 0)))) (h "1i491a8adana34ajpnwhrxsif2zi6nimgbdvngqng7qwysjanfgr")))

(define-public crate-colorhash-0.1.2 (c (n "colorhash") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "colorsys") (r "^0.6.7") (d #t) (k 0)) (d (n "float_eq") (r "^1.0") (d #t) (k 2)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 2)))) (h "0hkiw6j4afdibd7bzim0qgbrhg77zhzncfqlfgrb8karb7gqhkgp")))

(define-public crate-colorhash-0.1.3 (c (n "colorhash") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "colorsys") (r "^0.6.7") (d #t) (k 0)) (d (n "float_eq") (r "^1.0") (d #t) (k 2)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 2)))) (h "0p4a92dg5f7m0sywqy156h072asf5vssq5pp8454g2d1bzcy0imw")))

