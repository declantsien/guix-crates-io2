(define-module (crates-io co lo colours) #:use-module (crates-io))

(define-public crate-colours-0.1.0 (c (n "colours") (v "0.1.0") (h "1rv2hw5qd5dbbi7a5wrkkx2bn07720m24fz26vb1d4yp3clpmgar")))

(define-public crate-colours-0.1.1 (c (n "colours") (v "0.1.1") (h "0jrq9al3pcwf6mrwhbammmwv8amid1bmnvqvbkw4nc1pd5cflvhr")))

