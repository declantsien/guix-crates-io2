(define-module (crates-io co lo colored_truecolor) #:use-module (crates-io))

(define-public crate-colored_truecolor-0.1.0 (c (n "colored_truecolor") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)) (d (n "winconsole") (r "^0.10.0") (d #t) (k 0)))) (h "0052drx350mbwr4icyvnfdvj09ia9rxdq685fji24kd3rab45x46") (f (quote (("no-color"))))))

