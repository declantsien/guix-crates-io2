(define-module (crates-io co lo colorgrad) #:use-module (crates-io))

(define-public crate-colorgrad-0.1.0 (c (n "colorgrad") (v "0.1.0") (d (list (d (n "csscolorparser") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 2)) (d (n "noise") (r "^0.6.0") (d #t) (k 2)))) (h "12y2b1ffibc8pp4wq8x9n916q2s89fi6c64ib9gzjj50p3ydxfbi")))

(define-public crate-colorgrad-0.2.0 (c (n "colorgrad") (v "0.2.0") (d (list (d (n "csscolorparser") (r "^0.3.0") (d #t) (k 0)))) (h "1iwrbi0v17d9p1yh4kigja1nkz4w1nqagh10i4lvr54lipsg9jqv")))

(define-public crate-colorgrad-0.3.0 (c (n "colorgrad") (v "0.3.0") (d (list (d (n "csscolorparser") (r "^0.4.0") (d #t) (k 0)))) (h "0asyadwyzdq7mzd8c91s42lwkmzvhvhh4bfgxgq8zplsdcw2jmj9")))

(define-public crate-colorgrad-0.4.0 (c (n "colorgrad") (v "0.4.0") (d (list (d (n "csscolorparser") (r "^0.4.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 2)))) (h "0bbj3cdc8vsahxvbfcn7r8ipbjqjyfzf85a5yq0ixmmm214yirjg")))

(define-public crate-colorgrad-0.5.0 (c (n "colorgrad") (v "0.5.0") (d (list (d (n "csscolorparser") (r "^0.5.0") (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("png"))) (k 2)))) (h "01p35k866dgr84q4z9vdi8smm35kgmq74pkpykyw0hmlvk939j32") (f (quote (("named-colors" "csscolorparser/named-colors") ("default" "named-colors"))))))

(define-public crate-colorgrad-0.6.0 (c (n "colorgrad") (v "0.6.0") (d (list (d (n "csscolorparser") (r "^0.6.0") (k 0)) (d (n "image") (r "^0.24.2") (f (quote ("png"))) (k 2)))) (h "1g4yms961g7gwzcvz9kczql3igf2cyc2yrzdcsa2jrmlw08xqaqr") (f (quote (("named-colors" "csscolorparser/named-colors") ("default" "named-colors"))))))

(define-public crate-colorgrad-0.6.1 (c (n "colorgrad") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "csscolorparser") (r "^0.6.2") (k 0)) (d (n "image") (r "^0.24.3") (f (quote ("png"))) (k 2)))) (h "0vnn9j2nppxl6ibq1q355xzpxxvl21dh195scfl65g05adk1cbaa") (f (quote (("named-colors" "csscolorparser/named-colors") ("default" "named-colors"))))))

(define-public crate-colorgrad-0.6.2 (c (n "colorgrad") (v "0.6.2") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "csscolorparser") (r "^0.6.2") (k 0)) (d (n "image") (r "^0.24.3") (f (quote ("png"))) (k 2)))) (h "1dkzc72il8iam904pa2p3d32dgz9fw715lwkl3h0b7ab8xfl0pva") (f (quote (("named-colors" "csscolorparser/named-colors") ("default" "named-colors"))))))

