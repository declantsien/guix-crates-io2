(define-module (crates-io co lo colortypes) #:use-module (crates-io))

(define-public crate-colortypes-0.1.0 (c (n "colortypes") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1djmchwdh2gqdxxp7v6jdly877jippn7dykj7z0n3hzar0aksbbq")))

(define-public crate-colortypes-0.1.1 (c (n "colortypes") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vz9f1zcnm5n4017lwsfya92jdj53nnd4d2v6wqixayjb9gy8cjg")))

(define-public crate-colortypes-0.1.2 (c (n "colortypes") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1i4rcmrbbzrf2z7xq5n88fwhhhi8vrwapha5dc8zjhrgwh03pswc")))

(define-public crate-colortypes-0.1.3 (c (n "colortypes") (v "0.1.3") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "09f55jwa7gnlhgvizw7y29m58a6dl6cjhdbicy7qnsapmg5z8fcn")))

(define-public crate-colortypes-0.1.4 (c (n "colortypes") (v "0.1.4") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "01nr15paiaxx8mw5pjfrpjljl6ws4xi495vmva5qh5x637b88fzq") (y #t)))

(define-public crate-colortypes-0.2.0 (c (n "colortypes") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0r3gxpqm55vqczrd6pa6vnr9yvnh4jw1igr317p6m88l4jq42n3z")))

(define-public crate-colortypes-0.3.0 (c (n "colortypes") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04da04m2qxlsr5ww9dpx0fhgsly65rpp8gbbqphk9qb0drkxfm0j") (y #t)))

(define-public crate-colortypes-0.3.1 (c (n "colortypes") (v "0.3.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1n4mc3zjz490k12876q6afkin1ph0yrc2qcaq9jdwwpcaf14jk8z")))

(define-public crate-colortypes-0.3.2 (c (n "colortypes") (v "0.3.2") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "05a2y7242qamr9di799vbkjsiarnh7y6wdh4wbxlmisnkbk481gr")))

(define-public crate-colortypes-0.3.3 (c (n "colortypes") (v "0.3.3") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vn5y3ba2j5k9r356nqy0djvskc57j7g2bwxpl9cwy6yrjc3vvkf")))

(define-public crate-colortypes-0.4.0 (c (n "colortypes") (v "0.4.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1lav3n5j1vjcclp75kivda8n1ffly5lk8w4ycv1bbaw6y0dpxfil")))

(define-public crate-colortypes-0.4.2 (c (n "colortypes") (v "0.4.2") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1jz3rf3mk3hkswi8r9ljzq5pd3jjsifii11a11qsvk24250nipc2")))

(define-public crate-colortypes-0.5.0 (c (n "colortypes") (v "0.5.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12d8vamzycjdi42ag37smmkzl4bgkhgxj35qfvbiywfkz6syhzby")))

(define-public crate-colortypes-0.5.1 (c (n "colortypes") (v "0.5.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1zh75spms7whwqmib55xwk1pnws76m3i000s4mz7a0phav0yd1md")))

(define-public crate-colortypes-0.6.0 (c (n "colortypes") (v "0.6.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0wbdjgqa6gb6qd6765kxxrdwh3zqrd0lk2cickd1g6xlfbi15rdx") (y #t)))

(define-public crate-colortypes-0.6.1 (c (n "colortypes") (v "0.6.1") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "19cpnkbxva3sxrm570npc887b1rqilbqxv17rv888cq0c9varnf8")))

(define-public crate-colortypes-0.6.2 (c (n "colortypes") (v "0.6.2") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0cj1lyixq4prnpal4pvaycg0vcd66qsklla744sywryh7x8rcy2i")))

