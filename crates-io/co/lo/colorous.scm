(define-module (crates-io co lo colorous) #:use-module (crates-io))

(define-public crate-colorous-1.0.0 (c (n "colorous") (v "1.0.0") (d (list (d (n "dejavu") (r "= 2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "imageproc") (r "^0.20") (d #t) (k 2)) (d (n "rusttype") (r "^0.8") (d #t) (k 2)))) (h "1lnqwcbrhm7rrk9v22f5yj81p0hv2pkwral2n46h9sczkzi658rc")))

(define-public crate-colorous-1.0.1 (c (n "colorous") (v "1.0.1") (d (list (d (n "dejavu") (r "= 2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "imageproc") (r "^0.20") (d #t) (k 2)) (d (n "rusttype") (r "^0.8") (d #t) (k 2)))) (h "0dn7965i479f5vcm8n3h4l5laxyi0cx1z6pg97p7jh9ksgb4gszb")))

(define-public crate-colorous-1.0.2 (c (n "colorous") (v "1.0.2") (d (list (d (n "dejavu") (r "= 2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "imageproc") (r "^0.20") (d #t) (k 2)) (d (n "rusttype") (r "^0.8") (d #t) (k 2)))) (h "0lichn26d3gxr8zdari761cqkxj0snfd39ya86ylvmpb5dg4kb76") (f (quote (("std") ("default" "std"))))))

(define-public crate-colorous-1.0.3 (c (n "colorous") (v "1.0.3") (d (list (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "imageproc") (r "^0.20") (d #t) (k 2)) (d (n "rusttype") (r "^0.8") (d #t) (k 2)))) (h "12ahi4rnynzcy2cxgpjzn244fv71aayhj8mbq9dfycfj0ycp185b") (f (quote (("std") ("default" "std"))))))

(define-public crate-colorous-1.0.4 (c (n "colorous") (v "1.0.4") (d (list (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "imageproc") (r "^0.20") (d #t) (k 2)) (d (n "rusttype") (r "^0.8") (d #t) (k 2)))) (h "0nqqawhhnlgs2jnj73gwpcq9vlnp5a5isxdlrijw8sphmj1q2x40") (f (quote (("std") ("default" "std"))))))

(define-public crate-colorous-1.0.5 (c (n "colorous") (v "1.0.5") (d (list (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "imageproc") (r "^0.20") (d #t) (k 2)) (d (n "rusttype") (r "^0.8") (d #t) (k 2)))) (h "0l076377vdwzn32fpz6y4rhwqykcgswzgl1sda2893bldjzzn29p") (f (quote (("std") ("default" "std"))))))

(define-public crate-colorous-1.0.6 (c (n "colorous") (v "1.0.6") (d (list (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "imageproc") (r "^0.22") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 2)))) (h "0r1iygm2h84z3bk4kbvb0cmzgvw806p100dlc3h148bmxqi4i04q") (f (quote (("std") ("default" "std")))) (r "1.32")))

(define-public crate-colorous-1.0.7 (c (n "colorous") (v "1.0.7") (d (list (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "imageproc") (r "^0.23") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 2)))) (h "1n5krg94a57lj2n5w07v7n8b7005qa27fiss2nskkyqbykcn8glw") (f (quote (("std") ("default" "std")))) (r "1.32")))

(define-public crate-colorous-1.0.8 (c (n "colorous") (v "1.0.8") (d (list (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "imageproc") (r "^0.23") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 2)))) (h "10gl9g2jy3jickyg22zqakhmxj7ykrp1c247ych5algd70kkjbl8") (f (quote (("std") ("default" "std")))) (r "1.32")))

(define-public crate-colorous-1.0.9 (c (n "colorous") (v "1.0.9") (d (list (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "imageproc") (r "^0.23") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 2)))) (h "0pdzjr5ijli1kgn3qm9g1dds4xm24w7i9bcyq1zf83firapwlx98") (f (quote (("std") ("default" "std")))) (r "1.32")))

(define-public crate-colorous-1.0.10 (c (n "colorous") (v "1.0.10") (d (list (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "imageproc") (r "^0.23") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 2)))) (h "0jz2zgiz73z4lq48dwhhx1syy3rrnlars453pvhbx36m5ayf1vrr") (f (quote (("std") ("default" "std")))) (r "1.32")))

(define-public crate-colorous-1.0.11 (c (n "colorous") (v "1.0.11") (d (list (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "imageproc") (r "^0.23") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 2)))) (h "0xx33brvq47s1w6k0pksx4l5z9y34x6p1wv2sf7aqkw818kx7l6h") (f (quote (("std") ("default" "std")))) (r "1.32")))

(define-public crate-colorous-1.0.12 (c (n "colorous") (v "1.0.12") (d (list (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "imageproc") (r "^0.23") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 2)))) (h "1crqxkhpvwjcnjggp2qjs2mzyd1xrv3drgqq4bzlhi9ggj687c3y") (f (quote (("std") ("default" "std")))) (r "1.32")))

(define-public crate-colorous-1.0.13 (c (n "colorous") (v "1.0.13") (d (list (d (n "ab_glyph") (r "^0.2") (d #t) (k 2)) (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.25") (d #t) (k 2)) (d (n "imageproc") (r "^0.24") (d #t) (k 2)))) (h "0hmhikyr8ziizdscy0nb8b4gcy385mrfmary1x5rqv6sqbapa6zm") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-colorous-1.0.14 (c (n "colorous") (v "1.0.14") (d (list (d (n "ab_glyph") (r "^0.2") (d #t) (k 2)) (d (n "dejavu") (r "=2.37.0") (d #t) (k 2)) (d (n "image") (r "^0.25") (d #t) (k 2)) (d (n "imageproc") (r "^0.24") (d #t) (k 2)))) (h "1gbk3gfsgizjn0zi28lgcfrirp7rnjmvdh7mprc7nkq9xvfd21hg") (f (quote (("std") ("default" "std")))) (r "1.60")))

