(define-module (crates-io co lo color-ansi) #:use-module (crates-io))

(define-public crate-color-ansi-0.0.0 (c (n "color-ansi") (v "0.0.0") (h "05bhy69fbwzl8sxdf9cb181ky8y50x22xjz5fchp3b50aqz0m7pj") (f (quote (("default"))))))

(define-public crate-color-ansi-0.0.1 (c (n "color-ansi") (v "0.0.1") (h "146sgnh86a8qbvkihg5z7r9qrgc309zi3zy1wivv40ggx3gbb422") (f (quote (("default"))))))

(define-public crate-color-ansi-0.1.0 (c (n "color-ansi") (v "0.1.0") (h "13jacpswzb59yp9aq9frpfidcd8y0axspc7xdnxv84mb7mbn0r75") (f (quote (("default"))))))

