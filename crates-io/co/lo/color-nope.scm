(define-module (crates-io co lo color-nope) #:use-module (crates-io))

(define-public crate-color-nope-0.1.0 (c (n "color-nope") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "0l9xbp6ld39x0v3cjrpz0kzxbb1nlwg30065hipky882l1699dqh")))

(define-public crate-color-nope-0.2.0 (c (n "color-nope") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "1850vb5k11ayg601hvyc1782f5x6lp4mh6w4rvp4jdb7b1x6dbpz")))

(define-public crate-color-nope-0.3.0 (c (n "color-nope") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "0bsnh1nk5yrzlswm8r31qdq3rdrf2injqgcrql863y06cr11pgs5")))

(define-public crate-color-nope-0.4.0 (c (n "color-nope") (v "0.4.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "1mm611iv5pd3y5is57pll2nxyc0p62kp33wgkfdnqd3404yqwix1")))

