(define-module (crates-io co lo colorconv) #:use-module (crates-io))

(define-public crate-colorconv-0.1.0 (c (n "colorconv") (v "0.1.0") (d (list (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1xkz082hq4r2i90k0qa1fijly3g7n9f8mj1gkj12nrhx18298gdr")))

(define-public crate-colorconv-0.1.1 (c (n "colorconv") (v "0.1.1") (d (list (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1fqcc788a8nhfjbzpyacl5pggisks5d2syb8d112334c3fd1hw76")))

