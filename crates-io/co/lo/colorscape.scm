(define-module (crates-io co lo colorscape) #:use-module (crates-io))

(define-public crate-colorscape-0.1.0 (c (n "colorscape") (v "0.1.0") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (k 0)))) (h "0l2lqrkw88l153pb5slcpalmyappjx0q9pnr2y09dxald7f0yk2f")))

(define-public crate-colorscape-0.1.1 (c (n "colorscape") (v "0.1.1") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (k 0)))) (h "0qqfabpjwqd4rglijb6d4f9wg0q19rkx6na91734sy1cklv04cvx")))

(define-public crate-colorscape-0.1.2 (c (n "colorscape") (v "0.1.2") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (k 0)))) (h "0jlkgww1hamjd6xn8sql6xwlcrlpf01y1cgprcyf5il06mi1j2kn")))

