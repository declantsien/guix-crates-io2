(define-module (crates-io co lo coloured-strings) #:use-module (crates-io))

(define-public crate-coloured-strings-0.1.0 (c (n "coloured-strings") (v "0.1.0") (h "1s11vvazk7152zzsd9ddjaml18ic0vmz30397y54w3q4azkzarck")))

(define-public crate-coloured-strings-0.1.1 (c (n "coloured-strings") (v "0.1.1") (h "0aajpb5vnlx25kjaj1qyya4kxin51wa5f8lsaz4ffzlf7vzss1ng")))

(define-public crate-coloured-strings-0.1.2 (c (n "coloured-strings") (v "0.1.2") (h "1dapb9r9arkb7ykksfhyg0nljvmx3a36iwhc8krpdk6jlxav8mzr")))

(define-public crate-coloured-strings-0.1.3 (c (n "coloured-strings") (v "0.1.3") (h "1cav4vqpwqvdh6bndssvvsdnip1p1ll3cq8b3vm4r9wpkfw72rgh")))

(define-public crate-coloured-strings-0.1.4 (c (n "coloured-strings") (v "0.1.4") (h "0kasl4cq5r40xb9xfd90159zbkp0fzcn7sr5ws980j3a44zjpr0v") (y #t)))

(define-public crate-coloured-strings-0.1.5 (c (n "coloured-strings") (v "0.1.5") (h "09rc3nj176j6ldbvr4c9d9pfwbs4q6jhk7zjd1v3q38b1ykkz3js")))

(define-public crate-coloured-strings-0.1.6 (c (n "coloured-strings") (v "0.1.6") (h "1ag5067xw63p670xdzqb41i2d9ly0dvqj5ixpvbimlkxbm5xy8bz")))

(define-public crate-coloured-strings-0.1.7 (c (n "coloured-strings") (v "0.1.7") (h "1yqzw9h5a1k9rl2nsv8pq570rvqfmzgajd44800vkmli69sm2js1")))

(define-public crate-coloured-strings-0.1.8 (c (n "coloured-strings") (v "0.1.8") (h "11kg57w2riqdafsbwxk5h2vkrchp7b77qkv84a65ihfggm7gh9gd") (y #t)))

(define-public crate-coloured-strings-0.1.9 (c (n "coloured-strings") (v "0.1.9") (h "0mx7f9pkinb5wknpcan3c15s38adqfih2ghi395nqfp6imc42yq0")))

(define-public crate-coloured-strings-0.1.10 (c (n "coloured-strings") (v "0.1.10") (h "1r07ss9cgk1hwj1z8mc3nm0lb75yri2rfc4sxmxvghlx25v7rll0")))

