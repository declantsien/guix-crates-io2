(define-module (crates-io co lo colornamer) #:use-module (crates-io))

(define-public crate-colornamer-0.1.0 (c (n "colornamer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "vpsearch") (r "^1.3.4") (d #t) (k 0)))) (h "1wqa3alzvniwa3sybki11bqx0qw4g15jkkxqvk103frdpjwm3fwn")))

(define-public crate-colornamer-1.0.1 (c (n "colornamer") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0mqrh65l4pvpk6987pfr5dqn3dv9wsxdrbs41qskm2zc84pp1wdv")))

