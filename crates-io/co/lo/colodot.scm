(define-module (crates-io co lo colodot) #:use-module (crates-io))

(define-public crate-colodot-0.1.0 (c (n "colodot") (v "0.1.0") (h "00m6bcxzji9vkiqg0mzsg559vpdc413v62b9h6ngdk10c3l9vl45")))

(define-public crate-colodot-0.1.1 (c (n "colodot") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1mhs5816wsv4ws8kpnrqim41apy1ydzkq0gfcf8fp89c0bw4w8xp")))

(define-public crate-colodot-0.1.2 (c (n "colodot") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0fs30c3cm8sdw9gnrndks4fw0w1nbivrcb5w611rlpwsqlrp3dxb")))

(define-public crate-colodot-0.1.3 (c (n "colodot") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "18gcn27znaapcj3bcn2nfjgzffddxny7pb07gg0m975famysiqd3")))

(define-public crate-colodot-0.1.4 (c (n "colodot") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0g3rqamw4mfks20a3svnvzi8py0xkm97jy8n9kwq1bbv57i91d8j")))

(define-public crate-colodot-0.1.5 (c (n "colodot") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0xlwg7fzp4ijxvv7s6dimimd5madc2afpzknxrlp4n3lpqvxnycd")))

