(define-module (crates-io co lo colorbuddy) #:use-module (crates-io))

(define-public crate-colorbuddy-0.1.1 (c (n "colorbuddy") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("derive" "suggestions" "color"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "~0.24.4") (d #t) (k 0)) (d (n "mcq") (r "^0.1.0") (d #t) (k 0)))) (h "0bpnbq82qm40vv5vzvyd9jy991156ayrfap7c5f7w0986wlrlqg1") (y #t)))

(define-public crate-colorbuddy-0.1.2 (c (n "colorbuddy") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("derive" "suggestions" "color"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "~0.24.4") (d #t) (k 0)) (d (n "mcq") (r "^0.1.0") (d #t) (k 0)))) (h "1adxqryl0w3jw4zqvqrw7v90gqbfz76f9cnhm46cnkkp90kfazy3")))

(define-public crate-colorbuddy-0.1.3 (c (n "colorbuddy") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("derive" "suggestions" "color"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "~0.24.4") (d #t) (k 0)) (d (n "mcq") (r "^0.1.0") (d #t) (k 0)))) (h "0w1dzcvsf6dyn4kk3ni1fjxlhzr02cxiby5yb8vjmfh2dj1hmj70")))

(define-public crate-colorbuddy-0.1.4 (c (n "colorbuddy") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("derive" "suggestions" "color"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "~0.24.4") (d #t) (k 0)) (d (n "mcq") (r "^0.1.0") (d #t) (k 0)))) (h "1xy914qh6jy2lkfdfyknwk7s32072hy3gcnw9i5m03v1z9nk5n7i")))

(define-public crate-colorbuddy-0.1.5 (c (n "colorbuddy") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("derive" "suggestions" "color"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "~0.24.4") (d #t) (k 0)) (d (n "mcq") (r "^0.1.0") (d #t) (k 0)))) (h "05jwhk3s1jw2bh93lxanpy7h7f2f9savk1q8l7kps3amd47jmd0j")))

(define-public crate-colorbuddy-0.1.6 (c (n "colorbuddy") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("derive" "suggestions" "color"))) (d #t) (k 0)) (d (n "exoquant") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "~0.24.4") (d #t) (k 0)) (d (n "mcq") (r "^0.1.0") (d #t) (k 0)))) (h "0jhrxkp37yiihhd9abl4a8bjabpkpga9i5pmkj5hy6hsl9k22sxn")))

