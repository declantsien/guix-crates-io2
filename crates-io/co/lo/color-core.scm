(define-module (crates-io co lo color-core) #:use-module (crates-io))

(define-public crate-color-core-0.1.0 (c (n "color-core") (v "0.1.0") (h "0rf8wkkkswz1fn74vffc8gplpn8i9d6gfi2vf7v1645w22rs6api")))

(define-public crate-color-core-0.1.1 (c (n "color-core") (v "0.1.1") (h "1j6c7m7gjcv1hd79y7d9h484qgww79km2limrks0lra9s61bmjx5") (f (quote (("strict") ("default"))))))

(define-public crate-color-core-0.1.2 (c (n "color-core") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0bb05af58wc6w1b15h7zz8gll3r0yp0s3rgakbs8nj8fs4rzwyjg") (f (quote (("strict") ("default"))))))

(define-public crate-color-core-0.1.3 (c (n "color-core") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05ydahkv0sk3qd1mmjax4gqy24c74a4fbacz0wbwpm4d2yk03vrl") (f (quote (("strict") ("default" "serde"))))))

(define-public crate-color-core-0.1.4 (c (n "color-core") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vzhwnipriari3yl928marhyvara3f642sgngc43xhlvfnq24rrh") (f (quote (("strict") ("default" "serde"))))))

(define-public crate-color-core-0.1.5 (c (n "color-core") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gi6h3myx0wjz7xjdkkwk3vfm3vsrlqbg0lp05xnl5fqqy9928db") (f (quote (("default"))))))

(define-public crate-color-core-0.1.6 (c (n "color-core") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h5xxdbvxrh4rryxlgvy85zccfrjk25pwb8i5z3dw2b2fy2c3k5v") (f (quote (("default"))))))

