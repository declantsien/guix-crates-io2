(define-module (crates-io co lo colorizex) #:use-module (crates-io))

(define-public crate-colorizex-0.1.0 (c (n "colorizex") (v "0.1.0") (d (list (d (n "colored") (r "1.*") (d #t) (k 0)) (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "error-chain") (r "0.*") (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "regex") (r "0.*") (d #t) (k 0)) (d (n "structopt") (r "0.*") (d #t) (k 0)) (d (n "structopt-derive") (r "0.*") (d #t) (k 0)))) (h "1c1la9bpn4ai5zf8hfxzi6rs48d0924jqc2qz12cihhy8rk14jjv")))

(define-public crate-colorizex-0.1.1 (c (n "colorizex") (v "0.1.1") (d (list (d (n "colored") (r "1.*") (d #t) (k 0)) (d (n "env_logger") (r "0.*") (d #t) (k 2)) (d (n "error-chain") (r "0.*") (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "regex") (r "0.*") (d #t) (k 0)) (d (n "structopt") (r "0.*") (d #t) (k 0)) (d (n "structopt-derive") (r "0.*") (d #t) (k 0)))) (h "0xprjnbr22zz31vg86ys4v23yya17vqcpspg6bg2mrh1lnylr04j")))

(define-public crate-colorizex-0.1.2 (c (n "colorizex") (v "0.1.2") (d (list (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "15d3msawvvkvn1qij1lw68fjqznjdrp2qnf9g7zl9z533g27ghmr")))

(define-public crate-colorizex-0.1.3 (c (n "colorizex") (v "0.1.3") (d (list (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)))) (h "10khc182vzadmbsd53lb40vw0wq458g18zic7qrmaw7df0p5827d")))

