(define-module (crates-io co lo color-reduction) #:use-module (crates-io))

(define-public crate-color-reduction-0.1.0 (c (n "color-reduction") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1v5f7jpaifgp2r89b3ad7iacxd58kdjfsw0gzsh4gbzskx8avqfv") (y #t)))

(define-public crate-color-reduction-0.1.1 (c (n "color-reduction") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0gvvilz73vvlr2d4a7g3bwczwglsan3sdk8qqf2ybzxhq7i4jr7d") (y #t)))

(define-public crate-color-reduction-0.1.2 (c (n "color-reduction") (v "0.1.2") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0ch7lrkgix6j54i5mp8wabxvzy1fzpiwyy984sv4hzgbr1vhwgs9") (y #t)))

