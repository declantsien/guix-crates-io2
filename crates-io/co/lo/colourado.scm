(define-module (crates-io co lo colourado) #:use-module (crates-io))

(define-public crate-colourado-0.1.0 (c (n "colourado") (v "0.1.0") (d (list (d (n "piston_window") (r "^0.87") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0z46ih33wv5ndrcsbrw63p0c2lqsgazh0gvgm8igsnci5jjfhwpf")))

(define-public crate-colourado-0.1.1 (c (n "colourado") (v "0.1.1") (d (list (d (n "piston_window") (r "^0.87") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0gky367cvwc1ypw8sqadr32i2nqbk4mfl03i343hx67w5yiigrba")))

(define-public crate-colourado-0.2.0 (c (n "colourado") (v "0.2.0") (d (list (d (n "piston_window") (r "^0.87") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0zkj7cv8g1ikyya2zqvdjyhkq7i6nbhb5a3xj4x843z6qrzb8l8x")))

