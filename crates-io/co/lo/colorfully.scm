(define-module (crates-io co lo colorfully) #:use-module (crates-io))

(define-public crate-colorfully-0.1.0 (c (n "colorfully") (v "0.1.0") (h "0laxl5d52pgd9zlhsifd8j7hz2kwd6658jx9h1kw10fr7wrxskhk")))

(define-public crate-colorfully-0.2.0 (c (n "colorfully") (v "0.2.0") (h "02i3jz5n9ycjm5v50acghj1mzq0rqfkzi5wqqyqc989qlccvqpw3")))

(define-public crate-colorfully-0.2.1 (c (n "colorfully") (v "0.2.1") (h "1ll35p4rhlnxma1wccqkd3y57p06p923mqlqi8jywa2pfvbi0ymn")))

(define-public crate-colorfully-0.2.2 (c (n "colorfully") (v "0.2.2") (h "0q5ls2rlxhf698sdx31x3hvaxpc760lpmrrapbhi6w70359xgyzv")))

