(define-module (crates-io co lo color-name) #:use-module (crates-io))

(define-public crate-color-name-1.0.0 (c (n "color-name") (v "1.0.0") (h "0lgpjw4a5mf3agkyk7y9i3z27x5nmickq3pbhss19y0kkcyczif1")))

(define-public crate-color-name-1.1.0 (c (n "color-name") (v "1.1.0") (h "0p6n84snqiws30aqwsbwc0z0da03vjr85gg2l6b1037nfdsvvjn8")))

