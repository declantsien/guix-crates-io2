(define-module (crates-io co lo colorz) #:use-module (crates-io))

(define-public crate-colorz-1.0.0 (c (n "colorz") (v "1.0.0") (d (list (d (n "supports-color") (r "^2") (o #t) (d #t) (k 0)))) (h "1zynzvyb2qmjqfgxhnc7avg2wzj9hvsgx6v0aa7rjbskx6y1rwbh") (f (quote (("strip-colors") ("std" "alloc") ("alloc"))))))

(define-public crate-colorz-1.1.0 (c (n "colorz") (v "1.1.0") (d (list (d (n "supports-color") (r "^2") (o #t) (d #t) (k 0)))) (h "1i7s292lbahivl6myj3r4c4rqn8nm9ig828fpqnr6l3j55bib5n1") (f (quote (("strip-colors") ("std" "alloc") ("alloc"))))))

(define-public crate-colorz-1.1.1 (c (n "colorz") (v "1.1.1") (d (list (d (n "supports-color") (r "^2") (o #t) (d #t) (k 0)))) (h "04l7ra5380ml2iqzj63rkwq0abh0fh0jgi6mbwixj15zscl6k4kp") (f (quote (("strip-colors") ("std" "alloc") ("alloc"))))))

(define-public crate-colorz-1.1.2 (c (n "colorz") (v "1.1.2") (d (list (d (n "supports-color") (r "^2") (o #t) (d #t) (k 0)))) (h "112igkaxhg8kv47rd79blzzzrik1cxrsg4qcj8v2mm8qxvv5samw") (f (quote (("strip-colors") ("std" "alloc") ("alloc"))))))

