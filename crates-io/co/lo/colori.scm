(define-module (crates-io co lo colori) #:use-module (crates-io))

(define-public crate-colori-0.1.0 (c (n "colori") (v "0.1.0") (h "08l7jcd11ngq4sgrciljljq6k6p8mffsyn2p044wi094m45gcips")))

(define-public crate-colori-0.1.1 (c (n "colori") (v "0.1.1") (h "1grmxiilc9m3zg9nwbm3097xbkpsby6lpay0v6kh137pz2414r3i")))

