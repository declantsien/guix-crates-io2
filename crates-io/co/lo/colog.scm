(define-module (crates-io co lo colog) #:use-module (crates-io))

(define-public crate-colog-0.1.1 (c (n "colog") (v "0.1.1") (d (list (d (n "colored") (r "^1.3") (d #t) (k 0)) (d (n "env_logger") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)))) (h "02in7jkpzy9h12lq024l7gf647khfnvz7gm1vhz8aiw4vn0i4mmp")))

(define-public crate-colog-0.2.0 (c (n "colog") (v "0.2.0") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0dj0icf87f7jjv6bhjqxf1cg1xgkx28mwla1z92clvz47p39id5d")))

(define-public crate-colog-0.2.1 (c (n "colog") (v "0.2.1") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0nzq1cr5ajjq55zkglisy3hlsgid12ncyhxsrfsfh9g710976gln")))

(define-public crate-colog-1.0.0 (c (n "colog") (v "1.0.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1q378yzkj4ni6mi5kqgh58g7saw9a3jy8lm3x0r4vdmm01kkpwqw")))

(define-public crate-colog-1.1.0 (c (n "colog") (v "1.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dflhvk9wgc1rlp9kpkc0rfjsrbq1qkmll610zsfq9qh3dd1hvnp")))

(define-public crate-colog-1.2.0 (c (n "colog") (v "1.2.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14v075pw6334cwdh686swqs4dx36ba2khaqyg4d55n7f51j71rs4")))

(define-public crate-colog-1.3.0 (c (n "colog") (v "1.3.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1w4xa5p3jjv51pxnhh58igbg0cff69k9j4v7vrwsvq6mz1x6nhic")))

