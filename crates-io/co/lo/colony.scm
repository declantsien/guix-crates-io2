(define-module (crates-io co lo colony) #:use-module (crates-io))

(define-public crate-colony-0.1.0 (c (n "colony") (v "0.1.0") (d (list (d (n "iai") (r "^0.1.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "0glzqj47fpj9v31bby8xc93w94cd76r8ic89w216m4iwbi2qnxzy")))

(define-public crate-colony-0.2.0 (c (n "colony") (v "0.2.0") (d (list (d (n "iai") (r "^0.1.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "10v6i3cfymfqbdwd2jslbwr2lcrhzj9b7pymi62jnj3wfhb80hs6")))

(define-public crate-colony-0.3.0 (c (n "colony") (v "0.3.0") (d (list (d (n "iai") (r "^0.1.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "0gs1sd7cfhb7wjl95afprpf6aab7ha1xnzr6shxg9g56z6bfnqpg")))

