(define-module (crates-io co lo colorizer) #:use-module (crates-io))

(define-public crate-colorizer-0.1.0 (c (n "colorizer") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1b8h55zxd4czzzj960km94520zj57208pfdlnqfcs1yyn7jgz8bs")))

(define-public crate-colorizer-0.2.0 (c (n "colorizer") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1pf3dvd387k3z774msi57c5xgj981pxs2b5kaw7lfv4piq30sq7i")))

(define-public crate-colorizer-1.0.0 (c (n "colorizer") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "15df2y3lzzf7yja4ar00f3ismgxri53aqg8vrrm3qr38887gwx02")))

(define-public crate-colorizer-1.0.1 (c (n "colorizer") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0nqs5gp9a8rvdl6rnys5fkk8p50naa03kmc8fijz26fxjiixidav")))

(define-public crate-colorizer-1.1.0 (c (n "colorizer") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1dc95hrhmx28vq6dypalg0gia89plgrd0ak4xqgx0rw4kqf7fxr3")))

(define-public crate-colorizer-1.1.1 (c (n "colorizer") (v "1.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0qwh3kbczcbnk03xir91v0pgnwg243raxmbc3pj7jcrsavfwl9bz")))

(define-public crate-colorizer-1.1.2 (c (n "colorizer") (v "1.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1sy64fj7zxmdxqi0n3vn846aym1mpyx0dzab3pw8q3jvdgcwjvrd")))

(define-public crate-colorizer-2.0.0 (c (n "colorizer") (v "2.0.0") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "17013wiky062x7dgdnqnaxpn2jrgd687lypihp902l5x0jxnp6s1")))

(define-public crate-colorizer-2.1.0 (c (n "colorizer") (v "2.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1h60fqymaciivyi6643srv8vyyb5dff9lhsm591yvij52yb5rpiz")))

