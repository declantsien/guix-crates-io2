(define-module (crates-io co lo color-operators) #:use-module (crates-io))

(define-public crate-color-operators-0.0.1 (c (n "color-operators") (v "0.0.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "135l9g75lhzyzwywzyjybykaslgjaq9iiacfcgirsqxnqals5v18")))

(define-public crate-color-operators-0.0.2 (c (n "color-operators") (v "0.0.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "13fvfkmj96x020j1y436h8a10gv5r942fxpmg3nv364n7sks8n5d")))

(define-public crate-color-operators-0.0.3 (c (n "color-operators") (v "0.0.3") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1mg1fppzljz1pm2wkhr4aq55szrdak1qzbv27zplimh5q48nlq6v")))

