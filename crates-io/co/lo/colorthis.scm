(define-module (crates-io co lo colorthis) #:use-module (crates-io))

(define-public crate-colorthis-0.1.0 (c (n "colorthis") (v "0.1.0") (d (list (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "parse-color") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "13k27n76kd5v1xjnxckz3ixdrlard01zdi9f6956xss2diawp2i5") (f (quote (("unchecked") ("tailwind" "parse-color") ("default" "tailwind") ("compact") ("clamp" "unchecked"))))))

(define-public crate-colorthis-0.1.1 (c (n "colorthis") (v "0.1.1") (d (list (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "parse-color") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0qpwwi1dl5lcxz77axxnpy53b5sl3vyslmk2q6f98h2dz9i1n70h") (f (quote (("unchecked") ("tailwind" "parse-color") ("default" "tailwind") ("compact") ("clamp" "unchecked"))))))

