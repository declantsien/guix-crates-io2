(define-module (crates-io co lo colossal) #:use-module (crates-io))

(define-public crate-colossal-0.1.0 (c (n "colossal") (v "0.1.0") (h "1gpngdsln4dwivdphw3p64hjz8xrbg8rycqv1nkby2a77d5m21fg")))

(define-public crate-colossal-0.1.1 (c (n "colossal") (v "0.1.1") (h "0vbihw3l5sl3sx5lrm8yy7jpdybs0qadpvvbpdshhxj8xbmq2sdi")))

(define-public crate-colossal-0.1.2 (c (n "colossal") (v "0.1.2") (h "1c9lq2fzz8i5cakcx0g7qc7x65ynz931rwgc4ylc993bp0n4q1z3")))

(define-public crate-colossal-0.1.3 (c (n "colossal") (v "0.1.3") (h "1fn3dd27v2s22hhz4swfc9w5c2f2sxm07s5l5fpsk27laxl8mi1j")))

(define-public crate-colossal-1.0.0 (c (n "colossal") (v "1.0.0") (h "119yqxdi5zg7i67z7c8ml6x4vd4n535r3x583ljda9kvcnj61w0l")))

(define-public crate-colossal-1.0.1 (c (n "colossal") (v "1.0.1") (h "0sb7fi2k7yl0is1fdrmphsg463rmcrsd92xv1fq5v44myr7vx37c")))

