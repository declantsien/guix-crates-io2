(define-module (crates-io co lo color_scaling) #:use-module (crates-io))

(define-public crate-color_scaling-0.0.1 (c (n "color_scaling") (v "0.0.1") (d (list (d (n "image") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "19qnml5kf1gpirq6vanaca4nhyk8vzdgcvrj0vipr3mqdhc7a2jq")))

(define-public crate-color_scaling-0.0.2 (c (n "color_scaling") (v "0.0.2") (d (list (d (n "image") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0kq3xbw0y0cylyxng0qkfxxgwmdqmdfh5zv0rnnxxr64rhwwmpgi")))

(define-public crate-color_scaling-0.0.3 (c (n "color_scaling") (v "0.0.3") (d (list (d (n "image") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "11apl9i2j1vyq0i0pvdkxcrxivil2g3ndf5hkl9griv028llpdjn")))

(define-public crate-color_scaling-0.0.4 (c (n "color_scaling") (v "0.0.4") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0bk3h9na0s5iiwms8yqgw1srw4h3hcknr4xs5mm9m7h97lzwy6jp")))

(define-public crate-color_scaling-0.0.5 (c (n "color_scaling") (v "0.0.5") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "122f042lh082vfl12lgnmfx81jwp8izl4hwmhzrhkx6cjcmay2ch")))

