(define-module (crates-io co lo color_names) #:use-module (crates-io))

(define-public crate-color_names-0.1.0 (c (n "color_names") (v "0.1.0") (h "017549x5xxixsdz5ayqv1bk27500xaljcfn1snxw50gdv61nalrm")))

(define-public crate-color_names-1.0.0 (c (n "color_names") (v "1.0.0") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1bc7a25dnxlz6zirn90phbjvqdp1c6mprck15n32aibwlzj3xzh7")))

