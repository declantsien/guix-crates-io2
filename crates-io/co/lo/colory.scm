(define-module (crates-io co lo colory) #:use-module (crates-io))

(define-public crate-colory-0.1.0 (c (n "colory") (v "0.1.0") (h "0jafl2vmn4b5sk7asz79drln1s920717pv4b6jlay1gphj33bwd9")))

(define-public crate-colory-0.2.0 (c (n "colory") (v "0.2.0") (h "15b9gziqna7k2i8fl38wnairygpz94ijzwyxa55nrkcnddnmfajd")))

(define-public crate-colory-0.2.1 (c (n "colory") (v "0.2.1") (h "0lyia0ij938yxkdpsybl41fnyqkirwfadzkqd61v3ddng5ljf591")))

(define-public crate-colory-0.2.2 (c (n "colory") (v "0.2.2") (h "0fh9k8bmrhww4jgkzkr9mw2v17jr73fh09sijx7gvmd1375hmq36")))

(define-public crate-colory-0.3.0 (c (n "colory") (v "0.3.0") (h "05305skjwgmcd96a3rrq6mzgy7pmy03a1sa3fssirv08s5ivfmnn")))

(define-public crate-colory-0.4.0 (c (n "colory") (v "0.4.0") (h "0wmcggngvhsdyg09l20a921qnib0p619c24jzgiwfndy7s39c2zf")))

(define-public crate-colory-0.4.1 (c (n "colory") (v "0.4.1") (h "0wbx43hb9nzd1jgqmwdxgqdprvjmm69iwkn5r9xhkci4140y9plq")))

(define-public crate-colory-0.5.0 (c (n "colory") (v "0.5.0") (h "186arz7103rfim5lpw9z303g0vy6xkid6j6s1k8zmf6asjk9kwgk")))

