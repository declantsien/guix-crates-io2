(define-module (crates-io co lo colorify) #:use-module (crates-io))

(define-public crate-colorify-0.1.0 (c (n "colorify") (v "0.1.0") (h "1sank3vh3xxvwhgq6lhcigk22aykp3l3jq39r1zzvl0adq0ijh02")))

(define-public crate-colorify-0.2.0 (c (n "colorify") (v "0.2.0") (h "1h1h19q2xdhj1y4mgvcszwsh82l3ysgcd7kwxrsl9rp4n6mr53wf")))

(define-public crate-colorify-0.2.1 (c (n "colorify") (v "0.2.1") (h "0wfk4a8hx7q4sfrcdfgnzyzpv49mb246bzjp4pi7qkkb44bwmzv2")))

(define-public crate-colorify-0.2.2 (c (n "colorify") (v "0.2.2") (h "0n8ws05d8z612rclslbsa13d5mcb5brf5cygxs4cmhrc739hdc1y")))

(define-public crate-colorify-0.2.3 (c (n "colorify") (v "0.2.3") (h "0rfawkhy8bv2pjqdgr2q5fld16rjqm53i8ng7b1l09lcqxj9paq7") (f (quote (("enable_windows"))))))

