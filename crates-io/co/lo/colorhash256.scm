(define-module (crates-io co lo colorhash256) #:use-module (crates-io))

(define-public crate-colorhash256-0.1.0 (c (n "colorhash256") (v "0.1.0") (d (list (d (n "sodiumoxide") (r "*") (d #t) (k 0)))) (h "0manh442l90hsxbv0iqscgcb447v8v4pizfj5maxxz230p7d6jjp")))

(define-public crate-colorhash256-0.1.1 (c (n "colorhash256") (v "0.1.1") (d (list (d (n "sodiumoxide") (r "^0") (d #t) (k 0)))) (h "11bpx96v1vmz62bmw8nyi2f8k99fmxidnr1zjd52jbjpwqjmh2lg")))

(define-public crate-colorhash256-0.1.2 (c (n "colorhash256") (v "0.1.2") (d (list (d (n "sodiumoxide") (r "^0") (d #t) (k 0)))) (h "12gz33k8rkwylhijsb1083cdjg9nnph2v2d6dwq4w1s0161jm97b")))

