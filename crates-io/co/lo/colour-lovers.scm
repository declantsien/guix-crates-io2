(define-module (crates-io co lo colour-lovers) #:use-module (crates-io))

(define-public crate-colour-lovers-0.0.0 (c (n "colour-lovers") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)))) (h "1pz9gf2vjz15q3xvgkwm0z8mcn2nl897js78zy5i49ckacsy50ma")))

