(define-module (crates-io co lo colorblind) #:use-module (crates-io))

(define-public crate-colorblind-0.0.1 (c (n "colorblind") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "human-panic") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10hiff1c3bnbcc2pgiixfg0qwc5ac82a9dbcarz354c9b5ig0jb3")))

