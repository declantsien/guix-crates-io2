(define-module (crates-io co lo colourizer) #:use-module (crates-io))

(define-public crate-colourizer-0.1.0 (c (n "colourizer") (v "0.1.0") (d (list (d (n "rargsxd") (r "^0.1.7") (d #t) (k 0)))) (h "0jwh8yvxcafm70vf9pws2lfw68239fmvk62vixacia8wnz0yk8f2")))

(define-public crate-colourizer-0.1.1 (c (n "colourizer") (v "0.1.1") (d (list (d (n "rargsxd") (r "^0.1.7") (d #t) (k 0)))) (h "1ghmygfnrcdgcl9xj81jqswccbmji4k179v49qlxmp9nxkxs57g9")))

(define-public crate-colourizer-0.1.2 (c (n "colourizer") (v "0.1.2") (d (list (d (n "rargsxd") (r "^0.1.8") (d #t) (k 0)))) (h "145b124dql0m8hi7j3x5rnbpbbv19w62p70ca9ib4kw74yym4vhw")))

(define-public crate-colourizer-0.1.3 (c (n "colourizer") (v "0.1.3") (d (list (d (n "rargsxd") (r "^0.1.8") (d #t) (k 0)))) (h "0phl6plvr4db156xva4hh03jb2fngzkrf0r51wgj6gqa65cw2qh6")))

(define-public crate-colourizer-0.1.4 (c (n "colourizer") (v "0.1.4") (d (list (d (n "rargsxd") (r "^0.1.8") (d #t) (k 0)))) (h "0xqazyg9prvvzwxa2bkvd9chr9cf05z53g48xfs94ag6wyarbixm")))

