(define-module (crates-io co lo color-string) #:use-module (crates-io))

(define-public crate-color-string-0.1.0 (c (n "color-string") (v "0.1.0") (h "0hy2g34v8jpaqv43jq1m09j5ik6lnpk96pnra8338ddz47cpan10")))

(define-public crate-color-string-0.1.1 (c (n "color-string") (v "0.1.1") (h "0nn1m3vw19z0x2gjfw2kdyrxyc88l7g2kgjrmykaqb041i3wx5mi")))

(define-public crate-color-string-0.1.2 (c (n "color-string") (v "0.1.2") (h "14f6d3shkbrmlxab4b9cds672z6wxlvx0fvxczk7yjix1vk9g2sc")))

(define-public crate-color-string-0.1.3 (c (n "color-string") (v "0.1.3") (h "0xczhx0iyqyjlwdavxzmbisy3vm1g2l4l4p44b1g394lcjc4xlm5")))

(define-public crate-color-string-0.1.4 (c (n "color-string") (v "0.1.4") (h "06hv25fkfdm73g63wl5ciwgpbrj7zbb8258s3lrxswcq1x71frb1") (y #t)))

(define-public crate-color-string-0.1.5 (c (n "color-string") (v "0.1.5") (h "05kqpvvng9ch5ll9qw6irnn6sj64klgsc0j69zy1ghyli1086z7w") (y #t)))

(define-public crate-color-string-0.1.6 (c (n "color-string") (v "0.1.6") (h "1rkaxhvk2icsqz8i9bx5sr8xhxk7j0b6iakh9c93pxi36rakz02x")))

