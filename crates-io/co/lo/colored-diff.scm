(define-module (crates-io co lo colored-diff) #:use-module (crates-io))

(define-public crate-colored-diff-0.1.0 (c (n "colored-diff") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (k 0)))) (h "0xha79c838v0cr18qx7w41wvhv5fj2vbhgxa65l02qxapaywidh0")))

(define-public crate-colored-diff-0.1.1 (c (n "colored-diff") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (k 0)))) (h "1dx1ar05ryhmbglsk7f85pxm05x6q8af82shacq6bd7j789n0pgh")))

(define-public crate-colored-diff-0.1.2 (c (n "colored-diff") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (k 0)))) (h "1iasibdkm3m4ch5nx848hjx7yj98c4h5a7gfirinzm21h0napq5d")))

(define-public crate-colored-diff-0.2.0 (c (n "colored-diff") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (k 0)))) (h "1h80g8j9qvmxkn6fqy1vgkqksbzws3la9xdmnw7i7c7p5jq12abd")))

(define-public crate-colored-diff-0.2.1 (c (n "colored-diff") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (k 0)))) (h "0qviim9141j7y5608hgdyhqfvpc2r2apxkpvi4i1c6fw6w2xkjwg")))

(define-public crate-colored-diff-0.2.2 (c (n "colored-diff") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (k 0)))) (h "1zbfjkp7w1wjcxb1p19dd21mn9xkj6nr2s5pav8b16whzh52cvsi")))

(define-public crate-colored-diff-0.2.3 (c (n "colored-diff") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 2)))) (h "1dfwjxd13f8l8bdzm76kkp6cp4sr1pyc8lavp52avwy313mhh0j1")))

