(define-module (crates-io co lo colored) #:use-module (crates-io))

(define-public crate-colored-1.0.0 (c (n "colored") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)))) (h "0hl7w8mpqv59x6rvvyd4jh1xbv4gq6qnwsp82ldnq34kvmkl87s9")))

(define-public crate-colored-1.0.1 (c (n "colored") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)))) (h "0w6ya3j0kcphj14pj4xmfnmdjskcglyxya2hicx002g9g82r2zz2")))

(define-public crate-colored-1.1.0 (c (n "colored") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "19bwkx29sg5ix65v13gam76mm44c8gi6hl5d5kx9d9vh13dnwlk0")))

(define-public crate-colored-1.1.1 (c (n "colored") (v "1.1.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0xvwb578fhl9r8fh5n36d7gl90kj7lysy5fj2mxs6506n986p5q4")))

(define-public crate-colored-1.1.2 (c (n "colored") (v "1.1.2") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0xd0z09v4lfy9kz64nrl8jsdl3zm3xksvd6w5qqw06xnx1v5npij")))

(define-public crate-colored-1.2.0 (c (n "colored") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1qk1ya3xzs3k5sh7n6l8s16dqd6hjfsdnvmazmxhyj0jrc946h5g")))

(define-public crate-colored-1.3.0 (c (n "colored") (v "1.3.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1qvjda8gh13b1z18a9bydgkrn2sln8ckz3kga0w8wqbanbym612y") (f (quote (("no-color"))))))

(define-public crate-colored-1.3.1 (c (n "colored") (v "1.3.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0wkrnbwhwx7hc2fvnyy7wcrjsgmjajl4wvhzii1kz0jqg1c9a51m") (f (quote (("no-color"))))))

(define-public crate-colored-1.3.2 (c (n "colored") (v "1.3.2") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0k2gdck590p94xpsqddmj0smmqmhdr6fhfwnkkgyr84psnjmcmsq") (f (quote (("no-color"))))))

(define-public crate-colored-1.4.0 (c (n "colored") (v "1.4.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0-beta.3") (d #t) (k 2)))) (h "0cn1m981hlkaklg7nrn8wbbj68pda1wpy755lavvw0hfv7bk9rih") (f (quote (("no-color"))))))

(define-public crate-colored-1.4.1 (c (n "colored") (v "1.4.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0-beta.3") (d #t) (k 2)))) (h "0yk9xjn7g48rz7xm9d8nj288li8kv1sav388h3nnrh5hxii95b18") (f (quote (("no-color"))))))

(define-public crate-colored-1.5.0 (c (n "colored") (v "1.5.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0-beta.3") (d #t) (k 2)))) (h "1v09xmkhqmngx910gzcckf1n26f08w9f3vrbawcmr537szmgqs6j") (f (quote (("no-color"))))))

(define-public crate-colored-1.5.1 (c (n "colored") (v "1.5.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0-beta.3") (d #t) (k 2)))) (h "1vjpwfpjf96wqysnliyrwbmk1hppbq7rjgclby850fmszjnkdr36") (f (quote (("no-color"))))))

(define-public crate-colored-1.5.2 (c (n "colored") (v "1.5.2") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rspec") (r "^1.0.0-beta.3") (d #t) (k 2)))) (h "12p31nr8ks2y86vnyrs7j8mcnkvqlw9i3jkx9khy1zakvimmk5cx") (f (quote (("no-color"))))))

(define-public crate-colored-1.5.3 (c (n "colored") (v "1.5.3") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)))) (h "14ydr2gg9983zvnjxk91bdsbxh43djpn70mhm1966c07wala5m61") (f (quote (("no-color"))))))

(define-public crate-colored-1.6.0 (c (n "colored") (v "1.6.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)))) (h "1z67f9jsmgmil5gggsmg9j3pgdcv51mhkmj5k2sn2casx1rk9amh") (f (quote (("no-color"))))))

(define-public crate-colored-1.6.1 (c (n "colored") (v "1.6.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)))) (h "0csmwkzyz1c8074xfmk0cjs5lr5rfkjq1nn452v2zdh1j1kn02nw") (f (quote (("no-color"))))))

(define-public crate-colored-1.7.0 (c (n "colored") (v "1.7.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)))) (h "1a9840vbdn4fl73n6rkkwciy5cc0qcw6496h5zhp2hka2mg4b6kf") (f (quote (("no-color"))))))

(define-public crate-colored-1.8.0 (c (n "colored") (v "1.8.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)) (d (n "winconsole") (r "^0.10.0") (d #t) (k 0)))) (h "00vwd3r2jrd6qz4r91bwqhmkl371wyyjvirrc7bzh9r91yv91nvc") (f (quote (("no-color"))))))

(define-public crate-colored-1.9.0 (c (n "colored") (v "1.9.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "1r9prlrallizq4nlp1ip3q9m98j7gyac9c45xlkq2xhisp3plgj3") (f (quote (("no-color"))))))

(define-public crate-colored-1.9.1 (c (n "colored") (v "1.9.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "0fildacm47g86acmx44yvxx6cka8fdym5qkgfm8x8gh2hsrghc7r") (f (quote (("no-color"))))))

(define-public crate-colored-1.9.2 (c (n "colored") (v "1.9.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "0gaviala6knn5xxz8hqf4h53526fxvxl3q9jzhl9k9gkg2my45c8") (f (quote (("no-color"))))))

(define-public crate-colored-1.9.3 (c (n "colored") (v "1.9.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rspec") (r "= 1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "0nbc1czs512h1k696y7glv1kjrb2b914zpxraic6q5fgv80wizzl") (f (quote (("no-color"))))))

(define-public crate-colored-2.0.0 (c (n "colored") (v "2.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "=1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "1gbcijscmznzy42rn213yp9ima7210zakgaqibgg1n441dsnyqdk") (f (quote (("no-color"))))))

(define-public crate-colored-2.0.1 (c (n "colored") (v "2.0.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "=1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "0i8fy3vyxnsb538k4f078ilqpjmxvysh3s0df282aqzy02aargqp") (f (quote (("no-color"))))))

(define-public crate-colored-2.0.2 (c (n "colored") (v "2.0.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "=1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "0wy75agxbg9fm66g11rsm2s0qshq9cz06bvdjrg473l7vhpq0ibg") (f (quote (("no-color"))))))

(define-public crate-colored-2.0.3 (c (n "colored") (v "2.0.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "=1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "0qyygfyi46v685vv4v6c63b3js3mnpldk5hgqljn2ncswf8x22vg") (f (quote (("no-color")))) (r "1.63")))

(define-public crate-colored-2.0.4 (c (n "colored") (v "2.0.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "=1.0.0-beta.3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xhnlnyv3am5xx0gw5bgrfh33d3p06x44v0yycn02f5w5x4fqx16") (f (quote (("no-color")))) (r "1.63")))

(define-public crate-colored-1.9.4 (c (n "colored") (v "1.9.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "=1.0.0-beta.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "winbase"))) (t "cfg(windows)") (k 0)))) (h "0mc302pm2x0vpmc3ni35w0666858pmqlqzbipyz42cw2j4f78pss") (f (quote (("no-color"))))))

(define-public crate-colored-2.1.0 (c (n "colored") (v "2.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rspec") (r "=1.0.0-beta.3") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1f4h9p64snrnz4x432iza15p4diqjcgpmpvhi956d6r1rq61bwnb") (f (quote (("no-color")))) (r "1.70")))

