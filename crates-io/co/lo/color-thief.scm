(define-module (crates-io co lo color-thief) #:use-module (crates-io))

(define-public crate-color-thief-0.1.0 (c (n "color-thief") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.14") (f (quote ("jpeg" "png_codec"))) (k 2)) (d (n "rgb") (r "^0.7") (d #t) (k 0)))) (h "10cjw7mvpfngvg3q9icl7fbr5va9h3j66phy7xxwjqrska5h569i")))

(define-public crate-color-thief-0.2.0 (c (n "color-thief") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.22") (f (quote ("jpeg" "png_codec"))) (k 2)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "16j8cj3004p2hm3s9yfzayxspbvrhf81dz48qx152g91lcn89954")))

(define-public crate-color-thief-0.2.1 (c (n "color-thief") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.22") (f (quote ("jpeg" "png_codec"))) (k 2)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1y4x3vyrvpbbprkjx06bj6sdnfsmh16q5ja1dhcycp9s6g7w633j")))

(define-public crate-color-thief-0.2.2 (c (n "color-thief") (v "0.2.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.22") (f (quote ("jpeg" "png_codec"))) (k 2)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "163c0q8p4m9cbgy3n2id9z2sq848afbgh603kryfd37k1iv0sip6")))

