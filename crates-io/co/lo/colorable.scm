(define-module (crates-io co lo colorable) #:use-module (crates-io))

(define-public crate-colorable-0.1.0 (c (n "colorable") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.4.1") (d #t) (k 0)))) (h "12vrhr1il5zf459qji3xd8cravnvvrj6rsb9zxwdhnxxlnf28ni0")))

(define-public crate-colorable-0.1.1 (c (n "colorable") (v "0.1.1") (h "1fdlnjbx7rhr4v7ds1bb00wdbwhjyqm64rf7q68aya2c4k19gk72")))

(define-public crate-colorable-0.1.2 (c (n "colorable") (v "0.1.2") (h "0mbq78pfrs5m2nmiflc7cmcx00205jfh7ghs5brrdaqcqwvihzxf")))

(define-public crate-colorable-0.1.3 (c (n "colorable") (v "0.1.3") (h "1awb12z7g3ibq20fjsiqsrxhvvd9bhnq96wr2912ykd2g3b9rwdp")))

(define-public crate-colorable-0.1.4 (c (n "colorable") (v "0.1.4") (h "11pq10z3kg7i3p1skf21sfs5hd3fynz9r9f8lp35cmamwq4rdzs5")))

