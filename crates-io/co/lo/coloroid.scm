(define-module (crates-io co lo coloroid) #:use-module (crates-io))

(define-public crate-coloroid-0.1.0 (c (n "coloroid") (v "0.1.0") (h "1dpkwghv0p49zxc4vdaac2insmd468in668fk0jip620qkrxzamm")))

(define-public crate-coloroid-0.2.0 (c (n "coloroid") (v "0.2.0") (h "1hsdpmmz3ihcwhy2yq69qc7cdz3vmabm1m9kn9i5q7xql8ils4vb")))

(define-public crate-coloroid-0.3.0 (c (n "coloroid") (v "0.3.0") (h "04nbgbqpmp3b0kv0lg0yyd4yngrh2rvf3xhmy731mn61dpz0s1hi")))

(define-public crate-coloroid-0.4.0 (c (n "coloroid") (v "0.4.0") (h "0x1csbwbf363n450n4x1cr1sggav88z1hn0bwknvz3qcc9i6bx5c")))

(define-public crate-coloroid-0.5.0 (c (n "coloroid") (v "0.5.0") (h "1rx173d3gkkbcv6k6c4dgg3lil36hycjy495jxpcnkri1avyablc")))

(define-public crate-coloroid-0.6.0 (c (n "coloroid") (v "0.6.0") (h "0dr8w5azirdwsl9hj91j7z396nb23hljzvm3qnl1dcxlcxibydv7")))

(define-public crate-coloroid-0.7.0 (c (n "coloroid") (v "0.7.0") (h "1rsszcd7s4gbprdfdvhkirxspm0snkd6hb9bb6hycfz93iqrfl9h")))

