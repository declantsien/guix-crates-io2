(define-module (crates-io co lo color_conv) #:use-module (crates-io))

(define-public crate-color_conv-0.1.0 (c (n "color_conv") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1gr5dk397xj4vvrpc5fwisjlsrawfci5fpa0bc5ziwj46zdxzfqg")))

(define-public crate-color_conv-0.2.0 (c (n "color_conv") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1hwk5jz9fb2v4h385mxzff2b8blxi22hm56ga85czfjhhgkz8s90") (y #t)))

(define-public crate-color_conv-0.2.1 (c (n "color_conv") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "15gprk5i8x6j9qxd3xlzp47nfpc1nrxk3bmgyb8vf15g5fxs8qrn")))

