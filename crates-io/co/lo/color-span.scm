(define-module (crates-io co lo color-span) #:use-module (crates-io))

(define-public crate-color-span-0.0.0 (c (n "color-span") (v "0.0.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)))) (h "1im7m90016q0n6lqcd9fa1xpqgzmvcmvidrar6w34j6kharzv0sk") (f (quote (("strict") ("default"))))))

(define-public crate-color-span-0.1.0 (c (n "color-span") (v "0.1.0") (d (list (d (n "color-char") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "0pryrnp8g3biqy8z1r6xqpax7x9arcz4585692wnycr84q33vbyn") (f (quote (("default"))))))

(define-public crate-color-span-0.2.0 (c (n "color-span") (v "0.2.0") (d (list (d (n "code-span") (r "^0.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "16c80w837fx7xhma9a6s2fbribwfwvvgklqz1h6y7q93wwbafy6a") (f (quote (("default"))))))

(define-public crate-color-span-0.2.1 (c (n "color-span") (v "0.2.1") (d (list (d (n "code-span") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "1igfpainqw21bh8lbggajqrjdag4ijjf6fhrn0723rc66mrlj67d") (f (quote (("default"))))))

