(define-module (crates-io co lo colored-str) #:use-module (crates-io))

(define-public crate-colored-str-0.1.5 (c (n "colored-str") (v "0.1.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0x9h0ga0203l7568878jjml7cs0gg1nly6lk21n69fkndsrns5hy")))

(define-public crate-colored-str-0.1.6 (c (n "colored-str") (v "0.1.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1m1wd4fglffk6rvcr5zy12y0hfbwvkxvx5bmylaxwmsxz9i432aw")))

(define-public crate-colored-str-0.1.7 (c (n "colored-str") (v "0.1.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "14nmywzp6pzj9rqk3p1nkab9m6wsfsi1v3f64igx7p2mwnj3bbz6")))

(define-public crate-colored-str-0.1.8 (c (n "colored-str") (v "0.1.8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "14m8lyzrxv4ppmhhgwrzxzix89i25h052cpfribxy8md9gc3vqb7")))

