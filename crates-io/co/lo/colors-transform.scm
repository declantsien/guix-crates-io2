(define-module (crates-io co lo colors-transform) #:use-module (crates-io))

(define-public crate-colors-transform-0.1.0 (c (n "colors-transform") (v "0.1.0") (h "0gv9qcgsijfz3m96y23w0mwl9crsgiyhc4b5qavh578c33qmx2c6")))

(define-public crate-colors-transform-0.1.1 (c (n "colors-transform") (v "0.1.1") (h "0sfwxs5k5yvgxp1bafqa84jzi57q79p1j6gkz385sqbczi91idgb")))

(define-public crate-colors-transform-0.2.0 (c (n "colors-transform") (v "0.2.0") (h "0ly96af8i5w59chzdwb0zq68xmrwgnjp3r7grv8y13byk3xcqpq7")))

(define-public crate-colors-transform-0.2.1 (c (n "colors-transform") (v "0.2.1") (h "1pq99hs4q7lwbx4c0898i1mfj622nd7vz0a46g7c0f0pb0x1kdl0")))

(define-public crate-colors-transform-0.2.3 (c (n "colors-transform") (v "0.2.3") (h "1yp8f58lmnj4lz6q3lbjm8mnhkrkn3hgd42b7f0c3708d0mcnqv5")))

(define-public crate-colors-transform-0.2.4 (c (n "colors-transform") (v "0.2.4") (h "1srikar0hvaw3wq1s9lcfkhiw4zk16jlhyj763a4gs9vbpwk0dzk")))

(define-public crate-colors-transform-0.2.5 (c (n "colors-transform") (v "0.2.5") (h "1f1a3x1iq3g1q5i2yh4l1nanmmpbfbz59jppkp52xrh17n3ml4rx")))

(define-public crate-colors-transform-0.2.7 (c (n "colors-transform") (v "0.2.7") (h "1iraz9wjs101z77rrl3a5zmj1v4ihr1cmmk4pfrzxph49hzhsm8h")))

(define-public crate-colors-transform-0.2.9 (c (n "colors-transform") (v "0.2.9") (h "0cg8cb7ysyk58gkjmw0bswzczjsn5jbv21rfx8x0walv88bbp56i")))

(define-public crate-colors-transform-0.2.10 (c (n "colors-transform") (v "0.2.10") (h "0qgp6gd1mnzxd15nmjp5gaz3c814hfdslmghlggb31v8pckc7lkh")))

(define-public crate-colors-transform-0.2.11 (c (n "colors-transform") (v "0.2.11") (h "0y3i2lf4rczl2g1d3i869hy8i015ac0v0c6p91priyzlbp0dn9lj")))

