(define-module (crates-io co lo colorize-macros) #:use-module (crates-io))

(define-public crate-colorize-macros-0.5.0 (c (n "colorize-macros") (v "0.5.0") (h "1ndyb0bqlpc483z5584xl0vb0gd52if2k6zaf4zyb7zbckpvr83p")))

(define-public crate-colorize-macros-0.5.1 (c (n "colorize-macros") (v "0.5.1") (h "1vij5l81fsnhla4r086am32jhv96djn1x1j8qfhkqx81j1wy9qcl")))

(define-public crate-colorize-macros-0.6.0 (c (n "colorize-macros") (v "0.6.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1xzp93di4wip4cc83nfsy691ssrcgbklpx8xp3n8rxl1d4csv9im") (y #t)))

(define-public crate-colorize-macros-0.6.1 (c (n "colorize-macros") (v "0.6.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "15yzfc2w5a7gqydwql19hhhpb5v8wma1y50ijdsjf6frx8zq4l1n") (y #t)))

(define-public crate-colorize-macros-0.6.2 (c (n "colorize-macros") (v "0.6.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1jlf97asw2kzwvy87g89s3wjcg0rha13a3jcq2kil3k86yazc5wl") (y #t)))

(define-public crate-colorize-macros-0.6.3 (c (n "colorize-macros") (v "0.6.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0nz4zwsrx23hd3z4dz04cmi8dqlsmq9mx3d2ajqx6ln80yw708k2")))

(define-public crate-colorize-macros-0.6.4 (c (n "colorize-macros") (v "0.6.4") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0w1sl6hgfn7c18cjblxb5d4ir1y5d36h1afvygkyag9rv3p5x1ma")))

(define-public crate-colorize-macros-0.7.0 (c (n "colorize-macros") (v "0.7.0") (d (list (d (n "colorize-proc-macro") (r "^0.1.0") (o #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "02nmxgmga5n4am490awy6mjs2pswb5fl7p8c4jpcr41ap1r9l3a8") (f (quote (("default")))) (y #t) (s 2) (e (quote (("proc" "dep:colorize-proc-macro"))))))

(define-public crate-colorize-macros-0.7.1 (c (n "colorize-macros") (v "0.7.1") (d (list (d (n "colorize-proc-macro") (r "^0.1.0") (o #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "143qslbi8s76j3qrifpcrqgb6w1h3qb6vg1j1vzd45z90qk6qph3") (f (quote (("default")))) (s 2) (e (quote (("proc" "dep:colorize-proc-macro"))))))

(define-public crate-colorize-macros-0.8.0 (c (n "colorize-macros") (v "0.8.0") (d (list (d (n "colorize-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0v50sq9mzppqaw7csnvq7v6rvspizp5l5l4y2804r3f0jwnrjxgp")))

