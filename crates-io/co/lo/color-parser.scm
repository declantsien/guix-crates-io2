(define-module (crates-io co lo color-parser) #:use-module (crates-io))

(define-public crate-color-parser-0.1.0 (c (n "color-parser") (v "0.1.0") (d (list (d (n "color-core") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "1nab0kgknhg4j1m168197zd7zbizc7n7p6r0xs0h8wp1gky23srq") (f (quote (("strict" "color-core/strict") ("default"))))))

(define-public crate-color-parser-0.1.1 (c (n "color-parser") (v "0.1.1") (d (list (d (n "color-core") (r "^0.1.0") (d #t) (k 0)) (d (n "pex") (r "^0.0.6") (d #t) (k 0)))) (h "0sxs2agssf3l9s211lm6z3wwfkk3fcqkigmflvpgibw9wby0h0ri") (f (quote (("default"))))))

