(define-module (crates-io co lo colorutils-rs) #:use-module (crates-io))

(define-public crate-colorutils-rs-0.1.0 (c (n "colorutils-rs") (v "0.1.0") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "1k929b81n0lm8vz85clcq83c7jp134lvdiknjgy5hw4m8l96pcja")))

(define-public crate-colorutils-rs-0.1.1 (c (n "colorutils-rs") (v "0.1.1") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "110hl0ihq5l6ccs8p4x49jx8i33s5vs891ilzxshxfwgxdil6sqk")))

(define-public crate-colorutils-rs-0.1.2 (c (n "colorutils-rs") (v "0.1.2") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "1x3c5hn57xvcwiy4i2w672mik55gk62rpbpxna9ykdp6z25c3d39")))

(define-public crate-colorutils-rs-0.1.3 (c (n "colorutils-rs") (v "0.1.3") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "0acrsynhp7khznbqwzan3f6h77r9yj0wp5az2x4p0pdymbnwq1rb")))

(define-public crate-colorutils-rs-0.1.4 (c (n "colorutils-rs") (v "0.1.4") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "0ddzwjb28jm8g46dhyrncgmb07fgwvcjiry3lq42flz2zc158kkq")))

(define-public crate-colorutils-rs-0.2.0 (c (n "colorutils-rs") (v "0.2.0") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "0d7a6nwyij3gaydflvfyxvcqrb6j68m1aivjrm9m25arqkb7wyvk") (f (quote (("default") ("avx2"))))))

(define-public crate-colorutils-rs-0.2.1 (c (n "colorutils-rs") (v "0.2.1") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "1vny00bs4aswysn41jlrxddl4xk4av6la6q3m30vzx2rlbn8hq8k") (f (quote (("default") ("avx2"))))))

(define-public crate-colorutils-rs-0.2.2 (c (n "colorutils-rs") (v "0.2.2") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "0699j0gi7a6h2cgls4j1znsd7jxggah6y6zkzq97hkywyn6bphpg") (f (quote (("default") ("avx2"))))))

(define-public crate-colorutils-rs-0.2.3 (c (n "colorutils-rs") (v "0.2.3") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "0wj3awhpjb7liyb5q8qr74xzxcw922g95gnzsmpn1kmg88c01ywb") (f (quote (("default") ("avx2"))))))

(define-public crate-colorutils-rs-0.2.4 (c (n "colorutils-rs") (v "0.2.4") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "1dz66qk8cn1f2s7whc4bsbg1m5zhab39mif0pjjh4qwmzcsxsc4z") (f (quote (("default") ("avx2"))))))

(define-public crate-colorutils-rs-0.2.5 (c (n "colorutils-rs") (v "0.2.5") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)))) (h "1qabankq4bjydm107bk7bxr0ibsrk5ywjafz5q5lqd8azwiawc5h") (f (quote (("default") ("avx2"))))))

