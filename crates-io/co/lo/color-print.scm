(define-module (crates-io co lo color-print) #:use-module (crates-io))

(define-public crate-color-print-0.1.0 (c (n "color-print") (v "0.1.0") (d (list (d (n "color-print-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "terminfo_crate") (r "^0.7.3") (o #t) (d #t) (k 0) (p "terminfo")))) (h "046rfp93m1iipn7q91ysh6ah4dinpssqnsyvj7hvwnbx0rnnn4qg") (f (quote (("terminfo" "color-print-proc-macro/terminfo" "lazy_static" "terminfo_crate"))))))

(define-public crate-color-print-0.2.0 (c (n "color-print") (v "0.2.0") (d (list (d (n "color-print-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "terminfo_crate") (r "^0.7.3") (o #t) (d #t) (k 0) (p "terminfo")))) (h "06mywgyz8n8i3vks6cj0yd7j8llrb77918ba839bjl0q8xzv51vq") (f (quote (("terminfo" "color-print-proc-macro/terminfo" "lazy_static" "terminfo_crate"))))))

(define-public crate-color-print-0.3.0 (c (n "color-print") (v "0.3.0") (d (list (d (n "color-print-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "terminfo_crate") (r "^0.7.3") (o #t) (d #t) (k 0) (p "terminfo")))) (h "05v89ba5rn3cjq7hf05vgmwm6j1ym7vxhw45sim18zciq4q3b4fm") (f (quote (("terminfo" "color-print-proc-macro/terminfo" "lazy_static" "terminfo_crate")))) (y #t)))

(define-public crate-color-print-0.3.1 (c (n "color-print") (v "0.3.1") (d (list (d (n "color-print-proc-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "terminfo_crate") (r "^0.7.3") (o #t) (d #t) (k 0) (p "terminfo")))) (h "088wqx8y8fv7fsmhbbwpxn3y5dy2kdpmbpp9pd47s1lc7jw7x6a3") (f (quote (("terminfo" "color-print-proc-macro/terminfo" "lazy_static" "terminfo_crate"))))))

(define-public crate-color-print-0.3.2 (c (n "color-print") (v "0.3.2") (d (list (d (n "color-print-proc-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "terminfo_crate") (r "^0.7.3") (o #t) (d #t) (k 0) (p "terminfo")))) (h "10s4bnqdr7csfn6hcddrjl1f6n2vak4jmvbiasjvab0j4rxx4gh0") (f (quote (("terminfo" "color-print-proc-macro/terminfo" "lazy_static" "terminfo_crate")))) (y #t)))

(define-public crate-color-print-0.3.3 (c (n "color-print") (v "0.3.3") (d (list (d (n "color-print-proc-macro") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "terminfo_crate") (r "^0.7.3") (o #t) (d #t) (k 0) (p "terminfo")))) (h "0b34aagvmlifywz4s5f5wg48v4z1y8zp5k3pgjyj5pl7r6mb7b7c") (f (quote (("terminfo" "color-print-proc-macro/terminfo" "lazy_static" "terminfo_crate")))) (y #t)))

(define-public crate-color-print-0.3.4 (c (n "color-print") (v "0.3.4") (d (list (d (n "color-print-proc-macro") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "terminfo_crate") (r "^0.7.3") (o #t) (d #t) (k 0) (p "terminfo")))) (h "1l26nrk0w21kzh7hs15wfqs7lm80xbngwl46jra8ar6q9r8fd9gj") (f (quote (("terminfo" "color-print-proc-macro/terminfo" "lazy_static" "terminfo_crate"))))))

(define-public crate-color-print-0.3.5 (c (n "color-print") (v "0.3.5") (d (list (d (n "color-print-proc-macro") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "terminfo_crate") (r "^0.7.3") (o #t) (d #t) (k 0) (p "terminfo")))) (h "0p9xlsmpnahxsji2pr73ms14alxwlkr0998fnfqvkfhlzxr871bs") (f (quote (("terminfo" "color-print-proc-macro/terminfo" "lazy_static" "terminfo_crate"))))))

(define-public crate-color-print-0.3.6 (c (n "color-print") (v "0.3.6") (d (list (d (n "color-print-proc-macro") (r "^0.3.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "terminfo_crate") (r "^0.7.3") (o #t) (d #t) (k 0) (p "terminfo")))) (h "1h58sxz6nmx7p5q6cp529gnjgpclaks72mkshws8k27k1z347r8y") (f (quote (("terminfo" "color-print-proc-macro/terminfo" "lazy_static" "terminfo_crate"))))))

