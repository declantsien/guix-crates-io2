(define-module (crates-io co lo color-char) #:use-module (crates-io))

(define-public crate-color-char-0.0.0 (c (n "color-char") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "10pfrybf6bi7imsmgibhdlh39pcq0spsja40965biyhhfi5p96cj") (f (quote (("default" "serde"))))))

(define-public crate-color-char-0.1.0 (c (n "color-char") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "1wa0sjaqnri6n7f35nqhgnz5w0f86390icfhaj0m40scdqpblid1") (f (quote (("default" "serde"))))))

(define-public crate-color-char-0.2.0 (c (n "color-char") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)))) (h "1xg24s7kk8krwiw51n8pl7b4v4r24rwlsmiiwghrwab5gvnc8pml") (f (quote (("default" "serde"))))))

