(define-module (crates-io co lo colored-macro) #:use-module (crates-io))

(define-public crate-colored-macro-0.1.0 (c (n "colored-macro") (v "0.1.0") (d (list (d (n "css-color") (r "^0.2.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1r788fk5xlpjg1s5fnmwmcl81d20nmm9ixh1844vln58qs5pd4cl")))

(define-public crate-colored-macro-0.2.0 (c (n "colored-macro") (v "0.2.0") (d (list (d (n "colored-macro-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1vwsvfx5ix7qf2ayh2fiy0ip9mp6d3qgh96f7p9zgln1jzf31jb7") (f (quote (("no-color" "colored-macro-macro/no-color"))))))

