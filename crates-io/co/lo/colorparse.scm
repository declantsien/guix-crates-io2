(define-module (crates-io co lo colorparse) #:use-module (crates-io))

(define-public crate-colorparse-1.0.0 (c (n "colorparse") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.7.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "02j8yxp3w062d4byihv7q3z2xizp0zsddqgb7hr8g7wv04yfrc00")))

(define-public crate-colorparse-1.1.0 (c (n "colorparse") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.8.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "1hk26cx1wl8g10g2h3p2sdb150x81hvx6wkn812bqfmq184srizr")))

(define-public crate-colorparse-2.0.0 (c (n "colorparse") (v "2.0.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "1sdcc2aiimpfcs26ml9jc0krnfh2xg9cdy0799gis1zayl4x25qz")))

(define-public crate-colorparse-2.0.1 (c (n "colorparse") (v "2.0.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)))) (h "0ybkydapdihylrfad2fxdpz32iskb6ysi2qx0halqpyhc8s9m7qp")))

(define-public crate-colorparse-3.0.0 (c (n "colorparse") (v "3.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "0cfp0nkq58dv0a4i18amgq0bx07gsadaxf2b1abr10r588kv9dkd")))

