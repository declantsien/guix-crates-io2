(define-module (crates-io co lo color-your-life) #:use-module (crates-io))

(define-public crate-color-your-life-0.1.0 (c (n "color-your-life") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1nd51g59zy7zrawp6knqqkaaadk012wkrhw7f7y399x9ap81w795") (r "1.62.1")))

(define-public crate-color-your-life-0.1.1 (c (n "color-your-life") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0chq9w0gi6vrx0fncmz0jvn8xwynr1ks8a5ljsg2wxh4gk1x5j07") (r "1.62.1")))

(define-public crate-color-your-life-0.2.0 (c (n "color-your-life") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1ds2k7r317mk5d16g8k96wcl0w6rps29dmwslp1f8q6l11bmm269") (r "1.62.1")))

(define-public crate-color-your-life-0.3.0 (c (n "color-your-life") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1nc75nh3i3hdx1arh1a20lwciqglivqx8llq95ks48g4iidcpvql") (r "1.62.1")))

(define-public crate-color-your-life-0.4.0 (c (n "color-your-life") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0v5b9zfhvwjmrm4766rhsb3s27wavz86cb3wc91idpvaznsw05ck") (r "1.62.1")))

(define-public crate-color-your-life-0.5.0 (c (n "color-your-life") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0biwka95cnk339bm3wwxmrwckpylz70laq5w5zxn0l5y54s863ad") (r "1.62.1")))

(define-public crate-color-your-life-0.6.0 (c (n "color-your-life") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1jnwpchbikhn9mlr16jsbyc97y8pc8nzyz746gvwncvx096laflg") (r "1.62.1")))

(define-public crate-color-your-life-0.7.0 (c (n "color-your-life") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0k6bd8xzzwacaqi10988j4sqp486sxnx4kppjnw2flbq9lbn6ga4") (r "1.62.1")))

(define-public crate-color-your-life-0.7.1 (c (n "color-your-life") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0w3xganmp6wx46q6ckj4l05bps94ywmp2ip8kjhvzpyb8wpg4mj1") (r "1.62.1")))

(define-public crate-color-your-life-0.8.0 (c (n "color-your-life") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0zxkbz600yy7y6ibas3b63sx3iarhbqkp0ivfwxnz55l5q99sh36") (r "1.62.1")))

