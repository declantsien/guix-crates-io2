(define-module (crates-io co lo colorstyle) #:use-module (crates-io))

(define-public crate-colorstyle-1.0.1 (c (n "colorstyle") (v "1.0.1") (h "09dv5k1prn77zqg86aq44ssz6dggcg2grsm755i4vwd500r383nh")))

(define-public crate-colorstyle-1.0.2 (c (n "colorstyle") (v "1.0.2") (h "01pzlq67aa71lk1w4kjgrfiwrdr4vim6fwwh92qxrirvridszcli")))

