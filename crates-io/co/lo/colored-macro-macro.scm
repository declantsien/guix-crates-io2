(define-module (crates-io co lo colored-macro-macro) #:use-module (crates-io))

(define-public crate-colored-macro-macro-0.2.0 (c (n "colored-macro-macro") (v "0.2.0") (d (list (d (n "colored-macro-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10d2srlyf71ri3bycs7l5jxa5z4pl0fgannz4x43wr9sa67crrfn") (f (quote (("no-color" "colored-macro-impl/no-color"))))))

