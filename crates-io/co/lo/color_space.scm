(define-module (crates-io co lo color_space) #:use-module (crates-io))

(define-public crate-color_space-0.1.0 (c (n "color_space") (v "0.1.0") (h "1gz9qknclahddws4ixl21fh9h07kgfwdy8r9n6lmvqkg8a012kz5")))

(define-public crate-color_space-0.2.0 (c (n "color_space") (v "0.2.0") (h "18j2kzj02cxlsyd1b45arbz80701rp620q44dj5vv8khnh9bm6wk")))

(define-public crate-color_space-0.3.0 (c (n "color_space") (v "0.3.0") (h "0gl3fzc3nk9pdc8qdfk0yzi8ams06ad1b3m1z353bjb0x4kvz0ip")))

(define-public crate-color_space-0.3.1 (c (n "color_space") (v "0.3.1") (h "19jpdd8gzwjxjl398zbkp1dsfabx7sdgl47ikh684416yr5nlxj4")))

(define-public crate-color_space-0.3.2 (c (n "color_space") (v "0.3.2") (h "1msq5gfk14ln7yd433h920k0qhq0195kpkzrqm056h5ki5vwz24p")))

(define-public crate-color_space-0.4.0 (c (n "color_space") (v "0.4.0") (h "0rl9jfsgmg3xjzx3mgmqh6nddkcb987pfplsis1qz228gyjki2c5")))

(define-public crate-color_space-0.4.1 (c (n "color_space") (v "0.4.1") (h "0abhb0g8hdziasr1l41x40nx2gzs7jldgyq68lwcfyckzqgddnd6")))

(define-public crate-color_space-0.4.2 (c (n "color_space") (v "0.4.2") (h "0kpdfzmi9zn58rqkxcmxx528kvfblvr177qklszd3jwdh1a1qw24")))

(define-public crate-color_space-0.4.3 (c (n "color_space") (v "0.4.3") (h "0k3wdc3cfjgpyrpqm0k9awj81wz82pfn3dn1754zxih060yz7inh")))

(define-public crate-color_space-0.5.0 (c (n "color_space") (v "0.5.0") (h "0vs9ai2y74q75nxvd8l8821qni3fh9lrpr37090sfff6rwjsrd9c")))

(define-public crate-color_space-0.5.2 (c (n "color_space") (v "0.5.2") (h "0rx2vn8239aai32svqplcw96ni1r7p3ysix3rkchxjwcly9dbcz0")))

(define-public crate-color_space-0.5.3 (c (n "color_space") (v "0.5.3") (h "108k50fnbaix9y4q5y5qkr5k8vkhvmr9bgmr3d8dn579qjyb4xip")))

