(define-module (crates-io co lo colourado-iter) #:use-module (crates-io))

(define-public crate-colourado-iter-1.0.0 (c (n "colourado-iter") (v "1.0.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "piston_window") (r "^0.131.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jb6bc293wmaysbgach6awxdzxipzlvzjp3s2mx7pahqqxv1dafd")))

(define-public crate-colourado-iter-1.0.1 (c (n "colourado-iter") (v "1.0.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "piston_window") (r "^0.131.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "185kbw2y1fabh6pz317ql0kzp5x0xm0xjcylc3i227cq97bvfs7p")))

(define-public crate-colourado-iter-1.1.0 (c (n "colourado-iter") (v "1.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "piston_window") (r "^0.131.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0471viv7s90sms8r6x22vsvbb3189c856qvq7w28b1adhpjfwda6")))

(define-public crate-colourado-iter-1.1.1 (c (n "colourado-iter") (v "1.1.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "piston_window") (r "^0.131.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gpahlc7ggc9lcprwp06y2g5bs68f9q8253rny2rhqqklb4i0hgk")))

(define-public crate-colourado-iter-1.2.0 (c (n "colourado-iter") (v "1.2.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "piston_window") (r "^0.131.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rrj8s5lm5gin6970n2vba3cfcfkbi4vbwrkbcw7j7bxa5bqnp7r")))

