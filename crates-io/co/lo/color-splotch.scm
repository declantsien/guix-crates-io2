(define-module (crates-io co lo color-splotch) #:use-module (crates-io))

(define-public crate-color-splotch-0.1.0 (c (n "color-splotch") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "gif") (r "^0.11.4") (d #t) (k 0)) (d (n "glam") (r "^0.21.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "macroquad") (r "^0.3.0") (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jv2q49h4365bdck6y7cmvx32sp4qqkvbfmhmamg6fg4xr5fgx3c")))

