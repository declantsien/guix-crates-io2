(define-module (crates-io co lo colorskill) #:use-module (crates-io))

(define-public crate-colorskill-1.0.0 (c (n "colorskill") (v "1.0.0") (d (list (d (n "colorsys") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1iy8zc8021qxks89km3crvg22m3prxvdc5dq46dvz7cxgh9v14b2")))

(define-public crate-colorskill-1.0.1 (c (n "colorskill") (v "1.0.1") (d (list (d (n "colorsys") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1fkaxn6rp10hk14fvq4r8q208q7dmvfdd8aw6zinlkvp1ckjnpz9")))

(define-public crate-colorskill-1.0.2 (c (n "colorskill") (v "1.0.2") (d (list (d (n "colorsys") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0y3mq5gniphym2ylay502r476gbnfaac7gq31k17qyyfrj6dns6r")))

(define-public crate-colorskill-1.0.3 (c (n "colorskill") (v "1.0.3") (d (list (d (n "colorsys") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1s2x59qwvd0il4hirccl1v9b9qjnswhx5xzlk99nbwwm0yzjq8cm")))

(define-public crate-colorskill-1.0.4 (c (n "colorskill") (v "1.0.4") (d (list (d (n "colorsys") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "195ak2kr0cqsaihg1np9sa5qn6cz7n77haplylrrgmy4i0aggkfn")))

(define-public crate-colorskill-1.1.0 (c (n "colorskill") (v "1.1.0") (d (list (d (n "colorsys") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1gpvf9s2m0sq2x1br4x8mnv7r8azzsai3bzki4qxh608xjj0hn8a")))

(define-public crate-colorskill-1.1.1 (c (n "colorskill") (v "1.1.1") (d (list (d (n "colorsys") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1yw9zn4nh3c2xkrqrl09fmr6xk3g4dv5im4953v0kf2v8as4lrfl")))

(define-public crate-colorskill-1.1.2 (c (n "colorskill") (v "1.1.2") (d (list (d (n "colorsys") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1p0768yvngxz2z3n8pcc0fk4qvx3kkvhkbbijk8f2xwzkp46cb4r")))

(define-public crate-colorskill-1.1.3 (c (n "colorskill") (v "1.1.3") (d (list (d (n "colorsys") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1aaamz42v4ncdb73b59jdbznapk1l43997fz2s58w7cl1qmwlrp9")))

