(define-module (crates-io co lo color_picker) #:use-module (crates-io))

(define-public crate-color_picker-1.0.0 (c (n "color_picker") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "1x8ld0wq8bv1h5n346rl5124f0spkkgzgjd6z3ir9h27jay3xpcm")))

(define-public crate-color_picker-1.0.1 (c (n "color_picker") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "1ic2275dgm6xy2x72g9wmb02kx8r8hmldwhyr28l4qc5kkr63h37")))

(define-public crate-color_picker-1.0.2 (c (n "color_picker") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "168pha5vgm7r9v4vgm2j6siyxrj1d4zc36d813fs3cqhkw10823g")))

(define-public crate-color_picker-1.1.2 (c (n "color_picker") (v "1.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "1p3hqkm16rd5gfmyx4h29k9kl05ddjqvgdi89sli2rki0d28krw8")))

(define-public crate-color_picker-1.2.0 (c (n "color_picker") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "15ipwgcd2x329khs1g8q1y7y59v02s1mjracvax80yc1bm5mb0v0")))

