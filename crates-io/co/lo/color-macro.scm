(define-module (crates-io co lo color-macro) #:use-module (crates-io))

(define-public crate-color-macro-0.1.0 (c (n "color-macro") (v "0.1.0") (d (list (d (n "color-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1az0rp45z3p1kaihlxx88p7gfcxwqpjxdfrdxsa83mrr79wjm0i3") (f (quote (("strict" "color-parser/strict") ("default"))))))

(define-public crate-color-macro-0.1.1 (c (n "color-macro") (v "0.1.1") (d (list (d (n "color-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d6vlvkjlhmzk1wlc64zfj2lhhfmbq1r6xqk3xi9sqajpg04mgf0") (f (quote (("strict" "color-parser/strict") ("default"))))))

