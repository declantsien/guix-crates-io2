(define-module (crates-io co lo colol) #:use-module (crates-io))

(define-public crate-colol-0.1.0 (c (n "colol") (v "0.1.0") (h "0zpd34mnknq84ks04hqh5df9k7930xzixmvqi6kagnq1v8n40yk4")))

(define-public crate-colol-0.2.0 (c (n "colol") (v "0.2.0") (h "1wkadvhs6didr7c6a15lwzgha6284vryfjlif6fdr409qnn132zz")))

(define-public crate-colol-0.2.1 (c (n "colol") (v "0.2.1") (h "0srcvf0a63j7zap4slrv55alfgpb510mrqgpgj0gx8ms1rmc5z3h")))

(define-public crate-colol-0.2.2 (c (n "colol") (v "0.2.2") (h "0v8p7v8gjn0z9l4mfk3g4fh34skalymyw4iqnmv4860a2rdp6s12")))

(define-public crate-colol-0.3.0 (c (n "colol") (v "0.3.0") (h "01273402pzpv35d1pzrjx78ddjxb3i4fsgx63mzgwc0cdp2m3bz1") (f (quote (("default" "colol") ("colol"))))))

(define-public crate-colol-0.3.1 (c (n "colol") (v "0.3.1") (h "1vxmjwy4mifzvi2x3f2hx0ysb1vx8rwc5a7r7xpdiq2j67l22wx7") (f (quote (("default" "colol") ("colol"))))))

(define-public crate-colol-0.3.2 (c (n "colol") (v "0.3.2") (d (list (d (n "winapi") (r "^0.3.4") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1x3mlbn5cqs9hyy83yj4k9lc5lyiv3j9xr94bxcnn6h1gw96f1rn") (f (quote (("default" "colol") ("colol"))))))

(define-public crate-colol-0.4.0 (c (n "colol") (v "0.4.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0brznjcjp13hyw4z1qm6nr2py8vfmk0q0b240031i002h5i882hw") (f (quote (("default" "colol") ("colol"))))))

