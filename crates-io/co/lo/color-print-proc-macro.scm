(define-module (crates-io co lo color-print-proc-macro) #:use-module (crates-io))

(define-public crate-color-print-proc-macro-0.1.0 (c (n "color-print-proc-macro") (v "0.1.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vwqa7yzv3h2q2xdj0mfdsas6irpbcy250gwq2wdmi3wvak6j33z") (f (quote (("terminfo"))))))

(define-public crate-color-print-proc-macro-0.2.0 (c (n "color-print-proc-macro") (v "0.2.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19jknbrd1ncfavh8qsq9w7zysdb13c2miw2ziya44gbkz301x8z8") (f (quote (("terminfo"))))))

(define-public crate-color-print-proc-macro-0.3.0 (c (n "color-print-proc-macro") (v "0.3.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1shcaryn2nvn1mhm47k5g1p5k8kkknwyhfdzh4s2imfprsv3l72h") (f (quote (("terminfo"))))))

(define-public crate-color-print-proc-macro-0.3.2 (c (n "color-print-proc-macro") (v "0.3.2") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01lzzb56575isa2zsq8pjhkafj97bvnrg17rh1jmb2qk5x8x2ysh") (f (quote (("terminfo")))) (y #t)))

(define-public crate-color-print-proc-macro-0.3.4 (c (n "color-print-proc-macro") (v "0.3.4") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jsijb8b1hiyrmz6hpgcn8544w7ijp00prsfycgjsgfp6yjyl6ym") (f (quote (("terminfo"))))))

(define-public crate-color-print-proc-macro-0.3.5 (c (n "color-print-proc-macro") (v "0.3.5") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gq00lg9irnzag890m85w3j1p8rpmsns2fa48051swka8mk7iqsp") (f (quote (("terminfo"))))))

(define-public crate-color-print-proc-macro-0.3.6 (c (n "color-print-proc-macro") (v "0.3.6") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "14yvvhlfsl1zskxl28f4jvwdimkdd8dxgzbgq2kirjzkqn01mzvp") (f (quote (("terminfo"))))))

