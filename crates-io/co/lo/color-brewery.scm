(define-module (crates-io co lo color-brewery) #:use-module (crates-io))

(define-public crate-color-brewery-0.1.0 (c (n "color-brewery") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.35") (d #t) (k 0)))) (h "1vryhqb8g6p4w7flgv96yvcx09liklmwhsmwrk90hxlx7b99928h")))

