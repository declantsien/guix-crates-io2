(define-module (crates-io co lo coloursum) #:use-module (crates-io))

(define-public crate-coloursum-0.1.0 (c (n "coloursum") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0bbzvrkm1fndni871qps6s6ar7rp4345my0fx2h1skww2dwl6xyn")))

(define-public crate-coloursum-0.2.0 (c (n "coloursum") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "ecoji") (r "^1.0.0") (d #t) (k 0)) (d (n "indoc") (r "^0.3.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0nw0p5vl45q5sr0hwbdvpbldj6wzr2ij5rrbp9nqjwxsyg6z29xk")))

(define-public crate-coloursum-0.3.0 (c (n "coloursum") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ecoji") (r "^1.0.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.13.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "which") (r "^3.1.1") (t "cfg(unix)") (k 0)))) (h "1519m94sy627fvdjw1gsdml5kqvgmh0sh5mpcqz9mcc69skr72vb")))

