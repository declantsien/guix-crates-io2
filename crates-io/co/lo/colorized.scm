(define-module (crates-io co lo colorized) #:use-module (crates-io))

(define-public crate-colorized-0.8.5 (c (n "colorized") (v "0.8.5") (h "06w9qxm3yz3bh6xad1fbm2prfyqnvh6i3bl2sb0c8hd46kg4gkhw")))

(define-public crate-colorized-0.8.6 (c (n "colorized") (v "0.8.6") (h "1iydhvdh2b4p4ixz3irrr15midq8kl6npkqx11fpyyh8chs6668l")))

(define-public crate-colorized-0.8.7 (c (n "colorized") (v "0.8.7") (h "14aqdxg7slr369iqyfygbyysa5jy5135cscq51qazki27v3dg3fg")))

(define-public crate-colorized-0.9.0 (c (n "colorized") (v "0.9.0") (h "17lfqcss47cky5lav55d2k4gy3hvzcg9i8bla80hlm4mybyka9rl")))

(define-public crate-colorized-1.0.0 (c (n "colorized") (v "1.0.0") (h "0r35bpwb6ly1imn4cj52n9d6vcf4fy5myss75x9inljk4jaq34kk")))

