(define-module (crates-io co lo colorid) #:use-module (crates-io))

(define-public crate-colorid-0.0.1-dev.0 (c (n "colorid") (v "0.0.1-dev.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1kajbh7lns4ck50qkabxr61hvi9ranig2ahgqgrjwwmbll0ng2w7")))

(define-public crate-colorid-0.0.1-dev.1 (c (n "colorid") (v "0.0.1-dev.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13hpxybkwkfwhsn4b3b9f5i1pdxm8c0jfblwry6vp638fq4b4prd")))

(define-public crate-colorid-0.0.1-dev.2 (c (n "colorid") (v "0.0.1-dev.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "rng"))) (d #t) (k 2)))) (h "1klh8l6wy83awnp1gzjbidi4a132g79n41j8s4ss3qpzbli44pr1")))

(define-public crate-colorid-0.0.1 (c (n "colorid") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "rng"))) (d #t) (k 2)))) (h "1kp0rrfz4x2ahvmdrmda9p5bnlhf8jga4v64x3mv8a21bv6drdqw")))

(define-public crate-colorid-0.0.2 (c (n "colorid") (v "0.0.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "rng"))) (d #t) (k 2)))) (h "0ab251mc2lak9wmfmvdzc1d13rxsdxg6qbs01raxcap3j7qrfl62")))

(define-public crate-colorid-0.0.4 (c (n "colorid") (v "0.0.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "rng"))) (d #t) (k 2)))) (h "152la2ilxipn19hyl946yka9iy0vk4i0h0gis93cf495awlwnbj0")))

(define-public crate-colorid-0.0.6 (c (n "colorid") (v "0.0.6") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "rng"))) (d #t) (k 2)))) (h "0h5k8wg5mjx38ihd2aw7ky6qbbp7q6789n43bjlklf15gdzkmlnb")))

