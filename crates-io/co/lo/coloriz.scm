(define-module (crates-io co lo coloriz) #:use-module (crates-io))

(define-public crate-coloriz-0.1.0 (c (n "coloriz") (v "0.1.0") (h "1ylzn00jzwviv1dr7hzxkr7ayknvaym32qwg97d3849ghc13wfga")))

(define-public crate-coloriz-0.1.1 (c (n "coloriz") (v "0.1.1") (h "1w4wsz16mhc8zw4qrys6y2pc0g9jlfd1xx9qvjg875hnkkfb2x6m")))

(define-public crate-coloriz-0.1.2 (c (n "coloriz") (v "0.1.2") (d (list (d (n "overload") (r "^0.1.1") (d #t) (k 0)))) (h "0k4b8sh4zvj01nsiqyc6wzn1rymkyq728pbx9gpg8rqcphnaizpp")))

(define-public crate-coloriz-0.1.3 (c (n "coloriz") (v "0.1.3") (d (list (d (n "overload") (r "^0.1.1") (d #t) (k 0)))) (h "0as68222292vwy92rgazn4i9gb22hfimff49zijqd8gi04xjf538") (y #t)))

(define-public crate-coloriz-0.1.4 (c (n "coloriz") (v "0.1.4") (d (list (d (n "overload") (r "^0.1.1") (d #t) (k 0)))) (h "1x9nnhszihzmnaiyc9rrdas7034vxh70jiwqxbfdbzjbr27f6inx")))

(define-public crate-coloriz-0.1.5 (c (n "coloriz") (v "0.1.5") (d (list (d (n "overload") (r "^0.1.1") (d #t) (k 0)))) (h "08f7l3xvlm691qqj8l60a8y7r1wd2539fqifmf9gs1cknbin9lzk")))

(define-public crate-coloriz-0.1.6 (c (n "coloriz") (v "0.1.6") (d (list (d (n "overload") (r "^0.1.1") (d #t) (k 0)))) (h "023pak3yfqy4xjqd6s2lpcam6vylnlnsq8af7rm63wlkw7dgv9hm")))

(define-public crate-coloriz-0.1.7 (c (n "coloriz") (v "0.1.7") (d (list (d (n "overload") (r "^0.1.1") (d #t) (k 0)))) (h "0dwqy80m4k5x2gl1v3yrrh45v7gd7plfmzhvs0vck5f6kfv7ji29")))

(define-public crate-coloriz-0.1.8 (c (n "coloriz") (v "0.1.8") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "overload") (r "^0.1.1") (d #t) (k 0)))) (h "02n2yhi2q0949mb3zr7diylab1nks0ppfilnadg6hjpvhza5yyxh")))

(define-public crate-coloriz-0.1.9 (c (n "coloriz") (v "0.1.9") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "overload") (r "^0.1.1") (d #t) (k 0)))) (h "1aas19xa0z07iaw7cn7spqsnilrl00s1y1zmp29gx6jyklv7j7gz")))

(define-public crate-coloriz-0.2.0 (c (n "coloriz") (v "0.2.0") (d (list (d (n "overload") (r "^0.1.1") (d #t) (k 0)))) (h "0ymlm1h8m0c766lqh0ig4hws286fs5vglm28d8rhnr4bvfvyc1dw")))

