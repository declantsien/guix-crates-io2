(define-module (crates-io co lo colorer) #:use-module (crates-io))

(define-public crate-colorer-0.1.0 (c (n "colorer") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "onig") (r "^6.2.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)))) (h "0g9zn4y0nlsg0x24dbd2hwh3ysrh29lb23fjphfb91x9zyrinpn2") (y #t)))

(define-public crate-colorer-0.1.1 (c (n "colorer") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "onig") (r "^6.2.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)))) (h "1r50bsaq3fp4lw0hzjkhzd3hlh5v1vnpmywqmh4rah5srgqnma0n") (y #t)))

(define-public crate-colorer-0.2.0 (c (n "colorer") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "onig") (r "^6.2.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)))) (h "10922rp9ihwgmr7bvs7463hpqf02qamicpvppw1iy741ylhq4bny") (y #t)))

(define-public crate-colorer-0.3.0 (c (n "colorer") (v "0.3.0") (d (list (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "onig") (r "^6.2.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)))) (h "0vhap7mj2whr8l101zqg87z0liqb33wwl26vq0if6gafpq2x2kbj") (y #t)))

(define-public crate-colorer-0.3.1 (c (n "colorer") (v "0.3.1") (d (list (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "onig") (r "^6.2.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)))) (h "0vc0vd9ncskgnnjbwy0my8yhkyxjlj06nwh9g6p533j6wc8x1lh5") (y #t)))

(define-public crate-colorer-0.3.2 (c (n "colorer") (v "0.3.2") (d (list (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "onig") (r "^6.2.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)))) (h "1hfhwnpw454sdf26spj44sh10jc61jp65w8a3vgsmmyh6d175swb") (y #t)))

(define-public crate-colorer-1.0.0 (c (n "colorer") (v "1.0.0") (d (list (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "onig") (r "^6.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1adc8xww9bli74j57sy42m0q8l5f2hq34jaf5avbf05pfakkrf0l")))

(define-public crate-colorer-1.0.1 (c (n "colorer") (v "1.0.1") (d (list (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "onig") (r "^6.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1yl4cvi11vnr7fxjkqixsxqim3gzxpzl32llyss5ps1khn32ajn5")))

(define-public crate-colorer-1.1.0 (c (n "colorer") (v "1.1.0") (d (list (d (n "ctrlc") (r "^3.1.9") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "onig") (r "^6.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.133") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "08q7va540mnjd1isq3f2gsj9lbgp179k3djfaqj9j4agb45ynfi6")))

