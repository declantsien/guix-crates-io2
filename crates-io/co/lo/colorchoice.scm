(define-module (crates-io co lo colorchoice) #:use-module (crates-io))

(define-public crate-colorchoice-1.0.0 (c (n "colorchoice") (v "1.0.0") (h "1ix7w85kwvyybwi2jdkl3yva2r2bvdcc3ka2grjfzfgrapqimgxc") (r "1.64.0")))

(define-public crate-colorchoice-1.0.1 (c (n "colorchoice") (v "1.0.1") (h "08h4jsrd2j5k6lp1b9v5p1f1g7cmyzm4djsvb3ydywdb4hmqashb") (r "1.65.0")))

