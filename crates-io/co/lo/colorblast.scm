(define-module (crates-io co lo colorblast) #:use-module (crates-io))

(define-public crate-colorblast-0.0.1 (c (n "colorblast") (v "0.0.1") (d (list (d (n "any-lexer") (r "^0.0.1") (d #t) (k 0)))) (h "0hy0yw9cr690wl3fw21lkwn4gdlljrfnlnmd6ha609aycxl7zj0b")))

(define-public crate-colorblast-0.0.2 (c (n "colorblast") (v "0.0.2") (d (list (d (n "any-lexer") (r "^0.0.3") (d #t) (k 0)))) (h "1zpnjv718g131bmv4qrbrmrfyhw3q5q7l9ypi2qpf50zd6xf1q7p")))

(define-public crate-colorblast-0.0.3 (c (n "colorblast") (v "0.0.3") (d (list (d (n "any-lexer") (r "^0.0.3") (d #t) (k 0)))) (h "1pmj7f9p7zbp58ppldlkb3vs8mwcs4w4zdl2qwzc6y7iqnccpm0c")))

