(define-module (crates-io co lo colored_markup) #:use-module (crates-io))

(define-public crate-colored_markup-0.1.0 (c (n "colored_markup") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "0rjgv35myy1fpgqrvgl310rg2b21clxb2dsg2bjx1aqxhg44wp1l") (y #t)))

(define-public crate-colored_markup-0.1.1 (c (n "colored_markup") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "0mx29c6m0ygjknvsqdcskbhs2500djbg9rsphlmzyylx6ah60zwm")))

