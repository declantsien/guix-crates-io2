(define-module (crates-io co lo color_counter) #:use-module (crates-io))

(define-public crate-color_counter-0.1.0 (c (n "color_counter") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "1yg7hnnyhnbkhq5d5mc54bzqzn78z0gggfx3irp6fv1hdq03ixdq")))

(define-public crate-color_counter-0.1.1 (c (n "color_counter") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "17wg3p90sbcnc2ca5nw926726d9hmpif25hwa4890d652vxgsx9c")))

(define-public crate-color_counter-0.1.2 (c (n "color_counter") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "1kmbcff24l1yxwcwhrr4qfp7h55jc0bj6j0pxsczppclfwsksjnl")))

(define-public crate-color_counter-0.1.3 (c (n "color_counter") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "1z8c4aph2ymcck4nk69jwaglmc6vldl2wm3iwx9kb6jx985zz905")))

(define-public crate-color_counter-0.1.4 (c (n "color_counter") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "16xg5hlbp5cb12jzmvxklxgvyx75syni64zcqsvdrg5pvd87wasg")))

(define-public crate-color_counter-0.1.5 (c (n "color_counter") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "0b8f9mlbijnbppxmn9yrli29cvfgnccx47v0qfsz9n3ngw1agnk3")))

(define-public crate-color_counter-0.1.6 (c (n "color_counter") (v "0.1.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "0pikq4qlrv1w3lf6f35g0aqnfsaqhcva3xah73f4y4vgybl5mv5w")))

(define-public crate-color_counter-0.1.7 (c (n "color_counter") (v "0.1.7") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)))) (h "1165z8wjg3hpnwlrl67f71h53izh3b34micd75y9xjf2x0csp75f")))

(define-public crate-color_counter-0.1.8 (c (n "color_counter") (v "0.1.8") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (f (quote ("gif_codec" "jpeg" "ico" "png_codec" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt"))) (k 0)))) (h "0v209mwfndwjpqmps28zq5fg1m8r6l0mi2c2ii4h6jc1q98xp7ng") (f (quote (("jpeg_rayon" "image/jpeg_rayon"))))))

(define-public crate-color_counter-0.1.9 (c (n "color_counter") (v "0.1.9") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (f (quote ("gif_codec" "jpeg" "ico" "png_codec" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt"))) (k 0)))) (h "079g4svjbfsjgy0151g2l7rjzgyxd55aczkwsak2mnk5vx5vbasd") (f (quote (("jpeg_rayon" "image/jpeg_rayon"))))))

(define-public crate-color_counter-0.2.0 (c (n "color_counter") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (f (quote ("gif_codec" "jpeg" "ico" "png_codec" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt"))) (k 0)))) (h "0p8i9bikj0sjw0jmc9hypyrryr27qhji1wx32jxf6l5wvvplwnzn") (f (quote (("jpeg_rayon" "image/jpeg_rayon"))))))

(define-public crate-color_counter-0.2.1 (c (n "color_counter") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (f (quote ("gif_codec" "jpeg" "ico" "png_codec" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt"))) (k 0)))) (h "0l30hi4bg3vbhkrgwyc1llrh5lpb53nnrm72mh7yzravi258sb6c") (f (quote (("jpeg_rayon" "image/jpeg_rayon"))))))

(define-public crate-color_counter-0.2.2 (c (n "color_counter") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.3.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (f (quote ("gif_codec" "jpeg" "ico" "png_codec" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt"))) (k 0)))) (h "172l7m8sx3crzdkky3352q79h692iw6h9hgj2niw8h8xqgspy85a") (f (quote (("jpeg_rayon" "image/jpeg_rayon"))))))

(define-public crate-color_counter-0.2.3 (c (n "color_counter") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "color_processing") (r "^0.4.1") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (f (quote ("gif_codec" "jpeg" "ico" "png_codec" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt"))) (k 0)))) (h "0dm5nzh9zwqi5h50x01a6g75kanlyp7xldvxsyz19vqp7bw33jqj") (f (quote (("jpeg_rayon" "image/jpeg_rayon"))))))

