(define-module (crates-io co lo color-art) #:use-module (crates-io))

(define-public crate-color-art-0.1.0 (c (n "color-art") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0iv2nn2iqgh4k8ay41aqcllq74cdhmmilral4ky48zhzgkgnvagl")))

(define-public crate-color-art-0.1.1 (c (n "color-art") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0dfin2l2f32557is48rlz7hqk1ar1m69pfdc1aqp8nq3la8nvmfa")))

(define-public crate-color-art-0.1.2 (c (n "color-art") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0hzrarhhbrhyqyxw3fpdhgfg58w00h2lx4gsgrc8ks5qlmcarny8")))

(define-public crate-color-art-0.1.3 (c (n "color-art") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "02zajlpzryp0al3wpdhq6a7158g794cc2kvjvzsk2yix2i65dp2p")))

(define-public crate-color-art-0.1.4 (c (n "color-art") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "000g1372bhg3q9wqq00cyffh3gzzxwlpwciwc82vgn9ajbx7l5qz")))

(define-public crate-color-art-0.1.5 (c (n "color-art") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "09m9c40b7yysk15rvac3yjzchwkrfrx3xkg4j3y2rdc134ma6xk8")))

(define-public crate-color-art-0.1.6 (c (n "color-art") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0nnvcwb0pbm6c5fv8drs69p6k58im4wmn6r3lwyikdjdhhq4irka")))

(define-public crate-color-art-0.1.8 (c (n "color-art") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0mywq10i93hz4g9y91d3akzsmkxhz4r9g1qdyhj7r769pgrsznc9")))

(define-public crate-color-art-0.1.9 (c (n "color-art") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1br67xc949r59x7rfmqi0y9rjjkknygic3wk3bjpp77pf5y1j7fj")))

(define-public crate-color-art-0.2.0 (c (n "color-art") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "17qigpn9rsflxjgmm4v9cfp49my9iyhpcima4xm4bvcw3rcx9r5h")))

(define-public crate-color-art-0.2.2 (c (n "color-art") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1lb52qdfv2gxz5f7lkf2bj95l90d7c797a53w0cmmb86xpdbq65w")))

(define-public crate-color-art-0.2.3 (c (n "color-art") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0kfm5xkg8mxczj58zwz2rjjl0rklfqzl7yv6sahwc8r3khxzgfjp")))

(define-public crate-color-art-0.2.4 (c (n "color-art") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1319kxgykxs7z9aag77gnddzi9k4cdckynxa8v2fnb092kbwal3f")))

(define-public crate-color-art-0.2.5 (c (n "color-art") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1bmgdny1k7habi150l8hgj7k0p3vbwcpm4ymyc7786b1jwn9lga3")))

(define-public crate-color-art-0.2.6 (c (n "color-art") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1cjfi332sdbqg2hcz4h55bfmwnwc643kg79415b6waa92rrh0r0a")))

(define-public crate-color-art-0.2.7 (c (n "color-art") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "03271bdwf5b9pbfk39mmanpm85vl1bw9d7lv5d9bkxpjq1ry5aac")))

(define-public crate-color-art-0.2.8 (c (n "color-art") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "17sj3li7b5xqlymfvylz3l48mfnv5mypl9js4dsqlwhhx0mbgxbw")))

(define-public crate-color-art-0.2.9 (c (n "color-art") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "038bbjg7fff40hyzc4r6q31dv6sw66jc0l806wj8yhcbqkczi80h")))

(define-public crate-color-art-0.2.10 (c (n "color-art") (v "0.2.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "076c9l9qjnzj8qcp3whn4pnb336h35bbrc0958qg1b4fkz6y81l5")))

(define-public crate-color-art-0.2.11 (c (n "color-art") (v "0.2.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1axiz8gz2b1mgg5af97qr23gcff81hig7bzm9s29ldm6ys20srl2")))

(define-public crate-color-art-0.2.12 (c (n "color-art") (v "0.2.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1j821wghv6pds4ms7sxr5nzmzmnwn480a8zykg8c8k1m4a5c24z6")))

(define-public crate-color-art-0.2.13 (c (n "color-art") (v "0.2.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0q3p7vb713gis5qihxal5mq3frsd3h2zz6v51772zabhhsc3x0qh")))

(define-public crate-color-art-0.3.0 (c (n "color-art") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0jj11flrz4pglggk4n2iasrgm89mdkjm52csv0z376vcrjkf89i3")))

(define-public crate-color-art-0.3.1 (c (n "color-art") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "01nkrpfgn1rpk1hpfxrk1vmgvmmxmnjdj8rv2as4rxsffxwhl2l5")))

(define-public crate-color-art-0.3.2 (c (n "color-art") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "07ysvl2d8cbj5c06g6m43bd02rphmczyqaxr9zxg6iavray35z76")))

(define-public crate-color-art-0.3.3 (c (n "color-art") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1cxn20dgqa8z6ra21qfv8rr4fsixcbwnhind1c5v2h9cd3yvpmq8")))

(define-public crate-color-art-0.3.4 (c (n "color-art") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "149jbsp6d3fwwyk8a5w13d32bh5nx05nmbnp84h0zcgwhvd3qlbj")))

(define-public crate-color-art-0.3.5 (c (n "color-art") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1z025fmkmfbdjfkd6va1mk8ba4bp2xpsj46xd9rangwhynrzxiw0")))

(define-public crate-color-art-0.3.6 (c (n "color-art") (v "0.3.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0f9d99138f494ra1cpdphmi56ndrp2inj2mbg3wwkflnayj6rq90")))

(define-public crate-color-art-0.3.7 (c (n "color-art") (v "0.3.7") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1x55wyizp0pi9a8g9x6ci5il6gzrdwkfm8cdswzw3p7lqdl7m9sx")))

(define-public crate-color-art-0.3.8 (c (n "color-art") (v "0.3.8") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "07yri9cpdmszin8mf6z04sirc56zanzrl2yhm1byrprnjhkncx09") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

