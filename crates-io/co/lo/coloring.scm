(define-module (crates-io co lo coloring) #:use-module (crates-io))

(define-public crate-coloring-0.1.0 (c (n "coloring") (v "0.1.0") (h "1h68aa4kd7r1llmiir7kl2p9mn3p9yr6f44h2rjp941i4mdlh61m")))

(define-public crate-coloring-0.2.0 (c (n "coloring") (v "0.2.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "1h4k4rv450ijs02n0i9wvk9zij8ygpj7547dwxiz8pbcyw9n2b3f")))

(define-public crate-coloring-0.3.0 (c (n "coloring") (v "0.3.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "1h770gzndhxj993a0sywflm9y1pqg77a79jyy7w1mbpkkkmpxxsy")))

(define-public crate-coloring-0.3.1 (c (n "coloring") (v "0.3.1") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "0kgc2ipzyfgj8d542s6ryx3p6zmadbhzzs6flz428sx8kmy5z5v6")))

