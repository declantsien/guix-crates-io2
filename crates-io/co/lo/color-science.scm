(define-module (crates-io co lo color-science) #:use-module (crates-io))

(define-public crate-color-science-0.0.0 (c (n "color-science") (v "0.0.0") (h "0ird8c1npdbxkisybq476rvikg68qmdjcrlsbv7y3mqlxin431rd") (y #t)))

(define-public crate-color-science-0.0.1 (c (n "color-science") (v "0.0.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1ncr54dd127331gdx0d4b41cayh8jzsplrdrcyjdzj5lvxjnbhjr") (y #t)))

