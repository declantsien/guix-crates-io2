(define-module (crates-io co lo colorama) #:use-module (crates-io))

(define-public crate-colorama-0.1.0 (c (n "colorama") (v "0.1.0") (h "1pk2if88c9sbyhnk0hp5993wzxx2h23nrcvfizlm7ixv8x324h11")))

(define-public crate-colorama-1.0.0 (c (n "colorama") (v "1.0.0") (h "0v2k82qhcm4b4a0gminq9kaja8d8rnrl7fjcdc2hzz5n4r962rfi")))

