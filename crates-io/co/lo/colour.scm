(define-module (crates-io co lo colour) #:use-module (crates-io))

(define-public crate-colour-0.1.0 (c (n "colour") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1ri0w19wcan12k4cviy8ggck83p25g6j5xwby2r0qf2dyv6k40yg")))

(define-public crate-colour-0.2.0 (c (n "colour") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0dkk90hcywjwcg10dhr23qr1sw6llp4d11fnwbws5iq2ajs65217")))

(define-public crate-colour-0.2.1 (c (n "colour") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1bminsy9agci2795by03yc52rydkyj1dwz7sdya2j08shw4cv6mb")))

(define-public crate-colour-0.3.0 (c (n "colour") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "1mqwnia32bvi0k893h7qbhnwgh2riyjnknp77ss7czmkvpc2i8w3")))

(define-public crate-colour-0.3.1 (c (n "colour") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "1kirji703k807frwn3v3ks1ysvjnm97vnqvyr09faxzqgvz607x3")))

(define-public crate-colour-0.4.0 (c (n "colour") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)))) (h "06fphkhdj45z400gny0wfx8g9djx4bpkvmspybcxbdgzgrspimr3")))

(define-public crate-colour-0.5.0 (c (n "colour") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)))) (h "1p0kb7i6j58f592k8dhlqa4ni13462p6s4n6dzbm8xxiaz5ds88m")))

(define-public crate-colour-0.6.0 (c (n "colour") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "06i0jga3c1fbrh3wbcn3jgi2gvy3q1ixjxw4pcj0qlbgy8r4azm2")))

(define-public crate-colour-0.7.0 (c (n "colour") (v "0.7.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)))) (h "1absz2dqf101x89r5h7pznw5hm00pbppwh40lzqvgkc3v00san0g")))

(define-public crate-colour-1.0.0 (c (n "colour") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0r4pwvmd6n7zh77j6b60v22pk2p81vqcx25hv5rbrqamvn8d8b53")))

(define-public crate-colour-1.1.0 (c (n "colour") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "09xxq2v9wd8ina3ix9yr3p54zlk7q0mjn240n0s9ffadfm5hpfk9")))

(define-public crate-colour-2.0.0 (c (n "colour") (v "2.0.0") (h "0a5r82y4wlwi8388qqwys7ryz9rhi675w2h8xcqjjhql3zp3jbk9") (r "1.70.0")))

