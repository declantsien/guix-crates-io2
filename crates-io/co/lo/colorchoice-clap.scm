(define-module (crates-io co lo colorchoice-clap) #:use-module (crates-io))

(define-public crate-colorchoice-clap-1.0.0 (c (n "colorchoice-clap") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("std" "derive"))) (k 0)) (d (n "colorchoice") (r "^1.0.0") (k 0)))) (h "0jdw7nvc21dx09i0xnhdifg681bs8vmrpd09d51jxxd3lfiqhbj1") (r "1.64.0")))

(define-public crate-colorchoice-clap-1.0.1 (c (n "colorchoice-clap") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("std" "derive" "color"))) (k 0)) (d (n "colorchoice") (r "^1.0.0") (k 0)))) (h "05dlb91caxxndlf66vc7k6wad35b0gwf8hhdayygq93m7a91s7x8") (r "1.64.0")))

(define-public crate-colorchoice-clap-1.0.2 (c (n "colorchoice-clap") (v "1.0.2") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("std" "derive" "color"))) (k 0)) (d (n "clap") (r "^4.3.5") (d #t) (k 2)) (d (n "colorchoice") (r "^1.0.0") (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 2)))) (h "0ldj4m7zik08i3a9qs2a9mlx5rb2q943klgkyvmlpn0yd9vr3yva") (r "1.64.0")))

(define-public crate-colorchoice-clap-1.0.3 (c (n "colorchoice-clap") (v "1.0.3") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("std" "derive" "color"))) (k 0)) (d (n "clap") (r "^4.4.2") (d #t) (k 2)) (d (n "colorchoice") (r "^1.0.0") (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 2)))) (h "1gnhn2lbrnc55dji2hfhd9s8zhjpg1f8m0ahxf505bsc0rfsvsbg") (r "1.70.0")))

(define-public crate-colorchoice-clap-1.0.4 (c (n "colorchoice-clap") (v "1.0.4") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("std" "derive" "color"))) (k 0)) (d (n "clap") (r "^4.3.5") (d #t) (k 2)) (d (n "colorchoice") (r "^1.0.0") (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 2)))) (h "0kh960r22aqcrahfrgv16hb6z1xy8pmbnfd9sm3nnvvrkb4k9sfr") (r "1.65.0")))

