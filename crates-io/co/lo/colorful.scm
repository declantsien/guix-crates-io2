(define-module (crates-io co lo colorful) #:use-module (crates-io))

(define-public crate-colorful-0.1.0 (c (n "colorful") (v "0.1.0") (h "104blhd4aazp2zfpr49q05wdnlqjp6a477cjbyddqqqhixll9b04")))

(define-public crate-colorful-0.2.0 (c (n "colorful") (v "0.2.0") (h "0s87lma469v1dqpsski67b7ran82m4b6fs94mc22v9rjg5lzbs36")))

(define-public crate-colorful-0.2.1 (c (n "colorful") (v "0.2.1") (h "0rdfk0dhld8h6ijy2gshivkj7wcr87pjb3lanmb7mpapzwcidjhb")))

(define-public crate-colorful-0.2.2 (c (n "colorful") (v "0.2.2") (h "1kj7vb4glvwcp75ambq2npf3dv1vjq4zkz12j8ypyzasaii0bbwp")))

