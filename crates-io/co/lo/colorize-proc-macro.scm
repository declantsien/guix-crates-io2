(define-module (crates-io co lo colorize-proc-macro) #:use-module (crates-io))

(define-public crate-colorize-proc-macro-0.1.0 (c (n "colorize-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.30") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "1mxnjdqba7y2zv984zbmh4y72pc7qwvk9sbfsdqhg2xjbz281kdw")))

(define-public crate-colorize-proc-macro-0.2.0 (c (n "colorize-proc-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.30") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("parsing" "extra-traits" "full"))) (d #t) (k 0)))) (h "1f7swg4kgdbiwj7rbfxnp7cs50j5v9446i26sr7kq9kd4r7qdbk7")))

