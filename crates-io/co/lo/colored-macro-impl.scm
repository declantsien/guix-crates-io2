(define-module (crates-io co lo colored-macro-impl) #:use-module (crates-io))

(define-public crate-colored-macro-impl-0.2.0 (c (n "colored-macro-impl") (v "0.2.0") (d (list (d (n "css-color") (r "^0.2") (d #t) (k 0)) (d (n "gag") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "154g1pgb4pgk6k3k77ri5q935rcn03fq8lcgxprpq9vq1mxmlwnc") (f (quote (("no-color"))))))

