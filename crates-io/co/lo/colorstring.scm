(define-module (crates-io co lo colorstring) #:use-module (crates-io))

(define-public crate-colorstring-0.0.1 (c (n "colorstring") (v "0.0.1") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "17da7khg6z0zl8sz6m1cnqy4db4cy2nc7cnbwh2pk9w5k0d9ngsi")))

