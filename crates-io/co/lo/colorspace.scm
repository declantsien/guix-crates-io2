(define-module (crates-io co lo colorspace) #:use-module (crates-io))

(define-public crate-colorspace-0.1.0 (c (n "colorspace") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "derive_more") (r "^0.15.0") (d #t) (k 0)) (d (n "float-cmp") (r "^0.5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "numeric_literals") (r "^0.1.0") (d #t) (k 0)) (d (n "png") (r "^0.16.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "simdeez") (r "^0.6.4") (d #t) (k 0)))) (h "15h2vz6hcpc558amrqdx9j9z7aj8wp5xs04asa30m6w4kbn4i7sv")))

