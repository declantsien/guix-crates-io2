(define-module (crates-io co lo colony-rs) #:use-module (crates-io))

(define-public crate-colony-rs-0.1.0 (c (n "colony-rs") (v "0.1.0") (d (list (d (n "ethers") (r "^1") (d #t) (k 0)) (d (n "ethers") (r "^1") (d #t) (k 1)) (d (n "eyre") (r "^0.6") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls" "trust-dns" "trust-dns-resolver"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1pw1sx2h1qmd6l8pj80callv2ff49qa2pvs9xajyqgwbypqykzhw")))

