(define-module (crates-io co lo colorsys) #:use-module (crates-io))

(define-public crate-colorsys-0.1.0 (c (n "colorsys") (v "0.1.0") (h "0xryk7sy27bq7kwrlm5h99yj9cjxwjb66n555r9c87q2fxbicwn9")))

(define-public crate-colorsys-0.1.1 (c (n "colorsys") (v "0.1.1") (h "17fn5h062sd90gjliqq1nkvknnbrm3w115ag93xznhb7y8sb1f71")))

(define-public crate-colorsys-0.1.2 (c (n "colorsys") (v "0.1.2") (h "1y1g5314mcsb5nn454jz1fwym1b529zrks0f6fv7vy5l3v5klwpl")))

(define-public crate-colorsys-0.5.0 (c (n "colorsys") (v "0.5.0") (h "085j7vqssxak6l4xij4wvjv4d05923kcsibyjm893zpjkgf2qjp2")))

(define-public crate-colorsys-0.5.1 (c (n "colorsys") (v "0.5.1") (h "0dc3maxd8hv8h3m3rk0y9aw51lcszi8kj0v22ndg9w1yyyvqj03a")))

(define-public crate-colorsys-0.5.2 (c (n "colorsys") (v "0.5.2") (h "13c3kqjzd5vp4znwjf3y73pj26c9i8ah7lgsyym4dkd6bxq3v56b")))

(define-public crate-colorsys-0.5.3 (c (n "colorsys") (v "0.5.3") (h "0b1xzlhcs0zbmnhk4pizivndaka60gqk7hlpf7wgqw1mqpm5gnd3")))

(define-public crate-colorsys-0.5.4 (c (n "colorsys") (v "0.5.4") (h "1qkd2p31nd6y233ml8fqjhz1y539zf7qqdq2f94kzyskkgf992ks")))

(define-public crate-colorsys-0.5.5 (c (n "colorsys") (v "0.5.5") (h "1ivr82p6263s72biwy6ipfg3xj5f9jmim753yfqmjs28g2vk8mnb")))

(define-public crate-colorsys-0.5.6 (c (n "colorsys") (v "0.5.6") (h "11jjxfqdyqai2y83bhifi29r10paymlx5lzhjs5yaafgz3011dwd")))

(define-public crate-colorsys-0.5.7 (c (n "colorsys") (v "0.5.7") (h "1knbw2j0gjka6n96kbd0jf5f93wfp8275jyipykj45dd3y4mqixy")))

(define-public crate-colorsys-0.6.0 (c (n "colorsys") (v "0.6.0") (h "1dmpxyiajbm124rhcim977b4w0nmdij9ap204vv3rf55z3qn6njj")))

(define-public crate-colorsys-0.6.1 (c (n "colorsys") (v "0.6.1") (h "04v3fk4s4l03jr1lfk9z0ylsdw2y3fzg9210q1cxqkh4y2jlw3dp") (f (quote (("std") ("default" "std"))))))

(define-public crate-colorsys-0.6.2 (c (n "colorsys") (v "0.6.2") (h "06nilgcnmsad0gddd1h095nxllcd0g78k2jq9qmim5jrpsg4gl8x") (f (quote (("std") ("default" "std"))))))

(define-public crate-colorsys-0.6.3 (c (n "colorsys") (v "0.6.3") (h "0x9yp2qi3a8c6b1jbp3h0j83s9n4zsmkdbn2gxjcm1fqcjvya0ph") (f (quote (("std") ("default" "std"))))))

(define-public crate-colorsys-0.6.4 (c (n "colorsys") (v "0.6.4") (h "12b68wpf6a48z974nc3qc7qb1a3bz4b0g1cc3152gkc26waw8rpn") (f (quote (("std") ("default" "std"))))))

(define-public crate-colorsys-0.6.5 (c (n "colorsys") (v "0.6.5") (h "0w5vvcq3497m184a399x2rh9fgmarwzdkjc87zzmasslklbzkzad") (f (quote (("std") ("default" "std"))))))

(define-public crate-colorsys-0.6.6 (c (n "colorsys") (v "0.6.6") (h "1s490y29szjccgq7nq0cnaq0sqbncvq55p3975z67gc27i2svhjj") (f (quote (("std") ("default" "std"))))))

(define-public crate-colorsys-0.6.7 (c (n "colorsys") (v "0.6.7") (h "1g8vwcv89n2dzi9bmbzqlj9cl9a89jz49668grbcncv4cjx1l9jl") (f (quote (("std") ("default" "std"))))))

