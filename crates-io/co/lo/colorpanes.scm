(define-module (crates-io co lo colorpanes) #:use-module (crates-io))

(define-public crate-colorpanes-3.0.0 (c (n "colorpanes") (v "3.0.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recolored") (r "^1.9.3") (d #t) (k 0)))) (h "1icwl3a82a3fi3q8gi0vv9b78qrvq9krabysamxzyga62h6h1wxw")))

(define-public crate-colorpanes-3.0.1 (c (n "colorpanes") (v "3.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recolored") (r "^1.9.3") (d #t) (k 0)))) (h "0xdgdaj0m7c1p0k2nn9hyaa801f2wszbrn7ahn7wp6zd1gdvnzdc")))

