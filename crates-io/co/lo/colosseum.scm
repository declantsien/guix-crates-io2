(define-module (crates-io co lo colosseum) #:use-module (crates-io))

(define-public crate-colosseum-0.1.0 (c (n "colosseum") (v "0.1.0") (h "1r40spxx2hq1rb1dwxmiz7qwcv1a2dg184ih2qf0r37q3n5mvs7l")))

(define-public crate-colosseum-0.1.1 (c (n "colosseum") (v "0.1.1") (h "0rqw4d02gbs9p415wmxpxca9m0acd2wcqnxw87rdvbfpj5ixvw47")))

(define-public crate-colosseum-0.2.0 (c (n "colosseum") (v "0.2.0") (h "0xnlklh06fz2rfva0i84d5h0lywvjgv794wa19i3vpfahghdgk27") (y #t)))

(define-public crate-colosseum-0.2.1 (c (n "colosseum") (v "0.2.1") (h "1pgyahyxmpibmha63ky31p5n8x88gf5hg89brak4wm3bmm2h8wxj")))

(define-public crate-colosseum-0.2.2 (c (n "colosseum") (v "0.2.2") (h "1y86cj7x0ma41raycgk5r46z876yv7hqlbll4zp25w7dkas8631p")))

