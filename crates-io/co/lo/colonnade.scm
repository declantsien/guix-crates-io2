(define-module (crates-io co lo colonnade) #:use-module (crates-io))

(define-public crate-colonnade-0.1.0 (c (n "colonnade") (v "0.1.0") (h "0mcddz4ya8yz2p8lkmk2qrrw1a5b1pnn0fvc3gns05jq8lpa76nj")))

(define-public crate-colonnade-0.1.1 (c (n "colonnade") (v "0.1.1") (h "0mlmgxa0vhdnbd9gxvi3zrwvhkkw6knnjrys141am968jlbbgq2x")))

(define-public crate-colonnade-0.2.0 (c (n "colonnade") (v "0.2.0") (d (list (d (n "term") (r "^0") (d #t) (k 2)))) (h "162c8idmp21xvjpvpr573035r0k2h2g2hdabvqlpyzm0mxbv68jr")))

(define-public crate-colonnade-1.0.0 (c (n "colonnade") (v "1.0.0") (d (list (d (n "term") (r "^0") (d #t) (k 2)))) (h "1hfaqczy1vg3am0by5sddf6s300cja8vxm7rjdfjqhjypnqr8ivb")))

(define-public crate-colonnade-1.1.0 (c (n "colonnade") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 2)) (d (n "term") (r "^0") (d #t) (k 2)))) (h "0w1vsyzlgslw2xyyv2cv8pyrjam4lv6swiqa5ahnqlq81sygyfwm")))

(define-public crate-colonnade-1.1.1 (c (n "colonnade") (v "1.1.1") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 2)) (d (n "term") (r "^0") (d #t) (k 2)))) (h "1ckfx5d48dnrdw8823xgn81kqimr9yy3cn7y03n2jrpb7g3nnlz2")))

(define-public crate-colonnade-1.1.2 (c (n "colonnade") (v "1.1.2") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 2)) (d (n "term") (r "^0") (d #t) (k 2)))) (h "1dbn9bmsv0bss57960f64bv6lwjfgx2yfw20bcwd29vxj4ibhflv")))

(define-public crate-colonnade-1.1.3 (c (n "colonnade") (v "1.1.3") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 2)) (d (n "term") (r "^0") (d #t) (k 2)))) (h "1d4df39gfk5l8ifg50vi5alrzp2gpxvvk7pwbd8mikmng02181pr")))

(define-public crate-colonnade-1.2.0 (c (n "colonnade") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0nvasdf3h33q42x0asfv1gclvknaddi9042ni8i1h1x9g4wcr522")))

(define-public crate-colonnade-1.3.0 (c (n "colonnade") (v "1.3.0") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1k8nx96dximzcq0akbwzml2ykg9lm6hc5jmf4bsjy7vlspp3zxxb") (f (quote (("nbsp" "regex" "lazy_static"))))))

(define-public crate-colonnade-1.3.1 (c (n "colonnade") (v "1.3.1") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "11sbdhljjv1afyy7355hmacsrcmnlxsgkf7q70g7n1y7xz7gzk9s") (f (quote (("nbsp" "regex" "lazy_static"))))))

(define-public crate-colonnade-1.3.2 (c (n "colonnade") (v "1.3.2") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0lcqf5p4wx0mq95ay4jc3pjpnlq8dq45anh02i8ib43cjjrapny4") (f (quote (("nbsp" "regex" "lazy_static"))))))

(define-public crate-colonnade-1.3.3 (c (n "colonnade") (v "1.3.3") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0xg4wc3ss7ln0avqadrb2yqibpp9gcw3rc40i90z7h0hyshk0cl3") (f (quote (("nbsp" "regex" "lazy_static"))))))

