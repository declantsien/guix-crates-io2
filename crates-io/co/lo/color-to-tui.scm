(define-module (crates-io co lo color-to-tui) #:use-module (crates-io))

(define-public crate-color-to-tui-0.1.2 (c (n "color-to-tui") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tui") (r "^0.16.0") (k 0)))) (h "098a96l6i5gq90ixf3fmb79kkilnchypkywwyrpvgxgq3fic5slf") (f (quote (("optional") ("default" "optional"))))))

(define-public crate-color-to-tui-0.1.3 (c (n "color-to-tui") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tui") (r "^0.16.0") (k 0)))) (h "17072by58vlw43sgpz411am2p1qqpgp8vh4w6ll8g7nmrjbf36n2") (f (quote (("optional") ("default" "optional"))))))

(define-public crate-color-to-tui-0.2.0 (c (n "color-to-tui") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tui") (r "0.*") (k 0)))) (h "1k3vyp2fl0lcqs8iwssv56562kag6ljqaixirrci77ydmcq3zi0s") (f (quote (("optional") ("default" "optional"))))))

(define-public crate-color-to-tui-0.3.0 (c (n "color-to-tui") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tui") (r "0.*") (k 0) (p "ratatui")) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "14pj4bfw6fclnznfhsi7bw96jbzwvw12afshzxlqds745wygxdzv") (f (quote (("optional") ("default" "optional"))))))

