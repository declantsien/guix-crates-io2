(define-module (crates-io co lo colorix) #:use-module (crates-io))

(define-public crate-colorix-0.1.0 (c (n "colorix") (v "0.1.0") (h "0kiiy0n4pbp4zyk0cps3p3g9diy0p9pprjyf1qn4pkmszk8qrhfg")))

(define-public crate-colorix-0.1.1 (c (n "colorix") (v "0.1.1") (h "1aazkl3vh135x5hj7p9v09lnr5yr5k2hxx9r7b6f6fn56jd257p6")))

