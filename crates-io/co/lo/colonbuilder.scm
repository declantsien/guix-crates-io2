(define-module (crates-io co lo colonbuilder) #:use-module (crates-io))

(define-public crate-colonbuilder-0.1.0 (c (n "colonbuilder") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bz64mv0h189g3v87ifblhf5775gbrwcssnhifpxav3f2q3s7p82")))

(define-public crate-colonbuilder-0.1.1 (c (n "colonbuilder") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qwfswmj31varg1qhf340191mrm9f3454kics3jy8l3498n9xl5j")))

(define-public crate-colonbuilder-0.1.2 (c (n "colonbuilder") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0iq9kdg272d37aqahwzhnv9wa0szhbwcfzp0cj74ysis0sxy3yxj")))

(define-public crate-colonbuilder-0.1.3 (c (n "colonbuilder") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15r5zz5sbhzz1hpzwi1j7hd61cnlrsifmnrmfr7xazrwgya4iw94")))

(define-public crate-colonbuilder-0.1.4 (c (n "colonbuilder") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0z2dxgixs7r97vvwa0c9k8268n0b5yr1lz83ddb4ffqp4jx0qklm")))

(define-public crate-colonbuilder-0.1.5 (c (n "colonbuilder") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1v2aq4lzj8b7m0vrdarcd6aydywzcasnnwkblddyr7xzhdkprr5m")))

(define-public crate-colonbuilder-0.1.6 (c (n "colonbuilder") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1h62cy908fr45bkwq3r80a22ay8wv5r572wk1p720b023l1fy4ab")))

