(define-module (crates-io co lo color_processing) #:use-module (crates-io))

(define-public crate-color_processing-0.1.0 (c (n "color_processing") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09hhcvhssmzhg4jr3biqg2hkn4h0g02lljkznpzsbcmw3k2nq7kq")))

(define-public crate-color_processing-0.1.1 (c (n "color_processing") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "185kkma8sf2pja1k3db6a46xjwngyp64g9z7cjz1bc6lsvgb9g0w")))

(define-public crate-color_processing-0.1.2 (c (n "color_processing") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0d6v43pkz47r24ibiizq898ifs4dx0jssl40izpbd0zhqx6lran7")))

(define-public crate-color_processing-0.1.3 (c (n "color_processing") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0728gvl6f7szkdz98pfljvjslp793hpp3wyad1r5iqnwj5d0rw48")))

(define-public crate-color_processing-0.2.0 (c (n "color_processing") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02hrbj1mz0xlbizm71c2fibriw94hx213m11ywhk8lv1kjcm2qa2")))

(define-public crate-color_processing-0.2.1 (c (n "color_processing") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0c188w7pm2hn93jqs9wfprx5vz4izs9i5xvb3h3hq8pjm1fhp287")))

(define-public crate-color_processing-0.2.2 (c (n "color_processing") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dj7wwvjw090g2a78fq2lrzw9zdrkz885kkn5nkfn8v7a48c8nyw")))

(define-public crate-color_processing-0.3.0 (c (n "color_processing") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ffg134v4shpa5v8apwchmwxv32vyzlg6xjdx1vmq739k5kgmjw6")))

(define-public crate-color_processing-0.3.1 (c (n "color_processing") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xjxw12yf8rflismfimsc7hhyc8ki1fgkp54nfhwi28485s61nsc")))

(define-public crate-color_processing-0.4.0 (c (n "color_processing") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nxdh693fz80zw36gr4gvl6p5cl4hb2ldwwl29ch3a1if8prrvzq")))

(define-public crate-color_processing-0.4.1 (c (n "color_processing") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1s15n9m986pa7sgw9g4fvc1x5zr9xn7d704lggxam7qvqw3lxs6q")))

(define-public crate-color_processing-0.5.0 (c (n "color_processing") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yj97fvpzv87x9z6ihqzssfcf2njb6b358gv6izmvqzg8m63yx9a")))

(define-public crate-color_processing-0.6.0 (c (n "color_processing") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07k8k82yx399rbyaqzax7z6fp0w7sv575c79sjh91dd3avwmr2r1")))

(define-public crate-color_processing-0.6.1 (c (n "color_processing") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0krxgr7ical8s7cn7fgpb6yrkbyc7wm8mzqb9b14vxavnnpr4mi7")))

(define-public crate-color_processing-0.6.2 (c (n "color_processing") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1f1d3lr8c4rkjzsq8bnk982m5xaacqxlb78wyhy3zsm516vigca1")))

