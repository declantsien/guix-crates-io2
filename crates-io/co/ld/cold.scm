(define-module (crates-io co ld cold) #:use-module (crates-io))

(define-public crate-cold-0.0.0 (c (n "cold") (v "0.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "15mrkbjp655rkclprq9dkykqkdw1a0c7f4rgxja50ydy3mpzifdr") (f (quote (("std"))))))

(define-public crate-cold-0.0.1 (c (n "cold") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("os-poll" "os-ext"))) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 2)))) (h "1xj2rbn9fmk1qj8il9cpl1i1s3yhhp112r3bql30xpswgcpia2zk")))

(define-public crate-cold-0.0.2 (c (n "cold") (v "0.0.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("os-poll" "os-ext"))) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 2)))) (h "1993nwzlg7d7c19v8mbrs11f9yw2n4x997450gpzsqmip7bbhaxh")))

