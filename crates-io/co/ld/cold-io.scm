(define-module (crates-io co ld cold-io) #:use-module (crates-io))

(define-public crate-cold-io-0.1.0 (c (n "cold-io") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "mio") (r "^0.7.13") (f (quote ("os-poll" "tcp"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "09ix7skwkm5kald2ffyxnxiw975f36a504xigq3nswqml690gvr6")))

(define-public crate-cold-io-0.1.1 (c (n "cold-io") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "mio") (r "^0.7.13") (f (quote ("os-poll" "tcp"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "1hgbl64gmnjqs9dqw6mdc59vvlbd62mqmbmf3njjbhg93lcm6n74")))

(define-public crate-cold-io-0.1.2 (c (n "cold-io") (v "0.1.2") (d (list (d (n "ctrlc") (r "^3.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "mio") (r "^0.7.13") (f (quote ("os-poll" "tcp"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "1601jzlr2cp0rk1gsyh7x8j2k48656iwp7c6d9kgfn3c7mi15m7q")))

(define-public crate-cold-io-0.1.3 (c (n "cold-io") (v "0.1.3") (d (list (d (n "ctrlc") (r "^3.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "mio") (r "^0.7.13") (f (quote ("os-poll" "tcp"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "0j9i9zrlvwbarnh1v00dakykqrv4za87y7h63x0sgmksd7fpggdw")))

(define-public crate-cold-io-0.2.0 (c (n "cold-io") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mio") (r "^0.7.13") (f (quote ("os-poll" "tcp"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "1y7jmlgq5grk5kzq1dim5qihc3zcwm9iblk1cq4sqy84nkblx7vz")))

