(define-module (crates-io co la cola-crdt) #:use-module (crates-io))

(define-public crate-cola-crdt-0.1.0 (c (n "cola-crdt") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0b4frp097608nqfrpyb93vlm1gz6gcq0mknpr08fmkvwapswdvmm") (s 2) (e (quote (("serde" "encode" "dep:serde") ("encode" "dep:bincode" "dep:sha2" "dep:serde"))))))

(define-public crate-cola-crdt-0.1.1 (c (n "cola-crdt") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0xan7fwq22f5y62nhgxn2f3daydda0r7zv5jxqxm0pzf0877n2yn") (s 2) (e (quote (("serde" "encode" "dep:serde") ("encode" "dep:bincode" "dep:sha2" "dep:serde"))))))

