(define-module (crates-io co la colain) #:use-module (crates-io))

(define-public crate-colain-0.1.0 (c (n "colain") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "1ihygq0nwbkp3vdwf8bzbw3bf998jhv37s83yhbkkc26bpc9l0lg") (y #t)))

(define-public crate-colain-0.1.1 (c (n "colain") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "033hqfv7hjrsi8padw967bg709hcnh5pfyy430m66wvhkrjcqii5") (y #t)))

(define-public crate-colain-0.1.2 (c (n "colain") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "1rcrcn39p6wgiwv0zy812kcky0bwcjjq55bxca1mqv8hjqzs496k") (y #t)))

(define-public crate-colain-0.1.3 (c (n "colain") (v "0.1.3") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "1q64kfsmlkx8s0jacqyw2vjdl6v8m4k2nzwswxvpdmhd9a17v1y9") (y #t)))

(define-public crate-colain-0.1.4 (c (n "colain") (v "0.1.4") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "1m3ia5nxyzdbsk9smmfxwbplyhqg1pwq7555gqz8kig6q3wvsaml")))

