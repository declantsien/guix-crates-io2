(define-module (crates-io co _s co_sort) #:use-module (crates-io))

(define-public crate-co_sort-0.1.0 (c (n "co_sort") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "14y26v3pcmyrnk6gvbij5bkn9q6gikrbrjv6j7m459cf7z934dc9")))

(define-public crate-co_sort-0.1.1 (c (n "co_sort") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "08phjln7c990xsih9rzk091kidrpl8p0fg4jqadrswflzschv336")))

(define-public crate-co_sort-0.1.2 (c (n "co_sort") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1k51df6rh724ivisv0wc60lwhhryxinjw9gs1np5j9rf2wp23956")))

(define-public crate-co_sort-0.2.0 (c (n "co_sort") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1x6wmf4ayyp3pkn6dw15gki68wsvs8qpc3423cgbm57dbl8qxhaw")))

