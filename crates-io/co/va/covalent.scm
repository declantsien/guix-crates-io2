(define-module (crates-io co va covalent) #:use-module (crates-io))

(define-public crate-covalent-0.1.0 (c (n "covalent") (v "0.1.0") (h "1vwxqzcxvj94j96gk7sbqwmdspyhyyarq6i14gi2cjchkzqd6zq6")))

(define-public crate-covalent-0.1.1 (c (n "covalent") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)))) (h "0by0qjcm5vl6dn7sygpbqljc2wyacviabjqaapd64hhiap3h1px5")))

(define-public crate-covalent-0.1.2 (c (n "covalent") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "1yxxa5lpav0npwk867mps9xjjn8wxj2yg6qdjmz0zxgky3zxk737")))

