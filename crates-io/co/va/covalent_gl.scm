(define-module (crates-io co va covalent_gl) #:use-module (crates-io))

(define-public crate-covalent_gl-0.1.0 (c (n "covalent_gl") (v "0.1.0") (d (list (d (n "covalent") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.27") (d #t) (k 0)))) (h "00a71s0k09bclxv12zl51r6y8h1yvhk2kv4x67mcmhksak911jbr")))

(define-public crate-covalent_gl-0.1.1 (c (n "covalent_gl") (v "0.1.1") (d (list (d (n "covalent") (r "^0.1.1") (d #t) (k 0)) (d (n "glium") (r "^0.27") (d #t) (k 0)))) (h "0rdibhwji591jb9ari7hh871f9qinc30ahkgxks5rqh2z56fdr21")))

(define-public crate-covalent_gl-0.1.2 (c (n "covalent_gl") (v "0.1.2") (d (list (d (n "covalent") (r "^0.1.1") (d #t) (k 0)) (d (n "glium") (r "^0.27") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1girsb554axrprpn3bl82d294l85mjh06m7dyn7gv11hirx881ls")))

