(define-module (crates-io co lu columnar_derive) #:use-module (crates-io))

(define-public crate-columnar_derive-0.0.1 (c (n "columnar_derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rustfmt") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1j4lj0lbgpc4lx15dd4n1zhsbrfanmlh1v8f62k8rqm2kvylcl6v") (f (quote (("verbose" "rustfmt") ("default")))) (y #t)))

