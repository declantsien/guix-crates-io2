(define-module (crates-io co lu column_derive) #:use-module (crates-io))

(define-public crate-column_derive-0.0.1 (c (n "column_derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rustfmt") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1z3097ds45w6kd65giwdanpsisjxfc1g4pg4yq1lpdsrxy3xv1nk") (f (quote (("verbose" "rustfmt") ("default"))))))

