(define-module (crates-io co li coliseum) #:use-module (crates-io))

(define-public crate-coliseum-0.1.0 (c (n "coliseum") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "151lb0l38xxgi1i5w0ykwyp24807wcljirx2kcqbzch1gky3ynfm")))

