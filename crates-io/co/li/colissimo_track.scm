(define-module (crates-io co li colissimo_track) #:use-module (crates-io))

(define-public crate-colissimo_track-0.1.0 (c (n "colissimo_track") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "hyper") (r "^0.13.6") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full" "macros"))) (d #t) (k 2)))) (h "141djzjzjhgxqn4x4vkj76i0fhz35g8489pwz1xsqh8idfxz6z5d")))

(define-public crate-colissimo_track-0.1.1 (c (n "colissimo_track") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.16") (d #t) (k 0)) (d (n "hyper") (r "^0.14.20") (f (quote ("client" "http1" "http2"))) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full" "macros"))) (d #t) (k 2)))) (h "0lwlglnk2fvcjysmgl8yrnpap5d3rll02wprq3jxmgy1f4pjsrbc")))

