(define-module (crates-io co as coasys_juniper_subscriptions) #:use-module (crates-io))

(define-public crate-coasys_juniper_subscriptions-0.17.0 (c (n "coasys_juniper_subscriptions") (v "0.17.0") (d (list (d (n "coasys_juniper") (r "^0.16.0") (k 0)) (d (n "futures") (r "^0.3.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.18") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "13pvjnk78gjqh9jnilx14g8qwwsxxb42ss0kcpcvcc1a39nak6q8") (r "1.65")))

