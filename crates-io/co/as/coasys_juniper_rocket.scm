(define-module (crates-io co as coasys_juniper_rocket) #:use-module (crates-io))

(define-public crate-coasys_juniper_rocket-0.9.0 (c (n "coasys_juniper_rocket") (v "0.9.0") (d (list (d (n "coasys_juniper") (r "^0.16.0") (k 0)) (d (n "coasys_juniper") (r "^0.16.0") (f (quote ("expose-test-schema"))) (d #t) (k 2)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.22") (d #t) (k 0)) (d (n "inlinable_string") (r "^0.1.15") (d #t) (k 0)) (d (n "pear") (r "^0.2.4") (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.3") (k 0)) (d (n "serde_json") (r "^1.0.18") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1f7c3n7lfs4429s82scf7sqdwjc34029ip1h66g6ygiqlhj7rwsv") (r "1.65")))

