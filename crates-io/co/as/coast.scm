(define-module (crates-io co as coast) #:use-module (crates-io))

(define-public crate-coast-0.0.0 (c (n "coast") (v "0.0.0") (d (list (d (n "wasm-bindgen") (r "^0.2.58") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.35") (f (quote ("AddEventListenerOptions" "Event" "EventTarget"))) (d #t) (k 0)))) (h "0gx203kxsac1zlh7dij7j1mrp08hipba5zqlr7svzj3jzyfl1a2h")))

