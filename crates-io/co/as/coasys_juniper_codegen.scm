(define-module (crates-io co as coasys_juniper_codegen) #:use-module (crates-io))

(define-public crate-coasys_juniper_codegen-0.16.0 (c (n "coasys_juniper_codegen") (v "0.16.0") (d (list (d (n "derive_more") (r "^0.99.8") (d #t) (k 2)) (d (n "futures") (r "^0.3.22") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (d #t) (k 2)) (d (n "syn") (r "^1.0.90") (f (quote ("extra-traits" "full" "parsing" "visit" "visit-mut"))) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "07px3mnhk9250d7746y9cd84m7l3q8sdbdmx8mljza7cykjm8d4a") (r "1.65")))

