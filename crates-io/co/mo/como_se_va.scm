(define-module (crates-io co mo como_se_va) #:use-module (crates-io))

(define-public crate-como_se_va-0.1.0 (c (n "como_se_va") (v "0.1.0") (h "0d0mxmbx3pfyxh4fijasz9bda9f53196s4iigskj3jqvw1wdi94s")))

(define-public crate-como_se_va-0.0.0 (c (n "como_se_va") (v "0.0.0") (h "1hcxrns7z01yffipj2byw7xb7yam91ap29i11w06bzj131qp8r1d")))

