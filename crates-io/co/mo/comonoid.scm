(define-module (crates-io co mo comonoid) #:use-module (crates-io))

(define-public crate-comonoid-0.1.0 (c (n "comonoid") (v "0.1.0") (h "0h87r6v9jdmgwlnfzspc4v8x92x8405a5lp9a9kqk2q3cnnn0g8z") (y #t)))

(define-public crate-comonoid-0.1.1 (c (n "comonoid") (v "0.1.1") (h "192jbf7zp5jhxdj0h2ca5cgypkwkxwfzyr463wix40gy4l5rrrv5") (y #t)))

(define-public crate-comonoid-0.1.2 (c (n "comonoid") (v "0.1.2") (h "184nghdq5rqa613r0fpapgz955mnbzkf05mbsr255flv4k2nzc3x") (y #t)))

(define-public crate-comonoid-0.2.0 (c (n "comonoid") (v "0.2.0") (h "1cxq40rr4ppc5xa6kh0aflmi315jakxmvwq6ji8sxxi881mgcjb8") (y #t)))

(define-public crate-comonoid-0.2.1 (c (n "comonoid") (v "0.2.1") (h "0pra7923vrcwy6av663hkg3sbgp9naryqigvzigaxmqvxsi31p4j") (y #t)))

(define-public crate-comonoid-0.3.0 (c (n "comonoid") (v "0.3.0") (h "02ay9gqxfsz7mj400bbw5rrrcm2vqnwflmnpbifgb046pxqwsqqy") (y #t)))

