(define-module (crates-io co da coda) #:use-module (crates-io))

(define-public crate-coda-0.1.0 (c (n "coda") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "user-hooks"))) (k 2)))) (h "0lgqsphzhjrs6llm3g75qb1xxdk0yf4kslm4hsj3fgbid8frb1x4") (f (quote (("hex") ("default" "hex"))))))

(define-public crate-coda-0.1.1 (c (n "coda") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "user-hooks"))) (k 2)))) (h "16wyd5cjb3yjb3i4mi1cabx28x20g7a3kmp0xfsbr4jhiqh3g6ak") (f (quote (("hex") ("default" "hex"))))))

