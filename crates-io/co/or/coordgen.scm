(define-module (crates-io co or coordgen) #:use-module (crates-io))

(define-public crate-coordgen-0.1.0 (c (n "coordgen") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.4") (f (quote ("rustls-tls" "blocking" "json"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "12l85qqqm6yfxslsfl9kpf4rgipfk17qs0g15ich2zsjp4qlk783")))

(define-public crate-coordgen-0.2.0 (c (n "coordgen") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.4") (f (quote ("rustls-tls" "blocking" "json"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0x5zfgwrs6kwc9cra0sd78f6jyxc42jyg2gw3zclvrxi39ijl7bz")))

(define-public crate-coordgen-0.2.1 (c (n "coordgen") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.4") (f (quote ("rustls-tls" "blocking" "json"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0mjgm2n9iix0km0mjrnf1hzirbzkmc95rqh6r8mk8dfax4kwshs7")))

(define-public crate-coordgen-0.2.2 (c (n "coordgen") (v "0.2.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.4") (f (quote ("rustls-tls" "blocking" "json"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "12v49m4lgjzyp26vh05sf6bkn4spfq9mfhm3fssgf4zqg0zmcndg")))

