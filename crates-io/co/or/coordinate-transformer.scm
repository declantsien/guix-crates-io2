(define-module (crates-io co or coordinate-transformer) #:use-module (crates-io))

(define-public crate-coordinate-transformer-1.0.0 (c (n "coordinate-transformer") (v "1.0.0") (h "1bkycpzb1m9yb7x82wskd01fh21xxm5390lvdkhfwp24a726a4g3")))

(define-public crate-coordinate-transformer-1.1.0 (c (n "coordinate-transformer") (v "1.1.0") (d (list (d (n "assert-be-close") (r "^1.0.0") (d #t) (k 2)))) (h "15117i6436yamskzdwv047w8q5vzvxcvpqi33lf7lbb7h7mi55db")))

(define-public crate-coordinate-transformer-1.2.0 (c (n "coordinate-transformer") (v "1.2.0") (d (list (d (n "assert-be-close") (r "^1.0.0") (d #t) (k 2)))) (h "15gvd1zjv3cjcv9skyg8lzmiq71pw0fdv7jpq68s2r4fifzpmbi0")))

(define-public crate-coordinate-transformer-1.3.0 (c (n "coordinate-transformer") (v "1.3.0") (d (list (d (n "close-to") (r "^0.1.0") (d #t) (k 2)))) (h "1dl1p6lp77hhl7bvk5xwfi2sgjnq40zl5xpjq0i6268s7hbg1yb1") (y #t)))

(define-public crate-coordinate-transformer-1.4.0 (c (n "coordinate-transformer") (v "1.4.0") (d (list (d (n "close-to") (r "^0.1.0") (d #t) (k 2)))) (h "0bp2i9gb2jjvwi6nwady5vzly1km3s22k68wcr0zffl9hxyg30si")))

(define-public crate-coordinate-transformer-1.5.0 (c (n "coordinate-transformer") (v "1.5.0") (d (list (d (n "close-to") (r "^0.1.0") (d #t) (k 2)) (d (n "vec-x") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1n3d71dycj7dlz1vsym4dcq8d5hniqs53qil2075r3yqyrp1hr2n")))

(define-public crate-coordinate-transformer-1.6.0 (c (n "coordinate-transformer") (v "1.6.0") (d (list (d (n "close-to") (r "^0.1.0") (d #t) (k 2)) (d (n "vec-x") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1i99h054fhs2bwfpj4lhgwh0k9bsafbrwxnc6j2f6185njr9my6c")))

(define-public crate-coordinate-transformer-1.7.0 (c (n "coordinate-transformer") (v "1.7.0") (d (list (d (n "close-to") (r "^0.1.0") (d #t) (k 2)) (d (n "num") (r "^0.4.3") (d #t) (k 0)) (d (n "vec-x") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0fq6h025iw3cd9g35x4ysdahz1yamb6j20754c0anlnbghs85mba")))

