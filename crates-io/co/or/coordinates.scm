(define-module (crates-io co or coordinates) #:use-module (crates-io))

(define-public crate-coordinates-0.1.0 (c (n "coordinates") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "roots") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1n4p19n81xyffxk0s4c7vb3d21zcr6r8c2vrmv9118l8ns60zi6i") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-coordinates-0.1.1 (c (n "coordinates") (v "0.1.1") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "roots") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "013wg1gbf6w25250v9h7kp3ablfxspfddc599z6jmmm636mb4ns6") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-coordinates-0.2.0 (c (n "coordinates") (v "0.2.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "roots") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1lhrff6jzjs0rn9ndp85i1gr5fhyb97yzn2cfkcr3iyp5zqnsir8") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-coordinates-0.3.1 (c (n "coordinates") (v "0.3.1") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "roots") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nwvbv7z6n4vdgqr7vs6fqvvxi4jyiajrfdqg54f7g3phzgk5f4c") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-coordinates-0.3.0 (c (n "coordinates") (v "0.3.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "roots") (r "^0.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0nqgrsrpjvbkr6v97pnwiiywlbj0xkvrdk3q3g0rf12bxxbj36zw") (s 2) (e (quote (("serde" "dep:serde"))))))

