(define-module (crates-io co or coord_2d) #:use-module (crates-io))

(define-public crate-coord_2d-0.1.0 (c (n "coord_2d") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "08l6x2vgdb5qvnrfy4x6ajiggah01swm464zxis8wzm9vzyhnkcz")))

(define-public crate-coord_2d-0.2.0 (c (n "coord_2d") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0lz753vb4wf1lzi0844mwhcv30f3fhbchj35inririkh82y5cqhx")))

(define-public crate-coord_2d-0.2.1 (c (n "coord_2d") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0r4xcwr19kf8k3zmzp8icr027half54mr355axz65wyr0z3p8z88") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.2 (c (n "coord_2d") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1wp6bziia9pnnhwbpik8hpha8rjhr87rnigcgksfb2ifx6p66a3z") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.3 (c (n "coord_2d") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0vqi6nl9hlfjbycppa6vx673ir5jyz4mgvgb6kvsr22hn9djagza") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.4 (c (n "coord_2d") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0jv38mjkw5mbzrl6b2b5i49ffkhs76fpc52d0a056s3qlgzisv0h") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.5 (c (n "coord_2d") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "012q8x05i00fi1d1jqqwp5wi3axq5jh6aj8q2g8f44vxlxh1h3z0") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.6 (c (n "coord_2d") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0hhsnsmjfwh054m8kz8afcchiim9qiajwbdx3d2x6nk5i8phkm4z") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.7 (c (n "coord_2d") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0wr0y66ihdp6q7amwd0ynj4ic8a2ibh3yd2g63i6d68yd5c9k0v9") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.8 (c (n "coord_2d") (v "0.2.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1k1bcf5fcvd13nq10his9lrz1avg411xk9cikb6gjc7h0c1w2r76") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.9 (c (n "coord_2d") (v "0.2.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "16zc091hvpb0w1fblvad1zpx7mncgmwnlchx4b1ygh5xm4y4k6y3") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.10 (c (n "coord_2d") (v "0.2.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0fljjxfnh42kwl0if3f9vkdx8xsxvq2irp8sljygwpx7p3hwbvcj") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.11 (c (n "coord_2d") (v "0.2.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1ghjjqkjmyjh93sz1dgljyrcfqipxqwgabff101q4d25d14774pp") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.12 (c (n "coord_2d") (v "0.2.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0whn4xj8swvnkxgzxl3g36492b4vzir4q3n1n1059bynzlvp36pb") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.13 (c (n "coord_2d") (v "0.2.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1596dbl56siwrf66m1dqz4qj0wj70a0z022yzw7gdcxwjwr49kxh") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.14 (c (n "coord_2d") (v "0.2.14") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1d20s9r4ra9jbr9k2zsr50j32512migsfcdhkkz46nhzqj0nqry8") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.15 (c (n "coord_2d") (v "0.2.15") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0qjkj3ip7dixkvy32by34jkxa55dnnb4rxz05yd0wj53inc17zbl") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.16 (c (n "coord_2d") (v "0.2.16") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0ycimzjgkwcrg4rcs1jydzyb0njddn2m49lqawi0i6785ijk2hnf") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.17 (c (n "coord_2d") (v "0.2.17") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1nm3nmvv1zqvpyj958jwaz9b6m12y59syf7jplrbyfy3hfxz2dlc") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.18 (c (n "coord_2d") (v "0.2.18") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1zakidyza5ziry32rd0ivlaxkm5fnpf2cl3hi60v2cp4czgb2w9a") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.19 (c (n "coord_2d") (v "0.2.19") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "16s1zljbigjxapjwnxdkm2x4yqpawks9xybnvag0cjp2160136ap") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.20 (c (n "coord_2d") (v "0.2.20") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "147sqdm863gwpl1m9352vh2p04z43sa62qrh9gmsx0d5fibjpmlm") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.21 (c (n "coord_2d") (v "0.2.21") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0g80z85q074bjjypvirjdkp2k5kxbk742j71dw3xh544z7gi0dak") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.22 (c (n "coord_2d") (v "0.2.22") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1lz6z1jjp3ymjxmgv548fhxjzpq0las25gp7ljdvmkla72jl6nlf") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.23 (c (n "coord_2d") (v "0.2.23") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1cp67qwlzslmnwkik1lwia9sckgbi4rkiihij0hrn3wxbbn4jpgn") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.24 (c (n "coord_2d") (v "0.2.24") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1s3r2qgl93inrlwpy0i48wdcyp6k7clb0qn2rykgx8x2jx7n47sw") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2.25 (c (n "coord_2d") (v "0.2.25") (d (list (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0vkrsg5nslm2xs4ywhgqnx8yaibjq02606d3ndrv34jnxp2dma67") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.3.0 (c (n "coord_2d") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1f16g42g1a047spa98sajqfmh819axchs57lwa4grbz8fmrrmnll") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.3.1 (c (n "coord_2d") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1jgyg45m20jl06cmkn7hnd5y66m1dq2m2g18y8csn2yvr7jrzy87") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.3.2 (c (n "coord_2d") (v "0.3.2") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "04gc9jyw8y7y04329csacmqc63sqijijl2n852jlifnjrn7gqkwl") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.3.3 (c (n "coord_2d") (v "0.3.3") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1lisxh9ghywsr0hb93nblsyzrf6qy919r2hycgskrj18gh180p9g") (f (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.3.4 (c (n "coord_2d") (v "0.3.4") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0dg2sg8f5s089jv0h2sz0yddasn6xvpah4fq7nwzwj227gp931q0") (f (quote (("std") ("serialize" "serde"))))))

(define-public crate-coord_2d-0.3.5 (c (n "coord_2d") (v "0.3.5") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0pbmbp3x58hyqrbsy8d6qfd3c5fm44sp15x6y6zsy1zspaq82dj6") (f (quote (("std") ("serialize" "serde"))))))

(define-public crate-coord_2d-0.3.6 (c (n "coord_2d") (v "0.3.6") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0mzczqaspljp31gnm04d7lz4s6g4q1q7bnq11f2zqhxzv0p0l0pi") (f (quote (("std") ("serialize" "serde"))))))

