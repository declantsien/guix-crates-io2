(define-module (crates-io co or coord) #:use-module (crates-io))

(define-public crate-coord-0.1.0 (c (n "coord") (v "0.1.0") (h "0gkw9v9b3rxf63ifzflyk002zvav60cqjb1rn58rnqca10drznab")))

(define-public crate-coord-0.2.0 (c (n "coord") (v "0.2.0") (h "1r8mjjyy3xilynw0gpzh76vh5gf6vgamzxazazlkdnpm43a7s8ys")))

(define-public crate-coord-0.3.0 (c (n "coord") (v "0.3.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0q1m3b8n3d2hm5niy6b4z3rqx63vq8fr78jgjjfz4b6199vs5s6c")))

(define-public crate-coord-0.4.0 (c (n "coord") (v "0.4.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0y97c46fynk31nkjigp7rvl2s69dsrckprijsk6lk2fvaizihz07") (f (quote (("large_defaults"))))))

(define-public crate-coord-0.5.0 (c (n "coord") (v "0.5.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0b6pk7q2r5awd195isqfz0rwdhf43w3phk2r17mvp894f260389m") (f (quote (("large_defaults"))))))

(define-public crate-coord-0.6.0 (c (n "coord") (v "0.6.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0p6ggg539ghc3iy5lsscjbalsx1j5h0gwf6qwd9fsq0ymhanm3pq") (f (quote (("large_defaults"))))))

(define-public crate-coord-0.6.1 (c (n "coord") (v "0.6.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0i17zn05690njv48dssdysfpdy9mi8xmc58zy46dhv6ds5sh0d60") (f (quote (("large_defaults"))))))

(define-public crate-coord-0.7.0 (c (n "coord") (v "0.7.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0f53qf7l4f6vs2hsij4h6wgnx1hmsblvz1a54b8hc55xx2h41nj0") (f (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.8.0 (c (n "coord") (v "0.8.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07845nvvydy30fx08795g4ca9h3f6d7a3xw48x8ka80f34ij24h1") (f (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.8.1 (c (n "coord") (v "0.8.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1nc144jhyn7rdk0m640d4xqdjs2r23g36jxc8jx2j64xzk78kc72") (f (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.9.0 (c (n "coord") (v "0.9.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "08favnp4hmhpfg7hmi4hqwl5m9xppqiaza7rl3l0xzjg30brlm41") (f (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.9.1 (c (n "coord") (v "0.9.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00b8a4614b4g9wdl6majikyc2dakdnlr6a5n2hcbfvvj67pgn6cj") (f (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.10.0 (c (n "coord") (v "0.10.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01bpkdmdg1z297a2qd3hnfwyw0kiwnffxx0yjjv1q6v60z3fsvp9") (f (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.10.1 (c (n "coord") (v "0.10.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1s274cxy0vq1nx20gw8pz47wm5sv7jh9sdqnfjvkkifmhhcgc9jj") (f (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.11.0 (c (n "coord") (v "0.11.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04v4s9ihzc3b6xsqi6d9jrglgaq6k13ahfcc6nx0nbb0s98jpc3w") (f (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.11.1 (c (n "coord") (v "0.11.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0a3y00blqg1pil2y0vhw4if3g1fpl6xa1j0m606gzv8y75gc1jsq") (f (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

