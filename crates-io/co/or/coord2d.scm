(define-module (crates-io co or coord2d) #:use-module (crates-io))

(define-public crate-coord2d-0.1.0 (c (n "coord2d") (v "0.1.0") (h "1awsr69pnz3zpbaf56hcmll8gh3616z9am78mf6xl1pzn1jsxp7d") (y #t)))

(define-public crate-coord2d-0.1.2 (c (n "coord2d") (v "0.1.2") (h "1mhk7zksks38dcd3rh397x24ky9zk9zfvlfyr6m8f2blw7nnvd08") (y #t)))

(define-public crate-coord2d-0.1.4 (c (n "coord2d") (v "0.1.4") (h "1cr4184ni9x5ard0v780qfq2xfkjrsbfzr6mmcc7a298mdx94mad") (y #t)))

(define-public crate-coord2d-0.1.8 (c (n "coord2d") (v "0.1.8") (h "1hf60cl5y2rn7pgbrgnxijjbjd67aazmynr7xqx63m2hwd2yi80i") (y #t)))

(define-public crate-coord2d-0.1.7 (c (n "coord2d") (v "0.1.7") (h "1321is1bi4vlyb81fgdfl6vd69k7ywpqw8sfvf69k7k7jj6ygd26") (y #t)))

(define-public crate-coord2d-0.1.9 (c (n "coord2d") (v "0.1.9") (h "1d7kxwica6g121jjbq8sfv7z9dnz8kj08x0dc36i2s99bvfaxxbg")))

(define-public crate-coord2d-0.1.10 (c (n "coord2d") (v "0.1.10") (h "1pf1783idnpla4dds79azg67hykijf534canyl5hfhh43nd8dgav") (y #t)))

(define-public crate-coord2d-0.1.11 (c (n "coord2d") (v "0.1.11") (h "0dr28ndcgd8alwk66n8mvq7s8ysqxly7j2sbzhdgnraxhmdps845") (y #t)))

(define-public crate-coord2d-0.1.12 (c (n "coord2d") (v "0.1.12") (h "1jvf2c93vh0rb99qpm3h72q28cpc5da730acjq615g54ygm3dx7g") (y #t)))

(define-public crate-coord2d-0.1.13 (c (n "coord2d") (v "0.1.13") (h "1xq2k6kqbx4jla5b9pk15symp7hjrvpmyzs11kg2912b21lp1d6k") (y #t)))

(define-public crate-coord2d-0.1.14 (c (n "coord2d") (v "0.1.14") (h "0inms69vcaabamhr119h8a0xdkfffs0670h5kzgjljpfihgm9w5d") (y #t)))

(define-public crate-coord2d-0.1.15 (c (n "coord2d") (v "0.1.15") (h "0a1lj8njvhpsv9qpf92xhngk3l6z6azmz8hgn9x0s918bwbyswyp") (y #t)))

(define-public crate-coord2d-0.1.16 (c (n "coord2d") (v "0.1.16") (h "1xw41kazcv0p3g9ylnysvpqbrrs7f31kr8hai4npw1crqp2x77z3") (y #t)))

(define-public crate-coord2d-0.1.17 (c (n "coord2d") (v "0.1.17") (h "04ar4q2x6zhlbg6z7kc9kacgpwhaajkrx6qhdqyfz88sxwwbf8za") (y #t)))

(define-public crate-coord2d-0.1.18 (c (n "coord2d") (v "0.1.18") (h "0pixbw9nnbim25xb7lzzc7wfy8cyh24wr7bhf0iyajiq98z8yqby") (y #t)))

(define-public crate-coord2d-0.1.19 (c (n "coord2d") (v "0.1.19") (h "05z5h2wnmd5l9rvkjsb8z8hlp39wvbhailk69d9mgx7rvb4fb1yh") (y #t)))

(define-public crate-coord2d-0.2.0 (c (n "coord2d") (v "0.2.0") (h "0jjynbrgx14x5ca5y08mfamqw34d147mn1rh5vhw2nc93qcygvc9") (y #t)))

(define-public crate-coord2d-0.2.1 (c (n "coord2d") (v "0.2.1") (h "172bs6ghfrgj6ndn0vp19fvf7c6bzanyh9q48jqsxb6h6r8b135i") (y #t)))

(define-public crate-coord2d-0.2.2 (c (n "coord2d") (v "0.2.2") (h "00818li22ldnldr353k4xsdg4h38kr1bhqbi9s1s2pyjrqnqld99") (y #t)))

(define-public crate-coord2d-0.2.3 (c (n "coord2d") (v "0.2.3") (h "0791pnfaidynyjdzdmq5a8kz8gf10h5rg9kd2lxyz4nixp411jq3") (y #t)))

(define-public crate-coord2d-0.2.4 (c (n "coord2d") (v "0.2.4") (h "1zdcwsihjsnmsgyzl6liwrdianb3zjbbavdzlrnnm4qcc27i9qfl") (y #t)))

(define-public crate-coord2d-0.2.5 (c (n "coord2d") (v "0.2.5") (h "0mx0fham029j6ihh2f03kxgj3s7x5p2swaan8hpkzyhvqmrva2z6") (y #t)))

(define-public crate-coord2d-0.2.6 (c (n "coord2d") (v "0.2.6") (h "0rlyvvz3k5ja43jsl37w6jhxmix3by6amgyrmnq25wrp4m6vlqf4") (y #t)))

(define-public crate-coord2d-0.2.7 (c (n "coord2d") (v "0.2.7") (h "0mwrc72vfrc1l9kzdhlcfxsxciihchzq8z7i4h2h927hsswgbkbd") (y #t)))

(define-public crate-coord2d-0.2.8 (c (n "coord2d") (v "0.2.8") (h "1zs8lzr62iv91fc36xr4lvksrjg6ivwjk19nlzzqbyqkxb1arr75") (y #t)))

