(define-module (crates-io co or coordinates_outliers) #:use-module (crates-io))

(define-public crate-coordinates_outliers-0.1.0 (c (n "coordinates_outliers") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple_accumulator") (r "^0.3.1") (d #t) (k 0)))) (h "090ir5p2pjmk4f9i5p1xnhw74i9vcmd1q6aa07w58x126kp0z0cn")))

(define-public crate-coordinates_outliers-0.2.1 (c (n "coordinates_outliers") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "simple_accumulator") (r "^0.3.1") (d #t) (k 0)))) (h "1yyyasxkm6fw8hwn2b3araxmf3gl92h5kqpbclzspz5dvsvrw9pd")))

