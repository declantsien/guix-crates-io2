(define-module (crates-io co bi cobin) #:use-module (crates-io))

(define-public crate-cobin-0.1.0 (c (n "cobin") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)))) (h "1fqqjp512dpjdynd3nwahaadwq43dgndk4z201sznsy8nwy439zn")))

(define-public crate-cobin-0.1.1 (c (n "cobin") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)))) (h "0gr56ds9jf1p48sa1sycf7jy2c8w4n94qfcyxxc6savmchl15v4f")))

