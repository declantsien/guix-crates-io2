(define-module (crates-io cq db cqdb-sys) #:use-module (crates-io))

(define-public crate-cqdb-sys-0.1.0 (c (n "cqdb-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "0ldjf2s1p8kxhviw4cva64l97vkmax1sf0mhsasw204jzikk6k1z") (l "cqdb")))

(define-public crate-cqdb-sys-0.1.1 (c (n "cqdb-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "09bgzphhv4rjzq0a4z3pi7z8v2ggzdmrhbrjgmyvm2xc6l535xaa") (l "cqdb")))

