(define-module (crates-io cq db cqdb) #:use-module (crates-io))

(define-public crate-cqdb-0.1.0 (c (n "cqdb") (v "0.1.0") (d (list (d (n "arr_macro") (r "^0.1.3") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "087g28pbh26p2mxyv431slrv4kz8hxkbqrn4w1bm7lsq64hixh33")))

(define-public crate-cqdb-0.2.0 (c (n "cqdb") (v "0.2.0") (d (list (d (n "arr_macro") (r "^0.1.3") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "1p5rgjhm8sr9i2m5idmb21cym9bdka2blja0rfa2k6k8c9p0haxj")))

(define-public crate-cqdb-0.3.0 (c (n "cqdb") (v "0.3.0") (d (list (d (n "arr_macro") (r "^0.1.3") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "025z0lbjsjpjgdsmk8w063hkzcq1imi2ssgpy4ya74kjr5z4csd8")))

(define-public crate-cqdb-0.4.0 (c (n "cqdb") (v "0.4.0") (d (list (d (n "arr_macro") (r "^0.1.3") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "0461p9lwzbyf0mm6992g90rzhy8y16xw0gsms8djdaiv4igb7bbk")))

(define-public crate-cqdb-0.5.0 (c (n "cqdb") (v "0.5.0") (d (list (d (n "arr_macro") (r "^0.1.3") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "0przinyvh5nvr9k9x87818zv1rb1c8xv4x4sn8la2c7ppr98nm4b")))

(define-public crate-cqdb-0.5.1 (c (n "cqdb") (v "0.5.1") (d (list (d (n "array-init") (r "^1.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "08fhvj2avwa9pz4fcnzccqyac41drl45pni2n5f8n6hn26yg8s0q")))

(define-public crate-cqdb-0.5.2 (c (n "cqdb") (v "0.5.2") (d (list (d (n "array-init") (r "^1.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "0j6d5wmfzs5yv1vnlaz3gfnpmcydhqa6q9dm44w0dapx82a2l72w")))

(define-public crate-cqdb-0.5.3 (c (n "cqdb") (v "0.5.3") (d (list (d (n "array-init") (r "^1.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "028ywcdzwnxk2mldc0vq40cfq2rsm2kfymj93m1lw57x5b6dxpmc")))

(define-public crate-cqdb-0.5.4 (c (n "cqdb") (v "0.5.4") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "0vli7f3bw0p0mg6amlgj8cfvf4rpcamhaawv56n2vkw661l1iplp")))

(define-public crate-cqdb-0.5.5 (c (n "cqdb") (v "0.5.5") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^0.2.14") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "1rf0n3rmpn9w11mzd8hswygrz0xbm5pb0vkrbh39gwq0wvl1i0zc")))

(define-public crate-cqdb-0.5.6 (c (n "cqdb") (v "0.5.6") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^0.2.16") (f (quote ("std"))) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "14xvl3552z5afmqd35aqbqcsyjpmpyypja067ayxnfdx2h7hcrgr")))

(define-public crate-cqdb-0.5.7 (c (n "cqdb") (v "0.5.7") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^1.0.0") (f (quote ("std"))) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "17dyvv0f0l8f9zd921zmdp5jaj757mglwjzymwhp496jy9j0psf5")))

(define-public crate-cqdb-0.5.8 (c (n "cqdb") (v "0.5.8") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "bstr") (r "^1.0.0") (f (quote ("std"))) (k 0)) (d (n "jhash") (r "^0.1.1") (d #t) (k 0)) (d (n "cqdb-sys") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (d #t) (k 2)))) (h "1x3ac1wnpy722wrrgi6l4n48241zw5wqs0x36qgfny77lw54c5a0")))

