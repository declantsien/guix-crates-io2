(define-module (crates-io cq t- cqt-rs) #:use-module (crates-io))

(define-public crate-cqt-rs-0.1.0 (c (n "cqt-rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hann-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon" "matrixmultiply-threading"))) (d #t) (k 0)) (d (n "rustfft") (r "^6.1") (d #t) (k 0)))) (h "1120isiicw8s6hwh5v5xk3r9wx7ak3p05c35lklyp1ifwpvw7pkc")))

