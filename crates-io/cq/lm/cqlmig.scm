(define-module (crates-io cq lm cqlmig) #:use-module (crates-io))

(define-public crate-cqlmig-0.0.1 (c (n "cqlmig") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.61") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1j60p8nwfxg5h9rylz6bnzlfkjysmx3vnpw0p66mbn9lxw1r5k94")))

