(define-module (crates-io cq lm cqlmig-cdrs-tokio) #:use-module (crates-io))

(define-public crate-cqlmig-cdrs-tokio-0.0.1 (c (n "cqlmig-cdrs-tokio") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.61") (d #t) (k 0)) (d (n "cdrs-tokio") (r "^7.0.3") (d #t) (k 0)) (d (n "cqlmig") (r "^0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "dockertest") (r "^0.3.1") (d #t) (k 2)))) (h "1zpyvbqjfkglfan096ca1q3piin7bskwql8l274ljhwssqcm1hv7")))

