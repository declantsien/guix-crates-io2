(define-module (crates-io cq ui cquill-migrate) #:use-module (crates-io))

(define-public crate-cquill-migrate-0.0.1 (c (n "cquill-migrate") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "scylla") (r "^0.8.0") (d #t) (k 0)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "10nzyli1vm7bykvkhdcl69sg18blkwx2naz1r3svpgkka8dvnink") (y #t)))

