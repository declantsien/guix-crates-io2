(define-module (crates-io cq l- cql-rust) #:use-module (crates-io))

(define-public crate-cql-rust-0.0.1 (c (n "cql-rust") (v "0.0.1") (h "0flf0f5cdpsxbgbp8bafgjal3sy7i0r4y75a8q1bjj5kx721q2lv")))

(define-public crate-cql-rust-0.0.2 (c (n "cql-rust") (v "0.0.2") (h "1ni68gawnnddkrdqq4577nj5rzg4g9fjxsh5dpg06fhl1d3nphvj")))

(define-public crate-cql-rust-0.0.3 (c (n "cql-rust") (v "0.0.3") (h "12r8rbzp38z5mhav4y9rnklxzp07izs5pmvcjcw9q88w60grz0f1")))

