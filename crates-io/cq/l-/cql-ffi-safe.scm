(define-module (crates-io cq l- cql-ffi-safe) #:use-module (crates-io))

(define-public crate-cql-ffi-safe-0.0.3 (c (n "cql-ffi-safe") (v "0.0.3") (d (list (d (n "cql_ffi") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0xgixl7n1h2rlcb4lcjziq0igq5mjhqvwawhwyhhaj52sp14lc7y")))

(define-public crate-cql-ffi-safe-0.0.4 (c (n "cql-ffi-safe") (v "0.0.4") (d (list (d (n "cql_ffi") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "00qpcp5g6frblllvkh5kyggqi6vaynbv5vy838m2ksk6xd8pvy88")))

(define-public crate-cql-ffi-safe-0.0.5 (c (n "cql-ffi-safe") (v "0.0.5") (d (list (d (n "cql_ffi") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0s2zgrhb1cwn7p7cq0h7v1nm7524nlq1pk14i6m2b0nz3sgrln7v")))

(define-public crate-cql-ffi-safe-0.0.6 (c (n "cql-ffi-safe") (v "0.0.6") (d (list (d (n "cql_ffi") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0jhl9b1hkijsb1qkhinzpci005ricw87j2d3im0jiinyrb2kjikg")))

(define-public crate-cql-ffi-safe-0.0.7 (c (n "cql-ffi-safe") (v "0.0.7") (d (list (d (n "cql_ffi") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "012z3lymb9lg7wb1rfbjfihnfh689fcpqfnij8cl5wl3da5lad2g") (y #t)))

