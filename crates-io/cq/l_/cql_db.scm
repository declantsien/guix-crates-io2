(define-module (crates-io cq l_ cql_db) #:use-module (crates-io))

(define-public crate-cql_db-0.1.0 (c (n "cql_db") (v "0.1.0") (d (list (d (n "cql_model") (r "^0.1.0") (d #t) (k 0)) (d (n "cql_u64") (r "^0.1.0") (d #t) (k 0)))) (h "1z7a2zphp5567kyi1j47659r0j9mp7q280w6lxxd6zgcx565wr4x")))

(define-public crate-cql_db-0.2.0 (c (n "cql_db") (v "0.2.0") (d (list (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "cql_u64") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "1d8jqwg2s4nbr5ax2skgkfv69w44wh0cyxjv7h4m70i4jk2kbpif")))

(define-public crate-cql_db-0.2.1 (c (n "cql_db") (v "0.2.1") (d (list (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "cql_u64") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "1xnda3213pj485hjwh428k1bci3crsmqlrj410lrmbh0rh2y4x3c")))

(define-public crate-cql_db-0.2.2 (c (n "cql_db") (v "0.2.2") (d (list (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "cql_u64") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "0pbw4bsi5g4c2yd7yz78gkqv06aj2bi2cjn74wy4hqj95a1l8j9n")))

(define-public crate-cql_db-0.2.3 (c (n "cql_db") (v "0.2.3") (d (list (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "cql_u64") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "1jjcy84g18dfbbgsccmjpwv2dwxdffqmj95z5mpqwgrav2h947pl")))

(define-public crate-cql_db-0.2.4 (c (n "cql_db") (v "0.2.4") (d (list (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "cql_u64") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "14mq43yn5i6ks4zjq4fplp1v8cdhvkx3npbi4ni8xiiyk4rkv391")))

