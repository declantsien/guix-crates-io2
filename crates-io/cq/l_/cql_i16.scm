(define-module (crates-io cq l_ cql_i16) #:use-module (crates-io))

(define-public crate-cql_i16-0.2.0 (c (n "cql_i16") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cql_db") (r "^0.2.4") (d #t) (k 2)) (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "1m16286f2yj7dr4sv1mff5rbvfw6i1s4a1791xsf8bi2y7lx0ji2")))

