(define-module (crates-io cq l_ cql_u64) #:use-module (crates-io))

(define-public crate-cql_u64-0.1.0 (c (n "cql_u64") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cql_model") (r "^0.1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "128ndfrci4hhvqaxi4fx0vdky8nsnj34yscq2rlikr53xhmmzq46")))

(define-public crate-cql_u64-0.2.0 (c (n "cql_u64") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cql_db") (r "^0.1.0") (d #t) (k 2)) (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "0n3q2y3iiicmvywz60ky4h4kfy0a7psijp6sxs15cx57dxppc5y9")))

(define-public crate-cql_u64-0.2.1 (c (n "cql_u64") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cql_db") (r "^0.2") (d #t) (k 2)) (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "07dm8m8r2cxcywxq4q8n78qpi18jmal7iznh85q7fiaab43zaglq")))

(define-public crate-cql_u64-0.2.2 (c (n "cql_u64") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cql_db") (r "^0.2") (d #t) (k 2)) (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "08s08hjgj6piszmy9s0xnlp3d4y9h0gij5sjznxbwg6jqf7phg2y")))

(define-public crate-cql_u64-0.2.3 (c (n "cql_u64") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cql_db") (r "^0.2.4") (d #t) (k 2)) (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "0waarjpc66zf8c1kq1w79jzdgawpj0q18r2f94fpbh8xzz4jhyy4")))

