(define-module (crates-io cq l_ cql_model) #:use-module (crates-io))

(define-public crate-cql_model-0.1.0 (c (n "cql_model") (v "0.1.0") (h "1lkkvhv4zbfkcsq44f4sxycb1rskfh3p7cv5ii33gj3ljws5gr4j")))

(define-public crate-cql_model-0.2.0 (c (n "cql_model") (v "0.2.0") (h "1f00f0w2cz5fdap3vd78hn5nx2fa52j6g60mg8bwwqh1l0xlrn33")))

