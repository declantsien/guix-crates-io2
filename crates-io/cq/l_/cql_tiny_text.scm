(define-module (crates-io cq l_ cql_tiny_text) #:use-module (crates-io))

(define-public crate-cql_tiny_text-0.1.0 (c (n "cql_tiny_text") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cql_db") (r "^0.1.0") (d #t) (k 2)) (d (n "cql_model") (r "^0.1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "1nkmlg3176djnbr2dgg6c04pc06w8wr78lp5qdswcrnllb5mi7ii")))

(define-public crate-cql_tiny_text-0.2.0 (c (n "cql_tiny_text") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cql_db") (r "^0.2") (d #t) (k 2)) (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "16330hy8hx9l2jpi72q8h81087lmi7v48cn8ynlkpklzm8s2j56v")))

(define-public crate-cql_tiny_text-0.2.1 (c (n "cql_tiny_text") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cql_db") (r "^0.2.4") (d #t) (k 2)) (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "0b5zlv1lmsyjgf4ijr4cl8ps5g4km0k3ifh2m3x8w40l58fljflf")))

