(define-module (crates-io cq l_ cql_bindgen) #:use-module (crates-io))

(define-public crate-cql_bindgen-0.0.1 (c (n "cql_bindgen") (v "0.0.1") (h "1d2xddiff4ylhv35ivmm39vp9acym7bny5y9gxgsnqb8av5a8hnp")))

(define-public crate-cql_bindgen-0.0.2 (c (n "cql_bindgen") (v "0.0.2") (h "0s2fhxdvvc1ps3bi1d9w29dv56f7qgfb8m9zbsyvbh31v7b6wgc4")))

(define-public crate-cql_bindgen-0.0.3 (c (n "cql_bindgen") (v "0.0.3") (h "0j2ml1kxqzyp157ms518krn0wcrhsmkqpkig3fax2k6amn8aczlj")))

(define-public crate-cql_bindgen-0.0.4 (c (n "cql_bindgen") (v "0.0.4") (h "0ay9plzjv9rkdqzyl7ckvm4nxcab22iwrf71jx49cxyvcrck8pyd")))

(define-public crate-cql_bindgen-0.0.5 (c (n "cql_bindgen") (v "0.0.5") (h "0pyn990b3y83i88jvfpzjp799k2hvrs4knn6hvrczgnm4brzkwpb")))

(define-public crate-cql_bindgen-0.0.6 (c (n "cql_bindgen") (v "0.0.6") (h "12w8y44z02pbsc8iqjslg2axck2qafrr9qbssr0ylb8hqr7b3059")))

(define-public crate-cql_bindgen-0.0.7 (c (n "cql_bindgen") (v "0.0.7") (h "00qphm3n5x94ib8rznk3y5bccdlck3xl8df0mq0acjp5z05mrqya")))

(define-public crate-cql_bindgen-0.0.8 (c (n "cql_bindgen") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1ychll7cnw34q2hyl06k02jjb14mr1p95g8bwsdwj4ggaxz2wndf")))

(define-public crate-cql_bindgen-0.0.10 (c (n "cql_bindgen") (v "0.0.10") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "18bvq5cyi783d7f1gmwp0f43xa8lf7w5qsm2mp2r9xrh2yf7f73m")))

(define-public crate-cql_bindgen-0.0.11 (c (n "cql_bindgen") (v "0.0.11") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "13axsqyhl31hv30619m71yqlzvmir4v34p1p6wlm1q5bd9pd1srm")))

(define-public crate-cql_bindgen-0.0.12 (c (n "cql_bindgen") (v "0.0.12") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1sdp0r9dqvh36vk8li4wnvz4x1c2p6v6ygqs63nwmml24kj0igxw")))

(define-public crate-cql_bindgen-0.0.13 (c (n "cql_bindgen") (v "0.0.13") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "139p30zz9nk9409gyq2h2ix16av7pbg4dxhkv1ifmhpba61526qf")))

(define-public crate-cql_bindgen-0.0.14 (c (n "cql_bindgen") (v "0.0.14") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "077bp1ns4nr7jxrb3n8kcfaad5v64k7xw2yn6wlj1c0g1lh2as7i")))

(define-public crate-cql_bindgen-0.0.15 (c (n "cql_bindgen") (v "0.0.15") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0fankz6hc7vl6nd9i16d69ailip078ky3b64943p5cygincvdlya")))

(define-public crate-cql_bindgen-0.1.1 (c (n "cql_bindgen") (v "0.1.1") (d (list (d (n "clippy") (r "*") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "threadpool") (r "*") (d #t) (k 0)))) (h "06lri7mjhv783mw1vpqzi7r5m0dh4k16hb1nxm0ja2rhsmqrrxbl")))

(define-public crate-cql_bindgen-0.1.2 (c (n "cql_bindgen") (v "0.1.2") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0iyh22gbs51xz7p97gm9khfr14gl8mwm6sfngxagxjw4pb37av7n")))

(define-public crate-cql_bindgen-0.1.3 (c (n "cql_bindgen") (v "0.1.3") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1jqbw2c6p4dhihyylnlmc9pr0bahi0dw1c5cbxnlcd1hq5czq4ak")))

(define-public crate-cql_bindgen-0.2.0 (c (n "cql_bindgen") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1j53iqmc6fy1n3nxq0bgagjymdchwlj3b0z1y5p8ingbyll6xb6i")))

