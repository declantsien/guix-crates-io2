(define-module (crates-io cq l_ cql_f64) #:use-module (crates-io))

(define-public crate-cql_f64-0.2.0 (c (n "cql_f64") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cql_db") (r "^0.2.4") (d #t) (k 2)) (d (n "cql_model") (r "^0.2") (d #t) (k 0)) (d (n "cql_storage_type_testing_lib") (r "^0.3.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.3.2") (d #t) (k 2)))) (h "1p4cdsld7q9xd472k102lchpddxj32f4175v4jpryap3liwxbw6h")))

