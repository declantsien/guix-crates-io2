(define-module (crates-io cq lp cqlparser) #:use-module (crates-io))

(define-public crate-cqlparser-0.1.0 (c (n "cqlparser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "serde-wasm-bindgen") (r "^0.3.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0796jif54x49mgb7in70lx9gbx0n1v7wg74x1n0m4w7dbhjjsci9")))

