(define-module (crates-io cq rs cqrs-eventsourcing) #:use-module (crates-io))

(define-public crate-cqrs-eventsourcing-0.1.0 (c (n "cqrs-eventsourcing") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0rykj40wmx9fxslrxk1zz2dwzprwzfk9xwwzfdyksm34dlvzhwzk") (y #t)))

(define-public crate-cqrs-eventsourcing-0.1.1 (c (n "cqrs-eventsourcing") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "08b5jvmwycai5939n8hraqyjb6jv1cqgfi7jzz7m4wphzpyfiqrz")))

