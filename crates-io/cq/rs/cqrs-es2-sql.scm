(define-module (crates-io cq rs cqrs-es2-sql) #:use-module (crates-io))

(define-public crate-cqrs-es2-sql-0.1.0 (c (n "cqrs-es2-sql") (v "0.1.0") (d (list (d (n "cqrs-es2") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (f (quote ("with-serde_json-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1m5gnc3ji06kymq8jbfl61pxdgggn2zx2pzj6z5akyqrkbiwks2h")))

