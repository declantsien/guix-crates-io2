(define-module (crates-io cq rs cqrs) #:use-module (crates-io))

(define-public crate-cqrs-0.1.0 (c (n "cqrs") (v "0.1.0") (d (list (d (n "cqrs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "cqrs-todo-core") (r "^0.1.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "14kszrlx1vwgms2jcgfmpqzvjsqwvmwvvxsp61fgblicf87m46vk")))

(define-public crate-cqrs-0.2.0 (c (n "cqrs") (v "0.2.0") (d (list (d (n "cqrs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "cqrs-todo-core") (r "^0.1.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "14qhixsnkjww7cl6h9v9n1mac19741lrwfk6lcahl975ald3wj28")))

(define-public crate-cqrs-0.2.1 (c (n "cqrs") (v "0.2.1") (d (list (d (n "cqrs-core") (r "^0.1.1") (d #t) (k 0)) (d (n "cqrs-todo-core") (r "^0.1.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0j3sxzp2dwck8xkyrgjhbxwbc5h7l93nw5y3p6x7kad6whdixh85")))

(define-public crate-cqrs-0.3.0 (c (n "cqrs") (v "0.3.0") (d (list (d (n "cqrs-core") (r "^0.2.1") (d #t) (k 0)) (d (n "cqrs-todo-core") (r "^0.1.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "06m5r76yyvalh6ypj2pzhw8xwj3prvs3s1br1hc17rrlf2avr2qi")))

(define-public crate-cqrs-0.3.1 (c (n "cqrs") (v "0.3.1") (d (list (d (n "cqrs-core") (r "^0.2.1") (d #t) (k 0)) (d (n "cqrs-todo-core") (r "^0.2.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "void") (r "^1.0") (d #t) (k 0)))) (h "0ckcsl5maa451zh9if13yphdvplc4ks0hp9x43pdz7m7lyjac87j")))

