(define-module (crates-io cq rs cqrs_builder) #:use-module (crates-io))

(define-public crate-cqrs_builder-0.1.0 (c (n "cqrs_builder") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0jyfwdckcmw0pnsfrhx5ny5jq3y47yal6zmd0f6gwklcpkhymyh0")))

(define-public crate-cqrs_builder-0.1.1 (c (n "cqrs_builder") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1w8jlxc2xsbnwmdc9626aakkhfc1inmxay50npirrqs30h51172r") (f (quote (("full-priority"))))))

