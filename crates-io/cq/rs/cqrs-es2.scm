(define-module (crates-io cq rs cqrs-es2) #:use-module (crates-io))

(define-public crate-cqrs-es2-0.2.0 (c (n "cqrs-es2") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0gk56ysma92rd32swfr9bx0ddgyshykxm5m3jz1j5dj5vdcr478h")))

(define-public crate-cqrs-es2-0.2.1 (c (n "cqrs-es2") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "15frcqxwyra593scfw90qv9gwiy8d99zpnlnbw1wg750v862v31p")))

(define-public crate-cqrs-es2-0.2.2 (c (n "cqrs-es2") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1y8fvrc9xba2zl6zixg73kq2skdrcarcc08xsshb013yivy5az1v")))

(define-public crate-cqrs-es2-0.2.3 (c (n "cqrs-es2") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0nazm7p61ddir1fyhg37malvzk3221ypfjjrn61kjmpcvnkdw4ni")))

(define-public crate-cqrs-es2-0.2.4 (c (n "cqrs-es2") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "03bkscni46vrchwsd876nj06f7df6pqq3ymzy76f5hn4ql071ns2")))

(define-public crate-cqrs-es2-0.2.5 (c (n "cqrs-es2") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0k8l5q08dmd0jvh9j5nb03jsz9wr6lxsvwmvka0arp7br14gk1ra")))

(define-public crate-cqrs-es2-0.3.0 (c (n "cqrs-es2") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "08dbzjyann4shz3fbhxhfhr5z921ym1lsgxs6ph64rq7jh5n42i3")))

(define-public crate-cqrs-es2-0.4.0 (c (n "cqrs-es2") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1cyz4iqdcx51d6q77a1i5sibrmhh5gpf92jb50xrpnmg5v6l909f")))

(define-public crate-cqrs-es2-0.5.0 (c (n "cqrs-es2") (v "0.5.0") (d (list (d (n "postgres") (r "^0.19.1") (f (quote ("with-serde_json-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1x7iwkg998i0m0ahyxlgs9jx1ahg7crpxq4si77f851xdjk8r5nc")))

(define-public crate-cqrs-es2-0.6.0 (c (n "cqrs-es2") (v "0.6.0") (d (list (d (n "postgres") (r "^0.19.1") (f (quote ("with-serde_json-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "03w5j5pj2dm37r5zizr1c085mmk5d0kqx6lmf19p4zfn8948l3gm")))

(define-public crate-cqrs-es2-0.7.0 (c (n "cqrs-es2") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (f (quote ("with-serde_json-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0vlvyvsh11rvwxvig3p97n5c9zpjcy2c7l1xdf9xqwg8nyhfj6ig")))

(define-public crate-cqrs-es2-0.8.0 (c (n "cqrs-es2") (v "0.8.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1nlbakqfy3kmldm8m6dzkld1dgxjln0i3yv9ys2dfz88k5xp1zlb")))

(define-public crate-cqrs-es2-0.9.0 (c (n "cqrs-es2") (v "0.9.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0d590qjicdqq4g4qxfglq80ywgx9m2bxmj3xvl0mf702m00sp66z")))

(define-public crate-cqrs-es2-0.10.0 (c (n "cqrs-es2") (v "0.10.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0xrvjpyd41l93q0rw8wqpa14qqwlzabzgbsnrllb8znx82hb31b0")))

