(define-module (crates-io cq rs cqrs-core) #:use-module (crates-io))

(define-public crate-cqrs-core-0.1.0 (c (n "cqrs-core") (v "0.1.0") (d (list (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "0alq0wzgx8ly7mni50a9f4rvg8rs70a5skj9n48i1cpc4ldczfh5")))

(define-public crate-cqrs-core-0.1.1 (c (n "cqrs-core") (v "0.1.1") (d (list (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "0rbmxwsiq7k7nycclcmhsaypm8a53vmq1s9fjgphp1v6l5ympavb")))

(define-public crate-cqrs-core-0.2.0 (c (n "cqrs-core") (v "0.2.0") (d (list (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "04r9yb6vpkbbn7kxiqm92psyl3dfv1mr70iincsczfw9d0v0gxlg") (y #t)))

(define-public crate-cqrs-core-0.2.1 (c (n "cqrs-core") (v "0.2.1") (d (list (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "1g3syifxlisg04kmlk1kjrx2qc7rzqm19nwg6z7pf6yw6vc69xir")))

(define-public crate-cqrs-core-0.2.2 (c (n "cqrs-core") (v "0.2.2") (d (list (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "15r8gqgkyfqb81b343csyr0nq1wfrswq6fg2g2srz6pnf159g83s")))

