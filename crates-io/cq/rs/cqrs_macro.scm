(define-module (crates-io cq rs cqrs_macro) #:use-module (crates-io))

(define-public crate-cqrs_macro-0.1.0 (c (n "cqrs_macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1r44syc8frp7da2s1jrs1scd6c655xgg2l3167m9bn15p65g2pb7")))

(define-public crate-cqrs_macro-0.1.1 (c (n "cqrs_macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0djkgp6skjcrshhm4pi7a2d1rmnzcci9bhch2v154i1vpkhynpqg") (f (quote (("async-listener"))))))

