(define-module (crates-io cq rs cqrs-proptest) #:use-module (crates-io))

(define-public crate-cqrs-proptest-0.1.0 (c (n "cqrs-proptest") (v "0.1.0") (d (list (d (n "cqrs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0569qs9158zhc9kyih5drf979prwfcinz2hm7dzq70zl59z3hphd")))

(define-public crate-cqrs-proptest-0.2.0 (c (n "cqrs-proptest") (v "0.2.0") (d (list (d (n "cqrs-core") (r "^0.2.1") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01d0pvj70lp5s65sm6wbahq031vprafxs36sz5xcrq28l7q96lp7")))

(define-public crate-cqrs-proptest-0.3.0 (c (n "cqrs-proptest") (v "0.3.0") (d (list (d (n "cqrs-core") (r "^0.2.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bazmsmf2jpa0pinxg2vx8l841rizn9lgs6bz2rlq1s1nz91b0pn")))

