(define-module (crates-io mf qe mfqe) #:use-module (crates-io))

(define-public crate-mfqe-0.4.0 (c (n "mfqe") (v "0.4.0") (d (list (d (n "assert_cli") (r "0.6.*") (d #t) (k 2)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "0.6.*") (d #t) (k 0)) (d (n "flate2") (r "1.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "seq_io") (r "0.3.*") (d #t) (k 0)) (d (n "tempfile") (r "3.0.*") (d #t) (k 2)))) (h "1wc88n29bh6a4jsr4kgnsvgb0h8dwyf8py97432pg62lrr9s6p0z")))

(define-public crate-mfqe-0.4.1 (c (n "mfqe") (v "0.4.1") (d (list (d (n "assert_cli") (r "0.6.*") (d #t) (k 2)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "0.6.*") (d #t) (k 0)) (d (n "flate2") (r "1.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "seq_io") (r "0.3.*") (d #t) (k 0)) (d (n "tempfile") (r "3.0.*") (d #t) (k 2)))) (h "15halj1bq4z4cs0rsycbyxxhn99rjxdg3aka9p9hf3zfqv6s9p8c")))

(define-public crate-mfqe-0.5.0 (c (n "mfqe") (v "0.5.0") (d (list (d (n "assert_cli") (r "0.6.*") (d #t) (k 2)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "0.7.*") (d #t) (k 0)) (d (n "flate2") (r "1.0.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "seq_io") (r "0.3.*") (d #t) (k 0)) (d (n "tempfile") (r "3.1.*") (d #t) (k 2)))) (h "08d6lyj2iymx1dp28mwin43n9746jqr3hn0abjqgix7j718i3x1z")))

