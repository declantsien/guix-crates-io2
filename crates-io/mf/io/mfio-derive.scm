(define-module (crates-io mf io mfio-derive) #:use-module (crates-io))

(define-public crate-mfio-derive-0.1.0 (c (n "mfio-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xvq09kx8ia0hv93w7lk7nz03wb36k40jsh3fbm0lc933717vdig") (r "1.72")))

