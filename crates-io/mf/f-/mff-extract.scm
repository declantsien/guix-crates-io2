(define-module (crates-io mf f- mff-extract) #:use-module (crates-io))

(define-public crate-mff-extract-0.1.0 (c (n "mff-extract") (v "0.1.0") (d (list (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tabled") (r "^0.12.0") (d #t) (k 0)) (d (n "tauri-winres") (r "^0.1.1") (d #t) (k 1)))) (h "1cccjvsc0xcnsj9yd7w41njhzhjla4499jcb3c1x25i0nvb2c7hp")))

