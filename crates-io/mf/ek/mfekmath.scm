(define-module (crates-io mf ek mfekmath) #:use-module (crates-io))

(define-public crate-MFEKmath-0.1.0 (c (n "MFEKmath") (v "0.1.0") (d (list (d (n "flo_curves") (r "^0.6") (d #t) (k 0)) (d (n "glifparser") (r "=1.2.5-1") (k 0)) (d (n "kurbo") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plist") (r "^1.3") (d #t) (k 0)) (d (n "skia-safe") (r ">0.0") (o #t) (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "0zm8qsmh4wbrjlmlbr6fbpma3j32hji1dnikxxyisv5h6kjl96ws") (f (quote (("strict") ("skia" "skia-safe" "glifparser/skia") ("default" "glifparser/mfek"))))))

(define-public crate-MFEKmath-0.1.1 (c (n "MFEKmath") (v "0.1.1") (d (list (d (n "flo_curves") (r "^0.6") (d #t) (k 0)) (d (n "glifparser") (r "^1.2") (k 0)) (d (n "kurbo") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plist") (r "^1.3") (d #t) (k 0)) (d (n "skia-safe") (r ">0.0") (o #t) (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "1vvnfr5zxdybb4vwpkfzvlyiy68mj5zppri6ca657fcwqcpd6wcj") (f (quote (("strict") ("skia" "skia-safe" "glifparser/skia") ("default" "glifparser/mfek"))))))

