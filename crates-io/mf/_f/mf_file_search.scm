(define-module (crates-io mf _f mf_file_search) #:use-module (crates-io))

(define-public crate-mf_file_search-0.0.1 (c (n "mf_file_search") (v "0.0.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "grep") (r "^0.3.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4.22") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.1") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0wf7ln918qqzjy8rpc7k8znijn8nl5ywfryb08gdirm8x74pvb82")))

