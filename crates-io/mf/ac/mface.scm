(define-module (crates-io mf ac mface) #:use-module (crates-io))

(define-public crate-mface-0.1.0 (c (n "mface") (v "0.1.0") (d (list (d (n "divider") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "rustybuzz") (r "^0.12") (d #t) (k 0)) (d (n "tessor") (r "^0.1") (d #t) (k 0)) (d (n "tessor_viewer") (r "^0.1") (d #t) (k 2)) (d (n "zeno") (r "^0.3") (d #t) (k 0)))) (h "0ygqxixpmk7y3jlczyx9xihvkngi1sc8l9lsn56ig6cs8bxrqnmy")))

(define-public crate-mface-0.2.0 (c (n "mface") (v "0.2.0") (d (list (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "rustybuzz") (r "^0.12") (d #t) (k 0)) (d (n "tessor") (r "^0.2") (d #t) (k 0)) (d (n "tessor_viewer") (r "^0.2") (d #t) (k 2)) (d (n "zeno") (r "^0.3") (d #t) (k 0)))) (h "1xrwwhhr5ndcsb4j3fvc1lni3zrn1zm5989b9563lchb5q96l04l")))

