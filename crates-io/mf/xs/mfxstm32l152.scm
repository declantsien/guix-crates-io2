(define-module (crates-io mf xs mfxstm32l152) #:use-module (crates-io))

(define-public crate-mfxstm32l152-0.0.1 (c (n "mfxstm32l152") (v "0.0.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "i2c-hal-tools") (r "^0.0.2") (d #t) (k 0)))) (h "0vbysnnaxpd3vp09dh51npjc44dkn93gbkhqck74nsl9zik3kpcm")))

