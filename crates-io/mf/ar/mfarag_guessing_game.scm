(define-module (crates-io mf ar mfarag_guessing_game) #:use-module (crates-io))

(define-public crate-mfarag_guessing_game-0.1.0 (c (n "mfarag_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n9brh398szvyw8fhssv5c6pr50mv1619yif5s00nimw75pxd0lr") (y #t)))

(define-public crate-mfarag_guessing_game-0.1.1 (c (n "mfarag_guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1kih2d6yxn0wbqmia85zd7jm8ryshddj29ikzdmk5k8skf432j4d")))

