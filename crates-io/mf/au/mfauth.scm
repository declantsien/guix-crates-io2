(define-module (crates-io mf au mfauth) #:use-module (crates-io))

(define-public crate-mfauth-1.0.0 (c (n "mfauth") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "hyper") (r "^0") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1gfw1c33873slv202f0s5220wsf0a726niy15x1z7yc7ixh60ab7")))

