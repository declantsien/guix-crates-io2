(define-module (crates-io mf em mfem-sys) #:use-module (crates-io))

(define-public crate-mfem-sys-0.1.0 (c (n "mfem-sys") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "mfem-cpp") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "semver") (r "^1.0.22") (d #t) (k 1)))) (h "1hv0mgjmr3yhr4l4mrcanrm7cndw7qq3psdda8lgj1lflfbgd3z0") (f (quote (("default" "bundled") ("bundled" "mfem-cpp"))))))

(define-public crate-mfem-sys-0.1.1 (c (n "mfem-sys") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "mfem-cpp") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "semver") (r "^1.0.22") (d #t) (k 1)))) (h "0bxhinlnrrhznj8g2rx3zz31vnlwyhwfj0x4whql4p5d48sgbv4x") (f (quote (("default" "bundled") ("bundled" "mfem-cpp"))))))

