(define-module (crates-io mf rc mfrc522) #:use-module (crates-io))

(define-public crate-mfrc522-0.1.0 (c (n "mfrc522") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.0") (d #t) (k 2)))) (h "0ks3kvbfn9kvd25fxfrrfkhw1aawlyx26d74jgybayy6096narpr")))

(define-public crate-mfrc522-0.2.0 (c (n "mfrc522") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)))) (h "1y4ykd0x2z3yk43aslax4gf2li606vbq26k8sn42k5flj39hxxrs")))

(define-public crate-mfrc522-0.3.0 (c (n "mfrc522") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "0ki10qimzkhmnxdxcflcszivjwdybdfvpp3ihw03xhj61f1lw91w")))

(define-public crate-mfrc522-0.3.1 (c (n "mfrc522") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "0mvqa9r8yaqbz714aa1kmi5r4x0hazaw8rl7ksr55p0lyxjx2142")))

(define-public crate-mfrc522-0.4.0 (c (n "mfrc522") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "1257qncqxk1rf5klp9d0hi3yzyavlrd5pgdsk76cv1p4zdz2q32a")))

(define-public crate-mfrc522-0.4.1 (c (n "mfrc522") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "1l7jwidvw4pkg8m8mqnsqbhzi2hrn1wnaap3fm08bbriv38dd64v") (f (quote (("std"))))))

(define-public crate-mfrc522-0.5.0 (c (n "mfrc522") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "05iqhwzb1c6cal67ch67k96cz3fvcjdxyppa3g5wwrwms1x9q3xz") (f (quote (("std"))))))

(define-public crate-mfrc522-0.6.0 (c (n "mfrc522") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "embedded-hal-02") (r "^0.2") (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "=1.0.0-alpha.10") (o #t) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-mock-02") (r "^0.9") (d #t) (k 2) (p "embedded-hal-mock")) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (t "cfg(target_os = \"linux\")") (k 2)))) (h "1p391hqafi10gxbafc9vdxzikx3smdfja9wgwwix56hjfdaqd9g2") (f (quote (("std")))) (s 2) (e (quote (("eh1" "dep:embedded-hal-1"))))))

(define-public crate-mfrc522-0.6.1 (c (n "mfrc522") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "embedded-hal-02") (r "^0.2") (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "=1.0.0-rc.1") (o #t) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-mock") (r "^0.10.0-rc.1") (f (quote ("eh1"))) (d #t) (k 2)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (t "cfg(target_os = \"linux\")") (k 2)))) (h "1xsjd251dlk8pgdbbj84wfy9hgsj08wc5pvha7f52h46q0vshqmv") (f (quote (("std")))) (s 2) (e (quote (("eh1" "dep:embedded-hal-1"))))))

(define-public crate-mfrc522-0.7.0 (c (n "mfrc522") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "embedded-hal-02") (r "^0.2") (o #t) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "^1.0") (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-bus") (r "^0.1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (d #t) (k 2)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (t "cfg(target_os = \"linux\")") (k 2)))) (h "0phia3bm8vc4f3sz7ansicrg33c3qvqqkbbz2d1dmbc7vgky7gar") (f (quote (("std")))) (s 2) (e (quote (("eh02" "dep:embedded-hal-02"))))))

