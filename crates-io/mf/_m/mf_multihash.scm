(define-module (crates-io mf _m mf_multihash) #:use-module (crates-io))

(define-public crate-mf_multihash-0.0.0 (c (n "mf_multihash") (v "0.0.0") (d (list (d (n "arrayvec") (r "^0.3.22") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "ring") (r "^0.7.5") (d #t) (k 0)))) (h "0r78h2ngkq7n4m7r7lsidirdnfcr0raxhk44djdqs42yh7c0x1p1") (y #t)))

