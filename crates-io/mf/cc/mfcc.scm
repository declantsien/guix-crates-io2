(define-module (crates-io mf cc mfcc) #:use-module (crates-io))

(define-public crate-mfcc-0.0.1 (c (n "mfcc") (v "0.0.1") (d (list (d (n "rustfft") (r "^3") (d #t) (k 0)))) (h "1ags8qn2qz2lk6hqqz49ikfk972msgc7pcxb3qqmf9ybshdc5ms2")))

(define-public crate-mfcc-0.1.0 (c (n "mfcc") (v "0.1.0") (d (list (d (n "fftw") (r "^0.6") (f (quote ("system"))) (o #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rustfft") (r "^3") (o #t) (d #t) (k 0)))) (h "1i13mq0ffnkd26ldragawzbnlj0dhlp6dgfdn46jq1xy2gz3x8iy") (f (quote (("fftrust" "rustfft") ("fftextern" "fftw/system") ("default" "fftrust"))))))

