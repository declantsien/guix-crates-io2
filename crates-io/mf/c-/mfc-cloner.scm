(define-module (crates-io mf c- mfc-cloner) #:use-module (crates-io))

(define-public crate-mfc-cloner-0.1.0 (c (n "mfc-cloner") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "log-update") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1idz36iq84mvh3657jijpjf046jpka9y487mdqg07rdvyhfi2gsm")))

(define-public crate-mfc-cloner-0.1.1 (c (n "mfc-cloner") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "log-update") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1fhc75nadwrrcfd2fknybfpk4gd0sbxqs9ic42gypxh56ra1x5ax")))

(define-public crate-mfc-cloner-0.1.2 (c (n "mfc-cloner") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "log-update") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0cjrhq9dwri7mzfrg7n40flmbdf68b4m90xbdgg5swf1y67vjx9h")))

