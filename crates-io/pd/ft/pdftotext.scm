(define-module (crates-io pd ft pdftotext) #:use-module (crates-io))

(define-public crate-pdftotext-0.1.0 (c (n "pdftotext") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0adi3ljs1q1gvm00wz9fwpfyx7gm2c7mhllzcaqp8wrgcg6dhkfi")))

(define-public crate-pdftotext-0.1.1 (c (n "pdftotext") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "02lnnljim6d3gsjb2cfd2q58ccswnz2mqsklfvypdw7szkir05hw")))

(define-public crate-pdftotext-0.1.2 (c (n "pdftotext") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1nya90x94l6m6b3z1n6cibi155m0pd5nl440nval8v5kv819brg9")))

(define-public crate-pdftotext-0.1.3 (c (n "pdftotext") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0qyd2m5q9ligzzwhyilac1kb22qsh5wpkzk3ddv1xgbyl2jndb85")))

(define-public crate-pdftotext-0.1.4 (c (n "pdftotext") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0dg6438miaybza5jqqghlznd6al2xwv4bzjd0mbf4ipa7zmy9gsk") (f (quote (("static-poppler" "cmake") ("default"))))))

(define-public crate-pdftotext-0.1.5 (c (n "pdftotext") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0zbpskmm63c05rf0c1d2p9bx2y6k7bsw7i4s2zh9gkbvwfas6dnq") (f (quote (("static-poppler" "cmake") ("default"))))))

