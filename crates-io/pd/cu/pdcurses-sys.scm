(define-module (crates-io pd cu pdcurses-sys) #:use-module (crates-io))

(define-public crate-pdcurses-sys-0.1.0 (c (n "pdcurses-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03zky1vlvgjh0bxabwiqhc9csxa6yzgpyb91j28fpl38w2vl7j6a")))

(define-public crate-pdcurses-sys-0.2.0 (c (n "pdcurses-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l0bakcff1dirns2jcsckh3cnaqwdcgna4m92kw42qrsaqpwmc3i")))

(define-public crate-pdcurses-sys-0.2.1 (c (n "pdcurses-sys") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kgzvnq7igchq05zdcfv32wdgbmkc5ias7cx8k9gas9blmwkc2sz")))

(define-public crate-pdcurses-sys-0.2.2 (c (n "pdcurses-sys") (v "0.2.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xh07ba7a86lmbi72a2qp1zg2rcnxfz5ad8plqyq7ccmbrffxisq")))

(define-public crate-pdcurses-sys-0.3.0 (c (n "pdcurses-sys") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wfnyz7ysxhgk14vnxl00i29slkx9zcfmr956a6b7r7h087br8r7")))

(define-public crate-pdcurses-sys-0.4.0 (c (n "pdcurses-sys") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cbka55g6m1kbdmbhaxv37rflnw6zxdm5c9nwx6a1njxqadglapw")))

(define-public crate-pdcurses-sys-0.5.0 (c (n "pdcurses-sys") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00c62bffx3gnc0jx3ycbnr0cm2vz1zg6fkpvxhccrqvzls0nbkx3")))

(define-public crate-pdcurses-sys-0.5.1 (c (n "pdcurses-sys") (v "0.5.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wjwf8kmmj5j7ag2l7db85ycmqx9x0fjqvg9wqls0gd897q2jflj")))

(define-public crate-pdcurses-sys-0.6.0 (c (n "pdcurses-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05wlm0jnf47qfpkb6w8691hm31nrb9nhgbm229dxci054nh3ai7k")))

(define-public crate-pdcurses-sys-0.7.0 (c (n "pdcurses-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0apshhh3m10wp3k4l1naa2i1spbxrqi7lbvll3ghy25papz2pqch") (f (quote (("win32a") ("win32"))))))

(define-public crate-pdcurses-sys-0.7.1 (c (n "pdcurses-sys") (v "0.7.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sqn552nz33bmd0d8lcx862lrbxg6fgk5djfblig2q7zjqkx4k88") (f (quote (("win32a") ("win32")))) (l "pdcurses")))

