(define-module (crates-io pd qs pdqselect) #:use-module (crates-io))

(define-public crate-pdqselect-0.1.0 (c (n "pdqselect") (v "0.1.0") (h "09vwywzavhgqgr3vi2ycgv2nd0067jirv36fb3jvp860xikigjaf")))

(define-public crate-pdqselect-0.1.1 (c (n "pdqselect") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "1vis37a2ppvsf9i6ydkdv9gwz7g5ffdadzyiw76mdp91jdnr0y3p")))

