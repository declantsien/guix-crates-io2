(define-module (crates-io pd qs pdqsort) #:use-module (crates-io))

(define-public crate-pdqsort-0.1.0 (c (n "pdqsort") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "120mi95majh5jjnfv8qcgr4d0cxc1fq4krk8qy8yd6jg7y8dvsdg")))

(define-public crate-pdqsort-0.1.1 (c (n "pdqsort") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0m5snnz9xqm2mr8cnwlkb1q9md0jgd3jl4g34q7k679gcl2q8j4v")))

(define-public crate-pdqsort-0.1.2 (c (n "pdqsort") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0z18sq4pp7zljm77r464bwsyzaw3qfh7biqwc42wlj4ir111djnf")))

(define-public crate-pdqsort-1.0.0 (c (n "pdqsort") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "123p9zmj0pkzyj1xf224wvxaxy9v6p9yv5j9chclv8li4dbwa5s4")))

(define-public crate-pdqsort-1.0.1 (c (n "pdqsort") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1c4yc6zwcl6bpjkxk8f4a0m0idwfy04zz1yx8appz1xi266jflg6")))

(define-public crate-pdqsort-1.0.2 (c (n "pdqsort") (v "1.0.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "161simf9pxpg0f2i3rb02b3mncpr8ysj10453jnw1vcwrzb1ap5y")))

(define-public crate-pdqsort-1.0.3 (c (n "pdqsort") (v "1.0.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1rf5qlgkr0l0fk3y56s2pxsc615gnnqz6kxniccgly1h11q1glcj")))

