(define-module (crates-io pd fo pdfork) #:use-module (crates-io))

(define-public crate-pdfork-0.1.0 (c (n "pdfork") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ha1j2mjhzfyn2s1sqyyc51lm43j68c2isqpzgrxx296wan9rldn")))

(define-public crate-pdfork-0.1.1 (c (n "pdfork") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09mv9k61myxqs9hxdnmhhb2brcpdnin8llq74samh29qq584p8d7")))

