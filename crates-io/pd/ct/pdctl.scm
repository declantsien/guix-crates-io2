(define-module (crates-io pd ct pdctl) #:use-module (crates-io))

(define-public crate-pdctl-0.1.0 (c (n "pdctl") (v "0.1.0") (d (list (d (n "pagersduty") (r "^0.1") (d #t) (k 0)))) (h "1pb5xcxjn42gxqcf0hfdvx6dyighmmrdy2nlrh8d7mxydvfanz01")))

(define-public crate-pdctl-0.1.1 (c (n "pdctl") (v "0.1.1") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "pagersduty") (r "^0.2") (d #t) (k 0)))) (h "14pj70nwwm4fqcfgs5p81zbvpf2y7xnf8zmw92h95as2kqi631c7")))

