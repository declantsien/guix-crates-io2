(define-module (crates-io pd al pdal) #:use-module (crates-io))

(define-public crate-pdal-0.1.0 (c (n "pdal") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "pdal-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("default"))) (d #t) (k 0)))) (h "1z04205j6m0pypvx5ahcaipqxnh5sja6fzzhjgjw9pn3mjplg4j2")))

