(define-module (crates-io pd al pdal-sys) #:use-module (crates-io))

(define-public crate-pdal-sys-0.1.0 (c (n "pdal-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "cxx") (r "^1.0.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "env_logger") (r "^0.11") (d #t) (k 1)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1bj54ym29iwc3q55xf7k12al3q3ipj2r28lyrhx923hc8qzaw60w") (l "pdal")))

