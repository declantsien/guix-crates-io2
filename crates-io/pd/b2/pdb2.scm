(define-module (crates-io pd b2 pdb2) #:use-module (crates-io))

(define-public crate-pdb2-0.9.0 (c (n "pdb2") (v "0.9.0") (d (list (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 2)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (d #t) (k 0)))) (h "0p8p1ldsn7i5h0bwyjvnl3r0454vx9vzf7275qdd9c6a3c9hxqq0")))

