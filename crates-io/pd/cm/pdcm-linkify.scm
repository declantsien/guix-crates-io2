(define-module (crates-io pd cm pdcm-linkify) #:use-module (crates-io))

(define-public crate-pdcm-linkify-0.1.0 (c (n "pdcm-linkify") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1jq610ib5nrry47h33yf7bbkvpv86xynqzj3qvc3h74nmpihd6l5")))

