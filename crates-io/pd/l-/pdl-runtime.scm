(define-module (crates-io pd l- pdl-runtime) #:use-module (crates-io))

(define-public crate-pdl-runtime-0.2.0 (c (n "pdl-runtime") (v "0.2.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0ch7g4splrbm26dg7lf3lsmxl9ymafixjp7bg2zbl4wng7ww4did")))

(define-public crate-pdl-runtime-0.2.1 (c (n "pdl-runtime") (v "0.2.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0a7bj1zhd36h0p2795vz2ryk8x5m2xrybipxhkbavvjvj8145cl2")))

(define-public crate-pdl-runtime-0.2.2 (c (n "pdl-runtime") (v "0.2.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "16bm8l023xfyn1sabjz9djmzczcfp0zzarwdr8hj5acqlp39iiir")))

(define-public crate-pdl-runtime-0.2.3 (c (n "pdl-runtime") (v "0.2.3") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1gia8jxmd4lci4rgimqiyfysw1m7mn78bqij2ys0n9dh2shy01ff")))

(define-public crate-pdl-runtime-0.3.0 (c (n "pdl-runtime") (v "0.3.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0z0dxmqqmhwnwig18cf8b5p3pcvkjmk36q6yhdn372b6wc94hs58")))

