(define-module (crates-io pd -s pd-sys) #:use-module (crates-io))

(define-public crate-pd-sys-0.1.0 (c (n "pd-sys") (v "0.1.0") (h "1p7yag1x08n017jw5rf6g37x1g2zyp02wwf0sqyh3pxrjy87q9rd") (f (quote (("instance") ("doubleprecision") ("default"))))))

(define-public crate-pd-sys-0.1.1 (c (n "pd-sys") (v "0.1.1") (h "1gvc37q9aw90y2ajvl16006nh09jyykkcxin7dvm5rw3ljppi8jm") (f (quote (("instance") ("doubleprecision") ("default"))))))

