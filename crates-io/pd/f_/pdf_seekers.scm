(define-module (crates-io pd f_ pdf_seekers) #:use-module (crates-io))

(define-public crate-pdf_seekers-0.1.0 (c (n "pdf_seekers") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.17") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (f (quote ("pom" "pom_parser"))) (d #t) (k 0)) (d (n "pdf-extract") (r "^0.7.2") (d #t) (k 0)) (d (n "tantivy") (r "^0.21.1") (d #t) (k 0)))) (h "1yy016415dx1sxlzjk13ggkdb23igrsal5hbxb668mnpy990ddqc")))

(define-public crate-pdf_seekers-0.1.1 (c (n "pdf_seekers") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.17") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (f (quote ("pom" "pom_parser"))) (d #t) (k 0)) (d (n "pdf-extract") (r "^0.7.2") (d #t) (k 0)) (d (n "tantivy") (r "^0.21.1") (d #t) (k 0)))) (h "0iapqwhlbdk0bc7ph092nnaqkpj6lcvlxg4kipi3r3k9chnkfskb")))

(define-public crate-pdf_seekers-0.1.2 (c (n "pdf_seekers") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.17") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (f (quote ("pom" "pom_parser"))) (d #t) (k 0)) (d (n "pdf-extract") (r "^0.7.2") (d #t) (k 0)) (d (n "tantivy") (r "^0.21.1") (d #t) (k 0)))) (h "16hyap5waakiwcq6qfgsnc7df4505zhids67xck68vld67dvn3qs")))

(define-public crate-pdf_seekers-0.1.3 (c (n "pdf_seekers") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.17") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (f (quote ("pom" "pom_parser"))) (d #t) (k 0)) (d (n "tantivy") (r "^0.21.1") (d #t) (k 0)))) (h "129dkd73bsycawcnnhk0pw2rw6kmqrgqa05xbaxbjbsb2av41q57")))

(define-public crate-pdf_seekers-0.1.4 (c (n "pdf_seekers") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.17") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (f (quote ("pom" "pom_parser"))) (d #t) (k 0)) (d (n "tantivy") (r "^0.21.1") (d #t) (k 0)))) (h "0x3wjpn9s3xypxjv8n7xv55f320g4pyq0xp8qckf29p4kj5kr4ai")))

