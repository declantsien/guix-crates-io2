(define-module (crates-io pd f_ pdf_cli) #:use-module (crates-io))

(define-public crate-pdf_cli-0.1.1 (c (n "pdf_cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("default" "unstable-doc"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("pom_parser" "nom_parser"))) (d #t) (k 0)))) (h "09y5v6pndlf27i61qv5xzn3j4fsa3snksl7wwr8bxjammaxcdy7v")))

(define-public crate-pdf_cli-0.1.2 (c (n "pdf_cli") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("default" "unstable-doc"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("pom_parser" "nom_parser"))) (d #t) (k 0)))) (h "041c6ljjcqwfpsdqjg02sxxqf2nh67i6qd5ci95msi2l9k0bk8ra")))

(define-public crate-pdf_cli-0.1.3 (c (n "pdf_cli") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("default" "unstable-doc"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("pom_parser" "nom_parser"))) (d #t) (k 0)))) (h "1z4kva522cw213nrmrh6ikk5qg0v36p4qkck4qf890w0lhjvbh86")))

(define-public crate-pdf_cli-0.1.4 (c (n "pdf_cli") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("default" "unstable-doc"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("pom_parser" "nom_parser"))) (d #t) (k 0)))) (h "0ln1ppzisahjxjcdji466r4i2g128s7kv087znbbwhnh37w3g2ds")))

(define-public crate-pdf_cli-0.1.5 (c (n "pdf_cli") (v "0.1.5") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("default" "unstable-doc"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("pom_parser" "nom_parser"))) (d #t) (k 0)))) (h "0y8fa26bw370by768yl62kmv3m9kqxm1ifvrqa7b09qx6m9d6hlz")))

(define-public crate-pdf_cli-0.1.6 (c (n "pdf_cli") (v "0.1.6") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("default" "unstable-doc"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("pom_parser" "nom_parser"))) (d #t) (k 0)))) (h "1sikkhjh1fcb517h0w4m7yrhi7d974zsrlx1v8jzz53vwzgaxn85")))

(define-public crate-pdf_cli-0.1.7 (c (n "pdf_cli") (v "0.1.7") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("default" "unstable-doc"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("pom_parser" "nom_parser"))) (d #t) (k 0)))) (h "1in1jpnznavdsmbb6dvb9vwv9l30k0hl54lh1ljh4lh49dxz8qlj")))

(define-public crate-pdf_cli-0.1.8 (c (n "pdf_cli") (v "0.1.8") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("default" "unstable-doc"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("pom_parser" "nom_parser"))) (d #t) (k 0)))) (h "0mbg01l3b4ikvasd7d11if9f7jhi454yq42w9lrjmzngf90pnnyw")))

(define-public crate-pdf_cli-1.0.0 (c (n "pdf_cli") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("default" "unstable-doc"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("pom_parser" "nom_parser"))) (d #t) (k 0)))) (h "03ijp5hqi4mk5qwy32h9h6g4yqxgcmhg372mfn6f5jwgnwpydri7")))

(define-public crate-pdf_cli-1.0.1 (c (n "pdf_cli") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("default" "unstable-doc"))) (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("pom_parser" "nom_parser"))) (d #t) (k 0)))) (h "1dj0kd1pcc88ivpwqfbw63j12ccv08yq2gz81rcgbv7cmgjriifk")))

