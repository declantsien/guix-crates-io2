(define-module (crates-io pd f_ pdf_parser) #:use-module (crates-io))

(define-public crate-pdf_parser-0.1.0 (c (n "pdf_parser") (v "0.1.0") (d (list (d (n "fast-float") (r "^0.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)))) (h "09pywa72pqvji8db9cq45zxazwnkbf924lvr18860zznlai0nsc5")))

