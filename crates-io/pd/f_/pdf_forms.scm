(define-module (crates-io pd f_ pdf_forms) #:use-module (crates-io))

(define-public crate-pdf_forms-0.1.0 (c (n "pdf_forms") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.25") (d #t) (k 0)))) (h "1a40ba4n4s4416jk74gzszdy7bibkkmfz3i5c1nrd9sis00hm05g")))

(define-public crate-pdf_forms-0.2.0 (c (n "pdf_forms") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.25") (d #t) (k 0)))) (h "10g29vww393h4f4bps8vi3ji8wi575098q9bi6y14xnxb8y2g6pf")))

(define-public crate-pdf_forms-0.2.1 (c (n "pdf_forms") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.25") (d #t) (k 0)))) (h "1d36vrq5y0b2wkjix8sfzy7bf410qdabhnhabd9hsm7kwywxfq3l")))

(define-public crate-pdf_forms-0.2.2 (c (n "pdf_forms") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.25") (d #t) (k 0)))) (h "18cichlnsyx5ai198x02xxz0vq07wxphnfr32rynl4qmigqmpqr2")))

(define-public crate-pdf_forms-0.2.3 (c (n "pdf_forms") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.25") (d #t) (k 0)))) (h "0qzxchvx2aii56rm0qv5zqpjnrfwbkl87jcxc8r9rpr67f5c14as")))

(define-public crate-pdf_forms-0.3.0 (c (n "pdf_forms") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.25") (f (quote ("embed_image"))) (d #t) (k 0)))) (h "1pibi2di7icd0ffzf95nbaj6l6ja9f37b37s3627gpf4jy355ps9")))

(define-public crate-pdf_forms-0.3.1 (c (n "pdf_forms") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.25") (f (quote ("embed_image"))) (d #t) (k 0)))) (h "1rn1m5bdvvlh11f5im1jvcxfipc3gjr06fdk810371ib95rh7sd7")))

(define-public crate-pdf_forms-0.3.2 (c (n "pdf_forms") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.25") (f (quote ("embed_image"))) (d #t) (k 0)))) (h "18aikiv2nf5q30a6sql9cvzw09q26n1bmwfg9bz2n46hgb1nc02a")))

(define-public crate-pdf_forms-0.3.3 (c (n "pdf_forms") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.26") (f (quote ("embed_image"))) (d #t) (k 0)))) (h "1fwfd2rlx524d4jjn3dlyr88x2w2dxr9639vhd42dgffsdm11mxg")))

(define-public crate-pdf_forms-0.3.4 (c (n "pdf_forms") (v "0.3.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.26") (f (quote ("embed_image"))) (d #t) (k 0)))) (h "1h8ir8gw553g4szagp6r46cyvdcdb2vhp2r5mrbs36gw9605gs7j")))

