(define-module (crates-io pd f_ pdf_derive) #:use-module (crates-io))

(define-public crate-pdf_derive-0.1.15 (c (n "pdf_derive") (v "0.1.15") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0ns26jybrzd8mh09jx15vw8mq3y4d6mh6ar87gnp9bcd973nplsw")))

(define-public crate-pdf_derive-0.1.16 (c (n "pdf_derive") (v "0.1.16") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "014ac361a8jc454mh5w9wjnm6bc3fxlvzajjhl7i8fcvbnfcslkj")))

(define-public crate-pdf_derive-0.1.17 (c (n "pdf_derive") (v "0.1.17") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "078yc81jpnp0s6y5l2qyr464b4hsbq8kzgb4qncd5x7gdd64a9if")))

(define-public crate-pdf_derive-0.1.20 (c (n "pdf_derive") (v "0.1.20") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06kc89hxf4fi0y3rw10p2y5l28c6npwvbd6g39h3f0bh42ly0m27")))

(define-public crate-pdf_derive-0.1.21 (c (n "pdf_derive") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00678ydnaniva6vv8j0h8xppb62rpkpq8584rfra65lz0as177jn")))

(define-public crate-pdf_derive-0.1.22 (c (n "pdf_derive") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y5lsj79vdlpw93wivl7xvvxbbpi9k3cn5bvx26pkl3m4wk0fh3z")))

(define-public crate-pdf_derive-0.2.0 (c (n "pdf_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q13ralywd40z8g25sdpz617j2404kxsqgpjx7p5xhyy735vjf0h")))

