(define-module (crates-io pd f_ pdf_signing) #:use-module (crates-io))

(define-public crate-pdf_signing-0.1.0 (c (n "pdf_signing") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("chrono_time" "nom_parser"))) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)))) (h "0r9sqg5k00q6ab82psrxscj361ghybr9z22gafw4ig1ha2i29661")))

