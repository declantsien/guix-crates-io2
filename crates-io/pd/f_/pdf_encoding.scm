(define-module (crates-io pd f_ pdf_encoding) #:use-module (crates-io))

(define-public crate-pdf_encoding-0.1.0 (c (n "pdf_encoding") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "16gnqzbfv4k6slrlhvsvxlssdb8ns8dkfvagfkh8c9mc45bam5kq")))

(define-public crate-pdf_encoding-0.2.0 (c (n "pdf_encoding") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1kbpjj7a27ns9yvqzdwsrv5dkzrlliy0awx895y60zr22xwh0ry0")))

(define-public crate-pdf_encoding-0.3.0 (c (n "pdf_encoding") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0h4cp34ia5y3srwdyg8ks559kzpx5p45992cqzdk5fq974bnhx7d")))

(define-public crate-pdf_encoding-0.4.0 (c (n "pdf_encoding") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0cbnbfzqwgms34a8x9hm6rl5x9a5v6vqb25ik83r6m0nyrjksy87")))

