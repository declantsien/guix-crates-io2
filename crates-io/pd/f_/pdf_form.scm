(define-module (crates-io pd f_ pdf_form) #:use-module (crates-io))

(define-public crate-pdf_form-0.1.0 (c (n "pdf_form") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "1s5qz3jfibn38xfa9fm3rjr00iywxicnvnhwcy18i3bp034l9g30")))

(define-public crate-pdf_form-0.2.0 (c (n "pdf_form") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "0mziwswx5xmd5fqrrqmsv63jmpvxpy2pwjnk1pfsn4a2h8j6f12c")))

(define-public crate-pdf_form-0.3.0 (c (n "pdf_form") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.25") (d #t) (k 0)))) (h "1k9vf10fa4xv2gaj0mxln9n22sh48shc4gih155klry5w40ldq9z")))

(define-public crate-pdf_form-0.4.0 (c (n "pdf_form") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.25") (d #t) (k 0)))) (h "0a3pb1i0k8jpf25z0yhzjrm8im39zz65nxnx6jmvh079cvdfhbsz")))

