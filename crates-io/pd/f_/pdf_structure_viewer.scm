(define-module (crates-io pd f_ pdf_structure_viewer) #:use-module (crates-io))

(define-public crate-pdf_structure_viewer-0.1.0 (c (n "pdf_structure_viewer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "lopdf") (r "^0.27.0") (f (quote ("chrono_time" "nom_parser"))) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0rl33krhyzfgmn8k5s74dn9mnyhqs4flnfaqhi812clcf6408n3p")))

