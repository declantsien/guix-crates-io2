(define-module (crates-io pd f_ pdf_form_ids) #:use-module (crates-io))

(define-public crate-pdf_form_ids-0.2.0 (c (n "pdf_form_ids") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "0i7wv1kjzc4rnlncpilacc877ai7xjm60dml05dw9ba41r0b1464")))

(define-public crate-pdf_form_ids-0.3.0 (c (n "pdf_form_ids") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "1ngz9a5hwv3fjs509b2cwp0sll3nh5kw699jdiwlwj7bg202hwh6")))

(define-public crate-pdf_form_ids-0.4.0 (c (n "pdf_form_ids") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "158xpic7133s3y1hmm1m5nni4mv7nv81z6rhzwaw6q084fbwqx7a")))

(define-public crate-pdf_form_ids-0.5.0 (c (n "pdf_form_ids") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "0lw7c3l6wgarrqmmvrm2ap8ap6w6p9cfc89kp7ivcmaa0ly5ayxm")))

(define-public crate-pdf_form_ids-0.5.1 (c (n "pdf_form_ids") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "1n5igzrhkda4bmh3z3dhlnlgqlk1ypq9mb5556vivpr0r66wcxas")))

(define-public crate-pdf_form_ids-0.6.0 (c (n "pdf_form_ids") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "08jm9kh249nr7v9w6p85gkpzc8cvlv900qga9hcdswgw8kssv209")))

