(define-module (crates-io pd bs pdbs_benchmark) #:use-module (crates-io))

(define-public crate-pdbs_benchmark-0.1.0 (c (n "pdbs_benchmark") (v "0.1.0") (d (list (d (n "cpu-time") (r "^1") (d #t) (k 0)) (d (n "csf") (r "^0.1") (d #t) (k 0)) (d (n "fsum") (r "^0.1") (d #t) (k 0)) (d (n "ph") (r "^0.8") (d #t) (k 0)) (d (n "puzzles") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)))) (h "1s99nnjfirz976fszpdalj1m2xqz740syv65waniml8f87g9dink")))

