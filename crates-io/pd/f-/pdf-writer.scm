(define-module (crates-io pd f- pdf-writer) #:use-module (crates-io))

(define-public crate-pdf-writer-0.1.0 (c (n "pdf-writer") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1srikqygvmdgw6cq8vh1wv3ww7hj44jnwx4rgbvxwy8h8zfkjsg7")))

(define-public crate-pdf-writer-0.2.0 (c (n "pdf-writer") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0bjm4x1icpa7a7m9b29dlvjjh6dm6yw76hbfnn2whhvmhn2kypsj")))

(define-public crate-pdf-writer-0.3.0 (c (n "pdf-writer") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1vrhv0pj065pw1f45vfsz60c4drrw9cajsw933anmzws5bkkv4wv")))

(define-public crate-pdf-writer-0.3.1 (c (n "pdf-writer") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "11332qiss3ik2wxcbm5m3zdclvk7bz5c7vsnn2d8v7k5vhd9dm17")))

(define-public crate-pdf-writer-0.3.2 (c (n "pdf-writer") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "099qqb4lg3wi0w4082divv4gwhzlgzhhivkpqqkj08s3d5mkjrdy")))

(define-public crate-pdf-writer-0.4.0 (c (n "pdf-writer") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 0)))) (h "0r9ghh4xgblm7ibrj9wa3zvijp60ra0ih3dnp9r4r818gid4lz4v")))

(define-public crate-pdf-writer-0.4.1 (c (n "pdf-writer") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 0)))) (h "1x30dd51m480f1zxrd9bj8ncxrd7x2ca41hhp8f8345cyak61mrn")))

(define-public crate-pdf-writer-0.5.0 (c (n "pdf-writer") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.4") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "1zcqzm9a4a3ndz85yb61lxpjwmrisn49f4sc1xb6f9kgcpizinwi")))

(define-public crate-pdf-writer-0.6.0 (c (n "pdf-writer") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "1bfyvpnpz5rrh96wyn4djrjjvhqydkb0qjwzrpr2c9hrlcrrp7r4")))

(define-public crate-pdf-writer-0.7.0 (c (n "pdf-writer") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "0i7nj5i79v4psll7v0hdk79znxdxggwy8xs9rdc7rriqfmy5zx33")))

(define-public crate-pdf-writer-0.7.1 (c (n "pdf-writer") (v "0.7.1") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "1dhdh4r3xz38vwk6fwphajp5lgd9mac1y5rpjrfzr5m6iqbhz41h")))

(define-public crate-pdf-writer-0.8.0 (c (n "pdf-writer") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "01724w2w5q419c1vwxqm7gzifsp74n65ggd8zp3lnqd4zarjxbw6")))

(define-public crate-pdf-writer-0.8.1 (c (n "pdf-writer") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "0iqlqzndv80i2x7kck3q7cgbsv0a4wz6krm7hqzsd2lnr13vqxwx")))

(define-public crate-pdf-writer-0.9.0 (c (n "pdf-writer") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "139b6jrx19xdffwbkj3dz4jnpgbzs841yjfpa8h71gq3rl4i8rab")))

(define-public crate-pdf-writer-0.9.1 (c (n "pdf-writer") (v "0.9.1") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "00lawc29lf2r6mknp6c5h2zqhk3iz1pavnnvzggnvlwmrzl78239")))

(define-public crate-pdf-writer-0.9.2 (c (n "pdf-writer") (v "0.9.2") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "1bzy31l1ywig0k7n1kiki89d2qx5frd919153szmg1725m7najv4")))

(define-public crate-pdf-writer-0.9.3 (c (n "pdf-writer") (v "0.9.3") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png" "jpeg"))) (k 2)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 2)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "0arksb7ahf389q0r7gjm3y3x4fh4v7nckahwcrp82g06ams15s94")))

