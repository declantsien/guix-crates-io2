(define-module (crates-io pd f- pdf-word-count) #:use-module (crates-io))

(define-public crate-pdf-word-count-0.1.0-alpha (c (n "pdf-word-count") (v "0.1.0-alpha") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "1ww96rpqm7g9xdpb1v3rhypk5z5lhmsi9lwl1sdr9hmslwihm5pf")))

(define-public crate-pdf-word-count-0.1.0-alpha.2 (c (n "pdf-word-count") (v "0.1.0-alpha.2") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "1kd8fpywhznlrh8gl7yy2y7xj48r5x0v7jahdz167rsi5p6cwzfw")))

(define-public crate-pdf-word-count-0.1.0 (c (n "pdf-word-count") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "lopdf") (r "^0.15.1") (d #t) (k 0)))) (h "0ibg5qh1yirpzniz5npzw9kr749bk94sp0hq533s1jb6ssl70bjr")))

