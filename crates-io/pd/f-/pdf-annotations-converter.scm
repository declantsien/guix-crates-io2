(define-module (crates-io pd f- pdf-annotations-converter) #:use-module (crates-io))

(define-public crate-pdf-annotations-converter-0.1.0 (c (n "pdf-annotations-converter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0arfpr71a07ssamanh6bpf2845nx9pqz7vazz61yp1bqdzw6adia")))

(define-public crate-pdf-annotations-converter-0.1.1 (c (n "pdf-annotations-converter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1z8qsmgv4d3hgwy3jzrm1lpnjfp6lfpc58fkrfl7xq9ikwkrq38a")))

(define-public crate-pdf-annotations-converter-0.1.2 (c (n "pdf-annotations-converter") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0nirx91c7xbnshghggfw13ccpmqxfqxrrbq6whymxbhli8j9zij2")))

(define-public crate-pdf-annotations-converter-0.1.3 (c (n "pdf-annotations-converter") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0rnzbd60vg67p9iygfkqrq0zwk3zlwkf92jbqwfbgz1rg4hdpsyz")))

(define-public crate-pdf-annotations-converter-0.2.0 (c (n "pdf-annotations-converter") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1nayv391s67kwnvg2gig9agli1vdsdhncpyq2wmmvrkp64k89gmx")))

