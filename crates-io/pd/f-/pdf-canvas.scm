(define-module (crates-io pd f- pdf-canvas) #:use-module (crates-io))

(define-public crate-pdf-canvas-0.5.2 (c (n "pdf-canvas") (v "0.5.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 1)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0agwxfm3mjq0dmjyrqriaj0ym0rn53ydf694z6xl19mv6rb1xdmm")))

(define-public crate-pdf-canvas-0.5.4 (c (n "pdf-canvas") (v "0.5.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 1)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0qq7jsn0y426kd7fam67n0wc83nj8mcdvxhd7r6vwalnyidgyjzn")))

(define-public crate-pdf-canvas-0.6.0 (c (n "pdf-canvas") (v "0.6.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 1)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1rl7p88mq0j4bywrig3xmgm18nci59nmyp4pyj87f6gkzvgqb64s")))

(define-public crate-pdf-canvas-0.7.0 (c (n "pdf-canvas") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 1)))) (h "0f4s59rhqzhg3s0xs0yjf1mf4ayas1jsnxlqg366dhl1qvxfnxsf")))

