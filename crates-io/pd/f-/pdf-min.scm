(define-module (crates-io pd f- pdf-min) #:use-module (crates-io))

(define-public crate-pdf-min-0.1.0 (c (n "pdf-min") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^0.1.21") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)))) (h "14xh05ic539svqf683z52114adlpb3ndb849wmxzfki0i5lcn0f4")))

(define-public crate-pdf-min-0.1.1 (c (n "pdf-min") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^0.1.21") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)))) (h "0rqswkq7zlmar1gwasv5gi682dlqap1fpp6xx80v6wdm3ax4shrf")))

(define-public crate-pdf-min-0.1.2 (c (n "pdf-min") (v "0.1.2") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^0.1.21") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "03rp7kpbqfib0g96m605pil21cgkxrhl4fcnl7bfqkm4w1177md2")))

(define-public crate-pdf-min-0.1.3 (c (n "pdf-min") (v "0.1.3") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^0.1.21") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "1wfy1avvf456wybjk480mz4608rafffgxxkvpplrf2h5sv18p08d")))

(define-public crate-pdf-min-0.1.4 (c (n "pdf-min") (v "0.1.4") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^0.1.21") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "1vmh5l8ap5qsvw6s1fk9wmsx2j2r1vlp0c1xg46d41wnqgid31k7")))

(define-public crate-pdf-min-0.1.5 (c (n "pdf-min") (v "0.1.5") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^0.1.21") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "17nlj40drg3b2kwdlrb1hhyr15cy1b5k6m45119b75b3f5wy6iib")))

(define-public crate-pdf-min-0.1.6 (c (n "pdf-min") (v "0.1.6") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^0.1.21") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "170ra6mjj9f76ya7gixakv2a32q1yzyd00bfccp1jblbshmzlzhz")))

(define-public crate-pdf-min-0.1.7 (c (n "pdf-min") (v "0.1.7") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^0.1.21") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "06w2vc2n9hfm88zjsq0rpa3zal0fl8l481lsgrw0ii5m7c4kf35c")))

(define-public crate-pdf-min-0.1.8 (c (n "pdf-min") (v "0.1.8") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^0.1.21") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "15z53fkwmbc0d7g5qvvmwgcy8f2f61ry9kpvx79pk7zc16cab3wx")))

(define-public crate-pdf-min-0.1.9 (c (n "pdf-min") (v "0.1.9") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^1.0.0") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "0alfz7504x4nvai1g1kyn45s9zgqrz6yk1k39z24wswwwrwa3k62")))

(define-public crate-pdf-min-0.1.10 (c (n "pdf-min") (v "0.1.10") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^1.0.0") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "1s4vvwba8xh2xhgg4xm3vjvg3i548bbn6rakwdfd0fs672asgfwb")))

(define-public crate-pdf-min-0.1.11 (c (n "pdf-min") (v "0.1.11") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^1.0.0") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "0cdyqsfmv75qp28ivx87jhdcjvb25xjh0fv8lb2g65y7qx8v5sa0")))

(define-public crate-pdf-min-0.1.12 (c (n "pdf-min") (v "0.1.12") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "flate3") (r "^1.0.0") (d #t) (k 0)) (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)))) (h "1jhicdng26d14q6xi99gkdn6a1spdm198rqjmjr02k3np08sihxy")))

