(define-module (crates-io pd f- pdf-create) #:use-module (crates-io))

(define-public crate-pdf-create-0.1.0 (c (n "pdf-create") (v "0.1.0") (d (list (d (n "ccitt-t4-t6") (r ">=0.1.0, <0.2.0") (d #t) (k 2)) (d (n "chrono") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "color-eyre") (r ">=0.5.0, <0.6.0") (k 2)) (d (n "pdf") (r ">=0.7.0, <0.8.0") (d #t) (k 2)))) (h "0ll85ghylh4mgi5h8cqk4q2hfys1jw9nz9d39vlacqrvrnd7mgr6")))

(define-public crate-pdf-create-0.1.1 (c (n "pdf-create") (v "0.1.1") (d (list (d (n "ccitt-t4-t6") (r ">=0.1.0, <0.2.0") (d #t) (k 2)) (d (n "chrono") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "color-eyre") (r ">=0.5.0, <0.6.0") (k 2)) (d (n "pdf") (r ">=0.7.0, <0.8.0") (d #t) (k 2)))) (h "05ck7ksanasx49mm1pivi6cy6hj5940wpjvhk0iw5yf49ilnsry3")))

