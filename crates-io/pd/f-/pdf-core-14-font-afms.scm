(define-module (crates-io pd f- pdf-core-14-font-afms) #:use-module (crates-io))

(define-public crate-pdf-core-14-font-afms-0.1.0 (c (n "pdf-core-14-font-afms") (v "0.1.0") (h "07j5p20nnfrl6z4k9m05nr80pamv4317bhrmmkxp4qvpqa34dwci")))

(define-public crate-pdf-core-14-font-afms-0.1.1 (c (n "pdf-core-14-font-afms") (v "0.1.1") (h "1wz4xnfwijqg5i06by6g2dzwahsafhzk880j6yaxs6d12qwz86qh")))

