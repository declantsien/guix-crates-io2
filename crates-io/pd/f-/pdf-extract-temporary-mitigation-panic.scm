(define-module (crates-io pd f- pdf-extract-temporary-mitigation-panic) #:use-module (crates-io))

(define-public crate-pdf-extract-temporary-mitigation-panic-0.7.1 (c (n "pdf-extract-temporary-mitigation-panic") (v "0.7.1") (d (list (d (n "adobe-cmap-parser") (r "^0.3.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "euclid") (r "^0.20.5") (d #t) (k 0)) (d (n "linked-hash-map") (r "=0.5.3") (d #t) (k 0)) (d (n "lopdf") (r "^0.29") (f (quote ("pom_parser"))) (k 0)) (d (n "postscript") (r "^0.17") (d #t) (k 0)) (d (n "type1-encoding-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)))) (h "1nnk33ynfpqdi0a8jx8xs6n6bh7cnh24acmpd306xi1j1231wkxi")))

