(define-module (crates-io pd f2 pdf2image) #:use-module (crates-io))

(define-public crate-pdf2image-0.1.0 (c (n "pdf2image") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (f (quote ("rayon" "jpeg"))) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "02k421vwa4l4pjracs2gnixrp7fjkxwp8ll4fi8hgipbaxcg0s71")))

(define-public crate-pdf2image-0.1.1 (c (n "pdf2image") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (f (quote ("rayon" "jpeg"))) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "190zh16kq6ra9gqafai3hhadirhpfl1qj3gbm8jgf449pblvj11d")))

(define-public crate-pdf2image-0.1.2 (c (n "pdf2image") (v "0.1.2") (d (list (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (f (quote ("rayon" "jpeg"))) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0m7z9bp8rvnzgf627qhrf1mkan0gx924kwbhpblh07z2p2czrhmr")))

