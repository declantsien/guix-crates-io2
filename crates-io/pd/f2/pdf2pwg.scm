(define-module (crates-io pd f2 pdf2pwg) #:use-module (crates-io))

(define-public crate-pdf2pwg-0.1.0 (c (n "pdf2pwg") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "pdfium-render") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1sgy29r74cczxhlx6xc353y3fnnvyp9xghcamr6bqala4s2njn90")))

(define-public crate-pdf2pwg-0.1.1 (c (n "pdf2pwg") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "pdfium-render") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12nj91hzclk7v8m0d9ds3hfzrq7cy2qczbwiibxrabicdspfc5dg")))

(define-public crate-pdf2pwg-0.2.0 (c (n "pdf2pwg") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "pdfium-render") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0753ajdrprjbpj2y86v8nn3vp457j5vlpa4i5pxvp3k5614bw2yd")))

(define-public crate-pdf2pwg-0.2.1 (c (n "pdf2pwg") (v "0.2.1") (d (list (d (n "blocking") (r "^1.5") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "pdfium-render") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06kqd0ajdq11y17b5ybc0s716dfjfvl3rkzqn9d7nr021106nq11")))

(define-public crate-pdf2pwg-0.2.2 (c (n "pdf2pwg") (v "0.2.2") (d (list (d (n "blocking") (r "^1.5") (d #t) (k 0)) (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "pdfium-render") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g3p0fmkrlw07apgg8ajaz1b348xi9ysj02lzd4bis3gkc3bqn17")))

