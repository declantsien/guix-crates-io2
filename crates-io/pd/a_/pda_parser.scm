(define-module (crates-io pd a_ pda_parser) #:use-module (crates-io))

(define-public crate-pda_parser-0.1.0 (c (n "pda_parser") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "fi-night") (r "^0.1.6") (f (quote ("fsm_gen_code"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "06993fdv9f4zsbxq2nmirzxhdhg0zxyr3vrh0vgmi36fs0yx0qh8") (y #t)))

