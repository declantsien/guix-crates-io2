(define-module (crates-io pd at pdatastructs) #:use-module (crates-io))

(define-public crate-pdatastructs-0.1.0 (c (n "pdatastructs") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)))) (h "1d1aabwz53q6kdxclgz1pcpz8i8s1a46xq03xkfprw8g0b3cpq4z")))

(define-public crate-pdatastructs-0.2.0 (c (n "pdatastructs") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)))) (h "1ahka7fy0vl6dj0fz7zmljxx4qvw90s8w7kmaljwsla52az2j0wc")))

(define-public crate-pdatastructs-0.3.0 (c (n "pdatastructs") (v "0.3.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1vjwrmjar49pzbw6hv6zvc9h0kw01q7zh26sm8al8vkwpb2ab3mc")))

(define-public crate-pdatastructs-0.4.0 (c (n "pdatastructs") (v "0.4.0") (d (list (d (n "bytecount") (r "^0.3") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0dk3gn6g3j0jw650fc6rzig9a1h193zjq5mx5x8a47nx581zyc37")))

(define-public crate-pdatastructs-0.5.0 (c (n "pdatastructs") (v "0.5.0") (d (list (d (n "bytecount") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1") (d #t) (k 2)) (d (n "succinct") (r "^0.4.4") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "0jw7zinkk3hhlr5pkln8adw7as886r7p9zd82npwscn800d2r30a")))

(define-public crate-pdatastructs-0.6.0 (c (n "pdatastructs") (v "0.6.0") (d (list (d (n "bytecount") (r "^0.5") (f (quote ("runtime-dispatch-simd"))) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.1.9") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1") (d #t) (k 2)) (d (n "succinct") (r "^0.4.4") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "1h7n4p364mw487ggwnfyibbknfqvad1s63wxm7sq5afpb4m9hb7x")))

(define-public crate-pdatastructs-0.7.0 (c (n "pdatastructs") (v "0.7.0") (d (list (d (n "bytecount") (r "^0.6") (f (quote ("runtime-dispatch-simd"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fixedbitset") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "succinct") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1yl7cqp8nysgjdzb83xx00wvg4zxnbzp2iqjj2b6as1w7jab9p2b") (f (quote (("default" "bytecount" "fixedbitset" "num-traits" "rand" "succinct"))))))

