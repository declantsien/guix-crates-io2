(define-module (crates-io pd fs pdfshrink) #:use-module (crates-io))

(define-public crate-pdfshrink-0.1.0 (c (n "pdfshrink") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "05ykv0vmihh957k4x18pcij3kprvs97hmb1pp17dbm5h78qy0phx")))

(define-public crate-pdfshrink-0.1.1 (c (n "pdfshrink") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1gbyfs3zl2cv8lly7fr558nbd1dz6igrs6xnjswd7q4lq3lg11q7")))

(define-public crate-pdfshrink-0.1.2 (c (n "pdfshrink") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0x8x4jxi14ryd7zg5m12il41x41zksi2l9nrxxcrjllrnfapw2gb")))

(define-public crate-pdfshrink-0.1.3 (c (n "pdfshrink") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0syiv1q34xr6r45crvbil8fbycw6amlnwap05a9sqvi43najrgjm") (f (quote (("logging" "log") ("build-binary" "logging" "fern" "shell-escape" "chrono"))))))

(define-public crate-pdfshrink-0.1.4 (c (n "pdfshrink") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1z4w1pbsl90nahv7xs6c21jddzzzsjx3r8r31pq55s6zllrn77fw") (f (quote (("logging" "log") ("build-binary" "logging" "fern" "shell-escape" "chrono"))))))

(define-public crate-pdfshrink-0.1.5 (c (n "pdfshrink") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0w270sdnyrq0a2fjz718dq14l8m0idavjv371ma55rsv96gb4cqh") (f (quote (("logging" "log") ("build-binary" "logging" "fern" "shell-escape" "chrono"))))))

(define-public crate-pdfshrink-0.1.6 (c (n "pdfshrink") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "15h2rkl42gk7sllswxb7zrxps4avsj4qzz88qwqjngncbw3cn8fp") (f (quote (("logging" "log") ("build-binary" "logging" "fern" "shell-escape" "chrono"))))))

(define-public crate-pdfshrink-0.1.7 (c (n "pdfshrink") (v "0.1.7") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1kvplc7sv1fs4ci1zlqa5ivs1ahjd13dw4a5r38vzczzfz9x5dk0") (f (quote (("logging" "log") ("build-binary" "logging" "env_logger" "shell-escape"))))))

(define-public crate-pdfshrink-0.1.8 (c (n "pdfshrink") (v "0.1.8") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("color"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0qpjwrrjf0cnw8hgzmzd6bbzl21pacy3ywjbn7bhkbbpvrfbgvap") (f (quote (("logging" "log") ("build-binary" "logging" "env_logger" "shell-escape"))))))

(define-public crate-pdfshrink-0.2.0 (c (n "pdfshrink") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("color"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0vsnbphyn30raihj4vdkmn47k6r17fpb8291r8nmd70s1wv8x6k7") (f (quote (("logging" "log") ("default" "build-binary") ("build-binary" "logging" "env_logger" "shell-escape"))))))

