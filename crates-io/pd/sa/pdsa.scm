(define-module (crates-io pd sa pdsa) #:use-module (crates-io))

(define-public crate-pdsa-0.1.0 (c (n "pdsa") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "cargo-audit") (r "^0.17") (d #t) (k 2)) (d (n "cargo-tarpaulin") (r "^0.22.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "03gryq9i1z55bk1xa7gyp51k4xdlzgvamjxxdgs9mxii4asf5mv7") (y #t)))

