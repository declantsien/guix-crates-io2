(define-module (crates-io pd fp pdfpdf) #:use-module (crates-io))

(define-public crate-pdfpdf-0.1.0 (c (n "pdfpdf") (v "0.1.0") (d (list (d (n "deflate") (r "^0.7") (d #t) (k 0)))) (h "1kibm827hm3x1mxrry1diz0l4815pghq6xj06nx2a8am210a5b45")))

(define-public crate-pdfpdf-0.2.0 (c (n "pdfpdf") (v "0.2.0") (d (list (d (n "deflate") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1k0kxm8mfs7f71kfndwasxyjlb582lpjn1g5k6jdgipp9sgwajf9")))

