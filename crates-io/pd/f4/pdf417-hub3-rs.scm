(define-module (crates-io pd f4 pdf417-hub3-rs) #:use-module (crates-io))

(define-public crate-pdf417-hub3-rs-0.1.0 (c (n "pdf417-hub3-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32") (d #t) (k 0)) (d (n "rxing") (r "^0.5") (d #t) (k 0)))) (h "0vsmw3lzg347ck0yl730hws8zv5ia7vs61ddyw5f8i0ip07cnwi3") (y #t)))

(define-public crate-pdf417-hub3-rs-0.2.0 (c (n "pdf417-hub3-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32") (d #t) (k 0)) (d (n "rxing") (r "^0.5") (k 0)) (d (n "svg") (r "^0.16.0") (o #t) (d #t) (k 0)) (d (n "svg2pdf") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0fkkbhn1pzspncz9qvbf9z3mckyabp143mf71lkzwrxkqn60is2q") (f (quote (("svg" "rxing/svg_write") ("image" "rxing/image") ("default" "image" "svg" "pdf")))) (s 2) (e (quote (("pdf" "dep:svg2pdf" "dep:svg"))))))

(define-public crate-pdf417-hub3-rs-0.2.1 (c (n "pdf417-hub3-rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32") (d #t) (k 0)) (d (n "rxing") (r "^0.5") (k 0)) (d (n "svg") (r "^0.16.0") (o #t) (d #t) (k 0)) (d (n "svg2pdf") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0jggrd07nvhk2p01a19fp90h8n7vxvgj2bys62lyh2nlfzy60al5") (f (quote (("svg" "rxing/svg_write") ("image" "rxing/image") ("default" "image" "svg" "pdf")))) (y #t) (s 2) (e (quote (("pdf" "dep:svg2pdf" "dep:svg"))))))

(define-public crate-pdf417-hub3-rs-0.2.2 (c (n "pdf417-hub3-rs") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.32") (d #t) (k 0)) (d (n "rxing") (r "^0.5") (k 0)) (d (n "svg") (r "^0.16.0") (o #t) (d #t) (k 0)) (d (n "svg2pdf") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0f5kshvqhz7scmn9hg8wyz9xk885bs8cjw0zmfb224pxqwm6k1zk") (f (quote (("image" "rxing/image") ("default" "image" "svg" "pdf")))) (s 2) (e (quote (("svg" "rxing/svg_write" "dep:svg") ("pdf" "dep:svg2pdf"))))))

