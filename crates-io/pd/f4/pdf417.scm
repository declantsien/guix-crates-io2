(define-module (crates-io pd f4 pdf417) #:use-module (crates-io))

(define-public crate-pdf417-0.1.0 (c (n "pdf417") (v "0.1.0") (d (list (d (n "awint_core") (r "^0.11.0") (k 0)) (d (n "awint_macros") (r "^0.11.0") (k 0)))) (h "0pp0bw9539qqj8pwi3gdm3dn635p0zh16wh9xyjvz7pq8hp4izb8")))

(define-public crate-pdf417-0.1.1 (c (n "pdf417") (v "0.1.1") (d (list (d (n "awint_core") (r "^0.11.0") (k 0)) (d (n "awint_macros") (r "^0.11.0") (k 0)))) (h "0krls5kll5f9qf7snb6fvpdknnk7d68ymvmqs17wm9pb17j6n7l5")))

(define-public crate-pdf417-0.2.0 (c (n "pdf417") (v "0.2.0") (d (list (d (n "awint_core") (r "^0.11.0") (k 0)))) (h "1qhxdga4c48q260r65b4r00vf2w8xkl7yxclycyzr0h1x5jag842")))

(define-public crate-pdf417-0.2.1 (c (n "pdf417") (v "0.2.1") (d (list (d (n "awint_core") (r "^0.11.0") (k 0)))) (h "1hz7pxpbbwyc47j95bfw2pdg91ch2jkd8qzfrnn9h8m1j8jsnmh8")))

