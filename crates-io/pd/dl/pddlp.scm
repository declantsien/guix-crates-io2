(define-module (crates-io pd dl pddlp) #:use-module (crates-io))

(define-public crate-pddlp-0.1.0 (c (n "pddlp") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1bxcp7sa0kl0cl73x310f438nylkdrra3y3ikdmgqnv1i4sfrka0")))

(define-public crate-pddlp-0.1.1 (c (n "pddlp") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1f1s8i7mn0nbswz856171zv4zwi7wh7xngd3f9rzgr8qds0352dq")))

(define-public crate-pddlp-0.1.2 (c (n "pddlp") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "14j660z976d2a7ri97lgpzg49k0qg12xrri7984lsrhd6rw5dg98")))

(define-public crate-pddlp-0.1.3 (c (n "pddlp") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "04f1ifmal5v5xk5s8icv5xw7k1zynzj5x4wky6ldsyha16jbzmam")))

(define-public crate-pddlp-0.1.4 (c (n "pddlp") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0jx3qdwhymlshgi2jwcck8jldgz6j7gp7d0v4r923fzp78zkn8mw")))

(define-public crate-pddlp-0.1.5 (c (n "pddlp") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "logos") (r "^0.14.0") (f (quote ("export_derive"))) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1xizv8z08mnbs6crw6r4whyicxb7xqk6n4hdasqvhqwhprdyqkrk")))

(define-public crate-pddlp-0.1.6 (c (n "pddlp") (v "0.1.6") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "logos") (r "^0.14.0") (f (quote ("export_derive"))) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)))) (h "165c97ps3d76ww2w5s6gv58idjsp4scj6ar6mlz58d7hfi3qrkij")))

