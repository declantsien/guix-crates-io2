(define-module (crates-io pd dl pddl) #:use-module (crates-io))

(define-public crate-pddl-0.0.0 (c (n "pddl") (v "0.0.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0s0p8pawsqsw7xb5m6rlhfjf7mnqaini11k1pn6xzmqpys97qaqh") (r "1.69.0")))

(define-public crate-pddl-0.0.1 (c (n "pddl") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1zs912231kfsh72mvn8614m0syfhbs8zrjhgmcr6ah71v7k27k4z") (r "1.68.0")))

(define-public crate-pddl-0.0.2 (c (n "pddl") (v "0.0.2") (d (list (d (n "nom") (r "^7.1.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0iakq4qmwiibmzbwhn58nvh40b048nijgs3svwfxfi5lldlma1wc") (f (quote (("parser" "nom") ("default" "parser")))) (r "1.68.0")))

(define-public crate-pddl-0.0.3 (c (n "pddl") (v "0.0.3") (d (list (d (n "nom") (r "^7.1.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1wl6zvgqq5z36a6j31jk2x58cibsx6vvmh805lrhm6zj1k2nfgk6") (f (quote (("default" "parser")))) (s 2) (e (quote (("parser" "dep:nom")))) (r "1.68.0")))

(define-public crate-pddl-0.0.4 (c (n "pddl") (v "0.0.4") (d (list (d (n "nom") (r "^7.1.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1kr2vmccplshi1ckzwpg876q1lzr7hswykdl89llvnr12qkb2xjy") (f (quote (("default" "parser")))) (s 2) (e (quote (("parser" "dep:nom")))) (r "1.68.0")))

(define-public crate-pddl-0.0.5 (c (n "pddl") (v "0.0.5") (d (list (d (n "nom") (r "^7.1.3") (o #t) (d #t) (k 0)) (d (n "nom-greedyerror") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "nom_locate") (r "^4.1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1a6fbbv3w0a2xqffzl7x72vgg1qvas52sygn89inwv097chrd8nh") (f (quote (("default" "parser")))) (s 2) (e (quote (("parser" "dep:nom" "dep:nom-greedyerror" "dep:nom_locate")))) (r "1.68.0")))

(define-public crate-pddl-0.0.6 (c (n "pddl") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (o #t) (d #t) (k 0)) (d (n "nom-greedyerror") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "nom_locate") (r "^4.1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (o #t) (d #t) (k 0)))) (h "148higcbvxhrvpvrq1fnh9r6n002xvaj0brh2pwmzaav0qwzqg4d") (f (quote (("default" "parser" "interning")))) (s 2) (e (quote (("parser" "dep:nom" "dep:nom-greedyerror" "dep:nom_locate" "dep:thiserror") ("interning" "dep:lazy_static")))) (r "1.68.0")))

(define-public crate-pddl-0.0.7 (c (n "pddl") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (o #t) (d #t) (k 0)) (d (n "nom-greedyerror") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "nom_locate") (r "^4.1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (o #t) (d #t) (k 0)))) (h "0yp5zyx81gzrp2i6qwrznv5ry4d6xgwdmzqc92wymjlpgjqr1nab") (f (quote (("default" "parser" "interning")))) (s 2) (e (quote (("parser" "dep:nom" "dep:nom-greedyerror" "dep:nom_locate" "dep:thiserror") ("interning" "dep:lazy_static")))) (r "1.68.0")))

(define-public crate-pddl-0.1.0 (c (n "pddl") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (o #t) (d #t) (k 0)) (d (n "nom-greedyerror") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (o #t) (d #t) (k 0)))) (h "1r3v14gjhivgv57zyacmm8ahjw58i0wjsbdikd8jb275svvcfx7v") (f (quote (("default" "parser" "interning")))) (s 2) (e (quote (("parser" "dep:nom" "dep:nom-greedyerror" "dep:nom_locate" "dep:thiserror") ("interning" "dep:lazy_static")))) (r "1.68.0")))

