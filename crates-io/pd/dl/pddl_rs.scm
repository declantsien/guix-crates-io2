(define-module (crates-io pd dl pddl_rs) #:use-module (crates-io))

(define-public crate-pddl_rs-0.1.0 (c (n "pddl_rs") (v "0.1.0") (d (list (d (n "enumset") (r "^1.0.12") (d #t) (k 0)))) (h "0xi3hvxfj89sa5jirsy3059yi57c9n955j23nhsyrdi3kc47p528") (y #t)))

(define-public crate-pddl_rs-0.1.1 (c (n "pddl_rs") (v "0.1.1") (d (list (d (n "enumset") (r "^1.0.12") (d #t) (k 0)))) (h "1hahdqhcj18fb2h3xnrgywprgahn3rb6cxzpxrrn7xpkbgkh3bvh") (y #t)))

(define-public crate-pddl_rs-0.1.2 (c (n "pddl_rs") (v "0.1.2") (d (list (d (n "enumset") (r "^1.0.12") (d #t) (k 0)))) (h "0xsxcs3lai2zjqv8wk11qwa0rcar18ml6xajxcl3h2ln5qhfjny9")))

(define-public crate-pddl_rs-0.1.3 (c (n "pddl_rs") (v "0.1.3") (d (list (d (n "enumset") (r "^1.0.12") (d #t) (k 0)))) (h "1cpjk26vhr182sxdpv1a31pq0l05qyqb1mhvz5fvzqic275m03lm")))

(define-public crate-pddl_rs-0.2.0 (c (n "pddl_rs") (v "0.2.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "enumset") (r "^1.0.12") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0ymls1s4kgl80jq5av3cdgbzd0kz1q3b3b2lgz8m7c747gy5jba1")))

(define-public crate-pddl_rs-0.2.1 (c (n "pddl_rs") (v "0.2.1") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "enumset") (r "^1.0.12") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1vl7hv964zh8r4irs45cginb2i6ysn75akqwxr8prhpiq4kf6jig")))

(define-public crate-pddl_rs-0.3.0 (c (n "pddl_rs") (v "0.3.0") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "enumset") (r "^1.0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00273db57zpivjmwwrhw5nlmaggay2dzlzclpi8kz2dqkxakl6pi")))

(define-public crate-pddl_rs-0.3.1 (c (n "pddl_rs") (v "0.3.1") (d (list (d (n "ariadne") (r "^0.3.0") (d #t) (k 0)) (d (n "enumset") (r "^1.0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0ryiw8449pl6rnsy2famiya71fbgwp6h90wh2kmnprsdm51sdfg5")))

