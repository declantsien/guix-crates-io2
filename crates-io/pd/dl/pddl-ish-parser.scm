(define-module (crates-io pd dl pddl-ish-parser) #:use-module (crates-io))

(define-public crate-pddl-ish-parser-0.0.3 (c (n "pddl-ish-parser") (v "0.0.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17l673g398ff98ii2iyam98qaly7bqgj3giczs1fdz7hgs4fs3s6")))

(define-public crate-pddl-ish-parser-0.0.4 (c (n "pddl-ish-parser") (v "0.0.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0h4c32bp07mxfwb6x1sd4lw5splwlffk6h5paj5f87l0nrsl56x1")))

