(define-module (crates-io pd fu pdfutil) #:use-module (crates-io))

(define-public crate-pdfutil-0.1.0 (c (n "pdfutil") (v "0.1.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)) (d (n "lopdf") (r "^0.8.0") (d #t) (k 0)))) (h "0msi75ppdwgzkv6wbh1wzgl6ap4ja28axzgq3kjmxyrq3iilrk54")))

(define-public crate-pdfutil-0.2.0 (c (n "pdfutil") (v "0.2.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)) (d (n "lopdf") (r "^0.13.0") (d #t) (k 0)))) (h "1zqc7cv5lq4wfcpbkcpvi9mvgkncda046xq12z1nzzpa5v5bnh22")))

(define-public crate-pdfutil-0.4.0 (c (n "pdfutil") (v "0.4.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "lopdf") (r "^0.26.0") (d #t) (k 0)))) (h "1n4b3nq8dh2kqb7n1vr7hlxfadqhzd7p3sjyzb01wndsw84z3d4d")))

