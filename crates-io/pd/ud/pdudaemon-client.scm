(define-module (crates-io pd ud pdudaemon-client) #:use-module (crates-io))

(define-public crate-pdudaemon-client-0.1.0 (c (n "pdudaemon-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 2)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1zf1fx8qdwvpcqk4djfmqzhhaay1krsmhz4mi3z2wn4iwhc0js7z")))

