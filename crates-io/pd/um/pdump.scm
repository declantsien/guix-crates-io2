(define-module (crates-io pd um pdump) #:use-module (crates-io))

(define-public crate-pdump-0.2.0 (c (n "pdump") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "default-net") (r "^0.15.0") (d #t) (k 0)) (d (n "pnet") (r "^0.33.0") (d #t) (k 0)))) (h "0jh9ywgwk6r5bgy3hy7wlsyv8r27061fjk61xbd1iiwhgb972hlw")))

(define-public crate-pdump-0.2.1 (c (n "pdump") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "default-net") (r "^0.16.1") (d #t) (k 0)) (d (n "pnet") (r "^0.33.0") (d #t) (k 0)))) (h "1fhw0p3qzni1p9cjij10a9vd5iracdqxmy12rpav0h1dnxzn7v2r")))

(define-public crate-pdump-0.2.2 (c (n "pdump") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "default-net") (r "^0.16") (d #t) (k 0)) (d (n "pnet") (r "^0.33.0") (d #t) (k 0)))) (h "0spc1r9dalx23yqr5sqxl7vhgwnxv17ky29f1zg4653jgwyqkj0b")))

