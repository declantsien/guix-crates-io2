(define-module (crates-io pd fi pdfio-sys) #:use-module (crates-io))

(define-public crate-pdfio-sys-0.1.0 (c (n "pdfio-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1cjmq7kbkcr0s0dyqajyfj9874pj2ginf8y5hzpz8jdnqldlya1k")))

(define-public crate-pdfio-sys-0.2.0 (c (n "pdfio-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1gvqymrvipv0mf9rd3adqxm21k5qqxrgab59cr3cx8fkyvvhqxdp")))

