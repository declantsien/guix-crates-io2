(define-module (crates-io pd bt pdbtbx) #:use-module (crates-io))

(define-public crate-pdbtbx-0.1.0 (c (n "pdbtbx") (v "0.1.0") (h "1dyxfxgiqnaz013xz70c55mfbjkgvshhzzn2q15ddh0xsssb6lqw")))

(define-public crate-pdbtbx-0.1.1 (c (n "pdbtbx") (v "0.1.1") (h "1j37akl3d3cik0dpaz6bwz8bgwx1mi4drr6qz23cln669d8342rk")))

(define-public crate-pdbtbx-0.1.2 (c (n "pdbtbx") (v "0.1.2") (h "1ks1r5ng91may2v48ww7h0qwhky96ilw8zqs3wg0sm1szcys4jpi")))

(define-public crate-pdbtbx-0.1.3 (c (n "pdbtbx") (v "0.1.3") (h "0v8x0s44xzrd29rzc05m2pn3rk6fjqkv94prqlz78c9pn9fp1xbb") (y #t)))

(define-public crate-pdbtbx-0.1.4 (c (n "pdbtbx") (v "0.1.4") (h "19ryg29n2jrgi9a9c5sja3f48mkpwjp84vnrzbphmc4yv6y1zfsz") (y #t)))

(define-public crate-pdbtbx-0.1.5 (c (n "pdbtbx") (v "0.1.5") (h "0h06hfrh8nfybp6c92kv331c993y0ml6dwf00nlsmydf3d5l2p2j")))

(define-public crate-pdbtbx-0.2.0 (c (n "pdbtbx") (v "0.2.0") (h "1mv6dymyhak1plh4yj85dc4961l3dm5x6ji4ij7wzssja3qswp15")))

(define-public crate-pdbtbx-0.2.1 (c (n "pdbtbx") (v "0.2.1") (h "07ys214z575ais543xshmwrpcv1py75rfza8nksfs3jyndbplliw")))

(define-public crate-pdbtbx-0.3.0 (c (n "pdbtbx") (v "0.3.0") (h "0q7y12c8r993fsznfrxkgfdidjw1hqqpaz52ndycgvq637vh5291")))

(define-public crate-pdbtbx-0.3.1 (c (n "pdbtbx") (v "0.3.1") (h "1dk860y2s1g4w2fxkzr0s6gf3lzinvwp7v8hl1p69sav6mc9am3f")))

(define-public crate-pdbtbx-0.3.2 (c (n "pdbtbx") (v "0.3.2") (h "1iql8vcy6x9608h7mamviyx306qnd5dvqhwm2rxqqcs0z7ppg3yx")))

(define-public crate-pdbtbx-0.3.3 (c (n "pdbtbx") (v "0.3.3") (h "1kplbpl4fn89nhl10lc9j12dbsbwdw8y5blrzkzk70nrnmk70h46")))

(define-public crate-pdbtbx-0.4.0 (c (n "pdbtbx") (v "0.4.0") (h "0md5c09cps0b943gcdhgpalmcaqaj6rgc7jsrqaizgn8qm3zq7hy")))

(define-public crate-pdbtbx-0.4.1 (c (n "pdbtbx") (v "0.4.1") (h "1p6a3rbx9n74s63zvzzzph25jpyf8sax7x2nrx4m3c0pwbacni7f")))

(define-public crate-pdbtbx-0.5.0 (c (n "pdbtbx") (v "0.5.0") (h "158h4v5jrsc8khgxs8xhnil5qsbwqa8k141p9cq6rp0bjfw802v0")))

(define-public crate-pdbtbx-0.5.1 (c (n "pdbtbx") (v "0.5.1") (h "1ihwjyj0kq98qs5bvvkkcfhwm0y514bjxngk5fplxhqgi68wflfh")))

(define-public crate-pdbtbx-0.6.0 (c (n "pdbtbx") (v "0.6.0") (h "1ijlf9hxkbrdspqc1x9qvbj0s675cag71166llwqj7ybg3ywahqk")))

(define-public crate-pdbtbx-0.6.1 (c (n "pdbtbx") (v "0.6.1") (h "1a4dncyc4h830gvy5nar6a92wx119q38fimf9i3hjkcy5wm879kr")))

(define-public crate-pdbtbx-0.6.2 (c (n "pdbtbx") (v "0.6.2") (h "0ih4hb2li6c742np59zd2a73qkhxjb4xngiwghcj9645znz4a40w")))

(define-public crate-pdbtbx-0.6.3 (c (n "pdbtbx") (v "0.6.3") (h "11aqjj7gvwc66zihdlwx31qq5abgv3q91np0sn9jbb37j8w5a4f6")))

(define-public crate-pdbtbx-0.7.0 (c (n "pdbtbx") (v "0.7.0") (d (list (d (n "doc-cfg") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rstar") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0r52fvsm18s34ws7p6iljvjmq52mnw2i6dlcq2xabvlqrgmdq5d6") (f (quote (("unstable-doc-cfg") ("default" "rayon"))))))

(define-public crate-pdbtbx-0.8.0 (c (n "pdbtbx") (v "0.8.0") (d (list (d (n "doc-cfg") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bx2ci7mxpr0wzp9yxskv75ilm9r0d9dvrc8w9bffml7h50r9fl1") (f (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.9.0 (c (n "pdbtbx") (v "0.9.0") (d (list (d (n "doc-cfg") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "00dpxxafmkzkh65hgdvpzdnzvpird8mmkdm9w1xdijm1bax8rfsv") (f (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.9.1 (c (n "pdbtbx") (v "0.9.1") (d (list (d (n "doc-cfg") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "0dz7906plf0z1ssjwmq4ssiqzbj2d7bnh69wc88a49a552cbywas") (f (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.9.2 (c (n "pdbtbx") (v "0.9.2") (d (list (d (n "doc-cfg") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "19vjl42wnlxxd3zkan2iacjscglsnpld8yycz025acpryvr2kn5a") (f (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.10.0 (c (n "pdbtbx") (v "0.10.0") (d (list (d (n "doc-cfg") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "1v1psds2rni011wl4p9gbmzbv1fhgs9c6ghs8fmfcqvazrmfz0gz") (f (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.10.1 (c (n "pdbtbx") (v "0.10.1") (d (list (d (n "doc-cfg") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstar") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "0jm38lw87iyh6y1h5j8rx59wwvsb5169rcdp1jcyaybfqmx5nf8r") (f (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.10.2 (c (n "pdbtbx") (v "0.10.2") (d (list (d (n "doc-cfg") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstar") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "130z9k4xx0rg06bj059qbf3xrd3yarjcnfkclh8vrqabg25040bl") (f (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.11.0 (c (n "pdbtbx") (v "0.11.0") (d (list (d (n "doc-cfg") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "rstar") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 2)))) (h "0jzb9c7qwph1sla49md7mz5m9l5gwa7zs7izdqm10bjss89dmbnq") (f (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde" "compression") ("compression" "flate2")))) (r "1.56")))

