(define-module (crates-io pd b- pdb-fetch) #:use-module (crates-io))

(define-public crate-pdb-fetch-0.1.0 (c (n "pdb-fetch") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-channel") (r "^1.7.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fnsahc8w7vx0qwp1cl359pygjzzmfifcw18yn9jx8nv60y1qqhk")))

(define-public crate-pdb-fetch-0.1.1 (c (n "pdb-fetch") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-channel") (r "^1.7.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h1n1pwg2fmxz2ysrsghnnpgnv4xdqv2s1ybdjss6djwiagbalh2")))

