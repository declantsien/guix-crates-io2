(define-module (crates-io pd b_ pdb_wrapper) #:use-module (crates-io))

(define-public crate-pdb_wrapper-0.1.0 (c (n "pdb_wrapper") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pdb") (r "^0.6") (d #t) (k 2)) (d (n "snafu") (r "^0.6.9") (d #t) (k 0)))) (h "09gmx2rxi3pksysz3vr789j1xqafkl7vilm17v6lxs4nhln26ksx")))

(define-public crate-pdb_wrapper-0.2.0 (c (n "pdb_wrapper") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "pdb") (r "^0.6") (d #t) (k 2)) (d (n "snafu") (r "^0.6.9") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 1)))) (h "034g35lvwxr0vczd8n4l91j1q7yjww7nwv48frjqy3qk5hl5qyh7") (f (quote (("llvm_13") ("llvm_10") ("default" "llvm_13"))))))

(define-public crate-pdb_wrapper-0.2.2 (c (n "pdb_wrapper") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "pdb") (r "^0.6") (d #t) (k 2)) (d (n "snafu") (r "^0.6.9") (d #t) (k 0)) (d (n "which") (r "^4.1.0") (d #t) (k 1)))) (h "1fnmhx87mdzg3p4hf9zk3yydifh4dms45dgna15gncdhj7kpmf27") (f (quote (("llvm_13") ("llvm_10") ("default" "llvm_13"))))))

