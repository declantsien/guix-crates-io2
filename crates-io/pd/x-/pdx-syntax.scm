(define-module (crates-io pd x- pdx-syntax) #:use-module (crates-io))

(define-public crate-pdx-syntax-0.1.0 (c (n "pdx-syntax") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strip_bom") (r "^1.0") (d #t) (k 0)))) (h "1h2gq9899an70rq44ags0skhz54pqrmrkm5jzpfs4w9whhj29g93")))

(define-public crate-pdx-syntax-0.2.0 (c (n "pdx-syntax") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strip_bom") (r "^1.0") (d #t) (k 0)))) (h "19sngzanadsz6gizpad2jyy42y5x9grnf17a8qhrl8r2n7xsflvq")))

