(define-module (crates-io pd qh pdqhash) #:use-module (crates-io))

(define-public crate-pdqhash-0.1.0 (c (n "pdqhash") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1rm0sbnjycn8qpdn9aygbihn9ik8qk0x1g2mpfcc5dsijvs2nh9n")))

(define-public crate-pdqhash-0.1.1 (c (n "pdqhash") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1vjrcwkyflqn5m465wdj78sx3ls8rnv8fhv42bsfqi75hmaxcx5x")))

