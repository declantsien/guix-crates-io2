(define-module (crates-io yj um yjump) #:use-module (crates-io))

(define-public crate-yjump-0.1.0 (c (n "yjump") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "1y7z4qnj87v7jrpg94dhdj0djqs7l78jf2irsfm4hy754zdcw4zn")))

(define-public crate-yjump-0.1.1 (c (n "yjump") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "1r1ri383jll1z1z2ia9zpsfdfz3vr5b41pw949izvsh2dvfvn901")))

(define-public crate-yjump-0.1.2 (c (n "yjump") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "11vrx1ifylw2lyxn0hicdwkbcr2fpxzqgnipqc0mmxjcs2dd5ayh")))

(define-public crate-yjump-0.1.3 (c (n "yjump") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "0g76slg35igg80hp912sm1g17s04nlvjms4xgsyb2n7b2b9bvbp9")))

(define-public crate-yjump-0.1.4 (c (n "yjump") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "16rk9fybbp1w13lmswi2jw87vpybhr71d0f7nw0k42yjalaw5g1p")))

(define-public crate-yjump-0.1.5 (c (n "yjump") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "0nrnbxja8z6vxkxfnydygpjqfbl1v8gw47vw8fk66dpzy2qilyg3")))

(define-public crate-yjump-0.1.6 (c (n "yjump") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)))) (h "0fqsdmchz7qb80v3nwvwgychpk0g9dbfi3n5fkwpv7axnrmp393w")))

