(define-module (crates-io rt ai rtaichi_attr_impl) #:use-module (crates-io))

(define-public crate-rtaichi_attr_impl-0.0.1+1.3.0 (c (n "rtaichi_attr_impl") (v "0.0.1+1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "taichi-runtime") (r "^0.0.5") (d #t) (k 0)))) (h "0p8f9607fk5cnm5agm516jgyigwvk7ipsf26p8dqcxaf5sk3njf1")))

(define-public crate-rtaichi_attr_impl-0.0.2+1.3.0 (c (n "rtaichi_attr_impl") (v "0.0.2+1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "taichi-runtime") (r "^0.0.7") (d #t) (k 0)))) (h "0551h62ry5fch10q4hbvldrn398q88qgd5h9msk9z1kp0h1cfg6f")))

