(define-module (crates-io rt ai rtaichi) #:use-module (crates-io))

(define-public crate-rtaichi-0.0.3+1.3.0 (c (n "rtaichi") (v "0.0.3+1.3.0") (d (list (d (n "taichi-runtime") (r "^0") (d #t) (k 0)))) (h "1hzln0aycf32k37m55g43hs5wdzn6sy0rdhgxmilvh6yiqqxzk87")))

(define-public crate-rtaichi-0.0.4+1.3.0 (c (n "rtaichi") (v "0.0.4+1.3.0") (d (list (d (n "ref_thread_local") (r "^0.1.1") (d #t) (k 0)) (d (n "rtaichi_attr") (r "^0.0.1") (d #t) (k 0)) (d (n "taichi-runtime") (r "^0.0.5") (d #t) (k 0)))) (h "0mm91kvsj2fwxxcp8fx6adjq2j91976072fi0amm184ll7584faf")))

(define-public crate-rtaichi-0.0.5+1.3.0 (c (n "rtaichi") (v "0.0.5+1.3.0") (d (list (d (n "ref_thread_local") (r "^0.1.1") (d #t) (k 0)) (d (n "rtaichi_attr") (r "^0.0.2") (d #t) (k 0)) (d (n "taichi-runtime") (r "^0.0.7") (d #t) (k 0)))) (h "10v0cvlh0v08gnvmbp0z271haap1a0jca4ca9cvj1nwga731xirh")))

