(define-module (crates-io rt ai rtaichi_attr) #:use-module (crates-io))

(define-public crate-rtaichi_attr-0.0.1+1.3.0 (c (n "rtaichi_attr") (v "0.0.1+1.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rtaichi_attr_impl") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "taichi-runtime") (r "^0.0.5") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "12kbzwl1im6488xzz9ay2arr5a96wyyb0nyv8cxr4r7qsn5ap490")))

(define-public crate-rtaichi_attr-0.0.2+1.3.0 (c (n "rtaichi_attr") (v "0.0.2+1.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rtaichi_attr_impl") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "taichi-runtime") (r "^0.0.7") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0449lyww8fspfr1dql5ypm4lcvgz7lr361mf1cf4mi6bab03ksbs")))

