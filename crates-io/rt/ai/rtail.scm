(define-module (crates-io rt ai rtail) #:use-module (crates-io))

(define-public crate-rtail-0.1.0 (c (n "rtail") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "curl") (r "^0.4.23") (d #t) (k 0)))) (h "0av6jsgvyyj6aapgsy6pb8rcbch503lnm0jipqzi6lgvbnm56d6w")))

(define-public crate-rtail-0.1.1 (c (n "rtail") (v "0.1.1") (d (list (d (n "clap") (r "~2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "curl") (r "^0.4.23") (d #t) (k 0)))) (h "1vab8rrg7a6qnv05620djscwgizpklnlxcc6jsragnq56hdivl6y")))

(define-public crate-rtail-0.1.3 (c (n "rtail") (v "0.1.3") (d (list (d (n "clap") (r "~2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "curl") (r "^0.4.23") (d #t) (k 0)))) (h "1kmdfskzi6qhgfjq2z90gf4fghf6v2hi9fkr7jy0dqz6qxkyciwb")))

(define-public crate-rtail-0.1.6 (c (n "rtail") (v "0.1.6") (d (list (d (n "clap") (r "~2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "curl") (r "^0.4.23") (d #t) (k 0)))) (h "0s2cf8mwz1b6g1g52nq5adnkkxn30zaxlpjhb086fjdv3sk6bpyr")))

