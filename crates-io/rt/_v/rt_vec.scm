(define-module (crates-io rt _v rt_vec) #:use-module (crates-io))

(define-public crate-rt_vec-0.1.0 (c (n "rt_vec") (v "0.1.0") (d (list (d (n "rt_ref") (r "^0.1.0") (d #t) (k 0)))) (h "0098jlmcgxppyyaz3lp161v7sf0x40hpqdrkzmrh9rcxfm73i9b4")))

(define-public crate-rt_vec-0.1.1 (c (n "rt_vec") (v "0.1.1") (d (list (d (n "rt_ref") (r "^0.2.0") (d #t) (k 0)))) (h "00jbbkiv15n9h4b40198b44q3qbjwd360npm3ngwmxc17cj7iicc") (f (quote (("unsafe_debug" "rt_ref/unsafe_debug"))))))

