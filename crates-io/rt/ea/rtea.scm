(define-module (crates-io rt ea rtea) #:use-module (crates-io))

(define-public crate-rtea-0.1.0 (c (n "rtea") (v "0.1.0") (d (list (d (n "rtea-proc") (r "^1.0.0") (d #t) (k 0)))) (h "13ycnv130dpf9fcd1yqphqbfplmcrbagwxmj49sqac4c0kpj3qr8")))

(define-public crate-rtea-0.1.1 (c (n "rtea") (v "0.1.1") (d (list (d (n "rtea-proc") (r "^1.0.0") (d #t) (k 0)))) (h "0ww5j19c5p9qb73c7f8v8824p2pghnh9lj7mnik183bsfxv70v7g")))

(define-public crate-rtea-0.2.0 (c (n "rtea") (v "0.2.0") (d (list (d (n "rtea-proc") (r "^2.0.0") (d #t) (k 0)))) (h "0if4bf1iy6901gzdg8cd0cdh2953xrp0c27gn9azdjw3j041pa3i")))

