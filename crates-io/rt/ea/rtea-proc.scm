(define-module (crates-io rt ea rtea-proc) #:use-module (crates-io))

(define-public crate-rtea-proc-1.0.0 (c (n "rtea-proc") (v "1.0.0") (h "09p4qfbrqfdraz03l29gzc7xgq2wzm8m2jky12jsl8grci664hin")))

(define-public crate-rtea-proc-1.0.1 (c (n "rtea-proc") (v "1.0.1") (h "0ghs4vsq1r587xs1zdjkkj2c2j15811nxzpk8a5275npdbpfvnmv")))

(define-public crate-rtea-proc-2.0.0 (c (n "rtea-proc") (v "2.0.0") (h "1bnmbm8gsi5jsgrj0jncpcg5lf3cy14jigk5lmrn1c8bf3nvm5lb")))

