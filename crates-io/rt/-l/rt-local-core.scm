(define-module (crates-io rt -l rt-local-core) #:use-module (crates-io))

(define-public crate-rt-local-core-0.1.0 (c (n "rt-local-core") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3.21") (d #t) (k 0)) (d (n "slabmap") (r "^0.1.1") (d #t) (k 0)))) (h "097qsns3bjxm15z5v5ir203a1n416lbz61fa9g6bn9qw8fr59lc8")))

(define-public crate-rt-local-core-0.1.3 (c (n "rt-local-core") (v "0.1.3") (d (list (d (n "futures-core") (r "^0.3.28") (d #t) (k 0)) (d (n "slabmap") (r "^0.2.0") (d #t) (k 0)))) (h "0b1rsshavm5sfrbrhlxhl9kcib980bwivzmplwhfvsh2qlga1mpj")))

