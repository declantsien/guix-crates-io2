(define-module (crates-io rt od rtodo) #:use-module (crates-io))

(define-public crate-rtodo-0.1.0 (c (n "rtodo") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "1rvxxk2xxm77gl46p8gcz48pwc09jpqi0783sc0mybpj3cnykkw4") (y #t)))

(define-public crate-rtodo-0.1.1 (c (n "rtodo") (v "0.1.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "1d8mn5xm76qrb7dypv6rs3hz8lgcyif049z3248q536fpyawz26q") (y #t)))

(define-public crate-rtodo-0.1.2 (c (n "rtodo") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r3pl01rd7r7sq3kllq6yjb95ajw89v5y1xfx9vpsxqgvrm4vif9") (y #t)))

(define-public crate-rtodo-0.1.3 (c (n "rtodo") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m7x8qij4cqqq57dcrrn86hl5b3d067xdl7nz5006zwr4mzpxj54") (y #t)))

(define-public crate-rtodo-0.1.4 (c (n "rtodo") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0dhi00b3zm9qjcc9ax3pyks2vj3vhj5b1gz0id2ypp55rr2p2ry5")))

