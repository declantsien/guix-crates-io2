(define-module (crates-io rt ee rtee) #:use-module (crates-io))

(define-public crate-rtee-0.1.0 (c (n "rtee") (v "0.1.0") (d (list (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "0f8qlwih2yihg3g0p9559njx8x6vd013c5zikj8k8l16h41za7lg")))

(define-public crate-rtee-0.2.0 (c (n "rtee") (v "0.2.0") (d (list (d (n "clap") (r "~2.31") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "0s3a0lyrllr4xq8jcaddmsfjxddlfkzz3y17m8q01kqfafxk6cl8")))

(define-public crate-rtee-0.2.2 (c (n "rtee") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "01sm14ihddb1wdbviw53f20kybg0n42nvlz12yl99r1p6fhdz2af")))

(define-public crate-rtee-0.2.3 (c (n "rtee") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "nix") (r "^0.20.2") (d #t) (k 0)))) (h "0f22v9iyni8kdlknk8120a7kp7l2lkj0r9mal0hvc4sqwkmxcr41")))

