(define-module (crates-io rt c- rtc-turn) #:use-module (crates-io))

(define-public crate-rtc-turn-0.0.0 (c (n "rtc-turn") (v "0.0.0") (h "18x0kjnlgjh9h8ybsxnrmby02k7wlhi0130z4yi3va7q3fdidp5v")))

(define-public crate-rtc-turn-0.1.0 (c (n "rtc-turn") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 2)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "ctrlc") (r "^3.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "md-5") (r "^0.10.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)) (d (n "shared") (r "^0.2.0") (k 0) (p "rtc-shared")) (d (n "stun") (r "^0.2.0") (d #t) (k 0) (p "rtc-stun")) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1fdf2zwszp7irfirh33wb4s4sgmw6dm8y7v6i0vzs4ggrv9vybiz") (f (quote (("metrics"))))))

