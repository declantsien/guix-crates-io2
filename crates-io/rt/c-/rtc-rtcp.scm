(define-module (crates-io rt c- rtc-rtcp) #:use-module (crates-io))

(define-public crate-rtc-rtcp-0.1.0 (c (n "rtc-rtcp") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "shared") (r "^0.1") (f (quote ("marshal"))) (k 0) (p "rtc-shared")) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1q3601xqajdfaak4rzqiwfkshiibsrxmnm4w484ippyc1h91m7dg")))

(define-public crate-rtc-rtcp-0.2.0 (c (n "rtc-rtcp") (v "0.2.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "shared") (r "^0.2.0") (f (quote ("marshal"))) (k 0) (p "rtc-shared")) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0cnszqzs20893xp1dc12imrnyxqlyipm1296zpmsgks8s0qp0n74")))

