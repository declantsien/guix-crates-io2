(define-module (crates-io rt c- rtc-sdp) #:use-module (crates-io))

(define-public crate-rtc-sdp-0.1.0 (c (n "rtc-sdp") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "substring") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0n7fgcxzj1j77k24icij9i3zd9ff832xcdjcj80bm5y86wfimjc9")))

(define-public crate-rtc-sdp-0.1.1 (c (n "rtc-sdp") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "substring") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0g69x7yqp8dvpckqz5vc5mfrbdk5y5b1r58n2w7pw5zr7arafy1f")))

(define-public crate-rtc-sdp-0.2.0 (c (n "rtc-sdp") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1hkhmkxqqyj5j67rv3lvv3nc2mga2myzm0aplfi6agykx30pmdyi")))

