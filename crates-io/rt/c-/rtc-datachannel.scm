(define-module (crates-io rt c- rtc-datachannel) #:use-module (crates-io))

(define-public crate-rtc-datachannel-0.1.0 (c (n "rtc-datachannel") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sctp") (r "^0.1") (d #t) (k 0) (p "rtc-sctp")) (d (n "shared") (r "^0.1") (f (quote ("marshal"))) (k 0) (p "rtc-shared")) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15293xlnly4sna5slmfpri6jj0kxj5kz3vn9cs5zxnyzsqam4gdd")))

(define-public crate-rtc-datachannel-0.2.0 (c (n "rtc-datachannel") (v "0.2.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "sctp") (r "^0.2.0") (d #t) (k 0) (p "rtc-sctp")) (d (n "shared") (r "^0.2.0") (f (quote ("marshal"))) (k 0) (p "rtc-shared")) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "062gax2nnfdkwvqci31svqlvcygjwb24lf1xq8fa8cmb6dxzqrjc")))

