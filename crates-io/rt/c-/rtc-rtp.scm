(define-module (crates-io rt c- rtc-rtp) #:use-module (crates-io))

(define-public crate-rtc-rtp-0.1.0 (c (n "rtc-rtp") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shared") (r "^0.1") (f (quote ("marshal"))) (k 0) (p "rtc-shared")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pvsqfn1k7hby9rzr0h3l4jjcw2gacwl40xpqhrmiwd4q094yya9")))

(define-public crate-rtc-rtp-0.2.0 (c (n "rtc-rtp") (v "0.2.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shared") (r "^0.2.0") (f (quote ("marshal"))) (k 0) (p "rtc-shared")) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1q1wc9j9v2457h2kzxxjkdx8n8frh8ld280dai6s23zp3vcq8jsg")))

