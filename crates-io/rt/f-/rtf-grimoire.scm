(define-module (crates-io rt f- rtf-grimoire) #:use-module (crates-io))

(define-public crate-rtf-grimoire-0.1.0 (c (n "rtf-grimoire") (v "0.1.0") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)))) (h "1s5ri8wgd6jfmf7qd1y7ssb1y4d1a8zi85m23dbpswqf3h5av7r8")))

(define-public crate-rtf-grimoire-0.1.1 (c (n "rtf-grimoire") (v "0.1.1") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)))) (h "1sidp6ink6xz83ljgiq6mmgqibd9dhcf7wjf87hjm42lhzia8hng")))

(define-public crate-rtf-grimoire-0.2.0 (c (n "rtf-grimoire") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1hc30nd1ksky822m3vmdwbgrsdqsnsm8ycfmlv2mlh12nksgny7k") (r "1.58")))

(define-public crate-rtf-grimoire-0.2.1 (c (n "rtf-grimoire") (v "0.2.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1ld4vcd1qjxwgqdfj0wysrx2q1zhfk0mbdmzz6sayd9wpkp6rmly") (r "1.58")))

