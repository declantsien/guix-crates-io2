(define-module (crates-io rt r_ rtr_translator) #:use-module (crates-io))

(define-public crate-rtr_translator-0.1.0 (c (n "rtr_translator") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1i3n1ylwi0hryg5kgsnrkbnmnsbpnji0wv33l7ra6dzp28bcppwd")))

(define-public crate-rtr_translator-0.1.1 (c (n "rtr_translator") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "05n164y20jsd4hsbjbxnc0ygk4vw16w5m1p2f6hlkkjlfd10xkbs")))

(define-public crate-rtr_translator-0.1.2 (c (n "rtr_translator") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "string-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "092zw46118p94vfjr2pi50z3xnmzsq9va3avmixgdk1lv1cagrn3")))

