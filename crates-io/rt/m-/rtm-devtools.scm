(define-module (crates-io rt m- rtm-devtools) #:use-module (crates-io))

(define-public crate-rtm-devtools-0.0.3 (c (n "rtm-devtools") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "aquamarine") (r "^0.1.11") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-timer") (r "^3") (d #t) (k 2)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "112qz9ka6187nmrhbmbyhcl20i4jcskspc43m9czxagi7w4k3fdl") (f (quote (("default")))) (r "1.56.0")))

