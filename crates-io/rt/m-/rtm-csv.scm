(define-module (crates-io rt m- rtm-csv) #:use-module (crates-io))

(define-public crate-rtm-csv-0.0.1 (c (n "rtm-csv") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "aquamarine") (r "^0.1.11") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-timer") (r "^3") (d #t) (k 2)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0cpb0qivfydf8bixplb0zvvbqib50qfy3l1p8gg1zd8h0pq64qkx") (f (quote (("default")))) (r "1.56.0")))

(define-public crate-rtm-csv-0.0.2 (c (n "rtm-csv") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "aquamarine") (r "^0.1.11") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-timer") (r "^3") (d #t) (k 2)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1vbz28jh1zyhkf7bd5yl5gk67ys9wzfcf99ia4p496nkr65ks44g") (f (quote (("default")))) (r "1.56.0")))

