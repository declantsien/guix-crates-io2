(define-module (crates-io rt m- rtm-attributes) #:use-module (crates-io))

(define-public crate-rtm-attributes-0.0.2 (c (n "rtm-attributes") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "aquamarine") (r "^0.1.11") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-timer") (r "^3") (d #t) (k 2)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1dp762i16nnr158qajnr6q1qsvgnpvmm2psq0z1fm3yzckm3vgvq") (f (quote (("default")))) (r "1.56.0")))

