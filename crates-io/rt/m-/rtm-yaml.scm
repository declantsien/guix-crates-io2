(define-module (crates-io rt m- rtm-yaml) #:use-module (crates-io))

(define-public crate-rtm-yaml-0.0.2 (c (n "rtm-yaml") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "aquamarine") (r "^0.1.11") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-timer") (r "^3") (d #t) (k 2)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "17jp2lzymmlxrmz3w8skhq9bfqxv5mda9k5i3sjrbjj4mgw0jj1d") (f (quote (("default")))) (r "1.56.0")))

