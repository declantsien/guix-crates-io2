(define-module (crates-io rt t- rtt-target) #:use-module (crates-io))

(define-public crate-rtt-target-0.1.0 (c (n "rtt-target") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "003llkphh8xzg9gasaqy7084ji7bw6g3ghzdylcpkpbrwji4imxi") (f (quote (("examples-cortex-m" "cortex-m" "cortex-m-rt" "panic-halt" "ufmt"))))))

(define-public crate-rtt-target-0.1.1 (c (n "rtt-target") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0p16nyk6nvib8zfml8d83h0vnxglamq4435s3dfjzpd2f0hdmsr6") (f (quote (("examples-cortex-m" "cortex-m" "cortex-m-rt" "panic-halt" "ufmt"))))))

(define-public crate-rtt-target-0.2.0 (c (n "rtt-target") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "14f11v3l3fnmldbdh76jp58vwvjaw4fszdxk8iq25kxvhilz7caq") (f (quote (("examples-cortex-m" "cortex-m" "cortex-m-rt" "panic-halt" "ufmt"))))))

(define-public crate-rtt-target-0.2.1 (c (n "rtt-target") (v "0.2.1") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "03agzin6p53jl5v8x4wshfxvsq3660pfwq88pd8hzbarslss7mbb") (f (quote (("examples-cortex-m" "cortex-m" "cortex-m-rt" "panic-halt" "ufmt"))))))

(define-public crate-rtt-target-0.2.2 (c (n "rtt-target") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "1qkhazddrv8qzfkwif16fvvp18936kmzjwy47xccbn56nv2v8s88") (f (quote (("examples-cortex-m" "cortex-m" "cortex-m-rt" "panic-halt" "ufmt"))))))

(define-public crate-rtt-target-0.3.0 (c (n "rtt-target") (v "0.3.0") (d (list (d (n "cortex-m") (r ">=0.6.2, <0.7.0") (o #t) (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r ">=0.2.0, <0.3.0") (o #t) (d #t) (k 0)) (d (n "riscv") (r ">=0.6.0, <0.7.0") (o #t) (d #t) (k 0)) (d (n "ufmt") (r ">=0.1.0, <0.2.0") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0rlgh541zwla8ss15kqvz65a5c3dkbv9l4i0zhpq64madmang8h1") (f (quote (("examples-cortex-m" "cortex-m" "cortex-m-rt" "panic-halt" "ufmt"))))))

(define-public crate-rtt-target-0.3.1 (c (n "rtt-target") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "16wx1dxzlxawlid89kymb53p3kqp32g20rraaqdga10jpdc60p86")))

(define-public crate-rtt-target-0.4.0 (c (n "rtt-target") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.0.0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "00zkbj6xs5xrxrakk4p6x7hcvfcbl2wsjfa01rbbzfd1gg3i5yis")))

(define-public crate-rtt-target-0.5.0 (c (n "rtt-target") (v "0.5.0") (d (list (d (n "critical-section") (r "^1.0.0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "1w1pa39bw4s9sgbwv05dljh88qq11av1n7y0yd2qwf1jd2g4rcqh")))

