(define-module (crates-io rt t- rtt-log) #:use-module (crates-io))

(define-public crate-rtt-log-0.1.0 (c (n "rtt-log") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rtt-target") (r "^0.3") (d #t) (k 0)))) (h "0qb5a6qqpw9dyfakakps16ya7klgwhp6cqpb1s2m339brlfa6b1d") (f (quote (("riscv" "rtt-target/riscv") ("default") ("cortex-m" "rtt-target/cortex-m")))) (r "1.56")))

(define-public crate-rtt-log-0.2.0 (c (n "rtt-log") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rtt-target") (r "^0.4") (d #t) (k 0)))) (h "01aicnfs401w623hd3drgb8f5k6q9argparm8mcv86r1hjca0svf") (f (quote (("default")))) (r "1.56")))

(define-public crate-rtt-log-0.3.0 (c (n "rtt-log") (v "0.3.0") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rtt-target") (r "^0.5.0") (d #t) (k 0)))) (h "11g1q33qpsb3skjp9f86kd3dja1gkyffs074c070g49fnlm5lk4k") (f (quote (("default")))) (r "1.56")))

