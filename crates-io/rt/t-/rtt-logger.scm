(define-module (crates-io rt t- rtt-logger) #:use-module (crates-io))

(define-public crate-rtt-logger-0.1.0 (c (n "rtt-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rtt-target") (r "^0.2.0") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "1snj8qyi2ygzajr53bx15q4s4npyfwbxw84pa2nnjksx1aid70nh")))

(define-public crate-rtt-logger-0.2.0 (c (n "rtt-logger") (v "0.2.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rtt-target") (r "^0.3.1") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "0ssbxzxiqjy3vhms7rfl7lmxrwlg8qpkbap7mc46ljl80cn7b0rc")))

(define-public crate-rtt-logger-0.2.1 (c (n "rtt-logger") (v "0.2.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rtt-target") (r "^0.3.1") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "0kim0f5v9b5a0955pdhj1fz7vc4xifcg6kipx07snvmgihi2rx5j")))

(define-public crate-rtt-logger-0.3.0 (c (n "rtt-logger") (v "0.3.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rtt-target") (r "^0.4") (d #t) (k 0)))) (h "1mqzja26x3w6y0whk3qqf82cfzj8ngdm8ib7hribqxy4m0pfllkk") (y #t)))

