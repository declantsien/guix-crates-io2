(define-module (crates-io rt -f rt-format) #:use-module (crates-io))

(define-public crate-rt-format-0.1.0 (c (n "rt-format") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0662axvixhhb9cl20x91bfpymx6vi4j09dx4jmznz32w7sgi9s21")))

(define-public crate-rt-format-0.1.1 (c (n "rt-format") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qdnsh0r043irfpn8k2083w68qwq7qb5jllv5kkdf4im9xcf9850")))

(define-public crate-rt-format-0.1.2 (c (n "rt-format") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1f0jndh895xsl6kmx5c7sb5mfgz6l8rxxjd1a3aiigxahn1swsyd")))

(define-public crate-rt-format-0.2.0 (c (n "rt-format") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1c5hi3c9if7wsgvnwgy2sk02xmrs1yp8d7ix3lnxzrijzccknclk")))

(define-public crate-rt-format-0.2.1 (c (n "rt-format") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1i476gh47cjipn4dg8y1ls4my7p1g9kbicg19aj7ivw1597kwd27")))

(define-public crate-rt-format-0.3.0 (c (n "rt-format") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bmxn40aaflxgrz5pdjdkk244aj293g65vyrg0dbnb65gwizyglm")))

(define-public crate-rt-format-0.3.1 (c (n "rt-format") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1qjjwh9ny95xck1kp99gi6hfm9glrx54jx8npnj6yccxc7p7q225")))

