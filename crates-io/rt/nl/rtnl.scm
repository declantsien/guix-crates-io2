(define-module (crates-io rt nl rtnl) #:use-module (crates-io))

(define-public crate-rtnl-1.0.0 (c (n "rtnl") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "ipnet") (r "^2.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rtnetlink") (r "^0.13.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "1i84hlcdzknj0yr5l21s7j4kh11kh59fyyddfkf5nd6bac0dadd2")))

