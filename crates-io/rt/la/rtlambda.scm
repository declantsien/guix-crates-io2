(define-module (crates-io rt la rtlambda) #:use-module (crates-io))

(define-public crate-rtlambda-0.0.1 (c (n "rtlambda") (v "0.0.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4") (o #t) (d #t) (k 0)))) (h "10z6i9lxj56fnj8xvja4m5iibyn0jkaifyxqchvd60mlf0d0sf5g") (f (quote (("default" "ureq")))) (s 2) (e (quote (("ureq" "dep:ureq"))))))

