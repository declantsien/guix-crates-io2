(define-module (crates-io rt ft rtftp) #:use-module (crates-io))

(define-public crate-rtftp-1.1.1 (c (n "rtftp") (v "1.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "16x7048jc1aq9jzw6gjzds55ixk76l0vf458xdq7mqb3pdl8ig37")))

(define-public crate-rtftp-1.1.2 (c (n "rtftp") (v "1.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "1cq9l2c1jm8r15m23rhlmg63sqypi6qsb867bgc66m3q58anky61")))

