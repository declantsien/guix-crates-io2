(define-module (crates-io rt #{3c}# rt3conf) #:use-module (crates-io))

(define-public crate-rt3conf-0.1.0 (c (n "rt3conf") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0ns6vjwwj4cd6cw8lc1py498fya6q8117jri41skr994ih1vm3c6")))

