(define-module (crates-io rt sh rtshark) #:use-module (crates-io))

(define-public crate-rtshark-1.0.0 (c (n "rtshark") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.24") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1qqszid3lh7wzgpfidfw6689b9khhgsbpmkc5715422l6jy0dhjy")))

(define-public crate-rtshark-2.0.0 (c (n "rtshark") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.24") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1xfr0wpfqdzq7lrcm8p7zp0lhc6bp9kd0sihv14aw8ydjykr0gbx")))

(define-public crate-rtshark-2.1.0 (c (n "rtshark") (v "2.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.24") (d #t) (k 2)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1xg26gf2dhciiqaa4ixfqyhi2rv35bicav16m2c2qvp0kqvyai3z")))

(define-public crate-rtshark-2.1.1 (c (n "rtshark") (v "2.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.24") (d #t) (k 2)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0i62gnwaqqy1l3aydhy10as1wpmrmr9xn5bja86dynrvkizbm7hs")))

(define-public crate-rtshark-2.2.0 (c (n "rtshark") (v "2.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.24") (d #t) (k 2)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1zq6smwbzsrlx2s0m7yxjgak6fl0ww9908gjjgviablcd8zz92i9")))

(define-public crate-rtshark-2.3.1 (c (n "rtshark") (v "2.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.25") (d #t) (t "cfg(unix)") (k 2)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "serial_test") (r "^0.9") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1479v8p6ss4v6kiiwymdx1ns906vs40mpkaw08d70jc921jy78yd")))

(define-public crate-rtshark-2.4.0 (c (n "rtshark") (v "2.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.25") (d #t) (t "cfg(unix)") (k 2)) (d (n "quick-xml") (r "^0.27") (d #t) (k 0)) (d (n "serial_test") (r "^1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "17salxxgs6xz333fx5lb97sibv9fvz4szgdv5s5mlsmhd3lyjsi8")))

(define-public crate-rtshark-2.5.0 (c (n "rtshark") (v "2.5.0") (d (list (d (n "chrono") (r "^0.4.20") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.25") (d #t) (t "cfg(unix)") (k 2)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "serial_test") (r "^2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "09b13j4m0w231km31vhgk6w415wg60bzqbb5nwd6cy4pci702m1z")))

(define-public crate-rtshark-2.6.0 (c (n "rtshark") (v "2.6.0") (d (list (d (n "chrono") (r "^0.4.20") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.25") (d #t) (t "cfg(unix)") (k 2)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "serial_test") (r "^2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0swn0s7axijsz8sm05rhhdavkzyvng1vj7q1qbwvpq0rv9ljfqdw")))

(define-public crate-rtshark-2.7.0 (c (n "rtshark") (v "2.7.0") (d (list (d (n "chrono") (r "^0.4.20") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.25") (d #t) (t "cfg(unix)") (k 2)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "serial_test") (r "^2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "07fha6fv29jngmsia911mnnm3b95wv289a1kvg4j45x6kb7zrrcf")))

(define-public crate-rtshark-2.7.1 (c (n "rtshark") (v "2.7.1") (d (list (d (n "chrono") (r "^0.4.20") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.25") (d #t) (t "cfg(unix)") (k 2)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "serial_test") (r "^2") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "107kdlhf3pbgaazsbl8y9a819gch3fybd7hg977b448am7dr10cj")))

