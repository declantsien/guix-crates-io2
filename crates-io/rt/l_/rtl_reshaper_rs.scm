(define-module (crates-io rt l_ rtl_reshaper_rs) #:use-module (crates-io))

(define-public crate-rtl_reshaper_rs-0.1.0 (c (n "rtl_reshaper_rs") (v "0.1.0") (d (list (d (n "arabic_reshaper") (r "^0.2.0") (d #t) (k 0)) (d (n "unic-bidi") (r "^0.9.0") (d #t) (k 0)))) (h "0c8d3yv3j017jjfxcpj99ymcvdw1405rls97kmwzkharsnakqz1r")))

