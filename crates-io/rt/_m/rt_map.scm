(define-module (crates-io rt _m rt_map) #:use-module (crates-io))

(define-public crate-rt_map-0.1.0 (c (n "rt_map") (v "0.1.0") (h "0wacjmcgd3w71ri4336vn9l6aqsibs9m67hwd84gjzvbbvvsqhwc")))

(define-public crate-rt_map-0.2.0 (c (n "rt_map") (v "0.2.0") (h "02b2c90m4r45v7ybzri062pxlzrp32zc9r5578ibnr128rkxzic9")))

(define-public crate-rt_map-0.3.0 (c (n "rt_map") (v "0.3.0") (h "186vxph6jgapkgiv4lg3wbng0qp459jnpq8q0jsfwzff38ykjmb6")))

(define-public crate-rt_map-0.4.0 (c (n "rt_map") (v "0.4.0") (h "0magiq5vy0rrjnbs12c7nz290cr70mhw0d84f11i2x8s12xll8pm")))

(define-public crate-rt_map-0.5.0 (c (n "rt_map") (v "0.5.0") (h "11ap3yiknnwj048nl3piwym3bhjld7qyj5vsr6016mfzaginc4ym")))

(define-public crate-rt_map-0.5.1 (c (n "rt_map") (v "0.5.1") (h "1dx7dqf16k85z57kwmqiywpfirq584hcsar5px8gfmic5yapyfjk") (f (quote (("rt_vec") ("default"))))))

(define-public crate-rt_map-0.5.2 (c (n "rt_map") (v "0.5.2") (d (list (d (n "rt_ref") (r "^0.2.0") (d #t) (k 0)))) (h "06xvp4dirf3r2hm1bp2x7k10z42dnph8al00dz2irrgsjyg5k3al") (f (quote (("unsafe_debug" "rt_ref/unsafe_debug"))))))

