(define-module (crates-io rt op rtop-rust) #:use-module (crates-io))

(define-public crate-rtop-rust-0.1.0 (c (n "rtop-rust") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "07f3nrkjgl720zz5syg840f5v3vh11091xrz8v31irll0i05bnbn")))

