(define-module (crates-io rt _p rt_proto) #:use-module (crates-io))

(define-public crate-rt_proto-0.1.0 (c (n "rt_proto") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1bwdiirkxj3y5kb60767kg5khbs858yfjbjm73cpxbha3m9chssn")))

(define-public crate-rt_proto-0.1.1 (c (n "rt_proto") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "05c742ny4cjw3jw6ykw9bcx1rwgjqa7iv55ikixvj20hpzjlsvk0")))

(define-public crate-rt_proto-0.1.2 (c (n "rt_proto") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0imdxhz008nzsq06zfvmczxh3m6d2ghm4i2s5y21kd3igl58idrq")))

(define-public crate-rt_proto-0.1.3 (c (n "rt_proto") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "12fbvwxdrv1kx998lxz5y6sjkrk76jsyf26qgyp5jkfcqpqjcm2n")))

(define-public crate-rt_proto-0.1.4 (c (n "rt_proto") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0djv4pabkxq75hinjdp9dkfj7ix13iiqx6wy5rlmc6hq5ab30c59")))

(define-public crate-rt_proto-0.1.5 (c (n "rt_proto") (v "0.1.5") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "041jy7khsg9w46lywlk8vy6d7x6xisadc42d47hjqxq67rjdhwlm")))

(define-public crate-rt_proto-0.1.6 (c (n "rt_proto") (v "0.1.6") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0hphlkvkvykib0lwkd1fs63wv1m84brkz0s1s94npsd67jy9f48f")))

(define-public crate-rt_proto-0.1.7 (c (n "rt_proto") (v "0.1.7") (d (list (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0dwwrfgj59hjrbh5viqkmx9k46zqzqf362gmh9l7ciryvkq1lav9")))

