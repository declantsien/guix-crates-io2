(define-module (crates-io rt tm rttmon) #:use-module (crates-io))

(define-public crate-rttmon-0.1.0 (c (n "rttmon") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "0bqc6sfx5hmfd8yhg94wmv38w9x4dmdl3grjw8lhj3m2pkzvcc15")))

(define-public crate-rttmon-0.1.1 (c (n "rttmon") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "1qvpmrz188xfrwsl613p674li98s5fq3hfrn8bfd1wzmjrfxaiff")))

(define-public crate-rttmon-0.1.2 (c (n "rttmon") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "11j62z678xnxkxf1agdhpifa4g7ylf362fybm1qvlrf6k4dm0fgc")))

(define-public crate-rttmon-0.1.3 (c (n "rttmon") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "032dsm5xryqn14dgmnvxhn124pv3ncycc8gd9isfwps88hddqrk5")))

(define-public crate-rttmon-0.2.0 (c (n "rttmon") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "134kpg2nv0hk96bnyvfkb8sn0h1vj4k7wh5xfrc7ks3v4yh5l5vn")))

(define-public crate-rttmon-0.2.1 (c (n "rttmon") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "0fc9ghsndkb4gsz4sk8hy251i929fyw80yznpm74y19fdh0wvw9y")))

(define-public crate-rttmon-0.2.2 (c (n "rttmon") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "0sy5chnhzmn1475y00wgc70xz38mqf34v4sfvv9sqmkiwcshb5cp")))

(define-public crate-rttmon-0.2.3 (c (n "rttmon") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)))) (h "04anckip8rrjy77paj3f3shv4qqpjxvpcx5hi4x27xpawylbb4da")))

