(define-module (crates-io rt ss rtss-sys) #:use-module (crates-io))

(define-public crate-rtss-sys-0.1.0 (c (n "rtss-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0w6xvmscdrmjqpjkpdxhm0j5zzvcay5l3plwppn577k9j2yi00hs")))

(define-public crate-rtss-sys-0.1.1 (c (n "rtss-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0kv1h8l09jaqh1xdnkyqkrw2m712gyq61ayvrhrcr2vmhc7vnf76")))

(define-public crate-rtss-sys-0.1.2 (c (n "rtss-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "148yv5lhwrfzxrm9v4hxx8zca5ggxmapp29fg9ayhg333c60rb73")))

