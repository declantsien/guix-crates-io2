(define-module (crates-io rt ss rtss) #:use-module (crates-io))

(define-public crate-rtss-0.3.0 (c (n "rtss") (v "0.3.0") (h "01qg52bivq5q9w9cr05w1781c66h6nwzv23ag6hda9q4ismm21p9")))

(define-public crate-rtss-0.5.0 (c (n "rtss") (v "0.5.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "1s28xf11jrzk02qgmj51wj2v4cb3k2p8l56m8733qxn2zqka7r2j")))

(define-public crate-rtss-0.6.0 (c (n "rtss") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "14rl2nlbpyim82diwif833k5m1hcsffkm5fwrwxj9rywfnnn4jpy")))

(define-public crate-rtss-0.6.1 (c (n "rtss") (v "0.6.1") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "0qls7ba9wd15wpyap9hxfwz728lkshgqiw2g7rhg0ym6f0dkssgz")))

(define-public crate-rtss-0.6.2 (c (n "rtss") (v "0.6.2") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "1r1b6fynkjnpj5p3k209sa13mjvh4k0ghzwnribm48dh9v7lfnnv")))

