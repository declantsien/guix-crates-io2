(define-module (crates-io rt ps rtps-rs) #:use-module (crates-io))

(define-public crate-rtps-rs-0.1.0 (c (n "rtps-rs") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "speedy") (r "^0.4.1") (d #t) (k 0)) (d (n "speedy-derive") (r "^0.3.4") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "tokio") (r "^0.1.13") (d #t) (k 0)))) (h "0zdw5sczglnbvy55rax7f8cbvcl5sdljk7dn0k2k6css0dwf89xj")))

(define-public crate-rtps-rs-0.1.1 (c (n "rtps-rs") (v "0.1.1") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "speedy") (r "^0.4.1") (d #t) (k 0)) (d (n "speedy-derive") (r "^0.3.4") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "tokio") (r "^0.1.13") (d #t) (k 0)))) (h "0q3k5gqqlarllsia3bgwgvms50spsym4c5rahfz2fh2lhb7jrv31")))

(define-public crate-rtps-rs-0.1.2 (c (n "rtps-rs") (v "0.1.2") (d (list (d (n "bit-set") (r "^0.5.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "bytes") (r "^0.6.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "speedy") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.5.1") (f (quote ("codec"))) (d #t) (k 0)))) (h "1y8p634rdwjlrhll9yjls5g4ifmx4200prdmn29va69hzr7sr0kv")))

