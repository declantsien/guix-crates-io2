(define-module (crates-io rt ps rtps-elements) #:use-module (crates-io))

(define-public crate-rtps-elements-0.1.0 (c (n "rtps-elements") (v "0.1.0") (d (list (d (n "rtps-idl") (r "^0.1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "066npfahznk4g17air4hnb37xpfn8ib4iqa94jk434af4b64kmna")))

(define-public crate-rtps-elements-0.2.0 (c (n "rtps-elements") (v "0.2.0") (d (list (d (n "rtps-idl") (r "^0.2.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ckh06gawxw8k9a1363cjqyzapypgf0ph91zax7s4c684j2chqnr")))

