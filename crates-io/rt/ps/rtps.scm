(define-module (crates-io rt ps rtps) #:use-module (crates-io))

(define-public crate-rtps-0.1.0 (c (n "rtps") (v "0.1.0") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "clippy") (r "^0.0.89") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "0.8.*") (d #t) (k 0)) (d (n "serde_json") (r "0.8.*") (d #t) (k 0)))) (h "15c0m0g8y3lqc8ajf1b81v1ikf2lvwcc7wwrqhfz08fppypancm6")))

(define-public crate-rtps-0.2.0 (c (n "rtps") (v "0.2.0") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)))) (h "1p9z1zhmg9xgqwr0xx5r1958ajp9lxfvrg3rafv76i8am1a0ddlj")))

(define-public crate-rtps-0.2.1 (c (n "rtps") (v "0.2.1") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)))) (h "0p2lgsdykfwc6pdb1f5nbnrcbp5kspidi77fc8kjnlz34xpzz7qz")))

(define-public crate-rtps-0.2.2 (c (n "rtps") (v "0.2.2") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)))) (h "06papbqvisblxwcx0xg12m9mc09lga1hh7hzxxqsmc6kvnz77ggn")))

(define-public crate-rtps-0.2.3 (c (n "rtps") (v "0.2.3") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (d #t) (k 0)) (d (n "net2") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)))) (h "08f8a94vnrxfidw3dsaj74gfb059jrkp74g2dlyyzl28m6pqfaba")))

