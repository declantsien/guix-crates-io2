(define-module (crates-io rt ps rtps-idl) #:use-module (crates-io))

(define-public crate-rtps-idl-0.1.0 (c (n "rtps-idl") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "rtps-idl-grammar") (r "^0.2") (d #t) (k 0)))) (h "1mvz8dllavwakx5sji6vgr3lyzvzd12anrva328g48f9w0frc1hk")))

(define-public crate-rtps-idl-0.2.0 (c (n "rtps-idl") (v "0.2.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "rtps-idl-grammar") (r "^0.2") (d #t) (k 0)))) (h "0n87jcq3vbwbv1d8wbppmcnpsch06n5xgjglsl3cj0q9b6iljbrp")))

