(define-module (crates-io rt ps rtps-idl-grammar) #:use-module (crates-io))

(define-public crate-rtps-idl-grammar-0.2.1 (c (n "rtps-idl-grammar") (v "0.2.1") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "1rjhxcw8mxb70ixxncav6gs582kb3pgd5nz49s2nl603s47721pk")))

(define-public crate-rtps-idl-grammar-0.2.3 (c (n "rtps-idl-grammar") (v "0.2.3") (d (list (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "01ihvpxbdnl61z3r59lc8rfqjgz0rwrn7cpiz5nf73ax059h4xl9")))

