(define-module (crates-io rt ps rtps-gen) #:use-module (crates-io))

(define-public crate-rtps-gen-0.1.0 (c (n "rtps-gen") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rtps-elements") (r "^0.1.0") (d #t) (k 2)) (d (n "rtps-idl") (r "^0.1.0") (d #t) (k 0)))) (h "1v86y3dqidkymdzaynpsirdh1i4yrpann5czdwp679hzrw84d8ic")))

(define-public crate-rtps-gen-0.2.0 (c (n "rtps-gen") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rtps-elements") (r "^0.2.0") (d #t) (k 2)) (d (n "rtps-idl") (r "^0.2.0") (d #t) (k 0)))) (h "1vcs1zimpc05zrn6698qnr213apn6d3r027nzr78hqhrpayqq6nc")))

