(define-module (crates-io rt p- rtp-rs) #:use-module (crates-io))

(define-public crate-rtp-rs-0.1.0 (c (n "rtp-rs") (v "0.1.0") (h "0i45i0lqjmj694kzfa4crl08wy2jfvm2b3avkm8bvs5nzkc08kq5")))

(define-public crate-rtp-rs-0.2.0 (c (n "rtp-rs") (v "0.2.0") (h "0qc1dqr03n4bhzq0gbll6d581n570mviak5xbhwx8shbk9fg71mw")))

(define-public crate-rtp-rs-0.3.0 (c (n "rtp-rs") (v "0.3.0") (h "132r7ib2j1s0pk4k113k388cqxz1ry0kw725fzii0xi0kwbv33g7")))

(define-public crate-rtp-rs-0.4.0 (c (n "rtp-rs") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1fr0fwi9sglb01503hvfd71j7060v1kb5zib1lic21prwxxlhpyp")))

(define-public crate-rtp-rs-0.5.0 (c (n "rtp-rs") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1x8pgqbwwqrqnp2ykzdva7inmn7jzcgr42fy07lldm4ka5lhs4g1")))

(define-public crate-rtp-rs-0.6.0 (c (n "rtp-rs") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ma365x07n8k5nkwzc96wd5fchrgnkhlsspz9i1w8dixbd52gvfl")))

