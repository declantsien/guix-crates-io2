(define-module (crates-io rt p- rtp-types) #:use-module (crates-io))

(define-public crate-rtp-types-0.0.1 (c (n "rtp-types") (v "0.0.1") (d (list (d (n "smallvec") (r "^1") (f (quote ("union"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0s52881y5mwvwwgb2wl6bv6pj31nn90vjsf3z8nd5l40p1jsl97w")))

(define-public crate-rtp-types-0.1.0 (c (n "rtp-types") (v "0.1.0") (d (list (d (n "smallvec") (r "^1") (f (quote ("union"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xq28wxm42z1gqqy1aydm9ipg1a7hlxqlkk7bhlpd6gfphdx7aip")))

(define-public crate-rtp-types-0.1.1 (c (n "rtp-types") (v "0.1.1") (d (list (d (n "smallvec") (r "^1") (f (quote ("union"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bzr6d6mbrl36l3lz00576v5rv44vr5971k7hwl5chnrgyxkh6z0") (r "1.57.0")))

