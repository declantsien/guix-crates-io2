(define-module (crates-io rt er rterm) #:use-module (crates-io))

(define-public crate-rterm-0.0.1 (c (n "rterm") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "vte") (r "^0.10") (d #t) (k 0)) (d (n "x11") (r "^2.0") (d #t) (k 0)))) (h "0pl1b7x7mrajmlbcj2g6whzhvcv5p24hxiwa3lii9njl1pak917p")))

(define-public crate-rterm-0.0.2 (c (n "rterm") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "vte") (r "^0.10") (d #t) (k 0)) (d (n "x11") (r "^2.0") (d #t) (k 0)))) (h "0p7i2y8x6mynmasj7a9cws14iwv5gd8r8szynvnralvbpqijipr5")))

(define-public crate-rterm-0.0.3 (c (n "rterm") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "servo-fontconfig") (r "^0.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "vte") (r "^0.10") (d #t) (k 0)) (d (n "x11") (r "^2.0") (d #t) (k 0)))) (h "0rcd95q229wpnrm6kyg5mmbshn5x6n9w1wj6zgrvi82yw61wx3gm")))

(define-public crate-rterm-0.0.4 (c (n "rterm") (v "0.0.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "servo-fontconfig") (r "^0.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "vte") (r "^0.10") (d #t) (k 0)) (d (n "x11") (r "^2.0") (d #t) (k 0)))) (h "0r0sn4irnwqhwn0lvawd63z7a9zk9mg3qpzgk06ixbx21w87xrr5")))

(define-public crate-rterm-0.0.5 (c (n "rterm") (v "0.0.5") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "servo-fontconfig") (r "^0.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "vte") (r "^0.10") (d #t) (k 0)) (d (n "x11") (r "^2") (d #t) (k 0)))) (h "01gcs7bqzbpjwkmrl240xk7vg70c2l76kp0d6m64gpljikir4ia5")))

(define-public crate-rterm-0.0.6 (c (n "rterm") (v "0.0.6") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "servo-fontconfig") (r "^0.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "vte") (r "^0.10") (d #t) (k 0)) (d (n "x11") (r "^2") (d #t) (k 0)))) (h "0w3fyyawr0h0rbx83ynrcsqzjvlk088svr9y9acmlkjypbw7cjah")))

(define-public crate-rterm-0.0.7 (c (n "rterm") (v "0.0.7") (d (list (d (n "anyhow") (r ">=1.0") (d #t) (k 0)) (d (n "bitflags") (r ">=1.2") (d #t) (k 0)) (d (n "clap") (r ">=3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r ">=0.22") (d #t) (k 0)) (d (n "servo-fontconfig") (r ">=0.5") (d #t) (k 0)) (d (n "unicode-width") (r ">=0.1") (d #t) (k 0)) (d (n "vte") (r ">=0.10") (d #t) (k 0)) (d (n "x11") (r ">=2") (d #t) (k 0)))) (h "1mc6wqcfxjs7xjlin2xap73ar00bdj0w66r3vknzydflxc0aixpa")))

(define-public crate-rterm-0.0.8 (c (n "rterm") (v "0.0.8") (d (list (d (n "anyhow") (r ">=1.0") (d #t) (k 0)) (d (n "bitflags") (r ">=1.2") (d #t) (k 0)) (d (n "clap") (r ">=3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r ">=0.22") (d #t) (k 0)) (d (n "servo-fontconfig") (r ">=0.5") (d #t) (k 0)) (d (n "unicode-width") (r ">=0.1") (d #t) (k 0)) (d (n "vte") (r ">=0.10") (d #t) (k 0)) (d (n "x11") (r ">=2") (d #t) (k 0)))) (h "0xx07zwmfcvr96g2i9q49hfqyr6lf6yd773v2yr85h18q519fmsl")))

