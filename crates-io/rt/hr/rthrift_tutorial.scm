(define-module (crates-io rt hr rthrift_tutorial) #:use-module (crates-io))

(define-public crate-rthrift_tutorial-0.1.0 (c (n "rthrift_tutorial") (v "0.1.0") (d (list (d (n "clap") (r "< 2.28.0") (d #t) (k 0)) (d (n "ordered-float") (r "^0.3.0") (d #t) (k 0)) (d (n "rthrift") (r "^0.11.0") (d #t) (k 0)) (d (n "try_from") (r "^0.2.0") (d #t) (k 0)))) (h "0qgiy8arbl53irxkq67r7a7i4vf5dm8w6g7gyv3j3l35ch6j3fa6")))

