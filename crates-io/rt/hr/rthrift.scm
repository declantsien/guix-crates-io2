(define-module (crates-io rt hr rthrift) #:use-module (crates-io))

(define-public crate-rthrift-0.11.0 (c (n "rthrift") (v "0.11.0") (d (list (d (n "byteorder") (r "~1.1.0") (d #t) (k 0)) (d (n "integer-encoding") (r "~1.0.4") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "threadpool") (r "~1.7.1") (d #t) (k 0)) (d (n "try_from") (r "~0.2.2") (d #t) (k 0)))) (h "0bnfh58b82yywd0r7yrzlacijcdbwmjr62nq07nbvg9yn1az29pn")))

(define-public crate-rthrift-0.11.0-git-cf7ba4ca (c (n "rthrift") (v "0.11.0-git-cf7ba4ca") (d (list (d (n "byteorder") (r "~1.1.0") (d #t) (k 0)) (d (n "integer-encoding") (r "~1.0.4") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "threadpool") (r "~1.7.1") (d #t) (k 0)) (d (n "try_from") (r "~0.2.2") (d #t) (k 0)))) (h "163rsj6zlshgz333wjv9jrzhgyn8mjh6l8n9adgszciss8w5f4ba")))

