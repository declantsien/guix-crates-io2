(define-module (crates-io rt im rtime_rs) #:use-module (crates-io))

(define-public crate-rtime_rs-0.1.2 (c (n "rtime_rs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "ureq") (r "^2.4") (d #t) (k 0)))) (h "0sxcjmw8pvv1rbi3k2wvmq1jc1vqm08aik4ggspspkkkah71ccw5")))

(define-public crate-rtime_rs-0.1.3 (c (n "rtime_rs") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "ureq") (r "^2.4") (d #t) (k 0)))) (h "01lq6njii1bkhii367w0z0xxikmr36s8v9sgrhxg7ivcvmiqywim")))

