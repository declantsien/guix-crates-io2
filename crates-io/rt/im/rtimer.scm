(define-module (crates-io rt im rtimer) #:use-module (crates-io))

(define-public crate-rtimer-0.2.0 (c (n "rtimer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (f (quote ("mp3"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "17qpslgp2v0sy0n0ivl9gr9vhrfajci959z7gcgqwwyvnya7kkw2")))

(define-public crate-rtimer-0.3.0 (c (n "rtimer") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (f (quote ("mp3"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "0qgwh0xr8f2rw72xzszvs6mx2lci19vv44rhm4s66r5jj30pn0xp")))

(define-public crate-rtimer-1.3.0 (c (n "rtimer") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (f (quote ("mp3"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "0k7z0zjk7yraqqv9i1d733r7d0rhzf1afnlbbzhvz7vmn79ramyp") (y #t)))

(define-public crate-rtimer-1.0.0 (c (n "rtimer") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (f (quote ("mp3"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "11n7kcc64j8iabhbwdl3ak9gzaafg4m52jc4drv7bxmqpa9p8lzr")))

(define-public crate-rtimer-1.1.0 (c (n "rtimer") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (f (quote ("mp3"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "185rd4fm5j0vb5y0vj4c9l3rxdrcj2ffsv96r359w05bw8ssac1r")))

(define-public crate-rtimer-1.2.0 (c (n "rtimer") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (f (quote ("mp3"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "077ibaz3m15gv0i9yypbhw654pbbi8gbmg1fbhvcwrhm7j6m2isp")))

