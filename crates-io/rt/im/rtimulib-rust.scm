(define-module (crates-io rt im rtimulib-rust) #:use-module (crates-io))

(define-public crate-rtimulib-rust-0.1.0 (c (n "rtimulib-rust") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0qgc8xwabr7m3iwzinmy1c60z9wgvl7v0var6cqh1887w4d5k3hm")))

(define-public crate-rtimulib-rust-0.2.0 (c (n "rtimulib-rust") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "01rz3zl0xy32y6ciwkk9kc70x2dknqzic661n2aq02lqpiqrkjnh")))

(define-public crate-rtimulib-rust-0.2.1 (c (n "rtimulib-rust") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0myivz6arlc87rkibdr49pl8qjjlzqp3dbf1rhqy6az9rici2ii3")))

