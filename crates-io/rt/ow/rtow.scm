(define-module (crates-io rt ow rtow) #:use-module (crates-io))

(define-public crate-rtow-0.1.0 (c (n "rtow") (v "0.1.0") (d (list (d (n "chan") (r "^0.1.21") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.14.1") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1h9ir7k3pz5jg9k9mzp13wrkd5jfhrp0vpnrbfrl4mplqf56dlbp")))

