(define-module (crates-io rt no rtnorm-rust) #:use-module (crates-io))

(define-public crate-rtnorm-rust-0.1.0 (c (n "rtnorm-rust") (v "0.1.0") (d (list (d (n "GSL") (r "4.0.*") (d #t) (k 0)) (d (n "num") (r "0.4.*") (d #t) (k 0)))) (h "0g2jxxn9prpbs9fqn5bgq7xvflxk9gx27cv93sc6l0vf8x9lf5m9")))

