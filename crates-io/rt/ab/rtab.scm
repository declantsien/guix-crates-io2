(define-module (crates-io rt ab rtab) #:use-module (crates-io))

(define-public crate-rtab-0.1.0 (c (n "rtab") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "0rbh0hcm7c31qqvrpqw2g2b2pdm6py60ckl5ql7jyhk7f01fxb8v")))

(define-public crate-rtab-0.1.1 (c (n "rtab") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1jcrxhs4zyksnsa5bb282frj2mvki4gqqnagjh2xdjqmr88jrvnz")))

(define-public crate-rtab-0.1.2 (c (n "rtab") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1wvh8j9hg1cadbcvglndh6vk79kkzjqqf9f64vpimswa1qkq9gba")))

(define-public crate-rtab-0.1.3 (c (n "rtab") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1nrmy38x4k0vsmlmx4v0ij6ql1k0mrq3mx4vj55qp3x1c94255pw")))

(define-public crate-rtab-0.1.4 (c (n "rtab") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "18qsxaljcq9cgpn5bagnj0r4pqhhcszq0b8lzqc2pzmx7z5p7jf4")))

(define-public crate-rtab-0.1.5 (c (n "rtab") (v "0.1.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1khb4zjf8164a7wdd9xwhz7zd50qgd8blmfdzj043a0w3qv9g3sw")))

