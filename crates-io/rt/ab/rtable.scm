(define-module (crates-io rt ab rtable) #:use-module (crates-io))

(define-public crate-rtable-0.1.0 (c (n "rtable") (v "0.1.0") (h "15xn6s8li7naqxxs0a4jr28npz2bl416vnmmgwwjf9wylvafw2mn")))

(define-public crate-rtable-0.1.1 (c (n "rtable") (v "0.1.1") (h "0rp046ipy4z4wpj0z5kd527kirs69b44j8haw7gk8rngjs0jwzzy")))

(define-public crate-rtable-0.1.2 (c (n "rtable") (v "0.1.2") (h "113lg74hd1mg45jpx06z95rzm12wd732iqfj7ybj4w2p0jgjzk9z")))

(define-public crate-rtable-0.1.3 (c (n "rtable") (v "0.1.3") (h "08pfw5dzpk8n209yh8ykwc5c3cpz45x9lxdvmldd30grw9zyy35w")))

(define-public crate-rtable-0.1.4 (c (n "rtable") (v "0.1.4") (h "1qccahgijcm19gr46xwhg76sf74323k6yvsi82i1w9lhbprgnqxb")))

(define-public crate-rtable-0.2.0 (c (n "rtable") (v "0.2.0") (h "0c6ch85dlhwi741pskdb60ri4j4hsifp9vhmh7nx41s1q5c4gj82")))

(define-public crate-rtable-0.2.1 (c (n "rtable") (v "0.2.1") (h "0m6jlm067p2glhhv08xdnxjx5xag2f747n8qsqfihk6d98y1w7pz")))

