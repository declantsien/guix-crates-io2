(define-module (crates-io rt lt rtltcp) #:use-module (crates-io))

(define-public crate-rtltcp-0.1.1 (c (n "rtltcp") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "listenfd") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "rtlsdr_mt") (r "^2.1") (d #t) (k 0)) (d (n "systemd") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "1z456qgdzcifdhbvxgd4050k7nsvarq4p2d3qmr2ki4cc5bzynqc") (f (quote (("default" "daemon_systemd") ("daemon_systemd" "listenfd" "systemd"))))))

