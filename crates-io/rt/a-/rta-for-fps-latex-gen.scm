(define-module (crates-io rt a- rta-for-fps-latex-gen) #:use-module (crates-io))

(define-public crate-rta-for-fps-latex-gen-0.1.0 (c (n "rta-for-fps-latex-gen") (v "0.1.0") (d (list (d (n "rta-for-fps-latex-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "rta-for-fps-lib") (r "^0.1.0") (d #t) (k 0)))) (h "1vvv18wzb3xb4f5l4b7vbcviwaqsq1h434s8mj8qsqxl8y2gzkjp")))

(define-public crate-rta-for-fps-latex-gen-0.1.1 (c (n "rta-for-fps-latex-gen") (v "0.1.1") (d (list (d (n "rta-for-fps-latex-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "rta-for-fps-lib") (r "^0.1.0") (d #t) (k 0)))) (h "02wzwa3k6k1k89a7qlsycfx4j6x0krcy1zf0snarhdg21310mdnk")))

(define-public crate-rta-for-fps-latex-gen-0.2.0 (c (n "rta-for-fps-latex-gen") (v "0.2.0") (d (list (d (n "rta-for-fps-latex-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "rta-for-fps-lib") (r "^0.2.0") (d #t) (k 0)))) (h "11z5s17qk90x790vy731adrf739fxb5mszfnikc5ryl71k8d6vyc")))

