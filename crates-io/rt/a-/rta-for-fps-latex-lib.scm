(define-module (crates-io rt a- rta-for-fps-latex-lib) #:use-module (crates-io))

(define-public crate-rta-for-fps-latex-lib-0.1.0 (c (n "rta-for-fps-latex-lib") (v "0.1.0") (d (list (d (n "rta-for-fps-lib") (r "^0.1.0") (d #t) (k 0)))) (h "1s9ak6w8n9xfni2wi9m7h52n0l26lbhg5r4i404f9chnxlzvsabc")))

(define-public crate-rta-for-fps-latex-lib-0.1.1 (c (n "rta-for-fps-latex-lib") (v "0.1.1") (d (list (d (n "rta-for-fps-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0im8255ikxky0cw37ljcmmgz5r3gv2a63q4506j8xmja5ykvmra2")))

(define-public crate-rta-for-fps-latex-lib-0.2.0 (c (n "rta-for-fps-latex-lib") (v "0.2.0") (d (list (d (n "rta-for-fps-lib") (r "^0.2.0") (d #t) (k 0)))) (h "1aphjl79sghgjzxhszqmk9f63qf2lg9by398kqzc3b5yhm2qxwbx")))

