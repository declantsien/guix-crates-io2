(define-module (crates-io rt a- rta-for-fps-lib) #:use-module (crates-io))

(define-public crate-rta-for-fps-lib-0.1.0 (c (n "rta-for-fps-lib") (v "0.1.0") (h "1hpj6majzyf4q5iiqjrxff5arg88vm60cqi62z4fvqnbcswysigv")))

(define-public crate-rta-for-fps-lib-0.1.1 (c (n "rta-for-fps-lib") (v "0.1.1") (h "0kvmkhc9lj60xznp59f2yyl4nfqy0d2a556k5ssiig39n5w4yysa")))

(define-public crate-rta-for-fps-lib-0.2.0 (c (n "rta-for-fps-lib") (v "0.2.0") (h "02cr3p3bpvk2r9y4gv52h1r10d828lw5x1p81d00scdyzns3r4w3")))

