(define-module (crates-io rt -x rt-xenomai) #:use-module (crates-io))

(define-public crate-rt-xenomai-0.1.0 (c (n "rt-xenomai") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "realtime") (r "^0.1") (d #t) (k 0)))) (h "09sy9bdhb716ymw2p53y1mnqa1dgs0mbs6z32p5ypvjizjkqiqhj")))

(define-public crate-rt-xenomai-0.1.1 (c (n "rt-xenomai") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (k 0)) (d (n "realtime") (r "^0.1") (d #t) (k 0)))) (h "08gd7bgsqfpmj5vzfrw478ybbg8vsh09ib9p3gqjcnsyqkhca7lh")))

(define-public crate-rt-xenomai-0.1.2 (c (n "rt-xenomai") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "13sbm9rv0q9a3ckww8xza68pbljaq1y8f8nq05xlv669vakq0c5a")))

(define-public crate-rt-xenomai-0.1.3 (c (n "rt-xenomai") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "1nkb2zlbzhinss90g74gsj1bl43971p3k03aanwa9ghkmpwanr21")))

(define-public crate-rt-xenomai-0.1.7 (c (n "rt-xenomai") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "core_affinity") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "0jwpvvm649n4xpmbkvxil8abny42zdji0ndkxkci244i1cylx3ns")))

(define-public crate-rt-xenomai-0.1.8 (c (n "rt-xenomai") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "core_affinity") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "1q67cidw04kpckdnfqwkh989r84rn87h6q8r0kbrkc77423g0dbw")))

(define-public crate-rt-xenomai-0.1.11 (c (n "rt-xenomai") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "1hfwrclg9g8pxr4c3g63693dhn06ai9zvr355zx194ryy78p1k91")))

