(define-module (crates-io rt lo rtlola_kotlin_bridge) #:use-module (crates-io))

(define-public crate-rtlola_kotlin_bridge-0.1.0 (c (n "rtlola_kotlin_bridge") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (k 0)) (d (n "ordered-float") (r "^2.5.1") (d #t) (k 0)) (d (n "rtlola-frontend") (r "^0.4.3") (d #t) (k 0)) (d (n "rtlola-interpreter") (r "^0.6.0") (d #t) (k 0)))) (h "0mljqrdj12rfpx2ri0qqbp89dgd93m2bs2h8pcyhfsj5j0fxjzqj")))

(define-public crate-rtlola_kotlin_bridge-0.1.1 (c (n "rtlola_kotlin_bridge") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (k 0)) (d (n "ordered-float") (r "^2.5.1") (d #t) (k 0)) (d (n "rtlola-frontend") (r "^0.4.4") (d #t) (k 0)) (d (n "rtlola-interpreter") (r "^0.6.0") (d #t) (k 0)))) (h "0lkvc0wfz283vkhxlsaj37vabsbmcbg29pm8brsn25f5qjywwsmp")))

(define-public crate-rtlola_kotlin_bridge-0.2.0 (c (n "rtlola_kotlin_bridge") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "jni") (r "^0.19.0") (k 0)) (d (n "ordered-float") (r "^2.5.1") (d #t) (k 0)) (d (n "rtlola-frontend") (r "^0.4.4") (d #t) (k 0)) (d (n "rtlola-interpreter") (r "^0.6.0") (d #t) (k 0)))) (h "1msq7ing1xx6m144pnzg0wfs7j2rwj7l417pqzx9vf5r59wab1bb")))

