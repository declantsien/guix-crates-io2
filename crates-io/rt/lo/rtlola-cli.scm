(define-module (crates-io rt lo rtlola-cli) #:use-module (crates-io))

(define-public crate-rtlola-cli-0.1.0 (c (n "rtlola-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "humantime") (r "^2.0") (d #t) (k 0)) (d (n "rtlola-frontend") (r "^0.6.0") (d #t) (k 0)) (d (n "rtlola-input-plugins") (r "^0.1.0") (f (quote ("csv_plugin"))) (k 0)) (d (n "rtlola-interpreter") (r "^0.9.0") (d #t) (k 0)))) (h "0g031brpn5blwq2mpq1qk4ccgjb6fw75kqy3b650wr2y31n50biq") (f (quote (("public" "human-panic") ("pcap_interface" "rtlola-input-plugins/pcap_plugin"))))))

