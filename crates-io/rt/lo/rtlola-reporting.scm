(define-module (crates-io rt lo rtlola-reporting) #:use-module (crates-io))

(define-public crate-rtlola-reporting-0.1.0 (c (n "rtlola-reporting") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)))) (h "0rgndr3isva3w994hmvn9ibi4wyccq02gx2kqjji5hx2ri5ygl5w")))

(define-public crate-rtlola-reporting-0.1.1 (c (n "rtlola-reporting") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)))) (h "1hqcr8llkixhg4g87g9lbi76a3x0sw92fvv68w4ysv02y7grjcsh")))

(define-public crate-rtlola-reporting-0.2.0 (c (n "rtlola-reporting") (v "0.2.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "05pqbbhkl5rpzcwnvlbb5p0vm05hjxs1chzsw62bs6qynj06w3nh")))

(define-public crate-rtlola-reporting-0.2.1 (c (n "rtlola-reporting") (v "0.2.1") (d (list (d (n "codespan-reporting") (r "^0.11.1") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ln86ci8acxkzirwh5jr7hmn47aa3r0qafd4zwcdxngwxwy0r8y6")))

(define-public crate-rtlola-reporting-0.2.2 (c (n "rtlola-reporting") (v "0.2.2") (d (list (d (n "codespan-reporting") (r "^0.11.1") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jq8i2mbyybhlg0lmb3m1z5c1p5cvvazbygr3kaaj2ysp0fplf76")))

