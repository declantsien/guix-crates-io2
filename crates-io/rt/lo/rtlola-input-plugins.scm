(define-module (crates-io rt lo rtlola-input-plugins) #:use-module (crates-io))

(define-public crate-rtlola-input-plugins-0.1.0 (c (n "rtlola-input-plugins") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (o #t) (d #t) (k 0)) (d (n "etherparse") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "ip_network") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "pcap") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "rtlola-frontend") (r "^0.6.0") (d #t) (k 0)) (d (n "rtlola-interpreter") (r "^0.9.0") (d #t) (k 0)))) (h "09fwrdfvr6mp64yj4g9n6rbg6xq8vdbkmwc67c6nxbcn8zfbz2y7") (f (quote (("pcap_plugin" "pcap" "etherparse" "ip_network") ("default" "pcap_plugin" "csv_plugin") ("csv_plugin" "csv"))))))

