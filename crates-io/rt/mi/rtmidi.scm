(define-module (crates-io rt mi rtmidi) #:use-module (crates-io))

(define-public crate-rtmidi-0.1.0 (c (n "rtmidi") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1ppqx4404f243csv92g2cryddvpir1da59b4j3sh7d4iccv9k0yl")))

(define-public crate-rtmidi-0.2.0 (c (n "rtmidi") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "043hg0p8g14y6f093m6fyikg4nbkbk8prd9v6fxcwarsf1iny6ry")))

