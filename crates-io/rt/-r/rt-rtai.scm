(define-module (crates-io rt -r rt-rtai) #:use-module (crates-io))

(define-public crate-rt-rtai-0.1.0 (c (n "rt-rtai") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "realtime") (r "^0.1") (d #t) (k 0)))) (h "180215ii0xxfgaw62ixz6hyhy0lrqn2hb6i7yw6llkxs2vpjkbmw") (y #t)))

(define-public crate-rt-rtai-0.1.1 (c (n "rt-rtai") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (k 0)) (d (n "realtime") (r "^0.1") (d #t) (k 0)))) (h "0dik86y026bnwvnaffbn2fbjpi53m9x9k6ny6wzc0ppfjpg0xnhw") (y #t)))

(define-public crate-rt-rtai-0.1.2 (c (n "rt-rtai") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "109qrkwrbhkk7r1lj39iws78h97msv3zhrfda57maim8k4li0cd7") (y #t)))

(define-public crate-rt-rtai-0.1.3 (c (n "rt-rtai") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "0my5njwi8lp0y24kpvdpz75ijyljg0iwbc53ynm3frr64a2h0rwc") (y #t)))

(define-public crate-rt-rtai-0.1.4 (c (n "rt-rtai") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "1k0zb9wl6dwprn01z5idn697nhyhjgynyf3gnava48as7ddc38zj")))

(define-public crate-rt-rtai-0.1.5 (c (n "rt-rtai") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "122s1gkmz0yz52834h79zhgrknv3saiis7qgahw6067qcch1lj7x")))

(define-public crate-rt-rtai-0.1.6 (c (n "rt-rtai") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "0acxxpjizf1k8h2jdfmwlfl95yp9rhc65i0w0rcqja0bbr87md7b") (y #t)))

(define-public crate-rt-rtai-0.1.7 (c (n "rt-rtai") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "core_affinity") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "0ssgl8kv4lk0swy8fdw6vpd10ib8l3yb28pl4zc8jgf8s5w7g654")))

(define-public crate-rt-rtai-0.1.8 (c (n "rt-rtai") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "core_affinity") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "04gbcw4l0ygsr80nmlrkc20bx3ar4zkzivmva34pflp234ywdh72")))

(define-public crate-rt-rtai-0.1.9 (c (n "rt-rtai") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "core_affinity") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "0hqy4nivm68pzvac4la0h7dk8aks1vy6a3mih95ch9jlhdjvamy4")))

(define-public crate-rt-rtai-0.1.11 (c (n "rt-rtai") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)))) (h "1wxcv3q25yawgjmz6mwx5v5zr414kidwsmf61x84655p42x19v6f")))

