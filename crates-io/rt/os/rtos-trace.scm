(define-module (crates-io rt os rtos-trace) #:use-module (crates-io))

(define-public crate-rtos-trace-0.1.0 (c (n "rtos-trace") (v "0.1.0") (h "1l57raxlg4v55kngpfr7bw628ckl4r9rap8y44s75s4jkcqzam3n") (f (quote (("trace_impl") ("default" "trace_impl")))) (y #t)))

(define-public crate-rtos-trace-0.1.1 (c (n "rtos-trace") (v "0.1.1") (h "1wzxi60x8ydy0ijs2p2dxnp6ckz7j9phixhxlyqhgl0qp05idmgk") (f (quote (("trace_impl") ("default" "trace_impl")))) (y #t)))

(define-public crate-rtos-trace-0.1.2 (c (n "rtos-trace") (v "0.1.2") (h "18xbkjpmkfd9ynk6kg0rvikvsrcirfpzvfhdq2mzw2p9dp43clb3") (f (quote (("trace_impl") ("default" "trace_impl"))))))

(define-public crate-rtos-trace-0.1.3 (c (n "rtos-trace") (v "0.1.3") (h "15w0zcqnb9vakisvkscdy0vjm6y4wsi2y4br76dh9wkl6gnbrdnd") (f (quote (("trace_impl") ("default" "trace_impl"))))))

