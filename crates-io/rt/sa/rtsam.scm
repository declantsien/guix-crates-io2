(define-module (crates-io rt sa rtsam) #:use-module (crates-io))

(define-public crate-rtsam-0.1.1 (c (n "rtsam") (v "0.1.1") (d (list (d (n "alga") (r "^0.8.2") (d #t) (k 0)) (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "finitediff") (r "^0.1.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.17.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1qzhkf7xhfbzvx5v1vchjip5xqgw2h07ckhp5rvi7srfsjxr5b3d")))

(define-public crate-rtsam-0.1.2 (c (n "rtsam") (v "0.1.2") (d (list (d (n "alga") (r "^0.8.2") (d #t) (k 0)) (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "finitediff") (r "^0.1.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.17.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1bai7a15ljqisv3hw9r25csbgmy74vqwj9vgjayz6fvjgpvrpb09")))

(define-public crate-rtsam-0.1.4 (c (n "rtsam") (v "0.1.4") (d (list (d (n "alga") (r "^0.9.3") (d #t) (k 0)) (d (n "approx") (r "^0.5.0") (d #t) (k 0)) (d (n "finitediff") (r "^0.1.4") (d #t) (k 2)) (d (n "inkwell") (r "^0.1.0-beta4") (o #t) (d #t) (k 0)) (d (n "llvm-sys") (r "^130.0.0") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "typenum") (r "^1.14.0") (d #t) (k 0)))) (h "07hfgk3gvncaxw85zkmyyq2m76vi4w85k110zc9592290paxnh19") (f (quote (("default") ("codegen" "inkwell" "llvm-sys"))))))

