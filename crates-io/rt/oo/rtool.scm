(define-module (crates-io rt oo rtool) #:use-module (crates-io))

(define-public crate-rtool-0.1.0 (c (n "rtool") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.43") (d #t) (k 0)))) (h "1gqrp94fsinq26fjzbyzbbd02pzfzmgd3ac3gpmhswxngalr2cx5")))

