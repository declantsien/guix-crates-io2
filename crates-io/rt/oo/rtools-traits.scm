(define-module (crates-io rt oo rtools-traits) #:use-module (crates-io))

(define-public crate-rtools-traits-0.0.48 (c (n "rtools-traits") (v "0.0.48") (h "0pkdg48rwz5m5aizjzplbfjnkxn818ilhsyhnk1g37l9fmnw6bxr")))

(define-public crate-rtools-traits-0.0.49 (c (n "rtools-traits") (v "0.0.49") (h "18z1p6kg4g7rvmxj4srf8fpl57ld9msc5ld1rlzydvcf7c7bffbh")))

(define-public crate-rtools-traits-0.0.50 (c (n "rtools-traits") (v "0.0.50") (h "1rjjkh5i96bd9h6m5p3cpz370vjp4szdc4bmdgydahcb41ap0w1k")))

(define-public crate-rtools-traits-0.0.51 (c (n "rtools-traits") (v "0.0.51") (h "0104rmk3nmf4dfwmd79pm9xc2avy966xxx4847d6439kx6w42lkb")))

(define-public crate-rtools-traits-0.0.52 (c (n "rtools-traits") (v "0.0.52") (h "10dvv0rlzcar7mlkl3wrma4bsmv2vd8lmka453kgpy1dghh77v6l")))

