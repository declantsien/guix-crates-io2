(define-module (crates-io rt -h rt-history) #:use-module (crates-io))

(define-public crate-rt-history-1.0.0 (c (n "rt-history") (v "1.0.0") (d (list (d (n "atomic") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "testbench") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04x1y09jivnsy7in2fy899l9n84gni8amqhpdzqm2zglhh20sypv") (r "1.56")))

(define-public crate-rt-history-2.0.0 (c (n "rt-history") (v "2.0.0") (d (list (d (n "atomic") (r "^0.5") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "testbench") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j9hnli12hr5n95hmzf7alwgcyy19j3cxidihdqrig40qkajc0dn") (r "1.60")))

(define-public crate-rt-history-3.0.0 (c (n "rt-history") (v "3.0.0") (d (list (d (n "atomic") (r "^0.6") (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "testbench") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vpqjvkg4yf9nq1sd893pj5sqwk33ba6ryzwbkiyn3377k9c9a17") (r "1.64")))

