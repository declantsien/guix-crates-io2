(define-module (crates-io rt ls rtlsdr-full) #:use-module (crates-io))

(define-public crate-rtlsdr-full-0.1.0 (c (n "rtlsdr-full") (v "0.1.0") (h "10mhiwh3wqp9nlqhh9rm21ykx2x9z1m1fn9pbdkdrdcmg9i1pgi0")))

(define-public crate-rtlsdr-full-0.1.1 (c (n "rtlsdr-full") (v "0.1.1") (h "107wvgf99wqb9gr3kfg84q10ffqchjz94xrgg2k98sw2a5nw8gmr")))

(define-public crate-rtlsdr-full-0.1.2 (c (n "rtlsdr-full") (v "0.1.2") (h "0dz477mx851s860llw64n5ya6qwxgvkb2h0avx6749k5d53xg5lh")))

(define-public crate-rtlsdr-full-0.1.3 (c (n "rtlsdr-full") (v "0.1.3") (h "0x298ny3zwbsrxrwmip4xm5k7wcix3k7ndqffgp6d4kimrjn6szl")))

(define-public crate-rtlsdr-full-0.1.4 (c (n "rtlsdr-full") (v "0.1.4") (h "085mhq736fvjfn6xl3jrmsdh5ssh4hqn28i0x5wwyzmd0slvfm0a")))

(define-public crate-rtlsdr-full-0.1.5 (c (n "rtlsdr-full") (v "0.1.5") (h "0k13hax3m2x03jp6r60crbhayxzqv7lbq6l316b2762j6rn21mrq")))

(define-public crate-rtlsdr-full-0.1.6 (c (n "rtlsdr-full") (v "0.1.6") (h "15rhkdjznwjphbx3830jywmkjcg9cqgkpap20y88h1s0402b88j8")))

(define-public crate-rtlsdr-full-0.1.7 (c (n "rtlsdr-full") (v "0.1.7") (h "1c7rw22qjz6lbxpb8fp8m5msn0nn5mr94brwz3jqry3mj7f0mbpd")))

