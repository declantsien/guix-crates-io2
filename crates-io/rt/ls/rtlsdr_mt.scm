(define-module (crates-io rt ls rtlsdr_mt) #:use-module (crates-io))

(define-public crate-rtlsdr_mt-2.0.0 (c (n "rtlsdr_mt") (v "2.0.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "rtlsdr_sys") (r "^1.1.0") (d #t) (k 0)))) (h "10bxg6n4bvzzwvpkd7j7vmy5v354h4z6h89wbxyf7rrmnnc49l1j")))

(define-public crate-rtlsdr_mt-2.0.1 (c (n "rtlsdr_mt") (v "2.0.1") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "rtlsdr_sys") (r "^1.1.0") (d #t) (k 0)))) (h "1l35qszccxvpdazkw48v1ggxgj6zr8z60xdyan00spcm4grngk6c")))

(define-public crate-rtlsdr_mt-2.1.0-rc0 (c (n "rtlsdr_mt") (v "2.1.0-rc0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "rtlsdr_sys") (r "^1.1.0") (d #t) (k 0)))) (h "0wjzp1l13lkfc28h020cgvb1zmxw2ryzc6wjykls1prgi9x0d4f9")))

(define-public crate-rtlsdr_mt-2.1.0-rc1 (c (n "rtlsdr_mt") (v "2.1.0-rc1") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "rtlsdr_sys") (r "^1.1.0") (d #t) (k 0)))) (h "1r32sb37fnw7v1b217rlbb79z8492sqx0b12bb0c7x6hi3n8bsry")))

(define-public crate-rtlsdr_mt-2.1.0 (c (n "rtlsdr_mt") (v "2.1.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "rtlsdr_sys") (r "^1.1.0") (d #t) (k 0)))) (h "0d00bqlakbz59ww753gsgly07f1ryn52viisbj6im0360d5lpsv2")))

(define-public crate-rtlsdr_mt-2.2.0 (c (n "rtlsdr_mt") (v "2.2.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "rtlsdr_sys") (r "^1.1.0") (d #t) (k 0)))) (h "0njy848rq9l43pglvz64k8zbara4snr5vfrwva85315wyy5l5bby")))

