(define-module (crates-io rt ls rtlsdr_sys) #:use-module (crates-io))

(define-public crate-rtlsdr_sys-1.1.1 (c (n "rtlsdr_sys") (v "1.1.1") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "14zib6mr1d2a4swh2dgxzplmp4kgan3fh366pc2ps6rzhc8p7skg")))

(define-public crate-rtlsdr_sys-1.1.2 (c (n "rtlsdr_sys") (v "1.1.2") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0rss4pnb8gxhi1j4wm28zlkj9kp3n5g0slz7wh4x6rzbjdy7c42v")))

