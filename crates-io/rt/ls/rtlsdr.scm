(define-module (crates-io rt ls rtlsdr) #:use-module (crates-io))

(define-public crate-rtlsdr-0.0.1 (c (n "rtlsdr") (v "0.0.1") (h "13v8ynx0marqxfs82rkzvirlxkjg54dv53p6if6y52l03jnjza4z")))

(define-public crate-rtlsdr-0.1.0 (c (n "rtlsdr") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "04pf941xcwc6nckvgyqd5p4qppvvjpsmpk22qhsi1pn4zkbs42dn")))

(define-public crate-rtlsdr-0.1.1 (c (n "rtlsdr") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xh4fl43zmifwg9z4qfm38ggqjdjrx4nzwy9m2rdh669gdg2s8v9")))

(define-public crate-rtlsdr-0.1.2 (c (n "rtlsdr") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qbqp998ydfxj2s53bjrgg031jp2vfqpfqkl6s6dwi79rgnd2gc2")))

(define-public crate-rtlsdr-0.1.3 (c (n "rtlsdr") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fi566gk22dsg0fx743xyxkvhg5r6qpf0gm44p6ya43k28sxf28a")))

(define-public crate-rtlsdr-0.1.4 (c (n "rtlsdr") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gpvrp3pjkr464wf0jgc48383xm7ypcgw5kr4y71ip8fdgjaasqk")))

