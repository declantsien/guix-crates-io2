(define-module (crates-io rt s_ rts_derive) #:use-module (crates-io))

(define-public crate-rts_derive-0.1.0 (c (n "rts_derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1blk1qmhipwvr9xglfms49z0dn18kiwvsmpbpj9y88fljl7sl9m0")))

