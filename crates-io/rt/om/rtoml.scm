(define-module (crates-io rt om rtoml) #:use-module (crates-io))

(define-public crate-rtoml-0.1.0 (c (n "rtoml") (v "0.1.0") (h "0yiy87yn7qgqhwnn7786z8rfg1nqvzk6lphv6g0lkfp86a1mfj8k")))

(define-public crate-rtoml-0.1.1 (c (n "rtoml") (v "0.1.1") (h "0blk5ynsb567a36p6w8icdgwx3rs4n9l31zhi17xc4a9hfpwwpws")))

