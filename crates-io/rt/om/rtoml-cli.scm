(define-module (crates-io rt om rtoml-cli) #:use-module (crates-io))

(define-public crate-rtoml-cli-0.1.0 (c (n "rtoml-cli") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sensible-env-logger") (r "^0.3.2") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "07mxpi41ki3qc8hzr200wzwvnfczskf59agl3yinn22f22wr2i1q")))

