(define-module (crates-io rt cp rtcp) #:use-module (crates-io))

(define-public crate-rtcp-0.0.0 (c (n "rtcp") (v "0.0.0") (h "07q0n1xz3drnakg2n3g5x2wlalsdk78rixvazvi5da8nzii94032") (y #t)))

(define-public crate-rtcp-0.2.0 (c (n "rtcp") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1v47qhl8y0jmjkq7j5xgzls59lfj5ydk5400xqq0ql2gycrkqsfh")))

(define-public crate-rtcp-0.2.1 (c (n "rtcp") (v "0.2.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15mwiyvj6yh27lpr0v721cpyv8sxm5jmv9r22vcqz52an0ql9yzm")))

(define-public crate-rtcp-0.2.2 (c (n "rtcp") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "util") (r "^0.2.1") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "097js8j8nnb6646k1imh261a2qrigb54nzf9fhy8540a9h1akv7l")))

(define-public crate-rtcp-0.2.3 (c (n "rtcp") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "util") (r "^0.2.3") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1bjnxr9s550i80xg49fbb4rx7j3xcvvy0x5727aqis85ads6qjrx")))

(define-public crate-rtcp-0.2.4 (c (n "rtcp") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "util") (r "^0.2.4") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1mqgbkd7ilrdw5xcxndq37if8ipbfhnn6rnm6s4w5c9bp212ksn3")))

(define-public crate-rtcp-0.3.0 (c (n "rtcp") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "util") (r "^0.3.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1hayk20d6zfyqp6ciy6cbn05ya5587awns5fyxklkzs40kjr6phd")))

(define-public crate-rtcp-0.3.1 (c (n "rtcp") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "util") (r "^0.4.1") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "07q20vrgi2wql1ss0y5wjra6h8hyg74cxvj63qxbp11mq3wcppk2")))

(define-public crate-rtcp-0.3.3 (c (n "rtcp") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "util") (r "^0.4.2") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "15w72bshij2jx94kcpp4l1a9x566f4kbjkbds7508rj3cvsfbldq")))

(define-public crate-rtcp-0.3.4 (c (n "rtcp") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "util") (r "^0.4.3") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "0r4xvmyg2f46bq66zrblfv6lmmjg4b71xdxvcm4m5apkckyq2p02")))

(define-public crate-rtcp-0.4.0 (c (n "rtcp") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "util") (r "^0.4.3") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "0ng4j593jlaf66mg9khzh33slg0jznpy4q50j5igljvp50hhcqx0")))

(define-public crate-rtcp-0.4.1 (c (n "rtcp") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "util") (r "^0.4.3") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "0ylfxbsp8diybiifl9yyw1nd3yqbhxbp9cr8fqb8zwbng2x6kz02")))

(define-public crate-rtcp-0.5.0 (c (n "rtcp") (v "0.5.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "util") (r "^0.5.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "0f5j16gf9nj4dfmnvmfyj27ixqfrjac3i95jxp08p371f2xhv4dr")))

(define-public crate-rtcp-0.5.1 (c (n "rtcp") (v "0.5.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "util") (r "^0.5.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1qb3j9j69vbd1j910wl4flsgxx6p9cgzg9wkazwyikf0q7n6krxc")))

(define-public crate-rtcp-0.5.2 (c (n "rtcp") (v "0.5.2") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "util") (r "^0.5.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "088bv53cz4kpywcwms7xgmbiaydhw3wsh56z32hjz58si517j51j")))

(define-public crate-rtcp-0.6.0 (c (n "rtcp") (v "0.6.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "util") (r "^0.5.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1rrq7wnq9dfky0p266n08yfg1ngk32ks97n2gq57avib6zr9p1g1")))

(define-public crate-rtcp-0.6.1 (c (n "rtcp") (v "0.6.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "util") (r "^0.5.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1db5n48anglzqsaql0pf69bd3vyn1h9i18diwfb8cn39q61bprys")))

(define-public crate-rtcp-0.6.2 (c (n "rtcp") (v "0.6.2") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "util") (r "^0.5.2") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "13cislchcqbdsw20q9sm4cp3syqzqckqs0gswk50glvxlykzyzzc")))

(define-public crate-rtcp-0.6.3 (c (n "rtcp") (v "0.6.3") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "util") (r "^0.5.2") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1qark623gfvxj8s6rp2c38jvib15rnrxm3drz1abiv2lm35fl79q")))

(define-public crate-rtcp-0.6.4 (c (n "rtcp") (v "0.6.4") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "util") (r "^0.5.3") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "05i1jjp0cbq38w8jy9m8llx87kysbr1fpcipj1qqs5ld0sxfjw2j")))

(define-public crate-rtcp-0.6.5 (c (n "rtcp") (v "0.6.5") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "util") (r "^0.5.3") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "0mp1w6rp216lz3kg8q4yv953ck8m2anjdj0yf753md7d8xg65nc0")))

(define-public crate-rtcp-0.6.6 (c (n "rtcp") (v "0.6.6") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)) (d (n "util") (r "^0.5.4") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1lglmmq0n39qjjiqk0pxznvr25y05n6d1zsbhyax5a6bca6rvkq6")))

(define-public crate-rtcp-0.7.0 (c (n "rtcp") (v "0.7.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)) (d (n "util") (r "^0.6.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "13h3ijg9h1nnssbzfv9y6biyxl2z4myxas9mn9sggmi4qrcnvzbh")))

(define-public crate-rtcp-0.7.1 (c (n "rtcp") (v "0.7.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)) (d (n "util") (r "^0.7.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1hs59i1d3zj9cfy3jzac8aacjchr5rp4m7cy9bsxr63rwgkp24f1") (r "1.60.0")))

(define-public crate-rtcp-0.7.2 (c (n "rtcp") (v "0.7.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)) (d (n "util") (r "^0.7.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "149nhx9vay9gszzh7k4qf7qmkqxqkda8g54g709mva56skbfy68r") (r "1.60.0")))

(define-public crate-rtcp-0.8.0 (c (n "rtcp") (v "0.8.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "util") (r "^0.7.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1akksypmq3fj4yfrl9biz0xy7fgvxya6xbkdyskn25dpg91b9fr3") (r "1.63.0")))

(define-public crate-rtcp-0.9.0 (c (n "rtcp") (v "0.9.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "util") (r "^0.7.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1kck2aq7kdai97yhj2911zqs3qfbsly3ik7k4xnjf7120hw4j8v4") (r "1.63.0")))

(define-public crate-rtcp-0.10.0 (c (n "rtcp") (v "0.10.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "util") (r "^0.8") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1fcc913ficg1hqwajbkwzaylxbjga8r6m9d93p6b9v7vmn690xrn")))

(define-public crate-rtcp-0.10.1 (c (n "rtcp") (v "0.10.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "util") (r "^0.8.1") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1anqvkm9ggs1r9n6w9ass33jpqqpkzp5y9lyv1i6likl31w8lr1k")))

(define-public crate-rtcp-0.11.0 (c (n "rtcp") (v "0.11.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "util") (r "^0.9.0") (f (quote ("marshal"))) (k 0) (p "webrtc-util")))) (h "1ckaqwb7c9wfv0nsbwc8wrff52k8axygpfharkqffpwwz1gpg7zw")))

