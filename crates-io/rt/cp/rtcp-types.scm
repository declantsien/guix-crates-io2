(define-module (crates-io rt cp rtcp-types) #:use-module (crates-io))

(define-public crate-rtcp-types-0.0.1 (c (n "rtcp-types") (v "0.0.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19202m0zdd64zakmysxp8436fdaqzb4bsn1bxi6wq3pyr8sd9j2a")))

(define-public crate-rtcp-types-0.1.0 (c (n "rtcp-types") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i1qy7cm7jsrzkpqjrq1n13334qrdqgwsw2ihclxwr5lkm44hlnf")))

