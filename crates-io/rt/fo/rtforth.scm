(define-module (crates-io rt fo rtforth) #:use-module (crates-io))

(define-public crate-rtforth-0.1.26 (c (n "rtforth") (v "0.1.26") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1bffsxjnx19x6qwp4c6n8fcgzp7gi930mzxgl4a4b30ns4d7sy9f")))

(define-public crate-rtforth-0.1.39 (c (n "rtforth") (v "0.1.39") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rustyline") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "1blrfqbbf7gakwcq6bhn4q6hq26h475fdlba9z5ak0gx7akm74ph") (f (quote (("subroutine-threaded") ("primitive-centric") ("default"))))))

(define-public crate-rtforth-0.1.40 (c (n "rtforth") (v "0.1.40") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rustyline") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.15") (d #t) (k 0)))) (h "0zbcl1ggnpyahhhvcgs2nk2lhnk25y22lnhn6icmx26fc145izxc") (f (quote (("subroutine-threaded") ("primitive-centric") ("default"))))))

(define-public crate-rtforth-0.7.0 (c (n "rtforth") (v "0.7.0") (d (list (d (n "approx") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2.15") (d #t) (k 2)) (d (n "hibitset") (r "= 0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)) (d (n "region") (r "^1.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.18") (d #t) (k 0)))) (h "0dkwl6dhy41h72iafnvxhsf3ib3gwhiccqjr8sfr6ac30klq9ipp") (f (quote (("stc") ("default")))) (y #t)))

(define-public crate-rtforth-0.8.0 (c (n "rtforth") (v "0.8.0") (d (list (d (n "approx") (r "^0.2") (d #t) (k 0)) (d (n "contracts") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2.15") (d #t) (k 2)) (d (n "hibitset") (r "= 0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)) (d (n "region") (r "^1.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.18") (d #t) (k 0)))) (h "0qf4qm6s23xghgis9khp3ac1igvmxpl4vhlwylbnphdjjwj8r2ds") (y #t)))

(define-public crate-rtforth-0.9.0 (c (n "rtforth") (v "0.9.0") (d (list (d (n "approx") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2.15") (d #t) (k 2)) (d (n "hibitset") (r "= 0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)) (d (n "region") (r "^1.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.18") (d #t) (k 0)))) (h "0x08z8nca3xmm0bdjrpamzzmsif20g79wiynf5n0bzmiyhi8gbk7") (y #t)))

(define-public crate-rtforth-0.6.0 (c (n "rtforth") (v "0.6.0") (d (list (d (n "approx") (r "~0.3") (d #t) (k 0)) (d (n "hibitset") (r "~0.5.0") (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "uom") (r "~0.31.0") (d #t) (k 0)))) (h "0xzbzmhyv1crdnh2g38bnf8pmmm917xrrmf3d31idwzq7mc71vwn")))

(define-public crate-rtforth-0.6.2 (c (n "rtforth") (v "0.6.2") (d (list (d (n "approx") (r "~0.3") (d #t) (k 0)) (d (n "eframe") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "hibitset") (r "~0.5.0") (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "uom") (r "~0.31.0") (d #t) (k 0)))) (h "1if6siwypimznm19i6w9cla1f3rfpwxm4d4q8d0rjf69bpn9y84k") (f (quote (("gui" "eframe"))))))

(define-public crate-rtforth-0.6.3 (c (n "rtforth") (v "0.6.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "hibitset") (r "^0.6.3") (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "uom") (r "^0.33.0") (d #t) (k 0)))) (h "0ph32i1cwdx15dny5cdnmp9n7ikm9601cw8x0rnypgfl26yz7rqk")))

(define-public crate-rtforth-0.6.7 (c (n "rtforth") (v "0.6.7") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "hibitset") (r "^0.6.3") (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "uom") (r "^0.33.0") (d #t) (k 0)))) (h "1ry0v6z6b2ajlzqkr7lsr9lalb0jxjy6dy6iax7fd0b70zp3rdvr")))

(define-public crate-rtforth-0.6.8 (c (n "rtforth") (v "0.6.8") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "hibitset") (r "^0.6.3") (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "uom") (r "^0.33.0") (d #t) (k 0)))) (h "12vvc1i8ys5pcgf16c0nsa4gh23h917r7yhc3m256wsq48f45svz")))

