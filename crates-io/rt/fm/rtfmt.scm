(define-module (crates-io rt fm rtfmt) #:use-module (crates-io))

(define-public crate-rtfmt-0.1.0 (c (n "rtfmt") (v "0.1.0") (h "0gpmpmdzb2wddhjvlxw0i7lsidgzdzla071r15piq1gcpgzk0fif")))

(define-public crate-rtfmt-0.1.1 (c (n "rtfmt") (v "0.1.1") (h "1ry3fqzjmkshrjq5bip0inlivlvbxwgkzfghps4r8kv2xjm06mm5")))

