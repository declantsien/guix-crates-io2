(define-module (crates-io rt fm rtfm-syntax) #:use-module (crates-io))

(define-public crate-rtfm-syntax-0.1.0 (c (n "rtfm-syntax") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1fvwrdrzhbayyc7av0sc4x94nx678ddj746hia3i7mc3729sbm9n")))

(define-public crate-rtfm-syntax-0.2.0 (c (n "rtfm-syntax") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "18ff07dc7v7dlxhn748vfh04nwsyrpvwpg9v4vwdfsssmxww09rw")))

(define-public crate-rtfm-syntax-0.2.1 (c (n "rtfm-syntax") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "16fn35vy6chazlin541ca5zsw8rpgassj5cisp9lw9292d2k0yy8")))

(define-public crate-rtfm-syntax-0.3.0 (c (n "rtfm-syntax") (v "0.3.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ksywg2cc0gyf4dykizj1nz4i5p4wb3vdfb12m86lb7c6pwacs9j")))

(define-public crate-rtfm-syntax-0.3.1 (c (n "rtfm-syntax") (v "0.3.1") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ckxlfwppswy6cgb8c8rrf92d3xzm0a2hywh23cv73gnsklzh6bz")))

(define-public crate-rtfm-syntax-0.3.2 (c (n "rtfm-syntax") (v "0.3.2") (d (list (d (n "either") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1gvmclars87gaw227lcrlk43z4rk4hrsm6jbf5x1793khlrzy8bj")))

(define-public crate-rtfm-syntax-0.3.3 (c (n "rtfm-syntax") (v "0.3.3") (d (list (d (n "either") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "07pmcb6k4zm57lh79jaifjpnl9crqw6p8z1bsd1r7al0036xdjcw")))

(define-public crate-rtfm-syntax-0.3.4 (c (n "rtfm-syntax") (v "0.3.4") (d (list (d (n "either") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1gg6r81891wi0y50fdkzlj6015xfdr9zik7avam3bnrsimj4h142")))

(define-public crate-rtfm-syntax-0.4.0-beta.1 (c (n "rtfm-syntax") (v "0.4.0-beta.1") (d (list (d (n "compiletest_rs") (r "^0.3.22") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18qj5nccvjw29ld72fxjw3zc0dfd7zqj69iz8hpji334gnzlmdf5") (y #t)))

(define-public crate-rtfm-syntax-0.4.0-beta.2 (c (n "rtfm-syntax") (v "0.4.0-beta.2") (d (list (d (n "compiletest_rs") (r "^0.3.22") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1x6xqg3xcn9vim5g448vzbv0vypjhibnzqq9cq8cydqrid5pixlm") (y #t)))

(define-public crate-rtfm-syntax-0.4.0 (c (n "rtfm-syntax") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0vhiqk742cs62kk6p02as0hk7y57fr6ql8by9r2ncgfz6hyf4ma4")))

