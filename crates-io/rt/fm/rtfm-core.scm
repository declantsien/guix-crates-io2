(define-module (crates-io rt fm rtfm-core) #:use-module (crates-io))

(define-public crate-rtfm-core-0.1.0 (c (n "rtfm-core") (v "0.1.0") (d (list (d (n "static-ref") (r "^0.2.1") (d #t) (k 0)))) (h "17dwryw9c8flf9m854nanx597naah6aa8953gr4yvw7wcivn6yqs")))

(define-public crate-rtfm-core-0.2.0 (c (n "rtfm-core") (v "0.2.0") (h "0hdlpr04kpzpk6xkda307khnxqrkc4qkk7257qmpinwmm06l9fhi")))

(define-public crate-rtfm-core-0.3.0-beta.1 (c (n "rtfm-core") (v "0.3.0-beta.1") (h "1hgkgh54fqh55cywy10kkjkz1q98hibk1wcak9lcbfvfz3sgzdyl") (y #t)))

(define-public crate-rtfm-core-0.3.0-beta.2 (c (n "rtfm-core") (v "0.3.0-beta.2") (h "190znx9d297x6npin4nx03zgj2dgrapz8fvkm8928v45dbpa1g9b") (y #t)))

(define-public crate-rtfm-core-0.3.0 (c (n "rtfm-core") (v "0.3.0") (h "14rllrjshq2vrmkvnhprycfbnzialbz6z2cl1cr70nxanbnr7j4y")))

