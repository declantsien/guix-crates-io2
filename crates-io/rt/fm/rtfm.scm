(define-module (crates-io rt fm rtfm) #:use-module (crates-io))

(define-public crate-rtfm-0.1.0 (c (n "rtfm") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "opener") (r "^0.4.1") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1.1") (d #t) (k 0)))) (h "04n2yfsznsxjfcapx1n12x0yd85hxg5jzwjsc3yyq9dbqgx7lmn9")))

