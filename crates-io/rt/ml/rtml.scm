(define-module (crates-io rt ml rtml) #:use-module (crates-io))

(define-public crate-rtml-0.1.0 (c (n "rtml") (v "0.1.0") (h "0s1fzih7nlhak0i5dhdjh6x9ny4q653im7v7diddckkq8qrc4iwa")))

(define-public crate-rtml-0.2.0 (c (n "rtml") (v "0.2.0") (h "1dxajs5araca3dkkgi4y43myd9nvszrlmsx599kvp4j928mk0gax") (y #t)))

(define-public crate-rtml-0.2.1 (c (n "rtml") (v "0.2.1") (h "0bz71v4jscn3nvwcczi43bxjvxlh0xm7jniknll74fa7skxizaj8")))

(define-public crate-rtml-0.3.0 (c (n "rtml") (v "0.3.0") (h "0wnaay89pk68z4a787lin761ql30xz7bqh1f3rh0yxw1m939gqdm") (y #t)))

(define-public crate-rtml-0.3.1 (c (n "rtml") (v "0.3.1") (h "0siz452rqsry8dg7dpjb0mch2hmihpgdvnlzw7hirac9wwlbijvb") (y #t)))

(define-public crate-rtml-0.3.2 (c (n "rtml") (v "0.3.2") (h "152xvw4raqjv8mq1lkl7hqwvaffqd2f8g8vd64x3fql6f2g62gi7") (y #t)))

(define-public crate-rtml-0.4.0 (c (n "rtml") (v "0.4.0") (h "1xnb9s1wfh1zs42z10yrklpqqwwl1dskmm75c28c4h5yr535rxcb")))

(define-public crate-rtml-0.5.0 (c (n "rtml") (v "0.5.0") (h "1a4jlwvixp5c5ppjk8bpxycv1bvgnya2xnmlwvlmqfwdcw24maz8")))

(define-public crate-rtml-0.5.1 (c (n "rtml") (v "0.5.1") (h "1qhkga2k89gwzvhj99anzmz7n5dpzrm51rz1z321770c7ib413cy")))

(define-public crate-rtml-0.6.0 (c (n "rtml") (v "0.6.0") (h "1992j1ccg3scjc07dprvr2yapa4azhf3a58j5frbkqw45rh7798j")))

(define-public crate-rtml-0.7.0 (c (n "rtml") (v "0.7.0") (h "13l22mjlmvw2ja1ki3z8rxr34xd60wpqvc185jn445k53wr0fx1z")))

(define-public crate-rtml-0.8.0 (c (n "rtml") (v "0.8.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0gj0k3v1bbzd45y9qwgsyxjqzfv846zf13014lghiihzhr6rxd49")))

(define-public crate-rtml-0.8.1 (c (n "rtml") (v "0.8.1") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1ksl5p03vw1rjc0998rncjf9ribqmq3jflg7q37dwgnq7wha4wam")))

(define-public crate-rtml-0.9.0 (c (n "rtml") (v "0.9.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "12hcb2bpvp1lq41wqqisqsjj62wniz9fvfy0jprnm5p9nbk229pa")))

(define-public crate-rtml-0.9.1 (c (n "rtml") (v "0.9.1") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "195lph01ijsjj6ldc1g8wjag18ziyg3z640ll2fdnxd5zxyfwkg7")))

(define-public crate-rtml-0.9.2 (c (n "rtml") (v "0.9.2") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1f9y034zjaakrvxjqx97f1bhy9xc4vaqkbs20rqwpbxsy2v2hh2y")))

(define-public crate-rtml-0.10.0 (c (n "rtml") (v "0.10.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0wgx6x7hs56cd64x8p3xkyhmfmgrkg1knfp0223qs6w3wkzqs48h")))

(define-public crate-rtml-0.11.0 (c (n "rtml") (v "0.11.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0665j965g4lyji6i2x52v3kzvr8lrcm7zm0xvp056v8g04qf3q5a")))

(define-public crate-rtml-0.12.0 (c (n "rtml") (v "0.12.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1i888jivljcr99b83rkbrhl7zigmjmnfv70c1a5zngfphjagwdrn")))

(define-public crate-rtml-0.13.0 (c (n "rtml") (v "0.13.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "111188nmfzk2j0l87w8xwvskk9pc59x5ksq8jhys69zwlrxqccz0")))

(define-public crate-rtml-0.14.0 (c (n "rtml") (v "0.14.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0dp4qrid2mxvbyqkg5mjza1cpmjii3b46413qr9fmssazxafi31b")))

(define-public crate-rtml-0.15.0 (c (n "rtml") (v "0.15.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1fy5yz432dd5b9mpir3jjxv2v516d33a4yg28jg7j5gr5a2dh6pk")))

(define-public crate-rtml-0.16.0 (c (n "rtml") (v "0.16.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1nl6rvbljy7dl9cj6cx6gxz773k8g0a0qrj1h0v8pznw91j7mlys")))

(define-public crate-rtml-0.16.1 (c (n "rtml") (v "0.16.1") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0d7zapxy1ahhl3l5cx40sjxmy4n3llv6j11d7lk50p2gsbn0wzxn")))

(define-public crate-rtml-0.17.0 (c (n "rtml") (v "0.17.0") (d (list (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1z2j8yhl1nc86dfrrc4jld6vf42kabi5h7awp00cg2i5c75ix05p")))

