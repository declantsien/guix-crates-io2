(define-module (crates-io rt ic rtic-monotonic) #:use-module (crates-io))

(define-public crate-rtic-monotonic-0.1.0-alpha.0 (c (n "rtic-monotonic") (v "0.1.0-alpha.0") (d (list (d (n "embedded-time") (r "^0.10.1") (d #t) (k 0)))) (h "00cvkw8dflvndhmdfmmxk8n1rgxcxfkvbc20d6f98rwfbbah6fpc")))

(define-public crate-rtic-monotonic-0.1.0-alpha.1 (c (n "rtic-monotonic") (v "0.1.0-alpha.1") (d (list (d (n "embedded-time") (r "^0.11.0") (d #t) (k 0)))) (h "1nz1jvvdnkvb06i4331bapk49nnjn4cxwqqib6j513ag7w5cwfwr")))

(define-public crate-rtic-monotonic-0.1.0-alpha.2 (c (n "rtic-monotonic") (v "0.1.0-alpha.2") (d (list (d (n "embedded-time") (r "^0.12.0") (d #t) (k 0)))) (h "1f84crki5dmpvjg5qxc1myhmr8azfic0i7l3ps2d3rm761p2pgww")))

(define-public crate-rtic-monotonic-0.1.0-rc.1 (c (n "rtic-monotonic") (v "0.1.0-rc.1") (d (list (d (n "embedded-time") (r "^0.12.0") (d #t) (k 0)))) (h "19sbnf6pwb3s6s690067pk9dcd6rh8pn23abmwkb6s986zwandyk")))

(define-public crate-rtic-monotonic-0.1.0-rc.2 (c (n "rtic-monotonic") (v "0.1.0-rc.2") (h "0in9nqp903a4ci2x6dsx84dpicnah6iyhmv81x8pza7y9rkrs0by")))

(define-public crate-rtic-monotonic-1.0.0 (c (n "rtic-monotonic") (v "1.0.0") (h "10nfzs3wgg12l15xqf9mfvdr50vq9qfkma6fp5q68dhs5n10p2zv")))

