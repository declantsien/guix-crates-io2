(define-module (crates-io rt ic rtic-syntax) #:use-module (crates-io))

(define-public crate-rtic-syntax-0.4.0 (c (n "rtic-syntax") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "017zxvvfjmbyv6xq190qgwq2qk0li65m8w65dhgdc82phjmgqll1")))

(define-public crate-rtic-syntax-0.5.0-alpha.0 (c (n "rtic-syntax") (v "0.5.0-alpha.0") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "039ri4sqb1x3hlh1f1xps4bgd61ixkzpl1w3kwpim1xpi1dmdb8n")))

(define-public crate-rtic-syntax-0.5.0-alpha.1 (c (n "rtic-syntax") (v "0.5.0-alpha.1") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0w0y17gjclscvl9phxpdhggcb4yrrsr4107jjs8fcxbpa0pc01hr")))

(define-public crate-rtic-syntax-0.5.0-alpha.2 (c (n "rtic-syntax") (v "0.5.0-alpha.2") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0s5b16l377igqyhrs1kg5amj59krx53jd9s3m1c53jxbdw39wr4g")))

(define-public crate-rtic-syntax-0.5.0-alpha.3 (c (n "rtic-syntax") (v "0.5.0-alpha.3") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "18w26di5r3n8lw4dr87x160angax3bl2pksjn1id47cs9yk1sxlf")))

(define-public crate-rtic-syntax-0.5.0-alpha.4 (c (n "rtic-syntax") (v "0.5.0-alpha.4") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1vx8g9mpnkv6fnd3ipv9yn6fcm7mq2h1kzkyvgck2b8265dv8xkn")))

(define-public crate-rtic-syntax-0.5.0-rc.1 (c (n "rtic-syntax") (v "0.5.0-rc.1") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1ijcpaw0d5nnm8bg3fpxj4ik1mx52vbjxdhgkim5vaji9mf8nq3s")))

(define-public crate-rtic-syntax-0.5.0-rc.2 (c (n "rtic-syntax") (v "0.5.0-rc.2") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "193y7idk3al29ik2sk1pwbv4yqqcf69w0b2nw2v9rbyp3i1701pn")))

(define-public crate-rtic-syntax-1.0.0 (c (n "rtic-syntax") (v "1.0.0") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1qjbxivbir708r7ak858zqgkq3fjcxwfz0dv8bjfxnkp67zlkc94") (y #t)))

(define-public crate-rtic-syntax-1.0.1 (c (n "rtic-syntax") (v "1.0.1") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "04xpcscq7x5imm1y6n0rks6n7s9xvcsp81h9yqkcpnrsq3n0v9m3") (y #t)))

(define-public crate-rtic-syntax-1.0.2 (c (n "rtic-syntax") (v "1.0.2") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0r9ha16drvz5rdjcax4hjclkkrm79x6ncpv1cjqa3l6q7ljaxlrs")))

(define-public crate-rtic-syntax-1.0.3 (c (n "rtic-syntax") (v "1.0.3") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0y4lrgjbfnndmn9djsdmpb9g7ii2ls265p5xq997finw05b22pjz")))

