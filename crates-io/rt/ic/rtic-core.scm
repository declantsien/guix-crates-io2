(define-module (crates-io rt ic rtic-core) #:use-module (crates-io))

(define-public crate-rtic-core-0.3.0 (c (n "rtic-core") (v "0.3.0") (h "0n3ib40w4if69fwgjlfsqg15by5d3bwmkn5kd7w0bs0p4f1zwldb")))

(define-public crate-rtic-core-0.3.1 (c (n "rtic-core") (v "0.3.1") (h "1vx5jysfz945175y7d8vlr7zbf7p2fgqv8j6lfbzg3yy95lqmmcb")))

(define-public crate-rtic-core-1.0.0 (c (n "rtic-core") (v "1.0.0") (h "0hhsbkd34j6av2mn1g5cfyvlsqi5sai1xxf01rwa61jdn1ar6dnr")))

