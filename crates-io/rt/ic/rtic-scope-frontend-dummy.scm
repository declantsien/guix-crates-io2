(define-module (crates-io rt ic rtic-scope-frontend-dummy) #:use-module (crates-io))

(define-public crate-rtic-scope-frontend-dummy-0.1.0-alpha.0 (c (n "rtic-scope-frontend-dummy") (v "0.1.0-alpha.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rtic-scope-api") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1g783rpw4n27d3jc71xw3z4hchajmqk84jjljsz4klnfq1fpcmwf")))

(define-public crate-rtic-scope-frontend-dummy-0.1.0-alpha.1 (c (n "rtic-scope-frontend-dummy") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rtic-scope-api") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "05k7xfms5xh3l59wr5gjcjxci62jzi2fxqd584kbk0pxwx401yh9")))

