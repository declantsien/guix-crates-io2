(define-module (crates-io rt ic rtic-scope-api) #:use-module (crates-io))

(define-public crate-rtic-scope-api-0.1.0-alpha.0 (c (n "rtic-scope-api") (v "0.1.0-alpha.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itm-decode") (r "^0.4.0-alpha") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nl01fxd106z3h2cjs5593fvq7mkqycysq234ip5163y4dchqz6c")))

(define-public crate-rtic-scope-api-0.1.0-alpha.1 (c (n "rtic-scope-api") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itm-decode") (r "^0.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g0lz9kd7qnv9fg8j8viy9ds2584g315zzp7pk7z632lc2jsms53")))

