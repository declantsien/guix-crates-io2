(define-module (crates-io rt ic rtic-common) #:use-module (crates-io))

(define-public crate-rtic-common-1.0.0-alpha.0 (c (n "rtic-common") (v "1.0.0-alpha.0") (d (list (d (n "critical-section") (r "^1") (d #t) (k 0)))) (h "17vrl8sx7hm0mpwhi53l4m3iddhf36qafib91qzvkmr57mlk75p3") (f (quote (("testing" "critical-section/std") ("default"))))))

(define-public crate-rtic-common-1.0.0 (c (n "rtic-common") (v "1.0.0") (d (list (d (n "critical-section") (r "^1") (d #t) (k 0)))) (h "0p7k44innqzyyf1d38jxsh6g72xjbd0603q09aa2m7ggh45vb1h7") (f (quote (("testing" "critical-section/std") ("default"))))))

