(define-module (crates-io rt sp rtsp-rtp-rs) #:use-module (crates-io))

(define-public crate-rtsp-rtp-rs-0.1.33 (c (n "rtsp-rtp-rs") (v "0.1.33") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "openh264") (r "^0.4.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1bvqb7dknln66hzrc6dbk68ncs31kjawvri96xi14iwi9lg4rx82")))

(define-public crate-rtsp-rtp-rs-0.1.34 (c (n "rtsp-rtp-rs") (v "0.1.34") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "openh264") (r "^0.4.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "01ac6l8pw9l7p1ib9sbddd6r75hamxbfrfqclhqcfnhpx21y51a4")))

(define-public crate-rtsp-rtp-rs-0.1.35 (c (n "rtsp-rtp-rs") (v "0.1.35") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "openh264") (r "^0.4.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0nrbv3njjxcnkcakw3ckib6gpnx6qfyjsq4xd8ajcxij8a7zi80b")))

