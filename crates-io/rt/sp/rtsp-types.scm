(define-module (crates-io rt sp rtsp-types) #:use-module (crates-io))

(define-public crate-rtsp-types-0.0.0 (c (n "rtsp-types") (v "0.0.0") (h "06lnsq3h861i75gq8ymlyq22xcdb601paiypcjigz0h8yzvhjpa7")))

(define-public crate-rtsp-types-0.0.1 (c (n "rtsp-types") (v "0.0.1") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "11lfgi51iz9p814hi3bpvpvw95agvvbhh56x4f66mik7l0ppilxw")))

(define-public crate-rtsp-types-0.0.2 (c (n "rtsp-types") (v "0.0.2") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0v8x57d0zq2pdv5ipfmzb3hzrmzg045rp7xih48xc25jiii7grlh")))

(define-public crate-rtsp-types-0.0.3 (c (n "rtsp-types") (v "0.0.3") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0xagmf455fwrdcm8706arh32d0v96qjhlapv12f4f36hshi82rkv")))

(define-public crate-rtsp-types-0.0.4 (c (n "rtsp-types") (v "0.0.4") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0d8dmz0sayhamqq7k1ig9nd3qhdxfmzy11sxvpsdci15p95f1jyh")))

(define-public crate-rtsp-types-0.0.5 (c (n "rtsp-types") (v "0.0.5") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1byy5lm4ikj5cwb2y6v5n6n3j540f1bqhcj0q62yr3axvj8fq6ia")))

(define-public crate-rtsp-types-0.1.0 (c (n "rtsp-types") (v "0.1.0") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1f91rqg9w7jl2z8c0mizm3av3dxls0ly1agskd2vfkwg7cri874r")))

(define-public crate-rtsp-types-0.1.1 (c (n "rtsp-types") (v "0.1.1") (d (list (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0k27b6gv5j15prsryi0ywnnpnrqwvxyry0praxharwb3mzh12p3c") (r "1.58")))

