(define-module (crates-io rt sp rtsp-mock) #:use-module (crates-io))

(define-public crate-rtsp-mock-0.2.0 (c (n "rtsp-mock") (v "0.2.0") (d (list (d (n "glib") (r "^0.9.0") (d #t) (k 0)) (d (n "gstreamer") (r "^0.15.0") (d #t) (k 0)) (d (n "gstreamer-rtsp-server") (r "^0.15.0") (d #t) (k 0)))) (h "0lmbgajzi8ydzsr8kqcznfak9nd99krvzc08zs1xkd2msf701a3i")))

