(define-module (crates-io rt en rten-imageproc) #:use-module (crates-io))

(define-public crate-rten-imageproc-0.1.0 (c (n "rten-imageproc") (v "0.1.0") (d (list (d (n "rten-tensor") (r "^0.1.0") (d #t) (k 0)))) (h "1nphvvb421z81mgk5jc4bzwakcbc79i5xia2wbjjnz6sn8imjqhk")))

(define-public crate-rten-imageproc-0.3.0 (c (n "rten-imageproc") (v "0.3.0") (d (list (d (n "rten-tensor") (r "^0.3.0") (d #t) (k 0)))) (h "0zvljivyq9pqvvm9dr99hbb1qvniiw1z0lbfpig95f6wp0lapknr")))

(define-public crate-rten-imageproc-0.4.0 (c (n "rten-imageproc") (v "0.4.0") (d (list (d (n "rten-tensor") (r "^0.4.0") (d #t) (k 0)))) (h "0rvanxp8shcrpppl3zb6ah06rqwmaa3fs32mac6cqgr25h50w0if")))

(define-public crate-rten-imageproc-0.6.0 (c (n "rten-imageproc") (v "0.6.0") (d (list (d (n "rten-tensor") (r "^0.6.0") (d #t) (k 0)))) (h "1faarzx71lxip0msq5n389cr1ka4m0vwpzyqvmw467jr6s6fsg57")))

(define-public crate-rten-imageproc-0.7.0 (c (n "rten-imageproc") (v "0.7.0") (d (list (d (n "rten-tensor") (r "^0.7.0") (d #t) (k 0)))) (h "1ynawz2xfx64kjrxvydcwsgz21qp7drmbkicr4gsfvwv8hpzpyb3")))

(define-public crate-rten-imageproc-0.8.0 (c (n "rten-imageproc") (v "0.8.0") (d (list (d (n "rten-tensor") (r "^0.8.0") (d #t) (k 0)))) (h "0h3fnvcwkkdk0rxpl59awvz6gb6ilqv4ic7zgarwks4rh97gs9kd")))

(define-public crate-rten-imageproc-0.9.0 (c (n "rten-imageproc") (v "0.9.0") (d (list (d (n "rten-tensor") (r "^0.9.0") (d #t) (k 0)))) (h "0ldapqmhwxg86rg8ki7bickgp9wpxj2wgkxniyqfncl2bzrdx7sj")))

(define-public crate-rten-imageproc-0.10.0 (c (n "rten-imageproc") (v "0.10.0") (d (list (d (n "rten-tensor") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bi9wjw406d5mvx7izg7ar5m9kcdg7xmbzav8j82qawv4rvi19jv") (f (quote (("serde_traits" "serde"))))))

