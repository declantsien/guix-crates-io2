(define-module (crates-io rt en rten-vecmath) #:use-module (crates-io))

(define-public crate-rten-vecmath-0.1.0 (c (n "rten-vecmath") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)))) (h "1f7wvx9fmgs9lyf4cwshzp4h0dz2mchbvry36dk079i7156bg65m")))

(define-public crate-rten-vecmath-0.1.1 (c (n "rten-vecmath") (v "0.1.1") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)))) (h "030sbn4qyxajgpjb7p2lg8q0sqqmci72vfszls8xgaxjpycq1y7x")))

(define-public crate-rten-vecmath-0.3.0 (c (n "rten-vecmath") (v "0.3.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)))) (h "113f17kw99ff9170wfx840mbzpsi23kqx72znkd2985ynnw9ds0s")))

(define-public crate-rten-vecmath-0.4.0 (c (n "rten-vecmath") (v "0.4.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)))) (h "0zcnj5n32a3krp7sbjj90fbhpl6fanwryr251jqq6ffcax2na5vj")))

(define-public crate-rten-vecmath-0.6.0 (c (n "rten-vecmath") (v "0.6.0") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)))) (h "0lcznmfc45w74pjzd72ijizr01zcx8gmli32w04ayfzq4s67d6is") (f (quote (("avx512"))))))

(define-public crate-rten-vecmath-0.7.0 (c (n "rten-vecmath") (v "0.7.0") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)))) (h "1dpwbn4f13n99q1py42m5mglgpw38x1r770dgavj850ha16cnv1v") (f (quote (("avx512"))))))

(define-public crate-rten-vecmath-0.8.0 (c (n "rten-vecmath") (v "0.8.0") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)))) (h "1y4hl3qyci0w958fvnkcj23cx8i9811cjp1v9vbsgdx5412dd2gw") (f (quote (("avx512"))))))

(define-public crate-rten-vecmath-0.9.0 (c (n "rten-vecmath") (v "0.9.0") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)))) (h "03icianw3ljdl54zlyac083sghxaqydgf65l6z5633bnb7a4hps9") (f (quote (("avx512"))))))

(define-public crate-rten-vecmath-0.10.0 (c (n "rten-vecmath") (v "0.10.0") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (d #t) (k 2)) (d (n "rten-simd") (r "^0.10.0") (d #t) (k 0)))) (h "17rf6yvnl5r2c5yjybqg3sh446d02gk9bfmpxcndz8p7lx3crv2n") (f (quote (("avx512" "rten-simd/avx512"))))))

