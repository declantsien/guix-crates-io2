(define-module (crates-io rt en rten-cli) #:use-module (crates-io))

(define-public crate-rten-cli-0.4.0 (c (n "rten-cli") (v "0.4.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "rten") (r "^0.4.0") (d #t) (k 0)) (d (n "rten-tensor") (r "^0.4.0") (d #t) (k 0)))) (h "1zf6bnwjl6932rdpd8jcpva0mznniia32xy0qpxyc10pwdrvd96z")))

(define-public crate-rten-cli-0.5.0 (c (n "rten-cli") (v "0.5.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "rten") (r "^0.5.0") (d #t) (k 0)) (d (n "rten-tensor") (r "^0.4.0") (d #t) (k 0)))) (h "0rwn45d18ym7yp9zdpdz7wqf6zd4vihq662qyp3q5mdm1xpx0p2q")))

(define-public crate-rten-cli-0.6.0 (c (n "rten-cli") (v "0.6.0") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "rten") (r "^0.6.0") (f (quote ("random"))) (d #t) (k 0)) (d (n "rten-tensor") (r "^0.6.0") (d #t) (k 0)))) (h "1badlcbaq9k61z9j7djqh62iq7rx8rkns923gi3rf08ch83qn53p")))

(define-public crate-rten-cli-0.7.0 (c (n "rten-cli") (v "0.7.0") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "rten") (r "^0.7.0") (f (quote ("random"))) (d #t) (k 0)) (d (n "rten-tensor") (r "^0.7.0") (d #t) (k 0)))) (h "1zf62by2p8r8yimzy3z8vl0m2xk2cpkzjnbskvvqvkivl0qax7x0")))

(define-public crate-rten-cli-0.8.0 (c (n "rten-cli") (v "0.8.0") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "rten") (r "^0.8.0") (f (quote ("random"))) (d #t) (k 0)) (d (n "rten-tensor") (r "^0.8.0") (d #t) (k 0)))) (h "0d2qy9appi3l57z6hb3gq4nkh2b434421m61kq6zkgjplkhz61g9") (f (quote (("avx512" "rten/avx512"))))))

(define-public crate-rten-cli-0.9.0 (c (n "rten-cli") (v "0.9.0") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "rten") (r "^0.9.0") (f (quote ("random"))) (d #t) (k 0)) (d (n "rten-tensor") (r "^0.9.0") (d #t) (k 0)))) (h "1zscjpsqhz6ljjzhfl0jkf1mlqpldf1073y9qmsgdz4kywkbz05x") (f (quote (("avx512" "rten/avx512"))))))

(define-public crate-rten-cli-0.10.0 (c (n "rten-cli") (v "0.10.0") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "rten") (r "^0.10.0") (f (quote ("random"))) (d #t) (k 0)) (d (n "rten-tensor") (r "^0.10.0") (d #t) (k 0)))) (h "0i9r02fc2v0xm91g5cg081rzs4gzfr60542aqvzj1f49j8177d9k") (f (quote (("avx512" "rten/avx512"))))))

