(define-module (crates-io rt en rten-imageio) #:use-module (crates-io))

(define-public crate-rten-imageio-0.1.0 (c (n "rten-imageio") (v "0.1.0") (d (list (d (n "image") (r "^0.24.6") (f (quote ("png" "jpeg" "jpeg_rayon" "webp"))) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "rten-tensor") (r "^0.1.0") (d #t) (k 0)))) (h "0d9mwjk5b2wsadgzv62lcnc6r5za7dk9sl28favl6rj316433kff")))

(define-public crate-rten-imageio-0.3.0 (c (n "rten-imageio") (v "0.3.0") (d (list (d (n "image") (r "^0.24.6") (f (quote ("png" "jpeg" "jpeg_rayon" "webp"))) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "rten-tensor") (r "^0.3.0") (d #t) (k 0)))) (h "0q7ky4y4313k89y7046jk49l8p1rr4psv894yl0l9ys174zh6z40")))

(define-public crate-rten-imageio-0.4.0 (c (n "rten-imageio") (v "0.4.0") (d (list (d (n "image") (r "^0.24.6") (f (quote ("png" "jpeg" "jpeg_rayon" "webp"))) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "rten-tensor") (r "^0.4.0") (d #t) (k 0)))) (h "09idgvr1xnxhrlqs6z4m6y8nhrk0vzivcmzjjp79j6741v8gmwhr")))

(define-public crate-rten-imageio-0.6.0 (c (n "rten-imageio") (v "0.6.0") (d (list (d (n "image") (r "^0.24.6") (f (quote ("png" "jpeg" "jpeg_rayon" "webp"))) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "rten-tensor") (r "^0.6.0") (d #t) (k 0)))) (h "1ggfyi0paqiymdnn7745913gcywn6d555j5a7frgkw6h9dqah1kg")))

(define-public crate-rten-imageio-0.7.0 (c (n "rten-imageio") (v "0.7.0") (d (list (d (n "image") (r "^0.24.6") (f (quote ("png" "jpeg" "jpeg_rayon" "webp"))) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "rten-tensor") (r "^0.7.0") (d #t) (k 0)))) (h "051ncsmfhr7w74j7lp80caqrgxs1m0ikkyqlh67b5add770qz6f5")))

(define-public crate-rten-imageio-0.8.0 (c (n "rten-imageio") (v "0.8.0") (d (list (d (n "image") (r "^0.24.6") (f (quote ("png" "jpeg" "jpeg_rayon" "webp"))) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "rten-tensor") (r "^0.8.0") (d #t) (k 0)))) (h "063y06zk7gmmw3wk009r4msh34n7adix9kx596akq0qfv1qqmkz2")))

(define-public crate-rten-imageio-0.9.0 (c (n "rten-imageio") (v "0.9.0") (d (list (d (n "image") (r "^0.24.6") (f (quote ("png" "jpeg" "jpeg_rayon" "webp"))) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "rten-tensor") (r "^0.9.0") (d #t) (k 0)))) (h "1qmxn0ra6p1nm8p9cq7sqas5hrpyl1ifr4pdlydja72lj0hdb601")))

(define-public crate-rten-imageio-0.10.0 (c (n "rten-imageio") (v "0.10.0") (d (list (d (n "image") (r "^0.25.1") (f (quote ("png" "jpeg" "webp"))) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "rten-tensor") (r "^0.10.0") (d #t) (k 0)))) (h "13pxx949yf96y65qv2fy222gxagj3hfqfi01p48f0n7fvvvb6w01")))

