(define-module (crates-io rt en rten-text) #:use-module (crates-io))

(define-public crate-rten-text-0.1.0 (c (n "rten-text") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "1x91x45lpm7crs08h649bq9gipsj45w38a85by5s0yag192pxf5a")))

(define-public crate-rten-text-0.4.0 (c (n "rten-text") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "1dn5qmllia20lxyl251gmfh1pdwympsmklij5l3428mhwxd34wil")))

(define-public crate-rten-text-0.7.0 (c (n "rten-text") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "1h07midycndlikil4jdkgwx9frzp4s72s6wr1csjsl14861kqzyv")))

(define-public crate-rten-text-0.9.0 (c (n "rten-text") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "04jy5vac4s69aqn84xjwlnss4hh8dplw4fy9qg7nl57ghhza47yx")))

(define-public crate-rten-text-0.10.0 (c (n "rten-text") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.1") (d #t) (k 0)))) (h "09xqcvgqwfcy6hvrmn7hj5q262ld5qpsxyk7322jv6087inidwp5")))

