(define-module (crates-io rt en rten-tensor) #:use-module (crates-io))

(define-public crate-rten-tensor-0.1.0 (c (n "rten-tensor") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union" "const_generics" "const_new"))) (d #t) (k 0)))) (h "0m3m58jjcdzxgpyp28vb2531r983xxkavpcisaigrz7bh66ah6dp")))

(define-public crate-rten-tensor-0.3.0 (c (n "rten-tensor") (v "0.3.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union" "const_generics" "const_new"))) (d #t) (k 0)))) (h "1lph8c6cjcn0jhh3qr7iw5q6gvkf7zyak1vzfdbrj8fk0k55a2mh")))

(define-public crate-rten-tensor-0.4.0 (c (n "rten-tensor") (v "0.4.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union" "const_generics" "const_new"))) (d #t) (k 0)))) (h "0fjwsnp0k3vh0yd09hh2y2q0dpqavl0zx6gbdffj903jb72b555r")))

(define-public crate-rten-tensor-0.6.0 (c (n "rten-tensor") (v "0.6.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union" "const_generics" "const_new"))) (d #t) (k 0)))) (h "0h889z6z1da6x2h0aa87i43yn6j1qjc2lz63npqdxr54i8ifpn95")))

(define-public crate-rten-tensor-0.7.0 (c (n "rten-tensor") (v "0.7.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union" "const_generics" "const_new"))) (d #t) (k 0)))) (h "1x65c0rlaj6hgsivv31zix3qjk28mqw0bf9ymjpz86hfpj2frqa8")))

(define-public crate-rten-tensor-0.8.0 (c (n "rten-tensor") (v "0.8.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union" "const_generics" "const_new"))) (d #t) (k 0)))) (h "0ms3yza0w3v4w4ff47qv731w1pa7jvy8d0qgfcp4q0b9mzgl29ad")))

(define-public crate-rten-tensor-0.9.0 (c (n "rten-tensor") (v "0.9.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union" "const_generics" "const_new"))) (d #t) (k 0)))) (h "10fmikj59a550pxwqv1nh8xsxs9044h8znp9ccqs8dw3m60839zz")))

(define-public crate-rten-tensor-0.10.0 (c (n "rten-tensor") (v "0.10.0") (d (list (d (n "smallvec") (r "^1.10.0") (f (quote ("union" "const_generics" "const_new"))) (d #t) (k 0)))) (h "02cr10id1hm3ypfkwz54ic7ij9700yvl3sp4i5p77fs35qyybxaj")))

