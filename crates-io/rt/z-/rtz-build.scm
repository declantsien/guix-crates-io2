(define-module (crates-io rt z- rtz-build) #:use-module (crates-io))

(define-public crate-rtz-build-0.1.0 (c (n "rtz-build") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "rtz-core") (r "^0.1.0") (d #t) (k 0)))) (h "006yhaqsl269c6p8qims3aamxzjwncw6na8paw7mmwhw7f8zks0f") (f (quote (("tz-ned") ("self-contained" "reqwest") ("full" "tz-ned" "self-contained") ("force-rebuild") ("default"))))))

(define-public crate-rtz-build-0.2.0 (c (n "rtz-build") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "rtz-core") (r "^0.1.0") (d #t) (k 0)))) (h "0xrv2xj4phb8pvlfzk1d7z89cryd4j2d5cp4hba2bnbl4m2ryngq") (f (quote (("tz-ned") ("self-contained" "reqwest") ("full" "tz-ned" "self-contained") ("force-rebuild") ("default"))))))

(define-public crate-rtz-build-0.2.1 (c (n "rtz-build") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "rtz-core") (r "^0.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (o #t) (d #t) (k 0)))) (h "1h5p6r7mhmsxvynvb3k118w46r4m04aww5i7547frjnn59gznisl") (f (quote (("tz-osm" "zip") ("tz-ned") ("self-contained" "reqwest") ("full" "tz-ned" "self-contained") ("force-rebuild") ("default"))))))

(define-public crate-rtz-build-0.2.2 (c (n "rtz-build") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "rtz-core") (r "^0.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (o #t) (d #t) (k 0)))) (h "1x5y9lnd1rihp5b671l2wpw8d4xnh3vrwpn1fci6kgrafb6x91y4") (f (quote (("tz-osm" "zip") ("tz-ned") ("self-contained" "reqwest") ("full" "tz-ned" "self-contained") ("force-rebuild") ("default"))))))

(define-public crate-rtz-build-0.2.4 (c (n "rtz-build") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (o #t) (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "rtz-core") (r "^0.1.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (o #t) (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0mf6c6xqmllrxgi9vfcd2v9xr3357zhlk4k8g510m8wpdy7a18fi") (f (quote (("tz-osm") ("tz-ned") ("self-contained" "reqwest" "zip") ("full" "tz-ned" "self-contained") ("force-rebuild") ("default"))))))

(define-public crate-rtz-build-0.2.6 (c (n "rtz-build") (v "0.2.6") (d (list (d (n "rtz-core") (r "^0.1.4") (d #t) (k 0)))) (h "07xb1gjm1zqgi777sn08pcxn5sfybj008qip8mhz9qb4viwxd6yf") (f (quote (("tz-osm") ("tz-ned") ("self-contained") ("full" "tz-ned" "tz-osm" "admin-osm" "self-contained") ("force-rebuild") ("default") ("admin-osm"))))))

