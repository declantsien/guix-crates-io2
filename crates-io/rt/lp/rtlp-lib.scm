(define-module (crates-io rt lp rtlp-lib) #:use-module (crates-io))

(define-public crate-rtlp-lib-0.1.0 (c (n "rtlp-lib") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)))) (h "1kjk78gb2pmxwwxxv11xgc7j7zr2s0945jzq6xf0m481g689pzlv")))

(define-public crate-rtlp-lib-0.2.0 (c (n "rtlp-lib") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)))) (h "1rpdm0ahsbr42hkyrs4a6l42s4ir93s4z3n6y5dv93lfgbk7cjgb")))

(define-public crate-rtlp-lib-0.2.2 (c (n "rtlp-lib") (v "0.2.2") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)))) (h "11q7y5mbg1znhbsyvpfdhxa1rj61259dlg9b6f8kd6fsi85l7jn1")))

