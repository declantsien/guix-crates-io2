(define-module (crates-io rt or rtorrent_xml_rpc) #:use-module (crates-io))

(define-public crate-rtorrent_xml_rpc-0.1.0 (c (n "rtorrent_xml_rpc") (v "0.1.0") (h "1vh94qzlqbid3n020b629wm0zyk1aj1yqiqm56z3ffdmr4nmn3ji")))

(define-public crate-rtorrent_xml_rpc-0.1.1 (c (n "rtorrent_xml_rpc") (v "0.1.1") (h "1slnl84bqkb5ay1367b4s31p6g6byhnxmwkmfbs3fligkxyx8zi0")))

(define-public crate-rtorrent_xml_rpc-0.1.2 (c (n "rtorrent_xml_rpc") (v "0.1.2") (h "1vzcm9cjpvaihqi8rfq2a8idiwl7v1v4rc4q4m3pq5rl042x04cb")))

(define-public crate-rtorrent_xml_rpc-0.1.3 (c (n "rtorrent_xml_rpc") (v "0.1.3") (h "18cwldglnskvim6kf368zm61s4dcp3k4dqqc9f7ckwgybfzrcclj")))

(define-public crate-rtorrent_xml_rpc-0.1.4 (c (n "rtorrent_xml_rpc") (v "0.1.4") (h "0qa315adwfz94g1745pd0x51q9l5n63agvfmjarp3fbxh5vcfsvb")))

