(define-module (crates-io rt or rtorrent-utils) #:use-module (crates-io))

(define-public crate-rtorrent-utils-1.0.0 (c (n "rtorrent-utils") (v "1.0.0") (d (list (d (n "rtorrent-xmlrpc-bindings") (r "^1.0.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1f2ic72vngz87cy5d03wvgs5qv4c86g4nkk5y62x58q9vdhnylcq")))

(define-public crate-rtorrent-utils-1.0.1 (c (n "rtorrent-utils") (v "1.0.1") (d (list (d (n "rtorrent-xmlrpc-bindings") (r "^1.0.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0jbzzdl8f1q8yggdqjq7i7pwzzq2zkjlimn49w25dvc5m0jcnij0")))

(define-public crate-rtorrent-utils-1.0.2 (c (n "rtorrent-utils") (v "1.0.2") (d (list (d (n "rtorrent-xmlrpc-bindings") (r "^1.0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0xil66h3zzpf4zvmjzl9f0v5vr66hbgkh5j29jwxwqs82p8cvwjj")))

