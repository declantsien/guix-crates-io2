(define-module (crates-io rt or rtorrent-xmlrpc-bindings) #:use-module (crates-io))

(define-public crate-rtorrent-xmlrpc-bindings-0.1.0 (c (n "rtorrent-xmlrpc-bindings") (v "0.1.0") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "1mgbyxhdab19vl2qabayziqfx54b3apjfiz065igj9aaa1yz0xgj")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1.1 (c (n "rtorrent-xmlrpc-bindings") (v "0.1.1") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "0gaz7vkyzac957h4bj972xn8s2z6bcp85a0j5378gyy5mzsc7sbc")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1.2 (c (n "rtorrent-xmlrpc-bindings") (v "0.1.2") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "0x6qca7f77zdsfhhqmr0a490jdn9fykp6fqac3zzw3bc0yz4wqvy")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1.3 (c (n "rtorrent-xmlrpc-bindings") (v "0.1.3") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "0aky1gpplyxsaf7qbwvdwiy40irkpyx9azgvhm5973v29bxbfi4l")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1.4 (c (n "rtorrent-xmlrpc-bindings") (v "0.1.4") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "1li0w9ljb8fbxglbhmymydrmvqwh49fnz7fldharwmpzjpk9br3m")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1.5 (c (n "rtorrent-xmlrpc-bindings") (v "0.1.5") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "0kxp1v3296qdi92z3m5402w4wfqjfs64jb6jb1dch413zjpnxv6g")))

(define-public crate-rtorrent-xmlrpc-bindings-0.2.0 (c (n "rtorrent-xmlrpc-bindings") (v "0.2.0") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "00pi5ydbzkf9mxwlyi5p4in4zs05aahykcyiyyd8br4wa52y4m7z")))

(define-public crate-rtorrent-xmlrpc-bindings-0.2.1 (c (n "rtorrent-xmlrpc-bindings") (v "0.2.1") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "0yfkaqxkk16ihvn9l7zc1sf1a509vqghhbv78hamzzak2k1drpvm")))

(define-public crate-rtorrent-xmlrpc-bindings-1.0.0 (c (n "rtorrent-xmlrpc-bindings") (v "1.0.0") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "1rby1c5s3c4fmk3g3vnf6ic273w8wxl4hlp967zd0fk390h0z4iw")))

(define-public crate-rtorrent-xmlrpc-bindings-1.0.1 (c (n "rtorrent-xmlrpc-bindings") (v "1.0.1") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "1hxrk2544w8zscknmjpd666l0jfc951dw76vglmhqg30yvvwvsxp")))

(define-public crate-rtorrent-xmlrpc-bindings-1.0.2 (c (n "rtorrent-xmlrpc-bindings") (v "1.0.2") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "02a3q0lw2skqjjnmxcm449fx740n9s76c18vk413slwy6k75k317")))

(define-public crate-rtorrent-xmlrpc-bindings-1.0.3 (c (n "rtorrent-xmlrpc-bindings") (v "1.0.3") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "1fdx5qx5rindffnpg9qd4dvjixffajvxm8ak3a3l03nyb9kmjwfr")))

(define-public crate-rtorrent-xmlrpc-bindings-1.0.4 (c (n "rtorrent-xmlrpc-bindings") (v "1.0.4") (d (list (d (n "xmlrpc") (r "^0.15.0") (d #t) (k 0)))) (h "0id5a3kx18zg2f6z6ga5dg2pg9ywc8afl1rcccxd2p4xcxfgrfpl")))

