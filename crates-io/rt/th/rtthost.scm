(define-module (crates-io rt th rtthost) #:use-module (crates-io))

(define-public crate-rtthost-0.14.1 (c (n "rtthost") (v "0.14.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.14.1") (d #t) (k 0)) (d (n "probe-rs-rtt") (r "^0.14.1") (d #t) (k 0)))) (h "0fnyimzhnh2pgjfnyc32nqsfp0r64bl2hvbss0kialpsp2llzs6c")))

(define-public crate-rtthost-0.14.2 (c (n "rtthost") (v "0.14.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "probe-rs-rtt") (r "^0.14.2") (d #t) (k 0)))) (h "0qq36xh7rbvlkk2n84ql13cfa3374jdbyqccnlq7g3rnv9ygibc1")))

(define-public crate-rtthost-0.15.0 (c (n "rtthost") (v "0.15.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.15.0") (d #t) (k 0)))) (h "0n5isrvn82d56nlrg411kx34jnqn57x417qcc692pd35df6qisak")))

(define-public crate-rtthost-0.16.0 (c (n "rtthost") (v "0.16.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.16.0") (d #t) (k 0)))) (h "19ggp4hpy10lczv10qz8z6iqr1i0favaimbnv005r5zpd8xphww8")))

(define-public crate-rtthost-0.18.0 (c (n "rtthost") (v "0.18.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.18.0") (d #t) (k 0)))) (h "0sk9hn8xlnpq9ry6w7z54mh426ifdqaivqsr4dl3w1792kzs157i")))

(define-public crate-rtthost-0.19.0 (c (n "rtthost") (v "0.19.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.19.0") (d #t) (k 0)))) (h "10sydygb98a2wlw7bscmyblqfydf96l3qcpphwjq4jiqr97pjrsw")))

(define-public crate-rtthost-0.20.0 (c (n "rtthost") (v "0.20.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.20.0") (d #t) (k 0)))) (h "1b9zgvkww7mfbgr8k8bgkw4pbr09pb8bdxbp3n324gggbh8dmzk7")))

(define-public crate-rtthost-0.21.0 (c (n "rtthost") (v "0.21.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.21.0") (d #t) (k 0)))) (h "0mg6bc1nqnw1c9x9hnrg0naz533vp7mwbdyqwqfjpadr40lbna7w")))

(define-public crate-rtthost-0.21.1 (c (n "rtthost") (v "0.21.1") (d (list (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.21.1") (d #t) (k 0)))) (h "0py0d5bjlxskn39wk7y85li1m4c0jdh4z0mbz0b3pik2hyqb6zcy")))

(define-public crate-rtthost-0.22.0 (c (n "rtthost") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "probe-rs") (r "^0.22.0") (d #t) (k 0)))) (h "0q1qq07aviw9abjykp7mdklss2p65p8v4yvdj9a8ksd3izjnnjla")))

(define-public crate-rtthost-0.23.0 (c (n "rtthost") (v "0.23.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "probe-rs") (r "^0.23.0") (d #t) (k 0)))) (h "0pwzlnihm5xcd64g6qnik1apx3p2jzwxr2sv8xynii13xnbki03i")))

(define-public crate-rtthost-0.24.0 (c (n "rtthost") (v "0.24.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "probe-rs") (r "^0.24.0") (d #t) (k 0)))) (h "0v5nldk5s9g8cvksrwbvkvhnz1hlg62m7kl34nffmc6f7fqjnfwq")))

