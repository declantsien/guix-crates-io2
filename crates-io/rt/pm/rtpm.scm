(define-module (crates-io rt pm rtpm) #:use-module (crates-io))

(define-public crate-rtpm-0.0.1 (c (n "rtpm") (v "0.0.1") (d (list (d (n "cargo") (r "^0.66.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.16.1") (f (quote ("https"))) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "05n59gd7i9cdmj6x19c073vqykasd4cxv0l732zad7bbz939cvh4")))

