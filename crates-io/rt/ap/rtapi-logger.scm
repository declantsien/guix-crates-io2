(define-module (crates-io rt ap rtapi-logger) #:use-module (crates-io))

(define-public crate-rtapi-logger-0.1.0 (c (n "rtapi-logger") (v "0.1.0") (d (list (d (n "linuxcnc-hal-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "19bx57r3bcmpcddnnbsav1iqg84yk2rl70ja12z9fb0i37y3y743")))

(define-public crate-rtapi-logger-0.2.0 (c (n "rtapi-logger") (v "0.2.0") (d (list (d (n "linuxcnc-hal-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0irdp2jjsgz1agbqh6j11bg7b9xv16f495gxqkn1fxnlm2zgbrhv")))

