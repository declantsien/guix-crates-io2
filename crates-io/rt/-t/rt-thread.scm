(define-module (crates-io rt -t rt-thread) #:use-module (crates-io))

(define-public crate-rt-thread-0.1.0 (c (n "rt-thread") (v "0.1.0") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "realtime") (r "^0.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.8.0") (d #t) (k 0)))) (h "03784iv2fhkc48ijl2fyfmw48w1hji37varh50x1m6ixmz2h3rjj")))

(define-public crate-rt-thread-0.1.1 (c (n "rt-thread") (v "0.1.1") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "realtime") (r "^0.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.8.0") (d #t) (k 0)))) (h "1yfz9my761adinyafp12p0l1vd3qicw4gbngcama4rw175wqgwah")))

(define-public crate-rt-thread-0.1.2 (c (n "rt-thread") (v "0.1.2") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.8.0") (d #t) (k 0)))) (h "1cp6yw6x4zzfpzg12xd1sb53qpv3gg5004jq38zdip5grlgs29ap")))

(define-public crate-rt-thread-0.1.3 (c (n "rt-thread") (v "0.1.3") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.24.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.9.2") (d #t) (k 0)))) (h "059bmgv4jdfxkq6niqcrslcbx05r2bs0pzdc5aa8m7cs77gswvyd")))

(define-public crate-rt-thread-0.1.4 (c (n "rt-thread") (v "0.1.4") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.13") (d #t) (k 0)))) (h "1ms0iqmnja01gcqn4czngnpdvain79mdv9xrg3b7m8aqmi0yf15h")))

(define-public crate-rt-thread-0.1.5 (c (n "rt-thread") (v "0.1.5") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.13") (d #t) (k 0)))) (h "0p842lnjp8ygp0vhhnwfhr7abyrj6ssia6by6fvwrqmk681gkaz1") (y #t)))

(define-public crate-rt-thread-0.1.7 (c (n "rt-thread") (v "0.1.7") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "core_affinity") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.13") (d #t) (k 0)))) (h "1hak35b0ffxbvza2l5rf6z119wjrydcjlj1bq2swb5n1n9vyyws1")))

(define-public crate-rt-thread-0.1.8 (c (n "rt-thread") (v "0.1.8") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "core_affinity") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.13") (d #t) (k 0)))) (h "0ywd4fyjf45v5j9f4vl8zfw65k2qr4drq2dwciz9j4qyq9z3z3yb")))

(define-public crate-rt-thread-0.1.9 (c (n "rt-thread") (v "0.1.9") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "core_affinity") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.13") (d #t) (k 0)))) (h "1h0azb7mrkf00yvx20gyyl9fhi4afgd5ziczmgrb2jh3ayv782rk")))

(define-public crate-rt-thread-0.1.11 (c (n "rt-thread") (v "0.1.11") (d (list (d (n "clock_source") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "realtime-core") (r "^0.1") (d #t) (k 0)) (d (n "thread-priority") (r "^0.13") (d #t) (k 0)))) (h "0jjv89s3z4c1ii42bmys69f2dwvqyh4mfi53bmm9bc7sfjbz6hkc")))

