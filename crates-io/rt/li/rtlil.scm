(define-module (crates-io rt li rtlil) #:use-module (crates-io))

(define-public crate-rtlil-0.1.0 (c (n "rtlil") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13wlaglmhbhywiirwwyckcsxc6hr49aflap010ia6wgpxgjaz381")))

(define-public crate-rtlil-0.1.1 (c (n "rtlil") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qs77w2ilpkdaradw42h2954afwzcgjfnr0an6k6fs9b20mwxix0")))

