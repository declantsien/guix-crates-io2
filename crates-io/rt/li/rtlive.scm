(define-module (crates-io rt li rtlive) #:use-module (crates-io))

(define-public crate-rtlive-0.1.0 (c (n "rtlive") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0dhvnw283wl8w3d3s02vnna584x1mh8fwdjffifq3dp7hmf82wvc")))

