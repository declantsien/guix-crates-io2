(define-module (crates-io rt rb rtrb) #:use-module (crates-io))

(define-public crate-rtrb-0.1.0 (c (n "rtrb") (v "0.1.0") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1xm1ycgr483r9azd0lrx57ighnkqv58abn7b1rwrw2c52vs0g72a") (y #t)))

(define-public crate-rtrb-0.1.1 (c (n "rtrb") (v "0.1.1") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0g05qayxaya4gyic33f9sjzfmaj2lksiwyvy11xylcl3mfbpgfll")))

(define-public crate-rtrb-0.1.2 (c (n "rtrb") (v "0.1.2") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "09m45d8iiqzianmnychji019h2d1adxnpqkd1zm9k3mjqcndx9p2")))

(define-public crate-rtrb-0.1.3 (c (n "rtrb") (v "0.1.3") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0mfqpjl79dvb18shxmrivm76f30cmm1c11l6lj61bi0hj70b3qai") (f (quote (("std") ("default" "std"))))))

(define-public crate-rtrb-0.1.4 (c (n "rtrb") (v "0.1.4") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0pr7wlmbn2d9ir73igva94bypmwbr9fvlf83l541mqgp0an5d0ii") (f (quote (("std") ("default" "std"))))))

(define-public crate-rtrb-0.2.0 (c (n "rtrb") (v "0.2.0") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0n442vl7lqvn0wj6bblfrwbf6cam2sibvrhqs66mjnklx611v4bx") (f (quote (("std") ("default" "std"))))))

(define-public crate-rtrb-0.2.1 (c (n "rtrb") (v "0.2.1") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "021kbzsmk7lb3i186d4fjrv9hyab7k92dqxhq61fzh8bpwqd27h4") (f (quote (("std") ("default" "std"))))))

(define-public crate-rtrb-0.2.2 (c (n "rtrb") (v "0.2.2") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "17414h7fgbdrjymw6pp0512x20z9kf0h21n1imcqi0vdrw8fw9lh") (f (quote (("std") ("default" "std"))))))

(define-public crate-rtrb-0.2.3 (c (n "rtrb") (v "0.2.3") (d (list (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "02cih5wqlmkci9sb38faq43xcdmpy1q0y510lck27bsg23fh9rwr") (f (quote (("std") ("default" "std"))))))

(define-public crate-rtrb-0.3.0 (c (n "rtrb") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1glg50ih1sf1a4bsks22z6q6z9lasf2507p4y3bsscnvba5j4sv3") (f (quote (("std") ("default" "std")))) (r "1.38")))

(define-public crate-rtrb-0.3.1 (c (n "rtrb") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0hs1n6jx0sk3yba905bldncbjhl8z8i4gd0j81fviwvkq224xygk") (f (quote (("std") ("default" "std")))) (r "1.38")))

