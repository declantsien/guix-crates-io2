(define-module (crates-io rt rb rtrb-basedrop) #:use-module (crates-io))

(define-public crate-rtrb-basedrop-0.1.0 (c (n "rtrb-basedrop") (v "0.1.0") (d (list (d (n "basedrop") (r "^0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0h4pk28drxml0kpajg53r6k8vfrvf536pc2swfc7r7khv9ddbw0a") (f (quote (("std") ("default" "std"))))))

(define-public crate-rtrb-basedrop-0.1.1 (c (n "rtrb-basedrop") (v "0.1.1") (d (list (d (n "basedrop") (r "^0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "07vdspcmhqg0zn7m317z2hp24a9plcsd5d1pw0f4qz6n5r37drnb") (f (quote (("std") ("default" "std"))))))

(define-public crate-rtrb-basedrop-0.1.2 (c (n "rtrb-basedrop") (v "0.1.2") (d (list (d (n "basedrop") (r "^0.1") (d #t) (k 0)) (d (n "cache-padded") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0z7c5l96zfcfwnh11la5bzqfq07r2q74y9i4dcvy6vcq9h3wl4kf") (f (quote (("std") ("default" "std"))))))

