(define-module (crates-io rt re rtrees) #:use-module (crates-io))

(define-public crate-rtrees-0.1.0 (c (n "rtrees") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1cs1n5klv4d1ibqzpbsvbn9jjm9fk0z8ayciychfsjg4c551id76") (f (quote (("serialize" "serde") ("default"))))))

