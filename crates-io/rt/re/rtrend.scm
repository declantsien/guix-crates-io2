(define-module (crates-io rt re rtrend) #:use-module (crates-io))

(define-public crate-rtrend-0.1.0 (c (n "rtrend") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sayy6iskp5r7903434km1sbm52j2gf414asfwzh6r73hicvx6k9")))

(define-public crate-rtrend-0.1.1 (c (n "rtrend") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a4b5b9cz044j63agw0brq8224zvhvk8gnhdqaabz9apmkglhk5i")))

(define-public crate-rtrend-0.1.2 (c (n "rtrend") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v3z90idzadlv7a4gkzb9mqxwmj55ap1phahh5xb2i06c51xd5i6")))

(define-public crate-rtrend-0.1.3 (c (n "rtrend") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "07j4b42vf8jvrpjlm6nh0jzcd3kkg4mv9qskrj2jsdidg4cyv1qn")))

(define-public crate-rtrend-0.1.4 (c (n "rtrend") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "04qbwhps44n7f1651dha21v9c1gvlhrbl313xr0pw6zz80i6paax")))

(define-public crate-rtrend-0.1.5 (c (n "rtrend") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("cookies" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "1hrjb9ylxgibfdr0mfa4cagc1id3l447cpma8rbyc7hcl7nq6729")))

(define-public crate-rtrend-0.1.6 (c (n "rtrend") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("cookies" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)))) (h "0w5kd2y9w5lx41bcikgznn2d8h7h0z6cmvmx619r7z56iq6m03ar")))

(define-public crate-rtrend-0.1.7 (c (n "rtrend") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "blocking" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "16nd50whg586gwkc4f5kq4hppjdx27jg6qbyz86k0rxwwmqfljs4")))

