(define-module (crates-io rt re rtreec) #:use-module (crates-io))

(define-public crate-rtreec-0.1.0 (c (n "rtreec") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0v5rvz3xwl89arf6bb2vs5gphsyjmivya0r4s4073464scchg78z")))

