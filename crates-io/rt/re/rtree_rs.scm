(define-module (crates-io rt re rtree_rs) #:use-module (crates-io))

(define-public crate-rtree_rs-0.1.0 (c (n "rtree_rs") (v "0.1.0") (d (list (d (n "pqueue") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1y5mvlk8qdnmxhki49354nq3zavd6167yqhjl0r8r7micvp63342")))

(define-public crate-rtree_rs-0.1.1 (c (n "rtree_rs") (v "0.1.1") (d (list (d (n "pqueue") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1wj7lzlzj5i70fdi156bvhvppshk4vnv1zwyxrbzrk45289pmk5i")))

(define-public crate-rtree_rs-0.1.2 (c (n "rtree_rs") (v "0.1.2") (d (list (d (n "pqueue") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qj4q784vriblipsad2i5n6a8p332wgzbvnawcncyjkcj6lfrbr9")))

(define-public crate-rtree_rs-0.1.3 (c (n "rtree_rs") (v "0.1.3") (d (list (d (n "pqueue") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1w9bjspxppg1rr4rngkv4dscxzc3j51sliqfxpy1nx2fgqszgr8m")))

(define-public crate-rtree_rs-0.1.4 (c (n "rtree_rs") (v "0.1.4") (d (list (d (n "pqueue") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0vmra69g6gvgvn53c7f4apn2l5n7p5b7vsxqsrgnf74cjw79d1vp")))

