(define-module (crates-io rt ri rtrie) #:use-module (crates-io))

(define-public crate-rtrie-0.1.0 (c (n "rtrie") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "100s583ss1fqsq9a42qbzx7cnwzq97khsl0mn9gay1gmgkkjv0kq")))

(define-public crate-rtrie-0.1.1 (c (n "rtrie") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1f0aavdi5lrskgr3wjiy01m2532lzyx2xnax56564fhzcaa25fm3")))

