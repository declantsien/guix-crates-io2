(define-module (crates-io rt ri rtriangulate) #:use-module (crates-io))

(define-public crate-rtriangulate-0.1.0 (c (n "rtriangulate") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)))) (h "1h9warc0c0qykvij30qzdbig8yxb3abrnh4f9yxflv3fbm5s8vzd")))

(define-public crate-rtriangulate-0.1.1 (c (n "rtriangulate") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)))) (h "1bmr66kxw956vq1n16v2g0xmcz7fzmx360mx1hwhkcy7v5w7vhq3")))

(define-public crate-rtriangulate-0.1.2 (c (n "rtriangulate") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)))) (h "15sbl3j3m7k09rnxjvx8fiyr59m7hmbhz4dxi3ya32x6p9alvfz1")))

(define-public crate-rtriangulate-0.2.0 (c (n "rtriangulate") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)))) (h "0rqnf8nhpg3s0akj3wp78y753sdns0xx0jk4f4hf8f92i44r1m2l")))

(define-public crate-rtriangulate-0.2.1 (c (n "rtriangulate") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)))) (h "1jpxr7m78wcb3smqw8cfbcl1axdsy07gyrdsrl2jlrn1x7331d8b")))

(define-public crate-rtriangulate-0.3.0 (c (n "rtriangulate") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1jskrhj7pb80an009x3f5751q2920phdcrkjd4abvhvs5h16jz4a")))

(define-public crate-rtriangulate-0.3.1 (c (n "rtriangulate") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "15qnbkzzalf5fd4xc4v27jq8z7g6nx3azd9dxzqmw42wdf5bsdy7")))

