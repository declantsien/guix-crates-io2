(define-module (crates-io rt d- rtd-tutorial) #:use-module (crates-io))

(define-public crate-rtd-tutorial-0.1.0 (c (n "rtd-tutorial") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wjy1rzj9bn5j7613dc2cm408wrf66s7y1dfkf7lmwb8j6lpwz02")))

(define-public crate-rtd-tutorial-0.1.1 (c (n "rtd-tutorial") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "140pbmp9aqlyk7wl6216zl3h0sd0xmi7rcxwcg165pjz85rqq7kx")))

(define-public crate-rtd-tutorial-0.1.2 (c (n "rtd-tutorial") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vs3pjbxddps7fhaf4lp6qg1bdyr6hb3fhi0mlaqwf09ynrmf0jv")))

(define-public crate-rtd-tutorial-0.1.3 (c (n "rtd-tutorial") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fz236rgnn09qzcgz4z7pbd7j85swsba3g6v3s0b0h5h20hdzjrk")))

(define-public crate-rtd-tutorial-0.1.4 (c (n "rtd-tutorial") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sb2hskjpb9mk3c6gz8cj3m2ij3i4z7062fk6i2bgy7yq4qqmdd3")))

(define-public crate-rtd-tutorial-0.1.5 (c (n "rtd-tutorial") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hgzp6nyajrxvqayccdd82cf1w64gspn092gm3i0y3p6nnw6dfcy")))

(define-public crate-rtd-tutorial-0.1.6 (c (n "rtd-tutorial") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "18lg3iza77k6m96nhik3y2f8grgs3jkjj1ch0gcd0bph4gqkp5bb")))

(define-public crate-rtd-tutorial-0.1.7 (c (n "rtd-tutorial") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nbhh4hl2grg38bagia8ry7w8hxwpfjfq1lc9q2pjgc98b7lnvg0")))

(define-public crate-rtd-tutorial-0.1.8 (c (n "rtd-tutorial") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "14gbkinnmkzqf5jrrg3sxw3h3xck195pal0qlw9hacgwdkas1f6k")))

(define-public crate-rtd-tutorial-0.1.9 (c (n "rtd-tutorial") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gd3r8xviv0lclpj7pb23vvzx4hz83km8myqgingpr97qcsmqxw1")))

(define-public crate-rtd-tutorial-0.1.10 (c (n "rtd-tutorial") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xm0aprr0yk0ay38nnl374wwhacy9kbc12lj78k4d8a2bcl4fygy")))

(define-public crate-rtd-tutorial-0.1.11 (c (n "rtd-tutorial") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ly3r5mp7x8h96i55ynbymv9lj86j9cqc6d75fyd622clmw5lqzw")))

(define-public crate-rtd-tutorial-0.1.12 (c (n "rtd-tutorial") (v "0.1.12") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b37c4wxrmsn9nmg87w0r7zry8flmqs08qzv20siizf0xl5gzkba")))

(define-public crate-rtd-tutorial-0.1.13 (c (n "rtd-tutorial") (v "0.1.13") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yf3ywv02zffdj9d58087q5r6iwl5kjlmfgcq2wfrggrg7jbmsbz")))

(define-public crate-rtd-tutorial-0.1.14 (c (n "rtd-tutorial") (v "0.1.14") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qmp5pla7n7gpirbvx7wyblbqn24fkn5n7mcn9xqck323ck1zpfr")))

(define-public crate-rtd-tutorial-0.1.15 (c (n "rtd-tutorial") (v "0.1.15") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "157j7xh0lcr0zanx1zpbg6bcihbg6q2byvdyg04k10d71ynnw7xh")))

(define-public crate-rtd-tutorial-0.1.16 (c (n "rtd-tutorial") (v "0.1.16") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v0yz1mcpy7q0s896nz9fks6ky1zis4zw4m133qzmzdpplpvnvk4")))

(define-public crate-rtd-tutorial-0.1.17 (c (n "rtd-tutorial") (v "0.1.17") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "12ww16iap99zb9c5h6wz1wjpikmab2qbqy2fh404wzabiv7ks8xy")))

(define-public crate-rtd-tutorial-0.1.18 (c (n "rtd-tutorial") (v "0.1.18") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f6p7z6jlwflkcmqjwa1z53k6x11ll3yl0plxfcdrhfpknb0qcvh")))

(define-public crate-rtd-tutorial-0.1.19 (c (n "rtd-tutorial") (v "0.1.19") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "11dbxmq7m9wg4yi2bicbj85ja2y0lnw2w11b7d6kvvb1s9k2992d")))

(define-public crate-rtd-tutorial-0.1.21 (c (n "rtd-tutorial") (v "0.1.21") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qng7n9cibyx3bngj4h1lfy2yx0vhfxbimmfh5inivgzkfp263gm")))

(define-public crate-rtd-tutorial-0.1.22 (c (n "rtd-tutorial") (v "0.1.22") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qv4nhi81s986vhsmwajwy0psbcdn42rkgrxc4zwaavn2xd4v7g5")))

(define-public crate-rtd-tutorial-0.1.23 (c (n "rtd-tutorial") (v "0.1.23") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "146qspcfhyh6igh4hyq53l7hnr9467kj03jikw6jn8637c1vms50")))

