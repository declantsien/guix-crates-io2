(define-module (crates-io rt es rtest-util) #:use-module (crates-io))

(define-public crate-rtest-util-0.1.0 (c (n "rtest-util") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wk58qb0llw5ccj049grnsxfy5cf1r1hzvqincmw0j3fvhdrdknk") (f (quote (("derive" "darling" "quote" "proc-macro2"))))))

(define-public crate-rtest-util-0.2.0 (c (n "rtest-util") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02k2z8zyb76law6larigcg07dk7d6jw61504lp8f13a6vlql8543") (f (quote (("derive" "darling" "quote" "proc-macro2"))))))

