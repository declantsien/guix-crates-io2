(define-module (crates-io rt es rtest-derive) #:use-module (crates-io))

(define-public crate-rtest-derive-0.1.0 (c (n "rtest-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10r8wia2pfvnpbrva2q436h6xha1ags2khcxgmi4wy297gys78sl")))

(define-public crate-rtest-derive-0.1.1 (c (n "rtest-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zlny27qdwrjf9vif9yiv8rfjxxnn0lac8616mj91lmsm6azycfq")))

(define-public crate-rtest-derive-0.1.2 (c (n "rtest-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13lva1yvqj3bijjb1115140lq2ifpbkx9myhgyagr3psyg0clddm")))

(define-public crate-rtest-derive-0.1.3 (c (n "rtest-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12vp0j7s6235yxkvpqyrd88fapd9zmc3q8z2450a0sp5bh92kvp1")))

(define-public crate-rtest-derive-0.1.4 (c (n "rtest-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0krah5s70rvy591jnsx4xd5daqpkjpq9z2c27l9hp1dzpilgd2vl")))

(define-public crate-rtest-derive-0.1.5 (c (n "rtest-derive") (v "0.1.5") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtest-util") (r "^0.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "179s7al7h24pqz3w768h70mhjcnmk18gjr52d2rlpjkz63ivshp9")))

(define-public crate-rtest-derive-0.1.6 (c (n "rtest-derive") (v "0.1.6") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtest-util") (r "^0.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11pdw1mjashn6qnrd3gl7hy4ljp1xzy93ywr4iw4xnwvh7rx0pbr")))

(define-public crate-rtest-derive-0.2.0 (c (n "rtest-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtest-util") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k94a7175axf3jkciqplacy4pn6x363w4mknisagr9ip3n7yk7b8")))

