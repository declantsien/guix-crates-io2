(define-module (crates-io rt ra rtran) #:use-module (crates-io))

(define-public crate-rtran-0.1.0 (c (n "rtran") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13i7p4fd2szg5bzw7hg1ikxiqg4bhqmwfhy0pms3y5v3y1zrb9fl")))

