(define-module (crates-io rt ra rtrace) #:use-module (crates-io))

(define-public crate-rtrace-0.1.0 (c (n "rtrace") (v "0.1.0") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "docopt_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "109jxdarczxxqsbs87dysw4m0jcinqjg3v9w66i8wd81s78lx5qx")))

(define-public crate-rtrace-0.2.0 (c (n "rtrace") (v "0.2.0") (d (list (d (n "clap") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "threadpool") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1shg9a3sfjw0ckg5cwgq2q4rvhn3jx2914d9gvq1f64n3c29z8k6")))

(define-public crate-rtrace-1.0.0 (c (n "rtrace") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "threadpool") (r "^1") (d #t) (k 0)))) (h "1ihia8sag6pv778lgxkkxskv1hqmrqswbh4ik8p0103yx4ykkb15")))

