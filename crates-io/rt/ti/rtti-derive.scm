(define-module (crates-io rt ti rtti-derive) #:use-module (crates-io))

(define-public crate-rtti-derive-0.1.0 (c (n "rtti-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "0xqi7cpaqbk4k7ba0k348vyg35yp604a585fv9axc7qjf7r983lx")))

(define-public crate-rtti-derive-0.2.0 (c (n "rtti-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "1krj6grp2x30gynizvkxj2jnvc6wcppkbb82n542gkhdzww02mgk")))

(define-public crate-rtti-derive-0.3.0 (c (n "rtti-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "060awfzx82clqxyyn7mrx7bplivnhf4ri1mg9c8ncqz0h6zmp1m6")))

(define-public crate-rtti-derive-0.4.0 (c (n "rtti-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "1xy5wijrb8yvwgm72yx7sgn1yh2ij8pcyphhxjg17vsb4fzli157")))

