(define-module (crates-io rt ti rtti) #:use-module (crates-io))

(define-public crate-rtti-0.1.0 (c (n "rtti") (v "0.1.0") (h "0365idc184cg7h1y4z72xa8vyxrwx0zm6dhgp55mv5h9kbwhvq68")))

(define-public crate-rtti-0.2.0 (c (n "rtti") (v "0.2.0") (h "1k3ys1f337sdip9cac0kf8n97hjszv2vj9if8gwxf2c5qvk9zyyp")))

(define-public crate-rtti-0.3.0 (c (n "rtti") (v "0.3.0") (h "1jcfr3v5asyznjg8mv93qv2qyapaxarbkcmb8qanclgxcqn8dvzp") (y #t)))

(define-public crate-rtti-0.3.1 (c (n "rtti") (v "0.3.1") (h "02d64m3sv8n2wf77332fbyjd9v37zcc1d8k9sr9kz5rmydpxs34x")))

(define-public crate-rtti-0.3.2 (c (n "rtti") (v "0.3.2") (h "0b6ylxxhmvfwz15xpsfqnn9plnfks0ypc5sqhyggyyb5pdbpw9hm")))

(define-public crate-rtti-0.4.0 (c (n "rtti") (v "0.4.0") (d (list (d (n "rtti-derive") (r "^0.4") (d #t) (k 2)))) (h "1wdm14r6yi0mk1pnzba4qfvyl4003fbahnkig3xiix5q7pslmqa1")))

