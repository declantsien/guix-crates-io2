(define-module (crates-io ah re ahref) #:use-module (crates-io))

(define-public crate-ahref-0.1.1 (c (n "ahref") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1bj1s57rky4mlkrg62gvlf42asd7nxx1cfrislccik2lla4qlcq3") (r "1.72.0")))

(define-public crate-ahref-0.1.2 (c (n "ahref") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1mnd4i77wg2bkpf3lhbz16fc304f3llp9cgqyvyq745g8rprxibz") (r "1.72.0")))

(define-public crate-ahref-0.1.3 (c (n "ahref") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0kb3y640x02sign0ckmk4h2zv11g6swfk9dyz0rhav6a9mq8y0za") (r "1.72.0")))

(define-public crate-ahref-0.1.4 (c (n "ahref") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1sxq7y9l43spqd97hyk3kn6674nyrclkzqa2v0d27w3k7ms160hz") (r "1.72.0")))

(define-public crate-ahref-0.2.0 (c (n "ahref") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1w61ld6mg84c38hxyk5jych9lsm8jkx2cd735qf30chhv3y7kllv") (r "1.72.0")))

(define-public crate-ahref-0.2.1 (c (n "ahref") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0yra8b8vm28441v4awk3d16k8pm3hhq0bcykbsf40xvlm1qg9x2d") (r "1.72.0")))

(define-public crate-ahref-0.2.2 (c (n "ahref") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0gx59hx8h6d17p1sp1c0qhyhg9810wkjgmwqy70h6v0s64h3aq7f") (r "1.72.0")))

(define-public crate-ahref-0.2.3 (c (n "ahref") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "05afshs626x7mrnhkkk9k71rql60gv18sr9ry82a3swxz9b3gf75") (r "1.72.0")))

(define-public crate-ahref-0.2.4 (c (n "ahref") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "19wb4lfwl9ww3mscwf33zlisijd8z3z4308lqgx1hlkzh0za2y51") (r "1.72.0")))

(define-public crate-ahref-0.2.5 (c (n "ahref") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "198z8hyvw4sklzazv02kbx9x9375ayzrvd5izqz7w9zk0xwvzk7m") (r "1.72.0")))

(define-public crate-ahref-0.2.6 (c (n "ahref") (v "0.2.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1h4dhx83rw4vm357d3vnp0jds5i10qwvbh2pwp1xs187zcd836sv") (r "1.72.0")))

(define-public crate-ahref-0.2.7 (c (n "ahref") (v "0.2.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1fsfj3vsrf44jy29w2v16djqs4i86ilpfqxvq1w9iz3z570zcayd") (r "1.72.0")))

(define-public crate-ahref-0.2.8 (c (n "ahref") (v "0.2.8") (d (list (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "tagparser") (r "^0.1.0") (d #t) (k 0)))) (h "1k0vn56f06nbc48zf2k6vzbbmimj33f9gh4czpj66k501wwb9xaz") (r "1.72.0")))

(define-public crate-ahref-0.2.9 (c (n "ahref") (v "0.2.9") (d (list (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "tagparser") (r "^0.2.0") (d #t) (k 0)))) (h "07pz5zz3qz93wg2cw4xzrigsrdb2992ffnlvdmmcvazcm3hl4dcn") (r "1.72.0")))

(define-public crate-ahref-0.3.0 (c (n "ahref") (v "0.3.0") (d (list (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "tagparser") (r "^0.2.0") (d #t) (k 0)))) (h "0kjd8jpp1qc15j7msmqvr50pqhr7inha7w7gwzcgwkddckyfy9rr") (r "1.72.0")))

