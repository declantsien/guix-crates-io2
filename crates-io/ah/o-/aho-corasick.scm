(define-module (crates-io ah o- aho-corasick) #:use-module (crates-io))

(define-public crate-aho-corasick-0.1.0 (c (n "aho-corasick") (v "0.1.0") (d (list (d (n "memchr") (r "0.1.*") (d #t) (k 0)))) (h "1x4n3z0drbc9gdjwq7mynfmds59h8y6i6zg98712am1kq35mi919")))

(define-public crate-aho-corasick-0.1.1 (c (n "aho-corasick") (v "0.1.1") (d (list (d (n "memchr") (r "0.1.*") (d #t) (k 0)))) (h "120k9hq2gbg66x0qg8j8cn7mac4g63nngf720vpr8yarr706brf4")))

(define-public crate-aho-corasick-0.1.2 (c (n "aho-corasick") (v "0.1.2") (d (list (d (n "csv") (r "*") (d #t) (k 2)) (d (n "docopt") (r "*") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0qgzjacmk7b9ix9q3pki9c8cb99w3hpnas7iamns1skxawyj5fan")))

(define-public crate-aho-corasick-0.1.3 (c (n "aho-corasick") (v "0.1.3") (d (list (d (n "csv") (r "*") (d #t) (k 2)) (d (n "docopt") (r "*") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0gcffcc0ybj6bslwj6wgrv4fxgmgmav3622vgp5cl2npfdpiflyf")))

(define-public crate-aho-corasick-0.1.4 (c (n "aho-corasick") (v "0.1.4") (d (list (d (n "csv") (r "*") (d #t) (k 2)) (d (n "docopt") (r "*") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0693fcrrvwl4jknwrk2kz5z358312n4qi2k44bhapjglcmajz6w2")))

(define-public crate-aho-corasick-0.1.5 (c (n "aho-corasick") (v "0.1.5") (d (list (d (n "csv") (r "*") (d #t) (k 2)) (d (n "docopt") (r "*") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "094wdwgl5n811s6l88hc6j36i7b27p5r7s46wym1chabgykk7gp1")))

(define-public crate-aho-corasick-0.2.0 (c (n "aho-corasick") (v "0.2.0") (d (list (d (n "csv") (r "*") (d #t) (k 2)) (d (n "docopt") (r "*") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "1al22piywd9xv5hv2xai49vpmhk8yf9lx1k7bgrdg231b9m0q7vv")))

(define-public crate-aho-corasick-0.2.1 (c (n "aho-corasick") (v "0.2.1") (d (list (d (n "csv") (r "*") (d #t) (k 2)) (d (n "docopt") (r "*") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0fyqlx0isphcwq7bapls76153n0hpxszz1wghh2qm6q79rnkls01")))

(define-public crate-aho-corasick-0.2.2 (c (n "aho-corasick") (v "0.2.2") (d (list (d (n "csv") (r "*") (d #t) (k 2)) (d (n "docopt") (r "*") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "0xbxajdxp9j5k67gajh5wiy964cm84ly1zfh0md4zh30wnh9915k")))

(define-public crate-aho-corasick-0.3.0 (c (n "aho-corasick") (v "0.3.0") (d (list (d (n "csv") (r "*") (d #t) (k 2)) (d (n "docopt") (r "*") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 2)))) (h "136b8wr7azlk62j4rbzwnj93fxillj6f9knlprw2j98iwk0csfhg")))

(define-public crate-aho-corasick-0.3.1 (c (n "aho-corasick") (v "0.3.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1jxn9x2mvcgys8848jp146wcqzk3p0b6r2gwj46bmwn1fgipba5z")))

(define-public crate-aho-corasick-0.3.2 (c (n "aho-corasick") (v "0.3.2") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0api6lbb9ima6hdfy2xbyccjsn9mh9rpl4grkqw5a7ybvvaazd00")))

(define-public crate-aho-corasick-0.3.4 (c (n "aho-corasick") (v "0.3.4") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0k9argw1p5rwk4r5naxjz1xsrzhrxcbaa6gdk75bkjax28w4scgm")))

(define-public crate-aho-corasick-0.4.0 (c (n "aho-corasick") (v "0.4.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1vf5p9jak98b3ksap6i4mykvbkid31ln7jlandvbkplhqhvz4dlg")))

(define-public crate-aho-corasick-0.4.1 (c (n "aho-corasick") (v "0.4.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "memchr") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1c6fnx7cx04fdvh9z4wfxsfvc6gmzkkr66879qfxyfmw7xd55cfb")))

(define-public crate-aho-corasick-0.5.0 (c (n "aho-corasick") (v "0.5.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "memchr") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1jq0rqwglfznizlg0dzf7dkb1haj56anxmzxqav5sh5qgjb3djh0")))

(define-public crate-aho-corasick-0.5.1 (c (n "aho-corasick") (v "0.5.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "memchr") (r "^0.1.9") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "00zx2xrn15mga2nhhvl3x4jhrm00shw66y76saz54fd0y1w781v7")))

(define-public crate-aho-corasick-0.5.2 (c (n "aho-corasick") (v "0.5.2") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "memchr") (r "^0.1.9") (d #t) (k 0)) (d (n "memmap") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1jcdhyds4rc5wfl6k6bar552xs2ba46kjdfb3ab0nwf114mvagrb")))

(define-public crate-aho-corasick-0.5.3 (c (n "aho-corasick") (v "0.5.3") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "memchr") (r "^0.1.9") (d #t) (k 0)) (d (n "memmap") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0rnvdmlajikq0i4zdy1p3pv699q6apvsxfc7av7byhppllp2r5ya")))

(define-public crate-aho-corasick-0.6.0 (c (n "aho-corasick") (v "0.6.0") (d (list (d (n "csv") (r "^0.15") (d #t) (k 2)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "memchr") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0wsh6i2wq336l5z3rlfjrmimxr5z1c555apvcdgnz9nlwvli44cv")))

(define-public crate-aho-corasick-0.6.1 (c (n "aho-corasick") (v "0.6.1") (d (list (d (n "csv") (r "^0.15") (d #t) (k 2)) (d (n "docopt") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1gymiyg646qyfzb0abqhmpxpd5smpcv4nyzhr5b9p5v24ya0nrjg")))

(define-public crate-aho-corasick-0.6.2 (c (n "aho-corasick") (v "0.6.2") (d (list (d (n "csv") (r "^0.15") (d #t) (k 2)) (d (n "docopt") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "18hcxn0acwz0dnfk7mqm9pmjadwy3dnij54q946akji7jiagsf06")))

(define-public crate-aho-corasick-0.6.3 (c (n "aho-corasick") (v "0.6.3") (d (list (d (n "csv") (r "^0.15") (d #t) (k 2)) (d (n "docopt") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "16cni15riakgqdvdnrn57bay3s9k124nsqi6bcsm57ksz320j2ah")))

(define-public crate-aho-corasick-0.6.4 (c (n "aho-corasick") (v "0.6.4") (d (list (d (n "csv") (r "^0.15") (d #t) (k 2)) (d (n "docopt") (r "^0.7") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1i1gm3x274xb28w72c7s01d74a9j50an92irh6m2af3jvr21slyn")))

(define-public crate-aho-corasick-0.6.5 (c (n "aho-corasick") (v "0.6.5") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1040pw3yb1pj6grxancpzb8ccv34qm4z08ikg7i7rx519qaj1fph")))

(define-public crate-aho-corasick-0.6.6 (c (n "aho-corasick") (v "0.6.6") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1yhsrslzw7wypgba1qlbs288zsry11y9wj2v1dr2ivg7rdix9in1")))

(define-public crate-aho-corasick-0.6.7 (c (n "aho-corasick") (v "0.6.7") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1h4fa1cpvljp64yqmji0shmnvidjhwk0h25pwy0xdffccfa4xsby")))

(define-public crate-aho-corasick-0.6.8 (c (n "aho-corasick") (v "0.6.8") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0npb5qab3j06ihfvvaafknh3pz2zpgvr1vbnpmy59ag5adrnrxb8")))

(define-public crate-aho-corasick-0.6.9 (c (n "aho-corasick") (v "0.6.9") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "17pbmrm3pzcyl7hf7syy684g4867qmnzkz6y29xqsraq9qzr76hy")))

(define-public crate-aho-corasick-0.6.10 (c (n "aho-corasick") (v "0.6.10") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "docopt") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "19f8v503ibvlyr824g5ynicrh1lsmp2i0zmpszr8lqay0qw3vkl1")))

(define-public crate-aho-corasick-0.7.0 (c (n "aho-corasick") (v "0.7.0") (d (list (d (n "memchr") (r "^2.2.0") (k 0)))) (h "1q6zlbfimbn0fxcg41h9qk3xizywa37v74mis0gglfmd2cwvhjxw") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.1 (c (n "aho-corasick") (v "0.7.1") (d (list (d (n "memchr") (r "^2.2.0") (k 0)))) (h "1swdxylzclmlqszgwi2akpy25lzzia9ddqr1ddwprykqvgx00pvn") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.2 (c (n "aho-corasick") (v "0.7.2") (d (list (d (n "memchr") (r "^2.2.0") (k 0)))) (h "1p4ynsnr1mc80flk3dfsy3la8hk80pz75zy7gi1r87qlzxyc7g5r") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.3 (c (n "aho-corasick") (v "0.7.3") (d (list (d (n "memchr") (r "^2.2.0") (k 0)))) (h "0k1nqabiz37mnfnlrn084qi9yf8pj6a38qgbb3lc5zlr1jp89x76") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.4 (c (n "aho-corasick") (v "0.7.4") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "10p2yl9fz7a4zwc4kj4zpk0wcz64n4m5l0ng6zsa6zkxrcfamdrn") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.5 (c (n "aho-corasick") (v "0.7.5") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "0bk34g1ciiyzrpd4rhcnvvpda2smqsv6kk7iglvwcdx4952a15h9") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.6 (c (n "aho-corasick") (v "0.7.6") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "0b8dh20fhdc59dhhnfi89n2bi80a8zbagzd5c122hf1vv2amxysq") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.7 (c (n "aho-corasick") (v "0.7.7") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "0i2p0c424q2xizl7wpavi5syiisz1dcggmhiz7g4khkd4mvc8mjz") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.8 (c (n "aho-corasick") (v "0.7.8") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "048q5vr1qac4lf90z80lw8kcya6qmlxw857xhwxsssk832jdafkl") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.9 (c (n "aho-corasick") (v "0.7.9") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "1v6j2w4di47icbfh3j5bd2a7n2l7g8cw15bhmrsa52z18k8kzrnm") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.10 (c (n "aho-corasick") (v "0.7.10") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "1nka9509afjgal6lpymn8w2lq11dmjwxs8yjcmzys966if5l05l7") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.11 (c (n "aho-corasick") (v "0.7.11") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "1pbwh4hnz907kmxbssh6llgyi28r7666r0m0siks4qnbhi29sc2l") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.12 (c (n "aho-corasick") (v "0.7.12") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "04fmv26hy41dr0ygpcm6z4vq64ly8q9zqwwvc0ysfsvhmi4afnf2") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.13 (c (n "aho-corasick") (v "0.7.13") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "11hfmqf90rdvjdpk0x1lixw1s9n08y3fxfy9zqsk0k2wpbc68c84") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.14 (c (n "aho-corasick") (v "0.7.14") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "038x2d8n736llndrggi8pl8gk3yl66pbnp9r7mnhr2v70dqwwxml") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.15 (c (n "aho-corasick") (v "0.7.15") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2.0") (k 0)))) (h "1rb8gzhljl8r87dpf2n5pnqnkl694casgns4ma0sqzd4zazzw13l") (f (quote (("std" "memchr/use_std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.16 (c (n "aho-corasick") (v "0.7.16") (d (list (d (n "memchr") (r "^2.4.0") (k 0)))) (h "15k7smfzblsphhnd5nlyyhmn2lahxd1wi2rjcx894rxicda9f59c") (f (quote (("std" "memchr/std") ("default" "std")))) (y #t)))

(define-public crate-aho-corasick-0.7.17 (c (n "aho-corasick") (v "0.7.17") (d (list (d (n "memchr") (r "^2.4.0") (k 0)))) (h "1p2nrql40jid0rc0c5m28k9bw8qh4gbi5z0dp17ydcwczn5x5pr4") (f (quote (("std" "memchr/std") ("default" "std")))) (y #t)))

(define-public crate-aho-corasick-0.7.18 (c (n "aho-corasick") (v "0.7.18") (d (list (d (n "memchr") (r "^2.4.0") (k 0)))) (h "0vv50b3nvkhyy7x7ip19qnsq11bqlnffkmj2yx2xlyk5wzawydqy") (f (quote (("std" "memchr/std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.19 (c (n "aho-corasick") (v "0.7.19") (d (list (d (n "memchr") (r "^2.4.0") (k 0)))) (h "0knl5n9f396068qk4zrvhcf01d5qp9ja2my4j7ywny093bcmpxdl") (f (quote (("std" "memchr/std") ("default" "std"))))))

(define-public crate-aho-corasick-0.7.20 (c (n "aho-corasick") (v "0.7.20") (d (list (d (n "memchr") (r "^2.4.0") (k 0)))) (h "1b3if3nav4qzgjz9bf75b2cv2h2yisrqfs0np70i38kgz4cn94yc") (f (quote (("std" "memchr/std") ("default" "std"))))))

(define-public crate-aho-corasick-1.0.0 (c (n "aho-corasick") (v "1.0.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "11faanlzngxv2ldvcmf4w1hdvza1qk7308jsz0v3n7l9a3afig75") (f (quote (("default" "std" "perf-literal")))) (s 2) (e (quote (("std" "memchr?/std") ("perf-literal" "dep:memchr") ("logging" "dep:log")))) (r "1.60.0")))

(define-public crate-aho-corasick-1.0.1 (c (n "aho-corasick") (v "1.0.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "014ddyrlbwg18m74fa52wrfik8y3pzhwqg811yvsyc8cjb70iz37") (f (quote (("default" "std" "perf-literal")))) (s 2) (e (quote (("std" "memchr?/std") ("perf-literal" "dep:memchr") ("logging" "dep:log")))) (r "1.60.0")))

(define-public crate-aho-corasick-1.0.2 (c (n "aho-corasick") (v "1.0.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "0has59a3571irggpk5z8c0lpnx8kdx12qf4g2x0560i2y8dwpxj3") (f (quote (("default" "std" "perf-literal")))) (s 2) (e (quote (("std" "memchr?/std") ("perf-literal" "dep:memchr") ("logging" "dep:log")))) (r "1.60.0")))

(define-public crate-aho-corasick-1.0.3 (c (n "aho-corasick") (v "1.0.3") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "031h4bnpwg3z68jlx64zhc6qm4zb229n5psyjfg2szvr1x1gkf46") (f (quote (("default" "std" "perf-literal")))) (s 2) (e (quote (("std" "memchr?/std") ("perf-literal" "dep:memchr") ("logging" "dep:log")))) (r "1.60.0")))

(define-public crate-aho-corasick-1.0.4 (c (n "aho-corasick") (v "1.0.4") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "0niskpw2ajzn0gf8q99hypzn7mrc2bs03a3gk4a4vva8yggfhj37") (f (quote (("default" "std" "perf-literal")))) (s 2) (e (quote (("std" "memchr?/std") ("perf-literal" "dep:memchr") ("logging" "dep:log")))) (r "1.60.0")))

(define-public crate-aho-corasick-1.0.5 (c (n "aho-corasick") (v "1.0.5") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "10s7pa9rab0m54bdxmfvzpr1jg73dr92gy0njq4diniz89w8sdqc") (f (quote (("default" "std" "perf-literal")))) (s 2) (e (quote (("std" "memchr?/std") ("perf-literal" "dep:memchr") ("logging" "dep:log")))) (r "1.60.0")))

(define-public crate-aho-corasick-1.1.0 (c (n "aho-corasick") (v "1.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "1w2vvl1ib62pyihrdyihajsb0hdwwy61wz5qnb90kimm7xb3a88g") (f (quote (("default" "std" "perf-literal")))) (s 2) (e (quote (("std" "memchr?/std") ("perf-literal" "dep:memchr") ("logging" "dep:log")))) (r "1.60.0")))

(define-public crate-aho-corasick-1.1.1 (c (n "aho-corasick") (v "1.1.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "1aqqalh66jygy54fbnpglzrb9dwlrvn6zl1nhncdvynl8w376pga") (f (quote (("default" "std" "perf-literal")))) (s 2) (e (quote (("std" "memchr?/std") ("perf-literal" "dep:memchr") ("logging" "dep:log")))) (r "1.60.0")))

(define-public crate-aho-corasick-1.1.2 (c (n "aho-corasick") (v "1.1.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "1w510wnixvlgimkx1zjbvlxh6xps2vjgfqgwf5a6adlbjp5rv5mj") (f (quote (("default" "std" "perf-literal")))) (s 2) (e (quote (("std" "memchr?/std") ("perf-literal" "dep:memchr") ("logging" "dep:log")))) (r "1.60.0")))

(define-public crate-aho-corasick-1.1.3 (c (n "aho-corasick") (v "1.1.3") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.4.0") (o #t) (k 0)))) (h "05mrpkvdgp5d20y2p989f187ry9diliijgwrs254fs9s1m1x6q4f") (f (quote (("default" "std" "perf-literal")))) (s 2) (e (quote (("std" "memchr?/std") ("perf-literal" "dep:memchr") ("logging" "dep:log")))) (r "1.60.0")))

