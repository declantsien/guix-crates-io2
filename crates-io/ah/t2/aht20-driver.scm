(define-module (crates-io ah t2 aht20-driver) #:use-module (crates-io))

(define-public crate-aht20-driver-0.1.0 (c (n "aht20-driver") (v "0.1.0") (d (list (d (n "crc-any") (r "^2.4.1") (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "rtt-target") (r "^0.2.0") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "1hcrbbx4lrs6dx5wj9l65nf3a81x2baikxgw2cm135v26473za2f")))

(define-public crate-aht20-driver-1.0.0 (c (n "aht20-driver") (v "1.0.0") (d (list (d (n "crc-any") (r "^2.4.1") (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "rtt-target") (r "^0.2.0") (f (quote ("cortex-m"))) (d #t) (k 0)))) (h "0rbc63hb16xm7s1596fh06sihn253ib2gb1vkdvsw0m6jbilahcn")))

(define-public crate-aht20-driver-1.1.0 (c (n "aht20-driver") (v "1.1.0") (d (list (d (n "crc-any") (r "^2.4.1") (k 0)) (d (n "defmt") (r "^0.3.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3.0") (f (quote ("unstable-test"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "0vnq5x85nnl91srb9gkrmhxcqg08bmd6nhq9i69907bxan2iy5iv")))

(define-public crate-aht20-driver-1.2.0 (c (n "aht20-driver") (v "1.2.0") (d (list (d (n "crc-any") (r "^2.4.1") (k 0)) (d (n "defmt") (r "^0.3.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3.0") (f (quote ("unstable-test"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1py4yf43ibmrnlyjizpamm18w8w9bq8vi8wrhv27bxi664h4qv28")))

(define-public crate-aht20-driver-1.2.1 (c (n "aht20-driver") (v "1.2.1") (d (list (d (n "crc-any") (r "^2.4.4") (k 0)) (d (n "defmt") (r "^0.3.6") (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (f (quote ("unstable-test"))) (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "10s5nbxqd6rq1sqfgq63jysxh5kny6zv72lw67s5wfmhcxwyghb0")))

