(define-module (crates-io ah t2 aht20) #:use-module (crates-io))

(define-public crate-aht20-0.1.0 (c (n "aht20") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crc_all") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1ymlh18vavhzmwan44iix2nwvvm6y7x1cwvaczmxi6drdzdkqarj")))

