(define-module (crates-io ah t2 aht20-async) #:use-module (crates-io))

(define-public crate-aht20-async-1.0.0 (c (n "aht20-async") (v "1.0.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "crc_all") (r "^0.2.2") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (k 2)))) (h "09vqycn42kv5jvf3hzkw3s6lf67kz0z42mp16bpyq2ch3hfq14ww")))

