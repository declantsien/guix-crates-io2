(define-module (crates-io ah rs ahrs) #:use-module (crates-io))

(define-public crate-ahrs-0.0.1 (c (n "ahrs") (v "0.0.1") (d (list (d (n "nalgebra") (r "0.10.*") (d #t) (k 0)))) (h "0sz6gllvbzz1z2q8mc46hcdpgs74ln951iklpv7hg5jl5svfiwvy")))

(define-public crate-ahrs-0.1.0 (c (n "ahrs") (v "0.1.0") (d (list (d (n "nalgebra") (r "0.10.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "00zf9wswams5k89spj93a8bfl7v7gw4j2mpdn0zzjp4ahi7krs1z")))

(define-public crate-ahrs-0.1.1 (c (n "ahrs") (v "0.1.1") (d (list (d (n "nalgebra") (r "0.10.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1yrxfpwpl83fh1mnv8a8vbmfzivw80sjdmijghyq4yj2sc1l72i1")))

(define-public crate-ahrs-0.1.2 (c (n "ahrs") (v "0.1.2") (d (list (d (n "nalgebra") (r "0.10.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0ivvz4dhy16xf4i58wqz5xrxp2k593zfxwjzqb3kcbnymdp8zidw")))

(define-public crate-ahrs-0.1.3 (c (n "ahrs") (v "0.1.3") (d (list (d (n "nalgebra") (r "0.10.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "08xdgb223w4j6imqq1xjxgkywk64qmcdf6yzkiqfbhxi66sm98zs")))

(define-public crate-ahrs-0.1.4 (c (n "ahrs") (v "0.1.4") (d (list (d (n "nalgebra") (r "0.10.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1y1rg6p336qwvi5qdvf9a2clnp0jxmwrz9vv9h68zg69cvf110ml")))

(define-public crate-ahrs-0.1.5 (c (n "ahrs") (v "0.1.5") (d (list (d (n "nalgebra") (r "0.10.*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "103mpz6wizdpwrr2gdkss84sjqv8r2502ksaqq6s97yb92xiqqn0")))

(define-public crate-ahrs-0.2.0 (c (n "ahrs") (v "0.2.0") (d (list (d (n "alga") (r "^0.5.0") (d #t) (k 0)) (d (n "approx") (r "^0.1.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "10q252iycvx88kjyhh300p48d9yarsx5i6pvkid04jpcx03vyg8j")))

(define-public crate-ahrs-0.2.1 (c (n "ahrs") (v "0.2.1") (d (list (d (n "alga") (r "^0.9") (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (k 0)) (d (n "rand") (r "^0.7") (k 2)))) (h "0n12g374n5apv4gk034ik9gn0cjr41dgrv3h1kbm15gvx6s7chcl") (f (quote (("std" "nalgebra/default" "alga/default" "rand/default") ("field_access") ("default" "std"))))))

(define-public crate-ahrs-0.2.2 (c (n "ahrs") (v "0.2.2") (d (list (d (n "alga") (r "^0.9") (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (k 0)) (d (n "rand") (r "^0.7") (k 2)))) (h "0gmakj6x7aljpdv2yxhsvfsabzqn77x16qkl6hv8lc14pbgi3qml") (f (quote (("std" "nalgebra/default" "alga/default") ("field_access") ("default" "std"))))))

(define-public crate-ahrs-0.3.0 (c (n "ahrs") (v "0.3.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.21") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "simba") (r "^0.1") (k 0)))) (h "0lycaz5mq6p9y5176vmnpxcgf562x0rya6vviq4mmphkhjy0qwak") (f (quote (("std" "nalgebra/default" "simba/default" "num-traits/default") ("field_access") ("default" "std"))))))

(define-public crate-ahrs-0.4.0 (c (n "ahrs") (v "0.4.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.21") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "simba") (r "^0.1") (k 0)))) (h "03gnpjn1i8pgvc1v4w226cdsknc0mjgcyzb2ml8fb46lvvwvrscq") (f (quote (("std" "nalgebra/default" "simba/default" "num-traits/default") ("field_access") ("default" "std"))))))

(define-public crate-ahrs-0.5.0 (c (n "ahrs") (v "0.5.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.30") (f (quote ("libm-force"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simba") (r "^0.7") (k 0)))) (h "14lrr92m78m9mfwmy9zqky4shl97dgxkinagxyrkc2mdkwkvdrfw") (f (quote (("std" "nalgebra/default" "simba/default" "num-traits/default") ("field_access") ("default" "std"))))))

(define-public crate-ahrs-0.6.0 (c (n "ahrs") (v "0.6.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "nalgebra") (r "^0.31") (f (quote ("libm-force"))) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "simba") (r "^0.7") (k 0)))) (h "1ljkb7zz4kaws9qlpdnwyq1nnf2w0lqlii4bn90kixfinizkdkzd") (f (quote (("std" "nalgebra/default" "simba/default" "num-traits/default") ("field_access") ("default" "std"))))))

