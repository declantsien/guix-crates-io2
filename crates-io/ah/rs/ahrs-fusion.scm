(define-module (crates-io ah rs ahrs-fusion) #:use-module (crates-io))

(define-public crate-ahrs-fusion-0.1.0 (c (n "ahrs-fusion") (v "0.1.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0") (d #t) (k 0)))) (h "1rrw46mz0yxrmhyamspxlhn7lqbnv80iy2glzfnd06jkirpk2h8c")))

(define-public crate-ahrs-fusion-0.2.0 (c (n "ahrs-fusion") (v "0.2.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0") (d #t) (k 0)) (d (n "micromath") (r "^2") (f (quote ("quaternion" "vector"))) (o #t) (d #t) (k 0)))) (h "1lcfl0k1wmd33la69axsxgdxxslda8ik644121z29qcgybc6nikk") (f (quote (("build-bin" "micromath"))))))

(define-public crate-ahrs-fusion-0.3.0 (c (n "ahrs-fusion") (v "0.3.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0") (d #t) (k 0)) (d (n "micromath") (r "^2") (f (quote ("quaternion" "vector"))) (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.16.3") (f (quote ("extension-module" "abi3-py39"))) (o #t) (d #t) (k 0)))) (h "09nrc5xmqrbrji7h6pps0xdfvwzz5m4v969qw9b9l7h60r4qwym3") (f (quote (("python" "pyo3") ("build-bin" "micromath"))))))

(define-public crate-ahrs-fusion-0.4.0 (c (n "ahrs-fusion") (v "0.4.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0") (d #t) (k 0)) (d (n "micromath") (r "^2") (f (quote ("quaternion" "vector"))) (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.16.3") (f (quote ("extension-module" "abi3-py39"))) (o #t) (d #t) (k 0)))) (h "18536cdnh3rvm6hfm2aqf1nq7i7j7jj9dvcgdxyqdir4b307wkmp") (f (quote (("python" "pyo3") ("build-bin" "micromath"))))))

