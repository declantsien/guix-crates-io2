(define-module (crates-io ah um ahum_dialoguer) #:use-module (crates-io))

(define-public crate-ahum_dialoguer-0.4.0 (c (n "ahum_dialoguer") (v "0.4.0") (d (list (d (n "console") (r ">= 0.3.0, < 1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "05fs2r9ygrlgzzyd2461hsdbrmpi16qhnyswsbkkx380nyl8wpnh")))

(define-public crate-ahum_dialoguer-0.5.0 (c (n "ahum_dialoguer") (v "0.5.0") (d (list (d (n "console") (r ">= 0.3.0, < 1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "1bvm3p62yvcqx0ikqdif3brg7k0ga83y1c1h89vhff7w9dvk1fbn")))

