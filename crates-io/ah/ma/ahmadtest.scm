(define-module (crates-io ah ma ahmadtest) #:use-module (crates-io))

(define-public crate-ahmadtest-0.1.0 (c (n "ahmadtest") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18amp8ss9jfawv2kxdh8n2spnc7px8967fakg33ncab1h35kwj2c") (y #t)))

(define-public crate-ahmadtest-0.1.1 (c (n "ahmadtest") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x5zh78xdpvmiwggxx5phj9n1l385mprnvh63hs2psg086hks3az") (y #t)))

(define-public crate-ahmadtest-0.1.2 (c (n "ahmadtest") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "157yyvaablgx51glxzk0fa4jljgjbgvly4h0xrzj6pgr77na8jaa") (y #t)))

(define-public crate-ahmadtest-0.1.3 (c (n "ahmadtest") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "122ixp56b3gvph5zn0iqghqqhdvlrddi6cz92qxq78g6hf4pcgjw") (y #t)))

(define-public crate-ahmadtest-0.1.4 (c (n "ahmadtest") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11a6qgdfd9x3sp5rif6ff9jwapraznmihdzc15yynn9lwny37ldw") (y #t)))

(define-public crate-ahmadtest-0.1.5 (c (n "ahmadtest") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03m3cq066sy54zp22qd5cdy9j5km77mxp1f99n91q9mvqcaf6xdf") (y #t)))

(define-public crate-ahmadtest-0.1.6 (c (n "ahmadtest") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1683yc7llwm7mlbn73703hb1zmpj21z36avb601g7icf5mkv85ip") (y #t)))

(define-public crate-ahmadtest-0.1.7 (c (n "ahmadtest") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z0khn25mv3kk4anghbdfjc5my172w25rhw29hblfrnqglpi58cj") (y #t)))

(define-public crate-ahmadtest-0.1.8 (c (n "ahmadtest") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1392w1j2r7dy4rdhgd7h2ghqjpmbk74fi4ssyp0nbz8sqrxqy9ry") (y #t)))

(define-public crate-ahmadtest-0.1.9 (c (n "ahmadtest") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02dnrqsqs5cs2wcvcv881xzq94b9f582y0dg1n8hs8yyv7gx1356") (y #t)))

(define-public crate-ahmadtest-0.1.10 (c (n "ahmadtest") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j57cbaim786c6gawl17z21yjnaykv6r5azw8lmyn43jhydrjslj") (y #t)))

(define-public crate-ahmadtest-0.1.11 (c (n "ahmadtest") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mvy38a5y9f1nc2cb4lwjhjj6ad07kxwgcdbzxk6m9cd8hb06m2f") (y #t)))

(define-public crate-ahmadtest-0.1.12 (c (n "ahmadtest") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mlfbj7lqsgvlbmr1cn9yb8bxymif7p824chqnlky87s9nnrhvcn") (y #t)))

(define-public crate-ahmadtest-0.1.13 (c (n "ahmadtest") (v "0.1.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v5lj91p5c940fqzx7xvl5apkja4s4r779ykr814rm6aw07zk33w") (y #t)))

(define-public crate-ahmadtest-0.1.14 (c (n "ahmadtest") (v "0.1.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kl6s2qwnpr4ig9anqqg2p3wdvh23ga25havqv9gb6zznfwy4ayz") (y #t)))

(define-public crate-ahmadtest-0.1.15 (c (n "ahmadtest") (v "0.1.15") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1plj3j3l5m16ry8cccykryp0nzw769p1lg3dxs7rb3q7bi1pjxnf") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.16 (c (n "ahmadtest") (v "0.1.16") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ph54731qc9qinxxljz04r76n0i1fz037fb9dc42scsksibf0sbf") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.17 (c (n "ahmadtest") (v "0.1.17") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k5wc0klkhw4zfldzwv28ahazdpqz6qyhp73vf5czhl53h25nvki") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.18 (c (n "ahmadtest") (v "0.1.18") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "183xv0yq3wmd74ys5vnr960lklaypsmw9z0dv6lik27qc7w13s99") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.19 (c (n "ahmadtest") (v "0.1.19") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wa4da2vsymacz88d228lhg5v4w8s1a06sq0d9s3c27nh3y0xw1h") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.20 (c (n "ahmadtest") (v "0.1.20") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f71dsg5d7ykcj98dbkdaq1avf0aa9m0zv2c139hgh2h4z51zy0x") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.21 (c (n "ahmadtest") (v "0.1.21") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zlla49nkf4547f497c95mcg99ca4n8sxjw25wj4pi6n7vw51q8m") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.22 (c (n "ahmadtest") (v "0.1.22") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bf54g35r1mm5bbf6ak4lflibzh1bidhhcq52w1acv3q7dg27k7y") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.24 (c (n "ahmadtest") (v "0.1.24") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gdprfkdpiygsk4x8j8iljmfqxbq9rr4m6mylrcl8679g8csx4l6") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.25 (c (n "ahmadtest") (v "0.1.25") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wqq42lq2m1km3m7cyandxw3p94d599bzkbdbcknns00ncc93lif") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.26 (c (n "ahmadtest") (v "0.1.26") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i0chd0l9p60x740c1nd0cc7rw8aqdzlx8z72sh05wxnd6shfixg") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.27 (c (n "ahmadtest") (v "0.1.27") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g1nx21k27azfwr4snfq8hfw7dcnq7v4c6x6hm7igndlhndqkbri") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-0.1.28 (c (n "ahmadtest") (v "0.1.28") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bxb7qy6sg2spp9by0hi9fmqd4n6q4k3yhwvs84frlz942vrwy6r") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-1.2.6 (c (n "ahmadtest") (v "1.2.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14hqw8kf496mimvh4nmp974ki5icqm8x04mgmz0ij3szjb4d06bk") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-1.2.7 (c (n "ahmadtest") (v "1.2.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02w3csk3ak81gllpn08x64m9c0j3bik00v27ipayj40ys6r8sc6x") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-1.2.8 (c (n "ahmadtest") (v "1.2.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08y22fvrz9j63gdyfq1wd5dnl84008rm7i4l4aqyng2gmrf1yw5l") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-1.2.9 (c (n "ahmadtest") (v "1.2.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jn9m3s150a97k7dfp3kgrgsj2y9wil4b2yy8lmriy9lq1d1kkd9") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-1.2.10 (c (n "ahmadtest") (v "1.2.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mndrp8i2s3lya6kjdp48f0qb36q9g8h6ndgnkbyzmm15k52jxdn") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-1.2.11 (c (n "ahmadtest") (v "1.2.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c3hy4piyilahf5hq2jpp5hh04lz4p6gnc844vl7j7vmqg062brm") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-1.2.12 (c (n "ahmadtest") (v "1.2.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q9d0l9xcp6w4xwlrih99pkvcxr3fr3c060wnvs70wwr5wnv7knz") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-1.2.13 (c (n "ahmadtest") (v "1.2.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z2xxda1ygb9m50dla40w8im0jc2jvcx5v2s5vay06gzw774dl7q") (y #t) (l "LexActivator")))

(define-public crate-ahmadtest-1.2.14 (c (n "ahmadtest") (v "1.2.14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04zalrfhy1khcr2dwhcgmxri7z6c5d581a4189a7ml8r5ff8mkqq") (l "JustCheck")))

