(define-module (crates-io ah ec ahecha_cli) #:use-module (crates-io))

(define-public crate-ahecha_cli-0.0.1 (c (n "ahecha_cli") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("any" "macros" "postgres" "runtime-tokio-rustls"))) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0samwyx546z2zlha60h317cxs8vmc3zp2zbwjdb58ip9x2vn40qm")))

