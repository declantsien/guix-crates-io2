(define-module (crates-io ah ec ahecha) #:use-module (crates-io))

(define-public crate-ahecha-0.0.1 (c (n "ahecha") (v "0.0.1") (h "0k60s4fjpwlrbqyayz231wjsj74jn6yb7q27kb9isr4ahwfq52ad")))

(define-public crate-ahecha-0.0.2 (c (n "ahecha") (v "0.0.2") (d (list (d (n "ahecha_html") (r "^0") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)))) (h "15657jvxv7vyspqrvdwllmca2lk925wl36xfcdsfyvqkxkw0kd8k") (f (quote (("frontend" "ahecha_macro/frontend") ("backend" "ahecha_macro/backend"))))))

(define-public crate-ahecha-0.0.3 (c (n "ahecha") (v "0.0.3") (d (list (d (n "ahecha_html") (r "^0") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)))) (h "1ncbvg0rih07v7dbck8arv8dggyhhm06r3sjyqwh22c74p4xnbg9") (f (quote (("time" "ahecha_html/time") ("frontend" "ahecha_macro/frontend") ("default") ("chrono" "ahecha_html/chrono") ("backend" "ahecha_macro/backend"))))))

(define-public crate-ahecha-0.0.4 (c (n "ahecha") (v "0.0.4") (d (list (d (n "ahecha_html") (r "^0.0") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0.0") (d #t) (k 0)))) (h "133kdm49dj982kl3p5wq87w5452w75i51vv389vjvq3v5nkdj47c") (f (quote (("time" "ahecha_html/time") ("frontend" "ahecha_macro/frontend") ("default") ("chrono" "ahecha_html/chrono") ("backend" "ahecha_macro/backend"))))))

(define-public crate-ahecha-0.0.5 (c (n "ahecha") (v "0.0.5") (d (list (d (n "ahecha_html") (r "^0.0.5") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0.0.5") (d #t) (k 0)))) (h "0wdxch4555g3drwdgc1mz03bms9wg75n08i841ijzjnjwx5a2mck") (f (quote (("time" "ahecha_html/time") ("frontend" "ahecha_macro/frontend") ("default") ("chrono" "ahecha_html/chrono") ("backend" "ahecha_macro/backend"))))))

(define-public crate-ahecha-0.0.6 (c (n "ahecha") (v "0.0.6") (d (list (d (n "ahecha_html") (r "^0.0.5") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0.0.6") (d #t) (k 0)))) (h "13kdc2i86a25nqklgl36789g9zq4xa540w5cn9vdmw3p8ik2vf67") (f (quote (("time" "ahecha_html/time") ("frontend" "ahecha_macro/frontend") ("default") ("chrono" "ahecha_html/chrono") ("backend" "ahecha_macro/backend"))))))

(define-public crate-ahecha-0.0.7 (c (n "ahecha") (v "0.0.7") (d (list (d (n "ahecha_html") (r "^0.0.6") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0.0.7") (d #t) (k 0)))) (h "15znn5yg3d30f26x52fh4jqjnybl7clwl7w65y142asg7jfkcbdc") (f (quote (("time" "ahecha_html/time") ("frontend" "ahecha_macro/frontend") ("default") ("chrono" "ahecha_html/chrono") ("backend" "ahecha_macro/backend"))))))

(define-public crate-ahecha-0.0.8 (c (n "ahecha") (v "0.0.8") (d (list (d (n "ahecha_html") (r "^0.0.7") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0.0.8") (d #t) (k 0)))) (h "0bsd54xdg24328134l5g3h2sdm0bidlzr96by3r8926x5gjgs4cp") (f (quote (("time" "ahecha_html/time") ("frontend" "ahecha_macro/frontend") ("default") ("chrono" "ahecha_html/chrono") ("backend" "ahecha_macro/backend"))))))

(define-public crate-ahecha-0.0.9 (c (n "ahecha") (v "0.0.9") (d (list (d (n "ahecha_html") (r "^0.0.8") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0.0.8") (d #t) (k 0)))) (h "0cx3ic542dmksmj4lxa3fwp1fdcqvb058isw0550838mjk4dflf4") (f (quote (("time" "ahecha_html/time") ("frontend" "ahecha_macro/frontend") ("default") ("chrono" "ahecha_html/chrono") ("backend" "ahecha_macro/backend"))))))

(define-public crate-ahecha-0.0.10 (c (n "ahecha") (v "0.0.10") (d (list (d (n "ahecha_html") (r "^0.0.9") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0.0.9") (d #t) (k 0)))) (h "1j1cfgnlgffxsqwkffs7f2zjmpng0p3qivz1jvvz8b1ci2qbd6by") (f (quote (("time" "ahecha_html/time") ("partials" "ahecha_html/partials") ("frontend" "ahecha_macro/frontend") ("default") ("chrono" "ahecha_html/chrono") ("backend" "ahecha_macro/backend"))))))

(define-public crate-ahecha-0.0.11 (c (n "ahecha") (v "0.0.11") (d (list (d (n "ahecha_html") (r "^0.0.9") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0.0.10") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0h1g3sim9f92r212qm8m8r1wbv22iiz6s8dsf6b7vb79dbkvy6ma") (f (quote (("time" "ahecha_html/time") ("partials" "ahecha_html/partials") ("frontend" "ahecha_macro/frontend") ("default") ("backend" "ahecha_macro/backend"))))))

(define-public crate-ahecha-0.0.12 (c (n "ahecha") (v "0.0.12") (d (list (d (n "ahecha_html") (r "^0.0.10") (d #t) (k 0)) (d (n "ahecha_macro") (r "^0.0.10") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "067ivk1dclgl79bs2hijcd62pnl8lc01m66kli4hk32pcdvc1k61") (f (quote (("time" "ahecha_html/time") ("partials" "ahecha_html/partials") ("frontend" "ahecha_macro/frontend") ("default") ("backend" "ahecha_macro/backend"))))))

