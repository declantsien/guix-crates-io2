(define-module (crates-io ah uf ahuff) #:use-module (crates-io))

(define-public crate-ahuff-0.1.0 (c (n "ahuff") (v "0.1.0") (d (list (d (n "endio_bit") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.9") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "14244raw7zzr3c8w4m4gb4ygik19fqa4w3qdhdvdd26flxr6zb31")))

(define-public crate-ahuff-0.1.1 (c (n "ahuff") (v "0.1.1") (d (list (d (n "endio_bit") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.9") (f (quote ("const_generics"))) (d #t) (k 0)))) (h "13qp3hcnpi8if20nb55cnq5c28r62cm6daahz0h7lpmxa90l79hd")))

