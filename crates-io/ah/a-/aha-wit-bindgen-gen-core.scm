(define-module (crates-io ah a- aha-wit-bindgen-gen-core) #:use-module (crates-io))

(define-public crate-aha-wit-bindgen-gen-core-0.0.0 (c (n "aha-wit-bindgen-gen-core") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wit-parser") (r "^0.0.0") (d #t) (k 0) (p "aha-wit-parser")))) (h "1zvrarhrplv34v94ik06p11f6yd807ns234i5kszi37dh1l6ykzk")))

(define-public crate-aha-wit-bindgen-gen-core-0.0.1 (c (n "aha-wit-bindgen-gen-core") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wit-parser") (r "^0.0.2") (d #t) (k 0) (p "aha-wit-parser")))) (h "1gpvcyfisbigsff9vqaqgb39lszbfyn8mwga2002q8n6xcjnvsbs")))

(define-public crate-aha-wit-bindgen-gen-core-0.1.0 (c (n "aha-wit-bindgen-gen-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wit-parser") (r "^0.1.0") (d #t) (k 0) (p "aha-wit-parser")))) (h "1zj71i9j9sa6cb7clq8zmav3rmibgrsnssnkny8lrk11mf7da7bs")))

(define-public crate-aha-wit-bindgen-gen-core-0.2.0 (c (n "aha-wit-bindgen-gen-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wit-parser") (r "^0.2.0") (d #t) (k 0) (p "aha-wit-parser")))) (h "1pynxyiycik451rnhibh8qbw9jkrysyfk1m7j6hmh8wf5prr7qzm")))

