(define-module (crates-io ah q_ ahq_store_rs) #:use-module (crates-io))

(define-public crate-ahq_store_rs-0.1.0 (c (n "ahq_store_rs") (v "0.1.0") (d (list (d (n "ahq_store_rs_core") (r "^0.1.0") (d #t) (k 0)))) (h "0qyr931h4xadw942w2z8n243gs2bci1k0r8xq7kz1zsji04k2ni2") (y #t)))

(define-public crate-ahq_store_rs-0.10.0-alpha.1.1 (c (n "ahq_store_rs") (v "0.10.0-alpha.1.1") (d (list (d (n "ahq_store_rs_core") (r "^0.1.0") (d #t) (k 0)))) (h "0q8xbirrdmpc3h1k1a6wbyplb2hvv9x9ll2m5liqj9jbhzm6rw3j") (y #t)))

(define-public crate-ahq_store_rs-0.10.0-alpha.1.8 (c (n "ahq_store_rs") (v "0.10.0-alpha.1.8") (d (list (d (n "ahq_store_rs_core") (r "^0.10.0-alpha.1.8") (d #t) (k 0)))) (h "0aiv3bmqv7h9syr3mmpjm0hqib5a73c49grx5wxnj9h8syj5cyhb") (y #t)))

