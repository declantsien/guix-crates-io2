(define-module (crates-io ah q_ ahq_store_rs_core) #:use-module (crates-io))

(define-public crate-ahq_store_rs_core-0.1.0 (c (n "ahq_store_rs_core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (d #t) (k 0)))) (h "16k20wv8iiinnicxf385v2gyjzan0k6r1cf72biwg2vvgl03p99g") (y #t)))

(define-public crate-ahq_store_rs_core-0.10.0-alpha.1.1 (c (n "ahq_store_rs_core") (v "0.10.0-alpha.1.1") (d (list (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (d #t) (k 0)))) (h "1aklzrbbkl7z8f1fcrk9z6axbx79nizgsr6s0hhsyngqb0w337y1") (y #t)))

(define-public crate-ahq_store_rs_core-0.10.0-alpha.1.6 (c (n "ahq_store_rs_core") (v "0.10.0-alpha.1.6") (d (list (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 1)) (d (n "tungstenite") (r "^0.19.0") (d #t) (k 0)))) (h "0xy8a1kkbfqkmzvarx78z8frr8d9vnws3gkkyzqdxm15z08dp1dk") (y #t)))

(define-public crate-ahq_store_rs_core-0.10.0-alpha.1.7 (c (n "ahq_store_rs_core") (v "0.10.0-alpha.1.7") (d (list (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 1)) (d (n "tungstenite") (r "^0.19.0") (d #t) (k 0)))) (h "1rpdism6lr1hdizssrl39yb9x0w8860rlr6jgz62flh5w3lb937m") (y #t)))

(define-public crate-ahq_store_rs_core-0.10.0-alpha.1.8 (c (n "ahq_store_rs_core") (v "0.10.0-alpha.1.8") (d (list (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 1)) (d (n "tungstenite") (r "^0.19.0") (d #t) (k 0)))) (h "1ibj1ljdsjpkrylh4c0bnkflgyw1bgzjnigbq96i23vg6l9ma53x") (y #t)))

