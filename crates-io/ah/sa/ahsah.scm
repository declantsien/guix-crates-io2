(define-module (crates-io ah sa ahsah) #:use-module (crates-io))

(define-public crate-ahsah-0.1.0 (c (n "ahsah") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v22rvb8j38mbj8i390hqsn69kgy2pad0rycq7pb378pjp2hdiah")))

(define-public crate-ahsah-0.1.1 (c (n "ahsah") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dnljg2c5874vsyppf8yyxf89809cf043ljxvgkj3402p9xfmv1h") (s 2) (e (quote (("args" "dep:clap"))))))

(define-public crate-ahsah-0.1.2 (c (n "ahsah") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "023gifvk0mf17pyyjzqkfj43kps122c6rf1j50adhpnpnwivqxz1") (s 2) (e (quote (("args" "dep:clap"))))))

(define-public crate-ahsah-0.1.3 (c (n "ahsah") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0634vm98s4jvia3nzw73l705nrydjnmsshwazzfd8s8hwxyhd5z7") (s 2) (e (quote (("args" "dep:clap"))))))

(define-public crate-ahsah-0.1.4 (c (n "ahsah") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kl7hccnwgi2d8f2rnpk370wjk8m7c5dsa0j9df8scmhrglx4ckd") (s 2) (e (quote (("args" "dep:clap"))))))

(define-public crate-ahsah-0.1.5 (c (n "ahsah") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10s0ynk1204805j1q1yd5xp4m2yvywxbgabgzvxhj0c3m9w6xivj") (s 2) (e (quote (("args" "dep:clap"))))))

(define-public crate-ahsah-1.0.0 (c (n "ahsah") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02y742gi8q27m88dwi2k3vr09n1rgnyqxs518wbj9nk483i3k5bd") (s 2) (e (quote (("args" "dep:clap"))))))

