(define-module (crates-io ah t1 aht10) #:use-module (crates-io))

(define-public crate-aht10-0.0.1 (c (n "aht10") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)))) (h "0d7g5nl31w652b6zvaxhds4nm41nkflhk444dbxrf0f2j54m09wl")))

