(define-module (crates-io ah t1 aht10-async) #:use-module (crates-io))

(define-public crate-aht10-async-0.0.1 (c (n "aht10-async") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.1") (d #t) (k 0)))) (h "0qgqsmyic729hjadr1nkfkf1y0m2mb3ijndzcm5kak19s4abmzbx")))

(define-public crate-aht10-async-0.1.0 (c (n "aht10-async") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)))) (h "03v9zsf56vkw8rh8ih6ggdglhd03pgqbnrl6w1b7r423ci823ywf")))

