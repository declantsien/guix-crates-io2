(define-module (crates-io ah t1 aht10-embedded) #:use-module (crates-io))

(define-public crate-aht10-embedded-0.0.1 (c (n "aht10-embedded") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7.3") (d #t) (k 2)) (d (n "embedded-alloc") (r "^0.5.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "fugit") (r "^0.3.7") (d #t) (k 2)) (d (n "rp-pico") (r "^0.8.0") (f (quote ("rp2040-e5"))) (d #t) (k 2)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 2)) (d (n "usbd-serial") (r "^0.1.1") (d #t) (k 2)))) (h "0vxs1czls1iqnfskkf38d5xq9fgqs39k3kp1fknzfny7ray0awfw")))

