(define-module (crates-io ah oy ahoy) #:use-module (crates-io))

(define-public crate-ahoy-0.0.0 (c (n "ahoy") (v "0.0.0") (h "1b8jy2zbw4lz4vj4sikbrk8hnfcbg9fhapn6zgb16yplsvn8sac9")))

(define-public crate-ahoy-0.0.1 (c (n "ahoy") (v "0.0.1") (h "1ilr16xa2bmkbcpl7hv8z62jypdmr77x2yyh36fww9qlxdgk7kcn")))

