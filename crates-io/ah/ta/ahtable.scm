(define-module (crates-io ah ta ahtable) #:use-module (crates-io))

(define-public crate-ahtable-0.1.0 (c (n "ahtable") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "16lrblnil8c82gr7jhzbgrj8bjdr6bwgn8dh983pzh4lq7vw4g28")))

(define-public crate-ahtable-0.1.1 (c (n "ahtable") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "08bg998amq6sk8xfli1y29jy85rpfrw2yyj86s894nvr25775rgk")))

(define-public crate-ahtable-0.1.2 (c (n "ahtable") (v "0.1.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "01qznjng2izkagcbg49iajpdnn784a4ard8gx86bqxgznacflbm1")))

(define-public crate-ahtable-0.1.3 (c (n "ahtable") (v "0.1.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "0apsi0qq4g64vjfijkh8diw6s4ph58rcnzy0c1l644v65xqi26ra")))

(define-public crate-ahtable-0.1.4 (c (n "ahtable") (v "0.1.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "0ykxm6986j05b3bn5x1lxwx4iwwrjmh760b9fcghwcvbwzcg5db7")))

(define-public crate-ahtable-0.1.5 (c (n "ahtable") (v "0.1.5") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "055zdj17mpx0awnfa93hpsp0k1smk7ah63q5da2wazd4csp0gl03")))

(define-public crate-ahtable-0.2.0 (c (n "ahtable") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "twox-hash") (r "^1.5") (d #t) (k 0)))) (h "17kfj3yih0v4v4myqzibfdlnp1amxmlrw1ghdpz2d448nalk4m71")))

