(define-module (crates-io ah ri ahri) #:use-module (crates-io))

(define-public crate-ahri-1.0.0 (c (n "ahri") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1msq9nqm8a6c72wn86s76ala38a82d89njfj94d4amkqfhya9dz7")))

(define-public crate-ahri-1.0.1 (c (n "ahri") (v "1.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "mysql") (r "^24.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0a9wnr1qaxb1q816z27idbhflbz54flgh3q9kpl72b6id68i8dbs")))

