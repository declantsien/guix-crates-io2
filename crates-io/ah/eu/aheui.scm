(define-module (crates-io ah eu aheui) #:use-module (crates-io))

(define-public crate-aheui-0.0.1 (c (n "aheui") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)) (d (n "rpaheui") (r "^0.0.1") (f (quote ("clap"))) (d #t) (k 0)))) (h "1zqdy721ka9vvngavyznki2g4wr9k4jgdql8fynhk0sgb81cjz9g") (f (quote (("default" "bin") ("bin" "clap" "num-traits")))) (r "1.67.1")))

