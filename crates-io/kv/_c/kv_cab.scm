(define-module (crates-io kv _c kv_cab) #:use-module (crates-io))

(define-public crate-kv_cab-0.2.0 (c (n "kv_cab") (v "0.2.0") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qb9c1ljapig0qcccbn4cmg4kw1kxfb7skwhq2swmlm9xwg9x2df")))

(define-public crate-kv_cab-0.2.1 (c (n "kv_cab") (v "0.2.1") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0wifhzcy4cvqrkabiyaw2p4scgp23iy3kq4qpaqdgj66qr189i6a")))

(define-public crate-kv_cab-0.2.2 (c (n "kv_cab") (v "0.2.2") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "16jrgg1mi9kqrqpki4mk67bh4drakv5hi6xpmam9w1s26xb2vzrw")))

(define-public crate-kv_cab-0.2.3 (c (n "kv_cab") (v "0.2.3") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0rmzd7fakvq3vill8n4lmq3ryz799hkilwaf9i5lqsa3vd7yfmjn")))

(define-public crate-kv_cab-0.3.0 (c (n "kv_cab") (v "0.3.0") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "03hwdbrf6lkr2k1rhxyfskks1xagsarai9bkmq1hdlxrg50wrgcq")))

(define-public crate-kv_cab-0.3.1 (c (n "kv_cab") (v "0.3.1") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ybzza65m25pv0bkd0kgx81v1lrkcp62zxdm7hwj8gzwqa15dj91")))

(define-public crate-kv_cab-0.4.0 (c (n "kv_cab") (v "0.4.0") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0jmz4czs1lxjqq2lh2xc9j803vd0p8sfw5y63blhyy919825vyzr")))

(define-public crate-kv_cab-0.4.1 (c (n "kv_cab") (v "0.4.1") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ls80a234yax88m49k3dyn3np90wv0scfaj51lb179z4i2mg2024")))

(define-public crate-kv_cab-0.4.2 (c (n "kv_cab") (v "0.4.2") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1j4psyfad566dhv7vvbaps7z1qpkx4w2vn9bkh6nb98lmf642bn3")))

(define-public crate-kv_cab-0.4.3 (c (n "kv_cab") (v "0.4.3") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1hixgwk39v11v6zbdzfhmj501903cmacklmw6dhrlbla3139g4gb")))

(define-public crate-kv_cab-0.4.4 (c (n "kv_cab") (v "0.4.4") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1inmya8y0s49khnz49n2d81khkv7bwk4n7wrwgbr6i7b6zkmkphg")))

(define-public crate-kv_cab-0.4.5 (c (n "kv_cab") (v "0.4.5") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1h0a50n73zc1jxii3f7n9i6aijcfr0ym5h7h6gm0nycapr08nx8x")))

(define-public crate-kv_cab-0.4.6 (c (n "kv_cab") (v "0.4.6") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0gqiszkw5h9y62psg9b6l4vi0nprdvaxs5hfsjgw21kafx3fg20k")))

(define-public crate-kv_cab-0.5.0 (c (n "kv_cab") (v "0.5.0") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "fs2") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0305i811h6n6hh5qlh985cdfvryp36nwirlvjqxsxrf4dbl1hls4")))

(define-public crate-kv_cab-0.5.1 (c (n "kv_cab") (v "0.5.1") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "fs2") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13qi4l1i3s790zpwzkjjc2irgzlm9jhb5iz9aks9sykklma70n7b")))

(define-public crate-kv_cab-0.6.0 (c (n "kv_cab") (v "0.6.0") (d (list (d (n "bincode") (r "^0.7") (d #t) (k 0)) (d (n "fs2") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)))) (h "1w4cmya5g11bwmyajf5679fq7fk83xwsmnl7yfqxs27sp0y3xb15")))

