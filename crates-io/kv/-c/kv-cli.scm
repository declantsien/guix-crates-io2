(define-module (crates-io kv -c kv-cli) #:use-module (crates-io))

(define-public crate-kv-cli-0.1.0 (c (n "kv-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.0.10") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "080mnz2in4idqqaiaq2lrhjwdfhvjh1zv5mw48h2xxj7glm90xi4")))

