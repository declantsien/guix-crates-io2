(define-module (crates-io kv -c kv-crud-core) #:use-module (crates-io))

(define-public crate-kv-crud-core-0.1.0 (c (n "kv-crud-core") (v "0.1.0") (h "1357qfkjkgbsfrjcny96fnk0arfz3l1nkskb0nfnlpcvslff35lh")))

(define-public crate-kv-crud-core-0.1.1 (c (n "kv-crud-core") (v "0.1.1") (h "1929hcs6ffkkmxk20s1g7rddsfy3cy4s168igxv63dpjd84skb8b")))

