(define-module (crates-io kv st kvstore) #:use-module (crates-io))

(define-public crate-kvstore-0.1.0 (c (n "kvstore") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 0)))) (h "01icr8ih6yjv0f4qrlnvbjxp3hpz74kwd8w3p7xhds3537fl781a")))

(define-public crate-kvstore-0.2.0 (c (n "kvstore") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 0)))) (h "12zq9bgj7a8wfgp8af8qidypdislg58bjqqj5pxml91dc69avhk7")))

(define-public crate-kvstore-0.2.1 (c (n "kvstore") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.2") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 0)))) (h "01f97m2dg667dmsvkrmagmrb4scsbc4xc1a8s9y746f8f71i3fqh")))

