(define-module (crates-io kv st kvstructs) #:use-module (crates-io))

(define-public crate-kvstructs-0.0.1 (c (n "kvstructs") (v "0.0.1") (d (list (d (n "bytes") (r "^1.1") (k 0)))) (h "1hbylcz8vc00ddw2bn0507s8fh2i3y0wzp8wlp8xcm1ilr2ngb49") (f (quote (("std" "bytes/std") ("default" "std"))))))

(define-public crate-kvstructs-0.0.2 (c (n "kvstructs") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "0mbd8x041g02fxdcixd7m2vhlqnlsn1c9yzi7nm00cb21rph0bq0") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.3 (c (n "kvstructs") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "1mrjib7435lqycx1263i8i3g8w3158pb9d53iabq7l8rb1qqyd61") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.4 (c (n "kvstructs") (v "0.0.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "0hhahd3xgsvyfcg87rz0i4ln3pd9lcp8pvf05n7p26zzkh9qbkrf") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.5 (c (n "kvstructs") (v "0.0.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "1kbq913r2lllfvmczj0pvc36kv05zq14ji2q852k0v9ad5c0x8r6") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.6 (c (n "kvstructs") (v "0.0.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "0hshh2gwq6kszwv3xjvfd3qhjr5i70178s3bi10y6zrwggjg7g94") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.7 (c (n "kvstructs") (v "0.0.7") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "0syghkcaz84qb26p3zbci3diiwikgvh8s5h022crh06cy4m406y1") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.8 (c (n "kvstructs") (v "0.0.8") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "10dp1x95zx46wzak34d8km78s8as1yy3z005h6gy6z1cmp724hki") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.9 (c (n "kvstructs") (v "0.0.9") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "1hlpa6ljb7lsi2rikpbs98x9nhk193df12hhv2cgq1jfmx6zi30z") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.10 (c (n "kvstructs") (v "0.0.10") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "1jr0j1rczl0r7djnqlbd9y59bvmabzd8lpg3va27aisj4rfxqs7k") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.11 (c (n "kvstructs") (v "0.0.11") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "0qy3ng255jzy7aknw5qsl1a4xdxnp02qqxvym5mf1zvcaqbsqsh7") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.12 (c (n "kvstructs") (v "0.0.12") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "16hcz2a1kjxlrdnw35yy0vi6mjw8cf03dgxi2nnn9903ra23c97v") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.13 (c (n "kvstructs") (v "0.0.13") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "07w6qwq24fqy3frinasr8c8pj0xjscrmabsl169dvc9pnjar2ywx") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.14 (c (n "kvstructs") (v "0.0.14") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)))) (h "08mjmncfdkz555yb77s6b7c4zjbz4sglny7wyh5x1d1ackn7xrnl") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.15 (c (n "kvstructs") (v "0.0.15") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "03lz59nk7lr9sg5wm5gg7pq9ifg3pgqckjjhndrsd3vfnzvbdj0q") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.16 (c (n "kvstructs") (v "0.0.16") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "1ws10484fxj3zyz02s8v60ybgan2gypd4hlbccabr159q6j0il7r") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.17 (c (n "kvstructs") (v "0.0.17") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "15dq96c5kw2qaq5cc36yknyi37clwgbvnf9zg9xqb02xmd9qgfk3") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.18 (c (n "kvstructs") (v "0.0.18") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "1vycblf4l3nhqnrfg2cyh5nbrw2dzbgjmwkrsrhfvfrlw0iyvk25") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.19 (c (n "kvstructs") (v "0.0.19") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "0wddv5717xykaifx6wbrhf9s5g0zczlf6j74n7y8fa25vylpyfyk") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.20 (c (n "kvstructs") (v "0.0.20") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "1mqlg75vb0qxnrh0mg0m02kfspzsi61v6qc5ajwpcm2gxz7idkcm") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.21 (c (n "kvstructs") (v "0.0.21") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "1gqvb4rjwicjpzlbywwjx6gzjk8wsg4549idbpgshcgsifz2y1fp") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.22 (c (n "kvstructs") (v "0.0.22") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "1k720z1i30ifvd8vy0ydcsacslfalsx9cyci659li10r7f7y7f3c") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.23 (c (n "kvstructs") (v "0.0.23") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "0swx0p1s9wm0vrxjpkflik53ni5b3if2xpccadvpa0hywjssv9yd") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.24 (c (n "kvstructs") (v "0.0.24") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "0dk65gxy3ch3b2v5ij4zqs77bbmc02z2a91wff7bnyx472k7d47r") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.1.0 (c (n "kvstructs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "1w6qhh988jpni0df5rn0qfdgqgd841y0r4hl7zddcwlvjfi2krgw") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.1.1 (c (n "kvstructs") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.2") (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)))) (h "0g608phzi6h5m9cxs1z5jpnd34dcn59d4b7mz1am94fsyqibipmr") (f (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

