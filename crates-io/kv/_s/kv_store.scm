(define-module (crates-io kv _s kv_store) #:use-module (crates-io))

(define-public crate-kv_store-0.1.0 (c (n "kv_store") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "10f1221ilzx07h1ydcy9pgx9x9hn31h241ml1cdrmdp4nrk9wv8j") (s 2) (e (quote (("impl" "dep:parking_lot"))))))

(define-public crate-kv_store-0.1.1 (c (n "kv_store") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0l3g9waz3ag8ad68zap7x62jj386r4hcdz1xbrxdyjhzvgjrj25m") (s 2) (e (quote (("impl" "dep:parking_lot"))))))

