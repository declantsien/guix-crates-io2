(define-module (crates-io kv fe kvfeeds) #:use-module (crates-io))

(define-public crate-kvfeeds-0.1.0-alpha.1 (c (n "kvfeeds") (v "0.1.0-alpha.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "custom_codes") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "timeit") (r "^0.1.2") (d #t) (k 2)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "0csn10rnac9bwldchzycdlz6n0rrw7b0yxl9c8jywx6yjz057ck4") (y #t)))

