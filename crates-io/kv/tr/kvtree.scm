(define-module (crates-io kv tr kvtree) #:use-module (crates-io))

(define-public crate-kvtree-0.1.0 (c (n "kvtree") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "tuplify") (r "^1.1.4") (d #t) (k 0)))) (h "1zzq1xf7z908dlw8cb9id6a1sbrhk0far4grxl0i3zkcydp4hvyi") (f (quote (("default" "rayon" "core-storage") ("core-storage")))) (s 2) (e (quote (("rayon" "dep:rayon" "indexmap/rayon"))))))

