(define-module (crates-io kv db kvdb-shared-tests) #:use-module (crates-io))

(define-public crate-kvdb-shared-tests-0.1.0 (c (n "kvdb-shared-tests") (v "0.1.0") (d (list (d (n "kvdb") (r "^0.3") (d #t) (k 0)))) (h "1w5f4782nbgy6lyw6fgbxskzyx5qb280zxhpd9agy0nnvlhdd18b")))

(define-public crate-kvdb-shared-tests-0.2.0 (c (n "kvdb-shared-tests") (v "0.2.0") (d (list (d (n "kvdb") (r "^0.4") (d #t) (k 0)))) (h "1537s7md4vjrz8qx5c1aiz0qwdg343f1z7gwnm8xv6r97cgf82vj")))

(define-public crate-kvdb-shared-tests-0.4.0 (c (n "kvdb-shared-tests") (v "0.4.0") (d (list (d (n "kvdb") (r "^0.6") (d #t) (k 0)))) (h "14b6ihn4nqz6fbfpmmrn4c14fg2p9aa3p575fm1v5c97py940m40")))

(define-public crate-kvdb-shared-tests-0.5.0 (c (n "kvdb-shared-tests") (v "0.5.0") (d (list (d (n "kvdb") (r "^0.7") (d #t) (k 0)))) (h "1sydxp627skw7499ib2q3b5jxff31ckxiz9yh7vc3qpibf8bk345")))

(define-public crate-kvdb-shared-tests-0.6.0 (c (n "kvdb-shared-tests") (v "0.6.0") (d (list (d (n "kvdb") (r "^0.8") (d #t) (k 0)))) (h "1h573cyna767cf12pj426ql8nfl838j48s1ma3kykf3pb1dj2s48")))

(define-public crate-kvdb-shared-tests-0.7.0 (c (n "kvdb-shared-tests") (v "0.7.0") (d (list (d (n "kvdb") (r "^0.9") (d #t) (k 0)))) (h "1zasw7bqv3b179232jw6w04fziqqyvxdng2azw7z4kaa6n30r4bq")))

(define-public crate-kvdb-shared-tests-0.8.0 (c (n "kvdb-shared-tests") (v "0.8.0") (d (list (d (n "kvdb") (r "^0.10") (d #t) (k 0)))) (h "0v2qnapzwhl2pcadxhv3r6d6hwg4gksv4abvypff1jiijasy8psp")))

(define-public crate-kvdb-shared-tests-0.9.0 (c (n "kvdb-shared-tests") (v "0.9.0") (d (list (d (n "kvdb") (r "^0.11") (d #t) (k 0)))) (h "13p6a08spxf69i4q45pm43hf59hq8v7iahm87r8a36j5sgnh343s") (r "1.56.1")))

(define-public crate-kvdb-shared-tests-0.10.0 (c (n "kvdb-shared-tests") (v "0.10.0") (d (list (d (n "kvdb") (r "^0.12") (d #t) (k 0)))) (h "1gprq790njzziav5wycccg092qb2k82nvm8d17095qddnyns30ny") (r "1.56.1")))

(define-public crate-kvdb-shared-tests-0.11.0 (c (n "kvdb-shared-tests") (v "0.11.0") (d (list (d (n "kvdb") (r "^0.13") (d #t) (k 0)))) (h "0ha50bwvf104gh7mg9cibql052lvpbgardk4acdrydhcx3iv9lv4") (r "1.56.1")))

