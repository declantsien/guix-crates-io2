(define-module (crates-io kv db kvdb) #:use-module (crates-io))

(define-public crate-kvdb-0.1.0 (c (n "kvdb") (v "0.1.0") (d (list (d (n "elastic-array") (r "^0.10") (d #t) (k 0)) (d (n "parity-bytes") (r "^0.1") (d #t) (k 0)))) (h "0im59w22skfg37yxn40n4aa5h4wn9d2mm7dk2hhc6cgadhh8kbkj")))

(define-public crate-kvdb-0.1.1 (c (n "kvdb") (v "0.1.1") (d (list (d (n "bytes") (r "^0.1") (d #t) (k 0) (p "parity-bytes")) (d (n "elastic-array") (r "^0.10.2") (d #t) (k 0)))) (h "1qv99y3y5a4gzm21g0jxk7ggfmnq0wvmdcmxd9128whsy18z5cn1")))

(define-public crate-kvdb-0.2.0 (c (n "kvdb") (v "0.2.0") (d (list (d (n "bytes") (r "^0.1") (d #t) (k 0) (p "parity-bytes")) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0m056sfm0ns798c1yzi1z0qrxw4n25yjmm8h8wlbixklbbcfiknf")))

(define-public crate-kvdb-0.3.0 (c (n "kvdb") (v "0.3.0") (d (list (d (n "bytes") (r "^0.1") (d #t) (k 0) (p "parity-bytes")) (d (n "parity-util-mem") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0qna7apfhmjggldf31sp9my35jblfn39kmwr7hc42qbv7bfq0l5x")))

(define-public crate-kvdb-0.3.1 (c (n "kvdb") (v "0.3.1") (d (list (d (n "bytes") (r "^0.1") (d #t) (k 0) (p "parity-bytes")) (d (n "parity-util-mem") (r "^0.4") (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1n3ypx6mvcq6m5pgwwv80mxaimydhxs01cphgyzx3k31al7bx5l3")))

(define-public crate-kvdb-0.4.0 (c (n "kvdb") (v "0.4.0") (d (list (d (n "bytes") (r "^0.1") (d #t) (k 0) (p "parity-bytes")) (d (n "parity-util-mem") (r "^0.5") (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "11zxb1lq7g9c5lnq1dvzxbi5xyypsnnxcs7mm5nrkka2dzz0l203")))

(define-public crate-kvdb-0.5.0 (c (n "kvdb") (v "0.5.0") (d (list (d (n "parity-util-mem") (r "^0.6") (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1rbw1bjz25sy3qvail46yp9y7l2nsfpc8ddyz8kz0blvhk39dl6a")))

(define-public crate-kvdb-0.6.0 (c (n "kvdb") (v "0.6.0") (d (list (d (n "parity-util-mem") (r "^0.6") (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0fjanp988b2d5a1cznv7y2hsh0sz7fyfilb1h2a4gfh0nnlv4qz7")))

(define-public crate-kvdb-0.7.0 (c (n "kvdb") (v "0.7.0") (d (list (d (n "parity-util-mem") (r "^0.7") (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "16izwkx8bwf5j5l5qddrv0hxrczjcg1127xk01288cwfd0pyy583")))

(define-public crate-kvdb-0.8.0 (c (n "kvdb") (v "0.8.0") (d (list (d (n "parity-util-mem") (r "^0.8") (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0j7qwlbamjgpfi46dddc1a24zmcyscx2cnfwlrv9kr5dv9426ccj")))

(define-public crate-kvdb-0.9.0 (c (n "kvdb") (v "0.9.0") (d (list (d (n "parity-util-mem") (r "^0.9") (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0498slchqnjj5g6mywg0z614r66wg3apjp8r4hqf747z7s2vv4c8")))

(define-public crate-kvdb-0.10.0 (c (n "kvdb") (v "0.10.0") (d (list (d (n "parity-util-mem") (r "^0.10") (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "04pm51iayhc2ad0haal0xpx4cakj41cv9x97b8h0xv39q26zb8s5")))

(define-public crate-kvdb-0.11.0 (c (n "kvdb") (v "0.11.0") (d (list (d (n "parity-util-mem") (r "^0.11") (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "11hzsqjzwxgkwgmrv43rz3dkndbxlyn9p92p5ip4m7cqnzndh0d3") (r "1.56.1")))

(define-public crate-kvdb-0.12.0 (c (n "kvdb") (v "0.12.0") (d (list (d (n "parity-util-mem") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "16s0a33m00ifab1zpk2z3wy5wchb6nml0rxgx7zrf0dsmp78jl2q") (r "1.56.1")))

(define-public crate-kvdb-0.13.0 (c (n "kvdb") (v "0.13.0") (d (list (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1nddanhh8x6j504myyp9pm5zy13s22spkaf3hxc87xibn3f71mz7") (r "1.56.1")))

