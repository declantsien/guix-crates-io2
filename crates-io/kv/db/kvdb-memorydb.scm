(define-module (crates-io kv db kvdb-memorydb) #:use-module (crates-io))

(define-public crate-kvdb-memorydb-0.1.0 (c (n "kvdb-memorydb") (v "0.1.0") (d (list (d (n "kvdb") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "1aipkl8znf8wg14gr4z82kbh0y9ars6l7y56c7zjqq43n1gdzg25")))

(define-public crate-kvdb-memorydb-0.1.2 (c (n "kvdb-memorydb") (v "0.1.2") (d (list (d (n "kvdb") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0ld3c0f8z3ahvgpi0m3dlnxc6w8rpynhcr10b56vfv6kkqq14v19")))

(define-public crate-kvdb-memorydb-0.2.0 (c (n "kvdb-memorydb") (v "0.2.0") (d (list (d (n "kvdb") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9.0") (d #t) (k 0)))) (h "03gn5kr9x0xalhhvlqhjw47mvcdhziiajdpfwwng1q0z5dqp0p8a")))

(define-public crate-kvdb-memorydb-0.3.0 (c (n "kvdb-memorydb") (v "0.3.0") (d (list (d (n "kvdb") (r "^0.3") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.1") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9.0") (d #t) (k 0)))) (h "1jis5863ggj40frr912pswcqhi9qbln0zm4ar5pv5n981y1f1a22")))

(define-public crate-kvdb-memorydb-0.3.1 (c (n "kvdb-memorydb") (v "0.3.1") (d (list (d (n "kvdb") (r "^0.3") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.1") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.9.0") (d #t) (k 0)))) (h "0ga9jmh1czrbldaz4il9wq33bpirr12937l3qhaqahay2lafy9ad")))

(define-public crate-kvdb-memorydb-0.4.0 (c (n "kvdb-memorydb") (v "0.4.0") (d (list (d (n "kvdb") (r "^0.4") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.2") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.5") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "13kwlds3khd6spjgz39avqj91573x986k4a3xzlagq59wms54ddr")))

(define-public crate-kvdb-memorydb-0.5.0 (c (n "kvdb-memorydb") (v "0.5.0") (d (list (d (n "kvdb") (r "^0.5") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.2") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "1sllwmpa6ndbg6mw865r0gqp3v1y9yrslxzx5n15ijgs5k8m9aaa")))

(define-public crate-kvdb-memorydb-0.6.0 (c (n "kvdb-memorydb") (v "0.6.0") (d (list (d (n "kvdb") (r "^0.6") (d #t) (k 0)) (d (n "parity-util-mem") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "1909hm04avpc6fdy1qva18ijdz040i9mscxpnl1zbrld49g7s0kk")))

(define-public crate-kvdb-memorydb-0.7.0 (c (n "kvdb-memorydb") (v "0.7.0") (d (list (d (n "kvdb") (r "^0.7") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.5") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.7") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "0vn6lyja4jfilnj6ygcpacifwgj75nxnbfyvi6wdyfqa4qmq5pkk")))

(define-public crate-kvdb-memorydb-0.8.0 (c (n "kvdb-memorydb") (v "0.8.0") (d (list (d (n "kvdb") (r "^0.8") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.6") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "1hn8jkn1sq0ba1c2c4q7757p29mclgwr2lvppsp2wsbcs6l54q4q") (f (quote (("default"))))))

(define-public crate-kvdb-memorydb-0.9.0 (c (n "kvdb-memorydb") (v "0.9.0") (d (list (d (n "kvdb") (r "^0.9") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.7") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.9") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "04jrya0iifbzc41f4j3j2f25rf69nsdw23k298w8vw6a127dm81h") (f (quote (("default"))))))

(define-public crate-kvdb-memorydb-0.10.0 (c (n "kvdb-memorydb") (v "0.10.0") (d (list (d (n "kvdb") (r "^0.10") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.8") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.10") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "1xgr84c04w11nwfx9w3s4rwr682hs6kchb7vpz8arxa3qrgvidn3") (f (quote (("default"))))))

(define-public crate-kvdb-memorydb-0.11.0 (c (n "kvdb-memorydb") (v "0.11.0") (d (list (d (n "kvdb") (r "^0.11") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.9") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.11") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0mq34li86i116b46p92g05zph0n8yjk310b2nsp8f4yjmdlfdrzc") (f (quote (("default")))) (r "1.56.1")))

(define-public crate-kvdb-memorydb-0.12.0 (c (n "kvdb-memorydb") (v "0.12.0") (d (list (d (n "kvdb") (r "^0.12") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.10") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.12") (f (quote ("std"))) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0wyq1p3lv6s771masc6rhpvmmqmgq54jd6x4sbnmjxzvgg40kla0") (f (quote (("default")))) (r "1.56.1")))

(define-public crate-kvdb-memorydb-0.13.0 (c (n "kvdb-memorydb") (v "0.13.0") (d (list (d (n "kvdb") (r "^0.13") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.11") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "1ck06xfhn87k9lxsg69c856fgqf6jhndv7qn9vbrrzzrcvz8aymz") (f (quote (("default")))) (r "1.56.1")))

