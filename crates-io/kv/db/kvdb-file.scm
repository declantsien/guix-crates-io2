(define-module (crates-io kv db kvdb-file) #:use-module (crates-io))

(define-public crate-kvdb-file-0.10.0 (c (n "kvdb-file") (v "0.10.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "kvdb") (r "^0.10") (d #t) (k 0)) (d (n "kvdb-memorydb") (r "^0.10") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.8") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.10") (f (quote ("std"))) (k 0)))) (h "0xixydx2mwx0pynf69qrr5zns863b764c1n5zvjq74r1lm3wd85f")))

(define-public crate-kvdb-file-0.11.0 (c (n "kvdb-file") (v "0.11.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "kvdb") (r "^0.11") (d #t) (k 0)) (d (n "kvdb-memorydb") (r "^0.11") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.9") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.11") (f (quote ("std"))) (k 0)))) (h "0lqn050bdmaq6jiyj3nlf4b5kp9k47y2fyl3m0kp5zq3n249h836")))

(define-public crate-kvdb-file-0.12.0 (c (n "kvdb-file") (v "0.12.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "kvdb") (r "^0.12") (d #t) (k 0)) (d (n "kvdb-memorydb") (r "^0.12") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.10") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.12") (f (quote ("std"))) (k 0)))) (h "1qj53b06914mi9zb4pgv7vx85nvyiwyf035h7x7sk3v91w6lxwmd")))

(define-public crate-kvdb-file-0.13.0 (c (n "kvdb-file") (v "0.13.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "kvdb") (r "^0.13") (d #t) (k 0)) (d (n "kvdb-memorydb") (r "^0.13") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.11") (d #t) (k 2)))) (h "1z1m7k71v9z11sbx8bsds4x2z7rdfsqkrj6mjpn1yx91q542fb31")))

