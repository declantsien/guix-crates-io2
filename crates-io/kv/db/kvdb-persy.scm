(define-module (crates-io kv db kvdb-persy) #:use-module (crates-io))

(define-public crate-kvdb-persy-0.1.0 (c (n "kvdb-persy") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.11.0") (d #t) (k 2)) (d (n "parity-util-mem") (r "^0.12.0") (d #t) (k 0)) (d (n "persy") (r "^1.4.0") (f (quote ("background_ops"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)))) (h "1njkhpkf1p5aqw40x0fhazkfigzxpwhsmk64v0vsxgppzzm63si8")))

(define-public crate-kvdb-persy-0.1.1 (c (n "kvdb-persy") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parity-util-mem") (r "^0.12.0") (d #t) (k 0)) (d (n "persy") (r "^1.4.0") (f (quote ("background_ops"))) (d #t) (k 0)) (d (n "kvdb-shared-tests") (r "^0.11.0") (d #t) (k 2)))) (h "0hwa15p05ivjj27ja01n9scfrlbi3lr0girajzp9g6d87synmv4z")))

