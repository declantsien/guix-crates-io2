(define-module (crates-io kv m_ kvm_wrapper) #:use-module (crates-io))

(define-public crate-kvm_wrapper-0.1.0 (c (n "kvm_wrapper") (v "0.1.0") (h "10l3ifkjxw7phri4r5zvklwyhp05s23d08fv14hsp3hzm9fn0zxs") (f (quote (("kvm-v4_20_0") ("kvm-v4_14_0")))) (y #t)))

(define-public crate-kvm_wrapper-0.1.1 (c (n "kvm_wrapper") (v "0.1.1") (h "0gpwj8gq9s63g3flzkpqdgm8kinrxh3i6819xwapp8033nxjjycy") (f (quote (("kvm-v4_20_0") ("kvm-v4_14_0")))) (y #t)))

(define-public crate-kvm_wrapper-0.1.2 (c (n "kvm_wrapper") (v "0.1.2") (h "1vld91r9hmpm3r4226a0zc6kaya3d4fsvnzxihls3anmam3p3pzj") (f (quote (("kvm-v4_20_0") ("kvm-v4_14_0")))) (y #t)))

