(define-module (crates-io kv -d kv-derive-macro) #:use-module (crates-io))

(define-public crate-kv-derive-macro-0.3.0-alpha.1 (c (n "kv-derive-macro") (v "0.3.0-alpha.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "1fbk7ghs87k22dlm391c280qfwp7s01sypdnkbaf0lh5xrfp9kd6")))

(define-public crate-kv-derive-macro-1.0.0-rc.1 (c (n "kv-derive-macro") (v "1.0.0-rc.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "0npqxxqcdqnvmhl8y7b9wrl25prdibmrmnsd35xzk3sb02ys0cj5")))

(define-public crate-kv-derive-macro-1.0.0-rc.2 (c (n "kv-derive-macro") (v "1.0.0-rc.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.0-rc.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "10dwfl77bgf5zy6zb90dwq79mnda64p6a48fmafaj64wa3w7vfc7")))

(define-public crate-kv-derive-macro-1.0.0-rc.3 (c (n "kv-derive-macro") (v "1.0.0-rc.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "1ph031bm7x2v239hn0jgr7cqm75xrjqjhcbp962mwyplz5mz8cgl")))

(define-public crate-kv-derive-macro-1.0.0-rc.4 (c (n "kv-derive-macro") (v "1.0.0-rc.4") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.0-rc.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "15fn018lqz6fwpdmk0bkmm2d5paxldw9vxbrk6ydfbmqx9i2vh3l")))

(define-public crate-kv-derive-macro-1.0.0-rc.5 (c (n "kv-derive-macro") (v "1.0.0-rc.5") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.0-rc.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "0i6b26ppixrkffd5nw2l44p0fyiz93zwf6bqgvbb8mlambv14kv8")))

(define-public crate-kv-derive-macro-1.0.0-rc.6 (c (n "kv-derive-macro") (v "1.0.0-rc.6") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.0-rc.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "0d99wmswn9g519v1gp5yhba1xaglmlm5r7cpqjxv9kpngrnpj9mx")))

(define-public crate-kv-derive-macro-1.0.0-rc.7 (c (n "kv-derive-macro") (v "1.0.0-rc.7") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.0-rc.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "1lv7kfr5vrnpdxsp8jyv7ffkh2lx8civsq9k6g7sw6j6wk354qmq")))

(define-public crate-kv-derive-macro-1.0.0-rc.8 (c (n "kv-derive-macro") (v "1.0.0-rc.8") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.0-rc.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "0aff5yq4ycczz1bylk05c7kcnz335jqxgj50kfbs8li86g7aw66i")))

(define-public crate-kv-derive-macro-1.0.0-rc.9 (c (n "kv-derive-macro") (v "1.0.0-rc.9") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.0-rc.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "1dh1jwdgywr1cyxxpzvnjz7i2xxjg00fybijy3brlbcs3il0q6z0")))

(define-public crate-kv-derive-macro-1.0.0 (c (n "kv-derive-macro") (v "1.0.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "1cjhv6jbnkzhkxxg4z2izwgkj0f0053kh399ji34ggf6vxxnl55y")))

(define-public crate-kv-derive-macro-1.0.1 (c (n "kv-derive-macro") (v "1.0.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "18df7n9s9qg073dca5gv7v3qkngn49wbwykllgg1q6a1dbsqjv3h")))

