(define-module (crates-io kv -d kv-derive-impl) #:use-module (crates-io))

(define-public crate-kv-derive-impl-0.1.0-alpha.3 (c (n "kv-derive-impl") (v "0.1.0-alpha.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "0l3vj7i1hnxi84nvgm06x3wq70zkjf7ay56wmiyzxq4rzr3dahrk") (f (quote (("default")))) (y #t)))

(define-public crate-kv-derive-impl-0.1.0-alpha.4 (c (n "kv-derive-impl") (v "0.1.0-alpha.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "0wxy8y3rrm9iyk2dyqwfrp84pax3xs6r5dv1sgavyivvk9mv3kam") (f (quote (("default")))) (y #t)))

(define-public crate-kv-derive-impl-0.1.0-alpha.5 (c (n "kv-derive-impl") (v "0.1.0-alpha.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "0qqycmv5y8mm255lgj6zis0g2hra09mkd7sls0jiv26j3a66bv12") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.6 (c (n "kv-derive-impl") (v "0.1.0-alpha.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "1gnxajdl9fmcv8bwmwqzhcc6jxgf81nfl8zj3kpvn7hdy9idr7jr") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.7 (c (n "kv-derive-impl") (v "0.1.0-alpha.7") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "181qdwhr0pzgq4wi1szyynh68vpgch7xiz7pv4wmkp7i110kr2r2") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.8 (c (n "kv-derive-impl") (v "0.1.0-alpha.8") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "0dp53f5fvwky6v3555yqdzv4jdc5qihsshn5kqvw7lphnxzlz3nx") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.9 (c (n "kv-derive-impl") (v "0.1.0-alpha.9") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "19in2zxpjmayzajcgnzy9n1xf05wnrmikxp2q34b30hlnab1p9m9") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.10 (c (n "kv-derive-impl") (v "0.1.0-alpha.10") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "0kmbjxk3x3q65gi3qxp0cknwqs6avwf09b73d7dkxbjx3wc717s4") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.11 (c (n "kv-derive-impl") (v "0.1.0-alpha.11") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "1jrindbrr83lmb2sgzmdy4mly5v1rfmz423qx056q9m2hwn591m1") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.12 (c (n "kv-derive-impl") (v "0.1.0-alpha.12") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "0cj5r1lzmjd53rhacb0zm47q2i87k0klg7d3rsiprwxq551vjj5z") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.13 (c (n "kv-derive-impl") (v "0.1.0-alpha.13") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "0fmp7s1sba0hc15mfscabjkjn13hw6mm91v5abjzzq5z7ddx7v14") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.15 (c (n "kv-derive-impl") (v "0.1.0-alpha.15") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "011w9dkiqls64jcacvww41vhcag8c38i2dbhzv81inhwqb43s4i2") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.16 (c (n "kv-derive-impl") (v "0.1.0-alpha.16") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "0ll07fi0xym7pl1kyqzrazjnbc4g8a1sz7ddhjs90jbg5nzq4892") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1.0-alpha.17 (c (n "kv-derive-impl") (v "0.1.0-alpha.17") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)))) (h "056mhbn9f2n0fwffapmdkqng6lx5xd5gkl7v9ahj0hg685xc06ww") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.2.0-alpha.1 (c (n "kv-derive-impl") (v "0.2.0-alpha.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vaciwf27qyi743hkqry8y7asynkkhfnyyzxic7500jzgw7qn809") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.2.0-alpha.2 (c (n "kv-derive-impl") (v "0.2.0-alpha.2") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0klr06gcfpldjflblq5vyn3il1074q5jmn47liy2j75bcwk6i2s4") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.2.0-alpha.3 (c (n "kv-derive-impl") (v "0.2.0-alpha.3") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1k13ayw1zjjidcl7gyc6kb1rqvljj6ia6fv3qjmg23aw642qda7a") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-0.3.0-alpha.1 (c (n "kv-derive-impl") (v "0.3.0-alpha.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0nffpa35177iqkihv74m7azh1r3s3692nylqv46rs6xw85qzyqsf") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.0-rc.1 (c (n "kv-derive-impl") (v "1.0.0-rc.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0rxq5r29sk4clfbw32bkw4qzrnr4jl1fg6ciywxmbhpj3arbskcm") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.0-rc.2 (c (n "kv-derive-impl") (v "1.0.0-rc.2") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1lxrzscjbnsikf91acpf16skadkqnkcr6jm73y0d0nnfilghrpfh") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.0-rc.3 (c (n "kv-derive-impl") (v "1.0.0-rc.3") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ggjv51whvwm0bw6iisikqx0dkfpy42808bsh6yzbc7f7hid8p9z") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.0-rc.4 (c (n "kv-derive-impl") (v "1.0.0-rc.4") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0h549wv0lbd3d1vqxar8jcji9ap1mksfadzy4p4nk6yn8s26hxgh") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.0-rc.5 (c (n "kv-derive-impl") (v "1.0.0-rc.5") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "18az07gx9d4lgkbazldndgscsw0xgy2lmb6b68kdwa7didq5v5b8") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.0-rc.6 (c (n "kv-derive-impl") (v "1.0.0-rc.6") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "10z8wi9k6ij463mk5mijz6zrp0wajbqpnlwsxxfnd7qhjdvm1d7n") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.0-rc.7 (c (n "kv-derive-impl") (v "1.0.0-rc.7") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "02jms16rhm9cvf9w9hxv3cd94d7dhrhfbcpj7ah627kyd3aq28zk") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.0-rc.8 (c (n "kv-derive-impl") (v "1.0.0-rc.8") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "07ach59mskikmd38j065wjmacwy0xhsrcgz0qf08raqazdb533im") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.0-rc.9 (c (n "kv-derive-impl") (v "1.0.0-rc.9") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1b9nfn8043vqh1lc21nvxp9ddfnxjpgqvas9b5y069dcpc2f6mry") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.0 (c (n "kv-derive-impl") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1gcbygvsgsa9p9j49r2w6m4jdmvb8dvyqxshzrqh5c51vd9wpn20") (f (quote (("default"))))))

(define-public crate-kv-derive-impl-1.0.1 (c (n "kv-derive-impl") (v "1.0.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "049xpxm6b4i34j9qs3dz66kkd8xdgvx36i0jrjbjai1jb75d2vdf") (f (quote (("default"))))))

