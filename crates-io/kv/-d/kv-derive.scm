(define-module (crates-io kv -d kv-derive) #:use-module (crates-io))

(define-public crate-kv-derive-0.1.0-alpha.1 (c (n "kv-derive") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "0m5za0hzc75fglws50imlnfyn8cl58zs5phiwwy8c4192zzpija4") (f (quote (("default"))))))

(define-public crate-kv-derive-0.1.0-alpha.2 (c (n "kv-derive") (v "0.1.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "16p1mhs1x9s3hxvgd1w81kfidqz8961qd011rhabnyzxmf1f7dvj") (f (quote (("default"))))))

(define-public crate-kv-derive-0.1.0-alpha.14 (c (n "kv-derive") (v "0.1.0-alpha.14") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^0.1.0-alpha.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "17mkih19npvzzsq6959vbbh3w8f4dv9k0nqxq80bz4g0iy4zihrn") (f (quote (("default"))))))

(define-public crate-kv-derive-0.1.0-alpha.17 (c (n "kv-derive") (v "0.1.0-alpha.17") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^0.1.0-alpha.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "04mswqb2plr4dfbz9sf52556rhl9rwp047ha3zpfcgqdbcwj7nm9") (f (quote (("default"))))))

(define-public crate-kv-derive-0.2.0-alpha.1 (c (n "kv-derive") (v "0.2.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "0kxbf8p0z6cdgc2iwskfrdbc6jb1254k31dvsbia38k2ay4hzcc2") (f (quote (("default"))))))

(define-public crate-kv-derive-0.2.0-alpha.2 (c (n "kv-derive") (v "0.2.0-alpha.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^0.2.0-alpha.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "1asnm2z86ri71al06mn7k5f8q31485b5kl12yg75dgx6jv7ypci2") (f (quote (("default"))))))

(define-public crate-kv-derive-0.2.0-alpha.3 (c (n "kv-derive") (v "0.2.0-alpha.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "kv-derive-impl") (r "^0.2.0-alpha.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (d #t) (k 0)))) (h "1d5pbjjam2qvqnfil8ygq4m17rkh3rdfxw2pc1b1lbzsf8spvz0a") (f (quote (("default"))))))

(define-public crate-kv-derive-0.3.0-alpha.1 (c (n "kv-derive") (v "0.3.0-alpha.1") (d (list (d (n "kv-derive-impl") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "kv-derive-macro") (r "^0.3.0-alpha.1") (d #t) (k 0)))) (h "1sqcsqjx8y9cn8h35n4shysk6yair4swisqj0rmz4qqp120v8z44") (f (quote (("default"))))))

(define-public crate-kv-derive-1.0.0-rc.1 (c (n "kv-derive") (v "1.0.0-rc.1") (d (list (d (n "kv-derive-impl") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "kv-derive-macro") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "1r4wyww9bbf1w1h84i04fb4ik0gxrsg399aygnsp81s3ilmqamlp") (f (quote (("default"))))))

(define-public crate-kv-derive-1.0.0-rc.4 (c (n "kv-derive") (v "1.0.0-rc.4") (d (list (d (n "kv-derive-impl") (r "^1.0.0-rc.4") (d #t) (k 0)) (d (n "kv-derive-macro") (r "^1.0.0-rc.4") (d #t) (k 0)))) (h "0lg71jsvf9z69gj5n2fzs00df6a85smafbgjjx9g5qp9ab25v445") (f (quote (("default"))))))

(define-public crate-kv-derive-1.0.0-rc.5 (c (n "kv-derive") (v "1.0.0-rc.5") (d (list (d (n "kv-derive-impl") (r "^1.0.0-rc.5") (d #t) (k 0)) (d (n "kv-derive-macro") (r "^1.0.0-rc.5") (d #t) (k 0)))) (h "10w1k6f6qny1snbbcv5pq4maq7yb6rdh2yrrq5vh9hfb9c1ia2yl") (f (quote (("default"))))))

(define-public crate-kv-derive-1.0.0-rc.6 (c (n "kv-derive") (v "1.0.0-rc.6") (d (list (d (n "kv-derive-impl") (r "^1.0.0-rc.6") (d #t) (k 0)) (d (n "kv-derive-macro") (r "^1.0.0-rc.6") (d #t) (k 0)))) (h "0krm222iavl6n2wj9fkw9z0d9005n8ivx2q7fbjh89vj0gcrfkn2") (f (quote (("default"))))))

(define-public crate-kv-derive-1.0.0-rc.7 (c (n "kv-derive") (v "1.0.0-rc.7") (d (list (d (n "kv-derive-impl") (r "^1.0.0-rc.7") (d #t) (k 0)) (d (n "kv-derive-macro") (r "^1.0.0-rc.7") (d #t) (k 0)))) (h "1c773w97s6j02q1pvx5lg0bf9wk26yzaadcgy5a9526gs4zvxmjk") (f (quote (("default"))))))

(define-public crate-kv-derive-1.0.0-rc.8 (c (n "kv-derive") (v "1.0.0-rc.8") (d (list (d (n "kv-derive-impl") (r "^1.0.0-rc.8") (d #t) (k 0)) (d (n "kv-derive-macro") (r "^1.0.0-rc.8") (d #t) (k 0)))) (h "1zxscn4jq3v007l18d55zxv4yrxc1fyvd70qg8c5vmz20ckcv80v") (f (quote (("default"))))))

(define-public crate-kv-derive-1.0.0-rc.9 (c (n "kv-derive") (v "1.0.0-rc.9") (d (list (d (n "kv-derive-impl") (r "^1.0.0-rc.9") (d #t) (k 0)) (d (n "kv-derive-macro") (r "^1.0.0-rc.9") (d #t) (k 0)))) (h "1rm001lzrpr5w15l7d5mczk6957kjmw28rck8k84cgalwx6blr1a") (f (quote (("default"))))))

(define-public crate-kv-derive-1.0.0 (c (n "kv-derive") (v "1.0.0") (d (list (d (n "kv-derive-impl") (r "^1.0.0") (d #t) (k 0)) (d (n "kv-derive-macro") (r "^1.0.0") (d #t) (k 0)))) (h "10lsmsf76fhr262l3wm42yjq55aiwqjvqf9bf4gi1x0infgwy76s") (f (quote (("default"))))))

(define-public crate-kv-derive-1.0.1 (c (n "kv-derive") (v "1.0.1") (d (list (d (n "kv-derive-impl") (r "^1.0.1") (d #t) (k 0)) (d (n "kv-derive-macro") (r "^1.0.1") (d #t) (k 0)))) (h "11g043r1nw4msbi8vk2n5phk523sc0gkfxgwkfqd4rrj48rphs1v") (f (quote (("default"))))))

