(define-module (crates-io kv li kvlite) #:use-module (crates-io))

(define-public crate-kvlite-0.1.0 (c (n "kvlite") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "0wk1wscngwyvfb5vm9pvgd3wcinp8w3dl0yzcqc4vjxzqlsqm560")))

(define-public crate-kvlite-0.1.1 (c (n "kvlite") (v "0.1.1") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "0ica033fdg4i0ghk0dr249zrkjshihrklg8rpv9cim7hljwr44ii")))

(define-public crate-kvlite-0.1.2 (c (n "kvlite") (v "0.1.2") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "0niabasd4m94p6an9rvcm23a6zhr0marw8sjy5z26s8djq9d03q2")))

(define-public crate-kvlite-0.1.3 (c (n "kvlite") (v "0.1.3") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "1l9pqnirfc3nib4wycy5w9kfzq3xqcvwz9sbqsd66mfvaibxr9dg")))

