(define-module (crates-io kv v- kvv-efa-api) #:use-module (crates-io))

(define-public crate-kvv-efa-api-0.1.0 (c (n "kvv-efa-api") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^4.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "1.20.*") (f (quote ("full"))) (d #t) (k 2)))) (h "0ngkckbixywnyq5sgs1ipn781h74b4ldvzywz90x1al5d2nam3wb") (s 2) (e (quote (("reqwest" "dep:reqwest"))))))

(define-public crate-kvv-efa-api-0.1.1 (c (n "kvv-efa-api") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^4.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "1.20.*") (f (quote ("full"))) (d #t) (k 2)))) (h "0cvjwjq0fxvg60sf72fm410w42n7p6p5y9qah36fy0wmfx74xmbr") (s 2) (e (quote (("reqwest" "dep:reqwest"))))))

