(define-module (crates-io kv ar kvarn_signal) #:use-module (crates-io))

(define-public crate-kvarn_signal-0.1.0 (c (n "kvarn_signal") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("net" "time" "io-util" "rt" "fs" "sync"))) (d #t) (k 0)))) (h "11s14nc11wy6nmvjfy16v3dfqnv6adi7gn5j7wiaxv8h4wfj16d0") (r "1.56")))

(define-public crate-kvarn_signal-0.2.0 (c (n "kvarn_signal") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^6") (f (quote ("macos_fsevent"))) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("net" "time" "io-util" "rt" "fs" "sync" "macros"))) (d #t) (k 0)) (d (n "tokio-uring") (r "^0.4.0-alpha1") (o #t) (d #t) (k 0) (p "kvarn-tokio-uring")))) (h "1k63s8grg8mbyvc7dkj3fnxkc988zx3zhspl8klvhr05j7g2dsa0") (f (quote (("uring" "tokio-uring")))) (r "1.56")))

