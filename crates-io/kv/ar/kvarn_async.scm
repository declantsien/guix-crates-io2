(define-module (crates-io kv ar kvarn_async) #:use-module (crates-io))

(define-public crate-kvarn_async-0.3.0 (c (n "kvarn_async") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "kvarn_utils") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("rt" "io-util" "net" "fs" "sync" "parking_lot" "time"))) (d #t) (k 0)))) (h "07f4q5k35w7pqhg1aajp17h2yr4p1xp5lndx76v9wdxk86w3k2br") (r "1.56")))

(define-public crate-kvarn_async-0.4.0 (c (n "kvarn_async") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "kvarn_utils") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt" "io-util" "net" "fs" "sync" "parking_lot" "time"))) (d #t) (k 0)))) (h "02ghs4d5xvr1ycrvj1s1h1680p5qyrbgnr50wpnqssxlxik1ydc9") (r "1.56")))

(define-public crate-kvarn_async-0.4.1 (c (n "kvarn_async") (v "0.4.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "kvarn_utils") (r "=0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt" "io-util" "net" "fs" "sync" "parking_lot" "time"))) (d #t) (k 0)))) (h "09f76dnf960fqxfkfwba2149kqjmn2rirnbl6ja02v8ckblzgiqw") (r "1.56")))

(define-public crate-kvarn_async-0.5.0 (c (n "kvarn_async") (v "0.5.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "kvarn_utils") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt" "io-util" "net" "fs" "sync" "parking_lot" "time"))) (d #t) (k 0)))) (h "14fqc1knc7k5bzc6jhmhsgc6nnsxp7226s1mscrkxvz7sgqs3kna") (r "1.56")))

(define-public crate-kvarn_async-0.6.0 (c (n "kvarn_async") (v "0.6.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "kvarn_utils") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("rt" "io-util" "fs" "sync" "parking_lot" "time"))) (d #t) (k 0)))) (h "0pa325pcya8dqryadzq6jlnlayljg320frkzf0gnzm9pygkln3dp") (r "1.56")))

