(define-module (crates-io kv ar kvarn-fastcgi-client) #:use-module (crates-io))

(define-public crate-kvarn-fastcgi-client-0.1.0 (c (n "kvarn-fastcgi-client") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1c77059bk97brnv8ga5nbv8mq7kcqhlwppal9q82zici5p040a71")))

(define-public crate-kvarn-fastcgi-client-0.9.0 (c (n "kvarn-fastcgi-client") (v "0.9.0") (d (list (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("io-util" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 2)))) (h "0fn05apaxmn8jf857g2fzf9zqlhqyvwa07w1id1rmrw6b781qygp")))

