(define-module (crates-io kv ar kvarn-h3-quinn) #:use-module (crates-io))

(define-public crate-kvarn-h3-quinn-0.0.6-alpha1 (c (n "kvarn-h3-quinn") (v "0.0.6-alpha1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "h3") (r "^0.0.4") (d #t) (k 0)) (d (n "quinn") (r "^0.11.0-alpha1") (f (quote ("futures-io"))) (k 0) (p "kvarn-quinn")) (d (n "quinn-proto") (r "^0.11.0-alpha1") (k 0) (p "kvarn-quinn-proto")) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "06dga5jqkqj1fyz85bz6q7mh182kwfdbqby0xhzbsjbzbai1yfsw") (r "1.63")))

