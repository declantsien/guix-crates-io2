(define-module (crates-io kv ar kvarn_utils) #:use-module (crates-io))

(define-public crate-kvarn_utils-0.3.0 (c (n "kvarn_utils") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wpb4yx394dpqhasm1av60m92zfs21mlf19ls69806da6ag7xr20") (r "1.56")))

(define-public crate-kvarn_utils-0.3.1 (c (n "kvarn_utils") (v "0.3.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0vyc67gqf9bw2zvw6kjjjyrxyixcf01h1n1b4xqyvqqvlzy5xrbk") (r "1.56")))

(define-public crate-kvarn_utils-0.4.0 (c (n "kvarn_utils") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1n7bcrqyyy1rp5pvmcrdsixad3cal4dq1miv1z8fx3ahmzfk0bfa") (r "1.56")))

(define-public crate-kvarn_utils-0.4.1 (c (n "kvarn_utils") (v "0.4.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0rplxvwdx6zs6h82n4b683ji6897nrcsms112h9h0d3czgbj629i") (r "1.56")))

(define-public crate-kvarn_utils-0.5.0 (c (n "kvarn_utils") (v "0.5.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "164k3c69axm2c185hqli37ifkvk5m9c8wx3wwxyxhifrq6n7jpc1") (r "1.56")))

(define-public crate-kvarn_utils-0.6.0 (c (n "kvarn_utils") (v "0.6.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "compact_str") (r "^0.7") (d #t) (k 0)) (d (n "http") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.3") (d #t) (k 0)))) (h "0imf5dyqrkr2g0pshbdp45b446npglx0vimllznvja1vyxkxjx7y") (r "1.56")))

