(define-module (crates-io kv ar kvarn-quinn-udp) #:use-module (crates-io))

(define-public crate-kvarn-quinn-udp-0.5.0-alpha1 (c (n "kvarn-quinn-udp") (v "0.5.0-alpha1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "socket2") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.10") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_IO" "Win32_Networking_WinSock"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0pyp1hkddwp2hzicnph907fwjq58zrqga48bczdv7rb0qlg7fa7a") (f (quote (("log" "tracing/log") ("default" "log")))) (r "1.65")))

