(define-module (crates-io kv x_ kvx_macros) #:use-module (crates-io))

(define-public crate-kvx_macros-0.3.0 (c (n "kvx_macros") (v "0.3.0") (d (list (d (n "kvx") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "1wjaaamqn9caxynjaklglxvwg9r7ndv03zkfd6aqivgg70nxca1w")))

(define-public crate-kvx_macros-0.4.0 (c (n "kvx_macros") (v "0.4.0") (d (list (d (n "kvx_types") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0clrhw4skais2763ff3cl6mmq3dy5f95g16amywnas75spsz6njb")))

(define-public crate-kvx_macros-0.5.0 (c (n "kvx_macros") (v "0.5.0") (d (list (d (n "kvx_types") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0wmdbwms0b1h28g8zsqcg576hgps1f9435ij8f8bcg5f7i2fwjcf")))

(define-public crate-kvx_macros-0.6.0 (c (n "kvx_macros") (v "0.6.0") (d (list (d (n "kvx_types") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0ikfv97j3lz9lq8j24803wiqzl8wdkfjfyg8j7rrrlbfmm1zw3pl")))

(define-public crate-kvx_macros-0.7.0 (c (n "kvx_macros") (v "0.7.0") (d (list (d (n "kvx_types") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "06s9xw65axdhvp044653shjxzrq1wila7knngzd6488z680fx93v")))

(define-public crate-kvx_macros-0.8.0 (c (n "kvx_macros") (v "0.8.0") (d (list (d (n "kvx_types") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1pg3y9awgvxckz0a8638vg58dgqarjs298n344cjjddzqjidwzz8")))

(define-public crate-kvx_macros-0.9.0 (c (n "kvx_macros") (v "0.9.0") (d (list (d (n "kvx_types") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0zz8h5njwzyhkvfwzsicpp5n8kni438ciq5mbzwplkyd8k6xn1pd")))

(define-public crate-kvx_macros-0.9.1 (c (n "kvx_macros") (v "0.9.1") (d (list (d (n "kvx_types") (r "^0.9.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1zy937r8wj4mqg1cm5iss9j8mwzp6lgxirgqvl645cd1i6xn0k3w")))

(define-public crate-kvx_macros-0.9.2 (c (n "kvx_macros") (v "0.9.2") (d (list (d (n "kvx_types") (r "^0.9.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "065c2dmpjvqski2w60m9g0gz059kx17h9sz2mpqsyww5i9wygwx1")))

(define-public crate-kvx_macros-0.9.3 (c (n "kvx_macros") (v "0.9.3") (d (list (d (n "kvx_types") (r "^0.9.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "10ghdv09dg5p28yf3cx0wzvxga5iikmxcnl3cg3yazcjbpxs86ax")))

