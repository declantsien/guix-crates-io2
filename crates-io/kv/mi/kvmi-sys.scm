(define-module (crates-io kv mi kvmi-sys) #:use-module (crates-io))

(define-public crate-kvmi-sys-0.2.0 (c (n "kvmi-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1zdrkdrn3pvg9qzrkardqdsk9gx7f04hjzydrqazmwx2y1xl2wq9") (l "kvmi")))

(define-public crate-kvmi-sys-0.2.1 (c (n "kvmi-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1wm55amxmd1yw9xmsln24slig73jafbhl8v24zisj6rpjgqn3isk") (l "kvmi")))

(define-public crate-kvmi-sys-0.2.2 (c (n "kvmi-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0h5sqhv5cwbzmgg8dax79hlg84766lyqig3prhhlrxbf3y9qpd1d") (l "kvmi")))

