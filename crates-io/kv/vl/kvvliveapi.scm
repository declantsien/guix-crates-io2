(define-module (crates-io kv vl kvvliveapi) #:use-module (crates-io))

(define-public crate-kvvliveapi-0.1.0 (c (n "kvvliveapi") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "02gmm4m3db70c6rdhdymvdyy7zv9vxv438clccwdfilxhywgvwis")))

