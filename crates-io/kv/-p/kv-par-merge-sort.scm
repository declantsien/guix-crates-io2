(define-module (crates-io kv -p kv-par-merge-sort) #:use-module (crates-io))

(define-public crate-kv-par-merge-sort-0.1.0 (c (n "kv-par-merge-sort") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-chrome") (r "^0.6") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1w833x4wrb3ds869bwkspbv1di9f0nyi7513dkqmdvbdrnhddayp")))

