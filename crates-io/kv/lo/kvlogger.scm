(define-module (crates-io kv lo kvlogger) #:use-module (crates-io))

(define-public crate-kvlogger-0.1.0 (c (n "kvlogger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "1kpb34p2aa79xbcslj3g0r32f37v64wgmwkwan7wfkwb23592waj")))

(define-public crate-kvlogger-0.1.1 (c (n "kvlogger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "03j0wajh1f9wa5zadlvgrs68dv862x10p8x9rm2md35z2w49cqfm")))

(define-public crate-kvlogger-0.2.0 (c (n "kvlogger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "1rmqyswxvv190s98l33r38s67mmgsf486h8kdv8ql8rg9w5b80q4")))

(define-public crate-kvlogger-0.2.1 (c (n "kvlogger") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "1w4cn7zm5x8r1ynnn2v5iicvx92i0y3c4p7l4wlwm6yp98dzlgx4")))

(define-public crate-kvlogger-0.2.2 (c (n "kvlogger") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "135zw1irm9wg8hyd8m6algi7566zrylxrnbnq4b2ypj14nl2k4h1")))

(define-public crate-kvlogger-0.3.0 (c (n "kvlogger") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "1pir410lnxxxx02ynik9zblssma07dp7qvyy9mqs6ahqif02wdim") (f (quote (("datetime" "chrono"))))))

(define-public crate-kvlogger-0.3.1 (c (n "kvlogger") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "0wqn6hxlj33vr75rv7bsprdr3zfrmdf1kq80wmw720irvsxhqpsj") (f (quote (("datetime" "chrono"))))))

(define-public crate-kvlogger-0.3.2 (c (n "kvlogger") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "1is0fzbfcba81q70nxx6f7n4i1wyk4fv40nwqy54pvm46k3zl3qm") (f (quote (("datetime" "chrono"))))))

(define-public crate-kvlogger-0.4.0 (c (n "kvlogger") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "1bs9vks3y2p9lwqlq26j5kmmqs6ygkn1cmmqjp0mhvwgja8yhcgr") (f (quote (("datetime" "chrono"))))))

(define-public crate-kvlogger-0.4.1 (c (n "kvlogger") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "0yi01g7lqa6yvni2qnwvxs7vfni739jf300rcm5ya12099hjp1y1") (f (quote (("datetime" "chrono"))))))

(define-public crate-kvlogger-0.5.0 (c (n "kvlogger") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "0b0r8xxnrdcmzswbp8mrkcxihm2rybkyafdnd3hjmy9zjbrv6fh9") (f (quote (("datetime" "chrono"))))))

