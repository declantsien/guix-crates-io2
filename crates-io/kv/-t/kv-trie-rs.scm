(define-module (crates-io kv -t kv-trie-rs) #:use-module (crates-io))

(define-public crate-kv-trie-rs-0.1.1 (c (n "kv-trie-rs") (v "0.1.1") (d (list (d (n "louds-rs") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "057ji3ldlw1l76lxvkgwg32sxvqlz8ylzn6c8f0izhb73niygfkk")))

(define-public crate-kv-trie-rs-0.1.2 (c (n "kv-trie-rs") (v "0.1.2") (d (list (d (n "louds-rs") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0nsphjfwl9cxg9d5ppff6mg3hhzcl8hsn1dhb276lxkfaq3bixaq")))

