(define-module (crates-io kv on kvon-rs) #:use-module (crates-io))

(define-public crate-kvon-rs-0.1.0 (c (n "kvon-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1jgazi63rhgr04rz1ij1fcdwxch54j1cnd29dcqzi11fyfcb9q5b")))

(define-public crate-kvon-rs-0.2.0 (c (n "kvon-rs") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1jy3p275dx2qln83zhw7dmbbqsj083zhgpvx56bp5nh8fv57gqd9")))

(define-public crate-kvon-rs-0.3.0 (c (n "kvon-rs") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0jk65xjh0r485glcp58s4xmb4clbi21jg4580ayd8dfx2skmbsh4")))

(define-public crate-kvon-rs-0.3.1 (c (n "kvon-rs") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1zy39xp6f49gfhsj4vkbk8j4jcjkws6wqc6ab8w01652nvlwqkcv")))

