(define-module (crates-io kv fi kvfilter) #:use-module (crates-io))

(define-public crate-kvfilter-0.2.0 (c (n "kvfilter") (v "0.2.0") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "1drsijcn9wfdfh2ms454giv6br6m123frwwjylznz0rh6n3ifwwa") (y #t)))

(define-public crate-kvfilter-0.3.0 (c (n "kvfilter") (v "0.3.0") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "1aa3cafvx0c7k4crdmm0np9vmsqhkjsxll8vs4j78j3igj8xydn8") (y #t)))

(define-public crate-kvfilter-0.3.1 (c (n "kvfilter") (v "0.3.1") (d (list (d (n "slog") (r "^2") (d #t) (k 0)))) (h "1pvlbfwzw1g48676qr66m62bmdqx0drp087sac3nplz1yyq80vyf") (y #t)))

