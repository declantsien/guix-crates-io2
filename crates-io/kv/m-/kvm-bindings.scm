(define-module (crates-io kv m- kvm-bindings) #:use-module (crates-io))

(define-public crate-kvm-bindings-0.1.0 (c (n "kvm-bindings") (v "0.1.0") (h "0g3sl1x7ad6yi5ani88hhjgzwlq02hpchva4pq17zc219wgk2bsc") (f (quote (("kvm-v4_20_0") ("kvm-v4_14_0"))))))

(define-public crate-kvm-bindings-0.1.1 (c (n "kvm-bindings") (v "0.1.1") (h "00zwf5y8x517x6yvl8xq4h7gbxmhhnff4n2z1jcnvdrf7mqfh8y2") (f (quote (("kvm-v4_20_0") ("kvm-v4_14_0"))))))

(define-public crate-kvm-bindings-0.2.0 (c (n "kvm-bindings") (v "0.2.0") (d (list (d (n "vmm-sys-util") (r ">= 0.2.0") (o #t) (d #t) (k 0)))) (h "0lr4q4zshfb6xbircd35kq29y3xq08gl08clb9jv8190smm1b0fk") (f (quote (("kvm-v4_20_0") ("kvm-v4_14_0") ("fam-wrappers" "vmm-sys-util"))))))

(define-public crate-kvm-bindings-0.3.0 (c (n "kvm-bindings") (v "0.3.0") (d (list (d (n "vmm-sys-util") (r ">=0.2.0") (o #t) (d #t) (k 0)))) (h "18hxayj89v20ar47h1qnl8z8q3sxr8layarlfwnx3hv1fmk0f74g") (f (quote (("kvm-v4_20_0") ("kvm-v4_14_0") ("fam-wrappers" "vmm-sys-util"))))))

(define-public crate-kvm-bindings-0.3.1 (c (n "kvm-bindings") (v "0.3.1") (d (list (d (n "vmm-sys-util") (r "= 0.7.0") (o #t) (d #t) (k 0)))) (h "0gfd2y1c57r9gdxzcms3hcc5shahrlrc0lsqjn457fac1y0l55sf") (f (quote (("kvm-v4_20_0") ("kvm-v4_14_0") ("fam-wrappers" "vmm-sys-util"))))))

(define-public crate-kvm-bindings-0.4.0 (c (n "kvm-bindings") (v "0.4.0") (d (list (d (n "vmm-sys-util") (r ">=0.8.0") (o #t) (d #t) (k 0)))) (h "0kwagb46cz74p2a7y7cfaip20adk6k113i45vyh26asx2mk6p4q4") (f (quote (("kvm-v4_20_0") ("kvm-v4_14_0") ("fam-wrappers" "vmm-sys-util"))))))

(define-public crate-kvm-bindings-0.5.0 (c (n "kvm-bindings") (v "0.5.0") (d (list (d (n "vmm-sys-util") (r ">=0.8.0") (o #t) (d #t) (k 0)))) (h "0li9gxfwsd5vz6j3m3qviws0l3cklbcc3dwlk6azyvw2j28h9357") (f (quote (("fam-wrappers" "vmm-sys-util"))))))

(define-public crate-kvm-bindings-0.6.0 (c (n "kvm-bindings") (v "0.6.0") (d (list (d (n "vmm-sys-util") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0yafikvlmz1na1sjwcqclya7mzpfwmk5n07m2wfid4mhlmjhxrzg") (f (quote (("fam-wrappers" "vmm-sys-util"))))))

(define-public crate-kvm-bindings-0.7.0 (c (n "kvm-bindings") (v "0.7.0") (d (list (d (n "vmm-sys-util") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "0cp0n3qna0a6vynl4p9b1h8xnh3p51fz7894pw7rk6i2cj0vs7q8") (f (quote (("fam-wrappers" "vmm-sys-util"))))))

(define-public crate-kvm-bindings-0.8.0 (c (n "kvm-bindings") (v "0.8.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xn5k752gs5n9j5xcaj40hp1lmxb0j2vm5i4pff0d6fbfw1ybkn4") (f (quote (("fam-wrappers" "vmm-sys-util")))) (s 2) (e (quote (("serde" "dep:serde" "serde/derive" "dep:zerocopy"))))))

(define-public crate-kvm-bindings-0.8.1 (c (n "kvm-bindings") (v "0.8.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08dx92g9lcyls32105nvh30yq36ryz0nr90iwlah16m34n3pwbm8") (f (quote (("fam-wrappers" "vmm-sys-util")))) (s 2) (e (quote (("serde" "dep:serde" "serde/derive" "dep:zerocopy"))))))

(define-public crate-kvm-bindings-0.8.2 (c (n "kvm-bindings") (v "0.8.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s7i2sqndg0vcy7b6ibi2rcpn5bzx2pdd42shskqzzb3jxy19hvs") (f (quote (("fam-wrappers" "vmm-sys-util")))) (s 2) (e (quote (("serde" "dep:serde" "serde/derive" "dep:zerocopy"))))))

