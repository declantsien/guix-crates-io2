(define-module (crates-io kv m- kvm-ioctls) #:use-module (crates-io))

(define-public crate-kvm-ioctls-0.0.1 (c (n "kvm-ioctls") (v "0.0.1") (h "17sd2al8wmk7w3wd8mmffj3ljl2hk0kywfhrx6dpifw4i6mjcy0j")))

(define-public crate-kvm-ioctls-0.1.0 (c (n "kvm-ioctls") (v "0.1.0") (d (list (d (n "byteorder") (r ">= 1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">= 0.1.0") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)))) (h "01jkiz35kz14gakfcm1i7y0qg931jxn18qgnzklr93v549rhqyzq")))

(define-public crate-kvm-ioctls-0.2.0 (c (n "kvm-ioctls") (v "0.2.0") (d (list (d (n "byteorder") (r ">= 1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">= 0.1.0") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)))) (h "06r1kdvjk40rl05dqr2zs5fkxbgbfqns3ihl44bkm29rh36yy67p")))

(define-public crate-kvm-ioctls-0.3.0 (c (n "kvm-ioctls") (v "0.3.0") (d (list (d (n "byteorder") (r ">= 1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">= 0.1.0") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">= 0.1.1") (d #t) (k 0)))) (h "0yyfp03j8rrf1hyg2qny9xigrlmawz11yq1qp5723xdhj3nlgrj3")))

(define-public crate-kvm-ioctls-0.4.0 (c (n "kvm-ioctls") (v "0.4.0") (d (list (d (n "byteorder") (r ">= 1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">= 0.2.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">= 0.2.1") (d #t) (k 0)))) (h "16jrdipvv2fpsjqpjy9fshfs1kfy2x1pmss0y63wr63h7kdmk2mc")))

(define-public crate-kvm-ioctls-0.5.0 (c (n "kvm-ioctls") (v "0.5.0") (d (list (d (n "byteorder") (r ">= 1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">= 0.2.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">= 0.2.1") (d #t) (k 0)))) (h "02f1qd8hmq773h4pqaynzmfn7dvm8xvxcnh9iwcaf51qvzsj15yr")))

(define-public crate-kvm-ioctls-0.6.0 (c (n "kvm-ioctls") (v "0.6.0") (d (list (d (n "byteorder") (r ">=1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">=0.2.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.2.1") (d #t) (k 0)))) (h "0zz3v90gqxfmipm5sqy4zg1bs5wkifgdqcgs4f1cmpavi7d1b38m")))

(define-public crate-kvm-ioctls-0.7.0 (c (n "kvm-ioctls") (v "0.7.0") (d (list (d (n "byteorder") (r ">=1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">=0.2.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.8.0") (d #t) (k 0)))) (h "0sahkgd576sylm7ck74yy6q5q94v8zban0r13rjks79psr9a68sw")))

(define-public crate-kvm-ioctls-0.6.1 (c (n "kvm-ioctls") (v "0.6.1") (d (list (d (n "byteorder") (r ">= 1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r "= 0.3.1") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r "= 0.7.0") (d #t) (k 0)))) (h "0wdz8l93xn9dxycxi32knhly1fp59pz41i5qllfzdq8kbsn0504v")))

(define-public crate-kvm-ioctls-0.8.0 (c (n "kvm-ioctls") (v "0.8.0") (d (list (d (n "byteorder") (r ">=1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">=0.4.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.8.0") (d #t) (k 0)))) (h "1rfwr7bfh6pr5hhbdlyc28ax8g5p374kxjnl0bdj6rrcj4b8n1bl")))

(define-public crate-kvm-ioctls-0.9.0 (c (n "kvm-ioctls") (v "0.9.0") (d (list (d (n "byteorder") (r ">=1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">=0.4.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.8.0") (d #t) (k 0)))) (h "18lw5w6y37hja317vjfrvq8lmisr0hqswc9kwhwcg598w9a494pj")))

(define-public crate-kvm-ioctls-0.10.0 (c (n "kvm-ioctls") (v "0.10.0") (d (list (d (n "byteorder") (r ">=1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">=0.5.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.8.0") (d #t) (k 0)))) (h "0xjbyvq4q316cb8s19vc94yrrl9ifk6aqb4lyqy8gwbx0kwi9p28")))

(define-public crate-kvm-ioctls-0.11.0 (c (n "kvm-ioctls") (v "0.11.0") (d (list (d (n "byteorder") (r ">=1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r ">=0.5.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.8.0") (d #t) (k 0)))) (h "14vm8gnczkcij9xsidqzg6zvpyb6mdr0y4qq9pyndyvzinj2nhlp")))

(define-public crate-kvm-ioctls-0.12.0 (c (n "kvm-ioctls") (v "0.12.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r "^0.6.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.11.0") (d #t) (k 0)))) (h "1r1syx3vjvs7hrmp3jm52ykq7pc8yca77qkpki4l4xl2pp5238y3")))

(define-public crate-kvm-ioctls-0.13.0 (c (n "kvm-ioctls") (v "0.13.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r "^0.6.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.11.0") (d #t) (k 0)))) (h "0szbzamqj17kj5d5habdd13s0fczqadic1sxxi2g3rcn32fdry5q")))

(define-public crate-kvm-ioctls-0.14.0 (c (n "kvm-ioctls") (v "0.14.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r "^0.6.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.11.0") (d #t) (k 0)))) (h "12nygrx4c676aaq4iy1wjk3xl60mwchfhclkc7n98b2k62r4cqj3")))

(define-public crate-kvm-ioctls-0.15.0 (c (n "kvm-ioctls") (v "0.15.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r "^0.6.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.11.0") (d #t) (k 0)))) (h "1m3bdk5wbygkd5fxya267qnlyv3jqhci0xczyxz5idp7dssf5pcv")))

(define-public crate-kvm-ioctls-0.16.0 (c (n "kvm-ioctls") (v "0.16.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r "^0.7.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.12.1") (d #t) (k 0)))) (h "133apmr9s0b221xjkr86idnpvc40d6pccbln5gr18m3m17qdy0lh")))

(define-public crate-kvm-ioctls-0.17.0 (c (n "kvm-ioctls") (v "0.17.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 2)) (d (n "kvm-bindings") (r "^0.8.0") (f (quote ("fam-wrappers"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.12.1") (d #t) (k 0)))) (h "0zclfaw3pi1r5sm3q93m2jifl56cyn8rdbxb27iyn6sk9b5f5nmy")))

