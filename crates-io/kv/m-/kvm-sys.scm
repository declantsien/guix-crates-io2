(define-module (crates-io kv m- kvm-sys) #:use-module (crates-io))

(define-public crate-kvm-sys-0.3.0 (c (n "kvm-sys") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.23") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.1.4") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "memmap") (r "^0.2.1") (d #t) (k 0)))) (h "0pc8jr6ymfhc88f3zy4cz2q49km1fr2hn2cf6w2ncnpdxdsprcff") (f (quote (("dev" "clippy") ("default"))))))

