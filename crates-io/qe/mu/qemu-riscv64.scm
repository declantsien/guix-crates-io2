(define-module (crates-io qe mu qemu-riscv64) #:use-module (crates-io))

(define-public crate-qemu-riscv64-0.1.0 (c (n "qemu-riscv64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-riscv64"))) (d #t) (k 0)))) (h "04ps5yv4fad4ihi250lv4jm1vz9qkxb2r9fg3q0mmlg96h168rwz")))

(define-public crate-qemu-riscv64-0.1.3 (c (n "qemu-riscv64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-riscv64"))) (d #t) (k 0)))) (h "1g85xd6zd6m4zybyhzrzi6p2g56q67h20zccr556dw6vpr341dcl")))

