(define-module (crates-io qe mu qemu-xtensaeb) #:use-module (crates-io))

(define-public crate-qemu-xtensaeb-0.1.0 (c (n "qemu-xtensaeb") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-xtensaeb"))) (d #t) (k 0)))) (h "0l7830a8h6kz6vc6ffck9l63mb6h89cpa49s8ak91c5bp956ga99")))

(define-public crate-qemu-xtensaeb-0.1.3 (c (n "qemu-xtensaeb") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-xtensaeb"))) (d #t) (k 0)))) (h "0ny1f1gz0294diwhy6xlxhf4r02n9yrdm7ybbwiw79rbqsbb6kgs")))

