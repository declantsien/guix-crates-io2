(define-module (crates-io qe mu qemu-system-mipsel) #:use-module (crates-io))

(define-public crate-qemu-system-mipsel-0.1.0 (c (n "qemu-system-mipsel") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-mipsel"))) (d #t) (k 0)))) (h "118dr9ncfwvjxp5bb6vhi4gh96rx0blhyk4kz18mw7346n7pydg2")))

(define-public crate-qemu-system-mipsel-0.1.3 (c (n "qemu-system-mipsel") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-mipsel"))) (d #t) (k 0)))) (h "028li5v1s9bxvigd2hgsj29q0bxlnfka2yb9ljz5cda6077cn3hp")))

