(define-module (crates-io qe mu qemu_print) #:use-module (crates-io))

(define-public crate-qemu_print-0.1.0 (c (n "qemu_print") (v "0.1.0") (d (list (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "spinning_top") (r "^0.2.4") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (k 0)))) (h "15ja1hcryd5fdkyczv1k9nph2vjc93ka0ggm8qrk9iv0p0q5i5j9") (f (quote (("stable" "uart_16550/stable") ("nightly" "uart_16550/nightly") ("default" "nightly"))))))

