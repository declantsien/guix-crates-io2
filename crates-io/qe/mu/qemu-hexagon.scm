(define-module (crates-io qe mu qemu-hexagon) #:use-module (crates-io))

(define-public crate-qemu-hexagon-0.1.0 (c (n "qemu-hexagon") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-hexagon"))) (d #t) (k 0)))) (h "09mnbxbdg026vyks058ja55bv62q88lmb6sdjw4vav7nlaghv8vn")))

(define-public crate-qemu-hexagon-0.1.3 (c (n "qemu-hexagon") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-hexagon"))) (d #t) (k 0)))) (h "1r7fvvz7g20nfccpy3m7wknj0dr7yji1dr25sk90qag7dncsplfa")))

