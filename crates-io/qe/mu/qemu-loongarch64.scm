(define-module (crates-io qe mu qemu-loongarch64) #:use-module (crates-io))

(define-public crate-qemu-loongarch64-0.1.0 (c (n "qemu-loongarch64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-loongarch64"))) (d #t) (k 0)))) (h "1d04fphqjd2377wzxsd74gkgzz4pll7pl8x1sqn07sfvshmvv48h")))

(define-public crate-qemu-loongarch64-0.1.3 (c (n "qemu-loongarch64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-loongarch64"))) (d #t) (k 0)))) (h "1ha4alf4x3kvza87cx2lj2r48j8zr2hc9rxnj76y8p8n5819c895")))

