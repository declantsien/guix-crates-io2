(define-module (crates-io qe mu qemu-system-m68k) #:use-module (crates-io))

(define-public crate-qemu-system-m68k-0.1.0 (c (n "qemu-system-m68k") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-m68k"))) (d #t) (k 0)))) (h "122chsgng9r1li4z3748f320w4xnvn1s2rgfgbqs9fzzw00ikxwa")))

(define-public crate-qemu-system-m68k-0.1.3 (c (n "qemu-system-m68k") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-m68k"))) (d #t) (k 0)))) (h "11j52ghkiay6j5hhl9y259ypiafk2z1my443688g76xkqkikvbzs")))

