(define-module (crates-io qe mu qemu-system-ppc) #:use-module (crates-io))

(define-public crate-qemu-system-ppc-0.1.0 (c (n "qemu-system-ppc") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-ppc"))) (d #t) (k 0)))) (h "1jyx3gl7lpnvc88kb3dz1z1kg47pnsbfq6093xmbqrfmx2xmjcci")))

(define-public crate-qemu-system-ppc-0.1.3 (c (n "qemu-system-ppc") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-ppc"))) (d #t) (k 0)))) (h "1qgmf4s0wcpkb7xzndw7qg7l0qsrbrwnqnbchpzv8rbbpiwis0sy")))

