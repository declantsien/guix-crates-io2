(define-module (crates-io qe mu qemu-system-xtensa) #:use-module (crates-io))

(define-public crate-qemu-system-xtensa-0.1.0 (c (n "qemu-system-xtensa") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-xtensa"))) (d #t) (k 0)))) (h "0bjvnnswfz9dyr3a5m87vjlg2bzr0xak4ma5db0v2w5w7xrnzjax")))

(define-public crate-qemu-system-xtensa-0.1.3 (c (n "qemu-system-xtensa") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-xtensa"))) (d #t) (k 0)))) (h "0c8bg19mg2nx274bavrqbbflv1wg7h7fd4niai96q46a5d1y62qr")))

