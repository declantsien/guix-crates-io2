(define-module (crates-io qe mu qemu-ppc) #:use-module (crates-io))

(define-public crate-qemu-ppc-0.1.0 (c (n "qemu-ppc") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-ppc"))) (d #t) (k 0)))) (h "0mw6814k8j2y73w7cm54g8mhablxg210nyxb3mx2lr0vwznlph9a")))

(define-public crate-qemu-ppc-0.1.3 (c (n "qemu-ppc") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-ppc"))) (d #t) (k 0)))) (h "16p91pcn6d10kizcx45wcx09pnga98dhr7s57zbprqkfsmyjap9b")))

