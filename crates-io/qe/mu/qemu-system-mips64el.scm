(define-module (crates-io qe mu qemu-system-mips64el) #:use-module (crates-io))

(define-public crate-qemu-system-mips64el-0.1.0 (c (n "qemu-system-mips64el") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-mips64el"))) (d #t) (k 0)))) (h "0lf5vfqjd2342xmxfiimcyyxsnx1fxrn8ihvyd5b00yx0l76fsz5")))

(define-public crate-qemu-system-mips64el-0.1.3 (c (n "qemu-system-mips64el") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-mips64el"))) (d #t) (k 0)))) (h "1nk11qbr0l3x2f2p4w93kdymnvzvwfyfiybl9g93i4iqqp8zr4h3")))

