(define-module (crates-io qe mu qemu-microblazeel) #:use-module (crates-io))

(define-public crate-qemu-microblazeel-0.1.0 (c (n "qemu-microblazeel") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-microblazeel"))) (d #t) (k 0)))) (h "1cqf95br9cyr1wk0drxml8hdhc5zg7sanrq2kw9mpv92xhvi90fr")))

(define-public crate-qemu-microblazeel-0.1.3 (c (n "qemu-microblazeel") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-microblazeel"))) (d #t) (k 0)))) (h "0vf9rrriq1q87f15kjp8cpxv43grmqyzid4dnn83jr33875dc0zq")))

