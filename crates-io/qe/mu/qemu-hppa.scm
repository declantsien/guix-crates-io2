(define-module (crates-io qe mu qemu-hppa) #:use-module (crates-io))

(define-public crate-qemu-hppa-0.1.0 (c (n "qemu-hppa") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-hppa"))) (d #t) (k 0)))) (h "0138k207k9rs74xwalw2a702l9v8c6ljsakf9nhiksk7w4mx03l9")))

(define-public crate-qemu-hppa-0.1.3 (c (n "qemu-hppa") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-hppa"))) (d #t) (k 0)))) (h "18qrwki9c5r9xnkcdcmyba1d0hmz4hb08n2096nna2xncim1mi95")))

