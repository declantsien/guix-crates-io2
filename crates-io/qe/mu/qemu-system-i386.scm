(define-module (crates-io qe mu qemu-system-i386) #:use-module (crates-io))

(define-public crate-qemu-system-i386-0.1.0 (c (n "qemu-system-i386") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-i386"))) (d #t) (k 0)))) (h "1zf7xkf20fph1278z9mjna64cfglwwapmb4wsy5l472i3z8l5g4j")))

(define-public crate-qemu-system-i386-0.1.3 (c (n "qemu-system-i386") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-i386"))) (d #t) (k 0)))) (h "08g6lfvjq22y2nswm7zl6fq15av4b7crmcks6vlpqaym3c39fn94")))

