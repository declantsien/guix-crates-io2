(define-module (crates-io qe mu qemu-fw-cfg) #:use-module (crates-io))

(define-public crate-qemu-fw-cfg-0.1.0 (c (n "qemu-fw-cfg") (v "0.1.0") (h "01db9kv7082dm42pg9q3lw28zs3la4gyfi30ijcwfji80y8bfz14") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-qemu-fw-cfg-0.1.1 (c (n "qemu-fw-cfg") (v "0.1.1") (h "1jy84al9crf4k7x6pv1mambjaq6vvm8vhdijn919lxp8ndhpgs5n") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-qemu-fw-cfg-0.2.0 (c (n "qemu-fw-cfg") (v "0.2.0") (d (list (d (n "fdt") (r "^0.1.3") (d #t) (t "cfg(target_arch = \"riscv32\")") (k 2)) (d (n "riscv-rt") (r "^0.9.0") (d #t) (t "cfg(target_arch = \"riscv32\")") (k 2)))) (h "0gzjy1i2fhbi5pwhbsc33qhcd1f0kw3rl9jr71fmacm8iv9f66lf") (f (quote (("default" "alloc") ("alloc")))) (r "1.59")))

