(define-module (crates-io qe mu qemu-arm) #:use-module (crates-io))

(define-public crate-qemu-arm-0.1.0 (c (n "qemu-arm") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-arm"))) (d #t) (k 0)))) (h "1g2rlqrna42rsmcwf0lvliqs9scaliyr068sawmy60q5vplrnh4p")))

(define-public crate-qemu-arm-0.1.2 (c (n "qemu-arm") (v "0.1.2") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-arm"))) (d #t) (k 0)))) (h "1ki2w6lcd1i8cim8r2lx44qliaqpfq0jw9d4sh240hl8w3f866qv")))

(define-public crate-qemu-arm-0.1.3 (c (n "qemu-arm") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-arm"))) (d #t) (k 0)))) (h "1fi0jby4f9n6zcxxfjpw4z9zgn6fzpigbbyjqkf6cmrxc0zyl5g4")))

