(define-module (crates-io qe mu qemu-s390x) #:use-module (crates-io))

(define-public crate-qemu-s390x-0.1.0 (c (n "qemu-s390x") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-s390x"))) (d #t) (k 0)))) (h "0758qw9fvb8nm9dnxswbnffsxn20s79pp4frzh0cw4cbn79yb6bl")))

(define-public crate-qemu-s390x-0.1.3 (c (n "qemu-s390x") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-s390x"))) (d #t) (k 0)))) (h "1jkk7cjgs02y327vvr3nsnn3fa5i9dv013k33zwdifr822dp12dy")))

