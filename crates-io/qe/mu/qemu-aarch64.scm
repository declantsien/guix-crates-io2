(define-module (crates-io qe mu qemu-aarch64) #:use-module (crates-io))

(define-public crate-qemu-aarch64-0.1.0 (c (n "qemu-aarch64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.4") (f (quote ("qemu-aarch64"))) (d #t) (k 0)))) (h "1vh92wqllyjnsjpxdq1c3qhkzsrwp4af0v83z5q82g5n155v7gnd")))

(define-public crate-qemu-aarch64-0.1.2 (c (n "qemu-aarch64") (v "0.1.2") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-aarch64"))) (d #t) (k 0)))) (h "0hd2sgr55ybq5cwpd03i90r6ji76651n9xbrs6ylxz1bn4i7pch5")))

(define-public crate-qemu-aarch64-0.1.3 (c (n "qemu-aarch64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-aarch64"))) (d #t) (k 0)))) (h "18vc954asq3hla6lvrkkxhjgg3vdqkwb9qa4vs68q2wykza03z2l")))

