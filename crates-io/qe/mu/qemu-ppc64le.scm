(define-module (crates-io qe mu qemu-ppc64le) #:use-module (crates-io))

(define-public crate-qemu-ppc64le-0.1.0 (c (n "qemu-ppc64le") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-ppc64le"))) (d #t) (k 0)))) (h "1gm7zl4ljw9l6brn345lqzajiiyncx4x8mm7hpk0dfhl6w7kd0l5")))

(define-public crate-qemu-ppc64le-0.1.3 (c (n "qemu-ppc64le") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-ppc64le"))) (d #t) (k 0)))) (h "1sgybijxa1360wxy8clqj155dnf47s35gjwa02nkvdjg6f1p3yvz")))

