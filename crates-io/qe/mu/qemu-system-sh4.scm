(define-module (crates-io qe mu qemu-system-sh4) #:use-module (crates-io))

(define-public crate-qemu-system-sh4-0.1.0 (c (n "qemu-system-sh4") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-sh4"))) (d #t) (k 0)))) (h "1wn5vz4ddjpjwxh99dxgsli5i05r3pgn0iyxmhlxwbmblkdlmai8")))

(define-public crate-qemu-system-sh4-0.1.3 (c (n "qemu-system-sh4") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-sh4"))) (d #t) (k 0)))) (h "1pcpcyyqpv6r2cqpxqv5pna1v5msx8hmkv4gva3w98vcx3vccpvi")))

