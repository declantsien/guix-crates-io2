(define-module (crates-io qe mu qemu-m68k) #:use-module (crates-io))

(define-public crate-qemu-m68k-0.1.0 (c (n "qemu-m68k") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-m68k"))) (d #t) (k 0)))) (h "050zlafrhzn9fwd0yll2krf3blydidxcg28zpxvl1swbwhgp8q72")))

(define-public crate-qemu-m68k-0.1.3 (c (n "qemu-m68k") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-m68k"))) (d #t) (k 0)))) (h "171cv0dbkk7svmbpd391vnipb86h1bjscv6844a90jcwiwazd4c2")))

