(define-module (crates-io qe mu qemu-system-tricore) #:use-module (crates-io))

(define-public crate-qemu-system-tricore-0.1.0 (c (n "qemu-system-tricore") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-tricore"))) (d #t) (k 0)))) (h "08pmigm2387kilw771mlxwvpbgmyd0vywv3aqa83dyrr9gjgx1xm")))

(define-public crate-qemu-system-tricore-0.1.3 (c (n "qemu-system-tricore") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-tricore"))) (d #t) (k 0)))) (h "05vxbalj5mr8nmgc1vrbr07jq533vwcb9bfifnd42zan5mibi5v7")))

