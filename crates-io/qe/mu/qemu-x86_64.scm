(define-module (crates-io qe mu qemu-x86_64) #:use-module (crates-io))

(define-public crate-qemu-x86_64-0.1.0 (c (n "qemu-x86_64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-x86_64"))) (d #t) (k 0)))) (h "108jlvm63qw65fl5542wzh9kyjgdf1y2p2k1934xm3rndph2aw8i")))

(define-public crate-qemu-x86_64-0.1.3 (c (n "qemu-x86_64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-x86_64"))) (d #t) (k 0)))) (h "10h6s832v1qwbry3h6diwfm5j8xyqygbkdd0ha1xymhrkwp3qq4f")))

