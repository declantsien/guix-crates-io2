(define-module (crates-io qe mu qemu-system-hppa) #:use-module (crates-io))

(define-public crate-qemu-system-hppa-0.1.0 (c (n "qemu-system-hppa") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-hppa"))) (d #t) (k 0)))) (h "1ma2fvz3acsindgyrbg4vbdg1h01078w2xkmw9446n58m55a0ys8")))

(define-public crate-qemu-system-hppa-0.1.3 (c (n "qemu-system-hppa") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-hppa"))) (d #t) (k 0)))) (h "0vdq35jq33znad07rhldbg10jf91q2705xxi65xlmpr65xillhfw")))

