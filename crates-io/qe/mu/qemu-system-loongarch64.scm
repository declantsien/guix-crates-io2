(define-module (crates-io qe mu qemu-system-loongarch64) #:use-module (crates-io))

(define-public crate-qemu-system-loongarch64-0.1.0 (c (n "qemu-system-loongarch64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-loongarch64"))) (d #t) (k 0)))) (h "02xmjdc41s6rk9gbzg5iy7b0dd3bmv7gx6nlgq39slfnavl20wgj")))

(define-public crate-qemu-system-loongarch64-0.1.3 (c (n "qemu-system-loongarch64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-loongarch64"))) (d #t) (k 0)))) (h "05w814vbsifi3hs5ylck5nkzq0la7a1lcwb2w7xf0q39wkq0gvdj")))

