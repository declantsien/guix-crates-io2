(define-module (crates-io qe mu qemu-system-or1k) #:use-module (crates-io))

(define-public crate-qemu-system-or1k-0.1.0 (c (n "qemu-system-or1k") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-or1k"))) (d #t) (k 0)))) (h "149m1qagihnlh38pk3ygnv9yz10lj0xfjrmsij3pw579ainz4kdx")))

(define-public crate-qemu-system-or1k-0.1.3 (c (n "qemu-system-or1k") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-or1k"))) (d #t) (k 0)))) (h "0s1ra8sws5gfkld64lp1i5z2xvyd0yv5r904d1bwr5rpq6qygmxl")))

