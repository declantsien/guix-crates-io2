(define-module (crates-io qe mu qemu-system-xtensaeb) #:use-module (crates-io))

(define-public crate-qemu-system-xtensaeb-0.1.0 (c (n "qemu-system-xtensaeb") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-xtensaeb"))) (d #t) (k 0)))) (h "0rgrnn01jqhlapp288m0gc9bp4vf01rx91i9c5fhydgi27563kr6")))

(define-public crate-qemu-system-xtensaeb-0.1.3 (c (n "qemu-system-xtensaeb") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-xtensaeb"))) (d #t) (k 0)))) (h "1myidyyys1rrpvwcriqwg1r0lvgnbisd29zdaflr21sqw96s4w4y")))

