(define-module (crates-io qe mu qemu-system-riscv32) #:use-module (crates-io))

(define-public crate-qemu-system-riscv32-0.1.0 (c (n "qemu-system-riscv32") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-riscv32"))) (d #t) (k 0)))) (h "0h5av9hp00hn0rcfnwsvnbfjf8ggi6x1b2jqlrfh75pkj635nadl")))

(define-public crate-qemu-system-riscv32-0.1.3 (c (n "qemu-system-riscv32") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-riscv32"))) (d #t) (k 0)))) (h "1rx2lllag6669wb21p7z9da18ixjyak7l3gj2nmjs098m8c37yws")))

