(define-module (crates-io qe mu qemu-system-cris) #:use-module (crates-io))

(define-public crate-qemu-system-cris-0.1.0 (c (n "qemu-system-cris") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-cris"))) (d #t) (k 0)))) (h "18c1nrzpvlfa816xi8cj95wdqy3bckyg0yj2a93n9gnj3jgi5yq9")))

(define-public crate-qemu-system-cris-0.1.3 (c (n "qemu-system-cris") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-cris"))) (d #t) (k 0)))) (h "1qfw3rgva0sm8r98s8cv436jrsnsj7g9jr1i88n7ksapva3v76mm")))

