(define-module (crates-io qe mu qemu-system-avr) #:use-module (crates-io))

(define-public crate-qemu-system-avr-0.1.0 (c (n "qemu-system-avr") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-avr"))) (d #t) (k 0)))) (h "0slv8ksv6rvc1dcpxc28xrwwfndpqjkc93z4icmqazx50s26zx70")))

(define-public crate-qemu-system-avr-0.1.3 (c (n "qemu-system-avr") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-avr"))) (d #t) (k 0)))) (h "0zdbamwkzd9bzy56xq028v8ir80jnd5sxmyq4nmajslgsvhnnj82")))

