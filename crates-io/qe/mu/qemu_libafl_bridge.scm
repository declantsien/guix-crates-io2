(define-module (crates-io qe mu qemu_libafl_bridge) #:use-module (crates-io))

(define-public crate-qemu_libafl_bridge-0.1.0 (c (n "qemu_libafl_bridge") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "0zgjii166r4584jakk6gagc0ff4bbvjiqnwx4apgmw0anyb6nj67")))

(define-public crate-qemu_libafl_bridge-0.2.0 (c (n "qemu_libafl_bridge") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "1awplvbnc7dj1abqygjjsyz7dn9wydzrgj1f8df764d0xlj5vkaq")))

(define-public crate-qemu_libafl_bridge-0.2.1 (c (n "qemu_libafl_bridge") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "08j7g66bd9hdwvq85zpw9ip3fzd5yhmqx2la007qf9dx2dy636ay")))

(define-public crate-qemu_libafl_bridge-0.2.2 (c (n "qemu_libafl_bridge") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "1dv44n2qhjnqir4dncw735xfdxy07jw6ssh9k6m37k91w9ikfajq")))

(define-public crate-qemu_libafl_bridge-0.3.0 (c (n "qemu_libafl_bridge") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "0xrdcgrd92r5ywm6syqf9938az4ja9ci38i1wy8gvpamzkvb0qag")))

(define-public crate-qemu_libafl_bridge-0.3.1 (c (n "qemu_libafl_bridge") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "03rrf5a76wa9d0q2mv1vh6wrj6xsk12c80bjxsi0imz7fm1d43d4")))

(define-public crate-qemu_libafl_bridge-0.3.2 (c (n "qemu_libafl_bridge") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "06p276x5nzgw7y46s4p5gkpdfffw1bv7ixbwx30jmwwzsjnmkhnx")))

