(define-module (crates-io qe mu qemu-armeb) #:use-module (crates-io))

(define-public crate-qemu-armeb-0.1.0 (c (n "qemu-armeb") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-armeb"))) (d #t) (k 0)))) (h "1jk3rzzvf6zqvj90kxgbpys100awjbwk0fdyxhyr58zkjk7a4nk1")))

(define-public crate-qemu-armeb-0.1.3 (c (n "qemu-armeb") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-armeb"))) (d #t) (k 0)))) (h "05i1kfqjzfgnafbrg1canmpyswqixvc3nwqip5rx3wrwamn988kv")))

