(define-module (crates-io qe mu qemu-exit) #:use-module (crates-io))

(define-public crate-qemu-exit-0.1.0 (c (n "qemu-exit") (v "0.1.0") (d (list (d (n "x86_64") (r "0.7.*") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "01dizp0s7mqirj22a0h0ln20l1qa1jklq7w0id2p8854vxk8kvvf")))

(define-public crate-qemu-exit-0.1.1 (c (n "qemu-exit") (v "0.1.1") (d (list (d (n "x86_64") (r "0.7.*") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "1anz5ric5ggqz34n4wmlvha5vrd7ny2pixl6mn6sw6mmnqz1ny65")))

(define-public crate-qemu-exit-0.1.2 (c (n "qemu-exit") (v "0.1.2") (d (list (d (n "x86_64") (r "0.7.*") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "0hcskh7gzr404b2j0q6nfffw78gcnxi3c4h38azb7917qdi3clwm")))

(define-public crate-qemu-exit-0.1.3 (c (n "qemu-exit") (v "0.1.3") (d (list (d (n "x86_64") (r "^0.11.4") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "18pl9drjc6k827lxwqhv8f385mbv53ng5wwfj5xr2gfkvd499ysf")))

(define-public crate-qemu-exit-0.1.4 (c (n "qemu-exit") (v "0.1.4") (d (list (d (n "x86_64") (r "^0.11.7") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "0k9ikriwk844vy4mf3hha0z8gniv12lmgzv55rwxh0w0i0wcvvhf")))

(define-public crate-qemu-exit-1.0.0 (c (n "qemu-exit") (v "1.0.0") (h "1i9qypfmmzag735zhqmsdq3940wwlpk9zfj8xjh7qb2pahwy2fmp")))

(define-public crate-qemu-exit-1.0.1 (c (n "qemu-exit") (v "1.0.1") (h "1dfis1vab1wsqjvb1sy723n538v06jnsckrcard54drh8kpz0r0f")))

(define-public crate-qemu-exit-1.0.2 (c (n "qemu-exit") (v "1.0.2") (h "0xmlqqdhjia8vr9ggykslm4il20p85n35nkvssj21gjm68ybwakh")))

(define-public crate-qemu-exit-2.0.0 (c (n "qemu-exit") (v "2.0.0") (h "10yd15svyk81r3c84xa758v2ky8rl232mb9084fhcwnp58hxlyg1")))

(define-public crate-qemu-exit-2.0.1 (c (n "qemu-exit") (v "2.0.1") (h "1283p2nwh4jr3fvlmamzdlfd87xahs6qmgqlc6mm4m36817vj3i2")))

(define-public crate-qemu-exit-3.0.0 (c (n "qemu-exit") (v "3.0.0") (h "0hymwm68gsx70dmn8k7vnfxsicm3hd0qggqpsw7a88nmfr1a03qf")))

(define-public crate-qemu-exit-3.0.1 (c (n "qemu-exit") (v "3.0.1") (h "12j2zhyi2vvx91f0l0i0r4f17cr570wdby71j2w3zizwbcj27w4z")))

(define-public crate-qemu-exit-3.0.2 (c (n "qemu-exit") (v "3.0.2") (h "1j915dlvhqsvcj5g4qipjivg93v1qaigpqslq01h3vgfh1jzvc4b")))

