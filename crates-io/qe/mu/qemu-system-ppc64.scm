(define-module (crates-io qe mu qemu-system-ppc64) #:use-module (crates-io))

(define-public crate-qemu-system-ppc64-0.1.0 (c (n "qemu-system-ppc64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-ppc64"))) (d #t) (k 0)))) (h "0z5v8xcgdibgryfzvvggw8ksq67yvhgm27mm9jls8j7a81h7zj0y")))

(define-public crate-qemu-system-ppc64-0.1.3 (c (n "qemu-system-ppc64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-ppc64"))) (d #t) (k 0)))) (h "155phqfklxi6126a8pwlw67w4b029yhg5nyilgaysqb7rqavl154")))

