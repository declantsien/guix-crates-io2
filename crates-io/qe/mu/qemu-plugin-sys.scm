(define-module (crates-io qe mu qemu-plugin-sys) #:use-module (crates-io))

(define-public crate-qemu-plugin-sys-8.1.3-v1 (c (n "qemu-plugin-sys") (v "8.1.3-v1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.22") (f (quote ("rustls-tls" "blocking"))) (k 1)) (d (n "tar") (r "^0.4.40") (d #t) (k 1)) (d (n "xz2") (r "^0.1.7") (d #t) (k 1)))) (h "0azdmxjgcglnvv61wbya181qqbpv3a0bsc58wly77c092s1pkn3a")))

(define-public crate-qemu-plugin-sys-8.1.3-v2 (c (n "qemu-plugin-sys") (v "8.1.3-v2") (h "1jv4n5hr6xq22qy0sjdm6vr2yra8vps7sxapjvjl2qcjqa71xnpj")))

(define-public crate-qemu-plugin-sys-8.1.3-v3 (c (n "qemu-plugin-sys") (v "8.1.3-v3") (h "09dnv3saivgzwshf1j3h1zgy177jh8crkxvpz0xjign4snl4ykjw")))

(define-public crate-qemu-plugin-sys-8.1.3-v4 (c (n "qemu-plugin-sys") (v "8.1.3-v4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)))) (h "0sj36akbhf1dngf5sqkiy0j5fjys45rdqs6h26xvam2hcpz8ynqm")))

(define-public crate-qemu-plugin-sys-8.2.0-v1 (c (n "qemu-plugin-sys") (v "8.2.0-v1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)))) (h "1rbyvz2z6svxj7w43dkbnhg8d05mbd93h3k6h30cyrkjjjq2zsjr")))

(define-public crate-qemu-plugin-sys-8.2.2-v0 (c (n "qemu-plugin-sys") (v "8.2.2-v0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)))) (h "0lc1nrsx30hdwh3a9w7npi1sikf048zfq0k0d601fxj5s3ngd552")))

