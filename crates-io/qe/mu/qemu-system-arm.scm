(define-module (crates-io qe mu qemu-system-arm) #:use-module (crates-io))

(define-public crate-qemu-system-arm-0.1.0 (c (n "qemu-system-arm") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-arm"))) (d #t) (k 0)))) (h "0g1kf26agpqb9mk7akfx4saz097iw8h2ixj9fz7pgywk0rldwqcp")))

(define-public crate-qemu-system-arm-0.1.3 (c (n "qemu-system-arm") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-arm"))) (d #t) (k 0)))) (h "0ciw5g64jpkfjd8z7c38mmz58slr6z0ngpgpvnr4q76z0siivd2n")))

