(define-module (crates-io qe mu qemu-microblaze) #:use-module (crates-io))

(define-public crate-qemu-microblaze-0.1.0 (c (n "qemu-microblaze") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-microblaze"))) (d #t) (k 0)))) (h "0b26xybcfx7y9k1l9dixgwsj96i5fig175vbasqw8y9829vsjc6s")))

(define-public crate-qemu-microblaze-0.1.3 (c (n "qemu-microblaze") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-microblaze"))) (d #t) (k 0)))) (h "0k8mnrmh8rchgpwc0ibr53cp9pqvqvp06v8zyx72a3akg5l55j2y")))

