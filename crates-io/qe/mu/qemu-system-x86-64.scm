(define-module (crates-io qe mu qemu-system-x86-64) #:use-module (crates-io))

(define-public crate-qemu-system-x86-64-0.1.0 (c (n "qemu-system-x86-64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-x86_64"))) (d #t) (k 0)))) (h "1fbb9s291nd90gzm6harsj6b2696v06wdfkn1qckaxcgn8h2rrmx")))

(define-public crate-qemu-system-x86-64-0.1.3 (c (n "qemu-system-x86-64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-x86_64"))) (d #t) (k 0)))) (h "1dxnc73xw8f3vx3kviznjqdwahih24cdjs3j1j4wnsq1w8gh0dfh")))

