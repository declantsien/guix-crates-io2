(define-module (crates-io qe mu qemu-system-s390x) #:use-module (crates-io))

(define-public crate-qemu-system-s390x-0.1.0 (c (n "qemu-system-s390x") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-s390x"))) (d #t) (k 0)))) (h "12c04f8y308y6g6p3ysfbzd5g0hslxcyqxfndxfbjhg81zkdjd34")))

(define-public crate-qemu-system-s390x-0.1.3 (c (n "qemu-system-s390x") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-s390x"))) (d #t) (k 0)))) (h "08bsv355353cmnqnfm8ld8f4s250m4bvwmvs7apflgvccxs3s46c")))

