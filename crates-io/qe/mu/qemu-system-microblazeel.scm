(define-module (crates-io qe mu qemu-system-microblazeel) #:use-module (crates-io))

(define-public crate-qemu-system-microblazeel-0.1.0 (c (n "qemu-system-microblazeel") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-microblazeel"))) (d #t) (k 0)))) (h "1a3shryillz6bzw3mg6rjhsr5makrijmqm7y76yraczz20y15ywd")))

(define-public crate-qemu-system-microblazeel-0.1.3 (c (n "qemu-system-microblazeel") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-microblazeel"))) (d #t) (k 0)))) (h "1mrznhd0xhm69dgndyqg5qgf5rm80pgr0vjvkg0ib5ysb2hmlyii")))

