(define-module (crates-io qe mu qemu-mipsn32el) #:use-module (crates-io))

(define-public crate-qemu-mipsn32el-0.1.0 (c (n "qemu-mipsn32el") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-mipsn32el"))) (d #t) (k 0)))) (h "1l1cz3cmz9jp3yz083w2lx9zbczi3w2d55nbfiyc8gkkagxb71vv")))

(define-public crate-qemu-mipsn32el-0.1.3 (c (n "qemu-mipsn32el") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-mipsn32el"))) (d #t) (k 0)))) (h "00rpbi11knkmhg4b0v49ni3s9xfka6x8za71v4f428ivn6zp47l4")))

