(define-module (crates-io qe mu qemu-alpha) #:use-module (crates-io))

(define-public crate-qemu-alpha-0.1.0 (c (n "qemu-alpha") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.4") (f (quote ("qemu-alpha"))) (d #t) (k 0)))) (h "1likhybfk24vhrmwnacj0vi0vg6996vwis1274iwklgv3vyj4g8x")))

(define-public crate-qemu-alpha-0.1.2 (c (n "qemu-alpha") (v "0.1.2") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-alpha"))) (d #t) (k 0)))) (h "15fqjzwpfvrqys20knyn73clf2d8xxsxb8ypc6i5bgza6cnq4xry")))

(define-public crate-qemu-alpha-0.1.3 (c (n "qemu-alpha") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-alpha"))) (d #t) (k 0)))) (h "0s2n5zplzckdw4ccx30b0gw62ijhd1j6djn01rwj67n0qhmyfms4")))

