(define-module (crates-io qe mu qemu-ppc64) #:use-module (crates-io))

(define-public crate-qemu-ppc64-0.1.0 (c (n "qemu-ppc64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-ppc64"))) (d #t) (k 0)))) (h "04lnkxk9hpqikjzwm3wr3bbqz4y94nchv9gfanf0ybq7nmrkfvcd")))

(define-public crate-qemu-ppc64-0.1.3 (c (n "qemu-ppc64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-ppc64"))) (d #t) (k 0)))) (h "07rnp5yjqa7gha0j88hcvwcd8lsrqa9q2q90c31fyy4pcysw5scq")))

