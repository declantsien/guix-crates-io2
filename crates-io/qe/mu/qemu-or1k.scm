(define-module (crates-io qe mu qemu-or1k) #:use-module (crates-io))

(define-public crate-qemu-or1k-0.1.0 (c (n "qemu-or1k") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-or1k"))) (d #t) (k 0)))) (h "0gixmiiiwwljwsib8yzclaqhwicpycvxd2adv8h6rf2hwk9p7f5h")))

(define-public crate-qemu-or1k-0.1.3 (c (n "qemu-or1k") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-or1k"))) (d #t) (k 0)))) (h "0ifhri89168swg4gyvxh5kqdv0gvra59b1ziv00dzfs2gpl211h6")))

