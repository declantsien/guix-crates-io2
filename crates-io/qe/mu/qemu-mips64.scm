(define-module (crates-io qe mu qemu-mips64) #:use-module (crates-io))

(define-public crate-qemu-mips64-0.1.0 (c (n "qemu-mips64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-mips64"))) (d #t) (k 0)))) (h "13ci3yrzvr0l3r0bk0ra622ngp7mpy59c0f0wfl9l6i4vlskn6vp")))

(define-public crate-qemu-mips64-0.1.3 (c (n "qemu-mips64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-mips64"))) (d #t) (k 0)))) (h "18d763g2mnfk6rv5j5rwyasklgfj8wnbflq3yqw6hv5msf6q547n")))

