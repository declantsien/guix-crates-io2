(define-module (crates-io qe mu qemu-sparc64) #:use-module (crates-io))

(define-public crate-qemu-sparc64-0.1.0 (c (n "qemu-sparc64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-sparc64"))) (d #t) (k 0)))) (h "0gdimbgxrvl196h1fx6ikyxvv1bv02dfiwd81cx8sqjl7vi64pv6")))

(define-public crate-qemu-sparc64-0.1.3 (c (n "qemu-sparc64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-sparc64"))) (d #t) (k 0)))) (h "09a6j21g5pwdpp5a15r88b9dvrlpbaxz2p003gc7mdnyfmjgdcac")))

