(define-module (crates-io qe mu qemu-system-mips64) #:use-module (crates-io))

(define-public crate-qemu-system-mips64-0.1.0 (c (n "qemu-system-mips64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-mips64"))) (d #t) (k 0)))) (h "0w632ymr8p02sbz0yyvyxk6sphpp27lclqh1slwhjpzwb5y8y8r8")))

(define-public crate-qemu-system-mips64-0.1.3 (c (n "qemu-system-mips64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-mips64"))) (d #t) (k 0)))) (h "02gl07873dmi8ymgc3kkryqdpjk71k00z4c19nyxgxj612swrq69")))

