(define-module (crates-io qe mu qemu-system-sparc) #:use-module (crates-io))

(define-public crate-qemu-system-sparc-0.1.0 (c (n "qemu-system-sparc") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-sparc"))) (d #t) (k 0)))) (h "0vk654flslq44gb2xqa0gdmwgp3mz2jg8mvyqlyxs36ci0751vw7")))

(define-public crate-qemu-system-sparc-0.1.3 (c (n "qemu-system-sparc") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-sparc"))) (d #t) (k 0)))) (h "1p8fmxkmbywyhlc2dbbl085mfni7rd62gwcajshmdy3drpx0wbm3")))

