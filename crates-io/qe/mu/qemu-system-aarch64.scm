(define-module (crates-io qe mu qemu-system-aarch64) #:use-module (crates-io))

(define-public crate-qemu-system-aarch64-0.1.0 (c (n "qemu-system-aarch64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-aarch64"))) (d #t) (k 0)))) (h "0m6flrkc0180ihl08gdn8bvkc91cw25j05fn40fwjszjnqx01zwa")))

(define-public crate-qemu-system-aarch64-0.1.3 (c (n "qemu-system-aarch64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-aarch64"))) (d #t) (k 0)))) (h "18jcvva5hjwcq0yhv9ir5241cmjbi6m7wfz0i65vvgs48mi8x9b4")))

