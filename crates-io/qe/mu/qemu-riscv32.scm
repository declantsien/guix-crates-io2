(define-module (crates-io qe mu qemu-riscv32) #:use-module (crates-io))

(define-public crate-qemu-riscv32-0.1.0 (c (n "qemu-riscv32") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-riscv32"))) (d #t) (k 0)))) (h "1hg1md7bn3sh63q7897nl6k8bj4fn55p7ypwiqz0r5jhs5g7zfil")))

(define-public crate-qemu-riscv32-0.1.3 (c (n "qemu-riscv32") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-riscv32"))) (d #t) (k 0)))) (h "0parzjm65b59fzi9brwkq2v2bpsqwqdp2lvxfnxp87x5m20b4x6n")))

