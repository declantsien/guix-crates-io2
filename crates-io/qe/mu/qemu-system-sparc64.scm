(define-module (crates-io qe mu qemu-system-sparc64) #:use-module (crates-io))

(define-public crate-qemu-system-sparc64-0.1.0 (c (n "qemu-system-sparc64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-sparc64"))) (d #t) (k 0)))) (h "14vkps92717m3vlm8f7afna0mqryqg1wnins4f7jb20f7j2xfdp3")))

(define-public crate-qemu-system-sparc64-0.1.3 (c (n "qemu-system-sparc64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-sparc64"))) (d #t) (k 0)))) (h "062v9b4p7yqkc7zfnb1dirm2siw2p9mxq4w1rrj1zvb7qj8lc13h")))

