(define-module (crates-io qe mu qemu-mips) #:use-module (crates-io))

(define-public crate-qemu-mips-0.1.0 (c (n "qemu-mips") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-mips"))) (d #t) (k 0)))) (h "1x4py4hphzja1c35z5vihf9s7kkq76rcjwwbcjkfd2672s1i37jc")))

(define-public crate-qemu-mips-0.1.3 (c (n "qemu-mips") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-mips"))) (d #t) (k 0)))) (h "0qiil8lx7r3hcsjfgh04zx1hpy12zz94x7vapgq86hmmn5qhpf3d")))

