(define-module (crates-io qe mu qemu-system-microblaze) #:use-module (crates-io))

(define-public crate-qemu-system-microblaze-0.1.0 (c (n "qemu-system-microblaze") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-microblaze"))) (d #t) (k 0)))) (h "1z64af2h8m1pcd4nb44yh6j89fgr32wzm24cmfdk658kp83sldsw")))

(define-public crate-qemu-system-microblaze-0.1.3 (c (n "qemu-system-microblaze") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-microblaze"))) (d #t) (k 0)))) (h "0m8vblgmwf5jkb1a3lg2n1g5l65alwmwrnc0j4l46r492md1pq0q")))

