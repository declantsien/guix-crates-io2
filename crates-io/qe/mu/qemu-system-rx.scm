(define-module (crates-io qe mu qemu-system-rx) #:use-module (crates-io))

(define-public crate-qemu-system-rx-0.1.0 (c (n "qemu-system-rx") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-rx"))) (d #t) (k 0)))) (h "04s0853cz4ij11malma92ljsb5nh0krv28dsf8sigdyd380xjn60")))

(define-public crate-qemu-system-rx-0.1.3 (c (n "qemu-system-rx") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-rx"))) (d #t) (k 0)))) (h "13b2cclrw11796mcnsnf9acx95b7mf15sxjm43smd2050wyh5yqi")))

