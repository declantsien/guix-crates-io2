(define-module (crates-io qe mu qemu-nios2) #:use-module (crates-io))

(define-public crate-qemu-nios2-0.1.0 (c (n "qemu-nios2") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-nios2"))) (d #t) (k 0)))) (h "0a49dwhaw9b9xxfn8wvgaska12457yqqdww0vh1cx661p5afk550")))

(define-public crate-qemu-nios2-0.1.3 (c (n "qemu-nios2") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-nios2"))) (d #t) (k 0)))) (h "0ayv1a62b9p95y5h44xsrfi5lmy4i3cx6h13fgi7vyjxbqyd0660")))

