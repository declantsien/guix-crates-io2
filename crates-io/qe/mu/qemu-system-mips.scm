(define-module (crates-io qe mu qemu-system-mips) #:use-module (crates-io))

(define-public crate-qemu-system-mips-0.1.0 (c (n "qemu-system-mips") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-mips"))) (d #t) (k 0)))) (h "06wiccn8w0xmlzdjb3j73585vkg7p0dn375makn46vqqj7ymxks4")))

(define-public crate-qemu-system-mips-0.1.3 (c (n "qemu-system-mips") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-mips"))) (d #t) (k 0)))) (h "0mlq0jx7kl3xx9grkqw8m5sa1pb2vd2j2v7fr6bssrjj87xk5s2i")))

