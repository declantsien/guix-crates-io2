(define-module (crates-io qe mu qemu-system-alpha) #:use-module (crates-io))

(define-public crate-qemu-system-alpha-0.1.0 (c (n "qemu-system-alpha") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-alpha"))) (d #t) (k 0)))) (h "10vy32g4d1phdlzkpnqd8b3fvj4p4345qg6lgwiypbhld0m1xhar")))

(define-public crate-qemu-system-alpha-0.1.3 (c (n "qemu-system-alpha") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-alpha"))) (d #t) (k 0)))) (h "014xqnfqfkrgdjw921bd46z9kq4945shxqqrqk2pkrjxxp2nxvgx")))

