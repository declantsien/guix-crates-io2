(define-module (crates-io qe mu qemu-sh4) #:use-module (crates-io))

(define-public crate-qemu-sh4-0.1.0 (c (n "qemu-sh4") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-sh4"))) (d #t) (k 0)))) (h "1njdi2h6qyiywyirk52c1a5vkcgjla7r9nkymppa74q4cxmsvrd7")))

(define-public crate-qemu-sh4-0.1.3 (c (n "qemu-sh4") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-sh4"))) (d #t) (k 0)))) (h "1raqvinw4vqrq7fa1bhd0wxxclrcad5p3w2w8mgy536hs4xw54hc")))

