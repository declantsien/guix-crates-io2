(define-module (crates-io qe mu qemu-i386) #:use-module (crates-io))

(define-public crate-qemu-i386-0.1.0 (c (n "qemu-i386") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-i386"))) (d #t) (k 0)))) (h "0f16p04iyj14x3pq2bgkg6a4b5q26adxjckpkn2pqis078l3k7ba")))

(define-public crate-qemu-i386-0.1.3 (c (n "qemu-i386") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-i386"))) (d #t) (k 0)))) (h "06n1xc168650rjanhwr0xxar3idw5bnsjk9klgkahqmzgl09a0hr")))

