(define-module (crates-io qe mu qemu-system-riscv64) #:use-module (crates-io))

(define-public crate-qemu-system-riscv64-0.1.0 (c (n "qemu-system-riscv64") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-riscv64"))) (d #t) (k 0)))) (h "0f1c4gcfp4mkr2r9zdwhg1zy5p5rjnrchzdkmisxk7x8nhaxwala")))

(define-public crate-qemu-system-riscv64-0.1.3 (c (n "qemu-system-riscv64") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-riscv64"))) (d #t) (k 0)))) (h "0xzrdg7jbrll7dhfkpy3k33mdy9m6av0yy6fi2bsyn0nzwapw5wa")))

