(define-module (crates-io qe mu qemu-mipsel) #:use-module (crates-io))

(define-public crate-qemu-mipsel-0.1.0 (c (n "qemu-mipsel") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-mipsel"))) (d #t) (k 0)))) (h "1hriiy2x9inmqg39qybfknnw5bfycr9sxw5lr940rf34fkaa5nha")))

(define-public crate-qemu-mipsel-0.1.3 (c (n "qemu-mipsel") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-mipsel"))) (d #t) (k 0)))) (h "1mb70dx58b9pnz1r63vnhjkidcpyvis5chqpagkfc7wzy0q9q1vv")))

