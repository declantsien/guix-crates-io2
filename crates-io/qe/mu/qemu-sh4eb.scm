(define-module (crates-io qe mu qemu-sh4eb) #:use-module (crates-io))

(define-public crate-qemu-sh4eb-0.1.0 (c (n "qemu-sh4eb") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-sh4eb"))) (d #t) (k 0)))) (h "1rjy3sbj8z1354vjzf7bapmlmf5xaqmkpvdlcw294jnwvsnw98l8")))

(define-public crate-qemu-sh4eb-0.1.3 (c (n "qemu-sh4eb") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-sh4eb"))) (d #t) (k 0)))) (h "1gxj3p2mgpgnasbx1vi3719gbv3c5sj7vj8lw3r40jp4xzmsfz3f")))

