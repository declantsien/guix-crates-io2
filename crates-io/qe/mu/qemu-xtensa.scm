(define-module (crates-io qe mu qemu-xtensa) #:use-module (crates-io))

(define-public crate-qemu-xtensa-0.1.0 (c (n "qemu-xtensa") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-xtensa"))) (d #t) (k 0)))) (h "0h6s2dxfvg635ylwzn9pyj5nwsx5jpc5gxasakk3y73jjd4cb1iq")))

(define-public crate-qemu-xtensa-0.1.3 (c (n "qemu-xtensa") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-xtensa"))) (d #t) (k 0)))) (h "0acbh7h53dxi1r3nspqaa1qssswz2hhpcnb1y6r52gd6g8jsxqgn")))

