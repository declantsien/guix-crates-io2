(define-module (crates-io qe mu qemu-cris) #:use-module (crates-io))

(define-public crate-qemu-cris-0.1.0 (c (n "qemu-cris") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-cris"))) (d #t) (k 0)))) (h "1z5n5awsni0xm4w1gr2m1zgdv7rqk0kmhhx284623ifxybjav03n")))

(define-public crate-qemu-cris-0.1.3 (c (n "qemu-cris") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-cris"))) (d #t) (k 0)))) (h "0ilm0857c6zj9gi91drfj81sjhd5rj04xinkg8qwa17dm7sa5qq3")))

