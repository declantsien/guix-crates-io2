(define-module (crates-io qe mu qemu-system-nios2) #:use-module (crates-io))

(define-public crate-qemu-system-nios2-0.1.0 (c (n "qemu-system-nios2") (v "0.1.0") (d (list (d (n "memfd-exec") (r "^0.1.4") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-nios2"))) (d #t) (k 0)))) (h "0255rcl1jbm4kkz0mcpdrcbdzri29njdq3gsmkilwv8iiir74s1g")))

(define-public crate-qemu-system-nios2-0.1.3 (c (n "qemu-system-nios2") (v "0.1.3") (d (list (d (n "memfd-exec") (r "^0.1.5") (d #t) (k 0)) (d (n "qemu") (r "^0.1.5") (f (quote ("qemu-system-nios2"))) (d #t) (k 0)))) (h "1j09vnz7r4rj71wkd18ikjncnvk1hvbzl0dj667b1p4hzd2kajhq")))

