(define-module (crates-io qe cs qecs-core) #:use-module (crates-io))

(define-public crate-qecs-core-0.0.1 (c (n "qecs-core") (v "0.0.1") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.2") (d #t) (k 0)))) (h "19j02i22amwby6l83qpgf4a7sq68rcc5966wdfy6mbizkhs7anic")))

(define-public crate-qecs-core-0.0.2 (c (n "qecs-core") (v "0.0.2") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.2") (d #t) (k 0)))) (h "0i81z8fgjn40wmqqfvln2scankrd3cnw7zxd507ykgaj16mjfxa9")))

(define-public crate-qecs-core-0.0.3 (c (n "qecs-core") (v "0.0.3") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.2") (d #t) (k 0)))) (h "00634bzskncxgb1kbpgc295p3rq9ajx4pllrzbiqb2s2dyzvbigv")))

(define-public crate-qecs-core-0.0.4 (c (n "qecs-core") (v "0.0.4") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.2") (d #t) (k 0)))) (h "0hvxk967s7kfk0lrycqlp5mcwh5dq9ahjmcpjhwf8icyzc1aq6q2")))

(define-public crate-qecs-core-0.0.5 (c (n "qecs-core") (v "0.0.5") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.2") (d #t) (k 0)))) (h "134qapdv2ks4f1xkd1n5cwdcp8q25zxw06358z4ay4bnbrlfvckg")))

(define-public crate-qecs-core-0.0.6 (c (n "qecs-core") (v "0.0.6") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.2") (d #t) (k 0)))) (h "18an0wkdjqmkcpz4wkw8nyszwrjmnamy5vyh5d3vpas6lffw198n")))

(define-public crate-qecs-core-0.0.7 (c (n "qecs-core") (v "0.0.7") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.3") (d #t) (k 0)))) (h "0273xcw5nvlaxhr9pnr60l0s2vv746sqik89v3vgx27bs8dqpb4a")))

(define-public crate-qecs-core-0.0.8 (c (n "qecs-core") (v "0.0.8") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.4") (d #t) (k 0)))) (h "1ppyfjnf6918lxgz9apdarqzx0j0p2dkcqaigxq3ph29mkx16s7m")))

(define-public crate-qecs-core-0.0.9 (c (n "qecs-core") (v "0.0.9") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.4") (d #t) (k 0)))) (h "0z7hlkgqann2l79nhfv4vjczadmxvakc4vprayqq30ny0l8a4ffn")))

(define-public crate-qecs-core-0.0.10 (c (n "qecs-core") (v "0.0.10") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.4") (d #t) (k 0)))) (h "0hvs6hjw9sbvc04g6z1wam4gd5z8b1qsf65sfjfi745as7ds02fk")))

(define-public crate-qecs-core-0.0.11 (c (n "qecs-core") (v "0.0.11") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.4") (d #t) (k 0)))) (h "1k7d4grynh3b6z1n35kydxmxj6953yxrxvnaw1p22kfkdd9lh3cq")))

(define-public crate-qecs-core-0.0.12 (c (n "qecs-core") (v "0.0.12") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.4") (d #t) (k 0)))) (h "05q6lv3pqgkd75nvki6bpkss0w8d01ym0p03nxy0x6bikr73krjp")))

(define-public crate-qecs-core-0.0.13 (c (n "qecs-core") (v "0.0.13") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.4") (d #t) (k 0)))) (h "1inagp94ybpf5jrbkd7g56kwhmvyqaa45aab63dk8yjrv1rd4ar6")))

(define-public crate-qecs-core-0.0.14 (c (n "qecs-core") (v "0.0.14") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.4") (d #t) (k 0)))) (h "1dl8qllpyy5vr5s9qdng3p4wrbnr72y3hnnp98knnz918nn9amb7")))

(define-public crate-qecs-core-0.0.15 (c (n "qecs-core") (v "0.0.15") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.4") (d #t) (k 0)))) (h "146g2qqn6bwzymai3xkf1kfmb7gpahk1kcrzwrh15xw3df775cf5")))

(define-public crate-qecs-core-0.0.16 (c (n "qecs-core") (v "0.0.16") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.4") (d #t) (k 0)))) (h "0jdfwimg595gwrslldx1qayyhd7yz33zaf0iva2mbb1gqf9kaa1w")))

(define-public crate-qecs-core-0.0.17 (c (n "qecs-core") (v "0.0.17") (d (list (d (n "downcast") (r "^0.2.1") (d #t) (k 0)) (d (n "ioc") (r "^0.0.4") (d #t) (k 0)))) (h "06rdsxivhzcan26anz6jfplpyspfxq4r60jvfv89mjzkw1qv8799")))

