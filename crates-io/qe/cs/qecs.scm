(define-module (crates-io qe cs qecs) #:use-module (crates-io))

(define-public crate-qecs-0.0.1 (c (n "qecs") (v "0.0.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "qecs-core") (r "^0.0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (d #t) (k 0)))) (h "0384am2m1jpfp9lsxwr48cb3qy172gbidcr1nhg9jhjg4xzn4j51")))

(define-public crate-qecs-0.0.2 (c (n "qecs") (v "0.0.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "qecs-core") (r "^0.0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (d #t) (k 0)))) (h "0n11rqqinvwwpjvhsp0c07ad5wxg3qrmqyf834lg0qimrz0m27bs")))

(define-public crate-qecs-0.0.3 (c (n "qecs") (v "0.0.3") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "qecs-core") (r "^0.0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (d #t) (k 0)))) (h "1rm7gv76r4pj9vj1bf3mjwdcqlm6c7xzmzfayjrp31aqmnfqg83l")))

(define-public crate-qecs-0.0.4 (c (n "qecs") (v "0.0.4") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "qecs-core") (r "0.0.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (d #t) (k 0)))) (h "0p67xswbv8ykh0xiiifxrax4ddn6symf0qc9jr4bpb1jah1ydnxc")))

(define-public crate-qecs-0.0.5 (c (n "qecs") (v "0.0.5") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "qecs-core") (r "0.0.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (d #t) (k 0)))) (h "1gxxazr0v1w5wnawsbv2xdgxx5an91ja3jpzln04lkn7md0fq3vw")))

(define-public crate-qecs-0.0.6 (c (n "qecs") (v "0.0.6") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "qecs-core") (r "0.0.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (d #t) (k 0)))) (h "0i7xz9npjyfvjp799nb6bchvg6s2ar222n7rn8zarsxzaa8mqm9l")))

(define-public crate-qecs-0.0.7 (c (n "qecs") (v "0.0.7") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "qecs-core") (r "0.0.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (d #t) (k 0)))) (h "10qhk071i80d8yfrpjbdqd0awwaax8anr1nq4l0bqhn12afnnjqw")))

