(define-module (crates-io qe da qeda) #:use-module (crates-io))

(define-public crate-qeda-0.0.1 (c (n "qeda") (v "0.0.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "0qamsxjjbvv6k3rqbajkypqir0s60pgccx388cg9314877zj47fb")))

(define-public crate-qeda-0.0.3 (c (n "qeda") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "svgdom") (r "^0.17") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0zmic1v4syflk5h29rnalmjmbg3hrysc9sswyhzndrrbphs48p2x")))

