(define-module (crates-io qe nt qentities) #:use-module (crates-io))

(define-public crate-qentities-0.1.0 (c (n "qentities") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "bstr") (r "^1.6.0") (d #t) (k 0)) (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (f (quote ("raw"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "11b18d7jbp0f8h5cxcaydgb7fispgnvys0bgq0nzigsprxd2c77a")))

(define-public crate-qentities-0.2.0 (c (n "qentities") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "bstr") (r "^1.6.0") (d #t) (k 0)) (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0al0qz973ml8qdhp8y71lchs7yjz85q5ssk2y3m6wpiipvnngnmw")))

(define-public crate-qentities-0.2.1 (c (n "qentities") (v "0.2.1") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "bstr") (r "^1.6.0") (d #t) (k 0)) (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "1zjmrf08q7770wpp9kwgqwqi33jkjj40pz0ry2kfaivf865qcmkm")))

(define-public crate-qentities-0.2.2 (c (n "qentities") (v "0.2.2") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "bstr") (r "^1.6.0") (d #t) (k 0)) (d (n "deepsize") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "052ar0afhi4apl40nnm1i3ay6nqqzs32ms9ln3z9a0cdz77i5lbv")))

