(define-module (crates-io qe sp qesp) #:use-module (crates-io))

(define-public crate-qesp-0.1.0 (c (n "qesp") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1fbl7km0wif2gf8k7gbrrl8a2m66lpfavcn8qlvnqw0gxfqrm64z")))

(define-public crate-qesp-0.2.0 (c (n "qesp") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0fqrialrp1nynn0y8sbaf2ncinb1h9iiv5y6d83jpqh6fbzlk05p")))

(define-public crate-qesp-0.2.1 (c (n "qesp") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1jfn7q8b07qqxk3kgg6xgv9yn2ih7qn644wzqyhrn6afci6ac71z")))

(define-public crate-qesp-0.2.2 (c (n "qesp") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1xj5p2vcz8ijqbiw7n6l43sbx2fq53dljk790xpb9kin25fkc0sq")))

(define-public crate-qesp-0.2.3 (c (n "qesp") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "11wlbfgiq8wcp2pv8hlxi1pv8zv267s7crcljz45bbd44839c5m5")))

(define-public crate-qesp-0.2.4 (c (n "qesp") (v "0.2.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1px9sm73kaglg04h3fnsgi024wmn1z2xc8gz6y929c97fd0jlqjp")))

