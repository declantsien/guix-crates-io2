(define-module (crates-io fp lu fplus-cli) #:use-module (crates-io))

(define-public crate-fplus-cli-0.1.0 (c (n "fplus-cli") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fplus-database") (r "^0.1.0") (d #t) (k 0)) (d (n "fplus-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (d #t) (k 0)))) (h "1zr3ns0d9bgadxj7ffnwcb5i4qlfq86y1nh9qsniw3abfd3isnqz")))

(define-public crate-fplus-cli-0.1.1 (c (n "fplus-cli") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fplus-database") (r "^0.1.1") (d #t) (k 0)) (d (n "fplus-lib") (r "^0.1.1") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (d #t) (k 0)))) (h "0dal0zgxsyp3kji8kcj4njgblgpka5w7wssgixg5plbaljg28a4h")))

(define-public crate-fplus-cli-0.1.3 (c (n "fplus-cli") (v "0.1.3") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fplus-database") (r "^0.1.3") (d #t) (k 0)) (d (n "fplus-lib") (r "^0.1.3") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (d #t) (k 0)))) (h "1x67m3wygsar5w64zvlczcql7liqy75bpp9v1kk4p9mhci6zpii7")))

(define-public crate-fplus-cli-1.0.3 (c (n "fplus-cli") (v "1.0.3") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fplus-lib") (r "^1.0.3") (d #t) (k 0)))) (h "0avmv2g925grm46z99ldz0rqbl229z352flwga49hqrpsmsyr301")))

(define-public crate-fplus-cli-1.0.5 (c (n "fplus-cli") (v "1.0.5") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fplus-lib") (r "^1.0.5") (d #t) (k 0)))) (h "00kc9yjbkwjvb8503fqar1s9nvmsk1kdi2j4mb4lw2x3c8hsgx4g")))

(define-public crate-fplus-cli-1.0.6 (c (n "fplus-cli") (v "1.0.6") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fplus-lib") (r "^1.0.6") (d #t) (k 0)))) (h "0fzz18j0v8rnngwg26yfvxhs3m535qsk061fwc4wvc87zazfskk9")))

(define-public crate-fplus-cli-1.0.7 (c (n "fplus-cli") (v "1.0.7") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fplus-lib") (r "^1.0.7") (d #t) (k 0)))) (h "0zxp2bv5cxny98nsrdshjq66595ic0a6h4hnnm4jxbrdkdc2n1lf")))

(define-public crate-fplus-cli-1.0.8 (c (n "fplus-cli") (v "1.0.8") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fplus-lib") (r "^1.0.8") (d #t) (k 0)))) (h "1iadapnx1zr01qqm5nzzb4c3lrrbr2y813yymb0f7qx0sk4wa4g2")))

(define-public crate-fplus-cli-1.0.9 (c (n "fplus-cli") (v "1.0.9") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fplus-lib") (r "^1.0.9") (d #t) (k 0)))) (h "1rr7hwszs3knbh3bjgndk8ggkkkd6yr98pq87a8cbw97gyasb918")))

