(define-module (crates-io fp lu fplus-database) #:use-module (crates-io))

(define-public crate-fplus-database-0.1.0 (c (n "fplus-database") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0lmhc6f5jjds3gcd4mfp1hmzbq5f2yy4gvji4jqilan73nbxsdhg")))

(define-public crate-fplus-database-0.1.1 (c (n "fplus-database") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (f (quote ("openssl-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "1z9mhz0hqpf7g499m7ya1ygi45ix2vgnc20wfgc1hgcl1xsvmrbv")))

(define-public crate-fplus-database-0.1.2 (c (n "fplus-database") (v "0.1.2") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (f (quote ("openssl-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "05257k4nkhb083l04j80x6c6hisjci9wfjy3ha7iysww74xk6d44")))

(define-public crate-fplus-database-0.1.3 (c (n "fplus-database") (v "0.1.3") (d (list (d (n "actix-web") (r "^4.4.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.0") (f (quote ("openssl-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "03zaq2b3vilc5cra0clwgsi5qlyxfjk24bxrhn9w9l3zcl81fnsa")))

