(define-module (crates-io fp e- fpe-v04) #:use-module (crates-io))

(define-public crate-fpe-v04-0.4.0 (c (n "fpe-v04") (v "0.4.0") (d (list (d (n "aes") (r "^0.7.4") (d #t) (k 0)) (d (n "aes-old") (r "^0.5") (d #t) (k 2) (p "aes")) (d (n "block-modes") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0v21z8cin8r1ndfclf2k5c6b7f8jik9fb63if1fykdb1vq6mc8c5") (y #t)))

