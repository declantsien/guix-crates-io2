(define-module (crates-io fp op fpopt) #:use-module (crates-io))

(define-public crate-fpopt-0.0.1 (c (n "fpopt") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0lvs7nwfazn09156k6y04q2mn3gvf8zm045mh9lsd9ihj3razcv3")))

(define-public crate-fpopt-0.0.2 (c (n "fpopt") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0mvp9qyv14lbd1x4f5ha0hp7q2c5dfw9mnh8gdmbi3c3k43x82dc")))

(define-public crate-fpopt-0.0.3 (c (n "fpopt") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "11j50jayhfbk1k823jnwqh2lw9wsfdz6ihwairxikcng992xxygf")))

