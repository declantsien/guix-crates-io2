(define-module (crates-io fp -b fp-bindgen-macros) #:use-module (crates-io))

(define-public crate-fp-bindgen-macros-1.0.0-rc.1 (c (n "fp-bindgen-macros") (v "1.0.0-rc.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0m0svgy26s49fx8pycmbhkycwv1g86nf27pfdfml7dknmn8azgf0")))

(define-public crate-fp-bindgen-macros-1.0.0 (c (n "fp-bindgen-macros") (v "1.0.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gkmplr14raranb7jrxf9afz6j538kx2zylai7pcnvbl9y40i5y2")))

(define-public crate-fp-bindgen-macros-2.0.0 (c (n "fp-bindgen-macros") (v "2.0.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10a8xr8mqnb5ix6a2f09977prhzgvlv9jnf04lr6zxxavrzmqpw1")))

(define-public crate-fp-bindgen-macros-2.0.1 (c (n "fp-bindgen-macros") (v "2.0.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06hx3qd0xf2q5q0yy6cryhjhrwxrdvhrvb7aipyvqybn2130npnz")))

(define-public crate-fp-bindgen-macros-2.1.0 (c (n "fp-bindgen-macros") (v "2.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09sb4a2i73v410an1z1nqd9k1b4yqfifw8h1h8z0ncak9qximzhb")))

(define-public crate-fp-bindgen-macros-2.2.0 (c (n "fp-bindgen-macros") (v "2.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m6ifgw3qvpmmcpja0zaxs4wayw4hdg7m8li4pv5lqsypwp02a3f")))

(define-public crate-fp-bindgen-macros-2.3.0 (c (n "fp-bindgen-macros") (v "2.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xvlfi5cqq9jrb9bbi7pgm722n0bmd1r0icy54pcsnmgqac76bgn")))

(define-public crate-fp-bindgen-macros-2.4.0 (c (n "fp-bindgen-macros") (v "2.4.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y8kmdcld417vq8rhf248nccyjrw8fm3jai3q0zy65i5k336blpm")))

(define-public crate-fp-bindgen-macros-3.0.0-alpha.1 (c (n "fp-bindgen-macros") (v "3.0.0-alpha.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hjpb2njf74g8n1pj7hh25bg1kx3clqik5c2xpl11sfab20c30hg")))

(define-public crate-fp-bindgen-macros-3.0.0-beta.1 (c (n "fp-bindgen-macros") (v "3.0.0-beta.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zqw4mrkmg0vzrscfzqrg6xdy40wygdkrps3cvvvf84gpagrz6hp")))

(define-public crate-fp-bindgen-macros-3.0.0 (c (n "fp-bindgen-macros") (v "3.0.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g1nabjj8lrr501l6s723knxhh818zh7wi5rn6jsqqscdxphi822")))

