(define-module (crates-io fp -g fp-growth) #:use-module (crates-io))

(define-public crate-fp-growth-0.1.0 (c (n "fp-growth") (v "0.1.0") (h "15zxmvi21vbnjkazkjjddvjk91p3qbhsbcsk0f7rvbi62mwakf6z")))

(define-public crate-fp-growth-0.1.1 (c (n "fp-growth") (v "0.1.1") (h "1fipvs9b748vr8pqc1yvdf8zpj1kh1k1q7c147c0spvz0wlqqmyd")))

(define-public crate-fp-growth-0.1.2 (c (n "fp-growth") (v "0.1.2") (h "1644s5bgbfmc7n4ypjjrhgv9zimhc79li91rdwlnj8g6lzwiydyn")))

(define-public crate-fp-growth-0.1.3 (c (n "fp-growth") (v "0.1.3") (h "19g7dyi8rd64qjfwhvnqbda9ay0l4avh1c24wm9v6h6s1bi8h91a") (y #t)))

(define-public crate-fp-growth-0.1.4 (c (n "fp-growth") (v "0.1.4") (h "1spmw19vqmhpfd8qvyckhfpin7glhn4kc2l36pdncmd7yic3jn6h")))

(define-public crate-fp-growth-0.1.6 (c (n "fp-growth") (v "0.1.6") (h "1f4yp5c4idzqbkhyaz5wlp7sxvhsn8acxjjsrn6wf50rs7fva470")))

