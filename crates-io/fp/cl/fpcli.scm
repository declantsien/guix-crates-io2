(define-module (crates-io fp cl fpcli) #:use-module (crates-io))

(define-public crate-fpcli-0.0.1 (c (n "fpcli") (v "0.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.8") (d #t) (k 0)))) (h "18nad7hcmg8mz7q8bj4a38y7vnfgm2qp7fc036yj8cl7wkmlbi0w")))

(define-public crate-fpcli-0.0.2 (c (n "fpcli") (v "0.0.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.8") (d #t) (k 0)))) (h "17anw8rr8qvsdlkg8wwlc5sii73ivzzqxx5hpyvpnjjr9ah0yz9h")))

(define-public crate-fpcli-0.0.3 (c (n "fpcli") (v "0.0.3") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.8") (d #t) (k 0)))) (h "1paxzas60m84dj5vhx91sc0pkwhb2nnxfqjm4g8gqrh0yyih09z8")))

(define-public crate-fpcli-0.0.4 (c (n "fpcli") (v "0.0.4") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.9") (d #t) (k 0)))) (h "1kr29by1xijwcncfzakd2zg061plfrz4fq0i9laqx6zrn8vg8l8a")))

(define-public crate-fpcli-0.0.5 (c (n "fpcli") (v "0.0.5") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.9") (d #t) (k 0)))) (h "01wyy7xxb66bsr1rc5xr68drznjyzb8n3dvqh2k6bzc68ii5gx3v") (r "1.56")))

(define-public crate-fpcli-0.0.6 (c (n "fpcli") (v "0.0.6") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.10") (d #t) (k 0)))) (h "0ld5jx5arpf2m51qhhsnx8267qdxmipjgk8iha3hap8y679l84lm") (r "1.56")))

(define-public crate-fpcli-0.0.7 (c (n "fpcli") (v "0.0.7") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.12") (d #t) (k 0)))) (h "19sfr644n4yn5a5gf4cr2m4rh4dxhhbqybwjmjcq7a0514b3hxqr") (r "1.56")))

(define-public crate-fpcli-0.0.8 (c (n "fpcli") (v "0.0.8") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.12") (d #t) (k 0)))) (h "0aj5p51gaf21vlla7pipf52pclnkigqd4y463f0jdv1cp80x98v1") (r "1.56")))

(define-public crate-fpcli-0.1.0 (c (n "fpcli") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)))) (h "0lx16dj1496b85hxjizbzcp4ak906gns71c7ldkyrmdf18k4na6a") (r "1.56")))

(define-public crate-fpcli-0.2.0 (c (n "fpcli") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.13") (f (quote ("toml"))) (d #t) (k 0)))) (h "16p3mzcfgr7wipfagh3rv1246pr5ir4nly0ikzf649n56gpnbamw") (r "1.56")))

(define-public crate-fpcli-0.3.0 (c (n "fpcli") (v "0.3.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.14") (f (quote ("toml"))) (d #t) (k 0)))) (h "1b0jin0ryzhxiqiv8cnmq1g5g3ny9wv2s8gzf2nfq7f6vnv5r67n") (r "1.56")))

(define-public crate-fpcli-0.3.1 (c (n "fpcli") (v "0.3.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.14") (f (quote ("toml"))) (d #t) (k 0)))) (h "1ih12mfscm6mxq6vmq6wv7v6i9f15i3lgaxbx2njg9likfajp1gx") (r "1.56")))

(define-public crate-fpcli-0.4.0 (c (n "fpcli") (v "0.4.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.18") (d #t) (k 0)))) (h "0y3dplrrlhwdmyiiwhmciy8352agwjh5j15q2mpi820wcqsi44qa") (r "1.56")))

(define-public crate-fpcli-0.5.0 (c (n "fpcli") (v "0.5.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flatpak-rs") (r "^0.18") (d #t) (k 0)))) (h "0cbdk83q6iawibr1qq9pgjw3kvnwzbs0jya35yp95lv17r2rrpn7") (r "1.56")))

