(define-module (crates-io fp ri fprint-rs) #:use-module (crates-io))

(define-public crate-fprint-rs-0.1.0 (c (n "fprint-rs") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "fprint-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0xcbx179slhn3wjc3nb2accbvgf85381z2pwa07wa3kyq0w4yqrr")))

