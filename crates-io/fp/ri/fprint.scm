(define-module (crates-io fp ri fprint) #:use-module (crates-io))

(define-public crate-fprint-0.1.0 (c (n "fprint") (v "0.1.0") (h "00n849kay29y88hpna5wzz507scahnbm10xpair4plk6rbc6b0sr")))

(define-public crate-fprint-0.1.1 (c (n "fprint") (v "0.1.1") (h "111lhjhx4z6dpzirmlfrhfchgmc1w32lq81i0x1qpgkbk4z4dvkg")))

