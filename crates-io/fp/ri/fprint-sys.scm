(define-module (crates-io fp ri fprint-sys) #:use-module (crates-io))

(define-public crate-fprint-sys-0.1.0 (c (n "fprint-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1gd3imfk6w5l39s7dm9sg6dz4hj74ivpswvl60mpr8v2306s8nc4") (l "fprint")))

(define-public crate-fprint-sys-0.1.1 (c (n "fprint-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1didx2ani6rsm6vnp9g6jspdzjsqir7g40nl01r1qvjm8dcw1h5g") (l "fprint")))

(define-public crate-fprint-sys-0.1.2 (c (n "fprint-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "127wakcj13p5zzf9fcq1dg1y63iips04ag1gsprmw7gfh1nnhj0r") (l "fprint")))

(define-public crate-fprint-sys-0.1.3 (c (n "fprint-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0fcdllv84aqa3y7axk4fl7aasyhqsbpfyd1kcys6x3h7y8ijjp6a") (l "fprint")))

