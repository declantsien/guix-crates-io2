(define-module (crates-io fp _t fp_tui) #:use-module (crates-io))

(define-public crate-fp_tui-0.1.0 (c (n "fp_tui") (v "0.1.0") (h "0rk0nzwl9hdmiw9ypl287ks4syhvz9l17gb5p2y93wlw5rnvqy00")))

(define-public crate-fp_tui-0.2.0 (c (n "fp_tui") (v "0.2.0") (h "1jpiffxh7i3pia47d9smazh1j509l2zff3ry41gdmpvkww0j6jrm")))

(define-public crate-fp_tui-0.2.1 (c (n "fp_tui") (v "0.2.1") (h "05rh49ykp5jg1rs3gakr0w8mr7f2vgr40v0ng7sic0p5ygpqi3k7")))

(define-public crate-fp_tui-0.2.2 (c (n "fp_tui") (v "0.2.2") (h "0yk7gv623rpvz5sxydbf9cimd9q7s08fpnh5m22v6aym6arh2j2w")))

