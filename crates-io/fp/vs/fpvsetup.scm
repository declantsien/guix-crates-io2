(define-module (crates-io fp vs fpvsetup) #:use-module (crates-io))

(define-public crate-fpvsetup-0.1.0 (c (n "fpvsetup") (v "0.1.0") (d (list (d (n "edid") (r "^0.1") (o #t) (d #t) (t "cfg(windows)") (k 0) (p "edid-rs")) (d (n "fltk") (r "^0.16") (f (quote ("fltk-bundled"))) (o #t) (d #t) (k 0)) (d (n "native-dialog") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "uom") (r "^0.31") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("setupapi" "handleapi" "winreg"))) (o #t) (d #t) (t "cfg(windows)") (k 0)) (d (n "winres") (r "^0.1") (o #t) (d #t) (t "cfg(windows)") (k 1)))) (h "0spizqgn87swhcqqwbqn6xjbiay99jwig7syk7fpibzk9dadkni8") (f (quote (("gui" "fltk" "native-dialog" "winapi" "edid" "winres") ("default" "gui"))))))

