(define-module (crates-io fp gc fpgcli) #:use-module (crates-io))

(define-public crate-fpgcli-0.0.1 (c (n "fpgcli") (v "0.0.1") (d (list (d (n "arboard") (r "^3.2.1") (d #t) (k 0)) (d (n "arboard") (r "^3.2.1") (f (quote ("wayland-data-control" "wl-clipboard-rs"))) (d #t) (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"android\", target_os = \"emscripten\"))))") (k 0)) (d (n "flowerypassgen") (r "^0.0.1") (d #t) (k 0)))) (h "1xfn7a85759psdiias4nhirmz70jnhwgll77033yn4qad83yjqck")))

