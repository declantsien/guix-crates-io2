(define-module (crates-io fp s- fps-camera) #:use-module (crates-io))

(define-public crate-fps-camera-0.1.0 (c (n "fps-camera") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3") (d #t) (k 0)) (d (n "vecmath") (r "^0.3") (d #t) (k 0)))) (h "0cv8zz71z9l9sd3lnhinnssw5si97ldpzbywqk2gvkw5i6z9pp64")))

(define-public crate-fps-camera-0.1.1 (c (n "fps-camera") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3") (d #t) (k 0)) (d (n "vecmath") (r "^0.3") (d #t) (k 0)))) (h "1cgzrq4vjrngrxm25gr6lqh6j5fvk2gkbllm089b2wcm9rc19lv4")))

(define-public crate-fps-camera-0.1.2 (c (n "fps-camera") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "piston3d-cam") (r "^0.3") (d #t) (k 0)) (d (n "vecmath") (r "^0.3") (d #t) (k 0)))) (h "1cz9n5ir1sm10sp9hm6pp8kx7c86845fpyp161cn285042haviv0")))

