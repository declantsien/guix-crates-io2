(define-module (crates-io fp s- fps-controller) #:use-module (crates-io))

(define-public crate-fps-controller-0.1.0 (c (n "fps-controller") (v "0.1.0") (d (list (d (n "piston_window") (r "^0.86.0") (d #t) (k 2)))) (h "1rg854csll1lfc794l2dqzqa417aha6gf07wx11x33jgrpi4i19a")))

(define-public crate-fps-controller-0.1.1 (c (n "fps-controller") (v "0.1.1") (d (list (d (n "piston_window") (r "^0.89.0") (d #t) (k 2)))) (h "1j7sr56k168hfizqh4bix83msnyv7095gb2d66ifa63wnkvsac8r")))

