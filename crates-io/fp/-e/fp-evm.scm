(define-module (crates-io fp -e fp-evm) #:use-module (crates-io))

(define-public crate-fp-evm-0.1.0 (c (n "fp-evm") (v "0.1.0") (h "1pqi44ymjzdf75k0wiinq3kgbzf7dg4wm6j92mqa8s0hvq2x1mq1")))

(define-public crate-fp-evm-1.0.0 (c (n "fp-evm") (v "1.0.0") (d (list (d (n "codec") (r "^2.0.0") (k 0) (p "parity-scale-codec")) (d (n "evm") (r "^0.25.0") (f (quote ("with-codec"))) (k 0)) (d (n "impl-trait-for-tuples") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "1qrv9bqmwscll4gny7mg7jpzimsx01j7j5gydnax4smdy4cjv1i9") (f (quote (("std" "sp-core/std" "sp-std/std" "serde" "codec/std" "evm/std" "evm/with-serde") ("default" "std"))))))

(define-public crate-fp-evm-2.0.0 (c (n "fp-evm") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.0") (k 0) (p "parity-scale-codec")) (d (n "evm") (r "^0.27.0") (f (quote ("with-codec"))) (k 0)) (d (n "impl-trait-for-tuples") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "09080yj955klnfz3c60y8wnznmjckqk7lfnya2vdf86i99f05wbm") (f (quote (("std" "sp-core/std" "sp-std/std" "serde" "codec/std" "evm/std" "evm/with-serde") ("default" "std"))))))

