(define-module (crates-io fp zi fpzip-sys) #:use-module (crates-io))

(define-public crate-fpzip-sys-0.1.0 (c (n "fpzip-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "12hzbpdq8k0rg0xy4bqpcasm3qh96a7md8agks2j7jw95sw61hdk")))

(define-public crate-fpzip-sys-0.1.1 (c (n "fpzip-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "09is2cicr8r9jmd1s1zk5sidhqjlarqdb21j4x8ykr8z52jj687z")))

(define-public crate-fpzip-sys-0.1.2 (c (n "fpzip-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "16rkzpz8bl53xi1fvzz3kz0b58zgcqwrv8p0nj2icr6ib4vy2wny")))

(define-public crate-fpzip-sys-0.1.3 (c (n "fpzip-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1014crq6g77vvf06bs9bq82aj1qqh30z3y4vb36b96r26nhz6xf5")))

(define-public crate-fpzip-sys-0.1.4 (c (n "fpzip-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1226p6k7ysihm3wi8lypy5q1115dq9xb4ip6bqch3npy8gxqbqzb")))

(define-public crate-fpzip-sys-0.1.5 (c (n "fpzip-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0p2d34zcy8zg5sggz30g7l5yvh49liagml3sq9pq8d3aappzdgi4")))

(define-public crate-fpzip-sys-0.1.6 (c (n "fpzip-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "08vg0zidmbrxsjj6a11glzlpg05ljqvm1z8wgn08by146nr6719f")))

(define-public crate-fpzip-sys-0.1.7 (c (n "fpzip-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "016xh3r7fy9wqv5h1yqszqjf7gcq32i637vm97jpvxznqlk41qn5")))

(define-public crate-fpzip-sys-0.1.8 (c (n "fpzip-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "19f0xqbxxkpz07xz91siapzi65m8hif07vbzvc2dfkiswydhmnsp")))

