(define-module (crates-io fp -s fp-storage) #:use-module (crates-io))

(define-public crate-fp-storage-1.0.0 (c (n "fp-storage") (v "1.0.0") (d (list (d (n "codec") (r "^2.0.0") (k 0) (p "parity-scale-codec")) (d (n "ethereum") (r "^0.7.1") (f (quote ("with-codec"))) (k 0)) (d (n "ethereum-types") (r "^0.11") (k 0)) (d (n "fp-evm") (r "^1.0.0") (k 0)) (d (n "sp-api") (r "^3.0.0") (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-runtime") (r "^3.0.0") (k 0)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "1kkyfnnhxdhrvnpjf77dpz1y20nlj90pp9zgzjy5ykgljpbf8pyn") (f (quote (("std" "sp-core/std" "sp-api/std" "fp-evm/std" "ethereum/std" "ethereum-types/std" "codec/std" "sp-runtime/std" "sp-std/std") ("default" "std"))))))

(define-public crate-fp-storage-1.0.1 (c (n "fp-storage") (v "1.0.1") (h "1s9dibrnknwrch3l2qid2ggmkffdfabjas7mws9b0f3hz557sda1") (f (quote (("std") ("default" "std"))))))

(define-public crate-fp-storage-2.0.0 (c (n "fp-storage") (v "2.0.0") (h "0374j7z6mnrdkm2kp8pm1llcrd4b2bkhz5nrp6clam5qpi3p76cv") (f (quote (("std") ("default" "std"))))))

