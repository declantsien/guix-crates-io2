(define-module (crates-io fp s_ fps_counter) #:use-module (crates-io))

(define-public crate-fps_counter-0.0.0 (c (n "fps_counter") (v "0.0.0") (d (list (d (n "time") (r "^0.1.1") (d #t) (k 0)))) (h "10sn00z6z547sjrvk72cwwm9bslxkzbn633ilwgrl9zhmndmbpzv")))

(define-public crate-fps_counter-0.0.1 (c (n "fps_counter") (v "0.0.1") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "027kz4ih9lj2ljcl60cbgrr9r938mcp5f22f52wkr8jqbm9qank7")))

(define-public crate-fps_counter-0.0.2 (c (n "fps_counter") (v "0.0.2") (d (list (d (n "clock_ticks") (r "^0.0.5") (d #t) (k 0)))) (h "1bd4mi71y3w4bzry77psa6rzaih26p6za4g8j9hsfkc0bkg985q1")))

(define-public crate-fps_counter-0.1.0 (c (n "fps_counter") (v "0.1.0") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)))) (h "1bxgcr6i1b9pk0v10jbrkm6bawr7cmi88nrg8rrklbd4f7xk0jaf")))

(define-public crate-fps_counter-0.1.1 (c (n "fps_counter") (v "0.1.1") (d (list (d (n "clock_ticks") (r "^0.0.6") (d #t) (k 0)))) (h "01y3xzwghh6f0v1s68r35yiwcxa0qvn1qyi2jx0z4ilbhxhpjffz")))

(define-public crate-fps_counter-0.2.0 (c (n "fps_counter") (v "0.2.0") (d (list (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1sijsg2f4bxyhga0gjdil0l3d230lk8dfb22bfyf60gvsh5zcxsn")))

(define-public crate-fps_counter-1.0.0 (c (n "fps_counter") (v "1.0.0") (h "19ina9gx8nbla2jw5qdbz09gydj9ikbxa9mg8mw2kwy9n7f82wg8")))

(define-public crate-fps_counter-2.0.0 (c (n "fps_counter") (v "2.0.0") (h "0sx3y6mjsxn3r4sb2cacwzc96kp9n607z4k2nl1disafa7zsgars")))

(define-public crate-fps_counter-3.0.0 (c (n "fps_counter") (v "3.0.0") (d (list (d (n "instant") (r "^0.1.12") (d #t) (k 0)))) (h "1mkdvpd2r1gn6phpkcj4w670va0w7g514g7ff3rmkf541gcs88zz") (f (quote (("wasm-bindgen" "instant/wasm-bindgen") ("stdweb" "instant/stdweb"))))))

