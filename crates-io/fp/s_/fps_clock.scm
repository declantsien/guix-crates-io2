(define-module (crates-io fp s_ fps_clock) #:use-module (crates-io))

(define-public crate-fps_clock-1.0.0 (c (n "fps_clock") (v "1.0.0") (h "1fn4p2x7vkmkm26xidgknzwrsz5p3wdy0z53h520yrn37s0ww9y9")))

(define-public crate-fps_clock-2.0.0 (c (n "fps_clock") (v "2.0.0") (h "1w12byi14f8mhm1rfv8agrnns1za51i3cf47mgv1ziik8dj0fbyn")))

