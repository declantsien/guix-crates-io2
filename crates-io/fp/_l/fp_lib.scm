(define-module (crates-io fp _l fp_lib) #:use-module (crates-io))

(define-public crate-fp_lib-0.1.0 (c (n "fp_lib") (v "0.1.0") (h "0lm3f40q7lw6y8vc9c6g8ni23h0ac4nzlf67y90x8mr39yi0kfww")))

(define-public crate-fp_lib-0.2.0 (c (n "fp_lib") (v "0.2.0") (h "0ki8g4bx8b8gkgkjf82sjk43qxqcna14iwq9iwsacwq5b6203w7v")))

(define-public crate-fp_lib-0.2.1 (c (n "fp_lib") (v "0.2.1") (h "0zrz8i8bhxrzalraig3z275f0yrig9j7cdib181da3vg2i94jhrg")))

(define-public crate-fp_lib-0.3.0 (c (n "fp_lib") (v "0.3.0") (h "051ir3wgwcx5z9h3varhy1wgyzvf2bddvvpnvmg0jqzw91jnvqs1")))

(define-public crate-fp_lib-0.4.0 (c (n "fp_lib") (v "0.4.0") (h "1fsq9phz3ipnx73yrzag1qj7c56s6g90szkvj34cig167p2rpdrj")))

(define-public crate-fp_lib-0.5.0 (c (n "fp_lib") (v "0.5.0") (h "0jgfypx0zsqn3aq379gnlls30xr31pismcxpawvvxa9lq6skzprh")))

(define-public crate-fp_lib-0.5.1 (c (n "fp_lib") (v "0.5.1") (h "08ps9lsmf4lc8352w6p7hdqa0b18q272wz5y05dgmh65mb1341yi")))

(define-public crate-fp_lib-0.5.2 (c (n "fp_lib") (v "0.5.2") (h "0xqszr3gmljhhdzk7cfzxsdcb9qb9pi7d0xwqydakk41jzqwpczn")))

(define-public crate-fp_lib-0.5.3 (c (n "fp_lib") (v "0.5.3") (h "1m81agcc8ck16f7d0cnia5zca4q97mnxjgdg3v026qaz6brz8r9h")))

