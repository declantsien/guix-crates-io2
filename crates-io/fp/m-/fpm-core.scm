(define-module (crates-io fp m- fpm-core) #:use-module (crates-io))

(define-public crate-fpm-core-0.0.1 (c (n "fpm-core") (v "0.0.1") (d (list (d (n "flatpak-rs") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "1fz184dbbs33q452qmg1xcw14rzncxyqsay3wvchb65rwpw2qa28")))

(define-public crate-fpm-core-0.1.0 (c (n "fpm-core") (v "0.1.0") (d (list (d (n "flatpak-rs") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "1qkgd1c6zc3w9cy6fx76xgzap6z4w133g4gwdwssr5lq3lkj6lb5")))

(define-public crate-fpm-core-0.1.1 (c (n "fpm-core") (v "0.1.1") (d (list (d (n "flatpak-rs") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "0bm8akra0ys21yxzajjsvdaxbrsr1xigjkz5w0pzg1165wi8zkq2")))

(define-public crate-fpm-core-0.2.0 (c (n "fpm-core") (v "0.2.0") (d (list (d (n "flatpak-rs") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "1vqrfhqw0ixaifhdzbqchlsmbxf1jxq88l4jqrgca0cyg0ndwm2i")))

(define-public crate-fpm-core-0.2.1 (c (n "fpm-core") (v "0.2.1") (d (list (d (n "flatpak-rs") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "16zy7pvyj2ypzm3h8igzs6gd67wdcv31sg7rm93nnn8dlf5iq066")))

(define-public crate-fpm-core-0.2.2 (c (n "fpm-core") (v "0.2.2") (d (list (d (n "flatpak-rs") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "11srhdznbbshwf8pwz96nknwqi26rjhjvyx2897yiwd3b1ws7a9n")))

(define-public crate-fpm-core-0.2.3 (c (n "fpm-core") (v "0.2.3") (d (list (d (n "flatpak-rs") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "1cai8kpn35bj754ylja9mmyy96l4w97ll752k4shcli7mm66hk7i")))

(define-public crate-fpm-core-0.3.0 (c (n "fpm-core") (v "0.3.0") (d (list (d (n "flatpak-rs") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "1d66drb20vqfhz4wbizhn03dqz3h1zdcrsxhkvflb4vddlg2bhcw")))

(define-public crate-fpm-core-0.4.0 (c (n "fpm-core") (v "0.4.0") (d (list (d (n "flatpak-rs") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "1dz9ja15qs853218zwp4w74rld0sdak5wbw7x34rcrxaim8x6rmc")))

(define-public crate-fpm-core-0.5.0 (c (n "fpm-core") (v "0.5.0") (d (list (d (n "flatpak-rs") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "1aarnq5gb011ssrlqap09yqhamx5sh4dpl76rng4yslcqdpkr57g")))

(define-public crate-fpm-core-0.6.0 (c (n "fpm-core") (v "0.6.0") (d (list (d (n "flatpak-rs") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "15j9jksmaj9x1qacscyip6l8jj70npvm8bw2hwk4bm33vf3dd8xy")))

(define-public crate-fpm-core-0.7.0 (c (n "fpm-core") (v "0.7.0") (d (list (d (n "flatpak-rs") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "0a2761x2zzddvx70jklqdwjk0pw00hhb7ybpc10ygnaav6x5m26b")))

(define-public crate-fpm-core-0.8.0 (c (n "fpm-core") (v "0.8.0") (d (list (d (n "flatpak-rs") (r "^0.16") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "1w16yzjbk4lljk1jnl3rfl4p0x8cdi7bpgsydwz30s6443gir75p")))

(define-public crate-fpm-core-0.9.0 (c (n "fpm-core") (v "0.9.0") (d (list (d (n "flatpak-rs") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "1bhxphhp4g73aqkgi27j7f6l6hfyaxxmjmxjk3gchhc586k54z30")))

(define-public crate-fpm-core-0.10.0 (c (n "fpm-core") (v "0.10.0") (d (list (d (n "flatpak-rs") (r "^0.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0") (d #t) (k 0)))) (h "05w85x822r3147kvswjnyxqz53zkq22lnq78dq9qhj4iy3wqn5gw")))

