(define-module (crates-io fp m- fpm-cli) #:use-module (crates-io))

(define-public crate-fpm-cli-0.6.1 (c (n "fpm-cli") (v "0.6.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "fpm-lib") (r "^0.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0n3gzfmjc2kjmg1w1zzjwlbcibl0qsl0zy6ld3wllvq7q48zf4cv") (y #t)))

