(define-module (crates-io fp de fpdec-core) #:use-module (crates-io))

(define-public crate-fpdec-core-0.1.0 (c (n "fpdec-core") (v "0.1.0") (h "0zvn1pv9rdzvzhxj33b9c9r5sci45raqh7xgx6117mwci3nm3l5j") (y #t)))

(define-public crate-fpdec-core-0.1.1 (c (n "fpdec-core") (v "0.1.1") (h "1d3m5jbm8bilcxab2fzb1lvwc8lzm3y3mwzidw5i171xavwzzf8p") (y #t)))

(define-public crate-fpdec-core-0.2.0 (c (n "fpdec-core") (v "0.2.0") (h "1zk4dp7rvnn8h1ypig42fcrbkw5gi7laqsc9d1wsy7b0z546jrd4") (y #t)))

(define-public crate-fpdec-core-0.3.0 (c (n "fpdec-core") (v "0.3.0") (h "0p9jpa3gzbl0d5aqdhj5c3vvc9f74wba9j3i67b7s0aidl9g254l")))

(define-public crate-fpdec-core-0.4.1 (c (n "fpdec-core") (v "0.4.1") (h "0f0pphb99vcm9aahc44bxs2rg4k7xpz34ppbr6a48lw4xmdrf7ks") (f (quote (("std") ("default" "std"))))))

(define-public crate-fpdec-core-0.5.0 (c (n "fpdec-core") (v "0.5.0") (h "0r1ypzy9n7adfilgvq49l5sn6as7vijh986hsxlg5zmaqz5vkbxd") (f (quote (("std") ("default" "std"))))))

(define-public crate-fpdec-core-0.5.3 (c (n "fpdec-core") (v "0.5.3") (h "02p3z897p5hps6p2sy7lh61qx58s39gjmmwzyvix744ym22xis5f") (f (quote (("std") ("default" "std"))))))

(define-public crate-fpdec-core-0.5.4 (c (n "fpdec-core") (v "0.5.4") (h "09dshqkwbndm18qymgnlzf88sd2zgw0gwrg58i5rj7wi8n3g21r3") (f (quote (("std") ("default" "std"))))))

(define-public crate-fpdec-core-0.6.0 (c (n "fpdec-core") (v "0.6.0") (h "1qsw3llvkl38nzckbnwh85d7dc7ylddkhwh0ppzz59mizqlyp2i3") (f (quote (("std") ("default" "std"))))))

(define-public crate-fpdec-core-0.6.4 (c (n "fpdec-core") (v "0.6.4") (h "0f5xrq3z4ww9cq8w5bz6y80w9960gw5gwg7fhf75x7fgdba9lgiz") (f (quote (("std") ("default" "std"))))))

(define-public crate-fpdec-core-0.8.0 (c (n "fpdec-core") (v "0.8.0") (h "17ycpv8smb0zhcy26bh0r1wfavz6k5k9agzqsphwdnakyw51mj34") (f (quote (("std") ("default" "std"))))))

