(define-module (crates-io fp de fpdec-macros) #:use-module (crates-io))

(define-public crate-fpdec-macros-0.1.0 (c (n "fpdec-macros") (v "0.1.0") (d (list (d (n "fpdec-core") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1v76iwkbpgrf1raqb1sjjfd5nhfs905vl91a4xq55a67aqymf7y4") (y #t)))

(define-public crate-fpdec-macros-0.1.1 (c (n "fpdec-macros") (v "0.1.1") (d (list (d (n "fpdec-core") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "10g3l74zb55h8zls4zigfiazv2m2mkzyk6n787zhq1by8vm4da0q") (y #t)))

(define-public crate-fpdec-macros-0.2.0 (c (n "fpdec-macros") (v "0.2.0") (d (list (d (n "fpdec-core") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0qljlaxbxfmzgxz4ps6ymlfy6z34mmdy58nzvd0aw93p806p2hnz") (y #t)))

(define-public crate-fpdec-macros-0.3.0 (c (n "fpdec-macros") (v "0.3.0") (d (list (d (n "fpdec-core") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1ijzcd65fqhqj76xnshdsl0z6dpi84zwk5pi9pnbyn83c9skhxr6")))

(define-public crate-fpdec-macros-0.4.1 (c (n "fpdec-macros") (v "0.4.1") (d (list (d (n "fpdec-core") (r "^0.4.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "06aqsgyrv5byg0ddqjm6577plvxcls0njyhbz9fw90f29n1pvvfn")))

(define-public crate-fpdec-macros-0.5.0 (c (n "fpdec-macros") (v "0.5.0") (d (list (d (n "fpdec-core") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1vbgx7lgdyrmwnnk5sac3pd7j4j3aaf11vrba6lhfndnxxm6v44g")))

(define-public crate-fpdec-macros-0.5.3 (c (n "fpdec-macros") (v "0.5.3") (d (list (d (n "fpdec-core") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0890br7szhlp459ybildwksj4hwr341mxql0i2xfma40rxbml1cr")))

(define-public crate-fpdec-macros-0.5.4 (c (n "fpdec-macros") (v "0.5.4") (d (list (d (n "fpdec-core") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0q9pqjyqbpdyqvr24mndz05qhvwggd76mk4ms9gk5pbkjc982wnm")))

(define-public crate-fpdec-macros-0.6.0 (c (n "fpdec-macros") (v "0.6.0") (d (list (d (n "fpdec-core") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1p279mz2ysqmzkb1r2zaii6c1wk6jx6rqrsvsf5z0kq12jwqmq0w")))

(define-public crate-fpdec-macros-0.8.0 (c (n "fpdec-macros") (v "0.8.0") (d (list (d (n "fpdec-core") (r "^0.8.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0328v2abkni9ypjixkpwhp0r18rdj98k2x8xjmm9d2knddd2vka5")))

