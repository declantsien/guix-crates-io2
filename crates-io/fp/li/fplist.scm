(define-module (crates-io fp li fplist) #:use-module (crates-io))

(define-public crate-fplist-0.1.0 (c (n "fplist") (v "0.1.0") (h "16a0db7wx8xwm0j01xv6cvarhiakxb996fn6g2b570z8saskslmr") (f (quote (("multithreaded"))))))

(define-public crate-fplist-0.1.1 (c (n "fplist") (v "0.1.1") (h "12byy7vqh5m49vnfgs9fzw91fszxbqzhz7dr39qf4hfjapqvq9cm") (f (quote (("multithreaded"))))))

(define-public crate-fplist-0.1.2 (c (n "fplist") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ma6fa4r2mrjsnacrgbarn79z623qll0qs9jd6bsjfr35jll64s6") (f (quote (("serde_impls" "serde") ("multithreaded"))))))

(define-public crate-fplist-0.1.3 (c (n "fplist") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0k9dv0a0ikv38lgv783z68zq95bmfc877hzdavnpyhbfbgidif8j") (f (quote (("serde_impls" "serde") ("multithreaded"))))))

(define-public crate-fplist-0.1.4 (c (n "fplist") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0i7i8dwkx774bxfnlf6zavrlhwc5sk48x1rdllpapph6la086p86") (f (quote (("serde_impls" "serde") ("multithreaded"))))))

(define-public crate-fplist-0.2.0 (c (n "fplist") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13r8rzzaklclyfn2sjbgfylkn2hx68ficcajnps6ma5bi59cvwx4") (f (quote (("serde_impls" "serde") ("multithreaded"))))))

(define-public crate-fplist-0.2.1 (c (n "fplist") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13xa6sc3ik87wslnnx65pfwsj6d8g4xbjxxrsmnp39smrqvb9b8w") (f (quote (("serde_impls" "serde") ("multithreaded"))))))

