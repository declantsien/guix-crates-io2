(define-module (crates-io fp ro fprompt) #:use-module (crates-io))

(define-public crate-fprompt-0.0.1 (c (n "fprompt") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "whoami") (r "^0.9.0") (d #t) (k 0)))) (h "1gf42lr8bsj7fwv22ps8gwp2imimfn7mhg3nhrjm4x2rnxxvcahy") (y #t)))

(define-public crate-fprompt-0.0.2 (c (n "fprompt") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "whoami") (r "^0.9.0") (d #t) (k 0)))) (h "0zll07r7qxqnzv9ppqngayh9v5g4hw4rjjkxcynldvp2vgc9769q") (y #t)))

(define-public crate-fprompt-0.0.3 (c (n "fprompt") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "whoami") (r "^0.9.0") (d #t) (k 0)))) (h "17k8n4jy48y71fad4h0css0sgwpmskqh69f5vj5sdwaxga6ffvi2") (y #t)))

(define-public crate-fprompt-0.0.4 (c (n "fprompt") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "whoami") (r "^0.9.0") (d #t) (k 0)))) (h "08axl7fbxigvc33g6vnbxj1qmfs4w3s5qap2ns98i3is46ypfyra") (y #t)))

(define-public crate-fprompt-0.0.5 (c (n "fprompt") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "whoami") (r "^0.9.0") (d #t) (k 0)))) (h "06cxxfl0nlfq0p85x9zrnf1k9kka31ra923cgj2liy30qfva01lv") (y #t)))

(define-public crate-fprompt-0.0.6 (c (n "fprompt") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "whoami") (r "^0.9.0") (d #t) (k 0)))) (h "1abi7x9dyv0g4z1bx94xw8h1wrs4jiq8ssrvwzrgp7zs09qzjjjw") (y #t)))

(define-public crate-fprompt-0.0.7 (c (n "fprompt") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "whoami") (r "^0.9.0") (d #t) (k 0)))) (h "0nqj75qba90mlrcmk8kdn88zm058wpaq0c65gs1xvmrsl90lj0xr") (y #t)))

(define-public crate-fprompt-0.0.8 (c (n "fprompt") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)) (d (n "whoami") (r "^0.9.0") (d #t) (k 0)))) (h "1698bvs9lgjf40h602lv3s15jrw1lvnzwvl18j1b2d0md77ifqq5") (y #t)))

(define-public crate-fprompt-0.0.10 (c (n "fprompt") (v "0.0.10") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0qnhglgib28y52b1n9zlf3axa95frhrxzxnswkkb8g604zbps975") (y #t)))

(define-public crate-fprompt-1.0.0 (c (n "fprompt") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0ip2k5qhfb2kzrb8wabliqf5zbvjd8jdcfrjcl6f8apwyfzfi05p") (y #t)))

(define-public crate-fprompt-1.1.1 (c (n "fprompt") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1hb7zkqxwvmm6v1mrn6xrnfazbhhhr7z5p0sfvfwrcg0b054ha4n") (y #t)))

(define-public crate-fprompt-1.1.2 (c (n "fprompt") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1kk785k8f2acxk4gihka6fhwdslgxw2h2whd52z7c7m4m278f1r5") (y #t)))

(define-public crate-fprompt-1.1.3 (c (n "fprompt") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0x8rdk2g4yzaaxknhs3wv7q9mnsk6wh9sx887lhn2zzxbydkj58v") (y #t)))

