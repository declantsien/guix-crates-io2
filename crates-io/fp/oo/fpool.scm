(define-module (crates-io fp oo fpool) #:use-module (crates-io))

(define-public crate-fpool-0.1.0 (c (n "fpool") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1z8pk10nv8pjq5m5dw4w157fn3axf6i34l3vbxaqphfzrwbks8cb")))

(define-public crate-fpool-0.2.0 (c (n "fpool") (v "0.2.0") (h "11s23kzvfvp6p5wd4y8v9f4x6iq3i5fhiz4zavhzdva5alicjpnw")))

(define-public crate-fpool-0.3.0 (c (n "fpool") (v "0.3.0") (h "0xzk1qq4214vlq8dv281ab3vhmsrvlb1ryg5k9l7rxy3657igcn5")))

(define-public crate-fpool-0.3.1 (c (n "fpool") (v "0.3.1") (h "1r9n2ahaqj40nmclc2bki9gqxy70zagaqp2wgqa4fwf666jhjnrl")))

(define-public crate-fpool-0.4.0 (c (n "fpool") (v "0.4.0") (h "12wgapdbmfkjd4r39ncw5z3qs4ifwg17vc6q8vl1yxxpl6yxma29")))

(define-public crate-fpool-0.5.0 (c (n "fpool") (v "0.5.0") (h "1w37zf2jb5qs6assvl8rwb1d2xlqq9r1qrplvqd7f0zzvx41jnl9")))

