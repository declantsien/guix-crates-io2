(define-module (crates-io fp ls fpls_lib) #:use-module (crates-io))

(define-public crate-fpls_lib-0.1.0 (c (n "fpls_lib") (v "0.1.0") (h "173cm0ygv55ax18bqa94wxwk7z24gdpw55m2q0ym5569jh8ayk3f")))

(define-public crate-fpls_lib-0.1.1 (c (n "fpls_lib") (v "0.1.1") (h "0z864sirwdpy39s62yz2bj3i500hspzz8xd91x42ylaz2n8axxbz")))

(define-public crate-fpls_lib-0.1.2 (c (n "fpls_lib") (v "0.1.2") (h "0gkhbyny8g3f4aiqx26mz22yijxm3w5fiwa176jkzcvf70590mvq")))

