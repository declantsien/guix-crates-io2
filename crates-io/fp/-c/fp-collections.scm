(define-module (crates-io fp -c fp-collections) #:use-module (crates-io))

(define-public crate-fp-collections-0.0.0 (c (n "fp-collections") (v "0.0.0") (h "1f3amiqhr4q6hcx9l6pww324a1pkz2w1v2krkqbjgdfs7q1ypvl1")))

(define-public crate-fp-collections-0.0.1 (c (n "fp-collections") (v "0.0.1") (h "0q58ai8k4n6imjak0y4a9sfbmj5ysv1hgs7kv71hzs3qfyhd6mb5")))

(define-public crate-fp-collections-0.0.2 (c (n "fp-collections") (v "0.0.2") (h "05llr6qk77v86nfiwsk4bjzwpncdyfdrwm4hm430kks9fzi2kilc")))

