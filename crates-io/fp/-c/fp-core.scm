(define-module (crates-io fp -c fp-core) #:use-module (crates-io))

(define-public crate-fp-core-0.1.0 (c (n "fp-core") (v "0.1.0") (h "1x2pqgasyrjg26gkcs2cipm15dalbphrbz09z4g1hn6npp8rqdk5")))

(define-public crate-fp-core-0.1.1 (c (n "fp-core") (v "0.1.1") (h "11ibfp4hn3kcm9cj76jsx65f1rnvd62hs75i4fr2qic6n9fb77n7")))

(define-public crate-fp-core-0.1.2 (c (n "fp-core") (v "0.1.2") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0grynkhka3k0pakamwz5iy7xnc24vj9i3yl6cqhmnrj4mb4z5p8s")))

(define-public crate-fp-core-0.1.3 (c (n "fp-core") (v "0.1.3") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1qwj43nnwnwjal2jsyizyaijgc9b3rrdfrks6yp2ajanmnpxwnc4")))

(define-public crate-fp-core-0.1.4 (c (n "fp-core") (v "0.1.4") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0n4qj77xxmy9yvb1gqg8lmbrikq098v7ypkzj2184r12lfnks8wi")))

(define-public crate-fp-core-0.1.5 (c (n "fp-core") (v "0.1.5") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "13xrmdmmmd0y6wxb97qgwsjcvdk27w5v00jbcvv86zvc277a6ycv")))

(define-public crate-fp-core-0.1.6 (c (n "fp-core") (v "0.1.6") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0x44njlkwsrwa7bg3v0mpz4gg4a2fx6kjr8qrpx8wj2bnia95bs7")))

(define-public crate-fp-core-0.1.7 (c (n "fp-core") (v "0.1.7") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1jdky6mkrh6r2q985442vkrxq7zqwdihgwsg3lvy5jyk0459637i")))

(define-public crate-fp-core-0.1.8 (c (n "fp-core") (v "0.1.8") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1g1vni876dzb1jh98s719c28n5hxv7r4vjcmxpcf4gaq0ligdm0l")))

(define-public crate-fp-core-0.1.9 (c (n "fp-core") (v "0.1.9") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "02ydzgsw5rspsvhpy59ag5gbdawr9vd5i9rxz8yn0j3jdkmmz2ik")))

