(define-module (crates-io fp ng fpng-rs) #:use-module (crates-io))

(define-public crate-fpng-rs-1.0.6 (c (n "fpng-rs") (v "1.0.6") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0xl6yz04xg7mb48984zylpf7lqvlhbxip8w8gs06mshf4qb7j9hm") (f (quote (("internal-bindgen-on-build" "bindgen")))) (y #t)))

(define-public crate-fpng-rs-1.0.7 (c (n "fpng-rs") (v "1.0.7") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "07rwj8pyb3lpdzrba5c15n228d9ahl0pxvjrz81clayrfa5zbdfq") (f (quote (("internal-bindgen-on-build" "bindgen"))))))

(define-public crate-fpng-rs-1.0.8 (c (n "fpng-rs") (v "1.0.8") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vrxlywzbvwhlzb872qj08473w3agb09bjqwis3w119rkqgima8p") (f (quote (("internal-bindgen-on-build" "bindgen"))))))

(define-public crate-fpng-rs-1.0.9 (c (n "fpng-rs") (v "1.0.9") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1hvhcvhk0x9yja2f1b6cwjsr9hjv9ksa9aka1g3h1vklzv2fgp6z") (f (quote (("internal-bindgen-on-build" "bindgen"))))))

