(define-module (crates-io fp ip fpipe) #:use-module (crates-io))

(define-public crate-fpipe-0.1.0 (c (n "fpipe") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "test_dir") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.3.0") (f (quote ("io-std" "io-util" "process" "rt"))) (d #t) (k 0)))) (h "0xkn9hrlx0dl3x1i1k21p5s5pw30lnw3fqhc4kj2q7f1w84h29nh")))

(define-public crate-fpipe-0.1.1 (c (n "fpipe") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "test_dir") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.3.2") (f (quote ("io-std" "io-util" "process" "rt"))) (d #t) (k 0)))) (h "04r1yxk7wgblk33w8rz5j2kd7j1vnksm1xp7l4wclbylzpyb9q6l")))

(define-public crate-fpipe-0.1.2 (c (n "fpipe") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "clap") (r "^3.0.5") (f (quote ("std" "derive" "color"))) (k 0)) (d (n "predicates") (r "^2.1.0") (d #t) (k 2)) (d (n "test_dir") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("io-std" "io-util" "process" "rt"))) (k 0)))) (h "14v0jiia4p9ajd7a0fwj72ngdsgwhfry0pyr2pp5s50ppb2jrl5x")))

(define-public crate-fpipe-0.1.3 (c (n "fpipe") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "concolor-clap") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("io-std" "io-util" "process" "rt"))) (k 0)) (d (n "assert_cmd") (r "^2.0.10") (d #t) (k 2)) (d (n "predicates") (r "^3.0.1") (d #t) (k 2)) (d (n "test_dir") (r "^0.2.0") (d #t) (k 2)))) (h "1f6azw6l6zrb1wdcgind5hy7f4hbafpksvmc0bnbs9rsf6np41wr")))

