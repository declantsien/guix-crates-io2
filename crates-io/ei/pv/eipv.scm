(define-module (crates-io ei pv eipv) #:use-module (crates-io))

(define-public crate-eipv-0.0.1 (c (n "eipv") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0hnacy1dd23y7ik1nd0j68jmi7idnh6m5s8gkqdasfv9680n1pdf")))

(define-public crate-eipv-0.0.2 (c (n "eipv") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "04qrchng2yiyvzrl81h0b38j4blabgix8pvsi5238nvdm8nwncrh")))

(define-public crate-eipv-0.0.3 (c (n "eipv") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1pksvf826d8jdm4x2a65xj4qzf1mz2m4izcm1id20zma4hx7f034")))

(define-public crate-eipv-0.0.4 (c (n "eipv") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1i3p4jsc43zqjhs7wlc4jshxfjpgpfkjjp3yrymxp9bj6k31l5mq")))

(define-public crate-eipv-0.0.5 (c (n "eipv") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "16j9pg2yc9zyvmz6idndgg27nfjacimp94i65mx33hl5znamy0pa")))

(define-public crate-eipv-0.0.6 (c (n "eipv") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "02ya17limwsym5yc80qfcaj7cxc4fzds4v4wz0yfma35lgq8qvhb")))

(define-public crate-eipv-0.1.0 (c (n "eipv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1dayy1kxbjzhkxjyc55afvg4q8d4cmn78jk802sp9d6q47mcib88")))

(define-public crate-eipv-0.1.1 (c (n "eipv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0g426axs1r12zqrxsayqr1wb5idj982k14m9a7a60xqzaqavxx2s")))

(define-public crate-eipv-0.2.0 (c (n "eipv") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "1gdiw7ppy8wnprl71sx3cqa8r7fanmlv24b9zpzfqyscrfb34vin")))

(define-public crate-eipv-0.3.0 (c (n "eipv") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0d463rrgkq00wnvrrqxk6zaz20wr86i9hw99b52x0sgbn835k6mw")))

(define-public crate-eipv-0.4.0 (c (n "eipv") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "04yybb0cmj6sqskw1rjvn3pv301j4m4hqdxhfqpz49lzky60974j")))

