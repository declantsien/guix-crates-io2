(define-module (crates-io ei p7 eip7594) #:use-module (crates-io))

(define-public crate-eip7594-0.1.0 (c (n "eip7594") (v "0.1.0") (d (list (d (n "bls12_381") (r "^0.1.0") (d #t) (k 0) (p "crate_crypto_internal_peerdas_bls12_381")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "kzg_multi_open") (r "^0.1.0") (d #t) (k 0) (p "crate_crypto_kzg_multi_open_fk20")) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0yjig2limldc5kr7cd28x11dj3nbsmmq3lvnnwcfgldvyzimh4fs") (r "1.66")))

(define-public crate-eip7594-0.2.1 (c (n "eip7594") (v "0.2.1") (d (list (d (n "bls12_381") (r "^0.2.0") (d #t) (k 0) (p "crate_crypto_internal_peerdas_bls12_381")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "kzg_multi_open") (r "^0.2.0") (d #t) (k 0) (p "crate_crypto_kzg_multi_open_fk20")) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "12ax811labkkzd2fsm94352a7m4701qigidqrzpwl0k40k4b7iii") (r "1.66")))

(define-public crate-eip7594-0.2.5 (c (n "eip7594") (v "0.2.5") (d (list (d (n "bls12_381") (r "^0.2.5") (d #t) (k 0) (p "crate_crypto_internal_peerdas_bls12_381")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "kzg_multi_open") (r "^0.2.5") (d #t) (k 0) (p "crate_crypto_kzg_multi_open_fk20")) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0cf3h7hvnqygfb14w0lsibfz65ycv91dnb2ij6l6qimkp657yg4r") (r "1.66")))

(define-public crate-eip7594-0.2.6 (c (n "eip7594") (v "0.2.6") (d (list (d (n "bls12_381") (r "^0.2.6") (d #t) (k 0) (p "crate_crypto_internal_peerdas_bls12_381")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "kzg_multi_open") (r "^0.2.6") (d #t) (k 0) (p "crate_crypto_kzg_multi_open_fk20")) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "02yk110csghy0rgm9yxap3j2jq2ylxzv3kzj21xdlpjc2g1dl3p8") (r "1.66")))

(define-public crate-eip7594-0.3.0 (c (n "eip7594") (v "0.3.0") (d (list (d (n "bls12_381") (r "^0.3.0") (d #t) (k 0) (p "crate_crypto_internal_peerdas_bls12_381")) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "kzg_multi_open") (r "^0.3.0") (d #t) (k 0) (p "crate_crypto_kzg_multi_open_fk20")) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1m8v7crhr7zbvd1gb3w49msng3mqd32hxikk4ia6apw0cdjl21d5") (r "1.66")))

