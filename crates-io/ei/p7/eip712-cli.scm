(define-module (crates-io ei p7 eip712-cli) #:use-module (crates-io))

(define-public crate-eip712-cli-0.1.0 (c (n "eip712-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "eip712") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "05g6k2gsdpi3gjcaf5ii0qhn7b7hkwb1ivg3c37r3bibbnw70yns") (f (quote (("backtraces" "snafu/backtraces" "eip712/backtraces"))))))

