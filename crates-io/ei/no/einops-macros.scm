(define-module (crates-io ei no einops-macros) #:use-module (crates-io))

(define-public crate-einops-macros-0.1.0-alpha.1 (c (n "einops-macros") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b0zxg367n2s8xlmznl5kqx6s3x2ff95aghw32kiqwsd82aznlx4")))

(define-public crate-einops-macros-0.1.0-alpha.2 (c (n "einops-macros") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bymm0br2raf2k36drzizzcvb3ngd8m0szgg0m7fivaw04ws3c17")))

