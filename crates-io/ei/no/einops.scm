(define-module (crates-io ei no einops) #:use-module (crates-io))

(define-public crate-einops-0.1.0 (c (n "einops") (v "0.1.0") (d (list (d (n "tch") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12qj7dixfqg2px55dlc58yfwrzx236agiw3i69va0k0nq6hskbbq") (f (quote (("tch-bindings" "tch"))))))

(define-public crate-einops-0.2.0 (c (n "einops") (v "0.2.0") (d (list (d (n "tch") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fjckp86879w5qgrs8wb0ilvczg7k17ak19yjihfjcva55p1p5vr") (f (quote (("tch-bindings" "tch") ("default" "tch-bindings"))))))

(define-public crate-einops-0.2.1 (c (n "einops") (v "0.2.1") (d (list (d (n "tch") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rqcacld5gxy2dzplwcpwzg7wyzjq1q72a2qafka60xanjjzrkzy") (f (quote (("tch-bindings" "tch") ("default" "tch-bindings"))))))

(define-public crate-einops-0.3.0-alpha.1 (c (n "einops") (v "0.3.0-alpha.1") (d (list (d (n "einops-macros") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "tch") (r "^0.6") (o #t) (d #t) (k 0)))) (h "03gak5m1fy1rmkv45cjgrm5wmwrn844bzb7k1dvczh8qb0iilgih") (f (quote (("tch-bindings" "tch") ("default" "tch-bindings"))))))

(define-public crate-einops-0.3.0-alpha.2 (c (n "einops") (v "0.3.0-alpha.2") (d (list (d (n "einops-macros") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "tch") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0mqqq802vph5anqkqhggl6gycbq8qjgkb54pg0rkhhks7wjqr60y") (f (quote (("tch-bindings" "tch") ("default" "tch-bindings"))))))

