(define-module (crates-io ei p5 eip55) #:use-module (crates-io))

(define-public crate-eip55-0.1.0 (c (n "eip55") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "1ifhdjv6pxwx6ycp529bays3bx64yx5kpnhcgyk1ccs8al6qzxgm")))

(define-public crate-eip55-0.1.1 (c (n "eip55") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0bzkg2i3gds4pm3jbaxkbqwz4a833mx2mv9mbnjfdr6p4fr95ylj")))

(define-public crate-eip55-0.2.0 (c (n "eip55") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.5") (d #t) (k 0)))) (h "0wqmh15ihgkx5d30vm33yvb99myv78aj7yr418nqnpsl7pmh2klp")))

(define-public crate-eip55-0.3.0 (c (n "eip55") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.5") (d #t) (k 0)))) (h "1kxa52ha0qgy87xxsgx25x6hxn8l3l1fm3g6b22v37i7wzwb08a9")))

(define-public crate-eip55-0.4.0 (c (n "eip55") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha3") (r "^0.10.5") (d #t) (k 0)))) (h "08g4mjgdrip9vx3jrvn37cacs73msyhsggcsk99bhbh339p6hizl")))

