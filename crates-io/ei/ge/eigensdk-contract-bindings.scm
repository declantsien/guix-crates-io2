(define-module (crates-io ei ge eigensdk-contract-bindings) #:use-module (crates-io))

(define-public crate-eigensdk-contract-bindings-0.0.1-alpha (c (n "eigensdk-contract-bindings") (v "0.0.1-alpha") (d (list (d (n "ethers") (r "^2.0.14") (d #t) (k 0)))) (h "0jij4i3vjzr2vn1fhafvwmskhciz88zs1mjrmxb5k4s4ahal8w9n") (r "1.76")))

