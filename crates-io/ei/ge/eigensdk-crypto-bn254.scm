(define-module (crates-io ei ge eigensdk-crypto-bn254) #:use-module (crates-io))

(define-public crate-eigensdk-crypto-bn254-0.0.1-alpha (c (n "eigensdk-crypto-bn254") (v "0.0.1-alpha") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "ethers") (r "^2.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r5jrmb4dicjh3xv7r129vn85wwpj6gsxspj4v571f065x15hfr6") (r "1.76")))

