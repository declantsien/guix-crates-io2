(define-module (crates-io ei ge eigen-trust) #:use-module (crates-io))

(define-public crate-eigen-trust-0.1.0 (c (n "eigen-trust") (v "0.1.0") (d (list (d (n "ark-std") (r "^0.3.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"unknown\"))") (k 2)) (d (n "rand") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1zv9sg4i8n6245nicyi0kgvdrcpl7ymr4hajmc3jv37zab22sdy7") (f (quote (("std" "ark-std/std" "rand/std") ("default" "std"))))))

(define-public crate-eigen-trust-0.2.0 (c (n "eigen-trust") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "libp2p") (r "^0.44") (f (quote ("request-response" "tcp-async-io" "noise" "yamux"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("time" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1x03sg25hycrlpaxqkmnvs36l77dpnhxm56pp9n5q1m36pgkhh02")))

