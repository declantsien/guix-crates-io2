(define-module (crates-io ei ge eigensdk-crypto-bls) #:use-module (crates-io))

(define-public crate-eigensdk-crypto-bls-0.0.1-alpha (c (n "eigensdk-crypto-bls") (v "0.0.1-alpha") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "eigensdk-crypto-bn254") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n33cqc0rcxzm23af1iyhphf6m80mcl87mv5fsb7z7vvnkmx556k") (r "1.76")))

