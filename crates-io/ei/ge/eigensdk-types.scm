(define-module (crates-io ei ge eigensdk-types) #:use-module (crates-io))

(define-public crate-eigensdk-types-0.0.1-alpha (c (n "eigensdk-types") (v "0.0.1-alpha") (d (list (d (n "eigensdk-crypto-bls") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "ethers") (r "^2.0.14") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)))) (h "0s7b22jgvkxk03k1avppkq2apdy0hb1v0ci4z7qkfbqsvk1bkhrl") (r "1.76")))

