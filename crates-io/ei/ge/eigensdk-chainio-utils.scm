(define-module (crates-io ei ge eigensdk-chainio-utils) #:use-module (crates-io))

(define-public crate-eigensdk-chainio-utils-0.0.1-alpha (c (n "eigensdk-chainio-utils") (v "0.0.1-alpha") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "eigensdk-contract-bindings") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "eigensdk-crypto-bls") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "eigensdk-crypto-bn254") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "ethers") (r "^2.0.14") (d #t) (k 0)))) (h "1qf7qg8qjvd0npnhcwrlsz17qpil6n6q17sfsfmaghjxfxcnv3r0") (r "1.76")))

