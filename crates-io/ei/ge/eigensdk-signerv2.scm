(define-module (crates-io ei ge eigensdk-signerv2) #:use-module (crates-io))

(define-public crate-eigensdk-signerv2-0.0.1-alpha (c (n "eigensdk-signerv2") (v "0.0.1-alpha") (d (list (d (n "eth-keystore") (r "^0.5.0") (d #t) (k 0)) (d (n "ethers") (r "^2.0.14") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0.14") (d #t) (k 0)) (d (n "ethers-signers") (r "^2.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z7qqyx4b6nr4qcfcrdbqzh49vkc71g4spr99kkmnjzczfy24p4f") (r "1.76")))

