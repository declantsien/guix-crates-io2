(define-module (crates-io ei ge eigenvalues) #:use-module (crates-io))

(define-public crate-eigenvalues-0.1.0 (c (n "eigenvalues") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)))) (h "0snrg754rl3xp4ydb13fh2p9whjblsmwsgwnw35jgj0ad7xdpfki")))

(define-public crate-eigenvalues-0.1.1 (c (n "eigenvalues") (v "0.1.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)))) (h "154255shcz82mixn4cri22jvmilijkshlbz9f8mpp0g5qvbh4ni5")))

(define-public crate-eigenvalues-0.1.2 (c (n "eigenvalues") (v "0.1.2") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)))) (h "1crbk7z3icbqhxg0zxb9c8xq8irighrkrpwfrhwagvybmi66zqw1")))

(define-public crate-eigenvalues-0.1.3 (c (n "eigenvalues") (v "0.1.3") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)))) (h "14hag35sx9vglhgm8scasq560jx2k5si48d47fazxzknqry4jxa3")))

(define-public crate-eigenvalues-0.2.0 (c (n "eigenvalues") (v "0.2.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)))) (h "12xg9ss8chvrv5s3jvlxwiriz37a25s65dm24z313dl8kfzv71vf")))

(define-public crate-eigenvalues-0.3.0 (c (n "eigenvalues") (v "0.3.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)))) (h "009hr3s4r3r9gcrns2sw4k7q8niz30ya3i6pyv3563jx1cg3qslj")))

(define-public crate-eigenvalues-0.3.1 (c (n "eigenvalues") (v "0.3.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)))) (h "1zdzgar7lyvdzbd7zh6bafzh6anvbn25lf69ksf8hdlszmxi48zv")))

(define-public crate-eigenvalues-0.4.0 (c (n "eigenvalues") (v "0.4.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "nalgebra") (r "<0.25") (d #t) (k 0)))) (h "14ag041sfd3j77gdr5bf88anxqfb4a01x0iywqw0ngvicyzbq1jh")))

