(define-module (crates-io ei ge eigensdk-client-elcontracts) #:use-module (crates-io))

(define-public crate-eigensdk-client-elcontracts-0.0.1-alpha (c (n "eigensdk-client-elcontracts") (v "0.0.1-alpha") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "eigensdk-contract-bindings") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "eigensdk-txmgr") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "eigensdk-types") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "ethers") (r "^2.0.14") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0.14") (d #t) (k 0)) (d (n "ethers-providers") (r "^2.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0iww0mga71b6w5j6aa1qsg4ldhs3d9y7m5cifgyin4xlj2nck95b") (r "1.76")))

