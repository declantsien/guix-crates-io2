(define-module (crates-io ei ge eigensdk-txmgr) #:use-module (crates-io))

(define-public crate-eigensdk-txmgr-0.0.1-alpha (c (n "eigensdk-txmgr") (v "0.0.1-alpha") (d (list (d (n "eigensdk-client-wallet") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "eigensdk-signerv2") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "ethers") (r "^2.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04k1f33glfy7dpjhz44k62sqgjmbgw1dm271v2fnkrxzy9m0im92") (r "1.76")))

