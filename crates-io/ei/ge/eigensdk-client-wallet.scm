(define-module (crates-io ei ge eigensdk-client-wallet) #:use-module (crates-io))

(define-public crate-eigensdk-client-wallet-0.0.1-alpha (c (n "eigensdk-client-wallet") (v "0.0.1-alpha") (d (list (d (n "alloy-primitives") (r "^0.7.0") (d #t) (k 0)) (d (n "eigensdk-signerv2") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "ethers") (r "^2.0.14") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0.14") (d #t) (k 0)) (d (n "ethers-providers") (r "^2.0.14") (d #t) (k 0)) (d (n "ethers-signers") (r "^2.0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h56h020sqzmcda25ikxai9a91p10v7y2f2wb7w4p0y6hhrli938") (r "1.76")))

