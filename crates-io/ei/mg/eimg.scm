(define-module (crates-io ei mg eimg) #:use-module (crates-io))

(define-public crate-eimg-0.1.0 (c (n "eimg") (v "0.1.0") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)))) (h "1kg4n86nrry64dz3a26s2k2gw4m4h2dvnfla4hv1zkqbmfvzxvrm")))

(define-public crate-eimg-0.2.0 (c (n "eimg") (v "0.2.0") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)))) (h "0ilwlvim8d17fc4q5f5syxir3mjiksbc5i97yw0s89dcmfdqlglz")))

(define-public crate-eimg-0.2.1 (c (n "eimg") (v "0.2.1") (d (list (d (n "image") (r "^0.21.1") (d #t) (k 0)))) (h "0k89a4p09mvg6alrv96wrpjwsll1rwhmjynm0ksfpjbzlw4xdpp8")))

