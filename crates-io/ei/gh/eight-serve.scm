(define-module (crates-io ei gh eight-serve) #:use-module (crates-io))

(define-public crate-eight-serve-1.0.0 (c (n "eight-serve") (v "1.0.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eight") (r "^1.0.0-alpha") (f (quote ("expose"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "signal"))) (d #t) (k 0)))) (h "03j0cwrd0qk71qammqkgq0vb2wvvam5k2fxf7vznb6d4nkkgpjvd")))

(define-public crate-eight-serve-1.0.1 (c (n "eight-serve") (v "1.0.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eight") (r "^1.0.0-alpha.2") (f (quote ("expose"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "signal"))) (d #t) (k 0)))) (h "0da0n2bpzzakg919gvf6jm556gj880yh0bx39b6brh03zix8law2")))

