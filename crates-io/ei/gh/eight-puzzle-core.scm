(define-module (crates-io ei gh eight-puzzle-core) #:use-module (crates-io))

(define-public crate-eight-puzzle-core-1.0.0 (c (n "eight-puzzle-core") (v "1.0.0") (d (list (d (n "permutator") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "05sl9763d7vqfnvdx9y578ng23db7hvpa0lh6m42aajryi5dl25f")))

