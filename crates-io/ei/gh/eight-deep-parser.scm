(define-module (crates-io ei gh eight-deep-parser) #:use-module (crates-io))

(define-public crate-eight-deep-parser-0.1.0 (c (n "eight-deep-parser") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d7jraswdlx9802879047m5z8ziss9b4pjhra8gicnv24f14b0rb")))

(define-public crate-eight-deep-parser-0.1.1 (c (n "eight-deep-parser") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c4hwcq377ngi2wqhp6l4410j6qcm8fwqd0jyi511ldsf2q7qrla")))

(define-public crate-eight-deep-parser-0.2.0 (c (n "eight-deep-parser") (v "0.2.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06kzvd8a1gi6fjrq4ds5kq42lcjyfkyyrqjnljxzbb5g5gafgihv")))

(define-public crate-eight-deep-parser-0.2.1 (c (n "eight-deep-parser") (v "0.2.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15p7nav959hflnvw68f7iccpzg40mfiq3a5h6pkc9daj83s85dkb")))

(define-public crate-eight-deep-parser-0.2.2 (c (n "eight-deep-parser") (v "0.2.2") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00m7iakif0w5gpmkicfd1pxpqmcyq6ija6hs7w9zmrlhlq639r3x")))

(define-public crate-eight-deep-parser-0.3.0 (c (n "eight-deep-parser") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l9vlp6x94cvck8vbziw71gq9ng9yslmzilc2w841j80iqxkfnb7")))

(define-public crate-eight-deep-parser-0.3.1 (c (n "eight-deep-parser") (v "0.3.1") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09h4xrp9cphfx8w6anns183pcy4yryw94h2l8anjijrw4454fbc7")))

(define-public crate-eight-deep-parser-0.3.2 (c (n "eight-deep-parser") (v "0.3.2") (d (list (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j3g4af5pprrnwbhvaqva934zc3cymkrszma36wm1l6acxapd65x")))

