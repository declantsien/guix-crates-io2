(define-module (crates-io ei gh eight-puzzle) #:use-module (crates-io))

(define-public crate-eight-puzzle-1.1.0 (c (n "eight-puzzle") (v "1.1.0") (d (list (d (n "eight-puzzle-core") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "find_folder") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "piston_window") (r "^0.98.0") (d #t) (k 0)))) (h "02dsd2mfb12cqr2dllc434y4h8slh8hims1b1scqc44icg9dax34")))

