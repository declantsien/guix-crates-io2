(define-module (crates-io ei gh eight-point) #:use-module (crates-io))

(define-public crate-eight-point-0.1.0 (c (n "eight-point") (v "0.1.0") (d (list (d (n "cv-core") (r "^0.6.1") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 2)))) (h "1fsxw7yyfcda9c962vr8ayhc5q996qy4yw04lbcgffid5crzj29g")))

(define-public crate-eight-point-0.2.0 (c (n "eight-point") (v "0.2.0") (d (list (d (n "cv-core") (r "^0.7.1") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 2)))) (h "1vsfaz0wpq353ybjd3hq5p9ymb6v8jzsl3xw88x4f7jvrgdhb2l5")))

(define-public crate-eight-point-0.3.0 (c (n "eight-point") (v "0.3.0") (d (list (d (n "cv-core") (r "^0.9.0") (f (quote ("pinhole"))) (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.20.0") (d #t) (k 2)))) (h "0xxbhfvh2kk1kffcsqj2wr8b10xsdyliph48pyg2k03j85wh787s")))

(define-public crate-eight-point-0.4.0 (c (n "eight-point") (v "0.4.0") (d (list (d (n "cv-core") (r "^0.10.0") (d #t) (k 0)) (d (n "cv-pinhole") (r "^0.1.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (d #t) (k 2)))) (h "1vsnim5xmfjv03p4zi0a7dyl76pz9h8izlkw9djcch7nhr793vxp")))

(define-public crate-eight-point-0.5.0 (c (n "eight-point") (v "0.5.0") (d (list (d (n "cv-core") (r "^0.11.0") (d #t) (k 0)) (d (n "cv-geom") (r "^0.1.0") (d #t) (k 2)) (d (n "cv-pinhole") (r "^0.2.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (d #t) (k 2)))) (h "16rgkwqpxhlahlb1da7cwj2sr9w2v1x8ljg0ya4hi9xbgmw1kkv7")))

(define-public crate-eight-point-0.6.0 (c (n "eight-point") (v "0.6.0") (d (list (d (n "cv-core") (r "^0.12.0") (d #t) (k 0)) (d (n "cv-geom") (r "^0.2.0") (d #t) (k 2)) (d (n "cv-pinhole") (r "^0.3.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (d #t) (k 2)))) (h "1knvwjazqa5m4wibh7mby3acbx0ygdmygczkgrbq62ngpvb2brlr")))

(define-public crate-eight-point-0.7.0 (c (n "eight-point") (v "0.7.0") (d (list (d (n "cv-core") (r "^0.14.0") (d #t) (k 0)) (d (n "cv-pinhole") (r "^0.5.0") (k 0)) (d (n "derive_more") (r "^0.99.8") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (k 0)))) (h "1ndvy9f4ad7x157k5rgg117vdbvhz0nvvr31llvyzj4zzc9811pl")))

(define-public crate-eight-point-0.8.0 (c (n "eight-point") (v "0.8.0") (d (list (d (n "cv-core") (r "^0.15.0") (d #t) (k 0)) (d (n "cv-pinhole") (r "^0.6.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.8") (d #t) (k 0)) (d (n "float-ord") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (k 0)))) (h "0xvbj5rqr0933a59d78m5sa4kdmb5b4d63cvs536mdiv5lfsk18a")))

