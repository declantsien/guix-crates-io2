(define-module (crates-io ei gh eightyseven) #:use-module (crates-io))

(define-public crate-eightyseven-0.1.0 (c (n "eightyseven") (v "0.1.0") (d (list (d (n "arraystring") (r "^0.3.0") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (f (quote ("approx"))) (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "0sx3zsy8rjf5kmcbxjxrwpzrksxdimxka1fh5zz374iwdnp5qv9m")))

(define-public crate-eightyseven-0.1.1 (c (n "eightyseven") (v "0.1.1") (d (list (d (n "arraystring") (r "^0.3.0") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (f (quote ("approx"))) (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "08igrpdjj808gggd16r19mk6mcdbklbjlmcvdq5cn3g1z8qjy6cc")))

(define-public crate-eightyseven-0.1.2 (c (n "eightyseven") (v "0.1.2") (d (list (d (n "arraystring") (r "^0.3.0") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (f (quote ("approx"))) (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1h6h5y67lp7yzzmim452xp3m4gnc9jan9rd94yn301bcn5wydhwi")))

