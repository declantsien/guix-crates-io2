(define-module (crates-io ei gh eight_ball) #:use-module (crates-io))

(define-public crate-eight_ball-0.1.0 (c (n "eight_ball") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0-pre.1") (d #t) (k 0)))) (h "1mxzwcnz19mdk1cfg85gwabylaww332fn4giv746qzxkw9v3mw9f")))

(define-public crate-eight_ball-0.2.0 (c (n "eight_ball") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "161k2b7jx2n6acvv8d9br9kz2s2j256v5zr27dba2b7h4s69hlga")))

(define-public crate-eight_ball-0.2.1 (c (n "eight_ball") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0qicibi5iah1m92hk4imf6ghzs83f66dnbxshx4bgyq7jbw8ywik")))

(define-public crate-eight_ball-0.2.2 (c (n "eight_ball") (v "0.2.2") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0vkplvp94nws0r2srif3gzwgzjakjx2vg4kyn4bv6clwxjnscl3w")))

(define-public crate-eight_ball-0.2.3 (c (n "eight_ball") (v "0.2.3") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1vbz5gxqw0v0v4zqhbpzk7mvzrhjfa3s61hdkwsr6ppgl3zd8agg")))

(define-public crate-eight_ball-0.2.4 (c (n "eight_ball") (v "0.2.4") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "05x7594i9i1kaf79ig07b1mb8f8r644ssrrpfn33sdq7crh9rvia")))

(define-public crate-eight_ball-0.2.5 (c (n "eight_ball") (v "0.2.5") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1zvmblba8h7i2bf0qbclppqaivzaadg1vshm81z173r3wbg6c04c")))

(define-public crate-eight_ball-0.2.6 (c (n "eight_ball") (v "0.2.6") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1dshfindhw7wzyiqw08sv3121npfpxz5mbbjww1b836qhlx4kb2z")))

