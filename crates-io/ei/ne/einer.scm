(define-module (crates-io ei ne einer) #:use-module (crates-io))

(define-public crate-einer-0.1.0 (c (n "einer") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hammer-cli") (r "^0.3.0") (d #t) (k 0)))) (h "0iwqcxcd3hsfsp4dsyw2m05xad8kc8wymdy4wn8c6wx7nxgn0qs3")))

(define-public crate-einer-0.2.0 (c (n "einer") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hammer-cli") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0nglib1lwz7kwqc1w56pbc7s8yk8ny4ayvzf6bmzv516mmzgv66q")))

(define-public crate-einer-0.2.1 (c (n "einer") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "hammer-cli") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0h3k4kfgd30kn8kvsf60mfgxpvqxh20d6hsqkclpg0zhvj2mfqh1")))

(define-public crate-einer-0.4.0 (c (n "einer") (v "0.4.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hammer-cli") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "080xijhfbv4pq2qbj793z1jg5mkb4khp3p2pwyv79zklfzkjd9pr")))

(define-public crate-einer-0.5.0 (c (n "einer") (v "0.5.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hammer-cli") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "1yf8fw8alpn8ikxka2w8ly0wjb7gzmrci2c4f5dcwllwaq4y5lcw")))

(define-public crate-einer-0.5.1 (c (n "einer") (v "0.5.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hammer-cli") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "08wlymd2d6k461ybkkyayr73q3pfqlf8xdpnca11m5ig8i2af92v")))

(define-public crate-einer-0.5.2 (c (n "einer") (v "0.5.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hammer-cli") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "036n7rzkihbk797jnfniz1bd5hwi0wi73awrfbdps59fbid16bws")))

