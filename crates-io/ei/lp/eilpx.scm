(define-module (crates-io ei lp eilpx) #:use-module (crates-io))

(define-public crate-eilpx-0.1.0 (c (n "eilpx") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "04i0kr039jq9n5ab6irb1x0bqnj9czn6w9iiallipmkz4p6yaxlq")))

(define-public crate-eilpx-0.1.1 (c (n "eilpx") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0jb4x7hg57dxjcy9bz2m4z1vl180rh7qq4kq7v8aw9a1a1106z9a")))

(define-public crate-eilpx-0.2.0 (c (n "eilpx") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ccfbvkggsajsvqhyblmrldmfrs4b7fya0qyl7wv1jh6812xrizb")))

(define-public crate-eilpx-0.2.1 (c (n "eilpx") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qbbp4psan00an0gmbb6gzxns0ckbzjlvffnppk4abvj6xll3jil")))

