(define-module (crates-io ei -s ei-sys) #:use-module (crates-io))

(define-public crate-ei-sys-0.1.0 (c (n "ei-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1dd1nrawd2fgxyhn49b2ad9dq7s45c93kkny4x13h0rwm7ailq3j") (l "ei")))

(define-public crate-ei-sys-0.1.1 (c (n "ei-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1dcl612wh5sjy6xz9hbzs9w26l8x8qcwmmsvdqs1lxggn6rldcwg") (l "ei")))

(define-public crate-ei-sys-0.2.0 (c (n "ei-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("inaddr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wykkqkjqd7nw33pds1qqw2g6krcn9w7k1ah88f96hh1ld376lm0") (l "ei")))

(define-public crate-ei-sys-0.3.0 (c (n "ei-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("inaddr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17aynn1llkpignarsmq2al9gj4rav271fgki9mc5k9c7fgs926mr") (l "ei")))

(define-public crate-ei-sys-0.4.0 (c (n "ei-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("inaddr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04kabdvs2k3rdmb78rr2pbs9djji60s2r07r0jy675bj3d1askz4") (l "ei")))

(define-public crate-ei-sys-0.5.0 (c (n "ei-sys") (v "0.5.0") (d (list (d (n "in_addr") (r "^0.1.2") (f (quote ("no-std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "1ydrbd5p6z9inwkq675q6wi4lnl9317s2qjq8h2xfnyl2q02fvg2") (l "ei")))

(define-public crate-ei-sys-0.6.0 (c (n "ei-sys") (v "0.6.0") (d (list (d (n "in_addr") (r "^0.2") (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "1dlrcf5fkgy5wxj3x6yscn82hq4cyjikjaz6f7rnqp8g2h93pg3v") (l "ei")))

(define-public crate-ei-sys-0.7.0 (c (n "ei-sys") (v "0.7.0") (d (list (d (n "in_addr") (r "^1.0") (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "17dkk9x5nrzcxxi7k7s7nrznfciwyy8wcmrpxwrbldns6ia986rc") (l "ei")))

(define-public crate-ei-sys-0.8.0 (c (n "ei-sys") (v "0.8.0") (d (list (d (n "in_addr") (r "^1.0") (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "1l6nyb9nx4b1pscrb0z5hkjdq1iv0xpxk6dc7g17r81j06vvp8sb") (l "ei")))

(define-public crate-ei-sys-0.8.1 (c (n "ei-sys") (v "0.8.1") (d (list (d (n "in_addr") (r "^1.0") (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "1y7bh1sp1xfdlnq2qnwa4b4iiargfaj3yixnxi5gb8vjsdyz8vla") (l "ei")))

