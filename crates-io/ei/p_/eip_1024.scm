(define-module (crates-io ei p_ eip_1024) #:use-module (crates-io))

(define-public crate-eip_1024-0.1.0 (c (n "eip_1024") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "saltbabe") (r "^0.1.0") (d #t) (k 0)))) (h "1g3piiykxhvbdgwfaprvzdmqr6szkplk4fggnk141jryqdi91nss") (y #t)))

(define-public crate-eip_1024-0.1.1 (c (n "eip_1024") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "saltbabe") (r "^0.1.0") (d #t) (k 0)))) (h "0ija5yddcfnajhcbn069yc8h6n87ln8i5j78sx8nm3l2xyifis0v")))

