(define-module (crates-io ei th either-slot) #:use-module (crates-io))

(define-public crate-either-slot-0.1.0 (c (n "either-slot") (v "0.1.0") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 2)))) (h "1hl005yam6z5zdj1b1sb7xgj50fly9b6qbm6w3nazkk6bwkkx7pq")))

(define-public crate-either-slot-1.0.0 (c (n "either-slot") (v "1.0.0") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 0)) (d (n "tuple_list") (r "^0.1") (k 0)))) (h "1ylpqhff1b7gv2idmp3mpnjgf3q6qnh20nny74wj37gfypvplsxf")))

(define-public crate-either-slot-1.1.0 (c (n "either-slot") (v "1.1.0") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 0)) (d (n "tuple_list") (r "^0.1") (k 0)))) (h "0ayq6l32j33cbhyarg3y3bssg58q22i50p921rrrqsm6s4z50vdl")))

(define-public crate-either-slot-1.2.0 (c (n "either-slot") (v "1.2.0") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 0)) (d (n "tuple_list") (r "^0.1") (k 0)))) (h "1qdmaayy45hk1pzmn92l2v5i75npdgkvzw5mczi3gcysnvlhx2y7")))

