(define-module (crates-io ei th either) #:use-module (crates-io))

(define-public crate-either-0.1.0 (c (n "either") (v "0.1.0") (h "06ljmgv5w26rh7s32746wks5vph81v1av5azpp3zq298b970zprl")))

(define-public crate-either-0.1.1 (c (n "either") (v "0.1.1") (h "1ddfv93njs54gzldbsfbjcf4rp97zl7y952f8xhg1ds6xhfh74gl")))

(define-public crate-either-0.1.2 (c (n "either") (v "0.1.2") (h "09p6jm7gb03lq6h5xii3z4l1aypqv41mpqjrx5lwn1zpr62kqza2")))

(define-public crate-either-0.1.3 (c (n "either") (v "0.1.3") (h "0696xqfapyc8xzmp9r0zc05gs05704zs8l23cp3zzfry68s4iyhi")))

(define-public crate-either-0.1.4 (c (n "either") (v "0.1.4") (h "0zd9q77c92ak5bp21cwwsf2ajsz5hfy2p4h0xcn0vsibhvnakl4k")))

(define-public crate-either-0.1.5 (c (n "either") (v "0.1.5") (h "1jgm3smnxa4c275c7w761xvclxf73w3y41igy1npm8bb5bv0m0n9")))

(define-public crate-either-0.1.6 (c (n "either") (v "0.1.6") (h "16c6bbrf6xq05l67vawbhdc446x8glw1c8qf7gxms86m659sj9xn")))

(define-public crate-either-0.1.7 (c (n "either") (v "0.1.7") (h "19wy97sp344n6i6653rr8b45imyh937v0g3plvcca5903vngz6x3")))

(define-public crate-either-1.0.0 (c (n "either") (v "1.0.0") (h "19mrvdv98w33xr46h920ckfp32sadpr2siirvwb99xwpsivvyiaq") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.0.1 (c (n "either") (v "1.0.1") (h "13gdvxpd8kcl51dmvpsfi5mafvjpa559rn4zlnl8kg8sgqmwi8la") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.0.2 (c (n "either") (v "1.0.2") (h "149aygbby4hbgx9i5n7qngnsnfblaa62pksf86pjmmnshqy50arx") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.0.3 (c (n "either") (v "1.0.3") (h "0707v2rrk2nng0vrkrh3sv2mbsic7wvm00jzx1w423fam4slmyb3") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.1.0 (c (n "either") (v "1.1.0") (h "0ajng2fhcbyd89shfwiwgsinjzpfm6nl8zlkgh9mihh6m0dmqy0q") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.2.0 (c (n "either") (v "1.2.0") (h "0lm208pjfx5qi7h881sia4k4iqjrkg66rgajpxlnhha5j9g17vnb") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.3.0 (c (n "either") (v "1.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kkksacv24msdgs5jdhiq1dzifgkb7n92kdmiy2xzyqjjm3sf4g3") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.4.0 (c (n "either") (v "1.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cw136q6z8zqlvkyiai857yl4b9a8jhhjrbdis3rw6lbykfph0bl") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.5.0 (c (n "either") (v "1.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1l610pr0b00ya5pmdii9r583c0z3z521ikygwxcpyzambk56br9v") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.5.1 (c (n "either") (v "1.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1b0z3nbnry7cc8vamygg7fxykpjksydxc0hrx8j7316w87356wy6") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.5.2 (c (n "either") (v "1.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0yyggfd5yq9hyyp0bd5jj0fgz3rwws42d19ri0znxwwqs3hcy9sm") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.5.3 (c (n "either") (v "1.5.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qyz1b1acad6w0k5928jw5zaq900zhsk7p8dlcp4hh61w4f6n7xv") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.6.0 (c (n "either") (v "1.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "0krcvv612bd14iz2nxncjya3nv4ga3x9qxsqb6n4gsdwcncbamnd") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.6.1 (c (n "either") (v "1.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "0mwl9vngqf5jvrhmhn9x60kr5hivxyjxbmby2pybncxfqhf4z3g7") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.7.0 (c (n "either") (v "1.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "1gl1cybx0rrcvny4rcfl2agqbj6n0vz5bb1ws57sdhmgns3pn41z") (f (quote (("use_std") ("default" "use_std")))) (r "1.31")))

(define-public crate-either-1.8.0 (c (n "either") (v "1.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "15z70yaivlkpx27vzv99ibf8d2x5jp24yn69y0xi20w86v4c3rch") (f (quote (("use_std") ("default" "use_std")))) (r "1.36")))

(define-public crate-either-1.8.1 (c (n "either") (v "1.8.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "14bdy4qsxlfnm4626z4shwaiffi8l5krzkn7ykki1jgqzsrapjkz") (f (quote (("use_std") ("default" "use_std")))) (r "1.36")))

(define-public crate-either-1.9.0 (c (n "either") (v "1.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "01qy3anr7jal5lpc20791vxrw0nl6vksb5j7x56q2fycgcyy8sm2") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1.10.0 (c (n "either") (v "1.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "0jiyq2mc1aa5b8whwl1bhm11i06xxcbk9ck7macxxggzjk07l58i") (f (quote (("use_std") ("default" "use_std")))) (r "1.36")))

(define-public crate-either-1.11.0 (c (n "either") (v "1.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "18l0cwyw18syl8b52syv6balql8mnwfyhihjqqllx5pms93iqz54") (f (quote (("use_std") ("default" "use_std")))) (r "1.36")))

(define-public crate-either-1.12.0 (c (n "either") (v "1.12.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 2)))) (h "12xmhlrv5gfsraimh6xaxcmb0qh6cc7w7ap4sw40ky9wfm095jix") (f (quote (("use_std") ("default" "use_std")))) (r "1.37")))

