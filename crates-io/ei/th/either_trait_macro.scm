(define-module (crates-io ei th either_trait_macro) #:use-module (crates-io))

(define-public crate-either_trait_macro-0.1.0 (c (n "either_trait_macro") (v "0.1.0") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "017jf10m7ykxsqyfwpk2ghpnz2arqn107pq4g0ax9qgi9360fb92")))

(define-public crate-either_trait_macro-0.1.1 (c (n "either_trait_macro") (v "0.1.1") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1k4mkvg633vxa1bcb1cazrpywnmhd4p3k7sqfw6nlh31qk3lxibn")))

