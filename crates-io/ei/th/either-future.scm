(define-module (crates-io ei th either-future) #:use-module (crates-io))

(define-public crate-either-future-0.1.0 (c (n "either-future") (v "0.1.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio01") (r "^0.1") (d #t) (k 2) (p "tokio")) (d (n "tokio02") (r "^0.2.0-alpha") (d #t) (k 2) (p "tokio")))) (h "1300q8d321nql8566vda9k9sbx71s2n6h3q0s3dw85qaf69vzpg8") (f (quote (("std_future") ("futures_future" "futures") ("default" "std_future" "futures_future"))))))

(define-public crate-either-future-1.0.0 (c (n "either-future") (v "1.0.0") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (k 0)) (d (n "futures") (r "^0.1") (f (quote ("use_std"))) (k 2)))) (h "0r02j8zdg94qmzbawaminghlw4myzhg1y7cqy104kfmhy3rrk4pg") (f (quote (("std_future") ("futures01" "futures") ("default" "std_future"))))))

(define-public crate-either-future-1.1.0 (c (n "either-future") (v "1.1.0") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (k 0)) (d (n "futures") (r "^0.1") (f (quote ("use_std"))) (k 2)) (d (n "futures-util") (r "^0.3") (o #t) (k 0)))) (h "08drrdnysr1hvnbb896rgczv6bk5r5gvxn15yyvm114zv3xlj7m1") (f (quote (("std_future") ("futures03" "futures-util") ("futures01" "futures") ("default" "std_future"))))))

