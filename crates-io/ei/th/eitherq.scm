(define-module (crates-io ei th eitherq) #:use-module (crates-io))

(define-public crate-eitherq-0.1.0 (c (n "eitherq") (v "0.1.0") (h "0nzns5w7lqgrnqdmj982vnrd064zi3dyxl0mn5fcavdpxj2akmbz")))

(define-public crate-eitherq-0.1.1 (c (n "eitherq") (v "0.1.1") (h "1blz6xj78vx0qb2z1w29jw2ksqkdvjzrcrmspl7v9bhgwnv1lag6")))

(define-public crate-eitherq-0.1.2 (c (n "eitherq") (v "0.1.2") (h "02iifs03g8xywwb85k115l4rq07kgkqwadb6ckrypf085n384hqi")))

