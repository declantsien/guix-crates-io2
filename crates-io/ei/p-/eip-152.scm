(define-module (crates-io ei p- eip-152) #:use-module (crates-io))

(define-public crate-eip-152-0.1.0 (c (n "eip-152") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 2)))) (h "1bw49bd6s6yjhnijwfrvfqhsgmr5mmvmzy8d5cycrh41di1zc7ns")))

