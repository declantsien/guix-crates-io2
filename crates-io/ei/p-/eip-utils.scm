(define-module (crates-io ei p- eip-utils) #:use-module (crates-io))

(define-public crate-eip-utils-0.1.0 (c (n "eip-utils") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rlp") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1r12zjwgyf0bl3qy3jqdsrjg48ga2ndlw5idfcby5ajv06myqzhp")))

(define-public crate-eip-utils-0.1.1 (c (n "eip-utils") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rlp") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "05n8ck9zdw6dhx40rr5vvpps1i3gy1z44pacia1yrlqrdng4i2vp")))

(define-public crate-eip-utils-0.1.2 (c (n "eip-utils") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rlp") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0jky48f1my91r7jjckspcxpkswq0c4chwx4zmmjr1ralvkxgc2xz")))

(define-public crate-eip-utils-0.1.3 (c (n "eip-utils") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rlp") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0242r8q2z9isg17sd38ly47bav16cl56c3472d4wbm36gxnplwvz")))

(define-public crate-eip-utils-0.1.4 (c (n "eip-utils") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rlp") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1ahc85jp3630bamchpvlmiji4ahkpgadflpaxdbabnp83xiclflb")))

(define-public crate-eip-utils-0.1.5 (c (n "eip-utils") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rlp") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0gmmpja4dpbn9dnjkax9bsx9cp9n7s3iky41jry0i8kiz4i3y2rm")))

