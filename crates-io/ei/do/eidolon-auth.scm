(define-module (crates-io ei do eidolon-auth) #:use-module (crates-io))

(define-public crate-eidolon-auth-0.1.0 (c (n "eidolon-auth") (v "0.1.0") (d (list (d (n "amplify") (r "^4.0.0-beta.4") (d #t) (k 0)) (d (n "cyphergraphy") (r "^0.1.0") (d #t) (k 0)))) (h "11yp89mhpkksq478ahbm6g2c1c14py6vvyrrvz96412f0c2qszdw") (r "1.65")))

(define-public crate-eidolon-auth-0.2.0 (c (n "eidolon-auth") (v "0.2.0") (d (list (d (n "amplify") (r "^4.0.0-beta.17") (d #t) (k 0)) (d (n "cyphergraphy") (r "^0.2.0") (d #t) (k 0)))) (h "1z1z7w82wip3s0b1rsi35gk2n69d1fglxlh3ab5wm2zfn48l8xja") (r "1.65")))

(define-public crate-eidolon-auth-0.3.0 (c (n "eidolon-auth") (v "0.3.0") (d (list (d (n "amplify") (r "^4.6.0") (d #t) (k 0)) (d (n "cyphergraphy") (r "^0.3.0") (d #t) (k 0)))) (h "17krb5r4c19v6irjna9gscrkb99h66cy8vw928lpssaqghya2ms0") (r "1.69")))

