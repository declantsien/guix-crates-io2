(define-module (crates-io ei do eidolon) #:use-module (crates-io))

(define-public crate-eidolon-1.1.0 (c (n "eidolon") (v "1.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1vlhmchvx934r0x1x00hmyli5wqdvp6fb0z0rs8nqcjj8kvc51ff")))

(define-public crate-eidolon-1.1.1 (c (n "eidolon") (v "1.1.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "183rf1wglc9m2h11hnk35q85zww4abhf44nfgm1yyafal6r9rl4v")))

(define-public crate-eidolon-1.1.2 (c (n "eidolon") (v "1.1.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "15ws55x47bzfnrqxdhv8hdqq1xvxamy9813ivhk6mx10qxk7qf8n")))

(define-public crate-eidolon-1.1.3 (c (n "eidolon") (v "1.1.3") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1kc9f6ydyz9vxhbq89hx5hnirzmvrhs99mz61bvincp58jbhgkjp")))

(define-public crate-eidolon-1.1.4 (c (n "eidolon") (v "1.1.4") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "18738pqpfzpzwsbr9qg1ahr9s6qrspzwa9wg9hdgy8q5f1w0xz08")))

(define-public crate-eidolon-1.1.5 (c (n "eidolon") (v "1.1.5") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0hfaiiinnm679zxyaii6x7kwidid5ykjms98m8hdcp243vjqq7fn")))

(define-public crate-eidolon-1.2.5 (c (n "eidolon") (v "1.2.5") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "13zfwjb4pp0bfq2vhjqsbgv183jhw9dwp8y2zf5hsf78a71kggyk")))

(define-public crate-eidolon-1.2.6 (c (n "eidolon") (v "1.2.6") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0al2g6g30snlh7lzw282dq1wwhxxvqbfdcgc27kw6b9r40607chl")))

(define-public crate-eidolon-1.2.7 (c (n "eidolon") (v "1.2.7") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gvry6v8lpmlazgk1spylmm4nmfs3f5hljrshc8a6hmg3l7f1y86")))

(define-public crate-eidolon-1.3.0 (c (n "eidolon") (v "1.3.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00b0ak8mfv4j2vkimp5n2aaxzg7ldb22gmkwx1w27arwhyz2lgj0")))

(define-public crate-eidolon-1.4.0 (c (n "eidolon") (v "1.4.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d0wf7cchcjx8jypjjm493ajhk1hxphw9bdyjy7pm7b8aqbfnnnj")))

(define-public crate-eidolon-1.4.1 (c (n "eidolon") (v "1.4.1") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0bnf2b8lv4mzdk9bg967kld41yjx8d1bjwfdnccbg2ywvc4w5a95")))

(define-public crate-eidolon-1.4.2 (c (n "eidolon") (v "1.4.2") (d (list (d (n "butlerd") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0dnxb69agdgqjh0g63r1yk7hpmdngmp99ydizw15rp3lzv2v6x6p")))

(define-public crate-eidolon-1.4.3 (c (n "eidolon") (v "1.4.3") (d (list (d (n "butlerd") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1gvigj3855f56myb2ngvi5qch3b44mmk9n57vi3815j4h4gwcayb")))

(define-public crate-eidolon-1.4.4 (c (n "eidolon") (v "1.4.4") (d (list (d (n "butlerd") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "172gq7l9987m3c4aqczbmd97gacr8c0af77sw7yi16w2h7b5ggq0")))

(define-public crate-eidolon-1.4.5 (c (n "eidolon") (v "1.4.5") (d (list (d (n "butlerd") (r "^0.1.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0h97hzz6b69iq1lj47z4jdmcvk9gjzqh90dlmkmr7v2w9d2ziqbr")))

(define-public crate-eidolon-1.4.6 (c (n "eidolon") (v "1.4.6") (d (list (d (n "butlerd") (r "^0.1.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0w8n6n8xah0gmlbi5bn4dp76n8csg7508qab87hjinvg4vpiwl4a")))

