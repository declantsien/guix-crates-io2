(define-module (crates-io ei rc eirc) #:use-module (crates-io))

(define-public crate-eirc-0.0.0 (c (n "eirc") (v "0.0.0") (d (list (d (n "eventbus") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)))) (h "1a72ykq9zjmqajcgb7ciydb3rpvxxljjvn6f7h1v66a3qp058fw4")))

