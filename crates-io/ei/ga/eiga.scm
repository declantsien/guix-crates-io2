(define-module (crates-io ei ga eiga) #:use-module (crates-io))

(define-public crate-eiga-0.1.0 (c (n "eiga") (v "0.1.0") (d (list (d (n "eiga_builder_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "httpmock") (r "^0.6.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1js2ajza8mdwsa6w357arc4g1an0lsnwsq0g35b71zdds3qi3qsy")))

(define-public crate-eiga-0.2.0 (c (n "eiga") (v "0.2.0") (d (list (d (n "eiga_builder_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "httpmock") (r "^0.6.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "00r8nparxpgwnj5hypg6f98vrm4b0fy337bh725r2cciiihwh41y")))

(define-public crate-eiga-0.3.0 (c (n "eiga") (v "0.3.0") (d (list (d (n "eiga_builder_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "httpmock") (r "^0.6.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "05c9babddwjqydacivq779rpkbpqy1xbb03h9dp4yjd436szg92a")))

