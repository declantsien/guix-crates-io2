(define-module (crates-io ei ga eiga_builder_derive) #:use-module (crates-io))

(define-public crate-eiga_builder_derive-0.1.0 (c (n "eiga_builder_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q0q3zm4v5r9shlgf8lav8wrwfpl8mymxi39wg06vyqym8zihza0")))

(define-public crate-eiga_builder_derive-0.2.0 (c (n "eiga_builder_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ki36ymbiw8zx37p8vq4sdylgknxry79pxv7vhdpzmp380dnjc8v")))

(define-public crate-eiga_builder_derive-0.3.0 (c (n "eiga_builder_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14k0k9babmbv2xy043yn4qkwi7hsca9jhfpkmj4np5c1qaqxqjgr")))

