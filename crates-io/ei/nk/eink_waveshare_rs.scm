(define-module (crates-io ei nk eink_waveshare_rs) #:use-module (crates-io))

(define-public crate-eink_waveshare_rs-0.1.0 (c (n "eink_waveshare_rs") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1hzxv22j38pizyhvf5zrf4g1ffgqwqhshahqfnpaqca8p53mqxch") (f (quote (("epd4in2_fast_update") ("epd4in2") ("epd2in9") ("epd1in54") ("default" "epd1in54" "epd2in9" "epd4in2")))) (y #t)))

(define-public crate-eink_waveshare_rs-0.1.1 (c (n "eink_waveshare_rs") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "158srr42y1rd0b0ays3kgzwpzs9jd54gmjkhvqy0hdziq4z48l2y") (f (quote (("epd4in2_fast_update") ("epd4in2") ("epd2in9") ("epd1in54") ("default" "epd1in54" "epd2in9" "epd4in2")))) (y #t)))

(define-public crate-eink_waveshare_rs-0.1.2 (c (n "eink_waveshare_rs") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1vql7jfnvbpy1fxsd1na8g0gqqp9syb1472sn3jpsxw566x0hbh0") (f (quote (("epd4in2_fast_update") ("epd4in2") ("epd2in9") ("epd1in54") ("default" "epd1in54" "epd2in9" "epd4in2")))) (y #t)))

(define-public crate-eink_waveshare_rs-0.1.3 (c (n "eink_waveshare_rs") (v "0.1.3") (h "1s45c7x13garb08la6g6cxz69m7ngh7bwv7scxvfa8s123n70ryx") (y #t)))

(define-public crate-eink_waveshare_rs-0.1.4 (c (n "eink_waveshare_rs") (v "0.1.4") (h "164vvhwg9kjcvbh9vqwa0fx03m6i88c94adkaha8by04awyby6i9") (y #t)))

(define-public crate-eink_waveshare_rs-0.1.5 (c (n "eink_waveshare_rs") (v "0.1.5") (h "1kby1f959ygma29hh4y0pdbjll474lqw54h7qs4r6m7mmrri9wx0") (y #t)))

(define-public crate-eink_waveshare_rs-0.1.6 (c (n "eink_waveshare_rs") (v "0.1.6") (h "0lzzhd1wqbv2p507z9m63gb23q1s225x6i3gvsb3kp6nkiyj772x")))

