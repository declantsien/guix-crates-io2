(define-module (crates-io ei ng eingang-frontend) #:use-module (crates-io))

(define-public crate-eingang-frontend-0.1.0 (c (n "eingang-frontend") (v "0.1.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "eingang") (r "0.1.*") (d #t) (k 0)) (d (n "wasm-bindgen") (r "0.2.*") (d #t) (k 0)) (d (n "yew") (r "0.17.*") (d #t) (k 0)))) (h "0l3psnx2bynk4g2kk19cpmn2vjfnfv0b2igzby2sn4fbc1b2iqrn")))

