(define-module (crates-io ei ng eingang) #:use-module (crates-io))

(define-public crate-eingang-0.1.0 (c (n "eingang") (v "0.1.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "chrono") (r "0.4.*") (f (quote ("serde" "wasmbind"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "0.8.*") (f (quote ("serde" "wasm-bindgen" "v4"))) (d #t) (k 0)))) (h "0l8sy5pw155fsfrr2bpcyqx4ka9fz1jlann6pf6qdggmqxygbnzb")))

