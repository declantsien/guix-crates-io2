(define-module (crates-io ei ch eiche) #:use-module (crates-io))

(define-public crate-eiche-0.1.0 (c (n "eiche") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0f3jin71lw33bi6i0bf3isl7cy6x20fcpx5aam6p08d0dn8xb4c7")))

(define-public crate-eiche-0.1.1 (c (n "eiche") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1h0hq86ysg0s7qqs8b4pkgr3yihgp4f7dzaic99sbpdpfz783xmb")))

