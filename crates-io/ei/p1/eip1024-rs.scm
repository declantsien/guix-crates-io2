(define-module (crates-io ei p1 eip1024-rs) #:use-module (crates-io))

(define-public crate-eip1024-rs-0.1.0 (c (n "eip1024-rs") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "saltbabe") (r "^0.1.0") (d #t) (k 0)))) (h "120x9bsx33kb8nj6822i6yv0qdiah4k5m31y6l2ccafl2qp8bbk3") (y #t)))

