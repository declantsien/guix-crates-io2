(define-module (crates-io ei p1 eip1193) #:use-module (crates-io))

(define-public crate-EIP1193-0.1.0 (c (n "EIP1193") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.44") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.67") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.17") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (k 0)))) (h "1wqh9gd2qyph40r890jshf10c0rwxjdaxdldkr06xh05ajwxv1qy")))

