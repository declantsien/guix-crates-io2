(define-module (crates-io ei nt eint) #:use-module (crates-io))

(define-public crate-eint-0.1.0 (c (n "eint") (v "0.1.0") (h "1sp49ndv0wsbcd87lnv7h5najxha2liljyvkq433z8d1d06ifm9m")))

(define-public crate-eint-0.1.1 (c (n "eint") (v "0.1.1") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "151spqjk92d5q60s9ld415fmkgx0rdzq85kg8k8lh6ciahrryd4b")))

(define-public crate-eint-0.1.2 (c (n "eint") (v "0.1.2") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0qdjmv4npbbidbswgw0vds3c2k8l3cybjibhb0knlzkpyy24gf9c")))

(define-public crate-eint-0.1.3 (c (n "eint") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "036bjxwzdahyc4jpqw4shkmhkmwqmmp4d6za5v7mzm07pq1zjl1d")))

(define-public crate-eint-0.1.4 (c (n "eint") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1pg6qiznbx0qprcpvrh00174w80rv9ll4ly3yqx35fsqsiz89w4l")))

(define-public crate-eint-0.1.5 (c (n "eint") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0xln27kq3r42z945cjicgqlxkw2cyc9hg7sn0jmac7didx5njgvx")))

(define-public crate-eint-0.1.6 (c (n "eint") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "04303s6nx4am6magkrm1h6cldnzkjsdpf236352cl2zbvqs3v2k6")))

(define-public crate-eint-0.1.7 (c (n "eint") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1wviyr4prr278dvw110fw0rhcbz0abzqbk77j4k0l3s90q77i1v6")))

(define-public crate-eint-0.1.8 (c (n "eint") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "118yi8w5saynan0awijmgxwwp5vk239zjyxbpbixbqxd1hnh0l40")))

(define-public crate-eint-0.1.9 (c (n "eint") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1z7hwfq6yy89dx67q0r637g6hi0qhpayp4gjg3rrdwya34wjlwgf")))

(define-public crate-eint-0.1.10 (c (n "eint") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0r6hx1lnqmbnfrvlx4v571wxfcmq5ykz8506rvgw63wm6dqs2cyg")))

(define-public crate-eint-1.0.0 (c (n "eint") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "uint") (r "^0.9.3") (d #t) (k 0)))) (h "13ivrcsk5mx77vbak6y27r1pkfhdl8wzxdyi38hx3zqjr7hgnplr")))

