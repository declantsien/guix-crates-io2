(define-module (crates-io ei n- ein-ffi) #:use-module (crates-io))

(define-public crate-ein-ffi-0.1.0 (c (n "ein-ffi") (v "0.1.0") (d (list (d (n "bdwgc-alloc") (r "^0.5") (d #t) (k 0)))) (h "0189bza9ck03mgb52bxh4qvhk87a816fpifal9s65igz9390dcbf")))

(define-public crate-ein-ffi-0.2.0 (c (n "ein-ffi") (v "0.2.0") (d (list (d (n "bdwgc-alloc") (r "^0.5") (d #t) (k 0)))) (h "08ka1pfsywigvaimbh9846w47009qw8niiilrs6bwhmvfd7xi6pl")))

(define-public crate-ein-ffi-0.2.1 (c (n "ein-ffi") (v "0.2.1") (h "1ds7d82y16276vmz95j5fdwd9g6gl9q49mn8vszzdvg861ha3cgk")))

(define-public crate-ein-ffi-0.2.2 (c (n "ein-ffi") (v "0.2.2") (h "1xfvaa5l537422hbry6rv9nz21ar8m4l6lpvr81692515ipihp11")))

(define-public crate-ein-ffi-0.2.3 (c (n "ein-ffi") (v "0.2.3") (h "0z7hya11cqbj84rk1l700qbicq0i0wznn5dqah9jgycgpdfbv4wx")))

(define-public crate-ein-ffi-0.2.4 (c (n "ein-ffi") (v "0.2.4") (h "0yjvhsvf3qzb907d5610pyszl3cslibvi1xdsqd8hamh6dmn8i91")))

(define-public crate-ein-ffi-0.3.0 (c (n "ein-ffi") (v "0.3.0") (h "0a4vz3s6471q8mjl35pzpa99wg4b1h92gyg0jkjx640cg22kdps9")))

(define-public crate-ein-ffi-0.3.1 (c (n "ein-ffi") (v "0.3.1") (h "1fl3bkpk1hhgr9fp1my7pvz1zhr63r4yr05rcypiznf4m1mc5vgq")))

(define-public crate-ein-ffi-0.3.2 (c (n "ein-ffi") (v "0.3.2") (h "0p9rqlyr2117szlsm4ank8myxqzwzw80dcybgx3316lqpgr61rpp")))

(define-public crate-ein-ffi-0.3.3 (c (n "ein-ffi") (v "0.3.3") (h "028j9hy5imv8k52j9zvqs80xisq48npgcynp9d9xp65mvf888af5")))

(define-public crate-ein-ffi-0.3.4 (c (n "ein-ffi") (v "0.3.4") (h "1asvxc5sp981phrdy1kc3120hn0wdzns8qakbf4p5m921zrbdzbv")))

(define-public crate-ein-ffi-0.4.0 (c (n "ein-ffi") (v "0.4.0") (h "1rnh31x5mq08v8m9iqz2bd8vapy04631p43xhjlfw6qi6jf9dic5")))

(define-public crate-ein-ffi-0.5.0 (c (n "ein-ffi") (v "0.5.0") (h "18jcfmbzibbjxgd1w09afw3l78dc3rm5lws29z6m1simkhx7hbjr")))

(define-public crate-ein-ffi-0.5.1 (c (n "ein-ffi") (v "0.5.1") (h "1j369sxs4dlhq75gqb484gr7pj4fjvihn6m4nr21cnxm0fycf2rj")))

(define-public crate-ein-ffi-0.5.2 (c (n "ein-ffi") (v "0.5.2") (h "1084z2sc7xk6rinb600vdl6x439db742wq2k9ljykvgm69f2j2zq")))

(define-public crate-ein-ffi-0.5.3 (c (n "ein-ffi") (v "0.5.3") (h "0569b79zryvyd96k7bw24lhq2pz7hfy41r3ilgrmzcl8gmyw2k0n")))

(define-public crate-ein-ffi-0.5.4 (c (n "ein-ffi") (v "0.5.4") (h "1njjw3w9594awn4m0lp1lm9ak17x8bwy5n1qwhpag9zb248qkzy8")))

(define-public crate-ein-ffi-0.5.5 (c (n "ein-ffi") (v "0.5.5") (h "0gmvz6vb47zyjf43a1qjrld2cpxqxji57l14kbp3p7n2a1ay7p65")))

(define-public crate-ein-ffi-0.6.0 (c (n "ein-ffi") (v "0.6.0") (h "19bxfwgs707rxq9nbw5s804gpdn68jbj76r7vj5dpkf3m3b3nfva")))

