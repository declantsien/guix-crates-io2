(define-module (crates-io ei pw eipw-preamble) #:use-module (crates-io))

(define-public crate-eipw-preamble-0.1.0 (c (n "eipw-preamble") (v "0.1.0") (d (list (d (n "annotate-snippets") (r "^0.9.1") (d #t) (k 0)) (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "139wb9yzwrpdr0pyia756ynicbi59qasgns7hm7r52rv7ppzy2ld") (r "1.69")))

