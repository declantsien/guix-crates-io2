(define-module (crates-io ei gs eigs) #:use-module (crates-io))

(define-public crate-eigs-0.0.1 (c (n "eigs") (v "0.0.1") (d (list (d (n "arpack-ng-sys") (r "^0.2.1") (f (quote ("static"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "umfpack-rs") (r "^0.0.1") (d #t) (k 0)))) (h "0was0pn4w0pk36hry7b81gqzkr69z4hpciibbs91nixgcayp57m5")))

(define-public crate-eigs-0.0.2 (c (n "eigs") (v "0.0.2") (d (list (d (n "arpack-ng-sys") (r "^0.2.1") (f (quote ("static"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "umfpack-rs") (r "^0.0.1") (d #t) (k 0)))) (h "1wbwp3521ww03pj9d9701l5abfxi2z5b5h69ia3yxphjq9g4pgwc")))

(define-public crate-eigs-0.0.3 (c (n "eigs") (v "0.0.3") (d (list (d (n "arpack-ng-sys") (r "^0.2.1") (f (quote ("static"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "umfpack-rs") (r "^0.0.1") (d #t) (k 0)))) (h "05ald4z92zlpwzgx4wdzzcn887849d579498y342a6mk8c19rp73")))

