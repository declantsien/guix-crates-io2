(define-module (crates-io ei gs eigs-rs) #:use-module (crates-io))

(define-public crate-eigs-rs-0.0.1 (c (n "eigs-rs") (v "0.0.1") (d (list (d (n "arpack-ng-sys") (r "^0.2.1") (f (quote ("static"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "umfpack-rs") (r "^0.0.1") (d #t) (k 0)))) (h "1dfag8s6nkrvykpkz8xj5rmyynnvi9nyzvgdrhpfw3s12sfcd1km") (y #t)))

(define-public crate-eigs-rs-0.0.2 (c (n "eigs-rs") (v "0.0.2") (d (list (d (n "arpack-ng-sys") (r "^0.2.1") (f (quote ("static"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "umfpack-rs") (r "^0.0.1") (d #t) (k 0)))) (h "15ad9362jwhgbda13z400ma6q92k2c5157i83hwmz5mdgbvixjvj") (y #t)))

