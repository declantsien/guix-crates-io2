(define-module (crates-io ei ns einsum) #:use-module (crates-io))

(define-public crate-einsum-0.1.0 (c (n "einsum") (v "0.1.0") (d (list (d (n "einsum_impl") (r "^0.1.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rust-numpy") (r "^0.1.0") (d #t) (k 0)))) (h "0q4kiwa48kfhcs2235ymhb15k8xnhi597lsia8zkagyzy68sd1lz")))

