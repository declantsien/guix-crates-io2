(define-module (crates-io ei ns einsum-codegen) #:use-module (crates-io))

(define-public crate-einsum-codegen-0.1.0 (c (n "einsum-codegen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "insta") (r "^1.21.1") (d #t) (k 2)) (d (n "katexit") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0a4z6bwadlg8i1hykln2w08lhf79akiapydd81angwgbb7nb1zn3")))

