(define-module (crates-io ei ns einsum-derive) #:use-module (crates-io))

(define-public crate-einsum-derive-0.1.0 (c (n "einsum-derive") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "einsum-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "1w572myp92lmgqk48m0vq45xp6gpr9xz0irxjm3qlv8ccswq9h3h")))

