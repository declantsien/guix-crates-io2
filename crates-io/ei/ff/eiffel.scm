(define-module (crates-io ei ff eiffel) #:use-module (crates-io))

(define-public crate-eiffel-0.0.1 (c (n "eiffel") (v "0.0.1") (h "0b9hafz6wrpws41fjvmkv8l5jsrcynnpqrhb53i4m741y2qqzjf2")))

(define-public crate-eiffel-0.0.2 (c (n "eiffel") (v "0.0.2") (d (list (d (n "eiffel-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "eiffel-macros-gen") (r "^0.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "release-plz") (r "^0.3.49") (d #t) (k 2)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1a2g49id7wdpiwm04ysk1nlln2g1rcqig96y8s8r5wsryaw791pk")))

(define-public crate-eiffel-0.0.3 (c (n "eiffel") (v "0.0.3") (d (list (d (n "eiffel-macros") (r ">=0.0.3") (d #t) (k 0)) (d (n "eiffel-macros-gen") (r ">=0.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "release-plz") (r "^0.3.49") (d #t) (k 2)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "01ywq4kvygdfa8fpyshds38vpf9s0xd5qmhl6wxb0zr0h0y454kh")))

(define-public crate-eiffel-0.0.4 (c (n "eiffel") (v "0.0.4") (d (list (d (n "eiffel-macros") (r ">=0.0.3") (d #t) (k 0)) (d (n "eiffel-macros-gen") (r ">=0.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "109p4i7x5davfhlazvi7hcalzpg29iahlb7d8l38if815sv3vlsd")))

