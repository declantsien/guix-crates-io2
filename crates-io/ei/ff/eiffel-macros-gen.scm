(define-module (crates-io ei ff eiffel-macros-gen) #:use-module (crates-io))

(define-public crate-eiffel-macros-gen-0.0.1 (c (n "eiffel-macros-gen") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "13mnibh693cww71nafv049sv6dxv2a1bqv8nc2973lyfwdaa90c5")))

(define-public crate-eiffel-macros-gen-0.0.2 (c (n "eiffel-macros-gen") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1pfs57jvl37552vdk13adj2xlx1wln12by2p5f707vkb76zflmwp")))

(define-public crate-eiffel-macros-gen-0.0.3 (c (n "eiffel-macros-gen") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "181rv3j7lkjn76x3vm3mzc627ghf2z1gx02dph4l1j4jmbrgyhwv")))

(define-public crate-eiffel-macros-gen-0.0.4 (c (n "eiffel-macros-gen") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0m8j2qac725i4p24mm21bnsxz0d73ij68jbx7ddsmrbzcpkgkkar")))

