(define-module (crates-io ei ff eiffel-macros) #:use-module (crates-io))

(define-public crate-eiffel-macros-0.0.1 (c (n "eiffel-macros") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0n9rxczj52zzvxm2c44svizbwgmpjjazjq2vzc7gdsfxfsi444bw")))

(define-public crate-eiffel-macros-0.0.2 (c (n "eiffel-macros") (v "0.0.2") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0a7wvzkqfadqab4v437pkgjva3mzlfn3p0pf07ki9xs6w5b45bcx")))

(define-public crate-eiffel-macros-0.0.3 (c (n "eiffel-macros") (v "0.0.3") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1g4v2bv134bzb00cbihm7v49frxajp3ci38klrzihpdc1nq2ivr6")))

(define-public crate-eiffel-macros-0.0.4 (c (n "eiffel-macros") (v "0.0.4") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0dakx7xba7zqh5bnbfvmw0ybfc0sq6pv120h7cg6nnjb3bhqdsx5")))

