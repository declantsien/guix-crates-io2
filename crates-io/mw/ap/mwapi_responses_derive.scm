(define-module (crates-io mw ap mwapi_responses_derive) #:use-module (crates-io))

(define-public crate-mwapi_responses_derive-0.1.0 (c (n "mwapi_responses_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q2cad7y5ic8a6icsp9iy7sm42hqmc27y8ivp7zg9km7n5ch8865")))

(define-public crate-mwapi_responses_derive-0.2.0 (c (n "mwapi_responses_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04wblmmr8wmd74silbbklvz8nysxhvdl3dy57iwf0f0x8li5jir6") (r "1.56")))

(define-public crate-mwapi_responses_derive-0.2.1 (c (n "mwapi_responses_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r3w35w1ylwxp21fpjda05kpzjzgsj9yz6rl2j7x9850z4jcd53v") (r "1.56")))

(define-public crate-mwapi_responses_derive-0.2.2 (c (n "mwapi_responses_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yqaxdv51jw1vrhly3hxpbzns7vfnjizfsd61ssr1g1chkhqzii7") (r "1.56")))

(define-public crate-mwapi_responses_derive-0.3.0-alpha.1 (c (n "mwapi_responses_derive") (v "0.3.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17j9ayp28xri6g9dk94p6in8z4rcqqrss46hblsxkwm9id1y9yac") (r "1.56")))

(define-public crate-mwapi_responses_derive-0.3.0-alpha.2 (c (n "mwapi_responses_derive") (v "0.3.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nbs2942z60savh8w1irmp06dc6sxc0k72xa4n0ajsaq39ws37lr") (r "1.56")))

(define-public crate-mwapi_responses_derive-0.3.0-alpha.3 (c (n "mwapi_responses_derive") (v "0.3.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wkz7bsvw1apz6lzmszfs7syimy9a8bkzr63cczv7cag56dg1ni9") (r "1.56")))

(define-public crate-mwapi_responses_derive-0.3.0-alpha.4 (c (n "mwapi_responses_derive") (v "0.3.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wnxy8dx6ffn0p7m2vd1y9gri065w66g46czlvd9kp7h4gbg3rp8") (r "1.59")))

(define-public crate-mwapi_responses_derive-0.3.0 (c (n "mwapi_responses_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nk1d0lp010jch69xm5hsil6mc67jl5h03bai615gs4qxhma1mxn") (r "1.60")))

(define-public crate-mwapi_responses_derive-0.3.1 (c (n "mwapi_responses_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q9hid8qw5g2dxq6gh66hrmpfgxrls48nrv5l2543zqaypfa5ihp") (r "1.60")))

(define-public crate-mwapi_responses_derive-0.3.2 (c (n "mwapi_responses_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zf75wa7a6c0an91p4anp1dlrpcsybhsp2c95206nfd9wjiwgzwk") (r "1.63")))

(define-public crate-mwapi_responses_derive-0.3.3 (c (n "mwapi_responses_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rlwkp51xi6qb53gmlygj0avijj6a747f8p0r8vh7qcfxnk68k3r") (r "1.63")))

(define-public crate-mwapi_responses_derive-0.3.4 (c (n "mwapi_responses_derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "013wfg0sgn0v5idgwaz8kxfwqbjpgw7156zljgs89kfm54mcqna8") (r "1.65")))

(define-public crate-mwapi_responses_derive-0.4.0 (c (n "mwapi_responses_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "1njf3hhfkvs1ifwnlj982vjmcqp0gvl2033bq4cj2wbn9rjxzmqj") (r "1.67")))

(define-public crate-mwapi_responses_derive-0.4.1 (c (n "mwapi_responses_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "1910qb6j3l8y2xb9c10zk0rj1ydc8mjig2rg905x5lb69hhmq3qz") (r "1.67")))

(define-public crate-mwapi_responses_derive-0.4.2 (c (n "mwapi_responses_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (d #t) (k 0)))) (h "1fi7nxcckamzf3b957l9lrs1gqnf4shnspqysdi9r677p7la52qj") (r "1.67")))

