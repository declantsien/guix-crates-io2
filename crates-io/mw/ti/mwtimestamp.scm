(define-module (crates-io mw ti mwtimestamp) #:use-module (crates-io))

(define-public crate-mwtimestamp-0.1.0 (c (n "mwtimestamp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std" "clock" "serde"))) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0yhfn4sqd8sp5s4q6hzdn9r0zk5va13v3fw5487iiyxq6llirfqp") (r "1.67")))

