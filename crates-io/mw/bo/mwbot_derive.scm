(define-module (crates-io mw bo mwbot_derive) #:use-module (crates-io))

(define-public crate-mwbot_derive-0.5.0 (c (n "mwbot_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.20") (d #t) (k 0)))) (h "1878rq8lr6y59fnbsxp7lxahx4apknxnkrc3clwngavnaak1wzzg") (r "1.65")))

(define-public crate-mwbot_derive-0.6.0 (c (n "mwbot_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.20") (d #t) (k 0)))) (h "1qryl3n16ayzapw6p7jss8szcygdc1hyi2j0l2p0dz7c6j3gsfpa") (r "1.67")))

