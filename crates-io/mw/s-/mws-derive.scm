(define-module (crates-io mw s- mws-derive) #:use-module (crates-io))

(define-public crate-mws-derive-0.1.0 (c (n "mws-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "09cr6av4g0dzdbswdqr4vkh1d3ghylls0b785r4k2mdzi2amg58n")))

(define-public crate-mws-derive-0.9.0 (c (n "mws-derive") (v "0.9.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1c56rlyx6dvr79r2sjij56k4mb283p99wa78jbzyy8nsfbyv31s2")))

(define-public crate-mws-derive-0.12.0 (c (n "mws-derive") (v "0.12.0") (d (list (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cfdyvdlrx7x5ax9d0w1gngzyym6pvxbgb9nc7jmycmn7k52s8xv")))

