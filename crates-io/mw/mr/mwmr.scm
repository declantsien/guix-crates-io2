(define-module (crates-io mw mr mwmr) #:use-module (crates-io))

(define-public crate-mwmr-0.0.0 (c (n "mwmr") (v "0.0.0") (h "0g27gsc8w9rbc3063h98dk6j7jfpsh8nqzv815arx8i7x1nx512w")))

(define-public crate-mwmr-0.1.0 (c (n "mwmr") (v "0.1.0") (d (list (d (n "cheap-clone") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (k 0)) (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2") (f (quote ("default"))) (k 0)) (d (n "mwmr-core") (r "^0.1.0") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1") (f (quote ("use_std"))) (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wmark") (r "^0.1.0") (f (quote ("default"))) (k 0)))) (h "0xd5h0vy35k37xmlyc2nzcx0mlxsp80mmsfgwbv1hc09msgh5pxn") (f (quote (("test" "rand") ("default"))))))

