(define-module (crates-io mw mr mwmr-core) #:use-module (crates-io))

(define-public crate-mwmr-core-0.1.0 (c (n "mwmr-core") (v "0.1.0") (d (list (d (n "cheap-clone") (r "^0.1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2") (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (d #t) (k 0)))) (h "0hb010nszrm7y4ki694z20zvin6fcvm1sqgzrj3gfqxvzdvazs0y")))

