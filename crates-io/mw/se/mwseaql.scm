(define-module (crates-io mw se mwseaql) #:use-module (crates-io))

(define-public crate-mwseaql-0.1.0 (c (n "mwseaql") (v "0.1.0") (d (list (d (n "sea-query") (r "^0.26.3") (f (quote ("derive"))) (k 0)))) (h "1zzyzvwykdv3rjsg8lg778sapxm2ffm1jyy1s9xvqwbvr0svbqaw") (f (quote (("toolforge") ("proofread_page") ("page_assessments") ("linter")))) (r "1.60")))

(define-public crate-mwseaql-0.1.1 (c (n "mwseaql") (v "0.1.1") (d (list (d (n "sea-query") (r "^0.26.3") (f (quote ("derive"))) (k 0)))) (h "00xavhm1mwb8a37cr472c26n15lhgrf1j6z1wdnm9xqk8rynyn06") (f (quote (("toolforge") ("proofread_page") ("page_assessments") ("linter")))) (r "1.60")))

(define-public crate-mwseaql-0.1.2 (c (n "mwseaql") (v "0.1.2") (d (list (d (n "sea-query") (r "^0.26.3") (f (quote ("derive" "backend-mysql"))) (k 0)))) (h "06msb53zqp7jvbc3bm6a41xkvxr7lr2mrlrabqmicpk8vnxmrqiq") (f (quote (("toolforge") ("proofread_page") ("page_assessments") ("linter")))) (r "1.60")))

(define-public crate-mwseaql-0.2.0 (c (n "mwseaql") (v "0.2.0") (d (list (d (n "sea-query") (r "^0.27.1") (f (quote ("derive" "backend-mysql"))) (k 0)))) (h "18c9sws2as3456r14ikvjqhh51696rals4w9ig3qyahf9fh8jzl3") (f (quote (("toolforge") ("proofread_page") ("page_assessments") ("linter")))) (r "1.60")))

(define-public crate-mwseaql-0.3.0 (c (n "mwseaql") (v "0.3.0") (d (list (d (n "sea-query") (r "^0.28.2") (f (quote ("derive" "backend-mysql"))) (k 0)))) (h "1fbnd8fm5hzlc5msqknw60bc6v7nwaqqxbksn86j9ccmb7w7by3x") (f (quote (("toolforge") ("proofread_page") ("page_assessments") ("linter")))) (r "1.62")))

