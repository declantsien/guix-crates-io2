(define-module (crates-io mw ma mwmatching) #:use-module (crates-io))

(define-public crate-mwmatching-0.1.0 (c (n "mwmatching") (v "0.1.0") (h "0zsn7yh2y50x0cfalr5yv97sqj43dsvav99amnbaw7yk51a9fk2m")))

(define-public crate-mwmatching-0.1.1 (c (n "mwmatching") (v "0.1.1") (h "01nwwx4x66qjwg01fl123arzqhb26fp7d2lk5k63d1wqim250fzi")))

