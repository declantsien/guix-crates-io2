(define-module (crates-io mw ca mwcas) #:use-module (crates-io))

(define-public crate-mwcas-0.1.0 (c (n "mwcas") (v "0.1.0") (d (list (d (n "crossbeam-epoch") (r "^0.9.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0kvwcvafcksdn34qnvnd9ykxbdbs4cqxvm3fxw2y94chbk09ax2f")))

(define-public crate-mwcas-0.2.0 (c (n "mwcas") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "14jvji6gsi1wdl9mn8jxx04rgakp730laislrp5g00bxybq916dn")))

