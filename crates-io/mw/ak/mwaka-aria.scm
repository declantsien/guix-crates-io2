(define-module (crates-io mw ak mwaka-aria) #:use-module (crates-io))

(define-public crate-mwaka-aria-0.0.1 (c (n "mwaka-aria") (v "0.0.1") (h "1m2qragxd0kcg54ax1l464g94i02b0jhm5rxkb1mbqii1r7xbii2")))

(define-public crate-mwaka-aria-0.0.2 (c (n "mwaka-aria") (v "0.0.2") (h "0asw8hhpgi251a1pl3z84dcby3yvknn8fhjbindnlzpf2dpsblrz")))

