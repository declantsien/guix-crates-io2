(define-module (crates-io mw ak mwaka) #:use-module (crates-io))

(define-public crate-mwaka-0.0.1 (c (n "mwaka") (v "0.0.1") (d (list (d (n "mwaka-aria") (r "^0.0.1") (d #t) (k 0)))) (h "02km59h1gy5d7sblni0w7kwznh1r1dvj1sz1s4i27by61gljxvbb")))

(define-public crate-mwaka-0.0.2 (c (n "mwaka") (v "0.0.2") (d (list (d (n "mwaka-aria") (r "^0.0.2") (d #t) (k 0)))) (h "1b8jylc90a3ldxk005i67h683n8rqcwm5a9jl8ng9ld72k250i6i")))

