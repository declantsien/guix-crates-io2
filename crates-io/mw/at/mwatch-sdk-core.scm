(define-module (crates-io mw at mwatch-sdk-core) #:use-module (crates-io))

(define-public crate-mwatch-sdk-core-0.1.0 (c (n "mwatch-sdk-core") (v "0.1.0") (d (list (d (n "ssd1351") (r "^0.1.4") (d #t) (k 0)) (d (n "stm32l432xx-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0svqp7v31rh24iqah9s6prg2209fpm6d2y578236cbgphp94llfv")))

