(define-module (crates-io mw at mwatch-send) #:use-module (crates-io))

(define-public crate-mwatch-send-0.1.0 (c (n "mwatch-send") (v "0.1.0") (d (list (d (n "blurz") (r "^0.4.0") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "simple-hex") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "04b2vywxcz8an2ik5nv70yzr3fpi6ws3q2fnc6g62zpd895cakji")))

(define-public crate-mwatch-send-0.1.1 (c (n "mwatch-send") (v "0.1.1") (d (list (d (n "blurz") (r "^0.4.0") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "simple-hex") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "094nzlqzlj5ypxhzgx319i4yavmccari1z4396x0fcxr5rzykwgd")))

(define-public crate-mwatch-send-0.1.2 (c (n "mwatch-send") (v "0.1.2") (d (list (d (n "blurz") (r "^0.4.0") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "simple-hex") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0wyvnl79yn6yz482swrarnh34r6q96aid5y495kkwm7smkk6vzbf")))

(define-public crate-mwatch-send-0.2.0 (c (n "mwatch-send") (v "0.2.0") (d (list (d (n "blurz") (r "^0.4.0") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "simple-hex") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "059q27vljqq3r631qpbidf3gm5wsnv6y7jpgk8aj3vgdjykdjgjl")))

