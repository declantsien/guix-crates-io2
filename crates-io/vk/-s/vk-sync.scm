(define-module (crates-io vk -s vk-sync) #:use-module (crates-io))

(define-public crate-vk-sync-0.1.0 (c (n "vk-sync") (v "0.1.0") (d (list (d (n "ash") (r "^0.24.4") (o #t) (d #t) (k 0)))) (h "05mqcqzwq9lrkjrmz2asds4qr8waqjhqcirkisn5ijbiqlikm9c7") (f (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1.1 (c (n "vk-sync") (v "0.1.1") (d (list (d (n "ash") (r "^0.25") (o #t) (d #t) (k 0)))) (h "05g5pwnp79wanqc7zv0kmlnxcnxig2gbsb96xjvkdqxcmayjcilr") (f (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1.2 (c (n "vk-sync") (v "0.1.2") (d (list (d (n "ash") (r "^0.26") (o #t) (d #t) (k 0)))) (h "16fdvag03g9alh2npsmq9qkm20wwksxg1n4ihhqzl1rhn7g0fsfg") (f (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1.3 (c (n "vk-sync") (v "0.1.3") (d (list (d (n "ash") (r "^0.26") (o #t) (d #t) (k 0)))) (h "0nw03895x8b05v38syd5241zdg12nc1rq3m433rdwxi4074dqzvc") (f (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1.4 (c (n "vk-sync") (v "0.1.4") (d (list (d (n "ash") (r "^0.26") (o #t) (d #t) (k 0)))) (h "1j63xxbi8jwg4zafhk1bh38fan6dg27ycyfkx3m6xk7kx0yvy32n") (f (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1.5 (c (n "vk-sync") (v "0.1.5") (d (list (d (n "ash") (r "^0.29") (o #t) (d #t) (k 0)))) (h "0rssi5wri069i1ffcci0djl663ivpshmh9vjmsk3wnnix3yd6xh5") (f (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1.6 (c (n "vk-sync") (v "0.1.6") (d (list (d (n "ash") (r "^0.29") (o #t) (d #t) (k 0)))) (h "16j8wqavxmqp8fgz4inrd95775vb0p893dcckqiqb01l4d6vapqj") (f (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

