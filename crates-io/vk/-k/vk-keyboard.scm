(define-module (crates-io vk -k vk-keyboard) #:use-module (crates-io))

(define-public crate-vk-keyboard-0.1.0 (c (n "vk-keyboard") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bspmzb7yjw2sqydgnaj52663xk7gvk827vbkr4h6m74sggxn8q8")))

(define-public crate-vk-keyboard-0.2.0 (c (n "vk-keyboard") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00lx2c07ij77giy98wx9rjv3j1zqipivdjpzp1lin8bj9y1zap57")))

