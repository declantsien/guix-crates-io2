(define-module (crates-io vk -m vk-mem-alloc) #:use-module (crates-io))

(define-public crate-vk-mem-alloc-0.1.0 (c (n "vk-mem-alloc") (v "0.1.0") (d (list (d (n "ash") (r ">=0.37.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)))) (h "15xdyn4ain1cd5zl77d8cxr94i4g9zb742l2mwg0whzzm47j6jpy") (f (quote (("recording") ("link_vulkan") ("generate_bindings" "bindgen") ("default"))))))

(define-public crate-vk-mem-alloc-0.1.1 (c (n "vk-mem-alloc") (v "0.1.1") (d (list (d (n "ash") (r ">=0.37.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)))) (h "0nd7mnsy0z2cz4y03l70awfjr7g89fl9ac6dm8whv7hwc9lgilc7") (f (quote (("recording") ("link_vulkan") ("generate_bindings" "bindgen") ("default"))))))

(define-public crate-vk-mem-alloc-0.2.0 (c (n "vk-mem-alloc") (v "0.2.0") (d (list (d (n "ash") (r "^0.37.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "vk-mem-alloc-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0m9zb77hf6naqw2q2n3895g07vir77j7rjg419vl8cyp0aig3g8g")))

