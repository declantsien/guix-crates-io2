(define-module (crates-io vk -m vk-mem-erupt) #:use-module (crates-io))

(define-public crate-vk-mem-erupt-0.2.3 (c (n "vk-mem-erupt") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.53.2") (o #t) (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "erupt") (r "^0.19") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0ks0w2cj9ndyl2pbyakj4dp5w4hnz72hqagy9nlfw9ifv2nj2b32") (f (quote (("recording") ("link_vulkan") ("generate_bindings" "bindgen") ("default"))))))

(define-public crate-vk-mem-erupt-0.2.4 (c (n "vk-mem-erupt") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.53.2") (o #t) (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "erupt") (r "^0.20") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "0n2fgsyrq6pg0jlc1pwcnjs9274zdmsccknn180v10fa3k61k6zk") (f (quote (("recording") ("link_vulkan") ("generate_bindings" "bindgen") ("default"))))))

