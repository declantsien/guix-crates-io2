(define-module (crates-io vk -m vk-mem-alloc-sys) #:use-module (crates-io))

(define-public crate-vk-mem-alloc-sys-0.2.0 (c (n "vk-mem-alloc-sys") (v "0.2.0") (d (list (d (n "ash") (r "^0.37.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)))) (h "162prnk3xd22h8s2bhwphk1rsbzrclmaw0m7f3k5rxk65hy52yqk")))

