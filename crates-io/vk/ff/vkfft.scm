(define-module (crates-io vk ff vkfft) #:use-module (crates-io))

(define-public crate-vkfft-0.1.0 (c (n "vkfft") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 2)) (d (n "vk-sys") (r "^0.6") (d #t) (k 0)) (d (n "vkfft-sys") (r "^0.1") (d #t) (k 0)) (d (n "vulkano") (r "^0.22") (d #t) (k 0)))) (h "0w9453dlqn0zlhf24pyl1irlb8x1723x5g62nhbsns6an11mwz1f")))

(define-public crate-vkfft-0.1.1 (c (n "vkfft") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 2)) (d (n "vk-sys") (r "^0.6") (d #t) (k 0)) (d (n "vkfft-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "vulkano") (r "^0.22") (d #t) (k 0)))) (h "0h76q48160arv8zc26djxdjj97zq5dcr8pfchwd3pfh3aq4shspj")))

