(define-module (crates-io vk ff vkfft-sys) #:use-module (crates-io))

(define-public crate-vkfft-sys-0.1.0 (c (n "vkfft-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0hrhv1cfcd9bmr3p7p0i3iqxssw58ysya2f45yn0knyzwxv89bx5")))

(define-public crate-vkfft-sys-0.1.1 (c (n "vkfft-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0d5qr9kidwld1m13cwrr6fqgf7rxd9pn1wi1231ih7w4g8kdzl5w")))

