(define-module (crates-io vk dl vkdl) #:use-module (crates-io))

(define-public crate-vkdl-0.1.0 (c (n "vkdl") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0392baynknb9lm69dnd5xz070cwfii8v7kslngli1m52ha6466jy") (y #t)))

(define-public crate-vkdl-0.1.1 (c (n "vkdl") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07g76f6qld1fb9piggdfx19a73phzhw1vgqf8byggrqacd4wxywx") (y #t)))

(define-public crate-vkdl-0.1.2 (c (n "vkdl") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("stream"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01115is2q6zgjdvpmzvw9br3n4fscvxqqmsnfnzm19431zp5ylyf")))

