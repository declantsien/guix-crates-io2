(define-module (crates-io vk -t vk-token-manager) #:use-module (crates-io))

(define-public crate-vk-token-manager-0.1.0 (c (n "vk-token-manager") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)))) (h "1g1rp9w7amg35khfqsaic98c8wgl7fnppjairqjcsy5c5f8r2cfn")))

(define-public crate-vk-token-manager-0.2.0 (c (n "vk-token-manager") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)))) (h "09jvkr05ixdkf6dvc734951gvyqxiwgdf2mixp2z155qp4xcacy9")))

