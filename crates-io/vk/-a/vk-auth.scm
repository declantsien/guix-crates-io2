(define-module (crates-io vk -a vk-auth) #:use-module (crates-io))

(define-public crate-vk-auth-0.1.0 (c (n "vk-auth") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0h7ijc68g4flprmx2gfp7klzm62zjqk2wxm6a88x1bai7m4jj2lm")))

