(define-module (crates-io vk -p vk-parse) #:use-module (crates-io))

(define-public crate-vk-parse-0.1.0 (c (n "vk-parse") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ron") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1pxgpmdyimfkwwrr1vmk9ydsvgvjaq42f567m2f81idzg46gikx2")))

(define-public crate-vk-parse-0.2.0 (c (n "vk-parse") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "195ydggwkn8m0wjvb7x32ypl56s1warczai7cg9ba1mmldxi5n2r")))

(define-public crate-vk-parse-0.3.0 (c (n "vk-parse") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0lamcnfa7199d3d7yirn2kpvb0kw6i8shifh68zy5wsaskngbh69")))

(define-public crate-vk-parse-0.4.0 (c (n "vk-parse") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "07c3609agc3p2gxfypln431fs0fw0z3a8cyd94fdbwkfzyn8954d")))

(define-public crate-vk-parse-0.5.0 (c (n "vk-parse") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "00md6zqr03p6acc96lh90as8vv4wdhj9r7gi6zbiiybckqllc1rn") (f (quote (("vkxml-convert" "vkxml") ("serialize" "serde" "serde_derive"))))))

(define-public crate-vk-parse-0.6.0 (c (n "vk-parse") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "02im9p35c895y8zpgwvgwr7rln96yhjg15dldrdgnirzg3x3h5cw") (f (quote (("vkxml-convert" "vkxml") ("serialize" "serde" "serde_derive"))))))

(define-public crate-vk-parse-0.7.0 (c (n "vk-parse") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "111yw590jzvrkplvmcqli2v280xn2kbs3njhbww9ynna0pfbzwr9") (f (quote (("vkxml-convert" "vkxml") ("serialize" "serde" "serde_derive"))))))

(define-public crate-vk-parse-0.8.0 (c (n "vk-parse") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0ap3q9g93r211h9s24a33sra7k74iym80v25wr89wsxykgd0nsjc") (f (quote (("vkxml-convert" "vkxml") ("serialize" "serde" "serde_derive"))))))

(define-public crate-vk-parse-0.9.0 (c (n "vk-parse") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1i8ywav5pkn8shvz2pd6bdpx8jpdngrrg0y0i9kcjf8s9p5h4pw7") (f (quote (("vkxml-convert" "vkxml") ("serialize" "serde" "serde_derive"))))))

(define-public crate-vk-parse-0.10.0 (c (n "vk-parse") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0js7nik7al31066wl3w5ccp7ir5wl916dwfsnm9jkry3k4kgik3n") (f (quote (("vkxml-convert" "vkxml") ("serialize" "serde" "serde_derive"))))))

(define-public crate-vk-parse-0.11.0 (c (n "vk-parse") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1kqpg5v72lw9178dfim5amvd2x27a4p1i9ykzgknqx75d0xjcrai") (f (quote (("vkxml-convert" "vkxml") ("serialize" "serde" "serde_derive"))))))

(define-public crate-vk-parse-0.12.0 (c (n "vk-parse") (v "0.12.0") (d (list (d (n "serde") (r "^1.0.77") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.77") (o #t) (d #t) (k 0)) (d (n "vkxml") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0ycfhz8w5jd1smsfpiazc1s0h3jjgf7krfqcv2f7ba37pql6q241") (f (quote (("vkxml-convert" "vkxml") ("serialize" "serde" "serde_derive"))))))

