(define-module (crates-io vk _l vk_llw) #:use-module (crates-io))

(define-public crate-vk_llw-0.0.1 (c (n "vk_llw") (v "0.0.1") (d (list (d (n "ash") (r "^0.31.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0hkhpqvckf19k4q2qlxjwqxxwpfyiqch28cj88xcvv5grn1w0793")))

