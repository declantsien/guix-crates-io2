(define-module (crates-io vk #{2d}# vk2dfd) #:use-module (crates-io))

(define-public crate-vk2dfd-0.1.0 (c (n "vk2dfd") (v "0.1.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qynfs9rlvyngz3m23ss07lvdvsq5y8afwxxg1yr0rhfdynxhy12") (r "1.70")))

