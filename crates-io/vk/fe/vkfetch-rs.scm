(define-module (crates-io vk fe vkfetch-rs) #:use-module (crates-io))

(define-public crate-vkfetch-rs-0.0.1 (c (n "vkfetch-rs") (v "0.0.1") (d (list (d (n "ash") (r "^0.38.0") (d #t) (k 0)))) (h "0f5s9b3jwd7n01bg8vhmiq82c0q1n4zv9vjp46gssx28rvqraa60")))

(define-public crate-vkfetch-rs-0.0.2 (c (n "vkfetch-rs") (v "0.0.2") (d (list (d (n "ash") (r "^0.38.0") (d #t) (k 0)))) (h "0fn98inyzw15sds9ip8qjz21k4rjm0w1l2idish14wjh4l8nycp3")))

(define-public crate-vkfetch-rs-0.0.3 (c (n "vkfetch-rs") (v "0.0.3") (d (list (d (n "ash") (r "^0.38.0") (d #t) (k 0)))) (h "0i9vk8b760b5an7hmqaq1l61x2qs212nnr6rfafn772y6l92grqk")))

(define-public crate-vkfetch-rs-0.0.4 (c (n "vkfetch-rs") (v "0.0.4") (d (list (d (n "ash") (r "^0.38.0") (d #t) (k 0)))) (h "1s8m4wjkpslrjw425qpswxz54hniwcbz9m1bx1np8py3mjzfmbba")))

(define-public crate-vkfetch-rs-0.0.5 (c (n "vkfetch-rs") (v "0.0.5") (d (list (d (n "ash") (r "^0.38.0") (d #t) (k 0)))) (h "0zf8bhk1ibpsnw5irxcq6spqqnzd2ws6908lib6pn81qwxxyr9rl")))

(define-public crate-vkfetch-rs-0.0.6 (c (n "vkfetch-rs") (v "0.0.6") (d (list (d (n "ash") (r "^0.38.0") (f (quote ("std"))) (k 0)))) (h "0ig5cvb6v2r0iavv5q9vacl1jpg4kv6s54j0iinwgcg8n1xgky3c") (f (quote (("loaded" "ash/loaded") ("linked" "ash/linked") ("default" "linked"))))))

(define-public crate-vkfetch-rs-0.1.0 (c (n "vkfetch-rs") (v "0.1.0") (d (list (d (n "ash") (r "^0.38.0") (f (quote ("std"))) (k 0)))) (h "0b82vq8afyc87lc5jahvaadz5cy3lrdxjqqw1zjrilizrx9wcgdi") (f (quote (("loaded" "ash/loaded") ("linked" "ash/linked") ("default" "linked"))))))

