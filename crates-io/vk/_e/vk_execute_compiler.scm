(define-module (crates-io vk _e vk_execute_compiler) #:use-module (crates-io))

(define-public crate-vk_execute_compiler-0.1.0 (c (n "vk_execute_compiler") (v "0.1.0") (d (list (d (n "fast_vk") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g7pigc8i8v2cqp8j3fb49x7fw0qiz8gg9cw57r6p5m6q6wmpvmc")))

(define-public crate-vk_execute_compiler-0.1.1 (c (n "vk_execute_compiler") (v "0.1.1") (d (list (d (n "ijson") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "vk_method") (r "^0.1") (d #t) (k 0)))) (h "0rrwx8ny5wbmic2x5ika1lyig4ds10d6a0j92pdjfblrzinys7h1")))

(define-public crate-vk_execute_compiler-0.1.2 (c (n "vk_execute_compiler") (v "0.1.2") (d (list (d (n "ijson") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "vk_method") (r "^0.2") (d #t) (k 0)))) (h "0ypx1l0wbkwn0g0a1shbywwg0x2v4fzn9zlyacs0fak0na12aqfz")))

(define-public crate-vk_execute_compiler-0.1.3 (c (n "vk_execute_compiler") (v "0.1.3") (d (list (d (n "ijson") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "vk_method") (r "^0.2") (d #t) (k 0)))) (h "1mlrcd2v6lkdsc28pna1fzwnwgyff9c0x7n9wigg25iqqa56g5x6")))

