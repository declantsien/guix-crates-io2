(define-module (crates-io vk op vkopt-message-parser) #:use-module (crates-io))

(define-public crate-vkopt-message-parser-0.1.0 (c (n "vkopt-message-parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07b4nivig2cvq6f27spvb0drq2v4ckyl0bydyzgv28zhyh2ysa3y")))

(define-public crate-vkopt-message-parser-0.3.0 (c (n "vkopt-message-parser") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.18") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1313yn6rchx3s6zskhysg3f4zdx4mc6smgvywhpfmgiac9z7wby2")))

