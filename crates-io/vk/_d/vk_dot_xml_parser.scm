(define-module (crates-io vk _d vk_dot_xml_parser) #:use-module (crates-io))

(define-public crate-vk_dot_xml_parser-0.1.0 (c (n "vk_dot_xml_parser") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "magnesium") (r "^1.2.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (k 0)))) (h "02v98dpyvjkvr2rii6z900hdsi21rnaw9jamxmy977y4yclrz709")))

(define-public crate-vk_dot_xml_parser-0.2.0 (c (n "vk_dot_xml_parser") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "magnesium") (r "^1.2.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (k 0)))) (h "0zh920ahq7qxgvbpj8mpah4kq4vfps5073wchi0jfm8w0m8pwlml")))

(define-public crate-vk_dot_xml_parser-0.2.1 (c (n "vk_dot_xml_parser") (v "0.2.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "magnesium") (r "^1.2.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (k 2)))) (h "00jrzrfyiprdw56gx3sbrkmm78bsfwjgllpgi92g5m8zgdnjr431")))

(define-public crate-vk_dot_xml_parser-0.2.2 (c (n "vk_dot_xml_parser") (v "0.2.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "magnesium") (r "^1.2.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (k 2)))) (h "1qja834zhfjsb7pch93skmxj11vbgzqhwiwkcb6s50gankq91l7r") (y #t)))

(define-public crate-vk_dot_xml_parser-0.3.0 (c (n "vk_dot_xml_parser") (v "0.3.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "magnesium") (r "^1.2.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.4") (k 2)))) (h "10gpgli8485584cjscqd0r8s3p1hfvwl8146gj0k4224v2smbj6s")))

