(define-module (crates-io vk _d vk_deps) #:use-module (crates-io))

(define-public crate-vk_deps-0.1.0 (c (n "vk_deps") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.22") (d #t) (k 0)) (d (n "dirs") (r "^2.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.7") (d #t) (k 0)) (d (n "tar") (r "^0.4.2") (d #t) (k 0)))) (h "0i5yq7w502s6fyg1d58qv2gwnaavq0p66scr3hpclbnl7wqyrm6r")))

