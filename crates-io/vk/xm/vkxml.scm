(define-module (crates-io vk xm vkxml) #:use-module (crates-io))

(define-public crate-vkxml-0.3.0 (c (n "vkxml") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)))) (h "1qhvx77bi3gj9wls4167w0pfbbrkr5yysc1jd5dymraqngga1wy7")))

(define-public crate-vkxml-0.3.1 (c (n "vkxml") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)))) (h "1fnjw3srqzkyfhs8p82s8xnww6psxsykdsrspqgmvjn04g1gig8z")))

