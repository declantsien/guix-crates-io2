(define-module (crates-io vk _g vk_generator) #:use-module (crates-io))

(define-public crate-vk_generator-0.0.1 (c (n "vk_generator") (v "0.0.1") (h "0ag9ic2zznpprkfkpa23b8bgsfghqrr4j1iviylbj7vc9qs2zzql") (y #t)))

(define-public crate-vk_generator-0.1.0 (c (n "vk_generator") (v "0.1.0") (d (list (d (n "boolinator") (r "^0.1.0") (d #t) (k 0)) (d (n "vk_api") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "1gzg5mxmidzqc8y0c4diips21b9wnqw5a7widc0dhya6yz4n5baa") (f (quote (("unstable_generator_api")))) (y #t)))

(define-public crate-vk_generator-0.1.1 (c (n "vk_generator") (v "0.1.1") (d (list (d (n "boolinator") (r "^0.1.0") (d #t) (k 0)) (d (n "vk_api") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "04x79qr1xpmf6fzp3h7w6i1vr2za2s5c0yia3jrv9f7pi10m9aba") (f (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.1.2 (c (n "vk_generator") (v "0.1.2") (d (list (d (n "boolinator") (r "^0.1.0") (d #t) (k 0)) (d (n "vk_api") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "11iw9lxhlqiqkmdz1638642n2xcbb55l3mzqcz590injx9y7har1") (f (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.1.3 (c (n "vk_generator") (v "0.1.3") (d (list (d (n "boolinator") (r "^0.1.0") (d #t) (k 0)) (d (n "vk_api") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "132kj0mmy9r2rh24yxib1cpmbzsgyxanqcca7ipdczkg2kh8mg8k") (f (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.2.0 (c (n "vk_generator") (v "0.2.0") (d (list (d (n "boolinator") (r "^0.1.0") (d #t) (k 0)) (d (n "vk_api") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0nxlr00kx3979cbg0cwx9bxdlvvg20vpi6dglrrfzczc1r9q1ff7") (f (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.2.1 (c (n "vk_generator") (v "0.2.1") (d (list (d (n "boolinator") (r "^0.1.0") (d #t) (k 0)) (d (n "vk_api") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0bcw5dl9pm9vxvik0q2jhw0hnvcnfs6j8fgc5yiklmq3rf0jkz9i") (f (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.2.2 (c (n "vk_generator") (v "0.2.2") (d (list (d (n "boolinator") (r "^0.1.0") (d #t) (k 0)) (d (n "vk_api") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0g5x6zvbxnl3l472rb5a0sc64axcmnxm2h5j3y5yl60ax6aj9l4y") (f (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.3.0 (c (n "vk_generator") (v "0.3.0") (d (list (d (n "boolinator") (r "^0.1.0") (d #t) (k 0)) (d (n "vk_api") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "1n7fqvkcszhj1qa19dsinigq1q4phcs3cjlf21mjdrqck8nbr7w9") (f (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.3.1 (c (n "vk_generator") (v "0.3.1") (d (list (d (n "boolinator") (r "^0.1.0") (d #t) (k 0)) (d (n "vk_api") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0yz4r3m07rl0pf90gsf90n20y8lp3j3x8vbakili177j3p5isi1x") (f (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.3.2 (c (n "vk_generator") (v "0.3.2") (d (list (d (n "boolinator") (r "^0.1.0") (d #t) (k 0)) (d (n "vk_api") (r "^1.0") (d #t) (k 2)) (d (n "xml-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0xhxi6vgd0w0zv7ncz8p285rrv2b841s7fz28v3bf2grssgmzld1") (f (quote (("unstable_generator_api"))))))

