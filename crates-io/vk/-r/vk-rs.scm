(define-module (crates-io vk -r vk-rs) #:use-module (crates-io))

(define-public crate-vk-rs-0.1.0 (c (n "vk-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef" "winuser" "libloaderapi"))) (d #t) (k 0)))) (h "1cvrp9gdj2j7zy09r10r6kjlka37qn3iq73b78agxd7jj82qgm2n") (y #t)))

(define-public crate-vk-rs-0.1.1 (c (n "vk-rs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef" "winuser" "libloaderapi"))) (d #t) (k 0)))) (h "0w00d5zlm376b89cpkl41njdiahc10jzmns3ss1b2wgf2hws9rb7") (y #t)))

(define-public crate-vk-rs-0.1.2 (c (n "vk-rs") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef" "winuser" "libloaderapi"))) (d #t) (k 2)))) (h "0flk1dhvwmq96y1hda9lbcwymqffm7qss996h60v4wrb4mndgi7r")))

(define-public crate-vk-rs-0.1.3 (c (n "vk-rs") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef" "winuser" "libloaderapi"))) (o #t) (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (o #t) (d #t) (k 0)))) (h "1zq9dfjqhdfg67imi4xbmakcdpmwwwl0hhkfl0bx9bd7jpb48al9") (f (quote (("xlib" "x11") ("xcb") ("win32" "winapi") ("wayland") ("android"))))))

(define-public crate-vk-rs-0.1.4 (c (n "vk-rs") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("windef" "winuser" "libloaderapi"))) (o #t) (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (o #t) (d #t) (k 0)))) (h "08vydq3psvds1h76241bgf3b03c2vijxy58i1bgaj7mywqj2jvfa") (f (quote (("xlib" "x11") ("xcb") ("win32" "winapi") ("wayland") ("android"))))))

