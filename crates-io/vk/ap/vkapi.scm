(define-module (crates-io vk ap vkapi) #:use-module (crates-io))

(define-public crate-vkapi-0.1.0 (c (n "vkapi") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1h68bmibh1vih9y8rysd1x83k0qdci4016aypihp56l3ss6qwr82")))

(define-public crate-vkapi-0.2.0 (c (n "vkapi") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json" "blocking" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.3.1") (d #t) (k 0)))) (h "0smx70amgxkfb0dag9abj7l2npgrcas3gii4lzhah8r58v11ahw9")))

