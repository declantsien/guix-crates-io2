(define-module (crates-io vk ap vkapi2) #:use-module (crates-io))

(define-public crate-vkapi2-0.1.0 (c (n "vkapi2") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0jf98m6klhv2cifjmyh5n6bqhbdripps8xphcwm1888m9aaajdwr") (y #t)))

(define-public crate-vkapi2-0.1.1 (c (n "vkapi2") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1g0q9lwc1hx4z072wbw6r5z5i58wpc418a7m151brjxzr97rbcba") (y #t)))

(define-public crate-vkapi2-0.1.2 (c (n "vkapi2") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1v0fix72a7p6va14nnk4jylxlfi51i9v4h945hx4a9q4z8zlpgrr") (y #t)))

(define-public crate-vkapi2-0.1.3 (c (n "vkapi2") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1kgxrcpb8gfbnark943yzcyr14ddwnwr5i6ydd139abpqz7avgkc") (y #t)))

(define-public crate-vkapi2-0.2.0 (c (n "vkapi2") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0m4bxz17qzbaa5q8g0kyi3zd41vjb3w3429r9h3ms33lfjmkdmsc") (y #t)))

(define-public crate-vkapi2-0.2.1 (c (n "vkapi2") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0sy0bv1aq0pd6vdg1rq5iqycg4z79nv6494ynr6vndf9rr8xb8fc") (y #t)))

(define-public crate-vkapi2-0.2.2 (c (n "vkapi2") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1mqxccm2ka7d8i0pjvbx0klckynvw8cj3ci9vi6dngchwc03jrb0") (y #t)))

(define-public crate-vkapi2-0.2.3 (c (n "vkapi2") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1sdsi9y4axpm27cn30ih5513ixx139rdyr6hkjjj7982jgl1fc2a") (y #t)))

(define-public crate-vkapi2-0.2.4 (c (n "vkapi2") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1wm16sc56krqghjgcx5g4yngh3wrapyknsi84392mvkncyqah968") (y #t)))

(define-public crate-vkapi2-0.2.5 (c (n "vkapi2") (v "0.2.5") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0a1apvcky70z17zyygv83p6qajmghnj7jzb9nkmpi449564mrdzr") (y #t)))

(define-public crate-vkapi2-0.2.6 (c (n "vkapi2") (v "0.2.6") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "04yxjz25f253ffvbw1957rg2r8vqkq2pbi8f1874d2mxrpqg1bdl") (y #t)))

(define-public crate-vkapi2-0.2.7 (c (n "vkapi2") (v "0.2.7") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1xgjn5hagdldmzc99fss2wnbi0ikv4i2rs5gjhbj6j5kj86gmwjc") (y #t)))

(define-public crate-vkapi2-0.2.9 (c (n "vkapi2") (v "0.2.9") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "129dpajkvpn1jwjshf93agnskpnn02cbwrjypqp2whzazxp6456d") (y #t)))

(define-public crate-vkapi2-0.2.10 (c (n "vkapi2") (v "0.2.10") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "02092wz9lf93dikjrd1rz0v05qcnigr4449anfjrb3zzjlf19rj9") (y #t)))

(define-public crate-vkapi2-0.2.11 (c (n "vkapi2") (v "0.2.11") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0gsw1z3a28z4ckcmhvig7cxym12w1hlr9ipyqn9klnzahccvy6gs") (y #t)))

(define-public crate-vkapi2-0.2.12 (c (n "vkapi2") (v "0.2.12") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "06r2biz2d3rdml1x9wsgccg4jqzzflxwjgji2k5x72pyslrjsvkx") (y #t)))

(define-public crate-vkapi2-0.2.13 (c (n "vkapi2") (v "0.2.13") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0siwc4i9py6ib6jxs8s4iaqq6fn5h88b37rxyv07mm817y2sw1ia") (y #t)))

(define-public crate-vkapi2-0.2.14 (c (n "vkapi2") (v "0.2.14") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0lyjyl14rv4h56fd3l28p60x86jlg3vswgmia3b0wp3smgj70ig4") (y #t)))

(define-public crate-vkapi2-0.2.15 (c (n "vkapi2") (v "0.2.15") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1n05sb5ijwbch6l6y0wyyj3wa5gwz6qkpjn14rznq09539avzgxw") (y #t)))

(define-public crate-vkapi2-0.3.0 (c (n "vkapi2") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0ffsin87xnr9njsn5bjh3chn8723av944r7aj9397d30s3zp35y5") (y #t)))

(define-public crate-vkapi2-0.3.1 (c (n "vkapi2") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0545pnrclmx9kdxv1006n4cizknmfmklkpi2djff7sis1b5v3hgl") (y #t)))

(define-public crate-vkapi2-0.3.2 (c (n "vkapi2") (v "0.3.2") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "195pqqzw79brq7zjp05nsnzlj8xrwzyx0xrykp252jcjmg9mnc4n") (y #t)))

(define-public crate-vkapi2-0.3.3 (c (n "vkapi2") (v "0.3.3") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0p3czkcnfyb8ran2wz48rq2qk21bls1ap672mlibk0hk7xzqiq0q") (y #t)))

(define-public crate-vkapi2-0.3.4 (c (n "vkapi2") (v "0.3.4") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0yxk6gp339rhz25gv15b837hfmnd1mmrykxif2cr7i7x7x2g0zhm") (y #t)))

(define-public crate-vkapi2-0.3.5 (c (n "vkapi2") (v "0.3.5") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1h4w07x2qk5bvx5h28qr6jfifxj6yjlp8riw4byv1cl7dwhrb9p9") (y #t)))

(define-public crate-vkapi2-0.3.6 (c (n "vkapi2") (v "0.3.6") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0y6b41934bk0h9kkpgkl0crzjazpnr4mddxdfa6sl61684lszj87") (y #t)))

(define-public crate-vkapi2-0.3.7 (c (n "vkapi2") (v "0.3.7") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0wf5rq9798nlbbhgk9czlgvi7lvhm6pzs7drhzkkfifsrld522y4") (y #t)))

(define-public crate-vkapi2-0.3.8 (c (n "vkapi2") (v "0.3.8") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0q8r0vfs4hj767m00r69s6jjy7bd2yc7viwx74d3mk61qpxy6lxm") (y #t)))

(define-public crate-vkapi2-0.3.9 (c (n "vkapi2") (v "0.3.9") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "02qlw3l8pnilyb7mrhwphbkyrv7wjrhh51d7lr31sh4xj80fz2qh") (y #t)))

(define-public crate-vkapi2-0.4.0 (c (n "vkapi2") (v "0.4.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "168ysm0jygx1rmsgka9yzpksdkkqr7sfg9b4bkljcmjglxrrc9r0") (y #t)))

(define-public crate-vkapi2-0.4.1 (c (n "vkapi2") (v "0.4.1") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0aq00vm89b7m5xn4xahwfh75bzz9306apfppc6pcl47srm64zqqh")))

(define-public crate-vkapi2-0.4.2 (c (n "vkapi2") (v "0.4.2") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "07n4245p7ip2xi2zhpvyml9cp4czmx3289j3wqck34b3v782h55s")))

