(define-module (crates-io vk on vkontakte_api) #:use-module (crates-io))

(define-public crate-vkontakte_api-0.0.1 (c (n "vkontakte_api") (v "0.0.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1p9j1iwb2y1kly0jfckg46ak14r3nnq0dv5bxnddnnz94p0v1pgc")))

(define-public crate-vkontakte_api-0.0.2 (c (n "vkontakte_api") (v "0.0.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0grmkkfbqv88s350qxwpsizn91l9d1xy4mdp18cv2dn1ms0k256m")))

(define-public crate-vkontakte_api-0.0.3 (c (n "vkontakte_api") (v "0.0.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "19kbbrcds5p7bkv6dbq0szswmw70s966ac8fa36sgvw0fnndb725")))

(define-public crate-vkontakte_api-0.0.4 (c (n "vkontakte_api") (v "0.0.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1rkzlpjc1fmnlvygpv7b8nxajyf3xb9nd0abm7mnw1srckl8b98w")))

(define-public crate-vkontakte_api-0.0.5 (c (n "vkontakte_api") (v "0.0.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1j88rj126kinlccbj8lvn5rphz8vq4hlpy0hfgvrvwby4n1arn1z")))

(define-public crate-vkontakte_api-0.0.6 (c (n "vkontakte_api") (v "0.0.6") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0f39n0lg36k6dbamn5595x2a96qhgh2ypr2m4zzq7np20g7mnmw6")))

