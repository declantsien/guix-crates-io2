(define-module (crates-io vk _m vk_method) #:use-module (crates-io))

(define-public crate-vk_method-0.1.1 (c (n "vk_method") (v "0.1.1") (d (list (d (n "ijson") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14n30fhpfqjyrw4p0dsc2zagbirh5mry42z1yw2lzjmb6x2hd73i")))

(define-public crate-vk_method-0.1.2 (c (n "vk_method") (v "0.1.2") (d (list (d (n "ijson") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zl831ybrf78v0n8hij74l64r8yjvicyvv2h1zrmfwjh52jhyk92")))

(define-public crate-vk_method-0.1.3 (c (n "vk_method") (v "0.1.3") (d (list (d (n "ijson") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "007cgs4x2839npwk68ii2308s6n1bzcsg1h37rarbnippmwy3wqm")))

(define-public crate-vk_method-0.1.4 (c (n "vk_method") (v "0.1.4") (d (list (d (n "ijson") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10q4s9n49niwxfsvnvspd9rfk2pj6bqwcifnfxldcwicr6xn1ah5")))

(define-public crate-vk_method-0.1.5 (c (n "vk_method") (v "0.1.5") (d (list (d (n "ijson") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "039nlv189gbnnmnsvxwviwvzsik0aksy911wbysm1hrz1xnllck6")))

(define-public crate-vk_method-0.2.0 (c (n "vk_method") (v "0.2.0") (d (list (d (n "ijson") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fnz0mwlvgba37hpn70nbxlyl6ik0r61nfxncx2cssvz948alada")))

(define-public crate-vk_method-0.2.1 (c (n "vk_method") (v "0.2.1") (d (list (d (n "ijson") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w3vbm0w41h6ca4k57s7aw8sv4wrgfzy67fwx3v4jbc5yxdfahk3")))

