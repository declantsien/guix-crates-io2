(define-module (crates-io vk ge vkgen) #:use-module (crates-io))

(define-public crate-vkgen-0.1.0 (c (n "vkgen") (v "0.1.0") (h "1xshqgi02ngl76k31d5n2wm38f315bq3sp8f5q6dvhsf6mps835a") (y #t)))

(define-public crate-vkgen-0.1.1 (c (n "vkgen") (v "0.1.1") (h "1b2alcg3hzsqn0393rvnyf3f4czvixisy6qf8752sk96w21rcx0k") (y #t)))

(define-public crate-vkgen-0.1.2 (c (n "vkgen") (v "0.1.2") (h "115k0c0bjy295d2a0w8gwzz32r2fkplmva8bqiixzp7xfp559n4b") (y #t)))

(define-public crate-vkgen-0.1.3 (c (n "vkgen") (v "0.1.3") (h "1s4c7as6qh1c4jby3c6a80gn39h5frfg4s30l4zy2jqn2ym3i3v4") (y #t)))

(define-public crate-vkgen-0.1.4 (c (n "vkgen") (v "0.1.4") (h "1v1yv3g8hgpckd98rjnlv6hwwak1qqpl7rdnm60mxn883nncicl8") (y #t)))

(define-public crate-vkgen-0.1.5 (c (n "vkgen") (v "0.1.5") (h "03bapriw9y9kz46brlxvbkqp8yhwpw934qlcgngzfcv56vrmp3mx") (y #t)))

(define-public crate-vkgen-0.1.6 (c (n "vkgen") (v "0.1.6") (h "0a81k1mj2y8m2acnfkjvas4q9kixgqrp9lg9aii6pmbsas66gb52") (y #t)))

(define-public crate-vkgen-0.1.7 (c (n "vkgen") (v "0.1.7") (h "10yr9019sqwjdymhb8wvmdism6dajhr6raah14q6qcya4y67bnb9") (y #t)))

(define-public crate-vkgen-0.1.8 (c (n "vkgen") (v "0.1.8") (h "0hq66zrc1iy0mfrpd7q7dq29g5l26k4nd44xrc9wzkxq8hdvk94i") (y #t)))

(define-public crate-vkgen-0.1.9 (c (n "vkgen") (v "0.1.9") (h "07wf1xssrs2b3g35ra31x8fl8cj04pjmcmzxvfqyn14aqfzn5cnx") (y #t)))

(define-public crate-vkgen-1.0.0 (c (n "vkgen") (v "1.0.0") (h "18004m0v8d2l46f997lz7sbzinnv2q6j5d4vsprsnd4rlcqnmnai")))

(define-public crate-vkgen-1.1.0 (c (n "vkgen") (v "1.1.0") (h "0r3b3xd2jyjn3nfs8fdywrfa5azfms2hlxbp5fnhygls8whma5qd")))

(define-public crate-vkgen-1.1.1 (c (n "vkgen") (v "1.1.1") (h "0xakvj9s4qhpzy9d15gy1l84kpjxzxl0pcwyq6zvrl6sx10nz83v")))

(define-public crate-vkgen-1.2.0 (c (n "vkgen") (v "1.2.0") (h "005gayhy9ir38gbpjkbmz2kavrj1jxc2ypy073z9dhn0mzzmd3ma")))

(define-public crate-vkgen-1.2.1 (c (n "vkgen") (v "1.2.1") (h "1gnd6qizimp5qap7r892rwasvip981wgnlbr1gwdfi8wa3ryzyxm")))

(define-public crate-vkgen-1.2.2 (c (n "vkgen") (v "1.2.2") (h "0wq5bfs33r756gprpqylllwvy5s5nk4fjlr8pqw8hsj8fnffd59r")))

(define-public crate-vkgen-1.2.3 (c (n "vkgen") (v "1.2.3") (h "0vc22899iyrfy6mwgv0imhbp5dnri50xnxpa8l43shnanip0b5lh")))

(define-public crate-vkgen-2.0.0 (c (n "vkgen") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0p33vhbm3yrf0y94c92l5z0y6cq5rz7hcslj10xbzhc960p2af8i")))

(define-public crate-vkgen-2.0.1 (c (n "vkgen") (v "2.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1d7va1gr66v7x0pli5k0h2hccbrbpysqcqfkc095w1qdp0kj6qgl")))

(define-public crate-vkgen-2.0.2 (c (n "vkgen") (v "2.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1q7nvw8iayk4ga76skcm8i88npzcd8124prmxqxncz4pzxzcyjkn")))

(define-public crate-vkgen-2.0.3 (c (n "vkgen") (v "2.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1fz3s8bcnxqqfx06j5r6azrjnz04fwbrv0inhgij4kk8irajay61")))

(define-public crate-vkgen-2.2.0 (c (n "vkgen") (v "2.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0h5nzq8q4gcvanw13jzk59hbzq89nkzbv7bqq6svgkyj7x07pa1y") (f (quote (("xml_dbg"))))))

(define-public crate-vkgen-2.2.1 (c (n "vkgen") (v "2.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "164djcfkfmckgxbff7a470m1sqdlldnzf396i4dvcw2w76r7cv2v") (f (quote (("xml_dbg"))))))

