(define-module (crates-io uc lo ucloud-cdn-log-parser) #:use-module (crates-io))

(define-public crate-ucloud-cdn-log-parser-0.1.0 (c (n "ucloud-cdn-log-parser") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vhahqhl52a0g6891yxigwrc4wfyxh3gch4drlvm1rihj0rgpsw7")))

(define-public crate-ucloud-cdn-log-parser-0.1.1 (c (n "ucloud-cdn-log-parser") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15m33ibxr68abvg44dj0qr5v0q0k1svbifx7fj4wkj9s1bihwcn6")))

(define-public crate-ucloud-cdn-log-parser-0.1.2 (c (n "ucloud-cdn-log-parser") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dalbki6xicm7md0b0552psidvphkv78mld53rymfwzzsq8lalq3")))

(define-public crate-ucloud-cdn-log-parser-0.1.3 (c (n "ucloud-cdn-log-parser") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17j7wbigksi05619y0nhq8n9aq6hv8dq0xvz7754w3nz075w1zv9")))

