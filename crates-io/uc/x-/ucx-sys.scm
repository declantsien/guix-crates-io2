(define-module (crates-io uc x- ucx-sys) #:use-module (crates-io))

(define-public crate-ucx-sys-0.0.1 (c (n "ucx-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17z7avm7ly6npr9nlnhxy7857xwrp9schdndrn47iz7hp3vzpwzg")))

(define-public crate-ucx-sys-0.0.2 (c (n "ucx-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ddbxl4w62l7hznarhx3hjaas7jxc04s4ji8vpdj0y1dn78ay6jb")))

(define-public crate-ucx-sys-0.1.0 (c (n "ucx-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "mlnx-ofed-libmlx5-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "mlnx-ofed-librdmacm-sys") (r "^0.0.5") (d #t) (k 0)))) (h "13a6fry6kld21pxgl5v4aw0sfh23rqsl8b324d5libxqzvsknh63")))

(define-public crate-ucx-sys-0.1.1 (c (n "ucx-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "mlnx-ofed-libmlx5-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "mlnx-ofed-librdmacm-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1siv0kckg9mm9r8phjig2h97shw8b2inzqqyfp69k89jgknfzi4p")))

(define-public crate-ucx-sys-0.1.2 (c (n "ucx-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "mlnx-ofed-libmlx5-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "mlnx-ofed-librdmacm-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0gmmlih3abnlcr43jj7hh0pqs9xlj5324mzabpbhph1aszv0iin8")))

(define-public crate-ucx-sys-0.1.3 (c (n "ucx-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "mlnx-ofed-libibverbs-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "mlnx-ofed-libmlx5-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "mlnx-ofed-librdmacm-sys") (r "^0.0.6") (d #t) (k 0)))) (h "12jv6g9679542rlkrr2jifj15xicpavpbnw7x0b1d5arizi56811")))

