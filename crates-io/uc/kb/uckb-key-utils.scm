(define-module (crates-io uc kb uckb-key-utils) #:use-module (crates-io))

(define-public crate-uckb-key-utils-0.1.0 (c (n "uckb-key-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "faster-hex") (r "^0.4.1") (d #t) (k 0)) (d (n "kernel") (r "^0.1.0") (f (quote ("insecure"))) (d #t) (k 0) (p "uckb-key")) (d (n "property") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "19phl4qp846x06z6cws4dr33fms8y5fw8gm7p39xr4gk1fz3k0zm")))

(define-public crate-uckb-key-utils-0.1.1 (c (n "uckb-key-utils") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "faster-hex") (r "^0.6.0") (d #t) (k 0)) (d (n "kernel") (r "^0.1.1") (f (quote ("insecure"))) (d #t) (k 0) (p "uckb-key")) (d (n "property") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0x822zix8j8fwwa980rfm849vb99rnvxxcpzfvwxqjccl296562d")))

