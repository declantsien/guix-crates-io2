(define-module (crates-io uc kb uckb-scanner) #:use-module (crates-io))

(define-public crate-uckb-scanner-0.1.0 (c (n "uckb-scanner") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "property") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "tokio") (r "^0.3.3") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.6.0") (d #t) (k 0)) (d (n "uckb-jsonrpc-core") (r "^0.2.0") (d #t) (k 0)))) (h "1rjrl8866igds2zynlkqblwxmpsxijqr2cd52sr2a9klb7ypgid0")))

(define-public crate-uckb-scanner-0.2.0 (c (n "uckb-scanner") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "property") (r "^0.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.6.0") (d #t) (k 0)) (d (n "uckb-jsonrpc-core") (r "^0.3.0") (d #t) (k 0)))) (h "0akg79d9aq08pjy9hm10l7yh6dpash3y1rkfgfkgr1ghgrz4kp87")))

