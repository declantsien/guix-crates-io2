(define-module (crates-io uc l- ucl-sys) #:use-module (crates-io))

(define-public crate-ucl-sys-0.1.0 (c (n "ucl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0dc3904achvksiyap0kv9k7fc2icn0f8dwd301pa52m6k7ykbrxl") (y #t) (l "ucl")))

(define-public crate-ucl-sys-0.1.1 (c (n "ucl-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1ijrzv73v83404axrzxvywq1h7dnbpjpw859s6mxx61ddxy4ag88") (l "ucl")))

