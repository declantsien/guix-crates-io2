(define-module (crates-io uc fi ucfirst) #:use-module (crates-io))

(define-public crate-ucfirst-0.1.0 (c (n "ucfirst") (v "0.1.0") (h "1rbjsfq0r5n8430casj1h4fssbwnrs1vyf49nfpyd17vjfin05q4")))

(define-public crate-ucfirst-0.2.0 (c (n "ucfirst") (v "0.2.0") (h "17dlksla8mscbr05199azvqqmrips4jkldsdanc1fswrns1z1qca")))

(define-public crate-ucfirst-0.2.1 (c (n "ucfirst") (v "0.2.1") (h "0h9q8x2bvsshfq0j8qabjm3i9cglxgr5vwmlp250d7djcv9mc1zx")))

