(define-module (crates-io uc #{81}# uc8151) #:use-module (crates-io))

(define-public crate-uc8151-0.1.0 (c (n "uc8151") (v "0.1.0") (d (list (d (n "embedded-graphics-core") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0vxp7cr7zlvsmv981z6ybzi8w2ckh4898bkw67k9i986hqkgicp8") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-uc8151-0.1.1 (c (n "uc8151") (v "0.1.1") (d (list (d (n "embedded-graphics-core") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0wc5cx9c0g9xbbizwvbw3cbksw2s5863vwgn732mb9141g1p08m1") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-uc8151-0.1.2 (c (n "uc8151") (v "0.1.2") (d (list (d (n "embedded-graphics-core") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1bnsygvqy6234wjzcsx3fala23yaqqa5mlsw7fwas6s7c3d2i7my") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-uc8151-0.1.3 (c (n "uc8151") (v "0.1.3") (d (list (d (n "embedded-graphics-core") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1ska0ng1yw1gqg08424vk0pplrqxr9vnbpvxnch8z4qil5n99x8h") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-uc8151-0.1.4 (c (n "uc8151") (v "0.1.4") (d (list (d (n "embedded-graphics-core") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "14xfzyayfimrv8jvif4arhpkfymbvcn4v6xjh3qwgz8i1zgdayjf") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-uc8151-0.2.0 (c (n "uc8151") (v "0.2.0") (d (list (d (n "embedded-graphics-core") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "109yq4887wg9r81byqdsixa6jd879jl14pkwdvf0q3vs6v77ls3y") (f (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

