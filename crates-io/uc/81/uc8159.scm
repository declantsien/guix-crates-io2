(define-module (crates-io uc #{81}# uc8159) #:use-module (crates-io))

(define-public crate-uc8159-0.1.0 (c (n "uc8159") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 2)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal-unproven"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 2)))) (h "0dn0sgy6lz47br15hqbj091mm7w31plqdlw0bddn5cjxqyg8098s")))

(define-public crate-uc8159-0.1.1 (c (n "uc8159") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 2)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal-unproven"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 2)))) (h "1jzrrc2ydvjlnk7m881ddf2iw4q22nkrskc6z049cynzgwl9km9z")))

