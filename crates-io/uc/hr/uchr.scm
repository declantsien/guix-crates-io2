(define-module (crates-io uc hr uchr) #:use-module (crates-io))

(define-public crate-uchr-0.0.1 (c (n "uchr") (v "0.0.1") (h "1dfnb8g4apcvcca4zjr0j3sbasy2j563ssjqhvq37jx763b8z99f")))

(define-public crate-uchr-0.0.2 (c (n "uchr") (v "0.0.2") (h "14ff8jzc4xmzf5b108sijkv98mydqg453r0xwphsq6yp39as4pi0")))

