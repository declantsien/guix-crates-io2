(define-module (crates-io uc hr uchrono) #:use-module (crates-io))

(define-public crate-uchrono-0.1.0 (c (n "uchrono") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "00zmpdqnwk1d6l1jnajlxx1gplpmxvrj73cm1gq27k0nhdiakkn4") (f (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-uchrono-0.1.1 (c (n "uchrono") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)))) (h "0jwghk5hp3hqgcprz9lnzz7asbbhf636211phhyvlakb8b6817hl") (f (quote (("std" "lazy_static") ("default" "std"))))))

