(define-module (crates-io uc ha uchardet) #:use-module (crates-io))

(define-public crate-uchardet-0.0.1 (c (n "uchardet") (v "0.0.1") (d (list (d (n "uchardet-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1n5f2sxvm8xjd4pxfacfw4nx06x7l9fkk3w1z91gax158fw126qb") (y #t)))

(define-public crate-uchardet-0.0.2 (c (n "uchardet") (v "0.0.2") (d (list (d (n "uchardet-sys") (r "= 0.0.2") (d #t) (k 0)))) (h "1rm7dcqk228v4dygck95vd7g5la82jai28cgx7yskypl2wm9ahxy") (y #t)))

(define-public crate-uchardet-0.0.3 (c (n "uchardet") (v "0.0.3") (d (list (d (n "uchardet-sys") (r "~0.0.2") (d #t) (k 0)))) (h "09r7x3d03qv5gq1bslljil55vi9zs4ndm9fbngnzg5hill4ilavj") (y #t)))

(define-public crate-uchardet-0.0.4 (c (n "uchardet") (v "0.0.4") (d (list (d (n "uchardet-sys") (r "~0.0.2") (d #t) (k 0)))) (h "1q81lqn0cn5n9vkdlvwl88ncclrxpga9nmgwh3l57wpwidfvkkki")))

(define-public crate-uchardet-0.0.5 (c (n "uchardet") (v "0.0.5") (d (list (d (n "uchardet-sys") (r "~0.0.2") (d #t) (k 0)))) (h "14ghb1y1yin251mmirsvybi3l6d6apgmi4vxkk6hyjnhpmdk3fdp") (y #t)))

(define-public crate-uchardet-0.0.6 (c (n "uchardet") (v "0.0.6") (d (list (d (n "uchardet-sys") (r "~0.0.2") (d #t) (k 0)))) (h "0gnlk02bnhmxr29wcfrlqyfnbfnw6qyrvgxbvzcdb79wn1cla6vr")))

(define-public crate-uchardet-0.0.7 (c (n "uchardet") (v "0.0.7") (d (list (d (n "uchardet-sys") (r "~0.0.2") (d #t) (k 0)))) (h "0mwak3y3ka47kpk5frd5f6mg5gi30s04998dj2l8ijhw4gifhxf6")))

(define-public crate-uchardet-0.0.8 (c (n "uchardet") (v "0.0.8") (d (list (d (n "uchardet-sys") (r "*") (d #t) (k 0)))) (h "0ici1aq8a560bwbrfdnd7rc1bl69aya4bvqbbzm4c7dk0jgfr09i")))

(define-public crate-uchardet-0.0.9 (c (n "uchardet") (v "0.0.9") (d (list (d (n "uchardet-sys") (r "*") (d #t) (k 0)))) (h "13z11vg26f1sn1vk84x6nnwi2qnls452asqhj9w5q9vjc1svw4ba")))

(define-public crate-uchardet-0.0.11 (c (n "uchardet") (v "0.0.11") (d (list (d (n "uchardet-sys") (r "*") (d #t) (k 0)))) (h "012i8i91xcxl9aqiwvr03cfn83xl6cxy8q0395vs54j56shm3k0p")))

(define-public crate-uchardet-0.0.12 (c (n "uchardet") (v "0.0.12") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "uchardet-sys") (r "*") (d #t) (k 0)))) (h "12w2sdanlgp1pws2q8iy75l32x6chslzkgswidvcnzx31qc0mf76")))

(define-public crate-uchardet-1.0.0 (c (n "uchardet") (v "1.0.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "uchardet-sys") (r "^1.0.0") (d #t) (k 0)))) (h "0lfc318v73xpdrpfxa9wwb4ldkab77vv16mkrg5d6n38xlzrmdc1") (f (quote (("unstable"))))))

(define-public crate-uchardet-2.0.0-pre.1 (c (n "uchardet") (v "2.0.0-pre.1") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "uchardet-sys") (r "^2.0.0-pre.1") (d #t) (k 0)))) (h "1avf75m38fs5qyn96275xfz4dnfnny9m2x9ljrh4ag3kp6xyzv8y") (f (quote (("unstable"))))))

(define-public crate-uchardet-2.0.0 (c (n "uchardet") (v "2.0.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "uchardet-sys") (r "^2.0.0") (d #t) (k 0)))) (h "1dvw89lqf30ard0n070am35zas9plyv6gmyz8m87jxghqw7zvzdp") (f (quote (("unstable"))))))

(define-public crate-uchardet-2.0.1 (c (n "uchardet") (v "2.0.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "uchardet-sys") (r "^2.0.0") (d #t) (k 0)))) (h "074arzqy0dj87rpf968gyfawca64mvp3nw4j4yq6i3557gpckcnl") (f (quote (("unstable"))))))

(define-public crate-uchardet-2.0.2 (c (n "uchardet") (v "2.0.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "uchardet-sys") (r "^2.0.0") (d #t) (k 0)))) (h "174l2s3b0ll8iq5d3cbc39pppj9yj7ah46i9ymdynar99kdx15m4") (f (quote (("unstable"))))))

(define-public crate-uchardet-2.0.3 (c (n "uchardet") (v "2.0.3") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "uchardet-sys") (r "^2.0.3") (d #t) (k 0)))) (h "1rmrh63kjr5aykr5i8c31ny2cmalap1045w1v39gk93mbs8xd8vx") (f (quote (("unstable"))))))

(define-public crate-uchardet-2.0.4 (c (n "uchardet") (v "2.0.4") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "uchardet-sys") (r "^2.0.3") (d #t) (k 0)))) (h "1m6li17239s8p7s6rsvsjim3gbzrklrdsc8hglqr5s1hn517gwrv") (f (quote (("unstable"))))))

