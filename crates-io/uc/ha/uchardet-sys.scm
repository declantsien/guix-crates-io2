(define-module (crates-io uc ha uchardet-sys) #:use-module (crates-io))

(define-public crate-uchardet-sys-0.0.1 (c (n "uchardet-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "^0.0.1") (d #t) (k 0)))) (h "001znvi4qba1hkwmrz3blgsqrp7k7r92f9r5p5rsz9vmlbp30rh5")))

(define-public crate-uchardet-sys-0.0.2 (c (n "uchardet-sys") (v "0.0.2") (d (list (d (n "pkg-config") (r "~0.0.1") (d #t) (k 0)))) (h "1m1vhyfzyqdik0lmnilx5q23ngrfyz1lfynjpxsq031bvk75flbq")))

(define-public crate-uchardet-sys-0.0.7 (c (n "uchardet-sys") (v "0.0.7") (d (list (d (n "pkg-config") (r "~0.0.1") (d #t) (k 1)))) (h "1pd0w4wp889m25hxc95lrqixpw9r0i6y4bsn9b4bqry3h22hvpfw")))

(define-public crate-uchardet-sys-0.0.9 (c (n "uchardet-sys") (v "0.0.9") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1jzqrsik0640cgvddx908acy3w4mqdplj23lz3wliw1g61qsrwmw")))

(define-public crate-uchardet-sys-0.0.10 (c (n "uchardet-sys") (v "0.0.10") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0ma475jbzja29xqaqiaym21fgp57nzg5d61269gispcr7yr9fcqz")))

(define-public crate-uchardet-sys-0.0.12 (c (n "uchardet-sys") (v "0.0.12") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "00j2rwqf0ibd30m5yjj0ryrvylb1sd8pr8xykf3b71xiccfcprfw")))

(define-public crate-uchardet-sys-1.0.0 (c (n "uchardet-sys") (v "1.0.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1d1q9zcy1c2fa2nb4mq4a9acy0jpivywhdhdli5bckr151a5nvch")))

(define-public crate-uchardet-sys-2.0.0-pre.1 (c (n "uchardet-sys") (v "2.0.0-pre.1") (d (list (d (n "cmake") (r "^0.1.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "15dimn6x0yzm9nhqvv7djkx71yhn45yy5bgn3fhihjagbwy6xpkq")))

(define-public crate-uchardet-sys-2.0.0 (c (n "uchardet-sys") (v "2.0.0") (d (list (d (n "cmake") (r "^0.1.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0spiw53cp67v6akvyb6330bzpjvgxhknqbk35hhyih4cgcm6q6gc")))

(define-public crate-uchardet-sys-2.0.2 (c (n "uchardet-sys") (v "2.0.2") (d (list (d (n "cmake") (r "^0.1.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "18b869gbwjb2lnv7nz3ch9280p1p58yrz1895wyz3byf1lbf816r")))

(define-public crate-uchardet-sys-2.0.3 (c (n "uchardet-sys") (v "2.0.3") (d (list (d (n "cmake") (r "^0.1.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1w05sm09xdk7bx21x98wc6d3w7052l5khbf0qyd2ksrh27mz3kw6")))

