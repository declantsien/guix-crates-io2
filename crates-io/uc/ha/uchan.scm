(define-module (crates-io uc ha uchan) #:use-module (crates-io))

(define-public crate-uchan-0.0.1 (c (n "uchan") (v "0.0.1") (h "1hfzf9x2fy3pmi82f081nlwirsridc2bl7crjlrn62dxc6y5fm8f")))

(define-public crate-uchan-0.1.0 (c (n "uchan") (v "0.1.0") (d (list (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "05lmvznvmw3q4m759234kxam608fc7yy67ak74jxc17lzf47vlns") (f (quote (("std") ("default" "std"))))))

(define-public crate-uchan-0.1.1 (c (n "uchan") (v "0.1.1") (d (list (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "07gh7cjgp596ci5kvvkpzskwjy4w2cnbshvqa1f4i05nmzwcp7bd") (f (quote (("std") ("default" "std"))))))

(define-public crate-uchan-0.1.2 (c (n "uchan") (v "0.1.2") (d (list (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "0x5x9hr4967gkvf64csyf81czl2sj2y8q0q2dxqfn5ly2a50n2d7") (f (quote (("std") ("default" "std"))))))

(define-public crate-uchan-0.1.3 (c (n "uchan") (v "0.1.3") (d (list (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "0ml2n56zdcigp9zshg5lb5fr2z44m1rj32qxp3aiprip2wq21zn6") (f (quote (("std") ("default" "std"))))))

(define-public crate-uchan-0.1.4 (c (n "uchan") (v "0.1.4") (d (list (d (n "cache-padded") (r "^1.2.0") (d #t) (k 0)) (d (n "sptr") (r "^0.3.2") (d #t) (k 0)))) (h "1p0rbskc18famzy6n7g0i8mpw0c4ibbd1a3a18g6nzcpjmxs0iah") (f (quote (("std") ("default" "std"))))))

