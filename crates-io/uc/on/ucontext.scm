(define-module (crates-io uc on ucontext) #:use-module (crates-io))

(define-public crate-ucontext-0.0.1 (c (n "ucontext") (v "0.0.1") (h "0la5241dnr5l4cvba655wswr8jxdb0azy7bi3ddb4ksvwpnbq2sn") (y #t)))

(define-public crate-ucontext-0.0.2 (c (n "ucontext") (v "0.0.2") (h "0f9jmmsh7dvdxmbbgm050a7cxsqcn9z7j9fsjn5ssp3gwdi2imf6") (y #t)))

(define-public crate-ucontext-0.0.3 (c (n "ucontext") (v "0.0.3") (h "1aa8sq0p5vkakmv5ri46ahcym378h3wc78zl1dbjadhxalcwqswh") (y #t)))

(define-public crate-ucontext-0.0.4 (c (n "ucontext") (v "0.0.4") (h "0v70gydx3gdxnmv4bf5v00787wxnpyql5g9ynrjh0a6k7zi88nx4") (y #t)))

(define-public crate-ucontext-0.0.5 (c (n "ucontext") (v "0.0.5") (h "1mq0dk6rhpf61fbfi5wkgi7vkbih7aklk4kgj0n6nj7wcdiply6b")))

