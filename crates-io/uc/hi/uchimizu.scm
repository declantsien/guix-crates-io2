(define-module (crates-io uc hi uchimizu) #:use-module (crates-io))

(define-public crate-uchimizu-0.0.1 (c (n "uchimizu") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "06gpd2x6sbw5bnmdymxj4ax3fyqvmjpl188wqdziiqk24w2vvb94") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:chrono"))))))

