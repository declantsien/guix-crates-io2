(define-module (crates-io uc re ucred) #:use-module (crates-io))

(define-public crate-ucred-0.0.1 (c (n "ucred") (v "0.0.1") (h "0i771ynwqwrsl2dkd3hvydn8rqglpp8s9qzrbikc6jhqlr359jkg") (y #t)))

(define-public crate-ucred-0.2.0 (c (n "ucred") (v "0.2.0") (h "1imwqhi4l3r7zl2gx6dxkqr0wyb07xn91g73i1wm2rkc7yijk7rh") (y #t)))

(define-public crate-ucred-1.0.0 (c (n "ucred") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1993mg3mbk1n1fikc9ir0xl4266gq2nqvkk6ijc9gjmqd2f44f62")))

(define-public crate-ucred-1.0.1 (c (n "ucred") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14lzr6livq0pc128giqm6jr2gm4mfaqb5x8fnqih841d0ybblijs")))

(define-public crate-ucred-1.1.0 (c (n "ucred") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yimjnrzsj674ca723j24ab3mbkf4p0dysgznv9srb7bzly78525")))

