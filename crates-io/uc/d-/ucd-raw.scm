(define-module (crates-io uc d- ucd-raw) #:use-module (crates-io))

(define-public crate-ucd-raw-0.1.0 (c (n "ucd-raw") (v "0.1.0") (h "1458a74pzaymlav200jfdlai40bn26ahf10214qazjjhbm8nmx63")))

(define-public crate-ucd-raw-0.2.0 (c (n "ucd-raw") (v "0.2.0") (h "1amk0k0v1h6ahqgnjs6123ncyl5szjcx8qg7fa48c0mvh60ncywv")))

(define-public crate-ucd-raw-0.3.0 (c (n "ucd-raw") (v "0.3.0") (h "0i736x54zhb0dhgcdqz6hffysxznpj8m346n7wky9bx0vcgd2xii")))

(define-public crate-ucd-raw-0.4.0 (c (n "ucd-raw") (v "0.4.0") (h "10rxai44gzilm8s7k1rq0nn13i3ggnvvyclqhsizmmxy6f1xwq9y")))

(define-public crate-ucd-raw-0.5.0 (c (n "ucd-raw") (v "0.5.0") (h "07z0v28vff0irqgqj1ikagv5nvfrdl8n21xvl7pyj6ix6xh4yax4")))

