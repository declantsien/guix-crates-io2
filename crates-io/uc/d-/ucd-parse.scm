(define-module (crates-io uc d- ucd-parse) #:use-module (crates-io))

(define-public crate-ucd-parse-0.1.0 (c (n "ucd-parse") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "033vkk4ypchn4a3hprgrzhgs5pa7iya9ynb3h3zh4bl8iypvhm05")))

(define-public crate-ucd-parse-0.1.1 (c (n "ucd-parse") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1z0pxkiycvgvzsn39a10yfbjjyswwrvmyd210khg1miwm3iyyj7z")))

(define-public crate-ucd-parse-0.1.2 (c (n "ucd-parse") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1l12ri4y7bly29xmm5k7jj3wrr1lb6xbjcnm1ldyc6hxlmh906i9")))

(define-public crate-ucd-parse-0.1.3 (c (n "ucd-parse") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13mq6c85r6ak10gjlq74mzdhsi0g0vps2y73by420513gfnipm97")))

(define-public crate-ucd-parse-0.1.4 (c (n "ucd-parse") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1d06wcnzs1kqdc0wszlycclhvr92j9v08nkq0w7jyld69nzm4sya")))

(define-public crate-ucd-parse-0.1.5 (c (n "ucd-parse") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "unicode"))) (k 0)))) (h "10bncqsc9r3rskyp04i4z9zb3a2ycfjznpsp6ikgs75lbggks29r") (y #t)))

(define-public crate-ucd-parse-0.1.6 (c (n "ucd-parse") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "unicode"))) (k 0)))) (h "08kyyj2hlchvis4bamd3mvw9vb769ga9vzkm22yl2vdx56rm7h04")))

(define-public crate-ucd-parse-0.1.7 (c (n "ucd-parse") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "unicode"))) (k 0)))) (h "0ms8rhkckwnmjlxz7k01sqxsywbc73wx2l2gc2slclbkpyi5jzjq")))

(define-public crate-ucd-parse-0.1.8 (c (n "ucd-parse") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "unicode"))) (k 0)))) (h "0z0vw78ynfjhylmf7bizl6yf82fg5rs6sfilb03vdf7nbp9zhsaj")))

(define-public crate-ucd-parse-0.1.9 (c (n "ucd-parse") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "unicode"))) (k 0)))) (h "18hb5jykk1lg7aaycjfivq7i3lwjlgxfbn9wz9kblsk07f1gi06y")))

(define-public crate-ucd-parse-0.1.10 (c (n "ucd-parse") (v "0.1.10") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "unicode"))) (k 0)))) (h "0fr5c5fm488l17fcns00hazayax33f8305yf01awbx4qm5b0abgw")))

(define-public crate-ucd-parse-0.1.11 (c (n "ucd-parse") (v "0.1.11") (d (list (d (n "regex-lite") (r "^0.1.0") (d #t) (k 0)))) (h "0nvwjv9jc3842dmdda753rvzhwzin2k97576ln0zqjbml261y1ci") (r "1.70")))

(define-public crate-ucd-parse-0.1.12 (c (n "ucd-parse") (v "0.1.12") (d (list (d (n "regex-lite") (r "^0.1.0") (d #t) (k 0)))) (h "0a7lampg020dzdgnh4bakk22zi86csg7klpfawpqrcapc5imjb11") (r "1.70")))

(define-public crate-ucd-parse-0.1.13 (c (n "ucd-parse") (v "0.1.13") (d (list (d (n "regex-lite") (r "^0.1.0") (d #t) (k 0)))) (h "1fbryah32sswz34kfi61mvkmhq1kwgvia2v6q7s4vgzw488zhvy0") (r "1.70")))

