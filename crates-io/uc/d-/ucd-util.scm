(define-module (crates-io uc d- ucd-util) #:use-module (crates-io))

(define-public crate-ucd-util-0.1.0 (c (n "ucd-util") (v "0.1.0") (h "07x3vgdvgnlwq3gh4r9fbp30lyslcbyqgy92midw97ya4xz5dj9s")))

(define-public crate-ucd-util-0.1.1 (c (n "ucd-util") (v "0.1.1") (h "0p98v8mqksf3sa4jfvj6imajjdp2ava1lafsrpk8y3wxcgbf4azx")))

(define-public crate-ucd-util-0.1.2 (c (n "ucd-util") (v "0.1.2") (h "0i14sn3k88d693cyblykrvli6p0lbwn9vb9904hwvb8czylvzy6h")))

(define-public crate-ucd-util-0.1.3 (c (n "ucd-util") (v "0.1.3") (h "11lgx380zgqsm265cg78w2mcjpmldbwbi01lb5w48hyqwi720p2k")))

(define-public crate-ucd-util-0.1.4 (c (n "ucd-util") (v "0.1.4") (h "1r1mldava1zzy4yy765qs6pv5cmv813lvaa71665fsyzy76zafwg")))

(define-public crate-ucd-util-0.1.5 (c (n "ucd-util") (v "0.1.5") (h "0x088q5z0m09a2jqcfgsnq955y8syn1mgn35cl78qinkxm4kp6zs")))

(define-public crate-ucd-util-0.1.6 (c (n "ucd-util") (v "0.1.6") (h "0y1m6n94c3apdh95bnfl6jny30khlqb1mld1fa25zlnmgqkvm4k2") (y #t)))

(define-public crate-ucd-util-0.1.7 (c (n "ucd-util") (v "0.1.7") (h "13ng291mkc9b132jjf4laj76f5nqm5qd2447rm8bry3wxbdc5kaw")))

(define-public crate-ucd-util-0.1.8 (c (n "ucd-util") (v "0.1.8") (h "0dnjzvcb3q5b69w214wvsmdz2b08jmkxfp0ykckqqd2x15752py8")))

(define-public crate-ucd-util-0.1.9 (c (n "ucd-util") (v "0.1.9") (h "1z4v3hhbbqjwszj300wpz5fy5hn4zdr63fxi1v0z48mi27vcpgv5")))

(define-public crate-ucd-util-0.1.10 (c (n "ucd-util") (v "0.1.10") (h "00z0if2q1zlwkbaymfyhxmas1v1jgy1hv8mhz156345m69fzrlmb")))

(define-public crate-ucd-util-0.2.0 (c (n "ucd-util") (v "0.2.0") (h "1hvzw355chhyzklk8dv2h2xp4zcxzl52dpks7mjarclz6zv7wll6")))

(define-public crate-ucd-util-0.2.1 (c (n "ucd-util") (v "0.2.1") (h "10qr4vgah6pbvv9hskiysrvcm9pvzfh01dwnig5r1a9r7x9s61sy")))

