(define-module (crates-io uc d- ucd-trie) #:use-module (crates-io))

(define-public crate-ucd-trie-0.1.0 (c (n "ucd-trie") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0v2ihmj4zds8bl4404qqginc6bkjh7z0mj9vaa10vldjmqbd14pk") (f (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1.1 (c (n "ucd-trie") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0xwxkg0fyclbz8fl99iidq4gaw2jjngf8c6c8kqnqhkpzsqwbabi") (f (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1.2 (c (n "ucd-trie") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1hh6kyzh5xygwy96wfmsf8v8czlzhps2lgbcyhj1xzy1w1xys04g") (f (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1.3 (c (n "ucd-trie") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "072cblf8v3wzyaz3lhbpzgil4s03dpzg1ppy3gqx2l4v622y3pjn") (f (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1.4 (c (n "ucd-trie") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "070lb1h3abm00zvrj4wa4gls8zwzx53sp2iq5gg8amgyqjchamw9") (f (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1.5 (c (n "ucd-trie") (v "0.1.5") (d (list (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "10ggllapxq99cxxy179wbklmabj5fikm02233v4idf7djvcw8ycy") (f (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1.6 (c (n "ucd-trie") (v "0.1.6") (d (list (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "1ff4yfksirqs37ybin9aw71aa5gva00hw7jdxbw8w668zy964r7d") (f (quote (("std") ("default" "std"))))))

