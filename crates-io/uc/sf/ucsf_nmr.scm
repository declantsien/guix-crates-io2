(define-module (crates-io uc sf ucsf_nmr) #:use-module (crates-io))

(define-public crate-ucsf_nmr-0.1.0 (c (n "ucsf_nmr") (v "0.1.0") (d (list (d (n "float_eq") (r "^0.2.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 2)) (d (n "plotters") (r "^0.2.12") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1jfinv0n371mqz5bsjhc5l2ddbvzd6mbv2xpy373n6wp0wrincsz")))

(define-public crate-ucsf_nmr-0.1.1 (c (n "ucsf_nmr") (v "0.1.1") (d (list (d (n "float_eq") (r "^0.2.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 2)) (d (n "plotters") (r "^0.2.12") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0fxz0cm1vpyrqfrksya3v0w6v0xv77ss0481314p08fg13hcpwdm")))

(define-public crate-ucsf_nmr-0.1.2 (c (n "ucsf_nmr") (v "0.1.2") (d (list (d (n "float_eq") (r "^0.2.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 2)) (d (n "plotters") (r "^0.2.12") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "017s81zc0y0kg8yy7lzvmh09ai9mc6260bbl3k7shc0bvaf6lp61")))

(define-public crate-ucsf_nmr-0.1.3 (c (n "ucsf_nmr") (v "0.1.3") (d (list (d (n "float_eq") (r "^0.2.0") (d #t) (k 2)) (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 2)) (d (n "plotters") (r "^0.2.12") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0pzn8kv25vfmlzsdfqp0hdbvgafykg5a9kwgakh0j9z4hzf603x6")))

(define-public crate-ucsf_nmr-0.2.0 (c (n "ucsf_nmr") (v "0.2.0") (d (list (d (n "float_eq") (r "^0.2.0") (d #t) (k 2)) (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 2)) (d (n "plotters") (r "^0.2.12") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "19rlgnadpb58n8ycf930kfrz06q5pj0lzrrmxjwmjrmqq4z3r6w6")))

