(define-module (crates-io uc i_ uci_rs) #:use-module (crates-io))

(define-public crate-uci_rs-0.1.0 (c (n "uci_rs") (v "0.1.0") (h "14fx830z23na7wvskvafp7792xal71hljy7ad5qkvav56rmqi4x0")))

(define-public crate-uci_rs-0.1.1 (c (n "uci_rs") (v "0.1.1") (h "1xw3p1czx1ih27klijr4gnafgrp5y54vf571yd8za5i63wvwi3r5")))

(define-public crate-uci_rs-0.1.2 (c (n "uci_rs") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "1wihhk2kic3m36n720n3a679wid3amcgm3nn5kk66x1ld12qiglg")))

