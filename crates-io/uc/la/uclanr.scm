(define-module (crates-io uc la uclanr) #:use-module (crates-io))

(define-public crate-uclanr-1.0.0 (c (n "uclanr") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hdg9i1rl4lxn7wm1vf18s1pmzb2ng4vgshpdpf474zbhzf46fz3")))

(define-public crate-uclanr-2.0.0 (c (n "uclanr") (v "2.0.0") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1v604qrd2gc493arzg79dynbzf15vd9v7ia83w0pfh9xw7w939bs")))

(define-public crate-uclanr-2.0.1 (c (n "uclanr") (v "2.0.1") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vhzdr9ylxwrmrbzmjv6n8kgp1z1fs2hysi8bglmdsd01b4nbz3b")))

(define-public crate-uclanr-2.1.0 (c (n "uclanr") (v "2.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n3kbj4afl4p5agg378gxr4y1kmdfk2yhhccr1sq6gvxzd7xsxcm")))

