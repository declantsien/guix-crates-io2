(define-module (crates-io uc an ucan-capabilities-object) #:use-module (crates-io))

(define-public crate-ucan-capabilities-object-0.1.0 (c (n "ucan-capabilities-object") (v "0.1.0") (d (list (d (n "iri-string") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nutype") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10hki5d0wm5qqk4zys0y727qdhb4zj4mkh775207yfsjhrymjfaz")))

