(define-module (crates-io uc li uclicious-libucl-sys) #:use-module (crates-io))

(define-public crate-uclicious-libucl-sys-0.8.1 (c (n "uclicious-libucl-sys") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0k5g6yrbdsc5avy4g5hhs059ip1zizdc09zgbnkisx4pw4rplg7f")))

(define-public crate-uclicious-libucl-sys-0.8.2-alpha.1 (c (n "uclicious-libucl-sys") (v "0.8.2-alpha.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "19kfvz9k7j3cqmpb4l4z4bcv7r3s6lyv6k01mac35vls0ijdlll2")))

(define-public crate-uclicious-libucl-sys-0.8.2-alpha.2 (c (n "uclicious-libucl-sys") (v "0.8.2-alpha.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "14qhjqz7mafpy1fbz26lf80fqxqlds6kv5z870xkcw9hiy735c8j")))

(define-public crate-uclicious-libucl-sys-0.8.2 (c (n "uclicious-libucl-sys") (v "0.8.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "122xsw5di8vwixcl8qsna4isn1a39yckxr6rag24a8vkch0dznj9")))

