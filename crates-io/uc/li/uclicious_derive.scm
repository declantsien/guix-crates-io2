(define-module (crates-io uc li uclicious_derive) #:use-module (crates-io))

(define-public crate-uclicious_derive-0.1.0 (c (n "uclicious_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0qyypadiwyp9jf8jm4qaf8ysfr7hh09wcw974kmh7d3if6izjmyr")))

(define-public crate-uclicious_derive-0.1.1 (c (n "uclicious_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0xafpqj74miyysfh42w2pnzmkqjmb8vfp9j27l9kcvpwxqmdlvc0")))

(define-public crate-uclicious_derive-0.1.3 (c (n "uclicious_derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "16l2cklm6a2jjv395hxlckv1rl2zzj255lnchf87f0vwqayblax2")))

(define-public crate-uclicious_derive-0.1.5 (c (n "uclicious_derive") (v "0.1.5") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1msfpp6cbdmik11n2hdxk0zcimicz9a8mwx3cpzr1b1i6zagrp59")))

(define-public crate-uclicious_derive-0.1.7 (c (n "uclicious_derive") (v "0.1.7") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1x1jvlpp93dwv9pchh37324yibjsqxf1228bm4hrpbp90p065vdb")))

