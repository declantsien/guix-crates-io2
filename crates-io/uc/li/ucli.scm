(define-module (crates-io uc li ucli) #:use-module (crates-io))

(define-public crate-ucli-0.1.0 (c (n "ucli") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "06sask3zifhv59rmy8r0hsn7cvymbgk0y14xnd2jvanf0qw9ksbr")))

(define-public crate-ucli-0.2.0 (c (n "ucli") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "17aminh91xsiv454fvlb0kkizn63vwm209m4qvxlrjq97fpra88j")))

(define-public crate-ucli-0.2.1 (c (n "ucli") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "0n3v4y9g0qjp38dlcz24w78qqw2librvz8147k5rg302k38c4hwc")))

(define-public crate-ucli-0.2.2 (c (n "ucli") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "0f91vmdy3d087c0l82rwsalphaq6cra34s4afk6pkklm0hgh715l")))

