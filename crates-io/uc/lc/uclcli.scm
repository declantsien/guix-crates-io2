(define-module (crates-io uc lc uclcli) #:use-module (crates-io))

(define-public crate-uclcli-0.1.0 (c (n "uclcli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n49qx8kvcfjh97ha3qvi2311zrnl6m5apzb5gfpvyq7s6dy02gb") (l "ucl")))

