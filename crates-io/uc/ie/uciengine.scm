(define-module (crates-io uc ie uciengine) #:use-module (crates-io))

(define-public crate-uciengine-0.1.0 (c (n "uciengine") (v "0.1.0") (d (list (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1yqb2974wac0mvcwsnlhr518hdhq07b7ig8rd42gvh5m3vkysqkw")))

(define-public crate-uciengine-0.1.1 (c (n "uciengine") (v "0.1.1") (d (list (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1v86bd3wani4wm1af203fs8xqycgclix04xgw6y28m7h3zwdzzhz")))

(define-public crate-uciengine-0.1.2 (c (n "uciengine") (v "0.1.2") (d (list (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0swscdvd6df2lr7f7khgdm907bnzk7cnba38985ramwv4mlqjq3g")))

(define-public crate-uciengine-0.1.3 (c (n "uciengine") (v "0.1.3") (d (list (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0ff3c98bgg6dgrmcxqimfwhcn84awji88al3wc4vgv2gz2qk5x14")))

(define-public crate-uciengine-0.1.4 (c (n "uciengine") (v "0.1.4") (d (list (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0q75j1gj92jnq0lzn6bb1r1qqxzj8q9dni9b9ky2bgin5w892wnb")))

(define-public crate-uciengine-0.1.5 (c (n "uciengine") (v "0.1.5") (d (list (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "12ndircvrh0siin44jkb4rbw57bbyb6ifb7vg9an3lhc459wdmhr")))

(define-public crate-uciengine-0.1.6 (c (n "uciengine") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0r5b1w2dw8pp00mgsfdnp1zz9dymf3a2r2vsj3d6z22s89p0dnzp")))

(define-public crate-uciengine-0.1.7 (c (n "uciengine") (v "0.1.7") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0wlsv10w0wqj8i60fpyd33l8q4czrbmxiajrfjfyrkqhi304lmff")))

(define-public crate-uciengine-0.1.8 (c (n "uciengine") (v "0.1.8") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0m72c7z7bn4cm49jf641j3xbm5fsz1pjx44izbrizi4mf5aaaz8g")))

(define-public crate-uciengine-0.1.9 (c (n "uciengine") (v "0.1.9") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1sz934jdk0fk6wdi5lcfxqfm906y1i50k14r8fqc88riqfm8f6k0")))

(define-public crate-uciengine-0.1.10 (c (n "uciengine") (v "0.1.10") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "07bkan1qvzdqnhk75clqzpw7a239zff88iy955xm67jzhdrvfxpk")))

(define-public crate-uciengine-0.1.11 (c (n "uciengine") (v "0.1.11") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "042zppss9wgvrd9y2df81ky1gjbqdyvk0nz9bp5zapif7zfva1gf")))

(define-public crate-uciengine-0.1.12 (c (n "uciengine") (v "0.1.12") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1bcpi6jsz4gnbbgywh49p2wz2kpgb9irj1pjkay24bkvdb0mdarf")))

(define-public crate-uciengine-0.1.13 (c (n "uciengine") (v "0.1.13") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1rk77w44pslmlvjc9gi07185n3ri8wxgcd4j2mm4iq9hmp28n663")))

(define-public crate-uciengine-0.1.14 (c (n "uciengine") (v "0.1.14") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1wcc22cmygzpmwbkbmyl1afq1gdrwxbs6yv4gw7g1kld2wc9rh52")))

(define-public crate-uciengine-0.1.15 (c (n "uciengine") (v "0.1.15") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1cj5kan7kimi6bbd82h7mxajcwass70bn3qd28slvdgng342g3sj")))

(define-public crate-uciengine-0.1.16 (c (n "uciengine") (v "0.1.16") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "15ikm047cgp8pwkhwzbdisi23k66b1ajsiviaagp45fzwj2gj2v8")))

(define-public crate-uciengine-0.1.17 (c (n "uciengine") (v "0.1.17") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1x7mnh9qdrvjyr3ii2y51l1n6mzp2hbz0qhbkawxq3zcz16lfzxg")))

(define-public crate-uciengine-0.1.18 (c (n "uciengine") (v "0.1.18") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "05qq6lc122qqkw0khxh2ckm3mrcazb8zj3k0jax892cv44yhij14")))

(define-public crate-uciengine-0.1.19 (c (n "uciengine") (v "0.1.19") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xzwx8wbs85g4k6pyzinidya9arrc2g18ycdk4b2dhr40f8szh2m")))

(define-public crate-uciengine-0.1.20 (c (n "uciengine") (v "0.1.20") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "05zaxmnw8hhww59lxpm2wgwkv5phw6kb0fpadjgjqr5j37n74vyj")))

(define-public crate-uciengine-0.1.21 (c (n "uciengine") (v "0.1.21") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pqak2n11y34iy86kai9f8pby1py8fk85x8h6vzdy4wmwnzcy9f5")))

(define-public crate-uciengine-0.1.22 (c (n "uciengine") (v "0.1.22") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xnhdykvsvz6hafys6dvxdqa5a409xy1ams8x9y4f1qpp4v8wkrc")))

(define-public crate-uciengine-0.1.23 (c (n "uciengine") (v "0.1.23") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yzwjsrsgk48rfzzb06sp0vxpq4cnmxhw3nb4k38lxxmf4w53f8s")))

(define-public crate-uciengine-0.1.24 (c (n "uciengine") (v "0.1.24") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "18yfwwrhwllxfcjhvsrgvbgd815vfpwi27638zdq69287mlxqjwf")))

(define-public crate-uciengine-0.1.25 (c (n "uciengine") (v "0.1.25") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yzmyws5638x55bfwkhryg59s5al6zzfxvimx80fxgbdn86i0qb8")))

(define-public crate-uciengine-0.1.26 (c (n "uciengine") (v "0.1.26") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mr185i3rgkbxwk82x0i029lhr86zbg47829n341920q7zd64bar")))

(define-public crate-uciengine-0.1.27 (c (n "uciengine") (v "0.1.27") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yd8xbsm56gpdy62nvk58d22dz5lcsy0zaa6xx7dmki24a50jxgi")))

(define-public crate-uciengine-0.1.28 (c (n "uciengine") (v "0.1.28") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "020nfbn3zvk1ws76zs8xnazx34lssnfb4qvcbbfwh77rx9pf6bm1")))

(define-public crate-uciengine-0.1.30 (c (n "uciengine") (v "0.1.30") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1109gwri4c3qlmczj12129kfhqfwvspiwwhx1m4hvsl1rvm40q34")))

(define-public crate-uciengine-0.1.31 (c (n "uciengine") (v "0.1.31") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vhw3jz2y2zmzy7rr10vwvgf72cxcr5p6b9h7wjgclj658zzzvkr")))

(define-public crate-uciengine-0.1.32 (c (n "uciengine") (v "0.1.32") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jzgkqlcz6sn5jl16wdd9hplnvcfhh6r5ih1m56zy8vmdspx3h89")))

(define-public crate-uciengine-0.1.33 (c (n "uciengine") (v "0.1.33") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "envor") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("full"))) (d #t) (k 0)))) (h "13swv0dlb7inahf1b9pyb2wwcj3rp81nq95ygqy1fj034jrw9ys0")))

