(define-module (crates-io te tk tetkey) #:use-module (crates-io))

(define-public crate-tetkey-0.0.0 (c (n "tetkey") (v "0.0.0") (h "0jbhrj3r96qddpl4lv74x61xaqj75y2d5kcj1p686g99lxz8dka6") (y #t)))

(define-public crate-tetkey-2.0.0 (c (n "tetkey") (v "2.0.0") (d (list (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tc-cli") (r "^0.8.0") (d #t) (k 0)))) (h "1scjy01rs5ni5hfqpyi7zaw0j67m7yj261nmh802zfdk43d9wkxr")))

