(define-module (crates-io te rp terp-marketplace-utils) #:use-module (crates-io))

(define-public crate-terp-marketplace-utils-0.1.1 (c (n "terp-marketplace-utils") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.3") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.3.1") (d #t) (k 0)) (d (n "cw721") (r "^0.18.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (k 0)))) (h "06qsl4qcni93mdqj9kfs33bzlgbgfxm9n8iyq84r7pcadd56yr6m") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

