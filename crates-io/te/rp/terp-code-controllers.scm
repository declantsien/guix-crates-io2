(define-module (crates-io te rp terp-code-controllers) #:use-module (crates-io))

(define-public crate-terp-code-controllers-0.1.0 (c (n "terp-code-controllers") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.3.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "terp-sdk") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1mfyaxsplw4iw1nm3r1navhfnyclnnz62dkvqakz675wh3n6q6az")))

