(define-module (crates-io te rp terp-metadata) #:use-module (crates-io))

(define-public crate-terp-metadata-0.1.0 (c (n "terp-metadata") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.3.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "1nnzp3gjpsn9zy8dvwmvmwjf90a286dmad2zkg8cp8im8cj4xw1p")))

