(define-module (crates-io te rp terp-multi-test) #:use-module (crates-io))

(define-public crate-terp-multi-test-0.1.0 (c (n "terp-multi-test") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.1") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "terp-sdk") (r "^0.1.0") (d #t) (k 0)))) (h "0vdc80akdn0fmglxpc7i87118wchl9yrah0ml6l3sfp4rjhszykw")))

