(define-module (crates-io te rp terp-sdk) #:use-module (crates-io))

(define-public crate-terp-sdk-0.1.0 (c (n "terp-sdk") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.3.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "010zqf02s249m2p6mnjhq7272qxa3v8r6jn4m0x0cgbfjgw21kcb") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

