(define-module (crates-io te rp terp721) #:use-module (crates-io))

(define-public crate-terp721-0.1.0 (c (n "terp721") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.3.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.1") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16.5") (d #t) (k 2)) (d (n "cw-ownable") (r "^0.5.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "cw721-base") (r "^0.18.0") (f (quote ("library"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0c8aysnvzhpnh1l4j32fqxn7ckrr5ksab83wmq5f9qh1nlapiwr8") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

