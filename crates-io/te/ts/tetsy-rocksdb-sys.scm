(define-module (crates-io te ts tetsy-rocksdb-sys) #:use-module (crates-io))

(define-public crate-tetsy-rocksdb-sys-0.5.6 (c (n "tetsy-rocksdb-sys") (v "0.5.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "tetsy-snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "1cxgh1ss3i7y6wrr9wdwavr7w2wzd3ddfl4akv9y7wwdwhfa05k4") (f (quote (("default")))) (l "rocksdb")))

(define-public crate-tetsy-rocksdb-sys-0.5.2 (c (n "tetsy-rocksdb-sys") (v "0.5.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "tetsy-snappy-sys") (r "^0.1.0") (d #t) (k 0)))) (h "04wncadayj5llkaxfb8r4129dc9mwhiss6q7qc3s7qii4s9w7i54") (f (quote (("default")))) (l "rocksdb")))

