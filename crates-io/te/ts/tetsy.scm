(define-module (crates-io te ts tetsy) #:use-module (crates-io))

(define-public crate-tetsy-0.0.0 (c (n "tetsy") (v "0.0.0") (h "1wh64ha3b6xpwn9vnhbk2ndxr9b92w3986ip79x4ahh9zwg7x8ip") (y #t)))

(define-public crate-tetsy-0.0.1 (c (n "tetsy") (v "0.0.1") (h "13jlsd5p4h8q3xbqdl0f5vqk36vwmpidwa1id7qi3l2j73rjj82y")))

