(define-module (crates-io te ts tetsy-send-wrapper) #:use-module (crates-io))

(define-public crate-tetsy-send-wrapper-0.0.0 (c (n "tetsy-send-wrapper") (v "0.0.0") (h "1cg2zcg6zf52cz57k44jrzz666g80dyq9ikcg6dn0d9f1rbjbwcp") (y #t)))

(define-public crate-tetsy-send-wrapper-0.1.0 (c (n "tetsy-send-wrapper") (v "0.1.0") (h "0by693vszfx43m7arcllmmmmyr0i33503h10qx0iffil22qaylvd")))

