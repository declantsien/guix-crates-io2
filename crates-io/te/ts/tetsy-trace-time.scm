(define-module (crates-io te ts tetsy-trace-time) #:use-module (crates-io))

(define-public crate-tetsy-trace-time-0.1.3 (c (n "tetsy-trace-time") (v "0.1.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0i7y51mqgxk4wn18w3pxykmral8jz9ym1c0x9naazwyj1s8ia12p")))

(define-public crate-tetsy-trace-time-0.1.1 (c (n "tetsy-trace-time") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0z6vhablkgxnbd7q2y9n5d9k0x5lbb4p3ss9chjgvx9h2qsf9fq7")))

(define-public crate-tetsy-trace-time-0.1.3-alpha (c (n "tetsy-trace-time") (v "0.1.3-alpha") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1dngbwppv9nz11r0mi23xabz75v5489g0b59xdcr3dgnwrhh1q4k")))

(define-public crate-tetsy-trace-time-0.1.4 (c (n "tetsy-trace-time") (v "0.1.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0ayqard874d85ggfm24j52ypz0zgxag9h9w1s91kjsj1pqxj4f1p")))

