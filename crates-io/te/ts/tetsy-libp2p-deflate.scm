(define-module (crates-io te ts tetsy-libp2p-deflate) #:use-module (crates-io))

(define-public crate-tetsy-libp2p-deflate-0.0.0 (c (n "tetsy-libp2p-deflate") (v "0.0.0") (h "0cvk80znbg91b0hdifr22lmrvj95w56hg7712vsb7wdnmszrkwnp") (y #t)))

(define-public crate-tetsy-libp2p-deflate-0.27.1 (c (n "tetsy-libp2p-deflate") (v "0.27.1") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)))) (h "1mq7fw87b7l9hmwk0fsgf9yc1v5ifaim23fcfp31by7zqagyw35s")))

(define-public crate-tetsy-libp2p-deflate-0.27.0 (c (n "tetsy-libp2p-deflate") (v "0.27.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)))) (h "0r2lwmqz5ra1lwzvz5vzc3jk4vnszb55gssv15fj50799sfs6642")))

(define-public crate-tetsy-libp2p-deflate-0.27.2 (c (n "tetsy-libp2p-deflate") (v "0.27.2") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tetsy-libp2p-core") (r "^0.27.1") (d #t) (k 0)))) (h "1k9cilrhqpjkrah4jl32n6q7g17zhyn8a7g2zqf37g0lp8lrxx6m")))

