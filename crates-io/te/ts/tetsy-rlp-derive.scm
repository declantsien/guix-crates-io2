(define-module (crates-io te ts tetsy-rlp-derive) #:use-module (crates-io))

(define-public crate-tetsy-rlp-derive-0.1.0 (c (n "tetsy-rlp-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "tetsy-rlp") (r "^0.5.0") (d #t) (k 2)))) (h "1c28n5fq9362xsnil1m74kwbjd8bppbpim4nkxjqsi87ymp7nlka")))

(define-public crate-tetsy-rlp-derive-0.1.1 (c (n "tetsy-rlp-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "tetsy-rlp") (r "^0.4.5") (d #t) (k 2)))) (h "0l3ganql24gky9fj96644shfisdvk3flmvgi6pd4zjcdlcsw82bj")))

(define-public crate-tetsy-rlp-derive-0.0.1 (c (n "tetsy-rlp-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "tetsy-rlp") (r "^0.4.5") (d #t) (k 2)))) (h "05yjgkj5pdzhxxzpjdab1nynwd94fkrgbmsf7x51dh1l7bxnisjp")))

(define-public crate-tetsy-rlp-derive-0.1.0-alpha (c (n "tetsy-rlp-derive") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "tetsy-rlp") (r "^0.5.0-alpha") (d #t) (k 2)))) (h "0pq8jqjhii4dfaqv0l1i0xzp8j7mf7yaj6wqyi79s4jx6kxr7fk2")))

(define-public crate-tetsy-rlp-derive-0.1.2 (c (n "tetsy-rlp-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)) (d (n "tetsy-rlp") (r "^0.5.1") (d #t) (k 2)))) (h "1r2p03cja4q0lvnssnigqs6rr4c3vhfkf0zi7vbkvkjpl9nv72js")))

