(define-module (crates-io te ts tetsy-bytes) #:use-module (crates-io))

(define-public crate-tetsy-bytes-0.1.2 (c (n "tetsy-bytes") (v "0.1.2") (h "1q5l0anw170rgyv3gyjp2pkx8sq5wy938zr5fblpip7ab9ni66ri") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-bytes-0.1.0 (c (n "tetsy-bytes") (v "0.1.0") (h "1c0xa5i4jyh98c9lc7hys0h4b6kyarkxvvj04p00fzixgaf7xr3r") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-bytes-0.1.2-alpha (c (n "tetsy-bytes") (v "0.1.2-alpha") (h "105cgph38ggb6daacwphl1vwbkvi31cy79jgvag94hbhxmylphi0") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-bytes-0.1.4 (c (n "tetsy-bytes") (v "0.1.4") (h "0h4lph50b9q9n5fmbv8mycdsjiarsrlj6qmg2gyw7ay5615729mi") (f (quote (("std") ("default" "std"))))))

