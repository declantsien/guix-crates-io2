(define-module (crates-io te ts tetsy-rocksdb) #:use-module (crates-io))

(define-public crate-tetsy-rocksdb-0.5.1 (c (n "tetsy-rocksdb") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tetsy-rocksdb-sys") (r "^0.5") (d #t) (k 0)))) (h "1k4wzhfb0fas163nnmhsg8a7q1q7s3rhrivr3yxfg4dk3fxxbds8")))

(define-public crate-tetsy-rocksdb-0.5.0 (c (n "tetsy-rocksdb") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "tetsy-rocksdb-sys") (r "^0.5") (d #t) (k 0)))) (h "1s66wzh4scnr58my5lvjbvypn4lwmyxpcgkql7ykif32z8cyjgs6") (f (quote (("valgrind") ("default"))))))

