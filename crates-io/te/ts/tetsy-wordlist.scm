(define-module (crates-io te ts tetsy-wordlist) #:use-module (crates-io))

(define-public crate-tetsy-wordlist-1.2.0 (c (n "tetsy-wordlist") (v "1.2.0") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "09acb8lyai7zz17q022hk0xqqyjb81khij7val5g9q334pmskm6p")))

(define-public crate-tetsy-wordlist-1.3.1 (c (n "tetsy-wordlist") (v "1.3.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "07c2dzzfdzq87rjgn7wk6grhf8h7nfp2660vax1a17rfsg3ckycj")))

