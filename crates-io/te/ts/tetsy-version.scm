(define-module (crates-io te ts tetsy-version) #:use-module (crates-io))

(define-public crate-tetsy-version-2.7.2 (c (n "tetsy-version") (v "2.7.2") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "target_info") (r "^0.1") (d #t) (k 0)) (d (n "tetsy-bytes") (r "^0.1") (d #t) (k 0)) (d (n "tetsy-rlp") (r "^0.4.5") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 1)) (d (n "vergen") (r "^3.0.4") (d #t) (k 1)))) (h "0apdllsl2rw08n0fm86xa85y2lnajg7vlhwlqmx87azd2yzni7v6") (f (quote (("final"))))))

