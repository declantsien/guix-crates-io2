(define-module (crates-io te ts tetsy-wasm-gc) #:use-module (crates-io))

(define-public crate-tetsy-wasm-gc-0.1.6 (c (n "tetsy-wasm-gc") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.5") (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tetsy-wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "12bs2bjvix7x9r0cf29dr6qqa1amvgwkhzw6682pi4c2i2nag7kv")))

