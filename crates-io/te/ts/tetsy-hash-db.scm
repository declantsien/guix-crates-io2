(define-module (crates-io te ts tetsy-hash-db) #:use-module (crates-io))

(define-public crate-tetsy-hash-db-0.15.2 (c (n "tetsy-hash-db") (v "0.15.2") (h "1navbd9mrsf9c4qskkpm7g7pxkj04q45xbcs001xp4s26m2vzmxj") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-hash-db-0.15.2-alpha (c (n "tetsy-hash-db") (v "0.15.2-alpha") (h "0qj3fw0pnjqlbqi3h1xls3hca1wcp89g5jlm2vqqv18xqsf78iv5") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-hash-db-0.15.2-beta (c (n "tetsy-hash-db") (v "0.15.2-beta") (h "0f08d4ap1cmnnzn3fsl6is3983dpywzk8dzsz9716gnq8ddsay93") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-hash-db-0.15.3 (c (n "tetsy-hash-db") (v "0.15.3") (h "1xglj54z2wy912amw4jaa5hc714xb3mm2ski9mwyqnrnh3kszgb3") (f (quote (("std") ("default" "std"))))))

