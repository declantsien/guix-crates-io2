(define-module (crates-io te ts tetsy-codec) #:use-module (crates-io))

(define-public crate-tetsy-codec-3.0.0 (c (n "tetsy-codec") (v "3.0.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tetsy-codec-derive") (r "^3.0") (o #t) (k 0)) (d (n "tetsy-codec-derive") (r "^3.0") (k 2)))) (h "0w65cx2b1cc8fhk3xkbz4kx73hp2fh96137lz3z0pvd6n5pl65c2") (f (quote (("std" "serde") ("full") ("derive" "tetsy-codec-derive") ("default" "std"))))))

