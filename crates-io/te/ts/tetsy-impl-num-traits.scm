(define-module (crates-io te ts tetsy-impl-num-traits) #:use-module (crates-io))

(define-public crate-tetsy-impl-num-traits-0.1.0 (c (n "tetsy-impl-num-traits") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "uint-crate") (r "^0.9.0") (k 0)))) (h "1y3ckc5lpk7zdcvwpvysg2pd7490ny4zpxpcfa8crq7xhjixb5f4")))

(define-public crate-tetsy-impl-num-traits-0.1.0-alpha (c (n "tetsy-impl-num-traits") (v "0.1.0-alpha") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "uint-crate") (r "^0.9.0-alpha") (k 0)))) (h "1angcpyz5lq9h36yd3k5asa4a8bs86v7sfqdysj9b2rnz4fg9282")))

(define-public crate-tetsy-impl-num-traits-0.1.1 (c (n "tetsy-impl-num-traits") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "uint-crate") (r "^0.9.1") (k 0)))) (h "1ac7v49bbb0524rmdmw0kzcmzb090j7bkfvw3nry1bzsv1cq8bcj")))

