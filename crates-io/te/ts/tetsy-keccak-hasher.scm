(define-module (crates-io te ts tetsy-keccak-hasher) #:use-module (crates-io))

(define-public crate-tetsy-keccak-hasher-0.15.2 (c (n "tetsy-keccak-hasher") (v "0.15.2") (d (list (d (n "tetsy-hash-db") (r "^0.15.2") (k 0)) (d (n "tetsy-hash256-std-hasher") (r "^0.15.2") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "0zhxb01wy7g5fw1flan8c9680qv7ma17mnhipa1yh60gmsybhd52") (f (quote (("std" "tetsy-hash-db/std") ("default" "std"))))))

(define-public crate-tetsy-keccak-hasher-0.1.1 (c (n "tetsy-keccak-hasher") (v "0.1.1") (d (list (d (n "tetsy-hash-db") (r "^0.15.2") (d #t) (k 0)) (d (n "tetsy-plain-hasher") (r "^0.2.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "090ffc8s8winw63b8gfxhl290wv1wzhzfk0874m1ggxmrjqfqa6n")))

(define-public crate-tetsy-keccak-hasher-0.15.3 (c (n "tetsy-keccak-hasher") (v "0.15.3") (d (list (d (n "tetsy-hash-db") (r "^0.15.2-beta") (k 0)) (d (n "tetsy-hash256-std-hasher") (r "^0.15.2-alpha") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "17mgwri1s0w1fikmbpkwv38ai4d2hlqbj5cf2548z2prqbrwa2r3") (f (quote (("std" "tetsy-hash-db/std") ("default" "std"))))))

(define-public crate-tetsy-keccak-hasher-0.15.4 (c (n "tetsy-keccak-hasher") (v "0.15.4") (d (list (d (n "tetsy-hash-db") (r "^0.15.3") (k 0)) (d (n "tetsy-hash256-std-hasher") (r "^0.15.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1w42dx53hfa0mvji6z2hc1hw1z3pryky2h2qmgc01659v6jd1vy4") (f (quote (("std" "tetsy-hash-db/std") ("default" "std"))))))

