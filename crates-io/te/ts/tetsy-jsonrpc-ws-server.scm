(define-module (crates-io te ts tetsy-jsonrpc-ws-server) #:use-module (crates-io))

(define-public crate-tetsy-jsonrpc-ws-server-14.2.0 (c (n "tetsy-jsonrpc-ws-server") (v "14.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tetsy-jsonrpc-core") (r "^14.2") (d #t) (k 0)) (d (n "tetsy-jsonrpc-server-utils") (r "^14.2") (d #t) (k 0)) (d (n "ws") (r "^0.9") (d #t) (k 0)))) (h "1q7sc1jfqjwljgc8zfa2mb80j0x4karkx79n3isqi9q291hp3hm1")))

(define-public crate-tetsy-jsonrpc-ws-server-14.2.1 (c (n "tetsy-jsonrpc-ws-server") (v "14.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tetsy-jsonrpc-core") (r "^14.2") (d #t) (k 0)) (d (n "tetsy-jsonrpc-server-utils") (r "^14.2") (d #t) (k 0)) (d (n "ws") (r "^0.9") (d #t) (k 0)))) (h "03lbc00hkzr3dmvq3zkf6fm5np8k79pp4ys600864cqk68r46dmv")))

(define-public crate-tetsy-jsonrpc-ws-server-15.1.0 (c (n "tetsy-jsonrpc-ws-server") (v "15.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tetsy-jsonrpc-core") (r "^15.1.0") (d #t) (k 0)) (d (n "tetsy-jsonrpc-server-utils") (r "^15.1.0") (d #t) (k 0)) (d (n "tetsy-ws") (r "^0.10") (d #t) (k 0)))) (h "19i61zmj485s8asjwmi8dk53n2v1nx084rby06wq9pgqmm840axn")))

