(define-module (crates-io te ts tetsy-wasm) #:use-module (crates-io))

(define-public crate-tetsy-wasm-0.42.1 (c (n "tetsy-wasm") (v "0.42.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1lbwq0haygzbn61a62cbipl9przky7mqbaf530ig372h6whskhnl") (f (quote (("std") ("simd") ("sign_ext") ("multi_value") ("default" "std") ("bulk") ("atomics"))))))

(define-public crate-tetsy-wasm-0.31.1 (c (n "tetsy-wasm") (v "0.31.1") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0i172a1qr3ah4y49clsj8maw6bfsamw4r52ryk6sigsx0yb7w6xq") (f (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-tetsy-wasm-0.41.0 (c (n "tetsy-wasm") (v "0.41.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0ha6yyljvdmc6s6nsr408iqilgm3dgi1bai9kmds84di6pi4s7fs") (f (quote (("std") ("simd") ("sign_ext") ("default" "std") ("bulk") ("atomics"))))))

