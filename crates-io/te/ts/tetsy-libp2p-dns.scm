(define-module (crates-io te ts tetsy-libp2p-dns) #:use-module (crates-io))

(define-public crate-tetsy-libp2p-dns-0.0.0 (c (n "tetsy-libp2p-dns") (v "0.0.0") (h "13xn984jkw3y7pwd9ynqm5bnl261wf0yx6ml21q18bgyd4a145vc") (y #t)))

(define-public crate-tetsy-libp2p-dns-0.27.0 (c (n "tetsy-libp2p-dns") (v "0.27.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)))) (h "0bj12b47b64cmypjrsnyrp6s21jlnyj4ii70n8zfkp71a5cra3by")))

(define-public crate-tetsy-libp2p-dns-0.27.1 (c (n "tetsy-libp2p-dns") (v "0.27.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)))) (h "13pmndi30q0ab249s9sc5fv81y0f8bvri7a0pl10dhnv3pdhmsp7")))

(define-public crate-tetsy-libp2p-dns-0.27.2 (c (n "tetsy-libp2p-dns") (v "0.27.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.1") (d #t) (k 0)))) (h "16d6m136381lnh1cn8gwv5gvslk4idly0xclg809v0hiic46rdx0")))

