(define-module (crates-io te ts tetsy-keccak-hash) #:use-module (crates-io))

(define-public crate-tetsy-keccak-hash-0.7.0 (c (n "tetsy-keccak-hash") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tetsy-primitive-types") (r "^0.9") (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1gdz2cm02vly22najlh0b2ixibfrcq5x0fa84kk75r4z8nz2aap7") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-keccak-hash-0.4.0 (c (n "tetsy-keccak-hash") (v "0.4.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tetsy-primitive-types") (r "^0.6") (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "066lxpk92afza1hphqygwqjj40d8s0v64d9zymi6l8fi1b6rayxa") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-keccak-hash-0.6.0 (c (n "tetsy-keccak-hash") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tetsy-primitive-types") (r "^0.8.0") (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1xiv6b6ras4xdsjafph87z9i91ml89zw2vnpggjdfz8p8zy4dhch") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-keccak-hash-0.7.1 (c (n "tetsy-keccak-hash") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tetsy-primitive-types") (r "^0.9.1") (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1mhkhzvqkj914aszx5rr7q1p50xnvf9h4lx6n3rmms02qw3kz4m0") (f (quote (("std") ("default" "std"))))))

