(define-module (crates-io te ts tetsy-wasm-gc-api) #:use-module (crates-io))

(define-public crate-tetsy-wasm-gc-api-0.1.11 (c (n "tetsy-wasm-gc-api") (v "0.1.11") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)) (d (n "tetsy-wasm") (r "^0.31.1") (d #t) (k 0)))) (h "1if7ca27g56578qci566vih2bja396yjm9n7sazw55ipj1llqrwq")))

