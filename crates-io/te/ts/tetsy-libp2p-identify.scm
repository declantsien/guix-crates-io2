(define-module (crates-io te ts tetsy-libp2p-identify) #:use-module (crates-io))

(define-public crate-tetsy-libp2p-identify-0.0.0 (c (n "tetsy-libp2p-identify") (v "0.0.0") (h "0gf79pj96q1g9s7qhvv21c8xbhrkphmch1dkdc16milgz16hb25z") (y #t)))

(define-public crate-tetsy-libp2p-identify-0.28.0 (c (n "tetsy-libp2p-identify") (v "0.28.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "tetsy-libp2p-swarm") (r "^0.28.0") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "1s83hgrp39aapwx8icryd622sg5j5kj6v2q82ngx2nw10n06nlk7")))

(define-public crate-tetsy-libp2p-identify-0.27.0 (c (n "tetsy-libp2p-identify") (v "0.27.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "tetsy-libp2p-swarm") (r "^0.27.0") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "1mq193vyhbdhdw52xgk4wqb5nsxqf3d0922cyl9krycfgqfx8k16")))

(define-public crate-tetsy-libp2p-identify-0.27.1 (c (n "tetsy-libp2p-identify") (v "0.27.1") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.1") (d #t) (k 0)) (d (n "tetsy-libp2p-swarm") (r "^0.27.1") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "02z213ppf8dzwzd7q7v5ikb7b83j5xh27nasklz9hcqcrx8mcsn0")))

