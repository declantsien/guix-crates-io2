(define-module (crates-io te ts tetsy-trie-db-test) #:use-module (crates-io))

(define-public crate-tetsy-trie-db-test-0.0.0 (c (n "tetsy-trie-db-test") (v "0.0.0") (h "08zd3001kzdi3i5603ywcs5208w3zmk0533wrhx1bz4q8cjjbirr")))

(define-public crate-tetsy-trie-db-test-0.23.0 (c (n "tetsy-trie-db-test") (v "0.23.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 0)) (d (n "rustc-hex") (r "^2.1.0") (d #t) (k 0)) (d (n "tetsy-hash-db") (r "^0.15.3") (d #t) (k 0)) (d (n "tetsy-keccak-hasher") (r "^0.15.4") (d #t) (k 0)) (d (n "tetsy-memory-db") (r "^0.26.0") (d #t) (k 0)) (d (n "tetsy-reference-trie") (r "^0.23.0") (d #t) (k 0)) (d (n "tetsy-trie-db") (r "^0.22.3") (d #t) (k 0)) (d (n "tetsy-trie-root") (r "^0.16.1") (d #t) (k 0)) (d (n "tetsy-trie-standardmap") (r "^0.15.3") (d #t) (k 0)))) (h "02a8afgl407w3ygm285s6c8vfqwn9zp3fba39fsrisy94anij3yh")))

