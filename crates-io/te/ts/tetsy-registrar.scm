(define-module (crates-io te ts tetsy-registrar) #:use-module (crates-io))

(define-public crate-tetsy-registrar-0.0.1 (c (n "tetsy-registrar") (v "0.0.1") (d (list (d (n "call-contract") (r "^0.1.0") (d #t) (k 0) (p "vapcore-call-contract")) (d (n "tetsy-keccak-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "types") (r "^0.1.0") (d #t) (k 0) (p "common-types")) (d (n "vapabi") (r "^9.0.1") (d #t) (k 0)) (d (n "vapabi-contract") (r "^9.0.0") (d #t) (k 0)) (d (n "vapabi-derive") (r "^9.0.1") (d #t) (k 0)))) (h "0kmp5h90qywdlqddhdzkyfb9sg07kc6vyqkqjc68ykb98mnz8sr5")))

