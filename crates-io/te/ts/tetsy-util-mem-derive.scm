(define-module (crates-io te ts tetsy-util-mem-derive) #:use-module (crates-io))

(define-public crate-tetsy-util-mem-derive-0.1.0 (c (n "tetsy-util-mem-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1a715j1s1vk88jmadpzmsrrvqf39jdknp72jn4lcckm9nsm8fyrw")))

(define-public crate-tetsy-util-mem-derive-0.1.0-alpha (c (n "tetsy-util-mem-derive") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1r6n2ksgxpd5pymnvxniri0kg6vhkzz2c2qvn536ddjf1pf77ii7")))

(define-public crate-tetsy-util-mem-derive-0.1.0-beta (c (n "tetsy-util-mem-derive") (v "0.1.0-beta") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0ja001fjrn7mw97wwspy5a4vny1y6v449kdfg6ql728s45rdm82m")))

(define-public crate-tetsy-util-mem-derive-0.1.1 (c (n "tetsy-util-mem-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "06dx9hb0hmsmcxh74d67q83fxk2px287wcj35adkib1yswqa4nva")))

