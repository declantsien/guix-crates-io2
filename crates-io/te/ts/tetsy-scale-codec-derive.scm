(define-module (crates-io te ts tetsy-scale-codec-derive) #:use-module (crates-io))

(define-public crate-tetsy-scale-codec-derive-2.0.0 (c (n "tetsy-scale-codec-derive") (v "2.0.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1fx9b96yjdv30a2ra4dmlx4wdw8svz4as6l65z27kx7kxrqmwlb8")))

(define-public crate-tetsy-scale-codec-derive-1.0.2 (c (n "tetsy-scale-codec-derive") (v "1.0.2") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0fw340zdmy50fv3i85hx20qvsy5i13jcsxw9rvg8bwx4ylwk0w0k") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-scale-codec-derive-2.0.1 (c (n "tetsy-scale-codec-derive") (v "2.0.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "tetsy-scale-codec") (r "^2.0.0-update2") (d #t) (k 2)))) (h "13hpy6k409s52kiz1ql37v26rzv97ipl0wx0gzr4ax670xcg3iwk")))

(define-public crate-tetsy-scale-codec-derive-1.2.0 (c (n "tetsy-scale-codec-derive") (v "1.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0j93jm0vlq2n08ckp8ajx677r0g4r02jh18lycr6d1yshgvrd7mr")))

