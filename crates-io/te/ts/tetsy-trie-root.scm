(define-module (crates-io te ts tetsy-trie-root) #:use-module (crates-io))

(define-public crate-tetsy-trie-root-0.16.0 (c (n "tetsy-trie-root") (v "0.16.0") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "tetsy-hash-db") (r "^0.15.2") (k 0)) (d (n "tetsy-keccak-hasher") (r "^0.15.2") (d #t) (k 2)) (d (n "tetsy-trie-standardmap") (r "^0.15.2") (d #t) (k 2)))) (h "0gpc47mvfriqhqw3ydlw4gajaf34sninzc876xzbpynszmanbcb9") (f (quote (("std" "tetsy-hash-db/std") ("default" "std"))))))

(define-public crate-tetsy-trie-root-0.16.0-alpha (c (n "tetsy-trie-root") (v "0.16.0-alpha") (d (list (d (n "tetsy-hash-db") (r "^0.15.2-beta") (k 0)))) (h "03rw92z2ki3al9iaaifms9jy38r28mxqlmyiq8dk2ihc0kx146xd") (f (quote (("std" "tetsy-hash-db/std") ("default" "std"))))))

(define-public crate-tetsy-trie-root-0.16.0-beta (c (n "tetsy-trie-root") (v "0.16.0-beta") (d (list (d (n "tetsy-hash-db") (r "^0.15.2-beta") (k 0)))) (h "1l5hddxbgpm2dj1mp8i8jrdvfmnh0apvbybic31pyg44gjpynzsz") (f (quote (("std" "tetsy-hash-db/std") ("default" "std"))))))

(define-public crate-tetsy-trie-root-0.16.1 (c (n "tetsy-trie-root") (v "0.16.1") (d (list (d (n "tetsy-hash-db") (r "^0.15.3") (k 0)))) (h "0zaaf4wrrz2rmkxfi6pwqd4fh5k328bxjijhpxjalq6rgpv0s193") (f (quote (("std" "tetsy-hash-db/std") ("default" "std"))))))

