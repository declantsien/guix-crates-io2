(define-module (crates-io te ts tetsy-finality-grandpa) #:use-module (crates-io))

(define-public crate-tetsy-finality-grandpa-0.0.0 (c (n "tetsy-finality-grandpa") (v "0.0.0") (h "1jms85hkpjl3404p85885qjxprp80xkn28j0vwcaz84v5jr02j4c")))

(define-public crate-tetsy-finality-grandpa-0.13.0 (c (n "tetsy-finality-grandpa") (v "0.13.0") (d (list (d (n "either") (r "^1.6") (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures-timer") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.2") (k 0) (p "num-traits")) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tetsy-scale-codec") (r "^2.0.1") (f (quote ("derive"))) (o #t) (k 0)))) (h "1bmwyf83haxl41p9ay99mn1psy61njsydpqpv8k47w8f80qlba9w") (f (quote (("test-helpers" "fuzz-helpers" "rand" "std") ("std" "tetsy-scale-codec/std" "num/std" "parking_lot" "log" "futures-timer" "futures/executor") ("fuzz-helpers") ("derive-codec" "tetsy-scale-codec") ("default" "std"))))))

