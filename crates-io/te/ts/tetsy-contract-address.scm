(define-module (crates-io te ts tetsy-contract-address) #:use-module (crates-io))

(define-public crate-tetsy-contract-address-0.6.0 (c (n "tetsy-contract-address") (v "0.6.0") (d (list (d (n "tetsy-keccak-hash") (r "^0.7") (k 0)) (d (n "tetsy-rlp") (r "^0.5") (d #t) (k 0)) (d (n "vapory-types") (r "^0.11.0") (d #t) (k 0)))) (h "1784p65p3hpaff4911czgsv5jakmhrz0203xn6ka7c2sikgn2w9a") (f (quote (("external_doc") ("default"))))))

(define-public crate-tetsy-contract-address-0.3.0 (c (n "tetsy-contract-address") (v "0.3.0") (d (list (d (n "tetsy-keccak-hash") (r "^0.4") (k 0)) (d (n "tetsy-rlp") (r "^0.4") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8") (d #t) (k 0)))) (h "0xl0hjijs03ydfylx0q8l19iaf010ql2s883mbrz9gsvr1878qry") (f (quote (("external_doc") ("default"))))))

(define-public crate-tetsy-contract-address-0.5.0 (c (n "tetsy-contract-address") (v "0.5.0") (d (list (d (n "tetsy-keccak-hash") (r "^0.6") (k 0)) (d (n "tetsy-rlp") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "vapory-types") (r "^0.10.0") (d #t) (k 0)))) (h "0761agngl2fvj0cpyynnfnc2sjigg2b8s4rb43l0qlx5z40yrsg5") (f (quote (("external_doc") ("default"))))))

(define-public crate-tetsy-contract-address-0.6.1 (c (n "tetsy-contract-address") (v "0.6.1") (d (list (d (n "tetsy-keccak-hash") (r "^0.7.1") (k 0)) (d (n "tetsy-rlp") (r "^0.5.1") (d #t) (k 0)) (d (n "vapory-types") (r "^0.11.1") (d #t) (k 0)))) (h "0smgr4xiw8vssmjj2d8hqvx66h6v86nzb2qw8k085jm20a0fdjad") (f (quote (("external_doc") ("default"))))))

