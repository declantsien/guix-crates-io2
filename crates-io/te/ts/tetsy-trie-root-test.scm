(define-module (crates-io te ts tetsy-trie-root-test) #:use-module (crates-io))

(define-public crate-tetsy-trie-root-test-0.0.0 (c (n "tetsy-trie-root-test") (v "0.0.0") (h "0k1r0z4946f1sll8llf30hng58bjvbgl36rq7jqjiwyarypbi2fg")))

(define-public crate-tetsy-trie-root-test-0.17.0 (c (n "tetsy-trie-root-test") (v "0.17.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 0)) (d (n "tetsy-hash-db") (r "^0.15.3") (d #t) (k 0)) (d (n "tetsy-keccak-hasher") (r "^0.15.4") (d #t) (k 0)) (d (n "tetsy-reference-trie") (r "^0.23.0") (d #t) (k 0)) (d (n "tetsy-trie-root") (r "^0.16.1") (d #t) (k 0)) (d (n "tetsy-trie-standardmap") (r "^0.15.3") (d #t) (k 0)))) (h "1bsga48asiy9q4sj9wj58b70930dshvkl783jnd2lhv9gl977fpp")))

