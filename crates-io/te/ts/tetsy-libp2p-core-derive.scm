(define-module (crates-io te ts tetsy-libp2p-core-derive) #:use-module (crates-io))

(define-public crate-tetsy-libp2p-core-derive-0.0.0 (c (n "tetsy-libp2p-core-derive") (v "0.0.0") (h "08mm7s8cm06qhcypqppwi1hpbi11j3b0gyckhsd947s0v89k3vjd")))

(define-public crate-tetsy-libp2p-core-derive-0.21.0 (c (n "tetsy-libp2p-core-derive") (v "0.21.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0c67sksgvnnldl26pg8n69zsl0vfhc4zlrna2j4q7psypyczcxpx")))

(define-public crate-tetsy-libp2p-core-derive-0.21.1 (c (n "tetsy-libp2p-core-derive") (v "0.21.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1nrz5blwbch5dfqk1882aiy27c8d7h5drjrh42irz1q97f9ygwbk")))

