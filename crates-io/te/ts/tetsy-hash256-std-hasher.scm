(define-module (crates-io te ts tetsy-hash256-std-hasher) #:use-module (crates-io))

(define-public crate-tetsy-hash256-std-hasher-0.15.2 (c (n "tetsy-hash256-std-hasher") (v "0.15.2") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.1") (d #t) (k 0)))) (h "1bdp6qcjspyndp0zijidycn4y2rz3jjazq5arihbbkf073bjwi54") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-hash256-std-hasher-0.15.2-alpha (c (n "tetsy-hash256-std-hasher") (v "0.15.2-alpha") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.1") (d #t) (k 0)))) (h "0llvlpc9rzyvckr8ii3wladm2153kn2sda23h2dc7gizxy6a4i3n") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetsy-hash256-std-hasher-0.15.3 (c (n "tetsy-hash256-std-hasher") (v "0.15.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.1") (d #t) (k 0)))) (h "1rzls3h8jqxpfaqfxq04ad0cdpf09q5s7p2hi5g14gkhrr4j96ap") (f (quote (("std") ("default" "std"))))))

