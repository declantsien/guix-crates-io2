(define-module (crates-io te ts tetsy-fetch) #:use-module (crates-io))

(define-public crate-tetsy-fetch-0.1.0 (c (n "tetsy-fetch") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "~0.12.9") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "01rcm57cp698g798bmgh28v4px26bwl6ada4s72v7zzha5skc0v3") (f (quote (("default"))))))

