(define-module (crates-io te ts tetsy-trie-standardmap) #:use-module (crates-io))

(define-public crate-tetsy-trie-standardmap-0.15.2 (c (n "tetsy-trie-standardmap") (v "0.15.2") (d (list (d (n "tetsy-hash-db") (r "^0.15.2") (d #t) (k 0)) (d (n "tetsy-keccak-hasher") (r "^0.15.2") (d #t) (k 0)))) (h "0h0iwgw8fdlh9zzr3r6iryrd99h9q6bkfg1hl9b0xj3m0cqi39j9")))

(define-public crate-tetsy-trie-standardmap-0.15.2-alpha (c (n "tetsy-trie-standardmap") (v "0.15.2-alpha") (d (list (d (n "tetsy-hash-db") (r "^0.15.2-beta") (d #t) (k 0)) (d (n "tetsy-keccak-hasher") (r "^0.15.2") (d #t) (k 0)))) (h "10j9va3ffbqd2qa8bcdzw1969cjdqb6yh1ccf2fjrb119056dmmn")))

(define-public crate-tetsy-trie-standardmap-0.15.3 (c (n "tetsy-trie-standardmap") (v "0.15.3") (d (list (d (n "tetsy-hash-db") (r "^0.15.3") (d #t) (k 0)) (d (n "tetsy-keccak-hasher") (r "^0.15.4") (d #t) (k 0)))) (h "094qky76y6f956pckirvihfgzx2nrbn38265aa5g2xlpyak5j6ri")))

