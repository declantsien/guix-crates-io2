(define-module (crates-io te ts tetsy-libp2p-relay) #:use-module (crates-io))

(define-public crate-tetsy-libp2p-relay-0.0.0 (c (n "tetsy-libp2p-relay") (v "0.0.0") (h "0xn3plxgxw6kp0q1cp05yfq255c6wx1mm6iyra36k9h66y1hyvmj") (y #t)))

(define-public crate-tetsy-libp2p-relay-0.1.0 (c (n "tetsy-libp2p-relay") (v "0.1.0") (d (list (d (n "asynchronous-codec") (r "^0.6") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-timer") (r "^3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.9") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27") (d #t) (k 0)) (d (n "tetsy-libp2p-swarm") (r "^0.28") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (f (quote ("asynchronous_codec"))) (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "0h9i3bkkl41dbgg8bxfv16zqm7xa9p9xi3nniimmx31vmxyqpf1v")))

