(define-module (crates-io te ts tetsy-daemonize) #:use-module (crates-io))

(define-public crate-tetsy-daemonize-0.3.0 (c (n "tetsy-daemonize") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "0a1c0pvsarbm30l89yf1mlsfdcp6klamilibv9apic312vl9rdcy")))

