(define-module (crates-io te ts tetsy-path) #:use-module (crates-io))

(define-public crate-tetsy-path-0.1.3 (c (n "tetsy-path") (v "0.1.3") (d (list (d (n "home") (r "^0.5.1") (d #t) (k 0)))) (h "00xvciq4k7srcnmpfs0ccvvjidn7jvfhg61iw7swd0asm725swld")))

(define-public crate-tetsy-path-0.1.2 (c (n "tetsy-path") (v "0.1.2") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)))) (h "0blwnzfsl33a7cabnqbv8nvl1wkhbg6jpzvfc0z7lq068if0fnhb")))

(define-public crate-tetsy-path-0.1.3-alpha (c (n "tetsy-path") (v "0.1.3-alpha") (d (list (d (n "home") (r "^0.5.1") (d #t) (k 0)))) (h "1k1x9qx9ylv4ifwh4a6m3d6rswj6qcx4y5wbjssqxh65mm9j4m91")))

(define-public crate-tetsy-path-0.1.4 (c (n "tetsy-path") (v "0.1.4") (d (list (d (n "home") (r "^0.5.1") (d #t) (k 0)))) (h "1xx0a1qci00ijj6nsa5avnc83fi4sghlvl93fhjxsrb27zjgfzcr")))

