(define-module (crates-io te ts tetsy-jsonrpc-pubsub-examples) #:use-module (crates-io))

(define-public crate-tetsy-jsonrpc-pubsub-examples-15.1.0 (c (n "tetsy-jsonrpc-pubsub-examples") (v "15.1.0") (d (list (d (n "tetsy-jsonrpc-core") (r "^15.1.0") (d #t) (k 0)) (d (n "tetsy-jsonrpc-ipc-server") (r "^15.1.0") (d #t) (k 0)) (d (n "tetsy-jsonrpc-pubsub") (r "^15.1.0") (d #t) (k 0)) (d (n "tetsy-jsonrpc-ws-server") (r "^15.1.0") (d #t) (k 0)))) (h "0z7r8gjx3cfhszm35syvr4mbfqgfckjr25r6d06y16vfnxrjrpqg")))

