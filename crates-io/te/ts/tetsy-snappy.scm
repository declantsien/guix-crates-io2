(define-module (crates-io te ts tetsy-snappy) #:use-module (crates-io))

(define-public crate-tetsy-snappy-0.1.0 (c (n "tetsy-snappy") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "tetsy-snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "1m3dg0s0avpylrpnh2z9xi8mk7nkw20770yzy26wz2mvqh167jsy")))

