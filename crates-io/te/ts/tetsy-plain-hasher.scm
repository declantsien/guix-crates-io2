(define-module (crates-io te ts tetsy-plain-hasher) #:use-module (crates-io))

(define-public crate-tetsy-plain-hasher-0.3.0 (c (n "tetsy-plain-hasher") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (k 0)))) (h "0hz7p3j0h6bxf3qnn08kpv9xg6jafvrzrw3v8brb06xllzg4lyr5")))

(define-public crate-tetsy-plain-hasher-0.2.1 (c (n "tetsy-plain-hasher") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crunchy") (r "^0.2") (k 0)))) (h "0f77mki4z9amzpp76d9kiw5kahrz5cpmkb641a7mbzcgiwyjrpr6") (f (quote (("std" "crunchy/std") ("default" "std"))))))

(define-public crate-tetsy-plain-hasher-0.3.0-alpha (c (n "tetsy-plain-hasher") (v "0.3.0-alpha") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (k 0)))) (h "14c759gd2qfd00kmzhfjh0f7ig5j09hbcymy0rq31lb8gxhgdsh3")))

(define-public crate-tetsy-plain-hasher-0.3.1 (c (n "tetsy-plain-hasher") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (k 0)))) (h "0lqawyvfb4v288fxzmnizsz5w5ccx3q1biln59vc1fzxp7b7ilxp")))

