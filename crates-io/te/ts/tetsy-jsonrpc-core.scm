(define-module (crates-io te ts tetsy-jsonrpc-core) #:use-module (crates-io))

(define-public crate-tetsy-jsonrpc-core-14.2.0 (c (n "tetsy-jsonrpc-core") (v "14.2.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sh3ylw1cr7p5p00hlxm1wg6k7a8dd84yp0lxyywvgvdk9y0hbd8") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision"))))))

(define-public crate-tetsy-jsonrpc-core-14.2.1 (c (n "tetsy-jsonrpc-core") (v "14.2.1") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iz0z58w45nm2wq8dhczx558bkwj91v7prrvgncnl789qjv8wvi1") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision"))))))

(define-public crate-tetsy-jsonrpc-core-15.1.0 (c (n "tetsy-jsonrpc-core") (v "15.1.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ng4x66r71mf9hd8mksjprb17c344k3zjk7mcvnd9fh11lhk9bky") (f (quote (("arbitrary_precision" "serde_json/arbitrary_precision"))))))

