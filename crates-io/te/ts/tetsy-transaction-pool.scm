(define-module (crates-io te ts tetsy-transaction-pool) #:use-module (crates-io))

(define-public crate-tetsy-transaction-pool-2.0.3 (c (n "tetsy-transaction-pool") (v "2.0.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.11.0") (d #t) (k 2)))) (h "197hlzn7iacdan5lsvj2jsi9yvi1j4j9f9ymwq9g4gf5h7fp2gh2")))

(define-public crate-tetsy-transaction-pool-2.0.1 (c (n "tetsy-transaction-pool") (v "2.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "tetsy-trace-time") (r "^0.1") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8") (d #t) (k 2)))) (h "164iqcyq5j99bqrl3hfdbb6irg9gyd1xd9fyhh34ggsdx2462qk2")))

(define-public crate-tetsy-transaction-pool-2.0.3-alpha (c (n "tetsy-transaction-pool") (v "2.0.3-alpha") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.10.0") (d #t) (k 2)))) (h "1vq9lbibwki70xqg00gacryfp4iza1ds5mlr2s2brw34bvj7dxfw")))

(define-public crate-tetsy-transaction-pool-2.0.4 (c (n "tetsy-transaction-pool") (v "2.0.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.11.1") (d #t) (k 2)))) (h "0ql3y2fnm410idp1a45fz77ddxg01lcy2k3jaa2rwfj14231b8i8")))

