(define-module (crates-io te ts tetsy-impl-serde) #:use-module (crates-io))

(define-public crate-tetsy-impl-serde-0.3.1 (c (n "tetsy-impl-serde") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "uint-crate") (r "^0.9.0") (d #t) (k 2)))) (h "0l8dy4as7wfs3a3pa52h2bbc1wmhyl9fwh6hax60fbnl98jrrgrb") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-tetsy-impl-serde-0.2.2 (c (n "tetsy-impl-serde") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uint-crate") (r "^0.8.1") (d #t) (k 2)))) (h "1rdi19xv97kyrf1zzq2b5cjlfvmzrn8k1dg6wqik9smkggs9p13k")))

(define-public crate-tetsy-impl-serde-0.3.0 (c (n "tetsy-impl-serde") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "uint-crate") (r "^0.9.0-alpha") (d #t) (k 2)))) (h "1f9nyl3lylid9qjpci8fgc21hjvdxa35l6s476ffivhlb512jm1v") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-tetsy-impl-serde-0.3.2 (c (n "tetsy-impl-serde") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 2)) (d (n "uint-crate") (r "^0.9.1") (d #t) (k 2)))) (h "1wb2y7g2lb7018qf2x4h8i73p2hqx3n8mh04kag7awga5yx6yrcx") (f (quote (("std" "serde/std") ("default" "std"))))))

