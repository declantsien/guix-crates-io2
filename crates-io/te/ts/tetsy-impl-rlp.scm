(define-module (crates-io te ts tetsy-impl-rlp) #:use-module (crates-io))

(define-public crate-tetsy-impl-rlp-0.3.0 (c (n "tetsy-impl-rlp") (v "0.3.0") (d (list (d (n "tetsy-rlp") (r "^0.5") (k 0)))) (h "0i7nrv4yvaixcbmvyz245xr231m1qzsj5yj4a45z2cyv082bwv2r") (f (quote (("std" "tetsy-rlp/std") ("default" "std"))))))

(define-public crate-tetsy-impl-rlp-0.2.0 (c (n "tetsy-impl-rlp") (v "0.2.0") (d (list (d (n "tetsy-rlp") (r "^0.4.2") (k 0)))) (h "16jcjzkzsf0wfknk8g8b3cyavk7w95ija8p10vn7gvklj578kn8h") (f (quote (("std" "tetsy-rlp/std") ("default" "std"))))))

(define-public crate-tetsy-impl-rlp-0.3.0-alpha (c (n "tetsy-impl-rlp") (v "0.3.0-alpha") (d (list (d (n "tetsy-rlp") (r "^0.5.0-alpha") (k 0)))) (h "1g7kylb1507y1d303hlphi8zqn7s963aq0b31mdi60s5c14i2h4i") (f (quote (("std" "tetsy-rlp/std") ("default" "std"))))))

(define-public crate-tetsy-impl-rlp-0.3.1 (c (n "tetsy-impl-rlp") (v "0.3.1") (d (list (d (n "tetsy-rlp") (r "^0.5.1") (k 0)))) (h "1lhd7ic1rgikqy5zhd81nzks3g9k2iza5sc2yfjz3wql9c29ggci") (f (quote (("std" "tetsy-rlp/std") ("default" "std"))))))

