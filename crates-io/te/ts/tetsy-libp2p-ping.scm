(define-module (crates-io te ts tetsy-libp2p-ping) #:use-module (crates-io))

(define-public crate-tetsy-libp2p-ping-0.0.0 (c (n "tetsy-libp2p-ping") (v "0.0.0") (h "0p98mbh6fskc0prdqv04mmsjdh5l6jxnfxnzww43n5lqsjcjx100") (y #t)))

(define-public crate-tetsy-libp2p-ping-0.28.0 (c (n "tetsy-libp2p-ping") (v "0.28.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "tetsy-libp2p-swarm") (r "^0.28.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "1g5ln7phm98ww8pqhp1yx4n7w7ps3k2jgx0h45950rgiqrnfmrvb")))

(define-public crate-tetsy-libp2p-ping-0.27.0 (c (n "tetsy-libp2p-ping") (v "0.27.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "tetsy-libp2p-swarm") (r "^0.27.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "12zd4qidkxb7qn5cfydbcibgs03b9ks7ssrrrmifwhw8na309i44")))

(define-public crate-tetsy-libp2p-ping-0.27.1 (c (n "tetsy-libp2p-ping") (v "0.27.1") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.1") (d #t) (k 0)) (d (n "tetsy-libp2p-swarm") (r "^0.27.1") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "1yc775677ixbi2lma8qwyw1l7idadad7pzsxybm71q3hf76m5zg5")))

