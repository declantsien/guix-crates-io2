(define-module (crates-io te ts tetsy-scale-info-derive) #:use-module (crates-io))

(define-public crate-tetsy-scale-info-derive-0.3.0 (c (n "tetsy-scale-info-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1kn6k90nf8ysmp9wkbcb1xzrch3iy6mvsy8z720nv47wdrkrg4rs")))

(define-public crate-tetsy-scale-info-derive-0.2.0 (c (n "tetsy-scale-info-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vlwsbyl3ka1nv2rndqis2ai66zks3z4dwxdqgcf1f7rlrda5si4")))

