(define-module (crates-io te ts tetsy-wasm-testsuite) #:use-module (crates-io))

(define-public crate-tetsy-wasm-testsuite-0.0.0 (c (n "tetsy-wasm-testsuite") (v "0.0.0") (d (list (d (n "tetsy-wasm") (r "^0.31.1") (d #t) (k 0)) (d (n "wabt") (r "^0.2") (d #t) (k 0)))) (h "10kwwfjdpw4lp9k41567gyk7njs0szxqhnyxvby1gcrkiqfk1fx7")))

