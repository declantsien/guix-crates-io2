(define-module (crates-io te ts tetsy-db) #:use-module (crates-io))

(define-public crate-tetsy-db-0.2.0 (c (n "tetsy-db") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap2") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)))) (h "1x9s8ws8r4zv6cmhlrcx53fgrjv5ccd5rn5zsk7pwj9m7dpx3swv")))

(define-public crate-tetsy-db-0.1.2 (c (n "tetsy-db") (v "0.1.2") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0l2vyxfxmkcxr0b8y8fxjl8spzdv5y197w0l6jb4gqlndmr76k3g")))

