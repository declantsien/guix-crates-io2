(define-module (crates-io te ts tetsy-kvdb-memorydb) #:use-module (crates-io))

(define-public crate-tetsy-kvdb-memorydb-0.9.0 (c (n "tetsy-kvdb-memorydb") (v "0.9.0") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.9") (d #t) (k 0)) (d (n "tetsy-kvdb-shared-tests") (r "^0.7") (d #t) (k 2)) (d (n "tetsy-util-mem") (r "^0.9") (f (quote ("std"))) (k 0)))) (h "1ziqppvhy0icxy4q37m8980c7n5ydfh3ajn4fsnajahm1lx232c8") (f (quote (("default"))))))

(define-public crate-tetsy-kvdb-memorydb-0.1.2 (c (n "tetsy-kvdb-memorydb") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.1") (d #t) (k 0)))) (h "0yxkq7fgxndj16frfvqh0jf37d24zngkhfhikj2x3yhjpfz0lrdr")))

(define-public crate-tetsy-kvdb-memorydb-0.3.0 (c (n "tetsy-kvdb-memorydb") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.9.0") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.3") (d #t) (k 0)) (d (n "tetsy-kvdb-shared-tests") (r "^0.1") (d #t) (k 2)) (d (n "tetsy-util-mem") (r "^0.4") (d #t) (k 0)))) (h "1n0c2in0rs1gdxg20f8aci51zsw2kdm0wb7j9j1yql8hvifvnma7")))

(define-public crate-tetsy-kvdb-memorydb-0.3.1 (c (n "tetsy-kvdb-memorydb") (v "0.3.1") (d (list (d (n "parking_lot") (r "^0.9.0") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.3.1") (d #t) (k 0)) (d (n "tetsy-kvdb-shared-tests") (r "^0.1.1") (d #t) (k 2)) (d (n "tetsy-util-mem") (r "^0.4.2") (f (quote ("std"))) (k 0)))) (h "08d5rsaak34z74mk4p7vnn9afmblds8fm7b774m871wqypmplx0b")))

(define-public crate-tetsy-kvdb-memorydb-0.4.0 (c (n "tetsy-kvdb-memorydb") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.4") (d #t) (k 0)) (d (n "tetsy-kvdb-shared-tests") (r "^0.2") (d #t) (k 2)) (d (n "tetsy-util-mem") (r "^0.5.2") (f (quote ("std"))) (k 0)))) (h "0631s6fzl7f0zka5na67f3cjg4ja6mxckm3h6lmbwyvd3d4i4j52")))

(define-public crate-tetsy-kvdb-memorydb-0.3.2 (c (n "tetsy-kvdb-memorydb") (v "0.3.2") (d (list (d (n "parking_lot") (r "^0.9.0") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.3.2") (d #t) (k 0)) (d (n "tetsy-kvdb-shared-tests") (r "^0.1.2") (d #t) (k 2)) (d (n "tetsy-util-mem") (r "^0.4.2") (d #t) (k 0)))) (h "1b3rswvshn7awldwf6kny9bk1r29k1r5mrszpgvga7yy9fs37izr")))

(define-public crate-tetsy-kvdb-memorydb-0.8.0 (c (n "tetsy-kvdb-memorydb") (v "0.8.0") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.8") (d #t) (k 0)) (d (n "tetsy-kvdb-shared-tests") (r "^0.6") (d #t) (k 2)) (d (n "tetsy-util-mem") (r "^0.8") (f (quote ("std"))) (k 0)))) (h "0jraya7bnrn1wnqrg0xayp09xg3kl2dxgj5j593y9yjsa65s38dr") (f (quote (("default"))))))

(define-public crate-tetsy-kvdb-memorydb-0.9.1 (c (n "tetsy-kvdb-memorydb") (v "0.9.1") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.9.1") (d #t) (k 0)) (d (n "tetsy-kvdb-shared-tests") (r "^0.7.1") (d #t) (k 2)) (d (n "tetsy-util-mem") (r "^0.9.1") (f (quote ("std"))) (k 0)))) (h "1anzxhzm45gjpmyp3ifbkj7b7bjw28yn2b0x8lz4zb6lk3q1g6vy") (f (quote (("default"))))))

