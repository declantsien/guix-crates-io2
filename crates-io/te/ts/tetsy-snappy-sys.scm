(define-module (crates-io te ts tetsy-snappy-sys) #:use-module (crates-io))

(define-public crate-tetsy-snappy-sys-0.1.2 (c (n "tetsy-snappy-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gngy9lgqb15pnfxjnm2pf1k4skji7ml0j9495pzfm3w36ny1x0z") (f (quote (("default")))) (l "snappy")))

