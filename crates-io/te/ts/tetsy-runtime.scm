(define-module (crates-io te ts tetsy-runtime) #:use-module (crates-io))

(define-public crate-tetsy-runtime-0.1.0 (c (n "tetsy-runtime") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "1r11363cafvch7sx7iq9h8205pw5js2cxa0gfvmxhxfagwag2bnp")))

(define-public crate-tetsy-runtime-0.1.2 (c (n "tetsy-runtime") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (f (quote ("compat"))) (k 0)) (d (n "futures01") (r "^0.1") (d #t) (k 0) (p "futures")) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-compat") (r "^0.1") (d #t) (k 0)))) (h "0g1i0ify4byihzr7raa616b1g5bdbpah9x3cz6fa1b072mrh48jp") (f (quote (("test-helpers"))))))

