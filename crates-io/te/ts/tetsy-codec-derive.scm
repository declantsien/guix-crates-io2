(define-module (crates-io te ts tetsy-codec-derive) #:use-module (crates-io))

(define-public crate-tetsy-codec-derive-3.0.0 (c (n "tetsy-codec-derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zk7r114mzj0vw82jyn99j4scr4p7203hzlsjh4rmpy3vjm8zx4v") (f (quote (("std") ("default" "std"))))))

