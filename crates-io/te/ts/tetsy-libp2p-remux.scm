(define-module (crates-io te ts tetsy-libp2p-remux) #:use-module (crates-io))

(define-public crate-tetsy-libp2p-remux-0.30.0 (c (n "tetsy-libp2p-remux") (v "0.30.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "remux") (r "^0.8.0") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1awz1cc7csmqygigwd19sy2pfhwmfbybjrikcwmxrpjbg9s3964l")))

(define-public crate-tetsy-libp2p-remux-0.30.1 (c (n "tetsy-libp2p-remux") (v "0.30.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "remux") (r "^0.8.0") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12gva0bfc2k7lharv70nlk4xfjyqgrh7sdqkmndazralhjndk33l")))

