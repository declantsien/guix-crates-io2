(define-module (crates-io te ts tetsy-secp256k1) #:use-module (crates-io))

(define-public crate-tetsy-secp256k1-0.7.0 (c (n "tetsy-secp256k1") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 1)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 2)))) (h "1cnbnkz3rv5kjlf3hm0ay0fgrnsclg8i4cryindbpwznsv4r3dfj") (f (quote (("unstable") ("dev" "clippy") ("default"))))))

