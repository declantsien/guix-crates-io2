(define-module (crates-io te ts tetsy-libp2p-pnet) #:use-module (crates-io))

(define-public crate-tetsy-libp2p-pnet-0.0.0 (c (n "tetsy-libp2p-pnet") (v "0.0.0") (h "0vn6vb9jk0i1jpsg6hkyw041d4pc6w26zlhqcvv12czkls1r8lr6") (y #t)))

(define-public crate-tetsy-libp2p-pnet-0.20.0 (c (n "tetsy-libp2p-pnet") (v "0.20.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "salsa20") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "1c4sldis9hk5cwpcyihji13wdb8pzfv863nl92c0vsq21walr5b0")))

(define-public crate-tetsy-libp2p-pnet-0.20.1 (c (n "tetsy-libp2p-pnet") (v "0.20.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "salsa20") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "1d9f3byzmhyxig5ydv4zrmbi0qqyx19il0wapwp69zimp5294vsx")))

(define-public crate-tetsy-libp2p-pnet-0.20.2 (c (n "tetsy-libp2p-pnet") (v "0.20.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "salsa20") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "03bxjcg851d27ykb0zlr80g2zdybkynrsmfri3rg7qdknnnfcp1k")))

