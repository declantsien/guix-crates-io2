(define-module (crates-io te ts tetsy-impl-codec) #:use-module (crates-io))

(define-public crate-tetsy-impl-codec-0.5.0 (c (n "tetsy-impl-codec") (v "0.5.0") (d (list (d (n "tetsy-scale-codec") (r "^2.0.0") (k 0)))) (h "0f9lk5qnis072r2j4gykzryc43l4qr2fcid6ivmqd316blqjlcjp") (f (quote (("std" "tetsy-scale-codec/std") ("default" "std"))))))

(define-public crate-tetsy-impl-codec-0.4.1 (c (n "tetsy-impl-codec") (v "0.4.1") (d (list (d (n "tetsy-scale-codec") (r "^1.0.3") (k 0)))) (h "0px02ll7rvprcs1fknjfyczyrin6s8bsc6d65x5ir0rdaw4d77yg") (f (quote (("std" "tetsy-scale-codec/std") ("default" "std"))))))

(define-public crate-tetsy-impl-codec-0.4.0 (c (n "tetsy-impl-codec") (v "0.4.0") (d (list (d (n "tetsy-scale-codec") (r "^1.3.0") (k 0)))) (h "1ply9nm0wq717hqfc7fxphlqgzimngqv838dpv0f5l0psf6jsmjf") (f (quote (("std" "tetsy-scale-codec/std") ("default" "std"))))))

(define-public crate-tetsy-impl-codec-0.5.1 (c (n "tetsy-impl-codec") (v "0.5.1") (d (list (d (n "tetsy-scale-codec") (r "^2.0.0") (k 0)))) (h "1fkmmx0kdgd4l91gjxi3m7vmvgwmqdb8in3chnwxpmdjfc711pqx") (f (quote (("std" "tetsy-scale-codec/std") ("default" "std"))))))

