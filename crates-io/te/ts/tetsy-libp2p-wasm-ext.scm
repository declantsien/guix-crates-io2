(define-module (crates-io te ts tetsy-libp2p-wasm-ext) #:use-module (crates-io))

(define-public crate-tetsy-libp2p-wasm-ext-0.0.0 (c (n "tetsy-libp2p-wasm-ext") (v "0.0.0") (h "06v01sffrgs5h8zpdmkkl2j1wl69qmxy0blp79rk91p4spcwxpx6")))

(define-public crate-tetsy-libp2p-wasm-ext-0.27.0 (c (n "tetsy-libp2p-wasm-ext") (v "0.27.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.19") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "tetsy-send-wrapper") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.4") (d #t) (k 0)))) (h "11gdar5wkfkxxrmmpywmag9h3nbcs2n4nk2x646jxfcq4gk7v0dp") (f (quote (("websocket"))))))

(define-public crate-tetsy-libp2p-wasm-ext-0.27.1 (c (n "tetsy-libp2p-wasm-ext") (v "0.27.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.19") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.1") (d #t) (k 0)) (d (n "tetsy-send-wrapper") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.4") (d #t) (k 0)))) (h "0s7vpxxvaxprzs3g8s9aglg4hy4a02nmypz91wvp2i6if252v52s") (f (quote (("websocket"))))))

