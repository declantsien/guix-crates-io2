(define-module (crates-io te ts tetsy-rlp) #:use-module (crates-io))

(define-public crate-tetsy-rlp-0.5.0 (c (n "tetsy-rlp") (v "0.5.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (k 0)))) (h "0aaszj2qcrhfrxfzwxzrw7jjdbkrv22m6z5h5qmbnw5xw1fj3vp0") (f (quote (("std" "bytes/std" "rustc-hex/std") ("default" "std"))))))

(define-public crate-tetsy-rlp-0.4.2 (c (n "tetsy-rlp") (v "0.4.2") (d (list (d (n "rustc-hex") (r "^2.0") (k 0)))) (h "0hyl3ix8nlrsmwax8gm7bbdn5xmvpf07x8apvy37ljb4bn2cn8jg") (f (quote (("std" "rustc-hex/std") ("default" "std"))))))

(define-public crate-tetsy-rlp-0.4.5 (c (n "tetsy-rlp") (v "0.4.5") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (k 0)) (d (n "tetsy-primitive-types") (r "^0.9.0") (f (quote ("tetsy-impl-rlp"))) (d #t) (k 2)))) (h "0yqs3b8ykqpi0qrlkiyqlm99819lf2i5caca7d1idy76c2q9r8ks") (f (quote (("std" "rustc-hex/std") ("default" "std"))))))

(define-public crate-tetsy-rlp-0.5.0-alpha (c (n "tetsy-rlp") (v "0.5.0-alpha") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (k 0)))) (h "15bzm2p4c9nw72mjx8cnia1ashdkksamprgkab20jym8l8rfgrf7") (f (quote (("std" "bytes/std" "rustc-hex/std") ("default" "std"))))))

(define-public crate-tetsy-rlp-0.5.1 (c (n "tetsy-rlp") (v "0.5.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (k 0)))) (h "0ifhffjg1vcsmvcv1zykl7jpk68dhhdg46qx05gzwaqklxifshzz") (f (quote (("std" "bytes/std" "rustc-hex/std") ("default" "std"))))))

