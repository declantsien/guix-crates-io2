(define-module (crates-io te ts tetsy-kvdb-shared-tests) #:use-module (crates-io))

(define-public crate-tetsy-kvdb-shared-tests-0.7.0 (c (n "tetsy-kvdb-shared-tests") (v "0.7.0") (d (list (d (n "tetsy-kvdb") (r "^0.9") (d #t) (k 0)))) (h "07f7l67f9b6cbbjp2vl68555hglrkz6qld0x6h52i6a1d852h4nz")))

(define-public crate-tetsy-kvdb-shared-tests-0.1.0 (c (n "tetsy-kvdb-shared-tests") (v "0.1.0") (d (list (d (n "tetsy-kvdb") (r "^0.3") (d #t) (k 0)))) (h "0ys8999xn2zqrdd5xjd07p21x41ylp36nmp6wp74sj1bqzkhn9rj")))

(define-public crate-tetsy-kvdb-shared-tests-0.1.1 (c (n "tetsy-kvdb-shared-tests") (v "0.1.1") (d (list (d (n "tetsy-kvdb") (r "^0.3.1") (d #t) (k 0)))) (h "10m619i5ka9lzaqwy6nhdm7p4pqyxpn9dpr583dphandii9aj2z8")))

(define-public crate-tetsy-kvdb-shared-tests-0.2.0 (c (n "tetsy-kvdb-shared-tests") (v "0.2.0") (d (list (d (n "tetsy-kvdb") (r "^0.4") (d #t) (k 0)))) (h "1cb5qbnvz8rmaiy896g63b8v4i35galpl0lkamxs8bjf2gmrax8w")))

(define-public crate-tetsy-kvdb-shared-tests-0.1.2 (c (n "tetsy-kvdb-shared-tests") (v "0.1.2") (d (list (d (n "tetsy-kvdb") (r "^0.3.2") (d #t) (k 0)))) (h "1jv03b1gs3v521nqc1k56bzi58iv36cfwilcsi501w0jp3vp6y57")))

(define-public crate-tetsy-kvdb-shared-tests-0.6.0 (c (n "tetsy-kvdb-shared-tests") (v "0.6.0") (d (list (d (n "tetsy-kvdb") (r "^0.8") (d #t) (k 0)))) (h "1qh0h6hxsj8fz6l0374y1k9fgqix92npibc9hhykbzni24nmrkjm")))

(define-public crate-tetsy-kvdb-shared-tests-0.7.1 (c (n "tetsy-kvdb-shared-tests") (v "0.7.1") (d (list (d (n "tetsy-kvdb") (r "^0.9.1") (d #t) (k 0)))) (h "0vwgzfh8qp8818s67yka51s73d0pc24h3m151qfcg1j7vhcq4ll8")))

