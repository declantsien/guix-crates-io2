(define-module (crates-io te ts tetsy-local-store) #:use-module (crates-io))

(define-public crate-tetsy-local-store-0.1.0 (c (n "tetsy-local-store") (v "0.1.0") (d (list (d (n "common-types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tetsy-crypto") (r "^0.4.2") (f (quote ("publickey"))) (d #t) (k 2)) (d (n "tetsy-kvdb") (r "^0.3.2") (d #t) (k 0)) (d (n "tetsy-kvdb-memorydb") (r "^0.3.2") (d #t) (k 2)) (d (n "tetsy-rlp") (r "^0.4.5") (d #t) (k 0)) (d (n "vapcore-io") (r "^1.12.0") (d #t) (k 0)) (d (n "vapkey") (r "^0.4.0") (d #t) (k 2)))) (h "1p02mjjvawsahx0xkisyglf38qcz63hwvv5pmwyinvkd9l6hwknf")))

