(define-module (crates-io te a- tea-adapter-actor-codec) #:use-module (crates-io))

(define-public crate-tea-adapter-actor-codec-0.2.0-dev.1 (c (n "tea-adapter-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "1q35l71mckifvkp0ql23y2y6bs531b9sw6gksxx14rjjbac52jgv")))

