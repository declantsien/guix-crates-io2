(define-module (crates-io te a- tea-layer1-actor-codec) #:use-module (crates-io))

(define-public crate-tea-layer1-actor-codec-0.2.0-dev.1 (c (n "tea-layer1-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actor-txns") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-solc-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-tapp-common") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-tappstore-actor-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "1f0xqr69frzq05ylwg1b39nvr5hpn3zs6nal1njpqcg5pfs715l9")))

