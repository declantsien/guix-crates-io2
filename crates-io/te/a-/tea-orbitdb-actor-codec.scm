(define-module (crates-io te a- tea-orbitdb-actor-codec) #:use-module (crates-io))

(define-public crate-tea-orbitdb-actor-codec-0.2.0-dev.1 (c (n "tea-orbitdb-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "0k3i1bv7gy6ap5f56xwwv4nfr9l1l6isf9wfjzj0z6y9yg2z1ds7")))

