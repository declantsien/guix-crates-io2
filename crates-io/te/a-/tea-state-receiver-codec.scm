(define-module (crates-io te a- tea-state-receiver-codec) #:use-module (crates-io))

(define-public crate-tea-state-receiver-codec-0.2.0-dev.1 (c (n "tea-state-receiver-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-tapp-common") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "058zr73yzfhhz4vfh5800ga9p2na4jvxzkcv6mhpyliy8rc1ax0m")))

