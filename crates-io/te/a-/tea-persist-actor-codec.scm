(define-module (crates-io te a- tea-persist-actor-codec) #:use-module (crates-io))

(define-public crate-tea-persist-actor-codec-0.2.0-dev.1 (c (n "tea-persist-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "1v6ya68gvdvpy035iw4a1yn5f24ip4caadyqbgkvq202xfk5vhmr")))

