(define-module (crates-io te a- tea-nitro-actor-codec) #:use-module (crates-io))

(define-public crate-tea-nitro-actor-codec-0.2.0-dev.1 (c (n "tea-nitro-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-tapp-common") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "18887xh5dv09fbsxzc2f4w2b4c5qmhybzgbm7cj6wgkh0lawi6h7")))

