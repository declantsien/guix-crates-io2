(define-module (crates-io te a- tea-tokenstate-actor-codec) #:use-module (crates-io))

(define-public crate-tea-tokenstate-actor-codec-0.2.0-dev.1 (c (n "tea-tokenstate-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actor-txns") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-tapp-common") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "1qk39vqc8bvyr7i8pnp6rgccql71srl34v1vyqq9f5xfgxxd2x73")))

