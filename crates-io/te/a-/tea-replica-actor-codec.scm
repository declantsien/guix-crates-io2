(define-module (crates-io te a- tea-replica-actor-codec) #:use-module (crates-io))

(define-public crate-tea-replica-actor-codec-0.2.0-dev.1 (c (n "tea-replica-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actor-txns") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-tapp-common") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "0az41wrp9ch2gcpx29aqgg290cdzlz51q27lj4ijpmpn3dikkrdi")))

