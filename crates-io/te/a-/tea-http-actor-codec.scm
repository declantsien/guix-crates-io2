(define-module (crates-io te a- tea-http-actor-codec) #:use-module (crates-io))

(define-public crate-tea-http-actor-codec-0.2.0-dev.1 (c (n "tea-http-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "125dnn3m5rqcppkvxwkharzp4xd1b9rjlsr88rgi4j9iqzlgyrhm")))

