(define-module (crates-io te a- tea-codec-macros) #:use-module (crates-io))

(define-public crate-tea-codec-macros-0.2.0-dev.1 (c (n "tea-codec-macros") (v "0.2.0-dev.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nslsnkj745y7c0kisl7gzsksaxwbqzy72rn1rjqx4v7x110wsqh")))

(define-public crate-tea-codec-macros-0.2.0-dev.2 (c (n "tea-codec-macros") (v "0.2.0-dev.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yakamh3k5w2581drk5v5i0hxr63j948jkav91r5c2dsvg522dch")))

(define-public crate-tea-codec-macros-0.2.0-dev.3 (c (n "tea-codec-macros") (v "0.2.0-dev.3") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00jrvjsv41ws1zmi0fxhazppqzpmnwh8vjz2lh63pi6bjqr554z4")))

(define-public crate-tea-codec-macros-0.2.0-dev.4 (c (n "tea-codec-macros") (v "0.2.0-dev.4") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qhk47ilr2c535dr86zb8b03ka6q8q48m7wiqvw6wb10xfxai0sq")))

(define-public crate-tea-codec-macros-0.2.0-dev.5 (c (n "tea-codec-macros") (v "0.2.0-dev.5") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r5dadhhnwvg7ar3vjq36890mv20zklykqa0xyy8yayac2706429")))

(define-public crate-tea-codec-macros-0.2.0-dev.6 (c (n "tea-codec-macros") (v "0.2.0-dev.6") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k1dqgqc0a1wwmk1wkhidklj30gszacc3371j26p96wiz9d0frr9")))

(define-public crate-tea-codec-macros-0.2.0-dev.7 (c (n "tea-codec-macros") (v "0.2.0-dev.7") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h6nslbf67ck07rv9907nzk5qb9p1365vvngnx46p5gvvwhqvz6f")))

(define-public crate-tea-codec-macros-0.2.0-dev.8 (c (n "tea-codec-macros") (v "0.2.0-dev.8") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g2cwida7pm5brlmvbpbh91rpwh6vvspgyp9f6y2jasbwy12zhgp")))

(define-public crate-tea-codec-macros-0.2.0-dev.9 (c (n "tea-codec-macros") (v "0.2.0-dev.9") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rjbqkdp1vqb92cii44294px76gwpv7djgr0cwbrjp0005lcvcig")))

(define-public crate-tea-codec-macros-0.2.0-dev.10 (c (n "tea-codec-macros") (v "0.2.0-dev.10") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18zkdif53i5443fsxhm72hr1i0wyw2ggz8yyva8crdhnpskw2dff")))

(define-public crate-tea-codec-macros-0.2.0-dev.11 (c (n "tea-codec-macros") (v "0.2.0-dev.11") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n8zwgxpqri9dlikrwllzx1iksa3c84rsv4a3n1b1z18y7k4qx9d")))

(define-public crate-tea-codec-macros-0.2.0-dev.12 (c (n "tea-codec-macros") (v "0.2.0-dev.12") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h3007q07az4shp52c7vgqgxg1xsiy6lwbq9fhqj0gy21dh2zvwj")))

(define-public crate-tea-codec-macros-0.2.0-dev.13 (c (n "tea-codec-macros") (v "0.2.0-dev.13") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bzpqa10cs9w2yy4g5yxa1b66ahx7xap4012mm0x8d2syr1cxxd6")))

(define-public crate-tea-codec-macros-0.2.0-dev.14 (c (n "tea-codec-macros") (v "0.2.0-dev.14") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y4s6vq679fab1gwr92i1ml3lqldyd3rxn9cv99rj17vgsnknmw4")))

(define-public crate-tea-codec-macros-0.2.0-dev.15 (c (n "tea-codec-macros") (v "0.2.0-dev.15") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05a3v5kaw0fn8gm9b4705z7fvfgk11lgf3g7wsmzbcdswycw9h3c")))

(define-public crate-tea-codec-macros-0.2.0-dev.16 (c (n "tea-codec-macros") (v "0.2.0-dev.16") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10klclv89mx23ibx33mx0bkc2zhixyaw40ny22jccfmbn00kn1sx")))

(define-public crate-tea-codec-macros-0.2.0-dev.17 (c (n "tea-codec-macros") (v "0.2.0-dev.17") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bw04xxpmdk4w92bzzckl0fcxcz78zlhck1l33v9v4r6x3zzapc2")))

(define-public crate-tea-codec-macros-0.2.0-dev.18 (c (n "tea-codec-macros") (v "0.2.0-dev.18") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0b7p03cfh6qq2p9087fkj4wd2p6vwlmppsvgdd7fgx7qg2icj63x")))

(define-public crate-tea-codec-macros-0.2.0-dev.19 (c (n "tea-codec-macros") (v "0.2.0-dev.19") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11bhpm8xhwwr2cynjqkd099ilcp6d2583msg7ksi1mj6hrl5jgwg")))

(define-public crate-tea-codec-macros-0.2.0-dev.20 (c (n "tea-codec-macros") (v "0.2.0-dev.20") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1panbz1chv8qpqzhv0fgyrwfzjfvzl632whfjxynwrk694c8crmf")))

(define-public crate-tea-codec-macros-0.2.0-dev.21 (c (n "tea-codec-macros") (v "0.2.0-dev.21") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v4236rn8gpyxpilwq9d7x3zzx8094y75x9bk44xzl005vayg3nr")))

(define-public crate-tea-codec-macros-0.2.0-dev.22 (c (n "tea-codec-macros") (v "0.2.0-dev.22") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s6l6ybxfin2js9vqw1zk8x8w58jdj4bcp1pm00d46xh0xgyr9ks")))

(define-public crate-tea-codec-macros-0.3.0-dev.1 (c (n "tea-codec-macros") (v "0.3.0-dev.1") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05x06hsfhabdy3fakz78zczksbd7xg1nc0ia87myf185svckj3k0")))

(define-public crate-tea-codec-macros-0.3.0-dev.2 (c (n "tea-codec-macros") (v "0.3.0-dev.2") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08774pdp45l58afx79122p9fw4jk6xwcbdszz8vp3rmd7baqxx3b")))

(define-public crate-tea-codec-macros-0.3.0-dev.3 (c (n "tea-codec-macros") (v "0.3.0-dev.3") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03pi9yqnk3kl7hnjlkzdinv1w4airy0dzpcag2x5a275dyql8723")))

(define-public crate-tea-codec-macros-0.3.0-dev.4 (c (n "tea-codec-macros") (v "0.3.0-dev.4") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j21hw4d3xvgkm23vsacr7yvnyg1nmv5g1c7sc7qwg3b9p196cbn")))

(define-public crate-tea-codec-macros-0.3.0-dev.5 (c (n "tea-codec-macros") (v "0.3.0-dev.5") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x0sd8f0cbys23j8bhhl5n25m3v6dyaqngi687ylsjlw7sf2951q")))

(define-public crate-tea-codec-macros-0.3.0-dev.6 (c (n "tea-codec-macros") (v "0.3.0-dev.6") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x58sal8yi96lxb6an245bmmjyhg82506ajr1rpap41w2vnqhiwa")))

(define-public crate-tea-codec-macros-0.3.0-dev.7 (c (n "tea-codec-macros") (v "0.3.0-dev.7") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h9by99k1hx5h7h7igi7zxvmqkh4nklx1mvj1k1rab2r2qjp1lzc")))

