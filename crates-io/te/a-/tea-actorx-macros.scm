(define-module (crates-io te a- tea-actorx-macros) #:use-module (crates-io))

(define-public crate-tea-actorx-macros-0.2.0-dev.3 (c (n "tea-actorx-macros") (v "0.2.0-dev.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xqkxbis6x6l2j60ynsn2yc6vqg15i6bkm0w78x8krc11g4bfyd9")))

(define-public crate-tea-actorx-macros-0.2.0-dev.4 (c (n "tea-actorx-macros") (v "0.2.0-dev.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "031mvr635597s9agb9snqc3d2fm3q60z5qch9wr2inanmfzvkb3w")))

(define-public crate-tea-actorx-macros-0.2.0-dev.5 (c (n "tea-actorx-macros") (v "0.2.0-dev.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cy3bzmzwhn2far5kihp6xs413k1c949zbbn3h12326z60g3mldf")))

(define-public crate-tea-actorx-macros-0.2.0-dev.6 (c (n "tea-actorx-macros") (v "0.2.0-dev.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15xk8x08qshkc13zbzksfbl5kgi1y1bf44p8siw1srqv8bg76ha5")))

(define-public crate-tea-actorx-macros-0.2.0-dev.7 (c (n "tea-actorx-macros") (v "0.2.0-dev.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gvy4xr7k35r1gp0sfspj9mflxx2m18h1cn8jjcn7dzicz8jqlfi")))

(define-public crate-tea-actorx-macros-0.2.0-dev.8 (c (n "tea-actorx-macros") (v "0.2.0-dev.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rs41y0cnk71f4lylsxm8vx8rczqfa7pxs2xksclq8lkbyr5bb8l")))

(define-public crate-tea-actorx-macros-0.2.0-dev.9 (c (n "tea-actorx-macros") (v "0.2.0-dev.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rwsmahpx8gwl9j5svvgika5vnjhdkp7qm1sj66dzzbkhgjq6ra1")))

(define-public crate-tea-actorx-macros-0.2.0-dev.10 (c (n "tea-actorx-macros") (v "0.2.0-dev.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1657fnqdvvrs0fgfpv4xzdn435hhpwsacvs8wbjaxkrr8bcqpnl4")))

(define-public crate-tea-actorx-macros-0.2.0-dev.11 (c (n "tea-actorx-macros") (v "0.2.0-dev.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rrbnqi3c0r5nfzlhp5rbsr8rci78cnc3payhij8lrbijv8lfv1v")))

(define-public crate-tea-actorx-macros-0.2.0-dev.12 (c (n "tea-actorx-macros") (v "0.2.0-dev.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xhn31rnhdqdd08p2rg5n424zc10znp3pyg7zqn896wgx26nr3cx")))

(define-public crate-tea-actorx-macros-0.2.0-dev.13 (c (n "tea-actorx-macros") (v "0.2.0-dev.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h5nm0dk36f6sz5qfkrs5vj7321dhdwd726xalxgmzvd87qx13jb")))

(define-public crate-tea-actorx-macros-0.2.0-dev.14 (c (n "tea-actorx-macros") (v "0.2.0-dev.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yc1i4lkyvgc8c7png37qbn57185wihl0rk9nw000616biramcf6")))

(define-public crate-tea-actorx-macros-0.2.0-dev.15 (c (n "tea-actorx-macros") (v "0.2.0-dev.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h0j4skw37sdj0s3jk7mzddv0qgakcw4y9d7gn8by2bkz8xcdknd")))

(define-public crate-tea-actorx-macros-0.2.0-dev.16 (c (n "tea-actorx-macros") (v "0.2.0-dev.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13w6vlw69iln9bmllhgjvnp70jscil7lr2c4ikiyy7gz80ac9m91")))

(define-public crate-tea-actorx-macros-0.2.0-dev.17 (c (n "tea-actorx-macros") (v "0.2.0-dev.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0785z0w19xg6ba8jl431xksm9raah5q1d7i4l7cza9zaa6j2brib")))

(define-public crate-tea-actorx-macros-0.2.0-dev.18 (c (n "tea-actorx-macros") (v "0.2.0-dev.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d5n09xd78p3gklcg1am4n4z1h75q49xk1b82grzdj8f26779nv7")))

(define-public crate-tea-actorx-macros-0.2.0-dev.19 (c (n "tea-actorx-macros") (v "0.2.0-dev.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07yhmhb7aaryzbk15sczddvp1kw4n9k3wf22drpsqcli28p91fcw")))

(define-public crate-tea-actorx-macros-0.2.0-dev.20 (c (n "tea-actorx-macros") (v "0.2.0-dev.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d132cjzbnyghmw3sd7lknvjs0kcxgskzic7329ld9d9p1jzns44")))

(define-public crate-tea-actorx-macros-0.2.0-dev.21 (c (n "tea-actorx-macros") (v "0.2.0-dev.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h3y44p98bwmd4g6knc1ifrzzrndy97zhi1c9qsj8j1309bqh7h8")))

(define-public crate-tea-actorx-macros-0.2.0-dev.22 (c (n "tea-actorx-macros") (v "0.2.0-dev.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "110zrzadh0viyc8lfc4bk80qvvarffw7m13jsgcihrcg6jk4y8ns")))

