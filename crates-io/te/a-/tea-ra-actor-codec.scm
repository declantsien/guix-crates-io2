(define-module (crates-io te a- tea-ra-actor-codec) #:use-module (crates-io))

(define-public crate-tea-ra-actor-codec-0.2.0-dev.1 (c (n "tea-ra-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-nitro-actor-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "1jza2x2y21f0zxnryn2lmimw84s81fb6vrzbl33j6vwgsfa9dkp5")))

