(define-module (crates-io te a- tea-crypto-actor-codec) #:use-module (crates-io))

(define-public crate-tea-crypto-actor-codec-0.2.0-dev.1 (c (n "tea-crypto-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-tapp-common") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "1p0gdflyvf07ib1zwsnrrpz77lpjgx6cq8xr88ywayk6nnaj7sxz")))

