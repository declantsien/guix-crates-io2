(define-module (crates-io te a- tea-actor-utility) #:use-module (crates-io))

(define-public crate-tea-actor-utility-0.1.0 (c (n "tea-actor-utility") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "tea-codec") (r "^0.1.0") (d #t) (k 0)) (d (n "wascc-actor") (r "^0.7.1") (d #t) (k 0)))) (h "1aqhw9vxaay58abxvd0i4xcnx166pvyz8cjprh1ak9m89vl36z2h")))

