(define-module (crates-io te a- tea-tokenstate-service-actor-codec) #:use-module (crates-io))

(define-public crate-tea-tokenstate-service-actor-codec-0.2.0-dev.1 (c (n "tea-tokenstate-service-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-tapp-common") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "1qvhqkibv6d23s2schlk0q98gnp377zddwbsr5q2glyf8m5jf6a0")))

