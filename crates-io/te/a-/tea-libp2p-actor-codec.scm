(define-module (crates-io te a- tea-libp2p-actor-codec) #:use-module (crates-io))

(define-public crate-tea-libp2p-actor-codec-0.2.0-dev.1 (c (n "tea-libp2p-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-vmh-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "1iw1m3cb5zrhssqgnsk6238y5zfys023g92jb8h2zayph3qa6h84") (f (quote (("simulator" "tea-vmh-codec/simulator") ("nitro" "tea-vmh-codec/nitro") ("default") ("__test" "tea-vmh-codec/__test"))))))

