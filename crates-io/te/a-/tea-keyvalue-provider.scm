(define-module (crates-io te a- tea-keyvalue-provider) #:use-module (crates-io))

(define-public crate-tea-keyvalue-provider-0.1.0 (c (n "tea-keyvalue-provider") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "key-vec") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tea-codec") (r "^0.1.0") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.6.0") (d #t) (k 0)))) (h "1p9z5phv8xlshzqq1l7nc4s7zv31fyz54wrf7g69czjq56dc2lb8") (f (quote (("static_plugin"))))))

(define-public crate-tea-keyvalue-provider-0.1.1 (c (n "tea-keyvalue-provider") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "key-vec") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tea-codec") (r "^0.1.0") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.6.0") (d #t) (k 0)))) (h "1jr4sq21vfpnmzi2hv7jkr2gykbfbdwk4vxc5qllvmvb30qyzw8x") (f (quote (("static_plugin"))))))

