(define-module (crates-io te a- tea-env-actor-codec) #:use-module (crates-io))

(define-public crate-tea-env-actor-codec-0.2.0-dev.1 (c (n "tea-env-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-solc-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-vmh-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "0512598ki07qjh9s2n6bdiv3p8p2figdvnrpa8m61110bb56yvfq") (f (quote (("simulator") ("nitro") ("default"))))))

