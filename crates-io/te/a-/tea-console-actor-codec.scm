(define-module (crates-io te a- tea-console-actor-codec) #:use-module (crates-io))

(define-public crate-tea-console-actor-codec-0.2.0-dev.1 (c (n "tea-console-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "185szpvzphad7k32blwaafzxdyv2cj4xda8gr57niciljr8lvjx3")))

