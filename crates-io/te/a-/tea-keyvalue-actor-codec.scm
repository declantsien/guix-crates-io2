(define-module (crates-io te a- tea-keyvalue-actor-codec) #:use-module (crates-io))

(define-public crate-tea-keyvalue-actor-codec-0.2.0-dev.1 (c (n "tea-keyvalue-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "0b0ikhm531c5r5lil7q6yg82xvn1g035pxrkbci6dj80dv952bxj")))

