(define-module (crates-io te a- tea-tappstore-actor-codec) #:use-module (crates-io))

(define-public crate-tea-tappstore-actor-codec-0.2.0-dev.1 (c (n "tea-tappstore-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "tea-actor-txns") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-solc-codec") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-tapp-common") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "15gwgkzc0haajjc49gkcbf4wix7khpnazl9ya552qaj49df1zkm9")))

