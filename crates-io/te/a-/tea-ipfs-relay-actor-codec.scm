(define-module (crates-io te a- tea-ipfs-relay-actor-codec) #:use-module (crates-io))

(define-public crate-tea-ipfs-relay-actor-codec-0.2.0-dev.1 (c (n "tea-ipfs-relay-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "0a7jxcvw818rm7v0a2rfii7z1prxzk0rw7hzn1f05387mvz673h7")))

