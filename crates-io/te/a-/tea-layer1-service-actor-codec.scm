(define-module (crates-io te a- tea-layer1-service-actor-codec) #:use-module (crates-io))

(define-public crate-tea-layer1-service-actor-codec-0.2.0-dev.1 (c (n "tea-layer1-service-actor-codec") (v "0.2.0-dev.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tea-actorx-core") (r "^0.2.0-dev.1") (d #t) (k 0)) (d (n "tea-codec") (r "^0.2.0-dev.1") (d #t) (k 0)))) (h "12v29ra26q4raynl4nvf1kd4f9a90vw5fanp13zv4kyaafr9s92l")))

