(define-module (crates-io te su tesuto_project) #:use-module (crates-io))

(define-public crate-tesuto_project-0.1.0 (c (n "tesuto_project") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "1fn0nz64851dxr09ldzra9b74j7la0863awbhb1p26ja0cjiizh2")))

(define-public crate-tesuto_project-0.1.1 (c (n "tesuto_project") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "13f9r0v3myhq1gnjfvxsp4h6w5g1jv6ibr66sd1l2yrq7x5s15ya")))

