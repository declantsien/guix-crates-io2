(define-module (crates-io te lt teltonika-rs) #:use-module (crates-io))

(define-public crate-teltonika-rs-0.1.0 (c (n "teltonika-rs") (v "0.1.0") (d (list (d (n "crc") (r "^2.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "0y7q186vlb9snrw8xs29aad33xkzspfk2qdz831pqfgn06zqf2v3")))

(define-public crate-teltonika-rs-0.1.1 (c (n "teltonika-rs") (v "0.1.1") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0yli4cv559c8k0hwqag57gaqx739clnxf6hw5qkm3w9di83j44dk")))

