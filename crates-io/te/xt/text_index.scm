(define-module (crates-io te xt text_index) #:use-module (crates-io))

(define-public crate-text_index-0.1.0 (c (n "text_index") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "clap") (r "^2.32") (k 0)) (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09nbfh937528ihgdzfr65x0qw4cyps1ri8xaf7bsh1djh6d6hdll")))

(define-public crate-text_index-0.1.1 (c (n "text_index") (v "0.1.1") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "clap") (r "^2.32") (k 0)) (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1013j2mrims5mpcdf0dbrkjdny9mi5kw9bj9ymc330q97v1gni2i")))

