(define-module (crates-io te xt textwidth) #:use-module (crates-io))

(define-public crate-textwidth-1.0.0 (c (n "textwidth") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "x11") (r "^2.18.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1amw1ik5yk58v78rf5xrydbqjicy4n7x1g3r1wil1n1n9vqy25jy")))

(define-public crate-textwidth-1.1.0 (c (n "textwidth") (v "1.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "x11") (r "^2.18.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "109l3fr3l6xvymqs5ip7akrn986frgkspwkks5k8icc445nn905p")))

(define-public crate-textwidth-2.0.0 (c (n "textwidth") (v "2.0.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1dfjhs7sa10723z5h1493v0fkjymwc23xvr40zq0qqk6vfrrzi54") (y #t)))

(define-public crate-textwidth-2.1.0 (c (n "textwidth") (v "2.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1n0x1h21dyb72bjm8da15zqf2lgn5m74zy323ziz6y5y4a3hxmlv")))

