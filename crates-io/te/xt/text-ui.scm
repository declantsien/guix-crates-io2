(define-module (crates-io te xt text-ui) #:use-module (crates-io))

(define-public crate-text-ui-0.1.0-beta (c (n "text-ui") (v "0.1.0-beta") (h "1gnq10n9a1kycvfr2zv6kprkimzpabi13zj5qz6k4whil4k4sr5k") (y #t)))

(define-public crate-text-ui-0.1.1-beta (c (n "text-ui") (v "0.1.1-beta") (h "0xgfvijd9fhqmi682qdvyfgpw2wlcg740kc76pbccndqm09xmxq3") (y #t)))

(define-public crate-text-ui-0.1.2-beta (c (n "text-ui") (v "0.1.2-beta") (h "1j5j3ykrr61b5k8zwch0hkpjrg2ll0v9gkl0dk14l00zi4zlxsxm")))

