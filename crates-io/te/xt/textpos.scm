(define-module (crates-io te xt textpos) #:use-module (crates-io))

(define-public crate-textpos-0.1.0 (c (n "textpos") (v "0.1.0") (h "0abpchhxyfpdl1v3rsj8hjrmnn7f55p3fjz6yh2w4kgcv7bbf57f")))

(define-public crate-textpos-0.2.0 (c (n "textpos") (v "0.2.0") (h "0dsg4yfxl878kc5b3fw2h470vmg95j8s8qjv0qagrjz6dn5lbfnx")))

(define-public crate-textpos-0.2.1 (c (n "textpos") (v "0.2.1") (h "1g5y495my1fmqxjplyhm38ixdlndymyfc5l41mppvr1rsqlgj4n1")))

(define-public crate-textpos-0.3.0 (c (n "textpos") (v "0.3.0") (h "1iv5av2i3ql6r280ja8hbzkzgaws0b3wv9zb9vvyx709xhxmkaa5")))

(define-public crate-textpos-0.3.1 (c (n "textpos") (v "0.3.1") (h "0znpsxbd1gpnpxmnsg48m6cm4n4vpjb5ycnrrxw77r0v6dls51mm")))

(define-public crate-textpos-0.3.2 (c (n "textpos") (v "0.3.2") (h "1vqnrifflkv40dnlxlf3zs5hrxz7pfzpdvyygx4p23n1kmbqmyk1")))

(define-public crate-textpos-0.3.3 (c (n "textpos") (v "0.3.3") (h "1z3597pkchbv8mly3sniccs5jl750fd96rqx94ikvwc5p6bz4n14")))

