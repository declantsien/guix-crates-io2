(define-module (crates-io te xt textsweeper) #:use-module (crates-io))

(define-public crate-textsweeper-0.1.0 (c (n "textsweeper") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hc0mr9zqrpzvr7sq9vhg540inph7mfblpsn5kndzbhqp5y1bp4f") (y #t)))

(define-public crate-textsweeper-0.1.1 (c (n "textsweeper") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m9fq9755ijjnszfv2cgiwn92547gdsn54zs84f5y10s4kmyyqif")))

(define-public crate-textsweeper-0.1.2 (c (n "textsweeper") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rb08ng2d8nhwv52dyfh1fap8iaba933zlp4iy13smbzrd36gygl")))

