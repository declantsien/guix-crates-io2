(define-module (crates-io te xt textstream) #:use-module (crates-io))

(define-public crate-textstream-0.1.0 (c (n "textstream") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "memchr") (r "^1.0.1") (d #t) (k 0)))) (h "0wafg8bgxaad2cmim70r2jji5ja8r04dyc0vghw0fn71m7p3p7i1") (y #t)))

(define-public crate-textstream-0.1.1 (c (n "textstream") (v "0.1.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "memchr") (r "^1.0.1") (d #t) (k 0)))) (h "1apxiz2w4wnf2b92gr12jwaiq2w1k1vlnzsyflk60mpn8arq3vg7")))

