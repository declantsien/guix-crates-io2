(define-module (crates-io te xt text-measurer-freetype) #:use-module (crates-io))

(define-public crate-text-measurer-freetype-0.1.0 (c (n "text-measurer-freetype") (v "0.1.0") (d (list (d (n "freetype-rs") (r "^0.26.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.13.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (k 0)))) (h "0sif2cdd9pjv1n3sfd44hd6b9zdly26chh47lwq9b712rwpnavb7")))

