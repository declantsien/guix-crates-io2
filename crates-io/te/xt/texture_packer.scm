(define-module (crates-io te xt texture_packer) #:use-module (crates-io))

(define-public crate-texture_packer-0.0.0 (c (n "texture_packer") (v "0.0.0") (d (list (d (n "image") (r "~0.2.0") (d #t) (k 0)))) (h "1m3vh24j8s3yfkjrifra5nqjw744n1bimxavzwkd1hv0r8zkia9p")))

(define-public crate-texture_packer-0.0.1 (c (n "texture_packer") (v "0.0.1") (d (list (d (n "image") (r "^0.3.8") (d #t) (k 0)))) (h "1k33hj6ywg6wyg0n0vc1a2738mikbiglm4500ykclsrjhpssc1pi")))

(define-public crate-texture_packer-0.1.0 (c (n "texture_packer") (v "0.1.0") (d (list (d (n "image") (r "^0.3.12") (d #t) (k 0)))) (h "1367fkdzdvbr6nn02hzrr78q30pyaavmg5kfcbgb6bg7qnsh9p95")))

(define-public crate-texture_packer-0.2.0 (c (n "texture_packer") (v "0.2.0") (d (list (d (n "image") (r "^0.4.0") (d #t) (k 0)))) (h "0gx7c3frrkz6i0jg90j4csk0plsd24x9h3r5w3p2kawg9k39fw0l")))

(define-public crate-texture_packer-0.3.0 (c (n "texture_packer") (v "0.3.0") (d (list (d (n "image") (r "^0.5.0") (d #t) (k 0)))) (h "1v3i2vnhb6kcdq5p9fq9p8mkmnn5jwvb5ki7f145sqsxbwgd8729")))

(define-public crate-texture_packer-0.5.0 (c (n "texture_packer") (v "0.5.0") (d (list (d (n "image") (r "^0.7.0") (d #t) (k 0)))) (h "10dpq168yf68r62f9g0m6qcqykcsfgclfs30wynixkrck3w7kzh9")))

(define-public crate-texture_packer-0.6.0 (c (n "texture_packer") (v "0.6.0") (d (list (d (n "image") (r "^0.8.0") (d #t) (k 0)))) (h "0k3djgibx5lgxj7bkg25cl4vln5y2hvghmcr9fh6q65ckzrz7d6m")))

(define-public crate-texture_packer-0.7.0 (c (n "texture_packer") (v "0.7.0") (d (list (d (n "image") (r "^0.9.0") (d #t) (k 0)))) (h "14wpa62ki0abbw35xkilx0h5yfz77wwgsr66c1gw0c32ypqqzvcn")))

(define-public crate-texture_packer-0.8.0 (c (n "texture_packer") (v "0.8.0") (d (list (d (n "image") (r "^0.10.0") (d #t) (k 0)))) (h "17zcb2nr02lng6rh3nrla7plfxhirrd52n6p520h5m8gry6rmvd7")))

(define-public crate-texture_packer-0.9.0 (c (n "texture_packer") (v "0.9.0") (d (list (d (n "image") (r "^0.10.0") (k 0)))) (h "1xxjimy70hipm5z0kxsx41rp15qhlmas6iqrc9d7f9xnmcz96h19") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("ppm" "image/ppm") ("png_codec" "image/png_codec") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif_codec" "image/gif_codec") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.10.0 (c (n "texture_packer") (v "0.10.0") (d (list (d (n "image") (r "^0.15.0") (k 0)))) (h "0yqq3icmg55aq8jcxp1p4vfvzir9h90x8kmcpcqlhzg5fnacwhih") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("ppm" "image/ppm") ("png_codec" "image/png_codec") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif_codec" "image/gif_codec") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.11.0 (c (n "texture_packer") (v "0.11.0") (d (list (d (n "image") (r "^0.16.0") (k 0)))) (h "18ph522rw9f84xb5611zp8c2isknmk0x8kkz07wvpyn9rd33mmyf") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("ppm" "image/ppm") ("png_codec" "image/png_codec") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif_codec" "image/gif_codec") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.12.0 (c (n "texture_packer") (v "0.12.0") (d (list (d (n "image") (r "^0.17.0") (k 0)))) (h "1lnzlma97609455bl3rb5nfjwcjzzqbxm4qhllvpsrq3d0dbqxq1") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("ppm" "image/ppm") ("png_codec" "image/png_codec") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif_codec" "image/gif_codec") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.13.0 (c (n "texture_packer") (v "0.13.0") (d (list (d (n "image") (r "^0.18.0") (k 0)))) (h "1xqidvjgf4k3nmxj7kmqva7ighynhhlnr6ylwjqvr5dkybv9y66s") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("ppm" "image/ppm") ("png_codec" "image/png_codec") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif_codec" "image/gif_codec") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.14.0 (c (n "texture_packer") (v "0.14.0") (d (list (d (n "image") (r "^0.19.0") (k 0)))) (h "1g8ipxq2g9m6fap545zj3qy78ds1wbqwzwb48059f5n2w0yvaq0g") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png_codec" "image/png_codec") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif_codec" "image/gif_codec") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.15.0 (c (n "texture_packer") (v "0.15.0") (d (list (d (n "image") (r "^0.20.0") (k 0)))) (h "0zlz5hf29jxd4dg9yv2w5br8kgqia912gv93agr1wxsn5hsr2yyz") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png_codec" "image/png_codec") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif_codec" "image/gif_codec") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.16.0 (c (n "texture_packer") (v "0.16.0") (d (list (d (n "image") (r "^0.21.0") (k 0)))) (h "0wfqhj27x5lbckwdkysln68496avgafr1llapk35310rfq25fk9f") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png_codec" "image/png_codec") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif_codec" "image/gif_codec") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.17.0 (c (n "texture_packer") (v "0.17.0") (d (list (d (n "image") (r "^0.22.1") (k 0)))) (h "1zwhiif8g2pz3xqszfnz975byl7fmwackkl4860mr6jw05mgwl56") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png_codec" "image/png_codec") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif_codec" "image/gif_codec") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.18.0 (c (n "texture_packer") (v "0.18.0") (d (list (d (n "image") (r "^0.22") (k 0)))) (h "148caadl5j4d8i70qjxx14sflwkdr6yvp9184wsjpp9vrizrzl8b") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png_codec" "image/png_codec") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif_codec" "image/gif_codec") ("common" "gif_codec" "jpeg" "png_codec" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.19.0 (c (n "texture_packer") (v "0.19.0") (d (list (d (n "image") (r "^0.23") (k 0)))) (h "083prlk1hwkml4cvwczbhwplqyzd9c8s4bx3mm1jzcdgvv8x12qq") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.20.0 (c (n "texture_packer") (v "0.20.0") (d (list (d (n "image") (r "^0.23") (k 0)))) (h "1y160wlg86w3k1s77w052kdd14nsrcgfpxrlrh7cgzdz9iwlmwvl") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.21.0 (c (n "texture_packer") (v "0.21.0") (d (list (d (n "image") (r "^0.23") (k 0)))) (h "0ixhv23fyq30a6iiiwhfyclivqyc1mp826rr7wz18f6dlsdxmj95") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.21.1 (c (n "texture_packer") (v "0.21.1") (d (list (d (n "image") (r "^0.23") (k 0)))) (h "01f2kni149gg1y3zyz5i1a8b98y7n3hiba1dgw5g7y91h3ys8cyb") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.22.0 (c (n "texture_packer") (v "0.22.0") (d (list (d (n "image") (r "^0.23") (k 0)))) (h "197bvx7ixfby1ylngq8d34yrc31jc0akqyk5ln284cvfzhzric26") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.23.0 (c (n "texture_packer") (v "0.23.0") (d (list (d (n "image") (r "^0.23") (k 0)))) (h "1km127mqwpbpjzhhp2k2pkjnsbq2xhlq9bm4as51fhmvdcrlbcm6") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.23.1 (c (n "texture_packer") (v "0.23.1") (d (list (d (n "image") (r "^0.23") (k 0)))) (h "0a12mbzpp9m25iv3wrs3rkhska2xijzmp0fggs354i2pidn6c7vw") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.24.0 (c (n "texture_packer") (v "0.24.0") (d (list (d (n "image") (r "^0.24.1") (k 0)))) (h "070mw7szmdqy4zzdwivxj2sdbwznrqi59sahdh5f9jyv1759l33c") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.24.1 (c (n "texture_packer") (v "0.24.1") (d (list (d (n "image") (r "^0.24.1") (k 0)))) (h "0z56da0dd490n0ri5qzb1l6f34vv5wzly8gwjb0vbqv59kfmr2vj") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.25.0 (c (n "texture_packer") (v "0.25.0") (d (list (d (n "image") (r "^0.24.1") (k 0)))) (h "16zs2yx34zsbpj61k47ddmbg9078jv8a887l5fh1pzw4n78a1br8") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.27.0 (c (n "texture_packer") (v "0.27.0") (d (list (d (n "image") (r "^0.24.1") (k 0)))) (h "1ffwva3bn7l1afk0k7wyhnmgiyw2ffa5a8n8fvyl6rk8rqyav0n4") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.28.0 (c (n "texture_packer") (v "0.28.0") (d (list (d (n "image") (r "^0.24.1") (k 0)))) (h "1zbl18rqavdabqw4hwbcm4hx73frs0x427cm1qqwjcf4ckbisqr4") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

(define-public crate-texture_packer-0.29.0 (c (n "texture_packer") (v "0.29.0") (d (list (d (n "image") (r "^0.25.1") (k 0)))) (h "0n8dwyvyc7pi4h5hdp0gp8j2d1xf8qjgdn98r903lpa6n9k8glbz") (f (quote (("webp" "image/webp") ("tiff" "image/tiff") ("tga" "image/tga") ("pnm" "image/pnm") ("png" "image/png") ("jpeg" "image/jpeg") ("ico" "image/ico") ("hdr" "image/hdr") ("gif" "image/gif") ("common" "gif" "jpeg" "png" "tga" "bmp") ("bmp" "image/bmp"))))))

