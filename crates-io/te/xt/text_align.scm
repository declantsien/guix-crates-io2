(define-module (crates-io te xt text_align) #:use-module (crates-io))

(define-public crate-text_align-0.1.0 (c (n "text_align") (v "0.1.0") (d (list (d (n "justify") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "18kvzsfm4ivzvpbpdbnh6gvrgmyf109m35wkvwjxgbx1k6ahfxjk")))

(define-public crate-text_align-0.1.1 (c (n "text_align") (v "0.1.1") (d (list (d (n "justify") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "00qvl5zyxkq1izs4zvspjl1cbf56dxnckvsrafjy76z6gj84pm6l")))

(define-public crate-text_align-0.1.2 (c (n "text_align") (v "0.1.2") (d (list (d (n "justify") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1sdf37png96hlmbddczy84dn0066lkqrhm2d8zrbs12pda3sq6pk")))

(define-public crate-text_align-0.2.0 (c (n "text_align") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0djnmz93kmrvf02savhbzgg1ssb14k2qg12xgb302vlial4dm9w1")))

(define-public crate-text_align-0.3.0 (c (n "text_align") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0wfh8vlrmvhgz0xb8vcdpvxagbsv9l7smq31p07sdw4hgw72qv57")))

(define-public crate-text_align-0.3.1 (c (n "text_align") (v "0.3.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0dahwq9ik5jzsclnzb5v0f0px9llsaqs3siq0fi21z48rrvqp4h5")))

