(define-module (crates-io te xt text-mel) #:use-module (crates-io))

(define-public crate-text-mel-0.7.0-rc1 (c (n "text-mel") (v "0.7.0-rc1") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.7.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0-rc1") (d #t) (k 0)))) (h "0x6nkjzf6yaifb45qy860g81g07nqv5cl9nm30zm00c6zwz5g102") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-text-mel-0.7.0 (c (n "text-mel") (v "0.7.0") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "=0.7.0") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0") (d #t) (k 0)))) (h "1d2nm7rzwlfxh2f0kxwnq6v1hir9cbnfjclrmpsv4iywq4wzd122") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-text-mel-0.7.1 (c (n "text-mel") (v "0.7.1") (d (list (d (n "futures") (r "~0.3.28") (d #t) (k 0)) (d (n "melodium-core") (r "^0.7.1") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.7.1") (d #t) (k 0)))) (h "0qf2dm0n0hykgfnb2d31xgdq9i8fddr8k3l637bzn8x834hhx4ks") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

