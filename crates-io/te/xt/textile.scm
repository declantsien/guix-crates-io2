(define-module (crates-io te xt textile) #:use-module (crates-io))

(define-public crate-textile-0.1.0 (c (n "textile") (v "0.1.0") (d (list (d (n "pipeline") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)))) (h "191lfsarznfipsh9pmsvdb3qcqybllpjhkkiij7pa6kbw5fa35xk")))

(define-public crate-textile-0.2.0 (c (n "textile") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "maplit") (r "^0.1.4") (d #t) (k 2)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "url") (r "^1.2.3") (d #t) (k 0)))) (h "07plq8k8nhn0yrjcfzgcikwk3w83g7w0568vfwcgi2fq5nk734r3")))

(define-public crate-textile-0.2.1 (c (n "textile") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "maplit") (r "^0.1.4") (d #t) (k 2)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "url") (r "^1.2.3") (d #t) (k 0)))) (h "16gqapjd1xa26yil44kgghd3jjmnrhzbxdy943yvzkfz1nq90drp")))

