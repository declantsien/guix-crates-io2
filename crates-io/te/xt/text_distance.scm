(define-module (crates-io te xt text_distance) #:use-module (crates-io))

(define-public crate-text_distance-0.1.0 (c (n "text_distance") (v "0.1.0") (h "0pvhmgrmb6bx6s3fdhhssq81fiq43gm3qddsvcs73r8ddf8x9v20")))

(define-public crate-text_distance-0.2.0 (c (n "text_distance") (v "0.2.0") (h "09hf62mmlpidr7w31ra46ndxq5fda0lrm9xxp1402kr92is9a0sh")))

(define-public crate-text_distance-0.3.0 (c (n "text_distance") (v "0.3.0") (h "18qj240hdnfkghaf82mla4j8qq16xsfif9760p6nkqc6h9j32rq0")))

(define-public crate-text_distance-0.4.0 (c (n "text_distance") (v "0.4.0") (h "10i7r0afj3paw3q25yx9j11czss1g1g128dqnhd0s0k5k84nfzva")))

(define-public crate-text_distance-0.5.0 (c (n "text_distance") (v "0.5.0") (h "1q5h9b061rds7kvjnk28bsrjx51hclflnif7xphgiwq42h64c31h")))

