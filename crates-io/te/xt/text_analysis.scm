(define-module (crates-io te xt text_analysis) #:use-module (crates-io))

(define-public crate-text_analysis-0.1.0 (c (n "text_analysis") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "0246qx9n1k8a2m52v5k3fimp88pkdh1k179ncc8hzxsg92cw33bh")))

(define-public crate-text_analysis-0.1.1 (c (n "text_analysis") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "0kg79z7nk5284p154n8j9mgxw24pms6020vmaz502jjzq60hf44p")))

(define-public crate-text_analysis-0.1.2 (c (n "text_analysis") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.12") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "1asp59s6gd045ls0igif1hq5sari31l1p14sbkm6dwbq1d6g7jp0")))

(define-public crate-text_analysis-0.1.3 (c (n "text_analysis") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.12") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "05flm56pibg3xqa9m657g7nnbnyhy8wair5sdg4fsrsscjsisvjb")))

(define-public crate-text_analysis-0.1.4 (c (n "text_analysis") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "0ggr55jq4pfbimnwlqwbzzikhrp5i0q2a7jizccrdzqijb7jb6vd")))

(define-public crate-text_analysis-0.1.5 (c (n "text_analysis") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 0)))) (h "1s0kcngsiwykpk5p68j63nzdc60d3n571fbs0i71sj086x6l6x8g")))

(define-public crate-text_analysis-0.1.6 (c (n "text_analysis") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.4.1") (d #t) (k 0)))) (h "1icn8k9n49pg7fp04absbddg9zwdl7zl4v84v0cdzxwlc8kgjr72")))

(define-public crate-text_analysis-0.1.7 (c (n "text_analysis") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.4.1") (d #t) (k 0)))) (h "1wklzxy0my59a87wsk4av8c144289la313zx5is39aqdjmyyd5wz")))

(define-public crate-text_analysis-0.2.0 (c (n "text_analysis") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0dhip1zvxq2j7cr0vm5scg14p4mnnmqvb6rabpzl50kzx5sxqm1j")))

(define-public crate-text_analysis-0.2.1 (c (n "text_analysis") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0jrn6ykbx1jzgdh0vya0nw0qcf2nq67knj2ad8sfppsfri6vhvfl")))

(define-public crate-text_analysis-0.3.0 (c (n "text_analysis") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.6.4") (d #t) (k 0)))) (h "0dhf955rp8sib2irhyl6qkgsw24a90ibar3g8pn0shvjsrp80c66")))

(define-public crate-text_analysis-0.3.1 (c (n "text_analysis") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.6.4") (d #t) (k 0)))) (h "1mm6bv0s7d0v0j5a28gg541k1ly91n16q540xrx8av4qgn92h64z")))

(define-public crate-text_analysis-0.3.2 (c (n "text_analysis") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.7.4") (d #t) (k 0)))) (h "072ljwqigw55mmr412lbdn0ryfa3hs4hq8k3hg8m6ihrbf4i5r7f")))

