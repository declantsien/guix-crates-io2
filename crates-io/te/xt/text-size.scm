(define-module (crates-io te xt text-size) #:use-module (crates-io))

(define-public crate-text-size-0.99.0-dev.2 (c (n "text-size") (v "0.99.0-dev.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1vd3ikj3p42ssfih0xwjj7ykycsks91gh571cnhmmmxyhb5h1xq5")))

(define-public crate-text-size-0.99.0-dev.3 (c (n "text-size") (v "0.99.0-dev.3") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0aw04i4bynw94xv7zc706mv8bjav2r3a8zz5ib56a2gkaf6spral")))

(define-public crate-text-size-0.99.0 (c (n "text-size") (v "0.99.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "03gsi8ip0bkpv72gmhahsc2974qcnhp04j17v921y7l6qa2rharq")))

(define-public crate-text-size-1.0.0 (c (n "text-size") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "12kd18wggzrpn7my9nb2yxg5zr1rbiwf5w9p4g5pig63xpypwgph")))

(define-public crate-text-size-1.1.0 (c (n "text-size") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "02jn26l5wcdjqpy80ycnk9ha10flyc0p4yga8ci6aaz7vd4bb318")))

(define-public crate-text-size-1.1.1 (c (n "text-size") (v "1.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0cwjbkl7w3xc8mnkhg1nwij6p5y2qkcfldgss8ddnawvhf3s32pi")))

