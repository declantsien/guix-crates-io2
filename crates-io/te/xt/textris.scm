(define-module (crates-io te xt textris) #:use-module (crates-io))

(define-public crate-textris-0.1.0 (c (n "textris") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1qf51iffi9sgvmwicg30szix12kczjxd5y42g7695x63nmxyq6cr")))

(define-public crate-textris-0.2.0 (c (n "textris") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.5.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1hh2czx7s9zrl04pg2shzqgdgqf973bkv5njgbdgbi00w701gsps")))

