(define-module (crates-io te xt text_io) #:use-module (crates-io))

(define-public crate-text_io-0.0.1 (c (n "text_io") (v "0.0.1") (d (list (d (n "quasi") (r "*") (d #t) (k 0)) (d (n "quasi_macros") (r "*") (d #t) (k 0)))) (h "1jjl436abx18zrq0pwd7vgn19n37andpxy0f0w6h5pq55ak5by62")))

(define-public crate-text_io-0.0.2 (c (n "text_io") (v "0.0.2") (d (list (d (n "quasi") (r "*") (d #t) (k 0)) (d (n "quasi_macros") (r "*") (d #t) (k 0)))) (h "1fxlh0qhhdimhrbmcyvmh1d6sldz3f9c3sik4ragr7kcy1k4i3h6")))

(define-public crate-text_io-0.0.3 (c (n "text_io") (v "0.0.3") (d (list (d (n "quasi") (r "*") (o #t) (d #t) (k 0)) (d (n "quasi_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0kakzbp0dk7z3jxqjfjixms98rpwq27gy7arqcwq2bgvyx2na56v") (f (quote (("nightly" "quasi" "quasi_macros"))))))

(define-public crate-text_io-0.0.4 (c (n "text_io") (v "0.0.4") (d (list (d (n "quasi") (r "*") (o #t) (d #t) (k 0)) (d (n "quasi_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0gs0fkrwgzvqrskqw7ffdg2wdf9r18fpqdryxmbmmihg5pzixjmh") (f (quote (("nightly" "quasi" "quasi_macros"))))))

(define-public crate-text_io-0.1.0 (c (n "text_io") (v "0.1.0") (d (list (d (n "quasi") (r "*") (o #t) (d #t) (k 0)) (d (n "quasi_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1b9y6jl3wrhgm2vd5fiaiprfmzgvyc819ykgpg175b377wg7aazq") (f (quote (("nightly" "quasi" "quasi_macros"))))))

(define-public crate-text_io-0.1.1 (c (n "text_io") (v "0.1.1") (d (list (d (n "quasi") (r "*") (o #t) (d #t) (k 0)) (d (n "quasi_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0w35l530hprqlxqjm68hcl8hc1xvna7589rjq22kgb41mp30bjn4") (f (quote (("nightly" "quasi" "quasi_macros"))))))

(define-public crate-text_io-0.1.2 (c (n "text_io") (v "0.1.2") (d (list (d (n "quasi") (r "*") (o #t) (d #t) (k 0)) (d (n "quasi_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0anrdfhggiyr0ll7mcivma48pyfccw750izadsm2np5acqfw45n3") (f (quote (("nightly" "quasi" "quasi_macros"))))))

(define-public crate-text_io-0.1.3 (c (n "text_io") (v "0.1.3") (h "1k41w4n9yfmh3wwyq16hzz50fiq4nm223jhs2501r5l4qgc316cv")))

(define-public crate-text_io-0.1.4 (c (n "text_io") (v "0.1.4") (h "1wml39amkms510zbydmgj6vkxxgaz5lv0yx136dc28v1df8i3f5q")))

(define-public crate-text_io-0.1.5 (c (n "text_io") (v "0.1.5") (h "16n1f7ps6ajcm1gq14qy6nl4374adn3k00si2wxhvk18m9hy2nxp")))

(define-public crate-text_io-0.1.6 (c (n "text_io") (v "0.1.6") (h "0v9yyy1z1gcv6mc5vcdpfjhsxcphk4pvgyq0sm31dw787bkjw389")))

(define-public crate-text_io-0.1.7 (c (n "text_io") (v "0.1.7") (h "0l0a6p1mhbh2way6z2j5cw5mbdly1f4ka8xsfv140ahxplgbcn4n")))

(define-public crate-text_io-0.1.8 (c (n "text_io") (v "0.1.8") (h "1m6j1wr6qnhz78pbw92sn11faqw95p0mjqn5zcsqii3xyjs71cbc")))

(define-public crate-text_io-0.1.9 (c (n "text_io") (v "0.1.9") (h "0xmsb19kjymnb93rr7gyjcwkn1aswvwsl0ilh04ip6a8kc0vax7f")))

(define-public crate-text_io-0.1.10 (c (n "text_io") (v "0.1.10") (h "0i8ga7fcrv72s5cw77dylgpnxhl9pnp0x3lm5c2qk15xwrs2cbs4")))

(define-public crate-text_io-0.1.12 (c (n "text_io") (v "0.1.12") (h "0imsbfyfd5mdpy2sxzhw9d7r4705nfcz824mlsk1436p5bmwiw6m")))

