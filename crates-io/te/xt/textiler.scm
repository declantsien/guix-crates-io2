(define-module (crates-io te xt textiler) #:use-module (crates-io))

(define-public crate-textiler-0.1.0 (c (n "textiler") (v "0.1.0") (d (list (d (n "textiler-core") (r "^0.1.0") (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (d #t) (k 0)))) (h "1jgckn908b4ln54g7j9wwsijw8fyzvj7ld3g3gk0g4ph8mh629qb")))

