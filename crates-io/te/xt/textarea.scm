(define-module (crates-io te xt textarea) #:use-module (crates-io))

(define-public crate-textarea-0.0.0 (c (n "textarea") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.68") (k 0)) (d (n "rustyline") (r "^10.0.0") (k 0)))) (h "0haqzqrg696jikh4c77zf36sw98smq8d2cpiz254rf5dpi2y9qdk") (f (quote (("multiline" "rustyline/custom-bindings") ("default" "multiline"))))))

