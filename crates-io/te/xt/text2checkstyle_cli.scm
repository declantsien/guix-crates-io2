(define-module (crates-io te xt text2checkstyle_cli) #:use-module (crates-io))

(define-public crate-text2checkstyle_cli-0.0.6 (c (n "text2checkstyle_cli") (v "0.0.6") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)))) (h "1ciy643w9z9ryfh9vnh75a1zniswmjq1chljlz5z307ipjxag4b9")))

(define-public crate-text2checkstyle_cli-0.0.7 (c (n "text2checkstyle_cli") (v "0.0.7") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)))) (h "0h3fhlcsnnfqwcq7b8v25vg50hqilq8ah6xbs58isjb0mf2sds80")))

(define-public crate-text2checkstyle_cli-0.0.8 (c (n "text2checkstyle_cli") (v "0.0.8") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)))) (h "1ys86bqdvdxj1vpiwdv9h6zicgc7wbhk8nrg15nqb3byqxr6i1hy")))

(define-public crate-text2checkstyle_cli-0.0.9 (c (n "text2checkstyle_cli") (v "0.0.9") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)))) (h "0vz88vi045x5p7xrpglnnqngxzlvv1kb38b8jlz8pgq244k6wyls")))

(define-public crate-text2checkstyle_cli-0.0.10 (c (n "text2checkstyle_cli") (v "0.0.10") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)))) (h "16pkp7ylipa22i4iw0f912absaj9p1qpa41pdv7psj0yd6jnkznm")))

(define-public crate-text2checkstyle_cli-0.1.0 (c (n "text2checkstyle_cli") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)))) (h "1bk4gr72n7rmrdjlha1q74azywl90y6xfc91w63dqvccb7l5cqdb")))

(define-public crate-text2checkstyle_cli-1.0.0 (c (n "text2checkstyle_cli") (v "1.0.0") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "text2checkstyle") (r "^0.1.0") (d #t) (k 0)))) (h "05bvsdqhwaka3w06j796jy8h0wjsg13n1c5jnvlyhadjnmni5wpc")))

(define-public crate-text2checkstyle_cli-1.1.0 (c (n "text2checkstyle_cli") (v "1.1.0") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "text2checkstyle") (r "^1.0.0") (d #t) (k 0)))) (h "1fsangm8g25yszvnsj6zn0ms73q0shlwxw51xzskimxw6zcyh9hm")))

(define-public crate-text2checkstyle_cli-1.1.1 (c (n "text2checkstyle_cli") (v "1.1.1") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "text2checkstyle") (r "^1.0.0") (d #t) (k 0)))) (h "1ax51sjw8l87fnp74kdwa767vf0srhi1j0a8d91abj9znf5s7411")))

