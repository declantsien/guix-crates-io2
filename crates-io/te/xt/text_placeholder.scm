(define-module (crates-io te xt text_placeholder) #:use-module (crates-io))

(define-public crate-text_placeholder-0.1.0 (c (n "text_placeholder") (v "0.1.0") (h "0q94r3s35qa0qhvn3djlc913xx26gmlk5sr3hy3whhzskqzslml2")))

(define-public crate-text_placeholder-0.2.0 (c (n "text_placeholder") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1803kps99ynys4m01wlpw7ymv283yfmr5ibv68cx0j99w1nk0f1c") (f (quote (("struct_context" "serde" "serde_json"))))))

(define-public crate-text_placeholder-0.2.1 (c (n "text_placeholder") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0lbw7apv6j8mjn3yw68p7zya4j5facdhni9rsq4s47krazaky0gd") (f (quote (("struct_context" "serde" "serde_json"))))))

(define-public crate-text_placeholder-0.3.0 (c (n "text_placeholder") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0yxgw9rn20353b49kvbyr63v2vyly4xfbg669bmiy78gy74md4mj") (f (quote (("struct_context" "serde" "serde_json"))))))

(define-public crate-text_placeholder-0.3.1 (c (n "text_placeholder") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01zgxfd0fp9hn1cj59n8i568g9x02hns67jnv5j2di806hahb40v") (f (quote (("struct_context" "serde" "serde_json"))))))

(define-public crate-text_placeholder-0.4.0 (c (n "text_placeholder") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1x4yjsa944vz14cbdwainh252yjnc2dsghzl0raf6j6px35szmnb") (f (quote (("struct_context" "serde" "serde_json"))))))

(define-public crate-text_placeholder-0.4.1 (c (n "text_placeholder") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0q0iq60dflczvyfp3m4wn3w358yyxmcljmzdb9b5favwnb2c8qcn") (f (quote (("struct_context" "serde" "serde_json"))))))

(define-public crate-text_placeholder-0.5.0 (c (n "text_placeholder") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0w608l9jxl8rm98x5k79b6lgf8fc2iyrs3rlypg50vyfhbwh88ai") (f (quote (("struct_context" "std" "serde" "serde_json") ("std") ("default" "std"))))))

