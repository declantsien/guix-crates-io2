(define-module (crates-io te xt textparse) #:use-module (crates-io))

(define-public crate-textparse-0.0.1 (c (n "textparse") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "textparse_derive") (r "^0.0.1") (d #t) (k 0)))) (h "05764ngzzh9jrnaawxa7x7hngbw9wq504z63lghsli0xsbzzgh4k")))

(define-public crate-textparse-0.0.2 (c (n "textparse") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "textparse_derive") (r "^0.0.1") (d #t) (k 0)))) (h "1lj9hcql6vai8kzq5lmvbngli5as4b472kbzka170wv2cn373rpx")))

(define-public crate-textparse-0.1.0 (c (n "textparse") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "textparse_derive") (r "^0.1") (d #t) (k 0)))) (h "0xh8963c0kpv7plnxcj6ipm64ivn08r3nmz5l2hmls1qxk9f5j7s")))

