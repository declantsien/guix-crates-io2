(define-module (crates-io te xt text-completions) #:use-module (crates-io))

(define-public crate-text-completions-0.1.0 (c (n "text-completions") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10") (f (quote ("completion"))) (d #t) (k 0)) (d (n "envmnt") (r "^0.10") (d #t) (k 0)))) (h "0k4d9z8vr4hbnf517mn10qh4kl6442py6vas1ik0wjzwdihf76wr")))

