(define-module (crates-io te xt textocx) #:use-module (crates-io))

(define-public crate-textocx-0.1.0 (c (n "textocx") (v "0.1.0") (d (list (d (n "ac") (r "^2.2.0") (d #t) (k 0) (p "async-channel")) (d (n "clipboard-win") (r "^5.1.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nwd") (r "^1.0.4") (d #t) (k 0) (p "native-windows-derive")) (d (n "nwg") (r "^1.0.13") (f (quote ("flexbox"))) (d #t) (k 0) (p "native-windows-gui")) (d (n "rq") (r "^0.4.3") (f (quote ("exports" "futures" "rust-alloc" "loader" "dyn-load" "macro"))) (k 0) (p "rquickjs")))) (h "0343ssknn7r5ipq04lddg33mylpcgj5b3kpv6aawk7lj4sp11x4j")))

