(define-module (crates-io te xt text-blind-watermark) #:use-module (crates-io))

(define-public crate-text-blind-watermark-0.1.0 (c (n "text-blind-watermark") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18jnslipn2lpgc4rvvhfg82cv1ac7bimy35s2cy0x63ayrc9b4ab")))

(define-public crate-text-blind-watermark-0.1.1 (c (n "text-blind-watermark") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nzwlxixwsxjs0xinppdx2nq3vvl4x3pw1h6mhss896q09x7y9c7")))

(define-public crate-text-blind-watermark-0.1.2 (c (n "text-blind-watermark") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "112hf8p36g7wj7szzbqqklbppflwfrz6dxifx8dssa2503q2z7i1")))

(define-public crate-text-blind-watermark-0.1.3 (c (n "text-blind-watermark") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bgvfkain62ipziqijriggr2cy4mkkrlliz3vc0g4nsg89cxqrla")))

(define-public crate-text-blind-watermark-0.1.4 (c (n "text-blind-watermark") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wrvvj9fp6y98r2g9sz9glhxc0ixdsjashjvbx632h8z9bzg903f")))

(define-public crate-text-blind-watermark-0.1.5 (c (n "text-blind-watermark") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pxhxxv4wnklmjvzm3r26a5iyrjgsyzfskr7aihid69bqj3ybqrq")))

