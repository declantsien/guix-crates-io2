(define-module (crates-io te xt textract) #:use-module (crates-io))

(define-public crate-textract-0.1.0 (c (n "textract") (v "0.1.0") (d (list (d (n "dotext_ed8fc7b") (r "^0.1.2") (d #t) (k 0)) (d (n "html2text") (r "^0.4.2") (d #t) (k 0)) (d (n "tree_magic") (r "^0.2.3") (d #t) (k 0)))) (h "0yjpl0n5q370c43za0q61gc9gl5xzj1sag8a3535pwsa0lxrpkvx")))

