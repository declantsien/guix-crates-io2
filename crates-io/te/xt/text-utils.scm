(define-module (crates-io te xt text-utils) #:use-module (crates-io))

(define-public crate-text-utils-0.1.0 (c (n "text-utils") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "15cv4a80mg6fc4i7065b4hb8dn5k16jda7zwy8z8y482wmyigbvi")))

(define-public crate-text-utils-0.2.0 (c (n "text-utils") (v "0.2.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "0izpjfr0pm48cpa4synqrgrsi9qw8kv39q0wl2f6xg9cypr0hpyh")))

(define-public crate-text-utils-0.2.1 (c (n "text-utils") (v "0.2.1") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "1rxilazb5d0l06s425qq5fcijm43nv5b2q47dzvfx5bak8vg6ash")))

(define-public crate-text-utils-0.2.2 (c (n "text-utils") (v "0.2.2") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "0n528y2hdmh6gpl25szsgp0944g4arspa8916yd8hjawh4bd7bky")))

(define-public crate-text-utils-0.2.3 (c (n "text-utils") (v "0.2.3") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "19q267iqj6x9p2fcq7zb0hnhl8ib6kmkph9hh3i2jc6ab4n7xr2x")))

(define-public crate-text-utils-0.2.4 (c (n "text-utils") (v "0.2.4") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "10y6mbqwyl9px354p7h2k4xpkpcsygcb2nyp2cz0sd838i6v57cz")))

(define-public crate-text-utils-0.2.5 (c (n "text-utils") (v "0.2.5") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "0dww5y36qbyqq6gdzczm9mbz7lh0j1hzyvk1w5wqi4bqshr59xzi")))

(define-public crate-text-utils-0.3.0 (c (n "text-utils") (v "0.3.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "1n2lxb952kzy8m3mrpnbm36p4agmr8c35qncfzjxb05pjikx6182")))

(define-public crate-text-utils-0.3.1 (c (n "text-utils") (v "0.3.1") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "1jg7cv1br88hs8gbm09nv78zx9lih4248bqxqf951jywvfyl0h77")))

(define-public crate-text-utils-0.4.0 (c (n "text-utils") (v "0.4.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "017jlxhjj03nc0l12jdchrih7s1jv2hqj9b6a60fl61lmbwl37qh")))

(define-public crate-text-utils-0.4.1 (c (n "text-utils") (v "0.4.1") (d (list (d (n "emojic") (r "^0.4.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)))) (h "055k1gqn3ywap8yh8qy6yqmxwydpl9h75dghyw8s799zqn19k57s")))

(define-public crate-text-utils-0.4.2 (c (n "text-utils") (v "0.4.2") (d (list (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "emojic") (r "^0.4.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)))) (h "1midg7b1fng0v8hi0ivwdcfi4n8hrkddsc0mxcjm82kq22nrm3ry")))

(define-public crate-text-utils-0.4.3 (c (n "text-utils") (v "0.4.3") (d (list (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "emojic") (r "^0.4.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "slugify") (r "^0.1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)))) (h "0cmmj8nba2i23ss0vy4m5lyyasm2xk1k85y8sgxd3ylrprp8a1vj")))

