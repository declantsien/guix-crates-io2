(define-module (crates-io te xt text-transliterate) #:use-module (crates-io))

(define-public crate-text-transliterate-0.1.0 (c (n "text-transliterate") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1wkpfjkh8a5bfsg5lsgkn5367wd0cr1rsaap8f8fdmcm3sg4jqlb")))

(define-public crate-text-transliterate-1.0.0 (c (n "text-transliterate") (v "1.0.0") (d (list (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0viiq3s0ld5py2drlq7g4d4bcpmi0z3y12f7avzsh20hmnxb0s8n")))

(define-public crate-text-transliterate-1.1.0 (c (n "text-transliterate") (v "1.1.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1918rh3f4ms3k58swx96a4qkvga2qhgwikxsvp55jl41h1qn5n6n")))

(define-public crate-text-transliterate-1.1.1 (c (n "text-transliterate") (v "1.1.1") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "04929s8m9vk7hnq7dpi5jnfis08a6ppp8kdp19563qm81x955lvb")))

(define-public crate-text-transliterate-1.1.2 (c (n "text-transliterate") (v "1.1.2") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0n2k6107j399kwxf8sxgsqbzzwyr4jz8ldxjhf5f4w367p2vmvrs")))

(define-public crate-text-transliterate-1.1.3 (c (n "text-transliterate") (v "1.1.3") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "18kc7iqn7s35djs42synq3agkx14wb3pwlzdnpjzkixw3xhg413k")))

(define-public crate-text-transliterate-2.0.0 (c (n "text-transliterate") (v "2.0.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "024mrcg50ip0230k7yz7gnrd85di52ipwdvpl1zn6l88ly30gmh6")))

