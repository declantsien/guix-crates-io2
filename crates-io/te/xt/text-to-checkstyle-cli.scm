(define-module (crates-io te xt text-to-checkstyle-cli) #:use-module (crates-io))

(define-public crate-text-to-checkstyle-cli-0.0.1 (c (n "text-to-checkstyle-cli") (v "0.0.1") (h "0cxsxlk23n5n70jmxcscazn0ibr0gl71aisx98q7lphlim17drf4") (y #t)))

(define-public crate-text-to-checkstyle-cli-0.0.2 (c (n "text-to-checkstyle-cli") (v "0.0.2") (h "0i5rdsmp4frvpvw7qi7291z95yn7ldyynrjfdfa245sy21537sax") (y #t)))

(define-public crate-text-to-checkstyle-cli-0.0.3 (c (n "text-to-checkstyle-cli") (v "0.0.3") (h "0mpmlddchrcqb8n91zq40brjsm7yxm32sd3hd97zhdhyy24cy2z6") (y #t)))

(define-public crate-text-to-checkstyle-cli-0.0.4 (c (n "text-to-checkstyle-cli") (v "0.0.4") (d (list (d (n "atty") (r "^0.2.3") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)))) (h "0hcgwhxx9pd0avijxa0y4dpd31f3nbvari3d2h3bfvrbhir32kvh") (y #t)))

