(define-module (crates-io te xt textyle) #:use-module (crates-io))

(define-public crate-textyle-0.1.0 (c (n "textyle") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "defer-lite") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0kq4kkjlm7mk1aaj4nr3qv04fpsy0wbrr6mk9rd7cv2rghc8d0a2")))

