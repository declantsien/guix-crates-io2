(define-module (crates-io te xt textspan) #:use-module (crates-io))

(define-public crate-textspan-0.1.0 (c (n "textspan") (v "0.1.0") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "tokenizations") (r "^0.3") (d #t) (k 0)))) (h "04k6gss9rrcmflkwm86hb0ibyv4sacdl7gv48lp39nwb9msvmwpx")))

(define-public crate-textspan-0.2.0 (c (n "textspan") (v "0.2.0") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "tokenizations") (r "^0.4") (d #t) (k 0)))) (h "15m9f7blv0qvhiqayrkgmrabkixbhl67pz0m8kzq5wabyl2c0k7x")))

(define-public crate-textspan-0.2.2 (c (n "textspan") (v "0.2.2") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "tokenizations") (r "^0.4") (d #t) (k 0)))) (h "045nli260d4gkf0mkxi768lik6ny9h7l7z1wam96sgp9jzsbxqh8")))

(define-public crate-textspan-0.4.1 (c (n "textspan") (v "0.4.1") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "tokenizations") (r "^0.4") (d #t) (k 0)))) (h "0afqfcch4q4s274j2c8gzw38rvsim0skrvfab123lgj82xvgmvmw")))

(define-public crate-textspan-0.5.0 (c (n "textspan") (v "0.5.0") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "tokenizations") (r "^0.4") (d #t) (k 0)))) (h "15bd14r4rqm2pfxvmxpbh51sza1ymbgk3p135hpbml08ixgas63b")))

(define-public crate-textspan-0.5.2 (c (n "textspan") (v "0.5.2") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "rstest") (r "^0.7.0") (d #t) (k 2)) (d (n "tokenizations") (r "^0.4.2") (d #t) (k 0)))) (h "06wf1wj1ja3d09sy1hfgblas53afrg76r4mc6if1v128cbffx6aq")))

