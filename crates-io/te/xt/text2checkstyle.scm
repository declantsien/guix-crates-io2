(define-module (crates-io te xt text2checkstyle) #:use-module (crates-io))

(define-public crate-text2checkstyle-0.0.2 (c (n "text2checkstyle") (v "0.0.2") (h "1pi3p60px82hzs3xhpnyfzn9hvb13wa8lmqvhjc1yqin268h4dcx")))

(define-public crate-text2checkstyle-0.1.0 (c (n "text2checkstyle") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.9.4") (d #t) (k 0)))) (h "07r3b4qw3s0rw76w9ysf3q2r2253bxb3vdz6964z60ca5brwwf9s")))

(define-public crate-text2checkstyle-1.0.0 (c (n "text2checkstyle") (v "1.0.0") (d (list (d (n "checkstyle_formatter") (r "^1.0.0") (d #t) (k 0)))) (h "0d4q8vs21ysbpn1zy3rpbl2xfccs964ymmrq9a100rzvgjg3kx4c")))

(define-public crate-text2checkstyle-1.0.1 (c (n "text2checkstyle") (v "1.0.1") (d (list (d (n "checkstyle_formatter") (r "^1.0.0") (d #t) (k 0)))) (h "1v9kyjn0fr5rlj4wwa77mf21mcwbwk2vgxz1aqn953j15gcf0rsi")))

(define-public crate-text2checkstyle-1.0.2 (c (n "text2checkstyle") (v "1.0.2") (d (list (d (n "checkstyle_formatter") (r "^1.0.0") (d #t) (k 0)))) (h "18va4n5igwyjzgslmxlhr2haiqp0242gvs37zvlwh2x0pr35695i")))

