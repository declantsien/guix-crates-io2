(define-module (crates-io te xt text-to-sounds) #:use-module (crates-io))

(define-public crate-text-to-sounds-0.1.2 (c (n "text-to-sounds") (v "0.1.2") (h "1x37bjkbjllzc7s1igxjr2dqxfl024gfnam2aqm80rjd30g12l7r")))

(define-public crate-text-to-sounds-0.1.3 (c (n "text-to-sounds") (v "0.1.3") (h "13dn4b4hrb4hzpa6hdvgim23bd8rkrikg3360nvgm0kdzcxjrxkb")))

(define-public crate-text-to-sounds-0.1.4 (c (n "text-to-sounds") (v "0.1.4") (h "0pm02gkrq4v80b3shiyipsk2fagxx5dx95pkrwclrrlsaqv9b74l")))

(define-public crate-text-to-sounds-0.2.0 (c (n "text-to-sounds") (v "0.2.0") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1v19kva2rh98z8mk6a4ka78zpywhskpwzjhms8avnhgddf2s3wqh")))

(define-public crate-text-to-sounds-0.2.1 (c (n "text-to-sounds") (v "0.2.1") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "09bpsls7pj6zsmx30gdr5sybwa8cpg8gbqj73khavvxhxypm1ivm")))

(define-public crate-text-to-sounds-0.2.2 (c (n "text-to-sounds") (v "0.2.2") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1pcx3navll52pf0nn11yi98nq0ma4b5n69d5dq0i337zk5zn2y8x")))

(define-public crate-text-to-sounds-0.3.0 (c (n "text-to-sounds") (v "0.3.0") (d (list (d (n "js-sys") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "serde" "js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "17q6pj6iij4gsr5jpl3094admbadd9pz53jbmzjck2if5f6f8rqx")))

(define-public crate-text-to-sounds-0.3.1 (c (n "text-to-sounds") (v "0.3.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.58") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "serde" "js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0zvnzpah2400rh80wvhay9b7dp7j9vi55xsfcgj47cz69zvrg9aq")))

(define-public crate-text-to-sounds-1.0.0 (c (n "text-to-sounds") (v "1.0.0") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "1n4syq5r5kb1bwfawbv447582zjzf1a4s5h9kmrgxzb4i18ix9w4")))

(define-public crate-text-to-sounds-1.0.1 (c (n "text-to-sounds") (v "1.0.1") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "1fckvss1inbrzv1lkz51df9ip8ajzkxzj4x959kv0g8agh8xh5ya")))

(define-public crate-text-to-sounds-1.0.2 (c (n "text-to-sounds") (v "1.0.2") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "0mpdys1rhm35w2y6aqfadz5h18bbfn9vikd7f2l0ywk193ani93n")))

(define-public crate-text-to-sounds-1.1.0 (c (n "text-to-sounds") (v "1.1.0") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "0hphip0vqpdn4lm5knlvg2xhixmqd6zf85aajrmbdjyy4dgbz8hg")))

(define-public crate-text-to-sounds-1.1.1 (c (n "text-to-sounds") (v "1.1.1") (d (list (d (n "uuid") (r "^1.1.2") (f (quote ("v4" "js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "1qk3b4g07rpm3rrbmzdp9xmygijijjj97nn9ajbhdw26ca2pf7f4")))

