(define-module (crates-io te xt textual) #:use-module (crates-io))

(define-public crate-textual-0.1.0 (c (n "textual") (v "0.1.0") (h "0d8zr9rs9fk6zlihh97snp9sbvryzmrvfkp7kskbhm5xivfs918g") (y #t)))

(define-public crate-textual-0.1.1 (c (n "textual") (v "0.1.1") (h "0dqirvk02jsjwr01hvj37ff4bw3s93j3z8lnjdvlyc09acxx85vk") (y #t)))

