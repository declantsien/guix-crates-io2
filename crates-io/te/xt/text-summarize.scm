(define-module (crates-io te xt text-summarize) #:use-module (crates-io))

(define-public crate-text-summarize-0.1.0 (c (n "text-summarize") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1f4l9hrm9jsmqdksq1x905cnd03s03jwa26gg9dcpkahgx2ga618")))

(define-public crate-text-summarize-0.1.1 (c (n "text-summarize") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1a3wkzxqmy1fdwfr11yki29yvaskm1c1cjd4xrsb0126ss1lipv2")))

(define-public crate-text-summarize-0.1.2 (c (n "text-summarize") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "03rxmlavxd5ar6k3lkdvsgigk9dc8ljq5z753czc394pks5v04nx")))

(define-public crate-text-summarize-0.1.3 (c (n "text-summarize") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1pnsrichsn49nw3i29ph6gw8bd68s1adspq86s03sz4b5c34j0hq")))

