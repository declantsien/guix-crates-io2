(define-module (crates-io te xt text_block_layout) #:use-module (crates-io))

(define-public crate-text_block_layout-0.1.0 (c (n "text_block_layout") (v "0.1.0") (h "1c1l4mqs30glfbxjij03yk72mi863c9zgqy6cvh7f4ji1549vhnq")))

(define-public crate-text_block_layout-0.1.1 (c (n "text_block_layout") (v "0.1.1") (h "060fckqh2xr9cc0mwg46is4bv3avngi8mi2708fgkmk8h18iapgw")))

(define-public crate-text_block_layout-1.0.0 (c (n "text_block_layout") (v "1.0.0") (d (list (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1y09p9kfncdnqiiqp6ia0kvksiamd9rgpn3qbyjqk4vrm94cr3pz")))

(define-public crate-text_block_layout-1.1.0 (c (n "text_block_layout") (v "1.1.0") (d (list (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1rvk8a60l4r4zlh7xcsr1k9n8pm8a09m9k2k4rgvk8x2ry3jck7m")))

(define-public crate-text_block_layout-1.2.0 (c (n "text_block_layout") (v "1.2.0") (d (list (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1hrq5bd58pl7plxa3vj0a3npyf8hpgrjclb843f9yqn946fcgy55")))

