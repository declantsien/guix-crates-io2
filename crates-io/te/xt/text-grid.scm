(define-module (crates-io te xt text-grid) #:use-module (crates-io))

(define-public crate-text-grid-0.1.0 (c (n "text-grid") (v "0.1.0") (d (list (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "0gdrf8rxqjkjjhqcksk5px0xcfhwdhrbxzz8kdw849rg0p2w51cy")))

(define-public crate-text-grid-0.1.1 (c (n "text-grid") (v "0.1.1") (d (list (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1yrd77mq25np264i51bair8wysy4z3zfhjwi4ddhczv80k0880k5")))

(define-public crate-text-grid-0.2.0 (c (n "text-grid") (v "0.2.0") (d (list (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "07algcynivs6jg8xx9dwbfsrbkriwnc513vig0yz46c5fms19b7l")))

(define-public crate-text-grid-0.3.0 (c (n "text-grid") (v "0.3.0") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "05d3cnsrsmrpv2rkvb8ds81ap28q3krdklmlss3k3q3y3shb7qp6")))

(define-public crate-text-grid-0.4.0 (c (n "text-grid") (v "0.4.0") (d (list (d (n "derive-ex") (r "^0.1.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0d1njs74kq90q7znca8g43ric4fyh3n3n4gxp73zdjbhfi3m7x0v")))

(define-public crate-text-grid-0.4.1 (c (n "text-grid") (v "0.4.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "derive-ex") (r "^0.1.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "text-grid-macros") (r "=0.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "196mrkl04cmpam412ml7qah37i28y30nf5073vc1z3h505hgibqa")))

