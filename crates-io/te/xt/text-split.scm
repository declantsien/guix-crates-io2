(define-module (crates-io te xt text-split) #:use-module (crates-io))

(define-public crate-text-split-0.1.0 (c (n "text-split") (v "0.1.0") (d (list (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "093wzcvs6vv8v05grcn68k79cv0px3vqq4m2fcsd16j0idmx9ff7")))

