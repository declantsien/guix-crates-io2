(define-module (crates-io te xt text_table) #:use-module (crates-io))

(define-public crate-text_table-0.0.1 (c (n "text_table") (v "0.0.1") (d (list (d (n "stainless") (r "^0.0.7") (d #t) (k 0)))) (h "05zg5kdbpphxxrbqxhnp3d54wscz9ndz1rlr1h6g3iwh1djp79m5")))

(define-public crate-text_table-0.0.2 (c (n "text_table") (v "0.0.2") (d (list (d (n "stainless") (r "^0.0.9") (d #t) (k 0)))) (h "0633w2yc2qjxi9b034xiq1yz09wrsabsjqv4mw6bigd0sg0k58lj")))

(define-public crate-text_table-0.0.3 (c (n "text_table") (v "0.0.3") (d (list (d (n "stainless") (r "^0.0.10") (d #t) (k 0)))) (h "0d4rx8z09khqsqah6xmn44hlkfy7d2kasp0245cb4fkbi2z53jhf")))

(define-public crate-text_table-0.0.4 (c (n "text_table") (v "0.0.4") (d (list (d (n "speculate") (r "^0.0.8") (d #t) (k 0)))) (h "14iafnb6syykdivxc37j2ca10kmwc3vk78yh7hys4zaig4pzn2ym")))

