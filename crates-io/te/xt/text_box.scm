(define-module (crates-io te xt text_box) #:use-module (crates-io))

(define-public crate-text_box-0.1.5 (c (n "text_box") (v "0.1.5") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "19h0bhbzsrm6zhb6481j4gp0a13dxdyd9wsn597gvdfmpvy590ay")))

(define-public crate-text_box-0.1.6 (c (n "text_box") (v "0.1.6") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1wx5m3liwv20fl62dmcp4mgcqr92lsvmxz827ppd4jni56y40vj8")))

(define-public crate-text_box-0.1.7 (c (n "text_box") (v "0.1.7") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1y17mnkixjbi7zafhwj7ysy2a827gjk5hj4fiz1cvl3ih8a8hhzd")))

(define-public crate-text_box-0.1.8 (c (n "text_box") (v "0.1.8") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1zan8lk0kcjcbg0jzl5p4pa2x7cql994y93lz11p3bywl5zc9n89")))

(define-public crate-text_box-0.1.9 (c (n "text_box") (v "0.1.9") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "0fl3jcwb5wgvlc7m7vj58n5kbvg8ra5xh47aj8hpkdxcc94rxnfi")))

(define-public crate-text_box-0.1.10 (c (n "text_box") (v "0.1.10") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "14jvn8b379ynnbngpfs1iaaka9g82x31mr7b2ryj1kzfmyc96c1s")))

(define-public crate-text_box-0.1.11 (c (n "text_box") (v "0.1.11") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "0pbw13vbg78y4kpwybjvk0daag19rarxrx73yjw8n73bm9z7ws5c")))

(define-public crate-text_box-0.2.0 (c (n "text_box") (v "0.2.0") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "14gz6g3phby66ilpdxpv1kfnb2zm06s2l6bk10hzcnwm682kxbgy")))

(define-public crate-text_box-0.2.1 (c (n "text_box") (v "0.2.1") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1k6xpvvjkpjqvx7fiwz3hgwipkxc7xzgssd48bs6b2y3ik3dqsrf")))

(define-public crate-text_box-0.2.2 (c (n "text_box") (v "0.2.2") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "0h1kr6cfq66msg8kzc4drafi08ysx5kf8wi1y0xydrhjd5c7r5bb")))

(define-public crate-text_box-0.2.3 (c (n "text_box") (v "0.2.3") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "143b487c9ndvhpabj5bxrqbpr23fq71l8099cbkfj6gai43g7nk5")))

(define-public crate-text_box-0.2.4 (c (n "text_box") (v "0.2.4") (d (list (d (n "termion") (r "^1.5.4") (d #t) (k 0)))) (h "1vrff1wnyw7q0g3v5y11bl3k0xl22lqfhl2frpdlsj4kbqpd59ss")))

