(define-module (crates-io te xt textwrap-macros-impl) #:use-module (crates-io))

(define-public crate-textwrap-macros-impl-0.1.0 (c (n "textwrap-macros-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)))) (h "1z4rdirg9qdp0vl4695salxi2n8ri222h4ar4as4jzxcpzmp8irv")))

(define-public crate-textwrap-macros-impl-0.2.0 (c (n "textwrap-macros-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)))) (h "1jsx778xnxzpqx0hkkz6hyd8b2gw3ym1jk77zb7mj0pm80kqlp8d")))

(define-public crate-textwrap-macros-impl-0.2.1 (c (n "textwrap-macros-impl") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full"))) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)))) (h "0pwf2w8gdcw8ivy3qcq1wmr1w8sqcbgbd66kw8hnc96iv5vmk0nc")))

(define-public crate-textwrap-macros-impl-0.2.2 (c (n "textwrap-macros-impl") (v "0.2.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "printing" "proc-macro"))) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)))) (h "0q4wlhm6xnsrrdnazbsqk3m61mvm0s62wmxdiwkgdjmfq3n8xjjz")))

(define-public crate-textwrap-macros-impl-0.2.3 (c (n "textwrap-macros-impl") (v "0.2.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "printing" "proc-macro"))) (k 0)) (d (n "textwrap") (r "^0.12.0") (d #t) (k 0)))) (h "097yhi9fhk1cxx2mlhxccgj858ljfc9q9by3yjygab38ka4j8nni")))

(define-public crate-textwrap-macros-impl-0.2.4 (c (n "textwrap-macros-impl") (v "0.2.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "printing" "proc-macro"))) (k 0)) (d (n "textwrap") (r "^0.13") (d #t) (k 0)))) (h "0i9vm38339wabygcg5j3xg3h4k7zf946gpr67w96qvk4ay6gzq92")))

(define-public crate-textwrap-macros-impl-0.2.5 (c (n "textwrap-macros-impl") (v "0.2.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "printing" "proc-macro"))) (k 0)) (d (n "textwrap") (r "^0.13") (d #t) (k 0)))) (h "1d9dvcz9d36n29cfmhdq9x2l8wdycvipx959bjl5jcsg2csfqm30")))

(define-public crate-textwrap-macros-impl-0.3.0 (c (n "textwrap-macros-impl") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "printing" "proc-macro"))) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "1k633z2vzn9i4hn0wmp8sx6bcchj6b38hqq8whw59j3iiw99wdrj")))

