(define-module (crates-io te xt text-reader) #:use-module (crates-io))

(define-public crate-text-reader-0.1.0 (c (n "text-reader") (v "0.1.0") (h "1za88dni85l9wn9dcaq4p0haqy312pz4vnjp7i31mb7zc36cb7qb")))

(define-public crate-text-reader-0.1.1 (c (n "text-reader") (v "0.1.1") (h "1lkab3g8bpvavykd6i481f6f854x3mr7pcsfb2wbw6j5lskj24g7")))

(define-public crate-text-reader-0.2.0 (c (n "text-reader") (v "0.2.0") (h "1fhjfjfq5zb2hiwzf3nv1zcz7mwfjmhd6r9wxphd9rfyhgs26dy0")))

(define-public crate-text-reader-0.2.1 (c (n "text-reader") (v "0.2.1") (h "126j7ihqzg85pkvncwdkf3jipmb3cmpvjb9asc6p8xb4kidc1pb9")))

