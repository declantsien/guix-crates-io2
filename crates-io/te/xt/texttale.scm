(define-module (crates-io te xt texttale) #:use-module (crates-io))

(define-public crate-texttale-0.1.0 (c (n "texttale") (v "0.1.0") (d (list (d (n "rustyline") (r "^11.0") (d #t) (k 0)))) (h "1nx91qim4p3zlj57zd92rjaf2v5miw6bnwffwqcm25kc7hgcicvd")))

(define-public crate-texttale-0.1.1 (c (n "texttale") (v "0.1.1") (d (list (d (n "rustyline") (r "^11.0") (d #t) (k 0)))) (h "1n9v0yq8n62rwzxcf8ngj8hkl07m88cin78lgab4whfnigb3cd3k")))

(define-public crate-texttale-0.2.0 (c (n "texttale") (v "0.2.0") (d (list (d (n "rustyline") (r "^11.0") (d #t) (k 0)))) (h "0nbri2q6m6bgj9b7s39s20haw4wnx27jyvng23qa43pgpaq2hbp4")))

(define-public crate-texttale-0.3.0 (c (n "texttale") (v "0.3.0") (d (list (d (n "rustyline") (r "^11.0") (d #t) (k 0)) (d (n "utilz") (r "^0.3") (d #t) (k 0)))) (h "18yfa65piydkfbqn13dagfgfl0qip770rdwfh9afzgqqpilx6k2b") (f (quote (("default" "binaries") ("binaries"))))))

