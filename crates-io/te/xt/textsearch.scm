(define-module (crates-io te xt textsearch) #:use-module (crates-io))

(define-public crate-textsearch-0.0.2 (c (n "textsearch") (v "0.0.2") (d (list (d (n "uuid") (r "^0.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0w03kgl1dkan306sq6gd6mjhcvxsfjcx34vxkp7admllnjigyxnl")))

(define-public crate-textsearch-0.0.3 (c (n "textsearch") (v "0.0.3") (d (list (d (n "uuid") (r "^0.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1ljxf41ldk97rqqwb99n3sg8rniskm0j2bp14dzifsx54d7rlikg")))

(define-public crate-textsearch-0.1.0 (c (n "textsearch") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1w7x18d8hml84qzrk7mn2ia1ghawii9jd9lmlb783n78prddjjmh")))

(define-public crate-textsearch-0.1.1 (c (n "textsearch") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "01wgwcgzkzm7xkp0yl1pgh1mbb52p8dkxjxzfdjdkk07apkwdqji")))

(define-public crate-textsearch-0.2.0 (c (n "textsearch") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.9") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0skz1amszsk8askqgfd4kggan7b1p9mgk9i7xmf9hvpqssrqxpcf")))

