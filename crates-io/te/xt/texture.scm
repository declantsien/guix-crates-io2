(define-module (crates-io te xt texture) #:use-module (crates-io))

(define-public crate-texture-0.0.1 (c (n "texture") (v "0.0.1") (d (list (d (n "linenoise-rust") (r "^0.2.0") (d #t) (k 0)))) (h "08spvm8maah9h1gw0kcg5czzz85bmdjaa36mz00yqlqgy6z1ipir")))

(define-public crate-texture-0.0.2 (c (n "texture") (v "0.0.2") (d (list (d (n "linenoise-rust") (r "^0.2.0") (d #t) (k 0)))) (h "0lw590920wa4f9jz3ji3gb48918nrp6xkv5yrsy0a16p80iqikxw")))

(define-public crate-texture-0.0.3 (c (n "texture") (v "0.0.3") (d (list (d (n "linenoise-rust") (r "^0.2.0") (d #t) (k 0)))) (h "13f8ksn7pjfrlyn7gmycbzg8fs0cschr01r9cdg151qpqq52yl90")))

(define-public crate-texture-0.0.4 (c (n "texture") (v "0.0.4") (d (list (d (n "linenoise-rust") (r "^0.2.0") (d #t) (k 0)))) (h "00sb2srr02360r9aa48galnqpvx30qzsn86g2p9gi8lrfx2hm0fj")))

(define-public crate-texture-0.1.0 (c (n "texture") (v "0.1.0") (d (list (d (n "linenoise-rust") (r "^0.2.0") (d #t) (k 0)))) (h "1a6s69vlxcr7c1p3jg9m0sy9d2ifp67m40m5np19p6f1w4k42xx3")))

