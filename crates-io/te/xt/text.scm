(define-module (crates-io te xt text) #:use-module (crates-io))

(define-public crate-text-0.0.1 (c (n "text") (v "0.0.1") (h "10gcwik4kcvil6cp15j6ijqypcrdwy748fzqimg8fpkdylg53r0m")))

(define-public crate-text-0.0.2 (c (n "text") (v "0.0.2") (d (list (d (n "font") (r "^0.0") (d #t) (k 0)))) (h "1bx9ybxcig8iglrgx1sbvip8sbvss0wl9g9qcwhm63j1akczclc5")))

(define-public crate-text-0.0.3 (c (n "text") (v "0.0.3") (d (list (d (n "font") (r "^0.0") (d #t) (k 0)))) (h "01rjjmj1ydfilys4qgal8f60fcsxaiq5nhv265f1gdpfj2ghy1i9")))

(define-public crate-text-0.0.4 (c (n "text") (v "0.0.4") (d (list (d (n "font") (r "^0.3") (d #t) (k 0)))) (h "0hi2sg249h1c713c3h0006pzkl2zrvyvlfi6jris6c8257zj2wli")))

