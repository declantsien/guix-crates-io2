(define-module (crates-io te xt text-to-png-cli) #:use-module (crates-io))

(define-public crate-text-to-png-cli-0.1.0 (c (n "text-to-png-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "text-to-png") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04n1a8nxcjwfqkx3dnp6w36b33js51nayv9pjr6v88nsgxskpl6g")))

(define-public crate-text-to-png-cli-0.1.1 (c (n "text-to-png-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "text-to-png") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0a30x6f3al63fblhm17g8sb4msrzk6hhbn8sx2bvy0vbljx1scfm")))

(define-public crate-text-to-png-cli-0.2.0 (c (n "text-to-png-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "text-to-png") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "07284zj1x4gz2j6nskyidlx96vbyghfqpb91970nc19pzy5s9jlx")))

