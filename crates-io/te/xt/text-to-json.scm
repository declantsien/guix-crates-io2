(define-module (crates-io te xt text-to-json) #:use-module (crates-io))

(define-public crate-text-to-json-0.1.0 (c (n "text-to-json") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0w0xx6ds4h1h52nzd0kmc2j8s6czbswys9k0pbnm9x36qmj2fr2j")))

(define-public crate-text-to-json-0.1.1 (c (n "text-to-json") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1hs1n7mkhqp4s8l6na4s2kc2bjvrkypf8xmrg360y1q4dab46vwp")))

(define-public crate-text-to-json-0.1.2 (c (n "text-to-json") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "05jd2nii58zai34xg36k33m5l3hvna72qk3wa1zf0i82jncda8z4")))

(define-public crate-text-to-json-0.1.3 (c (n "text-to-json") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1lnw98cm9rj8i124zimf2bmyn3f5i4bjn6aqmszn9pqw1163ljlq")))

