(define-module (crates-io te xt textcat) #:use-module (crates-io))

(define-public crate-textcat-0.1.0 (c (n "textcat") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p00crvb349sjziwmyr95ld2yi1dp8xz75n1q1qax3lnls1crfdh")))

(define-public crate-textcat-0.1.1 (c (n "textcat") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14ai27sfipzgrf0iqp0sb02xrwrigkz152brm1zr84miskmq5nz2")))

(define-public crate-textcat-0.1.2 (c (n "textcat") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1x24gwzs01jpqlgwarid8lgyzx3r84nqmn2b2v9mq4vzw205qkgx")))

(define-public crate-textcat-0.1.3 (c (n "textcat") (v "0.1.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0956h1bdv0pkgfc6naimgxfdn683v8grla1dybhvgcsmmg3xyvs5")))

(define-public crate-textcat-0.1.4 (c (n "textcat") (v "0.1.4") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1zfw659s5sfq3yyz9gsv7z3mcd2k5wx5yzh8i8la1virn5zqba7c")))

(define-public crate-textcat-0.1.5 (c (n "textcat") (v "0.1.5") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "19ysckvrw1fmvp9jy1zvhhdy3bmvc99wf1l68ihrmrg56iiznsrw")))

(define-public crate-textcat-0.2.0 (c (n "textcat") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "07vx0ck6m02wcibpdk06plsxnslddkyg0jl13hhc2dp7sxmvk7d7")))

(define-public crate-textcat-0.3.0 (c (n "textcat") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1qg88fh19nxsgpig7ngfr1jjmv9yzpm5xyap069svrw6nfazywa7")))

(define-public crate-textcat-0.3.1 (c (n "textcat") (v "0.3.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1afzqqjx8vj2l11sbg58ddc2sqrax44miz8nsq4g1r3jc45d8x9i")))

(define-public crate-textcat-0.3.2 (c (n "textcat") (v "0.3.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0j6j8gnvhm058hj46cwygskfpn8chg1lia1pmsr9c4cxii3h2694")))

