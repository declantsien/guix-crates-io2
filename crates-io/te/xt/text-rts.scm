(define-module (crates-io te xt text-rts) #:use-module (crates-io))

(define-public crate-text-rts-0.1.0 (c (n "text-rts") (v "0.1.0") (d (list (d (n "bracket-lib") (r "^0.8") (f (quote ("threaded"))) (k 0)) (d (n "legion") (r "^0.3") (k 0)) (d (n "rand") (r "^0.7") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.23") (d #t) (k 0)))) (h "1v3sl2xfpsgwd53prq223x3c4j2h0gasw2rz25w0plrzmyxjv5as") (f (quote (("web" "bracket-lib/opengl") ("default" "bracket-lib/opengl" "legion/default") ("console" "bracket-lib/crossterm" "legion/default"))))))

