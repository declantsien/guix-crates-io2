(define-module (crates-io te xt textblocks) #:use-module (crates-io))

(define-public crate-textblocks-0.1.0 (c (n "textblocks") (v "0.1.0") (h "02kbk6fh9kp69gq7wp34dpv5pzwil127232whz7jsh2vdv8236vy")))

(define-public crate-textblocks-0.1.1 (c (n "textblocks") (v "0.1.1") (h "1k2vnpwvmgffm3qw2mpvlaczank2flcwfx05g25120m1zwagz8j7")))

