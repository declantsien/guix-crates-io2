(define-module (crates-io te xt textcanvas) #:use-module (crates-io))

(define-public crate-textcanvas-0.1.0 (c (n "textcanvas") (v "0.1.0") (h "1kq57ylirsf1djnn1z9xa0kjhxh8pqd7lyqy1m3x77myp5kzc7pz")))

(define-public crate-textcanvas-0.0.2 (c (n "textcanvas") (v "0.0.2") (h "0b5s6d38a6v0hsiifa0cgipm8hvk0qyvbf0psl480d4gkci9i6ij")))

(define-public crate-textcanvas-0.0.3 (c (n "textcanvas") (v "0.0.3") (h "1w13nnhwg85w7kdwz4p6zf4h6hrn0gjrssmv807kcc1wpa7p562k")))

(define-public crate-textcanvas-2.0.0 (c (n "textcanvas") (v "2.0.0") (h "1pifckjxm8jnc9mn5nfwlh625s0g55g1hlxnlrds7rw5gqg131x9")))

(define-public crate-textcanvas-2.1.0 (c (n "textcanvas") (v "2.1.0") (h "0j6sbpkjcbb54yc5qpschj5rp4l22g96iyl2lj3747akcrlxfxwa")))

