(define-module (crates-io te xt text-editor) #:use-module (crates-io))

(define-public crate-text-editor-0.1.0 (c (n "text-editor") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("std" "std_rng"))) (d #t) (k 2)) (d (n "small_vec2") (r "^0.1.1") (d #t) (k 0)))) (h "076hccdgjmd7486g0kyl80kxhb4qsyjhxfg8mf1zdipn1qpdv46c") (y #t)))

(define-public crate-text-editor-0.1.1 (c (n "text-editor") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("std" "std_rng"))) (d #t) (k 2)) (d (n "small_vec2") (r "^0.1.1") (d #t) (k 0)))) (h "1sb2rcf0m7ypxicdd49l2dwyj4dy23rdkgcx8kfa6ik0x8fkqsbl") (y #t)))

