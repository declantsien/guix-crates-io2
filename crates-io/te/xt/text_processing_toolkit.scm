(define-module (crates-io te xt text_processing_toolkit) #:use-module (crates-io))

(define-public crate-Text_Processing_Toolkit-0.1.0 (c (n "Text_Processing_Toolkit") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1s0ssb08la95iidgidxvabb6q16788y8lbjc2z7yp6s8di9g206w") (y #t)))

(define-public crate-Text_Processing_Toolkit-0.1.1 (c (n "Text_Processing_Toolkit") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "09n28nsn6q7f2h03dr9iblnmjj859ishq10jqbpdkyrid3i23b4i") (y #t)))

