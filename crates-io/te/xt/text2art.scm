(define-module (crates-io te xt text2art) #:use-module (crates-io))

(define-public crate-text2art-0.1.0 (c (n "text2art") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "17gw481ih3n0apb61sx8kqyb03l372qwzkw2rcmvri3lnk3dny3h") (y #t)))

(define-public crate-text2art-0.2.0 (c (n "text2art") (v "0.2.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0wfad47i7rp62451ks5zcfcyj392mc426wny0kadrrwqgack2jaw") (y #t)))

(define-public crate-text2art-0.3.0 (c (n "text2art") (v "0.3.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0fxpd44x7bqywsvnyr209pwq5mzvwm6v3hl90y41r2jamjky6lj8") (y #t)))

(define-public crate-text2art-1.0.0 (c (n "text2art") (v "1.0.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1zqsy7zvwgj948sm0s6np4adb11airirmc08flhbkz14p926rbbf")))

(define-public crate-text2art-1.0.1 (c (n "text2art") (v "1.0.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0ldkpz47yv9p5fx8px7bb2ny3hqdyg413ahnvqsw6mdy9qnhdny4")))

(define-public crate-text2art-1.0.2 (c (n "text2art") (v "1.0.2") (d (list (d (n "regex") (r "1.10.*") (d #t) (k 0)) (d (n "unicode-segmentation") (r "1.10.*") (d #t) (k 0)))) (h "0cb20mps227304mf8pphd26lkbv033114saq09p2ps5jc7n8rxmq")))

