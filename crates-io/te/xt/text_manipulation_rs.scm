(define-module (crates-io te xt text_manipulation_rs) #:use-module (crates-io))

(define-public crate-text_manipulation_rs-0.1.0 (c (n "text_manipulation_rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1amrhxgr8wvr3ww041rwrq0nm15yf2ms4g8wjm73bkvfccid99f1")))

(define-public crate-text_manipulation_rs-0.1.1 (c (n "text_manipulation_rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1h5j4gsipdi0db9vdjkbb304lmcgq11kgdsb6gj7s104lxfjmi4k")))

(define-public crate-text_manipulation_rs-0.1.2 (c (n "text_manipulation_rs") (v "0.1.2") (d (list (d (n "cargo-fuzz") (r "^0.11.2") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0pblqn65s2zbcrvnyv2vk7qhs6qralfn4sw0zmlcqkndspg6jrpk")))

(define-public crate-text_manipulation_rs-0.1.3 (c (n "text_manipulation_rs") (v "0.1.3") (d (list (d (n "cargo-fuzz") (r "^0.11.2") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1x195h8mfy3aq7rqk9xab205zy07vm13m69gl9m4x95wm8jj06qm")))

