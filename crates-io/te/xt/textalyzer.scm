(define-module (crates-io te xt textalyzer) #:use-module (crates-io))

(define-public crate-textalyzer-0.2.0 (c (n "textalyzer") (v "0.2.0") (d (list (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1bgra1c3q84n5z8hmb3z06vg6jlvy4bgqsr5phk13axq9xfizxvy")))

(define-public crate-textalyzer-0.2.1 (c (n "textalyzer") (v "0.2.1") (d (list (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1ddqwzj5rvhsss0giq3c99k3cakxp129dyj2qvvabqlq1xb11chf")))

