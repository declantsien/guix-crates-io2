(define-module (crates-io te xt texture2ddecoder) #:use-module (crates-io))

(define-public crate-texture2ddecoder-0.0.1 (c (n "texture2ddecoder") (v "0.0.1") (d (list (d (n "half") (r "^2.3.1") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ktx2") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0i9b7zz6r0h6136031wrbr9r4xgdir9hb76ibv6pcjjnimah10qx")))

(define-public crate-texture2ddecoder-0.0.2 (c (n "texture2ddecoder") (v "0.0.2") (d (list (d (n "ddsfile") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.3.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ktx2") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "19m2fpvfk36wpnvr62nqsdnyjg4x5lmyn6bxbnsm7cmcskvbnpxw") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-texture2ddecoder-0.0.3 (c (n "texture2ddecoder") (v "0.0.3") (d (list (d (n "ddsfile") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.3.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ktx2") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1wsz5qhznj8zxzj10a4mgf3d8ikv6gvs0gmw024dka4ac4saw82c") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-texture2ddecoder-0.0.4 (c (n "texture2ddecoder") (v "0.0.4") (d (list (d (n "ddsfile") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.3.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ktx2") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1nx09idrjadp0k4ncvafzdplbfi6z2zlmg36gq8ynjr1ws43ksdg") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-texture2ddecoder-0.0.5 (c (n "texture2ddecoder") (v "0.0.5") (d (list (d (n "ddsfile") (r "^0.5.1") (d #t) (k 2)) (d (n "half") (r "^2.3.1") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ktx2") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1n96dlaklzw4ip8dmqgz8bxjaz17h8byav9p1x6v9fffbp5fbw2l") (f (quote (("default" "alloc") ("alloc"))))))

