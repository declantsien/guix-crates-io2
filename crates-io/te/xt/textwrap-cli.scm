(define-module (crates-io te xt textwrap-cli) #:use-module (crates-io))

(define-public crate-textwrap-cli-0.1.0 (c (n "textwrap-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.15.0") (d #t) (k 0)))) (h "007vr18907cgxgbz1yns313ixbsf1xy02dqkx30zzal9yhxxnnam")))

(define-public crate-textwrap-cli-0.1.1 (c (n "textwrap-cli") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)))) (h "0yd14i46d2x2zmj566w5krs1dnf1djvs9ipf6sps5gccxd1q41xn")))

