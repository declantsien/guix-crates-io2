(define-module (crates-io te xt text_art_cn) #:use-module (crates-io))

(define-public crate-text_art_cn-0.3.0 (c (n "text_art_cn") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "0xd1k37a7vjm75pz43wlm106l9xrr4q5vkwanaang5wczxqmi3ax") (f (quote (("cli"))))))

(define-public crate-text_art_cn-0.3.1 (c (n "text_art_cn") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "0d90l3pi85a19dwxkjl0hbi1jjsvz6w1l5kmib8pbz1d4qai39xq") (f (quote (("cli"))))))

