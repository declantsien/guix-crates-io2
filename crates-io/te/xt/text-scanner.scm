(define-module (crates-io te xt text-scanner) #:use-module (crates-io))

(define-public crate-text-scanner-0.0.1 (c (n "text-scanner") (v "0.0.1") (d (list (d (n "char-ranges") (r "^0.1") (d #t) (k 0)))) (h "0zxwy6xxfs1r7ynfk52n6pms805crvd8lp1ghmpczhx61j2r42yb")))

(define-public crate-text-scanner-0.0.2 (c (n "text-scanner") (v "0.0.2") (d (list (d (n "char-ranges") (r "^0.1") (d #t) (k 0)))) (h "0mk9nrymr0g4py3v3qyfr5ihivrafshyy9hx18vsa522m9pq1ava") (f (quote (("ext") ("default" "ext"))))))

(define-public crate-text-scanner-0.0.3 (c (n "text-scanner") (v "0.0.3") (d (list (d (n "char-ranges") (r "^0.1") (d #t) (k 0)))) (h "0h63gv5fdzfirhpyzcd81yk0bzcsjgpy6ivq2i417d774mxvd003") (f (quote (("ext") ("default" "ext"))))))

