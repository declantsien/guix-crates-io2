(define-module (crates-io te xt text-diff) #:use-module (crates-io))

(define-public crate-text-diff-0.3.0 (c (n "text-diff") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.2.7") (d #t) (k 0)))) (h "0sx1psclhk5gb4il6mhlabzs4981nph0dwa4ff76b0wxijr8whaj")))

(define-public crate-text-diff-0.4.0 (c (n "text-diff") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.2.7") (d #t) (k 0)))) (h "15rqqhi7lklqpdq6gmdqzh9llabgj9xp4nq11ni13gzqcvfki4ih")))

