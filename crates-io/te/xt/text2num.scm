(define-module (crates-io te xt text2num) #:use-module (crates-io))

(define-public crate-text2num-1.7.0 (c (n "text2num") (v "1.7.0") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1w4lh9si38p8jm8spnx47in2nksjb40q0bqiggavm2inraw4j4kj")))

(define-public crate-text2num-2.0.0 (c (n "text2num") (v "2.0.0") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0cacz57xkqaadx6lij36bxicyv2raps9y70bqp3lhyack1prpq21")))

(define-public crate-text2num-2.0.1 (c (n "text2num") (v "2.0.1") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "078p00giy9vvfhk6nzhh423cg5p070k8x6wyrpj5lfn39pr502gl")))

(define-public crate-text2num-2.1.0 (c (n "text2num") (v "2.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0yw2cjdhzns7rlnbl9p39yai84map58jqi2m6bync4wk6cisryin")))

(define-public crate-text2num-2.1.1 (c (n "text2num") (v "2.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0rdqq81pbspadgisc82i5lh0q8fx1jal5p9r821wz1j2xpm9r4ir")))

(define-public crate-text2num-2.1.2 (c (n "text2num") (v "2.1.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0sl5ana9i3d5rlm1z9if428qk1qh9hxzi8zr4j3m8fzih57wvjjc")))

(define-public crate-text2num-2.1.3 (c (n "text2num") (v "2.1.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "10d37dgxhwbygqlqm3gw5vdygiy9czs8hl2gh3965wgv2dyxd99x")))

(define-public crate-text2num-2.1.4 (c (n "text2num") (v "2.1.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "1kv41q1ylm5wlhwya0j0qz30n3d4izds0zwrsc2dbah56q6ndn0j")))

