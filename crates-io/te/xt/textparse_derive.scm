(define-module (crates-io te xt textparse_derive) #:use-module (crates-io))

(define-public crate-textparse_derive-0.0.1 (c (n "textparse_derive") (v "0.0.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1hffjj3gr6y412fyjlg9bh0z027hmm2ccvhf8d852g613h0j2qk0")))

(define-public crate-textparse_derive-0.1.0 (c (n "textparse_derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0cqdyvyn7z6il8gaf450pfbwb849hvn9v3n7nsw59xsgwpbalrrw")))

