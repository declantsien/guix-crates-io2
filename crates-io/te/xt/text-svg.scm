(define-module (crates-io te xt text-svg) #:use-module (crates-io))

(define-public crate-text-svg-0.1.0 (c (n "text-svg") (v "0.1.0") (d (list (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "svg") (r "^0.12.1") (d #t) (k 0)))) (h "0m8dsrpab30jxjr7ym2559hp19pdbq690ig7zs6wjy00iwaymxv4")))

(define-public crate-text-svg-0.1.1 (c (n "text-svg") (v "0.1.1") (d (list (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "svg") (r "^0.12.1") (d #t) (k 0)))) (h "0s2mv7lfdj0fcx7jwyv1w7qizfw4wbaxjxvygvjw7bdvdmx1d6as")))

(define-public crate-text-svg-0.1.2 (c (n "text-svg") (v "0.1.2") (d (list (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "svg") (r "^0.12.1") (d #t) (k 0)))) (h "0k7bmpijjp3s7yx7f6wmxasg4nj8nrkmlcnmp4y1k8v8kiac51z9")))

