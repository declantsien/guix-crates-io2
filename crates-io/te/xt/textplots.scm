(define-module (crates-io te xt textplots) #:use-module (crates-io))

(define-public crate-textplots-0.1.0 (c (n "textplots") (v "0.1.0") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "0i9whk11cbh9fzqna99v6s2kffv96h0gc650l18wbdsycwzrqi2n")))

(define-public crate-textplots-0.1.1 (c (n "textplots") (v "0.1.1") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "186m0yrh7sbazb70vv91whgz6niavd53yd17541z4fhdj01h95fr")))

(define-public crate-textplots-0.1.2 (c (n "textplots") (v "0.1.2") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "0vp8g50wzbd02d608sar3brq25lkrrjvjmkz52zq3z97l5m9x3dj")))

(define-public crate-textplots-0.2.0 (c (n "textplots") (v "0.2.0") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "1ay0yyxbmvfw3f689m3djiiy7m6l1l7hnclv9q6gz6jgnzgskx5r")))

(define-public crate-textplots-0.2.1 (c (n "textplots") (v "0.2.1") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "1pw6637fgiwi66im4hxda470hbax8fwp5gw4iwsz4sl9v0df1hvn")))

(define-public crate-textplots-0.2.2 (c (n "textplots") (v "0.2.2") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "1ckmb4siklarhfjc5ymnpvgxkg9ncs3lc95cj7i106cxjw0bh3xa")))

(define-public crate-textplots-0.2.3 (c (n "textplots") (v "0.2.3") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "0zywpraqq2g145kfz3h0lz6vd8mncp0286943c0lhq1a935df69s")))

(define-public crate-textplots-0.3.0 (c (n "textplots") (v "0.3.0") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "17zav177i7lbs20q4cyhadsi8qhy4lzgcwklzz9n7ldjniw4kw51")))

(define-public crate-textplots-0.4.0 (c (n "textplots") (v "0.4.0") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "18wc02hlrb2v1y282mn39ac1vhdfili6cv7a6alsl01389zxyq32")))

(define-public crate-textplots-0.4.1 (c (n "textplots") (v "0.4.1") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "0l2j3ir9jk2mykpq63fv4iddi5w5krm2qmkq6125z67ly7xbywrr")))

(define-public crate-textplots-0.5.0 (c (n "textplots") (v "0.5.0") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "17drmhdmhlj445cd5nzv687lw8qcn5cdsvxrz84q8l0wx2cxj2fs")))

(define-public crate-textplots-0.5.1 (c (n "textplots") (v "0.5.1") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "09s1jzl47gch8n35s5lzswn8j8dpzdlsydnyzyw6km11pgwgxqcz")))

(define-public crate-textplots-0.5.2 (c (n "textplots") (v "0.5.2") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "07n947704saybf25kv9j08jwi929d9wgw91i3rvilxspkbg2mf0k")))

(define-public crate-textplots-0.5.3 (c (n "textplots") (v "0.5.3") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)))) (h "05svf8hm86kh8wrfmjwggnx8qx5c14rv4g3g4dvfdq0qnbx5gf2g")))

(define-public crate-textplots-0.5.4 (c (n "textplots") (v "0.5.4") (d (list (d (n "drawille") (r ">=0.2.3, <0.3.0") (d #t) (k 0)))) (h "0hai5nwkdx2ig5d6d0b8g1c7gnwjcxspl2yx4mvz1xl0zbl3b19n")))

(define-public crate-textplots-0.6.0 (c (n "textplots") (v "0.6.0") (d (list (d (n "drawille") (r "^0.2.3") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1fnw87l6zd8ynx271qzkm2sqkvammlmnr9b0rbq6hzxpzcl81j51")))

(define-public crate-textplots-0.7.0 (c (n "textplots") (v "0.7.0") (d (list (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "091bmw105sfk5d7c1y5s9z3zr22cbdnqwjcg9anp7z8kh95nv11g")))

(define-public crate-textplots-0.8.0 (c (n "textplots") (v "0.8.0") (d (list (d (n "console") (r "^0.14") (d #t) (k 2)) (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "16dpy4d40xqqj67396pdr9ypkzai8d6cxx9rdl7xadvykl792wf7")))

(define-public crate-textplots-0.8.1 (c (n "textplots") (v "0.8.1") (d (list (d (n "console") (r "^0.14") (d #t) (k 2)) (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1swicm31l9nvnfsw2iawrvc2540xj6yp4i6cgwcw4gaiixh4czmp")))

(define-public crate-textplots-0.8.2 (c (n "textplots") (v "0.8.2") (d (list (d (n "console") (r "^0.14") (d #t) (k 2)) (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "193yg8pq5dhs1sqf0nw7cz5aii00hh0xgnvr396mar04dmbkrl5y")))

(define-public crate-textplots-0.8.3 (c (n "textplots") (v "0.8.3") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 2)) (d (n "console") (r "^0.15.7") (d #t) (k 2)) (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "05049jcx3fqq6b4ydjd3n0ayjqbck84vylvg00x0qh8x3z0wvka6")))

(define-public crate-textplots-0.8.4 (c (n "textplots") (v "0.8.4") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 2)) (d (n "console") (r "^0.15.7") (d #t) (k 2)) (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "meval") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0yfwdkf876qpdkyrlg24d2kka0fp14ain2a2zrl80wqwsxmi2w91") (f (quote (("tool" "meval" "structopt"))))))

(define-public crate-textplots-0.8.5 (c (n "textplots") (v "0.8.5") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 2)) (d (n "console") (r "^0.15.7") (d #t) (k 2)) (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "meval") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1zcwyb22qldbgfjc2q21ch3jyd8b8cwyi62ql596nqk8nqhj73yq") (f (quote (("tool" "meval" "structopt"))))))

(define-public crate-textplots-0.8.6 (c (n "textplots") (v "0.8.6") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 2)) (d (n "console") (r "^0.15.7") (d #t) (k 2)) (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "meval") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1qvsccf19yybpcr02rhqivyjv01c98av6hi8z5igznqq660696zm") (f (quote (("tool" "meval" "structopt"))))))

