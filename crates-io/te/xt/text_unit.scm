(define-module (crates-io te xt text_unit) #:use-module (crates-io))

(define-public crate-text_unit-0.1.0 (c (n "text_unit") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0ivnadkjy7rwn6yvw1n13rqmary8wk1fmm87pr1dr0n2s7i6gw48")))

(define-public crate-text_unit-0.1.1 (c (n "text_unit") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0rr50v4rqaczi9fw1p5rlx77skrwsg4l3v1xl9yrr2rnfjg70s6c")))

(define-public crate-text_unit-0.1.2 (c (n "text_unit") (v "0.1.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0lkiq5n67xxyc6l33g92qmxpck7wlbriy8f23wdar6dc2mnd5k7g")))

(define-public crate-text_unit-0.1.3 (c (n "text_unit") (v "0.1.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "067d1kasln6w4kd2c404180kmvnmg0hzv6cw1zx16dzmxx2a93xw")))

(define-public crate-text_unit-0.1.4 (c (n "text_unit") (v "0.1.4") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1sgqm58qa28vp42yzbbycd9brij28cdw63lv6n6smffhcvd8dz4k")))

(define-public crate-text_unit-0.1.5 (c (n "text_unit") (v "0.1.5") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "13q46lhvh0jji7860gk5133py10rb8rgky4mnl4pwsl9pnyxf2c0")))

(define-public crate-text_unit-0.1.6 (c (n "text_unit") (v "0.1.6") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "17hqxgz2xy90lpiinc6hkz9x8h7ac7qdk2jsr6iiv2v35g1b32qm")))

(define-public crate-text_unit-0.1.7 (c (n "text_unit") (v "0.1.7") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0j52a9grx3mbj52mqik25s6a7g504mcv95hrxxil80nskvnssrwr") (y #t)))

(define-public crate-text_unit-0.1.8 (c (n "text_unit") (v "0.1.8") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1r5dyvjd4ypfl01y7wnqal2bcs88ii69c446jrrk1dlqqv25p7h1")))

(define-public crate-text_unit-0.1.9 (c (n "text_unit") (v "0.1.9") (d (list (d (n "deepsize") (r "^0.1") (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0y95lhkmw8gjdg9p47iixa6kvbdxafv0c4j34gms1gddlfvvr2z0")))

(define-public crate-text_unit-0.1.10 (c (n "text_unit") (v "0.1.10") (d (list (d (n "deepsize") (r "^0.1") (o #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1hmzkmdc2wvgrvizypy3x6h900p1j31xny15hx01mk7y9c81whr0")))

