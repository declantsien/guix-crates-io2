(define-module (crates-io te xt text-sanitizer) #:use-module (crates-io))

(define-public crate-text-sanitizer-1.4.2 (c (n "text-sanitizer") (v "1.4.2") (h "0j648nggybizdkl64gkw7i7c5f4p06ka2yr1420j22ank5lxj8qz")))

(define-public crate-text-sanitizer-1.4.3 (c (n "text-sanitizer") (v "1.4.3") (h "0mw85f157h1siyrknxxzjqfpp6pccfd8yv1acrn36d81c8v8kqyn")))

(define-public crate-text-sanitizer-1.4.4 (c (n "text-sanitizer") (v "1.4.4") (h "14ajkaslgp56862fdavaz89l64k4skm253izazc5x6qs57hnkjjd")))

(define-public crate-text-sanitizer-1.4.5 (c (n "text-sanitizer") (v "1.4.5") (h "0nl3di5vxy01h8qnwqv3vlbi7daf3dl9r33g7iax46ssfk22532z")))

(define-public crate-text-sanitizer-1.4.6 (c (n "text-sanitizer") (v "1.4.6") (h "0yqxc2pnf5cygzc86m7dlqb8nqrc68qgrb0vv9xw652whz80zkj6")))

(define-public crate-text-sanitizer-1.4.7 (c (n "text-sanitizer") (v "1.4.7") (h "15c9bzlvhzx8l0i5jkaj4hhf54z506plpl2njrlj3fvjcnmkb6fh")))

(define-public crate-text-sanitizer-1.5.0 (c (n "text-sanitizer") (v "1.5.0") (h "1a3gnaplr96yp5yjj0kwjhvfcy0i3pl949wy3gsry3kkm4pj21km")))

(define-public crate-text-sanitizer-1.5.1 (c (n "text-sanitizer") (v "1.5.1") (h "0hhnk4nqwnwmzg4v79sah82w443rbh9am2as9ghxz98qx8f162ad")))

(define-public crate-text-sanitizer-1.5.2 (c (n "text-sanitizer") (v "1.5.2") (h "1jg0avmj9zz6rd28dwqfqip7a8rv9zgyiiwxibkbcjqgyyvqxcpy")))

(define-public crate-text-sanitizer-1.5.3 (c (n "text-sanitizer") (v "1.5.3") (h "1jyrxm6dkwypj2ihhdq8yrdk1b8fd9mvav60mrg0vv1r51w7jwwz")))

(define-public crate-text-sanitizer-1.6.0 (c (n "text-sanitizer") (v "1.6.0") (d (list (d (n "serde") (r "=1.0.106") (d #t) (k 0)) (d (n "serde_derive") (r "=1.0.106") (d #t) (k 0)))) (h "0nrkpc6f34sjdgkiqxjq0qkfvp3i4mn8pa5w1k7yxbpizb4br4pf")))

