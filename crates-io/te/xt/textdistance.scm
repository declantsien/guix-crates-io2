(define-module (crates-io te xt textdistance) #:use-module (crates-io))

(define-public crate-textdistance-0.1.0 (c (n "textdistance") (v "0.1.0") (h "07nf5r8jmq57dbhb00svs5hqakgad3z99bbg6a5mk02q8f32qv0h") (y #t)))

(define-public crate-textdistance-0.1.1 (c (n "textdistance") (v "0.1.1") (h "1lwap9wg9rs3yvh1qq4jly5ckfaqq296d02hij2v5hcq1qyzapd2") (y #t)))

(define-public crate-textdistance-0.1.2 (c (n "textdistance") (v "0.1.2") (h "124p7a29szd21mwwjhjkmxq1nqzbawgc7xm1bk6ml9ikvcizyxdk") (y #t)))

(define-public crate-textdistance-0.1.3 (c (n "textdistance") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "0q0nd3pjgf4f7x5k0bdw1nbkciiyygpl75m86q7jcmv1408xny2k") (y #t)))

(define-public crate-textdistance-0.1.4 (c (n "textdistance") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2.2") (d #t) (k 2)))) (h "0sj3j79hpflm2nvnlj4six2hsy4nhmdz6acs1msm96xygz9hfbab") (y #t)))

(define-public crate-textdistance-0.1.5 (c (n "textdistance") (v "0.1.5") (h "1wayj7i0ma0wl3wq0vas3ah6a2hjjh025jy1qp728x9pdl0z67n6") (y #t)))

(define-public crate-textdistance-0.1.6 (c (n "textdistance") (v "0.1.6") (h "00084bg43mqh2px2p3s3fnzb2l5p30w5a5k6yz1fyfi6q1cg99vn") (y #t)))

(define-public crate-textdistance-1.0.0 (c (n "textdistance") (v "1.0.0") (d (list (d (n "assert2") (r "^0.3.10") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)))) (h "1mrvg171hvgdp0qkwqj04ms3rgmgq3sjd3rv55jfhqfdbzgicn1w")))

(define-public crate-textdistance-1.0.1 (c (n "textdistance") (v "1.0.1") (d (list (d (n "assert2") (r "^0.3.10") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)))) (h "09vg6j6ilzfwd9b2nwxggp3xl4cz3v360r9idgs0h0wrlb9dn99v")))

(define-public crate-textdistance-1.0.2 (c (n "textdistance") (v "1.0.2") (d (list (d (n "assert2") (r "^0.3.10") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 2)))) (h "0yq3lqn0hh2313z1irbc1apxck0dalvf5k79acwy8irbdibwh8fk")))

