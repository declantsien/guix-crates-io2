(define-module (crates-io te xt text-grid-macros) #:use-module (crates-io))

(define-public crate-text-grid-macros-0.4.1 (c (n "text-grid-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)))) (h "0q8h8i57gjxldi7qsc13chzp1cn0zg5mby84ljbbglrpyr7dbvnl")))

