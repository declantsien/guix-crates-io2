(define-module (crates-io te xt text_layout) #:use-module (crates-io))

(define-public crate-text_layout-0.1.0 (c (n "text_layout") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)))) (h "1zcka3637wf23rhj4qcccs0rz28v59hs75z897jrsfyvny7yplly") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("libm" "dep:libm"))))))

(define-public crate-text_layout-0.2.0 (c (n "text_layout") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "fixed") (r "^1.24.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)))) (h "0822h4y43pv3ff0cc3692rf1fj94kw4cwmw4yvsb82zx2gwmcpdb") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("libm" "dep:libm"))))))

(define-public crate-text_layout-0.3.0 (c (n "text_layout") (v "0.3.0") (d (list (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "fixed") (r "^1.24.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)))) (h "1xwh1l2wr8w4g9200982yxsr7ip3pki6dcab5fb41r3w30qzg771") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("libm" "dep:libm"))))))

