(define-module (crates-io te xt text_lines) #:use-module (crates-io))

(define-public crate-text_lines-0.1.0 (c (n "text_lines") (v "0.1.0") (h "10rr2kjk8ankr8q1wkrb214kwk83gjbvwdxvx0cyaz4j5w4fkviq")))

(define-public crate-text_lines-0.1.1 (c (n "text_lines") (v "0.1.1") (h "04f61xlzfwfnbb47q62dmg3va3wb1k4jw0lbyjii364qq582n2p5")))

(define-public crate-text_lines-0.1.2 (c (n "text_lines") (v "0.1.2") (h "13rhmayp05n5f4gf31iljmd35km5hmpywy8cybfgn9psv3n7jqhi")))

(define-public crate-text_lines-0.2.0 (c (n "text_lines") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1l6b362y4yzwf4bqbvpvzs300fn86v2g54xd9y0qvqd27kncd49l") (f (quote (("serialization" "serde"))))))

(define-public crate-text_lines-0.3.0 (c (n "text_lines") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z3v1js5vab768m21ngipym4asm6ir2wfj0pzh5k0qhiqk0lidzk") (f (quote (("serialization" "serde"))))))

(define-public crate-text_lines-0.4.0 (c (n "text_lines") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02h2fwcvnchybjdnysvzrmskwpqfym7j2z0wrv4ls3rqggwngc7n") (f (quote (("serialization" "serde"))))))

(define-public crate-text_lines-0.4.1 (c (n "text_lines") (v "0.4.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ygrr58n6gl027j6y21vsgvpmnznaymlzg4hhf78pph4vm9kr7p4") (f (quote (("serialization" "serde"))))))

(define-public crate-text_lines-0.5.0 (c (n "text_lines") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jhw3qmpyhsqa6ig60fc3i8imq81jl5hh1c3x4x45140rp7h139j") (f (quote (("serialization" "serde"))))))

(define-public crate-text_lines-0.6.0 (c (n "text_lines") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kwv0ln0gy7cczmpk6r7scrfwfvbx5m004yp3lp7ianywy6q5mbz") (f (quote (("serialization" "serde"))))))

