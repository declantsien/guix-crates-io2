(define-module (crates-io te xt text-image) #:use-module (crates-io))

(define-public crate-text-image-0.1.0 (c (n "text-image") (v "0.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "0k35swgg8n88fi3zp5bpvy1a04273lhdrfdlk1bcdk0i7gxsl0gy")))

(define-public crate-text-image-0.1.1 (c (n "text-image") (v "0.1.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "17k2ymm2qbxb1zdnilvldz70yscdzp6x6cijzbak4zbnc61s2fky")))

(define-public crate-text-image-0.1.2 (c (n "text-image") (v "0.1.2") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full" "parsing"))) (d #t) (k 0)))) (h "114rxasgn618wphi7isx0q9ml24pbj9ya7x42269m1qvsgr15mal")))

