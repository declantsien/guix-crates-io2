(define-module (crates-io te xt textcode) #:use-module (crates-io))

(define-public crate-textcode-0.1.0 (c (n "textcode") (v "0.1.0") (h "1984bs5s26c0ccd36211y7fcsgi3dwg1jp3nwinr9qa6f27nx36i")))

(define-public crate-textcode-0.1.1 (c (n "textcode") (v "0.1.1") (h "06zkw8ipvfsz3pv3yqjbv6mh6d50xpd8jx97mrgvlgnljl8rbl3l")))

(define-public crate-textcode-0.1.2 (c (n "textcode") (v "0.1.2") (h "1pfb0b4axh2xvv4fi3ndi60fjaynlm2df92ndnc8z5caibz89pm7")))

(define-public crate-textcode-0.2.0 (c (n "textcode") (v "0.2.0") (h "1jjbpd0vhkacc5p5nrw09i83pvbwyxri73blpwgzm6zwiblh1faj")))

(define-public crate-textcode-0.2.1 (c (n "textcode") (v "0.2.1") (h "1aa45qisag030hyrkf05b85hbgx5np43j081d9ph3siy5d1jvphk")))

(define-public crate-textcode-0.2.2 (c (n "textcode") (v "0.2.2") (h "1zbp0q59zkscdmq7ivz7r77x3nmhimiphv4k54hpp2qgfwa56g9q")))

