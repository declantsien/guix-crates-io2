(define-module (crates-io te xt textflow) #:use-module (crates-io))

(define-public crate-textflow-0.1.0 (c (n "textflow") (v "0.1.0") (d (list (d (n "textwrap") (r "^0.14") (d #t) (k 0)))) (h "02y6zjv6k5vr6d0r318rdkcn5l4k9yd02467y4qygg6d7kjvbx5v")))

(define-public crate-textflow-0.2.0 (c (n "textflow") (v "0.2.0") (d (list (d (n "lazy_static") (r "~1") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "textwrap") (r "~0.14") (d #t) (k 0)) (d (n "unicode-width") (r ">=0.1.9") (d #t) (k 0)))) (h "14r7m9vc3x9l4v5kk9k032g0zsfmhywg34bvm197i0bl7ck6kbmj")))

