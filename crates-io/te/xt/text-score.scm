(define-module (crates-io te xt text-score) #:use-module (crates-io))

(define-public crate-text-score-0.1.0 (c (n "text-score") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 0)))) (h "1s2shgni0da6m5w8czdc6waydn9zj5a68z1hgrq0n0342y3nydni")))

