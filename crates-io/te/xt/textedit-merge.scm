(define-module (crates-io te xt textedit-merge) #:use-module (crates-io))

(define-public crate-textedit-merge-0.1.0 (c (n "textedit-merge") (v "0.1.0") (h "102z338jgfnya5051rpl2800q8psr453ik3namh9632lvgqhkxji") (y #t)))

(define-public crate-textedit-merge-0.2.0 (c (n "textedit-merge") (v "0.2.0") (h "0naw8z9185sgxcypdzgiw6k5kblw1kbbbhriavprvyr4h0icad93")))

(define-public crate-textedit-merge-0.2.1 (c (n "textedit-merge") (v "0.2.1") (h "00zwybq2yxk50f9rqs0a3gi18mswsw2yicm4z3galfji7inmdfz9")))

