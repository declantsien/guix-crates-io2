(define-module (crates-io te xt texting) #:use-module (crates-io))

(define-public crate-texting-0.0.1 (c (n "texting") (v "0.0.1") (d (list (d (n "veho") (r ">=0.0.5") (d #t) (k 2)))) (h "048aygr7h0mn9nsvzp167j7hhj7yfbcfpripgf59k4znqszgnqmc")))

(define-public crate-texting-0.0.2 (c (n "texting") (v "0.0.2") (d (list (d (n "veho") (r ">=0.0.15") (d #t) (k 2)))) (h "1cf48gagys11dqwnwszzgcwqjb8bpy99fh7m5jp9rrmrvyz8qan6")))

(define-public crate-texting-0.0.3 (c (n "texting") (v "0.0.3") (d (list (d (n "regex") (r ">=1.5") (d #t) (k 0)) (d (n "veho") (r ">=0.0.15") (d #t) (k 2)))) (h "1ij97yh1ywj6qmkg90qvcrgwbjq59n3rr2cp0zjmpbjp564dfgci")))

(define-public crate-texting-0.0.4 (c (n "texting") (v "0.0.4") (d (list (d (n "lazy_static") (r ">=1.4.0") (d #t) (k 0)) (d (n "regex") (r ">=1.5") (d #t) (k 0)) (d (n "veho") (r ">=0.0.15") (d #t) (k 2)))) (h "0d4xfnb5n8sl92y638hp5m4zn9wz2wl9gmhlhgyg30rmspgim1fj")))

(define-public crate-texting-0.0.5 (c (n "texting") (v "0.0.5") (d (list (d (n "lazy_static") (r ">=1.4.0") (d #t) (k 0)) (d (n "regex") (r ">=1.5") (d #t) (k 0)) (d (n "veho") (r ">=0.0.15") (d #t) (k 2)) (d (n "vte") (r ">=0.10.0") (d #t) (k 0)))) (h "11g3vs1kmrxg3xx2j8bdmz0w25wmbqahw71hli2g4g562biq4vib")))

(define-public crate-texting-0.0.6 (c (n "texting") (v "0.0.6") (d (list (d (n "lazy_static") (r ">=1.4.0") (d #t) (k 0)) (d (n "regex") (r ">=1.5") (d #t) (k 0)) (d (n "veho") (r ">=0.0.19") (d #t) (k 2)) (d (n "vte") (r ">=0.10.0") (d #t) (k 0)))) (h "1r4lyvsi9sx6216mbph43ndssln7gyh8fzg4ckp74gpchn1i19ck")))

(define-public crate-texting-0.0.7 (c (n "texting") (v "0.0.7") (d (list (d (n "lazy_static") (r ">=1.4.0") (d #t) (k 0)) (d (n "regex") (r ">=1.5") (d #t) (k 0)) (d (n "veho") (r ">=0.0.20") (d #t) (k 2)) (d (n "vte") (r ">=0.10.0") (d #t) (k 0)))) (h "15b17k1jc49vrdqajj90wqcmg33d2s64xpz7yarv0lygvrhjvq8v")))

