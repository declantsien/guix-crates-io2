(define-module (crates-io te xt text2img) #:use-module (crates-io))

(define-public crate-text2img-0.8.0 (c (n "text2img") (v "0.8.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "raqote") (r "^0.8.0") (d #t) (k 0)))) (h "1mqf7d0c9q7lcar5rxs71jyhr8pl0r590ykgirxspwcg5i3pf8wf")))

(define-public crate-text2img-0.8.1 (c (n "text2img") (v "0.8.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "raqote") (r "^0.8.0") (d #t) (k 0)))) (h "04wi7k5dzm13bxkdhkvkwgpz8y15098yp6krifn180rwpv49g3n1")))

(define-public crate-text2img-0.8.3 (c (n "text2img") (v "0.8.3") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)) (d (n "raqote") (r "^0.8.0") (d #t) (k 0)))) (h "0i140dg9hygs5llibh92hwgbqpdxmzli9v6czl33fqq9w4qvx607")))

