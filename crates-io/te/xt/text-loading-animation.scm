(define-module (crates-io te xt text-loading-animation) #:use-module (crates-io))

(define-public crate-text-loading-animation-1.0.0 (c (n "text-loading-animation") (v "1.0.0") (h "0acvg5zr9nm9q1qzyiym9gp4ncb1salmbg3yzc3ngrbjr6lp5qib")))

(define-public crate-text-loading-animation-1.0.1 (c (n "text-loading-animation") (v "1.0.1") (h "06hvn84mca0jadp6c9kbz5l0aad9a9rwv55r9rx2hb8svyri77gg")))

