(define-module (crates-io te xt text-block-macros) #:use-module (crates-io))

(define-public crate-text-block-macros-0.0.0 (c (n "text-block-macros") (v "0.0.0") (h "0biwpnazbq0g6b7q13cfjwdjs055h84j7n8ii6pvniz7pr2pn7kv")))

(define-public crate-text-block-macros-0.1.0 (c (n "text-block-macros") (v "0.1.0") (h "0w4r542zxj2yg5dpg98y9a20n5hqgwmk3rxxxyhgf2xmsxmd4a9p")))

(define-public crate-text-block-macros-0.1.1 (c (n "text-block-macros") (v "0.1.1") (h "0ja0p1vz33sakzyxb1n9j6nb9n59g40hzs0xmzg1f5qwvas5k2vz")))

