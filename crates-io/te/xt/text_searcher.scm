(define-module (crates-io te xt text_searcher) #:use-module (crates-io))

(define-public crate-text_searcher-0.1.0 (c (n "text_searcher") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nsl9s7jmmv6np4bxx75ifl56y9sz9yc96kvnji43pfdvxbkczjw")))

(define-public crate-text_searcher-0.1.1 (c (n "text_searcher") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sp62kqfmyk23si4z9gf743axirmcib4r5p04dasq1r1mpr7hgg4")))

(define-public crate-text_searcher-0.1.4 (c (n "text_searcher") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yvd0rl419zyhzl54hw96iwnd9b5izqiz53b71idhl7m2i9rabnw")))

(define-public crate-text_searcher-0.1.5 (c (n "text_searcher") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "089vdb2dzbblpfs6nkh6sj2ma0ddcz2m1xl6znv3rkd4g6pkwi7d")))

(define-public crate-text_searcher-0.1.6 (c (n "text_searcher") (v "0.1.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15kmpb60f653hp32gl9mkxgpv9vsgr5gb6m7182ib9pi5pisqym5")))

(define-public crate-text_searcher-0.1.7 (c (n "text_searcher") (v "0.1.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0by4xdh3k2d1ymc6g322kwm0z7lm9mcgr5rdrqwkalxsg0xpsff2")))

