(define-module (crates-io te xt text-document) #:use-module (crates-io))

(define-public crate-text-document-0.0.1 (c (n "text-document") (v "0.0.1") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1mp9l43k21sahy6xy8d5nqs6lhjrg4j97pjn5792wg2nnrdrm69d")))

(define-public crate-text-document-0.0.2 (c (n "text-document") (v "0.0.2") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0l877y1kv5zr8qj3r22q6jb9q479ly639jrbsr74snjc44xpyy6a")))

(define-public crate-text-document-0.0.3 (c (n "text-document") (v "0.0.3") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0z00f71kbkkq0yvnk19arcbn70znhykbpsqxh91zvd5vy8lcxrxh")))

(define-public crate-text-document-0.0.4 (c (n "text-document") (v "0.0.4") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0n7cpwcvb0v9dzjvb311hzx8770p94p7q49lqiw298ql05wjz6aw")))

(define-public crate-text-document-0.0.5 (c (n "text-document") (v "0.0.5") (d (list (d (n "array_tool") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "uuid") (r "^1.1.1") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "02i6bw2z2wmg57cv3n6vjql6kadsrablrqdpf6hb8751c0q534ap")))

