(define-module (crates-io te xt text_writer) #:use-module (crates-io))

(define-public crate-text_writer-0.1.0 (c (n "text_writer") (v "0.1.0") (h "134jdp79b0l0hyy6s8q13wmy6hvaxdfam7j7irxlilbcgwr524jq")))

(define-public crate-text_writer-0.1.1 (c (n "text_writer") (v "0.1.1") (h "1qpam89mdlcr6rbvvd0xrjjkvfc1qnrx4skq4an3qxbjlc8ykw7p")))

(define-public crate-text_writer-0.1.2 (c (n "text_writer") (v "0.1.2") (h "0ipb9y4j0k85p9wj4g9g1lrza8rkb5zgnzg1396wgwanz633pgjn")))

(define-public crate-text_writer-0.1.3 (c (n "text_writer") (v "0.1.3") (h "0hkayca8gmf0h94pvwvrxs8b74m6sxvl4y2karsc62f26c86dr2b")))

(define-public crate-text_writer-0.1.4 (c (n "text_writer") (v "0.1.4") (h "057smhfik00zsp4wahrffyszgvgyfvbqalfjyladl8wzdb48w7s3")))

(define-public crate-text_writer-0.1.5 (c (n "text_writer") (v "0.1.5") (h "14vivipxdzhb9qi6c57kbvrz6wzml354sn55xpcii5rpb3i0rxrx")))

(define-public crate-text_writer-0.1.6 (c (n "text_writer") (v "0.1.6") (h "08xchi51wzv7c67rcs0slgkhndgb9kgjq0avxnb0l1wzwpkw9bw4")))

(define-public crate-text_writer-0.1.7 (c (n "text_writer") (v "0.1.7") (h "1ywn228br9js84qcx93489pcrsvh0hr0526pwwwfadacqnvspx0y")))

(define-public crate-text_writer-0.1.8 (c (n "text_writer") (v "0.1.8") (h "0wvc8lrnxxc3j8pbj7qsgvsyidmryh0dpfscs867h31r3x4z22l7")))

(define-public crate-text_writer-0.1.9 (c (n "text_writer") (v "0.1.9") (h "0aa98qa7i1dn93x5fnxr7s3ll1jb4bk4fi9l9m15x317s1bdf94j")))

(define-public crate-text_writer-0.1.10 (c (n "text_writer") (v "0.1.10") (h "0nmfxinwjj96mzxxcva6byx2lj4qnss0p18gy06xv1415m7w61g1")))

(define-public crate-text_writer-0.1.11 (c (n "text_writer") (v "0.1.11") (h "0dk03m42ngxz0vwzmg2rxvrl1dk79aw369ppvvqdaa0hxljm06zb")))

