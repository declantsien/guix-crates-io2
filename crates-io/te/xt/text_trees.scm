(define-module (crates-io te xt text_trees) #:use-module (crates-io))

(define-public crate-text_trees-0.1.0 (c (n "text_trees") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0wqpsf33z6mdcwb02csd9il0gb245awnfy4nvq6fnqyj0iq0y4j1")))

(define-public crate-text_trees-0.1.1 (c (n "text_trees") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1m0a9ag6b1c5shq1lpvyajadv8fz676wx687arf8c71zs0dhb6zk")))

(define-public crate-text_trees-0.1.2 (c (n "text_trees") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1kyh0vf885cwchhvxbdxz103k4vp6svvq94lbsfcbzq75ymsg7j1")))

