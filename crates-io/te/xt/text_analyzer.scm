(define-module (crates-io te xt text_analyzer) #:use-module (crates-io))

(define-public crate-text_analyzer-0.1.0 (c (n "text_analyzer") (v "0.1.0") (h "063lkq5gdh9y49b7sccdpd23iday76wvr3k0kqrrj2a72rb7bddj")))

(define-public crate-text_analyzer-0.1.1 (c (n "text_analyzer") (v "0.1.1") (h "05n10121kijyng3c0k054mnsh7csvgqx6i4mcycdy48z4lr7gn4p")))

