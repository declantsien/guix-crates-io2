(define-module (crates-io te xt text-buffer) #:use-module (crates-io))

(define-public crate-text-buffer-0.1.0 (c (n "text-buffer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "get-size") (r "^0.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.0") (f (quote ("union"))) (d #t) (k 0)) (d (n "str_indices") (r "^0.4.3") (d #t) (k 0)))) (h "09xx38wvc3gv0fx9s9yi8fm6walpwvcp7nvl9p05zd47jhd340dq")))

