(define-module (crates-io te xt text_on_image) #:use-module (crates-io))

(define-public crate-text_on_image-0.1.0 (c (n "text_on_image") (v "0.1.0") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1p151zdvf8bnphnlwcg2n9x8api7gkw3l97c0sfzb6pyx5m3zl9b")))

