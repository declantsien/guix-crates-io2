(define-module (crates-io te xt text-to-polly-ssml) #:use-module (crates-io))

(define-public crate-text-to-polly-ssml-0.1.0 (c (n "text-to-polly-ssml") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.7.0") (d #t) (k 0)))) (h "1w4c2cfhbmkiknc0k9wv4p452xxkf0kajzjgdds3w53gcgpwh52p")))

(define-public crate-text-to-polly-ssml-0.2.0 (c (n "text-to-polly-ssml") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^2.2.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.7.0") (d #t) (k 0)))) (h "0zmxx5rj9f4ds7pfws148c1gb4sx79ys2nqkvhmbb0wd4blv78d5")))

(define-public crate-text-to-polly-ssml-0.3.0 (c (n "text-to-polly-ssml") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.7.3") (d #t) (k 0)))) (h "17ihndy6fk524kdly3g48l069n4rv83h2dnvdy4ilbiq8iaiwh8q")))

(define-public crate-text-to-polly-ssml-0.3.1 (c (n "text-to-polly-ssml") (v "0.3.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.2") (d #t) (k 0)))) (h "0rcha58in3sm9r23ghslm27rgy9gzcfxyhamcv009dyb1whbs3nz")))

(define-public crate-text-to-polly-ssml-0.3.2 (c (n "text-to-polly-ssml") (v "0.3.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "nom") (r "^3.0.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.9.2") (d #t) (k 0)))) (h "13j7r5wbarq8mbrya8cvcsaixna7gwxlq31d3hfmqbbmblx7fyrw")))

(define-public crate-text-to-polly-ssml-0.3.3 (c (n "text-to-polly-ssml") (v "0.3.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "quick-xml") (r "^0.13") (d #t) (k 0)))) (h "1n1a6hwkd19739nfj608g7ki0v91bbl7qsdzb806aqxz19z2wbgn")))

(define-public crate-text-to-polly-ssml-0.4.0 (c (n "text-to-polly-ssml") (v "0.4.0") (d (list (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)))) (h "03c4xqf3wgzxf8n8vjhnfkwwif2q4avr09h9ggll0wdpyap3axnd")))

