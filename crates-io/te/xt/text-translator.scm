(define-module (crates-io te xt text-translator) #:use-module (crates-io))

(define-public crate-text-translator-0.1.0 (c (n "text-translator") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.20") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0") (d #t) (k 0)))) (h "0sckfi52jn6037szbsxi6a1vgg68brb4b7gwnxfyk068061jgqbw")))

