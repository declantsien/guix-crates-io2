(define-module (crates-io te xt text-editing) #:use-module (crates-io))

(define-public crate-text-editing-0.1.0 (c (n "text-editing") (v "0.1.0") (h "03rkwcqm5n7pc8kzyc9867jgf8m70gbp0llf7jgr9vc92vfdqj6m")))

(define-public crate-text-editing-0.1.1 (c (n "text-editing") (v "0.1.1") (h "0minilm5r6pxpbk5n0xc3nwwzpwrc4l0xfkm7x1y0zmk8cfhzscv") (y #t)))

(define-public crate-text-editing-0.1.3 (c (n "text-editing") (v "0.1.3") (h "0wclpr8b9qq7mgqq9ggni1m9bg1wlhjqnqmkyccpx03jzamwbqaw")))

