(define-module (crates-io te xt text-svg-plus) #:use-module (crates-io))

(define-public crate-text-svg-plus-0.1.3 (c (n "text-svg-plus") (v "0.1.3") (d (list (d (n "font-kit") (r "^0.12") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)) (d (n "svg") (r "^0.14") (d #t) (k 0)))) (h "1m2lsbjrcc9j9n495mdpmc40dnh41kxx6k62459b4bmrsb6j8wk1")))

