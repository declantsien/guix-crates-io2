(define-module (crates-io te xt text_styled) #:use-module (crates-io))

(define-public crate-text_styled-0.1.0 (c (n "text_styled") (v "0.1.0") (h "0hbs23k17442y1dmqj0azdbj2qzf82dwihllcddwmbak0m3h5j14")))

(define-public crate-text_styled-0.1.1 (c (n "text_styled") (v "0.1.1") (h "0k09brivz3nwil8k4vvy5l96vvqiqscq08xnwnpvpvj4zmsxf49m")))

