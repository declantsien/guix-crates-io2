(define-module (crates-io te xt texture-atlasser) #:use-module (crates-io))

(define-public crate-texture-atlasser-0.1.0 (c (n "texture-atlasser") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rectangle-pack") (r "^0.3.0") (d #t) (k 0)))) (h "1wr62msvpl1xhkd8mgxc0dzz0xx12dp0ysvd6y3gc3hbslypn0pj")))

(define-public crate-texture-atlasser-0.1.1 (c (n "texture-atlasser") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rectangle-pack") (r "^0.3.0") (d #t) (k 0)))) (h "046mgfikjf5ldgafrqvam7nqnwmy1l00sqr6gsilyafrzm4f6627")))

