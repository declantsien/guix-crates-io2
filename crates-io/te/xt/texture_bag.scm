(define-module (crates-io te xt texture_bag) #:use-module (crates-io))

(define-public crate-texture_bag-0.0.1 (c (n "texture_bag") (v "0.0.1") (d (list (d (n "glium") (r "^0.29.0") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0xnf5bqkxm94wbzrdkn8r83q5j3319bm88f2zlwy1asl6px69lbk")))

(define-public crate-texture_bag-0.0.2 (c (n "texture_bag") (v "0.0.2") (d (list (d (n "glium") (r "^0.29.0") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1b478hdl7fni5g3s0hr5z0r7xl46x5sv2j1c202p30blh01l07l7")))

(define-public crate-texture_bag-0.0.3 (c (n "texture_bag") (v "0.0.3") (d (list (d (n "glium") (r "^0.29.0") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1ml06ifbqnlrq2l6vh8gd96cgi1f6y8iwxhyk9h3fifv8xk6krnf")))

(define-public crate-texture_bag-0.0.4 (c (n "texture_bag") (v "0.0.4") (d (list (d (n "glium") (r ">= 0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "1j7rcgldaqayfiim0kjk7z9720826ls80wf3wba5l49j7pzk6ca8")))

