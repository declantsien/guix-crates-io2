(define-module (crates-io te xt text-to-ascii-art) #:use-module (crates-io))

(define-public crate-text-to-ascii-art-0.1.0 (c (n "text-to-ascii-art") (v "0.1.0") (h "04ayqxfkv8fzjyc40z8ixgaih74asqzxj5p8fz5fx2h7w3dfwzfi") (y #t)))

(define-public crate-text-to-ascii-art-0.1.1 (c (n "text-to-ascii-art") (v "0.1.1") (h "1kizyqx1fy9733nvd18vyq66nxdl5v6rm2hzny9g53cmj5k31win") (y #t)))

(define-public crate-text-to-ascii-art-0.1.2 (c (n "text-to-ascii-art") (v "0.1.2") (h "1pqvw118wfkyip7f3fcv8yp5h488zp12a4x9hay8zl1ajkljjpn0") (y #t)))

(define-public crate-text-to-ascii-art-0.1.3 (c (n "text-to-ascii-art") (v "0.1.3") (h "045bqh8anvvpg9gz54pmjmshzs558zmz4ad1jsbcl4d6402zmkm7") (y #t)))

(define-public crate-text-to-ascii-art-0.1.4 (c (n "text-to-ascii-art") (v "0.1.4") (h "08cwbgi3vs5g6cm07c70dplv0f4cjcc663brr515gwar9if21sf8")))

(define-public crate-text-to-ascii-art-0.1.5 (c (n "text-to-ascii-art") (v "0.1.5") (h "04haww38jg2f6y7mv34182k0iwzlc8a6p4g6qvg2jprb1x8aqivj")))

(define-public crate-text-to-ascii-art-0.1.6 (c (n "text-to-ascii-art") (v "0.1.6") (h "11zsphw242rs41aa1c3m7f7s190q9jrr982pz51bl29d7sm19cb9")))

(define-public crate-text-to-ascii-art-0.1.7 (c (n "text-to-ascii-art") (v "0.1.7") (h "03a0pw669hiyjyarsrr31cvanri3bg6z9idsh8gzzh7bz80dx1b9")))

(define-public crate-text-to-ascii-art-0.1.8 (c (n "text-to-ascii-art") (v "0.1.8") (h "0sj0x5932rhz6q7nq9956i306n3iika93csy0n3n93yf35ys51lm")))

(define-public crate-text-to-ascii-art-0.1.9 (c (n "text-to-ascii-art") (v "0.1.9") (h "0xkr006whqbjrphpmp5mgyq9qfav67kq0ai6bw3qwrmfrdy5xy7z")))

