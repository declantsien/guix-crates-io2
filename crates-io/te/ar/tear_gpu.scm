(define-module (crates-io te ar tear_gpu) #:use-module (crates-io))

(define-public crate-tear_gpu-0.1.0 (c (n "tear_gpu") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "pixels") (r "^0.13.0") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.14") (d #t) (k 0)))) (h "0hmq9yk82gp3r1s5nkixg2dxfdszpg8z1649hqx90x1rigdkv7pf") (y #t)))

