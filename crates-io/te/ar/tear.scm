(define-module (crates-io te ar tear) #:use-module (crates-io))

(define-public crate-tear-0.1.0 (c (n "tear") (v "0.1.0") (h "12cm0cabx4plinqiplssmdc61p6j8j6nslnlgn3g08km5mpic4d5")))

(define-public crate-tear-0.1.1 (c (n "tear") (v "0.1.1") (h "0y64a8874c0fs00ycj8ja8qkc2fp99rd45x8mx4c79xs932hv8jw")))

(define-public crate-tear-0.2.0 (c (n "tear") (v "0.2.0") (d (list (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1msrsj54yd3ppy6khzd53gay4f6vyznvida3kjj2n0bfzl6vapds") (f (quote (("ignore-ui") ("experimental") ("combinators" "either"))))))

(define-public crate-tear-0.3.0 (c (n "tear") (v "0.3.0") (d (list (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0dfqlb8p79pw9v2q881k1rp5w9xgg0grmprqh3ma7w3bhp9w46wz") (f (quote (("ignore-ui") ("experimental") ("combinators" "either"))))))

(define-public crate-tear-0.4.0 (c (n "tear") (v "0.4.0") (d (list (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "054f6id2g6l6v1l1y3k6hgxxcngpvz5db27d4dikyvq7lnyqvm9p") (f (quote (("ignore-ui") ("experimental") ("combinators" "either"))))))

(define-public crate-tear-0.5.0 (c (n "tear") (v "0.5.0") (d (list (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1yc2nk6a9w46699xjwlnjs1cw60fns4r26aliqc2c62hbv6ib5z6") (f (quote (("ignore-ui") ("experimental") ("combinators" "either"))))))

(define-public crate-tear-0.5.1 (c (n "tear") (v "0.5.1") (d (list (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0mhnp2qhap6m5djf9cz8ksy51a6vipl068d2ycl9274h2nsb6liz") (f (quote (("ignore-ui") ("experimental") ("combinators" "either"))))))

