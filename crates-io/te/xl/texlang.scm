(define-module (crates-io te xl texlang) #:use-module (crates-io))

(define-public crate-texlang-0.1.0 (c (n "texlang") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "texcraft-stdext") (r "^0.1.0") (d #t) (k 0)))) (h "1rh7hg5jkwwrmk9n4x2n2k3mncgc0z1dnq5iwmqr9cc6v8xmvbkx") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "texcraft-stdext/serde"))))))

