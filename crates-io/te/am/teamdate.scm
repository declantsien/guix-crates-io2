(define-module (crates-io te am teamdate) #:use-module (crates-io))

(define-public crate-teamdate-0.1.0 (c (n "teamdate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wyz") (r "^0.5") (d #t) (k 0)))) (h "0b302in3xr4708c7dlhlrz7agz3bcgspv7y72wvl4hm1izhn5frf")))

