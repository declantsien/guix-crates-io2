(define-module (crates-io te ja tejar) #:use-module (crates-io))

(define-public crate-tejar-0.1.0 (c (n "tejar") (v "0.1.0") (d (list (d (n "camino") (r "^1.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0nps07xdis5zlq72y8v95dpdkm0ffarj1sw461z6igw9l1b2hry1")))

(define-public crate-tejar-0.1.1 (c (n "tejar") (v "0.1.1") (d (list (d (n "camino") (r "^1.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0p8rjvr7qmd7017y542vas94gy7xn4fbq3k2plpvsgy4l924qrar")))

(define-public crate-tejar-0.1.2 (c (n "tejar") (v "0.1.2") (d (list (d (n "camino") (r "^1.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0dfqjmmrx29w2j8h40ifqhqlg6gq28lqz271c0r0bdbiyvlwaqqn")))

(define-public crate-tejar-0.1.3 (c (n "tejar") (v "0.1.3") (d (list (d (n "camino") (r "^1.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0g54h7nqa616j5ms5v2cgpsqhs434d5jkx15fhsqwy2cj3iwz9k0")))

(define-public crate-tejar-0.1.4 (c (n "tejar") (v "0.1.4") (d (list (d (n "camino") (r "^1.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "08h0w7bq9aifkmys4il4mis78qhzdfbl55vhag33brfmh1f8ld39")))

