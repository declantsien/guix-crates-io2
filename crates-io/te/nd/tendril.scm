(define-module (crates-io te nd tendril) #:use-module (crates-io))

(define-public crate-tendril-0.1.0 (c (n "tendril") (v "0.1.0") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "001x0cq2m9d7a06y5s83pb0ga6xixshj2jjb3qrn4gjdgbgawp28")))

(define-public crate-tendril-0.1.1 (c (n "tendril") (v "0.1.1") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "14x0myb85mlahw405kf2b2mgj3b24x5fzqzjdlqrfjpjv0hg7c89")))

(define-public crate-tendril-0.1.2 (c (n "tendril") (v "0.1.2") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "1hv38jr330d5ycycch6gcnbxhf3yjf1wbiyir9x6c3rx9p7kczpl")))

(define-public crate-tendril-0.1.3 (c (n "tendril") (v "0.1.3") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "0phl4pqpkhlfayv66i6rxi65zlhcd5iiinnmiwj74x891iikzywv") (f (quote (("unstable"))))))

(define-public crate-tendril-0.1.4 (c (n "tendril") (v "0.1.4") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "1amqac7cs0ll6ar1i29bn9c7b6kkdra1g3n87b3zzva6s0yqxxjm") (f (quote (("unstable"))))))

(define-public crate-tendril-0.1.5 (c (n "tendril") (v "0.1.5") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "1ijvg2hpcj5syfwflbykcvzl6pgid9v5iyw0lky0f3s5q6z1b5qs") (f (quote (("unstable"))))))

(define-public crate-tendril-0.1.6 (c (n "tendril") (v "0.1.6") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "0rb8haxg0q439q8d2sqhxhjqcmqbg1zzs65zla8aj3swqxfjvnl7") (f (quote (("unstable"))))))

(define-public crate-tendril-0.2.0 (c (n "tendril") (v "0.2.0") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "utf-8") (r "^0.6") (d #t) (k 0)))) (h "1ban5bkhpl80s7gdx8z3w1im8lc1grd4hniknbhq9nwa5pg61nba") (f (quote (("unstable"))))))

(define-public crate-tendril-0.2.1 (c (n "tendril") (v "0.2.1") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "utf-8") (r "^0.6") (d #t) (k 0)))) (h "04y807qcg4yjiirg96zc6g22dahn37sgjnjbrwfca4j5249fvjnc") (f (quote (("unstable"))))))

(define-public crate-tendril-0.2.2 (c (n "tendril") (v "0.2.2") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "utf-8") (r "^0.6") (d #t) (k 0)))) (h "1fw9w00zg8mxf5n59sgf48g7z8pbnjd7ipqilc0q1jhr36nbrlnm") (f (quote (("unstable"))))))

(define-public crate-tendril-0.2.3 (c (n "tendril") (v "0.2.3") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "utf-8") (r "^0.6") (d #t) (k 0)))) (h "11s5kizn4xqg0sjxw3hsz0m7y5ws7yb4bznncqdllfch5m68dgyf") (f (quote (("unstable"))))))

(define-public crate-tendril-0.2.4 (c (n "tendril") (v "0.2.4") (d (list (d (n "encoding") (r "^0") (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "utf-8") (r "^0.6") (d #t) (k 0)))) (h "0qnz7072vl9rmqla8dv9dh9g597ajnyd7qr1940bhb901ljlrq2c") (f (quote (("unstable"))))))

(define-public crate-tendril-0.3.0 (c (n "tendril") (v "0.3.0") (d (list (d (n "encoding") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "utf-8") (r "^0.7") (d #t) (k 0)))) (h "0pxjr44vmi2paa2b9y8v7blrzbrb4yqh65hbz5xh27i1dblnnmq1") (f (quote (("unstable"))))))

(define-public crate-tendril-0.3.1 (c (n "tendril") (v "0.3.1") (d (list (d (n "encoding") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "utf-8") (r "^0.7") (d #t) (k 0)))) (h "1snajnvjpgmcx26yjfazlbbvj90gfc6agc8mqdjkpdzmwbw746wc") (f (quote (("unstable"))))))

(define-public crate-tendril-0.4.0 (c (n "tendril") (v "0.4.0") (d (list (d (n "encoding") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "utf-8") (r "^0.7") (d #t) (k 0)))) (h "027mhib12ndiqp2ixmjgmqjg0dawpjxr9na03437622sb531bqlx") (f (quote (("bench"))))))

(define-public crate-tendril-0.4.1 (c (n "tendril") (v "0.4.1") (d (list (d (n "encoding") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "futf") (r "^0.1.1") (d #t) (k 0)) (d (n "mac") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "utf-8") (r "^0.7") (d #t) (k 0)))) (h "0fsx7blrrzgca8aa2yqy8zxyi8s7amskhgkk1ml5sbaqyalyszvh") (f (quote (("bench"))))))

(define-public crate-tendril-0.4.2 (c (n "tendril") (v "0.4.2") (d (list (d (n "encoding") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "futf") (r "^0.1.2") (d #t) (k 0)) (d (n "mac") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "utf-8") (r "^0.7") (d #t) (k 0)))) (h "0csypx961p6ramqd8r4y43r671gp2mjz0a56lfjz194pndy5bvx9") (f (quote (("bench"))))))

(define-public crate-tendril-0.4.3 (c (n "tendril") (v "0.4.3") (d (list (d (n "encoding") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "futf") (r "^0.1.5") (d #t) (k 0)) (d (n "mac") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "utf-8") (r "^0.7") (d #t) (k 0)))) (h "1c3vip59sqwxn148i714nmkrvjzbk7105vj0h92s6r64bw614jnj") (f (quote (("bench"))))))

