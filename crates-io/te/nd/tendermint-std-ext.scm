(define-module (crates-io te nd tendermint-std-ext) #:use-module (crates-io))

(define-public crate-tendermint-std-ext-0.22.0 (c (n "tendermint-std-ext") (v "0.22.0") (h "1l3574qfrd0dgzqads304h93j6cgr7xpb8w2564xhsjs85ws9nr1")))

(define-public crate-tendermint-std-ext-0.23.0-internal (c (n "tendermint-std-ext") (v "0.23.0-internal") (h "11gxvbsidww2clvbwry92rbcn6wcmhfwslppc0fc1d1wd2d23r1p")))

(define-public crate-tendermint-std-ext-0.23.0 (c (n "tendermint-std-ext") (v "0.23.0") (h "1km6xwrmrxwjwhqrpq3xrjf2l0vds4mhrffajpzk00m3nfv8d20z")))

(define-public crate-tendermint-std-ext-0.23.1 (c (n "tendermint-std-ext") (v "0.23.1") (h "1v09njvdzc29dk084s3s6hii980iigvccvak08g8012phlnl9l5s")))

(define-public crate-tendermint-std-ext-0.23.2 (c (n "tendermint-std-ext") (v "0.23.2") (h "08vqrm0f3z30xmk4qm3hh362hfv8vr9m9bkvhk5aa7fhcg9ag6k3")))

(define-public crate-tendermint-std-ext-0.23.3 (c (n "tendermint-std-ext") (v "0.23.3") (h "092ybmxxvvxx8pcz74kh34ax5pf754b95w9w8g1cai0ryn0fmil1")))

(define-public crate-tendermint-std-ext-0.23.4 (c (n "tendermint-std-ext") (v "0.23.4") (h "1nmr44ir7jgqb8faw52xlxir8d6vwpdmczrfp15fanmnjf84njzw")))

(define-public crate-tendermint-std-ext-0.23.5 (c (n "tendermint-std-ext") (v "0.23.5") (h "0j8pr3mfrwd1d0pk0fb5vf2sxygpj06y0rgi860d8my8nsn2l71m")))

(define-public crate-tendermint-std-ext-0.24.0-pre.1 (c (n "tendermint-std-ext") (v "0.24.0-pre.1") (h "02hnlv3mp584w8wqliiwkfy1agpy4a0yzv1raxlp2lb7wlwdad0a")))

(define-public crate-tendermint-std-ext-0.23.6 (c (n "tendermint-std-ext") (v "0.23.6") (h "1yi3d7akh90vpb1n0f1nk50sfsn7c9zv349ca2mn0q4dap75cfl5")))

(define-public crate-tendermint-std-ext-0.23.7 (c (n "tendermint-std-ext") (v "0.23.7") (h "1n7k18pdsq738v90kxjd3pd17nky8banznz5xiqv3pn2q3d4qrjn")))

(define-public crate-tendermint-std-ext-0.24.0-pre.2 (c (n "tendermint-std-ext") (v "0.24.0-pre.2") (h "1bvprhfwndna0lcgz2gzq951nvyf2wyfs1xdl49awsbp967i8p93")))

(define-public crate-tendermint-std-ext-0.23.8-pre.1 (c (n "tendermint-std-ext") (v "0.23.8-pre.1") (h "1psy2919jw4hq6pk9n7m6p3vbb51h4v1hzbxpksg1ryxpwggsfk7")))

(define-public crate-tendermint-std-ext-0.23.8 (c (n "tendermint-std-ext") (v "0.23.8") (h "1anzxrhcd7ba2qyznkf5ylmpb3cvyjx1w6q4v88bxnr8s9j08hr3")))

(define-public crate-tendermint-std-ext-0.23.9 (c (n "tendermint-std-ext") (v "0.23.9") (h "0rqgwbvfnh4piiv9dwgc6hlivh9d524cvak0v734il4asynmnmi2")))

(define-public crate-tendermint-std-ext-0.25.0 (c (n "tendermint-std-ext") (v "0.25.0") (h "197m8rrhbg0qdnnxbfy2x9d0420ks7gkc063ky4r2hmgqkqb30mm")))

(define-public crate-tendermint-std-ext-0.26.0 (c (n "tendermint-std-ext") (v "0.26.0") (h "17hybfk0dyvnlbjnw8y4h8zwzidci5wdhhmmr515ysj2ci4n6m8a")))

(define-public crate-tendermint-std-ext-0.27.0 (c (n "tendermint-std-ext") (v "0.27.0") (h "07phx38qn8nn55qcwnmh32b04w1n3b3qcgdazy87y4cjwjj5m10m")))

(define-public crate-tendermint-std-ext-0.28.0 (c (n "tendermint-std-ext") (v "0.28.0") (h "02ngky5kmwzpw92a9kaydaxxib474cw7nlcqr6pyvlcv30mgdg51")))

(define-public crate-tendermint-std-ext-0.29.0 (c (n "tendermint-std-ext") (v "0.29.0") (h "1s9kdlxc6qi7drzfn2zp7ka34130ylhpjxdahgzb444b1f3j4cx0")))

(define-public crate-tendermint-std-ext-0.29.1 (c (n "tendermint-std-ext") (v "0.29.1") (h "1dngg5vkhfqlylzld4hrm7996s80lp7i8hvdw2c7s7pjq7yiygqb")))

(define-public crate-tendermint-std-ext-0.30.0 (c (n "tendermint-std-ext") (v "0.30.0") (h "0cjwwm9p5ag4jn7lwjb7nmzhbgcy6l46sjj122pw5b4dqf1vlb0s")))

(define-public crate-tendermint-std-ext-0.31.0 (c (n "tendermint-std-ext") (v "0.31.0") (h "1z6mw05jjvrrzl73cp748vs3clp8zw4s28qfxpq3jmhh9wlnp9wx")))

(define-public crate-tendermint-std-ext-0.31.1 (c (n "tendermint-std-ext") (v "0.31.1") (h "0fijcl7qhr7hm9q2ccc8clc61cdwixysv8nzmhq0vmxd64z83gcc")))

(define-public crate-tendermint-std-ext-0.32.0 (c (n "tendermint-std-ext") (v "0.32.0") (h "0p57a75jsbhaj60j3kwd4b4p5ib023shh1lb6m6dwrypyijsanmk")))

(define-public crate-tendermint-std-ext-0.32.1 (c (n "tendermint-std-ext") (v "0.32.1") (h "11miifqscjqq3zhzlg9vdb1if0ihw9w2hqgzhn359bkkm5wgv180")))

(define-public crate-tendermint-std-ext-0.32.2 (c (n "tendermint-std-ext") (v "0.32.2") (h "14nar3fp5qpp69vfz2mcrcl5v0gnv0g4dpnij3h4rvm32x9likja")))

(define-public crate-tendermint-std-ext-0.33.0 (c (n "tendermint-std-ext") (v "0.33.0") (h "1hyrg6xdqmidci617yd6if50l6gg891zdli6kpjhwpm90dmj6ndk")))

(define-public crate-tendermint-std-ext-0.33.1 (c (n "tendermint-std-ext") (v "0.33.1") (h "0hyjrh3z0gcsvymkzfhzbf1s3s1zzw4r1qmxq3qsmq6b8ypwi70z")))

(define-public crate-tendermint-std-ext-0.33.2 (c (n "tendermint-std-ext") (v "0.33.2") (h "1hl4474f02shil12fd52mrai7sg49qqa2pywv8qrm7iyir9cplbp")))

(define-public crate-tendermint-std-ext-0.34.0 (c (n "tendermint-std-ext") (v "0.34.0") (h "0q6bg3dmwbc0g6va9af72ms1ascfgaqny16gd8x0qx05j3hlscp6")))

(define-public crate-tendermint-std-ext-0.34.1 (c (n "tendermint-std-ext") (v "0.34.1") (h "0kz9hi240qfai28pvcjrn4fhlnrls0mk3h5s78hd1xpx70fajlsa")))

(define-public crate-tendermint-std-ext-0.35.0 (c (n "tendermint-std-ext") (v "0.35.0") (h "17xhi2n3xgzagyxphx253vdl2v3r2jpj0dlhyjk66gjlxdskin11")))

(define-public crate-tendermint-std-ext-0.36.0 (c (n "tendermint-std-ext") (v "0.36.0") (h "05gcmkaa3y6pbir5026v710dqsbyjz6ji28vgm2xs48z10cyr8da")))

