(define-module (crates-io te nd tendermint-machine) #:use-module (crates-io))

(define-public crate-tendermint-machine-0.1.0 (c (n "tendermint-machine") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-runtime") (r "^6.0.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "sync" "time" "rt"))) (d #t) (k 0)))) (h "1v61kfpd80l7l4qqwcsx8w03hndv4bgk4ck1ya6r2nnqy674v9h4") (f (quote (("substrate" "sp-runtime")))) (y #t)))

(define-public crate-tendermint-machine-0.2.0 (c (n "tendermint-machine") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-runtime") (r "^6.0.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "sync" "time" "rt"))) (d #t) (k 0)))) (h "0x64h6nyqrr5j7jwcgqqpasg534k9j6qkw1jsfr0adfnvwj91g74") (f (quote (("substrate" "sp-runtime")))) (y #t)))

