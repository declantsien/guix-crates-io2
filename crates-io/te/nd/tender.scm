(define-module (crates-io te nd tender) #:use-module (crates-io))

(define-public crate-tender-0.1.0 (c (n "tender") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 2)) (d (n "sync-wait-group") (r "^0.1.1") (d #t) (k 0)) (d (n "watch") (r "^0.2.2") (f (quote ("parking_lot"))) (d #t) (k 0)))) (h "17j3388nnwchi452slf6b5wpjdmpfbyajbv35smgj6d1azdxz27c") (r "1.57")))

(define-public crate-tender-0.1.1 (c (n "tender") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 2)) (d (n "sync-wait-group") (r "^0.1.1") (d #t) (k 0)) (d (n "watch") (r "^0.2.2") (f (quote ("parking_lot"))) (d #t) (k 0)))) (h "15lyby5sv5fdl27xd3hrb4dpd902959rd75m79dfy5iyq93g1j70") (r "1.57")))

