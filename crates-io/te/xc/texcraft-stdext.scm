(define-module (crates-io te xc texcraft-stdext) #:use-module (crates-io))

(define-public crate-texcraft-stdext-0.1.0 (c (n "texcraft-stdext") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1s88nvvjpvvqzdjrnqm7ps0bj9k2ifmblr3rrbxz0zbmj7089nvl") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

