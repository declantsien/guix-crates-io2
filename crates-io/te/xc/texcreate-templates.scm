(define-module (crates-io te xc texcreate-templates) #:use-module (crates-io))

(define-public crate-texcreate-templates-0.1.0 (c (n "texcreate-templates") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "texcore") (r "^0.2.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1wx5w6abx12gzsc77ngffzsyxybdxsw7s4kpq96wi1bxxzyvfjgf")))

(define-public crate-texcreate-templates-0.1.1 (c (n "texcreate-templates") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "texcore") (r "^0.2.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1sncjs73k6fcxkhx8h43c9h7r15yzp626bmk8310hzsdjcgsdi39")))

(define-public crate-texcreate-templates-0.1.2 (c (n "texcreate-templates") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "texcore") (r "^0.2.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0i5dd9fn0jd5mdchd0sjjwhpwjs4ryaaa6msn3n6kj2l44z0va78")))

(define-public crate-texcreate-templates-0.1.3 (c (n "texcreate-templates") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "texcore") (r "^0.2.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "01pvmbwkzk9dnqw9x8pm0xr54alpv5zb57xb5rqhikawgsgrnbfv")))

(define-public crate-texcreate-templates-0.1.4 (c (n "texcreate-templates") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "texcore") (r "^0.2.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "19z6r6yc1840n1md652mr1j2m35il1mcs4qslrf4rq4h4l973qnl")))

(define-public crate-texcreate-templates-0.1.5 (c (n "texcreate-templates") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "texcore") (r "^0.3.1") (f (quote ("compile"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0gcnsvsg02r9rwjfyf7p8r6a6qss6g51lpild9savsz0bdmx27a0")))

(define-public crate-texcreate-templates-0.1.6 (c (n "texcreate-templates") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "texcore") (r "^0.3.1") (f (quote ("compile"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1imkmy1yccyhnzfa8y7jk6fyq939i3wp5r1vpbrzp5gmp0f0y65l")))

