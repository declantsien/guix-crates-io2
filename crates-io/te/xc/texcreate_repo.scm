(define-module (crates-io te xc texcreate_repo) #:use-module (crates-io))

(define-public crate-texcreate_repo-0.1.0 (c (n "texcreate_repo") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "texcore") (r "^0.4.10") (f (quote ("texcreate_template"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "07d2jzrgipa409clx4r241v572aamvmz4msh303kc6zsi09cr17n")))

(define-public crate-texcreate_repo-0.1.1 (c (n "texcreate_repo") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "texcore") (r "^0.4.10") (f (quote ("texcreate_template"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0pawhcpcz8g9588cm9n3r22qjb5maqdd53wknyjdbqs6syjry88x")))

(define-public crate-texcreate_repo-0.1.2 (c (n "texcreate_repo") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "texcore") (r "^0.6.0") (f (quote ("texcreate_template"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0a543pnc7mv8g2ypzqkc212zsiqjzy5i1p5vzlad8qmjbqma67rb")))

(define-public crate-texcreate_repo-0.2.0 (c (n "texcreate_repo") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "texcore") (r "^0.7.1") (f (quote ("texcreate_template"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0kdmls9r9l340i39ldynbinyc3ivfr442k809lmrp5wnhd0k87rj") (f (quote (("release") ("default"))))))

(define-public crate-texcreate_repo-0.2.1 (c (n "texcreate_repo") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "texcore") (r "^0.7.1") (f (quote ("texcreate_template"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "1sbnjjf7mgpmsxshlwgwm7y1vypffdj6d3g2nygzi7dq6gyr9qpj") (f (quote (("release") ("default"))))))

