(define-module (crates-io te xc texclean) #:use-module (crates-io))

(define-public crate-texclean-0.1.0 (c (n "texclean") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "inquire") (r "^0.7.1") (d #t) (k 0)) (d (n "spinoff") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1inybwjmkadsy92801z7j7wbrdpmbnch2wicafnmlv6iydj06zd5")))

(define-public crate-texclean-0.2.0 (c (n "texclean") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "inquire") (r "^0.7.1") (d #t) (k 0)) (d (n "spinoff") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0hdj7m82wg55skdz15m4vixcfv9i3i9gxl772yy2zh137b8j4sac")))

