(define-module (crates-io te xc texcraft) #:use-module (crates-io))

(define-public crate-texcraft-0.1.0 (c (n "texcraft") (v "0.1.0") (d (list (d (n "texcraft-stdext") (r "^0.1.0") (d #t) (k 0)) (d (n "texlang") (r "^0.1.0") (d #t) (k 0)) (d (n "texlang-stdlib") (r "^0.1.0") (d #t) (k 0)))) (h "0w2hlw7sbl4azvbpk98d2qm2mrgqizq9654f9wx37x2z9k41kbib")))

