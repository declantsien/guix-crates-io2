(define-module (crates-io te xc texc-utils) #:use-module (crates-io))

(define-public crate-texc-utils-0.1.0 (c (n "texc-utils") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "texc-config") (r "^0.1") (d #t) (k 0)))) (h "1vwrzyxw2c0zjfrbc483c0krqv5y9k8wi668szdjkri064zjyva3")))

(define-public crate-texc-utils-0.1.1 (c (n "texc-utils") (v "0.1.1") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "tex-rs") (r "^0.2") (d #t) (k 0)) (d (n "texc-config") (r "^0.1") (d #t) (k 0)))) (h "1x6cn6dgrwjf6r4mmxvkhhfc2wx0h2qw988xwr6pjxkaq7gz89m8")))

(define-public crate-texc-utils-0.1.3 (c (n "texc-utils") (v "0.1.3") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "tex-rs") (r "^0.2") (d #t) (k 0)) (d (n "texc-config") (r "^0.1") (d #t) (k 0)))) (h "1prfvbnvjqg3ifms2qzkdvyk5b6m6744hywf2i678fh5fksa1brd")))

(define-public crate-texc-utils-0.1.4 (c (n "texc-utils") (v "0.1.4") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "tex-rs") (r "^0.2") (d #t) (k 0)) (d (n "texc-config") (r "^0.1") (d #t) (k 0)))) (h "00hggggv9hbw03772017bgnzm0w7ng56j57pjmwj6sgspv65gmkh")))

(define-public crate-texc-utils-0.1.5 (c (n "texc-utils") (v "0.1.5") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "tex-rs") (r "^0.2") (d #t) (k 0)) (d (n "texc-config") (r "^0.1") (d #t) (k 0)))) (h "0w9wcvbqz2l2gg1c19z42dpwfkhvg7d6yf0c9mxk3v0q37vs2g65")))

(define-public crate-texc-utils-0.1.6 (c (n "texc-utils") (v "0.1.6") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "tex-rs") (r "^0.2") (d #t) (k 0)) (d (n "texc-config") (r "^0.1") (d #t) (k 0)))) (h "1r9a0c59sjvm620cvyn2bkrwp80mxbch2d0v87hwngcb4hdgyw61")))

