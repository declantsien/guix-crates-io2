(define-module (crates-io te xc texc-latex) #:use-module (crates-io))

(define-public crate-texc-latex-0.1.0 (c (n "texc-latex") (v "0.1.0") (d (list (d (n "tex-rs") (r "^0.2") (d #t) (k 0)))) (h "0qcs3rhna6jdr7kws574m35vd572irllrb5ia74pn78zaqm1h52y")))

(define-public crate-texc-latex-0.1.1 (c (n "texc-latex") (v "0.1.1") (d (list (d (n "tex-rs") (r "^0.2") (d #t) (k 0)))) (h "1l02a97bws1jdw9v14kfrf9x5wj73yk2lyfzwzg7glm34m5w0ngw")))

(define-public crate-texc-latex-0.1.2 (c (n "texc-latex") (v "0.1.2") (d (list (d (n "mdbook") (r "^0.4.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "tex-rs") (r "^0.2") (d #t) (k 0)))) (h "17f58dfsv7x6iydbjisgzk3d2sjv7rjnb2mnacixh0v1l7kbd30j")))

(define-public crate-texc-latex-0.1.3 (c (n "texc-latex") (v "0.1.3") (d (list (d (n "mdbook") (r "^0.4.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "tex-rs") (r "^0.2") (d #t) (k 0)))) (h "0wd13s4mynkrr9lcdyxdi6pn0b3zgf2gia07x75bx67kqj9gkla8")))

(define-public crate-texc-latex-0.1.4 (c (n "texc-latex") (v "0.1.4") (d (list (d (n "mdbook") (r "^0.4.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "tex-rs") (r "^0.2") (d #t) (k 0)))) (h "1lsjhspg7ih2lfdr5bypnaja8i2fvnkdf94fvxf3af05cmfl7jhj")))

(define-public crate-texc-latex-0.1.5 (c (n "texc-latex") (v "0.1.5") (d (list (d (n "mdbook") (r "^0.4.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "tex-rs") (r "^0.2") (d #t) (k 0)))) (h "1baqhz2zb9i496p8s0bjzanp5s9aqy3q51py3ds9pzk9gwknm2d3")))

(define-public crate-texc-latex-0.1.6 (c (n "texc-latex") (v "0.1.6") (d (list (d (n "mdbook") (r "^0.4.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "tex-rs") (r "^0.2") (d #t) (k 0)))) (h "1v8y5ahq67g5wl9bjd0xv4hqaljh99hk8fq48cfqfacbjvp3q8h4")))

