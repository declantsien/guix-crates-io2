(define-module (crates-io te xc texc_v3_compiler_conf) #:use-module (crates-io))

(define-public crate-texc_v3_compiler_conf-0.1.0 (c (n "texc_v3_compiler_conf") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "00xbhfcv0pk07bknx6c4lkdbckz6w6l0xm4lcrcg7a1mp6y2c0hl")))

