(define-module (crates-io te lr telraam-rs) #:use-module (crates-io))

(define-public crate-telraam-rs-0.1.0 (c (n "telraam-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo" "derive" "env"))) (o #t) (d #t) (k 0)) (d (n "geojson") (r "^0.24.1") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "brotli" "deflate" "gzip" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "02vsi8glbkzmc3cia76gs92dmf0isfdq5dgdq00zm7w3nw3wc7p7")))

