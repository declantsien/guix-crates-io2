(define-module (crates-io te a5 tea5767) #:use-module (crates-io))

(define-public crate-tea5767-0.1.0 (c (n "tea5767") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "micromath") (r "^1.1.0") (d #t) (k 0)))) (h "0yfrihlic698b0x66wfjyyxag2ip41zskxb3qsc6hj5z89z3jiah")))

