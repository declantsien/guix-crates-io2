(define-module (crates-io te ta tetanes-web) #:use-module (crates-io))

(define-public crate-tetanes-web-0.1.0 (c (n "tetanes-web") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "console_log") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tetanes") (r "^0.7.0") (f (quote ("wasm-driver" "no-randomize-ram"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "0is9qbv064wq9v802faf808wkf2bhg0205mh2cwdqy3a2ac7ank3") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-tetanes-web-0.1.1 (c (n "tetanes-web") (v "0.1.1") (d (list (d (n "console_error_panic_hook") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "console_log") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tetanes") (r "^0.7") (f (quote ("wasm-driver" "no-randomize-ram"))) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "0ycqx6r0p1kr1hh7z2svfgzc83sain58rd1slw1pzhwvpy69fcmi") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-tetanes-web-0.2.0 (c (n "tetanes-web") (v "0.2.0") (d (list (d (n "console_error_panic_hook") (r "^0.1") (d #t) (k 0)) (d (n "console_log") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tetanes") (r "^0.9") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0rrbpsmmib76kjjj51dmdzy6dypwlnqn1m5f5nkhfig0siz2p707")))

