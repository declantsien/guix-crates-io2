(define-module (crates-io te t- tet-libp2p-dns) #:use-module (crates-io))

(define-public crate-tet-libp2p-dns-0.27.0 (c (n "tet-libp2p-dns") (v "0.27.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "tet-libp2p-core") (r "^0.27.0") (d #t) (k 0)))) (h "0fhwp9jmfjzl2cchs45lxpfdjg853dbp1zkpb3ddkpjdrja011lm")))

