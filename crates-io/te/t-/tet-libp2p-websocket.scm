(define-module (crates-io te t- tet-libp2p-websocket) #:use-module (crates-io))

(define-public crate-tet-libp2p-websocket-0.28.0 (c (n "tet-libp2p-websocket") (v "0.28.0") (d (list (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-rustls") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quicksink") (r "^0.1") (d #t) (k 0)) (d (n "rw-stream-sink") (r "^0.2.0") (d #t) (k 0)) (d (n "soketto") (r "^0.4.1") (f (quote ("deflate"))) (d #t) (k 0)) (d (n "tet-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.21") (d #t) (k 0)))) (h "16jfsxay9145isf2ff6ab7zawx1cl9cmvf0q3p8gj84x6k48pnwl")))

