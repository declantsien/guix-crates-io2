(define-module (crates-io te t- tet-libp2p-pnet) #:use-module (crates-io))

(define-public crate-tet-libp2p-pnet-0.20.0 (c (n "tet-libp2p-pnet") (v "0.20.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "salsa20") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 0)))) (h "0s1csinaldkjznd2jxh9gp6bwdf6f62gjwil4kwsi21hpypiklnk")))

