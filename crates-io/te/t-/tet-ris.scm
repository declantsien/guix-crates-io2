(define-module (crates-io te t- tet-ris) #:use-module (crates-io))

(define-public crate-tet-ris-0.2.1 (c (n "tet-ris") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fqksk53hr95a1ch7wj889giriw5z4wcl8k0cb1ca3nk7jqz1vya")))

(define-public crate-tet-ris-0.2.2 (c (n "tet-ris") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0l90jrfzgcd290f8l3afvsxj0j9a43vs4hxw4g9z5dyayx7nfj5n")))

(define-public crate-tet-ris-0.2.3 (c (n "tet-ris") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wqlvri9f3dk5xjscvpfpfdhc3w661jyli5mrwkssgadxqixrd09")))

(define-public crate-tet-ris-0.4.0 (c (n "tet-ris") (v "0.4.0") (d (list (d (n "bendy") (r "^0.3") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n4jpgbm97gi95kmnhps21pf6gk71q56zsd17b1sgh5qmlgpjdg2")))

(define-public crate-tet-ris-0.4.1 (c (n "tet-ris") (v "0.4.1") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (k 0)))) (h "00yx6061lmmy682y1aa27z4jhc3qrib039aq5ss1xhf0fc37cn1m")))

(define-public crate-tet-ris-0.4.2 (c (n "tet-ris") (v "0.4.2") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (k 0)))) (h "0i7x35anww4ypai47m5iy5zv4ggjjysxq35gqm3yjgbd66sv76f9")))

(define-public crate-tet-ris-0.5.0 (c (n "tet-ris") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (k 0)))) (h "1zq9sw8ywk9v3bql304p403sqvqpzdqy3zgzvl5fkzm97wvvgqbh")))

(define-public crate-tet-ris-0.5.1 (c (n "tet-ris") (v "0.5.1") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (k 0)))) (h "01c3rswmw6h3yfjz64hhdf3lvmv7amq3shl42cj7qpmicgfi3m1w")))

(define-public crate-tet-ris-0.5.2 (c (n "tet-ris") (v "0.5.2") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (k 0)))) (h "0llcs4kafvzv6bvnzay9dln34q5gg44c7xi6ca7y7x69ii1bs99z")))

(define-public crate-tet-ris-0.6.0 (c (n "tet-ris") (v "0.6.0") (d (list (d (n "bincode") (r ">=1.3.3") (k 0)) (d (n "clap") (r ">=4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r ">=0.26.1") (k 0)) (d (n "rand") (r ">=0.8.5") (d #t) (k 0)) (d (n "serde") (r ">=1.0.160") (f (quote ("derive"))) (k 0)))) (h "1aw3svv8883jnv40k9wc00krga8xlpk17sp9w1i2knl0h24764qk")))

(define-public crate-tet-ris-0.6.1 (c (n "tet-ris") (v "0.6.1") (d (list (d (n "bincode") (r ">=1.3.3") (k 0)) (d (n "clap") (r ">=4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r ">=0.26.1") (k 0)) (d (n "rand") (r ">=0.8.5") (d #t) (k 0)) (d (n "serde") (r ">=1.0.160") (f (quote ("derive"))) (k 0)))) (h "10bc6hkaplm0a7vypsnnwcfcy9ypvx1axfpf26pipv1v4s04msmr")))

(define-public crate-tet-ris-0.6.3 (c (n "tet-ris") (v "0.6.3") (d (list (d (n "bincode") (r ">=1.3.3") (k 0)) (d (n "clap") (r ">=4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r ">=0.26.1") (k 0)) (d (n "rand") (r ">=0.8.5") (d #t) (k 0)) (d (n "serde") (r ">=1.0.160") (f (quote ("derive"))) (k 0)))) (h "0cfvx3hbxmyikzi8qhx10f6h88z7ms15bykmm4gdbgjj5r11ypxx")))

(define-public crate-tet-ris-0.6.4 (c (n "tet-ris") (v "0.6.4") (d (list (d (n "bincode") (r ">=1.3.3") (k 0)) (d (n "clap") (r ">=4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r ">=0.26.1") (d #t) (k 0)) (d (n "rand") (r ">=0.8.5") (d #t) (k 0)) (d (n "serde") (r ">=1.0.160") (f (quote ("derive"))) (k 0)))) (h "03g54ilxzjljb77czmql7psfwnhk11j7fx2n2yqk0iwjyddifq9w")))

