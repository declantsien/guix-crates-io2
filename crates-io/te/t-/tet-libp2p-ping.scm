(define-module (crates-io te t- tet-libp2p-ping) #:use-module (crates-io))

(define-public crate-tet-libp2p-ping-0.27.0 (c (n "tet-libp2p-ping") (v "0.27.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "tet-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "tet-libp2p-swarm") (r "^0.27.0") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "1vd3ssrrg2apnlrfi2ybjp4gcm8d11rpqrygacy7j6406k2ldwdw")))

