(define-module (crates-io te t- tet-libp2p-identify) #:use-module (crates-io))

(define-public crate-tet-libp2p-identify-0.27.0 (c (n "tet-libp2p-identify") (v "0.27.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-build") (r "^0.7") (d #t) (k 1)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "tet-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "tet-libp2p-swarm") (r "^0.27.0") (d #t) (k 0)) (d (n "wasm-timer") (r "^0.2") (d #t) (k 0)))) (h "11q9mqa05k5mnbdkmrwk330vd8h72js8d1ac25mf3k5s6sfj7v4y")))

