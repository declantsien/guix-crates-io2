(define-module (crates-io te t- tet-libp2p-wasm-ext) #:use-module (crates-io))

(define-public crate-tet-libp2p-wasm-ext-0.27.0 (c (n "tet-libp2p-wasm-ext") (v "0.27.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.19") (d #t) (k 0)) (d (n "tet-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "tetsy-send-wrapper") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.4") (d #t) (k 0)))) (h "045gxga7k0d2j2bc42djzm1rgd9hjqnsbx5gf3z0z1m0xmncijlh") (f (quote (("websocket"))))))

