(define-module (crates-io te t- tet-libp2p-core-derive) #:use-module (crates-io))

(define-public crate-tet-libp2p-core-derive-0.21.0 (c (n "tet-libp2p-core-derive") (v "0.21.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0z7f4wglq0nc8ga1iip8hwl2v5szzb964ngrklcz3wxpbm7nklda")))

