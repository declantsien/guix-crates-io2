(define-module (crates-io te t- tet-multistream-select) #:use-module (crates-io))

(define-public crate-tet-multistream-select-0.10.0 (c (n "tet-multistream-select") (v "0.10.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rw-stream-sink") (r "^0.2.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.6") (d #t) (k 0)))) (h "003civ4v338vsxk5r2vwwx4byzhiy67kfs4n6bzc14apxcwgicbh")))

