(define-module (crates-io te t- tet-libp2p-remux) #:use-module (crates-io))

(define-public crate-tet-libp2p-remux-0.30.0 (c (n "tet-libp2p-remux") (v "0.30.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "remux") (r "^0.8.0") (d #t) (k 0)) (d (n "tet-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01r3a7cspbb79fhlrv4zbmialg9hf85sndgw36kf3fypyn2rgw40")))

