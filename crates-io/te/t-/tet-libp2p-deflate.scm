(define-module (crates-io te t- tet-libp2p-deflate) #:use-module (crates-io))

(define-public crate-tet-libp2p-deflate-0.27.0 (c (n "tet-libp2p-deflate") (v "0.27.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tet-libp2p-core") (r "^0.27.0") (d #t) (k 0)))) (h "05hn3yb5gx11qbpg18z2vxbmy9ii1m8dfcfy4jy6dwr5w4gq7pdw")))

