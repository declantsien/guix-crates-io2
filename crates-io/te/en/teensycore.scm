(define-module (crates-io te en teensycore) #:use-module (crates-io))

(define-public crate-teensycore-0.0.1 (c (n "teensycore") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1qyrwqx023xvp0pj4v7m0bsjyixlrxj51gh1s9f9y6qqnj531b4k")))

(define-public crate-teensycore-0.0.2 (c (n "teensycore") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04ndwv6xkp97qziviigg1kb9pvaks2abb7v1c7x5b85bac8xms3y")))

(define-public crate-teensycore-0.0.3 (c (n "teensycore") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wy0ncxa5syki1yizg7l8wagqkyychpikpgmp4446y8r94c7vzry")))

(define-public crate-teensycore-0.0.4 (c (n "teensycore") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0gfk8g23vnyx2dn53wziwiwj2gmp8jhma3ijxwhgj23zi09r3jmc")))

(define-public crate-teensycore-0.0.5 (c (n "teensycore") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0rnfbh2w4gqlhhd56y6qpbfksp9dx18v2h096796w47lprn6zydv")))

(define-public crate-teensycore-0.0.6 (c (n "teensycore") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "19k455hi1gpfn3wgnvpx6gcfbj9n2yvibwmvnqgglj0h81dgc9mx") (f (quote (("testing"))))))

(define-public crate-teensycore-0.0.7 (c (n "teensycore") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "10qlsiclzcvzwc8hi5hi9slaqyylw2gyyx6i5wivm55ncsh5g0ci") (f (quote (("testing"))))))

(define-public crate-teensycore-0.0.8 (c (n "teensycore") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1r4kn4sxy9ab5fr48221w6p9a2p37ga83c467v5m7iympy5n63nm") (f (quote (("testing"))))))

(define-public crate-teensycore-0.0.9 (c (n "teensycore") (v "0.0.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1jdl4m8fql70wrayj01rl1kj2b7v8pq2liipscfd672f4d78dfy5") (f (quote (("testing"))))))

(define-public crate-teensycore-0.0.10 (c (n "teensycore") (v "0.0.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0v8xsrmgcqcvz3mjpr4v7x2cmz7f5a4nlcghyv2fwzq3qmkc5zsg") (f (quote (("testing"))))))

(define-public crate-teensycore-0.0.11 (c (n "teensycore") (v "0.0.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "16l0ksns8ghwx4jmk4srn0w87pjp8q14w7sgh1f7d1qjj27y1701") (f (quote (("testing"))))))

(define-public crate-teensycore-0.0.12 (c (n "teensycore") (v "0.0.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0h6md32xbwrymfinnl4q2ib1pph2qsy3qnzgs9a33in2bhq78dz0") (f (quote (("testing"))))))

(define-public crate-teensycore-0.0.13 (c (n "teensycore") (v "0.0.13") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wjaaxbnbd7imhrlaydsz3009v7s06vzfaq3pbpndh5cxh5xigyj") (f (quote (("testing"))))))

(define-public crate-teensycore-0.0.14 (c (n "teensycore") (v "0.0.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "02j57wwlhznnkpprw1h4gl93afggqc76i44ipks23shxhc47nw03") (f (quote (("testing"))))))

(define-public crate-teensycore-0.0.15 (c (n "teensycore") (v "0.0.15") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0y9vmqv7k8rigjd6k98ny8wyw5172mw6mml8javyzghvqijnzv7z") (f (quote (("testing"))))))

(define-public crate-teensycore-0.1.0 (c (n "teensycore") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0fzz7dvc9689jzbxnzc2ypfyz3vwpykadisrr0hijg4d27svbfwx") (f (quote (("testing"))))))

