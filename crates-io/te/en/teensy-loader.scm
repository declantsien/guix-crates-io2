(define-module (crates-io te en teensy-loader) #:use-module (crates-io))

(define-public crate-teensy-loader-0.1.0 (c (n "teensy-loader") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "elf") (r "^0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rusb") (r "^0.5") (d #t) (k 0)))) (h "1va3rdpdczhcckrl6dlfdjsfxwln9imnn8nqrwc6il61rsxrjmq1")))

(define-public crate-teensy-loader-0.1.1 (c (n "teensy-loader") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "elf") (r "^0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rusb") (r "^0.5") (d #t) (k 0)))) (h "0nhvlx5cmixmwh21r07xl4wmgb6707303bnfdbmrycl3vxg3a5l8")))

(define-public crate-teensy-loader-0.1.2 (c (n "teensy-loader") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "elf") (r "^0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rusb") (r "^0.5") (d #t) (k 0)))) (h "0rr0xrrb1m56f69san195pz2lmx1af2hm5nb921h7amka0cp6lry")))

