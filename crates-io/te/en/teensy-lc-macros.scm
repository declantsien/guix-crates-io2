(define-module (crates-io te en teensy-lc-macros) #:use-module (crates-io))

(define-public crate-teensy-lc-macros-0.1.0 (c (n "teensy-lc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1442b8s5gl0k9q3iss4gn942rd58fd8zyjss3ym8w6smj2zdga13")))

(define-public crate-teensy-lc-macros-0.1.1 (c (n "teensy-lc-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ig69sl17824y6q5p0z5rf7s92nsci84kyz8s3ygx07abflvqz35")))

