(define-module (crates-io te en teensy4-panic) #:use-module (crates-io))

(define-public crate-teensy4-panic-0.1.0 (c (n "teensy4-panic") (v "0.1.0") (h "0q6yk0mcy44dib8s0a873i1zv5yds3wnfymh57bscs9njgad0zg4")))

(define-public crate-teensy4-panic-0.2.0 (c (n "teensy4-panic") (v "0.2.0") (h "1nmj6rrf2p2sfa9xa4lvmfqjgqx6gwsp4zfp847k96rs430yw1z3") (f (quote (("panic-handler") ("default" "panic-handler"))))))

(define-public crate-teensy4-panic-0.2.1 (c (n "teensy4-panic") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0l1gjyr2mg1w152i9cd05qdi8n7mvjbfn3dxsdfi2amyq7nmnvj8") (f (quote (("panic-handler") ("default" "panic-handler"))))))

(define-public crate-teensy4-panic-0.2.2 (c (n "teensy4-panic") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1rcm65cbqcd288sf6kgn0d81iw3az6kyyr875anzaarl66ffqh3j") (f (quote (("panic-handler") ("default" "panic-handler"))))))

(define-public crate-teensy4-panic-0.2.3 (c (n "teensy4-panic") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "084ci0sb7yz1gv5888r2mb5yjh8qsrbllwgn7l4psm8pkxbklwvw") (f (quote (("panic-handler") ("default" "panic-handler"))))))

