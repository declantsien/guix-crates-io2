(define-module (crates-io te en teensy3-sys) #:use-module (crates-io))

(define-public crate-teensy3-sys-0.1.0 (c (n "teensy3-sys") (v "0.1.0") (h "0ryjzcicrd7j0nxpgxyblk11xy5dy4xphpkpxhhnnf48nbidkm49")))

(define-public crate-teensy3-sys-0.2.0 (c (n "teensy3-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.23.1") (d #t) (k 1)))) (h "0i00d17j5h6w1zkc8jga6jgis33wnlgh845n0mk8ilrdcbrymjj6")))

