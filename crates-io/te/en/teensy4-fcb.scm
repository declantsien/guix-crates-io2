(define-module (crates-io te en teensy4-fcb) #:use-module (crates-io))

(define-public crate-teensy4-fcb-0.1.0 (c (n "teensy4-fcb") (v "0.1.0") (d (list (d (n "imxrt1062-fcb-gen") (r "^0.1.0") (d #t) (k 1)))) (h "0xvrdq005wwmwx5g0a301h5n0xxklkg1g51fldf023ynblsmys5x")))

(define-public crate-teensy4-fcb-0.2.0 (c (n "teensy4-fcb") (v "0.2.0") (d (list (d (n "imxrt-boot-gen") (r "^0.1.0") (f (quote ("imxrt1062"))) (d #t) (k 1)))) (h "1hs067zm4sfa7bhkc1pb8wbdqm4f1kmyjz7j689n1jp1r08qkh64")))

(define-public crate-teensy4-fcb-0.3.0 (c (n "teensy4-fcb") (v "0.3.0") (d (list (d (n "imxrt-boot-gen") (r "^0.2.0") (f (quote ("imxrt1060"))) (d #t) (k 0)))) (h "1686galdvm7wsr5qf8alpmr5f56gssimx6xfvd9g1p9jzynblyy3")))

(define-public crate-teensy4-fcb-0.4.0 (c (n "teensy4-fcb") (v "0.4.0") (d (list (d (n "imxrt-boot-gen") (r "^0.3.0") (f (quote ("imxrt1060"))) (d #t) (k 0)))) (h "03qxpypmas90fkr8g2k9cd80pb7g17jjnjssmbbsylxqh3bbc1w0")))

