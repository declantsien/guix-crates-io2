(define-module (crates-io te en teensy4-pins) #:use-module (crates-io))

(define-public crate-teensy4-pins-0.1.0 (c (n "teensy4-pins") (v "0.1.0") (d (list (d (n "imxrt-iomuxc") (r "^0.1.1") (f (quote ("imxrt106x"))) (d #t) (k 0)))) (h "1igbax3365x3f1div2sxd59vrgqvqn6wlfrdq9vcqx701mpnprvr")))

(define-public crate-teensy4-pins-0.2.0 (c (n "teensy4-pins") (v "0.2.0") (d (list (d (n "imxrt-iomuxc") (r "^0.1.1") (f (quote ("imxrt106x"))) (d #t) (k 0)))) (h "0b2qsnj6qk25qii3a1yrq3wvnb7b2nyy3qa4fd8xskl8pyxgyvq9")))

(define-public crate-teensy4-pins-0.3.0 (c (n "teensy4-pins") (v "0.3.0") (d (list (d (n "imxrt-iomuxc") (r "^0.2.0") (f (quote ("imxrt1060"))) (d #t) (k 0)))) (h "1nfdx5wpmyfwbacngn9pad64217dq94g4yqg92d8cjyr2g3628w2")))

(define-public crate-teensy4-pins-0.3.1 (c (n "teensy4-pins") (v "0.3.1") (d (list (d (n "imxrt-iomuxc") (r "^0.2.0") (f (quote ("imxrt1060"))) (d #t) (k 0)))) (h "01msymizg4xfjhhiqq12rlhkj5gj7a3mhk816h03r62i4ncwql2s")))

(define-public crate-teensy4-pins-0.3.2 (c (n "teensy4-pins") (v "0.3.2") (d (list (d (n "imxrt-iomuxc") (r "^0.2.0") (f (quote ("imxrt1060"))) (d #t) (k 0)))) (h "17yml6iqwl0cpijpw90h6x45wmvm70wa0ms4jy1v0d5m65a1y6k3")))

