(define-module (crates-io te en teensy) #:use-module (crates-io))

(define-public crate-teensy-0.1.0 (c (n "teensy") (v "0.1.0") (d (list (d (n "curl") (r "^0.3") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "1sa495y4x3azaa1k81bcdqy5qs17v3bsa44rfjfa86xhrjis981z")))

