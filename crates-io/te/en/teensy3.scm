(define-module (crates-io te en teensy3) #:use-module (crates-io))

(define-public crate-teensy3-0.1.0 (c (n "teensy3") (v "0.1.0") (d (list (d (n "teensy3-sys") (r "^0.1") (d #t) (k 0)))) (h "11dbf55kpwscl2ssyxb0zmqb3lllywjprzqd816z5c6lrm622iw4")))

(define-public crate-teensy3-0.1.1 (c (n "teensy3") (v "0.1.1") (d (list (d (n "teensy3-sys") (r "^0.1") (d #t) (k 0)))) (h "0l0l4m69m7hi2fdg1f67ksm90bqxnlbd6q7j1bclpj30vzyrmqp5")))

(define-public crate-teensy3-0.1.2 (c (n "teensy3") (v "0.1.2") (d (list (d (n "teensy3-sys") (r "^0.1") (d #t) (k 0)))) (h "050s3n3mqmmk62dm4ma8yfd64h6yffh7bq0qmbga7h635lc9qjn4")))

(define-public crate-teensy3-0.1.3 (c (n "teensy3") (v "0.1.3") (d (list (d (n "teensy3-sys") (r "^0.1") (d #t) (k 0)))) (h "128q7m841bh1gjl2n44y44clkzn4ydvibnglb47i11v0sfyd749k")))

(define-public crate-teensy3-0.2.0 (c (n "teensy3") (v "0.2.0") (d (list (d (n "teensy3-sys") (r "^0.2") (d #t) (k 0)))) (h "02vjb6l1z9k283sg53sxxin3i23mhqwgxhflgvira00gf3k0xsmz")))

