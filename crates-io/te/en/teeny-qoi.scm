(define-module (crates-io te en teeny-qoi) #:use-module (crates-io))

(define-public crate-teeny-qoi-0.1.0 (c (n "teeny-qoi") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 2)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "0097dqvd5jl735d6vv18yh2qbcfm7p0n7c49rxv5znhm7kgnizp7") (f (quote (("std") ("default" "std") ("alloc"))))))

