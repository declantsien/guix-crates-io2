(define-module (crates-io te mp tempest-source-redis) #:use-module (crates-io))

(define-public crate-tempest-source-redis-0.1.0 (c (n "tempest-source-redis") (v "0.1.0") (d (list (d (n "redis-streams") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempest") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "03zi861lxy8q96lrydabpp0lw6hhwjjpfsrz0pkp3hyxiw7cdd7w")))

(define-public crate-tempest-source-redis-0.1.1 (c (n "tempest-source-redis") (v "0.1.1") (d (list (d (n "redis-streams") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempest") (r "= 0.1.1") (d #t) (k 0)) (d (n "tempest-source") (r "= 0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0i9blsi26s5pqvcw463kfbcbv672ixlq5fvbl6f7ykak8rccsbpm")))

