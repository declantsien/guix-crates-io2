(define-module (crates-io te mp templ) #:use-module (crates-io))

(define-public crate-templ-0.1.0 (c (n "templ") (v "0.1.0") (h "11wwbjlnl3lx9jnznmn3fa5v81lgnv8gflnvzgkfzli0jxhfr96v") (y #t)))

(define-public crate-templ-0.0.0 (c (n "templ") (v "0.0.0") (h "1k5hjk7ra2rmy86qk6l264qg9l88aiil9dmxpf47y032ma5qavsh")))

