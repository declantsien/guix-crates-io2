(define-module (crates-io te mp temporary_mail) #:use-module (crates-io))

(define-public crate-temporary_mail-0.1.0 (c (n "temporary_mail") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "058ncr7q3x4kr92vw2z6j03liaspwfck9k4bn9zk3c9hav83ysni") (y #t)))

(define-public crate-temporary_mail-0.1.1 (c (n "temporary_mail") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0r4pwfnagchq90vz6cc3dbgjmpjx6z9fbi3yivr3mxr7g9zqgis7") (y #t)))

(define-public crate-temporary_mail-0.1.2 (c (n "temporary_mail") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1kpfk51jfjb3zwqm6z1vdhfkg7mjrh9kg8fsv201ccqaq1q51ass")))

