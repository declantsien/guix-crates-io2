(define-module (crates-io te mp temply) #:use-module (crates-io))

(define-public crate-temply-0.1.0 (c (n "temply") (v "0.1.0") (d (list (d (n "temply-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1lkpjpqxm7d0wj3pgncajxw2aghyggx0brvzk26xspav8yfaj1k9") (f (quote (("derive" "temply-derive") ("default" "derive")))) (r "1.56")))

(define-public crate-temply-0.2.0 (c (n "temply") (v "0.2.0") (d (list (d (n "temply-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0wyds1kgjhdg22vqm1q2jg9ld51bgsxxdwn2y5phymmw3idc2bzf") (f (quote (("derive" "temply-derive") ("default" "derive")))) (r "1.56")))

(define-public crate-temply-0.3.0 (c (n "temply") (v "0.3.0") (d (list (d (n "temply-derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "12p3zlszip8rds4jmji9xsc1i2r9kli50l0faqswas6k3fsb63rc") (f (quote (("derive" "temply-derive") ("default" "derive")))) (r "1.56")))

