(define-module (crates-io te mp templog) #:use-module (crates-io))

(define-public crate-templog-0.1.0 (c (n "templog") (v "0.1.0") (d (list (d (n "kern") (r "^1.3.0") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "0zf5y1p0lacf8mjr9gv95kjs8krca1kflf88zk2505y21c0ry3by")))

