(define-module (crates-io te mp temporary) #:use-module (crates-io))

(define-public crate-temporary-0.0.1 (c (n "temporary") (v "0.0.1") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "16ha1g6rmjs13hiqdnpf76lrfy6wda0nr18akrbv309vgqcjxjay")))

(define-public crate-temporary-0.0.2 (c (n "temporary") (v "0.0.2") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "09fmb727id2cidmpkmxr7hxdvjdbcxy4gxx6anlknpxdymi8zcxp")))

(define-public crate-temporary-0.0.3 (c (n "temporary") (v "0.0.3") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0r9bh04sv2sk3r200fq5idia0zfbk7r604yflxw0labv9gsa09v4")))

(define-public crate-temporary-0.0.4 (c (n "temporary") (v "0.0.4") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "1a4irbbdr1ca1mlvmdyn8znchs0l5nbba59xrq2a24c1dm1p92xz")))

(define-public crate-temporary-0.0.5 (c (n "temporary") (v "0.0.5") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0sjr84l72rsmrrh8dy6z9jisdr3jvdjar6idjv3plwj69a8vxfj6")))

(define-public crate-temporary-0.1.0 (c (n "temporary") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "1b4mpgc0q0jkn77axy7czj2m5210j89hmrg1jp69smjjzmxhrvpx")))

(define-public crate-temporary-0.1.1 (c (n "temporary") (v "0.1.1") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "033qmf7ykk7l81mbgci85pqk8w9awn9f7f719qfgdvy4ginz45q2")))

(define-public crate-temporary-0.2.0 (c (n "temporary") (v "0.2.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "198nalsivjdqf42c82nrwsrrjh0xcaghvzyis3vkv92w938zfsl9")))

(define-public crate-temporary-0.3.0 (c (n "temporary") (v "0.3.0") (h "12gn7m1yldzvmralswxpm5lhmsizsbw5z9r0x1bcv74fbz79dfwa")))

(define-public crate-temporary-0.4.0 (c (n "temporary") (v "0.4.0") (h "021v7bg488x322ylqf9r9nmyhd4akmbmj7s7sz12k78v3ln063q3")))

(define-public crate-temporary-0.4.1 (c (n "temporary") (v "0.4.1") (d (list (d (n "random") (r "^0.9") (d #t) (k 0)))) (h "0pznl856kxsg5nwybiqaa34vsgph648ims9v3hffas5j2ag2mgq3")))

(define-public crate-temporary-0.5.0 (c (n "temporary") (v "0.5.0") (d (list (d (n "random") (r "^0.9") (d #t) (k 0)))) (h "1rmr0qzhzcmjrpcfi52nmchlx7d2x59x8nqkarkzbzgqv4z3vxsy")))

(define-public crate-temporary-0.5.1 (c (n "temporary") (v "0.5.1") (d (list (d (n "random") (r "^0.9") (d #t) (k 0)))) (h "1xck6innx3fwjbd3x1k7lj6bps4pizglp5llq8v8x0wryxcc3j3n")))

(define-public crate-temporary-0.6.0 (c (n "temporary") (v "0.6.0") (d (list (d (n "random") (r "^0.10") (d #t) (k 0)))) (h "04xz5cysm4nsggaymcv90nna62a820irg3cbd5hpjq11lsxppffm")))

(define-public crate-temporary-0.6.1 (c (n "temporary") (v "0.6.1") (d (list (d (n "random") (r "^0.11") (d #t) (k 0)))) (h "0gybaq8rrk5bv3cinif91qvjnfky9fn9y1q2sgbcv1d5pmdb7qjw")))

(define-public crate-temporary-0.6.2 (c (n "temporary") (v "0.6.2") (d (list (d (n "random") (r "^0.12") (d #t) (k 0)))) (h "13pgvkhw0yy3hz3j9g8a6y12g3m92sww42m9bicnljrjvz4jnnla")))

(define-public crate-temporary-0.6.3 (c (n "temporary") (v "0.6.3") (d (list (d (n "random") (r "^0.12") (d #t) (k 0)))) (h "1z0vg6yb54xlb5aapdwga57s5xbcvgzw2n21m2zv7flx8b4dsd5n")))

(define-public crate-temporary-0.6.4 (c (n "temporary") (v "0.6.4") (d (list (d (n "random") (r "^0.12") (d #t) (k 0)))) (h "1llj529jcbxxc5g76mdyqkysrpy9009n43k028kj1ywv7qqfvmxc")))

(define-public crate-temporary-0.7.0 (c (n "temporary") (v "0.7.0") (d (list (d (n "random") (r "^0.14") (d #t) (k 0)))) (h "0i48n7ffi17pqifn5r9m8yg3myx6cgfj2qlksjzwln7gxwgxfmpv")))

