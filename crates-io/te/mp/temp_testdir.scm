(define-module (crates-io te mp temp_testdir) #:use-module (crates-io))

(define-public crate-temp_testdir-0.2.0 (c (n "temp_testdir") (v "0.2.0") (h "1mi4avjqimxqi1gfywbhksrg6w6950h8bw9hlgmzwzyjmmdyzkk7")))

(define-public crate-temp_testdir-0.2.1 (c (n "temp_testdir") (v "0.2.1") (h "1zmcdmyspg4h9asc7j6casm3ci4lp9hdzk9mlfa9bhq1mdvh0wkn")))

(define-public crate-temp_testdir-0.2.2 (c (n "temp_testdir") (v "0.2.2") (h "1fgx66dm0i4y3sd8a56321bgps5hi42ajs4gxd31v0f52cxmp38b")))

(define-public crate-temp_testdir-0.2.3 (c (n "temp_testdir") (v "0.2.3") (h "1z5yv6d9944md5zg6g0sbahv7xjga2k232x40x4l20kq8af1w7wj")))

