(define-module (crates-io te mp temp-file) #:use-module (crates-io))

(define-public crate-temp-file-0.1.0 (c (n "temp-file") (v "0.1.0") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "07h6q3l1kbxpqzwzllnxzf0f2ygbr66d8mjpycaaqhd941yljkcr")))

(define-public crate-temp-file-0.1.1 (c (n "temp-file") (v "0.1.1") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "1hr10jpjjj71r3j94nf86gcjhwcx1ra0j09569m6l7har2vjb1mv")))

(define-public crate-temp-file-0.1.2 (c (n "temp-file") (v "0.1.2") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "13k40mkh3x7aglql1h0j7ssil19qrz44jas80lz4ggq2m48rfv3v")))

(define-public crate-temp-file-0.1.3 (c (n "temp-file") (v "0.1.3") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "011q63n9mng3bv52bz5xbsa8dfra5a8djj9dpxz3wphig1z1rnqf")))

(define-public crate-temp-file-0.1.4 (c (n "temp-file") (v "0.1.4") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "0lyiyawdfr41kr1hix8yigfi9pazrpp5bfp6llwrbbmsy9x2apmq")))

(define-public crate-temp-file-0.1.5 (c (n "temp-file") (v "0.1.5") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "0a6rxlws3vx68gr93n96z01azk3f4ibwq58hh3w5a8f2mqihsrnq")))

(define-public crate-temp-file-0.1.6 (c (n "temp-file") (v "0.1.6") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "0lk1y2m2kbc0k90b5z7ij685yc2lgbyg72zsllmv2hhzhcvdlwgf")))

(define-public crate-temp-file-0.1.7 (c (n "temp-file") (v "0.1.7") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1") (d #t) (k 2)))) (h "139slwcvph9216wsj0s5v2bxzvkkm934nxy47xf4r3llxrq9hizl")))

(define-public crate-temp-file-0.1.8 (c (n "temp-file") (v "0.1.8") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1") (d #t) (k 2)))) (h "079jh7lbshaljjgn1hrjwzl8zpv1hfvd24anv48z60yhc7d0n8bz")))

