(define-module (crates-io te mp templates) #:use-module (crates-io))

(define-public crate-templates-0.1.0 (c (n "templates") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1r9627rm1afdhgd506s07qw3cphp9k9arp78jjdgam68asn84mx8")))

(define-public crate-templates-0.2.0 (c (n "templates") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1id8l1bgd6b6bmbb93apf8p0jbsyv4wclsg7c2k2pspk7m79kb42")))

(define-public crate-templates-0.3.0 (c (n "templates") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1nvpnhm0ywiiynyv3fbyyiw3vg0fcm3zw36qjhs7x09d0l4qhd9s")))

(define-public crate-templates-0.3.1 (c (n "templates") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "11466q4qd7zk57y88gif7jrc4bni4jw71ia3rzdcm6jfnkkbdc5g")))

(define-public crate-templates-0.3.2 (c (n "templates") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1hqgzjwx4wn5z39wnmyvifqvgvmb670xpaighaxvx4p8i5pc0xiq")))

(define-public crate-templates-0.4.2 (c (n "templates") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "12qgv5lqr1n19cd8gp9w333gvll65srm249qlr9dnrbci2y5rnja")))

(define-public crate-templates-0.5.2 (c (n "templates") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1v2q0q3ip2g7j98id6m5s4g3kyys3j5linrs8fc2z8vad7bym3h0")))

(define-public crate-templates-0.6.2 (c (n "templates") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1hbd4h55s50invbf8gmck21ajq10kxqrcvpv7v5gzcjx3yc9dkd3")))

(define-public crate-templates-0.7.2 (c (n "templates") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0d01qs1r9b90an91lvrqbi2yw74vpdj6bv661spn38hy5f7s19h3")))

(define-public crate-templates-0.7.3 (c (n "templates") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1pyz4qp5wra65fs59v3gh6l5h6qn1yvjsm62wjf2m2ch42psw5v5")))

(define-public crate-templates-0.8.0 (c (n "templates") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1kg0g3ma0i50416rq0jmyrfnhxzhjcz0hc4ipv1gmi6r2nn004hq")))

(define-public crate-templates-0.10.0 (c (n "templates") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1w3slgs9dp0d62512w1ca64v37khg1w9030vr7hdlah93l905vg6")))

