(define-module (crates-io te mp tempdb_cockroach) #:use-module (crates-io))

(define-public crate-tempdb_cockroach-0.1.0 (c (n "tempdb_cockroach") (v "0.1.0") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "07gvwgglj2qs8xiwq2ps7nzw88cqg10awc646dy6jwsrx2slxxzx")))

(define-public crate-tempdb_cockroach-1.0.1 (c (n "tempdb_cockroach") (v "1.0.1") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0f4sd4q9q8dv9rf14lbc2z2py8bv82wnlvzp1wv9g1ivdm6602i0")))

(define-public crate-tempdb_cockroach-1.0.2 (c (n "tempdb_cockroach") (v "1.0.2") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1qzw9cg2jal4x38f8psac3lhc2zwn630094wfvgw8yxq5cx12p8y")))

(define-public crate-tempdb_cockroach-1.0.3 (c (n "tempdb_cockroach") (v "1.0.3") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "11fr4ih3y208kv0p3pl3cg6anpyi73p146yikdrg4lr3lcfnvqjc")))

(define-public crate-tempdb_cockroach-1.0.4 (c (n "tempdb_cockroach") (v "1.0.4") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "16pi75i6ryhpc4glyxsznpphsq743qcdrnr60sqgknr5a1050q7v")))

(define-public crate-tempdb_cockroach-1.0.5 (c (n "tempdb_cockroach") (v "1.0.5") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1rhq0ivldw7g56g5rsx99nxk8zmrgibymq8apifr5xx7c092cdqk")))

(define-public crate-tempdb_cockroach-1.0.6 (c (n "tempdb_cockroach") (v "1.0.6") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0qd8rbdc19dbyma62w0idz6n1m013ymcbryh5q6jlka9av0rbv46")))

(define-public crate-tempdb_cockroach-1.0.7 (c (n "tempdb_cockroach") (v "1.0.7") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "07rvwa32cn5ybjqzw5d1g5vsn34ksk6gvvf3qfx4j2zbvfcksaw0")))

(define-public crate-tempdb_cockroach-1.0.9 (c (n "tempdb_cockroach") (v "1.0.9") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0kk5c5845pmjdx3md389220v26v0yjw7xxibic6bg222m8i421ip")))

