(define-module (crates-io te mp template-quote-impl) #:use-module (crates-io))

(define-public crate-template-quote-impl-0.1.0 (c (n "template-quote-impl") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "derive" "printing"))) (d #t) (k 0)))) (h "1m6cw1kah5j44366bh70hwx5nkkgwgzkmy4wz8yajzdhcb7z8ms0")))

(define-public crate-template-quote-impl-0.1.1 (c (n "template-quote-impl") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "derive" "printing"))) (d #t) (k 0)))) (h "1b6xn0a3f3rczid2w6l8wzzn8fng2hfh61saa0dbrd279vnikhlm")))

(define-public crate-template-quote-impl-0.2.0 (c (n "template-quote-impl") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "derive" "printing"))) (d #t) (k 0)))) (h "1ggjjz9hi8c5w8ms7g1w3vd45m5h6wnnlw9fpajaa149yvlqf3l4")))

(define-public crate-template-quote-impl-0.3.0 (c (n "template-quote-impl") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "derive" "printing"))) (d #t) (k 0)))) (h "1jq96zwsrq3aa880nv3djnzk432gprcmdhmamfrxa9xq85ji421h")))

(define-public crate-template-quote-impl-0.3.1 (c (n "template-quote-impl") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "derive" "printing"))) (d #t) (k 0)))) (h "0s1v0lhz1yvp7z04p514s548hfk8mq54zy8rnak9ym0csc9mj928")))

