(define-module (crates-io te mp templest) #:use-module (crates-io))

(define-public crate-templest-0.1.0 (c (n "templest") (v "0.1.0") (h "08ijzd8258qcmasyg8sphxc6bdrc4pyl6781rqwgdshvz4pvzddz")))

(define-public crate-templest-0.1.1 (c (n "templest") (v "0.1.1") (h "0y05xd92kczpjmmyq9ihpkcqvj45kw4qbdg09ky07c08nxnhz67y")))

(define-public crate-templest-1.0.0 (c (n "templest") (v "1.0.0") (h "1i9pxpm65g2ajj1yqchqglrkaw1zmwgyhnswp032jck65pva89ll")))

