(define-module (crates-io te mp temply-derive) #:use-module (crates-io))

(define-public crate-temply-derive-0.1.0 (c (n "temply-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "19bn7b4yi20blv3261jpj80jwq4zc1fbi96bk0a1nmgbyrmii5jn") (r "1.56")))

(define-public crate-temply-derive-0.2.0 (c (n "temply-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0xbkx7nxv116msqcj7aay0qsq60g0gnb6s50zln4kxmdw8c4bjg4") (r "1.56")))

(define-public crate-temply-derive-0.3.0 (c (n "temply-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0nphbvna2khpj51s545axssl6ag638rg5jz5ah4ra4kir0vs8qql") (r "1.56")))

