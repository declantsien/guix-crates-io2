(define-module (crates-io te mp tempature_converter) #:use-module (crates-io))

(define-public crate-tempature_converter-1.0.0 (c (n "tempature_converter") (v "1.0.0") (h "0bdkmrwr3ym6nzkg3v7vyqjd52li5dyskdwm1nqy00ca8lmdfaz0")))

(define-public crate-tempature_converter-1.0.1 (c (n "tempature_converter") (v "1.0.1") (h "1saya1fg9hyjf4f7qa00c1ypfav2c90pah6zrcn56afm6svsw655")))

(define-public crate-tempature_converter-1.0.2 (c (n "tempature_converter") (v "1.0.2") (h "0dp8pr1c7fz8hbladp4vwwpk7r89k2asia1xkkly2b7y06hgaxq3")))

