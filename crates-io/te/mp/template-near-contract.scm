(define-module (crates-io te mp template-near-contract) #:use-module (crates-io))

(define-public crate-template-near-contract-0.1.0 (c (n "template-near-contract") (v "0.1.0") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "0jzqvdkq2w70m66qkz6x9qpn0cl99jwcr74ai0xfavm7vg8wpgir")))

(define-public crate-template-near-contract-0.1.1 (c (n "template-near-contract") (v "0.1.1") (d (list (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)))) (h "0a6kgsqqlllfw019r3nfpsyvg37k8y9yqn69h3waszx8l71bmn9m")))

