(define-module (crates-io te mp tempi) #:use-module (crates-io))

(define-public crate-tempi-0.1.0 (c (n "tempi") (v "0.1.0") (d (list (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h62w98v4jpyyv3rr1ndn1g5ccsikcfvy7a3s9r3aqx9q3sn63jn") (y #t)))

