(define-module (crates-io te mp templateme) #:use-module (crates-io))

(define-public crate-templateme-0.1.0 (c (n "templateme") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 0)))) (h "1c410w5ym4fl91xpck7bc7vrcjq6l80f7m4g6sz3h7si2hq6454f")))

(define-public crate-templateme-0.1.1 (c (n "templateme") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 0)))) (h "0xcb0n347knl3p9i2ifsfnn3r56pchbbapp0bpjpd124qqix0xfv")))

(define-public crate-templateme-0.1.3 (c (n "templateme") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 0)))) (h "0yp5fb97pfsabayw22ac28szqzxyzxziv9vlw3ygsnm2950dmq1s")))

