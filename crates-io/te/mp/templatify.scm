(define-module (crates-io te mp templatify) #:use-module (crates-io))

(define-public crate-templatify-0.1.0 (c (n "templatify") (v "0.1.0") (h "1n414bfx8igkcfzykf0r7k0iyj5316c3qjn8rwhd3zvmcrkizn11")))

(define-public crate-templatify-0.2.0 (c (n "templatify") (v "0.2.0") (h "10j1qfawrmkh9yx059czbcr45ziqvrqrlq8i334869i1wlimy2qk")))

(define-public crate-templatify-0.2.1 (c (n "templatify") (v "0.2.1") (h "0gkss28d8sjakdfdnnhkff4sp23xjdb76bcl8n2hv96whcyzvczv")))

(define-public crate-templatify-0.2.2 (c (n "templatify") (v "0.2.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "05ddwkllyqmmzsz3bf7w4jhngxjr0yhhwd14yv19r6xfxid74f1v")))

(define-public crate-templatify-0.2.3 (c (n "templatify") (v "0.2.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1qcgibxdrgssq6zjnjfkca6nw19lzvpzxnclz06aij8pd4nh6a55")))

