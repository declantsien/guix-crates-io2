(define-module (crates-io te mp tempest-source) #:use-module (crates-io))

(define-public crate-tempest-source-0.1.0 (c (n "tempest-source") (v "0.1.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1cxrw5451xnd5myxsq9w5fcsqzw1yjf9b28mfqa7clbam221zbll")))

(define-public crate-tempest-source-0.1.1 (c (n "tempest-source") (v "0.1.1") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "165skd925vkyfvkwl7h5m14dlq9g4rg7lr87nvdrgzbpxf4m2qgb")))

