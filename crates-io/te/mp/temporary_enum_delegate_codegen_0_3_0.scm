(define-module (crates-io te mp temporary_enum_delegate_codegen_0_3_0) #:use-module (crates-io))

(define-public crate-temporary_enum_delegate_codegen_0_3_0-0.3.0 (c (n "temporary_enum_delegate_codegen_0_3_0") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0w4ppfamr8smnn6frj6bqx2k94ki3sn574jih6j2shvq94bgk4c9")))

