(define-module (crates-io te mp temp-dir) #:use-module (crates-io))

(define-public crate-temp-dir-0.1.0 (c (n "temp-dir") (v "0.1.0") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "0f6dli9z05hgg0cncaa1l03pzpy24v7kh0sym8i4zaijr7za4sa2")))

(define-public crate-temp-dir-0.1.1 (c (n "temp-dir") (v "0.1.1") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "0zhq09q4x5f61cvgc98pfg0icpr3cx3agsynf5j2znjpf2zpr32j")))

(define-public crate-temp-dir-0.1.2 (c (n "temp-dir") (v "0.1.2") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "05g25dc4x2zga4q2r4dk8viffj2lsn15xi2pzcb63v1yplw5a6k1")))

(define-public crate-temp-dir-0.1.3 (c (n "temp-dir") (v "0.1.3") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "150cdgmmknpc1fmkvw95z4kysjhwkja82d1j9zbx5dpvmzcsviyx")))

(define-public crate-temp-dir-0.1.4 (c (n "temp-dir") (v "0.1.4") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "0fvp25il1aaz3jkzqhiyw9zqw123cm3p9lrrggrrnnw23s30g7ld")))

(define-public crate-temp-dir-0.1.5 (c (n "temp-dir") (v "0.1.5") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "10xdwmhd2jd6q65r71xa38z54nszfs7zd6wqax55l0nbl81rvqh7")))

(define-public crate-temp-dir-0.1.6 (c (n "temp-dir") (v "0.1.6") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "15xrzksfpgqh45d25fasjvc1gnyrv5wk5sj4abw8l9r4z8qw4b4m")))

(define-public crate-temp-dir-0.1.7 (c (n "temp-dir") (v "0.1.7") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "0jpxk034kc6p1lgb0xcxx2laq58pl6q6qc0ivag5c1nvqqvks5xg")))

(define-public crate-temp-dir-0.1.8 (c (n "temp-dir") (v "0.1.8") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "0dk5rn6kxj72mhzx32wi23jinvs9273v2y3q1vrr9996zs0hd8l6")))

(define-public crate-temp-dir-0.1.9 (c (n "temp-dir") (v "0.1.9") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "0z3cfch3khjfal2wbnabc3kqakdx9w8afc95nsfv2dnxip4791ks")))

(define-public crate-temp-dir-0.1.10 (c (n "temp-dir") (v "0.1.10") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "1zc8d5d20njybhk20gb3y5qrq9gfxgk8rbjqw349ah7dp7zwncb8")))

(define-public crate-temp-dir-0.1.11 (c (n "temp-dir") (v "0.1.11") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "1arxa8vfc1gj1w5njm7q2s0q3dngav29ymk5453lpsnidlb7nm5g")))

(define-public crate-temp-dir-0.1.12 (c (n "temp-dir") (v "0.1.12") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "1mkizff1mxk4d3hng8cmlggrhvic2dr6fxp3dqf05zhmzsgsl5nx")))

(define-public crate-temp-dir-0.1.13 (c (n "temp-dir") (v "0.1.13") (d (list (d (n "safe-lock") (r "^0.1") (d #t) (k 2)))) (h "0cbj4y3c5494i7ljhnqa38cgdkybl33p70cv5wrfbw00xil7j8hz")))

