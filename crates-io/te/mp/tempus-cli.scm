(define-module (crates-io te mp tempus-cli) #:use-module (crates-io))

(define-public crate-tempus-cli-1.0.2 (c (n "tempus-cli") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (d #t) (k 0)))) (h "1p6xydw3b4bqv7zwmkynlp44yf62ymnc08ai4wacj00l8vgmpkr1")))

(define-public crate-tempus-cli-1.1.0 (c (n "tempus-cli") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (d #t) (k 0)))) (h "10ciq1akh8rnrmr7bm3p4ksl083rcfhyfsmj06fv3ymvg2p3zmzw")))

(define-public crate-tempus-cli-1.2.0 (c (n "tempus-cli") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rlbr47viyk4zras6fw8a0zzsvazgc494abc6w5xq7nnxlsv2zav")))

