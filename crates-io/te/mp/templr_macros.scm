(define-module (crates-io te mp templr_macros) #:use-module (crates-io))

(define-public crate-templr_macros-0.1.0 (c (n "templr_macros") (v "0.1.0") (d (list (d (n "askama_escape") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "templr_parser") (r "^0.1") (d #t) (k 0)))) (h "09fc5mxkba1im37f2m86508pq2jsjpbbxg02cjvb4kwjaxav85zi")))

(define-public crate-templr_macros-0.1.1 (c (n "templr_macros") (v "0.1.1") (d (list (d (n "askama_escape") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "templr_parser") (r "^0.1.1") (d #t) (k 0)))) (h "1a3b886nmx4skc1lin01fxkblmv78jnqvzrxd5sdr2wlb8541vm9")))

(define-public crate-templr_macros-0.1.2 (c (n "templr_macros") (v "0.1.2") (d (list (d (n "askama_escape") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "templr_parser") (r "^0.1.1") (d #t) (k 0)))) (h "0m2m35pn7wwi1svjhjkrg119i8xrcgxr21brx56pk462rgwcy9ik")))

(define-public crate-templr_macros-0.1.3 (c (n "templr_macros") (v "0.1.3") (d (list (d (n "askama_escape") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "templr_parser") (r "^0.1.2") (d #t) (k 0)))) (h "0sis97xs2izg0k87jid00mrx86s01qvr9bm2jjfffqilf44a552h")))

(define-public crate-templr_macros-0.1.4 (c (n "templr_macros") (v "0.1.4") (d (list (d (n "askama_escape") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "templr_parser") (r "^0.1.2") (d #t) (k 0)))) (h "0jz56a39zdji3qmf3cg0sp8iwck197izvlidcah91xv4ps0l6pg7")))

(define-public crate-templr_macros-0.2.0 (c (n "templr_macros") (v "0.2.0") (d (list (d (n "askama_escape") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "templr_parser") (r "^0.2.0") (d #t) (k 0)))) (h "02p56p2lwazvs9rp1fdn7ibkfm587icd3ns6rr88rz512ndvfabc")))

(define-public crate-templr_macros-0.2.1 (c (n "templr_macros") (v "0.2.1") (d (list (d (n "askama_escape") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "templr_parser") (r "^0.2.1") (d #t) (k 0)))) (h "1grmfmw5y4xwxcsxkfjdl4h7dgr5lqz7pq33qa3j6s96x8mfch3h")))

(define-public crate-templr_macros-0.2.2 (c (n "templr_macros") (v "0.2.2") (d (list (d (n "askama_escape") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "templr_parser") (r "^0.2.1") (d #t) (k 0)))) (h "1zkxlaq1f2g0aajqn1dlrr3z2di279l0x9scd0inp19ngflmx8p1")))

