(define-module (crates-io te mp temprs) #:use-module (crates-io))

(define-public crate-temprs-0.2.0 (c (n "temprs") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "12wk59bfiy64flx4jb07pndb5pn8rpi4a3rs4kfqw2071myq56z0") (y #t)))

(define-public crate-temprs-0.2.1 (c (n "temprs") (v "0.2.1") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "178v9z4bdhxg2a50rv3dhkpf7q41glwg267s9jfjzla20s31k46g") (y #t)))

(define-public crate-temprs-0.2.2 (c (n "temprs") (v "0.2.2") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "0ipyh2x1fk08h88a0qlzv69lx14kfmls84zrgjc9y4sw716rbsfa") (y #t)))

(define-public crate-temprs-0.2.3 (c (n "temprs") (v "0.2.3") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1gk5fmj2zaf6x1y0lachhf3icldwf0n79dpi8fyqxv41i76inl85") (y #t)))

(define-public crate-temprs-0.2.4 (c (n "temprs") (v "0.2.4") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1xg9qbaijwk40ljj437fk56siqh9dny30hm1fp7f7fnb3c94p2pc") (y #t)))

(define-public crate-temprs-0.2.5 (c (n "temprs") (v "0.2.5") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "02g98f5k8q4rxbqwmvrbf41v27sb9xcn47mn9pr4q6ns9rmwryfn") (y #t)))

(define-public crate-temprs-0.2.6 (c (n "temprs") (v "0.2.6") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1gw008idwjfa16mss604a8x7jmfpqqg2q6awhrmmcs2jkda7ik4q") (y #t)))

(define-public crate-temprs-0.2.7 (c (n "temprs") (v "0.2.7") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "09d9vfivmnj98vrb5raf78jjkic05ip66ajq4j6wzghnfd0d4gp1") (y #t)))

(define-public crate-temprs-0.2.8 (c (n "temprs") (v "0.2.8") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "0kzlkkf8a14yf6c6mpvbinwb1axxp0r3i6l4665iqnv7gj4gs1vd") (y #t)))

(define-public crate-temprs-0.2.9 (c (n "temprs") (v "0.2.9") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "0v2nqn755sqiaizk7lnjazgadmlyglbcsf1ihz22451hh3h6bpy3") (y #t)))

(define-public crate-temprs-0.3.0 (c (n "temprs") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "0gwfigbzpgvknb51x6fg0d2q1iy9i2rcazmknww9k4g6qcq9lz3b") (y #t)))

(define-public crate-temprs-0.3.1 (c (n "temprs") (v "0.3.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "1mhqi96hrrz7icikyk6n4y7my2lm1ylwiig6bxz3bdqn23hmmydx") (y #t)))

(define-public crate-temprs-0.3.2 (c (n "temprs") (v "0.3.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "00fi99gz7dwp3ky7mizc6wy2azdf34ha8vhbq1ykr2c19zpdi3q9") (y #t)))

(define-public crate-temprs-0.3.3 (c (n "temprs") (v "0.3.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "1z7sn1vm9n5drd29f97g6274n3w58al1kk0c27kph27pq80gg1mk") (y #t)))

(define-public crate-temprs-0.3.4 (c (n "temprs") (v "0.3.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "0bx1yx978dz9l915jr664blhi4cp093fzxs70apxk2qk9bwlnzmw") (y #t)))

(define-public crate-temprs-0.3.5 (c (n "temprs") (v "0.3.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "1zidsrwvallzc3xlg1cxk6v5w35sjffmxaqgy07q3awl7q792ks8") (y #t)))

(define-public crate-temprs-0.4.0 (c (n "temprs") (v "0.4.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "0b7hrd9sm1sc6rvv4d8qkf6qnklq3mfi03d4qpxrqkza5xmixm1r") (y #t)))

(define-public crate-temprs-0.4.1 (c (n "temprs") (v "0.4.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "0720nf7nhs4c485k1g9n34czhsmii5dp8cri4mqfkriyk1xzbdv3") (y #t)))

(define-public crate-temprs-0.4.2 (c (n "temprs") (v "0.4.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "0ixrhqpjpfq68554pg4zmx3nxhs5q1k9rsnsdvyhwl6kv0kwdplq") (y #t)))

(define-public crate-temprs-0.4.3 (c (n "temprs") (v "0.4.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "043zfazanx64jdgabbfy5yy0637rkgd1abb1l54x12r68bzgs2px") (y #t)))

(define-public crate-temprs-0.4.4 (c (n "temprs") (v "0.4.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "17a49kxc02ar4mnqzyw8277zy0xg0a7dnq2sd8hxsny9arn5h2ia") (y #t)))

(define-public crate-temprs-0.4.5 (c (n "temprs") (v "0.4.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "0bs5vdaqlx1zc1i59d7lw0gl6fzy8nzf3wh0cick1lslpksjfihk") (y #t)))

(define-public crate-temprs-0.4.6 (c (n "temprs") (v "0.4.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "00r0g5h1ycfvyw8w2vlxm61l5zypskzmay9b924i6hk6zf8ji44h") (y #t)))

(define-public crate-temprs-0.4.7 (c (n "temprs") (v "0.4.7") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "1vnk2i853cxyn5k8z84q6h0d4gsmzjagmjg34s8gfqivlm01cgw9") (y #t)))

(define-public crate-temprs-0.5.0 (c (n "temprs") (v "0.5.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "00rrk3pw0d43381f6pzk3yvmh7b83qqn05y4c56mzwav5s1whdck") (y #t)))

(define-public crate-temprs-0.5.1 (c (n "temprs") (v "0.5.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "14k5xraxhcppwf96c5adrk5cgnpv4magwi2b5yjxfxw6kghb7y0p") (y #t)))

(define-public crate-temprs-0.5.2 (c (n "temprs") (v "0.5.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "1yvlqga8wkwxw5684ws8sya28gcwsn7xfcix5sjq3006n29i0c54")))

