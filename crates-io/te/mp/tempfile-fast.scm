(define-module (crates-io te mp tempfile-fast) #:use-module (crates-io))

(define-public crate-tempfile-fast-0.1.0 (c (n "tempfile-fast") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.6") (d #t) (k 0)))) (h "03ganwm3yysgv696gsbc1jxrnk8xm5vdg487g1bb44msh9m6xzmx")))

(define-public crate-tempfile-fast-0.1.1 (c (n "tempfile-fast") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.6") (d #t) (k 0)))) (h "04798jng54z0ngwyjyxvgxbafamrc0q9ag5dvynwxcgnkib454gy")))

(define-public crate-tempfile-fast-0.2.0 (c (n "tempfile-fast") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "078d60gqyrfwxip536rg9bc4cmclkxy3m44yi59ng1zdnm38hdkr")))

(define-public crate-tempfile-fast-0.2.1 (c (n "tempfile-fast") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1jfkj1m0bx7dwyvyvs4f6p764l0m1s3ivqrig3frpbf5psbq28fy")))

(define-public crate-tempfile-fast-0.2.2 (c (n "tempfile-fast") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1j73ca3z0a06bpy35hh6crj3pxq7775ivswb3kqkq0svmrws1sdg")))

(define-public crate-tempfile-fast-0.2.3 (c (n "tempfile-fast") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1xqlhk0nj89cjf2m3qjyq53c9q0nasz3x3cais7s7bshhw72q999")))

(define-public crate-tempfile-fast-0.2.4 (c (n "tempfile-fast") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "074xnj9czb5h9cdicgl6fin5540p3sga5c20cmkw6f20lp133ps6")))

(define-public crate-tempfile-fast-0.3.0 (c (n "tempfile-fast") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0xsc5dvgxjf4lkk57im9xyks59jw52wp37bicynpcbk5qkcvslca")))

(define-public crate-tempfile-fast-0.3.1 (c (n "tempfile-fast") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0b3n1g3h71c1isnzif61a847w9hjksw6swg94b8g23nlz0rdmaqq")))

(define-public crate-tempfile-fast-0.3.2 (c (n "tempfile-fast") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "02m77q2gxv632imyxhs4jp5gclnafxizn2v22ldzm1x6b2d96qm3")))

(define-public crate-tempfile-fast-0.3.3 (c (n "tempfile-fast") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0gl8yg6vxvcbmm4sigfj1ghzibnkk7za2dwm4pcmfw15qz482rvk")))

(define-public crate-tempfile-fast-0.3.4 (c (n "tempfile-fast") (v "0.3.4") (d (list (d (n "libc") (r "^0.2.34") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1xksx1l1019k9q0az9mhqsgb14w0vm88yax30iq6178s3d9yhjx7")))

