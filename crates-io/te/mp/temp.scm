(define-module (crates-io te mp temp) #:use-module (crates-io))

(define-public crate-temp-0.1.0 (c (n "temp") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1zv3ly1m704m76r33mqqjyic1fpl2xzf6pdxplp5shwmbkngjcgg") (y #t)))

(define-public crate-temp-0.2.0 (c (n "temp") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "clap") (r "^2.3.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1gmya5gs2akqvfa39qiikngrd5z5p9q7klabmb0kxz49bkca79qf") (y #t)))

(define-public crate-temp-0.1.1 (c (n "temp") (v "0.1.1") (h "1n29h2c0970z7rd0fxvriqbi7kqdac5vm63swnlvbk7fav6lsk95")))

(define-public crate-temp-1.0.0 (c (n "temp") (v "1.0.0") (h "01sim003g90ycwyxb0ysp9ax61cphn0pwm7y1lcd5grcs6df1ky3")))

