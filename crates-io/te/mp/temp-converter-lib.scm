(define-module (crates-io te mp temp-converter-lib) #:use-module (crates-io))

(define-public crate-temp-converter-lib-1.0.0 (c (n "temp-converter-lib") (v "1.0.0") (h "1bpbyg1waqxy1g6ahj9pv00idk25c2s4dg56iv32qda6c6mkmbd1") (r "1.59.0")))

(define-public crate-temp-converter-lib-3.0.2 (c (n "temp-converter-lib") (v "3.0.2") (h "1m2bc5bk1rz04zmblch6yribcmx6kwd3vd2ar37nhic9j5dlnsmg") (r "1.59.0")))

(define-public crate-temp-converter-lib-3.1.1 (c (n "temp-converter-lib") (v "3.1.1") (h "0dy6d95z6wprrhj1v2ww1g0fn3m5l389l4jlhfk67z6a0mx2q5br") (r "1.59.0")))

(define-public crate-temp-converter-lib-3.1.2 (c (n "temp-converter-lib") (v "3.1.2") (d (list (d (n "rstest") (r "^0.19.0") (k 2)))) (h "196jxvd8494xv2pq8vs3dcjccwxlfpvzggz1inbcq55wcpxqz6mk") (r "1.59.0")))

