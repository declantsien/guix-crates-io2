(define-module (crates-io te mp temporenc) #:use-module (crates-io))

(define-public crate-temporenc-0.0.1 (c (n "temporenc") (v "0.0.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1s7gbgc2w11rcpvwdsvsjbyyg04ffi9dk8q5c66asjh4w6l5mk58")))

(define-public crate-temporenc-0.0.2 (c (n "temporenc") (v "0.0.2") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0680r5ag7kyl4hi71inqxs44hc68rsfri5zg465awnkr90lnhwlr")))

(define-public crate-temporenc-0.0.3 (c (n "temporenc") (v "0.0.3") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1s2ql7wp8k85icd86i8k5zfmpkvhiqcnvs40wd4llpddiyxwk8nn")))

