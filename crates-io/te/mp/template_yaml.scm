(define-module (crates-io te mp template_yaml) #:use-module (crates-io))

(define-public crate-template_yaml-0.1.0 (c (n "template_yaml") (v "0.1.0") (d (list (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)))) (h "0j5gbznrw3w0xki7gksbdd4ls7g4m058h4gw9ndfzj2myqbrld1h") (y #t)))

(define-public crate-template_yaml-0.1.1 (c (n "template_yaml") (v "0.1.1") (d (list (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)))) (h "01fxi7fqwsraljbfri82xa4i7j7f41gwixrqkvmzas83pljr3fps")))

