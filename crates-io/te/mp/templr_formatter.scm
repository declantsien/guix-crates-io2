(define-module (crates-io te mp templr_formatter) #:use-module (crates-io))

(define-public crate-templr_formatter-0.1.0 (c (n "templr_formatter") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (k 0)) (d (n "templr_parser") (r "^0.1") (d #t) (k 0)))) (h "0glawc6p53pc5fxlcb1vpq3fcrrn556ackrnc0fkcyj0sh3mw5kh")))

(define-public crate-templr_formatter-0.1.1 (c (n "templr_formatter") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (k 0)) (d (n "templr_parser") (r "^0.1") (d #t) (k 0)))) (h "1sjm0m9758a5xzkdf1fv3p8il00zj1qx5jn13fpmywrdjp3pkl4q")))

