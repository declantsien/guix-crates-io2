(define-module (crates-io te mp tempmail-lol) #:use-module (crates-io))

(define-public crate-tempmail-lol-0.1.0 (c (n "tempmail-lol") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1iws8nbwkixxysy5h7r3q5xw3sbn87hqyld67pp4y3nn1zhcgzqx") (f (quote (("async"))))))

