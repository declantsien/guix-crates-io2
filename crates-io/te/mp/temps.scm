(define-module (crates-io te mp temps) #:use-module (crates-io))

(define-public crate-temps-0.0.3 (c (n "temps") (v "0.0.3") (h "0mxq66a0mjlrvja7m6a4nmcd6ksp7cjx796lc4zijk16ag2rhkp7")))

(define-public crate-temps-0.0.9 (c (n "temps") (v "0.0.9") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "temps-macros") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yxqa64r25hvdiax56agd88m60p65vlwxx815rq2wzv1dikpsjmb")))

(define-public crate-temps-0.0.10 (c (n "temps") (v "0.0.10") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "temps-macros") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gjy3hqxk74w874p0xmis9nlqym1n94b8hlqsw8yl2w48bc1rr3g")))

(define-public crate-temps-0.0.11 (c (n "temps") (v "0.0.11") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "temps-macros") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ds0krwmymbxhn6pwyrdmsc6fhdfqkckgjhnzc25fy3dsf3826as")))

(define-public crate-temps-0.0.12 (c (n "temps") (v "0.0.12") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "temps-macros") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ssg3pdjyz0r784d9v2x3xl8v3f8g8dcrhd7al491a4laidrigh4")))

(define-public crate-temps-0.0.13 (c (n "temps") (v "0.0.13") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)))) (h "02jcpc994jamwmvyzrawlp67j1arkq9fsl2pfnvlm3z6zpyknccl") (f (quote (("default")))) (s 2) (e (quote (("time" "dep:time") ("chrono" "dep:chrono"))))))

(define-public crate-temps-0.0.14 (c (n "temps") (v "0.0.14") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)))) (h "14vm3f8wwl4h4fs8rs6jz42pikqbjizi56x0821w9d0dc4x8l6iq") (f (quote (("default")))) (s 2) (e (quote (("time" "dep:time") ("chrono" "dep:chrono"))))))

(define-public crate-temps-0.0.15 (c (n "temps") (v "0.0.15") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1pia7w6kphphja0kg27adqh6k8dy5dvyb2saivshv57a595fx159") (f (quote (("default")))) (s 2) (e (quote (("time" "dep:time") ("chrono" "dep:chrono"))))))

