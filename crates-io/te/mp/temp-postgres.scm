(define-module (crates-io te mp temp-postgres) #:use-module (crates-io))

(define-public crate-temp-postgres-0.1.0 (c (n "temp-postgres") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("fs" "process" "rt" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-postgres") (r "^0.7.10") (d #t) (k 0)))) (h "1dh5wrjz9pv800mz8yf7amx7n0hsx58x2q75gi8c283h9jh4j9a3")))

