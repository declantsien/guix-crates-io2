(define-module (crates-io te mp tempr) #:use-module (crates-io))

(define-public crate-tempr-1.1.0 (c (n "tempr") (v "1.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1axhg45xvanmp5wyxavbxldjcb8k4q8r90nl85im437vq5b7ly0p")))

(define-public crate-tempr-1.1.1 (c (n "tempr") (v "1.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ggb9y3idhm5ckg9y83knvh4wvf3syby6pvly13i7a8nid1ssc0x")))

(define-public crate-tempr-1.1.2 (c (n "tempr") (v "1.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1912ymiyvb2b4phzrga65m15xfs4734qr12l0pn6m1kf7s2awqx9")))

(define-public crate-tempr-1.1.3 (c (n "tempr") (v "1.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1anczazap5lw7hhlp21hmkpdwgd4a4l195j6f01hym7an6xl6b7n")))

(define-public crate-tempr-1.2.0 (c (n "tempr") (v "1.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vpaj5y4slrxmf3227ax9xw0dh9yin8nic7ziasswlvp6z5420gx")))

