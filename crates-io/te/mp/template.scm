(define-module (crates-io te mp template) #:use-module (crates-io))

(define-public crate-template-0.1.0 (c (n "template") (v "0.1.0") (d (list (d (n "handlebars") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "notify") (r "^2.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qmikjg6l0jrd7g2ym015c2cvsyizqahw395cb5my6j2j0fn8vdk")))

