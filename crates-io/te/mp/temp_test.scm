(define-module (crates-io te mp temp_test) #:use-module (crates-io))

(define-public crate-temp_test-0.1.0 (c (n "temp_test") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "15q07nl9w84j9k755m89amxv3vkfvicngbbzjngx4ms6g7dr60c2")))

(define-public crate-temp_test-0.1.1 (c (n "temp_test") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "09hw7j7gyvrx7rridgwnqq4ml2scbkyfsgfzglz7nx9zglkdnr2h")))

