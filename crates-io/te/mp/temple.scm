(define-module (crates-io te mp temple) #:use-module (crates-io))

(define-public crate-temple-0.0.0 (c (n "temple") (v "0.0.0") (d (list (d (n "enumn") (r "^0.1.2") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1f4v0vnk388ps8751d38yzxyzklv79520qly1sdgpvayva5vbsjn")))

