(define-module (crates-io te mp tempergb) #:use-module (crates-io))

(define-public crate-tempergb-0.1.0 (c (n "tempergb") (v "0.1.0") (h "16a606lchn0sbi4m5b1a6klq6633aavryrxizmalznnnw8w1amb2")))

(define-public crate-tempergb-0.1.1 (c (n "tempergb") (v "0.1.1") (h "041rv41qv8dgkbllmgcjf8zcvhsn7qb6h09zgsm6l2wkrq9qqhig")))

(define-public crate-tempergb-0.1.2 (c (n "tempergb") (v "0.1.2") (h "0gw2iyi71x7rah2h2wh5aqvm8wlrbd7wx7i5ldgvc3f9z5vxw6v8")))

