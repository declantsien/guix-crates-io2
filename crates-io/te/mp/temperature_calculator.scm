(define-module (crates-io te mp temperature_calculator) #:use-module (crates-io))

(define-public crate-temperature_calculator-1.0.0 (c (n "temperature_calculator") (v "1.0.0") (h "1j3s7aac4kh8v6bcpjrbdsp4562dp6488gmi3iarwipf8ajxcal7")))

(define-public crate-temperature_calculator-1.1.0 (c (n "temperature_calculator") (v "1.1.0") (h "0665qjvzjcvrfggk87ymxwwz6w2wss01d6q5vaslvfkrv0m0br03")))

(define-public crate-temperature_calculator-1.1.1 (c (n "temperature_calculator") (v "1.1.1") (h "1wkkiq5ys6f0sv60ki43zslmm8k6jgcvamig5l6662nv0rfpna4i")))

