(define-module (crates-io te mp temps-macros) #:use-module (crates-io))

(define-public crate-temps-macros-0.0.7 (c (n "temps-macros") (v "0.0.7") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "03hivqch4x4xxjzda6h2hqyi2wp4f6qari6kabpm3iv82ab1bkyi")))

(define-public crate-temps-macros-0.0.8 (c (n "temps-macros") (v "0.0.8") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1qcsrb04yghzbvbipykmml7ggr8v2wsf4jd4r5i9q5cg19sfc0q4")))

(define-public crate-temps-macros-0.0.9 (c (n "temps-macros") (v "0.0.9") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0z2j4zbvzc3gca4xslbpack3vl9qc23ccs3dzv9hsgjrq52ds973")))

(define-public crate-temps-macros-0.0.10 (c (n "temps-macros") (v "0.0.10") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1am0mjsj62d4ql5cddaa9l4asvzzv763vz070gpcci9nwjdmm1gj")))

(define-public crate-temps-macros-0.0.11 (c (n "temps-macros") (v "0.0.11") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0parjqjb33p8h9hkcadawmy6zb5xxmj0lii8hwvcbmjhd6fbq8dz")))

(define-public crate-temps-macros-0.0.12 (c (n "temps-macros") (v "0.0.12") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0m5yr8p25jwpsr562rjcmf95xj163qhv4fb0pf1pfrwad6753q5l")))

