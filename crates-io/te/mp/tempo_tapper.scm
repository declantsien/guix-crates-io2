(define-module (crates-io te mp tempo_tapper) #:use-module (crates-io))

(define-public crate-tempo_tapper-0.3.1 (c (n "tempo_tapper") (v "0.3.1") (h "0zaivcp4i197pd8fgymyd205zr605xb4b650jqdwpy26glfhz3m5") (r "1.61.0")))

(define-public crate-tempo_tapper-0.3.2 (c (n "tempo_tapper") (v "0.3.2") (h "1m5f1fp1nvqblxf9dgxv4qpfwh8j1nqp1cmgrbafs2mjlxg9fyb6") (r "1.61.0")))

(define-public crate-tempo_tapper-0.4.0 (c (n "tempo_tapper") (v "0.4.0") (h "1dasvkcv3zmjbyrlywqhi3v9962h3hq2yvf9s18ip4jpjcsmi62a") (r "1.61.0")))

(define-public crate-tempo_tapper-0.4.1 (c (n "tempo_tapper") (v "0.4.1") (h "1gc853f5w3kd9wh41p2vaza680mj1af2inx03imlxk1rykjjqzcx") (r "1.61.0")))

(define-public crate-tempo_tapper-0.5.0 (c (n "tempo_tapper") (v "0.5.0") (h "0clbpg6pq0abp01p0qiczgcipyn15g26bcrhclghnbp5xvrs65l1") (r "1.61.0")))

(define-public crate-tempo_tapper-0.5.1 (c (n "tempo_tapper") (v "0.5.1") (h "1q5lb0b7bysxn830z8ijqrwdgbkcwkaqssfqwgrwbhcy8gw70k52") (r "1.61.0")))

(define-public crate-tempo_tapper-0.5.2 (c (n "tempo_tapper") (v "0.5.2") (h "0r1br6km9cvyf8allixrcprrg3v9wddklk14yj4cxi3gwklb4frg") (r "1.61.0")))

(define-public crate-tempo_tapper-0.5.3 (c (n "tempo_tapper") (v "0.5.3") (h "0v13skw5vr6l49bp4218lckjrhx5zdbxnkwl9qhlyx5dasrriag2") (r "1.61.0")))

(define-public crate-tempo_tapper-0.5.4 (c (n "tempo_tapper") (v "0.5.4") (h "04fbxsfl3mbfpg90x9phjz84kssrvbpg0y72903dwamgi8nm0q42") (r "1.61.0")))

(define-public crate-tempo_tapper-0.5.5 (c (n "tempo_tapper") (v "0.5.5") (h "1jnn3anb453bvvzns69xnrh9ixlqs5faz7r4pqf0g1z868xc80ij") (r "1.61.0")))

