(define-module (crates-io te mp tempus) #:use-module (crates-io))

(define-public crate-tempus-0.1.0 (c (n "tempus") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0kifsvq1xa302nhgrmnby36zp1bpf71y4qhah23af6n0d6db9z30")))

(define-public crate-tempus-0.2.0 (c (n "tempus") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0r834n8v1nmwv9qbs8vlg5f54adnbsc2w7nr2qws9g2lk6dxc7r7")))

(define-public crate-tempus-0.2.1 (c (n "tempus") (v "0.2.1") (d (list (d (n "idem") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0fg04a37rdp6y9cbfk9g1g30ilkymk4a0ndb1wln8gnjgi276fsz")))

(define-public crate-tempus-0.2.2 (c (n "tempus") (v "0.2.2") (d (list (d (n "idem") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "10b7kv3zzn55vwxh2pa4q3szr70j9q28gzqhdk1pzm3qsjxlsy5q")))

(define-public crate-tempus-0.2.3 (c (n "tempus") (v "0.2.3") (d (list (d (n "idem") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "033da04p243xmvin1sh89bb3nnb1w6661v4k0163cqhnnr1xfpkl")))

(define-public crate-tempus-0.2.4 (c (n "tempus") (v "0.2.4") (d (list (d (n "idem") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (o #t) (d #t) (k 0) (p "libc-interface")) (d (n "system-call") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0j0q33cbnilcsfl00p4w5k0xgz9iiacsw5zhlm748rl8m0l17khc") (f (quote (("posix" "libc" "system-call"))))))

