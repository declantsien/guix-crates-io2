(define-module (crates-io te mp tempera) #:use-module (crates-io))

(define-public crate-tempera-0.1.0 (c (n "tempera") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1gfipw5shjgx90jxzhwn8dsb6xlz6gypk9kki9mmr2h5lj0i5xr9")))

(define-public crate-tempera-0.1.1 (c (n "tempera") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0ywa61rbs1ziynrsbxnyfg0qcqbwq422ljgsxs85h4j68ryxf4x4")))

