(define-module (crates-io te mp temp-env) #:use-module (crates-io))

(define-public crate-temp-env-0.1.0 (c (n "temp-env") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0gln0jd349k4p86zvi42hk5d25zdjv5nk5hvr4kp5s5729mh4wk9")))

(define-public crate-temp-env-0.2.0 (c (n "temp-env") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0mxbfwcrbzgplf5ixs4n2xsl3pqazna25h2kfjwc9y6xq8v72425")))

(define-public crate-temp-env-0.3.0 (c (n "temp-env") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1k0h0gqqzchmrbrv8yhxa836r9prchl51yav8bvsnvwdv2rlp6qk")))

(define-public crate-temp-env-0.3.1 (c (n "temp-env") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.21") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0rgd6dby32phksbpwfagnj8drqcjs318r4kv7npvdyvpkwslh3d3") (f (quote (("default")))) (s 2) (e (quote (("async_closure" "dep:tokio" "dep:futures"))))))

(define-public crate-temp-env-0.3.2 (c (n "temp-env") (v "0.3.2") (d (list (d (n "futures") (r "^0.3.21") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1hszw55ncpzx1ngakczzb84yb9l30hsvhk1rgkwxq5gy8bz0f34c") (f (quote (("default")))) (s 2) (e (quote (("async_closure" "dep:tokio" "dep:futures"))))))

(define-public crate-temp-env-0.3.3 (c (n "temp-env") (v "0.3.3") (d (list (d (n "futures") (r "^0.3.21") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 2)))) (h "108n6qm6hr66hvnay1kd3p4z6h40kq177yr1486mlgll7ls5psay") (f (quote (("default")))) (s 2) (e (quote (("async_closure" "dep:futures"))))))

(define-public crate-temp-env-0.3.4 (c (n "temp-env") (v "0.3.4") (d (list (d (n "futures") (r "^0.3.21") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0p51s60wpx90cds2f6nks9j8vjp6i9yhij662naxgjsjzr5l8iwm") (f (quote (("default")))) (s 2) (e (quote (("async_closure" "dep:futures")))) (r "1.62.1")))

(define-public crate-temp-env-0.3.5 (c (n "temp-env") (v "0.3.5") (d (list (d (n "futures") (r "^0.3.21") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0sk5imj1q63lnzx6rh85mcfkrw0005bwfn66j08k389y3ydl4470") (f (quote (("default")))) (s 2) (e (quote (("async_closure" "dep:futures")))) (r "1.62.1")))

(define-public crate-temp-env-0.3.6 (c (n "temp-env") (v "0.3.6") (d (list (d (n "futures") (r "^0.3.21") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0l7hpkd0nhiy4w70j9xbygl1vjr9ipcfxii164n40iwg0ralhdwn") (f (quote (("default")))) (s 2) (e (quote (("async_closure" "dep:futures")))) (r "1.62.1")))

