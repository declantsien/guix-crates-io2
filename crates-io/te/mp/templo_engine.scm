(define-module (crates-io te mp templo_engine) #:use-module (crates-io))

(define-public crate-templo_engine-0.1.0 (c (n "templo_engine") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1s9py0c9qci534bv7f6b0rb1w9hvq5iq6d4qy7w8pg9lan9h78i0") (y #t)))

(define-public crate-templo_engine-0.1.1 (c (n "templo_engine") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1qxw6l7712qg321a26r3xir84sgix33zch26kjizq0jnid6v93q7") (y #t)))

(define-public crate-templo_engine-0.1.2 (c (n "templo_engine") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1hflyyp6x64fkmh72934phxwpga7zagkpv0fnzg5qhmvfxgi3w7p")))

(define-public crate-templo_engine-0.1.3 (c (n "templo_engine") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0pd2x3k369l9by07x972k0mqzz9lzmwhqvj5521ax0cn9nx4ii0g") (y #t)))

(define-public crate-templo_engine-0.1.4 (c (n "templo_engine") (v "0.1.4") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "13b982z6jmzgqlbyy1fj40p3lfpwsnw5ima7rqn9wm7qiq4qjkik") (y #t)))

(define-public crate-templo_engine-0.1.5 (c (n "templo_engine") (v "0.1.5") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0kq0q7qinncvgmqs2i2nr5a5sjj6046rwz4pqqzlxx6di3sc7w98")))

(define-public crate-templo_engine-0.2.0 (c (n "templo_engine") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "17appqmrsvx954jp99bgpgpd7idlsyhd11q17nd3ayc742zx4gp6")))

