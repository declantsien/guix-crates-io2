(define-module (crates-io te mp tempel) #:use-module (crates-io))

(define-public crate-tempel-0.1.0 (c (n "tempel") (v "0.1.0") (d (list (d (n "hyper") (r "^0.6.11") (d #t) (k 0)))) (h "0kjj4gp7a2fblly3rsc430761mbxjhdl6j51r3sjgsdf7p6r545h")))

(define-public crate-tempel-0.1.1 (c (n "tempel") (v "0.1.1") (d (list (d (n "hyper") (r "^0.6.11") (d #t) (k 0)) (d (n "url") (r "^0.2.37") (d #t) (k 0)))) (h "1k9r5n5p6yzdyy1y92jm2c0qv65zdk9ahfnqg24jm3pbp5vcwrsw")))

