(define-module (crates-io te mp templrfmt) #:use-module (crates-io))

(define-public crate-templrfmt-0.1.0 (c (n "templrfmt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rustfmt-wrapper") (r "^0.2") (d #t) (k 0)) (d (n "templr_formatter") (r "^0.1") (d #t) (k 0)))) (h "0jmp1jq6yp2hwjmlvf3kdm0wyyd7f71j388fqqxm33pq7by3h3jm")))

(define-public crate-templrfmt-0.1.1 (c (n "templrfmt") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rustfmt-wrapper") (r "^0.2") (d #t) (k 0)) (d (n "templr_formatter") (r "^0.1.0") (d #t) (k 0)))) (h "0cb11jmfb4gad4m0yswwra49cc35hz44s5lxwcha1ldmdsldcmf7")))

(define-public crate-templrfmt-0.1.2 (c (n "templrfmt") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rustfmt-wrapper") (r "^0.2") (d #t) (k 0)) (d (n "templr_formatter") (r "^0.1.1") (d #t) (k 0)))) (h "0lvh88a67kgrv088vvnnplm8s6722kk3z7ga6xy3bp9nb55xaxq1")))

