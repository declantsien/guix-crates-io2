(define-module (crates-io te mp temp-conv) #:use-module (crates-io))

(define-public crate-temp-conv-0.1.0 (c (n "temp-conv") (v "0.1.0") (h "0m12217a7h11qgyvqq21k3fkd1v37pyg92sn1dp964p3jsaz37xr")))

(define-public crate-temp-conv-0.1.1 (c (n "temp-conv") (v "0.1.1") (h "1qakw55qyrf8fxbld8yyfavzg5ipvfg5i6w93gpsyb02kihgavxb")))

(define-public crate-temp-conv-0.1.2 (c (n "temp-conv") (v "0.1.2") (h "16q9p1dm6wv5l7gf7la2fk22bp3wlqsgqliyg36yxrb655527n9w")))

(define-public crate-temp-conv-0.2.0 (c (n "temp-conv") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k8g76xxp7zv2ib8kjp1hkjl4ya9imvzr9ysv7y92yrbph5xv09j")))

(define-public crate-temp-conv-0.2.1 (c (n "temp-conv") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wqjy95v1l40nlrcl1h427w1vfmnbi065sbxq5kqc8gqpbywf93d")))

