(define-module (crates-io te mp template-nest) #:use-module (crates-io))

(define-public crate-template-nest-0.1.0 (c (n "template-nest") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0rvdwj2am3m1c3z0p9cws8wcvyqwzw6bjj8h14f3idlr0i4yggbv") (r "1.65")))

(define-public crate-template-nest-0.2.0 (c (n "template-nest") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l8yk92qqlhxz5nwflbxa4y5p0a0xjm82qdq97dqp1569q578751") (r "1.65")))

(define-public crate-template-nest-0.2.1 (c (n "template-nest") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p7f8l3vgyk6yq102d2a3szfh5p9jj0rggv8r37a9pq98v1nab86") (y #t) (r "1.65")))

(define-public crate-template-nest-0.2.2 (c (n "template-nest") (v "0.2.2") (d (list (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cc1blqg0y89ky3c6894qra5cxyyfg42xbsf8b4622ha11r9mnj5") (r "1.65")))

