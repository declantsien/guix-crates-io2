(define-module (crates-io te mp tempest-source-mock) #:use-module (crates-io))

(define-public crate-tempest-source-mock-0.1.0 (c (n "tempest-source-mock") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempest") (r "^0.1.0") (d #t) (k 0)))) (h "065r9j812v22qp09hhfwvrj0bl3n4wam3bjkhnky73hfhrd4sbrs")))

(define-public crate-tempest-source-mock-0.1.1 (c (n "tempest-source-mock") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempest-source") (r "= 0.1.1") (d #t) (k 0)))) (h "0iq97ijvp54f6fivdbc25f9im98x5xpiyri4iwvdb8w1s2c0g3zk")))

