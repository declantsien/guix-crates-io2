(define-module (crates-io te mp temperature) #:use-module (crates-io))

(define-public crate-temperature-0.0.8 (c (n "temperature") (v "0.0.8") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.7") (d #t) (k 0)) (d (n "matrix") (r "^0.0.8") (d #t) (k 0)))) (h "16advy26idij8czanh30wlrb3k1p32kx1ispk6g3q782fwy59gi0")))

(define-public crate-temperature-0.0.9 (c (n "temperature") (v "0.0.9") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.8") (d #t) (k 0)) (d (n "matrix") (r "^0.0.9") (d #t) (k 0)))) (h "19vskvkk6mcr78j2wgax1iajb4ngq69b485lxm7j9g0shrm5fmkl")))

(define-public crate-temperature-0.0.10 (c (n "temperature") (v "0.0.10") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.8") (d #t) (k 0)) (d (n "matrix") (r "^0.0.9") (d #t) (k 0)))) (h "1k7cr9zfz7v918fd62kch3aqi4jki49a1lbhwg0xsy1d0p2g8r2c")))

(define-public crate-temperature-0.0.11 (c (n "temperature") (v "0.0.11") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.8") (d #t) (k 0)) (d (n "matrix") (r "^0.0.9") (d #t) (k 0)))) (h "15fr67g9rx82php68mgz2vchzhddjwk2k9pg18salxn28aigsry4")))

(define-public crate-temperature-0.0.12 (c (n "temperature") (v "0.0.12") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.9") (d #t) (k 0)) (d (n "matrix") (r "^0.0.9") (d #t) (k 0)))) (h "1j8am76i44jsy49k57cbyffwgs5fniqaysvkz38xiyw48fg1d0sm")))

(define-public crate-temperature-0.0.13 (c (n "temperature") (v "0.0.13") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.10") (d #t) (k 0)) (d (n "matrix") (r "^0.0.11") (d #t) (k 0)))) (h "19s4jfn1xy359y15d1cp0rjn9nymgrzvh271qx0fv5is18ll9lnf")))

(define-public crate-temperature-0.0.14 (c (n "temperature") (v "0.0.14") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.13") (d #t) (k 0)) (d (n "matrix") (r "^0.0.13") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1l7r7na20f5w3xzrb25vh76ic51js3zabmbvsflc1cbk13kbhfhk")))

(define-public crate-temperature-0.0.15 (c (n "temperature") (v "0.0.15") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.15") (d #t) (k 0)) (d (n "matrix") (r "^0.0.13") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1i0vs036ij6qr18lnc0ngzf2ylh89qilhmhlp5p12d1ap2hwwwy2")))

(define-public crate-temperature-0.0.16 (c (n "temperature") (v "0.0.16") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.16") (d #t) (k 0)) (d (n "matrix") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "02w2f0mabs1cxzl36n2hbbl9s4ikaqzigq5695j6whhcwjcivhkm")))

(define-public crate-temperature-0.0.17 (c (n "temperature") (v "0.0.17") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.19") (d #t) (k 0)) (d (n "matrix") (r "^0.0.15") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0ncszxxa332ggkqfd00rir2wma2vxllq55c9m67vymwbbdb4gynp")))

(define-public crate-temperature-0.1.0 (c (n "temperature") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "*") (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1mw60acwl6qa5hng3z1yg9s8alvmsfj0fl5ffa5dva5hfc0bgnbz")))

(define-public crate-temperature-0.2.0 (c (n "temperature") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "*") (d #t) (k 0)) (d (n "linear") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "1xyd3z9pwq9p03s7mrsj65k55y3h7f8fq8c40nnblvd6pb9l8v7w")))

(define-public crate-temperature-0.3.0 (c (n "temperature") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "linear") (r "*") (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)) (d (n "threed-ice") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0x1n4fdy63xvlgs8h8n4m4iircrs4iqlidcry271afy8bv7443w3") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.4.0 (c (n "temperature") (v "0.4.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)) (d (n "threed-ice") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1lf7zj72h5x9995j4n0b60i1mh7w8nzxykqxb26mlamidg12mfr3") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.4.1 (c (n "temperature") (v "0.4.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)) (d (n "threed-ice") (r "^0.9") (o #t) (d #t) (k 0)))) (h "04yk0iqm21a7l9xkll7iycas3ag4pzk9qblik0lxcg2vxc6zdkng") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.5.0 (c (n "temperature") (v "0.5.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)) (d (n "threed-ice") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0s13vpb4gp4p7ngsi98wmil5z8qdwwpin2qx61jj6gr6vxdhnf2v") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.6.0 (c (n "temperature") (v "0.6.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)) (d (n "threed-ice") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0y740gmmagsxgjlcyy0q90s2igq2qxpywdj10w79w16j3x9gx4pj") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.6.1 (c (n "temperature") (v "0.6.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)) (d (n "threed-ice") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0pjph2vl61ccd57lsknrpsgk9b2appaqya8j8i37n06k9w8b55ww") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.7.0 (c (n "temperature") (v "0.7.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)) (d (n "threed-ice") (r "^0.10") (o #t) (d #t) (k 0)))) (h "06zq3idyyyimrkahcg089ixwppbf0z118ilywiprvg8a5fiw4gm7") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.7.1 (c (n "temperature") (v "0.7.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)) (d (n "threed-ice") (r "^0.10") (o #t) (d #t) (k 0)))) (h "02kva9ggyb9qaajkqljn93547wpb30d9zlclqdacf3yk5jh0v136") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.8.0 (c (n "temperature") (v "0.8.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)) (d (n "threed-ice") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1x7ghzrh0xsf70sx193sv0dps92bcn4jc7cclx25r75yk1i50fa4") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.9.0 (c (n "temperature") (v "0.9.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "hotspot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)) (d (n "threed-ice") (r "^0.10") (o #t) (d #t) (k 0)))) (h "11mnsippl9wylzcqk06hmlxyvfmclg7ljvys6h34594xqav0jz98") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.9.1 (c (n "temperature") (v "0.9.1") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "hotspot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "matrix") (r "^0.20") (d #t) (k 0)) (d (n "random") (r "^0.9") (d #t) (k 2)) (d (n "threed-ice") (r "^0.11") (o #t) (d #t) (k 0)))) (h "12dxd64fdagm6gkh298ywchn90srw9681lmgi5sb13xl2apr4bbb") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.9.2 (c (n "temperature") (v "0.9.2") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "hotspot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "matrix") (r "^0.21") (d #t) (k 0)) (d (n "random") (r "^0.9") (d #t) (k 2)) (d (n "threed-ice") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0c6kh0narpi2myjzr9cyjk9sa16ylyiqy2g91jxvlvdib4rcq8bh") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.10.0 (c (n "temperature") (v "0.10.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "hotspot") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "matrix") (r "^0.21") (d #t) (k 0)) (d (n "random") (r "^0.10") (d #t) (k 2)) (d (n "threed-ice") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1yf626mzdba3m78vv0c5jnkdb7fsysim47cnvfwwypzxhfn2ac68") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.10.1 (c (n "temperature") (v "0.10.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "hotspot") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "matrix") (r "^0.21") (d #t) (k 0)) (d (n "random") (r "^0.11") (d #t) (k 2)) (d (n "threed-ice") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1a04gdrrmpglghxv97b88xp6xb65k79f6g76130f17sp5l4vbism") (f (quote (("default" "hotspot" "threed-ice"))))))

(define-public crate-temperature-0.10.2 (c (n "temperature") (v "0.10.2") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "hotspot") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "matrix") (r "^0.21") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "threed-ice") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0xxxj5bmfm9v6iwgh9p43pni03xd6v9vgssxkaghwlalav1m9xcl") (f (quote (("default" "hotspot" "threed-ice"))))))

