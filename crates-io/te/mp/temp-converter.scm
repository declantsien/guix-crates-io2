(define-module (crates-io te mp temp-converter) #:use-module (crates-io))

(define-public crate-temp-converter-1.0.4 (c (n "temp-converter") (v "1.0.4") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)))) (h "008dmsfs901ls87kcjdvph0n3j825nq7n225mxrqab9qsnl1c8m1") (y #t)))

(define-public crate-temp-converter-1.1.0 (c (n "temp-converter") (v "1.1.0") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)))) (h "11ysdm1ys787x1gx5axcnkfrr4icm9avw2h3w20acpm1bxdar0ca") (y #t)))

(define-public crate-temp-converter-1.1.2 (c (n "temp-converter") (v "1.1.2") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)))) (h "03v5zdj3yygq8450yv8y3316g5h5zh18aj1mssdp41s7a720ci75") (y #t)))

(define-public crate-temp-converter-1.1.3 (c (n "temp-converter") (v "1.1.3") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)))) (h "0fxaflf24pbmk1h9a04piydksqf73jasb4j4f5v99bxnfyx53hbm")))

(define-public crate-temp-converter-1.1.4 (c (n "temp-converter") (v "1.1.4") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)))) (h "1rvix4839qdflif0zzjcw5viq3rdd75ba9x9gwp64zmyjvhr23lq")))

(define-public crate-temp-converter-1.1.5 (c (n "temp-converter") (v "1.1.5") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)))) (h "1jjzznbp6i82sl19ljh43bs7594lhfm0wsfymailyxg7zb1jj1z1")))

(define-public crate-temp-converter-2.0.9 (c (n "temp-converter") (v "2.0.9") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.3.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "191h22nfasyqkb6dav919dgq4clrncklg5l86hd090hy2wbvnzih")))

(define-public crate-temp-converter-2.0.10 (c (n "temp-converter") (v "2.0.10") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.3.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z9n7ygigq57n2hgimmgih03cp6cq4yy8rdanrshdxjvdzrk4mvs") (r "1.70.0")))

(define-public crate-temp-converter-2.0.11 (c (n "temp-converter") (v "2.0.11") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qgcdvi6w59ix0n286ny2q4bi5w61s71ga2bnr1hhazmq2g5zxmx") (r "1.70.0")))

(define-public crate-temp-converter-2.0.12 (c (n "temp-converter") (v "2.0.12") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cgpis0q86c5nwv82k4zihhcmxbc1lh236q3g04ysqkg37dcmyb6") (r "1.70.0")))

(define-public crate-temp-converter-2.0.13 (c (n "temp-converter") (v "2.0.13") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ay3gb6388hn3c8draqif84986fj4y0nlrvaahgrx0nwj4walbzf") (r "1.70.0")))

(define-public crate-temp-converter-2.0.14 (c (n "temp-converter") (v "2.0.14") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fv6481i84w20paidm141ds4pkh4lir55l9iaw15xi9m42zn92gp") (r "1.70.0")))

(define-public crate-temp-converter-2.0.15 (c (n "temp-converter") (v "2.0.15") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jgpm19qgby03hiy44idpkk8avczq940cnhdl9gwi1cl1zxy0hzl") (r "1.70.0")))

(define-public crate-temp-converter-3.0.0 (c (n "temp-converter") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "temp-converter-lib") (r "^1.0.0") (d #t) (k 0)))) (h "0aymy7b8lvr8rqqpj1zhxby2gxg8ln69919zffbz6km560q88wsj")))

(define-public crate-temp-converter-3.0.2 (c (n "temp-converter") (v "3.0.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "temp-converter-lib") (r "^3.0.1") (d #t) (k 0)))) (h "032pkdw1zhydfc38hc6kgq55gjh0lh8vlc0mvc1j0fz5v5jji5xz")))

(define-public crate-temp-converter-3.1.1 (c (n "temp-converter") (v "3.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "temp-converter-lib") (r "^3.1.0") (d #t) (k 0)))) (h "1r8nkb4hmkcqr9ca4mpbh6gpk09zslqq53hnhypxr6d63g32r3d5")))

(define-public crate-temp-converter-3.1.2 (c (n "temp-converter") (v "3.1.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "temp-converter-lib") (r "^3.1.2") (d #t) (k 0)))) (h "1arj80f9f27q2q8gjxqdp71vzr0fscgw7b3lhgrn59gdr7p7wxsd")))

(define-public crate-temp-converter-3.1.3 (c (n "temp-converter") (v "3.1.3") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "temp-converter-lib") (r "^3.1.2") (d #t) (k 0)))) (h "03baaqh2sj364klz4dl0i68ccpl12zxhq7p25fanrv1qmwahvgrm")))

