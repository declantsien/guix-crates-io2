(define-module (crates-io te mp temporal_core) #:use-module (crates-io))

(define-public crate-temporal_core-0.0.1 (c (n "temporal_core") (v "0.0.1") (d (list (d (n "icu_calendar") (r "^0.5.0") (d #t) (k 0)) (d (n "parse-zoneinfo") (r "^0.3") (d #t) (k 1)) (d (n "phf") (r "^0.10") (f (quote ("uncased"))) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("uncased"))) (k 1)) (d (n "phf_codegen") (r "^0.10") (k 1)) (d (n "uncased") (r "^0.9") (k 0)) (d (n "uncased") (r "^0.9") (k 1)))) (h "1nn2mxvjdz8mxrl34rv4chvjj31brnlcjqm56clkq5xyv2l6vr7m")))

