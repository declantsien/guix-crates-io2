(define-module (crates-io te mp tempdir) #:use-module (crates-io))

(define-public crate-tempdir-0.1.0 (c (n "tempdir") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "18bawnpcyrckl25hw52iy47rzw882fh3g1nqk9hbjgzplkr70yvp")))

(define-public crate-tempdir-0.2.0 (c (n "tempdir") (v "0.2.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0h542238f36wvngj6aclisybabjqnaz3zwjy6jrnwzd7nxfbf3c9")))

(define-public crate-tempdir-0.2.1 (c (n "tempdir") (v "0.2.1") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "19c2walrf7rgbgck26dq4j0byqb1sravja98cp172hsgyayy0f7i")))

(define-public crate-tempdir-0.3.0 (c (n "tempdir") (v "0.3.0") (d (list (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "144v056db6k3g7ac9xwrn45wknvb6vqa42a2kjs71c8iljzv4yil")))

(define-public crate-tempdir-0.3.1 (c (n "tempdir") (v "0.3.1") (d (list (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "1nv25c2jay9wgp750yiz7naz2y4j5s6f8615hfsjglngb24q98wb")))

(define-public crate-tempdir-0.3.2 (c (n "tempdir") (v "0.3.2") (d (list (d (n "rand") (r "^0.2") (d #t) (k 0)))) (h "0cymwbhkjg9kknm8dmi5cpvgn6sbhp14a5mclr7aiar3gawnigb0")))

(define-public crate-tempdir-0.3.3 (c (n "tempdir") (v "0.3.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0xhkvmhs2z7qxsiw934w8qb0d93lxgs3idwhmxagb6zp52mq7c1m")))

(define-public crate-tempdir-0.3.4 (c (n "tempdir") (v "0.3.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1aas7bqwf6c46r7zp282akixk0dqpbw38b3602bmbkcn7wx96qhb")))

(define-public crate-tempdir-0.3.5 (c (n "tempdir") (v "0.3.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1mij45kgzflkja0h8q9avrik76h5a0b60m9hfd6k9yqxbiplm5w7")))

(define-public crate-tempdir-0.3.6 (c (n "tempdir") (v "0.3.6") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.3") (d #t) (k 0)))) (h "0khk3ry8ivm75vj0cq8snfkpy3l3g5hajkppmqjcnjy1d3dyngpp")))

(define-public crate-tempdir-0.3.7 (c (n "tempdir") (v "0.3.7") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.5") (d #t) (k 0)))) (h "1n5n86zxpgd85y0mswrp5cfdisizq2rv3la906g6ipyc03xvbwhm")))

