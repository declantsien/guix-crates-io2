(define-module (crates-io te mp template_distribution_sv2) #:use-module (crates-io))

(define-public crate-template_distribution_sv2-0.1.0 (c (n "template_distribution_sv2") (v "0.1.0") (d (list (d (n "binary_sv2") (r "^0.1.0") (d #t) (k 0)) (d (n "const_sv2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "06hv62y4n0i9w8ib0znr60a121xgd1ya3cw1j9a2y2fcb82rvdsv") (f (quote (("with_serde" "binary_sv2/with_serde" "serde"))))))

(define-public crate-template_distribution_sv2-0.1.1 (c (n "template_distribution_sv2") (v "0.1.1") (d (list (d (n "binary_sv2") (r "^0.1.2") (d #t) (k 0)) (d (n "const_sv2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "1h6ijdkv5bg36064y8154hr60idm8bg902pb4489wdiwg5n61cq7") (f (quote (("with_serde" "binary_sv2/with_serde" "serde"))))))

(define-public crate-template_distribution_sv2-0.1.3 (c (n "template_distribution_sv2") (v "0.1.3") (d (list (d (n "binary_sv2") (r "^0.1.3") (d #t) (k 0)) (d (n "const_sv2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "10fffirrxaqsgf8ajwvfkd9lzay2mnlk0l5vh3b1cqjg3aw90mxd") (f (quote (("with_serde" "binary_sv2/with_serde" "serde"))))))

(define-public crate-template_distribution_sv2-0.1.4 (c (n "template_distribution_sv2") (v "0.1.4") (d (list (d (n "binary_sv2") (r "0.1.*") (d #t) (k 0)) (d (n "const_sv2") (r "0.1.*") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "1acw2pw3qhwhwp0y9mk0wg6f1liq1z0pagzf9daa2h12yjgqw2sc") (f (quote (("with_serde" "binary_sv2/with_serde" "serde") ("prop_test" "quickcheck"))))))

(define-public crate-template_distribution_sv2-0.1.6 (c (n "template_distribution_sv2") (v "0.1.6") (d (list (d (n "binary_sv2") (r "^0.1.5") (d #t) (k 0)) (d (n "const_sv2") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "1iza1z0xf1yi3p4jdqmfzh354zhlbn6aq8br6w0iyk36vpx2dvjx") (f (quote (("with_serde" "binary_sv2/with_serde" "serde") ("prop_test" "quickcheck"))))))

(define-public crate-template_distribution_sv2-1.0.0 (c (n "template_distribution_sv2") (v "1.0.0") (d (list (d (n "binary_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "const_sv2") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (k 0)))) (h "1mm3jhy01q4jv9svc8izprbda7mlj32h3xsl62xpxq51iw92hzs8") (f (quote (("with_serde" "binary_sv2/with_serde" "serde") ("prop_test" "quickcheck"))))))

