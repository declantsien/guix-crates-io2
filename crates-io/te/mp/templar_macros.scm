(define-module (crates-io te mp templar_macros) #:use-module (crates-io))

(define-public crate-templar_macros-0.5.0 (c (n "templar_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0k74rsyn5h76chc8zlhn2pc2wazqll5qx7bjc02s4rri48dr2csy")))

