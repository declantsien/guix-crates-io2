(define-module (crates-io te mp template-quote) #:use-module (crates-io))

(define-public crate-template-quote-0.1.0 (c (n "template-quote") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 2)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "template-quote-impl") (r "^0.1.0") (d #t) (k 0)))) (h "1fkfx5mbjd7gjvbzlbmbpfx1dz1dslsjvz2rsrhcz0fwsmn6x04y")))

(define-public crate-template-quote-0.1.1 (c (n "template-quote") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 2)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "template-quote-impl") (r "^0.1.1") (d #t) (k 0)))) (h "13gj9p32v10b247yinbfwwww492k81chcqmnsd2bdmv9yi256g6g")))

(define-public crate-template-quote-0.2.0 (c (n "template-quote") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 2)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "template-quote-impl") (r "^0.2.0") (d #t) (k 0)))) (h "0y8ma8ardizpmnsvqb967iabdmcgircmw58xclw0qjxa3dm5nhns")))

(define-public crate-template-quote-0.3.0 (c (n "template-quote") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 2)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "template-quote-impl") (r "^0.3.0") (d #t) (k 0)))) (h "034paqzqdky2dn5d6pmw361nmrics00javw67cdwwvhakip6fx5p")))

(define-public crate-template-quote-0.3.1 (c (n "template-quote") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 2)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "template-quote-impl") (r "^0.3.1") (d #t) (k 0)))) (h "08abs9m4jp7gdf2ny4qfy0mrk2s778j4i5jd040n3slb8419dds1")))

