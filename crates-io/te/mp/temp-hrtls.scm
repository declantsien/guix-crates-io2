(define-module (crates-io te mp temp-hrtls) #:use-module (crates-io))

(define-public crate-temp-hrtls-0.0.1 (c (n "temp-hrtls") (v "0.0.1") (d (list (d (n "futures") (r "^0.1.13") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "rustls") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.2.3") (f (quote ("tokio-proto"))) (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.11.0") (d #t) (k 0)))) (h "133v4wabzsw9if5pkd84dsqgp4cfdd8cjvb20gdkb902nvlqwjbc")))

