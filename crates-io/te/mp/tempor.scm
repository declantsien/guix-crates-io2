(define-module (crates-io te mp tempor) #:use-module (crates-io))

(define-public crate-tempor-0.1.0 (c (n "tempor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "termion") (r "^1.5.6") (d #t) (k 2)))) (h "08c23d2vk5lf4k208h6jn36dqnk6kprb5y20b6wb094iml9hjnia")))

(define-public crate-tempor-0.2.0 (c (n "tempor") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 2)) (d (n "termion") (r "^1.5.6") (d #t) (k 2)))) (h "0zffmk0faa299sv3985f4azlzs79jvv7iibaqpz1fkcsywawhan0")))

