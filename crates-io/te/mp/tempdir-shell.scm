(define-module (crates-io te mp tempdir-shell) #:use-module (crates-io))

(define-public crate-tempdir-shell-0.1.0 (c (n "tempdir-shell") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0gd0jkaanfz4kpq6q767fjbhgqwk3vasa2ykh9nabpn121a1d1g2")))

(define-public crate-tempdir-shell-0.1.1 (c (n "tempdir-shell") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "15xq20wcf4vpis1c3zl2pddimrpsmg0l99w8njaa8zj23v0zxhhz")))

