(define-module (crates-io te mp tempan) #:use-module (crates-io))

(define-public crate-tempan-0.0.1 (c (n "tempan") (v "0.0.1") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)))) (h "0sg6dag2jbsza7l2s3clq2bnxxc8jsb1bgwmfczly09876cc74x4")))

(define-public crate-tempan-0.0.2 (c (n "tempan") (v "0.0.2") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)) (d (n "hotspot") (r "^0.0.2") (d #t) (k 0)) (d (n "matrix") (r "^0.0.2") (d #t) (k 0)))) (h "1ilnagyzrv28xrmrg1syc34dh3j8qg6wv5dm70yrcdxlqpxz06ad")))

(define-public crate-tempan-0.0.3 (c (n "tempan") (v "0.0.3") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.3") (d #t) (k 0)) (d (n "matrix") (r "^0.0.3") (d #t) (k 0)))) (h "09m6z4aw2yqd7n6mylgz52gpwzxn0ykqh78i6svmacyl4v5w17q8")))

(define-public crate-tempan-0.0.4 (c (n "tempan") (v "0.0.4") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.3") (d #t) (k 0)) (d (n "matrix") (r "^0.0.4") (d #t) (k 0)))) (h "0iby7wngmyzdgv73xy0xqnpbbaf1ajg8dlwpqg5585mnr37yp1mh")))

(define-public crate-tempan-0.0.5 (c (n "tempan") (v "0.0.5") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.5") (d #t) (k 0)) (d (n "matrix") (r "^0.0.6") (d #t) (k 0)))) (h "1g4ka8zyy0cnjb05z264vl5f92q77v6lilvzblk5fbya17yf809g")))

(define-public crate-tempan-0.0.6 (c (n "tempan") (v "0.0.6") (d (list (d (n "assert") (r "^0.0.3") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.6") (d #t) (k 0)) (d (n "matrix") (r "^0.0.7") (d #t) (k 0)))) (h "004nirf334x5p57q2n00h846l9cjw9d6nsjqv9rzy48m5lfqbpia")))

(define-public crate-tempan-0.0.7 (c (n "tempan") (v "0.0.7") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)) (d (n "hotspot") (r "^0.0.7") (d #t) (k 0)) (d (n "matrix") (r "^0.0.8") (d #t) (k 0)))) (h "0zb4d4qp51s6a4rrxqaqff7q7dhp226ida43w5h3blidj5j5ll6h") (y #t)))

