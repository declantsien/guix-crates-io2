(define-module (crates-io te mp templing) #:use-module (crates-io))

(define-public crate-templing-0.1.0-alpha.0 (c (n "templing") (v "0.1.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q0y3swyc144q0hwf2vabg7i9ax7l5c702mxlpvcdqlv543wyqac")))

(define-public crate-templing-0.1.0-alpha.1 (c (n "templing") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "086pvdcdirir79km92kf2fnpcark90yw0scgpz9x35a2gnqhnxnn")))

(define-public crate-templing-0.1.0-alpha.2 (c (n "templing") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vs5q3byb22wy92bh2ijxs3zjsbyirhpzkvwwcgykxm4r9qhrzbc")))

(define-public crate-templing-0.1.0-alpha.3 (c (n "templing") (v "0.1.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01pncil2k29n0xnx19qiy0xbzrxvx2srmjn3hd2mi5n8v0chvnva")))

(define-public crate-templing-0.1.0-alpha.4 (c (n "templing") (v "0.1.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ryr1yjl97rpv1p65xn2fg0nbw0gmgxzjzqn60ijasm1dlwrblsr")))

(define-public crate-templing-0.1.0-alpha.5 (c (n "templing") (v "0.1.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s79nwb2ipxf6b1s3rad8j8nmb0gs8aj5hd0p7fdx1ps6dif7zha")))

(define-public crate-templing-0.1.0 (c (n "templing") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1p4pw0mb7qad7xqmqzx57s3kaczcg6j712whnxph9mrwqvn55px8")))

