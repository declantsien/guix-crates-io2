(define-module (crates-io te mp temporal_rs) #:use-module (crates-io))

(define-public crate-temporal_rs-0.0.1 (c (n "temporal_rs") (v "0.0.1") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "icu_calendar") (r "~1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tinystr") (r "^0.7.4") (d #t) (k 0)))) (h "1g6rl2zpkifzm49dr9bpzkxjm0c6mq0y10vsxahh9n12kqikf13f") (r "1.74")))

(define-public crate-temporal_rs-0.0.2 (c (n "temporal_rs") (v "0.0.2") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "icu_calendar") (r "~1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tinystr") (r "^0.7.4") (d #t) (k 0)))) (h "1ac9n4vsjhgbfdq3kf6g0pab6izk90ydb2gz1lvyg0jjii5123j6") (r "1.74")))

