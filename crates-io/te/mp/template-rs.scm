(define-module (crates-io te mp template-rs) #:use-module (crates-io))

(define-public crate-template-rs-0.1.0 (c (n "template-rs") (v "0.1.0") (h "1gcm4qcgfr3hscfyms3drk587imzsm6a6gsd1y8si37mv2w4ymmg") (f (quote (("default"))))))

(define-public crate-template-rs-0.1.1 (c (n "template-rs") (v "0.1.1") (h "0b3irsj0brm3948qg9s55dbpdrj4gwpynhg87v8anvydbcnprc9b") (f (quote (("default"))))))

(define-public crate-template-rs-0.1.3 (c (n "template-rs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1c0w3l0b0wdz14hq06j285p22xx464ncvn0bd4wixzfja9w9izgz") (f (quote (("default"))))))

(define-public crate-template-rs-0.1.4 (c (n "template-rs") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1lk66hc0ddsc612f12dimbsq3g7h1q2bs35k86czg1r5azmszha6") (f (quote (("default"))))))

(define-public crate-template-rs-0.1.5 (c (n "template-rs") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "07wsaddfki2ajccqw5dml15csrndgq6rvcmd56yd900rhqhqraqv") (f (quote (("default"))))))

(define-public crate-template-rs-0.1.6 (c (n "template-rs") (v "0.1.6") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "15adb59mwwjmfgpdi2yz9a9sx5fymi0d62di1pdq5sjnhff1vn18") (f (quote (("default")))) (r "1.73")))

