(define-module (crates-io te mp templr_parser) #:use-module (crates-io))

(define-public crate-templr_parser-0.1.0 (c (n "templr_parser") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "171as40xvxzk6ldaq7iq6l2fsvzwrrsjirkb1j35w5d3fg5z9ykl")))

(define-public crate-templr_parser-0.1.1 (c (n "templr_parser") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dffn1x7895ivrs4ccx64ynvv7dcsbkv6fdsg4pq27mkqbki1n3i")))

(define-public crate-templr_parser-0.1.2 (c (n "templr_parser") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jm19klxxdadks606q8fs5fd55vhm2bj757yazi5ncb7say44505")))

(define-public crate-templr_parser-0.2.0 (c (n "templr_parser") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bbbfdc9fx30s10dw0vy8p1p6wi736jcczgzd7rzpr2kkvmc87ql")))

(define-public crate-templr_parser-0.2.1 (c (n "templr_parser") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06kl23ghrcf9slq4kvf6n2ccrk6pyilx96584v45ypzwm7gs8ws8")))

