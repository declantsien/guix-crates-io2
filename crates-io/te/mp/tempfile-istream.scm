(define-module (crates-io te mp tempfile-istream) #:use-module (crates-io))

(define-public crate-tempfile-istream-0.1.0 (c (n "tempfile-istream") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "windows") (r "^0.30.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_Com" "Win32_System_Com_StructuredStorage" "Win32_System_Ole" "Win32_System_Time" "Win32_UI_Shell"))) (d #t) (k 0)) (d (n "windows_macros") (r "^0.30.0") (d #t) (k 0)))) (h "1s5450j14s89f86k5j4ngh1d7hjazw20r5jjj5vqrafhg6hb1m4x")))

(define-public crate-tempfile-istream-0.2.0 (c (n "tempfile-istream") (v "0.2.0") (d (list (d (n "windows") (r "^0.30.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_Com" "Win32_System_Com_StructuredStorage" "Win32_UI_Shell"))) (d #t) (k 0)))) (h "0rspr5bg8j9m97hm3k37hmijdw1vvlq3jg6r0p7ikjk1n659s2qj")))

(define-public crate-tempfile-istream-0.3.0 (c (n "tempfile-istream") (v "0.3.0") (d (list (d (n "windows") (r "^0.32.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_Com" "Win32_System_Com_StructuredStorage" "Win32_UI_Shell"))) (d #t) (k 0)))) (h "12aqac830j9wbnpw9p95ww6jsn8gh1fpq5qfxi6i6jx4cf1ywis5")))

(define-public crate-tempfile-istream-1.0.0 (c (n "tempfile-istream") (v "1.0.0") (d (list (d (n "windows") (r "^0.32.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_Com" "Win32_System_Com_StructuredStorage" "Win32_UI_Shell"))) (d #t) (k 0)))) (h "1067vkaj401x62vkb9rmya0ca3l8r8klc8sprky9dkn5gf28ib0n")))

(define-public crate-tempfile-istream-1.0.1 (c (n "tempfile-istream") (v "1.0.1") (d (list (d (n "windows") (r "^0.32.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_Com" "Win32_System_Com_StructuredStorage" "Win32_UI_Shell"))) (d #t) (k 0)))) (h "1svh6jyas1z104fhr2ladczpvfwkz9jnimbcr6z3k3rdyky078n5")))

(define-public crate-tempfile-istream-1.0.2 (c (n "tempfile-istream") (v "1.0.2") (d (list (d (n "windows") (r "^0.32.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_Com" "Win32_System_Com_StructuredStorage" "Win32_UI_Shell"))) (d #t) (k 0)))) (h "1sgvl61z5m0wwka6xcrjal83s45v3m5x8shpspn42gjanni4ksm0")))

(define-public crate-tempfile-istream-1.0.3 (c (n "tempfile-istream") (v "1.0.3") (d (list (d (n "windows") (r "^0.33.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_Com" "Win32_System_Com_StructuredStorage" "Win32_UI_Shell"))) (d #t) (k 0)))) (h "0xck69lasdz38q354p5v1vkispr6gfnlvwxr31xqwsa4gh186xg4")))

(define-public crate-tempfile-istream-1.0.4 (c (n "tempfile-istream") (v "1.0.4") (d (list (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_Com" "Win32_System_Com_StructuredStorage" "Win32_UI_Shell"))) (d #t) (k 0)))) (h "17pa47d0gpa5ri6nvkl9nyw7h8pjvcb13b6nvgfk1igsj52a1w7m")))

