(define-module (crates-io te s3 tes3mp-plugin) #:use-module (crates-io))

(define-public crate-tes3mp-plugin-0.1.0 (c (n "tes3mp-plugin") (v "0.1.0") (h "119x8kpazapj455y536d7xd130zy3q81hqfkdm9gvai4knkqf6dg") (y #t)))

(define-public crate-tes3mp-plugin-0.1.1 (c (n "tes3mp-plugin") (v "0.1.1") (h "0vk7a4mvfzqksijl9bcv79fycd7znqdgf0x5h90x6gsdzlyinkbj") (y #t)))

(define-public crate-tes3mp-plugin-0.1.2 (c (n "tes3mp-plugin") (v "0.1.2") (h "0zdj46djbq1028720sl28rlp58mwpq6qjz82pd2w48zr1xvxv9p1")))

