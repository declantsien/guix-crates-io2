(define-module (crates-io te kt tekton) #:use-module (crates-io))

(define-public crate-tekton-0.1.0 (c (n "tekton") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "176ral65izwffjxx8xnsd0r62n5rr0sy7kwh8s4rx7n4337cml92")))

(define-public crate-tekton-0.2.0 (c (n "tekton") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0b1x93019pxljfc149byi15glgf6j87711qjpv0czp3prfmaylsf")))

(define-public crate-tekton-0.2.1 (c (n "tekton") (v "0.2.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1v473ag22ng9lcqp9j0gv53jc398sj19bl693crn3d0xic9y4ddl") (y #t)))

(define-public crate-tekton-0.3.0 (c (n "tekton") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1dh3hla6hhms5y5c0d6bjv3irlc5b2sdvjn65d53c011p82idfsr")))

