(define-module (crates-io te nk tenki) #:use-module (crates-io))

(define-public crate-tenki-0.1.0 (c (n "tenki") (v "0.1.0") (h "1k38myqfqhv7f84rsw8z77flhf0c84m57077bg918b4qdnh78r5s") (y #t)))

(define-public crate-tenki-0.0.1 (c (n "tenki") (v "0.0.1") (h "1malqzigxshrsn55xdmcs4cc2ralm5ryzwrs8nw1k7ngvpsg2dam")))

