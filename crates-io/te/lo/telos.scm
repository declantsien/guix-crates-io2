(define-module (crates-io te lo telos) #:use-module (crates-io))

(define-public crate-telos-0.1.0 (c (n "telos") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "tls-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "15z5k2ls4brjkdmvr44j5vd7vkpg7mi3bfy7j7da1jnl7fszv2kg")))

(define-public crate-telos-0.2.0 (c (n "telos") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "tls-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "0wfxqr0rvssll453aa41sflmf2bh66g7wr8id7g2yyqjvv4wx1vh")))

(define-public crate-telos-0.2.1 (c (n "telos") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "tls-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "ws2_32-sys") (r "^0.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "0rw33ah9706jjdn6z364210rqw734mvmsid39bd8z4qqy6g5jjv5")))

