(define-module (crates-io te lo teloxide-rocksdb) #:use-module (crates-io))

(define-public crate-teloxide-rocksdb-0.1.0 (c (n "teloxide-rocksdb") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19") (f (quote ("lz4"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "teloxide") (r "^0.12") (k 0)) (d (n "teloxide-core") (r "^0.9") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "teloxide") (r "^0.12") (f (quote ("cbor-serializer" "bincode-serializer"))) (d #t) (k 2)) (d (n "tokio") (r "^1.8") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0mln5gc22k3hmy2qia7j8cws61ywg6v59bnpa3nydcnwiqln5fpw")))

