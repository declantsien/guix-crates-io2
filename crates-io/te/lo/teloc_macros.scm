(define-module (crates-io te lo teloc_macros) #:use-module (crates-io))

(define-public crate-teloc_macros-0.1.0 (c (n "teloc_macros") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1cih3pp59z31ia0f6wk2fdxxgm8j7a8865y7s6642qsga857vg3a")))

(define-public crate-teloc_macros-0.2.0 (c (n "teloc_macros") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y7dzg3m581xp5ghm23v3l1wk5r9ab888rvic3xgkf0ca75lxana")))

