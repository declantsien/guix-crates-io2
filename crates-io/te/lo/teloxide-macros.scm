(define-module (crates-io te lo teloxide-macros) #:use-module (crates-io))

(define-public crate-teloxide-macros-0.1.0 (c (n "teloxide-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1kmw7da2x8398x25xw8g4i0fv591zi7a7272smsi2ffpjhp1qi8z")))

(define-public crate-teloxide-macros-0.1.1 (c (n "teloxide-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1m1fgmnwv0i802jdis1hfxhbibv69bafl2qg7azvf1agi81dpgcg") (y #t)))

(define-public crate-teloxide-macros-0.2.0 (c (n "teloxide-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "011n5bp97g2xvyz91icfcn9jqp41bwfs0qy4khk9z2x9x6jf55dg") (y #t)))

(define-public crate-teloxide-macros-0.1.2 (c (n "teloxide-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1hh0kjvhir5lxxf9v5bbpvjmpqsmdhnpylkkr4llmj2slr4sjrrf") (y #t)))

(define-public crate-teloxide-macros-0.2.1 (c (n "teloxide-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0mdi4v6fwjy5igh7c0in82g3a6sig9lb8rjnl0lhjiiwc9lc1ssl")))

(define-public crate-teloxide-macros-0.3.0 (c (n "teloxide-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0r3ipza60v1bvs2q3fvpcjn2iq8ajgfixwlsskzqg08bc6dj2w67")))

(define-public crate-teloxide-macros-0.3.1 (c (n "teloxide-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1crhpqda4vm05mdrf0zjqamngfjqpjinwr4579n2ghay5k9wqbcx")))

(define-public crate-teloxide-macros-0.3.2 (c (n "teloxide-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0bwmisyks5nqlyqhf2bzam4qix41sgygv9zl6y5hslkmsm58bn0k")))

(define-public crate-teloxide-macros-0.4.0 (c (n "teloxide-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1x2i1p7lr07g30nm0sm89wkjq2wgvi17w9r7nrmcamzsb693vns4")))

(define-public crate-teloxide-macros-0.4.1 (c (n "teloxide-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0v91y7gjxb4g9pr4kjn4flk1373zgha5hdb4lsp328pgidxykdrg")))

(define-public crate-teloxide-macros-0.5.0 (c (n "teloxide-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0gygcbaynlri6fbnvfhpbi60qrdw68dacvs28van3vn0z0kcbv7x")))

(define-public crate-teloxide-macros-0.5.1 (c (n "teloxide-macros") (v "0.5.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0n4a37g31j0p5pprslk7qpmhwd7rkpqkns7mmm5dq43i20pk423x")))

(define-public crate-teloxide-macros-0.6.0 (c (n "teloxide-macros") (v "0.6.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "02xwvq19blggv0in5zfs4vqahixr0w5p5hgh0wxim9r7888sl253")))

(define-public crate-teloxide-macros-0.6.1 (c (n "teloxide-macros") (v "0.6.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0r03rd7f2x1nxg9ak01a8j4qddc486xjk15cbiwscsdicsbllwsg")))

(define-public crate-teloxide-macros-0.6.2 (c (n "teloxide-macros") (v "0.6.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "00z1k65rsir0znm0dlc1i42694pq0in8j0vgfrygqc96wpl2c56h")))

(define-public crate-teloxide-macros-0.6.3 (c (n "teloxide-macros") (v "0.6.3") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "068sf39nzmqha6hx4ccl206jmpz328nrky7wbpa1jjdl8bygklb0")))

(define-public crate-teloxide-macros-0.7.0 (c (n "teloxide-macros") (v "0.7.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1mpz72ba41d5m4zq35vjix4ji3siac7kmdz34gp0ciq0v53gr9a0")))

(define-public crate-teloxide-macros-0.7.1 (c (n "teloxide-macros") (v "0.7.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "016qnrxm334wwm1r8qcaggrps5kjylbaamysr925xfix14xna78g") (r "1.64")))

