(define-module (crates-io te lo telo) #:use-module (crates-io))

(define-public crate-telo-0.1.0 (c (n "telo") (v "0.1.0") (d (list (d (n "biodivine-lib-bdd") (r "^0.4") (d #t) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "dot") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pqlkp4ahzazx9644xv897kn8zmvnnicjfa2kd7y36bjdljpbag8")))

