(define-module (crates-io te te tetengo_trie) #:use-module (crates-io))

(define-public crate-tetengo_trie-0.1.0 (c (n "tetengo_trie") (v "0.1.0") (h "0g68fy322z581729gqi16m8v3naq2l73z42h63yj7japgxbffzap") (y #t)))

(define-public crate-tetengo_trie-0.1.1 (c (n "tetengo_trie") (v "0.1.1") (h "0ks3np0m26y5ilgh5jvbfjvsrqbai0zsb8h7jgf5lknmkww05is7") (y #t)))

(define-public crate-tetengo_trie-0.1.2 (c (n "tetengo_trie") (v "0.1.2") (h "0xc7l29big093v6qg4cbp0s5xfmljq7nvfzggpjnwq74rg120w3a") (y #t)))

(define-public crate-tetengo_trie-1.0.0 (c (n "tetengo_trie") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hashlink") (r "^0.8.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "16axzcjr29alff5lwrbbhv4lddcr6bip26mmf2fybzijdxsrfrwi")))

