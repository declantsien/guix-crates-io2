(define-module (crates-io te cs tecs) #:use-module (crates-io))

(define-public crate-tecs-0.1.0 (c (n "tecs") (v "0.1.0") (h "1dl76m1qzzpmw6zw8bwkafyhnprh4skikjdyvmmks9vhgq9z98nz")))

(define-public crate-tecs-0.1.1 (c (n "tecs") (v "0.1.1") (h "18860qkqqpjjwiz74gcv17zjy0m4w2j77b2l20xjrmswww8pi6gp")))

(define-public crate-tecs-1.0.0 (c (n "tecs") (v "1.0.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "1fvrl1mww11xiys5ijr32wd4v8li6x0a6vi28wh3n5j4hldr0hqm")))

(define-public crate-tecs-1.1.0 (c (n "tecs") (v "1.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0bv4hj5gq129m6kanl2da60jli5kfzsnlgsp4r4x4832prydgn2m")))

