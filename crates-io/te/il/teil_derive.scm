(define-module (crates-io te il teil_derive) #:use-module (crates-io))

(define-public crate-teil_derive-0.0.0 (c (n "teil_derive") (v "0.0.0") (h "1vp64wn6fjan3j2z5j0qp4195m6qmca2crach4cdnpsn22vhl3nq")))

(define-public crate-teil_derive-0.1.0 (c (n "teil_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03fk1rbxlq23vn6d494wjcn8c4kzjiwccdkzck8lr4ic8axgl8d3") (f (quote (("sync") ("iter"))))))

(define-public crate-teil_derive-0.1.1 (c (n "teil_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0c11rsgrpi30j5l6zwmcbqgsy9l2pw2bkb7d4i6q7z8x71yqd6q5") (f (quote (("sync") ("iter"))))))

(define-public crate-teil_derive-0.2.0-alpha.1 (c (n "teil_derive") (v "0.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "139fl6bwg9cn9z11fwqyn13bkg7vjmrmj1wpd7vqwz1g1a3v42ha") (f (quote (("sync") ("iter") ("chrono-field"))))))

(define-public crate-teil_derive-0.2.0-alpha.2 (c (n "teil_derive") (v "0.2.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fgj23qrf3sz6pjzyrzfkfgf3k93y6fgp433dq4h4ans0fc9j6x0") (f (quote (("sync") ("iter") ("chrono-field"))))))

(define-public crate-teil_derive-0.2.0-alpha.3 (c (n "teil_derive") (v "0.2.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xzmx7h046gl4z63k3jvf0cx4rgv4g0wkrp5c17xgsbwgkpfppsn") (f (quote (("sync") ("iter") ("chrono-field"))))))

(define-public crate-teil_derive-0.2.0-alpha.4 (c (n "teil_derive") (v "0.2.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0f9208lw6xm7wrv1qnbgffl1333q14ppjds73dl9hb8za7pd7k6y") (f (quote (("sync") ("iter") ("chrono-field"))))))

(define-public crate-teil_derive-0.2.0-beta.1 (c (n "teil_derive") (v "0.2.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0y85qlgf17jfy3x2xz11wlhgxi2bms2mh6rw5425ayxciwkh1kb2") (f (quote (("sync") ("partial") ("iter"))))))

(define-public crate-teil_derive-0.2.0-beta.2 (c (n "teil_derive") (v "0.2.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "012fzrq5v7wmvw49vq4sfjc8fbs6pn80ka6806dra53dinfbssw4") (f (quote (("sync") ("partial") ("iter"))))))

(define-public crate-teil_derive-0.2.0-beta.3 (c (n "teil_derive") (v "0.2.0-beta.3") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1c7md824g0dglsqa0d8afliyqylsddkmaaj1jkbz54cysr97n9jr") (f (quote (("sync") ("partial") ("iter"))))))

(define-public crate-teil_derive-0.2.0-beta.4 (c (n "teil_derive") (v "0.2.0-beta.4") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08p4g79480ld8blk787km16j3cj9f1hzvyjd2pcwwwy89l2hpv03") (f (quote (("sync") ("serde") ("partial") ("iter"))))))

(define-public crate-teil_derive-0.2.0-beta.5 (c (n "teil_derive") (v "0.2.0-beta.5") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "118aq6l7brb4kz38s5n3ili8fv44b3ikqac4vhgf38a6zxck6c7g") (f (quote (("sync") ("serde") ("partial") ("iter"))))))

(define-public crate-teil_derive-0.2.0 (c (n "teil_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dpckm7453s731b2599f0ds2nk1hmgggrfg9p7wznb80rmnqwmns") (f (quote (("sync") ("serde") ("partial") ("iter"))))))

(define-public crate-teil_derive-0.2.1 (c (n "teil_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ab64y4j141b03ykbnr0f9lqkhh1rf47x2mr0d54rjdx2dzvz33n") (f (quote (("sync") ("serde") ("partial") ("iter"))))))

