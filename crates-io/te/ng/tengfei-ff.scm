(define-module (crates-io te ng tengfei-ff) #:use-module (crates-io))

(define-public crate-tengfei-ff-0.1.0 (c (n "tengfei-ff") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0hlg0hzha0wqqzv3igbmrsv3lqcwfhqs8zf7pfb2g341wya2gmvy")))

(define-public crate-tengfei-ff-0.1.1 (c (n "tengfei-ff") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0kdwbpj483g1l9vgd3gadmccvk6p7x5d6hkxsgipdhplg3ij2pg8")))

(define-public crate-tengfei-ff-0.2.0 (c (n "tengfei-ff") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0p0i1lzi927dnzhma45xqr6jbi1ygld3471n5817437bjrhvj0i9")))

(define-public crate-tengfei-ff-0.3.0 (c (n "tengfei-ff") (v "0.3.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "034vp8qbq6qj2jqvaqv03jaskbx75hif09h5i2z0hrqi2vw4jdhb")))

(define-public crate-tengfei-ff-0.3.1 (c (n "tengfei-ff") (v "0.3.1") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "07ckhh9ai3f3hj4kqfcq5n0mai9b9wfkbzpxlc9amcjz7lxli8d1")))

