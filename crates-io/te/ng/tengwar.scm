(define-module (crates-io te ng tengwar) #:use-module (crates-io))

(define-public crate-tengwar-0.3.0 (c (n "tengwar") (v "0.3.0") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "12za3r9yf272vajiykdbh84jy4lg0vj0r8xcdwp9cm7nhl75ndyl") (f (quote (("long-vowel-unique") ("long-vowel-double") ("circumflex"))))))

(define-public crate-tengwar-0.6.0 (c (n "tengwar") (v "0.6.0") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "1z3lynbwrk290di9d6sc27r0xzrd60kp4xma06hw3wf3yfcid36y") (f (quote (("nuquernar") ("long-vowel-unique") ("long-vowel-double") ("default" "long-vowel-double" "nuquernar") ("circumflex") ("alt-rince"))))))

(define-public crate-tengwar-0.6.1 (c (n "tengwar") (v "0.6.1") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "1jyhrgnrzi3hd05nrcz7fqfh7qljpdp127009m7in0xxxhmzccsn") (f (quote (("nuquernar") ("long-vowel-unique") ("long-vowel-double") ("default" "long-vowel-double" "nuquernar") ("circumflex") ("alt-rince"))))))

(define-public crate-tengwar-0.7.0 (c (n "tengwar") (v "0.7.0") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0w55mzwf91gmssg6kb2bfmki8yh32gjqs3nmxhb85vmkpjg6mlwn") (f (quote (("nuquernar") ("long-vowel-unique") ("long-vowel-double") ("default" "long-vowel-double" "nuquernar") ("circumflex") ("alt-rince"))))))

(define-public crate-tengwar-0.7.1 (c (n "tengwar") (v "0.7.1") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "06vv6m6dh5df5jmgzh7g1gwkc7sv6yjxbaravj18l0ai6l5cb5s1") (f (quote (("nuquernar") ("long-vowel-unique") ("long-vowel-double") ("default" "long-vowel-double" "nuquernar") ("circumflex") ("alt-rince"))))))

(define-public crate-tengwar-0.8.0 (c (n "tengwar") (v "0.8.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("cargo" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0xn953k2qvgi7z6yigmpgwa6115lln7ajyl74ryyip1f1bb5djcv") (f (quote (("nuquernar") ("long-vowel-unique") ("long-vowel-double") ("default" "long-vowel-double" "nuquernar") ("circumflex") ("alt-rince"))))))

(define-public crate-tengwar-0.9.0 (c (n "tengwar") (v "0.9.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("cargo" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)))) (h "0wg3xqr6f9jbsn20i35ah4rfnjmlbb94yrs9f5zz3bv6n227yaaq") (f (quote (("dots-standard") ("default"))))))

(define-public crate-tengwar-0.9.1 (c (n "tengwar") (v "0.9.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("cargo" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "039vyiqd23mfkn6jvz5wz4f1jj6f1sqh9vwx4n7rjrds29f5rzyn") (f (quote (("dots-standard") ("default"))))))

(define-public crate-tengwar-1.0.0 (c (n "tengwar") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("cargo" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "1v2ckp1pvbrri7pl65gajnavad33zfzr96i8cxz5xabb64m7wf1b") (f (quote (("dots-standard") ("default") ("csur"))))))

(define-public crate-tengwar-1.1.0 (c (n "tengwar") (v "1.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("cargo" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "1rc9wnpbxjz5zc2cmx4sbk25373cv6xzcrbcf6gwb997i5hmrdyz") (f (quote (("mode-custom" "serde") ("dots-standard") ("default") ("csur"))))))

