(define-module (crates-io te le telety) #:use-module (crates-io))

(define-public crate-telety-0.1.0 (c (n "telety") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("visit" "visit-mut"))) (d #t) (k 0)) (d (n "telety-impl") (r "^0.1") (k 0)) (d (n "telety-macro") (r "^0.1") (d #t) (k 0)))) (h "1fawnwf53a53a8xq35fs9lvq798gjggcha0kzhh6f0g4x3ksc413") (f (quote (("v1" "telety-impl/v1") ("default" "v1"))))))

(define-public crate-telety-0.2.0 (c (n "telety") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("visit" "visit-mut"))) (d #t) (k 0)) (d (n "telety-impl") (r "^0.2") (k 0)) (d (n "telety-macro") (r "^0.2") (d #t) (k 0)))) (h "1cfw81q1p0dx21r6746x9dinfhdr8k294fkfnvckg9z47pv0vrbx") (f (quote (("v1" "telety-impl/v1") ("default" "v1"))))))

