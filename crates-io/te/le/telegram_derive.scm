(define-module (crates-io te le telegram_derive) #:use-module (crates-io))

(define-public crate-telegram_derive-0.1.0 (c (n "telegram_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "0z88va5kkxxj4c75msw3hxfsqpjnglz608xd09hanvdcik7p8ka7")))

(define-public crate-telegram_derive-0.2.0 (c (n "telegram_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "191d8lq9a7s3rmapwa1f1bwm4wqng753q40nail8q0y51wfb7yvw")))

