(define-module (crates-io te le teleport) #:use-module (crates-io))

(define-public crate-teleport-0.1.0 (c (n "teleport") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "meio") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.16") (f (quote ("full"))) (d #t) (k 0)))) (h "1l4ndzvcnbn1y1rqg4h9fvzgagfcdxb86dhi3bvppypi7hmi7b3l")))

