(define-module (crates-io te le telexide_proc_macros) #:use-module (crates-io))

(define-public crate-telexide_proc_macros-0.1.0 (c (n "telexide_proc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0vnivr5k1wyx7nrymq4sn4gvx6srpcn0rxbvmfi4x14x51xznals")))

(define-public crate-telexide_proc_macros-0.1.1 (c (n "telexide_proc_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "14qsxsga1ny53z2gc9k1zjm4p3sg0nqgzrhlap1h347fcdkqz05b")))

(define-public crate-telexide_proc_macros-0.1.2 (c (n "telexide_proc_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1qv7c9lcr4zbx88c98h6z5dcygcwcl8zlg7xpck0492ck9jvs5ix")))

