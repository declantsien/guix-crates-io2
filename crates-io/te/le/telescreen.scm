(define-module (crates-io te le telescreen) #:use-module (crates-io))

(define-public crate-telescreen-0.1.0 (c (n "telescreen") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "slack") (r "^0.17.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "0sg3xb6q3wpmispgp2nanmsmkxds8mzvahd0l6dix7r24xxxv5xs")))

(define-public crate-telescreen-0.1.2 (c (n "telescreen") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "slack") (r "^0.17.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "0hd6kl9i11amf9wpb11ms5b4srhg6w4b0c7y2b2kp95slc5xsqxy")))

(define-public crate-telescreen-0.1.3 (c (n "telescreen") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "slack") (r "^0.17.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "0bsnpd0kybd6p7p5fk79fyn4ing4cshhw6p3kc6awfbgwfahz836")))

