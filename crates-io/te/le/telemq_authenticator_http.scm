(define-module (crates-io te le telemq_authenticator_http) #:use-module (crates-io))

(define-public crate-telemq_authenticator_http-0.1.0 (c (n "telemq_authenticator_http") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "telemq_plugin_types") (r "^0.1") (f (quote ("authenticator"))) (d #t) (k 0)))) (h "0qqdc6kqicphpl9cbgq8cb1p4myq0fb360ppwn505ciplbaq5br7")))

