(define-module (crates-io te le telegram-bot-async-raw) #:use-module (crates-io))

(define-public crate-telegram-bot-async-raw-0.8.1 (c (n "telegram-bot-async-raw") (v "0.8.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kzxwy941ls5k0lwgizxh11bd9h8898cyjvnymyx32pvvmd7a8rx")))

(define-public crate-telegram-bot-async-raw-0.8.2 (c (n "telegram-bot-async-raw") (v "0.8.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0440fyy536d04cp60cinjrh79sswvvhff6s3f3pr82cx850192i5")))

