(define-module (crates-io te le telegra-ph) #:use-module (crates-io))

(define-public crate-telegra-ph-0.1.0 (c (n "telegra-ph") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0n5ywrkhi30pjzh37xy4vwak47x2dhsjbyk5v0d6x4qzm17cp94g")))

(define-public crate-telegra-ph-0.1.1 (c (n "telegra-ph") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "01zah4czq1jhnm72xhzdmybbfr29szqx7r11zjmldi7jb78dc46b")))

(define-public crate-telegra-ph-0.1.2 (c (n "telegra-ph") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0a3yycj2kpb81csiba2ssi6j9cjlmf0fw19k1pc6d6ywz91jdrrn")))

(define-public crate-telegra-ph-0.1.3 (c (n "telegra-ph") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "0aw3gdax31iknisql8l0n6lm6ka0wn441h6w0nb4aw09nsz31lcp")))

