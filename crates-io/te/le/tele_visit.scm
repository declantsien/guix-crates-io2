(define-module (crates-io te le tele_visit) #:use-module (crates-io))

(define-public crate-tele_visit-0.1.0 (c (n "tele_visit") (v "0.1.0") (d (list (d (n "insta") (r "^1.10.0") (d #t) (k 2)) (d (n "tele_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "tele_tokenizer") (r "^0.1.0") (d #t) (k 2)))) (h "10im5iazs8ry57nxwhx0ryq72d0c4ymixibq0ycvjq27qg5r48hq")))

(define-public crate-tele_visit-0.2.0 (c (n "tele_visit") (v "0.2.0") (d (list (d (n "insta") (r "^1.10.0") (d #t) (k 2)) (d (n "tele_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "tele_tokenizer") (r "^0.2.0") (d #t) (k 2)))) (h "0593kqfsidmvv7lcc2yy8qwg5d8vhixn1wk9ricx8wirxf7b8p28")))

