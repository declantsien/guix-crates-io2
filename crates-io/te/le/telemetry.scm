(define-module (crates-io te le telemetry) #:use-module (crates-io))

(define-public crate-telemetry-0.1.0 (c (n "telemetry") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 2)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "0dh7blw9n324z6s6inzhrikk0fg8nv6szgcazx7427n96142rz9z")))

(define-public crate-telemetry-0.1.1 (c (n "telemetry") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 2)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "084qaxp9qc8pimk5xmy1r1nsxjrfwwss1w4rli7bii9r31hvalqf")))

(define-public crate-telemetry-0.1.2 (c (n "telemetry") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 2)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "0pfhbqnn9knxnmfg1457pbg7w2pzifgkdwv66wsy5wp3ajr194k1")))

(define-public crate-telemetry-0.1.3 (c (n "telemetry") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 2)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "0vsw3a7jhf0dh6vzm8511gr7rgphz1g5zcxrsi6s6kx0vgwrw25h")))

