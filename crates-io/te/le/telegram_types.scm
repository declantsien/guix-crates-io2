(define-module (crates-io te le telegram_types) #:use-module (crates-io))

(define-public crate-telegram_types-0.1.0 (c (n "telegram_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1glwx68ba2gdy5000viypfm0hqanasg0bj10hwr1x5z1ynkghrh7")))

(define-public crate-telegram_types-0.1.1 (c (n "telegram_types") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r75r3w9d5ph7lhc3v7yba6zp2jl5jwjd2imwrrqxj72l9iq9q3j")))

(define-public crate-telegram_types-0.1.2 (c (n "telegram_types") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dh6qzpqx3h51gczbd945001924q21j9dhfwbz1k52mik4h4mlwm")))

(define-public crate-telegram_types-0.1.3 (c (n "telegram_types") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rdi2f0byvxfb6rnl1qas5744xvn83avzwkv65jv8mynvwf4n4ri")))

(define-public crate-telegram_types-0.1.4 (c (n "telegram_types") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "108f6y23x2m377j575lr9grchdfigp3xbzcbpfd7j8a74zgfb0xv") (f (quote (("high" "chrono" "reqwest"))))))

(define-public crate-telegram_types-0.1.5 (c (n "telegram_types") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xba6s156pqc6d1h2whh5hpy0hni6dfxkp9skyapl124p3ln41ja") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.1.6 (c (n "telegram_types") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "020aslcnpbi4s5adwylxczhw2yrgmx4w33dl2rfz43qbj3y4vjna") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.1.7 (c (n "telegram_types") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15nzy0w0issqah8qbq8l1b1wmsil4xwnynivpc3810g82582fgl9") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.1.8 (c (n "telegram_types") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0snm0xjkzd5fkq0j00l3fnzi7y7l80ghf7g3wayyi77ziik401y5") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.1.9 (c (n "telegram_types") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zfp8lbgmwzl3xpyp2m8gwqnddnanhgxl227jryrjy39n46a2p1l") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.1.10 (c (n "telegram_types") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10gafi4mkqkk654w3fa2l7c151hb1v2kslp14qwm2hg8jk0kc9gq") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.1.11 (c (n "telegram_types") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kg15rhnl98j4kvryxl8ls9pw7vi89iyf7nf8xmwwfl93f78dkwy") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.2.0 (c (n "telegram_types") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y7a3ljs3y4adw8r0172acfcaglrwx2aiaz4qb0mnzk500yzmhyq") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.2.1 (c (n "telegram_types") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iv4304q05rh9ndrhi4462rxnj91jlllkl56r2pcj6r8qh8g4mzi") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.3.0 (c (n "telegram_types") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05rp7xal063xr4slqflbx4yl73z5zb4877mshl4z4m4mxqd8g5qj") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.4.0 (c (n "telegram_types") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hbcnmy0qrga01yrlizdfj5hpwvbai506spi2igs5n983iqc9h1m") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.5.0 (c (n "telegram_types") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08h642fdnkf3yysgy36rylnmrnly8s4f9vr4prnxcy1hipp43zc9") (f (quote (("high" "chrono"))))))

(define-public crate-telegram_types-0.6.0 (c (n "telegram_types") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "06z9mpmg7ylh6asbymj145nxg7dlq6jd3n47dlvm0sb7h8vbvyv5") (f (quote (("high" "chrono"))))))

