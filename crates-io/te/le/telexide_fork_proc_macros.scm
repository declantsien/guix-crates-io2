(define-module (crates-io te le telexide_fork_proc_macros) #:use-module (crates-io))

(define-public crate-telexide_fork_proc_macros-0.1.1 (c (n "telexide_fork_proc_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1hl8h5x5msypxj0jlrjg4vl776gji3mfjg4a6dr297yjdkfx9f2b")))

