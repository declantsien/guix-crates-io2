(define-module (crates-io te le tele_utils) #:use-module (crates-io))

(define-public crate-tele_utils-0.1.0 (c (n "tele_utils") (v "0.1.0") (h "1xpfn5qbn22qdqzcinm8w1rmzc6dkd3l37nfpaff9v7076hfga0x")))

(define-public crate-tele_utils-0.2.0 (c (n "tele_utils") (v "0.2.0") (h "1d1r33s6f4za1jg3h4ggw7i5zyvn4s7lwyym0wb91ca07vr1klhq")))

