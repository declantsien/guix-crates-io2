(define-module (crates-io te le telemq_plugin_types) #:use-module (crates-io))

(define-public crate-telemq_plugin_types-0.1.0 (c (n "telemq_plugin_types") (v "0.1.0") (d (list (d (n "mqtt-packets") (r "^0.1.0") (f (quote ("v_3_1_1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0p06hnwlxwd3j49ykx4n7cs6pd1g2823xjx1a3pnmjaylfyf4cxh") (f (quote (("authenticator"))))))

