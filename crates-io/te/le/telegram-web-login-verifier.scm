(define-module (crates-io te le telegram-web-login-verifier) #:use-module (crates-io))

(define-public crate-telegram-web-login-verifier-0.1.0 (c (n "telegram-web-login-verifier") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 0)))) (h "1gap7wlx9b8nd3ljs5b2kc0wlskkrfzqlphj0n00pn04vdq5kagp")))

