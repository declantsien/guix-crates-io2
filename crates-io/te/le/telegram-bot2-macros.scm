(define-module (crates-io te le telegram-bot2-macros) #:use-module (crates-io))

(define-public crate-telegram-bot2-macros-0.1.0 (c (n "telegram-bot2-macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "15xlg0sdya6g7i0grpjji5ayqb2vhy5wapkkrwiaqi5zighc89bb")))

(define-public crate-telegram-bot2-macros-0.1.1 (c (n "telegram-bot2-macros") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1lvyj6kk7mspn2xp6iz7hbh8lmk07qgraxd8m50yk6ldzs4psh0f")))

(define-public crate-telegram-bot2-macros-0.1.2 (c (n "telegram-bot2-macros") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0hpv2qjly06flm06x3z9dvmn845wxfxns9nzrz0d2jmwa2krdalq")))

(define-public crate-telegram-bot2-macros-0.1.3 (c (n "telegram-bot2-macros") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "17ly47wxpcykcdjiddrv7ynr3a1frv2hw9d31gh8jss2816g4k83")))

