(define-module (crates-io te le teleborg) #:use-module (crates-io))

(define-public crate-teleborg-0.1.0 (c (n "teleborg") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "00qx9cc89ah01bgkf85ykwvzjb8xk0qh0n9in3wi46cg7amf2xm7")))

(define-public crate-teleborg-0.1.1 (c (n "teleborg") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "0y4pc2dsprr49nl6inncvzlzlxw2m2xxxp5ciwf2fqjlm6r65cxq") (y #t)))

(define-public crate-teleborg-0.1.2 (c (n "teleborg") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "02sp4ng8h929fpnmqjgh21a8vnxf9yrssjswm2ckmfg6vr5brphm") (y #t)))

(define-public crate-teleborg-0.1.3 (c (n "teleborg") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "198j75z8vad4m28041vhfipaj5sizav7lpb7d2q5bdfipla3drvv") (y #t)))

(define-public crate-teleborg-0.1.31 (c (n "teleborg") (v "0.1.31") (d (list (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "15ib6b5vl6rvv9cp7wqprhwrdqnny2fgjf8pv524wr97f8sl8hj5")))

(define-public crate-teleborg-0.1.32 (c (n "teleborg") (v "0.1.32") (d (list (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.5") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.4") (d #t) (k 0)))) (h "0a4j15jf8whk18v3m4626xbvjxsp9sfzzsgb058fxa1n9cc4yajs")))

