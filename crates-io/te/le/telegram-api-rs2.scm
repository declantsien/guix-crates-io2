(define-module (crates-io te le telegram-api-rs2) #:use-module (crates-io))

(define-public crate-telegram-api-rs2-0.0.5 (c (n "telegram-api-rs2") (v "0.0.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "urlencoding") (r "^1.3.3") (d #t) (k 0)))) (h "1lqbwplhk3x3xi0l1vjrbmjbxr73saqfgp0p3m5v44xv431napd5")))

