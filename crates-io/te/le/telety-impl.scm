(define-module (crates-io te le telety-impl) #:use-module (crates-io))

(define-public crate-telety-impl-0.1.0 (c (n "telety-impl") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "090y3g6j0dqkv6g8ddxf739zjpsbvmd7r8w5dy8v270d06fcnd7l") (f (quote (("v1") ("default" "v1"))))))

(define-public crate-telety-impl-0.2.0 (c (n "telety-impl") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1wmwc5n53wvaisf4x753zgnvcb132vv3p676km97k7m9brf6hj32") (f (quote (("v1") ("default" "v1"))))))

