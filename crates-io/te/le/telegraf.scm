(define-module (crates-io te le telegraf) #:use-module (crates-io))

(define-public crate-telegraf-0.1.0 (c (n "telegraf") (v "0.1.0") (d (list (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1cckwfa12nzwvgf03z34ncmffbmb83214aq7lscj70grh9ky8nxv")))

(define-public crate-telegraf-0.1.1 (c (n "telegraf") (v "0.1.1") (d (list (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1abk3fsv7w49c9lkw1614smghy82bi677ca54dnhjm2c10214mlw")))

(define-public crate-telegraf-0.1.2 (c (n "telegraf") (v "0.1.2") (d (list (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0pkd383p2a2i2xpb94wj9697kbfnfgvp4wckfkbbvbc24c28w0xp")))

(define-public crate-telegraf-0.1.3 (c (n "telegraf") (v "0.1.3") (d (list (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "11y6wjvs4yv366yf3vlsvs2l5hd518pz7zj8s2kgj3ksydmnb8qj")))

(define-public crate-telegraf-0.2.0 (c (n "telegraf") (v "0.2.0") (d (list (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1vc3jaihyn038dq6i7j3isw4hh1vsirwr94rh12g5kb5lwipx4hn")))

(define-public crate-telegraf-0.2.1 (c (n "telegraf") (v "0.2.1") (d (list (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0qb7l1r2vdcp340gm7samnxc1zirz3i7mpgznhm85r57xjhvgcyw")))

(define-public crate-telegraf-0.3.0 (c (n "telegraf") (v "0.3.0") (d (list (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0v0vl6ii6lg9yhga2jq5cj9ssqr6yg18yf09mqlr51hsbqxn2zcc")))

(define-public crate-telegraf-0.4.0 (c (n "telegraf") (v "0.4.0") (d (list (d (n "telegraf_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "02qbzvwgjxmnpxwzgrh9ahhszihygm4f8zmg5dsyfvls2nml7nvf")))

(define-public crate-telegraf-0.4.1 (c (n "telegraf") (v "0.4.1") (d (list (d (n "telegraf_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0ykvrr2la0561grisc7rxyi5ha1qj4ddhiyazvjknbx3yn43lij0")))

(define-public crate-telegraf-0.4.2 (c (n "telegraf") (v "0.4.2") (d (list (d (n "telegraf_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1yz9s64n3ja81zyx2bdaxdzzjn6pzvvd2i27sd2jp718xgdkvr8m")))

(define-public crate-telegraf-0.4.3 (c (n "telegraf") (v "0.4.3") (d (list (d (n "telegraf_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1k1pjf6ci9rf2bbyi59lfi2xr3wxnj1xjk4f061mx1ypkzlsarp5")))

(define-public crate-telegraf-0.4.4 (c (n "telegraf") (v "0.4.4") (d (list (d (n "telegraf_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1bydh6zswqlfbr718wskgp2dv8dspsl5a8chj641plx38nczmf2c")))

(define-public crate-telegraf-0.4.5 (c (n "telegraf") (v "0.4.5") (d (list (d (n "telegraf_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0ykabj6sh3qlz4sxl0ibam0fy8h7wjzjlqfbwnjh2rxc4gjqif36")))

(define-public crate-telegraf-0.5.0 (c (n "telegraf") (v "0.5.0") (d (list (d (n "telegraf_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "09gxm1c38idrbhrr1wy1jjn8ass927a30vrfs27mb92gbx34jkb2")))

(define-public crate-telegraf-0.5.1 (c (n "telegraf") (v "0.5.1") (d (list (d (n "telegraf_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0h0mjrjbnaq3ij0iznhz4igag0bp8a9lw2psnmcfpkyglx1lvvs9")))

(define-public crate-telegraf-0.6.0 (c (n "telegraf") (v "0.6.0") (d (list (d (n "telegraf_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1i1270dyj2wv0mp2ifiy8f65ihbrkc5nzz23zdr1myjlj79hf81d")))

