(define-module (crates-io te le teleapi) #:use-module (crates-io))

(define-public crate-TeleApi-0.1.0 (c (n "TeleApi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "10jdswqrkg998bay5zhlnk8qjlsxaqnv1537j4c97id75crp454i") (y #t)))

(define-public crate-TeleApi-0.1.1 (c (n "TeleApi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "089zcd7dsf4nfsf5jgyrynxa7qix2jvvz2002ch8qls8kf5m85an") (y #t)))

(define-public crate-TeleApi-0.1.2 (c (n "TeleApi") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1hxkqmdqs2ww5g8smqhm2w2jy1fsi502w4xml6j2q3hqn8y2aklb")))

(define-public crate-TeleApi-0.1.3 (c (n "TeleApi") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (d #t) (k 0)))) (h "1pxw1as9k3n7nsq1fg2wq61l8njs81jkd0ycqj9fni1rjh8rilx5")))

(define-public crate-TeleApi-0.1.4 (c (n "TeleApi") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v3jvlvg1rc02y3rlfd1hdnv78y01jd3hh207nznd8jfs3kr1a6h")))

