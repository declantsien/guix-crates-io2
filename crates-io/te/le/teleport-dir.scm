(define-module (crates-io te le teleport-dir) #:use-module (crates-io))

(define-public crate-teleport-dir-0.1.0 (c (n "teleport-dir") (v "0.1.0") (d (list (d (n "askama") (r "^0.11.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.12") (d #t) (k 0)))) (h "15ybjlj3vn9d1nf1szp6112fg6vc5hq1yfl650qb1zwxqbn4fhbz")))

