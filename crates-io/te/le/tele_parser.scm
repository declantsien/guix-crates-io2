(define-module (crates-io te le tele_parser) #:use-module (crates-io))

(define-public crate-tele_parser-0.1.0 (c (n "tele_parser") (v "0.1.0") (d (list (d (n "insta") (r "^1.10.0") (d #t) (k 2)) (d (n "tele_tokenizer") (r "^0.1.0") (d #t) (k 0)))) (h "1xjbqri71xbxll3zgq0sayd2fdxh6za65l9pqhx2gqlb6d3xa62b")))

(define-public crate-tele_parser-0.2.0 (c (n "tele_parser") (v "0.2.0") (d (list (d (n "insta") (r "^1.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tele_tokenizer") (r "^0.2.0") (d #t) (k 0)))) (h "15bsjbznpl5kzin2jlpys7wzvlzixcabfq3888d15yywwmasgvwa")))

