(define-module (crates-io te le telegram-bot-raw-ars) #:use-module (crates-io))

(define-public crate-telegram-bot-raw-ars-0.9.0 (c (n "telegram-bot-raw-ars") (v "0.9.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08417xy7agy916sklwmgc3nk9mhw4k54r1lz28hxpjcjyb4kg3l8")))

(define-public crate-telegram-bot-raw-ars-0.9.1 (c (n "telegram-bot-raw-ars") (v "0.9.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wrka4vh8dbzx06dy1mzy6hjff93p85crwczzigl6m1bivpm1k32")))

