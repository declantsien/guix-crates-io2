(define-module (crates-io te le teleinfo-parser) #:use-module (crates-io))

(define-public crate-teleinfo-parser-0.1.0 (c (n "teleinfo-parser") (v "0.1.0") (h "12r4wvl012s4h1prpg5qbz6nirymp52v0d6i70d9lr5wkx54jl4b")))

(define-public crate-teleinfo-parser-0.2.0 (c (n "teleinfo-parser") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)))) (h "168sph1z60cn6hx6dcxwzy6p9pwf190zqcl9h18vbc6rsgdk5zkp")))

(define-public crate-teleinfo-parser-0.3.0 (c (n "teleinfo-parser") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "14f6nxmc0qd9j2q7jsrrb2w5zrhj2v9yw61bq3b544lbs26ak64g")))

(define-public crate-teleinfo-parser-0.4.0 (c (n "teleinfo-parser") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "0rrfjrfaglil0i09v84dfzyh0wfll92s5c6jn7d6gk5i8lgc2h18")))

(define-public crate-teleinfo-parser-0.5.0 (c (n "teleinfo-parser") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)))) (h "08658g6m35vl72kbmkd40i34p9rznwr88ddiv63c50i2aq8mn5s8")))

