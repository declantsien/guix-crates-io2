(define-module (crates-io te le telegram-bot-fork-raw) #:use-module (crates-io))

(define-public crate-telegram-bot-fork-raw-0.7.0 (c (n "telegram-bot-fork-raw") (v "0.7.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c67rigrfkiyhhg478fh626qlmrbqnqq2k1yr3kbq4p2dsp3jpkh")))

(define-public crate-telegram-bot-fork-raw-0.7.1 (c (n "telegram-bot-fork-raw") (v "0.7.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r8pqjxcsx7mq54raiax0naz72n5ka1gnbkmvp0izj5hhyhfapm9")))

(define-public crate-telegram-bot-fork-raw-0.7.2 (c (n "telegram-bot-fork-raw") (v "0.7.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f6aj4y03pmjgkcrbwp5vc77p0bvyx9xja64afpr2yr6j83zcxr9")))

(define-public crate-telegram-bot-fork-raw-0.7.3 (c (n "telegram-bot-fork-raw") (v "0.7.3") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mk2vjnq76wnh058dw5782sb94mi7id28hmkkqkyiqx9q4475bwd")))

(define-public crate-telegram-bot-fork-raw-0.7.4 (c (n "telegram-bot-fork-raw") (v "0.7.4") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s65cnxxqis8whk892wf8v37mq02h6z6cjkdjdi4mrcpzss7kvpv")))

(define-public crate-telegram-bot-fork-raw-0.7.5 (c (n "telegram-bot-fork-raw") (v "0.7.5") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hr52r85nf7105cglhxi6966y6rp063xmi62gbk1kg2p0j8vsx67")))

(define-public crate-telegram-bot-fork-raw-0.7.6 (c (n "telegram-bot-fork-raw") (v "0.7.6") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mfb846a5in69gkziqz4iqgqhp7dfjs43rp88daxbb42ivdjn1s4")))

(define-public crate-telegram-bot-fork-raw-0.7.7 (c (n "telegram-bot-fork-raw") (v "0.7.7") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fjl5ihxrvf8p4ilig7hxm23zqbbb76jpb4cfgdx8pcylsc2gm0w")))

(define-public crate-telegram-bot-fork-raw-0.7.8 (c (n "telegram-bot-fork-raw") (v "0.7.8") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-value") (r "^0.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f908lhrq8ikjr9gnl96ab1a89la31kxxzwd2n4cxhkxz18nnn00")))

