(define-module (crates-io te le telety-macro) #:use-module (crates-io))

(define-public crate-telety-macro-0.1.0 (c (n "telety-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "telety-impl") (r "^0.1") (d #t) (k 0)))) (h "1666g0wqq0867bycdskrcg3iia484xi3q4vhx36gbcwk6c5vr98c")))

(define-public crate-telety-macro-0.2.0 (c (n "telety-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "telety-impl") (r "^0.2") (d #t) (k 0)))) (h "01h0xqrbfansrbhb4mim6r65sqi85xm5p77bhij7pblkjfmbhd81")))

