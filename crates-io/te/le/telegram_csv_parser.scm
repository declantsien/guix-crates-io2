(define-module (crates-io te le telegram_csv_parser) #:use-module (crates-io))

(define-public crate-telegram_csv_parser-0.1.0 (c (n "telegram_csv_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "03ws7rvwh26qz0jkahf7qqrp8pq2k6flv19b8biqhhl2cz9h9k6g")))

(define-public crate-telegram_csv_parser-0.1.1 (c (n "telegram_csv_parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "05xrypmngsws9dlkf2cdk98qzacf3ga3b7jqj9626qi7liapqxa0")))

(define-public crate-telegram_csv_parser-0.1.2 (c (n "telegram_csv_parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "13dnry825c17ixmzlpcf8d3186i7k5p0wqfw10aynzjqdy4xqaky")))

(define-public crate-telegram_csv_parser-0.1.3 (c (n "telegram_csv_parser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "016r5bldgpgjrgxwmkyh41l8fnwi5nnx097sx4h272287c8djh7a")))

(define-public crate-telegram_csv_parser-0.1.4 (c (n "telegram_csv_parser") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ppi0b38zcgrwwbd8n4nmga1bi3ayrdd2n47bn1bs7h59rxcdzq9")))

(define-public crate-telegram_csv_parser-0.1.5 (c (n "telegram_csv_parser") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1hdzp0wwl11a9ggm9fcmzjgbx251wgxn4l6kzspx8bamcgw4nlxx")))

(define-public crate-telegram_csv_parser-0.1.6 (c (n "telegram_csv_parser") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "17zhhcnmriv7zcmpijdm7hll120wv7z8j2mmivfcglik1rk6bj0f")))

(define-public crate-telegram_csv_parser-0.1.7 (c (n "telegram_csv_parser") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1bk7bm7hnq4k76dx9xmrp548aigfhxbwmc3wq6vld9kc715sjgj0")))

(define-public crate-telegram_csv_parser-0.1.8 (c (n "telegram_csv_parser") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0i8rfnx6k5gdvpwfnfp6ds1y7kszdzadb78mvfd6qlml56gsa1sq")))

(define-public crate-telegram_csv_parser-0.1.9 (c (n "telegram_csv_parser") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1mhppkc0b983pklj1bzjsxnk7vjk8lrvych88ag4710zjq9hp15d")))

