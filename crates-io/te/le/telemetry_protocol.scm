(define-module (crates-io te le telemetry_protocol) #:use-module (crates-io))

(define-public crate-telemetry_protocol-0.1.0 (c (n "telemetry_protocol") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jdl3g9l8z9dasgfarfd52w2lak03qrv9b2n63gfmq6llijfyhs6")))

(define-public crate-telemetry_protocol-0.1.1 (c (n "telemetry_protocol") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "182v7z2myq83hg4n6q6ym9ldkzrfdzhlqbg7aqrxk506sqy02zy6")))

