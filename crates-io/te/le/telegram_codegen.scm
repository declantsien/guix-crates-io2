(define-module (crates-io te le telegram_codegen) #:use-module (crates-io))

(define-public crate-telegram_codegen-0.1.0 (c (n "telegram_codegen") (v "0.1.0") (d (list (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.6") (d #t) (k 0)))) (h "0vz3hbb12zcwba5icznvsc05bdzym5zlcgwb1k9zfgysm04pz304")))

(define-public crate-telegram_codegen-0.2.0 (c (n "telegram_codegen") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1ww3a343hchq77m15zzcv7r8lypsf87qnq2d4dkzhhc4byz0c7f6")))

