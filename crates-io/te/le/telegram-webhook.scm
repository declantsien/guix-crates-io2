(define-module (crates-io te le telegram-webhook) #:use-module (crates-io))

(define-public crate-telegram-webhook-0.1.0 (c (n "telegram-webhook") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "teloxide") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "vergen") (r "^8.2") (f (quote ("build" "cargo" "git" "gitcl"))) (d #t) (k 1)))) (h "0hwcdsrvjnwd6clzr5lh60xfnhpa2h9jw6miz3xv2j1l4kqlpzl2")))

