(define-module (crates-io te le teleser_gen) #:use-module (crates-io))

(define-public crate-teleser_gen-0.1.0 (c (n "teleser_gen") (v "0.1.0") (d (list (d (n "devise") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "11dxfrxccn8lkbijv2jajhs831nz6m6l9ci5gg2b1i2cfrbwlsji")))

