(define-module (crates-io te le tele_tokenizer) #:use-module (crates-io))

(define-public crate-tele_tokenizer-0.1.0 (c (n "tele_tokenizer") (v "0.1.0") (d (list (d (n "insta") (r "^1.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tele_utils") (r "^0.1.0") (d #t) (k 0)))) (h "095816422iv2511kdnv2hjwp5r11zsx3sd5rjalrq27539p3m4qf")))

(define-public crate-tele_tokenizer-0.2.0 (c (n "tele_tokenizer") (v "0.2.0") (d (list (d (n "insta") (r "^1.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tele_utils") (r "^0.2.0") (d #t) (k 0)))) (h "04m54j9hx0n5sfapncll2swdi1qmzkhlfr8rk00jfn3mcsrm03pz")))

