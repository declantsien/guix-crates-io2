(define-module (crates-io te le teleboard) #:use-module (crates-io))

(define-public crate-teleboard-0.1.0 (c (n "teleboard") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.6") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "meio") (r "^0.11.0") (d #t) (k 0)) (d (n "meio-ws") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)) (d (n "tui") (r "^0.12.0") (f (quote ("crossterm"))) (k 0)) (d (n "warp") (r "^0.2.5") (d #t) (k 0)))) (h "139azy4j8by29622n4wckgbq0ixml70bwas5xvl40ncz09b5kh7c")))

