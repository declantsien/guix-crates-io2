(define-module (crates-io te le telestes) #:use-module (crates-io))

(define-public crate-telestes-0.1.0 (c (n "telestes") (v "0.1.0") (d (list (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (o #t) (k 0)))) (h "18slns8zwx7gh2inkkspzsgvfksys8ylphy1xpmh2hjxynh6kyd6") (f (quote (("db") ("alloc" "serde/alloc"))))))

(define-public crate-telestes-0.1.1 (c (n "telestes") (v "0.1.1") (d (list (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (o #t) (k 0)))) (h "0c4pn1g7smq20i13xm6pffm2jvn1vrnnxm05ih089ihjy4cvd192") (f (quote (("db") ("alloc" "serde/alloc"))))))

