(define-module (crates-io te le teleecho) #:use-module (crates-io))

(define-public crate-teleecho-0.1.0 (c (n "teleecho") (v "0.1.0") (d (list (d (n "clap") (r "^2.10") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "telegram-bot") (r "^0.4.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1xj8dqpljlghzaacypqw487xf3zlk5xnri9bc6cn7hn9m0rvsgba") (y #t)))

