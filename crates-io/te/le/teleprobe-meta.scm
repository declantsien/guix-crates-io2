(define-module (crates-io te le teleprobe-meta) #:use-module (crates-io))

(define-public crate-teleprobe-meta-1.0.0 (c (n "teleprobe-meta") (v "1.0.0") (h "1wvz5x3vpf2w58zbcz196s453wmmgy8sw6cp9ji487yhcj3l7jm1")))

(define-public crate-teleprobe-meta-1.1.0 (c (n "teleprobe-meta") (v "1.1.0") (h "053gziwq99dbwgnhgv1n739i6v90fr0finj7dx46njz9jdzgikhr")))

