(define-module (crates-io te le telegram-login) #:use-module (crates-io))

(define-public crate-telegram-login-0.1.0 (c (n "telegram-login") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "ring") (r "^0.13.1") (d #t) (k 0)))) (h "0m3jsx5kf9rmf5kn10k99zj5fwjg58783sbclx01gjc0lwn9fixs")))

(define-public crate-telegram-login-0.2.0 (c (n "telegram-login") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "ring") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0vvabkkpsxdh2wks9c5shy3l4viqx96dk4r62z8dxdq18qnnfdmb")))

(define-public crate-telegram-login-0.2.1 (c (n "telegram-login") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0h8gh59j8irjxsqf2lmdakcjh70i0ysfh13r4fsm6fb6h5x5hzbx")))

