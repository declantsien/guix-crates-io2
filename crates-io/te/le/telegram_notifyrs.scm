(define-module (crates-io te le telegram_notifyrs) #:use-module (crates-io))

(define-public crate-telegram_notifyrs-0.1.0 (c (n "telegram_notifyrs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "09s6fccm10r8z7nj03y9s1spzzs7q1ykk3yqyrpvk9iyczlsw47p")))

(define-public crate-telegram_notifyrs-0.1.1 (c (n "telegram_notifyrs") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1h4lxzni196s98qyjj3msfiagxjrb7gagrhks0svc84cxx2rvkzf")))

(define-public crate-telegram_notifyrs-0.1.3 (c (n "telegram_notifyrs") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "ureq") (r "^1.5.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1zbi4435jhvzvwmwf65yv1b33kbh5bmiyx7w3ywgqvyzi990nfyv")))

