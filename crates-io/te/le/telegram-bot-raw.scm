(define-module (crates-io te le telegram-bot-raw) #:use-module (crates-io))

(define-public crate-telegram-bot-raw-0.6.0 (c (n "telegram-bot-raw") (v "0.6.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lnc19x1zpc995na93fh0lkgq96qcxa6v6zswg4hb02pawknzi4v")))

(define-public crate-telegram-bot-raw-0.6.1 (c (n "telegram-bot-raw") (v "0.6.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hgfyyv1ln0q6m3g147icqx3rarlij9yg3v1lvficqw031hlp5g7")))

(define-public crate-telegram-bot-raw-0.6.2 (c (n "telegram-bot-raw") (v "0.6.2") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.5.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03grd5hbc6xfyd0gymv636snhcs57hjvw9vmkp6q16iapq35xmca")))

(define-public crate-telegram-bot-raw-0.6.3 (c (n "telegram-bot-raw") (v "0.6.3") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.5.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0i4pxr4j74ib01bpjyarni97mmg1lw4mdm30rm3p7lddimqqymv5")))

(define-public crate-telegram-bot-raw-0.7.0 (c (n "telegram-bot-raw") (v "0.7.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.5.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0b39v4sl0vfwx9a00mwr0v2d21dfgija861b14gka1g4pqs5immd")))

(define-public crate-telegram-bot-raw-0.8.0 (c (n "telegram-bot-raw") (v "0.8.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.5.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ngm3s7isz8309x8v546zf4i2pva3dyam91jr6gn6pdqw8hc2kqf")))

