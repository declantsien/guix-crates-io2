(define-module (crates-io te le teleparse-macros) #:use-module (crates-io))

(define-public crate-teleparse-macros-0.0.1 (c (n "teleparse-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)))) (h "1c334xb5yyihqd6dvcsc6bawyi1zsv4y5bf4a8g0ji0s079mm23m")))

