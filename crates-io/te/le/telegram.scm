(define-module (crates-io te le telegram) #:use-module (crates-io))

(define-public crate-telegram-0.1.0 (c (n "telegram") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "telegram_codegen") (r "^0.1.0") (d #t) (k 1)) (d (n "telegram_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0mw5sxkmpz90ganppcx25aych6xcpr3vrflan4dlw7yq7wpi7s4j")))

(define-public crate-telegram-0.2.0 (c (n "telegram") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "extprim") (r "^1.4.0") (d #t) (k 0)) (d (n "telegram_codegen") (r "^0.2.0") (d #t) (k 1)) (d (n "telegram_derive") (r "^0.2.0") (d #t) (k 0)))) (h "0cqx1jkj695khwic4384vrgzmcd48r1mlsb0vjkidpfiqs0f19m1")))

