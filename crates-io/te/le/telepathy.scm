(define-module (crates-io te le telepathy) #:use-module (crates-io))

(define-public crate-telepathy-0.0.1 (c (n "telepathy") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.11.0") (d #t) (k 0)) (d (n "emojicons") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "1cnmvj7fyiaqz0p94r19q0mkqcykn8j2a5chrzm289dcn8ysiz1r")))

