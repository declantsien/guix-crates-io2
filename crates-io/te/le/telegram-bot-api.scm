(define-module (crates-io te le telegram-bot-api) #:use-module (crates-io))

(define-public crate-telegram-bot-api-0.1.0 (c (n "telegram-bot-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("stream" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0rx7l5kjmvh4kgygyqj5anp1w7jfmr3jnzkznymjm2ydd7r1kbyv")))

(define-public crate-telegram-bot-api-0.1.1 (c (n "telegram-bot-api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("stream" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "1qkr2vf1c1vv91w6ggp431nn9zb3vihi59gfgkhg1zsdxsz9i4d0")))

(define-public crate-telegram-bot-api-0.1.2 (c (n "telegram-bot-api") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("stream" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0zayfsaz7cy0zqb12kj5kp958gav79v1m2d5ylfmv8y1gprbgl5k")))

