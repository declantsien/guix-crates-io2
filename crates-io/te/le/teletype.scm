(define-module (crates-io te le teletype) #:use-module (crates-io))

(define-public crate-teletype-0.3.0 (c (n "teletype") (v "0.3.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "intel8080") (r "^0.8.0") (d #t) (k 0)))) (h "0i7ni6wnb1ybbnpkvzikwsfh8289qg108mr75r9pgw83320qrp8p")))

(define-public crate-teletype-0.5.0 (c (n "teletype") (v "0.5.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "intel8080") (r "^0.9.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1c9v7icm9bwv9n868vip85jz0bsh8pigc7sx2csjvf704zvkjjiw")))

(define-public crate-teletype-0.6.0 (c (n "teletype") (v "0.6.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "intel8080") (r "^0.12.0") (d #t) (k 0)))) (h "0s17z1jqh3q367x3yszss8nacdwhlxz4z1lf349xl7csmy1br841") (y #t)))

(define-public crate-teletype-0.7.0 (c (n "teletype") (v "0.7.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "intel8080") (r "^0.12.0") (d #t) (k 0)))) (h "0ik3r4f9mxchij4q7wr8mf5w6c14cwm6cbfipb071xd9649cmrl6")))

(define-public crate-teletype-0.8.0 (c (n "teletype") (v "0.8.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "intel8080") (r "^0.13.0") (d #t) (k 0)))) (h "1ni51s691hvin3zx0arahf4a73zg68icp76kdxawm6lrzsq10dyz")))

(define-public crate-teletype-0.9.0 (c (n "teletype") (v "0.9.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "intel8080") (r "^0.14.0") (d #t) (k 0)))) (h "0m0fpf8rhwxrlba39v0fy47yb3fds1isxfrhjbvyjlv67l046p0l")))

(define-public crate-teletype-0.10.0 (c (n "teletype") (v "0.10.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "intel8080") (r "^0.15.0") (d #t) (k 0)))) (h "1n9c4shdlkqwsch09d7n3h1gm9s0mnzm2a267jk4yqyzml6ali8w")))

(define-public crate-teletype-0.11.0 (c (n "teletype") (v "0.11.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "intel8080") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (d #t) (k 0)))) (h "1b5iv3k2bp1pn2ckl1592209khq80d8wd1sdg2gks5g7j2qm71pv")))

(define-public crate-teletype-0.12.0 (c (n "teletype") (v "0.12.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "intel8080") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (d #t) (k 0)))) (h "07xak0smznfqrgjr25c1ybsgij2gaasj1wl2y0csryj4byslbc9v")))

(define-public crate-teletype-0.13.0 (c (n "teletype") (v "0.13.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "intel8080") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (d #t) (k 0)))) (h "0qb4awq9hxp4hi0m0ij8i1ngi2wylm8zfrvprn0m9s4k0h51vy60")))

(define-public crate-teletype-0.14.0 (c (n "teletype") (v "0.14.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "intel8080") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (d #t) (k 0)))) (h "1p652aj9lfkd4yrzgpvw2dvxvany2x25rzxy42bngvqjh8myjzzi")))

(define-public crate-teletype-0.15.0 (c (n "teletype") (v "0.15.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "intel8080") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (d #t) (k 0)))) (h "15xl3cj0h67agi7vsa8s6kah2xsvg7c6fq0nm5qzimzkcc4814d0")))

