(define-module (crates-io te le telegram-api-rs) #:use-module (crates-io))

(define-public crate-telegram-api-rs-0.0.1 (c (n "telegram-api-rs") (v "0.0.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ghbfpq1l842v3f226zrdw8r6zid1cpgjx255644vc2ikkvjxb5q")))

(define-public crate-telegram-api-rs-0.0.2 (c (n "telegram-api-rs") (v "0.0.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0f1i8hpwncwq9qipfsn1fg78vblmm3ajjnlw4xjcbrxb7hygx7y1")))

(define-public crate-telegram-api-rs-0.0.3 (c (n "telegram-api-rs") (v "0.0.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "urlencoding") (r "^1.3.3") (d #t) (k 0)))) (h "00biqhncqifcnpg8bd8cqih11fsj7nhdc5xifv7pmgv39vw4dgi6")))

(define-public crate-telegram-api-rs-0.0.4 (c (n "telegram-api-rs") (v "0.0.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "urlencoding") (r "^1.3.3") (d #t) (k 0)))) (h "1ah89hjl1dzhhd4p1ifb06p12ib39pir04pbmb929sqbyiq0i8j3")))

(define-public crate-telegram-api-rs-0.0.5 (c (n "telegram-api-rs") (v "0.0.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "urlencoding") (r "^1.3.3") (d #t) (k 0)))) (h "1mr2jzadiqxhf9bhmlwl7cnjhk0lxnz1xx36cafdv5iwf5nl004w")))

(define-public crate-telegram-api-rs-0.0.6 (c (n "telegram-api-rs") (v "0.0.6") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0y01kwy0jc04lprm4276mvi2hs9a7r8kv4kyn1lsc0440v753708")))

(define-public crate-telegram-api-rs-0.0.7 (c (n "telegram-api-rs") (v "0.0.7") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.25") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "0dzmjxbyl340i2xpd6jgvg9mr6mbxwkva2gaqllv6wdwnyx8cy09")))

(define-public crate-telegram-api-rs-0.0.8 (c (n "telegram-api-rs") (v "0.0.8") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.25") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1qnrlldfdr3ifsh09az8svik3bbzqb8ljhxagimg4qjmiq72z9zf")))

(define-public crate-telegram-api-rs-0.0.9 (c (n "telegram-api-rs") (v "0.0.9") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.25") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1h2ycr8s1pq06v2kzpznb8wv763czjy1nrfbrh2f2b8v5v5n34k3")))

