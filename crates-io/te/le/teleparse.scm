(define-module (crates-io te le teleparse) #:use-module (crates-io))

(define-public crate-teleparse-0.0.1 (c (n "teleparse") (v "0.0.1") (d (list (d (n "bit-set") (r "^0.5.3") (d #t) (k 0)) (d (n "deref-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "macrotest") (r "^1.0.12") (d #t) (k 2)) (d (n "num") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "teleparse-macros") (r "=0.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "001k0yxil9fwqnh5pbd3j68pgzaf420p0ylr9avyky8bw2lm79xj") (f (quote (("arc"))))))

