(define-module (crates-io te le telegraf_derive) #:use-module (crates-io))

(define-public crate-telegraf_derive-0.1.0 (c (n "telegraf_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "07x2j6c9c08gnlyzqaj5vn81l7jvvymdng80k4ylc5siwizhi99v")))

(define-public crate-telegraf_derive-0.1.1 (c (n "telegraf_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1yjis6fi8llx8bf7z17k7703vficyiyiis77hhv5idii2jc1iins")))

(define-public crate-telegraf_derive-0.2.0 (c (n "telegraf_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1i5br7nyd53akxdycd2mlrz4jch43wgfhhr0fzc7k58sn5zxdmc7")))

