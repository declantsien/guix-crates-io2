(define-module (crates-io te le telebot-derive) #:use-module (crates-io))

(define-public crate-telebot-derive-0.0.1 (c (n "telebot-derive") (v "0.0.1") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "syn") (r "^0.9.0") (d #t) (k 0)))) (h "0yr2pw35ssl2kqwscs8dsvd6gdcmvlxm58rxvmp7y6s6f89fvykf")))

(define-public crate-telebot-derive-0.0.2 (c (n "telebot-derive") (v "0.0.2") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "syn") (r "^0.9.0") (d #t) (k 0)))) (h "09ms5sa1cbx44q1lxcwicb9b1c44wgpk21ij7s7qp1nin8jlhvvy")))

(define-public crate-telebot-derive-0.0.3 (c (n "telebot-derive") (v "0.0.3") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "syn") (r "^0.9.0") (d #t) (k 0)))) (h "1f6j4rl9mvcnwjcyn1p8llj723lfgbwp0gh2kwam495f5bs4i9i3")))

(define-public crate-telebot-derive-0.0.4 (c (n "telebot-derive") (v "0.0.4") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "syn") (r "^0.9.0") (d #t) (k 0)))) (h "0qk0fc4rnzvn5ivv0li5a51safmrskgc8105c5i3c4xbmkkj8wmr")))

(define-public crate-telebot-derive-0.0.5 (c (n "telebot-derive") (v "0.0.5") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "syn") (r "^0.9.0") (d #t) (k 0)))) (h "1fsfqbx0if5w11s4sif9dp3hf6srq0an1xfw52rjwgczhzn5f0zr")))

(define-public crate-telebot-derive-0.0.6 (c (n "telebot-derive") (v "0.0.6") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "quote") (r "^0.3.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1q1d9fz0vh838fr9n4z1pv7zgh44gamxfxhahcv3rhdbcck1pj49")))

(define-public crate-telebot-derive-0.0.8 (c (n "telebot-derive") (v "0.0.8") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1rkay76qmqbjwa10qym1lyhwb3m3v30xb432xcz9zz3fw7skpngs")))

(define-public crate-telebot-derive-0.0.9 (c (n "telebot-derive") (v "0.0.9") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1akkc7smw9b61k5kd4r6z1231ylv4awmlisrdaw4mnbdx9m0qw5a")))

(define-public crate-telebot-derive-0.0.10 (c (n "telebot-derive") (v "0.0.10") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0k1z39py78s3v8v0mb2vd41wbnkn9d1wnm1bj2qkrlyn0wjqy69i")))

(define-public crate-telebot-derive-0.0.11 (c (n "telebot-derive") (v "0.0.11") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0lyfqnr53cfiiyvzpm7p9ia36har21x1jlfrr65q7g5yfpv63qrx")))

(define-public crate-telebot-derive-0.0.12 (c (n "telebot-derive") (v "0.0.12") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0wxgc5a7877baf6q4q1hz1cydb2ix0nhvqm3bs44g1wcf3qz7qm9")))

(define-public crate-telebot-derive-0.0.13 (c (n "telebot-derive") (v "0.0.13") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0m23pwcsn30b74f5bxcgbmhhr0qcrmx8yq3qwd58bkr8w1q1bmz0")))

(define-public crate-telebot-derive-0.0.14 (c (n "telebot-derive") (v "0.0.14") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0hb77igfl32qwabpv41lhb8xq95l97mz4rkqlmfpr89z2nz5gjsq")))

