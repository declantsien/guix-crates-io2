(define-module (crates-io te le tele_codegen) #:use-module (crates-io))

(define-public crate-tele_codegen-0.2.0 (c (n "tele_codegen") (v "0.2.0") (d (list (d (n "insta") (r "^1.10.0") (d #t) (k 2)) (d (n "tele_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "tele_tokenizer") (r "^0.2.0") (d #t) (k 2)) (d (n "tele_visit") (r "^0.2.0") (d #t) (k 2)))) (h "04dl6z1rzh9yjaq9pkz0rfyymf3p9jkd9qcga2rzaaq07aj03z52")))

