(define-module (crates-io te le telecomande) #:use-module (crates-io))

(define-public crate-telecomande-0.1.0 (c (n "telecomande") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "19ck2fchfrzynjdqxdp95lh74bv7km5wrsrxaizkipsrqqnyasyb")))

(define-public crate-telecomande-1.0.0 (c (n "telecomande") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rznsxxjyc0n16zhrbqv2v81wihpffaphr3q9m1s8qvvd4phblmr")))

(define-public crate-telecomande-1.0.1 (c (n "telecomande") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "17nk62kiffsy76702xanxy9cbbg3rr67hdq12a3mgxgn7dzsc8j7")))

(define-public crate-telecomande-1.2.0 (c (n "telecomande") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 0)))) (h "165q3hqh7d06wqpqcx0pnxw0zk7ms72fwvsmkcydd3g68i7yj7k6")))

(define-public crate-telecomande-1.2.1 (c (n "telecomande") (v "1.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 0)))) (h "1ys7frp5019fd5dhbcfdxswhz9lwnm4g3b69s9aw00phga1ngap8")))

(define-public crate-telecomande-1.2.2 (c (n "telecomande") (v "1.2.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 0)))) (h "0y8naxmmrs4957jgq9ag3ay8p9r73c9c70d8v566fwqfbww12a8y")))

