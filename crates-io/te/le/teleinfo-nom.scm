(define-module (crates-io te le teleinfo-nom) #:use-module (crates-io))

(define-public crate-teleinfo-nom-0.1.0 (c (n "teleinfo-nom") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1wp0mhllfyp1sz8cxah29m4m1l4c3jfp67q8nqbrgb13cq73b46c")))

(define-public crate-teleinfo-nom-0.1.1 (c (n "teleinfo-nom") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0ampbkmgclfmynqssh1njkq2msz4fnl1r0i3mp2m9w52ws521a36")))

(define-public crate-teleinfo-nom-0.2.1 (c (n "teleinfo-nom") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1wfw8c0m8cq5wc1bh3pcmsi9y20x0s5d2qn4nx3klnhh1a4ainhk")))

