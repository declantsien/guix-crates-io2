(define-module (crates-io te le telegram-markov-chain) #:use-module (crates-io))

(define-public crate-telegram-markov-chain-0.1.0 (c (n "telegram-markov-chain") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "markov") (r "^1.1") (d #t) (k 0)))) (h "105gpvib2sgi30v4p49wfhq016gpkqzqwi8s5jvvv5fk98qc9s2c")))

(define-public crate-telegram-markov-chain-0.1.1 (c (n "telegram-markov-chain") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "markov") (r "^1.1") (d #t) (k 0)))) (h "0gg3bmv00wx92b3wha7rzxb2p2lv5a1xc2n4ngbfjcq2k2vjjzs6")))

(define-public crate-telegram-markov-chain-0.1.2 (c (n "telegram-markov-chain") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "markov") (r "^1.1") (d #t) (k 0)))) (h "0bcbii3chrqkc7ghkdbbazf5c7c8qi90xvb4735mfsckyr37wiyd")))

