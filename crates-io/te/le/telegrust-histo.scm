(define-module (crates-io te le telegrust-histo) #:use-module (crates-io))

(define-public crate-telegrust-histo-0.1.0 (c (n "telegrust-histo") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1a3snkikrw0w4ysacl9pgkmg1hzv1np3zmdbipnlf7j3m5gnw5d4")))

(define-public crate-telegrust-histo-0.1.1 (c (n "telegrust-histo") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1hkhrb3rdh3c5lsj6i3h1pxa7smjmn6v5lmami5x7z18z479iacw")))

(define-public crate-telegrust-histo-0.1.2 (c (n "telegrust-histo") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0scqi2qpp24fh6dcij17k3rjg7461fjmkhn627hv2jk259wm4rv1")))

