(define-module (crates-io te le telegram-bot-types) #:use-module (crates-io))

(define-public crate-telegram-bot-types-0.1.0 (c (n "telegram-bot-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1qnqdcwm7bwmmd945ysqmiqbwhx63n70idf4jlc5lpnnllrsjcf3")))

(define-public crate-telegram-bot-types-0.1.1 (c (n "telegram-bot-types") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1a7kkw215jqsaf1fg3jqp55zmkpqqbf6bjlwk7rfv619wjydrlah")))

(define-public crate-telegram-bot-types-0.1.2 (c (n "telegram-bot-types") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0rjzprhfmanhyiqpa22vsps23livcpmrpfm49r8mnqgq7bdacyq7") (y #t)))

(define-public crate-telegram-bot-types-0.1.3 (c (n "telegram-bot-types") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1k070dgl6v51wrnw4a41kzrhz1mh4fh9jhkdm1c6cy5gpbk3wzh0")))

(define-public crate-telegram-bot-types-0.1.4 (c (n "telegram-bot-types") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "09qvfr1w9j8iwa4fb1wkqsxyjmkkki1j2irsi6ybr7ijj8djc6wm")))

(define-public crate-telegram-bot-types-0.2.0 (c (n "telegram-bot-types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0gn38pxcr1is53m6qdkh349zgnbffmqn5m4d6w0b5mshyxz63qbp")))

(define-public crate-telegram-bot-types-0.2.1 (c (n "telegram-bot-types") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0kgz4qbis4r3xkc0brq41z0y1v8v6r3kn9cg5jisds78wp5w179x")))

