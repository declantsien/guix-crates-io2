(define-module (crates-io te gr tegra_swizzle) #:use-module (crates-io))

(define-public crate-tegra_swizzle-0.1.0 (c (n "tegra_swizzle") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "119v2d9z58bysmrhkcxlizs44z6q05ml0j6whczjl439gjynjc1p")))

(define-public crate-tegra_swizzle-0.2.0 (c (n "tegra_swizzle") (v "0.2.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1qvpyci29b234jkqn87za3amzlnpr7bs3bvjp0n0jx1yaxs7q2ci")))

(define-public crate-tegra_swizzle-0.3.0 (c (n "tegra_swizzle") (v "0.3.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1grhbm9b8000miw492b6w3p33sfyw012h0zsz98sywjfl2m0k1w9") (f (quote (("ffi"))))))

(define-public crate-tegra_swizzle-0.3.1 (c (n "tegra_swizzle") (v "0.3.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0y0nphs4rajgld54lz490x9gwr5drxfbjnfj94cv5p3wk7qrd9nm") (f (quote (("ffi"))))))

(define-public crate-tegra_swizzle-0.3.2 (c (n "tegra_swizzle") (v "0.3.2") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0y4fzly1xnccnxjf02cq13r7y1px5ns3q9sp6afbm3gy13la5bqj") (f (quote (("ffi"))))))

