(define-module (crates-io te fi tefi-oracle) #:use-module (crates-io))

(define-public crate-tefi-oracle-0.2.0 (c (n "tefi-oracle") (v "0.2.0") (d (list (d (n "cosmwasm-bignumber") (r "^2.2.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^0.16.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1s0hcxx4gwz762hgp8d1rdzqkh74fwf2fh8zry7kj0br6hcs0drg") (f (quote (("internal"))))))

