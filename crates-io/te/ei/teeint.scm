(define-module (crates-io te ei teeint) #:use-module (crates-io))

(define-public crate-teeint-0.1.0 (c (n "teeint") (v "0.1.0") (h "1nv6pwdfma4k8xjqkdn2vbcv32afsa55v3zng8aamf6xc34bnv5r")))

(define-public crate-teeint-0.1.1 (c (n "teeint") (v "0.1.1") (h "1xf7zcx8wa48mfs9fcm2l2ckd6651g77id5qwfhbmiq5b0wq2fpc")))

(define-public crate-teeint-0.1.2 (c (n "teeint") (v "0.1.2") (h "1fga65lpz8y9kc8nly6sjczvmf4azxizsvq6dixdxmpjgwqy4bm1")))

(define-public crate-teeint-1.0.0 (c (n "teeint") (v "1.0.0") (h "08k2ysnj2ksz35291xp9z97lidsxyjj8b3m48dcymhidpg3ffnlx")))

