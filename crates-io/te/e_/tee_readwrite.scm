(define-module (crates-io te e_ tee_readwrite) #:use-module (crates-io))

(define-public crate-tee_readwrite-0.1.0 (c (n "tee_readwrite") (v "0.1.0") (h "1y3a8v5g6gijd0vfh6jscap3556l6yhyfl5c2jyyal3k525aqhh6")))

(define-public crate-tee_readwrite-0.2.0 (c (n "tee_readwrite") (v "0.2.0") (h "0mfrr3n1fgvffwsiyh22b52hlbr7nqy2sym86i0h0zxhkdifblsp")))

