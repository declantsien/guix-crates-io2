(define-module (crates-io te mi temis) #:use-module (crates-io))

(define-public crate-temis-0.1.0 (c (n "temis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo") (r "^0.74.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jq-rs") (r "^0.4.1") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "near-abi-client") (r "^0.1.0") (d #t) (k 0)) (d (n "near-sdk") (r "^4.1.1") (d #t) (k 0)) (d (n "near-workspaces") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)))) (h "1qacfxyylp92igpnqin73zg9jc8lpfshzhrfvy19idr5xjc03f63")))

