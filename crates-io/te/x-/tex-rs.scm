(define-module (crates-io te x- tex-rs) #:use-module (crates-io))

(define-public crate-tex-rs-0.1.0 (c (n "tex-rs") (v "0.1.0") (h "0jxvd8vxnj0g6yqbsqalalyfjx4d5hc77rybvdmhzf0jqypbj2va")))

(define-public crate-tex-rs-0.2.0 (c (n "tex-rs") (v "0.2.0") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1w37hb3xc8apqx95hr018y7bzhvkrfq156v9jkndp5vvnlcgf8w2") (f (quote (("async"))))))

(define-public crate-tex-rs-0.2.1 (c (n "tex-rs") (v "0.2.1") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "01424szj8mlfnviw6zsg5lhi13c75g39i684qg3yhdn1wvkszi8q") (f (quote (("async"))))))

(define-public crate-tex-rs-0.2.2 (c (n "tex-rs") (v "0.2.2") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "11n3jnxmf0i5my5y9ic7j601cc39vrl87n71n7s2zqz33vs76ljn")))

(define-public crate-tex-rs-0.2.3 (c (n "tex-rs") (v "0.2.3") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0wzp2jp24d65rmzif6lyvb13cs3y993y0qq22ila5jh2n32qkp3g")))

(define-public crate-tex-rs-0.2.4 (c (n "tex-rs") (v "0.2.4") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1jib3b4lnidk8kc1spd1wwargh6l91az2hrw01dpc70jx7bxsy6z")))

(define-public crate-tex-rs-0.2.5 (c (n "tex-rs") (v "0.2.5") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "07k18qccvayly66l5mfcslh95lglpj2ixp82mbil99i2x8p4gx5m")))

(define-public crate-tex-rs-0.2.6 (c (n "tex-rs") (v "0.2.6") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0vsi6qshn8x96c7mxhi5yy19hi2kjlg9vaqqdw02b9ch26m9ila4")))

(define-public crate-tex-rs-0.2.7 (c (n "tex-rs") (v "0.2.7") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0pil83550fdw8npq9jnq6k2qbm4r55b88xq4nj7403cnll2v4d52")))

(define-public crate-tex-rs-0.2.8 (c (n "tex-rs") (v "0.2.8") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "00a6mkrpqhjyp3igbv5syf1w09fwciq9ypyij9278p6xwz32ad1n")))

