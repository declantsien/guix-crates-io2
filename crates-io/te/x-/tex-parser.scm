(define-module (crates-io te x- tex-parser) #:use-module (crates-io))

(define-public crate-tex-parser-0.1.0 (c (n "tex-parser") (v "0.1.0") (d (list (d (n "peg") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bx79gdng8f6q9r5j1mnvilc41ya9hc0pa4vjcx2c028h0fn0lfp") (f (quote (("trace" "peg/trace") ("default" "serde"))))))

