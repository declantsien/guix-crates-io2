(define-module (crates-io te x- tex-fmt) #:use-module (crates-io))

(define-public crate-tex-fmt-0.1.0 (c (n "tex-fmt") (v "0.1.0") (d (list (d (n "clap") (r "=4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0f6h10193sskwz5jxvl6jybi4zyfm7c3r4hvs7ahhlccdpgybysg")))

(define-public crate-tex-fmt-0.2.0 (c (n "tex-fmt") (v "0.2.0") (d (list (d (n "clap") (r "=4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 0)) (d (n "rstest_reuse") (r "^0.6.0") (d #t) (k 0)))) (h "1zim0snbq3226nxicd9pz2yfxk2fh6g6nlfarqa15fdpvgfnkh0y")))

