(define-module (crates-io te xf texfmt) #:use-module (crates-io))

(define-public crate-texfmt-0.1.0 (c (n "texfmt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "cfg_log") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "logging_timer") (r "^1.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "should-color") (r "^0.5.2") (f (quote ("clap"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "023zn8waig13xzxzb284djcnb4c6xn8z2qsqvx0rfrzrb3dhhnnn") (f (quote (("default" "log"))))))

