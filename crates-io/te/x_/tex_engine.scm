(define-module (crates-io te x_ tex_engine) #:use-module (crates-io))

(define-public crate-tex_engine-0.0.1 (c (n "tex_engine") (v "0.0.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("kv_unstable"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.13") (d #t) (k 0)))) (h "1r0zbxdzbbrb11mr2b5v8sr6wsqcxha24cdqvhfnczw9k7kbcdrs")))

