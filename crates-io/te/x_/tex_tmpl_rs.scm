(define-module (crates-io te x_ tex_tmpl_rs) #:use-module (crates-io))

(define-public crate-tex_tmpl_rs-0.1.0 (c (n "tex_tmpl_rs") (v "0.1.0") (d (list (d (n "handlebars") (r "^3.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tectonic") (r "^0.8.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "10nlpbgv1c2x05r96z4is5mg09k8snlysmfrr5clf1y1sgkyfq9w")))

(define-public crate-tex_tmpl_rs-0.1.1 (c (n "tex_tmpl_rs") (v "0.1.1") (d (list (d (n "handlebars") (r "^3.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tectonic") (r "^0.8.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1kqm9gash5cviikphhqqk4dd8szq3nw2psi77i584vw07lpplrdl")))

(define-public crate-tex_tmpl_rs-0.1.2 (c (n "tex_tmpl_rs") (v "0.1.2") (d (list (d (n "handlebars") (r "^4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tectonic") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "13hmqfpxmqnl1219km94r789k8xh4v7bbwzz54zb56b533kpav24")))

