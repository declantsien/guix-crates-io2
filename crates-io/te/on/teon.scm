(define-module (crates-io te on teon) #:use-module (crates-io))

(define-public crate-teon-0.0.58-alpha.3 (c (n "teon") (v "0.0.58-alpha.3") (d (list (d (n "bigdecimal") (r "=0.3.1") (d #t) (k 0)) (d (n "bson") (r "^2.6.0") (f (quote ("chrono-0_4" "serde_with"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (f (quote ("js"))) (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0p9y71jrd1qzmvavriwhzkyqf9x8gxfm0qfaf47iv1c3xr8krjg9")))

