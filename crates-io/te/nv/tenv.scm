(define-module (crates-io te nv tenv) #:use-module (crates-io))

(define-public crate-tenv-0.2.0 (c (n "tenv") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.2") (d #t) (k 0)))) (h "0v4fpp87w2npqycwjh2728vy08fh7rl2nxlrkzfrlpi4mw8y6d77")))

(define-public crate-tenv-0.3.0 (c (n "tenv") (v "0.3.0") (d (list (d (n "argfile") (r "^0.1.4") (d #t) (k 0)) (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)))) (h "1hjf3lz00dn1wpb701kycdfk06gfj8wcd616hlzm7727sfd8jlg4")))

