(define-module (crates-io te zo tezos-smart-rollup-panic-hook) #:use-module (crates-io))

(define-public crate-tezos-smart-rollup-panic-hook-0.1.0 (c (n "tezos-smart-rollup-panic-hook") (v "0.1.0") (d (list (d (n "tezos-smart-rollup-core") (r "^0.1.0") (d #t) (k 0)))) (h "171ykq8mk9zmj89ifiwjqhxf2d81h0d6w9yvs4vwbyaf1ffq8f86") (f (quote (("default" "abort" "debug") ("debug") ("abort"))))))

(define-public crate-tezos-smart-rollup-panic-hook-0.2.0 (c (n "tezos-smart-rollup-panic-hook") (v "0.2.0") (d (list (d (n "tezos-smart-rollup-core") (r "^0.2.0") (d #t) (k 0)))) (h "0c0v7i9gm40ky84nn56wb8139cxbk6m178xmxxb1dw45n79djy5q") (f (quote (("default" "abort" "debug") ("debug") ("abort"))))))

(define-public crate-tezos-smart-rollup-panic-hook-0.2.1 (c (n "tezos-smart-rollup-panic-hook") (v "0.2.1") (d (list (d (n "tezos-smart-rollup-core") (r "^0.2.1") (d #t) (k 0)))) (h "1amlbsg9ibbx4ff7l06z331dqjhvch1pr1lbyg25qafh6kch9mvy") (f (quote (("default" "abort" "debug") ("debug") ("abort"))))))

(define-public crate-tezos-smart-rollup-panic-hook-0.2.2 (c (n "tezos-smart-rollup-panic-hook") (v "0.2.2") (d (list (d (n "tezos-smart-rollup-core") (r "^0.2.2") (d #t) (k 0)))) (h "1cywsw6rsa81kcd72zlwv2bskf4iq5rwfxr5riw6jm3psqhjpjsk") (f (quote (("std") ("proto-alpha" "tezos-smart-rollup-core/proto-alpha") ("default" "abort" "debug" "std") ("debug") ("abort"))))))

