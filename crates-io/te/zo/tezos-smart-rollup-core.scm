(define-module (crates-io te zo tezos-smart-rollup-core) #:use-module (crates-io))

(define-public crate-tezos-smart-rollup-core-0.1.0 (c (n "tezos-smart-rollup-core") (v "0.1.0") (d (list (d (n "mockall") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "0831x0cmvpd4zm1367b54z7k6zfvgb7s8adwxv3cicfgxdlybymg") (f (quote (("testing" "mockall"))))))

(define-public crate-tezos-smart-rollup-core-0.2.0 (c (n "tezos-smart-rollup-core") (v "0.2.0") (d (list (d (n "mockall") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "149a9wzwpzj50w2vbcghabk6h19m4pyvs9v4b16dw74jmljx3ch1") (f (quote (("testing" "mockall") ("proto-nairobi"))))))

(define-public crate-tezos-smart-rollup-core-0.2.1 (c (n "tezos-smart-rollup-core") (v "0.2.1") (d (list (d (n "mockall") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0.11.0") (d #t) (k 2)))) (h "1cfbaf2zk4wdkpxxa0jgw9zslgzm545bpm694y1mscbcjdyad0k9") (f (quote (("testing" "mockall") ("proto-nairobi"))))))

(define-public crate-tezos-smart-rollup-core-0.2.2 (c (n "tezos-smart-rollup-core") (v "0.2.2") (d (list (d (n "mockall") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "11wqj2as6rcm81g8mj6vxpprhy5kbadyblihb5ds64zrkb3j0d61") (f (quote (("testing" "mockall") ("proto-nairobi") ("proto-alpha"))))))

