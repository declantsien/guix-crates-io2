(define-module (crates-io te zo tezos-smart-rollup-debug) #:use-module (crates-io))

(define-public crate-tezos-smart-rollup-debug-0.1.0 (c (n "tezos-smart-rollup-debug") (v "0.1.0") (d (list (d (n "tezos-smart-rollup-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tezos-smart-rollup-core") (r "^0.1.0") (f (quote ("testing"))) (d #t) (k 2)) (d (n "tezos-smart-rollup-host") (r "^0.1.0") (k 0)))) (h "1zpi7179g3npbw3ss2z743a30w2hyjbphm4811yypw6ibp6kmb8w") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-tezos-smart-rollup-debug-0.2.0 (c (n "tezos-smart-rollup-debug") (v "0.2.0") (d (list (d (n "tezos-smart-rollup-core") (r "^0.2.0") (d #t) (k 0)) (d (n "tezos-smart-rollup-core") (r "^0.2.0") (f (quote ("testing"))) (d #t) (k 2)) (d (n "tezos-smart-rollup-host") (r "^0.2.0") (k 0)))) (h "1h6f06xm86c5l785vphgg68yj8535z4zsz4i2w4v3x0h0wgqc201") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-tezos-smart-rollup-debug-0.2.1 (c (n "tezos-smart-rollup-debug") (v "0.2.1") (d (list (d (n "tezos-smart-rollup-core") (r "^0.2.1") (d #t) (k 0)) (d (n "tezos-smart-rollup-core") (r "^0.2.1") (f (quote ("testing"))) (d #t) (k 2)) (d (n "tezos-smart-rollup-host") (r "^0.2.1") (k 0)))) (h "1chl0gpmnld37n7mi6z1hyhissyp1h4wgk4dzk8pxib4lxr8wkg8") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-tezos-smart-rollup-debug-0.2.2 (c (n "tezos-smart-rollup-debug") (v "0.2.2") (d (list (d (n "tezos-smart-rollup-core") (r "^0.2.2") (d #t) (k 0)) (d (n "tezos-smart-rollup-host") (r "^0.2.2") (k 0)))) (h "0zz2xgafr4bnnj2l29l88x100d9wbyyzp9afdl5inw7lj3g7253j") (f (quote (("testing" "tezos-smart-rollup-core/testing") ("proto-alpha" "tezos-smart-rollup-core/proto-alpha" "tezos-smart-rollup-host/proto-alpha") ("default" "alloc") ("alloc"))))))

