(define-module (crates-io te na tenacious) #:use-module (crates-io))

(define-public crate-tenacious-0.0.1 (c (n "tenacious") (v "0.0.1") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "02cq3vamjax908ghh0a1h5kmnhc7rdcls583xidhjbqxwmn7hjq1")))

(define-public crate-tenacious-0.0.2 (c (n "tenacious") (v "0.0.2") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "0lf1dm3vzq2vh8mw0d0sy4jg5bnngccpbx4pp61dgjbw70npj0fg")))

(define-public crate-tenacious-0.0.3 (c (n "tenacious") (v "0.0.3") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "0r0dkmjngpp1vj1dzi3m35x602zc79gsvh6k75bja20mjrq9zkdp")))

(define-public crate-tenacious-0.0.4 (c (n "tenacious") (v "0.0.4") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "13xjim1v4wklrw633x034skzkdbhw2cjxi0lx8ihb1hlqlrp7yif")))

(define-public crate-tenacious-0.0.5 (c (n "tenacious") (v "0.0.5") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "1a2jdz9zrad4l1r0f0fnhjk520l0xqc5jzg5jqc594x32fmhngm8")))

(define-public crate-tenacious-0.0.6 (c (n "tenacious") (v "0.0.6") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "038rkldgl6xls3sk78gmid3lpcf370xsdwdaxk63yjk16q6fzs16")))

(define-public crate-tenacious-0.0.7 (c (n "tenacious") (v "0.0.7") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "0q2lxxxw15kn2m9m52r54bhs98hjfw8mkv21i0nyfb5940jb0d84")))

(define-public crate-tenacious-0.0.8 (c (n "tenacious") (v "0.0.8") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "1sdnv32wv3wpw4zb2if27xfzk8f04xff0fx8v56ra1m7xhmj0gpp") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.9 (c (n "tenacious") (v "0.0.9") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "123jj7z0yscawnjqdpx9n8iqcml0w2zv2ivpzp5v1b2wwwk74lpk") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.10 (c (n "tenacious") (v "0.0.10") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "1cjcgx34qhdnakkkxqvpgnil0kyx3pq83h0qrfha0nkpxqc8jkbw") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.11 (c (n "tenacious") (v "0.0.11") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "1apfinhfady487n5hyhm1j54g4fsp8jpdjwwvz2cki2mvhzrcsx4") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.12 (c (n "tenacious") (v "0.0.12") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "0k6l8mqcgg8wdvy0ri0d5k47xpkwszlbyj3lnk79sw8v58khfqi8") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.13 (c (n "tenacious") (v "0.0.13") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "1qvrh7pwqn2sqp3dqw4kg856ac2r4m3kn7jjqgvnnw44aswam7yx") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.14 (c (n "tenacious") (v "0.0.14") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "1h1j3xn0m17akgnb71x0m1cywshxgjn9yvavz7qimxxbxrnn52w0") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.15 (c (n "tenacious") (v "0.0.15") (d (list (d (n "compiletest_rs") (r "*") (d #t) (k 2)))) (h "05i48yfkkjx449hqpw7zf07xcnlp02i66f0vy9kwk7d6l7hvwc87") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.1.0 (c (n "tenacious") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.0.11") (d #t) (k 2)))) (h "10idlgpsmm37cxindrsclzcvxpsnyxw5v2v53msfj3kmrmqmfzda") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.1.1 (c (n "tenacious") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.1.0") (d #t) (k 2)))) (h "1ar4sicn7mnxs6szxwz6c11b9wcv9zhjmll4djs6x12c858am8hz") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.1.2 (c (n "tenacious") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.1.0") (d #t) (k 2)))) (h "0cbygk79xygvj4rn7ffx8axwg6bxsbdl3b4s9pkwdjn4rmw5rgzw") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.2.0 (c (n "tenacious") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.1.0") (d #t) (k 2)))) (h "120n7bswbnalgl7v4pyc1yyv5bj1h0f6fgr265p8q4n7djs07hlb") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.2.1 (c (n "tenacious") (v "0.2.1") (d (list (d (n "compiletest_rs") (r "^0.1.0") (d #t) (k 2)))) (h "0ny4vhfw2zd58bprqy77lq0655wk0ikckxqhwgsn98n5g4f7jidr") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.2.2 (c (n "tenacious") (v "0.2.2") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)))) (h "02y24vlkkp5z69pl4vx0n7d8lk2j3r223cb239sq2sp759gy7832") (f (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.2.3 (c (n "tenacious") (v "0.2.3") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)))) (h "0mh4r0dx27qv507xrnqb990m645q771s8i5vk8i3h9vd1ljps6m6") (f (quote (("rvalue_checks"))))))

