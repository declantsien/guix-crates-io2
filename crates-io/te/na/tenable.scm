(define-module (crates-io te na tenable) #:use-module (crates-io))

(define-public crate-tenable-0.1.0 (c (n "tenable") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "rustls-tls"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)))) (h "1rm8gd2c3gzi682018nkjxm36pb8am5qwc5vzqmqfhzk82w5ap7i")))

(define-public crate-tenable-0.1.1 (c (n "tenable") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "rustls-tls"))) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)))) (h "0fpcd9kn74ln5zlxm5sr3rar1gfx2lhy21g7acs7l7730aqrqmz7")))

