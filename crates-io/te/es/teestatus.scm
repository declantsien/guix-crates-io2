(define-module (crates-io te es teestatus) #:use-module (crates-io))

(define-public crate-teestatus-0.1.0 (c (n "teestatus") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "087vd0n2x6zmgd4biwv545nnzfx8i31rkav22wp53g0imz0nnn2q")))

(define-public crate-teestatus-0.1.1 (c (n "teestatus") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "157x009izs0bp0vhg12xlxh70wd08s60jahxbdfn7kr8dz48gxcy")))

(define-public crate-teestatus-0.1.2 (c (n "teestatus") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1f8wr4z94y5bnj5c59swhbn9xnsd3kpiz8h55b29g7i5qg1xc44y")))

(define-public crate-teestatus-0.2.0-alpha.1 (c (n "teestatus") (v "0.2.0-alpha.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1ipzh9svr0m1k64xzhihhiz7qmd96g5qscc39bp1ybplkyn79439")))

(define-public crate-teestatus-0.2.0-alpha.2 (c (n "teestatus") (v "0.2.0-alpha.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1gcvgr23b8cah9fa2fn3h3vybbhp5ys61psbvqjbh760fv9pdgv6")))

(define-public crate-teestatus-0.2.1 (c (n "teestatus") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1zsw4n5fwzkraxzg1m7w3gz5y3lqlfzmi4g3vh2b1fihvsq6kixg")))

