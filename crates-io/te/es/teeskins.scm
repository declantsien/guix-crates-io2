(define-module (crates-io te es teeskins) #:use-module (crates-io))

(define-public crate-teeskins-0.1.0 (c (n "teeskins") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "02h883lai2vhq9gvdjf758hc66m0l4ma2bgrqa2jmpzh7x8n6a9z")))

