(define-module (crates-io te za tezaursapi) #:use-module (crates-io))

(define-public crate-tezaursapi-0.1.0 (c (n "tezaursapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "193ah43gp18vk2k2913hrrdfnvgy2lrzr0j84gjvzynhsd14wppw")))

(define-public crate-tezaursapi-0.1.1 (c (n "tezaursapi") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0z8jj2dqdpc0cfzjj2gnb4y6ry7f96jj7pwgg0400cgl1ryfs8rw")))

(define-public crate-tezaursapi-0.1.2 (c (n "tezaursapi") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "08wfkxfid75snwfnm537b8d33g437jqhn00mc3gkchf6qfcgvy8r")))

(define-public crate-tezaursapi-0.1.3 (c (n "tezaursapi") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1f74607pscdr8zc62j2sj0sqdxryagbfbf1r27493wcxs95lbxbn")))

