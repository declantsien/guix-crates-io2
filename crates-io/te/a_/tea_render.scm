(define-module (crates-io te a_ tea_render) #:use-module (crates-io))

(define-public crate-tea_render-0.1.0 (c (n "tea_render") (v "0.1.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)))) (h "1r15adrh2c2k8cniqkizdqnz40qdvzr4m2j9dam976cysh49h5zd")))

(define-public crate-tea_render-0.1.1 (c (n "tea_render") (v "0.1.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)))) (h "1g0kqa6i51rfwxsdfzy965ca35wvkdanx0y9196c4cmmakx4h1p4")))

