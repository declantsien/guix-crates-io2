(define-module (crates-io te ln telnet-codec) #:use-module (crates-io))

(define-public crate-telnet-codec-0.1.0 (c (n "telnet-codec") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "05dqbff2jrfxn518bvidx466v12808vqnj2f34dsrnyrqfz2sb4r")))

