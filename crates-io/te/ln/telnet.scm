(define-module (crates-io te ln telnet) #:use-module (crates-io))

(define-public crate-telnet-0.1.0 (c (n "telnet") (v "0.1.0") (h "06xqwfanwj8dhxqxdpmf8fqiwy4308p31j1j7gr5ww5miwrnd4h5")))

(define-public crate-telnet-0.1.1 (c (n "telnet") (v "0.1.1") (h "0gfb27va7w908ccy8hm0gcbwip46cvkm1x2bnrl7nsfsay4gyq9v")))

(define-public crate-telnet-0.1.2 (c (n "telnet") (v "0.1.2") (h "1lz9maff2c7gx33n3hqf9hz6krvg5466jziy6pahz71zy9i6jl79")))

(define-public crate-telnet-0.1.3 (c (n "telnet") (v "0.1.3") (h "1dw88gk133yzlphnnf3sa4pc75c7sxk1z34f4hdawi8jjmkvlbcm")))

(define-public crate-telnet-0.1.4 (c (n "telnet") (v "0.1.4") (d (list (d (n "flate2") (r "^1.0.9") (o #t) (d #t) (k 0)) (d (n "replace_with") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1mdr1j6751dcliz6vk6r63bxq4gp2zacyjvpjw0y6yr1r7x6dd1y") (f (quote (("zcstream" "flate2" "replace_with"))))))

(define-public crate-telnet-0.2.0 (c (n "telnet") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0.22") (o #t) (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "13ldg6rn68bi826gxysn36ijlaq9q0v9wdzvp2ph0xf4ifqjgaas") (f (quote (("zcstream" "flate2" "replace_with"))))))

(define-public crate-telnet-0.2.1 (c (n "telnet") (v "0.2.1") (d (list (d (n "flate2") (r "^1.0.22") (o #t) (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1wx4d0y0plbdmvnkj2983l88f6x402b2r0xlbaaijlpa1ckcrwlr") (f (quote (("zcstream" "flate2" "replace_with"))))))

