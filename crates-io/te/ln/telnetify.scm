(define-module (crates-io te ln telnetify) #:use-module (crates-io))

(define-public crate-telnetify-0.1.0 (c (n "telnetify") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1i2fcswndi68mfsh86kdgvpxiy10zy4ff43bn8pcq0v68aqp68vl")))

(define-public crate-telnetify-0.1.1 (c (n "telnetify") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1akwyjzfqn8z44x059skrzhxav1z9dqc5608f65q9shspdc54cyx")))

