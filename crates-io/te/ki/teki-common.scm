(define-module (crates-io te ki teki-common) #:use-module (crates-io))

(define-public crate-teki-common-0.3.0 (c (n "teki-common") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "1liwnfdcwh8pwwrrf6w8fj3xbhmzlk74pq22bdi5hvshqpi648fz")))

