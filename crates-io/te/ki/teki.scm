(define-module (crates-io te ki teki) #:use-module (crates-io))

(define-public crate-teki-0.1.0 (c (n "teki") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (f (quote ("image"))) (k 0)) (d (n "specs") (r "^0.16.1") (f (quote ("specs-derive"))) (d #t) (k 0)))) (h "0cr8bzq9q58x02w9w7kiwj1ha78wccj0n1k495scz09k4makmr0b")))

(define-public crate-teki-0.2.0 (c (n "teki") (v "0.2.0") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "legion") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (f (quote ("image" "mixer"))) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "1r3imb71sa0v14rx1m0d6m7fcp1bwmx55pj3zdvx3r21vc3579h5")))

