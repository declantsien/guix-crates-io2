(define-module (crates-io te ki teki-ecs) #:use-module (crates-io))

(define-public crate-teki-ecs-0.3.0 (c (n "teki-ecs") (v "0.3.0") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "legion") (r "^0.3.1") (f (quote ("codegen"))) (k 0)) (d (n "teki-common") (r "^0.3.0") (d #t) (k 0)) (d (n "vector2d") (r "^2.2.0") (d #t) (k 0)))) (h "03sciyizsl1xzl75wbwjwvym3aimhj34bxjv6q29qn9nlqkcdq0y") (f (quote (("wasm" "legion/wasm-bindgen"))))))

