(define-module (crates-io te nt tent_proc_macros) #:use-module (crates-io))

(define-public crate-tent_proc_macros-0.0.1 (c (n "tent_proc_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "06kd33hidwnyqcqwd38cs6jwass8p0ha0zb1ddkm29njphk11c8c")))

(define-public crate-tent_proc_macros-0.0.3 (c (n "tent_proc_macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0ff0jjjn4qy9snzkj8hll4ivsv90gk8hk24wpm06k4p97vbr60ai")))

