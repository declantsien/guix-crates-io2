(define-module (crates-io te nt tenthash) #:use-module (crates-io))

(define-public crate-tenthash-0.1.0 (c (n "tenthash") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "0n51mqf1xw6qn8mim454vla02i5wjh53l8mxiw7rxkmhn5isbxmd")))

(define-public crate-tenthash-0.2.0 (c (n "tenthash") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "1whxxs9friv17wjqill5fgm9p6164738schaxrlyf1qr989p9wcd")))

(define-public crate-tenthash-0.3.0 (c (n "tenthash") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "0mwxj2jv766arlr3034q1q1cijihbcrxb23gp3732dh83d3hig03")))

