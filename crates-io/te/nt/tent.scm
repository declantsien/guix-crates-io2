(define-module (crates-io te nt tent) #:use-module (crates-io))

(define-public crate-tent-0.0.1 (c (n "tent") (v "0.0.1") (d (list (d (n "tent_proc_macros") (r "^0") (d #t) (k 0)))) (h "0z2nq0nw5lvsj0ns8q58aqn3wkvylgqnsz4ak7phx204dxigrwgh")))

(define-public crate-tent-0.0.2 (c (n "tent") (v "0.0.2") (d (list (d (n "tent_proc_macros") (r "^0") (d #t) (k 0)))) (h "1w52zdsw7l8fpci0xn4pcf6hhh0s6rw4q4xxhn5kzrb6mxy65g3r")))

(define-public crate-tent-0.0.3 (c (n "tent") (v "0.0.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "tent_proc_macros") (r "^0") (d #t) (k 0)))) (h "170gk7279b2vx7vnzyrz9c99iz3wgw7i7z8qrpmvwrn3j87iiic9")))

(define-public crate-tent-0.0.4 (c (n "tent") (v "0.0.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "tent_proc_macros") (r "^0") (d #t) (k 0)))) (h "0l6b3nvibqa7j5q29anl5akv1v8hrd12s6g8msqvwipgr9zzld2p")))

(define-public crate-tent-0.0.5 (c (n "tent") (v "0.0.5") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "tent_codegen") (r "^0") (d #t) (k 0)))) (h "1jsfy7l4sxfxcvqwazqlhj78z9mv1zwm7rq1w3rkbpgpyb3iwsdq")))

(define-public crate-tent-0.0.6 (c (n "tent") (v "0.0.6") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "tent_codegen") (r "^0") (d #t) (k 0)))) (h "0iynqp188sm3mvkdcgsl8dnaig5f9rmdvdcdys6119j6a6wm432n")))

(define-public crate-tent-0.0.7 (c (n "tent") (v "0.0.7") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "tent_codegen") (r "^0") (d #t) (k 0)))) (h "1m009cj9k5xg6r00w5dvc74nq750cz6mw8igjgmq1anpfwb8h3in")))

(define-public crate-tent-0.0.8 (c (n "tent") (v "0.0.8") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "tent_codegen") (r "^0") (d #t) (k 0)))) (h "14km58ajdbprbzbsjqvglgmgiayicjf7l1hffs2a0h2ysl5vmlyw")))

(define-public crate-tent-0.0.9 (c (n "tent") (v "0.0.9") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "tent_codegen") (r "^0") (d #t) (k 0)))) (h "14fycjvs02i62bvg856pzlf88vrz5l127glz8q8zbsfsblxq12j7")))

(define-public crate-tent-0.0.10 (c (n "tent") (v "0.0.10") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "tent_codegen") (r "^0") (d #t) (k 0)))) (h "135hhqjqcs1acmqcd7h7gk60j1z4vyx4g519l4wvjkrx4sf9a9j0")))

(define-public crate-tent-0.0.11 (c (n "tent") (v "0.0.11") (d (list (d (n "tent_codegen") (r "^0.0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 2)))) (h "1sa25zj816fla9q9r3f3y6qzxiz75jv2myjyl97nrcy37anfmvxc")))

