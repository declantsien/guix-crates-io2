(define-module (crates-io te nt tentacli-utils) #:use-module (crates-io))

(define-public crate-tentacli-utils-0.1.0 (c (n "tentacli-utils") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1zw7f3p7f37h92qp4aamm847d7cmnj9djs3b06hfna3ibq1vsi4d")))

(define-public crate-tentacli-utils-1.0.0 (c (n "tentacli-utils") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0wzisjq83as6rw6j0kjh2fj734zs9y9p3cnz4xas7naa71lgs0sb")))

(define-public crate-tentacli-utils-1.1.0 (c (n "tentacli-utils") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0rnk7dk51gl1mrsz8bdhmy5szq10d8cgb7cphwna9fc3pzxjqrxi")))

(define-public crate-tentacli-utils-1.1.1 (c (n "tentacli-utils") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "061ngqz58514gvzhpydz1q7m056jgvyzhcid4z6gdjhh3ksidw3w")))

(define-public crate-tentacli-utils-2.0.0 (c (n "tentacli-utils") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0nr0lwm4kjmcs8bn223cyi96rqvg6a7zdyw3vfskwa407hrnn8l1")))

