(define-module (crates-io te nt tentacli-packet) #:use-module (crates-io))

(define-public crate-tentacli-packet-1.3.2 (c (n "tentacli-packet") (v "1.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "0638h5cnvpzbcy2ngbwc1n39yv3n3719k9rkg30bcbvd3pk1c5a0")))

(define-public crate-tentacli-packet-2.0.0 (c (n "tentacli-packet") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "0kmyya01v1df1kisv7m3m2mqh3wjg8s2h977fh82sz5v9qb12qqc")))

(define-public crate-tentacli-packet-2.0.1 (c (n "tentacli-packet") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "0d1kja9ifyig3zil9khhn3ncfq973h3gj150mlc2vykhgq07yzmr")))

(define-public crate-tentacli-packet-3.0.0 (c (n "tentacli-packet") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "13qwl2lg0xv529z0c82iz3qcvw4ijnsd6siy9blszrr3arlfs1v2")))

(define-public crate-tentacli-packet-4.0.0 (c (n "tentacli-packet") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "0cg7ia30k2a789qmi38kmqhib8v1g0b2fcqlf3wfflgdw2bwxfjs")))

(define-public crate-tentacli-packet-5.0.0 (c (n "tentacli-packet") (v "5.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "structmeta") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full"))) (d #t) (k 0)))) (h "1zl41ki5g0p2zp0gdzhd227891r4a8yn6ng44syqb4lr4m5gfz9r")))

