(define-module (crates-io te nt tent_codegen) #:use-module (crates-io))

(define-public crate-tent_codegen-0.0.5 (c (n "tent_codegen") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "08wvndrr4gmd2jmsi7pm0dlyf3a0siyn5w45n3x6m29njcap7pkf")))

(define-public crate-tent_codegen-0.0.6 (c (n "tent_codegen") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1lz90lxl6q4ay5kjcv7sk9hs77c1c0bypjk8dlds3j8apaz4zrix")))

(define-public crate-tent_codegen-0.0.7 (c (n "tent_codegen") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "011s8ijzslhc3sihqv1k77zbl4kjb9zlzwdb9skkfkj20hl6jlf0")))

(define-public crate-tent_codegen-0.0.8 (c (n "tent_codegen") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1v03vn75jin9938va0jbdwm2vvg5sqlkdankvjkk3l802718nmmr")))

(define-public crate-tent_codegen-0.0.9 (c (n "tent_codegen") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "069f6v484wvvjm7pgigngvrxcmq96pnglimhm42ihrn5ccrkrhb5")))

(define-public crate-tent_codegen-0.0.10 (c (n "tent_codegen") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1ca08qkmwkl2m1rk2j6flpwnj54xdsyd4l9n63383cdxj27s3k6k")))

(define-public crate-tent_codegen-0.0.11 (c (n "tent_codegen") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ynq5hnvmvi65jbxn27kzqg59rmjwkjca2g9zjib77xqda8v6i4z")))

