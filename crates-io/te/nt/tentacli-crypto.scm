(define-module (crates-io te nt tentacli-crypto) #:use-module (crates-io))

(define-public crate-tentacli-crypto-0.1.0 (c (n "tentacli-crypto") (v "0.1.0") (d (list (d (n "hmac-sha") (r "^0.6.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)))) (h "0bbrq41xwks597xfljrl296js0fqld4nyv2kdfswa4b3gas9fa6p")))

