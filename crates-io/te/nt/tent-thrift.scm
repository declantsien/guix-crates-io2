(define-module (crates-io te nt tent-thrift) #:use-module (crates-io))

(define-public crate-tent-thrift-0.18.1 (c (n "tent-thrift") (v "0.18.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (o #t) (d #t) (k 0)))) (h "106irh06ic165lwmvnm4d565867rqmgdmb18vjn813c68wl5f269") (f (quote (("server" "threadpool" "log") ("default" "server"))))))

