(define-module (crates-io te ns tensorflow_proto_derive) #:use-module (crates-io))

(define-public crate-tensorflow_proto_derive-0.1.0 (c (n "tensorflow_proto_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.7") (o #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("derive" "parsing" "printing" "proc-macro" "visit-mut"))) (o #t) (k 0)))) (h "1m432jy09j8ian0rwig2fvb7zvwnp1hd8sxifgr697r0ybwfjsc0") (f (quote (("serde" "quote" "syn") ("convert" "quote" "syn"))))))

