(define-module (crates-io te ns tensorrt-rs-sys) #:use-module (crates-io))

(define-public crate-tensorrt-rs-sys-0.1.0 (c (n "tensorrt-rs-sys") (v "0.1.0") (d (list (d (n "cuda-rs") (r "^0.1") (d #t) (k 0)) (d (n "cxx") (r "^1") (f (quote ("c++17" "c++14"))) (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)))) (h "0dsk37icgmfr4c7rs37kx3lhx25vfq3dldrknhyh6w5rb8lcq45n")))

(define-public crate-tensorrt-rs-sys-0.1.1 (c (n "tensorrt-rs-sys") (v "0.1.1") (d (list (d (n "cuda-rs") (r "^0.1") (d #t) (k 0)) (d (n "cxx") (r "^1") (f (quote ("c++17" "c++14"))) (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)))) (h "1g1dn3mpq4xrbs7zinxr3cxszalv6gsgqbsjlbgzfwmipab4sl8h")))

(define-public crate-tensorrt-rs-sys-0.1.2 (c (n "tensorrt-rs-sys") (v "0.1.2") (d (list (d (n "cuda-rs") (r "^0.1") (d #t) (k 0)) (d (n "cxx") (r "^1") (f (quote ("c++17" "c++14"))) (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)))) (h "0snsh9dlzikqcj3kw0avia7yzlb6h9nbw4h7q34zmabpa92kqgxj")))

