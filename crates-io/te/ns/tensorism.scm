(define-module (crates-io te ns tensorism) #:use-module (crates-io))

(define-public crate-tensorism-0.1.0 (c (n "tensorism") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ly9f4nvbrj9qdyhgkzqrqy9d66kkmalb5zfg98zj2nybwgyy9a0")))

(define-public crate-tensorism-0.1.1 (c (n "tensorism") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "02p7x6cla039hlnj5yqdi1kj7b3dlghdg2jvv4hyij0fnxngiadw")))

(define-public crate-tensorism-0.1.2 (c (n "tensorism") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01grjladq7pn246ly2rrmx8jz5rl6w3fj6p40yvz5zb5kmy37v1a")))

