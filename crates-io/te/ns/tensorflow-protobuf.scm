(define-module (crates-io te ns tensorflow-protobuf) #:use-module (crates-io))

(define-public crate-tensorflow-protobuf-0.0.0 (c (n "tensorflow-protobuf") (v "0.0.0") (d (list (d (n "protobuf") (r "^3.2.0") (d #t) (k 0)) (d (n "safetensors") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "030wxl6jjkrb5rvxsbfps8847r0cx30cwxqmh0hfl25vifs9iy78")))

