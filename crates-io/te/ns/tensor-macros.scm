(define-module (crates-io te ns tensor-macros) #:use-module (crates-io))

(define-public crate-tensor-macros-0.1.0 (c (n "tensor-macros") (v "0.1.0") (h "1cffbcml8h32m1h8vr4wjkjynkl83pki5mb8sysj2zczy0pxjj4s")))

(define-public crate-tensor-macros-0.1.1 (c (n "tensor-macros") (v "0.1.1") (h "19l028b2pzv39bwjdh6f5nbzvbmjk89h190y9fhg3151sy8s0d67")))

(define-public crate-tensor-macros-0.1.2 (c (n "tensor-macros") (v "0.1.2") (h "04dj3grr7179q7i6zdrpmv4n5qyk178vagnxsdxsqk80pybpp5j3")))

(define-public crate-tensor-macros-0.1.3 (c (n "tensor-macros") (v "0.1.3") (h "0mp0d9bs0pbs1vz6i9qw6nm4029afg5glkrm220a9f5wlv4pwarw")))

(define-public crate-tensor-macros-0.1.4 (c (n "tensor-macros") (v "0.1.4") (h "1bwzyxmbvi4jga789hc84y9lxcg4viz34dlnq33mx0i9slh78awr")))

(define-public crate-tensor-macros-0.1.5 (c (n "tensor-macros") (v "0.1.5") (h "1cb80540683kwmmmybln2qc039yiw5g292qkzvp92s9h9bs77491")))

(define-public crate-tensor-macros-0.1.6 (c (n "tensor-macros") (v "0.1.6") (h "14jjpmhhgyfd6dnk6c88y571ggl2lky0gcna3z0vfn8ifvglx343")))

(define-public crate-tensor-macros-0.1.7 (c (n "tensor-macros") (v "0.1.7") (h "0cnh13sar3fkg1hy37c5fbkdbxv12m1qx623a40f7gp16p12f6z7")))

(define-public crate-tensor-macros-0.2.0 (c (n "tensor-macros") (v "0.2.0") (h "0f05x3f3myhq4gm3nwniqacrgsiyhpx0bim561bbx76ajwc774va")))

