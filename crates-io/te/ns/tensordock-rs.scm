(define-module (crates-io te ns tensordock-rs) #:use-module (crates-io))

(define-public crate-tensordock-rs-0.1.0 (c (n "tensordock-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.173") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z3l6sqx2hdnnf39rzxbcb9plfd7sx2wmc5mnbh2ah9f06dm434q")))

(define-public crate-tensordock-rs-0.1.1 (c (n "tensordock-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.173") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fyxzf6w4z5mazac1rssql9cbnnf2s4ii3px6b5jwdlmc2y745d3")))

