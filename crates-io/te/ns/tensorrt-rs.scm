(define-module (crates-io te ns tensorrt-rs) #:use-module (crates-io))

(define-public crate-tensorrt-rs-0.1.1 (c (n "tensorrt-rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "tensorrt-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0g552sndaq1xgl63q98am9hwgbhj9lll6g2ywpmk5z364ly2q8y8")))

(define-public crate-tensorrt-rs-0.2.0 (c (n "tensorrt-rs") (v "0.2.0") (d (list (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-image") (r "^0.2.1") (d #t) (k 0)) (d (n "tensorrt-sys") (r "^0.2.0") (d #t) (k 0)))) (h "12f6rn03sdjrwfm94ny2iib8s9xpcggghnamd1gnb7njvmm6aly1")))

(define-public crate-tensorrt-rs-0.2.1 (c (n "tensorrt-rs") (v "0.2.1") (d (list (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-image") (r "^0.2.1") (d #t) (k 0)) (d (n "tensorrt-sys") (r "^0.2.1") (d #t) (k 0)))) (h "047ps9r0qdfgcvggsh8gc1cdrsap6g5lll6fvfapkh1h8p0chxzk")))

(define-public crate-tensorrt-rs-0.3.0 (c (n "tensorrt-rs") (v "0.3.0") (d (list (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "imageproc") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "ndarray-image") (r "^0.2.1") (d #t) (k 0)) (d (n "tensorrt-sys") (r "^0.3.0") (d #t) (k 0)))) (h "065xzlbnv48mi1220h13q9yxqp6gisl8vl3p9s4lqrn853zl7622")))

