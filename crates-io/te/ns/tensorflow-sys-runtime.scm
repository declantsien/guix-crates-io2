(define-module (crates-io te ns tensorflow-sys-runtime) #:use-module (crates-io))

(define-public crate-tensorflow-sys-runtime-0.2.0 (c (n "tensorflow-sys-runtime") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "06rd225ygrjfn1w1asxzwyny9xc2fyrs8rd1l4bkbfl925bwd6nr")))

