(define-module (crates-io te ns tensorism-gen) #:use-module (crates-io))

(define-public crate-tensorism-gen-0.1.0 (c (n "tensorism-gen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)) (d (n "tensorism") (r "^0.1.0") (d #t) (k 0)))) (h "108sw9qpnh7crhl051z93s3rgnv6564901h2rjk609rx4jk6s348")))

(define-public crate-tensorism-gen-0.1.2 (c (n "tensorism-gen") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)) (d (n "tensorism") (r "^0.1.0") (d #t) (k 0)))) (h "1ry2fxknv8sqk67s76rw0mn8hgrsw5wpa8hj8prh528i70m7ci86")))

