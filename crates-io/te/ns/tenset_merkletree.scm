(define-module (crates-io te ns tenset_merkletree) #:use-module (crates-io))

(define-public crate-tenset_merkletree-0.1.0 (c (n "tenset_merkletree") (v "0.1.0") (h "17y19may92m0y8lxz1hq8b737frk3jykp0i9kjy4gkkhyy6d6wsm")))

(define-public crate-tenset_merkletree-0.1.1 (c (n "tenset_merkletree") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.30.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0zh18hz5qjg1kabk42msic6yc1f580ki178ilpw53ja1ghb6man4")))

