(define-module (crates-io te ns tensorflow-internal-macros) #:use-module (crates-io))

(define-public crate-tensorflow-internal-macros-0.0.1 (c (n "tensorflow-internal-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.36") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ad4cf0b26723y0ak2ny0vmgi1gj25r1ki6b96r2ig7qk839wjrz")))

(define-public crate-tensorflow-internal-macros-0.0.2 (c (n "tensorflow-internal-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0smi0bayl52r842xjqwc4aal16cza5sjr5vyhz27v311jmdsjqy2")))

(define-public crate-tensorflow-internal-macros-0.0.3 (c (n "tensorflow-internal-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0m7b5k4pz1bl833zr7my0w4pwbp9067bqzh0pxy5mh6rnrswjxa9")))

