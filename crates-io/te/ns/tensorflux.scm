(define-module (crates-io te ns tensorflux) #:use-module (crates-io))

(define-public crate-tensorflux-0.0.1 (c (n "tensorflux") (v "0.0.1") (d (list (d (n "tensorflow-sys") (r "^0.2") (d #t) (k 0)))) (h "1s6ka438limpj8lnnaxbq5zmmmm48qv73v4hzlz5da1n7mz69d39")))

(define-public crate-tensorflux-0.0.2 (c (n "tensorflux") (v "0.0.2") (d (list (d (n "tensorflow-sys") (r "^0.3") (d #t) (k 0)))) (h "1ijkpmqy6sp0w1ak2sj89023scxb9z93gwm4w038wf1kj29hd4h0")))

(define-public crate-tensorflux-0.1.0 (c (n "tensorflux") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tensorflow-sys") (r "^0.3") (d #t) (k 0)))) (h "1fx83hkyqadqlwb7fwk7v1p4x2g967b72r7iskza8q7mbh9jsvbk")))

(define-public crate-tensorflux-0.1.1 (c (n "tensorflux") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tensorflow-sys") (r "^0.3") (d #t) (k 0)))) (h "0yggmq2qgw5swlwyswnxaagibnwkadd29fhgahad535811m18biw")))

(define-public crate-tensorflux-0.2.0 (c (n "tensorflux") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tensorflow-sys") (r "^0.3") (d #t) (k 0)))) (h "1ip6zhj16lwrx2fy3fswla5g50kqp51yp80h245xj5h47r5pwx4y")))

(define-public crate-tensorflux-0.2.1 (c (n "tensorflux") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tensorflow-sys") (r "^0.3") (d #t) (k 0)))) (h "1qmmwyp0pcb7lzg13byvrbjxrpmpvwlp513fzdiwdhv55ip9nbvd")))

(define-public crate-tensorflux-0.3.0 (c (n "tensorflux") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tensorflow-sys") (r "^0.3") (d #t) (k 0)))) (h "08r5b5iz5m1fm7i2fyzmr5pgv2qf33khz6a56xcdnh8arj8gjvag")))

(define-public crate-tensorflux-0.4.0 (c (n "tensorflux") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tensorflow-sys") (r "^0.3") (d #t) (k 0)))) (h "11klqx1991qsr9a9bbwh1gm1pf89d8q5qwwb7r35mj1df2sgl8af")))

(define-public crate-tensorflux-0.4.1 (c (n "tensorflux") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (o #t) (k 0)) (d (n "tensorflow-sys") (r "^0.3") (d #t) (k 0)))) (h "1b7ms5dal86yzfac8lvjrzv6yfdf2ppimdqcmpw5y3v72n4jh7cm") (f (quote (("complex" "num-complex"))))))

(define-public crate-tensorflux-0.5.0 (c (n "tensorflux") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (o #t) (k 0)) (d (n "random") (r "^0.11") (d #t) (k 2)) (d (n "tensorflow-sys") (r "^0.3") (d #t) (k 0)))) (h "1sqiknryv0bshwwzbgnpfcbl0lqk6dghl3k1ziv7sm3v2yqjbb0h") (f (quote (("complex" "num-complex"))))))

(define-public crate-tensorflux-0.6.0 (c (n "tensorflux") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (o #t) (k 0)) (d (n "random") (r "^0.11") (d #t) (k 2)) (d (n "tensorflow-sys") (r "^0.3") (d #t) (k 0)))) (h "0aqy22s6xps04382i6yqzs3pivlc1rmi8b0x395zb4qcj8svh26j") (f (quote (("complex" "num-complex"))))))

(define-public crate-tensorflux-0.7.0 (c (n "tensorflux") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (o #t) (k 0)) (d (n "random") (r "^0.11") (d #t) (k 2)) (d (n "tensorflux-sys") (r "^0.1") (d #t) (k 0)))) (h "1nzj5rxyjvcikab4rvh1hk0m77w5pdwws11mnwk9q5k6b37agzqf") (f (quote (("complex" "num-complex"))))))

(define-public crate-tensorflux-0.8.0 (c (n "tensorflux") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (o #t) (k 0)) (d (n "random") (r "^0.11") (d #t) (k 2)) (d (n "tensorflux-sys") (r "^0.1") (d #t) (k 0)))) (h "0hbimvykz1dcagx0drl71y4ylkqpliix1qyg2rlh0z502z4zvmyy") (f (quote (("complex" "num-complex"))))))

(define-public crate-tensorflux-0.8.1 (c (n "tensorflux") (v "0.8.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (o #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "tensorflux-sys") (r "^0.1") (d #t) (k 0)))) (h "12r3ayfdi5br97k9mb4g807mxhhaj3wnraxxci33jr646np08b7k") (f (quote (("complex" "num-complex"))))))

(define-public crate-tensorflux-0.8.2 (c (n "tensorflux") (v "0.8.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (o #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "tensorflux-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0hxyyfxlbsssa9w3gd0nn4iil6fbks3b4ynvfx74l2y8sgi3rwgn") (f (quote (("complex" "num-complex"))))))

