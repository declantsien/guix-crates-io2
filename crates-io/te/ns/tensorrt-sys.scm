(define-module (crates-io te ns tensorrt-sys) #:use-module (crates-io))

(define-public crate-tensorrt-sys-0.1.1 (c (n "tensorrt-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.41") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0cd5f1kikb9a6f3lmskplrn1v6yn8wsk87hsiigb2dc03c77m65h")))

(define-public crate-tensorrt-sys-0.2.0 (c (n "tensorrt-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.41") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1p8mqz8j8ffjgfrvlj1knxx854ai5kvn95gsihrgy3li4k8yjwlz")))

(define-public crate-tensorrt-sys-0.2.1 (c (n "tensorrt-sys") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.41") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1g4dnspah4rm2g43x0vvpn8gj5l6s737722a44vap14lqm6mqi14")))

(define-public crate-tensorrt-sys-0.3.0 (c (n "tensorrt-sys") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.41") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0izbm2lb97mzy646ik8md1ady0xb111jyn4lkyd6cq9hj30ssvm5") (f (quote (("trt-713") ("trt-515" "cuda-101") ("default" "trt-515") ("cuda-110") ("cuda-102") ("cuda-101"))))))

