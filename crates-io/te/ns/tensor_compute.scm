(define-module (crates-io te ns tensor_compute) #:use-module (crates-io))

(define-public crate-tensor_compute-0.1.0 (c (n "tensor_compute") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "blocking") (r "^0.4.7") (d #t) (k 0)) (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.6.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)) (d (n "zerocopy") (r "^0.3.0") (d #t) (k 0)))) (h "0ba6apzwy9iq7i4s02bdpfd8f1gvw4a9n8nrnbprgdgrmkdhr34n")))

