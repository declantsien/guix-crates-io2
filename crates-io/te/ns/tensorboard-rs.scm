(define-module (crates-io te ns tensorboard-rs) #:use-module (crates-io))

(define-public crate-tensorboard-rs-0.1.1 (c (n "tensorboard-rs") (v "0.1.1") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14") (d #t) (k 2)))) (h "06pg7ig86pfy755n14d0pfc43ibkdfg10wgnmqm6mnfmrw08qbvi")))

(define-public crate-tensorboard-rs-0.1.2 (c (n "tensorboard-rs") (v "0.1.2") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14") (d #t) (k 2)))) (h "0hpg9fqd682lhbzcdz4g4r4lg84mzxdwgwbqrq156014kwcw30c1")))

(define-public crate-tensorboard-rs-0.2.0 (c (n "tensorboard-rs") (v "0.2.0") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14") (d #t) (k 2)))) (h "0drssls763wd7hnqgrfa4zrgkzs9ws91j94lhdz3v70pz0ai6x4d")))

(define-public crate-tensorboard-rs-0.2.1 (c (n "tensorboard-rs") (v "0.2.1") (d (list (d (n "auto-diff") (r "^0.3.3") (d #t) (k 2)) (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14") (d #t) (k 2)))) (h "06z162gsmgbzd8sm4saghisy46y1laniw1hgz75wqsivrd7rzhpk")))

(define-public crate-tensorboard-rs-0.2.2 (c (n "tensorboard-rs") (v "0.2.2") (d (list (d (n "auto-diff") (r "^0.3.3") (d #t) (k 2)) (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14") (d #t) (k 2)))) (h "0r4j9b55nzjjgwmgnn1fii360x71y3hqlh46nzxibvggdw525w3s")))

(define-public crate-tensorboard-rs-0.2.3 (c (n "tensorboard-rs") (v "0.2.3") (d (list (d (n "auto-diff") (r "^0.3.3") (d #t) (k 2)) (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14") (d #t) (k 2)))) (h "1mrjamgcs3a571kr4gs87hwgs6bqsfxlry7v7989611c81c9k9wi")))

(define-public crate-tensorboard-rs-0.2.4 (c (n "tensorboard-rs") (v "0.2.4") (d (list (d (n "auto-diff") (r "^0.3.3") (d #t) (k 2)) (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14") (d #t) (k 2)))) (h "0p4jkc4h7mgpyxymr3sh08qq9jaqyv0l43kv847zicg6ag9bqrda")))

(define-public crate-tensorboard-rs-0.5.6 (c (n "tensorboard-rs") (v "0.5.6") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "tensorboard-proto") (r "^0.5.6") (d #t) (k 0)))) (h "0i0sd8vmrc6msxap7z3wmm277rsb8z451pjkzl0l79gswv9zi6bf")))

(define-public crate-tensorboard-rs-0.5.7 (c (n "tensorboard-rs") (v "0.5.7") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "tensorboard-proto") (r "^0.5.7") (d #t) (k 0)))) (h "1fs5m7ymd0pw0cpgj8a1d45xf2wvcf2994dc1g148038k4zmpzjp")))

(define-public crate-tensorboard-rs-0.5.8 (c (n "tensorboard-rs") (v "0.5.8") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "tensorboard-proto") (r "^0.5.7") (d #t) (k 0)))) (h "100mz4q67aya6xs22qg43hhr7yndmr529fzrpzpddv6pkd5ddj05")))

(define-public crate-tensorboard-rs-0.5.9 (c (n "tensorboard-rs") (v "0.5.9") (d (list (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "tensorboard-proto") (r "^0.5.7") (d #t) (k 0)))) (h "019rwwpl80926046y9a6l2payfxbhis4wd2ynqdvgg9wgj7vb74q")))

