(define-module (crates-io te ns tensorflow-serving) #:use-module (crates-io))

(define-public crate-tensorflow-serving-0.1.0 (c (n "tensorflow-serving") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.4") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 2)))) (h "0gp1sf2bv592kmymlbfy2xny1kvm860jnbczc8jv7iwvk79px9bd")))

