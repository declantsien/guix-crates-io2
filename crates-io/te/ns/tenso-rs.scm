(define-module (crates-io te ns tenso-rs) #:use-module (crates-io))

(define-public crate-tenso-rs-0.0.1 (c (n "tenso-rs") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13p8g3byj9rrfxgbbyx7rrrnz9xq7knfvf3abvrfs8qrg17rs0p7")))

