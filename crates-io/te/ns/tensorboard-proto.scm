(define-module (crates-io te ns tensorboard-proto) #:use-module (crates-io))

(define-public crate-tensorboard-proto-0.5.5 (c (n "tensorboard-proto") (v "0.5.5") (d (list (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14") (d #t) (k 2)))) (h "1nn2bnva9cfwb2i9ip6gd1yyszlw41nnh203gz4sqnxz3y6frpsj")))

(define-public crate-tensorboard-proto-0.5.6 (c (n "tensorboard-proto") (v "0.5.6") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2") (d #t) (k 2)))) (h "1az6bn6wm8vgrw2y81721zq6bnyy4x88y956g08pcy0w0fiv2ffr")))

(define-public crate-tensorboard-proto-0.5.7 (c (n "tensorboard-proto") (v "0.5.7") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2") (d #t) (k 2)))) (h "0zs2jdgharwqbhbss7zjnyra45p1jinih6iiy3pn6v3fjpn8iwgy")))

