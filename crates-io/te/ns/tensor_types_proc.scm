(define-module (crates-io te ns tensor_types_proc) #:use-module (crates-io))

(define-public crate-tensor_types_proc-0.9.0 (c (n "tensor_types_proc") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "tch") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0dwwhsg6ry0nq91pklil7z2493m2y0r18d1zclqrzbjrjwjv9fyc")))

(define-public crate-tensor_types_proc-0.9.1 (c (n "tensor_types_proc") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "tch") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1xxxr301krjrvq949ql64h0arhysb7nqr2vqcr69g6c6522zn74p")))

(define-public crate-tensor_types_proc-1.0.0 (c (n "tensor_types_proc") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "tch") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1pzpnb23mjadwp5ymp19cv4cr052zhz0gqllpi6wby6a3blk89pq")))

