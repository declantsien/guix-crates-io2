(define-module (crates-io te ns tensorflow-serving-client) #:use-module (crates-io))

(define-public crate-tensorflow-serving-client-2.0.0 (c (n "tensorflow-serving-client") (v "2.0.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)))) (h "04n23wp2i49k331n34dvcdshq3ics9lx25bsqxp6sajs4xfadf55")))

(define-public crate-tensorflow-serving-client-1.12.0 (c (n "tensorflow-serving-client") (v "1.12.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)))) (h "0wgihhdcd1crlk4cp7niv19nx74pag869hzg08816bknna4034kf")))

(define-public crate-tensorflow-serving-client-1.13.0 (c (n "tensorflow-serving-client") (v "1.13.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)))) (h "1smcds8n9mqim1gxvy7bhgy75yy46lb5lqqkh5846yhzs8b476zz")))

(define-public crate-tensorflow-serving-client-1.14.0 (c (n "tensorflow-serving-client") (v "1.14.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)))) (h "1836nhlminc4rikhdgzlz52r3ckk46p4m5fn2n2axys621ym0g38")))

(define-public crate-tensorflow-serving-client-1.15.0 (c (n "tensorflow-serving-client") (v "1.15.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)))) (h "1d2ikrw4j3jgq96dymrhjvabpbnyzm72vhqpbhsrwhf0z94nxi4g")))

(define-public crate-tensorflow-serving-client-2.1.0 (c (n "tensorflow-serving-client") (v "2.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.10.1") (d #t) (k 0)))) (h "0i8230dwhbqn3vfxca3z534kv1k5a0b6i9kmak1hsfgdwadhakhb")))

(define-public crate-tensorflow-serving-client-2.2.0 (c (n "tensorflow-serving-client") (v "2.2.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)))) (h "1r6299b8lzv463l204y6jr7qgxcywr4h75gbjsh3g6nnxzj5r185")))

(define-public crate-tensorflow-serving-client-2.3.0 (c (n "tensorflow-serving-client") (v "2.3.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "grpcio") (r "^0.6.0") (d #t) (k 0)) (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)))) (h "06pr6v4dpyvp78bal0r0wlliwfgdkzmf836bi7sv941qxlfdw55x")))

