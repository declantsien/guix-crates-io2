(define-module (crates-io te ns tenshi) #:use-module (crates-io))

(define-public crate-tenshi-0.1.0 (c (n "tenshi") (v "0.1.0") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)))) (h "1m54g0yyz1fy4bddx8gv6cps7bnj8w6frm6mhv40anshvn1j5cvn")))

(define-public crate-tenshi-0.1.1 (c (n "tenshi") (v "0.1.1") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)))) (h "1c50nqasax6h5jpqc9imdy1w2z4k337sgzwwmc2qi6fvxvvggpxg")))

(define-public crate-tenshi-0.1.2 (c (n "tenshi") (v "0.1.2") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)))) (h "0xxwkvhxg5snxvqjb1igb7rkzh6d2r03m3kighxbzp4872zcrsc7")))

(define-public crate-tenshi-0.1.3 (c (n "tenshi") (v "0.1.3") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)))) (h "17py7q4a03cfzixhsv1nv6lnwnll3n90ycm7j6w4fd6l4s7b71sh")))

(define-public crate-tenshi-0.1.4 (c (n "tenshi") (v "0.1.4") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)))) (h "0jacmqd2ksa1ahwh0zh334g6pyaa9b40fhvy4fbspy9vbl426k58")))

(define-public crate-tenshi-0.2.0 (c (n "tenshi") (v "0.2.0") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)))) (h "0va8z1959d6ji3yhgi9xswpgilzrx6p7jigc0p191cj0dz6z1iql")))

(define-public crate-tenshi-0.2.1 (c (n "tenshi") (v "0.2.1") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)))) (h "085ylf31l1ssbaczkbg75m7aihvcph5wrgjzs616wk4csflgbdal")))

(define-public crate-tenshi-0.2.2 (c (n "tenshi") (v "0.2.2") (d (list (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)))) (h "0l2naiqcw3a5cvi1mjb5qqq440bryl9brk6630hgv9zbkx32vw9m")))

