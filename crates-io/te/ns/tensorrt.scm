(define-module (crates-io te ns tensorrt) #:use-module (crates-io))

(define-public crate-tensorrt-0.0.1 (c (n "tensorrt") (v "0.0.1") (h "1kjrza0h8k2n1dz9sx67hn40hqgxz83p4wgkbd9l1ldhjmi94grx")))

(define-public crate-tensorrt-0.1.0 (c (n "tensorrt") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cuda-rs") (r "^0.1") (d #t) (k 0)) (d (n "tch") (r "^0.14.0") (d #t) (k 2)) (d (n "tensorrt-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xp25zb0wqs3sj0vfrjxqcz1a61wpj19wfhybhwlk8daz2wgpkzb")))

