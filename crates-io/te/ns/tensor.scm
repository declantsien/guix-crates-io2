(define-module (crates-io te ns tensor) #:use-module (crates-io))

(define-public crate-tensor-0.1.0 (c (n "tensor") (v "0.1.0") (h "0bxicj88sk749sqc32p857rz1ywg9aix48rc2gcd9ck8g0mfcyzk")))

(define-public crate-tensor-0.1.1 (c (n "tensor") (v "0.1.1") (d (list (d (n "num") (r "^0.1.25") (d #t) (k 0)))) (h "1axr04rk9wichrwcmsqv5l63h4jaw1al821ixyr1i0cw4kp61h2w")))

(define-public crate-tensor-0.1.2 (c (n "tensor") (v "0.1.2") (d (list (d (n "num") (r "^0.1.25") (d #t) (k 0)))) (h "15vwwqzvv5gf1yb4dacbnakigrcxarv01y9nn8n15wqw8zvh7d8m")))

