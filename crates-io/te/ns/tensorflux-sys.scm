(define-module (crates-io te ns tensorflux-sys) #:use-module (crates-io))

(define-public crate-tensorflux-sys-0.1.0 (c (n "tensorflux-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1w441n38j2qrbxchr91ngknk4jq5swsv1lfjmrx39vncxgiab9g9")))

(define-public crate-tensorflux-sys-0.1.1 (c (n "tensorflux-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0vyvnyx355d22ybi9742jkspxh3wh821y754g7saflzy0c2vsr20")))

(define-public crate-tensorflux-sys-0.2.0 (c (n "tensorflux-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1xbkb1w7mvyrpghs6qzg8x8w3rs0im55rz95p5fg4d97pm4498wz")))

(define-public crate-tensorflux-sys-0.3.0 (c (n "tensorflux-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0m87wapp5cja8rw035hv152llrai3pgalkidax966jy3i9n65pfr")))

