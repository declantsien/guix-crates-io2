(define-module (crates-io te cl tecla_common) #:use-module (crates-io))

(define-public crate-tecla_common-0.1.0 (c (n "tecla_common") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "license") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x98y3v1f8lbiwv714ybm7askm2m0hvd5ybx7zm5ckh920vdcgn5")))

(define-public crate-tecla_common-0.1.1 (c (n "tecla_common") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "license") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zzmzvbxpwhva0y76sxplblmdllx8r5m51qxnq5n75a017cxcnzn")))

(define-public crate-tecla_common-0.1.2 (c (n "tecla_common") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "license") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pbsa06y6d33qwb67pxhf2m4xrr34yy1nsfp9yjnwx0z18l3pfxj")))

(define-public crate-tecla_common-0.1.3 (c (n "tecla_common") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "license") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15g8cmw67jkl5dkh3p2pz2hjsj89as98v2vlhz249ylxs8i4h744") (f (quote (("docsrs" "license/offline"))))))

(define-public crate-tecla_common-0.1.4 (c (n "tecla_common") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "license") (r "^2.0") (f (quote ("offline"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18sqcha1gm8h71cbn32fkz8131yqhhasaj8n8dgjlq12qf049735")))

(define-public crate-tecla_common-0.1.5 (c (n "tecla_common") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "license") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z0mxs0217zps0aa0r0308mfzzn1ar8c3rrw3a59ffbsqy68a5sg") (f (quote (("docsrs" "license/offline"))))))

