(define-module (crates-io te cl tecla_client) #:use-module (crates-io))

(define-public crate-tecla_client-0.1.0 (c (n "tecla_client") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tecla_common") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("json"))) (d #t) (k 0)))) (h "156nws3ihjkvyys2p1shw4jav1r715lgdvr813wzifr300m5jfys")))

(define-public crate-tecla_client-0.1.2 (c (n "tecla_client") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tecla_common") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0wjgz978hvd1wmyx2vw3bjskd06p4ifrw9am9ziwna58ajqnn7ay")))

(define-public crate-tecla_client-0.1.3 (c (n "tecla_client") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tecla_common") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "02fq1xwxxr951q034zs4j777hrpvwdgcz1i6i71n9n5xq5d7wwxd")))

(define-public crate-tecla_client-0.1.4 (c (n "tecla_client") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tecla_common") (r "^0.1.4") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0s8ga43v2zszckb3vh54j48abhczdj0mazbipw3x9bzrcv3jmzd1")))

