(define-module (crates-io te tc tetched-node) #:use-module (crates-io))

(define-public crate-tetched-node-0.0.0 (c (n "tetched-node") (v "0.0.0") (h "19ms2qg2cfndp1gxzx5xdjin0lpmmlcin5lghsfzk3wxnp17hx07")))

(define-public crate-tetched-node-0.1.0 (c (n "tetched-node") (v "0.1.0") (h "1w54ql6mg7ihdg203qr0iqvbizk429khdn7ikmm4jkjsymjfz62d")))

