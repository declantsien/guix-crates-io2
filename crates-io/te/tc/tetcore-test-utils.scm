(define-module (crates-io te tc tetcore-test-utils) #:use-module (crates-io))

(define-public crate-tetcore-test-utils-2.0.1 (c (n "tetcore-test-utils") (v "2.0.1") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("compat"))) (d #t) (k 0)) (d (n "tc-service") (r "^0.8.0") (d #t) (k 2)) (d (n "tetcore-test-utils-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.38") (f (quote ("diff"))) (d #t) (k 2)))) (h "0713absq2vdlqkldbyd1xrpzc0znhgqx2izxg5k43xdsz3wfy2k3")))

