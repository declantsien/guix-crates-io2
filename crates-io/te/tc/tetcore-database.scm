(define-module (crates-io te tc tetcore-database) #:use-module (crates-io))

(define-public crate-tetcore-database-0.0.0 (c (n "tetcore-database") (v "0.0.0") (h "0nn157kslk16nvb1wzfgbwvr5chx30sxvnl7q58i8yc235vjsjkm") (y #t)))

(define-public crate-tetcore-database-2.0.1 (c (n "tetcore-database") (v "2.0.1") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.9.0") (d #t) (k 0)))) (h "0cic24f97rlhw2kxfn406bkfppz0v45kc9wvmmhb0w2gx9m5ymz8")))

(define-public crate-tetcore-database-2.0.2 (c (n "tetcore-database") (v "2.0.2") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.9.0") (d #t) (k 0)))) (h "0y177adjb3x57b37l0haizmsq4ajmmil29lcmhrl47vff0ni8nqb")))

(define-public crate-tetcore-database-2.1.2 (c (n "tetcore-database") (v "2.1.2") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tetsy-kvdb") (r "^0.9.1") (d #t) (k 0)))) (h "0xa3n8w2n6jzg9i30980hrry5fkfv65jslrsv92l5n6xnrfgf9rb")))

