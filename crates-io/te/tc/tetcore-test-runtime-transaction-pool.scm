(define-module (crates-io te tc tetcore-test-runtime-transaction-pool) #:use-module (crates-io))

(define-public crate-tetcore-test-runtime-transaction-pool-2.0.0 (c (n "tetcore-test-runtime-transaction-pool") (v "2.0.0") (d (list (d (n "codec") (r "^2.0.1") (d #t) (k 0) (p "tetsy-scale-codec")) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("compat"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tc-transaction-graph") (r "^2.0.0") (d #t) (k 0)) (d (n "tetcore-test-runtime-client") (r "^2.0.0") (d #t) (k 0)) (d (n "tp-blockchain") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-transaction-pool") (r "^2.0.2") (d #t) (k 0)))) (h "1hwanfb8sbw2njq01wx9rbzbhspr2l97vxly7a2jy7mlfzw4a3h5")))

