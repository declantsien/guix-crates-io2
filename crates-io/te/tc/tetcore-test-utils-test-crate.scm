(define-module (crates-io te tc tetcore-test-utils-test-crate) #:use-module (crates-io))

(define-public crate-tetcore-test-utils-test-crate-0.1.0 (c (n "tetcore-test-utils-test-crate") (v "0.1.0") (d (list (d (n "tc-service") (r "^0.8.0") (d #t) (k 2)) (d (n "test-utils") (r "^2.0.0") (d #t) (k 2) (p "tetcore-test-utils")) (d (n "tokio") (r "^0.2.13") (f (quote ("macros"))) (d #t) (k 2)))) (h "0bhmb5g253ywnbv9r5xdsxlan77xq34r4asa9vzvxqj89wy2my0z")))

