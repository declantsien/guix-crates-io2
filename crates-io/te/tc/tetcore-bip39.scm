(define-module (crates-io te tc tetcore-bip39) #:use-module (crates-io))

(define-public crate-tetcore-bip39-0.4.2 (c (n "tetcore-bip39") (v "0.4.2") (d (list (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.3.0") (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 2)) (d (n "schnorrkel") (r "^0.9.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.6.2") (d #t) (k 2)) (d (n "zeroize") (r "^1.0.0") (k 0)))) (h "18pdazy2idpiq763ij7z7jigw1kd39jfnngkxd3x2hq2nh2zc4i9")))

