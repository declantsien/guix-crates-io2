(define-module (crates-io te tc tetcore-fabric-cli) #:use-module (crates-io))

(define-public crate-tetcore-fabric-cli-2.0.1 (c (n "tetcore-fabric-cli") (v "2.0.1") (d (list (d (n "fabric-system") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "tc-cli") (r "^0.8.0") (d #t) (k 0)) (d (n "tet-core") (r "^2.0.2") (d #t) (k 0)) (d (n "tp-runtime") (r "^2.0.2") (d #t) (k 0)))) (h "16ay372gfkpx7vz9fmfm9khmd2zcwc5irvh7qfjknb1mfnmrk9w8") (f (quote (("default"))))))

