(define-module (crates-io te tc tetcore-test-utils-derive) #:use-module (crates-io))

(define-public crate-tetcore-test-utils-derive-0.8.1 (c (n "tetcore-test-utils-derive") (v "0.8.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0rw9x5wnz74w9ckbf9ypz1amzvka62f8qxyrk79xnp5wgwkiirxj")))

