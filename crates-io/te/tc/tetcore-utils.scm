(define-module (crates-io te tc tetcore-utils) #:use-module (crates-io))

(define-public crate-tetcore-utils-0.0.0 (c (n "tetcore-utils") (v "0.0.0") (h "1jysfzy66p4jrfxz9mrwdhm72vpl0afdag0vnafrfrb9cwfp8y8c") (y #t)))

(define-public crate-tetcore-utils-2.0.1 (c (n "tetcore-utils") (v "2.0.1") (d (list (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.10.0") (k 0)))) (h "1vc4xmp5s3wm4l8pdxb1hsifbdhj44nn2b18jy6grh43kqz9lsyg") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-tetcore-utils-2.0.2 (c (n "tetcore-utils") (v "2.0.2") (d (list (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.10.0") (k 0)))) (h "1lyrl3k3ggn1pgnfq0fbw2s6251x9jj6xggpgzilzfinhhc1r4h3") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-tetcore-utils-2.1.2 (c (n "tetcore-utils") (v "2.1.2") (d (list (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.10.0") (k 0)))) (h "0dagnddsyr65xn2v9ji8vmadm3prvkmv0grs5q5mbvs0imix0nvq") (f (quote (("metered") ("default" "metered"))))))

