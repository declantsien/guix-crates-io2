(define-module (crates-io te tc tetcore-std) #:use-module (crates-io))

(define-public crate-tetcore-std-0.0.0 (c (n "tetcore-std") (v "0.0.0") (h "0kcvh3g9fvbmfqckl1y5mba1xy4y0mmq8iii777jq1qk2n3xa29i") (y #t)))

(define-public crate-tetcore-std-2.0.1 (c (n "tetcore-std") (v "2.0.1") (h "0lhsmzyv1p0rwqs8vfa2lligarayqvi0m3a6kpzrm59da9pv2zz3") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetcore-std-2.0.2 (c (n "tetcore-std") (v "2.0.2") (h "199z4j4chs49lfxwz61xm5ymbx172rhk9i2wxhc47al5l8873vks") (f (quote (("std") ("default" "std"))))))

(define-public crate-tetcore-std-2.1.2 (c (n "tetcore-std") (v "2.1.2") (h "01ra4fpldavnym74mil2flsdcasjnr0ly9jsbaspx3xalv4kxmv0") (f (quote (("std") ("default" "std"))))))

