(define-module (crates-io te nr tenrus) #:use-module (crates-io))

(define-public crate-tenrus-0.1.0 (c (n "tenrus") (v "0.1.0") (d (list (d (n "clap") (r "^2.9.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "openssl") (r "^0.7.14") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.7.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "serde") (r "^0.7.14") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.4") (d #t) (k 0)))) (h "1lbi73cd6wbm6agldcddmlgh7bg1jmarpk1qmbbvxb3dhnzp0psv")))

(define-public crate-tenrus-0.1.1 (c (n "tenrus") (v "0.1.1") (d (list (d (n "clap") (r "^2.20.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "hyper") (r "^0.10.0") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "serde") (r "^0.8.22") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.5") (d #t) (k 0)))) (h "03m1ixca37nq0bqwyybqz40zq11lmbw9aw93m82n5lapz5ddyi56")))

