(define-module (crates-io te ci tecio) #:use-module (crates-io))

(define-public crate-tecio-0.1.0 (c (n "tecio") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1m1dgy443iqw2h05qhi5qw1na907jlzfbv6mq9gjwqk60ygq0w8i") (y #t)))

(define-public crate-tecio-0.1.1 (c (n "tecio") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "0ddi4xa8p471vzhc0zgzj0xm5lw5i7gwf35ml1w1b50rgp0hgyka") (y #t)))

(define-public crate-tecio-0.1.2 (c (n "tecio") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1i9ns2f989b8s06m8dwj9akylcdifylrxi9q7h867m7gnfslj1ar") (y #t)))

(define-public crate-tecio-0.2.0 (c (n "tecio") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "16sgc8zfdbsq44c6vcr4wh198fqcnb9ckwgy2kwaz01dnsjg8ign") (y #t)))

(define-public crate-tecio-0.2.1 (c (n "tecio") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "05g60lai1skpqw3251hr81plf3d3wk4nvn8pssvyn33zr9fvkgq1") (y #t)))

(define-public crate-tecio-0.2.2 (c (n "tecio") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1077a708mgxlylnm24axbz2gaxg6g1157zs06v4v0qqybgkxz5i4") (y #t)))

(define-public crate-tecio-0.3.0 (c (n "tecio") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1lwfkh364vfrm0yb5jkayjdf07fi95p3f4hkknl5vnp4ngiy7vds") (y #t)))

