(define-module (crates-io te rr terraform-trustfall-adapter) #:use-module (crates-io))

(define-public crate-terraform-trustfall-adapter-0.1.0 (c (n "terraform-trustfall-adapter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "hcl-rs") (r "^0.16.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "trustfall") (r "^0.7.1") (d #t) (k 0)))) (h "1rpdsgy60301bb0960ri9smh4nm08ndnm4fshmwh1jvdcqyxbfkx")))

