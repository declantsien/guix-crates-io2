(define-module (crates-io te rr terraria-protocol) #:use-module (crates-io))

(define-public crate-terraria-protocol-0.1.0 (c (n "terraria-protocol") (v "0.1.0") (d (list (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0ib8r5lmkz1n9wvzdljx9b82jxz00j04rq1m3habs5xnmvxvf9gy")))

