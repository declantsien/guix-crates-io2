(define-module (crates-io te rr terrasound) #:use-module (crates-io))

(define-public crate-terrasound-0.1.0 (c (n "terrasound") (v "0.1.0") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "1ms3azy9pmvhhr74s132cmhh3g09f14jkgrsh6mslfsmvm5qqvbw")))

(define-public crate-terrasound-0.1.1 (c (n "terrasound") (v "0.1.1") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "02kp2wi79czlgqs3dzkn361xnmrrd6zm9hqnf8jfan574r78fvf4")))

(define-public crate-terrasound-0.1.2 (c (n "terrasound") (v "0.1.2") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "1wl6hhw1zx86jmkfni4yvq1iw84vkq7zrq662r7x3frrr15v9klb")))

(define-public crate-terrasound-0.1.3 (c (n "terrasound") (v "0.1.3") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "1kfijkbf7mga8xl1zriqsv43sj1p4j3wsw6fia7xd25gr9mg49ac")))

(define-public crate-terrasound-0.1.4 (c (n "terrasound") (v "0.1.4") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "0bc7whg6y9rk2adm4h6qyvh04g0q6r8c57n9fdwml8f80gcdhqy1")))

(define-public crate-terrasound-0.2.0 (c (n "terrasound") (v "0.2.0") (d (list (d (n "resonator") (r "^0.2.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "1rsdv48svf31zmwssa5fi85lmgdbyp3511vf3yskbg2lf5j7cbw9")))

(define-public crate-terrasound-0.3.0 (c (n "terrasound") (v "0.3.0") (d (list (d (n "resonator") (r "^0.3.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "0l03lv8qp2jxagw8h5x0clbdz1kywxj8pvgfnv5g82877zvymgry")))

(define-public crate-terrasound-0.3.2 (c (n "terrasound") (v "0.3.2") (d (list (d (n "resonator") (r "^0.3.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "0rfjpcndi8fcib4jk7psiwq3vqynas4zksvr19xwf45rzkqhgg69")))

