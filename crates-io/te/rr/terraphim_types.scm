(define-module (crates-io te rr terraphim_types) #:use-module (crates-io))

(define-public crate-terraphim_types-0.1.0 (c (n "terraphim_types") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.8") (f (quote ("serde"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "opendal") (r "^0.44.2") (f (quote ("services-dashmap" "services-redis" "services-sled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (f (quote ("serde" "uuid"))) (d #t) (k 0)))) (h "0p3571ki5f5xclh707cd8x8g9cn27h92kq131wsmi3js0mqm5knj")))

