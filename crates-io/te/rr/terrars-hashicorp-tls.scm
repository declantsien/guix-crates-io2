(define-module (crates-io te rr terrars-hashicorp-tls) #:use-module (crates-io))

(define-public crate-terrars-hashicorp-tls-0.1.0 (c (n "terrars-hashicorp-tls") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.5") (d #t) (k 0)))) (h "0cv38ln4yjic9h5fmliwyj33v2q9yvazmgjd5w9ai5yszj17bw6j") (f (quote (("self_signed_cert") ("private_key") ("locally_signed_cert") ("data_public_key") ("data_certificate") ("cert_request"))))))

