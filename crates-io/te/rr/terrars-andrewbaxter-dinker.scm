(define-module (crates-io te rr terrars-andrewbaxter-dinker) #:use-module (crates-io))

(define-public crate-terrars-andrewbaxter-dinker-0.1.0 (c (n "terrars-andrewbaxter-dinker") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.9") (d #t) (k 0)))) (h "0lvjl6k1nfwz652z045h4lqvprqakisx8v5sc7dd8slb1kh5lym8") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.2 (c (n "terrars-andrewbaxter-dinker") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.10") (d #t) (k 0)))) (h "1ckdmcw9gqp9yg34sn48wl1xdmm46cl9dk24ji27s5zmgn6691dd") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.4 (c (n "terrars-andrewbaxter-dinker") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.10") (d #t) (k 0)))) (h "1pxzdxwhvala5sw1dvl6rkdh9vnlrzg00h4s8z0gi3ki2g4q0l0w") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.5 (c (n "terrars-andrewbaxter-dinker") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.10") (d #t) (k 0)))) (h "079xhqm6djsvbjgx2q96pcgmla5dzchbclxfss64fyx027yzgkpm") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.6 (c (n "terrars-andrewbaxter-dinker") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.10") (d #t) (k 0)))) (h "1g2v8b48xgcjvf9g8mw55s8ammw045g80sd4v1r90592cjbay95j") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.7 (c (n "terrars-andrewbaxter-dinker") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.0") (d #t) (k 0)))) (h "0gwpsrk2vpiingrvdrcwfvpfxx4amg1rw23qz3kqm553i9hl0pk8") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.8 (c (n "terrars-andrewbaxter-dinker") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.0") (d #t) (k 0)))) (h "0n05g9b9lrs1jmwl9dwyknhcfls885b6fj1lksl0grwdnabykz4j") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.10 (c (n "terrars-andrewbaxter-dinker") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.1") (d #t) (k 0)))) (h "1rkkk3r7sj39jx0vq4g4s73n7lxnrbg9vfbd3bdhyrv6f9zsxn9d") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.11 (c (n "terrars-andrewbaxter-dinker") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.2") (d #t) (k 0)))) (h "03h2wcw17253vxfn4177pvddmfrimqgxki8036pdylfk3mr0zpk3") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.12 (c (n "terrars-andrewbaxter-dinker") (v "0.1.12") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.2") (d #t) (k 0)))) (h "165fgg970s6blj6wqbciayg60q1mlp9cnfb9qwwfdmrphs1lrsig") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.13 (c (n "terrars-andrewbaxter-dinker") (v "0.1.13") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.3") (d #t) (k 0)))) (h "1gsvhbdwichvylr5961p68zvl8qs9qir56jnzl7zqx1xx1x5bxql") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.14 (c (n "terrars-andrewbaxter-dinker") (v "0.1.14") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.4") (d #t) (k 0)))) (h "1sifgg76wcv2f897mw56h9i8fmw0y1wk9h6q3gd6w30vi8wbzxwv") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.15 (c (n "terrars-andrewbaxter-dinker") (v "0.1.15") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.4") (d #t) (k 0)))) (h "1c0imy8fp41i27syhgjpsckj9n2cxzgyh29apgagsxb98l50pgy1") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.16 (c (n "terrars-andrewbaxter-dinker") (v "0.1.16") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.4") (d #t) (k 0)))) (h "1ik3gn96l8lq30xkk9ihfbmxajbn7hqcskq293lprzm6438ycy19") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.17 (c (n "terrars-andrewbaxter-dinker") (v "0.1.17") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.4") (d #t) (k 0)))) (h "16fazvrs5q0mcx0mmk931626bq68qg76l93w88f7v3yrm970rk3d") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.18 (c (n "terrars-andrewbaxter-dinker") (v "0.1.18") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.4") (d #t) (k 0)))) (h "0zk42jhb8adimc7ifh94kqzmf7z5iw7xb91r8d1kq8aih2xz39x9") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.19 (c (n "terrars-andrewbaxter-dinker") (v "0.1.19") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.5") (d #t) (k 0)))) (h "14b19k4kv5m03d45pgc3j9g0sdb7dk199md20d6r02a5v63mk7nc") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.20 (c (n "terrars-andrewbaxter-dinker") (v "0.1.20") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.7") (d #t) (k 0)))) (h "1mx869phb6f3c2mjahksgw8m5lciq9nzkygyad3a17a2ccgyh2y1") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.21 (c (n "terrars-andrewbaxter-dinker") (v "0.1.21") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "terrars") (r "^0.1") (d #t) (k 0)))) (h "05hwhqc3l0cmwh1sb771f9sljq21kz9zizi0imz5rddqj0z7xgsr") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.22 (c (n "terrars-andrewbaxter-dinker") (v "0.1.22") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "terrars") (r "^0.1") (d #t) (k 0)))) (h "0zjyqnjacawyvcprq15lyc7g787j9gv7r76zn69inzcrkf5j2jzy") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.23 (c (n "terrars-andrewbaxter-dinker") (v "0.1.23") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "terrars") (r "^0.1") (d #t) (k 0)))) (h "1ak6r9bgqjxbwkq1r1ax7kf0pwqw6hy9vd50kxmffik306062hdi") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.24 (c (n "terrars-andrewbaxter-dinker") (v "0.1.24") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "terrars") (r "^0.1") (d #t) (k 0)))) (h "1lrwvy59njxw0q5sx0axb8ri7jh9qg2nsayqvy0g3qqin4zj409b") (f (quote (("image"))))))

(define-public crate-terrars-andrewbaxter-dinker-0.1.25 (c (n "terrars-andrewbaxter-dinker") (v "0.1.25") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "terrars") (r "^0.1") (d #t) (k 0)))) (h "0aqa1xc34fv4bd364kij9ja9w8zzxpaj6w0cqjz27d8p0i92xvvv") (f (quote (("image"))))))

