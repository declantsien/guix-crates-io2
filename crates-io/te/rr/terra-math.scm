(define-module (crates-io te rr terra-math) #:use-module (crates-io))

(define-public crate-terra-math-0.0.0 (c (n "terra-math") (v "0.0.0") (d (list (d (n "cosmwasm-schema") (r "^0.10.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.10.0") (d #t) (k 0)) (d (n "cosmwasm-vm") (r "^0.10.0") (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1am2qvmlrsxdj6yc8h8qqsb0zxx2ifrhpdkvn9mx1g9jgl1dp7sw") (f (quote (("singlepass" "cosmwasm-vm/default-singlepass") ("default" "cranelift") ("cranelift" "cosmwasm-vm/default-cranelift") ("backtraces" "cosmwasm-std/backtraces" "cosmwasm-vm/backtraces"))))))

