(define-module (crates-io te rr terraform-profile) #:use-module (crates-io))

(define-public crate-terraform-profile-0.1.0 (c (n "terraform-profile") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)))) (h "1m57i3jgwsmgxa09mhhiz1y43pdhakq9wm8jnvz2rqcg78l88rb2")))

(define-public crate-terraform-profile-0.1.1 (c (n "terraform-profile") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)))) (h "1xi5dwi7p7gx4z7a5ssjnjlgf3kfr9xcxka6zc6xp58kcg3w70ip")))

