(define-module (crates-io te rr terraform) #:use-module (crates-io))

(define-public crate-terraform-0.1.0 (c (n "terraform") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0g3jk634xjnhibjnim3wcy6isch99l85a9dw96v11irg4sylx56w")))

