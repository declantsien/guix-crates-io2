(define-module (crates-io te rr terraformer3d) #:use-module (crates-io))

(define-public crate-terraformer3d-0.1.0 (c (n "terraformer3d") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "image") (r "^0.24.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)))) (h "1k6fz5vvlqg3ml34d130xh9ac1p8xcrcyliw6vm68y6f6s2hms3n")))

