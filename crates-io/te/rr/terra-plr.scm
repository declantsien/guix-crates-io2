(define-module (crates-io te rr terra-plr) #:use-module (crates-io))

(define-public crate-terra-plr-0.1.0 (c (n "terra-plr") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "bounded-integer") (r "^0.5.7") (f (quote ("types"))) (d #t) (k 0)) (d (n "bounded-vector") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cs-datetime-parse") (r "^1.1.0") (d #t) (k 0)) (d (n "cs-string-rw") (r "^1.0.0") (d #t) (k 0)) (d (n "terra-items") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (d #t) (k 0)))) (h "0g0vsaf41iz7vm26izbg4k301ws3bqwz1d4083ah4nyni6gly57v")))

