(define-module (crates-io te rr terrapin) #:use-module (crates-io))

(define-public crate-terrapin-0.0.0 (c (n "terrapin") (v "0.0.0") (h "0x4ibrmf8m8yzlbrhdgcvf8zqv3hq3d3ap8qfjj08lcrx97pi9lf") (y #t)))

(define-public crate-terrapin-0.1.0 (c (n "terrapin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "0mv8cncrab5m2myiw68dvs52wk7yy9ydbrv6hjlx2vdv0nhxszv4") (y #t)))

(define-public crate-terrapin-0.1.1 (c (n "terrapin") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "1lwr0br68sqkzhpxjhqpqvysxwhfbyrsj7c74iwqwms5w377c89j") (y #t)))

(define-public crate-terrapin-0.1.2 (c (n "terrapin") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "112wxipfr3x5r27qk07h72v1r1yd8carpxpcq69iv4slw5024mv5") (y #t)))

(define-public crate-terrapin-0.1.4 (c (n "terrapin") (v "0.1.4") (d (list (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "002bida7a8h3y7hc94kw7i51rs4qxpb29677b5xdk0rbff5wvm4m") (y #t)))

(define-public crate-terrapin-0.1.5 (c (n "terrapin") (v "0.1.5") (d (list (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "1rhjpp6zs7mi5vygpzq9a4mrrwml0k86mxn7hnfaq41g3an46rl4") (y #t)))

(define-public crate-terrapin-0.1.6 (c (n "terrapin") (v "0.1.6") (d (list (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "0r482nc54fi3k3a9wzz74j0xd6lp41qi5nj0z1rcwh4b4jmgcrjh") (y #t)))

(define-public crate-terrapin-0.1.7 (c (n "terrapin") (v "0.1.7") (d (list (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "1395m4di1184h5ivz7b8kxnshcjyzm99hqnrydj7p43dybxkgk29") (y #t)))

(define-public crate-terrapin-0.1.8 (c (n "terrapin") (v "0.1.8") (d (list (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "1l9lq38m7893ys0b4n1gn21wpyr4nl0jcqx29yag3z4v4wmvc492") (y #t)))

(define-public crate-terrapin-0.1.9 (c (n "terrapin") (v "0.1.9") (d (list (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "1s7sml8zwh9k96llip77dlcjmw0mx4rnp63g6x95g3hcj7yd5fjq") (y #t)))

(define-public crate-terrapin-0.1.10 (c (n "terrapin") (v "0.1.10") (d (list (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "1ibq7kjpbs1zz46hvmrj5rk40cn8ziibm8pr7saqxwz9chx1nfg6") (y #t)))

(define-public crate-terrapin-0.1.11 (c (n "terrapin") (v "0.1.11") (d (list (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "0c5abvr5h1s0dqy3v3wn1gh6nywvsxmxn2wap6yq6hws1y70kjsc") (y #t)))

(define-public crate-terrapin-0.1.12 (c (n "terrapin") (v "0.1.12") (d (list (d (n "rustyline") (r "^9.1") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)))) (h "09mlkdsp1virlrmi20jhpag1v9z73jazzaiwd8kmxbdpkyxm5jbz") (y #t)))

