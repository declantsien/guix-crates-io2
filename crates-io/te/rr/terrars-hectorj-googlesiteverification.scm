(define-module (crates-io te rr terrars-hectorj-googlesiteverification) #:use-module (crates-io))

(define-public crate-terrars-hectorj-googlesiteverification-0.1.0 (c (n "terrars-hectorj-googlesiteverification") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.7") (d #t) (k 0)))) (h "0mdbvaq8bi52hj9vsd74sa5ypbfjpms4w46cvclrybw5wrjwdkif") (f (quote (("dns") ("data_dns_token"))))))

