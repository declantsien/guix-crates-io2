(define-module (crates-io te rr terra-asset) #:use-module (crates-io))

(define-public crate-terra-asset-0.1.0 (c (n "terra-asset") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw20") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1kn7hgvn1r5qmqniyc3hbgzh1cjvgnq5can2kxq62yj5b5z7fxpx")))

(define-public crate-terra-asset-0.2.0 (c (n "terra-asset") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw20") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "01ig8qk9rgknclvhvnx93sgnb0sbxxp4y7vflz7bjy1aqllg87z8")))

(define-public crate-terra-asset-0.2.1 (c (n "terra-asset") (v "0.2.1") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw20") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1k318hn9hz521akay4hslxv9jm2l0a2abzkdmi6ya24dgxakcy0k")))

(define-public crate-terra-asset-0.2.2 (c (n "terra-asset") (v "0.2.2") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw20") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "18wiw8772n9a3h1zkdxa6iqkbxd3g6cznaw7inhgi9j3g06dz4w6")))

