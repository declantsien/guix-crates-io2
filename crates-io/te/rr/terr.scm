(define-module (crates-io te rr terr) #:use-module (crates-io))

(define-public crate-terr-0.1.0 (c (n "terr") (v "0.1.0") (d (list (d (n "kiss3d") (r "^0.19") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "ncollide3d") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "109fjnrm6mbjj8brzr3npmrh8v2g0mp61bsv48qr5bj5s2p6mc3w")))

(define-public crate-terr-0.1.1 (c (n "terr") (v "0.1.1") (d (list (d (n "kiss3d") (r "^0.19") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "ncollide3d") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1v8n22hp5xinib57hv79b4lgbpcm8x248n3hbm4vjmlgjn48dys5")))

(define-public crate-terr-0.1.2 (c (n "terr") (v "0.1.2") (d (list (d (n "kiss3d") (r "^0.19") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "ncollide3d") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0vdf6dil41kr2ni7kcj0w87dq5kw16rj13z98xsaamz9z5zvlxpf")))

