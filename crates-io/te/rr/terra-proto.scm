(define-module (crates-io te rr terra-proto) #:use-module (crates-io))

(define-public crate-terra-proto-0.5.12-beta.0 (c (n "terra-proto") (v "0.5.12-beta.0") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0rdlycyx3kgig656gi4r1hc1wa80m36yg538c8lm6l9alfhfr6zz")))

(define-public crate-terra-proto-0.5.12-beta.1 (c (n "terra-proto") (v "0.5.12-beta.1") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0l4y5cc67rjms2wz29waahg17ixwifbs5wxs08qnn84rf0sjv970")))

(define-public crate-terra-proto-0.5.12-beta.2 (c (n "terra-proto") (v "0.5.12-beta.2") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 1)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0r3iz06samwwqh73nbap94mnx00xxz4hwzikcaxindw6kx9kypfz")))

(define-public crate-terra-proto-0.5.12-beta.3 (c (n "terra-proto") (v "0.5.12-beta.3") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 2)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0mw4rqvnw84f9jk20ixvqlccfiv3iskgaxv1v5hp16fpfpnx44dx")))

(define-public crate-terra-proto-0.5.16-beta.1 (c (n "terra-proto") (v "0.5.16-beta.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "prost-build") (r "^0.9") (d #t) (k 2)) (d (n "prost-derive") (r "^0.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0hbrd3wdysf6079mih5iyfxn8yrxr5izmiqjmmp8kjqxnq35wjn7")))

