(define-module (crates-io te rr terraform-zap-ignore-lib) #:use-module (crates-io))

(define-public crate-terraform-zap-ignore-lib-0.1.0 (c (n "terraform-zap-ignore-lib") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 2)))) (h "119ddbq9bd8k6aiwy7a159nhrsa3s70iniv1hg9yabk67rcq62ws")))

(define-public crate-terraform-zap-ignore-lib-0.2.0 (c (n "terraform-zap-ignore-lib") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 2)))) (h "1irrg64zfcpd8alp591gg0wimkr91bdmxlfp3g4dl5jlmr9l3f00")))

(define-public crate-terraform-zap-ignore-lib-0.2.1 (c (n "terraform-zap-ignore-lib") (v "0.2.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 2)))) (h "0mifb5n7646bdyi8k1giidxlvvzc2a1k7yqxlc8lwn1qzhgcc8vi")))

(define-public crate-terraform-zap-ignore-lib-0.2.3 (c (n "terraform-zap-ignore-lib") (v "0.2.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 2)))) (h "14sy445fscmgmp2mx2xr0m2v2pn41hd0j28yym7zp517rgrryhsw")))

(define-public crate-terraform-zap-ignore-lib-0.3.0 (c (n "terraform-zap-ignore-lib") (v "0.3.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 2)))) (h "09wrwvb9aa9j6rsi72vacdzsf7hfk58x61ppglwfrf3lfmy9dqfk")))

(define-public crate-terraform-zap-ignore-lib-0.3.1 (c (n "terraform-zap-ignore-lib") (v "0.3.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 2)))) (h "0qg1hawd9yagx38hzbvs0azs1lp08wf50zl5f2fqbxw2s9znawnz")))

(define-public crate-terraform-zap-ignore-lib-0.3.2 (c (n "terraform-zap-ignore-lib") (v "0.3.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 2)))) (h "1hnhnipd3acdc5xfs0b05q648i1lqxmsiz9pwvyjivbj8lszqsx7")))

(define-public crate-terraform-zap-ignore-lib-0.3.3 (c (n "terraform-zap-ignore-lib") (v "0.3.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 2)))) (h "06f0xvrv6ls3gk7d1agdkxmz58slh8bqswcbg0jw35xl1g46j0nr")))

(define-public crate-terraform-zap-ignore-lib-0.4.0 (c (n "terraform-zap-ignore-lib") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 2)))) (h "0pimlgw0xxiibqb54388nlbaa7z5k1jliwpdcshx5ldr8m8vjh7q")))

