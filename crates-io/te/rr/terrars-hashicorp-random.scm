(define-module (crates-io te rr terrars-hashicorp-random) #:use-module (crates-io))

(define-public crate-terrars-hashicorp-random-0.1.0 (c (n "terrars-hashicorp-random") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.9") (d #t) (k 0)))) (h "08z15230ps7pra5fdadimxhf6dg8i7m4l0pn8gxd4rgw6539n5cm") (f (quote (("uuid") ("string") ("shuffle") ("pet") ("password") ("integer") ("id"))))))

(define-public crate-terrars-hashicorp-random-0.1.1 (c (n "terrars-hashicorp-random") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.10") (d #t) (k 0)))) (h "088j5c5xiynr8jb7sxi6ygr0hnfh1yj5mnn9c27j96hsp8br9wfs") (f (quote (("uuid") ("string") ("shuffle") ("pet") ("password") ("integer") ("id"))))))

(define-public crate-terrars-hashicorp-random-0.1.2 (c (n "terrars-hashicorp-random") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.0") (d #t) (k 0)))) (h "09rkrsimwdkrcf0hbc1qdqf5im28413plk4a014h145csycc5f4r") (f (quote (("uuid") ("string") ("shuffle") ("pet") ("password") ("integer") ("id"))))))

(define-public crate-terrars-hashicorp-random-0.1.3 (c (n "terrars-hashicorp-random") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.1") (d #t) (k 0)))) (h "06g1f1cj5gc9brl8f4bl4jjp1psxgq83w3vxpd0vzl5gfjcypxlh") (f (quote (("uuid") ("string") ("shuffle") ("pet") ("password") ("integer") ("id"))))))

(define-public crate-terrars-hashicorp-random-0.1.4 (c (n "terrars-hashicorp-random") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.2") (d #t) (k 0)))) (h "0k611yvx7wncqx1bz03m985kffbfh4gm5pfxyydhcn9g50zsardq") (f (quote (("uuid") ("string") ("shuffle") ("pet") ("password") ("integer") ("id"))))))

(define-public crate-terrars-hashicorp-random-0.1.5 (c (n "terrars-hashicorp-random") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.2") (d #t) (k 0)))) (h "0hq4nhbw3bkgpqkaxzbcz4wa6h8zxlq700xz0i99cwr2skhpkjgs") (f (quote (("uuid") ("string") ("shuffle") ("pet") ("password") ("integer") ("id"))))))

(define-public crate-terrars-hashicorp-random-0.1.6 (c (n "terrars-hashicorp-random") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.3") (d #t) (k 0)))) (h "0dqwb6dm4msrjlhjba4z45910047swqgvgychygbwfgbcs5pzvcl") (f (quote (("uuid") ("string") ("shuffle") ("pet") ("password") ("integer") ("id"))))))

(define-public crate-terrars-hashicorp-random-0.1.7 (c (n "terrars-hashicorp-random") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.4") (d #t) (k 0)))) (h "08cm5mpfpqj42vlpiicr6idhyw4m5znd8gh8a9739drkpf50pac2") (f (quote (("uuid") ("string") ("shuffle") ("pet") ("password") ("integer") ("id"))))))

(define-public crate-terrars-hashicorp-random-0.1.8 (c (n "terrars-hashicorp-random") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.5") (d #t) (k 0)))) (h "0j35aflrk7gdq7r2bclr642nfnwp3mfd0axi3b3cg1frwns2lpgd") (f (quote (("uuid") ("string") ("shuffle") ("pet") ("password") ("integer") ("id"))))))

