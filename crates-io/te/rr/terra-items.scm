(define-module (crates-io te rr terra-items) #:use-module (crates-io))

(define-public crate-terra-items-0.1.0 (c (n "terra-items") (v "0.1.0") (h "12wyh2p6mbc9m8raaf28256mbjgk4mnwmixc20frsb2kgcxa8pmw")))

(define-public crate-terra-items-0.2.0 (c (n "terra-items") (v "0.2.0") (h "1cp067qsikz180zrafrvs5srn92adl1q8vaw51fgscjgkxb8rdvi")))

(define-public crate-terra-items-0.2.1 (c (n "terra-items") (v "0.2.1") (h "0whkdsqapxg6n3cac6d5j1ka9pwihinpbi9lmbf3wpbrpynqjfgy")))

(define-public crate-terra-items-0.2.2 (c (n "terra-items") (v "0.2.2") (h "04wxrap8a54y0iv75fz7qwr1d46zp7bvxn9x857jq34yn2gx2a30")))

