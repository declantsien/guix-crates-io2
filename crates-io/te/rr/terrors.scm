(define-module (crates-io te rr terrors) #:use-module (crates-io))

(define-public crate-terrors-0.1.0 (c (n "terrors") (v "0.1.0") (h "0zgisrmijrls71kg8wa186kwk1wqa581v7bgr2fhk23kn1n85k3j")))

(define-public crate-terrors-0.1.1 (c (n "terrors") (v "0.1.1") (h "079wjav9lqccrmlffj77l3jnrfxi02p2yiypyska2k2536p3qmwb")))

(define-public crate-terrors-0.1.2 (c (n "terrors") (v "0.1.2") (h "0xal0iiy8k6m6qp6b0k11jcpxwxwcmzb978gr6863jp7107l00js")))

(define-public crate-terrors-0.1.3 (c (n "terrors") (v "0.1.3") (h "1lz0sfj1zk8argphs1mc41kz0kqgfiqp3q20bzajlhi9fri2hcag")))

(define-public crate-terrors-0.1.4 (c (n "terrors") (v "0.1.4") (h "06rrnsknq0x0ir1k7wgbjlx2dnm74ql2jbrkwdiwxww6jpi4p0sm")))

(define-public crate-terrors-0.1.6 (c (n "terrors") (v "0.1.6") (h "1alxwxxcmgnr0d99v1wmy0s0pamzmhycvy3jbl59dkxvfngq5il1")))

(define-public crate-terrors-0.2.0 (c (n "terrors") (v "0.2.0") (h "1i4k1aqpbcahv9wwwjx4byyaz5z6xmf4h0wccq62bj2z4vv6nw4i")))

(define-public crate-terrors-0.2.1 (c (n "terrors") (v "0.2.1") (h "05vyln9hhd403yggfji7v84fgvpg76ca763r5l92pqsqqk7bkafm")))

(define-public crate-terrors-0.2.2 (c (n "terrors") (v "0.2.2") (h "0i832dbp9g3wxs3fni075j4hgqiq1j7v6p0jiqf5nnl96xhfywxm")))

(define-public crate-terrors-0.2.3 (c (n "terrors") (v "0.2.3") (h "0zxmg85qf2ri39x85h8vlsj858a6r0qplm33v421k488dnq5jfhs")))

(define-public crate-terrors-0.2.5 (c (n "terrors") (v "0.2.5") (h "0qahpxpqmglxyz0sqxbp72qswacl8giamy89cb1r9j435h9wpz27")))

(define-public crate-terrors-0.2.6 (c (n "terrors") (v "0.2.6") (h "0iwdcr5qsv8ihfdm1bxzmhv18rqxnaasz4mbxvi42qrx1xj0wf13")))

(define-public crate-terrors-0.3.0 (c (n "terrors") (v "0.3.0") (h "1mlzv138l6nhfy70l9fhq0s46vy6hljzx87frmzyy37ggyj7xkx5") (f (quote (("error_provide_feature") ("error_provide"))))))

