(define-module (crates-io te rr terraphim_settings) #:use-module (crates-io))

(define-public crate-terraphim_settings-0.1.0 (c (n "terraphim_settings") (v "0.1.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "test-log") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "twelf") (r "^0.13") (f (quote ("json" "toml" "env" "clap"))) (d #t) (k 0)))) (h "0iil3q1schc16a4c010hic89f6wvwknxfrvsl0ygv80a3xv4nii1")))

