(define-module (crates-io te rr terrars-hashicorp-local) #:use-module (crates-io))

(define-public crate-terrars-hashicorp-local-0.1.0 (c (n "terrars-hashicorp-local") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.0") (d #t) (k 0)))) (h "1x329hl9h1v0k3i4cxpf493avmp0l18xf3d54v06hm2sw0ip61fp") (f (quote (("sensitive_file") ("file") ("data_sensitive_file") ("data_file"))))))

(define-public crate-terrars-hashicorp-local-0.1.1 (c (n "terrars-hashicorp-local") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.1") (d #t) (k 0)))) (h "0c4rhhncg511qiynhrxzdc493mgrr132y3yda53xl5fqglsb1j29") (f (quote (("sensitive_file") ("file") ("data_sensitive_file") ("data_file"))))))

(define-public crate-terrars-hashicorp-local-0.1.2 (c (n "terrars-hashicorp-local") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.2") (d #t) (k 0)))) (h "0jfh8if2j2az2vjfbbdns68kbwk3rfx8c24y15f16b1n8jid7yid") (f (quote (("sensitive_file") ("file") ("data_sensitive_file") ("data_file"))))))

(define-public crate-terrars-hashicorp-local-0.1.3 (c (n "terrars-hashicorp-local") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.2") (d #t) (k 0)))) (h "0pz7dw9m0p93y662b2pi7ib41s7albz1sxwm8ziiksd7zr1yd26z") (f (quote (("sensitive_file") ("file") ("data_sensitive_file") ("data_file"))))))

(define-public crate-terrars-hashicorp-local-0.1.4 (c (n "terrars-hashicorp-local") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.3") (d #t) (k 0)))) (h "01cq8kxhvpx3cvb9xpg6l7fqqj4kbkjn24aqyiy68m9lk9xwa4qm") (f (quote (("sensitive_file") ("file") ("data_sensitive_file") ("data_file"))))))

(define-public crate-terrars-hashicorp-local-0.1.5 (c (n "terrars-hashicorp-local") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.4") (d #t) (k 0)))) (h "0ys9xkfb43irl8f9hbfmy5lw6ngppkfp4m4569131ckp3mpgk790") (f (quote (("sensitive_file") ("file") ("data_sensitive_file") ("data_file"))))))

(define-public crate-terrars-hashicorp-local-0.1.6 (c (n "terrars-hashicorp-local") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.5") (d #t) (k 0)))) (h "1l6m9r5ngdhqpkaxqlgzcy0j8nk6ipl6nq0dcqml5p8s04kvhffx") (f (quote (("sensitive_file") ("file") ("data_sensitive_file") ("data_file"))))))

