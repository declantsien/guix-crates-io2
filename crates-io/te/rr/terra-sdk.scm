(define-module (crates-io te rr terra-sdk) #:use-module (crates-io))

(define-public crate-terra-sdk-0.0.0 (c (n "terra-sdk") (v "0.0.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "15fgn4m7fjqihwnggp04jhrgax78ssnyig2jqiy0df91q5p26qdf") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

