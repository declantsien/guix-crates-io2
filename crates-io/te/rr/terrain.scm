(define-module (crates-io te rr terrain) #:use-module (crates-io))

(define-public crate-terrain-0.0.1 (c (n "terrain") (v "0.0.1") (h "0f9w1llm5blfmhbvid9crb2lci4qhf0scsl2ll8nil9m5gcz5ql8") (y #t)))

(define-public crate-terrain-0.0.5 (c (n "terrain") (v "0.0.5") (h "01k4j5nn1042c4qnrckaf3d94yl98p5blzl1n69swshmj6kylaf7") (y #t)))

(define-public crate-terrain-0.0.7 (c (n "terrain") (v "0.0.7") (h "0xiwvrz7w88ivzjlbxp433n5gvlsgsy0nx2m280b7xrmn3ml1hnf") (y #t)))

(define-public crate-terrain-0.1.1 (c (n "terrain") (v "0.1.1") (h "16srsqfv5rk78yiakrgm61k6fiqdm7h2z4rhvdf5dyh974ny3s77") (y #t)))

(define-public crate-terrain-0.1.3 (c (n "terrain") (v "0.1.3") (h "1dfglhr1k9r1dm9dl56kd19gr8wixzqbggqhlnhldbw8r2zzrbv9") (y #t)))

(define-public crate-terrain-0.1.4 (c (n "terrain") (v "0.1.4") (h "1arrrmbrppyiw3sql2n6ri1li9wndhkmci3dxjqjp60lyidy3h2w") (y #t)))

(define-public crate-terrain-0.1.77 (c (n "terrain") (v "0.1.77") (h "0vvywfg6lhm5df5qasbn9gc8zvczpnwrff2qaqp4migdzqw6k244") (y #t)))

(define-public crate-terrain-0.77.0 (c (n "terrain") (v "0.77.0") (h "1ssf4v33rcxccb02wj1845l5ha0jdkhkrybfkwxhsb8kb4z85hmg") (y #t)))

(define-public crate-terrain-7.7.18 (c (n "terrain") (v "7.7.18") (h "0ldvfzyg46zqf79i820nq7l0f17nfhl1xcbh9maf9lq8jlg5636l") (y #t)))

(define-public crate-terrain-2018.7.7 (c (n "terrain") (v "2018.7.7") (h "0wdhsf1piwh9hal8qifidz9l127sb24bm43z1wl7mnmzcm4izck5") (y #t)))

(define-public crate-terrain-2019.12.13 (c (n "terrain") (v "2019.12.13") (h "1jml7xa0m05ldw29aw7sdykdi7qv18cymfi0fq7z3i531g5k8jy8") (y #t)))

(define-public crate-terrain-9999.999.99 (c (n "terrain") (v "9999.999.99") (h "14n6bchnmpkqkb827p9cgxhqjzk5q1p5kyhl7r9sviczdq273x27") (y #t)))

(define-public crate-terrain-9.9.9 (c (n "terrain") (v "9.9.9") (h "1m2mjam0qap1b4f4rc6872qkvr7p1dbr74f6i24f64bsjkwc5s46") (y #t)))

(define-public crate-terrain-99999.99999.99999 (c (n "terrain") (v "99999.99999.99999") (h "14gc52p4xc4p13vdyh34y0yk5clsh5s71p5rpv1bljz9fx9pd12h") (y #t)))

(define-public crate-terrain-9999999.9999999.9999999 (c (n "terrain") (v "9999999.9999999.9999999") (h "0izp5xa31sd28idrfmll94dfpja4zmffsbjzh8h06j0qj7jg506n") (y #t)))

(define-public crate-terrain-999999999.999999999.999999999 (c (n "terrain") (v "999999999.999999999.999999999") (h "017kpdy81habcknahymgn3c7b3jgjcbkp0zlw8va0wdhpxay1227")))

