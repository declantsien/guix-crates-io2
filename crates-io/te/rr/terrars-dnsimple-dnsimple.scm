(define-module (crates-io te rr terrars-dnsimple-dnsimple) #:use-module (crates-io))

(define-public crate-terrars-dnsimple-dnsimple-0.1.0 (c (n "terrars-dnsimple-dnsimple") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.5") (d #t) (k 0)))) (h "0gjhw1wj6scigbjimnp1gnvcfq89kzxh9ynl2pr052fiymj1bjax") (f (quote (("zone_record") ("registered_domain") ("lets_encrypt_certificate") ("email_forward") ("ds_record") ("domain_delegation") ("domain") ("data_zone") ("data_registrant_change_check") ("data_certificate") ("contact"))))))

(define-public crate-terrars-dnsimple-dnsimple-0.1.1 (c (n "terrars-dnsimple-dnsimple") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.7") (d #t) (k 0)))) (h "0d8lpmfwsdwqm6hams09742j3jb7i8fcc5zczqnkc9l3j5g1nhpk") (f (quote (("zone_record") ("registered_domain") ("lets_encrypt_certificate") ("email_forward") ("ds_record") ("domain_delegation") ("domain") ("data_zone") ("data_registrant_change_check") ("data_certificate") ("contact"))))))

