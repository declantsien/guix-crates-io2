(define-module (crates-io te rr terrars-andrewbaxter-localrun) #:use-module (crates-io))

(define-public crate-terrars-andrewbaxter-localrun-0.1.0 (c (n "terrars-andrewbaxter-localrun") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.9") (d #t) (k 0)))) (h "05ij32ljbz1axc01bwaq477sb2lk5b54arxj15kwizmwh1h20gf0") (f (quote (("run"))))))

(define-public crate-terrars-andrewbaxter-localrun-0.1.4 (c (n "terrars-andrewbaxter-localrun") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.10") (d #t) (k 0)))) (h "0557ww5g1qmbifhbkld6pa6bc2hdqj370lzmhlzny062wxzw17xw") (f (quote (("run"))))))

(define-public crate-terrars-andrewbaxter-localrun-0.1.9 (c (n "terrars-andrewbaxter-localrun") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.2") (d #t) (k 0)))) (h "1cdf5mmg4na75kadify01gyvfnacd1hn9333s6q3cn9fqvn1230r") (f (quote (("data_run"))))))

(define-public crate-terrars-andrewbaxter-localrun-0.1.10 (c (n "terrars-andrewbaxter-localrun") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.2") (d #t) (k 0)))) (h "1i18rgyzncal9bb81wc9rd37clbj37i79xhxr45xcnpn4mdfhfx3") (f (quote (("data_run"))))))

(define-public crate-terrars-andrewbaxter-localrun-0.1.11 (c (n "terrars-andrewbaxter-localrun") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.3") (d #t) (k 0)))) (h "0k9mj16afbiib6xgwsxq7ivk03h09mm5v3flia2q6bvg4lyvi4sf") (f (quote (("data_run"))))))

(define-public crate-terrars-andrewbaxter-localrun-0.1.12 (c (n "terrars-andrewbaxter-localrun") (v "0.1.12") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.4") (d #t) (k 0)))) (h "12y0b4iwyd21ldh4rnnkr114yjnz6xiwinj7i83wxqcswdzvdrvs") (f (quote (("data_run"))))))

(define-public crate-terrars-andrewbaxter-localrun-0.1.13 (c (n "terrars-andrewbaxter-localrun") (v "0.1.13") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.5") (d #t) (k 0)))) (h "0w2ny0qn54afs82y51ibfl4p3yclgbjmjx0gzkq0gyrx3pfzrhj6") (f (quote (("data_run"))))))

(define-public crate-terrars-andrewbaxter-localrun-0.1.14 (c (n "terrars-andrewbaxter-localrun") (v "0.1.14") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.7") (d #t) (k 0)))) (h "1qkj9i2g4jsdxzwbgiwr5afkh6rc05jk0m15d0351hy74ygd4705") (f (quote (("data_run"))))))

(define-public crate-terrars-andrewbaxter-localrun-0.1.15 (c (n "terrars-andrewbaxter-localrun") (v "0.1.15") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.7") (d #t) (k 0)))) (h "1n5l6spzp6vpz8y116hvxy68kfpgfpjac9i6pqvqj6c818pbd92p") (f (quote (("data_run"))))))

(define-public crate-terrars-andrewbaxter-localrun-0.1.16 (c (n "terrars-andrewbaxter-localrun") (v "0.1.16") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.7") (d #t) (k 0)))) (h "0m7dqr6dm6jsgj0x29nyyk3khkcs8qpr3ss8cggsj0a60b6zwnaz") (f (quote (("run") ("data_run"))))))

(define-public crate-terrars-andrewbaxter-localrun-0.2.0 (c (n "terrars-andrewbaxter-localrun") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.7") (d #t) (k 0)))) (h "1g7zl3i3kkpmgp597nyzfvvkax8drr81wqyan68af5kgxiadm4j2") (f (quote (("run") ("data_always_run"))))))

