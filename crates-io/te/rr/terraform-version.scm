(define-module (crates-io te rr terraform-version) #:use-module (crates-io))

(define-public crate-terraform-version-0.1.0 (c (n "terraform-version") (v "0.1.0") (d (list (d (n "miette") (r "^5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0phcx48x4ln1pl3kdgvz1j6c3va1l398zd6lizigqq0as2b02ccm")))

(define-public crate-terraform-version-0.2.0 (c (n "terraform-version") (v "0.2.0") (d (list (d (n "miette") (r "^5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m5hvip8zpm44s2kirdf9s3vndww5a8xqavx4wnv096glgc76kz9")))

(define-public crate-terraform-version-0.3.0 (c (n "terraform-version") (v "0.3.0") (d (list (d (n "miette") (r "^5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jdvyzag3jz9diap0zqck6z48vyq0s7frm45ph8g9cn7kki59z2f")))

(define-public crate-terraform-version-0.4.0 (c (n "terraform-version") (v "0.4.0") (d (list (d (n "miette") (r "^5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 0)))) (h "0bj5vn1d8lb82ff5m42f1amq0f9gcphc7gdrjgr56h0p8y2bz3w1")))

