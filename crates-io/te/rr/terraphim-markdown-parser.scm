(define-module (crates-io te rr terraphim-markdown-parser) #:use-module (crates-io))

(define-public crate-terraphim-markdown-parser-0.1.0 (c (n "terraphim-markdown-parser") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)))) (h "0v1wm26q0irg0r4whbghv6sd1cs4wilaz7k11n1f8ffslwfflim8")))

