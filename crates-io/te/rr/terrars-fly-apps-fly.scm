(define-module (crates-io te rr terrars-fly-apps-fly) #:use-module (crates-io))

(define-public crate-terrars-fly-apps-fly-0.1.0 (c (n "terrars-fly-apps-fly") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.9") (d #t) (k 0)))) (h "182ljs7gzajpclm36smk6xh522zvzp9h0l98nkrqvzs6lgx474b7") (f (quote (("volume") ("machine") ("ip") ("data_ip") ("data_cert") ("data_app") ("cert") ("app"))))))

(define-public crate-terrars-fly-apps-fly-0.1.1 (c (n "terrars-fly-apps-fly") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.0.10") (d #t) (k 0)))) (h "14y7mxma79wr2010bb8ihhhaprazns89j8bb5xp13a4178xz0y2l") (f (quote (("volume") ("machine") ("ip") ("data_ip") ("data_cert") ("data_app") ("cert") ("app"))))))

(define-public crate-terrars-fly-apps-fly-0.1.2 (c (n "terrars-fly-apps-fly") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.0") (d #t) (k 0)))) (h "0dngn7bpc8q71zn8cn384msffqlbfm4b7mkqm4lqpvfjlym4wwwx") (f (quote (("volume") ("machine") ("ip") ("data_ip") ("data_cert") ("data_app") ("cert") ("app"))))))

(define-public crate-terrars-fly-apps-fly-0.1.3 (c (n "terrars-fly-apps-fly") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.1") (d #t) (k 0)))) (h "05m6x3difpp0j8khs3v0jgmgha302x969sdhxwjdyrmji1wpw7kl") (f (quote (("volume") ("machine") ("ip") ("data_ip") ("data_cert") ("data_app") ("cert") ("app"))))))

(define-public crate-terrars-fly-apps-fly-0.1.4 (c (n "terrars-fly-apps-fly") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.2") (d #t) (k 0)))) (h "02dx3alh1hy6f95jvk1s9x4h0wcrzpdxij8g1nhjjkmxzkgixk1i") (f (quote (("volume") ("machine") ("ip") ("data_ip") ("data_cert") ("data_app") ("cert") ("app"))))))

(define-public crate-terrars-fly-apps-fly-0.1.5 (c (n "terrars-fly-apps-fly") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.2") (d #t) (k 0)))) (h "02w25yvzaid64m090cngc8f21sl5h0nkkh2c9wacfyr9a974pq5k") (f (quote (("volume") ("machine") ("ip") ("data_ip") ("data_cert") ("data_app") ("cert") ("app"))))))

(define-public crate-terrars-fly-apps-fly-0.1.6 (c (n "terrars-fly-apps-fly") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.3") (d #t) (k 0)))) (h "1wxq3spck2kx8znc43pgp5gix665q6vapxh09wqyhb995saqi6ps") (f (quote (("volume") ("machine") ("ip") ("data_ip") ("data_cert") ("data_app") ("cert") ("app"))))))

(define-public crate-terrars-fly-apps-fly-0.1.7 (c (n "terrars-fly-apps-fly") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.4") (d #t) (k 0)))) (h "1yj3fx97cajd37b5s6a44jzwjmkzng7mc9w09lmjgnpnkk44bpg2") (f (quote (("volume") ("machine") ("ip") ("data_ip") ("data_cert") ("data_app") ("cert") ("app"))))))

(define-public crate-terrars-fly-apps-fly-0.1.8 (c (n "terrars-fly-apps-fly") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "terrars") (r "^0.1.5") (d #t) (k 0)))) (h "0cnbz13vyakdbpbrr5pq4p4l0zrjp3p2jvyr006wb340nm51vn9j") (f (quote (("volume") ("machine") ("ip") ("data_ip") ("data_cert") ("data_app") ("cert") ("app"))))))

