(define-module (crates-io te rr terra-cosmwasm) #:use-module (crates-io))

(define-public crate-terra-cosmwasm-1.0.0 (c (n "terra-cosmwasm") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^0.9.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.9.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "008j0pr2y1lf1gqi6hcl04w98nnw32c2h2qpg3jfb1wmin391qr7")))

(define-public crate-terra-cosmwasm-1.1.0-alpha (c (n "terra-cosmwasm") (v "1.1.0-alpha") (d (list (d (n "cosmwasm-schema") (r "^0.9.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.0-alpha2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1rz614nvh0qqr9xpd1lsgaxjbqh74dzs27z5q0rs6gsdm0ydrp32")))

(define-public crate-terra-cosmwasm-1.1.0 (c (n "terra-cosmwasm") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^0.10.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0i5pl0y1wafynkb95g3b86w030ilrs997fcz17qjvpcgjq504fr9")))

(define-public crate-terra-cosmwasm-1.2.0 (c (n "terra-cosmwasm") (v "1.2.0") (d (list (d (n "cosmwasm-schema") (r "^0.10.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1d31ydhi8g4rzfnh31kb1yvaicprvz7nfg6g0mq7z59sw9ls1i7a")))

(define-public crate-terra-cosmwasm-1.2.1 (c (n "terra-cosmwasm") (v "1.2.1") (d (list (d (n "cosmwasm-schema") (r "^0.10.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1xsy5scgwa81k7r57nasajgcn5bqafcf5mb7pk7njm0xp4rx8iyk")))

(define-public crate-terra-cosmwasm-1.2.2 (c (n "terra-cosmwasm") (v "1.2.2") (d (list (d (n "cosmwasm-schema") (r "^0.10.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1b35f313msl6nk7r6npghb5ph9slgd5yh083qpr7xhpvahapj574")))

(define-public crate-terra-cosmwasm-1.2.3 (c (n "terra-cosmwasm") (v "1.2.3") (d (list (d (n "cosmwasm-schema") (r "^0.10.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1n557a9iflfz14djkbjhcjjwbmvlsyzp02853s6rywm6g9hrs16r")))

(define-public crate-terra-cosmwasm-1.2.4 (c (n "terra-cosmwasm") (v "1.2.4") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0pg49kp88fgszp0wykbvj62j165ij9bc6d3nci0lypiqrnm7awjd")))

(define-public crate-terra-cosmwasm-2.0.0-alpha1 (c (n "terra-cosmwasm") (v "2.0.0-alpha1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0aysq150fv9gmj5wlisdsqn0b26rji3fw1cwqfl7h8msn74c76fl") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-terra-cosmwasm-2.0.0-alpha2 (c (n "terra-cosmwasm") (v "2.0.0-alpha2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "104xf9p1l67sn96xqqqly924rp69av26vzbgmb3d1sqz1wjza9pr") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-terra-cosmwasm-2.0.0-alpha3 (c (n "terra-cosmwasm") (v "2.0.0-alpha3") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "07bciwg8l7836nbwim2bgzq69cq7xnr42g6k4cyqy9vzfyg6i859") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-terra-cosmwasm-2.0.0-alpha4 (c (n "terra-cosmwasm") (v "2.0.0-alpha4") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1wkp109xzg3ayk2phxmpa1i0qxcr9a8kfch2c5lrrwb1mf6wp1h2") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-terra-cosmwasm-2.0.0 (c (n "terra-cosmwasm") (v "2.0.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0rr9741w3n924ljy0dcf4yq93zfxf88fapcsf1nr2qlwsm7qf5gi") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-terra-cosmwasm-2.1.0 (c (n "terra-cosmwasm") (v "2.1.0") (d (list (d (n "cosmwasm-schema") (r "^0.15.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "18scpsjxqmq6fjkgz2iwvndah8prvldl83sfgg2cfj9yd6yp3dhh") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-terra-cosmwasm-2.2.0-rc2 (c (n "terra-cosmwasm") (v "2.2.0-rc2") (d (list (d (n "cosmwasm-schema") (r "^0.16.0-rc2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0-rc2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "129sjd81w9fjb3lz26qmd8cll592yca4r7l5m3m4srpijf7zlji7") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-terra-cosmwasm-2.2.0-rc3 (c (n "terra-cosmwasm") (v "2.2.0-rc3") (d (list (d (n "cosmwasm-schema") (r "^0.16.0-rc5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0-rc5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0hra5g6snmkpdfc5m40f430r0a619ai2w6ph37f9g73nza55llfw") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-terra-cosmwasm-2.2.0 (c (n "terra-cosmwasm") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "061761166alzj0rjwpakqx5ra2w2jrvg7cz3q3wd2ddmlb5ihbsm") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-terra-cosmwasm-3.0.0-beta.0 (c (n "terra-cosmwasm") (v "3.0.0-beta.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0j4fabn4lxkmnfz99j1zis2d34b4fag9kh4rg7hl77ba8a8yxhkz") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

