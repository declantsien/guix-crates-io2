(define-module (crates-io te rr terra-bindings) #:use-module (crates-io))

(define-public crate-terra-bindings-0.1.0 (c (n "terra-bindings") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^0.9.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.9.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "03albdd1r6yvicj93q2wqk073k0k4s3ak491zrbxas2v8svwrmzj")))

(define-public crate-terra-bindings-1.0.0 (c (n "terra-bindings") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^0.9.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.9.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "14y2w8bp93bfjm1d947hnb64q0vi0k6gdqzvrp19jdz22s3pbz9h") (y #t)))

