(define-module (crates-io te rr terraform-parser) #:use-module (crates-io))

(define-public crate-terraform-parser-0.1.0 (c (n "terraform-parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "184xfh41937j0qbz34zsvf0x5nqc6rsbdy7dvsqbjkw0x2vziz66")))

