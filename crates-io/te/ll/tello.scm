(define-module (crates-io te ll tello) #:use-module (crates-io))

(define-public crate-tello-0.2.0 (c (n "tello") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.32") (f (quote ("ttf"))) (d #t) (k 2)))) (h "1hwb6v2l06s68yvlbcys64nrplaimns82cyrrhk4s968z8ldq3r3")))

(define-public crate-tello-0.2.1 (c (n "tello") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.32") (f (quote ("ttf"))) (d #t) (k 2)))) (h "08kzsb7a95w2316fyhq9q7v8ggd883kq50w4vsaixlnxf6sq1f4f")))

(define-public crate-tello-0.3.0 (c (n "tello") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.32") (f (quote ("ttf"))) (d #t) (k 2)))) (h "1ybf186w8rvy61l4yc8m9khkl0kqhlcizfvv75wdn736885did4j")))

(define-public crate-tello-0.3.1 (c (n "tello") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "gilrs") (r "^0.7.2") (d #t) (k 2)) (d (n "sdl2") (r "^0.32") (f (quote ("ttf"))) (d #t) (k 2)))) (h "02xr7p4kbmz9a4cl6g9w4r769ncbkf7hxyv2w07mgbjac11y3hm9")))

(define-public crate-tello-0.4.0 (c (n "tello") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.2") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (f (quote ("ttf"))) (d #t) (k 2)))) (h "1w5hb0703zgcigm71r7ky0yh11sd4b3bc7gd6x5xmg8q8kqlgf83")))

(define-public crate-tello-0.4.1 (c (n "tello") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.2") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (f (quote ("ttf"))) (d #t) (k 2)))) (h "1r5mn4l5yydkqkglrsanbpz2rpxzvxaqgdr13dbbiw208lzvrlgi")))

(define-public crate-tello-0.5.0 (c (n "tello") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.2") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (f (quote ("ttf"))) (d #t) (k 2)))) (h "12m178cl317wiffphsxhis4493x6rdqdhk482jwabxg3m0wvyywi")))

(define-public crate-tello-0.5.1 (c (n "tello") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.2") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (f (quote ("ttf"))) (d #t) (k 2)))) (h "06yhcgi5m7y2kd0mv23hvn4kj0mxj40j4hj3zh77hkr9zmciza77")))

(define-public crate-tello-0.5.2 (c (n "tello") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.2") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (f (quote ("ttf"))) (d #t) (k 2)))) (h "0rlsjky6z3805ba82y06ci97lprkksrbsi6jrzsiw8fy089h94qz")))

(define-public crate-tello-0.6.0 (c (n "tello") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (f (quote ("ttf"))) (d #t) (k 2)) (d (n "tokio") (r "^1.11.0") (f (quote ("net" "rt" "sync" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1vkmg4n8fj98hzixbqf7j8h010v374dbbcvsm7jxylpln07s0vq1") (f (quote (("tokio_async" "tokio" "tokio-stream"))))))

(define-public crate-tello-0.6.1 (c (n "tello") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (f (quote ("ttf"))) (d #t) (k 2)) (d (n "tokio") (r "^1.11.0") (f (quote ("net" "rt" "sync" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "09wainyjhzwhwzjnblvqg1pb7z52hgs7f2m5pg42alvz5q6n5896") (f (quote (("tokio_async" "tokio" "tokio-stream"))))))

(define-public crate-tello-0.6.2 (c (n "tello") (v "0.6.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (f (quote ("ttf"))) (d #t) (k 2)) (d (n "tokio") (r "^1.11.0") (f (quote ("net" "rt" "sync" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1c2sgqqgkh04wpkq1nr0jyvz0xky11335plihaqd5gk0xb3dviqr") (f (quote (("tokio_async" "tokio" "tokio-stream"))))))

(define-public crate-tello-0.6.3 (c (n "tello") (v "0.6.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 2)) (d (n "sdl2") (r "^0.34.5") (f (quote ("ttf"))) (d #t) (k 2)) (d (n "tokio") (r "^1.11.0") (f (quote ("net" "rt" "sync" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.7") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1739758q5mm5fgrganlx3m9pmy7hhl6q32dkk8i7sbra4r6izryq") (f (quote (("tokio_async" "tokio" "tokio-stream") ("default" "tokio_async"))))))

