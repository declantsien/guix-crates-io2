(define-module (crates-io te ll tello-edu) #:use-module (crates-io))

(define-public crate-tello-edu-0.1.0 (c (n "tello-edu") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net"))) (d #t) (k 0)))) (h "0h2fm8n977ywckjflqv7bwni9amvjkkzs5xkyq4bl8dpbnfn34nl")))

(define-public crate-tello-edu-0.1.1 (c (n "tello-edu") (v "0.1.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net"))) (d #t) (k 0)))) (h "054vwjhz98rpdlr3l8rm31k354g99lgiji77rp6qvb9l7i9qvvsa")))

(define-public crate-tello-edu-0.2.0 (c (n "tello-edu") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net"))) (d #t) (k 0)))) (h "0yp4lyzkn1sxyzf70sg14aiyadr2ckz44c9gw8wrdk9l0004bg09")))

(define-public crate-tello-edu-0.3.0 (c (n "tello-edu") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net" "sync"))) (d #t) (k 0)))) (h "130v6am2ndx7ljvncarwxwwcrxdll1dwsb61vnw8vrnc4hp1i6hc")))

(define-public crate-tello-edu-0.3.1 (c (n "tello-edu") (v "0.3.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net" "sync"))) (d #t) (k 0)))) (h "1xnz68kcmfl0aicqkjqcs6w31sscj75cqv1aqfpb48q0iql3i47s")))

(define-public crate-tello-edu-0.4.0 (c (n "tello-edu") (v "0.4.0") (d (list (d (n "bytebuffer") (r "^2.1") (d #t) (k 0)) (d (n "openh264") (r "^0.4") (f (quote ("decoder"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net" "sync"))) (d #t) (k 0)))) (h "1l2pw00wzc0fk0wym841hj5sna54n93189r14yr50fv5abjang6p")))

(define-public crate-tello-edu-0.4.1 (c (n "tello-edu") (v "0.4.1") (d (list (d (n "bytebuffer") (r "^2.1") (d #t) (k 0)) (d (n "openh264") (r "^0.4") (f (quote ("decoder"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net" "sync"))) (d #t) (k 0)))) (h "13s8sbmch328947sxwblfrxggjzf4hbc6110hhfxbqvkp96l7mb0")))

(define-public crate-tello-edu-0.5.0 (c (n "tello-edu") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytebuffer") (r "^2.1") (d #t) (k 0)) (d (n "openh264") (r "^0.4") (f (quote ("decoder"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net" "sync"))) (d #t) (k 0)))) (h "0vl66wmrjyrvb1pz46sf4b53aiwrycc2p0lz3fvrcc6dlzvw8f23")))

(define-public crate-tello-edu-0.5.1 (c (n "tello-edu") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytebuffer") (r "^2.1") (d #t) (k 0)) (d (n "openh264") (r "^0.4") (f (quote ("decoder"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net" "sync"))) (d #t) (k 0)))) (h "06fi965fmmlvvika22242ifbqrnl9hbii5f7b7489j8iqpvqbvd2")))

(define-public crate-tello-edu-0.5.2 (c (n "tello-edu") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytebuffer") (r "^2.1") (d #t) (k 0)) (d (n "openh264") (r "^0.4") (f (quote ("decoder"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net" "sync"))) (d #t) (k 0)))) (h "0czgw7ndmby3nbf9hlzzkfbdzca60zbc3dp17s0hjmji1my0spa4")))

(define-public crate-tello-edu-0.5.3 (c (n "tello-edu") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytebuffer") (r "^2.1") (d #t) (k 0)) (d (n "openh264") (r "^0.4") (f (quote ("decoder"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net" "sync"))) (d #t) (k 0)))) (h "103j3yszxw4fzc4q9pzynmdaqb9fxc97rz3zincd4b0ikrm7qm5v")))

(define-public crate-tello-edu-0.5.4 (c (n "tello-edu") (v "0.5.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytebuffer") (r "^2.1") (d #t) (k 0)) (d (n "openh264") (r "^0.4") (f (quote ("decoder"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net" "sync"))) (d #t) (k 0)))) (h "1rhjhg67c0m2n9g5423v1zsq0zvbb96g8niqgqc6sjw7fcpq6djf")))

(define-public crate-tello-edu-0.5.5 (c (n "tello-edu") (v "0.5.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytebuffer") (r "^2.1") (d #t) (k 0)) (d (n "openh264") (r "^0.4") (f (quote ("decoder"))) (d #t) (k 2)) (d (n "sdl2") (r "^0.35") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("rt-multi-thread" "macros" "time" "net" "sync"))) (d #t) (k 0)))) (h "02v0nh6zqchysvdb33js2wgk26p2gj7gb2zspm8l339837m394bq")))

