(define-module (crates-io te ll tellme-client) #:use-module (crates-io))

(define-public crate-tellme-client-0.1.0 (c (n "tellme-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1anjldjwddaslmw06d5vc89gpgnjmf0qkwpy3da0sx85pmpnbwcq") (y #t)))

(define-public crate-tellme-client-0.1.1 (c (n "tellme-client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "15dybgq5lvhm2m4ys2y787f9gk7p97mg1s25730x3jyfrx14b7xg") (y #t)))

(define-public crate-tellme-client-0.1.2 (c (n "tellme-client") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0dzfqdj20w933wc3b00djacisi3y2fnk3v6aabknfljq1110cpx5") (y #t)))

(define-public crate-tellme-client-0.1.3 (c (n "tellme-client") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0s5w14821in7knpw0wrl8gl27wnlb6czf7fs8dwgmm08lnyq8c13") (y #t)))

(define-public crate-tellme-client-0.1.4 (c (n "tellme-client") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0c79hf9ndnal4gg33zmc6vx7iy4l7s3zahr1l75gks9ljxrs81ng") (y #t)))

(define-public crate-tellme-client-0.1.5 (c (n "tellme-client") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1n6yacvxqr0g66inawvfgvd5cxhpd38znq4ywvx8arfhdbqfzk9y") (y #t)))

(define-public crate-tellme-client-0.1.6 (c (n "tellme-client") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1nk6vw0885hapblxci5wqn58jnrnchhr8lc1f18sbxgc3308y7j8") (y #t)))

(define-public crate-tellme-client-0.1.7 (c (n "tellme-client") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "179kyxm1mmsd9pxl7yfi3jjyribpvd964xabk8dglinnsjll41i1")))

(define-public crate-tellme-client-2.0.0 (c (n "tellme-client") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "057zkkv2aridfghl8ixpq372368q4an0z37rviswjbb6a5dbkfgx")))

(define-public crate-tellme-client-2.0.1 (c (n "tellme-client") (v "2.0.1") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1zybwbscbv8fnfl9v584cvyxmvdl8jjgchidk1hb9nwlc0gsasrd")))

