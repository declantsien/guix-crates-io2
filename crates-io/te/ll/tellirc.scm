(define-module (crates-io te ll tellirc) #:use-module (crates-io))

(define-public crate-tellirc-0.1.0 (c (n "tellirc") (v "0.1.0") (d (list (d (n "irc") (r "^0.13") (d #t) (k 0)) (d (n "rusqlite") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04yz6s975hg1sjvskqfgyrgajrs36by35h1k2lg6vkv2k471i13l")))

(define-public crate-tellirc-0.1.4 (c (n "tellirc") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "irc") (r "^0.13") (d #t) (k 0)) (d (n "rusqlite") (r "^0.18.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18a5r5dvlwnav8p0allsqkm7xcd5nyci1kpf0bq3knkbfyr1k1n9")))

(define-public crate-tellirc-0.1.5 (c (n "tellirc") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "irc") (r "^0.13") (d #t) (k 0)) (d (n "rusqlite") (r "^0.18.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ddzmhbgq8x65z2vf4h7v28ykv5gvz35bywbljfh2qhp9px66jx3")))

(define-public crate-tellirc-0.1.6 (c (n "tellirc") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "irc") (r "^0.13") (d #t) (k 0)) (d (n "rusqlite") (r "^0.18.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m108n7xj8b23x6sjnxff6sr5jh2dckam4xi6zm4py1vavl2r3mv")))

