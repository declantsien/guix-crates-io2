(define-module (crates-io te ll tellmewhy) #:use-module (crates-io))

(define-public crate-tellmewhy-0.1.0 (c (n "tellmewhy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "07937vkpj1993cl1sl7621bmr96m32ndw4n4n2gpghzw0ff1l320")))

