(define-module (crates-io te ct tectonic_xdv) #:use-module (crates-io))

(define-public crate-tectonic_xdv-0.1.8 (c (n "tectonic_xdv") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^2.29") (d #t) (k 2)))) (h "0xyn88xh103jq7xqbjz4ryzi0ys3zbgw934m0adpppcwajvmbfyb")))

(define-public crate-tectonic_xdv-0.1.9 (c (n "tectonic_xdv") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)))) (h "1z5mm1az4lbpd1q14pc2zsjim8nmll9p1asw63cky6xs2zyjcicg")))

(define-public crate-tectonic_xdv-0.1.10 (c (n "tectonic_xdv") (v "0.1.10") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)))) (h "04zkihaf6ilz4ign2zcv1glhx89xmxz1gnzfjcz5imr35172bv70")))

(define-public crate-tectonic_xdv-0.1.11 (c (n "tectonic_xdv") (v "0.1.11") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)))) (h "1ibxv32i7dla3iw6s01cagzgdgzhm1mmxwqjv841m6m4r7g57gxj")))

(define-public crate-tectonic_xdv-0.1.12 (c (n "tectonic_xdv") (v "0.1.12") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)))) (h "0k7qm7kpbyymmzf4l3pd37h00ny8ra09h58mfhz8jnn32h2hdpqs")))

(define-public crate-tectonic_xdv-0.2.0 (c (n "tectonic_xdv") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)))) (h "1fcys9v5zcdavfkq72h5ajkz2pxjpc6km6wqajk29qc65870xd5k")))

(define-public crate-tectonic_xdv-0.2.1 (c (n "tectonic_xdv") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)))) (h "177kzzkwjr67sqp8fs6qj94vxk29bq85wrfbg6zkr5j64l9m4rcq")))

(define-public crate-tectonic_xdv-0.2.2 (c (n "tectonic_xdv") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 2)))) (h "0dc9hs4nyxligv53nbn20mpqysfv5awrw24bm3bgzawiv7vf17gm")))

