(define-module (crates-io te ct tectonic_errors) #:use-module (crates-io))

(define-public crate-tectonic_errors-0.1.0 (c (n "tectonic_errors") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0x5wnxj3qvi4j45zm6a3ihdwikgg48vygi0vp3p64sicnqadcv6n")))

(define-public crate-tectonic_errors-0.2.0 (c (n "tectonic_errors") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1cqy8ydsm1gi094l5hlkaq8lmkmgg6mivqpkh4q6h61lr4fhpckg")))

(define-public crate-tectonic_errors-0.2.1 (c (n "tectonic_errors") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1xa10hy6llyhvn5yxnzdkhp94qfnidnwfrd858k86jp74f9c0wwf")))

