(define-module (crates-io te ct tectonic_xetex_format) #:use-module (crates-io))

(define-public crate-tectonic_xetex_format-0.1.0 (c (n "tectonic_xetex_format") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "tectonic_errors") (r ">=0.1.0, <1") (d #t) (k 0)))) (h "006b7ikpjawa3gj45yk9k41rbw918lxx0i58cmwvdz1shwxa5sd9")))

(define-public crate-tectonic_xetex_format-0.2.0 (c (n "tectonic_xetex_format") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "tectonic_errors") (r ">=0.1.0, <1") (d #t) (k 0)))) (h "1vywg83pyc25l5zgfyn3zcbngidzaz728klhabh939cn5zqrbsm7")))

(define-public crate-tectonic_xetex_format-0.3.0 (c (n "tectonic_xetex_format") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "tectonic_errors") (r ">=0.1.0, <1") (d #t) (k 0)))) (h "1dsx4i9ljy7vpvz5280rwg5mh35vrvmhdaffajcdpr9r4kq3f8l4")))

(define-public crate-tectonic_xetex_format-0.3.1 (c (n "tectonic_xetex_format") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "tectonic_errors") (r ">=0.1.0, <1") (d #t) (k 0)))) (h "04jhqzy4cd7dg06b3qvmprgq6aid06rybhxlap1xr6k49d4z631h")))

(define-public crate-tectonic_xetex_format-0.3.2 (c (n "tectonic_xetex_format") (v "0.3.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "tectonic_errors") (r ">=0.1.0, <1") (d #t) (k 0)))) (h "1a45jm8q5gkx16s8prc3jjsw7zwf7n70rksl52prmjr9nlw5lmb4")))

