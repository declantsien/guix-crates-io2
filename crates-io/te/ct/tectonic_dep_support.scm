(define-module (crates-io te ct tectonic_dep_support) #:use-module (crates-io))

(define-public crate-tectonic_dep_support-0.1.0 (c (n "tectonic_dep_support") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.11") (d #t) (k 0)))) (h "0l901rjcjmx0d5wjrj95b32rj3lnnhb89hn4i0d29agndn45qw6j")))

(define-public crate-tectonic_dep_support-0.1.1 (c (n "tectonic_dep_support") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.11") (d #t) (k 0)))) (h "1sr9yvxar239qmhhzb9ql7j79iqlma11r83shalv9h82nzcky2cv")))

