(define-module (crates-io te ct tectonic_bridge_graphite2) #:use-module (crates-io))

(define-public crate-tectonic_bridge_graphite2-0.1.0 (c (n "tectonic_bridge_graphite2") (v "0.1.0") (d (list (d (n "tectonic_dep_support") (r "^0.1.0") (d #t) (k 1)))) (h "1s2gwicjqpj3kz65lpp15dfww3gfdd7mjvlqcragnpyndjkbp379") (l "graphite2")))

(define-public crate-tectonic_bridge_graphite2-0.1.1 (c (n "tectonic_bridge_graphite2") (v "0.1.1") (d (list (d (n "tectonic_dep_support") (r "^0.1.0") (d #t) (k 1)))) (h "1sq7bf7g42ig9r6msxwwx74rssi44587acqkdy6943mn8dcssbn8") (l "graphite2")))

(define-public crate-tectonic_bridge_graphite2-0.2.0 (c (n "tectonic_bridge_graphite2") (v "0.2.0") (d (list (d (n "tectonic_dep_support") (r ">=0.1.0, <1") (d #t) (k 1)))) (h "1qsq337y4p3z832kmn2xcaj6xh3z6ngr0izn4jgdjrymnsq1ac81") (l "graphite2")))

(define-public crate-tectonic_bridge_graphite2-0.2.1 (c (n "tectonic_bridge_graphite2") (v "0.2.1") (d (list (d (n "tectonic_dep_support") (r ">=0.1.0, <1") (d #t) (k 1)))) (h "1lkpqxz0y7m2ib1g0k850g2d7g6z8062niaw7vgvql33f2n94xkr") (l "graphite2")))

(define-public crate-tectonic_bridge_graphite2-0.2.2 (c (n "tectonic_bridge_graphite2") (v "0.2.2") (d (list (d (n "tectonic_dep_support") (r ">=0.1.0, <1") (d #t) (k 1)))) (h "0r0bi0qs0393vnh7q3yh71niwvqlm5qdzi0yq13z5v4przqi7lpc") (l "graphite2")))

