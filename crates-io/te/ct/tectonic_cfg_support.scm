(define-module (crates-io te ct tectonic_cfg_support) #:use-module (crates-io))

(define-public crate-tectonic_cfg_support-0.0.1 (c (n "tectonic_cfg_support") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1yrn27j2ggdg2ndm54qm9qnv5hcg80f3whc3qsk851wdm76i2hpg")))

(define-public crate-tectonic_cfg_support-0.0.2 (c (n "tectonic_cfg_support") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "17fj18crmb23v6gl66z6vvidppqy1abb5f6nhnjrwg7vll11crpg")))

(define-public crate-tectonic_cfg_support-0.0.3 (c (n "tectonic_cfg_support") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1l3svsnwac917ck3agjlfd00ncnrr9my4vcdm90j1jvxk0jdb55v")))

(define-public crate-tectonic_cfg_support-0.1.0 (c (n "tectonic_cfg_support") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0xn0dw2x4zjvcr0anml00v4iy5f5n44iyvaxqa7lciacmgnmdf15")))

(define-public crate-tectonic_cfg_support-0.1.1 (c (n "tectonic_cfg_support") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1a1bx5l945cv27zkkw5xjdmwjk1cd9gr7kwyspasnnxhp3gn2xc1")))

(define-public crate-tectonic_cfg_support-0.1.2 (c (n "tectonic_cfg_support") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1jsbk89g4s75cdav6350anls81k3lwaq6imhjb4q2c4cmr24i1cz")))

(define-public crate-tectonic_cfg_support-0.1.3 (c (n "tectonic_cfg_support") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1j2ib1jwfj31kx45xdxqlqck5zikvrqaq51p81smvi4755s0417b")))

(define-public crate-tectonic_cfg_support-0.1.4 (c (n "tectonic_cfg_support") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "07pd5fpqshxa7x8iky3hg4dznsx6xxf51s4336ynbvfw82ycis12")))

