(define-module (crates-io te ct tectonic_status_base) #:use-module (crates-io))

(define-public crate-tectonic_status_base-0.1.0 (c (n "tectonic_status_base") (v "0.1.0") (d (list (d (n "tectonic_errors") (r "^0.1.0") (d #t) (k 0)))) (h "0pdyva28cdrak2jcmw0i9blf3imyfg04h3dhg8zfn96s578wp000")))

(define-public crate-tectonic_status_base-0.2.0 (c (n "tectonic_status_base") (v "0.2.0") (d (list (d (n "tectonic_errors") (r ">=0.1.0, <1") (d #t) (k 0)))) (h "02pxbpvwdxg2jdjs4vygz0h6qb2c4zcqkdlcjx217xdrn13jcbz3")))

(define-public crate-tectonic_status_base-0.2.1 (c (n "tectonic_status_base") (v "0.2.1") (d (list (d (n "tectonic_errors") (r ">=0.1.0, <1") (d #t) (k 0)))) (h "11v0ds5jwkg4jm18n0a0qxhlrfh27inls0kyjsi42arrmqm0mcwk")))

