(define-module (crates-io te ct tectonic_bridge_freetype2) #:use-module (crates-io))

(define-public crate-tectonic_bridge_freetype2-0.1.0 (c (n "tectonic_bridge_freetype2") (v "0.1.0") (d (list (d (n "tectonic_dep_support") (r "^0.1.0") (d #t) (k 1)))) (h "1fgxhmdw4bz1pafyg17aqgjzmc1plvw02qqy420vxir1a32w19zb") (l "freetype2")))

(define-public crate-tectonic_bridge_freetype2-0.2.0 (c (n "tectonic_bridge_freetype2") (v "0.2.0") (d (list (d (n "tectonic_dep_support") (r ">=0.1.0, <1") (d #t) (k 1)))) (h "1vnn6c4cp1qaqnr74bi9mzh25l6dxi8h753lqrfkqz5wb57fv29b") (l "freetype2")))

