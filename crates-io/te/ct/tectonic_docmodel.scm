(define-module (crates-io te ct tectonic_docmodel) #:use-module (crates-io))

(define-public crate-tectonic_docmodel-0.1.0 (c (n "tectonic_docmodel") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "041hbp5rjrnnf6pbi7b9039jx5vn5f0d9fwhk0vshwjn69jmknkm")))

(define-public crate-tectonic_docmodel-0.1.1 (c (n "tectonic_docmodel") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0j0d3apa1djna1i4cpi0f22fmz3aprj6fs0mhy9arfhmdiwqalgz")))

(define-public crate-tectonic_docmodel-0.1.2 (c (n "tectonic_docmodel") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1b5vdqcnjbbda6am0mb7qyxyc6pn8v0pqz0w10xia87ycyyfflxw")))

(define-public crate-tectonic_docmodel-0.2.0 (c (n "tectonic_docmodel") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1z3yhw7h11a5xggi3bjip3kz1931mjl5gi53vx2zwd89rg99373y")))

(define-public crate-tectonic_docmodel-0.2.1 (c (n "tectonic_docmodel") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "176wl4f1na18s0bjmxf9chw3854jbpkh64nzsm32d2bvgja9acih")))

(define-public crate-tectonic_docmodel-0.2.2 (c (n "tectonic_docmodel") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1dn539wm8lcb34zqwybcwdnhj4yd44adk2wky0frx7s2lr5hfaas")))

