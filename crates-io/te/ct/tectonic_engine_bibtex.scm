(define-module (crates-io te ct tectonic_engine_bibtex) #:use-module (crates-io))

(define-public crate-tectonic_engine_bibtex-0.1.1 (c (n "tectonic_engine_bibtex") (v "0.1.1") (d (list (d (n "cbindgen") (r "^0.16") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tectonic_bridge_core") (r ">=0.1.0, <1") (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)))) (h "1l5s21yid2iidfn70bn4pq0dz7095dl0cd7h1d54abpz7zzs2z7q") (l "tectonic_engine_bibtex")))

(define-public crate-tectonic_engine_bibtex-0.1.2 (c (n "tectonic_engine_bibtex") (v "0.1.2") (d (list (d (n "cbindgen") (r "^0.16") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tectonic_bridge_core") (r ">=0.1.0, <1") (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)))) (h "0fbvigavidgxy1b5ia3nqxxmwh2ma9phha0q21mjzbzqn23m4ll6") (l "tectonic_engine_bibtex")))

(define-public crate-tectonic_engine_bibtex-0.1.3 (c (n "tectonic_engine_bibtex") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tectonic_bridge_core") (r ">=0.1.0, <1") (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)))) (h "1zwd7344iqwgfxq69n7m2nn7d89k77bkap92rdn7kkh7im91q8yx") (l "tectonic_engine_bibtex")))

(define-public crate-tectonic_engine_bibtex-0.1.4 (c (n "tectonic_engine_bibtex") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tectonic_bridge_core") (r ">=0.1.0, <1") (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)))) (h "06grwrz3vwk8b5j5j1wp1wx3nw8vw431kvb5nw1k43088n1sh1wr") (l "tectonic_engine_bibtex")))

(define-public crate-tectonic_engine_bibtex-0.1.5 (c (n "tectonic_engine_bibtex") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tectonic_bridge_core") (r ">=0.1.0, <1") (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)))) (h "1h6q7wba6i92rfhyp98dy6v3sv6ccjdkc8wwl202vc26pllw1cn9") (l "tectonic_engine_bibtex")))

(define-public crate-tectonic_engine_bibtex-0.2.0 (c (n "tectonic_engine_bibtex") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tectonic_bridge_core") (r ">=0.1.0, <1") (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "tectonic_io_base") (r ">=0.4.2, <1") (d #t) (k 0)))) (h "0bmh0glbbjlxailz2abilc5smr97xi19jm9fgay575z6nxkvpjl1") (l "tectonic_engine_bibtex")))

(define-public crate-tectonic_engine_bibtex-0.2.1 (c (n "tectonic_engine_bibtex") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tectonic_bridge_core") (r ">=0.1.0, <1") (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "tectonic_io_base") (r ">=0.4.2, <1") (d #t) (k 0)))) (h "1s08payrv1a57p9k0xl4ql5c0xyrvckfs6qpsn4ypl59mx4w2hqp") (l "tectonic_engine_bibtex")))

(define-public crate-tectonic_engine_bibtex-0.2.2 (c (n "tectonic_engine_bibtex") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tectonic_bridge_core") (r ">=0.1.0, <1") (d #t) (k 0)) (d (n "tectonic_errors") (r ">=0.2.0, <1") (d #t) (k 0)) (d (n "tectonic_io_base") (r ">=0.4.2, <1") (d #t) (k 0)))) (h "064rmml8hi3n0db0yn493vcj7cs2b1lj3cfhppa5h6qy3b54cgcc")))

