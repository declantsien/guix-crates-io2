(define-module (crates-io te ct tectonic_bridge_flate) #:use-module (crates-io))

(define-public crate-tectonic_bridge_flate-0.1.0 (c (n "tectonic_bridge_flate") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.16") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qsjbyjcr5crzvqh3801igzzgm71pqsw6fff43iqy36ras9hm19a") (l "tectonic_bridge_flate")))

(define-public crate-tectonic_bridge_flate-0.1.1 (c (n "tectonic_bridge_flate") (v "0.1.1") (d (list (d (n "cbindgen") (r "^0.16") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fa1jj1zcm9grlfrrdd1r2kjpzblfbrwv6a4vasczng93xiyprxx") (l "tectonic_bridge_flate")))

(define-public crate-tectonic_bridge_flate-0.1.2 (c (n "tectonic_bridge_flate") (v "0.1.2") (d (list (d (n "cbindgen") (r "^0.16") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wbkw77k1vhcvsl2kjvkvkz39il1zbmjlv5d3z0i18xzm33fdi1s") (l "tectonic_bridge_flate")))

(define-public crate-tectonic_bridge_flate-0.1.3 (c (n "tectonic_bridge_flate") (v "0.1.3") (d (list (d (n "cbindgen") (r "^0.16") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0313f63ar2v2c25qbyadymxw2b6vacg5jly1gzf4v7sy009vkzgs") (l "tectonic_bridge_flate")))

(define-public crate-tectonic_bridge_flate-0.1.4 (c (n "tectonic_bridge_flate") (v "0.1.4") (d (list (d (n "cbindgen") (r "^0.16") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pszfdgvhgn5w3s9r7vid85fsn5fbay3qvk29y5z85z0n6lr9vj5") (l "tectonic_bridge_flate")))

(define-public crate-tectonic_bridge_flate-0.1.5 (c (n "tectonic_bridge_flate") (v "0.1.5") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qhwdrn0m7p5fhwxyda5v38ypg97c5fipvbh2vx79dj2qdidc1za") (l "tectonic_bridge_flate")))

(define-public crate-tectonic_bridge_flate-0.1.6 (c (n "tectonic_bridge_flate") (v "0.1.6") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gc3kvfji85hq3vp75fy1ib866h2gmxwy3dymccvj8iqz0ymg5lf") (l "tectonic_bridge_flate")))

(define-public crate-tectonic_bridge_flate-0.1.7 (c (n "tectonic_bridge_flate") (v "0.1.7") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hz274rzlx528a8szlrpjxlv35sqw558wzybm2y63yy1s317z316") (l "tectonic_bridge_flate")))

