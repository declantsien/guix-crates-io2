(define-module (crates-io te xa texas) #:use-module (crates-io))

(define-public crate-Texas-0.1.0 (c (n "Texas") (v "0.1.0") (h "057gr8zv7ass05i8lbsxkz02s6z7iq4jhbgq8ciqmdaa9rrpwx44") (y #t)))

(define-public crate-Texas-0.1.2 (c (n "Texas") (v "0.1.2") (h "1acz390pdss859rj6r1s4ajqs1n1l4dsgnzl5js3y161r4shfx13") (y #t)))

(define-public crate-Texas-0.1.3 (c (n "Texas") (v "0.1.3") (h "1y8cglynkzinmrii9s6m5izd0ap7yhsnl15f0n1hk1dkhx2cmnnz") (y #t)))

(define-public crate-Texas-0.1.4 (c (n "Texas") (v "0.1.4") (h "01wvpqnfss2p3w51nsf83vy4rhnnykra08c7f8rk01n3pcjhcm34") (y #t)))

(define-public crate-Texas-0.1.5 (c (n "Texas") (v "0.1.5") (h "18bvkpmrwxzc7dmp38szirls7z443kc2di0xypr4qjnsf7z28lzq") (y #t)))

(define-public crate-Texas-0.1.6 (c (n "Texas") (v "0.1.6") (h "06wddw1xw6la1yg3nvr3i9b6l91hgdh1bh6kqizph6fhzxgxf7dl") (y #t)))

(define-public crate-Texas-0.1.7 (c (n "Texas") (v "0.1.7") (h "1dd7mji769djml3vlvsnhc4yy2nc0gnsbdj9ivz77krw89kyb6q5") (y #t)))

(define-public crate-Texas-0.1.8 (c (n "Texas") (v "0.1.8") (h "1a9lrybnb9chlh28dkz1yb8r6ad92903g7r9qi59afrx2jk8wdka") (y #t)))

(define-public crate-Texas-0.1.9 (c (n "Texas") (v "0.1.9") (h "0nqj0lm5dyhp29w214y8bcncyhddvxpgwxb82qrf990g1648wzn8") (y #t)))

(define-public crate-Texas-0.2.0 (c (n "Texas") (v "0.2.0") (h "08fi2q59xn0xmqbwagdigq3hqkzn1q9p67l3md07rxrfljib4z4l") (y #t)))

(define-public crate-Texas-0.2.1 (c (n "Texas") (v "0.2.1") (h "0kv56hkq11r4nfifikm8wms6xbv50zdj70g71dh0bd3xpzj1q6g6") (y #t)))

(define-public crate-Texas-0.2.2 (c (n "Texas") (v "0.2.2") (h "18can59v35b85s4gdixzjpqq7ynw50p7hi6h6fd119m816nfgj00") (y #t)))

(define-public crate-Texas-0.2.3 (c (n "Texas") (v "0.2.3") (h "0ypwxxfvlfvniwccm5wyg107hy75vhvvibyy6kh9hasfkcwya463") (y #t)))

