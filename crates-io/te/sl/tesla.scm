(define-module (crates-io te sl tesla) #:use-module (crates-io))

(define-public crate-tesla-0.1.0 (c (n "tesla") (v "0.1.0") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0afl8y11p5ivzjawfcghfqd3ncmv4qwzwr2snighq0xi9cr31gn4")))

