(define-module (crates-io te rb terbium) #:use-module (crates-io))

(define-public crate-terbium-0.0.0 (c (n "terbium") (v "0.0.0") (h "16bs74x0aqa3w89fcjxfc8zmj6iclh5q9qk6pxm26b394ssvh6k3") (y #t)))

(define-public crate-terbium-0.0.1 (c (n "terbium") (v "0.0.1") (d (list (d (n "terbium_grammar") (r "^0") (d #t) (k 0)))) (h "0l3jpl4k30whd2xdbhwibncq369ywkfzpy72j0pyxv8v85wksa4n")))

