(define-module (crates-io te me temex) #:use-module (crates-io))

(define-public crate-temex-0.10.0 (c (n "temex") (v "0.10.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "polars") (r "^0.30.0") (f (quote ("rows"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18iqnq87r2wxvmar6bf7xs7hfd58fykas5h3gim7qw9s7cs25qgv") (f (quote (("default" "polars")))) (s 2) (e (quote (("polars" "dep:polars"))))))

