(define-module (crates-io te pi tepimg) #:use-module (crates-io))

(define-public crate-tepimg-0.1.0 (c (n "tepimg") (v "0.1.0") (d (list (d (n "camino") (r "^1.1.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "png") (r "^0.17.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0n1mfqlsmrs5c09vs4j94wjdjj1zd4f8fi5vidhbbwy90513nma2")))

