(define-module (crates-io te ko teko-rs) #:use-module (crates-io))

(define-public crate-teko-rs-0.1.2 (c (n "teko-rs") (v "0.1.2") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "teko") (r "^0.1.3") (d #t) (k 0)))) (h "01dmnw9w7d301craplgz6xa75iawrfk7lqv8bdbxc1z5z1dnfqcy")))

(define-public crate-teko-rs-0.1.3 (c (n "teko-rs") (v "0.1.3") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "teko") (r "^0.1.3") (d #t) (k 0)))) (h "0s745s9f41xiyak0dd31ah5k9f99f67r7nd7by8qh2h1warmabv1") (f (quote (("default"))))))

(define-public crate-teko-rs-0.1.4 (c (n "teko-rs") (v "0.1.4") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "teko") (r "^0.1.3") (d #t) (k 0)))) (h "1hmks4wx9ynx3kjnzsr46n2cb4m1fs5hvi0wimbqxcc7gq4zxp2p") (f (quote (("default"))))))

(define-public crate-teko-rs-0.1.5 (c (n "teko-rs") (v "0.1.5") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "teko") (r "^0.1.4") (d #t) (k 0)))) (h "0vrgsqkibw7irdy5chymjncqi0avw5kjq8885wxz6gy191hav7vr") (f (quote (("default"))))))

(define-public crate-teko-rs-0.1.6 (c (n "teko-rs") (v "0.1.6") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "teko") (r "^0.1.6") (d #t) (k 0)))) (h "1d1xwg1ahalhnbw6286cwgln626civ7v6l95cfbd0mr44970m0cd") (f (quote (("default"))))))

(define-public crate-teko-rs-0.1.7 (c (n "teko-rs") (v "0.1.7") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "teko") (r "^0.1.7") (d #t) (k 0)))) (h "14pswdlbq62piqsr8fzyky4il7pdvi9vgnvy7ndrapr2322ihnvv") (f (quote (("default"))))))

(define-public crate-teko-rs-0.1.8 (c (n "teko-rs") (v "0.1.8") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "teko") (r "^0.1.8") (d #t) (k 0)))) (h "1ah0pb74gjpn8nd5hrbrhhpqms08jnsfcs965gmrvsajych3nb0v") (f (quote (("default"))))))

(define-public crate-teko-rs-0.1.9 (c (n "teko-rs") (v "0.1.9") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "teko") (r "^0.1.9") (d #t) (k 0)))) (h "0qf5ipbgybsvs2ncdi1a6svvsxk2vkv73ln9bsrc382alrrm3d1n") (f (quote (("default"))))))

