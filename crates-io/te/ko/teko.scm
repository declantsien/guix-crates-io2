(define-module (crates-io te ko teko) #:use-module (crates-io))

(define-public crate-teko-0.1.0 (c (n "teko") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1fkgpikdl8qgbjazchxi974smrpj6fhqrj3ryzzvmns7gcyk7sky") (f (quote (("default"))))))

(define-public crate-teko-0.1.1 (c (n "teko") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "0gqlh7f129w44x98igh9q449w2cix6ms4aq2ps6gm8bcypjzklmk") (f (quote (("default"))))))

(define-public crate-teko-0.1.2 (c (n "teko") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1iwmjh7l1ci48my2miplzh0mimd0hz3c2vvb2b0cizalqh937k1m") (f (quote (("default"))))))

(define-public crate-teko-0.1.3 (c (n "teko") (v "0.1.3") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1592yp36pl3ni7qrw8dg1jz23xg5qap90p8q1fwwxx846zm8pj2a") (f (quote (("default"))))))

(define-public crate-teko-0.1.4 (c (n "teko") (v "0.1.4") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1x1mc61s0h8fync3lffl68i8n57di8mslagvsavasxbi7xvbssmp") (f (quote (("default"))))))

(define-public crate-teko-0.1.6 (c (n "teko") (v "0.1.6") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "01bra43ynnd2rm8mbr59695m02l9l01fa3xzn13slk9gyi6kgaiy") (f (quote (("default"))))))

(define-public crate-teko-0.1.7 (c (n "teko") (v "0.1.7") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1nlwdyxp8l31mm89ldwiy4qvw4fw5q64ira6lrjhqgi60qrclb2g") (f (quote (("default"))))))

(define-public crate-teko-0.1.8 (c (n "teko") (v "0.1.8") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "13lyb405lhwv26q8qmp7sjcq41ri962qp1a002w30hhpc78pc67q") (f (quote (("default"))))))

(define-public crate-teko-0.1.9 (c (n "teko") (v "0.1.9") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "02mn42mp27irzymypsl08s1m1kbdxj65y0d8f2xrifkymjf0gpc0") (f (quote (("default"))))))

