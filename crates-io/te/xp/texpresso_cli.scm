(define-module (crates-io te xp texpresso_cli) #:use-module (crates-io))

(define-public crate-texpresso_cli-2.0.0 (c (n "texpresso_cli") (v "2.0.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ddsfile") (r "^0.5") (d #t) (k 0)) (d (n "jpeg-decoder") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "texpresso") (r "^2.0.0") (d #t) (k 0)))) (h "16kjyh4a3nf9y7ma9v4vc70gj1mdds664dr6jbff5llw4m9g7psv") (f (quote (("rayon" "texpresso/rayon") ("default" "rayon"))))))

(define-public crate-texpresso_cli-2.0.1 (c (n "texpresso_cli") (v "2.0.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ddsfile") (r "^0.5") (d #t) (k 0)) (d (n "jpeg-decoder") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "texpresso") (r "^2.0.1") (d #t) (k 0)))) (h "0i8g98ab9hldy02haicws7yn0n4r063fji77w4jhj449y2hp3zhv") (f (quote (("rayon" "texpresso/rayon") ("default" "rayon"))))))

