(define-module (crates-io te xp texpresso) #:use-module (crates-io))

(define-public crate-texpresso-2.0.0 (c (n "texpresso") (v "2.0.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1471yaqr1adn4k8yc8xvl4vvvp70wk5dw9rqq0rn2lw6bssnw20r")))

(define-public crate-texpresso-2.0.1 (c (n "texpresso") (v "2.0.1") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0dpkhymx9kx4asjjs08xv2rncqycmagp9mbk0wynkf9lr41yfxw2")))

