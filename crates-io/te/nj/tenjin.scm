(define-module (crates-io te nj tenjin) #:use-module (crates-io))

(define-public crate-tenjin-0.1.0 (c (n "tenjin") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pxcss6jw240b3fhvcznwvnhgxgy3kas8irmbf81w7hdxsjx19z1")))

(define-public crate-tenjin-0.2.0 (c (n "tenjin") (v "0.2.0") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "139p38wgph68lfh92s81i2giwj2nyglsdcz2pvqb3l114zxbkv5z")))

(define-public crate-tenjin-0.3.0 (c (n "tenjin") (v "0.3.0") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1ddvsazfakjdgy7pxcijs3yvkcgmhl1hmljh25j2cjbqphlj4rsh") (f (quote (("default" "serde_json" "toml"))))))

(define-public crate-tenjin-0.4.0 (c (n "tenjin") (v "0.4.0") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "04n43wwfbjf15m9k2lwfj9ff05k08habz8a8gc3bclgh140163q3") (f (quote (("default" "serde_json" "toml"))))))

(define-public crate-tenjin-0.4.1 (c (n "tenjin") (v "0.4.1") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0a86rh50828a6ssrisn24xral22s7z70ayvz1jb94pnqscybw857") (f (quote (("default" "serde_json" "toml"))))))

(define-public crate-tenjin-0.4.2 (c (n "tenjin") (v "0.4.2") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0f8a9mkyfyq4xm3asxg3006gm78q3i86bmdxz98h8gjacpl1xn6v") (f (quote (("default" "serde_json" "toml"))))))

(define-public crate-tenjin-0.5.0 (c (n "tenjin") (v "0.5.0") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1zijvhdp3qk0fnndrnwb90r3qsqzpbsdr3slf1677hdrmadilp5g") (f (quote (("default" "serde_json" "toml"))))))

(define-public crate-tenjin-0.5.1 (c (n "tenjin") (v "0.5.1") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.4") (o #t) (d #t) (k 0)))) (h "179mfxaxflyrq85lpr62iaiy5pwx7m22d6sxgcxzkcr5fb8rj52h") (f (quote (("default" "serde_json" "toml"))))))

