(define-module (crates-io te go tego) #:use-module (crates-io))

(define-public crate-tego-0.1.0 (c (n "tego") (v "0.1.0") (h "1za9w9xqy4fyjrs4gmmgi65hzqj822wp6kfplclvcrmhxv5c22d7")))

(define-public crate-tego-0.1.1 (c (n "tego") (v "0.1.1") (d (list (d (n "libflate") (r "^1.1.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14xj01aim11iip3ccxiibmmrnfxpr8g6gwg5md07acnfr2hnjsry") (y #t)))

(define-public crate-tego-0.1.2 (c (n "tego") (v "0.1.2") (d (list (d (n "libflate") (r "^1.1.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "152hlw8m6rv9bqgdxxvyjmjjsyqnfbyrn5cy7bhz8z0gh1bda4rj")))

(define-public crate-tego-0.1.3 (c (n "tego") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "libflate") (r "^1.1.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kbhxg930wnqbaxaa2svgxlx6dp8kwp32kn9nvh6bmvg66addfxf")))

(define-public crate-tego-0.2.0 (c (n "tego") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "image") (r "0.*") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11x6v3my733zagrl4zrf878yf0qr0yyx8vl8q08a3xyvf8fr202g")))

(define-public crate-tego-0.3.0 (c (n "tego") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "image") (r "0.*") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xzl45di9acpy0ym871m3hmqbbcg72m4kbwbw9l9592ri9qrdqva")))

(define-public crate-tego-0.3.1 (c (n "tego") (v "0.3.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "image") (r "0.*") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d7p3hhdavg5fx0i1fkqpx3rn8daa7kvqg0kjryh3hi9sbjqdicw")))

(define-public crate-tego-0.4.0 (c (n "tego") (v "0.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "image") (r "0.*") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18l9j6kkzg3vs4hg35c3k0jaxzc0yx3c04pcbx3xxki0gv6l094s")))

(define-public crate-tego-0.4.1 (c (n "tego") (v "0.4.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "image") (r "0.*") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y7wycxawbrjd0whr34gbcld4jc0bjxv4p1z7j8kidf53inhrdqg")))

(define-public crate-tego-0.5.0 (c (n "tego") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "image") (r "0.*") (d #t) (k 2)) (d (n "impl_ops") (r "^0.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a973cacn5h32gvx23rjzr8720rjfdilyxxr8byl7xgaijl2zzas")))

