(define-module (crates-io te t_ tet_rs) #:use-module (crates-io))

(define-public crate-tet_rs-0.1.1 (c (n "tet_rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "114mlxlkk1yjpdbdj47hx5mjqh9h3ywysarghwyc2wisg9wxnvi1") (f (quote (("serde1" "serde"))))))

(define-public crate-tet_rs-0.1.2 (c (n "tet_rs") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p2s504x6ly1phixdwnwm4fx3wawdlxpcs12hqksqhfq1pbx3zmq") (f (quote (("serde1" "serde"))))))

(define-public crate-tet_rs-0.2.1 (c (n "tet_rs") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "130naxjjx85y64rsibbpy1kx10jbva9jd58i36w17v5fp1nbcy4b") (f (quote (("serde1" "serde"))))))

(define-public crate-tet_rs-0.2.2 (c (n "tet_rs") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11lk9iv16q0w53pnzg1b1zxa71n72c4kh8p1qsv3jn4z159dgijd") (f (quote (("serde1" "serde"))))))

(define-public crate-tet_rs-0.3.1 (c (n "tet_rs") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nkj2hylky212ky2pmgaksngpyjdljlj1dfvhc8zgi1742ll9lc1") (f (quote (("serde1" "serde"))))))

