(define-module (crates-io te ne tenebrous-rpc) #:use-module (crates-io))

(define-public crate-tenebrous-rpc-0.1.0 (c (n "tenebrous-rpc") (v "0.1.0") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 1)))) (h "108bpwv3jgkazy3v9xfaikv8gsr60qix3mamwspah1ik3j8b6sfd")))

