(define-module (crates-io te pe tepe) #:use-module (crates-io))

(define-public crate-tepe-0.0.0 (c (n "tepe") (v "0.0.0") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "teloxide") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (d #t) (k 0)))) (h "0kzpgy0mccm2rja8z91vj1zgyxavizw0rf5q90hjhyy90q8xm60z")))

(define-public crate-tepe-0.0.1 (c (n "tepe") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "teloxide") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (d #t) (k 0)))) (h "0zpcij3iqjgkhn3ixpalbqri7l9sacjrkywflcic7c2myzqyhpgn")))

(define-public crate-tepe-0.0.2 (c (n "tepe") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "teloxide") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (d #t) (k 0)))) (h "1h0029bqp21mp3qcbnp5gv8wjd1ls6lvwhah39lkhqb7zz68x5pv")))

(define-public crate-tepe-0.0.3 (c (n "tepe") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "teloxide") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (d #t) (k 0)))) (h "053q4l1p9r9pxv2fqbws3qbb8r3qvjdzh9b3m6vpd8vcfa324nsz")))

(define-public crate-tepe-0.0.4 (c (n "tepe") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "teloxide") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (d #t) (k 0)))) (h "1psnj0166smdfm3wgqdhva7ynvxp8z5g93mabmjnrbrn5rb99vw1")))

(define-public crate-tepe-0.0.5 (c (n "tepe") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "teloxide") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("rt-threaded" "macros"))) (d #t) (k 0)))) (h "12c7fcpr15pps8d7mai95rh1m98r88liwdv8is48c61009bgfydv") (f (quote (("vendored-openssl" "openssl"))))))

