(define-module (crates-io te vm tevm) #:use-module (crates-io))

(define-public crate-tevm-0.1.0 (c (n "tevm") (v "0.1.0") (h "0qaji2nyhv6is9nqkldd2ri2gscx75mq0w3qkbzsvj80ra6077kd") (y #t)))

(define-public crate-tevm-0.2.0 (c (n "tevm") (v "0.2.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1") (d #t) (k 0)))) (h "1l4fwgwk0pcdv34hqjw5912k4zd78gkhzd622pzldisvyr5rv503") (y #t)))

(define-public crate-tevm-0.0.0 (c (n "tevm") (v "0.0.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1") (d #t) (k 0)))) (h "0fl4gpsq7p69bzkn1rc7z7jxf2q1c3234x9n6wdjxafbdyqs1yir") (y #t)))

(define-public crate-tevm-0.0.1 (c (n "tevm") (v "0.0.1") (h "0hhhdap149rgg4if7dcb59bhz2g3dw67q6zzij2bzablg8flcs6v") (y #t)))

