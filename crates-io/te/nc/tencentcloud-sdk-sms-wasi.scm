(define-module (crates-io te nc tencentcloud-sdk-sms-wasi) #:use-module (crates-io))

(define-public crate-tencentcloud-sdk-sms-wasi-0.2.1 (c (n "tencentcloud-sdk-sms-wasi") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "reqwest_wasi") (r "^0.11") (f (quote ("json" "wasmedge-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "sha256") (r "^1.4.0") (k 0)) (d (n "tokio_wasi") (r "^1.20.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1gp1nc5gsp529wiszylax6dskhcvqlpg89lgk7m4nqvq286qhkmm")))

