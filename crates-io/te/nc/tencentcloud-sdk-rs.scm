(define-module (crates-io te nc tencentcloud-sdk-rs) #:use-module (crates-io))

(define-public crate-tencentcloud-sdk-rs-0.1.0 (c (n "tencentcloud-sdk-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "sha256") (r "^1.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (d #t) (k 0)))) (h "0gihyk18kqzp2hamd90jlz2ha27glwls96iygm64gjwa5i79f9sz")))

