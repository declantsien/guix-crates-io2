(define-module (crates-io te ss tesseract-plumbing) #:use-module (crates-io))

(define-public crate-tesseract-plumbing-0.1.0 (c (n "tesseract-plumbing") (v "0.1.0") (d (list (d (n "leptonica-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.1") (d #t) (k 0)))) (h "1ad5na08maa1xcykahsqqnfq2qwfydw6s48fsjcb5490f02kvxyn")))

(define-public crate-tesseract-plumbing-0.2.0 (c (n "tesseract-plumbing") (v "0.2.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^0.1.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nzd5mj99jmiyk81qnp0mw8val2i4h8kmphphnpj19wcnyfp3zj1")))

(define-public crate-tesseract-plumbing-0.2.1 (c (n "tesseract-plumbing") (v "0.2.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^0.1.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02hskh7wga81ljlzwsskbyvb4qxap015g1gfasrgdkm9znp0yn1i")))

(define-public crate-tesseract-plumbing-0.3.0 (c (n "tesseract-plumbing") (v "0.3.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^0.1.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pm50si901nbsiv5hkiim173smgbxk8cxhjvwbkbbbcsz1day35w")))

(define-public crate-tesseract-plumbing-0.4.0 (c (n "tesseract-plumbing") (v "0.4.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^0.2.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zgzw9jhp2i9vhsp9z5pi9qd46fm72s18zdip5xg5xa0l8n84zhd")))

(define-public crate-tesseract-plumbing-0.5.0 (c (n "tesseract-plumbing") (v "0.5.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^0.2.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12z718k0182zhmdaanlnb9cmffpd7jkivvq7m8p7zgs1v1g537fn")))

(define-public crate-tesseract-plumbing-0.5.1 (c (n "tesseract-plumbing") (v "0.5.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^0.3.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1llniqaixssh4dwcax3lm5i1vqi0749anjmsc3w02l246b13bh40")))

(define-public crate-tesseract-plumbing-0.5.2 (c (n "tesseract-plumbing") (v "0.5.2") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^0.4.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "121agpwafprqmdbjq3pvgdnfy0blmg2r1sgi8f0hj6jiv4d2w8a9")))

(define-public crate-tesseract-plumbing-0.6.0 (c (n "tesseract-plumbing") (v "0.6.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^0.4.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gw0lal0n00l7dayf13ywhm3ddwnxh2slxbl4j59cg7429vg2amw")))

(define-public crate-tesseract-plumbing-0.6.1 (c (n "tesseract-plumbing") (v "0.6.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "~0.4") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xsqrh61fn68vb4kw5dkf3x25cdwghjznq5b8s3gcrr4b06w367g")))

(define-public crate-tesseract-plumbing-0.7.0 (c (n "tesseract-plumbing") (v "0.7.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "~0.4") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1njmp7s5rylvbx2cck8rhy0svc7iycpzqhxfjyn34dvxnnjcbkhw")))

(define-public crate-tesseract-plumbing-0.7.1 (c (n "tesseract-plumbing") (v "0.7.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "~0.6") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lrshv4p2pja0i4x03r2xrbzbnmr3zvbjm2crfs2jnx4rd25y4ha")))

(define-public crate-tesseract-plumbing-0.8.0 (c (n "tesseract-plumbing") (v "0.8.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^1.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dab66vy58rjcqp18j0x4ynrsk8w03xznrd5ca94m5b9a6wvnpx2")))

(define-public crate-tesseract-plumbing-0.9.0 (c (n "tesseract-plumbing") (v "0.9.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^1.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z0c32mxr3ml1jl0z3chlcy0scqc05g2bdl52bbaw0pxid42dlcg")))

(define-public crate-tesseract-plumbing-0.10.0 (c (n "tesseract-plumbing") (v "0.10.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^1.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mb6yx882v18v51vgvry9csgvdfcbnw98lwpfsi419gb54z6sj9y")))

(define-public crate-tesseract-plumbing-0.11.0 (c (n "tesseract-plumbing") (v "0.11.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "leptonica-plumbing") (r "^1.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "149127b400ax8p33vbam05pvsb66d8a0mpbkmwbkbl014b2h5yzp") (f (quote (("tesseract_5_2") ("default" "tesseract_5_2"))))))

