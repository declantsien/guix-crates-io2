(define-module (crates-io te ss tessor) #:use-module (crates-io))

(define-public crate-tessor-0.1.0 (c (n "tessor") (v "0.1.0") (h "1vvk89q4pficxhnb28pkhc2y5xq2y9bamzgpx6l6szlsifhgc7kd")))

(define-public crate-tessor-0.2.0 (c (n "tessor") (v "0.2.0") (h "11nw30p3scdm9wbdd9n58bzxrq3a8m2dhqjjglmn5gi2dx21lgzr")))

(define-public crate-tessor-0.3.0 (c (n "tessor") (v "0.3.0") (h "14mv71gi250ixan73lwaw08rg3c9f1da2inriwignk214mmlya67")))

