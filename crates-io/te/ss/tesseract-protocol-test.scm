(define-module (crates-io te ss tesseract-protocol-test) #:use-module (crates-io))

(define-public crate-tesseract-protocol-test-0.5.5 (c (n "tesseract-protocol-test") (v "0.5.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tesseract-one") (r "^0.5.5") (d #t) (k 0)))) (h "1irnqq5dm4m32mhbdx9qfi17axxz39lp07faylgdgix090b1gv8y") (f (quote (("service" "tesseract-one/service") ("client" "tesseract-one/client"))))))

(define-public crate-tesseract-protocol-test-0.5.6 (c (n "tesseract-protocol-test") (v "0.5.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tesseract-one") (r "^0.5.6") (d #t) (k 0)))) (h "15146vmi40s612wh5x6np72lna8am46v8aiyk9bmy7n42qb9vxp5") (f (quote (("service" "tesseract-one/service") ("client" "tesseract-one/client"))))))

