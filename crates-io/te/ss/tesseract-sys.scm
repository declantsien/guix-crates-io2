(define-module (crates-io te ss tesseract-sys) #:use-module (crates-io))

(define-public crate-tesseract-sys-0.1.0 (c (n "tesseract-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "11fmd4chj3cymkah7b2rjfdihdlxyh1j6nk6i7qpil3nxy58g55k")))

(define-public crate-tesseract-sys-0.2.0 (c (n "tesseract-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.49.3") (d #t) (k 1)) (d (n "leptonica-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1ifr59fv88h1xsbyama7w1zsfy1clcz1qdvrpdsfiv59hpfpfxil") (l "tesseract")))

(define-public crate-tesseract-sys-0.3.0 (c (n "tesseract-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.49.3") (d #t) (k 1)) (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)))) (h "07jm2ljx3jfiq8gsv585cbxz35zp2sj5d1iwfjyfgfr1jqrjzdw3") (l "tesseract")))

(define-public crate-tesseract-sys-0.3.1 (c (n "tesseract-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1c9v8042wdyi4lh1pcxl76q9kdpp9y80l7l3gv1ajzf6afijb0yk") (l "tesseract")))

(define-public crate-tesseract-sys-0.4.0 (c (n "tesseract-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)))) (h "09k6rj11mq20q39z22hp8fkn03bwrbjsm686d270yaa4nkg24ghl") (l "tesseract")))

(define-public crate-tesseract-sys-0.4.1 (c (n "tesseract-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1hvv9h1vccld6klmnrxrpz7yn7x22zxfrrr4a6d5qbdiwkl90yky") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.0 (c (n "tesseract-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "leptonica-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1bzln721yl5r35sc07rlpmwgbnlvk5v1ag4q9v84ljmmbs7k6khz") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.1 (c (n "tesseract-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "leptonica-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1p1g5cc43fdlrfwwxz66kr3zja574prs9i0334s3139bbpqys409") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.2 (c (n "tesseract-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "leptonica-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "0h79lk1x0kbx7mkhnhzzpmc2m6k7a4191b13isgg87wjlamggb9n") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.3 (c (n "tesseract-sys") (v "0.5.3") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.3") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1qqhxmkr2hqr8in5vqcfwk4ngbi52pdh89cksw3bvsmmv7fm6ki2") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.4 (c (n "tesseract-sys") (v "0.5.4") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.3") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "0kq660iddhvkws3vkkccyiqmhvw1dlncfmcs2ha3p3wlj1iyx5dh") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.5 (c (n "tesseract-sys") (v "0.5.5") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.3") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "0zydv1ssksclsipjjmsf5kcz56252j283ww1qzrc0fw8casy4s35") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.6 (c (n "tesseract-sys") (v "0.5.6") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1qa75lp1gyq4w1l7ibq1alkhb45khg3lh5jci35f4zkvb2i9vgqa") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.7 (c (n "tesseract-sys") (v "0.5.7") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "03qj1x9n34i8fnb1cn8rhsp7j4syimybskwi6sbpp9al436kj9gr") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.8 (c (n "tesseract-sys") (v "0.5.8") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1y047yanf4cci8iqqchrlbbqgpvhi4p8gxsifk2dx7lmncb4xyvq") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.9 (c (n "tesseract-sys") (v "0.5.9") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1zbl5fmz147p4ikbzddxa8d9qlmkdbq64myww4zngbfgm9vmlqfi") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.10 (c (n "tesseract-sys") (v "0.5.10") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1rw3d52lamcbbzqzwqyrq4chjzjy391ghiswda1c35bxr4nx8ggv") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.11 (c (n "tesseract-sys") (v "0.5.11") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1q9l33a8hcmafx5wf3knhq3651hvd86fpq39s8bkmkqsgxrbvx1x") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.12 (c (n "tesseract-sys") (v "0.5.12") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "09xwb5knkfv3y61cil97plqh7gss080cbfvn47ifdyr37i1qby1j") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.13 (c (n "tesseract-sys") (v "0.5.13") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "05lp6nakw00s27givsp240h93dahdqy8rmz79iifshi6gzx56h94") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.14 (c (n "tesseract-sys") (v "0.5.14") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1yirm17r8bvk7pc7napf0x9vpc9xwbsisqhr5lah6q1n0ki1b83h") (l "tesseract")))

(define-public crate-tesseract-sys-0.5.15 (c (n "tesseract-sys") (v "0.5.15") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "0yw5lp1riym49zjcay5xaxi9p8sdy36w1hl6zbqglk0j2vrgccxx") (l "tesseract")))

(define-public crate-tesseract-sys-0.6.0 (c (n "tesseract-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"linux\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "0dxjw1rl54dgp94j51dsm6x1i3kl9bh76w6p0g1pxyf2nnqggycb") (l "tesseract")))

(define-public crate-tesseract-sys-0.6.1 (c (n "tesseract-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "leptonica-sys") (r "~0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (t "cfg(any(target_os=\"macos\", target_os=\"linux\", target_os=\"freebsd\"))") (k 1)) (d (n "vcpkg") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 1)))) (h "1kan4h4lj93fzq7m09cis24184zppa0bf6h9bx5pw6k5l8xnhx0m") (l "tesseract")))

