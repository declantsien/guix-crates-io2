(define-module (crates-io te ss tesseract-swift) #:use-module (crates-io))

(define-public crate-tesseract-swift-0.5.3 (c (n "tesseract-swift") (v "0.5.3") (d (list (d (n "tesseract-swift-transport") (r "^0.5.3") (d #t) (k 0)) (d (n "tesseract-swift-utils") (r "^0.5.3") (d #t) (k 0)))) (h "1dilbp88c50anp1w6p1cwbqybkr6cb13c93236sgwi83sb1dy882") (f (quote (("transport-sdk") ("transport-all" "transport-sdk") ("service" "tesseract-swift-transport/service") ("default" "transport-all") ("client" "tesseract-swift-transport/client"))))))

(define-public crate-tesseract-swift-0.5.4 (c (n "tesseract-swift") (v "0.5.4") (d (list (d (n "tesseract-swift-transport") (r "^0.5.4") (d #t) (k 0)) (d (n "tesseract-swift-utils") (r "^0.5.4") (d #t) (k 0)))) (h "0nmkbh1wnkf3dbm41hnl1sphy9p9wqlmpa0a82maxw57liqsgzwa") (f (quote (("transport-sdk") ("transport-all" "transport-sdk") ("service" "tesseract-swift-transport/service") ("default" "transport-all") ("client" "tesseract-swift-transport/client"))))))

(define-public crate-tesseract-swift-0.5.6 (c (n "tesseract-swift") (v "0.5.6") (d (list (d (n "tesseract-swift-transport") (r "^0.5.6") (d #t) (k 0)) (d (n "tesseract-swift-utils") (r "^0.5.6") (d #t) (k 0)))) (h "1ilfzjrck8fjmq4b3dpdj7kmnqzf44l7h56dqh4znr3arnssnyvx") (f (quote (("transport-sdk") ("transport-all" "transport-sdk") ("service" "tesseract-swift-transport/service") ("default" "transport-all") ("client" "tesseract-swift-transport/client"))))))

