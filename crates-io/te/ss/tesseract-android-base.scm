(define-module (crates-io te ss tesseract-android-base) #:use-module (crates-io))

(define-public crate-tesseract-android-base-0.5.6 (c (n "tesseract-android-base") (v "0.5.6") (d (list (d (n "crabdroid") (r "^0.3.0") (d #t) (k 0)) (d (n "jni") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "tesseract-one") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "040bdq1ahhmcx46mnbk3gzcn7r519w2i3k0yp8dgwamglr1zhrgh") (f (quote (("service" "tesseract-one/service") ("client" "tesseract-one/client"))))))

