(define-module (crates-io te ss tesseract-native) #:use-module (crates-io))

(define-public crate-tesseract-native-0.0.1 (c (n "tesseract-native") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "flate2") (r "^1.0.25") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "14b5mx5kiy9gy25wp9m6zh3ypz5rcdnwid475015ay0vv6jxfmx6")))

