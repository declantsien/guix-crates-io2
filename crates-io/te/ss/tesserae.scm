(define-module (crates-io te ss tesserae) #:use-module (crates-io))

(define-public crate-tesserae-0.1.0 (c (n "tesserae") (v "0.1.0") (d (list (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.2") (d #t) (k 0)))) (h "0k4bay9g5yj7055s9mi85qx29jb0bnyfjsypd55rpr53pripx3az")))

(define-public crate-tesserae-0.1.1 (c (n "tesserae") (v "0.1.1") (d (list (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.2") (d #t) (k 0)))) (h "1pgaw29g0px0m73h28a6590n49lkrfpvfyvvsqqcpy53569vfa29")))

(define-public crate-tesserae-0.1.2 (c (n "tesserae") (v "0.1.2") (d (list (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.2") (d #t) (k 0)))) (h "1jh9xmq1vjy8cb5q47l5lksk1j1yd8bgwi0rp81bx44j0ndm3pw7")))

(define-public crate-tesserae-0.1.3 (c (n "tesserae") (v "0.1.3") (d (list (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.2") (d #t) (k 0)))) (h "1dk226m0sd134vjwi04llwzg0c4cprx59ixkh32dmp1iilvrsm0l")))

(define-public crate-tesserae-0.2.0 (c (n "tesserae") (v "0.2.0") (d (list (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.2") (d #t) (k 0)))) (h "1188zipbiyc8bhrhrsqvg74gar5bc6bgdim5vr7i82s4xpfmlmbq")))

(define-public crate-tesserae-0.2.1 (c (n "tesserae") (v "0.2.1") (d (list (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.2") (d #t) (k 0)))) (h "1p6gnsjp28g4jvm2vzgw9h832s43nks7rphv1gnach0q18lfl44y")))

(define-public crate-tesserae-0.2.2 (c (n "tesserae") (v "0.2.2") (d (list (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.2") (d #t) (k 0)))) (h "1x7qpgh8ypqgg08vqm1dwf0d8rml39vwc3fdlvb32wmin77wqxzn")))

(define-public crate-tesserae-0.3.0 (c (n "tesserae") (v "0.3.0") (d (list (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "sdl2") (r "^0.32.2") (d #t) (k 0)))) (h "1f5qi8fk6gm464kwzqyj33ib1wv1lk39zfkglf0fhsf62j3jib5v")))

(define-public crate-tesserae-0.3.1 (c (n "tesserae") (v "0.3.1") (d (list (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 2)) (d (n "byteorder") (r "1.3.*") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "sdl2") (r "0.33.*") (d #t) (k 0)))) (h "0bd6svazv9ipv36z6cx49m816wa6kv3yhjzpsdsl5daw7h0iy9b7")))

