(define-module (crates-io te ss tessie) #:use-module (crates-io))

(define-public crate-tessie-0.1.0 (c (n "tessie") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "08lmrrprdgs4a5k1fby5bha08bk6gz64z50qw1vbfjgj8vshz0xd")))

(define-public crate-tessie-0.1.1 (c (n "tessie") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0y2710hmfbh6ksj7pvg8yh52ww0rbcqrzw3r1zcpzkms5pjpvn1z")))

