(define-module (crates-io te ss tessellations) #:use-module (crates-io))

(define-public crate-tessellations-0.1.0 (c (n "tessellations") (v "0.1.0") (d (list (d (n "euclid") (r "^0.20.7") (d #t) (k 0)) (d (n "raqote") (r "^0.7.11") (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gb9z5lr3xijfb2clhvrar9dxbj574qbcv0n3bj5vazqcprfciw5")))

(define-public crate-tessellations-0.1.1 (c (n "tessellations") (v "0.1.1") (d (list (d (n "euclid") (r "^0.20.7") (d #t) (k 0)) (d (n "raqote") (r "^0.7.13") (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "1yw46mgx6az8l1mr6d7kx3v45vrfdv5w3jgs0r4flyssyz1xfp7s")))

(define-public crate-tessellations-0.1.2 (c (n "tessellations") (v "0.1.2") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "raqote") (r "^0.8.1") (f (quote ("png"))) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)))) (h "0a0c6wlfgnhnxspqky8hdbp11p6bd656kj9v6f8as4i0dwv0bsg6")))

(define-public crate-tessellations-0.2.0 (c (n "tessellations") (v "0.2.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "raqote") (r "^0.8.2") (f (quote ("png"))) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)) (d (n "svg") (r "^0.13") (d #t) (k 0)))) (h "1mpvk6n4pyhcm63mbyfy2abbvc77fgvpjxizq78jzd504j6zavf6")))

(define-public crate-tessellations-0.3.0 (c (n "tessellations") (v "0.3.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "lyon") (r "^1.0") (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)) (d (n "raqote") (r "^0.8.2") (f (quote ("png"))) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 2)) (d (n "svg") (r "^0.13") (d #t) (k 0)))) (h "144rh46ahw6iy8q19d0r58ilybg5grkvqldk1rqjajss3ppqrvn8")))

