(define-module (crates-io te ss tess2) #:use-module (crates-io))

(define-public crate-tess2-0.1.0 (c (n "tess2") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "itertools") (r "^0.7.4") (d #t) (k 0)))) (h "1zppx874hm7dyw96zf97qd58grsabjbs9pjalx2wd94jf6id8zxn")))

(define-public crate-tess2-0.2.0 (c (n "tess2") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "itertools") (r "^0.7.4") (d #t) (k 0)))) (h "0qlmc9g68m3wiynxl4n2hw080khpqcn2l50wcqwmax8ss7ldhi4d")))

(define-public crate-tess2-0.3.0 (c (n "tess2") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "itertools") (r "^0.7.4") (d #t) (k 0)) (d (n "tess2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "15fgqfgxwkhcsdsg173a43ngvc1cn2jj7iwd9gwyvj4rw6mb9v74")))

