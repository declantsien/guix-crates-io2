(define-module (crates-io te ss tesseract-swift-utils) #:use-module (crates-io))

(define-public crate-tesseract-swift-utils-0.5.3 (c (n "tesseract-swift-utils") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0f312cbxi1b3zvj77wmh1q62iah1gx88iiiavhrhlw3gp7rbw4z6") (s 2) (e (quote (("bigint" "dep:num-bigint"))))))

(define-public crate-tesseract-swift-utils-0.5.4 (c (n "tesseract-swift-utils") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1xxhlqpjf3rp0nvvmwbw7hvkvwal5m7m5nsl9kmicwmilm8qsz70") (s 2) (e (quote (("bigint" "dep:num-bigint"))))))

(define-public crate-tesseract-swift-utils-0.5.6 (c (n "tesseract-swift-utils") (v "0.5.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1gg1gwl9d9dfch5gx26gjb7g7kpxqsb8y1n8hs3vb0qfnwrx98f6") (s 2) (e (quote (("bigint" "dep:num-bigint"))))))

