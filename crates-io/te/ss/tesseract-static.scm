(define-module (crates-io te ss tesseract-static) #:use-module (crates-io))

(define-public crate-tesseract-static-0.1.0 (c (n "tesseract-static") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "flate2") (r "^1.0.25") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)))) (h "18c5fvc58rcsa2rhijmab2blxjlfrh00ba5ifjnz78wywybc695f")))

