(define-module (crates-io te ss tesseract) #:use-module (crates-io))

(define-public crate-tesseract-0.1.0 (c (n "tesseract") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.1") (d #t) (k 0)))) (h "1pm0nmvgvb3dd55q4knhibbqd655c2vksnzawv2awl5cb2clblrl")))

(define-public crate-tesseract-0.2.0 (c (n "tesseract") (v "0.2.0") (d (list (d (n "leptonica-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.2") (d #t) (k 0)))) (h "0nc5298iqp5qq41jpzzxbf8dxrv51xb04ilwzcxq4qk45r6i4n6l")))

(define-public crate-tesseract-0.3.0 (c (n "tesseract") (v "0.3.0") (d (list (d (n "leptonica-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0gi3q39rrjyi0x281jcqfwsjxrc9abww4zx356shp9bfbnlzvwz9")))

(define-public crate-tesseract-0.3.1 (c (n "tesseract") (v "0.3.1") (d (list (d (n "leptonica-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0rg3p8fxdrxg9ghlzcwx5arhdvxzhjls92g43pr7mf1wrhcsazqp")))

(define-public crate-tesseract-0.4.0 (c (n "tesseract") (v "0.4.0") (d (list (d (n "leptonica-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.1") (d #t) (k 0)))) (h "1jkmgjx5v56lrflb10brmkc200ll14ffnw5i0a3gyrwzwa00x2ay")))

(define-public crate-tesseract-0.5.0 (c (n "tesseract") (v "0.5.0") (d (list (d (n "leptonica-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.1") (d #t) (k 0)))) (h "07jvfh85apf194syxp6qd6b5q6qj4fn9wzallh1jzm8m0dg8hjqy")))

(define-public crate-tesseract-0.6.0 (c (n "tesseract") (v "0.6.0") (d (list (d (n "leptonica-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "172jiv0fmp6i3zymc5fixb3qa4bhyzspsypg3k9rqnh1jz95173x")))

(define-public crate-tesseract-0.6.1 (c (n "tesseract") (v "0.6.1") (d (list (d (n "leptonica-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c3jr0p3izjwlny86f8mc318pdhd1qqf1ygrphm8wvhhx5x2zkx8")))

(define-public crate-tesseract-0.6.2 (c (n "tesseract") (v "0.6.2") (d (list (d (n "leptonica-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mrdyjlqch87cpql4q35lnvz71m63mhngxizgrwr5ybl2fnninyc")))

(define-public crate-tesseract-0.7.1 (c (n "tesseract") (v "0.7.1") (d (list (d (n "leptonica-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bg9hsnji0df4c9yyn3gg6v08hy59c39p15llk523q3hrihzzb78")))

(define-public crate-tesseract-0.8.0 (c (n "tesseract") (v "0.8.0") (d (list (d (n "leptonica-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mvy55330kq8vz24bbg04l9m076wkcb5phjph5d3rdgf7ijp656n")))

(define-public crate-tesseract-0.9.0 (c (n "tesseract") (v "0.9.0") (d (list (d (n "tesseract-plumbing") (r "^0.3.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h4dssrqzhq6m35yql6h664brmyfrfjbv7fp3y04j3m3vysz1v33")))

(define-public crate-tesseract-0.10.0 (c (n "tesseract") (v "0.10.0") (d (list (d (n "tesseract-plumbing") (r "^0.3.0") (d #t) (k 0)) (d (n "tesseract-sys") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a5p77j4zfkkm60arhb8vcs48hjvw8g4n9qfg7ifbw0qf4xifmh5")))

(define-public crate-tesseract-0.10.1 (c (n "tesseract") (v "0.10.1") (d (list (d (n "tesseract-plumbing") (r "~0.6") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ny641g8izn1vmxp0pbzlpj1xswcx1c1s0r8iin7l09gsm1d3a93")))

(define-public crate-tesseract-0.11.0 (c (n "tesseract") (v "0.11.0") (d (list (d (n "tesseract-plumbing") (r "~0.6") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0afn445zfcbv9fdlbg7fkmsannfb52w2ddh5lbhhw1zqb8k3d90z")))

(define-public crate-tesseract-0.12.0 (c (n "tesseract") (v "0.12.0") (d (list (d (n "tesseract-plumbing") (r "~0.6") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1phi9pb2c4ckxz5sq3p7ly9fdl4nk7vfhzs2jrna8pkxgd3gvbpw")))

(define-public crate-tesseract-0.12.1 (c (n "tesseract") (v "0.12.1") (d (list (d (n "tesseract-plumbing") (r "~0.8") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m6sqj4mlxk7rsqp96q0slr7zb0lnbj9a97ashhj2q7biibc46dl")))

(define-public crate-tesseract-0.13.0 (c (n "tesseract") (v "0.13.0") (d (list (d (n "tesseract-plumbing") (r "~0.9") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12hvwkf1kxfzai4vqlz2jx2l29zk1wr7si9qc6bckpx3fcwddpah")))

(define-public crate-tesseract-0.14.0 (c (n "tesseract") (v "0.14.0") (d (list (d (n "tesseract-plumbing") (r "~0.10") (d #t) (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02i954ricmqkj322yaxyw8lnmlxda1ffvzgpjnq1ff5n133c5q1f")))

(define-public crate-tesseract-0.15.0 (c (n "tesseract") (v "0.15.0") (d (list (d (n "tesseract-plumbing") (r "~0.11") (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11lfr9lwprmla71xiln3phlj8mf5iman1zyzvwikdl48v5vni1gb") (f (quote (("tesseract_5_2" "tesseract-plumbing/tesseract_5_2") ("default" "tesseract_5_2"))))))

(define-public crate-tesseract-0.15.1 (c (n "tesseract") (v "0.15.1") (d (list (d (n "tesseract-plumbing") (r "~0.11") (k 0)) (d (n "tesseract-sys") (r "~0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19zxr5pg1xw47840zzlqx6swfyhxm7clm4nqxmb6dym2b8r5q392") (f (quote (("tesseract_5_2" "tesseract-plumbing/tesseract_5_2") ("default" "tesseract_5_2"))))))

