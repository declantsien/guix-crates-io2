(define-module (crates-io te ss tessor_viewer) #:use-module (crates-io))

(define-public crate-tessor_viewer-0.1.0 (c (n "tessor_viewer") (v "0.1.0") (d (list (d (n "gl") (r "^0.12") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "tessor") (r "^0.1") (d #t) (k 0)))) (h "05ikbq206ysa0pvimi60mm6aqrph9gd7kkxgw5d94dlxryc04hvf")))

(define-public crate-tessor_viewer-0.2.0 (c (n "tessor_viewer") (v "0.2.0") (d (list (d (n "gl") (r "^0.12") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "tessor") (r "^0.2") (d #t) (k 0)))) (h "1g2sls4a756h8kgxhk15xgrzqrislw83yvyzxk5s3j1i4wvisvsm")))

