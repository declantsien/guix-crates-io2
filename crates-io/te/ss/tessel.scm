(define-module (crates-io te ss tessel) #:use-module (crates-io))

(define-public crate-tessel-0.1.0 (c (n "tessel") (v "0.1.0") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "06m58mx4q3nkg6cspl0k6sr8pcvjjs1jzpwz8ysxg9snmnb6b03q")))

(define-public crate-tessel-0.2.0-beta (c (n "tessel") (v "0.2.0-beta") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "1b0ywdbqf70zlbh65wdf8h7szj0m5d8wni23mk1in1s2qlhv49r3") (y #t)))

(define-public crate-tessel-0.2.0 (c (n "tessel") (v "0.2.0") (d (list (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "0bj4zpkazwsvcqdl7nl7ibjyxn8d4ykf90rrhii3q31wv1dy9jyf")))

(define-public crate-tessel-0.3.0-beta.1 (c (n "tessel") (v "0.3.0-beta.1") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "0j78nd7mmdhxdbgqdd81ha3gqp67k1744712pwrdh0f2s52fraky")))

(define-public crate-tessel-0.3.0-beta.2 (c (n "tessel") (v "0.3.0-beta.2") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "0km8z11w40v8y5sin0iivwn2hqkcg47qlsf22fbi815iwi3qiz06")))

(define-public crate-tessel-0.3.0-beta.3 (c (n "tessel") (v "0.3.0-beta.3") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "02q1r54zygma3sp5n34znpi7h12xpf5ppc8ry8gxjd1a5kc8viwk")))

(define-public crate-tessel-0.3.0 (c (n "tessel") (v "0.3.0") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "072qmv6x5w88ayy5p493fzv1lr8sf7vpw343ns7gnh12skpjlvmy")))

(define-public crate-tessel-0.3.1 (c (n "tessel") (v "0.3.1") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)) (d (n "unix_socket") (r "^0.5.0") (d #t) (k 0)))) (h "107nkwyy3y28dkg02lb0xy3mfv37mr4nzc70av565sca6h091azv")))

