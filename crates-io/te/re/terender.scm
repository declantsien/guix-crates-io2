(define-module (crates-io te re terender) #:use-module (crates-io))

(define-public crate-terender-0.1.0 (c (n "terender") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)))) (h "1jz765asp9kv81r5cj03xaq3cq9wnbza7b7i1wag572aswvr1hzp")))

(define-public crate-terender-0.1.1 (c (n "terender") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)))) (h "1ip2rxabps5k2bw7qmizh0vxcxp7hf4v7834292lzy69cbxm02hv")))

(define-public crate-terender-0.1.2 (c (n "terender") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0a8x9xmgjyif1s6kx87fq6fmfysd96bmf6dpanvknax9wvgf847r")))

(define-public crate-terender-0.1.3 (c (n "terender") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "1lrp5cmya6fimip8g5kzlbhgqwyj6cn75myrsfxkar6a94x3m9nl")))

