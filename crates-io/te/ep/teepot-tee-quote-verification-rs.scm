(define-module (crates-io te ep teepot-tee-quote-verification-rs) #:use-module (crates-io))

(define-public crate-teepot-tee-quote-verification-rs-0.2.2 (c (n "teepot-tee-quote-verification-rs") (v "0.2.2") (d (list (d (n "intel-tee-quote-verification-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0gl7c0sd1y3dppsmb8bgdq642xkilx5aay63xy5fzz54pj2z7bs3")))

