(define-module (crates-io te dd teddy) #:use-module (crates-io))

(define-public crate-teddy-0.1.0 (c (n "teddy") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.5.1") (d #t) (k 0)) (d (n "memchr") (r "^0.1.9") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.3") (d #t) (k 2)) (d (n "simd") (r "^0.1.0") (d #t) (k 0)))) (h "1cz2xwgzhipqsr4hn64gm9kidvp0qma6p38q669b53pisyln965n")))

(define-public crate-teddy-0.1.1 (c (n "teddy") (v "0.1.1") (d (list (d (n "aho-corasick") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.3") (d #t) (k 2)) (d (n "simd") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0gkx5by5b1g7ihfran0361sswbfz06i62fqa37qzllflc84piw0a") (f (quote (("simd-accel" "simd"))))))

(define-public crate-teddy-0.2.0 (c (n "teddy") (v "0.2.0") (d (list (d (n "aho-corasick") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.3") (d #t) (k 2)) (d (n "simd") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0ajg1qn6f376f12rkmsh9v34fb5vcws17cbqdjpjvhzp9d0iaj2m") (f (quote (("simd-accel" "simd"))))))

