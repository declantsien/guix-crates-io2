(define-module (crates-io te ch tech_analysis) #:use-module (crates-io))

(define-public crate-tech_analysis-0.1.0 (c (n "tech_analysis") (v "0.1.0") (d (list (d (n "polars") (r "^0.32.1") (f (quote ("lazy" "describe"))) (d #t) (k 0)))) (h "00vlf4xbb8fz4kbabqv858cpa2pp392grb82z8isrd98cpis3a29")))

(define-public crate-tech_analysis-0.1.1 (c (n "tech_analysis") (v "0.1.1") (d (list (d (n "polars") (r "^0.32.1") (f (quote ("lazy" "describe"))) (d #t) (k 0)))) (h "175gpvs4wkgi6b1qv49qd8s9mwqdmhpgb74bpfsm8zl8k054x8hx")))

