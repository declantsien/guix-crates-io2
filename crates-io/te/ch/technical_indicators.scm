(define-module (crates-io te ch technical_indicators) #:use-module (crates-io))

(define-public crate-technical_indicators-0.1.0 (c (n "technical_indicators") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "json") (r "^0.11.9") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "1ldbanssvr8yz9n35pgnbgj9g7jf3x8cm5dbnj310f4xv6my2k20")))

(define-public crate-technical_indicators-0.2.0 (c (n "technical_indicators") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "json") (r "^0.11.9") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "0fvnll1f0030qgmsmw5kdihwc87k36y56g256fy2h13l05zsmyr6")))

(define-public crate-technical_indicators-0.3.0 (c (n "technical_indicators") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "json") (r "^0.11.12") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "0s3mw8l7xgs7nmf9hjf3wj133a8hd1awardx64nd2ldk1a2sgxkc")))

(define-public crate-technical_indicators-0.3.1 (c (n "technical_indicators") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "json") (r "^0.11.12") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "0qgwnyrimlmqfwsgpy30k6q9l8w65nji68v8zsgvv6g9xigqgwj7")))

(define-public crate-technical_indicators-0.4.0 (c (n "technical_indicators") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "json") (r "^0.11.12") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "1ihqjkxavy6wkcwgaq3yfh0vxc8rqh1lzdl6bljrzvig80n74lzm")))

(define-public crate-technical_indicators-0.5.0 (c (n "technical_indicators") (v "0.5.0") (d (list (d (n "assert_approx_eq") (r "^1") (d #t) (k 2)) (d (n "json") (r "^0.11.12") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)))) (h "0d4lbd0fwydxqaqy9cb1lc82wrbipp3igcwipr18b90j9zbqv8kc")))

