(define-module (crates-io te tr tetrs) #:use-module (crates-io))

(define-public crate-tetrs-0.0.3 (c (n "tetrs") (v "0.0.3") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0pbxh9fjdqq9db88nyz443r12kcryzy393qa8ag3vvzhqfbdg8lx")))

