(define-module (crates-io te tr tetromino-xlock-mode) #:use-module (crates-io))

(define-public crate-tetromino-xlock-mode-0.1.0 (c (n "tetromino-xlock-mode") (v "0.1.0") (d (list (d (n "raw-window-handle") (r "^0.5.2") (k 0)) (d (n "tetromino") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.2") (f (quote ("x11"))) (k 0)) (d (n "xlock") (r "^0.1") (d #t) (k 0) (p "tetromino-xlock-bindings")))) (h "0xm6pnqisxrszcsg3l03anrhxq9zbglahqz3x5z7fp6a3ckr5w2b")))

