(define-module (crates-io te tr tetromino-xlock-bindings) #:use-module (crates-io))

(define-public crate-tetromino-xlock-bindings-0.1.0+v5.77 (c (n "tetromino-xlock-bindings") (v "0.1.0+v5.77") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking"))) (o #t) (d #t) (k 1)) (d (n "tar") (r "^0.4.40") (o #t) (k 1)) (d (n "tempfile") (r "^3.4") (o #t) (d #t) (k 1)) (d (n "xz2") (r "^0.1.7") (o #t) (k 1)))) (h "1vgpj4gxndn3dsjbfzvbk2wc6xpbis77fp61k3xa2fy4nwi601qj") (f (quote (("generate-xlock-bindings" "bindgen") ("download-xlock-source" "reqwest" "tar" "tempfile" "xz2"))))))

