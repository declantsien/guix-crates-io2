(define-module (crates-io te tr tetr_ch) #:use-module (crates-io))

(define-public crate-tetr_ch-0.1.0 (c (n "tetr_ch") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "11750h2sx69kf77bg02wpv9a7j22qppj51h12g7xz5wvj5r88zcb")))

(define-public crate-tetr_ch-0.1.1 (c (n "tetr_ch") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1d128gzdkn5wqbimcc6y2jx81hm788k4wvrbbwik3znm9bm9911f")))

(define-public crate-tetr_ch-0.2.0 (c (n "tetr_ch") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1jfl51q37jdkl2z1aqsl622ywarigpp22qxvybh9bb2bfa8grnqa")))

(define-public crate-tetr_ch-0.2.1 (c (n "tetr_ch") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "04a97s5bdy03ar0v18x0j0xn9xkd7cav2vdrxddvvzvkx5y8ywyw")))

(define-public crate-tetr_ch-0.3.0 (c (n "tetr_ch") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1w2qwaghk5ngxrka8g5mbrsv93mrma8rb01xh7xclgv9fxg0y65b")))

(define-public crate-tetr_ch-0.3.1 (c (n "tetr_ch") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1css611k9swlv705dq2a0fg8nxg0kzrygrvzk2ih0jrrz6zzzwyf")))

(define-public crate-tetr_ch-0.3.2 (c (n "tetr_ch") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0qrc338dklfmacpf6na9gg8blgqk86kymfyrnahfz31n5hcr50yz")))

(define-public crate-tetr_ch-0.3.3 (c (n "tetr_ch") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0f26lqqzhvzwllindxi6dga2vagjn5g10rbhxfxp31a5b1l34nvw")))

(define-public crate-tetr_ch-0.3.4 (c (n "tetr_ch") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "09ixkmjma3j8p1si388cz2pf6j52rh1pnvms65nsr7idq0f57kma")))

(define-public crate-tetr_ch-0.3.5 (c (n "tetr_ch") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0cd1slzi2bggv3dkrix6sz29h2h69xijgfdjgm9jsjsp7jvn4zk8")))

(define-public crate-tetr_ch-0.4.0 (c (n "tetr_ch") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1zb02mvhr1adi43n6q8c8fv5vm0yal6g1hxs4n0xq1l3nawlvpiy")))

(define-public crate-tetr_ch-0.5.0 (c (n "tetr_ch") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "0kbppbkw1l4n7hh9ns0r2l3d75pp83gq4n0zkqrmwvfr7rwi89yx")))

(define-public crate-tetr_ch-0.5.1 (c (n "tetr_ch") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("clock"))) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "113nvmx57w7nwd66pmrwxz9ibikrxja5l2r5hd4frr6icxlfqmxp")))

