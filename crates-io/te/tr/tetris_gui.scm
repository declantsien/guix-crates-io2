(define-module (crates-io te tr tetris_gui) #:use-module (crates-io))

(define-public crate-tetris_gui-0.1.0 (c (n "tetris_gui") (v "0.1.0") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 0)) (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ddyhix488bx8554xrv2qmrc3h5arh0wjmn2zwyry310zadjpczw")))

(define-public crate-tetris_gui-0.1.1 (c (n "tetris_gui") (v "0.1.1") (d (list (d (n "eframe") (r "^0.20") (d #t) (k 0)) (d (n "egui") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m644p3l48445vwpsrph8nqbcz7h9dmjbqx4swwlpaklg8svfvh8")))

