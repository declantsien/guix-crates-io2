(define-module (crates-io te tr tetris-tui) #:use-module (crates-io))

(define-public crate-tetris-tui-0.1.0 (c (n "tetris-tui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)))) (h "149fa1bh7chllwi3bfb0jy3b7qzbsbzapc5nf15bix6zsg8pkffn")))

(define-public crate-tetris-tui-0.1.1 (c (n "tetris-tui") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)))) (h "1sknr9j41yymxxjis8jdsnapvcgbn187xd69w8y9mb8vhif88pw8")))

(define-public crate-tetris-tui-0.1.2 (c (n "tetris-tui") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)))) (h "10y1j0rjc5vqn7ir52qrbm0d8697bcjr4vnhi9a2l6i6phl2if4m")))

(define-public crate-tetris-tui-0.1.3 (c (n "tetris-tui") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)))) (h "0xc0jaspr2awlbg7arm5fr68hflka8pr89mk34qq2w9nbbsvn7p6")))

(define-public crate-tetris-tui-0.1.4 (c (n "tetris-tui") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1yanrrjfb72hnlgd6dcv8x9h0pvfvs66srxv01dj1nnnkc8wqc6d")))

(define-public crate-tetris-tui-0.1.5 (c (n "tetris-tui") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1bhwavf9l4b2b43zslb2n9inxwf8zkcz4zf7filwfdghbqwb8w1d")))

(define-public crate-tetris-tui-0.1.6 (c (n "tetris-tui") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0jc0l0fx65hl4w494j1xhhi99pycnmisih1mgw8cid7a73y5b9hj")))

(define-public crate-tetris-tui-0.1.8 (c (n "tetris-tui") (v "0.1.8") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "11s3kxqm68gqfax08f9nj9l8fs6qdh7wwdnd9jinicapizcxcs3s")))

(define-public crate-tetris-tui-0.1.9 (c (n "tetris-tui") (v "0.1.9") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0z27ijrqzivb6ki1jh646wmnaqy61f163qrs8jsj99iq2d58a9q4")))

(define-public crate-tetris-tui-0.2.0 (c (n "tetris-tui") (v "0.2.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1azn4nzl188hkrj7khs3d746f33cyk86qj8wic749955pc4ych9w")))

(define-public crate-tetris-tui-0.2.1 (c (n "tetris-tui") (v "0.2.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0v2vq7v9iaagri58xn05b66bj41rhd8zj4k6vz4j2z9s3zd0m3lq")))

(define-public crate-tetris-tui-0.2.2 (c (n "tetris-tui") (v "0.2.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "11md128bg0fkdd7lkjlpbkk237xb3vrwbx1za4m9xpzsivwwzl91")))

(define-public crate-tetris-tui-0.2.3 (c (n "tetris-tui") (v "0.2.3") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1bfz36vba5b4akd9mssxn0s7271cyicvrvmdjwam36hphlfi5nay")))

(define-public crate-tetris-tui-0.2.4 (c (n "tetris-tui") (v "0.2.4") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0d60dvjpswanhs0c4czqpzfk9l1bbx161dfhrmkcplkzal665ng3")))

(define-public crate-tetris-tui-0.2.5 (c (n "tetris-tui") (v "0.2.5") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "05vf2dnc0a6g6fq900ijr82rc8iw2rw8hnz68msj9ff64a3841hw")))

(define-public crate-tetris-tui-0.2.6 (c (n "tetris-tui") (v "0.2.6") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1w6rb8va0229ziwph6k74qkxjcxfwyhi6dmsmwpf3iw09ncl5m92")))

(define-public crate-tetris-tui-0.2.8 (c (n "tetris-tui") (v "0.2.8") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "15djid7iyg02dw68s1khjq439lyrss88aai3247zzyh1nw2cb883")))

(define-public crate-tetris-tui-0.2.9 (c (n "tetris-tui") (v "0.2.9") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0i3rsk8gpy91wm6irlgk13n9p3f16iaaa6wbis5hapn4ffqii20h")))

(define-public crate-tetris-tui-0.2.10 (c (n "tetris-tui") (v "0.2.10") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1qbdh1bz9g0yhcsbbxxgbaya3snxc38q287n89gxkf697ns21h80")))

