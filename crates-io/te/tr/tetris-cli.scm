(define-module (crates-io te tr tetris-cli) #:use-module (crates-io))

(define-public crate-tetris-cli-22.66.1 (c (n "tetris-cli") (v "22.66.1") (d (list (d (n "big_num") (r "^0.1.0") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "082m65d8hrcaw7432h2gdfzvbr9xa6lkmbl9l6hcql0fzangvxjq")))

(define-public crate-tetris-cli-22.67.1 (c (n "tetris-cli") (v "22.67.1") (d (list (d (n "big_num") (r "^0.1.0") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1v0f135byzs72vslhhpbdqm8a7w5ni2f1dw8ln6nar1gvpsx2wcv")))

(define-public crate-tetris-cli-22.67.2 (c (n "tetris-cli") (v "22.67.2") (d (list (d (n "big_num") (r "^0.1.0") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1qcc1j4yxg3g3wn45bypfybyxrngrvxgwcvniwbiqwrbc1ggys02")))

(define-public crate-tetris-cli-23.96.1 (c (n "tetris-cli") (v "23.96.1") (d (list (d (n "big_num") (r "^0.1.0") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "libmath") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0if68ws5mw5j2g3mn1bk2k2kx4cmbcd26q2k7ani98zbh16mwzcb")))

