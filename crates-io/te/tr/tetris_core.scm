(define-module (crates-io te tr tetris_core) #:use-module (crates-io))

(define-public crate-tetris_core-0.1.0 (c (n "tetris_core") (v "0.1.0") (h "1zjy0ans0higrmsjxwbh4py687hwr8gr7da9wxnlplj5ddhh4wjh")))

(define-public crate-tetris_core-0.2.0 (c (n "tetris_core") (v "0.2.0") (h "0523km28jsqkb20gg2jfdd9hh0w8is9fzf6yjf2pzw1qwhh73mim")))

(define-public crate-tetris_core-0.2.1 (c (n "tetris_core") (v "0.2.1") (h "1fixx2gpiaip1dsgwkpd5mb5gfz9251cihs6md8shnmn4ay1llwv")))

