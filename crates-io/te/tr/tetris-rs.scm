(define-module (crates-io te tr tetris-rs) #:use-module (crates-io))

(define-public crate-tetris-rs-0.1.0 (c (n "tetris-rs") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "03rs43d52631snb0qc9vskwh3dasp2h6a3c3q1l16hbzrpq5b162")))

(define-public crate-tetris-rs-0.1.2 (c (n "tetris-rs") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0x5smy8w98dm5kd9gmd09x5bd6vskqqwm8wz0qxccnvwixcjy6gx")))

(define-public crate-tetris-rs-0.1.3 (c (n "tetris-rs") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1sgfmbvpigvk1239qs5gd2ijvvazkhmjzfmkf1d7dak9x7q4czdc")))

(define-public crate-tetris-rs-0.1.4 (c (n "tetris-rs") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1h35bgz0gcr6irjl9m2r240wiljjjp6fqx3nlyh0djaqb12vyssk")))

