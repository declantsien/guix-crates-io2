(define-module (crates-io te ra teraron) #:use-module (crates-io))

(define-public crate-teraron-0.0.1 (c (n "teraron") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "ron") (r "^0.4.0") (d #t) (k 0)) (d (n "tera") (r "^0.11.18") (d #t) (k 0)))) (h "0khq8szhi4yfsgrid5kpw5wk908y0jmaszq6659wbpni2x3av28d")))

(define-public crate-teraron-0.1.0 (c (n "teraron") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "ron") (r "^0.4.0") (d #t) (k 0)) (d (n "tera") (r "^0.11.18") (d #t) (k 0)))) (h "1q211sdar463qmbrkgi8gyjlgqx7yb78za8i9cpj4p7g5w0psi4s")))

