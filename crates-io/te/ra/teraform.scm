(define-module (crates-io te ra teraform) #:use-module (crates-io))

(define-public crate-teraform-0.1.0 (c (n "teraform") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "1mp84l5ajv63bwcih8dsrs3qrzjalp140sjq14id4kpcshv2k5d2")))

