(define-module (crates-io te ra tera-hot) #:use-module (crates-io))

(define-public crate-tera-hot-0.1.0 (c (n "tera-hot") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "tera") (r "^1.5") (d #t) (k 0)))) (h "0main75b61brig530ay7h1hk51jw4bw61hgiaicj314qkh86b1r1")))

(define-public crate-tera-hot-0.2.0 (c (n "tera-hot") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "tera") (r "^1.5") (d #t) (k 0)))) (h "0rvcimh5pyfr5z29qyhhfpapsmc2iicrbcc8fp0my733q2dc2s2s")))

(define-public crate-tera-hot-0.3.0 (c (n "tera-hot") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "tera") (r "^1.5") (d #t) (k 0)))) (h "1c3qj78a7y4d59gdnc9lwpmzsivy25jx9r4x7cy2jcxbk08c9my7")))

(define-public crate-tera-hot-0.4.0 (c (n "tera-hot") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^5.0") (d #t) (k 0)) (d (n "tera") (r "^1.5") (d #t) (k 0)))) (h "1qrqxplxph55p760gg5k87iqyafp7nry7zpk68c0k57y203hpl94")))

