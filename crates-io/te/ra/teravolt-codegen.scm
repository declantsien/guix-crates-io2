(define-module (crates-io te ra teravolt-codegen) #:use-module (crates-io))

(define-public crate-teravolt-codegen-0.1.0 (c (n "teravolt-codegen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04iva4vq4k2nw3kwd01g1h2flffmvb78zl10rkihh2dz4hsc01zv") (y #t)))

(define-public crate-teravolt-codegen-0.2.0 (c (n "teravolt-codegen") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0avk0wrcc7wlsgx17kjqiy3ks0g1vnfsm0qwb2xg8sv4fjk3bfnf") (y #t)))

(define-public crate-teravolt-codegen-0.3.0 (c (n "teravolt-codegen") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n250kax7v6jfsyhprafp44hm2rifijakfbvwrkvh704dm5l0m7a") (y #t)))

(define-public crate-teravolt-codegen-0.3.1 (c (n "teravolt-codegen") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kkryv30ycjhl7zk3x5qdjqm1bh38aqa8fx8r4arsa0i9zdyd249") (y #t)))

(define-public crate-teravolt-codegen-0.3.2 (c (n "teravolt-codegen") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j7v9kk9xxvjz4qwz7fm06yln2y3g2s0ysr9wqgb2bcnvfsjlr5y") (y #t)))

(define-public crate-teravolt-codegen-0.3.3 (c (n "teravolt-codegen") (v "0.3.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vz5nfhf5xwimkgb6j8ybk28vg7fcjbxbzqnvpp2xv3csfla0fb0") (y #t)))

