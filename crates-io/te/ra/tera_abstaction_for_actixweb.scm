(define-module (crates-io te ra tera_abstaction_for_actixweb) #:use-module (crates-io))

(define-public crate-tera_abstaction_for_actixweb-0.1.0 (c (n "tera_abstaction_for_actixweb") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "actix-web-lab") (r "^0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "0x5hd14hwkmvpkp1vasrzs2zhasaw30w5xgqifi513x94wlxxsr1") (y #t)))

