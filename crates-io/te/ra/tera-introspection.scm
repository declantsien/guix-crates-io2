(define-module (crates-io te ra tera-introspection) #:use-module (crates-io))

(define-public crate-tera-introspection-0.1.0 (c (n "tera-introspection") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.4.1") (f (quote ("serde_json"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1fvn308w9qni3i95mgdkf8ps73nsz2gq823rvq6dv6m2hh2xjnl5")))

