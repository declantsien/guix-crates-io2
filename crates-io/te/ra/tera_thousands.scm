(define-module (crates-io te ra tera_thousands) #:use-module (crates-io))

(define-public crate-tera_thousands-0.1.0 (c (n "tera_thousands") (v "0.1.0") (d (list (d (n "tera") (r "^1.19.0") (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)))) (h "0cpk9z87zxis8fdka4ffhmhn2dwjz3ih8xv1876jrcdll2gw51qh")))

