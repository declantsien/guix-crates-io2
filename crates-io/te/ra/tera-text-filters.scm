(define-module (crates-io te ra tera-text-filters) #:use-module (crates-io))

(define-public crate-tera-text-filters-1.0.0 (c (n "tera-text-filters") (v "1.0.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "tera") (r "^1.5.0") (k 0)))) (h "1855h2jd132g3386cwm03m3j8c0pz0dm3c51anzkr7rc7avx1x7y")))

