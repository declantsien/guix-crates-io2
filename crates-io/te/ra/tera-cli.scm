(define-module (crates-io te ra tera-cli) #:use-module (crates-io))

(define-public crate-tera-cli-0.1.0 (c (n "tera-cli") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "tera") (r "^0.11.20") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1cl0arl76hgl3r9nj0frk1m8shr3px6h1859n372l3fkkm6yqjdc")))

(define-public crate-tera-cli-0.1.1 (c (n "tera-cli") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "tera") (r "^0.11.20") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "06ngjwl1n9pj7q16cdqanxqr7x3xfc6x21rwgr2z1v5a9ibk94ha")))

(define-public crate-tera-cli-0.2.0 (c (n "tera-cli") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "snafu") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "tera") (r "^0.11.20") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "1g8r6q7k9cxwx5n2jwgdvrzzajj0awbr0i70yisdcdjb5ixp08pz")))

(define-public crate-tera-cli-0.2.1 (c (n "tera-cli") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "tera") (r "^0.11.20") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0zw0wfaxlfyw1dg73s9azgmmib9ahijgraam3rsjqcpa00nq8ilq")))

(define-public crate-tera-cli-0.3.0 (c (n "tera-cli") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "snafu") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)) (d (n "tera") (r "^1.0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1blc7d4zpjp09f3ikf9abjxvkcbxm3kk4aks1akyyd1p56kj0vm7")))

(define-public crate-tera-cli-0.4.0 (c (n "tera-cli") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "snafu") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)) (d (n "tera") (r "^1.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "16mh59hv60zpmasb29kvq80mdrrvm86c4bg179l5vfgnafv0qdxc")))

(define-public crate-tera-cli-0.4.1 (c (n "tera-cli") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "snafu") (r "^0.6.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tera") (r "^1.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1g7mz4pgrckchalh4765pf25fp4spbva829w26g9fhsmvrs6ah4n")))

(define-public crate-tera-cli-0.5.0 (c (n "tera-cli") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.16") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tera") (r "^1.17.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "0xrkh0qkp225rrijivvz9qhlng51603f1yd9kpah4w2ph11xhv9n")))

