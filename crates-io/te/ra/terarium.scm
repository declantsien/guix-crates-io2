(define-module (crates-io te ra terarium) #:use-module (crates-io))

(define-public crate-terarium-0.1.0 (c (n "terarium") (v "0.1.0") (d (list (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0bshr2vglcbikg5khl92020vab7b45gw04s4lrbkyf7rq3shflhl") (r "1.71.0")))

(define-public crate-terarium-0.2.0 (c (n "terarium") (v "0.2.0") (d (list (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0kmyw6sdfrdw32whcncicg3azihjy89jjq1114x96sr4fp41n1bx") (r "1.71.0")))

(define-public crate-terarium-0.3.0 (c (n "terarium") (v "0.3.0") (d (list (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0rlqvcv8w8zsnmr9sk72lahfgniyqnqjbjprwkhpigk1nh9143hs") (r "1.71.0")))

