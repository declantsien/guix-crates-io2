(define-module (crates-io te ra tera-plaintext-filters) #:use-module (crates-io))

(define-public crate-tera-plaintext-filters-0.1.0 (c (n "tera-plaintext-filters") (v "0.1.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "1kpcdrk58gm4dx88rdjz1ymwbfr96yqkkrvgr4fs2fvq3n323v62")))

(define-public crate-tera-plaintext-filters-0.1.1 (c (n "tera-plaintext-filters") (v "0.1.1") (d (list (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "06qhm9nqg4saj1b5s1ycx4v9m8g7w3f465b5i8b8qdhpqi861n10")))

