(define-module (crates-io te ra tera-macro) #:use-module (crates-io))

(define-public crate-tera-macro-0.0.1 (c (n "tera-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0xn9p69r7q1hf65pf9xsrazljk0kqhh436smqfsk4fyfg4kkj2ki")))

