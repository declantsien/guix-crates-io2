(define-module (crates-io te xr texrender) #:use-module (crates-io))

(define-public crate-texrender-0.1.0 (c (n "texrender") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1xan3pnpyzvi723llk07vw1pavvh1b373x9apk4zl0zy0lhb4fcw")))

(define-public crate-texrender-0.1.1 (c (n "texrender") (v "0.1.1") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0s28zricdwfmxzac2sw52n5134cd66vnnj27w99wy43v430jz3n8")))

(define-public crate-texrender-0.1.2 (c (n "texrender") (v "0.1.2") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0x9bwsrbz507sm7ivm718gicrs57ygj1ljzlsa43zd241kwfr577")))

(define-public crate-texrender-0.2.0 (c (n "texrender") (v "0.2.0") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1blcdnxyx7l90n2xhh0savkqxs2p3dqb3nyhl13nk29p57rlnvkj")))

(define-public crate-texrender-0.2.1 (c (n "texrender") (v "0.2.1") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0cv9mn04nqnl4ipkaw7ghr2q11kmg52ih05m1n7p045vy462y6kq")))

(define-public crate-texrender-0.3.0 (c (n "texrender") (v "0.3.0") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0jx2x14czhmmfwa995b45hnf12wmy8xv8pm6cr5d12sd9ib2krz1")))

(define-public crate-texrender-0.3.1 (c (n "texrender") (v "0.3.1") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0q0ii8gkk22w4pmj6l03jxba9d9nm0skkqbxk3vvx7ri26qr4z4c")))

(define-public crate-texrender-0.3.2 (c (n "texrender") (v "0.3.2") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0v3n4i7w6np4pfs874ah62hyxychhphc3503rrjbwks75zz4xq91")))

(define-public crate-texrender-0.3.3 (c (n "texrender") (v "0.3.3") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0mmw37iqwmwx3npx810mcg97aqvx9f305s78q6ky19hglba4jl55")))

