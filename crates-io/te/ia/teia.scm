(define-module (crates-io te ia teia) #:use-module (crates-io))

(define-public crate-teia-0.1.0 (c (n "teia") (v "0.1.0") (d (list (d (n "botao") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0rsnbxymzmghdz85klafg84wcl623x4lf25dmb42ji1y08v635g9")))

