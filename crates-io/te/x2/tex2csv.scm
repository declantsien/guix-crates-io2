(define-module (crates-io te x2 tex2csv) #:use-module (crates-io))

(define-public crate-tex2csv-0.1.0 (c (n "tex2csv") (v "0.1.0") (h "1jh7p13cjwxnkha3m48kkfjh1b6am83vx2p1ykkjvn97qkwskamq")))

(define-public crate-tex2csv-0.2.0 (c (n "tex2csv") (v "0.2.0") (h "0hiqfdk493ch3lggs16002wxc8a877lf87w6i7wqc0grkx53max3")))

