(define-module (crates-io te ec teeconfig) #:use-module (crates-io))

(define-public crate-teeconfig-0.1.0-alpha.0 (c (n "teeconfig") (v "0.1.0-alpha.0") (d (list (d (n "bitflags") (r "^2.3.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "1icdc4cwi5zh82zflqzxdfnaphwyk0j1p0svhsln7kd57wc1paf8")))

(define-public crate-teeconfig-0.1.0-alpha.1 (c (n "teeconfig") (v "0.1.0-alpha.1") (d (list (d (n "bitflags") (r "^2.3.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "114flld9rfxag7rabya1am4i99bg9g6jsmvjx7ajbsl0nwqkckaf")))

(define-public crate-teeconfig-0.1.0-alpha.2 (c (n "teeconfig") (v "0.1.0-alpha.2") (d (list (d (n "bitflags") (r "^2.3.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "1fkgqgn25waklyz94l34wn8c30zwkvyhgppzkrvckyidxa4c1cwi")))

(define-public crate-teeconfig-0.1.0 (c (n "teeconfig") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "0iwsa8v490nj2sj8i2yn4ji0filjhd50sg0g2j1ma5zkayfvapzx")))

