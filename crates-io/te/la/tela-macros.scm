(define-module (crates-io te la tela-macros) #:use-module (crates-io))

(define-public crate-tela-macros-0.1.0 (c (n "tela-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1qig1fykadqjy8xnkaw1rmkz552pkx5dzw439xmi66y4xni56z34")))

(define-public crate-tela-macros-0.1.1 (c (n "tela-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0k7kqx9ynmfpi6vvlg4g2jvdi1bfcl5r2lynrx24ydh8xv2k87jz")))

(define-public crate-tela-macros-0.1.2 (c (n "tela-macros") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0xs3p8zd7f4ila1vdzrk19x0nc3kcpnjlhl880dz37ln63fsf8xi")))

