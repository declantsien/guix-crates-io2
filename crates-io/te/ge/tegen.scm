(define-module (crates-io te ge tegen) #:use-module (crates-io))

(define-public crate-tegen-0.1.0 (c (n "tegen") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ii6ypppc5bfn5jk1aqgljjbbd7a1bhcx2m4lfm4xys9n18z0hvf")))

(define-public crate-tegen-0.1.1 (c (n "tegen") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dxhyb20diz1cwymddqn0inki8mf7x339y689s0bqqa5qr0zv8gm")))

(define-public crate-tegen-0.1.2 (c (n "tegen") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n6rxk5pznmc909lddv24fc90irjgd2rx6phhhgg3d1xykgn5k1b")))

(define-public crate-tegen-0.1.3 (c (n "tegen") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "136kvfrsji7blgnrzd4wl7mhjfrx26a5ys44d424x5ms66igqi0l")))

(define-public crate-tegen-0.1.4 (c (n "tegen") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1k9a03cnj5ywbf6vs6wbn71p6r0kfnl395qk22s5kj5payixb8hh")))

