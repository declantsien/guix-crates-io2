(define-module (crates-io te eh teehistorian) #:use-module (crates-io))

(define-public crate-teehistorian-0.3.0 (c (n "teehistorian") (v "0.3.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "10am376mnpsy9gn18x6ca4jiwdlvc20x7v5akgjbz9d08m2zbz5q")))

(define-public crate-teehistorian-0.3.1 (c (n "teehistorian") (v "0.3.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "110ckisk17vlm2621y4vx1xwb6chzf2wqfbjklkg36ha1f687xaa")))

(define-public crate-teehistorian-0.3.2 (c (n "teehistorian") (v "0.3.2") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0j0pq4gmpb5zzzv93a4g6aqgga9vb5x7kh9w1wbcy9mjkyxq3jjk")))

(define-public crate-teehistorian-0.4.0 (c (n "teehistorian") (v "0.4.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0q0kl5zdkzyi6w4a7di49zickbhfany1yxgprkkc6yqafin078hb")))

(define-public crate-teehistorian-0.5.1 (c (n "teehistorian") (v "0.5.1") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1a45ccvds3lrkx31gljwg7pggdiqb84gl7rsan45qx5c2ixhfj6k") (f (quote (("capi"))))))

(define-public crate-teehistorian-0.6.0 (c (n "teehistorian") (v "0.6.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0ivybdsrhhbfgc35k4669xwq39fz1llln43v5zicpqlg3hzg2mbp") (f (quote (("capi"))))))

(define-public crate-teehistorian-0.7.0 (c (n "teehistorian") (v "0.7.0") (d (list (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0dxbr20z7gb9gj9g0lvxm36hm05xxg6disa2nkq6b1pyscsis9a6") (f (quote (("capi")))) (r "1.57.0")))

(define-public crate-teehistorian-0.7.1 (c (n "teehistorian") (v "0.7.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.6") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1jdm8dj0kqzr7v80sdwhj63wm41mixswwwqg12idggyabqfh7p48") (f (quote (("capi")))) (r "1.57.0")))

(define-public crate-teehistorian-0.8.0 (c (n "teehistorian") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.6") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1w223msydz237n70rp0wfc87ssbj56zfk8fj74091rzwwnvlj86z") (f (quote (("capi")))) (r "1.57.0")))

(define-public crate-teehistorian-0.8.1 (c (n "teehistorian") (v "0.8.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.6") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "07plww3bzf0hd15hllc2m588jmfj1nfwbrrgp65bkjk7wy8dfxls") (f (quote (("capi")))) (r "1.57.0")))

(define-public crate-teehistorian-0.8.2 (c (n "teehistorian") (v "0.8.2") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.8") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0pga9x1vvzr55sf58jb700v08pjcy5w364i7ir72nw83933bnww0") (f (quote (("capi")))) (r "1.57.0")))

(define-public crate-teehistorian-0.8.3 (c (n "teehistorian") (v "0.8.3") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.8") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1gr6dk2380jrn2cqw4jcbd21q0kmvzqxrfh5c7bpi9khxf99v887") (f (quote (("capi")))) (r "1.57.0")))

(define-public crate-teehistorian-0.9.0 (c (n "teehistorian") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.12") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "13il2nx32lixzbvi8ikvdphfqzxki8ii6wfdj02yw13f9ab2jp9v") (f (quote (("capi")))) (r "1.57.0")))

(define-public crate-teehistorian-0.10.0 (c (n "teehistorian") (v "0.10.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.12") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "12vzkwmkpv1zdhncpsgx2693lk11ab6j69z5rrhbv4awb62a1n29") (f (quote (("capi")))) (r "1.57.0")))

