(define-module (crates-io te eh teehee) #:use-module (crates-io))

(define-public crate-teehee-0.1.0 (c (n "teehee") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "xi-rope") (r "^0.3.0") (d #t) (k 0)))) (h "0qjwf702f8n52pzqn43sq4jvdgbbxij61d7lag9dqzmv8q4iz09l")))

(define-public crate-teehee-0.2.0 (c (n "teehee") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "jetscii") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "xi-rope") (r "^0.3.0") (d #t) (k 0)))) (h "0iwadib688gmqxwvxmsa49x4v7z2g3b5nhg2hjy2mx9qxljdngxk")))

(define-public crate-teehee-0.2.1 (c (n "teehee") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "jetscii") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "xi-rope") (r "^0.3.0") (d #t) (k 0)))) (h "1psj04388ixxf4d12h3j4s62ri531gvwgkqlmnrg7vr24xrakjz7") (y #t)))

(define-public crate-teehee-0.2.2 (c (n "teehee") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "jetscii") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "xi-rope") (r "^0.3.0") (d #t) (k 0)))) (h "19311x4qs1wy2h516m3y06szfpvial2q54lp90q7hl8iqr5a6hmp")))

(define-public crate-teehee-0.2.3 (c (n "teehee") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.17.5") (d #t) (k 0)) (d (n "jetscii") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "xi-rope") (r "^0.3.0") (d #t) (k 0)))) (h "1f741lnlb64337irm9injxi7591gsac5ril17p4jh1z9lfj4jq0k")))

(define-public crate-teehee-0.2.6 (c (n "teehee") (v "0.2.6") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "jetscii") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "xi-rope") (r "^0.3.0") (d #t) (k 0)))) (h "1815c0s5wc1525qircknml15nr2y86g3dzrzb4h2hsx7cgp8f3ym")))

(define-public crate-teehee-0.2.7 (c (n "teehee") (v "0.2.7") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "jetscii") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "xi-rope") (r "^0.3.0") (d #t) (k 0)))) (h "15wmj03j9s5wdmyryj0a5cn2nrf50vj7vrw79aw9k2rb10hrrrzl")))

(define-public crate-teehee-0.2.8 (c (n "teehee") (v "0.2.8") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "jetscii") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "xi-rope") (r "^0.3.0") (d #t) (k 0)))) (h "1zml3vwrnh64lsydc425rdf81fzdbbg48g5ydpy3qas8iy6x08ns")))

