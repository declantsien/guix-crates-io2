(define-module (crates-io te rm termal) #:use-module (crates-io))

(define-public crate-termal-0.1.0 (c (n "termal") (v "0.1.0") (d (list (d (n "termal_codes") (r "^0.1.0") (d #t) (k 0)) (d (n "termal_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0gy1crlnpymri1wrg003gj1b19s2jpcar3cxwck8sxalfnjzas0d")))

(define-public crate-termal-1.0.0 (c (n "termal") (v "1.0.0") (d (list (d (n "termal_core") (r "^1.0.0") (d #t) (k 0)) (d (n "termal_proc") (r "^1.0.0") (d #t) (k 0)))) (h "1rv5wrsja5nc756mqdf9v4fd1rh3q8k94bcsq4f9b9iw98b3phx5")))

(define-public crate-termal-1.0.1 (c (n "termal") (v "1.0.1") (d (list (d (n "termal_core") (r "^1.0.0") (d #t) (k 0)) (d (n "termal_proc") (r "^1.0.0") (d #t) (k 0)))) (h "1sicwivwcjfzj61cjicssm7vr7c68hbwl7f1pqypcynkyalxqnd2")))

(define-public crate-termal-1.0.2 (c (n "termal") (v "1.0.2") (d (list (d (n "termal_core") (r "^1.0.1") (d #t) (k 0)) (d (n "termal_proc") (r "^1.0.1") (d #t) (k 0)))) (h "0a1r958gh58wfw0bxf953jsrf5dqlx7dziv3xqlx1lxm0z8dafiv")))

