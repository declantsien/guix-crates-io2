(define-module (crates-io te rm term-handler) #:use-module (crates-io))

(define-public crate-term-handler-0.1.0 (c (n "term-handler") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "signal-simple") (r "^0.1") (d #t) (k 0)))) (h "14j2xjx4fwm8pvn8ddfp2774044gda2yivssk82bgfbfx3y6n28r")))

