(define-module (crates-io te rm termrun) #:use-module (crates-io))

(define-public crate-termrun-0.1.0 (c (n "termrun") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.95") (d #t) (k 0)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)))) (h "1vdl8c0gygk0dilrgjqcp3d8jb6ghr2h08xnp08fq743v103fdxl")))

