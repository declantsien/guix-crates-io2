(define-module (crates-io te rm termpixels) #:use-module (crates-io))

(define-public crate-termpixels-0.1.0 (c (n "termpixels") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0q7kqa5rwshfv4myqism5b07li8c0ll9svbay8ssv5g541zjkcmd")))

(define-public crate-termpixels-0.2.0 (c (n "termpixels") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "05iczv8ndsvmbps7pskri9aqbz3mkq8qk2jdjbgbsns4j9brqz6j")))

(define-public crate-termpixels-0.2.1 (c (n "termpixels") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "14iyrnnal2601pf7mrwkqvfwanvisw8zynvzvswfq6l5pmwcp2k0")))

(define-public crate-termpixels-0.2.2 (c (n "termpixels") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "008mzj9g8n4fzzkyhhcas6qq0byhncz7i83qjyh81kzs213jdcrj")))

(define-public crate-termpixels-0.2.3 (c (n "termpixels") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1fk753nmxj0ka7w98n6n39b8wnw6h43xg12vghmydhgbfwcq9glv")))

(define-public crate-termpixels-0.3.0 (c (n "termpixels") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "termpixels_derive") (r "^0.1.0") (d #t) (k 0)))) (h "04igypk1pparas3h8dv8rxzfvyz1sybc0cr8q3i725gy7d3cmnv7")))

(define-public crate-termpixels-0.4.0 (c (n "termpixels") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "termpixels_derive") (r "^0.4.0") (d #t) (k 0)))) (h "113wl65npqkv05qnp1i8pl23c9vk0bpbd95l77gq883krpr9m68s")))

(define-public crate-termpixels-0.4.1 (c (n "termpixels") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "termpixels_derive") (r "^0.4.0") (d #t) (k 0)))) (h "1lynycnyqg1a20v6gd7zipprwr2qh2r745m5v0mlvnrmpd55fxb3")))

(define-public crate-termpixels-0.5.0 (c (n "termpixels") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "termpixels_derive") (r "^0.5.0") (d #t) (k 0)))) (h "1dxa3dxy9sqq5591pn2k9dds57bwnrfh1pa5a52dbc08hpyps21a")))

(define-public crate-termpixels-0.6.0 (c (n "termpixels") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1avfrw4h1gam2z9scd74cww1dn0krja67kamny5gfkw26xar1g8k")))

(define-public crate-termpixels-0.7.0 (c (n "termpixels") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1plrg2jkc3847ih3y8a8hbam6mwi4w6i1kgj28x953k0l8kby3hq")))

(define-public crate-termpixels-0.7.1 (c (n "termpixels") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "15n5zwn2nwcw6ni6rci9hmhib9vvi7zz6b7ghpwn8aid4k2aldq8")))

