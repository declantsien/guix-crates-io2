(define-module (crates-io te rm terminity_widgets_proc) #:use-module (crates-io))

(define-public crate-terminity_widgets_proc-0.1.0 (c (n "terminity_widgets_proc") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "021qv8kf4kn2xm9c10qk4rkrcfx1d3fqck5m89dphacadmni8vds")))

