(define-module (crates-io te rm term_input) #:use-module (crates-io))

(define-public crate-term_input-0.1.0 (c (n "term_input") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)) (d (n "nix") (r "^0.7") (d #t) (k 0)))) (h "124j5xciyv6qh40bx8v8q0wz5bby1wfl38d6bfa9h3l9xqf0vkyw")))

(define-public crate-term_input-0.1.1 (c (n "term_input") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)) (d (n "nix") (r "^0.7") (d #t) (k 0)))) (h "1wd2ygyn0bl2nr9804d2sfphk0vqfn14sprw166zi8y5bnxyr8sb")))

(define-public crate-term_input-0.1.2 (c (n "term_input") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)) (d (n "nix") (r "^0.7") (d #t) (k 0)))) (h "1yvix2wvy99vaq1gj2566fwmhbprhgc78695dxjwzidcw3rgp25b")))

(define-public crate-term_input-0.1.3 (c (n "term_input") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)) (d (n "nix") (r "^0.7") (d #t) (k 0)))) (h "1k5n8awb7vafrw72wnp3czmrbixfjnvxm9lcj45lqlhh5870c1g1")))

(define-public crate-term_input-0.1.4 (c (n "term_input") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)) (d (n "nix") (r "^0.7") (d #t) (k 0)))) (h "1kii76kip69fdk7kr062y9jbbw9l24hcr1n88iaaspk1kqsjh45f")))

(define-public crate-term_input-0.1.5 (c (n "term_input") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)) (d (n "nix") (r "^0.7") (d #t) (k 0)))) (h "096818bsqzpj7135mqhqwjhqp20wscl8722cbl3x0hqada0xzlja")))

