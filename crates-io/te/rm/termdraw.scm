(define-module (crates-io te rm termdraw) #:use-module (crates-io))

(define-public crate-termdraw-0.1.0 (c (n "termdraw") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0pwsqwv7kxq5xssfbb4mznkb38br02pkhl1nviicshn9i4b9820b")))

(define-public crate-termdraw-0.1.1 (c (n "termdraw") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1yyxg430vmq534jlvgs0f17k8k5z93pibz13prks6h227r6nrg4z")))

(define-public crate-termdraw-0.1.2 (c (n "termdraw") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "191p2y8r4yagzglb7x46iswrdbqhb4nvj8b91sjqz5p7xm9hz5mb")))

(define-public crate-termdraw-0.1.3 (c (n "termdraw") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0rj2bncvv4wg37l09rx833xl9jxgqvq4n4qfmg3655bpnp12fi88")))

(define-public crate-termdraw-0.1.4 (c (n "termdraw") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0rhwkgpk68m6v7fzgc20wmvmilz473ll8zaslh4683d9q5myrjw6")))

(define-public crate-termdraw-0.1.5 (c (n "termdraw") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1rjn0p1bbfxxkrhp9av5xgf6hh6ma2m0l2pkd2r7fshpmpmiycnn")))

(define-public crate-termdraw-0.1.6 (c (n "termdraw") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0s7fkk5m3dynh7ni0jbfzrn971amn12xmjd677akip6jiifkyiiz")))

