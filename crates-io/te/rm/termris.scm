(define-module (crates-io te rm termris) #:use-module (crates-io))

(define-public crate-termris-0.1.0 (c (n "termris") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)))) (h "13fqa244jk3j73v3yrzicvx5yv01fmv37am5sd2772dvjkcs3s6v") (y #t)))

(define-public crate-termris-0.1.1 (c (n "termris") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)))) (h "15wlxwh6ys1rlb0209jxyhwdwvzd4a62yn2bf07j8a78lhhzgc99") (y #t)))

(define-public crate-termris-0.1.2 (c (n "termris") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)))) (h "0vwyhgwrjkagn0n71g6jpbwm730w0bbr3vlan7z6kf1d8pk9h2id") (y #t)))

(define-public crate-termris-0.3.1 (c (n "termris") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mvc-rs") (r "^0.1") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)))) (h "133lk1agp7ihk0gi2q8gpa0s0cqw24gdwqxixzfc7c0y908i0x75") (y #t)))

(define-public crate-termris-0.3.3 (c (n "termris") (v "0.3.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)))) (h "19blz1ggw3574cxrxcqpxnyl7blwiinjv4vb2g7nvnal96cfa8rv") (y #t)))

(define-public crate-termris-0.3.4 (c (n "termris") (v "0.3.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w036b7yids50qds3j451hjvikzba26515dkdb0f9d4j55h3ipg5") (y #t)))

(define-public crate-termris-3.4.0 (c (n "termris") (v "3.4.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qi50xyi139hk4y5dasss935wmnzzwm5xs3fjpca3ba44jhzy7rd") (y #t)))

(define-public crate-termris-3.4.1 (c (n "termris") (v "3.4.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ha274bn8ljz750fg4lgymimdjiqmbga75k2dnwrw3mbhvipsagq") (y #t)))

(define-public crate-termris-3.4.2 (c (n "termris") (v "3.4.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jf3i4girw6ddmpbhhrjq7lv484p4m3yliw5xlp15cfd87r3y80a") (y #t)))

(define-public crate-termris-3.4.3 (c (n "termris") (v "3.4.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "161xkb3355x6f5708sqj8yqff4y98ck8f4agmdvbsns01chvrn8h") (y #t)))

(define-public crate-termris-3.4.4 (c (n "termris") (v "3.4.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "spin_sleep") (r "=1.1.1") (d #t) (k 0)))) (h "1h9ais580idb1vywjjipma0qryqrr64n4qil08a06p1myj57098h") (y #t)))

(define-public crate-termris-3.4.5 (c (n "termris") (v "3.4.5") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "prayterm") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "spin_sleep") (r "=1.1.1") (d #t) (k 0)))) (h "111ijfd2bib2vrj5qm4bzmli9qkp4nxjifs63p2iissp62i0ksdy")))

