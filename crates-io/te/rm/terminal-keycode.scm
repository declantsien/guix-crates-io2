(define-module (crates-io te rm terminal-keycode) #:use-module (crates-io))

(define-public crate-terminal-keycode-1.0.0 (c (n "terminal-keycode") (v "1.0.0") (d (list (d (n "raw_tty") (r "^0.1.0") (d #t) (k 2)))) (h "0cq1ii26wcxnksz52vknywjdf9pm8fhi8flqvrllj63sgvifiwqn")))

(define-public crate-terminal-keycode-1.1.0 (c (n "terminal-keycode") (v "1.1.0") (d (list (d (n "raw_tty") (r "^0.1.0") (d #t) (k 2)))) (h "1lzq7709fpa1s1909inzax1v2v546v00pvsn2jppsmpq191slsqm")))

(define-public crate-terminal-keycode-1.1.1 (c (n "terminal-keycode") (v "1.1.1") (d (list (d (n "raw_tty") (r "^0.1.0") (d #t) (k 2)))) (h "0f2gsxk38faj91ihnnl9px9pl3f7qlkyfnpidz0wdhx6bwh6s8cy")))

