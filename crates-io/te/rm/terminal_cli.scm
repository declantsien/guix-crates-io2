(define-module (crates-io te rm terminal_cli) #:use-module (crates-io))

(define-public crate-terminal_cli-0.1.0 (c (n "terminal_cli") (v "0.1.0") (h "0ylv0n229b5ljwsg5r89i8hmh8r0kya8dbr47vmzyy4hiw9iikys")))

(define-public crate-terminal_cli-0.1.1 (c (n "terminal_cli") (v "0.1.1") (h "0ch9zqanmz2xl28z4hsyv5gm5mvjcjix1hlhyb202d6b1fwbljnl")))

(define-public crate-terminal_cli-0.1.2 (c (n "terminal_cli") (v "0.1.2") (h "16py9m8ykmdspcqy0fi9s7rd6d4b04hvi86kbv14dwknbgqc6dvf")))

(define-public crate-terminal_cli-0.2.0 (c (n "terminal_cli") (v "0.2.0") (h "09bj05j8wshnz59ajhynp7fdd4fycm13h3q9rr6n7cy0n8axsng7") (f (quote (("std") ("no_std") ("default" "std"))))))

