(define-module (crates-io te rm terminalcolor) #:use-module (crates-io))

(define-public crate-terminalcolor-0.1.0 (c (n "terminalcolor") (v "0.1.0") (h "1v5qac04cd08yrn0qixnzf67fw7rhbq9p999a5ayhy2m0ch6gm4j")))

(define-public crate-terminalcolor-0.1.1 (c (n "terminalcolor") (v "0.1.1") (h "0sr5xp2iha00c4kvwf0fbajj9iblrsf2951lmj7r9c2y8gqnz44k")))

(define-public crate-terminalcolor-0.1.2 (c (n "terminalcolor") (v "0.1.2") (h "1j904rg5j2vzswy9n8rpfqghqmn7pzw2bhn9b3wgjymkqvz6ij97")))

