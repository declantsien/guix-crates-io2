(define-module (crates-io te rm termal_core) #:use-module (crates-io))

(define-public crate-termal_core-1.0.0 (c (n "termal_core") (v "1.0.0") (d (list (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "place_macro") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "144j5kb9qgim08zpzwl0y3a87ym5rryxfy0zqj21g33678w18z5z")))

(define-public crate-termal_core-1.0.1 (c (n "termal_core") (v "1.0.1") (d (list (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "place_macro") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1kmxsy7fcvvz3r65xpmncnahqwmg6iahwy436mdizdvm1whwjz83")))

