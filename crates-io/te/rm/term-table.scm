(define-module (crates-io te rm term-table) #:use-module (crates-io))

(define-public crate-term-table-0.1.0 (c (n "term-table") (v "0.1.0") (d (list (d (n "wcwidth") (r "^1.0.1") (d #t) (k 0)))) (h "01rj186dmi1k7h4qz5k3n89pgcl8100cr9w5f2gv754i06jp2dlh")))

(define-public crate-term-table-0.1.1 (c (n "term-table") (v "0.1.1") (d (list (d (n "wcwidth") (r "^1.0.1") (d #t) (k 0)))) (h "1gfwb9nihb7xirvq7hncwnn1aka8s3fn0z868wh4vy8w5hqwb66r")))

(define-public crate-term-table-0.1.2 (c (n "term-table") (v "0.1.2") (d (list (d (n "wcwidth") (r "^1.0.1") (d #t) (k 0)))) (h "0w71mckyai7cad38mxng80xsmczgwxzy4nm6f8ryzn55i3ag7xq5")))

(define-public crate-term-table-0.1.3 (c (n "term-table") (v "0.1.3") (d (list (d (n "wcwidth") (r "^1.0.1") (d #t) (k 0)))) (h "0iphzs5lhksnas1dagym6bg0mx6i1cfjbq8kjn9k3b5v1bz34wz6")))

(define-public crate-term-table-0.1.4 (c (n "term-table") (v "0.1.4") (d (list (d (n "wcwidth") (r "^1.0.1") (d #t) (k 0)))) (h "05dwg9b2s60dxa6zk1yp25fwkpdkiha6aqbski5rjxks8j39lzkk")))

(define-public crate-term-table-0.1.5 (c (n "term-table") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "wcwidth") (r "^1.0.1") (d #t) (k 0)))) (h "02h6zpvwms5cldbprn8j36ka4n4l0r8n0d8llbn33wgj6fqn4rhp")))

(define-public crate-term-table-0.1.6 (c (n "term-table") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "wcwidth") (r "^1.0.1") (d #t) (k 0)))) (h "07vmbqxrplw7pj32l4xvhyqzb7kkrgk8s2a2b48r4zncmpk6akv0")))

(define-public crate-term-table-1.0.0 (c (n "term-table") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "wcwidth") (r "^1.0.1") (d #t) (k 0)))) (h "1vl2kif39irm25v9irkrwd49qsxy9ygwzm2vsvwj160l86xcyibr")))

(define-public crate-term-table-1.1.0 (c (n "term-table") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "wcwidth") (r "^1.0.1") (d #t) (k 0)))) (h "073l7fq1lz276pgzxfdpibqfwd8mcrn38z1z9lhjijk5fyvryi0g")))

(define-public crate-term-table-1.2.0 (c (n "term-table") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "17lzwsk7rr40y8wfivyddjwrnxy04snr48hsil9qdnb74a6x85wq")))

(define-public crate-term-table-1.2.1 (c (n "term-table") (v "1.2.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1v5f6c0qcffw8n3qv5x0mbg5qd7l6p2z9yv5bfrq5ydx8lw6gmna")))

(define-public crate-term-table-1.3.0 (c (n "term-table") (v "1.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "01ai81vxw7sr48arkzk6xxkgmkbrrzlfk5k2r7s9j1rl07w5x86y")))

(define-public crate-term-table-1.3.1 (c (n "term-table") (v "1.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1wrinqnjgx0w8968biz9f2fwcd37i2n8ya4vgjg95kx08c18fjli")))

(define-public crate-term-table-1.3.2 (c (n "term-table") (v "1.3.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1f6kcrjhl680iqp715z6a1pazyfpwj0qvgk8lpi7s58kndzrvrfm")))

