(define-module (crates-io te rm termwrap) #:use-module (crates-io))

(define-public crate-termwrap-0.1.0 (c (n "termwrap") (v "0.1.0") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "10icyv6ad6kyy5vyxxbsjvpg89nnsgvj3hjlckgq13aj1zw8fk9c")))

(define-public crate-termwrap-0.1.1 (c (n "termwrap") (v "0.1.1") (d (list (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1qyyfvl0jd9f9km8hvmprpp2crglh0ygz31zhl7z6d8gx8gs0qqq")))

