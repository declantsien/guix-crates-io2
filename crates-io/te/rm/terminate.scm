(define-module (crates-io te rm terminate) #:use-module (crates-io))

(define-public crate-terminate-0.1.0 (c (n "terminate") (v "0.1.0") (h "0w42wxnrx4qkzasnwr2gnqzrjwf9pqc708yqw7xcjg38p2i7ri3j")))

(define-public crate-terminate-0.2.0 (c (n "terminate") (v "0.2.0") (h "0pa19503fxxqyvjzgvsvswd8h94qcyq640g4f2fa4pq9g26r6r47")))

(define-public crate-terminate-0.2.1 (c (n "terminate") (v "0.2.1") (h "06xk47cmwwgsvgbs4xx2z8l86nk42pgzdpksywq82b0rq3hdxgbb")))

