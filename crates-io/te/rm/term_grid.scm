(define-module (crates-io te rm term_grid) #:use-module (crates-io))

(define-public crate-term_grid-0.1.0 (c (n "term_grid") (v "0.1.0") (d (list (d (n "unicode-width") (r "*") (d #t) (k 0)))) (h "1q13rgkb75l5rmjw17nbadjayjifwmgx6sn3y566pc7li74x7sdk")))

(define-public crate-term_grid-0.1.1 (c (n "term_grid") (v "0.1.1") (d (list (d (n "unicode-width") (r "*") (d #t) (k 0)))) (h "1i8nfq9rw8v5nj489scyyfzzm0hx8rj63h9fqhnvzddb3mkpwk9g")))

(define-public crate-term_grid-0.1.2 (c (n "term_grid") (v "0.1.2") (d (list (d (n "unicode-width") (r "*") (d #t) (k 0)))) (h "1x6m9nwspv7dkixwvvjbmkfx0ng1szj0l0s0xxgccdafbxs9spp3")))

(define-public crate-term_grid-0.1.3 (c (n "term_grid") (v "0.1.3") (d (list (d (n "unicode-width") (r "^0.1.3") (d #t) (k 0)))) (h "0r7yzknaf5wlhzg8rqgjinqq2r6gx6xcncwlrr738db93iky55p7")))

(define-public crate-term_grid-0.1.4 (c (n "term_grid") (v "0.1.4") (d (list (d (n "unicode-width") (r "^0.1.3") (d #t) (k 0)))) (h "03kwvk5k00ibrjq5yqxvxxgxwcwsf1iz1xjp8c74iiwnji0vv1p8")))

(define-public crate-term_grid-0.1.5 (c (n "term_grid") (v "0.1.5") (d (list (d (n "unicode-width") (r "^0.1.3") (d #t) (k 0)))) (h "0hw3mkx3vmv7339dnar07kkk1a220xpxdv51hfk75kwnaj3h5hnc")))

(define-public crate-term_grid-0.1.6 (c (n "term_grid") (v "0.1.6") (d (list (d (n "unicode-width") (r "^0.1.3") (d #t) (k 0)))) (h "1ib9wcha75bm02dv8g5ax7q1yr6b5p0spc8janx4g8saiyv4csmm")))

(define-public crate-term_grid-0.1.7 (c (n "term_grid") (v "0.1.7") (d (list (d (n "unicode-width") (r "^0.1.3") (d #t) (k 0)))) (h "1kq2sy3b8329jrsrpcvijvyz4gbqjyvyy6c3n0wmmvda9y03w393")))

(define-public crate-term_grid-0.2.0 (c (n "term_grid") (v "0.2.0") (d (list (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1dy47lybz4rcjmrw9s1rq4m18wnknqyv4m9r3pbhygyb0mvypjd7")))

