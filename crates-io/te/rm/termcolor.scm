(define-module (crates-io te rm termcolor) #:use-module (crates-io))

(define-public crate-termcolor-0.1.0 (c (n "termcolor") (v "0.1.0") (d (list (d (n "wincolor") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0l9qvngr2l3fp2m5kmhj4wznc83r70ibqfaa76f6p7hblmd0r6gh")))

(define-public crate-termcolor-0.1.1 (c (n "termcolor") (v "0.1.1") (d (list (d (n "wincolor") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "062kgd4c33hz83adbpc9wisdxamr53rrlk4bv8hfa5c6279fk0j0")))

(define-public crate-termcolor-0.2.0 (c (n "termcolor") (v "0.2.0") (d (list (d (n "wincolor") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0ljxhak0pynbhfh84yp79m025n6x146c5x0yck1b5m7bjhrn4dg0")))

(define-public crate-termcolor-0.3.0 (c (n "termcolor") (v "0.3.0") (d (list (d (n "wincolor") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "1pzg0v4pnml637cckyqbw799kd3wb7maclj7157lfwd6k0cgsziq")))

(define-public crate-termcolor-0.3.1 (c (n "termcolor") (v "0.3.1") (d (list (d (n "wincolor") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0bq3hqrpw604fb6bjnnvx7jb9v7m0pdlw6xv12s7r57hxgl7rjfh")))

(define-public crate-termcolor-0.3.2 (c (n "termcolor") (v "0.3.2") (d (list (d (n "wincolor") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1jlxbypxl1z048bv18hqy3d53j5ylvg37ff4c93030lddfjr6lcs")))

(define-public crate-termcolor-0.3.3 (c (n "termcolor") (v "0.3.3") (d (list (d (n "wincolor") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0x9c4h5xvh4h20d1kynj86rmb10bb6c1wvymlcx4ahrykknvqrch")))

(define-public crate-termcolor-0.3.4 (c (n "termcolor") (v "0.3.4") (d (list (d (n "wincolor") (r "^0.1.6") (d #t) (t "cfg(windows)") (k 0)))) (h "0znwy9prvgql2739vn35bgdbm91abyrhc5pjlr0la2klvab3is3k")))

(define-public crate-termcolor-0.3.5 (c (n "termcolor") (v "0.3.5") (d (list (d (n "wincolor") (r "^0.1.6") (d #t) (t "cfg(windows)") (k 0)))) (h "18dx67bv3kws0n9dcrpdalj0mihygk9fxpjdfwgzkya45qsmdi2n")))

(define-public crate-termcolor-0.3.6 (c (n "termcolor") (v "0.3.6") (d (list (d (n "wincolor") (r "^0.1.6") (d #t) (t "cfg(windows)") (k 0)))) (h "10sg0w2xhwz5zn84xnqrba5mc1jcc9dfa0xg25ph3gs1mmz5ii5d")))

(define-public crate-termcolor-1.0.0 (c (n "termcolor") (v "1.0.0") (d (list (d (n "wincolor") (r "^1") (d #t) (t "cfg(windows)") (k 0)))) (h "1pfc97q8ji63xmr4cs5q59p6qcr55qkhp5j31cjrjd4k4cw04b55")))

(define-public crate-termcolor-1.0.1 (c (n "termcolor") (v "1.0.1") (d (list (d (n "wincolor") (r "^1") (d #t) (t "cfg(windows)") (k 0)))) (h "0di8gv8d8269d68bwlx0yk7v8h6m1bcikd6rzz2a57akl322c93j")))

(define-public crate-termcolor-1.0.2 (c (n "termcolor") (v "1.0.2") (d (list (d (n "wincolor") (r "^1") (d #t) (t "cfg(windows)") (k 0)))) (h "161n5z62prsfmk3bnpr6gjinbayrj97jqskv55q8hvbh3x7z941k")))

(define-public crate-termcolor-1.0.3 (c (n "termcolor") (v "1.0.3") (d (list (d (n "wincolor") (r "^1") (d #t) (t "cfg(windows)") (k 0)))) (h "1ix2wx4wvjaw9kf7wxjd4gzmcvijn02d8zh3wyag2nav8q7aqfzz")))

(define-public crate-termcolor-1.0.4 (c (n "termcolor") (v "1.0.4") (d (list (d (n "wincolor") (r "^1") (d #t) (t "cfg(windows)") (k 0)))) (h "0bvzdmna2qjgdj6yasjyczic30fwhr8bvkgxya4j4qhj0vbsv5j0")))

(define-public crate-termcolor-1.0.5 (c (n "termcolor") (v "1.0.5") (d (list (d (n "wincolor") (r "^1") (d #t) (t "cfg(windows)") (k 0)))) (h "0vjfsn1a8zvqhnrbygrz1id6yckwv1dncw3w4zj65qdx0f00kmln")))

(define-public crate-termcolor-1.1.0 (c (n "termcolor") (v "1.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "winapi-util") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0pyp8vc0gx7124y80ixdl6plbfn1yjhw04i875k5fz2dk8lglsxv")))

(define-public crate-termcolor-1.1.1 (c (n "termcolor") (v "1.1.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "winapi-util") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "03p2n50vnagql8jl8vpksb0vz6w5qj146rfszhhryf1mn5p6f4dz")))

(define-public crate-termcolor-1.1.2 (c (n "termcolor") (v "1.1.2") (d (list (d (n "winapi-util") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1x65i1ny4m6z1by62ra6wdcrd557p2ysm866x0pg60zby2cxizid")))

(define-public crate-termcolor-1.1.3 (c (n "termcolor") (v "1.1.3") (d (list (d (n "winapi-util") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0mbpflskhnz3jf312k50vn0hqbql8ga2rk0k79pkgchip4q4vcms")))

(define-public crate-termcolor-1.2.0 (c (n "termcolor") (v "1.2.0") (d (list (d (n "winapi-util") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1dmrbsljxpfng905qkaxljlwjhv8h0i3969cbiv5rb7y8a4wymdy")))

(define-public crate-termcolor-1.3.0 (c (n "termcolor") (v "1.3.0") (d (list (d (n "winapi-util") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0r1byqaq6f97qd0wc7k09lza190apvj9301s2afsp6m6gp9vm4v0")))

(define-public crate-termcolor-1.4.0 (c (n "termcolor") (v "1.4.0") (d (list (d (n "winapi-util") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0jfllflbxxffghlq6gx4csv0bv0qv77943dcx01h9zssy39w66zz")))

(define-public crate-termcolor-1.4.1 (c (n "termcolor") (v "1.4.1") (d (list (d (n "winapi-util") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0mappjh3fj3p2nmrg4y7qv94rchwi9mzmgmfflr8p2awdj7lyy86")))

