(define-module (crates-io te rm termination_attrib) #:use-module (crates-io))

(define-public crate-termination_attrib-0.1.0 (c (n "termination_attrib") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ldrnzd65036jgbkvb2zngilghf4356x5nw0pz75cs6l3j23ay9w")))

(define-public crate-termination_attrib-0.1.1 (c (n "termination_attrib") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kzl4nvqjpirh955rg1sb2cnwmsv5x9k4v6j19ayrpqckhfmz2mn")))

