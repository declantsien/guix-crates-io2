(define-module (crates-io te rm termfest) #:use-module (crates-io))

(define-public crate-termfest-0.1.0 (c (n "termfest") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)) (d (n "signal-notify") (r "^0.1.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1agn7i4n3zph0301fgwpis90f60q22h73s27hxly63hjj66zcqbi")))

(define-public crate-termfest-0.1.1 (c (n "termfest") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)) (d (n "signal-notify") (r "^0.1.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0jr53861l5vvnra1xggr18bmp03z4ai27dj3n2gkgrmrwzspz0ad")))

(define-public crate-termfest-0.1.2 (c (n "termfest") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)) (d (n "signal-notify") (r "^0.1.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1cyahmn4kbayz8sydlf1a2wkngl7qc2dchfqjfwa21abj5kcdc55")))

(define-public crate-termfest-0.1.3 (c (n "termfest") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)) (d (n "signal-notify") (r "^0.1.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "057ycxbahrhj5wkvffx7mhlhqh6qsbwapxk4hijzd94ni1wznbdz")))

(define-public crate-termfest-0.2.0 (c (n "termfest") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)) (d (n "signal-notify") (r "^0.1.3") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1nqn8b2l9njfmsgiafca0a3xgqnmsjh5jbwa83ix5976kjrrz5vr")))

