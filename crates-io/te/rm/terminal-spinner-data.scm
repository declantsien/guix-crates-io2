(define-module (crates-io te rm terminal-spinner-data) #:use-module (crates-io))

(define-public crate-terminal-spinner-data-0.1.0 (c (n "terminal-spinner-data") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 1)) (d (n "heck") (r "^0.3.2") (d #t) (k 1)) (d (n "quote") (r "^1.0.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 1)))) (h "0vq9704fa74m9b2ki1xrn1kw4lchza5vdl71s0f1lcvl8c12b1fy") (y #t)))

(define-public crate-terminal-spinner-data-0.1.1 (c (n "terminal-spinner-data") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 1)) (d (n "heck") (r "^0.3.2") (d #t) (k 1)) (d (n "quote") (r "^1.0.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 1)))) (h "1qfz5g9m2f8plfa9j3skgkg1mwj25vwc7g0b2g2ybkh9s3n3habl")))

(define-public crate-terminal-spinner-data-0.1.2 (c (n "terminal-spinner-data") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 1)) (d (n "heck") (r "^0.3.2") (d #t) (k 1)) (d (n "quote") (r "^1.0.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 1)))) (h "1x7kgrl0nln963am8wjxnfwh2lh6l5kx3kpr3z2nb64hpjfczb9s")))

(define-public crate-terminal-spinner-data-0.1.3 (c (n "terminal-spinner-data") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 1)) (d (n "heck") (r "^0.3.2") (d #t) (k 1)) (d (n "quote") (r "^1.0.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 1)))) (h "1xdfc6gxyl61fnys4ga6blrm5n1wh009xvj5ajb8kiibiwbbd86m") (y #t)))

