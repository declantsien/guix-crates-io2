(define-module (crates-io te rm termize) #:use-module (crates-io))

(define-public crate-termize-0.1.0 (c (n "termize") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("handleapi" "processenv" "wincon" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xhmza0jcczjyy2k2gwrgfl9i2bnijg6502kdz73p59c5rgwdm36")))

(define-public crate-termize-0.1.1 (c (n "termize") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("handleapi" "processenv" "wincon" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "158j22glm4cw6b2whs8fxpvqwk5132qydxzmj9qcw8s3armvw1hp")))

