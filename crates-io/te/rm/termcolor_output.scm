(define-module (crates-io te rm termcolor_output) #:use-module (crates-io))

(define-public crate-termcolor_output-1.0.0-rc.1 (c (n "termcolor_output") (v "1.0.0-rc.1") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "termcolor_output_impl") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "03mqxclaqh9j8dwiym8j07wqm74ydnfppvj2cps7v42glyrl3iwp")))

(define-public crate-termcolor_output-1.0.0-rc.2 (c (n "termcolor_output") (v "1.0.0-rc.2") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "termcolor_output_impl") (r "^1.0.0-rc.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1nbqvhppk18gaslm7w7ymmpqmbgwmz1g8i6qyn2znh0f5f08zx1l")))

(define-public crate-termcolor_output-1.0.0-rc.3 (c (n "termcolor_output") (v "1.0.0-rc.3") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "termcolor_output_impl") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1s01nj099zxj761xvpmvslx89blmv50vbdsk4dmjwkr2wckf1ghk")))

(define-public crate-termcolor_output-1.0.0 (c (n "termcolor_output") (v "1.0.0") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "termcolor_output_impl") (r "^1.0.0") (d #t) (k 0)))) (h "1rlfy9j9zxgvh3r3v8cbps4zxp10slavqwc54x1623z7ch6n5gmn") (y #t)))

(define-public crate-termcolor_output-1.0.1 (c (n "termcolor_output") (v "1.0.1") (d (list (d (n "batch_run") (r "^1.0.0") (d #t) (k 2)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)) (d (n "termcolor_output_impl") (r "^1.0.0") (d #t) (k 0)))) (h "1zwm50305hzchm9zvxj9iw7gkal00j072fy0d4xaa3lr42zsyqq3")))

