(define-module (crates-io te rm termbox-sys) #:use-module (crates-io))

(define-public crate-termbox-sys-0.0.1 (c (n "termbox-sys") (v "0.0.1") (h "125d9hkf4w423jzsry7wi719yzxq14h85r8n5wyk73b47ifvqp7q")))

(define-public crate-termbox-sys-0.0.2 (c (n "termbox-sys") (v "0.0.2") (h "02sv9blwfgkc4ljfrwzal7f0l51af6x8v269c9isrp8a1vkncr2q")))

(define-public crate-termbox-sys-0.1.0 (c (n "termbox-sys") (v "0.1.0") (h "1w6linfl84cvxwyslcw1ai6167k18m7b2pppllabg8hdz7d0cz9l")))

(define-public crate-termbox-sys-0.1.1 (c (n "termbox-sys") (v "0.1.1") (h "0wlmi6a1b3f6yza5a45cri522n14bjip5014zh70siscq0321qvy")))

(define-public crate-termbox-sys-0.1.2 (c (n "termbox-sys") (v "0.1.2") (h "1glri1in8qqhh942xgg72yczjkp9mdclppf51h82hjgzg00ypwq7")))

(define-public crate-termbox-sys-0.1.3 (c (n "termbox-sys") (v "0.1.3") (h "0w1l2qlbyndw03y5zlz9my0nyj34z3gd36qhjp3qx111539lqvq6")))

(define-public crate-termbox-sys-0.1.4 (c (n "termbox-sys") (v "0.1.4") (h "06byh05v6x583lpjsrdi2va2p0i42gfmj1asx593rjw17zbdpk9r")))

(define-public crate-termbox-sys-0.1.5 (c (n "termbox-sys") (v "0.1.5") (h "165bn5n492j8qk9vrxrmjvxhnmw47hh0nnx2vwcmj71a6sh8z7k4")))

(define-public crate-termbox-sys-0.1.6 (c (n "termbox-sys") (v "0.1.6") (h "0fnxhrs5ijaj2jqds75sp1nal3r8yj1rfbrw2hbv79yq656j198j")))

(define-public crate-termbox-sys-0.1.7 (c (n "termbox-sys") (v "0.1.7") (h "00hgidxzw11g6mzvzrjr2gslxh5khac3z8ffy8zvi863y8bim6y3")))

(define-public crate-termbox-sys-0.1.8 (c (n "termbox-sys") (v "0.1.8") (h "1xb04jsiycv6kx6cy4b4wvmjls0444xln2r1war6wa23zy4498bk")))

(define-public crate-termbox-sys-0.1.9 (c (n "termbox-sys") (v "0.1.9") (h "0f973gmcd2z06ccj0m34335i3pv2ignjf420092vyfqclb2v28l0")))

(define-public crate-termbox-sys-0.2.0 (c (n "termbox-sys") (v "0.2.0") (h "0jhax6c49g24mfgiqbkfc2dric7qw2x7bi9lxwmraz7qwqqrjhvf")))

(define-public crate-termbox-sys-0.2.1 (c (n "termbox-sys") (v "0.2.1") (h "0hhmyhrdizxpp2c3swbfqc10xs4y2bfz80m77kvfvzmalzk1h0x6")))

(define-public crate-termbox-sys-0.2.2 (c (n "termbox-sys") (v "0.2.2") (h "0s6hdwvscvjw3w8xyxwi6vw6ca82amnxfslz44fz2i508fqx0gi2")))

(define-public crate-termbox-sys-0.2.3 (c (n "termbox-sys") (v "0.2.3") (h "0wqykps0jr227830dw956yf2wyh3xjgz2awzsggihgwsrb9h1ih7")))

(define-public crate-termbox-sys-0.2.4 (c (n "termbox-sys") (v "0.2.4") (h "1gpbigxppr1qw1p7zw7pwzdxr1mx2ib1kr0ks3yclwrpq6rw1s81")))

(define-public crate-termbox-sys-0.2.5 (c (n "termbox-sys") (v "0.2.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0pf72mqrc539fff8fwhglw0z8hdagbfvsdhy13s96jq22iayf51q")))

(define-public crate-termbox-sys-0.2.6 (c (n "termbox-sys") (v "0.2.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1zmgqncx13i43xx9664zcycmpyk3wxxjx5f9qldjgg2r9vkrrh87")))

(define-public crate-termbox-sys-0.2.7 (c (n "termbox-sys") (v "0.2.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1f8hx3nkwraqhcrk5cv7fqyn4sx99csc6cbjrvjbvyjxxf7bs3np")))

(define-public crate-termbox-sys-0.2.8 (c (n "termbox-sys") (v "0.2.8") (h "1wj33h9n83qf7c4pxazl1x94r3iw2w4awr52w9apxc6rd2nh1ai9")))

(define-public crate-termbox-sys-0.2.9 (c (n "termbox-sys") (v "0.2.9") (h "0dkwvd232f2ykfx3qyc0gf3wpbkm0nr1rc9337brigq2plzd9n8h")))

(define-public crate-termbox-sys-0.2.10 (c (n "termbox-sys") (v "0.2.10") (h "0q473diim3b83cw1ils8mhciq97vvrn2v5z46vpv590yi0kslbg3")))

(define-public crate-termbox-sys-0.2.11 (c (n "termbox-sys") (v "0.2.11") (h "1vj2py7y021v2cm004x2mb171kf13mq8hnb7nachyz0kl9xmkgxg") (l "termbox")))

(define-public crate-termbox-sys-0.2.12 (c (n "termbox-sys") (v "0.2.12") (h "0qbydvrck1vvzk48ck9vy70m58ksisl9dj24imjq04lp4kmh0l32") (l "termbox")))

