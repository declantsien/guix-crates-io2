(define-module (crates-io te rm terminal-midi-monitor) #:use-module (crates-io))

(define-public crate-terminal-midi-monitor-0.1.0 (c (n "terminal-midi-monitor") (v "0.1.0") (d (list (d (n "alsa") (r "^0.2.1") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "095cxfx847dgbwmddcy7dc689yvd4dnd7b4k4bfxdn8j4wz62iyp")))

(define-public crate-terminal-midi-monitor-0.2.0 (c (n "terminal-midi-monitor") (v "0.2.0") (d (list (d (n "alsa") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ny058yc994gwwbg3zlmxrmykb05w8h2j2rs8gqqa16djh96cd6y")))

(define-public crate-terminal-midi-monitor-0.2.1 (c (n "terminal-midi-monitor") (v "0.2.1") (d (list (d (n "alsa") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qp2zndpsm1rjc4pikzq2c2ybskgkmhj4symh34qv93gan1k2d2y")))

(define-public crate-terminal-midi-monitor-0.3.0 (c (n "terminal-midi-monitor") (v "0.3.0") (d (list (d (n "alsa") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qi0mcahxcmv0ksza0gmmngy7ldppr4mr99r6bb1nbgp4anpkjga")))

