(define-module (crates-io te rm terminable_threads) #:use-module (crates-io))

(define-public crate-terminable_threads-0.1.0 (c (n "terminable_threads") (v "0.1.0") (h "0j6mhjfn5317l57418c3kyrzn9pb7q2q1h3yczz39hx2j3bf1584")))

(define-public crate-terminable_threads-0.2.0 (c (n "terminable_threads") (v "0.2.0") (h "04y0nag055nq0j3lx2h6x9plxvqxaq55lq5haknysfnzrccm1mbf")))

(define-public crate-terminable_threads-0.2.1 (c (n "terminable_threads") (v "0.2.1") (h "0jsxvsd17ms731821kwrbcqnvzxfhivsmayvlg5463al0rj3p0d0")))

