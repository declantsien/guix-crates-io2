(define-module (crates-io te rm termbox2-rs) #:use-module (crates-io))

(define-public crate-termbox2-rs-0.1.0 (c (n "termbox2-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1145q1rqa7xl65529mkqgvi8ki3w597g9r1zj8qgg7szxp3ns16d")))

