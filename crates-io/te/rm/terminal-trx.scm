(define-module (crates-io te rm terminal-trx) #:use-module (crates-io))

(define-public crate-terminal-trx-0.1.0 (c (n "terminal-trx") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_System_Console" "Win32_Storage_FileSystem" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0jpczmi1qgqq2vydcald2qsir9azawlm6hxgi1j3xpg1dic6jm66") (f (quote (("__test_unsupported")))) (r "1.70.0")))

(define-public crate-terminal-trx-0.2.0 (c (n "terminal-trx") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.152") (d #t) (t "cfg(unix)") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_System_Console" "Win32_Storage_FileSystem" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "044jfszg2ih3m472v8p8c1q3n00knizjz08c29gbvm827z4zfjhs") (f (quote (("__test_unsupported")))) (r "1.70.0")))

