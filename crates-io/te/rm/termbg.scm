(define-module (crates-io te rm termbg) #:use-module (crates-io))

(define-public crate-termbg-0.1.0 (c (n "termbg") (v "0.1.0") (d (list (d (n "termios") (r "^0.3") (d #t) (k 0)) (d (n "timeout-readwrite") (r "^0.3") (d #t) (k 0)))) (h "0zb8a9l893gvmwi6gv9mc1z2y9pyihh1iza2276ch8ggi8mq4b3c")))

(define-public crate-termbg-0.2.0 (c (n "termbg") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1q70v5bij9py08y4zj3mqk3bh36mg1yjnvdlddhi0sks1gs0jg0k")))

(define-public crate-termbg-0.2.1 (c (n "termbg") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "03j3s5yy2himj88ardmsyi424r9fcklln9iv53rm0p0x6sa4al2x")))

(define-public crate-termbg-0.2.2 (c (n "termbg") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "17512zgvm81g4mn78snbwwcijs9sjysanb2fzxk0hbzmj7iddzfx")))

(define-public crate-termbg-0.2.3 (c (n "termbg") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "10j2sqdp40qh2h4kkh5bdkp6jkmbpw4phdhp4n85qawxhxzaczvy")))

(define-public crate-termbg-0.2.4 (c (n "termbg") (v "0.2.4") (d (list (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1samkg9qhg9bh4m4zwkvlv934hf5jbxbbwszc6rr256zvy1zprfx")))

(define-public crate-termbg-0.3.0 (c (n "termbg") (v "0.3.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0wp3q8ysz3r6af4zn5jv15l67gsq9sjzxaxlp3mi5cf051fba1nv")))

(define-public crate-termbg-0.4.0 (c (n "termbg") (v "0.4.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0wrnjlqx909lidv1bp33d4awfq1kqy1p1jkqiyshjk6iph77drpc")))

(define-public crate-termbg-0.4.1 (c (n "termbg") (v "0.4.1") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0jkl9kckl3a0p7fi4jsv69mc6v16kfkysljd86i1kl844dannnzb")))

(define-public crate-termbg-0.4.2 (c (n "termbg") (v "0.4.2") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0l2qrnqrkfjnz49916arb7a52l12w27yzmpiws1inlh5sr7rv5b6")))

(define-public crate-termbg-0.4.3 (c (n "termbg") (v "0.4.3") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1lnb5nkhbql1fvki58zpx6kzmxfqk55rzwvhl59p8yyr0q1pk3xg")))

(define-public crate-termbg-0.4.4 (c (n "termbg") (v "0.4.4") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1r9c2fki6kq7fb2a4mkrxfrxqxiy8gkryhh28sv9ws705i36wh1p")))

(define-public crate-termbg-0.5.0 (c (n "termbg") (v "0.5.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "winbase" "processenv" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1pidjkz38z13iz8xchrz1nidxny5zjmxvbl1fs4arilvpzhfc4ic")))

