(define-module (crates-io te rm terminal-supports-emoji) #:use-module (crates-io))

(define-public crate-terminal-supports-emoji-0.1.0 (c (n "terminal-supports-emoji") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0d1wgqf1j79nqly9rl88n47zjkl1v4cklbkb36f6ciy5xx1r7hwz") (y #t)))

(define-public crate-terminal-supports-emoji-0.1.1 (c (n "terminal-supports-emoji") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jrzbfdsn96zqipyz2jan7b9d039jwlh4812bdpwgr5vzg43k28k") (y #t)))

(define-public crate-terminal-supports-emoji-0.1.2 (c (n "terminal-supports-emoji") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15qg03dzfsk0v8ks1fkbkcjnn7fjyx2dpil616z692psg6ria38l") (y #t)))

(define-public crate-terminal-supports-emoji-0.1.3 (c (n "terminal-supports-emoji") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sy607lwnf77mr4z2b45j90cgc894dr3lrhhvkz6qa1d3xx3m1y8")))

