(define-module (crates-io te rm terminal-menu) #:use-module (crates-io))

(define-public crate-terminal-menu-0.1.0 (c (n "terminal-menu") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "1mdkf3lanpcd7x3hz877giswmz0awn03rd8rm5m20s2hx7xyrafc") (y #t)))

(define-public crate-terminal-menu-0.1.1 (c (n "terminal-menu") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "02wfz52jhk3hbyz9xbq1ik28zfnl71z0d6zjzldfgs6x2d46k3z7") (y #t)))

(define-public crate-terminal-menu-1.0.0 (c (n "terminal-menu") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "14k3s276drarl0nls8xk3cmpdf4bpwwx5d787yib876v83f2w15r") (y #t)))

(define-public crate-terminal-menu-1.0.1 (c (n "terminal-menu") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "0fln0551mvxxjj5zm63lrapzfvdjc0gjfb72llw9y6rhfayg7fyk") (y #t)))

(define-public crate-terminal-menu-1.0.2 (c (n "terminal-menu") (v "1.0.2") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "1pkl2h1a1xc0fr4xiy1j7mav9rgscsbjwbcgn8rqj38mxyk6y13j") (y #t)))

(define-public crate-terminal-menu-1.0.3 (c (n "terminal-menu") (v "1.0.3") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "0pqk4dq9vb27nz7l1abn67pc9bm74n7yc28bxbpcqckhwds61iiq") (y #t)))

(define-public crate-terminal-menu-1.0.4 (c (n "terminal-menu") (v "1.0.4") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "0by53k73zfiyw9z2hg8jy6iklcs0xvsrcj481kdifi99wa09wpcp") (y #t)))

(define-public crate-terminal-menu-1.0.5 (c (n "terminal-menu") (v "1.0.5") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "0mnygljnflp347kgajjaidsggik4fwbif93zg14d1npzp0z94y4w") (y #t)))

(define-public crate-terminal-menu-1.0.6 (c (n "terminal-menu") (v "1.0.6") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "1y2khx2h3llqgy7gq7jcbn5n16gv3ylrj7sfwb4039dap444in8v") (y #t)))

(define-public crate-terminal-menu-1.0.7 (c (n "terminal-menu") (v "1.0.7") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "0lmgkdmkz5ddd5d41c0lq652xi1jnyr31d9pqxf1vdba3a4riw1c") (y #t)))

(define-public crate-terminal-menu-1.0.8 (c (n "terminal-menu") (v "1.0.8") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "0xwagl8ac646952zirfv0nma2v11vaxvw5a5cnb0zd2v5mv3jlz4") (y #t)))

(define-public crate-terminal-menu-1.1.0 (c (n "terminal-menu") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.13.3") (d #t) (k 0)))) (h "0y6rm5rlxw6x0s9qw0lrcl7vyng7dg9jdhbccwcnm7j3hnxcs2gb") (y #t)))

(define-public crate-terminal-menu-1.9.7 (c (n "terminal-menu") (v "1.9.7") (d (list (d (n "crossterm") (r "^0.17.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "137cvhxc724pxkz5jqrdqm6mikdb2bh07g9zmgginsxmkp17wd20") (y #t)))

(define-public crate-terminal-menu-2.0.0 (c (n "terminal-menu") (v "2.0.0") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0nznx1q5vxihdbmkr2rfl2n125yqjzx1wsrwlp1800bjd4i3n3w1") (y #t)))

(define-public crate-terminal-menu-2.0.1 (c (n "terminal-menu") (v "2.0.1") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1vg4vnylrvm25y61srihhh6j3swrp8dlrik9ibr7ka3vvf1jca6y") (y #t)))

(define-public crate-terminal-menu-2.0.2 (c (n "terminal-menu") (v "2.0.2") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0xhpp8svbjdi16pz49831gza5lbshz7z9r17jb0lpnxyf2racyh7") (y #t)))

(define-public crate-terminal-menu-2.0.3 (c (n "terminal-menu") (v "2.0.3") (d (list (d (n "crossterm") (r "0.23.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zmfvsn5vq9d42pbmikb5aywryh16x3xzrh3jr2dpv6ci7c07zdg") (y #t)))

(define-public crate-terminal-menu-2.0.4 (c (n "terminal-menu") (v "2.0.4") (d (list (d (n "crossterm") (r "0.23.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1wkr7qmwhhp0dzkjz81c4dmf4ldy8n4y7xrg2ah0z6hlwkxsn61f") (y #t)))

(define-public crate-terminal-menu-2.0.5 (c (n "terminal-menu") (v "2.0.5") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1rfp3cdfr0m2jipzkhpp524f2lap3i5770q8r6372q467fh5lkyz") (y #t)))

(define-public crate-terminal-menu-2.0.6 (c (n "terminal-menu") (v "2.0.6") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gr0sx759fycpniy5g25bbzvkwz4rhzrg6lhpa7zc3crdgsnbvij")))

(define-public crate-terminal-menu-3.0.0 (c (n "terminal-menu") (v "3.0.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "12mwlk29bkdsrg7wai18xms5wal7pvw3mjy77dv2bnz3kkkvzrv6")))

