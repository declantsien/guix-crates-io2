(define-module (crates-io te rm termrs) #:use-module (crates-io))

(define-public crate-termrs-0.1.0 (c (n "termrs") (v "0.1.0") (d (list (d (n "core") (r "^0.1.0") (d #t) (k 0) (p "termrs_core")) (d (n "runtime") (r "^0.1.0") (d #t) (k 0) (p "termrs_runtime")))) (h "1b2gd0bpd7b8hhvcmqn6iijlkm042rvm79kwhg775f3bxg3q7rls") (f (quote (("x11" "core/x11" "runtime/x11") ("wayland" "core/wayland" "runtime/wayland"))))))

(define-public crate-termrs-0.2.0 (c (n "termrs") (v "0.2.0") (d (list (d (n "core") (r "^0.2.0") (d #t) (k 0) (p "termrs_core")) (d (n "runtime") (r "^0.2.0") (d #t) (k 0) (p "termrs_runtime")))) (h "0pilqh8mafgf6lvsz6mifh4kizx3imyh3xjiwwl14rjgwdljvhdi") (f (quote (("x11" "core/x11" "runtime/x11") ("wayland" "core/wayland" "runtime/wayland"))))))

(define-public crate-termrs-0.3.0 (c (n "termrs") (v "0.3.0") (d (list (d (n "core") (r "^0.3.0") (d #t) (k 0) (p "termrs_core")) (d (n "runtime") (r "^0.3.0") (d #t) (k 0) (p "termrs_runtime")))) (h "1jjbisn528j2jrr64hmq4sdzkbwflxknhxyhbjsaq541j6x4bjsy") (f (quote (("x11" "core/x11" "runtime/x11") ("wayland" "core/wayland" "runtime/wayland"))))))

