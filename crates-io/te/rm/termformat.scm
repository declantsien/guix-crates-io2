(define-module (crates-io te rm termformat) #:use-module (crates-io))

(define-public crate-termformat-0.0.1 (c (n "termformat") (v "0.0.1") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 0)))) (h "0vgdd152ijga1ff8kx9aaxnkw1cvq9brk95x32p2f8h8zglf5rpq")))

