(define-module (crates-io te rm term) #:use-module (crates-io))

(define-public crate-term-0.1.0 (c (n "term") (v "0.1.0") (d (list (d (n "log") (r "^0.1.0") (d #t) (k 0)))) (h "1pw80cmkfy57dy1ppsmm5p1wpbp68yhgqsmcf4g90dvqc2sc72dg")))

(define-public crate-term-0.1.1 (c (n "term") (v "0.1.1") (d (list (d (n "log") (r "^0.1.0") (d #t) (k 0)))) (h "1lygfv533d676bym31n72asrjy91difgk8kdbm3z22jfhr52qsi8")))

(define-public crate-term-0.1.2 (c (n "term") (v "0.1.2") (d (list (d (n "log") (r "^0.1.0") (d #t) (k 0)))) (h "0b22swcwzzm2b8cha9yr4vn2rh4mn09lydlr5pjvr0vqzdamaqqz")))

(define-public crate-term-0.1.3 (c (n "term") (v "0.1.3") (d (list (d (n "log") (r "^0.1.0") (d #t) (k 0)))) (h "1hfq4ivmz9p2k1a7gy9b2y5df19qkas3z1r5r89dj8v19mmlczbi")))

(define-public crate-term-0.1.4 (c (n "term") (v "0.1.4") (d (list (d (n "log") (r "^0.1.0") (d #t) (k 0)))) (h "00pgn54g1rxdha6r4inz8j35awixbs560jnfi6z5k11lq1cqkbam")))

(define-public crate-term-0.1.5 (c (n "term") (v "0.1.5") (d (list (d (n "log") (r "^0.1.0") (d #t) (k 0)))) (h "00xsqw7n506bp3d1sj0bdhxm90k77ziczv38laqn3jdby6wa4a0y")))

(define-public crate-term-0.1.6 (c (n "term") (v "0.1.6") (d (list (d (n "log") (r "^0.1.0") (d #t) (k 0)))) (h "0fgq6n9dwxdywfh0k3m44a1vw9b44ivc28xgnymdkjys760v15kp")))

(define-public crate-term-0.1.7 (c (n "term") (v "0.1.7") (d (list (d (n "log") (r "^0.1.0") (d #t) (k 0)))) (h "0yahw3gxg28fx4zpsmspb4cdr5fafx8nyq3lgwq1kf8c2qzp8x2p")))

(define-public crate-term-0.1.8 (c (n "term") (v "0.1.8") (d (list (d (n "log") (r "^0.1.0") (d #t) (k 0)))) (h "0lwpwa183c3ic901dhc1cfmx7bpdxhx1w7h14nvmsdighj55wikl")))

(define-public crate-term-0.1.9 (c (n "term") (v "0.1.9") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "log") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "1l45ihpv1whf0criz4wa56y3igmz2n26h47sqkdw0giz2r2adp0p")))

(define-public crate-term-0.1.10 (c (n "term") (v "0.1.10") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "log") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "19b856s9ikrlmf7q0h0csdmfq4kpmqkpc9dpxkijq3jix1qja26r")))

(define-public crate-term-0.1.11 (c (n "term") (v "0.1.11") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "log") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "1k27k4jhphzdkbvcszhfr267vhsx47acdkvim3z7g0jzdpxq586s")))

(define-public crate-term-0.1.12 (c (n "term") (v "0.1.12") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "log") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "0vamv326z0cj1swrixa7sxzx3g4rp81xz2052m5vlwwrxzha6iv2")))

(define-public crate-term-0.1.13 (c (n "term") (v "0.1.13") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "log") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "1pn08n8ik7gc315ss86lvq787cmff98mhm8gh14kilvlqc8lbffd")))

(define-public crate-term-0.2.0 (c (n "term") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "132xfpnfa0fmhkyw5di9j168b69w7jnnz154m0882vy78p3xyyan")))

(define-public crate-term-0.2.1 (c (n "term") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "142x4lr8dgyr8xjy2gml252vzvlm3pifkfxhn0bbjgslz44dg6ii")))

(define-public crate-term-0.2.2 (c (n "term") (v "0.2.2") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "1zmb59m2cz6xklac2098ajiwfzgy57fr7njsl9hbyzjn3ilv1rsv")))

(define-public crate-term-0.2.3 (c (n "term") (v "0.2.3") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "07vm34dkyls9l0gh0ih9qpac1y0n344p12ryra7yffmcmgfgr6i5")))

(define-public crate-term-0.2.4 (c (n "term") (v "0.2.4") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "0mqrry1nvan95mjlc8czgci119rcpwh58gq823azjvzl6mwk3d9k")))

(define-public crate-term-0.2.5 (c (n "term") (v "0.2.5") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "13cj9yh6mxr110nry0fy9h3ck92m471wxp662v4chlnyr5d3gigq")))

(define-public crate-term-0.2.6 (c (n "term") (v "0.2.6") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "1gy4ng9lhd3b7kjvk898xcdpd686pyksl333g973ihzbmx6xp7xq")))

(define-public crate-term-0.2.7 (c (n "term") (v "0.2.7") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "18i2pajps5xbzaa6zv2npyasiv3z06i4s7rkf6sskq1g6iz32y7m")))

(define-public crate-term-0.2.8 (c (n "term") (v "0.2.8") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.1") (d #t) (k 0)))) (h "0gywl0nxi728bqx6a9d5mbg7l4dsn4qg6a2ymniacvc6cjky8xrm")))

(define-public crate-term-0.2.9 (c (n "term") (v "0.2.9") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "034lwdqdifzan5hjff5s5qsxc7y7jngbzynws225q0gcy5q8z1xd")))

(define-public crate-term-0.2.10 (c (n "term") (v "0.2.10") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.1") (d #t) (k 0)))) (h "00k75h8b9h7x62sdaj6pvsja3zaccs0661j9acfp4pff8z72rxig")))

(define-public crate-term-0.2.11 (c (n "term") (v "0.2.11") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0mjfsdhdikda41cniby50hy0fisz468wghjim9gzyg8hiwi2wgjs")))

(define-public crate-term-0.2.12 (c (n "term") (v "0.2.12") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1pk3r3jigqr1n28wgmi2mhycc25jr6vf8gspmih9b8nvn4bb5mph")))

(define-public crate-term-0.2.13 (c (n "term") (v "0.2.13") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "12cj3000k1gsx5ywkkial78ibqwx0578hmj3157p1y17gm8q0ikx")))

(define-public crate-term-0.2.14 (c (n "term") (v "0.2.14") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "109jmzddq1kz6wm2ndgddy7yrlqcw2i36ygxl0fcymc0sda7w1zj")))

(define-public crate-term-0.4.0 (c (n "term") (v "0.4.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1m9sqqzyymz759n7xgmc513af640kg01jlfgznwjskl3np8hkkz7")))

(define-public crate-term-0.4.1 (c (n "term") (v "0.4.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0hqvx7z71dnnqm17va8ch91a7nxhyqpf99vv391l6shx8i7sxf9v")))

(define-public crate-term-0.4.2 (c (n "term") (v "0.4.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0mqqgxfagv0jfbzm9npsxjbgsnvssc51am2pw0vwbzachxm8nws0")))

(define-public crate-term-0.4.3 (c (n "term") (v "0.4.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0m4z0pxcyj96v90fiv4sajw1g43rm74bg17b4dang2mnbp09bcyr") (y #t)))

(define-public crate-term-0.4.4 (c (n "term") (v "0.4.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0jpr7jb1xidadh0arklwr99r8w1k1dfc4an3ginpsq5nnfigivrx")))

(define-public crate-term-0.4.5 (c (n "term") (v "0.4.5") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "12frpf6mdin235d7aw4lsrrxips7kmbm098k8picysdk60wsys6i") (f (quote (("default"))))))

(define-public crate-term-0.4.6 (c (n "term") (v "0.4.6") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1wbh8ngqkqr3f6wz902yplf60bd5yapnckvrkgmzp5nffi7n8qzs") (f (quote (("default"))))))

(define-public crate-term-0.5.0 (c (n "term") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "handleapi" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0brfi4isf8imsrf2l9745wsp62sg8nz4j7ks3jm9rsd0rk9g5si6") (f (quote (("default"))))))

(define-public crate-term-0.5.1 (c (n "term") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "handleapi" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qbmqd8jbjlqr4608qdmvp6yin5ypifzi5s2xyhlw8g8s5ynfssy") (f (quote (("default"))))))

(define-public crate-term-0.5.2 (c (n "term") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "wincon" "handleapi" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0hkgjrfisj6zjwz525639pmsvzhlc48a0h65nw87qrdp6jihdlgd") (f (quote (("default"))))))

(define-public crate-term-0.6.0 (c (n "term") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "wincon" "handleapi" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0r0426y51sf3hwhzmhk8czrv706lh73k02sjsci48sh0sl2hbn8d") (f (quote (("default"))))))

(define-public crate-term-0.6.1 (c (n "term") (v "0.6.1") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "wincon" "handleapi" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ddqxq9hrk8zqq1f8pqhz72vrlfc8vh2xcza2gb623z78lrkm1n0") (f (quote (("default"))))))

(define-public crate-term-0.7.0 (c (n "term") (v "0.7.0") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "wincon" "handleapi" "fileapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07xzxmg7dbhlirpyfq09v7cfb9gxn0077sqqvszgjvyrjnngi7f5") (f (quote (("default"))))))

