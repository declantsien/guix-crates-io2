(define-module (crates-io te rm termprogress) #:use-module (crates-io))

(define-public crate-termprogress-0.1.0 (c (n "termprogress") (v "0.1.0") (d (list (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1mjfi1phcmp8mdyjp0f589s4ylc8j11wyxbhk2yq3rdvf9vpx98d")))

(define-public crate-termprogress-0.1.1 (c (n "termprogress") (v "0.1.1") (d (list (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1697g2jhzc0v1a2dykb1892di6grlcf99kxfga2cdjbx6gbk4az2")))

(define-public crate-termprogress-0.1.2 (c (n "termprogress") (v "0.1.2") (d (list (d (n "terminal_size") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0m6rhbpk2806d7vjcvbywz3yr5c9gp35zhc4zkhxg3wggzjdvk9k") (f (quote (("size" "terminal_size") ("default" "size"))))))

(define-public crate-termprogress-0.2.2 (c (n "termprogress") (v "0.2.2") (d (list (d (n "terminal_size") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1vwdr4d393929n5hw2liczjqyxv6zdxcfnnam01kgyjz3zcs8hzm") (f (quote (("size" "terminal_size") ("default" "size"))))))

(define-public crate-termprogress-0.2.4 (c (n "termprogress") (v "0.2.4") (d (list (d (n "terminal_size") (r "^0.1") (o #t) (d #t) (k 0)))) (h "19hsqp54whkbryb4cwf68b6dmw17wb52ysc3hnqqk22y7b7lmb5x") (f (quote (("size" "terminal_size") ("default" "size"))))))

(define-public crate-termprogress-0.3.4 (c (n "termprogress") (v "0.3.4") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "terminal_size") (r "^0.1") (o #t) (d #t) (k 0)))) (h "057bw0mcmix9c0gvsjxfjzph0p7i30cxm5319pvv1zfyzvvyq4b6") (f (quote (("size" "terminal_size") ("default" "size"))))))

(define-public crate-termprogress-0.10.0 (c (n "termprogress") (v "0.10.0") (d (list (d (n "atomic_refcell") (r "^0.1.10") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "stackalloc") (r "^1.2.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0692zpp53sn5fyny0qbbd2wv7sy6hy2gx5s8ks35zdvbv4lkhxm7") (f (quote (("size" "terminal_size") ("reactive") ("default" "size"))))))

