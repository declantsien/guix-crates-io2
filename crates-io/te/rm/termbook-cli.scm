(define-module (crates-io te rm termbook-cli) #:use-module (crates-io))

(define-public crate-termbook-cli-1.0.0 (c (n "termbook-cli") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "termbook") (r "^1.0.0") (d #t) (k 0)))) (h "0yzb2589r0aq81a0zbs1p41vl6wh9rw9nimxa550d3fpcmscx09b")))

(define-public crate-termbook-cli-1.1.0 (c (n "termbook-cli") (v "1.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "termbook") (r "^1.1.0") (d #t) (k 0)))) (h "02r4znjgkkaq2l8a9cjbk6vfswvc468fpx70zdyi01px2amphv1r")))

(define-public crate-termbook-cli-1.2.0 (c (n "termbook-cli") (v "1.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "termbook") (r "^1.1.0") (d #t) (k 0)))) (h "1y9swsd4szzw8jzwf07ir6wyps1n1h9iqd9f679lbmcvhs5w0xql")))

(define-public crate-termbook-cli-1.2.1 (c (n "termbook-cli") (v "1.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "termbook") (r "^1.2.0") (d #t) (k 0)))) (h "1z40fxbc25pzxf23bp343g1x9il3hmmyfvgcbj3gw1pxpgayh6fc")))

(define-public crate-termbook-cli-1.2.2 (c (n "termbook-cli") (v "1.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "termbook") (r "^1.2.0") (d #t) (k 0)))) (h "03zs2hpqxq8s4g1nw06p6h8xqshcp6llp973yb6ncmjp3drnjwhw")))

(define-public crate-termbook-cli-1.2.4 (c (n "termbook-cli") (v "1.2.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "termbook") (r "^1.2.4") (d #t) (k 0)))) (h "11i71l2wcpnyb38j083cmyb2z2mpf49clffyykqj32p82mcrh3sk")))

(define-public crate-termbook-cli-1.3.0 (c (n "termbook-cli") (v "1.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "termbook") (r "^1.3.0") (d #t) (k 0)))) (h "1wi0w3j9naz69dc7cq6rql6g56lx3qd0d02w3l4zjqm986zcwznb")))

(define-public crate-termbook-cli-1.4.0 (c (n "termbook-cli") (v "1.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "termbook") (r "^1.4.0") (d #t) (k 0)))) (h "16ngpg73f82vzm91ri30xy3xhf4qpvj56k1dnni7w6j4666dy1vq")))

(define-public crate-termbook-cli-1.4.2 (c (n "termbook-cli") (v "1.4.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1.2.4") (d #t) (k 0)) (d (n "termbook") (r "^1.4.2") (d #t) (k 0)))) (h "10l6rs3mhdvkzhma9n66g0xjld8qnghlh5lq9w0zl0826dry2q8r")))

(define-public crate-termbook-cli-1.4.3 (c (n "termbook-cli") (v "1.4.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1.2.4") (d #t) (k 0)) (d (n "termbook") (r "^1.4.2") (d #t) (k 0)))) (h "09yxy2hxwlvf1k4am398n1g2hnkwvfhwzjd6xbsrvzqv4fw6wh2v")))

(define-public crate-termbook-cli-1.4.4 (c (n "termbook-cli") (v "1.4.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1.2.4") (d #t) (k 0)) (d (n "termbook") (r "^1.4.2") (d #t) (k 0)))) (h "0ydssp07gmdd7am8a9ql0m0aqny5g00d0pp1c02qlmfb90n7hqvs")))

(define-public crate-termbook-cli-1.4.5 (c (n "termbook-cli") (v "1.4.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1.2.4") (d #t) (k 0)) (d (n "termbook") (r "^1.4.2") (d #t) (k 0)))) (h "1cdq7a0y4dnmhl6bqvs465mc32alig69x6mwi70kqy0bxmv3hl5h")))

(define-public crate-termbook-cli-1.4.6 (c (n "termbook-cli") (v "1.4.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^4.0.0") (d #t) (k 0)) (d (n "termbook") (r "^1.4.2") (d #t) (k 0)))) (h "0nalispzpcnywh39fy0h6xgz06nyzdhpgm9wd8bb26z0cgkhrgwc")))

