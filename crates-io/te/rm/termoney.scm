(define-module (crates-io te rm termoney) #:use-module (crates-io))

(define-public crate-termoney-0.2.1 (c (n "termoney") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "term") (r "^0.7.0") (d #t) (k 0)))) (h "1qpn0ml5kzkr5dj873nbym6y622r8bdzphycidysn03a79ijkv84")))

(define-public crate-termoney-0.2.2 (c (n "termoney") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "term") (r "^0.7.0") (d #t) (k 0)))) (h "1pk4apvs8ypgrn36z3jx2p08h9b48slrff6niwlrcriigqagfgf7")))

(define-public crate-termoney-0.2.3 (c (n "termoney") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "term") (r "^0.7.0") (d #t) (k 0)))) (h "04ydsd0aivrlx1xj7dw70zz2srq3cd13qqg9xaspw9jmqazih68n")))

