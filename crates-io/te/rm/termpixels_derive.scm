(define-module (crates-io te rm termpixels_derive) #:use-module (crates-io))

(define-public crate-termpixels_derive-0.1.0 (c (n "termpixels_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f76yd515xfw5qjyjnprx7q0nvvarznrvabfb7aj8f55gs51iddk")))

(define-public crate-termpixels_derive-0.4.0 (c (n "termpixels_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mq9w21v6mmzwahnacgm9wbsbwjyfwkvinsr48c3mqixahid71hg")))

(define-public crate-termpixels_derive-0.4.1 (c (n "termpixels_derive") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f4406pynxy4lb5g5zhhxk06q7d06prd7j1s7rvwg6hwlvqjjbx9")))

(define-public crate-termpixels_derive-0.5.0 (c (n "termpixels_derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1azhai37634rsafxxqd2wnxjbw812s88cf23pa2fc47f68g4m1a8")))

