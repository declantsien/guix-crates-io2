(define-module (crates-io te rm termix) #:use-module (crates-io))

(define-public crate-termix-0.0.1 (c (n "termix") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "indent") (r "^0.1.1") (d #t) (k 2)))) (h "053ddhy2zalwadlw6vhljm13r6mjy8nh6gvyqhf5sczz58sf29r1") (y #t)))

(define-public crate-termix-0.0.2 (c (n "termix") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "indent") (r "^0.1.1") (d #t) (k 2)))) (h "1pqalgbbp6qjgxfl7c26pba0imppy44yms32ah2x7lzsc03jcwza")))

(define-public crate-termix-0.0.3 (c (n "termix") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "indent") (r "^0.1.1") (d #t) (k 2)))) (h "128lw7imwzvjdbjpq11di900z793639f5fgb73gf6arg05dpq33c")))

