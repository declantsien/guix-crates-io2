(define-module (crates-io te rm termite) #:use-module (crates-io))

(define-public crate-termite-0.1.0-alpha (c (n "termite") (v "0.1.0-alpha") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "async-channel") (r "^1.7") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "event-listener") (r "^2.5") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "hyena") (r "^0.2") (d #t) (k 0)) (d (n "termite-macro") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "1fp9plckqik43z684lfyd313j35mgl9cg9j77dicrxcc0zhh7v99")))

