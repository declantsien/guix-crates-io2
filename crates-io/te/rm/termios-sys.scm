(define-module (crates-io te rm termios-sys) #:use-module (crates-io))

(define-public crate-termios-sys-0.0.1 (c (n "termios-sys") (v "0.0.1") (h "0szycpgr9x820cm99y7ag6xi33p2bzbcj9hrsql4ddq8vrk3myzb")))

(define-public crate-termios-sys-0.0.2 (c (n "termios-sys") (v "0.0.2") (h "1r7qgx62k7ni0md634sa8xy9ra3v5ym8kqqzp83yc1a2qqv5sh05")))

(define-public crate-termios-sys-0.0.3 (c (n "termios-sys") (v "0.0.3") (h "1sdh8w3nybad9yn6mnqxifgknv2vm0imhw3v7g98br61w6nqrz59")))

(define-public crate-termios-sys-0.0.4 (c (n "termios-sys") (v "0.0.4") (h "090ag3njb1hwvs5vsim3rz5n9qg1lvy19gxy1b72ajvlxyw3k8v8")))

(define-public crate-termios-sys-0.0.5 (c (n "termios-sys") (v "0.0.5") (h "0raq4ma8fkkvzmnxs5n48ls0h0liinrikglm2v94j6vc7k2vgr9m")))

(define-public crate-termios-sys-0.0.6 (c (n "termios-sys") (v "0.0.6") (h "13g8yd4pykcw1kn747qcyqfi0vnw12n018jk5w80pw724a0xbgn7")))

