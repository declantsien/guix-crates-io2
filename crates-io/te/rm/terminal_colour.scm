(define-module (crates-io te rm terminal_colour) #:use-module (crates-io))

(define-public crate-terminal_colour-0.1.0 (c (n "terminal_colour") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1azd1vl7g042ylrvws7wnjgf2gs86yshs7h38q4vzn5c4kn8zq24")))

(define-public crate-terminal_colour-0.2.0 (c (n "terminal_colour") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "01rrq7fr464yx2vpxyxcm7sbr70da4zmrq3j1rmzlwga97jz60i6")))

