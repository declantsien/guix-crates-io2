(define-module (crates-io te rm terms) #:use-module (crates-io))

(define-public crate-terms-0.1.0 (c (n "terms") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "166rnglw0qcdj32i20cvz0y3f72mncylwvjwbn94f5c0c14nin90")))

(define-public crate-terms-0.1.1 (c (n "terms") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0g40j82qrrjj8cnj43dm2rbm7x6f494hmvmsdqa71zf9bf91dp6r")))

(define-public crate-terms-0.1.2 (c (n "terms") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "029z5wjmfkp23rlb3bjfizfn68i2qsysby40w9kqalqvj8y43mvs")))

(define-public crate-terms-0.1.3 (c (n "terms") (v "0.1.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0l733wdzhpkd12m1drdqaalxvwn16v01bgbsidzd1x8c5rk32psm")))

