(define-module (crates-io te rm termui) #:use-module (crates-io))

(define-public crate-termui-0.1.0 (c (n "termui") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0kq8i45nylc1jmxghqdzfmaxfgjaybjdhq7kfjv0mipx3v9gadyj") (f (quote (("test_utils" "proptest") ("default")))) (r "1.64")))

