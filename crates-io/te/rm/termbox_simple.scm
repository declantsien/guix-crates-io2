(define-module (crates-io te rm termbox_simple) #:use-module (crates-io))

(define-public crate-termbox_simple-0.1.0 (c (n "termbox_simple") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)))) (h "1hd94xr305a052dmrdvjiqaj4sc2g5hgkw8m8mhhqvfha6377gcz")))

(define-public crate-termbox_simple-0.2.0 (c (n "termbox_simple") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)))) (h "10jc03lk2sffsvyqpw14s8jm4xfw05q6jg8w1492nds19v9ljb42")))

(define-public crate-termbox_simple-0.2.1 (c (n "termbox_simple") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)) (d (n "term_input") (r "^0.1.3") (d #t) (k 2)))) (h "13mfkkinxh2nzzngrpmd4y9544qxbzivr2i08jdwhvf125lmz2zx")))

(define-public crate-termbox_simple-0.2.2 (c (n "termbox_simple") (v "0.2.2") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)) (d (n "term_input") (r "^0.1.3") (d #t) (k 2)))) (h "1pkx3045y0ijzf7vwf9z2h00l5m49zyfiij7kyv1cz9n2ipis083") (l "termbox_simple")))

(define-public crate-termbox_simple-0.2.3 (c (n "termbox_simple") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.38") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6.9") (d #t) (k 2)) (d (n "term_input") (r "^0.1.3") (d #t) (k 2)))) (h "08rcdvrls23nkk9525rmpvags3d5z2a7pxyghn5qcmrc062dywb7") (l "termbox_simple")))

