(define-module (crates-io te rm terminal-emulator) #:use-module (crates-io))

(define-public crate-terminal-emulator-0.1.0 (c (n "terminal-emulator") (v "0.1.0") (d (list (d (n "arraydeque") (r "^0.4.3") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "vte") (r "^0.3.3") (d #t) (k 0)))) (h "0h9294jpm3rpnf7vbc19bbvx4ld4ssyibv5yfxhj3wrp10h5rfwa")))

