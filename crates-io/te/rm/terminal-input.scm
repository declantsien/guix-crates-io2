(define-module (crates-io te rm terminal-input) #:use-module (crates-io))

(define-public crate-terminal-input-0.1.0 (c (n "terminal-input") (v "0.1.0") (d (list (d (n "const-cstr") (r "^0.3.0") (d #t) (k 0)) (d (n "ncurses") (r "^5.99.0") (d #t) (k 0)))) (h "008jamm38xbzvgwkwgmgmiih5llfj8xrvi6zlqjcmc6crf9y0904") (f (quote (("ncurses-ext") ("default" "ncurses-ext"))))))

