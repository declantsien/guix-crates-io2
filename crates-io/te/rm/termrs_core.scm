(define-module (crates-io te rm termrs_core) #:use-module (crates-io))

(define-public crate-termrs_core-0.1.0 (c (n "termrs_core") (v "0.1.0") (d (list (d (n "copyrs") (r "^0.4.2") (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1203zf47m325lhi27bkq18qlvh4qbmjg0qk40h49p3mshb2d8shf") (f (quote (("x11" "copyrs/x11") ("wayland" "copyrs/wayland") ("default" "x11"))))))

(define-public crate-termrs_core-0.2.0 (c (n "termrs_core") (v "0.2.0") (d (list (d (n "copyrs") (r "^0.4.2") (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1kpzy18pw8c8sa5f5l7wldx95g4amqsjm392yfq4dwgidhq6wxjh") (f (quote (("x11" "copyrs/x11") ("wayland" "copyrs/wayland") ("default" "x11"))))))

(define-public crate-termrs_core-0.3.0 (c (n "termrs_core") (v "0.3.0") (d (list (d (n "copyrs") (r "^0.4.2") (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1fir1dzzd971hx0vbjh0rjcyr81m8ak2csr0di2ifldakyi3svhi") (f (quote (("x11" "copyrs/x11") ("wayland" "copyrs/wayland") ("default" "x11"))))))

