(define-module (crates-io te rm termine) #:use-module (crates-io))

(define-public crate-termine-0.1.0 (c (n "termine") (v "0.1.0") (d (list (d (n "termioff") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1flmrm2wls9ykrqc7941ilpz87w4f6ywy5rdh8pf0cdaancqgsjj") (y #t)))

(define-public crate-termine-0.1.1 (c (n "termine") (v "0.1.1") (d (list (d (n "termioff") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "08qi7yai8qzx2pm9lldmwac9rjnwkb404scid3gyrrfwh3kpb7a7") (y #t)))

(define-public crate-termine-0.1.2 (c (n "termine") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termioff") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0agkha9vxpaxaswpclql9r4h9971f2568ga3983rz1xg7dz9m7lm") (y #t)))

(define-public crate-termine-1.0.0 (c (n "termine") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termioff") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1nizifaz0k5ix4wvs25gnxqr18d2c7g50m7bz9mkb5i396sphn17") (y #t)))

(define-public crate-termine-1.1.0 (c (n "termine") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termioff") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0y1kgdcp7vjm35g56f6dqz9v5dzc4xk2kdllqf7k96lrx51xrkqh") (y #t)))

(define-public crate-termine-3.0.0 (c (n "termine") (v "3.0.0") (d (list (d (n "minefield") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termioff") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "139ih0sd6xpbflfpjpsr8f6qc9x5c3hdallq2aimhmk5yjxm1icr") (y #t)))

(define-public crate-termine-3.1.0 (c (n "termine") (v "3.1.0") (d (list (d (n "minefield") (r "^3.1") (d #t) (k 0)) (d (n "mvc-rs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termioff") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "12d1ggj177ibx7dg90ap13xfgbw2l18pnvqyj3f6739s0l6khd2q") (y #t)))

(define-public crate-termine-3.2.0 (c (n "termine") (v "3.2.0") (d (list (d (n "minefield") (r "^3.2") (d #t) (k 0)) (d (n "mvc-rs") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termioff") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0ffhii5iv5wcrn17kgq32x6r73ccih39b52d9b60qrf6zhr77jjy") (y #t)))

(define-public crate-termine-3.3.0 (c (n "termine") (v "3.3.0") (d (list (d (n "minefield") (r "^3.3") (d #t) (k 0)) (d (n "mvc-rs") (r "^3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termioff") (r "^0.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0yk45d3pmcb5nw6aq9l1a94brnymcf8ang0iyqdqvvrkg16rvz4x")))

