(define-module (crates-io te rm term_fb) #:use-module (crates-io))

(define-public crate-term_fb-0.0.1 (c (n "term_fb") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ryu") (r "^0.2") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "00x40js7cmh6lcrc5ldzggiq9piisz2gksc58fxl1ndh02ypv0sr")))

