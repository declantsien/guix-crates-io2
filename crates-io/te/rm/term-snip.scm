(define-module (crates-io te rm term-snip) #:use-module (crates-io))

(define-public crate-term-snip-0.1.0 (c (n "term-snip") (v "0.1.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)))) (h "1j9gbx1r4qr7zy8c5qcbccqpk1jpdmxl4d7lkm2ah7gha4ffibvd")))

(define-public crate-term-snip-0.1.1 (c (n "term-snip") (v "0.1.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)))) (h "1ia3169kmw16fhcawdafnkdpc4pfw2ajb5hhm5v3kxhfszkyn89a")))

(define-public crate-term-snip-0.1.2 (c (n "term-snip") (v "0.1.2") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)))) (h "0bzyn58cl85f0whc0y65naxpm7gwbxnf12h6269wjbpnsajgczhm")))

(define-public crate-term-snip-0.1.3 (c (n "term-snip") (v "0.1.3") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)))) (h "17pgravqy0wz74ani1jf78nmr9bmlk6vcvcn4dbdlw41igrncwih")))

(define-public crate-term-snip-0.1.4 (c (n "term-snip") (v "0.1.4") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)))) (h "03rikyf6fc2lsk6l3nm9jfnn3limv18yvp4sf1hfi0jlkgilp1kd")))

(define-public crate-term-snip-0.1.5 (c (n "term-snip") (v "0.1.5") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)))) (h "1g64r9xmspf23vzp9xw4kzkf9pl2l6pypd68hncxijbxf9pjs5sf")))

(define-public crate-term-snip-0.1.6 (c (n "term-snip") (v "0.1.6") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "mockall") (r "^0.9.1") (d #t) (k 2)))) (h "1v420alvj5adjbdwf4bccx5rvs5y1v5rd9agf1g0j2x2wgrggl34")))

(define-public crate-term-snip-0.1.7 (c (n "term-snip") (v "0.1.7") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 2)))) (h "1wxwsw7qgx2sv9xrbv42gvklv5dl56sr2ha8xrychvh57aqzyys9")))

(define-public crate-term-snip-0.1.8 (c (n "term-snip") (v "0.1.8") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 2)))) (h "0cz00k153l39362lyqnbrd70lhlki48pfzpykbjsgw70ykqjx4b1")))

