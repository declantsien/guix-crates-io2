(define-module (crates-io te rm terminal_cli_termion) #:use-module (crates-io))

(define-public crate-terminal_cli_termion-0.1.0 (c (n "terminal_cli_termion") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(unix)") (k 0)) (d (n "terminal_cli") (r "^0.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "04w1wqxmbfvapd37hcg7n4a82b5ixcv9r80qhkrs7p868qd7ib04")))

