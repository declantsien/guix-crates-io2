(define-module (crates-io te rm termatrix) #:use-module (crates-io))

(define-public crate-termatrix-0.2.0 (c (n "termatrix") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.14") (d #t) (k 0)) (d (n "cursive") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "06n5bwx2yzcdq9gf45v29c31l63nda0hmd5wwhazdrn8lvyixnpk")))

(define-public crate-termatrix-0.2.1 (c (n "termatrix") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.14") (d #t) (k 0)) (d (n "cursive") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "19mb3j3z25zkyprwaqljn2a6dbdy1vrmmmp96saa2vx05xywcqfp")))

(define-public crate-termatrix-1.0.0 (c (n "termatrix") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.14") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "cursive") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1vj69sjyvlp3q8rqfcmspq3nvxlsmhgbkry247yz2sz4rpkljp3v")))

(define-public crate-termatrix-1.0.1 (c (n "termatrix") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.14") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "cursive") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1hl38x8qa28hbd8czrj8prf9xyh8lh5cazsdb713rh27dc1xinif")))

(define-public crate-termatrix-1.0.2 (c (n "termatrix") (v "1.0.2") (d (list (d (n "clap") (r "^3.1.14") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "cursive") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1p9k59y8zhk5afi0hfg3isv35dmd0ccpi7axq2hnmx807gby9m18")))

(define-public crate-termatrix-2.0.0 (c (n "termatrix") (v "2.0.0") (d (list (d (n "clap") (r "^3.1.14") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "cursive") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0pmg3shkp2azndrmnfzzx2djj6rdq4rg2wsk64iawkh266jqn3pv")))

(define-public crate-termatrix-2.0.1 (c (n "termatrix") (v "2.0.1") (d (list (d (n "clap") (r "^3.1.14") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "cursive") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1r2k755l7cc9n1wygizdmja4n9fg0g6wjlm56ls569xjf082y47f")))

(define-public crate-termatrix-2.1.0 (c (n "termatrix") (v "2.1.0") (d (list (d (n "clap") (r "^3.1.14") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "cursive") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1sly0r04a3pf96zwv66n09dhapvm2m6vglh249axmb45yx3v5b3a")))

(define-public crate-termatrix-2.2.0 (c (n "termatrix") (v "2.2.0") (d (list (d (n "clap") (r "^3.1.14") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "cursive") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "00v0ays5cv30c2gacxp290g7rf7wfsg500wpipm27i58vj4hh2aq")))

