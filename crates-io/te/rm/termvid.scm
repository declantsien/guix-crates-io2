(define-module (crates-io te rm termvid) #:use-module (crates-io))

(define-public crate-termvid-0.3.0 (c (n "termvid") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (f (quote ("ansi-parsing"))) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)) (d (n "y4m") (r "^0.7.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1czjak9z5n0kq7crb0s0zi0xv6brkzrs4bayr2n6hkxda61f5p5r")))

(define-public crate-termvid-0.3.1 (c (n "termvid") (v "0.3.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (f (quote ("ansi-parsing"))) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)) (d (n "y4m") (r "^0.7.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "00d9656mazlyfk90ribwgcdh13wbnqzwrw4cbzksbxh25kzvpz3y")))

(define-public crate-termvid-0.3.2 (c (n "termvid") (v "0.3.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (f (quote ("ansi-parsing"))) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)) (d (n "y4m") (r "^0.7.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1365akp8q4z3rxhwr880d53khpqlsxzaagm568jwwg8m71nzbhdx")))

(define-public crate-termvid-0.4.0 (c (n "termvid") (v "0.4.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (f (quote ("ansi-parsing"))) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (f (quote ("symphonia-wav"))) (k 0)) (d (n "unix-named-pipe") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("fast-rng" "v4"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)) (d (n "y4m") (r "^0.7.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1p7fhryw27q8x6i6fy7928wry7najyay3sg2bfi90n6dx77s2lsi")))

