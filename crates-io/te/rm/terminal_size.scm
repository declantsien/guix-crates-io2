(define-module (crates-io te rm terminal_size) #:use-module (crates-io))

(define-public crate-terminal_size-0.1.0 (c (n "terminal_size") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "15nxj0dakl85fmyr9y5pbamq1lccvhpinaj5kcmgzgq9j9dcw4rl")))

(define-public crate-terminal_size-0.1.1 (c (n "terminal_size") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "07mhjza3rhqvh3vbma7r4aq6d8nxhpvgz15vxfm56yyhdw6jh05m")))

(define-public crate-terminal_size-0.1.4 (c (n "terminal_size") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1sgnvzk7xxz0h9l5lv62gjljyfhqjymm59fy55kg5hxngzzz6h31")))

(define-public crate-terminal_size-0.1.5 (c (n "terminal_size") (v "0.1.5") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "11c0s7jqxchvzrxrwdhrav6xvqcxn3x5dnh0swy029pflvnsz008")))

(define-public crate-terminal_size-0.1.6 (c (n "terminal_size") (v "0.1.6") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1k4lqqk0wzzxcnj8hnv515yrdam5cxsnji1favlz9hg7iw9ybz91")))

(define-public crate-terminal_size-0.1.7 (c (n "terminal_size") (v "0.1.7") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "03xfbf2qqzv7ig5ckzyr1hzbq00970wjb9yrc79k4c065bdpykzg")))

(define-public crate-terminal_size-0.1.8 (c (n "terminal_size") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt" "processenv" "wincon" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0czi3h6jfvq22y3syxpbv3ir0lm3591sbn8vfi4ridjhb39lacq2")))

(define-public crate-terminal_size-0.1.9 (c (n "terminal_size") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processenv" "winbase" "wincon" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0v9r47b22aly4ml9sij3knry9qvc098djbzygwq4s8pjr230k84r") (y #t)))

(define-public crate-terminal_size-0.1.10 (c (n "terminal_size") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processenv" "winbase" "wincon" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vlj1s63il3p00bj46l670jw4xwa6535zq2b86d05yad0bin0np2")))

(define-public crate-terminal_size-0.1.11 (c (n "terminal_size") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processenv" "winbase" "wincon" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xpbflqngr9swzm38sf24kmnfimrb7sjqjdhhl5wb5w67ywvr394")))

(define-public crate-terminal_size-0.1.12 (c (n "terminal_size") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processenv" "winbase" "wincon" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fyif1psbb4459ghinzp53lcj9mx67v4m5jb7wb53wx6qxgzjf40")))

(define-public crate-terminal_size-0.1.13 (c (n "terminal_size") (v "0.1.13") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processenv" "winbase" "wincon" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04qy9i0k3qkhl749xk30xga0l7w61rf4bj5zy0r44w3jijgws54s")))

(define-public crate-terminal_size-0.1.14 (c (n "terminal_size") (v "0.1.14") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processenv" "winbase" "wincon" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1sc759kgjckcnpwa241j9hsf670x7gldxjy7sdb319rxll8qla2y")))

(define-public crate-terminal_size-0.1.15 (c (n "terminal_size") (v "0.1.15") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processenv" "winbase" "wincon" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1qfb07d040wh3m4yfnbxknda7i5fr56ypnwdwdgmzb1zpn1x3ljb")))

(define-public crate-terminal_size-0.1.16 (c (n "terminal_size") (v "0.1.16") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processenv" "winbase" "wincon" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01i4zlv8dplx8ps328wl14xv7w1ah0ni6i3g0w1dnd07fpnqrjl6")))

(define-public crate-terminal_size-0.1.17 (c (n "terminal_size") (v "0.1.17") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processenv" "winbase" "wincon" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pq60ng1a7fjp597ifk1cqlz8fv9raz9xihddld1m1pfdia1lg33")))

(define-public crate-terminal_size-0.2.0 (c (n "terminal_size") (v "0.2.0") (d (list (d (n "rustix") (r "^0.35.6") (f (quote ("termios"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "14ldfpbwiblnc1jpd2dav3pr1dbiib9i5aiysqkzzz8l46yc8751")))

(define-public crate-terminal_size-0.2.1 (c (n "terminal_size") (v "0.2.1") (d (list (d (n "rustix") (r "^0.35.7") (f (quote ("termios"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "18f57ag083ckf460wyhp34jdh193rhxrh2ja9qbgdpkrrxhchh44")))

(define-public crate-terminal_size-0.2.2 (c (n "terminal_size") (v "0.2.2") (d (list (d (n "rustix") (r "^0.35.7") (f (quote ("termios"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0yhza8sc6jkka6j0nq5sl749ckx1jagvxp3b38yhh4px6k291jj0")))

(define-public crate-terminal_size-0.2.3 (c (n "terminal_size") (v "0.2.3") (d (list (d (n "rustix") (r "^0.36.3") (f (quote ("termios"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01qrl8apw7psj72ii4lrdqcvy73n4g8d5y4iskmrvdm2ifd0h86b")))

(define-public crate-terminal_size-0.2.4 (c (n "terminal_size") (v "0.2.4") (d (list (d (n "rustix") (r "^0.36.3") (f (quote ("termios"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17nag72dj2b607p9w542pj6c57f3wpgs5b7scxl07g6lz16pp5xz") (y #t)))

(define-public crate-terminal_size-0.2.5 (c (n "terminal_size") (v "0.2.5") (d (list (d (n "rustix") (r "^0.36.3") (f (quote ("termios"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fmlla5frpwd42m119n4g02wrcjaz4ryy03by04hj77c5kfzv6jc")))

(define-public crate-terminal_size-0.2.6 (c (n "terminal_size") (v "0.2.6") (d (list (d (n "rustix") (r "^0.37.0") (f (quote ("termios"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0drj7gb77kay5r1cv53ysq3g9g4f8n0jkhld0kadi3lzkvqzcswf")))

(define-public crate-terminal_size-0.3.0 (c (n "terminal_size") (v "0.3.0") (d (list (d (n "rustix") (r "^0.38.0") (f (quote ("termios"))) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xqdzdjq77smg41z67vg3qwrcilf1zf5330gdrgm22lyghmvzgi1")))

