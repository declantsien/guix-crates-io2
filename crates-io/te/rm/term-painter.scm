(define-module (crates-io te rm term-painter) #:use-module (crates-io))

(define-public crate-term-painter-0.1.0 (c (n "term-painter") (v "0.1.0") (d (list (d (n "term") (r "*") (d #t) (k 0)))) (h "0v9fd6zp3acvdrvvvm7jkigna8a5zp73h21zvqgb81q2ibywa3g6")))

(define-public crate-term-painter-0.1.1 (c (n "term-painter") (v "0.1.1") (d (list (d (n "term") (r "*") (d #t) (k 0)))) (h "119n95lpzsqr31xg5ap3gjy95n2vv9adsshx09rnpgppf1jmc3mw")))

(define-public crate-term-painter-0.2.0 (c (n "term-painter") (v "0.2.0") (d (list (d (n "term") (r "*") (d #t) (k 0)))) (h "0q4kk0v2a0kl5g164gpxrnknl7jn2dba3s6957s3irin18xfb4zq")))

(define-public crate-term-painter-0.2.1 (c (n "term-painter") (v "0.2.1") (d (list (d (n "term") (r "*") (d #t) (k 0)))) (h "070vnkrcjwmljddvlx92arxhb7h4b3wn8cy165w16pz1459c91dy")))

(define-public crate-term-painter-0.2.2 (c (n "term-painter") (v "0.2.2") (d (list (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "188as33blkkwadlv6zgcmafgzg9dc50j2bb0m6g9gvxsxql798dy")))

(define-public crate-term-painter-0.2.3 (c (n "term-painter") (v "0.2.3") (d (list (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0ss93s231jdj39wrhjdh2mvpgvq9zzw15z6l2cmr6xaiy3r0p45b")))

(define-public crate-term-painter-0.2.4 (c (n "term-painter") (v "0.2.4") (d (list (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1kq87jdjpgj570cbnxc0y91rwp57c7jwz3fwv064ff1y1s7r9anw")))

(define-public crate-term-painter-0.3.0 (c (n "term-painter") (v "0.3.0") (d (list (d (n "term") (r "^0.6") (d #t) (k 0)))) (h "1y0xfxxx69sgjvas56rhcdw1b245ipf0j4a2s4307jrcv3lh55s1")))

(define-public crate-term-painter-0.4.0 (c (n "term-painter") (v "0.4.0") (d (list (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "0x2jzjr8z7ibjc1kll177jkh2468pz21a71pc363j2jckn2bjqsw")))

