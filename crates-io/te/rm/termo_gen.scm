(define-module (crates-io te rm termo_gen) #:use-module (crates-io))

(define-public crate-termo_gen-1.0.3 (c (n "termo_gen") (v "1.0.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0x1d4ygqqqaks392v8av6k71yll4r012b625jgms8953ck1i9zm6")))

