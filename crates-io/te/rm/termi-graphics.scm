(define-module (crates-io te rm termi-graphics) #:use-module (crates-io))

(define-public crate-termi-graphics-0.1.5 (c (n "termi-graphics") (v "0.1.5") (h "1d1x2k450jxrfmh08a52392pm0i6fr9qlm0l7g9g70qjgdiyxrzm")))

(define-public crate-termi-graphics-0.1.6 (c (n "termi-graphics") (v "0.1.6") (h "1wvsyrh8cgqmj9pl0z1hrxclr15mk9l72f7szma6nk19c1k7s201")))

