(define-module (crates-io te rm term-inquiry) #:use-module (crates-io))

(define-public crate-term-inquiry-0.1.0 (c (n "term-inquiry") (v "0.1.0") (d (list (d (n "ansi-builder") (r "^0.1.3") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "1hl1pa1dvk2w21j3xbfim2763s8b6rhkqnngybqbnj4f4v3mdv5g")))

(define-public crate-term-inquiry-0.1.1 (c (n "term-inquiry") (v "0.1.1") (d (list (d (n "ansi-builder") (r "^0.1.6") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 1)))) (h "09fyazclwl432kn4xql4w549ixhg4zh3bnnq5zqgapgy37n54n5m")))

(define-public crate-term-inquiry-0.1.2 (c (n "term-inquiry") (v "0.1.2") (d (list (d (n "ansi-builder") (r "^0.1.6") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.10.0") (d #t) (t "cfg(windows)") (k 1)))) (h "012vp1xqdc0zqwgj72yxz9v8kgwwyyrc4gs2z58qz62j28cca4bk")))

