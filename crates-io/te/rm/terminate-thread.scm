(define-module (crates-io te rm terminate-thread) #:use-module (crates-io))

(define-public crate-terminate-thread-0.1.0 (c (n "terminate-thread") (v "0.1.0") (d (list (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)))) (h "1wbgnwxf3gsynqjb4h3d55fmal93qnybjgrq1awx6x0pqvv8jvdz") (y #t)))

(define-public crate-terminate-thread-0.1.1 (c (n "terminate-thread") (v "0.1.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "1xwvrbjb9y6m72d2bjxrgna66qp767sbjpry6qrxspagpz50sv07") (y #t)))

(define-public crate-terminate-thread-0.2.0 (c (n "terminate-thread") (v "0.2.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "1177xgpcjapkk5vgadz2x87ybcv55j5n6jkscv9bg87qivd7389q")))

(define-public crate-terminate-thread-0.3.0 (c (n "terminate-thread") (v "0.3.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "0g3wvkvpcv6ccmg76qlmzbsw0ggijcnvmmkmc07jc4jzc0mmsb4s") (y #t)))

(define-public crate-terminate-thread-0.3.1 (c (n "terminate-thread") (v "0.3.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "0n0c7rm4l78miamvpfhxj6ckxkqbcza2valpxapcqpqkkswsa0dl")))

