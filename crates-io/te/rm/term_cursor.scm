(define-module (crates-io te rm term_cursor) #:use-module (crates-io))

(define-public crate-term_cursor-0.1.0 (c (n "term_cursor") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0m6c31j7pgxqbr3dm22xb8px0zbk49m370gwxk6xkampdgsvpjhy")))

(define-public crate-term_cursor-0.1.1 (c (n "term_cursor") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1jd80g4pqs0cz92d8a7700ah4anlx31jbw0q7ha7l0qna8l46gqv")))

(define-public crate-term_cursor-0.1.2 (c (n "term_cursor") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.3.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "10r6gr9yabb2bcy8zn4csf72ljkbl7l77a2cjz5z101sc200kmfs")))

(define-public crate-term_cursor-0.1.3 (c (n "term_cursor") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.3.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "15bzxzpa9h28ias5i17wdwdv868a629gckzxbhc6hjacp72zdnys")))

(define-public crate-term_cursor-0.1.4 (c (n "term_cursor") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.3.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1sf8gpb8nc5ms2vsg1m9nc6dwym43rsxy5ivv1kvw2njgv55j2vm")))

(define-public crate-term_cursor-0.2.0 (c (n "term_cursor") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.3.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.4") (d #t) (t "cfg(windows)") (k 0)))) (h "0nfmgx78h300xkhds1ia41z2yi9k81sxhl5rr3fcizjs1cz433qa")))

(define-public crate-term_cursor-0.2.1 (c (n "term_cursor") (v "0.2.1") (d (list (d (n "termios") (r "^0.3.0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("minwindef" "winbase" "wincon" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0a9693l4662rn2q9y7dg17nbc39nvwwyvz36fc3slmmzp2py29g2")))

