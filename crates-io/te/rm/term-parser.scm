(define-module (crates-io te rm term-parser) #:use-module (crates-io))

(define-public crate-term-parser-0.1.0 (c (n "term-parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0kdj4zv6x2b8rlz4bwgiyzsx457vbhjairsrhys52a3hcps4590z") (y #t)))

(define-public crate-term-parser-0.1.1 (c (n "term-parser") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0h0hmjpw746b0csfir232zwmygqs3l9w07bwr93y9m6810llim8y") (y #t)))

(define-public crate-term-parser-0.2.0 (c (n "term-parser") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1g1cfp437p8801mg3a776fjz5dgysmxr5zz4s0v1disr81l3lj1w") (y #t)))

