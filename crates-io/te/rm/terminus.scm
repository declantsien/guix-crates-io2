(define-module (crates-io te rm terminus) #:use-module (crates-io))

(define-public crate-terminus-0.1.0 (c (n "terminus") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "elf_rs") (r "^0.1.3") (d #t) (k 0)) (d (n "r2pipe") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1bxlrr87l2iviczy6722d7d8kcg080g1vbxlgsrkqqkx0rsivdgm")))

(define-public crate-terminus-0.1.1 (c (n "terminus") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "elf_rs") (r "^0.1.3") (d #t) (k 0)) (d (n "r2pipe") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0x5f64w3p030n1j0i9hjli1xc47hxv06gfmjabw999kgvxjxgim6")))

(define-public crate-terminus-0.2.0 (c (n "terminus") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "elf_rs") (r "^0.1.3") (d #t) (k 0)) (d (n "r2pipe") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0fzck872asvjjqgb3pxwv6n7wqq2vqvi3lm3dy0m4818nm5mch1q")))

