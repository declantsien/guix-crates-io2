(define-module (crates-io te rm termutils) #:use-module (crates-io))

(define-public crate-termutils-0.1.0 (c (n "termutils") (v "0.1.0") (d (list (d (n "argrust") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "rustypath") (r "^0.1.1") (d #t) (k 0)))) (h "0zc69w21dq0msg2p0zmgklaqsz7n945973s6vzccqpzv9ywnv2qh")))

