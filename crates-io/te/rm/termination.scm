(define-module (crates-io te rm termination) #:use-module (crates-io))

(define-public crate-termination-0.1.0 (c (n "termination") (v "0.1.0") (d (list (d (n "termination_attrib") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1kh3vfd5025fdchyrrc0k1lh0z1vnhz62m2zx2ia8v00cqs1b0sk")))

(define-public crate-termination-0.1.1 (c (n "termination") (v "0.1.1") (d (list (d (n "termination_attrib") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1z6yifsfjx6d5njcyna5b89yz4ylpdgw0vm2kjvd895ffs5r198y")))

(define-public crate-termination-0.1.2 (c (n "termination") (v "0.1.2") (d (list (d (n "termination_attrib") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0i72as0rcxpi9xc1sfgmzrfg8sfwcn88xfsfklldzdmzp35yyqaa")))

