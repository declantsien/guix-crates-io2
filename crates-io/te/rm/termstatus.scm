(define-module (crates-io te rm termstatus) #:use-module (crates-io))

(define-public crate-termstatus-0.1.0 (c (n "termstatus") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0b7krl7dpf8dbbii93s8gm6fjs8p6ay38gqyxw4ddpgag8xq2mjh")))

(define-public crate-termstatus-0.1.1 (c (n "termstatus") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1j6xsqlacwj0ra74fbpkcx6pq2cpz3ds694s1m8kd8lzhqaj76v6")))

(define-public crate-termstatus-0.2.0 (c (n "termstatus") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "00mra0fyxp3y9fakkzj7wr9fn3w6r5zfxq98g51n2i256rxq4jkx")))

