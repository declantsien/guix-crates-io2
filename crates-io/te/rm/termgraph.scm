(define-module (crates-io te rm termgraph) #:use-module (crates-io))

(define-public crate-termgraph-0.1.0 (c (n "termgraph") (v "0.1.0") (h "1n4ak7fwxknwbcdwmwwdmmc5pwc3rh31anbmnnrs8pa3bngzbnbj")))

(define-public crate-termgraph-0.2.0 (c (n "termgraph") (v "0.2.0") (h "1nqwam9a3pbg77hqwbll2y6icsic0xwvavircf78mcabqw8x4qgv")))

(define-public crate-termgraph-0.3.0 (c (n "termgraph") (v "0.3.0") (h "0vp5rm0xys6sm14svi0laxwzz8h2fbxwv3zmdx1d8pw7b2ndsn6c")))

(define-public crate-termgraph-0.4.0 (c (n "termgraph") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0hpvwrm1hfvygicannzb6091h74ncwsy0wpp7d2wmdhqchzg4y4g")))

