(define-module (crates-io te rm termbuffer) #:use-module (crates-io))

(define-public crate-termbuffer-0.1.0 (c (n "termbuffer") (v "0.1.0") (d (list (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0h4456xqd8rizd9v77xy0pql247kzy5avsphkzxcslhpjb9w082k")))

(define-public crate-termbuffer-0.1.1 (c (n "termbuffer") (v "0.1.1") (d (list (d (n "termion") (r "^1") (d #t) (k 0)))) (h "19mir8kh47iaqpiifz7l6i9hnz70d194gjxgasnvy2bdxwsbchm7")))

