(define-module (crates-io te rm termal-term) #:use-module (crates-io))

(define-public crate-termal-term-0.1.0 (c (n "termal-term") (v "0.1.0") (d (list (d (n "arboard") (r "^3.3.2") (d #t) (k 0)) (d (n "font-kit") (r "^0.11") (d #t) (k 0)) (d (n "minifb") (r "^0.25.0") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("process" "term" "fs" "ioctl"))) (d #t) (k 0)) (d (n "raqote") (r "^0.8.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)) (d (n "utf8parse") (r "^0.2.1") (d #t) (k 0)) (d (n "vt100") (r "^0.15.2") (d #t) (k 0)) (d (n "vte") (r "^0.13.0") (d #t) (k 0)) (d (n "x11") (r "^2.21.0") (d #t) (k 0)))) (h "1nlhmgwcrpy7mlbws01ix386jm6xryxi1ga99vpnsycfcyz0q4n2") (f (quote (("default" "debugger") ("debugger"))))))

