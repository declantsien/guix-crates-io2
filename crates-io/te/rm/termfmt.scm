(define-module (crates-io te rm termfmt) #:use-module (crates-io))

(define-public crate-termfmt-0.1.0 (c (n "termfmt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "01cp5qkx7xf1jwk255rmkff4p16nv1lx4ljs75ay1d36j07am021") (f (quote (("command"))))))

(define-public crate-termfmt-0.1.1 (c (n "termfmt") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0lp8gfpxaxr0gv6l4s7q473l7fddqp7s2k3r2h2srsr7128karlr") (f (quote (("command"))))))

(define-public crate-termfmt-0.1.2 (c (n "termfmt") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0mf8vhqnp5wswkm67nbdxvlbkmvanwl1p4ag6b2zkrj7bqhjv3sm") (f (quote (("command"))))))

(define-public crate-termfmt-0.1.3 (c (n "termfmt") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0m7645rf6sphmaspkz2ys7m1inybinzgcjw70j31638q87adccfg") (f (quote (("command"))))))

(define-public crate-termfmt-0.1.4 (c (n "termfmt") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0k0nfnlbfpfd289qp6qs0r8qq9m4lj75x07zaz4mhlmliqlwr80c") (f (quote (("command"))))))

(define-public crate-termfmt-0.1.5 (c (n "termfmt") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0hfh8ryd8ccw8rgx3h04fcj4xz076bk2zcl18bgfii4abw0bcd0s") (f (quote (("command"))))))

(define-public crate-termfmt-0.1.6 (c (n "termfmt") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "12xbawbk46z705k6m4wpr3fkrs2v54ljqd0gwaj4r36shi10ii2j") (f (quote (("command"))))))

(define-public crate-termfmt-0.1.7 (c (n "termfmt") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0dk8bvzqnciliniw3x5v53r4fhk8c9xmzjv4238m81dqpcpl8pd3") (f (quote (("command"))))))

(define-public crate-termfmt-0.1.8 (c (n "termfmt") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "02dgj1j8pj2p763ijqnsmzjga7xlxgbhnin6aw2laahpk05wx2gz") (f (quote (("command"))))))

(define-public crate-termfmt-0.1.9 (c (n "termfmt") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "19g71p0h339al6fff42cj6f7zykgy2iwl3wf9xm1ddlvw4slx07s") (f (quote (("layout") ("command"))))))

(define-public crate-termfmt-0.2.0 (c (n "termfmt") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "13g67dan786ma8bnnma992f872scms2xhz4v5j1hj5hczsb60nkm")))

(define-public crate-termfmt-0.2.1 (c (n "termfmt") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "058b8y3jr6y17hk970l3w0sahcjikvawh1wypx25pgqr8pricjn1")))

