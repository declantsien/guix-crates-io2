(define-module (crates-io te rm terminalui) #:use-module (crates-io))

(define-public crate-TerminalUI-0.0.1 (c (n "TerminalUI") (v "0.0.1") (h "0aixlc1r7zilczflgyjb9h224zzyf563r90kfkmnslpw9qb3lpif") (y #t)))

(define-public crate-TerminalUI-0.1.0 (c (n "TerminalUI") (v "0.1.0") (h "0gxcyq0d9ij163ppvwmvv9zfz567hpc0dbayxymqps3012nr5022") (y #t)))

(define-public crate-TerminalUI-0.1.1 (c (n "TerminalUI") (v "0.1.1") (h "10h73j63z5jvp9m1w63v6mhvsvd6j27gg5xhbikah6b6jc5c10x8") (y #t)))

(define-public crate-TerminalUI-0.1.2 (c (n "TerminalUI") (v "0.1.2") (h "0njbgm0xck22pzp1na61k12hhsg2khdkm5rxvipayh4gdz4m03kn") (y #t)))

(define-public crate-TerminalUI-0.1.3 (c (n "TerminalUI") (v "0.1.3") (h "1p0yp86cc7nqj780jrqn55lriissx10m0sr0bp8428wla1h5yfz0") (y #t)))

(define-public crate-TerminalUI-0.1.5 (c (n "TerminalUI") (v "0.1.5") (h "0yaijxw91vsa1ccs3bjj4klxcidg0n2dzmq28mvb4pw0lxs0iqif") (y #t)))

