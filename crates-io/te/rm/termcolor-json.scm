(define-module (crates-io te rm termcolor-json) #:use-module (crates-io))

(define-public crate-termcolor-json-0.1.0 (c (n "termcolor-json") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0579mnmfb2zg4amfv8wgq6lnf42fv1i9lfdwg0bm31q2msfcz12y")))

(define-public crate-termcolor-json-0.1.1 (c (n "termcolor-json") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0jxn5m9sl5gwn0vk28rhllw3ram4fgamam17s74wj8s8d2hgbmx2")))

(define-public crate-termcolor-json-0.1.2 (c (n "termcolor-json") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1l8wmfbi6v7m8k5y2bicswj3841cx137gs7mfqi5dhakdi4hcjph")))

(define-public crate-termcolor-json-0.1.3 (c (n "termcolor-json") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0jgh7myfc1dl6q4q7b07ppqn79q05ikk7ji5qys3s02ls8c266kb")))

(define-public crate-termcolor-json-0.1.4 (c (n "termcolor-json") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.29") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "1hq1dlz4z4cv5zw8hfd8lwdii5qijw0q8zsdlcqcpmrh9gb5ma2k")))

(define-public crate-termcolor-json-1.0.0 (c (n "termcolor-json") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.29") (d #t) (k 0)) (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "085kb4s73v16rwkadqgbhq8a77cgcwrn9mnrnv818y9103h6f9y4")))

