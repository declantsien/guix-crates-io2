(define-module (crates-io te rm termongo) #:use-module (crates-io))

(define-public crate-termongo-0.1.0 (c (n "termongo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "mongodb") (r "^2.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.0") (d #t) (k 0)))) (h "17q4mr00lz011jninjkc92slald1r0z73kf93qgkkajdlflg31yw")))

(define-public crate-termongo-0.2.0 (c (n "termongo") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "mongodb") (r "^2.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.0") (d #t) (k 0)))) (h "0fh0d6y59ysxc487n62yg4qv4h99rjik9baaxwp5clbhvfp367yr")))

