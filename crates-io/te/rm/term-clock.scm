(define-module (crates-io te rm term-clock) #:use-module (crates-io))

(define-public crate-term-clock-0.1.0 (c (n "term-clock") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0m7qqkkhzy8jkidrdj599b2ddj76wykv4fl0a34cg9d0sgbikqh6")))

(define-public crate-term-clock-0.1.1 (c (n "term-clock") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "terminal") (r "^0.2.1") (d #t) (k 0)))) (h "0iz9hakqbnh7m5x68g81gv16jqng2k6yk2jpl1xiydwy1h8agrvw")))

(define-public crate-term-clock-0.1.2 (c (n "term-clock") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "terminal") (r "^0.2.1") (d #t) (k 0)))) (h "0hixvz2gnnfny7vch7c4qlc6fxhm8ydj2h3f1qp8wvxdam1blg9c")))

(define-public crate-term-clock-0.1.3 (c (n "term-clock") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "terminal") (r "^0.2.1") (d #t) (k 0)))) (h "1bmxj6s38my4rxy04k7bgy7zaxzyncbgznzs28hams0wn3q0696h")))

