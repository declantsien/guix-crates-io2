(define-module (crates-io te rm termini) #:use-module (crates-io))

(define-public crate-termini-0.1.0 (c (n "termini") (v "0.1.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "0n94qigdalh99wv467201ya4y4bhmgzwsbk271d23zw3jvcybra4")))

(define-public crate-termini-0.1.1 (c (n "termini") (v "0.1.1") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "18q13phcxb7bmmz5i2rxnkhrlx89dkmjh6hpsd7vva60i9i3swq0")))

(define-public crate-termini-0.1.2 (c (n "termini") (v "0.1.2") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "1yd2xyrwk51mbs6p1xbp1yck221nyp6ih188gw3yinpk3q16cirr")))

(define-public crate-termini-0.1.3 (c (n "termini") (v "0.1.3") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "1468lazq6gxx66hhhgqcbnzm7rgw6wq1lzhsvpmblk041dwrfms6")))

(define-public crate-termini-0.1.4 (c (n "termini") (v "0.1.4") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)))) (h "04lafqlvvcqa4ayrzlz84qip2hs0azyf8ix7hqk0sf1akk5pw3wc")))

(define-public crate-termini-1.0.0 (c (n "termini") (v "1.0.0") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0n8dvbwkp2k673xqwivb01iqg5ir91zgpwhwngpcb2yrgpc43m1a")))

