(define-module (crates-io te rm term_do) #:use-module (crates-io))

(define-public crate-term_do-0.5.0 (c (n "term_do") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1hkba9hd6853jz59kz75jam8f6nl3dvn1w3mgjav50hijhc10bya")))

(define-public crate-term_do-0.5.1 (c (n "term_do") (v "0.5.1") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "15hgvz21xik5w4gkx8lpfjvagfyxrlbipvm0bjmx6l1lq3p126rk") (y #t)))

(define-public crate-term_do-0.5.2 (c (n "term_do") (v "0.5.2") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0gglspc9wypmqrjfvzkhzgqwdlq95b7nls46m7dj6ppmydq3dyh2") (y #t)))

(define-public crate-term_do-0.5.3 (c (n "term_do") (v "0.5.3") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1fp44zblxflkzqwa8i4ibys6pwwq7s875jcmcdldxlv6wi65w4ar")))

(define-public crate-term_do-0.5.4 (c (n "term_do") (v "0.5.4") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "18n1f1bswzaamrppr7snhhrv09cvckq0ngc0d64170l1hffv2817")))

(define-public crate-term_do-0.6.0 (c (n "term_do") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "167sy5d9mbcib1k1pv9gkdkjbp5snn2c9n02z4z4yvdjd8i61w4z")))

(define-public crate-term_do-0.7.0 (c (n "term_do") (v "0.7.0") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "17nh55954px5lllvi4m778karmawii3qc5b3z0ql3ycm9yrnhn4p")))

