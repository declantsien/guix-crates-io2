(define-module (crates-io te rm termbf) #:use-module (crates-io))

(define-public crate-termbf-0.1.0 (c (n "termbf") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "itsuki") (r "^0.1.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (f (quote ("all-widgets" "unstable-widget-ref"))) (d #t) (k 0)) (d (n "tui-input") (r "^0.8.0") (d #t) (k 0)))) (h "1d4cc3qsysjraiwkjpnxx9l6lgh3zfrm8010c6byds59ns3d3fid")))

(define-public crate-termbf-0.1.1 (c (n "termbf") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "itsuki") (r "^0.1.2") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (f (quote ("all-widgets" "unstable-widget-ref"))) (d #t) (k 0)) (d (n "tui-input") (r "^0.8.0") (d #t) (k 0)))) (h "193p4g3mcxrcdwin7k3sf6m0nkv466f72vqqbj27k0wgcl0rsc36")))

