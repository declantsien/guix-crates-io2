(define-module (crates-io te rm termit-vt) #:use-module (crates-io))

(define-public crate-termit-vt-0.0.1 (c (n "termit-vt") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pseudoterm") (r "^0.1") (d #t) (k 0)) (d (n "termit-ui") (r "^0.0.1") (d #t) (k 0)) (d (n "vte") (r "^0.3.3") (d #t) (k 0)))) (h "0c1gdh1zyshfzb07hc7zyzm76da3993bank28sa0rrbssg8rxi9r") (y #t)))

