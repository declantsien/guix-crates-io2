(define-module (crates-io te rm termin-8) #:use-module (crates-io))

(define-public crate-termin-8-0.0.1 (c (n "termin-8") (v "0.0.1") (h "1mxghnzaygpwvlx36jvp2p76l81ag5mm7fm37nsg4k8qm5iav2nz")))

(define-public crate-termin-8-0.1.1 (c (n "termin-8") (v "0.1.1") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 0)) (d (n "deca") (r "^0.0.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17") (d #t) (k 0)))) (h "1wr62fq3czb7fjz46pd32pjxibrw3mj5ggl40i727rci2pdr50j1")))

(define-public crate-termin-8-0.1.2 (c (n "termin-8") (v "0.1.2") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 0)) (d (n "deca") (r "^0.0.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17") (d #t) (k 0)))) (h "1df5vgqk3z1yfqm6bligbxgbzg6r1zs4c1jg3g7dyih5zyrjmgjq")))

(define-public crate-termin-8-0.1.3 (c (n "termin-8") (v "0.1.3") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 0)) (d (n "deca") (r "^0.0.5") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17") (d #t) (k 0)))) (h "03wifbab2iylfy8vq2n10ksc54v77wrnw7l05zp5cdm3fn8j6n9b")))

(define-public crate-termin-8-0.1.4 (c (n "termin-8") (v "0.1.4") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 0)) (d (n "deca") (r "^0.0.7") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "octopt") (r "^0.1.3") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17") (d #t) (k 0)))) (h "10vz1d3wz8dbc26vy34jaxkkd0dc7adj9y6yh68g90yqbz8jhvb7")))

(define-public crate-termin-8-0.1.5 (c (n "termin-8") (v "0.1.5") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "deca") (r "^0.0.8") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "octopt") (r "^0.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "1n46d8zx69n71qf75k28mlzhh9phyhqhjhvrxxnv41zwsv3wvd62")))

(define-public crate-termin-8-0.1.6 (c (n "termin-8") (v "0.1.6") (d (list (d (n "ansi_colours") (r "^1.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "deca") (r "^0.0.9") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "drawille") (r "^0.3") (d #t) (k 0)) (d (n "octopt") (r "^0.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "184w8jx0rlibqm4ismbg8ndw97aa0ix3jlv7g94kh85bcwpilhib")))

(define-public crate-termin-8-0.1.7 (c (n "termin-8") (v "0.1.7") (d (list (d (n "ansi_colours") (r "^1.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "deca") (r "^0.0.10") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "drawille") (r "^0.3") (d #t) (k 0)) (d (n "octopt") (r "^1.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)))) (h "0xl6ggmxklrbimim8qmg4ir6rv6f7kmpczm9glhr9mr3kbzidbam")))

