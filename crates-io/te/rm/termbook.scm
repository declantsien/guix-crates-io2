(define-module (crates-io te rm termbook) #:use-module (crates-io))

(define-public crate-termbook-1.0.0 (c (n "termbook") (v "1.0.0") (d (list (d (n "mdbook") (r "^0.1.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)))) (h "0i8p1a62jg9ci42pqrm51czk4hala51xp77di65xdkvk1zr6b4ia")))

(define-public crate-termbook-1.1.0 (c (n "termbook") (v "1.1.0") (d (list (d (n "mdbook") (r "^0.1.2") (d #t) (k 0)) (d (n "mdcat") (r "^0.7.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^2.0.0") (d #t) (k 0)))) (h "0mz4kfpznalifz86p89haz03x6y65wi1yyzj23pkg4slvshwimbr")))

(define-public crate-termbook-1.2.0 (c (n "termbook") (v "1.2.0") (d (list (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "globset") (r "^0.3.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.1.2") (d #t) (k 0)) (d (n "mdcat") (r "^0.8.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^2.0.0") (d #t) (k 0)))) (h "0bpzn99g8shcm7qsk0ynyrglqvq9m6xbfy0fjhbc35k5lmys8mva")))

(define-public crate-termbook-1.2.4 (c (n "termbook") (v "1.2.4") (d (list (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "globset") (r "^0.3.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.1.2") (d #t) (k 0)) (d (n "mdcat") (r "^0.8.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^2.0.0") (d #t) (k 0)))) (h "1prjmsnhxpr80p03pnlrndm049sjng7ns31h8sh4x8yfmnd9sa91")))

(define-public crate-termbook-1.3.0 (c (n "termbook") (v "1.3.0") (d (list (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "globset") (r "^0.3.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.1.2") (d #t) (k 0)) (d (n "mdcat") (r "^0.8.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^2.0.0") (d #t) (k 0)))) (h "1wda8v72lzi1p9mn0qs8f0p9j16fs1bjpcwg11lhwxd05l5zndgg")))

(define-public crate-termbook-1.4.0 (c (n "termbook") (v "1.4.0") (d (list (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "globset") (r "^0.3.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.1.2") (d #t) (k 0)) (d (n "mdcat") (r "^0.8.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^2.0.0") (d #t) (k 0)))) (h "162kxld4xby9gsqv9hsgj68vrw7hy9d8n0bgkkqna66vrh19ihwy")))

(define-public crate-termbook-1.4.1 (c (n "termbook") (v "1.4.1") (d (list (d (n "atty") (r "^0.2.6") (d #t) (k 0)) (d (n "globset") (r "^0.3.0") (d #t) (k 0)) (d (n "mdbook") (r "^0.1.2") (d #t) (k 0)) (d (n "mdcat") (r "^0.8.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^1") (d #t) (k 0)) (d (n "syntect") (r "^2.0.0") (d #t) (k 0)))) (h "1w4fk1q7x6i3pl1q9xc58qxq3issq7ba34yc9s9b6irhsdpm5hnv")))

(define-public crate-termbook-1.4.2 (c (n "termbook") (v "1.4.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "globset") (r "^0.4.4") (d #t) (k 0)) (d (n "mdbook") (r "^0.3.5") (d #t) (k 0)) (d (n "mdcat") (r "^0.15.0") (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (d #t) (k 0)) (d (n "pulldown-cmark-to-cmark") (r "^2.0.1") (d #t) (k 0)) (d (n "syntect") (r "^3.1.0") (d #t) (k 0)))) (h "1ywpla7if7zm35apz3pqxlsjkr2hfqqfxnijwa3sv6gj9iadkwsz")))

