(define-module (crates-io te rm terminal-juice) #:use-module (crates-io))

(define-public crate-terminal-juice-0.1.0 (c (n "terminal-juice") (v "0.1.0") (h "1ikzv2i1mh4i5cd3h5jx0qdcw4jijibvncf3v6cyrvbxma1qyx5x")))

(define-public crate-terminal-juice-0.1.1 (c (n "terminal-juice") (v "0.1.1") (h "14cpd32mwqb0kvf01nfia1q1zxw9gkg28sf19p5lknajmgc86b4k")))

