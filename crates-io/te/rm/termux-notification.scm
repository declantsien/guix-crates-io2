(define-module (crates-io te rm termux-notification) #:use-module (crates-io))

(define-public crate-termux-notification-0.1.0 (c (n "termux-notification") (v "0.1.0") (h "001slx060k9p50aj0nfzi69hmaxhq21nzx5km12vmk1ikpgfjs8l") (f (quote (("callbacks"))))))

(define-public crate-termux-notification-0.1.1 (c (n "termux-notification") (v "0.1.1") (h "0jx46qn9pgh072q0sp7rb6hjsfwid72gxzvqnzkwy18aipa7x3r8") (f (quote (("callbacks"))))))

(define-public crate-termux-notification-0.1.2 (c (n "termux-notification") (v "0.1.2") (h "103rd127icjh0w2lzjvpf1mp3wkpcwdvvgifsrsh1qnx5pa650kp") (f (quote (("callbacks"))))))

(define-public crate-termux-notification-0.1.3 (c (n "termux-notification") (v "0.1.3") (h "1dx95nq0s7ich1fby8wfmj0cd9c86a48zjhi2ffz1sha8p4i9x23") (f (quote (("callbacks"))))))

