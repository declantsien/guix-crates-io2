(define-module (crates-io te rm terminator) #:use-module (crates-io))

(define-public crate-terminator-0.1.0 (c (n "terminator") (v "0.1.0") (h "041qq5rplmh6jnabdz68jc595j52x2xadzzwnfa3pl8g05zr25r0") (f (quote (("rust2015") ("default")))) (y #t)))

(define-public crate-terminator-0.2.0-alpha (c (n "terminator") (v "0.2.0-alpha") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^4") (d #t) (k 0)))) (h "1jb6zcs1r01ws9h4gpd6bhaq0ad7g0i90yg73x1znigxmy3m5jc7") (f (quote (("default")))) (s 2) (e (quote (("eyre" "dep:eyre") ("compat" "dep:anyhow" "dep:eyre") ("anyhow" "dep:anyhow"))))))

(define-public crate-terminator-0.2.0-beta (c (n "terminator") (v "0.2.0-beta") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^4") (d #t) (k 0)))) (h "1hhsflrwlf9jp5hy0rnvsmp1jin63w2ycalmr6g3nvficdvnrk16") (f (quote (("default")))) (s 2) (e (quote (("eyre" "dep:eyre") ("compat" "dep:anyhow" "dep:eyre") ("anyhow" "dep:anyhow"))))))

(define-public crate-terminator-0.2.0 (c (n "terminator") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "owo-colors") (r "^4") (d #t) (k 0)))) (h "1jyjhbskxjkib1hzvsc3rvnl8f1gs7qcbmh62hwkzmizvj25af6b") (f (quote (("default")))) (s 2) (e (quote (("eyre" "dep:eyre") ("compat" "dep:anyhow" "dep:eyre") ("anyhow" "dep:anyhow"))))))

(define-public crate-terminator-0.3.0 (c (n "terminator") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1820v4nmxjyfxlrxzhkn3p85naddgc6x8q300p77jl0ys1vx34a3") (f (quote (("default")))) (s 2) (e (quote (("eyre" "dep:eyre") ("compat" "dep:anyhow" "dep:eyre") ("anyhow" "dep:anyhow"))))))

(define-public crate-terminator-0.3.1 (c (n "terminator") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (o #t) (d #t) (k 0)))) (h "04xhmwwqp960ygd82zaqyb7isxav8hzf06r7mnllq3qrpx45g8l2") (f (quote (("default")))) (s 2) (e (quote (("eyre" "dep:eyre") ("compat" "dep:anyhow" "dep:eyre") ("anyhow" "dep:anyhow"))))))

