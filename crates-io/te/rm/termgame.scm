(define-module (crates-io te rm termgame) #:use-module (crates-io))

(define-public crate-termgame-0.1.0 (c (n "termgame") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1gs9z85fhdh19kfn7d0689nz71l9v9zlzqfsw3c4f4c46snk2f6k")))

(define-public crate-termgame-0.1.1 (c (n "termgame") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "071jncdh9xgxigp0phk94b8m2a3qgfmhip0rdxd21cginn8qrz9l")))

(define-public crate-termgame-0.2.0 (c (n "termgame") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "12n6zdpyzcck4k0fikrmn584nvn07864pqi22sg9ka9d1ajk610q")))

(define-public crate-termgame-0.3.0 (c (n "termgame") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "02l36n13nkw4nmi5smmlg2ybm3h587792qr8j9xmngmzj4h08p2i")))

(define-public crate-termgame-1.0.0 (c (n "termgame") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1q7d639q0iq516fc650p296ir4j6qcsivijgr8nkw8lxcsjsih4v")))

(define-public crate-termgame-1.0.1 (c (n "termgame") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0gy3l1cdhmcnwh1l8n8argjm1r2jxfz325jxabh2rk4gkxrj8lf0")))

(define-public crate-termgame-1.0.2 (c (n "termgame") (v "1.0.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0dry28rm7iq6z909xhdmplcvz3nb6asr4g4ipwnkr4vf9sfh7d4c")))

(define-public crate-termgame-1.1.0 (c (n "termgame") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0wcrhdh1x37wnxfpaklhz2qshw2q9f9jlg51cqgx7nkl0812cbqi")))

(define-public crate-termgame-1.2.0 (c (n "termgame") (v "1.2.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1sgak5vrnc2rix1bf8ixrc39jwhj64r0f7rf8p78yafgdzfs2jb3")))

(define-public crate-termgame-1.3.0 (c (n "termgame") (v "1.3.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1fziykgxn58s49pkbh119bwc9xvdj61k0jgwcpn8bsqlkzs86sjs")))

