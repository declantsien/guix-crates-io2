(define-module (crates-io te rm terminal-talk) #:use-module (crates-io))

(define-public crate-terminal-talk-0.1.0 (c (n "terminal-talk") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "enigo") (r "^0.2.0") (f (quote ("x11rb"))) (d #t) (k 0)))) (h "1csp9cpjhcsrhc71s7mqz8dyvpkpki297nakrvf8k0v7mnhz2czd")))

(define-public crate-terminal-talk-0.1.1 (c (n "terminal-talk") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "enigo") (r "^0.2.0") (f (quote ("x11rb"))) (d #t) (k 0)))) (h "00l1827sspg4vjliwhbvhy9mhw5njcsq5287pkdy8k2fvxlah9a8")))

(define-public crate-terminal-talk-0.2.0 (c (n "terminal-talk") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "enigo") (r "^0.2.0") (d #t) (k 0)))) (h "1zz9r23wvyn8rmjp7rwz3ym20bl30l1xpzcxnzghn53jhf3s6ni2") (f (quote (("x11rb" "enigo/x11rb") ("default" "x11rb"))))))

(define-public crate-terminal-talk-0.2.1 (c (n "terminal-talk") (v "0.2.1") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "enigo") (r "^0.2.0") (f (quote ("wayland"))) (d #t) (k 0)))) (h "07h3ibx7pgrzmvnayabckqi6mh3z7ir57ssary982s21nc2qg56v") (f (quote (("x11rb" "enigo/x11rb") ("default" "x11rb"))))))

(define-public crate-terminal-talk-0.2.2 (c (n "terminal-talk") (v "0.2.2") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "enigo") (r "^0.2.0") (f (quote ("wayland"))) (d #t) (k 0)))) (h "07kkgkcgn7j4piagkik685xd5v18jhsiynvmjwzxal0jsl4fqzm6") (f (quote (("x11rb" "enigo/x11rb") ("default" "x11rb"))))))

