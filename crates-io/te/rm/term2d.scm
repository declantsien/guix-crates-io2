(define-module (crates-io te rm term2d) #:use-module (crates-io))

(define-public crate-term2d-0.1.0 (c (n "term2d") (v "0.1.0") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "00v4pw5v98zv6lgz26yq06blkx32rk02p8cf153i03qmdf41v7a8")))

(define-public crate-term2d-0.2.0 (c (n "term2d") (v "0.2.0") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0lk78zwq6g66w6l93pxcc7li3ysh3qi01hp6hz7zb1rvy2s2l41i")))

(define-public crate-term2d-0.3.0 (c (n "term2d") (v "0.3.0") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0d871w6v58n8kmh6v1wk2dxxhyva0pmnk4rcaqd9vnm33iqd08dw")))

(define-public crate-term2d-0.3.1 (c (n "term2d") (v "0.3.1") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "009pyh31inlh7zy67wjdflf9sxsq41n6jcbilkphmj02fn1114s8")))

(define-public crate-term2d-0.3.2 (c (n "term2d") (v "0.3.2") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0icv880m38bah5yhpv6374hssapx3c9da6f0bq5jpl98y0wdj446")))

(define-public crate-term2d-0.4.0 (c (n "term2d") (v "0.4.0") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0qajcnf4ppqgia4q3hqxwn9d6jzmi2wizpq4ajx3sxphgldc4hw4")))

(define-public crate-term2d-0.5.0 (c (n "term2d") (v "0.5.0") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1s9xvjdq3acmxb0ccwrh8y6cmi7nh0yl8d33z0jrgyzc1h5339zx")))

(define-public crate-term2d-0.6.0 (c (n "term2d") (v "0.6.0") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "02xwky050cshm9h92l2apg4jzb6mpvb8ghf1664j5ncgvmvl2may")))

(define-public crate-term2d-0.6.1 (c (n "term2d") (v "0.6.1") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0j73c2a7ym0c4d5jwjnpc3sw9ngqb7qswh83dyqxl7y6p6hrii6a")))

(define-public crate-term2d-0.7.0 (c (n "term2d") (v "0.7.0") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0jkjknl53bi93bzhlhkkhn9hx0111bay1c1mcq79k7w367wj1911")))

(define-public crate-term2d-0.7.1 (c (n "term2d") (v "0.7.1") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "06vh9kxcx4i9wrpwp97rj9azx8b5gh2wly7gfakan9nidj66hfsp")))

(define-public crate-term2d-0.7.2 (c (n "term2d") (v "0.7.2") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1h4cgrnbycrq36m73140nr8vwvllsg31fqk3j5cnzswb6kz9gkk5")))

(define-public crate-term2d-0.7.3 (c (n "term2d") (v "0.7.3") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "02srkdsn600239g1s8vixxjc94i7ly63dswdjvp7nchi2j6jla33")))

(define-public crate-term2d-0.8.0 (c (n "term2d") (v "0.8.0") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1ijc8slf8cip0w3nwqirday61q5z0xcdqlacz579vrj0daqg6lvg")))

(define-public crate-term2d-0.8.1 (c (n "term2d") (v "0.8.1") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "108hnlnya5mjh5hdxzv1rnhh6k5swpqxb7s4w7pw10g4lhgzkamv")))

(define-public crate-term2d-0.8.2 (c (n "term2d") (v "0.8.2") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "10bmz1p0715zv72m4q87kp4v0bl5dnicpq4dr26mrpirx2f2m7jl")))

(define-public crate-term2d-0.8.3 (c (n "term2d") (v "0.8.3") (d (list (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0lim5gsw4iynqqzyx3bnz3s1l9dd6wf0637drhsbg3naf75q7qkw")))

