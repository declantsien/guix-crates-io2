(define-module (crates-io te rm termal_proc) #:use-module (crates-io))

(define-public crate-termal_proc-1.0.0 (c (n "termal_proc") (v "1.0.0") (d (list (d (n "termal_core") (r "^1.0.0") (d #t) (k 0)))) (h "1s71x4ijqbn4q2j2qx3k4b0vw5abskvr1z7v4apfl0lz148qcvi0")))

(define-public crate-termal_proc-1.0.1 (c (n "termal_proc") (v "1.0.1") (d (list (d (n "termal_core") (r "^1.0.1") (d #t) (k 0)))) (h "184riiimsjl9fpqqkxbbppp863zpl01hsh17szzppcnmb4kykkwz")))

