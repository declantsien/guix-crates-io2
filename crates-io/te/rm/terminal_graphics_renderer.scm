(define-module (crates-io te rm terminal_graphics_renderer) #:use-module (crates-io))

(define-public crate-terminal_graphics_renderer-0.1.0 (c (n "terminal_graphics_renderer") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0yfdinx7pfdp3cpdixzbhlv38dbyg1srvibszgzfvwqj7lf4dkdp")))

