(define-module (crates-io te rm terminal_chat) #:use-module (crates-io))

(define-public crate-terminal_chat-0.1.0 (c (n "terminal_chat") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0znhfxkmkbnakqc2gvwcpp1mizmc4qil2339y3kpdp7ama7k4np3")))

(define-public crate-terminal_chat-0.2.0 (c (n "terminal_chat") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.182") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "termimad") (r "^0.23.2") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ajpz47h01s7gd8icpjb3kdkwa4db58ph07ryinzsr7g70sbirfg")))

