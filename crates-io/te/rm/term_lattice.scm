(define-module (crates-io te rm term_lattice) #:use-module (crates-io))

(define-public crate-term_lattice-0.1.0 (c (n "term_lattice") (v "0.1.0") (d (list (d (n "enum_variant_eq") (r "^0.1.0") (d #t) (k 0)))) (h "00m3y1yj34szidk3vvixha3cvb8zmng3c9x9bfwb1wk8v593j7dy")))

(define-public crate-term_lattice-0.1.1 (c (n "term_lattice") (v "0.1.1") (d (list (d (n "enum_variant_eq") (r "^0.1.0") (d #t) (k 0)))) (h "06lmbzwb4lg259zfgib3as16nww9hpiz7q3fxa865r2h74p6pkz6") (y #t)))

(define-public crate-term_lattice-0.1.2 (c (n "term_lattice") (v "0.1.2") (d (list (d (n "enum_variant_eq") (r "^0.1.0") (d #t) (k 0)))) (h "0j1glhbwr6n39c29a8rvhsic6s2g4fliqf9mnj9gps5m85ym9znp")))

(define-public crate-term_lattice-0.2.0 (c (n "term_lattice") (v "0.2.0") (d (list (d (n "enum_variant_eq") (r "^0.1.0") (d #t) (k 0)))) (h "030b1s9y39b7a9hsa5d9vd08zs1wgb10iy2z7vlfm9fgkb1867yn")))

(define-public crate-term_lattice-0.3.0 (c (n "term_lattice") (v "0.3.0") (d (list (d (n "enum_variant_eq") (r "^0.1.0") (d #t) (k 0)))) (h "1dard4qk4ax1akprzpjb1kcdnpgz3c64wf32m3z3x3jyplg7nr0g")))

(define-public crate-term_lattice-0.4.0 (c (n "term_lattice") (v "0.4.0") (h "1537l4mkfvflddgk4q9xkfx0xx1w4ys4s9a4pjfvm1hmfcfwsn7x")))

(define-public crate-term_lattice-0.4.1 (c (n "term_lattice") (v "0.4.1") (h "1r8ky5bwnmbqq1yaahg5ynpf5s64s5ax0h0f6ywya542wbycp6b2")))

(define-public crate-term_lattice-0.4.2 (c (n "term_lattice") (v "0.4.2") (h "11918fchv0qq0nah2s65i1hhbjyfrhmpqxqxh7ixbq1s948s0qx1")))

