(define-module (crates-io te rm term_graph) #:use-module (crates-io))

(define-public crate-term_graph-0.1.0 (c (n "term_graph") (v "0.1.0") (d (list (d (n "line_drawing") (r "^0.8.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16") (d #t) (k 0)))) (h "12q1z1j3s3icna0vkwmlhjxn3gv0nq9a9w6qccgl8s5had22gq6l")))

