(define-module (crates-io te rm termtree) #:use-module (crates-io))

(define-public crate-termtree-0.2.0 (c (n "termtree") (v "0.2.0") (h "13y04qn5lc9w2y3ds8dhgipr45alvsl3c2yxy6dd9whqigmcfdgd")))

(define-public crate-termtree-0.2.1 (c (n "termtree") (v "0.2.1") (h "0y33b2v6inf22wgvkbpizrpqd2dingkd6wi4zb62i7774gfz5yvq")))

(define-public crate-termtree-0.2.2 (c (n "termtree") (v "0.2.2") (h "05hg5nlcwcwj57gfsi85qbz97z930sfs6ninbh5iglpiilpmlmkn")))

(define-public crate-termtree-0.2.3 (c (n "termtree") (v "0.2.3") (h "05jchbaqqy88zac8fwrp4yqqkxv7v7xcq1278dbrpr9d18cfr90k")))

(define-public crate-termtree-0.2.4 (c (n "termtree") (v "0.2.4") (h "02y9lhad22p56y8pgwz04si2pd91nxcl5djmmall6v1vd2c9hzjh")))

(define-public crate-termtree-0.3.0 (c (n "termtree") (v "0.3.0") (h "1zm3sidixz0qb9sgndr1srhsargmszncvz797anhp8jhr640aivk")))

(define-public crate-termtree-0.4.0 (c (n "termtree") (v "0.4.0") (h "1n2z544ygdb6pyjp9pxkgj4nsaqqgzv99jbdprkcnja7328rw1cm")))

(define-public crate-termtree-0.4.1 (c (n "termtree") (v "0.4.1") (h "0xkal5l2r3r9p9j90x35qy4npbdwxz4gskvbijs6msymaangas9k")))

