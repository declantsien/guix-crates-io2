(define-module (crates-io te rm termrs_runtime) #:use-module (crates-io))

(define-public crate-termrs_runtime-0.1.0 (c (n "termrs_runtime") (v "0.1.0") (d (list (d (n "core") (r "^0.1.0") (d #t) (k 0) (p "termrs_core")) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "12sd1kdp82h9w1g8hkxppw6faj0lqz6kpi3yf8x9lwh5lj4322cc") (f (quote (("x11" "core/x11") ("wayland" "core/wayland"))))))

(define-public crate-termrs_runtime-0.2.0 (c (n "termrs_runtime") (v "0.2.0") (d (list (d (n "core") (r "^0.2.0") (d #t) (k 0) (p "termrs_core")) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "13j6j2f1rkcbyvmya9gg75sbl820560n8hdmap9wf6bfzac253hx") (f (quote (("x11" "core/x11") ("wayland" "core/wayland"))))))

(define-public crate-termrs_runtime-0.3.0 (c (n "termrs_runtime") (v "0.3.0") (d (list (d (n "core") (r "^0.3.0") (d #t) (k 0) (p "termrs_core")) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0310fl0dd9j02s50kf7dg8bq6pxnf4ix05iw6ybbj2ajlsb7qgm0") (f (quote (("x11" "core/x11") ("wayland" "core/wayland"))))))

