(define-module (crates-io te rm termprompt) #:use-module (crates-io))

(define-public crate-termprompt-0.7.1 (c (n "termprompt") (v "0.7.1") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "zeroize") (r "^0.9.3") (f (quote ("std"))) (k 0)))) (h "1rjgkp13fk0qazm7b88vz9vy33iy4rcxhhkq5z56a9ngb7xdgzva")))

