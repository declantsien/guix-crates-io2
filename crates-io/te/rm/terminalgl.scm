(define-module (crates-io te rm terminalgl) #:use-module (crates-io))

(define-public crate-terminalgl-0.1.0 (c (n "terminalgl") (v "0.1.0") (h "1c7cppvl7n2pxgmavh4djvzc1ygsbrqzzbdbwq048awmnd7hxdgi")))

(define-public crate-terminalgl-0.2.0 (c (n "terminalgl") (v "0.2.0") (d (list (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "1rs7pzh6wvax40s55w0vxkd4n782279kii7lni0ji4jjgmpy5ljh")))

(define-public crate-terminalgl-0.2.1 (c (n "terminalgl") (v "0.2.1") (d (list (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "0r0pcja1qwpjn5z3vzagpglcrvmf849cn42gv10si9y9pgkxh4nn")))

(define-public crate-terminalgl-0.2.2 (c (n "terminalgl") (v "0.2.2") (d (list (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "01g16xcl46ls5xay7sxcrgkifjpb86in2krd7z3d0vqs0sga5iy2")))

