(define-module (crates-io te rm termbox) #:use-module (crates-io))

(define-public crate-termbox-0.0.1 (c (n "termbox") (v "0.0.1") (h "0fzng65kcp8an4ynw8xswvhlcvbqcby37mmlnrm8gbz5xs79iipp")))

(define-public crate-termbox-0.0.2 (c (n "termbox") (v "0.0.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0m53911sf0p9z08m4k8xn3sq6hm82wr28qibgwhl7b7z2ifghjfq")))

(define-public crate-termbox-0.0.3 (c (n "termbox") (v "0.0.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "08q3p9an4knfc4bccsnsz2ahbjgzndazwbw440rq90f3iq5002l1")))

(define-public crate-termbox-0.0.4 (c (n "termbox") (v "0.0.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0qb8k2szr8dj3rm2m8gzcm1wv2ymg9kxyvig6iszb4cdbkslz85g")))

(define-public crate-termbox-0.0.5 (c (n "termbox") (v "0.0.5") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "16i6zgdwvq8pj9pkk96iik3imvvlyrx71zlrfbli6bcbn5jp85f9")))

(define-public crate-termbox-0.0.6 (c (n "termbox") (v "0.0.6") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0p740p4b7j1729r277fxrjcyxx94qna8dx7rrmagabp1w18yxaza")))

(define-public crate-termbox-0.0.7 (c (n "termbox") (v "0.0.7") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0pa315hhpqpi89sa2jc5d6isbx822wbd7lgnp2c645qyq6vmzz54")))

(define-public crate-termbox-0.0.8 (c (n "termbox") (v "0.0.8") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "17gbkbllw894gq6l86k85wyyi3463yrjgq50n1lvdk9mjg7lpw38")))

(define-public crate-termbox-0.0.10 (c (n "termbox") (v "0.0.10") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "*") (d #t) (k 0)))) (h "17q5b4pk3wcm0fvj7wlh7bljmy1ddhpqma850frcxx2rvsw87lx6")))

(define-public crate-termbox-0.0.11 (c (n "termbox") (v "0.0.11") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "*") (d #t) (k 0)))) (h "1cimc6j8vbklsc0f2r7vdwyrnfzbb66q17ah7c08q0mbx6xqz8hg")))

(define-public crate-termbox-0.0.12 (c (n "termbox") (v "0.0.12") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "termbox-sys") (r "*") (d #t) (k 0)))) (h "1s0i2wk9hs1rh98vgjj82b1cyxvc5wa52ayqfq0843d7vwa03qyq")))

(define-public crate-termbox-0.1.0 (c (n "termbox") (v "0.1.0") (d (list (d (n "aurum-numeric") (r "^0.1.1") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.8") (d #t) (k 0)))) (h "02kfqdwvfgvydxg06qps7a7cs5y7gnyfljb23bj5hhss9dy1l399")))

(define-public crate-termbox-0.1.1 (c (n "termbox") (v "0.1.1") (d (list (d (n "aurum-numeric") (r "^0.1.3") (d #t) (k 0)) (d (n "termbox-sys") (r "^0.2.9") (d #t) (k 0)))) (h "0sw9kgd1dpyg17a9xmhv1hswrxvhqjksw643zsydsli01xnmbxrd")))

(define-public crate-termbox-0.2.0 (c (n "termbox") (v "0.2.0") (d (list (d (n "termbox-sys") (r "^0.2.9") (d #t) (k 0)) (d (n "try_from") (r "^0.2.1") (d #t) (k 0)))) (h "1ab4lbq75zmblh8r05bkp0nn242ghcvkhr5x8qmf6zpjhk0w78d9")))

