(define-module (crates-io te rm terminity_widgets) #:use-module (crates-io))

(define-public crate-terminity_widgets-0.1.0 (c (n "terminity_widgets") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "terminity_widgets_proc") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)) (d (n "format") (r "^0.2.4") (d #t) (k 2)))) (h "1sds1h1q8gc2c4dmf33z84blnxjjp1xvqlz3iw2bi6dadf7dwlv8")))

