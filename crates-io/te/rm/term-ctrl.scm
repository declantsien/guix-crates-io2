(define-module (crates-io te rm term-ctrl) #:use-module (crates-io))

(define-public crate-term-ctrl-0.5.0 (c (n "term-ctrl") (v "0.5.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1k5sg7gnmvp0lcg4v1qsr03gka5kpvccijyz1lc0zdl6vz5srakg")))

(define-public crate-term-ctrl-0.6.0 (c (n "term-ctrl") (v "0.6.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zd2jmz8r64rvwfgdrchd63m2wlwmyccvyl29d6dj4hfsmicrin5")))

(define-public crate-term-ctrl-0.6.1 (c (n "term-ctrl") (v "0.6.1") (d (list (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.0, <0.4.0") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11sarsxpa8hcvdaqxyvhrk91c3191drix6swa96b3a79ipv4p324")))

(define-public crate-term-ctrl-0.7.0 (c (n "term-ctrl") (v "0.7.0") (d (list (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.0, <0.4.0") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1b18z7ajihnkm2ljgnambmpjb4790zi2kl2safi1pjqbhz0rzlks")))

(define-public crate-term-ctrl-0.7.1 (c (n "term-ctrl") (v "0.7.1") (d (list (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.0, <0.4.0") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fj3jj1j0qgzdrdmq6dj80rmrpfbsgynda91vqmkfka72njd4ilr")))

(define-public crate-term-ctrl-0.7.2 (c (n "term-ctrl") (v "0.7.2") (d (list (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.0, <0.4.0") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rjff5axmkm4446wp1q3wcbk139q5y0sfywywwmadbwz48cc7kgp")))

(define-public crate-term-ctrl-0.7.3 (c (n "term-ctrl") (v "0.7.3") (d (list (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.0, <0.4.0") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1013k3hvi9rihylqnm8l74004yxb5yi2pw22zragigi0j9xhqhhp")))

(define-public crate-term-ctrl-0.7.4 (c (n "term-ctrl") (v "0.7.4") (d (list (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.0, <0.4.0") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rw3gdssszb4yrlddi09189p1sl3ww76rgnb0ajd4fp3d58kifh5")))

(define-public crate-term-ctrl-0.7.5 (c (n "term-ctrl") (v "0.7.5") (d (list (d (n "atty") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.0, <0.4.0") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fiflvw4bssfyy69iz8mjy0dxs2xdg78xar8i61ccfpk3lah1fpf")))

(define-public crate-term-ctrl-0.7.6 (c (n "term-ctrl") (v "0.7.6") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bkabyndvj1nns6ypm52kps84vjw36yhmcncsy9968mwil3412cc")))

(define-public crate-term-ctrl-0.7.7 (c (n "term-ctrl") (v "0.7.7") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1l3570932fvg849d8n0a6nvq3hzj91yway7djgdg7ndqsv6rfs70")))

(define-public crate-term-ctrl-0.7.8 (c (n "term-ctrl") (v "0.7.8") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "consoleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1r6agqy0fivc16x2w4rjhngxlvk82jgilrbx24ylrr9d88ng3ni1")))

