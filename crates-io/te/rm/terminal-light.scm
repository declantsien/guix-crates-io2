(define-module (crates-io te rm terminal-light) #:use-module (crates-io))

(define-public crate-terminal-light-0.1.0 (c (n "terminal-light") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1isbc0vpdd282q1cab1gr9lxci9qbiqlsb3cijpy1gxyqvwjv3jl")))

(define-public crate-terminal-light-0.2.0 (c (n "terminal-light") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0adb72sq0x0bq79bgphd8lkh4dbc147wb3jb7k8c8l9yi5jqc9fh")))

(define-public crate-terminal-light-0.3.0 (c (n "terminal-light") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06lq6m39k2czhz9dphdvfldh50ka62jkk3x38980hfywydfzd569")))

(define-public crate-terminal-light-0.4.0 (c (n "terminal-light") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.22") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bp2vdaxj21vpd9blrmp1qpqi11hb398fr5f4iqss418ka4d8b90")))

(define-public crate-terminal-light-0.5.0 (c (n "terminal-light") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.21") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.22") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xc455h1brcmhxljlixz1lj01qfnghf0r0xn6ryxjhv7w998vlxn")))

(define-public crate-terminal-light-0.6.0 (c (n "terminal-light") (v "0.6.0") (d (list (d (n "coolor") (r "^0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (t "cfg(unix)") (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 2)) (d (n "nix") (r "^0.22") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13061mbdnb1p79mzxg5rmpfbd053ijdrg6nzagyxhsvp0p4qgx2k")))

(define-public crate-terminal-light-0.6.1 (c (n "terminal-light") (v "0.6.1") (d (list (d (n "coolor") (r "^0.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (t "cfg(unix)") (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 2)) (d (n "nix") (r "^0.22") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cgzishyb5v1js63likzfwvsl2s7i0qlpv5n36fnir89b1l2p1a8")))

(define-public crate-terminal-light-0.7.0 (c (n "terminal-light") (v "0.7.0") (d (list (d (n "coolor") (r "^0.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (t "cfg(unix)") (k 0)) (d (n "crossterm") (r "^0.21") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xterm-query") (r "^0.1") (d #t) (k 0)))) (h "1hf89z4v31ccvyqmw6lxc4s95sw60nn6hxasw0vhm4phrmmkhzpq")))

(define-public crate-terminal-light-0.8.0 (c (n "terminal-light") (v "0.8.0") (d (list (d (n "coolor") (r "^0.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xterm-query") (r "^0.1") (d #t) (k 0)))) (h "0ijzx8h7n5zh7m8i3s524scpdf7w495f2dpicc499r9p2lryifiy")))

(define-public crate-terminal-light-0.8.1 (c (n "terminal-light") (v "0.8.1") (d (list (d (n "coolor") (r "^0.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xterm-query") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1lb2ssvr5hwrpgfm7x6baj3r0l1adzx9iaywnnqh92y1k58fjygj")))

(define-public crate-terminal-light-1.0.0 (c (n "terminal-light") (v "1.0.0") (d (list (d (n "coolor") (r "^0.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xterm-query") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "057r95pypv7lxnz3wpha84aningan1l6c3pdavfwizfpx3p66w6h")))

(define-public crate-terminal-light-1.0.1 (c (n "terminal-light") (v "1.0.1") (d (list (d (n "coolor") (r "^0.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xterm-query") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1nqdbgij01yb7qizs2klwx7qd6jp8fjlkp9fwjnpnzdk0aw3dai3")))

(define-public crate-terminal-light-1.1.0 (c (n "terminal-light") (v "1.1.0") (d (list (d (n "coolor") (r "^0.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xterm-query") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0lhld86jjw359d7w9javninym6vbnbgygradldhxqa7fj9rr64gf")))

(define-public crate-terminal-light-1.1.1 (c (n "terminal-light") (v "1.1.1") (d (list (d (n "coolor") (r "^0.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xterm-query") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1bqi8ldlvg2jh1k5lq2xnhrgr74r35iz9z61wj8j7wbhzfdb6xwh")))

(define-public crate-terminal-light-1.2.0 (c (n "terminal-light") (v "1.2.0") (d (list (d (n "coolor") (r "^0.9") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xterm-query") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0lal556ylq548hmqlrqs824j6frynj91dmwh7njyhfwb91mr8j5p")))

(define-public crate-terminal-light-1.3.0 (c (n "terminal-light") (v "1.3.0") (d (list (d (n "coolor") (r "^0.9") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xterm-query") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)))) (h "0hzddxy63vqnm4jaa83a49d4kppnjpmal14pqqw7mfzr651k9580")))

(define-public crate-terminal-light-1.4.0 (c (n "terminal-light") (v "1.4.0") (d (list (d (n "coolor") (r "^0.9") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xterm-query") (r "^0.4") (d #t) (t "cfg(unix)") (k 0)))) (h "1drp4h9knlpgc9f7h8ckwa2g2wg9s49x6w0pnvrfyglamfvbgv52")))

