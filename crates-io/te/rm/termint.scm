(define-module (crates-io te rm termint) #:use-module (crates-io))

(define-public crate-termint-0.1.0 (c (n "termint") (v "0.1.0") (d (list (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1qxw9r73x5h805pwgwr65r9zkrnf580kfddsad2nr8956b4i5fpv")))

(define-public crate-termint-0.1.1 (c (n "termint") (v "0.1.1") (d (list (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1lfpkjz5bbx8fly0ayw3lmj37zibvhccrpbvbd5bwd32hx8xdyvj")))

(define-public crate-termint-0.2.0 (c (n "termint") (v "0.2.0") (d (list (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0b6sr3gdziqxj3ln7jp5dxxzm7y410gmy376v9qq41k9d070nxq4")))

(define-public crate-termint-0.3.0 (c (n "termint") (v "0.3.0") (d (list (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1081d2hdd1wh3fqiizydsh8m91smckypvjykrm5h0ll1xggmcpqc")))

(define-public crate-termint-0.3.1 (c (n "termint") (v "0.3.1") (d (list (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "00w69ra8qn9avwrw15qsjjr8m0w9d06p675cjdbdmavydxihvqwm")))

(define-public crate-termint-0.4.0 (c (n "termint") (v "0.4.0") (d (list (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0dcsyjrw4z2206d1qjk66gm2j7b8ibvl6nlp5xrv78h5v4li255m")))

(define-public crate-termint-0.4.1 (c (n "termint") (v "0.4.1") (d (list (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1xz8hpnbnyp7r2qr8ba0jyffi35dl50nhgnxq7d8b8v9jw133wgj")))

(define-public crate-termint-0.4.2 (c (n "termint") (v "0.4.2") (d (list (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1083xgb3j0ykr4ggsknq6r9hkhsddvmbrgpfix3gyq398c7qsx41")))

