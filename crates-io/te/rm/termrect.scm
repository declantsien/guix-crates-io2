(define-module (crates-io te rm termrect) #:use-module (crates-io))

(define-public crate-termrect-0.1.0 (c (n "termrect") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-width") (r "~0") (d #t) (k 0)))) (h "0h2rf1zy71pmhlpja4d1l2mpjanc2g9yb1wgkqjr48j4hq5vdkyb")))

(define-public crate-termrect-0.1.2 (c (n "termrect") (v "0.1.2") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "06w4c6b6haj2q80w19my39driyy6dpdmgn4kcry18568xa4zfzav")))

