(define-module (crates-io te rm termination_signal) #:use-module (crates-io))

(define-public crate-termination_signal-0.1.0 (c (n "termination_signal") (v "0.1.0") (h "1pg1a68yaghmxc7acp5a3qz5y7nx1zp7igysjsbgs7ldvvh2faqx")))

(define-public crate-termination_signal-0.1.1 (c (n "termination_signal") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "signal-hook-tokio") (r "^0.3") (f (quote ("futures-v0_3"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1v1lzsyqcxzb784sbiznp8dgvqrcj05ibvz3kw0xybipdnqirqix") (f (quote (("sync") ("default" "sync" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:signal-hook-tokio" "dep:futures"))))))

(define-public crate-termination_signal-0.1.2 (c (n "termination_signal") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "signal-hook-tokio") (r "^0.3") (f (quote ("futures-v0_3"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1ykbdfacylyhb24hi6rpqm00qs2759rnj3xbyb0wbzjy681z92kw") (f (quote (("sync") ("default" "sync" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:signal-hook-tokio" "dep:futures"))))))

(define-public crate-termination_signal-0.1.3 (c (n "termination_signal") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "signal-hook-tokio") (r "^0.3") (f (quote ("futures-v0_3"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "13xdy69idhpl5bs7bhbr0x8588vz4ah33pq73hmp78k96hx9kqhn") (f (quote (("sync") ("default" "sync" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:signal-hook-tokio" "dep:futures"))))))

