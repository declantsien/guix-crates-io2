(define-module (crates-io te rm termit-ansi) #:use-module (crates-io))

(define-public crate-termit-ansi-0.1.1 (c (n "termit-ansi") (v "0.1.1") (h "1qhmfsh1fcdr7zfrv5k2ns2naa0zmw7kqyr3kp92f73klf7i4w4p") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-termit-ansi-0.2.0 (c (n "termit-ansi") (v "0.2.0") (h "05y52lrqpkcm4dcp16h3yqsigsx6h0yzkdapk234fi3c9c5dvmsp") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-termit-ansi-0.2.1 (c (n "termit-ansi") (v "0.2.1") (h "03vspmky2iimqz9jcaffy34jasff3vpgn6jhld6k5186jl0gzzh3") (f (quote (("std") ("default")))) (y #t)))

