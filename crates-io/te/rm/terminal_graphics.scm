(define-module (crates-io te rm terminal_graphics) #:use-module (crates-io))

(define-public crate-terminal_graphics-0.1.0 (c (n "terminal_graphics") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1vsla1j6j1paz9gpifq694vb3hygxwlsy5kp8mxskrc32by88b06")))

(define-public crate-terminal_graphics-0.1.1 (c (n "terminal_graphics") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0gzpysn47chz9g91kfcfzndg56b4yf9jhwjpza1649hxb0y3khj7")))

(define-public crate-terminal_graphics-0.1.2 (c (n "terminal_graphics") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1vwghjcrkwf5nk05azasai1b7825l5n3n58ydw4777773bps6n2b")))

(define-public crate-terminal_graphics-0.1.3 (c (n "terminal_graphics") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "08m2kn90lpp8pxayz9y6izsjmg76yc1ymjbrcjhwhclxa6rdrm0m")))

(define-public crate-terminal_graphics-0.1.4 (c (n "terminal_graphics") (v "0.1.4") (h "0xwdzw4dxg6ycgbj1w02kqi1m864g0cnzivxfbwlmii5xl32r4z9")))

(define-public crate-terminal_graphics-0.1.5 (c (n "terminal_graphics") (v "0.1.5") (h "1nsf920k4wqdihfv0v6p9qflhadk35sdwgcjdpap5jr3w495jn7k")))

