(define-module (crates-io te rm term-saver) #:use-module (crates-io))

(define-public crate-term-saver-0.1.0 (c (n "term-saver") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)))) (h "0yzlcl2bjzs55xr8zi3nmbbw67r8bckdjrmnpnxg8sjpiap0hwi9") (r "1.74")))

