(define-module (crates-io te rm terminal_thrift) #:use-module (crates-io))

(define-public crate-terminal_thrift-0.0.1 (c (n "terminal_thrift") (v "0.0.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "05g74yd3jqj483ja4vih69ap9ab38ph0y84hd4ks1q4221rrn8gv")))

(define-public crate-terminal_thrift-0.0.2 (c (n "terminal_thrift") (v "0.0.2") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "0zjm29nvbx8dbz71sdi4rzmm49n6wipcqphi300p28wqfx1bzqx9")))

(define-public crate-terminal_thrift-0.0.3 (c (n "terminal_thrift") (v "0.0.3") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "11nnhrjmqbp1h3bsfapa333d1caqaxwdvfc1f7lxdnizdndnk1x7")))

(define-public crate-terminal_thrift-0.1.0 (c (n "terminal_thrift") (v "0.1.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "ordered-float") (r "^0") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "01c6qlkfp7jfg36313xb3jd460a9s1sfx8r6zja3bdyxaiyl9als")))

(define-public crate-terminal_thrift-0.2.0 (c (n "terminal_thrift") (v "0.2.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "ordered-float") (r "^0") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "03rnf6jn7q8fqrg39g883qbj592figfwllh66rafv8j82rsz1hdr")))

(define-public crate-terminal_thrift-0.2.1 (c (n "terminal_thrift") (v "0.2.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "ordered-float") (r "^0") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "0rc1i0mgawg2mi7ismxhfh3042k9z1aiafxiv2n5p30jahmk8yca")))

(define-public crate-terminal_thrift-0.3.0 (c (n "terminal_thrift") (v "0.3.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "ordered-float") (r "^0") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "0jlaqmyj6m2s1209ladqdnc9sk7j97fixd5fvvwz107j6dpmw296")))

(define-public crate-terminal_thrift-0.3.1 (c (n "terminal_thrift") (v "0.3.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "ordered-float") (r "^0") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "0vyy27m245y51sjmjfgk1710kn606zvs8k99swpa8qnfrh0wj86m")))

(define-public crate-terminal_thrift-0.3.2 (c (n "terminal_thrift") (v "0.3.2") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "ordered-float") (r "^0") (d #t) (k 0)) (d (n "podio") (r "^0.1") (d #t) (k 0)))) (h "0hkl1pkbffdkfscawn3r3pc4cyh6ixy8mm7sxzs3qj05iy78jxcz")))

