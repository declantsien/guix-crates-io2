(define-module (crates-io te rm termite-macro) #:use-module (crates-io))

(define-public crate-termite-macro-0.1.0-alpha (c (n "termite-macro") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s143mncsl8ys5vm6s4dl5av712i013nxz8f4y7fibgwxn17nhxh")))

