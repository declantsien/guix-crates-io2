(define-module (crates-io te rm terminfo) #:use-module (crates-io))

(define-public crate-terminfo-0.1.0 (c (n "terminfo") (v "0.1.0") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1rk2f7rs4zqdpnh6qdh5jfg54f12sykx1cyhr1c541jmza93k8nm")))

(define-public crate-terminfo-0.1.1 (c (n "terminfo") (v "0.1.1") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1wjjrbd2nkgc5j8ymzj16801y1kwlz7ivp3j9ldlibfd86kr4kcw")))

(define-public crate-terminfo-0.1.2 (c (n "terminfo") (v "0.1.2") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "08g228x36grdg774fmdsyxf98l1ya77g5gj4qn393nndm3nmmh7k")))

(define-public crate-terminfo-0.1.3 (c (n "terminfo") (v "0.1.3") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "0ibjrhmczw21v8gbvm01ziacirz6rf11r3mgwxcc0gjix4vsyask")))

(define-public crate-terminfo-0.1.4 (c (n "terminfo") (v "0.1.4") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "082y31cqgr2ylsca1hy12vfm73rwsc53x6l7zmqa3615cabh2hl0")))

(define-public crate-terminfo-0.1.5 (c (n "terminfo") (v "0.1.5") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "1kz4mk1ds6p6vynfdh9z849ngkwzl6nyhx86dmazv1fxfm7bd177")))

(define-public crate-terminfo-0.2.0 (c (n "terminfo") (v "0.2.0") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "0cx8fd6qh3aaxa7adjgvbms4qgdh4n9wy2v2j8k3bv3hgfj9qqn6")))

(define-public crate-terminfo-0.2.1 (c (n "terminfo") (v "0.2.1") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "0mxrw04m1lq07pjw0pkp2m0nk7w0lr3vkyqmwfcncn6rxk6xmknn")))

(define-public crate-terminfo-0.2.2 (c (n "terminfo") (v "0.2.2") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "1aiwbxhibxf48572vwpjfhs8kzkgsjjnnq4ycqqp1wch8yzddm0i")))

(define-public crate-terminfo-0.2.3 (c (n "terminfo") (v "0.2.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "12fy43dj5sabb2g1p8z1mk4jx192x5kbx3fwfhvqbbf0nz82q1yw")))

(define-public crate-terminfo-0.2.4 (c (n "terminfo") (v "0.2.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "1mrlck9djf1krraaxsfvfzw8yivvkrdmikqy5iw9gipv288y6yvs")))

(define-public crate-terminfo-0.3.0 (c (n "terminfo") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "0y8xhn29zbn22ldns8by04sr5iwfyvy471ysgrlvhi66c3ihqy2i")))

(define-public crate-terminfo-0.3.1 (c (n "terminfo") (v "0.3.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "1xjkqkrpv35c32dcp615ln2cj31bv13kj2ncbxrx7g894iiwkphv")))

(define-public crate-terminfo-0.4.0 (c (n "terminfo") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "1qbg6ac10azbx5rgsz5gq0db92jf370fhhdx0xgi62c18ykfnxk0")))

(define-public crate-terminfo-0.5.0 (c (n "terminfo") (v "0.5.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "0j2a39ls79fgy5nqv4w65fz94ix1a1pw89mvmwi9qqv20yr8xa6q")))

(define-public crate-terminfo-0.6.0 (c (n "terminfo") (v "0.6.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "16j9jbj66l1vjxb1zy8i3d8rc63fhgpz6drjs8hpisxgsscqrpwz")))

(define-public crate-terminfo-0.6.1 (c (n "terminfo") (v "0.6.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)))) (h "17k8vqvicd6yg0iqmkjnxjhz8h8pknv86r03nq3f3ayjmxdhclcf")))

(define-public crate-terminfo-0.7.0 (c (n "terminfo") (v "0.7.0") (d (list (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "0dknpmiks34203m16asdfyhjnlcwjg96qfyryc93bvww0vyn7fk9")))

(define-public crate-terminfo-0.7.1 (c (n "terminfo") (v "0.7.1") (d (list (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "0gd0kmd3b1w7cdc6kc4hdzyhj9148nlyjcya721cab9qx7by1lwq")))

(define-public crate-terminfo-0.7.2 (c (n "terminfo") (v "0.7.2") (d (list (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "1z7cixn80b6sbcfb47nf2slnh4lq9y4241s55xbaqx3h40alf1hj")))

(define-public crate-terminfo-0.7.3 (c (n "terminfo") (v "0.7.3") (d (list (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5.0") (f (quote ("std"))) (k 0)) (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "0zkln56bsixjad6rsfy9mm15d9ygm89i63cn3gn685hjwrvik5vn")))

(define-public crate-terminfo-0.7.4 (c (n "terminfo") (v "0.7.4") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)) (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)))) (h "1nx1c0lphcyp7vmnhz9kazwwlx12pn1gcq592sxkfafwr37y6a7v")))

(define-public crate-terminfo-0.7.5 (c (n "terminfo") (v "0.7.5") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)))) (h "11d576lbjis5437rdz7k3k8glayxnhz6hqmlphnkbxm01pvswcfs")))

(define-public crate-terminfo-0.8.0 (c (n "terminfo") (v "0.8.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("std"))) (k 0)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)))) (h "13s9920jzmnr0jidik8yn6gvkic9n39sl28440mx4x8pd2kd6v36")))

(define-public crate-terminfo-0.9.0 (c (n "terminfo") (v "0.9.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("std"))) (k 0)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)))) (h "0qp6rrzkxcg08vjzsim2bw7mid3vi29mizrg70dzbycj0q7q3snl")))

