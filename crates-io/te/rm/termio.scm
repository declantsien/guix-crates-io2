(define-module (crates-io te rm termio) #:use-module (crates-io))

(define-public crate-termio-0.1.0 (c (n "termio") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "08535b6md0a0cqgp0zczjjm58nw19332a1spmimsz207nnh287hr")))

