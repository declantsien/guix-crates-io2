(define-module (crates-io te rm term-basics-linux) #:use-module (crates-io))

(define-public crate-term-basics-linux-0.1.0 (c (n "term-basics-linux") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1zr6xcv2pfh6i8ka0i6l8yflg1jhza3yhbvmd0ydzizv2kp6rlxx")))

(define-public crate-term-basics-linux-0.1.1 (c (n "term-basics-linux") (v "0.1.1") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "0gy3590lilp72w3rv4k7yiljr3yxwx7q4l43zqvcfwiw9b473852")))

(define-public crate-term-basics-linux-0.1.2 (c (n "term-basics-linux") (v "0.1.2") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1nypmwpiz6nar9b10nbfhrgwzvi1x429477b6jiqfyb2r67smzaa")))

(define-public crate-term-basics-linux-0.1.3 (c (n "term-basics-linux") (v "0.1.3") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "0b9lp41gymvik2v3qm7svcv02mgq16ypcf6xk7mz5maybcy7kgb1")))

(define-public crate-term-basics-linux-0.2.0 (c (n "term-basics-linux") (v "0.2.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1ljzxsrhn50f7pvqwrk69nz4l113r8pm2f7yn95zkifivlgsrj4b")))

(define-public crate-term-basics-linux-0.2.1 (c (n "term-basics-linux") (v "0.2.1") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1d0lbss99hjpk5544mw1i2fsm8rwvvfmpdh30xbxmn2mkk9d4hvz")))

(define-public crate-term-basics-linux-0.2.2 (c (n "term-basics-linux") (v "0.2.2") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "169ppj6bmri2f1r9hv2c07j6lsxwpp9q0m98gc7i8c62bh0vbq9n")))

(define-public crate-term-basics-linux-0.2.3 (c (n "term-basics-linux") (v "0.2.3") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "0l62a0nq4f5zr8fvb42wbm3hiip0bvd09r15bq1057vq84zbn2kf")))

(define-public crate-term-basics-linux-0.2.4 (c (n "term-basics-linux") (v "0.2.4") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1zgli8ghsk0sxgfk8nlbbb45bzvgzcccjiazih826mfkmx1q44rk")))

(define-public crate-term-basics-linux-0.2.5 (c (n "term-basics-linux") (v "0.2.5") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "0s8b7zmx8c9l9izi6bvrg2xr2hkbd0dm1fmmr54wd8205nvvik0g")))

(define-public crate-term-basics-linux-0.2.6 (c (n "term-basics-linux") (v "0.2.6") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1wihmyjxpjnsxn3qc7l14a9c8vh132la4kbad5kbgykzgvqhlrw9")))

(define-public crate-term-basics-linux-0.3.0 (c (n "term-basics-linux") (v "0.3.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "0c9aag8s7hgc2mb2jpcdf8zid05h5nb1g87nb89ja8pjfgq2d5ma")))

(define-public crate-term-basics-linux-0.4.0 (c (n "term-basics-linux") (v "0.4.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "0rjx360npnr5hyqhzwj3pdhvrnkfkskqhkwv775hnsyalpa9lk7b")))

(define-public crate-term-basics-linux-0.5.0 (c (n "term-basics-linux") (v "0.5.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "0mc2dvd9clf6r8yzqyqxx70pqjp5jfkpj48ilqxgix1md4fxc6c4")))

(define-public crate-term-basics-linux-0.5.1 (c (n "term-basics-linux") (v "0.5.1") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "16frkgkgavr710ql6z8y86idhlzc2apiy9v8dmvjf62mjx25vdx9")))

(define-public crate-term-basics-linux-0.5.2 (c (n "term-basics-linux") (v "0.5.2") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1s5q6dh8sxqil0ardr03h5dl3zsp28nyqlxrg1frjx90z0391sd6")))

(define-public crate-term-basics-linux-0.5.3 (c (n "term-basics-linux") (v "0.5.3") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "123v6aavhxlixzvwwqsaqar9ihvjrbxrzdw796adbrrs8q1k8jn6")))

(define-public crate-term-basics-linux-0.5.4 (c (n "term-basics-linux") (v "0.5.4") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1s2qn76z7lsgcsgpf2wdlyh8gwfaizdfdpnafasqs3rwszzk3rxb")))

(define-public crate-term-basics-linux-0.5.5 (c (n "term-basics-linux") (v "0.5.5") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "18csy3c358cclms0xsicpshqdg1q20ckmr04sz8swrlvlwfdj6mk")))

(define-public crate-term-basics-linux-0.5.6 (c (n "term-basics-linux") (v "0.5.6") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1vqfagfmw8r81jbzpdwnv0splcdavyr6s1dfwv6jmxwqgbh6l65n")))

(define-public crate-term-basics-linux-0.5.7 (c (n "term-basics-linux") (v "0.5.7") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "0gbrgj1xhx3hyv4r7k9srczkrpkfs23nkgn8jc88am4kqxyr06vg")))

(define-public crate-term-basics-linux-0.5.8 (c (n "term-basics-linux") (v "0.5.8") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1mhw8f0m7i6hmvxkk9r8fa82fvv5cb2pd1yi7sqk607lpgvcj4dg")))

(define-public crate-term-basics-linux-1.0.0 (c (n "term-basics-linux") (v "1.0.0") (d (list (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "0sjlvss09l2084jji64bwj8nhxi61m5w6dlvvxv9r2nsfvzaagiw")))

(define-public crate-term-basics-linux-1.0.1 (c (n "term-basics-linux") (v "1.0.1") (d (list (d (n "num-derive") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "1kpi0xghxq1dl75ppz78b42kkiywy9ph453ykp401kfkmbjkq6wc")))

