(define-module (crates-io te rm termclock) #:use-module (crates-io))

(define-public crate-termclock-0.1.0 (c (n "termclock") (v "0.1.0") (d (list (d (n "clap") (r "^2.20.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pancurses") (r "^0.7.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "10jvhx1g4k8nwlmxi79y8a89sxdkf8g3wyjfbis436drisqhi3sr")))

(define-public crate-termclock-0.1.1 (c (n "termclock") (v "0.1.1") (d (list (d (n "clap") (r "^2.20.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pancurses") (r "^0.7.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1159srvs1dj153qyrf2rqavdnmppbrrmi37lpr42incqlbphlz79")))

