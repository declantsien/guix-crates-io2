(define-module (crates-io te rm termpad) #:use-module (crates-io))

(define-public crate-termpad-1.0.0 (c (n "termpad") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (k 0)) (d (n "simplelog") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0xsn21m79xzlfz3kjddnb4bnqd0h4kcrs8jqgp3ssbj37p9rkjpl")))

(define-public crate-termpad-1.1.0 (c (n "termpad") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (k 0)) (d (n "simplelog") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "zstd") (r "^0.6") (d #t) (k 0)))) (h "135v1khsg276nry5d2fb0sp1aaaylm6ccviwdvzb4hzrhh4rk92g")))

(define-public crate-termpad-1.2.0 (c (n "termpad") (v "1.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (k 0)) (d (n "simplelog") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "zstd") (r "^0.6") (d #t) (k 0)))) (h "0qgl1z6rbjk5fx5h77s2mgvjy3is0zcpr0j5w1syhr1kvkymwby5")))

