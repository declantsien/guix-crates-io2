(define-module (crates-io te rm termion-input-tokio) #:use-module (crates-io))

(define-public crate-termion-input-tokio-0.2.0 (c (n "termion-input-tokio") (v "0.2.0") (d (list (d (n "bytes") (r "^0.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3.0") (f (quote ("macros" "rt-multi-thread" "io-std" "time"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.5.1") (f (quote ("codec"))) (d #t) (k 0)))) (h "1fws9hyfix5kz0pzlavm59mliixa2pkrnbwqsvhqk2a6n27khhqv")))

(define-public crate-termion-input-tokio-0.3.0 (c (n "termion-input-tokio") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread" "io-std" "time"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "1jcsgi4zw1zhw9gz0kjq4p0p4dvhcmrmv15px5a08w5vzmy8rfjv")))

