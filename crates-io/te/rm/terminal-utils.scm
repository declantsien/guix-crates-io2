(define-module (crates-io te rm terminal-utils) #:use-module (crates-io))

(define-public crate-terminal-utils-0.1.0 (c (n "terminal-utils") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "sync" "signal" "time"))) (o #t) (d #t) (k 0)) (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Console" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07ivixab6h0n6m2bkvl57ysdn6k3az7wf0h43qdg6inwsmhaf1p3") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

