(define-module (crates-io te rm termcolor2rgb) #:use-module (crates-io))

(define-public crate-termcolor2rgb-1.0.0 (c (n "termcolor2rgb") (v "1.0.0") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0jw0xb3zas1ag8gxhfr5c74qvvw2k1bqfy0fbfpcrsaz0h7pnqia")))

(define-public crate-termcolor2rgb-1.0.1 (c (n "termcolor2rgb") (v "1.0.1") (d (list (d (n "termcolor") (r "^1.0") (d #t) (k 0)))) (h "0lmvf9yl44dng7ziag1b6jbljy82xm83k5fjyfq77i8rp63dgxp0")))

