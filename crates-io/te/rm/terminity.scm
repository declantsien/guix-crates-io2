(define-module (crates-io te rm terminity) #:use-module (crates-io))

(define-public crate-terminity-0.1.0 (c (n "terminity") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "terminity_widgets") (r "^0.1") (d #t) (k 0)))) (h "0b68sawc4lxw7mgb5fb4i0sb4wzvm3ng3452s6yhlv41vpkzjl68")))

