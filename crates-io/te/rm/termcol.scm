(define-module (crates-io te rm termcol) #:use-module (crates-io))

(define-public crate-termcol-0.1.0 (c (n "termcol") (v "0.1.0") (h "1gs7k8lvg8dkf544xw6qbyzpswdq59p85jfwl1fv0p13q60hvziw")))

(define-public crate-termcol-0.1.1 (c (n "termcol") (v "0.1.1") (h "1sbhqqjvbagdpczhkwn5rvrwr5pjk9ps4w9n8mx0pz8zi431n1lc")))

