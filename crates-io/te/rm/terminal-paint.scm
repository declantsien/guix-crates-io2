(define-module (crates-io te rm terminal-paint) #:use-module (crates-io))

(define-public crate-terminal-paint-0.1.0 (c (n "terminal-paint") (v "0.1.0") (h "1vya098fj5krh71zwgghpha3rp21j8sa1fpafrwbbnjy1sy39025") (y #t)))

(define-public crate-terminal-paint-1.0.0 (c (n "terminal-paint") (v "1.0.0") (h "141la3m51br20apr7fwsz0dimgb8gidg7hcq9kdbac0ysymz6rvv")))

