(define-module (crates-io te rm termios) #:use-module (crates-io))

(define-public crate-termios-0.0.1 (c (n "termios") (v "0.0.1") (h "05ssbmgf07gkqjqll1r5d7li0s3r3qwskblzxs2ngfpq1mfwbbsx")))

(define-public crate-termios-0.0.2 (c (n "termios") (v "0.0.2") (h "01zymqd5fyqb3kwgq2xsvs005klg4559g5f4j1ylgrwsvrayafqj")))

(define-public crate-termios-0.0.3 (c (n "termios") (v "0.0.3") (h "1h1aiyfb2hf1m1qfkmwwndd7w6nbjalsf37zh2hf27srynnvwsfq")))

(define-public crate-termios-0.0.4 (c (n "termios") (v "0.0.4") (h "02s59ms4ajd33g004khv2shcggxnfaavn2hq1n3l0a1qwaaaxhqg")))

(define-public crate-termios-0.0.5 (c (n "termios") (v "0.0.5") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "1zn3rb6g5css784rmagfc3778slnaqvp31qk156sg08pfqqil1vm")))

(define-public crate-termios-0.1.0 (c (n "termios") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "1ynqvkgm75vq75f9jb0m54dijsdyf552yd029nkyfbxlxwaj8xqc")))

(define-public crate-termios-0.2.0 (c (n "termios") (v "0.2.0") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "1g1k6343ipb91zp9nf75f01w9wmypa917kzrfy7xi1qs72rg93v0")))

(define-public crate-termios-0.2.1 (c (n "termios") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a1dw11249y1si9yklqmsx3im4i14wcrcw8rf7lnb1zzhq18m1za")))

(define-public crate-termios-0.2.2 (c (n "termios") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fk8nl0rmk43jrh6hjz6c6d83ri7l6fikag6lh0ffz3di9cwznfm")))

(define-public crate-termios-0.3.0 (c (n "termios") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yp2wif18f3bgmh9y1f893n5h5bs4p00gyyrazvjs61dy76nl8kh")))

(define-public crate-termios-0.3.1 (c (n "termios") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09any1p4jp4bphvb5ikagnvwjc3xn2djchy96nkpa782xb2j1dkj")))

(define-public crate-termios-0.3.2 (c (n "termios") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hh0qh03cfibgrg2jmlqph3xzc21dvgb8nqdwifnf9aanbkww3vg")))

(define-public crate-termios-0.3.3 (c (n "termios") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sxcs0g00538jqh5xbdqakkzijadr8nj7zmip0c7jz3k83vmn721")))

