(define-module (crates-io te rm termcandy-macros) #:use-module (crates-io))

(define-public crate-termcandy-macros-0.1.0 (c (n "termcandy-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1zpjn19zpmn9i8fry1ri8x7w1dba77v41y0faf2q86vaf9s41v92")))

