(define-module (crates-io te rm termlight) #:use-module (crates-io))

(define-public crate-termlight-0.2.0 (c (n "termlight") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "0cpsqkbc8yrsqvxnqvbnzbxzpnah65gdjwaxwppdwvyb0z6m7c6q")))

(define-public crate-termlight-1.0.0 (c (n "termlight") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "0lpb5k9dpbjz57b6wh7j7lzmimn6jz33awvn6caq58n91cm9v9na")))

(define-public crate-termlight-1.1.0 (c (n "termlight") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0b9sl9mvbj0n2hvj8blxv36ka7l87mv630lnz0mpphg8amwdj1an")))

