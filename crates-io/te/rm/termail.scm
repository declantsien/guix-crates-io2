(define-module (crates-io te rm termail) #:use-module (crates-io))

(define-public crate-termail-0.1.0 (c (n "termail") (v "0.1.0") (h "0vqn23l97c1x91140jg93zbk36gcgjcsqbvh350vzxrg66rpc37h")))

(define-public crate-termail-0.1.1 (c (n "termail") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maildir") (r "^0.5") (d #t) (k 0)) (d (n "mailparse") (r "^0.13") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "tui-realm-stdlib") (r "^0.6") (d #t) (k 0)) (d (n "tui-realm-treeview") (r "^0.3") (d #t) (k 0)) (d (n "tuirealm") (r "^0.6") (d #t) (k 0)))) (h "1mng77j3w7n5hgwil2vsc61nq65i8kicf4gc1zy6m90nnkcqbl18") (f (quote (("default"))))))

