(define-module (crates-io te rm termdiff) #:use-module (crates-io))

(define-public crate-termdiff-0.1.0 (c (n "termdiff") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (d #t) (k 0)))) (h "1cnkwb1if8m1n1h3ixjw3i0ll4m8zgaxc8v2xshqjrbfcpnqy4sh")))

(define-public crate-termdiff-1.0.0 (c (n "termdiff") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline" "unicode"))) (d #t) (k 0)))) (h "1zs9lbqc0ri8akrgfv2yaqpmn4x5zf8v59dw39y8gry4gais3j4h")))

(define-public crate-termdiff-1.1.0 (c (n "termdiff") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline" "unicode"))) (d #t) (k 0)))) (h "1yja1764cy6hdml14mz6flbllsfdk8v3dizrazwgbk535hinz0g1")))

(define-public crate-termdiff-1.1.1 (c (n "termdiff") (v "1.1.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline"))) (d #t) (k 0)))) (h "1q6ph9zv357rm5ccyp86jy3lf8frjhgvssnyggi0s0fhplmzq802")))

(define-public crate-termdiff-1.2.0 (c (n "termdiff") (v "1.2.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline"))) (d #t) (k 0)))) (h "03k8daw4nd97li69ww3hiwhi2f6mwq12xlns9373c47c101cyrg6")))

(define-public crate-termdiff-1.2.1 (c (n "termdiff") (v "1.2.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline"))) (d #t) (k 0)))) (h "1q314rd560i9np7scpa1g78cdsnhrrcl1yb18b4fyclsdj2kn7rd")))

(define-public crate-termdiff-2.0.0 (c (n "termdiff") (v "2.0.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline"))) (d #t) (k 0)))) (h "1f49hf9jvw0ksqvl97as8yjnb9kpqqd56csmpnshyqc6v6idkac0")))

(define-public crate-termdiff-3.0.0 (c (n "termdiff") (v "3.0.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline"))) (d #t) (k 0)))) (h "1gma878v71njj9ayl2zgvas8vafzxvz1s5n0mwjg45ifnxyvvfhy")))

(define-public crate-termdiff-3.0.1 (c (n "termdiff") (v "3.0.1") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline"))) (d #t) (k 0)))) (h "1n3llb1sw5szf7wv5irqpqxxp81ibvi8pidvnw3zm3midqsxw0jh")))

(define-public crate-termdiff-3.0.2 (c (n "termdiff") (v "3.0.2") (d (list (d (n "crossterm") (r "^0.23.0") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline"))) (d #t) (k 0)))) (h "0rc8522ds3yq3fmvr6g93f0cl4b9w6gn9wrd49xx5cb6aazbdnjk")))

(define-public crate-termdiff-3.1.0 (c (n "termdiff") (v "3.1.0") (d (list (d (n "crossterm") (r "^0.23.0") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline"))) (d #t) (k 0)))) (h "0x7r5p1mfpr8m2wg8c5y54kz4cri4i2sakb2wpl1xgkzvwf5s6rs")))

(define-public crate-termdiff-3.1.1 (c (n "termdiff") (v "3.1.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline"))) (d #t) (k 0)))) (h "0g1wi0g3ds9dxc9758h1ykwv0bkswwm981zbnjmn7bg3nyj9w7s2")))

(define-public crate-termdiff-3.1.2 (c (n "termdiff") (v "3.1.2") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (f (quote ("inline"))) (d #t) (k 0)))) (h "0gdg6faw9q9w85bchsa9anbgayim62gwmfldm2l212lj3xkna487")))

