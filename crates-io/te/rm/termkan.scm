(define-module (crates-io te rm termkan) #:use-module (crates-io))

(define-public crate-termkan-0.1.0 (c (n "termkan") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0x2rldzy991x3g6s8vv2d38njhnhcgznlln0k6yjjahv1cq74gj9")))

(define-public crate-termkan-0.2.0 (c (n "termkan") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0wbjhl42gw0qkbsgvpbzpqkd8s4qmhflrlsa03px3xyyb2bcim75")))

(define-public crate-termkan-0.3.0 (c (n "termkan") (v "0.3.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1lgnnsd293bpfm6bfngy354lhc7jxr55l16874sazra2b6srpc74")))

(define-public crate-termkan-0.3.1 (c (n "termkan") (v "0.3.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1qsq7zn7gkjgr80hv8aax1iwa33lrdr1y2f1rn9a58idyrfc2msf")))

(define-public crate-termkan-0.4.0 (c (n "termkan") (v "0.4.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "0bhlmgxy59baigncranrwva58imkpghgd2dkvvm6p2zwxw0py9f4")))

(define-public crate-termkan-0.4.1 (c (n "termkan") (v "0.4.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1wxys4azdx84jj9d8vy303ny522170izq5jlx30igrfpl6n5ydxz")))

(define-public crate-termkan-0.4.2 (c (n "termkan") (v "0.4.2") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "termios") (r "^0.3") (d #t) (k 0)))) (h "1v91nhcyrv702lkass5x0qwcgr5xngk3z4rh4c1bx5izmxlxxbhv")))

