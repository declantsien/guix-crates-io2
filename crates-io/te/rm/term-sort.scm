(define-module (crates-io te rm term-sort) #:use-module (crates-io))

(define-public crate-term-sort-0.1.0 (c (n "term-sort") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "10isrqd0vr20dv8j9ar0bbxhziijsl9slnacagmy9nmbj1iwi2c2")))

