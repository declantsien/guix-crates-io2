(define-module (crates-io te rm termess) #:use-module (crates-io))

(define-public crate-termess-0.1.0 (c (n "termess") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "enigo") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02qfm77ppcrck8b672vxnid0bqi8wxh7wv9wal3sq9s64ini22rm")))

