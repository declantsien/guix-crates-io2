(define-module (crates-io te rm terminal-log-symbols) #:use-module (crates-io))

(define-public crate-terminal-log-symbols-0.1.0 (c (n "terminal-log-symbols") (v "0.1.0") (d (list (d (n "terminal-emoji") (r "^0.2.5") (d #t) (k 0)))) (h "0657xyfz1lm06j6mn5h6j2770vxrphpivpw89c1dgaz8zwg064hb")))

(define-public crate-terminal-log-symbols-0.1.1 (c (n "terminal-log-symbols") (v "0.1.1") (d (list (d (n "terminal-emoji") (r "^0.3.0") (d #t) (k 0)))) (h "096kr4wxjsqymghxx9xqii9k5wq2jq5b4787dw0y6q0r392mywwi")))

(define-public crate-terminal-log-symbols-0.1.2 (c (n "terminal-log-symbols") (v "0.1.2") (d (list (d (n "ansi-colors-macro") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "terminal-emoji") (r "^0.3.0") (d #t) (k 0)))) (h "0pv5nq813cs683xjv8k6vxs6bg8z9vpn7gmphg3qcsyx9qxpalnp") (f (quote (("default" "colors") ("colors" "ansi-colors-macro"))))))

(define-public crate-terminal-log-symbols-0.1.3 (c (n "terminal-log-symbols") (v "0.1.3") (d (list (d (n "ansi-colors-macro") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "terminal-emoji") (r "^0.3.0") (d #t) (k 0)))) (h "1ymj9b0fx81j2l5gb29c9hhjjahbxajpp9ms7gkbclxbcqkjf54p") (f (quote (("default" "colors") ("colors" "ansi-colors-macro"))))))

(define-public crate-terminal-log-symbols-0.1.4 (c (n "terminal-log-symbols") (v "0.1.4") (d (list (d (n "ansi-colors-macro") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "terminal-emoji") (r "^0.4.0") (d #t) (k 0)))) (h "13dff4c5j2c06142rqbx51lwxy44rz4r9yikpd9azgwxnkbwz5w9") (f (quote (("default" "colors") ("colors" "ansi-colors-macro"))))))

(define-public crate-terminal-log-symbols-0.1.5 (c (n "terminal-log-symbols") (v "0.1.5") (d (list (d (n "ansi-colors-macro") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "terminal-emoji") (r "^0.4.0") (d #t) (k 0)))) (h "0sq4xvxyjnia5jg59wlrpblg2cdwr4ad3xacq39ym99rsiwgj5k2") (f (quote (("default" "colors") ("colors" "ansi-colors-macro"))))))

(define-public crate-terminal-log-symbols-0.1.6 (c (n "terminal-log-symbols") (v "0.1.6") (d (list (d (n "ansi-colors-macro") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "terminal-emoji") (r "^0.4.1") (d #t) (k 0)))) (h "0vz4yw27fscvr4wi2px7zhahcl225ia99l994x505b8bcy396mmp") (f (quote (("default" "colors") ("colors" "ansi-colors-macro"))))))

