(define-module (crates-io te rm termagotchi) #:use-module (crates-io))

(define-public crate-termagotchi-0.1.0 (c (n "termagotchi") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1jmvkp65c9b7krr50bbc5zkpjcd2blw7k7kmjw8v6dxsj9xaq8i7")))

(define-public crate-termagotchi-0.2.0 (c (n "termagotchi") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w7wzwi4mqc8h4jxicjacfnmfcnvxjkl26jypmnmj47jmz84gswj")))

