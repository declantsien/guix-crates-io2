(define-module (crates-io te rm termit-ui) #:use-module (crates-io))

(define-public crate-termit-ui-0.0.1 (c (n "termit-ui") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "termit-ion") (r "^1.5.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "02zf7y4dj1fyz7bgkjfizxkx0zdcqm7zdjd9mn7wl3pxcsscav07") (y #t)))

