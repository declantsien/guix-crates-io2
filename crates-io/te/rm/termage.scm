(define-module (crates-io te rm termage) #:use-module (crates-io))

(define-public crate-termage-1.0.0 (c (n "termage") (v "1.0.0") (d (list (d (n "clap") (r "^2.29.4") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "terminal_graphics") (r "^0.1.3") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "1zr6j39svnpbf5nprwlyzywrmalsbx3cwm8wszkwwqsk9in5hy3q")))

(define-public crate-termage-1.0.1 (c (n "termage") (v "1.0.1") (d (list (d (n "clap") (r "^2.29.4") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "terminal_graphics") (r "^0.1.4") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "1q6ppd2ghmiwf5rykhwh4c3frxjy0n4r7nap7vpi8ajbyaigr47m")))

(define-public crate-termage-1.1.0 (c (n "termage") (v "1.1.0") (d (list (d (n "clap") (r "^2.29.4") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "terminal_graphics") (r "^0.1.4") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "0817kkqfh9n871q5kxzs041idb89wdykf463sp01qqf0kli166sh")))

(define-public crate-termage-1.1.1 (c (n "termage") (v "1.1.1") (d (list (d (n "clap") (r "^2.29.4") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "terminal_graphics") (r "^0.1.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "1ga72cg2l2lq00ixfc6m2pbbc45q3hgl3hhdpnn9nyzhl8fc0ba6")))

