(define-module (crates-io te rm term-detect) #:use-module (crates-io))

(define-public crate-term-detect-0.1.0 (c (n "term-detect") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0jxkg57baywv9h1bzdapmd5x38kmf16wf0ggikz4vngk4q6z60l5") (f (quote (("default" "cache" "cmdtrait") ("cmdtrait")))) (s 2) (e (quote (("cache" "dep:once_cell"))))))

(define-public crate-term-detect-0.1.1 (c (n "term-detect") (v "0.1.1") (d (list (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0acbhhdskx7jpfybwkk7ah17i2cm05lvmcnw8vm2nkhcjwjzq1ri") (f (quote (("default" "cache" "cmdtrait") ("cmdtrait")))) (s 2) (e (quote (("cache" "dep:once_cell"))))))

(define-public crate-term-detect-0.1.2 (c (n "term-detect") (v "0.1.2") (d (list (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0hr87lfl4cvzv400nsk0v353prjmf1g1vinv185zam806frv5xiv") (f (quote (("default" "cache" "cmdtrait") ("cmdtrait")))) (s 2) (e (quote (("cache" "dep:once_cell"))))))

(define-public crate-term-detect-0.1.3 (c (n "term-detect") (v "0.1.3") (d (list (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0gjn9f34gv1qhdqcglff4fwb30n4ynwzs2h68d7lijwyrg807wds") (f (quote (("default" "cache" "cmdtrait") ("cmdtrait")))) (s 2) (e (quote (("cache" "dep:once_cell"))))))

(define-public crate-term-detect-0.1.4 (c (n "term-detect") (v "0.1.4") (d (list (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0hhq9p9qbqwf18hqv3vb97smizgr9cj9jbmlqy5vis11nxavbpvb") (f (quote (("default" "cache" "cmdtrait") ("cmdtrait")))) (s 2) (e (quote (("cache" "dep:once_cell"))))))

(define-public crate-term-detect-0.1.5 (c (n "term-detect") (v "0.1.5") (d (list (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1hi0wd7pbzhfvif0srd1ny9lzd2pwpcsnxsbiwkg5q6ivn75zrg3") (f (quote (("default" "cache" "cmdtrait") ("cmdtrait")))) (s 2) (e (quote (("cache" "dep:once_cell"))))))

(define-public crate-term-detect-0.1.6 (c (n "term-detect") (v "0.1.6") (d (list (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1p1f3ryyqq05l6ifdf6nik9hlcfqglkrlx55jj74j3fzxmwfb2vr") (f (quote (("default" "cache" "cmdtrait") ("cmdtrait")))) (s 2) (e (quote (("cache" "dep:once_cell"))))))

(define-public crate-term-detect-0.1.7 (c (n "term-detect") (v "0.1.7") (d (list (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0dnfhwxrx155nlpgr3kkgzbms93mfz48k3b9vk34jg02z1k4z2ac") (f (quote (("default" "cache" "cmdtrait") ("cmdtrait")))) (s 2) (e (quote (("cache" "dep:once_cell"))))))

