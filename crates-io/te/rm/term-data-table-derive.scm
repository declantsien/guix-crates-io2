(define-module (crates-io te rm term-data-table-derive) #:use-module (crates-io))

(define-public crate-term-data-table-derive-0.1.0 (c (n "term-data-table-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1z41c5vny3cnpz4pb46kqhd6n51ym0fpbxxx7d46blkmvg52mpdf")))

(define-public crate-term-data-table-derive-0.1.1 (c (n "term-data-table-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "03s3cvzbjj3399k6x7xl0fmzci6gmz113678czjlp77qjsz7hsvn")))

