(define-module (crates-io te rm terminal-snake) #:use-module (crates-io))

(define-public crate-terminal-snake-0.1.0 (c (n "terminal-snake") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x4hmprgba6ivdsixxdc4fnv02v5zm1cvkx3ig5hsgm9i7ik7jhi")))

(define-public crate-terminal-snake-0.1.1 (c (n "terminal-snake") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "085c4vcqdb3kq9k5k79gk1w2qx2yzdf75sz97issnfhqhczgyxcb")))

(define-public crate-terminal-snake-0.1.2 (c (n "terminal-snake") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0686rsc589jwwlsmfq8jzg73b2086gsdj25sjni3pmcrp61bfb4i")))

(define-public crate-terminal-snake-0.1.3 (c (n "terminal-snake") (v "0.1.3") (d (list (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07pb2xp5d7wsmb4y9r6imhznjhavasi307p26qsv8c510p9dg1xn")))

(define-public crate-terminal-snake-0.1.4 (c (n "terminal-snake") (v "0.1.4") (d (list (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pz4s69a11l4yflxq9kwmmch85w00p2mypb9x15lsr5j75bshg85")))

