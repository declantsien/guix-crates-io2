(define-module (crates-io te rm term-rs) #:use-module (crates-io))

(define-public crate-term-rs-0.1.0 (c (n "term-rs") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.7.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16") (d #t) (k 0)))) (h "09zlf20s3s91vgpfr8cn14hp2ldym8nn09ycfmv460hl22ixqqg6")))

(define-public crate-term-rs-0.1.1 (c (n "term-rs") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.7.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16") (d #t) (k 0)))) (h "0wq7a3781i97zvq6lnrksfdkzyb1m5s46f5h5mpqgbckzys1lyqg")))

(define-public crate-term-rs-0.1.2 (c (n "term-rs") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.7.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16") (d #t) (k 0)))) (h "1m4x1jbif19r9qlvxznckrvr6wq7s49mjk1i2s9p5ld7wr32zmjv")))

