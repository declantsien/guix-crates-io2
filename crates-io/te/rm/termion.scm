(define-module (crates-io te rm termion) #:use-module (crates-io))

(define-public crate-termion-1.0.0 (c (n "termion") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "0x880vpji2j3157k0mk305anl8zmfpri79byy1amv61dksad1qgy")))

(define-public crate-termion-1.0.1 (c (n "termion") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "0i6kp3f2z47wbmawpydc3bi7ky1vp9z89fhjf2ndynqygi2vb5gj")))

(define-public crate-termion-1.0.2 (c (n "termion") (v "1.0.2") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "1ilzms3isv2hbkbpa0qc833qxcw3fx42scagficqk4vlxxlj2fgb")))

(define-public crate-termion-1.0.3 (c (n "termion") (v "1.0.3") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "1icly27hvylfl6ffi38xq5wr60xf59yidzqzwkh4zn90nfvxxsbb")))

(define-public crate-termion-1.0.4 (c (n "termion") (v "1.0.4") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "0w39fgmz0wlzbz7j3895s3yj2jkb520a69ngpz8p5j8gmi8vv9d5")))

(define-public crate-termion-1.0.5 (c (n "termion") (v "1.0.5") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "102grz5k79cr2j6zyzip8s2va4lgfkhw8fd989gsmgvb85821p48")))

(define-public crate-termion-1.0.6 (c (n "termion") (v "1.0.6") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "0dasz0cwyinlryqmh0mcrnkfvpckgl0vjgqr2q2zmg61siqfrgrv")))

(define-public crate-termion-1.0.7 (c (n "termion") (v "1.0.7") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "12y482difglr1j62ib46q1p75w3b2sq8qcvl9fq92iaa1va4vbpv")))

(define-public crate-termion-1.0.8 (c (n "termion") (v "1.0.8") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "0f9wl6x22mzwiyxk1gddsiqj5jg6xppdagl70xlmcv37w8xr0g3c")))

(define-public crate-termion-1.1.0 (c (n "termion") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "054zvf54h56ray71jdq7lzq7sdl5mcc5b6syfjlgp55j2wyalihn")))

(define-public crate-termion-1.1.1 (c (n "termion") (v "1.1.1") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "0rxqzn440fkgmad6bcl6ymw96p4dj2w4hmpx35z8cmll0spq966f")))

(define-public crate-termion-1.1.2 (c (n "termion") (v "1.1.2") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "0i9yngba4c6v59g20g232zminqv8dwhkzpy79g74fqgdfvrp31mk")))

(define-public crate-termion-1.1.3 (c (n "termion") (v "1.1.3") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "06mplm5a0vm5lvq7qsib3wva50n3y9vpspaq2aads9vsagm5ljnr")))

(define-public crate-termion-1.1.4 (c (n "termion") (v "1.1.4") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "05ql9s941ajz2v11q7r57c5y605biwzk3djhpc5mzcl5vqgdz44s")))

(define-public crate-termion-1.2.0 (c (n "termion") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "1bj01wjipf1g0l8784cal2wrncm57vjav1b890cq05paxichqwbf")))

(define-public crate-termion-1.3.0 (c (n "termion") (v "1.3.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "10c6m4zgdcpjjzxyy43csvyfg21ikchr7646zxrpi37v4dc8chcb")))

(define-public crate-termion-1.4.0 (c (n "termion") (v "1.4.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)))) (h "05cnan20q2rpmp6mpc5rjxvlvi2s8d17igk7ivisi494ln5nf1ci")))

(define-public crate-termion-1.5.0 (c (n "termion") (v "1.5.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "1fnfhx45xsrrblb5badi91mn0wal58c9h6szdmyi5izjs19dgzwa")))

(define-public crate-termion-1.5.1 (c (n "termion") (v "1.5.1") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "15i0x5vcwb8bkd72g1vlafz3qza1g165rpw7pj9gsfdlmbgkp6k8")))

(define-public crate-termion-1.5.2 (c (n "termion") (v "1.5.2") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "numtoa") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "1sldaj4kv62nv1281nf02b5yrd8m00skjarrlp7aqiwdxcx5kq6x")))

(define-public crate-termion-1.5.3 (c (n "termion") (v "1.5.3") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "numtoa") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "0c634rg520zjjfhwnxrc2jbfjz7db0rcpsjs1qici0nyghpv53va")))

(define-public crate-termion-1.5.4 (c (n "termion") (v "1.5.4") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "numtoa") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "1kdjj6cr575v6v30dj03w5p5wqzy861x478sr9yl8yra1iqg73l1")))

(define-public crate-termion-1.5.5 (c (n "termion") (v "1.5.5") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "numtoa") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "01f9787d5nx445bqbj644v38bn0hl2swwjy9baz0dnbqi6fyqb62")))

(define-public crate-termion-1.5.6 (c (n "termion") (v "1.5.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "numtoa") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "0zk023f0zkws358ll399cawvwdnd0wg8wad4g61kz766xbi8aw87")))

(define-public crate-termion-1.6.0 (c (n "termion") (v "1.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "numtoa") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0km6lcdavp1hxdagd35z59db36vwd48zfwb00bgax9v94law3mv2") (y #t)))

(define-public crate-termion-2.0.0 (c (n "termion") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "numtoa") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xg76nxwhzfamgwkbl0snjxz159c9k781gbsgm9h0pc2s6ysh8wk")))

(define-public crate-termion-2.0.1 (c (n "termion") (v "2.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "numtoa") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "147c0a9l2dj4l8xhd7bb1f0f611lv6k0szacx3jwf21lkwviz735")))

(define-public crate-termion-2.0.2 (c (n "termion") (v "2.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "libredox") (r "^0.0.1") (f (quote ("call"))) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "numtoa") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1k8wfgrqpdiv058md16asavafhgvmasd5bvm8qbahkmdv389n3mj")))

(define-public crate-termion-2.0.3 (c (n "termion") (v "2.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "libredox") (r "^0.0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "numtoa") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_termios") (r "^0.1.3") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zcxsj2mms9hv8gizg2x3358ibjynzwvj5w6asr4683gxxyqqr64")))

(define-public crate-termion-3.0.0 (c (n "termion") (v "3.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "libredox") (r "^0.0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "numtoa") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_termios") (r "^0.1.3") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19f7q542ih13j13jp5lr750z5yav7v035pmz46zznkahb9ki6y21")))

(define-public crate-termion-4.0.0 (c (n "termion") (v "4.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "libredox") (r "^0.0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "numtoa") (r "^0.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_termios") (r "^0.1.3") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0yi5j1j7nr3gjnk2md9qb7r2zk143rmmln2m6gwrlns6hf8zmwvs")))

