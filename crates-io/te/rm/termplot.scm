(define-module (crates-io te rm termplot) #:use-module (crates-io))

(define-public crate-termplot-0.1.0 (c (n "termplot") (v "0.1.0") (d (list (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vdbzbsk5yv3j75wn1gryg7hisfp57i2wzc2mk7smicnf3ah3qnh")))

(define-public crate-termplot-0.1.1 (c (n "termplot") (v "0.1.1") (d (list (d (n "drawille") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "063nv2md3249xs01n2b140i326injnns1b2hg8k1c71h03im46m7")))

