(define-module (crates-io te rm termit-ion) #:use-module (crates-io))

(define-public crate-termit-ion-1.5.2 (c (n "termit-ion") (v "1.5.2") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "numtoa") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "00dbr52arg011zysa1m8c9nch9nafl367pb3j79711bjyjxbkm1b") (y #t)))

(define-public crate-termit-ion-1.6.0 (c (n "termit-ion") (v "1.6.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "numtoa") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1rzm0pg37fvywzi6jgd9p8af3b2m86rhq9p5s60nnsxpxx682anr") (y #t)))

