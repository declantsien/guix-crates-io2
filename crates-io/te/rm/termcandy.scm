(define-module (crates-io te rm termcandy) #:use-module (crates-io))

(define-public crate-termcandy-0.1.0 (c (n "termcandy") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "mio") (r "^0.6.16") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)) (d (n "termcandy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio-signal") (r "^0.2.7") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "13p82qfw54vcl5arwm8h118lg3k5nzi4n2qnqspb94ksxwmynpal")))

