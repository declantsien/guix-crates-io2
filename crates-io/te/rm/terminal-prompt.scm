(define-module (crates-io te rm terminal-prompt) #:use-module (crates-io))

(define-public crate-terminal-prompt-0.1.0 (c (n "terminal-prompt") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13bd6g7vrxnmaglnwgy0r0spkasbdy7xnkdhzic4pfkqlsyk2vdj")))

(define-public crate-terminal-prompt-0.2.0 (c (n "terminal-prompt") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1g5r4c6q5zd6sy62xq72bb7v2b1g1bcnmhaiqb0w5jdmxg6nglvd")))

(define-public crate-terminal-prompt-0.2.1 (c (n "terminal-prompt") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lfz5kb15mknmdxcsn3jma07i0600da0cdwivqxkapg9max73fgn")))

(define-public crate-terminal-prompt-0.2.2 (c (n "terminal-prompt") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0y8k66ql8pibsm7p8m2wy3rk351wfbppfcl6b2jjlwdpd26hpz25")))

(define-public crate-terminal-prompt-0.2.3 (c (n "terminal-prompt") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("consoleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xk3y4m2qysa8sxb46h79f9qxh8m6x0s6ipzbnysq4198yriha2p") (r "1.66")))

