(define-module (crates-io te rm terminal-emoji) #:use-module (crates-io))

(define-public crate-terminal-emoji-0.1.0 (c (n "terminal-emoji") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1jzq04j5n49dz10km040y3v2xv2b8mzh6aipiz7cmbjxax76wavk") (y #t)))

(define-public crate-terminal-emoji-0.2.0 (c (n "terminal-emoji") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0sdmkbg7b317rqi2ssj8kfhmzkq31xfb4v3y67bxlvnsp379m9xg") (y #t)))

(define-public crate-terminal-emoji-0.2.1 (c (n "terminal-emoji") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "032nwx0fl5m0pk7y09kfv4fqdnfii2bj9v136rr87q0spky76w9k") (y #t)))

(define-public crate-terminal-emoji-0.2.2 (c (n "terminal-emoji") (v "0.2.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1adxmig88jx1ppba6hyy0f0i2shz9d5yz24189a3pxjch1b8cdpj") (y #t)))

(define-public crate-terminal-emoji-0.2.3 (c (n "terminal-emoji") (v "0.2.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0qlfdh77dcfn4wv96w813hfzr72wan7n2nsq27jk39j7hhrkvx4x") (y #t)))

(define-public crate-terminal-emoji-0.2.4 (c (n "terminal-emoji") (v "0.2.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01cik7xp0r3723w18fj27zhk5286nin9lzx8i7z95h5n0jy12344")))

(define-public crate-terminal-emoji-0.2.5 (c (n "terminal-emoji") (v "0.2.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j2ljsl4w3352y18hfcb2lnin0ramsyz7kypqkifqp0syr9dymfi")))

(define-public crate-terminal-emoji-0.3.0 (c (n "terminal-emoji") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i270926b3347p3gz10nygqsvps1kvd0rg2z8v7fl30hbpyj76ab")))

(define-public crate-terminal-emoji-0.4.0 (c (n "terminal-emoji") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "terminal-supports-emoji") (r "^0.1.0") (d #t) (k 0)))) (h "1m7yc1pwanymkiwynhjycblqn0536fjy2pyar1khjisnkcx842y7") (y #t)))

(define-public crate-terminal-emoji-0.4.1 (c (n "terminal-emoji") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "terminal-supports-emoji") (r "^0.1.3") (d #t) (k 0)))) (h "0w2ilma4rvvy38j15p7c9fg5qsjm3aqqpavkynwb6w2jin75chw1")))

