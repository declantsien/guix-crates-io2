(define-module (crates-io te rm termion_temporary_zellij_fork) #:use-module (crates-io))

(define-public crate-termion_temporary_zellij_fork-1.6.0 (c (n "termion_temporary_zellij_fork") (v "1.6.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (t "cfg(not(target_os = \"redox\"))") (k 0)) (d (n "numtoa") (r "^0.1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "redox_termios") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1l5yjw8k0a6ljsyvg21rqhz29h0iqhm2m2xwj3b74zvj07xml5v5")))

