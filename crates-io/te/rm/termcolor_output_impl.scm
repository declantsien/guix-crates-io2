(define-module (crates-io te rm termcolor_output_impl) #:use-module (crates-io))

(define-public crate-termcolor_output_impl-1.0.0-rc.1 (c (n "termcolor_output_impl") (v "1.0.0-rc.1") (h "1qxl7an79sbryvrfkn0wfppdszbdcd95scfifslldhm3ma8j4qf3")))

(define-public crate-termcolor_output_impl-1.0.0-rc.2 (c (n "termcolor_output_impl") (v "1.0.0-rc.2") (h "1pcpamkcq18mapf3zsysra5lg5cbfiq7v1sksgl6kag7grb9ign3")))

(define-public crate-termcolor_output_impl-1.0.0-rc.3 (c (n "termcolor_output_impl") (v "1.0.0-rc.3") (h "1ickp4fddlszhid9lk8inkq5phw3bvbaqp2qz1nvc0s74wk5ldrp")))

(define-public crate-termcolor_output_impl-1.0.0 (c (n "termcolor_output_impl") (v "1.0.0") (h "1hc63w41vkpmallh2ckvpniw1nxv27dziprbnii3gss1p05xwkgk")))

