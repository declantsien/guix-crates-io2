(define-module (crates-io te rm term_rewriting) #:use-module (crates-io))

(define-public crate-term_rewriting-0.1.0 (c (n "term_rewriting") (v "0.1.0") (d (list (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "0g3c30z73a7mfpq4ivg6dzgjqv79vx8wphpmi8ccy2h3z0bpgzj0")))

(define-public crate-term_rewriting-0.2.0 (c (n "term_rewriting") (v "0.2.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "13i6gdc141hqwjng9llkj5qbj2ww2m4i1n8n99f1sbwqgrr8q9aq")))

(define-public crate-term_rewriting-0.3.0 (c (n "term_rewriting") (v "0.3.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0spw4i06dxdnsx8iklgcjmsw9kqybxm7n95chfhhhv288xladq45")))

(define-public crate-term_rewriting-0.4.0 (c (n "term_rewriting") (v "0.4.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "08xsh49qgl2h686hdi6nmnbjfvcqz7ava1kf8qsqq4b48pxs152f")))

(define-public crate-term_rewriting-0.5.0 (c (n "term_rewriting") (v "0.5.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1l3wv5wwcj158vdfzxfj1rb9j2qcrxsqa9lxs593v7wfwjb6a1f5")))

(define-public crate-term_rewriting-0.6.0 (c (n "term_rewriting") (v "0.6.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1kqq3pgqnabml4lavq1pq2s9qivg290jabljfgvz0p5q1jhvl2i7")))

(define-public crate-term_rewriting-0.7.0 (c (n "term_rewriting") (v "0.7.0") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1jccxy166wc7hdb31pl8wb96hxr1mmq7znmk6p0ckhknf1fgpk92")))

