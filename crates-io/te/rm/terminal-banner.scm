(define-module (crates-io te rm terminal-banner) #:use-module (crates-io))

(define-public crate-terminal-banner-0.1.0 (c (n "terminal-banner") (v "0.1.0") (d (list (d (n "textwrap") (r "^0.15") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "02z284wvl968sn4gg6qbhnvhabpiaspx9zkd9craf74wzp1d960b")))

(define-public crate-terminal-banner-0.2.0 (c (n "terminal-banner") (v "0.2.0") (d (list (d (n "textwrap") (r "^0.15") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1ks6k1nwiy390gqx5lyad73d4f1k7ch8z7xg1y4j5v8z7i6kb1hw")))

(define-public crate-terminal-banner-0.3.0 (c (n "terminal-banner") (v "0.3.0") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "18i6affx9fhhlsg0jl71hbhsd7kv2bxpv4k6vbc9zliq910ky9f2") (f (quote (("color" "colored"))))))

(define-public crate-terminal-banner-0.4.0 (c (n "terminal-banner") (v "0.4.0") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "02y3dwj8prjsg1wjxg17j65j7rp99raibp2ak4s721yi07v55ip6") (f (quote (("color" "colored"))))))

(define-public crate-terminal-banner-0.4.1 (c (n "terminal-banner") (v "0.4.1") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0llakk7rl8mgrr59gpmr17spmv1kpb4686kdvj2fgakr5fv8gwpm") (f (quote (("color" "colored"))))))

