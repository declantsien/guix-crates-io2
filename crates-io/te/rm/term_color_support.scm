(define-module (crates-io te rm term_color_support) #:use-module (crates-io))

(define-public crate-term_color_support-0.1.0 (c (n "term_color_support") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "os_info") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0b2l0p61fkncvmp4rhsdb6fwwj3icsisrss0jjvrxy884c85sx72")))

(define-public crate-term_color_support-0.1.1 (c (n "term_color_support") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "os_info") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "10mkljc8d87lzjy1qyz9sw95wjg8dcisakms8fcgcglzh6rmsmx1")))

