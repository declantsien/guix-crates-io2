(define-module (crates-io te lb telbot-ureq) #:use-module (crates-io))

(define-public crate-telbot-ureq-0.1.0 (c (n "telbot-ureq") (v "0.1.0") (d (list (d (n "multipart") (r "^0.18.0") (f (quote ("client"))) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "telbot-types") (r "^0.1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1n1x52p9n4k9waa4jgb1q4xx8sxpys2r4kzf9avbf0wx3x7zyqhh")))

(define-public crate-telbot-ureq-0.2.0 (c (n "telbot-ureq") (v "0.2.0") (d (list (d (n "multipart") (r "^0.18.0") (f (quote ("client"))) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "telbot-types") (r "^0.2.0") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0p9flkqakiaxn626061yg3md4va7k2gq5fk2y3k5rg0cg14mf49r")))

(define-public crate-telbot-ureq-0.3.0 (c (n "telbot-ureq") (v "0.3.0") (d (list (d (n "multipart") (r "^0.18.0") (f (quote ("client"))) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "telbot-types") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "13mgcycynqpsidsfrzmxdy3y013yd5idgdz904ckyk5xczkziin4")))

(define-public crate-telbot-ureq-0.3.1 (c (n "telbot-ureq") (v "0.3.1") (d (list (d (n "multipart") (r "^0.18.0") (f (quote ("client"))) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "telbot-types") (r "^0.3.0") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "154d198x2w3yarrpdbr3fcw4bnmfkn3jgf6ma637h9fxknwi1q00")))

