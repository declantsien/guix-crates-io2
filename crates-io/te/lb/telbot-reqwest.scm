(define-module (crates-io te lb telbot-reqwest) #:use-module (crates-io))

(define-public crate-telbot-reqwest-0.1.0 (c (n "telbot-reqwest") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.6") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.70") (d #t) (k 0)) (d (n "telbot-types") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0d3dix4wvvl4hlvwkm4fizidl8246g3bw6hl6aab6mfb9kc6vccd")))

