(define-module (crates-io te lb telbot-cf-worker) #:use-module (crates-io))

(define-public crate-telbot-cf-worker-0.1.0 (c (n "telbot-cf-worker") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (k 0)) (d (n "multipart") (r "^0.18.0") (f (quote ("client"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "telbot-types") (r "^0.1.0") (d #t) (k 0)) (d (n "worker") (r "^0.0.7") (d #t) (k 0)))) (h "0lr8mq5j1aldsgcqdqsdss9fxv2fyhrz53xpv1wfm1irnyvncj1v")))

(define-public crate-telbot-cf-worker-0.2.0 (c (n "telbot-cf-worker") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (k 0)) (d (n "multipart") (r "^0.18.0") (f (quote ("client"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "telbot-types") (r "^0.2.0") (d #t) (k 0)) (d (n "worker") (r "^0.0.7") (d #t) (k 0)))) (h "1awgki5wh96v3sh2xaw5qrjn2300famjww5scg4di92qd38d1jqm")))

(define-public crate-telbot-cf-worker-0.3.0 (c (n "telbot-cf-worker") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (k 0)) (d (n "multipart") (r "^0.18.0") (f (quote ("client"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "telbot-types") (r "^0.3.0") (d #t) (k 0)) (d (n "worker") (r "^0.0.7") (d #t) (k 0)))) (h "0kppw8hycv7prw58yx39f50apld2lawfmg7mj4dz4c2b1prsrp47")))

