(define-module (crates-io te lb telbot-types) #:use-module (crates-io))

(define-public crate-telbot-types-0.1.0 (c (n "telbot-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y02z26rr8mx0ka6bwhq1a530hmqcwrn2vspfh3hn14aygcdyfmh")))

(define-public crate-telbot-types-0.2.0 (c (n "telbot-types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ayxhvw29jw2xdbcgkrc4i7p14kd9cjrh5cim60g7w77v2yc7dsf")))

(define-public crate-telbot-types-0.3.0 (c (n "telbot-types") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "117d759w5d4sbgjhxaabwws8mlpfxnz84sc54gf6yd4r3n3lbnja")))

