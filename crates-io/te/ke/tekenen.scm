(define-module (crates-io te ke tekenen) #:use-module (crates-io))

(define-public crate-tekenen-0.0.1 (c (n "tekenen") (v "0.0.1") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (o #t) (d #t) (k 0)))) (h "159l532blvhwr5isnjw7vnl06acvjlm3ascs98wfqlksirjsw08w") (f (quote (("sdl" "sdl2") ("default" "sdl"))))))

(define-public crate-tekenen-0.0.2 (c (n "tekenen") (v "0.0.2") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^6.8.1") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (o #t) (d #t) (k 0)))) (h "0h51jq6fzdlm9vgqmqx168xgqlqhhclzh9w3f57d0q87w6aj9nhg") (f (quote (("sdl" "sdl2") ("preloader" "image") ("default" "sdl"))))))

(define-public crate-tekenen-0.0.3 (c (n "tekenen") (v "0.0.3") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^6.8.1") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (o #t) (d #t) (k 0)))) (h "0xss40n2n53xg13sdk9prs3gh77y171k4qpaijy7w30r92dy5ikm") (f (quote (("sdl" "sdl2") ("preloader" "image") ("default" "sdl"))))))

(define-public crate-tekenen-0.0.4 (c (n "tekenen") (v "0.0.4") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^6.8.1") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (o #t) (d #t) (k 0)))) (h "1rk6qq3wrf93kc0m7scp3v8mqrm6lgx2ir0m7chlav6bx38amqqa") (f (quote (("sdl" "sdl2") ("preloader" "image") ("default" "sdl"))))))

(define-public crate-tekenen-0.0.5 (c (n "tekenen") (v "0.0.5") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^6.8.1") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (o #t) (d #t) (k 0)))) (h "19lmbnk3lz2fiz4g375nys067f5y99cswb5pd0zxflmzp3l68g6y") (f (quote (("sdl" "sdl2") ("preloader" "image") ("default" "sdl"))))))

