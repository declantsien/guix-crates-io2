(define-module (crates-io te rn ternary-tree-wasm) #:use-module (crates-io))

(define-public crate-ternary-tree-wasm-0.0.1 (c (n "ternary-tree-wasm") (v "0.0.1") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "ternary-tree") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)))) (h "09b1x52n4g4s3blm9fyrbg84lqi8sjlyrlxs774z4lv6960bsanx")))

(define-public crate-ternary-tree-wasm-0.0.2 (c (n "ternary-tree-wasm") (v "0.0.2") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "ternary-tree") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)))) (h "03v181wr8yk5lnwqmmg7nz66j29v675jm9m4lk789r6m9nk8i0g8")))

(define-public crate-ternary-tree-wasm-0.0.3 (c (n "ternary-tree-wasm") (v "0.0.3") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "ternary-tree") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)))) (h "0kxydxmx1f8ykx6fvi8c6qcba9mfi7dsg6ln0fq1i0qn9ljmbyi5")))

(define-public crate-ternary-tree-wasm-0.0.4 (c (n "ternary-tree-wasm") (v "0.0.4") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "ternary-tree") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)))) (h "04bkrg1k5bafi22p9kck75cbzak16mmc4rv777plp3717pnj4vhn")))

(define-public crate-ternary-tree-wasm-0.0.5 (c (n "ternary-tree-wasm") (v "0.0.5") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "ternary-tree") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)))) (h "085ngfbd5kazpyfq34wpakr1rm7xbjw3ph0njgl8fpvncvgkviin")))

(define-public crate-ternary-tree-wasm-0.0.6 (c (n "ternary-tree-wasm") (v "0.0.6") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "ternary-tree") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)))) (h "1dl8xs2a8g3jxa68kfmlsvfq35jsd0mqfnxbfnwrffkk3lz44hz7")))

