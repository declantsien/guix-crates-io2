(define-module (crates-io te rn terny) #:use-module (crates-io))

(define-public crate-terny-0.1.0 (c (n "terny") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0ks94d86yh40pgflppkvjla7ddg0dqhg82i7s8y6qpqbd75qrckz")))

(define-public crate-terny-0.2.0 (c (n "terny") (v "0.2.0") (h "1phms73nbly000v88sxrg6bgwn7mlm57x0bk16mh4hclp1z8r8ff")))

