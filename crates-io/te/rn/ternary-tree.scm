(define-module (crates-io te rn ternary-tree) #:use-module (crates-io))

(define-public crate-ternary-tree-0.0.1 (c (n "ternary-tree") (v "0.0.1") (h "1ysy3pbr7p39gnw03byvkz3zm98l8jbyfjiyyfcya3vh86nrzzbq")))

(define-public crate-ternary-tree-0.0.2 (c (n "ternary-tree") (v "0.0.2") (h "19crx4gafsx2fjjr4fjcyi9jpr6c3ywwwi62fj8qav7bdp15cblf")))

(define-public crate-ternary-tree-0.0.3 (c (n "ternary-tree") (v "0.0.3") (h "0ibgc8kdsnqz9d1w5k6k12zi8gi7f072sj3mq7xxs8x0igq2zm2v")))

(define-public crate-ternary-tree-0.0.4 (c (n "ternary-tree") (v "0.0.4") (h "1rk6cm8177k51cjn4mqxf18r1jz25iwk48vympj4q8fgkl29jyal")))

(define-public crate-ternary-tree-0.0.5 (c (n "ternary-tree") (v "0.0.5") (h "1q338py1ly4b3dj1dzaqwn02d1cxkaazypr2yc5mr06wqf7mr6c5")))

(define-public crate-ternary-tree-0.0.6 (c (n "ternary-tree") (v "0.0.6") (h "1lmzfykfn3ki146xbic1bqv57dxl9sf84lgzskdffch2fbrvavb9")))

(define-public crate-ternary-tree-0.1.0 (c (n "ternary-tree") (v "0.1.0") (h "0xi2al9w4ii9d22ywqkalwr56687j1sjadq6gd5zg02698x1rywi")))

(define-public crate-ternary-tree-0.1.1 (c (n "ternary-tree") (v "0.1.1") (h "1fviyx610bs2cm7h8zdfrw9k50v578vxzv81hgz3azp91bga52k8")))

