(define-module (crates-io te rn ternary) #:use-module (crates-io))

(define-public crate-ternary-0.1.0 (c (n "ternary") (v "0.1.0") (h "0h0pnfs3m80j9cixxaglbzk760pis1xxjvgx9v3d0psp8iplbvpl")))

(define-public crate-ternary-0.1.2 (c (n "ternary") (v "0.1.2") (h "1zv7cfaj8kvx71w56zz21ywvgy7xz75xp48aa9y7bngxh61s3kh2") (f (quote (("unstable"))))))

(define-public crate-ternary-0.1.3 (c (n "ternary") (v "0.1.3") (h "04fvi3bkjlxwgcsk8f71mg6l08w3w0ja2r961n33pjfbqyvmcims") (f (quote (("unstable"))))))

