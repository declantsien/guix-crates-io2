(define-module (crates-io te li telium) #:use-module (crates-io))

(define-public crate-telium-0.1.0 (c (n "telium") (v "0.1.0") (d (list (d (n "image") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0nhsxf1slqzjqj41adik84a87g49swpdiaik3sqw0bbcbn5a2y77") (y #t)))

(define-public crate-telium-0.1.1 (c (n "telium") (v "0.1.1") (d (list (d (n "image") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1fhl71kxsc15si3jmlhi8zpknyp3gfbprz66i0vlizhbng5zzizz") (y #t)))

(define-public crate-telium-0.0.1 (c (n "telium") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0vnmr3giqwwyw260b0n4k0r1qc1mqr0azx2q57z7bf0glkl7g756")))

(define-public crate-telium-0.1.2 (c (n "telium") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "05nvrmfn4001m29c35d8dch2h9ppa3qncxxz486pkvybcayr993g")))

