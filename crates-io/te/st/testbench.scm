(define-module (crates-io te st testbench) #:use-module (crates-io))

(define-public crate-testbench-0.1.0 (c (n "testbench") (v "0.1.0") (h "0ns0vdyncyvxcid866mxlg642bfvy6z3bpmxcvyvbvz0qf0hm79i")))

(define-public crate-testbench-0.1.1 (c (n "testbench") (v "0.1.1") (h "01vcfc85c89ycd5v95h9xijqd2srg3m8hc82jxm21wsszd228zb9")))

(define-public crate-testbench-0.2.0 (c (n "testbench") (v "0.2.0") (h "10awnnc52xan34xa3x49w8ifbrgcdpcjjzfcypqis1kk8rmviy16")))

(define-public crate-testbench-0.3.0 (c (n "testbench") (v "0.3.0") (h "0c936kx7grbz47ab0rm1smcrjjwvk2hn2xggi08fjy8rzzw1c6ns")))

(define-public crate-testbench-0.3.1 (c (n "testbench") (v "0.3.1") (h "0rdk7dbbdw2sm82nh832lzyd70y5bj2hzqg99g2x0hfimv5psvi1")))

(define-public crate-testbench-0.4.0 (c (n "testbench") (v "0.4.0") (h "073ixk20jg4vdj2v5af93xdwgnfp9iskpliw4l865zdn3r8g9g6z")))

(define-public crate-testbench-0.4.1 (c (n "testbench") (v "0.4.1") (h "01l5bbwn5ykl09jjjdndg279b13y6i9817nab47k23bpmc9lvwmx")))

(define-public crate-testbench-0.5.0 (c (n "testbench") (v "0.5.0") (h "19b6bii1lf9br7gvfxvys2c6mahclfgw2rj26imia4yri8x8pv2j")))

(define-public crate-testbench-0.6.0 (c (n "testbench") (v "0.6.0") (h "1pnx0fzdv2kqvvfhqkls0hsfvmpj5jbl8s7dfs6900lhr7y50xpf")))

(define-public crate-testbench-0.7.0 (c (n "testbench") (v "0.7.0") (h "1zyzhbykjbi6sm701400mqgrvxwb7yz7vsfmxvbnmi8d8ibcxbib")))

(define-public crate-testbench-0.7.1 (c (n "testbench") (v "0.7.1") (h "0p0hjhyb8qyd38nz0wh2v8lixcyybpvysyp4g9p23m4mn33vv89l")))

(define-public crate-testbench-0.7.2 (c (n "testbench") (v "0.7.2") (h "1lpxhl0gcs1jq0dqdvv21pcrscijwmwil69305ih55vxbnl6b30s")))

(define-public crate-testbench-0.7.3 (c (n "testbench") (v "0.7.3") (h "0iwspwiw1a9ih2cfnknb6zfbgdbrz41zz9d5cd36s0vh1kpzyc6y")))

(define-public crate-testbench-0.8.0 (c (n "testbench") (v "0.8.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "126pg9xfmcwyg0a13v5qipkfw8lramkh2n8847cgpgwxqk1fnjln")))

(define-public crate-testbench-0.8.1 (c (n "testbench") (v "0.8.1") (d (list (d (n "crossbeam-utils") (r "<0.8.11") (d #t) (k 0)))) (h "1hc61rckv1n3562z2zfr6ar5i2mrxrsaqw33sk9f4ajax6w0ci65") (r "1.36.0")))

(define-public crate-testbench-0.9.0 (c (n "testbench") (v "0.9.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "1im2iwvvrazwwbq8a47ngzbsimapi7vbikiww6h73ddvfbcwbidg") (r "1.38.0")))

(define-public crate-testbench-1.0.0 (c (n "testbench") (v "1.0.0") (h "1qwlqbm0yh8qjapvj8xdghjsylibvwkwqv5l1d98qkl1wyhawhkg") (r "1.63.0")))

