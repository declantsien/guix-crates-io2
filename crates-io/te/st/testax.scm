(define-module (crates-io te st testax) #:use-module (crates-io))

(define-public crate-testax-0.1.0 (c (n "testax") (v "0.1.0") (d (list (d (n "actix-http") (r "^1.0") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (d #t) (k 0)) (d (n "actix-web-httpauth") (r "^0.4") (d #t) (k 0)))) (h "1ar9wz7xar4lbz4ffmw2qh62qslvk7n8a0vfp87mkyra44711dh7")))

(define-public crate-testax-0.1.1 (c (n "testax") (v "0.1.1") (d (list (d (n "actix-http") (r "^1.0") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (d #t) (k 0)) (d (n "actix-web-httpauth") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c7w4jzi2vamafn2w3xv787b0wy2cav66vyzsg12p8r7m7w3dlnc")))

(define-public crate-testax-0.1.2 (c (n "testax") (v "0.1.2") (d (list (d (n "actix-http") (r "^1.0") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (d #t) (k 0)) (d (n "actix-web-httpauth") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "058mdxkvfp6jzz83v5fsb5h9gddwqrya1qkcfh2hbkgcrny0w0cf")))

(define-public crate-testax-0.2.0 (c (n "testax") (v "0.2.0") (d (list (d (n "actix-http") (r "^1.0") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (d #t) (k 0)) (d (n "actix-web-httpauth") (r "^0.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15kskbk7zx5cg8zlv7yvb2rqxksbmrg2h581bc1mrcicqsyi5hf3")))

(define-public crate-testax-0.3.0 (c (n "testax") (v "0.3.0") (d (list (d (n "actix-http") (r "^2.0") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 2)) (d (n "actix-service") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.0") (k 0)) (d (n "actix-web-httpauth") (r "^0.5") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08y09l4cfkqfq7pk3lmf45kyhmxnd0wg7p8g418kxs71a88hdj4l")))

