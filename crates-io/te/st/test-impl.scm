(define-module (crates-io te st test-impl) #:use-module (crates-io))

(define-public crate-test-impl-0.1.0 (c (n "test-impl") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k42sdz2j6qnryhzdi3mbvmpip7a74jkhb0mm22df4lrrpa8ijmr")))

(define-public crate-test-impl-0.2.0 (c (n "test-impl") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ngmnsjynrjvd2innxggyj6klrff0s9n2n60xrzm4mjc5sm9lmrm")))

