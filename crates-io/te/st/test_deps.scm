(define-module (crates-io te st test_deps) #:use-module (crates-io))

(define-public crate-test_deps-0.1.0 (c (n "test_deps") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "test_deps_if") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0iz3vviwli8iib567gaq9ci0qn18d8mxw0arr4xibpykxnvfigx3")))

