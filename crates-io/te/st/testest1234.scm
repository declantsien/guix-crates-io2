(define-module (crates-io te st testest1234) #:use-module (crates-io))

(define-public crate-testest1234-0.1.0 (c (n "testest1234") (v "0.1.0") (h "0vfgavmjyf3hnaybx8k96a3djdj66hmjal6nymjrqzav70qb0cg4") (y #t)))

(define-public crate-testest1234-0.2.0 (c (n "testest1234") (v "0.2.0") (h "0jqjxmb8kc3p6lxr95hdqk1361n9fsh114c0cvcfphpnf6wygrvx") (y #t)))

