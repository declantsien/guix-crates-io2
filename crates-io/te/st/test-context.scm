(define-module (crates-io te st test-context) #:use-module (crates-io))

(define-public crate-test-context-0.1.0 (c (n "test-context") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "test-context-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0j7lz93afj1igxll0vjm2190vfy0ghl5x4k6fcwpb6qgyz2gibxc")))

(define-public crate-test-context-0.1.1 (c (n "test-context") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 2)) (d (n "test-context-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1l81ia300avapn90wwywdpv644nj6y5gkbqw701l6g4021wmj3hf")))

(define-public crate-test-context-0.1.2 (c (n "test-context") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "test-context-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1y62mnpfnb700jzwk9wfiqfgcs3vxg7i47f2nlx9p1889znlc26v")))

(define-public crate-test-context-0.1.3 (c (n "test-context") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "test-context-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "18xz19y9rkw7akc3khaf74ysgj2327hw243gwk82wv3ixi50c4yq")))

(define-public crate-test-context-0.1.4 (c (n "test-context") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "test-context-macros") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1hyv63pp36ympdgaqn5r466fnws20412jzz6xn7s4njg5ah32n05")))

(define-public crate-test-context-0.1.5 (c (n "test-context") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "test-context-macros") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "08v9j6a9wf4bk1rd2hn28j5r1b4qambqbka2x4vdw4bn92lcr3gs") (y #t)))

(define-public crate-test-context-0.1.6 (c (n "test-context") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "test-context-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1spfb0j2f9ck42xvl3cnqg7hgrhg0cc3q8wj02pqc89345f9ddmp")))

(define-public crate-test-context-0.2.0 (c (n "test-context") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "test-context-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0iirrkxsizfaip3a408iw6dwpg4gfw62qd9cf9klfy33ihm7blc5") (r "1.75.0")))

(define-public crate-test-context-0.3.0 (c (n "test-context") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "test-context-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1n85dfq26w1zr5ga4p1hk28aqp5lzl1i2qh8l40jdzgd2f2snxk6") (r "1.75.0")))

