(define-module (crates-io te st testcat) #:use-module (crates-io))

(define-public crate-testcat-0.1.0 (c (n "testcat") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("derive" "printing" "parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1yaygcna8sa988nra11m7j1m3nplmmbjgqw64yjdh72zgk96g73f")))

(define-public crate-testcat-0.1.1 (c (n "testcat") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "0zjrbps9n3n99dqxa8x01iry1vfqgi7ws1ipr15qwgbqpp571rqd")))

(define-public crate-testcat-0.2.0 (c (n "testcat") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "1kl3ljxjmmdisdiz4c3q3zd8vjp47h11mlf9vhjirkjlvkh83ci1")))

(define-public crate-testcat-1.0.0 (c (n "testcat") (v "1.0.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "19c5f74p9v3skqwfy42asninv6fb932jjngxilcq114hipvlraxi")))

(define-public crate-testcat-1.1.0 (c (n "testcat") (v "1.1.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "022n2qzs6m7q2qg0vgbs4dv5csbr0pmhmxq8niv5sbv1f31i8xr3")))

(define-public crate-testcat-1.1.1 (c (n "testcat") (v "1.1.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "1h2r8xfw1z0d54slm53pr1l0gqvhc7aa6zsfwq39hfjwvq38g59q")))

(define-public crate-testcat-1.1.2 (c (n "testcat") (v "1.1.2") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "1z8s2nl0py0l9a5ajr6nvjpwvkmxbjdixr9j9daqly5wkp73zbm8")))

(define-public crate-testcat-2.0.0 (c (n "testcat") (v "2.0.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0isl91vrb90r6hpiij35gb1i6inb3mnj62q9p63im0967l4nrz1v")))

(define-public crate-testcat-2.0.1 (c (n "testcat") (v "2.0.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("parsing"))) (d #t) (k 0)))) (h "19gnx4i8xbxcnvi4b4x1vqwvvg467z26wh09vmz2pcv9iqlik3f6")))

(define-public crate-testcat-2.1.0 (c (n "testcat") (v "2.1.0") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1rkxpkc2xnvpkmq3n1d6qx0vhf7v750w1674pjd9f6lb8wsf3dxd")))

