(define-module (crates-io te st test-assets) #:use-module (crates-io))

(define-public crate-test-assets-0.1.0 (c (n "test-assets") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.1") (d #t) (k 0)))) (h "0d8vk1mv3xx9ji1lsia74v0x6fpgcb6l5d52n66qvkpyqxqm7v8m")))

(define-public crate-test-assets-0.2.0 (c (n "test-assets") (v "0.2.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.1") (d #t) (k 0)))) (h "0n9994y4r7j620nn02wx34vsgxkw6fjdf70wi897ql0h0mkqsbfw")))

