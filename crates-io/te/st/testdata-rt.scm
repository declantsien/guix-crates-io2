(define-module (crates-io te st testdata-rt) #:use-module (crates-io))

(define-public crate-testdata-rt-0.1.0 (c (n "testdata-rt") (v "0.1.0") (d (list (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)) (d (n "utime") (r "^0.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0z0cfi6wd4yr603y3av6ml60lfj1834p430idbnyaf0gs4yijhi4") (f (quote (("default") ("__doc_cfg")))) (s 2) (e (quote (("json" "dep:serde_json" "dep:serde"))))))

