(define-module (crates-io te st test-to-vec) #:use-module (crates-io))

(define-public crate-test-to-vec-0.1.0 (c (n "test-to-vec") (v "0.1.0") (d (list (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0nhyckbw4ljbc5nh250jk3fp6kyrksa9r7zppmp9l80bigyq2laj") (y #t)))

(define-public crate-test-to-vec-0.2.0 (c (n "test-to-vec") (v "0.2.0") (d (list (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "04cbrmf12q6csmx7qfkkg0klhhpixilv7vv5f3qk0z8vh2igyqrh") (y #t)))

(define-public crate-test-to-vec-0.3.0 (c (n "test-to-vec") (v "0.3.0") (d (list (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "18hl8bshrs3qj61pizmmkmqhzmkkpafy439fdf6f6gsia82syal0") (y #t)))

(define-public crate-test-to-vec-0.4.0 (c (n "test-to-vec") (v "0.4.0") (d (list (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "13vjjrijvvjgw087jq7qxg35bcrrw5pdaa9wkijf99pmj0lld6m7") (y #t)))

(define-public crate-test-to-vec-0.4.1 (c (n "test-to-vec") (v "0.4.1") (d (list (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0ja73cz6qjj0kmpw2w6j4nrm93hx6gr7ygm9zpnnz0fv0sdlb4l6")))

(define-public crate-test-to-vec-0.4.2 (c (n "test-to-vec") (v "0.4.2") (d (list (d (n "nom") (r "^2.0.1") (d #t) (k 0)))) (h "0z2j9ns3pmfmgaf81iv89drs19cnampxv9r91fdc1l7811kjxj70")))

(define-public crate-test-to-vec-0.4.3 (c (n "test-to-vec") (v "0.4.3") (d (list (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "04m5y102z7cf1n6n6925n8ycyggcs3vxjclnklg2kp9r3zmykfl8")))

