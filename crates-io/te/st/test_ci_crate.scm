(define-module (crates-io te st test_ci_crate) #:use-module (crates-io))

(define-public crate-test_ci_crate-0.1.0 (c (n "test_ci_crate") (v "0.1.0") (h "1zrgd8ksifcs7c5bba9bcac108agfk8bjn9dcad6nkwr1f1xixf8")))

(define-public crate-test_ci_crate-0.1.1 (c (n "test_ci_crate") (v "0.1.1") (d (list (d (n "deltalake") (r "=0.3.0") (d #t) (k 0)) (d (n "flatbuffers") (r "=0.8.0") (d #t) (k 0)))) (h "19sri33dsjwzqbpw8x0bszpj4p5xi9lzlr9s7zl7apvy7igwbblj")))

