(define-module (crates-io te st test_logs) #:use-module (crates-io))

(define-public crate-test_logs-0.1.0 (c (n "test_logs") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0fqpwpmp1vx49cxigk1g8gbr53269qn6ns59y7975ad37a96yji8")))

(define-public crate-test_logs-0.1.1 (c (n "test_logs") (v "0.1.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0x1h0gdrp9x513ck4vp9inny0fpmysqi4wxlwc29lm10xf7aa8i0")))

(define-public crate-test_logs-0.1.2 (c (n "test_logs") (v "0.1.2") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "157f41fm48ax0i6d22jih7lmkbg8sfl72bm9zaswzwc4n2kljy0l")))

(define-public crate-test_logs-0.1.3 (c (n "test_logs") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0999i854l9bsyjxinpdjrydcsngx1wnszvvdv801jp27vhplsfpb")))

(define-public crate-test_logs-0.1.4 (c (n "test_logs") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1hf92qi2x01awr95cgygysx4smzdh1xgxnp9qjm01nwizcclpm6f")))

(define-public crate-test_logs-0.1.5 (c (n "test_logs") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1bbfxybx1vv7d9pb0qxz4agri4kcwxdqmi8iwhf17w9h710l9hn9")))

(define-public crate-test_logs-0.1.6 (c (n "test_logs") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1zc9axa9hxw0miwvq3qpl7amfr5ia85x647858v11y85j3m3zkzi")))

(define-public crate-test_logs-0.1.7 (c (n "test_logs") (v "0.1.7") (d (list (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.14") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0nwfbxrjplszjx99zqwzyg3n455iix9ym2w98n64agqlk1ydhyfq")))

(define-public crate-test_logs-0.1.8 (c (n "test_logs") (v "0.1.8") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1qx3w1wsv71is41h883kzbhq1lz6z63xhsg93vzqjwg6sp5616d4")))

(define-public crate-test_logs-0.1.9 (c (n "test_logs") (v "0.1.9") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1ap7rkp55xq286z0zhli2n7vpv240dganbglpybjqjxygx95rvmf")))

(define-public crate-test_logs-0.1.10 (c (n "test_logs") (v "0.1.10") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "028dh2w57x9qg0g5aa2abj0g0q4x47whjv9cia4sbg3hps1ccg5g")))

(define-public crate-test_logs-0.1.11 (c (n "test_logs") (v "0.1.11") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0nh3hwglks31zyw73268vwyqm8w30z2hi1qbwmkw8hrvj1s884ir")))

(define-public crate-test_logs-0.1.12 (c (n "test_logs") (v "0.1.12") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0m0i6j2wi8k7w60g1xvqhd2glsxpkxbwcilqyaz46dd3hngcfci3")))

(define-public crate-test_logs-0.1.13 (c (n "test_logs") (v "0.1.13") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "01s22cb8wav1ddfqprjcv9fx8df04l8c58nv2kvhwnhawj697m0n")))

(define-public crate-test_logs-0.1.14 (c (n "test_logs") (v "0.1.14") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0i344jbw5np51z8bmk11pscjpvcrhjrr2sdk0ggn7qkrpmx155sl")))

(define-public crate-test_logs-0.1.15 (c (n "test_logs") (v "0.1.15") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.38") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "13brw71mnxq90a1s8ylyjs31dxzn7dn9xlp82b3xwdwvlzlnwd5y")))

(define-public crate-test_logs-0.1.16 (c (n "test_logs") (v "0.1.16") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0cirgwa2p3g01p7qfjij06gagawys2rkd7adf1j8bkmh29xdar1x")))

(define-public crate-test_logs-0.1.17 (c (n "test_logs") (v "0.1.17") (d (list (d (n "once_cell") (r "^1.17.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "11657vffsh1nrxs29m46yrm9d822n55gwrq6zbk118hvn82ci9gr")))

(define-public crate-test_logs-0.1.18 (c (n "test_logs") (v "0.1.18") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0apjip4pz6qqdpld5h7lgm0a5557ldf7800hbg1jay8cifyzxbxz")))

(define-public crate-test_logs-0.1.19 (c (n "test_logs") (v "0.1.19") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0wigmj49r3crw92anrdgyf4z8s3if3wv6kjd227l80455ma6m06k")))

(define-public crate-test_logs-0.1.20 (c (n "test_logs") (v "0.1.20") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "16w6lphylzr9wi7j11x8hgrb94x1zs8lcrbdg3f7blddpr80139n")))

(define-public crate-test_logs-0.1.21 (c (n "test_logs") (v "0.1.21") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1rdzggg5p3a19j44hq0q20in4dpnvmfq2a2gp7gjjv9scjzbjl9f")))

(define-public crate-test_logs-0.1.22 (c (n "test_logs") (v "0.1.22") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.39") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0fgfzzhcj3750ac2q0853y99hbs46a6ysgjp95hy7h9niw7drids")))

(define-public crate-test_logs-0.1.23 (c (n "test_logs") (v "0.1.23") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1cadp60an33j466yb5g1n4n5janzimbbka5c9za39gifhs9qjmdd")))

(define-public crate-test_logs-0.1.24 (c (n "test_logs") (v "0.1.24") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-log") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0wbd0d7f8kgi8d79x3zn9n2h7zv86y6lxz2yj3ibsb22j016rrfn")))

(define-public crate-test_logs-0.1.25 (c (n "test_logs") (v "0.1.25") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-log") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "11sjin3cppgd1csc8skh2fa949dbmvrb7jhlzg41y9a3gg7h4qbl")))

(define-public crate-test_logs-0.1.26 (c (n "test_logs") (v "0.1.26") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-log") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "08js53n89mj4zz6d7ad8vn7srs35y1n86grzxyn359lsac5dksjg")))

