(define-module (crates-io te st test-max-version-example-crate) #:use-module (crates-io))

(define-public crate-test-max-version-example-crate-0.1.0 (c (n "test-max-version-example-crate") (v "0.1.0") (h "09jm3ghkhpadbij3xz3wpqc60kmgdnwbldmybq3q3zfygl3l1bk7")))

(define-public crate-test-max-version-example-crate-18446744073709551615.18446744073709551615.18446744073709551615 (c (n "test-max-version-example-crate") (v "18446744073709551615.18446744073709551615.18446744073709551615") (h "06w429iv1xkrfl2rnzq3y2dg66rg3yflgvgkahyv1wwjqbjl56v4")))

