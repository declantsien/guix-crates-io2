(define-module (crates-io te st test-rename-pkg) #:use-module (crates-io))

(define-public crate-test-rename-pkg-0.0.0 (c (n "test-rename-pkg") (v "0.0.0") (h "19y6xwkbppj4l6hz358rdzkdwzwa111gqkdnqg27lmy86zs0lmvh") (y #t)))

(define-public crate-test-rename-pkg-0.0.1 (c (n "test-rename-pkg") (v "0.0.1") (h "1l8nq3scx8d99xxx2r991ih9lbfghzn9p5afyyhfnq952fdsivn2") (y #t)))

(define-public crate-test-rename-pkg-0.0.2 (c (n "test-rename-pkg") (v "0.0.2") (h "07y78xr5hpb7d10jdy54ipc8cbmrg43dgsyl0zcmzm7n7m53rvy2") (y #t)))

