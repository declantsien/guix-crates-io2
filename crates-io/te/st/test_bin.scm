(define-module (crates-io te st test_bin) #:use-module (crates-io))

(define-public crate-test_bin-0.1.0 (c (n "test_bin") (v "0.1.0") (h "0v0cnjym3a6ckbl836vlfkrrnraw7havgm8cf0d3bgi8d1ny0sin") (y #t)))

(define-public crate-test_bin-0.2.0 (c (n "test_bin") (v "0.2.0") (h "1jpc39d22my3ii51fh4rd5xgga16lx1wjxjlf5558y9gnq398936")))

(define-public crate-test_bin-0.3.0 (c (n "test_bin") (v "0.3.0") (h "1fhlffdpll0j6aqbvx30mbszly6xns7l1lr8jv0z5l0b9fqbldqr")))

(define-public crate-test_bin-0.4.0 (c (n "test_bin") (v "0.4.0") (h "0p0ij5n6kr58dsbilg1a3g3r9v6148wcz0fvsxfydik8akhpsylf")))

