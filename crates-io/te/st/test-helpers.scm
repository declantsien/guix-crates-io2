(define-module (crates-io te st test-helpers) #:use-module (crates-io))

(define-public crate-test-helpers-0.2.3 (c (n "test-helpers") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "15v3d6b264b8d5izwcvmc6gpd6ywxx4ip4jwyncjjdrkdd7ks30k")))

