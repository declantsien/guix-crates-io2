(define-module (crates-io te st test-project) #:use-module (crates-io))

(define-public crate-test-project-0.1.0 (c (n "test-project") (v "0.1.0") (h "0si518aw8dkazmasb8dzqd9xdyyahj337fbh4xfqy2lwvpx2i95h")))

(define-public crate-test-project-0.2.0 (c (n "test-project") (v "0.2.0") (h "1hsirx1f3xwwgqfal85xdrx7g28za395mhns3rz1n690k36liyx3")))

(define-public crate-test-project-1.0.0 (c (n "test-project") (v "1.0.0") (h "0w7nyh0rg22pih4pgjnyrv2bqld1g2b7nxsdw4yj60rixqzjw103")))

(define-public crate-test-project-1.1.0 (c (n "test-project") (v "1.1.0") (h "133qf5fkfnphfcb24dkyxbpwpxfz57s22zawbpc3z01dn46madm9")))

(define-public crate-test-project-1.1.1 (c (n "test-project") (v "1.1.1") (h "1fkb8rcj49bi3hzkkw29r9hxpgm3cccbmld6knrn6a9c0h0b3c31")))

(define-public crate-test-project-1.3.0 (c (n "test-project") (v "1.3.0") (h "14f7zlb6x0jbgp7jv99nck8m5403ix46hmciysfgrqsjxdkmqdwa")))

(define-public crate-test-project-1.4.1 (c (n "test-project") (v "1.4.1") (h "0am1i9sls1v3pdmnmqfaz5mvq131sppifk50346zl97aigdxcws3")))

(define-public crate-test-project-1.4.2 (c (n "test-project") (v "1.4.2") (h "1xvj4vjlzz2c1iahmyq8f9n9pw3y326xmsr9whhxyy46hxv6r3w5")))

(define-public crate-test-project-1.4.3 (c (n "test-project") (v "1.4.3") (h "1iicllj46rhhn4rnsfs3a2p51pb94bkzf57z7rnx9qg1mkn5xcg6")))

(define-public crate-test-project-1.5.0 (c (n "test-project") (v "1.5.0") (h "18f7q3wm0q21c8ys6wavgxwlg1sl76h8srmj06bsi59lfi8kzwap")))

(define-public crate-test-project-1.5.1 (c (n "test-project") (v "1.5.1") (h "1741aad9ga29gkllazj7df0ykk1niq7x76gr2xn05ml9b21brr23")))

(define-public crate-test-project-2.0.3 (c (n "test-project") (v "2.0.3") (h "1z29834ycw5nklnmnffkjfc9smjf82ax57sg3kvc7vlm3ixvz927")))

(define-public crate-test-project-2.1.0 (c (n "test-project") (v "2.1.0") (h "06nh9919wkswjdhqam525s39q9q55754zfj08sbsf2gsm2x4pw6c")))

(define-public crate-test-project-2.2.0 (c (n "test-project") (v "2.2.0") (h "1bbhg63zgaq172rhkg9q20zg9dkav22h91ir2q6q4v2l0wrnm720")))

(define-public crate-test-project-2.3.0 (c (n "test-project") (v "2.3.0") (h "1k380c947k822vy0sdmdmm89fybqkrdarp9bwk055v89i15ks1xv")))

