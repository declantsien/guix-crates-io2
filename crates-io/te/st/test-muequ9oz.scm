(define-module (crates-io te st test-muequ9oz) #:use-module (crates-io))

(define-public crate-test-mueQu9Oz-0.1.0 (c (n "test-mueQu9Oz") (v "0.1.0") (h "0fllkcjj5hjprnix8bi893f2qvmzvpdkmxfgqkl37qixsi445bcx")))

(define-public crate-test-mueQu9Oz-0.1.1 (c (n "test-mueQu9Oz") (v "0.1.1") (d (list (d (n "oracle") (r "^0.3.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "1l8kkj9cws3af3374zqalmpv0k5svks270an49n3l01l49hc6a0p") (f (quote (("chrono" "oracle/chrono"))))))

