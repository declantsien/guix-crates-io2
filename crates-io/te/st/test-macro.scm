(define-module (crates-io te st test-macro) #:use-module (crates-io))

(define-public crate-test-macro-0.1.0 (c (n "test-macro") (v "0.1.0") (h "1mp0bx8sjwybrqinv761h3m8x2rhfp4svn03sx2lgpm1sq4c0wgv") (y #t)))

(define-public crate-test-macro-0.1.1 (c (n "test-macro") (v "0.1.1") (h "1khgv9hlry0dybvh2pfd5sd95hcsj6md6axv0h6z644rmp9r8zjz") (y #t)))

(define-public crate-test-macro-0.1.2 (c (n "test-macro") (v "0.1.2") (h "15y40ljd0fi7rdb24wpz0mlq9pw1nxbspznym7n3wcca0b0jixqf") (y #t)))

(define-public crate-test-macro-0.1.3 (c (n "test-macro") (v "0.1.3") (h "020wk5d6rs76946b4hddz5zd75wsvp9ygdczmzgmp1il0a486i73") (y #t)))

(define-public crate-test-macro-0.1.4 (c (n "test-macro") (v "0.1.4") (h "08wc99bjxn3jbgf3z93z0gnknx3av263i4acym997fzydhcjri0r") (y #t)))

(define-public crate-test-macro-0.1.5 (c (n "test-macro") (v "0.1.5") (h "011f8i3xayrjd9x314idsqsksjmp67mhl6bd8v9wpsvbnd45n9c4") (y #t)))

(define-public crate-test-macro-0.1.6 (c (n "test-macro") (v "0.1.6") (h "02mfi789v659hy9gkqpwc3fk63ykxq0gv7yyklzg8h7rjp28k346") (y #t)))

(define-public crate-test-macro-0.1.7 (c (n "test-macro") (v "0.1.7") (h "0hmk8wvw7nnaclkyi42r2qcn2ald3igbj5wz0pp7yv4v3zidd1pv") (y #t)))

(define-public crate-test-macro-0.1.8 (c (n "test-macro") (v "0.1.8") (h "1sjbzvpps2dvkfkywbvggd367v4mwsdhd6s953xd6shw87c4f3cv")))

(define-public crate-test-macro-0.1.9 (c (n "test-macro") (v "0.1.9") (h "18xcvji1yk70d6yjjqn68ramc3m4gl678ng696kywxj8a1n0k2am")))

