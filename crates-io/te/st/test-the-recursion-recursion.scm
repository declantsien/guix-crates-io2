(define-module (crates-io te st test-the-recursion-recursion) #:use-module (crates-io))

(define-public crate-test-the-recursion-recursion-0.1.0 (c (n "test-the-recursion-recursion") (v "0.1.0") (h "0j9vcmk6s5k92n6nk3yfh9hnh6jg369bnfd3rvqnkjh4jqmzl99r")))

(define-public crate-test-the-recursion-recursion-0.1.1 (c (n "test-the-recursion-recursion") (v "0.1.1") (d (list (d (n "test-the-recursion") (r "^0.1.0") (d #t) (k 0)))) (h "0ii9npxfxsdfqjw44xxb3mbqj0y4d7xb7jq3qdq0nb34snh8n932")))

