(define-module (crates-io te st testssl-sys) #:use-module (crates-io))

(define-public crate-testssl-sys-0.1.15 (c (n "testssl-sys") (v "0.1.15") (h "0jp5jl5jqqdzq0rlpky8kp04ngbl75nqljhhd7g34i7ym58qxmam")))

(define-public crate-testssl-sys-0.1.16 (c (n "testssl-sys") (v "0.1.16") (h "10vmh0ixq6hs0g3y3w85x11ij4sgq64fvxaadqpzlgr5f80djpmx")))

(define-public crate-testssl-sys-0.1.17 (c (n "testssl-sys") (v "0.1.17") (h "1p6k451ljskprqgaw5cv6i4yv410ilryi2dsach64scs5dqj0rmf")))

(define-public crate-testssl-sys-0.1.18 (c (n "testssl-sys") (v "0.1.18") (h "0yi715ayysckaysxj7b7dsv28k09arfjypak6aws78y8sf6z2jr8")))

(define-public crate-testssl-sys-0.1.19 (c (n "testssl-sys") (v "0.1.19") (h "0h02wncpj2z2zsz6fh0b75ndabrkhpgzy95dby62i708v3wp0bxy")))

(define-public crate-testssl-sys-0.1.20 (c (n "testssl-sys") (v "0.1.20") (h "0fiybqniaigd3hm4m51p9187cvpbfb2jhrgph13169saccyszywg")))

(define-public crate-testssl-sys-0.1.21 (c (n "testssl-sys") (v "0.1.21") (h "1yvnfcarfc2011fv3g5vff6gzfw0zqd9xy523mmx8px5601d6xd0")))

(define-public crate-testssl-sys-0.1.22 (c (n "testssl-sys") (v "0.1.22") (h "1fqqpk16a5apbi00bfh2bbwcvm8ihjrx1q7xww6ii7l5in60adsi")))

(define-public crate-testssl-sys-0.1.23 (c (n "testssl-sys") (v "0.1.23") (h "0a4jx1a26sbnpvjrcja1hhb69wl1ix02nwfhs2863vwjn8f21gp5")))

(define-public crate-testssl-sys-0.1.25 (c (n "testssl-sys") (v "0.1.25") (h "1qy37x7dvz62jipm6196cwzbwqbbrvd5jq4s671if033sw18i4d7")))

(define-public crate-testssl-sys-0.1.26 (c (n "testssl-sys") (v "0.1.26") (h "12jx1x0cwy2hyz5vq6ssc984vpp7kk9ggwipvkkswj8dbphgpdc0")))

(define-public crate-testssl-sys-0.1.27 (c (n "testssl-sys") (v "0.1.27") (h "0q633plpk58nfb8frvjmbaznzl8xj4jf25n6mchmzwn6javm7mbr")))

(define-public crate-testssl-sys-0.1.28 (c (n "testssl-sys") (v "0.1.28") (h "1gaazwgf5a38lscw0lsvn4mdyba30wmmq79hq5kzzf56ca7d4qr5")))

(define-public crate-testssl-sys-0.1.30 (c (n "testssl-sys") (v "0.1.30") (h "1bc06fk3a5bbga10r8d2c76vknbg58pvzbscz49p21yxg1b3fxvr")))

(define-public crate-testssl-sys-0.1.31 (c (n "testssl-sys") (v "0.1.31") (h "00n2542ww1l33jd5phnzav4385hv3gzw5q3d7lpgbaimc2wvm39l")))

