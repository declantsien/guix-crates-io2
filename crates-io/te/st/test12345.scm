(define-module (crates-io te st test12345) #:use-module (crates-io))

(define-public crate-test12345-0.1.0 (c (n "test12345") (v "0.1.0") (h "0xxx5piv5ashfg4hwhjssqzr7xl6i0fxgl727z32q49gsyhx783p") (y #t)))

(define-public crate-test12345-0.1.1 (c (n "test12345") (v "0.1.1") (h "03pm2slqajgn81anlbvlp2w924brbxgyb9pf2m2hwxal8yik3z9k")))

