(define-module (crates-io te st test-vectors) #:use-module (crates-io))

(define-public crate-test-vectors-0.1.0 (c (n "test-vectors") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "test-vectors-macro") (r "^0.1.0") (d #t) (k 0)))) (h "10axy07i630hwvkkcymip7rqn0q0b0a4djf192q30bh5f1f6xbgj")))

