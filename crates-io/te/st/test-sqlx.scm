(define-module (crates-io te st test-sqlx) #:use-module (crates-io))

(define-public crate-test-sqlx-0.1.0 (c (n "test-sqlx") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-async-std-native-tls" "postgres" "mysql"))) (d #t) (k 0)))) (h "1dp847gzlhayfdsj6pah489jd695m1z90f5vdsmzc919iz3d124f") (y #t)))

