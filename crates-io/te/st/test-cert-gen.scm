(define-module (crates-io te st test-cert-gen) #:use-module (crates-io))

(define-public crate-test-cert-gen-0.1.0 (c (n "test-cert-gen") (v "0.1.0") (h "1lrpirh863h9rq2iqp0kivshzwd909fgiy13a5684d685ap7c565")))

(define-public crate-test-cert-gen-0.5.0 (c (n "test-cert-gen") (v "0.5.0") (d (list (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0b3rvrj1g2wyhvb4s9537s5sy3nkn52ckqb2ab7ajgzvyb85nril")))

(define-public crate-test-cert-gen-0.6.0 (c (n "test-cert-gen") (v "0.6.0") (d (list (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "16khiiv6j6xsd9wwzaz4rd0r1prjs8hqlb0zrx3r822s363zyiqq")))

(define-public crate-test-cert-gen-0.7.0 (c (n "test-cert-gen") (v "0.7.0") (d (list (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "06gl7zqvrsl4fqm6d3i21q459gcv9nf258kb5ynd8dip5spd021j")))

(define-public crate-test-cert-gen-0.8.0 (c (n "test-cert-gen") (v "0.8.0") (d (list (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "12p96xm1ivpa750fpm8m6wxlmfq6dwp4fbpdkwvwlfvaw1xyy1rp")))

(define-public crate-test-cert-gen-0.9.0 (c (n "test-cert-gen") (v "0.9.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pem") (r "^0.8.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "02nmsm23660525igrxii7l3f4h3w7x4wb652mg6hg5f5ravr4prl")))

