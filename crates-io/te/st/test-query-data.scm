(define-module (crates-io te st test-query-data) #:use-module (crates-io))

(define-public crate-test-query-data-0.1.0 (c (n "test-query-data") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (d #t) (k 0)))) (h "0m4ni570lcls40bws5yg9cisdpva456iqanjbq4dvwqb944sh66r") (f (quote (("websocket") ("http") ("full" "websocket" "http") ("default"))))))

(define-public crate-test-query-data-0.1.1 (c (n "test-query-data") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (d #t) (k 0)))) (h "1lwg94yhyx9yml88xz9r13wlwf657q2kim7v82pcm3yh7d3hnm7v") (f (quote (("websocket") ("http") ("full" "websocket" "http") ("default"))))))

(define-public crate-test-query-data-0.1.2 (c (n "test-query-data") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "13r87n0hlkbkbw9y1kzbp5fmi92wpmwsk957l1zm9fcazy2i5kmb") (f (quote (("websocket") ("http") ("full" "websocket" "http") ("default"))))))

(define-public crate-test-query-data-0.1.3 (c (n "test-query-data") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "0ahc33dajir6rz76hq2ps9wdiwprv0ksfz6qqwia7h4dwm4yqzk1") (f (quote (("websocket") ("http") ("full" "websocket" "http") ("default"))))))

