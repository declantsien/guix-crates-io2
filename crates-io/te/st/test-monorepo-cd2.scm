(define-module (crates-io te st test-monorepo-cd2) #:use-module (crates-io))

(define-public crate-test-monorepo-cd2-0.1.0 (c (n "test-monorepo-cd2") (v "0.1.0") (d (list (d (n "test-monorepo-cd") (r "^0.1.0") (d #t) (k 0)))) (h "0ffi842hcbrn6da3l6xsdbdwivdixrlsnv5lfyrqjw6d2405zrhf")))

(define-public crate-test-monorepo-cd2-0.1.1 (c (n "test-monorepo-cd2") (v "0.1.1") (d (list (d (n "test-monorepo-cd") (r "^0.1.0") (d #t) (k 0)))) (h "1q6507sf0vc8b5ql8nzx5xrwn2m3kspb67mz8c6qcqfpwagcxx4j")))

(define-public crate-test-monorepo-cd2-0.2.0 (c (n "test-monorepo-cd2") (v "0.2.0") (d (list (d (n "test-monorepo-cd") (r "^0.2.0") (d #t) (k 0)))) (h "0ml1r9vmc8058kxhyxhgmf42af6073hw0i7jbs4vxz1vkpmh4i3z")))

(define-public crate-test-monorepo-cd2-0.2.1 (c (n "test-monorepo-cd2") (v "0.2.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.0") (d #t) (k 0)))) (h "1ryyn9gm4rxkpgan1zrpn1d2a8nrqdnbm88a32d0kdbs3y4fx009")))

(define-public crate-test-monorepo-cd2-0.2.2-alpha.1 (c (n "test-monorepo-cd2") (v "0.2.2-alpha.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.2-alpha.1") (d #t) (k 0)))) (h "0pxjivc28gica987dfr6j0plm54rjn875d85qa9ivg6kq8qmmq0s")))

(define-public crate-test-monorepo-cd2-0.2.2 (c (n "test-monorepo-cd2") (v "0.2.2") (d (list (d (n "test-monorepo-cd") (r "^0.2.0") (d #t) (k 0)))) (h "0z33q1px501w6vggarrphr9hdsr6dxyizn15mwbnxwzam6siwwmn")))

(define-public crate-test-monorepo-cd2-0.3.0 (c (n "test-monorepo-cd2") (v "0.3.0") (d (list (d (n "test-monorepo-cd") (r "^0.3.0") (d #t) (k 0)))) (h "077q8r6vdymnznffw2dcbm9x6qimb19vzf3137zrkk2d364wmh96")))

(define-public crate-test-monorepo-cd2-0.2.2-beta.1 (c (n "test-monorepo-cd2") (v "0.2.2-beta.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.2-beta.1") (d #t) (k 0)))) (h "02ff1fmxiqvvn44g8y35b1jf48cpsjddsy3qvisyip0n9bz4miaz")))

(define-public crate-test-monorepo-cd2-0.2.2-rc.1 (c (n "test-monorepo-cd2") (v "0.2.2-rc.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.2-rc.1") (d #t) (k 0)))) (h "0ybr1cgbif7c211cwmln41ybja57kfl4fln274pzm0wl0a9b8v2y")))

(define-public crate-test-monorepo-cd2-1.0.0 (c (n "test-monorepo-cd2") (v "1.0.0") (d (list (d (n "test-monorepo-cd") (r "^1.0.0") (d #t) (k 0)))) (h "1w6lwxp2prs4d0p53fkkhfyzzai7h4p5y57mq268bf3gfzl845s7")))

(define-public crate-test-monorepo-cd2-0.2.3-alpha.1 (c (n "test-monorepo-cd2") (v "0.2.3-alpha.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "0qmagx6rag5f0l1n4ikhgi411ldmfh6ggp15my5kkvg5yyx7gy4g")))

(define-public crate-test-monorepo-cd2-0.2.3-alpha.2 (c (n "test-monorepo-cd2") (v "0.2.3-alpha.2") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "0pq898lfa4mr47v3xhv56cvw5f2a7z6skqv7nnmh9g4v93m9bw2y")))

(define-public crate-test-monorepo-cd2-0.2.3-alpha.3 (c (n "test-monorepo-cd2") (v "0.2.3-alpha.3") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "0k6qd0m4g6xp5fcm67bdbfvwc13wrf28xlj5i1pwvv31273nprbl")))

(define-public crate-test-monorepo-cd2-0.2.3-alpha.4 (c (n "test-monorepo-cd2") (v "0.2.3-alpha.4") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "0f533dis4pxn4898nb40j46z8y363w7ry93fk5f3114c85c5q0da")))

(define-public crate-test-monorepo-cd2-0.2.3-alpha.5 (c (n "test-monorepo-cd2") (v "0.2.3-alpha.5") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "1zniqwwb55l5bhsji9z2vb0qhmpdk26448l42y514kjvpqmrzix4")))

(define-public crate-test-monorepo-cd2-0.2.3 (c (n "test-monorepo-cd2") (v "0.2.3") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "1ia0hlm1004l2ysdfpp40fxmq15yjsyxhidp10pymq8frvz1c78y")))

(define-public crate-test-monorepo-cd2-0.2.4-rc.1 (c (n "test-monorepo-cd2") (v "0.2.4-rc.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.4-rc.1") (d #t) (k 0)))) (h "06wy4kdcl8iiqfqx16lah2ci2ibckgylwibgn9bmnw595qq2hf63")))

(define-public crate-test-monorepo-cd2-0.2.4 (c (n "test-monorepo-cd2") (v "0.2.4") (d (list (d (n "test-monorepo-cd") (r "^0.2.4-rc.1") (d #t) (k 0)))) (h "1hd2azir7dzc2402s1zw1bysl2i564xj4x3didbnm1i34m3qc755")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.1 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1nazl9qv7xl3gyf4cyywvpbhnzvxmq66r07lq5frcs41g9g8hs27")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.2 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.2") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1rb7ai3xz446zv7477cyl30531wfikmvcxrbiql8jlb0qqni2xg4")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.3 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.3") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "08lyxa8qcwcmfhmvfchlwjgvf39lwp27bifla7z7k8p3qqw7vb7b")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.4 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.4") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "0j77wbbq3r6hrd89xkw2v9fxlg38cg2rr9if8byfi5yrmyhp1nd2")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.5 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.5") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "0vpghfysn7ikbswd2hjlxanpfpzhj320y4vn1kp1hrhlw92g0fy0")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.6 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.6") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1q5cn65nyvb8z1nay8pa33q5s288cmcj8zk9zfz16fgc765zfwi0")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.7 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.7") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1511bs76f3jsh1j0nxczy0y0bvxcp2d2fna9ddcylbxa9ww20gkk")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.8 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.8") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1hsl80vlqy3cc8ibqjgnd2pfm7nkg8bfwlxhl3i7zb3dh2nzhi5r")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.9 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.9") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "148hw1y4iapj4lv9vjzwjqvyzjvbv55j0fsigjkkhayqcx1vx8j2")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.10 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.10") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "01p5l72zj6amwixsaxm4hh174qsbv3mmwksjrivqvf4x9x6vs5dm")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.11 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.11") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1367b7zss1qbad7k60znpkwbyyac0f835pnbbyiqqh960hwqpdi6")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.12 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.12") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1hxmbjnfbb5nk8j94gz8a79q849sknaidf3f100i0zbymx4l3pa8")))

(define-public crate-test-monorepo-cd2-0.2.5-alpha.13 (c (n "test-monorepo-cd2") (v "0.2.5-alpha.13") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "0g8gpi71ibr7zlz9hgz47jhvmxami66m55sakpd4fpy7njlmh5wr")))

(define-public crate-test-monorepo-cd2-0.2.5 (c (n "test-monorepo-cd2") (v "0.2.5") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1qqf9ig3m81isy1yd865lnp82blrcnvgih3pnck1fnj1p55s08nr")))

(define-public crate-test-monorepo-cd2-0.2.6-alpha.1 (c (n "test-monorepo-cd2") (v "0.2.6-alpha.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.6-alpha.1") (d #t) (k 0)))) (h "11snzsnrqb1jiiikm2cdam6x5isa8m6xlvh7ajvw0y15ca15snfg")))

(define-public crate-test-monorepo-cd2-0.2.6-alpha.2 (c (n "test-monorepo-cd2") (v "0.2.6-alpha.2") (d (list (d (n "test-monorepo-cd") (r "^0.2.6-alpha.1") (d #t) (k 0)))) (h "1hj3rmcj9aypcjygahvlrp8g4s1v9x568mnbfawaa6jad6dp3qjr")))

(define-public crate-test-monorepo-cd2-0.2.6-alpha.3 (c (n "test-monorepo-cd2") (v "0.2.6-alpha.3") (d (list (d (n "test-monorepo-cd") (r "^0.2.6-alpha.1") (d #t) (k 0)))) (h "1m1lc19wv37wm5jndqwgsxr58wmsyfr5cpp36r4sb3wjy21dcpbw")))

