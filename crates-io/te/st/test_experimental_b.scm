(define-module (crates-io te st test_experimental_b) #:use-module (crates-io))

(define-public crate-test_experimental_b-0.1.0 (c (n "test_experimental_b") (v "0.1.0") (d (list (d (n "test_experimental_c") (r "~0.1.0") (d #t) (k 0)))) (h "0ppjnr6cnr7dkvqmj4zh0sxdk1nl57my27wlfa7d0yb2viringc0")))

(define-public crate-test_experimental_b-0.2.0 (c (n "test_experimental_b") (v "0.2.0") (d (list (d (n "test_experimental_c") (r "~0.2.0") (d #t) (k 0)))) (h "09hnngdmnwsy39x79gzm9c09jhiwri2v21gnvi2p12r2kqik86k0")))

(define-public crate-test_experimental_b-0.3.0 (c (n "test_experimental_b") (v "0.3.0") (d (list (d (n "test_experimental_c") (r "~0.3.0") (d #t) (k 0)))) (h "15sl8zjry0ifw4di5slk2x2694jvsd89pcldjwfnq6qwvzfbayhg")))

(define-public crate-test_experimental_b-0.4.0 (c (n "test_experimental_b") (v "0.4.0") (d (list (d (n "test_experimental_c") (r "~0.4.0") (d #t) (k 0)))) (h "1nx55wp0fabnh96dbvlh9x2n91s9b83shnpy7g6id45vff5mb19i")))

