(define-module (crates-io te st test_version) #:use-module (crates-io))

(define-public crate-test_version-0.1.0 (c (n "test_version") (v "0.1.0") (h "0fdi2r9dppp1gglxz73jxd9fmnccyr1ngqihvjciabg1907s2ny6")))

(define-public crate-test_version-0.2.0 (c (n "test_version") (v "0.2.0") (h "0nj3vcq3mj6fmdyl6b3fmpv4zv7ij741vp7gkm782migr3dycf8g")))

