(define-module (crates-io te st test-release) #:use-module (crates-io))

(define-public crate-test-release-0.1.1 (c (n "test-release") (v "0.1.1") (h "0c05g1l2kf2fk3nzhlr12dz2j4vj0gs7h8ncfgmyh03pn5mms0sa")))

(define-public crate-test-release-0.1.2 (c (n "test-release") (v "0.1.2") (h "1zf77y18a384ravrrbj7fkmr6rf0dzaishj7jwxcjgsa73k5xsi6")))

(define-public crate-test-release-0.1.3 (c (n "test-release") (v "0.1.3") (h "135ywgg5l8vn6i15n3jzlipzniza26kkgivg137iym7726fgxfxr")))

(define-public crate-test-release-0.1.4 (c (n "test-release") (v "0.1.4") (h "0yqf7a08fza26q16wl6x5j1q2c9gls43sv3ghwvaj0zh2qvsavn7")))

(define-public crate-test-release-0.1.5 (c (n "test-release") (v "0.1.5") (h "0vy25abr64k8n690qygbi2qhv9r1qlyasggcpkqsd5gn8p9vcgh4")))

(define-public crate-test-release-0.1.6 (c (n "test-release") (v "0.1.6") (h "1qn1lmzhqfzi2dq48miqywlyi0mhaixb1mq9ya6l97wx2v8y7yv4")))

