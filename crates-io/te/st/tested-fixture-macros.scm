(define-module (crates-io te st tested-fixture-macros) #:use-module (crates-io))

(define-public crate-tested-fixture-macros-1.0.0 (c (n "tested-fixture-macros") (v "1.0.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dhrfi9lh7w19lf5d86chdvrmfdsc24qv1qqzmm411bawg1qhk31") (r "1.69")))

