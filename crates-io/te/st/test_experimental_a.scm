(define-module (crates-io te st test_experimental_a) #:use-module (crates-io))

(define-public crate-test_experimental_a-0.1.0 (c (n "test_experimental_a") (v "0.1.0") (d (list (d (n "test_experimental_b") (r "~0.1.0") (d #t) (k 0)))) (h "00ighxivk0nmyx949dcazn6kkar80fwlscm3gbk8qi7x48zvsckq")))

(define-public crate-test_experimental_a-0.2.0 (c (n "test_experimental_a") (v "0.2.0") (d (list (d (n "test_experimental_b") (r "~0.2.0") (d #t) (k 0)))) (h "1d9f4znpf4nq2sldn2gq8kp1098xgms5pjx67dg58q5idcnhlvh1")))

(define-public crate-test_experimental_a-0.3.0 (c (n "test_experimental_a") (v "0.3.0") (d (list (d (n "test_experimental_b") (r "~0.2.0") (d #t) (k 0)))) (h "05l0gsc1b982dr7iq4qvyaxwj59q7d1xgxs171dg7i6fdlx20jzq")))

(define-public crate-test_experimental_a-0.5.0 (c (n "test_experimental_a") (v "0.5.0") (d (list (d (n "test_experimental_b") (r "~0.3.0") (d #t) (k 0)))) (h "1xpwk1nm8d1c8vqrn47pb33f544afl3fc1xpia4sfzw8kqf0lwfw")))

(define-public crate-test_experimental_a-0.6.0 (c (n "test_experimental_a") (v "0.6.0") (d (list (d (n "test_experimental_b") (r "~0.4.0") (d #t) (k 0)))) (h "0pj7pavfsa4jgxmkj34rqgnzjdagcwxcjjvr143gfi40gfrfn554")))

