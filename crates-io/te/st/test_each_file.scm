(define-module (crates-io te st test_each_file) #:use-module (crates-io))

(define-public crate-test_each_file-0.0.1 (c (n "test_each_file") (v "0.0.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0drc2cdgr2lrf4h3a3mgxgixqf1jx9fnhm4n54zj9lzk79nbw2dz")))

(define-public crate-test_each_file-0.0.2 (c (n "test_each_file") (v "0.0.2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "19hzbv28zp6wd51dshykshdi30z5xhc1ab8jw9kaf4sdj94235zw")))

(define-public crate-test_each_file-0.1.0 (c (n "test_each_file") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0agbf06fw7hlycy4g0zh0cvcqqrf3fm65xq6c8mxjlsycxrnrh16")))

(define-public crate-test_each_file-0.1.1 (c (n "test_each_file") (v "0.1.1") (d (list (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "07mkxajvmcdcd5wq6g3c6nd3a6fvq3w2zn3c8bwahzvzziv9cawx")))

(define-public crate-test_each_file-0.2.0 (c (n "test_each_file") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0fz9qz0z4hi5skp8al7hq8xa4jysi9rjidj12b3lv65qfwamsrar")))

(define-public crate-test_each_file-0.3.0 (c (n "test_each_file") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1g1346zslqdrm9cf4k832di3hyqgjmww38l5q87nzvg9h8knjpp3")))

(define-public crate-test_each_file-0.3.1 (c (n "test_each_file") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.12") (d #t) (k 0)))) (h "1alnv5m5w926j93bsqqspi261zsr8pqj0yi6gw4w5f0xcrara2v3")))

(define-public crate-test_each_file-0.3.2 (c (n "test_each_file") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.12") (d #t) (k 0)))) (h "0rdkp8428riwh96cbqhr4mln1ngpfna0b48k244hgpkq0hrr46hg")))

