(define-module (crates-io te st testingforproject0126) #:use-module (crates-io))

(define-public crate-testingForProject0126-0.1.0 (c (n "testingForProject0126") (v "0.1.0") (h "0hgflzaa1my484ykhd0y5lg7ccw41srq6g4wvjygvx2x8xdn27va")))

(define-public crate-testingForProject0126-0.1.1 (c (n "testingForProject0126") (v "0.1.1") (h "0bkmxb5f00f7dl130n13373q35y0nhblkxsn7q4lzsvca941k06d")))

(define-public crate-testingForProject0126-0.1.2 (c (n "testingForProject0126") (v "0.1.2") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1csnpplnwxccjal02apcqyjif4k4zfmlhx7zy4776b468qrzkz3d")))

(define-public crate-testingForProject0126-0.1.3 (c (n "testingForProject0126") (v "0.1.3") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "151dy5q5h1nm3a3zfg73mvaziqz9jgfmrsmbfdyvvbn34bjlszxf")))

(define-public crate-testingForProject0126-0.1.4 (c (n "testingForProject0126") (v "0.1.4") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0vnw43sapv408hnq912phvsfpcx9nxq596xhqcxyvv9q1wchzx04")))

(define-public crate-testingForProject0126-0.1.5 (c (n "testingForProject0126") (v "0.1.5") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "10pwrqh7nmgnlpzi10150hdc47pscrrrz6897xjzr1rrig6c5a0p")))

(define-public crate-testingForProject0126-0.2.0 (c (n "testingForProject0126") (v "0.2.0") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0xb7gz03sias4mrjs7rcl40x68avr6188j8spsjnvyxnnhkxb15s")))

(define-public crate-testingForProject0126-0.2.1 (c (n "testingForProject0126") (v "0.2.1") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0s688x6q8mx626zz5hda9qk6zarlb0di7kql2mqgsv22g4r9ba3r")))

(define-public crate-testingForProject0126-0.2.2 (c (n "testingForProject0126") (v "0.2.2") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0g1g0qxiwadk3m2fhzhvii3719sr33z7iabvdp64mn3dcip9ja4i")))

(define-public crate-testingForProject0126-0.2.3 (c (n "testingForProject0126") (v "0.2.3") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0dv2s6s4ls2vr3s3ys9mirf354sxlk6js35jpyzksx4xiablgfg7")))

(define-public crate-testingForProject0126-0.2.4 (c (n "testingForProject0126") (v "0.2.4") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1ij1zvhl95ks332m3wp81rrbi97jkv0r7fxwwdai7j29np65jxjg")))

(define-public crate-testingForProject0126-0.2.5 (c (n "testingForProject0126") (v "0.2.5") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1d1p1w9x392k42s2ki56s5j0khx6cdjimgjmsdh0flp9l439yc6w")))

(define-public crate-testingForProject0126-0.2.6 (c (n "testingForProject0126") (v "0.2.6") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1xrndqf393k4x40hy0vfqcs93sg9qh437hcr8lkyzga4kj4icd4c")))

(define-public crate-testingForProject0126-0.2.7 (c (n "testingForProject0126") (v "0.2.7") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1q6rdpjysg37skhig73mnl4vm5r7friwrwbzsgn0plczjbxnjg4j")))

(define-public crate-testingForProject0126-0.2.8 (c (n "testingForProject0126") (v "0.2.8") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0fcacnb1nxbj579dnzgi3pkf49xp59ba4zk1n019sa930w1gd85a")))

(define-public crate-testingForProject0126-0.1.19 (c (n "testingForProject0126") (v "0.1.19") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0g3g88z9mmddaw0whfskflidgn4339b2zdkppqbfljx9iwl7z0s7")))

(define-public crate-testingForProject0126-0.2.9 (c (n "testingForProject0126") (v "0.2.9") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0vikh7ya6iqfpv54avl8dwfh77cqj6shnfw0rsimz8m3jpfwvvdm")))

(define-public crate-testingForProject0126-0.2.10 (c (n "testingForProject0126") (v "0.2.10") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0zi18jqakwbpbsy85lam2zilxnr2vbfdzyjz3vj69bcg445si975")))

(define-public crate-testingForProject0126-0.2.11 (c (n "testingForProject0126") (v "0.2.11") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1dwmpdamhf0804c09v8wcc3l2sk6qrv14xm9n87md4vawzjwc0jf")))

(define-public crate-testingForProject0126-0.2.12 (c (n "testingForProject0126") (v "0.2.12") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "01v89ry1gkvn77fdvi6d5s8yzv2scq754x6msz75sd6ss8r1s1ly") (y #t)))

(define-public crate-testingForProject0126-0.2.13 (c (n "testingForProject0126") (v "0.2.13") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0flxlm7k2pgy79kyp944dh9f845f4mywfx5d2xzqd2yjx87460m0") (y #t)))

(define-public crate-testingForProject0126-0.2.14 (c (n "testingForProject0126") (v "0.2.14") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1nrdvcnyc1fcy7yvh3m07jji47mid5m82cwvwa2iyxkk4vxvsa25") (y #t)))

(define-public crate-testingForProject0126-0.2.15 (c (n "testingForProject0126") (v "0.2.15") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0rm7hh7jvs471w9yf87rk660mf3f1xm6bp3mcqb59naq3xlm8y01")))

(define-public crate-testingForProject0126-0.2.16 (c (n "testingForProject0126") (v "0.2.16") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.1") (d #t) (k 0)))) (h "16cbs6d1qfcj59zjyc185gfc97kp61pfi3xkggcbzhyhd92pm0zk")))

(define-public crate-testingForProject0126-0.2.17 (c (n "testingForProject0126") (v "0.2.17") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.1") (d #t) (k 0)))) (h "0swkag28byaa9j8y02p1h3gwssw3dcy7yzyqjy5bz0kz2rf1lzyf")))

