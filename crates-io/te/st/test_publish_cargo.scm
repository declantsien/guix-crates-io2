(define-module (crates-io te st test_publish_cargo) #:use-module (crates-io))

(define-public crate-test_publish_cargo-0.1.0 (c (n "test_publish_cargo") (v "0.1.0") (h "0fk6bgdmcv6yh95j0hbbmilnlvwq0drkckaa20gq53i2xri5byhy")))

(define-public crate-test_publish_cargo-0.1.1 (c (n "test_publish_cargo") (v "0.1.1") (h "0kf44037xkpxfzhsyjkijgnzljz874yk0ic159ddzp4qfcankxpv")))

