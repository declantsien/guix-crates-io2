(define-module (crates-io te st testpackage) #:use-module (crates-io))

(define-public crate-testpackage-0.1.0 (c (n "testpackage") (v "0.1.0") (h "00664dqj3ljw32vfm1pqgiak89gx2bzz3sfjcln5gj9ijvwg088c")))

(define-public crate-testpackage-0.1.1 (c (n "testpackage") (v "0.1.1") (h "13lxhvh0wrfmrnr72j7y91v10y0qpwxk2xnmrycxndsrjhy438x1")))

(define-public crate-testpackage-0.1.2 (c (n "testpackage") (v "0.1.2") (h "1hx3nwqnjs0jxha6l194yb6657f91550habinzqvipqhav78wl7r")))

