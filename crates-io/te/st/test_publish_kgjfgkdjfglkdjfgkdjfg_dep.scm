(define-module (crates-io te st test_publish_kgjfgkdjfglkdjfgkdjfg_dep) #:use-module (crates-io))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg_dep-0.1.0 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (v "0.1.0") (h "1ddifx0qvdbgq940z41mmcg4nca9gnkk52mr6di67qfdl4b77miz")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg_dep-0.1.1 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (v "0.1.1") (h "1pspdzm8yw8abzv133vfx8mximm07mfmw1bx3lphfy6irzapjkby")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg_dep-1.2.5 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (v "1.2.5") (h "13mmxmfgvic4riq4dqgl8k4arq7jkdmzw5x9zljmlw1mhpkk8qwp")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg_dep-1.2.7 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (v "1.2.7") (h "1vrk4x5ahw42wgnbqhl6ygq7i5dm4bv92b1vzjrz2j8dmi2n7qrg")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg_dep-1.2.8 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (v "1.2.8") (h "09nr2kmpw2k1pm8k4h9l59a9b45n1lsh3p0glm8qf79gwpkgwm29")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg_dep-1.2.9 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (v "1.2.9") (h "00n2fbk3vmfndl20bddzzf60lmaad6p5d212wy9g8x10j1m7czz6")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg_dep-1.2.9-dev0 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (v "1.2.9-dev0") (h "1yx6x68sb22c53vd057p4lscc0lwlnv1xs2db9pzc2j3cnjhgj1i")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg_dep-1.2.9-dev.0 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (v "1.2.9-dev.0") (h "1lllwykk01x9w79a9mxlzhz5hc9fqdxlv4qi66rgdy9bswawhc97")))

