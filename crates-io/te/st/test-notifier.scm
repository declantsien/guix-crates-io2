(define-module (crates-io te st test-notifier) #:use-module (crates-io))

(define-public crate-test-notifier-0.1.0 (c (n "test-notifier") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (f (quote ("executor"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nustify") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "14351xwh9wm9hqsapm1lm2vi4hx9jm0igz3qkkkv0wxblf8sfgix")))

(define-public crate-test-notifier-0.1.1 (c (n "test-notifier") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (f (quote ("executor"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nustify") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "14vb3zi4a8imkb0wwxl6qgbgjgv0j04y4hyy8vmrbk7d0hpznrij")))

