(define-module (crates-io te st test_common) #:use-module (crates-io))

(define-public crate-test_common-1.0.0 (c (n "test_common") (v "1.0.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 2)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "00w0dhx7cjx62jgx2p1qv8hak810xkjf5b2hcivvhhnkzxr3pqjh")))

(define-public crate-test_common-1.0.1 (c (n "test_common") (v "1.0.1") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 2)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "0bafspcylrj6b2drzg1z2h7flq7j92y9rwl6b0946a8d3qr5vpgc")))

(define-public crate-test_common-1.1.0 (c (n "test_common") (v "1.1.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "0mi9gl9vcg0s0yn68sijidmxqa3zgh2lp41xwmnvxa6fljxr023l")))

(define-public crate-test_common-1.1.0-post1 (c (n "test_common") (v "1.1.0-post1") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "15pbprim0ykj93clppbp8kaj890x9js9phhrii5rzj105cm2v6x4")))

(define-public crate-test_common-1.2.0 (c (n "test_common") (v "1.2.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "04zk4n8y0q1pk719d17lfgxb97kzqmibrm67gj4ba44d2yrjrwgi")))

(define-public crate-test_common-1.3.0 (c (n "test_common") (v "1.3.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "0b0xb2scas5mqa14yp3vj9wcay2i881p3dqcg3wx0xbyyawxgr1n")))

(define-public crate-test_common-1.4.0 (c (n "test_common") (v "1.4.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 2)))) (h "0gk8jswgb7xxgrk4wf2l4i5msfzs3pbc6i51bi62nix3r3ch7m36")))

