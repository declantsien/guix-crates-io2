(define-module (crates-io te st test2) #:use-module (crates-io))

(define-public crate-test2-0.1.0 (c (n "test2") (v "0.1.0") (h "143j616nazh91k4nml0l5c87yih6r9p3vmiypgr8djjwiyj184hs")))

(define-public crate-test2-0.1.1 (c (n "test2") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1np2h9137i0xj0bpm0lckc72n1dfzah7dc00j6fxvaprplq5c3sp")))

