(define-module (crates-io te st testfiles) #:use-module (crates-io))

(define-public crate-testfiles-0.1.0 (c (n "testfiles") (v "0.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (f (quote ("glob" "metadata"))) (d #t) (k 0)))) (h "1qdrnyx4pcy9np02xicnkfj6a7z7gmpr1i1cxakgv1p1bs6ybhcq") (y #t) (r "1.56")))

(define-public crate-testfiles-0.1.1 (c (n "testfiles") (v "0.1.1") (d (list (d (n "include_dir") (r "^0.7.3") (f (quote ("glob" "metadata"))) (d #t) (k 0)))) (h "19yggr9ksl8iac8fz472vx3k8ih63kwbm0d2z0lsih35w2gcrwi8") (y #t) (r "1.56")))

(define-public crate-testfiles-0.0.1 (c (n "testfiles") (v "0.0.1") (d (list (d (n "include_dir") (r "^0.7.3") (f (quote ("glob" "metadata"))) (d #t) (k 0)))) (h "13p40inpjh3r5i97ixcz43crkxkf9m9c4qnwvnyiyah3mm469wl9") (r "1.56")))

(define-public crate-testfiles-0.0.2 (c (n "testfiles") (v "0.0.2") (d (list (d (n "include_dir") (r "^0.7.3") (f (quote ("glob" "metadata"))) (d #t) (k 0)))) (h "13smsykvmd626p0r95drsh6qmb5wk0y9g6v75r7q73d86v3j9r8y") (r "1.56")))

(define-public crate-testfiles-0.0.3 (c (n "testfiles") (v "0.0.3") (d (list (d (n "include_dir") (r "^0.7.3") (f (quote ("glob" "metadata"))) (d #t) (k 0)))) (h "0w44li1w6jd69yh0qk86qapx937b07wxrnl37z6fjynm9qf4jpbd") (r "1.56")))

(define-public crate-testfiles-0.0.4 (c (n "testfiles") (v "0.0.4") (d (list (d (n "include_dir") (r "^0.7.3") (f (quote ("glob" "metadata"))) (d #t) (k 0)))) (h "12zbl2kskcq415y9llifl3vcd3vz1243xldvbmb52bxhjlsf85wr") (r "1.56")))

