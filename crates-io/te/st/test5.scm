(define-module (crates-io te st test5) #:use-module (crates-io))

(define-public crate-test5-0.1.0 (c (n "test5") (v "0.1.0") (h "1qrr0nfbnc731rkyklr3fs0m8b5xr25xmsnm09rpzvq38xgm4bs7") (y #t)))

(define-public crate-test5-0.1.1 (c (n "test5") (v "0.1.1") (h "13pj560zwzga08hx0cmkcyrys2h1ar5fj5mjn7ya44d0pdnwzv2n") (y #t)))

(define-public crate-test5-0.1.2 (c (n "test5") (v "0.1.2") (h "13ipd232i5gj71kxbnlvyp7nzs9j8yrkv4fmfwl9gs9dra6kzbbv")))

(define-public crate-test5-0.1.3 (c (n "test5") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "10jbbg118a9v8lzy2zcn9wkm7qd1w8jc43xx9k2lm8wwpkyr1a71")))

