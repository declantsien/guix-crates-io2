(define-module (crates-io te st test_yank_rc_dep) #:use-module (crates-io))

(define-public crate-test_yank_rc_dep-0.1.0 (c (n "test_yank_rc_dep") (v "0.1.0") (d (list (d (n "serde") (r "^0.9.0-rc4") (d #t) (k 2)))) (h "047snwv8pqy9irr52a7fk48n9p45qg4ivlf2p0rz8sc689wgvv5a") (y #t)))

(define-public crate-test_yank_rc_dep-0.1.1 (c (n "test_yank_rc_dep") (v "0.1.1") (d (list (d (n "serde") (r "^0.9.0") (d #t) (k 2)))) (h "0phxndzi6r1pwmsf93hhygqb3mmvc0y4ncvyq1287bphqv2xhsws") (y #t)))

(define-public crate-test_yank_rc_dep-0.1.2 (c (n "test_yank_rc_dep") (v "0.1.2") (d (list (d (n "serde") (r "^0.9.1") (d #t) (k 2)))) (h "1q23wb0d4jcnlw6s3ylr0zjvaafljk6134j6v6crv3jswg1j1akz") (y #t)))

(define-public crate-test_yank_rc_dep-0.1.3 (c (n "test_yank_rc_dep") (v "0.1.3") (h "026jr840gpnp9fg8iihl0advdzgz682ibi2a29l3drqzflcdsh1f")))

