(define-module (crates-io te st testanything) #:use-module (crates-io))

(define-public crate-testanything-0.1.0 (c (n "testanything") (v "0.1.0") (h "0xla000lm73i2lb0q1dxmscpnr6m44n4xkbg4lmayj6796xgv48a")))

(define-public crate-testanything-0.1.1 (c (n "testanything") (v "0.1.1") (h "0c1dqgjfvy8s1n966qj6yvfvrgnwiranksis15nv0vi1di6fd3hx")))

(define-public crate-testanything-0.1.2 (c (n "testanything") (v "0.1.2") (h "0x5l2wawa1b0lkxmzx7v9nlyrdwh2r1igdscal7x8f2gynqhifi8")))

(define-public crate-testanything-0.1.3 (c (n "testanything") (v "0.1.3") (h "0g4vw2hhfc6ymp3shfax61aw6aj2mjby204qzjzx16li0glfs7hs")))

(define-public crate-testanything-0.2.0 (c (n "testanything") (v "0.2.0") (h "1yby4wz5bjwiis2zyc7rqryhbdwfcjynk6p02h7bhb1dn0ad9jc6")))

(define-public crate-testanything-0.2.1 (c (n "testanything") (v "0.2.1") (h "110ag9q0pf3phbgv847pw3m4v0p1qlfmqmdk2fzawszqh05ry76i")))

(define-public crate-testanything-0.3.0 (c (n "testanything") (v "0.3.0") (h "0m8220xb1x8s84jrnjyaj0v0m5276rqcvnhvc1shf50fqi8myn7f")))

(define-public crate-testanything-0.3.1 (c (n "testanything") (v "0.3.1") (h "1ysrn39zfg1i1wlq01w42vraam1mzcpwbkb7p2y5qmw0s6089njs")))

(define-public crate-testanything-0.3.2 (c (n "testanything") (v "0.3.2") (h "08sdys54jm0zi4y771bfsg5kglivhds7307w16v12kx7p0igm0ny")))

(define-public crate-testanything-0.4.0 (c (n "testanything") (v "0.4.0") (h "0lv4ak6iipfv2rhf9vkxqrjfgss21ysv4vz1p1p6j5qjlh27j9mk") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-testanything-0.4.1 (c (n "testanything") (v "0.4.1") (h "0dcxs18pd3d53bb592g36wqyrj0f77fks1hfhcznwz5h75hj76w4") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-testanything-0.4.2 (c (n "testanything") (v "0.4.2") (h "13qnyjy4ncys2z910glj06nn3ijgzf88cw0nj7h0a2pagr3z0fqr") (f (quote (("std") ("default" "std") ("alloc"))))))

