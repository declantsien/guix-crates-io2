(define-module (crates-io te st testmark) #:use-module (crates-io))

(define-public crate-testmark-0.1.0 (c (n "testmark") (v "0.1.0") (h "0scmz8x2xr8phgq2gwd8wqdacjc50f4fddicg5qzrzzfiyfqylmd")))

(define-public crate-testmark-0.1.1 (c (n "testmark") (v "0.1.1") (h "1vkflrg4asbn1mpljxdks23z6fzapmy2b4i1ynan1w3450bxcr62")))

