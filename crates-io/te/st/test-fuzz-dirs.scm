(define-module (crates-io te st test-fuzz-dirs) #:use-module (crates-io))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.1 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.1") (d (list (d (n "cargo_metadata") (r "^0.11.1") (d #t) (k 0)))) (h "1g9kvixpd6yfx32n62a3h5s3f18crpn63cdpi2d4h9wi9ijn5aqq")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.2 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.2") (d (list (d (n "cargo_metadata") (r "^0.11.1") (d #t) (k 0)))) (h "1gygb9sawa8ps0890ml86dpcwpi3gd6l3ay86im9cvfnzm8fk6sd")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.3 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.3") (d (list (d (n "cargo_metadata") (r "^0.11.1") (d #t) (k 0)))) (h "002amc847svb34gic38v9j91bb10inz9nl555ka40y776l3rk4nd")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.4 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.4") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)))) (h "1v8y0l6l8rcabki4sxiv0rvj86mby7s0ahg186cp8kwx5cxkngqr")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.9 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.9") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)))) (h "084lsrnbplwlp7vvyf2af8cki87j0dsw0mkx2aczc40fyym0j2za")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.10 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.10") (d (list (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)))) (h "19jv5rncxqdp51s8gjr9xsfd1ps5ji34ndywinaaqk0apb27d5fw")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.11 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.11") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "19xi8352455g7ibzji1jfxvm9jfvybbl1z7zkj7indsfd62hq20h")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.12 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.12") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "1wsvlq0bqgpj22zmi4jv2v9axs13lpmwgm9jr3qpzi80mrqbfxkw")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.13 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.13") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "1m2cvl6lkf8w1h9p60mqcyw3c4ypcx7jvvhnwyzbxwlppzc79df3")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.14 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.14") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "1q8cj0q9xwlw7dwanchw5mqpqgxd1232pl9ylvf9xdl3980hzdi4")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.15 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.15") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "0mrdvdsrwpzcf6ka9x7qn24y91gmw51gfi751l9021d0r5mbdxax")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.16 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.16") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "106kqcv74acmmqd2gs0c8qag8nh1340pwpgdkrbp28acnqnch016")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.17 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.17") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "01rx70a6zr480dsr16xinv4zr3ig8jr67ypgfjh8lxlfnlfna016")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.18 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.18") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "1zplq13l7pk9m4lp3fxmv5f6jflnlvj4a771amlwajpjwdfy5a99")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.19 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.19") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "0f8v5jai4p3nwmlpa4y2x591n0lrqy88a515rq8f3z6a751yx8xi")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.20 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.20") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "19j66dp50hx27pjp5aivmlw9pfrhkk78jcc7bf5jwh9fip6a0zw5")))

(define-public crate-test-fuzz-dirs-0.1.0-alpha.21 (c (n "test-fuzz-dirs") (v "0.1.0-alpha.21") (d (list (d (n "cargo_metadata") (r "^0.13.1") (d #t) (k 0)))) (h "12gsav9gw8spsqnyra1kzpkxhnxc8r2p27a3zdkb6dc7jk5z2xwp")))

