(define-module (crates-io te st test-env-helpers) #:use-module (crates-io))

(define-public crate-test-env-helpers-0.1.0 (c (n "test-env-helpers") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "1daws73wi9igiqx43nlj1l8l1ajrz3r2638ywn3sg89yj5b9j7sx")))

(define-public crate-test-env-helpers-0.2.0 (c (n "test-env-helpers") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "0p461jfsn9g8dml44ar5wqg88c8vxpplic0nb9051dqfj8l80lg2")))

(define-public crate-test-env-helpers-0.2.1 (c (n "test-env-helpers") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "1kf085p7hbab474664ghx5zyix3wgyya68xnv61v6b1lwxkshlws")))

(define-public crate-test-env-helpers-0.2.2 (c (n "test-env-helpers") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "test-util"))) (d #t) (k 2)))) (h "1imifbm2zrsph886pq7cqsrd244ayllg56fxz7gdl1694948zay6")))

