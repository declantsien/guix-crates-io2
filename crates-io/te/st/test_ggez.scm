(define-module (crates-io te st test_ggez) #:use-module (crates-io))

(define-public crate-test_ggez-0.1.0 (c (n "test_ggez") (v "0.1.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)))) (h "0hqwzicd28apj9fj0qlkks7q6fnngw47rd3xjwklyb4lj119zf23")))

(define-public crate-test_ggez-0.1.1 (c (n "test_ggez") (v "0.1.1") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "png") (r "^0.16.8") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)))) (h "0sb720mna4yn4ng81jw6vvxc7wmgpdfsn5k4n4i4gr4k5czqrk9a")))

