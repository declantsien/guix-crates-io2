(define-module (crates-io te st test_crate_01) #:use-module (crates-io))

(define-public crate-test_crate_01-0.1.0 (c (n "test_crate_01") (v "0.1.0") (h "17z251lywpj7klp05p8dq02vsimpw5rdsr0pcj5gpv8my32ifqj8")))

(define-public crate-test_crate_01-0.1.1 (c (n "test_crate_01") (v "0.1.1") (h "0zl84pgk7hqvaqiwnq2sfgxb2vfclv1lw61slyqimkpssl1k2ghr")))

(define-public crate-test_crate_01-0.1.2 (c (n "test_crate_01") (v "0.1.2") (h "1c5111q7iyrr1rk7vqyfpyns745b1ppbv7facdfsl3z2xin7i911")))

(define-public crate-test_crate_01-0.1.3 (c (n "test_crate_01") (v "0.1.3") (h "04x6f151lwjq0ybhzhw8n19c3jywa7icpvj9vaabwz5an3pmfixs")))

(define-public crate-test_crate_01-0.1.4 (c (n "test_crate_01") (v "0.1.4") (h "1g8nr808mr6c86dzhdm7ykv3wkn1l46s7p6bp2q1156fxwpb0f4g")))

(define-public crate-test_crate_01-0.1.5 (c (n "test_crate_01") (v "0.1.5") (h "1d00qiyqv889d9s4kfrw36h5mmkk03zrjrl0m1nr6y0k7lnkxzhl")))

(define-public crate-test_crate_01-0.1.6 (c (n "test_crate_01") (v "0.1.6") (h "1h8njvn0wf44srzzrcyxmyfg6ggqijm63fy72gz6iymv6c6fvzwd")))

(define-public crate-test_crate_01-0.1.7 (c (n "test_crate_01") (v "0.1.7") (h "17z3ydhbb649ijxrlz8fgiqlvnr0ns7h3yd8cjj69cmf3svg8s70")))

(define-public crate-test_crate_01-0.1.8 (c (n "test_crate_01") (v "0.1.8") (h "0g8585m5mqr6avsrglx6019q2qjbwd7ahqg41a5v0b2pfy9awqzb")))

