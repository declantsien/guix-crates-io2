(define-module (crates-io te st test_gen) #:use-module (crates-io))

(define-public crate-test_gen-0.1.0 (c (n "test_gen") (v "0.1.0") (h "1p8p4gf1kjskd8xy4nlmav17aw7cvybv3nsc94v5zvxiapgndgg4")))

(define-public crate-test_gen-0.2.0 (c (n "test_gen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0frcj32l544q40cc4sf2gy8295qsrink5vcc99sir1pd3xlfaxjz")))

(define-public crate-test_gen-0.2.1 (c (n "test_gen") (v "0.2.1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "135jkwwrj0lgm36iqqijz8dsl36b5q5ajdzrmzgdvgj7v7dk2sws")))

(define-public crate-test_gen-0.2.2 (c (n "test_gen") (v "0.2.2") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1y7l5wip8s7s0wj3j3l3ibi60yvpxf5f8krqvj2l5nwp9sj3k849")))

(define-public crate-test_gen-0.2.3 (c (n "test_gen") (v "0.2.3") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "13h5mc1w41x1gz4dgl2rpwqrsfy3844r4lljssn3lm7shbps1l8w")))

