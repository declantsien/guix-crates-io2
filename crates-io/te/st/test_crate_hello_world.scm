(define-module (crates-io te st test_crate_hello_world) #:use-module (crates-io))

(define-public crate-test_crate_hello_world-0.1.0 (c (n "test_crate_hello_world") (v "0.1.0") (h "04pgl1smw5pyqy1015gm7xzdwwywk6hcbc4k4z5yfpk2s6j8mafy")))

(define-public crate-test_crate_hello_world-0.1.1 (c (n "test_crate_hello_world") (v "0.1.1") (h "0qyz6hspmy182ikc43c5zsp2150mx2yqxw3zdcqq7n45gkzfm98f")))

(define-public crate-test_crate_hello_world-0.1.2 (c (n "test_crate_hello_world") (v "0.1.2") (h "12fi86gzn74bqg7mpm6cs3zn6x1pfvpk87xx3hyangwkxgnv0jim")))

