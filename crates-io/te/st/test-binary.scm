(define-module (crates-io te st test-binary) #:use-module (crates-io))

(define-public crate-test-binary-1.0.0 (c (n "test-binary") (v "1.0.0") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "03q0l9rm0xawjv94vgkvfcb0x0q9slhlmc5hkh4bhxshcdzas7ij") (f (quote (("test-doesnt-build"))))))

(define-public crate-test-binary-1.0.1 (c (n "test-binary") (v "1.0.1") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "17l4md562bwfhmgalxn66zf683vjcngk0al45fsv83yprckl7ng3") (f (quote (("test-doesnt-build"))))))

(define-public crate-test-binary-2.0.0 (c (n "test-binary") (v "2.0.0") (d (list (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1pns0zbivdcy6fncf0l2b77w9190cmzb5w3hypzrmxq0h6f60517")))

(define-public crate-test-binary-3.0.0 (c (n "test-binary") (v "3.0.0") (d (list (d (n "camino") (r "^1.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17xsdj4b6hyxrgrfjl73n4qzaannxkd6rl9qdw6aynaxgrbnaxk7")))

(define-public crate-test-binary-3.0.1 (c (n "test-binary") (v "3.0.1") (d (list (d (n "camino") (r "^1.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jmnnyw84n66ljgirv54ymrs4cnra6ab18gj0mbjxw2lg0g7fa5v")))

(define-public crate-test-binary-3.0.2 (c (n "test-binary") (v "3.0.2") (d (list (d (n "camino") (r "^1.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15") (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qz616938rga68aa27lad0k15fx2cfzmiqxd1wfbch2w51abhz3c")))

