(define-module (crates-io te st testgen) #:use-module (crates-io))

(define-public crate-testgen-0.0.1 (c (n "testgen") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1qaa449kzn9rinkj5vqp134xd3vj8jvjbgn3iknidiicffb6i067")))

(define-public crate-testgen-0.1.0 (c (n "testgen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6r1rh0niqb86bz7hxwgvl56yfhlyafa40gddkbj5ii8yzp4prn")))

