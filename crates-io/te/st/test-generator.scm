(define-module (crates-io te st test-generator) #:use-module (crates-io))

(define-public crate-test-generator-0.1.0 (c (n "test-generator") (v "0.1.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.25") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "0n7nw8prwyyhjaw9klpm1w2r411b6kzyrr0hnqidgwaniqff73ks")))

(define-public crate-test-generator-0.2.0 (c (n "test-generator") (v "0.2.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.25") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0q9fg4wc2rx9xggp6wn5wpm0idli5ch5g9varvlwchrnz54419hf")))

(define-public crate-test-generator-0.2.1 (c (n "test-generator") (v "0.2.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.25") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1yhnnslsvc2alp4m611ws9fj71108j2acvqnima61453aj720na7")))

(define-public crate-test-generator-0.2.2 (c (n "test-generator") (v "0.2.2") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.25") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0wbgrm66j7944pi8028cg5ripcz7kcly6cxbmgjw3fr6y2gbn9lv")))

(define-public crate-test-generator-0.3.0 (c (n "test-generator") (v "0.3.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1h03y4503jhhrks4m7xqfjya9lsx3ip5dlbldr7mgcws6j8bx5za")))

(define-public crate-test-generator-0.3.1 (c (n "test-generator") (v "0.3.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0fh7gaxlj48gg9l716084xcnll1p7v5lcr6bw4k348krvlmbw8sv")))

