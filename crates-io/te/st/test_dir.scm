(define-module (crates-io te st test_dir) #:use-module (crates-io))

(define-public crate-test_dir-0.1.0 (c (n "test_dir") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "079q6svshv7hkjagd7wm1wzcwzvpxylai7qai4hsi7bs2bwynwg5")))

(define-public crate-test_dir-0.2.0 (c (n "test_dir") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0fk8h15sxnfbysm2j1ia5bi9r3fh1jmbljhcfk7sszy5kyprvh8z")))

