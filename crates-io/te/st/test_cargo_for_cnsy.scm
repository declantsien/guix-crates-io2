(define-module (crates-io te st test_cargo_for_cnsy) #:use-module (crates-io))

(define-public crate-test_cargo_for_cnsy-0.1.0 (c (n "test_cargo_for_cnsy") (v "0.1.0") (h "17xnv7bv21kgdcb1xiqfh715q28aq0k2fqx1g0sczfpiasanqwds")))

(define-public crate-test_cargo_for_cnsy-0.1.1 (c (n "test_cargo_for_cnsy") (v "0.1.1") (h "04zqr7h664pjj465wj7dvhj37gfxd8brbc515bacah4q69ibi0cd")))

