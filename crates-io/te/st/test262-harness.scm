(define-module (crates-io te st test262-harness) #:use-module (crates-io))

(define-public crate-test262-harness-0.1.0 (c (n "test262-harness") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ar97q4ryaphpjclgms6dlv8s2f5l8p5g2d06kkdvh2xphgw7dwd")))

(define-public crate-test262-harness-0.1.1 (c (n "test262-harness") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0gklpm6igbrk4gxbsj940byy0m7gy23ap7jriihl566rlh2950y5")))

