(define-module (crates-io te st testrudder) #:use-module (crates-io))

(define-public crate-testrudder-1.0.0 (c (n "testrudder") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0xll6kr9gwyk5xg8bx8ldqldvw4fm4f8q9a5kzgllqzvi1qa377x") (f (quote (("cli" "clap")))) (y #t)))

