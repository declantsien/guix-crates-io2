(define-module (crates-io te st test_program) #:use-module (crates-io))

(define-public crate-test_program-0.1.0 (c (n "test_program") (v "0.1.0") (h "0kl3jz11zy0ycss1cmmm5wq2shrvxh6zljf2hgqrs7bj2mid7c9q")))

(define-public crate-test_program-0.1.1 (c (n "test_program") (v "0.1.1") (h "0w9bkavsbznn6ran2msl46kh3smijsk4yq98v4k6w00p5phlniai")))

