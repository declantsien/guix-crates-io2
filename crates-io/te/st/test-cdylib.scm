(define-module (crates-io te st test-cdylib) #:use-module (crates-io))

(define-public crate-test-cdylib-1.0.0 (c (n "test-cdylib") (v "1.0.0") (d (list (d (n "cargo_metadata") (r "^0.9.0") (d #t) (k 0)) (d (n "dlopen") (r "^0.1.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "1v6cxihvcf7maywgbdbbsf373h2riykgm1z6qk7ql9fkdhll1dzi")))

(define-public crate-test-cdylib-1.1.0 (c (n "test-cdylib") (v "1.1.0") (d (list (d (n "cargo_metadata") (r "^0.9.0") (d #t) (k 0)) (d (n "dlopen") (r "^0.1.8") (d #t) (k 2)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.2") (d #t) (k 0)))) (h "13g75r8gyk4l7jmn1ql6hggiiqsij9d2xdpagcbzapwzf8gipx68")))

