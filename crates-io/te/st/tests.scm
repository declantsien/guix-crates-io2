(define-module (crates-io te st tests) #:use-module (crates-io))

(define-public crate-tests-0.0.1 (c (n "tests") (v "0.0.1") (d (list (d (n "bip39") (r "=2.0.0") (d #t) (k 0)) (d (n "bitcoin") (r "=0.26.0") (d #t) (k 0)) (d (n "clap") (r "=2.33.3") (d #t) (k 0)) (d (n "colored") (r "=2.1.0") (d #t) (k 0)) (d (n "elements") (r "=0.24.1") (d #t) (k 0)) (d (n "qrcode") (r "=0.13.0") (d #t) (k 0)) (d (n "rand") (r "=0.8.5") (d #t) (k 0)) (d (n "regex") (r "=1.10.3") (d #t) (k 0)))) (h "0k541l9bn74ya87k3mrqx8h7hjq0a8f2273bgvm3vpx2gf1l8fc0") (y #t)))

