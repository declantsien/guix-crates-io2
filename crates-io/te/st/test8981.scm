(define-module (crates-io te st test8981) #:use-module (crates-io))

(define-public crate-test8981-0.1.0 (c (n "test8981") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.3") (d #t) (k 0)))) (h "1z0yx49hd5x8v8bsikpk34m0m4r4yp4vygb5jq36nwmzsikvskpl")))

(define-public crate-test8981-0.1.1 (c (n "test8981") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "05135v0ihpz0myjcwdndf2a9zajmgdj8vf97p562x8n7dzjkxwcx")))

(define-public crate-test8981-0.1.2 (c (n "test8981") (v "0.1.2") (h "10jkf0ljhqpgclp2bjw78wvzi8dv4sdpvrdm12l2ld5swlw96wv5")))

(define-public crate-test8981-0.2.0 (c (n "test8981") (v "0.2.0") (h "05zz30s1fng8max2m233h0c4pzzqbx3ix7618bdq74ycn35k47k8") (r "1.65")))

