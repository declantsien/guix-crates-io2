(define-module (crates-io te st testo) #:use-module (crates-io))

(define-public crate-testo-0.3.0 (c (n "testo") (v "0.3.0") (h "0y5x0mmg61zb6h92m3fayz7x9qb6ccy4zvc5lk7yvjgv4bz865wv")))

(define-public crate-testo-0.3.5 (c (n "testo") (v "0.3.5") (h "1l5sf69a85ma7ylzxna90fyzpq5wczj065xsqxkyr3pcd0dav3s8")))

(define-public crate-testo-0.3.9 (c (n "testo") (v "0.3.9") (h "044a47n6d3h8vb1mi2zr8kqyxwv73pk7a6hdp6vkxnq7dizpvr0x")))

