(define-module (crates-io te st teste_publish_c4rls) #:use-module (crates-io))

(define-public crate-teste_publish_c4rls-0.1.0 (c (n "teste_publish_c4rls") (v "0.1.0") (h "15jwhcd8dqaqz402mibbkns9z627jjlz02ias75l8wpyy9dviik7") (y #t)))

(define-public crate-teste_publish_c4rls-0.0.1 (c (n "teste_publish_c4rls") (v "0.0.1") (h "05b5qnbib16dq4yayk9avchjkiqr9vllvax0ih299c95xsd0zgqg")))

(define-public crate-teste_publish_c4rls-1.0.0 (c (n "teste_publish_c4rls") (v "1.0.0") (h "0rkqzpwg0alr0xllzhsdfq1m456hg5d7r5cil9nvahabglzqq25g")))

