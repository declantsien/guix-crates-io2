(define-module (crates-io te st test-testt) #:use-module (crates-io))

(define-public crate-test-testt-0.1.0 (c (n "test-testt") (v "0.1.0") (h "0cf3iwjflpzy60rd5ac9z671inp6s5m52rb50q89mia3vvxjxn6b")))

(define-public crate-test-testt-3.0.0 (c (n "test-testt") (v "3.0.0") (h "09q7zzsbz7an03rwcw2pgndnypgpxkx35vzgf3ybc5b6yq8imkyq")))

