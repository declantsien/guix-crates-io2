(define-module (crates-io te st test_pub_create) #:use-module (crates-io))

(define-public crate-test_pub_create-0.1.0 (c (n "test_pub_create") (v "0.1.0") (h "05r8dibvrnv5a8rkqqncrjncxl1l5n2h9i5nrnyfdq9shvipvwzn")))

(define-public crate-test_pub_create-0.1.1 (c (n "test_pub_create") (v "0.1.1") (h "03r8s67lm5k9js13d3r984rd6iqpknyvwy17y7c5l9wxyh733yhg")))

