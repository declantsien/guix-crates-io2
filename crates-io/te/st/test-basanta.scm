(define-module (crates-io te st test-basanta) #:use-module (crates-io))

(define-public crate-test-basanta-0.1.0 (c (n "test-basanta") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "file-size") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0frizqa2iyaii45qavh8xaanbg5bxfigjr7znn4bkqm6wcdii4m3") (y #t)))

(define-public crate-test-basanta-0.1.1 (c (n "test-basanta") (v "0.1.1") (h "1j9zhn8k7jlba2j9gp2sdh8lhni0jmqv4p9j7ijm5ypxr7k7m863") (y #t)))

(define-public crate-test-basanta-0.1.2 (c (n "test-basanta") (v "0.1.2") (d (list (d (n "basanta") (r "^0.1.2") (d #t) (k 0)))) (h "0xdbx65437ljpbi3xad6zsdhb8skv47ix71pxlq529scr1wprb9b") (y #t)))

(define-public crate-test-basanta-0.1.3 (c (n "test-basanta") (v "0.1.3") (h "0r1y7ypyw1xln21daky4bnlnzimp2gipprvya2bs22higr5718xd") (y #t)))

(define-public crate-test-basanta-0.1.4 (c (n "test-basanta") (v "0.1.4") (h "0cw6sfp21n6yvkgq7yg8dy27znqfhyjvw03xmhsdqkck4cv7swgd") (y #t)))

