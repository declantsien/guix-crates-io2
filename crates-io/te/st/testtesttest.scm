(define-module (crates-io te st testtesttest) #:use-module (crates-io))

(define-public crate-testtesttest-0.1.0 (c (n "testtesttest") (v "0.1.0") (h "00lhcp9ip1pvcdmc7zb25ck19crs9xghx6xl47zhcffjhyk4glgg") (y #t)))

(define-public crate-testtesttest-0.2.0 (c (n "testtesttest") (v "0.2.0") (h "070pwrzzlsgz88ffqfqaf6g2qphfcmfsphw8chmayjzqkavsmrk3") (y #t)))

(define-public crate-testtesttest-0.3.0 (c (n "testtesttest") (v "0.3.0") (h "0cnczwlp08v91srg29bz0i33gsd843fivrvcy8gp9v18ldfhw5j9") (y #t)))

