(define-module (crates-io te st test-with-tokio) #:use-module (crates-io))

(define-public crate-test-with-tokio-0.1.0 (c (n "test-with-tokio") (v "0.1.0") (d (list (d (n "test-with-tokio-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt"))) (d #t) (k 2)))) (h "104srfsw7dakik800p23nl48214f31kg0wyrd7z8knl4q13jhvm5") (r "1.63")))

(define-public crate-test-with-tokio-0.2.0 (c (n "test-with-tokio") (v "0.2.0") (d (list (d (n "test-with-tokio-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt"))) (d #t) (k 2)))) (h "0mmgaqklsd7xc724w46n5gzyx47isjfm7hkq1df6ff2zlrarlja2") (r "1.63")))

(define-public crate-test-with-tokio-0.2.1 (c (n "test-with-tokio") (v "0.2.1") (d (list (d (n "test-with-tokio-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt"))) (d #t) (k 2)))) (h "1hdn3rrk301q5mspb7jscw6z6zfplvqnp7rg6h2k8xz01dig5a8a") (r "1.63")))

(define-public crate-test-with-tokio-0.3.0 (c (n "test-with-tokio") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 2)) (d (n "test-with-tokio-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt" "time"))) (d #t) (k 2)))) (h "04ybkpf4raiw4rmgxx595bcw0iaw73d54rkr1kc7wzywy6fm79dd") (r "1.63")))

(define-public crate-test-with-tokio-0.3.1 (c (n "test-with-tokio") (v "0.3.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 2)) (d (n "test-with-tokio-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt" "time" "fs" "io-util"))) (d #t) (k 2)))) (h "0l7jisil90k3095ikrmr6mq16kw3rfy0x55w46i0ch46hxp2cvnq") (r "1.63")))

(define-public crate-test-with-tokio-0.3.2 (c (n "test-with-tokio") (v "0.3.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 2)) (d (n "test-with-tokio-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt" "time" "fs" "io-util"))) (d #t) (k 2)))) (h "09rmmdppaxbp70vvrpbmf8zgiyvbnjidapchrd0zxjkqmnmcnwg0") (r "1.63")))

(define-public crate-test-with-tokio-0.3.3 (c (n "test-with-tokio") (v "0.3.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 2)) (d (n "test-with-tokio-macros") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt" "time" "fs" "io-util"))) (d #t) (k 2)))) (h "10gsfz1nvrcgwi8kmgr61awspanyxl1wzcg060lk0k5r7h1yahrr") (r "1.63")))

