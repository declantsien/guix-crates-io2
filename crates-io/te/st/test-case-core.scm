(define-module (crates-io te st test-case-core) #:use-module (crates-io))

(define-public crate-test-case-core-3.0.0 (c (n "test-case-core") (v "3.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "120v9dpm5c1b8wbg49bgd2xszwi8vj2x00k5cp234h3zi2sj3p3j") (f (quote (("with-regex"))))))

(define-public crate-test-case-core-3.1.0 (c (n "test-case-core") (v "3.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vhckjjw4w1kaanhgy6bw8kdr8ligr68bz5nfbvr8xz2s7ar80yi") (f (quote (("with-regex"))))))

(define-public crate-test-case-core-3.2.0 (c (n "test-case-core") (v "3.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0plamwqlfxjryp88mb070xmzi4szp3mjjhmzda8qp73c5y249l0b") (f (quote (("with-regex"))))))

(define-public crate-test-case-core-3.2.1 (c (n "test-case-core") (v "3.2.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qjlxz8wrfpsd7wkfrn92m3pwvxa7244wqsph4qxgz7mp0n5xhjl") (f (quote (("with-regex"))))))

(define-public crate-test-case-core-3.3.0 (c (n "test-case-core") (v "3.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05gd29ypcz3svavj2rgiqbfjk712nh9xwhwqq0v1rhf9hrsyi2zi") (f (quote (("with-regex"))))))

(define-public crate-test-case-core-3.3.1 (c (n "test-case-core") (v "3.3.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0krqi0gbi1yyycigyjlak63r8h1n0vms7mg3kckqwlfd87c7zjxd") (f (quote (("with-regex"))))))

