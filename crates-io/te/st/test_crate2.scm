(define-module (crates-io te st test_crate2) #:use-module (crates-io))

(define-public crate-test_crate2-0.1.0 (c (n "test_crate2") (v "0.1.0") (d (list (d (n "test_crate_01") (r "^0.1.0") (d #t) (k 0)))) (h "0nqhfcg0vlyj4js88ahqq6zszlw2gp3qin9pillmb3ak2nzd0f03")))

(define-public crate-test_crate2-0.1.3 (c (n "test_crate2") (v "0.1.3") (d (list (d (n "test_crate_01") (r "^0.1.0") (d #t) (k 0)))) (h "17m8lb74c541z8nwdw813vgc3hv7ayf32lvqhcrz3mpgi41zyisn")))

(define-public crate-test_crate2-0.1.4 (c (n "test_crate2") (v "0.1.4") (d (list (d (n "test_crate_01") (r "^0.1.6") (d #t) (k 0)))) (h "1m2vzkcp57kxvvjadarcglagbs9fi1vp4kfwnjvfv25bhpkf0ph4")))

(define-public crate-test_crate2-0.2.4 (c (n "test_crate2") (v "0.2.4") (h "11srczq3p3yb0jkj5f75c9gvqxbhc6fmsj9gvdd11km7zjxvh6gd")))

