(define-module (crates-io te st test-temp-file) #:use-module (crates-io))

(define-public crate-test-temp-file-0.1.0 (c (n "test-temp-file") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0qkwfkfp6smbc9wvqq7sjlvlqvydwimfg4yk1izjp865a88lx75m")))

(define-public crate-test-temp-file-0.1.1 (c (n "test-temp-file") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1zxs7z0i4xch60b4jcajdnqlvw1ggzaahr7ccp1dd2kjl10wz89v")))

(define-public crate-test-temp-file-0.1.2 (c (n "test-temp-file") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1c2qhj1gyaxsdyy5xpqsa4kdaib8jlwzs545fki2si26g2v8m2cn")))

