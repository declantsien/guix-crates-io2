(define-module (crates-io te st testing_rust) #:use-module (crates-io))

(define-public crate-testing_rust-0.1.0 (c (n "testing_rust") (v "0.1.0") (d (list (d (n "mongodb") (r "^1.2.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0dw0vbvsjaadcmsz4gn1g0mswp89cn4xwpqya1qllzqvib0vqn2d") (y #t)))

