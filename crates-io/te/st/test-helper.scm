(define-module (crates-io te st test-helper) #:use-module (crates-io))

(define-public crate-test-helper-0.1.0 (c (n "test-helper") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "04b4n7v62vxk0mibl0hkkx5ph4hl1rxy08qi8n0gds8vbc9w14g9")))

