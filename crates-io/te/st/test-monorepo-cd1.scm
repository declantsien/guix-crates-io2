(define-module (crates-io te st test-monorepo-cd1) #:use-module (crates-io))

(define-public crate-test-monorepo-cd1-0.1.0 (c (n "test-monorepo-cd1") (v "0.1.0") (d (list (d (n "test-monorepo-cd") (r "^0.1.0") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.1.0") (d #t) (k 0)))) (h "18aya5r1rfk1nhq9lqig8smk0cyxr5hj2wdhhaw60x876030lw3r")))

(define-public crate-test-monorepo-cd1-0.1.1 (c (n "test-monorepo-cd1") (v "0.1.1") (d (list (d (n "test-monorepo-cd") (r "^0.1.0") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.1.0") (d #t) (k 0)))) (h "1jfcyqdl3arkiviigjgswc63aka8459pbq7rpq09fjif1md9in61")))

(define-public crate-test-monorepo-cd1-0.2.0 (c (n "test-monorepo-cd1") (v "0.2.0") (d (list (d (n "test-monorepo-cd") (r "^0.2.0") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.0") (d #t) (k 0)))) (h "0qwfb9sjywsidvifkh5732shiq52crkprq629j2bcbnk4ivvz95q")))

(define-public crate-test-monorepo-cd1-0.2.1 (c (n "test-monorepo-cd1") (v "0.2.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.0") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.0") (d #t) (k 0)))) (h "0pw54ffa5sw59p4kp9b557xphpjvzq1l5mi2iy387dfx1zyjwrk7")))

(define-public crate-test-monorepo-cd1-0.2.2-alpha.1 (c (n "test-monorepo-cd1") (v "0.2.2-alpha.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.2-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.2-alpha.1") (d #t) (k 0)))) (h "0kksagqrxi52363snqhwmwvwkqhj8z747gnvk6qw1dy86fhg4gjv")))

(define-public crate-test-monorepo-cd1-0.2.2 (c (n "test-monorepo-cd1") (v "0.2.2") (d (list (d (n "test-monorepo-cd") (r "^0.2.0") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.0") (d #t) (k 0)))) (h "061gcsvq3dy280n5ygc12x3qbfinqn977yq9pbj9yxs080742vs5")))

(define-public crate-test-monorepo-cd1-0.3.0 (c (n "test-monorepo-cd1") (v "0.3.0") (d (list (d (n "test-monorepo-cd") (r "^0.3.0") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.3.0") (d #t) (k 0)))) (h "09my5g5asq1k15d4q2fdq60d4abnbc02ari74sanh313ipvjambp")))

(define-public crate-test-monorepo-cd1-0.2.2-beta.1 (c (n "test-monorepo-cd1") (v "0.2.2-beta.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.2-beta.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.2-beta.1") (d #t) (k 0)))) (h "1xm6v476rilpwaffibran4d180f1mvfma5wq4hp8vkvl1fa5nyrw")))

(define-public crate-test-monorepo-cd1-0.2.2-rc.1 (c (n "test-monorepo-cd1") (v "0.2.2-rc.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.2-rc.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.2-rc.1") (d #t) (k 0)))) (h "1c0pddirwr8bswqwn7w9pvzs10jb0kxcj6cf9gnqigqlfx7jn8sj")))

(define-public crate-test-monorepo-cd1-1.0.0 (c (n "test-monorepo-cd1") (v "1.0.0") (d (list (d (n "test-monorepo-cd") (r "^1.0.0") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^1.0.0") (d #t) (k 0)))) (h "1v1k05xdgn0p59sm8m5ihp9d7rqw1gpiawa4jmz7jvzfwnj039d4")))

(define-public crate-test-monorepo-cd1-0.2.3-alpha.1 (c (n "test-monorepo-cd1") (v "0.2.3-alpha.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "10570qdgi9xjy11qir4fvd00bwb7q7vc7mxlg87fs53micjmmybh")))

(define-public crate-test-monorepo-cd1-0.2.3-alpha.2 (c (n "test-monorepo-cd1") (v "0.2.3-alpha.2") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "0s64a738r913z7dkp64wqbrvxw631ks4vlfadx04l17pp4waxvam")))

(define-public crate-test-monorepo-cd1-0.2.3-alpha.3 (c (n "test-monorepo-cd1") (v "0.2.3-alpha.3") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "1sdv4w4ab3zdf7y1kw0z5r7hx2y7fvjcmjb1sbi4pzdhl4y9j847")))

(define-public crate-test-monorepo-cd1-0.2.3-alpha.4 (c (n "test-monorepo-cd1") (v "0.2.3-alpha.4") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "0c31fc0m0nd0nda8dlkyxcri2qixnmscvqy9igafba0k81mn5s6j")))

(define-public crate-test-monorepo-cd1-0.2.3-alpha.5 (c (n "test-monorepo-cd1") (v "0.2.3-alpha.5") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "1mjidmmrqaqm674g3snsf479w2xn5r1jhzj2yrhihniq8xl68mm7")))

(define-public crate-test-monorepo-cd1-0.2.3 (c (n "test-monorepo-cd1") (v "0.2.3") (d (list (d (n "test-monorepo-cd") (r "^0.2.3-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.3-alpha.1") (d #t) (k 0)))) (h "065fcn2qydqnmjn3ylq321mzylh2v2gbbnvx8vbv3dlbgblrahng")))

(define-public crate-test-monorepo-cd1-0.2.4-rc.1 (c (n "test-monorepo-cd1") (v "0.2.4-rc.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.4-rc.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.4-rc.1") (d #t) (k 0)))) (h "1xcka14622bimqfnn3dfxdwz7mng2ddp2i4pkn93r02015mswfp8")))

(define-public crate-test-monorepo-cd1-0.2.4 (c (n "test-monorepo-cd1") (v "0.2.4") (d (list (d (n "test-monorepo-cd") (r "^0.2.4-rc.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.4-rc.1") (d #t) (k 0)))) (h "1y2l9d6a99nif8zmndd07mnnpjv171lgxh1gg4ch1k8jyhsi0dp7")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.1 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "0jz6h73858xql7p6ay9dls13l15jw4iqnrx4cmz9lkiask5mf6gj")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.2 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.2") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1l4gmdkl2ghwc68mnn802zhckik76vvsr4m2v207i1zyfdpr2rni")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.3 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.3") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "0dldc6w921zdvb32amgd86ld083piqg1d3na0vm4g3cf45442i5q")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.4 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.4") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "16gynm7ybw4y98jmncvk1p2ff5ph91ypabn8n8ynn5xagvxggllf")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.5 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.5") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "00023s6mdv1vrf9rjzmmymj5kp4w47brkwqh1npfs75p9fm5ahsk")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.6 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.6") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1n3lpq1cx8mm7szh1jw17phy3nm7cxjfxw0iqv4hxq3gqs2rvnm3")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.7 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.7") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "0vrjc9lmpm1k18q1gqhvh0kp65vrjxxw549zp7hi7wvzaq2mlmx8")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.8 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.8") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "09zla8c14pvyfa2hazcwwr04vysbjyp2zaj4v8g22v4c2b3ss8da")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.9 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.9") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "12xnzw4nlpa7zkwk9x0vlqwl5x0p4k57rdainwrw4qxvw0dmbqln")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.10 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.10") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "0ldjgh84g1x2b8b3fizbc9szc4vr5lc65nfm3v69mnqixaca805c")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.11 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.11") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "0nq2rn0f2nhj3r7xfsv6kbxn2yj5bmhcx3r4f3ykyvw4k7mvcbcy")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.12 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.12") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "1zvhjfb46g3sq0ygbgs0x51m0yrlvsbmmqvn0fxvapy72fynll1w")))

(define-public crate-test-monorepo-cd1-0.2.5-alpha.13 (c (n "test-monorepo-cd1") (v "0.2.5-alpha.13") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "0n78ikp28ipik7xc3lcnv7ib10bp781jj7cmqkfy68v2l0xg7abg")))

(define-public crate-test-monorepo-cd1-0.2.5 (c (n "test-monorepo-cd1") (v "0.2.5") (d (list (d (n "test-monorepo-cd") (r "^0.2.5-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.5-alpha.1") (d #t) (k 0)))) (h "0p2p6j9lw56rjhwlmyrrvdibgxagxp5z3kwr37a1s5jxg7yc8lxz")))

(define-public crate-test-monorepo-cd1-0.2.6-alpha.1 (c (n "test-monorepo-cd1") (v "0.2.6-alpha.1") (d (list (d (n "test-monorepo-cd") (r "^0.2.6-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.6-alpha.1") (d #t) (k 0)))) (h "05sxxanwqskqjdwf8vya5kp8wqcwnspvj46n1i2c0vdpm7srzriq")))

(define-public crate-test-monorepo-cd1-0.2.6-alpha.2 (c (n "test-monorepo-cd1") (v "0.2.6-alpha.2") (d (list (d (n "test-monorepo-cd") (r "^0.2.6-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.6-alpha.1") (d #t) (k 0)))) (h "1f3iwzl760mmbg1rxllbl6bm1pdawj78090v02a5vr3kmidhmv1b")))

(define-public crate-test-monorepo-cd1-0.2.6-alpha.3 (c (n "test-monorepo-cd1") (v "0.2.6-alpha.3") (d (list (d (n "test-monorepo-cd") (r "^0.2.6-alpha.1") (d #t) (k 0)) (d (n "test-monorepo-cd2") (r "^0.2.6-alpha.1") (d #t) (k 0)))) (h "0id0gl0snqlzx3fd7wn2svfhg29kskcpjm9yl55v06yicnxlax7z")))

