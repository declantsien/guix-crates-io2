(define-module (crates-io te st testdb) #:use-module (crates-io))

(define-public crate-testdb-0.0.1 (c (n "testdb") (v "0.0.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("postgres" "runtime-async-std-rustls"))) (k 0)))) (h "0vv1qy9jrhbs9cvxgxg5d0kqi85rvh0s7lz0dbhk6w2w4ddccxbf")))

