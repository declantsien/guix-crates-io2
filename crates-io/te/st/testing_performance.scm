(define-module (crates-io te st testing_performance) #:use-module (crates-io))

(define-public crate-testing_performance-0.1.1 (c (n "testing_performance") (v "0.1.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "0827y5fqxc1iilyhkmmjhdinbfqjq3bsmnxrrnqxyjbh281z2f4r")))

(define-public crate-testing_performance-0.1.2 (c (n "testing_performance") (v "0.1.2") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "036pw16bfqjzfxdq90ca6fbzvia646cjr84qsqp2jz2brwb4vmi5")))

(define-public crate-testing_performance-0.1.3 (c (n "testing_performance") (v "0.1.3") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "0lcm537yvspwlq9zmcgh5g3aa4j9dgq1hvpgpmmf0yplmqa99bcf")))

(define-public crate-testing_performance-0.1.4 (c (n "testing_performance") (v "0.1.4") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "1m9gki1iyvy42v892k9xigiwbpxgksf044hybw7yc7yv1v5i6mz4")))

(define-public crate-testing_performance-0.1.5 (c (n "testing_performance") (v "0.1.5") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)))) (h "08cyiaxm3afq6zfww1dlc6h7hay6cpfm4blqa61szxkvbahlj9cb")))

