(define-module (crates-io te st testresult) #:use-module (crates-io))

(define-public crate-testresult-0.1.0 (c (n "testresult") (v "0.1.0") (h "0vi1zxqznipsw7qm8874kkg9rkxhlqzfv429zz5f5nfhcg34prw1")))

(define-public crate-testresult-0.2.0 (c (n "testresult") (v "0.2.0") (h "1vy5ji75by0a1p3b37ic686j185x4ids0513y1w9zdgaasmi74b4")))

(define-public crate-testresult-0.3.0 (c (n "testresult") (v "0.3.0") (h "0nf4qm0b3jjk9sk104sspf5zhpd7cxajyrf9q5r9gmlsrzslbq2j")))

(define-public crate-testresult-0.4.0 (c (n "testresult") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1r4rbab9rzksz3c8zsjcxabs2hfr1fvkjw98n24nby210mf2abnp")))

