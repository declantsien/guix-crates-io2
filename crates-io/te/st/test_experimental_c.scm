(define-module (crates-io te st test_experimental_c) #:use-module (crates-io))

(define-public crate-test_experimental_c-0.1.0 (c (n "test_experimental_c") (v "0.1.0") (h "0l6dkvn4s94vg16f941cnyqhki76mwqsl284fj5pvfb6qd80i73j")))

(define-public crate-test_experimental_c-0.1.1 (c (n "test_experimental_c") (v "0.1.1") (h "1cb0jm3g45r5ah2prcld0b5wrv2rrbc44rxgb8y1qjw9ky63f4kn")))

(define-public crate-test_experimental_c-0.2.0 (c (n "test_experimental_c") (v "0.2.0") (h "1c2g1h29khnpi9gq3hbg9xnybfm7fvrjxwzjncpmdi8yi3n47b7q")))

(define-public crate-test_experimental_c-0.3.0 (c (n "test_experimental_c") (v "0.3.0") (h "158wkmsvxnaz453km31gqv48b1pcs9nsfa6ca16q2fcl89z5v5f4")))

(define-public crate-test_experimental_c-0.4.0 (c (n "test_experimental_c") (v "0.4.0") (h "1g7wl4ryhzrsqydz8482ydvdmcc3i0wnb53qpjf18v2b8178s62j")))

