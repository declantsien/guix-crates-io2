(define-module (crates-io te st testy) #:use-module (crates-io))

(define-public crate-testy-0.0.1 (c (n "testy") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0zgaskanbl4wnhl5chbv19x1igrqicxinv9jdr10azr9az57zhwj") (y #t)))

(define-public crate-testy-0.0.2 (c (n "testy") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "04gg1yhrnb4kdkmwip5fpv7yxmrn37d4q6pac1mjqwdrljhm62zr") (y #t)))

(define-public crate-testy-0.0.3 (c (n "testy") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0y9wab5b10i3mfc52niaaj120wmh19sjdq6q14a4prni3a3vlh97") (y #t)))

(define-public crate-testy-0.0.4 (c (n "testy") (v "0.0.4") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1fajj8399sbajli74bqgbkk7r44c7lj8fijjnlyqvqhz3cml16w9") (y #t)))

(define-public crate-testy-0.0.5 (c (n "testy") (v "0.0.5") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0774gi8dklajd233z49fml87dyl42i4jncnlg2j476yydjrcsjb5") (y #t)))

(define-public crate-testy-0.1.0 (c (n "testy") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1bc16xhr4362fa6pagr8bvp5wlvqj84j3yk1q1anb26hcrl7i89n") (y #t)))

(define-public crate-testy-0.1.1 (c (n "testy") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1i7nfhv9k74hy8f6gwm650f3af298drrqjnscjh2r2l3aqjds7d2") (y #t)))

(define-public crate-testy-0.2.0 (c (n "testy") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "035zripca62jz4a4xja6mr0wb5d0wzb7bjh20hw212zx74070cki") (y #t)))

(define-public crate-testy-0.2.1 (c (n "testy") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0fdv429g2j0fgfyfd1ci2glfds38x6awa5js3ym23fnjkqwslbil") (y #t)))

(define-public crate-testy-0.2.2 (c (n "testy") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "16lszmwzpm3bywnad4ipr34n62b9nnmw7vv4cwv3c32rvxz9yaam") (y #t)))

(define-public crate-testy-0.2.3 (c (n "testy") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0v7ld2xd5gj70y0ivkd1g8aj11s7hs84185f9b6qcs2cyxflrlhd") (y #t)))

(define-public crate-testy-0.2.4 (c (n "testy") (v "0.2.4") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1mdp3x1432s9ganxl10vy4j6jw4c45v4s9hrk3ziqchkbs1sxnk0") (y #t)))

(define-public crate-testy-0.2.5 (c (n "testy") (v "0.2.5") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0fkdl6b1j9jd5b3g59av1zp0djnr27cx3wifrg09s6liyq15nr4i") (y #t)))

(define-public crate-testy-0.2.6 (c (n "testy") (v "0.2.6") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1zgf13miwdr1a7f4zb1zkg5hyibm570798mmmwplwig80l43l3d5") (y #t)))

(define-public crate-testy-0.2.7 (c (n "testy") (v "0.2.7") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "06a1627a749z6224m8qcs2adh1c7q0r8yxhghry22a76pgbj70m5") (y #t)))

(define-public crate-testy-0.2.8 (c (n "testy") (v "0.2.8") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1a111rb8sv8rj14q4vgx18y7hcnagpnxb5jvcys429pdalb0160n") (y #t)))

(define-public crate-testy-0.2.9 (c (n "testy") (v "0.2.9") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0y7gxvyaddj5p1mf0qrv95q27wg1xifq1981qvgq5aiaqwp5gmf4") (y #t)))

(define-public crate-testy-0.3.0 (c (n "testy") (v "0.3.0") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "1m4hivpdgnbxbfygqwfzqvnqglrgvmbcyf67jlllzparl3m5wmmz") (y #t)))

(define-public crate-testy-0.3.1 (c (n "testy") (v "0.3.1") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "1nhlhgj9jw9smzv6aqkw4lsxgdrvm0faslr2d9kypgf2z86yndgf") (y #t)))

