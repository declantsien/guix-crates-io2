(define-module (crates-io te st testpublish_gsg) #:use-module (crates-io))

(define-public crate-testpublish_gsg-0.0.1 (c (n "testpublish_gsg") (v "0.0.1") (d (list (d (n "static-toml") (r "^1.0.1") (d #t) (k 0)))) (h "1d9k01mfm69kjr2w0f6yqg77lk8wrz6v9ymwcl9g07ax3ysvbzyk") (y #t) (r "1.68")))

(define-public crate-testpublish_gsg-0.0.2 (c (n "testpublish_gsg") (v "0.0.2") (d (list (d (n "static-toml") (r "^1.0.1") (d #t) (k 0)))) (h "0pfccj0adgszj3z7yzz2xkrm62fa10bpn8pwpz3qbjbi8qqcl5s5") (y #t) (r "1.68")))

(define-public crate-testpublish_gsg-0.0.3 (c (n "testpublish_gsg") (v "0.0.3") (d (list (d (n "static-toml") (r "^1.0.1") (d #t) (k 0)))) (h "169bfispaf4fsy4yiam93nknpyvls7wvwjmjv3a1xdr633ra53i0") (y #t) (r "1.68")))

