(define-module (crates-io te st test-generator-utest) #:use-module (crates-io))

(define-public crate-test-generator-utest-0.1.0 (c (n "test-generator-utest") (v "0.1.0") (h "1qnp4f3q2c0gx96af9851z8arb7a7w9aw7ydqj5xg2bp6ia7bfvk")))

(define-public crate-test-generator-utest-0.1.1 (c (n "test-generator-utest") (v "0.1.1") (h "1grw6jwzcxqdgq2jg8hq17qa1q693lm2wqv9kxnaqkwxyp2884jd")))

(define-public crate-test-generator-utest-0.1.2 (c (n "test-generator-utest") (v "0.1.2") (h "0js0sx6pxnay28jxlmib962qlq684ff037f6hw3ibjn9bvvdyww4")))

