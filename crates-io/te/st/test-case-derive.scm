(define-module (crates-io te st test-case-derive) #:use-module (crates-io))

(define-public crate-test-case-derive-0.1.0 (c (n "test-case-derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "17cywyfxv8y01w1pm3w2py2fmkcw0bzm8hayd15rc6xipgady69n")))

(define-public crate-test-case-derive-0.1.1 (c (n "test-case-derive") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0qn18qrn6p77k77wnzkxxrxwr8dzvqdirnzqjzgp9sm4k1aag53b")))

(define-public crate-test-case-derive-0.1.2 (c (n "test-case-derive") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0vjhz7ncn6bh0da32hknvlgxy188nqmnm5lyb7g8jm5m90s2cih6")))

(define-public crate-test-case-derive-0.1.3 (c (n "test-case-derive") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1m7l3w4wrc74xkkwq5zbbl4lj0fb4z3xa37f7328wk6gpkhjqy2y")))

(define-public crate-test-case-derive-0.1.4 (c (n "test-case-derive") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0v1qgj6965zvhp0k00xm7p22cxyr9qnp40h5bjncz3w20cqlcb6b")))

(define-public crate-test-case-derive-0.1.5 (c (n "test-case-derive") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1ix8hrlar6lr1dda5rij6dxbwqdjvl4rk4gsizn5cgb4sdn2a8ff")))

(define-public crate-test-case-derive-0.2.0 (c (n "test-case-derive") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0q4sd4bwzzis186jrk746011khag7gpxjilbyg3pxndc5q3c4ihb")))

(define-public crate-test-case-derive-0.2.1 (c (n "test-case-derive") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0r83clvqybwhd67r9pgf49jb135c592n9d4dahn3ph09nbd79gf3")))

(define-public crate-test-case-derive-0.2.2 (c (n "test-case-derive") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1j6p9lkvf6nwih28gll8h32avcvs5pk9dlm83k4bd4c0bg0vyps5")))

(define-public crate-test-case-derive-0.2.3 (c (n "test-case-derive") (v "0.2.3") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0izkkncbsfv71km1wcbzmvdk9fi2fz9ysk2kqh8n9ikvss6f23xa")))

