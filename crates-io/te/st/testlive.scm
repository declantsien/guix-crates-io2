(define-module (crates-io te st testlive) #:use-module (crates-io))

(define-public crate-testlive-0.1.0 (c (n "testlive") (v "0.1.0") (h "0l2bw7m15rv2bh49dyk9mjqcbpnr7j6a99c5v5vxifx1r6784h7r")))

(define-public crate-testlive-0.2.0 (c (n "testlive") (v "0.2.0") (h "1ilqqpnhpkqxndcjmixnac9l09ldm7zlm8p17v0rp1f9l5xaq3yv")))

