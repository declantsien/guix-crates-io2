(define-module (crates-io te st test_ryle_hello_crates) #:use-module (crates-io))

(define-public crate-test_ryle_hello_crates-0.1.0 (c (n "test_ryle_hello_crates") (v "0.1.0") (h "0pys05336bdkr5gn7z69yk6y1vw72s2przqlzn0i6s94my6vs4rq") (y #t)))

(define-public crate-test_ryle_hello_crates-0.1.1 (c (n "test_ryle_hello_crates") (v "0.1.1") (h "1hfv2sczpw13pb3rs4g64pk8w8rxny55yfaz3bb97idgwgdz24gh")))

(define-public crate-test_ryle_hello_crates-0.1.2 (c (n "test_ryle_hello_crates") (v "0.1.2") (h "1fj5vabmzcrvwsjp035mn7jghg96nhiahwd6kar4fvpws4lpyfwl")))

