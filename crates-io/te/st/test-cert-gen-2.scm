(define-module (crates-io te st test-cert-gen-2) #:use-module (crates-io))

(define-public crate-test-cert-gen-2-0.11.0 (c (n "test-cert-gen-2") (v "0.11.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pem") (r "^3.0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0nxck59570h5hydp1h53m938hjbk4mj2fa3jhhgcj4km7ikl270l")))

