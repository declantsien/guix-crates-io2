(define-module (crates-io te st test-assembler) #:use-module (crates-io))

(define-public crate-test-assembler-0.1.0 (c (n "test-assembler") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3.13") (d #t) (k 0)))) (h "0vhw40rdcngq1yq83piwf11zqh53y5pcr10aj6b15d1jjsq1fsl1")))

(define-public crate-test-assembler-0.1.1 (c (n "test-assembler") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "0s423vy3ddkph3lav9r7228pnx7id1m9psvcjv9mix23qazr2gp7")))

(define-public crate-test-assembler-0.1.2 (c (n "test-assembler") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)))) (h "1p68ynhxxsblfv32vx1jmxrxi4gkgkyilzpmrlv6bwk2kvc4qdp3")))

(define-public crate-test-assembler-0.1.3 (c (n "test-assembler") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)))) (h "17iv2gcffkpd44ch85k8lbw0ps00bcfv7ihql2ilw0wd3bgwx4k8")))

(define-public crate-test-assembler-0.1.4 (c (n "test-assembler") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)))) (h "0fdigkysd6j7znilm6hdndl713xiivnmvz0awcdnhl46gxbd9m6a")))

(define-public crate-test-assembler-0.1.5 (c (n "test-assembler") (v "0.1.5") (d (list (d (n "byteorder") (r "^0.5.0") (d #t) (k 0)))) (h "1sdx9hk0dk3z9crm8834ysyxsi92chls8arpd0gs796kis6lik2w")))

(define-public crate-test-assembler-0.1.6 (c (n "test-assembler") (v "0.1.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "10mm5mzw9wy530pqm8r7awy5pmyc7kkxarzs8df3yia9w4fsavcs")))

