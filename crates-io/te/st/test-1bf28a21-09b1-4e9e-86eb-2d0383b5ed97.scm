(define-module (crates-io te st test-1bf28a21-09b1-4e9e-86eb-2d0383b5ed97) #:use-module (crates-io))

(define-public crate-test-1bf28a21-09b1-4e9e-86eb-2d0383b5ed97-0.1.0 (c (n "test-1bf28a21-09b1-4e9e-86eb-2d0383b5ed97") (v "0.1.0") (h "0m3jbbds5q7in9qpj3lcj78sxj5kc0kgq56hq8vkwlknbk5dbfrc")))

(define-public crate-test-1bf28a21-09b1-4e9e-86eb-2d0383b5ed97-0.1.1 (c (n "test-1bf28a21-09b1-4e9e-86eb-2d0383b5ed97") (v "0.1.1") (h "00pw3wn4c8bcb1xqvkr7lhdr7ica99z6caj5mvqyjmdxm1hyji6h")))

(define-public crate-test-1bf28a21-09b1-4e9e-86eb-2d0383b5ed97-0.1.2 (c (n "test-1bf28a21-09b1-4e9e-86eb-2d0383b5ed97") (v "0.1.2") (h "1j4vrw04z3jvxy62228z5wcnqwzp1j3hsp086i1kpk5ilv2mp01d")))

