(define-module (crates-io te st test_1337) #:use-module (crates-io))

(define-public crate-test_1337-0.1.0 (c (n "test_1337") (v "0.1.0") (h "1qz2jx2r9s2h43w6p8f3ryibgncahljnfwf343swii5kk83m8igh")))

(define-public crate-test_1337-0.1.1 (c (n "test_1337") (v "0.1.1") (h "149a2m76f63jvs600lmh4icbn0ragk6c4ard0bifbq77mgf10fw9")))

(define-public crate-test_1337-0.1.2 (c (n "test_1337") (v "0.1.2") (h "182fjsc06d4xiw6xqpqgnmrfddvfnki7z9gz9f0n111mrr6fvksk")))

(define-public crate-test_1337-0.1.3 (c (n "test_1337") (v "0.1.3") (h "0w5g6xkzqzh3nsysh84cs5gx0ib8zs91lmxv3ii6dd0cm4w63ix3")))

(define-public crate-test_1337-0.1.4 (c (n "test_1337") (v "0.1.4") (h "0v4pbkrj07yqgm3yv4i6w567m87cw6xsshdvzy3mw0rhhlsss8iz")))

(define-public crate-test_1337-0.1.5 (c (n "test_1337") (v "0.1.5") (h "1vnvg0m006ydsqyjq0jz0v7qdz6n2jnw85bp0r39xgfjgchgxg8k")))

(define-public crate-test_1337-0.1.6 (c (n "test_1337") (v "0.1.6") (h "0y89nvnvrmr8cwi056i9i54sazvgp974njsqkgyrf1ccc19p1hnm")))

