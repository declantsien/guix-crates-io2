(define-module (crates-io te st test_ci) #:use-module (crates-io))

(define-public crate-test_ci-0.1.0 (c (n "test_ci") (v "0.1.0") (h "1l5a7zw0ckybv1n0jv2sbf3w5s7fsf78sjhzivdkn0nkfsz0d4kw")))

(define-public crate-test_ci-0.1.1 (c (n "test_ci") (v "0.1.1") (h "1vrqil9vq739scvdf660hjcmrky9nhy166vv3zca0wchwh6c30zc")))

(define-public crate-test_ci-0.1.2 (c (n "test_ci") (v "0.1.2") (h "0z20k9syfr93x360sm3xv0ld2dj5zgdy4q00byivhjl0f88dw0w5")))

(define-public crate-test_ci-0.1.3 (c (n "test_ci") (v "0.1.3") (h "1x51m2z4rc7dxnjj2d2pr33hyj8jvdxr7rz3jkdpmd0p83a5zwbv")))

