(define-module (crates-io te st test-group) #:use-module (crates-io))

(define-public crate-test-group-1.0.0 (c (n "test-group") (v "1.0.0") (d (list (d (n "hex") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0fwcq5vdzp3dahj6glksygzlr24j60niiqi63g8kr4rvam7sslcn")))

(define-public crate-test-group-1.0.1 (c (n "test-group") (v "1.0.1") (d (list (d (n "hex") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "sha2") (r "^0.10") (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1d4qxw5ypglhrbvqzq8mjaddc6zry9rvy06qwrgzqqzx1s0i20ki")))

