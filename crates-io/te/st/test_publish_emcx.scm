(define-module (crates-io te st test_publish_emcx) #:use-module (crates-io))

(define-public crate-test_publish_emcx-0.1.0 (c (n "test_publish_emcx") (v "0.1.0") (h "0k6arxxxmxw6wx8gn5bhp5565lqszxv1qxv1pwgaig755aapyk2g")))

(define-public crate-test_publish_emcx-0.2.0 (c (n "test_publish_emcx") (v "0.2.0") (h "1z13wqb3rfigsn17lj0zxdijxdkmn6xjshipnpcz65nq30fi8kkp")))

(define-public crate-test_publish_emcx-0.3.0 (c (n "test_publish_emcx") (v "0.3.0") (h "0lmlnnbhzjqfb5h3d7v9mir3m1c5lpx20clr593jngc8z3qp1brg")))

