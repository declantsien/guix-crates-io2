(define-module (crates-io te st test-monorepo-cd) #:use-module (crates-io))

(define-public crate-test-monorepo-cd-0.1.0 (c (n "test-monorepo-cd") (v "0.1.0") (h "0w23zg7zqb68khqz01y1d8rgsjsb62wcwfkmk8rf1c8y1gadrx4s")))

(define-public crate-test-monorepo-cd-0.1.1 (c (n "test-monorepo-cd") (v "0.1.1") (h "0czcqgn3pmf27c0cwhpifxvzcd30hcqg00qrcf5w9386c3nlpzh2")))

(define-public crate-test-monorepo-cd-0.2.0 (c (n "test-monorepo-cd") (v "0.2.0") (h "03h4w6c6wabrk1adrchq9vrccs1b1fb2hk3dsw0i7wmm831cwbyd")))

(define-public crate-test-monorepo-cd-0.2.1 (c (n "test-monorepo-cd") (v "0.2.1") (h "03qllpq2acwlgzniizlfkf653v0hl8iwd4n92n6pqnk0zlq4h76d")))

(define-public crate-test-monorepo-cd-0.2.2-alpha.1 (c (n "test-monorepo-cd") (v "0.2.2-alpha.1") (h "1rmzra1xb23j55w15b7p3cdrwls2g3hcjj0bxillxzhpgmfvdzy5")))

(define-public crate-test-monorepo-cd-0.2.2 (c (n "test-monorepo-cd") (v "0.2.2") (h "14pps2r255ibgx7bf905isdqr998zrd8xs2l79ypj6hlbkmgyh13")))

(define-public crate-test-monorepo-cd-0.3.0 (c (n "test-monorepo-cd") (v "0.3.0") (h "1gsl9k252pppn0gqlnsd1bw8zqcfrf4nwm1p71ys1lpqxskvx3ms")))

(define-public crate-test-monorepo-cd-0.2.2-beta.1 (c (n "test-monorepo-cd") (v "0.2.2-beta.1") (h "15r4knbjzf6j9gzr3sq1i2vh1nkidnvaw4g6ydgng5ihkz4kws4x")))

(define-public crate-test-monorepo-cd-0.2.2-rc.1 (c (n "test-monorepo-cd") (v "0.2.2-rc.1") (h "1pcc7ggasvc3x228z1b8axf86iivhk55x9ah9pzp1rh1iydqax29")))

(define-public crate-test-monorepo-cd-1.0.0 (c (n "test-monorepo-cd") (v "1.0.0") (h "0bbh8kwazhgjyx4df6yajwp80x8nxp03wspkl4xapd8g36g3x0n8")))

(define-public crate-test-monorepo-cd-0.2.3-alpha.1 (c (n "test-monorepo-cd") (v "0.2.3-alpha.1") (h "0jyc01ivvpw20ckg9v4riijk1rygslsnr0ga21zxrjqy5a5jl3ph")))

(define-public crate-test-monorepo-cd-0.2.3-alpha.2 (c (n "test-monorepo-cd") (v "0.2.3-alpha.2") (h "0j38lwh0xb29jgnhyzf0frqvcngjyp1j8z4c97p0f5mv7gbajlbp")))

(define-public crate-test-monorepo-cd-0.2.3-alpha.3 (c (n "test-monorepo-cd") (v "0.2.3-alpha.3") (h "15ihvnycbbflsm9xjap2i7hj5hjdlrak342s7awkgklkf36znay1")))

(define-public crate-test-monorepo-cd-0.2.3-alpha.4 (c (n "test-monorepo-cd") (v "0.2.3-alpha.4") (h "0py10wxpdkqdadw6bzj2i777qx41pm2hg28ccxg9zm19r2s7qbnh")))

(define-public crate-test-monorepo-cd-0.2.3-alpha.5 (c (n "test-monorepo-cd") (v "0.2.3-alpha.5") (h "0ih493ni4bfdpsgpsy28rvhc8spnyb7zsnkgv36qsndzqygqbn1a")))

(define-public crate-test-monorepo-cd-0.2.3 (c (n "test-monorepo-cd") (v "0.2.3") (h "1idr426shz0j78cw58xbmsqarmzl4d7i1pmkq2mp29b73ri4lwvz")))

(define-public crate-test-monorepo-cd-0.2.4-rc.1 (c (n "test-monorepo-cd") (v "0.2.4-rc.1") (h "0738rj7ad9wxah384v929s5sr20fik33jz8mgap58li2jpcil3m3")))

(define-public crate-test-monorepo-cd-0.2.4 (c (n "test-monorepo-cd") (v "0.2.4") (h "1k2rpjaz0hldp69i4idamkm814br7lviv5b8g0w72pz8mx1slmi4")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.1 (c (n "test-monorepo-cd") (v "0.2.5-alpha.1") (h "1a624clyf9fch2py7iv210v2sxbdzq0ikgxpw0pa0vpiqz001zl6")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.2 (c (n "test-monorepo-cd") (v "0.2.5-alpha.2") (h "1almhqaj1s003n8va8xk901nd0g4bs2b3853v6pqf89jwmdwhlpj")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.3 (c (n "test-monorepo-cd") (v "0.2.5-alpha.3") (h "03b4rzwrsr13gfcyib6453x03bahb9d6b1n44vcfdqs2h60f0d2b")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.4 (c (n "test-monorepo-cd") (v "0.2.5-alpha.4") (h "1li71brprqqf65z8d30qwj8jw16m5pdb8i1cjjwnmy8jg66my4fx")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.5 (c (n "test-monorepo-cd") (v "0.2.5-alpha.5") (h "0iqj0x5g2wyvziw3gs7hh2v683k41m6y29hy6x2bkznpg2f57x0d")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.6 (c (n "test-monorepo-cd") (v "0.2.5-alpha.6") (h "1cx2zjbjqhfbacs5irl93qbgyccmvfgl31f8mwgkc9gfpcpqg2dw")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.7 (c (n "test-monorepo-cd") (v "0.2.5-alpha.7") (h "045505v1y8bbk9673cqh5vz18ppv1hcgxbs2jwa29358mvvf7qp1")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.8 (c (n "test-monorepo-cd") (v "0.2.5-alpha.8") (h "1ars33vbinrpbgc8w301pglq5jjq78sf9z5imqws5cgkbh5abfjl")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.9 (c (n "test-monorepo-cd") (v "0.2.5-alpha.9") (h "0dzr3z3qya6nyccgdlwypxl74mfzz8jnjpha9dld4bsfb6hbpbnz")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.10 (c (n "test-monorepo-cd") (v "0.2.5-alpha.10") (h "1z9q4diwjp20lz0fp8h7f9qmchr8yyv7dr7cc7j734r50bpgvhck")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.11 (c (n "test-monorepo-cd") (v "0.2.5-alpha.11") (h "078jwkrpv63alwhjcfgnysq497l8zc6yb18xs9vf2qyspi9r6awi")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.12 (c (n "test-monorepo-cd") (v "0.2.5-alpha.12") (h "1gl2amqklqa21ml2z25if3nq6ic4q5jbvvfmsl3qhimg0x63cwdw")))

(define-public crate-test-monorepo-cd-0.2.5-alpha.13 (c (n "test-monorepo-cd") (v "0.2.5-alpha.13") (h "1rih3r3zaca6vxpz5q6p7r9z3izpcvyl34af8jx1hl37rm3gnhay")))

(define-public crate-test-monorepo-cd-0.2.5 (c (n "test-monorepo-cd") (v "0.2.5") (h "1nws849vijihlj172z2llqanx4pmhzvhcdzpj51jnphhndr8373z")))

(define-public crate-test-monorepo-cd-0.2.6-alpha.1 (c (n "test-monorepo-cd") (v "0.2.6-alpha.1") (h "16spimyb72pafh98z35ws8n6lwil8s0662zgpdlmm82pcsgyj5gi")))

(define-public crate-test-monorepo-cd-0.2.6-alpha.2 (c (n "test-monorepo-cd") (v "0.2.6-alpha.2") (h "1ffbqlmv34y9i1kw9x6n3ccif89xnj1sgj5kkrjmgyiaa0pkmyi3")))

(define-public crate-test-monorepo-cd-0.2.6-alpha.3 (c (n "test-monorepo-cd") (v "0.2.6-alpha.3") (h "1kf8cjl9q8gp1gfzxv5wk82jlhfgg1qr7a85vxc6b922q65cmxjk")))

