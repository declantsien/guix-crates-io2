(define-module (crates-io te st test-span-macro) #:use-module (crates-io))

(define-public crate-test-span-macro-0.1.0 (c (n "test-span-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kkn1apq19309lml6zyxxxdizmqhbabz7ys2yjb4si5vqfqs4i04")))

(define-public crate-test-span-macro-0.1.1 (c (n "test-span-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hbfxd3km7ixylhpvj4blv43fv5mjqdsz76g2x0c8s64i9mmvzfi")))

(define-public crate-test-span-macro-0.2.0 (c (n "test-span-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cr035ywd9xbb4n8j47ccxh8mgl2487lb6imm14sdhf3722qfrah")))

(define-public crate-test-span-macro-0.3.0 (c (n "test-span-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a53lzi1wzc5213cib3a3l8lfsgr72v8kp48avclwhnibhpw2qvm")))

(define-public crate-test-span-macro-0.4.0 (c (n "test-span-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1762my8lijs8rgw4i8xfyrrsp9qa7xhydz6zlhkd51n6zafabx3j")))

(define-public crate-test-span-macro-0.5.0 (c (n "test-span-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k1d21nm7ilvz7w5j057pv1cdp4yfshpq8yncvdwl8zxdhs3jj7f")))

(define-public crate-test-span-macro-0.6.0 (c (n "test-span-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hf8imr932k4k9j0z6qlf4wg6404wn4flinwhh401lf96m508a95")))

(define-public crate-test-span-macro-0.7.0 (c (n "test-span-macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17gbqr20sazbshgpazwpknkh9r50ndfp2jpf8xnvp0f7y92j95qz")))

(define-public crate-test-span-macro-0.8.0 (c (n "test-span-macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vqaryls19hd70va6c6db0arzbpiaa1b1vpl2fnb8z66ck6dfc0s")))

