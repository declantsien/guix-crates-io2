(define-module (crates-io te st testserver) #:use-module (crates-io))

(define-public crate-testserver-0.1.0 (c (n "testserver") (v "0.1.0") (d (list (d (n "ar") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "tiny_http") (r "^0.8") (d #t) (k 0)))) (h "1l9sysnmlnpy8a9ds6khgm4hpw9as9yasw4ljyx4m8pswbn3y0qv")))

(define-public crate-testserver-0.1.1 (c (n "testserver") (v "0.1.1") (d (list (d (n "ar") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "tiny_http") (r "^0.8") (d #t) (k 0)))) (h "01acphqbrafxgr8091vzccnha4x52nmp9a5gxz83gqig26ncg0n6")))

(define-public crate-testserver-0.1.2 (c (n "testserver") (v "0.1.2") (d (list (d (n "ar") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "tiny_http") (r "^0.8") (d #t) (k 0)))) (h "0n3bnlwdgkfg70wclrnbrvns3jb414qyq55j8yk09w6qkvwiiaqr")))

(define-public crate-testserver-0.1.3 (c (n "testserver") (v "0.1.3") (d (list (d (n "ar") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "tiny_http") (r "^0.8") (d #t) (k 0)))) (h "0jxwmxz7996k83j8ffcafrf4lva1ldpjx708d04zlvj8yzf6sq51")))

