(define-module (crates-io te st testcargo) #:use-module (crates-io))

(define-public crate-testCargo-0.1.0 (c (n "testCargo") (v "0.1.0") (d (list (d (n "claims") (r "^0.7.1") (d #t) (k 0)))) (h "0qr5387jzcdcikb4hph8vqmjxz15vir5fgz96ybg3dyqgy7sizbn")))

(define-public crate-testCargo-0.1.1 (c (n "testCargo") (v "0.1.1") (d (list (d (n "claims") (r "^0.7.1") (d #t) (k 0)) (d (n "deux") (r "^0.1.0") (d #t) (k 0)))) (h "0ys0mrbw9sh1qyk4yrwjhzgh88vlcm9qgxph2abw64fzwinjcy8f")))

(define-public crate-testCargo-0.1.2 (c (n "testCargo") (v "0.1.2") (d (list (d (n "claims") (r "^0.7.1") (d #t) (k 0)) (d (n "deux") (r "^0.1.0") (d #t) (k 0)))) (h "0kfc13bi0qf5z96k1q1awijvzxqz4zi6q6sh0v0hgyizbxla6zpn") (y #t)))

