(define-module (crates-io te st test_module_impl) #:use-module (crates-io))

(define-public crate-test_module_impl-0.2.0 (c (n "test_module_impl") (v "0.2.0") (h "0gcfbil8dzxjav5rxr54f7616lcds40zzcc7jbv7iyylrk31j04f")))

(define-public crate-test_module_impl-0.3.0 (c (n "test_module_impl") (v "0.3.0") (h "026yyyl0p7a5xib2q86z1cid8iq19xjx5ijkj9yxfjizg9vgwljn")))

