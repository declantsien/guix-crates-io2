(define-module (crates-io te st test-vectors-macro) #:use-module (crates-io))

(define-public crate-test-vectors-macro-0.1.0 (c (n "test-vectors-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "target-test-dir") (r "^0.2.0") (d #t) (k 2)))) (h "0myid7bm1k9lyzrkm5viixvkxhjar6b342d4l7c369xyg8qniskv")))

