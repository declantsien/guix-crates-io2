(define-module (crates-io te st test_publish__1) #:use-module (crates-io))

(define-public crate-test_publish__1-0.2.0 (c (n "test_publish__1") (v "0.2.0") (h "1s1xzbz37n88ai5h1iw1dgh7ms88f1xyf8h1ch61h0qk2hdx4asj") (y #t)))

(define-public crate-test_publish__1-0.3.0 (c (n "test_publish__1") (v "0.3.0") (h "0pkrl64wl5nhv46dpplcwpmdvni8q8k1ih94v93ig0wlcn91faxa")))

