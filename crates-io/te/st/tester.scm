(define-module (crates-io te st tester) #:use-module (crates-io))

(define-public crate-tester-0.4.0 (c (n "tester") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0xxamjixsk5v97dglaw81q8imr2q4zmvk4qv8ypi7ajwb71h9j8z") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-tester-0.4.1 (c (n "tester") (v "0.4.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "137k961gj0bmznl6dscq0k2siwlcg97lx6qfxhxl08kn779k9bcl") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-tester-0.4.2 (c (n "tester") (v "0.4.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "0g86mcj0xdrbzr1m16jwa9cfq9636l5vjdl9f5ib6m9aqidjx0pp") (f (quote (("capture") ("asm_black_box")))) (y #t)))

(define-public crate-tester-0.5.0 (c (n "tester") (v "0.5.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1xkgapz2i4j977f6kh1zp6sa5llbhy5vbnr6kfj8czsrdjr2r0ay") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-tester-0.5.1 (c (n "tester") (v "0.5.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)))) (h "1az6brh39ai1jcc6yy7xglwq8m65samkb31zr7lr18swrd2103fd") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-tester-0.6.0 (c (n "tester") (v "0.6.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)))) (h "0kqynxab4g4qyhrmw8y7pzfn267q31wa7gbsirfkg17b6bbycivn") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-tester-0.7.0 (c (n "tester") (v "0.7.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)))) (h "0v1vy4kj3dfvzjhvsrs73qxsp4xlcbsdidx6x4yvahls00qyqwpf") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-tester-0.8.0 (c (n "tester") (v "0.8.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)))) (h "16q3ihgkwn3ildh00zj2bjfxz4z9ivpjg3m0gmfjj54lf99j539p") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-tester-0.8.1 (c (n "tester") (v "0.8.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "1d756y2n3hxffb8vlj4pnpzdvn6wcmbb36jp5n11ckgzpq7fbx8j") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-tester-0.9.0 (c (n "tester") (v "0.9.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "1svnlmaynfvfqmy7zjvbh2xwzdxxz50cyxbjlliz45a6iw6x2f86") (f (quote (("capture") ("asm_black_box"))))))

(define-public crate-tester-0.9.1 (c (n "tester") (v "0.9.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "0pqy14bc122lm19gcndf85lcs52izhhnh6yc5117ppdj1rzbzs49") (f (quote (("capture") ("asm_black_box"))))))

