(define-module (crates-io te st test0) #:use-module (crates-io))

(define-public crate-test0-0.1.0 (c (n "test0") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1a5rdbsdbwn9lls3w76kx8kwd1cmzr3dh3jjjf67bp9b75ls8wbh")))

(define-public crate-test0-0.2.0 (c (n "test0") (v "0.2.0") (h "1r392jq8p3qp2x5ivvzfamsfckp423j446ma932zqk4vyscz0rjr")))

(define-public crate-test0-0.3.0 (c (n "test0") (v "0.3.0") (h "0bwclr0vja086817lxi0hbbkjmiik1a3q45133pzi2xjsybsxdgn")))

(define-public crate-test0-0.4.0 (c (n "test0") (v "0.4.0") (h "0qwiymbvdn17ykhlr6hvnscskn2vqn4v5cjalbg6xqp8vh2r2jxw")))

(define-public crate-test0-0.5.0 (c (n "test0") (v "0.5.0") (h "1bv5b5hmrmw116xjs3iwz1n3z2pkjb3c5swlvil3p1mjrgqc3qa3")))

