(define-module (crates-io te st test-binary-features) #:use-module (crates-io))

(define-public crate-test-binary-features-0.0.1 (c (n "test-binary-features") (v "0.0.1") (d (list (d (n "phantom_newtype") (r "^0.2.0") (d #t) (k 0)) (d (n "test-binary") (r "^3.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0wbi935wni631y9hf1z0libg5bqpm6gmf7g535lkf8xx7j34awna") (f (quote (("default")))) (r "1.65.0")))

