(define-module (crates-io te st test-results) #:use-module (crates-io))

(define-public crate-test-results-0.1.0 (c (n "test-results") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "1krm0h3ngmbwf9by612yds44dfncx77jcjrl5f71yavp2w1pwipx") (r "1.56")))

(define-public crate-test-results-0.1.1 (c (n "test-results") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "13k5ryk4xas1921j8j9vrk5665llznch3wkdpq04ajbhah03k007") (r "1.56")))

(define-public crate-test-results-0.1.2 (c (n "test-results") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "0ksp56c3hldayhihw3wmiai6zkfjc50zj75qab9yi8kf4dq7pbdl") (r "1.56")))

