(define-module (crates-io te st test-context-macros) #:use-module (crates-io))

(define-public crate-test-context-macros-0.1.0 (c (n "test-context-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1d2bphkfm5yfw8mkhlwz8q7w7mp7y51xmxb9qss58zig8vg5i0ck")))

(define-public crate-test-context-macros-0.1.1 (c (n "test-context-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pps52351s3g656ln71j4i2bhlszzcp4jm3jv1dwc3wx2l4hfp4g")))

(define-public crate-test-context-macros-0.1.4 (c (n "test-context-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0k8mzc5d2kl0vyhn7cd37xhqxnkha69cmp3llv2fn1ks19dsa0c9")))

(define-public crate-test-context-macros-0.1.6 (c (n "test-context-macros") (v "0.1.6") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0x62cnnwslzmjmd6z3li247slqlhsd7fx6wlcksldqik8dkcf1nm")))

(define-public crate-test-context-macros-0.2.0 (c (n "test-context-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1aikkdb47yyigw6qmmi2hy8p9sl4hdwqglfb2n7d12akghxnl3dc") (r "1.75.0")))

(define-public crate-test-context-macros-0.3.0 (c (n "test-context-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "18a4a5xbsi2qdq0shgv8cdnzfc8y3gnkqd2lynkfr2invji1gskq") (r "1.75.0")))

