(define-module (crates-io te st test-mobile) #:use-module (crates-io))

(define-public crate-test-mobile-0.1.0 (c (n "test-mobile") (v "0.1.0") (h "0p7c7p2lia14i16p3z4zjzlyv485cb4h0wrj02rjjnws2vy8fhib")))

(define-public crate-test-mobile-0.1.1 (c (n "test-mobile") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)))) (h "128r2dya364qmqjwyb8wrgaczdiymn0ayl1aqr6pmgp9xdi5ynys")))

(define-public crate-test-mobile-0.1.2 (c (n "test-mobile") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)))) (h "1xf46mccz19wdazs47mcwfp1xvan13j72qnajjc4mjfr81kj9zsd")))

(define-public crate-test-mobile-0.2.0 (c (n "test-mobile") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1xw9hswjkwp1khyh1a7pcr7kkwjm4ijfpf9jrik2920argckj225")))

(define-public crate-test-mobile-0.2.1 (c (n "test-mobile") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0g9737im9nhfa90xrl819zza4j2whh4kyf8lwlws4kcgysgkhxir")))

(define-public crate-test-mobile-0.2.2 (c (n "test-mobile") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1cqj7jfnvlw095i3cri13dg1w0hg8zc1lz62dhxxgi300ynciid2")))

(define-public crate-test-mobile-0.2.3 (c (n "test-mobile") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "10fnyja1j7z0y4zmgmi2d9wd4cksnly0cd0syddgw24afii9pvdh")))

(define-public crate-test-mobile-0.2.4 (c (n "test-mobile") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0x62f23ggqgcpcz529wgdx1v8zfrpxlbddsh7y699flgrs2f0lf7")))

(define-public crate-test-mobile-0.3.0 (c (n "test-mobile") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0i9r5gj2b7nqc8c3dhijr4i92nc9zlmwp89alg99r0kaabrkw0xm")))

(define-public crate-test-mobile-0.3.1 (c (n "test-mobile") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0sg6ywlk4hizvwfqpx67wfjx2pcymxaqksmssfa5zd06ax3xs33s")))

(define-public crate-test-mobile-0.3.2 (c (n "test-mobile") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1jnxrkpqc13mqr0wzflwrv0c45m0jka394jl840hmqaw2jdw913r")))

