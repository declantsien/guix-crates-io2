(define-module (crates-io te st testing_logger) #:use-module (crates-io))

(define-public crate-testing_logger-0.1.0 (c (n "testing_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.3") (d #t) (k 0)))) (h "17mfzql489hfc2ghadks13n07vrpvj6qiaiaganm022ws8mfy54w")))

(define-public crate-testing_logger-0.1.1 (c (n "testing_logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.3") (d #t) (k 0)))) (h "087pi7y9iisspafyzblj41qvrw95dfb6px7pavlkmls5rckvg4kd")))

