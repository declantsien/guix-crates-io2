(define-module (crates-io te st test-catalog) #:use-module (crates-io))

(define-public crate-test-catalog-0.1.0 (c (n "test-catalog") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.10") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "project-root") (r "^0.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mbksqgjny40d9l1jbqp62i5k9maav9jrf81a1xfhnzdxi96ba79")))

