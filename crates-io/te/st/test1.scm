(define-module (crates-io te st test1) #:use-module (crates-io))

(define-public crate-test1-0.1.0 (c (n "test1") (v "0.1.0") (h "1pr7z8yjia2pgpl38xhad70mpi4n6wwx0dxii5pzny600cf41291")))

(define-public crate-test1-0.1.1 (c (n "test1") (v "0.1.1") (h "1s0x69jjdm2v9hzqsvv34gvkj9ksmdw5z2r415n6yvxyv5n2137n") (y #t)))

