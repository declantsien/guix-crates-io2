(define-module (crates-io te st test-sys) #:use-module (crates-io))

(define-public crate-test-sys-0.3.1 (c (n "test-sys") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "09vl7lkx688r502kzrx84zf6zh5l0n2p9ngfhimqagybhl1shjhz")))

(define-public crate-test-sys-0.4.0 (c (n "test-sys") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1gxlmlkxlfrz59rr8cfg5v1llkq14b0lg7v00k2kda9kj9hrk70v")))

(define-public crate-test-sys-0.4.1 (c (n "test-sys") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0cngzz9ny4wv30cbg1i211cfalxg0wf4q9hw6cnjcz2xsqf4rbnc")))

(define-public crate-test-sys-0.5.0 (c (n "test-sys") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1d48sjxjqcfz4c2p4rdl4s8wmh3fi4dih7dg0z04sf6idaj53b5h") (r "1.71.1")))

