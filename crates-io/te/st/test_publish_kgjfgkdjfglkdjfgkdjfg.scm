(define-module (crates-io te st test_publish_kgjfgkdjfglkdjfgkdjfg) #:use-module (crates-io))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg-0.1.0 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg") (v "0.1.0") (d (list (d (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (r "^0.1.0") (d #t) (k 0)))) (h "05zw5x4wv1q6anlyflxs2b8smhqhplz804pas4jkrlwahh32227r") (y #t)))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg-0.1.1 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg") (v "0.1.1") (d (list (d (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (r "^0.1.0") (d #t) (k 0)))) (h "1w6igw26sf7zrl8idnjdqq53fhaphfanfzm6mw0iciv2hdpriqax")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg-1.2.3 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg") (v "1.2.3") (d (list (d (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (r "^0.1.0") (d #t) (k 0)))) (h "1rwf45pdksgcsc94gchgfv00ih3ysnjr85wm0i69v19ijx9vfmb3")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg-1.2.4 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg") (v "1.2.4") (d (list (d (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (r "0.*") (d #t) (k 0)))) (h "14w3nvxnidmz7zpj0cq67hgvyzhjrwca6sp59kvl96zgkhh5flc3")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg-1.2.5 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg") (v "1.2.5") (d (list (d (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (r "1.*") (d #t) (k 0)))) (h "0b9x0gs6k1ll00204crw14449v2y0qnrzgdiy04hhvp9gpr33gw6")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg-1.2.6 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg") (v "1.2.6") (d (list (d (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (r "1.*") (d #t) (k 0)))) (h "0g5qla97gp9a10bd8p6z2ly3lgl4as5fhi2zvg4i91xdwm02n3vz")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg-1.2.7 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg") (v "1.2.7") (d (list (d (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (r "1.*") (d #t) (k 0)))) (h "115ywyxwiahc813b0agd6waimj10401ldv35dzwky8lznqvcdiyc")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg-1.2.8 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg") (v "1.2.8") (d (list (d (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (r "^1.2.8") (d #t) (k 0)))) (h "0v5rcqg04s89m7kdig079vk7dn4020v72q2s55bszmdbdr7ngd07")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg-1.2.9 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg") (v "1.2.9") (d (list (d (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (r "^1.2.9") (d #t) (k 0)))) (h "0z41rb4lpf7l29isn7q4r0d5whbx2mn3pk6a8yc5qdhcq01j6d4y")))

(define-public crate-test_publish_kgjfgkdjfglkdjfgkdjfg-1.2.9-dev0 (c (n "test_publish_kgjfgkdjfglkdjfgkdjfg") (v "1.2.9-dev0") (d (list (d (n "test_publish_kgjfgkdjfglkdjfgkdjfg_dep") (r "^1.2.9-dev0") (d #t) (k 0)))) (h "17m391q3flrbclwjqik5h86cv58x3yqpavz3kc7y38mbayn9zizp")))

