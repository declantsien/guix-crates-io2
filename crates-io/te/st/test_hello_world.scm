(define-module (crates-io te st test_hello_world) #:use-module (crates-io))

(define-public crate-test_hello_world-0.1.0 (c (n "test_hello_world") (v "0.1.0") (h "0nz8f599898vm28gs3m301iq6r58ngb2f8kb3pgyrl0z0d35p2ld") (y #t)))

(define-public crate-test_hello_world-0.1.1 (c (n "test_hello_world") (v "0.1.1") (h "1waa3616x03faq0civy407nrgvwqasxhy0yqqm10zbn4fr2kqmyw") (y #t)))

