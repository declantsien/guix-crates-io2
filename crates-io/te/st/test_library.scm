(define-module (crates-io te st test_library) #:use-module (crates-io))

(define-public crate-test_library-0.1.0 (c (n "test_library") (v "0.1.0") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "15wmn1gi7jvdw4m8vp6kl48md976llq87z606ln0kj1ig4fmjm3d") (y #t)))

(define-public crate-test_library-0.1.1 (c (n "test_library") (v "0.1.1") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "15nq4allbs78qjczjhx47qhky66rqvlzd52vrxpjcz65xsn7cz2y")))

(define-public crate-test_library-0.1.2 (c (n "test_library") (v "0.1.2") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "03id21zwfmsmg3samp6r2dazha4gxyxbyg747nbs2zkddkc7pl57")))

(define-public crate-test_library-0.1.3 (c (n "test_library") (v "0.1.3") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "1ahhnmfdha4nqp2gygq7xxpl1r6w2x8y96v3lgxm1ilq620m9bnh")))

(define-public crate-test_library-0.1.4 (c (n "test_library") (v "0.1.4") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "16dpszafjc3f1jcb3rvll47g6qmglg9sc1ifdqikifpgcalczxbr")))

(define-public crate-test_library-0.1.5 (c (n "test_library") (v "0.1.5") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "17vn8hq83k0qvbnk8mvkxs94ayvmjqr705ikr8fvv4am6s6p8hg5")))

(define-public crate-test_library-0.1.6 (c (n "test_library") (v "0.1.6") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "0r75qdvcqyha7pck8r1i2nczygyml870h18hwvfjfafimmflbc35")))

(define-public crate-test_library-0.1.7 (c (n "test_library") (v "0.1.7") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "12dmk8vi0rmm3vqd36l3kcv2wjz053wkrs8prapf8nkxg49ms8sa")))

(define-public crate-test_library-0.1.8 (c (n "test_library") (v "0.1.8") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "0zm8ri84saqjpkzz2fivbajys1wwr741axrv3x16sh0m3lrbc0ar")))

(define-public crate-test_library-0.1.9 (c (n "test_library") (v "0.1.9") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "0kk6xgv1rfd9ngbdkyqp3r4f3qzsmwak6lvzv83bc6v1k4i4zaj7")))

(define-public crate-test_library-0.1.10 (c (n "test_library") (v "0.1.10") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "0np3mlzjx2ab1h73rvmhs532fn1gyvpaaqwqyg1dr6sngm6nv91j")))

(define-public crate-test_library-0.1.11 (c (n "test_library") (v "0.1.11") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "1gc4wq4414y43mf7a8w3j7chknj4121mb94kmafk9q472faa3ib0")))

(define-public crate-test_library-0.1.12 (c (n "test_library") (v "0.1.12") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "1hi8d7ykmig1wk8s0xaisrpyvrrj0snlb2nxf2l7hsvla1rf400k")))

(define-public crate-test_library-0.1.13 (c (n "test_library") (v "0.1.13") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "10b4lkk7x40y25ik5s164zq3q3xzkw3f5w4maqh9kn0jln2frbnl")))

(define-public crate-test_library-0.1.14 (c (n "test_library") (v "0.1.14") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "1dv8nylph540rp8z7267sirjyd51x2i09a7vbzp6s9y9r1700vnn")))

(define-public crate-test_library-0.1.15 (c (n "test_library") (v "0.1.15") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "0w7r3l5j7cwid8xlgx89wivvfq5cx5ln48ghmvz1p527xlv5q5f7")))

(define-public crate-test_library-0.1.16 (c (n "test_library") (v "0.1.16") (d (list (d (n "pentest-toolbox-improved") (r "^0.1.37") (d #t) (k 0)))) (h "1kbz37290jwg13gh3jl1916q2mly4ypksd78ls5pq7p7hinm7xn3")))

