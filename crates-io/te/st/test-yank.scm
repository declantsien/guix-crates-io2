(define-module (crates-io te st test-yank) #:use-module (crates-io))

(define-public crate-test-yank-0.0.1 (c (n "test-yank") (v "0.0.1") (h "0qd87rgyc3dvqf5lj2fnndmh5b6gka03idj3lipxksn48s7l8h54")))

(define-public crate-test-yank-0.0.2 (c (n "test-yank") (v "0.0.2") (h "1blvz2v3m7dxdvnvmd0ybybyn1r67miai28xbpglxs9aarhqlxcn")))

(define-public crate-test-yank-0.0.3 (c (n "test-yank") (v "0.0.3") (h "0zlvvjrd7x11i7q2x65lvjfjbanyxgx9qddfnkgxavsmxd4bm78f") (y #t)))

(define-public crate-test-yank-0.0.4 (c (n "test-yank") (v "0.0.4") (h "0z1jamysjpzm1x4y3fdr755lshay5p1q0p0wpkl42y3igw8n9sls")))

(define-public crate-test-yank-0.0.3+1 (c (n "test-yank") (v "0.0.3+1") (h "0vh0blca6cg9m4q58mv8zzf8ynh2n5nmkb1cfvyh40w9nppwm9y3")))

(define-public crate-test-yank-0.0.5 (c (n "test-yank") (v "0.0.5") (h "1201s2l5w5vlmhzg4ccws5rys9xypr6xyyakrblmyixr821i9rbb")))

(define-public crate-test-yank-0.0.6 (c (n "test-yank") (v "0.0.6") (h "177rxmfqsi5j50alm9nkxqk3m4s58jg6zd3wkhn3x313hhcbsiws")))

