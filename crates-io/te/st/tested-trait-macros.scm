(define-module (crates-io te st tested-trait-macros) #:use-module (crates-io))

(define-public crate-tested-trait-macros-0.1.0 (c (n "tested-trait-macros") (v "0.1.0") (d (list (d (n "manyhow") (r "^0.4.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (k 0)) (d (n "quote") (r "^1.0.0") (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("full" "parsing" "printing" "clone-impls" "proc-macro" "extra-traits"))) (k 0)))) (h "1cizqlc1d3h944gggsz01kwpcr25gbq6shlybdi9g6hddjsbaah1")))

