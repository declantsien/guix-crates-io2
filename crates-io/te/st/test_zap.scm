(define-module (crates-io te st test_zap) #:use-module (crates-io))

(define-public crate-TEST_zap-0.6.1 (c (n "TEST_zap") (v "0.6.1") (d (list (d (n "codespan-reporting") (r "^0.11.0") (d #t) (k 0)) (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "105j067pfxdzyxjwqnrdpmn6n5l8qk039nwwxnhay5r65p28qfjg")))

