(define-module (crates-io te st test_publish_package_timer2) #:use-module (crates-io))

(define-public crate-test_publish_package_timer2-0.0.2 (c (n "test_publish_package_timer2") (v "0.0.2") (h "14mdjaljr61b4h2rmhi3lk833vqlq3aa5dbkp51zajnr73gz6zn6")))

(define-public crate-test_publish_package_timer2-0.0.3 (c (n "test_publish_package_timer2") (v "0.0.3") (h "0dkc4wnzww589zb1d9qrgcsj9rvkil9sqkjwbqcll4akk712w9ar")))

(define-public crate-test_publish_package_timer2-0.0.5 (c (n "test_publish_package_timer2") (v "0.0.5") (h "1fi7fn7hpb6dfbfma2rwwgpwd0vywzm19bhvswcarhcrvgjh1c2r")))

(define-public crate-test_publish_package_timer2-0.0.6 (c (n "test_publish_package_timer2") (v "0.0.6") (h "06h3qxgjrrx54xdf8fnb7hcsbf7yhdx5d7rhb7ckz1k1lahckidm")))

(define-public crate-test_publish_package_timer2-0.0.7 (c (n "test_publish_package_timer2") (v "0.0.7") (d (list (d (n "test_publish_package_timer") (r "~0.0.6") (d #t) (k 0)))) (h "1brpzs0l7ga8m19gqh9hyc141rp0nh32wzicklx9dxzydz4xkq90")))

(define-public crate-test_publish_package_timer2-0.0.8 (c (n "test_publish_package_timer2") (v "0.0.8") (d (list (d (n "test_publish_package_timer") (r "~0.0.6") (d #t) (k 0)))) (h "0qs7aq9jqpsxzqzpikrfvk26dswgavca0yc0d3i472gja5d254wm")))

(define-public crate-test_publish_package_timer2-0.0.9 (c (n "test_publish_package_timer2") (v "0.0.9") (d (list (d (n "test_publish_package_timer") (r "~0.0.6") (d #t) (k 0)))) (h "01zx1qmp4rr99gfiklh1n9qk177hv0jrck65yxmvp6dxw42s9wm6")))

(define-public crate-test_publish_package_timer2-0.0.10 (c (n "test_publish_package_timer2") (v "0.0.10") (d (list (d (n "test_publish_package_timer") (r "~0.0.36") (d #t) (k 0)))) (h "034v6j5mcdi73qmlxmxjmdqc137krzk8fs5hkj8diflq6vqml1rz")))

(define-public crate-test_publish_package_timer2-0.0.11 (c (n "test_publish_package_timer2") (v "0.0.11") (d (list (d (n "test_publish_package_timer") (r "~0.0.37") (d #t) (k 0)))) (h "1c3sqha0z8g4l0ind7a1ncfhdrfq3mrxflqrwlf1jq5sxpzpzvfp")))

