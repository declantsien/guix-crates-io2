(define-module (crates-io te st testingtwice) #:use-module (crates-io))

(define-public crate-testingTwice-0.1.0 (c (n "testingTwice") (v "0.1.0") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1z2nqhyllpznhglsg186w7xsk8v7ir7np48clxl1pkvh1mczp20h")))

(define-public crate-testingTwice-0.1.1 (c (n "testingTwice") (v "0.1.1") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "009ivfh0lggar2kic7xjsl5cahr5kjkxvq8jk7pgrv79nvjdxjgz")))

(define-public crate-testingTwice-0.1.2 (c (n "testingTwice") (v "0.1.2") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0fav8a8127yk52w0jb63p258jkadpg319j38sf9knx3jwbxnrfnb")))

(define-public crate-testingTwice-0.1.3 (c (n "testingTwice") (v "0.1.3") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0xv4nihjz0mw921aavpnq8mxhxpb2kgvddhiw6ljm81zf2xk06xn")))

(define-public crate-testingTwice-0.1.4 (c (n "testingTwice") (v "0.1.4") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1xhck7kx05s0gkbn686c6nyq807z1h881k4zwafvifmp3zgaq36s")))

(define-public crate-testingTwice-0.1.5 (c (n "testingTwice") (v "0.1.5") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1vzn7szn761rdzh2ifj9bshqyrg6yhy2q1rjb3n88ik7akj38fxg")))

(define-public crate-testingTwice-0.1.6 (c (n "testingTwice") (v "0.1.6") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "04agsykgblf0arcg933qysa1mly4m63b2mailaxlkpgvxy68an23")))

(define-public crate-testingTwice-0.1.7 (c (n "testingTwice") (v "0.1.7") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1gmrqnni1vhl0gaxxd09d393f1xj09i9kapkxd965fnzgcs18wjf")))

(define-public crate-testingTwice-0.1.8 (c (n "testingTwice") (v "0.1.8") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "10f54ccsn2nspazhrrf2zdlzfv0qcsk4wkrp0z929gzm9m4pbwxx")))

(define-public crate-testingTwice-0.1.9 (c (n "testingTwice") (v "0.1.9") (d (list (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "06c8c9xx51kh8jqm2bi8arb14lwy4pkwqcp7brr18vhd510mk5m8")))

