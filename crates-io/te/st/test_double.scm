(define-module (crates-io te st test_double) #:use-module (crates-io))

(define-public crate-test_double-0.1.0 (c (n "test_double") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "06nn9nyhnrwg7kjyy7c85jvhkxirirzs8nlys63s0nxdq102cx3i")))

(define-public crate-test_double-0.1.1 (c (n "test_double") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "11is03rd2li7iwspxs49fal025fh7qc0paxc448v5v7jpxc18jgz")))

(define-public crate-test_double-0.2.0 (c (n "test_double") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0jm53ppzs5fni8z9igay4f3nlz47p0np2lkmk5n102n7k129q3x1")))

(define-public crate-test_double-0.3.0 (c (n "test_double") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1kvqal81ap2fwnz55z0liyifvmx74qbz44cs4hmv72yyqwkm4b71")))

