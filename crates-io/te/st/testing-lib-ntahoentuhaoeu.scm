(define-module (crates-io te st testing-lib-ntahoentuhaoeu) #:use-module (crates-io))

(define-public crate-testing-lib-ntahoentuhaoeu-0.1.0-beta.0 (c (n "testing-lib-ntahoentuhaoeu") (v "0.1.0-beta.0") (h "134yzr7481nf4wzzas5c3kkww686mg963hkwakkd6hcycqclvpmj")))

(define-public crate-testing-lib-ntahoentuhaoeu-0.1.0-gamma (c (n "testing-lib-ntahoentuhaoeu") (v "0.1.0-gamma") (h "06224bfhbw6q8dim722yr23pac4m3i142x5w4134hrgy7wibqw0w")))

(define-public crate-testing-lib-ntahoentuhaoeu-0.1.1-gamma (c (n "testing-lib-ntahoentuhaoeu") (v "0.1.1-gamma") (h "1yi88l365srydam7yhjbba0rr9jaqm8594h0ivh5l0pksivd22ca")))

