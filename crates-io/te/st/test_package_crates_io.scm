(define-module (crates-io te st test_package_crates_io) #:use-module (crates-io))

(define-public crate-test_package_crates_io-0.1.0 (c (n "test_package_crates_io") (v "0.1.0") (h "1pib6z4jkg0qr1syiji8bmrnvcy6k80z1rpz3qgjzayvhrn1h1rm")))

(define-public crate-test_package_crates_io-0.1.1 (c (n "test_package_crates_io") (v "0.1.1") (h "19jgm15ymj4ixpdw13pknrwjxdqwickdawll67id5kq4w19ilavl")))

(define-public crate-test_package_crates_io-0.1.2 (c (n "test_package_crates_io") (v "0.1.2") (h "0q675lj91mjzrzjgszbqfzq2n671f4c6hb09c0i25xgbn5d7sg88")))

(define-public crate-test_package_crates_io-0.1.3 (c (n "test_package_crates_io") (v "0.1.3") (h "0bcbklhqmlw0wdi8cl5dcwfwlnw2ixb5bbasdi5zjm70n7ypk9jm")))

(define-public crate-test_package_crates_io-0.1.4 (c (n "test_package_crates_io") (v "0.1.4") (h "0ic9xgbz6xxm89rmapha2rlmv1cxc835rbg6p3hjcy4ycgkckpkd")))

(define-public crate-test_package_crates_io-0.1.5 (c (n "test_package_crates_io") (v "0.1.5") (h "1swgidvpa5b9xl99ba8hj95plakz942ip50dfxfjbgi1wiywpzca")))

(define-public crate-test_package_crates_io-0.1.6 (c (n "test_package_crates_io") (v "0.1.6") (h "0skp64lk9fpyy8l093bpci13qfxfx6cjlpk21qfx3gb2rcnky443")))

