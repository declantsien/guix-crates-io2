(define-module (crates-io te st test_publish_cool_workspace_inner) #:use-module (crates-io))

(define-public crate-test_publish_cool_workspace_inner-0.13.3 (c (n "test_publish_cool_workspace_inner") (v "0.13.3") (h "1zij6z3ajcw23x2nindqqabgvd5amwcpb4gznysjdn940rkvks9n")))

(define-public crate-test_publish_cool_workspace_inner-0.13.4 (c (n "test_publish_cool_workspace_inner") (v "0.13.4") (h "0qsb3mjszfvz9qishjvl3g5hw9j02bg59apfg85ynqgyy7nbimjq")))

(define-public crate-test_publish_cool_workspace_inner-0.13.5 (c (n "test_publish_cool_workspace_inner") (v "0.13.5") (h "0jyq9ir7qnk19jjjiwklfwgrvnv0lxv9n8217v6rdd9zflzsjn6g")))

(define-public crate-test_publish_cool_workspace_inner-0.13.6 (c (n "test_publish_cool_workspace_inner") (v "0.13.6") (h "1npa87fs5km8sqsbmizc2pq5shl85ngqsl1mizcv83shx82hz5pa")))

(define-public crate-test_publish_cool_workspace_inner-0.13.7 (c (n "test_publish_cool_workspace_inner") (v "0.13.7") (h "1phvpl2jcqvy97jgz3cx9qvc6jmg2ybxvcp7sj9r4wwiv8nyypcw")))

(define-public crate-test_publish_cool_workspace_inner-0.13.8 (c (n "test_publish_cool_workspace_inner") (v "0.13.8") (h "16ki5dmdiqkzdhgd09f2v6s1r8apnafcp6x883jz6kyhvkfpabvv")))

(define-public crate-test_publish_cool_workspace_inner-0.13.9 (c (n "test_publish_cool_workspace_inner") (v "0.13.9") (h "1835bsqwl35s62n14qdh1w5crl9qhi8m785ismhi596fd6j6isdl")))

