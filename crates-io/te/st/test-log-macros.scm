(define-module (crates-io te st test-log-macros) #:use-module (crates-io))

(define-public crate-test-log-macros-0.2.14 (c (n "test-log-macros") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1.0.32") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0pd2kzfam9dmm8h9pnzj3yjdix8vvd18jl78d6hyxs8rfbkpg8kv") (f (quote (("unstable") ("trace") ("log")))) (r "1.61")))

(define-public crate-test-log-macros-0.2.15 (c (n "test-log-macros") (v "0.2.15") (d (list (d (n "proc-macro2") (r "^1.0.32") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "01q1sqhqd5mr6kwdvqirwynjnzays2gzx4whahzkff5a3r2ldxf8") (f (quote (("unstable") ("trace") ("log")))) (r "1.71")))

(define-public crate-test-log-macros-0.2.16 (c (n "test-log-macros") (v "0.2.16") (d (list (d (n "proc-macro2") (r "^1.0.32") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1xd3490hvpa6fk5sqnmyzbz5xwndfpmmshjfpa8k221jm97f56ar") (f (quote (("unstable") ("trace") ("log")))) (r "1.71")))

