(define-module (crates-io te st test_61a7cd7f28e0) #:use-module (crates-io))

(define-public crate-test_61a7cd7f28e0-0.1.0-nodocsrssettings (c (n "test_61a7cd7f28e0") (v "0.1.0-nodocsrssettings") (d (list (d (n "test_40a48f47864a") (r "^0.1.0") (d #t) (k 0)) (d (n "test_7018a98a70c0") (r "^0.1.0") (d #t) (k 0)))) (h "1zrwpv6yqs12wyp97in8z1zvbn3fh6xsc4yy1zcv0wm91achgqbn")))

(define-public crate-test_61a7cd7f28e0-0.1.0-withdocsrssettings (c (n "test_61a7cd7f28e0") (v "0.1.0-withdocsrssettings") (d (list (d (n "test_40a48f47864a") (r "^0.1.0") (d #t) (k 0)) (d (n "test_7018a98a70c0") (r "^0.1.0") (d #t) (k 0)))) (h "00n1v8p54g4bmhkly6i08rg9pqi1anc5n8nvx134dxilijjdlrs0")))

(define-public crate-test_61a7cd7f28e0-0.1.1-withdocsrssettings (c (n "test_61a7cd7f28e0") (v "0.1.1-withdocsrssettings") (d (list (d (n "test_40a48f47864a") (r "^0.1.1-withdocsrssettings") (d #t) (k 0)) (d (n "test_7018a98a70c0") (r "^0.1.1-withdocsrssettings") (d #t) (k 0)))) (h "12m3xfx688a2lf0n1vk4rh2byf7l57658csi4b53x1w6m7wqy4xx")))

(define-public crate-test_61a7cd7f28e0-0.1.2-withdocsrssettings (c (n "test_61a7cd7f28e0") (v "0.1.2-withdocsrssettings") (d (list (d (n "test_40a48f47864a") (r "^0.1.1-withdocsrssettings") (d #t) (k 0)) (d (n "test_7018a98a70c0") (r "^0.1.1-withdocsrssettings") (d #t) (k 0)))) (h "1kcvf2k41jwzgn9mxp6n23z567nwif2lxzivkywgq4qs8gd3xpfq")))

(define-public crate-test_61a7cd7f28e0-0.1.3-withoutpathdeps (c (n "test_61a7cd7f28e0") (v "0.1.3-withoutpathdeps") (d (list (d (n "test_40a48f47864a") (r "^0.1.1-withdocsrssettings") (d #t) (k 0)) (d (n "test_7018a98a70c0") (r "^0.1.1-withdocsrssettings") (d #t) (k 0)))) (h "1y6gf5hvhf7psl623pjy41xkqnfvz74087dcidj7s4c96xbdd2nq")))

