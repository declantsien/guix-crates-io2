(define-module (crates-io te st test_dep_resolver) #:use-module (crates-io))

(define-public crate-test_dep_resolver-0.1.0 (c (n "test_dep_resolver") (v "0.1.0") (d (list (d (n "thisdoesntexist") (r "^0.14") (d #t) (k 0) (p "hyper")))) (h "0x13s4w54ijzzjxz1d2vnnawij4c4xyka1azhdca1qnjp0yj5801")))

(define-public crate-test_dep_resolver-0.1.1 (c (n "test_dep_resolver") (v "0.1.1") (d (list (d (n "thisdoesntexist") (r "^0.14") (d #t) (k 0) (p "hyper")))) (h "0nhv2m7v6ka997zi0dq57m27s7lqzmx816apg9aq14q4h9pvb10l")))

