(define-module (crates-io te st testtools) #:use-module (crates-io))

(define-public crate-testtools-0.1.1 (c (n "testtools") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "0ibidg3dm0brw1w86a4gdzszj48dk3hzg36d9zss6ldvnspci3gv") (r "1.56")))

(define-public crate-testtools-0.1.2 (c (n "testtools") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "1n3af0vbf28jr7vj07qls0j5si0lwxi3s3f0gw0xinjns1mrdf6p") (r "1.56")))

(define-public crate-testtools-0.1.3 (c (n "testtools") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1qp2wf2b1l27dk4s4s292m4kkw0acz7v4r8w5angwwixjcmlj8kw") (r "1.56")))

