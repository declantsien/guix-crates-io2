(define-module (crates-io te st test_name) #:use-module (crates-io))

(define-public crate-test_name-0.1.0 (c (n "test_name") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1pf8rzcaqfczqbimva08rg8nsdgpni5lfpw84ffk5m76bysy8fzc") (y #t)))

(define-public crate-test_name-0.0.1 (c (n "test_name") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "18bzcxlm5hq9fislwkpckdjfbdnlygyagryyp3qbbyqjd6a20kxy") (y #t)))

(define-public crate-test_name-0.1.1 (c (n "test_name") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1pczrkf1aiwg50384a9b2r3b7dlqd426p7g83a6lk7js9lpv5hqd") (y #t)))

(define-public crate-test_name-0.1.2 (c (n "test_name") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0smb8kmi84pc9fyjl9banh0akmcirlccgrrinwx7rkslfw8iqsra") (y #t)))

(define-public crate-test_name-0.1.3 (c (n "test_name") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0yiq0nay645ya2yiwkr740fzz07h95gsgb3r1xnxh8mh58s68xrj") (y #t)))

(define-public crate-test_name-0.1.4 (c (n "test_name") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0v3nnrzdl1q6dm4brvyivqdq27fvyj2ssfnl7s28hmjffpbbk3sx") (y #t)))

(define-public crate-test_name-0.1.5 (c (n "test_name") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0r9rvalz0cwrb8fi7bimr8yk2j9nyrlxqcflfphrswxi3hn1hh4d") (y #t)))

(define-public crate-test_name-0.1.6 (c (n "test_name") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1f4yv5fp28rrjxdzihyjrp5c6v7kdyjyvkhjhcsmyi5a1qn3qhi6") (y #t)))

(define-public crate-test_name-0.1.7 (c (n "test_name") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0yg1nwc5nj8fllghg1wka2p24d8215i6n7p22paqrky2bng7z40f") (y #t)))

(define-public crate-test_name-0.1.8 (c (n "test_name") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1q3i8pbfqc5s62m2nwzjc92y10dlwaiknwcfqi5wcm5sxm5p5gjl")))

(define-public crate-test_name-0.0.2 (c (n "test_name") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1c2xic98jwa1j405gx5x7svamd186aknl1dich2aq5i1l2fcic36") (y #t)))

(define-public crate-test_name-0.0.3 (c (n "test_name") (v "0.0.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "049l8gly9spy65m26gby54vm06za97rmkdn7hiksn82bh375jgj8") (y #t)))

