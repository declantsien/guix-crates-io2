(define-module (crates-io te st test_publish_cool_workspace) #:use-module (crates-io))

(define-public crate-test_publish_cool_workspace-0.1.0 (c (n "test_publish_cool_workspace") (v "0.1.0") (h "071lnbnqvh1j5lagfsps909d1k3f2ivb9wk7rvz27rzzx7i0bqii")))

(define-public crate-test_publish_cool_workspace-0.1.1 (c (n "test_publish_cool_workspace") (v "0.1.1") (h "0p51vckdrwafyxkkr6wxbjvr15a8s685nph37frrk6crkakx2h6q")))

(define-public crate-test_publish_cool_workspace-0.13.2 (c (n "test_publish_cool_workspace") (v "0.13.2") (h "0958gds2izs30097mjvma1jcsqmx69jvnmh69biajv28n60c8dl1")))

(define-public crate-test_publish_cool_workspace-0.13.3 (c (n "test_publish_cool_workspace") (v "0.13.3") (h "0pj1wn5nqyr2h2yxvkl228vcx0ym02kdyixa0d1c2xglrp575slg")))

(define-public crate-test_publish_cool_workspace-0.13.4 (c (n "test_publish_cool_workspace") (v "0.13.4") (d (list (d (n "test_publish_cool_workspace_inner") (r "^0.13.4") (d #t) (k 0)))) (h "07l6j7f7a00qs8yr01b9v2yfzsdnbd6kdars9jmr629vhqmj7k9q")))

(define-public crate-test_publish_cool_workspace-0.13.5 (c (n "test_publish_cool_workspace") (v "0.13.5") (d (list (d (n "test_publish_cool_workspace_inner") (r "^0.13.5") (d #t) (k 0)))) (h "1lmijc5ycmhyzawz8x13mp3c95398h73jdn622nb70163sgwyprs")))

(define-public crate-test_publish_cool_workspace-0.13.6 (c (n "test_publish_cool_workspace") (v "0.13.6") (d (list (d (n "test_publish_cool_workspace_inner") (r "^0.13.6") (d #t) (k 0)))) (h "1xr61sppkxy3cln3924z13sbgb9i0v1fiadf1b79y86s0pn9ws1p")))

(define-public crate-test_publish_cool_workspace-0.13.7 (c (n "test_publish_cool_workspace") (v "0.13.7") (d (list (d (n "test_publish_cool_workspace_inner") (r "^0.13.7") (d #t) (k 0)))) (h "0jwhp4f5nxnwf8cgrp8xvz26h6pk55fvfasixbxj4jf185qqj9ji")))

(define-public crate-test_publish_cool_workspace-0.13.8 (c (n "test_publish_cool_workspace") (v "0.13.8") (d (list (d (n "test_publish_cool_workspace_inner") (r "^0.13.8") (d #t) (k 0)))) (h "1h8vcp6p57jcybm3vwa1y61hl4vnkaab7rxiwpg03ki6iw5k8rq6")))

(define-public crate-test_publish_cool_workspace-0.13.9 (c (n "test_publish_cool_workspace") (v "0.13.9") (d (list (d (n "test_publish_cool_workspace_inner") (r "^0.13.9") (d #t) (k 0)))) (h "11v36iip375idhylvl9jfx5yrmwrff2h13n3rl20sis705brkc9m")))

