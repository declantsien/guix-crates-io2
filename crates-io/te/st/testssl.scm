(define-module (crates-io te st testssl) #:use-module (crates-io))

(define-public crate-testssl-0.1.0 (c (n "testssl") (v "0.1.0") (d (list (d (n "testssl-sys") (r "^0.1.18") (d #t) (k 0)))) (h "1jdb188sh7mxyrk85wcw93sb5gsy2ssc42a22hkr7gf00wryv2d4")))

(define-public crate-testssl-0.1.1 (c (n "testssl") (v "0.1.1") (d (list (d (n "testssl-sys") (r "^0.1.18") (d #t) (k 0)))) (h "1752vyl5y55yw8smdgwrlbihnl0ghr1rz2r39cakd2rx43n31w9a")))

(define-public crate-testssl-0.1.3 (c (n "testssl") (v "0.1.3") (d (list (d (n "testssl-sys") (r "^0.1.18") (d #t) (k 0)))) (h "0fz61mza4ib8wa0z4zdm8761csykw8dskmhk2xra9ggw35vm26i8")))

(define-public crate-testssl-0.1.4 (c (n "testssl") (v "0.1.4") (d (list (d (n "testssl-sys") (r "^0.1.18") (d #t) (k 0)))) (h "0z9mxl2wj7jnmnh31sfh4igavxvmvwlv0d7qr81bc031hknzhp7h")))

(define-public crate-testssl-0.1.5 (c (n "testssl") (v "0.1.5") (d (list (d (n "testssl-sys") (r "^0.1.27") (d #t) (k 0)))) (h "0c4qzc31w4r66saxll8m92d98kd7xfxgnfzjzvqsrb1rlg85ynzg")))

(define-public crate-testssl-0.1.6 (c (n "testssl") (v "0.1.6") (d (list (d (n "testssl-sys") (r "^0.1.27") (d #t) (k 0)))) (h "0i8pkgqf5cm0fz7dardj49yki74ifc1cd57g16aq3dxnc24ia4bz")))

