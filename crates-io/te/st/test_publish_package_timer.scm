(define-module (crates-io te st test_publish_package_timer) #:use-module (crates-io))

(define-public crate-test_publish_package_timer-0.0.1 (c (n "test_publish_package_timer") (v "0.0.1") (h "1x7q3mbzr18zakp3jqf6q8iqblawj6aq241rv599bvxhpyyr7cas") (y #t)))

(define-public crate-test_publish_package_timer-0.0.2 (c (n "test_publish_package_timer") (v "0.0.2") (h "0c1naxgr07ilf70a2s6i93h7gigw3d4h4rphkm8qs0pdhghfpmlb") (y #t)))

(define-public crate-test_publish_package_timer-0.0.3 (c (n "test_publish_package_timer") (v "0.0.3") (h "159pyc8gxkj322rwfvgq15kwf93d29qaqa92b8j2qf6svq7lqn8p") (y #t)))

(define-public crate-test_publish_package_timer-0.0.4 (c (n "test_publish_package_timer") (v "0.0.4") (h "1jk7mk0hqzpcsjmmbzh1n3difhfckzxi861hmi58bhhphzh303z6") (y #t)))

(define-public crate-test_publish_package_timer-0.0.5 (c (n "test_publish_package_timer") (v "0.0.5") (h "1ks4v9yc2yhcpzgyg9g2crdswp8i1d975rg5h5jbcn02xkrlgh53")))

(define-public crate-test_publish_package_timer-0.0.6 (c (n "test_publish_package_timer") (v "0.0.6") (h "0bdr7zfifgs36s7bzq5xq5zpb7a85305vhs1gf1vygr7is9gzwc1")))

(define-public crate-test_publish_package_timer-0.0.7 (c (n "test_publish_package_timer") (v "0.0.7") (h "0m11bcqxg475hg933fjjrrhc2gahyhgc6xjqsim7scy5rd7wrs53")))

(define-public crate-test_publish_package_timer-0.0.8 (c (n "test_publish_package_timer") (v "0.0.8") (h "1kxcw2r1p8q1lz4hgh1zxpylk5qa48a6adpd1610jrk9h5728fg8")))

(define-public crate-test_publish_package_timer-0.0.9 (c (n "test_publish_package_timer") (v "0.0.9") (h "129dy39xkxf5ygayvxlrd8nj3n7pnfcp48m83s37qj1dwch7r6b2")))

(define-public crate-test_publish_package_timer-0.0.33 (c (n "test_publish_package_timer") (v "0.0.33") (h "0nkjv13i1ha2m1jh5jvj0afd022cpxmpsm6h3kqlrk25fcaw25wi")))

(define-public crate-test_publish_package_timer-0.0.34 (c (n "test_publish_package_timer") (v "0.0.34") (h "09hzqxdfps6zkmfriz002x699bdv9f665hp2ncrpqnpap812jlaw")))

(define-public crate-test_publish_package_timer-0.0.36 (c (n "test_publish_package_timer") (v "0.0.36") (h "1fqp5ccffvz6jgigr0pk52r7n38b8npwjg0mfs20g1pq7m5v1say")))

(define-public crate-test_publish_package_timer-0.0.37 (c (n "test_publish_package_timer") (v "0.0.37") (h "051pqg8vh0msmllgr5xcpv9snscz1ya36mhcp1mkyqk6pwkhpg84")))

