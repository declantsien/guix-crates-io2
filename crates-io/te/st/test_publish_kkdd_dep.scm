(define-module (crates-io te st test_publish_kkdd_dep) #:use-module (crates-io))

(define-public crate-test_publish_kkdd_dep-1.2.9-dev.0 (c (n "test_publish_kkdd_dep") (v "1.2.9-dev.0") (h "1fya6s2r8dvax7znjj2148r6casl6j381knfmqjnsfxgs7m0kdpv")))

(define-public crate-test_publish_kkdd_dep-1.2.9-dev.1 (c (n "test_publish_kkdd_dep") (v "1.2.9-dev.1") (h "1x8mwf6ln4wc799c58nl8nsb9c8zq3y3s7lqhh3sl9072wvh6bbq")))

(define-public crate-test_publish_kkdd_dep-1.1.1 (c (n "test_publish_kkdd_dep") (v "1.1.1") (h "0v0jhhpx386j1blnx8hcqh3yxk4byrckkdmyk6hzhswa0v3nvxpm")))

(define-public crate-test_publish_kkdd_dep-1.1.2 (c (n "test_publish_kkdd_dep") (v "1.1.2") (h "116lc4d4asajd7cn4rraq7b3v3yp3wqwpm1dbhkjgwhnmj3hpah0")))

(define-public crate-test_publish_kkdd_dep-1.1.3 (c (n "test_publish_kkdd_dep") (v "1.1.3") (h "1bjq3mqb6klmn0m9i2xswdb358xg7yq0r2aqi5an10hn5rwrv1pn")))

(define-public crate-test_publish_kkdd_dep-1.1.4 (c (n "test_publish_kkdd_dep") (v "1.1.4") (h "1m5dch6zxml4msi662917l2ssh2wzyznddiis17a2x6sf2k1p8n5")))

(define-public crate-test_publish_kkdd_dep-1.1.5 (c (n "test_publish_kkdd_dep") (v "1.1.5") (h "1pmf4ibv8w675wj59s98kjqxqxnyp524m1sn2m2b9ijg1jkkw880")))

(define-public crate-test_publish_kkdd_dep-1.1.8 (c (n "test_publish_kkdd_dep") (v "1.1.8") (h "0gsk0mzl9lnkw8m2lj55ksii4c654h3lx9vkrca2dh1whz8plkxm")))

