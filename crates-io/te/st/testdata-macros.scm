(define-module (crates-io te st testdata-macros) #:use-module (crates-io))

(define-public crate-testdata-macros-0.1.0 (c (n "testdata-macros") (v "0.1.0") (d (list (d (n "big_s") (r "^1.0.2") (d #t) (k 2)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)) (d (n "testdata-rt") (r "^0.1.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.3") (d #t) (k 0)))) (h "0g8jsfhr4ib4m4z67jrqnkibx32gzqyw6m83q2fzzfm3vza6v22n")))

