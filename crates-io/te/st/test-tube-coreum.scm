(define-module (crates-io te st test-tube-coreum) #:use-module (crates-io))

(define-public crate-test-tube-coreum-0.1.0 (c (n "test-tube-coreum") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "cosmrs") (r "^0.14.0") (f (quote ("cosmwasm" "rpc"))) (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "cw1-whitelist") (r "^1.1.0") (d #t) (k 2)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.106") (d #t) (k 0)) (d (n "tendermint-proto") (r "^0.33.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1b1nghxs8kabnq2i3gjp1qp331ghi91r4147pnf9smnj76s9xi3c")))

