(define-module (crates-io te st testx) #:use-module (crates-io))

(define-public crate-testx-0.1.0 (c (n "testx") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0b0wsd12q1p1i1s9da82hrbfwd29s3f1lnw5yy9ag04bihs1rrl8")))

(define-public crate-testx-0.1.1 (c (n "testx") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1jrxa7p3igf74nlcin8w2m1qqdv0dzz5cvg49gqwq9dscbnhzf4z")))

(define-public crate-testx-0.1.2 (c (n "testx") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "141ghbhvpnmkf21wj8azv2dwg7spbpcc9g1r5bp3hjbv9zw5b6cp")))

