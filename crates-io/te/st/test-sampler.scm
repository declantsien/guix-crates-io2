(define-module (crates-io te st test-sampler) #:use-module (crates-io))

(define-public crate-test-sampler-0.1.0 (c (n "test-sampler") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "argmin") (r "^0.8") (d #t) (k 0)) (d (n "is_sorted") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18cgy7lsd1j0xl2fsm9cqzva00304a9vd6qwrfhqwf708nr1p5y2")))

