(define-module (crates-io te st test-harness) #:use-module (crates-io))

(define-public crate-test-harness-0.1.0 (c (n "test-harness") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1y5gqrqlydppkkj1w90skhf7rcb4bvkyh8s4y9n21i29r7z6m17m")))

(define-public crate-test-harness-0.1.1 (c (n "test-harness") (v "0.1.1") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0hy67ifkl8q54kalv39rk326422lxpf4ka1647m7xcfq3cw7f4p7")))

(define-public crate-test-harness-0.2.0 (c (n "test-harness") (v "0.2.0") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0ll3khdpcsjrwhn6x3qk4xq5hfgpcg0hk0ni7bj9m1ddhh5gzl7m")))

