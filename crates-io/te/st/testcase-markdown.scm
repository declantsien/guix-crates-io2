(define-module (crates-io te st testcase-markdown) #:use-module (crates-io))

(define-public crate-testcase-markdown-0.0.1 (c (n "testcase-markdown") (v "0.0.1") (d (list (d (n "markdown") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 2)))) (h "1hvxzm745a40wf61p8ard0gb00irlm73hg7j9ypkp18invagpdd9")))

