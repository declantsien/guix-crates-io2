(define-module (crates-io te st test-with-tokio-macros) #:use-module (crates-io))

(define-public crate-test-with-tokio-macros-0.1.0 (c (n "test-with-tokio-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "17504wac86klmq2pfhk2xgg518ypkrycrwiff6mh01657c4pr1l3") (r "1.63")))

(define-public crate-test-with-tokio-macros-0.2.0 (c (n "test-with-tokio-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "1ahvipj0lv5wawpdlcfv0vf2rn2szq3i6w7x1cg7spdc3kwsxqd4") (r "1.63")))

(define-public crate-test-with-tokio-macros-0.3.0 (c (n "test-with-tokio-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1dad83ffp447gwc0ldnrb2mciyhrxnwk5iddzy93hb3fwn2j96si") (r "1.63")))

(define-public crate-test-with-tokio-macros-0.3.1 (c (n "test-with-tokio-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "066a4xais0fcgi878dkmmyqlrw7d4z6cq3wib3npln5sr4yiqi59") (r "1.63")))

(define-public crate-test-with-tokio-macros-0.3.3 (c (n "test-with-tokio-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1aa7nsjd3jr0p9x9pwhk3i721klg9r014al6kr93vga1w2qbbglj") (r "1.63")))

