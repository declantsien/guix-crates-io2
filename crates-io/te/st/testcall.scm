(define-module (crates-io te st testcall) #:use-module (crates-io))

(define-public crate-testcall-0.0.0 (c (n "testcall") (v "0.0.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "1qqgkb96n9jkma3q0yx22lwhwnx5pvr774qwng4sdxxnil0gczbm")))

(define-public crate-testcall-0.0.1 (c (n "testcall") (v "0.0.1") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "0wnsdifa6sighqchlyndl3bpp1nsk8jhgwpwhjivdvmm3yy5pdlk")))

(define-public crate-testcall-0.1.0 (c (n "testcall") (v "0.1.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "0ls5ry0rzz0qdkdxfxmndcysjxsbhlmzy10xg4n8pyjvzf0s8mbq")))

(define-public crate-testcall-0.2.0-pre0 (c (n "testcall") (v "0.2.0-pre0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "1ax10x61ww2wvr4576xbk10j2n7hpv40idybdcj131jab7j3dgl8")))

(define-public crate-testcall-0.2.0 (c (n "testcall") (v "0.2.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "07jl3f48nqv90jyxays2zpj1n4pmjzsfwb10z0xr20zwf6ax0wnb")))

(define-public crate-testcall-0.3.0 (c (n "testcall") (v "0.3.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "1m3d5rh8kcnhv8bj1lmw8vgjg9mrp2asvcqmvdp0znxczdi066m1")))

(define-public crate-testcall-0.3.1 (c (n "testcall") (v "0.3.1") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "084m041ankqjlysbr93rikhlbczzbmd6488wkkyzv09pf9r4spyw")))

(define-public crate-testcall-0.4.0 (c (n "testcall") (v "0.4.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1h660cdravk8v64pj47i2vngbinwpqm1bybifijd1lddqaw16951")))

(define-public crate-testcall-0.5.0 (c (n "testcall") (v "0.5.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "testpath") (r "^0.1.1") (d #t) (k 0)))) (h "1aw89zlsrmjga0r88nipi5x7w1cv2dm76a8whp8jqx7fpq44p024")))

(define-public crate-testcall-0.6.0 (c (n "testcall") (v "0.6.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "testpath") (r "^0.1.1") (d #t) (k 0)))) (h "1dwbpzi3gy4i1xbx7iiw5bpy5r505xlxysgcscmfdf45dbbvb099")))

(define-public crate-testcall-1.0.0 (c (n "testcall") (v "1.0.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "testpath") (r "^0.1.1") (d #t) (k 0)))) (h "13gb3ji0y9y5ivs14y012wbia1qgh195z5x74vrkcgijh7197037")))

(define-public crate-testcall-1.1.0 (c (n "testcall") (v "1.1.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "testpath") (r "^0.1.1") (d #t) (k 0)))) (h "0ky8a943i599ydlwj87dhc7x1jak9ws3hzkmim8w3bf6ih2k6j5q")))

(define-public crate-testcall-1.2.0 (c (n "testcall") (v "1.2.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "testpath") (r "^0.1.1") (d #t) (k 0)))) (h "042bzwqcjn15n61rvz94zvbii8wazzw5fk9c9ng0k7b87whnw9p2")))

(define-public crate-testcall-1.3.0 (c (n "testcall") (v "1.3.0") (d (list (d (n "bintest") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "testpath") (r "^0.2.0") (d #t) (k 0)))) (h "0x2chrz9mgbs43mpbdam3xx9rz2lbxkincdsb90y9zc03wcia754")))

