(define-module (crates-io te st testpath) #:use-module (crates-io))

(define-public crate-testpath-0.0.0 (c (n "testpath") (v "0.0.0") (h "1njam45ijs63w4nnb5hwbwnlsqwx65m1lb02dyqwpqjpq4q8s4nv")))

(define-public crate-testpath-0.0.1 (c (n "testpath") (v "0.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "1s0c4ihmljrn31sslnl2v8l76vcblpbc5kx2nbg86wfi1wdcijad")))

(define-public crate-testpath-0.1.0 (c (n "testpath") (v "0.1.0") (d (list (d (n "path-absolutize") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "testcall") (r "^0.4") (d #t) (k 0)))) (h "0lysbzskdd1m9lmcinwqwka1hxwsbxm1w8q4qm1ahfsaq78iz54w")))

(define-public crate-testpath-0.1.1 (c (n "testpath") (v "0.1.1") (d (list (d (n "path-absolutize") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "testcall") (r "^0.4") (d #t) (k 0)))) (h "13n7swjj2h2v32668hv58xpy0myawsfrb2lxp5r3ysixsagq8r9z")))

(define-public crate-testpath-0.2.0 (c (n "testpath") (v "0.2.0") (d (list (d (n "path-absolutize") (r "^3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "testcall") (r "^1") (d #t) (k 0)))) (h "04kf6q0k9lz9dv5vf7w3f36h846l27pia63pw73w35wpx3zmvd3w")))

