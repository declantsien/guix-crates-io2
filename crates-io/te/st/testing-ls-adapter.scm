(define-module (crates-io te st testing-ls-adapter) #:use-module (crates-io))

(define-public crate-testing-ls-adapter-0.0.1 (c (n "testing-ls-adapter") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lsp-types") (r "^0.95.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "testing-language-server") (r "^0.0.1") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.5") (d #t) (k 0)) (d (n "tree-sitter-javascript") (r "^0.21.0") (d #t) (k 0)) (d (n "tree-sitter-rust") (r "^0.21.2") (d #t) (k 0)))) (h "1mvzb41jk0dxibgzvz8ph9m110x2p0x31hban4xybg3rzc33g6h3")))

