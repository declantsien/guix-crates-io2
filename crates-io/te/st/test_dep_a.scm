(define-module (crates-io te st test_dep_a) #:use-module (crates-io))

(define-public crate-test_dep_a-1.0.0 (c (n "test_dep_a") (v "1.0.0") (h "0fvlnssadicrhxwac7bbd4jws498d2c48chyk6zgdlf97z78zjxl")))

(define-public crate-test_dep_a-2.0.0 (c (n "test_dep_a") (v "2.0.0") (h "1pn4kqg7kzm60xp7zfszvzlsa1mjyyrds8saippsgrhlfdg88a1j")))

