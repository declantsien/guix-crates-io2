(define-module (crates-io te st test_zia) #:use-module (crates-io))

(define-public crate-test_zia-0.1.0 (c (n "test_zia") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 0)))) (h "1kq1dkngl4v869rx55cdg8xcqbacfsv3kw2v2fspqrai1m9jnfx1")))

(define-public crate-test_zia-0.2.0 (c (n "test_zia") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 0)))) (h "0ycf3ysg6yaka45z9pnynjp7b25zz5wcb20g44y4dyzlpk4705l9")))

(define-public crate-test_zia-0.4.0 (c (n "test_zia") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 0)))) (h "1wkism7mvrcmkrqn5s6asi4ax4dfmb30g72r6ximviaxp205czj1")))

(define-public crate-test_zia-0.5.0 (c (n "test_zia") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 0)))) (h "04blzhfj42kbrwkn6nb2bdk6xd6i29fxcykpdadsiwa44qla8d38")))

