(define-module (crates-io te st test-collector-utils) #:use-module (crates-io))

(define-public crate-test-collector-utils-0.1.0 (c (n "test-collector-utils") (v "0.1.0") (d (list (d (n "inventory") (r "^0.1.11") (d #t) (k 0)))) (h "0vxaywhyk57122imaadfpvk6nqzsa5xsxbml1wwm828vzxpdzxhb")))

(define-public crate-test-collector-utils-0.1.2 (c (n "test-collector-utils") (v "0.1.2") (d (list (d (n "inventory") (r "^0.1.11") (d #t) (k 0)))) (h "13cc2d2dbij70pc14n2kd75xxi9324nwk1klxa7x3dkbf02ynfz3")))

