(define-module (crates-io te st test_suite_rs) #:use-module (crates-io))

(define-public crate-test_suite_rs-0.1.0 (c (n "test_suite_rs") (v "0.1.0") (h "0bwlrp2xzp3w2yrmmkbwff871jwdl1w1x3g8a5zc797k2fq2bpq2")))

(define-public crate-test_suite_rs-0.1.1 (c (n "test_suite_rs") (v "0.1.1") (h "0ll84bkd1lw45qxhwzrlva9shibfd6f3r1bxxri6cbc36xfc3ms6")))

(define-public crate-test_suite_rs-0.1.2 (c (n "test_suite_rs") (v "0.1.2") (h "16bkagszyf17hykbkjfnhm1nd359902cw3f4d28xdv1qd37kqqm9") (y #t)))

(define-public crate-test_suite_rs-0.1.3 (c (n "test_suite_rs") (v "0.1.3") (h "1l182pnyaxx5w90bgsaycfl7xcqqk1jg9lbvqir69ywz6ipzngfz")))

(define-public crate-test_suite_rs-0.1.4 (c (n "test_suite_rs") (v "0.1.4") (h "0cdfkpaxlbjjcm4lqgawggipssw564x4dvnacb9sp89fkvwk4xcv")))

