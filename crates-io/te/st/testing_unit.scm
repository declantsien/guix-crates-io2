(define-module (crates-io te st testing_unit) #:use-module (crates-io))

(define-public crate-testing_unit-1.1.3 (c (n "testing_unit") (v "1.1.3") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0xplns2yp2qw4sab2wbcfixfflnlzswcmiajnzahwwd3mhkkkxhg")))

(define-public crate-testing_unit-1.2.0 (c (n "testing_unit") (v "1.2.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "10gpgq429w2xhb416aik9vw9pvpfgxirmxmbmfziz6vbadiyq53f")))

(define-public crate-testing_unit-1.2.1 (c (n "testing_unit") (v "1.2.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "17k5pk8qgf141l00h7hh3fpkdgrap2if0vnj23kknk0md54bmsbz")))

