(define-module (crates-io te st testfile) #:use-module (crates-io))

(define-public crate-testfile-0.0.1 (c (n "testfile") (v "0.0.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)))) (h "1r4gml628jy8v6zh8zbmcazmsjnh9rz9457rbrjx3lnk0qrpwmc8")))

(define-public crate-testfile-0.0.2 (c (n "testfile") (v "0.0.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)))) (h "1wxnh1kyq1db7xafh2b29614c0x795igzbfm8vf1q8yar8lb9nsk")))

(define-public crate-testfile-0.0.3 (c (n "testfile") (v "0.0.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)))) (h "0l0am00ip5gnm73fs710r1pp60v782p94wlkx2qg42jlbgkmv84f")))

(define-public crate-testfile-0.0.4 (c (n "testfile") (v "0.0.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)))) (h "0w2qgv8xavsyssmg59wsk7aiqhmysij5bvvi4vxw7xpks1v9j6jy")))

(define-public crate-testfile-0.1.0 (c (n "testfile") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.16") (d #t) (k 2)))) (h "1hzic4xy1ipkw99fwgcpysiws2nn2dk802jfa9bqpfn3mg5nyazd")))

(define-public crate-testfile-0.1.1 (c (n "testfile") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.16") (d #t) (k 2)))) (h "1wjikiksjbjgb41hdlsdyr7i3n8xbijz2bv17xb99242ki7j0qzp")))

(define-public crate-testfile-0.1.2 (c (n "testfile") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.16") (d #t) (k 2)))) (h "1wsk71ihwwrj3mcqkhmjiy8b3h3r2m8y0lzz0g5hs3jc43j6xxzm")))

(define-public crate-testfile-0.1.3 (c (n "testfile") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.16") (d #t) (k 2)))) (h "0534nh2ca3p84cxbbfynapidka205ivpb88msszd8yffypblpyd2")))

(define-public crate-testfile-0.1.4 (c (n "testfile") (v "0.1.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0.17") (d #t) (k 2)))) (h "01q36m6v5vm5qmcvzqfpl995hyxmvwiphf0lb5pjkas6rwbmfdy6")))

