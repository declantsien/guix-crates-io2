(define-module (crates-io te st test_crates_io) #:use-module (crates-io))

(define-public crate-test_crates_io-0.1.0 (c (n "test_crates_io") (v "0.1.0") (h "09id27sy164arydrdvikxjn3qrcwc63r8451rk4qjqyn6ivl7h6g")))

(define-public crate-test_crates_io-0.2.0 (c (n "test_crates_io") (v "0.2.0") (h "0jgnawwhn28bzq5552pbnhg4v60d85vw4jvnil59jdsc7a0v8vd9")))

