(define-module (crates-io te st testing-rust-crates) #:use-module (crates-io))

(define-public crate-testing-rust-crates-0.1.0 (c (n "testing-rust-crates") (v "0.1.0") (h "1a0588mnqzy7rw6x4nvfljc3df66j1qx75wry8xwbw38p0g46468")))

(define-public crate-testing-rust-crates-0.1.1 (c (n "testing-rust-crates") (v "0.1.1") (h "0lq452hq9azbqlsfagcanjf8aqzmrm2ysiikzxs91cnfls70pv6w")))

(define-public crate-testing-rust-crates-0.1.2 (c (n "testing-rust-crates") (v "0.1.2") (h "09w76vm5wl2rk6nfn7jyfzaw84shc44jczi19s4bz3dq3g2ihzp2")))

(define-public crate-testing-rust-crates-0.1.3 (c (n "testing-rust-crates") (v "0.1.3") (h "1gxh112zxxm0ygi4izgn2fsg0rzk5ahgchpnl0igz8xjn7656bn4")))

(define-public crate-testing-rust-crates-0.1.4 (c (n "testing-rust-crates") (v "0.1.4") (h "0rk4s5jp21rc8sm82y72mzpd8wpxnxfmk2jijjffgaaqijf5x1hk")))

(define-public crate-testing-rust-crates-0.1.5 (c (n "testing-rust-crates") (v "0.1.5") (h "0vqlp7vy9jd67rq0n0vahw12as8d5hw8xd9imj3dpg18ckjn0swb")))

(define-public crate-testing-rust-crates-0.0.0 (c (n "testing-rust-crates") (v "0.0.0") (h "1x9wcb8m2gw3hk37g4k1vghd6j5dca4h3md88p5y1c1x21xlr710")))

(define-public crate-testing-rust-crates-0.1.6 (c (n "testing-rust-crates") (v "0.1.6") (h "0g2qrgwpfgykv7crm31fybaj06h8xvz0n7b4kjx9zbzxcacgh431")))

