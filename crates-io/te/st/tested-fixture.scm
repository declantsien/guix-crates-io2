(define-module (crates-io te st tested-fixture) #:use-module (crates-io))

(define-public crate-tested-fixture-1.0.0 (c (n "tested-fixture") (v "1.0.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tested-fixture-macros") (r "^1") (d #t) (k 0)))) (h "013zc3qnwxc3qqrsbf5b1k0shm4fyfwyc1l2sxzr7gwfspxbrbcs") (r "1.69")))

(define-public crate-tested-fixture-1.0.1 (c (n "tested-fixture") (v "1.0.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tested-fixture-macros") (r "^1") (d #t) (k 0)))) (h "0dv36nj5j4wdf3anz4nb6zqdcynp7ncz6hwkf4ni7f8n4x9h9hkn") (r "1.69")))

