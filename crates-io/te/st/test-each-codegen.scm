(define-module (crates-io te st test-each-codegen) #:use-module (crates-io))

(define-public crate-test-each-codegen-0.1.0 (c (n "test-each-codegen") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0va5mbbbp24w6j4ddp5vc3p8zgg5hh6bff1wa16hg7mwdmdg40vv")))

(define-public crate-test-each-codegen-0.2.0 (c (n "test-each-codegen") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0nfm67vvp3ccww6jpnhsyfgw7c2h0lljbid3bc7dhph9xmfciin1")))

(define-public crate-test-each-codegen-0.2.1 (c (n "test-each-codegen") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "08i1zyhcgkqzs7qbl47zmfz3zdlv5s0x08iqd3pcbgfx98rbwnxd")))

