(define-module (crates-io te st test-toolbox) #:use-module (crates-io))

(define-public crate-test-toolbox-0.1.0 (c (n "test-toolbox") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "gag") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1xj8yki91n5rjrna236i3ix18cbypy0a0x0rgkr5rc5g7m41cvqk") (f (quote (("expected" "cfg-if") ("default") ("capture" "gag") ("all" "actual" "capture" "expected") ("actual" "cfg-if"))))))

(define-public crate-test-toolbox-0.2.0 (c (n "test-toolbox") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "gag") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "08pzkfdkxa37bpdavk4cfhxlsl6055sb2hnzi97cn1g6az0i3n75") (f (quote (("expected" "cfg-if") ("default") ("capture" "gag") ("all" "actual" "capture" "expected") ("actual" "cfg-if"))))))

(define-public crate-test-toolbox-0.3.0 (c (n "test-toolbox") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "gag") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ak0yfciclgrhsw577p0fpkh1i98jyi8kv6q2iw18948aywxcfcz") (f (quote (("expected" "cfg-if") ("default") ("capture" "gag") ("all" "actual" "capture" "expected") ("actual" "cfg-if"))))))

(define-public crate-test-toolbox-0.4.0 (c (n "test-toolbox") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "gag") (r "^1") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0q95535vqyahqly53p28d704x2lzi28b47g1mfm9gglm2nrfkdg8") (f (quote (("expected" "cfg-if") ("default") ("capture" "gag") ("all" "actual" "capture" "expected") ("actual" "cfg-if"))))))

(define-public crate-test-toolbox-0.5.0 (c (n "test-toolbox") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "gag") (r "^1") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1plkrag7wzc6amzvvx6k6nsnlg8jncaa3n2v9blb13cyh4hpd894") (f (quote (("expected" "cfg-if") ("default") ("capture" "gag") ("all" "actual" "capture" "expected") ("actual" "cfg-if"))))))

