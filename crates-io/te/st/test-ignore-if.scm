(define-module (crates-io te st test-ignore-if) #:use-module (crates-io))

(define-public crate-test-ignore-if-0.1.0 (c (n "test-ignore-if") (v "0.1.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1qz1kysr315g3jxah4qqi0hcbmhb6j8rwmvciszsakr3ng79dpy8")))

