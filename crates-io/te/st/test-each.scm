(define-module (crates-io te st test-each) #:use-module (crates-io))

(define-public crate-test-each-0.1.0 (c (n "test-each") (v "0.1.0") (d (list (d (n "test-each-codegen") (r "^0.1.0") (d #t) (k 0)))) (h "110f0yrdqr66rszi6rjas7kijjlgbzfi5zlp42g3fxggl9as14n8")))

(define-public crate-test-each-0.2.0 (c (n "test-each") (v "0.2.0") (d (list (d (n "test-each-codegen") (r "^0.2.0") (d #t) (k 0)))) (h "060pmf7xlbidv67nk5ssn6yrgd1va4zfnappiqpd6r85sbv9carh")))

(define-public crate-test-each-0.2.1 (c (n "test-each") (v "0.2.1") (d (list (d (n "test-each-codegen") (r "^0.2.1") (d #t) (k 0)))) (h "1g75c2rzzr2fl13mmi0355llq57rhmnc4524qfnxia6ly4f728nq")))

