(define-module (crates-io te st test-temp-dir) #:use-module (crates-io))

(define-public crate-test-temp-dir-0.1.0 (c (n "test-temp-dir") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "educe") (r "^0.4.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "15qqmqjrkwwgncdji9z02vvqhvjc2axnbrhjizxgvnxrgknjwmnh") (f (quote (("full") ("default")))) (r "1.70")))

(define-public crate-test-temp-dir-0.2.0 (c (n "test-temp-dir") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "educe") (r "^0.4.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1d9w0ixglcqnlr5fvwczqgfkypqlmgsj2br2k4s45nb6qxsb1pg2") (f (quote (("full") ("default")))) (r "1.70")))

(define-public crate-test-temp-dir-0.2.1 (c (n "test-temp-dir") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.23") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "educe") (r "^0.4.6") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1xicni1y0kqfjfw4ysxcldlzdj8lh4wvvs86slprf5vwhp8chavj") (f (quote (("full") ("default")))) (r "1.70")))

