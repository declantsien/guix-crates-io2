(define-module (crates-io te st test-publish-crate) #:use-module (crates-io))

(define-public crate-test-publish-crate-0.1.0 (c (n "test-publish-crate") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)))) (h "1im48wx7i6wby232vxc21wdpsimqig6aw4arbrp6sndpbx5c3v6d")))

