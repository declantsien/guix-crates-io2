(define-module (crates-io te st test4a) #:use-module (crates-io))

(define-public crate-test4a-0.1.0 (c (n "test4a") (v "0.1.0") (h "19l8dk3bs3r8vbic8gsgkmn6zbmmnr1l145a763a80ich4bjvpps")))

(define-public crate-test4a-0.1.1 (c (n "test4a") (v "0.1.1") (h "1xq7mbl3ga1f1r76yxh9sxrgxkw95hfhplplhmp7sc0p96b3hab5")))

