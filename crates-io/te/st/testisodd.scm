(define-module (crates-io te st testisodd) #:use-module (crates-io))

(define-public crate-testisodd-0.1.0 (c (n "testisodd") (v "0.1.0") (h "0w6lrjfs3bij1kgxpgk8nb2kl31c9m5p2h4kmpzx7sh4d9z6bd0q")))

(define-public crate-testisodd-0.1.1 (c (n "testisodd") (v "0.1.1") (h "07r4iajvxxfi2ginjdgrk3dmkql0wnvw5i13kp32bzjc3pnmxj6c")))

