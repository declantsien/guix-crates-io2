(define-module (crates-io te st test-rusty-grader) #:use-module (crates-io))

(define-public crate-test-rusty-grader-0.1.0 (c (n "test-rusty-grader") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "19fmbxlcwkw2pg12gaxnwc5y3al857ia9bn014j2vmi56bmyk70y")))

