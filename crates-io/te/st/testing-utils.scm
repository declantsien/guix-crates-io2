(define-module (crates-io te st testing-utils) #:use-module (crates-io))

(define-public crate-testing-utils-0.1.0 (c (n "testing-utils") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "11961af3zz5ps17l3mhircxp8bkhslbzihfxsbhl43np61ylx61y")))

(define-public crate-testing-utils-0.1.1 (c (n "testing-utils") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1canswv9xaba2v0i2gs8s0av1b4ypxhih07xk9alf66mh99fk4js")))

(define-public crate-testing-utils-0.1.2 (c (n "testing-utils") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (f (quote ("std" "std_rng"))) (k 0)))) (h "0dhcpl94hz8b3jp6f2iv8bi9yxrr21fj5h51r1c9827dwi6jj0xm")))

