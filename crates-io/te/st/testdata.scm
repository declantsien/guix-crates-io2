(define-module (crates-io te st testdata) #:use-module (crates-io))

(define-public crate-testdata-0.1.0 (c (n "testdata") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)) (d (n "testdata-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "testdata-rt") (r "^0.1.0") (d #t) (k 0)))) (h "1n26ial6sq015la4ngm24m67i9hb0pbai5wpgk2q4i6d99haxs4r") (f (quote (("json" "testdata-rt/json") ("__doc_cfg" "testdata-rt/__doc_cfg"))))))

