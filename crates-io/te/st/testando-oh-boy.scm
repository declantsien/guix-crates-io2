(define-module (crates-io te st testando-oh-boy) #:use-module (crates-io))

(define-public crate-testando-oh-boy-3.0.0 (c (n "testando-oh-boy") (v "3.0.0") (h "0bh92rvphjws0x8374z4nakpka7swf727cqm3fkrbwrcw75mwimn") (y #t) (r "1.61.0")))

(define-public crate-testando-oh-boy-4.0.0 (c (n "testando-oh-boy") (v "4.0.0") (h "0sxmr73x3mhy69ns45jgdij1zw9nawkws0ssc1f1m2fz89501v3w") (r "1.61.0")))

