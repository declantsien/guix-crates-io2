(define-module (crates-io te nh tenhou-shuffle) #:use-module (crates-io))

(define-public crate-tenhou-shuffle-0.1.0 (c (n "tenhou-shuffle") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (k 0)) (d (n "sha2") (r "^0.10") (k 0)))) (h "0rpzjf6666llsqrm521s8yvlvkp8cpisk5xqvbilrf9838y6207v")))

(define-public crate-tenhou-shuffle-0.1.1 (c (n "tenhou-shuffle") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (k 0)) (d (n "sha2") (r "^0.10") (k 0)))) (h "1r2w64ablf7svkpigfdsmp66fr92p5fxacjgch45fv8fa33g5sy0")))

(define-public crate-tenhou-shuffle-0.1.2 (c (n "tenhou-shuffle") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (k 0)) (d (n "sha2") (r "^0.10") (k 0)))) (h "0wp286wlg2pkd1pwfdmm3rlzj45hjzwynfjpf9i6iyppfxlmaxmh")))

