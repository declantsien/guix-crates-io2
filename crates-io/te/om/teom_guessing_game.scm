(define-module (crates-io te om teom_guessing_game) #:use-module (crates-io))

(define-public crate-teom_guessing_game-0.1.0 (c (n "teom_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vv6l060s5mljwjsd7l3ss5qqlrh97jrcfp8sf8a0rxpkr8b7dgc")))

(define-public crate-teom_guessing_game-0.1.1 (c (n "teom_guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wwlbzbmwsfxyc8x4h29yaf09fzqmxlz0071mdq4knn9xmfhmgi8")))

