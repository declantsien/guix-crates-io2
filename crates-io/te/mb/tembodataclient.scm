(define-module (crates-io te mb tembodataclient) #:use-module (crates-io))

(define-public crate-tembodataclient-0.0.1 (c (n "tembodataclient") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "01xxgxl8d3v4042dk8mn958s26wzv1acs1n318l119n85xf3dqsm")))

(define-public crate-tembodataclient-0.0.2 (c (n "tembodataclient") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0ddl79d4k9ylmv3z14ypibddzr072b4wkdn106gsf47w1h9w2ccq")))

