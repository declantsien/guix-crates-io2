(define-module (crates-io te ap teapot) #:use-module (crates-io))

(define-public crate-teapot-0.1.0 (c (n "teapot") (v "0.1.0") (h "1svqlldcgw18vfm1pb37gffzdbl90alf7i8mmfzrh1i7pi22sm5r")))

(define-public crate-teapot-0.1.1 (c (n "teapot") (v "0.1.1") (h "004br9fn1m5rzxvr10xnagds696s3z66k0mkk4sr34zcvah90grp")))

(define-public crate-teapot-0.1.2 (c (n "teapot") (v "0.1.2") (h "0fkd0nm4v9209nqrl6p1bga0vih59qp7mjig9rhg0d4w83qm2iaf")))

