(define-module (crates-io vu e_ vue_convert) #:use-module (crates-io))

(define-public crate-vue_convert-0.1.1 (c (n "vue_convert") (v "0.1.1") (d (list (d (n "askama") (r "^0.10.1") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "select") (r "^0.4.3") (d #t) (k 0)))) (h "0v8524c33ipqj46ivjngfhmz4fp83qp4mzykq2hv5nhwqv217lng")))

