(define-module (crates-io vu lk vulkan_raw) #:use-module (crates-io))

(define-public crate-vulkan_raw-0.1.1 (c (n "vulkan_raw") (v "0.1.1") (h "0vky2f42zka5p8kq437vgdm0i43rvpsbs6knfpigj6d02apkf982")))

(define-public crate-vulkan_raw-0.1.2 (c (n "vulkan_raw") (v "0.1.2") (h "0s4d934j7hb1f9npqhfwmvz2yv5kfw7vflg33lqi8q6za6zgyp4k") (f (quote (("docs"))))))

(define-public crate-vulkan_raw-0.1.3 (c (n "vulkan_raw") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0xsp1qrqkdf0nyvyf4zcn8k38as7ldsdkgmgazibiqz1yzg21xql") (f (quote (("docs"))))))

(define-public crate-vulkan_raw-0.1.4 (c (n "vulkan_raw") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1wzvjybhqx70l5bl16csjgb23k2b8jk2fnma85d63ysj4059wrr2") (f (quote (("docs"))))))

(define-public crate-vulkan_raw-0.1.5 (c (n "vulkan_raw") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0pvfaki0cgccm9j79byyk7pb7ggdsdnw1kdxjnaacas0g1r1igv6") (f (quote (("docs"))))))

(define-public crate-vulkan_raw-0.1.6 (c (n "vulkan_raw") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "0lx2sr6gvy9wp49w4by95by97alkr005lr6hq0nmy6j56p5l1s4z") (f (quote (("docs"))))))

(define-public crate-vulkan_raw-0.1.7 (c (n "vulkan_raw") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1w4qf78a1whiw1wyskdyj4ijrjjh5xvzhzmlzzmnwvngprhm358n") (f (quote (("docs"))))))

(define-public crate-vulkan_raw-0.1.8 (c (n "vulkan_raw") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)))) (h "0d1vvgj8hrsk5p7r1cgv0i0xkgxdwhzsjx432y432y23sfgzmb1j") (f (quote (("docs"))))))

(define-public crate-vulkan_raw-0.1.9 (c (n "vulkan_raw") (v "0.1.9") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0jf81wxxb8g21g6vfqwk315xxns6rps9xwkn0ymwiaihkf6x0fmf") (f (quote (("default" "VulkanMemoryAllocator" "VK_KHR_surface" "VK_KHR_swapchain" "VK_KHR_deferred_host_operations" "VK_KHR_pipeline_library" "VK_KHR_acceleration_structure" "VK_KHR_ray_tracing_pipeline" "VK_KHR_ray_query" "VK_EXT_debug_utils" "VK_EXT_index_type_uint8" "VK_EXT_memory_budget") ("VulkanMemoryAllocator") ("VK_VERSION_1_3") ("VK_KHR_win32_surface" "VK_KHR_surface") ("VK_KHR_swapchain" "VK_KHR_surface") ("VK_KHR_surface") ("VK_KHR_ray_tracing_pipeline" "VK_KHR_acceleration_structure") ("VK_KHR_ray_query" "VK_KHR_acceleration_structure") ("VK_KHR_pipeline_library") ("VK_KHR_external_fence_win32") ("VK_KHR_external_fence_fd") ("VK_KHR_deferred_host_operations") ("VK_KHR_acceleration_structure" "VK_KHR_deferred_host_operations") ("VK_EXT_memory_budget") ("VK_EXT_index_type_uint8") ("VK_EXT_debug_utils"))))))

