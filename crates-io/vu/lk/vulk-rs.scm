(define-module (crates-io vu lk vulk-rs) #:use-module (crates-io))

(define-public crate-vulk-rs-0.0.1 (c (n "vulk-rs") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1yd1sggkdp5avci21l516q8j3nr0fq81m3rrq7r9hxcfc7kv7n3k")))

(define-public crate-vulk-rs-0.0.2 (c (n "vulk-rs") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "0jm1imv36n6j3rbbrnjhc64iqpam6546kzkng7byvmqmx0fikls3")))

