(define-module (crates-io vu lk vulkan-sys) #:use-module (crates-io))

(define-public crate-vulkan-sys-0.0.1 (c (n "vulkan-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1z959648qgnafkdxggx1yijc90g1cjbd901zcwl8i5yxlxbi4w55")))

(define-public crate-vulkan-sys-0.0.2 (c (n "vulkan-sys") (v "0.0.2") (h "1qx46njgyadw66l7f0x6dkwbiwzwxyrzi5129i5mlydhkmwybff6")))

(define-public crate-vulkan-sys-0.1.0 (c (n "vulkan-sys") (v "0.1.0") (h "086h6z1vr89x7hhgljsmk34klf6mpjqpwjbrlzwwchj04hl7a8dc")))

(define-public crate-vulkan-sys-0.1.1 (c (n "vulkan-sys") (v "0.1.1") (h "0p3bq3snfsb7rxvlp2fy28avlb73idjpfb04r6l3gr4b9dnw0bwm")))

