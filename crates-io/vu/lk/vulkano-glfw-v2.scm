(define-module (crates-io vu lk vulkano-glfw-v2) #:use-module (crates-io))

(define-public crate-vulkano-glfw-v2-0.1.0 (c (n "vulkano-glfw-v2") (v "0.1.0") (d (list (d (n "glfw") (r "^0.32.0") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "vk-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)))) (h "0kkvc3i8c0nvf2c5j9nm2fjf71c9pgvky1glczr3pw079paj3hqz")))

(define-public crate-vulkano-glfw-v2-0.1.1 (c (n "vulkano-glfw-v2") (v "0.1.1") (d (list (d (n "glfw") (r "^0.32.0") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "vk-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)))) (h "0zyh51ksxgkxyw2lwf5hq062l7k07y4y7hdxyx5z2ygyjxzmkkjj")))

(define-public crate-vulkano-glfw-v2-0.1.2 (c (n "vulkano-glfw-v2") (v "0.1.2") (d (list (d (n "glfw") (r "^0.32.0") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "vk-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)))) (h "1jqcq76pz2zkv0fyazrxmfy4kb2jwm8l3zk16yqnrccyw3j5zrn3")))

(define-public crate-vulkano-glfw-v2-0.2.0 (c (n "vulkano-glfw-v2") (v "0.2.0") (d (list (d (n "glfw") (r "^0.37.0") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "vk-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "vulkano") (r "^0.18.0") (d #t) (k 0)))) (h "1dza2a9dpk2xz6hr1friynicxw38a3zsn9y28wap6m69kbgwbn0d")))

