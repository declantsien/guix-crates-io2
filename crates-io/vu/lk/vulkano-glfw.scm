(define-module (crates-io vu lk vulkano-glfw) #:use-module (crates-io))

(define-public crate-vulkano-glfw-0.3.0 (c (n "vulkano-glfw") (v "0.3.0") (d (list (d (n "glfw") (r "^0.21") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "vk-sys") (r "^0.3") (d #t) (k 0)) (d (n "vulkano") (r "^0.7") (d #t) (k 0)))) (h "0zgfxc6wxv22f078fzmri561s3zl86jhxngas3z947c24skyw7gy")))

(define-public crate-vulkano-glfw-0.4.0 (c (n "vulkano-glfw") (v "0.4.0") (d (list (d (n "glfw") (r "^0.21") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "vk-sys") (r "^0.3") (d #t) (k 0)) (d (n "vulkano") (r "^0.7") (d #t) (k 0)))) (h "1wy9jm9jmpfgfa159mz75ax6impynr5dxgwpla5w5i45bx0fpir8")))

(define-public crate-vulkano-glfw-0.5.0 (c (n "vulkano-glfw") (v "0.5.0") (d (list (d (n "glfw") (r "^0.21") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "vk-sys") (r "^0.3") (d #t) (k 0)) (d (n "vulkano") (r "^0.8") (d #t) (k 0)))) (h "1ryqls19cwz23cwa63vjlhqk1j37clrcm9xn5gxdbnjq12cswvn5")))

