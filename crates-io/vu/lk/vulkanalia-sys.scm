(define-module (crates-io vu lk vulkanalia-sys) #:use-module (crates-io))

(define-public crate-vulkanalia-sys-0.1.0 (c (n "vulkanalia-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0qk83srb1mrrcz04bs5q8p90a0mn7vb6ipa0jpf6gs98f54psxav")))

(define-public crate-vulkanalia-sys-0.2.0 (c (n "vulkanalia-sys") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1va7cjm3jdqqwbcx9pl2224iajdnbimkxac729pv46waclh5abaz")))

(define-public crate-vulkanalia-sys-0.2.1 (c (n "vulkanalia-sys") (v "0.2.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "00pwwhqca4yxdh2yw874h65883nq9np14wx43abpl18ywl3zvldz")))

(define-public crate-vulkanalia-sys-0.2.2 (c (n "vulkanalia-sys") (v "0.2.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "01x2gdga6xynyp59n3zh3w6j7laysc12chpkghf3dzgvfv96y6ib")))

(define-public crate-vulkanalia-sys-0.2.3 (c (n "vulkanalia-sys") (v "0.2.3") (d (list (d (n "bitflags") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1n9wji75cbgmkxbj91z6g8lx01i3by4r28lqkfjh530w6k0zgshz")))

(define-public crate-vulkanalia-sys-0.3.0 (c (n "vulkanalia-sys") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0c05jzapwwrsihnrac119amc6mph4hf30x8x30m77ia402rj9016") (y #t)))

(define-public crate-vulkanalia-sys-0.4.0 (c (n "vulkanalia-sys") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1lsx7y79nqlfgazvmb98w8almmsvdm6561b561ggy13cgnankysk") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.5.0 (c (n "vulkanalia-sys") (v "0.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0f5bix2hymr3gdzg70jmsnxpzqrxh9ylv6lcpqlaxs2gc02g5c85") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.6.0 (c (n "vulkanalia-sys") (v "0.6.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0mdrf0h7pdldhnkf5ajha5432j1acpgdgfsqhbmcnk7r22q1s408") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.7.0 (c (n "vulkanalia-sys") (v "0.7.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0gmgd0fj3lrmzhx7jvxkp1azqsyskbwfnpqhj4bwqvx1gjplphg2") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.8.0 (c (n "vulkanalia-sys") (v "0.8.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0yz1yy40pcq0bnjx80klmlrygmqjndwg5ys4dihl9a49j960rb9j") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.9.0 (c (n "vulkanalia-sys") (v "0.9.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1r1iz1fj4a445576lycvyqkb6ph2mgd40yx8i17gjrs0jlb5b28w") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.10.0 (c (n "vulkanalia-sys") (v "0.10.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0lf5bgc6fmny2yjs6h9cwavr0dw5mcc6sy9xfi1yy7g7ifqxfcrj") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.11.0 (c (n "vulkanalia-sys") (v "0.11.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "17faims21s8rmsdqnlwgh6qknnxk5vsis6qmpmm9v04zwwy8r94x") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.12.0 (c (n "vulkanalia-sys") (v "0.12.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1k6rslagdwgb1f1pj79alfrbg8h4acaycdijjjgyh81rx3yj8kpi") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.13.0 (c (n "vulkanalia-sys") (v "0.13.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0j26wrbzc6g3vzccx0mq3gsnj9yafl95d8bsvhw3hazvwd60m2id") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.14.0 (c (n "vulkanalia-sys") (v "0.14.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0x9xd1bv6gifak15a434pwcm21yn5dxd0xfzsrm9ydmvfxzn3b9p") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.15.0 (c (n "vulkanalia-sys") (v "0.15.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "17x14grhx6321kwg7icgqvkg30lyjs1lcb69ss1wrfdzbi2z9z1y") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.16.0 (c (n "vulkanalia-sys") (v "0.16.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "16wcj22imgz5jpjxwx7rzwxs1dpy2v54zf8fgsp0x4yndxfnl746") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.17.0 (c (n "vulkanalia-sys") (v "0.17.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "11q29xivgb48jghvk16j908cjq89xfzyn2k0xsan8iv8vca42n50") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.18.0 (c (n "vulkanalia-sys") (v "0.18.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "08fjbxlbcll8kwkw7gbv60x6iipyylr7s33bqfy99myykjy8kfyf") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.19.0 (c (n "vulkanalia-sys") (v "0.19.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "18hh6yar85s0hjw8649ld8v84w826686xyv3lglinbx6wvbysmnq") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.20.0 (c (n "vulkanalia-sys") (v "0.20.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "161xm042ci7idhlskjnvc9xi5243mwbr4nc16y6ib4i6y6k27rk5") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.21.0 (c (n "vulkanalia-sys") (v "0.21.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "1zv2jviahpakshi4vd5zpg4c12rvl5xbmnwkplz2dpcmbmmysdh0") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.22.0 (c (n "vulkanalia-sys") (v "0.22.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "009dls61s5hih76pg4w14pdbkc47b1y949w96zk2b52dmd5ppkdq") (f (quote (("provisional"))))))

(define-public crate-vulkanalia-sys-0.23.0 (c (n "vulkanalia-sys") (v "0.23.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "181n960yl4yai3ay5nn1z5g304yjcnvxah2ylpmwczzciicnvh0v") (f (quote (("std") ("provisional") ("no_std_error") ("default" "std"))))))

