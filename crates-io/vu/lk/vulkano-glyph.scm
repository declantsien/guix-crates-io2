(define-module (crates-io vu lk vulkano-glyph) #:use-module (crates-io))

(define-public crate-vulkano-glyph-0.0.0 (c (n "vulkano-glyph") (v "0.0.0") (h "0xk12wca842ln000p6iqcrpgwz6lcaw896fcqry861dj4qygnbhj")))

(define-public crate-vulkano-glyph-0.1.0 (c (n "vulkano-glyph") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.6.1") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.10.0") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.10.0") (d #t) (k 2)) (d (n "winit") (r "^0.17.0") (d #t) (k 2)))) (h "0l4xl7cxfcjdbjswg5ydq2ga7h9i5q0y0dyavhrq4wijr4ycy3yz")))

(define-public crate-vulkano-glyph-0.1.1 (c (n "vulkano-glyph") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.0") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.10.0") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.10.0") (d #t) (k 2)) (d (n "winit") (r "^0.17.0") (d #t) (k 2)))) (h "03j1qrzflml1g8kplg28ix9i9rscshdks1apd73sm88jg0lqnlsl")))

(define-public crate-vulkano-glyph-0.2.0 (c (n "vulkano-glyph") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.0") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.10.0") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.10.0") (d #t) (k 2)) (d (n "winit") (r "^0.17.0") (d #t) (k 2)))) (h "0nzyw7kl90cp12873pqhk9z1mxz90qz8pjs04rkvqmwh1nlxcz4p")))

(define-public crate-vulkano-glyph-0.3.0 (c (n "vulkano-glyph") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.0") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.11.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.11.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.11.0") (d #t) (k 2)) (d (n "winit") (r "^0.18.0") (d #t) (k 2)))) (h "0gxn6gihdbn26xph1iih7iwlqpx4ra0f9f3hm6v1jbss85ni20xx")))

(define-public crate-vulkano-glyph-0.4.0 (c (n "vulkano-glyph") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.0") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 2)) (d (n "vulkano") (r "^0.11.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.11.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.11.0") (d #t) (k 2)) (d (n "winit") (r "^0.18.0") (d #t) (k 2)))) (h "1d7zshi2kixhfv3a72mmr4g8afpyxbf309w4nbkcn6csscd47iyw")))

