(define-module (crates-io vu lk vulk-ext) #:use-module (crates-io))

(define-public crate-vulk-ext-0.1.3 (c (n "vulk-ext") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "0izxp74p7ypy19qymac1m1qrjdgs7cyih20mcqxqaixi3pzrnlnk")))

(define-public crate-vulk-ext-0.1.4 (c (n "vulk-ext") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "1mk2l1na4i37m575hvpnnzyw601prb4ij2wlya4b4c2431qxrxiw")))

(define-public crate-vulk-ext-0.1.5 (c (n "vulk-ext") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "1jzpg1kpw64di1cdzrc4dh41g18adm3xcm5a9cqbn6g58qkvs9nq")))

(define-public crate-vulk-ext-0.1.6 (c (n "vulk-ext") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "0s27030il1b3c8qrgjzgzgj18qdp2m2h0nh6qia4vrkrxqykk4k3")))

(define-public crate-vulk-ext-0.1.7 (c (n "vulk-ext") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "062p5wc1hf5nssnc0i9z7bccb5hy1nlnsibvk51kq4807qzfsbqq")))

(define-public crate-vulk-ext-0.1.8 (c (n "vulk-ext") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "1xx4nf2g9x19sp0c9ywymakm3hpl78dv06mpxczrp0av0638xv36")))

(define-public crate-vulk-ext-0.1.9 (c (n "vulk-ext") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "102vzr7l1mz3bdignajlpn16nfx3219c6157fgyr83prd2d7vr70")))

(define-public crate-vulk-ext-0.1.10 (c (n "vulk-ext") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "1h0xmhalmkih0vvwq0k48pwhl0x12lv8i4kbk47s8hckjz87w5vd")))

(define-public crate-vulk-ext-0.1.11 (c (n "vulk-ext") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "0rzsxxgdswxcp2gjffg2hakkw8vksyhgj93h73hnn69mcc6plz7y")))

(define-public crate-vulk-ext-0.1.12 (c (n "vulk-ext") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "000yjiaa25fl25959a76bwqcxf0zwwwp53d0v1i07y0qaj4g0qkm")))

(define-public crate-vulk-ext-0.1.13 (c (n "vulk-ext") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "0l0hyfcqv1hm6qcla17d6ppgybdpb54jjbi05413vfhcjb9a5iak")))

(define-public crate-vulk-ext-0.1.14 (c (n "vulk-ext") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "0r5fsad40sxdgqmha7c819qdmg0ghw085j2nirkr3d97b8alk4zq")))

(define-public crate-vulk-ext-0.1.15 (c (n "vulk-ext") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "0b7b89yl19yaf884gq9a6ddi17inqzhypz4fyg0ggxjaqlvik3j0")))

(define-public crate-vulk-ext-0.1.16 (c (n "vulk-ext") (v "0.1.16") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "05a539cs914d46adv906w7p7fg7p9z672lxhn8a4pgnw41c2h763")))

(define-public crate-vulk-ext-0.1.17 (c (n "vulk-ext") (v "0.1.17") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.8.2") (d #t) (k 0)) (d (n "vulk") (r "0.*") (d #t) (k 0)))) (h "0sp0scakr0gz80v82jcjyp2gmmbxvd5gl30chvndl3mi28hjbcn2")))

