(define-module (crates-io vu lk vulkano-framing) #:use-module (crates-io))

(define-public crate-vulkano-framing-0.1.0 (c (n "vulkano-framing") (v "0.1.0") (d (list (d (n "framing") (r "^0.2") (d #t) (k 0)) (d (n "png-framing") (r "^0.2") (d #t) (k 2)) (d (n "vulkano") (r "^0.5") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.5") (d #t) (k 2)))) (h "1w08w3ha1id5zk9ia2vrp404wgpd8qmbilz65camys98a0rrlbj0")))

(define-public crate-vulkano-framing-0.2.0 (c (n "vulkano-framing") (v "0.2.0") (d (list (d (n "framing") (r "^0.3") (d #t) (k 0)) (d (n "png-framing") (r "^0.3") (d #t) (k 2)) (d (n "vulkano") (r "^0.5") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.5") (d #t) (k 2)))) (h "1gbqcr764jgmc2l5cxzlyk1ra0w9sih6yazxmkdhgzf55bdnbfkv")))

(define-public crate-vulkano-framing-0.2.1 (c (n "vulkano-framing") (v "0.2.1") (d (list (d (n "framing") (r "^0.3") (d #t) (k 0)) (d (n "png-framing") (r "^0.3") (d #t) (k 2)) (d (n "vulkano") (r "^0.5") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.5") (d #t) (k 2)))) (h "1gkvq154a5ifl4m4cwfafrflmcv62fwf5qk6d91z406cbmzlh3bv")))

(define-public crate-vulkano-framing-0.2.2 (c (n "vulkano-framing") (v "0.2.2") (d (list (d (n "framing") (r "^0.3") (d #t) (k 0)) (d (n "png-framing") (r "^0.3") (d #t) (k 2)) (d (n "vulkano") (r "^0.5") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.5") (d #t) (k 2)))) (h "0n0gz6yn8zb6x7wn66qjy40jn971abjg71aqvn6ka98a18iwqaax")))

(define-public crate-vulkano-framing-0.3.0 (c (n "vulkano-framing") (v "0.3.0") (d (list (d (n "framing") (r "^0.6") (d #t) (k 0)) (d (n "png-framing") (r "^0.5") (d #t) (k 2)) (d (n "vulkano") (r "^0.6") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.6") (d #t) (k 2)))) (h "19fd2nvvc9bzwk8zdq8mj3zrdxw1j3klrj65m0dq8addparmzagk")))

