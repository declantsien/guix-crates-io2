(define-module (crates-io vu lk vulkan_rs_generator) #:use-module (crates-io))

(define-public crate-vulkan_rs_generator-0.1.0 (c (n "vulkan_rs_generator") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "106f11xqbpi3fyjs25dr0fyg0b08cnc8l0113lq145wpj5shzagg")))

