(define-module (crates-io vu lk vulk) #:use-module (crates-io))

(define-public crate-vulk-0.1.0 (c (n "vulk") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1487iccal3cvx030k1xh675gwav928n7p3235j0qg393pkgxzwl4")))

(define-public crate-vulk-0.1.1 (c (n "vulk") (v "0.1.1") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1p4qbn737kl9225biydnpzknk62qg6pslb9qrd4zddh08mjc9yn2")))

(define-public crate-vulk-0.1.2 (c (n "vulk") (v "0.1.2") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0n4y96mqpgbfldz850fvh1c75b83gpzrvd2654h7i3mwghsdxazi")))

(define-public crate-vulk-0.1.3 (c (n "vulk") (v "0.1.3") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1i2q3fp32pwa2wkmixk0bca1dzjh1iv7pi3hczlk5kxn3rqsai1d")))

(define-public crate-vulk-0.1.4 (c (n "vulk") (v "0.1.4") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "060jydlcrfd09nhbhc133qpdawig1sk10s81bqhwd4lrzlyrp4bk")))

(define-public crate-vulk-0.1.5 (c (n "vulk") (v "0.1.5") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0hy7xvhzds0ss3cz3m4vvfj2zbisr069z75akq13g01qbnq6x3xk")))

(define-public crate-vulk-0.1.6 (c (n "vulk") (v "0.1.6") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "18w75s9znnmwd3d1h71bp6w3xq3yx75m2bqpzllwx7440333gjyg")))

(define-public crate-vulk-0.1.7 (c (n "vulk") (v "0.1.7") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "064dy9cngqalj42qlc2fy6pmcfqa8mwld7zl7rvdcncmdxx30wn0")))

(define-public crate-vulk-0.1.8 (c (n "vulk") (v "0.1.8") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1aqf9ykyhkhivc3mqs2v5wm10wqg8v287a1gd2s8b2218qsj8mab")))

(define-public crate-vulk-0.1.9 (c (n "vulk") (v "0.1.9") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0glzx18ambnqx6hg3x48dpfnfhcfcfs95c2gnxinnqdfiaip3r0x")))

(define-public crate-vulk-0.1.10 (c (n "vulk") (v "0.1.10") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1pp3ndf3mcwv3477alx4l687qb93qxr5l3fwa17g3j08r9pcyxim")))

(define-public crate-vulk-0.1.11 (c (n "vulk") (v "0.1.11") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1cq04y6mkrhm9lchlb6fqgk5a91vg0n5a6xzcldbpdpdc8lbrjz1")))

(define-public crate-vulk-0.1.12 (c (n "vulk") (v "0.1.12") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0h05wi4534i7zckkg9zfj9hw33zq4xpjc64c9hpmcp9v9fbyj9k7")))

(define-public crate-vulk-0.1.13 (c (n "vulk") (v "0.1.13") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "13ap1i8khaj7hzy47klbriwhnzmqi91jd7admnd4g3ligfwiwd1v")))

(define-public crate-vulk-0.1.14 (c (n "vulk") (v "0.1.14") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1zh3zlk32gwsfldx5rizb5pn5f5zy6s78ha59bvxv999hnbfmpzh")))

(define-public crate-vulk-0.1.15 (c (n "vulk") (v "0.1.15") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1mz4mpfgq2hkgg9d51ai674fwssja2vcgxyzwwdx7ka355vwsmq5")))

(define-public crate-vulk-0.1.16 (c (n "vulk") (v "0.1.16") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0m8aylml0pmlia8cgyg317ixk8xqgl5qvzp34lxjyldzhk59v0fv")))

(define-public crate-vulk-0.1.17 (c (n "vulk") (v "0.1.17") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0jd6ddjgyaqcc6aar9zqh9w03w1kfyzcf4hvll4fzxnygc2jhwc6")))

