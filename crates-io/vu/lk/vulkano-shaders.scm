(define-module (crates-io vu lk vulkano-shaders) #:use-module (crates-io))

(define-public crate-vulkano-shaders-0.1.0 (c (n "vulkano-shaders") (v "0.1.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.0") (d #t) (k 0)))) (h "14gd04a72rwwb6csyjpyqgzk14klyz9z8ba2agch4nj52yyr841h")))

(define-public crate-vulkano-shaders-0.2.0 (c (n "vulkano-shaders") (v "0.2.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.0") (d #t) (k 0)))) (h "14vqa9lz9n462c31n4cfgv26yr02s57kbq4jpg4197drjyqy86ag")))

(define-public crate-vulkano-shaders-0.2.1 (c (n "vulkano-shaders") (v "0.2.1") (d (list (d (n "glsl-to-spirv") (r "^0.1.0") (d #t) (k 0)))) (h "1hrs63lds2g87q5gxvjlkppngrb8pdlxlw0pq829w2wbg35j5qpy")))

(define-public crate-vulkano-shaders-0.2.2 (c (n "vulkano-shaders") (v "0.2.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.0") (d #t) (k 0)))) (h "112yadkq81zdrara7qh67da2aarisypimy9fbg0445ax045b3mrj") (y #t)))

(define-public crate-vulkano-shaders-0.3.0 (c (n "vulkano-shaders") (v "0.3.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.0") (d #t) (k 0)))) (h "1sqnfcglzs82958yp7zcxjvcl8l9lngx20cwa0589qk9q0raqnr6")))

(define-public crate-vulkano-shaders-0.3.1 (c (n "vulkano-shaders") (v "0.3.1") (d (list (d (n "glsl-to-spirv") (r "^0.1.0") (d #t) (k 0)))) (h "1krk3axz26gjcccqn5sq2j6dbyp6q9qnrd35bjnrj48shnr7fd01")))

(define-public crate-vulkano-shaders-0.3.2 (c (n "vulkano-shaders") (v "0.3.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "1b78s4pbbdg4kjrj8b29dnvw2hkl2lkgvrrbhwrlrxn79wx753h7")))

(define-public crate-vulkano-shaders-0.4.0 (c (n "vulkano-shaders") (v "0.4.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "0ply82jh314rx0pzzacd26gs8sf7mw4aycm2mqp0h4ckxqjk1338")))

(define-public crate-vulkano-shaders-0.4.1 (c (n "vulkano-shaders") (v "0.4.1") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "056m17w5gpvs97jisx0a661n0kq3bg8x8w8ynfri38r3awd916q9")))

(define-public crate-vulkano-shaders-0.4.2 (c (n "vulkano-shaders") (v "0.4.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "1pc23rpmh3k7w1593gjrv8a7vjxbhdci56vv8d30z50ypwgc9069")))

(define-public crate-vulkano-shaders-0.4.3 (c (n "vulkano-shaders") (v "0.4.3") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "0pkd7aczn5dcx9kxb2hg10rmzld20l09cpvi75b2pn48y8fx081c")))

(define-public crate-vulkano-shaders-0.4.4 (c (n "vulkano-shaders") (v "0.4.4") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "03xzyivwb7gzy9bcs86zns25l08wzzd14zijbj7dx7l55a14gdqm")))

(define-public crate-vulkano-shaders-0.5.0 (c (n "vulkano-shaders") (v "0.5.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "0ryipnnc5gjzmfdixcxc8rnsh0z7yikd61qv3s6kizgyp3knja3d")))

(define-public crate-vulkano-shaders-0.5.1 (c (n "vulkano-shaders") (v "0.5.1") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "1fjn56c8lhsj5r3528h2yhmrzkvzwmns1371aa7cdzrfa6hvsq11")))

(define-public crate-vulkano-shaders-0.5.2 (c (n "vulkano-shaders") (v "0.5.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "0ir4ygx32k838d8r2dzicdaivgqrqxz9g5j73wb3q98ma7pfpisp")))

(define-public crate-vulkano-shaders-0.5.3 (c (n "vulkano-shaders") (v "0.5.3") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "0y8x353p2w25xhi15692944lh99x9rjszrghd5igldq470amxpqq")))

(define-public crate-vulkano-shaders-0.5.4 (c (n "vulkano-shaders") (v "0.5.4") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "10kqqph6yi7n5wdih2m0746p2ah339npxsxrw36wllnls7zn3ils")))

(define-public crate-vulkano-shaders-0.5.5 (c (n "vulkano-shaders") (v "0.5.5") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "0466zg1d96ryvyx5vymbsyl8f9k8rbrf0iqbb7y6v1d9qy87f8bn")))

(define-public crate-vulkano-shaders-0.5.6 (c (n "vulkano-shaders") (v "0.5.6") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "0d9p5yinmffisd0dbcmz1wcg6wz01y4bqndffrmnain4hlc7dikr")))

(define-public crate-vulkano-shaders-0.6.0 (c (n "vulkano-shaders") (v "0.6.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "075rc2pff8xqdjfvfs0z17z6cf25hr1fg7rfa5mv7z4wai392sns")))

(define-public crate-vulkano-shaders-0.6.1 (c (n "vulkano-shaders") (v "0.6.1") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "0vf7g723ryk4i29ny672y8629f57rrmf5i3x3ffj10m4008rflrm")))

(define-public crate-vulkano-shaders-0.6.2 (c (n "vulkano-shaders") (v "0.6.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "1l7qcblw0skr8nb282y2n2fr9nfnzxv3aqxiy7y6a444c81w3qxv")))

(define-public crate-vulkano-shaders-0.7.0 (c (n "vulkano-shaders") (v "0.7.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "0hw4fl8ksc0shkprv82l04lc43c6jsr6rskkywq4adgz8kfw90jw")))

(define-public crate-vulkano-shaders-0.7.1 (c (n "vulkano-shaders") (v "0.7.1") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "1vsnqr1d19dphr0nc4f01wji792j33ia7s7aai8rzdis68pm3a6m")))

(define-public crate-vulkano-shaders-0.7.2 (c (n "vulkano-shaders") (v "0.7.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "1l7mv4y78d08kr3ks68s4h729dys1wzxnzrl20aia298vyxdqi2b")))

(define-public crate-vulkano-shaders-0.7.3 (c (n "vulkano-shaders") (v "0.7.3") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "08icf6apa58r07qiqav8j1103dlqqi1i3mpv2b4f5k7cakdqjvny") (y #t)))

(define-public crate-vulkano-shaders-0.8.0 (c (n "vulkano-shaders") (v "0.8.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "00agkb1cvysh2qbb6ri7yfyxjwm59giij34fc9wkd22hs63lfmpi")))

(define-public crate-vulkano-shaders-0.9.0 (c (n "vulkano-shaders") (v "0.9.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)))) (h "0j5al7affq29y7046alx9bplc8hrmq1hssh74cv1rp06z9mla7sj")))

(define-public crate-vulkano-shaders-0.10.0 (c (n "vulkano-shaders") (v "0.10.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.6") (d #t) (k 0)))) (h "1hj17vc8rrs60y48a92rzakjph9pf29p8n0ykaddncxppprpl4wz")))

(define-public crate-vulkano-shaders-0.11.0 (c (n "vulkano-shaders") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "vulkano") (r "^0.11") (d #t) (k 2)))) (h "09gkgwjc6x8nqjxxmvihhdm2ic4r6m6k2d8vc7sn46f3gqn37slk") (y #t)))

(define-public crate-vulkano-shaders-0.11.1 (c (n "vulkano-shaders") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "vulkano") (r "^0.11") (d #t) (k 2)))) (h "02h3y1ginxvz11zl9vhwiw9pz5091bj0kbyi52xcbm38ss6rsa6f")))

(define-public crate-vulkano-shaders-0.12.0 (c (n "vulkano-shaders") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "vulkano") (r "^0.12") (d #t) (k 2)))) (h "0q39h4r0sgwxiwnhxx4r1hsk7czv6rpd7rv127a74301vnvn3f54")))

(define-public crate-vulkano-shaders-0.13.0 (c (n "vulkano-shaders") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "vulkano") (r "^0.13") (d #t) (k 2)))) (h "0hh7691cmhvjqdf892psi0h50mf2qqyxf78dqifjdwinb83rhq0i")))

(define-public crate-vulkano-shaders-0.14.0 (c (n "vulkano-shaders") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "vulkano") (r "^0.14") (d #t) (k 2)))) (h "0lyjmfhmj7qpsnh9z6wlzy1swg65mp0nn5c19h64qxl131rn3v18") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.16.0 (c (n "vulkano-shaders") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.16") (d #t) (k 2)))) (h "062n88hzcy27jijr02i7g5mrpfsz0bqixibqln6lqzy9s71y1v31") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.17.0 (c (n "vulkano-shaders") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.17") (d #t) (k 2)))) (h "1bjp8cmkkpirjca29z3nl0p1pz5yjdixjg5ymb4k1fvhp9k3kqa4") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.18.0 (c (n "vulkano-shaders") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.18") (d #t) (k 2)))) (h "0a4i5a6wp9pw3y1if6xmap4s6jnl3qq822kv3mv0j85nl6ki98sc") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.19.0 (c (n "vulkano-shaders") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.19.0") (d #t) (k 2)))) (h "0xvlkw14pbl14sndcjpf28sbbcl8pbkr8n47i0p9c7hwkfp6w3ls") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.20.0 (c (n "vulkano-shaders") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.20.0") (d #t) (k 2)))) (h "1gyz3l58yndy7718kfvs6l3k519bcjgfrssvf8ligwbvhxldglb7") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.21.0 (c (n "vulkano-shaders") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.21.0") (d #t) (k 2)))) (h "1wa876vl5iqkyprh2pc678hyqf98amz5rqs7ddcz607hkh7vg7p9") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.22.0 (c (n "vulkano-shaders") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.22.0") (d #t) (k 2)))) (h "1d48clzhzkjksyi5k3kgmfsnkm34lf2lxpkkhisyj540r9a0i03n") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.23.0 (c (n "vulkano-shaders") (v "0.23.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.23.0") (d #t) (k 2)))) (h "0d43n24zk1m24i66x8jlfxsxa2bdiyj0mbjfr9fs6r47qnpabag0") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.24.0 (c (n "vulkano-shaders") (v "0.24.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.24.0") (d #t) (k 2)))) (h "1aa9jblr6vzsxw3bkc5vl6pz2kkfjflki8qlvd1m4vcsxkbxqf0v") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.25.0 (c (n "vulkano-shaders") (v "0.25.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "spirv_headers") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.25.0") (d #t) (k 2)))) (h "0zm6n5v4xgzcbb6rj3bqqyg4xcfrcgqq739mbb4zvq2a29786ibz") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.26.0 (c (n "vulkano-shaders") (v "0.26.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.26.0") (d #t) (k 0)))) (h "0im6fw8lrqr66w65xh1mj3xrq989sb9af2g7xbg5mqagzscb35z0") (f (quote (("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.27.0 (c (n "vulkano-shaders") (v "0.27.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.27.0") (d #t) (k 0)))) (h "1bm7p462a19l6gca5xkajn89qcdnrcy5fyi39lrl42sr3kg0l1wc") (f (quote (("shaderc-debug") ("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.27.1 (c (n "vulkano-shaders") (v "0.27.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.27.1") (d #t) (k 0)))) (h "0x299j5yrg1xp25l6mg70i0fck75pva91kpv0zsf6np77hyx85pl") (f (quote (("shaderc-debug") ("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.28.0 (c (n "vulkano-shaders") (v "0.28.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.28.0") (d #t) (k 0)))) (h "0arc7qjjxzrzjsy9bq2812rln010mgj31zs91qlhf98g0l02b2aw") (f (quote (("shaderc-debug") ("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.29.0 (c (n "vulkano-shaders") (v "0.29.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.29.0") (d #t) (k 0)))) (h "0xx9s7170bn79z2lwgmfw1rq2yjr6gpli02d3cklm2cnwxx16qm4") (f (quote (("shaderc-debug") ("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.30.0 (c (n "vulkano-shaders") (v "0.30.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.30.0") (d #t) (k 0)))) (h "04ss7wgq6zjsv143c10841wbwzg5d19g8c6dsn4a2dws0lfdh11i") (f (quote (("shaderc-debug") ("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.31.0 (c (n "vulkano-shaders") (v "0.31.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.31.0") (d #t) (k 0)))) (h "1d5m7453n2zlckb273gg02i1scn4i240bvfqxqyyrvh9l71h9xxv") (f (quote (("shaderc-debug") ("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.31.1 (c (n "vulkano-shaders") (v "0.31.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.31.1") (d #t) (k 0)))) (h "0dh5a0aqfirb2azya2z4g7dchl8xfbvwgrlfkfb5y2smkrvanz6k") (f (quote (("shaderc-debug") ("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.32.0 (c (n "vulkano-shaders") (v "0.32.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.32.0") (d #t) (k 0)))) (h "1l2yq627hvf22amkpwn9jcv4g6k57yxq7a78zmhjdqr19dwf1fwn") (f (quote (("shaderc-debug") ("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.33.0 (c (n "vulkano-shaders") (v "0.33.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.33.0") (k 0)))) (h "1rd6jykahd6piisbagjmi4wwjilmysy2d7n3y4wnvg7ckf7g332z") (f (quote (("shaderc-debug") ("shaderc-build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vulkano-shaders-0.34.0 (c (n "vulkano-shaders") (v "0.34.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.34.0") (k 0)))) (h "1vpl093bmdlfb8rfw30l6mif9n474p8svscnzd5dfrbm540k9xni") (f (quote (("shaderc-debug") ("shaderc-build-from-source" "shaderc/build-from-source"))))))

