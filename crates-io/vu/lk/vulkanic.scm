(define-module (crates-io vu lk vulkanic) #:use-module (crates-io))

(define-public crate-vulkanic-0.1.0 (c (n "vulkanic") (v "0.1.0") (d (list (d (n "vk-sys") (r "^0.5.3") (d #t) (k 0)))) (h "1sbhk2m5nzm7czzpr2qvmsga7pyx7qsv49hi0rpgq5ahcjbmcksm")))

(define-public crate-vulkanic-0.1.1 (c (n "vulkanic") (v "0.1.1") (d (list (d (n "vk-sys") (r "^0.5.3") (d #t) (k 0)))) (h "02iap378mj779py1gl9d7wn7jdn6vbw96fi56pfj767yqgrnr7nx")))

(define-public crate-vulkanic-0.1.2 (c (n "vulkanic") (v "0.1.2") (d (list (d (n "vk-sys") (r "^0.5.3") (d #t) (k 0)))) (h "0z8ydj89d3mb41i5x7c94xwzb7q9b8y6apcy2646f4j3l29px9ia")))

(define-public crate-vulkanic-0.1.3 (c (n "vulkanic") (v "0.1.3") (d (list (d (n "vk-sys") (r "^0.5.3") (d #t) (k 0)))) (h "14a1329sdw7ds80i29vnpmc37i34k6ndx434s06ack4fn4cwfami")))

