(define-module (crates-io vu lk vulkano_blas) #:use-module (crates-io))

(define-public crate-vulkano_blas-0.1.0 (c (n "vulkano_blas") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.19.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.19.0") (d #t) (k 0)))) (h "16iqzi8j9mmza6bx7a2ln0piapg9xrll91cg0apfxmy8n3g9wjxj") (y #t)))

(define-public crate-vulkano_blas-0.1.1 (c (n "vulkano_blas") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.19.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.19.0") (d #t) (k 0)))) (h "0l22vr4aczvj6xw8b27k99mpzzpjqm6b6cbq6mjg9si6hih618xj") (y #t)))

