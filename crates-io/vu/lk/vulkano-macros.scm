(define-module (crates-io vu lk vulkano-macros) #:use-module (crates-io))

(define-public crate-vulkano-macros-0.33.0 (c (n "vulkano-macros") (v "0.33.0") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "160vfzfby6dgmls1mhasa5ca6w09va926bsms3950xhymhn8lnw9")))

(define-public crate-vulkano-macros-0.34.0 (c (n "vulkano-macros") (v "0.34.0") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r9rgvp2qh9mafw6xf3wd3kndbj7hn7gdrwqw9vzswj26qnn5gjj")))

