(define-module (crates-io vu lk vulkano_text) #:use-module (crates-io))

(define-public crate-vulkano_text-0.1.0 (c (n "vulkano_text") (v "0.1.0") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.3") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.3") (d #t) (k 1)) (d (n "vulkano-win") (r "^0.3") (d #t) (k 2)) (d (n "winit") (r "^0.5") (d #t) (k 2)))) (h "14p38d73wl85hq5i923nv1a6jjvf4j1ih3yiplgkgghigjqb29n3")))

(define-public crate-vulkano_text-0.1.1 (c (n "vulkano_text") (v "0.1.1") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.3") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.3") (d #t) (k 1)) (d (n "vulkano-win") (r "^0.3") (d #t) (k 2)) (d (n "winit") (r "^0.5") (d #t) (k 2)))) (h "1h8bgwyx5qnf41hq9phhrx2az2lg1vc8i0qxv7fcgb6klvsvqm7h")))

(define-public crate-vulkano_text-0.2.0 (c (n "vulkano_text") (v "0.2.0") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.4") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.4") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.4") (d #t) (k 2)) (d (n "winit") (r "^0.6") (d #t) (k 2)))) (h "0p24l0iipjbbvxxjdkrv71qnnlslbwr3hnz5v4xn8i52grwnzscp")))

(define-public crate-vulkano_text-0.2.1 (c (n "vulkano_text") (v "0.2.1") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.4") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.4") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.4") (d #t) (k 2)) (d (n "winit") (r "^0.6") (d #t) (k 2)))) (h "1nm900lxlc13ykfni2wj2aiphk3r7b13r5hp9x86cg88xplf6vxm")))

(define-public crate-vulkano_text-0.3.0 (c (n "vulkano_text") (v "0.3.0") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.5") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.5") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.5") (d #t) (k 2)) (d (n "winit") (r "^0.7") (d #t) (k 2)))) (h "1d0gml8gxf3bxj00wkbjimx2ri7s8ng9jnf1n2ygnl0vbc7cmzdn")))

(define-public crate-vulkano_text-0.3.1 (c (n "vulkano_text") (v "0.3.1") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.5") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.5") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.5") (d #t) (k 2)) (d (n "winit") (r "^0.7") (d #t) (k 2)))) (h "1fbgzvx1fncbgsjwz8781zxhgmarqyzpm3sp21xmxm1akdmka4z4")))

(define-public crate-vulkano_text-0.4.0 (c (n "vulkano_text") (v "0.4.0") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.6") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.6") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.6") (d #t) (k 2)) (d (n "winit") (r "^0.7") (d #t) (k 2)))) (h "0rmnisqrv1siy747mk89dk5r12j12m0c4ncfdbi1rvsyng5hpgnm")))

(define-public crate-vulkano_text-0.4.1 (c (n "vulkano_text") (v "0.4.1") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.6") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.6") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.6") (d #t) (k 2)) (d (n "winit") (r "^0.7") (d #t) (k 2)))) (h "13hm2wvkma148w2d434r4q340gig3bf662sv3z21fnf978cd37sy")))

(define-public crate-vulkano_text-0.5.0 (c (n "vulkano_text") (v "0.5.0") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.7") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.7") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.7") (d #t) (k 2)) (d (n "winit") (r "^0.7") (d #t) (k 2)))) (h "057356vmknl5hj7w7hd7n3zksbndlri0f3l3l844f08n9yy402m5")))

(define-public crate-vulkano_text-0.6.0 (c (n "vulkano_text") (v "0.6.0") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.7") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.7") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.7") (d #t) (k 2)) (d (n "winit") (r "^0.7") (d #t) (k 2)))) (h "1vj5bklzxxjdays3zjyhi7z8jhvw9b2czj7yk6hvd18zn9l02nkd")))

(define-public crate-vulkano_text-0.6.1 (c (n "vulkano_text") (v "0.6.1") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.7") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.7") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.7") (d #t) (k 2)) (d (n "winit") (r "^0.7") (d #t) (k 2)))) (h "0bm2sc6a5xfg579vwrj3nknzabzhis6d7r2mzipnhlc5l6g56d5q")))

(define-public crate-vulkano_text-0.7.0 (c (n "vulkano_text") (v "0.7.0") (d (list (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.9") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.9") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.9") (d #t) (k 2)) (d (n "winit") (r "^0.11") (d #t) (k 2)))) (h "0jhrdiclbv46m1wprvc31xyqcyjyr5y8zb122sibv004j4p62jxp")))

(define-public crate-vulkano_text-0.8.0 (c (n "vulkano_text") (v "0.8.0") (d (list (d (n "rusttype") (r "^0.6") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.10") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.10") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.10") (d #t) (k 2)) (d (n "winit") (r "^0.17") (d #t) (k 2)))) (h "1mp2a7ca1cq8xqr3f8nsp51ib11l0dnyxdvljvk1rmjvgkcj4rmi")))

(define-public crate-vulkano_text-0.8.1 (c (n "vulkano_text") (v "0.8.1") (d (list (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.10") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.10") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.10") (d #t) (k 2)) (d (n "winit") (r "^0.17") (d #t) (k 2)))) (h "1fizgh0kdqapzrxl5jn8nw6aj1wl4y8kxw2hbajl6ih18h8qjhld")))

(define-public crate-vulkano_text-0.9.0 (c (n "vulkano_text") (v "0.9.0") (d (list (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.11") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.11") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.11") (d #t) (k 2)) (d (n "winit") (r "^0.18") (d #t) (k 2)))) (h "0gqsgvv10jx7m35qjsfpp28br5zfwgcg7xhsf75g916687xa9apz")))

(define-public crate-vulkano_text-0.10.0 (c (n "vulkano_text") (v "0.10.0") (d (list (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.12") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.12") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.12") (d #t) (k 2)) (d (n "winit") (r "^0.19") (d #t) (k 2)))) (h "0liy762sc9zglhfy12332rwza52c06nyq25an403qc8df816q0pl")))

(define-public crate-vulkano_text-0.11.0 (c (n "vulkano_text") (v "0.11.0") (d (list (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.13") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.13") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.13") (d #t) (k 2)) (d (n "winit") (r "^0.19") (d #t) (k 2)))) (h "07ar4g2gzhxznx46z90ibkbdghn59z5wdr6j421cxnxd0r9qfqmz")))

(define-public crate-vulkano_text-0.12.0 (c (n "vulkano_text") (v "0.12.0") (d (list (d (n "rusttype") (r "^0.8") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.17") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.17") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.17") (d #t) (k 2)) (d (n "winit") (r "^0.21") (d #t) (k 2)))) (h "12f0llpbv2knppfp7ni7q2cljyad3hwialyhp3brcmsg2sdr2sw0")))

(define-public crate-vulkano_text-0.13.0 (c (n "vulkano_text") (v "0.13.0") (d (list (d (n "rusttype") (r "^0.8") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.20") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.20") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.20") (d #t) (k 2)) (d (n "winit") (r "^0.24") (d #t) (k 2)))) (h "1hz4ij2bhqzp71sdpnpjw3k2d93zqrhbq2nidziabc0qjgma9swd")))

(define-public crate-vulkano_text-0.14.0 (c (n "vulkano_text") (v "0.14.0") (d (list (d (n "rusttype") (r "^0.8") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.23") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.23") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.23") (d #t) (k 2)) (d (n "winit") (r "^0.24") (d #t) (k 2)))) (h "18qapv8r70wih0s7xhqpnbfjlgzi3bbf4gz9wk74cp54v7bhvgiy")))

(define-public crate-vulkano_text-0.15.0 (c (n "vulkano_text") (v "0.15.0") (d (list (d (n "rusttype") (r "^0.8") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.24.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.24.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.24.0") (d #t) (k 2)) (d (n "winit") (r "^0.25.0") (d #t) (k 2)))) (h "18zs5sqahrac2ibwdy6d7n0fnqvmm0ll86vnib4s9byccvnwa9n5")))

