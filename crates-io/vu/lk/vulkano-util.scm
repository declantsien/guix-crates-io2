(define-module (crates-io vu lk vulkano-util) #:use-module (crates-io))

(define-public crate-vulkano-util-0.30.0 (c (n "vulkano-util") (v "0.30.0") (d (list (d (n "vulkano") (r "^0.30.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.30.0") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)))) (h "0bp6iymizhsakyd3fyzvydz5j3igmabdf3lg56aq4rgv1c44hspg")))

(define-public crate-vulkano-util-0.31.0 (c (n "vulkano-util") (v "0.31.0") (d (list (d (n "vulkano") (r "^0.31.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.31.0") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "1xijfdvsshxqlfpa1vdf48a8nliwm92hv7xmx2fc9v7v14rwk4bq")))

(define-public crate-vulkano-util-0.32.0 (c (n "vulkano-util") (v "0.32.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "vulkano") (r "^0.32.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.32.0") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "0n1glrcnd51nylrcdsq4rac84vanj5nkj19ssq8al8nn6yzk8zfz")))

(define-public crate-vulkano-util-0.33.0 (c (n "vulkano-util") (v "0.33.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "vulkano") (r "^0.33.0") (k 0)) (d (n "vulkano-win") (r "^0.33.0") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "1x7m6wfjkplg6p479gwl6jfr9hiphj8mlr7cpb0n249rbbq6s6x7")))

(define-public crate-vulkano-util-0.34.0 (c (n "vulkano-util") (v "0.34.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "vulkano") (r "^0.34.0") (k 0)) (d (n "vulkano-win") (r "^0.34.0") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "15ks2ns67rw234vgbn9pya40r6p1r5r740clysxn52qpxd847i60")))

(define-public crate-vulkano-util-0.34.1 (c (n "vulkano-util") (v "0.34.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "vulkano") (r "^0.34.0") (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "19v9csgghgsk20hmgn8wjq968k0qv80pr8bpzb4650ss0nj9c7zz")))

