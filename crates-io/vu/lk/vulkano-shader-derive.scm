(define-module (crates-io vu lk vulkano-shader-derive) #:use-module (crates-io))

(define-public crate-vulkano-shader-derive-0.3.2 (c (n "vulkano-shader-derive") (v "0.3.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.3") (d #t) (k 0)))) (h "1nd3bkppkiwcnd5k94ynmlfxafr31v9jfm6wi7vkwwwzf5m1yhlf")))

(define-public crate-vulkano-shader-derive-0.4.0 (c (n "vulkano-shader-derive") (v "0.4.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.4") (d #t) (k 0)))) (h "11gba729irsp11dq9wpkcz25kj0z10y47jm7bfg8xwgm6lb2wq01")))

(define-public crate-vulkano-shader-derive-0.4.1 (c (n "vulkano-shader-derive") (v "0.4.1") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.4") (d #t) (k 0)))) (h "1mfjhlv8x4azjz56k5b7w38wjskkvnm1n9dyn2cs40w65hs0ircg")))

(define-public crate-vulkano-shader-derive-0.4.2 (c (n "vulkano-shader-derive") (v "0.4.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.4") (d #t) (k 0)))) (h "0c31zwbkmggq656gx50iic77ymr087qsm14zxdjzc2y631lhql6b")))

(define-public crate-vulkano-shader-derive-0.4.3 (c (n "vulkano-shader-derive") (v "0.4.3") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.4") (d #t) (k 0)))) (h "0av67s5ar4vd5ncggbrkas2jcpg44wrlah568c81fd7zkm5hlk67")))

(define-public crate-vulkano-shader-derive-0.4.4 (c (n "vulkano-shader-derive") (v "0.4.4") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.4") (d #t) (k 0)))) (h "0zxc5l9zbhkgnbh19mdqa0x20cgbdgswrimm64jiv5vgd4phv3yp")))

(define-public crate-vulkano-shader-derive-0.5.0 (c (n "vulkano-shader-derive") (v "0.5.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.5") (d #t) (k 0)))) (h "048wdkzh8q42f39rfl71c6k0cz2zfc9dfgxxvag0d6lp3z9q1aj2")))

(define-public crate-vulkano-shader-derive-0.5.1 (c (n "vulkano-shader-derive") (v "0.5.1") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.5") (d #t) (k 0)))) (h "0r55ngdjanwhdqm5h7nlq5n28n1svkdb1b55rvsph1hhcdc6ddl8")))

(define-public crate-vulkano-shader-derive-0.5.2 (c (n "vulkano-shader-derive") (v "0.5.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.5") (d #t) (k 0)))) (h "091597sb31q2jqalpx0kn8dwyn103ya02fary68f7prb2h6g31s9")))

(define-public crate-vulkano-shader-derive-0.5.3 (c (n "vulkano-shader-derive") (v "0.5.3") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.5") (d #t) (k 0)))) (h "0cvj6ald096kd5l0d6kg717zn2xvrvh7p03xk6jm7qshwgiagyzm")))

(define-public crate-vulkano-shader-derive-0.5.4 (c (n "vulkano-shader-derive") (v "0.5.4") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.5") (d #t) (k 0)))) (h "0l6wbpnqxv3s4a1jsf651c14zypk0n913ryizix9l03m74fhsygf")))

(define-public crate-vulkano-shader-derive-0.5.5 (c (n "vulkano-shader-derive") (v "0.5.5") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.5") (d #t) (k 0)))) (h "1rvbnaz0mc2v1yz1i74s0r3ig4s6mrjg7i9lyqwlwafgcs524bw3")))

(define-public crate-vulkano-shader-derive-0.5.6 (c (n "vulkano-shader-derive") (v "0.5.6") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.5") (d #t) (k 0)))) (h "0b33igpxa1s1a9fdxqh21qv0p9nr0wac0xl400f53qcqjl7bx6g8")))

(define-public crate-vulkano-shader-derive-0.6.0 (c (n "vulkano-shader-derive") (v "0.6.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.6") (d #t) (k 0)))) (h "15p4fgpgf9gral8nx89flavn5bf2izrl65cq0ffvivhpb2k6pfpj")))

(define-public crate-vulkano-shader-derive-0.6.1 (c (n "vulkano-shader-derive") (v "0.6.1") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.6") (d #t) (k 0)))) (h "1xsmblhxqpwxzzy34159il1wls7p3yr3l1xpqgxjabv1z4sia1wy")))

(define-public crate-vulkano-shader-derive-0.6.2 (c (n "vulkano-shader-derive") (v "0.6.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.6") (d #t) (k 0)))) (h "0slyws1f7cyh326gnsl1c560914iaa9jp2rqsyzxgz00d61j07j4")))

(define-public crate-vulkano-shader-derive-0.7.0 (c (n "vulkano-shader-derive") (v "0.7.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.7") (d #t) (k 0)))) (h "0sl6rrrcqfbqry4rkpafmgywxi9z7ql25z4qgv0x19r9aaiiashq")))

(define-public crate-vulkano-shader-derive-0.7.1 (c (n "vulkano-shader-derive") (v "0.7.1") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.7") (d #t) (k 0)))) (h "1avzmr0j6jda69aqwmhirpbxw8h588rgbmy9n0mbdk7ca8pmibjl")))

(define-public crate-vulkano-shader-derive-0.7.2 (c (n "vulkano-shader-derive") (v "0.7.2") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.10") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.7") (d #t) (k 0)))) (h "1h835x8lr3jclqqhx59nwg6fhhkbb1awbmix8hfjwmrd2aahy2vc")))

(define-public crate-vulkano-shader-derive-0.7.3 (c (n "vulkano-shader-derive") (v "0.7.3") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.7.2") (d #t) (k 2)) (d (n "vulkano-shaders") (r "^0.7") (d #t) (k 0)))) (h "1p6zllisjq1sg55ay9l4pgsn3hwzw834knz545yf0j1y66l4jjci") (y #t)))

(define-public crate-vulkano-shader-derive-0.8.0 (c (n "vulkano-shader-derive") (v "0.8.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.8") (d #t) (k 2)) (d (n "vulkano-shaders") (r "^0.8") (d #t) (k 0)))) (h "10gnd93302n45sx9h2imv4ycl7bh0kgagjpzql34fp73njrwfhp8")))

(define-public crate-vulkano-shader-derive-0.9.0 (c (n "vulkano-shader-derive") (v "0.9.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (f (quote ("aster" "visit"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.9") (d #t) (k 2)) (d (n "vulkano-shaders") (r "^0.9") (d #t) (k 0)))) (h "0a26c63292kbw4b72w83nrqbs4d8viqqshggryi0dd3dhcdcshjs")))

(define-public crate-vulkano-shader-derive-0.10.0 (c (n "vulkano-shader-derive") (v "0.10.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "vulkano") (r "^0.10") (d #t) (k 2)) (d (n "vulkano-shaders") (r "^0.10") (d #t) (k 0)))) (h "0p0ywlqriv5bwk009345npkdq16xpfs5hjvda36w7vbxsambviwj")))

(define-public crate-vulkano-shader-derive-0.11.0 (c (n "vulkano-shader-derive") (v "0.11.0") (h "12ms7xf85vk15mkbqqx46wyg23v7gpbamz8ygpjjvzbh83p5fmfc")))

