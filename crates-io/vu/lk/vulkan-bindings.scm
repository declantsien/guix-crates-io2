(define-module (crates-io vu lk vulkan-bindings) #:use-module (crates-io))

(define-public crate-vulkan-bindings-0.1.0 (c (n "vulkan-bindings") (v "0.1.0") (d (list (d (n "libloading") (r "^0.5.2") (d #t) (k 2)))) (h "05njfpw13065d1hmdg3v9xdc4bcrhisgk4vwa2z44hqq1ypd1blz")))

(define-public crate-vulkan-bindings-0.1.1 (c (n "vulkan-bindings") (v "0.1.1") (d (list (d (n "libloading") (r "^0.5.2") (d #t) (k 2)))) (h "1ajy67262dik6pwhgkhacdpl7lmlwx6bz79a2hzn54cmdb1dap06")))

(define-public crate-vulkan-bindings-0.2.148 (c (n "vulkan-bindings") (v "0.2.148") (d (list (d (n "libloading") (r "^0.6.2") (d #t) (k 2)))) (h "1rr5c70970yqrwyj8gcx6s266r81dbyc05rs8vs1dzd4h3w2ifch")))

(define-public crate-vulkan-bindings-0.2.152 (c (n "vulkan-bindings") (v "0.2.152") (d (list (d (n "libloading") (r "^0.6.3") (d #t) (k 2)))) (h "1pr1afrmxfh2z56mv5wv4d3qv97q9gd0samq2qarx2j1fd5m5wqf")))

(define-public crate-vulkan-bindings-0.2.165 (c (n "vulkan-bindings") (v "0.2.165") (d (list (d (n "libloading") (r "^0.6.6") (d #t) (k 2)))) (h "0422was6zw1mfczymx468dhnzik45hqhf6iv6ypz3jds5la2w8a5")))

(define-public crate-vulkan-bindings-0.2.169 (c (n "vulkan-bindings") (v "0.2.169") (d (list (d (n "libloading") (r "^0.6.7") (d #t) (k 2)))) (h "1vyhfnr91azdfzgxnf1hfcg0nbwqcpanqf3mma5ix4q0r8mgk8wg")))

(define-public crate-vulkan-bindings-0.2.176 (c (n "vulkan-bindings") (v "0.2.176") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 2)))) (h "13hgr6cvsdlkdpnn3q6fcwba7j8f896y6z2vv0h4va69yn1j3nzh")))

(define-public crate-vulkan-bindings-0.2.184 (c (n "vulkan-bindings") (v "0.2.184") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 2)))) (h "0z1fsghwy2rzaf88xs6j3drpbrrnd738xgq0373scs3r2n76ss3p")))

(define-public crate-vulkan-bindings-1.2.190 (c (n "vulkan-bindings") (v "1.2.190") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 2)))) (h "1bzihl3zv3vs51afg3yz2281gg8hh5zgsk9n0mlqzlribij90nn1")))

(define-public crate-vulkan-bindings-1.2.199 (c (n "vulkan-bindings") (v "1.2.199") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 2)))) (h "15pzwfzr7hs8bad2n7bj6s9i14bggbmqfi6kgl9p6v4znx7y2lyv")))

(define-public crate-vulkan-bindings-1.3.208 (c (n "vulkan-bindings") (v "1.3.208") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 2)))) (h "0lqydzb0idqg4wfbs6n63cf6414q4xbi937ambh1sx773ffy9564")))

(define-public crate-vulkan-bindings-1.3.211 (c (n "vulkan-bindings") (v "1.3.211") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 2)))) (h "0j189qrlxql3655as75r9955f1y2ig20qbh7nmcq1h9h2r69hnry")))

(define-public crate-vulkan-bindings-1.3.218 (c (n "vulkan-bindings") (v "1.3.218") (d (list (d (n "libloading") (r "^0.7.0") (d #t) (k 2)))) (h "1x2xx99q5qj5bkpcqbifqfvghvk09wnrzl73d0szxhww35z1kdqi")))

(define-public crate-vulkan-bindings-1.3.225 (c (n "vulkan-bindings") (v "1.3.225") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 2)))) (h "0scmn22398iibdipkbrn2j2h78qyx873n4hrva22a38jwksfs83l")))

(define-public crate-vulkan-bindings-1.3.231 (c (n "vulkan-bindings") (v "1.3.231") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 2)))) (h "0gnf89l4bg820hla14k4aay8918bjmhq53d785n7lp64f4rz41bi")))

(define-public crate-vulkan-bindings-1.3.238 (c (n "vulkan-bindings") (v "1.3.238") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 2)))) (h "0jishlvwyq017cmmq3i2223vr8abphg4axw1ajg9m7al38rcra3w")))

