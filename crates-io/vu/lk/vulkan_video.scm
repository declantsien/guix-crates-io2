(define-module (crates-io vu lk vulkan_video) #:use-module (crates-io))

(define-public crate-vulkan_video-0.1.0 (c (n "vulkan_video") (v "0.1.0") (d (list (d (n "ash") (r "^0.37.3") (d #t) (k 0)) (d (n "h264-reader") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1hq03yarawaf2shbknikx0imgndi9vgy6fcq2166fg2yp85mllbq")))

