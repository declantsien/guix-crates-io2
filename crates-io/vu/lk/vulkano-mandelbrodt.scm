(define-module (crates-io vu lk vulkano-mandelbrodt) #:use-module (crates-io))

(define-public crate-vulkano-mandelbrodt-0.1.0 (c (n "vulkano-mandelbrodt") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)) (d (n "vulkano") (r "^0.32") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.32") (d #t) (k 0)) (d (n "vulkano-util") (r "^0.32") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.32") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "1jqdzvl5jcxvn990wkwjg0qggaj4h22s5h4aybprf4knlhhdkwi6")))

