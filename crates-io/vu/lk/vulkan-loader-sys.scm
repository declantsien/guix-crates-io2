(define-module (crates-io vu lk vulkan-loader-sys) #:use-module (crates-io))

(define-public crate-vulkan-loader-sys-1.3.235 (c (n "vulkan-loader-sys") (v "1.3.235") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "16az3pfhqjjk0l87x57v0py26gqb0xnxyapcz2msfdpnz2ixy5d3")))

(define-public crate-vulkan-loader-sys-1.3.237 (c (n "vulkan-loader-sys") (v "1.3.237") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "128x1iidla4zv9fwyvg6r73mkc3313a7942hfdw2v663agz3jaa0")))

(define-public crate-vulkan-loader-sys-1.3.238 (c (n "vulkan-loader-sys") (v "1.3.238") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "glfw") (r "^0.48.0") (f (quote ("vulkan"))) (d #t) (k 2)))) (h "1zw3kklxcl507axa5pml9b0jxwrk5kjch5sgzpahni8asis1lpp7") (y #t)))

(define-public crate-vulkan-loader-sys-1.3.238+1 (c (n "vulkan-loader-sys") (v "1.3.238+1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "glfw") (r "^0.48.0") (f (quote ("vulkan"))) (d #t) (k 2)))) (h "1gkcswnmkqvkmbn6s355010y6rcj6flv8mkmklhiimg6xl5p287v")))

