(define-module (crates-io vu lk vulkan-malloc) #:use-module (crates-io))

(define-public crate-vulkan-malloc-0.1.0 (c (n "vulkan-malloc") (v "0.1.0") (d (list (d (n "array_ext") (r "^0.2") (d #t) (k 0)) (d (n "dacite") (r "^0.6") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "0pl55ka54la62v77vlhf8539kapzs9v960d0wr1h7mg8z13ydm0x")))

(define-public crate-vulkan-malloc-0.1.1 (c (n "vulkan-malloc") (v "0.1.1") (d (list (d (n "array_ext") (r "^0.2") (d #t) (k 0)) (d (n "dacite") (r "^0.6") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "0p19li3lj75fw8zi4fqsxspr9pidzkl6426rnxl0pn2kw8qrj9bm")))

(define-public crate-vulkan-malloc-0.1.2 (c (n "vulkan-malloc") (v "0.1.2") (d (list (d (n "array_ext") (r "^0.2") (d #t) (k 0)) (d (n "dacite") (r "^0.6") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "1l0j6pmzryckm89k0067ikibsv45asjkj0anxpc7m3j0alghlwdk")))

(define-public crate-vulkan-malloc-0.1.3 (c (n "vulkan-malloc") (v "0.1.3") (d (list (d (n "array_ext") (r "^0.2") (d #t) (k 0)) (d (n "dacite") (r "^0.6") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "00vs442gi89myppv42zpbksk1qyi09p1jd275ix33pak4lrzwfs5")))

(define-public crate-vulkan-malloc-0.1.4 (c (n "vulkan-malloc") (v "0.1.4") (d (list (d (n "array_ext") (r "^0.2") (d #t) (k 0)) (d (n "dacite") (r "^0.6") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "0f6cq89j4v4ii1whrl59wv6d6qfx4k7zv8p43xymrgzv13pyy89q")))

(define-public crate-vulkan-malloc-0.1.5 (c (n "vulkan-malloc") (v "0.1.5") (d (list (d (n "array_ext") (r "^0.2") (d #t) (k 0)) (d (n "dacite") (r "^0.7") (d #t) (k 0)) (d (n "option-filter") (r "^1.0") (d #t) (k 0)))) (h "0r4zhi9vwhiq7mxir5zlkzf9w672yanlqxxi6cmxz447p6xmr4vd")))

