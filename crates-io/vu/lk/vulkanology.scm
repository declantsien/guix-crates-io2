(define-module (crates-io vu lk vulkanology) #:use-module (crates-io))

(define-public crate-vulkanology-0.1.0 (c (n "vulkanology") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "vulkano") (r "^0.3.1") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.3.1") (d #t) (k 1)))) (h "03qcb1gwhx80nn69vjzfnw9b48w0511agr0mmy5vixcfngs4fjv2")))

(define-public crate-vulkanology-0.2.0 (c (n "vulkanology") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "vulkano") (r "^0.3.2") (d #t) (k 2)) (d (n "vulkano-shaders") (r "^0.3.2") (d #t) (k 1)))) (h "1z5cp0sm84qlbirbgng4a0f9pzzma6k5wj0sjapsgas7bdckqfj7")))

