(define-module (crates-io vu lk vulkano_maybe_molten) #:use-module (crates-io))

(define-public crate-vulkano_maybe_molten-0.30.0 (c (n "vulkano_maybe_molten") (v "0.30.0") (d (list (d (n "ash-molten") (r "^0.13.0") (f (quote ("pre-built"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "ash_molten_version") (r "^0.35.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0) (p "ash")) (d (n "ash_vulkano_version") (r "^0.37.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0) (p "ash")) (d (n "vulkano") (r "^0.30.0") (d #t) (k 0)))) (h "0iisvpzn5xabzxhfwwykrc3mcp4kmv214iq0m2rnbsdw6s2pxjkn") (y #t)))

(define-public crate-vulkano_maybe_molten-0.30.1 (c (n "vulkano_maybe_molten") (v "0.30.1") (d (list (d (n "ash-molten") (r "=0.13.0") (f (quote ("pre-built"))) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "ash_molten_version") (r "^0.35.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0) (p "ash")) (d (n "ash_vulkano_version") (r "^0.37.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0) (p "ash")) (d (n "vulkano") (r "^0.30.0") (d #t) (k 0)))) (h "1r6k73frz7jcs1g1ksjivhgm881mf8p01bl1i6llw96ig0h50m12")))

