(define-module (crates-io vu lk vulkaneum) #:use-module (crates-io))

(define-public crate-vulkaneum-0.1.0 (c (n "vulkaneum") (v "0.1.0") (h "1kadb7fklrfyzckh8wh449srfmrmfz6hs3zpyl2591n40b0xk33s")))

(define-public crate-vulkaneum-0.1.1 (c (n "vulkaneum") (v "0.1.1") (h "142k3355l1d4d5g2fs655jwzksvl6vmvklsy2c2nmhzavq088rnd")))

(define-public crate-vulkaneum-0.1.2 (c (n "vulkaneum") (v "0.1.2") (h "0vq3acdcs1wcw95l4m5jjhzxgjwb3kcc6z9kq32m7lydc5mdhnb4")))

(define-public crate-vulkaneum-0.1.3 (c (n "vulkaneum") (v "0.1.3") (h "1awkczb1svha08b5w6iax3pqjbz3c8a8y4mr7v4xvw1ac47dxr15")))

