(define-module (crates-io vu lp vulpo_rocket) #:use-module (crates-io))

(define-public crate-vulpo_rocket-0.1.0 (c (n "vulpo_rocket") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("uuid" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "vulpo") (r "^0.1.0") (d #t) (k 0)))) (h "0lmw71xmf683i5fh1nwixw8gc09g4qr2sbpqc5fsv6bwgfycabv6")))

