(define-module (crates-io vu e- vue-compiler-core) #:use-module (crates-io))

(define-public crate-vue-compiler-core-0.1.0 (c (n "vue-compiler-core") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "insta") (r "^1.8.0") (d #t) (k 2)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rslint_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)))) (h "19mhjyyr337rpdz1rmajf5nvf9bs2wby6xdbdr64zq0hcj8x6417") (f (quote (("default" "serde" "smallvec/serde"))))))

