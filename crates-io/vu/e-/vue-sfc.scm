(define-module (crates-io vu e- vue-sfc) #:use-module (crates-io))

(define-public crate-vue-sfc-0.1.0 (c (n "vue-sfc") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1wx0rbvs6yi3k390rnr2hb2jc8z0k1p0zszf62a7n7klqxdk8481") (y #t)))

(define-public crate-vue-sfc-0.2.0 (c (n "vue-sfc") (v "0.2.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1kqnry8g1kijqswshb6laxmajhdfmarrak7kyn76nixqvf5z2385")))

(define-public crate-vue-sfc-0.3.0 (c (n "vue-sfc") (v "0.3.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "07xgrxxvww6ah7qd2m4n13rg9akjl53pay7ra1hgraihgdzvngrz")))

(define-public crate-vue-sfc-0.3.1 (c (n "vue-sfc") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "pprof") (r "^0.6.2") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)))) (h "0b64xipl02mhpinm7dnxinhkxmm3qlk993vdr2gkjnv3kaxzrs0q")))

(define-public crate-vue-sfc-0.3.2 (c (n "vue-sfc") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "pprof") (r "^0.6.2") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)))) (h "0jkm5xv8dvlrw1rzwsrqa1gqnz7xj4d3mz2nvv0xd8brylk6z0n8")))

