(define-module (crates-io vu ot vuot) #:use-module (crates-io))

(define-public crate-vuot-0.0.1 (c (n "vuot") (v "0.0.1") (d (list (d (n "bumpalo") (r "^3") (f (quote ("allocator_api"))) (o #t) (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 2)) (d (n "smol") (r "^2") (d #t) (k 2)) (d (n "smol-macros") (r "^0.1") (d #t) (k 2)))) (h "1892km2gvznlq8smqvs9mawgajkvi7pga5g6vwkmk1c7bx6p6kgr") (s 2) (e (quote (("bumpalo" "dep:bumpalo"))))))

