(define-module (crates-io vu lt vultr-api) #:use-module (crates-io))

(define-public crate-vultr-api-0.1.0 (c (n "vultr-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1qxcyf4sfkls4ylijl94ag5gd8anww3kfv3fa86aywsxkcnyaij8") (f (quote (("client" "serde_json" "anyhow" "reqwest"))))))

