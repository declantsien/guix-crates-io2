(define-module (crates-io vu xt vuxt_macros) #:use-module (crates-io))

(define-public crate-vuxt_macros-0.1.0 (c (n "vuxt_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0b4cfiqs0nf8yr93sdvg4z2cq75103jrsiviz4cajqbm2zln2zwa") (y #t)))

(define-public crate-vuxt_macros-0.1.1 (c (n "vuxt_macros") (v "0.1.1") (h "102pw43fxysibwjcyrrqrpz6pf3sy4nspgli46m92nwwvmrqrm90")))

(define-public crate-vuxt_macros-0.1.2 (c (n "vuxt_macros") (v "0.1.2") (h "1rhawp511braqw2yi47mcyp1rpi6bx14s0pfdkc4vr7bzxq5gkya")))

