(define-module (crates-io jx ca jxcape) #:use-module (crates-io))

(define-public crate-jxcape-0.1.0 (c (n "jxcape") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0fjgnckk683m5aqn2xr0zkxl0spc0ca30nk220yynfa76pfrfs6p")))

(define-public crate-jxcape-0.2.0 (c (n "jxcape") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0vydb9c3fyikp74y80949vsrj098sf4i0ghiz5dq5h12gg3kccpv")))

