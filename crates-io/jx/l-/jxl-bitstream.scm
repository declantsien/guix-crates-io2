(define-module (crates-io jx l- jxl-bitstream) #:use-module (crates-io))

(define-public crate-jxl-bitstream-0.1.0 (c (n "jxl-bitstream") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "00n1nbyvaammz8v18s726zmsxx69dsnazaxl2nfhy5ay0yb5v6wn")))

(define-public crate-jxl-bitstream-0.1.1 (c (n "jxl-bitstream") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "1g1a1ikrhdv05d0pmg8253mp4kbia8svhrx1xll468fzv2kd0grm")))

(define-public crate-jxl-bitstream-0.1.2 (c (n "jxl-bitstream") (v "0.1.2") (d (list (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "1hdwdmj6yz2xc3bq36dcp1hd5428jgh5w0040dc86ljkqhlmhhri")))

(define-public crate-jxl-bitstream-0.1.3 (c (n "jxl-bitstream") (v "0.1.3") (d (list (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "148i7pzkgf52b72wf25fnqs10xi9i1nzwgn8nbcvamyyk535ag0h")))

(define-public crate-jxl-bitstream-0.2.0 (c (n "jxl-bitstream") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "1ad8cv8yhf1x3algqzzk1d0xas9i2d4gmblmjclj5w0k2603ivrv")))

(define-public crate-jxl-bitstream-0.2.1 (c (n "jxl-bitstream") (v "0.2.1") (d (list (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0n1pklpcmk59c3daz7pr8g6p2bnbycwmph6bln8w6572x6c9jxia")))

(define-public crate-jxl-bitstream-0.2.2 (c (n "jxl-bitstream") (v "0.2.2") (d (list (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "11jxw2j2zqq5njx6ij0nvhsp2vw1zk5pwmwi9ri5ac19dwznn74c")))

(define-public crate-jxl-bitstream-0.2.3 (c (n "jxl-bitstream") (v "0.2.3") (d (list (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "08k6dy6v5qwlaplc02vln2pzfvw8jbjndpllysrjjqng31fj1hvn")))

(define-public crate-jxl-bitstream-0.3.0 (c (n "jxl-bitstream") (v "0.3.0") (d (list (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "165322ld5jfgmyz2i7i24y8cbaaxv3x878jphf5jkvzv7n7rvbij")))

(define-public crate-jxl-bitstream-0.3.1 (c (n "jxl-bitstream") (v "0.3.1") (d (list (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0qip48sjgh250ns0y7c2333aqq3sblkckmbkzy0f64l0mv3d6f4z")))

(define-public crate-jxl-bitstream-0.3.2 (c (n "jxl-bitstream") (v "0.3.2") (d (list (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0m1jimkazq5mig10h460fp9xrwm9hi0sfb8kwcwwzxvx9qpybb13")))

(define-public crate-jxl-bitstream-0.3.3 (c (n "jxl-bitstream") (v "0.3.3") (d (list (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "1dpwanbd5ggp9z7g9si3ai9qi5cyiz5pwv851psjiysp8lkkca9b")))

(define-public crate-jxl-bitstream-0.4.0 (c (n "jxl-bitstream") (v "0.4.0") (d (list (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "1kw2dyblcb4lw721f6927lvs6p067qs7ngljw8786464zjrclgvh")))

