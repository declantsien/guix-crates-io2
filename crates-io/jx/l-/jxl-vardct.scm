(define-module (crates-io jx l- jxl-vardct) #:use-module (crates-io))

(define-public crate-jxl-vardct-0.1.0 (c (n "jxl-vardct") (v "0.1.0") (d (list (d (n "jxl-bitstream") (r "^0.1.3") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.1.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "jxl-modular") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "09grah00i42l8bvihbs01axmpj47lvcyl6052wiwvssjc4j1v771")))

(define-public crate-jxl-vardct-0.2.0 (c (n "jxl-vardct") (v "0.2.0") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "jxl-modular") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "06ykqr7wzbxscj2i8hpcgmcvgb2y1j77dvsxnpxq0943nfa1fnhk")))

(define-public crate-jxl-vardct-0.2.1 (c (n "jxl-vardct") (v "0.2.1") (d (list (d (n "jxl-bitstream") (r "^0.2.3") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "jxl-modular") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "1bgyn7h370xrbxls7i8g41k3p1waq4wsdb5r8p5lhzi1pk6gzfhs")))

(define-public crate-jxl-vardct-0.3.0 (c (n "jxl-vardct") (v "0.3.0") (d (list (d (n "jxl-bitstream") (r "^0.2.3") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.3") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "jxl-modular") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0gh52l6na3yn9kwajfdrr44i6zf6m8w1r81v2kv552n4m2djsjpb")))

(define-public crate-jxl-vardct-0.4.0 (c (n "jxl-vardct") (v "0.4.0") (d (list (d (n "jxl-bitstream") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-modular") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "10dl9m5yp6iydcpniszg2ahy3r788664qd8k8px9v140rynaqyq2")))

(define-public crate-jxl-vardct-0.5.0 (c (n "jxl-vardct") (v "0.5.0") (d (list (d (n "jxl-bitstream") (r "^0.3.1") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-modular") (r "^0.5.0") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "112ijp2kyyfdbdpm2a446vabw22x7dxnh6j4vj8f37dydinaz8w1")))

(define-public crate-jxl-vardct-0.6.0 (c (n "jxl-vardct") (v "0.6.0") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.1") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-modular") (r "^0.6.0") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0lsvbxc9xq8sqg9nmx1l5sppccbmfba2b42v73i3iy7d56bj7ky7")))

(define-public crate-jxl-vardct-0.6.1 (c (n "jxl-vardct") (v "0.6.1") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.1") (d #t) (k 0)) (d (n "jxl-modular") (r "^0.6.1") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "1ffzb3qm0jn3f6y8kkxf82yzrp33fbjbbw8grdd5l3pcmbhxlls8")))

(define-public crate-jxl-vardct-0.6.2 (c (n "jxl-vardct") (v "0.6.2") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.1") (d #t) (k 0)) (d (n "jxl-modular") (r "^0.6.2") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0a94b9hl1y83j98a1nrx7yk6gd74jhl2i72ylqwra82995ccr9i1")))

(define-public crate-jxl-vardct-0.7.0 (c (n "jxl-vardct") (v "0.7.0") (d (list (d (n "jxl-bitstream") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.1") (d #t) (k 0)) (d (n "jxl-modular") (r "^0.7.0") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "1q29nqw6ab6g4jd9hdkh6ypky0kddb6d7gr0sz58f23pmnhq5bqn")))

