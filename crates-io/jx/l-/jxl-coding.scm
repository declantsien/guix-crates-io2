(define-module (crates-io jx l- jxl-coding) #:use-module (crates-io))

(define-public crate-jxl-coding-0.1.0 (c (n "jxl-coding") (v "0.1.0") (d (list (d (n "jxl-bitstream") (r "^0.1.0") (d #t) (k 0)))) (h "02cwgnmcjkpisbaca7bk7c2831dcqm8rj4y37sszgff20kndn13n")))

(define-public crate-jxl-coding-0.2.0 (c (n "jxl-coding") (v "0.2.0") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)))) (h "1q2hxj7q2qwna0qsmcr93hjphpmlvl4xwqfkjvjqzczwa8n2rsvx")))

(define-public crate-jxl-coding-0.2.1 (c (n "jxl-coding") (v "0.2.1") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)))) (h "06p96w9jk7685mkvgm8xpvbm1r4idpnhk62s6b0z8s828crcgm8q")))

(define-public crate-jxl-coding-0.2.2 (c (n "jxl-coding") (v "0.2.2") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)))) (h "0m2as7c616mmr1m47mbvxkw8l97fis31bdnp6z2n1v2d5c1p5h2q")))

(define-public crate-jxl-coding-0.2.3 (c (n "jxl-coding") (v "0.2.3") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)))) (h "1d6h5ms7ffclrag48892zq0i9byjvsl3zg0zvpifj71cfq660xbh")))

(define-public crate-jxl-coding-0.3.0 (c (n "jxl-coding") (v "0.3.0") (d (list (d (n "jxl-bitstream") (r "^0.3.0") (d #t) (k 0)))) (h "14c8m2z6clpsq2gnzvhj1dz507ra47zxsnb2br93x50m8kqlj4c5")))

(define-public crate-jxl-coding-0.3.1 (c (n "jxl-coding") (v "0.3.1") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "1q0c22763h0iz32whsn2kmh6irsn5w82zmbwnsxw35k9ynbj1ij4")))

(define-public crate-jxl-coding-0.3.2 (c (n "jxl-coding") (v "0.3.2") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0s43fgd9zw4g4673izjmng5wsrx8vmjsbwsfsp79220h6bkij33j")))

(define-public crate-jxl-coding-0.4.0 (c (n "jxl-coding") (v "0.4.0") (d (list (d (n "jxl-bitstream") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0p883wgdyixf9nk11hkzf8qd2610b3jhvmlzydjjkrjahrf7gz51")))

