(define-module (crates-io jx l- jxl-color) #:use-module (crates-io))

(define-public crate-jxl-color-0.1.0 (c (n "jxl-color") (v "0.1.0") (d (list (d (n "jxl-bitstream") (r "^0.1.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.1.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)))) (h "0a64hhqjy3556v04h17llbrp1xkfmbiqlm3ajn0d96bxj71qya6b")))

(define-public crate-jxl-color-0.1.1 (c (n "jxl-color") (v "0.1.1") (d (list (d (n "jxl-bitstream") (r "^0.1.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.1.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)))) (h "0ma7nnz2nsyjfag0jgk2lhkrzyv43xsmm9pjim0pfys38ivyysk2")))

(define-public crate-jxl-color-0.1.2 (c (n "jxl-color") (v "0.1.2") (d (list (d (n "jxl-bitstream") (r "^0.1.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.1.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)))) (h "0vv97d7mk34lcjqnki2d1gifqp3rgmr8srkdwkgfy74xjsnkh49p")))

(define-public crate-jxl-color-0.2.0 (c (n "jxl-color") (v "0.2.0") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)))) (h "1dlpw6riz5p1k4zdx4k5lp5q830xaanq1daplqz8g016yidjvphd")))

(define-public crate-jxl-color-0.3.0 (c (n "jxl-color") (v "0.3.0") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)))) (h "0ayh7a8s2cjxi4fvzzzw3j0rafh55nc5x4xfgbl5d6rqq9ks1ssv")))

(define-public crate-jxl-color-0.3.1 (c (n "jxl-color") (v "0.3.1") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)))) (h "1yl5zbq6wxqzkbq2g650mn8vrak893zd53vn8rqvihl6y9b529sh")))

(define-public crate-jxl-color-0.3.2 (c (n "jxl-color") (v "0.3.2") (d (list (d (n "jxl-bitstream") (r "^0.2.3") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)))) (h "0wvfiyqf0pmm189g6d83g3iz8mqslla7a63cvfqjc7nhnqrvr14h")))

(define-public crate-jxl-color-0.4.0 (c (n "jxl-color") (v "0.4.0") (d (list (d (n "jxl-bitstream") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.2.0") (d #t) (k 0)))) (h "13vz5jphvl93rf3fc8bbj9z2fwhpp43p49cbdk2yy4978zdm82rj")))

(define-public crate-jxl-color-0.5.0 (c (n "jxl-color") (v "0.5.0") (d (list (d (n "jxl-bitstream") (r "^0.3.1") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0f77q2ssd2m7wcs87qdyyg9qi77w6y3krngjwhgz792fz270kaiy")))

(define-public crate-jxl-color-0.6.0 (c (n "jxl-color") (v "0.6.0") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.1") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "18gp39c3m39yy39qh9qfh1sdqd76srxi8k3570bxqwvcszz753gn")))

(define-public crate-jxl-color-0.6.1 (c (n "jxl-color") (v "0.6.1") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.1") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "1y25gcnjd6h2hfjfi6j6sbfximzsxyn43cxfsz497wd85194mxcy")))

(define-public crate-jxl-color-0.7.0 (c (n "jxl-color") (v "0.7.0") (d (list (d (n "jxl-bitstream") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.1") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0h5jbdb3sq3qbvai9pvhf6a3s6bqhb8yg89iqivmyg1y9lrm1w9j")))

