(define-module (crates-io jx l- jxl-image) #:use-module (crates-io))

(define-public crate-jxl-image-0.1.0 (c (n "jxl-image") (v "0.1.0") (d (list (d (n "jxl-bitstream") (r "^0.1.3") (d #t) (k 0)) (d (n "jxl-color") (r "^0.1.2") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "07wa8wg0yly9lr1m4wdzzmzrbzdkfylbw33a00aa9d16xxjrzq94")))

(define-public crate-jxl-image-0.2.0 (c (n "jxl-image") (v "0.2.0") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-color") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "1bjnzg5lxmmb8r2b6vkqb3w8bpjci43w8378j7axb7vfzspj5kax")))

(define-public crate-jxl-image-0.3.0 (c (n "jxl-image") (v "0.3.0") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-color") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "12aks4jw9q7chg0qmr6awc3l36339rgfhqjq8a13fid0lj035l0c")))

(define-public crate-jxl-image-0.4.0 (c (n "jxl-image") (v "0.4.0") (d (list (d (n "jxl-bitstream") (r "^0.2.2") (d #t) (k 0)) (d (n "jxl-color") (r "^0.3.1") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "1qwi9756fmjbpxpilfaq62w4x7asz327kkn31hl5v01h573vnlkk")))

(define-public crate-jxl-image-0.4.1 (c (n "jxl-image") (v "0.4.1") (d (list (d (n "jxl-bitstream") (r "^0.2.3") (d #t) (k 0)) (d (n "jxl-color") (r "^0.3.1") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "1rd380fszp4qm9jml8m6vbpxf1szpfp6nyxk4nfd23dagdqhrl98")))

(define-public crate-jxl-image-0.5.0 (c (n "jxl-image") (v "0.5.0") (d (list (d (n "jxl-bitstream") (r "^0.2.3") (d #t) (k 0)) (d (n "jxl-color") (r "^0.3.1") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0ndyc28vppfrdg9dg8sy8l65mvyd007073acc1k9x76c9bvzg1pg")))

(define-public crate-jxl-image-0.6.0 (c (n "jxl-image") (v "0.6.0") (d (list (d (n "jxl-bitstream") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-color") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.2.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0fcaq11maq5nj24l2yjagpvcas3vsaj17cvlz38f91h34kqi2ff3")))

(define-public crate-jxl-image-0.7.0 (c (n "jxl-image") (v "0.7.0") (d (list (d (n "jxl-bitstream") (r "^0.3.1") (d #t) (k 0)) (d (n "jxl-color") (r "^0.5.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "1fymkdxw7mi0b0j3zk08gpfac5lf2zl8mrbyzi5pky9myx82ibim")))

(define-public crate-jxl-image-0.8.0 (c (n "jxl-image") (v "0.8.0") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-color") (r "^0.6.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0l515q5pwk6gh37m120wrzd725qqgg34nkij1dagbv06dwvqvza7")))

(define-public crate-jxl-image-0.8.1 (c (n "jxl-image") (v "0.8.1") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-color") (r "^0.6.1") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "1hf7gflakzmpb4f2s8hq6y6id5l6gymrnxk27j6dnc0jw9wd11p2")))

(define-public crate-jxl-image-0.9.0 (c (n "jxl-image") (v "0.9.0") (d (list (d (n "jxl-bitstream") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-color") (r "^0.7.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "1inyxqzhm8jra77khxnidkd9s00fjgsjhrmhwxjkn3mxsdzb2c8b")))

