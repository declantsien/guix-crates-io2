(define-module (crates-io jx l- jxl-modular) #:use-module (crates-io))

(define-public crate-jxl-modular-0.1.0 (c (n "jxl-modular") (v "0.1.0") (d (list (d (n "jxl-bitstream") (r "^0.1.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.1.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "05nwm81raabrfws8jw469mn6515scds1qkxr2x218wzhl98j59gy")))

(define-public crate-jxl-modular-0.2.0 (c (n "jxl-modular") (v "0.2.0") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "179z96f9gajk7jx7xam0dh25jqqzkbqvyz74ijvwjk65glzwcyca")))

(define-public crate-jxl-modular-0.2.1 (c (n "jxl-modular") (v "0.2.1") (d (list (d (n "jxl-bitstream") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0l2npvib47n5grrqiqbgvmq6pkcpzmlsb2dhw9g4carjqda72466")))

(define-public crate-jxl-modular-0.2.2 (c (n "jxl-modular") (v "0.2.2") (d (list (d (n "jxl-bitstream") (r "^0.2.3") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.3") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0z9a35zch1n10kgq0cqfxaajiwlpr25cb1b11nk5ils8v8qsgzm4")))

(define-public crate-jxl-modular-0.3.0 (c (n "jxl-modular") (v "0.3.0") (d (list (d (n "jxl-bitstream") (r "^0.2.3") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.2.3") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0j377r653p32zxlyfymndz2qcrzwfb3r66g85dcnh99nvdannkjh")))

(define-public crate-jxl-modular-0.4.0 (c (n "jxl-modular") (v "0.4.0") (d (list (d (n "jxl-bitstream") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.2.0") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0w1dda26aiqm453v9h2wwzv0a9vjmk674wfg3lf11ryrbvbhzam3")))

(define-public crate-jxl-modular-0.5.0 (c (n "jxl-modular") (v "0.5.0") (d (list (d (n "jxl-bitstream") (r "^0.3.1") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.3.0") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "03pph8mhnkbd0lxmqnm0xvgfgh9yfg6kbpsmirfhibr154pm2mgr")))

(define-public crate-jxl-modular-0.6.0 (c (n "jxl-modular") (v "0.6.0") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.1") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0ir2djrzcqbpdv6xlspx3sjgrhd9bp5jm91lxiz8p0rlfl13zg88")))

(define-public crate-jxl-modular-0.6.1 (c (n "jxl-modular") (v "0.6.1") (d (list (d (n "jxl-bitstream") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.1") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0ds8yrfj6alqlfkgkapg3sbbl9j81xy6sva0hxpj29dzsx0w4x22")))

(define-public crate-jxl-modular-0.6.2 (c (n "jxl-modular") (v "0.6.2") (d (list (d (n "jxl-bitstream") (r "^0.3.3") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.3.2") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.1") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "0f53yhhpxl3yps6vcyriih1pzcc7arak1yp0jdlvjx4gkck134jz")))

(define-public crate-jxl-modular-0.7.0 (c (n "jxl-modular") (v "0.7.0") (d (list (d (n "jxl-bitstream") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-coding") (r "^0.4.0") (d #t) (k 0)) (d (n "jxl-grid") (r "^0.4.1") (d #t) (k 0)) (d (n "jxl-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (f (quote ("std"))) (k 0)))) (h "1644vfp7h0bhb34cn6kc1lc50sdkxc3jq9pdi77nm6rkkjzkm161")))

