(define-module (crates-io l2 #{98}# l298n) #:use-module (crates-io))

(define-public crate-l298n-0.1.0 (c (n "l298n") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1hcffxfmymav1a0almysj4v10z8rypz53qkzfhx9s0nz1zzqrh15")))

(define-public crate-l298n-0.1.1 (c (n "l298n") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "03rylcksicqgybf5pnpdj559dsa7mhzgsd2lqkz5y0lnhrpklh6f")))

(define-public crate-l298n-0.1.2 (c (n "l298n") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0idjcjgxkv27h00cw6s9b2aq0i3lfmdw99r2c2qr7l7hjciyj1cg")))

(define-public crate-l298n-0.1.3 (c (n "l298n") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0p6qyam2jwx5zzmxp02igw6d01p7s3cql2r1pg15rayzmdqkh8s1")))

(define-public crate-l298n-0.2.0 (c (n "l298n") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1l95vqk8iimyf6y3klsj7ldhxvx6dlja2awmyjk4afxgmqvknhqx")))

