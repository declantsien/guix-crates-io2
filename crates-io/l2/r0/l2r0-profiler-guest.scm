(define-module (crates-io l2 r0 l2r0-profiler-guest) #:use-module (crates-io))

(define-public crate-l2r0-profiler-guest-0.21.0 (c (n "l2r0-profiler-guest") (v "0.21.0") (h "0m38rs2b27syfsqjnw9f68rfbclwdl4khkxwn5zd9jz5hnlk5sm8") (f (quote (("print-trace")))) (y #t)))

(define-public crate-l2r0-profiler-guest-0.20.1 (c (n "l2r0-profiler-guest") (v "0.20.1") (h "0h5yxzpssiymmc2kcgs14nlss6s4f2splx6v17w73m64mr06iqgl") (f (quote (("print-trace"))))))

