(define-module (crates-io l2 r0 l2r0-profiler-host) #:use-module (crates-io))

(define-public crate-l2r0-profiler-host-0.21.0 (c (n "l2r0-profiler-host") (v "0.21.0") (d (list (d (n "ahash") (r "=0.8.6") (d #t) (k 0)) (d (n "colored") (r "=2.0.4") (d #t) (k 0)) (d (n "raki") (r "^0.1.3") (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.20.1") (f (quote ("client"))) (d #t) (k 0)))) (h "1y1242hc7yg37hg5j580q3cby8ysd5xv5jy6qmrmc6pyxvmgmaca") (y #t)))

(define-public crate-l2r0-profiler-host-0.20.1 (c (n "l2r0-profiler-host") (v "0.20.1") (d (list (d (n "ahash") (r "=0.8.6") (d #t) (k 0)) (d (n "colored") (r "=2.0.4") (d #t) (k 0)) (d (n "raki") (r "^0.1.3") (d #t) (k 0)) (d (n "risc0-zkvm") (r "^0.20.1") (f (quote ("client"))) (d #t) (k 0)))) (h "0md874irz9yrfngx3qhphbjj7lw3h8ryywx4qc8c3clb4hva6v7b")))

