(define-module (crates-io l2 #{0n}# l20n) #:use-module (crates-io))

(define-public crate-l20n-0.0.1 (c (n "l20n") (v "0.0.1") (h "1chvaa9062qvp1jqfbw20c1a1kjqgybjsh2j78pcdbl8m37vpixr") (y #t)))

(define-public crate-l20n-0.1.0 (c (n "l20n") (v "0.1.0") (d (list (d (n "serde") (r "^0.6") (d #t) (k 0)))) (h "08x5fyw8c1r9lbjmh8xxqycv0jawyxdsgaqs0vcwv2ky82j4aslx") (y #t)))

(define-public crate-l20n-0.1.1 (c (n "l20n") (v "0.1.1") (d (list (d (n "serde") (r "^0.6") (d #t) (k 0)))) (h "0ikh6vkycc068smq784a3cl054v28xclqn57gckks0l3gvvvjmfg") (y #t)))

(define-public crate-l20n-0.1.2 (c (n "l20n") (v "0.1.2") (d (list (d (n "serde") (r "^0.6") (d #t) (k 0)))) (h "1fgl3a6b5rld17y07gv96f1mcpdw598hdaywqxzf4ifn0r1lw1yq")))

