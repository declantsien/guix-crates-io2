(define-module (crates-io dw t- dwt-systick-monotonic) #:use-module (crates-io))

(define-public crate-dwt-systick-monotonic-0.1.0-alpha.0 (c (n "dwt-systick-monotonic") (v "0.1.0-alpha.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "0sp91fm7rnjxdmqw46zw2y7cdnzwrzvlmdmrhi3c16zvjxh8k8ml")))

(define-public crate-dwt-systick-monotonic-0.1.0-alpha.1 (c (n "dwt-systick-monotonic") (v "0.1.0-alpha.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "1qjxidmb0f5za97n5qq8clqk8ngq3acly8jxd1wnd4azf2map14n")))

(define-public crate-dwt-systick-monotonic-0.1.0-alpha.2 (c (n "dwt-systick-monotonic") (v "0.1.0-alpha.2") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "1qni9c4j50xllh8jqqn45bxcksbqz282rbpc5i6mgd97klplvlli")))

(define-public crate-dwt-systick-monotonic-0.1.0-alpha.3 (c (n "dwt-systick-monotonic") (v "0.1.0-alpha.3") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "0sxasi44s4g4qggqblim1ny6pbnn5s6y3lxd8xa82h327dp4h4m1")))

(define-public crate-dwt-systick-monotonic-0.1.0-rc.1 (c (n "dwt-systick-monotonic") (v "0.1.0-rc.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^0.1.0-rc.1") (d #t) (k 0)))) (h "06pl3v7gslrks0j2k2v8qkcswcw18giy4bx3q4an4yjkam8h097f")))

(define-public crate-dwt-systick-monotonic-0.1.0-rc.2 (c (n "dwt-systick-monotonic") (v "0.1.0-rc.2") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^0.1.0-rc.2") (d #t) (k 0)))) (h "14f6a55h397igimvdqdcsl7sw4fgj97wdn8y2zrlbmhsnqnkw2z5")))

(define-public crate-dwt-systick-monotonic-1.0.0 (c (n "dwt-systick-monotonic") (v "1.0.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^1.0.0") (d #t) (k 0)))) (h "0gpisqv1h20rbshnyyhjw84dw5ljw68gq5cfwb9xwcibw2hyy7fm")))

(define-public crate-dwt-systick-monotonic-1.1.0 (c (n "dwt-systick-monotonic") (v "1.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.4") (d #t) (k 0)) (d (n "fugit") (r "^0.3.0") (d #t) (k 0)) (d (n "rtic-monotonic") (r "^1.0.0") (d #t) (k 0)))) (h "1ky0i0lbld353l4niz86fa9yz2z5v91byawqqqgzkkgijiqq1kw9") (f (quote (("extend"))))))

