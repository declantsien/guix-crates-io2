(define-module (crates-io dw mb dwmbar) #:use-module (crates-io))

(define-public crate-dwmbar-0.1.0 (c (n "dwmbar") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (d #t) (k 0)) (d (n "x11") (r "^2.21.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0fs0yamz8piyv1icg6y03y12bqd1w6gw484gvf784k6z6mqfgj3r")))

(define-public crate-dwmbar-0.1.1 (c (n "dwmbar") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (d #t) (k 0)) (d (n "x11") (r "^2.21.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0nbaw9a8q8631hyrw2g4fw65nn2z5xylad7nzk7lydjls8bhrg0l")))

(define-public crate-dwmbar-0.1.2 (c (n "dwmbar") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (d #t) (k 0)) (d (n "x11") (r "^2.21.0") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1hpsn21zj80kk8pxcphb14h02ksgsbxn1zn81rhcgswrlmyhwkwa")))

