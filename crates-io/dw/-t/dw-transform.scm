(define-module (crates-io dw -t dw-transform) #:use-module (crates-io))

(define-public crate-dw-transform-0.1.0 (c (n "dw-transform") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dw-models") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12pv40x8fljdq7gk130lsh35cc0wj9cmzf1y9r2j85csxiqxzrxy")))

