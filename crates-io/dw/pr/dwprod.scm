(define-module (crates-io dw pr dwprod) #:use-module (crates-io))

(define-public crate-dwprod-0.1.0 (c (n "dwprod") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.2") (o #t) (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "gimli") (r "^0.14.0") (d #t) (k 0)) (d (n "object") (r "^0.4.1") (d #t) (k 0)))) (h "0bqyl6hbdfpbjcw611xk6jx85454w643h44lg1q3fnaww63cmsji") (f (quote (("exe" "clap") ("default" "exe"))))))

