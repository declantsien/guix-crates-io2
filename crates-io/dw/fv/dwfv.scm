(define-module (crates-io dw fv dwfv) #:use-module (crates-io))

(define-public crate-dwfv-0.1.0 (c (n "dwfv") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lalrpop") (r "^0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.5") (d #t) (k 0)))) (h "082b3ijpxr5v7bshjknc7si8x7k6rvbwkg1bdc6mdnjng9pls8cv")))

(define-public crate-dwfv-0.2.0 (c (n "dwfv") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lalrpop") (r "^0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.5") (d #t) (k 0)))) (h "1aaa8piqdbmqqcqvr3jvpyyhlj3ryrgmp1hj0qx8j8jzdnfz88fh")))

(define-public crate-dwfv-0.2.1 (c (n "dwfv") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.18") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.9") (d #t) (k 0)))) (h "0432gk3286v584fx7v7spa2gl7wbjpld20977h6m32icz4mzcg0d")))

(define-public crate-dwfv-0.3.0 (c (n "dwfv") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.18") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.9") (d #t) (k 0)))) (h "1gmqv3ag8s7m1iyp6lxban934qplsjjz8fpgqdjjxvplhj38kwpk")))

(define-public crate-dwfv-0.4.0 (c (n "dwfv") (v "0.4.0") (d (list (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^5.1") (f (quote ("std"))) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.9") (d #t) (k 0)))) (h "1db2q4q6gy0ydx3mdqda69inci0ym55qlbchl7yygy49yian2b2y")))

(define-public crate-dwfv-0.4.1 (c (n "dwfv") (v "0.4.1") (d (list (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^5.1") (f (quote ("std"))) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.9") (d #t) (k 0)))) (h "093z16k8daad01incj7y2pg05zfpnjxkqjn2x8pf4naz0px5jpff")))

