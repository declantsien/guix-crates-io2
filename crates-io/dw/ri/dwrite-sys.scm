(define-module (crates-io dw ri dwrite-sys) #:use-module (crates-io))

(define-public crate-dwrite-sys-0.0.1 (c (n "dwrite-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0a0qy7l2f8nzksza0vgyc93f6ckhb99fnwalfaxbzxf4fcil2r9b")))

(define-public crate-dwrite-sys-0.2.0 (c (n "dwrite-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0vldjn16bid1x565zl9g82mqf6lv2kr78b918a2vqqiqyf0854d7")))

