(define-module (crates-io dw -s dw-sys) #:use-module (crates-io))

(define-public crate-dw-sys-0.1.0 (c (n "dw-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0wx9anfiv11s6kqfmqa74nkjwdx5v73jpcqv0b8nqqpngz43bsql") (l "dw")))

