(define-module (crates-io dw ar dwarfdump) #:use-module (crates-io))

(define-public crate-dwarfdump-0.1.0 (c (n "dwarfdump") (v "0.1.0") (d (list (d (n "fallible-iterator") (r "^0.1.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "gimli") (r "^0.13.0") (d #t) (k 0)) (d (n "memmap") (r "^0.4.0") (d #t) (k 0)) (d (n "object") (r "^0.3.0") (d #t) (k 0)))) (h "0ph8cw9kccjkjhg7al11irrivfgpz1glp3d0kk4b7dhwv01rm666")))

