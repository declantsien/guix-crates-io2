(define-module (crates-io dw ar dwarfdump-wasm) #:use-module (crates-io))

(define-public crate-dwarfdump-wasm-0.1.0 (c (n "dwarfdump-wasm") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "gimli") (r "^0.26.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "wasm-edit") (r "^0.1.2") (d #t) (k 0)))) (h "126lblgyh53c4djd0cq6y89sic77ncnpq33k782mkkq7xr285la7") (y #t)))

(define-public crate-dwarfdump-wasm-0.1.1 (c (n "dwarfdump-wasm") (v "0.1.1") (d (list (d (n "core-wasm-ast") (r "^0.1.12") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "gimli") (r "^0.26.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.12") (d #t) (k 0)))) (h "0j0g8p5024v7lxqr8ypi9x4n3bz3daf7p0c8fz4rq6nzbn5dz9l6") (y #t)))

