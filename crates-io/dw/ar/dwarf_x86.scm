(define-module (crates-io dw ar dwarf_x86) #:use-module (crates-io))

(define-public crate-dwarf_x86-0.1.0 (c (n "dwarf_x86") (v "0.1.0") (d (list (d (n "elf") (r "^0.0.10") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "gimli") (r "^0.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.1") (d #t) (k 0)))) (h "0873m55xb1i0yrhkmchali1v848l68z8pg9n5nrjhv25skg8wybh")))

