(define-module (crates-io dw ar dwarf) #:use-module (crates-io))

(define-public crate-dwarf-0.0.1 (c (n "dwarf") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "elf") (r "^0.0.9") (d #t) (k 2)))) (h "1vf6wb01zm8db9kww2rc6r62qh7v26d8xpfg9s5y4mx99n4shaqy")))

(define-public crate-dwarf-0.0.2 (c (n "dwarf") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "elf") (r "^0.0.9") (d #t) (k 0)))) (h "1qrbxd5xzqimx2yy36a24w5wbcl02qbnwxadjvrkfbvpxp71hmjr")))

(define-public crate-dwarf-0.0.3 (c (n "dwarf") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "elf") (r "^0.0.9") (d #t) (k 0)))) (h "0w9zs39f7ph2w1pmp2lh5zc40ak8q7qz5r2159qkikqqzqlswsf0") (f (quote (("clippy"))))))

