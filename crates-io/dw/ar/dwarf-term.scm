(define-module (crates-io dw ar dwarf-term) #:use-module (crates-io))

(define-public crate-dwarf-term-0.1.0 (c (n "dwarf-term") (v "0.1.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)) (d (n "glutin") (r "^0.15") (d #t) (k 0)) (d (n "retro-pixel") (r "^0.3.0") (d #t) (k 0)))) (h "05a8y727ndynxxmg9rs1cq3aaa5cvnvkqfcfgx09144qlxs497f6")))

(define-public crate-dwarf-term-0.1.1 (c (n "dwarf-term") (v "0.1.1") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)) (d (n "glutin") (r "^0.16") (d #t) (k 0)) (d (n "retro-pixel") (r "^0.3") (d #t) (k 0)))) (h "07bprzp32dla4gadlx5rm556lb7lc7ks1r8410910yfsm1wijc75")))

(define-public crate-dwarf-term-0.2.0 (c (n "dwarf-term") (v "0.2.0") (d (list (d (n "gl") (r "^0.10") (d #t) (k 0)) (d (n "glium") (r "^0.22") (d #t) (k 0)) (d (n "glutin") (r "^0.17") (d #t) (k 0)) (d (n "retro-pixel") (r "^0.3") (d #t) (k 0)))) (h "12201rj23qrf4q72qjqzp464qmgg6lrz0d7jqj32n2vr1q5fisi6")))

