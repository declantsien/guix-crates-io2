(define-module (crates-io dw bh dwbhk) #:use-module (crates-io))

(define-public crate-dwbhk-0.1.0 (c (n "dwbhk") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("rustls-tls" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("rt"))) (d #t) (k 2)))) (h "170zwamzr7p6wsckhpcjgs02cn0rdp1ydq9jbz3msdz9nk6ri0pz")))

(define-public crate-dwbhk-0.1.1 (c (n "dwbhk") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("rustls-tls" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("rt"))) (d #t) (k 2)))) (h "180flhssfvfs5phhq24fwhf1wxh5md96hpy1yp1x6d9771mljgyk") (f (quote (("no-panic") ("blocking" "reqwest/blocking"))))))

(define-public crate-dwbhk-0.1.2 (c (n "dwbhk") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("rt"))) (d #t) (k 2)))) (h "17b1ja9m5x2gp2yjz1anj3kzx8azi5hn59dwdp20q5d976isafk7") (f (quote (("no-panic") ("blocking" "reqwest/blocking"))))))

