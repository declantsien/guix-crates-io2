(define-module (crates-io dw -q dw-query) #:use-module (crates-io))

(define-public crate-dw-query-0.1.0 (c (n "dw-query") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dw-datastore") (r "^0.1.0") (d #t) (k 0)) (d (n "dw-models") (r "^0.1.0") (d #t) (k 0)) (d (n "dw-transform") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plex") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c1c3rl6mqkwnc3css9kgkdsp1c8l95qj1flm5n1mxawgav3rrrb")))

