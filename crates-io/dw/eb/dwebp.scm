(define-module (crates-io dw eb dwebp) #:use-module (crates-io))

(define-public crate-dwebp-0.1.0 (c (n "dwebp") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (f (quote ("png"))) (k 0)) (d (n "png") (r "^0.17") (k 2)) (d (n "tempfile") (r "^3.2") (k 2)) (d (n "webp-animation") (r "^0.5") (f (quote ("image"))) (k 0)))) (h "11gmshmz8062kfdzmaxknjflhprsgfvvkr8wsh6jksdrjips5lkp")))

