(define-module (crates-io dw ee dweet) #:use-module (crates-io))

(define-public crate-dweet-0.1.0 (c (n "dweet") (v "0.1.0") (d (list (d (n "hyper") (r "^0.6.13") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "15f7vgi16fjnm820aayn8sm33cwq34w583dr9l2wq04z64k3qz2d")))

