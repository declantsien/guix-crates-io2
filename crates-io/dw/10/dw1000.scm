(define-module (crates-io dw #{10}# dw1000) #:use-module (crates-io))

(define-public crate-dw1000-0.1.0 (c (n "dw1000") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "ieee802154") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "0ccznzybsvn2zgi5lbcgbvrqxjqq10xsjlkgajimjhkl5xqc6xwg")))

(define-public crate-dw1000-0.2.0 (c (n "dw1000") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "ieee802154") (r "^0.3.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (f (quote ("derive"))) (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)))) (h "0cklqj437mb46dhc0glfkxz7ls1iv390h6gf6m4a2b2hmjrhv73n")))

(define-public crate-dw1000-0.3.0 (c (n "dw1000") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "ieee802154") (r "^0.3.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (f (quote ("derive"))) (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)))) (h "108f85i5pm4c9sg6v9dd9bby05sr0sqlwpdqxgm4lsk96il5k9bd")))

(define-public crate-dw1000-0.4.0 (c (n "dw1000") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "ieee802154") (r "^0.3.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)))) (h "1lks4n5za7hlmzjlhv06kr8rybkdghr0adl0y5pbirni1qjvbdal")))

(define-public crate-dw1000-0.5.0 (c (n "dw1000") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "ieee802154") (r "^0.3.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (k 0)) (d (n "ssmarshal") (r "^1.0.0") (k 0)))) (h "0azd68w6bxgq60v8cl1il8f4a2nryb0kdxixafvb0g5wf6dpi8f9")))

(define-public crate-dw1000-0.6.0 (c (n "dw1000") (v "0.6.0") (d (list (d (n "byte") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "fixed") (r "^1.11.0") (d #t) (k 0)) (d (n "ieee802154") (r "^0.6.0") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)) (d (n "ssmarshal") (r "^1.0.0") (k 0)))) (h "1v91hfbdn5mzfpnxw4560s0hvz67jmyb5wk9ibxy84b5lylgwv53") (f (quote (("std" "ssmarshal/std" "serde/std" "num_enum/std") ("default"))))))

