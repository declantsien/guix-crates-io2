(define-module (crates-io dw -m dw-models) #:use-module (crates-io))

(define-public crate-dw-models-0.1.0 (c (n "dw-models") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bxv4y0rsf8a65z2lzaw1ydxiqis3a23v1r5f0lpnazxfh7fza37")))

