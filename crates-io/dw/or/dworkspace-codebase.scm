(define-module (crates-io dw or dworkspace-codebase) #:use-module (crates-io))

(define-public crate-dworkspace-codebase-0.0.0 (c (n "dworkspace-codebase") (v "0.0.0") (d (list (d (n "ast") (r "^0.0.0") (d #t) (k 0) (p "deskc-ast")) (d (n "deskc-ids") (r "^0.0.0") (d #t) (k 0) (p "deskc-ids")) (d (n "hir") (r "^0.0.0") (d #t) (k 0) (p "deskc-hir")) (d (n "types") (r "^0.0.0") (d #t) (k 0) (p "deskc-types")) (d (n "uuid") (r "^1.2") (d #t) (k 0)))) (h "16xvvvy4hz8gn8ic2f6a5h7lh0fip63g2nns6qyd4k1a54yhxji1")))

