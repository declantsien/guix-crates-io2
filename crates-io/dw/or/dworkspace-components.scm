(define-module (crates-io dw or dworkspace-components) #:use-module (crates-io))

(define-public crate-dworkspace-components-0.0.0 (c (n "dworkspace-components") (v "0.0.0") (d (list (d (n "deskc-ids") (r "^0.0.0") (d #t) (k 0) (p "deskc-ids")) (d (n "hir") (r "^0.0.0") (d #t) (k 0) (p "deskc-hir")) (d (n "types") (r "^0.0.0") (d #t) (k 0) (p "deskc-types")) (d (n "uuid") (r "^1.2") (d #t) (k 0)))) (h "13dgx24j0l2ji5jhisqp1310bwsjl53b4dpjnyy3hj808drkcwha")))

