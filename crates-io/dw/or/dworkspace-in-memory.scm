(define-module (crates-io dw or dworkspace-in-memory) #:use-module (crates-io))

(define-public crate-dworkspace-in-memory-0.0.0 (c (n "dworkspace-in-memory") (v "0.0.0") (d (list (d (n "dworkspace") (r "^0.0.0") (d #t) (k 0) (p "dworkspace")) (d (n "dworkspace-components") (r "^0.0.0") (d #t) (k 0) (p "dworkspace-components")))) (h "0vza6d5rmhj0smadi1yq5k404wqb58kv38hw76n2z1j45dpnyr9g")))

