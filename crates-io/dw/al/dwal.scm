(define-module (crates-io dw al dwal) #:use-module (crates-io))

(define-public crate-dwal-0.1.0 (c (n "dwal") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "086phva185w3fx7haxkvk6qb02fvvs1gd13vrh8s0mv3bgjzl9ya")))

(define-public crate-dwal-0.1.1 (c (n "dwal") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0b9x7xlv56v9lpqnv6zjwf8097573zxwwj7yh2b8bnw6p5kczc9f")))

(define-public crate-dwal-0.1.2 (c (n "dwal") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0rkg6vpd5frd7kn93b46mk6ggqpyhif2aj9f36fm0qfcp6smqg1z")))

(define-public crate-dwal-0.1.3 (c (n "dwal") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0y1cn35845k7h6px0jmzgawxkglg00s4s9lf1a6vhrdfsfg7p4i3")))

