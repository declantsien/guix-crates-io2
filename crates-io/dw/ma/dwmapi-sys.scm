(define-module (crates-io dw ma dwmapi-sys) #:use-module (crates-io))

(define-public crate-dwmapi-sys-0.0.1 (c (n "dwmapi-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "140nwgq2dwydar0dr3l3aapbxl8iq1wk2czy63nf3prxc0bd3yvd")))

(define-public crate-dwmapi-sys-0.1.0 (c (n "dwmapi-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "1zj11h02idy0hz94fdmmad8cn5pf1hfkg42d1ay1jr1rgg6cgi07")))

(define-public crate-dwmapi-sys-0.1.1 (c (n "dwmapi-sys") (v "0.1.1") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0xiyc8vibsda0kbamr9zkjvkdzdxcq8bs1g5mq4yc4mbmr168jxl")))

