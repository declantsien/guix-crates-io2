(define-module (crates-io dw n- dwn-rs-core) #:use-module (crates-io))

(define-public crate-dwn-rs-core-0.1.0 (c (n "dwn-rs-core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.30") (f (quote ("serde"))) (d #t) (k 0)) (d (n "libipld-core") (r "^0.16.0") (f (quote ("serde" "serde-codec" "multibase"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "1z1nabi1az9mfacx8lrjkay8firbxsdl91zvrhz3rdz1q5kx12al")))

