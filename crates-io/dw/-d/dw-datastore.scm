(define-module (crates-io dw -d dw-datastore) #:use-module (crates-io))

(define-public crate-dw-datastore-0.1.0 (c (n "dw-datastore") (v "0.1.0") (d (list (d (n "appdirs") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dw-models") (r "^0.1.0") (d #t) (k 0)) (d (n "dw-transform") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mpsc_requests") (r "^0.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (f (quote ("chrono" "serde_json" "bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0inn30bwz3b9k1b5niwls62hhhizhaqiqjjhgj7sxbbapbqzxdc2") (f (quote (("legacy_import_tests") ("default"))))))

