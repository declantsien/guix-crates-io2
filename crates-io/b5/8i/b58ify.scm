(define-module (crates-io b5 #{8i}# b58ify) #:use-module (crates-io))

(define-public crate-b58ify-0.1.0 (c (n "b58ify") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1xa158sw6c88rgxl8r12yp1y3lsrhcxcb6jf51j0lnl1i62h17gm") (y #t)))

(define-public crate-b58ify-0.1.1 (c (n "b58ify") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "11cfv83pxy800q451b5yb7kicwlicmns5l9mk9hz2mwbbm2zq0w7") (y #t)))

(define-public crate-b58ify-0.1.2 (c (n "b58ify") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1ny52xpp084vfmydm9ga95ik27p8kpxjrn8m6l8565mz9nikyc94") (y #t)))

