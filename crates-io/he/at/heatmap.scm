(define-module (crates-io he at heatmap) #:use-module (crates-io))

(define-public crate-heatmap-0.1.0 (c (n "heatmap") (v "0.1.0") (d (list (d (n "histogram") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1xhzsnnxifsjd69mwyzlcxbfmlb2lp89zsc62jkcmvv5mw52alah")))

(define-public crate-heatmap-0.1.1 (c (n "heatmap") (v "0.1.1") (d (list (d (n "histogram") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0ir5bi5jf7pwwyi45gjpa0pzw1l0lscsnw5nljsj9crz79zdka5a")))

(define-public crate-heatmap-0.1.4 (c (n "heatmap") (v "0.1.4") (d (list (d (n "histogram") (r "^0.3.5") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "0afgdpk80nzgdqz4p8rq35m3sn7ifaql2n8236wc434r05vzj0iw")))

(define-public crate-heatmap-0.1.5 (c (n "heatmap") (v "0.1.5") (d (list (d (n "histogram") (r "^0.3.5") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1r9zfvnwcmkbbzd1wb0avnc1rvdiv28pm5mxqrsb9b5y2ndl0yfr")))

(define-public crate-heatmap-0.1.6 (c (n "heatmap") (v "0.1.6") (d (list (d (n "histogram") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "0pl87lmrqa7qb5w5hbq552jlh1n6c4bd3lcv7zf15iiir68z49k5") (y #t)))

(define-public crate-heatmap-0.1.7 (c (n "heatmap") (v "0.1.7") (d (list (d (n "histogram") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1z3d5bb5a45bj7z15bbry8ngblb30rw5655lhf3m959vp32fw06h")))

(define-public crate-heatmap-0.2.0 (c (n "heatmap") (v "0.2.0") (d (list (d (n "histogram") (r "^0.6.0") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1b3fw8i0dh2v4k53djxs0ym2if2dpplss2d6505ww18c5x93ijdc")))

(define-public crate-heatmap-0.3.0 (c (n "heatmap") (v "0.3.0") (d (list (d (n "histogram") (r "^0.6.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0gxri4gmyy2h5w3iq789khxbdx20kvi328cak2z0nwjy2ir4sn6q")))

(define-public crate-heatmap-0.4.0 (c (n "heatmap") (v "0.4.0") (d (list (d (n "histogram") (r "^0.6.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0bhcxz3wjxiv7zjm7nz1milm1cj7c18v8y2sk4s23w2zlyynd5j7")))

(define-public crate-heatmap-0.5.0 (c (n "heatmap") (v "0.5.0") (d (list (d (n "histogram") (r "^0.6.1") (d #t) (k 0)))) (h "1vcz2b5c53af1kgf355v6h5gd2qn7mrvr37ilq067mp6l6kfp1a2")))

(define-public crate-heatmap-0.6.0 (c (n "heatmap") (v "0.6.0") (d (list (d (n "histogram") (r "^0.6.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1q1wrhqzy586jzsamxy2wnkbdzl5q9488m3nydz9fk97xgcw4ich")))

(define-public crate-heatmap-0.6.1 (c (n "heatmap") (v "0.6.1") (d (list (d (n "histogram") (r "^0.6.2") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1yghza586fargbq156ljzza214f4kw38d67m745645k0y0kh8yps")))

(define-public crate-heatmap-0.6.2 (c (n "heatmap") (v "0.6.2") (d (list (d (n "histogram") (r "^0.6.3") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "14javakwhl0iri589a219afvgbk5v0cgnzyqyssif59c51wbvp9h")))

(define-public crate-heatmap-0.6.3 (c (n "heatmap") (v "0.6.3") (d (list (d (n "histogram") (r "^0.6.4") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "1vi68dqjqsdpbx8gdpj259ywhisk0alrh9s54m0slbfis5jph5v9")))

(define-public crate-heatmap-0.6.4 (c (n "heatmap") (v "0.6.4") (d (list (d (n "histogram") (r "^0.6.4") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "1a9khhhqa9a6mkdfd5anc6p2sfadmjwslw3f8w2rg2krs4p8wj88")))

(define-public crate-heatmap-0.6.5 (c (n "heatmap") (v "0.6.5") (d (list (d (n "histogram") (r "^0.6.6") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0yd39jb2dxzxn06kq7mdmr641l314855dv0pqf1vpl44qhr8aw9f")))

(define-public crate-heatmap-0.6.6 (c (n "heatmap") (v "0.6.6") (d (list (d (n "histogram") (r "^0.6.6") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0l80i71118khc4kd4bq8yh7wh4fxs1d3w2dw3ywck4bb06lm35ac")))

(define-public crate-heatmap-0.7.0 (c (n "heatmap") (v "0.7.0") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "histogram") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "10s64j1j5h8ndc9lsnbwla9jc8vmf9apa9fm2x369f7g7pa41824")))

(define-public crate-heatmap-0.7.1 (c (n "heatmap") (v "0.7.1") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "histogram") (r "^0.7.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1yk5cxrgm1rslhgn7jpjsmcvppmj788fr3m532dx1lxbq9r3kc0f")))

(define-public crate-heatmap-0.7.2 (c (n "heatmap") (v "0.7.2") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "histogram") (r "^0.7.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1fmph6mqn0r60b2mc33vv89p0kx8z1s7n8plwxl9mhvqsqlzbckb")))

(define-public crate-heatmap-0.7.3 (c (n "heatmap") (v "0.7.3") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "histogram") (r "^0.7.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0am0wv7jhf8cgmf0a49ymrhn8bilj5rq790fg8ph9762qadkal9x")))

(define-public crate-heatmap-0.7.4 (c (n "heatmap") (v "0.7.4") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "histogram") (r "^0.7.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1hh01bh5mh48c2qjs58h40gn8j8lcc3y8zwdzdqp1dk0ikz1alnx")))

(define-public crate-heatmap-0.7.5 (c (n "heatmap") (v "0.7.5") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "histogram") (r "^0.7.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0cayk0n3m2gymx2asrxs90s7r90xhihsc7xm36pdhsafd81g9vzx")))

(define-public crate-heatmap-0.7.6 (c (n "heatmap") (v "0.7.6") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "histogram") (r "^0.7.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1yjq7hsp1njgghrfc1i4kvr1m45r5a98pb7cic3gi66ph03zkjyq")))

(define-public crate-heatmap-0.7.7 (c (n "heatmap") (v "0.7.7") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "histogram") (r "^0.7.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1jxw4pqy5v23b26i3ai361a8fzbaycjw56qw1945j4d5wnp3gwqg")))

(define-public crate-heatmap-0.7.8 (c (n "heatmap") (v "0.7.8") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "histogram") (r "^0.7.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "15413mjw6lgd0651yrqq2mqh7vlimzqnq60zaadab2r0ls3z82w1")))

(define-public crate-heatmap-0.8.0 (c (n "heatmap") (v "0.8.0") (h "06bhshgmjp7gxgd6ihk3dxd4kqsd038awxpvmy0mgxssg1h2nivy")))

