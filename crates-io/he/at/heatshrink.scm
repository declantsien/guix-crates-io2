(define-module (crates-io he at heatshrink) #:use-module (crates-io))

(define-public crate-heatshrink-0.1.0 (c (n "heatshrink") (v "0.1.0") (h "0arl5sd7m43ywykzl94iryzpg8x7b0km7d0vvqx482m0ywlskc7g")))

(define-public crate-heatshrink-0.2.0 (c (n "heatshrink") (v "0.2.0") (d (list (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "1x8r5rrqzg53iswfg4y0s7dgdzr7vhp8gysjijhl9p5npgydx54b")))

