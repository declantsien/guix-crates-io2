(define-module (crates-io he de hedel-rs) #:use-module (crates-io))

(define-public crate-hedel-rs-0.1.0 (c (n "hedel-rs") (v "0.1.0") (d (list (d (n "backtrace-on-stack-overflow") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hb18554nh63cyrpdjzf26zh8c2gvxvn8vb7z91m9j6ljm3la08s")))

(define-public crate-hedel-rs-0.1.1 (c (n "hedel-rs") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fqaivvbx0gxbs58p3bimrh0r2k2y3dnhnbf0rl7r772p6mqzblr")))

(define-public crate-hedel-rs-0.1.2 (c (n "hedel-rs") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12grwbcn6bzf91rjakky1rl0w45sxnj7k14pj0plz3xz89gwgwmb")))

(define-public crate-hedel-rs-0.1.3 (c (n "hedel-rs") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1plcq9pqjh2rmxjyssidq70kgsr8m2f15rm6ihygbvrqw50fclfk")))

