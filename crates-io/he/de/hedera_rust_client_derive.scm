(define-module (crates-io he de hedera_rust_client_derive) #:use-module (crates-io))

(define-public crate-hedera_rust_client_derive-0.1.0 (c (n "hedera_rust_client_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yb1l94w1z6rk3sdqi9j1vbdn25ww0gkcsdvzm5bwhjnspy2q8dw")))

