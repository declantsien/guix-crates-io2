(define-module (crates-io he lv helvetia_client) #:use-module (crates-io))

(define-public crate-helvetia_client-0.1.0 (c (n "helvetia_client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "httpmock") (r "^0.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net"))) (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "05dixpvq2x51dvgpxbjqm6fx983vph1j0blajvcjdz6h40dl7ayw")))

(define-public crate-helvetia_client-0.1.1 (c (n "helvetia_client") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "httpmock") (r "^0.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net"))) (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1b3z8148phmqbzd69im76xsc1piixskcn9cp5lfa43hlxlfkr1k8")))

