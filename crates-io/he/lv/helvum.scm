(define-module (crates-io he lv helvum) #:use-module (crates-io))

(define-public crate-helvum-0.1.0 (c (n "helvum") (v "0.1.0") (h "1fjjhqh9667annqv4g2wg1c3gq6iys85r7r3lbmypagm4kqj1kn2") (y #t)))

(define-public crate-helvum-0.2.1 (c (n "helvum") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "gtk") (r "^0.1") (d #t) (k 0) (p "gtk4")) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "pipewire") (r "^0.4") (d #t) (k 0)))) (h "1g83k18ywwhc1nczpysxvb5n4y4pdm94z9xcj2hqrz3i2w8886gg")))

(define-public crate-helvum-0.3.0 (c (n "helvum") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "gtk") (r "^0.2") (d #t) (k 0) (p "gtk4")) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "pipewire") (r "^0.4") (d #t) (k 0)))) (h "176hph99r1g7if0piykrpna28saj97h43pc173pz667fzj0krx7r")))

(define-public crate-helvum-0.3.1 (c (n "helvum") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3") (d #t) (k 0) (p "gtk4")) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "pipewire") (r "^0.4") (d #t) (k 0)))) (h "05v80jsbnrp07wc7y7f4hgz8rx1nw0cyfvznlfjpnadmaggl2pyd")))

(define-public crate-helvum-0.3.2 (c (n "helvum") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "gtk") (r "^0.3") (d #t) (k 0) (p "gtk4")) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "pipewire") (r "^0.4") (d #t) (k 0)))) (h "1wnp05kcmv2i69rlp0i9k049msaja41mwvfyw1cj23fcghscmdc5") (r "1.56")))

(define-public crate-helvum-0.3.3 (c (n "helvum") (v "0.3.3") (d (list (d (n "glib") (r "^0.15.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.4.1") (d #t) (k 0) (p "gtk4")) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "pipewire") (r "^0.4") (d #t) (k 0)))) (h "1iq1p6jvcfb57wwgqil5bbi0j7p2ng6ph2vy7ws6nwk3gsf3h8qx") (r "1.56")))

(define-public crate-helvum-0.3.4 (c (n "helvum") (v "0.3.4") (d (list (d (n "glib") (r "^0.15.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.4.1") (d #t) (k 0) (p "gtk4")) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "pipewire") (r "^0.4") (d #t) (k 0)))) (h "1qshpwahqwgjwfawfxk2mizc4222dwh4h0r0llsl8i397k3av41w") (r "1.56")))

(define-public crate-helvum-0.4.0 (c (n "helvum") (v "0.4.0") (d (list (d (n "glib") (r "^0.17") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.6") (d #t) (k 0) (p "gtk4")) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "pipewire") (r "^0.6") (d #t) (k 0)))) (h "1x9k22rawwqxi0141g0m0550jvkbssdapcc2pcnaxlydkh43fmw3") (r "1.56")))

(define-public crate-helvum-0.4.99 (c (n "helvum") (v "0.4.99") (d (list (d (n "glib") (r "^0.18") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.7") (d #t) (k 0) (p "gtk4")) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "pipewire") (r "^0.7") (d #t) (k 0)))) (h "00bph64n9dnvgji8ibk4icv0yscibyca80sii1yczkp6x1m9rd2d") (r "1.70")))

