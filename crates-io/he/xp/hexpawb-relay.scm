(define-module (crates-io he xp hexpawb-relay) #:use-module (crates-io))

(define-public crate-hexpawb-relay-0.1.1 (c (n "hexpawb-relay") (v "0.1.1") (h "0m8k2v35v7f87093imsdjn36snx5n9bp7fdjy702dl4vxlf8wbn8")))

(define-public crate-hexpawb-relay-0.1.2 (c (n "hexpawb-relay") (v "0.1.2") (h "1ji3hj9vsydxif5nhf24lcl73cjp50207pxwxhjxhv35jlp1vgp2")))

