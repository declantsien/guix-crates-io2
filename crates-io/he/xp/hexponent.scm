(define-module (crates-io he xp hexponent) #:use-module (crates-io))

(define-public crate-hexponent-0.1.0 (c (n "hexponent") (v "0.1.0") (h "1bn4gw5l2x03cq73gmkjj0pmb29hfrnkic9pndawq0zn6migg8l6")))

(define-public crate-hexponent-0.2.0 (c (n "hexponent") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1265ckwwfv2cqsd1xjqa0vxffgl2hch16gikva14n9jb2sjsbla6")))

(define-public crate-hexponent-0.2.1 (c (n "hexponent") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "13hmjh0b8g2r9m4cjy9qj4anrpi0hz9l869gnps4icn2lylyyh6n")))

(define-public crate-hexponent-0.2.2 (c (n "hexponent") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1hvv4j3n3m25db8g60mzxnx2cjpqn3610shc49iq8y8mlh4yfyix")))

(define-public crate-hexponent-0.2.3 (c (n "hexponent") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1rcw3jlzr8s2sp4zsi4171d5igia48p42mpjij9ffrrvi93mcmv3") (f (quote (("std") ("default" "std"))))))

(define-public crate-hexponent-0.3.0 (c (n "hexponent") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "06hrzsqnv3cdbyqxgyc7aka1lr6f9q44skmsf3g393qy2vkpw15c") (f (quote (("std") ("default" "std"))))))

(define-public crate-hexponent-0.3.1 (c (n "hexponent") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0g8ds2gv53d9f2ggwly55vl1qdnfvcbrgpjlldxi1jr74nqjwj07") (f (quote (("std") ("default" "std"))))))

