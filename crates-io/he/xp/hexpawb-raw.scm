(define-module (crates-io he xp hexpawb-raw) #:use-module (crates-io))

(define-public crate-hexpawb-raw-0.0.1 (c (n "hexpawb-raw") (v "0.0.1") (h "1jgwq62fx8jyffqilzg0386mffbk7hgflw2nrgln1i08ic9yy3jr")))

(define-public crate-hexpawb-raw-0.1.1 (c (n "hexpawb-raw") (v "0.1.1") (h "1r5176ckzqwzkmj71qz2qyphlvrhgnz1s35bvcpc9vd0mxw8kkps")))

(define-public crate-hexpawb-raw-99.99.99 (c (n "hexpawb-raw") (v "99.99.99") (h "1cz29927n8n2illsswqb4fb22rrvb0qrcrnwr8dy3fkwygjav8nb")))

