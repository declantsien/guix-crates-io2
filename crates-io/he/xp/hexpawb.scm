(define-module (crates-io he xp hexpawb) #:use-module (crates-io))

(define-public crate-hexpawb-0.1.0 (c (n "hexpawb") (v "0.1.0") (h "0cwq6sj5x0akhmyqa9l12qkr87dc7cbgx1q0xrdp1rpcr149yjlw")))

(define-public crate-hexpawb-0.0.1 (c (n "hexpawb") (v "0.0.1") (h "0j5xmq3gnrwmkrda7d0fksxcbsp00z5rvxcrw60y4izkadpfg7v9")))

(define-public crate-hexpawb-0.1.1 (c (n "hexpawb") (v "0.1.1") (h "06q258vh4g93371fqw4dkmd0zcixwdpp4m17ar941g2k0712xfay")))

(define-public crate-hexpawb-0.1.2 (c (n "hexpawb") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "15j884p53bznprg81kf8malv6haxyl2xwj5699qv5xvwy8ck0sw4") (f (quote (("dangerous-low-level-bits"))))))

