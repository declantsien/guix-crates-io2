(define-module (crates-io he pt heptgen) #:use-module (crates-io))

(define-public crate-heptgen-0.1.0 (c (n "heptgen") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.17") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1a1xc9npll6z0sm3s506lz4rbyrsw35qrr8q1d4mmg0hk22vvgim")))

