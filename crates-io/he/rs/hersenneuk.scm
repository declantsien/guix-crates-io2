(define-module (crates-io he rs hersenneuk) #:use-module (crates-io))

(define-public crate-hersenneuk-0.1.0 (c (n "hersenneuk") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1w5hipp9aqjrdqxidx1mk0l4k1l71np40nkaikpbm365nj0yyyzh")))

(define-public crate-hersenneuk-0.2.0 (c (n "hersenneuk") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1f8f4dikhcksanw81kfmn9y8slf5q0awrr3qkq7p8npkrwph68rl")))

(define-public crate-hersenneuk-0.2.1 (c (n "hersenneuk") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0jwg9n9war6gngk147hgydhwj1jprwcsbpxnjx9pvrr0j257wl3h")))

(define-public crate-hersenneuk-0.2.2 (c (n "hersenneuk") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "08dck1c9lyk1w7khqm5lxyj6r2f1ijv8ix47a9bz7jlc8qwwj4w2")))

(define-public crate-hersenneuk-0.2.3 (c (n "hersenneuk") (v "0.2.3") (d (list (d (n "clap") (r "^4.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1llwf0b1vh34f3my0a3jsxvkmrgy0jwfghq90n89jfgaqjhivgrz")))

