(define-module (crates-io he rs hershey) #:use-module (crates-io))

(define-public crate-hershey-0.1.0 (c (n "hershey") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "127akaxp4cfx0nzih38nkjv7vnhwpcn1mr3xahrp3v70mj3jn7cb")))

(define-public crate-hershey-0.1.1 (c (n "hershey") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r62vh2wf18wi71j96518gayb8b4c01sakxl080cikyjjdlq4s6f")))

