(define-module (crates-io he du hedu) #:use-module (crates-io))

(define-public crate-hedu-0.1.0 (c (n "hedu") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive" "help"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.0.2") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.1") (d #t) (k 0)) (d (n "libpt") (r "^0.3.11") (f (quote ("log" "bintols"))) (d #t) (k 0)))) (h "1bz12dl2z9ha09hlxr6705w9ghdn4097dwms2n5d1zh352dsz1d1")))

(define-public crate-hedu-0.1.1 (c (n "hedu") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "help"))) (d #t) (k 0)) (d (n "clap-num") (r "^1.0.2") (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.1") (d #t) (k 0)) (d (n "libpt") (r "^0.3.11") (f (quote ("log" "bintols"))) (d #t) (k 0)))) (h "1g9ivkra4jb6kp5nsq1hd9v7gkhqlanrpdr441ccv6a31v0r9pml")))

(define-public crate-hedu-0.2.0 (c (n "hedu") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive" "help"))) (d #t) (k 0)) (d (n "libpt") (r "^0.5.1") (f (quote ("bintols" "log"))) (k 0)))) (h "0jfkkbb0gvm87b0xdi1lqi7i0dbssarxa05hb13yzqyd7mrzqhs6")))

