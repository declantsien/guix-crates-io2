(define-module (crates-io he xy hexy) #:use-module (crates-io))

(define-public crate-hexy-0.1.0 (c (n "hexy") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "0cm0n6gb6d5zbq1mmzwkhb6mh44rx86qyfzhxjf1q8m13iirf20y")))

(define-public crate-hexy-0.1.1 (c (n "hexy") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "1bvmkgzrng3bngn3y1j2vrgcadi7q11a9wwsbsh2gs2w544f8mk1")))

(define-public crate-hexy-0.1.2 (c (n "hexy") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "0zcws7y26ila4qcj7dmmvvprqfrbdki1ab0gqcvhcbpncxhjkk8r")))

(define-public crate-hexy-0.1.3 (c (n "hexy") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "1qrva931pc85242ab2qjrqpbhs7nk06a7n32jr542rfadhr2nn92")))

(define-public crate-hexy-0.1.4 (c (n "hexy") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "1fqwn9xddw8jqwmil6cyilrwwd7c7khvfdn3yzyjx3sdlbj3dzh6")))

