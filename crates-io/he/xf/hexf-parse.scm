(define-module (crates-io he xf hexf-parse) #:use-module (crates-io))

(define-public crate-hexf-parse-0.1.0 (c (n "hexf-parse") (v "0.1.0") (h "1b2h0lvksn8748764x46729ygpz8grack24spin0k29ssmr6yabr")))

(define-public crate-hexf-parse-0.2.0 (c (n "hexf-parse") (v "0.2.0") (h "18lpcm4qq8kjpxihc1cipjiy1wbaa0qazdjj4624c4jgczlp4bkw")))

(define-public crate-hexf-parse-0.2.1 (c (n "hexf-parse") (v "0.2.1") (h "1pr3a3sk66ddxdyxdxac7q6qaqjcn28v0njy22ghdpfn78l8d9nz")))

