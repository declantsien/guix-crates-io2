(define-module (crates-io he xf hexfloat2) #:use-module (crates-io))

(define-public crate-hexfloat2-0.1.0 (c (n "hexfloat2") (v "0.1.0") (h "1smbvvjichqghjyby819hlxn6cb05lkc5dym05icx62s41yv3dm9") (r "1.70")))

(define-public crate-hexfloat2-0.1.1 (c (n "hexfloat2") (v "0.1.1") (h "08p6rn0wsxqgk8mca03v9j2wq3q7ihm810id7k13xm79l2mr7n48") (r "1.67")))

(define-public crate-hexfloat2-0.1.2 (c (n "hexfloat2") (v "0.1.2") (h "1gc2qx9d6pz0hl7sdgz77lg65h94f915k35ivjxlsdjimy9sknsz") (r "1.67")))

(define-public crate-hexfloat2-0.1.3 (c (n "hexfloat2") (v "0.1.3") (h "134znh5pjgh798r6xdz2pxpqb38rqc71mlp0yv6l200998b6bzmy") (r "1.67")))

