(define-module (crates-io he xf hexf) #:use-module (crates-io))

(define-public crate-hexf-0.1.0 (c (n "hexf") (v "0.1.0") (d (list (d (n "hexf-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "hexf-parse") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3.3") (d #t) (k 0)))) (h "0f3mgrfzswbg83nfwpklamqvzi2rmnplf9jk6qd7ff6q6b656ip5")))

(define-public crate-hexf-0.2.0 (c (n "hexf") (v "0.2.0") (d (list (d (n "hexf-parse") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1pd0md9xdrj6kanj6sqvnjps3zdij9cpi1n6pkhi4sjn3qlr9rcj")))

(define-public crate-hexf-0.2.1 (c (n "hexf") (v "0.2.1") (d (list (d (n "hexf-parse") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0zl2pk9n995xj0iq7jsr46aslcgsipa1iar6a7drszfd1maz8636")))

