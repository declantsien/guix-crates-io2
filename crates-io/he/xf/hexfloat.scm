(define-module (crates-io he xf hexfloat) #:use-module (crates-io))

(define-public crate-hexfloat-0.0.1 (c (n "hexfloat") (v "0.0.1") (h "0i8rq4lk28ypny3nza84wbv7ahy0104qrpy21p1vdrdrilzbbdyg")))

(define-public crate-hexfloat-0.0.2 (c (n "hexfloat") (v "0.0.2") (h "1npgr5y1yjwlw0r10wrm16dmqiawv0ildmmm1ibapnjyav2h6z6i")))

(define-public crate-hexfloat-0.0.3 (c (n "hexfloat") (v "0.0.3") (h "0nw2rsgn25mfqbyg5fknsclxbcy0ld5v8vqlpy91mhdcaqa8p8j2")))

(define-public crate-hexfloat-0.1.0 (c (n "hexfloat") (v "0.1.0") (h "08v2frkx2imiyckand569xa5zr3ym5bfajmihnjq3ilhimr78agm")))

(define-public crate-hexfloat-0.1.1 (c (n "hexfloat") (v "0.1.1") (h "1vpr2rxgz4n5kf75da63pa09v85jz61ishk1yk3m7rlwjil6bbvb")))

(define-public crate-hexfloat-0.0.4 (c (n "hexfloat") (v "0.0.4") (h "0gnlzk1ks1fabfsnh3xn9lbzhx9b7civ1hzblz68vpbys6008j6n") (y #t)))

(define-public crate-hexfloat-0.1.2 (c (n "hexfloat") (v "0.1.2") (h "1g269b85ydjznaa9dyhfgnrxq3kwqv2a94d1pvia1n371p91a75k")))

(define-public crate-hexfloat-0.1.3 (c (n "hexfloat") (v "0.1.3") (h "1aq3f99qsns7bfdh1hhkcyfkvm4s4rg6ddpigx8jjn3yd8mzc29a")))

