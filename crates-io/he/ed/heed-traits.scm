(define-module (crates-io he ed heed-traits) #:use-module (crates-io))

(define-public crate-heed-traits-0.7.0 (c (n "heed-traits") (v "0.7.0") (h "0b07zkanmzz7ds4k1sfylpxi2ggf4zmfcs3braqbslby18kgca5k")))

(define-public crate-heed-traits-0.8.0 (c (n "heed-traits") (v "0.8.0") (h "18p12338m4k6rl0qs7dcskqczylsqcvf3pzzhdz42q7xnbjr8fm5")))

(define-public crate-heed-traits-0.20.0-alpha.0 (c (n "heed-traits") (v "0.20.0-alpha.0") (h "1mdcqbilx00vz27b34caw322azfzxd55k3v9qdssa04s3v897kq0")))

(define-public crate-heed-traits-0.20.0-alpha.1 (c (n "heed-traits") (v "0.20.0-alpha.1") (h "177f292j6nrskksw0sxk1k8yn43y7iwgy3drf1vjrpa4sw2dab9x")))

(define-public crate-heed-traits-0.20.0-alpha.2 (c (n "heed-traits") (v "0.20.0-alpha.2") (h "1g1w1h9iif92caxhzvz800j122hbjcl0rqkw3233pr59qgp2gf2r")))

(define-public crate-heed-traits-0.20.0-alpha.3 (c (n "heed-traits") (v "0.20.0-alpha.3") (h "07zssri7ma68ij0lfhzhivbq7858wigh8n8hcxc2pdlz0inmw1a4")))

(define-public crate-heed-traits-0.20.0-alpha.4 (c (n "heed-traits") (v "0.20.0-alpha.4") (h "0ckqw750yy834ngjl7zp7hph6bpc5jxm6f7bd5ckdc22m9dxrm1l")))

(define-public crate-heed-traits-0.20.0-alpha.5 (c (n "heed-traits") (v "0.20.0-alpha.5") (h "0acil5cyicqvmb2r9rj5a02r70v44shqxlqf5rr3wxmr0w0rs816")))

(define-public crate-heed-traits-0.20.0-alpha.6 (c (n "heed-traits") (v "0.20.0-alpha.6") (h "1lmajapx21rylpz4yqradkp9bx8k6m66crhzc2idz1ag5wql2mr7")))

(define-public crate-heed-traits-0.20.0-alpha.7 (c (n "heed-traits") (v "0.20.0-alpha.7") (h "1cxajkkrnyjjapjjkhgf1f47g3i0ga2vs1dvc3c9cxjy7mfvqx5x")))

(define-public crate-heed-traits-0.20.0-alpha.8 (c (n "heed-traits") (v "0.20.0-alpha.8") (h "00ma12dj8302s91dz8xibcmr24zkgyxm84ksyd9dw0g24nqbm3i4")))

(define-public crate-heed-traits-0.20.0-alpha.9 (c (n "heed-traits") (v "0.20.0-alpha.9") (h "16424zvlpbclp39pnsmzd1qpznc9vj3y94p6vlvassg9rpcvgc2s")))

(define-public crate-heed-traits-0.20.0 (c (n "heed-traits") (v "0.20.0") (h "1zvlxz1jf7zwzklr2accgvggrs4n6s81mihsbb75fk20il230cgb")))

