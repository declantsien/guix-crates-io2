(define-module (crates-io he rt hertz) #:use-module (crates-io))

(define-public crate-hertz-0.1.0 (c (n "hertz") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1jf4rbxj5lcp016gf080dw1h78z3gyf3cn27nxbn4jinml5m681k")))

(define-public crate-hertz-0.2.0 (c (n "hertz") (v "0.2.0") (d (list (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "08y037xi98bajyn7jfw4ps8i8ffi2hr03fj31y43sai73yg75cw3")))

(define-public crate-hertz-0.3.0 (c (n "hertz") (v "0.3.0") (h "134iqgv2dkyy29qx5kwjf2b2zx3imvjpb4cjn79k4hqn9s52ymwf")))

