(define-module (crates-io he rt herta-lib) #:use-module (crates-io))

(define-public crate-herta-lib-1.0.0 (c (n "herta-lib") (v "1.0.0") (d (list (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0bw5d252zpykrad0i6iagw6gssrjlwasr4hqmcx0vb4fnlmf1w51")))

(define-public crate-herta-lib-1.0.1 (c (n "herta-lib") (v "1.0.1") (d (list (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0xawcq2r1ibi03yzppdrdvb97h475fniswi6cx3akp10xxax3sab")))

(define-public crate-herta-lib-1.0.2 (c (n "herta-lib") (v "1.0.2") (d (list (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0zc9gywmr0q316ii5677dccz4kplqglxfc7p5dl7xjzqm1if3rdy") (y #t)))

(define-public crate-herta-lib-1.0.3 (c (n "herta-lib") (v "1.0.3") (d (list (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1qldvqlbdaz269q73njxb0fqk6fa0rl011wma3psmahkrjy8zgz4")))

(define-public crate-herta-lib-1.0.4 (c (n "herta-lib") (v "1.0.4") (d (list (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0rvh649hprw06yij3rjcmq51y0006l98km9mn2xybpr0n9fylckz")))

(define-public crate-herta-lib-1.0.5 (c (n "herta-lib") (v "1.0.5") (d (list (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0hqcf2lwlg2nxh1j80pk3q958avwn6dgxh35z8x7hg7vhrxcdrnw")))

(define-public crate-herta-lib-1.1.6 (c (n "herta-lib") (v "1.1.6") (d (list (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "0gnk7c5wigxfj43yqj5ig6rc388q0zriq0jzc4wv4qlbbzv8gfsz")))

(define-public crate-herta-lib-1.1.7 (c (n "herta-lib") (v "1.1.7") (d (list (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1v1a954q4k84g32alh055n6s0c5vx197j8rv8x6079bv78q86jzz")))

(define-public crate-herta-lib-1.1.8 (c (n "herta-lib") (v "1.1.8") (d (list (d (n "platform-dirs") (r "^0.3.0") (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1b2p3qf59z933bi6dgsaczm2zdjya9yrl93q5fzzfi10b3sv5gln")))

