(define-module (crates-io he so hesoyam) #:use-module (crates-io))

(define-public crate-hesoyam-0.1.0 (c (n "hesoyam") (v "0.1.0") (d (list (d (n "hesoyam_core") (r "~0.1.0") (d #t) (k 0)) (d (n "hesoyam_macro") (r "~0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1nf31zpwk4j2zmf4cgxashmm9yz8bjjr162q2ks5lr6qkwsi9ash")))

(define-public crate-hesoyam-0.1.1 (c (n "hesoyam") (v "0.1.1") (d (list (d (n "hesoyam_core") (r "~0.1.1") (d #t) (k 0)) (d (n "hesoyam_macro") (r "~0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "152zx4hvi3zg48aaczs9cqlgv9ppln9p94gln966xwny13dgmnj9")))

(define-public crate-hesoyam-0.1.2 (c (n "hesoyam") (v "0.1.2") (d (list (d (n "hesoyam_core") (r "~0.1.2") (d #t) (k 0)) (d (n "hesoyam_macro") (r "~0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1v7gbw5rm4qy2pm0m7hpvyvjzklj7wzwlgaqv52325pc8yy82sna")))

(define-public crate-hesoyam-0.1.3 (c (n "hesoyam") (v "0.1.3") (d (list (d (n "hesoyam_core") (r "~0.1.3") (d #t) (k 0)) (d (n "hesoyam_macro") (r "~0.1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0p605ynbakp0w0w25zv5g310f0526wvrm3s9q9yl87q5nzaqjkmx")))

(define-public crate-hesoyam-0.1.4 (c (n "hesoyam") (v "0.1.4") (d (list (d (n "hesoyam_core") (r "~0.1.4") (d #t) (k 0)) (d (n "hesoyam_macro") (r "~0.1.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1hjg9kgwvl7alpr93is49vlicvzxwy3nnghymbpvply2790hq5d9")))

(define-public crate-hesoyam-0.1.5 (c (n "hesoyam") (v "0.1.5") (d (list (d (n "hesoyam_core") (r "~0.1.5") (d #t) (k 0)) (d (n "hesoyam_macro") (r "~0.1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "115ckdhfbaglfwf0i1nb24jmvqrk4rvrqgh1blqpjrjhaczkns8d")))

(define-public crate-hesoyam-0.1.6 (c (n "hesoyam") (v "0.1.6") (d (list (d (n "hesoyam_core") (r "~0.1.6") (d #t) (k 0)) (d (n "hesoyam_macro") (r "~0.1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0pniaxkxqh1y9if0n8zj426iq8k0lcjrcx2zcr4l3km8nqi6my35")))

