(define-module (crates-io he so hesoyam_macro) #:use-module (crates-io))

(define-public crate-hesoyam_macro-0.1.0 (c (n "hesoyam_macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "hesoyam_core") (r "~0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0gk0rs9ywncghf1cr64dajw6r4h8pni5j4nxa6qpgbda5bdi813d")))

(define-public crate-hesoyam_macro-0.1.1 (c (n "hesoyam_macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "hesoyam_core") (r "~0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "0qv3p6whxi7jvndmhcmc8q9f4r2blcz06nvkxg49zvpir90xvdjf")))

(define-public crate-hesoyam_macro-0.1.2 (c (n "hesoyam_macro") (v "0.1.2") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "hesoyam_core") (r "~0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1xkkfmnr7sw74s4ng3645v104vlyy0z9hjady43d2qnfkqlhl6q4")))

(define-public crate-hesoyam_macro-0.1.3 (c (n "hesoyam_macro") (v "0.1.3") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "hesoyam_core") (r "~0.1.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "109chy76c4s6f43c7sscfrpwn9qdv651kal7cgd2vx3mn1igb80h")))

(define-public crate-hesoyam_macro-0.1.4 (c (n "hesoyam_macro") (v "0.1.4") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "hesoyam_core") (r "~0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "09jscdnmplqvfvizhp5lfvqfd8zlz1x6wbrwzkh95mpn7m0vlnll")))

(define-public crate-hesoyam_macro-0.1.5 (c (n "hesoyam_macro") (v "0.1.5") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "hesoyam_core") (r "~0.1.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "1bf709112lh89i1b5s9ha48zkav1j2yrvb3gj0hab9iynzw67phm")))

(define-public crate-hesoyam_macro-0.1.6 (c (n "hesoyam_macro") (v "0.1.6") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "hesoyam_core") (r "~0.1.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("full"))) (d #t) (k 0)))) (h "061far9wnjdqhl9fvlbkfhnrlz4dw0dbdbxnd6xyigcvq9i4gy03")))

