(define-module (crates-io he ft hefty) #:use-module (crates-io))

(define-public crate-hefty-0.1.0 (c (n "hefty") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "typle") (r "^0.2.1") (d #t) (k 0)))) (h "1m2n17vazih0vh8frjxn2z6mdqzzfvmy4m467q9vsrydi2hvip8x")))

(define-public crate-hefty-0.2.0 (c (n "hefty") (v "0.2.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "typle") (r "^0.2.3") (d #t) (k 0)))) (h "1gsaamlmfr0j94fi712hf2flbmfilq8khgs0mj3xf2xqvvrygp1q")))

(define-public crate-hefty-0.2.1 (c (n "hefty") (v "0.2.1") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "typle") (r "^0.2.3") (d #t) (k 0)))) (h "06gzxg9jzjms3dfi5bmw6v77lw0q1q08q6asg7gwcgizpxnd3yrm") (r "1.65")))

(define-public crate-hefty-0.3.0 (c (n "hefty") (v "0.3.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "typle") (r "^0.4") (d #t) (k 0)))) (h "1cznqni0zfl3lv3v30m6xqqhppnzv6kxbhwq77pi44yvsxcxd5hp") (r "1.65")))

(define-public crate-hefty-0.3.1 (c (n "hefty") (v "0.3.1") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "typle") (r "^0.10.3") (d #t) (k 0)))) (h "070axz4h6plvk2vsi3n9ccxxyd3az284pg8vvpwr4g3nmh38pdxv") (r "1.65")))

