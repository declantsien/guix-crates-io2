(define-module (crates-io he is heisspot) #:use-module (crates-io))

(define-public crate-heisspot-0.1.0 (c (n "heisspot") (v "0.1.0") (d (list (d (n "get-last-error") (r "^0.1.1") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_NetworkManagement_WiFi" "Networking_Connectivity" "Foundation_Collections" "Devices_WiFi" "Foundation"))) (d #t) (k 0)))) (h "0zwzqa393wd8kwr41i29nlf80qvfscrg7cngi54p9vp2f1j8jrcz")))

