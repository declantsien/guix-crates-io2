(define-module (crates-io he ad headers-derive) #:use-module (crates-io))

(define-public crate-headers-derive-0.0.0 (c (n "headers-derive") (v "0.0.0") (h "0axs2cn6q7yynpbrydxbx1fpj2xbk0w7csc1pxav9ajlva7iynbi")))

(define-public crate-headers-derive-0.0.1 (c (n "headers-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1jingw932jgc1jrsm8piy4f5dr2pm82mm01pkj63p2g3wndm1f4k")))

(define-public crate-headers-derive-0.0.2 (c (n "headers-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1y3v85ihc21hdis8zb2r8airan0lfirgjviy9yrd5bzh1kblzppd")))

(define-public crate-headers-derive-0.1.0 (c (n "headers-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1aa4sj56xl4lm4prlfvv331g4h2cwqnx3y6xd04lzjkb0vl65i4p")))

(define-public crate-headers-derive-0.1.1 (c (n "headers-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "04csmj8h9v3zm8qinkdqr58k8wk8qzy16l13l35wpqw5qh0g6g7k")))

