(define-module (crates-io he ad head-tail-iter) #:use-module (crates-io))

(define-public crate-head-tail-iter-0.1.0 (c (n "head-tail-iter") (v "0.1.0") (h "0wr2jcn6qgqhvzsaimji1lgs2qml16yaxkrhrrm4ya8cr94x4j09") (y #t) (r "1.54")))

(define-public crate-head-tail-iter-1.0.0 (c (n "head-tail-iter") (v "1.0.0") (h "03kj46ymdp8dhvx808f8s9h20cb994ciw3n44f1wv5f0vrp09hcv") (r "1.54")))

(define-public crate-head-tail-iter-1.0.1 (c (n "head-tail-iter") (v "1.0.1") (h "19b2mbiwi0ln7xynbpcaj76384d3916m26shx0izl028ryp5gkaj") (r "1.54")))

