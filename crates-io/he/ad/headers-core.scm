(define-module (crates-io he ad headers-core) #:use-module (crates-io))

(define-public crate-headers-core-0.0.0 (c (n "headers-core") (v "0.0.0") (h "1094w651p2hiwc4lmkh8jqv3w7196av8illd8l39sys5inqzgf73")))

(define-public crate-headers-core-0.0.1 (c (n "headers-core") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "0wwhhgfkkm1i882qwv9dslvv1xspz1f72gyl0qm83yx5lfyc753p")))

(define-public crate-headers-core-0.0.2 (c (n "headers-core") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1.13") (d #t) (k 0)))) (h "0qwv3n7rsj2b4hzxmwnph9b2lbkz02wky81iamlyr33agisxwxxi")))

(define-public crate-headers-core-0.1.0 (c (n "headers-core") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1.15") (d #t) (k 0)))) (h "0aj7xmk3yq8xlnqjxlvb82gaws087g9j2fvksbhrwm8pah5mpbji")))

(define-public crate-headers-core-0.1.1 (c (n "headers-core") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.1.15") (d #t) (k 0)))) (h "0ds20kg0igncs2r0jrcf26mq72k3j6ilanr0qwh7r7xak8kk2wcn")))

(define-public crate-headers-core-0.2.0 (c (n "headers-core") (v "0.2.0") (d (list (d (n "http") (r "^0.2.0") (d #t) (k 0)))) (h "0ab469xfpd411mc3dhmjhmzrhqikzyj8a17jn5bkj9zfpy0n9xp7")))

(define-public crate-headers-core-0.3.0 (c (n "headers-core") (v "0.3.0") (d (list (d (n "http") (r "^1.0.0") (d #t) (k 0)))) (h "1r1w80i2bhmyh8s5mjr2dz6baqlrm6cak6yvzm4jq96lacjs5d2l")))

