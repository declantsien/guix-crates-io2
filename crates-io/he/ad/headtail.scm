(define-module (crates-io he ad headtail) #:use-module (crates-io))

(define-public crate-headtail-0.1.0 (c (n "headtail") (v "0.1.0") (h "0h9fh7pnrjhhlaim8bbhyl1085wf2i3gsadldzg6fl6yrppjhf14")))

(define-public crate-headtail-0.2.0 (c (n "headtail") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n266rawadm35zna8wf5jz86mm10q3rw0k30fcg363mmwnms10yg")))

(define-public crate-headtail-0.3.0 (c (n "headtail") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f003x9mrb2d6iyi8gzqmwxbjn96g75mjvq350p6jc4vx4m0c4r8")))

(define-public crate-headtail-0.3.1 (c (n "headtail") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "06lk4p49l1wiipdgrv4b22xk6g048mxrvv0jwpc1b8r63zg2vcr5")))

(define-public crate-headtail-0.3.2 (c (n "headtail") (v "0.3.2") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0yr1b8k2kcnc2k327hfmy1g7j1993f9qyrx42l6m59cjmain89kn")))

