(define-module (crates-io he ad header-vec) #:use-module (crates-io))

(define-public crate-header-vec-0.1.0 (c (n "header-vec") (v "0.1.0") (h "0c72a8n7np81k0r8cwsy8gggkw46r4i878nllr5q4rfk8njcijrx")))

(define-public crate-header-vec-0.1.1 (c (n "header-vec") (v "0.1.1") (h "0kivi185dpdm1aa2fsnwcc96bg5d7akwcdp5dpjrsbvrnfcx4xhc")))

(define-public crate-header-vec-0.1.2 (c (n "header-vec") (v "0.1.2") (h "0vzh711nsqbd69hzhdyp13dxxw2djcna6d2f4v24260k69nyd9xx")))

