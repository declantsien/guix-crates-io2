(define-module (crates-io he ad headj) #:use-module (crates-io))

(define-public crate-headj-0.1.0 (c (n "headj") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "json-event-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)))) (h "0iadqgrv9nkf0a18zpl8i53j5z2fxhqcpzg1ibchdrwk6xkk2if4")))

(define-public crate-headj-0.1.1 (c (n "headj") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "json-event-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)))) (h "1x16nxvb2kails0b0ap5l93hqbgk2ninj88wy2dcnv13v046j22r")))

