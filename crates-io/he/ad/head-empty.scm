(define-module (crates-io he ad head-empty) #:use-module (crates-io))

(define-public crate-head-empty-0.1.0 (c (n "head-empty") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "linkme") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0mcj1dk1p3rwf542wdimhlnj0xg0z5lvnsmjm33bjmbw3f9vmwnz") (f (quote (("internal-doc-hidden") ("default" "internal-doc-hidden"))))))

