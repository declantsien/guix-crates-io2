(define-module (crates-io he ad headhunter-bindings) #:use-module (crates-io))

(define-public crate-headhunter-bindings-0.1.0 (c (n "headhunter-bindings") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.32.0-rc.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0adbwy0p97prcvdpvsn6mfr68jnlqzzazpkflgj6qqq22q2znadw")))

