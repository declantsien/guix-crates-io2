(define-module (crates-io he ad headers-client-ip) #:use-module (crates-io))

(define-public crate-headers-client-ip-0.1.0 (c (n "headers-client-ip") (v "0.1.0") (d (list (d (n "headers") (r "^0.3.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "04qhd9m9s0jkvc23m07iny1p7z8qzq2c7nf0qbwbyszgsz2pmpw6")))

