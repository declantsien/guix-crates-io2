(define-module (crates-io he ad headless_batch_engine) #:use-module (crates-io))

(define-public crate-headless_batch_engine-0.1.0 (c (n "headless_batch_engine") (v "0.1.0") (d (list (d (n "headless_common") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.155") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0b94cf6g22jiv8y20nn0lx6sf5lnaz2l9bp0j3hrwvd391n4pka3") (r "1.68.0")))

