(define-module (crates-io he ad headers-accept) #:use-module (crates-io))

(define-public crate-headers-accept-0.1.0 (c (n "headers-accept") (v "0.1.0") (d (list (d (n "headers-core") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "mediatype") (r "^0.19.18") (d #t) (k 0)))) (h "17a613l8zhjlff1ggg43j55aml5dha9minq5hbbds4gzzpxkizqa")))

(define-public crate-headers-accept-0.1.1 (c (n "headers-accept") (v "0.1.1") (d (list (d (n "headers-core") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "mediatype") (r "^0.19.18") (d #t) (k 0)))) (h "1pm9ddglins64mlff39x4v8vpcf3ak77fg5f2knn20dpqaqxl1q9")))

(define-public crate-headers-accept-0.1.2 (c (n "headers-accept") (v "0.1.2") (d (list (d (n "headers-core") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "mediatype") (r "^0.19.18") (d #t) (k 0)))) (h "123ypi6hynbjddlimxpxlnsw1kqgr1jgv6l2mihwzzgi02snc3n3")))

(define-public crate-headers-accept-0.1.3 (c (n "headers-accept") (v "0.1.3") (d (list (d (n "headers-core") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "mediatype") (r "^0.19.18") (d #t) (k 0)))) (h "08dcgi75l3l5w5zrla5pnbim86yi7r736psv89llk90mbyhrpb5b")))

