(define-module (crates-io he ad header-slice) #:use-module (crates-io))

(define-public crate-header-slice-0.1.0 (c (n "header-slice") (v "0.1.0") (h "1n3jxk2599b9hq00wjvlvy61hi8wfbc456kzgnjiwwx6jd49c8lp")))

(define-public crate-header-slice-0.1.1 (c (n "header-slice") (v "0.1.1") (h "09fnslqddnq4hmkz1xvmbxz6rig31qfmdncadkpcpyj2kj5kxr1w")))

(define-public crate-header-slice-0.1.2 (c (n "header-slice") (v "0.1.2") (h "1fa5hyi0lm27ah672xdi3x8myylbs9xdxfrlb6w823nanw4a3d72")))

(define-public crate-header-slice-0.1.3 (c (n "header-slice") (v "0.1.3") (h "10axlwpx1n7491ldvw6ijd8vdzvfpfgns7cmls29qakci2yhir91")))

