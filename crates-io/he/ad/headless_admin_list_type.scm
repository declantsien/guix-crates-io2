(define-module (crates-io he ad headless_admin_list_type) #:use-module (crates-io))

(define-public crate-headless_admin_list_type-0.1.0 (c (n "headless_admin_list_type") (v "0.1.0") (d (list (d (n "headless_batch_engine") (r "^0.1.0") (d #t) (k 0)) (d (n "headless_common") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.156") (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1m0nsr1ms9zvaf8086iirx29x1s4rbz3nadxj8gp54r3mvv5ia8q") (r "1.68.0")))

