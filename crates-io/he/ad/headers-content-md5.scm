(define-module (crates-io he ad headers-content-md5) #:use-module (crates-io))

(define-public crate-headers-content-md5-0.1.0 (c (n "headers-content-md5") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "headers") (r "^0.3.8") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 0)))) (h "1aj39c4wapa4437c4yaphpn2a10h5cl64xkyhr8naqg5j5fgh2hp")))

(define-public crate-headers-content-md5-0.1.1 (c (n "headers-content-md5") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "headers") (r "^0.3.8") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 0)))) (h "00fq85vh3lphv345k0x04ggn3ayz31mjx0c6mnimrs0qakg57vpk")))

(define-public crate-headers-content-md5-0.2.0 (c (n "headers-content-md5") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "headers") (r "^0.4.0") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)))) (h "0gniadr8j3dg6zsd505c79fimfwd8hlikngmxvvfbdbikfb4als4")))

