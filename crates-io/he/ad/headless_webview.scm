(define-module (crates-io he ad headless_webview) #:use-module (crates-io))

(define-public crate-headless_webview-0.1.0 (c (n "headless_webview") (v "0.1.0") (d (list (d (n "http") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "02lhm6wvp0fqqh8k8rxqcj6cq7z3awgwfrzr531d5qiw8pdybvz0") (f (quote (("protocol") ("default"))))))

(define-public crate-headless_webview-0.1.1 (c (n "headless_webview") (v "0.1.1") (d (list (d (n "http") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0b2rvh3j2df1cm4rg6501yk3ggq9fv8r2p9jnc8vi817yi6n73vq") (f (quote (("protocol") ("default"))))))

