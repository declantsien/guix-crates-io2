(define-module (crates-io he ad headifier) #:use-module (crates-io))

(define-public crate-headifier-0.1.0 (c (n "headifier") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)))) (h "087ng8837fc9ydh73j26drmrg6d959jbbayq3k2ndgjksvzf0nsn") (y #t)))

(define-public crate-headifier-0.1.1 (c (n "headifier") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)))) (h "0c1m1n9csqqsr2wsy2bk08nq3j719w4765kcy3yk196m7317w1d6") (y #t)))

(define-public crate-headifier-0.1.2 (c (n "headifier") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)))) (h "0n7ry9i6hy4pg8q9v0g5ww5sd4288yya1wb47qk5z1vcbc2plkgj") (y #t)))

(define-public crate-headifier-0.1.3 (c (n "headifier") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.1") (d #t) (k 0)))) (h "02rx4crshxbhp5f44w9i3gs8jzay1h7gp1yclxqvwr2r43snbxmd") (y #t)))

(define-public crate-headifier-0.1.4 (c (n "headifier") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.1") (d #t) (k 0)))) (h "0gmkfjp18yd90qccb923cazxk1n30a50vamlblya1z7di7whwrks") (y #t)))

(define-public crate-headifier-0.2.0 (c (n "headifier") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.1") (d #t) (k 0)))) (h "17vcv18vslyhwdm78c6hs0fy8zg39n8wqz9qljh5bynjqghxs2bc")))

