(define-module (crates-io he ad head-rs) #:use-module (crates-io))

(define-public crate-head-rs-0.1.0 (c (n "head-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0sahp3wp5m0nvhhfh3i3acj4w8xp58plw8fvhi0rqid4q4x6miqz")))

(define-public crate-head-rs-0.2.0 (c (n "head-rs") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hi8rcd2lxhm4q26xvqmcydwc0l3vpdj23nmkr1pqdrfpygpv481")))

