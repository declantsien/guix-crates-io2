(define-module (crates-io he ad headhunter-cli) #:use-module (crates-io))

(define-public crate-headhunter-cli-0.1.0 (c (n "headhunter-cli") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "headhunter-bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("time" "env-filter"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "17p0khspjwik62sd8slrzx05zzjppibdcr2xkl6hzfcqwjxjd7c6")))

