(define-module (crates-io he ad headway) #:use-module (crates-io))

(define-public crate-headway-0.1.0 (c (n "headway") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 2)))) (h "044qzd465srkmj3fhddcnfidr8c2092zv7jfawy57lma5f60gw17")))

(define-public crate-headway-0.1.1 (c (n "headway") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 2)))) (h "0j5xsvx2p7d6g8srdy0z0l9p2bwlybq4lb8f6g0mw423n0vvppwv")))

(define-public crate-headway-0.1.2 (c (n "headway") (v "0.1.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 2)))) (h "0yb8pjprcj1z8wz0mz740di3cq9g5iihksmc9dmcr6sld4b7ji05")))

