(define-module (crates-io he re heredity) #:use-module (crates-io))

(define-public crate-heredity-0.2.0 (c (n "heredity") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "01j016drv3zxdl5ml84kpdv1l7j3g9l25v7hwbxqibwann4x38hn")))

