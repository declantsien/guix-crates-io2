(define-module (crates-io he re here) #:use-module (crates-io))

(define-public crate-here-1.0.0 (c (n "here") (v "1.0.0") (d (list (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "1ld5yjripxw36bvr92dxdk22nw1mdrsa68wrdsps8a6xs252rgj3")))

(define-public crate-here-1.1.0 (c (n "here") (v "1.1.0") (d (list (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "06hlqgbm64qfw2mcsbr10lqam57n791p1l0dyh1m7ds51w9xp7jp") (f (quote (("std") ("off_on_release") ("default" "std" "off_on_release"))))))

(define-public crate-here-1.1.1 (c (n "here") (v "1.1.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1bkf5c5nzqib0saj8drdp4b7p5dkn99r051xhqnxpa5bi0y7j1mf") (f (quote (("std") ("off_on_release") ("default" "std" "off_on_release"))))))

