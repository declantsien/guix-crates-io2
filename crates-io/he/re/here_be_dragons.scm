(define-module (crates-io he re here_be_dragons) #:use-module (crates-io))

(define-public crate-here_be_dragons-0.1.0 (c (n "here_be_dragons") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "188ky8kgim5sli4r8wh95zr1w916nyj1dcn1qgwc5qds04rvg2p6")))

(define-public crate-here_be_dragons-0.1.1 (c (n "here_be_dragons") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0fdygm6yawl6hm2f13cslvfwlc43p8pjix0jyigcvdhr4gv5ly7l")))

(define-public crate-here_be_dragons-0.2.0 (c (n "here_be_dragons") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0na0s9awa1nsf46p11dbqmzm14vvrjwqpazp2vx4y1s9dl4yry2c")))

(define-public crate-here_be_dragons-0.2.1 (c (n "here_be_dragons") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1h0jjsbxkgbvkcd5vp5z40zz5xwqx508lz2xrq1wdh1phfppabxf")))

(define-public crate-here_be_dragons-0.3.0 (c (n "here_be_dragons") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l2i293fxg6qm20y6vbym98zpr6qrcmy13zhb4qrq4qw61d2gv8g")))

