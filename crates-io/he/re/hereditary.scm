(define-module (crates-io he re hereditary) #:use-module (crates-io))

(define-public crate-hereditary-0.1.0 (c (n "hereditary") (v "0.1.0") (d (list (d (n "forwarding") (r "^0.1.0") (d #t) (k 0)) (d (n "trait_info") (r "^0.1.0") (d #t) (k 0)) (d (n "trait_info_gen") (r "^0.1.0") (d #t) (k 2)))) (h "15plhvm429krpbagzkql6968lwn67b9vv3sa5hbi3x5dnv8b0zf1")))

