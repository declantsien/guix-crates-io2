(define-module (crates-io he lm helm-api) #:use-module (crates-io))

(define-public crate-helm-api-0.1.0 (c (n "helm-api") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "grpcio") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "~2") (d #t) (k 0)) (d (n "protoc-grpcio") (r "^1.0") (d #t) (k 1)))) (h "1j1234mampq5dmvn4lqbm8jdrkxbqww2wlym0679x486xa890848")))

