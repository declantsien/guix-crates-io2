(define-module (crates-io he lm helm) #:use-module (crates-io))

(define-public crate-helm-0.0.0 (c (n "helm") (v "0.0.0") (h "1vrcdvbz3v1wrlqzyfj6qn7lmk0z0b8j0r1vkmba7sm73bkr0mvb")))

(define-public crate-helm-0.0.1 (c (n "helm") (v "0.0.1") (h "0acyxddwmsgp13pnn4d1f3bzm5vhs0gx7wh880zxz1xnqfpcz17n")))

