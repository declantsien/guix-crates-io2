(define-module (crates-io he lm helm-build) #:use-module (crates-io))

(define-public crate-helm-build-0.0.0 (c (n "helm-build") (v "0.0.0") (h "1n9n3ygm9k43z8q3w5f5428z4mhhxh38wisvv72iv11b2sz32krw")))

(define-public crate-helm-build-0.0.1 (c (n "helm-build") (v "0.0.1") (h "14swwnza9p0vqd7r4i3d44q4f71w4v8s5598krxarm38rwfhyr0h")))

