(define-module (crates-io he ts hetseq) #:use-module (crates-io))

(define-public crate-hetseq-0.1.0 (c (n "hetseq") (v "0.1.0") (h "065cqyn96cgkj6m1jk03ly2ycg2n9va6cai1p8w8r12d1yf48r12") (f (quote (("nightly"))))))

(define-public crate-hetseq-0.1.1 (c (n "hetseq") (v "0.1.1") (h "0w1l7iv6qwc260j60c0z2x661z9wdicbh9p8axy0sz5fj91lyjbz") (f (quote (("nightly"))))))

(define-public crate-hetseq-0.1.2 (c (n "hetseq") (v "0.1.2") (h "1kgk4rsv3viisy121lxvzv62vp8m2gxfr67vzfmil1yvfli9nyw6") (f (quote (("nightly"))))))

(define-public crate-hetseq-0.1.3 (c (n "hetseq") (v "0.1.3") (d (list (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0ixqcgirv3xj7awblkzbnlxbigb9jfpbbipsa8m9nxnkf8svjhpc") (f (quote (("par_iter" "rayon") ("nightly"))))))

(define-public crate-hetseq-0.1.4 (c (n "hetseq") (v "0.1.4") (d (list (d (n "rayon") (r ">= 0.7") (o #t) (d #t) (k 0)) (d (n "shred") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1hzqhfnkrmflmrw1c1fixmd4ffw1b8z148g1g4z03sz2si3mmgaq") (f (quote (("system_data" "shred") ("par_iter" "rayon") ("nightly"))))))

(define-public crate-hetseq-0.1.5 (c (n "hetseq") (v "0.1.5") (d (list (d (n "rayon") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "shred") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0qshch1ryr5nas36vcyhk9g9yqcr0dv4q8fdnkpyk1ga0fg3pqfv") (f (quote (("system_data" "shred") ("par_iter" "rayon") ("nightly"))))))

(define-public crate-hetseq-0.1.6 (c (n "hetseq") (v "0.1.6") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "shred") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1dddl31yb72qvxfyz0mff4s8rvmz87knsyn83844ri8v5i7sxwsc") (f (quote (("nightly")))) (y #t)))

(define-public crate-hetseq-0.2.0 (c (n "hetseq") (v "0.2.0") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "shred") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1bm6vnwsnyb6fi99ykqi7nasfm3dkgwbgzpbqjcwjpf4k2kyi337") (f (quote (("nightly"))))))

