(define-module (crates-io he va hevayo_first) #:use-module (crates-io))

(define-public crate-hevayo_first-0.1.0 (c (n "hevayo_first") (v "0.1.0") (h "0vzgmdaadn6907wkvhw94iys0rfh17hb2rzw3g9fgsypc7gmrgpy")))

(define-public crate-hevayo_first-0.1.1 (c (n "hevayo_first") (v "0.1.1") (d (list (d (n "hevayo_greetings") (r "^0.1.0") (d #t) (k 0)))) (h "0jlq2lzvkk7mcg4gvhql6wprhzpdmycmw8sh508czn1a1xxag55q")))

