(define-module (crates-io he rm hermit-dtb) #:use-module (crates-io))

(define-public crate-hermit-dtb-0.1.0 (c (n "hermit-dtb") (v "0.1.0") (h "0mv9x8g5afdfir2qnhp6ifpw3pgjljjks0q3zskmspz4lyqn3bl8")))

(define-public crate-hermit-dtb-0.1.1 (c (n "hermit-dtb") (v "0.1.1") (h "1l5qvzp6wc93xczb1sxa7jkb7nfhf8qb6l13h5j9dfd9iwhdxq49")))

