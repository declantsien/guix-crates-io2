(define-module (crates-io he rm hermit-toolkit-utils) #:use-module (crates-io))

(define-public crate-hermit-toolkit-utils-0.1.0 (c (n "hermit-toolkit-utils") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "cosmwasm-storage") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-storage")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "045znpyahz7g7s7pkr0myf7dm3wis94qj753ayhq1sg2xdz6fj72")))

