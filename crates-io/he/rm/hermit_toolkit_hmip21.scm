(define-module (crates-io he rm hermit_toolkit_hmip21) #:use-module (crates-io))

(define-public crate-hermit_toolkit_hmip21-0.1.0 (c (n "hermit_toolkit_hmip21") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "hermit_toolkit_utils") (r "^0.1.0") (d #t) (k 0) (p "hermit-toolkit-utils")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0l6girwnrl8s1bafqa39h5a6irkq110c7g7kgv7vlqmnzx4h7w1g")))

(define-public crate-hermit_toolkit_hmip21-0.1.1 (c (n "hermit_toolkit_hmip21") (v "0.1.1") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "hermit_toolkit_utils") (r "^0.1.0") (d #t) (k 0) (p "hermit-toolkit-utils")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "04r2l9dnp67pg2kbjbin1y943rgasm0lvvcns44k4xja7j6dplv1")))

(define-public crate-hermit_toolkit_hmip21-0.1.2 (c (n "hermit_toolkit_hmip21") (v "0.1.2") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "hermit_toolkit_utils") (r "^0.1.0") (d #t) (k 0) (p "hermit-toolkit-utils")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "11zlbqb0lfck4r6g1pdwwliribla3qp79ln32b09ysv8j6krayz1")))

(define-public crate-hermit_toolkit_hmip21-0.1.3 (c (n "hermit_toolkit_hmip21") (v "0.1.3") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "hermit_toolkit_utils") (r "^0.1.0") (d #t) (k 0) (p "hermit-toolkit-utils")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "14f6ax1k5vybdp5cpdfwgwvrbwf7ajzij4xq4i2vfssn85fjjbw0")))

(define-public crate-hermit_toolkit_hmip21-0.1.5 (c (n "hermit_toolkit_hmip21") (v "0.1.5") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "hermit_toolkit_utils") (r "^0.1.0") (d #t) (k 0) (p "hermit-toolkit-utils")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "16qal9n9mwph1g4h3ssqw1b16dhiinj7j7dyj50f85qzzkfdn10v")))

