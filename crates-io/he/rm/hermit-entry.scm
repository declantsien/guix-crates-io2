(define-module (crates-io he rm hermit-entry) #:use-module (crates-io))

(define-public crate-hermit-entry-0.1.0 (c (n "hermit-entry") (v "0.1.0") (d (list (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)))) (h "0da43hcy41vzd5jbxh0c15r743fqdm4ridx94gc23dkhjyp3nfpy") (f (quote (("loader") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.1.1 (c (n "hermit-entry") (v "0.1.1") (d (list (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)))) (h "04f5m9kx3p8j5gzzc248fw0qmckgg0vw5sc3hni3wq6plckq1nc6") (f (quote (("loader") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.1.2 (c (n "hermit-entry") (v "0.1.2") (d (list (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)))) (h "05jpk25hna5lymav5c53cbjsh3d84m1fszz7bmszpxgm2fdx8f8r") (f (quote (("loader") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.1.3 (c (n "hermit-entry") (v "0.1.3") (d (list (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)))) (h "1llmssr6py8gq1xi8rffp9imjjfib22y62qjrypaj2m2v4rm135z") (f (quote (("loader") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.2.0 (c (n "hermit-entry") (v "0.2.0") (d (list (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)))) (h "0a1dabj9p48xzx0q4ixizka4yihfmn8vww7bb6ask5l4bv6899bb") (f (quote (("loader") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.3.0 (c (n "hermit-entry") (v "0.3.0") (d (list (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)))) (h "076j6mr5slvyl9bxygxcz52539110vpq3wflk1f8wi797y72772h") (f (quote (("loader") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.3.1 (c (n "hermit-entry") (v "0.3.1") (d (list (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)))) (h "141pa51q6bzdxkf3wl13hw9aay6m1wc8lflzlgjfv7a37nj62ni5") (f (quote (("loader") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.4.0 (c (n "hermit-entry") (v "0.4.0") (d (list (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)))) (h "0nav22jajw155wxp2sf7h6crrgy347202gz36lpxrqkj6jmcs03r") (f (quote (("loader") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.5.0 (c (n "hermit-entry") (v "0.5.0") (d (list (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)))) (h "0a2wwkv16jglxdywwzpa3c0pv3pcpqg38sf5dn8acdf6frplc30c") (f (quote (("loader") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.6.0 (c (n "hermit-entry") (v "0.6.0") (d (list (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "1a3qp6nb13saxh10rq37h2qbglzdmwkmxvszkr3ym38pf56s31ml") (f (quote (("loader") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.7.0 (c (n "hermit-entry") (v "0.7.0") (d (list (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "0rm72lqjfk1s4rkcyxy0bi4xxp8x281iawyx4xzmglvwn6s6lz92") (f (quote (("loader" "log" "goblin" "plain") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.8.0 (c (n "hermit-entry") (v "0.8.0") (d (list (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (f (quote ("unstable_const"))) (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "0c8gvxnf74ll87acbsqpi8vywj389682bglans0x06n84m9cjkdh") (f (quote (("loader" "log" "goblin" "plain") ("kernel" "memoffset") ("default"))))))

(define-public crate-hermit-entry-0.9.0 (c (n "hermit-entry") (v "0.9.0") (d (list (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "1bq5zd92kfvxhy1vgang6yxcyjd8yz8d0h1ai1kw9rz6kc3bg883") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default")))) (y #t)))

(define-public crate-hermit-entry-0.9.1 (c (n "hermit-entry") (v "0.9.1") (d (list (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "1rf343a6bgcq9k85s1hccn8nai28vix589yh0y88dzrinwqwbv3d") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

(define-public crate-hermit-entry-0.9.2 (c (n "hermit-entry") (v "0.9.2") (d (list (d (n "align-address") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.6") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "0sbkm52gmk409mw3md8dih52ci699hvkadcq50h4j6di3phkap5r") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

(define-public crate-hermit-entry-0.9.3 (c (n "hermit-entry") (v "0.9.3") (d (list (d (n "align-address") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.6") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "13q327zcz116gdwicwgs6fanx6dsyzsrwzlaqfyd671za51v6qai") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

(define-public crate-hermit-entry-0.9.4 (c (n "hermit-entry") (v "0.9.4") (d (list (d (n "align-address") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.7") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "06824z7jk068vr1jq4fq76lgyi16i98a4289v70c07i8xbkja130") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

(define-public crate-hermit-entry-0.9.5 (c (n "hermit-entry") (v "0.9.5") (d (list (d (n "align-address") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.7") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "0yrsp2893pnkyjgwd1c25y0nxncrlgmf9azzj5pxlcnip6j0zdm1") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

(define-public crate-hermit-entry-0.9.6 (c (n "hermit-entry") (v "0.9.6") (d (list (d (n "align-address") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.7") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "0ks2ij9xzfmpl4kv20p3x363cv2rk7s0h0b3m3dzyrc803pl0agq") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

(define-public crate-hermit-entry-0.9.7 (c (n "hermit-entry") (v "0.9.7") (d (list (d (n "align-address") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.7") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "02z5hdwvpkr751fy9i4nxl41han9rpdsnv94vglincpvgv7vh5wg") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

(define-public crate-hermit-entry-0.9.8 (c (n "hermit-entry") (v "0.9.8") (d (list (d (n "align-address") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.7") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "19y76x4b3qdh6armkdzr5imxqj81ckjyr2y2wc37kqk67wa01k4h") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

(define-public crate-hermit-entry-0.9.9 (c (n "hermit-entry") (v "0.9.9") (d (list (d (n "align-address") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.7") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "0kn47psqql02lcynqr36ch7aws051pwnqkb546ry38d6906f01z9") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

(define-public crate-hermit-entry-0.9.10 (c (n "hermit-entry") (v "0.9.10") (d (list (d (n "align-address") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.8") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "0zh9jw6y0dhilz3dk8ix3isw7wih8xq1xllipa7a7mv6cp7n7b92") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

(define-public crate-hermit-entry-0.10.0 (c (n "hermit-entry") (v "0.10.0") (d (list (d (n "align-address") (r "^0.1") (d #t) (k 0)) (d (n "goblin") (r "^0.8") (f (quote ("elf64"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "plain") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "0qxcbliqam7yphhfj1pn2a582vbv7g7ym0j5la29qib3c9aai3da") (f (quote (("loader" "log" "goblin" "plain") ("kernel") ("default"))))))

