(define-module (crates-io he rm hermit-toolkit-viewing-key) #:use-module (crates-io))

(define-public crate-hermit-toolkit-viewing-key-0.1.0 (c (n "hermit-toolkit-viewing-key") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "cosmwasm-storage") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-storage")) (d (n "hermit-toolkit-crypto") (r "^0.1.0") (f (quote ("hash" "rand"))) (k 0) (p "hermit-toolkit-crypto")) (d (n "hermit_toolkit_utils") (r "^0.1.0") (d #t) (k 0) (p "hermit-toolkit-utils")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "subtle") (r "^2.2.3") (k 0)))) (h "0an3blplkwy8djr8ilfv34adljnjbk5s3sklr7ds4xyaxnbrgg96")))

