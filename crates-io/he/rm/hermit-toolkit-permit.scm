(define-module (crates-io he rm hermit-toolkit-permit) #:use-module (crates-io))

(define-public crate-hermit-toolkit-permit-0.1.0 (c (n "hermit-toolkit-permit") (v "0.1.0") (d (list (d (n "bech32") (r "^0.8.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "hermit-toolkit-crypto") (r "^0.1.0") (f (quote ("hash"))) (d #t) (k 0) (p "hermit-toolkit-crypto")) (d (n "remain") (r "^0.2.2") (d #t) (k 0)) (d (n "ripemd160") (r "^0.9.1") (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "07afkyk6j5kmd50ajgkfghrqa1wc8filwcfjpsysdslb6080bb8a")))

