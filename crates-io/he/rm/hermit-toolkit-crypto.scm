(define-module (crates-io he rm hermit-toolkit-crypto) #:use-module (crates-io))

(define-public crate-hermit-toolkit-crypto-0.1.0 (c (n "hermit-toolkit-crypto") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "rand_chacha") (r "^0.2.2") (o #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (o #t) (k 0)) (d (n "secp256k1") (r "^0.21.3") (o #t) (d #t) (k 0)) (d (n "secp256k1-test") (r "^0.17") (f (quote ("rand-std" "recovery"))) (d #t) (k 2) (p "secp256k1")) (d (n "sha2") (r "^0.9.1") (o #t) (k 0)))) (h "1gzm2fj3jbxq5zq2xxaifsc4yf0ii3qzqscj77hy8a1h2m9dc1j2") (f (quote (("rand" "rand_chacha" "rand_core") ("hash" "sha2") ("ecc-secp256k1" "secp256k1"))))))

