(define-module (crates-io he rm hermit-toolkit-storage) #:use-module (crates-io))

(define-public crate-hermit-toolkit-storage-0.1.0 (c (n "hermit-toolkit-storage") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "cosmwasm-storage") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-storage")) (d (n "hermit-toolkit-serialization") (r "^0.1.0") (d #t) (k 0) (p "hermit-toolkit-serialization")) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1vr2r6hy25yr5j54cs056vzpxpmaqyfjv68vb6nicxxkkq75k0i5")))

