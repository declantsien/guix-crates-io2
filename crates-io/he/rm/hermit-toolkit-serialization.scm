(define-module (crates-io he rm hermit-toolkit-serialization) #:use-module (crates-io))

(define-public crate-hermit-toolkit-serialization-0.1.0 (c (n "hermit-toolkit-serialization") (v "0.1.0") (d (list (d (n "bincode2") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^0.9.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1xvsc5nriyrli35x7fy2862wcn9c4w7vv4xxh3sgclfizidw28kl") (f (quote (("json") ("default" "json" "bincode2" "base64") ("base64" "schemars"))))))

