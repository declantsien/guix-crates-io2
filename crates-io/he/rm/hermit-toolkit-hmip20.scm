(define-module (crates-io he rm hermit-toolkit-hmip20) #:use-module (crates-io))

(define-public crate-hermit-toolkit-hmip20-0.1.0 (c (n "hermit-toolkit-hmip20") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "hermit_toolkit_utils") (r "^0.1.0") (d #t) (k 0) (p "hermit-toolkit-utils")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0hds3b8a8sfpml0wy8v0fn1x7qv9xmhx9k8pwajnldypw21xag1q")))

