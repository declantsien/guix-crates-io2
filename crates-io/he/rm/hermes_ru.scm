(define-module (crates-io he rm hermes_ru) #:use-module (crates-io))

(define-public crate-hermes_ru-0.1.0 (c (n "hermes_ru") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2.7.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (d #t) (k 0)))) (h "0mn74lnn0c8s3np3cmj2j4w3mri8w0lpkf1nkkvia659l8r8z9m7") (r "1.66.1")))

