(define-module (crates-io he rm hermit-toolkit-hmip721) #:use-module (crates-io))

(define-public crate-hermit-toolkit-hmip721-0.1.0 (c (n "hermit-toolkit-hmip721") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "028isr9l5f0p6mxhnsl4q4h13magmc6g6r0mmxnq01sp1rw2mp26")))

(define-public crate-hermit-toolkit-hmip721-0.1.1 (c (n "hermit-toolkit-hmip721") (v "0.1.1") (d (list (d (n "cosmwasm-std") (r "^0.10") (d #t) (k 0) (p "secret-cosmwasm-std")) (d (n "hermit_toolkit_utils") (r "^0.1.0") (d #t) (k 0) (p "hermit-toolkit-utils")) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0fs1pcgp24bc5g0sap3fzadwvka8kjg6ys0ndxy1vksg89biwzj9")))

