(define-module (crates-io he bo hebo_codec) #:use-module (crates-io))

(define-public crate-hebo_codec-0.1.2 (c (n "hebo_codec") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "13x6wj14w6w9jcn0spfc49i3hsxzp1q8356diddh8nadv5g5bdp6")))

(define-public crate-hebo_codec-0.1.3 (c (n "hebo_codec") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "14fbxc339f4pcg8lx2c3xw1355jr5rdz320q9g0fmg14z6zib6xa")))

(define-public crate-hebo_codec-0.1.4 (c (n "hebo_codec") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aav93yrk26wmcn0dsas3qqnm52dlclfw9ankgmfp12i3jm63yn8")))

(define-public crate-hebo_codec-0.2.1 (c (n "hebo_codec") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bp5xwrwgxmrfzlgh44v5b47i3ypn03p8qifcl2ma6kqx3hbhsw4")))

(define-public crate-hebo_codec-0.2.2 (c (n "hebo_codec") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "05a9s32ih47naml8jnw2w7lj7nf4av1cf76x6vs05hr6dnkfgcw8")))

(define-public crate-hebo_codec-0.2.3 (c (n "hebo_codec") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xa8sl56d3jas3g083k43ga7cp3prvgyvlkllrwqmajd5s8ydnph")))

