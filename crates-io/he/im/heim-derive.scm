(define-module (crates-io he im heim-derive) #:use-module (crates-io))

(define-public crate-heim-derive-0.0.1 (c (n "heim-derive") (v "0.0.1") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("derive" "full" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1g0nlyspmqsmhzy36rhzkjww9hi5bi1sv0izhv8nkng73a3lw99k")))

(define-public crate-heim-derive-0.0.2 (c (n "heim-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("derive" "full" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "0mn4380injsy1dhbjk9grl13q9vn1plr8d0zng9sa7l2qbzm6b5h")))

(define-public crate-heim-derive-0.0.3 (c (n "heim-derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("derive" "full" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1gmjx73rpzb7sh7in4gn7dhfm27p1h4b09ydzy9vsd7d1gs6zac0")))

(define-public crate-heim-derive-0.0.4 (c (n "heim-derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive" "full" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)))) (h "1bslwlwglw03rgq2rrar8wpcqx72cgwkg6bxx156r0qvdahy6n2j")))

(define-public crate-heim-derive-0.0.5 (c (n "heim-derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive" "full" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1bbn4l8rwn1zrxb0i1az58zsxy8cjhb4pli28mk5j3bf9z4hifyd")))

(define-public crate-heim-derive-0.0.6 (c (n "heim-derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive" "full" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0qj2ibxpkkkxrskc9h3h5if5wg3y2mzzy9w5wxbfr7j58ks9h40g")))

(define-public crate-heim-derive-0.0.7 (c (n "heim-derive") (v "0.0.7") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1b4cgzb77s0biq0qwp8n7rikz2nxq99r55m12vw75c869p4i1pdd")))

(define-public crate-heim-derive-0.0.8-alpha.1 (c (n "heim-derive") (v "0.0.8-alpha.1") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0rja46bfglqgxyaalgh6zp7hjnyryw9rp0x6rsphccd0jvdjdwxk")))

(define-public crate-heim-derive-0.0.8 (c (n "heim-derive") (v "0.0.8") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "12qqbjqy4fx1bxlknsavk4kjp79jb4am5wg7piab5hbk8vgvwwwm")))

(define-public crate-heim-derive-0.0.9 (c (n "heim-derive") (v "0.0.9") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1b7p7izvbclywzw1sjj7b53kvwjnhxafmb1kll18mbx9ijzxk5yw")))

(define-public crate-heim-derive-0.0.10 (c (n "heim-derive") (v "0.0.10") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "10v27wgj7qnlda93am8xf5gcl70h64825zzvdsiv2795h0l8mjj6")))

(define-public crate-heim-derive-0.1.0-alpha.1 (c (n "heim-derive") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0z3nln8si6nzanxzszq4yqws6kacca46nm982fn05jagzhhbx4pf")))

(define-public crate-heim-derive-0.1.0-beta.1 (c (n "heim-derive") (v "0.1.0-beta.1") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "10bwbar62hp5gkbhalxi7rbfhafpdlz02qgycck2pkjljrbaqb6q")))

(define-public crate-heim-derive-0.1.0-rc.1 (c (n "heim-derive") (v "0.1.0-rc.1") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "17bzx7jw5wsik4rpc5fy7m3zjmq0p722yv24g7a7lz194nj62k7q")))

