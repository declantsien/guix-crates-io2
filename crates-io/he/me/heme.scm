(define-module (crates-io he me heme) #:use-module (crates-io))

(define-public crate-heme-0.3.1 (c (n "heme") (v "0.3.1") (h "0mm8rj1i5wnc9iizrdc8w0zm18f8m2wgvr0fy0mxqdq8hfx84yp5")))

(define-public crate-heme-0.3.2 (c (n "heme") (v "0.3.2") (h "0da25fg9l6wlhrsvmbkwcib8x1ycq16nrzphn13f97ckaadr30bb")))

