(define-module (crates-io he xd hexdi) #:use-module (crates-io))

(define-public crate-hexdi-0.1.0 (c (n "hexdi") (v "0.1.0") (d (list (d (n "iced") (r "^0.10.0") (d #t) (k 0)))) (h "0qzmag8636ckc795y831r36b8zd8lfcjagp8p59snjlk4fgwnkm0")))

(define-public crate-hexdi-0.1.1 (c (n "hexdi") (v "0.1.1") (d (list (d (n "iced") (r "^0.10.0") (d #t) (k 0)))) (h "1prbxwzyxpjgzz8kmrkc9lf5xrvj8dl0liw24vi32g55farln8nr")))

