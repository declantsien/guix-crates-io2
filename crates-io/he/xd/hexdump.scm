(define-module (crates-io he xd hexdump) #:use-module (crates-io))

(define-public crate-hexdump-0.1.0 (c (n "hexdump") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.3.12") (d #t) (k 0)) (d (n "itertools") (r ">= 0.3.0, < 0.5.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.27") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.2.26") (o #t) (d #t) (k 0)))) (h "0ar677vsqhjar3sfp209f8fh9zw0smyhhpj8qjb0y36j6cn3y3w5") (f (quote (("nightly-test" "quickcheck" "quickcheck_macros"))))))

(define-public crate-hexdump-0.1.1 (c (n "hexdump") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "itertools") (r ">=0.3.0, <0.5.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2.27") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.2.26") (o #t) (d #t) (k 0)))) (h "1wh555ab0c570fmkbng1jamy9d0pgdqivgkqi1vszwq2vgd860p4") (f (quote (("nightly-test" "quickcheck" "quickcheck_macros"))))))

(define-public crate-hexdump-0.1.2 (c (n "hexdump") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "0cnqvrrjw332ksdzci7lrisbscb15asfkn3v8b3s2ic1xmkancfg")))

