(define-module (crates-io he xd hexdino) #:use-module (crates-io))

(define-public crate-hexdino-0.1.0 (c (n "hexdino") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "ncurses") (r "^5.91") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "10lsn51rg6srbc1hrxkamr80334sdsvf5lywc921h5d9ygkjq8h4")))

(define-public crate-hexdino-0.1.1 (c (n "hexdino") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "ncurses") (r "^5.98") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1al29xha1gc3qri0dglbqzah10z4z8dk3dknm2znfrrl32yfrps1")))

(define-public crate-hexdino-0.1.2 (c (n "hexdino") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "1l8bcwva03m1xycdnmfhr4anv41gbaldpkp0k2zkyrajka38z9il")))

(define-public crate-hexdino-0.1.3 (c (n "hexdino") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "0l1h0jdw4nqc07pkb5sjb25f3jpnqs3g6ccyw3krz0nm3gxz9b9v")))

