(define-module (crates-io he xd hexdmp) #:use-module (crates-io))

(define-public crate-hexdmp-0.1.0 (c (n "hexdmp") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1kl7wnql12clk4hj4dr8qqga3f6fvpxybdxlwhi2dh9dmqg9f6gm")))

(define-public crate-hexdmp-0.1.1 (c (n "hexdmp") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0qp6w4z1lf381gf2kyykc47a25h4kdf3x747npqljpny9igwm3rh")))

(define-public crate-hexdmp-0.1.2 (c (n "hexdmp") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0l41g7x7xk12lrg3awjfmzdc34l9fvi91dfln1qzskgym7iphh5w")))

