(define-module (crates-io he xd hexdump-lt) #:use-module (crates-io))

(define-public crate-hexdump-lt-0.1.0 (c (n "hexdump-lt") (v "0.1.0") (h "0c11aj2mxkpfj55jlxpqv2s64advn735b3miq5nkaw00n8c5fy7a")))

(define-public crate-hexdump-lt-1.0.0 (c (n "hexdump-lt") (v "1.0.0") (h "0r5p379wyi75czk68707s41ymkm4avcjcr2r3dakf215csfwlcjf")))

(define-public crate-hexdump-lt-1.0.1 (c (n "hexdump-lt") (v "1.0.1") (h "19qk20gw24gizlq42lncyzsp50glrpv4i8z7297hnviv3hbx0wih")))

(define-public crate-hexdump-lt-1.0.2 (c (n "hexdump-lt") (v "1.0.2") (h "09dhzzy0gcalcbqwfj2iv8cygb478v3phnf96a3z5n9msr00riqa")))

