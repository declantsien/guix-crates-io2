(define-module (crates-io he ma heman) #:use-module (crates-io))

(define-public crate-heman-0.1.0 (c (n "heman") (v "0.1.0") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "http-status-codes2") (r "^0.1") (d #t) (k 0)))) (h "1d56kaamf2079zgl4qvcaj54zwpdb3xf3vc0i3lmb2qihalhsjpf")))

(define-public crate-heman-0.1.1 (c (n "heman") (v "0.1.1") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "http-status-codes2") (r "^0.1") (d #t) (k 0)))) (h "18yjh385ip1kjpnw7r8gswci4q2dng7w92algqxn9h1xg6zqharc")))

(define-public crate-heman-0.1.2 (c (n "heman") (v "0.1.2") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "http-status-codes2") (r "^0.1") (d #t) (k 0)))) (h "1iwww825ihvz8rny4bzncj4as03w9swrpds9387a78p2b5ki4zxv")))

(define-public crate-heman-0.1.3 (c (n "heman") (v "0.1.3") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "http-status-codes2") (r "^0.1") (d #t) (k 0)))) (h "1mnkdmr509a25b8mn0zdf8dlsp3kckg2mvqkf90brcg5gmmainnc")))

(define-public crate-heman-0.1.4 (c (n "heman") (v "0.1.4") (d (list (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "http-status-codes2") (r "^0.1") (d #t) (k 0)))) (h "0mn43awzqmmpkvaa6wvysifcfch8816g949hc9salfgmd1m4qrs0")))

