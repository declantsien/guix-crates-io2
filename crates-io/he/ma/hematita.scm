(define-module (crates-io he ma hematita) #:use-module (crates-io))

(define-public crate-hematita-0.1.0 (c (n "hematita") (v "0.1.0") (d (list (d (n "diff") (r "^0.1.12") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "visibility") (r "^0.0.1") (d #t) (k 0)))) (h "1xv1zlx24amwgv717230bv12x3776qp55pgj2h36hsxl0ix6j53y")))

