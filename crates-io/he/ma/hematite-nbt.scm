(define-module (crates-io he ma hematite-nbt) #:use-module (crates-io))

(define-public crate-hematite-nbt-0.1.0 (c (n "hematite-nbt") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0crvaa0lf2jdm7p4zpjw5kkiicwq3pqq74ngigwhprkxqypjyfbx")))

(define-public crate-hematite-nbt-0.2.0 (c (n "hematite-nbt") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "05g5jcg7qgby0myz3gwf0pan931v2nzns28j7lcrsb3sglimf1h6")))

(define-public crate-hematite-nbt-0.3.0 (c (n "hematite-nbt") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)))) (h "0r2jqcl5zmdax4pi37540k32bhnr743fr6lc0bl01lyvlsxkwkcc")))

(define-public crate-hematite-nbt-0.4.0 (c (n "hematite-nbt") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "045n4bqwvg6f4syxal6jc4cvspp2wvp0gib0par3304wam8cgwrc") (f (quote (("default" "serde"))))))

(define-public crate-hematite-nbt-0.4.1 (c (n "hematite-nbt") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ci9p6cg7vddg583qw53n9ajqdajvzldyvi7y165lwdi7fihgd4q") (f (quote (("default" "serde"))))))

(define-public crate-hematite-nbt-0.5.0 (c (n "hematite-nbt") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "indexmap") (r "^1.4") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1n9kxgbjvs994abwdkm8m4ihw1xfnivvm9i8qb414zhjhw6d7jaq") (f (quote (("preserve_order" "indexmap") ("default" "serde"))))))

(define-public crate-hematite-nbt-0.5.1 (c (n "hematite-nbt") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "indexmap") (r "^1.4") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0w36vyx2w3628w8dg06n4mg1jmczgs0k9y69d34v4z2nz1sb125l") (f (quote (("preserve_order" "indexmap") ("default" "serde"))))))

(define-public crate-hematite-nbt-0.5.2 (c (n "hematite-nbt") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "cesu8") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "indexmap") (r "^1.4") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fapfi7qb729afk9knx7kjjzjl99gn33f66wjdrvbkv7xs20f3b7") (f (quote (("preserve_order" "indexmap") ("default" "serde"))))))

