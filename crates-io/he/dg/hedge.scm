(define-module (crates-io he dg hedge) #:use-module (crates-io))

(define-public crate-hedge-0.0.1 (c (n "hedge") (v "0.0.1") (h "0q6a7xa61l29d833vmzf17xcvy54i92vv1714sh8sxvf27gd4ca0") (y #t)))

(define-public crate-hedge-0.0.2 (c (n "hedge") (v "0.0.2") (h "0dpv02a9bdwkq466faf143b1rhn34h83rcgf99nnxrq5hbzxls59") (y #t)))

(define-public crate-hedge-0.0.3 (c (n "hedge") (v "0.0.3") (h "0pxf4i2r5jmwwwwk9gjw2gsamcy4mpwfkjbdflh1pgkp6202h211") (y #t)))

(define-public crate-hedge-0.0.4 (c (n "hedge") (v "0.0.4") (h "10crc5nzazg2hzw5fzpph54ki4bsflab5ipav7hal29xchcv4qp4") (y #t)))

(define-public crate-hedge-0.0.5 (c (n "hedge") (v "0.0.5") (h "08fhj4ys6i5rp3y75z1pq22fwggrwyf89j6bv05xsgmf09ydfna7") (y #t)))

(define-public crate-hedge-0.0.6 (c (n "hedge") (v "0.0.6") (h "1xccyr79cdgrpvizmj1w35f58k2305sps1fy9qp239nrkvvwryax") (y #t)))

(define-public crate-hedge-0.0.7 (c (n "hedge") (v "0.0.7") (h "151id350hj0ip2w9ahnnnma3jg21hp1i4zmzx3vx9g1apdmw114z") (y #t)))

(define-public crate-hedge-0.0.8 (c (n "hedge") (v "0.0.8") (d (list (d (n "cgmath") (r "^0.14.1") (d #t) (k 0)))) (h "03j1f1m5246m5lj5k6jjm7dv3qlpdjly2jyrz130g6n2b2kff4sv") (y #t)))

(define-public crate-hedge-0.0.9 (c (n "hedge") (v "0.0.9") (d (list (d (n "cgmath") (r "^0.14.1") (d #t) (k 0)))) (h "0b6srr1dbn7npi0wsq80vxndyrj2y1wwlbfnzg10jv1w9xnb68na") (y #t)))

(define-public crate-hedge-0.0.10 (c (n "hedge") (v "0.0.10") (d (list (d (n "cgmath") (r "^0.14.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "12vxz806yf4p98ynf74hqs1s3vkw2ra81ryccp9ijqbnq2yb9h7x") (y #t)))

(define-public crate-hedge-0.0.11 (c (n "hedge") (v "0.0.11") (d (list (d (n "cgmath") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13") (o #t) (d #t) (k 0)))) (h "03kwgrf1m60c2cl6qm9arabnnz1sc57gv4b6cwlld7ih7f8lcapi") (f (quote (("with_nalgebra" "nalgebra") ("default" "cgmath")))) (y #t)))

(define-public crate-hedge-0.1.0 (c (n "hedge") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1nxg1sn7r0m36qlr4zawh39l6f70qc7pcqvri24w4mmyl29njm7p") (y #t)))

(define-public crate-hedge-0.1.1 (c (n "hedge") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "04jx3vabq8b142fqfh881hj34p01v5s3qqz6166g6lwvq0axlvp6") (y #t)))

(define-public crate-hedge-0.2.1 (c (n "hedge") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0yzsydn6w763n69kzbnliwsgaxgmxca6pjxrv79767kvx5l9d4qj")))

