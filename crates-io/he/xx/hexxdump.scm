(define-module (crates-io he xx hexxdump) #:use-module (crates-io))

(define-public crate-hexxdump-0.1.0 (c (n "hexxdump") (v "0.1.0") (h "0zpc9v2zs86nhw5gb53c8ybc29j057a6i8hr9ppl5cfkl6v36hgk")))

(define-public crate-hexxdump-1.0.0 (c (n "hexxdump") (v "1.0.0") (h "1n7pbnjzfd9c089j8bvpl88m0l8lghv3p8gf8q4zbl6b5qqym45q")))

(define-public crate-hexxdump-1.0.1 (c (n "hexxdump") (v "1.0.1") (h "0s1kqdm6dl9ai1aijrhyj8ppxhvnrmdfwi77xj6m878xi3cgjikn")))

