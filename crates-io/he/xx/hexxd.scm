(define-module (crates-io he xx hexxd) #:use-module (crates-io))

(define-public crate-hexxd-0.1.0 (c (n "hexxd") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1r7cfzq70fd5ramc7qndj5llnnmlvwxhrywnkgshac3h4ab6vg8i")))

(define-public crate-hexxd-0.1.1 (c (n "hexxd") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1jjcz8dbp1k70daglrwq7mrlnfrydh4wx3kh0973g17ih900km3y")))

(define-public crate-hexxd-0.1.2 (c (n "hexxd") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "01rnc57vbsyng1g1dhf5snfglkd7isxpwqz1biaasmjny5g8g34y")))

(define-public crate-hexxd-0.1.3 (c (n "hexxd") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "081isv2nn53gzlxjk10cjshapw1kahcqs3d78md6r6xh5lw963h5")))

(define-public crate-hexxd-0.1.4 (c (n "hexxd") (v "0.1.4") (d (list (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "06sipba7gbkgwhl5fhawp6y23lh7rfc1y08ai3pqpp7g62as71zb")))

