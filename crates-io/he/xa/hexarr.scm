(define-module (crates-io he xa hexarr) #:use-module (crates-io))

(define-public crate-hexarr-0.0.0-alpha (c (n "hexarr") (v "0.0.0-alpha") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1b4vflyq253n0j9rgq99522amgjhmr5w4g5lm3qn7bkk3s1rc8y0") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-hexarr-0.1.0 (c (n "hexarr") (v "0.1.0") (d (list (d (n "glam") (r "^0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "025018qdjgascl5hs2yyzvylkjl9844gpvdk218r8wdc6ibnkprb") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde") ("glam" "dep:glam"))))))

