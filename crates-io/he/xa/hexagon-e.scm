(define-module (crates-io he xa hexagon-e) #:use-module (crates-io))

(define-public crate-hexagon-e-0.1.0 (c (n "hexagon-e") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "148nbii97xcw4ifwjwj3i0dvpsi2vf8pmf3cn5ppqm78mx6m2mvf")))

(define-public crate-hexagon-e-0.1.1 (c (n "hexagon-e") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "0dm4z4j00ks21a98n9pas2yz56hjl0w3sndwxp12q67qcm22ll5k")))

(define-public crate-hexagon-e-0.1.2 (c (n "hexagon-e") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "08v1l118ygfwrlmqn2cwlls4mz9kvvrrsbdhfb61jc1sic02wa7y")))

(define-public crate-hexagon-e-0.1.3 (c (n "hexagon-e") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "0wk0iikdz85yg8dk7iys0pf8dx8cbs4pzp3jcz0vg95p2iggwvs2")))

(define-public crate-hexagon-e-0.1.4 (c (n "hexagon-e") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "12dq8npiji0yyppqwvlbqs5l9nz6823y309mhxiiav1cczqlk1dz")))

(define-public crate-hexagon-e-0.1.5 (c (n "hexagon-e") (v "0.1.5") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "1cna6zhrzdwc34b82ih4d1w74rw9vj2rlnckm54hl60lp45lgb83")))

