(define-module (crates-io he xa hexagon_tiles) #:use-module (crates-io))

(define-public crate-hexagon_tiles-0.1.0 (c (n "hexagon_tiles") (v "0.1.0") (d (list (d (n "float_eq") (r "^0.5.0") (d #t) (k 0)))) (h "1xz9h1ikqdklyqpv0zsxqm1iqmm3jlqn4p5snjl53lr5dlkxxy4a")))

(define-public crate-hexagon_tiles-0.2.0 (c (n "hexagon_tiles") (v "0.2.0") (d (list (d (n "float_eq") (r "~1.0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pa1p0ng7nz2dfx91sicawvb67lgf4pxkwmr8v6hrp8vzlfv4qqg")))

(define-public crate-hexagon_tiles-0.2.1 (c (n "hexagon_tiles") (v "0.2.1") (d (list (d (n "float_eq") (r "~1.0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yrxrqp4y1z2768iapmy9w32d17hlafkpd8fiaxyvgw9rf418lzm")))

