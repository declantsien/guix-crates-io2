(define-module (crates-io he xa hexasphere) #:use-module (crates-io))

(define-public crate-hexasphere-0.1.0 (c (n "hexasphere") (v "0.1.0") (d (list (d (n "glam") (r "^0.9.2") (d #t) (k 0)))) (h "1ilpmps88h9gvq2s4fmxy5ricrd2d3a3j6pcf2m9ic8jpyfj7l4s") (y #t)))

(define-public crate-hexasphere-0.1.1 (c (n "hexasphere") (v "0.1.1") (d (list (d (n "glam") (r "^0.9.2") (d #t) (k 0)))) (h "106bxafrk9cvk7f5vqr7p6w7vd6hgf1hi08jwlkv53ad70bb566z") (y #t)))

(define-public crate-hexasphere-0.1.2 (c (n "hexasphere") (v "0.1.2") (d (list (d (n "glam") (r "^0.9.2") (d #t) (k 0)))) (h "0rgg9wsid4ycbrd9d48if77bcl3k9zhc3zrlqyc1a51bbzfaqkd3") (y #t)))

(define-public crate-hexasphere-0.1.3 (c (n "hexasphere") (v "0.1.3") (d (list (d (n "glam") (r "^0.9.2") (d #t) (k 0)))) (h "0pdj1llcfwrlrrfw8s7pp9wmr3c75wf3705sqhd1241wzdri9cmv")))

(define-public crate-hexasphere-0.1.4 (c (n "hexasphere") (v "0.1.4") (d (list (d (n "glam") (r "^0.9.2") (d #t) (k 0)))) (h "0dj9mz2inxph0x4ha5q4vqk7k6a9xs7s34kj8z68zhkzl6b8kgci")))

(define-public crate-hexasphere-0.1.5 (c (n "hexasphere") (v "0.1.5") (d (list (d (n "glam") (r "^0.9") (d #t) (k 0)))) (h "085rb5mx3hwz8mw48n1y8agfr989qrr5a1zqgj0gnnks80iq66bz")))

(define-public crate-hexasphere-0.1.6 (c (n "hexasphere") (v "0.1.6") (d (list (d (n "glam") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "1j0hgjp7ffnwffnpa05h4kzw4fzjlxfrncif271qh8kdny0c19rd") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-0.1.7 (c (n "hexasphere") (v "0.1.7") (d (list (d (n "glam") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "08y5apxp00rql9ply78riyzfjllvswx0h9i1gvs0qpzaxbkpqlhg") (f (quote (("adjacency" "smallvec")))) (y #t)))

(define-public crate-hexasphere-1.0.0 (c (n "hexasphere") (v "1.0.0") (d (list (d (n "glam") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "0xc1za5mbrmhc2vis86mcnpkkpihl5n7d24n89lgn3p7flbr5b80") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-2.0.0 (c (n "hexasphere") (v "2.0.0") (d (list (d (n "glam") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "1y67ia0j5whgxf3ms58cgj4nmmw9vbfklab38dqw1pxpzr1njibf") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-2.1.0 (c (n "hexasphere") (v "2.1.0") (d (list (d (n "glam") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "0yybp0wvnn6g2ip4805pgzpqwj2i317f4n3qsk4did1lfij7wjak") (f (quote (("adjacency" "smallvec")))) (y #t)))

(define-public crate-hexasphere-3.0.0 (c (n "hexasphere") (v "3.0.0") (d (list (d (n "glam") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "0g9ap44m85fc7x2c1m7iwyf57bgxwnpxd0irmff9avsa0hvwi54f") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-3.1.0 (c (n "hexasphere") (v "3.1.0") (d (list (d (n "glam") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "0mck0prrdgi4yb73hlyqj2daq279wv2dfp0smn7x92q999bz87mq") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-3.1.1 (c (n "hexasphere") (v "3.1.1") (d (list (d (n "glam") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "12sabk2n617qni2nxbh210f53hcn3blwa3xv2xp0wcyd5x5mn16d") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-3.2.0 (c (n "hexasphere") (v "3.2.0") (d (list (d (n "glam") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "0wa135ab4rxp7gyzdvzmdvzgkipm7szdsjsaw0w4256gc4ls94n5") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-3.3.0 (c (n "hexasphere") (v "3.3.0") (d (list (d (n "glam") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "0i63161bv8l1hr39azqr4jmf0yd89y47f3pmjbi7szfp2d0flplp") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-3.4.0 (c (n "hexasphere") (v "3.4.0") (d (list (d (n "glam") (r "^0.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "1z70841j3j6rwg4kgm7b4yp1bg2za55ijfdf97k7ywycja8ajzs9") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-4.0.0 (c (n "hexasphere") (v "4.0.0") (d (list (d (n "glam") (r "^0.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "0dcamzzj2b44hw40w0p25j877jhy0pl5x18kmqxa4zbv8v16bb81") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-4.1.0 (c (n "hexasphere") (v "4.1.0") (d (list (d (n "glam") (r "^0.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "1qncdypr8x6xqxfvxxc1m15nyfbx4y7n7kcljggccb5pmgnkc23l") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-4.1.1 (c (n "hexasphere") (v "4.1.1") (d (list (d (n "glam") (r "^0.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "09qizj5qmqvvyi7h5k4130wa1im3y57d48f6hxhzwgj4cgpj965n") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-5.0.0 (c (n "hexasphere") (v "5.0.0") (d (list (d (n "glam") (r "^0.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "1hpcv5gg8rgj8agfsi01psgagv20fai5waj2fdkd5zs9j7g654jn") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-5.0.1 (c (n "hexasphere") (v "5.0.1") (d (list (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "0zc7mm1skr77n3qwls2qffmbx2ldckpqxciwg23s0ap2z6b732v3") (f (quote (("adjacency" "smallvec")))) (y #t)))

(define-public crate-hexasphere-6.0.0 (c (n "hexasphere") (v "6.0.0") (d (list (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "09v66vdsk25ypghb0wx3n4950rc85rpma13qf3211j4fsv7jvilx") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-6.1.0 (c (n "hexasphere") (v "6.1.0") (d (list (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "1xa6109ij858d788ci9dchk3c17h9py6nqr9akx0ajdiavdrn89b") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-7.0.0 (c (n "hexasphere") (v "7.0.0") (d (list (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "16wp2mrpiji80hdnpigppqlcyqlzhf8mx23x59cg2gsip8h9vaq4") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-7.1.0 (c (n "hexasphere") (v "7.1.0") (d (list (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "0zb168hk9bzhmicpxjwk0000hqfh264jns4ricbr44ak417kp9mj") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-7.2.0 (c (n "hexasphere") (v "7.2.0") (d (list (d (n "glam") (r "^0.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "1w0b9frg76k6clrzh47sqzd7lpg2qq7zcf8h0r53gip9gvnz4lln") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-7.2.1 (c (n "hexasphere") (v "7.2.1") (d (list (d (n "glam") (r "^0.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "1m5d8fk9dy5z76qgs0almpwlhn6fgq0qf6ajyp7k9bdnpv8szbda") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-8.0.0 (c (n "hexasphere") (v "8.0.0") (d (list (d (n "glam") (r "^0.22") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "0slhibrgfr7znxxslszf2gzk4w4w4hh1p7l0lz8dg0c6amafd731") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-8.1.0 (c (n "hexasphere") (v "8.1.0") (d (list (d (n "glam") (r "^0.23") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.2") (o #t) (d #t) (k 0)))) (h "121myzm13f8ms3li67v3c2wvqhwcl1i8pmfsls0a7gvqz51x8hdx") (f (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-9.0.0 (c (n "hexasphere") (v "9.0.0") (d (list (d (n "constgebra") (r "^0.1.3") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "1djv1ip7wdqihm5n9f68gblm3im2xn6ssg80qcr80jrjnzyd5rja") (f (quote (("shape-extras") ("adjacency" "tinyvec"))))))

(define-public crate-hexasphere-9.1.0 (c (n "hexasphere") (v "9.1.0") (d (list (d (n "constgebra") (r "^0.1.3") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "044si1igr1j54rp04ia4l6p71jkp9zhmbg9a16ybbcdwlwbdzcvw") (f (quote (("shape-extras") ("adjacency" "tinyvec"))))))

(define-public crate-hexasphere-10.0.0 (c (n "hexasphere") (v "10.0.0") (d (list (d (n "constgebra") (r "^0.1.3") (d #t) (k 0)) (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "0an08gpd46rm54a6fyhkfkqik5wbrnc8ps3jq01ygna3f5zxnggk") (f (quote (("shape-extras") ("adjacency" "tinyvec"))))))

(define-public crate-hexasphere-11.0.0 (c (n "hexasphere") (v "11.0.0") (d (list (d (n "constgebra") (r "^0.1.4") (d #t) (k 0)) (d (n "glam") (r "^0.26.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "0hpf6dhxhmi82vvy3p2hlzsn39kkbswgg46698z4psl2b3cxklif") (f (quote (("shape-extras") ("adjacency" "tinyvec"))))))

(define-public crate-hexasphere-12.0.0 (c (n "hexasphere") (v "12.0.0") (d (list (d (n "constgebra") (r "^0.1.4") (d #t) (k 0)) (d (n "glam") (r "^0.27.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "1v286wvxi8x54yvf5jwkhwr2zqls2qsaxvcnfh56n20g2qwb1mpd") (f (quote (("shape-extras") ("adjacency" "tinyvec"))))))

