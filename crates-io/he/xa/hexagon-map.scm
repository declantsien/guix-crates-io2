(define-module (crates-io he xa hexagon-map) #:use-module (crates-io))

(define-public crate-hexagon-map-0.0.0 (c (n "hexagon-map") (v "0.0.0") (d (list (d (n "ndarray") (r "^0.15.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)))) (h "08mmprbgcnb4052jdbfmjxm3pxa25zd7xy1yxy8sg08y0g8cqnfp") (f (quote (("default"))))))

(define-public crate-hexagon-map-0.0.1 (c (n "hexagon-map") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)))) (h "0alz9ypbs2p79qjnfbj419rj1sdbldc6gjbsz5gsy7k55q2qf671") (f (quote (("default"))))))

(define-public crate-hexagon-map-0.0.2 (c (n "hexagon-map") (v "0.0.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ordered-float") (r "^3.6.0") (d #t) (k 0)) (d (n "pathfinding") (r "^4.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shape-core") (r "^0.1.1") (d #t) (k 0)))) (h "1cc6sslykg3rim99x181k1wdpfx54fkn3446cjbxha21ajbiqlic") (f (quote (("default"))))))

