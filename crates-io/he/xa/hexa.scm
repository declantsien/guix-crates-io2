(define-module (crates-io he xa hexa) #:use-module (crates-io))

(define-public crate-hexa-0.0.7 (c (n "hexa") (v "0.0.7") (h "0f640hgb3jzam5sb3gkkgiy5wz5vmy01w0gfn0y1v18s23v6gikj") (y #t)))

(define-public crate-hexa-0.0.0 (c (n "hexa") (v "0.0.0") (h "1fxd54zlbvl9jfkjk6qvkrkdrc4s25hmg7zcn64rkw6li90n388j") (y #t)))

