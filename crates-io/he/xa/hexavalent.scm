(define-module (crates-io he xa hexavalent) #:use-module (crates-io))

(define-public crate-hexavalent-0.1.1 (c (n "hexavalent") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (k 0)) (d (n "libc") (r "^0.2.67") (k 0)) (d (n "time") (r "^0.2.9") (k 0)))) (h "1r8mh0kckwyhfcssyiqj6irv70n4cjykw5qm576h6x91hwwb0zfd") (f (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1.2 (c (n "hexavalent") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (k 0)) (d (n "libc") (r "^0.2.67") (k 0)) (d (n "time") (r "^0.2.9") (k 0)))) (h "1rvysf8d5avcizqcj9m3kgmihfijx5shb5r7sxp9kxkvnf23g2gy") (f (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1.3 (c (n "hexavalent") (v "0.1.3") (d (list (d (n "bitflags") (r ">=1.2.1, <2.0.0") (k 0)) (d (n "libc") (r ">=0.2.67, <0.3.0") (k 0)) (d (n "time") (r ">=0.2.23, <0.3.0") (k 0)))) (h "0wrr3hi7958y378yjz4l8x52yws44baldganwqpx5fin1qn3sim3") (f (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1.4 (c (n "hexavalent") (v "0.1.4") (d (list (d (n "bitflags") (r ">=1.2.1, <2.0.0") (k 0)) (d (n "libc") (r ">=0.2.67, <0.3.0") (k 0)) (d (n "time") (r ">=0.2.23, <0.3.0") (k 0)))) (h "05dx7arssxy30gmavzl6dhgz7k4y1ax1rdzsdidc72sxm3fblqkg") (f (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1.5 (c (n "hexavalent") (v "0.1.5") (d (list (d (n "bitflags") (r ">=1.2.1, <2.0.0") (k 0)) (d (n "libc") (r ">=0.2.67, <0.3.0") (k 0)) (d (n "time") (r ">=0.2.23, <0.3.0") (k 0)))) (h "085r89f91bd2bhchvf9fv2nq61yl8ap6m9i8jjzqwqir6s53yald") (f (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1.6 (c (n "hexavalent") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.2.1") (k 0)) (d (n "libc") (r "^0.2.67") (k 0)) (d (n "time") (r "^0.2.23") (k 0)))) (h "0gpr4bbj5xi777pdcg30485kv2k6as3fz03fl310nll2k0l5n9ms") (f (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1.7 (c (n "hexavalent") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.2.1") (k 0)) (d (n "libc") (r "^0.2.67") (k 0)) (d (n "time") (r "^0.2.23") (k 0)))) (h "027xwvzsapabbvw6v5d7j9n0h5r05gmsiz4kd9r5yd9mx2bf45xz") (f (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.2.0 (c (n "hexavalent") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (k 0)) (d (n "libc") (r "^0.2.67") (k 0)) (d (n "time") (r "^0.3.7") (k 0)))) (h "177f9qz0kazcslvxz77gnbh74a5n1pljrz24pxxqmypv0i1ca5ya") (f (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.2.1 (c (n "hexavalent") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (k 0)) (d (n "libc") (r "^0.2.67") (k 0)) (d (n "time") (r "^0.3.7") (k 0)))) (h "0m7xhhsm2vfcdabzx98l1swmymd21frvfljnlg4kgpqjvjyz3f2p") (f (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.3.0 (c (n "hexavalent") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (k 0)) (d (n "libc") (r "^0.2.67") (k 0)) (d (n "time") (r "^0.3.7") (k 0)))) (h "0larwkgdafbvcby8w1m2jpy6m2lj0nh089vrdmr4z4yb41n2l6p2") (f (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

