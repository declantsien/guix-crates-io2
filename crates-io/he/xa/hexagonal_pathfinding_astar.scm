(define-module (crates-io he xa hexagonal_pathfinding_astar) #:use-module (crates-io))

(define-public crate-hexagonal_pathfinding_astar-0.4.1 (c (n "hexagonal_pathfinding_astar") (v "0.4.1") (h "1s0ajkgfm4sq8ha7i5gk0vnxmd92z1qkdh1z8ii31v49r6rwq3z5")))

(define-public crate-hexagonal_pathfinding_astar-0.4.2 (c (n "hexagonal_pathfinding_astar") (v "0.4.2") (h "0y2gdhrhq1xlh9j4r515i7mraa7mlq12njzrfz1jbhj6z3b1iv65")))

(define-public crate-hexagonal_pathfinding_astar-0.5.0 (c (n "hexagonal_pathfinding_astar") (v "0.5.0") (h "1nry3jjgjwdhzimkqxrb9igrf9rljpbnb8pk6vd6sm8w69h8pwmb")))

(define-public crate-hexagonal_pathfinding_astar-0.5.1 (c (n "hexagonal_pathfinding_astar") (v "0.5.1") (h "1ggivf24s1c2m41w7man30881cza3331qzsbpvv8asd48va2q3qy")))

(define-public crate-hexagonal_pathfinding_astar-0.6.0 (c (n "hexagonal_pathfinding_astar") (v "0.6.0") (h "12k5yxganq17lkd6p4vaz5k74dblj8gbkrm5k04dkmqa795fdzj3") (r "1.56.0")))

(define-public crate-hexagonal_pathfinding_astar-0.7.0 (c (n "hexagonal_pathfinding_astar") (v "0.7.0") (h "06iy427b5q7k8s42aw9hr3pwvpgfr54y0nrizqg2qzx46hp50331") (r "1.56.0")))

(define-public crate-hexagonal_pathfinding_astar-0.8.0 (c (n "hexagonal_pathfinding_astar") (v "0.8.0") (h "1kv6bihlpxpd9f44vc6lm1n71icwij9m352vrcz3zk3gfhk1gj6q") (r "1.56.0")))

(define-public crate-hexagonal_pathfinding_astar-0.8.1 (c (n "hexagonal_pathfinding_astar") (v "0.8.1") (h "1bjz33vhqc6cw0yq64lkp1ml7plwsfwlw51l65r9qilf2d8yq353") (r "1.56.0")))

(define-public crate-hexagonal_pathfinding_astar-0.8.2 (c (n "hexagonal_pathfinding_astar") (v "0.8.2") (h "0qynhcz8r4f1zx1j7dq0yj64xgv40gavz6bwkyfpizhcbxbhk034") (r "1.58")))

(define-public crate-hexagonal_pathfinding_astar-0.8.3 (c (n "hexagonal_pathfinding_astar") (v "0.8.3") (h "016kgh46k8flv3jkgynw4jmy549lzdd2pwxqpvndm3pdc3vkkqn1") (r "1.58")))

(define-public crate-hexagonal_pathfinding_astar-0.8.4 (c (n "hexagonal_pathfinding_astar") (v "0.8.4") (h "02bvfmir34hnp5s7g4xpx6rr4k95f2rmpxvx8rv3z8a5lq80jr2x") (r "1.58")))

(define-public crate-hexagonal_pathfinding_astar-0.9.0 (c (n "hexagonal_pathfinding_astar") (v "0.9.0") (h "1c0hirli695wvaxv8ilxhlmmnyf642yls06ncs78kw8djz77jrzs") (r "1.58")))

(define-public crate-hexagonal_pathfinding_astar-1.0.0 (c (n "hexagonal_pathfinding_astar") (v "1.0.0") (h "0nmmqvfh7q6z8lkn42p6i2pfb26d2a3i6phwkj827qgvd29vnk5m") (r "1.59")))

