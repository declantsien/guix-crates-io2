(define-module (crates-io he xa hexagex) #:use-module (crates-io))

(define-public crate-hexagex-0.1.0 (c (n "hexagex") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.25") (d #t) (k 0)))) (h "11kh9l7hq5y1f63c60ab8swxm6vk8pz7nrrxbng1bfgp37xbcg59")))

(define-public crate-hexagex-0.1.1 (c (n "hexagex") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.25") (d #t) (k 0)))) (h "1xqbbcy80h8c01gcqy5hsz15w8476j17zj9j2chpgkb8lx4wf5bc")))

(define-public crate-hexagex-0.2.0 (c (n "hexagex") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.25") (d #t) (k 0)))) (h "1pnms896k8gkkb6rmzc0krqswh58rd2ds0c58ivylr75b235v9xm")))

(define-public crate-hexagex-0.2.1 (c (n "hexagex") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.27") (d #t) (k 0)))) (h "0ni030jlyjzgbjplf354z67wdvm2ddwj6vdwc000267xlx35n8dy")))

(define-public crate-hexagex-0.2.2 (c (n "hexagex") (v "0.2.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)))) (h "0yrwb9iv1p521rqlcchmhsqg4acc6v3sz9ah4vwxl1801mjkra19")))

(define-public crate-hexagex-0.2.3 (c (n "hexagex") (v "0.2.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.8.3") (d #t) (k 0)))) (h "0ladz08ylp53z45im1ibsnhbbccb7ynvlcmy5zllnx0lkgaxgpwh")))

