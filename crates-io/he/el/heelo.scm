(define-module (crates-io he el heelo) #:use-module (crates-io))

(define-public crate-heelo-0.1.0 (c (n "heelo") (v "0.1.0") (h "0nwihphvmkjngrr0ji4kbm8lrgc8jdylfyfr6l0xjyjimygp8msb") (y #t)))

(define-public crate-heelo-0.1.1 (c (n "heelo") (v "0.1.1") (h "10mxl4h04bdf110gj71j4him3ysjhy99yr2jfpxkrxkd5pv56kl6")))

(define-public crate-heelo-0.1.2 (c (n "heelo") (v "0.1.2") (h "1ywqqjqifld3dx9xnxmgrcahqgipqcibgyklqyvpny273g56w6xv")))

