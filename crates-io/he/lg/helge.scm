(define-module (crates-io he lg helge) #:use-module (crates-io))

(define-public crate-helge-0.1.0 (c (n "helge") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 2)) (d (n "diesel") (r "^1.4.8") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0yx4l30xbrifpicv717hdq3qzc423s0db9qsbqd6fbccylxsjamk")))

(define-public crate-helge-1.0.0 (c (n "helge") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 2)) (d (n "diesel") (r "^1.4.8") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1zqdhz6jj2cwm84i1wnwjv9099yyj6w501fqnz87pdk6xypil4lh")))

(define-public crate-helge-2.0.0-rc.0 (c (n "helge") (v "2.0.0-rc.0") (d (list (d (n "diesel") (r "^2.0.0-rc.0") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0v30d2cm5vhqljn161x52ij362n4fb90x6jns1l6bh3a1lbx9pa6")))

(define-public crate-helge-2.0.0-rc.1 (c (n "helge") (v "2.0.0-rc.1") (d (list (d (n "diesel") (r "^2.0.0-rc.1") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0ndqzaaaxhlxzzklzvxzc7950x4bzb5vp1xyqbvh64qmc1m6zqcm")))

(define-public crate-helge-2.0.0 (c (n "helge") (v "2.0.0") (d (list (d (n "diesel") (r "^2.0.0") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0lc45scl7pqq8xvvda41jf1k5hmcgkig8lzcrx7nmmb9pwv3rpgy")))

(define-public crate-helge-3.0.0 (c (n "helge") (v "3.0.0") (d (list (d (n "diesel") (r "^2") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0915b05yw4qsm22fcygjigx9k2mvl3lp43dfvlvhw59lw2j32lwp")))

