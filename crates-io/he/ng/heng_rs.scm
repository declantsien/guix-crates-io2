(define-module (crates-io he ng heng_rs) #:use-module (crates-io))

(define-public crate-heng_rs-0.1.0 (c (n "heng_rs") (v "0.1.0") (d (list (d (n "futures-channel-preview") (r "^0.3.0-alpha.19") (f (quote ("sink"))) (k 0)) (d (n "futures-util-preview") (r "^0.3.0-alpha.19") (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)) (d (n "tokio-executor") (r "^0.2.0-alpha.6") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.3.0-alpha.6") (d #t) (k 0)))) (h "1w8mmy156qdw81lwjpb1mz87jz1q38blprgig2xsgqj220zxm72m")))

