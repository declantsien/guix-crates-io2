(define-module (crates-io he y_ hey_listen) #:use-module (crates-io))

(define-public crate-hey_listen-0.1.0 (c (n "hey_listen") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.5") (d #t) (k 0)))) (h "06nh76wb2mwyy1vf7vwbji11xm4g56zvn1h3c956l11xagli57hn")))

(define-public crate-hey_listen-0.1.1 (c (n "hey_listen") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.5") (d #t) (k 0)))) (h "1033w8z195ngapzdxi587whlmz6p2ka4jsbw7wav2nkjr1dd71sz")))

(define-public crate-hey_listen-0.1.2 (c (n "hey_listen") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.5") (d #t) (k 0)))) (h "0bks4qlcnqq1qrw7fzg21zw3hk5k0siw8n9hgf4wnyag3ym0w507")))

(define-public crate-hey_listen-0.2.0 (c (n "hey_listen") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1qjkfjpak5swnszd1icw0lry3w99gbqac3kcf82n4rm3akdml0vr")))

(define-public crate-hey_listen-0.2.1 (c (n "hey_listen") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0wpkia958mb2i6yz6nxw1346080wisp6gk61i1kmlqq25wd708kk")))

(define-public crate-hey_listen-0.3.0 (c (n "hey_listen") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0agixyinn814q0vqhhb9zlpfx8lia8gacca1d7ig7k7w1sz782nn")))

(define-public crate-hey_listen-0.4.0 (c (n "hey_listen") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1xkr5j6f6nna2kiyq9nbv45ysxlap0nf2hw62vcihxga7lis099r")))

(define-public crate-hey_listen-0.5.0 (c (n "hey_listen") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (o #t) (k 0)))) (h "0yd06r9h6sy6pnnh8h0fyd96211zds55jxj3vzmzh863w3vn5w7m") (f (quote (("parallel" "rayon" "parking_lot") ("default" "async" "parallel") ("blocking") ("async" "tokio" "futures" "async-trait" "parking_lot"))))))

