(define-module (crates-io he ca heca-convert-lib) #:use-module (crates-io))

(define-public crate-heca-convert-lib-0.0.4 (c (n "heca-convert-lib") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0p38p1473xmq2pr3wwypd6k2cm8gn95vbc3x4rhq5czmlfx1i76c")))

(define-public crate-heca-convert-lib-0.0.5 (c (n "heca-convert-lib") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "11mgylmjdqrmw0r0qjrqh29dnb50yllhl95089p0dlpx4mp2z18x")))

