(define-module (crates-io he rb herbert) #:use-module (crates-io))

(define-public crate-herbert-0.1.0 (c (n "herbert") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ngiaw653421y86iigmh44z3y7dzcg5rclc9m50wqmr9jr1jm6qv")))

(define-public crate-herbert-0.2.0 (c (n "herbert") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vn96vv720scg219k993x3q6j1gsffrdcl9pvq8yvms2s2qwsgsg")))

(define-public crate-herbert-0.2.1 (c (n "herbert") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10cyxpx25fc9nh5fap8b14i2hq1ccyzllkxwysyw8zriwhk6bqg6")))

