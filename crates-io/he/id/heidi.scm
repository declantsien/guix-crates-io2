(define-module (crates-io he id heidi) #:use-module (crates-io))

(define-public crate-heidi-0.1.0 (c (n "heidi") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0nq5l825vi4jyjmviic3v9yin4xl1xs4cmvqlchxd81zq3vmp9w0")))

(define-public crate-heidi-0.2.0 (c (n "heidi") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1irx7qpr54gnx3cfmidb8fgxg05xnw1x96j4nc8qbw438ps4v4km")))

