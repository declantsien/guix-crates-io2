(define-module (crates-io he xt hextacy_derive) #:use-module (crates-io))

(define-public crate-hextacy_derive-0.1.0 (c (n "hextacy_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "19pilr15w6h6a1hlmy4k9c1z5zpigiax2ckph6ibkhjlhng2xdpm")))

