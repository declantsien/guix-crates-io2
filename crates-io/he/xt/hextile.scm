(define-module (crates-io he xt hextile) #:use-module (crates-io))

(define-public crate-hextile-0.1.2 (c (n "hextile") (v "0.1.2") (d (list (d (n "bevy") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "lerp") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1159hnviyad7v09lxwq5x1c4v5dmzzgkicyy8vnma7yzy15sx0qs") (s 2) (e (quote (("bevy" "dep:bevy"))))))

