(define-module (crates-io he xt hextral) #:use-module (crates-io))

(define-public crate-hextral-0.1.0 (c (n "hextral") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0zw3d08xy7dv8rdyi53ypw909ga0hf9bl42s88kmgw85kh74sdl9") (y #t)))

(define-public crate-hextral-0.1.3 (c (n "hextral") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1s1w24xir0jagq8cdh1cfk3jgq1zcypqgdbi2mf3rn4p6i6i1d15")))

(define-public crate-hextral-0.2.0 (c (n "hextral") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1k6jfxmmccl4c1bpa2g0m3iad89hlsh9q1ax1knr8p8ady82kg4q")))

(define-public crate-hextral-0.2.2 (c (n "hextral") (v "0.2.2") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0midac4aar1421dgap07xa7hx15ahni7dwxz25ad575rl3zas4xm")))

(define-public crate-hextral-0.2.5 (c (n "hextral") (v "0.2.5") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "01y6g1l4scvzc8acd1fmgn3ha7kg6wl9rrg9fg6rs4fhhk9sdcli")))

(define-public crate-hextral-0.3.0 (c (n "hextral") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11yd3pk89fwf54f942mfim72618cg63a7d5jf1dj2px3nddsnx38")))

