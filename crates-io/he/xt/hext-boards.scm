(define-module (crates-io he xt hext-boards) #:use-module (crates-io))

(define-public crate-hext-boards-0.1.0 (c (n "hext-boards") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.4") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "04j1rzf9a4id7n8nml6likl79jll187wp9d0h0hvll2c1bdlzqy9")))

(define-public crate-hext-boards-0.1.1 (c (n "hext-boards") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.31.4") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0zpxfrm623sx78znzykdh6z3idj85ndlkwmdpqn7z3gzkaqr74li")))

(define-public crate-hext-boards-0.2.0 (c (n "hext-boards") (v "0.2.0") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1c8s4lbndb7qy9ccxkc8wczpiwwnz365br2cn2ry479rdyggjbsk")))

(define-public crate-hext-boards-0.3.0 (c (n "hext-boards") (v "0.3.0") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0n7xwyymqg0nqy693c7p3i1lbqxjg5y8b9np913mmk33chxwcly0")))

