(define-module (crates-io he xt hextool) #:use-module (crates-io))

(define-public crate-hextool-0.1.0 (c (n "hextool") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0g6nka95cr762zqwq40rqxb813m6f6fly9knaybas63ijmd09452")))

(define-public crate-hextool-0.1.1 (c (n "hextool") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0ack3d2gq4b46lf0hgfjlh28v43rmdxbz24rvkqyjj570p2b5ds3")))

(define-public crate-hextool-0.1.2 (c (n "hextool") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "14hk8xb2lazpl941kmyhsj0zs0ab63pjhj34wswswxqqms57xwzg")))

(define-public crate-hextool-0.1.3 (c (n "hextool") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "06kc20xphrncgs8my31rzc8kkgrxwjqi71i2rv4219c245d75wa7")))

