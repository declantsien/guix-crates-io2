(define-module (crates-io he xt hext) #:use-module (crates-io))

(define-public crate-hext-0.4.0 (c (n "hext") (v "0.4.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)))) (h "11203ldv4vkx7dg6qi3hcyfl65p22mzq4h39v33ybbyyi4j4591m")))

(define-public crate-hext-0.4.1 (c (n "hext") (v "0.4.1") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)))) (h "0pv62z296a7hzn45xbrznc8096vjvbnjp24iin9xzybwy43r3xw3")))

(define-public crate-hext-0.4.2 (c (n "hext") (v "0.4.2") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)))) (h "1568pmyswvrp290i8cjq0d6s1ia3b5ywfd7dadqyc0ygxacbd2ak")))

