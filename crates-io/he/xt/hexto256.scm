(define-module (crates-io he xt hexto256) #:use-module (crates-io))

(define-public crate-hexto256-0.1.0 (c (n "hexto256") (v "0.1.0") (h "0kkncn5w09p17lqja3bjcg0cz883687rpz93qg8jrp7pl0cfxbb1")))

(define-public crate-hexto256-1.0.0 (c (n "hexto256") (v "1.0.0") (h "14v097j4anwqm8apjkk2avxqzh05fsq74xj7r7fh6bw2wix80fpz")))

