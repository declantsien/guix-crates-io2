(define-module (crates-io he bi hebi_diag) #:use-module (crates-io))

(define-public crate-hebi_diag-0.0.1 (c (n "hebi_diag") (v "0.0.1") (d (list (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "span") (r "^0.0.1") (d #t) (k 0) (p "hebi_span")) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 2)))) (h "1g4wpl473n72hlg9v7x4v2rmpxzfwagppfkfm81xqsvcdlcfp199")))

(define-public crate-hebi_diag-0.2.0 (c (n "hebi_diag") (v "0.2.0") (d (list (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "span") (r "^0.0.1") (d #t) (k 0) (p "hebi_span")) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 2)))) (h "182kmrsgz1ci6wyj4wrvlbxvsjkd1kbj73j8qip55a935ch9gnw6")))

