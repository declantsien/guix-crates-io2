(define-module (crates-io he bi hebi_derive) #:use-module (crates-io))

(define-public crate-hebi_derive-0.0.1 (c (n "hebi_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18rdj1b4yn1dyc7rqbc8j48nfq3322g90dn2gs6z24y78nlixid5")))

(define-public crate-hebi_derive-0.2.0 (c (n "hebi_derive") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z3fckgs19biahjpk797fnj0cxf2c7vfhnnbq6zivpxbbjsrv2ki")))

(define-public crate-hebi_derive-0.3.0 (c (n "hebi_derive") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10y3zim4i1cidj9p2fnis0py5sg98gggvq0slw563sxddwz75wz6")))

(define-public crate-hebi_derive-0.3.1 (c (n "hebi_derive") (v "0.3.1") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h36x5j4zc1m6x9h894p10af268dn269js7if5g09j2swhcd75dm")))

