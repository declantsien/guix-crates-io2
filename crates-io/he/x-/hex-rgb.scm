(define-module (crates-io he x- hex-rgb) #:use-module (crates-io))

(define-public crate-hex-rgb-0.1.0 (c (n "hex-rgb") (v "0.1.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "17vkyhg9sl3hywf6cs6mykmhz1sycjcj080d8fg2jab0zzmkph4j")))

(define-public crate-hex-rgb-0.1.1 (c (n "hex-rgb") (v "0.1.1") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "owo-colors") (r "^1.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0csc7m98qyg3ap9c5m7zhcrr6jdhw75jw9xzm4ln82blzmh76rbh")))

