(define-module (crates-io he x- hex-wrapper) #:use-module (crates-io))

(define-public crate-hex-wrapper-1.0.0 (c (n "hex-wrapper") (v "1.0.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07kllj24h9qqvnsqx19rnd43p6bj46y617bc76rnhybd0wl4gi6f") (f (quote (("default"))))))

(define-public crate-hex-wrapper-1.0.1 (c (n "hex-wrapper") (v "1.0.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17qb7qhiykqkl446qf7c5xh7gbcdxwa6is14dcza8swml8i1z95v") (f (quote (("default"))))))

(define-public crate-hex-wrapper-1.0.2 (c (n "hex-wrapper") (v "1.0.2") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1sw6w41w33c8fmkknyq5kk94yza6q331fszx6kv5n5m41a5sn7cn") (f (quote (("default"))))))

(define-public crate-hex-wrapper-1.1.0 (c (n "hex-wrapper") (v "1.1.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jz6n8wnw77c4jh99h6n6r09y7nfwljj0jag49f5xbms4mwmakpg") (f (quote (("default"))))))

(define-public crate-hex-wrapper-1.1.1 (c (n "hex-wrapper") (v "1.1.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0smiinlhxv4vjpm1yy0kskkq0d4zialx74s7d9jxx4imgd41xkwn") (f (quote (("default"))))))

(define-public crate-hex-wrapper-1.2.0 (c (n "hex-wrapper") (v "1.2.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ida4j52zzf5281g50d92zvrw5fpwa0lmv5izszpnmga0abx9d92") (f (quote (("default"))))))

(define-public crate-hex-wrapper-1.2.1 (c (n "hex-wrapper") (v "1.2.1") (d (list (d (n "diesel") (r "^1.4") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qhzc0y2nqbdzpb881wc4wvvk3nilrhyr5lsjblv8zhbpshfqbg4") (f (quote (("default") ("db" "diesel"))))))

(define-public crate-hex-wrapper-1.3.0 (c (n "hex-wrapper") (v "1.3.0") (d (list (d (n "diesel") (r "^1.4") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1kdzwk7lrqyn122bcwfi2wn03q9n0i7w078ssb7iay01ld4canhk") (f (quote (("default") ("db" "diesel"))))))

(define-public crate-hex-wrapper-1.3.1 (c (n "hex-wrapper") (v "1.3.1") (d (list (d (n "diesel") (r "^1.4") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1vyr9b5df8kw3vj3k0jxj6q5b02n232s91ap49w90a5n17kf1pba") (f (quote (("default") ("db" "diesel"))))))

(define-public crate-hex-wrapper-1.3.2 (c (n "hex-wrapper") (v "1.3.2") (d (list (d (n "diesel") (r "^1.4") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "06pdq4v15njmic0xrl45v03972l0bi8p4qi0qjpwcsrrlb183vi1") (f (quote (("default") ("db" "diesel"))))))

(define-public crate-hex-wrapper-1.4.0 (c (n "hex-wrapper") (v "1.4.0") (d (list (d (n "diesel") (r "^2.0") (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0b1wprmh0v20rfc6s43kz71cqp2mggdlqpvj270wdvx0n137lvrq") (f (quote (("default") ("db" "diesel"))))))

(define-public crate-hex-wrapper-1.4.1 (c (n "hex-wrapper") (v "1.4.1") (d (list (d (n "diesel") (r "^2.0") (f (quote ("with-deprecated"))) (o #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1whc22jm1gcwxxxgi56wwqcic83znqkg0vwqigjqzwppdlg82qwm") (f (quote (("default") ("db" "diesel"))))))

