(define-module (crates-io he x- hex-literal) #:use-module (crates-io))

(define-public crate-hex-literal-0.1.0 (c (n "hex-literal") (v "0.1.0") (d (list (d (n "hex-literal-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1ys7b0rvzprxxfwkic8s9plrhr4yvv3cvwp5mbqladxb43snwm5x")))

(define-public crate-hex-literal-0.1.1 (c (n "hex-literal") (v "0.1.1") (d (list (d (n "hex-literal-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "159day3l4lfk0jphza5pvf4a7gfarymfrvaf98i1m9yq3ghg19ad")))

(define-public crate-hex-literal-0.1.2 (c (n "hex-literal") (v "0.1.2") (d (list (d (n "hex-literal-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "00zkpfsx0kkag8syjrgqmwwlvgnz01qy6q4qwfh63rk5zcq5q3mf")))

(define-public crate-hex-literal-0.1.3 (c (n "hex-literal") (v "0.1.3") (d (list (d (n "hex-literal-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1yinds8bw7g60mwij46wv9mlg59rm34mjjqf4a3nqrm6nkl5qi97")))

(define-public crate-hex-literal-0.1.4 (c (n "hex-literal") (v "0.1.4") (d (list (d (n "hex-literal-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0ffnn5g9q5xhdmzj2ic5hk9y18kyqflbmqcssqcya9gixs5r5hnx")))

(define-public crate-hex-literal-0.2.0 (c (n "hex-literal") (v "0.2.0") (d (list (d (n "hex-literal-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0ni2nv3di0jpih2xnmlnr6s96zypkdr8xrw2cvk4f8fx5wb6inn3")))

(define-public crate-hex-literal-0.2.1 (c (n "hex-literal") (v "0.2.1") (d (list (d (n "hex-literal-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1q36f0qq31ggh4ipcwb7a5g6jmci2010vn2v3qpaz4csxhhf47cn")))

(define-public crate-hex-literal-0.3.0 (c (n "hex-literal") (v "0.3.0") (h "1hd0gwnnymsi88vgc32chinzii756ahkjphqy6gxif513i0clf6w")))

(define-public crate-hex-literal-0.3.1 (c (n "hex-literal") (v "0.3.1") (h "1j0ri05s2r5870v4mj13j019x069w6zkdccjhgblbi8vxwszdwas")))

(define-public crate-hex-literal-0.3.2 (c (n "hex-literal") (v "0.3.2") (h "0f71b21g4d6a5rkl7p55by69k05vmdr89dk0nfxvsfyanqk5wl3n")))

(define-public crate-hex-literal-0.3.3 (c (n "hex-literal") (v "0.3.3") (h "0nzljsyz9rwhh4vi0xs9ya4l5g0ka754wgpy97r1j3v42c75kr11")))

(define-public crate-hex-literal-0.3.4 (c (n "hex-literal") (v "0.3.4") (h "1q54yvyy0zls9bdrx15hk6yj304npndy9v4crn1h1vd95sfv5gby")))

(define-public crate-hex-literal-0.2.2 (c (n "hex-literal") (v "0.2.2") (d (list (d (n "hex-literal-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.7") (d #t) (k 0)))) (h "04ba6fmk6q2mmzpl1wmfsaz3wyljcd0va8577wpmbx1wkccr61np")))

(define-public crate-hex-literal-0.4.0 (c (n "hex-literal") (v "0.4.0") (h "13ijkrzbxy333kdqqdl67xffb3fy6gkvpygj3lcsg4lw8cz5pjsb") (r "1.57")))

(define-public crate-hex-literal-0.4.1 (c (n "hex-literal") (v "0.4.1") (h "0iny5inkixsdr41pm2vkqh3fl66752z5j5c0cdxw16yl9ryjdqkg") (r "1.57")))

