(define-module (crates-io he x- hex-display) #:use-module (crates-io))

(define-public crate-hex-display-0.1.0 (c (n "hex-display") (v "0.1.0") (h "0mqqmmj1nhc7ipx0schd5529cmkfsq37z1679lyyd6m49bnr2q4c")))

(define-public crate-hex-display-0.1.1 (c (n "hex-display") (v "0.1.1") (h "0335ng09ym75b10x7m657ii31rccv203m2iz59d5bcj8i799f3q1") (f (quote (("std"))))))

(define-public crate-hex-display-0.2.0 (c (n "hex-display") (v "0.2.0") (h "0d0xy5v5s9556w2y1876cghzyv3a6n7idd81afsip6krn73xbfwq") (f (quote (("std")))) (y #t)))

(define-public crate-hex-display-0.3.0 (c (n "hex-display") (v "0.3.0") (h "0mhp58lw60sv0a4l6c7mhxwgzld8ajp62wp7vsgmsz2h6jkdclwb") (f (quote (("std"))))))

