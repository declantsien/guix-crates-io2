(define-module (crates-io he x- hex-coordinates) #:use-module (crates-io))

(define-public crate-hex-coordinates-0.1.0 (c (n "hex-coordinates") (v "0.1.0") (h "1w5bdggn2yb17mf9dbkhr2a35xjqyx0agynfjqca21125v1r227v")))

(define-public crate-hex-coordinates-0.1.1 (c (n "hex-coordinates") (v "0.1.1") (h "1hj4snmbmgq1sxn1a1ryq5sdrjsxhx2zpp7qsdjkhw8nnqja1ayj")))

(define-public crate-hex-coordinates-0.1.2 (c (n "hex-coordinates") (v "0.1.2") (h "0qf0bbhjqmy6brp0w3pvbmf03wpznapycj443pyq8bcqkfddakfx")))

(define-public crate-hex-coordinates-0.1.3 (c (n "hex-coordinates") (v "0.1.3") (h "1hg80cfpkabp3p04a4sf7s7y1116b3v9234vyqsmv7n6ds231ic3")))

