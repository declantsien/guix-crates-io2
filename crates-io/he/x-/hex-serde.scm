(define-module (crates-io he x- hex-serde) #:use-module (crates-io))

(define-public crate-hex-serde-0.1.0 (c (n "hex-serde") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "04kh3fzyhi72j92c9fr0zjcj87dl99g8mbjn33ycbi2jksydg1il")))

