(define-module (crates-io he x- hex-view) #:use-module (crates-io))

(define-public crate-hex-view-0.1.0 (c (n "hex-view") (v "0.1.0") (h "0afqhicp7nl2dbmbf4dfswl907yjr3iz1wlnalbgs53aavgv5igv")))

(define-public crate-hex-view-0.1.1 (c (n "hex-view") (v "0.1.1") (h "0zigbibnrvbd45lkf2yb11x49ilv0apq2vf5lz6cpydib6jdysmn")))

(define-public crate-hex-view-0.1.2 (c (n "hex-view") (v "0.1.2") (h "0vpfqkn73gvj0zqlh0zcd1fyzhgf6l13s66gwss6g3qxmjcjbmbi")))

(define-public crate-hex-view-0.1.3 (c (n "hex-view") (v "0.1.3") (h "0zx9bkzj1wkh6rh13sqcxa8mc11qgvrc17pyyf42ml2dzv4ickj9")))

