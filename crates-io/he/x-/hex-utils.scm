(define-module (crates-io he x- hex-utils) #:use-module (crates-io))

(define-public crate-hex-utils-0.1.8 (c (n "hex-utils") (v "0.1.8") (h "0y4ppxy5qcfg7hx9z86kwzxrg76vzw81l6svl4z684vhz410bxqn")))

(define-public crate-hex-utils-0.1.11 (c (n "hex-utils") (v "0.1.11") (h "0ifcjzysrd3yknj6lz9xqwzpyp0m9g5m3hbr5sx437z42v0g7z0h")))

(define-public crate-hex-utils-0.1.12 (c (n "hex-utils") (v "0.1.12") (h "0mf937wh71y8qvpx3za6ks7nfgrvs81265hc4lsdcxpgfzsxjcng")))

