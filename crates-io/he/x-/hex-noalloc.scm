(define-module (crates-io he x- hex-noalloc) #:use-module (crates-io))

(define-public crate-hex-noalloc-0.3.2 (c (n "hex-noalloc") (v "0.3.2") (h "1pq4ylwr5annp832m7wlmbcdk9d6b83sbz7agb332p0hin7mjcp3") (f (quote (("std") ("default" "std") ("benchmarks")))) (y #t)))

(define-public crate-hex-noalloc-0.3.2-post1 (c (n "hex-noalloc") (v "0.3.2-post1") (h "028xqdkvinx6c5xncnhzq9g21fg61kla93cbkf8y331px57cvh2q") (f (quote (("std") ("default" "std") ("benchmarks"))))))

