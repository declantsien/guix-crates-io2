(define-module (crates-io he x- hex-game) #:use-module (crates-io))

(define-public crate-hex-game-0.1.0 (c (n "hex-game") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^1.7") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)))) (h "0hry6v2c0nfxzv87lvyg9zcyniqrmqwhd8ypafgyj61g52ddjspg")))

