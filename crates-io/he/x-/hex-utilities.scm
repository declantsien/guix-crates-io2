(define-module (crates-io he x- hex-utilities) #:use-module (crates-io))

(define-public crate-hex-utilities-0.1.0 (c (n "hex-utilities") (v "0.1.0") (h "150gvgz10mn7nmq9ymwxlyvvalw70kkxxcxkbqabva1759wzp141")))

(define-public crate-hex-utilities-0.1.1 (c (n "hex-utilities") (v "0.1.1") (h "0qwsasv8369i5032f2dpcxvnd393lp703pfwblnw4sasjilciaqx")))

(define-public crate-hex-utilities-0.1.2 (c (n "hex-utilities") (v "0.1.2") (h "1gk9pbnddl3p2dsxg96njaqqkax1c18l22qfbg8c3fz2yaczx5sr")))

(define-public crate-hex-utilities-0.1.3 (c (n "hex-utilities") (v "0.1.3") (h "1iq7a9zzgxli4ap0pn09msx7agchhrqgd125lv99crgn515aysvg")))

(define-public crate-hex-utilities-0.1.4 (c (n "hex-utilities") (v "0.1.4") (h "01v05fi3ikhk9cg78c0dwfmmsfcf8yh88gsv7wixgxazm8rdjk3k")))

(define-public crate-hex-utilities-0.1.5 (c (n "hex-utilities") (v "0.1.5") (d (list (d (n "binary-utils") (r "=0.1.1") (d #t) (k 0)))) (h "14p8108xykq874x8l7xwf6b4kwmvhc274wirky250b5i59apl5fj")))

