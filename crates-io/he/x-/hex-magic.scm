(define-module (crates-io he x- hex-magic) #:use-module (crates-io))

(define-public crate-hex-magic-0.0.1 (c (n "hex-magic") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "04lsrk2gvx2nb811469jqg3g6z0zz4b4wnzhw0pd2b9akwicw0wm")))

(define-public crate-hex-magic-0.0.2 (c (n "hex-magic") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0ip6sks33ilkkl173l9yn9sk2vzcmsqhvhc9mz52k2mrh25i5jr5")))

