(define-module (crates-io he x- hex-slice) #:use-module (crates-io))

(define-public crate-hex-slice-0.1.0 (c (n "hex-slice") (v "0.1.0") (h "1q0daiba6brd19bjba51m65bnpda3213wj1vd7nkcl2jb1ak3br5")))

(define-public crate-hex-slice-0.1.1 (c (n "hex-slice") (v "0.1.1") (h "0miylg2raz33i7c9xwh4n4yqcxz11sg8w53icpijr9z3rg5f65g1")))

(define-public crate-hex-slice-0.1.2 (c (n "hex-slice") (v "0.1.2") (d (list (d (n "suppositions") (r "^0.1.2") (d #t) (k 2)))) (h "1qm7l0zi86fhm91pxdca9c95cb02zr5qlsxc391drsd7wbs6i443")))

(define-public crate-hex-slice-0.1.3 (c (n "hex-slice") (v "0.1.3") (d (list (d (n "suppositions") (r "^0.1.2") (d #t) (k 2)))) (h "1w5g2lpwbv9hhva533qvs170b2zw3x2933zm5zfmly7m6mxid22k")))

(define-public crate-hex-slice-0.1.4 (c (n "hex-slice") (v "0.1.4") (d (list (d (n "suppositions") (r "^0.1.2") (d #t) (k 2)))) (h "0k3ck81m9lz5wz3c7qqj3j3m5xa5pr599n41gbq58i91w04a74al")))

