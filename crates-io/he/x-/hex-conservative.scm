(define-module (crates-io he x- hex-conservative) #:use-module (crates-io))

(define-public crate-hex-conservative-0.0.1 (c (n "hex-conservative") (v "0.0.1") (h "1kfsjm602nmp89yj5g70z5hnsdszg5974l8w03slvyi45b5am9wl")))

(define-public crate-hex-conservative-0.1.0 (c (n "hex-conservative") (v "0.1.0") (d (list (d (n "core2") (r "^0.3.2") (o #t) (k 0)))) (h "0gsqd97hiifvpdwgpnbp3avwghb48yqg8n4mp0njwnsr5ffcv79b") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-hex-conservative-0.1.1 (c (n "hex-conservative") (v "0.1.1") (d (list (d (n "core2") (r "^0.3.2") (o #t) (k 0)))) (h "1qji1wg2m41vahpxzc9gfmv32kgr8lanczhy3kcbdk2qyhx49v9h") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-hex-conservative-0.2.0 (c (n "hex-conservative") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "core2") (r "^0.3.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11n9q9qwsjsiy9c4247v9832jkiabqdcfhgdbk9pxqsiyhxjgap1") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-hex-conservative-0.1.2 (c (n "hex-conservative") (v "0.1.2") (d (list (d (n "core2") (r "^0.3.2") (o #t) (k 0)))) (h "085xvjs4fzq7kdf67s298f9l10byi5n0098074clhjrm08hbjai1") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-hex-conservative-0.2.1 (c (n "hex-conservative") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "core2") (r "^0.3.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kg6xb2n59zpid87kdgqvc5mks68qh9gdnq8m1jp0n9wrrrb04sk") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

