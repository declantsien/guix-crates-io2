(define-module (crates-io he x- hex-literal-impl) #:use-module (crates-io))

(define-public crate-hex-literal-impl-0.1.0 (c (n "hex-literal-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0ccn5ly8h7424jh0v5vvplmhp5nx7xnmd1a9sl0lv9giqyj6v9rf")))

(define-public crate-hex-literal-impl-0.1.1 (c (n "hex-literal-impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "02hk47xfbrrjp5y8dkprzq28l9x5099vcvfi3gdzccpj2ijhnd0x")))

(define-public crate-hex-literal-impl-0.1.2 (c (n "hex-literal-impl") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1nnxqhyn9l998ma04ip79bmpqv1as6003s03g26ynhrr471p022j")))

(define-public crate-hex-literal-impl-0.2.0 (c (n "hex-literal-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "04m6d1k57a9h3hhdgn0vq1hkfwjv9hfkw6q73bqn0my0qw45s286")))

(define-public crate-hex-literal-impl-0.2.1 (c (n "hex-literal-impl") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0bgldhp5gdwwnikfdxigmz9b64qpgwbjqk6mfgv0pvig9s25qk4x")))

(define-public crate-hex-literal-impl-0.2.2 (c (n "hex-literal-impl") (v "0.2.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0ilvbgqfxhw8pjk08xksik1rjclplixpn683ccbxwcgbk6apcgw5")))

(define-public crate-hex-literal-impl-0.2.3 (c (n "hex-literal-impl") (v "0.2.3") (d (list (d (n "proc-macro-hack") (r "^0.5.7") (d #t) (k 0)))) (h "1a25nz3v1323qhy328qr0mdzz0iyhjfxdhvqgy8bcpraz318yi2r")))

