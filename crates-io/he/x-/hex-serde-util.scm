(define-module (crates-io he x- hex-serde-util) #:use-module (crates-io))

(define-public crate-hex-serde-util-0.1.0 (c (n "hex-serde-util") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vw12s94wg7zx41cv7clqcbqa4wg2v8mkxnqbrg7h7kci09whpr4")))

