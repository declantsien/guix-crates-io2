(define-module (crates-io he xv hexviewer) #:use-module (crates-io))

(define-public crate-hexviewer-0.1.0 (c (n "hexviewer") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "evalexpr") (r "^5.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (f (quote ("with-fuzzy"))) (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)))) (h "0rn99x14ym2g3dwb9nhrzswpxc9sh29s4n9v9wisg2m61fa0ppp1")))

