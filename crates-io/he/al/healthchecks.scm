(define-module (crates-io he al healthchecks) #:use-module (crates-io))

(define-public crate-healthchecks-0.1.0 (c (n "healthchecks") (v "0.1.0") (d (list (d (n "ureq") (r "^1.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (k 0)))) (h "0harj2mm357lz95xc3yiass0f29p25wwx8v6vaw6f093i5irbbpw")))

(define-public crate-healthchecks-1.0.0 (c (n "healthchecks") (v "1.0.0") (d (list (d (n "ureq") (r "^1.3.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (k 0)))) (h "1vahg9rayzb1firshn3yb8h5b5fvf7hq6agg0kp1mvkbskh7ghic")))

(define-public crate-healthchecks-1.0.1 (c (n "healthchecks") (v "1.0.1") (d (list (d (n "ureq") (r "^1.3.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (k 0)))) (h "06k5zirm4bckd272j6lmhsb06x3dnap42wrh8smv19sqjk3w4skn")))

(define-public crate-healthchecks-2.0.0 (c (n "healthchecks") (v "2.0.0") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "ureq") (r "^1.5.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v1"))) (k 0)))) (h "1gqia1npxanrp1y8869pshbz4syfa486d914nfkpb309kcm6i8i2")))

(define-public crate-healthchecks-3.0.0 (c (n "healthchecks") (v "3.0.0") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~0.8") (f (quote ("v1"))) (k 0)))) (h "1qqwy601j800jbzrr17b13w1ic6dc2bv243264c21p90lmlvidfz")))

(define-public crate-healthchecks-3.0.1 (c (n "healthchecks") (v "3.0.1") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~0.8") (f (quote ("v1"))) (k 0)))) (h "195ak2w4114a8vwnfjf76kxa6ybk1brhncrlaz10fca552s9cjwm")))

(define-public crate-healthchecks-3.0.2 (c (n "healthchecks") (v "3.0.2") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~0.8") (f (quote ("v1"))) (k 0)))) (h "09jl818w9zi2rw5aknyixsyqnb4x7jd6id0h4srqvinai1q3n5f1")))

(define-public crate-healthchecks-3.0.3 (c (n "healthchecks") (v "3.0.3") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~0.8") (f (quote ("v1"))) (k 0)))) (h "0rv5crc813qfv3yckkzqh7ngnyifmzb3h2xg6w82j0y4jffjwy61")))

(define-public crate-healthchecks-3.0.4 (c (n "healthchecks") (v "3.0.4") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~0.8") (f (quote ("v1"))) (k 0)))) (h "0j7c0ayy7z8fz1vd8dj6gjpzp4mn1cz1lwblx3ijxwf9abws106n")))

(define-public crate-healthchecks-3.0.5 (c (n "healthchecks") (v "3.0.5") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~1.0") (f (quote ("v1"))) (k 0)))) (h "0jvw1895ns9q0gjjhccmq8ac1dk472hjqh1c7z8s167m385q4npq")))

(define-public crate-healthchecks-3.0.6 (c (n "healthchecks") (v "3.0.6") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~1.0") (f (quote ("v1"))) (k 0)))) (h "0hidmrp88ck3a8dk3qd2rl1z571y2vf8pmryqxijw0a918xanmcl") (r "1.58.0")))

(define-public crate-healthchecks-3.1.0 (c (n "healthchecks") (v "3.1.0") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~1.1") (f (quote ("v1"))) (k 0)))) (h "1fv76cng7dfjwgz90iycb9i1wqhdx8z7al8rpz0w6vlajy2cavmx") (r "1.59.0")))

(define-public crate-healthchecks-3.1.1 (c (n "healthchecks") (v "3.1.1") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)) (d (n "ureq") (r "~2.5") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~1.1") (f (quote ("v1"))) (k 0)))) (h "06k98nj41773ig5zi0qjdc2fddcf7xvxjy8mv2wp9kdv21ayshyi") (r "1.59.0")))

(define-public crate-healthchecks-3.1.2 (c (n "healthchecks") (v "3.1.2") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.2") (d #t) (k 0)) (d (n "ureq") (r "~2.5") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~1.1") (f (quote ("v1"))) (k 0)))) (h "0y0430mkqwai226cxjjw4l8dswvg3f3ahmv7mvcikhrjn6bsyl2y") (r "1.59.0")))

(define-public crate-healthchecks-3.1.3 (c (n "healthchecks") (v "3.1.3") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.2") (d #t) (k 0)) (d (n "ureq") (r "~2.5") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~1.2") (f (quote ("v1"))) (k 0)))) (h "0g89d2nkqqccq0pfrc1z69g49nc6d7b62xww1gi9civ5r75v3d3k") (r "1.64.0")))

(define-public crate-healthchecks-3.1.4 (c (n "healthchecks") (v "3.1.4") (d (list (d (n "serde") (r "~1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.2") (d #t) (k 0)) (d (n "ureq") (r "~2.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "~1.2") (f (quote ("v1"))) (k 0)))) (h "181rp9a5lfpjqdli491ygry3462xklrv78bfs2lp938p4fgl6gn5") (f (quote (("v2")))) (r "1.64.0")))

(define-public crate-healthchecks-3.1.5 (c (n "healthchecks") (v "3.1.5") (d (list (d (n "serde") (r "^1.0.164") (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (f (quote ("v1"))) (k 0)))) (h "091bwli9mnwkjmr47jfv2i5xv6sw5zndcv4x1mihil2x490hf104") (f (quote (("v3" "v2") ("v2")))) (r "1.64.0")))

(define-public crate-healthchecks-3.1.6 (c (n "healthchecks") (v "3.1.6") (d (list (d (n "serde") (r "^1.0.183") (k 0)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v1"))) (k 0)))) (h "19jsr9a6qmwpjxinx2iynhsbm5vcwa77j8lki2xjnfm2hlf3jbvk") (f (quote (("v3" "v2") ("v2")))) (r "1.64.0")))

