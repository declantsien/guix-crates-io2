(define-module (crates-io he al healm) #:use-module (crates-io))

(define-public crate-healm-0.1.0 (c (n "healm") (v "0.1.0") (d (list (d (n "blake3") (r "^1") (o #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0dnl613bwbaw2cfjj5yqq3m67sjjq5x00ml1cdzs2ykafp2i36g0") (f (quote (("test"))))))

(define-public crate-healm-0.2.0 (c (n "healm") (v "0.2.0") (d (list (d (n "blake3") (r "^1") (o #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1cjn3z10a1h5r39immms3094ga3r5hb5v4h413swnbjz0j4hpvxi") (f (quote (("bench"))))))

(define-public crate-healm-0.2.1 (c (n "healm") (v "0.2.1") (d (list (d (n "blake3") (r "^1") (o #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1545ss5sxffnx39i3m9hwk479qzxvs0j0mmwacczwd5rnnkybfzn") (f (quote (("bench"))))))

