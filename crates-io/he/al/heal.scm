(define-module (crates-io he al heal) #:use-module (crates-io))

(define-public crate-heal-0.0.0 (c (n "heal") (v "0.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1i2p3zclb9w7xicyi9w1k6wqd9cbf3xslz0yq7rfbqfy8nf16xji") (f (quote (("default")))) (r "1.73")))

