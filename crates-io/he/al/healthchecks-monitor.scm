(define-module (crates-io he al healthchecks-monitor) #:use-module (crates-io))

(define-public crate-healthchecks-monitor-1.0.0 (c (n "healthchecks-monitor") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("suggestions" "color"))) (k 0)) (d (n "healthchecks") (r "^1.0.0") (d #t) (k 0)) (d (n "pretty-exec-lib") (r "^0.0.17") (d #t) (k 0)))) (h "0dp1wb0aadqsvgb9csy9ciw8dazw519099697p416pj6djnr91l9")))

(define-public crate-healthchecks-monitor-1.0.1 (c (n "healthchecks-monitor") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("suggestions" "color"))) (k 0)) (d (n "healthchecks") (r "^1.0.1") (d #t) (k 0)) (d (n "pretty-exec-lib") (r "^0.0.17") (d #t) (k 0)))) (h "0f3k3x34yndrv405z3vxs64zgy2d76r0lmwh4bncfzlzspa211x1")))

(define-public crate-healthchecks-monitor-2.0.0 (c (n "healthchecks-monitor") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "healthchecks") (r "^2.0.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "0631nilb8mx4jwqrr3lzncm81flr2j5vdzfzmak62a9wfb8ysxsi")))

(define-public crate-healthchecks-monitor-3.0.0 (c (n "healthchecks-monitor") (v "3.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (k 0)) (d (n "healthchecks") (r "^3.0.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.7") (d #t) (k 0)))) (h "098y9c3yp4bsfglkxq6chirmqgw95clqzanz7ml4r246m8ainm84")))

(define-public crate-healthchecks-monitor-3.0.1 (c (n "healthchecks-monitor") (v "3.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.1") (k 0)) (d (n "healthchecks") (r "^3.1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "13lm1k79k1rbr12xhzzycp5m0xlpz4s0kyfprzpxzhs6bxixm2zi")))

(define-public crate-healthchecks-monitor-3.0.2 (c (n "healthchecks-monitor") (v "3.0.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "healthchecks") (r "^3.1.2") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "1fdizfwlj52flfkfls6km94997zmya8i3cf1rq6xqxng7cgz8cy4")))

(define-public crate-healthchecks-monitor-3.0.3 (c (n "healthchecks-monitor") (v "3.0.3") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "healthchecks") (r "^3.1.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "0gcqjb4h44fhnxrw957axn6rryljqvpli62lvkj92204mvkaj1qz")))

(define-public crate-healthchecks-monitor-3.0.4 (c (n "healthchecks-monitor") (v "3.0.4") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "healthchecks") (r "^3.1.4") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "03bx5cmk8kdlha90m3i6bkf14627js2mbdizzxyxxbmrlbrg1779")))

(define-public crate-healthchecks-monitor-3.0.5 (c (n "healthchecks-monitor") (v "3.0.5") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "healthchecks") (r "^3.1.5") (f (quote ("v3"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "1ghjc63yp9ad5dxvvl821d43nvq3fdg3lnzjnvzg2m1xiya3x19v")))

(define-public crate-healthchecks-monitor-3.0.6 (c (n "healthchecks-monitor") (v "3.0.6") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("color" "deprecated" "derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "healthchecks") (r "^3.1.6") (f (quote ("v3"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)))) (h "0z2dsgrwp8kcrrdp4ylh8v076m3ri715kh2053y9jwk8k729j8ml")))

