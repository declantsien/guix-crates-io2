(define-module (crates-io he al healthcheck-rs) #:use-module (crates-io))

(define-public crate-healthcheck-rs-0.1.0 (c (n "healthcheck-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0917fn8ll3y48ypqxp24scjs8zn9ybzi41c0y8vn3n1ikp6757hi")))

(define-public crate-healthcheck-rs-0.1.1 (c (n "healthcheck-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vjscz5mr0wik7gijkwmxiqyfrpbjjx0bw9qcx1c9day8mr4v8n2")))

(define-public crate-healthcheck-rs-0.1.2 (c (n "healthcheck-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19qh8am5rbsy390g45904bgnv5ifrnc7yvb7s3nzcg4qyjlx89ws")))

