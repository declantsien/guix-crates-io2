(define-module (crates-io he al healthdog) #:use-module (crates-io))

(define-public crate-healthdog-1.0.0 (c (n "healthdog") (v "1.0.0") (d (list (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)) (d (n "systemd") (r "^0.4") (d #t) (k 0)))) (h "0wvbvhhrfl1l4ms599sdbff3xz0y9wfmzx0hws4nzdw3ay5ilxwx")))

(define-public crate-healthdog-1.0.1 (c (n "healthdog") (v "1.0.1") (d (list (d (n "exec") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)) (d (n "systemd") (r "^0.4") (d #t) (k 0)))) (h "0m5dgkph1ixryy14n5w0rkfnsir7x3dwfajgakasijlds47k3hrl")))

