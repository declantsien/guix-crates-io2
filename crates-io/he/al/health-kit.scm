(define-module (crates-io he al health-kit) #:use-module (crates-io))

(define-public crate-health-kit-0.1.0 (c (n "health-kit") (v "0.1.0") (h "1ldgmacqlg48lzcg0vmiymffpjh09qr2byhazgrcdgfw7l1xqywl")))

(define-public crate-health-kit-0.1.1 (c (n "health-kit") (v "0.1.1") (h "1vqslp637rmim3163ikfh51l18ladg341qmnmic8ps00ww7nf5rs")))

