(define-module (crates-io he -r he-ring) #:use-module (crates-io))

(define-public crate-he-ring-0.1.1 (c (n "he-ring") (v "0.1.1") (d (list (d (n "feanor-math") (r "^1.5.2") (f (quote ("generic_tests"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "11il7f3cq6gr60p438fivzk6ws8r92rdcxnz3akbb2n5c1qwgkg2") (f (quote (("record_timings"))))))

(define-public crate-he-ring-0.2.0 (c (n "he-ring") (v "0.2.0") (d (list (d (n "feanor-math") (r "^1.6.0") (f (quote ("generic_tests"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0fvrp46mf03wyxs3vbby8bamf38zy9li2f800gnrbdw3vacf60ds") (f (quote (("record_timings"))))))

(define-public crate-he-ring-0.2.1 (c (n "he-ring") (v "0.2.1") (d (list (d (n "feanor-math") (r "^1.6.0") (f (quote ("generic_tests"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "09jgw0lyviqwwmvm2skm7smvjz2f9490mj82h58rxfn64qdkknw3") (f (quote (("record_timings"))))))

