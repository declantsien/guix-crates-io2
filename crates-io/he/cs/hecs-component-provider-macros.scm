(define-module (crates-io he cs hecs-component-provider-macros) #:use-module (crates-io))

(define-public crate-hecs-component-provider-macros-0.1.0 (c (n "hecs-component-provider-macros") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1") (d #t) (k 0)))) (h "0rmxv9xi35d0hjbbyql89ii2i1wq3hmspdp01z9ywz2xxwh9v675")))

(define-public crate-hecs-component-provider-macros-0.1.1 (c (n "hecs-component-provider-macros") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1") (d #t) (k 0)))) (h "1s8pgki2ap2a44q669lx7wk76nflx3p138ml46cvxmw7rg9cjwzy")))

(define-public crate-hecs-component-provider-macros-0.1.2 (c (n "hecs-component-provider-macros") (v "0.1.2") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1") (d #t) (k 0)))) (h "0z7gispl8h2n8d1dd70q4aq509i62xyb6gndmmjhllfzihvzqpch")))

(define-public crate-hecs-component-provider-macros-0.2.0 (c (n "hecs-component-provider-macros") (v "0.2.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1") (d #t) (k 0)))) (h "0dqrhnrfawwdvss41nh32cqy0qiz1rf7ssmzglvjsmp7h3pn4ag2")))

