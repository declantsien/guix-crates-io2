(define-module (crates-io he cs hecs-component-provider) #:use-module (crates-io))

(define-public crate-hecs-component-provider-0.1.0 (c (n "hecs-component-provider") (v "0.1.0") (d (list (d (n "gensym") (r "^0.1") (d #t) (k 0)) (d (n "hecs") (r ">0.3.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "hecs-component-provider-macros") (r "=0.1.0") (d #t) (k 0)))) (h "1s2q7hvrbc93601vdwf59ggb6yv8jddvdda94nm28zx09w01m4dw")))

(define-public crate-hecs-component-provider-0.1.1 (c (n "hecs-component-provider") (v "0.1.1") (d (list (d (n "gensym") (r "^0.1") (d #t) (k 0)) (d (n "hecs") (r ">0.3.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "hecs-component-provider-macros") (r "=0.1.1") (d #t) (k 0)))) (h "04mnvq7h730fl12m7rg27br12cqxy862m28gmlbbz5bmxv60mq03")))

(define-public crate-hecs-component-provider-0.1.2 (c (n "hecs-component-provider") (v "0.1.2") (d (list (d (n "gensym") (r "^0.1") (d #t) (k 0)) (d (n "hecs") (r ">0.3.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "hecs-component-provider-macros") (r "=0.1.2") (d #t) (k 0)))) (h "0y0kmhc67a35wy2b8idwzxlacdx97xpi8bkdy2kmx4ywp4yabb2l")))

(define-public crate-hecs-component-provider-0.2.0 (c (n "hecs-component-provider") (v "0.2.0") (d (list (d (n "gensym") (r "^0.1") (d #t) (k 0)) (d (n "hecs") (r ">0.3.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "hecs-component-provider-macros") (r "=0.2.0") (d #t) (k 0)))) (h "0cx2c95v9khr9brqyixpxfbhl0zj03ygg2lwcxrrcy6dpr7qy2pb")))

