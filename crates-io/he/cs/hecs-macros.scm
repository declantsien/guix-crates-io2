(define-module (crates-io he cs hecs-macros) #:use-module (crates-io))

(define-public crate-hecs-macros-0.1.0 (c (n "hecs-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive"))) (k 0)))) (h "186xgv5a6y7g87rgj43xg23ajn4g5dqj0fjzfg38jp2phxgqh794")))

(define-public crate-hecs-macros-0.2.0 (c (n "hecs-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive"))) (k 0)))) (h "1gfcpwxs2pswqywyib95x313rwyc4l349jcnczsl86x5g7wfvrvf")))

(define-public crate-hecs-macros-0.3.0 (c (n "hecs-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive"))) (k 0)))) (h "1sjjxq8090g9nnrmv0qavjaxmvgddxlx9hv9gy0cyl4q9ap1391g")))

(define-public crate-hecs-macros-0.4.0 (c (n "hecs-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive" "clone-impls" "visit-mut"))) (k 0)))) (h "0slz2i3lqy7rhpj0mgnkdyf8b2276karw78h05r1nc1gsmy03hvy")))

(define-public crate-hecs-macros-0.5.0 (c (n "hecs-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive" "clone-impls" "visit-mut"))) (k 0)))) (h "053wqblgl2m15jib1ssagx32xrx89z4m6mgi82qa2r6hmm97g42r")))

(define-public crate-hecs-macros-0.6.0 (c (n "hecs-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive" "clone-impls" "visit-mut"))) (k 0)))) (h "1fp7gzj2czzv9m0wvvjska7gfh9rbhv7bi3k8h4gsa9yf5db007k")))

(define-public crate-hecs-macros-0.7.0 (c (n "hecs-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive" "clone-impls" "visit-mut"))) (k 0)))) (h "1c0020x2l1abb5258ii8knj0l5dzyxzv85bnn4xxl1nisi8z2izb")))

(define-public crate-hecs-macros-0.7.1 (c (n "hecs-macros") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive" "clone-impls" "visit-mut"))) (k 0)))) (h "1vqmda5g1wzy18fw8q8jh8719nhz6k3xln2spfckkc90prz0rjws")))

(define-public crate-hecs-macros-0.8.0 (c (n "hecs-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive" "clone-impls" "visit-mut"))) (k 0)))) (h "0m18gwg5mj7g1zii12v0r5adzyrbk57ywz4avvqml2pvvsll9wxg")))

(define-public crate-hecs-macros-0.8.1 (c (n "hecs-macros") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive" "clone-impls" "visit-mut"))) (k 0)))) (h "10dqk17xgxy49p1krs5zvyvbd8cfc5wr9czbz20pdnjifms5jy28")))

(define-public crate-hecs-macros-0.8.2 (c (n "hecs-macros") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive" "clone-impls" "visit-mut"))) (k 0)))) (h "0r9pzlknkv4cfkgmj691vyx1xn8p653vj893vnsn68yg1nvkz20l")))

(define-public crate-hecs-macros-0.9.0 (c (n "hecs-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("proc-macro" "parsing" "printing" "derive" "clone-impls" "visit-mut"))) (k 0)))) (h "1l6yh6n9wh5d44pqya8rw0qrmcs2y8pqg6b808s961f5w06840vq")))

(define-public crate-hecs-macros-0.10.0 (c (n "hecs-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("proc-macro" "parsing" "printing" "derive" "clone-impls" "visit-mut"))) (k 0)))) (h "0w69bxb8pxd3m9zymsf0l0psfl30ny70kljwc2160cnw29dw4bq5")))

