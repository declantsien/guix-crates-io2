(define-module (crates-io he vc hevc_parser) #:use-module (crates-io))

(define-public crate-hevc_parser-0.1.0 (c (n "hevc_parser") (v "0.1.0") (d (list (d (n "bitvec_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "0zs96lric2vwhpxpa4nqfbvnc8sszkyv404fg6d3v6cvqj3akls5")))

(define-public crate-hevc_parser-0.1.1 (c (n "hevc_parser") (v "0.1.1") (d (list (d (n "bitvec_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "0jgnmqjl8qryi9sxfcmwxlj8zk35a3w33vj4gd82z8p747sgpq53")))

(define-public crate-hevc_parser-0.1.2 (c (n "hevc_parser") (v "0.1.2") (d (list (d (n "bitvec_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "1dgkfxdn7j5pwv22q9xnavdzm1nya9wh0lc91i3innks3h9calhl")))

(define-public crate-hevc_parser-0.1.3 (c (n "hevc_parser") (v "0.1.3") (d (list (d (n "bitvec_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "0mijvgdb6cxv2hgn2ywcj8ilg19w1vjg7wn310gzf337rs8fqsn1")))

(define-public crate-hevc_parser-0.1.4 (c (n "hevc_parser") (v "0.1.4") (d (list (d (n "bitvec_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "0w6awp2nys7l7gw0vgv9kj5a3g5f0x1vw2c3mggy79bcwh7qlabz") (y #t)))

(define-public crate-hevc_parser-0.1.5 (c (n "hevc_parser") (v "0.1.5") (d (list (d (n "bitvec_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "1b55w4f5z87d1bi06szrja9bn7bxyf14ywxac6bpl1qvq6y9qyc3")))

(define-public crate-hevc_parser-0.1.6 (c (n "hevc_parser") (v "0.1.6") (d (list (d (n "bitvec_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "1djjz47csj7zn01mj7mppvvv8hj839y0jcv2bmx57cgdfxncvxvg")))

(define-public crate-hevc_parser-0.1.7 (c (n "hevc_parser") (v "0.1.7") (d (list (d (n "bitvec_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "0lqgd26rhqv840naa8n4sc9rm05nzviy2i8by3m9z9ycy5wvfgxj")))

(define-public crate-hevc_parser-0.2.0 (c (n "hevc_parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)))) (h "161cmncz6rs1483ckbgkis5di0dbgy5lylfw8h1wwh8y7a5yf0sb")))

(define-public crate-hevc_parser-0.2.1 (c (n "hevc_parser") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "1ls3iqnxxr9l48sx13hryfih4chid2lclhh8aq3hjqpgxqzh984w")))

(define-public crate-hevc_parser-0.3.0 (c (n "hevc_parser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "005hw25b3r1jl17asgvwzshjqjcv6l9pz6gggq9j9v5hx8g8r468")))

(define-public crate-hevc_parser-0.3.1 (c (n "hevc_parser") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0jn2mxnra0ik0njzcvbh4wdd15s2ysmk183331x83f9qv9d8mmx0")))

(define-public crate-hevc_parser-0.3.2 (c (n "hevc_parser") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "1kwir897bi239v5a756ii4rg1vmkqcv4zqnb8apwr6j5k0i8shn0") (r "1.56.0")))

(define-public crate-hevc_parser-0.3.3 (c (n "hevc_parser") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "112mb7fcpgi4zrhasy22ik7k0is35v3vbm7ra43n4py3r4qy50b1") (r "1.56.0")))

(define-public crate-hevc_parser-0.3.4 (c (n "hevc_parser") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "18v3aschmyq6xd7nz4s9ww5v3v67idhl7vzp1v0708n8fnyc6gj7") (r "1.56.0")))

(define-public crate-hevc_parser-0.4.0 (c (n "hevc_parser") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "19yzpw5nnxwxsfskm0l4g6m5sh6rm1rmg3g32dxa869y3yx4qgy8") (f (quote (("hevc_io" "regex") ("default" "hevc_io")))) (y #t) (r "1.56.0")))

(define-public crate-hevc_parser-0.4.1 (c (n "hevc_parser") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "11r8mmg0p0lkj78g5czak0pipqhim7j0gxvld6plczb5cal4d5fi") (f (quote (("hevc_io" "regex")))) (r "1.56.0")))

(define-public crate-hevc_parser-0.4.2 (c (n "hevc_parser") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "13dkj4l73j02wkai09wx28z7mmc8hd3xz2w4qxwdv26rnpd1wk3k") (f (quote (("hevc_io" "regex")))) (r "1.56.0")))

(define-public crate-hevc_parser-0.4.3 (c (n "hevc_parser") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "03bb77s274qq73yw5nxbn9cxqbklwrzgi3h9mw8qscphay5zkk7k") (f (quote (("hevc_io" "regex")))) (r "1.56.0")))

(define-public crate-hevc_parser-0.4.4 (c (n "hevc_parser") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "194mvvb9w5y39l8nqbh4hvcaghbwm0szzka8lbyxc6yi13d33n8c") (f (quote (("hevc_io" "regex")))) (r "1.56.0")))

(define-public crate-hevc_parser-0.4.5 (c (n "hevc_parser") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "007dbr4dd8pfq0c5n8zcjsajv268055vj88ygn1az6w4yqa95dbg") (f (quote (("hevc_io" "regex")))) (r "1.56.0")))

(define-public crate-hevc_parser-0.4.6 (c (n "hevc_parser") (v "0.4.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (o #t) (d #t) (k 0)))) (h "0f1sh6yj8f65f2h0sxbnnqf7fm276x8g8cbnrb62jw9j0h0q8hs6") (f (quote (("hevc_io" "regex")))) (r "1.56.0")))

(define-public crate-hevc_parser-0.4.7 (c (n "hevc_parser") (v "0.4.7") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^1.0.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "09lxklggd1l9nvnmyx0qvbmay2zckfpf8nc8la8s2xj96zyfyz91") (f (quote (("hevc_io" "regex")))) (r "1.56.1")))

(define-public crate-hevc_parser-0.5.0 (c (n "hevc_parser") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^2.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "0l46mx769fp6jgk8s2h56nsvqzji8s91rcdzxr3m5jqw00kfcq9d") (f (quote (("hevc_io" "regex")))) (r "1.56.1")))

(define-public crate-hevc_parser-0.5.1 (c (n "hevc_parser") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^2.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "1zwyv72njabain63ikjqyhny1qml7a1kjdwb600d9k8y5i6s0dhy") (f (quote (("hevc_io" "regex")))) (r "1.56.1")))

(define-public crate-hevc_parser-0.5.2 (c (n "hevc_parser") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^2.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "05cd3lfmbsdc774h2llvj34id39crb0m0h7qd91qrj41l44z4d5b") (f (quote (("hevc_io" "regex")))) (r "1.56.1")))

(define-public crate-hevc_parser-0.6.0 (c (n "hevc_parser") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^3.1.2") (f (quote ("bitstream-io"))) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (o #t) (d #t) (k 0)))) (h "1955z0vzwglvzmis0p1vrx6pmg1wiz3b0lgq5g8v4ybnjqmqyynf") (f (quote (("hevc_io" "regex")))) (r "1.56.1")))

(define-public crate-hevc_parser-0.6.1 (c (n "hevc_parser") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^3.1.2") (f (quote ("bitstream-io"))) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (o #t) (d #t) (k 0)))) (h "05zn1ykcwfbz2c966l8cn72b84m2gdyi97ca6rpc7nl09qxvciz4") (f (quote (("hevc_io" "regex")))) (r "1.60.0")))

(define-public crate-hevc_parser-0.6.2 (c (n "hevc_parser") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "bitvec_helpers") (r "^3.1.3") (f (quote ("bitstream-io"))) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (o #t) (d #t) (k 0)))) (h "0vs8mnpxczsjrj5fl629ck3iv31ysim6zgdi97sn3cvky7wz1ssi") (f (quote (("hevc_io" "regex")))) (r "1.70.0")))

