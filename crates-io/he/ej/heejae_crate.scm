(define-module (crates-io he ej heejae_crate) #:use-module (crates-io))

(define-public crate-heejae_crate-0.1.0 (c (n "heejae_crate") (v "0.1.0") (h "1qngz8psy385vg7946hddn0wqy65jmalqxazj1gqwl0vaag3gw0w")))

(define-public crate-heejae_crate-0.1.1 (c (n "heejae_crate") (v "0.1.1") (h "0cwa8m4pki1bly80k59bn2qfp1iayw68y4y6jl5q61yvrl25abxs")))

