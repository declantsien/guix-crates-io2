(define-module (crates-io he rc hercules) #:use-module (crates-io))

(define-public crate-hercules-0.1.0 (c (n "hercules") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "smolprng") (r "^0.1.5") (d #t) (k 0)) (d (n "sprs") (r "^0.11.1") (d #t) (k 0)))) (h "1jh4jy25k72wxvp0mawkyf1zdqqvh56kqmic3121hyysy6wc4fw2")))

(define-public crate-hercules-0.2.0 (c (n "hercules") (v "0.2.0") (d (list (d (n "clarabel") (r "^0.7.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("intel-mkl-static"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (f (quote ("extension-module" "abi3-py37"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "smolprng") (r "^0.1.6") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "sprs") (r "^0.11.1") (d #t) (k 0)))) (h "12f3kzd4v0xii54r1mc649z10rsz8wf9m1cxww31jpb59yknc1s7")))

