(define-module (crates-io he rc herco) #:use-module (crates-io))

(define-public crate-herco-0.1.0 (c (n "herco") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aph8nn8c087cwibpbj5ckc6v4dc6jh3jki7arg5ms14xxw59xqf") (y #t)))

