(define-module (crates-io he _d he_di_derive) #:use-module (crates-io))

(define-public crate-he_di_derive-0.1.0 (c (n "he_di_derive") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "he_di_internals") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0i7k5bhx7cpj8y963690cqccw66vckjrrxca2g2ds5ba31jjldjh")))

(define-public crate-he_di_derive-0.1.1 (c (n "he_di_derive") (v "0.1.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "he_di") (r "^0.1") (d #t) (k 2)) (d (n "he_di_internals") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "164dqyknxjib1j3ik1zjd39kkbdja055s9i6lzlr43n25w5504g2")))

(define-public crate-he_di_derive-0.2.0 (c (n "he_di_derive") (v "0.2.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "he_di") (r "^0.2") (d #t) (k 2)) (d (n "he_di_internals") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0bpk3lj72anxw6n81cah9zrfbamkgpxy80h3ljpj83akqxb19gz5")))

(define-public crate-he_di_derive-0.2.1 (c (n "he_di_derive") (v "0.2.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "he_di") (r "^0.2.1") (d #t) (k 2)) (d (n "he_di_internals") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1ywdz7dk4cin3s60gar6pfml26v1bkp2yrk3lfd3jp1h5jm7xvff")))

