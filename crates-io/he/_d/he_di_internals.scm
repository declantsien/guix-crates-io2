(define-module (crates-io he _d he_di_internals) #:use-module (crates-io))

(define-public crate-he_di_internals-0.1.0 (c (n "he_di_internals") (v "0.1.0") (h "0nf4bdrihawxwrp27069piv5dzjj4zf1qyxm500pw60kvp1x931q")))

(define-public crate-he_di_internals-0.2.0 (c (n "he_di_internals") (v "0.2.0") (d (list (d (n "fluent_validator") (r "^0.1.0") (d #t) (k 0)))) (h "0airgiw652vghv3g1vmgf27d524qf4afh7jx5q2h8ddq6p6zvbg1")))

