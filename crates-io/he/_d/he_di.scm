(define-module (crates-io he _d he_di) #:use-module (crates-io))

(define-public crate-he_di-0.1.0 (c (n "he_di") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "he_di_derive") (r "^0.1") (d #t) (k 2)) (d (n "he_di_internals") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "unsafe-any") (r "^0.4") (d #t) (k 0)))) (h "15jkk8qjjyvf7471d90q8jx8lqixbhkjzmg2znylgaiffayc1wwh")))

(define-public crate-he_di-0.1.1 (c (n "he_di") (v "0.1.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "he_di_derive") (r "^0.1") (d #t) (k 2)) (d (n "he_di_internals") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "unsafe-any") (r "^0.4") (d #t) (k 0)))) (h "00xr0vr5jmslwwhnqzk9k6glm8sdyzk57pnjybdmykfr0lmfmqp9")))

(define-public crate-he_di-0.2.0 (c (n "he_di") (v "0.2.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "he_di_derive") (r "^0.2") (d #t) (k 2)) (d (n "he_di_internals") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "unsafe-any") (r "^0.4") (d #t) (k 0)))) (h "120i0d8ip4akl4hx1y41fqci26rd5q8d6idf4zffhid4qrjf4l99")))

(define-public crate-he_di-0.2.1 (c (n "he_di") (v "0.2.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)) (d (n "fluent_validator") (r "^0.1.0") (d #t) (k 0)) (d (n "he_di_derive") (r "^0.2.1") (d #t) (k 2)) (d (n "he_di_internals") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "unsafe-any") (r "^0.4") (d #t) (k 0)))) (h "0a0l9n86b5pd7mlj6k603x91q5wbcv9xshx735yh0f8qypb9c223")))

