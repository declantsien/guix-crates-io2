(define-module (crates-io he x2 hex2d-dpcext) #:use-module (crates-io))

(define-public crate-hex2d-dpcext-0.0.1 (c (n "hex2d-dpcext") (v "0.0.1") (d (list (d (n "hex2d") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "075pj8k384h8nn6yl6p20qxrw7g5lw39f0b9zxzyr06v7z01m1g7")))

(define-public crate-hex2d-dpcext-0.0.2 (c (n "hex2d-dpcext") (v "0.0.2") (d (list (d (n "hex2d") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1z69wy5rq3pa1y5wmyjny34slcdb71v8bnl5r6wj9g9vvxbbwvwi")))

(define-public crate-hex2d-dpcext-0.0.3 (c (n "hex2d-dpcext") (v "0.0.3") (d (list (d (n "hex2d") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0di5dkw2g461rxs1kc9f30vkx8wkz0yhmrkds5aw9ilbh201slix")))

(define-public crate-hex2d-dpcext-0.0.4 (c (n "hex2d-dpcext") (v "0.0.4") (d (list (d (n "hex2d") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1rxrljmhwcn2kypnk577i1r9kzjz3w5gww2w7nk8jm98y7hq2wi4")))

(define-public crate-hex2d-dpcext-0.0.5 (c (n "hex2d-dpcext") (v "0.0.5") (d (list (d (n "hex2d") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1j4xm87vbd7wfh20iq4kg41fckh1ah048pyjxgcwg74lyzzhd30f")))

(define-public crate-hex2d-dpcext-0.0.7 (c (n "hex2d-dpcext") (v "0.0.7") (d (list (d (n "hex2d") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0r3c79wr18nmhf6fr52sbbwnaz1xl3asm97fw7mwnbpyb7p4y0pg")))

(define-public crate-hex2d-dpcext-0.0.8 (c (n "hex2d-dpcext") (v "0.0.8") (d (list (d (n "hex2d") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0wgfkxdqbhz8ph73glc7hz7kz65pxhh2l08s48py11pfa2cnpfqr")))

(define-public crate-hex2d-dpcext-0.0.9 (c (n "hex2d-dpcext") (v "0.0.9") (d (list (d (n "hex2d") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0s2qqf1p97a8wss4fgg3cz57jcxk4w1y5sdy9bjqblgp4r9na0zx")))

(define-public crate-hex2d-dpcext-0.1.0 (c (n "hex2d-dpcext") (v "0.1.0") (d (list (d (n "hex2d") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "1mhklcynmm5zaziiyb721kz6094v12n3zn2d8x6iwnr87331jm92")))

