(define-module (crates-io he x2 hex2dec) #:use-module (crates-io))

(define-public crate-hex2dec-0.0.1 (c (n "hex2dec") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1fdwzd99icm3mciafb2h3xvl82cfi36ra7fpafk0amnlhrlc9b0j")))

