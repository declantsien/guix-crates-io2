(define-module (crates-io he x2 hex2ascii) #:use-module (crates-io))

(define-public crate-hex2ascii-0.1.0 (c (n "hex2ascii") (v "0.1.0") (d (list (d (n "clap") (r "^2.21.2") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)))) (h "1qmn596pm5y4p8kdwg00ij11zg6i3l7jg7mi9gdk0w1cnzxswx3l")))

(define-public crate-hex2ascii-0.2.0 (c (n "hex2ascii") (v "0.2.0") (d (list (d (n "clap") (r "^2.21.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "1n9wm303al7gcyzdqvf4rav287a82fzz9vyvl20b5lazi319z3cy")))

(define-public crate-hex2ascii-0.2.1 (c (n "hex2ascii") (v "0.2.1") (d (list (d (n "clap") (r "^2.21.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "1z3qqh0i60sb7j99vfggfn9ikw9z233czqr1w138msm9rxgy89w2")))

(define-public crate-hex2ascii-1.0.0 (c (n "hex2ascii") (v "1.0.0") (d (list (d (n "clap") (r "^2.21.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "0ypdq2fx468kb9zl4mym145s0f8hwf7qqna9gvqi2m7fdw5i6rrn")))

(define-public crate-hex2ascii-1.0.1 (c (n "hex2ascii") (v "1.0.1") (d (list (d (n "clap") (r "^2.21.2") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)))) (h "0dhb6ymdgqa09m27y7ca93q3701swzxbyvnjw2x5k41h37b85zd2")))

(define-public crate-hex2ascii-1.0.2 (c (n "hex2ascii") (v "1.0.2") (d (list (d (n "clap") (r "^2.21.2") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)))) (h "03499dg9gzzacpk385dmja8rwshpvk94bc8yimnqzlpx2mw15fcf")))

