(define-module (crates-io he xh hexhex_impl) #:use-module (crates-io))

(define-public crate-hexhex_impl-0.1.0 (c (n "hexhex_impl") (v "0.1.0") (d (list (d (n "fallible-iterator") (r "^0.2.0") (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)))) (h "0bjzrxi8q5yqnlypmzp55d0g8bq2dzdhiwhq92d3v9vy5g61s2hk") (f (quote (("std" "fallible-iterator/std") ("proptest" "std") ("default"))))))

(define-public crate-hexhex_impl-0.1.1 (c (n "hexhex_impl") (v "0.1.1") (d (list (d (n "fallible-iterator") (r "^0.2.0") (k 0)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)))) (h "0jhr5vxz0glw347j1z3rd6mrswrzivd24zgn50832q8r11k0qlqz") (f (quote (("std" "fallible-iterator/std") ("proptest" "std") ("default"))))))

