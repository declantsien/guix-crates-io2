(define-module (crates-io he xh hexhex) #:use-module (crates-io))

(define-public crate-hexhex-1.0.0 (c (n "hexhex") (v "1.0.0") (d (list (d (n "hexhex_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "hexhex_macros") (r "^1.0.0") (d #t) (k 0)))) (h "14fws0lx0hi5blnp42p0r57zrr4r72zs3hba3ns8v4gjlpvfpcag") (f (quote (("std" "hexhex_impl/std") ("default" "std"))))))

(define-public crate-hexhex-1.1.0 (c (n "hexhex") (v "1.1.0") (d (list (d (n "hexhex_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "hexhex_macros") (r "^1.0.0") (d #t) (k 0)))) (h "1chsni8lgm0yvhz63gfjm16b6cwxsqh9bsfdfl1r3iw22b24vydj") (f (quote (("std" "hexhex_impl/std") ("default" "std"))))))

(define-public crate-hexhex-1.1.1 (c (n "hexhex") (v "1.1.1") (d (list (d (n "hexhex_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "hexhex_macros") (r "^1.0.0") (d #t) (k 0)))) (h "1z6j7qknq6w6xcqhryshzppa7sfihcfiz92y2pwlv3briac94rf5") (f (quote (("std" "hexhex_impl/std") ("default" "std"))))))

