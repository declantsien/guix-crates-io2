(define-module (crates-io he mt hemtt-pbo) #:use-module (crates-io))

(define-public crate-hemtt-pbo-1.0.0 (c (n "hemtt-pbo") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hemtt-error") (r "^1.0.0") (d #t) (k 0)) (d (n "hemtt-io") (r "^1.0.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.10.1") (d #t) (k 0)))) (h "1cklbw8hwjmx5slg3p82wjq8f3pq9l8qpvh1dk0issi3zishp7yr")))

