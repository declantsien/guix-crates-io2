(define-module (crates-io he mt hemtt-signing) #:use-module (crates-io))

(define-public crate-hemtt-signing-1.0.0 (c (n "hemtt-signing") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hemtt-error") (r "^1.0.0") (d #t) (k 0)) (d (n "hemtt-io") (r "^1.0.0") (d #t) (k 0)) (d (n "hemtt-pbo") (r "^1.0.0") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.8.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.10.1") (d #t) (k 0)))) (h "0b74ayrycp4ilix35i6xrjzfl9qpl1nxpdxqasbp9biha1z6a20i")))

