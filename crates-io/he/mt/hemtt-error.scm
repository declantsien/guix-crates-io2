(define-module (crates-io he mt hemtt-error) #:use-module (crates-io))

(define-public crate-hemtt-error-1.0.0 (c (n "hemtt-error") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "hemtt-tokens") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1mbb6n6wznf5sd6z1fhidmqiby3jgaq53pclxvwip13ffkbxkdhx")))

