(define-module (crates-io he mt hemtt-version) #:use-module (crates-io))

(define-public crate-hemtt-version-1.0.0 (c (n "hemtt-version") (v "1.0.0") (d (list (d (n "hemtt-error") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qdxg5rm103da4hvcf3r146lmx1r9m9430y2r532k81b36411jba")))

