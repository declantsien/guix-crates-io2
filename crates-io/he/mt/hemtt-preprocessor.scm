(define-module (crates-io he mt hemtt-preprocessor) #:use-module (crates-io))

(define-public crate-hemtt-preprocessor-1.0.0 (c (n "hemtt-preprocessor") (v "1.0.0") (d (list (d (n "hemtt-error") (r "^1.0.0") (d #t) (k 0)) (d (n "hemtt-tokens") (r "^1.0.0") (d #t) (k 0)) (d (n "peekmore") (r "^1.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "03yszl7h0gk6qi1zj33pcpvl7hvkjm8nvj82xgd4680fn0l60sxf")))

