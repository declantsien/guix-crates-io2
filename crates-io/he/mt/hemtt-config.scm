(define-module (crates-io he mt hemtt-config) #:use-module (crates-io))

(define-public crate-hemtt-config-1.0.0 (c (n "hemtt-config") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hemtt-error") (r "^1.0.0") (d #t) (k 0)) (d (n "hemtt-tokens") (r "^1.0.0") (d #t) (k 0)) (d (n "peekmore") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)))) (h "0456mlnnwhllhll6l94chpgr2j2a28zidjrc9xms8d8v2r64r89j")))

