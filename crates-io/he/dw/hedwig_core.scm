(define-module (crates-io he dw hedwig_core) #:use-module (crates-io))

(define-public crate-hedwig_core-0.1.0 (c (n "hedwig_core") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "smallstr") (r "^0.3.0") (f (quote ("union"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (f (quote ("v4"))) (k 0)))) (h "08d51nxvqpbb38x60sdiyx5hmkm1l37bhirqz3hnzz8hrspy4824")))

