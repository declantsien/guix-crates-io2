(define-module (crates-io he l- hel-random) #:use-module (crates-io))

(define-public crate-hel-random-0.1.0 (c (n "hel-random") (v "0.1.0") (h "1h1qvf9i2fbqwf6if466g2f7319bdm0bnkda16xsc8nj1pqlncr1")))

(define-public crate-hel-random-0.1.1 (c (n "hel-random") (v "0.1.1") (h "10myrdgvbr2cm3yzxif9nsawp403947gsmxapgfa5k2gg1zm5n69")))

(define-public crate-hel-random-0.2.0 (c (n "hel-random") (v "0.2.0") (h "01kqwzmpqa74wsz16xcg4bvb9ys13hwdn8wf4bq7ynl428hh0qlg")))

(define-public crate-hel-random-0.3.0 (c (n "hel-random") (v "0.3.0") (h "1ss7mq1aba17m7hvjba0y0z6rlmrygrk4a11aks11pr9h4dvc89y")))

(define-public crate-hel-random-0.4.0 (c (n "hel-random") (v "0.4.0") (h "1ml7g1vrz49qj1ia7jw7hrf7q1gws9yapp2r51k17lqcl43q9fx9")))

