(define-module (crates-io he l- hel-time) #:use-module (crates-io))

(define-public crate-hel-time-0.1.0 (c (n "hel-time") (v "0.1.0") (h "0g7n865ahf6yi9sqyrcnl23w734y9lgllannhn7incq5vj3dbry0")))

(define-public crate-hel-time-0.2.0 (c (n "hel-time") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 2)))) (h "19w1akj8wc7i8r5cq10w8jlr793s2r24hnwbba0499h9653i7qx0")))

(define-public crate-hel-time-0.3.0 (c (n "hel-time") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 2)))) (h "0ywcs84rgsp44kgwcvlyvl6xshrhx5dfqanr4gd3s4b6ngm5asc0")))

(define-public crate-hel-time-0.4.0 (c (n "hel-time") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 2)))) (h "0grfrqcz1krs66y0gm5qlz74hy2nvfz00zaqw45w2kcwgsbbvhn6")))

(define-public crate-hel-time-0.4.1 (c (n "hel-time") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 2)))) (h "0617svdwwghs69p2mhrfy0la6gr6ppjakvcixliqkpdppw5b7z10")))

