(define-module (crates-io he l- hel-prompt) #:use-module (crates-io))

(define-public crate-hel-prompt-0.1.0 (c (n "hel-prompt") (v "0.1.0") (h "05qld58hk60hisk15fjvi0gn0ia716wqm01blxnm5dn7cjrvd9za")))

(define-public crate-hel-prompt-0.1.1 (c (n "hel-prompt") (v "0.1.1") (h "0n1vc92xyal1r3v8fgwmi4k89q8iy1yal6lxmd60sw77v8pxpz08")))

(define-public crate-hel-prompt-1.0.0 (c (n "hel-prompt") (v "1.0.0") (h "0fchpkydqbqzs1f13wqsc32ws5bd5xva69qwfj8i3whbz5h5qzrn")))

