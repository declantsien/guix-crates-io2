(define-module (crates-io he l- hel-thread-pool) #:use-module (crates-io))

(define-public crate-hel-thread-pool-0.1.0 (c (n "hel-thread-pool") (v "0.1.0") (h "0yhihwvzbpa5lwp4wl97fraan4nsay4mqcs839w87spwvmmgk5sy") (y #t)))

(define-public crate-hel-thread-pool-0.1.1 (c (n "hel-thread-pool") (v "0.1.1") (h "1whfzc2gnrx6cvjillll1f63srp8bslmy27jhy35qln5wb9cnf6f")))

(define-public crate-hel-thread-pool-0.1.2 (c (n "hel-thread-pool") (v "0.1.2") (h "0km4qlg5cgkryhcp76j7r45aq0p83cz9prz4jf801lglsl27dv8f")))

(define-public crate-hel-thread-pool-0.2.0 (c (n "hel-thread-pool") (v "0.2.0") (h "0vd8cqljlbg0l5r99smz59hrgqg388x2hg8sv5scprd32yiish1b")))

(define-public crate-hel-thread-pool-0.3.0 (c (n "hel-thread-pool") (v "0.3.0") (h "0yx7j7rapjxvmj8h9i1v9rxq4c7cw05a9izk54vlyy5rrwj66sv5")))

(define-public crate-hel-thread-pool-0.3.1 (c (n "hel-thread-pool") (v "0.3.1") (h "1w9iwja3zvi06nkz3mcp1f3l46d16nyr0x5v671nwzdx9dv75dlc")))

