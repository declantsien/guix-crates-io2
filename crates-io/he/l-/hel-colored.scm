(define-module (crates-io he l- hel-colored) #:use-module (crates-io))

(define-public crate-hel-colored-0.1.0 (c (n "hel-colored") (v "0.1.0") (h "086x82fniapdfkda50ciz12cbws2264ypzvah91mj0r12d5nrgwf")))

(define-public crate-hel-colored-0.2.0 (c (n "hel-colored") (v "0.2.0") (h "1658q5nv9j7d4891xyvbapd3yq3vps9i0mywmcgqk4njq7qfj0fl")))

(define-public crate-hel-colored-0.3.1 (c (n "hel-colored") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)))) (h "1q7mb82qncr574h3f8dpz24x87r7g9mmaqi6c0achkv69zz36jby")))

(define-public crate-hel-colored-0.3.2 (c (n "hel-colored") (v "0.3.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)))) (h "1bdw7vg9h0g19hy010qml0wf0xdafxkldrm4jpqh9d0vr38xs3l8")))

(define-public crate-hel-colored-0.4.0 (c (n "hel-colored") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)))) (h "0p4rifz39pjhp19byfl0gfqvjm2cmd5rfgbcba86npfwf69x8gif")))

(define-public crate-hel-colored-0.4.3 (c (n "hel-colored") (v "0.4.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)))) (h "0l02xi0jgs6dg8dc93j4lwir39f7a4b3dvpn0sd5ddwchh2rw2d1")))

(define-public crate-hel-colored-0.4.4 (c (n "hel-colored") (v "0.4.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)))) (h "07hs90pb8rvx0n4d9awv1ifzmn1g57dzzp2pmkifc3rhyrb6vrfq")))

(define-public crate-hel-colored-0.5.0 (c (n "hel-colored") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)))) (h "0yqz4cmvdxhj8bxz2xdj727qs6jpa71k17pyvzv9xlb6wxhzkv0c") (f (quote (("nested"))))))

(define-public crate-hel-colored-0.5.1 (c (n "hel-colored") (v "0.5.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)))) (h "1dqz0mnmq7czc9d1b1zdbszvmd20dvgdgl7y8m2yk4qslvf3f2yc") (f (quote (("nested"))))))

