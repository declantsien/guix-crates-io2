(define-module (crates-io he xo hexo) #:use-module (crates-io))

(define-public crate-hexo-0.1.0 (c (n "hexo") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "0d8qvbaph90ff043wvp6mqm16briyf7mca1y6fa9fhqfdvqqq1w8")))

(define-public crate-hexo-0.1.1 (c (n "hexo") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "1ysx5j772i76dkxwhfqh32a8r23p15b9f0fsh45z2n4sriik7fqi")))

(define-public crate-hexo-0.1.2 (c (n "hexo") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "0hw043szrfa47xby6hb8hib31a75jlcvllnv73m34x5jjfd2cp2c")))

(define-public crate-hexo-0.1.3 (c (n "hexo") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "04fkk4pak5skb9mdm874pjr93gz3mjjkfpdpp1szcjvm2jllf4li")))

(define-public crate-hexo-0.1.4 (c (n "hexo") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "0lsxqmncdfi0qf83223f897jw9q20fv2l7cbza1fch3nndq8zp20")))

(define-public crate-hexo-0.2.0 (c (n "hexo") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "02zvqhgr9jy2nmpj6f9bhziv6g5khd7hnabhvph4cwk84fp31qvz")))

(define-public crate-hexo-0.3.0 (c (n "hexo") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "1f4mmw5ds49vlb3p7b9s16yjshmzy7vlryhmkakdx0k6cc7kfqf7")))

(define-public crate-hexo-0.4.0 (c (n "hexo") (v "0.4.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "1dizv0igdn211zbh9qrd3w7bk3n898nc56sydc7n6amsa14siaxm")))

(define-public crate-hexo-0.5.0 (c (n "hexo") (v "0.5.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "19a0dirgmar1vfnlg4k67vll5j7imdms8fbn6bss9z50jhfch6s0")))

(define-public crate-hexo-0.6.0 (c (n "hexo") (v "0.6.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "1bj0zwbdv38vv2npv04fq14mq4i2qpcvj500p24r7rn1r1mh94zd")))

(define-public crate-hexo-0.6.1 (c (n "hexo") (v "0.6.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "06x2564km50np2a2afr56zkx6mjxnb1wad3hp75nj75yf4ikdvgw")))

(define-public crate-hexo-0.6.2 (c (n "hexo") (v "0.6.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.7.9") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.9") (d #t) (k 0)))) (h "1663fzwv7s46pc1wsl0pccmjyisqdc5q6lwv6dw98l3bkpcgy4cd")))

