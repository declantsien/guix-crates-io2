(define-module (crates-io he xe hexevent) #:use-module (crates-io))

(define-public crate-hexevent-0.1.0 (c (n "hexevent") (v "0.1.0") (h "08sgcbhlgfajagl7pq4f5b7llbp46p08r18brv1fl4yha8m9xyr7")))

(define-public crate-hexevent-0.1.1 (c (n "hexevent") (v "0.1.1") (h "023ms22d8cxc60isv1z6n4g1b03sw48b6a21x9gvpznq0ryj8plf") (y #t)))

(define-public crate-hexevent-0.1.2 (c (n "hexevent") (v "0.1.2") (h "1y4byy5nwl6n5qandag8ip22fv4zh90qxmjsx1hxkj4d5yj92d2c") (y #t)))

(define-public crate-hexevent-0.1.3 (c (n "hexevent") (v "0.1.3") (h "1vh91wx9qfckvs6y0zv6wib4zmymg6zsl4ljqjv0d5k9jqy1rgnj")))

