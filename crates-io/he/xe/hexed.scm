(define-module (crates-io he xe hexed) #:use-module (crates-io))

(define-public crate-hexed-0.1.0 (c (n "hexed") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)))) (h "0yi0pz9h96h0w409mdw4sibnm0yw37kkv2rfmyydcp5yqiwxw1y4")))

(define-public crate-hexed-0.2.0 (c (n "hexed") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("suggestions" "wrap_help"))) (k 0)))) (h "00z6ap4y3lnjh3cj93cn2m5xxfflm3px1fxj4a19j23dakbc1ha5")))

