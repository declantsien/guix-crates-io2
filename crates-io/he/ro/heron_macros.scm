(define-module (crates-io he ro heron_macros) #:use-module (crates-io))

(define-public crate-heron_macros-0.6.0 (c (n "heron_macros") (v "0.6.0") (d (list (d (n "heron_core") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16xw16p53sm5aljgvjyrfcqsz8zjij7a1mgwhfig2y2fzc6cw519")))

(define-public crate-heron_macros-0.7.0 (c (n "heron_macros") (v "0.7.0") (d (list (d (n "heron_core") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vhrzp7qy7ksmfkq48qszjlpv927qp8ag3fynj4rn34z9wjx96vr")))

(define-public crate-heron_macros-0.8.0 (c (n "heron_macros") (v "0.8.0") (d (list (d (n "heron_core") (r "^0.8.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i1xrwrjm9igapmh6l0m0r4a4lig94v9pa00gf8ba66lm417mhnx")))

(define-public crate-heron_macros-0.9.0 (c (n "heron_macros") (v "0.9.0") (d (list (d (n "heron_core") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ssxzbvi52z10via903h92b38irnbc9a61q02xa9h772w5hbvq5d")))

(define-public crate-heron_macros-0.9.1 (c (n "heron_macros") (v "0.9.1") (d (list (d (n "heron_core") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z1cb9zalj8599023cdan9xxddysbrnpqs7xif27q8dam5ia4sba")))

(define-public crate-heron_macros-0.10.0 (c (n "heron_macros") (v "0.10.0") (d (list (d (n "heron_core") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y2cbya6njb3a97fcavl0hyf2xhj9icxhaij4wxhnznwqhkv5kj0")))

(define-public crate-heron_macros-0.10.1 (c (n "heron_macros") (v "0.10.1") (d (list (d (n "heron_core") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bz1l93dvlwqq0qr6cnhh188g8nypv8hfmbah8f5g85vvmyr5h22") (y #t)))

(define-public crate-heron_macros-0.11.0 (c (n "heron_macros") (v "0.11.0") (d (list (d (n "heron_core") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04w0gnffgj3p82yi8301vjmq3qay7gizjxcl33x2nsld7pg2z5c8")))

(define-public crate-heron_macros-0.11.1 (c (n "heron_macros") (v "0.11.1") (d (list (d (n "heron_core") (r "^0.11.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g2zj9x68d16155q2c4a1llrm52ak4xf6pj7dpkhxpdxrgr9c1bs")))

(define-public crate-heron_macros-0.12.0 (c (n "heron_macros") (v "0.12.0") (d (list (d (n "heron_core") (r "^0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01fqzq4a1pihsgx1gcxhmxdr5abdxkzmmjxk6x2ifzk2hrqradi9") (r "1.56")))

(define-public crate-heron_macros-0.12.1 (c (n "heron_macros") (v "0.12.1") (d (list (d (n "heron_core") (r "^0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04c3p0436m1zhwmk22sc3ra85ww99kmzyj4iy6qhbfba6507acbg") (r "1.56")))

(define-public crate-heron_macros-0.13.0 (c (n "heron_macros") (v "0.13.0") (d (list (d (n "heron_core") (r "^0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hhkq27y0733m3mfbi951i76ikl88n6l3bnpa167qs00qpf5b7pv") (r "1.56")))

(define-public crate-heron_macros-1.0.1-rc.1 (c (n "heron_macros") (v "1.0.1-rc.1") (d (list (d (n "heron_core") (r "^1.0.1-rc.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15kziv6pinvrxwybq6kxl3jpy8z0n2a2373l9a0brwv3xfwbgfgp") (r "1.57")))

(define-public crate-heron_macros-1.0.1 (c (n "heron_macros") (v "1.0.1") (d (list (d (n "heron_core") (r "^1.0.1-rc.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0spsf73i5d47lril2r5pk7aj9936i6g7s9az8lgn35jvqj8kxhqx") (r "1.57")))

(define-public crate-heron_macros-1.1.0 (c (n "heron_macros") (v "1.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hv950zmzlic3bjkj3638f9pdwr2fk1gd1dlsvwgl12zcl11x750") (r "1.57")))

(define-public crate-heron_macros-1.2.0 (c (n "heron_macros") (v "1.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11biy62q3i8m92c3n5x7in7nqmvx5qy0ra1cj5m14jdvp2y79bvp") (r "1.57")))

(define-public crate-heron_macros-2.0.0 (c (n "heron_macros") (v "2.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z78dpw9im6d9ncxbsv8d9qssxbs97d7apmwfc8avvawph2dv4rv") (r "1.57")))

(define-public crate-heron_macros-2.0.1 (c (n "heron_macros") (v "2.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "137n36djcfm65yq63b3k6w7vg4dw0vp8i0wcksizd5v88sc01mya") (r "1.57")))

(define-public crate-heron_macros-2.1.0 (c (n "heron_macros") (v "2.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04rjn1nxb9q2vyglhdycfhzkzpx1ramv5nnak4v7wskdhb599b7l") (r "1.57")))

(define-public crate-heron_macros-2.2.0 (c (n "heron_macros") (v "2.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zaja4h6k8l0njvxqc7fki87vcsp26lkjnqhh2vphzd06pq58ns4") (r "1.57")))

(define-public crate-heron_macros-2.3.0 (c (n "heron_macros") (v "2.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f9p4zh5siaak67dc48z82h0dm5chrhlkhcbf3sd5ii2h04ij5la") (r "1.57")))

(define-public crate-heron_macros-2.4.0 (c (n "heron_macros") (v "2.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04kkdg1aam9fm620s34k7f5hhd1915733y97adxidsy6h21cnd8x") (r "1.57")))

(define-public crate-heron_macros-2.4.1 (c (n "heron_macros") (v "2.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l5ahggbi79dqqvfsb8q60rsjl3hwxi0sd7z8j9mm3ag9zdp0rjl") (r "1.57")))

(define-public crate-heron_macros-3.0.0 (c (n "heron_macros") (v "3.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12yr7cwga9kw3k9cdqawhhp7jfkyvkz4vk2x6hd7rz019w2i8qql") (r "1.60")))

(define-public crate-heron_macros-3.0.1 (c (n "heron_macros") (v "3.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0liwfjb380fi30smm0mr4kvc102xis5dwym077xfqckdzn10vgrd") (r "1.60")))

(define-public crate-heron_macros-3.1.0 (c (n "heron_macros") (v "3.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g59bf03gm4mx209llffcrszxd78hmlndlibr3n4616yvgafkz08") (r "1.60")))

(define-public crate-heron_macros-4.0.0-alpha.1 (c (n "heron_macros") (v "4.0.0-alpha.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "160lkc177sggl3mmlhbr3i3n2n6qk8iy1z0nhbl1bjp108j26hpd") (r "1.60")))

(define-public crate-heron_macros-4.0.0-alpha.2 (c (n "heron_macros") (v "4.0.0-alpha.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10d9kqrwp5avdwypkvsf1mj158yajdl1x558sbknjrphb1j0mcdr") (r "1.60")))

(define-public crate-heron_macros-4.0.0-alpha.3 (c (n "heron_macros") (v "4.0.0-alpha.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ip4y0r7ad4jv21jp1z5brj02nza4qv6ixi9w4qqyssgbqh0x6m4") (r "1.60")))

(define-public crate-heron_macros-4.0.0-alpha.4 (c (n "heron_macros") (v "4.0.0-alpha.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03bjzxhkl3l99h9sw0lj3y9yf2l43nq18j1q6k5f0avkwnmw819m") (r "1.60")))

(define-public crate-heron_macros-4.0.0 (c (n "heron_macros") (v "4.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nhhzs2rb2i2gccka94wi8q6mcpn33l7l4ird2s5vyykzwjl5pq4") (r "1.60")))

(define-public crate-heron_macros-4.0.1 (c (n "heron_macros") (v "4.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0byfpk4ca7wb9xp7j165fbarsis66bq4qzpvirjfv2pd6rfhiwxc") (r "1.60")))

(define-public crate-heron_macros-4.0.2 (c (n "heron_macros") (v "4.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vp8w8k3i77mrzabfjyb2sfp1x43ld2ddddd8b3dn0yi0fny8sl8") (r "1.60")))

(define-public crate-heron_macros-5.0.0 (c (n "heron_macros") (v "5.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1raxn3h8rg5w5fkdga86m0kj884fwa7f09c0y2vyzx9k0hgxl79j") (r "1.60")))

(define-public crate-heron_macros-4.1.0 (c (n "heron_macros") (v "4.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jiprs0ckpcv5aqw7hjv0256ikhm065lry9pl6ydzszbq8cdmlxv") (r "1.60")))

(define-public crate-heron_macros-4.2.0 (c (n "heron_macros") (v "4.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0x1yx1zzzmdy3lghsd0ccps0sw01ww3sbl7y8pm7wgz0ab52rzqs") (r "1.60")))

(define-public crate-heron_macros-4.3.0 (c (n "heron_macros") (v "4.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yjvl3wqvinimhibwnv07gd9v2pzyap84vv8j11w84dbvwrb5laf") (r "1.60")))

(define-public crate-heron_macros-5.0.1 (c (n "heron_macros") (v "5.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v4n0gm75qgch9mr0sd28avy2dj1g13dpq8qrya62f6klfjnwc60") (r "1.60")))

(define-public crate-heron_macros-5.0.2 (c (n "heron_macros") (v "5.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ysj2fap7685pnqspwckclghfmf00j770ssy1v8649x95b5xcbgn") (r "1.60")))

