(define-module (crates-io he ro heron_core) #:use-module (crates-io))

(define-public crate-heron_core-0.1.0-alpha.1 (c (n "heron_core") (v "0.1.0-alpha.1") (d (list (d (n "bevy") (r "^0.4.0") (k 2)) (d (n "bevy_ecs") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_math") (r "^0.4.0") (d #t) (k 0)))) (h "0zqgsvplpdazsi17sqbh262q624brwkapymn3srw3s2d56rr7g6l")))

(define-public crate-heron_core-0.1.0 (c (n "heron_core") (v "0.1.0") (d (list (d (n "bevy") (r "^0.4") (k 2)) (d (n "bevy_ecs") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_math") (r "^0.4.0") (d #t) (k 0)))) (h "07g0hl84whrmdvqpdp6w7yb95pl2in2m3d4isxfh79bc4px3c10g")))

(define-public crate-heron_core-0.1.1 (c (n "heron_core") (v "0.1.1") (d (list (d (n "bevy") (r "^0.4") (k 2)) (d (n "bevy_ecs") (r "^0.4.0") (d #t) (k 0)) (d (n "bevy_math") (r "^0.4.0") (d #t) (k 0)))) (h "0xim5q5bhbfwrpxjnrbqhybzndqvxmal0n8ha01gyfv99jcv2hnl")))

(define-public crate-heron_core-0.2.0 (c (n "heron_core") (v "0.2.0") (d (list (d (n "bevy") (r "^0.4.0") (k 0)))) (h "1ll69v6zlcsax6nmxfzppws4bs9wvz8fl0j40jpb6qxnx1d27mf6")))

(define-public crate-heron_core-0.3.0 (c (n "heron_core") (v "0.3.0") (d (list (d (n "bevy") (r "^0.4.0") (k 0)) (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)))) (h "11mlfms3af30db8dp013r9rk1ra705qnkbkmq71yyrwmlqcys9wp")))

(define-public crate-heron_core-0.4.0 (c (n "heron_core") (v "0.4.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)))) (h "02sf1437ik4dr2sr6hahm6b2xsbfb4v7qm2lnfb2i3c2ac8gph1g")))

(define-public crate-heron_core-0.5.0 (c (n "heron_core") (v "0.5.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)))) (h "180r1j3p234367wzaqkv539ran9y5frxcm6rwhw5djw1g8crn125")))

(define-public crate-heron_core-0.5.1 (c (n "heron_core") (v "0.5.1") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "0sdjvydxyaclrfhrybzw4n4l64jrv37ajikibcfqz83f6w17cy0h")))

(define-public crate-heron_core-0.6.0 (c (n "heron_core") (v "0.6.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "0hdkghlv5xka227872bsmqjppak4kggz9a9w1sx3zvhrwxisx21p")))

(define-public crate-heron_core-0.7.0 (c (n "heron_core") (v "0.7.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "18980h4dqz44iqjx5r1c7pw352q6lla47lf9pb24g6xvq2yr4f7v")))

(define-public crate-heron_core-0.8.0 (c (n "heron_core") (v "0.8.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "duplicate") (r "^0.2.9") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "0afzp2dyra0l17jc2hvzdhr8hgdz41q184mc9nz47h1i4rg2rmd0")))

(define-public crate-heron_core-0.9.0 (c (n "heron_core") (v "0.9.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "01afxp9a7b3cvg4709r3cmfs8dgchq2xnwcbrwpb7jid9xlcw6zm")))

(define-public crate-heron_core-0.9.1 (c (n "heron_core") (v "0.9.1") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "0fcy32c5zc0ngzl07p55pwxw0vbw9jbqabgdy9s03x8gh0a1zkd8")))

(define-public crate-heron_core-0.10.0 (c (n "heron_core") (v "0.10.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "1fb425yb9hjd0gvrbd1nw026dgxf9wkrf4jwqvpffji6pn6czng3")))

(define-public crate-heron_core-0.10.1 (c (n "heron_core") (v "0.10.1") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "1bsvw56baz7zygxfjp1mq0f638siiga62i2vxbdv70bzx0788q0c") (y #t)))

(define-public crate-heron_core-0.11.0 (c (n "heron_core") (v "0.11.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "03qmqb39f73f03gq5zxhfc1yvfbipfv5wg7cy9mr5wir1fhfn45w")))

(define-public crate-heron_core-0.11.1 (c (n "heron_core") (v "0.11.1") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "1isw6fiixy6aqsmcw9h1zmq8y2qql348gqbmq11awa144d87cvj5")))

(define-public crate-heron_core-0.12.0 (c (n "heron_core") (v "0.12.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "08rr07hgagqf2zsy7h3jivlisqyc5cvqx3k3dh1jw84lyjwyjg6c") (r "1.56")))

(define-public crate-heron_core-0.12.1 (c (n "heron_core") (v "0.12.1") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 2)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "1ghjhjrs19zxwhc2jhh2yc7jmgxkp02c02zpfacsc6zqn2s35l8p") (r "1.56")))

(define-public crate-heron_core-1.0.0 (c (n "heron_core") (v "1.0.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 2)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "0midm8iy3pcf3lv1bhml21cx37cs37axawfpkqj9f064a2i1sp89") (f (quote (("default") ("3d")))) (y #t) (r "1.56")))

(define-public crate-heron_core-0.13.0 (c (n "heron_core") (v "0.13.0") (d (list (d (n "bevy") (r "^0.5.0") (k 0)) (d (n "bevy") (r "^0.5.0") (f (quote ("render"))) (k 2)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.7") (d #t) (k 2)))) (h "1airjwd9l0074cmzn3fhpddmmmggzb82wbk279zgxqxnhk0cn15q") (f (quote (("default") ("3d")))) (r "1.56")))

(define-public crate-heron_core-1.0.1-rc.1 (c (n "heron_core") (v "1.0.1-rc.1") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy") (r "^0.6.0") (f (quote ("render"))) (k 2)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)))) (h "03z7zvnnld6jhifci085574yk36nh9x78224irb6yfaiw5x251jh") (f (quote (("default") ("3d")))) (r "1.57")))

(define-public crate-heron_core-1.0.1 (c (n "heron_core") (v "1.0.1") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy") (r "^0.6.0") (f (quote ("render"))) (k 2)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)))) (h "00g80m923jcyzxbg30mfyqbgqsq0yl447bplr9zixw303n0ngnxr") (f (quote (("default") ("3d")))) (r "1.57")))

(define-public crate-heron_core-1.1.0 (c (n "heron_core") (v "1.1.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy") (r "^0.6.0") (f (quote ("render"))) (k 2)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)))) (h "0hdvd82nn7jlfklk7zachf5ai3saszwgyx74n4k01xxb6rvs16bh") (f (quote (("default") ("3d")))) (r "1.57")))

(define-public crate-heron_core-1.2.0 (c (n "heron_core") (v "1.2.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy") (r "^0.6.0") (f (quote ("render"))) (k 2)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)))) (h "0acchg0kvhds2v83saj2svcqc2zk692am6xn5jjbf74ig2r93pqy") (f (quote (("default") ("collision-from-mesh" "bevy/render") ("3d")))) (r "1.57")))

(define-public crate-heron_core-2.0.0 (c (n "heron_core") (v "2.0.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy") (r "^0.6.0") (f (quote ("render"))) (k 2)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "0x07yi014iwagbp7qx2jiyqr0b9j14s0090qb5kwa7bw4srjx36y") (f (quote (("default") ("collision-from-mesh" "bevy/render") ("3d")))) (r "1.57")))

(define-public crate-heron_core-2.0.1 (c (n "heron_core") (v "2.0.1") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy") (r "^0.6.0") (f (quote ("render"))) (k 2)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.3.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "1gy2kz6bwm5ga6k1d7lk5d7cf3v2f35ixkq24561qx49izhylwbz") (f (quote (("default") ("collision-from-mesh" "bevy/render") ("3d")))) (r "1.57")))

(define-public crate-heron_core-2.1.0 (c (n "heron_core") (v "2.1.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy") (r "^0.6.0") (f (quote ("render"))) (k 2)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "04m69q6j9sgv60bi71w5x9c9lzf1l8y5kd3if67bij11qwhw4c8n") (f (quote (("default") ("collision-from-mesh" "bevy/render") ("3d")))) (r "1.57")))

(define-public crate-heron_core-2.2.0 (c (n "heron_core") (v "2.2.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "bevy") (r "^0.6.0") (f (quote ("render"))) (k 2)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "166zr4ck3sivrr7qx3cx5d1n1h0n3pfpshpd527g3c49mnzsq2s4") (f (quote (("default") ("collision-from-mesh" "bevy/render") ("3d")))) (r "1.57")))

(define-public crate-heron_core-2.3.0 (c (n "heron_core") (v "2.3.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "13maiyp7qga9sygfzdsv6v99nb19mrrbhnlzd3xmp8crsagbajg2") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render") ("3d")))) (r "1.57")))

(define-public crate-heron_core-2.4.0 (c (n "heron_core") (v "2.4.0") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "0pjq8g4glydw87kjdrrg9pkcw2iqydxrscqwml604805wd9wpkzk") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render") ("3d")))) (r "1.57")))

(define-public crate-heron_core-2.4.1 (c (n "heron_core") (v "2.4.1") (d (list (d (n "bevy") (r "^0.6.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "0488vr6sp189qz4z3cxigax2wm4v9ywahvysnv83539v0q72pfs8") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render") ("3d")))) (r "1.57")))

(define-public crate-heron_core-3.0.0 (c (n "heron_core") (v "3.0.0") (d (list (d (n "bevy") (r "^0.7.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "03fiz4zzw65snjr3pp7ajf9gwd25lgrr8wipw98j4i9dslcrq5d5") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render") ("3d")))) (r "1.60")))

(define-public crate-heron_core-3.0.1 (c (n "heron_core") (v "3.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.7.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "11w5gdijg19dmlwy5k09p0nh4czdf63qfkw26mn35hz99903rjbf") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render") ("3d")))) (r "1.60")))

(define-public crate-heron_core-3.1.0 (c (n "heron_core") (v "3.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.7.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "0im43l5z94sni82z4dral0yh1z88066ya4kxyvgw8ahqkckm8k5h") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render") ("3d")))) (r "1.60")))

(define-public crate-heron_core-4.0.0-alpha.1 (c (n "heron_core") (v "4.0.0-alpha.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.7.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "1daabmwj1izwqp0fwr71gvx75i5x3rb6dpsnxfaihh2s42gv3hgd") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render") ("3d")))) (r "1.60")))

(define-public crate-heron_core-4.0.0-alpha.2 (c (n "heron_core") (v "4.0.0-alpha.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.7.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "1wgf7zq62xj03ivsh45hcqk1qdnyv970cfg41344i44qakrppxxw") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render") ("3d")))) (r "1.60")))

(define-public crate-heron_core-4.0.0-alpha.3 (c (n "heron_core") (v "4.0.0-alpha.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.7.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "150xb40vg5b62ixf1f4pvr25qfckbak7qd79mjjdsflg6vj571yc") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render") ("3d")))) (r "1.60")))

(define-public crate-heron_core-4.0.0-alpha.4 (c (n "heron_core") (v "4.0.0-alpha.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)))) (h "0izjyjsn80x1w50sl8xr7hip18hing122kbnqbja10bdjp3m9sc8") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render" "bevy/bevy_scene" "bevy/bevy_asset") ("3d")))) (r "1.60")))

(define-public crate-heron_core-4.0.0 (c (n "heron_core") (v "4.0.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "smallvec") (r "^1.9") (d #t) (k 0)))) (h "0ic80kks7svr11xi1ainp01a6i6ndmhd2afn09hfqbnk1ydq7y44") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render" "bevy/bevy_scene" "bevy/bevy_asset") ("3d")))) (r "1.60")))

(define-public crate-heron_core-4.0.1 (c (n "heron_core") (v "4.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "smallvec") (r "^1.9") (d #t) (k 0)))) (h "0j9yzfnzg1k21zgy48pja2kpy26y3gj3228apvy98791ahvj4zb4") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render" "bevy/bevy_scene" "bevy/bevy_asset") ("3d")))) (r "1.60")))

(define-public crate-heron_core-4.0.2 (c (n "heron_core") (v "4.0.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "14v65dz797052q2v9bjv01fzs7m0nqk24fgrbmympyhijh8bdfxa") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render" "bevy/bevy_scene" "bevy/bevy_asset") ("3d")))) (r "1.60")))

(define-public crate-heron_core-5.0.0 (c (n "heron_core") (v "5.0.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "1ndw64bx62fp26fj2qm0vs979abn0vpz7wh2ajnrkdlckds1z8yi") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render" "bevy/bevy_scene" "bevy/bevy_asset") ("3d")))) (r "1.60")))

(define-public crate-heron_core-4.1.0 (c (n "heron_core") (v "4.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "heron_core_v5") (r "^5.0") (d #t) (k 0) (p "heron_core")) (d (n "rstest") (r "^0.15") (d #t) (k 2)))) (h "0fyvlysyxcbzz1nykp6vavjj6hp72n2v6fvpx8ck6g2m5ndxj55s") (f (quote (("default") ("collision-from-mesh" "heron_core_v5/collision-from-mesh" "bevy/bevy_render" "bevy/bevy_scene" "bevy/bevy_asset") ("3d" "heron_core_v5/3d")))) (r "1.60")))

(define-public crate-heron_core-4.2.0 (c (n "heron_core") (v "4.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "heron_core_v5") (r "^5.0") (d #t) (k 0) (p "heron_core")) (d (n "rstest") (r "^0.15") (d #t) (k 2)))) (h "0q9njggqin3xxjlj8z1crnnqdaq17l53li2xz8x6890hdp1saimf") (f (quote (("default") ("collision-from-mesh" "heron_core_v5/collision-from-mesh" "bevy/bevy_render" "bevy/bevy_scene" "bevy/bevy_asset") ("3d" "heron_core_v5/3d")))) (r "1.60")))

(define-public crate-heron_core-4.3.0 (c (n "heron_core") (v "4.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "heron_core_v5") (r "^5.0") (d #t) (k 0) (p "heron_core")) (d (n "rstest") (r "^0.15") (d #t) (k 2)))) (h "0lzihd24rid5lccw05zvs1wjf8cdlh7835i48wmb2s5rir7j6x94") (f (quote (("default") ("collision-from-mesh" "heron_core_v5/collision-from-mesh" "bevy/bevy_render" "bevy/bevy_scene" "bevy/bevy_asset") ("3d" "heron_core_v5/3d")))) (r "1.60")))

(define-public crate-heron_core-5.0.1 (c (n "heron_core") (v "5.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "1iszsy5bxl8xvjmvqd6c135pvzawy38sldf1h60fkn8dvkr1558a") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render" "bevy/bevy_scene" "bevy/bevy_asset") ("3d")))) (r "1.60")))

(define-public crate-heron_core-5.0.2 (c (n "heron_core") (v "5.0.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "bevy") (r "^0.8.0") (k 0)) (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "duplicate") (r "^0.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "0rd962jay0ins8fg7cr1s1gcj5x2riam6sh4lgrr1kgzdlxflaa3") (f (quote (("default") ("collision-from-mesh" "bevy/bevy_render" "bevy/bevy_scene" "bevy/bevy_asset") ("3d")))) (r "1.60")))

