(define-module (crates-io he te heterob) #:use-module (crates-io))

(define-public crate-heterob-0.1.0 (c (n "heterob") (v "0.1.0") (d (list (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "1ycjr1li8a6x8vgzqs7mz4rx3g9kn8hy4acb8yz1b0qi8cyyvymz")))

(define-public crate-heterob-0.2.0 (c (n "heterob") (v "0.2.0") (d (list (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "0v900szkqdnq68ipffwxpidzz2m5z3kgvxm3q06bpl58nd7s0wk9")))

(define-public crate-heterob-0.2.1 (c (n "heterob") (v "0.2.1") (d (list (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "18ifnsc2rjx0cr7bjdc07shkbjq7qlp898h5gbsp8xwz6mf9nr48")))

(define-public crate-heterob-0.2.2 (c (n "heterob") (v "0.2.2") (d (list (d (n "funty") (r "^2.0.0") (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "1ghn8h7g8rylx7hdr2fzii5xbzrassyjh5sa9kqa34kg620afvsj")))

(define-public crate-heterob-0.3.0 (c (n "heterob") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "funty") (r "^2.0.0") (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "11m7v6gri1lmq0yljbziqkkf1z32rhbbbsffw4pyd4sqlzzmfh6z")))

