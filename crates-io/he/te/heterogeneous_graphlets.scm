(define-module (crates-io he te heterogeneous_graphlets) #:use-module (crates-io))

(define-public crate-heterogeneous_graphlets-0.1.0 (c (n "heterogeneous_graphlets") (v "0.1.0") (d (list (d (n "csv") (r "^1.2") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "0ls62zkbzqj5klfdkmliyw5747gabd6g0y73khssj5djh22aph9q")))

(define-public crate-heterogeneous_graphlets-0.1.1 (c (n "heterogeneous_graphlets") (v "0.1.1") (d (list (d (n "csv") (r "^1.2") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "0n8ria5d7sd74ywlq6q4ydi0d8wdi7iay818zp4lkh3s60x9m6gv")))

