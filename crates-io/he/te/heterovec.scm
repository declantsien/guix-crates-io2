(define-module (crates-io he te heterovec) #:use-module (crates-io))

(define-public crate-heterovec-0.1.0 (c (n "heterovec") (v "0.1.0") (h "1qmscrik9k2hgzfrssym0xfmv55d62zm79s4kwq4jsa5idbk78ca")))

(define-public crate-heterovec-0.1.1 (c (n "heterovec") (v "0.1.1") (h "12ri3bgd4v7fwgfrxvymfs4v6477r7lipi57nm11371dkz7lpa1l")))

(define-public crate-heterovec-0.1.2 (c (n "heterovec") (v "0.1.2") (h "14azikizbm2ajcycvdrl5vrdcsyyi09p79g54xjqrbcq8d7abd7n")))

