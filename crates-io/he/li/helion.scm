(define-module (crates-io he li helion) #:use-module (crates-io))

(define-public crate-helion-0.5.0 (c (n "helion") (v "0.5.0") (d (list (d (n "captrs") (r "^0.3.0") (d #t) (k 0)) (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)) (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serial") (r "^0.3") (d #t) (k 0)))) (h "0va4zmlmb1iiaqhrp0gj3vjmiw4qp1a0xlmk71rr5r07c0b8a3fa") (f (quote (("profiling" "cpuprofiler"))))))

