(define-module (crates-io he li helix) #:use-module (crates-io))

(define-public crate-helix-0.1.0 (c (n "helix") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1q6ap6vhiinyibpcvxv4ip3fbc4s1miz0k7k238sv0c4r57mqps2") (y #t)))

(define-public crate-helix-0.5.0-alpha-4 (c (n "helix") (v "0.5.0-alpha-4") (d (list (d (n "cslice") (r "^0.2") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.5.0-alpha-4") (d #t) (k 0)))) (h "0kg534lqlvsnzz2ldpac2wfg9bq501z19gb4x9flc5lxhl2zma2h")))

(define-public crate-helix-0.5.0 (c (n "helix") (v "0.5.0") (d (list (d (n "cslice") (r "^0.2") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0chpvbg9qqqhqjprw4ppmijwddg53l9198rmrhsdga7xdyqlfjjf")))

(define-public crate-helix-0.6.0 (c (n "helix") (v "0.6.0") (d (list (d (n "cslice") (r "^0.3") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1b1icga77m9pqwwgnykxg932npmzgmqz9hnr56k7k3m6g62hngab")))

(define-public crate-helix-0.6.1 (c (n "helix") (v "0.6.1") (d (list (d (n "cslice") (r "^0.3") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.6.1") (d #t) (k 0)))) (h "1ghywrmzrqjy6p77mz4mbhvmi6qw40ms4cvl2axn4c17jbji4xmr")))

(define-public crate-helix-0.6.2 (c (n "helix") (v "0.6.2") (d (list (d (n "cslice") (r "^0.3") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.6.2") (d #t) (k 0)))) (h "0v3risssnyvdvcnk7vv3zzdnw8kqrqyg3h1x6cfka49f8n2d9lgq")))

(define-public crate-helix-0.0.1 (c (n "helix") (v "0.0.1") (d (list (d (n "cslice") (r "^0.3") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.0.1") (d #t) (k 0)))) (h "19j2lk0ikbivcrm4lhavkhjzyllaqpbn4z6ykbihhrb4ngfzzdnd") (y #t)))

(define-public crate-helix-0.6.3 (c (n "helix") (v "0.6.3") (d (list (d (n "cslice") (r "^0.3") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.6.3") (d #t) (k 0)))) (h "0nn2cbk4svqr1h9xw3bskf76cmps1c53amsm0jc8ci59wkkq75hg")))

(define-public crate-helix-0.6.4 (c (n "helix") (v "0.6.4") (d (list (d (n "cslice") (r "^0.3") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.6.4") (d #t) (k 0)))) (h "0yc0drrj4j1y33hj9bzknp20718gr779fzmqjbcz8y3yaplc0cg1")))

(define-public crate-helix-0.7.0 (c (n "helix") (v "0.7.0") (d (list (d (n "cslice") (r "^0.3") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.7.0") (d #t) (k 0)))) (h "12y98dv9zp74xid2rfcq6sf939ys2rycpkpnwd99p7lhy4zhxjhk")))

(define-public crate-helix-0.7.1 (c (n "helix") (v "0.7.1") (d (list (d (n "cslice") (r "^0.3") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.7.1") (d #t) (k 0)))) (h "042qk0wlqf1kb1z13m75s9j7dg72v0vfkwxz1s8yn6nmq1r1lhya")))

(define-public crate-helix-0.7.2 (c (n "helix") (v "0.7.2") (d (list (d (n "cslice") (r "^0.3") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.7.2") (d #t) (k 0)))) (h "1kyl6rjmmin7i0cng6jnzbf3hqfajfba5i8s939pczrka47mxx6b")))

(define-public crate-helix-0.7.3 (c (n "helix") (v "0.7.3") (d (list (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.7.3") (d #t) (k 0)))) (h "11ghkj33xl6cacngh1y0n20y2qf0njd97q5isbg7ycipkz84aphs")))

(define-public crate-helix-0.7.4 (c (n "helix") (v "0.7.4") (d (list (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.7.4") (d #t) (k 0)))) (h "0ln47kvdgi42y9p2wx93gqz798zb14dhjw4d5mh0r9jlk8c2h3h3")))

(define-public crate-helix-0.7.5 (c (n "helix") (v "0.7.5") (d (list (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.7.5") (d #t) (k 0)))) (h "0h4281pwwqlwd27ynhhjrh6ah13czmk4wn50w2397bcqwziig829")))

