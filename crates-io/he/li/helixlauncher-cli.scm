(define-module (crates-io he li helixlauncher-cli) #:use-module (crates-io))

(define-public crate-helixlauncher-cli-0.1.0 (c (n "helixlauncher-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "helixlauncher-core") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread" "fs" "process"))) (d #t) (k 0)))) (h "1dblq7z2ssnm365xdrsz7gsn5cx4b2hxqc3rdzyi7f5nm0wsydbw")))

