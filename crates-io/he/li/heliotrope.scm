(define-module (crates-io he li heliotrope) #:use-module (crates-io))

(define-public crate-heliotrope-0.0.2 (c (n "heliotrope") (v "0.0.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0jpj4pds7r3spk4r5dqaqmn3j6nmdx06scaq6x3x6vhi6z407djv")))

(define-public crate-heliotrope-0.0.3 (c (n "heliotrope") (v "0.0.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "09chfzkrww3l53rab2s5i92hzzdpa3ha0ir6g98s4xqdscckav6m")))

(define-public crate-heliotrope-0.1.0 (c (n "heliotrope") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0nwrvs6ghsq3hi2kysj3i2l4mrcc2jq06x8690fzp2mks2ib1nlj")))

