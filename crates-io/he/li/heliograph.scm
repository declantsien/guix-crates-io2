(define-module (crates-io he li heliograph) #:use-module (crates-io))

(define-public crate-heliograph-0.1.0 (c (n "heliograph") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "caring") (r "^0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)) (d (n "sendfd") (r "^0.3.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0bvx02i3jqc7b7nj6s0jjbb5zk257dx3prcs0bwfp2slx71z830v")))

