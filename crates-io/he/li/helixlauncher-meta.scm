(define-module (crates-io he li helixlauncher-meta) #:use-module (crates-io))

(define-public crate-helixlauncher-meta-0.1.0 (c (n "helixlauncher-meta") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1v4drwlnx0i9y86r28achwrk4w6ff7v268alwljc429n1gx2lb36")))

