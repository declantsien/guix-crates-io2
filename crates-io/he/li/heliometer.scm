(define-module (crates-io he li heliometer) #:use-module (crates-io))

(define-public crate-heliometer-0.2.4 (c (n "heliometer") (v "0.2.4") (h "1ifiak7rszlbw2sg4d5i39vr3f0ljdrf0vqv31q2i8gx9gjrizgp")))

(define-public crate-heliometer-0.2.5 (c (n "heliometer") (v "0.2.5") (h "0hdsllgxkxanc3j6ji73gl9yykvq5vd8wkm1sdkzp10j65y3v3a2")))

(define-public crate-heliometer-0.3.1 (c (n "heliometer") (v "0.3.1") (h "01w3llbms6mdsbz934zxjxja7b6m3c6pj1fi3aifwi0fzcf9mbv9")))

(define-public crate-heliometer-0.3.3 (c (n "heliometer") (v "0.3.3") (h "0kp5sankjgnv000cxwz9jkss8bks10pkyp79migsi4vfr81i8nva")))

(define-public crate-heliometer-0.3.6 (c (n "heliometer") (v "0.3.6") (h "03hrff6ggzfcksskkj3pbkwgsvvhs5gyvnnk207yw8dqpyg61gba")))

(define-public crate-heliometer-0.4.0 (c (n "heliometer") (v "0.4.0") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)))) (h "18c1s8shryvklxf01rirq038xcyw5cdsmbn5hqghcw27v6vz1mrl")))

(define-public crate-heliometer-0.4.1 (c (n "heliometer") (v "0.4.1") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)))) (h "1807h0nvzyprfns2babf0gxypbg6k3yi4kfbwqj1ycc9fly3v5mz")))

(define-public crate-heliometer-0.4.2 (c (n "heliometer") (v "0.4.2") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)))) (h "1h018qr9kpsm7fhza8qzkzfy882dg93ghrwslkfgw20alv5gvczq")))

(define-public crate-heliometer-0.4.3 (c (n "heliometer") (v "0.4.3") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)))) (h "1d7ldnkvicdyfpszs1pcb9c82fcf4fmzrd462g8jflbz0sbc4xvl")))

(define-public crate-heliometer-0.4.5 (c (n "heliometer") (v "0.4.5") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)))) (h "1fvfqgnccfbn5yfcmjhmx9qliyan6ffiq1x3bfvqhayw0sdwm5hg")))

(define-public crate-heliometer-0.4.6 (c (n "heliometer") (v "0.4.6") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)))) (h "0ghbissxm7xi3s1lgwkjbbv9f878hq0r4hc3jzf6gkbgx1kv4c80")))

(define-public crate-heliometer-0.4.7 (c (n "heliometer") (v "0.4.7") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)))) (h "1xypcg3ddhnw6lfn6hb761hkmsa67ks8crxhdmnabj0hahd2agg6")))

(define-public crate-heliometer-0.4.8 (c (n "heliometer") (v "0.4.8") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)))) (h "0l0ipwdrb8m5jw7lvlp86mav4rv0kc6q7yd3bm7q1cqlnwp2xzlv")))

(define-public crate-heliometer-0.4.9 (c (n "heliometer") (v "0.4.9") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)))) (h "0qp2pdg8pc07x62ws1xsqk21q7pkv2xcl80jh745rizv13vkc35r")))

(define-public crate-heliometer-0.4.13 (c (n "heliometer") (v "0.4.13") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)))) (h "07z0rdr15c035bhb04597kas0g0ixk3x7m85861vksxjnfsq5djr")))

(define-public crate-heliometer-0.5.0 (c (n "heliometer") (v "0.5.0") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "1qjhxay9arkwniadnz22qz1wa3h2kypa64cc1bmrvdg9j6zjnn6z")))

(define-public crate-heliometer-0.5.1 (c (n "heliometer") (v "0.5.1") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "1b8gl5a5hyzbxhm6v496n6c1rfnjsc4a5lbzba2fig77ich0zk36")))

(define-public crate-heliometer-0.5.2 (c (n "heliometer") (v "0.5.2") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "1z789rkcsvg2pp7awdjs2dk7xv6z7fwg4d9yk1w2q9gry463q8sq")))

(define-public crate-heliometer-0.5.3 (c (n "heliometer") (v "0.5.3") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "1xrzz4jfa2fbi0672df8jmhdvw03ifxfxdklrld51l6j6pf7r29w")))

(define-public crate-heliometer-0.5.4 (c (n "heliometer") (v "0.5.4") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "1lyn783833l0xykqarl80gc45zmgn9ynh3r7rqs67bljcxcwblfk")))

(define-public crate-heliometer-0.5.5 (c (n "heliometer") (v "0.5.5") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "1sghhw9fv7yscya6b3srqffwsb2jys1ashlq0c7p6qlbpg8wwh0n")))

(define-public crate-heliometer-0.5.6 (c (n "heliometer") (v "0.5.6") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "1jbmdgl3w31sj6cx82fd1n5lf6hyr33gajiwkg74y7zmzmvqhzb9")))

(define-public crate-heliometer-0.5.7 (c (n "heliometer") (v "0.5.7") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "1dz1x1wna1v6l6i8wpa9ph9gqhq7kkgshxs97nqk92sb9k32lcf6")))

(define-public crate-heliometer-0.5.8 (c (n "heliometer") (v "0.5.8") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "17skk82cj08h0drz7haw6mgah0kp65nhmmr2ya02k5dddgw4w87p")))

(define-public crate-heliometer-0.5.9 (c (n "heliometer") (v "0.5.9") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "089b6ql2zlp0n0dm93cmcl73g3fp66jwqbpww02dpn986f79rcy1")))

(define-public crate-heliometer-0.5.10 (c (n "heliometer") (v "0.5.10") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)))) (h "0av1dqpvgcac92b1civki7qadyck8kww20ibbjxmvhgyc0ps3k1s")))

