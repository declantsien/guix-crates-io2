(define-module (crates-io he li helios-dac) #:use-module (crates-io))

(define-public crate-helios-dac-0.1.0 (c (n "helios-dac") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "helios-dac-sys") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "rusb") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "00pl2522finxbglbdwmm97wxqjn080qsr6fk7m6zld1spfvx036a") (f (quote (("sdk" "helios-dac-sys") ("native" "rusb" "thiserror") ("default" "sdk"))))))

(define-public crate-helios-dac-0.2.0 (c (n "helios-dac") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "helios-dac-sys") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "rusb") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "0p6wwq8cj8j50i8vjlym9i6qfw0xy2zmpfavwk6w6q70a0iqh8m8") (f (quote (("sdk" "helios-dac-sys") ("native" "rusb" "thiserror") ("default" "sdk"))))))

