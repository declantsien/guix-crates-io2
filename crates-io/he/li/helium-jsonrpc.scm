(define-module (crates-io he li helium-jsonrpc) #:use-module (crates-io))

(define-public crate-helium-jsonrpc-1.0.1 (c (n "helium-jsonrpc") (v "1.0.1") (d (list (d (n "async-trait") (r "^0") (d #t) (k 0)) (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("gzip" "json" "rustls-tls"))) (k 0)) (d (n "rust_decimal") (r "^1") (f (quote ("serde-float"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "11bj4ca6nz3hz2jhla1nfzxb9cph1s1qj57i2r61y6v2aqvbap9f")))

