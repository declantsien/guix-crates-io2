(define-module (crates-io he li helixiser) #:use-module (crates-io))

(define-public crate-helixiser-0.1.0 (c (n "helixiser") (v "0.1.0") (d (list (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "1891r9nzc71q927c0k8dfyr0x7i7gyg50m3hrf3rq4pxkg4cchrg")))

(define-public crate-helixiser-0.2.0 (c (n "helixiser") (v "0.2.0") (d (list (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "1n9jmxbqdgv6mcrlsqh2sgqwyqp5ziqjzc3rd7jq722p7lnd7lji")))

(define-public crate-helixiser-0.2.1 (c (n "helixiser") (v "0.2.1") (d (list (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "1p17xznsknkypjgmkmna9ard2b99sss0dwnhp6lg6r9k79h2g15j")))

(define-public crate-helixiser-0.3.0 (c (n "helixiser") (v "0.3.0") (d (list (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "1lnqi3xnagq2hg7m3ksb31l08qdxjzxmfqpy460nzrmlalwrss41")))

(define-public crate-helixiser-0.4.0 (c (n "helixiser") (v "0.4.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "1v76z1b11fkpv2qf29pjppi4ygk1b7q1klhzhblqf7dhvf6g4477")))

(define-public crate-helixiser-0.4.1 (c (n "helixiser") (v "0.4.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "0zkags93cr9k5zfzlzl7n01c89fgfy8mn39i24h667qc59qy9b5s")))

(define-public crate-helixiser-0.4.2 (c (n "helixiser") (v "0.4.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "open") (r "^1.4.0") (d #t) (k 0)) (d (n "rustfft") (r "^3.0.1") (d #t) (k 0)))) (h "1wg4jybcxm9bvrgw0gqmd97izxlhx85x6l2xblrhkhzqb3qhrx1l")))

