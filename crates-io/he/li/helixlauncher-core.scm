(define-module (crates-io he li helixlauncher-core) #:use-module (crates-io))

(define-public crate-helixlauncher-core-0.1.0 (c (n "helixlauncher-core") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.24") (o #t) (d #t) (k 1)) (d (n "helixlauncher-meta") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "0fr6i1vd4b5mhhd3iw257dslc4br6gr33jhzqi4v1zsmriwabji6") (s 2) (e (quote (("ffi" "dep:cbindgen"))))))

