(define-module (crates-io he li helioselene) #:use-module (crates-io))

(define-public crate-helioselene-0.1.0 (c (n "helioselene") (v "0.1.0") (d (list (d (n "crypto-bigint") (r "^0.5") (f (quote ("zeroize"))) (k 0)) (d (n "dalek-ff-group") (r "^0.4.1") (k 0)) (d (n "ff") (r "^0.13") (f (quote ("std" "bits"))) (k 0)) (d (n "group") (r "^0.13") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 0)) (d (n "subtle") (r "^2.4") (f (quote ("std"))) (k 0)) (d (n "zeroize") (r "^1.5") (f (quote ("std" "zeroize_derive"))) (k 0)))) (h "0yab55y8wdf8x4w1k6d3hm1kdannnhb7zb4w4s1nl1bjsn4vw98r")))

