(define-module (crates-io he li heliosphere-signer) #:use-module (crates-io))

(define-public crate-heliosphere-signer-0.1.0 (c (n "heliosphere-signer") (v "0.1.0") (d (list (d (n "heliosphere-core") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "k256") (r "^0.11.6") (f (quote ("arithmetic" "ecdsa" "keccak256"))) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (k 0)))) (h "09nx4z6hs1rdv7f9r5xx4s8i8dhhnbhgn8mspp1m29mk220hwibl")))

(define-public crate-heliosphere-signer-0.1.1 (c (n "heliosphere-signer") (v "0.1.1") (d (list (d (n "heliosphere-core") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "k256") (r "^0.11.6") (f (quote ("arithmetic" "ecdsa" "keccak256"))) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (k 0)))) (h "0wkhk30nv28na7j45ns7lahmm9dsnrn9r41s3g50h0fgzw1wl42d") (y #t)))

(define-public crate-heliosphere-signer-0.2.0 (c (n "heliosphere-signer") (v "0.2.0") (d (list (d (n "heliosphere-core") (r "^0.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "k256") (r "^0.11.6") (f (quote ("arithmetic" "ecdsa" "keccak256"))) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "sha3") (r "^0.10.6") (k 0)))) (h "15x309pzjx06zhjzs5sdalx7wgd0iadl04gf0mkr8ic88z1d7p1k")))

