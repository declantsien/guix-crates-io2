(define-module (crates-io he li helix_runtime) #:use-module (crates-io))

(define-public crate-helix_runtime-0.1.0 (c (n "helix_runtime") (v "0.1.0") (d (list (d (n "cslice") (r "^0.2") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.5.0-alpha-1") (d #t) (k 0)))) (h "05zwna6d9f7gy6nfxiyjmr934as3p5m7sjidjlh54mwnyn35y7l4") (y #t)))

(define-public crate-helix_runtime-0.5.0-alpha-1 (c (n "helix_runtime") (v "0.5.0-alpha-1") (d (list (d (n "cslice") (r "^0.2") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.5.0-alpha-1") (d #t) (k 0)))) (h "1b3rv8rvk869z029hka4mad0l0s610crjy10103d5l1y2pnqbglx")))

(define-public crate-helix_runtime-0.5.0-alpha-2 (c (n "helix_runtime") (v "0.5.0-alpha-2") (d (list (d (n "cslice") (r "^0.2") (d #t) (k 0)) (d (n "cstr-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "libcruby-sys") (r "^0.5.0-alpha-2") (d #t) (k 0)))) (h "1kbssp614km17jfbq7isqdmf7dg745d3hjj3wjf19y3k7dl7rklw")))

