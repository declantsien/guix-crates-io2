(define-module (crates-io he li heliotrope-rs) #:use-module (crates-io))

(define-public crate-heliotrope-rs-0.1.0 (c (n "heliotrope-rs") (v "0.1.0") (d (list (d (n "blob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0b14n30mzp3mg8qj2wk1p1mgrygs4v8hdr2h968z9d5p415s1r1i")))

