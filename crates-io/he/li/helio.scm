(define-module (crates-io he li helio) #:use-module (crates-io))

(define-public crate-helio-0.1.0 (c (n "helio") (v "0.1.0") (h "0s8j41r44sp1pr9l5xy24gknd7qhqgmxn0pww7yycw05044bxvn0") (f (quote (("default")))) (y #t)))

(define-public crate-helio-0.1.1 (c (n "helio") (v "0.1.1") (h "1chy2dm05k92cjh5svnagh5kijf4xvcbv6c66b2j3gahl5psfj34") (f (quote (("default")))) (y #t)))

