(define-module (crates-io he li helium) #:use-module (crates-io))

(define-public crate-helium-0.1.0 (c (n "helium") (v "0.1.0") (h "05krjz8wiljw7d7i2mgl6yvg1359aqj7g9ndbs817xwq0h791jjw")))

(define-public crate-helium-0.2.0 (c (n "helium") (v "0.2.0") (h "1fsq7dffmn4xwmg1agvx0dg310c9ffh45xvx0da4hy264zlp0za6")))

