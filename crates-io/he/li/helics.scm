(define-module (crates-io he li helics) #:use-module (crates-io))

(define-public crate-helics-0.1.0 (c (n "helics") (v "0.1.0") (d (list (d (n "helics-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bh66lr53j04p4313nfw16d6sd1854kbikp18qqrs025gxclzn75")))

