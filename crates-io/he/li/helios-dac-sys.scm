(define-module (crates-io he li helios-dac-sys) #:use-module (crates-io))

(define-public crate-helios-dac-sys-0.1.0 (c (n "helios-dac-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1szhlyv70d9wgy98whxwzdfcy5sj4dpyj117pgjyxz3lmvzfqars")))

(define-public crate-helios-dac-sys-0.2.0 (c (n "helios-dac-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0x3snj0fk8r6qi2ncc6dvrsqlqxwm9rfwf47v7wr8ybgnalznm05")))

