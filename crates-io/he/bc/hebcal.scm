(define-module (crates-io he bc hebcal) #:use-module (crates-io))

(define-public crate-hebcal-0.1.0 (c (n "hebcal") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.1.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1qw4srv930cv4yx4qahfgish5xf9mhlfpihx04bb9fj02pha65lh")))

