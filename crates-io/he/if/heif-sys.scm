(define-module (crates-io he if heif-sys) #:use-module (crates-io))

(define-public crate-heif-sys-0.1.0 (c (n "heif-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)) (d (n "bmp") (r "^0.4") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.8") (d #t) (k 0)))) (h "1pdxfnm3vmwvlm2h42ym7sn7yyb743adjfyw7gzmf9hbywswakh5") (l "heif")))

(define-public crate-heif-sys-0.1.1 (c (n "heif-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.45") (d #t) (k 1)))) (h "13bnklr1a5kn4rxnc6ih4bqj72l4a8bbaxw3q89mf6m9f2ksbr8a") (l "heif")))

