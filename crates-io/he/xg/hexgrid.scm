(define-module (crates-io he xg hexgrid) #:use-module (crates-io))

(define-public crate-hexgrid-0.1.0 (c (n "hexgrid") (v "0.1.0") (h "0w9d5pyjmx8fyb3mbnffz0qnihg9hbb3sav424pp5sqrw05law18")))

(define-public crate-hexgrid-0.2.0 (c (n "hexgrid") (v "0.2.0") (h "0z87w437hr2cry1ai0c2z0vxbrissr0nhhzl39mriyh1baw3pq2y")))

