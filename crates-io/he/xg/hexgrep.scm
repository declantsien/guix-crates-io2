(define-module (crates-io he xg hexgrep) #:use-module (crates-io))

(define-public crate-hexgrep-0.0.0 (c (n "hexgrep") (v "0.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "iocore") (r "^0.10.0") (d #t) (k 0)))) (h "10wkhah0swkc4isfvpqdv20xhrfnbwwi3dyg7c1b0lh3g7c4xyzg")))

