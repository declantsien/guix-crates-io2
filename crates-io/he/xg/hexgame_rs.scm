(define-module (crates-io he xg hexgame_rs) #:use-module (crates-io))

(define-public crate-hexgame_rs-0.1.0 (c (n "hexgame_rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "158z83z71fi7lqhlyjsfr8bg2pvl4zkdq4r7vlh8131bsz17v9yx")))

(define-public crate-hexgame_rs-0.1.1 (c (n "hexgame_rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "139ndhvi01dszfdqdi4idp90npv0bqjajq0xnhnjr4dim9cdw713")))

(define-public crate-hexgame_rs-0.1.2 (c (n "hexgame_rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "1b6rlnd6mlxw4ah087ll97vcnilk4f0g9rdqr6mxm17zk2srszd4")))

(define-public crate-hexgame_rs-0.1.3 (c (n "hexgame_rs") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "16fizll72f2chd06kws5wm9jayrk7rdw4ndvr7rbwnhm557fha5g")))

