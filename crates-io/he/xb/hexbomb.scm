(define-module (crates-io he xb hexbomb) #:use-module (crates-io))

(define-public crate-hexbomb-0.1.0 (c (n "hexbomb") (v "0.1.0") (h "1a4lvpf64898bbfjma82lal3p5ldagyk688slianannw94x2sbwv")))

(define-public crate-hexbomb-0.2.0 (c (n "hexbomb") (v "0.2.0") (d (list (d (n "arguably") (r ">=2.0.0-alpha, <3.0.0") (d #t) (k 0)) (d (n "colored") (r ">=2.0.0, <3.0.0") (d #t) (k 0)))) (h "1snkyqblq407sxwf4qc5wv8hl9y0drb4d1xj46s34asifm4afs3p")))

(define-public crate-hexbomb-0.2.1 (c (n "hexbomb") (v "0.2.1") (d (list (d (n "arguably") (r ">=2.0.0-alpha, <3.0.0") (d #t) (k 0)) (d (n "colored") (r ">=2.0.0, <3.0.0") (d #t) (k 0)))) (h "0k0g52mz6w2c17hjbmnkas78yrmsvyfm4s92d3ackwzvix7nv8c0")))

(define-public crate-hexbomb-0.3.0 (c (n "hexbomb") (v "0.3.0") (d (list (d (n "arguably") (r "^2.0.0-alpha") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "11is2104jrc20ycmcsdddqr6gk5bkw67c5gl302rra516z97ak9g")))

(define-public crate-hexbomb-0.3.1 (c (n "hexbomb") (v "0.3.1") (d (list (d (n "arguably") (r "^2.0.0-alpha") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1wbwcf6xcpdgc86hfxyr1cvvgjdzw205f8wvc8cgc6a2cm3lcysi")))

(define-public crate-hexbomb-0.3.2 (c (n "hexbomb") (v "0.3.2") (d (list (d (n "arguably") (r "^2.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0q9g1pxccpabjxw6b1sfyn9zf3jsch1yg8kcxjwdiy1xva1wpia8")))

