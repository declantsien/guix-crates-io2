(define-module (crates-io he tz hetzner-robot-rs) #:use-module (crates-io))

(define-public crate-hetzner-robot-rs-0.0.1 (c (n "hetzner-robot-rs") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1d06s6mhavqsm5sb7m7ns81z7ibks5d5n4rv167zy29nb2d68by7") (y #t)))

(define-public crate-hetzner-robot-rs-0.0.1-alpha.1 (c (n "hetzner-robot-rs") (v "0.0.1-alpha.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1qlbg3yzh5bg861ckcdwcgq874r99sz82x2a4ma80xhq544sayz5")))

