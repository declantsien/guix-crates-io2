(define-module (crates-io he ar heartbeats-simple) #:use-module (crates-io))

(define-public crate-heartbeats-simple-0.2.0 (c (n "heartbeats-simple") (v "0.2.0") (d (list (d (n "hbs") (r "^0.2") (d #t) (k 0)) (d (n "hbs-acc") (r "^0.2") (d #t) (k 0)) (d (n "hbs-acc-pow") (r "^0.2") (d #t) (k 0)) (d (n "hbs-pow") (r "^0.2") (d #t) (k 0)))) (h "0cy9xlwpym19kdjnw6a6j04rnjx781dhdrx68bpgg7gq9z5czd9k")))

(define-public crate-heartbeats-simple-0.3.0 (c (n "heartbeats-simple") (v "0.3.0") (d (list (d (n "heartbeats-simple-sys") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cb04p6xw3f1hfwgciqrxn7vy04rplj1as8z7zfhpkga483q3h3q")))

(define-public crate-heartbeats-simple-0.4.0 (c (n "heartbeats-simple") (v "0.4.0") (d (list (d (n "heartbeats-simple-sys") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p5a733r7zwv3hsksrsapaljnc389vlcss9cbzcyjm9r4g707l4s")))

(define-public crate-heartbeats-simple-0.4.1 (c (n "heartbeats-simple") (v "0.4.1") (d (list (d (n "heartbeats-simple-sys") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12z2hr7xsy6wlfjdad9wb4jqc1xrbfgxamqa4rlakgqxpzgzggkq")))

