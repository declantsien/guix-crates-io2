(define-module (crates-io he ar heartfelt) #:use-module (crates-io))

(define-public crate-heartfelt-0.1.0 (c (n "heartfelt") (v "0.1.0") (d (list (d (n "ahash") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "regex-lexer") (r "^0.1.0") (d #t) (k 0)))) (h "1xcvai94p56klifl81zscsr25wrgm7zcpyv6g8d3pbh12zhdnklv")))

(define-public crate-heartfelt-0.1.1 (c (n "heartfelt") (v "0.1.1") (d (list (d (n "ahash") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "regex-lexer") (r "^0.1.0") (d #t) (k 0)))) (h "1gsdqhr7c17ldnpd8lls7qqvpcap0vdidbrjghylriqcf9fzkgg3")))

(define-public crate-heartfelt-0.1.2 (c (n "heartfelt") (v "0.1.2") (d (list (d (n "ahash") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "regex-lexer") (r "^0.1.0") (d #t) (k 0)))) (h "10lraawi90h5b2knhvp2skvpx1gqcaqjk2bg6sqay7l0rfvqh2ni")))

(define-public crate-heartfelt-0.1.3 (c (n "heartfelt") (v "0.1.3") (d (list (d (n "ahash") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "regex-lexer") (r "^0.1.0") (d #t) (k 0)))) (h "1w0v4ghmw5n058wq60rzmralgpbqny49dhhxh5ygqrdirf0bnz3i")))

(define-public crate-heartfelt-0.1.5 (c (n "heartfelt") (v "0.1.5") (d (list (d (n "ahash") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "regex-lexer") (r "^0.1.0") (d #t) (k 0)))) (h "0hqffck874gk36akicnn1rrb8iml0v5x1836xycr381gj09jrk53")))

