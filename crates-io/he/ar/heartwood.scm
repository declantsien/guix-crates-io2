(define-module (crates-io he ar heartwood) #:use-module (crates-io))

(define-public crate-heartwood-0.1.0 (c (n "heartwood") (v "0.1.0") (h "1w14k7j423kiy8a9yidk44bgx1pya4qmv3mjhsjmsbsf5bihjs2w")))

(define-public crate-heartwood-0.1.1 (c (n "heartwood") (v "0.1.1") (h "0jgypygc54zh5cl8gfaglywn0k3rns40zlsbrpjyffmj1q1h3yy1")))

