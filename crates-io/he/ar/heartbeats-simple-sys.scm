(define-module (crates-io he ar heartbeats-simple-sys) #:use-module (crates-io))

(define-public crate-heartbeats-simple-sys-0.2.0 (c (n "heartbeats-simple-sys") (v "0.2.0") (d (list (d (n "hbs-acc-pow-sys") (r "^0.2") (d #t) (k 0)) (d (n "hbs-acc-sys") (r "^0.2") (d #t) (k 0)) (d (n "hbs-common-sys") (r "^0.2") (d #t) (k 0)) (d (n "hbs-pow-sys") (r "^0.2") (d #t) (k 0)) (d (n "hbs-sys") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0jk51n8qhqyp1wjn37bkhyhg6b6b7rs5c1q0710i2zmsmx7jnln5")))

(define-public crate-heartbeats-simple-sys-0.2.1 (c (n "heartbeats-simple-sys") (v "0.2.1") (d (list (d (n "hbs-acc-pow-sys") (r "^0.2") (d #t) (k 0)) (d (n "hbs-acc-sys") (r "^0.2") (d #t) (k 0)) (d (n "hbs-common-sys") (r "^0.2") (d #t) (k 0)) (d (n "hbs-pow-sys") (r "^0.2") (d #t) (k 0)) (d (n "hbs-sys") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1mis2rhkg8d6mc9ir6hsrdbpm5hfrs6vq8zjhpnr0lbqd32b55hz")))

(define-public crate-heartbeats-simple-sys-0.3.0 (c (n "heartbeats-simple-sys") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0022h1056piyzvjflfvj53pkdz5jmmhz66l5fwg0gnqqg1sf8dil")))

(define-public crate-heartbeats-simple-sys-0.3.1 (c (n "heartbeats-simple-sys") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0pd405gzvgc88yn2k8p81yha5wx0jmcvm512myikrr0gp1k5d5wx")))

(define-public crate-heartbeats-simple-sys-0.3.2 (c (n "heartbeats-simple-sys") (v "0.3.2") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "008kvi0bxcg555jj5af9ri7nmv0v7j2gk0gkf90pypb62xvbdi2k")))

(define-public crate-heartbeats-simple-sys-0.4.0 (c (n "heartbeats-simple-sys") (v "0.4.0") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1al0sldk1cvaciq5igsg67wphc9zmia2j03vwlhmawjg62f87rwb")))

(define-public crate-heartbeats-simple-sys-0.4.1 (c (n "heartbeats-simple-sys") (v "0.4.1") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "195iizchzixi074yh1n0vyp1jy1pf0f8dxs9007cq9ql0700i971")))

(define-public crate-heartbeats-simple-sys-0.4.2 (c (n "heartbeats-simple-sys") (v "0.4.2") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0bn244nw61xhfd3h1d004358m84h3j1byhpjayiplb2vnhcj8idc")))

(define-public crate-heartbeats-simple-sys-0.4.3 (c (n "heartbeats-simple-sys") (v "0.4.3") (d (list (d (n "cmake") (r "^0.1.13") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0df94icyaacbl186b1ih135gymg8qlxxd2i8bl0jjgyw5x46xw19")))

