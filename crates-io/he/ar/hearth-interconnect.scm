(define-module (crates-io he ar hearth-interconnect) #:use-module (crates-io))

(define-public crate-hearth-interconnect-0.1.0 (c (n "hearth-interconnect") (v "0.1.0") (d (list (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1ikb2dzqw17xq2jmvyw494h3lphx28jxkjppq36j50hb3bg858vi")))

