(define-module (crates-io he ar heartbeat-server) #:use-module (crates-io))

(define-public crate-heartbeat-server-0.1.0 (c (n "heartbeat-server") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "pling") (r "^0.2.0") (f (quote ("http-sync"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)))) (h "1nc2dkw51pjcd2m9g96iqii9nc81r0xf3lp0n4560bpjynrhg6kz")))

