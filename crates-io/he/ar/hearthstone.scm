(define-module (crates-io he ar hearthstone) #:use-module (crates-io))

(define-public crate-hearthstone-0.1.0 (c (n "hearthstone") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l2d5k1dnk23jh0dcm9xgyy6sdc13xxnxdlnykk861j7wf8sv3bg")))

