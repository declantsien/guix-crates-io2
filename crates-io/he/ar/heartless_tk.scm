(define-module (crates-io he ar heartless_tk) #:use-module (crates-io))

(define-public crate-heartless_tk-0.1.0 (c (n "heartless_tk") (v "0.1.0") (d (list (d (n "bind") (r "^0.1.1") (d #t) (k 0)) (d (n "heartless") (r "^0.1") (f (quote ("altio"))) (d #t) (k 0)) (d (n "heartless_assets") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "tcl") (r "^0.1.9") (d #t) (k 0)) (d (n "tk") (r "^0.1.10") (d #t) (k 0)))) (h "145ij70il7nwma5gw1cak4y88m6458w8yxcsw4x6zmcvl9njf93d")))

