(define-module (crates-io he ar heartless) #:use-module (crates-io))

(define-public crate-heartless-0.1.0 (c (n "heartless") (v "0.1.0") (d (list (d (n "altio") (r "^0.2") (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "1d7whv5n9j0gdjhs8z0ljf1h0q1f8x5khxh3yf2980ij2bl2g26r") (f (quote (("test-replica") ("altio" "altio/altio"))))))

