(define-module (crates-io he ra heraclitus-compiler) #:use-module (crates-io))

(define-public crate-heraclitus-compiler-0.1.0 (c (n "heraclitus-compiler") (v "0.1.0") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "06k6ifg3n1nmq43q65c9hp916yid3xr48khjcnymx28a6dpjffnb")))

(define-public crate-heraclitus-compiler-0.2.0 (c (n "heraclitus-compiler") (v "0.2.0") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "11r1bq51n63b42aqlvqcmwagynfzfk6dz43pav3viq50bdg60yza")))

(define-public crate-heraclitus-compiler-0.2.1 (c (n "heraclitus-compiler") (v "0.2.1") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "0sj9cqcq2d3yvmzwzrwxcypyz23yk11z0rv3k500p5b1b90mqxqx")))

(define-public crate-heraclitus-compiler-1.2.2 (c (n "heraclitus-compiler") (v "1.2.2") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "1ivlhws9v539p63sn8rlkx89rg9q9hvdjxb7hdm99rzzivmiwhb3")))

(define-public crate-heraclitus-compiler-1.2.3 (c (n "heraclitus-compiler") (v "1.2.3") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "1x41m1cd2vlwzphy3jzyvyvnzi3hqvfs1mkyfpb7p6nq041ff1r7")))

(define-public crate-heraclitus-compiler-1.2.4 (c (n "heraclitus-compiler") (v "1.2.4") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "0zf4nwia6yp8b3ljjyckz2g8x06nqx2mfwgg996hz7ycnp4d1s95")))

(define-public crate-heraclitus-compiler-1.2.5 (c (n "heraclitus-compiler") (v "1.2.5") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "1wj712an3dzxzs76xxwb6iq4prv51na6j8z2j38bh8f70dbdszzk")))

(define-public crate-heraclitus-compiler-1.2.6 (c (n "heraclitus-compiler") (v "1.2.6") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "0ibzwd5n1rxznxhxrzyn7vykiiw5da0c0048c5ilgl5bn2220nqa")))

(define-public crate-heraclitus-compiler-1.2.7 (c (n "heraclitus-compiler") (v "1.2.7") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "0k9bmv04r3khsirxd6hfs313gki46ppycq37paln9sad7ishswhy")))

(define-public crate-heraclitus-compiler-1.2.8 (c (n "heraclitus-compiler") (v "1.2.8") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "0x54j0bc46cvkljynhd5cjahazx831a5vkb26j1pc2lyczl3nccx")))

(define-public crate-heraclitus-compiler-1.2.9 (c (n "heraclitus-compiler") (v "1.2.9") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "1k9z84apmljygapj3kjj2fhl31ndpf4828ggp02kgwfqsidxhvfn")))

(define-public crate-heraclitus-compiler-1.3.0 (c (n "heraclitus-compiler") (v "1.3.0") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "03519hqncwnaw2ldfh3rmlwxdb8fa5zfd32625i1bhywvd5386zx")))

(define-public crate-heraclitus-compiler-1.3.1 (c (n "heraclitus-compiler") (v "1.3.1") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "1rj50gxsjjd1bsc95d5h31ag27vvgq17sg00km2jwmrzjpaxw2ml")))

(define-public crate-heraclitus-compiler-1.3.2 (c (n "heraclitus-compiler") (v "1.3.2") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "1gyjz8wy2ylr3z7g6cahlcbh5nkkxdl571nzk2sylkv6d8nd4jif")))

(define-public crate-heraclitus-compiler-1.4.0 (c (n "heraclitus-compiler") (v "1.4.0") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "186frrql8mknxahg71cpwl5drcxv1iig2shhknmkc8fvaqyni05n")))

(define-public crate-heraclitus-compiler-1.5.0 (c (n "heraclitus-compiler") (v "1.5.0") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "1sy9vlf74lda2mh1jf5psa22468r0wsds4zw502387fmn1s2l6r2")))

(define-public crate-heraclitus-compiler-1.5.1 (c (n "heraclitus-compiler") (v "1.5.1") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "0k4pzqdbsk2vqpscac10kylvz53pjp4mm66c7lpmr7df6nsr2sw6")))

(define-public crate-heraclitus-compiler-1.5.2 (c (n "heraclitus-compiler") (v "1.5.2") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "1v0bndsnja4b7xsh8zydjs9k8l5b4v4j1b512bqm1pgnd6455f9g")))

(define-public crate-heraclitus-compiler-1.5.3 (c (n "heraclitus-compiler") (v "1.5.3") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "039bcyb6p8kvs41hpznlq66ia51f8nkh3vjb92vc3wbhfgyqaqwb")))

(define-public crate-heraclitus-compiler-1.5.4 (c (n "heraclitus-compiler") (v "1.5.4") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "08vj0p8ygh2b3fa9mw9rllfr6z32i60ywr4vibgzrk7qhd343sqj")))

(define-public crate-heraclitus-compiler-1.5.5 (c (n "heraclitus-compiler") (v "1.5.5") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "0d9kq4znxalj57pjzfrm7vabifpnv4idwgb7nw7cd6rw83my2rg0")))

(define-public crate-heraclitus-compiler-1.5.6 (c (n "heraclitus-compiler") (v "1.5.6") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "0sx20q4hxvzavrnf583kp6mjwbsw5r5rhygalv6ac7nkq7589zh7")))

(define-public crate-heraclitus-compiler-1.5.7 (c (n "heraclitus-compiler") (v "1.5.7") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "150xpb2j9ri1mcs4rxfpfpyr1mqpf2qw8d5mrl1hcrl01yzvz3f6")))

(define-public crate-heraclitus-compiler-1.5.8 (c (n "heraclitus-compiler") (v "1.5.8") (d (list (d (n "capitalize") (r "^0.1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "0hzb9l3xgb4bkbfn3gjx49rkyz5hx939a6mdj5x4gxfva1qx6c0v")))

