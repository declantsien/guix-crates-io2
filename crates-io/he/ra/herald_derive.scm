(define-module (crates-io he ra herald_derive) #:use-module (crates-io))

(define-public crate-herald_derive-0.1.0 (c (n "herald_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00cb82p1srpmc5a1yv775kwfw8dks5gvjx0h9aqpzgad2wbzgxpg")))

