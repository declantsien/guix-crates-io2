(define-module (crates-io he ra hera) #:use-module (crates-io))

(define-public crate-hera-0.1.0 (c (n "hera") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.9") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokei") (r "^8") (d #t) (k 0)))) (h "1sd2lvlzhgmxnzs6xcjmv577xl6f5j421ndsszpk3lnkvc7665sh")))

