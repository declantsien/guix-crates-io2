(define-module (crates-io he ra herald) #:use-module (crates-io))

(define-public crate-herald-0.1.0 (c (n "herald") (v "0.1.0") (d (list (d (n "herald_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rxrust") (r "^0.8.3") (d #t) (k 0)))) (h "0g33qh7f3nky3aiw5raw1rb8bnwnmx8s30bb1810j9kkf0psx5gh")))

