(define-module (crates-io he ph heph-inbox) #:use-module (crates-io))

(define-public crate-heph-inbox-0.1.0 (c (n "heph-inbox") (v "0.1.0") (d (list (d (n "futures-test") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "parking_lot") (r "^0.11.0") (k 0)))) (h "0hvrk2v622zn5jpb4jlwl38wvqgqlcxyrhl3izk7bi4r4gi8mjs1") (f (quote (("stress_testing"))))))

(define-public crate-heph-inbox-0.1.1 (c (n "heph-inbox") (v "0.1.1") (d (list (d (n "futures-test") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "parking_lot") (r "^0.11.0") (k 0)))) (h "08xm8vqkqhdhf3szlka1q31bjnrbjp5jgsc0ak4x4ffz7m1kfsmg") (f (quote (("stress_testing"))))))

(define-public crate-heph-inbox-0.1.2 (c (n "heph-inbox") (v "0.1.2") (d (list (d (n "futures-test") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "parking_lot") (r "^0.11.0") (k 0)))) (h "0svw2nh7kp8a8dplb20z6c53a2fcn7ll72sizdys0la8szc49sjy") (f (quote (("stress_testing"))))))

(define-public crate-heph-inbox-0.1.3 (c (n "heph-inbox") (v "0.1.3") (d (list (d (n "futures-test") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "parking_lot") (r "^0.11.0") (k 0)))) (h "183l5cr05b4vp6xcs77d95lc6zvm99wn1fs4qzr4bzvsgd8iidzx") (f (quote (("stress_testing"))))))

(define-public crate-heph-inbox-0.2.0 (c (n "heph-inbox") (v "0.2.0") (d (list (d (n "futures-test") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "parking_lot") (r "^0.11.0") (k 0)))) (h "0385n7acknqr6gy0z3n89j40qbk0ps20a0bj6nxj8dbcai5z325g") (f (quote (("stress_testing"))))))

(define-public crate-heph-inbox-0.2.1 (c (n "heph-inbox") (v "0.2.1") (d (list (d (n "futures-test") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "parking_lot") (r "^0.11.0") (k 0)))) (h "0db4d5mm8liqsrkxcl7q9h0ljicn3sf5mra0dricvcfflji6c0gj") (f (quote (("stress_testing"))))))

(define-public crate-heph-inbox-0.2.2 (c (n "heph-inbox") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.11.0") (k 0)))) (h "0dmrbvwn2456pmwqp3yf21dfa4101rhf0jfxql6hjinbpphfkbac") (f (quote (("stress_testing"))))))

(define-public crate-heph-inbox-0.2.3 (c (n "heph-inbox") (v "0.2.3") (h "00dx1vv9hphcni230zi535vhqc99zmjqm4sh56k0v8ba6665c5nc") (f (quote (("unstable_nightly") ("stress_testing"))))))

