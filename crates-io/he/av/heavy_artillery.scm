(define-module (crates-io he av heavy_artillery) #:use-module (crates-io))

(define-public crate-heavy_artillery-0.1.0 (c (n "heavy_artillery") (v "0.1.0") (h "04r7ldd0kbc2wymryc9bmbfjgz1yvxy0ijbyp51bsmayklpn0ngw") (y #t)))

(define-public crate-heavy_artillery-0.1.1 (c (n "heavy_artillery") (v "0.1.1") (h "0fw4qg6xc2m7712mv8hxzfhidw4ihn22w1pjlnkwh63cvmq7ycn2") (y #t)))

(define-public crate-heavy_artillery-0.1.2 (c (n "heavy_artillery") (v "0.1.2") (h "1hsa5a0j33lk6b1zj4qd07zy64mmjjwlzs78bh1h5dd6f6i7k6lb")))

