(define-module (crates-io he av heavyli) #:use-module (crates-io))

(define-public crate-heavyli-0.0.4 (c (n "heavyli") (v "0.0.4") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.45.0") (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^0") (d #t) (k 0)))) (h "06pvw4d2z35cr7p4lwvidbywmayv1dafrsc0pc2hhviaa04mz8sr") (y #t)))

(define-public crate-heavyli-0.0.5 (c (n "heavyli") (v "0.0.5") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.45.0") (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^0") (d #t) (k 0)))) (h "07bxvgksfhi5lyh024r3gig4j5am6wr3sqdyyq30ns798pn8w33x") (y #t)))

(define-public crate-heavyli-0.0.6 (c (n "heavyli") (v "0.0.6") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.45.0") (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^0") (d #t) (k 0)))) (h "0vibxyhxayq72ri9sx5l1fjbfzpgw1x1gkkl634ijblw27ghbskx") (y #t)))

