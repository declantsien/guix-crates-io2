(define-module (crates-io he ap heapless-bytes) #:use-module (crates-io))

(define-public crate-heapless-bytes-0.1.0 (c (n "heapless-bytes") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_cbor") (r "^0.11.0") (o #t) (k 0)))) (h "1ha4qz3zvmj3ql5mj8f6a91z9a5314f8a1vi3aycfl1sc1q7gqia") (f (quote (("cbor" "serde_cbor"))))))

(define-public crate-heapless-bytes-0.1.1 (c (n "heapless-bytes") (v "0.1.1") (d (list (d (n "heapless") (r "^0.5.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_cbor") (r "^0.11.0") (o #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "026l9ra94y9ggd3b2wgd5nlmmdpzrw4cnl0m30n3mdgh6wkq9x1b") (f (quote (("cbor" "serde_cbor"))))))

(define-public crate-heapless-bytes-0.2.0 (c (n "heapless-bytes") (v "0.2.0") (d (list (d (n "heapless") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_cbor") (r "^0.11.0") (o #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "12mlm6w5z411swqlb0vvl9bs23bjh5chvlnnwk1i4nchk5s7ja8p") (f (quote (("cbor" "serde_cbor"))))))

(define-public crate-heapless-bytes-0.3.0 (c (n "heapless-bytes") (v "0.3.0") (d (list (d (n "heapless") (r "^0.7") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_cbor") (r "^0.11.0") (o #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "12zlnvgrfzh1mx3hvzhgb1qaldbxdqdir7pv2ngkxby6faifp1bj") (f (quote (("cbor" "serde_cbor"))))))

