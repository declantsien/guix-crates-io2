(define-module (crates-io he ap heapnotize) #:use-module (crates-io))

(define-public crate-heapnotize-0.1.0 (c (n "heapnotize") (v "0.1.0") (h "0k04fh0hifg59mlyyqllm4rnpc47pradqkqx07k7c55lqjf0qb60") (y #t)))

(define-public crate-heapnotize-0.2.0 (c (n "heapnotize") (v "0.2.0") (h "0d7ziq096dhdz3xxnvdl7jyqh8459x0c4z0zf0fjq4hgl2bi6yfk")))

(define-public crate-heapnotize-0.2.1 (c (n "heapnotize") (v "0.2.1") (h "02w6szysqwivzv59irc7dj82mmkvxqmcr06l7992yq5djfikspqz")))

(define-public crate-heapnotize-1.0.0 (c (n "heapnotize") (v "1.0.0") (h "180r0bbwbmk4aanvmlmg59pg3cfkcza27kppmicz4ykmw48ybayh")))

(define-public crate-heapnotize-1.1.0 (c (n "heapnotize") (v "1.1.0") (h "17655mlbmqkidlqai5xcj0bhsyp4avafk8mnzgcnc38fsh35m5sm")))

