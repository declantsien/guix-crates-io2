(define-module (crates-io he ap heaparray) #:use-module (crates-io))

(define-public crate-heaparray-0.1.0 (c (n "heaparray") (v "0.1.0") (h "0m5mrg1gr39lprvwf6h0mf98mki2qark8av36ifhqyqnyvsfgbjd") (y #t)))

(define-public crate-heaparray-0.1.1 (c (n "heaparray") (v "0.1.1") (h "1slsjn13pyyx0zhnmxdxxgxbfm0xrs1a8pp30viqwninwp3m38hz") (y #t)))

(define-public crate-heaparray-0.1.2 (c (n "heaparray") (v "0.1.2") (d (list (d (n "containers-rs") (r "^0.1.3") (d #t) (k 0)))) (h "0i9izm6lf7paydgvkgzi7da7prwkl396nskd04fvmqvchpxvvbla") (y #t)))

(define-public crate-heaparray-0.1.3 (c (n "heaparray") (v "0.1.3") (d (list (d (n "containers-rs") (r "^0.1.3") (d #t) (k 0)))) (h "0srs7vz5bhwnxnix85fzpk9hpqcjgzfpvyj2hr1vzh9vj9q6pv9k") (y #t)))

(define-public crate-heaparray-0.1.4 (c (n "heaparray") (v "0.1.4") (d (list (d (n "containers-rs") (r "^0.1.3") (d #t) (k 0)))) (h "171ddjrq86nydda4vqav60034sp38vn3230hpcz5cqiqharnb954") (y #t)))

(define-public crate-heaparray-0.1.5 (c (n "heaparray") (v "0.1.5") (d (list (d (n "containers-rs") (r "= 0.1.4") (d #t) (k 0)))) (h "00z7fqlhr2bcadmfakkrbf98bywwn0sskzzkb13wr310xixlzl7c") (y #t)))

(define-public crate-heaparray-0.1.6 (c (n "heaparray") (v "0.1.6") (d (list (d (n "containers-rs") (r "^0.1.6") (d #t) (k 0)))) (h "1x1mrq1a9ggw591d97s8jh6nl6n5ch2wgj8jlpam4k8nin9hl2yk") (y #t)))

(define-public crate-heaparray-0.2.0 (c (n "heaparray") (v "0.2.0") (d (list (d (n "containers-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "interloc") (r "^0.1.0") (d #t) (k 2)))) (h "09b182ycmxr02fpa284jwh5z7w2r822n68sfg9ggngbbmdi2zysz") (y #t)))

(define-public crate-heaparray-0.2.1 (c (n "heaparray") (v "0.2.1") (d (list (d (n "containers-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "interloc") (r "^0.1.0") (d #t) (k 2)) (d (n "trait_tests") (r "^0.3.3") (d #t) (k 2)))) (h "0ijlpr9gjdw969xz4jy9dw8h6kw0qiba5d0pjif3zwfz4xj8irbk") (y #t)))

(define-public crate-heaparray-0.3.0 (c (n "heaparray") (v "0.3.0") (d (list (d (n "containers-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "interloc") (r "^0.1.0") (d #t) (k 2)))) (h "1s578z5py4acc32l8csh2dpicgis34jc1rnkv43sp7nz56lnysim") (f (quote (("no-asserts") ("default")))) (y #t)))

(define-public crate-heaparray-0.4.0 (c (n "heaparray") (v "0.4.0") (d (list (d (n "containers-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "interloc") (r "^0.1.0") (d #t) (k 2)))) (h "1ljgjndry6z1qnhywpd59gp4jlb78rqky2bkbnhys38mv1r1ffxm") (f (quote (("real-black-box") ("no-asserts") ("fast-drop") ("default")))) (y #t)))

(define-public crate-heaparray-0.4.1 (c (n "heaparray") (v "0.4.1") (d (list (d (n "containers-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "interloc") (r "^0.1.0") (d #t) (k 2)))) (h "0gnhl3ypkv71z9kivhfrx43vhh9b43pmfrc60qy42ripkjaqcdzc") (f (quote (("real-black-box") ("no-asserts") ("fast-drop") ("default")))) (y #t)))

(define-public crate-heaparray-0.4.2 (c (n "heaparray") (v "0.4.2") (d (list (d (n "containers-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "interloc") (r "^0.1.0") (d #t) (k 2)))) (h "0j98zbk4pvyk64mwpmz0jhvc27yp2qj41rafy9vlppii5lllh0ph") (f (quote (("real-black-box") ("no-asserts") ("fast-drop") ("default")))) (y #t)))

(define-public crate-heaparray-0.4.3 (c (n "heaparray") (v "0.4.3") (d (list (d (n "containers-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "interloc") (r "^0.1.0") (d #t) (k 2)))) (h "02i3p1vzc5bf16r27vq0aa8xx6xxwsrhgy8v6jn1s0r2z1qfjn2x") (f (quote (("no-asserts") ("default")))) (y #t)))

(define-public crate-heaparray-0.5.0 (c (n "heaparray") (v "0.5.0") (d (list (d (n "atomic-types") (r "^0.1.0") (d #t) (k 0)) (d (n "const-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "containers-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "interloc") (r "^0.1.0") (d #t) (k 2)))) (h "0476qkgpir5nyfvjzv6sqrcsy0pwvk79csd6bagxcys1kdjjxjcc") (f (quote (("no-asserts" "mem-block-skip-all") ("mem-block-skip-size-check") ("mem-block-skip-ptr-check") ("mem-block-skip-layout-check") ("mem-block-skip-all" "mem-block-skip-layout-check" "mem-block-skip-ptr-check" "mem-block-skip-size-check") ("default"))))))

(define-public crate-heaparray-0.5.1 (c (n "heaparray") (v "0.5.1") (d (list (d (n "atomic-types") (r "^0.1.0") (d #t) (k 0)) (d (n "const-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "containers-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "interloc") (r "^0.1.0") (d #t) (k 2)))) (h "152lsl9a12kggpb7i72fjanh7vf5224xyn1d8bgs9kc9c0xjrrcd") (f (quote (("ref-counter-skip-overflow-check") ("ref-counter-skip-all" "ref-counter-skip-overflow-check") ("no-asserts" "mem-block-skip-all" "ref-counter-skip-all") ("mem-block-skip-size-check") ("mem-block-skip-ptr-check") ("mem-block-skip-layout-check") ("mem-block-skip-all" "mem-block-skip-layout-check" "mem-block-skip-ptr-check" "mem-block-skip-size-check") ("default"))))))

