(define-module (crates-io he ap heapmon) #:use-module (crates-io))

(define-public crate-heapmon-0.1.2 (c (n "heapmon") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0qpmrs87r6x4lbaqsfnyn2d6yqi1hnvsalj8hyn0ab5n5nbvi16g") (f (quote (("default")))) (r "1.45.2")))

