(define-module (crates-io he ap heapsize_plugin) #:use-module (crates-io))

(define-public crate-heapsize_plugin-0.0.1 (c (n "heapsize_plugin") (v "0.0.1") (h "1clkl1jmhfhsln5n1qy1288qgl3k3h535rfv70b0xm3d6sdzf5d1")))

(define-public crate-heapsize_plugin-0.1.0 (c (n "heapsize_plugin") (v "0.1.0") (h "0j0np9i0b5ibc17l9yzwwwvfmd4m2krix8nmnc462bq9zw699v7f")))

(define-public crate-heapsize_plugin-0.1.1 (c (n "heapsize_plugin") (v "0.1.1") (h "1h8gpbf554wrr3vpdbc8jmil416j8gf88zywvf5n46138qg54jg5")))

(define-public crate-heapsize_plugin-0.1.2 (c (n "heapsize_plugin") (v "0.1.2") (h "0ri95xmdjp3fv18wl4np451l9wc31mblb38v9w288rwkbr4glk9v")))

(define-public crate-heapsize_plugin-0.1.3 (c (n "heapsize_plugin") (v "0.1.3") (d (list (d (n "heapsize") (r "^0.3") (d #t) (k 2)))) (h "0r66l355chr3xfyy1y0ipcl1rknmg68rfdsri9v58nyxfdyb55gr")))

(define-public crate-heapsize_plugin-0.1.4 (c (n "heapsize_plugin") (v "0.1.4") (d (list (d (n "heapsize") (r "^0.3") (d #t) (k 2)))) (h "13wjsijpf84kxzfs9x9wr190846k0j9wz6r4r165njxs8mxhgrk7")))

(define-public crate-heapsize_plugin-0.1.5 (c (n "heapsize_plugin") (v "0.1.5") (d (list (d (n "heapsize") (r "^0.3") (d #t) (k 2)))) (h "0ld8grazasb612qr06rd98shy6ka8c3k5cz28d4j8gsk6rj7dk3a")))

(define-public crate-heapsize_plugin-0.1.6 (c (n "heapsize_plugin") (v "0.1.6") (d (list (d (n "heapsize") (r "^0.3") (d #t) (k 2)))) (h "1i72isf699q9jl167g2kg4xd6h3cd05rc79zaph58aqjy0g0m9y9")))

