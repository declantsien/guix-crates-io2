(define-module (crates-io he ap heapswap_schemas) #:use-module (crates-io))

(define-public crate-heapswap_schemas-0.0.1 (c (n "heapswap_schemas") (v "0.0.1") (d (list (d (n "capnp") (r "^0.19.1") (d #t) (k 0)) (d (n "capnpc") (r "^0.19.0") (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.28") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0bd68rrdz6bz779b05580cxc006y3qkbrfsadzszkbc5azivk17k")))

