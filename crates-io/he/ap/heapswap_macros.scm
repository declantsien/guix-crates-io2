(define-module (crates-io he ap heapswap_macros) #:use-module (crates-io))

(define-public crate-heapswap_macros-0.0.1 (c (n "heapswap_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7.32") (d #t) (k 0)))) (h "0qsi2lxwnimmy7fdc39rcm0x58rx2p21bm1n0mv8g8lffnmxknzz")))

(define-public crate-heapswap_macros-0.0.2 (c (n "heapswap_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7.32") (d #t) (k 0)))) (h "0lzgzpm2j1qb3rkacq0byvgnabcr98jdjiv809wqf9sspx9dgc8m")))

