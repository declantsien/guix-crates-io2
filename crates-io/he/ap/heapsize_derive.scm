(define-module (crates-io he ap heapsize_derive) #:use-module (crates-io))

(define-public crate-heapsize_derive-0.1.0 (c (n "heapsize_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)) (d (n "synstructure") (r "^0.2") (d #t) (k 0)))) (h "14n4gdmnhd12vy0755nd1l43nrbmli9s2q3b0h2xc3lk594skdx7")))

(define-public crate-heapsize_derive-0.1.1 (c (n "heapsize_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)) (d (n "synstructure") (r "^0.2") (d #t) (k 0)))) (h "1y2gh97jljh33zwxw66g5by2k9rx9rz8v47affwn0qid1w6dgarm")))

(define-public crate-heapsize_derive-0.1.2 (c (n "heapsize_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)) (d (n "synstructure") (r "^0.2") (d #t) (k 0)))) (h "1accvg1gh2fg6ylfnpbsmln7s42pxvvvk8g931njmr94j08brfhl")))

(define-public crate-heapsize_derive-0.1.3 (c (n "heapsize_derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)) (d (n "synstructure") (r "^0.4") (d #t) (k 0)))) (h "1zmb4ijsx1m5gw8241gkijv6l1z0bm16y09z0zwycgbcbf97cs2b")))

(define-public crate-heapsize_derive-0.1.4 (c (n "heapsize_derive") (v "0.1.4") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.5") (d #t) (k 0)))) (h "0pdvi566rihly4xhfq5bk00w733jcmhyz1bvr6ghar0mzd96vya6")))

