(define-module (crates-io he ap heapsz) #:use-module (crates-io))

(define-public crate-heapsz-0.0.1 (c (n "heapsz") (v "0.0.1") (h "1w20lkhqazj9pd5f073zzrif0zzgkb39v3awj9fccwv647lvfn9n")))

(define-public crate-heapsz-0.1.0 (c (n "heapsz") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "protobuf2") (r "^2") (o #t) (d #t) (k 0) (p "protobuf")) (d (n "snapbox") (r "^0.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1rry6hsx6i1h6k3z8bhscaw6xpmcjf08qisspc5y7kdzzr03v5s2") (s 2) (e (quote (("protobuf2" "dep:protobuf2") ("bytes" "dep:bytes"))))))

