(define-module (crates-io he ap heaptrack) #:use-module (crates-io))

(define-public crate-heaptrack-0.1.0 (c (n "heaptrack") (v "0.1.0") (h "1p4x5klwkcc5sd76181dj28j92jm92zy55zp709k872k8lpr00rg")))

(define-public crate-heaptrack-0.2.0 (c (n "heaptrack") (v "0.2.0") (h "1v86p21fx6csb02a2y3m9y6bcjbwqm4iwl6wpb402fgr2hhiakcl")))

(define-public crate-heaptrack-0.3.0 (c (n "heaptrack") (v "0.3.0") (h "16v0yvgi8sgxchad3yaqnvbxsfh49jplq06iqsff4nh484ikqpwa")))

(define-public crate-heaptrack-0.4.0 (c (n "heaptrack") (v "0.4.0") (d (list (d (n "ccl") (r "^4.13.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0n8bj0xh4i3pmcn9i94v96d64m11ksn1w0pr283255nk70xh1kl8")))

