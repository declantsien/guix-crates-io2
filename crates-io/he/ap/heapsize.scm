(define-module (crates-io he ap heapsize) #:use-module (crates-io))

(define-public crate-heapsize-0.1.0 (c (n "heapsize") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1cxd7sa3dqr2lqz7g9nh0vn3x2nx6plj2622cpfgis6qvzrdxb5x")))

(define-public crate-heapsize-0.1.1 (c (n "heapsize") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1h7hxmq2g4gdhlwgslaisdxdjasn421z9nxpbf43pl5c22s3qja2") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.1.2 (c (n "heapsize") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0pwcb9wz69rmsaz375xqphp40gy0r8r3889pfy6dd9ykm5p5nbi7") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.1.3 (c (n "heapsize") (v "0.1.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1w5fih355m7c70nqjc1cv9ss8w4wc8fmyf1c3ymdsvrjb2s60xhr") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.2.0 (c (n "heapsize") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14sc25xygxn7zpw3qhklzdk0ji93l8bk8vcv1d43fn9l7f8z0s0n") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.2.2 (c (n "heapsize") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dsq55mmciv4q191r8zxb12js65f9lv5mal7wiif33qc9z77pa7c") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.2.4 (c (n "heapsize") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pvab5i9cifd162hg2583j1g0mnsm592yqfdrpk16bn52v4nnc2i") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.2.5 (c (n "heapsize") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dlbmam024wk1kpj2n6yzxjpx2vxmbawyj3h8dcv61imm1ja2ynd") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.3.0 (c (n "heapsize") (v "0.3.0") (h "08dnphh82xiqyr3vsgb8m8z9brxx3wxpnp048zfxd62w1ml5lx2i") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.3.1 (c (n "heapsize") (v "0.3.1") (h "1n87ph209x275v9dd3gn9g6zxcn1vv53rvp9b4210qy1a1gzy9jn") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.3.2 (c (n "heapsize") (v "0.3.2") (h "0jwy2gzlsb04a52ad017vdxy2sws051amhz4imwph218vwbhzr3y") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.3.3 (c (n "heapsize") (v "0.3.3") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 1)))) (h "1ibqch69ml781r4c3mjxpq2l3a7ammw35994zdp207jzg69sgivv") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.3.4 (c (n "heapsize") (v "0.3.4") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)))) (h "01xq8vm4szdvpam6kp9mpvwpzi55lxkmv06r90vl7zrdfjw2slap") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.3.5 (c (n "heapsize") (v "0.3.5") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)))) (h "14ai45qxf4jrrbvjqrr65n5qic6rgi2smqmhh7px0wpbcwl3azwj") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.3.6 (c (n "heapsize") (v "0.3.6") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1723s654z5aj4v2lamxq93h0nn1g5jvygcxirx9y166kp2mhdcxb") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.3.7 (c (n "heapsize") (v "0.3.7") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0j9m3cw91apd2shfqv3xk0ih9sj2dv04s4v6jl8ak5c4fnaf304c") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.3.8 (c (n "heapsize") (v "0.3.8") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "120c0mcy7hj00mnjiipc360c8287pkn46hjhpbhfcnxq09s6ydss") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.3.9 (c (n "heapsize") (v "0.3.9") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0dmwc37vgsdjzk10443dj4f23439i9gch28jcwzmry3chrwx8v2m") (f (quote (("unstable"))))))

(define-public crate-heapsize-0.4.0 (c (n "heapsize") (v "0.4.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "1m5692g00siifdaxfqqdjj7cc8d45h5c4nf950wh0q91aaqr6xac") (f (quote (("unstable") ("flexible-tests"))))))

(define-public crate-heapsize-0.4.1 (c (n "heapsize") (v "0.4.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0dx2s4qx9fr12aji6bgl9n0cnsdjn0rby4yzwix17rbl8dib5yjl") (f (quote (("unstable") ("flexible-tests"))))))

(define-public crate-heapsize-0.4.2 (c (n "heapsize") (v "0.4.2") (d (list (d (n "winapi") (r "^0.3.4") (f (quote ("std" "heapapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q94q9ppqjgrw71swiyia4hgby2cz6dldp7ij57nkvhd6zmfcy8n") (f (quote (("unstable") ("flexible-tests"))))))

