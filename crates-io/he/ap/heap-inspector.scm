(define-module (crates-io he ap heap-inspector) #:use-module (crates-io))

(define-public crate-heap-inspector-0.0.1 (c (n "heap-inspector") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "1s2d39hz5lc04qp7dzwl6x2d5f2v5p7yjj52542kd9hsqdxq4j7a")))

(define-public crate-heap-inspector-0.0.2 (c (n "heap-inspector") (v "0.0.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0hr6a9ql8ny0xk0xi19vy1kavpq9s4xr99lxcc56mwaa9kk5d2qs")))

(define-public crate-heap-inspector-0.0.3 (c (n "heap-inspector") (v "0.0.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0samm1ayp4kzanq5zab8zf42sbhmrgqlsqx80pr6phi8wqrk8gkh")))

(define-public crate-heap-inspector-0.0.4 (c (n "heap-inspector") (v "0.0.4") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "017dwjyq6hjhia6sl9bpqilyx9cmh7szp6x6ai64qci2fr3hx6qq")))

