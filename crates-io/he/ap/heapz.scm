(define-module (crates-io he ap heapz) #:use-module (crates-io))

(define-public crate-heapz-0.1.2 (c (n "heapz") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0jbdd7yr2yv2602q96ljcsym4i0qc6x79qhava7slhx7s82wb46i")))

(define-public crate-heapz-0.1.3 (c (n "heapz") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1g62glhzawc8j01817h8rfn9rfc3y4wg0g074rwgh9gclicpa471")))

(define-public crate-heapz-1.0.0 (c (n "heapz") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1zccrg3ssncwlpg041l4wxhf8df5cssyp2fhi80lggah8qyw7d46")))

(define-public crate-heapz-1.0.1 (c (n "heapz") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "02q4bddqv5qnkah029wxb7d397s0higxqcnd4injg3krfhk7k8ix")))

(define-public crate-heapz-1.1.0 (c (n "heapz") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1l23xk8f41a27ig99zs2279vqyy0w6gd44lzfd9rd5amz65166vc")))

(define-public crate-heapz-1.1.2 (c (n "heapz") (v "1.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18bcy2005ca8d76q9binqzbk4kfmqsj3f6yvdflpi0kn0566b4p1")))

(define-public crate-heapz-1.1.3 (c (n "heapz") (v "1.1.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1z5akcrfkiqfa43wr6rs1sc7wpsgsr34r6vdsswkbpv0cw3pikfk")))

(define-public crate-heapz-1.1.4 (c (n "heapz") (v "1.1.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0priif3l3xscrbwng4rqqhgsk748lc77km14y9irqhsah0sfafy7")))

