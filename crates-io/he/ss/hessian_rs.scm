(define-module (crates-io he ss hessian_rs) #:use-module (crates-io))

(define-public crate-hessian_rs-0.0.1 (c (n "hessian_rs") (v "0.0.1") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "0fxbw2s2cnw119zi8lp78bi42lq2cqrvbpilx2kg3iwwa5qkg75l")))

(define-public crate-hessian_rs-0.0.2 (c (n "hessian_rs") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "1cln968np68a2hkgczmww87z11ni6dfikdn40dq8wws3j4shd9jj")))

(define-public crate-hessian_rs-0.0.3 (c (n "hessian_rs") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)))) (h "0bsj7blii99cfysysfd4zs62q82rnbiznh2481qpl3ymvibaf73d")))

(define-public crate-hessian_rs-0.0.4-rc1 (c (n "hessian_rs") (v "0.0.4-rc1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "0266iljzh3sfimjn8a0rxa6il6mkpqalx7mc1lzhc9d6wasqr0j0")))

(define-public crate-hessian_rs-0.0.4-rc2 (c (n "hessian_rs") (v "0.0.4-rc2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "0pg8czb38wh4gb8ahqh75mx0ivdsn9n6xiin62p2kcycldsskqr2")))

(define-public crate-hessian_rs-0.0.4-rc3 (c (n "hessian_rs") (v "0.0.4-rc3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "0qc6wiyjg35m33rcg4slk1kgg6shqivdnwr48m5fgnxyqdf280lw")))

