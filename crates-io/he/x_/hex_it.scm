(define-module (crates-io he x_ hex_it) #:use-module (crates-io))

(define-public crate-hex_it-0.1.0 (c (n "hex_it") (v "0.1.0") (d (list (d (n "term_size") (r "^0.3.0") (d #t) (k 0)) (d (n "tui-tools") (r "^0.1.2") (d #t) (k 0)))) (h "0f8wccavmm7ks587i4h2lwyh4ry9ymfpfiisbnk8iba4fgaqfq1w")))

(define-public crate-hex_it-0.1.1 (c (n "hex_it") (v "0.1.1") (d (list (d (n "term_size") (r "^0.3.0") (d #t) (k 0)) (d (n "tui-tools") (r "^0.1.2") (d #t) (k 0)))) (h "0pzic5vv8v5h16bl7gjivhx9ysn08hlrvr096f2dycp6hildxba7")))

