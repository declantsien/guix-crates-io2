(define-module (crates-io he x_ hex_d_hex) #:use-module (crates-io))

(define-public crate-hex_d_hex-0.1.0 (c (n "hex_d_hex") (v "0.1.0") (h "01sliy89sga98x4axij9vrlqshx67l65m73j65gv82mr757bfl2b")))

(define-public crate-hex_d_hex-1.0.0 (c (n "hex_d_hex") (v "1.0.0") (h "09899m4pgddw1zjf1898j1w9gngc1ph5achl2dn0kkf8gcasdrvq")))

(define-public crate-hex_d_hex-1.0.1 (c (n "hex_d_hex") (v "1.0.1") (h "09s8mw6p76xz3zqddlaan17sf343zgxz5z0g41492hwjv0ag9lpq")))

