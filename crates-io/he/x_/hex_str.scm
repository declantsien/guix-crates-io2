(define-module (crates-io he x_ hex_str) #:use-module (crates-io))

(define-public crate-hex_str-0.1.0 (c (n "hex_str") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (f (quote ("min_const_gen"))) (o #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10fb8n03h5f8gyaxlz6ks0gk4iinwlk4j5nl2sf47ppnv6k8nsj1") (s 2) (e (quote (("serde" "dep:serde") ("rand" "dep:rand"))))))

(define-public crate-hex_str-0.1.1 (c (n "hex_str") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (f (quote ("min_const_gen"))) (o #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0f80rmx9fpg8hq79b2nshhgqj2nxynv49506xwxmy1i8cbaylyb6") (s 2) (e (quote (("serde" "dep:serde") ("rand" "dep:rand"))))))

