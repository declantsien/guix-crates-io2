(define-module (crates-io he x_ hex_table) #:use-module (crates-io))

(define-public crate-hex_table-0.1.0 (c (n "hex_table") (v "0.1.0") (h "0i5wcd6w665kwrj5dwm2fii3y4mx6jy91wvcnh6af60i0mzbdm72")))

(define-public crate-hex_table-0.1.1 (c (n "hex_table") (v "0.1.1") (h "10b1ahwzhwg78dxlgrdn4gh8q26mjrg5mn6n4m85l1hmx620xj62")))

(define-public crate-hex_table-0.1.2 (c (n "hex_table") (v "0.1.2") (h "055vr5sygxyr7bam7llhf1bnvwbbbwdya5ygr4is8dzh7vzsxq5n")))

(define-public crate-hex_table-0.1.3 (c (n "hex_table") (v "0.1.3") (h "0cwc2fd7wa0xckc960fm7kcq08pkk14i97zbd5n2an247k3bkskd")))

(define-public crate-hex_table-0.1.4 (c (n "hex_table") (v "0.1.4") (h "1sc87p552vkrim7ba1kw0gxifbzjiichv94d81d050n95839qxjn")))

