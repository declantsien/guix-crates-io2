(define-module (crates-io he x_ hex_dump) #:use-module (crates-io))

(define-public crate-hex_dump-0.1.0 (c (n "hex_dump") (v "0.1.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "1kk03kqlqjh03yyzaamgg2d3yv7q636pwh5ljhdydzx4aqwl49pl")))

(define-public crate-hex_dump-0.1.1 (c (n "hex_dump") (v "0.1.1") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "11cdiw4hk92plgg1jmixjq5ihiwbbpkjv0vb4hfa75vr78mg2bcc")))

(define-public crate-hex_dump-0.1.2 (c (n "hex_dump") (v "0.1.2") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "0ijsq5i01cz8ibw8ypb91pja0nvcypbgd61vpwxczg3cc2kmijgz")))

(define-public crate-hex_dump-0.1.3 (c (n "hex_dump") (v "0.1.3") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)))) (h "0xh5widkdp9wf5ffjqwkm88qma6l33bmrnf3lpmllw1jab59jjks")))

(define-public crate-hex_dump-0.1.5 (c (n "hex_dump") (v "0.1.5") (d (list (d (n "doe") (r "^1.1.4") (d #t) (k 0)))) (h "1dzxnnnfqyj3jix9qyqbk7jv0xpzsy7sraiz4k8mdwm7g98pc0yw")))

