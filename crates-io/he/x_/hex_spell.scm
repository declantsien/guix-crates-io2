(define-module (crates-io he x_ hex_spell) #:use-module (crates-io))

(define-public crate-hex_spell-0.1.0 (c (n "hex_spell") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 2)))) (h "05c1g687akiwn1jvxyawqbwfn05wnmjafnd9rnv079dza94ys6ys") (y #t)))

(define-public crate-hex_spell-0.1.1 (c (n "hex_spell") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 2)))) (h "0aiz1ldj8b31r6mqm16ykqakbc4fp8cv3x3pdxmc4rgms1vhpw95") (y #t)))

(define-public crate-hex_spell-0.1.2 (c (n "hex_spell") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 2)))) (h "1w87n6dql6645bjpknhrjb7j1jqs0vwbgb5l93qnvnbqjc8jf4y7") (y #t)))

(define-public crate-hex_spell-0.1.3 (c (n "hex_spell") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 2)))) (h "096scf5igzbkf262bdb6vjp0a88if1fcjwamzzw4p5zd2icbfl22") (y #t)))

(define-public crate-hex_spell-0.1.4 (c (n "hex_spell") (v "0.1.4") (d (list (d (n "toml") (r "^0.8.12") (d #t) (k 2)))) (h "1q159mndrnqifmz9lqd8vlf2ns6cj7nis21fynwi2cq832qf4iss") (y #t)))

(define-public crate-hex_spell-0.1.5 (c (n "hex_spell") (v "0.1.5") (d (list (d (n "toml") (r "^0.8.12") (d #t) (k 2)))) (h "1nix9aw9dykpwc2y5dmavzmakgj6352vdbsxkbgjxpmz29m70kax") (y #t)))

