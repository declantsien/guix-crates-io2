(define-module (crates-io he x_ hex_lit) #:use-module (crates-io))

(define-public crate-hex_lit-0.1.0 (c (n "hex_lit") (v "0.1.0") (h "1wmx5v4axqsxasxa8q4r1adfhnazp62nab9vbsvvcav1zfxq0wwx") (f (quote (("rust_v_1_46"))))))

(define-public crate-hex_lit-0.1.1 (c (n "hex_lit") (v "0.1.1") (h "1g8bln2js2c49ray6fy6c9bk9wfm5hnwjspx7jqng60m7whx249h") (f (quote (("rust_v_1_46"))))))

