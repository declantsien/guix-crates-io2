(define-module (crates-io he x_ hex_fmt) #:use-module (crates-io))

(define-public crate-hex_fmt-0.1.0 (c (n "hex_fmt") (v "0.1.0") (h "1nh4zdj474qsyh4i168c0a0svkian5fv4pgf4s4rxv9k3p5vd7l7")))

(define-public crate-hex_fmt-0.2.0 (c (n "hex_fmt") (v "0.2.0") (h "12zqyxv5756dinissv0ph5c6rw9baa97d9n2b5i0r3ybjn4k7y16")))

(define-public crate-hex_fmt-0.3.0 (c (n "hex_fmt") (v "0.3.0") (h "0vrkzxd1wb4piij68fhmhycj01ky6nsn73piy37dk97h7xwn0zxh")))

