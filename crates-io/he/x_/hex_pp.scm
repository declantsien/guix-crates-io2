(define-module (crates-io he x_ hex_pp) #:use-module (crates-io))

(define-public crate-hex_pp-0.1.0 (c (n "hex_pp") (v "0.1.0") (h "1ii2p1qdnnz75rkj1db46kh750vppvmjgfg717q34w5wqra2rj1i") (y #t)))

(define-public crate-hex_pp-0.1.1 (c (n "hex_pp") (v "0.1.1") (h "0p5zzkhxv9hgvz94lb4r608f24n1v2dk8nk8wql8fypsrp3b0dmw") (y #t)))

(define-public crate-hex_pp-0.1.2 (c (n "hex_pp") (v "0.1.2") (h "123ks2vp58m66wkqzklv4n3db923c7459jgf2s240009jxnajd8v")))

