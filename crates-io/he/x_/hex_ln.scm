(define-module (crates-io he x_ hex_ln) #:use-module (crates-io))

(define-public crate-hex_ln-0.1.0 (c (n "hex_ln") (v "0.1.0") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.18.0") (k 0)) (d (n "bindgen") (r "^0.64") (k 1)))) (h "0bsq8s7qigy7nz8xxfla3acz6i580ky6h0k0kkmn8abvxdzphbi4") (f (quote (("no_std") ("log"))))))

