(define-module (crates-io he x_ hex_renderer) #:use-module (crates-io))

(define-public crate-hex_renderer-0.1.0 (c (n "hex_renderer") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.1") (d #t) (k 0)))) (h "0z914sd0g892jlbfpr6s8yg32krphx6iv3iqlgk67afm91c30wn3")))

(define-public crate-hex_renderer-0.2.0 (c (n "hex_renderer") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.1") (d #t) (k 0)))) (h "1365gxzmmym9lcqx5wdp9alkv0nv85fzknd75zdl346ir1xyi0v5")))

(define-public crate-hex_renderer-0.2.1 (c (n "hex_renderer") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.1") (d #t) (k 0)))) (h "0hjjb55wmaz8cax0925mxzx8324hpj5pwi3s7724hdswl4awz9c6")))

(define-public crate-hex_renderer-0.2.2 (c (n "hex_renderer") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.1") (d #t) (k 0)))) (h "1l8wwqghgpizh4k4896f7lc8nalnxggsl9mgfs35xc9jzvv4nd6s")))

(define-public crate-hex_renderer-0.2.3 (c (n "hex_renderer") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.1") (d #t) (k 0)))) (h "0yrb45q86swhx01x1awa7ybh980a7v6018mljnh0hn50iayf45c8")))

(define-public crate-hex_renderer-0.2.4 (c (n "hex_renderer") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11.1") (d #t) (k 0)))) (h "0iaalm67bkyz2w5d3h5h6n7llagmjh2mfrmkmpbhpi95cfdw3wwd")))

