(define-module (crates-io he x_ hex_gfx) #:use-module (crates-io))

(define-public crate-hex_gfx-0.0.1 (c (n "hex_gfx") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "hex_math") (r "^0.0.1") (d #t) (k 0)) (d (n "hex_win") (r "^0.0.1") (d #t) (k 0)))) (h "1pfahh8nxbq5ff256vk7w0kkj08dwnvkc4ah8f0m27mqip13awkl")))

