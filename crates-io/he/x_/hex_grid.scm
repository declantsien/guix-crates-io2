(define-module (crates-io he x_ hex_grid) #:use-module (crates-io))

(define-public crate-hex_grid-0.1.0 (c (n "hex_grid") (v "0.1.0") (h "0003hv13c8jpjkgv7iaxgjbqr6v8s5325pdf5s9rpc3iqrj23sa3")))

(define-public crate-hex_grid-0.1.1 (c (n "hex_grid") (v "0.1.1") (h "1mm3i05lfvfkcafgj4qdf0jcwki13nbp0ydl2rb922nmrz4gd5dq")))

(define-public crate-hex_grid-0.1.2 (c (n "hex_grid") (v "0.1.2") (h "1p7j5dd5i1501dvla4cfl2vqgnwh06ahwksl41ai4xf5b0mrmc8j")))

(define-public crate-hex_grid-0.2.0 (c (n "hex_grid") (v "0.2.0") (h "1xsaf9cwajxw7sf0hzz2gljp58l189k88ldnpn8fnryz41jb6gl4")))

(define-public crate-hex_grid-0.2.1 (c (n "hex_grid") (v "0.2.1") (h "0ifsmwrl0jx6pnjchw07s42mnhxw8j5jclxfywbfp93ip62h3ciw")))

