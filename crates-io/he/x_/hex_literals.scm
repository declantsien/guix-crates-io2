(define-module (crates-io he x_ hex_literals) #:use-module (crates-io))

(define-public crate-hex_literals-0.1.0 (c (n "hex_literals") (v "0.1.0") (h "1pmm9gvc6cv8ypbd4kk02nk84imv5fcmav2x1397n3awpx9qdxv2")))

(define-public crate-hex_literals-0.1.1 (c (n "hex_literals") (v "0.1.1") (h "0iqz2mln56zvr5zdzhhy95z5pmfqz5hszpz0kkrfnan5ywwjgw9n")))

(define-public crate-hex_literals-0.1.2 (c (n "hex_literals") (v "0.1.2") (h "1dri7dyrn51jb4bc6w41f1a3nh6xd0d6vqdsd8xak5gkdl9drp97")))

(define-public crate-hex_literals-0.1.3 (c (n "hex_literals") (v "0.1.3") (h "0w4g3h43z0vj9kmrsm74kp8cwwvcyvc1km7pkfxdkb38179137yy")))

(define-public crate-hex_literals-0.1.4 (c (n "hex_literals") (v "0.1.4") (h "1p8b6h2a1hk2jli7nlg9zgjcrn4l6i7y7r7pzas1bzp8cka2gmmx")))

(define-public crate-hex_literals-0.1.5 (c (n "hex_literals") (v "0.1.5") (h "0d6h6rj1gaf9f0bb79s5ipm9ims6zx8cf5kv9q2rl7rzvidik9lr")))

(define-public crate-hex_literals-0.1.6 (c (n "hex_literals") (v "0.1.6") (h "16m550478sj7k3c26510jgnglcmsdfivk6lsrk9nvgbf98yxwa79")))

