(define-module (crates-io he lp helper_fn) #:use-module (crates-io))

(define-public crate-helper_fn-0.1.0 (c (n "helper_fn") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0qziglsm7wi0bcqnhv6sf4dgiy5a4cs928p713j6lp3q34n2hfxy")))

