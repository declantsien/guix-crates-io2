(define-module (crates-io he lp helper) #:use-module (crates-io))

(define-public crate-helper-1.0.0 (c (n "helper") (v "1.0.0") (h "02r3plk0zzzrc141c0dgir619c7fzpc7p4r5b9iz0v24q12xy9wk")))

(define-public crate-helper-2.0.0 (c (n "helper") (v "2.0.0") (h "0jmfh1vwvijpwxlypfqb4y7ry5rzjapbp0nm85bxfipls3k3z5ja")))

(define-public crate-helper-2.1.0-alpha.1 (c (n "helper") (v "2.1.0-alpha.1") (h "1f5v2isv5a4s8fn0s7dvjyf7k4xbq8r1yvzpqmwb159q4gxvxp8g")))

(define-public crate-helper-3.0.0 (c (n "helper") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "0rkz7fms0jjg3myhcdxjpq6spsn9l786nd0g3bsav4iwq40xiv0a") (r "1.56")))

(define-public crate-helper-3.1.0 (c (n "helper") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "0h64mac9ddirdlsvq5lck70xj0gryc4sp00i9nxqd2wyxwm6q5ip") (r "1.58")))

(define-public crate-helper-3.2.0 (c (n "helper") (v "3.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "0q2d5fs6m3x168h7ygm0idnfrlynjc62b8x21dzkqcg1cmj60qx6") (r "1.58")))

(define-public crate-helper-3.3.0 (c (n "helper") (v "3.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "0aybw4s88h2fsdjyx0iyp3b5y7zjdig9lha2x3s0w8iabk8zwx3h") (r "1.58")))

