(define-module (crates-io he lp help_crafter) #:use-module (crates-io))

(define-public crate-help_crafter-0.1.0 (c (n "help_crafter") (v "0.1.0") (h "04cvxd4c9nmysfip7pbgk0n68m4wf92jhj9vlvijd6fh5cmiygvd") (y #t)))

(define-public crate-help_crafter-0.1.1 (c (n "help_crafter") (v "0.1.1") (h "19ww02pvyi4x6pysdda9rf19ymr2grnsv5z3bqap7shkxmvhfsn4") (y #t)))

(define-public crate-help_crafter-0.2.0 (c (n "help_crafter") (v "0.2.0") (h "0vqphpw87ffhh1s8lc0xwk799mrijc1hyn7jdk24ir9vrsn2sfyp")))

(define-public crate-help_crafter-0.3.0 (c (n "help_crafter") (v "0.3.0") (h "1h19h5drba4n2k0kc001cs0li25dp0qzc8axmh9ik6k5768rzkhs") (y #t)))

(define-public crate-help_crafter-0.3.1 (c (n "help_crafter") (v "0.3.1") (h "0m8pyjn1sf8qy60aiwyr9azdsvq1mmgn4jm9id6pmjskx1wp349p")))

