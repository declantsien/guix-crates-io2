(define-module (crates-io he lp helpers) #:use-module (crates-io))

(define-public crate-helpers-0.1.0 (c (n "helpers") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kx7mciizavmq77ppb631756fl2qqwg2mdgzp4kyyyxiicdgaf1l")))

(define-public crate-helpers-0.1.1 (c (n "helpers") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0g8ad5x1mysqdjillxrwbb6mr4ak1i4q9n4lr63jwz66amwcgysg")))

(define-public crate-helpers-0.1.2 (c (n "helpers") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "118q06mm70hirndirlyys6g8qp091cqaizm966js8m48bm2dgxgp")))

(define-public crate-helpers-0.1.3 (c (n "helpers") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m6alda6kxvl39shifwjdbc7dj9p62z7fkc4rqrc5ghw1xx8ym4k")))

(define-public crate-helpers-0.1.4 (c (n "helpers") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rhdv78ymw0cpicylgbjryzr6nh4pz5v5lld14v56gf5njqvy52x") (f (quote (("rand") ("jwt") ("default"))))))

