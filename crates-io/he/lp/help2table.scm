(define-module (crates-io he lp help2table) #:use-module (crates-io))

(define-public crate-help2table-0.1.0 (c (n "help2table") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "terminal-supports-emoji") (r "^0.1.3") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hd19r4b962bln3cy9xipdd5bg8pixnx1x209swk2i936shvfb1f")))

