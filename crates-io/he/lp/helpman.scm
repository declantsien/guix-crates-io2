(define-module (crates-io he lp helpman) #:use-module (crates-io))

(define-public crate-helpman-1.0.0 (c (n "helpman") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cross") (r "^0.2.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "termimad") (r "^0.20.6") (d #t) (k 0)))) (h "19zj09y7g0dc5gsq1z6wkgjpn5kmqwas9xrfrxrzqx855icaw75h")))

(define-public crate-helpman-1.0.1 (c (n "helpman") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cross") (r "^0.2.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "termimad") (r "^0.20.6") (d #t) (k 0)))) (h "0wk79rzybpl5cwlx0sq3lgx9gakps4qhlvsvlrdq5xf9cd9izp7l")))

(define-public crate-helpman-1.0.2 (c (n "helpman") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cross") (r "^0.2.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "termimad") (r "^0.20.6") (d #t) (k 0)))) (h "1r8h2533mshyfycp5xbmlkcx5hf4w7zzc8v98bnwjpspgabmx12c")))

(define-public crate-helpman-1.0.3 (c (n "helpman") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cross") (r "^0.2.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "termimad") (r "^0.20.6") (d #t) (k 0)))) (h "1qa550bqb73hj1vc88ws1h8c4dzn93y2f9glgbzpbzhqgjpig3fd")))

(define-public crate-helpman-1.0.4 (c (n "helpman") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "termimad") (r "^0.20.6") (d #t) (k 0)))) (h "1d4hyhawd8anav323ckrlh38i9fg5rqc24k9diihxnd0jb73wsaq")))

