(define-module (crates-io he ct hectorl_logger) #:use-module (crates-io))

(define-public crate-hectorl_logger-0.1.0 (c (n "hectorl_logger") (v "0.1.0") (h "1826g0f4bg6lk2iwjdpr93l4ipy58i2nyh8m8brbbb1m73xgqhi7")))

(define-public crate-hectorl_logger-0.1.1 (c (n "hectorl_logger") (v "0.1.1") (h "1jfl6ar8nxnmm3iga1qq2g3c7rg7krb0hndgjqya6bkjd7b9qfi2")))

