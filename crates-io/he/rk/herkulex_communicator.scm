(define-module (crates-io he rk herkulex_communicator) #:use-module (crates-io))

(define-public crate-herkulex_communicator-0.2.1 (c (n "herkulex_communicator") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "cursive") (r "^0.9.1") (f (quote ("termion-backend"))) (k 0)) (d (n "drs-0x01") (r "^0.1.7") (d #t) (k 0)) (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "05znv9b36ix6p19cmy0qj95ljayv0i95s05g0ayfrvda3lkc2yjv")))

(define-public crate-herkulex_communicator-0.2.2 (c (n "herkulex_communicator") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "cursive") (r "^0.9.1") (f (quote ("termion-backend"))) (k 0)) (d (n "drs-0x01") (r "^0.1.7") (d #t) (k 0)) (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "146grb67qxzradk9x0h237ibni2h02bs6261vsapnv6b0d85sfjr")))

(define-public crate-herkulex_communicator-0.2.3 (c (n "herkulex_communicator") (v "0.2.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "cursive") (r "^0.9.1") (f (quote ("termion-backend"))) (k 0)) (d (n "drs-0x01") (r "^0.1.7") (d #t) (k 0)) (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "0avzwb0jlrhczxj2gcdkzyb1ngq81xyk4yjpxs24hz00rcfaf27r")))

(define-public crate-herkulex_communicator-0.2.4 (c (n "herkulex_communicator") (v "0.2.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "cursive") (r "^0.9.1") (f (quote ("termion-backend"))) (k 0)) (d (n "drs-0x01") (r "^0.1.7") (d #t) (k 0)) (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "19gvx18qgixfb5g8cc0km7gl1as48g8vhjsvga6vfahxkxmz54n0")))

(define-public crate-herkulex_communicator-0.2.5 (c (n "herkulex_communicator") (v "0.2.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "cursive") (r "^0.9.1") (f (quote ("termion-backend"))) (k 0)) (d (n "drs-0x01") (r "^0.1.7") (d #t) (k 0)) (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "1zl092lxp7qn55lflpjk0j1z9v43vxf1l14psn1g796q88760wb2")))

(define-public crate-herkulex_communicator-0.2.6 (c (n "herkulex_communicator") (v "0.2.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "cursive") (r "^0.14") (f (quote ("termion-backend"))) (k 0)) (d (n "drs-0x01") (r "^0.3.0") (d #t) (k 0)) (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "1vzzah3vw0nd7ahryc62krf58h4cx8p6i25fzjmh1mrjdh6pf9am")))

(define-public crate-herkulex_communicator-0.2.7 (c (n "herkulex_communicator") (v "0.2.7") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "cursive") (r "^0.16") (f (quote ("termion-backend"))) (k 0)) (d (n "drs-0x01") (r "^0.3.0") (d #t) (k 0)) (d (n "serialport") (r "^3.0.0") (d #t) (k 0)))) (h "0nb3g6bwi4bmrmakyia7ivrppc9kypr14cqckj3fvv3pgdnpzl61")))

