(define-module (crates-io he ek heekkr) #:use-module (crates-io))

(define-public crate-heekkr-0.0.0 (c (n "heekkr") (v "0.0.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "1wcvr6qqzpc84vv5fn7xgv1rrr9dn62acj61x2zwjz8rvp31cffc")))

(define-public crate-heekkr-1.3.1 (c (n "heekkr") (v "1.3.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "18akxbkwp30amba019v9fkjihg2g01v88skq7li8w83fr7dw4shy")))

