(define-module (crates-io he xm hexmap) #:use-module (crates-io))

(define-public crate-hexmap-0.1.0 (c (n "hexmap") (v "0.1.0") (h "1x69ykkrqq77nr55g65dzfcnk33ls5fnwf7arvla840qppy9r4s5")))

(define-public crate-hexmap-0.2.0 (c (n "hexmap") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)))) (h "1ixdpj9vkqqwq9fh21zv5r275k93944vypvr3vbk8a53i3qly56g")))

(define-public crate-hexmap-0.2.1 (c (n "hexmap") (v "0.2.1") (h "14a0q5qxw91si0a5bdgcynzrmj2j7sx6gk6n0bwblnsp3wr5pl9g")))

