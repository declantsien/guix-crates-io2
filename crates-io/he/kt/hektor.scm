(define-module (crates-io he kt hektor) #:use-module (crates-io))

(define-public crate-hektor-0.0.1 (c (n "hektor") (v "0.0.1") (d (list (d (n "nalgebra-glm") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)))) (h "03522sx21r43mmlq9j0awlzm1wxr2qmsmfwx3aknkfs6z87q41rv") (f (quote (("serde1" "serde" "serde_derive") ("default") ("debug_force_no_ssse3") ("debug_force_no_sse42") ("debug_force_no_sse41") ("debug_force_no_sse3") ("debug_force_no_sse2"))))))

(define-public crate-hektor-0.0.2 (c (n "hektor") (v "0.0.2") (d (list (d (n "nalgebra-glm") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)))) (h "0zk018379dl1r1gzcvpyzl6wzgpz5j7midnzgr5qfhjlrawxf2lb") (f (quote (("serde1" "serde" "serde_derive") ("default") ("debug_force_no_ssse3") ("debug_force_no_sse42") ("debug_force_no_sse41") ("debug_force_no_sse3") ("debug_force_no_sse2"))))))

(define-public crate-hektor-0.0.3 (c (n "hektor") (v "0.0.3") (d (list (d (n "getrandom") (r "^0.1") (d #t) (k 2)) (d (n "lokacore") (r "^0.1.2") (d #t) (k 0)) (d (n "randomize") (r "^3.0.0-rc.3") (d #t) (k 2)))) (h "1rcyvy0a70iln9r0jifvf8gvk8k8vhyyqbijyxklf1rh4kncbpi1") (f (quote (("free_functions") ("default" "free_functions"))))))

(define-public crate-hektor-0.1.0 (c (n "hektor") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.1") (d #t) (k 2)) (d (n "lokacore") (r "^0.1.4") (k 0)) (d (n "mint") (r "^0.5") (o #t) (k 0)) (d (n "randomize") (r "^3.0.0-rc.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0jp78b7xvwjh77mszandil7mqqlf38cv07cdy4agpf0gkznan7jr") (f (quote (("free_functions") ("default" "free_functions"))))))

(define-public crate-hektor-0.2.0 (c (n "hektor") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.1") (d #t) (k 2)) (d (n "lokacore") (r "^0.1.5") (k 0)) (d (n "mint") (r "^0.5") (o #t) (k 0)) (d (n "randomize") (r "^3.0.0-rc.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "12vzbra4syq8591nld7xph7920j57ndwmkzmlz82m6rk2r15q5g4") (f (quote (("free_functions") ("default" "free_functions"))))))

(define-public crate-hektor-0.2.1 (c (n "hektor") (v "0.2.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.1") (d #t) (k 2)) (d (n "lokacore") (r "^0.1.5") (k 0)) (d (n "mint") (r "^0.5") (o #t) (k 0)) (d (n "randomize") (r "^3.0.0-rc.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "03x7f7agmagkbzi5xmd27g07ipqw02kw6vis4h2zkj288zzjbnfw") (f (quote (("free_functions") ("default" "free_functions"))))))

(define-public crate-hektor-0.2.2 (c (n "hektor") (v "0.2.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "getrandom") (r "^0.1") (d #t) (k 2)) (d (n "lokacore") (r "^0.1.5") (k 0)) (d (n "mint") (r "^0.5") (o #t) (k 0)) (d (n "randomize") (r "^3.0.0-rc.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1m4rwz7lccfxyy3rd0cyrzk1snzhzp26bxm3pqw8cbhn31dmghm5") (f (quote (("free_functions") ("default" "free_functions"))))))

