(define-module (crates-io he pm hepmc2) #:use-module (crates-io))

(define-public crate-hepmc2-0.1.0 (c (n "hepmc2") (v "0.1.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08c1niprj333i63wrsms7sc852n6ynrpy6k7lygq8hy8aw0rwwv4")))

(define-public crate-hepmc2-0.2.0 (c (n "hepmc2") (v "0.2.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0071hldn6xaclaaz7hx8jw9sidvyxn056hkvis53b482rch8b9l3")))

(define-public crate-hepmc2-0.2.1 (c (n "hepmc2") (v "0.2.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fkff68ncdkdpgy37jb0276mrbdr07sps83hsskxvafm1hnbgynk")))

(define-public crate-hepmc2-0.2.2 (c (n "hepmc2") (v "0.2.2") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07zkz6ddj75gk7z78rvblglschwij908yy1l2j8i9dg5jrxn38gs")))

(define-public crate-hepmc2-0.3.0 (c (n "hepmc2") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "159fi43qzhnfy8mbc5xqivars86jl6wfy8aw4fs1cfqaywx7rg2j")))

(define-public crate-hepmc2-0.4.0 (c (n "hepmc2") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "127zpa857x8x6kqh8pvkrinszb3psrlfbjzh4385qa0904nmv1z3")))

(define-public crate-hepmc2-0.4.1 (c (n "hepmc2") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fylm1gxfcvjlq8bdka439p51r828vxyggsbh4qliklnr32klsm8")))

(define-public crate-hepmc2-0.5.0 (c (n "hepmc2") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qmbj0vwwhdmzpvjmgcgwyrari9x6zqi2p3xa32qnxn2h8gxr3ar")))

(define-public crate-hepmc2-0.5.1 (c (n "hepmc2") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.22") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hp1ry8s6wbadcx0dyc137pxiy0rsxvcg6vjxn3xxml0js35iq8m")))

(define-public crate-hepmc2-0.6.0 (c (n "hepmc2") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hk029i1ldh0aww8xz7khj9ghfz99nlc4qms0fs2865cj4fdzp45")))

(define-public crate-hepmc2-0.7.0 (c (n "hepmc2") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hepmc2-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "maybe-async") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("io-util" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros" "fs"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "0phwz2rkzv19yc19r9rl829530h333fc1223z3w537ly3pw1rk6h") (f (quote (("sync" "hepmc2-macros/sync" "maybe-async/is_sync") ("default" "sync")))) (s 2) (e (quote (("tokio" "hepmc2-macros/tokio" "dep:tokio"))))))

