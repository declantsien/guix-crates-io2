(define-module (crates-io he pm hepmc2-macros) #:use-module (crates-io))

(define-public crate-hepmc2-macros-0.1.0 (c (n "hepmc2-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16xv0nkkbg44kx8wcvv9hw105nxxlr97v7px1n2sblp6vx5rx6pl") (f (quote (("tokio") ("sync"))))))

