(define-module (crates-io he ll hello-world-1) #:use-module (crates-io))

(define-public crate-hello-world-1-0.1.0 (c (n "hello-world-1") (v "0.1.0") (h "0bwaln73wvlak67jvmyiki850bcic47zl9h8zdlsbvdx9sljqxjs")))

(define-public crate-hello-world-1-0.1.1 (c (n "hello-world-1") (v "0.1.1") (h "14z54mjjw7b68arv9zj45mmidfizdw4kncjnf122flfny3m0s5g5")))

(define-public crate-hello-world-1-0.1.2 (c (n "hello-world-1") (v "0.1.2") (h "0il1bdc0p47dzayiz8m7ikx6i9p1xh6wmvpq3fnx5xp28p89i8nm")))

