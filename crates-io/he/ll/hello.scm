(define-module (crates-io he ll hello) #:use-module (crates-io))

(define-public crate-hello-1.0.0 (c (n "hello") (v "1.0.0") (h "1cbck1m9wc6f64iwh5cnz178m7n2scxxkvfpjh5ykpcqbr8csvr5")))

(define-public crate-hello-1.0.1 (c (n "hello") (v "1.0.1") (h "1skacwlycwn30r23w67dxalx7s8y5w087aqgnmlpsm1vjjgz7n32")))

(define-public crate-hello-1.0.2 (c (n "hello") (v "1.0.2") (h "1z7lvzx1l8fsyc3z0akghxad82yp65qs39d5rkpxkfqf65ng351b")))

(define-public crate-hello-1.0.3 (c (n "hello") (v "1.0.3") (h "1f0ynk0ahhxfydix0vyl10a3icyk5v40dnwhs2p03n7q67m81cag")))

(define-public crate-hello-1.0.4 (c (n "hello") (v "1.0.4") (h "10vay1ia77pclip60ajmnrljx7ab3namyihf927l0gsq7wswsxz6")))

