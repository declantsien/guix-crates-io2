(define-module (crates-io he ll hellofluent) #:use-module (crates-io))

(define-public crate-hellofluent-0.1.1 (c (n "hellofluent") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "0g7y6i439fbybdj9mnzkq7wjwcgm48c13723zid040cw312l7f0z")))

(define-public crate-hellofluent-0.1.2 (c (n "hellofluent") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "0i79lxvv7s67b35wkclxmikqddgrf8qmaks272kqs8l02zklf0p7")))

(define-public crate-hellofluent-0.1.3 (c (n "hellofluent") (v "0.1.3") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "0ir39i8giwkdlzrw9hf12dch1hfxkqaqzkq12y87bd3w6nyf8k6k")))

(define-public crate-hellofluent-0.1.4 (c (n "hellofluent") (v "0.1.4") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "1v5z2avnlh75ln6jqml3szc8dhi8nw97q03jwcjkfhv5afj3in35")))

