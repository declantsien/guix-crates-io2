(define-module (crates-io he ll helldive_rs) #:use-module (crates-io))

(define-public crate-helldive_rs-0.3.1 (c (n "helldive_rs") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1lpw340qripfa82k8ryvdps3lj3pgvmn3mlb77jc10p5gbasrivq")))

(define-public crate-helldive_rs-0.4.0 (c (n "helldive_rs") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1d802j73pn1qfjnbxdyd8bfdnr2372q80vvhn5z1y1c0swg749q6")))

(define-public crate-helldive_rs-0.5.0 (c (n "helldive_rs") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "01iilff2ybrl0i6c7plffijwqvc8xmrpf54qh13jrs9p6raw0s47")))

(define-public crate-helldive_rs-0.6.0 (c (n "helldive_rs") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1kiz1iirxf6b6chnwmgj8njk9wb2lyyj9kxzlnvwaa4wwz22khy6")))

