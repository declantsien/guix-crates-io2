(define-module (crates-io he ll hello_world_0228) #:use-module (crates-io))

(define-public crate-hello_world_0228-0.1.0 (c (n "hello_world_0228") (v "0.1.0") (h "1bvm0x7yvka5w46vxpnbs7ywcnxlabr0qpf0pdq0qrbp32xkb6vj")))

(define-public crate-hello_world_0228-0.1.1 (c (n "hello_world_0228") (v "0.1.1") (h "1967fgws375qci6062l08sl1paxqij10c5x3zlp61lsrbkjcizqg")))

(define-public crate-hello_world_0228-0.1.2 (c (n "hello_world_0228") (v "0.1.2") (h "167z6dizgin3ca61rf6xw0g1sf7l2im0jws0c3v98k70p6s3a22a")))

