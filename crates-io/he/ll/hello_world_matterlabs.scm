(define-module (crates-io he ll hello_world_matterlabs) #:use-module (crates-io))

(define-public crate-hello_world_matterlabs-0.1.0 (c (n "hello_world_matterlabs") (v "0.1.0") (h "0vv5pbdzx80h0vplcbvzhvyjja0fsvkcaskagdm7c1a00da10mxp")))

(define-public crate-hello_world_matterlabs-0.1.1 (c (n "hello_world_matterlabs") (v "0.1.1") (h "1rnj5c3gcnj1yvqfkbcrj0va2cwxy83vb601lkrgxyrq7izadn8d")))

