(define-module (crates-io he ll hello_julianshu) #:use-module (crates-io))

(define-public crate-hello_julianshu-0.1.0 (c (n "hello_julianshu") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "08x2k0pbr2pc7phpw0qkf8pvgdf1dqiyz050hah5xf6bk15sa4jx")))

(define-public crate-hello_julianshu-0.1.1 (c (n "hello_julianshu") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1cqxjjda7fnpq23p3dr8x5ak4412hs6dchdqmm0nl0hxp90xf4b1")))

(define-public crate-hello_julianshu-0.1.2 (c (n "hello_julianshu") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1f1pml8317p5sjidr7xl02h7hjh5mmh5mjkmdfllp732jlb22r0p")))

