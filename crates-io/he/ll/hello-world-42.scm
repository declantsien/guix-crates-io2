(define-module (crates-io he ll hello-world-42) #:use-module (crates-io))

(define-public crate-hello-world-42-0.1.0 (c (n "hello-world-42") (v "0.1.0") (h "1izg6qp3yya3484q0k327alxbsn33mqh5ms6asw66p7mw035cskh")))

(define-public crate-hello-world-42-0.1.1 (c (n "hello-world-42") (v "0.1.1") (h "14ql092mpbyzvvw0n52j5ygnkxz8d6ify9318gvfhr3byzgkdmmn")))

