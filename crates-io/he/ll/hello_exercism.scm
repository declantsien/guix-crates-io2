(define-module (crates-io he ll hello_exercism) #:use-module (crates-io))

(define-public crate-hello_exercism-0.1.0 (c (n "hello_exercism") (v "0.1.0") (h "17dp7glqcyf9vppjhy7l9qm0j91cfiy893giajs6rlwx0kqw1fqv")))

(define-public crate-hello_exercism-0.1.2 (c (n "hello_exercism") (v "0.1.2") (h "0bi06dwyin9qgic0vdiy6nx1ahafb6lpxxnbkb834873q2fwf058")))

(define-public crate-hello_exercism-0.1.3 (c (n "hello_exercism") (v "0.1.3") (h "0x9zf4nlqkc6nn93jxsv8fk56rcviyyqfkx9wvhcd1dm0fk9w10x")))

(define-public crate-hello_exercism-0.1.4 (c (n "hello_exercism") (v "0.1.4") (h "11d2izpzjd4cd0dapjwr2h4sq714znwclw4m6zv6zq2znwd1a4nl")))

(define-public crate-hello_exercism-0.1.5 (c (n "hello_exercism") (v "0.1.5") (h "04c2r9qxms1a29cigxabaa5wvmsxybkzh0rsp0c8d7s0jax8472w")))

(define-public crate-hello_exercism-0.1.6 (c (n "hello_exercism") (v "0.1.6") (h "0sjswsx3xr1xwakpcnfnlbs5wsgmsi2f35vfb91fadk2ls6fvb5g")))

(define-public crate-hello_exercism-0.1.7 (c (n "hello_exercism") (v "0.1.7") (h "15rrnjsp1d783k680kwba0ysw25fp1lgcig2gj8d28l95h8mj766")))

(define-public crate-hello_exercism-0.1.8 (c (n "hello_exercism") (v "0.1.8") (h "0ixn6462c8fm0qgr3kbpda2pm7fl4mx2kn1bhxwzd7jzcb9mla2w")))

(define-public crate-hello_exercism-0.1.9 (c (n "hello_exercism") (v "0.1.9") (h "03k2igmfdjxvvxlhvqcqfnm1kyf0wc0d8k84asclsk26jc639idz")))

(define-public crate-hello_exercism-0.2.0 (c (n "hello_exercism") (v "0.2.0") (h "0qxgscp94h2526zy51zvwwfbr0lcr8dlazggf58aba85jaya1ykb")))

(define-public crate-hello_exercism-0.2.1 (c (n "hello_exercism") (v "0.2.1") (h "058c8slvznk7kx577b9ga5ar5f2zha493lh3qkc891195r8b8hw9")))

(define-public crate-hello_exercism-0.2.2 (c (n "hello_exercism") (v "0.2.2") (h "1261j7kvc430q18lbkjvsmh1s4myk2y8midqv226g7r05425mz8h")))

(define-public crate-hello_exercism-0.2.3 (c (n "hello_exercism") (v "0.2.3") (h "0m7iqj4c6kjg9dsz54b4q7ics25jxggwj1kn55hynlyk1qjmld40")))

(define-public crate-hello_exercism-0.2.4 (c (n "hello_exercism") (v "0.2.4") (h "0pb1fi9fn8qc83ndyjmqxnn98fkcvvc71i8ldz16z7mnssv3w4r0")))

(define-public crate-hello_exercism-0.2.5 (c (n "hello_exercism") (v "0.2.5") (h "0zrdi4bj5skkyd0lhb1z1k9fqfq7hpllf68ij8x284mxixwwarpd")))

(define-public crate-hello_exercism-0.2.6 (c (n "hello_exercism") (v "0.2.6") (h "10pm0szlmci4z54d0ff55c28whrx1hh734932ig2s5abi73gxmw1")))

(define-public crate-hello_exercism-0.2.7 (c (n "hello_exercism") (v "0.2.7") (h "0rqqk5h0jj17z9xd8kvdm0gmqqgi8xymkwa3jgq98ldjm9p8xgpr")))

(define-public crate-hello_exercism-0.2.8 (c (n "hello_exercism") (v "0.2.8") (h "0s7gcznk11f8j9rcxcy3hdryix88wgcfbvk86s3djmy7pwxvb9rh")))

(define-public crate-hello_exercism-0.2.9 (c (n "hello_exercism") (v "0.2.9") (h "0i6r6k520rqrd4hpgjcj3x7dqq4hf6sgml959nzxgpsqyfihnhmd")))

(define-public crate-hello_exercism-0.3.0 (c (n "hello_exercism") (v "0.3.0") (h "1mxpjyikqa1l3xnl4rcw2cwgqsqph44mjpld6cljnyysc9ljzwmq")))

(define-public crate-hello_exercism-0.3.1 (c (n "hello_exercism") (v "0.3.1") (h "1x8j0l4z9mz48plimhs9n9697y29cg9gimam9p27s33b68rm09j7")))

(define-public crate-hello_exercism-0.3.2 (c (n "hello_exercism") (v "0.3.2") (h "1c5vwhha1fzw173y21nvzgmlpa3rmdv856f9ap4hwwp6p1c5pq5i")))

(define-public crate-hello_exercism-0.3.3 (c (n "hello_exercism") (v "0.3.3") (h "0nv5nxhlyhzlgzzww99p979b9b2bfk49ivwp97lyii8wpv3dwmmp")))

(define-public crate-hello_exercism-0.3.4 (c (n "hello_exercism") (v "0.3.4") (d (list (d (n "i_crate") (r "^0.3.2") (d #t) (k 0) (p "hello_exercism")))) (h "061gkrsw13irznghcdyxj0xg5nm48h00ifsjazkl2i6yibipx8b4")))

(define-public crate-hello_exercism-0.3.5 (c (n "hello_exercism") (v "0.3.5") (d (list (d (n "i_crate") (r "^0.3.3") (d #t) (k 0) (p "hello_exercism")))) (h "1l15cwz5nyj2lpx3ar4sj6y4gc0jjcfbmn55g037mvryf955k3rf")))

(define-public crate-hello_exercism-0.3.6 (c (n "hello_exercism") (v "0.3.6") (d (list (d (n "i_crate") (r "^0.3.3") (d #t) (k 0) (p "hello_exercism")))) (h "1lfsjcdfk1nnjjl0kicnccxqqw6ymnv3zvirhd7zyv6padbsdkhy")))

(define-public crate-hello_exercism-0.3.7 (c (n "hello_exercism") (v "0.3.7") (h "1la7fwxnd9q07i0g503biargf1y9x4r6sw56ddmli1li54yli671")))

(define-public crate-hello_exercism-0.3.8 (c (n "hello_exercism") (v "0.3.8") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "0fajnm2iw500dw7hj33vhq5wb965xw05digx7706nbmy7ml5nhdf")))

(define-public crate-hello_exercism-0.4.0 (c (n "hello_exercism") (v "0.4.0") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "0k583m7nyhpfp4psvk8gc1xhkpvmr4b0sy444d7g1m1rbinf6caz")))

(define-public crate-hello_exercism-0.4.1 (c (n "hello_exercism") (v "0.4.1") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "0znlzkqjk6zjdbkgr8h2nszpffry1xaw1y2alf32knsdba8r98z8")))

(define-public crate-hello_exercism-0.4.2 (c (n "hello_exercism") (v "0.4.2") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "0ap267bq9xw8aw20aqhnj9wv6acl76dy4dj2y9wxr2giq31fwh8a")))

(define-public crate-hello_exercism-0.4.3 (c (n "hello_exercism") (v "0.4.3") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "14z0wcg6mj1lss6cmq3lrq3575iva6mcxasdbjnp8wvzkamlym1i")))

(define-public crate-hello_exercism-0.4.5 (c (n "hello_exercism") (v "0.4.5") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "0sjmmi4h2qv50r8r762cywgwyx2niimrb0a6pmh73wix176ygfkv")))

(define-public crate-hello_exercism-0.4.6 (c (n "hello_exercism") (v "0.4.6") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "01ay5s4wixnps6q21brcy5r1vdp28mj6v713a1yrp66d08sq83k5")))

(define-public crate-hello_exercism-0.4.7 (c (n "hello_exercism") (v "0.4.7") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "00rcklxsqprj2zmvj7bi2g2skv8ri76ksmj25bvkww993fddcmla")))

(define-public crate-hello_exercism-0.4.8 (c (n "hello_exercism") (v "0.4.8") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "09cj483lqa9461m8z5sx7bbxxfr48rwfjzr7a3i41pam99bl22pv")))

(define-public crate-hello_exercism-0.4.9 (c (n "hello_exercism") (v "0.4.9") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "1vc52xc6ijl138d4sz2xw0c61jzw872v7f9dzzbs4s2zqcii5wap")))

(define-public crate-hello_exercism-0.4.10 (c (n "hello_exercism") (v "0.4.10") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "09rwzgs6p6ldjkbgbfhf7cdjc99g7ncff1blbrrfqwlldnc1yjax")))

(define-public crate-hello_exercism-0.5.0 (c (n "hello_exercism") (v "0.5.0") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "0c611b96mbq2j1661k5m4fdldszz6fd4377wb86b0r4pnqc9kznk")))

(define-public crate-hello_exercism-0.5.1 (c (n "hello_exercism") (v "0.5.1") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "11sfg27p1y8yq3xpqgqf1b0kh3pf481kwz7fc3xahqg5wbk7ziib")))

(define-public crate-hello_exercism-0.5.2 (c (n "hello_exercism") (v "0.5.2") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "19m8rplj6npxdfkqrnlfydikfr5r8bkizsqfjr5g717whqlz34js")))

(define-public crate-hello_exercism-0.5.3 (c (n "hello_exercism") (v "0.5.3") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "1dgchspm4swrrzhc15qywpc5ggqvabajwhn0bf16l70szi9nyky0")))

(define-public crate-hello_exercism-0.5.4 (c (n "hello_exercism") (v "0.5.4") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "1jj511xy4063gzlzjdrwazkh4pzxl41xd8gf8vffz8bf0afw4xg1")))

(define-public crate-hello_exercism-0.5.5 (c (n "hello_exercism") (v "0.5.5") (d (list (d (n "i_crate") (r "^0.1.1") (d #t) (k 0) (p "hello_extern")))) (h "0s8mv918ys74x9x2adfzr7sadi08wcdg4y7piyi3ndalnlhs0p1p")))

