(define-module (crates-io he ll hello-virus) #:use-module (crates-io))

(define-public crate-hello-virus-0.1.0 (c (n "hello-virus") (v "0.1.0") (d (list (d (n "pyu_rust_util") (r "^0.1.71") (d #t) (k 0)))) (h "1z32nsvhn1n7j8c7h53yphs71iacbv2imcrhah79likik8i9psg6")))

(define-public crate-hello-virus-0.1.1 (c (n "hello-virus") (v "0.1.1") (d (list (d (n "pyu_rust_util") (r "^0.1.71") (d #t) (k 0)))) (h "128qamdszqk0p8p6z59qwrc4bwf34mgs6zxp7na4yw9dr4zmkqvv")))

