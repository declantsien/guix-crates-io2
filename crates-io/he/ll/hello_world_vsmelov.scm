(define-module (crates-io he ll hello_world_vsmelov) #:use-module (crates-io))

(define-public crate-hello_world_vsmelov-0.1.0 (c (n "hello_world_vsmelov") (v "0.1.0") (h "0w6snmpa9xmd1vf2m2wkb1k1c5n1nn7s7rkhx2af0vycaf9ir2rk")))

(define-public crate-hello_world_vsmelov-0.1.1 (c (n "hello_world_vsmelov") (v "0.1.1") (h "19mqx81cqmb2ghmj1ghsq1rns9jnxcdikzq88jmh5xbrzn3f4g2x")))

(define-public crate-hello_world_vsmelov-0.2.1 (c (n "hello_world_vsmelov") (v "0.2.1") (h "05qmzjn0njg88r12w9cyy0vq22yc6bncma7vhk0cx9nkhac22z91")))

(define-public crate-hello_world_vsmelov-0.2.2 (c (n "hello_world_vsmelov") (v "0.2.2") (h "1dvcbdff33j1xa5yx0prbhxn8lmcm3ynycjymq8ncf86l0j3lvkn")))

(define-public crate-hello_world_vsmelov-0.3.0 (c (n "hello_world_vsmelov") (v "0.3.0") (h "0y2a9nmf8iar6w9anm4wnmk8hynzjv3ms1bhz33704fhkqbbxbrf")))

