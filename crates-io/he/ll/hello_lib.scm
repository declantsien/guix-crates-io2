(define-module (crates-io he ll hello_lib) #:use-module (crates-io))

(define-public crate-hello_lib-0.1.0 (c (n "hello_lib") (v "0.1.0") (h "1b8r82bxbf410rmaxm008bcjjkj8sndf9401m3q7xrcjkhvdnk7x")))

(define-public crate-hello_lib-0.1.1 (c (n "hello_lib") (v "0.1.1") (h "1yx2j9zc06h29lng3w1sja5zfr53isr2i4icnfjjkg06wk5am90j")))

(define-public crate-hello_lib-0.1.2 (c (n "hello_lib") (v "0.1.2") (h "1qb6nvf7in5zzzc4iinyfykdmisl0v90x4v5acvxvwzfla5jpd5p")))

(define-public crate-hello_lib-0.1.3 (c (n "hello_lib") (v "0.1.3") (h "0nqwxnw9nh1cs4d9ampdry71zfdm1736gjd0n1b61v21id71kqyn")))

(define-public crate-hello_lib-0.1.4 (c (n "hello_lib") (v "0.1.4") (h "0191zly0wwk05f091jkn8j5ra2zvvdla14rk5y9szzm7jldbfsl1")))

(define-public crate-hello_lib-0.1.5 (c (n "hello_lib") (v "0.1.5") (h "02lw33m1bibpxcsxdwi78n7kwhy2k95a9zx88p73d8lnnpd7grm6")))

(define-public crate-hello_lib-0.1.6 (c (n "hello_lib") (v "0.1.6") (h "1xmcxb6f4vg24f01860kzbndgm8zifgcx03d2l9df05cm9mwr2hs")))

