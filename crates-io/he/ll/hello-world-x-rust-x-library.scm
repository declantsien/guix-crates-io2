(define-module (crates-io he ll hello-world-x-rust-x-library) #:use-module (crates-io))

(define-public crate-hello-world-x-rust-x-library-0.1.0 (c (n "hello-world-x-rust-x-library") (v "0.1.0") (h "1zn1bvdyrzmigfqic9gppxn9h3avzf527f5r6d5q22dmsrxjnqw9") (y #t)))

(define-public crate-hello-world-x-rust-x-library-0.1.1 (c (n "hello-world-x-rust-x-library") (v "0.1.1") (h "18zs9rfcm28s0z1px12pc3h6grla5zjhny4169kyfg8y6d1xg29j") (y #t)))

(define-public crate-hello-world-x-rust-x-library-0.1.2 (c (n "hello-world-x-rust-x-library") (v "0.1.2") (h "1960690swnf3yb3qww0yj5p480jkgnd32wn2jd39l967mc2bgvqx") (y #t)))

