(define-module (crates-io he ll hello_extern) #:use-module (crates-io))

(define-public crate-hello_extern-0.1.0 (c (n "hello_extern") (v "0.1.0") (h "0234g2vajsrj8fxlxw1w3ckhjjkb00b31631dhfxz4340spbwk1x")))

(define-public crate-hello_extern-0.1.1 (c (n "hello_extern") (v "0.1.1") (h "1nxbiq7c1a108x4l9rv6ib0y9z1abn6aa3yd04nfzbr23gjp4g1f")))

