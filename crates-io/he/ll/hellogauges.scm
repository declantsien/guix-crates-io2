(define-module (crates-io he ll hellogauges) #:use-module (crates-io))

(define-public crate-hellogauges-0.1.0 (c (n "hellogauges") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "02bk6a7km5wbmrvh4f5f0h8q0569kbmw2bn34v4caa72a0rc6mvw")))

