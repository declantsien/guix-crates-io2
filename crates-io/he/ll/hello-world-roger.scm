(define-module (crates-io he ll hello-world-roger) #:use-module (crates-io))

(define-public crate-hello-world-roger-0.1.0 (c (n "hello-world-roger") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.13") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^0.13") (d #t) (k 0)) (d (n "cw2") (r "^0.13") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zcxx80r8a0q89nrzg5d73jrlv0b97c3pcs5skqrx8krha3z7nv1") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

