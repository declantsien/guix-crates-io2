(define-module (crates-io he ll hello-crates-grrs) #:use-module (crates-io))

(define-public crate-hello-crates-grrs-0.1.0 (c (n "hello-crates-grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "0cficbqg10z3g750j39b67va2fbw3dy02zyz0i1n7h9f4g2ww675")))

