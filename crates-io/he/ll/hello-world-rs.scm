(define-module (crates-io he ll hello-world-rs) #:use-module (crates-io))

(define-public crate-hello-world-rs-0.1.0 (c (n "hello-world-rs") (v "0.1.0") (h "1dh5hznlvf4gys48m9j2g2zb9fhw5gkk4cw1m4khc93l7wqkyl2r")))

(define-public crate-hello-world-rs-0.1.1 (c (n "hello-world-rs") (v "0.1.1") (h "1i92cx8nkq9xkmz0kwgx945yzrn9b14k9czsl6fl5c1fxxlmy0g4")))

(define-public crate-hello-world-rs-0.1.2 (c (n "hello-world-rs") (v "0.1.2") (h "0ryrvbcg12rlgm6zxfs4fh333aimlhnzh20x14z2k8l1jyn5wmr4")))

(define-public crate-hello-world-rs-0.1.3 (c (n "hello-world-rs") (v "0.1.3") (h "0b48l99w4z3ypshpv4yanvadllalf6cvp913jdjl3lq6c09bjxp3")))

(define-public crate-hello-world-rs-0.1.4 (c (n "hello-world-rs") (v "0.1.4") (h "119b3l4njaif6bnm0f36h0pbhwg9abdfpbwd7hivnygzagwl5828")))

(define-public crate-hello-world-rs-1.0.0 (c (n "hello-world-rs") (v "1.0.0") (h "1fiffkhpik79qdkpjlrxaz8xx2r7nlh067prbgc1liznssf9jmlm")))

