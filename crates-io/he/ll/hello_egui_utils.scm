(define-module (crates-io he ll hello_egui_utils) #:use-module (crates-io))

(define-public crate-hello_egui_utils-0.1.0 (c (n "hello_egui_utils") (v "0.1.0") (d (list (d (n "egui") (r ">=0.22") (k 0)))) (h "1c3wmr6l4z8dkji2ii7b76xhkpbgwspzqdnq9vp7jsz3hrqprirz")))

(define-public crate-hello_egui_utils-0.1.1 (c (n "hello_egui_utils") (v "0.1.1") (d (list (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "egui") (r "^0.25") (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0134ci55b7zswsl6kp2jbawig2a19y55yw3j4amxanrk8caai2ka") (y #t) (s 2) (e (quote (("tokio" "async" "dep:tokio") ("async" "dep:wasm-bindgen-futures"))))))

(define-public crate-hello_egui_utils-0.2.0 (c (n "hello_egui_utils") (v "0.2.0") (d (list (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "egui") (r "^0.25") (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0r53q7cf98llsyq4dxbyhi57krag9r5klixh8rik90pr718nfcbq") (s 2) (e (quote (("tokio" "async" "dep:tokio") ("async" "dep:wasm-bindgen-futures"))))))

(define-public crate-hello_egui_utils-0.3.0 (c (n "hello_egui_utils") (v "0.3.0") (d (list (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "egui") (r "^0.26.0") (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1qjbk12hk301wv0bwf0i010391kkvj64kibly1wfs2z4zbzy5bw2") (s 2) (e (quote (("tokio" "async" "dep:tokio") ("async" "dep:wasm-bindgen-futures"))))))

(define-public crate-hello_egui_utils-0.4.0 (c (n "hello_egui_utils") (v "0.4.0") (d (list (d (n "concat-idents") (r "^1") (d #t) (k 0)) (d (n "egui") (r "^0.27.0") (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1i5k7dly5z6l27n2wqzyvflm2w7x8qd0qwqafwqjl1jznbg616w5") (s 2) (e (quote (("tokio" "async" "dep:tokio") ("async" "dep:wasm-bindgen-futures"))))))

