(define-module (crates-io he ll hello-rs) #:use-module (crates-io))

(define-public crate-hello-rs-0.1.0 (c (n "hello-rs") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.11.0") (d #t) (k 0)) (d (n "egg-mode") (r "^0.12.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "fantoccini") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "rawr") (r "^0.1.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "slack_api") (r "^0.20.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 0)))) (h "0vq4cmglk0a9xrhkabjznzhhfxd3wcha1j1gxzv8vzpxnrwdaciz")))

