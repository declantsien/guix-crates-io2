(define-module (crates-io he ll hello-cli) #:use-module (crates-io))

(define-public crate-hello-cli-0.1.0 (c (n "hello-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)))) (h "155l2xdac4l96afi7sb5m4wb7kfyzgs97fyg20pjkyfmchbr8vpn")))

(define-public crate-hello-cli-0.1.1 (c (n "hello-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)))) (h "1x3ghnb2j562fpqb6a565mhlx6i07m5d5cadwqbc5g9d45bm41nc")))

(define-public crate-hello-cli-0.1.2 (c (n "hello-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)))) (h "17zazijkdplp6rgf5dgxbbrx6bg6x6a6xjyi9yrg0vf028qwajr0")))

(define-public crate-hello-cli-0.1.3 (c (n "hello-cli") (v "0.1.3") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)))) (h "1i5i20rycc75hv7kwmpjw5rjp8vxrl8zxq69fqvnjfbqdkwwxi7a")))

(define-public crate-hello-cli-0.2.0 (c (n "hello-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)))) (h "1d9r093vx6pdhb2bjxwdq74jz99dcl6lq7khnmfa0rcmx9vrqcyy")))

(define-public crate-hello-cli-0.2.1 (c (n "hello-cli") (v "0.2.1") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)))) (h "0npxbbcl4gks5ixhcgcnl519dzz12r7z4vl3dcmvypjmdarw4gw2")))

(define-public crate-hello-cli-0.2.2 (c (n "hello-cli") (v "0.2.2") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)))) (h "0qilj9b94ig7z160kazk41k2iy1hprdk5qwaym4fnf5x9fiksild")))

