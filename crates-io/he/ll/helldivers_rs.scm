(define-module (crates-io he ll helldivers_rs) #:use-module (crates-io))

(define-public crate-helldivers_rs-0.1.0 (c (n "helldivers_rs") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1l1w1m4ajpgkdxdvl62xj4jasg7xkgxfglq0sls74xxh81fxcbss") (y #t)))

(define-public crate-helldivers_rs-0.2.0 (c (n "helldivers_rs") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1vfh4472m0xwx1r0qhaq8qjb7lwb38yl4laf2bajjh7jzrp1rjfq") (y #t)))

(define-public crate-helldivers_rs-0.3.0 (c (n "helldivers_rs") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1rhfdn1iblcph1vpggb3w2by57cfnkin6dr277gw8my8z7h7hlcv") (y #t)))

(define-public crate-helldivers_rs-0.3.1 (c (n "helldivers_rs") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0iazn7xs1vianalx5g3z436ch3idkf7z72my1i66rmpby7igpmnc") (y #t)))

(define-public crate-helldivers_rs-0.3.2 (c (n "helldivers_rs") (v "0.3.2") (d (list (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0bgd7x9n5skwf85l8saigqba6xpc7xfqynivhn8aapv31gs5h5l0") (y #t)))

