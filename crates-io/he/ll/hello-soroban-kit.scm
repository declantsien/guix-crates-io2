(define-module (crates-io he ll hello-soroban-kit) #:use-module (crates-io))

(define-public crate-hello-soroban-kit-0.1.7 (c (n "hello-soroban-kit") (v "0.1.7") (d (list (d (n "soroban-kit") (r "^0.1.7") (f (quote ("commitment-scheme" "storage" "state-machine" "circuit-breaker"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "008m491kyrarq987axbh5xh0047iqgam4mfrazd4wa1mb0306wn6")))

(define-public crate-hello-soroban-kit-0.1.8 (c (n "hello-soroban-kit") (v "0.1.8") (d (list (d (n "soroban-kit") (r "^0.1.8") (f (quote ("commitment-scheme" "storage" "state-machine" "circuit-breaker" "oracle"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1an4yid1cd7cwglhvqfhd2v302sll7w1x347wml6w83mfjpmcl0l")))

(define-public crate-hello-soroban-kit-0.1.9 (c (n "hello-soroban-kit") (v "0.1.9") (d (list (d (n "soroban-kit") (r "^0.1.9") (f (quote ("commitment-scheme" "storage" "state-machine" "circuit-breaker" "oracle" "utils"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1r649iv4826cagcrywxxqrsv1vxxaqpgx6pahlwgc091c24pdrnw")))

(define-public crate-hello-soroban-kit-0.1.10 (c (n "hello-soroban-kit") (v "0.1.10") (d (list (d (n "soroban-kit") (r "^0.1.10") (f (quote ("commitment-scheme" "storage" "state-machine" "circuit-breaker" "oracle" "utils"))) (k 0)) (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.3.1") (f (quote ("testutils"))) (d #t) (k 2)))) (h "112hdg6zgb4z42xiqphv7xi4pk7n3n8is7k8k154lr30mgf26hkb")))

(define-public crate-hello-soroban-kit-0.1.11 (c (n "hello-soroban-kit") (v "0.1.11") (d (list (d (n "soroban-kit") (r "^0.1.11") (f (quote ("commitment-scheme" "storage" "state-machine" "circuit-breaker" "oracle" "utils"))) (k 0)) (d (n "soroban-sdk") (r "^20.3.2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.3.2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1ddzy0cj6scs3y9wd8rgqd3ppjiqzm6m1ab00z2b27zhvib549dn")))

