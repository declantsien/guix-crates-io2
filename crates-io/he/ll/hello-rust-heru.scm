(define-module (crates-io he ll hello-rust-heru) #:use-module (crates-io))

(define-public crate-hello-rust-heru-0.1.0 (c (n "hello-rust-heru") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "time") (r "^0.1.12") (d #t) (k 0)))) (h "07jy08chi4x269yihldf7hv978crpwaqkfxlj1kzhqaqy55a967g")))

