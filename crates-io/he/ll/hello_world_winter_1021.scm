(define-module (crates-io he ll hello_world_winter_1021) #:use-module (crates-io))

(define-public crate-hello_world_winter_1021-0.1.0 (c (n "hello_world_winter_1021") (v "0.1.0") (d (list (d (n "time") (r "^0.1.12") (d #t) (k 0)))) (h "03j5spjda5na2s74b2v79kz02vfw5qvggqg0747k5dfipq6fjaq3")))

(define-public crate-hello_world_winter_1021-0.1.2 (c (n "hello_world_winter_1021") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "time") (r "^0.1.12") (d #t) (k 0)))) (h "1yp1rdq6fqr90nfpil27mx0whdkh1dygbrn36zs750l2bwq7r6gl")))

(define-public crate-hello_world_winter_1021-0.1.3 (c (n "hello_world_winter_1021") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "time") (r "^0.1.12") (d #t) (k 0)))) (h "0gv82ashrg6rb2dngnwkvgvpa9g8fzr3idhldcf25nv27sn9c5sz")))

