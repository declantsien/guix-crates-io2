(define-module (crates-io he ll hello_world_ev) #:use-module (crates-io))

(define-public crate-hello_world_ev-0.1.0 (c (n "hello_world_ev") (v "0.1.0") (h "1z99x50h3zxid5a449b6jf0hgqrrl233zndjiainaxhpzihahhqa") (y #t)))

(define-public crate-hello_world_ev-0.1.1 (c (n "hello_world_ev") (v "0.1.1") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "time") (r "^0.1.12") (d #t) (k 0)))) (h "1dy4ix5i796bb1n40ghd8klp7ah9ga57bc6qhla6ch9ymznz5iqc")))

