(define-module (crates-io he ll hello-adamben) #:use-module (crates-io))

(define-public crate-hello-adamben-0.1.0 (c (n "hello-adamben") (v "0.1.0") (h "1bbnmir9083xrdk76hjji27fyj1s405a2mlfhacbf3j1wddh1g5w") (y #t)))

(define-public crate-hello-adamben-0.1.1 (c (n "hello-adamben") (v "0.1.1") (h "0nzxmagbbvf4cqh9qpvkv6bnf76a0r076njpy24kwx2qkawnqhbs") (y #t)))

