(define-module (crates-io he si hesione_macros) #:use-module (crates-io))

(define-public crate-hesione_macros-0.1.0 (c (n "hesione_macros") (v "0.1.0") (d (list (d (n "hesione_shared") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 0)))) (h "0wjglqlndjmnpxfsvhjdpy0iffy9mjr43rl3whyc443n8j2gi8bf")))

(define-public crate-hesione_macros-0.1.1 (c (n "hesione_macros") (v "0.1.1") (d (list (d (n "hesione") (r "^0.1") (d #t) (k 2)) (d (n "hesione_shared") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "0k0zn5cnaf869njrn0gz445kgw6h62alsjjwwymalj90k31ybksb")))

