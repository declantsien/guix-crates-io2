(define-module (crates-io he si hesione) #:use-module (crates-io))

(define-public crate-hesione-0.1.0 (c (n "hesione") (v "0.1.0") (d (list (d (n "atomic-traits") (r "^0.3") (d #t) (k 0)) (d (n "hesione_macros") (r "^0.1") (d #t) (k 0)) (d (n "hesione_shared") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1nzw7m0zykidd490acijkbmf13cl7zwn5fc0iw48cs36iaip74xr")))

(define-public crate-hesione-0.1.1 (c (n "hesione") (v "0.1.1") (d (list (d (n "atomic-traits") (r "^0.3") (d #t) (k 0)) (d (n "hesione_macros") (r "^0.1") (d #t) (k 0)) (d (n "hesione_shared") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qa5j06v68dkb9fndl4rj24glp64w1zgplicnp1xq3g0nrr96nsj")))

