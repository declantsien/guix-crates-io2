(define-module (crates-io he xu hexutil) #:use-module (crates-io))

(define-public crate-hexutil-0.1.0 (c (n "hexutil") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 2)) (d (n "err-derive") (r "^0.2.4") (k 0)) (d (n "serde") (r "^1.0.106") (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 2)))) (h "1hcmbmd1208lcp0b3g667p0vkpqc82hi7zwcf7rzc0cbm1jjzphs") (f (quote (("std" "alloc" "err-derive/std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

