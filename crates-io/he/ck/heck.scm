(define-module (crates-io he ck heck) #:use-module (crates-io))

(define-public crate-heck-0.1.0 (c (n "heck") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.1.0") (d #t) (k 0)))) (h "01nf4l2dq5zfm5zdrsqvmacfc8h9ry3gnq7582bk6xbpj7d6ml2i")))

(define-public crate-heck-0.2.0 (c (n "heck") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.1.0") (d #t) (k 0)))) (h "1y90w2knz832z4ac04k6l73yw3dyb4zg43r5rxmll16cchppv03g")))

(define-public crate-heck-0.2.1 (c (n "heck") (v "0.2.1") (d (list (d (n "unicode-segmentation") (r "^1.1.0") (d #t) (k 0)))) (h "0l94sb6x6dfy7py1xl59a2365p9fkb7sirw5hri7spaajai45nz0")))

(define-public crate-heck-0.3.0 (c (n "heck") (v "0.3.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "10hvfw5j6wifsppx3im68vif9ggxf5rc0vw0ghdfa1afmlzgl17a")))

(define-public crate-heck-0.3.1 (c (n "heck") (v "0.3.1") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "01a2v7yvkiqxakdqz4hw3w3g4sm52ivz9cs3qcsv2arxsmw4wmi0")))

(define-public crate-heck-0.3.2 (c (n "heck") (v "0.3.2") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "1b56s2c1ymdd0qmy31bw0ndhm31hcdamnhg3npp7ssrmc1ag9jw7")))

(define-public crate-heck-0.3.3 (c (n "heck") (v "0.3.3") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0b0kkr790p66lvzn9nsmfjvydrbmh9z5gb664jchwgw64vxiwqkd")))

(define-public crate-heck-0.4.0 (c (n "heck") (v "0.4.0") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "1ygphsnfwl2xpa211vbqkz1db6ri1kvkg8p8sqybi37wclg7fh15") (f (quote (("unicode" "unicode-segmentation") ("default"))))))

(define-public crate-heck-0.4.1 (c (n "heck") (v "0.4.1") (d (list (d (n "unicode-segmentation") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "1a7mqsnycv5z4z5vnv1k34548jzmc0ajic7c1j8jsaspnhw5ql4m") (f (quote (("unicode" "unicode-segmentation") ("default"))))))

(define-public crate-heck-0.5.0-rc.1 (c (n "heck") (v "0.5.0-rc.1") (h "02ak8z07aynwcn0nn0ciiqc4sakl0lyvjrnhh3sr5vi1klpzgsi8") (r "1.56")))

(define-public crate-heck-0.5.0 (c (n "heck") (v "0.5.0") (h "1sjmpsdl8czyh9ywl3qcsfsq9a307dg4ni2vnlwgnzzqhc4y0113") (r "1.56")))

