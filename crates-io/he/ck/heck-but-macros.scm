(define-module (crates-io he ck heck-but-macros) #:use-module (crates-io))

(define-public crate-heck-but-macros-0.0.1 (c (n "heck-but-macros") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1cb5d9jflwq4g6zy8i1yc357li2cdvzlqbrfn4z1v4lhy4yxzzbp")))

