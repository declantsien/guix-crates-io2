(define-module (crates-io he ck heckcheck) #:use-module (crates-io))

(define-public crate-heckcheck-1.0.0 (c (n "heckcheck") (v "1.0.0") (d (list (d (n "arbitrary") (r "^1.0.1") (d #t) (k 0)) (d (n "arbitrary") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "08k7a6qr8wi69bbi37s7yxhpq10xka0jv2qcl8bv0c4n0khm0gyr")))

(define-public crate-heckcheck-1.0.1 (c (n "heckcheck") (v "1.0.1") (d (list (d (n "arbitrary") (r "^1.0.1") (d #t) (k 0)) (d (n "arbitrary") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0hi5d42cdnzj5a6i4m71dx53kmxz7bbl7jn19j7yk7f3y61f7xcs")))

(define-public crate-heckcheck-1.0.2 (c (n "heckcheck") (v "1.0.2") (d (list (d (n "arbitrary") (r "^1.0.1") (d #t) (k 0)) (d (n "arbitrary") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0xp927zxwdv18glxq4zdp3fb69xc3n15dryn7zl9704p1fw4fs1b")))

(define-public crate-heckcheck-2.0.0 (c (n "heckcheck") (v "2.0.0") (d (list (d (n "arbitrary") (r "^1.0.1") (d #t) (k 0)) (d (n "arbitrary") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1kr59bqynin2n7nwpkcigzrignwfpfxb32ds2hr6vwnilbajz52q")))

(define-public crate-heckcheck-2.0.1 (c (n "heckcheck") (v "2.0.1") (d (list (d (n "arbitrary") (r "^1.0.1") (d #t) (k 0)) (d (n "arbitrary") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0fpg34jxjm6snpa2w3dqprkgimyx498grl05sfgilhwz238fzgs7")))

