(define-module (crates-io he ck heckdiff) #:use-module (crates-io))

(define-public crate-heckdiff-0.1.0 (c (n "heckdiff") (v "0.1.0") (d (list (d (n "difflib") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)))) (h "0y01cn3vz7w01ljkz6b90rp9spici5c5pc38ki7z47fh9i2rfh68")))

(define-public crate-heckdiff-0.1.1 (c (n "heckdiff") (v "0.1.1") (d (list (d (n "difflib") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "17zfcr9zd3m3f0vwrdrr1v3wvnlgbw11ayszj4fci2pxzdgk93b6")))

(define-public crate-heckdiff-0.1.2 (c (n "heckdiff") (v "0.1.2") (d (list (d (n "difflib") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1iqwa92jjzxb4a5md9r69wq1bazh1wa94jxspmi2qap3c8w40m8g")))

