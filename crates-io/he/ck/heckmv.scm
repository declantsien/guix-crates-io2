(define-module (crates-io he ck heckmv) #:use-module (crates-io))

(define-public crate-heckmv-1.0.0 (c (n "heckmv") (v "1.0.0") (d (list (d (n "errata") (r "^2.1.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "sarge") (r "^7.0.2") (d #t) (k 0)))) (h "0w3lcq8m4isvgqzhc6pg4p8jyma0478ny21y0m51s2cvgshrfqsf")))

(define-public crate-heckmv-1.0.1 (c (n "heckmv") (v "1.0.1") (d (list (d (n "errata") (r "^2.1.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "sarge") (r "^7.0.2") (d #t) (k 0)))) (h "1w7pw6823b7gblxmm7wfp0mdrs63vv47g83nhq58mhd86fr1ranw")))

