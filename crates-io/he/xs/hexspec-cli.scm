(define-module (crates-io he xs hexspec-cli) #:use-module (crates-io))

(define-public crate-hexspec-cli-0.1.0 (c (n "hexspec-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "hexspec") (r "^0.1.0") (d #t) (k 0)))) (h "0s4dd20r0rzxavrk4abzfaz3f46p99krpgf71hyjzgsqj1xy39sx")))

(define-public crate-hexspec-cli-0.1.1 (c (n "hexspec-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "hexspec") (r "^0.1.1") (d #t) (k 0)))) (h "0a6k2k6hik51vbnbca67869f077p6gg22b0jn1ksr5dh0pbkq3z4")))

(define-public crate-hexspec-cli-0.2.0 (c (n "hexspec-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "hexspec") (r "^0.2.0") (f (quote ("parse"))) (d #t) (k 0)))) (h "0vkarmf3jclf8fzvd85fgfp4b8i34vzmb4fhz9y3v000g7m8xqj4")))

