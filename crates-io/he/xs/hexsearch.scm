(define-module (crates-io he xs hexsearch) #:use-module (crates-io))

(define-public crate-hexsearch-0.1.0 (c (n "hexsearch") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1x2h269kx82m9ny88v8drn29ilsdzmhrkrs9bkv6amcg4qjzvg3i")))

(define-public crate-hexsearch-0.1.1 (c (n "hexsearch") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1ysfpz17r19glbglvpp63wdmqqa75wbvddccns14mcrvnja36ph6")))

