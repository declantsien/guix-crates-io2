(define-module (crates-io he xs hexspec) #:use-module (crates-io))

(define-public crate-hexspec-0.1.0 (c (n "hexspec") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hglxnwhajw65vk116qnp0bsbhs6xynl7f5xy3qng4fqfq89qk50")))

(define-public crate-hexspec-0.1.1 (c (n "hexspec") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kbsfn3cw430x41i5rbvshzdjmh4qh48yj825n4zvglwqqinj62p")))

(define-public crate-hexspec-0.2.0 (c (n "hexspec") (v "0.2.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v3ik23gv3xsqcr0225s5a3495y3rjh427kvjg3wrd0cw9fkdnl8") (f (quote (("parse") ("default"))))))

