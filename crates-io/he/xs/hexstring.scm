(define-module (crates-io he xs hexstring) #:use-module (crates-io))

(define-public crate-hexstring-0.1.0 (c (n "hexstring") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.16") (f (quote ("display"))) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1831s6g3cxm2w34iyfxrhlmmcnv2qq2w2yvd1xfj8j0n0q3v3wki") (f (quote (("default"))))))

(define-public crate-hexstring-0.1.1 (c (n "hexstring") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.16") (f (quote ("display"))) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0mka4iiai9xyhs341l8hb6lw8yax6jrfh52l2a9pnnvy5rgp02y7") (f (quote (("default"))))))

(define-public crate-hexstring-0.1.2 (c (n "hexstring") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.16") (f (quote ("display"))) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0h0xbaakv6w1yl3yxqkq3xrriy1jnlv73wjk87rk2bb3zbq3vgfk") (f (quote (("default"))))))

(define-public crate-hexstring-0.1.3 (c (n "hexstring") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99.16") (f (quote ("display"))) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1319xvh2vcswsqarn5x392kacjfm27dbxc5yzcx29jvcjyhf1xsh") (f (quote (("default" "serde")))) (r "1.58.0")))

(define-public crate-hexstring-0.1.4 (c (n "hexstring") (v "0.1.4") (d (list (d (n "derive_more") (r "^0.99.16") (f (quote ("display"))) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1z7nawfykqsmabr1n0nkhbalc5nvcz2c9cln5fvc4m0dda061xlm") (f (quote (("default" "serde")))) (r "1.58.0")))

