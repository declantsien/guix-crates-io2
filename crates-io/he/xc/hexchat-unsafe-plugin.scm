(define-module (crates-io he xc hexchat-unsafe-plugin) #:use-module (crates-io))

(define-public crate-hexchat-unsafe-plugin-1.0.0 (c (n "hexchat-unsafe-plugin") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05wqk3hxx6ym6crh821vv5mav80mkzg0bn83mbr6dh7g7rykbj3f") (y #t)))

(define-public crate-hexchat-unsafe-plugin-2.0.0 (c (n "hexchat-unsafe-plugin") (v "2.0.0") (d (list (d (n "impl_trait") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0scwffvzga32h1rd8666l64yxa8jpadyd6a4765kr2fg7bgawhlk") (f (quote (("nul_in_strip"))))))

(define-public crate-hexchat-unsafe-plugin-2.0.1 (c (n "hexchat-unsafe-plugin") (v "2.0.1") (d (list (d (n "impl_trait") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03fynhky09ym0nbgi3bwr41nwyp5y7a3zi545xvrn7bx61szsj54") (f (quote (("nul_in_strip"))))))

(define-public crate-hexchat-unsafe-plugin-2.1.0 (c (n "hexchat-unsafe-plugin") (v "2.1.0") (d (list (d (n "impl_trait") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0aqsjkhbiqp22hjsd8rsnk6wkc85jk4v81g3vvkfsfq7dfsb4y5m") (f (quote (("nul_in_strip"))))))

(define-public crate-hexchat-unsafe-plugin-2.2.0 (c (n "hexchat-unsafe-plugin") (v "2.2.0") (d (list (d (n "impl_trait") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z92fxcm8g8r7l6wznr6r3ffx02x79xd7waf96b7a8szr2i8wg7k") (f (quote (("nul_in_strip") ("nightly_tests"))))))

(define-public crate-hexchat-unsafe-plugin-2.3.0 (c (n "hexchat-unsafe-plugin") (v "2.3.0") (d (list (d (n "impl_trait") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zc90yfyza69z6i9rn5yqrkjzp24jdbfrxa4xlvpfrd2bq2pygwj") (f (quote (("nul_in_strip") ("nightly_tests"))))))

