(define-module (crates-io he xc hexcrypt) #:use-module (crates-io))

(define-public crate-hexcrypt-0.1.0 (c (n "hexcrypt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1fqd62bwxvaqq9vv2scxvms9ldd4wl3pb4x3jj3l7cp6psdran9f")))

