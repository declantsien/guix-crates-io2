(define-module (crates-io he xc hexcli) #:use-module (crates-io))

(define-public crate-hexcli-0.0.1 (c (n "hexcli") (v "0.0.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0adlfn0vj9ajgxfjicg76xf1yks1a0y58h4pax0jc164bgsdsvcs")))

(define-public crate-hexcli-0.0.2 (c (n "hexcli") (v "0.0.2") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0yjm2517r6lzva4lpg7wncypicnhfgw86jwbgqz7gl14sdhwhixp")))

(define-public crate-hexcli-0.0.3 (c (n "hexcli") (v "0.0.3") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "04w4l7r24vi5kksnqw19h7dvja9nsc66bx7x8zlrlc7fsx5ljgrh")))

(define-public crate-hexcli-0.0.4 (c (n "hexcli") (v "0.0.4") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "00fj51ch86myxxx9bpfq0wba87a1083sk0n7cvzp4n762f0kdg7a")))

(define-public crate-hexcli-0.0.5 (c (n "hexcli") (v "0.0.5") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0kcx0sklqgynfxv5kfw8gdymmd3z0f1mf49y44p7z5vas21yscbs")))

