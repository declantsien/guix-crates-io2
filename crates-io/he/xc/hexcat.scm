(define-module (crates-io he xc hexcat) #:use-module (crates-io))

(define-public crate-hexcat-1.0.0 (c (n "hexcat") (v "1.0.0") (h "182aaiy0fiyyk20z7bh67hvmz1fqfld024vfp03jhaqkhvdiarjl") (y #t)))

(define-public crate-hexcat-1.0.1 (c (n "hexcat") (v "1.0.1") (h "06kiyrsh8m5s4j95smkp7j2kajdvpwkg99s7j3m6ah08rad3jmpw") (y #t)))

(define-public crate-hexcat-2.0.0 (c (n "hexcat") (v "2.0.0") (h "0097czn21xz1gfb59qd41mcd3hm2fdpsryi6hnzs3xp0bd238vsp")))

(define-public crate-hexcat-2.1.0 (c (n "hexcat") (v "2.1.0") (h "0zhkq7fcdw2hs2ykrdc38q5i72p8zag5iphp26cb1957glqqcg52")))

(define-public crate-hexcat-2.1.1 (c (n "hexcat") (v "2.1.1") (h "0x66a9w1n2g60likc8s1ax3h30501ylv1n5vqjg0kgzzhyn15vxa")))

(define-public crate-hexcat-2.1.2 (c (n "hexcat") (v "2.1.2") (h "0m43pvm3ibnls29073i3wqcx4ml15h7qdy33dv1hp2fa1s2g0c1r")))

(define-public crate-hexcat-2.3.2 (c (n "hexcat") (v "2.3.2") (h "1w2n2kwhafyvr777g2rw7v44csc07vriqhavqq1y120qwmxx93v4")))

(define-public crate-hexcat-2.3.3 (c (n "hexcat") (v "2.3.3") (h "0ga9ldkf1nc7ia53ifvk2g4h38bpdc88r78yh53g1k085216c513")))

(define-public crate-hexcat-2.3.4 (c (n "hexcat") (v "2.3.4") (h "00pblcdm44knraf1z9afx2kljzmhm2grgb9id80k61zsdizzp2gp")))

