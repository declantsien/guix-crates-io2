(define-module (crates-io he xc hexchat-plugin) #:use-module (crates-io))

(define-public crate-hexchat-plugin-0.2.0 (c (n "hexchat-plugin") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dsq095q578i3z02yn3156x3bsmz6qd8affmg6m5xpg7n7ajxlwh")))

(define-public crate-hexchat-plugin-0.2.1 (c (n "hexchat-plugin") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1j35h6f9n1v13v39znpv6sdx3j9h1sb1jfqph8nvdbrfrss369hv")))

(define-public crate-hexchat-plugin-0.2.2 (c (n "hexchat-plugin") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yq5x3n8ba7fnpmkg5y0phjncfb2qxwrp6972bb78paqghmhqsqy")))

(define-public crate-hexchat-plugin-0.2.3 (c (n "hexchat-plugin") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19hx7b28xbzb281wkgkjppzb8l1pfag3qjmig14g2yr3n38rxfw7")))

(define-public crate-hexchat-plugin-0.2.4 (c (n "hexchat-plugin") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "050rsyadfc1ld09rnm69x24mk0mh777j0zbm7ycfjjgsmzc9ni8l")))

(define-public crate-hexchat-plugin-0.2.5 (c (n "hexchat-plugin") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00l5z8zgw0r357avkfk2bpk61jbldkg6i8imnk0bn4gm077i5n4s")))

(define-public crate-hexchat-plugin-0.2.6 (c (n "hexchat-plugin") (v "0.2.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bhfn0ixzi8brxl4ishm4dz5kqdzb1rk8ys8qxdi89mvfxzwz5ws")))

(define-public crate-hexchat-plugin-0.2.7 (c (n "hexchat-plugin") (v "0.2.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rm32cflh3f5ci47c7ccj1xcsxrij5mxvjcl7nwxx1dldcr72dbv")))

(define-public crate-hexchat-plugin-0.2.8 (c (n "hexchat-plugin") (v "0.2.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hkn0fhpib2jq6wsf23s5pfffmi2x4kpjdiyxmx21wbmhwc949kp")))

(define-public crate-hexchat-plugin-0.2.9 (c (n "hexchat-plugin") (v "0.2.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kapz9c2kwzqlc24q0ychl88akf0n8jnz1kbhvs3gjmkqfzrgybz")))

(define-public crate-hexchat-plugin-0.2.10 (c (n "hexchat-plugin") (v "0.2.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1slw2piibgxgjd977i1ynf047dh1djja0zd6nyma79qlmncjzns9")))

(define-public crate-hexchat-plugin-0.2.11 (c (n "hexchat-plugin") (v "0.2.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vbafvq5d2zzj4yifrqwhr7ykllqfyy7s7vnxfgf37rjhgrk88yy")))

(define-public crate-hexchat-plugin-0.2.12 (c (n "hexchat-plugin") (v "0.2.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18slkd5vindd72r6rxldynrl3d4zlwwb8bdgjzqqij98krvymk5k")))

(define-public crate-hexchat-plugin-0.2.13 (c (n "hexchat-plugin") (v "0.2.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1660qq9fr0a897d6bdaz2ljl71kjjma436ig5dnj3z56jwd3xqix")))

(define-public crate-hexchat-plugin-0.2.14 (c (n "hexchat-plugin") (v "0.2.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "036jdm3nw52sc136q9yzb05hid0sb78vyf2liiz1hlmpqj5xzc7p")))

