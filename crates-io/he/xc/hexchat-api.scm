(define-module (crates-io he xc hexchat-api) #:use-module (crates-io))

(define-public crate-hexchat-api-0.2.1 (c (n "hexchat-api") (v "0.2.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)))) (h "0i9k4disf948qh8khknc7q9w38j3r0c9qf10m37rd715bd17f4p9") (y #t)))

(define-public crate-hexchat-api-0.2.2 (c (n "hexchat-api") (v "0.2.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)))) (h "16lr9snyn6i7wqcrjyqa488jld67q3jkia6zn8yli0xjjlyaq15a") (y #t)))

(define-public crate-hexchat-api-0.2.3 (c (n "hexchat-api") (v "0.2.3") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)))) (h "1ibyabyyzhca9809j4zlsjsjpy2hv961bkggxyb5x2f1and37lr8") (y #t)))

(define-public crate-hexchat-api-0.2.4 (c (n "hexchat-api") (v "0.2.4") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)))) (h "0mnmc3js1093qcsd13hr71rnb3x6qdcqax5ivi48jrc3x36s27hl")))

(define-public crate-hexchat-api-0.2.5 (c (n "hexchat-api") (v "0.2.5") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)))) (h "10fy92yhbayxwkb102m9l5gwdbxyy9gkr6xbm5i3yxb43ys2298j")))

(define-public crate-hexchat-api-0.3.0 (c (n "hexchat-api") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)))) (h "0dai1n9qq7948n5cr0kn5qckwrq9wfccmyabxw9f2vr8g7916miy") (f (quote (("threadsafe") ("default" "threadsafe")))) (y #t)))

(define-public crate-hexchat-api-0.3.1 (c (n "hexchat-api") (v "0.3.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)))) (h "0y2aa8g25xi5d01jkybxir8m2br49wninbkx2pdp2rjj39ks389a") (f (quote (("threadsafe") ("default" "threadsafe"))))))

(define-public crate-hexchat-api-0.3.2 (c (n "hexchat-api") (v "0.3.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)) (d (n "send_wrapper") (r "^0.6.0") (d #t) (k 0)))) (h "1g5vgxc2wghjlsmfn2v84x4cxspv1sn29wm4zb1aallzkmdpdzh7") (f (quote (("threadsafe") ("default" "threadsafe"))))))

