(define-module (crates-io he y- hey-jarvis) #:use-module (crates-io))

(define-public crate-hey-jarvis-0.1.2 (c (n "hey-jarvis") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chatgpt_rs") (r "^1.2.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "0djb9rbcilarrrpdrjcnrrjbqgra495cblcsjr8xh68ssvvwdxrl")))

