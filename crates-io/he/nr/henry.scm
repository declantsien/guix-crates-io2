(define-module (crates-io he nr henry) #:use-module (crates-io))

(define-public crate-henry-0.1.0 (c (n "henry") (v "0.1.0") (d (list (d (n "dashmap") (r "^3.11.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("sync"))) (k 0)))) (h "1jkdqkmvsv2p1zr6fggvxq1gq9j4a9xkf4rg4byjc7ir6zm6qhn6")))

(define-public crate-henry-0.2.0 (c (n "henry") (v "0.2.0") (d (list (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "07jd7n1ab7lzs2vygp4imgii7nj49y95hbcxd77dyx310i7ry0x8")))

