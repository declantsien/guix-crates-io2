(define-module (crates-io lh ap lhapdf) #:use-module (crates-io))

(define-public crate-lhapdf-0.1.0 (c (n "lhapdf") (v "0.1.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0cdwpigpslsyx8zxz083yjqh246f09gmspxhd3isi63wv7mx493w")))

(define-public crate-lhapdf-0.1.1 (c (n "lhapdf") (v "0.1.1") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0b3m7rxwzdd615zklmknfyhlv34i5yr1fqmhas635lw2adz8nrv9")))

(define-public crate-lhapdf-0.1.2 (c (n "lhapdf") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "068vxdwpiyvwnv2j336wy92qbyc33z7jqxxj4jwf3j548gd75pky") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.1.3 (c (n "lhapdf") (v "0.1.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0llf8y64rmi65bqr0ci3c5wny1rfx8hw2pkf4yqmqmhd7aa5cqbh") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.1.5 (c (n "lhapdf") (v "0.1.5") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gz5yq1d74alrcdd19sbb4ff4kbarrf442hp5n3n215ihzs262gw") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.1.6 (c (n "lhapdf") (v "0.1.6") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "10q738947h13n0rwbkqarcd0ndx3xahcdskl7v1sbhigq94vwd1k") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.1.7 (c (n "lhapdf") (v "0.1.7") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "025ghrp5crqhy7pjln6m5lh5mm4j06y2ixz1c70bh4zlbf4nkai2") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.1.8 (c (n "lhapdf") (v "0.1.8") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hhb1zpw9f6hkjh7gv9hv3b6kqhg6a4bd7mxrw29cqwplinfwim2") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.1.9 (c (n "lhapdf") (v "0.1.9") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ia97d2kjxvzr2m7ks02ppkgs31w33kn2ixgm8nwp2gr7sb8m0sp") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.1.10 (c (n "lhapdf") (v "0.1.10") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1apyqbkcfhvdmfpvcp6jwi97fins5m6l5z7vdw4bx7kfy61wg8v2") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.1.11 (c (n "lhapdf") (v "0.1.11") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "01wnc614q8qm0phhm8vr0xfbqvfbz81s4pzq9q9qsk06qamkkvhp") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.2.0 (c (n "lhapdf") (v "0.2.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r72p72587xqshpd68nwr3z73fabdma2r2cx0vgs62j81y48qjff") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.2.1 (c (n "lhapdf") (v "0.2.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s4cyy8f3qzpz4n3rbr96wxvbl86sqnli4bzbjvjp1d8x8x9kz6j") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.2.2 (c (n "lhapdf") (v "0.2.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iasz9pd314d82ijq2j10v0kml1hfj04radbchyrwg1cqchvs6fy") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.2.3 (c (n "lhapdf") (v "0.2.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mzsccvwdrjnkz875pascyqxp3nr234rjg3qdmv9vv9pa11w0isa") (f (quote (("docs-only"))))))

(define-public crate-lhapdf-0.2.4 (c (n "lhapdf") (v "0.2.4") (d (list (d (n "cxx") (r "^1.0.107") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.107") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19rd19h28lf4d05r74i9pb9xifazm9wl6sd6yiw6i2h84vy94q0g") (f (quote (("docs-only"))))))

