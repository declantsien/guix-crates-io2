(define-module (crates-io lh ef lhef) #:use-module (crates-io))

(define-public crate-lhef-0.1.0 (c (n "lhef") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "01hzgzjqm4n76s9s8ahm04f1d98xlpx7wfy4chbzzygzjvk5jrja")))

(define-public crate-lhef-0.2.0 (c (n "lhef") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xmltree") (r "^0.7") (d #t) (k 0)))) (h "03xa2d55qszlhmidrqfflkj2zkx45ds84dwxmzrav7a6nqbcf81b")))

(define-public crate-lhef-0.2.1 (c (n "lhef") (v "0.2.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xmltree") (r "^0.7") (d #t) (k 0)))) (h "19yb5lch9z0jq0c5nr6dbkmlwyl2ps7fqarr4f9ad56na9wnm9dm")))

(define-public crate-lhef-0.3.0 (c (n "lhef") (v "0.3.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xmltree") (r "^0.7") (d #t) (k 0)))) (h "0mrm7n4mjwsgkk99q6mlnpm7mnf9nnw7zw5xcm7jagw7a6b1pwkf")))

(define-public crate-lhef-0.3.1 (c (n "lhef") (v "0.3.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xmltree") (r "^0.8") (d #t) (k 0)))) (h "0ph3h16s7s5zz6wg0gaz8m2w1wym25y0jsy01h6wck3j7p6f792v")))

(define-public crate-lhef-0.3.2 (c (n "lhef") (v "0.3.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xmltree") (r "^0.9") (d #t) (k 0)))) (h "0wrqysislpyfycaf24pfdhkva7ixfxwhc9kf0c5yaim2m1ynsznw")))

(define-public crate-lhef-0.3.3 (c (n "lhef") (v "0.3.3") (d (list (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xmltree") (r "^0.9") (d #t) (k 0)))) (h "1i6c74lcsin7qa5x934kfhgp2c1kqdxaylp07zg123smkp4pa992")))

(define-public crate-lhef-0.3.4 (c (n "lhef") (v "0.3.4") (d (list (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xmltree") (r "^0.9") (d #t) (k 0)))) (h "0idvkbsks6abk72q13r1gkqzi3i25as6w0941yh87bbzm5dnwqr2")))

(define-public crate-lhef-0.4.0 (c (n "lhef") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.9") (d #t) (k 0)))) (h "0xibc344xyijlynz7mjvsj6inswpg05wgp63ihw6ixqng42c8gy0")))

(define-public crate-lhef-0.5.0 (c (n "lhef") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.9") (d #t) (k 0)))) (h "0qjhyrxp0cxr5grz57nvgkw85wwkf6yf3iir42cv6csn2yxzyxbp")))

(define-public crate-lhef-0.6.0 (c (n "lhef") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "fast-float") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.9") (d #t) (k 0)))) (h "0ayysb7zpyq9d9wckk0p30sbv5wggr7csgk3pzky6d2a25crffv6")))

