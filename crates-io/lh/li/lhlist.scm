(define-module (crates-io lh li lhlist) #:use-module (crates-io))

(define-public crate-lhlist-0.1.0 (c (n "lhlist") (v "0.1.0") (d (list (d (n "label_attribute") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0323iib271g5x5i54y82pyhrn7gi6v1sgx1lgc9l9428nfvl0693")))

(define-public crate-lhlist-0.1.1 (c (n "lhlist") (v "0.1.1") (d (list (d (n "label_attribute") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "00dhh8n65vcjw7kpwjx6zr1q2fsa7h2ycmjxzz0wh1n232148wg8")))

