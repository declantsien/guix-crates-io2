(define-module (crates-io jj -l jj-lib-proc-macros) #:use-module (crates-io))

(define-public crate-jj-lib-proc-macros-0.15.1 (c (n "jj-lib-proc-macros") (v "0.15.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1miimg16z1wbf0zfkcypg296xsjlz2k4sc9sqfzh9kab0mw9s4zz") (r "1.76")))

(define-public crate-jj-lib-proc-macros-0.16.0 (c (n "jj-lib-proc-macros") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0yhpgqsjv4276m7dipg4ph7am4wa9yvir1icjdlmzfvh5i2x7yrq") (r "1.76")))

(define-public crate-jj-lib-proc-macros-0.17.0 (c (n "jj-lib-proc-macros") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0ajsdhivx8s6vc0c79qp657csrmplkrhjpzsjlc5zv5nvgnsf5bj") (r "1.76")))

(define-public crate-jj-lib-proc-macros-0.17.1 (c (n "jj-lib-proc-macros") (v "0.17.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0m5nfz6dfk5d2jsbc0pmx548307pvpwchacfbnlvc6ab6ybxaibw") (r "1.76")))

