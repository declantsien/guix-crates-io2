(define-module (crates-io jj ss jjss_tocargo) #:use-module (crates-io))

(define-public crate-jjss_tocargo-0.1.0 (c (n "jjss_tocargo") (v "0.1.0") (h "12lfrc89x9ff9ah21p3r8vdpvhaqhl8kx6n0nqsw87izf9vkm2k2")))

(define-public crate-jjss_tocargo-0.1.1 (c (n "jjss_tocargo") (v "0.1.1") (h "1dj7fmy0fxpp9bzwdxswb78w80p1wsmsvhcn5vxlzx7k0whi5plx")))

(define-public crate-jjss_tocargo-0.1.2 (c (n "jjss_tocargo") (v "0.1.2") (h "16pij8zc8c140k7kfqgwzi1r4kpv99l6n8njqlvqzcpr327qmjkq")))

(define-public crate-jjss_tocargo-0.1.3 (c (n "jjss_tocargo") (v "0.1.3") (h "1a7fl1ygzyw76ss43i8g8431mr4dafb109k93z2kagldf4hjy7y9")))

(define-public crate-jjss_tocargo-0.1.4 (c (n "jjss_tocargo") (v "0.1.4") (h "0xjik9h3mh2hrdsimkvsjz9nkvsz5pm3l3zbfi5jpl1zapzns8jd")))

(define-public crate-jjss_tocargo-0.1.5 (c (n "jjss_tocargo") (v "0.1.5") (h "1b6yhxfcn0nfy6y1i56x5n5dhfjag504v0cwbdyqnljwwrd0y0xf")))

(define-public crate-jjss_tocargo-0.1.6 (c (n "jjss_tocargo") (v "0.1.6") (h "0hadvzwqyqjb8l6z9l23g5krj0v7drikzwvbls76lmhann4qdmd3")))

