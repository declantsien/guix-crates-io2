(define-module (crates-io j9 -s j9-sys) #:use-module (crates-io))

(define-public crate-j9-sys-0.1.0 (c (n "j9-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 1)) (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 1)))) (h "1qsgfbx3j1s3vn0jii59j1jg3k3prni6935zrky15zfakrw4hcvs")))

(define-public crate-j9-sys-0.1.1 (c (n "j9-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 1)) (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 1)))) (h "1kvvy0vyb1pvvxwmhn3bgds627b222sspx7q31v3rl98j2nj0phg")))

(define-public crate-j9-sys-0.1.2 (c (n "j9-sys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 1)) (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 1)))) (h "00a0p89lanvinw0jh7lm82b7qdhr24n4d2p84gi9rv7x2b35g1if")))

(define-public crate-j9-sys-0.1.3 (c (n "j9-sys") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 1)) (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 1)))) (h "1z8agprh04dbmx68ihrsjcfhiyfj6z9bw50n37j5c6wdbyfam37y")))

