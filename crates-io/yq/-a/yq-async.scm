(define-module (crates-io yq -a yq-async) #:use-module (crates-io))

(define-public crate-yq-async-0.3.0 (c (n "yq-async") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r "^0.22") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "yq") (r "^0.3") (d #t) (k 0)))) (h "0qy2w35p4sy37s476x2czv9sh1gihjwa5h3gr3vnfn0lh7qkd6fq")))

(define-public crate-yq-async-0.3.1 (c (n "yq-async") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r "^0.22") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "yq") (r "^0.3") (d #t) (k 0)))) (h "1pccd337vkjy4znahp6jn5y3j007ghyplh10l6lmwg44jdv47ls2")))

(define-public crate-yq-async-0.4.1 (c (n "yq-async") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "redis") (r "^0.23") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "yq") (r "^0.4") (d #t) (k 0)))) (h "0qc18r70li1vicwv10m9hgkw3ajs6nanqg5r4idqfrbkix8xz730")))

