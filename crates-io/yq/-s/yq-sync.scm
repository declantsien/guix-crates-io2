(define-module (crates-io yq -s yq-sync) #:use-module (crates-io))

(define-public crate-yq-sync-0.3.0 (c (n "yq-sync") (v "0.3.0") (d (list (d (n "redis") (r "^0.22") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "yq") (r "^0.3") (d #t) (k 0)))) (h "1lxfr61pz9ag2j0999ky4fa2y985s9mknij927r49jby8w8vc6hx")))

(define-public crate-yq-sync-0.3.1 (c (n "yq-sync") (v "0.3.1") (d (list (d (n "redis") (r "^0.22") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "yq") (r "^0.3") (d #t) (k 0)))) (h "1f27d2djmi8dllrmb9xrarmlm8ph65bm9s2bkg5y54xc70l04bjz")))

(define-public crate-yq-sync-0.4.1 (c (n "yq-sync") (v "0.4.1") (d (list (d (n "redis") (r "^0.23") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "yq") (r "^0.4") (d #t) (k 0)))) (h "0gq0ar95yzc86i83g7pszcq0bl6xb6vr7hb28r930rg3v8l89fl1")))

