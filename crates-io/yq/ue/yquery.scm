(define-module (crates-io yq ue yquery) #:use-module (crates-io))

(define-public crate-yquery-0.1.0 (c (n "yquery") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "05cprfd7rkq2jnv2k2n6p1sh8i5ihafc867j3v27c9hh4vr1g2i8")))

(define-public crate-yquery-0.1.1 (c (n "yquery") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "15i4lrifsk69m2md062gf5rh556sgi6ich7pp6ipf9yxa6hnbjyi")))

