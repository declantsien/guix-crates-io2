(define-module (crates-io ke yl keylogger) #:use-module (crates-io))

(define-public crate-keylogger-0.1.0 (c (n "keylogger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "device_query") (r "^0.2.4") (d #t) (k 0)))) (h "1q4nr1p5ghf1n7nli8lfi59fni19rf7j25iqml14lzra6ll5q2x1")))

(define-public crate-keylogger-0.1.1 (c (n "keylogger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "device_query") (r "^0.2.4") (d #t) (k 0)))) (h "1xfynqclx3hc78f9apjsah1rj8f6h31v3v4k2s72ljsi6fwvw24g")))

(define-public crate-keylogger-0.1.2 (c (n "keylogger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "device_query") (r "^0.2.4") (d #t) (k 0)))) (h "0kbw6nvmchjcj5r9bw4binwzp15kcvivlzp123yswzxazfqybcmh")))

(define-public crate-keylogger-0.1.3 (c (n "keylogger") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "device_query") (r "^0.2.4") (d #t) (k 0)))) (h "0j5ql2yjm9s9i4i19sqfd8pdhkjvr55gw7kd04n5136vyjfv568r")))

(define-public crate-keylogger-0.2.0 (c (n "keylogger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "device_query") (r "^1.1.3") (d #t) (k 0)))) (h "0xjma1xbm6halx155ywvdhsw9maaplpn37rd0vg6103q0b23m67f")))

(define-public crate-keylogger-0.2.1 (c (n "keylogger") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "device_query") (r "^1.1.3") (d #t) (k 0)))) (h "1apx7nyr29dwrr274q66m2jvp0c8pavkgkbln5kh5sxlasgm294d")))

