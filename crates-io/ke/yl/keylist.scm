(define-module (crates-io ke yl keylist) #:use-module (crates-io))

(define-public crate-keylist-0.1.0 (c (n "keylist") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0jfkk50b8gp0i2dky42y34wp02h4qqh64imhkci66glccq94fj1l")))

(define-public crate-keylist-0.1.1 (c (n "keylist") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0hzkmq2rfipqxmj7cf46jcnavcpgsc33xg1l5z5lq8g3lkrpjry3")))

(define-public crate-keylist-0.2.0 (c (n "keylist") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1kx2cn4dmsi1jkhx1iv9j8dj9jkr4mvsmjh90pf3f8w58xw1pxlp")))

(define-public crate-keylist-0.3.0 (c (n "keylist") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "00zwfqyhbr5v2ayb0fhphcm0zaahj0hwmsmlawlwf0qdxf46ypz8")))

