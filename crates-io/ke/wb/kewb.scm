(define-module (crates-io ke wb kewb) #:use-module (crates-io))

(define-public crate-kewb-0.1.0 (c (n "kewb") (v "0.1.0") (d (list (d (n "bincode") (r "^2.0.0-rc") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1scigyh6f3y84iwdsrgfdy47zdrkn4bwnx130bpmhr4zayygc6gr")))

(define-public crate-kewb-0.2.0 (c (n "kewb") (v "0.2.0") (d (list (d (n "bincode") (r "^2.0.0-rc") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1b2i75k212qs008ggsjgmrpyz4dxg8j49ybggi6zambx245s5b91")))

(define-public crate-kewb-0.2.1 (c (n "kewb") (v "0.2.1") (d (list (d (n "bincode") (r "^2.0.0-rc") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "14w91k4bkkbpapci4a8dcb2z50xdaph1l8whfds96djfm21mlc7i")))

(define-public crate-kewb-0.3.0 (c (n "kewb") (v "0.3.0") (d (list (d (n "bincode") (r "^2.0.0-rc") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1825fl55xhnh3i9hfa6j7b7mc96a1kwi1fvsk8gcn9n9mwj215d4")))

(define-public crate-kewb-0.4.0 (c (n "kewb") (v "0.4.0") (d (list (d (n "bincode") (r "^2.0.0-rc") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1gadcdbl5wpavlgm2ka0fy7yh4s82d66pm99ficpcn14k1s0b3mj")))

(define-public crate-kewb-0.4.1 (c (n "kewb") (v "0.4.1") (d (list (d (n "bincode") (r "^2.0.0-rc") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0rzyq1jig93scc0h7yb7wq4l3xzlhyq9gkwjparlvr9n51cigwd8")))

(define-public crate-kewb-0.4.2 (c (n "kewb") (v "0.4.2") (d (list (d (n "bincode") (r "^2.0.0-rc") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1rngkbgq0jg2wy1xh8iw0v1yygij9b8lzifgks3l1vcabb1qax3l")))

