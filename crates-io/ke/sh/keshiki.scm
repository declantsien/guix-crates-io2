(define-module (crates-io ke sh keshiki) #:use-module (crates-io))

(define-public crate-keshiki-0.1.0 (c (n "keshiki") (v "0.1.0") (h "0k3992a41wmc77xip2p8aphmvcjbvaybnvsg1mq5bjl3zjnliy5r") (y #t)))

(define-public crate-keshiki-0.0.0 (c (n "keshiki") (v "0.0.0") (h "084s9jgxwdxb5ksd1qvr9j1qcw1klaq30nqvv9yj3qp6ikpl1z1z") (y #t)))

