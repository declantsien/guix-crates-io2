(define-module (crates-io ke tc ketchup-codegen) #:use-module (crates-io))

(define-public crate-ketchup-codegen-0.0.0 (c (n "ketchup-codegen") (v "0.0.0") (d (list (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1sgd9g2vv4y90jcvg2z323hhcy2m6vnn64g54fdxwv01kik27rdz")))

