(define-module (crates-io ke tc ketchup) #:use-module (crates-io))

(define-public crate-ketchup-0.0.0 (c (n "ketchup") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0m2ryxs8pj80b36j2kwi2xbf8jxx4zrqq3xh5vchkkjdwws80d0n")))

