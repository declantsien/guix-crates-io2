(define-module (crates-io ke to ketos_derive) #:use-module (crates-io))

(define-public crate-ketos_derive-0.1.0 (c (n "ketos_derive") (v "0.1.0") (d (list (d (n "ketos") (r "^0.9") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "02k1w5bhf25ynkraz596m2rwmy5gc0xlpzdzliy47zb8zc7mrs32")))

(define-public crate-ketos_derive-0.10.0 (c (n "ketos_derive") (v "0.10.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "ketos") (r "^0.10") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1rprxpi1zhjm6d190xgiizkd80jlw119wfhpgm44r6mwg1z3gkkf")))

(define-public crate-ketos_derive-0.11.0 (c (n "ketos_derive") (v "0.11.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "1dq4hpx6k7bl4i216f4cm5gk99lp1ky0zhr4sgfjmyj5w490p3xc")))

(define-public crate-ketos_derive-0.12.0 (c (n "ketos_derive") (v "0.12.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0b8b3am4jd6kx8py5r79f6fp933hq6x9h077pgbqiqr12bra120y")))

