(define-module (crates-io ke yg keygenx) #:use-module (crates-io))

(define-public crate-keygenx-0.1.0 (c (n "keygenx") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11c3pss741swr7pp4hg410w7qx0ihjl11a5rdydhj54d10vxh26f") (y #t)))

(define-public crate-keygenx-0.1.1 (c (n "keygenx") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13d0g759j15y368xnww79h0dxd704d3rg91dqpr5sl45nhgcxmhx") (y #t)))

(define-public crate-keygenx-0.1.2 (c (n "keygenx") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1anzz03n4rw28xkk6zxvxb1syqksyg993b436k3kf4nfr0qn9ll5") (y #t)))

(define-public crate-keygenx-0.1.4 (c (n "keygenx") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13c2x2dy0h59fsx1vym28l9qyyn312jiprf8icv606rc67rsjbh6") (y #t)))

(define-public crate-keygenx-0.1.7 (c (n "keygenx") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0kp116px7mrvzwzkynisnmx6nqszxg0yl17ih0gy7kpdzyyk1gci") (f (quote (("test") ("crypto")))) (y #t)))

(define-public crate-keygenx-0.1.71 (c (n "keygenx") (v "0.1.71") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "030jfn0slpdimzy678xv0mgasjxc8rxzjcfghhxl5nmipa0nb1jy") (f (quote (("test") ("crypto")))) (y #t)))

(define-public crate-keygenx-0.1.72 (c (n "keygenx") (v "0.1.72") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "08v54v79gkd209schcd15m5dx0pk8p26s3ybmqjvrxi82gmms2p6") (f (quote (("test") ("crypto")))) (y #t)))

(define-public crate-keygenx-0.1.73 (c (n "keygenx") (v "0.1.73") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1v4w7fdgg72yfq0yvg2w2bvssjf6l5ja59w1nc8m54fl2vhy2d0r") (f (quote (("test") ("crypto"))))))

(define-public crate-keygenx-0.1.74 (c (n "keygenx") (v "0.1.74") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0xcid0i4q12gkn3z0w1lf8bjg4ig9vjyk2wjwkhmclcndlcyhsrb") (f (quote (("test") ("crypto"))))))

