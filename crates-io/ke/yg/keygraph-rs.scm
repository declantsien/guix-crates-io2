(define-module (crates-io ke yg keygraph-rs) #:use-module (crates-io))

(define-public crate-keygraph-rs-0.1.0 (c (n "keygraph-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.4") (d #t) (k 0)))) (h "1lg5pdh5ns1w27r32j59yi4kivsjvn4lghqpin5lblvdcfv2wmgq")))

(define-public crate-keygraph-rs-0.2.0 (c (n "keygraph-rs") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.4") (d #t) (k 0)))) (h "0dnrjkih0qg1h8f68xvdj9vfwy2gfm96cj6m89a85v78pxac55j4")))

(define-public crate-keygraph-rs-0.2.1 (c (n "keygraph-rs") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.4") (d #t) (k 0)))) (h "109b4r1jcv5nd9vg0z9hcbknjgmsdpx5yf5x7l12z35zp9mrh33s") (y #t)))

(define-public crate-keygraph-rs-0.2.2 (c (n "keygraph-rs") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.4") (d #t) (k 0)))) (h "018zfamdlh7slflw038mm4a7q5lsrkbdnwcvk5afbb612n08v1mr") (y #t)))

(define-public crate-keygraph-rs-0.2.3 (c (n "keygraph-rs") (v "0.2.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.4") (d #t) (k 0)))) (h "0san93p2ixghnda8srr5aw174lycvpi33b3hq3784ccqjzj2lb32")))

(define-public crate-keygraph-rs-0.2.4 (c (n "keygraph-rs") (v "0.2.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.4") (d #t) (k 0)))) (h "1lpm0vb5f3vwp3xb7f4a7f9bzfb9w0riwhxxsrj2d49aic2bd7xp")))

(define-public crate-keygraph-rs-0.2.5 (c (n "keygraph-rs") (v "0.2.5") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.4") (d #t) (k 0)))) (h "00ji6gbvflnk7y7b09wb8lbg0r8hxfi6nsrbjkxyq0sjhksarpqz")))

