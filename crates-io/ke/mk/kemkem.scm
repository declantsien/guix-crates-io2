(define-module (crates-io ke mk kemkem) #:use-module (crates-io))

(define-public crate-kemkem-1.0.0 (c (n "kemkem") (v "1.0.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "03586q4m02pqrxfjbcfgh5cd5wfjwd7g5bx2nyg1p1h09nx9kipy")))

(define-public crate-kemkem-1.0.1 (c (n "kemkem") (v "1.0.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "0dvagl5xwygjjsbiqkbqyci2z98b9x2vbvhqg1zxny7q2zlr0ibh")))

(define-public crate-kemkem-1.0.2 (c (n "kemkem") (v "1.0.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "1r4xs4n8sky581qmvm16ppvcaglmc247mrz4f6bgrngxzy9h6im7")))

