(define-module (crates-io ke yh keyhelper) #:use-module (crates-io))

(define-public crate-keyhelper-0.1.0 (c (n "keyhelper") (v "0.1.0") (d (list (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "1320s9sxw85nca2767dl2hgw3nf3mbvvd6bpx62wix447rndz1h6") (y #t)))

(define-public crate-keyhelper-0.1.1 (c (n "keyhelper") (v "0.1.1") (d (list (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "1sxrpcpdc7r8av319n2i1xxcj1pqvfd1fq8x2b0jsca1ndqjcfwc") (y #t)))

(define-public crate-keyhelper-0.1.2 (c (n "keyhelper") (v "0.1.2") (d (list (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "03gpcx2pbj08977jjas7bgikhyii0dwx74llvyldpdbzra47fsnb") (y #t)))

(define-public crate-keyhelper-0.1.3 (c (n "keyhelper") (v "0.1.3") (d (list (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "183lp1najjvvrbm61nn3wk5sznrkcb4ljggvg3hdp7x12ql6qnq2") (y #t)))

(define-public crate-keyhelper-0.1.4 (c (n "keyhelper") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "05j20ppxca4zflgvq9zj6c70w0sp6723n6g3bm1sm0afci45xmig") (y #t)))

(define-public crate-keyhelper-0.1.5 (c (n "keyhelper") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "1gl710abcjdr82c8v7wc3c45dslmgc3f5rb6chhxsz6jcy0r977b") (y #t)))

(define-public crate-keyhelper-0.1.6 (c (n "keyhelper") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "1y59a7xilc4mai57nx51905vy4bp54qjq2ps8ym3alchyq2ms5sq") (y #t)))

(define-public crate-keyhelper-0.1.7 (c (n "keyhelper") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0d2n8jbfm786459x1502a5z4kbifglhypdxfb5237pfjmc8ssxsx") (y #t)))

(define-public crate-keyhelper-0.1.8 (c (n "keyhelper") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0i815q6lngk5fsh7yrxw4cw0b4jsa2qwp20x6ya19rs1q1g0cvjm") (y #t)))

(define-public crate-keyhelper-0.1.9 (c (n "keyhelper") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0v70ql9cv7ljvck6hk3z4ic1wv505xbzjy71ax80vz8g0i6nkzqi") (y #t)))

(define-public crate-keyhelper-0.2.0 (c (n "keyhelper") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0lb1iabbzyfcab3wbvqqsfwmn4b3ymnjkdk22p8d8kqy16gvdcgr") (y #t)))

(define-public crate-keyhelper-0.2.1 (c (n "keyhelper") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "1n5qanrf2yz9zz0hmb114d0g5ihgphghkk8r2l12mzyk0416fxq7")))

