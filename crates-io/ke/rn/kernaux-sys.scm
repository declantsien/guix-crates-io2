(define-module (crates-io ke rn kernaux-sys) #:use-module (crates-io))

(define-public crate-kernaux-sys-0.0.0 (c (n "kernaux-sys") (v "0.0.0") (d (list (d (n "libc") (r "^0.2.113") (d #t) (k 0)))) (h "1pvd68i2dlnjk4xxv8wkrmblxgq1d4ixhn9yp0d03nm2rskl92b5") (y #t)))

(define-public crate-kernaux-sys-0.1.0 (c (n "kernaux-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.113") (d #t) (k 0)))) (h "1nlgivsaj3j6m9b5nms1rjgwr5q0qamy33v21qjp0r3smlicx0hi") (y #t)))

(define-public crate-kernaux-sys-0.2.0 (c (n "kernaux-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.113") (f (quote ("extra_traits"))) (k 0)))) (h "1828m3ng4hmazfn707f97mfliy6nipz7jflpg8r1aqy4sq3h58zh") (y #t)))

(define-public crate-kernaux-sys-0.3.0 (c (n "kernaux-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.113") (f (quote ("extra_traits"))) (k 0)))) (h "098ajvqf9nihikixly5gzir3fg8mqz8gyprh7wkiis6c7lwd1p8z") (y #t)))

(define-public crate-kernaux-sys-0.4.0 (c (n "kernaux-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.113") (f (quote ("extra_traits"))) (k 0)))) (h "1vsh5qdqjxvrjjlvpgdmjvj7pnkzla6r0ys2abvg64v1z38n63gf") (f (quote (("ntoa") ("default" "cmdline" "ntoa") ("cmdline")))) (y #t)))

(define-public crate-kernaux-sys-0.5.0 (c (n "kernaux-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.113") (f (quote ("extra_traits"))) (k 0)))) (h "0sllpqlgybggvr43jgxa9r978adm9ak6fzprlvhy3f7g1cdjyswq") (f (quote (("ntoa") ("default" "cmdline" "ntoa") ("cmdline"))))))

(define-public crate-kernaux-sys-0.6.0 (c (n "kernaux-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.113") (f (quote ("extra_traits"))) (k 0)))) (h "0xqls90cy6klhcpfm9rwbs3lv0drnpc39czykv7q27wx42cf1nac") (f (quote (("ntoa") ("default" "cmdline" "ntoa") ("cmdline"))))))

(define-public crate-kernaux-sys-0.6.1 (c (n "kernaux-sys") (v "0.6.1") (d (list (d (n "libc") (r "^0.2.113") (f (quote ("extra_traits"))) (k 0)))) (h "0j06qv1pzac6gy1kfdai48vgswd8zjfh5w24lj740n25v3c205gj") (f (quote (("ntoa") ("default" "cmdline" "ntoa") ("cmdline"))))))

(define-public crate-kernaux-sys-0.7.0 (c (n "kernaux-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.113") (f (quote ("extra_traits"))) (k 0)))) (h "0fk9vzb06h7x57rpkdrya6n72wf8rm1w4p6zhaj39ykhwa3irm0m") (f (quote (("ntoa") ("default" "cmdline" "ntoa") ("cmdline"))))))

