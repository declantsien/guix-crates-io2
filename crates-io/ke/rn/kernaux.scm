(define-module (crates-io ke rn kernaux) #:use-module (crates-io))

(define-public crate-kernaux-0.0.0 (c (n "kernaux") (v "0.0.0") (h "1byj84rllv2501rwjvp35y5znmx63l0fz62qj81zj328xyrs6phv") (y #t)))

(define-public crate-kernaux-0.1.0 (c (n "kernaux") (v "0.1.0") (h "1m4k94yja91pqpwrpb67kd73j75h1q4xb5r6by7hjdsx9wcqa6sr") (y #t)))

(define-public crate-kernaux-0.2.0 (c (n "kernaux") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.113") (d #t) (k 0)))) (h "09rd2dpwfh93lyga2bjbipsz3b4ddcvx51a92n6rp5bz23r0qzww") (y #t)))

(define-public crate-kernaux-0.3.0 (c (n "kernaux") (v "0.3.0") (d (list (d (n "kernaux-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0k4k8rzvc1yy2r4k55gsl9qrj2baqz4vlbi19qmnppalm1c5lkjn") (y #t)))

(define-public crate-kernaux-0.4.0 (c (n "kernaux") (v "0.4.0") (d (list (d (n "ctor") (r "^0.1.22") (d #t) (k 0)) (d (n "kernaux-sys") (r "^0.4.0") (k 0)) (d (n "libc") (r "^0.2.113") (k 0)))) (h "1cnmpfk22y8gw2hj6khly749rm7d31h0bc6dk24n71wgclnxf9zb") (f (quote (("ntoa" "kernaux-sys/ntoa") ("default" "cmdline" "ntoa") ("cmdline" "kernaux-sys/cmdline")))) (y #t)))

(define-public crate-kernaux-0.5.0 (c (n "kernaux") (v "0.5.0") (d (list (d (n "ctor") (r "^0.1.22") (d #t) (k 0)) (d (n "kernaux-sys") (r "^0.5.0") (k 0)) (d (n "libc") (r "^0.2.113") (k 0)))) (h "1z53cm8rfwhwysxr7qi3vr50a15dd1h3cmi3j6bkg2cnqg9hxg60") (f (quote (("ntoa" "kernaux-sys/ntoa") ("default" "cmdline" "ntoa") ("cmdline" "kernaux-sys/cmdline"))))))

(define-public crate-kernaux-0.6.0 (c (n "kernaux") (v "0.6.0") (d (list (d (n "ctor") (r "^0.1.22") (d #t) (k 0)) (d (n "kernaux-sys") (r "^0.6.0") (k 0)) (d (n "libc") (r "^0.2.113") (k 0)))) (h "0z27iz34lk57q2savi2mchz0310vq0dbdsbjxg6gbraaahgj8d17") (f (quote (("ntoa" "kernaux-sys/ntoa") ("default" "cmdline" "ntoa") ("cmdline" "kernaux-sys/cmdline"))))))

(define-public crate-kernaux-0.6.1 (c (n "kernaux") (v "0.6.1") (d (list (d (n "ctor") (r "^0.1.22") (d #t) (k 0)) (d (n "kernaux-sys") (r "^0.6.1") (k 0)) (d (n "libc") (r "^0.2.113") (k 0)))) (h "125ss3420ywl07cgcd6z7zigy44v0a44ngwzjz2yb4nk0jfz2c73") (f (quote (("ntoa" "kernaux-sys/ntoa") ("default" "cmdline" "ntoa") ("cmdline" "kernaux-sys/cmdline"))))))

(define-public crate-kernaux-0.7.0 (c (n "kernaux") (v "0.7.0") (d (list (d (n "ctor") (r "^0.1.22") (d #t) (k 0)) (d (n "kernaux-sys") (r "^0.7.0") (k 0)) (d (n "libc") (r "^0.2.113") (k 0)))) (h "1k6gssb3b8vms4qrdr8vivhrm7y4vk29gql296pcjyp3jbnxfm5b") (f (quote (("ntoa" "kernaux-sys/ntoa") ("default" "cmdline" "ntoa") ("cmdline" "kernaux-sys/cmdline"))))))

