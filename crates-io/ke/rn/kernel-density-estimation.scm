(define-module (crates-io ke rn kernel-density-estimation) #:use-module (crates-io))

(define-public crate-kernel-density-estimation-0.1.0 (c (n "kernel-density-estimation") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)) (d (n "plotly") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0251197w5jnjmx1wcs1v7342fnicy98szi0qj7d264ph5i4vf02n") (f (quote (("f64"))))))

(define-public crate-kernel-density-estimation-0.2.0 (c (n "kernel-density-estimation") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fastrand") (r "^1.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotly") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "137yfx954sy3jwmn0g4va30m5arb646fr04cyfd1rvfy4l9gnxcc")))

