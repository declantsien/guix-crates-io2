(define-module (crates-io ke rn kernal) #:use-module (crates-io))

(define-public crate-kernal-0.1.0 (c (n "kernal") (v "0.1.0") (h "197yr9wgiwa72mpww115haq5wqfv1jh96s44sv0sm68wbkwbdg42")))

(define-public crate-kernal-0.2.0 (c (n "kernal") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0q2s7vnwlns4x4rkpvdwizqkfz9h7m4ai6h8d6sg5dpaxngxxfmb")))

(define-public crate-kernal-0.3.0 (c (n "kernal") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0c58kclnshgw15z419km4x2ap5nm4kam758cfm8pgjlf866zci1r")))

