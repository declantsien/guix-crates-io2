(define-module (crates-io ke rn kernel-build) #:use-module (crates-io))

(define-public crate-kernel-build-0.1.0 (c (n "kernel-build") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 1)) (d (n "winreg") (r "^0.10.1") (d #t) (k 1)))) (h "11q8znv4i4yw1phwh3kcx75zap84csvd1kgwizjvi4s1x94xngm4")))

