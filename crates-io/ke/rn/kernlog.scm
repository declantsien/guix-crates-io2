(define-module (crates-io ke rn kernlog) #:use-module (crates-io))

(define-public crate-kernlog-0.1.0 (c (n "kernlog") (v "0.1.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0ir02109xlm6x3pjnyxzix0ljr6sxnib5vlx84lk0w4f8w5anhyf") (f (quote (("nightly"))))))

(define-public crate-kernlog-0.1.1 (c (n "kernlog") (v "0.1.1") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1iij8ywdcagrd9bq8ysr3k3ja1dsms0y7wrf2ag0jnlqbjnzjz83") (f (quote (("nightly"))))))

(define-public crate-kernlog-0.1.2 (c (n "kernlog") (v "0.1.2") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0g6s6crzpqpgw385zfd4kcd9rdbpfmzj82q3hhyb7qkl4z961f8g") (f (quote (("nightly"))))))

(define-public crate-kernlog-0.2.0 (c (n "kernlog") (v "0.2.0") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1rihymd7jak81k7gl6sar7ac1ja71c6vf45ah4q94zs21zgzqdmj") (f (quote (("nightly"))))))

(define-public crate-kernlog-0.2.1 (c (n "kernlog") (v "0.2.1") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0j1crn4xnmpcrvvxwylzxh0g47k3c6dzrc4fn0mnw08fz0qb966r") (f (quote (("nightly"))))))

(define-public crate-kernlog-0.3.0 (c (n "kernlog") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0i0x01274rbb1mkzpx9fy4lsvvhis3i6al2qg8630gwihbz93djb")))

(define-public crate-kernlog-0.3.1 (c (n "kernlog") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "05vd5v8p0s1wzgixadcxjcxp4vgdqajgp1smpj9pra86xav7kpgh")))

