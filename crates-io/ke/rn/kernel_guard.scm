(define-module (crates-io ke rn kernel_guard) #:use-module (crates-io))

(define-public crate-kernel_guard-0.1.0 (c (n "kernel_guard") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "crate_interface") (r "^0.1") (d #t) (k 0)))) (h "1p5gd0p27abpdbc6f1fmrh6556w41j4p5lwsb22g53ink21cqf2n") (f (quote (("preempt") ("default"))))))

