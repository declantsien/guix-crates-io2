(define-module (crates-io ke rn kern) #:use-module (crates-io))

(define-public crate-kern-0.1.0 (c (n "kern") (v "0.1.0") (h "0d94hgwvyaqf7g9agmy42wikkcd2mjd26mvyi4g0vkzq681wrgjf")))

(define-public crate-kern-0.1.1 (c (n "kern") (v "0.1.1") (h "13d3s0pravxf39d3jvwflv5qqvy7l7w5rz37jy34r122hy7l5hj7")))

(define-public crate-kern-0.2.0 (c (n "kern") (v "0.2.0") (h "061b6kws7kg88yci0bwsgprkiknjzqbi33wnpmdjard3wh2znf1y")))

(define-public crate-kern-0.2.1 (c (n "kern") (v "0.2.1") (h "1qyc16zj3b5rmqyfklmfd8xdpjwymybqqlnffz9v2kyfl7j6lp9h")))

(define-public crate-kern-0.2.2 (c (n "kern") (v "0.2.2") (h "08h3kyy6khdbkrdykqlm79l30c3bwbblmz4bja11gv1hzmwmlvfs")))

(define-public crate-kern-0.3.0 (c (n "kern") (v "0.3.0") (h "0dc4kd4pizakamr3fkjiny1grl845747w4lbr1bj9yhfykyzwa80")))

(define-public crate-kern-0.3.1 (c (n "kern") (v "0.3.1") (h "1si78gvxjdqkkqvks6lf3f7rvrmjpq6qwvw1lf8dvrlqlyynz03k")))

(define-public crate-kern-0.3.2 (c (n "kern") (v "0.3.2") (h "1390h89chvzda60rg3z1bycvxnyxj40iwws8m73pzx669gh3qvs3")))

(define-public crate-kern-0.4.0 (c (n "kern") (v "0.4.0") (h "0395zivcihaj62c5z596m75i9kmj9lw1s82484543zz240wgrwws")))

(define-public crate-kern-1.0.0 (c (n "kern") (v "1.0.0") (h "0qn3pi04g00620ylcdmjrwx5bhhsjkn799ljq5r5wgq2lbwif304")))

(define-public crate-kern-1.0.1 (c (n "kern") (v "1.0.1") (h "0z5gidzkjfc3bbghlgzzkqla0ngljygd1jw6qf83zb5am5n785i6")))

(define-public crate-kern-1.0.2 (c (n "kern") (v "1.0.2") (h "1h788l9dyl2yjdmjk9xi0cl3bmpbpgm7hkfag9bac8jdciakh04x")))

(define-public crate-kern-1.0.3 (c (n "kern") (v "1.0.3") (h "0g03nnjkp06q0rnfrzkpq8srm3n6md93i2i62l0d6856c7nzbxin")))

(define-public crate-kern-1.1.0 (c (n "kern") (v "1.1.0") (h "1l8zr8cp8j0gk1dklzqdpdjv9jlxc4m853gm4whs81dl6yx5q0g2")))

(define-public crate-kern-1.1.1 (c (n "kern") (v "1.1.1") (h "14vgd6wp7hnm4vvkdh6bv0qrg64h60xjywng56wkl199a65qhkzc")))

(define-public crate-kern-1.1.2 (c (n "kern") (v "1.1.2") (h "133yqjj0pvbxpdhkanng5lzdkk1a6rs3wnrav630rm6yw3igs6h8")))

(define-public crate-kern-1.1.3 (c (n "kern") (v "1.1.3") (h "0ywa1ixs5v9ybzbm8zqdi4sbjqg7ffdbjivjng8i0x8l2hmym8lf")))

(define-public crate-kern-1.1.4 (c (n "kern") (v "1.1.4") (h "1xdzc5anfz416qy3yyjdjizgnmn1gb0gnk7gmcj1qdrkkh8sq67x")))

(define-public crate-kern-1.1.5 (c (n "kern") (v "1.1.5") (h "10svfv9hyy15vs3mrvi4jc3zhydjjhq8ap900x56frcwackmyh8i")))

(define-public crate-kern-1.1.6 (c (n "kern") (v "1.1.6") (h "0v9lj1ma7wzmxyryifg4d6128rbklx9gwprlnd441gcmad32j493")))

(define-public crate-kern-1.2.0 (c (n "kern") (v "1.2.0") (d (list (d (n "rustls") (r "^0.18.1") (o #t) (d #t) (k 0)))) (h "198wk4k2isp4g8gnnqlmqp8jmammlqzc795iajzdd4bfc2rjai8m") (f (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1.2.1 (c (n "kern") (v "1.2.1") (d (list (d (n "rustls") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "0vv5m5dc4q1g4wbs11sf1ldyy6sx9vwr8irx87apg5d1icydyhy3") (f (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1.2.2 (c (n "kern") (v "1.2.2") (d (list (d (n "rustls") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "17ql2cn8fzh0fc3mhd8z9rk3zgsdaib23zg1lrnacnnc89qfznfw") (f (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1.2.3 (c (n "kern") (v "1.2.3") (d (list (d (n "rustls") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "04lv57zgi0ks9mdisy4v5qsr12b9w77x1x77jf7fzfrd380f1135") (f (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1.3.0 (c (n "kern") (v "1.3.0") (d (list (d (n "rustls") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "17yzj8rk32bmrfqdzqw35arphy0d4dcb5j18kf1m95m6rmsbalqb") (f (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1.3.1 (c (n "kern") (v "1.3.1") (d (list (d (n "rustls") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "1h9sl50wsr73283pw43cbz1d8am0fqinzrz6hbp9md7xspba418i") (f (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1.3.2 (c (n "kern") (v "1.3.2") (d (list (d (n "rustls") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "0kxn7y9nmr8hfs29gaiw7vdw5x65gvzls41laj1r8c0gpvvqr2bv") (f (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1.3.3 (c (n "kern") (v "1.3.3") (d (list (d (n "rustls") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "1bvv0q5hqzv9lv3x227k06jvx9bv3knb52c18s5rjii5fbrm9cnp") (f (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1.3.4 (c (n "kern") (v "1.3.4") (d (list (d (n "rustls") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1rzn86dap2kyqdjj2riz6gikd6dsvl0y6mqmcfxg6w19gp56fsz5") (f (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1.4.0 (c (n "kern") (v "1.4.0") (d (list (d (n "rustls") (r "^0.20.2") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0vdlmq2xmb1m76jc992jgrw5lkx1nzphsz94ggpzzd8q500vxwm9") (f (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1.4.1 (c (n "kern") (v "1.4.1") (d (list (d (n "rustls") (r "^0.20.6") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0szlqy8fx5mr02qwba5j323cq8dk1sq864b6w5mp119skspx7sha") (f (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1.5.0 (c (n "kern") (v "1.5.0") (d (list (d (n "rustls") (r "^0.20.6") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0lm0pvw072h24jrdyk53a8fhi0h98k28iic2k70v68cr8ac6dnl7") (f (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1.5.1 (c (n "kern") (v "1.5.1") (d (list (d (n "rustls") (r "^0.20.6") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "18jpl0an4h6smzkzah55h9c1p722hb9p19pp7wh09nds281bniqp") (f (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1.6.0 (c (n "kern") (v "1.6.0") (d (list (d (n "rustls") (r "^0.21.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "1429ia49f8jjks0ikqahiyznz9rnq1l1rlhmkyrzavxmwl3pfbsb") (f (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1.6.1 (c (n "kern") (v "1.6.1") (d (list (d (n "rustls") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "rustls-pki-types") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1cqv0r4qss346bbdhfxsxyqw2fwjvlh1r4gy2pwpjb2jkd3fyg3m") (f (quote (("http" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kern-1.6.2 (c (n "kern") (v "1.6.2") (d (list (d (n "rustls") (r "^0.22.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "rustls-pki-types") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "0px2v63ipnxbpmnf5x179q8aq6fhjh9lb1j98gyh1p8x6mc5738n") (f (quote (("http" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kern-1.6.3 (c (n "kern") (v "1.6.3") (d (list (d (n "rustls") (r "^0.22.2") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "rustls-pki-types") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0y50752xsswrw3mr3v1gqgyrz0wlryamg4pac716641js0mqc3j3") (f (quote (("http" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kern-1.6.4 (c (n "kern") (v "1.6.4") (d (list (d (n "rustls") (r "^0.22.2") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "rustls-pki-types") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0p85l453w0maa57ihvpcq4md7fdy3s4s5inhj04iwqibq11rfgnd") (f (quote (("http" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kern-1.7.0 (c (n "kern") (v "1.7.0") (d (list (d (n "rustls") (r "^0.22.2") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "rustls-pki-types") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "072169mlw41fn1fjrfhibw2dks57aac50qn2lsxaph5yywxc2nj6") (f (quote (("tls" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kern-1.7.1 (c (n "kern") (v "1.7.1") (d (list (d (n "rustls") (r "^0.23.1") (o #t) (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2.1.1") (o #t) (d #t) (k 0)) (d (n "rustls-pki-types") (r "^1.3.1") (o #t) (d #t) (k 0)))) (h "0p58ccb4vrfvlzrbzn5qjjm6khmkw681lclm7s0rddrkqc5xcr6j") (f (quote (("tls" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

