(define-module (crates-io ke rn kernel-asound-sys) #:use-module (crates-io))

(define-public crate-kernel-asound-sys-0.1.1 (c (n "kernel-asound-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "17q1fljb69rz1lx3rfhh86pgyr2pnmwhs16nd17kfcjqhvp8wqw6")))

(define-public crate-kernel-asound-sys-0.1.2 (c (n "kernel-asound-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0qvbsbnmg7qjgzjfrmnci0fa97s3zh2y8dz932n4pnkhlyly8473")))

(define-public crate-kernel-asound-sys-0.2.0 (c (n "kernel-asound-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "ioctl-sys") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0xd91949krpdyb7wp6lry9lmvqssxlw5jz4gzgms4j4783jjh8gy")))

(define-public crate-kernel-asound-sys-0.2.1 (c (n "kernel-asound-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "ioctl-sys") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1si985j511iy9j5chqb0g188zw9x2fp7mcivkikvdgb6f06iqk51")))

(define-public crate-kernel-asound-sys-0.3.0 (c (n "kernel-asound-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "ioctl-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0favp1by09jrf132d3h5ain35gwvb06scdv6yh733i2w0y7i7cif")))

