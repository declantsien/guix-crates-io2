(define-module (crates-io ke rn kernel_web) #:use-module (crates-io))

(define-public crate-kernel_web-0.1.0 (c (n "kernel_web") (v "0.1.0") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)))) (h "1s2x806yzcp72zaagxbn1kabkj6rlvcvh1k8mmag0cdjdbxl00r8")))

(define-public crate-kernel_web-0.1.1 (c (n "kernel_web") (v "0.1.1") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)))) (h "1zb6nswaald1lq9hyxnhg1ciflnm7pxp6zf4f8mk99hsyjim57nj")))

(define-public crate-kernel_web-0.1.2 (c (n "kernel_web") (v "0.1.2") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)))) (h "05qawdndjfbfaa2xx72iw5vz9169gf1jpl6zxa04dhi2wam917gj")))

(define-public crate-kernel_web-0.1.4 (c (n "kernel_web") (v "0.1.4") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)))) (h "0gmbh6wm7r0l995kpjlgdrf7ar1akxvl3vp5mzc44yj1mv5ycgl8")))

(define-public crate-kernel_web-0.1.5 (c (n "kernel_web") (v "0.1.5") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)))) (h "1zwzddrqdqdn3nfk4r84lf401cx2y47lv2ldmi2a1sq9cl90j1cx")))

(define-public crate-kernel_web-0.1.6 (c (n "kernel_web") (v "0.1.6") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)))) (h "0dwir8nyplfir6hlykbg8ksabaywh95fyb92rqxhsw7hiqpp02xk")))

(define-public crate-kernel_web-0.1.7 (c (n "kernel_web") (v "0.1.7") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)))) (h "1krl73jrc9ilj8lz0x2p7zq21clm145vfvbvdqv7f7kpkpbvb772")))

(define-public crate-kernel_web-0.1.8 (c (n "kernel_web") (v "0.1.8") (d (list (d (n "colorful") (r "^0.2.2") (d #t) (k 0)))) (h "114xd6b2kcjfckqhvjffkbsad7zcgs5a32rxabpmizsh7q8abxjb")))

