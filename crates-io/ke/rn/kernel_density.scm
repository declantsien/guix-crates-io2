(define-module (crates-io ke rn kernel_density) #:use-module (crates-io))

(define-public crate-kernel_density-0.0.1 (c (n "kernel_density") (v "0.0.1") (d (list (d (n "ordered-float") (r "^0.2.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "1cd1s8i86lk2j7xg3wkpljcjx2rwchydp7b6dzqmfpxmdfyf8l26")))

