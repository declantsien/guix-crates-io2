(define-module (crates-io ke #{04}# ke04z4-pac) #:use-module (crates-io))

(define-public crate-ke04z4-pac-0.1.0 (c (n "ke04z4-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "16v9343mjh3yk0f1108vmxxsh567ckv8m9q1kxr67jdngjxd8ysz") (f (quote (("rt" "cortex-m-rt/device"))))))

