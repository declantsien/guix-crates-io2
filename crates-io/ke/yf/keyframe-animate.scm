(define-module (crates-io ke yf keyframe-animate) #:use-module (crates-io))

(define-public crate-keyframe-animate-0.1.0 (c (n "keyframe-animate") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "bevy") (r "^0.7") (d #t) (k 2)))) (h "118528nzjpxp4vp61bj1plnc4100n2c50n4iccvlamwx93wj2n9f") (f (quote (("ui" "bevy/bevy_ui") ("sprite" "bevy/bevy_sprite") ("render" "bevy/bevy_render") ("examples" "ui" "sprite" "render" "bevy/render" "bevy/bevy_winit" "bevy/x11") ("default" "render" "sprite" "ui"))))))

(define-public crate-keyframe-animate-0.1.1 (c (n "keyframe-animate") (v "0.1.1") (d (list (d (n "bevy") (r "^0.7") (k 0)) (d (n "bevy") (r "^0.7") (d #t) (k 2)) (d (n "interpolation") (r "^0.2") (d #t) (k 2)))) (h "198ky8byqam1b57bi4zb1yvl05n68krc21mpigfm1l4ym3kr254r") (f (quote (("ui" "bevy/bevy_ui") ("sprite" "bevy/bevy_sprite") ("render" "bevy/bevy_render") ("examples" "ui" "sprite" "render" "bevy/render" "bevy/bevy_winit" "bevy/x11") ("default" "render" "sprite" "ui"))))))

