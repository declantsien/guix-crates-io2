(define-module (crates-io ke yf keyfn) #:use-module (crates-io))

(define-public crate-keyfn-0.1.0 (c (n "keyfn") (v "0.1.0") (d (list (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "13w46lx64hrd48q1i455wsn5s3i72q0jm2i3j0hv1r82pyj456jm")))

(define-public crate-keyfn-0.2.0 (c (n "keyfn") (v "0.2.0") (d (list (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "03242lg0xvgn7pxnwqahqlyl6rkd6iykpm38diizb68wqsabrczy")))

(define-public crate-keyfn-0.2.1 (c (n "keyfn") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "18j07jd6i7lbbw921h0ids22ds339wywq6hbsj12dgr7lp9r3ha2")))

