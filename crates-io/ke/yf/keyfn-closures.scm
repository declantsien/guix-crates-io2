(define-module (crates-io ke yf keyfn-closures) #:use-module (crates-io))

(define-public crate-keyfn-closures-0.2.1 (c (n "keyfn-closures") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0ws1cv39jbca4iap936fnp7sx2sxahv8nwna8m6y8sl7ccl373cr")))

