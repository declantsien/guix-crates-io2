(define-module (crates-io ke yf keyframe) #:use-module (crates-io))

(define-public crate-keyframe-1.0.0 (c (n "keyframe") (v "1.0.0") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 2)) (d (n "mint") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "09z2qnqrh8mv0qz7pjkppqkx4ii049v1g3f9zpphmr4mr1p4gh0a") (f (quote (("mint_types" "mint") ("default" "mint_types")))) (y #t)))

(define-public crate-keyframe-1.0.1 (c (n "keyframe") (v "1.0.1") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 2)) (d (n "mint") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "0f4a5bnxs6ma7vb1v29z620brycbv8inxhav5qf4z5h0643q4bf5") (f (quote (("mint_types" "mint") ("default" "mint_types"))))))

(define-public crate-keyframe-1.0.2 (c (n "keyframe") (v "1.0.2") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 2)) (d (n "mint") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "1fyb631b9ygqix7d3rlwmsgdczxqjq78dyxkaydbmlp5qj7vy3ly") (f (quote (("mint_types" "mint") ("default" "mint_types"))))))

(define-public crate-keyframe-1.0.3 (c (n "keyframe") (v "1.0.3") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 2)) (d (n "mint") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "0dnz3irjmlqhryd4ghb1vvv6yaya94fzljgdia1rk80vpangjl2h") (f (quote (("mint_types" "mint") ("default" "mint_types"))))))

(define-public crate-keyframe-1.0.4 (c (n "keyframe") (v "1.0.4") (d (list (d (n "ggez") (r "^0.6.0") (d #t) (k 2)) (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "02sadi4kb5l8ckmvk7aw2vyc5jrxjqiwfzqd5h5r4s37r5s2ph53") (f (quote (("mint_types" "mint") ("default" "mint_types"))))))

(define-public crate-keyframe-1.1.0 (c (n "keyframe") (v "1.1.0") (d (list (d (n "ggez") (r "^0.7.0") (d #t) (k 2)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)))) (h "0n9l4a3v9rbcjw1divcqma5gp3v32qp26g7y28hh5pwx6dvqcac9") (f (quote (("mint_types" "mint") ("default" "mint_types" "alloc") ("alloc")))) (r "1.51")))

(define-public crate-keyframe-1.1.1 (c (n "keyframe") (v "1.1.1") (d (list (d (n "ggez") (r "^0.7.0") (d #t) (k 2)) (d (n "mint") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)))) (h "1afr5ffns3k79xaqnw6rw3qn8sngwly6gxfnjn8d060mk3vqnw30") (f (quote (("mint_types" "mint") ("default" "mint_types" "alloc") ("alloc")))) (r "1.51")))

