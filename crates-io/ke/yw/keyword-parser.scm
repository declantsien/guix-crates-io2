(define-module (crates-io ke yw keyword-parser) #:use-module (crates-io))

(define-public crate-keyword-parser-0.0.1 (c (n "keyword-parser") (v "0.0.1") (d (list (d (n "combine") (r "^4") (k 0)) (d (n "combine") (r "^4") (d #t) (k 2)) (d (n "const-array-attrs") (r "^0.0") (d #t) (k 2)) (d (n "transition-table") (r "^0.0") (d #t) (k 0)))) (h "1rwc5d5zqx7w266mr0xwl6w3r46l13f03wgfvyxz0m08c1c43wbp")))

(define-public crate-keyword-parser-0.0.2 (c (n "keyword-parser") (v "0.0.2") (d (list (d (n "combine") (r "^4") (k 0)) (d (n "combine") (r "^4") (d #t) (k 2)) (d (n "const-array-attrs") (r "^0.0") (d #t) (k 2)) (d (n "transition-table") (r "^0.0") (d #t) (k 0)))) (h "1avam4q1vd615kbnw5mzd8w1rrqi7j85zqhzhf9zw1nsa49ma40j")))

(define-public crate-keyword-parser-0.0.3 (c (n "keyword-parser") (v "0.0.3") (d (list (d (n "combine") (r "^4") (k 0)) (d (n "combine") (r "^4") (d #t) (k 2)) (d (n "const-array-attrs") (r "^0.0") (d #t) (k 2)) (d (n "transition-table") (r "^0.0") (d #t) (k 0)))) (h "1ljby96jy1mz7wamq0qh2v9w5b4zn84la6qg2d4x5ba40l8ix474")))

