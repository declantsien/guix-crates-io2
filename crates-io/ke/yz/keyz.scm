(define-module (crates-io ke yz keyz) #:use-module (crates-io))

(define-public crate-keyz-1.0.2 (c (n "keyz") (v "1.0.2") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "03xmp64yczanrygzbqgkyz3qzfnspfz286d6g6a48as63hywfx5k")))

(define-public crate-keyz-1.0.3 (c (n "keyz") (v "1.0.3") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "10q2n339bqqxhq43q09a9aiqknhzzwpl0ds5kmraszz7yf3b0dcs")))

(define-public crate-keyz-1.0.4 (c (n "keyz") (v "1.0.4") (d (list (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "05f7955qzbd5gzsv00v5gmdf1bvqj1h8y3xmp6xzvqhjz12kmyi6")))

