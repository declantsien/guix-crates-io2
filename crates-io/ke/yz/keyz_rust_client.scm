(define-module (crates-io ke yz keyz_rust_client) #:use-module (crates-io))

(define-public crate-keyz_rust_client-1.0.0 (c (n "keyz_rust_client") (v "1.0.0") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "1d5iwy1dldb7fhzpz41xfvnc6kw9f0zlpsy314nv8x3hzw27igsd")))

(define-public crate-keyz_rust_client-1.0.1 (c (n "keyz_rust_client") (v "1.0.1") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "0fg56alfiz8s6fcbxrni0fddfqb5s8b8jrinbp0ysgk733kqhjdb")))

(define-public crate-keyz_rust_client-1.0.2 (c (n "keyz_rust_client") (v "1.0.2") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "16ly29s8ilmji6gykz3g9yy5qpfc2z6blfsmpqx4sx69h3ri61kd")))

(define-public crate-keyz_rust_client-1.0.3 (c (n "keyz_rust_client") (v "1.0.3") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "025c1bqskfgyh7j4kq0amrx4625py4xmwlhcj8di36ncy02x4iwl")))

(define-public crate-keyz_rust_client-1.0.4 (c (n "keyz_rust_client") (v "1.0.4") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "19d14n7idn5gxrz53yqnq8mfgpa4aqdwhlf7yim7qw5kns0ck96c")))

(define-public crate-keyz_rust_client-1.0.5 (c (n "keyz_rust_client") (v "1.0.5") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "1y2pazra8f684hmmigmlqh5w06qqvhpgvky1l2szzm3m9kdf2977")))

