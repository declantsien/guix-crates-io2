(define-module (crates-io ke yz keyzcli) #:use-module (crates-io))

(define-public crate-keyzcli-0.1.0 (c (n "keyzcli") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "0x52zzwxg01annpiq1k3b83rf033il2qc9riqj2sz5968qs7f2gm")))

(define-public crate-keyzcli-0.1.1 (c (n "keyzcli") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "18pj406cl7wd3vdpnq4chhjk4zjh9avmkdp57l51bda1pcb18qp5")))

(define-public crate-keyzcli-0.1.2 (c (n "keyzcli") (v "0.1.2") (d (list (d (n "ctrlc") (r "^3.2.4") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full" "time"))) (d #t) (k 0)))) (h "067mc9h16msppfwsy9lckciwzc0k4akiv5cvhpfjznwkkyl078fx")))

