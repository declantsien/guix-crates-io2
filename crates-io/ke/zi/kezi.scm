(define-module (crates-io ke zi kezi) #:use-module (crates-io))

(define-public crate-kezi-0.1.0 (c (n "kezi") (v "0.1.0") (h "14rpnh5iwnlvpf85lwrn683f3143jdgrdlgkpv9yf6bz1bqh5kcp")))

(define-public crate-kezi-0.6.9 (c (n "kezi") (v "0.6.9") (h "0cnaxgbnzxw6rmvm2r354cv65bka9yn68870brf7ccqcc5yi3dk2")))

