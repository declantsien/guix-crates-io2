(define-module (crates-io ke yb keybase-chat-notifier) #:use-module (crates-io))

(define-public crate-keybase-chat-notifier-0.1.0 (c (n "keybase-chat-notifier") (v "0.1.0") (d (list (d (n "keybase-protocol") (r "^0.1.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4.0.0-alpha.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1djgnzj47bnag8sbrgvpc18dxs8z9x109bg8nf22zqbyj9fn1932")))

