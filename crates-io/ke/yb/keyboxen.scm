(define-module (crates-io ke yb keyboxen) #:use-module (crates-io))

(define-public crate-KeyBoxen-0.1.0 (c (n "KeyBoxen") (v "0.1.0") (d (list (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "signal-hook-tokio") (r "^0.3") (f (quote ("futures-v0_3"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.22") (f (quote ("io-util" "macros" "process" "rt" "rt-multi-thread" "tokio-macros"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "zbus") (r "^3.6") (d #t) (k 0)))) (h "1w3p9cmpfvs2pqzw33kvps9qj3d35sv7zry5h8cq6ls4bc3vg773")))

