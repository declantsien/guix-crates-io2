(define-module (crates-io ke yb keyboard-types) #:use-module (crates-io))

(define-public crate-keyboard-types-0.1.0 (c (n "keyboard-types") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04g9zpwn85kzvln34xf7clpw5vkl1s95y72d9snnn5d6apw1jrf3")))

(define-public crate-keyboard-types-0.1.1 (c (n "keyboard-types") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s25cz0pnwlkbxcbbgrb6pirvshyq0lg6ivxlq17bam907r22wqa")))

(define-public crate-keyboard-types-0.1.2 (c (n "keyboard-types") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qcb7bfl6mrf5midhd17mmyb2b1zynqhbiww8im00zxy3bjylm07")))

(define-public crate-keyboard-types-0.2.0 (c (n "keyboard-types") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nr6l483bj43wxx9wk0kkfjbmb2gb8mkq045my6kzlilqdfh5xah")))

(define-public crate-keyboard-types-0.3.0 (c (n "keyboard-types") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "heapsize") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0azjhk6agmvm2ycmysnjwdmwxvbk1r8kaz2k3xj0d02ijmz2n9cy") (f (quote (("heap_size" "heapsize"))))))

(define-public crate-keyboard-types-0.4.0-servo (c (n "keyboard-types") (v "0.4.0-servo") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wni11078jdbpwzlj60948qrwiwai6wn0w8pxyksha9yr73m9zab")))

(define-public crate-keyboard-types-0.4.1-servo (c (n "keyboard-types") (v "0.4.1-servo") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zk9s7k7yrljbs1jf6z5rzzk4zfhaw03xs4fn54fzdjbc1rgqhwh")))

(define-public crate-keyboard-types-0.4.2-servo (c (n "keyboard-types") (v "0.4.2-servo") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10mlfvbnrd5lfddbhihqb4lcv8fyvzrhpwyj68igrq3q989jq23m")))

(define-public crate-keyboard-types-0.4.3 (c (n "keyboard-types") (v "0.4.3") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "09fw0kvi6d5b8dsw84gkmj0m4wkm2c6rmrqqlcj09f01xkjz0fw2") (f (quote (("webdriver" "unicode-segmentation") ("default" "serde" "webdriver"))))))

(define-public crate-keyboard-types-0.4.4 (c (n "keyboard-types") (v "0.4.4") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "0nbrx984wgw3hqflxcld5fqqaf02cb0d1nw5bqlvpnn04bf3ddak") (f (quote (("webdriver" "unicode-segmentation") ("default" "serde" "webdriver"))))))

(define-public crate-keyboard-types-0.5.0 (c (n "keyboard-types") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "0srl4bgfdr4dnnaqac2is0dby1flzfsk9lh2yj1096r7i2naz2d9") (f (quote (("webdriver" "unicode-segmentation") ("default" "serde" "webdriver"))))))

(define-public crate-keyboard-types-0.6.0 (c (n "keyboard-types") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "0f9ds3as1frl5a8arwr25znq3f9yfyivjy8lh77w5vb90y0rm19z") (f (quote (("webdriver" "unicode-segmentation") ("default" "serde" "webdriver"))))))

(define-public crate-keyboard-types-0.6.1 (c (n "keyboard-types") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "15hkscw7f4b5iwr45bi13f64b3jp47sw2qzjh1y4bp5qkq3v2s2i") (f (quote (("webdriver" "unicode-segmentation") ("default" "serde" "webdriver"))))))

(define-public crate-keyboard-types-0.6.2 (c (n "keyboard-types") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "0s7x2a0psvz3xdbinl4ryf30f8lagk94rrnx3kk1z9gnryvnhxhb") (f (quote (("webdriver" "unicode-segmentation") ("default" "serde" "webdriver"))))))

(define-public crate-keyboard-types-0.7.0 (c (n "keyboard-types") (v "0.7.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (o #t) (d #t) (k 0)))) (h "12jjfk7dwa1cqp6wzw0xl1zzg3arsrnqy4afsynxn2csqfnxql5p") (f (quote (("webdriver" "unicode-segmentation") ("default" "serde" "webdriver")))) (s 2) (e (quote (("serde" "dep:serde" "bitflags/serde")))) (r "1.60")))

