(define-module (crates-io ke yb keyby) #:use-module (crates-io))

(define-public crate-keyby-0.1.0 (c (n "keyby") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.38") (d #t) (k 0)))) (h "06hnajmg85zz984l1hs37xpdcv5ybyfxvb2sc20s6wjazgvjk657")))

(define-public crate-keyby-0.2.0 (c (n "keyby") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.38") (d #t) (k 0)))) (h "0ailn3x0hmi6lbwciil9khlkcp47ycbcfj7h3lvr9klp3m15aaj7")))

