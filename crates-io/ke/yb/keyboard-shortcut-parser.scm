(define-module (crates-io ke yb keyboard-shortcut-parser) #:use-module (crates-io))

(define-public crate-keyboard-shortcut-parser-0.1.0 (c (n "keyboard-shortcut-parser") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11-dl") (r "^2.18.5") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0z27dk2slppql5gw2bil093fn9qznbc1yb5ypjkyhv031ymv1yd7")))

