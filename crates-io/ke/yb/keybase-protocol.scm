(define-module (crates-io ke yb keybase-protocol) #:use-module (crates-io))

(define-public crate-keybase-protocol-0.1.0 (c (n "keybase-protocol") (v "0.1.0") (d (list (d (n "avdl-serde-code-generator") (r "^0.1.0") (d #t) (k 1)) (d (n "pest") (r "^2.1.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "16xw05mm0i8wqm2xbprfnaybl8y1kpb11r4nb3csvj5bh23pdfc0")))

(define-public crate-keybase-protocol-0.1.1 (c (n "keybase-protocol") (v "0.1.1") (d (list (d (n "avdl-serde-code-generator") (r "^0.1.1") (d #t) (k 1)) (d (n "pest") (r "^2.1.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1l89wds7wc8wf8h2xd88qazw0wmi9ds1vhl8v9fwxly3bz7dsy7h")))

(define-public crate-keybase-protocol-0.2.0 (c (n "keybase-protocol") (v "0.2.0") (d (list (d (n "avdl-serde-code-generator") (r "^0.2.0") (d #t) (k 1)) (d (n "heck") (r "^0.3.1") (d #t) (k 1)) (d (n "pest") (r "^2.1.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0363200fkigqa6j5aqmpiqhmbpicjwqzydv9hxpnpbczn11qsril")))

