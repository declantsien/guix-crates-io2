(define-module (crates-io ke yb keybindings-example) #:use-module (crates-io))

(define-public crate-keybindings-example-0.2.0 (c (n "keybindings-example") (v "0.2.0") (d (list (d (n "crokey") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1sdrjkqy29j17f2j79s4c9rznplb4majpv79kkzll5vim4ndnvk4")))

