(define-module (crates-io ke yb keybee) #:use-module (crates-io))

(define-public crate-keybee-0.0.0 (c (n "keybee") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "gilrs") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "16jf0xn47h9ccg0iqc2hc3329kan1izjglkh87zv1dr851zs1413")))

