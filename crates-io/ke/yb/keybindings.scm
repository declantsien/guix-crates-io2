(define-module (crates-io ke yb keybindings) #:use-module (crates-io))

(define-public crate-keybindings-0.0.1 (c (n "keybindings") (v "0.0.1") (d (list (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "05fawj86w9j16s676b95hsnb3b8si1sc4cnsf3fj41hwr6clc3k8") (r "1.67")))

