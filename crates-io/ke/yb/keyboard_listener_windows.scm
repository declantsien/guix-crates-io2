(define-module (crates-io ke yb keyboard_listener_windows) #:use-module (crates-io))

(define-public crate-keyboard_listener_windows-0.1.0 (c (n "keyboard_listener_windows") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winuser" "errhandlingapi" "ntdef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "094hk7yq6yb31nhglyn63cs4b41gms29aa21mldrkwx10q9b419r")))

(define-public crate-keyboard_listener_windows-0.1.1 (c (n "keyboard_listener_windows") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winuser" "errhandlingapi" "ntdef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "1ahbi7w3abc1pjfchz6bl703rc97k5ax9lxbkz0gjyn425dp8lj4")))

(define-public crate-keyboard_listener_windows-0.1.2 (c (n "keyboard_listener_windows") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winuser" "errhandlingapi" "ntdef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "0q8amr3mxbn27vni2vj8cd258hv9qmnbwm8wqrmsbd6h4pbx0ckk")))

(define-public crate-keyboard_listener_windows-0.2.0-alpha.1 (c (n "keyboard_listener_windows") (v "0.2.0-alpha.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winuser" "errhandlingapi" "ntdef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "0qmsrlnhfr7yzpv846l6w1kginbq62173dv2mi66xcz8kqyf7h9n") (y #t)))

(define-public crate-keyboard_listener_windows-0.2.0-alpha.2 (c (n "keyboard_listener_windows") (v "0.2.0-alpha.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winuser" "errhandlingapi" "ntdef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "13252wdsdxf1shv87n1w97hmfxrq4j17qr1d6sdv5sq3xlp7dnmy") (y #t)))

(define-public crate-keyboard_listener_windows-0.2.0-alpha.3 (c (n "keyboard_listener_windows") (v "0.2.0-alpha.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winuser" "errhandlingapi" "ntdef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "1anv212qdkf3qf425rhgli2d1w0nssrhq1ikh4xr5v1wm1b0szcm") (y #t)))

(define-public crate-keyboard_listener_windows-0.2.0-alpha.4 (c (n "keyboard_listener_windows") (v "0.2.0-alpha.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winuser" "errhandlingapi" "ntdef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "1jz6dxjhvqxqjn5gm770jyw29lwajdby0hp7066ri8zsmnq30sr5") (y #t)))

(define-public crate-keyboard_listener_windows-0.2.0-alpha.5 (c (n "keyboard_listener_windows") (v "0.2.0-alpha.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winuser" "errhandlingapi" "ntdef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "0n53zznb1a7b27nlrr449yyss36j52rwjldlrb9hwi6kkv8gwvib") (y #t)))

(define-public crate-keyboard_listener_windows-0.2.0-alpha.6 (c (n "keyboard_listener_windows") (v "0.2.0-alpha.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winuser" "errhandlingapi" "ntdef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "0430ghy0a6ck40wf9hvwrqsb3x4q5sg3bq8br7racqbj5h602lsf") (y #t)))

(define-public crate-keyboard_listener_windows-0.2.0 (c (n "keyboard_listener_windows") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "winuser" "errhandlingapi" "ntdef" "windef" "processthreadsapi"))) (d #t) (k 0)))) (h "1ybdqn5gzb884qlriks4rmmirk32hq35rrmz2rnhx7m68x2d8c76")))

