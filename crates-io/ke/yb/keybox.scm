(define-module (crates-io ke yb keybox) #:use-module (crates-io))

(define-public crate-keybox-0.1.0 (c (n "keybox") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1sjl7f4m07xls6zz76rgrm0lh5nkw758465arrxiyr9hhg3kj78s")))

(define-public crate-keybox-0.2.0 (c (n "keybox") (v "0.2.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1n9p7dr8dfka4iwhi9vdblkdy5pdzbgngx2c24b4jjnykafrklpk")))

