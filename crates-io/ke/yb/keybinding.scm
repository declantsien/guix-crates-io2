(define-module (crates-io ke yb keybinding) #:use-module (crates-io))

(define-public crate-keybinding-0.1.0 (c (n "keybinding") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "termion") (r "^2.0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "termwiz") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "16j3c5xswxkpn3k6aivv4p1f2nf5dyzsiwpg8my0gp7w1pml79ki")))

