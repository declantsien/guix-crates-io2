(define-module (crates-io ke yb keyberon) #:use-module (crates-io))

(define-public crate-keyberon-0.1.0 (c (n "keyberon") (v "0.1.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.5") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.0") (d #t) (k 0)))) (h "1s10llsng0qix12fw0nd0lf7577dl27asw9qyigc3skr1bg3r17m")))

(define-public crate-keyberon-0.1.1 (c (n "keyberon") (v "0.1.1") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "either") (r "^1.5") (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.0") (d #t) (k 0)))) (h "128lw52aphk3bjj63y69pnqn7ylk334rq9h4kxamnbaidjxn1kjc")))

