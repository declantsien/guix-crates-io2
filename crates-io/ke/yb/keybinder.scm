(define-module (crates-io ke yb keybinder) #:use-module (crates-io))

(define-public crate-keybinder-0.1.0 (c (n "keybinder") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11vsq6z5fm3kpx81aqm3w7vsr432gbh3xwd8aqkvwan9r6qv7179")))

(define-public crate-keybinder-0.2.0 (c (n "keybinder") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ryx9r0jy606ilsxgm7mi659ri4c3f8m2rppshv83zqsh9mmbkm6")))

(define-public crate-keybinder-0.3.0 (c (n "keybinder") (v "0.3.0") (d (list (d (n "gtk") (r "^0.15.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "120xd1nrx4zd0csnlzcwpvadjjrh3lb8m3qvx17gzh9lnxlkbhvv")))

(define-public crate-keybinder-0.4.0 (c (n "keybinder") (v "0.4.0") (d (list (d (n "gtk") (r "^0.15.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hk8h9n7dv7xv847gk6651b3h5a0lngxp5b7nq21wh563lr40w3q")))

(define-public crate-keybinder-0.4.1 (c (n "keybinder") (v "0.4.1") (d (list (d (n "gtk") (r "^0.15.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f9a1s5axgw35dcyk7xsb08nai1mkhzbgfyznf71xn43iw717ax0")))

