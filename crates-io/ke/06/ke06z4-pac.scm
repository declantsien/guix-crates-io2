(define-module (crates-io ke #{06}# ke06z4-pac) #:use-module (crates-io))

(define-public crate-ke06z4-pac-0.1.1 (c (n "ke06z4-pac") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1llr48mhg77szzcxnl466v0v0dwp40nbj9pb28dylpvzcv5yz46l") (f (quote (("rt" "cortex-m-rt/device"))))))

