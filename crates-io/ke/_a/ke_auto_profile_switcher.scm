(define-module (crates-io ke _a ke_auto_profile_switcher) #:use-module (crates-io))

(define-public crate-ke_auto_profile_switcher-0.1.2 (c (n "ke_auto_profile_switcher") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "usb_enumeration") (r "^0.1.2") (d #t) (k 0)))) (h "1b0jk22hh3bxk0bbhadqngr1gwqrcx76jgh7gdy10l6bj5d0pzyk")))

(define-public crate-ke_auto_profile_switcher-0.1.3 (c (n "ke_auto_profile_switcher") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "usb_enumeration") (r "^0.1.2") (d #t) (k 0)))) (h "1rgnrvxd0qa58wm58ciiy3ygmv4sn015nkqh8kpngjwwmbk2i6jc")))

