(define-module (crates-io ke yc keycloak-access) #:use-module (crates-io))

(define-public crate-keycloak-access-0.0.1 (c (n "keycloak-access") (v "0.0.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "futures-locks") (r "^0.7.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9.0.0") (d #t) (k 0)) (d (n "keycloak") (r "^21.0.101") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1nr5x4qz7klzh99mi1ln0rk5wiiml0bjbk7bjhmg10g72yy7g8bp") (r "1.70")))

