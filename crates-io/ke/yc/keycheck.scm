(define-module (crates-io ke yc keycheck) #:use-module (crates-io))

(define-public crate-keycheck-0.1.0 (c (n "keycheck") (v "0.1.0") (d (list (d (n "ethers") (r "^2.0.3") (d #t) (k 2)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "1g0qf7gw613wr5w0ksisk39hbdlxvgjh8s59kl890yzgmp8vmj19")))

