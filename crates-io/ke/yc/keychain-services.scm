(define-module (crates-io ke yc keychain-services) #:use-module (crates-io))

(define-public crate-keychain-services-0.0.1 (c (n "keychain-services") (v "0.0.1") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 2)) (d (n "untrusted") (r "^0.6") (d #t) (k 2)))) (h "07kyfffm7cwx4bplihzzh3kms136pl3skr2l2ipwb3kxwddgq44r") (f (quote (("interactive-tests"))))))

(define-public crate-keychain-services-0.0.2 (c (n "keychain-services") (v "0.0.2") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 2)) (d (n "untrusted") (r "^0.6") (d #t) (k 2)))) (h "0jd3557f36qmvyw9hzxz3py8qmbdz1ckjm8z8fcywbnjzc11gl1z") (f (quote (("interactive-tests"))))))

(define-public crate-keychain-services-0.1.0 (c (n "keychain-services") (v "0.1.0") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "untrusted") (r "^0.6") (d #t) (k 2)) (d (n "zeroize") (r "^0.4") (d #t) (k 0)))) (h "0qrcysaf7v08fb779qba0hbpycgg7cv43mkj3zsf5lqjyjy8py67") (f (quote (("interactive-tests"))))))

(define-public crate-keychain-services-0.1.1 (c (n "keychain-services") (v "0.1.1") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "untrusted") (r "^0.6") (d #t) (k 2)) (d (n "zeroize") (r "^0.4") (d #t) (k 0)))) (h "1a5hw0nd49wxrgfmxd1scq80q9zw8lsd7ddcl7grgzri7filnvbh") (f (quote (("interactive-tests"))))))

