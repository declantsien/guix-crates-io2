(define-module (crates-io ke yc keycutter) #:use-module (crates-io))

(define-public crate-keycutter-0.1.0 (c (n "keycutter") (v "0.1.0") (h "0084mw1pf43r14w4k7rsvg0xfvwg7jgixg9k6lk642adc4h6f21l") (y #t)))

(define-public crate-keycutter-0.0.1 (c (n "keycutter") (v "0.0.1") (h "1q8xkjpizawgqrf0c1rypcv5lcjfdvxamf5g1v6jyjfsd7mgbwi8")))

