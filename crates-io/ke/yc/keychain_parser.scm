(define-module (crates-io ke yc keychain_parser) #:use-module (crates-io))

(define-public crate-keychain_parser-0.1.0 (c (n "keychain_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "logos") (r "^0.14") (f (quote ("export_derive"))) (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ylssvzqplmrjs5z55fz86q8mc3yy9vz80ryyll7m8fl3f5zkvcd")))

