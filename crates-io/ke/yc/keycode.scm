(define-module (crates-io ke yc keycode) #:use-module (crates-io))

(define-public crate-keycode-0.1.0 (c (n "keycode") (v "0.1.0") (h "1a5qwgrz7xb9vmaj2hk87zmvyis68ij8jzcxwfaln9vhv4z590r0")))

(define-public crate-keycode-0.1.1 (c (n "keycode") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 1)) (d (n "regex") (r "^1.1.0") (d #t) (k 1)))) (h "0b9abjyc2mc1gfxm0b1n3fw20dshv7gm8s5iknc7lskk5818xlwj")))

(define-public crate-keycode-0.1.2 (c (n "keycode") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 1)) (d (n "regex") (r "^1.1.0") (d #t) (k 1)))) (h "036pal5jv8dirdq3n9gad87y89jxd6dknwmf1dc2vh7x2ahkdxb1")))

(define-public crate-keycode-0.2.0 (c (n "keycode") (v "0.2.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 1)) (d (n "regex") (r "^1.1.0") (d #t) (k 1)))) (h "01r6qhykbfmq63hb4zz0vfv3g4742fhwcv0b2jcpwpx9bf5i4hz1")))

(define-public crate-keycode-0.2.1 (c (n "keycode") (v "0.2.1") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 1)) (d (n "regex") (r "^1.1.0") (d #t) (k 1)))) (h "000z5lqpslahaj5h2bb957v36ci3ch4aashyl3b7smcbf3i55rvy")))

(define-public crate-keycode-0.2.2 (c (n "keycode") (v "0.2.2") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 1)) (d (n "regex") (r "^1.1.0") (d #t) (k 1)))) (h "0igvc5n5gncbkg3f5qg0fj3v7md8ypas44g655gw5anhda09ar11")))

(define-public crate-keycode-0.3.0 (c (n "keycode") (v "0.3.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "keycode_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0dqrfa1sgh2ph3avd2brz34m23lycxclbmqqv5wcvb4g2qhy78k2")))

(define-public crate-keycode-0.4.0 (c (n "keycode") (v "0.4.0") (d (list (d (n "arraydeque") (r "^0.4.5") (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "keycode_macro") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vbk8vcq4alkxsk9g157gcb9sbnljyqyg955n478mv1a331p6y5h")))

