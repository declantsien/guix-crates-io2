(define-module (crates-io ke yc keycloak-crd) #:use-module (crates-io))

(define-public crate-keycloak-crd-0.1.0 (c (n "keycloak-crd") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.9.0") (k 0)) (d (n "k8s-openapi") (r "^0.9.0") (f (quote ("v1_17"))) (k 2)) (d (n "kube-derive") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "003sx1awi43lh92jr17sdbwzz1nam1xm2vl4k34ax348dx8ml0ag")))

(define-public crate-keycloak-crd-0.1.1 (c (n "keycloak-crd") (v "0.1.1") (d (list (d (n "k8s-openapi") (r "^0.9.0") (k 0)) (d (n "k8s-openapi") (r "^0.9.0") (f (quote ("v1_17"))) (k 2)) (d (n "kube-derive") (r "^0.42") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "1m9zi7i2gfn8xqavs8div6dybw4yhq1virkc6d3m4kc6lxajswjw")))

