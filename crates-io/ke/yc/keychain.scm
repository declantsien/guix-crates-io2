(define-module (crates-io ke yc keychain) #:use-module (crates-io))

(define-public crate-keychain-0.0.1 (c (n "keychain") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "04kcg46s3iql07a99fc1z1931qpqmy078p8rx1l5gdk07n1c26bd")))

(define-public crate-keychain-0.0.2 (c (n "keychain") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)))) (h "1vjvhw0xwgmp00swz2fzbas9kkbnp2kg96hispdx1bzy8wdlx9jq")))

