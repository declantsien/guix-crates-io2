(define-module (crates-io ke yp keypaste) #:use-module (crates-io))

(define-public crate-keypaste-0.1.0 (c (n "keypaste") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "15iidy1f6p7myhxkwhnpgh0dzy5dk2mxwb6x22ppxd13g048zmq5")))

