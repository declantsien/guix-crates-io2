(define-module (crates-io ke yp keyphrases) #:use-module (crates-io))

(define-public crate-keyphrases-0.1.0 (c (n "keyphrases") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "030n94gdy1qa4d2ylhy5pnalqfh3g3a8hnb70bfhj0s1w4d4cr3j") (y #t)))

(define-public crate-keyphrases-0.1.1 (c (n "keyphrases") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0pni48yxiklzfqnp308kslkx0z3xv3c3nafr4cryqls9ri96ab2m") (y #t)))

(define-public crate-keyphrases-0.1.2 (c (n "keyphrases") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1kc6mpcsb4887m8q6kgi133dqz7limfybxxz1ywfflpzw1rcpqi5") (y #t)))

(define-public crate-keyphrases-0.1.3 (c (n "keyphrases") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02xipklmvjnia2yhd3pszdkrmr9s24hz9s6m06czk2g1wx8m63wy")))

(define-public crate-keyphrases-0.1.4 (c (n "keyphrases") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0aaydqqqzbdngw6lfz2a17c8ach1w4d1cckywxki9dmzrqihkncg")))

(define-public crate-keyphrases-0.1.5 (c (n "keyphrases") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "163vsh0z1mw7pf11m2j13x0wdg338zzzh34c43ffy27i981nnqrp")))

(define-public crate-keyphrases-0.1.6 (c (n "keyphrases") (v "0.1.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05a9p95q8b88g6v4zzq0wy4fzmb2nq1mc6wpfkcix36wjf6g26mx")))

(define-public crate-keyphrases-0.2.0 (c (n "keyphrases") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qkzj305adjwh8hs0fxsas2lmh0j9lhgdw46622q3bsy9z6nxiyw")))

(define-public crate-keyphrases-0.3.0 (c (n "keyphrases") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1grlrjcj9qy92m5yfysfjwmm6pnzcg24ysjr96g0nkdjkj707r7h") (y #t)))

(define-public crate-keyphrases-0.3.1 (c (n "keyphrases") (v "0.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1h1c3h9xjhv6c7azr6b0nvmdqy6qb3hyv69gyz3k42x8a0k4iwc4") (y #t)))

(define-public crate-keyphrases-0.3.2 (c (n "keyphrases") (v "0.3.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18m46yvkjvf3nwqfhap5yb5v52l6xid7ja2kdqkp8mnya2m9nink")))

(define-public crate-keyphrases-0.3.3 (c (n "keyphrases") (v "0.3.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02jdv9azpsvpl2d4710pddbgqlvkxbry53ik3dnmbz5k6wa1fkja")))

