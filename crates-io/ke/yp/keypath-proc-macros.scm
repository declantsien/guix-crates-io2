(define-module (crates-io ke yp keypath-proc-macros) #:use-module (crates-io))

(define-public crate-keypath-proc-macros-0.1.0 (c (n "keypath-proc-macros") (v "0.1.0") (d (list (d (n "keypath") (r "^0.1.0") (d #t) (k 2)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)))) (h "1riz2bw4m9hnvr8zhvay4fx6hlygaqqgk1c1d6dgc80adb4g55ha")))

(define-public crate-keypath-proc-macros-0.2.0 (c (n "keypath-proc-macros") (v "0.2.0") (d (list (d (n "keypath") (r "^0.2.0") (d #t) (k 2)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.39") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zl63kh98c4wp2v9ssnr4f1jz2g1789xypydhlnyyv1md3q43bwv")))

