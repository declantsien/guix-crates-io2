(define-module (crates-io ke yp keypair-rs) #:use-module (crates-io))

(define-public crate-keypair-rs-0.1.0 (c (n "keypair-rs") (v "0.1.0") (d (list (d (n "blake2") (r "^0.9.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.26.0") (f (quote ("bitcoin_hashes" "rand"))) (d #t) (k 0)))) (h "1y9i1myws3sv292j0cngmzjaic7jrr93yr8ax9kd2z5wi47r5r9y")))

(define-public crate-keypair-rs-0.1.1 (c (n "keypair-rs") (v "0.1.1") (d (list (d (n "blake2") (r "^0.9.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.26.0") (f (quote ("bitcoin_hashes" "rand"))) (d #t) (k 0)))) (h "1j3mk22ini5xc3qcls8r6abnnnj1yzak1v6l08n9ixq66lacfbkr")))

