(define-module (crates-io ke yp keypath) #:use-module (crates-io))

(define-public crate-keypath-0.1.0 (c (n "keypath") (v "0.1.0") (h "0w81vk7hc0gsbx82rwv1c8pvl6dzd9gwispg9c0ghpllanyfjcxv")))

(define-public crate-keypath-0.1.1 (c (n "keypath") (v "0.1.1") (d (list (d (n "keypath-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0ycvwnkkds0451mbjsqqbc3895v4cx2f9i6difyj1z9449b8qlg7")))

(define-public crate-keypath-0.2.0 (c (n "keypath") (v "0.2.0") (d (list (d (n "keypath-proc-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0n0qg1f5bi0frxvxffz9k0mx201s59pn3jhvif9nk906r7kzslqc")))

