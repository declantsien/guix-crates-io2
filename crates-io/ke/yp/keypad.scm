(define-module (crates-io ke yp keypad) #:use-module (crates-io))

(define-public crate-keypad-0.1.0 (c (n "keypad") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1sgdhp5pc17bhlkq6iz3hlzx7kd2fdza1kh5h6h9lmi39qsg5n72")))

(define-public crate-keypad-0.1.1 (c (n "keypad") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0g14wv6zhc8j4djf1783jv5yb0j6mf1ygvnamsvrcm8gxrj0kldi")))

(define-public crate-keypad-0.1.2 (c (n "keypad") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0kpnm334382jgrahcgphfkncglydfzdz6vl5lsvvgzq5l4705gfb")))

(define-public crate-keypad-0.1.3 (c (n "keypad") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "098904h0jb869y0pgzlkyngxamd7krnfk210l0zispplvizh69sb")))

(define-public crate-keypad-0.1.4 (c (n "keypad") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "076fj2p1c26z72ph7zviwpvmh335i18xf3pw5jrw7riwfbzy26rd") (f (quote (("example_generated"))))))

(define-public crate-keypad-0.2.0 (c (n "keypad") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0dagly0hkkf2jh43z4zkihgpqimqibn72wq0fg1r1gql2li48nc0") (f (quote (("example_generated")))) (r "1.56")))

(define-public crate-keypad-0.2.1 (c (n "keypad") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "020afcwflh4lv4a59adw1s8aa211jifn902wx3jykl2km5ivvcmz") (f (quote (("example_generated")))) (r "1.56")))

(define-public crate-keypad-0.2.2 (c (n "keypad") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0dqd6afab7rjfda18a7n4lpv8ym5gd2hlcpaf8bafy9wgxsril3c") (f (quote (("example_generated")))) (r "1.56")))

