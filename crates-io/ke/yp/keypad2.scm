(define-module (crates-io ke yp keypad2) #:use-module (crates-io))

(define-public crate-keypad2-0.1.0 (c (n "keypad2") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0ki3s2182dkgpsvx14is8lkxkcbh6rrvh0gh4xczr21hns6qrxnn")))

(define-public crate-keypad2-0.1.1 (c (n "keypad2") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1x1nf2ci39cydrf74faj752bpwznijayb94siwgflsk1hmk4j5n4")))

