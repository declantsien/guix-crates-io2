(define-module (crates-io ke yp keypropdecode) #:use-module (crates-io))

(define-public crate-keypropdecode-0.0.0 (c (n "keypropdecode") (v "0.0.0") (h "0n4g767b3by69hqzzw0q0bwjsf1x7qgri6ylnss57vcflxkxz6ng") (y #t)))

(define-public crate-keypropdecode-0.1.0 (c (n "keypropdecode") (v "0.1.0") (h "0rdw131ya0hiif85zvp6902mm826kj2kjhy8sk4ml9jb0jrnhkcf") (y #t)))

(define-public crate-keypropdecode-1.0.0 (c (n "keypropdecode") (v "1.0.0") (h "0sa4k3rcghv1sx2679sv31cqbjqvfwdvignx422lyhcfbzgpvfbw") (y #t)))

(define-public crate-keypropdecode-1.0.1 (c (n "keypropdecode") (v "1.0.1") (h "1194190892m83j5bawh5bb7c89wa8q5ilr5dn45h5glg79ax536r") (y #t)))

(define-public crate-keypropdecode-1.0.2 (c (n "keypropdecode") (v "1.0.2") (h "1gpjrzvg0bnv5y8yv3dc8jxb0m9raw8ny8bnwhwd6c2gymfr3hs2")))

(define-public crate-keypropdecode-1.0.3 (c (n "keypropdecode") (v "1.0.3") (h "0j83g1c54f1ip9bh1jp761jq5m2b60racpn48vlkvfmip3s9zh13")))

(define-public crate-keypropdecode-1.1.0 (c (n "keypropdecode") (v "1.1.0") (h "1yc9cdyr6ads04apdyzn9997cdpvxxpvnyfk4c96vgl6y8qkgx0i") (y #t)))

(define-public crate-keypropdecode-1.1.1 (c (n "keypropdecode") (v "1.1.1") (h "0s7qyc62g36rwszw95ng9kmsizxkmhaglxc6j3cqra9ydspipyvk") (y #t)))

(define-public crate-keypropdecode-1.1.2 (c (n "keypropdecode") (v "1.1.2") (h "0qzyazwwr4qram1xsmggc0s41fsk5ffrhns2kbf0wd64mvayh260") (y #t)))

(define-public crate-keypropdecode-1.1.3 (c (n "keypropdecode") (v "1.1.3") (h "1d0b6inmxgb7f1hcdqnshqz2zbzl2scg8h71xaq7nw02q7a3r584") (y #t)))

(define-public crate-keypropdecode-1.1.4 (c (n "keypropdecode") (v "1.1.4") (h "0ypj86jrfda96sjf1iynsiqxsasm47bhpm634aipncaypbxvihdj") (y #t)))

(define-public crate-keypropdecode-1.1.5 (c (n "keypropdecode") (v "1.1.5") (h "0cisicmghimpsh1cavbdnp63naw16hmfq5kqclyflx4nklah9waj")))

(define-public crate-keypropdecode-1.1.6 (c (n "keypropdecode") (v "1.1.6") (h "0lwf4w9r8glp31fbhmkmwk8rlrfq5965iady90rypklgf89v2083")))

(define-public crate-keypropdecode-1.1.7 (c (n "keypropdecode") (v "1.1.7") (h "05i37w9v423bkdz99kz1yqmhkhhqrjf7hdafmx5kdg8y3wqxpmig")))

(define-public crate-keypropdecode-1.1.8 (c (n "keypropdecode") (v "1.1.8") (h "0d6zi3jcadk8ivhsq65d6gwi3zn670z9zgh4qi44cp0bwsjyhm1j")))

(define-public crate-keypropdecode-2.0.0 (c (n "keypropdecode") (v "2.0.0") (h "1nc417a7sx5w63iginr3ygcji0mmz93c8fc4v8nfz0dzpzrz5pbn")))

(define-public crate-keypropdecode-2.0.1 (c (n "keypropdecode") (v "2.0.1") (h "01d7gsnpp9gnvgzhixnxjw3gm5sy1fqwvn1m4p8iiv7vdy4hkacl")))

(define-public crate-keypropdecode-2.0.2 (c (n "keypropdecode") (v "2.0.2") (h "0b8hj4fa3y92zmsz1f54r0751pfsmxkv5j6drga5wr69x3qg1hlf")))

(define-public crate-keypropdecode-2.0.3 (c (n "keypropdecode") (v "2.0.3") (h "0cz6iy2klx8v2mmancs7rkh90adh24pnmq1g1hjnpskmc2y7c54m")))

