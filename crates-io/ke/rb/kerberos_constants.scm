(define-module (crates-io ke rb kerberos_constants) #:use-module (crates-io))

(define-public crate-kerberos_constants-0.0.1 (c (n "kerberos_constants") (v "0.0.1") (h "0hdb4kzhm92vscd7yx0mycpmzycg7z8rdhny0dby1689xdgn0grq")))

(define-public crate-kerberos_constants-0.0.2 (c (n "kerberos_constants") (v "0.0.2") (h "10imlgv4sxvv0szy66wli876v4dwxvjhpz43l8php1i27dvlzp96")))

(define-public crate-kerberos_constants-0.0.3 (c (n "kerberos_constants") (v "0.0.3") (h "10416gdm80c7dwyvr92vqndhwmp7w6swn88pigj595fyl3g3acyk")))

(define-public crate-kerberos_constants-0.0.4 (c (n "kerberos_constants") (v "0.0.4") (h "1671fdfpz44hdkh1r9zf99hmmp3bfzpzxrbqyyzmv2x53105g002")))

(define-public crate-kerberos_constants-0.0.5 (c (n "kerberos_constants") (v "0.0.5") (h "1b9hdrjf6pn0682jr42xpc2bsd26v5nk8cyjip1gmd6hla8wf59h")))

(define-public crate-kerberos_constants-0.0.6 (c (n "kerberos_constants") (v "0.0.6") (h "0qx9b7ydrrnjccgd5appr9lz59ii7s52h20nnsib2d5v31q00k7i")))

(define-public crate-kerberos_constants-0.0.7 (c (n "kerberos_constants") (v "0.0.7") (h "1gwdfx3s62b9shk7ysvv9frin15mb218xi6pd0kqcrqhzqri87wf")))

(define-public crate-kerberos_constants-0.0.8 (c (n "kerberos_constants") (v "0.0.8") (h "1jvphibppng1679b5992sp8fv6b0hirzsg5izzq5d4v5shrr5gki")))

(define-public crate-kerberos_constants-0.0.9 (c (n "kerberos_constants") (v "0.0.9") (h "1gd3qzswfixdmqvspay91di0aak3707vnqvn6dyp5rg2fy7yp97b")))

