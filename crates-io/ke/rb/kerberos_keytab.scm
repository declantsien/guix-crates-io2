(define-module (crates-io ke rb kerberos_keytab) #:use-module (crates-io))

(define-public crate-kerberos_keytab-0.0.1 (c (n "kerberos_keytab") (v "0.0.1") (d (list (d (n "kerberos_constants") (r "^0.0") (d #t) (k 2)) (d (n "nom") (r "^6.0") (d #t) (k 0)))) (h "0ixz9hfgk10li50nrw041fb4ww1m9l3zq968npkqk8i3skya1vkj")))

(define-public crate-kerberos_keytab-0.0.2 (c (n "kerberos_keytab") (v "0.0.2") (d (list (d (n "kerberos_constants") (r "^0.0") (d #t) (k 2)) (d (n "nom") (r "^6.0") (d #t) (k 0)))) (h "088mbxfhvwdfn7q2mm31yrvs8k13r816qajlqi911b0xzj0affbr")))

