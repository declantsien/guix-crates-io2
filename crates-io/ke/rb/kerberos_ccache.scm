(define-module (crates-io ke rb kerberos_ccache) #:use-module (crates-io))

(define-public crate-kerberos_ccache-0.0.1 (c (n "kerberos_ccache") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0q5ipd645bf1hd6gk1hkb8s2cj2l2zb7b11n52xyrp18rwlgikwh")))

(define-public crate-kerberos_ccache-0.0.2 (c (n "kerberos_ccache") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "17233m71b8a4ig8j5djbj178g0c6shqpdbdv759frzar8hlh03l0")))

(define-public crate-kerberos_ccache-0.0.3 (c (n "kerberos_ccache") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1b5gaf8n52432i4j8sa8iyvn8w1qcsmylhqwy2gmnd8j5qhaq5v3")))

(define-public crate-kerberos_ccache-0.0.4 (c (n "kerberos_ccache") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1lcwric1f6lm8b7g56f6af77af3fln0jdq9r9qw2vb4dv4i9qm3z")))

(define-public crate-kerberos_ccache-0.0.5 (c (n "kerberos_ccache") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "kerberos_asn1") (r "^0.0") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1ksh701aklakagfzvq1gf5d2i46cbgdh1ylzxgqfaxl3x67iqcpr")))

(define-public crate-kerberos_ccache-0.0.6 (c (n "kerberos_ccache") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "kerberos_asn1") (r "^0.1") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0vpp5q0j8kfbjiiz91ib1xnz5q5jl03z9vb2jd8m5fnn6lgkxl0r")))

(define-public crate-kerberos_ccache-0.0.7 (c (n "kerberos_ccache") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "kerberos_asn1") (r "^0.2") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "07b8yz0hs3m89g8cannfcyhx7jhjcagh2s38l0gxz1f6k7bn9817")))

