(define-module (crates-io ke rb kerberos_asn1) #:use-module (crates-io))

(define-public crate-kerberos_asn1-0.0.1 (c (n "kerberos_asn1") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "19a8sdbx1h96z5ai6jc40z76j1x8r6z2clidigkyww695b8c1mcp")))

(define-public crate-kerberos_asn1-0.0.2 (c (n "kerberos_asn1") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "1jba69gw9dbasqd934dxy7jib42mcz0n44qmp1cxxhy97nwmypvj")))

(define-public crate-kerberos_asn1-0.0.3 (c (n "kerberos_asn1") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "1c0q972fwz4g7n7c8yjpnrqqh1wsz8za3lsdhmxa65jh7rg13c4y")))

(define-public crate-kerberos_asn1-0.0.4 (c (n "kerberos_asn1") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "11vgcppykfqml2kpwjzl29cz6lr7z35kwgq07ib7r0isnk64xqxy")))

(define-public crate-kerberos_asn1-0.0.5 (c (n "kerberos_asn1") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "1qi543p1p9av34d13lj89rd65cpx83fgw24dpihgc2ack6xm47wy")))

(define-public crate-kerberos_asn1-0.0.6 (c (n "kerberos_asn1") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "1z379shrxszvc7maqd90fq1ad9lmhmr8mkwmwqhr1vabli8cvxqh")))

(define-public crate-kerberos_asn1-0.0.7 (c (n "kerberos_asn1") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "1yxi5dglbqnv0a5k01rkad9r9j8r7g04hmik5hcskza464sw7iqz")))

(define-public crate-kerberos_asn1-0.0.8 (c (n "kerberos_asn1") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "1kc6gp8pfsn9yhl92nrxnnv8mgx58lzx6rxvgjmdx74cwcsi8g71")))

(define-public crate-kerberos_asn1-0.1.0 (c (n "kerberos_asn1") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "01bqc97kr9h2dljzdwwrq7dqmfrfv1hbm6z3px93fwiwh7sddpmp")))

(define-public crate-kerberos_asn1-0.2.0 (c (n "kerberos_asn1") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "1n7sg6pwlxl0cq4gvy1fgplv0l6fmigx3gl5hpxj1yagfnm0xzh2")))

(define-public crate-kerberos_asn1-0.2.1 (c (n "kerberos_asn1") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kerberos_constants") (r "^0.0") (d #t) (k 0)) (d (n "red_asn1") (r "^0.3") (d #t) (k 0)) (d (n "red_asn1_derive") (r "^0.2") (d #t) (k 0)))) (h "00mdgvcjhc7zg3kyjpqdamhv2lmvsa0c2whqh1b4s5sfh8si0zfy")))

