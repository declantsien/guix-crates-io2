(define-module (crates-io ke rb kerberos-parser) #:use-module (crates-io))

(define-public crate-kerberos-parser-0.1.0 (c (n "kerberos-parser") (v "0.1.0") (d (list (d (n "der-parser") (r "^0.5.2") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "rusticata-macros") (r "^0.4") (d #t) (k 0)))) (h "0qwghnk44djfa0932s5ddhlapivxvb7msd15k7jwnv5k5iqkh12g")))

(define-public crate-kerberos-parser-0.1.1 (c (n "kerberos-parser") (v "0.1.1") (d (list (d (n "der-parser") (r "^0.5.2") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "rusticata-macros") (r "^0.4") (d #t) (k 0)))) (h "08h00biv3rgp59qdk49c2s07vp4ra362j06snmlxmbp2lp1s5ygy")))

(define-public crate-kerberos-parser-0.1.2 (c (n "kerberos-parser") (v "0.1.2") (d (list (d (n "der-parser") (r "^0.5.3") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "rusticata-macros") (r "^0.4") (d #t) (k 0)))) (h "0hw5kxscv3nklf07qxn4qlbrs8wyk0nyp65d9fsdnfklm8r13xqy")))

(define-public crate-kerberos-parser-0.1.3 (c (n "kerberos-parser") (v "0.1.3") (d (list (d (n "der-parser") (r "^0.5.3") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "rusticata-macros") (r "^0.4") (d #t) (k 0)))) (h "0c3xgh7xg7gja8mnhiy11blg9jsszgwph323rcmx7vnhnc7qkwx2")))

(define-public crate-kerberos-parser-0.1.4 (c (n "kerberos-parser") (v "0.1.4") (d (list (d (n "der-parser") (r "^0.5.5") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "rusticata-macros") (r "^0.4") (d #t) (k 0)))) (h "0pkm7bwvhzdvlpniii00i5i3w5zhw6phdivkhlxm3fwjdvky0ncz")))

(define-public crate-kerberos-parser-0.2.0 (c (n "kerberos-parser") (v "0.2.0") (d (list (d (n "der-parser") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^1.0") (d #t) (k 0)))) (h "16xvh5xrjsix9smv5rl160f7xbc9msn0swnnm2h5gpwxllqjd3in")))

(define-public crate-kerberos-parser-0.3.0 (c (n "kerberos-parser") (v "0.3.0") (d (list (d (n "der-parser") (r "^2.0.0") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "rusticata-macros") (r "^1.0") (d #t) (k 0)))) (h "02xlrrdc7ld9js5bzdj5wld16w977fm5nfdd18vfc3qri2zlc976")))

(define-public crate-kerberos-parser-0.4.0 (c (n "kerberos-parser") (v "0.4.0") (d (list (d (n "der-parser") (r "^3.0.0") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^2.0.2") (d #t) (k 0)))) (h "0m707cjsh8ic7mmdlanvp21mrgspcyj6v21q7ayhgy4klhxjic4g")))

(define-public crate-kerberos-parser-0.5.0 (c (n "kerberos-parser") (v "0.5.0") (d (list (d (n "der-parser") (r "^4.0.0") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^2.0.2") (d #t) (k 0)))) (h "0h6dpwaww47xh536wsii4wz9djdsdbljb62s6cpn0ymzqrp4v42q")))

(define-public crate-kerberos-parser-0.6.0 (c (n "kerberos-parser") (v "0.6.0") (d (list (d (n "der-parser") (r "^5.0.0") (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^3.0") (d #t) (k 0)))) (h "0lzxii36ip7jzq99akbr5hddzikm18hn2db33pnqjj4b3khw6lam")))

(define-public crate-kerberos-parser-0.7.0 (c (n "kerberos-parser") (v "0.7.0") (d (list (d (n "der-parser") (r "^6.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)))) (h "0mlg44b3jgvglrl28a18483dg57f53j2bb8jvfrynbzhxr0yd5yf")))

(define-public crate-kerberos-parser-0.7.1 (c (n "kerberos-parser") (v "0.7.1") (d (list (d (n "der-parser") (r "^6.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)))) (h "1nlni6j1swsgpabqx8wnc4j5qsdxms6g9qk5x8vwxjsr8zypq3n1")))

(define-public crate-kerberos-parser-0.8.0 (c (n "kerberos-parser") (v "0.8.0") (d (list (d (n "der-parser") (r "^9.0") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "rusticata-macros") (r "^4.0") (d #t) (k 0)))) (h "08gk2js9y52m0pmnnff2jn5c81khzc0bxqmg5348v2kb6c6dip6y")))

