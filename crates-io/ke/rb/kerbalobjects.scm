(define-module (crates-io ke rb kerbalobjects) #:use-module (crates-io))

(define-public crate-kerbalobjects-1.0.0 (c (n "kerbalobjects") (v "1.0.0") (h "1k1nlp48l9bdg32lph2wkizqb36kj6pkq4f43fwvykvdi8whyp50")))

(define-public crate-kerbalobjects-1.0.1 (c (n "kerbalobjects") (v "1.0.1") (h "0k4dwzxagk7kiycvvdhjr3njj8qvq4q6hn7q71d821ljqmm887bv")))

(define-public crate-kerbalobjects-1.0.2 (c (n "kerbalobjects") (v "1.0.2") (h "14xvm86vylyfn5i0hkb8fiyvbapp3kckqlgzji6lzhwh1rw1yjxb")))

(define-public crate-kerbalobjects-1.0.3 (c (n "kerbalobjects") (v "1.0.3") (h "07w0cdv8sjgjpiddmipml0q1fsvmmkdapmfxavx13y37gbk136d0")))

(define-public crate-kerbalobjects-1.0.4 (c (n "kerbalobjects") (v "1.0.4") (h "0v34i34x720p922rr9xc7ka6d3arba0gsg4yrm8qfs8h9difia8r")))

(define-public crate-kerbalobjects-1.0.5 (c (n "kerbalobjects") (v "1.0.5") (h "1d31w2zswxzsh1xw8n7jqk1mls5xr8y2vpvxsmvdcspadsc4cbww")))

(define-public crate-kerbalobjects-1.0.6 (c (n "kerbalobjects") (v "1.0.6") (h "0nh6dd1cgzfhjrfddp3916hkb6imy19n54r2x76s6xh8cdmha815")))

(define-public crate-kerbalobjects-1.0.7 (c (n "kerbalobjects") (v "1.0.7") (h "0gm0jlda9c7bjfzixhk0szb6r9vg8xhcvc89bznppx1s683dm62b")))

(define-public crate-kerbalobjects-1.0.8 (c (n "kerbalobjects") (v "1.0.8") (h "05qqyynyisjnnn2ifdmnn9ilh64w47yhn9wbf21z29x3zyqxszyl")))

(define-public crate-kerbalobjects-1.0.9 (c (n "kerbalobjects") (v "1.0.9") (h "0fv29aiffs8zkw3hzjwvn9qs43dj30hsdhvlwv06awvgadmmkjlz")))

(define-public crate-kerbalobjects-1.0.10 (c (n "kerbalobjects") (v "1.0.10") (h "1abpx176ldq2z4g5i2khz2gkqvqh7a8lvh92473z37hi4pnq1qfn")))

(define-public crate-kerbalobjects-1.0.11 (c (n "kerbalobjects") (v "1.0.11") (h "17v19i0d3g0iyiwc88rmpvyc0q5jpqbqan0kr4zxygrih1q4qc7m")))

(define-public crate-kerbalobjects-1.0.12 (c (n "kerbalobjects") (v "1.0.12") (h "0b88fp4qpzjh1k3gxaf65vw0kif5nzxmf1iw23l38d0xz480ni3v")))

(define-public crate-kerbalobjects-2.0.0 (c (n "kerbalobjects") (v "2.0.0") (h "1klz8bzhmxdczqza46s8xfri85j2hzpnyajwnssqvmrwlfn5r4lc")))

(define-public crate-kerbalobjects-2.0.1 (c (n "kerbalobjects") (v "2.0.1") (h "0m0f890snrzhp19n08j7lk8fsjjwkiyjls51rs8qpn6v4lym4535")))

(define-public crate-kerbalobjects-2.0.2 (c (n "kerbalobjects") (v "2.0.2") (h "1w1w47821jrh61jn0pl62ljrj57mmhvhdj9mfmw1i77n8zx8s71c")))

(define-public crate-kerbalobjects-2.0.3 (c (n "kerbalobjects") (v "2.0.3") (h "042qsch24swb95p5jm42vd7cif4gnmshal00dbr3j5dv7rg8k1m0")))

(define-public crate-kerbalobjects-2.0.4 (c (n "kerbalobjects") (v "2.0.4") (h "1dkiq1gw3zs5qfixv74kzn0qfbrrp2x9p5nd7v2pm5mcwkigm9aq")))

(define-public crate-kerbalobjects-2.0.5 (c (n "kerbalobjects") (v "2.0.5") (h "00ahg9w8wb4ilqny1snpd8vsc35ik41466p7dpwhbfa85g37hbwg")))

(define-public crate-kerbalobjects-2.1.0 (c (n "kerbalobjects") (v "2.1.0") (h "0jrrrvzklnb4d207jkckzvflq58wd425imiimiplhmy02wsrbby6")))

(define-public crate-kerbalobjects-3.0.0 (c (n "kerbalobjects") (v "3.0.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "03zlbg0r2agas721njqp1ai83pvw8l7rvbmdcgsppdpcvr6ha7i8")))

(define-public crate-kerbalobjects-3.0.1 (c (n "kerbalobjects") (v "3.0.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1gawk56kr26xlm5n0v75kz6fldaw4q5zi9hx0dcrc8dp1p7h9bj9")))

(define-public crate-kerbalobjects-3.0.2 (c (n "kerbalobjects") (v "3.0.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1gizwxyshpfdvscihv3vq5cp4v3krxgrdl5jarx0hi58n6dj27ny")))

(define-public crate-kerbalobjects-3.0.3 (c (n "kerbalobjects") (v "3.0.3") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0zkm59my1wkzjdrz19ldv58kls795knyzhj38kmnl3ki4ww83gi9")))

(define-public crate-kerbalobjects-3.0.4 (c (n "kerbalobjects") (v "3.0.4") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0vdlmcir787l9h3dml9248cijvfjimay329g5v66j3gn8455ma2y")))

(define-public crate-kerbalobjects-3.0.5 (c (n "kerbalobjects") (v "3.0.5") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "114q3yyxix5i6h4hpy0400iwh6vzb6aza14mcii9r78lj01aw435")))

(define-public crate-kerbalobjects-3.0.7 (c (n "kerbalobjects") (v "3.0.7") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "06ndkzgqa7d7cwjr2c6rlg1d12nzz5mlc6awxn2srj0kfkvziw3y")))

(define-public crate-kerbalobjects-3.0.8 (c (n "kerbalobjects") (v "3.0.8") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "054359a4v77c7cnp6xirpidj2dd4ykka9wpnffblvp9l9xqmn2vf")))

(define-public crate-kerbalobjects-3.0.9 (c (n "kerbalobjects") (v "3.0.9") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "18wr4y07lm205z02m315pfb843iw84jkdqz9s6wbh5swp1qfdg69")))

(define-public crate-kerbalobjects-3.0.10 (c (n "kerbalobjects") (v "3.0.10") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "177j4lzfl30h6qmmqg5p8wx50375a356bbwjj6byhsa9zy4bwcqc")))

(define-public crate-kerbalobjects-3.0.11 (c (n "kerbalobjects") (v "3.0.11") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "04xny292lckyjlnf9pk6svn1v40wi0i6c2b2k2m5xbybhkg5d76c")))

(define-public crate-kerbalobjects-3.0.12 (c (n "kerbalobjects") (v "3.0.12") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1yxx769hdp6xfd2g5vb5nd9iba0kby4l2ij90jbk4hmy3fhvjzrk")))

(define-public crate-kerbalobjects-3.0.13 (c (n "kerbalobjects") (v "3.0.13") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1h1msvn0gclwamsndn2r5rivbmy41k7p2w2lv2p133cqq3ng050w")))

(define-public crate-kerbalobjects-3.0.14 (c (n "kerbalobjects") (v "3.0.14") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0s4f2c0s6rv69gq076iq2lbx7j6mcg15w0pllczqxrmsjmznsr3j")))

(define-public crate-kerbalobjects-3.1.0 (c (n "kerbalobjects") (v "3.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0b1hwyhsyb8s69ycm1y27ggqc7iwzwrdcfmbs6w0xzm0qfga6431")))

(define-public crate-kerbalobjects-3.1.1 (c (n "kerbalobjects") (v "3.1.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1ys3hka7gdyz30ysv7ahl3yjy28hcgm1q0kkhzcrxq5dakdllrxl")))

(define-public crate-kerbalobjects-3.1.2 (c (n "kerbalobjects") (v "3.1.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1ygp564xyh6z6fpddgh1ilvsya4hnvhiaqdw2apiziijnb10bmd4")))

(define-public crate-kerbalobjects-3.1.3 (c (n "kerbalobjects") (v "3.1.3") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0x5w1z2xf51kjqn2gmqvjcsp1yjqsz5ykz97zpgaas7a1kl7xm6j")))

(define-public crate-kerbalobjects-3.1.4 (c (n "kerbalobjects") (v "3.1.4") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1whcgx23yy5rbhn59flb999p6qx463n08254l8w2sgi2axn6mkx6")))

(define-public crate-kerbalobjects-3.1.5 (c (n "kerbalobjects") (v "3.1.5") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0jkzdfwv6idqffxh27gfhbxwg25ypbn8yfc6n3h7h8d15siixk67")))

(define-public crate-kerbalobjects-3.1.6 (c (n "kerbalobjects") (v "3.1.6") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "00kjg3sw9vbvz9f3wjfyqlg7avnbpq0qzlhbldv2wv1j14dnfnn8")))

(define-public crate-kerbalobjects-3.1.7 (c (n "kerbalobjects") (v "3.1.7") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1g0p1l9lmgy47r32bxwhnll3zxidp0qs50rn4bsb9nffm9rxf88p")))

(define-public crate-kerbalobjects-3.1.8 (c (n "kerbalobjects") (v "3.1.8") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1dr4y64kx4kvv6zmpvcj1b2rffykzh389fmf54n7hvr24ngiknvh")))

(define-public crate-kerbalobjects-3.1.9 (c (n "kerbalobjects") (v "3.1.9") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0shrp5rhj2iwp6frbw23ymgyxxjp1lr3n4mh0kwm18grswgkl5s2")))

(define-public crate-kerbalobjects-3.1.10 (c (n "kerbalobjects") (v "3.1.10") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1hjr2a69qj26303jynmm4i6jyvlv8k3c2dh5p57lz0vhndahjsdy")))

(define-public crate-kerbalobjects-3.1.11 (c (n "kerbalobjects") (v "3.1.11") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0g345mwwigm6gcbwmi3wk5q1g951dc1ncrragg45jvsb6cb4a29x")))

(define-public crate-kerbalobjects-3.1.12 (c (n "kerbalobjects") (v "3.1.12") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "12ymp8jvx3fk0vm5aifk64lbmv1nd0f3g00hbij701ylsdsskxcz")))

(define-public crate-kerbalobjects-3.1.13 (c (n "kerbalobjects") (v "3.1.13") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "15vigiva5rfmxfqdx9im0rh5klz9q7zg7vqc48g569nc12qw2qj4")))

(define-public crate-kerbalobjects-4.0.1 (c (n "kerbalobjects") (v "4.0.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00ripqj5gvj9mk75ss1nflcnmhbkz9a0zywdsxqkp558w9d9ny2h") (f (quote (("ksm") ("ko") ("default" "ksm" "ko"))))))

(define-public crate-kerbalobjects-4.0.2 (c (n "kerbalobjects") (v "4.0.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "178kf26b2ipk53vvlsdwcfz8wnzcwwgnp9gj2ann95b0zga3yg0y") (f (quote (("ksm") ("ko") ("default" "ksm" "ko"))))))

(define-public crate-kerbalobjects-4.0.3 (c (n "kerbalobjects") (v "4.0.3") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ghysnyrdpxmavx35sn8rcnpryb849928jx0aq685rx64giny40x") (f (quote (("ksm") ("ko") ("default" "ksm" "ko"))))))

