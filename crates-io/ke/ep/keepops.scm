(define-module (crates-io ke ep keepops) #:use-module (crates-io))

(define-public crate-keepops-0.0.1 (c (n "keepops") (v "0.0.1") (h "0jqizq1mw1h7w1nxyr6iwy64r08xzdyig1ga9h28mkd4svx1l08v")))

(define-public crate-keepops-0.0.2 (c (n "keepops") (v "0.0.2") (h "1yaj6v4fpa694744832s8a7qji1idypffynqdisbcjpdlj704ixy")))

