(define-module (crates-io ke ep keepass-diff) #:use-module (crates-io))

(define-public crate-keepass-diff-0.1.0 (c (n "keepass-diff") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "keepass") (r "^0.1.2") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "1x878qq252pfj1h66rd6bzpg92qhx9dfc2y89rb3zhcll80218sw")))

(define-public crate-keepass-diff-0.2.0 (c (n "keepass-diff") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "keepass") (r "^0.3.3") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "1x0z9n4ihbb62zp18m0bf9l3rd4zh86nzajl2kdmfrd6br0jb9pr")))

(define-public crate-keepass-diff-0.2.1 (c (n "keepass-diff") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "keepass") (r "^0.4.1") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "02zr3kdj78rjfn1fvif3pjkmfrxr8k6xm8lgr9zhylyg71rlwgfy")))

(define-public crate-keepass-diff-0.3.0 (c (n "keepass-diff") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "keepass") (r "^0.4.1") (d #t) (k 0)) (d (n "rpassword") (r "^2.0.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.4") (d #t) (k 0)))) (h "07nkfcqyrlzbm5qgc9xd3di4wv3z3r5h8mxla7bbq2gihlw1546j")))

(define-public crate-keepass-diff-1.0.0 (c (n "keepass-diff") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "keepass") (r "^0.4.5") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0ifkx1s5nbd9gw683vm7dz7ivsgnax1jr84vm2d8hl0w9fmqanhr")))

(define-public crate-keepass-diff-1.0.1 (c (n "keepass-diff") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "keepass") (r "=0.4.5") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "02q45m0n68bzdylwv94axr0aprsq61xfd8p1hn90jdsqxnnaycf9")))

(define-public crate-keepass-diff-1.1.0 (c (n "keepass-diff") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "keepass") (r "=0.4.7") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0v9c2wahz9224c1mxhjg7pgwgxk9j02sj3gr8qiimyrmizdafi64")))

(define-public crate-keepass-diff-1.1.1 (c (n "keepass-diff") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "keepass") (r "=0.4.9") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "0a3cvx2dd7plfmy6vqynh03y2mfj1pfdj6wb5appwlya8vdvb5ri")))

(define-public crate-keepass-diff-1.1.2 (c (n "keepass-diff") (v "1.1.2") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("cargo" "env" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "keepass") (r "=0.4.9") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1a1b2hbk0f2gk399vqzk9fdyikv1k6g9swy145yz8fphrdki43w8")))

(define-public crate-keepass-diff-1.1.3 (c (n "keepass-diff") (v "1.1.3") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("cargo" "env" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "keepass") (r "=0.4.9") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0vs1bfyhdsjqnavq6n3xd1yd4b7vsdhhdkqscr44zyn7p0s3pjs9")))

(define-public crate-keepass-diff-1.1.4 (c (n "keepass-diff") (v "1.1.4") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("cargo" "env" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "keepass") (r "=0.4.10") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1abyzah0j5sn6zqyacv1vvdn62ip28qv54wp8fh98gbmcqm1iiwr")))

(define-public crate-keepass-diff-1.2.0 (c (n "keepass-diff") (v "1.2.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("cargo" "env" "derive" "wrap_help"))) (d #t) (k 0)) (d (n "keepass") (r "^0.6.6") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.3.0") (d #t) (k 0)))) (h "0kk1rmp05cp5ia0d8jwkwy0926w3px444bwisg9nl0bxnffvcxib")))

