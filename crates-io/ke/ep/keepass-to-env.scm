(define-module (crates-io ke ep keepass-to-env) #:use-module (crates-io))

(define-public crate-keepass-to-env-0.1.0 (c (n "keepass-to-env") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "keepass") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10rl4qf7ik506h31q1qmvkvjxnhcfbqvc74az9zf7x03041gp11m")))

(define-public crate-keepass-to-env-0.2.0 (c (n "keepass-to-env") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "keepass") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "009l41nskv8pldzjh33q2awv1kcmga4aaz69ldaz9skcs3yg4d70")))

(define-public crate-keepass-to-env-0.3.0 (c (n "keepass-to-env") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "keepass") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)))) (h "055xjc5c3r10l3194w32bxwnl0w99b7s0lc0fhhdywak7k4dsrqw")))

(define-public crate-keepass-to-env-0.3.1 (c (n "keepass-to-env") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "keepass") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)))) (h "11cdxrvhfy11irrij00s5hj8669bvfz8ikjhzchkn9g6s6iiq737")))

(define-public crate-keepass-to-env-0.3.2 (c (n "keepass-to-env") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "keepass") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)))) (h "098xfrfr9bzn921g0a7g6grlg3zxhk131gmi3cwalj9m0q09cklq")))

