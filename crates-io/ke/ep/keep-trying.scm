(define-module (crates-io ke ep keep-trying) #:use-module (crates-io))

(define-public crate-keep-trying-0.1.0 (c (n "keep-trying") (v "0.1.0") (d (list (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)))) (h "1z9iqgj49vmmn32fn7zqnlkkaxrziqlid9a426qr7pnm751i5zw1")))

(define-public crate-keep-trying-0.1.1 (c (n "keep-trying") (v "0.1.1") (d (list (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)))) (h "1qmpzigy0dg2l7mrpvkgx343ylmh51dq6zys24f949j0d9mpzfdm")))

(define-public crate-keep-trying-0.1.2 (c (n "keep-trying") (v "0.1.2") (d (list (d (n "shell-escape") (r "^0.1") (d #t) (k 0)))) (h "0di8gppm2snw9mr9q274dlqb8z2nwcsql4sda4nzq8ld0n52h2yg")))

