(define-module (crates-io ke ep keep-a-changelog) #:use-module (crates-io))

(define-public crate-keep-a-changelog-0.1.0 (c (n "keep-a-changelog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "derive-getters") (r "^0.3") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)))) (h "16357ar3zld29fywiar3756ynq60sw5q6zl6991vlm7mfmc0dscb")))

(define-public crate-keep-a-changelog-0.1.1 (c (n "keep-a-changelog") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "derive-getters") (r "^0.3") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)))) (h "1v58vfh7nzz96y4xsyldxhmi4006in1kj8vqyj13iqkx310rs2rg")))

(define-public crate-keep-a-changelog-0.1.2 (c (n "keep-a-changelog") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "derive-getters") (r "^0.3") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "derive_setters") (r "^0.1.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)))) (h "1935cqhb9mkd1f42pm21hn47fsmnf8n6wavf5bqann1lxws5wh6k")))

