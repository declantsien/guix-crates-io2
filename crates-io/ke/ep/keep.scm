(define-module (crates-io ke ep keep) #:use-module (crates-io))

(define-public crate-keep-0.1.0 (c (n "keep") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 2)) (d (n "git2") (r "^0.7.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 2)))) (h "0gy2c222v03qp41mlsd0682yrbhz89svzkxyfcj6gpmvvc02hx9b")))

