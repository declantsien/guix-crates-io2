(define-module (crates-io ke ep keepass-db-derive) #:use-module (crates-io))

(define-public crate-keepass-db-derive-0.0.1 (c (n "keepass-db-derive") (v "0.0.1") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.15") (d #t) (k 0)))) (h "0cczs5kmxapyfdyjv35d6627y2nf7pxqhlfls7gaxa5z427316qv")))

(define-public crate-keepass-db-derive-0.0.2 (c (n "keepass-db-derive") (v "0.0.2") (d (list (d (n "change-case") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.18") (d #t) (k 0)))) (h "0i6dd3jg0m2gbk5lyj5b1lmxc2c2qfiym75lbaxgg1cqx7rm096n")))

