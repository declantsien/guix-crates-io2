(define-module (crates-io ke ep keeprompt) #:use-module (crates-io))

(define-public crate-keeprompt-0.1.1 (c (n "keeprompt") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "keepass") (r "^0.4.7") (d #t) (k 0)) (d (n "pinentry") (r "^0.3.0") (d #t) (k 0)) (d (n "popol") (r "^0.4.0") (d #t) (k 0)) (d (n "secrecy") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "0rk0z150pn7k9yfxm0qvvqjx56qmgmdwn0rs469zkqry9p7j4wns")))

