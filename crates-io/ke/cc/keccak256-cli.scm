(define-module (crates-io ke cc keccak256-cli) #:use-module (crates-io))

(define-public crate-keccak256-cli-0.1.0 (c (n "keccak256-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "1afkg68dmbdxg8w6abdkcxln835d91lx4s3scw5ka8zfm2fj3b4p")))

(define-public crate-keccak256-cli-0.1.1 (c (n "keccak256-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "11v3898firjvz8hypm58m7dqnjbj45va0mhhgf5klkh5d7wn2rsy") (y #t)))

(define-public crate-keccak256-cli-0.1.2 (c (n "keccak256-cli") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "1dy68zlsg5kw9rgx2yqykh134yb3jxk0a9adrmhfkk9vjml4c566")))

(define-public crate-keccak256-cli-0.2.0 (c (n "keccak256-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "034b6rpc14bcizr8ah4v9jlfv4v5n1gbjgl68wvbiafy3larpjdv")))

(define-public crate-keccak256-cli-0.2.1 (c (n "keccak256-cli") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "12wz8xjp7ls83djyn5mcc9qpk12rlaq7zn82hkypafj6542bs77w")))

