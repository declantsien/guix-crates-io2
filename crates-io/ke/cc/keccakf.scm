(define-module (crates-io ke cc keccakf) #:use-module (crates-io))

(define-public crate-keccakf-0.1.0 (c (n "keccakf") (v "0.1.0") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "1fk6dr143b8in7nxvxw88in1wz3v85hjfm7b67jckdm2dz3c657y") (y #t)))

(define-public crate-keccakf-0.1.1 (c (n "keccakf") (v "0.1.1") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "1jyg3hqn5ann3p8hp27aj1j8i4jbrzhnm7zcmnawizhxr7k34mr3") (y #t)))

(define-public crate-keccakf-0.1.2 (c (n "keccakf") (v "0.1.2") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "0rsgxp4p3mgq02yg7dsam7x0aswfqqf812hvsqrj8hhk49spf7yc") (y #t)))

(define-public crate-keccakf-0.1.3 (c (n "keccakf") (v "0.1.3") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "1qjdiamd7hlzw46vx9nw7qj8d7xrhibx82ydkpqpncn9lj0xwjhx")))

(define-public crate-keccakf-0.2.0 (c (n "keccakf") (v "0.2.0") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "1gmw9ykiky77lap2wa2dgylbp5bh0v5pbgg3y8hhc3c03gwbmap3") (y #t)))

(define-public crate-keccakf-0.2.1 (c (n "keccakf") (v "0.2.1") (d (list (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)))) (h "1swvs7pas3ag5d5bkcfgzdrfmcr2hl67spn5sdvixjbm0njh2aby")))

