(define-module (crates-io ke cc keccak-rust) #:use-module (crates-io))

(define-public crate-keccak-rust-1.0.0 (c (n "keccak-rust") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)))) (h "0rilxyxrph10rd9swz3zas0857hai7aj2fgw7pcn1p8mjh9bc6xk") (y #t)))

(define-public crate-keccak-rust-1.0.1 (c (n "keccak-rust") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)))) (h "07lnfc7yyakpbywj5k2bx03qg8l43b9i8ld3k02ybvz3wz1lc0y4")))

(define-public crate-keccak-rust-1.0.2 (c (n "keccak-rust") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)))) (h "10qszp3z27iid6dr0llcsw1lf2g2ql2y42ky31b04a8v6vr7nyaa")))

