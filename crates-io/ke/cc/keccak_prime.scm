(define-module (crates-io ke cc keccak_prime) #:use-module (crates-io))

(define-public crate-keccak_prime-0.1.0 (c (n "keccak_prime") (v "0.1.0") (d (list (d (n "aes-gcm-siv") (r "^0.9.0") (d #t) (k 0)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0msbh914wsi6zrvr98gi55rnraydyydcbw715hamgp0vmik4k6a5") (f (quote (("tuple_hash" "cshake") ("sp800" "cshake" "kmac" "tuple_hash") ("shake") ("sha3") ("parallel_hash" "cshake") ("kmac" "cshake") ("keccak") ("k12") ("fips202" "keccak" "shake" "sha3") ("default" "fips202") ("cshake"))))))

