(define-module (crates-io ke cc keccak-asm) #:use-module (crates-io))

(define-public crate-keccak-asm-0.1.0 (c (n "keccak-asm") (v "0.1.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex") (r "^1.10") (d #t) (k 2) (p "const-hex")) (d (n "sha3-asm") (r "^0.1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (o #t) (k 0)))) (h "0i7lrab929pmxdlf2bx800m3w04wa4p5ynd1ljp51n0fz3zib1dv") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std")))) (s 2) (e (quote (("zeroize" "dep:zeroize")))) (r "1.64")))

(define-public crate-keccak-asm-0.1.1 (c (n "keccak-asm") (v "0.1.1") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex") (r "^1.10") (d #t) (k 2) (p "const-hex")) (d (n "sha3-asm") (r "^0.1.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (o #t) (k 0)))) (h "0n47j8lr3myw4s79pzqq2harvv043fyaqww6pyz4yk43j4r678s7") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std")))) (s 2) (e (quote (("zeroize" "dep:zeroize")))) (r "1.64")))

