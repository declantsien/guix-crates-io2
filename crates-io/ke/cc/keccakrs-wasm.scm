(define-module (crates-io ke cc keccakrs-wasm) #:use-module (crates-io))

(define-public crate-keccakrs-wasm-0.1.0 (c (n "keccakrs-wasm") (v "0.1.0") (d (list (d (n "keccakrs") (r "^0.1.4") (d #t) (k 0)))) (h "1ysd9r1p5b3i8bwdfc4bq9ckzv2hxnd21sf1ccgl8cc0hb9d484z")))

