(define-module (crates-io ke cc keccak-p) #:use-module (crates-io))

(define-public crate-keccak-p-0.1.0 (c (n "keccak-p") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "keccak") (r "^0.1.0") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak" "k12"))) (d #t) (k 2)))) (h "1k43zxqf4fwpwnjp35hyixf72izwm3mydb4359qk3nlm1yc9bmdi") (y #t)))

(define-public crate-keccak-p-0.1.1 (c (n "keccak-p") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "keccak") (r "^0.1.2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak" "k12"))) (d #t) (k 2)))) (h "1np0j4wz4y6k32f3m749l8cvnrfi0qrmbl1594sv15zwnpszj767") (y #t)))

