(define-module (crates-io ke cc keccakrs) #:use-module (crates-io))

(define-public crate-keccakrs-0.1.0 (c (n "keccakrs") (v "0.1.0") (h "104gz5ih1i04wsk3cw5rymiszw9jhq837whcr6ny6dfzj3wpx05i")))

(define-public crate-keccakrs-0.1.1 (c (n "keccakrs") (v "0.1.1") (h "1kd33kazzxmwvnz4s5hm429nijjcnjsmwdndd5zgbvyydvxqj7ln")))

(define-public crate-keccakrs-0.1.2 (c (n "keccakrs") (v "0.1.2") (h "1mmgv149kf3cibdsjil9nnkjqfyxi3kqy6qpqxhzk5790819bd67")))

(define-public crate-keccakrs-0.1.3 (c (n "keccakrs") (v "0.1.3") (h "08arn5lq79sgz2j25kzrz7q5rxvarv6rmsnhnplv3rhz67k7qjmk")))

(define-public crate-keccakrs-0.1.4 (c (n "keccakrs") (v "0.1.4") (h "0ciljjp3h5hz4clw5dggf4cka0n1ysjb0rm965h9z3lg21p990gg")))

(define-public crate-keccakrs-0.1.5 (c (n "keccakrs") (v "0.1.5") (h "19dqbqyg88ppyp06wah5adk3x4zc5xczzdml5h2xy6fmkxyqsbjh")))

(define-public crate-keccakrs-0.1.6 (c (n "keccakrs") (v "0.1.6") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)))) (h "18cy0nq6zsq44h7y0w9x9g9jw0yxj0v2jj4acirqp8630v1fbfg0")))

