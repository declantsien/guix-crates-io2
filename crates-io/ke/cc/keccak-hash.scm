(define-module (crates-io ke cc keccak-hash) #:use-module (crates-io))

(define-public crate-keccak-hash-0.1.0 (c (n "keccak-hash") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "ethcore-bigint") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1.3") (d #t) (k 0)))) (h "1hcy4lbajjl2k5ab3f0g88bpan9sf5p4zlpf2i9cmncw2hghqc0z")))

(define-public crate-keccak-hash-0.1.1 (c (n "keccak-hash") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "ethereum-types") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1.3") (d #t) (k 0)))) (h "1rrsb8wvr4jjlriji2pypwyzp380i2iadi5frim571kr1prm2zqb")))

(define-public crate-keccak-hash-0.1.2 (c (n "keccak-hash") (v "0.1.2") (d (list (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "11jahlbkbp9alqbss1cp2fxyxvdgiwj8hnpsb2zidj1j7ijbwfr5")))

(define-public crate-keccak-hash-0.2.0 (c (n "keccak-hash") (v "0.2.0") (d (list (d (n "primitive-types") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "0r8asz83xbn0qgj5jnvscahn69q5lmgmcmvn9p1xr9lsgdlyxs39")))

(define-public crate-keccak-hash-0.3.0 (c (n "keccak-hash") (v "0.3.0") (d (list (d (n "primitive-types") (r "^0.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "00q9yak0rr05r81r72f951hyz39sn0kjdznx60klr5xj3p54ydh9")))

(define-public crate-keccak-hash-0.4.0 (c (n "keccak-hash") (v "0.4.0") (d (list (d (n "primitive-types") (r "^0.6") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "167rqwsyis4vl2mw7infp3k7avrfrgr8n4a6904qc9ibwmpzlqz5") (f (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.4.1 (c (n "keccak-hash") (v "0.4.1") (d (list (d (n "primitive-types") (r "^0.6") (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1.5.0") (d #t) (k 0)))) (h "116731dlnmlbngc34qnc2zfvcpd2c04xfk0wqa0w5za2ybc5a3jg") (f (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.5.0 (c (n "keccak-hash") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1q7c5cj3fpizjy9sqjda42hk4pal314fpmb5dlfwjp74n2gr8r94") (f (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.5.1 (c (n "keccak-hash") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "13sc1899lmw8fgw725gb1y6p9yv1xgaaigil4k7ri4yzycgaan0z") (f (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.6.0 (c (n "keccak-hash") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "primitive-types") (r "^0.8") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0wh0j0vkiw6dzp1397mmkkvvblslvpmi4s174v3abngsyvwa621j") (f (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.7.0 (c (n "keccak-hash") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "primitive-types") (r "^0.9") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0lfc9ighklgq0farbdkwxvrny58pm3rjnhd3m8hxfvf2k3n8c0xf") (f (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.8.0 (c (n "keccak-hash") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "primitive-types") (r "^0.10") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0l98snc1pkddbs2inxzdq0gj5x0af34bvxxdxb9j9rvhjb1d8ayf") (f (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.9.0 (c (n "keccak-hash") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "primitive-types") (r "^0.11") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1qy9irq81m0f72y81l42xdkslz9yz732mxhmjrhngc25ldf5vg42") (f (quote (("std") ("default" "std")))) (r "1.56.1")))

(define-public crate-keccak-hash-0.10.0 (c (n "keccak-hash") (v "0.10.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "primitive-types") (r "^0.12") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0g3d9hayhni1n8him1v5dmnd83mpkkk2i1dnxvhjdf9zcrmnwa2b") (f (quote (("std") ("default" "std")))) (r "1.56.1")))

