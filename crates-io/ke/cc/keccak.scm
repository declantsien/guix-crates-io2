(define-module (crates-io ke cc keccak) #:use-module (crates-io))

(define-public crate-keccak-0.0.0 (c (n "keccak") (v "0.0.0") (h "1nm5ddirs2sk4cc2666qyizncv0azn0fzyq39vrbn41ssr0b5ihd") (y #t)))

(define-public crate-keccak-0.1.0 (c (n "keccak") (v "0.1.0") (h "19ybbvxrdk9yy65rk7f5ad0hcxszkjwph68yzkj3954lnir1bhk7") (f (quote (("no_unroll"))))))

(define-public crate-keccak-0.1.1 (c (n "keccak") (v "0.1.1") (d (list (d (n "packed_simd") (r "^0.3.7") (o #t) (d #t) (k 0) (p "packed_simd_2")))) (h "1kgp4vxsfpnp2rnv2dn48kr1sv5w83bjrjc3qnpdsx4kkqqpw4a8") (f (quote (("simd" "packed_simd") ("no_unroll"))))))

(define-public crate-keccak-0.1.2 (c (n "keccak") (v "0.1.2") (h "0f78zsqzdjwhwfgzgkcqk7r8m4kgn3k9b6bjx5mlsd58limxbdzr") (f (quote (("simd") ("no_unroll"))))))

(define-public crate-keccak-0.1.3 (c (n "keccak") (v "0.1.3") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(target_arch = \"aarch64\")") (k 0)))) (h "0s47r5bgllyhapri9gbsmk5wiw67xqji2q5kz67rvkprxyvg7zis") (f (quote (("simd") ("no_unroll") ("asm"))))))

(define-public crate-keccak-0.1.4 (c (n "keccak") (v "0.1.4") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(target_arch = \"aarch64\")") (k 0)))) (h "0h7rcvwvf20g4k8cx2brnrqvah6jwzs84w09vrj4743dczc5wvcg") (f (quote (("simd") ("no_unroll") ("asm"))))))

(define-public crate-keccak-0.1.5 (c (n "keccak") (v "0.1.5") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(target_arch = \"aarch64\")") (k 0)))) (h "0m06swsyd58hvb1z17q6picdwywprf1yf1s6l491zi8r26dazhpc") (f (quote (("simd") ("no_unroll") ("asm"))))))

(define-public crate-keccak-0.2.0-pre.0 (c (n "keccak") (v "0.2.0-pre.0") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(target_arch = \"aarch64\")") (k 0)))) (h "143y5m19fvp3rlgg8ixxwxpn0qp8b0zqmm2xnaibj1sqvkqd9kd7") (f (quote (("simd") ("no_unroll") ("asm")))) (r "1.60")))

