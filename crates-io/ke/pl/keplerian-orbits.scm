(define-module (crates-io ke pl keplerian-orbits) #:use-module (crates-io))

(define-public crate-keplerian-orbits-0.1.0 (c (n "keplerian-orbits") (v "0.1.0") (d (list (d (n "glam") (r "^0.22") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.19") (d #t) (k 2)) (d (n "bevy_prototype_debug_lines") (r "^0.9") (f (quote ("3d"))) (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.7") (d #t) (k 2)))) (h "1q4b90qd4azv4prb6jspfqjk0js61sirv0n158gjsayhxdm6fi8c")))

(define-public crate-keplerian-orbits-0.2.0 (c (n "keplerian-orbits") (v "0.2.0") (d (list (d (n "glam") (r "^0.22") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.19") (d #t) (k 2)) (d (n "bevy_prototype_debug_lines") (r "^0.9") (f (quote ("3d"))) (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.7") (d #t) (k 2)))) (h "0gi1qi3r73fbp2i7kmwns0x5qhvws294ksc3zz8jy5ki1iyn7pil")))

