(define-module (crates-io ke pl keplerian-elements) #:use-module (crates-io))

(define-public crate-keplerian-elements-0.2.0 (c (n "keplerian-elements") (v "0.2.0") (d (list (d (n "glam") (r "^0.22") (d #t) (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)) (d (n "bevy_egui") (r "^0.19") (d #t) (k 2)) (d (n "bevy_prototype_debug_lines") (r "^0.9") (f (quote ("3d"))) (d #t) (k 2)) (d (n "smooth-bevy-cameras") (r "^0.7") (d #t) (k 2)))) (h "1kqarmwvcvi5497fzfy2ygi2d4rsgyh2vz1ymvwqaj2d08yrdnry")))

