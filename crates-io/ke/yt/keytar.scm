(define-module (crates-io ke yt keytar) #:use-module (crates-io))

(define-public crate-keytar-0.1.0 (c (n "keytar") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "cxx") (r "^0.3.4") (d #t) (k 0)) (d (n "cxx-build") (r "^0.3.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "182cwhbqp1ypqy9xgazl81i9sg2pppv25l3zzd0a2rwmrg5qdi1d") (y #t)))

(define-public crate-keytar-0.1.1 (c (n "keytar") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "cxx") (r "^0.3.4") (d #t) (k 0)) (d (n "cxx-build") (r "^0.3.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0d8mw2h97vfhlg8kfhpqbrysjrgq77mwn243jj15gqlh849f3x4r") (f (quote (("docs-rs")))) (y #t)))

(define-public crate-keytar-0.1.2 (c (n "keytar") (v "0.1.2") (d (list (d (n "keytar-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1c25ik8g1i250bq274d48rd31kl6lkw8jyis90zqnibdk9i7n70x")))

(define-public crate-keytar-0.1.3 (c (n "keytar") (v "0.1.3") (d (list (d (n "keytar-sys") (r "^0.1.2") (d #t) (k 0)))) (h "138r71h2yx2kw7if2ssbal1xjb9s25d8saycz67r81n6v8aclmw0") (f (quote (("docs-rs" "keytar-sys/docs-rs"))))))

(define-public crate-keytar-0.1.4 (c (n "keytar") (v "0.1.4") (d (list (d (n "keytar-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1c7gps24m1r8klnnn371pabakr8fkiivw7jbchn029dzkmm6jxyz") (f (quote (("docs-rs" "keytar-sys/docs-rs"))))))

(define-public crate-keytar-0.1.5 (c (n "keytar") (v "0.1.5") (d (list (d (n "keytar-sys") (r "^0.1.5") (d #t) (k 0)))) (h "1525mrj7bn8xxdkjra2w63ld77zp6hxsvijjjssh81wbrh5njigl") (f (quote (("docs-rs" "keytar-sys/docs-rs"))))))

(define-public crate-keytar-0.1.6 (c (n "keytar") (v "0.1.6") (d (list (d (n "keytar-sys") (r "^0.1.6") (d #t) (k 0)))) (h "0kfagn3dmj6a9al20s1x1h1v2fgjbd1gah5h4339m0h9p9gwaqfk") (f (quote (("docs-rs" "keytar-sys/docs-rs"))))))

