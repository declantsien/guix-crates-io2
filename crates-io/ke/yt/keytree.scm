(define-module (crates-io ke yt keytree) #:use-module (crates-io))

(define-public crate-keytree-0.2.0 (c (n "keytree") (v "0.2.0") (h "1aiclmj1v1x8i4dwiybkv2b2q0491aly3gldqzqi7qhv3k51ywb8")))

(define-public crate-keytree-0.2.1 (c (n "keytree") (v "0.2.1") (h "0kc4m57dmdyybq88wmlzc067q87cyxslx1s46y18pvn57slv16sn")))

(define-public crate-keytree-0.2.2 (c (n "keytree") (v "0.2.2") (h "1rqrmz5dla16cpv06mq34445vfql4cjfzyc3s46zj0khx6s1dw1k")))

(define-public crate-keytree-0.2.3 (c (n "keytree") (v "0.2.3") (h "0ihq9q46r4209xw3hbawiq69knxkbypva1mcf9mpgsdy4hndlfyp")))

(define-public crate-keytree-0.2.4 (c (n "keytree") (v "0.2.4") (h "07d19y7f7w00024k8snl4vm41108vd39j9by2qhiz7dan6b5cdyd")))

