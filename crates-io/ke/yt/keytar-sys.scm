(define-module (crates-io ke yt keytar-sys) #:use-module (crates-io))

(define-public crate-keytar-sys-0.1.2 (c (n "keytar-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "cxx") (r "^0.4.0") (d #t) (k 0)) (d (n "cxx-build") (r "^0.4.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "11cdjd9vn9wd1b850fm7saa8fiym6hcchrlpj20h25693q1h0p2m") (f (quote (("docs-rs"))))))

(define-public crate-keytar-sys-0.1.4 (c (n "keytar-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "cxx") (r "^1.0.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1zhzkhia45wxkz2nsyr8s8ja7svx2k1jfwjqdykcc1cckjz0p88y") (f (quote (("docs-rs"))))))

(define-public crate-keytar-sys-0.1.5 (c (n "keytar-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "cxx") (r "^1.0.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1j4y70zjy40p63gxsqalx9nbcb18jnkx25l90cmxrpyhrjn6j9s8") (f (quote (("docs-rs"))))))

(define-public crate-keytar-sys-0.1.6 (c (n "keytar-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "cxx") (r "^1.0.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0nm6wvhr7kcq78jr66z8mjaqvw33djamssnd2ssiqnkhjrl8r47y") (f (quote (("docs-rs"))))))

