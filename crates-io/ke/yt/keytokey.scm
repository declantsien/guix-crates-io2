(define-module (crates-io ke yt keytokey) #:use-module (crates-io))

(define-public crate-keytokey-0.1.0 (c (n "keytokey") (v "0.1.0") (d (list (d (n "heapless") (r "^0.4.4") (d #t) (k 0)) (d (n "no-std-compat") (r "^0.1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.3.1") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.4.0") (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0b9s4ayyh28k0c9926yiwm1d1ljd45gbxvbhxh3g5rh0xfbxri4i")))

(define-public crate-keytokey-0.2.0 (c (n "keytokey") (v "0.2.0") (d (list (d (n "heapless") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "no-std-compat") (r "^0.1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.3.1") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.4.0") (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "03nzdxv69sxbbmd3q6pcx0fa1796xcl49ch2z5lnykidym6xjcab")))

(define-public crate-keytokey-0.3.0 (c (n "keytokey") (v "0.3.0") (d (list (d (n "heapless") (r "^0.4.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "no-std-compat") (r "^0.1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.3.1") (d #t) (k 0)) (d (n "smallbitvec") (r "^2.5.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "0vvmdbp272592h762b07924184k4m3k4gjrr627f92gsidrafnza")))

