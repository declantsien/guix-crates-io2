(define-module (crates-io ke rr kerr) #:use-module (crates-io))

(define-public crate-kerr-0.1.0 (c (n "kerr") (v "0.1.0") (h "0gfk774jjiagglc377p1hpv8yn2f4saw6pkxksklhniay47wrsfy")))

(define-public crate-kerr-0.1.1 (c (n "kerr") (v "0.1.1") (h "0lpkqzb9zlx0h8cjj29ihvmm7w3gjhfgi8asd9jwwlpcbdzqx1s2")))

(define-public crate-kerr-0.1.2 (c (n "kerr") (v "0.1.2") (h "1v35j38dnf5dq75mrhh8aimb6aw4dx3qshffafzyx4jgvw86523j")))

(define-public crate-kerr-0.1.3 (c (n "kerr") (v "0.1.3") (h "0ynnqa5kq7m1c5rla0wr8b9qfw61hax9lx38fbs2hafqnc025wkg")))

(define-public crate-kerr-0.1.4 (c (n "kerr") (v "0.1.4") (h "0jg3l1f2z7z9f4c48z1wqikdg94arz097d3ybz6lw0krcmbifkam")))

