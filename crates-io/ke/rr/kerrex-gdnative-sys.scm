(define-module (crates-io ke rr kerrex-gdnative-sys) #:use-module (crates-io))

(define-public crate-kerrex-gdnative-sys-0.1.2 (c (n "kerrex-gdnative-sys") (v "0.1.2") (h "029bqxiajs2x02gm9fbbzl27zf7pxhd9a35y3xpc6lfjm5snq5jz")))

(define-public crate-kerrex-gdnative-sys-0.1.3 (c (n "kerrex-gdnative-sys") (v "0.1.3") (h "1d442xsl188fip52gnqfh3q99gr8ci1mq4azs1kywn5cvbafmkn3")))

