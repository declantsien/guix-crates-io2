(define-module (crates-io ke xp kexplain) #:use-module (crates-io))

(define-public crate-kexplain-0.1.0 (c (n "kexplain") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0q9f6x6jxbz95dapgvqqq6s2qmbz6dnyw52ijgsdxmsndxrgxikj")))

