(define-module (crates-io ke ym keymaps) #:use-module (crates-io))

(define-public crate-keymaps-0.0.0 (c (n "keymaps") (v "0.0.0") (h "0n6dfg61ni6qf7a410f3vl2zs8fqg6h8hlk56cj6axgh8qfhmhaz")))

(define-public crate-keymaps-0.1.0 (c (n "keymaps") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (f (quote ("grammar-extras"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0bbzpyrl2vsc1xm61z3wvq1jc0il6dbx4b39gbl9i2ia74p2r0wv") (f (quote (("default")))) (s 2) (e (quote (("crossterm" "dep:crossterm"))))))

(define-public crate-keymaps-0.1.1 (c (n "keymaps") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (f (quote ("grammar-extras"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1aplnvvk1jdpl30msr9qc4hbjsvhad8x0b5h0chczqfw6xh57vcm") (f (quote (("default")))) (s 2) (e (quote (("crossterm" "dep:crossterm"))))))

