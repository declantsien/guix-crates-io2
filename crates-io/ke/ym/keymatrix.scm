(define-module (crates-io ke ym keymatrix) #:use-module (crates-io))

(define-public crate-keymatrix-0.1.0 (c (n "keymatrix") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "generic-array") (r "~0.11") (d #t) (k 0)))) (h "0fwkaz93r0s3j7jslykrm09zw2jp3dw9clkw8swr3yjhjldwpspa")))

(define-public crate-keymatrix-0.1.1 (c (n "keymatrix") (v "0.1.1") (d (list (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "generic-array") (r "~0.11") (d #t) (k 0)))) (h "0gmrf6y2y11xvfdvf0ih5n02nd2dl0ar44hfx09kjwkiy35xprwv")))

(define-public crate-keymatrix-0.1.2 (c (n "keymatrix") (v "0.1.2") (d (list (d (n "atsamd21-hal") (r "~0.2") (d #t) (k 2)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 2)) (d (n "cortex-m-rt") (r "~0.6") (d #t) (k 2)) (d (n "cortex-m-rtfm") (r "~0.3") (d #t) (k 2)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 2)) (d (n "generic-array") (r "~0.11") (d #t) (k 0)) (d (n "nb") (r "~0.1") (d #t) (k 2)) (d (n "panic-abort") (r "~0.2") (d #t) (k 2)) (d (n "samd21_mini") (r "~0.1") (d #t) (k 2)) (d (n "samd21g18a") (r "~0.2") (d #t) (k 2)))) (h "12qrhl9vca5a28r8wkd12zij111gawc48q3fz1w2kc1w4k50ndjq") (f (quote (("samd21" "atsamd21-hal/samd21g18a-rt" "samd21_mini/rt" "samd21_mini/unproven"))))))

