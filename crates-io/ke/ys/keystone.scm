(define-module (crates-io ke ys keystone) #:use-module (crates-io))

(define-public crate-keystone-0.9.0 (c (n "keystone") (v "0.9.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1zi1shnplv1y80bsc7q2lvvmrjll0lxj6y4z7byabwkvyxbd2284")))

