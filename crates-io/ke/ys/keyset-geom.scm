(define-module (crates-io ke ys keyset-geom) #:use-module (crates-io))

(define-public crate-keyset-geom-0.3.0 (c (n "keyset-geom") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "kurbo") (r "^0.10") (d #t) (k 0)))) (h "1f0s3gdvs6j7pz9wqqspn9wb09frinjg57wvxhjlhgsnsjqgcriq") (r "1.70")))

(define-public crate-keyset-geom-0.3.1 (c (n "keyset-geom") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "kurbo") (r "^0.10") (d #t) (k 0)))) (h "07hifyxxcpm5flv44d92d6krk2agyxx888hpv16r85cbd8wakyrc") (r "1.70")))

(define-public crate-keyset-geom-0.3.2 (c (n "keyset-geom") (v "0.3.2") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "kurbo") (r "^0.10") (d #t) (k 0)))) (h "0rqq1avar2jqcwinhi9f506a04zrk5aqrk1fw98amqsakq4790sx") (r "1.70")))

