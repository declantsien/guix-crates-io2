(define-module (crates-io ke ys keysymdefs) #:use-module (crates-io))

(define-public crate-keysymdefs-0.1.0 (c (n "keysymdefs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "07r9plrdqcgalc22lnwa8czr6ch6jjb4vsvsflz2ak8caxgjgfri")))

(define-public crate-keysymdefs-0.2.0 (c (n "keysymdefs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1wdl4r9mmja7snnkm5vk2rpkf1bacah24yjrd8rlc5a9avian15d")))

