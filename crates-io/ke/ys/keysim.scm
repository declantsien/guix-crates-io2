(define-module (crates-io ke ys keysim) #:use-module (crates-io))

(define-public crate-keysim-0.1.0 (c (n "keysim") (v "0.1.0") (d (list (d (n "x11") (r "^2") (f (quote ("xlib" "xtest"))) (o #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "minwindef"))) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0zjj9kbxfx7f1w7ms46bxgr9wxkrrc7w70x67076wwii345g3rvp") (f (quote (("default" "x11"))))))

