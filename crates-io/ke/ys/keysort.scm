(define-module (crates-io ke ys keysort) #:use-module (crates-io))

(define-public crate-keysort-0.1.0-alpha (c (n "keysort") (v "0.1.0-alpha") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "1fna0gybw7man7qdv776r81w5wzxmsivj4pvhvjbv48npisqxvxm")))

(define-public crate-keysort-0.1.0-alpha.2 (c (n "keysort") (v "0.1.0-alpha.2") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "11p3m86l2p0dhrdcy13vm69v7145ifajh3w9bcl27s8ygr1l496b")))

(define-public crate-keysort-0.1.0-alpha.2B (c (n "keysort") (v "0.1.0-alpha.2B") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "0pngyvpsxhgfv5y7rwplc3dm0vxnbmvp9n3236kv85k3zn9zd3qg")))

(define-public crate-keysort-0.1.0-alpha.2C (c (n "keysort") (v "0.1.0-alpha.2C") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "01rdvmk2s1yp8hdpmgnf7wm0bxx2vf9k1778h6ipbx5zks0dyl0p")))

