(define-module (crates-io ke ys keystroke) #:use-module (crates-io))

(define-public crate-keystroke-0.0.2 (c (n "keystroke") (v "0.0.2") (d (list (d (n "user32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1xfz2zsk98g4yvj5hpl3yxhka9gfx5kcs5cw2c45lgfsy6n14zsj")))

(define-public crate-keystroke-0.0.3 (c (n "keystroke") (v "0.0.3") (d (list (d (n "user32-sys") (r "^0") (d #t) (k 0)) (d (n "winapi") (r "^0") (d #t) (k 0)))) (h "15p9i5p4z7dmnmqz4sqagd1xk16w4kf08wdixyihbhrkzg6aky81")))

