(define-module (crates-io ke ys keysmith) #:use-module (crates-io))

(define-public crate-keysmith-0.1.0 (c (n "keysmith") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rmvf6ggsfykir49qwvf8rpspr46nw3jgpxsaw1vn8jcdk43kvhr")))

(define-public crate-keysmith-0.1.1 (c (n "keysmith") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0gqdlpmfgnxai5znw31lvg41xws1ywy908giz5nr5l1bpj55c34z")))

(define-public crate-keysmith-0.2.0 (c (n "keysmith") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kpk75wfk56yjcin6m2g1wkx2nswpj6668d2lkd825a06kvdpm7m")))

(define-public crate-keysmith-0.2.1 (c (n "keysmith") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yhfcq1ki93ryylmdg4va84qr1n27d2cffd5dpkkq10fvd1mfy08")))

(define-public crate-keysmith-0.3.0 (c (n "keysmith") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02zhyqm4imf4913p77cynwsidyn16s4qlhxjanxlm4344i8gisdp")))

(define-public crate-keysmith-0.3.1 (c (n "keysmith") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0baclg4rbpzfcbhzkp63av2155d6gg5ygm740ss8vjnx8q39s3v1")))

(define-public crate-keysmith-0.4.0 (c (n "keysmith") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pr3bkg90sqmmnfn09d6zv3js8ii8yjgs2dw27144nv4c7w5awrp")))

