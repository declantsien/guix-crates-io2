(define-module (crates-io ke ys keyset-color) #:use-module (crates-io))

(define-public crate-keyset-color-0.3.0 (c (n "keyset-color") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "rgb") (r "^0.8") (o #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (f (quote ("std"))) (o #t) (k 0)))) (h "1r2c7w0yaky0jlhbxhzx4cz4zb6dl0n72gsr1j8ycqswybj3m3hm") (r "1.70")))

(define-public crate-keyset-color-0.3.1 (c (n "keyset-color") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "rgb") (r "^0.8") (o #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (f (quote ("std"))) (o #t) (k 0)))) (h "16dd7pd6afaaysrzi6hwq7knrp2iys56sf7jcb8hxgz06v81a8br") (r "1.70")))

(define-public crate-keyset-color-0.3.2 (c (n "keyset-color") (v "0.3.2") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "rgb") (r "^0.8") (o #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (f (quote ("std"))) (o #t) (k 0)))) (h "09wv834hj643kwjidb6q6chdifvch6m61m0f3qazkm4bf2p29m61") (r "1.70")))

