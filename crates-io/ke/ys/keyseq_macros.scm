(define-module (crates-io ke ys keyseq_macros) #:use-module (crates-io))

(define-public crate-keyseq_macros-0.1.0 (c (n "keyseq_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 0)))) (h "0x22w972gp78ma3d14gdsqr7f33vpdchsdfnbcnwxpszj5cbiahf") (f (quote (("winit") ("strict-order") ("poor") ("default" "strict-order") ("bevy-input-sequence") ("bevy"))))))

(define-public crate-keyseq_macros-0.2.0 (c (n "keyseq_macros") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 0)))) (h "18pbsx2c9xv45nz2bqi6133ffimfkgdni4gd3k779javnhm29gdk") (f (quote (("winit") ("strict-order") ("poor") ("default" "strict-order") ("bevy-input-sequence") ("bevy"))))))

(define-public crate-keyseq_macros-0.1.1 (c (n "keyseq_macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 0)))) (h "0msbanch4899ccji3i468klb4mmfyq8zx95hv26r3zm1qq165qbh") (f (quote (("winit") ("strict-order") ("poor") ("default" "strict-order") ("bevy-input-sequence") ("bevy"))))))

(define-public crate-keyseq_macros-0.2.1 (c (n "keyseq_macros") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 0)))) (h "087mi5lw0q0mz83lx266qs6wc166qi711mbci3kd84qg38pmsw15") (f (quote (("winit") ("strict-order") ("poor") ("default" "strict-order") ("bevy-input-sequence") ("bevy"))))))

(define-public crate-keyseq_macros-0.2.2 (c (n "keyseq_macros") (v "0.2.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 0)))) (h "1477mcl7i67wb41y5b10c6yv0x6l257x2zzmbc7a9r3wxxx4b6sk") (f (quote (("winit") ("strict-order") ("poor") ("default" "strict-order") ("bevy-input-sequence") ("bevy"))))))

