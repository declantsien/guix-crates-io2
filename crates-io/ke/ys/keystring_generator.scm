(define-module (crates-io ke ys keystring_generator) #:use-module (crates-io))

(define-public crate-keystring_generator-0.1.0 (c (n "keystring_generator") (v "0.1.0") (h "13grav1a2plsykqrigagn738dvk5gnkhndbsxl1y7j6rwxwlh7yh")))

(define-public crate-keystring_generator-0.1.1 (c (n "keystring_generator") (v "0.1.1") (h "03mf3w9dd8y5dn7xa5bl9s6kdsl6879rynyih02f3nyb4gkiajc1")))

(define-public crate-keystring_generator-0.1.2 (c (n "keystring_generator") (v "0.1.2") (h "07042wnnyviclbcs3kdkqaam5r53z6jf58ghk374lfms4ggnq5f6")))

