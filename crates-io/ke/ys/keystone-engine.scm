(define-module (crates-io ke ys keystone-engine) #:use-module (crates-io))

(define-public crate-keystone-engine-0.1.0 (c (n "keystone-engine") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1xd1ypy1ln270n4s7111rn98hy0cd68ikby8dhrwn1hhgvi3m45b") (f (quote (("use-system-lib" "pkg-config") ("default" "build-from-src") ("build-from-src" "cmake")))) (l "keystone")))

