(define-module (crates-io ke et keetree) #:use-module (crates-io))

(define-public crate-keetree-0.0.1 (c (n "keetree") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "matchit") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (f (quote ("perf" "unicode"))) (k 0)))) (h "1cnwbmnp72rjylsq1gjq1nnjm6qvm97bg2hnq2b7mn8bgsv163x2") (f (quote (("std" "regex/std") ("default" "std"))))))

