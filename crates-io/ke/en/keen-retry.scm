(define-module (crates-io ke en keen-retry) #:use-module (crates-io))

(define-public crate-keen-retry-0.1.0 (c (n "keen-retry") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "037nc030yyvyc9g97gl5nr7cwcz19gwc14f3jnchmm8h9lklzri3")))

(define-public crate-keen-retry-0.1.1 (c (n "keen-retry") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mxwcydv0bj65jh7bba1i80na0jpiqwkn6aq7w7xi0ppiyz05ja7")))

(define-public crate-keen-retry-0.1.2 (c (n "keen-retry") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11243n2zcq9557g04g9r6diwd7lb1diqx5bkrwipvvpz29cgm4c5")))

(define-public crate-keen-retry-0.1.3 (c (n "keen-retry") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07r6brv1l2p2zxrnv5dl74vvj5d7fkmpkzcgxam909h7x5imia9f")))

(define-public crate-keen-retry-0.1.4 (c (n "keen-retry") (v "0.1.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rvz3nk2lrhr01l0dq00vnmv52s4jicwjsv9mjag8pcl6dfhxlcg")))

(define-public crate-keen-retry-0.1.5 (c (n "keen-retry") (v "0.1.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gzwjqd0d3vjhzxp478d0i3w33y5d5yjd9nlrdz39y6gg5pna77d")))

(define-public crate-keen-retry-0.1.6 (c (n "keen-retry") (v "0.1.6") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hm1vkzxdxnmm66021xk48hwb457v75f6xiyyqha1q6fvxs1k7da")))

(define-public crate-keen-retry-0.1.7 (c (n "keen-retry") (v "0.1.7") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dg4jx82lgvsvpq3xhk4vnm5ids8bvianhn2f13lzvjw1c1zkwxs")))

(define-public crate-keen-retry-0.1.8 (c (n "keen-retry") (v "0.1.8") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17svnk7wdkla32ilk7l3g4nl8g1g5jrlmwm6hqnxfhdkbj60kj4d")))

(define-public crate-keen-retry-0.2.0 (c (n "keen-retry") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06m0qq0inl7bgd9vv095dspqsmvqz86qfn6mi8b6nn5kdljb8fcr")))

(define-public crate-keen-retry-0.2.1 (c (n "keen-retry") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1iv2s20aqnrlwj3gbih0im0j5yzbvay911245hfa2nxn0gicw6f5")))

(define-public crate-keen-retry-0.2.2 (c (n "keen-retry") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xa0dslkp5smivhwmgbjljiwvpjzfm74l0qylvslsr94bfzvjf19")))

(define-public crate-keen-retry-0.3.0 (c (n "keen-retry") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0f2hc23yzca1jzzhz9is62vag842mrnyjg5a3pzspjlsnkq4hxjk") (f (quote (("no-async") ("default" "async") ("async" "tokio" "futures"))))))

(define-public crate-keen-retry-0.3.1 (c (n "keen-retry") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1nm01wfr3a00c6k380bdrdah5n6g8lh181q5bj3id1qvw892q5bp") (f (quote (("no-async") ("default" "async") ("async" "tokio" "futures"))))))

(define-public crate-keen-retry-0.3.2 (c (n "keen-retry") (v "0.3.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0wrvxykn0ivq4yzjpml9j4zfa6p4173qiw2q5ca1n29hm4aqvjkz") (f (quote (("no-async") ("default" "async") ("async" "tokio" "futures"))))))

(define-public crate-keen-retry-0.4.0 (c (n "keen-retry") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0xkw0d2srja1hxzh8fk3zn6g6wr5jjxiy9392g3p0j7y02z2cbwl") (f (quote (("no-async") ("default" "async") ("async" "tokio" "futures"))))))

(define-public crate-keen-retry-0.4.1 (c (n "keen-retry") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "time" "macros"))) (o #t) (d #t) (k 0)))) (h "0as1gf9afma7igcyxw2x48gxzw3hjg6n1sqxp1hdrwh7winrjg3y") (f (quote (("no-async") ("default" "async") ("async" "tokio" "futures"))))))

