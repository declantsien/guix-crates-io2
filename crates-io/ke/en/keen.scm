(define-module (crates-io ke en keen) #:use-module (crates-io))

(define-public crate-keen-0.1.0 (c (n "keen") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "hyper") (r "^0.6.14") (d #t) (k 0)) (d (n "itertools") (r "^0.4.0") (d #t) (k 0)))) (h "1az8b02bidp5wk4kvkm61i6m7kvqb860hq873jqin2g6wkbdnqly")))

(define-public crate-keen-0.1.1 (c (n "keen") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "hyper") (r "^0.6.14") (d #t) (k 0)) (d (n "itertools") (r "^0.4.0") (d #t) (k 0)))) (h "032k2a81ad390n2r5l7dczfwm3zdrlx1273ha1zynqi41q3qbwwq")))

(define-public crate-keen-0.2.0 (c (n "keen") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "hyper") (r "^0.6.14") (d #t) (k 0)) (d (n "itertools") (r "^0.4.0") (d #t) (k 0)))) (h "0wlsdsra59ylafqd12vf2bswvqmchwnvlnrpafhy3fig6jkwgzgm")))

(define-public crate-keen-0.2.1 (c (n "keen") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "hyper") (r "^0.6.14") (d #t) (k 0)) (d (n "itertools") (r "^0.4.0") (d #t) (k 0)))) (h "0xqrbdin38626g0mys6vfxba5353zd0nd1qq3wnc4sdh263gi7cy")))

(define-public crate-keen-0.3.0 (c (n "keen") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "hyper") (r "^0.6.15") (f (quote ("timeouts"))) (d #t) (k 0)) (d (n "itertools") (r "^0.4.0") (d #t) (k 0)))) (h "1h1qja6fym4jr00i4x17rk04zyips09yfzqgv6gpi4mvwk4vkykc")))

(define-public crate-keen-1.0.1 (c (n "keen") (v "1.0.1") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "clippy") (r "^0.0.21") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.6.15") (f (quote ("timeouts"))) (d #t) (k 0)) (d (n "itertools") (r "^0.4.0") (d #t) (k 0)))) (h "1wd31ydlwd2xaksd5bw00w689wdcy3b4hp80g2kdc6j0imzkc8ib") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-keen-1.0.2 (c (n "keen") (v "1.0.2") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "clippy") (r "^0.0.21") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.6.15") (f (quote ("timeouts"))) (d #t) (k 0)) (d (n "itertools") (r "^0.4.0") (d #t) (k 0)))) (h "1r3svfxlkxlc23kp9xlag8imq33p7v2fphnfbfwn7abxsql9zwhp") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-keen-1.0.3 (c (n "keen") (v "1.0.3") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "clippy") (r "^0.0.21") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.6.15") (f (quote ("timeouts"))) (d #t) (k 0)) (d (n "itertools") (r "^0.4.0") (d #t) (k 0)))) (h "0vvcvyz2kbwp6c4x8rpm3bmm8jnnii18lig14cadpc39limgh45i") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-keen-1.0.4 (c (n "keen") (v "1.0.4") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "clippy") (r "^0.0.21") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.6.15") (f (quote ("timeouts"))) (d #t) (k 0)) (d (n "itertools") (r "^0.4.0") (d #t) (k 0)))) (h "07j0iib5d1i4iw880s3jfi3p4chw5klbdv82rzw9zzkhnmni2876") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-keen-1.1.0 (c (n "keen") (v "1.1.0") (d (list (d (n "chrono") (r "^0.2.16") (d #t) (k 0)) (d (n "clippy") (r "^0.0.21") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.6.15") (f (quote ("timeouts"))) (d #t) (k 0)) (d (n "itertools") (r "^0.4.0") (d #t) (k 0)))) (h "0p45ckbz45lp96h188gili1i6fy21699cpvlqrln95n52pp0lr32") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-keen-1.2.0 (c (n "keen") (v "1.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clippy") (r "^0.0.90") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "0vyqwq1s3wgphd0znf4kax0ac11nivv3qxg4n48a3nqwi11dk0lv") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-keen-1.3.0 (c (n "keen") (v "1.3.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clippy") (r "^0.0.90") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)))) (h "0anvc8v7qsbpmcy1k4xp3wc9551ydx5ihd817lmqsnf3w5dic8a7") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-keen-1.4.0 (c (n "keen") (v "1.4.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0.118") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.0.3") (d #t) (k 2)) (d (n "structopt-derive") (r "^0.0.3") (d #t) (k 2)))) (h "1hmk6ygn2dqcqzmnhrbdhhq5x9bm9b2sc4mis1y1606rvb0k5x1j") (f (quote (("dev" "clippy") ("default")))) (y #t)))

(define-public crate-keen-1.4.1 (c (n "keen") (v "1.4.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0.118") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.10.0") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.0.3") (d #t) (k 2)) (d (n "structopt-derive") (r "^0.0.3") (d #t) (k 2)))) (h "0pz99q4pv9c2jr1zghj4l2ii65dvyab75nip2pmpp2jw504qg1nw") (f (quote (("dev" "clippy") ("default"))))))

