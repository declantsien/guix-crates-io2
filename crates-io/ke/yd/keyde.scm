(define-module (crates-io ke yd keyde) #:use-module (crates-io))

(define-public crate-keyde-0.1.0 (c (n "keyde") (v "0.1.0") (d (list (d (n "glam") (r "^0.22") (o #t) (d #t) (k 0)))) (h "162i6jk65na60jqn2a3jbs8saxmb4z3dcqniqlcj7622pjswpx6f") (f (quote (("default")))) (y #t) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.1.1 (c (n "keyde") (v "0.1.1") (d (list (d (n "glam") (r "^0.14") (o #t) (d #t) (k 0)))) (h "1y3f0cic3j0j9ba2q95pm0z2wv12w62d1n8p2n2nrfd1pjibn07h") (f (quote (("default")))) (y #t) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.1.4 (c (n "keyde") (v "0.1.4") (d (list (d (n "glam") (r "^0.21") (o #t) (d #t) (k 0)))) (h "0kwgdkhnfz3f9mv5w6x7g8q7xi82b8ck42vw5nw0v1v9h33llfim") (f (quote (("default")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.2.0 (c (n "keyde") (v "0.2.0") (d (list (d (n "glam") (r "^0.21") (o #t) (d #t) (k 0)))) (h "0g9b84rn3cpw671k0gmy950j8jhkkjdfr2fm0pdmdxrrv81k2lhx") (f (quote (("default")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.2.1 (c (n "keyde") (v "0.2.1") (d (list (d (n "glam") (r "^0.21") (o #t) (d #t) (k 0)))) (h "12mrsv5fa1bjvw6f05bvn66dw290m02rlwvxcmxcawz7zhm8b00d") (f (quote (("default")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.2.2 (c (n "keyde") (v "0.2.2") (d (list (d (n "glam") (r "^0.21") (o #t) (d #t) (k 0)))) (h "0flpjv6j2h5fh3j3g7r1rhhlmkg1lj2xd26733hg3aciwxavljgc") (f (quote (("default")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.2.3 (c (n "keyde") (v "0.2.3") (d (list (d (n "glam") (r "^0.21") (o #t) (d #t) (k 0)))) (h "1i0rjhnbqqapq34pszxgsx7wclgsz6m3l3lhp9agbm4fkikw8q7y") (f (quote (("default")))) (s 2) (e (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.2.4 (c (n "keyde") (v "0.2.4") (d (list (d (n "glam") (r "^0.21") (o #t) (d #t) (k 0)))) (h "0hwxcdllx2j8nc5k5lkh8vw56nfp0sisp68sja03g62n8dx9wbj9") (f (quote (("default")))) (s 2) (e (quote (("glam" "dep:glam"))))))

