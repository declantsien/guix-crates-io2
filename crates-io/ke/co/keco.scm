(define-module (crates-io ke co keco) #:use-module (crates-io))

(define-public crate-keco-0.1.0 (c (n "keco") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "17d7cxy6savr9m6lv7pmq21wcqcgbiq9nmazh2bck7mb6n73n7d6")))

(define-public crate-keco-0.1.1 (c (n "keco") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "16qcz2s71vylzqlzyqyk9133zbcsdb80f9khdqqik49rsy7gj7ml")))

