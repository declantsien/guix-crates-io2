(define-module (crates-io ke n- ken-memofs) #:use-module (crates-io))

(define-public crate-ken-memofs-0.3.0 (c (n "ken-memofs") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "fs-err") (r "^2.11.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pn7123fpzbwz4zag8ahm6izcbvnyflc9nyiqwybcdvvvj8aipav") (y #t)))

