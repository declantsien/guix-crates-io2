(define-module (crates-io ke yu keyutils-raw) #:use-module (crates-io))

(define-public crate-keyutils-raw-0.4.0 (c (n "keyutils-raw") (v "0.4.0") (d (list (d (n "errno") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "uninit") (r "^0.3") (d #t) (k 0)))) (h "0arrg043ddrqdwvpf8xmg1ggzlk6i01xkafbgs6zvb4b6rwjwcnv")))

