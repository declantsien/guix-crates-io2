(define-module (crates-io ke yu keyutils) #:use-module (crates-io))

(define-public crate-keyutils-0.1.0 (c (n "keyutils") (v "0.1.0") (d (list (d (n "bitflags") (r "~0.3.2") (d #t) (k 0)) (d (n "errno") (r "~0.1.4") (d #t) (k 0)) (d (n "libc") (r "~0.2.2") (d #t) (k 0)))) (h "18pc2fkpi4v8crldcl5398l3hyz5wkcwswvq0z6s6jc0as1kiw3f")))

(define-public crate-keyutils-0.1.1 (c (n "keyutils") (v "0.1.1") (d (list (d (n "bitflags") (r "~0.3.2") (d #t) (k 0)) (d (n "errno") (r "~0.1.4") (d #t) (k 0)) (d (n "libc") (r "~0.2.2") (d #t) (k 0)))) (h "1a1krjx7jdacgh2r2v9g89vh8iia6b96gr3qfxwsi6szm9f6qsia")))

(define-public crate-keyutils-0.2.0 (c (n "keyutils") (v "0.2.0") (d (list (d (n "bitflags") (r "~0.3.2") (d #t) (k 0)) (d (n "errno") (r "~0.1.4") (d #t) (k 0)) (d (n "libc") (r "~0.2.2") (d #t) (k 0)))) (h "1793q8ry1al0dvrriy4bh0j3dll61jl18gfpm3r518lyd20ginfr")))

(define-public crate-keyutils-0.2.1 (c (n "keyutils") (v "0.2.1") (d (list (d (n "bitflags") (r "~0.3.2") (d #t) (k 0)) (d (n "errno") (r "~0.1.4") (d #t) (k 0)) (d (n "libc") (r "~0.2.2") (d #t) (k 0)))) (h "1k5jribk0bgvv3mdk8hw1m08qb9p55h0yybs8gqv9fzdkf2fgnyb")))

(define-public crate-keyutils-0.4.0 (c (n "keyutils") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "errno") (r "^0.3") (d #t) (k 0)) (d (n "keyutils-raw") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "uninit") (r "^0.3") (d #t) (k 0)))) (h "0drnfw5y1cr8dk5wa5c7jg8aifnxr6xzjmypmjbslmg2j9djfp8w")))

