(define-module (crates-io ke yv keyvaluedb-shared-tests) #:use-module (crates-io))

(define-public crate-keyvaluedb-shared-tests-0.1.0 (c (n "keyvaluedb-shared-tests") (v "0.1.0") (d (list (d (n "keyvaluedb") (r "^0.1.0") (d #t) (k 0)))) (h "0gip1c1fb5kpp8qq9s6lpjmxbsqwlwjrnq6nny9szr1h69lhbgm3")))

(define-public crate-keyvaluedb-shared-tests-0.1.1 (c (n "keyvaluedb-shared-tests") (v "0.1.1") (d (list (d (n "keyvaluedb") (r "^0.1.1") (d #t) (k 0)))) (h "00ylps71j0yg2kbishk6l4xyr6nw7b5cac7165q9bg9hm3hkjg4p")))

