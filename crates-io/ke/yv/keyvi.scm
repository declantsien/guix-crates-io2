(define-module (crates-io ke yv keyvi) #:use-module (crates-io))

(define-public crate-keyvi-0.3.0 (c (n "keyvi") (v "0.3.0") (d (list (d (n "bindgen") (r ">= 0.30.0") (d #t) (k 1)) (d (n "cmake") (r ">= 0.1.29") (d #t) (k 1)) (d (n "rand") (r ">= 0.4.2") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bzjzmvfxyqqzj4j2ca1bflrjrx023zig45z4cgbrb8nxmbswpj3")))

(define-public crate-keyvi-0.4.0 (c (n "keyvi") (v "0.4.0") (d (list (d (n "bindgen") (r ">= 0.30.0") (d #t) (k 1)) (d (n "cmake") (r ">= 0.1.29") (d #t) (k 1)) (d (n "rand") (r ">= 0.4.2") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14y8ps6x3a0hwk9s61k7c5gmckhlafj9hmbigvh9snffx03v3mh9")))

(define-public crate-keyvi-0.4.1 (c (n "keyvi") (v "0.4.1") (d (list (d (n "bindgen") (r ">= 0.30.0") (d #t) (k 1)) (d (n "cmake") (r ">= 0.1.29") (d #t) (k 1)) (d (n "rand") (r ">= 0.4.2") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hnl2icsc629q6dc4m4qcn3fhrb7npqy2j4vrlr795nxsnr6xgr5")))

(define-public crate-keyvi-0.4.3 (c (n "keyvi") (v "0.4.3") (d (list (d (n "bindgen") (r ">=0.30.0") (d #t) (k 1)) (d (n "cmake") (r ">=0.1.29") (d #t) (k 1)) (d (n "rand") (r ">=0.4.2") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c8d6r4y7kj5447rjxjj0dkxn7vxhhy46zj1ywjh75wb1a1kblkf")))

(define-public crate-keyvi-0.4.4 (c (n "keyvi") (v "0.4.4") (d (list (d (n "bindgen") (r ">=0.30.0") (d #t) (k 1)) (d (n "cmake") (r ">=0.1.29") (d #t) (k 1)) (d (n "rand") (r ">=0.4.2") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0") (d #t) (k 0)))) (h "1sbsq92cb0f2dd3v579pym9a8nmd00rh003cr0hgg1225ic301ad")))

(define-public crate-keyvi-0.4.5 (c (n "keyvi") (v "0.4.5") (d (list (d (n "bindgen") (r ">=0.30.0") (d #t) (k 1)) (d (n "cmake") (r ">=0.1.29") (d #t) (k 1)) (d (n "rand") (r ">=0.4.2") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0") (d #t) (k 0)))) (h "09byzfjsqs44sc5m64447nx57920pnpdbwmxdcdgljfg9wfv7qp8")))

(define-public crate-keyvi-0.4.6 (c (n "keyvi") (v "0.4.6") (d (list (d (n "bindgen") (r ">=0.30") (d #t) (k 1)) (d (n "cmake") (r ">=0.1") (d #t) (k 1)) (d (n "rand") (r ">=0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0") (d #t) (k 0)))) (h "1mlxzv2ml10nxqfa3p4qkb9zwspz3m26pn0qqmlgyhrjx4v2xhh8")))

(define-public crate-keyvi-0.4.7 (c (n "keyvi") (v "0.4.7") (d (list (d (n "bindgen") (r ">=0.30") (d #t) (k 1)) (d (n "cmake") (r ">=0.1") (d #t) (k 1)) (d (n "rand") (r ">=0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0") (d #t) (k 0)))) (h "0h59a0g1c4q0rs9q642ni90xcgaf1rlq8ijl1vjlh3cvpx90vmmd")))

(define-public crate-keyvi-0.5.0 (c (n "keyvi") (v "0.5.0") (d (list (d (n "bindgen") (r ">=0.30") (d #t) (k 1)) (d (n "cmake") (r ">=0.1") (d #t) (k 1)) (d (n "rand") (r ">=0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0") (d #t) (k 0)))) (h "11c6krw6fwrw174z9x34y9kzxz861s95qvkjq9zkj7d77gacdwp7")))

(define-public crate-keyvi-0.5.1 (c (n "keyvi") (v "0.5.1") (d (list (d (n "bindgen") (r ">=0.30") (d #t) (k 1)) (d (n "cmake") (r ">=0.1") (d #t) (k 1)) (d (n "rand") (r ">=0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0") (d #t) (k 0)))) (h "1bwjbhwp4l663q6n693gwa33nwy6012liw97dk3khdnmmpjjbach")))

(define-public crate-keyvi-0.5.3 (c (n "keyvi") (v "0.5.3") (d (list (d (n "bindgen") (r ">=0.30") (d #t) (k 1)) (d (n "cmake") (r ">=0.1") (d #t) (k 1)) (d (n "rand") (r ">=0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0") (d #t) (k 0)))) (h "0l3n6inwxc63khafafykbqnrbj7x6gjzf5y7ky1il2nxfy2m36s8")))

(define-public crate-keyvi-0.5.5 (c (n "keyvi") (v "0.5.5") (d (list (d (n "bindgen") (r ">=0.30") (d #t) (k 1)) (d (n "cmake") (r ">=0.1") (d #t) (k 1)) (d (n "rand") (r ">=0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0") (d #t) (k 0)))) (h "1s6gnvz5z2gdvkbbx87hlflikhmibj67l3xsnc9gr6kfinz7iivh")))

(define-public crate-keyvi-0.5.6 (c (n "keyvi") (v "0.5.6") (d (list (d (n "bindgen") (r ">=0.30") (d #t) (k 1)) (d (n "cmake") (r ">=0.1") (d #t) (k 1)) (d (n "rand") (r ">=0.4") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r ">=1.0") (d #t) (k 0)))) (h "1zk8i4gswgaw5llcw2xcagld4m2i87324jv073wwqa9dkqcpfp9j")))

