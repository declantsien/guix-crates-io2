(define-module (crates-io ke yv keyvalues-parser) #:use-module (crates-io))

(define-public crate-keyvalues-parser-0.1.0 (c (n "keyvalues-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "insta") (r "^1.7.1") (f (quote ("ron"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 2)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1rcpd2bmbly80py3wrl34ssks9zifqg2kg44gah6x1bck40h76bx") (f (quote (("unstable"))))))

(define-public crate-keyvalues-parser-0.2.0 (c (n "keyvalues-parser") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "09bdrp92yk1vlcwmap0d7nhz2qikwk5cjnh109iij2c3j5a86k3y") (r "1.61")))

