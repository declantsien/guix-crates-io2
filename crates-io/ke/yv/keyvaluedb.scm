(define-module (crates-io ke yv keyvaluedb) #:use-module (crates-io))

(define-public crate-keyvaluedb-0.1.0 (c (n "keyvaluedb") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)))) (h "1yq2vw1lbq550mz7sxd2bz5fdpk0vb6b6afix30bli3zjfgbqcw8")))

(define-public crate-keyvaluedb-0.1.1 (c (n "keyvaluedb") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)))) (h "097z5pmlhj1kl0yia8q7b09v9a6537v8095r3ypfm0zs2jzamp4v")))

