(define-module (crates-io ke yv keyvalint) #:use-module (crates-io))

(define-public crate-keyvalint-0.1.0 (c (n "keyvalint") (v "0.1.0") (d (list (d (n "rocksdb") (r "^0.21") (o #t) (d #t) (k 0)))) (h "0yq9q8yp34kvm48c8sm34smlpvbl275hl4a3pc4hh77p8hy3s88b") (f (quote (("reference") ("default" "reference")))) (s 2) (e (quote (("rocksdb" "dep:rocksdb"))))))

(define-public crate-keyvalint-0.2.0 (c (n "keyvalint") (v "0.2.0") (d (list (d (n "rocksdb") (r "^0.21") (o #t) (d #t) (k 0)))) (h "063px9bj5d35fjyja6vhgy12pxsr7ldmbbigk01m69hgmd9hr3bl") (f (quote (("reference") ("default" "reference")))) (s 2) (e (quote (("rocksdb" "dep:rocksdb"))))))

(define-public crate-keyvalint-0.3.0 (c (n "keyvalint") (v "0.3.0") (d (list (d (n "rocksdb") (r "^0.21") (o #t) (d #t) (k 0)))) (h "01vwhhnhwdmayi9cnxpl9yrixxv15p1hkripxb2kjniiqad8lbaa") (f (quote (("reference") ("default" "reference")))) (s 2) (e (quote (("rocksdb" "dep:rocksdb"))))))

