(define-module (crates-io ke yv keyvault-agent-azure-auth) #:use-module (crates-io))

(define-public crate-keyvault-agent-azure-auth-0.1.0-alpha (c (n "keyvault-agent-azure-auth") (v "0.1.0-alpha") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("process"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1p0c5xpj2g6j4b4bm3fbk8y6pqs0bcbzgr0va2kdgngb80j7aagg")))

