(define-module (crates-io ke lk kelk-env) #:use-module (crates-io))

(define-public crate-kelk-env-0.2.2 (c (n "kelk-env") (v "0.2.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "kelk-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "minicbor") (r "^0.11") (f (quote ("half" "derive"))) (d #t) (k 0)) (d (n "minicbor-derive") (r "^0.7") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1730p11860hhja13z1m7gzcivzmv8bq9ngbjqcp3hwgbsld38svz") (y #t)))

(define-public crate-kelk-env-0.3.0 (c (n "kelk-env") (v "0.3.0") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("half" "derive" "alloc"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "1hqsm227yr524l4adwywymwb2lxgnnmk10agck26lw9yzrwkm9gy")))

