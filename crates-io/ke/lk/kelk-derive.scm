(define-module (crates-io ke lk kelk-derive) #:use-module (crates-io))

(define-public crate-kelk-derive-0.2.0 (c (n "kelk-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12hdvl11dvlyf03wk0ca9wwzan8xpisin7rh9y62gfwdgcx2b2m6") (y #t)))

(define-public crate-kelk-derive-0.3.0 (c (n "kelk-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10b4dhqgq5g15pm7vvq7nw7qpfzqsw73hlwbba7wy107d1pl06sr")))

