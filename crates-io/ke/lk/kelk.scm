(define-module (crates-io ke lk kelk) #:use-module (crates-io))

(define-public crate-kelk-0.3.0 (c (n "kelk") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (k 0)) (d (n "kelk-allocator") (r "^0.3.0") (d #t) (k 0)) (d (n "kelk-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "kelk-env") (r "^0.3.0") (d #t) (k 0)) (d (n "minicbor") (r "^0.18") (f (quote ("half" "derive" "alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (k 0)) (d (n "wee_alloc") (r "^0.4") (k 0)))) (h "0c4lygbj1hdfnl1fbqij9pnzrmq4f4yhsfy640zjbn9s2smyms7j")))

