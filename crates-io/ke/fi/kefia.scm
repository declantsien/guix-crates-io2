(define-module (crates-io ke fi kefia) #:use-module (crates-io))

(define-public crate-kefia-0.1.0 (c (n "kefia") (v "0.1.0") (d (list (d (n "lazysort") (r "^0.1.1") (d #t) (k 0)) (d (n "qml") (r "^0.0.9") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1fsgza3pz87zyj4hyfx6zjr21m6xnl1magc75zcb8pxpi9164mnr")))

