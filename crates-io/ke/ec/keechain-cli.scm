(define-module (crates-io ke ec keechain-cli) #:use-module (crates-io))

(define-public crate-keechain-cli-0.1.0 (c (n "keechain-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "keechain-core") (r "^0.1") (d #t) (k 0)))) (h "0xrkr4kn5i59vndjklki6pnv1k06jljcndvzxlhsakpqp21whirw") (f (quote (("nostr" "keechain-core/nostr") ("default")))) (r "1.65.0")))

