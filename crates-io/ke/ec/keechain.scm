(define-module (crates-io ke ec keechain) #:use-module (crates-io))

(define-public crate-keechain-0.0.0 (c (n "keechain") (v "0.0.0") (h "07gnmbxyc1qm0mkjb32qk2sb3yasx5qfbvxh0ggzhrfjrpw9rib4") (y #t)))

(define-public crate-keechain-0.1.0 (c (n "keechain") (v "0.1.0") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui_extras") (r "^0.20.0") (f (quote ("image"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("png"))) (k 0)) (d (n "keechain-core") (r "^0.1") (d #t) (k 0)) (d (n "rfd") (r "^0.10.0") (d #t) (k 0)))) (h "02m9nqzdbn8050zhflk9xgm8p800rv5f4gixgd9rwpqp5qcx4glm") (f (quote (("nostr" "keechain-core/nostr") ("default")))) (r "1.65.0")))

