(define-module (crates-io ke ec keechain-core) #:use-module (crates-io))

(define-public crate-keechain-core-0.0.0 (c (n "keechain-core") (v "0.0.0") (h "13d0glbq076658q9ri4asgnrm599770msjw9pn140gjl4jn8j3xh") (r "1.64")))

(define-public crate-keechain-core-0.1.0 (c (n "keechain-core") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bdk") (r "^0.26") (f (quote ("keys-bip39"))) (k 0)) (d (n "bitcoin") (r "^0.29") (d #t) (k 0)) (d (n "cbc") (r "^0.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "rand_hc") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (k 0)))) (h "13m3dbn5kd7k6m3gbv80ncw6xsippsvfid6divh4ii0m7v7wv39x") (f (quote (("nostr") ("default")))) (r "1.65.0")))

