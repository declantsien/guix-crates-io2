(define-module (crates-io ke y_ key_parse) #:use-module (crates-io))

(define-public crate-key_parse-0.1.0 (c (n "key_parse") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "miette") (r "^5.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ayv83q1a3ag465ijpn5nnd185qfl1ris1f59gndpx0vjj1dd9n2")))

(define-public crate-key_parse-0.1.1 (c (n "key_parse") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1hb4vq5nwx0xy641x0j7jpr2wg3zly6x90fcfcylhhc1q3a3yphp") (r "1.77")))

(define-public crate-key_parse-0.1.2 (c (n "key_parse") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "miette") (r "^5") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1d8aj20dqcdckny4lil0h003nikb8ymwwcvpw030l0506pzk89z5")))

(define-public crate-key_parse-0.1.4 (c (n "key_parse") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "miette") (r "^5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0h2kasf4jdipb2q17p35baxsxkf94p6jn8f31rz2h6ccaaasmi17")))

(define-public crate-key_parse-0.1.5 (c (n "key_parse") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.27") (f (quote ("events"))) (k 0)) (d (n "miette") (r "^6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0mh5pr98xd31wbwz6gn5bjb9ds34r10rp2h5ghgcc9fk591ddnr0")))

(define-public crate-key_parse-0.1.6 (c (n "key_parse") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.27") (f (quote ("events"))) (k 0)) (d (n "miette") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "106aw3vfg2csz7l53ki51f2h0k6v3s4jk1vfnxals8fgxwfx58mn")))

(define-public crate-key_parse-0.1.7 (c (n "key_parse") (v "0.1.7") (d (list (d (n "crossterm") (r "^0.27") (f (quote ("events"))) (k 0)) (d (n "miette") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0igm2g8z91wac8di2zvw0ryymldmdsp61dvcznxrkqr7q7fw48iq")))

(define-public crate-key_parse-0.1.8 (c (n "key_parse") (v "0.1.8") (d (list (d (n "crossterm") (r "^0.27") (f (quote ("events"))) (k 0)) (d (n "miette") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1w0a95qr1lbc91j3wzlrxs2sb3w3yq5430r24w3j0d1a7n296czp") (r "1.77")))

(define-public crate-key_parse-0.1.9 (c (n "key_parse") (v "0.1.9") (d (list (d (n "crossterm") (r "^0.27") (f (quote ("events"))) (k 0)) (d (n "miette") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0cx3qsww3f9i0k8gqv9kkx72r627drabxirm0b42swfdi4mjc4d9") (r "1.77")))

(define-public crate-key_parse-0.2.0 (c (n "key_parse") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27") (f (quote ("events"))) (k 0)) (d (n "miette") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "151k3ksaf1jfaiz8fj7fpp3rhj6b4d8m9lfcvvjqygj9r52xxqkn") (r "1.77")))

