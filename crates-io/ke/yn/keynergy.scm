(define-module (crates-io ke yn keynergy) #:use-module (crates-io))

(define-public crate-keynergy-0.2.4 (c (n "keynergy") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ketos") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "ketos_derive") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1qz0xdkwzp6alb6rz8ijssanw00haw56qsq3lw6kv243hlki3zg8") (f (quote (("analysis" "ketos" "ketos_derive"))))))

(define-public crate-keynergy-0.2.5 (c (n "keynergy") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ketos") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "ketos_derive") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0gyjqai5s50xwzvd0b4adqa4pc8c7bx26lm5nnw6zd94dzq40157") (f (quote (("analysis" "ketos" "ketos_derive"))))))

(define-public crate-keynergy-0.2.6 (c (n "keynergy") (v "0.2.6") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ketos") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "ketos_derive") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02kbhl3qyw3lmyx793zpvpdq20mm85l7lz8cqqkfk5w081k9b3fz") (f (quote (("analysis" "ketos" "ketos_derive"))))))

(define-public crate-keynergy-0.2.7 (c (n "keynergy") (v "0.2.7") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "ketos") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "ketos_derive") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1vp5c4hf7634zq1h8w6bhd4digiqvl208hfkg2a4anp8nb1cfzby") (f (quote (("analysis" "ketos" "ketos_derive"))))))

