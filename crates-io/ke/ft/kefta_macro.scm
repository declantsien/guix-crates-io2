(define-module (crates-io ke ft kefta_macro) #:use-module (crates-io))

(define-public crate-kefta_macro-0.0.1 (c (n "kefta_macro") (v "0.0.1") (d (list (d (n "kefta_core") (r "^0.0.1") (f (quote ("syn"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0iwf3swjfkyzxyap8dfgydll9cpf141i7846m3lxcjwgnljcy0kl")))

