(define-module (crates-io ke ft kefta_core) #:use-module (crates-io))

(define-public crate-kefta_core-0.0.1 (c (n "kefta_core") (v "0.0.1") (d (list (d (n "litrs") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "02z2m3q21f4jqsnksw1rl5qh9fvy4km7dpwf5mrs2h1j6wc01525") (f (quote (("util") ("literal" "litrs") ("default" "literal")))) (s 2) (e (quote (("syn" "dep:syn"))))))

