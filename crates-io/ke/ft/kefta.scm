(define-module (crates-io ke ft kefta) #:use-module (crates-io))

(define-public crate-kefta-0.0.1 (c (n "kefta") (v "0.0.1") (d (list (d (n "kefta_core") (r "^0.0.1") (d #t) (k 0)) (d (n "kefta_macro") (r "^0.0.1") (d #t) (k 0)))) (h "05ywd97hxn7nz5lmzqin51bg8xgssc9j994dikz6lwi1ybnalpq5") (f (quote (("util" "kefta_core/syn") ("syn" "kefta_core/util") ("literal" "kefta_core/literal") ("default" "literal"))))))

