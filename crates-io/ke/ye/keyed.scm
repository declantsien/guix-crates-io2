(define-module (crates-io ke ye keyed) #:use-module (crates-io))

(define-public crate-keyed-0.1.0 (c (n "keyed") (v "0.1.0") (h "095v7j4far3gykf3nx6p5jg3k4q9nsydm5qarvh0qzrcj9mhb5z8") (y #t)))

(define-public crate-keyed-0.1.1 (c (n "keyed") (v "0.1.1") (h "176mdcwmxpwnv76bwd6czajzca30p3fyw1rjrpk0rwnh2lb1nbm0") (y #t)))

(define-public crate-keyed-0.1.2 (c (n "keyed") (v "0.1.2") (h "1lbqmpdymr8zpxhgdrbh0f4sxkm5cq3hzwiv68m4iizmnwbmz2m8") (y #t)))

(define-public crate-keyed-0.1.3 (c (n "keyed") (v "0.1.3") (h "0k85s81qnv4i38y6f1pfpy2r0mdxa9k0hplsaa6955nj8cwdg4af")))

