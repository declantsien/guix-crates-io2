(define-module (crates-io ke ye keyezinput) #:use-module (crates-io))

(define-public crate-keyezinput-0.1.2 (c (n "keyezinput") (v "0.1.2") (h "1fwfy0izgydkwy40l4689qzyzm2f7f4761mnlvynqfn1139w8b05")))

(define-public crate-keyezinput-0.1.21 (c (n "keyezinput") (v "0.1.21") (h "0zqdc9fgfn6mfnal4hczzf2zajmzmqscjhrsmw6rcp9vqadxmfk4") (y #t)))

(define-public crate-keyezinput-0.1.3 (c (n "keyezinput") (v "0.1.3") (h "13w4vfjisvj2nyxbhvb43gxymyzsflhxsjf0c8snbrbwbq528y4a")))

