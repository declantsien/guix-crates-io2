(define-module (crates-io ke ye keyed-set) #:use-module (crates-io))

(define-public crate-keyed-set-0.1.0 (c (n "keyed-set") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.12") (f (quote ("raw"))) (d #t) (k 0)))) (h "13m8pcl2zzvmvbdc6j41mhdszvvx4l4if1lm94abzzbrc3ybzqfs")))

(define-public crate-keyed-set-0.2.0 (c (n "keyed-set") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.12") (f (quote ("raw"))) (d #t) (k 0)))) (h "1r0nkx1rlclxzgk6r79zmni9c3igznlr912nm9bxksbzn4zmxybh")))

(define-public crate-keyed-set-0.3.0 (c (n "keyed-set") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.12") (f (quote ("raw"))) (d #t) (k 0)))) (h "0v6a3hk7vmyj4mhfwlx8fpk2dn2k4sn3cdalsykl72pfw274wn7z")))

(define-public crate-keyed-set-0.4.0 (c (n "keyed-set") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.12") (f (quote ("raw"))) (d #t) (k 0)))) (h "0jy2b9i52fm5kkqy7x8bmprxpnav0gj3wvrd984yrkarqxl6xp5r")))

(define-public crate-keyed-set-0.4.1 (c (n "keyed-set") (v "0.4.1") (d (list (d (n "hashbrown") (r "^0.12") (f (quote ("raw"))) (d #t) (k 0)))) (h "0s5q3fa0pdnyvmv1z0q9x9w5lk3hiypq1vl8xsx2vkinn4j9wg45")))

(define-public crate-keyed-set-0.4.2 (c (n "keyed-set") (v "0.4.2") (d (list (d (n "hashbrown") (r "^0.13.1") (f (quote ("raw"))) (d #t) (k 0)))) (h "0nss74l6knyklgh5ijpfvw3iha75jvikm4qckbz01vvg5k5glc1j")))

(define-public crate-keyed-set-0.4.3 (c (n "keyed-set") (v "0.4.3") (d (list (d (n "hashbrown") (r "^0.13.1") (f (quote ("raw"))) (d #t) (k 0)))) (h "0pbdn3vs8g0035rm4yv3n6kimw5agch30gps4zyyh573gfycmg3n")))

(define-public crate-keyed-set-0.4.4 (c (n "keyed-set") (v "0.4.4") (d (list (d (n "hashbrown") (r "^0.13.1") (f (quote ("raw"))) (d #t) (k 0)))) (h "076pk79z8kyl2mzsmsr6nnxxh9mp8d85yx2lh09hc43cmfcyfjsy")))

(define-public crate-keyed-set-0.4.5 (c (n "keyed-set") (v "0.4.5") (d (list (d (n "hashbrown") (r "^0.13.1") (f (quote ("raw"))) (d #t) (k 0)))) (h "0nk1cs1l7k1r9ikk4jjc9mnhq9q9jwxcz254kj083470hc1137mp")))

(define-public crate-keyed-set-1.0.0 (c (n "keyed-set") (v "1.0.0") (d (list (d (n "hashbrown") (r "^0.14.3") (f (quote ("raw"))) (d #t) (k 0)))) (h "0d5f0hwxxjavsr73p0s65sdzg21025m90qqdajhm6yf15nfw6gha")))

