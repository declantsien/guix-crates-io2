(define-module (crates-io ke ye keyed_priority_queue) #:use-module (crates-io))

(define-public crate-keyed_priority_queue-0.1.0 (c (n "keyed_priority_queue") (v "0.1.0") (h "1k4bfpr7rqfl2crqygcd92fhyx54mij3c8f0fhdn7pig3wqjg078") (y #t)))

(define-public crate-keyed_priority_queue-0.1.1 (c (n "keyed_priority_queue") (v "0.1.1") (h "1cym5y792sa90jb8gl2prdhay8bk8y4bxbgsj4hfrkbkxpsxg603") (y #t)))

(define-public crate-keyed_priority_queue-0.1.2 (c (n "keyed_priority_queue") (v "0.1.2") (h "03wmypkryhl9lxqxbs18sihgn0b0x336hj501p3if8dj0zgcqi1b") (y #t)))

(define-public crate-keyed_priority_queue-0.1.3 (c (n "keyed_priority_queue") (v "0.1.3") (h "0ai2k07mnrymqjxyrzaqm81jg56wgilvjypw72qxi7c4c5by35za") (y #t)))

(define-public crate-keyed_priority_queue-0.2.0 (c (n "keyed_priority_queue") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)))) (h "1l79zjb3i4bb140drypwxfbii2xa1hnfvs3s85cvc46cbdc1chmz") (y #t)))

(define-public crate-keyed_priority_queue-0.2.1 (c (n "keyed_priority_queue") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)))) (h "078jfnib46kv3kznwjrdkavpd8j77bqzlrgyjclpzksjisfxdvjb") (y #t)))

(define-public crate-keyed_priority_queue-0.3.0 (c (n "keyed_priority_queue") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)))) (h "0waigs31xl02xbp005hwa3hb9knfhig54sxjg532dfi2qp8kp68k") (y #t)))

(define-public crate-keyed_priority_queue-0.3.1 (c (n "keyed_priority_queue") (v "0.3.1") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)))) (h "05c4mzvlc28vpqdxbhq6q6ym2j3ds8n1kwjpl1s5jwpv6b4visbr") (y #t)))

(define-public crate-keyed_priority_queue-0.3.2 (c (n "keyed_priority_queue") (v "0.3.2") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)))) (h "1m78lgw7rhq7z9w941kr6ms05a1x980799m52m9nagb8ikq9v9d7")))

(define-public crate-keyed_priority_queue-0.4.0 (c (n "keyed_priority_queue") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)))) (h "06l6m31hhnzl91vbmwdgjp847bb3742bgs4i8dn3rdf4xj5m53s6") (r "1.56")))

(define-public crate-keyed_priority_queue-0.4.1 (c (n "keyed_priority_queue") (v "0.4.1") (d (list (d (n "indexmap") (r "^1.3") (d #t) (k 0)))) (h "1s06k84rav4pwcmnl6qhqn9z6sdnxkfz7k4xagy83z36gd0bcqrd") (r "1.56")))

(define-public crate-keyed_priority_queue-0.4.2 (c (n "keyed_priority_queue") (v "0.4.2") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)))) (h "03b452v0b56xa8msf1hvn087qcmayipz4wq1kmgswi1fmcyqkrsf") (r "1.63")))

