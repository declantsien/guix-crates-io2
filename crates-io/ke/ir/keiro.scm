(define-module (crates-io ke ir keiro) #:use-module (crates-io))

(define-public crate-keiro-0.0.1 (c (n "keiro") (v "0.0.1") (d (list (d (n "futures-util") (r "^0.3.13") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "route-recognizer") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "13ncnwm4yv50c55plzn9wbf59sghj3fcrrdyh20qb04wxqhxn9a6")))

(define-public crate-keiro-0.0.2 (c (n "keiro") (v "0.0.2") (d (list (d (n "futures-util") (r "^0.3.13") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "route-recognizer") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower") (r "^0.4.8") (f (quote ("full"))) (d #t) (k 2)) (d (n "tower-http") (r "^0.1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "1iqrazhr0sxr9an4j4wpw7lsjlvmrr1wbs9ilc1pgy7xc1pl6zlk")))

