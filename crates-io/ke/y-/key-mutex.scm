(define-module (crates-io ke y- key-mutex) #:use-module (crates-io))

(define-public crate-key-mutex-0.1.0 (c (n "key-mutex") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "guardian") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "13qplghk5hpkcq2c1l89vdn552q07na2jxyg46xzq4mypfdh3ks6") (f (quote (("default" "std")))) (s 2) (e (quote (("tokio" "dep:tokio") ("std" "dep:guardian"))))))

(define-public crate-key-mutex-0.1.1 (c (n "key-mutex") (v "0.1.1") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "guardian") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "17qml9a494r1m86hlg7dh04r22ap4gq9vp8x3crz3pgp9fl8rdgk") (f (quote (("default" "std")))) (s 2) (e (quote (("tokio" "dep:tokio") ("std" "dep:guardian"))))))

(define-public crate-key-mutex-0.1.2 (c (n "key-mutex") (v "0.1.2") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "guardian") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "1a1fs8cxqh6059w7c72pdlxb29vm3fv07wlzakamqddn3kzgplgs") (f (quote (("default" "std")))) (s 2) (e (quote (("tokio" "dep:tokio") ("std" "dep:guardian"))))))

(define-public crate-key-mutex-0.1.3 (c (n "key-mutex") (v "0.1.3") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "guardian") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "0ysm30l12q1vplh3kh3fp3fv3y0d75yi96926dlyxh85zxmg5vxn") (f (quote (("default" "std")))) (s 2) (e (quote (("tokio" "dep:tokio") ("std" "dep:guardian"))))))

