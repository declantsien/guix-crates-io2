(define-module (crates-io ke y- key-vec) #:use-module (crates-io))

(define-public crate-key-vec-0.2.0 (c (n "key-vec") (v "0.2.0") (h "1mjdad8mlrhzfsnbwr2mdsgxkgfi4crnwizfi0hxb31ca8acwzfc")))

(define-public crate-key-vec-0.2.1 (c (n "key-vec") (v "0.2.1") (h "03bxyb14y5zdqn3f524vnn78hz0q7586sq8qjb5ima1ll54djcbj")))

(define-public crate-key-vec-0.2.2 (c (n "key-vec") (v "0.2.2") (d (list (d (n "serde") (r "1.*") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0ialli2qz5w0sssn6hs7awcgkjp89sbwjp8ymw48bzkqdlxh3dmz")))

(define-public crate-key-vec-0.2.3 (c (n "key-vec") (v "0.2.3") (d (list (d (n "serde") (r "1.*") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0i8vv2zc1lb0xx8hppc8c717lbby4mldmnmw67jkswaibgm9a99d")))

(define-public crate-key-vec-0.2.4 (c (n "key-vec") (v "0.2.4") (d (list (d (n "serde") (r "1.*") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "07gl8s267v7vnc5f02vm87ha3gj6qfcx5rflqdj9ydkpwgyjxqs6")))

(define-public crate-key-vec-0.2.5 (c (n "key-vec") (v "0.2.5") (d (list (d (n "serde") (r "1.*") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "13na85li1aam8b2zp95f7z42ddnk8l3qcf2yczi4583p7hcwnbff")))

(define-public crate-key-vec-0.2.6 (c (n "key-vec") (v "0.2.6") (d (list (d (n "serde") (r "1.*") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1d1b5ya6g6biixdnqnq9pml7a4kz1xwidphvcbg06phi8bik0q0f")))

(define-public crate-key-vec-0.3.0 (c (n "key-vec") (v "0.3.0") (d (list (d (n "serde") (r "1.*") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1kigbnycgwridi8qbb54c092zj34nn2cjs3a28z3gnp66a653cs5")))

(define-public crate-key-vec-0.3.1 (c (n "key-vec") (v "0.3.1") (d (list (d (n "serde") (r "1.*") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0wb0f3i5i875ifmikan7x54h3hsckh4pis0qr6waavqbhwih8414")))

(define-public crate-key-vec-0.4.0 (c (n "key-vec") (v "0.4.0") (d (list (d (n "serde") (r "1.*") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1s3jhb1wnl6dkl0xjpwp9jn962hpyg7pp37shyr06n0s75pzd34x")))

(define-public crate-key-vec-0.4.1 (c (n "key-vec") (v "0.4.1") (d (list (d (n "serde") (r "1.*") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "017i29y3kbx9ch6nm8d16d8srnf0wn13scpvn3l1wx59qb5qm6df")))

