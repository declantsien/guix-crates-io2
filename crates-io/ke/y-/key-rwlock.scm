(define-module (crates-io ke y- key-rwlock) #:use-module (crates-io))

(define-public crate-key-rwlock-0.1.0 (c (n "key-rwlock") (v "0.1.0") (d (list (d (n "tokio") (r "^1.27.0") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "15q0iwcxn67skg2nskw5zd9dnss7hlshgd76y5a45n5dqh278jwm") (r "1.63.0")))

(define-public crate-key-rwlock-0.1.1 (c (n "key-rwlock") (v "0.1.1") (d (list (d (n "tokio") (r "^1.27.0") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "1c1ab4y5522x36143xphk4fjrwkjp3wbb80diyr46vqwxy30hwdf") (r "1.63.0")))

(define-public crate-key-rwlock-0.1.2 (c (n "key-rwlock") (v "0.1.2") (d (list (d (n "tokio") (r "^1.27.0") (f (quote ("sync"))) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros"))) (k 2)))) (h "0bbwbcc6v6ldamxmzq0cbg9yag7v93wda6x3x2gm97ryvwdn0myf") (r "1.63.0")))

