(define-module (crates-io ke y- key-lang) #:use-module (crates-io))

(define-public crate-key-lang-0.1.0 (c (n "key-lang") (v "0.1.0") (h "0s4r7dnjmf9l5hk00ilk7x6sqrqbizjfrf42q89d73z0sjak8n9b")))

(define-public crate-key-lang-0.1.1 (c (n "key-lang") (v "0.1.1") (h "1i1h5a76i4hg64h9bvcjxw8kfrs76dyzrqnyig15j69hvvbi4nla")))

