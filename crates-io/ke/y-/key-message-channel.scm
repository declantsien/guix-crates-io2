(define-module (crates-io ke y- key-message-channel) #:use-module (crates-io))

(define-public crate-key-message-channel-0.1.0 (c (n "key-message-channel") (v "0.1.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "riffy") (r "^0.1.0") (d #t) (k 0)))) (h "1m5aksbcjxmvjppahmblyxvdm2cpp4931wlrlwsf7xrdm52ldn10")))

