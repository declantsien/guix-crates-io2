(define-module (crates-io ke y- key-utils) #:use-module (crates-io))

(define-public crate-key-utils-1.0.0 (c (n "key-utils") (v "1.0.0") (d (list (d (n "bs58") (r "^0.4.0") (f (quote ("check"))) (d #t) (k 0)) (d (n "secp256k1") (r "^0.28.2") (f (quote ("alloc" "rand" "rand-std"))) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive" "alloc"))) (k 0)) (d (n "toml") (r "^0.5.6") (k 2)))) (h "13h5kg3wqidlb3g490zyg2f6szrbj8lki87q4jds9ivhjbjysdhk")))

(define-public crate-key-utils-1.1.0 (c (n "key-utils") (v "1.1.0") (d (list (d (n "bs58") (r "^0.4.0") (f (quote ("check"))) (d #t) (k 0)) (d (n "secp256k1") (r "^0.28.2") (f (quote ("alloc" "rand" "rand-std"))) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive" "alloc"))) (k 0)) (d (n "toml") (r "^0.5.6") (k 2)))) (h "0p53sksfy83wsawwsnxk4ivqmgjca2wzv5dzlqi9fm2ffxhp3s01")))

