(define-module (crates-io ke y- key-lock) #:use-module (crates-io))

(define-public crate-key-lock-0.1.0 (c (n "key-lock") (v "0.1.0") (d (list (d (n "ntest") (r "^0.9.0") (d #t) (k 2)) (d (n "tokio") (r "^1.23.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0wqq0pdy6nglkgqck1wfnjz1582nw8kq2b6s9iq3ya1gs3gd4bvr")))

