(define-module (crates-io ke y- key-path) #:use-module (crates-io))

(define-public crate-key-path-0.1.0 (c (n "key-path") (v "0.1.0") (h "188644p48f01dscpb4568y250j0lm28a63vnbsdrrlpr9jqf6a8c")))

(define-public crate-key-path-0.1.1 (c (n "key-path") (v "0.1.1") (h "0mj3xc6bfhmj800xx4hlxkiykxd3cgar9r0rsnyiv4mgs0sdhkvf")))

(define-public crate-key-path-0.1.2 (c (n "key-path") (v "0.1.2") (h "0h1bsvzppqkczldhi8krzc3gppk67f83ssj3cw2qapx1w7z28k6a")))

(define-public crate-key-path-0.1.3 (c (n "key-path") (v "0.1.3") (h "0kjzzzvkbf76bz9x06kzq6bbjb45y9xhb2zam901akmlff5hapgl")))

(define-public crate-key-path-0.1.4 (c (n "key-path") (v "0.1.4") (h "0nkm1b9kb2cgbvm5q88p5h7jn7449lnh79gv9r7bs9vpny5dydiv")))

(define-public crate-key-path-0.1.5 (c (n "key-path") (v "0.1.5") (h "1ak0b98rjqafkjbvk1alb0jlrlk28vx4zqwd6zh4yqbp2czshiyb")))

(define-public crate-key-path-0.1.6 (c (n "key-path") (v "0.1.6") (h "0r4af3jg9w6rvilcd6gfl32mbj05bfp9jy9520b38xshpkm4pjrf")))

(define-public crate-key-path-0.1.7 (c (n "key-path") (v "0.1.7") (h "0kp1hd3vq1jm517anym2p9q4dvda9wvz3yljql43k973pzgshlkp")))

(define-public crate-key-path-0.1.8 (c (n "key-path") (v "0.1.8") (h "0zw1gyvbv1ab862j156rn6sqcxfm3197hqknim6nxkr080y5r6qm")))

(define-public crate-key-path-0.1.9 (c (n "key-path") (v "0.1.9") (h "0m46mj72bbwlsklp2h7514n50wk447w133gkcg2kmvzhgl6dl66s")))

(define-public crate-key-path-0.1.10 (c (n "key-path") (v "0.1.10") (h "185zim7qqdh1p0swk3mrb0ivxdy0kswis0shq7pljkaj3z6dkrkf")))

(define-public crate-key-path-0.1.11 (c (n "key-path") (v "0.1.11") (h "1f0bzxxslmc4rbngvljgcici9q93x3jdn3qlm93my7b3817vvvim")))

(define-public crate-key-path-0.2.0 (c (n "key-path") (v "0.2.0") (h "0kp8j6m1l94aaahfh8w3ardgiqzxayarb0qxyfb121mpksya6faw")))

(define-public crate-key-path-0.2.1 (c (n "key-path") (v "0.2.1") (h "0xz0lzlh36f5qd3ka3lcfwxnk4rvbwl8xvfkf5s14qm8fdh330km")))

(define-public crate-key-path-0.2.2 (c (n "key-path") (v "0.2.2") (h "1m2imw8biwiac04kfiwjkxjlx3jjcn11iqwj4swbi1dh2alddgrd")))

