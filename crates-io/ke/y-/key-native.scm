(define-module (crates-io ke y- key-native) #:use-module (crates-io))

(define-public crate-key-native-0.1.0 (c (n "key-native") (v "0.1.0") (h "0dqc4m8mpcxhcf010l6fk6b8rxgywr1c635m3dhbng2hzs5al5ix")))

(define-public crate-key-native-0.1.1 (c (n "key-native") (v "0.1.1") (h "0zkfflaihyc7fc46qhi591klp0rrhlzmdjclbin3ma8pz38j4wy3")))

(define-public crate-key-native-0.1.5 (c (n "key-native") (v "0.1.5") (h "0rk7ib53b3m5qj8bfm1h7gay7cxzn7c8nrnln2g7h6jh690j0crh")))

(define-public crate-key-native-0.1.6 (c (n "key-native") (v "0.1.6") (h "18h3ms4m1ri1l6wm0x80ggq2ykrv4k0fmfgj4wy7nwf85rq1varf")))

(define-public crate-key-native-0.1.61 (c (n "key-native") (v "0.1.61") (h "1dj2661946xqjqncy4r6a15k6lh7gmcigmwri3as594q75cks2a5")))

