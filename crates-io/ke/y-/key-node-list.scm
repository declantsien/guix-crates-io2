(define-module (crates-io ke y- key-node-list) #:use-module (crates-io))

(define-public crate-key-node-list-0.0.1 (c (n "key-node-list") (v "0.0.1") (h "1mzmdcv47ywdqi5gb3i43z6ka96lwgn7xdk9c28dr1i1s17595c4")))

(define-public crate-key-node-list-0.0.2 (c (n "key-node-list") (v "0.0.2") (h "1yhyz4bzm75ss02p201h7v6zxgmwr95p2nhzribwmjayj0gq411v")))

(define-public crate-key-node-list-0.0.3 (c (n "key-node-list") (v "0.0.3") (h "1nfiz3npjkvw31ja9rmkjrp6yzw9plqda4117ycaazk18i9n5bkb")))

(define-public crate-key-node-list-0.0.4 (c (n "key-node-list") (v "0.0.4") (h "11byl2aw670zav1s4fyiigrlq546pxj2134dw4f3ra2inbs4nq29")))

(define-public crate-key-node-list-0.0.5 (c (n "key-node-list") (v "0.0.5") (h "15b2ljkk54cwi5462v2m6j72snxyafaipa8wpfnk5bln4m9fwgv5")))

