(define-module (crates-io ke lp kelpie-client) #:use-module (crates-io))

(define-public crate-kelpie-client-0.1.0 (c (n "kelpie-client") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.7") (f (quote ("secure"))) (d #t) (k 0)) (d (n "kelpie-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "kelpie-server") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "raft") (r "^0.4.3") (d #t) (k 0)))) (h "0hgffl95nsqhf3m7hmyv962psbln9vpajccflx6kf2h458hvf75p")))

