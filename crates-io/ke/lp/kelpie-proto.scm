(define-module (crates-io ke lp kelpie-proto) #:use-module (crates-io))

(define-public crate-kelpie-proto-0.1.0 (c (n "kelpie-proto") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.7") (f (quote ("secure"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "raft") (r "^0.4.3") (d #t) (k 0)))) (h "0l7lc9zixgrgcc7fx59n2yq6z0pyii4a67q51nhmpra3f5zh6nsv")))

