(define-module (crates-io ke lp kelpie-server) #:use-module (crates-io))

(define-public crate-kelpie-server-0.1.0 (c (n "kelpie-server") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.7") (f (quote ("secure"))) (d #t) (k 0)) (d (n "kelpie-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "raft") (r "^0.4.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "07lnigyryl6jj4rpf9fg7wqpm4z9ky8mg2gmg8lr1wrqkzsc0lzb")))

