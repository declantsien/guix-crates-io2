(define-module (crates-io ke lp kelpie) #:use-module (crates-io))

(define-public crate-kelpie-0.1.0 (c (n "kelpie") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.7") (f (quote ("secure"))) (d #t) (k 0)) (d (n "kelpie-client") (r "^0.1.0") (d #t) (k 0)) (d (n "kelpie-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "kelpie-server") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "raft") (r "^0.4.3") (d #t) (k 0)))) (h "1b3dahkrynx7yywy12wvax4n530cwgspbk5z26c5pyc4ndyhbcd3")))

