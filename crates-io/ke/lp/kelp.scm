(define-module (crates-io ke lp kelp) #:use-module (crates-io))

(define-public crate-kelp-0.1.0 (c (n "kelp") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0q9l2gzm2lfp08rl8q68106z19nasy1mdg9c6kqa3vrkg9icc7qs")))

(define-public crate-kelp-0.2.0 (c (n "kelp") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0f6sa25i6pr87y2yf0srjd8j4zi1pf0gmipsc61vpnws37baz6zn")))

(define-public crate-kelp-0.3.0 (c (n "kelp") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1gw7wl3wfimfziw46f7iv0g3rk49dcjf8qxsdywj68vc723hha5y")))

(define-public crate-kelp-0.4.0 (c (n "kelp") (v "0.4.0") (d (list (d (n "clap") (r "^3") (d #t) (k 0)))) (h "12y7wx62l7bc9qghba4n3hjcvnws1jfjglpi2xa4b04c8yi1l6qi")))

(define-public crate-kelp-0.5.0 (c (n "kelp") (v "0.5.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dwm7cccax1qq4grdby21ldm5d0sydgda9bmfawrwxlc4kcnxr32")))

