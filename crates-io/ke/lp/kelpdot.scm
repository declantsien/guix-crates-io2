(define-module (crates-io ke lp kelpdot) #:use-module (crates-io))

(define-public crate-kelpdot-1.0.0 (c (n "kelpdot") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 0)) (d (n "runas") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "02k08s5am4cvamx98fq9f4pm2ddm8dx7ns4j9k2bll21c3vi0cl8")))

(define-public crate-kelpdot-1.0.1 (c (n "kelpdot") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 0)) (d (n "runas") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0m90ss415p345cw7mdjfnrsn3cv8kq28rplvrk8bak5s6i4dxm34")))

(define-public crate-kelpdot-2.0.0 (c (n "kelpdot") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 0)) (d (n "kelpdot_macros") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.5") (d #t) (k 1)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "11lgi5wncgxvlj7k4l7x9c6344lnn1skcq5hz71739mz120kaa88")))

(define-public crate-kelpdot-2.0.1 (c (n "kelpdot") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 0)) (d (n "kelpdot_macros") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1j83csiq5i9zw4ymid8y7n4zzga8mryzk6289fsg3f3n2dzflxrl")))

(define-public crate-kelpdot-2.0.2 (c (n "kelpdot") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "kelpdot_macros") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1kf3vs9vhmb2xlzd71j3brm82s4zpxaqygb2kvqk4mzvcwlxr49j")))

(define-public crate-kelpdot-2.0.3 (c (n "kelpdot") (v "2.0.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "kelpdot_macros") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0mpydwa19gl22qww5cs12k1h9g4wb85r7dnkqdd3g0xzxd01375y")))

(define-public crate-kelpdot-2.1.3 (c (n "kelpdot") (v "2.1.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "kelpdot_macros") (r "^2.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "045zqqcc2gs81g3f4apzxs7kwi39gmf392lcz03f75fkyfp77bx0")))

(define-public crate-kelpdot-2.1.4 (c (n "kelpdot") (v "2.1.4") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "kelpdot_macros") (r "^2.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1mmv8wfmscbmj0kkj9mxd4nszr616zrn8xvy2pjdkrg9bvg3xkl1")))

(define-public crate-kelpdot-2.1.5 (c (n "kelpdot") (v "2.1.5") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "kelpdot_macros") (r "^2.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "17khnsah58agsph7zx1fnss3vpzri31scs2mp9h16nrq96c6g8lz")))

(define-public crate-kelpdot-2.1.6 (c (n "kelpdot") (v "2.1.6") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "kelpdot_macros") (r "^2.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1iza43gnmvsggw9nrb9ipfzzhjfxsi9j608y10wmh0p7hcvw6y52")))

(define-public crate-kelpdot-2.1.8 (c (n "kelpdot") (v "2.1.8") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "kelpdot_macros") (r "^2.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1k28bk1h96iscc0ppp2lvjfw3h9ysgjbplf942wdkxbyva9acgvz")))

(define-public crate-kelpdot-2.1.9 (c (n "kelpdot") (v "2.1.9") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "kelpdot_macros") (r "^2.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "1ajwlz3fi2rklkll5qqrhhhjhk58n24s6kxyxd5y8gw4fvkjvxmx")))

