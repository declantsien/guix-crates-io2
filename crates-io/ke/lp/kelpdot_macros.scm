(define-module (crates-io ke lp kelpdot_macros) #:use-module (crates-io))

(define-public crate-kelpdot_macros-2.0.0 (c (n "kelpdot_macros") (v "2.0.0") (h "1c1fg1wv08ij2bximly4qmr5mnhcrxwxnvxpi4jx822sb8f2zrll")))

(define-public crate-kelpdot_macros-2.0.1 (c (n "kelpdot_macros") (v "2.0.1") (h "0f98qzzibsw8pis1f6l0yyqhf9k9dlg80znm1g43j938k76lcdgb")))

(define-public crate-kelpdot_macros-2.0.2 (c (n "kelpdot_macros") (v "2.0.2") (h "041bgwwhnhqv189nqzpis3k0kri98kw7fhszvxrfpggala3b38fg")))

(define-public crate-kelpdot_macros-2.1.0 (c (n "kelpdot_macros") (v "2.1.0") (h "0ina0yq9a8nqvirh7dvynkm47nmbpf4vj7k649fvl3nh7lszcwdv")))

(define-public crate-kelpdot_macros-2.1.3 (c (n "kelpdot_macros") (v "2.1.3") (h "1bs6gl5bf5g99f7a12cgih3bwndlzb76n75xccqrhjlk3klv94zq")))

