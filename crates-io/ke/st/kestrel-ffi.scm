(define-module (crates-io ke st kestrel-ffi) #:use-module (crates-io))

(define-public crate-kestrel-ffi-0.1.0 (c (n "kestrel-ffi") (v "0.1.0") (d (list (d (n "kestrel-crypto") (r "^0.10.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00bg8p14q36bz12bx70car0vc0m8gdbjy2m1ldai5yy67jw6bkrz")))

