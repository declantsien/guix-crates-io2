(define-module (crates-io ke cl keclc-ioframe) #:use-module (crates-io))

(define-public crate-keclc-ioframe-0.1.0 (c (n "keclc-ioframe") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "either") (r "^1.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "kayrx") (r "^0.7") (d #t) (k 0)) (d (n "kayrx") (r "^0.7") (d #t) (k 2)) (d (n "kayrx-test") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "09psbaqkwnzfdavy36j2fkrc5191xm2ysklv39858fn8y7w5x52z")))

