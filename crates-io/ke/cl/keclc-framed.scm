(define-module (crates-io ke cl keclc-framed) #:use-module (crates-io))

(define-public crate-keclc-framed-0.1.0 (c (n "keclc-framed") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "kayrx") (r "^0.7") (d #t) (k 0)) (d (n "kayrx") (r "^0.7") (d #t) (k 2)) (d (n "kayrx-test") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)))) (h "1wyzliv2ri3qnhw2vhw72rr2b6ljcb8323nl2imfpfhbya4i7qqb")))

