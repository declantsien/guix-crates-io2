(define-module (crates-io ke cl keclc-multipart) #:use-module (crates-io))

(define-public crate-keclc-multipart-0.1.0 (c (n "keclc-multipart") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "httparse") (r "^1.3") (d #t) (k 0)) (d (n "kayrx") (r "^0.7.0") (d #t) (k 0)) (d (n "kayrx") (r "^0.7.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "twoway") (r "^0.2") (d #t) (k 0)))) (h "07n1pnz3dgkla26zyvwa82wizbb52l6gf2kkr5c1qi2s1s6dwxb9")))

