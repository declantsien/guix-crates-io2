(define-module (crates-io ke lv kelvin-btree) #:use-module (crates-io))

(define-public crate-kelvin-btree-0.1.0 (c (n "kelvin-btree") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.4") (d #t) (k 0)))) (h "0km9vavpjmidnrnh2x1nfbc0mdwph97pp8llgjhsm8qkvdkv4ny2") (y #t)))

(define-public crate-kelvin-btree-0.2.0 (c (n "kelvin-btree") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.5") (d #t) (k 0)))) (h "1aa94mgz1w14xbpwmfd51c6vssx7bzy96jyprxjzr7x5xlwj072c") (y #t)))

