(define-module (crates-io ke lv kelvin-hamt) #:use-module (crates-io))

(define-public crate-kelvin-hamt-0.1.0 (c (n "kelvin-hamt") (v "0.1.0") (d (list (d (n "kelvin") (r "^0.4") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 0)))) (h "05fjkc1bvvh6iifwb2p1bw5llxlcap8n56xma6x11zxk6ni99gcb") (y #t)))

(define-public crate-kelvin-hamt-0.2.0 (c (n "kelvin-hamt") (v "0.2.0") (d (list (d (n "kelvin") (r "^0.5") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 0)))) (h "0v9wixsywbpr2kpdl31fx5v0fs1azmw0gwl4k4v7813dvyr04wr7") (y #t)))

(define-public crate-kelvin-hamt-0.3.0 (c (n "kelvin-hamt") (v "0.3.0") (d (list (d (n "kelvin") (r "^0.6") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 0)))) (h "0wb7b0l1xhslfw6m6xyyclm5dx92mg5124266nzs21015x94zzy2") (y #t)))

(define-public crate-kelvin-hamt-0.4.0 (c (n "kelvin-hamt") (v "0.4.0") (d (list (d (n "kelvin") (r "^0.7") (d #t) (k 0)))) (h "0vn2cp1a7wpqs33y14jd04wfjxdlvf3zf7ljmizcmnx48xq4arvd") (y #t)))

(define-public crate-kelvin-hamt-0.5.0 (c (n "kelvin-hamt") (v "0.5.0") (d (list (d (n "kelvin") (r "^0.8") (d #t) (k 0)))) (h "1n0xvhkx68wrb2mjbdk37lk5wr5dir86rvnal9p3zli92cjwssgq") (y #t)))

(define-public crate-kelvin-hamt-0.6.0 (c (n "kelvin-hamt") (v "0.6.0") (d (list (d (n "kelvin") (r "^0.9") (d #t) (k 0)))) (h "0y0yicgaazj7l57nsz42baj0yprj3yq02i61mzz7g47xr3j6l4hf") (y #t)))

(define-public crate-kelvin-hamt-0.7.0 (c (n "kelvin-hamt") (v "0.7.0") (d (list (d (n "kelvin") (r "^0.10") (d #t) (k 0)))) (h "0fcq95ly0cxjhs4kp1gs721bf901pri8nga0map34wf75zf0qial") (y #t)))

(define-public crate-kelvin-hamt-0.8.0 (c (n "kelvin-hamt") (v "0.8.0") (d (list (d (n "kelvin") (r "^0.11") (d #t) (k 0)))) (h "1aw00h5h9xs74vr5qwjjnv4qw8mv54yizisdxr5dnw9n981xnn6g") (y #t)))

(define-public crate-kelvin-hamt-0.9.0 (c (n "kelvin-hamt") (v "0.9.0") (d (list (d (n "kelvin") (r "^0.12") (d #t) (k 0)))) (h "0kv4lbqibrjiairy56s7sq4dlqsrws08mdlgx7x8sj6gh7lh2q3s") (y #t)))

(define-public crate-kelvin-hamt-0.10.0 (c (n "kelvin-hamt") (v "0.10.0") (d (list (d (n "kelvin") (r "^0.13") (d #t) (k 0)))) (h "124mp8shqkc47g8gindfbj9fqqdb98v41a1x9i1h17rg0q7k3c08") (y #t)))

(define-public crate-kelvin-hamt-0.11.0 (c (n "kelvin-hamt") (v "0.11.0") (d (list (d (n "kelvin") (r "^0.17") (d #t) (k 0)))) (h "19f406f305qpjcxnj5mqayb4j82rf4yq8b4in0h1777n99vqfyhn") (y #t)))

(define-public crate-kelvin-hamt-0.12.0 (c (n "kelvin-hamt") (v "0.12.0") (d (list (d (n "kelvin") (r "^0.18") (d #t) (k 0)))) (h "0j7a1i5hxbzmkw4f9pn220dx4gd95z4w0kciz796ryfbixxf3l1l") (y #t)))

(define-public crate-kelvin-hamt-0.13.0 (c (n "kelvin-hamt") (v "0.13.0") (d (list (d (n "kelvin") (r "^0.19") (d #t) (k 0)))) (h "0r95vb0d9ajmizi6p510xlhwyvk58vs9s2wxj9hlv6chha6fv95m") (y #t)))

