(define-module (crates-io ke lv kelvin-two3) #:use-module (crates-io))

(define-public crate-kelvin-two3-0.2.0 (c (n "kelvin-two3") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.6") (d #t) (k 0)))) (h "1av8mdrka1yfysnh3r15azdii45i0rvzp502kqx404mjlay0alkf") (y #t)))

(define-public crate-kelvin-two3-0.3.0 (c (n "kelvin-two3") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.7") (d #t) (k 0)))) (h "09f99dx0wg8s9sqzlvialqi28k13w6fc507i0h83wyi619pv0ww1") (y #t)))

(define-public crate-kelvin-two3-0.4.0 (c (n "kelvin-two3") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.8") (d #t) (k 0)))) (h "1g3llg3jzwgcqza56bqg3jfj6gx0zpzq4n8xymk13n25f97n3c2d") (y #t)))

(define-public crate-kelvin-two3-0.5.0 (c (n "kelvin-two3") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.9") (d #t) (k 0)))) (h "0f1zh12hrvwrfarjr9xp1vg29c7a7ckchz14ckwykklml2lc1l43") (y #t)))

(define-public crate-kelvin-two3-0.6.0 (c (n "kelvin-two3") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.10") (d #t) (k 0)))) (h "0p3fgcfk30nl27gffhmqspv6r9niv8kl017j5ghpi96s8qanzm9k") (y #t)))

(define-public crate-kelvin-two3-0.7.0 (c (n "kelvin-two3") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.11") (d #t) (k 0)))) (h "02yb39fj84lk05dwig2bcb0jas0dg2c11c4zjxwk9k7cghyiyv44") (y #t)))

(define-public crate-kelvin-two3-0.8.0 (c (n "kelvin-two3") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.12") (d #t) (k 0)))) (h "0yacia79c3bmkxi0rsnr65hsbnflvvkxm2papasqmksqc527r67j") (y #t)))

(define-public crate-kelvin-two3-0.9.0 (c (n "kelvin-two3") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.17") (d #t) (k 0)))) (h "1jpnln8bhby48fbbyvfpp1fvl0gpwa23gvhimwk69n4p6dlni5ph") (y #t)))

(define-public crate-kelvin-two3-0.10.0 (c (n "kelvin-two3") (v "0.10.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.18") (d #t) (k 0)))) (h "08fpyy8jm264ypnnaiaw6jrsp19a46rrrwklgkgagzaza92b0zpf") (y #t)))

(define-public crate-kelvin-two3-0.11.0 (c (n "kelvin-two3") (v "0.11.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "kelvin") (r "^0.19") (d #t) (k 0)))) (h "110h93g9rhdd2cinm1ysxlasr1lgyi823djy1mgxg1np7dd47n6h") (y #t)))

