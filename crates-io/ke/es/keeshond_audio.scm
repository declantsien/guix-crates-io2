(define-module (crates-io ke es keeshond_audio) #:use-module (crates-io))

(define-public crate-keeshond_audio-0.25.0 (c (n "keeshond_audio") (v "0.25.0") (d (list (d (n "alto") (r "^3.0.4") (d #t) (k 0)) (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "keeshond_datapack") (r "^0.13.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lewton") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)))) (h "1rffagfgchk8ns0mzay39hivha5z5ad5q2007d2lka9jcrfz67f6")))

