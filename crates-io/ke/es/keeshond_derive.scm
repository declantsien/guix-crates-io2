(define-module (crates-io ke es keeshond_derive) #:use-module (crates-io))

(define-public crate-keeshond_derive-0.26.0 (c (n "keeshond_derive") (v "0.26.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sih2y08gjls1mfjrbwaz6s1v2ypk393yc09dm1rgzk9jmvaw6y2")))

