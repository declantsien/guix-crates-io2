(define-module (crates-io ke es keeshond_migrator) #:use-module (crates-io))

(define-public crate-keeshond_migrator-0.1.0 (c (n "keeshond_migrator") (v "0.1.0") (d (list (d (n "keeshond") (r "^0.24.0") (d #t) (k 0)) (d (n "keeshond_datapack") (r "^0.12.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toodee") (r "^0.3.0") (d #t) (k 0)))) (h "078cp49nlsm8y8bpslzcbqmpjjmrn4sv6rhxlhp477f23s680c83") (y #t)))

(define-public crate-keeshond_migrator-0.1.1 (c (n "keeshond_migrator") (v "0.1.1") (d (list (d (n "keeshond") (r "^0.24.0") (d #t) (k 0)) (d (n "keeshond_datapack") (r "^0.12.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toodee") (r "^0.3.0") (d #t) (k 0)))) (h "0f4v3pzcms6q9jl9asii0gc3myd0f5hp2dyvadzk9kidkw8x3dkh")))

