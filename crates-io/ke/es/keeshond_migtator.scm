(define-module (crates-io ke es keeshond_migtator) #:use-module (crates-io))

(define-public crate-keeshond_migtator-0.1.0 (c (n "keeshond_migtator") (v "0.1.0") (d (list (d (n "keeshond") (r "^0.24.0") (d #t) (k 0)) (d (n "keeshond_datapack") (r "^0.12.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toodee") (r "^0.3.0") (d #t) (k 0)))) (h "1f31dk6hkpiyq4gd69r3m4imdnkj5254rflmx0k8x6dmcvx283br") (y #t)))

(define-public crate-keeshond_migtator-0.1.1 (c (n "keeshond_migtator") (v "0.1.1") (h "1nib47l1hwjak60gccz6hla3qn06y2rm3y1qh72vaa242z42knjc") (y #t)))

