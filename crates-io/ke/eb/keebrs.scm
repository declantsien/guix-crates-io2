(define-module (crates-io ke eb keebrs) #:use-module (crates-io))

(define-public crate-keebrs-0.1.0 (c (n "keebrs") (v "0.1.0") (h "0p9dbzap43m8gzxzfgbqh4a8fa3rs67awx71vmdajid2xag07d8c")))

(define-public crate-keebrs-0.3.0 (c (n "keebrs") (v "0.3.0") (d (list (d (n "async-codec") (r "^0.4.0") (f (quote ("embrio"))) (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("alloc"))) (k 0)) (d (n "lock_api") (r "^0.4.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "newtype") (r "^0.2.1") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.23") (d #t) (k 0)) (d (n "polymer-core") (r "^0.1.0") (d #t) (k 0)) (d (n "postcard") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive" "alloc"))) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.5") (d #t) (k 0)))) (h "0waizwmsa0cx8c7fsn08csjvpnznczlbls65yw8s4w7a3chd7kgj") (f (quote (("exhaustive") ("default"))))))

