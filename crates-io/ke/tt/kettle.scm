(define-module (crates-io ke tt kettle) #:use-module (crates-io))

(define-public crate-kettle-0.1.0 (c (n "kettle") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "043wky20msrm3cd8f19av2ah5i0is7ligcpxxrmx2b7y3mb8gvsv") (y #t)))

(define-public crate-kettle-0.1.1 (c (n "kettle") (v "0.1.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1yvsvvixpmw67dpp9rdkvd510g390l1mrd1264qm5s0am4883vhv") (y #t)))

(define-public crate-kettle-0.2.0 (c (n "kettle") (v "0.2.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1wjrif9glmq46p2whi4yqhc2skqcmh391jb7g1z9vv95qj3imdm9") (y #t)))

(define-public crate-kettle-0.2.1 (c (n "kettle") (v "0.2.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0lx9l8xqzhy13agr386k1vmgpzlam9qgjzd27r6frx949w55jfwq") (y #t)))

(define-public crate-kettle-0.2.2 (c (n "kettle") (v "0.2.2") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "18i4mc890mp623aw1h66g261117as0q4n7agmch5vn6gcgq24vnh") (y #t)))

