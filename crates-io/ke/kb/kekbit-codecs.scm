(define-module (crates-io ke kb kekbit-codecs) #:use-module (crates-io))

(define-public crate-kekbit-codecs-0.1.0 (c (n "kekbit-codecs") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)))) (h "09pb55mhlh0kkjfphcp87hj29313afavixqzalg0av5xmwn44nba")))

(define-public crate-kekbit-codecs-0.1.1 (c (n "kekbit-codecs") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)))) (h "072swp2rrsi1f0kqc6f43r053pf2fcm20gd8l90x8ccjr1dgjcvh") (y #t)))

