(define-module (crates-io ke kb kekbit) #:use-module (crates-io))

(define-public crate-kekbit-0.1.0 (c (n "kekbit") (v "0.1.0") (d (list (d (n "kekbit-core") (r "^0.1.1") (d #t) (k 0)))) (h "116z8kl6ygcj51r8hd2ml1v2rqh1q4wjizd1v3iqwzj2d6vblkan")))

(define-public crate-kekbit-0.1.1 (c (n "kekbit") (v "0.1.1") (d (list (d (n "kekbit-core") (r "^0.1.1") (d #t) (k 0)))) (h "1vvslz5wg7jbdgd2kjg5zh44rjmad9pizi71s3879c7hp62rharn")))

(define-public crate-kekbit-0.2.1 (c (n "kekbit") (v "0.2.1") (d (list (d (n "kekbit-core") (r "^0.2.1") (d #t) (k 0)))) (h "161csm0x0vcb5szd33bvqsg4qzvmr06rry7jjhmgdgsf8b2gg2p4")))

(define-public crate-kekbit-0.2.2 (c (n "kekbit") (v "0.2.2") (d (list (d (n "kekbit-codecs") (r "^0.1.0") (d #t) (k 0)) (d (n "kekbit-core") (r "^0.2.2") (d #t) (k 0)))) (h "0v3wl67igb05yin8ncwpgnr9r20dkl7lfkx4i1xmjmgq5qawlx6c")))

(define-public crate-kekbit-0.2.3 (c (n "kekbit") (v "0.2.3") (d (list (d (n "kekbit-codecs") (r "^0.1.1") (d #t) (k 0)) (d (n "kekbit-core") (r "^0.2.3") (d #t) (k 0)))) (h "0m09bv942cja54fxd9q3lyk8rha0mm91zhg3xrq00cl44802c2lr")))

(define-public crate-kekbit-0.3.0 (c (n "kekbit") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1ycpkdcmnp4m129igms036b80snfg4lvzjysw0qkf88jgcld4qdr")))

(define-public crate-kekbit-0.3.1 (c (n "kekbit") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1vsn9r9dpwidxyp37m546iqkzb6xzinq23nall3m2x71qw2jgxvl")))

(define-public crate-kekbit-0.3.2 (c (n "kekbit") (v "0.3.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1dd3g7969c293n4bag88y0mj3yp65i3w01m6i9ym9ffplffnq73s")))

(define-public crate-kekbit-0.3.3 (c (n "kekbit") (v "0.3.3") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1mp2brskm59rilnhcxzqjns5gb2v51nqr53fad1bm0xhbfan1kph")))

(define-public crate-kekbit-0.3.4 (c (n "kekbit") (v "0.3.4") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.7.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1mxw7glmarwxlg2vwx8q4l8fkqrsh4d5a1kxivsh3h2ms88a17rv")))

(define-public crate-kekbit-0.3.5 (c (n "kekbit") (v "0.3.5") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.7") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "140l3wf13i6nfr0sm67iqkqawyml0qi8vn1f8pric4s0azg36k7k")))

