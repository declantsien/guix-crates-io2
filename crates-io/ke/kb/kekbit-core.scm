(define-module (crates-io ke kb kekbit-core) #:use-module (crates-io))

(define-public crate-kekbit-core-0.1.0 (c (n "kekbit-core") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0g77wnma9qr253lppmbch3p5mf9asxh64gjjla6q0a9k7jzhh0aj")))

(define-public crate-kekbit-core-0.1.1 (c (n "kekbit-core") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1zzcq69npcr11hxhxyazygi3snnw3hsijvrfv81a1zj83dhaxnmf")))

(define-public crate-kekbit-core-0.2.1 (c (n "kekbit-core") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "04pvalwr20kbzdff7vz2vlaaqj9i3fkfzjrxq4k9gl1by3rdalkf")))

(define-public crate-kekbit-core-0.2.2 (c (n "kekbit-core") (v "0.2.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "kekbit-codecs") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0z07byk50k5rnlp7fscsrmjnrvybiibv58j8lmzip984glxn35j4")))

(define-public crate-kekbit-core-0.2.3 (c (n "kekbit-core") (v "0.2.3") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "kekbit-codecs") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "07rx1b13liyyygcgxpm0y86i4dsl7ckswk4mc7gyrhryw85mgng8") (y #t)))

