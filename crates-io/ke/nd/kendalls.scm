(define-module (crates-io ke nd kendalls) #:use-module (crates-io))

(define-public crate-kendalls-0.1.0 (c (n "kendalls") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "0l3j4bpxp0c0q5xk5m3kd9d0b47mwa3s61wriwbx3hclp4qwan7i")))

(define-public crate-kendalls-0.1.1 (c (n "kendalls") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "0svw8xn8w3wn751kv8q0fmakqg9npwlscyxdv8b0wbf123gb05gn")))

(define-public crate-kendalls-0.1.2 (c (n "kendalls") (v "0.1.2") (h "0y71n46rqqavmlkzl2vajs5rcixlkxn6fap3f3mm3ffj4k6s1jp1")))

(define-public crate-kendalls-0.1.3 (c (n "kendalls") (v "0.1.3") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)))) (h "1z4gs40c901ms40q9j1xpnjbsygqqa3ci2w8wfc8fx4ycx21l096")))

(define-public crate-kendalls-0.1.4 (c (n "kendalls") (v "0.1.4") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)))) (h "0wirl9xj1nsrfjspqjsa55ym65jcii9rwr6q4kv0qz6w63gdpx82")))

(define-public crate-kendalls-0.1.5 (c (n "kendalls") (v "0.1.5") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)))) (h "1mmq663n3xv03f7xykkvzypz8cdq99z1w1grjghkgrbck9krqv23")))

(define-public crate-kendalls-0.2.0 (c (n "kendalls") (v "0.2.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)))) (h "1dxwiqx97y2rawkrvx8qsamz5pjaxj3lgnmn997wwiyxr94px91s")))

(define-public crate-kendalls-0.2.1 (c (n "kendalls") (v "0.2.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)))) (h "18fmcdgafh30fldinljx3w79mfj1yj9fwp2v3gmh95pq28k70c7r")))

