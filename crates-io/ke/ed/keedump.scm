(define-module (crates-io ke ed keedump) #:use-module (crates-io))

(define-public crate-keedump-0.1.0 (c (n "keedump") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1nljkjqjpnaj3lbllqs63xncfbc453n6b8szqk68ddr9ngvagzpq")))

