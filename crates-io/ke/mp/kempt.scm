(define-module (crates-io ke mp kempt) #:use-module (crates-io))

(define-public crate-kempt-0.1.0 (c (n "kempt") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.163") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 2)))) (h "10fd80mqm6ll53n5gy1xpc8i07wswmjvv4jl47qi2pm7pa5dmxxr") (r "1.65.0")))

(define-public crate-kempt-0.2.0 (c (n "kempt") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.163") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 2)))) (h "1vz5jd2np1l94z0lzpmi8l6yb6a5iqsdhkav0vxgwpd348gidsbq") (r "1.65.0")))

(define-public crate-kempt-0.2.1 (c (n "kempt") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.163") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 2)))) (h "04jljrjyjm786c2jhbawpxc9qsa67hfjbbl6lahbigzq5nqpmyg8") (r "1.65.0")))

(define-public crate-kempt-0.2.2 (c (n "kempt") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.163") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 2)))) (h "003j0vg8z6a5rsp5a31mj13xrpk6hw09d66jxb5wf7myggli5a8k") (r "1.65.0")))

(define-public crate-kempt-0.2.3 (c (n "kempt") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.163") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 2)))) (h "0w9h7ma86sw793yq08vifxnmlmb6yyd0dazv3v5nmbifnnyscdsa") (r "1.65.0")))

(define-public crate-kempt-0.2.4 (c (n "kempt") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.163") (o #t) (k 0)) (d (n "serde_test") (r "^1.0.163") (d #t) (k 2)))) (h "06qz9fpg1mr09ja9cjqpaiqs10xivk1lx5ni98z3r4xlrwrl9grm") (r "1.65.0")))

