(define-module (crates-io ke nk kenku_control) #:use-module (crates-io))

(define-public crate-kenku_control-0.1.0 (c (n "kenku_control") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hd0g2fir3q0mdc759wq6ym6alv59hc69rajgcgil8vmp5z0a5a9")))

(define-public crate-kenku_control-0.1.1 (c (n "kenku_control") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "037rmc51j8h51fn8rpmg60bdj3zwahvz46gd0wqwbbbzjpp4brfh")))

(define-public crate-kenku_control-0.2.0 (c (n "kenku_control") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0rrprvqavmwhjz82k39kpg90pjdh7kmdfmagaxnq39lvzgban68n")))

(define-public crate-kenku_control-0.2.1 (c (n "kenku_control") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0vpbf6dw80vy7z6yn486mr1ij70w022gghbarxi3vncy2kcrlizy")))

(define-public crate-kenku_control-0.2.2 (c (n "kenku_control") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1y433kw06cblp03sig5w8g3zz1sr9qkmgvsqnvn955dl5rvw4gdf")))

(define-public crate-kenku_control-0.2.2-hotfix (c (n "kenku_control") (v "0.2.2-hotfix") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ywj30913x24w5v1h3fxlc3fcsfmq4ixl4yj6275nvdp31dfx631")))

(define-public crate-kenku_control-0.2.3 (c (n "kenku_control") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1ln264mf71fbh391hq6ivs910sz52zd759q07vbznawdia6snxpp")))

