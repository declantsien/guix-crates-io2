(define-module (crates-io ke ra keratin) #:use-module (crates-io))

(define-public crate-keratin-0.1.0 (c (n "keratin") (v "0.1.0") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "03h25g19b8zls23jgf4yign2yp0fdz0d01paiz2a2k9w365pdgb0")))

(define-public crate-keratin-0.1.1 (c (n "keratin") (v "0.1.1") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1nxi7m4dhs7xki09w65012bg4y2r8p0byfmsm52fr5j19h306kn7")))

(define-public crate-keratin-0.1.2 (c (n "keratin") (v "0.1.2") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "03rmgx5w9ipc41znrh7rvxcy08jsipwiwcqxc5ivgb4xbpmyjp5z")))

(define-public crate-keratin-0.1.3 (c (n "keratin") (v "0.1.3") (d (list (d (n "bson") (r "^0.14.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "021jasifkg08lpx4d97y1n4b7k0ir8b01xxbawm2i2p9j21pcapb")))

(define-public crate-keratin-0.1.4 (c (n "keratin") (v "0.1.4") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1pfcpv8n0414rmrn7cawds098b76iq00xaali2nd7ixga8lbbmy6")))

(define-public crate-keratin-0.1.5 (c (n "keratin") (v "0.1.5") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0m30vmf80jvglhnmwy5b3h610zd468grymkpky47p9az8xz3pazq")))

(define-public crate-keratin-0.1.6 (c (n "keratin") (v "0.1.6") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "118z56y00jdwdvsdnvkh66dw1aza0k3rfqx12rsabggvpj0snzr3")))

(define-public crate-keratin-0.1.7 (c (n "keratin") (v "0.1.7") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "03ps0zl5p2xn9syfknj8f3al4z0iizbvgrb7lpcdx7gpiwqndmqz")))

(define-public crate-keratin-0.2.0 (c (n "keratin") (v "0.2.0") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "00m4lpwfzmff3lkszzjj18rg9d32rrr4yyk78xkp1jw8lr9766zc")))

(define-public crate-keratin-0.2.1 (c (n "keratin") (v "0.2.1") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "18xnzvijqdkwng918n7nbzh9gfylfmwlsiw3s63cigixbhh3p6ng")))

(define-public crate-keratin-0.2.2 (c (n "keratin") (v "0.2.2") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1434kp5lmbvsg6b97aixnfmfzwsv8dfy8lm0a3z0gjk0nvp87sks")))

(define-public crate-keratin-0.2.3 (c (n "keratin") (v "0.2.3") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "05r345w4kk0ffisn9lpg29916m580g6fbc77i5w78prl56l2as5g")))

(define-public crate-keratin-0.2.4 (c (n "keratin") (v "0.2.4") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0q5z7mx328p7sc9l0g8jgs25mawnxq83l50y7q6v5qjs6ggz2maw")))

(define-public crate-keratin-0.2.5 (c (n "keratin") (v "0.2.5") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1ax42c1848hk4qq28662v8vg6n0zdc3circgy3gvjdlb7ifc9rp5")))

(define-public crate-keratin-0.2.6 (c (n "keratin") (v "0.2.6") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "17i2aghqb0w99a59agk4pvq0v1y54r0rdm6pvj4ddw73n8ra9a9f")))

(define-public crate-keratin-0.2.7 (c (n "keratin") (v "0.2.7") (d (list (d (n "bson") (r "^1.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.56") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "14z9zl52ric9xkk4mdzxzw3qd82rvx379pykl7h58yifc4cdypzx")))

