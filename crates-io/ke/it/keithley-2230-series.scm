(define-module (crates-io ke it keithley-2230-series) #:use-module (crates-io))

(define-public crate-keithley-2230-series-0.1.0 (c (n "keithley-2230-series") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.1.1") (d #t) (k 0)))) (h "1cw6d6ym2zaxjdrybpcnjrwn868v3hwa6kxdls4gyxvw8s1rknip")))

(define-public crate-keithley-2230-series-0.1.1 (c (n "keithley-2230-series") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.1.1") (d #t) (k 0)))) (h "1dmsr408jq603ycmdypz6mrgbqk0mrzvsrq0szwfs6jlxr7x3qia")))

(define-public crate-keithley-2230-series-0.1.2 (c (n "keithley-2230-series") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.1.1") (d #t) (k 0)))) (h "1r1dlpchphvljq9vd9a9mrjb659hs7axm2xcppvlbpa0rfd6z59h")))

(define-public crate-keithley-2230-series-0.1.3 (c (n "keithley-2230-series") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.1.1") (d #t) (k 0)))) (h "0cd14c2akwancq108fg0n4h657gi5y2j9vd34yfrw8jd7m90nwar")))

(define-public crate-keithley-2230-series-0.1.4 (c (n "keithley-2230-series") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.1.1") (d #t) (k 0)))) (h "0j4kn4a2azbd7awiigf3smb0fpr9wyjk2g49bx4has1g84krrk9z")))

(define-public crate-keithley-2230-series-0.1.5 (c (n "keithley-2230-series") (v "0.1.5") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.1.1") (d #t) (k 0)))) (h "1xjv4vv1bgpvc7mnf54zrmhldp83yvfj8dmrz2maiyixwh6cka9f")))

(define-public crate-keithley-2230-series-0.2.0 (c (n "keithley-2230-series") (v "0.2.0") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.2.1") (d #t) (k 0)))) (h "0zk9fh2zngb8c05425kjln0ln6zzy37m9lfs7rrv6irpr7adiks6")))

(define-public crate-keithley-2230-series-0.2.1 (c (n "keithley-2230-series") (v "0.2.1") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.2.2") (d #t) (k 0)))) (h "1lil54pvg0l2b940byx8k7qbvzjvqmykmk4v2f36q1w8w76wd9ic")))

(define-public crate-keithley-2230-series-0.2.2 (c (n "keithley-2230-series") (v "0.2.2") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.2.2") (d #t) (k 0)))) (h "1xbrjrck1d87ayb4jhx6lsvlgiiysla4jkkcw2swassvir4cdb1v")))

(define-public crate-keithley-2230-series-0.2.3 (c (n "keithley-2230-series") (v "0.2.3") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.2.2") (d #t) (k 0)))) (h "0yncgj0227f3sfxlrgxrggs2a4z3sr6rk1fwdfv40wk3jfm38kd7")))

(define-public crate-keithley-2230-series-0.2.4 (c (n "keithley-2230-series") (v "0.2.4") (d (list (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "visa-api") (r "^0.2.2") (d #t) (k 0)))) (h "1bygxn0fzx6wm8p4pdlmk86lcqgh7w3x6sd04cax535a6336xizb")))

