(define-module (crates-io as in asink) #:use-module (crates-io))

(define-public crate-asink-0.1.0 (c (n "asink") (v "0.1.0") (d (list (d (n "bson") (r "^0.10") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "mongodb") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r6fl5yslszyax9pgxk8x1s9lrzsibbx2hibad60dylnm6dwav16")))

(define-public crate-asink-0.1.1 (c (n "asink") (v "0.1.1") (d (list (d (n "bson") (r "^0.10") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "mongodb") (r "^0.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17cp7hf7s2ak8v4yi1c1zj441fq7bam28y0y4lkg3a8wbqi5kw4y")))

(define-public crate-asink-0.1.2 (c (n "asink") (v "0.1.2") (d (list (d (n "bson") (r "^0.12") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "mongodb") (r "^0.3.10") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1grbp6ay4pbcf7ydnk13diqyr7zw4nd7azl9628lyw5np9ayjlvl")))

