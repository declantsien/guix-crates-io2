(define-module (crates-io as he asherpa) #:use-module (crates-io))

(define-public crate-asherpa-0.0.1 (c (n "asherpa") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("rustls-tls"))) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "14rbsq3hnhys6x2g1s2vhlja5r9ljbxiihf93waij4dhdk8af6bm")))

