(define-module (crates-io as -m as-method) #:use-module (crates-io))

(define-public crate-as-method-0.1.0 (c (n "as-method") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "116h9g1jidbl6r41a79jmsnrr47223gzg196q1mrp074iacjb9dm")))

