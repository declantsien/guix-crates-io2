(define-module (crates-io as ah asahi-btsync) #:use-module (crates-io))

(define-public crate-asahi-btsync-0.1.0 (c (n "asahi-btsync") (v "0.1.0") (d (list (d (n "apple-nvram") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)))) (h "1zkq2sl2w2bgpr3zjiqdp0x0mnf2mgxzrk723dc932d0dwjyi8dj")))

(define-public crate-asahi-btsync-0.2.0 (c (n "asahi-btsync") (v "0.2.0") (d (list (d (n "apple-nvram") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)))) (h "1a3hd2adg81k8cf3l4bpn99fg7bw6lcj5c3snjs89s8m9h4gyy5y")))

