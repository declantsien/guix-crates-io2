(define-module (crates-io as ah asahi-wifisync) #:use-module (crates-io))

(define-public crate-asahi-wifisync-0.1.0 (c (n "asahi-wifisync") (v "0.1.0") (d (list (d (n "apple-nvram") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)))) (h "0qw0plapda7w1mw9df7hyqhllr705sl6ylqpyj5jcyjachqk7y0w")))

(define-public crate-asahi-wifisync-0.2.0 (c (n "asahi-wifisync") (v "0.2.0") (d (list (d (n "apple-nvram") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)))) (h "1hb68im62jkzi1v8lip8ripiaz0c2n04k4mkdfrcbakb1s3hhzm8")))

