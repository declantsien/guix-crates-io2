(define-module (crates-io as ah asahi-bless) #:use-module (crates-io))

(define-public crate-asahi-bless-0.1.0 (c (n "asahi-bless") (v "0.1.0") (d (list (d (n "apple-nvram") (r "^0.1") (d #t) (k 0)) (d (n "gpt") (r "^3") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0v9bswzblmjhhg4s6jb9fsvcnlqy43av0p8k3z59h2a3b3yb6yp6")))

(define-public crate-asahi-bless-0.2.0 (c (n "asahi-bless") (v "0.2.0") (d (list (d (n "apple-nvram") (r "^0.2") (d #t) (k 0)) (d (n "gpt") (r "^3") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0dabbbnd39hma2psbn8vyzdc8jpjm6znxw4cf23pwd5fcyfq0qzp")))

(define-public crate-asahi-bless-0.2.1 (c (n "asahi-bless") (v "0.2.1") (d (list (d (n "apple-nvram") (r "^0.2") (d #t) (k 0)) (d (n "gpt") (r "^3") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0x153750d3pn6pwvwpk348n66xv4vxd5q4z0wqvw4qv1yl1wzl5h")))

(define-public crate-asahi-bless-0.3.0 (c (n "asahi-bless") (v "0.3.0") (d (list (d (n "apple-nvram") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gpt") (r "^3") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1x5g6pb5i3zgrlxvdzzrv58sp647cp5z8vdp3gp7czdlbyy1rhpx")))

