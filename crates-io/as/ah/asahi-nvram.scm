(define-module (crates-io as ah asahi-nvram) #:use-module (crates-io))

(define-public crate-asahi-nvram-0.1.0 (c (n "asahi-nvram") (v "0.1.0") (d (list (d (n "apple-nvram") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)))) (h "131dfrkvmwim806i53292aij226kqvp395260bqxi55by04iid3a")))

(define-public crate-asahi-nvram-0.2.0 (c (n "asahi-nvram") (v "0.2.0") (d (list (d (n "apple-nvram") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0b0pig15vw05zl44hpdwlzk84d7c871a28bqvl501gl718ljfkg9")))

(define-public crate-asahi-nvram-0.2.1 (c (n "asahi-nvram") (v "0.2.1") (d (list (d (n "apple-nvram") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)))) (h "04qp73w97792flzkv9b3qb549iwa9mam837b0l8zh04lcnbcgci6")))

