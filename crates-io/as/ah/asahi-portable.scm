(define-module (crates-io as ah asahi-portable) #:use-module (crates-io))

(define-public crate-asahi-portable-0.1.0 (c (n "asahi-portable") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "smurf") (r "^0.2.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1yvbxsl3cyr2prsry7z73h1c94ffihyx81gm6jrrn4r14gs5ry55") (y #t)))

