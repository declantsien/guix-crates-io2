(define-module (crates-io as db asdb-taxa) #:use-module (crates-io))

(define-public crate-asdb-taxa-0.1.0 (c (n "asdb-taxa") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "0xrglvial5ja4kapx984qvli4achmcml4v3shb90kcs30hqpzgvd")))

