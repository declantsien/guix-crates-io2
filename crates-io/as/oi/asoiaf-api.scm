(define-module (crates-io as oi asoiaf-api) #:use-module (crates-io))

(define-public crate-asoiaf-api-0.1.0 (c (n "asoiaf-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0g1qfqv4apavlj77ld62b3s8afcpxg7mf33228xlw1cik93bxpnb")))

(define-public crate-asoiaf-api-0.1.1 (c (n "asoiaf-api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0i1r96l2mg9l6r499pg1qxxhmkxhwhnn8zig26xbaa9975kaw08p")))

