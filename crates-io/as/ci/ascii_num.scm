(define-module (crates-io as ci ascii_num) #:use-module (crates-io))

(define-public crate-ascii_num-0.1.0 (c (n "ascii_num") (v "0.1.0") (h "0c9yxgidjzw2y3h7lp5yy4wkddx3l0g7xf7s0fp82v7cn3d7zqli")))

(define-public crate-ascii_num-0.1.1 (c (n "ascii_num") (v "0.1.1") (h "1ma1dl3qbnp6nzhgrpi1mkxnjq3nwvn5ya7lb50nssqvbi8sxdxz")))

(define-public crate-ascii_num-0.1.2 (c (n "ascii_num") (v "0.1.2") (h "1rkc6flkyc95g5771k0vn6gkvcjng71lgjwxbkpmsnbn85xm6b1l")))

(define-public crate-ascii_num-0.1.3 (c (n "ascii_num") (v "0.1.3") (h "1zx9jlcn0f43sjp7r2p8wb26kpkfq6q08p5yxrmd08f532xjgslz")))

