(define-module (crates-io as ci asciiutils) #:use-module (crates-io))

(define-public crate-asciiutils-0.5.0 (c (n "asciiutils") (v "0.5.0") (h "05v6fc4azf8lk4x57yfjadxv8z438crvdb18nmaxy8vm0gp3czjz") (y #t)))

(define-public crate-asciiutils-0.5.1 (c (n "asciiutils") (v "0.5.1") (h "07f97pcp1bp97llgzy05r4h609jllk3xwb3gczm5bncrwd5sd6bh") (y #t)))

(define-public crate-asciiutils-0.5.2 (c (n "asciiutils") (v "0.5.2") (h "1k84p3lxnf2phald66y206ifva7a47ivxpvwcv08sny257skxk87") (y #t)))

(define-public crate-asciiutils-0.6.0 (c (n "asciiutils") (v "0.6.0") (h "1nnb388mvh7al4czh4zpb2krzhrmx6709xc1d13s9bwscd89p00i") (y #t)))

(define-public crate-asciiutils-0.6.1 (c (n "asciiutils") (v "0.6.1") (h "124jfi16q1nbzpfbs88pqwdpa1gp6z8jwy2ps79305gr3ilk3rwn") (y #t)))

(define-public crate-asciiutils-0.6.2 (c (n "asciiutils") (v "0.6.2") (h "031y1ynri4f8j2mlz7z18z2s9bpayrdzfqp2bcmsv31zg19q5ydm") (y #t)))

(define-public crate-asciiutils-0.6.3 (c (n "asciiutils") (v "0.6.3") (h "12rz3mn97y47xym27ax5cry5pvhisilp958ncl5597wvdyy486lq") (y #t)))

(define-public crate-asciiutils-0.7.0 (c (n "asciiutils") (v "0.7.0") (h "0qbndq7fvm32awahl2wp2v8gyb01arignapb92w5ngpyk9q0n389") (y #t)))

(define-public crate-asciiutils-0.7.1 (c (n "asciiutils") (v "0.7.1") (h "0rwzlwdq3c7ia74z3qn2xqbmzlc8g72lakxj2mvq7n9fnnbcv9sp") (y #t)))

(define-public crate-asciiutils-0.7.2 (c (n "asciiutils") (v "0.7.2") (h "0s52gwkqyvaddyxnh5v6afrh0ya79w7p89j27f6806m5n1sn2iaq") (y #t)))

(define-public crate-asciiutils-0.7.3 (c (n "asciiutils") (v "0.7.3") (h "03jyakis3p74jfrv9k96mbw66q2xpdcbk0vlv3a4npbam1k85gc1") (y #t)))

(define-public crate-asciiutils-0.8.0 (c (n "asciiutils") (v "0.8.0") (h "0xa2m5nni278y0bnpx5lh9fx1c3g2ybvp43m6ap18d9i3naqjyra") (y #t)))

(define-public crate-asciiutils-0.8.1 (c (n "asciiutils") (v "0.8.1") (h "1knv215rynwfz548bz9b2iq52rn23sygdhwdsij5c6ksy0pz1cz5") (y #t)))

