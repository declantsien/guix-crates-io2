(define-module (crates-io as ci asciis) #:use-module (crates-io))

(define-public crate-asciis-0.1.0 (c (n "asciis") (v "0.1.0") (h "19qx7mv29zfn4cf0zxavrm72l4vg9xkgvfmgl2hg8y5i7ijignvd")))

(define-public crate-asciis-0.1.1 (c (n "asciis") (v "0.1.1") (h "0117gbz8znwsqnfqsxwc7ajrgd6l54c4qjaghm1pibx2rh70qs1z")))

(define-public crate-asciis-0.1.2 (c (n "asciis") (v "0.1.2") (h "0y4cvckp521la6zhczdq1c37s7y6508hrrq4jswrdhnmras5bvnn")))

(define-public crate-asciis-0.1.3 (c (n "asciis") (v "0.1.3") (h "06p7xd0ib0hnmz367g8zch282qvc9qmkmi3m1dcayvasvjxkh13n")))

