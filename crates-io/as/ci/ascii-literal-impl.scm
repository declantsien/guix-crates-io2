(define-module (crates-io as ci ascii-literal-impl) #:use-module (crates-io))

(define-public crate-ascii-literal-impl-0.1.0 (c (n "ascii-literal-impl") (v "0.1.0") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1id6hgfpvv99amzzfgf4rr23wcq9xlcvw97jyhgyhjidr4a1lga3")))

