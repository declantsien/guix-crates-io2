(define-module (crates-io as ci ascii_domain) #:use-module (crates-io))

(define-public crate-ascii_domain-0.1.0 (c (n "ascii_domain") (v "0.1.0") (h "1hbcr2p266a8yz4y3dfff0hpgr4js58y4b9wdq0pyh4abhx6fzap")))

(define-public crate-ascii_domain-0.2.0 (c (n "ascii_domain") (v "0.2.0") (h "1fpqm8kzqirm822pbz5r7gmkhrki91wyfz79dq4xkx95k39xazgm")))

(define-public crate-ascii_domain-0.3.0 (c (n "ascii_domain") (v "0.3.0") (h "0qg6717dwfn0l3wrdd1p3jpfhawwf6d396sn51yysabqw8vgj8y1")))

(define-public crate-ascii_domain-0.3.1 (c (n "ascii_domain") (v "0.3.1") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.196") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 2)))) (h "0q2n6p1bhwcwxz4km6q4a2aflq8iq6mcn4hzi4bk2r6n940fi1z0") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ascii_domain-0.4.0 (c (n "ascii_domain") (v "0.4.0") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.196") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 2)))) (h "0x183v5qaqfc98afr6gy5cxvi87xd339kmp245iv2xagvmagcrrg") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ascii_domain-0.5.0 (c (n "ascii_domain") (v "0.5.0") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.196") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 2)))) (h "1xarf1rm5jnhxnsqgzc7mmmp6zv4k7p6a4j9n69ggsz7c7ig183g") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ascii_domain-0.6.0 (c (n "ascii_domain") (v "0.6.0") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.196") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (f (quote ("alloc"))) (k 2)))) (h "1crb35xq4j3n80jcb70bwaj3knad0412jm733brkaynhn52mqzwi") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-ascii_domain-0.6.1 (c (n "ascii_domain") (v "0.6.1") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.197") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (f (quote ("alloc"))) (k 2)))) (h "1ga0srmv4cmwc19n1rcg5311am3fqc7nyycvw9xq5jasfpmzxy25") (f (quote (("std")))) (s 2) (e (quote (("serde" "dep:serde"))))))

