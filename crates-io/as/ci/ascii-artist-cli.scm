(define-module (crates-io as ci ascii-artist-cli) #:use-module (crates-io))

(define-public crate-ascii-artist-cli-0.0.1 (c (n "ascii-artist-cli") (v "0.0.1") (d (list (d (n "ascii-artist") (r "^0.0.1") (f (quote ("image"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jx3djgmy0bhp76npkkna4picx4ahk4awsy06cjrpix32hs8i4dr")))

