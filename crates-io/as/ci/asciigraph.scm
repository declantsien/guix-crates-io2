(define-module (crates-io as ci asciigraph) #:use-module (crates-io))

(define-public crate-asciigraph-0.1.0 (c (n "asciigraph") (v "0.1.0") (h "01vf66rvjvwkvxr6y6h7h1777sfq4kc8amk34f9gb8507xg2qmia")))

(define-public crate-asciigraph-0.2.0 (c (n "asciigraph") (v "0.2.0") (h "0d35gdc6m8hxsvk3qp4qbsdfck7ry1gxwa9j7555cfcjf1i64jbk")))

(define-public crate-asciigraph-0.2.1 (c (n "asciigraph") (v "0.2.1") (h "0vilnrlipgyjp2swlrnyr8vkzhc079hsw87dl6ahci25xyalpmwq")))

(define-public crate-asciigraph-0.2.2 (c (n "asciigraph") (v "0.2.2") (h "1lphni4bgm0aci4ljmqcb1xj3ia36b1kh5h49396s1ipyzlbgckc")))

(define-public crate-asciigraph-0.2.3 (c (n "asciigraph") (v "0.2.3") (h "0y61infvql0z8rqwinn59kq69gfk07z4qr9md3gh1v90rdll4xgi")))

(define-public crate-asciigraph-0.3.0 (c (n "asciigraph") (v "0.3.0") (h "12gycvbi94ish2asw03c56wlmskqrdgrnkfpnyvwzhblxzsvpj77")))

(define-public crate-asciigraph-0.4.0 (c (n "asciigraph") (v "0.4.0") (h "1308lv3sj5i3mwf6hrhg2qs41w4nvq86ln7ag0m4iibj84c4gp3y")))

(define-public crate-asciigraph-0.4.1 (c (n "asciigraph") (v "0.4.1") (h "0jn3x1lxrbdwr7gmb7h8bkzwzx5jrgkr4dbk8nnwjk7phpi2c08w")))

(define-public crate-asciigraph-0.5.0 (c (n "asciigraph") (v "0.5.0") (h "15hlc95axigxq6m72gidjimy1d9j3s780qdrw863k7z5xrgd8d60")))

(define-public crate-asciigraph-0.5.1 (c (n "asciigraph") (v "0.5.1") (h "0jy3480mqccsf1zsbkpagr7jnd34w5a5xcgc4n6vhm5cmbdy5sz3")))

(define-public crate-asciigraph-0.5.2 (c (n "asciigraph") (v "0.5.2") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)))) (h "0vrqwqhvz1s34w77qaibcp8bj89w4jrvq0w5nssm3cs0jpwsbfzi")))

(define-public crate-asciigraph-0.5.3 (c (n "asciigraph") (v "0.5.3") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)))) (h "1jq6xaxxxk1xy7m7m4pkjz6917yq8aglz1hd6zz9zvkc02cf0mar")))

(define-public crate-asciigraph-0.5.4 (c (n "asciigraph") (v "0.5.4") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)))) (h "1ys5lc5v7md77hrvhp0aarrqwpiwxmgfcsgbs54bnpk0y1gxhq88")))

(define-public crate-asciigraph-0.6.0 (c (n "asciigraph") (v "0.6.0") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)))) (h "1hcp2k054iyvagb22iby1hwbgfzxbjj947b3805nn946fa2vydj9")))

(define-public crate-asciigraph-0.7.0 (c (n "asciigraph") (v "0.7.0") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0jncq44978nprm5khy2pdmx8kzc3mb5kdih52qycg4kkb15x9p7h")))

(define-public crate-asciigraph-0.7.1 (c (n "asciigraph") (v "0.7.1") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0w95hgwbl6qk99bk71aa68arv1j7wr0plw21f1qdqqzq8zq3cl8b")))

(define-public crate-asciigraph-0.7.2 (c (n "asciigraph") (v "0.7.2") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "184jbkmy6f5ydvgi9bvh0kgq8ib910dfl8ijxmvvr03hf3hh1bpg")))

(define-public crate-asciigraph-0.7.3 (c (n "asciigraph") (v "0.7.3") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1p3xc6qrdrya4p144ikkv1j4d1x9fa3p10psdm81chf0hbs0pqn5")))

(define-public crate-asciigraph-0.7.4 (c (n "asciigraph") (v "0.7.4") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0nqvc6dvw686pgarf151jdhv92d5dnbs1b9awapyqv88p4nkv6aj")))

(define-public crate-asciigraph-0.7.5 (c (n "asciigraph") (v "0.7.5") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zlxj10hv74dz1plisd9mdvg9vcqa46412bj51qygccx7iqnrasy")))

(define-public crate-asciigraph-0.7.6 (c (n "asciigraph") (v "0.7.6") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0aqkhv9zn82vzx6p89dvfjky6v83jbwgipa4h01im65wqk7a4ynl")))

(define-public crate-asciigraph-0.7.7 (c (n "asciigraph") (v "0.7.7") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "034yhmm0ldd10v8qz56kjyi6gi5m2n2kshvzra3hnwgpyzi8gmqw")))

(define-public crate-asciigraph-0.7.8 (c (n "asciigraph") (v "0.7.8") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "199xdbg6mlmj5ncrxkc4gcikd58ab714b9mcac3y5dfh1a12dcbg")))

(define-public crate-asciigraph-0.7.9 (c (n "asciigraph") (v "0.7.9") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ql5ksqkf1vn4ks5izcs3gg3avkgsjlslyzw8dc5rvgjpkfxiag6")))

(define-public crate-asciigraph-0.7.10 (c (n "asciigraph") (v "0.7.10") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0dlgnapqx1x6q6hnfn5lxnkm2mkzzph7dgdm3k61a7cnrxvi69xm")))

(define-public crate-asciigraph-0.7.11 (c (n "asciigraph") (v "0.7.11") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1n2w3lm16s1m1ys159bv293kv6rxd21ikls70lw6dxcakxbclk5a")))

(define-public crate-asciigraph-0.8.0 (c (n "asciigraph") (v "0.8.0") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "078sqdjjjfsl9m249m2fx9x9pz7hycsd79a8awgb6ydv89qpxqi5")))

(define-public crate-asciigraph-0.8.1 (c (n "asciigraph") (v "0.8.1") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zksdsdgwpmk89i2krf1clg2cph6f24kbf61c1dpar8xn9sz8lyk")))

(define-public crate-asciigraph-0.8.2 (c (n "asciigraph") (v "0.8.2") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05fhz5x7hzf3h2fnfzhs47fkqixfj41x6xd009f9248gkcjlcmzb")))

(define-public crate-asciigraph-0.8.3 (c (n "asciigraph") (v "0.8.3") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "10w6yw3ix9bkq6nyw8jhbrqqckkfsvkcdiphrh5gfk22m01bxkka")))

(define-public crate-asciigraph-0.8.4 (c (n "asciigraph") (v "0.8.4") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0n785gfya01284l3h9prw87qiqbqc66swp4bdkghic618ggcy60f")))

(define-public crate-asciigraph-0.9.0 (c (n "asciigraph") (v "0.9.0") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.17") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0whmgyipnxdcbdrg4hz8l9ypcf3p2vnl1v2m1bhk4rpmiwlwkplk") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:json"))))))

(define-public crate-asciigraph-0.9.1 (c (n "asciigraph") (v "0.9.1") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.17") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1vrdl32w0hahilcy34ya553ws3n7475vxwdkf7nbndlkj5gynhyq") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:json"))))))

(define-public crate-asciigraph-0.9.2 (c (n "asciigraph") (v "0.9.2") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.17") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "06ma0w60bn3zfpq2dyf45dbrph881v1s6hyxgyw7170xlivq926l") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:json"))))))

(define-public crate-asciigraph-0.9.3 (c (n "asciigraph") (v "0.9.3") (d (list (d (n "asciibox") (r "^0.1.0") (d #t) (k 0)) (d (n "hmath") (r "^0.1.17") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0as5cbqshiyk2s837fvi72rmdz66f0m0ryx2a69a67r6wxmini4n") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:json"))))))

