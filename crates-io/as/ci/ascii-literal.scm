(define-module (crates-io as ci ascii-literal) #:use-module (crates-io))

(define-public crate-ascii-literal-0.1.0 (c (n "ascii-literal") (v "0.1.0") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 0)) (d (n "ascii-literal-impl") (r "^0.1") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.5") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1ni8w6c9s6v7zfvfw06c1rmh4wfwb2s7wzqx4rgsgdjbzvbsrjd8")))

