(define-module (crates-io as ci asciimusic) #:use-module (crates-io))

(define-public crate-asciimusic-0.1.0 (c (n "asciimusic") (v "0.1.0") (h "1akz05vl1xwyx2bs3bpkg6nlal1hhcngbilql1d149hilvadgwg4")))

(define-public crate-asciimusic-0.1.2 (c (n "asciimusic") (v "0.1.2") (h "0xbj4lbkjj0va2xfngjjfshwgw4hxdn92rrcg8ai3dgzwn3hgcjh")))

(define-public crate-asciimusic-0.1.3 (c (n "asciimusic") (v "0.1.3") (h "0gpiq4bm5hn5bhf9dv246y35in2p9qhm95jrad00ahczvaq8c5l4")))

