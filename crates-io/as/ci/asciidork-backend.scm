(define-module (crates-io as ci asciidork-backend) #:use-module (crates-io))

(define-public crate-asciidork-backend-0.1.0 (c (n "asciidork-backend") (v "0.1.0") (d (list (d (n "asciidork-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.1.0") (d #t) (k 0)))) (h "1r9vb8y7w6frk37ss5ycyqh4b0vxcr2gsl136lzn7khg0lvzsrj4")))

(define-public crate-asciidork-backend-0.2.0 (c (n "asciidork-backend") (v "0.2.0") (d (list (d (n "asciidork-ast") (r "^0.2.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.2.0") (d #t) (k 0)))) (h "0nfvasnbaaf1i691j6l4swnvgxxa0k6jdjh2mhllc8ir1iq8yjwv")))

(define-public crate-asciidork-backend-0.3.0 (c (n "asciidork-backend") (v "0.3.0") (d (list (d (n "asciidork-ast") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.2.0") (d #t) (k 0)))) (h "07r94gi2f64b39w4awx3akfv9vbjbhk7n72j6cak53x0s1c7za69")))

(define-public crate-asciidork-backend-0.3.3 (c (n "asciidork-backend") (v "0.3.3") (d (list (d (n "asciidork-ast") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.3.0") (d #t) (k 0)))) (h "0yfw5l9p2jnihcgdg00444fz4hh6rp49xz0cx796hqwxii3hn4bi")))

(define-public crate-asciidork-backend-0.3.4 (c (n "asciidork-backend") (v "0.3.4") (d (list (d (n "asciidork-ast") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.3.0") (d #t) (k 0)))) (h "03v2i317k61yymn7w5sf0ryjvg7lzrg0pnwi33q3xj41qxmi3njr")))

(define-public crate-asciidork-backend-0.4.0 (c (n "asciidork-backend") (v "0.4.0") (d (list (d (n "asciidork-ast") (r "^0.4.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.3.0") (d #t) (k 0)))) (h "0xs052q730lxr0s5a4c7wrhrx4n298rwyq2ng9s0zyn07qbv0yp9")))

(define-public crate-asciidork-backend-0.5.0 (c (n "asciidork-backend") (v "0.5.0") (d (list (d (n "asciidork-ast") (r "^0.5.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.3.0") (d #t) (k 0)))) (h "0srp482scq46gg07d3pny75diq062a4n4ljnr6cdng6k4iybccq4")))

