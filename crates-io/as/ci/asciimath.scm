(define-module (crates-io as ci asciimath) #:use-module (crates-io))

(define-public crate-asciimath-0.1.0 (c (n "asciimath") (v "0.1.0") (h "0n5qva066mrqyabagl9yyrbwdhxqx2bga53ik8sc19j6kpwwzixv")))

(define-public crate-asciimath-0.2.0 (c (n "asciimath") (v "0.2.0") (h "1ac6gjdbh0q9vm24q2lq5n89arai5cwivkjp76xr4kcbrg0z5cm3")))

(define-public crate-asciimath-0.3.0 (c (n "asciimath") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "03zc5w5d85lqvfs4skdv0mivisq3139vmhmzh8k25sm2myabvn2c")))

(define-public crate-asciimath-0.3.1 (c (n "asciimath") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "1fczcai7wbak6m026l3accxg8ni1i4zn0hcz3rxk5kpz8jg6s1j8")))

(define-public crate-asciimath-0.4.0 (c (n "asciimath") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "1q1lqs9w08cxhplbbq4ayhwfvdz2sxza4rhpdgymivm05wypkmp6")))

(define-public crate-asciimath-0.5.0 (c (n "asciimath") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "09k7iav7gwx7ri8b28ii24bmilpjp11bhmx338c8kmm20ih7b45f")))

(define-public crate-asciimath-0.5.1 (c (n "asciimath") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0p0f5wgbfmas2xrn861n8q61arl5hxyd56g47smy1jyl62l7z3b3")))

(define-public crate-asciimath-0.6.0 (c (n "asciimath") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0w1lqzfymy1cr9f6f79akxvw7kfxdy9qmrd57y04gfjf5p4kvvxy")))

(define-public crate-asciimath-0.7.0 (c (n "asciimath") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "1kivbwzsv6ixfpb39z66fyj6m9czkbfzrdlsk0gq46k09wp4vx0j")))

(define-public crate-asciimath-0.7.1 (c (n "asciimath") (v "0.7.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "04jqh2sfcpfxvprj8qzfyl9cqjiadwdas11qmjhjpaxrjkfb45vw")))

(define-public crate-asciimath-0.7.2 (c (n "asciimath") (v "0.7.2") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0i6f3bvhk0mclz651jgdbjz8h517fl2pf78radfg3pls5rpqybdj")))

(define-public crate-asciimath-0.7.3 (c (n "asciimath") (v "0.7.3") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0wminds0qkx5k22cqbrpaz04nhmvsw1m3xf1zv6iqxcg4d64s5fg")))

(define-public crate-asciimath-0.8.0 (c (n "asciimath") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "118sg4f9aa5s9bmvph7khvlf1wsmfhaz1lk3vh1c9jj996v1rq4f")))

(define-public crate-asciimath-0.8.1 (c (n "asciimath") (v "0.8.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "1g87nyyy9sizja5rnw2bj13wramr5yf9np8rly7k1grl5b7jk5s4")))

(define-public crate-asciimath-0.8.2 (c (n "asciimath") (v "0.8.2") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0li28jkl3ir2v16mvyc28lnh9lfyqwwjyycya3zrcsrj3xyp6b1n")))

(define-public crate-asciimath-0.8.3 (c (n "asciimath") (v "0.8.3") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0wfds5m6z96ya0jylpi6im0sac1b0br3gkk3kvhsr6khc0ivrzwz")))

(define-public crate-asciimath-0.8.4 (c (n "asciimath") (v "0.8.4") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "06pqflcvajikiyrga4q0vw5fbnrdk1wn5nmvaly88xy67lyga5sf")))

(define-public crate-asciimath-0.8.5 (c (n "asciimath") (v "0.8.5") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "1d2rj52a2msfrbn9m74y5lrg9mbl24pv6smw5cqi8jqknlz77d8p")))

(define-public crate-asciimath-0.8.6 (c (n "asciimath") (v "0.8.6") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "12ppik23rqhc45hq0xn33g0v55d6dkw3ajv3zrwzaa56nbf6kj89")))

(define-public crate-asciimath-0.8.7 (c (n "asciimath") (v "0.8.7") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "02ypdghab4sdgfymzrk65kmyd99b2v78ar0cqmjqpih9nyf14w0k")))

(define-public crate-asciimath-0.8.8 (c (n "asciimath") (v "0.8.8") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "033c5x727w2m68gh8ka49w27n5bykqsz7v4rnqyz75x30qgqlz1l")))

