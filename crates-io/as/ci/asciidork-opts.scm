(define-module (crates-io as ci asciidork-opts) #:use-module (crates-io))

(define-public crate-asciidork-opts-0.1.0 (c (n "asciidork-opts") (v "0.1.0") (h "12nj5nwrnznm8fbn735l90w1bmvvijnryk7w9dykji7vscx8jq55")))

(define-public crate-asciidork-opts-0.2.0 (c (n "asciidork-opts") (v "0.2.0") (h "10r3fxhk06wn3s8qz226qx98gqjbnc83p8djmy68bhpxfymqw48h")))

(define-public crate-asciidork-opts-0.3.3 (c (n "asciidork-opts") (v "0.3.3") (h "1n1js07ckxykddhapjc83cvyxjwydfkh0fsp93iaxd6agxagkx24")))

