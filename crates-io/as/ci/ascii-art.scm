(define-module (crates-io as ci ascii-art) #:use-module (crates-io))

(define-public crate-ascii-art-0.1.0 (c (n "ascii-art") (v "0.1.0") (d (list (d (n "fontdue") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "10d56iv7yh5fbqrhybbilh4zjigsanr4f4x8w0vfql5g50aqz569") (f (quote (("default"))))))

