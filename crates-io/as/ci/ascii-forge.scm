(define-module (crates-io as ci ascii-forge) #:use-module (crates-io))

(define-public crate-ascii-forge-0.0.0 (c (n "ascii-forge") (v "0.0.0") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1c3babwhmawfgrasgn4h84s4lsj5jvbvzcam0z9w0ppmba7b1vz5")))

(define-public crate-ascii-forge-0.1.0 (c (n "ascii-forge") (v "0.1.0") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0cmr7x8nxhpcl85wh0zvi7fdp1bf36f8sbjgpw46sld5vf8p4rmw")))

(define-public crate-ascii-forge-0.1.1 (c (n "ascii-forge") (v "0.1.1") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0sac7g14f4w8bhmy9i9slqv3iw5hm3zhk57nmi1cjgh9wwik07f2")))

(define-public crate-ascii-forge-0.1.2 (c (n "ascii-forge") (v "0.1.2") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0k1rm1s3m2ads25qcq9c92laqvbqb0cqg4zmnlv5w8hpbalipf36")))

(define-public crate-ascii-forge-0.1.3 (c (n "ascii-forge") (v "0.1.3") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1203424b6vmsrg9bkjqinc1n1vhiyxlwgsl6fnfd0wzz81sj238y")))

(define-public crate-ascii-forge-0.1.4 (c (n "ascii-forge") (v "0.1.4") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "04jaz1k6kpzdnhjjbs6w52irm95lp64c5pxglpcww6dwvxz5yp9q")))

(define-public crate-ascii-forge-0.1.5 (c (n "ascii-forge") (v "0.1.5") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "02qjafv0cg1j276pq3hjn6i0xp6zbyxx114jmyrmqpf56arskrm7")))

(define-public crate-ascii-forge-0.1.6 (c (n "ascii-forge") (v "0.1.6") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0za3ngkwwaq85s7jnac6b3y77c4xjhc2c9gvhk34zs62116d06m7")))

(define-public crate-ascii-forge-0.1.7 (c (n "ascii-forge") (v "0.1.7") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0hhqd1zlb1mmg2qi3zs343i8cfmmv0z7j9kdd69yildxafka36hl")))

(define-public crate-ascii-forge-0.1.71 (c (n "ascii-forge") (v "0.1.71") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0ip3yl61yp92ymflyfdhicrn210rfkq4yyvi50crzmf4sf8wjrh3")))

(define-public crate-ascii-forge-0.2.0 (c (n "ascii-forge") (v "0.2.0") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0ygaa1xhs8mryl7brc0zhifa73a9j20gqdyyh0x4mhmcjrvhfn7d") (f (quote (("keyboard")))) (y #t)))

(define-public crate-ascii-forge-0.2.1 (c (n "ascii-forge") (v "0.2.1") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1133nzw0pzysvjfn8ss5033mv2x12jfv9ls89wz8qvxp3gynpzcr") (f (quote (("keyboard")))) (y #t)))

(define-public crate-ascii-forge-0.2.2 (c (n "ascii-forge") (v "0.2.2") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0piq38z5dj9cmhgzmz19jjkwjgc8qzyvafgawg3ybrsndmbdvd99") (f (quote (("keyboard")))) (y #t)))

(define-public crate-ascii-forge-0.2.3 (c (n "ascii-forge") (v "0.2.3") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "00f84mgig1xy9h9gjij4imr6n03rxjpqjdasbgxyl3q7aqb2fbnh") (f (quote (("keyboard"))))))

(define-public crate-ascii-forge-0.2.4 (c (n "ascii-forge") (v "0.2.4") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0lpqd1xkm327sm6wn3jdf3ka0ia9cwkk79i22x8s99y8dzj58mqj") (f (quote (("keyboard"))))))

(define-public crate-ascii-forge-0.2.5 (c (n "ascii-forge") (v "0.2.5") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1fdj3pgr78vc8114m3025r34zg27x76d9jdli4s02x3cmrfyvznj") (f (quote (("keyboard"))))))

(define-public crate-ascii-forge-0.2.6 (c (n "ascii-forge") (v "0.2.6") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)))) (h "1yz40qy4g5whwi5plb2f1gkhhaffihgaldqz9f1l328bsmys7w77") (f (quote (("keyboard"))))))

(define-public crate-ascii-forge-0.2.7 (c (n "ascii-forge") (v "0.2.7") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)))) (h "0fhqlq1dfrxl86mjjq2i7kb3p1h5l8sgl2s8wq17y2d3cv8y8jzz") (f (quote (("keyboard"))))))

(define-public crate-ascii-forge-0.2.8 (c (n "ascii-forge") (v "0.2.8") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)))) (h "10h3vr0r5j2c68rikvkckzc1dlf5q0w73cgamddxdhsmghp495w9")))

(define-public crate-ascii-forge-0.2.9 (c (n "ascii-forge") (v "0.2.9") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)))) (h "0mjrzc2n2sl188l1qfzzc4ndfqg3m12xaynjsz93m97hqymm1ckx")))

(define-public crate-ascii-forge-0.2.10 (c (n "ascii-forge") (v "0.2.10") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)))) (h "03gd7d79z4jx0jmh72i1fydzdannykhk0b7dby2g2rdcf0w5cgng")))

(define-public crate-ascii-forge-0.2.11 (c (n "ascii-forge") (v "0.2.11") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)))) (h "0r3cw2lblpi3kr997q55dpaqxbry2j2qjdimrpb07lqcz69hc8q0")))

(define-public crate-ascii-forge-0.2.12 (c (n "ascii-forge") (v "0.2.12") (d (list (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 2)))) (h "1pa3jqlfxqvdml6jyvwim9q6gqprkv4sww2k0sxryis0afkgfg99")))

