(define-module (crates-io as ci asciiz) #:use-module (crates-io))

(define-public crate-asciiz-0.1.0 (c (n "asciiz") (v "0.1.0") (h "1ii3chh2zspyycxqnl6gzfzgc667kr6hvlqkyb0gxv37v2544gql") (y #t)))

(define-public crate-asciiz-0.1.1 (c (n "asciiz") (v "0.1.1") (h "1j1bivbfn2q43ph8lsnn43sld426ck1bdj7c5jp0aj7kyadnm5xa") (y #t)))

(define-public crate-asciiz-0.1.2 (c (n "asciiz") (v "0.1.2") (h "0k2ymk9clwzai07c903yj964r5pbqx5d3m0kg04gbarfvb7lhyd0") (y #t)))

(define-public crate-asciiz-0.1.3 (c (n "asciiz") (v "0.1.3") (h "12018ylraqfvrlgyvv3ngvywjh6xsbgag6d1nm20z69hm0mj890v") (y #t)))

(define-public crate-asciiz-0.1.4 (c (n "asciiz") (v "0.1.4") (h "0xv7qj08kkcqbcvrvazlkd0xa260bzfk3d7za14sf10d0jq21z1g")))

