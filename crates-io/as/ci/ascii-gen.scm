(define-module (crates-io as ci ascii-gen) #:use-module (crates-io))

(define-public crate-ascii-gen-1.0.0 (c (n "ascii-gen") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "0n5lq98348j02447g37d4gz3svz10v4g2a4v00vb51r246r65b1s")))

(define-public crate-ascii-gen-1.0.1 (c (n "ascii-gen") (v "1.0.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "1rp601lv375v7p15ykqcays3gcfix8y1b9r55jckwlmv6q0jhf2j")))

(define-public crate-ascii-gen-1.1.0 (c (n "ascii-gen") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "0qjnnffrrnxfvzjd7z32f2nmw42yqgigwxsqm8js0rvj9fi816ni")))

