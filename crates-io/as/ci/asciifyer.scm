(define-module (crates-io as ci asciifyer) #:use-module (crates-io))

(define-public crate-asciifyer-0.1.1 (c (n "asciifyer") (v "0.1.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0ijjbgpr5lx0d2m47q084s2lr7jqms69s2hbqvvq1hklykkizq27") (f (quote (("fetch" "reqwest" "tempfile" "tokio"))))))

