(define-module (crates-io as ci asciidork-ast) #:use-module (crates-io))

(define-public crate-asciidork-ast-0.1.0 (c (n "asciidork-ast") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)))) (h "12ycnlrhry644xgax111crrss1piywjgi2q2vi82c16qlwn479qp")))

(define-public crate-asciidork-ast-0.2.0 (c (n "asciidork-ast") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)))) (h "0b5vj1cjfnszghm96py86b46szxap5rypjl2hi4p0r5lx0gqgpdq")))

(define-public crate-asciidork-ast-0.3.0 (c (n "asciidork-ast") (v "0.3.0") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)))) (h "0ik5frd5xcma36q5a10kk0vn6rxbpiffd24llkhbdkbgs2qssgn3")))

(define-public crate-asciidork-ast-0.3.1 (c (n "asciidork-ast") (v "0.3.1") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)))) (h "1sqnmgrvb7rmdq51avdfcjipd6qx0ivf1x5qjzkvxd0m3fy8az0a")))

(define-public crate-asciidork-ast-0.3.3 (c (n "asciidork-ast") (v "0.3.3") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "jsonxf") (r "^1.1.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)))) (h "117i20di2dnpjjmhcr5q0i0n97i1pcvmw0wcky60pk9irl3h1qyc")))

(define-public crate-asciidork-ast-0.3.4 (c (n "asciidork-ast") (v "0.3.4") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "jsonxf") (r "^1.1.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)))) (h "1vy5rlv2iz63cjay4c003rp5abcf2pd2dx7ap16sdm0rch8a6y4y")))

(define-public crate-asciidork-ast-0.4.0 (c (n "asciidork-ast") (v "0.4.0") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "jsonxf") (r "^1.1.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)))) (h "1y65gs58iqyy54937xbf73k60sfc8zx0pjryk1mlii0y4xmr2532")))

(define-public crate-asciidork-ast-0.5.0 (c (n "asciidork-ast") (v "0.5.0") (d (list (d (n "bumpalo") (r "^3.15.4") (f (quote ("collections"))) (d #t) (k 0)) (d (n "jsonxf") (r "^1.1.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)))) (h "1iqb3azlwb427pvnwqfcbbq461lfhds193fyycilyxxz7wliaz9a")))

