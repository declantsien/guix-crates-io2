(define-module (crates-io as ci asciidork-eval) #:use-module (crates-io))

(define-public crate-asciidork-eval-0.1.0 (c (n "asciidork-eval") (v "0.1.0") (d (list (d (n "asciidork-ast") (r "^0.1.0") (d #t) (k 0)) (d (n "asciidork-backend") (r "^0.1.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.1.0") (d #t) (k 0)))) (h "093xzcij4f25sy7bh91n3hdhw76mdm4zyadz26zgxyp0glb041lw")))

(define-public crate-asciidork-eval-0.2.0 (c (n "asciidork-eval") (v "0.2.0") (d (list (d (n "asciidork-ast") (r "^0.2.0") (d #t) (k 0)) (d (n "asciidork-backend") (r "^0.2.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.2.0") (d #t) (k 0)))) (h "0f5ha8x41msbhbkz37amv1sxayf8sl4841d4q7zfyf7faz0hnxw4")))

(define-public crate-asciidork-eval-0.3.0 (c (n "asciidork-eval") (v "0.3.0") (d (list (d (n "asciidork-ast") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.2.0") (d #t) (k 0)))) (h "1halicdkhxrb8vcp6d02kp63jh1y0i379s8ng3ggvbsndbbxjgrh")))

(define-public crate-asciidork-eval-0.3.1 (c (n "asciidork-eval") (v "0.3.1") (d (list (d (n "asciidork-ast") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.2.0") (d #t) (k 0)))) (h "14fdga9rm0zwf95zxfbih0npixnrrlfr66l7814rhgj8vvdw23v9")))

(define-public crate-asciidork-eval-0.3.3 (c (n "asciidork-eval") (v "0.3.3") (d (list (d (n "asciidork-ast") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.3.0") (d #t) (k 0)))) (h "158v2gnar24iavb5mfvqamfhzp73lnhwvqbhnpbyv2wgpjamyslw")))

(define-public crate-asciidork-eval-0.3.4 (c (n "asciidork-eval") (v "0.3.4") (d (list (d (n "asciidork-ast") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.3.0") (d #t) (k 0)))) (h "0z7x7vs30j80wjlz221zshy3vy3jdp30lk2pxi4zr61nz7brlrxv")))

(define-public crate-asciidork-eval-0.4.0 (c (n "asciidork-eval") (v "0.4.0") (d (list (d (n "asciidork-ast") (r "^0.4.0") (d #t) (k 0)) (d (n "asciidork-backend") (r "^0.4.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.3.0") (d #t) (k 0)))) (h "142y1zcl82dp9935dvbzancl08gkaf8kpa1k406il4yz2manmvpn")))

(define-public crate-asciidork-eval-0.5.0 (c (n "asciidork-eval") (v "0.5.0") (d (list (d (n "asciidork-ast") (r "^0.5.0") (d #t) (k 0)) (d (n "asciidork-backend") (r "^0.5.0") (d #t) (k 0)) (d (n "asciidork-opts") (r "^0.3.0") (d #t) (k 0)))) (h "1g565ddf1liqc1j96iky61lpaq7gbs2mac2wgdqgis7nnk3pb5bp")))

