(define-module (crates-io as ci asciifier) #:use-module (crates-io))

(define-public crate-asciifier-0.0.1 (c (n "asciifier") (v "0.0.1") (h "19cj73n87gjyxkw351649y3g1yb0wy54bcipysld6wijz9lfh23j")))

(define-public crate-asciifier-0.0.2 (c (n "asciifier") (v "0.0.2") (h "1y47qz3dac8116i8g5r5ifl2bblc9hmiwfwrhm69nqah9615dnd2")))

