(define-module (crates-io as ci asciibar) #:use-module (crates-io))

(define-public crate-asciibar-0.1.0 (c (n "asciibar") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("std" "help" "cargo" "derive" "error-context"))) (k 0)) (d (n "snapbox") (r "^0.4.4") (d #t) (k 2)))) (h "15n6m4jdj080divvpvzl3nf09cx38gfzjs768zn7s64asgjyg1nq")))

(define-public crate-asciibar-0.1.1 (c (n "asciibar") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("std" "help" "cargo" "derive" "error-context"))) (k 0)) (d (n "snapbox") (r "^0.4.4") (d #t) (k 2)))) (h "16mdlmfajrn8m48w1qh6cdvcfzbqyjm529ggzk5r3q13d6vj179w")))

(define-public crate-asciibar-0.1.2 (c (n "asciibar") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("std" "help" "cargo" "derive" "error-context"))) (k 0)) (d (n "snapbox") (r "^0.4.14") (d #t) (k 2)))) (h "1pzwh1rbbl3w9gssjf41x2vxfzq7zmrwayjz0z19414l33v4aj1a")))

(define-public crate-asciibar-0.1.3 (c (n "asciibar") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("std" "help" "cargo" "derive" "error-context" "string" "color" "usage" "wrap_help"))) (k 0)) (d (n "snapbox") (r "^0.4.14") (d #t) (k 2)))) (h "0dcyhz79006hibpzzaqzapk2mpl4mgafwd6iv30cxyzag5ai5q1b")))

