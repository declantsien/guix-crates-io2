(define-module (crates-io as ci ascii_plist_derive) #:use-module (crates-io))

(define-public crate-ascii_plist_derive-0.0.1 (c (n "ascii_plist_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0469l0rdx5z17jy6vc5kcz68b3n5780211gmyqbaks8r03lm7c6d")))

