(define-module (crates-io as ci ascii_table) #:use-module (crates-io))

(define-public crate-ascii_table-0.1.0 (c (n "ascii_table") (v "0.1.0") (h "188sq4dfisybx2adfnj4gfk00710f5q738v5ggxjywfqhj9z0x2z")))

(define-public crate-ascii_table-0.1.1 (c (n "ascii_table") (v "0.1.1") (h "1q6b67wlvzprxc746xlimgccrz4v403z60lbk9wbjkvzbdcwjm9z")))

(define-public crate-ascii_table-1.0.0 (c (n "ascii_table") (v "1.0.0") (h "0685x8n13qvs42mnjfbqmyqqmn2r07cc8sghfhjdpsyr34585hpb")))

(define-public crate-ascii_table-2.0.0 (c (n "ascii_table") (v "2.0.0") (h "05i7nm4lqqrm6a10aaz0qb84ini6r3qk18p33g0valwf96zvb8fb")))

(define-public crate-ascii_table-2.1.0 (c (n "ascii_table") (v "2.1.0") (h "08sbrrqnf4a2mxb7aycaxi239axrialxz4ixnw60cvg7nagmmyxy")))

(define-public crate-ascii_table-3.0.0 (c (n "ascii_table") (v "3.0.0") (h "1dd94krav73p4yncz00hhbrpwmvpxxgky9d5g3l7f1k77jbc7jyb")))

(define-public crate-ascii_table-3.0.1 (c (n "ascii_table") (v "3.0.1") (d (list (d (n "colorful") (r "^0.2") (d #t) (k 2)))) (h "09q8idbdy0ri4zr7bgk2nn0x6522privr6i1yc4zl3ssk9ya0c37")))

(define-public crate-ascii_table-3.0.2 (c (n "ascii_table") (v "3.0.2") (d (list (d (n "colorful") (r "^0.2") (d #t) (k 2)))) (h "0ky22295lwjlr7byvy2hrpy19gj6fd53lq0bj4drhlql7f7sbw2i")))

(define-public crate-ascii_table-4.0.0 (c (n "ascii_table") (v "4.0.0") (d (list (d (n "colorful") (r "^0.2") (d #t) (k 2)))) (h "0psc21pkr6l6vlxgr0szn6apz8np1p2xkigp2i2b2x9v9sg5bkdn")))

(define-public crate-ascii_table-4.0.1 (c (n "ascii_table") (v "4.0.1") (d (list (d (n "colorful") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (o #t) (d #t) (k 0)))) (h "097kij6a1dpij33149grsqj2mxv4j0h5ilf79nfqzj9b0a7b5gzs") (f (quote (("wide_characters" "unicode-width") ("color_codes" "lazy_static" "regex"))))))

(define-public crate-ascii_table-4.0.2 (c (n "ascii_table") (v "4.0.2") (d (list (d (n "colorful") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1q9alygq45w17g7wa5vwg86gvkgqqvvgdhhdp3bn64j9c7jlq1bm") (f (quote (("wide_characters" "unicode-width") ("color_codes" "lazy_static" "regex") ("auto_table_width" "termion"))))))

(define-public crate-ascii_table-4.0.3 (c (n "ascii_table") (v "4.0.3") (d (list (d (n "colorful") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "termion") (r "^2") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0m8wcsi14my20xrfqdhkgq6ljfm21gphg373fa3pdrg0ksdywarw") (f (quote (("wide_characters" "unicode-width") ("color_codes" "lazy_static" "regex") ("auto_table_width" "termion"))))))

