(define-module (crates-io as ci ascii_renderer) #:use-module (crates-io))

(define-public crate-ascii_renderer-1.0.0 (c (n "ascii_renderer") (v "1.0.0") (d (list (d (n "clear_screen") (r "^0.1.0") (d #t) (k 0)) (d (n "obj") (r "^0.10.2") (d #t) (k 0)))) (h "10cn7h86vl57j7g8p2202ank5qyr2261grqa14wyvli26h90pkb6")))

(define-public crate-ascii_renderer-1.0.1 (c (n "ascii_renderer") (v "1.0.1") (d (list (d (n "clear_screen") (r "^0.1.0") (d #t) (k 0)) (d (n "obj") (r "^0.10.2") (d #t) (k 0)))) (h "03ljwvc5gr6bfry4cb94g4jbry4rrjcs3kgwkm0q8xzn4bp5yni3")))

(define-public crate-ascii_renderer-1.1.0 (c (n "ascii_renderer") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "obj") (r "^0.10.2") (d #t) (k 0)))) (h "1fqw6l3fw5djd76n69ixf06v5lqc0k86gz41rjhi9zicky6bkicw")))

(define-public crate-ascii_renderer-1.1.1 (c (n "ascii_renderer") (v "1.1.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "obj") (r "^0.10.2") (d #t) (k 0)))) (h "0158skrppnb89v3x8vryd4dzv3icm6vd2d8xhkdjmnr7ljd4fg08")))

