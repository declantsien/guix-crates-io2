(define-module (crates-io as ci ascii-cli) #:use-module (crates-io))

(define-public crate-ascii-cli-1.1.0 (c (n "ascii-cli") (v "1.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0b186crjvykidikgk91fw12ycby6l9zapq2c46pk38pp43nac204")))

