(define-module (crates-io as ci ascii-alphabetic-char) #:use-module (crates-io))

(define-public crate-ascii-alphabetic-char-0.1.0 (c (n "ascii-alphabetic-char") (v "0.1.0") (h "1k10gbglgmjz73g5c5r24mhswb1ianz6idzbrlzc3fi64662y00s")))

(define-public crate-ascii-alphabetic-char-0.1.1 (c (n "ascii-alphabetic-char") (v "0.1.1") (h "02cx9k28493fwh3sngsr1dzja66z15nxsbhxdhygk5324qxsvww7")))

