(define-module (crates-io as ci asciimath-rs) #:use-module (crates-io))

(define-public crate-asciimath-rs-0.1.0 (c (n "asciimath-rs") (v "0.1.0") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0n886s1akkvzi15jhd7kd61jagqary8vn1h1ffsj3gs0y65kp0i8")))

(define-public crate-asciimath-rs-0.2.0 (c (n "asciimath-rs") (v "0.2.0") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "11xhf6n3fdb7wqm96hszblibfg835gh65hv25al1nhw5b2r4jygd")))

(define-public crate-asciimath-rs-0.3.0 (c (n "asciimath-rs") (v "0.3.0") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "17b3la6dgsmwlvazga8b4n4yvgxgrqkjhvj006zd81yzcp8ydmng")))

(define-public crate-asciimath-rs-0.4.0 (c (n "asciimath-rs") (v "0.4.0") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1v686510lc9i3w3w0yqdp5my7xslbd21413d55mbpmapf82niqk0")))

(define-public crate-asciimath-rs-0.4.1 (c (n "asciimath-rs") (v "0.4.1") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "04826rdxmvg1hn7mwj06gcps7v3fcmvxd8v5jp1syb065msz1k9f")))

(define-public crate-asciimath-rs-0.4.2 (c (n "asciimath-rs") (v "0.4.2") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1bv7b5yaaxdslzs302950w26zshjlpfa4zfp9dxmd9rb8qqidqpa")))

(define-public crate-asciimath-rs-0.4.3 (c (n "asciimath-rs") (v "0.4.3") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0rqjjkq5ck10i8xd4a1xc6p0dd61yifmry8yky92iaps60bhqzk1")))

(define-public crate-asciimath-rs-0.4.4 (c (n "asciimath-rs") (v "0.4.4") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0s81dkv89d6zp8an4az71s8a8drp3s8pki4kl18d5kyw3ncnm4nv")))

(define-public crate-asciimath-rs-0.4.5 (c (n "asciimath-rs") (v "0.4.5") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0p2fzj4hw36qx46fslpv95pxwqiid8xq0529fcpysdm9zyvx01jc")))

(define-public crate-asciimath-rs-0.4.6 (c (n "asciimath-rs") (v "0.4.6") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0ya059fq7n0pq1779wljk97f0lbsfvg6krlr7dxal8bgbvjsgjz9")))

(define-public crate-asciimath-rs-0.4.7 (c (n "asciimath-rs") (v "0.4.7") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "17zqjm421qnxc75r89z86xrs8yagjlxjq7vhac0k7xyhchdz5h67")))

(define-public crate-asciimath-rs-0.4.8 (c (n "asciimath-rs") (v "0.4.8") (d (list (d (n "charred") (r "^0.3.0") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1zfhj8x61dpw18x0kc6i99nhf55knz1kp4gi4wfi39c3vlbbmbk4")))

(define-public crate-asciimath-rs-0.5.0 (c (n "asciimath-rs") (v "0.5.0") (d (list (d (n "charred") (r "^0.3.1") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0aqgaa9dhqjwbrqg56xrclbadxbq5w3fp66xjj23ihapblp779a7")))

(define-public crate-asciimath-rs-0.5.1 (c (n "asciimath-rs") (v "0.5.1") (d (list (d (n "charred") (r "^0.3.1") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "105ivb9n1zqblbnbimx9xayci6ljb7627xd0jaqyynfzfdhn6vsg")))

(define-public crate-asciimath-rs-0.5.2 (c (n "asciimath-rs") (v "0.5.2") (d (list (d (n "charred") (r "^0.3.1") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "10li10fvrw8x6vy8ira5v1d67rz44sb86acbsbya2sg844f5gy2d")))

(define-public crate-asciimath-rs-0.5.4 (c (n "asciimath-rs") (v "0.5.4") (d (list (d (n "charred") (r "^0.3.1") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1l3shiw323i9ahpjwx4skany63pzsbwb3dirklz6kiz8g73np3na")))

(define-public crate-asciimath-rs-0.5.5 (c (n "asciimath-rs") (v "0.5.5") (d (list (d (n "charred") (r "^0.3.1") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1fzakyw43vv6vc5fn9lv2j2hkjz0ixhqwd3cb456yf22zi26fi0p")))

(define-public crate-asciimath-rs-0.5.6 (c (n "asciimath-rs") (v "0.5.6") (d (list (d (n "charred") (r "^0.3.1") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0h9s0h08wljpvd6dxb73am03g4440kca82aspvind7hfc16si8bi")))

(define-public crate-asciimath-rs-0.5.7 (c (n "asciimath-rs") (v "0.5.7") (d (list (d (n "charred") (r "^0.3.3") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1lrgrhchzlshpi4i7nfp8bdqx6qzi4s1jqsx4z1hjaw5455hng83")))

(define-public crate-asciimath-rs-0.5.8 (c (n "asciimath-rs") (v "0.5.8") (d (list (d (n "charred") (r "^0.3.3") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0fmb26rnjhh0lxgfxjr8awb16znksjz2c62r9mnsgi51wc9khzzd")))

(define-public crate-asciimath-rs-0.6.0 (c (n "asciimath-rs") (v "0.6.0") (d (list (d (n "charred") (r "^0.3.3") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "1vzdny7s7v7xma3fsq28j04wjjmzld58rz6sa2msn5h9b61vq68f")))

(define-public crate-asciimath-rs-0.6.1 (c (n "asciimath-rs") (v "0.6.1") (d (list (d (n "charred") (r "^0.3.3") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "15gp9x5yf4h2ci4hc2ph75rvqsj5wx43p8rpw62y6y6pihjqc2sq")))

(define-public crate-asciimath-rs-0.6.2 (c (n "asciimath-rs") (v "0.6.2") (d (list (d (n "charred") (r "^0.3.3") (d #t) (k 0)) (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0arrq51b3msn98jyrdsy9prgg3ljdndn5b9ja5hrlfh0qz3dc9vx")))

