(define-module (crates-io as ci asciimath-text-renderer) #:use-module (crates-io))

(define-public crate-asciimath-text-renderer-0.1.0 (c (n "asciimath-text-renderer") (v "0.1.0") (d (list (d (n "asciimath-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1pphjcmdsvzp0lk4a98b6ds79cyy8a77pn9rkva45jkzcpk75mqq")))

