(define-module (crates-io as ci ascii_tree) #:use-module (crates-io))

(define-public crate-ascii_tree-0.1.0 (c (n "ascii_tree") (v "0.1.0") (h "1p8v0xnnlrgf4gbfr3si260kz1cs08vf8fgfagqln6mzp7f3pbni")))

(define-public crate-ascii_tree-0.1.1 (c (n "ascii_tree") (v "0.1.1") (h "0hy6wgpigv77xdxc43k98zx7v5c57ibz258lmm4wcrd679dn6v6a")))

