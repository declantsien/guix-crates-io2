(define-module (crates-io as ci ascii) #:use-module (crates-io))

(define-public crate-ascii-0.0.1 (c (n "ascii") (v "0.0.1") (h "0h2rjs1bbkgpyyq6x3q7rfvnkv7s2mndkg3vfjxxcz45iqndl9nx")))

(define-public crate-ascii-0.1.0 (c (n "ascii") (v "0.1.0") (h "0yc36cqa8m4wf3ycw8bcifvliyry7522m07qvgqx2x0hr3vvqs7i")))

(define-public crate-ascii-0.2.0 (c (n "ascii") (v "0.2.0") (h "0xkqmarzwqlad1yxin9ad8x0dzdgxpi59p9r62bnlshc8zrbb1g3")))

(define-public crate-ascii-0.2.1 (c (n "ascii") (v "0.2.1") (h "03kqgsasgqbyy60bww9bwfkcyxa159kzi9b90y8hc9gwz0zj5m80")))

(define-public crate-ascii-0.2.2 (c (n "ascii") (v "0.2.2") (h "0dyfpaznwb1i390ca50h8bzz9hgjcr149amvn0cfp336gdldaq47")))

(define-public crate-ascii-0.2.3 (c (n "ascii") (v "0.2.3") (h "1rmql587kd201gp4j197snifwvs67amcfdswgq0dppmy84jvc2ci")))

(define-public crate-ascii-0.2.4 (c (n "ascii") (v "0.2.4") (h "0wqw87r6dzrm0ssyz0dymqpyndcjvf3zn0yqpqx6db4bjl51871w")))

(define-public crate-ascii-0.2.5 (c (n "ascii") (v "0.2.5") (h "071yx5c0zfhi1qzkfylr7vn9hr5y0q9sak685324sqy1d2j938kr")))

(define-public crate-ascii-0.3.0 (c (n "ascii") (v "0.3.0") (h "1hdcp3y0d4z4cc5120hfhvvf9ch15dwfk8lcdghdrxwilbwjfjw5")))

(define-public crate-ascii-0.4.0 (c (n "ascii") (v "0.4.0") (h "09icnlqhqda7ggkzhnr72nws5afkd6lhgby26ijmm6l483w2pigc")))

(define-public crate-ascii-0.4.1 (c (n "ascii") (v "0.4.1") (h "1nmbmljarvf8nb8z65lqdks564zi7zrqjly738fav91ba7fcspbi")))

(define-public crate-ascii-0.5.0 (c (n "ascii") (v "0.5.0") (h "14m06qhb7wyr1g67p8cwzq1yvj678k9xsmh83lfgzcf67c632yrm") (f (quote (("unstable"))))))

(define-public crate-ascii-0.5.1 (c (n "ascii") (v "0.5.1") (h "1v6qhi84lf1zg9hgdg6ji5qhfccfyavcmkrlmwxdashy596skw65") (f (quote (("unstable"))))))

(define-public crate-ascii-0.5.2 (c (n "ascii") (v "0.5.2") (h "0p16rngfgn7bz8hfd0s3lxc9q99y370pgb72l60ppgld747kch58") (f (quote (("unstable"))))))

(define-public crate-ascii-0.5.3 (c (n "ascii") (v "0.5.3") (h "0krrf0fs3ii8k584jnz80zq7wc6y3lgp57imwnqjdykq9v8ia2rx") (f (quote (("unstable"))))))

(define-public crate-ascii-0.5.4 (c (n "ascii") (v "0.5.4") (h "0gnbp08blimp9ww484wqiwmfilan482kccy10l7kklx57icj2qyg") (f (quote (("unstable"))))))

(define-public crate-ascii-0.6.0 (c (n "ascii") (v "0.6.0") (h "0rfyflkw6crklb7g3m6pfifgk5w6dmn2j9xnxllrjvqmymd0c8l3") (f (quote (("unstable"))))))

(define-public crate-ascii-0.7.0 (c (n "ascii") (v "0.7.0") (h "182wrsz5z0var5naaviz6icnbfw6hia6xqz77vi1jxz7jx2glwyz") (f (quote (("no_std")))) (y #t)))

(define-public crate-ascii-0.7.1 (c (n "ascii") (v "0.7.1") (h "0l5d2cwfx4cm7fxhxmaqkhm0bbifpcwhmkv88hfrq64ck58xgrrs") (f (quote (("no_std"))))))

(define-public crate-ascii-0.8.0 (c (n "ascii") (v "0.8.0") (h "1z7qhlm791w71lpbma83pxlz9vr26pyr8ydl17mzi9d46z8ap9sb") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ascii-0.8.1 (c (n "ascii") (v "0.8.1") (h "1f09q1a4q6vdng8gh02g3z3is9j4s8addzlvs0cb56i4p52jwank") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ascii-0.8.2 (c (n "ascii") (v "0.8.2") (d (list (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "1rjcrn8yjdq4nba8nnlnrg61gx96mg5zgqr13l6s8ysanby29920") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ascii-0.8.3 (c (n "ascii") (v "0.8.3") (d (list (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0x1jygzp66av4spjrqxsl7arw3skcni43hgj0mjjv5mkblg99zjl") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ascii-0.8.4 (c (n "ascii") (v "0.8.4") (d (list (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "1rd6xba5cis9hy3wznpb9afsh5mj3rzzs6ykrsi0ij5jqhg3gnjg") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ascii-0.8.5 (c (n "ascii") (v "0.8.5") (d (list (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "01h3gsns4afri3783rpcnwafi5knx42mm9krhsi85jshd6xbh5n1") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ascii-0.8.6 (c (n "ascii") (v "0.8.6") (d (list (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "093dygd3w41njg29zdbvz3wf8vild3y7qj5b5hdr65sy9iqhfx6n") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ascii-0.8.7 (c (n "ascii") (v "0.8.7") (d (list (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "051gh2bgjq90s0f0i0hd9p4z4fpg5k82b570d1223jj7rhd8kglp") (f (quote (("std") ("default" "std"))))))

(define-public crate-ascii-0.9.0 (c (n "ascii") (v "0.9.0") (d (list (d (n "quickcheck") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0rqqdybmfjmzpi51njzlkirwfcskfzsn43fp5chn16pg8wwr8g1b") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ascii-0.9.1 (c (n "ascii") (v "0.9.1") (d (list (d (n "quickcheck") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0dck6rsjnxlczyjnncn8hf16bxj42m1vi6s2n32c1jg2ijd9dz55") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ascii-0.9.2 (c (n "ascii") (v "0.9.2") (d (list (d (n "quickcheck") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.25") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1mn5az4hkxgjhwy157pr1nrfdb3qjpw8jw8v91m2i8wg59b21qwi") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-ascii-0.9.3 (c (n "ascii") (v "0.9.3") (d (list (d (n "quickcheck") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.25") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0km3zzkhrr22drf9p1zcblqirlxkdc7zra25acpi0h8qax5c1cga") (f (quote (("std") ("default" "std"))))))

(define-public crate-ascii-1.0.0 (c (n "ascii") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.25") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0281gc828q4j692gb66jfdr5k16gyszgqflylh0pp30rllv63xdv") (f (quote (("std") ("default" "std"))))))

(define-public crate-ascii-1.1.0 (c (n "ascii") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.25") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "05nyyp39x4wzc1959kv7ckwqpkdzjd9dw4slzyjh73qbhjcfqayr") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

