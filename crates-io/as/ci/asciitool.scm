(define-module (crates-io as ci asciitool) #:use-module (crates-io))

(define-public crate-asciitool-0.1.0 (c (n "asciitool") (v "0.1.0") (d (list (d (n "clt") (r "^0.0.6") (d #t) (k 0)) (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "man") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.12") (d #t) (k 0)) (d (n "roff") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mw7rmx9zlfgczflh4lw2xp5i2pwg3bryljphy2l5q9n5zm9jk8k")))

