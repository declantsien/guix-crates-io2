(define-module (crates-io as ci asciicast) #:use-module (crates-io))

(define-public crate-asciicast-0.1.0 (c (n "asciicast") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zx67a3179d94zcxw5mpyrxdjb419hibiv01mfiif5vbhjmffxyl") (f (quote (("default" "chrono_support") ("chrono_support" "chrono")))) (y #t)))

(define-public crate-asciicast-0.1.1 (c (n "asciicast") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rcgyqgd5d6awarwlz0kwvglky140p1h0935gqbgvb3rxgx840ph") (f (quote (("default" "chrono_support") ("chrono_support" "chrono"))))))

(define-public crate-asciicast-0.2.0 (c (n "asciicast") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fld8w43bf95q0slq32a4n965873xjav3cbj5fqll9w328cd5mm5") (f (quote (("default" "chrono_support") ("chrono_support" "chrono")))) (y #t)))

(define-public crate-asciicast-0.2.1 (c (n "asciicast") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yq74ad550z7ps34k84vn8igd07jl1jl76d40ww5pr6y1dn8s4r9") (f (quote (("default" "chrono_support") ("chrono_support" "chrono"))))))

(define-public crate-asciicast-0.2.2 (c (n "asciicast") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yzdk2ibcwi6mnp9vfx67dwc5xrr4qxdw89bqfg67d7s8jlfa8a5") (f (quote (("default" "chrono_support") ("chrono_support" "chrono"))))))

