(define-module (crates-io as ci ascii_help) #:use-module (crates-io))

(define-public crate-ascii_help-0.1.0 (c (n "ascii_help") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "0axsm32lixq6wjr9m0fj0nv3mjyi1a3mrkhgj72hc1bgi0wz9x4z")))

(define-public crate-ascii_help-0.2.0 (c (n "ascii_help") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dcbg4z3k6l2i7hjmg2jm96jyynz81pxgc6g3dhp6zaxds810ng4")))

