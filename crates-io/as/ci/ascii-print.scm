(define-module (crates-io as ci ascii-print) #:use-module (crates-io))

(define-public crate-ascii-print-0.1.0 (c (n "ascii-print") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0v3290644kvapn6491v5l7d7ar690ghn1k4l5s2q9y54llxrw0mp")))

