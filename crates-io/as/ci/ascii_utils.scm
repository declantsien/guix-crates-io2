(define-module (crates-io as ci ascii_utils) #:use-module (crates-io))

(define-public crate-ascii_utils-0.8.1 (c (n "ascii_utils") (v "0.8.1") (h "16hqnkh18gvpvd74pgdr256pka3p4n96r14qmdq9xyk1plzx7yvv") (y #t)))

(define-public crate-ascii_utils-0.8.2 (c (n "ascii_utils") (v "0.8.2") (h "1g1qxx4ynprln80m3px312jqm1kiwwyq9s1a51mnkxr8r6k13i2z")))

(define-public crate-ascii_utils-0.8.3 (c (n "ascii_utils") (v "0.8.3") (h "12mcl0297pi4ff0l0dy7yaas0z2wkpaqfimlaaj6g3k8n1y5bw1l")))

(define-public crate-ascii_utils-0.9.0 (c (n "ascii_utils") (v "0.9.0") (h "1im9aabbfx3viwrm8in6am4sv3vp7rxjywra6m6xlkmkz8w8k43j") (y #t)))

(define-public crate-ascii_utils-0.9.1 (c (n "ascii_utils") (v "0.9.1") (h "13da1n6r73bb6hz7flqbdkznvc8f8fzcavvgancvv8qafbkn65iw") (y #t)))

(define-public crate-ascii_utils-0.9.2 (c (n "ascii_utils") (v "0.9.2") (h "13i3ydh15w5llar0zaihbp65xs8b0pfy1kbi8jv4z5va0ix1c8lm") (y #t)))

(define-public crate-ascii_utils-0.9.3 (c (n "ascii_utils") (v "0.9.3") (h "0jpp550pwi38msflpy7lnqm2r153kn9k19bss6k9ak9yacq8z4vi")))

