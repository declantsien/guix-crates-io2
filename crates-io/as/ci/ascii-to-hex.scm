(define-module (crates-io as ci ascii-to-hex) #:use-module (crates-io))

(define-public crate-ascii-to-hex-0.1.0 (c (n "ascii-to-hex") (v "0.1.0") (h "1wl4sir4k00k3if2gm8jjnkgpvz1k2k075v0qak1pzm8kfn424vn")))

(define-public crate-ascii-to-hex-0.1.1 (c (n "ascii-to-hex") (v "0.1.1") (h "1yxmg1y996gmlhqiywblafw0yfydldkpbyjwykv0g3blnphv3hyw")))

