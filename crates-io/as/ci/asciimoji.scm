(define-module (crates-io as ci asciimoji) #:use-module (crates-io))

(define-public crate-asciimoji-1.0.0 (c (n "asciimoji") (v "1.0.0") (d (list (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "10xkb4pvibi5ljd7z8q6l5zx0l12c2frlybs25imp66vvakw2gmk")))

(define-public crate-asciimoji-1.0.1 (c (n "asciimoji") (v "1.0.1") (d (list (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0w27cav3yrxzhl2g806f0p8csl7x5bwkljaxgzh4pi6g34lmwgir")))

(define-public crate-asciimoji-1.0.2 (c (n "asciimoji") (v "1.0.2") (d (list (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0fildmq77hs3ndmcaq6cqz8pm2zv2mpzpc7n20d3rgawm43mksa7")))

(define-public crate-asciimoji-1.1.0 (c (n "asciimoji") (v "1.1.0") (d (list (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 0)))) (h "0x89ll5lb861adz1x10ar7l4anvlmwhpsnml48c648crxh925jad")))

(define-public crate-asciimoji-1.1.1 (c (n "asciimoji") (v "1.1.1") (d (list (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 0)))) (h "112pl3x4zhgqwbpiwb6vssxzm0kdh5wz14b14pzlyxbw5l8ib4jf")))

(define-public crate-asciimoji-1.2.0 (c (n "asciimoji") (v "1.2.0") (d (list (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 0)))) (h "1rnnzjj4zrcn4c6fzfvqkyivhajyr8svlg8bb3djjix7qmw6fy6x")))

(define-public crate-asciimoji-1.2.1 (c (n "asciimoji") (v "1.2.1") (d (list (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 0)))) (h "1gr2h7i662ayhr9l9jpq57jqcd6js5kql8zqd7hl40q73mspvrfc")))

(define-public crate-asciimoji-1.3.0 (c (n "asciimoji") (v "1.3.0") (d (list (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 0)))) (h "1ihrnx7ihrfbrwzbc7g9da99h6p0nxyvwcaw306mplc1i2fmr0ii")))

(define-public crate-asciimoji-1.4.0 (c (n "asciimoji") (v "1.4.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 0)))) (h "0z2r5qx7876d0gacjcndzk9xsz5imvqx9xvkxhi1cyjx3xq82m3r")))

(define-public crate-asciimoji-1.4.1 (c (n "asciimoji") (v "1.4.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tabular") (r "^0.1.4") (d #t) (k 0)))) (h "1kbm69h4vi3dwh815lix2vhgayjs68skzdlr83d08qvnz2gzjmww")))

