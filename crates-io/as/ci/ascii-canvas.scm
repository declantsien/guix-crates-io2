(define-module (crates-io as ci ascii-canvas) #:use-module (crates-io))

(define-public crate-ascii-canvas-0.0.1 (c (n "ascii-canvas") (v "0.0.1") (d (list (d (n "term") (r "^0.4.5") (d #t) (k 0)))) (h "0j2sdksz1sndgaqpr1jkgvzjzbkr756b4280ndr6gmlrgb6x3jxv")))

(define-public crate-ascii-canvas-1.0.0 (c (n "ascii-canvas") (v "1.0.0") (d (list (d (n "term") (r "^0.4.5") (d #t) (k 0)))) (h "1qipmwnmlyklnjxijkz2wg0bq72k2aii381k8ljiq6l20aadd1dk")))

(define-public crate-ascii-canvas-1.0.1 (c (n "ascii-canvas") (v "1.0.1") (d (list (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "03zkm6y8cwgxnzi3qimj09fas4fy85yhrvy1lhxk3hl8hqrc44s9") (y #t)))

(define-public crate-ascii-canvas-2.0.0 (c (n "ascii-canvas") (v "2.0.0") (d (list (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "0a9s8vrbc5jr6ry5ygjyfqmbs9gyya1v6dsxzsczpai8z4nvg3pz")))

(define-public crate-ascii-canvas-3.0.0 (c (n "ascii-canvas") (v "3.0.0") (d (list (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "term") (r "^0.7") (d #t) (k 0)))) (h "1in38ziqn4kh9sw89ys4naaqzvvjscfs0m4djqbfq7455v5fq948")))

