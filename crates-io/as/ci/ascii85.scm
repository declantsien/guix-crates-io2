(define-module (crates-io as ci ascii85) #:use-module (crates-io))

(define-public crate-ascii85-0.1.0 (c (n "ascii85") (v "0.1.0") (h "03skrr4sy1hyqpc56pgqr4ybsyjyq472rcrv5864gzdnls9h7ajx")))

(define-public crate-ascii85-0.1.1 (c (n "ascii85") (v "0.1.1") (h "1dlx9b69xzn5n90hc8mm7af49r8clxdfn0hn4l9nn76ybn8k08wb")))

(define-public crate-ascii85-0.2.0 (c (n "ascii85") (v "0.2.0") (h "0g3gm9v4igj3lwb8idw6n1yxrv2b8fwbc5s7adv83firz8fgi6kr")))

(define-public crate-ascii85-0.2.1 (c (n "ascii85") (v "0.2.1") (h "0f6rxd26bay3xgn2wah8120r8cp23a81984r7v60bjng1k2jqyqz")))

