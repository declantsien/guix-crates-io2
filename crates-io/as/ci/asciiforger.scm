(define-module (crates-io as ci asciiforger) #:use-module (crates-io))

(define-public crate-asciiforger-0.1.0 (c (n "asciiforger") (v "0.1.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "1qdy58v5javix076p2wvrmrp50i4nbghww2y0szaahhv4l6k97h5")))

(define-public crate-asciiforger-0.2.0 (c (n "asciiforger") (v "0.2.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "0zxd0mkd65v5qzgqbrr1j7vkzisdhlx6x02ah7dpcphnjrkjvqb9")))

(define-public crate-asciiforger-0.3.0 (c (n "asciiforger") (v "0.3.0") (d (list (d (n "ffmpeg-next") (r "^7.0.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "1a4hm3ff8fhmp71p71hmnjbqhhszg3lzy5s45hzy5zga7n1dyvi5")))

