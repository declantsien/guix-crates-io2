(define-module (crates-io as ci asciicanvas) #:use-module (crates-io))

(define-public crate-asciicanvas-0.1.0 (c (n "asciicanvas") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0y221zvz7dry4v2h1w9z1jy5s2dwm061i8flh333q08bmp3k2jmk")))

