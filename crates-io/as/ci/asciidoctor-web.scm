(define-module (crates-io as ci asciidoctor-web) #:use-module (crates-io))

(define-public crate-asciidoctor-web-0.1.0 (c (n "asciidoctor-web") (v "0.1.0") (d (list (d (n "serde-wasm-bindgen") (r "^0.4.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "yew") (r "^0.20") (o #t) (d #t) (k 0)))) (h "0fd7x0251s7kzb216y3v7a46h9di1slfcjzd80axyp1dr1dlm3g6")))

(define-public crate-asciidoctor-web-0.1.1 (c (n "asciidoctor-web") (v "0.1.1") (d (list (d (n "serde-wasm-bindgen") (r "^0.4.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "yew") (r "^0.20") (o #t) (d #t) (k 0)))) (h "0zbiq0a250z3kg7r1hblxrknmahrm0dvbgm0anvp3jw2pkbf2wg5")))

