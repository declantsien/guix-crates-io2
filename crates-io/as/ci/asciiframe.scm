(define-module (crates-io as ci asciiframe) #:use-module (crates-io))

(define-public crate-asciiframe-0.1.3 (c (n "asciiframe") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "opencv") (r "^0.62") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "173j9m0l2zp4lh1pzy54lkiyi1gzaqwbg217xrsxvgn3m3rjlbrc")))

(define-public crate-asciiframe-0.1.4 (c (n "asciiframe") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.6") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "opencv") (r "^0.62") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "1hsz3lvnj6kk6pilnvj84h2n1fi23x6abxj213vdn1gfq0q3h1bl")))

(define-public crate-asciiframe-1.2.0 (c (n "asciiframe") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "opencv") (r "^0.63") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "15kp4lj96xghjn39vwci22p4ixklzrwj9a7l4pd3wc46zrq10jcc")))

(define-public crate-asciiframe-2.0.0 (c (n "asciiframe") (v "2.0.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo"))) (o #t) (d #t) (k 0)) (d (n "opencv") (r "^0.88.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dagnaikn4c7z1n4wygwz9gyj3s81j6338c5f82r73v4nv97lgj5") (f (quote (("default" "application") ("application" "clap" "terminal_size"))))))

