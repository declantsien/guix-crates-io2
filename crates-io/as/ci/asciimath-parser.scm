(define-module (crates-io as ci asciimath-parser) #:use-module (crates-io))

(define-public crate-asciimath-parser-0.1.0 (c (n "asciimath-parser") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "fst") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "qp-trie") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "asciimath-rs") (r "^0.6.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "114fs1lan261qs622h4qysg8f218hm5p5jz4cgkmxryl7j57kcnw") (f (quote (("default" "qp-trie"))))))

(define-public crate-asciimath-parser-0.1.1 (c (n "asciimath-parser") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "fst") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "qp-trie") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "asciimath-rs") (r "^0.6.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "16p5jj81xx8z3rjggczw8993hh17y9jam61k18kydmnrr5czaj76") (f (quote (("default" "qp-trie"))))))

