(define-module (crates-io as ci asciify) #:use-module (crates-io))

(define-public crate-asciify-0.1.0 (c (n "asciify") (v "0.1.0") (d (list (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1b2hdcax0laij6dih1sbq0fic71zim5w96xfpz1xsmmcxb2d4ca3")))

(define-public crate-asciify-0.1.1 (c (n "asciify") (v "0.1.1") (d (list (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0ki4y39wh2sxf1g6bkrgax02qgfc4khk43bc35y5rxjqs2ia0sjr")))

(define-public crate-asciify-0.1.2 (c (n "asciify") (v "0.1.2") (d (list (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1xh23z40abvrqg827fff2qw175ryx4pdys17rg0dcgrdkp7dbgl6")))

(define-public crate-asciify-0.1.3 (c (n "asciify") (v "0.1.3") (d (list (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1rz7f4v8ww381ig96a29cl04kwym965ck47pwapcw2ganssww85q")))

(define-public crate-asciify-0.1.4 (c (n "asciify") (v "0.1.4") (d (list (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1z30cchsc3agddxn63yybqbb0r8id0y1718gwhq1sfx6jjcgm652")))

(define-public crate-asciify-0.1.5 (c (n "asciify") (v "0.1.5") (d (list (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "05aa90cpafv3qdgy2rk833ssbhm7pds6140pgjc8ifa8wxirlndr")))

(define-public crate-asciify-0.1.6 (c (n "asciify") (v "0.1.6") (d (list (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0g6yvhn3fwdy23j7jrdikhsb4wnp91d0famzps2i9g7hab3w0h42")))

