(define-module (crates-io as ci ascii-osd-hud) #:use-module (crates-io))

(define-public crate-ascii-osd-hud-0.1.0 (c (n "ascii-osd-hud") (v "0.1.0") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 2)) (d (n "enum-map") (r "^0.6.2") (k 0)) (d (n "fastapprox") (r "^0.3.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.3") (d #t) (k 0)))) (h "17ra899n8js8avz3fwv7wnhpz6a00yljkxlz612p1ld4ndxsgvmv")))

(define-public crate-ascii-osd-hud-0.1.1 (c (n "ascii-osd-hud") (v "0.1.1") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 2)) (d (n "enum-map") (r "^0.6.2") (k 0)) (d (n "micromath") (r "^1.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2") (d #t) (k 0)))) (h "1lvmlzh9byylfbx5af6j6rc4kj22yvzg6175vbcfzflph5xjj6h0")))

(define-public crate-ascii-osd-hud-0.1.5 (c (n "ascii-osd-hud") (v "0.1.5") (d (list (d (n "ascii") (r "^1.0") (d #t) (k 2)) (d (n "enum-map") (r "^0.6") (k 0)) (d (n "heapless") (r "^0.6") (d #t) (k 0)) (d (n "micromath") (r "^1.1") (d #t) (k 0)))) (h "1kcx87j3i10p42b45wyz7w0q7k9l0r5qqa1rghi0znw3r4na600z")))

