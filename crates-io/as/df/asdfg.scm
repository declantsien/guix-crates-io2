(define-module (crates-io as df asdfg) #:use-module (crates-io))

(define-public crate-asdfg-0.1.0 (c (n "asdfg") (v "0.1.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "open") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1l7420pvpks45hdmkw5nk6sj4v1q66g2nq48mi0mri17524jw0k5")))

(define-public crate-asdfg-0.2.0 (c (n "asdfg") (v "0.2.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "open") (r "^5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1fqh4cqgsdx1m02crqgm1jg1rwwdnr3npl54v8pvnlk09zlya50z")))

