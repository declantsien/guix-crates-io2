(define-module (crates-io as df asdfler) #:use-module (crates-io))

(define-public crate-asdfler-0.4.0 (c (n "asdfler") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.157") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1fcjlgrbgpsffmvr4660n5jkq87r31akg98hm98czakgbi4m29xq")))

