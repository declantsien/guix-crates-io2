(define-module (crates-io as df asdf-pixel-sort) #:use-module (crates-io))

(define-public crate-asdf-pixel-sort-0.0.1 (c (n "asdf-pixel-sort") (v "0.0.1") (d (list (d (n "image") (r "^0.23") (k 0)))) (h "15j7q8ndag86z6fr7sr22pnvmxpgn34kwm8wm1yzk8pp4c037k1x")))

(define-public crate-asdf-pixel-sort-0.0.2 (c (n "asdf-pixel-sort") (v "0.0.2") (d (list (d (n "image") (r "^0.23") (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)))) (h "1015mirlsm6jpl18sp8z59gjja0x9g14shkxwy56rm4vf6s100bx")))

(define-public crate-asdf-pixel-sort-0.0.3 (c (n "asdf-pixel-sort") (v "0.0.3") (d (list (d (n "image") (r "^0.23") (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)))) (h "0c0igih9r5cpk52nsrcngwmpjf6nfpx6fvaqs4dv1f38hwsvzdss")))

(define-public crate-asdf-pixel-sort-0.0.4 (c (n "asdf-pixel-sort") (v "0.0.4") (d (list (d (n "image") (r "^0.23") (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)))) (h "1zxw72va7nm1wy43qrihql4zybs6bkdnlvw6ly0yp58scdcia86z")))

(define-public crate-asdf-pixel-sort-0.0.5 (c (n "asdf-pixel-sort") (v "0.0.5") (d (list (d (n "image") (r "^0.23") (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1xcn4n68dahb83gkcyqkkdzwbasws9mk094iimnzlscim5dmfxgc")))

(define-public crate-asdf-pixel-sort-0.0.6 (c (n "asdf-pixel-sort") (v "0.0.6") (d (list (d (n "image") (r "^0.23.14") (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("bmp"))) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1kxi9nf2iqqly9nd158jfp21r9hzqdyfvphg7zcm6kr5q53p0gn7")))

(define-public crate-asdf-pixel-sort-0.0.7 (c (n "asdf-pixel-sort") (v "0.0.7") (d (list (d (n "image") (r "^0.23.14") (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("bmp"))) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "14y360ccgiq8gzfqax0vv8mim8bhq79ynymqr6iqsn9mfyx6bsb8")))

(define-public crate-asdf-pixel-sort-0.1.0 (c (n "asdf-pixel-sort") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("bmp"))) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1swa1imf32b1xnqff22y0jmr9rjl15s7hp2nxirp2abzq3v7n1mc")))

(define-public crate-asdf-pixel-sort-0.1.1 (c (n "asdf-pixel-sort") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("bmp"))) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0sam14dlv3dy5jb32q4z4kfbg1f617r43ff80bnps4q2qa9id4gs")))

(define-public crate-asdf-pixel-sort-0.2.0 (c (n "asdf-pixel-sort") (v "0.2.0") (d (list (d (n "image") (r "^0.23.14") (k 0)) (d (n "image") (r "^0.23.14") (f (quote ("bmp"))) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1rdpvbs4dmybzazgy9mq3xjwq2scqfd22s34chrwawwxhf18yamy")))

