(define-module (crates-io as tr astree_macro) #:use-module (crates-io))

(define-public crate-astree_macro-0.0.0 (c (n "astree_macro") (v "0.0.0") (d (list (d (n "astree_core") (r "0.*") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "07ir39j99nrvb1cypln50snvxr0dlvrj9iwkmxgphzdvklwjgpdw")))

(define-public crate-astree_macro-0.1.0 (c (n "astree_macro") (v "0.1.0") (d (list (d (n "astree_core") (r "0.*") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1zzjcgcy8xdj9k0s9dz6bw418kgg48xwb29djmqwh1qv6hvg091w")))

