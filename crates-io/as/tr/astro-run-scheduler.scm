(define-module (crates-io as tr astro-run-scheduler) #:use-module (crates-io))

(define-public crate-astro-run-scheduler-0.1.0 (c (n "astro-run-scheduler") (v "0.1.0") (d (list (d (n "astro-run") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "macros" "process" "fs" "io-util" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "08bvg2lxddrmfcrf31ayhlzb3zc6lfa01bibnv4jiy1gddplkb75")))

(define-public crate-astro-run-scheduler-0.1.1 (c (n "astro-run-scheduler") (v "0.1.1") (d (list (d (n "astro-run") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "macros" "process" "fs" "io-util" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0k370g6r16viz89qspf69283j86a3wj3kfhbh96vkqs7zmrm333q")))

(define-public crate-astro-run-scheduler-0.1.2 (c (n "astro-run-scheduler") (v "0.1.2") (d (list (d (n "astro-run") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync" "macros" "process" "fs" "io-util" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "18816zg4mhrmabm74wn45lg3mfnl7ilq73dgh9m4i9d9zvkl8cqh")))

