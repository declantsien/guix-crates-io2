(define-module (crates-io as tr astral-util) #:use-module (crates-io))

(define-public crate-astral-util-0.0.1 (c (n "astral-util") (v "0.0.1") (h "0k579v4jf0iz7gbrpmpki74hcd24yrhdbr23kwmdjx88k7n3w3xm")))

(define-public crate-astral-util-0.0.2 (c (n "astral-util") (v "0.0.2") (h "1ghy31x0a3ba3rkbz0axwlsjzz95pivw2xwpbj3d8faq17ldahpx")))

(define-public crate-astral-util-0.0.3 (c (n "astral-util") (v "0.0.3") (d (list (d (n "astral") (r "^0.0.1") (k 2) (p "astral-engine")))) (h "16iihgmq0ha87dmyn3212bsjgbivyr5nvx79hl621n70kmlr22bj")))

(define-public crate-astral-util-0.0.4 (c (n "astral-util") (v "0.0.4") (d (list (d (n "astral") (r "^0.0.2") (k 2) (p "astral-engine")))) (h "1xxggsx4nvbcn3k9jnad2a2815fjkjg3m1rn6yksslyvnrgimpxp")))

(define-public crate-astral-util-0.0.5 (c (n "astral-util") (v "0.0.5") (d (list (d (n "astral-engine") (r "^0.0.3") (k 2)))) (h "1zh0qd44lvc4gyrqxskpx4jslnwjm2d9hyw8hxkjil30x0azrvjm")))

