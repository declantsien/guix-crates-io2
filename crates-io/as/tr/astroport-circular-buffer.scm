(define-module (crates-io as tr astroport-circular-buffer) #:use-module (crates-io))

(define-public crate-astroport-circular-buffer-0.1.0 (c (n "astroport-circular-buffer") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hyj75pbqfx6gdskysaxzxq7nnnqvwibrapapakfg0sk4ikp9hys")))

(define-public crate-astroport-circular-buffer-0.2.0 (c (n "astroport-circular-buffer") (v "0.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dizjdy0hnr61qc153vinykmyny144idn80nhr7q09j17jfkdiri")))

