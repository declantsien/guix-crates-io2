(define-module (crates-io as tr astrup) #:use-module (crates-io))

(define-public crate-astrup-0.0.9 (c (n "astrup") (v "0.0.9") (d (list (d (n "cairo-rs") (r "^0.3") (f (quote ("png"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "gio") (r "^0.3") (d #t) (k 0)) (d (n "gtk") (r "^0.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "palette") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "12wgi56ar5q8y9yy76zdf8fl41zrzzkqqqn1458d73k9bx9v3mdk")))

