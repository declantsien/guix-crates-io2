(define-module (crates-io as tr astronomical-quantities) #:use-module (crates-io))

(define-public crate-astronomical-quantities-0.10.0 (c (n "astronomical-quantities") (v "0.10.0") (d (list (d (n "qty-macros") (r "^0.9.0") (d #t) (k 0)) (d (n "quantities") (r "^0.10.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "09f8r13i14hi9fndb3z7girxw4m296wnq4wxy42qwl31l43akckc") (f (quote (("std") ("default" "std"))))))

(define-public crate-astronomical-quantities-0.11.0 (c (n "astronomical-quantities") (v "0.11.0") (d (list (d (n "qty-macros") (r "^0.11.0") (d #t) (k 0)) (d (n "quantities") (r "^0.11.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0wv8qmpqshx2gy8jfngi2aprikc700awhali29kqgs9ggzj41gnh") (f (quote (("std") ("default" "std"))))))

(define-public crate-astronomical-quantities-0.12.0 (c (n "astronomical-quantities") (v "0.12.0") (d (list (d (n "qty-macros") (r "^0.12.0") (d #t) (k 0)) (d (n "quantities") (r "^0.12.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0m9xg2ayw5pwwj011fv9lr8h0k83zv4glfsjf33sm35aqzqdq2fg") (f (quote (("std") ("default" "std"))))))

