(define-module (crates-io as tr astrotools) #:use-module (crates-io))

(define-public crate-astrotools-0.2.1 (c (n "astrotools") (v "0.2.1") (d (list (d (n "lightspeed-astro") (r "^0.6.2") (d #t) (k 0)) (d (n "serialport") (r "^4.1") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0wpb5pymvrzi5y02wg3cxf5a28i2yr4vw1bi7fixm8mnxpx0g1yy")))

(define-public crate-astrotools-0.3.0 (c (n "astrotools") (v "0.3.0") (d (list (d (n "lightspeed-astro") (r "^0.6.2") (d #t) (k 0)) (d (n "serialport") (r "^4.1") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1pqq53ml75vcll7xddi69awl47h49vn2n4p4nwx16xd8fx6y85d9")))

(define-public crate-astrotools-0.3.1 (c (n "astrotools") (v "0.3.1") (d (list (d (n "lightspeed-astro") (r "^0.6") (d #t) (k 0)) (d (n "serialport") (r "^4.1") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0gw9ikr0ggb7vy3r9j9ivcbaq6shjjqg8rwzpnsb0rsrhzy2kwrc")))

(define-public crate-astrotools-0.4.0 (c (n "astrotools") (v "0.4.0") (d (list (d (n "lightspeed-astro") (r "^0.8") (d #t) (k 0)) (d (n "serialport") (r "^4.1") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "048amcx3lyrz929nfsmpcfc4yq90xb83hpqcz5d06ymarxfs3gpl")))

(define-public crate-astrotools-0.4.1 (c (n "astrotools") (v "0.4.1") (d (list (d (n "lightspeed-astro") (r "^1.1.1") (d #t) (k 0)) (d (n "serialport") (r "^4.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1sblihsyl3ia8s3jnkjzdvpxsv9q8ndhqbc733fvk0q90ivdgi72")))

(define-public crate-astrotools-0.5.0 (c (n "astrotools") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serialport") (r "^4.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0ckmy6v6aq4pkysdnfg4vpcdfqg1cyy532mbhd9fzyjn5vvd60kg")))

(define-public crate-astrotools-0.6.0 (c (n "astrotools") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serialport") (r "^4.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0258f9dssi86w3lacsrwdqvv3wk3yzq0pbzbjfwab02pnq4yplpv")))

(define-public crate-astrotools-0.7.0 (c (n "astrotools") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1y850idya2avsvwyz2fhxyl1bx4gsbp4f43baac16sdcxgn3nj2c")))

(define-public crate-astrotools-0.7.1 (c (n "astrotools") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "095lf5ip38a7c9k5b73hsx8s1zgb2w96zzj3wqv9cmdz2098j920")))

(define-public crate-astrotools-0.8.0 (c (n "astrotools") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rfsxvgm4a36wpjw7b2izdx45r3sqzgbdz3h8jhs9h6ay83igb4j")))

