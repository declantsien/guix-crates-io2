(define-module (crates-io as tr astrolabe) #:use-module (crates-io))

(define-public crate-astrolabe-0.0.0 (c (n "astrolabe") (v "0.0.0") (h "15f9rgwjf5mxnc483lfzrnxd3x22wi8l8jairldqm739m0x0kx24")))

(define-public crate-astrolabe-0.0.1 (c (n "astrolabe") (v "0.0.1") (d (list (d (n "fancy-regex") (r "^0.10.0") (f (quote ("unicode"))) (o #t) (k 0)))) (h "1kfcb4zgfb7zjdbc6pd578d9a6lckhx1d93v1mmgb22vsydldpsw") (f (quote (("default")))) (s 2) (e (quote (("format" "dep:fancy-regex"))))))

(define-public crate-astrolabe-0.0.2 (c (n "astrolabe") (v "0.0.2") (h "09qpby3h1hw41jfcv48iryrnkazx826l99hqx7rjrr2ppb3q0x0r")))

(define-public crate-astrolabe-0.1.0 (c (n "astrolabe") (v "0.1.0") (h "1gdpkxzsqryy6fk1f5pn91wrsz502mgiffarg3hcbs0wnbcg7fmb") (r "1.56")))

(define-public crate-astrolabe-0.2.0 (c (n "astrolabe") (v "0.2.0") (h "0b7rasjynrmnjczp34m9bg62fibc1m2di5rim0adawjj3p7iyx9z") (r "1.56")))

(define-public crate-astrolabe-0.3.0 (c (n "astrolabe") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0z1bg80dfzas8kd81g9iij0x2bl8al4354213iv1l1z2c36piv1f") (r "1.56")))

(define-public crate-astrolabe-0.4.0 (c (n "astrolabe") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1c35anb4wzd00xl5689ai7w5wpf3wf8bw46fyb2pf83xammfk7i4") (f (quote (("cron")))) (r "1.56")))

(define-public crate-astrolabe-0.5.0 (c (n "astrolabe") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "14iy7zlrj0as07vd3wcjbg9rkyh97kvk0x9wm02fvv4c12fjl2ny") (r "1.56")))

(define-public crate-astrolabe-0.5.1 (c (n "astrolabe") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "097wz2736n2bk2gymvnz844218a1q4iw4zaazfv8n40x7bcry62m") (r "1.56")))

(define-public crate-astrolabe-0.5.2 (c (n "astrolabe") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1gxg1xr9y9rcxfyiwff9dz90g1mdkxbdbz6w0fddrbc0ga1rp5jy") (r "1.56")))

