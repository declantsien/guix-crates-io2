(define-module (crates-io as tr astray_core) #:use-module (crates-io))

(define-public crate-astray_core-0.1.0 (c (n "astray_core") (v "0.1.0") (d (list (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)))) (h "1pbyha1d2qh7mn3cvccbc0zqx6gjqin49gzpg51av1jfhf6zkw31")))

(define-public crate-astray_core-0.1.1 (c (n "astray_core") (v "0.1.1") (d (list (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0x333779jl38a0krgzp0y6wvqngwfibsw7nm75k1isjc37crp5ij")))

(define-public crate-astray_core-0.1.2 (c (n "astray_core") (v "0.1.2") (d (list (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0hnf2pmq0mdwb5320mjbbw6a5341q95r1a568xv6am7gqhh4vxg7")))

(define-public crate-astray_core-0.1.3 (c (n "astray_core") (v "0.1.3") (d (list (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "04dcrs53n4d33s6fzfvivv0hvkza5g2s9p173y3k56by6svv7761")))

(define-public crate-astray_core-0.1.4 (c (n "astray_core") (v "0.1.4") (d (list (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0hnvnqrcwzx88n7gmdqgii7g65x0cnrpljg1iavd54f9bsv5fwg2")))

(define-public crate-astray_core-0.1.5 (c (n "astray_core") (v "0.1.5") (d (list (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0v2zimsa9zr80hbd2ny9fsw2vm9w9jvi67c3vsq7b3w3vz6b9fsn")))

(define-public crate-astray_core-0.1.6 (c (n "astray_core") (v "0.1.6") (d (list (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1napq48apm7577m45j86fq34hrglcpa6nkcjja85dml904a9xl5i")))

(define-public crate-astray_core-0.1.8 (c (n "astray_core") (v "0.1.8") (d (list (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0p0w3z6l1q2hli11jm1bvjn4dlf2y19fwlc6p44y5s64lip62lbf")))

(define-public crate-astray_core-0.1.9 (c (n "astray_core") (v "0.1.9") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1x0bd3x4g6j6rh794xhhv1w5nm6fznwirazx3vw7wl72yb8qvys0")))

(define-public crate-astray_core-0.1.10 (c (n "astray_core") (v "0.1.10") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1gj9y41x4yp9brxvklzdbzwcx9qxx2x87gqrhhc67ml4vga3k3r8")))

(define-public crate-astray_core-0.1.11 (c (n "astray_core") (v "0.1.11") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "04lgmh28wvy3hjwzy2xvdsv0xsh9pcslbdli4wb2zw17r6k61m3r")))

(define-public crate-astray_core-0.1.12 (c (n "astray_core") (v "0.1.12") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0jvhm7x3dhvgi0c9n5kgnmj4kmjwkbia99rlfi9qc4fbjv0b29yx")))

(define-public crate-astray_core-0.1.13 (c (n "astray_core") (v "0.1.13") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "06xll1vvjw4b9sipv1s1bndxpqzd6rb2y4f15wkbpazz0zzkn67i")))

(define-public crate-astray_core-0.1.14 (c (n "astray_core") (v "0.1.14") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "15y4avdvi8xlxa98i92xzm19dipnfza5wg3d34wnj181z3hzhqak")))

(define-public crate-astray_core-0.1.15 (c (n "astray_core") (v "0.1.15") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0xp216qvfic80isibfrjgl1gj77m967hrnagjqiglznw0lk6mp7k")))

(define-public crate-astray_core-0.1.16 (c (n "astray_core") (v "0.1.16") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0z004265lpk5325ijhg42k7nzshiij4id4miywd34w99v3rdjsg2")))

(define-public crate-astray_core-0.1.17 (c (n "astray_core") (v "0.1.17") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1q5xlfx9f2j45vly0fh5wkykdb534p154dh8n4mprxg6p09xmwiv")))

(define-public crate-astray_core-0.1.18 (c (n "astray_core") (v "0.1.18") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "hatch_result") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "121fsxx0rhbby9x11dp9lxmakq3yhb7swm9mlngjkpm25nlkz1r8")))

