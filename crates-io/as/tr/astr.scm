(define-module (crates-io as tr astr) #:use-module (crates-io))

(define-public crate-astr-0.1.0 (c (n "astr") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0hl40p32qvf082q3psg9f24kz0rrwzy5lk8gfvbmwg43dzjszphm") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-astr-0.1.1 (c (n "astr") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0m2b0g4ax2nss04aa25q1521phi39ql1fp71mp0r9xx4bk9cz5mc") (f (quote (("std") ("default" "std"))))))

(define-public crate-astr-0.2.0 (c (n "astr") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "09gz50zmqcwgn7zancl2d1kmpqjqdjzygpgjs159dl1329qq4hq0") (f (quote (("std") ("default" "std"))))))

(define-public crate-astr-0.3.0 (c (n "astr") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0kyazfibmm8fxz16f5433dg4143ww26cgba3jiwc1mlsp4hchnc3") (f (quote (("std") ("default" "std"))))))

(define-public crate-astr-0.3.1 (c (n "astr") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "157zmas446igz67cn3gkajbm5iicnqbdawzrid3gdg6s058fzc8j") (f (quote (("std") ("default" "std"))))))

