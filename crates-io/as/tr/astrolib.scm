(define-module (crates-io as tr astrolib) #:use-module (crates-io))

(define-public crate-astrolib-0.1.0 (c (n "astrolib") (v "0.1.0") (d (list (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)))) (h "1f1vj6wf9nw0fflfqx416i40qgziq8hwplv37f56yz4f2xpyc93c")))

(define-public crate-astrolib-0.1.1 (c (n "astrolib") (v "0.1.1") (d (list (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)))) (h "1zy56dcwkmq4rfjbdvqay7qynxyacqdxxnjh5lw9fbk7wwnlya46")))

(define-public crate-astrolib-0.1.2 (c (n "astrolib") (v "0.1.2") (d (list (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)))) (h "07iqf0dpm3yh3srxs5h37p2fs50nmj4x1z6jf4qmarcnvznhjfkx")))

