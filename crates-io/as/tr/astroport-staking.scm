(define-module (crates-io as tr astroport-staking) #:use-module (crates-io))

(define-public crate-astroport-staking-1.1.0 (c (n "astroport-staking") (v "1.1.0") (d (list (d (n "astroport") (r "^2.8") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.15") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^0.15") (d #t) (k 0)) (d (n "cw2") (r "^0.15") (d #t) (k 0)) (d (n "cw20") (r "^0.15") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rrhn3dy345wrzfp6z91lyncfm4api37vg7dl5n8hkkr811brbb7") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

