(define-module (crates-io as tr astroport-native-coin-registry) #:use-module (crates-io))

(define-public crate-astroport-native-coin-registry-1.0.1 (c (n "astroport-native-coin-registry") (v "1.0.1") (d (list (d (n "astroport") (r "^2.8") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.1") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.15") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^0.15") (d #t) (k 0)) (d (n "cw2") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09kqqnyp0ghi40rdp1kcwhgm3s04j8r61yvp0cn024cgga1dd3k4") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

