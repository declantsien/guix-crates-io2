(define-module (crates-io as tr astroport-governance) #:use-module (crates-io))

(define-public crate-astroport-governance-1.2.0 (c (n "astroport-governance") (v "1.2.0") (d (list (d (n "astroport") (r "^2.8") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.15") (d #t) (k 0)) (d (n "cw20") (r "^0.15") (d #t) (k 0)))) (h "0v81dz64mgfigbb9gmkm0a1ssnzhj7ppbqmprbhw908f6p76m03j") (f (quote (("testnet") ("backtraces" "cosmwasm-std/backtraces"))))))

