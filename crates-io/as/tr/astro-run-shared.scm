(define-module (crates-io as tr astro-run-shared) #:use-module (crates-io))

(define-public crate-astro-run-shared-0.1.0-dev.1 (c (n "astro-run-shared") (v "0.1.0-dev.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "octocrate") (r "^0.1.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("sync" "macros" "process" "fs" "io-util" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)))) (h "09d67hr82h8y5zlswsh01f7kmackl9l0vrgpb0r5q3hj8068qjz1")))

