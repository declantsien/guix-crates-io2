(define-module (crates-io as tr astra) #:use-module (crates-io))

(define-public crate-astra-0.1.0 (c (n "astra") (v "0.1.0") (h "009j3g1xkd6pn2f4fnrxi2s58bkj7j5lrgk850xlnn9pp51c235y")))

(define-public crate-astra-0.1.1 (c (n "astra") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "hyper") (r "^0.14.6") (f (quote ("http1" "http2" "server" "stream"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "matchit") (r "^0.4.4") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "polling") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)))) (h "0m9wv40i74217f0vqpjhladmmp6qf94ky1515xzv779g7b90v9mq")))

(define-public crate-astra-0.1.2 (c (n "astra") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.3.19") (d #t) (k 0)) (d (n "hyper") (r "^0.14.6") (f (quote ("http1" "http2" "server" "stream"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "matchit") (r "^0.4.4") (d #t) (k 2)) (d (n "mio") (r "^0.8.0") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (d #t) (k 0)))) (h "199k53gs7lh1r8r7v04wr5x0zvhbgb2q7dnzi9qip1bhpjzq3iic")))

(define-public crate-astra-0.1.3 (c (n "astra") (v "0.1.3") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)) (d (n "hyper") (r "^0.14.23") (f (quote ("http1" "http2" "server" "stream"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "matchit") (r "^0.7.0") (d #t) (k 2)) (d (n "mio") (r "^0.8.5") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.1") (d #t) (k 0)))) (h "1m81p9dabf1b6462cam3wzf41z3b0y18n1yniy48d04js7z6azm9")))

(define-public crate-astra-0.2.0 (c (n "astra") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)) (d (n "hyper") (r "^0.14.23") (f (quote ("http1" "http2" "server" "stream"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "matchit") (r "^0.7.0") (d #t) (k 2)) (d (n "mio") (r "^0.8.5") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.1") (d #t) (k 0)))) (h "1zyqsr0wfblzcivairq9qf2w4w0510ysdl7qrkh1a7phx04ccv7m")))

(define-public crate-astra-0.3.0 (c (n "astra") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)) (d (n "hyper") (r "^0.14.23") (f (quote ("http1" "server" "stream"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "matchit") (r "^0.7.0") (d #t) (k 2)) (d (n "mio") (r "^0.8.5") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.1") (d #t) (k 0)))) (h "02g6fxq00rpfcjmpwx8zk4ibvaj7465ymzayz01hw3jgzf1ijnzs") (f (quote (("http2" "hyper/http2") ("default"))))))

