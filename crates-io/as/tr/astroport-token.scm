(define-module (crates-io as tr astroport-token) #:use-module (crates-io))

(define-public crate-astroport-token-1.1.1 (c (n "astroport-token") (v "1.1.1") (d (list (d (n "astroport") (r "^2.8") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw2") (r "^0.15") (d #t) (k 0)) (d (n "cw20") (r "^0.15") (d #t) (k 0)) (d (n "cw20-base") (r "^0.15") (f (quote ("library"))) (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "16adc1kddbaw09zki2ninm9q4mq0n5zdanj5kbd12ljq5qx3hq1k") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

