(define-module (crates-io as tr astray) #:use-module (crates-io))

(define-public crate-astray-0.1.0 (c (n "astray") (v "0.1.0") (d (list (d (n "astray_core") (r "0.*") (d #t) (k 0)) (d (n "astray_macro") (r "0.*") (d #t) (k 0)))) (h "1ykdpshc2r7ciwxyg9xnrsxf601234nyfifsg6488frm7ai7lj32")))

(define-public crate-astray-0.1.1 (c (n "astray") (v "0.1.1") (d (list (d (n "astray_core") (r "0.*") (d #t) (k 0)) (d (n "astray_macro") (r "0.*") (d #t) (k 0)))) (h "10qpr2gf51w9sah72x7x2sxn1z9jszf7lnffjzicrvk3sl6yzal9")))

(define-public crate-astray-0.1.2 (c (n "astray") (v "0.1.2") (d (list (d (n "astray_core") (r "0.*") (d #t) (k 0)) (d (n "astray_macro") (r "0.*") (d #t) (k 0)))) (h "0jdncvcifv4dz5hna3yihfj6d1pl5rlc29h8wmpgsr800ya5vd4q")))

(define-public crate-astray-0.1.3 (c (n "astray") (v "0.1.3") (d (list (d (n "astray_core") (r "0.*") (d #t) (k 0)) (d (n "astray_macro") (r "0.*") (d #t) (k 0)))) (h "1wcn7lyafh5ap61fxpy34vpvf8jd2rq8dqn8s7d20mn4qwhga2fs")))

(define-public crate-astray-0.1.4 (c (n "astray") (v "0.1.4") (d (list (d (n "astray_core") (r "0.*") (d #t) (k 0)) (d (n "astray_macro") (r "0.*") (d #t) (k 0)))) (h "0clbwkpwvy41rascdgv86hyizhvbgdqjp0jh9v0lhqbvidg0lbhb")))

(define-public crate-astray-0.1.5 (c (n "astray") (v "0.1.5") (d (list (d (n "astray_core") (r "0.*") (d #t) (k 0)) (d (n "astray_macro") (r "0.*") (d #t) (k 0)))) (h "0nfhiz3cn078qyphc232k60pafxj1vbivd4iaf9095ai677hp6ak")))

(define-public crate-astray-0.1.6 (c (n "astray") (v "0.1.6") (d (list (d (n "astray_core") (r "^0.1.15") (d #t) (k 0)) (d (n "astray_macro") (r "^0.2.0") (d #t) (k 0)))) (h "04gk5kr16dikbaji3i6avm8148z4as7rd1l6hcv44mkdhg1fgfjs")))

(define-public crate-astray-0.1.7 (c (n "astray") (v "0.1.7") (d (list (d (n "astray_core") (r "^0.1.15") (d #t) (k 0)) (d (n "astray_macro") (r "^0.2.1") (d #t) (k 0)))) (h "0i6s7n9ysadp4cvq2rp4ppvyn2546mfhcq6r51rc1y0f1g0pb3ac")))

(define-public crate-astray-0.1.8 (c (n "astray") (v "0.1.8") (d (list (d (n "astray_core") (r "^0.1.16") (d #t) (k 0)) (d (n "astray_macro") (r "^0.2.1") (d #t) (k 0)))) (h "1h22gb65d09jhp6dn05v491ll9h3fvmsvnrd7s231nr83h64f86d")))

(define-public crate-astray-0.1.9 (c (n "astray") (v "0.1.9") (d (list (d (n "astray_core") (r "^0.1.17") (d #t) (k 0)) (d (n "astray_macro") (r "^0.2.1") (d #t) (k 0)))) (h "08wj3dy0ywyhcdqgz8w13rawf6r4zg8mf4hdlazkcsw3cyv88dgs")))

(define-public crate-astray-0.1.10 (c (n "astray") (v "0.1.10") (d (list (d (n "astray_core") (r "^0.1.17") (d #t) (k 0)) (d (n "astray_macro") (r "^0.2.2") (d #t) (k 0)))) (h "06k3c87n8slzs1cqv6fc6m1bxdpwp2slh9qyfprzhr8n8lqyaxkv")))

(define-public crate-astray-0.1.11 (c (n "astray") (v "0.1.11") (d (list (d (n "astray_core") (r "^0.1.17") (d #t) (k 0)) (d (n "astray_macro") (r "^0.2.3") (d #t) (k 0)))) (h "17j6yriqdh45wmrrm3jr3bmc1qbzbgn1ji58pxn7lrj5srxyzs0g")))

(define-public crate-astray-0.1.12 (c (n "astray") (v "0.1.12") (d (list (d (n "astray_core") (r "^0.1.18") (d #t) (k 0)) (d (n "astray_macro") (r "^0.2.4") (d #t) (k 0)))) (h "1i60x39jcwn69xry793crkpmb26gcak5fkq7h8vrsjpygn18zfay")))

(define-public crate-astray-0.1.13 (c (n "astray") (v "0.1.13") (d (list (d (n "astray_core") (r "^0.1.18") (d #t) (k 0)) (d (n "astray_macro") (r "^0.2.5") (d #t) (k 0)))) (h "141q46s52y5x1q4ikqmpvz863pgwp37kxnvj45r1ipy7hx25akvq")))

