(define-module (crates-io as tr astro-float-macro) #:use-module (crates-io))

(define-public crate-astro-float-macro-0.1.0 (c (n "astro-float-macro") (v "0.1.0") (d (list (d (n "astro-float") (r "^0.6.0") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "170zmbqkdiwrn1922dhvc0f8vwcrf9pr8dnfkdxwpm01whl2crp2")))

(define-public crate-astro-float-macro-0.1.1 (c (n "astro-float-macro") (v "0.1.1") (d (list (d (n "astro-float-num") (r "^0.1.0") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0bi3vngj1dpfv6w6hdrx0c2fr1snsz9mwkwa1dznmc67kfp8r8ks")))

(define-public crate-astro-float-macro-0.1.2 (c (n "astro-float-macro") (v "0.1.2") (d (list (d (n "astro-float-num") (r "^0.1.1") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "0vpi65yvjxvmw0phx340c1ka60zb8p1cjp1il5padsjj2z5369wg")))

(define-public crate-astro-float-macro-0.1.3 (c (n "astro-float-macro") (v "0.1.3") (d (list (d (n "astro-float-num") (r "^0.1.2") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "1ilw06lg09qrsrzmsk6vag8blqx81yp15r78mb9b6nvix7l3258h")))

(define-public crate-astro-float-macro-0.1.4 (c (n "astro-float-macro") (v "0.1.4") (d (list (d (n "astro-float-num") (r "^0.1.3") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "17hjs5ydi2hrkn4qdi38r2ala0zwwv9cks1mkcxy1gxrb4ml8b0j")))

(define-public crate-astro-float-macro-0.1.5 (c (n "astro-float-macro") (v "0.1.5") (d (list (d (n "astro-float-num") (r "^0.1.4") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "0h1kgib3q7000k7lpi1l1x2pysh5wq1a7fhjax9lvrwyvcskdrwc")))

(define-public crate-astro-float-macro-0.1.6 (c (n "astro-float-macro") (v "0.1.6") (d (list (d (n "astro-float-num") (r "^0.1.5") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "16wqbp4dmd0nzr8pk1w7f79vig2an6k3djpz61dswy2s6wikxhbs")))

(define-public crate-astro-float-macro-0.1.7 (c (n "astro-float-macro") (v "0.1.7") (d (list (d (n "astro-float-num") (r "^0.1.6") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "0lbim9wam19g8zidkpx6ipjq929smhwm3i1bc3y2cil0hrw6whsd")))

(define-public crate-astro-float-macro-0.1.8 (c (n "astro-float-macro") (v "0.1.8") (d (list (d (n "astro-float-num") (r "^0.1.7") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "01fgbisw4rpdv04g5kq8aiha77bgm1844knv9qd6n72i4zmnc123")))

(define-public crate-astro-float-macro-0.1.9 (c (n "astro-float-macro") (v "0.1.9") (d (list (d (n "astro-float-num") (r "^0.1.8") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "0frljb1f4038xaiywn3j345jghwwlmwgd8i9wzbmb65qgj5gndmy")))

(define-public crate-astro-float-macro-0.2.0 (c (n "astro-float-macro") (v "0.2.0") (d (list (d (n "astro-float-num") (r "^0.1.9") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "14wxs4h37g17jpg6vci1cjfahs9s836yzzkirvmar4rjgd5p7cva")))

(define-public crate-astro-float-macro-0.3.0 (c (n "astro-float-macro") (v "0.3.0") (d (list (d (n "astro-float-num") (r "^0.2.0") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "0kdxhic18gcddiz5yf3s4qg4y7fdfnn77xpi3m1wpy465chsph0a")))

(define-public crate-astro-float-macro-0.3.1 (c (n "astro-float-macro") (v "0.3.1") (d (list (d (n "astro-float-num") (r "^0.2.1") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "1j86iwvnnw04wsa4dggmg172gdm1dk5n5qk38pbwhy596gwgdin4")))

(define-public crate-astro-float-macro-0.3.2 (c (n "astro-float-macro") (v "0.3.2") (d (list (d (n "astro-float-num") (r "^0.2.2") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "1h8pjrgqq1zsns09n4vq3aimwhssqhajzr1y1walxv9gi0l9w6j5")))

(define-public crate-astro-float-macro-0.4.0 (c (n "astro-float-macro") (v "0.4.0") (d (list (d (n "astro-float-num") (r "^0.3.0") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "1c4kznb1gc4pcdfx0j8nrw9cxil88i63h3vvakq2dx1p1yp03nyj")))

(define-public crate-astro-float-macro-0.4.1 (c (n "astro-float-macro") (v "0.4.1") (d (list (d (n "astro-float-num") (r "^0.3.2") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "1vk346zjra2ij4q66qy5vdq1chbs4jl7lsp60gfxavdbjilvr53s")))

(define-public crate-astro-float-macro-0.4.2 (c (n "astro-float-macro") (v "0.4.2") (d (list (d (n "astro-float-num") (r "^0.3.3") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "1jdgnsc232zbxb1il8zisdklskwhiyyx0hw8cf6v00ghjs7mlwq4")))

(define-public crate-astro-float-macro-0.4.3 (c (n "astro-float-macro") (v "0.4.3") (d (list (d (n "astro-float-num") (r "^0.3.4") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "0g3ifpw2dq4yjw9va5rh317l0gvn155nxfs8r5fvwmq61s5vgyzx")))

(define-public crate-astro-float-macro-0.4.4 (c (n "astro-float-macro") (v "0.4.4") (d (list (d (n "astro-float-num") (r "^0.3.5") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (k 0)))) (h "0ya2ncfqnvgwd941l41kw7gd8cb5vfv1szk6l86qrf93h7747j7a")))

