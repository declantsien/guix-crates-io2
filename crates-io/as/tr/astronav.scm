(define-module (crates-io as tr astronav) #:use-module (crates-io))

(define-public crate-astronav-0.1.0 (c (n "astronav") (v "0.1.0") (h "03bnhirymvmvp085j6w1j1f8ycj4d7v6y3mykybx2zgipix0nrf3") (r "1.69.0")))

(define-public crate-astronav-0.2.0 (c (n "astronav") (v "0.2.0") (h "0kppsbsr4ishzxhdvw1h1mnv3ma1n0m7bvxqnq7bm3lisn89bcr0") (f (quote (("noaa-sun") ("default" "noaa-sun")))) (y #t) (r "1.69.0")))

(define-public crate-astronav-0.2.1 (c (n "astronav") (v "0.2.1") (h "1czcp421hwkbk493mlkaaafqi16mfmbr1cxblrs32vb8m2l19jd6") (f (quote (("noaa-sun") ("default" "noaa-sun")))) (y #t) (r "1.69.0")))

(define-public crate-astronav-0.2.2 (c (n "astronav") (v "0.2.2") (h "0589rrm1j5r8d369cbsaxvixa8v8ps6bbqhhg5pdfg6h4ii3p9x5") (f (quote (("noaa-sun") ("default" "noaa-sun")))) (y #t) (r "1.69.0")))

(define-public crate-astronav-0.2.3 (c (n "astronav") (v "0.2.3") (h "1lvmr4zm3xhsglyv0mbllf6gildc8mcz74x28vh3p9av0zq8bap1") (f (quote (("noaa-sun") ("default" "noaa-sun")))) (r "1.69.0")))

(define-public crate-astronav-0.2.4 (c (n "astronav") (v "0.2.4") (h "1sb0wssd1adj1nfc75dzjq5f3rmlzxzkaqh6dq7wbcc3a8kqvvk0") (f (quote (("noaa-sun")))) (r "1.69.0")))

(define-public crate-astronav-0.2.5 (c (n "astronav") (v "0.2.5") (h "0sxi6kkwd5mlsl0mbqx6n61qpvzimakxcbrjg1x1bxdl43xvm9gc") (f (quote (("noaa-sun")))) (r "1.69.0")))

