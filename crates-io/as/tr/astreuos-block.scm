(define-module (crates-io as tr astreuos-block) #:use-module (crates-io))

(define-public crate-astreuos-block-0.1.0 (c (n "astreuos-block") (v "0.1.0") (d (list (d (n "astreuos-transaction") (r "^0.1.0") (d #t) (k 0)) (d (n "astro-format") (r "^0.2.0") (d #t) (k 0)) (d (n "fides") (r "^2.2.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.7") (d #t) (k 0)))) (h "16djrp2ardfzr2gxkyc31i9ighmjmm4vqvphkx01jpx0nk2xrjfx") (y #t)))

(define-public crate-astreuos-block-0.2.0 (c (n "astreuos-block") (v "0.2.0") (d (list (d (n "astreuos-transaction") (r "^0.2.0") (d #t) (k 0)) (d (n "astro-format") (r "^0.2.0") (d #t) (k 0)) (d (n "fides") (r "^2.2.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.7") (d #t) (k 0)))) (h "1n0rcx183f5xpg6ifyi3hy8scsycpzyzw4iwvxli9ynh4rqfzknk") (y #t)))

(define-public crate-astreuos-block-0.2.1 (c (n "astreuos-block") (v "0.2.1") (d (list (d (n "astreuos-transaction") (r "^0.2.0") (d #t) (k 0)) (d (n "astro-format") (r "^0.2.0") (d #t) (k 0)) (d (n "fides") (r "^2.2.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.7") (d #t) (k 0)))) (h "032pbs3pb15cq0igyqza0z3lzdv8984s1j7rbndl3b8f4v5g39d8") (y #t)))

