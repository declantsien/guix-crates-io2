(define-module (crates-io as tr astree) #:use-module (crates-io))

(define-public crate-astree-0.1.0 (c (n "astree") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "13sn8kgh6ikdhy19m3yz4l9v8c8x5rbis9k0fwy8yf01bdsr20pj")))

(define-public crate-astree-0.1.1 (c (n "astree") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "0azdig5y6z3anlk4mp7j5r3vaw2zs458ymkwa8xgfxy44mrp38nj")))

(define-public crate-astree-0.2.0 (c (n "astree") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "1zw7pm9vnix7cjkddw2zjfs635pvpaii4g8gig8lric18fqp37xm")))

(define-public crate-astree-0.2.1 (c (n "astree") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "0hnn34ysfjqkw58qp5rn9whfd4mgqd7y0rmk6cnidqpa4kvip8hw")))

(define-public crate-astree-0.2.2 (c (n "astree") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "17m6pig0r1mxmkzavispi0nyiprhvy3y2kyvsimnkfb4h6bcs6gb")))

(define-public crate-astree-0.2.3 (c (n "astree") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "0fnvwy6n9v2qaa2jkn6mbgfkkn52mbi7grc2b5svyd4p89c9zca5")))

(define-public crate-astree-0.2.4 (c (n "astree") (v "0.2.4") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "00wqh49i9davbrhffdgx6b4pkxz76nasxv37nzyn36649h49clvf")))

(define-public crate-astree-0.2.5 (c (n "astree") (v "0.2.5") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "1v8sgyq0bis684pffhlw402f426pzs7rkbjih4fl4gsdr7j6yjr0")))

(define-public crate-astree-0.2.6 (c (n "astree") (v "0.2.6") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "0cqclch58x3y5bvinlkyd53vm47sk3h9y27sm5hn8934fk99qlk9")))

(define-public crate-astree-0.2.7 (c (n "astree") (v "0.2.7") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)))) (h "02s0g0rsjg19fnxbqni6hvl2mjfsixgxf55135inw38dk6mr8042")))

