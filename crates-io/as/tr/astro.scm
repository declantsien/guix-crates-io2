(define-module (crates-io as tr astro) #:use-module (crates-io))

(define-public crate-astro-0.0.9 (c (n "astro") (v "0.0.9") (h "0d0s7gwaw90zr0vrp22663y80nf7q1480yaxv09w2hrm7mi1iw2m") (y #t)))

(define-public crate-astro-1.0.1 (c (n "astro") (v "1.0.1") (h "0cvrfb2an65znnlhiyskyrvafbvrdwvwaf2xfbk5kb06ld1fnasj") (y #t)))

(define-public crate-astro-1.0.2 (c (n "astro") (v "1.0.2") (h "0d8xw23cixa0rlmc8a4y02bgvy9qhzdcygfl42wpjs380fdf8psa") (y #t)))

(define-public crate-astro-1.0.3 (c (n "astro") (v "1.0.3") (h "07aginbfpj970jlqwbp7pi16ynypa3rj0v8kpq72naw2c561z13p") (y #t)))

(define-public crate-astro-1.0.4 (c (n "astro") (v "1.0.4") (h "1y30f2n2nryr9zyrq7cin0h4s5wj1mwf6rnxn49973bj4809v9ad") (y #t)))

(define-public crate-astro-0.1.0 (c (n "astro") (v "0.1.0") (h "12jk4gl234q4dfnflf7p6vf8ygni8b9gz63n4afjjaj37b339zms") (y #t)))

(define-public crate-astro-1.0.5 (c (n "astro") (v "1.0.5") (h "1gpz3bpsmr2m7h9vxi7i4s1sgcxjcvn40gr4a01hdm619c15nb1x") (y #t)))

(define-public crate-astro-1.0.6 (c (n "astro") (v "1.0.6") (h "0krz3y55wdhqh5qhishfwalxh18a8d1y2bafav0fi4jrrmzrmhva") (y #t)))

(define-public crate-astro-1.0.7 (c (n "astro") (v "1.0.7") (h "06dr03cx4xlwxg062bhzdl9wmk9b3dda58yyds02wi1yw3k007hg") (y #t)))

(define-public crate-astro-2.0.0 (c (n "astro") (v "2.0.0") (h "1ghlmvsacfqkw0ysqmr6wknj7ffajahypbd73mvwylblm3hk55hr")))

