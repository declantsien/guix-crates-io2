(define-module (crates-io as tr astrum-deus-package-sdk) #:use-module (crates-io))

(define-public crate-astrum-deus-package-sdk-0.1.0 (c (n "astrum-deus-package-sdk") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09bnyb20bdvl7420lksy23qy9pr1pndcvdhfrd7c1gw3jahkbwj8")))

(define-public crate-astrum-deus-package-sdk-0.2.0 (c (n "astrum-deus-package-sdk") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10lav33j0jnwjj9ky1d0gy98x3hf770bb4bpv36jy46qpxf5hd6i")))

(define-public crate-astrum-deus-package-sdk-0.2.1 (c (n "astrum-deus-package-sdk") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0bkmxk0v1hj7lw5kdy4g64gjp6l048r437ddx6dld8h1l1lvrsvz")))

(define-public crate-astrum-deus-package-sdk-0.2.2 (c (n "astrum-deus-package-sdk") (v "0.2.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nl6q2nsbk65xbacb2md17xqr1hvagbjbzvpzm5fdlvajlw9c2d4")))

(define-public crate-astrum-deus-package-sdk-0.2.3 (c (n "astrum-deus-package-sdk") (v "0.2.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jvi00ghfyfvybn0scib1755nlv6rm6lgkv29ljc40ccyjk80l9z")))

