(define-module (crates-io as tr astro-satellite-package) #:use-module (crates-io))

(define-public crate-astro-satellite-package-0.1.0 (c (n "astro-satellite-package") (v "0.1.0") (d (list (d (n "astroport-governance") (r "^1.2.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)))) (h "06nnw0fc7cpvql7zvj952z0lw36c8zdcd3zffnaild1cfv7kgxsw")))

(define-public crate-astro-satellite-package-1.0.0 (c (n "astro-satellite-package") (v "1.0.0") (d (list (d (n "astroport-governance") (r "^1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)))) (h "0f6ncpzm8gk4mhcjcqifg3xh44fqwlsrxwcrbmla98h4j60n6cw9")))

