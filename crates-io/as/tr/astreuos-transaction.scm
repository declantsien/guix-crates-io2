(define-module (crates-io as tr astreuos-transaction) #:use-module (crates-io))

(define-public crate-astreuos-transaction-0.1.0 (c (n "astreuos-transaction") (v "0.1.0") (d (list (d (n "astro-format") (r "^0.2.0") (d #t) (k 0)) (d (n "fides") (r "^2.2.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.7") (d #t) (k 0)))) (h "0hp779gzlybiqad3kj4wac13alp4fkwa4jaq3nqmcqcbk3pir9lx") (y #t)))

(define-public crate-astreuos-transaction-0.2.0 (c (n "astreuos-transaction") (v "0.2.0") (d (list (d (n "astro-format") (r "^0.2.0") (d #t) (k 0)) (d (n "fides") (r "^2.2.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.7") (d #t) (k 0)))) (h "19qjn3j72hzb6j903k7ss705pcb4zpqslv56l17vmpbpim16z8lm") (y #t)))

