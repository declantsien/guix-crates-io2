(define-module (crates-io as tr astro-notation) #:use-module (crates-io))

(define-public crate-astro-notation-1.0.0 (c (n "astro-notation") (v "1.0.0") (h "0fvrrr37ykhkqkr4rab2g86rdfca43086ifm146lf216ndcsx1f2")))

(define-public crate-astro-notation-1.1.0 (c (n "astro-notation") (v "1.1.0") (h "1mk609s7hrq9r4ccs04pzxw7zvgdq2s8dr83pcji1b1hfk64dvhw")))

(define-public crate-astro-notation-2.0.0 (c (n "astro-notation") (v "2.0.0") (h "182vwrq83piffhly9arcwx5dsf8vlff2f39wik2jblgsxhfnh5a8") (y #t)))

(define-public crate-astro-notation-1.2.0 (c (n "astro-notation") (v "1.2.0") (h "111nab7i8isvfw907va2a88vx92mqaakxd5v2b2bamc7b147nxi8")))

(define-public crate-astro-notation-1.2.1 (c (n "astro-notation") (v "1.2.1") (h "0rf32bhgznlayjqr3m40fyy1gx8x64wlyz20bjwf97bj85gw7wpp")))

(define-public crate-astro-notation-3.0.0 (c (n "astro-notation") (v "3.0.0") (h "08mihkldgx4ifbr41nsd3pf4ayhk4n9lr92s4xqwmvvd1pxpajdz")))

(define-public crate-astro-notation-3.1.0 (c (n "astro-notation") (v "3.1.0") (h "1a464w9cdmhvxxgkdhwlfs5mq6l1n87ihxxn8a4i67bdiy4nrq2p")))

