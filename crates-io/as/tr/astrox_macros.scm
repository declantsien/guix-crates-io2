(define-module (crates-io as tr astrox_macros) #:use-module (crates-io))

(define-public crate-astrox_macros-0.1.0 (c (n "astrox_macros") (v "0.1.0") (d (list (d (n "candid") (r "^0.7.10") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.5") (d #t) (k 0)) (d (n "ic-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "14hs91zz8xgws4fban8pwwna6irmkmxiblxksfrc2izx9wl5m1gk")))

(define-public crate-astrox_macros-0.1.1 (c (n "astrox_macros") (v "0.1.1") (d (list (d (n "candid") (r "^0.7.10") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.5") (d #t) (k 0)) (d (n "ic-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "00dv86r3r3dcjbqcilj3zyypixm8gz1j3dvfav05w1lfkm0rij1h")))

(define-public crate-astrox_macros-0.1.2 (c (n "astrox_macros") (v "0.1.2") (d (list (d (n "candid") (r "^0.7.10") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.5") (d #t) (k 0)) (d (n "ic-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0jhvmnfl76fkabc0hq768ipy0kq8hg4mrhpnhckf2fdg742xrx8y")))

(define-public crate-astrox_macros-0.2.0 (c (n "astrox_macros") (v "0.2.0") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0pdwcry0z44rw5fflb5cfz3z38sgpmkr4jbk6xilq331wygp96pl")))

(define-public crate-astrox_macros-0.2.1 (c (n "astrox_macros") (v "0.2.1") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1lf8v6maz77b9gzba5a02lcxzrh4jy0whk64hix8sxa0fvmjwvcl")))

(define-public crate-astrox_macros-0.2.2 (c (n "astrox_macros") (v "0.2.2") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0g8751l8sd2d1qmc4ki4xifrrckc1d1kx2hamm7i2hnqazkjlaai")))

(define-public crate-astrox_macros-0.2.3 (c (n "astrox_macros") (v "0.2.3") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "03vvx8bmspgy6fr54mxiyznjx01knnf8cr5jc3im9b4hmxzr6r2f")))

(define-public crate-astrox_macros-0.2.4 (c (n "astrox_macros") (v "0.2.4") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1bcl5s6zdsz3wga4zqpszs1qcld7kjv3pnjiq1pks161fmpya77q")))

(define-public crate-astrox_macros-0.2.5 (c (n "astrox_macros") (v "0.2.5") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "079k4cm8qr2y03gclf45nlfs3w2q3xpx0bzhh66sa9inml41cjmg")))

(define-public crate-astrox_macros-0.2.7 (c (n "astrox_macros") (v "0.2.7") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0nchlp18x6q1pg9li8ash0wk8xd0bwi7gsagbcf04qab4rq90dl9")))

(define-public crate-astrox_macros-0.2.8 (c (n "astrox_macros") (v "0.2.8") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "14xyqvh0pajgswjrk8z5ia7ixv6p8zz00ir8jzssvlk02zdj2qw5")))

(define-public crate-astrox_macros-0.2.9 (c (n "astrox_macros") (v "0.2.9") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1lkvlynjn7bk2534f81fcfpb0ljxhg4qnb96sg6qy9a8is6f4f30")))

(define-public crate-astrox_macros-0.2.10 (c (n "astrox_macros") (v "0.2.10") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1zhlv0r47kv2l3ir4c2bkmq8bja1clswd292s6mrddikkaamxpdg")))

(define-public crate-astrox_macros-0.2.11 (c (n "astrox_macros") (v "0.2.11") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0igxnnqfbd68v7y86psjn6fcd9rhdm883hmbvmdwjac504hb8ryh")))

(define-public crate-astrox_macros-0.2.12 (c (n "astrox_macros") (v "0.2.12") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "02klddm8mis2kbprjmhx8rwnr5wx3k0k8klmks1gavv1jh78wm0j")))

(define-public crate-astrox_macros-0.2.13 (c (n "astrox_macros") (v "0.2.13") (d (list (d (n "candid") (r "^0.8.4") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0yigiv4za0g41gr02g90kplypx3z98f6clznqc4bd2np55k98skm")))

