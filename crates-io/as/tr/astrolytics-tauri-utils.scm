(define-module (crates-io as tr astrolytics-tauri-utils) #:use-module (crates-io))

(define-public crate-astrolytics-tauri-utils-1.0.0 (c (n "astrolytics-tauri-utils") (v "1.0.0") (d (list (d (n "tauri") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.4") (d #t) (k 0)))) (h "0h382v9xpr2j4wqgpq5j6yja542j5x3mkhl3z355dbivxknw9cvp")))

