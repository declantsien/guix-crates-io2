(define-module (crates-io as tr astral-string) #:use-module (crates-io))

(define-public crate-astral-string-0.0.1 (c (n "astral-string") (v "0.0.1") (d (list (d (n "astral") (r "^0.0.1") (k 2) (p "astral-engine")) (d (n "astral-util") (r "^0.0.3") (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "slog") (r "^2.4") (d #t) (k 0)))) (h "1qdb6cv8248q5s4vfb6r4h7s13ks39j9kkgx5sx0qipg4jm2n0jm")))

(define-public crate-astral-string-0.0.2 (c (n "astral-string") (v "0.0.2") (d (list (d (n "astral") (r "^0.0.1") (k 2) (p "astral-engine")) (d (n "astral-util") (r "^0.0.3") (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "slog") (r "^2.4") (d #t) (k 0)))) (h "1dlnki4lpd781rabx9n1vibs9lsz4pz4c4mmk5ncyfi38v2fihpm")))

(define-public crate-astral-string-0.0.3 (c (n "astral-string") (v "0.0.3") (d (list (d (n "astral") (r "^0.0.1") (k 2) (p "astral-engine")) (d (n "astral-util") (r "^0.0.3") (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "slog") (r "^2.4") (d #t) (k 0)))) (h "0yvwxc5qm85r38hkx0z22pzlqmx51jws15zvbja6pqrb5rbc5rv1") (f (quote (("track-strings"))))))

(define-public crate-astral-string-0.0.4 (c (n "astral-string") (v "0.0.4") (d (list (d (n "astral") (r "^0.0.2") (k 2) (p "astral-engine")) (d (n "astral-thirdparty") (r "^0.0.1") (k 0)) (d (n "astral-util") (r "^0.0.4") (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1gc3s9005vfj3lj5sq1gfcxj2q0mqac5wwh830p798c17j85szar") (f (quote (("track-strings"))))))

(define-public crate-astral-string-0.0.5 (c (n "astral-string") (v "0.0.5") (d (list (d (n "astral-engine") (r "^0.0.3") (k 2)) (d (n "astral-thirdparty") (r "^0.0.2") (k 0)) (d (n "astral-util") (r "^0.0.5") (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1wyba3khx8skw2mfpd9nzlqmscnavakmg1nlh3p040zl7qlv0nz3") (f (quote (("track-strings"))))))

