(define-module (crates-io as tr astral-cli) #:use-module (crates-io))

(define-public crate-astral-cli-0.1.0 (c (n "astral-cli") (v "0.1.0") (d (list (d (n "astral") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)))) (h "079b5k0zjxfzbj40zy5fs6k4i4b4mxg7gdiivva5m0zczj37ai5n")))

(define-public crate-astral-cli-0.2.0 (c (n "astral-cli") (v "0.2.0") (d (list (d (n "astral") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "geocode") (r "^0.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)))) (h "1zw9gmak4xxm1ikwk0l9g8qazqsgf9z3nl0s6ws0wkxfhf55x5jq")))

(define-public crate-astral-cli-0.3.0 (c (n "astral-cli") (v "0.3.0") (d (list (d (n "astral") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "geocode") (r "^0.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)))) (h "0q1hziqnpdlfj9qx7n4qwqai1rrfmb4vdw5g2mk9z3i31525fsv2")))

(define-public crate-astral-cli-0.3.1 (c (n "astral-cli") (v "0.3.1") (d (list (d (n "astral") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "geocode") (r "^0.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)))) (h "1w8wcsj3gbaqmzggfh068klwq81nycx5c1nd52g6xn6n80ilad4h")))

(define-public crate-astral-cli-0.3.2 (c (n "astral-cli") (v "0.3.2") (d (list (d (n "astral") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "geocode") (r "^0.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.7") (d #t) (k 0)))) (h "1ygiyiii0pri3bgkhxw66kw6qkssl627pgqn5y40g550vs1cckbh")))

