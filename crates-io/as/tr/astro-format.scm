(define-module (crates-io as tr astro-format) #:use-module (crates-io))

(define-public crate-astro-format-0.1.0 (c (n "astro-format") (v "0.1.0") (h "0hs050pjvnj94191ar74w5vz5lkax8k73f4y7n6hmn1lvbvr8va5")))

(define-public crate-astro-format-0.2.0 (c (n "astro-format") (v "0.2.0") (h "1ksnnqydzddd16fa5cm7jglv78fb84m1a31jyghb5kcck24fvz7x")))

(define-public crate-astro-format-0.4.0 (c (n "astro-format") (v "0.4.0") (h "1fh1b1cvpjns6v2lmbiy5bs7s9j3myk0scclv3hs10jnpwv2x2lb")))

(define-public crate-astro-format-0.5.0 (c (n "astro-format") (v "0.5.0") (h "187a913gflxs22kbp9riwnhszknw83r2rls6zbcipfa63kij7xja")))

(define-public crate-astro-format-0.6.0 (c (n "astro-format") (v "0.6.0") (h "1dmwxq4sjfh15r97kd85bd0a7gg2bkdc12bj53v52n3vfjx6sjaq")))

(define-public crate-astro-format-0.7.0 (c (n "astro-format") (v "0.7.0") (h "17x1dhij61za0g9f007lfi5yqaav2dgmxvjnp327vrgj5ghnp6ai")))

(define-public crate-astro-format-0.8.0 (c (n "astro-format") (v "0.8.0") (h "0rjqvmn00ahgh683qva5x01ldaxp9dgrzmvwrh33qpajv9bk3nqn")))

(define-public crate-astro-format-1.0.0 (c (n "astro-format") (v "1.0.0") (h "1vzli2qdmwvhh9nkk8arrd4a54yacizj1r316jwd2zy74hxmmcvj")))

(define-public crate-astro-format-1.1.0 (c (n "astro-format") (v "1.1.0") (h "04jrlnjg7ga5vhjgl8j5b3p5qn0i52wz0bjss6pv2sf4fg2q7gjk")))

(define-public crate-astro-format-1.2.0 (c (n "astro-format") (v "1.2.0") (h "0xr4882d03w7s7iqc00nqyhzpsghy6420jccrs5i0vx4ma7f2xk6")))

(define-public crate-astro-format-2.0.0 (c (n "astro-format") (v "2.0.0") (h "178b5wfrcpz66qg70xz4hn2zn5vmmjbyd1gchi7cs6n7zd2s5k21")))

(define-public crate-astro-format-2.1.0 (c (n "astro-format") (v "2.1.0") (h "0bv3amv22wcj5scy0m3k8594fdy8cq3n0jlgzyi8hv7wsmw2sicr")))

(define-public crate-astro-format-2.1.1 (c (n "astro-format") (v "2.1.1") (h "1005a2lviaa0vagnmf8aibpnayyrwbjwk8657jbl9bwj2c8ml6gb")))

