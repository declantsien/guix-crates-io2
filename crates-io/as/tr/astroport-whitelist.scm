(define-module (crates-io as tr astroport-whitelist) #:use-module (crates-io))

(define-public crate-astroport-whitelist-1.0.1 (c (n "astroport-whitelist") (v "1.0.1") (d (list (d (n "astroport") (r "^2.8") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw1-whitelist") (r "^0.15") (f (quote ("library"))) (d #t) (k 0)) (d (n "cw2") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kdm3h493np3qg8x7y680xfz8y0l1n4npf6r1394ngdbpxbnf5a4") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

