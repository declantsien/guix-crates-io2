(define-module (crates-io as tr astreuos-accounts) #:use-module (crates-io))

(define-public crate-astreuos-accounts-0.1.0 (c (n "astreuos-accounts") (v "0.1.0") (d (list (d (n "astreuos-transaction") (r "^0.2.0") (d #t) (k 0)) (d (n "astro-format") (r "^0.4.0") (d #t) (k 0)) (d (n "fides") (r "^2.2.1") (d #t) (k 0)) (d (n "opis") (r "^3.0.7") (d #t) (k 0)))) (h "15vxy3dd0m7wqlq3sbk7wm4dk61mwwna5krc1wdr6xc6szw2rn2z") (y #t)))

