(define-module (crates-io as tr astral-math) #:use-module (crates-io))

(define-public crate-astral-math-0.0.1 (c (n "astral-math") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "18i0kinkf0898hvxq0hvf1l03n2vqg18zwkrc9bj23pscqqqycq3")))

(define-public crate-astral-math-0.0.2 (c (n "astral-math") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "191d8lk2kxaijpn11bq5znjnyavjzy4sy9r6w4hbsmnlbbnxldsm")))

(define-public crate-astral-math-0.0.3 (c (n "astral-math") (v "0.0.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "17qilcqmrwyal2by1px9l6q2i9yq4bmgxg76l2b4gwch6m5cpsxa")))

