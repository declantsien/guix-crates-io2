(define-module (crates-io as tr astro4nit) #:use-module (crates-io))

(define-public crate-astro4nit-0.0.1 (c (n "astro4nit") (v "0.0.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.2") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.8.0") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw20") (r "^0.8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (d #t) (k 0)))) (h "1df61g57vlckyl47d4rwqq83fw511avxw4hd97kwxj4kszdjs9fk") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

