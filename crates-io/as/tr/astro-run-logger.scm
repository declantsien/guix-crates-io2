(define-module (crates-io as tr astro-run-logger) #:use-module (crates-io))

(define-public crate-astro-run-logger-0.1.0 (c (n "astro-run-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "18sshh6shvl1v3pb0c186k7611m34126lyqlr3wnyzs6dqd2a515")))

(define-public crate-astro-run-logger-0.1.1 (c (n "astro-run-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ayxnj9b831jzb5kvw04pplnk1klk19giqq5w504z5qlk5zhl8n4")))

(define-public crate-astro-run-logger-0.1.2 (c (n "astro-run-logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "06jn74xx7d8h4idmlh1iimimwz52h5b6m964aj2zfp9dd3wdfp12")))

(define-public crate-astro-run-logger-0.1.3 (c (n "astro-run-logger") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "17fxz1kz1pv1mdml9pg6i2xrcgm6wnbfz06m5hkk7cv13pinbs5l")))

