(define-module (crates-io as tr astral) #:use-module (crates-io))

(define-public crate-astral-0.1.0 (c (n "astral") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0bmnl9g8c7m4icr4ggbpq7nlarbyk8h2hi859g5vliqjdmqvdmg8")))

(define-public crate-astral-0.2.0 (c (n "astral") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "vsop87") (r "^2.0.3") (d #t) (k 0)))) (h "04nqyjpxr8gg7f2091fgx5d2wby6vwnn6qjgvbdb4sdlax5v1nns")))

(define-public crate-astral-0.3.0 (c (n "astral") (v "0.3.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "vsop87") (r "^2.0.3") (d #t) (k 0)))) (h "0fm8ghxbwf7c6942y4smfdngpfq77445djm0lc2z16vmhzmc2mp5")))

(define-public crate-astral-0.3.1 (c (n "astral") (v "0.3.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)) (d (n "astro") (r "^2.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "vsop87") (r "^2.0.3") (d #t) (k 0)))) (h "0dwbpvmdf5zbn0yjfrfzww4k44n3qpazklpsv6jf8qz0rvfjcx9p")))

