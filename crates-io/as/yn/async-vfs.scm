(define-module (crates-io as yn async-vfs) #:use-module (crates-io))

(define-public crate-async-vfs-0.1.0 (c (n "async-vfs") (v "0.1.0") (h "1j03c2c3623aggwr5nz0nvvpkz2a6xpvghzw8x9fn59h4v6jxcp3")))

(define-public crate-async-vfs-0.2.0 (c (n "async-vfs") (v "0.2.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1yjxdx8caqkfwzsw5li86j4ar3qy1wjwgzwbncq9axmhycsglm95")))

(define-public crate-async-vfs-0.3.0 (c (n "async-vfs") (v "0.3.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "080fisyngkljxxkjfxvjl81g9dk39j2722l956fgf2zf756vx9d2")))

(define-public crate-async-vfs-0.4.0 (c (n "async-vfs") (v "0.4.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "06q1jj95kx3p45fim0hjxkbz3gna160l4da8h8jc29zzicfvx0xa")))

(define-public crate-async-vfs-0.5.0 (c (n "async-vfs") (v "0.5.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "04zs41gpl1y1gpy6ya6b2ikjrfjfd06nd9hbfa4wy5ajpyb0zfvi")))

(define-public crate-async-vfs-0.6.0 (c (n "async-vfs") (v "0.6.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0p593isr13ly2r2wv7i1hg4x7n30a7f274s7qwkhvmjsnrxcb8ds")))

(define-public crate-async-vfs-0.7.0 (c (n "async-vfs") (v "0.7.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "10298wj4ky81fbfmbf5f9yz7n10i8brmp7rgs7kxl7l3wnbjw4gn")))

(define-public crate-async-vfs-0.8.0 (c (n "async-vfs") (v "0.8.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0v4navp06hp7xs7hiianpq6m0cffiqcz1sg2jahz835244blfjq4")))

(define-public crate-async-vfs-0.9.0 (c (n "async-vfs") (v "0.9.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1922qxinqmymjmnafwp7bjzjr9h0il9jbhyh0xkhk4wxdgngz189")))

(define-public crate-async-vfs-0.10.0 (c (n "async-vfs") (v "0.10.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1xxqhm7hsh73yg9l16m19303caakbl5vdxs6js6wanpa6aclpdvy")))

(define-public crate-async-vfs-0.11.0 (c (n "async-vfs") (v "0.11.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1whri085d942n680493dqdz8q00rs3x5rx3pfspr890isfxvmvgn")))

(define-public crate-async-vfs-0.15.0 (c (n "async-vfs") (v "0.15.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "13sq1h8m8bxfnyx3qizicphh6v16lgzafj1ymljwcqv2z4yx6mc1")))

(define-public crate-async-vfs-0.20.0 (c (n "async-vfs") (v "0.20.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "063cfrkbqh5swvb06y75x5iq61nmaqp1blp6gmay0vrkmaj5ia9i")))

(define-public crate-async-vfs-0.50.0 (c (n "async-vfs") (v "0.50.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0pnwyzyz01lwkqnr4680s5inkyywcjgrz7dwa1wvi6q6xwi7hhmx")))

(define-public crate-async-vfs-0.52.0 (c (n "async-vfs") (v "0.52.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0cjxjxmsxy2bc1jyr68vcqqdpadw8ddv6ziwl84gvdkd7a350m8l")))

(define-public crate-async-vfs-0.53.0 (c (n "async-vfs") (v "0.53.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "10a2kw55sixmjii3ffg2kl76gxrgl7mv6qkib9lqirz4diapm1kd")))

(define-public crate-async-vfs-0.54.0 (c (n "async-vfs") (v "0.54.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1brqz027n24hzaaswa7qqrb5zwwxfyg54qn0l21b0qwv7qcbfyvm")))

(define-public crate-async-vfs-0.55.0 (c (n "async-vfs") (v "0.55.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1g0zvx0p29rdqd41q85rwrqckh1hvjjdsy9hskny91l56m14gnpn")))

(define-public crate-async-vfs-0.56.0 (c (n "async-vfs") (v "0.56.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1rkysyw0wmbzl8ip0jl4ir2n6ig71sw1yxv0q779d5nd1x5j8hwv")))

