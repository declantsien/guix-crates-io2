(define-module (crates-io as yn async_auto_traits) #:use-module (crates-io))

(define-public crate-async_auto_traits-0.1.0 (c (n "async_auto_traits") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "040c2vki6y92sza4vjl0f9xf5dp7wjagfyw9n9r118iln1yzfarq")))

(define-public crate-async_auto_traits-0.1.1 (c (n "async_auto_traits") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0kpx87b6mgcih3m7gy5798ff9n35lx2y36lvmm4jidl4ar5vcayr")))

(define-public crate-async_auto_traits-0.2.0 (c (n "async_auto_traits") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1h55p8gnhypjcnfrz9s53kvwjafm6z150pcrgw4cwmk33lsl7cyv")))

(define-public crate-async_auto_traits-0.2.1 (c (n "async_auto_traits") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "1v1r6lqmayr4v24hbagdzq9b0n8hzfcfdrf72i60xvb66pav4zi6")))

