(define-module (crates-io as yn async-gen) #:use-module (crates-io))

(define-public crate-async-gen-0.1.0 (c (n "async-gen") (v "0.1.0") (d (list (d (n "async-gen-macros") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "00gv0kfi2sp2kxi8714wxzliq0jw38y0p7wlyh4z57l4xv0kg0dm")))

(define-public crate-async-gen-0.1.1 (c (n "async-gen") (v "0.1.1") (d (list (d (n "async-gen-macros") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "18yazd16rpfcny46dd4fkw4y4xyl1mj29qzz67d6rg172n7ha41f")))

(define-public crate-async-gen-0.2.0 (c (n "async-gen") (v "0.2.0") (d (list (d (n "async-gen-macros") (r "^0.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1d43s795f3mggw3aagm8nxnp4mbpy5f84xqgspabmfdh2a13h0ik")))

(define-public crate-async-gen-0.2.1 (c (n "async-gen") (v "0.2.1") (d (list (d (n "async-gen-macros") (r "^0.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1zzk9h0835qpriwh4ixi13q09rwmmj97izrb7f4778mpj2fbdwlg")))

(define-public crate-async-gen-0.2.2 (c (n "async-gen") (v "0.2.2") (d (list (d (n "async-gen-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "095fk5id9syy4nch70yykaxag402a2xnrx6cxd9bdxp23xh5np21")))

(define-public crate-async-gen-0.2.3 (c (n "async-gen") (v "0.2.3") (d (list (d (n "async-gen-macros") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "04zqppz2kals2rl5sqi8k5r5vz0pixjz447ziahwfv4g96fjv1rg")))

