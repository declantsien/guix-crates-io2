(define-module (crates-io as yn async-port-scanner) #:use-module (crates-io))

(define-public crate-async-port-scanner-0.1.0 (c (n "async-port-scanner") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "10wpa4z7xnpgpb96h12xqv6pb21xcdrv3j0jg42hszxzihfsg5cc") (y #t)))

(define-public crate-async-port-scanner-0.1.1 (c (n "async-port-scanner") (v "0.1.1") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1y4brmmwqmvghswfddqad0dbb5rpyzxgiy0rahl73k7k58ckf4bb")))

(define-public crate-async-port-scanner-0.1.2 (c (n "async-port-scanner") (v "0.1.2") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "01dk0kv8yh5l3mi3gzkk3zvviad1r6rafh6dh7a608ma87hycr7v")))

(define-public crate-async-port-scanner-0.1.3 (c (n "async-port-scanner") (v "0.1.3") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0b100pqpqq9wqkmw5n2h9v0j9by452y1w8y0fj74n2j53444iyry")))

(define-public crate-async-port-scanner-0.1.4 (c (n "async-port-scanner") (v "0.1.4") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0vigaba18gg2hgasr7dpqhnwmb957gfx3bnqwkwv2bg3ll2445zg")))

