(define-module (crates-io as yn async-handle) #:use-module (crates-io))

(define-public crate-async-handle-0.1.0 (c (n "async-handle") (v "0.1.0") (d (list (d (n "async-rwlock") (r "^1") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1xgbxwdmx71b9mdd79vsibqjniyclgjb7i4x1rxv7d6hva4ikn8z")))

(define-public crate-async-handle-0.1.1 (c (n "async-handle") (v "0.1.1") (d (list (d (n "async-rwlock") (r "^1") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1028bf5jd9vdmzqiz4hah3fllr5zgn17x3plmh9wx577i0mcp4y4")))

(define-public crate-async-handle-0.1.2 (c (n "async-handle") (v "0.1.2") (d (list (d (n "async-rwlock") (r "^1") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "19gxhfv4x7kxc059syyvacnpkp7cpjsbs6whrps3r92zl9wpfgfk")))

(define-public crate-async-handle-0.1.3 (c (n "async-handle") (v "0.1.3") (d (list (d (n "async-rwlock") (r "^1") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0ksbh7ajnwfjvgf5d371q2dcc90mbgrkc7v70pijjgfygmv9zy38")))

(define-public crate-async-handle-0.1.4 (c (n "async-handle") (v "0.1.4") (d (list (d (n "async-rwlock") (r "^1") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0qclnl5f7f6irqg4a1y7namkwzvb828b14cj93shs9xa138rfqhf")))

