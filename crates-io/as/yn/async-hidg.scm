(define-module (crates-io as yn async-hidg) #:use-module (crates-io))

(define-public crate-async-hidg-0.2.0 (c (n "async-hidg") (v "0.2.0") (d (list (d (n "async-io") (r "^2") (d #t) (k 0)) (d (n "blocking") (r "^1") (d #t) (k 0)) (d (n "hidg-core") (r "^0.2") (k 0)) (d (n "smol") (r "^2") (d #t) (k 2)) (d (n "smol-potat") (r "^1") (d #t) (k 2)))) (h "1c2wb4z1bdnqy68z1i8y66rj7x5fjjij25mg9n3kjhw7sjhs5kwf") (f (quote (("serde" "hidg-core/serde") ("phf" "hidg-core/phf") ("mouse" "hidg-core/mouse") ("keyboard" "hidg-core/keyboard") ("fromstr" "hidg-core/fromstr") ("either" "hidg-core/either") ("display" "hidg-core/display") ("default" "fromstr" "display" "phf" "keyboard" "mouse"))))))

