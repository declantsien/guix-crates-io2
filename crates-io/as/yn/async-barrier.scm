(define-module (crates-io as yn async-barrier) #:use-module (crates-io))

(define-public crate-async-barrier-0.1.0 (c (n "async-barrier") (v "0.1.0") (h "0iliad8qhsazlafa5ndvpjd6v4zr01xb97w4dz1kjj0301317z2m")))

(define-public crate-async-barrier-1.0.0 (c (n "async-barrier") (v "1.0.0") (d (list (d (n "async-channel") (r "^1.4.1") (d #t) (k 2)) (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "1fv3qpjpqdnxlqldrii4h6lw6hbpvy1m651nd1wmfr7wwa17vqrs")))

(define-public crate-async-barrier-1.0.1 (c (n "async-barrier") (v "1.0.1") (d (list (d (n "async-channel") (r "^1.4.1") (d #t) (k 2)) (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "0an2f3s5567xbyy3bjhd4dn6p78r5ycfarsq2php5svmhrlr6qn0")))

(define-public crate-async-barrier-1.1.0 (c (n "async-barrier") (v "1.1.0") (d (list (d (n "async-channel") (r "^1.4.1") (d #t) (k 2)) (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "0np45qcrnwra59y602177d9vxqi6kqdv073m9qm43sha9pl0zd84")))

