(define-module (crates-io as yn async-dup) #:use-module (crates-io))

(define-public crate-async-dup-0.1.0 (c (n "async-dup") (v "0.1.0") (h "1scx6vzrs2x2fgc7pdnqzdf66niq71g9rdzanzf1fv8qyph08ffj")))

(define-public crate-async-dup-1.0.0 (c (n "async-dup") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "simple-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)))) (h "0hk4qjjmjif8k7lmj7va4jgws2jwly0g57siignlb6hiy6w01mm3")))

(define-public crate-async-dup-1.0.1 (c (n "async-dup") (v "1.0.1") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "simple-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)))) (h "1nq0adspnb79zs876gsv2l32yb5wgiq2mxfa0cs3hg3bagxcn339")))

(define-public crate-async-dup-1.1.0 (c (n "async-dup") (v "1.1.0") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "simple-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)))) (h "18dm2mx88pmcfx0bvx6g047n1m1bbsp9lxmfsaxwhwfdy1y2g5y8")))

(define-public crate-async-dup-1.2.0 (c (n "async-dup") (v "1.2.0") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "simple-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)))) (h "1vlpx497zfkc05kxakdmymc976iq3fkcsncfh3aqwkf8f8mp7lx3")))

(define-public crate-async-dup-1.2.1 (c (n "async-dup") (v "1.2.1") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "simple-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)))) (h "06m5i1k14x5npg7jahqmcbj0nw30gzmdhvzm85hmhpwgv9mdsfy2")))

(define-public crate-async-dup-1.2.2 (c (n "async-dup") (v "1.2.2") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "simple-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)))) (h "0z3grxarv9wpck6jm31qayib9barf12a47gvii9934n0ilms29vl")))

(define-public crate-async-dup-1.2.3 (c (n "async-dup") (v "1.2.3") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "smol") (r "^1") (d #t) (k 2)))) (h "10spih11imsdpa9x5ac7zrwbw1iw42aaz3g0z6bp2krdi99r8pc6") (y #t) (r "1.41")))

(define-public crate-async-dup-1.2.4 (c (n "async-dup") (v "1.2.4") (d (list (d (n "async-lock") (r "^3.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "futures-io") (r "^0.3.5") (d #t) (k 0)) (d (n "smol") (r "^1") (d #t) (k 2)))) (h "0484raf2r5lj97b8skkric9ysivrp3bns0gcg67h7x9sasmqca3w") (r "1.59")))

