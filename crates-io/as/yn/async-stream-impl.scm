(define-module (crates-io as yn async-stream-impl) #:use-module (crates-io))

(define-public crate-async-stream-impl-0.1.0 (c (n "async-stream-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0hdn0r686ha8w5h7b002dd9sv5hbalja7df27zszqd8vfikasi1b")))

(define-public crate-async-stream-impl-0.1.1 (c (n "async-stream-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "03kyfkb9cyip1z5ssj013xmp55a7qpzar2rq0kxxqdhy85dqq3ag")))

(define-public crate-async-stream-impl-0.2.0 (c (n "async-stream-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "101lac7iv7svklh8qk95pl1ap0rvcf9fbpc6qh5gyyxak7nmccrr")))

(define-public crate-async-stream-impl-0.2.1 (c (n "async-stream-impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0w26845j6l731b1y4i07wll3jj9vadkr2r6cwpvhp1xg70xxpy95")))

(define-public crate-async-stream-impl-0.3.0 (c (n "async-stream-impl") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0w0aif9aw103b5wrm1svkqdh7aaihjywa21819d8m3lzzj78nm53")))

(define-public crate-async-stream-impl-0.3.1 (c (n "async-stream-impl") (v "0.3.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0h0kvac5fbhnkjvx858ms0p8fsvdg3wg13ls6brn1h3m4jjln4yv")))

(define-public crate-async-stream-impl-0.3.2 (c (n "async-stream-impl") (v "0.3.2") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "024k57iwmmhzvbzacxlkssh45cqlnb8xjlvlsp60jm6fsb4di3k4")))

(define-public crate-async-stream-impl-0.3.3 (c (n "async-stream-impl") (v "0.3.3") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "09xy4ryvh8qdj3da4vascb9g69psj0wpc8nxnqpzl7d7fgdh7whh") (r "1.45")))

(define-public crate-async-stream-impl-0.3.4 (c (n "async-stream-impl") (v "0.3.4") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0rg98wlv5xvy38v0m7mx7k2jndgiyddhqy3g2m4z3kdhlzhmlrg4") (r "1.45")))

(define-public crate-async-stream-impl-0.3.5 (c (n "async-stream-impl") (v "0.3.5") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "14q179j4y8p2z1d0ic6aqgy9fhwz8p9cai1ia8kpw4bw7q12mrhn") (r "1.56")))

