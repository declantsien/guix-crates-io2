(define-module (crates-io as yn async-cuda-npp) #:use-module (crates-io))

(define-public crate-async-cuda-npp-0.1.0 (c (n "async-cuda-npp") (v "0.1.0") (d (list (d (n "async-cuda-core") (r "^0.1.0") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "test-util" "time"))) (k 2)))) (h "1x7yc5izv0qwm6giaa0qzm5as53f2638rg7jklwp66kbd7wdzvpn") (f (quote (("unstable"))))))

(define-public crate-async-cuda-npp-0.1.1 (c (n "async-cuda-npp") (v "0.1.1") (d (list (d (n "async-cuda-core") (r "^0.1.1") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "test-util" "time"))) (k 2)))) (h "0cz5a9mq50jvdy7gl0idnlg3fvpyz0v2aj5c9w81yg5m4qrg46v6") (f (quote (("unstable"))))))

(define-public crate-async-cuda-npp-0.1.3 (c (n "async-cuda-npp") (v "0.1.3") (d (list (d (n "async-cuda-core") (r "^0.1.3") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "test-util" "time"))) (k 2)))) (h "1vckgnc76d1lpzji18grg7799896c7y9z6fgaz1ivj5l37rp9acb") (f (quote (("unstable"))))))

(define-public crate-async-cuda-npp-0.2.0 (c (n "async-cuda-npp") (v "0.2.0") (d (list (d (n "async-cuda-core") (r "^0.2.0") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "test-util" "time"))) (k 2)))) (h "09gpd6zjfc25dxqhc0qkda57d6cmfm1w43a7k75mj5k9s2wrw5ic") (f (quote (("unstable"))))))

(define-public crate-async-cuda-npp-0.3.0 (c (n "async-cuda-npp") (v "0.3.0") (d (list (d (n "async-cuda-core") (r "^0.3.0") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "test-util" "time"))) (k 2)))) (h "18fzlyli06zj097vdiqjac0xsa7mn5znm7kwv6rqyk4k3jdrgvcg") (f (quote (("unstable"))))))

(define-public crate-async-cuda-npp-0.4.0 (c (n "async-cuda-npp") (v "0.4.0") (d (list (d (n "async-cuda-core") (r "^0.4.0") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "test-util" "time"))) (k 2)))) (h "14vi40jj06vjxsxi2ys8400q9css9wzgwk6kvm6nwqzrl7x3kdil") (f (quote (("unstable"))))))

