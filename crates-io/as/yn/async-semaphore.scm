(define-module (crates-io as yn async-semaphore) #:use-module (crates-io))

(define-public crate-async-semaphore-0.1.0 (c (n "async-semaphore") (v "0.1.0") (h "1z3i1qkkzlwck30j125yw65y5krxjks6qbbis8439nb785ywcb3p")))

(define-public crate-async-semaphore-1.0.0 (c (n "async-semaphore") (v "1.0.0") (d (list (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "12h6mlmybdcqfxhf3lvip7pwv1krxknsjs8qg18c8318w7y4byk4")))

(define-public crate-async-semaphore-1.1.0 (c (n "async-semaphore") (v "1.1.0") (d (list (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "1vpjnbsqkbj62skvmjh7nnydifsrn26np030jil62213fdcvxlk6")))

(define-public crate-async-semaphore-1.2.0 (c (n "async-semaphore") (v "1.2.0") (d (list (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "1xnkyi5lphsrsklw6pif7q1lpa3dzd580ly1mvpzsvzbhmp7b32k")))

