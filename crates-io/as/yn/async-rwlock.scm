(define-module (crates-io as yn async-rwlock) #:use-module (crates-io))

(define-public crate-async-rwlock-0.1.0 (c (n "async-rwlock") (v "0.1.0") (h "072m08aqqb9fchqkxzphfcx3xn724p43v3yvinq2aj1a04bvmlia")))

(define-public crate-async-rwlock-1.0.0 (c (n "async-rwlock") (v "1.0.0") (d (list (d (n "async-channel") (r "^1.4.0") (d #t) (k 2)) (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "event-listener") (r "^2.3.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.4") (d #t) (k 2)) (d (n "futures-lite") (r "^0.1.11") (d #t) (k 2)))) (h "169sv2lj6hvkf08j4s9yljs038g85b5pq2l02499ry0mxz2js9vn")))

(define-public crate-async-rwlock-1.0.1 (c (n "async-rwlock") (v "1.0.1") (d (list (d (n "async-channel") (r "^1.4.0") (d #t) (k 2)) (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "event-listener") (r "^2.3.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.4") (d #t) (k 2)) (d (n "futures-lite") (r "^0.1.11") (d #t) (k 2)))) (h "0pydhqm03lq3j8s05nvh0y08bd92r45hidz4wx0m9lfjbk3pq5cz")))

(define-public crate-async-rwlock-1.1.0 (c (n "async-rwlock") (v "1.1.0") (d (list (d (n "async-channel") (r "^1.4.1") (d #t) (k 2)) (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.4") (d #t) (k 2)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "0d3z1n9w3l9fx6hgrhvhp6yzja74zcdvz887v9vm32q0msspi2bg")))

(define-public crate-async-rwlock-1.2.0 (c (n "async-rwlock") (v "1.2.0") (d (list (d (n "async-channel") (r "^1.4.1") (d #t) (k 2)) (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.4") (d #t) (k 2)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "0786imjcbnfy3phj9jbpg95xc1p5xq7b8pzc14f2xv00kk68xk4a")))

(define-public crate-async-rwlock-1.2.1 (c (n "async-rwlock") (v "1.2.1") (d (list (d (n "async-channel") (r "^1.4.1") (d #t) (k 2)) (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.4") (d #t) (k 2)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "08ia2ixxmrf1042mmjb2kq00kby71ky47mxxrh8icawchb01qsw0")))

(define-public crate-async-rwlock-1.3.0 (c (n "async-rwlock") (v "1.3.0") (d (list (d (n "async-channel") (r "^1.4.1") (d #t) (k 2)) (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.4") (d #t) (k 2)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "0v4hlfpgbfgbpgxbz0afqjgyy7lv3781cvmsc0kygacvqgf06616")))

