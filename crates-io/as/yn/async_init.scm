(define-module (crates-io as yn async_init) #:use-module (crates-io))

(define-public crate-async_init-0.1.0 (c (n "async_init") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "02nffadl31sjq2b5s6fqp44dbw4xvpx5pd7gbysjyifclz5aajyz")))

