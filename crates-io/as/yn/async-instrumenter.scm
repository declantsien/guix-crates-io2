(define-module (crates-io as yn async-instrumenter) #:use-module (crates-io))

(define-public crate-async-instrumenter-0.1.0 (c (n "async-instrumenter") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "0f4fbbf2xqv475zq7yhd9l8igxpikrsfv8jar51myqs3lfmbnx12")))

(define-public crate-async-instrumenter-0.1.1 (c (n "async-instrumenter") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "0hp84gyksf8wwww7km39y749hhfbivan9104wl3zz38jayabm7my")))

(define-public crate-async-instrumenter-0.1.2 (c (n "async-instrumenter") (v "0.1.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "0w3l9hl7mihd5asx4rhrmy30fzni0ksbzmwpr22c083nvivr3lqn")))

(define-public crate-async-instrumenter-0.1.3 (c (n "async-instrumenter") (v "0.1.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "19l6lq38f0qx07zfhc5qr25863zn9prs1mrckmj10v39hz6nci5b")))

(define-public crate-async-instrumenter-0.1.4 (c (n "async-instrumenter") (v "0.1.4") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "13r5dk0w7ljnlsk7r3kskfw859hwrq8lfjnzvsr4x6bqh9yd9qzw")))

