(define-module (crates-io as yn async-pipe) #:use-module (crates-io))

(define-public crate-async-pipe-0.1.0 (c (n "async-pipe") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1zni6zbiwshfa6sg4gg1lsrc17w3i0z3g492b2nqanfyifrfi5m1")))

(define-public crate-async-pipe-0.1.1 (c (n "async-pipe") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1289maqwz35fcjh86spnxbimd4vy86dbhri0jfl6whvcxfglsrs7")))

(define-public crate-async-pipe-0.1.2 (c (n "async-pipe") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "14r9dsl9j118dml25q00ipg5gcbx8l2s4zhf34984rcm5vdm122v")))

(define-public crate-async-pipe-0.1.3 (c (n "async-pipe") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "05b2av0iqr2gl2nc0v78nyicg3cda717mrfjkkjyqm9pcvpyj19j")))

