(define-module (crates-io as yn async-std-test) #:use-module (crates-io))

(define-public crate-async-std-test-0.0.1 (c (n "async-std-test") (v "0.0.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1x92b9zdazp8wnp85yrm32vjd7ad9p6g6m2k6an494wnkx7r8npf")))

(define-public crate-async-std-test-0.0.2 (c (n "async-std-test") (v "0.0.2") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "101rczi7wldf6ibvl4lb5rgb7wx8mamc372w751z41c35qsn3ril")))

(define-public crate-async-std-test-0.0.3 (c (n "async-std-test") (v "0.0.3") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ww14bz5ij1zfmlky4x38xp61ncy8s6in947jv5gyjk0007f6pyl")))

(define-public crate-async-std-test-0.0.4 (c (n "async-std-test") (v "0.0.4") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05g4l21qk19bi4z4qbrazz3bxsp8vwn90vmz621alnb2a0fw6x95")))

