(define-module (crates-io as yn async-httype) #:use-module (crates-io))

(define-public crate-async-httype-0.1.0 (c (n "async-httype") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0149pw4s03cn6q25yxynfr37c5j0f250a90dxm7iad4xfsf7mi33")))

(define-public crate-async-httype-0.2.0 (c (n "async-httype") (v "0.2.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0d5s02yphdjfh21b586g9v66is70mf6mm1qzrl8i4cl85ln9jxal")))

(define-public crate-async-httype-0.2.1 (c (n "async-httype") (v "0.2.1") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0dybf5awvh5ypi620sb0ynd9f1kni9xvvll06nxlf2akg208m4fr")))

(define-public crate-async-httype-0.2.2 (c (n "async-httype") (v "0.2.2") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "07p6zgz8pl4rg0i5mxcmqwj65i0367xb783ssm41vvz7jkk3plsg")))

(define-public crate-async-httype-0.2.3 (c (n "async-httype") (v "0.2.3") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "02jpdhxcx3w463hrc4k4a0by3g9d9mbp4hv6vcdh1dmf12alrsn8")))

(define-public crate-async-httype-0.2.4 (c (n "async-httype") (v "0.2.4") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "1j7b2w663pda8dl1v7l5fls9swbb5a89v32c9lkvqi3r4g382k10")))

