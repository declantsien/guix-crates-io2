(define-module (crates-io as yn async-gitlib) #:use-module (crates-io))

(define-public crate-async-gitlib-0.0.1 (c (n "async-gitlib") (v "0.0.1") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "git2") (r "^0.13.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0f23xfcsfg12grh6z382n8n7647nvpgbd2l2rixwz4r9hqgn9bjr")))

(define-public crate-async-gitlib-0.0.2 (c (n "async-gitlib") (v "0.0.2") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "git2") (r "^0.13.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1dp1p613v0jdc38bh3khccf8h0viwm2gd9rbnjw5j93rdrcdi1yc")))

