(define-module (crates-io as yn async-ctrlc) #:use-module (crates-io))

(define-public crate-async-ctrlc-1.0.0 (c (n "async-ctrlc") (v "1.0.0") (d (list (d (n "async-std") (r "^1.3.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 2)))) (h "0baa65jy3jnd6l6xr62h5492m4g2i9161gfmrh3pgmlqrhnig6nm") (f (quote (("termination" "ctrlc/termination"))))))

(define-public crate-async-ctrlc-1.1.0 (c (n "async-ctrlc") (v "1.1.0") (d (list (d (n "async-std") (r "^1.3.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (o #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 2)))) (h "1fg9ag77pb1yx8nls4a7c8bg913383yaqbi4ysbf1k3p5416lkfd") (f (quote (("termination" "ctrlc/termination") ("stream" "futures-core"))))))

(define-public crate-async-ctrlc-1.2.0 (c (n "async-ctrlc") (v "1.2.0") (d (list (d (n "async-std") (r "^1.3.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (o #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 2)))) (h "1w9dxna5c4ky5z500xqcq6kivwh8hcg7295cgknchl8sx7v7jwlh") (f (quote (("termination" "ctrlc/termination") ("stream" "futures-core"))))))

