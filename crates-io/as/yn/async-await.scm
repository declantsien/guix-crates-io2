(define-module (crates-io as yn async-await) #:use-module (crates-io))

(define-public crate-async-await-0.1.0 (c (n "async-await") (v "0.1.0") (d (list (d (n "eventual") (r "0.1.*") (d #t) (k 0)))) (h "052i1cxgvn6b2r45g0m9v94135bvnn1khby4naha14l3v2bsqndy")))

(define-public crate-async-await-0.1.1 (c (n "async-await") (v "0.1.1") (d (list (d (n "eventual") (r "0.1.*") (d #t) (k 0)))) (h "165wgjm04hkygb4js8bm9s6bgbfbw06a1167qacx794dni7fmdz7")))

(define-public crate-async-await-0.1.2 (c (n "async-await") (v "0.1.2") (d (list (d (n "eventual") (r "^0.1.5") (d #t) (k 0)))) (h "1jh1r5hpd0rchcik7zjr38i3vrg52m2pxs2gpqlmb6rxxzgs4rw6")))

(define-public crate-async-await-0.2.0 (c (n "async-await") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.1") (d #t) (k 0)))) (h "1cz2hsh3wkqv8rz2a5azv25miaaqxl3v1flqdm4wbscjxadbs70z")))

(define-public crate-async-await-0.2.1 (c (n "async-await") (v "0.2.1") (d (list (d (n "futures") (r "^0.1.1") (d #t) (k 0)))) (h "10wbblbdnwz5n9jnrm1sbg0j28dmxkabn7gh484gp1yzx94zmd0x")))

