(define-module (crates-io as yn async-reply-derive) #:use-module (crates-io))

(define-public crate-async-reply-derive-0.1.0 (c (n "async-reply-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wi03z8vch5bc8k70qhxdcrccgswg56fb5lcjr6dkd793a2i6dfi")))

(define-public crate-async-reply-derive-0.1.1 (c (n "async-reply-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qv2ynx0v1xdfj7aqczf6j416a0bgsl2bmr5zmnr9mlgiz3jccci")))

(define-public crate-async-reply-derive-0.1.2 (c (n "async-reply-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06q6j9af4n2clmd7b8ashs1gjx9wr80hhs2zjbcb1nwckz585y40")))

