(define-module (crates-io as yn asynchron) #:use-module (crates-io))

(define-public crate-asynchron-0.1.0 (c (n "asynchron") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "08ifw60dqdkkl0blfhn58zvrk8d2nxj8rrj8jf76w6h6zlk7kd08") (y #t)))

(define-public crate-asynchron-0.1.1 (c (n "asynchron") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0kidgcnchqldz0m20l23w38qw5dps3k3ai4ynq7b7hc2njndy9mv") (y #t)))

(define-public crate-asynchron-0.1.2 (c (n "asynchron") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1kqghh2sfxf1wa8dhm8mrfrma0c1v3rvlqbj6wi7p5m6q1vrb4xy") (y #t)))

(define-public crate-asynchron-0.1.3 (c (n "asynchron") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1dsr29vfv1wz6acvg2c6sh8ap9dgxkcw45zwmxs4lfsfm5w56dcv") (y #t)))

(define-public crate-asynchron-0.1.4 (c (n "asynchron") (v "0.1.4") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1n0jvm2hrf55avdz9313ll2qx730di3q5nip74d50ff5s9ncly0i") (y #t)))

(define-public crate-asynchron-0.1.5 (c (n "asynchron") (v "0.1.5") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0si3jgzdva1wgglgaaqdmzdk1g4r8yx31i92wv8lhh4xxbkycz2k") (y #t)))

(define-public crate-asynchron-0.1.6 (c (n "asynchron") (v "0.1.6") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1shaw9y8xn0ahyg4bp8yjcf7ms5ms31gj5gkn6yif5d7lp14v89a") (y #t)))

(define-public crate-asynchron-0.1.7 (c (n "asynchron") (v "0.1.7") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "04gsv2jm27d0wz45jh4x26a8dhgd8vxp5q7l7w45asmfy6w1fzwf") (y #t)))

(define-public crate-asynchron-0.2.0 (c (n "asynchron") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "01svb1g3wsbsr8756a4rxypsdgg0r4pzr81108sblsw7b6kyml4i") (f (quote (("blocking")))) (y #t)))

(define-public crate-asynchron-0.3.0 (c (n "asynchron") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1m633gximsh74m0civmag6m0ajzf14004zc784drgh3pfjdkqkrj") (f (quote (("blocking")))) (y #t)))

(define-public crate-asynchron-0.4.0 (c (n "asynchron") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0wkzx0p5j8dijnwdqhihwffcjrqisbn19w91rj473lp8ikvnk6y7") (y #t)))

(define-public crate-asynchron-0.4.1 (c (n "asynchron") (v "0.4.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1z4hnbpajg9pvrvqlj8ji6sfl29ia3z7vb5qhg1kqy3s2hh5b0d8") (y #t)))

(define-public crate-asynchron-0.5.0 (c (n "asynchron") (v "0.5.0") (h "035zqzrvzc00qldyhhkz1fc3k0ss4p7qyq2lk7s61y2in6qjsa1r") (y #t)))

(define-public crate-asynchron-0.5.1 (c (n "asynchron") (v "0.5.1") (h "1p4xmyipqsw1yp1aj6k63d8fmkfw3yr8kxl3800qvnsgxvdcbs2h") (y #t)))

(define-public crate-asynchron-0.5.5 (c (n "asynchron") (v "0.5.5") (h "11klw3b76lvpn20b9x4wy6wplb8ckxsgdziv91sf6q0vinshmdxl") (y #t)))

(define-public crate-asynchron-0.5.6 (c (n "asynchron") (v "0.5.6") (h "1ak7rbvkim36pxf7sq81ddlyckkmg6j0af8ljvdi1alm29may3ac") (y #t)))

(define-public crate-asynchron-0.6.0 (c (n "asynchron") (v "0.6.0") (h "0rmz0inzfxx4n94gr6ciq569v5i1r2rxa5hip43bm3q8s153dca3") (y #t)))

(define-public crate-asynchron-0.7.0 (c (n "asynchron") (v "0.7.0") (h "0nzn0525scv3miv8cxnd4j097a479hqli0p8j2k967ssf95vjwfx") (y #t)))

(define-public crate-asynchron-0.7.6 (c (n "asynchron") (v "0.7.6") (h "1yrbqg6vm4185x2pqrpxy0qb34cal2z4nii9gvhirai94qlb8wv4") (y #t)))

(define-public crate-asynchron-0.7.7 (c (n "asynchron") (v "0.7.7") (h "0gxs1vk8myxcqaz2hd0pz8r6higih6sbgv1xsf39szd8h5v63f10") (y #t)))

(define-public crate-asynchron-0.8.0 (c (n "asynchron") (v "0.8.0") (h "1p2n5gmjr0iy8rj9jna0k6bqiidnj208a1r04v12irpndzrfsa9f") (y #t)))

(define-public crate-asynchron-0.8.1 (c (n "asynchron") (v "0.8.1") (h "054jc3j3mcr3c4byjpdxqh5x0xm13s192xl7anrpmisn0vwk3042") (y #t)))

(define-public crate-asynchron-0.8.2 (c (n "asynchron") (v "0.8.2") (h "0gaf6076n1p5rblfmnvdq3md34jpza7yi85qs1ck32ydfmr7yh53") (y #t)))

(define-public crate-asynchron-0.9.0 (c (n "asynchron") (v "0.9.0") (h "0k84wka84741hr9swx4yyiz5w8pliyf06y2vc4gb5pqr7mkvg11f") (y #t)))

(define-public crate-asynchron-0.9.1 (c (n "asynchron") (v "0.9.1") (h "1m00n2z1yc5ykf0kya8b8zzy9kznsncjfv1c3cb8l49s8qdihw3p") (y #t)))

