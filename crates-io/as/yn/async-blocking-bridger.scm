(define-module (crates-io as yn async-blocking-bridger) #:use-module (crates-io))

(define-public crate-async-blocking-bridger-0.1.0 (c (n "async-blocking-bridger") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qa9drcp2w60chfcclnc9ykh9zjfkwr9wmmhqdlwhycy0z10rw0h")))

