(define-module (crates-io as yn async-mwmr) #:use-module (crates-io))

(define-public crate-async-mwmr-0.0.0 (c (n "async-mwmr") (v "0.0.0") (h "105qirkmmc6cn80pmdd05i3cwfj36q2qlx06rlk3gijwpaclgsbx")))

(define-public crate-async-mwmr-0.1.0 (c (n "async-mwmr") (v "0.1.0") (d (list (d (n "async-channel") (r "^2") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cheap-clone") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "futures") (r "=0.3.29") (d #t) (k 0)) (d (n "indexmap") (r "^2") (k 0)) (d (n "mwmr-core") (r "^0.1.0") (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1") (f (quote ("use_std"))) (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wmark") (r "^0.1.0") (f (quote ("future" "default"))) (k 0)))) (h "0w5v2a9r0hw9ggjfdml3mbzwkjqcjm7cjba8kxbxjsdwsqckzn84") (f (quote (("test" "rand") ("smol" "wmark/smol") ("default") ("async-std" "wmark/async-std"))))))

