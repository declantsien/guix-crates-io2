(define-module (crates-io as yn async_tasks_state_map) #:use-module (crates-io))

(define-public crate-async_tasks_state_map-0.1.0 (c (n "async_tasks_state_map") (v "0.1.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "1wkis8rvhanq2w88d6mg25as0jxrh121dbjslrgxjx85sndi08mv")))

(define-public crate-async_tasks_state_map-0.1.1 (c (n "async_tasks_state_map") (v "0.1.1") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "0r71fb5q9h0mbd5mdibaaf9p3bv6dgalxyx9ncr696ky8a76jnzl")))

(define-public crate-async_tasks_state_map-0.1.2 (c (n "async_tasks_state_map") (v "0.1.2") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "0hqnn3b1wh51dcvxkrrlgsj3g7ln4kr7rycd5q970qvgsn7nhnsy")))

(define-public crate-async_tasks_state_map-0.2.0 (c (n "async_tasks_state_map") (v "0.2.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "0kjmv3hn5v6l4nkfp8kryxy8vikf6y7vh3jwxybsk1yxjhj4q72c")))

(define-public crate-async_tasks_state_map-1.0.0 (c (n "async_tasks_state_map") (v "1.0.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "1cpa6d1my96kfmx34fbpw16k5rn018viy2s8jci4am4r66aj1hx1") (y #t)))

(define-public crate-async_tasks_state_map-1.0.1 (c (n "async_tasks_state_map") (v "1.0.1") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "0076i6i19c2mnnjpqcidq284z32fjzbi4v7szl0qpvrylpshcg4r")))

