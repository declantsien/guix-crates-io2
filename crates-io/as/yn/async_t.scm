(define-module (crates-io as yn async_t) #:use-module (crates-io))

(define-public crate-async_t-0.1.0 (c (n "async_t") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "async_t_internal") (r "^0.2.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 0)))) (h "1w4krk4dyd43mi3f3xciib30sll6b2pdicy0zpv5hfzb3fhanv3l")))

(define-public crate-async_t-0.2.0 (c (n "async_t") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "async_t_internal") (r "^0.4.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 0)))) (h "0mzshc2mf87a6acfpndxpmjq5s3dr3rn4vr7j6cabz0fpsfhr4ky")))

(define-public crate-async_t-0.2.1 (c (n "async_t") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "async_t_internal") (r "^0.4.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 0)))) (h "076c1hf35s2qg046mbp36h8h6ri64n4adxjkp4hl1s7v3jrd5c6w")))

(define-public crate-async_t-0.6.0 (c (n "async_t") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "async_t_internal") (r "^0.6.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 0)))) (h "1sjgijiha8b0pi1p22lvp2pnq1yzh08c1rfs10iyfxk67bqz95fc")))

(define-public crate-async_t-0.6.1 (c (n "async_t") (v "0.6.1") (d (list (d (n "async-trait") (r "^0.1.56") (o #t) (d #t) (k 0)) (d (n "async_t_internal") (r "^0.6.1") (d #t) (k 0)))) (h "12bbqqpcqb4v7a8g6adh4s4n1b6gwlmc1kjhzqrikkxj9hvfwh1g") (f (quote (("boxed" "async-trait"))))))

(define-public crate-async_t-0.7.0 (c (n "async_t") (v "0.7.0") (d (list (d (n "async-trait") (r "^0.1.56") (o #t) (d #t) (k 0)) (d (n "async_t_internal") (r "^0.7.0") (d #t) (k 0)))) (h "1akycdns87yv10prfwv3sjrpkncfq1my6lkvrnpm8fzpa17ys2p3") (f (quote (("boxed" "async-trait"))))))

