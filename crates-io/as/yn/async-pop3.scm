(define-module (crates-io as yn async-pop3) #:use-module (crates-io))

(define-public crate-async-pop3-0.1.0 (c (n "async-pop3") (v "0.1.0") (d (list (d (n "async-native-tls") (r "^0.3.3") (d #t) (k 0)) (d (n "async-std") (r "^1.6.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.11") (d #t) (k 0)))) (h "18bdsxp54h2sdqpa01i7hgl30frd83hpzn5dnm9757x330dc5dq0")))

