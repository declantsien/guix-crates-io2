(define-module (crates-io as yn async-redis-session) #:use-module (crates-io))

(define-public crate-async-redis-session-0.1.0 (c (n "async-redis-session") (v "0.1.0") (d (list (d (n "async-session") (r "^2.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "redis") (r "^0.16.0") (f (quote ("aio"))) (d #t) (k 0)))) (h "03yx9z22y0jbqrdqvn8fwb7y1zyk8i3rq567zizb0cph470pf1a9")))

(define-public crate-async-redis-session-0.1.1 (c (n "async-redis-session") (v "0.1.1") (d (list (d (n "async-session") (r "^2.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "redis") (r "^0.16.0") (f (quote ("aio"))) (d #t) (k 0)))) (h "0hqd6xmr2j8729yfvyfgmyrp6m5bialv8paiirk68059j4ljhhri")))

(define-public crate-async-redis-session-0.2.0 (c (n "async-redis-session") (v "0.2.0") (d (list (d (n "async-session") (r "^2.0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "redis") (r "^0.20.1") (f (quote ("aio" "async-std-comp"))) (d #t) (k 0)))) (h "00lfd9b2fhk2hmrwssrgq0184m7i3azd05574dk4g1n0f6axxx7b") (y #t)))

(define-public crate-async-redis-session-0.2.1 (c (n "async-redis-session") (v "0.2.1") (d (list (d (n "async-session") (r "^2.0.1") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "redis") (r "^0.20.1") (f (quote ("aio" "async-std-comp"))) (d #t) (k 0)))) (h "1aj0kk3ia9mzd9vin59287wp8z94sw5wgzjwc2hf7ipdn13qrz7z")))

(define-public crate-async-redis-session-0.2.2 (c (n "async-redis-session") (v "0.2.2") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "redis") (r "^0.20.1") (f (quote ("aio" "async-std-comp"))) (d #t) (k 0)))) (h "0li44iy75qiswxm49zxshfib7mmxhalg8kk0fj05kpkc3q8cx0ms")))

