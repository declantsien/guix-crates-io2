(define-module (crates-io as yn async_ach-cell) #:use-module (crates-io))

(define-public crate-async_ach-cell-0.1.0 (c (n "async_ach-cell") (v "0.1.0") (d (list (d (n "ach-cell") (r "^0.1") (d #t) (k 0)) (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "0f6h6zbsfrb6ipz55j2s5qqpvh88rcich13v3gr92wzvh1ksjl48")))

(define-public crate-async_ach-cell-0.1.1 (c (n "async_ach-cell") (v "0.1.1") (d (list (d (n "ach-cell") (r "^0.1") (d #t) (k 0)) (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "1mrsnwjfm14da57b928z00fbdsr6l0cajl0vz5phk4h3anyrm19a")))

(define-public crate-async_ach-cell-0.1.2 (c (n "async_ach-cell") (v "0.1.2") (d (list (d (n "ach-cell") (r "^0.1") (d #t) (k 0)) (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "1r42rg1g8hdszjl4cfjxj4rnj3bj2j1c7mir52ybk40093ybyqw8")))

