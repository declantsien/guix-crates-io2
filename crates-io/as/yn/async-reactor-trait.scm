(define-module (crates-io as yn async-reactor-trait) #:use-module (crates-io))

(define-public crate-async-reactor-trait-0.0.0 (c (n "async-reactor-trait") (v "0.0.0") (d (list (d (n "async-io") (r "^1.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "reactor-trait") (r "^0.0") (d #t) (k 0)))) (h "0dgkj5ci03r9z8mr9dk6k7324bzafa79n298036yszfq28lvyzmv")))

(define-public crate-async-reactor-trait-0.1.0 (c (n "async-reactor-trait") (v "0.1.0") (d (list (d (n "async-io") (r "^1.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "reactor-trait") (r "^0.1") (d #t) (k 0)))) (h "0c6w1za5bnv3mc122jww5yrs6h3qvp01gs4n77p1hw10paclzrn3")))

(define-public crate-async-reactor-trait-0.2.0 (c (n "async-reactor-trait") (v "0.2.0") (d (list (d (n "async-io") (r "^1.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "reactor-trait") (r "^0.2") (d #t) (k 0)))) (h "0i54ag6qh5fb7biwb2qx6h6cb0cdsdavs3ip4pwmr8x8qjavnrdr")))

(define-public crate-async-reactor-trait-1.0.0 (c (n "async-reactor-trait") (v "1.0.0") (d (list (d (n "async-io") (r "^1.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "reactor-trait") (r "^1.0") (d #t) (k 0)))) (h "0zj2vwy4zvpsacrhly6c6d4amzbxwwybf7hxqrdys88h61rmxd13")))

(define-public crate-async-reactor-trait-1.0.1 (c (n "async-reactor-trait") (v "1.0.1") (d (list (d (n "async-io") (r "^1.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "reactor-trait") (r "^1.0") (d #t) (k 0)))) (h "190kd4kwrz3nb715hwfrcc25zyzsqjl3h2rvmkxix4fypi4ad5gc")))

(define-public crate-async-reactor-trait-1.1.0 (c (n "async-reactor-trait") (v "1.1.0") (d (list (d (n "async-io") (r "^1.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "reactor-trait") (r "^1.1") (d #t) (k 0)))) (h "03nyf53pg8z5w1a44l7cn7g5k0rm5vplldgfr5bdw05df38i4q3s") (r "1.56.0")))

