(define-module (crates-io as yn async_ach-notify) #:use-module (crates-io))

(define-public crate-async_ach-notify-0.1.0 (c (n "async_ach-notify") (v "0.1.0") (d (list (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "07zsw541aagcyxbz7vwxg4gpm108rimi80iznliw8134vw4v44lb")))

(define-public crate-async_ach-notify-0.1.1 (c (n "async_ach-notify") (v "0.1.1") (d (list (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "1g9sxi5qr12px63l1fp4fgg9rd6g2g9wj4rsrgdylickq0ynvhb1")))

(define-public crate-async_ach-notify-0.1.2 (c (n "async_ach-notify") (v "0.1.2") (d (list (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "0y2cfgvjidj8linl9ryhg09y7a8qk5a6ya1qqxl8lp3yiyhf7i7m")))

(define-public crate-async_ach-notify-0.1.3 (c (n "async_ach-notify") (v "0.1.3") (d (list (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "05lscdzjpzrlxccr2adc4p7wikcki4i6i6sn5va39bwzrzqf3pkn")))

(define-public crate-async_ach-notify-0.1.4 (c (n "async_ach-notify") (v "0.1.4") (d (list (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0ip2j7k70d4dn7hjgwj3v7n3zws19vigya7kcvn2v1fgka1wmkjv")))

(define-public crate-async_ach-notify-0.1.5 (c (n "async_ach-notify") (v "0.1.5") (d (list (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "13h5v4p696c0pxl07pm7cmjzpxlxga1mxsnshk1ghinj5y5zaag4")))

(define-public crate-async_ach-notify-0.1.6 (c (n "async_ach-notify") (v "0.1.6") (d (list (d (n "ach-ring") (r "^0.1") (d #t) (k 2)) (d (n "ach-util") (r "^0.1") (d #t) (k 2)) (d (n "async_ach-waker") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0") (f (quote ("async_futures"))) (d #t) (k 2)) (d (n "event-listener") (r "^2") (d #t) (k 2)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 2)))) (h "0860bp11car8qfq6rv7z84ajdc5q3238jxyvv4rijfws14hda5bb")))

