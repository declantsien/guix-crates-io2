(define-module (crates-io as yn async-time-mock-core) #:use-module (crates-io))

(define-public crate-async-time-mock-core-0.0.1 (c (n "async-time-mock-core") (v "0.0.1") (d (list (d (n "async-lock") (r "^2") (d #t) (k 0)) (d (n "event-listener") (r "^2") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1r32fx8bnxv063b3s0wghrjx0y2cs81wlpxlvsj4r06fzjmvcsqc") (r "1.64")))

(define-public crate-async-time-mock-core-0.1.0 (c (n "async-time-mock-core") (v "0.1.0") (d (list (d (n "async-lock") (r "^2") (d #t) (k 0)) (d (n "event-listener") (r "^2") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "16sl6ha7gwdzaxgsmw131f72ilyh2v6dbagvwmp1v19pp7i06hlp") (r "1.64")))

(define-public crate-async-time-mock-core-0.1.1 (c (n "async-time-mock-core") (v "0.1.1") (d (list (d (n "async-lock") (r "^3") (k 0)) (d (n "event-listener") (r "^3") (k 0)) (d (n "futures-lite") (r "^2") (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1q2ldg7wkbh3zffr24iw9jqqz9ynfqf53dzpjlbnpwflqr00vqhi") (r "1.64")))

(define-public crate-async-time-mock-core-0.1.2 (c (n "async-time-mock-core") (v "0.1.2") (d (list (d (n "async-lock") (r "^3") (k 0)) (d (n "event-listener") (r "^4") (k 0)) (d (n "futures-lite") (r "^2") (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1zxs0lsmn4nfc4zpzzrxxdzqkdrdcr2qh1nb2jsii487dhcdc0g7") (r "1.64")))

