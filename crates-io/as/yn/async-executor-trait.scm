(define-module (crates-io as yn async-executor-trait) #:use-module (crates-io))

(define-public crate-async-executor-trait-0.0.0 (c (n "async-executor-trait") (v "0.0.0") (d (list (d (n "async-std") (r "^1.8") (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "executor-trait") (r "^1.0") (d #t) (k 0)))) (h "127ml05mrg858snyb53alyh8dkqjs8qs1kfqmmn77m8zkmxyvlsx") (f (quote (("unstable" "async-std/unstable") ("std" "async-std/std") ("docs" "async-std/docs") ("default" "async-std/default") ("attributes" "async-std/attributes") ("alloc" "async-std/alloc"))))))

(define-public crate-async-executor-trait-1.0.0 (c (n "async-executor-trait") (v "1.0.0") (d (list (d (n "async-std") (r "^1.8") (f (quote ("default" "unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "executor-trait") (r "^1.0") (d #t) (k 0)))) (h "1n2y6yzkmsysx3gffxnfrfl8wsrx57gr450951wsxlkrxndmfvcy") (f (quote (("docs" "async-std/docs") ("attributes" "async-std/attributes"))))))

(define-public crate-async-executor-trait-1.0.1 (c (n "async-executor-trait") (v "1.0.1") (d (list (d (n "async-std") (r "^1.8") (f (quote ("default" "unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "executor-trait") (r "^1.0") (d #t) (k 0)))) (h "15c9bq6q7psb0r0v770bimwkh1a72ilhxjy4plmamza49lcc6f70") (f (quote (("docs" "async-std/docs") ("attributes" "async-std/attributes")))) (y #t)))

(define-public crate-async-executor-trait-1.0.2 (c (n "async-executor-trait") (v "1.0.2") (d (list (d (n "async-std") (r "^1.8") (f (quote ("default" "unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "executor-trait") (r "^1.0") (d #t) (k 0)))) (h "19jxhbq0hxsrx49fgz3lhymlm3981x9zmcqwkcrqycbzm391n1if")))

(define-public crate-async-executor-trait-2.0.0 (c (n "async-executor-trait") (v "2.0.0") (d (list (d (n "async-std") (r "^1.8") (f (quote ("default" "unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "executor-trait") (r "^2.0") (d #t) (k 0)))) (h "1859il7zlhd5bigjpn5dy7yrjnyxz75jiarhwrk2ysmvcv18ah3w") (y #t)))

(define-public crate-async-executor-trait-2.0.1 (c (n "async-executor-trait") (v "2.0.1") (d (list (d (n "async-std") (r "^1.8") (f (quote ("default" "unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "executor-trait") (r "^2.0") (d #t) (k 0)))) (h "1q8vgx1f0dwb3ry85bag62yd73rcj2ycz8mqn5n05qsy99pz59aa")))

(define-public crate-async-executor-trait-2.0.2 (c (n "async-executor-trait") (v "2.0.2") (d (list (d (n "async-std") (r "^1.8") (f (quote ("default" "unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "executor-trait") (r "^2.0") (d #t) (k 0)))) (h "1qhd880wkznjkfqrpb2z34v4xgs4sz0js2l577yb29l4p3vybg63")))

(define-public crate-async-executor-trait-2.1.0 (c (n "async-executor-trait") (v "2.1.0") (d (list (d (n "async-std") (r "^1.8") (f (quote ("default" "unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "executor-trait") (r "^2.1") (d #t) (k 0)))) (h "12f77ks6alyhvqrn51rhjimgabp4zzvppcj80aiz4xz006dajpwa") (r "1.56.0")))

