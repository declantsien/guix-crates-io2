(define-module (crates-io as yn async-rusqlite) #:use-module (crates-io))

(define-public crate-async-rusqlite-0.1.0 (c (n "async-rusqlite") (v "0.1.0") (d (list (d (n "asyncified") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)))) (h "073ms51k3zfyb266iny3f9vnzgi72bz77n3h0b91psxi0ljvrlxr")))

(define-public crate-async-rusqlite-0.3.0 (c (n "async-rusqlite") (v "0.3.0") (d (list (d (n "asyncified") (r "^0.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1x5zi8hhv6khdai2fnp20wl1g56gf37jbwmvsl226gdf4ijqr5s8")))

(define-public crate-async-rusqlite-0.4.0 (c (n "async-rusqlite") (v "0.4.0") (d (list (d (n "asyncified") (r "^0.6.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros" "sync"))) (d #t) (k 2)))) (h "0p9ah7059y5wsji8npdry5y3amw57pfzyk121wax36q4r4ajp8m7")))

