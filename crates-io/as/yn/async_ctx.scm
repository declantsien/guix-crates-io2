(define-module (crates-io as yn async_ctx) #:use-module (crates-io))

(define-public crate-async_ctx-0.1.0 (c (n "async_ctx") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "macros" "rt-threaded"))) (d #t) (k 2)))) (h "197fq5w1qcmv6ydq8lss4n00b676ml2b6i6mnfw14l79fvq4zsw5")))

(define-public crate-async_ctx-0.1.1 (c (n "async_ctx") (v "0.1.1") (d (list (d (n "tokio") (r "^0.2") (f (quote ("time" "macros" "rt-threaded"))) (d #t) (k 2)))) (h "0izjkd5z375cg7p0f3adfk9f3v914cbq34ix2ai7my19mwnwwm69")))

