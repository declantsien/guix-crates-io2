(define-module (crates-io as yn async-time-mock-async-std) #:use-module (crates-io))

(define-public crate-async-time-mock-async-std-0.0.1 (c (n "async-time-mock-async-std") (v "0.0.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-time-mock-core") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0c5rx8krqs8kbxpkvbnxzy9qhs29x34g5p4rdvn9c9v2vbw6mlwy") (f (quote (("interval" "async-std/unstable") ("default" "interval")))) (s 2) (e (quote (("mock" "dep:async-time-mock-core")))) (r "1.64")))

(define-public crate-async-time-mock-async-std-0.1.0 (c (n "async-time-mock-async-std") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-time-mock-core") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "15b1s1fkx8jlvdd53r5k3ylqlpn08rmylz4rxa5ffq104ryi7v2y") (f (quote (("interval" "async-std/unstable") ("default" "interval")))) (s 2) (e (quote (("mock" "dep:async-time-mock-core")))) (r "1.64")))

(define-public crate-async-time-mock-async-std-0.1.1 (c (n "async-time-mock-async-std") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-time-mock-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0m87a4pkxd7rrrs73hbhnihpgdvq96qskndfx03papfc6gbm9jvd") (f (quote (("interval" "async-std/unstable") ("default" "interval")))) (s 2) (e (quote (("mock" "dep:async-time-mock-core")))) (r "1.64")))

