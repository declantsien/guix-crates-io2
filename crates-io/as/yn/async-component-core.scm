(define-module (crates-io as yn async-component-core) #:use-module (crates-io))

(define-public crate-async-component-core-0.4.0 (c (n "async-component-core") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "0v7qqs74jnmhjcym0m8cwii6n0x46314ji52mmridqdjp922z16w")))

(define-public crate-async-component-core-0.4.2 (c (n "async-component-core") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "1211dqrapl5szzvyv8fb7vd1zybfd9xiclb10xbh9nix0wy9anmd")))

(define-public crate-async-component-core-0.5.0 (c (n "async-component-core") (v "0.5.0") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "1his6cwlw1z8xb3y8ggcw63hhl4hrl04rf33r09yivc4pyrizz7c")))

(define-public crate-async-component-core-0.5.1 (c (n "async-component-core") (v "0.5.1") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "0mx5ji8p3w9i9rij51l74b2ksng8psmr7402gkn7iv09q3aclfqm")))

(define-public crate-async-component-core-0.5.2 (c (n "async-component-core") (v "0.5.2") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "1n7a8l2hcc7zsvvias2pac6glwpsll6xhcynzjx9riinndhqvmbd")))

(define-public crate-async-component-core-0.5.3 (c (n "async-component-core") (v "0.5.3") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "10zsiy7cgc92zm3ykvmmrpzdya7hw6855vv4lp06wlxrbj4p53wd")))

(define-public crate-async-component-core-0.5.4 (c (n "async-component-core") (v "0.5.4") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "0i3q0j0hgihaxa4x2mlymgdyjscmar57ysnnkq8s065h3vg51ww6")))

(define-public crate-async-component-core-0.6.0 (c (n "async-component-core") (v "0.6.0") (d (list (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "1mqa9vw4q72c544gm15m84014w0hjr0c10qkbykjjw5n07jj994y")))

(define-public crate-async-component-core-0.7.0 (c (n "async-component-core") (v "0.7.0") (d (list (d (n "atomic-waker") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "081lxk4n5r6jv1znjr43bd7srv9lwq9qj99c2gjv9vd4nzzbzzzq")))

(define-public crate-async-component-core-0.8.0 (c (n "async-component-core") (v "0.8.0") (d (list (d (n "atomic-waker") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "1zd5n8wibws4xb9bbsv1q3p5wgqxf67az03wmc32rn23fgancbyv")))

(define-public crate-async-component-core-0.8.1 (c (n "async-component-core") (v "0.8.1") (d (list (d (n "atomic-waker") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "14a5w58v46h3dy32brqi45crf98hbppnb2qw3npc7m9062sxsqrc")))

(define-public crate-async-component-core-0.9.0 (c (n "async-component-core") (v "0.9.0") (d (list (d (n "atomic-waker") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.25") (d #t) (k 0)))) (h "0s6il63xiwb7hblkxizfh8px8acal88fj9kpxjxia5dd7ljcaw1x")))

