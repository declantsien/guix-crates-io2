(define-module (crates-io as yn async-aria2) #:use-module (crates-io))

(define-public crate-async-aria2-0.1.0 (c (n "async-aria2") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2.8") (f (quote ("process" "io-util" "time" "macros"))) (d #t) (k 0)))) (h "1s5hqq0m229dmms1xdvzmj8gfpy0l0h2wjrx5wl9w9sdrni9f48a")))

(define-public crate-async-aria2-0.1.1 (c (n "async-aria2") (v "0.1.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2.8") (f (quote ("process" "io-util" "time" "macros"))) (d #t) (k 0)))) (h "0si2wkn2j2h1gi5z7icpbwlsc6vng8wmwkd7xdga333g8cr1q8r1")))

(define-public crate-async-aria2-0.1.2 (c (n "async-aria2") (v "0.1.2") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2.9") (f (quote ("process" "io-util" "time" "macros"))) (d #t) (k 0)))) (h "094jvjc88psmmlp18dbrz9p0b4z35p1rj0qc4w7ga2qfkb2cak24")))

