(define-module (crates-io as yn async_ui_web_macros) #:use-module (crates-io))

(define-public crate-async_ui_web_macros-0.2.0 (c (n "async_ui_web_macros") (v "0.2.0") (d (list (d (n "cssparser") (r "=0.31.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1kgsrwlbgm9l3h4fc2w6xzzqf20lp19ps8j5kfbakxzv281fclnr")))

