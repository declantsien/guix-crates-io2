(define-module (crates-io as yn async-ping-cli) #:use-module (crates-io))

(define-public crate-async-ping-cli-0.1.0 (c (n "async-ping-cli") (v "0.1.0") (d (list (d (n "async-ping") (r "^0.1") (d #t) (k 0)) (d (n "icmp-client") (r "^0.1") (f (quote ("impl_tokio"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "1yxcyjxkh4sqrfyf5wmh6mpbm0161cb62nrf6wbppypfq1srrm6m")))

(define-public crate-async-ping-cli-0.2.0 (c (n "async-ping-cli") (v "0.2.0") (d (list (d (n "async-ping") (r "^0.2") (d #t) (k 0)) (d (n "icmp-client") (r "^0.2") (f (quote ("impl_tokio"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "19hj9d19zjvn1r8ihis5jcfq64jzlba7c83h75xd5a5dq0lxr8m8")))

