(define-module (crates-io as yn async-socks5) #:use-module (crates-io))

(define-public crate-async-socks5-0.1.0 (c (n "async-socks5") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("net" "tcp" "udp" "dns" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("net" "tcp" "udp" "dns" "io-util" "macros"))) (d #t) (k 2)))) (h "0q8v4idm9pfsnvyja7ld7slv5xwyh2gbs7dvjy32cyrd9wl2nm3i")))

(define-public crate-async-socks5-0.1.1 (c (n "async-socks5") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("net" "tcp" "udp" "dns" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("net" "tcp" "udp" "dns" "io-util" "macros"))) (d #t) (k 2)))) (h "1wdk8mxs8vwyfh25kpgk1mg23a5glyvad8pyy1az53x0xqlxmqvy")))

(define-public crate-async-socks5-0.2.0 (c (n "async-socks5") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("net" "tcp" "udp" "dns" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("net" "tcp" "udp" "dns" "io-util" "macros"))) (d #t) (k 2)))) (h "03ivc2daani8909xs99vhrk0ig5m8n40sxvyfdmbp1p66jmfwydr")))

(define-public crate-async-socks5-0.2.1 (c (n "async-socks5") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("net" "tcp" "udp" "dns" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("net" "tcp" "udp" "dns" "io-util" "macros"))) (d #t) (k 2)))) (h "0260a9f23dkj5jyblw2l0f26a2clhl0sy8kcpvma6r3i4s9nvkp3")))

(define-public crate-async-socks5-0.3.0 (c (n "async-socks5") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("udp" "dns" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("udp" "dns" "io-util" "tcp" "macros"))) (d #t) (k 2)))) (h "1si2y3wiaa4lps7acjyxzvdhrwihahslvqr8qm22agikmbr0xvsp")))

(define-public crate-async-socks5-0.3.1 (c (n "async-socks5") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("udp" "dns" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("udp" "dns" "io-util" "tcp" "macros"))) (d #t) (k 2)))) (h "179gdsqgz6lqy2izbhwzwgikgh7l8sws8iaavv7mckxn1n4y57b0")))

(define-public crate-async-socks5-0.3.2 (c (n "async-socks5") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("udp" "dns" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.3") (f (quote ("udp" "dns" "io-util" "tcp" "macros"))) (d #t) (k 2)))) (h "1mj2v1ss5x7rlacyfc60cbh80qw595gq5w6ki51grp0as3v66mjp")))

(define-public crate-async-socks5-0.4.0 (c (n "async-socks5") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("net" "io-util" "rt" "macros"))) (d #t) (k 2)))) (h "1sxa04qb37ir7z6aqxwsd3dnrqyf8bmg24jax0dx4ighj23hslaz")))

(define-public crate-async-socks5-0.5.0 (c (n "async-socks5") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "io-util" "rt" "macros"))) (d #t) (k 2)))) (h "1874vbnam9yqrd7i66hvd5zh3axw3kvil9ygcy06v5pg1jfznm1d")))

(define-public crate-async-socks5-0.5.1 (c (n "async-socks5") (v "0.5.1") (d (list (d (n "async-trait") (r "^0.1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "io-util" "rt" "macros"))) (d #t) (k 2)))) (h "1izdpnwzjpj3d75c6gg7f7fzwwqhr9kjlr45yz0v4pj4sank9xkp")))

(define-public crate-async-socks5-0.6.0 (c (n "async-socks5") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("net" "io-util" "rt" "macros"))) (d #t) (k 2)))) (h "128w31m6nvrvgbaycz3838m8fqsn6lxjmr9fjz99csz18rw578ld")))

