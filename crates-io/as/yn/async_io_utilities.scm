(define-module (crates-io as yn async_io_utilities) #:use-module (crates-io))

(define-public crate-async_io_utilities-0.1.0 (c (n "async_io_utilities") (v "0.1.0") (d (list (d (n "tokio") (r "^1.12.0") (d #t) (k 0)))) (h "07k0k5pnhp4a2hy75l53mc9lc8l9nds8qdh4akkdac680dfw7g3i") (y #t)))

(define-public crate-async_io_utilities-0.1.1 (c (n "async_io_utilities") (v "0.1.1") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1sb4drc7snn5839m31v9qylqqharbz4hrpf8ax4ihmwq4yzar0l7") (y #t)))

(define-public crate-async_io_utilities-0.1.2 (c (n "async_io_utilities") (v "0.1.2") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1ph4n9d9rp9czc42kxfj43lw50cvy07qalpkwkv2sjj5ls49dz8b") (y #t)))

(define-public crate-async_io_utilities-0.1.3 (c (n "async_io_utilities") (v "0.1.3") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0phkjb5ar8zrxmy46nm50w0jcmz91qc1xg06fg4q5dpapwm12j02") (y #t)))

(define-public crate-async_io_utilities-0.1.4 (c (n "async_io_utilities") (v "0.1.4") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 2)))) (h "183xzy96n2da3m05nci01p2sksvzb3ma75szy0rvzx4hapycy84v")))

