(define-module (crates-io as yn async-wait-group) #:use-module (crates-io))

(define-public crate-async-wait-group-0.1.0 (c (n "async-wait-group") (v "0.1.0") (h "0yrhgkirh65dfrb8bzmm4b0x14v35wn8p4ss8422124hr5diziwk") (y #t)))

(define-public crate-async-wait-group-0.2.0 (c (n "async-wait-group") (v "0.2.0") (h "199vql7vivb9d1bshdhpx8fkv1xbcyb2vw0w0b82zk9gzk725d5a")))

