(define-module (crates-io as yn async-backplane) #:use-module (crates-io))

(define-public crate-async-backplane-0.0.0 (c (n "async-backplane") (v "0.0.0") (h "0jiclc3z1plpn9kjnprvr50fsvq6lbwlsfsbp1wfwmvlkrlmxb8s")))

(define-public crate-async-backplane-0.1.0 (c (n "async-backplane") (v "0.1.0") (d (list (d (n "atomic-waker") (r "^1.0") (d #t) (k 0)) (d (n "concurrent-queue") (r "^1.1") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.5") (d #t) (k 0)) (d (n "maybe-unwind") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "waker-queue") (r "^0.1") (d #t) (k 0)))) (h "0bfcnrhnlm1fpkb2hsf1a8v5idcyi1mnafnwj328hrab23a1d9d9")))

(define-public crate-async-backplane-0.1.1 (c (n "async-backplane") (v "0.1.1") (d (list (d (n "atomic-waker") (r "^1.0") (d #t) (k 0)) (d (n "concurrent-queue") (r ">=1.2.1, <1.2.2") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1") (d #t) (k 0)) (d (n "futures-micro") (r "^0.2") (d #t) (k 0)) (d (n "maybe-unwind") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)) (d (n "waker-queue") (r "^0.1") (d #t) (k 0)))) (h "037x8jivfggi1y7y656yyflwc9kvk6qz1l57182qx4vsbprjmg2f")))

