(define-module (crates-io as yn async-notify) #:use-module (crates-io))

(define-public crate-async-notify-0.1.0 (c (n "async-notify") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.5") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "07cax9pv0r16wckvcsghhjfi3b09ldq754vf72qizhf82g58scm6")))

(define-public crate-async-notify-0.1.1 (c (n "async-notify") (v "0.1.1") (d (list (d (n "async-std") (r "^1.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.5") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)))) (h "14pvbfp4xlhqhpwnqlj9a7b7ygfxqb248013c9v6rkw9pfx8x9ri")))

(define-public crate-async-notify-0.1.2 (c (n "async-notify") (v "0.1.2") (d (list (d (n "async-std") (r "^1.5") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.5") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)))) (h "1advfxrfvf8rz05aylq1gd63v2v3ns5vy3g5wb8rv3mi9h9ddcgd")))

(define-public crate-async-notify-0.2.0 (c (n "async-notify") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0cgsr1gwv34fxliksxcji6mni3ggayl9gnpaphblhraajd9zcml3")))

(define-public crate-async-notify-0.3.0 (c (n "async-notify") (v "0.3.0") (d (list (d (n "async-global-executor") (r "^2") (k 2)) (d (n "event-listener") (r "^4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1rmyr2acbaxwbijldalx8hqpfv04f1qlqb01blgajvpmh9iw75nv")))

