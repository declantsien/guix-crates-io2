(define-module (crates-io as yn async-anyhow-logger) #:use-module (crates-io))

(define-public crate-async-anyhow-logger-0.1.0 (c (n "async-anyhow-logger") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 2)))) (h "076yn0hlahc9yix69wafn6a25330hl1hjjd0hm8nvsvnw7wz18h6")))

