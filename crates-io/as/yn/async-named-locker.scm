(define-module (crates-io as yn async-named-locker) #:use-module (crates-io))

(define-public crate-async-named-locker-0.1.0 (c (n "async-named-locker") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notify-future") (r "^0.1.1") (d #t) (k 0)))) (h "1hgdds1fyrbabzpwwmpvf5hqjd52n6j2cibpf8iba6rk3j22n6br")))

(define-public crate-async-named-locker-0.1.1 (c (n "async-named-locker") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notify-future") (r "^0.1.1") (d #t) (k 0)))) (h "0mgp8sg3lvv4wzsz4897dw8n1514vmxxyf8kf431vqx0sdpldc2i")))

