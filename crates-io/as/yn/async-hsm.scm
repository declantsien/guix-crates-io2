(define-module (crates-io as yn async-hsm) #:use-module (crates-io))

(define-public crate-async-hsm-0.1.0 (c (n "async-hsm") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)))) (h "001df6nqwxiwhqp7a8xcblz89cbgqgqiwicff7pw3vj6pbx32nc1") (y #t)))

(define-public crate-async-hsm-0.1.1 (c (n "async-hsm") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)))) (h "03zny4blf50jl64mgc38dk5yajv63hpnp9lksmmd89zp4vryfwzs") (y #t)))

(define-public crate-async-hsm-0.2.0 (c (n "async-hsm") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)))) (h "1vh9k8j9v9zai1bdr3r3lnrna2my77gwl1g8zk54li74lykdapzy")))

