(define-module (crates-io as yn async-redis-session-v2) #:use-module (crates-io))

(define-public crate-async-redis-session-v2-0.2.3 (c (n "async-redis-session-v2") (v "0.2.3") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "redis") (r "^0.24.0") (f (quote ("aio" "async-std-comp"))) (d #t) (k 0)))) (h "0dm5xxamm57knd2r00g9qs1n65dz0nkxiirjwdkq91lbwymyzqh6")))

