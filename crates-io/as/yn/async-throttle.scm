(define-module (crates-io as yn async-throttle) #:use-module (crates-io))

(define-public crate-async-throttle-0.1.0 (c (n "async-throttle") (v "0.1.0") (h "1vx3yvyv5zbf8yhwmydxylq1klhrgj8pd70dhagbl0zkz3k3ayl6") (y #t)))

(define-public crate-async-throttle-0.2.0 (c (n "async-throttle") (v "0.2.0") (h "067dn4gj1csj98kcy91mq7ix94wavxs266g27s8qzsql6phzym35") (y #t)))

(define-public crate-async-throttle-0.3.0 (c (n "async-throttle") (v "0.3.0") (d (list (d (n "backoff") (r "^0.4.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("time" "sync"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "futures") (r "^0.3.27") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0z9ydag2n8g8sq2zh45vsvhl1ri9q24s0xcvdjy1m6wg5xib3l68")))

(define-public crate-async-throttle-0.3.1 (c (n "async-throttle") (v "0.3.1") (d (list (d (n "backoff") (r "^0.4.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("time" "sync"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "futures") (r "^0.3.27") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)))) (h "18cxb243126lc63ad3fkwcvcjdzwfjsn7fyad9scxn6haxdy19qc")))

(define-public crate-async-throttle-0.3.2 (c (n "async-throttle") (v "0.3.2") (d (list (d (n "backoff") (r "^0.4.0") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("time" "sync"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "futures") (r "^0.3.27") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)))) (h "13zf231gy1qrfrkrlh1vcijd382gzxdp37i7j45mlhv4w4nm768w")))

