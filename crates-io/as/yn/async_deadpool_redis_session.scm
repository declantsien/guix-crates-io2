(define-module (crates-io as yn async_deadpool_redis_session) #:use-module (crates-io))

(define-public crate-async_deadpool_redis_session-0.1.0 (c (n "async_deadpool_redis_session") (v "0.1.0") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "deadpool-redis") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1mqyi1gyjfgna7ixpiv5vm6kahjbaiq4hylrzwqw8jqsciiami0p")))

(define-public crate-async_deadpool_redis_session-0.2.0 (c (n "async_deadpool_redis_session") (v "0.2.0") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "deadpool-redis") (r "^0.12.0") (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "14p33hcq92crhgxqbamf0g7v9ipya0244v0m574zwac79iwgk7z6") (f (quote (("tokio1" "deadpool-redis/rt_tokio_1") ("default" "tokio1") ("async-std1" "deadpool-redis/rt_async-std_1"))))))

(define-public crate-async_deadpool_redis_session-0.3.0 (c (n "async_deadpool_redis_session") (v "0.3.0") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "deadpool-redis") (r "^0.13.0") (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0jg7kgdisahwshfrw76mzccr3cfk14qb0b6mb03g060h846jgghf") (f (quote (("tokio1" "deadpool-redis/rt_tokio_1") ("default" "tokio1") ("async-std1" "deadpool-redis/rt_async-std_1"))))))

