(define-module (crates-io as yn async-fsw) #:use-module (crates-io))

(define-public crate-async-fsw-0.1.0 (c (n "async-fsw") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0-pre.2") (d #t) (k 0)))) (h "020b9jg49yfi99zkcxh3xkc4wrafddww3xj04k7z7vc177wcrpjq")))

(define-public crate-async-fsw-0.2.0 (c (n "async-fsw") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "notify") (r "^5.0.0-pre.2") (d #t) (k 0)))) (h "08cn8ghaj0wsmc10hzqrqq9qjmla4zsawjii942jdfz23lxm7sr1")))

