(define-module (crates-io as yn async-lock) #:use-module (crates-io))

(define-public crate-async-lock-0.1.0 (c (n "async-lock") (v "0.1.0") (h "0db2xvw4i89m9hxnavya6598hnbcaflcl2i7300msvdvk3l6xf0s")))

(define-public crate-async-lock-1.0.0 (c (n "async-lock") (v "1.0.0") (d (list (d (n "event-listener") (r "^1.0.0") (d #t) (k 0)) (d (n "smol") (r "^0.1.6") (d #t) (k 2)))) (h "060lx4jl3fqgd00pc4cjgbpb9306gvq9vrsgy7dyzrlfapqg1h6c")))

(define-public crate-async-lock-1.0.1 (c (n "async-lock") (v "1.0.1") (d (list (d (n "event-listener") (r "^1.0.0") (d #t) (k 0)) (d (n "smol") (r "^0.1.6") (d #t) (k 2)))) (h "15zkqf03lywvxvf3mj9zkm3xx0ykfmql0s4x5m70kfwdi7m5329f")))

(define-public crate-async-lock-1.0.2 (c (n "async-lock") (v "1.0.2") (d (list (d (n "event-listener") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "smol") (r "^0.1.6") (d #t) (k 2)))) (h "0qs68gqk5a1xgxdj2xn17ivpg8icdrpg5jgrah2nsmb1nx2pdnv2")))

(define-public crate-async-lock-1.1.0 (c (n "async-lock") (v "1.1.0") (d (list (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)))) (h "1n2y5yxn4w00kkqr7if6ydzi2c48ikdiq4lzkcnswcqngg3gzli1")))

(define-public crate-async-lock-1.1.1 (c (n "async-lock") (v "1.1.1") (d (list (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)))) (h "0lfqbw0cmxnd1xg4mi4858xn8rhzij35485fgwn88mgxbc5sd5ar")))

(define-public crate-async-lock-1.1.2 (c (n "async-lock") (v "1.1.2") (d (list (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)))) (h "07i1f2gi9ba4p90cc2bg70032pb04yvhgx703phwyqxc96cr6xgk")))

(define-public crate-async-lock-1.1.3 (c (n "async-lock") (v "1.1.3") (d (list (d (n "event-listener") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "smol") (r "^0.1.18") (d #t) (k 2)))) (h "0abxwym4b66w38m1savb699jlkyrh1lz9lvyy856bp106m0gn3mh")))

(define-public crate-async-lock-1.1.4 (c (n "async-lock") (v "1.1.4") (d (list (d (n "event-listener") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "smol") (r "^0.1.18") (d #t) (k 2)))) (h "0v5q97cvz2l59dsn5n38hvhl1agl99d53lfpi0g7vm7r3zqbqahl")))

(define-public crate-async-lock-1.1.5 (c (n "async-lock") (v "1.1.5") (d (list (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "smol") (r "^0.1.18") (d #t) (k 2)))) (h "1gdpfz46i5i14djx52z27magrjfmla24d230yrlrxy1pl7h4nz8i")))

(define-public crate-async-lock-2.0.0 (c (n "async-lock") (v "2.0.0") (d (list (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "async-rwlock") (r "^1.0.1") (d #t) (k 0)))) (h "0bba0gxlx6c60jzmr8xsjkxnkki3n9niy712s57kibr1i0hsn2v0")))

(define-public crate-async-lock-2.0.1 (c (n "async-lock") (v "2.0.1") (d (list (d (n "async-mutex") (r "^1.1.5") (d #t) (k 0)) (d (n "async-rwlock") (r "^1.0.1") (d #t) (k 0)))) (h "0qny8v80c99p4gqjzl3bhgbcwnd1kjkyw9qw3yq6v02q8jxp0sg6")))

(define-public crate-async-lock-2.1.0 (c (n "async-lock") (v "2.1.0") (d (list (d (n "async-barrier") (r "^1.0.0") (d #t) (k 0)) (d (n "async-mutex") (r "^1.2.0") (d #t) (k 0)) (d (n "async-rwlock") (r "^1.1.0") (d #t) (k 0)) (d (n "async-semaphore") (r "^1.0.0") (d #t) (k 0)))) (h "1arlyw4rmybb5v2xxc4mr6r8ayrrmc6j1zigmd31gv68a5s1lmy7")))

(define-public crate-async-lock-2.1.1 (c (n "async-lock") (v "2.1.1") (d (list (d (n "async-barrier") (r "^1.0.0") (d #t) (k 0)) (d (n "async-mutex") (r "^1.2.0") (d #t) (k 0)) (d (n "async-rwlock") (r "^1.1.0") (d #t) (k 0)) (d (n "async-semaphore") (r "^1.0.0") (d #t) (k 0)))) (h "1ibs86xvis16a71k35azdn95dcm9yxj6jw28f6q9imgj7v6glzyd")))

(define-public crate-async-lock-2.1.2 (c (n "async-lock") (v "2.1.2") (d (list (d (n "async-barrier") (r "^1.0.1") (d #t) (k 0)) (d (n "async-mutex") (r "^1.3.0") (d #t) (k 0)) (d (n "async-rwlock") (r "^1.1.0") (d #t) (k 0)) (d (n "async-semaphore") (r "^1.1.0") (d #t) (k 0)))) (h "00lqrqqrkx0zs6ayi5gwli8bx7znnkyqa6av1n2c9r34pywks4ib")))

(define-public crate-async-lock-2.1.3 (c (n "async-lock") (v "2.1.3") (d (list (d (n "async-barrier") (r "^1.0.1") (d #t) (k 0)) (d (n "async-mutex") (r "^1.3.0") (d #t) (k 0)) (d (n "async-rwlock") (r "^1.1.0") (d #t) (k 0)) (d (n "async-semaphore") (r "^1.1.0") (d #t) (k 0)))) (h "15bvd50w8k36n0dpklnjwn94n9xs1r5q8rlmq5bpwfa58gxxffmb")))

(define-public crate-async-lock-2.2.0 (c (n "async-lock") (v "2.2.0") (d (list (d (n "async-barrier") (r "^1.0.1") (d #t) (k 0)) (d (n "async-mutex") (r "^1.3.0") (d #t) (k 0)) (d (n "async-rwlock") (r "^1.1.0") (d #t) (k 0)) (d (n "async-semaphore") (r "^1.1.0") (d #t) (k 0)))) (h "0qk3cli7dsdms1xplds4r1py3y27b0avdxmxwbjdyrrwxf80403n")))

(define-public crate-async-lock-2.3.0 (c (n "async-lock") (v "2.3.0") (d (list (d (n "async-channel") (r "^1.5.0") (d #t) (k 2)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.10.1") (d #t) (k 2)))) (h "1yrvnshs94aiimvprqkhcg1z7x9abzsja8f4ifcakr5x6abn15hr")))

(define-public crate-async-lock-2.4.0 (c (n "async-lock") (v "2.4.0") (d (list (d (n "async-channel") (r "^1.5.0") (d #t) (k 2)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "12vrxabscfq0a20qayf60fqpgg3xchga7bawgh0a2iwrpxhyma76")))

(define-public crate-async-lock-2.5.0 (c (n "async-lock") (v "2.5.0") (d (list (d (n "async-channel") (r "^1.5.0") (d #t) (k 2)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1dkps300paviz3npwaq38n0sc92fv55b20mr3fizp0hp34fifyp9") (r "1.43")))

(define-public crate-async-lock-2.6.0 (c (n "async-lock") (v "2.6.0") (d (list (d (n "async-channel") (r "^1.5.0") (d #t) (k 2)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(any(target_arch = \"wasm32\", target_arch = \"wasm64\"))") (k 2)))) (h "11dn0wblps0rhjp8izykjan1irspaca0454i09zc39lmhvz1w468") (r "1.43")))

(define-public crate-async-lock-2.7.0 (c (n "async-lock") (v "2.7.0") (d (list (d (n "async-channel") (r "^1.5.0") (d #t) (k 2)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(any(target_arch = \"wasm32\", target_arch = \"wasm64\"))") (k 2)))) (h "1mrd4kai92fcgl9974dpmibiq6ja9drz41v3crvv0c27a8kzf97s") (r "1.48")))

(define-public crate-async-lock-2.8.0 (c (n "async-lock") (v "2.8.0") (d (list (d (n "async-channel") (r "^1.5.0") (d #t) (k 2)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "waker-fn") (r "^1.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(any(target_arch = \"wasm32\", target_arch = \"wasm64\"))") (k 2)))) (h "0asq5xdzgp3d5m82y5rg7a0k9q0g95jy6mgc7ivl334x7qlp4wi8") (r "1.48")))

(define-public crate-async-lock-3.0.0 (c (n "async-lock") (v "3.0.0") (d (list (d (n "async-channel") (r "^1.5.0") (d #t) (k 2)) (d (n "event-listener") (r "^3.0.0") (k 0)) (d (n "event-listener-strategy") (r "^0.3.0") (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.11") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(any(target_arch = \"wasm32\", target_arch = \"wasm64\"))") (k 2)))) (h "1vc6nigsf3jmndygxhy7wri228najbpzglw78jhr9frrrp6h1sa5") (f (quote (("std" "event-listener/std" "event-listener-strategy/std") ("default" "std")))) (r "1.59")))

(define-public crate-async-lock-3.1.0 (c (n "async-lock") (v "3.1.0") (d (list (d (n "async-channel") (r "^2.0.0") (d #t) (k 2)) (d (n "event-listener") (r "^3.0.0") (k 0)) (d (n "event-listener-strategy") (r "^0.3.0") (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.11") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(any(target_arch = \"wasm32\", target_arch = \"wasm64\"))") (k 2)))) (h "1mzi9hanvss9n9f8bw33cyz43fn6igs76v42mchy4im7m0mapcny") (f (quote (("std" "event-listener/std" "event-listener-strategy/std") ("default" "std")))) (r "1.59")))

(define-public crate-async-lock-3.1.1 (c (n "async-lock") (v "3.1.1") (d (list (d (n "async-channel") (r "^2.0.0") (d #t) (k 2)) (d (n "event-listener") (r "^3.0.0") (k 0)) (d (n "event-listener-strategy") (r "^0.3.0") (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.11") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(any(target_arch = \"wasm32\", target_arch = \"wasm64\"))") (k 2)))) (h "01ci0zgdhxywcidfnnrrpk2z140p82la217qq1fb5lw7wxzrqnv5") (f (quote (("std" "event-listener/std" "event-listener-strategy/std") ("default" "std")))) (r "1.59")))

(define-public crate-async-lock-3.1.2 (c (n "async-lock") (v "3.1.2") (d (list (d (n "async-channel") (r "^2.0.0") (d #t) (k 2)) (d (n "event-listener") (r "^4.0.0") (k 0)) (d (n "event-listener-strategy") (r "^0.4.0") (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.11") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(any(target_arch = \"wasm32\", target_arch = \"wasm64\"))") (k 2)))) (h "05p3yyvn6w97ky6scpwpaqv3xdvisq059drl31qrdk6p7m2v7a6y") (f (quote (("std" "event-listener/std" "event-listener-strategy/std") ("default" "std")))) (r "1.59")))

(define-public crate-async-lock-3.2.0 (c (n "async-lock") (v "3.2.0") (d (list (d (n "async-channel") (r "^2.0.0") (d #t) (k 2)) (d (n "event-listener") (r "^4.0.0") (k 0)) (d (n "event-listener-strategy") (r "^0.4.0") (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.11") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(any(target_arch = \"wasm32\", target_arch = \"wasm64\"))") (k 2)))) (h "031i8kx440v77cvr3lp4a5dcjdz92zpi4616akfvjgfmhwky89bi") (f (quote (("std" "event-listener/std" "event-listener-strategy/std") ("default" "std")))) (r "1.59")))

(define-public crate-async-lock-3.3.0 (c (n "async-lock") (v "3.3.0") (d (list (d (n "async-channel") (r "^2.0.0") (d #t) (k 2)) (d (n "event-listener") (r "^4.0.0") (k 0)) (d (n "event-listener-strategy") (r "^0.4.0") (k 0)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.11") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_family = \"wasm\")") (k 2)))) (h "0yxflkfw46rad4lv86f59b5z555dlfmg1riz1n8830rgi0qb8d6h") (f (quote (("std" "event-listener/std" "event-listener-strategy/std") ("default" "std")))) (r "1.60")))

