(define-module (crates-io as yn async-change-tracker) #:use-module (crates-io))

(define-public crate-async-change-tracker-0.1.0 (c (n "async-change-tracker") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 2)))) (h "0f2q1sjq0b077gwslv3b1q1p0qxa4g7pdkrmhl5s1kjdl8qdp4r6")))

(define-public crate-async-change-tracker-0.2.0 (c (n "async-change-tracker") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "10m88wyx6diaqlpqy61ghmipjrz8y8p9l7c4gzjjamvj983nydwf")))

(define-public crate-async-change-tracker-0.3.0 (c (n "async-change-tracker") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1w19l6p2al394rj5zaln8s4jgrqy2j6wbaf5yq2f10a8xp7gmadk")))

(define-public crate-async-change-tracker-0.3.1 (c (n "async-change-tracker") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1641pyyy2hb2rabg5hf7v37k6jrvp4fz5kcglpnfw1ap2ng8b13a")))

(define-public crate-async-change-tracker-0.3.2 (c (n "async-change-tracker") (v "0.3.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0qhc2x7ylv4zjj1bccpdbzcmjf9b8qw1yc7zq61cqanpkqhpf7d2")))

(define-public crate-async-change-tracker-0.3.3 (c (n "async-change-tracker") (v "0.3.3") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01vmrxwrfyis32qa6hvyb036kh7cs1g66fwdcdb7cbh0bvjjfllz")))

(define-public crate-async-change-tracker-0.3.4 (c (n "async-change-tracker") (v "0.3.4") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pma7yq7x6r3pw55xkz89ra7mpszac0i1drqvw3fkn0b3g04hsf6")))

(define-public crate-async-change-tracker-0.3.5 (c (n "async-change-tracker") (v "0.3.5") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1bcijhj70rgmscy4xwr3n41ysxzqay3303wis1jx28iliyy9kvln")))

