(define-module (crates-io as yn async_pipeline_for_lucas) #:use-module (crates-io))

(define-public crate-async_pipeline_for_lucas-0.0.2 (c (n "async_pipeline_for_lucas") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d4j433asbcyka7fy4z3fkgk437yv458xnbdbil8ygi30glm2m1m")))

(define-public crate-async_pipeline_for_lucas-0.0.3 (c (n "async_pipeline_for_lucas") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j23sdji5ms144348ryrl1my43nbmzxsjrfy01yv5q7mh533x0mc")))

(define-public crate-async_pipeline_for_lucas-0.0.4 (c (n "async_pipeline_for_lucas") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kmnpw0rj2mg32f7rnm9ys3wb2pcvhfvxhlvhfj3wykn2ajg4ca1")))

(define-public crate-async_pipeline_for_lucas-0.0.5 (c (n "async_pipeline_for_lucas") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1khkdi927pn0w010axy3hcpciz62iqfw7jh5hr99zx3509n79rq5")))

