(define-module (crates-io as yn async-signature) #:use-module (crates-io))

(define-public crate-async-signature-0.0.0 (c (n "async-signature") (v "0.0.0") (h "18irvxw47ys4swplfr70k9r8fpvm8slgvvf87n3s8xlhjs43ihwj") (y #t)))

(define-public crate-async-signature-0.0.1 (c (n "async-signature") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "signature") (r "^1.2.0") (d #t) (k 0)))) (h "1s4rnrza82vlrnq3cdg87k33cmmds4cv6bd0abc9z36sh1y4ah2a") (f (quote (("digest" "signature/digest-preview"))))))

(define-public crate-async-signature-0.1.0 (c (n "async-signature") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "signature") (r "^1.5") (d #t) (k 0)))) (h "1m4k71dpavjbanci5jywanh6vzgvb3c3gpx5vbma63yjykhzva1k") (f (quote (("digest" "signature/digest-preview"))))))

(define-public crate-async-signature-0.2.0-pre (c (n "async-signature") (v "0.2.0-pre") (d (list (d (n "async-trait") (r "^0.1.9") (d #t) (k 0)) (d (n "signature") (r "^1.6") (d #t) (k 0)))) (h "0dckzb6sn5z9i4c0l4a3dfk3wlfja937z5nqwms0jhzs8zacv3ww") (f (quote (("digest" "signature/digest-preview")))) (r "1.56")))

(define-public crate-async-signature-0.2.0 (c (n "async-signature") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.9") (d #t) (k 0)) (d (n "signature") (r "^1.6") (d #t) (k 0)))) (h "1qy8avkvz96q3vv29zjva75c2rw6fy63aibip1n90hiv4y5dwyn5") (f (quote (("digest" "signature/digest-preview")))) (y #t) (r "1.56")))

(define-public crate-async-signature-0.2.1 (c (n "async-signature") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.9") (d #t) (k 0)) (d (n "signature") (r "^1.6") (d #t) (k 0)))) (h "0jcdj1qx64k9x78yiysyv0b6dsmscsqpnzdacsz4v18caaf48hw6") (f (quote (("digest" "signature/digest-preview")))) (r "1.56")))

(define-public crate-async-signature-0.3.0 (c (n "async-signature") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.9") (d #t) (k 0)) (d (n "signature") (r "^2") (d #t) (k 0)))) (h "1v0by8mv6h4r97q326945bp6b98pdxmsfjnp1gjqnzn7259j24m0") (f (quote (("digest" "signature/digest")))) (r "1.56")))

(define-public crate-async-signature-0.4.0 (c (n "async-signature") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.9") (d #t) (k 0)) (d (n "signature") (r ">=2.0, <2.3") (d #t) (k 0)))) (h "122yy24iijbs7af73qb4p5a2f164xb8plra2968gvylfsyz16xrl") (f (quote (("digest" "signature/digest")))) (r "1.60")))

(define-public crate-async-signature-0.5.0-pre.0 (c (n "async-signature") (v "0.5.0-pre.0") (d (list (d (n "signature") (r ">=2.0, <2.3") (d #t) (k 0)))) (h "1rd6q7wyisizsv0ww1l1kqwd123apidz925jxvbbvakph8x9w2zz") (f (quote (("rand_core" "signature/rand_core") ("digest" "signature/digest")))) (r "1.75")))

(define-public crate-async-signature-0.5.0 (c (n "async-signature") (v "0.5.0") (d (list (d (n "signature") (r ">=2.0, <2.3") (d #t) (k 0)))) (h "1w267c4hbymszjgq1mh276jwby2yv4l72m9i4f65p8352y0hvxsi") (f (quote (("rand_core" "signature/rand_core") ("digest" "signature/digest")))) (r "1.75")))

(define-public crate-async-signature-0.6.0-pre.0 (c (n "async-signature") (v "0.6.0-pre.0") (d (list (d (n "signature") (r "=2.3.0-pre.2") (d #t) (k 0)))) (h "1y3i70q1ax3qmjnribhaapc9jf8c6xj1npi4n4a2xhwfra0zmrfn") (f (quote (("rand_core" "signature/rand_core") ("digest" "signature/digest")))) (r "1.75")))

(define-public crate-async-signature-0.6.0-pre.1 (c (n "async-signature") (v "0.6.0-pre.1") (d (list (d (n "signature") (r "=2.3.0-pre.3") (d #t) (k 0)))) (h "1v8j6gqb2m8jlck5yn18bjjcijlp3k2wz4xfqf1gf58ckpbax5y8") (f (quote (("rand_core" "signature/rand_core") ("digest" "signature/digest")))) (r "1.75")))

(define-public crate-async-signature-0.5.1 (c (n "async-signature") (v "0.5.1") (d (list (d (n "signature") (r ">=2.0, <2.3") (d #t) (k 0)))) (h "08kwwivy1kv7zhklbz055g2m5qfjwg6jnw0q2q6g949h2q8wqvb4") (f (quote (("rand_core" "signature/rand_core") ("digest" "signature/digest")))) (r "1.75")))

