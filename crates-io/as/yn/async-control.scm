(define-module (crates-io as yn async-control) #:use-module (crates-io))

(define-public crate-async-control-0.0.0 (c (n "async-control") (v "0.0.0") (h "0clszpjkfl3iziwczxvm90dn2bxxdnxcvszy4rl1far3a91zvz8d")))

(define-public crate-async-control-0.1.0-alpha.1 (c (n "async-control") (v "0.1.0-alpha.1") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "async-wakers") (r "^0.1.0-alpha.4") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "0s0y8gr6nmkzbv79lg4w9kdwysghs3p4n5w0vb503rhjjv8mhbdi") (f (quote (("std" "async-wakers/std") ("default" "std") ("alloc" "async-wakers/alloc")))) (y #t)))

(define-public crate-async-control-0.1.0-alpha.2 (c (n "async-control") (v "0.1.0-alpha.2") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "async-wakers") (r "^0.1.0-alpha.5") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "1kwyxrkg3xvlx65f7q943gdb5lzf0pkyqzl3svzs5fbbvdmnzgi8") (f (quote (("std" "async-wakers/std") ("default" "std") ("alloc" "async-wakers/alloc")))) (y #t)))

(define-public crate-async-control-0.1.0-alpha.3 (c (n "async-control") (v "0.1.0-alpha.3") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "async-wakers") (r "^0.1.0-alpha.5") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "1kvxm21ipigqp4phkpgyyp1kf5y58iv7c8dh9adavvgxar54b2fp") (f (quote (("std" "async-wakers/std") ("default" "std") ("alloc" "async-wakers/alloc")))) (y #t)))

(define-public crate-async-control-0.1.0-alpha.4 (c (n "async-control") (v "0.1.0-alpha.4") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "async-wakers") (r "^0.1.0-alpha.11") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "0v3f6n8m1s9bxfa412hr5w7xcz73rv5v68gcl857yflaanv4piyl") (f (quote (("std" "async-wakers/std") ("default" "std") ("alloc" "async-wakers/alloc")))) (y #t)))

(define-public crate-async-control-0.1.0-alpha.5 (c (n "async-control") (v "0.1.0-alpha.5") (d (list (d (n "async-wakers") (r "^0.1.0-alpha.12") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "1an4b2arp22b9rsjzyqxz5vvcmh59fc7ihgk654rwy5vizmwxp4i") (f (quote (("std" "async-wakers/std") ("default" "std") ("alloc" "async-wakers/alloc")))) (y #t)))

(define-public crate-async-control-0.1.0-alpha.6 (c (n "async-control") (v "0.1.0-alpha.6") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "async-wakers") (r "^0.1.0-alpha.13") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1m017f62j26baqmmw2dsbrhmqak8asjyy0134ma9v0zibvmk3bpw") (f (quote (("std" "async-wakers/std") ("default" "std") ("alloc" "async-wakers/alloc")))) (y #t)))

(define-public crate-async-control-0.1.0-alpha.7 (c (n "async-control") (v "0.1.0-alpha.7") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "async-wakers") (r "^0.1.0-alpha.20") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "0l1vp7hrynjzf260f3wv76lhnm981gjx61f5l5km5dn9n2s90jw6") (f (quote (("std" "async-wakers/std") ("default" "std") ("alloc" "async-wakers/alloc")))) (y #t)))

(define-public crate-async-control-0.1.0-alpha.8 (c (n "async-control") (v "0.1.0-alpha.8") (d (list (d (n "async-wakers") (r "^0.1.0-alpha.20") (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "05gfr8dxjgra98y88wsix6issccjyk642hvij2rq95xzklwl7s61") (f (quote (("std" "async-wakers/std") ("default" "std") ("alloc" "async-wakers/alloc")))) (y #t)))

(define-public crate-async-control-0.1.0-alpha.9 (c (n "async-control") (v "0.1.0-alpha.9") (d (list (d (n "async-wakers") (r "^0.1.0-alpha.21") (k 0)))) (h "1jmx35f3bzfqvwkbvhfpqjpvsddnw1h2vqzvqkkknzfkd9hgwvmk") (f (quote (("default")))) (y #t)))

(define-public crate-async-control-0.1.0-alpha.10 (c (n "async-control") (v "0.1.0-alpha.10") (d (list (d (n "async-wakers") (r "^0.1.0-alpha.21") (k 0)))) (h "0apypgx0nz4bkf5s3098588i958730hrl9qm0gszz4ydzwd5p0bp") (f (quote (("default")))) (y #t)))

(define-public crate-async-control-0.1.0-alpha.11 (c (n "async-control") (v "0.1.0-alpha.11") (d (list (d (n "async-wakers") (r "^0.1.0-alpha.21") (k 0)))) (h "0xy8p42w9q57xcjhipzhl3x2g7qdynw3s1ffw68w4cwdcpasvdvi") (f (quote (("default")))) (y #t)))

