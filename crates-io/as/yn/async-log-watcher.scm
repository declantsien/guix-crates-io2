(define-module (crates-io as yn async-log-watcher) #:use-module (crates-io))

(define-public crate-async-log-watcher-0.0.1 (c (n "async-log-watcher") (v "0.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "sync" "time" "rt" "macros" "parking_lot"))) (d #t) (k 2)))) (h "017q4jwys5f0bkr96j54lrrd0dsv2jr20w9h3w9b1m7f93y605xg")))

