(define-module (crates-io as yn async-proxies) #:use-module (crates-io))

(define-public crate-async-proxies-0.1.0 (c (n "async-proxies") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "io-util" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0mplwa9f7hvrlgsz2avddql1siq2galg34l33jfidsk7ldn38fh5")))

