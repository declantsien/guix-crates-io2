(define-module (crates-io as yn async_odoors) #:use-module (crates-io))

(define-public crate-async_odoors-1.0.0 (c (n "async_odoors") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_with") (r "^2.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fanf3ddj4rvk0f4hl3x1fhdy7lr79zm0c4hymrdhjhy3az54rqh")))

