(define-module (crates-io as yn async-let) #:use-module (crates-io))

(define-public crate-async-let-0.1.0 (c (n "async-let") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)))) (h "1mbd6pvab1i9da9iw85gybrcqlrs9nhz25ai9dazmha4y4m2laz5")))

