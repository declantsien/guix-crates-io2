(define-module (crates-io as yn async_user_lookup) #:use-module (crates-io))

(define-public crate-async_user_lookup-0.1.0 (c (n "async_user_lookup") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "01w6q3yklzf7kg18nr91bwkzm6x7kbm8x253x9q5bjy5vr1qqhp7")))

(define-public crate-async_user_lookup-0.1.1 (c (n "async_user_lookup") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "07cppxs515zk5yvccil5r5n68rfzcjznsvpxg1jswx3syn7gl8hp")))

(define-public crate-async_user_lookup-0.1.2 (c (n "async_user_lookup") (v "0.1.2") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "04sq01viwc1wnzx2sqqp18gninn6nm7my08s68d8fcjci0v26d2x")))

(define-public crate-async_user_lookup-0.1.3 (c (n "async_user_lookup") (v "0.1.3") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0hpaab9fii1g8c7wc06jss3bq620d033k6c9qiswqlmrcan3ml04")))

