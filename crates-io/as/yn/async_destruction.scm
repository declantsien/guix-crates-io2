(define-module (crates-io as yn async_destruction) #:use-module (crates-io))

(define-public crate-async_destruction-0.1.0 (c (n "async_destruction") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "10irk79mivrymz2130jy6j205lddj3k44zs7pc71qli84scjw5pc")))

(define-public crate-async_destruction-0.1.1 (c (n "async_destruction") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0k9wii8gzns30p86fndka1bfs2lsvznawrw7wwzv0fi03kqm3x4x")))

