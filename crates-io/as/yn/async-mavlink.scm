(define-module (crates-io as yn async-mavlink) #:use-module (crates-io))

(define-public crate-async-mavlink-0.1.0 (c (n "async-mavlink") (v "0.1.0") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "blocking") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3") (d #t) (k 0)) (d (n "mavlink") (r "^0.8") (d #t) (k 0)) (d (n "smol") (r "^1") (d #t) (k 2)))) (h "1sccv5cbgfbqmzgaijn5rvzg9bh5ffrpik399mwbjah7qhfpdx71")))

(define-public crate-async-mavlink-0.1.1 (c (n "async-mavlink") (v "0.1.1") (d (list (d (n "arc-swap") (r "^0.4") (d #t) (k 0)) (d (n "blocking") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mavlink") (r "^0.8") (d #t) (k 0)) (d (n "smol") (r "^1") (d #t) (k 2)))) (h "164lv7d6khw9rj68myi9nwc8xbjk82rx02gjlgyybhjg2g3zw8h2")))

(define-public crate-async-mavlink-0.1.2 (c (n "async-mavlink") (v "0.1.2") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "blocking") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mavlink") (r "^0.8") (d #t) (k 0)) (d (n "smol") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0psy2z7flxiba8110926qsmd46n1kdbilw76xwcfv032fh0hxlyb")))

(define-public crate-async-mavlink-0.1.3 (c (n "async-mavlink") (v "0.1.3") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "blocking") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mavlink") (r "^0.8") (d #t) (k 0)) (d (n "smol") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14n0qw6a8dgrd1ryqfb5cizdikq61w91ax78fvqqlk9gg1as463p")))

