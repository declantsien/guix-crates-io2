(define-module (crates-io as yn async-std-openssl) #:use-module (crates-io))

(define-public crate-async-std-openssl-0.6.2 (c (n "async-std-openssl") (v "0.6.2") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10.32") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "1m6cc56b0qck6hx1g5pkp18vs6s20ici1ixxs88fh6y8zn71285k")))

(define-public crate-async-std-openssl-0.6.3 (c (n "async-std-openssl") (v "0.6.3") (d (list (d (n "async-dup") (r "^1.2") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "1vlmddzfnw2sw4rznw253npqgj53aw2skxzig3z136y41yq7d2j0")))

