(define-module (crates-io as yn async-shell) #:use-module (crates-io))

(define-public crate-async-shell-0.1.0 (c (n "async-shell") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "blocking") (r "^0.5") (k 0)) (d (n "wait-timeout") (r "^0.2") (k 0)))) (h "0xf59lww3gvivs4yskw6zl7icmj4f6h48knf1lpjzm2l4h7yigch")))

(define-public crate-async-shell-0.1.1 (c (n "async-shell") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "blocking") (r "^0.6") (k 0)) (d (n "wait-timeout") (r "^0.2") (k 0)))) (h "07b78xdpbgnny4xiw48r9386f7q80mm9x1jrcmb5jzxx7k9ldb98")))

(define-public crate-async-shell-0.1.2 (c (n "async-shell") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "blocking") (r "^1.0") (k 0)) (d (n "wait-timeout") (r "^0.2") (k 0)))) (h "0vr0z8incwdvk1g6hwxh4bilk7qhzg7rmd36qc1zkbr4kv9y2mh7")))

