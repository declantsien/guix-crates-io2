(define-module (crates-io as yn asyncify) #:use-module (crates-io))

(define-public crate-asyncify-0.1.0 (c (n "asyncify") (v "0.1.0") (h "1pxhvif3p886zs2wc99jjfprjsv8sp6bj864ffs3cqbnxmmbv70r") (y #t)))

(define-public crate-asyncify-0.2.0 (c (n "asyncify") (v "0.2.0") (h "00kh56dqyga5r8dka9lb0s59bgic3ar9z3w564lcp7azvzdv6g0f")))

