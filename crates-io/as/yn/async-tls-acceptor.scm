(define-module (crates-io as yn async-tls-acceptor) #:use-module (crates-io))

(define-public crate-async-tls-acceptor-0.1.0 (c (n "async-tls-acceptor") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.14") (d #t) (k 0)))) (h "0xbk6bc18pzb1lcqdiskb7xgm49pcxnirpknknkpd9fjw735k28k")))

