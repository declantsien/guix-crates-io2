(define-module (crates-io as yn asynchelp) #:use-module (crates-io))

(define-public crate-asynchelp-0.1.0 (c (n "asynchelp") (v "0.1.0") (d (list (d (n "asynchelp-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "stackfuture") (r "^0.3.0") (d #t) (k 2)) (d (n "tarpc") (r "^0.31.0") (d #t) (k 2)))) (h "183n2kh645ppj74lbg1gv99idlk3w58fhpk5hv1xga5938b3g7hp") (f (quote (("tarpc" "asynchelp-macros/tarpc") ("stackfuture" "asynchelp-macros/stackfuture"))))))

