(define-module (crates-io as yn async-stm) #:use-module (crates-io))

(define-public crate-async-stm-0.1.0 (c (n "async-stm") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0fdg504785iaja5p903lvpdmdwhzz5f4qwwi1hlhff5ay134akrk") (f (quote (("queues") ("default" "queues"))))))

(define-public crate-async-stm-0.1.1 (c (n "async-stm") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "12358zmp0965xr5pl5865pfg3qgzgzn9lb87z23059ichylzyixn") (f (quote (("queues") ("default" "queues"))))))

(define-public crate-async-stm-0.1.2 (c (n "async-stm") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0l8ylm971kzis66yp751hris86hkrg4lsdzn6q9wjq592hx7d69z") (f (quote (("unstable") ("queues") ("default" "queues"))))))

(define-public crate-async-stm-0.2.0 (c (n "async-stm") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1l1lxznahrh2xmzsxqlpq0gd4pm0b9bgcpqjw715jxv11pa2ql3z") (f (quote (("unstable") ("queues") ("default" "queues"))))))

(define-public crate-async-stm-0.3.0 (c (n "async-stm") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1hrqkjvj8z0wm50aks6ibvvpk9k2x298b3mwizi1f232gfabafhd") (f (quote (("unstable") ("queues") ("default" "queues"))))))

(define-public crate-async-stm-0.4.0 (c (n "async-stm") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "03iir33fqcvh8znir2abg46zdriwxyl6j5x1vv6qf28dcbizjafk") (f (quote (("unstable") ("queues") ("default" "queues"))))))

(define-public crate-async-stm-0.5.0 (c (n "async-stm") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "02smx4268fhdr2y9xswlfv83q98kkn2c1b90x7rq8sg1akkxngnp") (f (quote (("unstable") ("queues") ("default" "queues"))))))

