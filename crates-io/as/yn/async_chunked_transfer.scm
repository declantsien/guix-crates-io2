(define-module (crates-io as yn async_chunked_transfer) #:use-module (crates-io))

(define-public crate-async_chunked_transfer-1.4.0 (c (n "async_chunked_transfer") (v "1.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1n2zsg2p3dlz5ygfvs6483qy20qpq0di9r3wlqgrxk181amn39ni")))

