(define-module (crates-io as yn async_t_internal) #:use-module (crates-io))

(define-public crate-async_t_internal-0.2.0 (c (n "async_t_internal") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rz4rwjgc7wcnwi6i42fa4anl1p2iysh9w0d35i2wsgkhja17j53")))

(define-public crate-async_t_internal-0.3.0 (c (n "async_t_internal") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jgksm8psrrbv89d6fp1v304yc84rkcqli7mxzgx4b770prq44ai")))

(define-public crate-async_t_internal-0.4.0 (c (n "async_t_internal") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13ydhmkgcp8h5563ca9440gpsnx3rskwixw8997fab2xcjzvr9xd")))

(define-public crate-async_t_internal-0.6.0 (c (n "async_t_internal") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16pj64zdcfh8q9r5glxb2rgsabcxj4p8784l50f87k3c3i2lcl5r")))

(define-public crate-async_t_internal-0.7.0 (c (n "async_t_internal") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "02jjjp9girbsxw7r8sv24phadq9bsqlnb676wa5gxh2hrfvji7hd")))

(define-public crate-async_t_internal-0.6.1 (c (n "async_t_internal") (v "0.6.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0vr2bi6zdcngxh692f8ksca7xqgd1vyw21kk8lgdfqdndsgq03ad")))

