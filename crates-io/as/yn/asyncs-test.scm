(define-module (crates-io as yn asyncs-test) #:use-module (crates-io))

(define-public crate-asyncs-test-0.1.0 (c (n "asyncs-test") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (f (quote ("full"))) (d #t) (k 0)))) (h "1f75m6ql5zbknnnml4169clfivnpdn8hjyfgal6gjqjmkydk9mxi")))

(define-public crate-asyncs-test-0.2.0 (c (n "asyncs-test") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (f (quote ("full"))) (d #t) (k 0)))) (h "16lcy2n30ms02ybxnajsg86p1f1lsic5l4c8spkwq88q1330qgak")))

(define-public crate-asyncs-test-0.2.1 (c (n "asyncs-test") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (f (quote ("full"))) (d #t) (k 0)))) (h "0r0vmwy43y8fnnbj26rfdh06sn0sl2ldb2nrci402h29vjs0aw84")))

(define-public crate-asyncs-test-0.3.0 (c (n "asyncs-test") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.61") (f (quote ("full"))) (d #t) (k 0)))) (h "00r8f4y2rallnmbwkccrbf9qjj6f68d0ba0n9ghbxpi2hqbrpjdk")))

