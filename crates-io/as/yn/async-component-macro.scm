(define-module (crates-io as yn async-component-macro) #:use-module (crates-io))

(define-public crate-async-component-macro-0.1.0 (c (n "async-component-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19544jndp8s7kn0v9q34xi8sr9dagw4dzp0wvv974wmmplh3mr3q")))

(define-public crate-async-component-macro-0.1.3 (c (n "async-component-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13psm990jnd6i07cmjbn5mxjnhwikn88zy62gnf6xqz9ni7kinsq")))

(define-public crate-async-component-macro-0.2.0 (c (n "async-component-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17lj6y5rbxwm643vd9b89iv1fc4m0cizlj2im21lpjwi521p1nm1")))

(define-public crate-async-component-macro-0.2.1 (c (n "async-component-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fy9nf7wvx90hv2nyz6rcvxqmjf5ccv2ra491j4cxqi19wiaxwwx")))

(define-public crate-async-component-macro-0.2.2 (c (n "async-component-macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gybbcyjswg4k1p241zxxkzfzm17ksfpck7wncjix5jgkn100q9y") (y #t)))

(define-public crate-async-component-macro-0.2.3 (c (n "async-component-macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j8v95yzzqprf7dz92z5lx3ynnvslnf7hm52gc06czkqp0pi722c")))

(define-public crate-async-component-macro-0.2.5 (c (n "async-component-macro") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "069lp2n39kvwxhajdq2i3l2akv2lbg1vh5i84zs3yggfh2maamkv")))

(define-public crate-async-component-macro-0.3.0 (c (n "async-component-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kik611qwz6jpc97zb1q69fnxkzc2snlhkn7p9m502109xj611px")))

(define-public crate-async-component-macro-0.4.0 (c (n "async-component-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kll8fna0x6fpa7vpzyy5bd6150qpyahbggbk8711p6v2b8i346k")))

(define-public crate-async-component-macro-0.4.2 (c (n "async-component-macro") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h8bjdq162pa6vxshwkp6q4nh6pq4zwd2mb9p93hxnk0108bnywx")))

(define-public crate-async-component-macro-0.5.0 (c (n "async-component-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ixw7g80si5ghikwl2d5v1zcpy307p1kfm817kci9g524qzp7ajr")))

(define-public crate-async-component-macro-0.5.1 (c (n "async-component-macro") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jwn6x0sgfcc8s3hpmpaiid7gvlsgcwhh8x637j4wq05vlpm7ryq")))

(define-public crate-async-component-macro-0.5.2 (c (n "async-component-macro") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09665ai3glp0401m3wh03bcii8102lspn7356wmk5l8cvb0a43w8")))

(define-public crate-async-component-macro-0.5.3 (c (n "async-component-macro") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rx1md9dcl81p2f1rd276sjl5nh1bmvdq71mdv15dir8dbhshi54")))

(define-public crate-async-component-macro-0.5.4 (c (n "async-component-macro") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1vi333nqp9ll8is2dafp55bl9j64fqgh9q7wlly0slnnv0qzmbs1")))

(define-public crate-async-component-macro-0.6.0 (c (n "async-component-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rkmbx663bi99wfrynwd25kd1z121gabg8ssmir0hfgn83ncifsp")))

(define-public crate-async-component-macro-0.7.0 (c (n "async-component-macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pvvd815mrbaplv108wjfmplybx2k8r29d5l2m0q7njhc8d6s2iq")))

(define-public crate-async-component-macro-0.8.0 (c (n "async-component-macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v4qxrvrczm3knwcgkz6wslrmr13wr1ps9kg1nlpc3vwbh9si4jc")))

(define-public crate-async-component-macro-0.8.1 (c (n "async-component-macro") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p0a696kh4pflz8vs8dj7qpddmnfnsiwb2k71nx1jw8pl6a5x6gr")))

(define-public crate-async-component-macro-0.9.0 (c (n "async-component-macro") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wnj1il0ydpdjf4b49r42w74j6n1v274ppj2qpr48zyy5k5gi0pz")))

