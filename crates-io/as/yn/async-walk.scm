(define-module (crates-io as yn async-walk) #:use-module (crates-io))

(define-public crate-async-walk-0.1.0 (c (n "async-walk") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "stream" "macros" "rt-threaded"))) (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0k0bma6c47ljnx75kcxdxl2fpmzy42k01jash73jd61d1zbqzhzr")))

