(define-module (crates-io as yn async-datagram) #:use-module (crates-io))

(define-public crate-async-datagram-1.0.0 (c (n "async-datagram") (v "1.0.0") (h "01i3bgrmc2528fa89zfyhi9qp36pl1w8hrziw6i7v3l2gm45icb9")))

(define-public crate-async-datagram-2.0.0 (c (n "async-datagram") (v "2.0.0") (h "0312448ww1abhrz2pa2hwacj2jyijxmcp49lqrmagfiryvbz4bpj")))

(define-public crate-async-datagram-2.1.0 (c (n "async-datagram") (v "2.1.0") (h "0lhf6777vd1rkvnawzvf9zqjdrcgsppl3agnbmcys64rywnfw2zl")))

(define-public crate-async-datagram-2.2.0 (c (n "async-datagram") (v "2.2.0") (h "1bnigyl0cnj41gvcqwhqwjgzldi5ygy41b12yd0cby5h6m65kn23")))

(define-public crate-async-datagram-3.0.0 (c (n "async-datagram") (v "3.0.0") (h "0k4kiy67d24ay8l7xrfjpsa4zkmhxv97ddj0f16rcv61qkky3i4f")))

