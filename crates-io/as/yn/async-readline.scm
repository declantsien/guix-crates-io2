(define-module (crates-io as yn async-readline) #:use-module (crates-io))

(define-public crate-async-readline-0.1.0 (c (n "async-readline") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "mio") (r "^0.6.1") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^0.0.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.1") (d #t) (k 0)))) (h "1v8ccpddf7wsaa66nw91mhchwkzyh047vf8v2gl2kzbxgq4gmxr1")))

