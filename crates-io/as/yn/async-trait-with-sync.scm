(define-module (crates-io as yn async-trait-with-sync) #:use-module (crates-io))

(define-public crate-async-trait-with-sync-0.1.31 (c (n "async-trait-with-sync") (v "0.1.31") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1pgrnlyrqa3y8jw4dh7xmxkwrs1qkr5paqhnb69v5mgns4piqi9h")))

(define-public crate-async-trait-with-sync-0.1.36 (c (n "async-trait-with-sync") (v "0.1.36") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.14") (d #t) (k 2)) (d (n "tracing-attributes") (r "^0.1.8") (d #t) (k 2)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1vm43v715igd84rwhjgic3wb4r15nj5lzffkshpw1w17glmlwf5k")))

