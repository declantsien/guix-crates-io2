(define-module (crates-io as yn async-rate-limit) #:use-module (crates-io))

(define-public crate-async-rate-limit-0.0.3 (c (n "async-rate-limit") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.26") (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full" "test-util"))) (d #t) (k 0)))) (h "1ahgyyg5kxakl6m7rv7lz7q4r0gwx4cq3d4j38i13d5q6wcm7wrd")))

(define-public crate-async-rate-limit-0.0.4 (c (n "async-rate-limit") (v "0.0.4") (d (list (d (n "tokio") (r "^1.29.1") (f (quote ("full" "test-util"))) (d #t) (k 0)))) (h "0mk89dlj63inhzjy66s3gv68ramd2qdv73pz93kqdprxj4p65i3j")))

