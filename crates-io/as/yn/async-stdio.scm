(define-module (crates-io as yn async-stdio) #:use-module (crates-io))

(define-public crate-async-stdio-0.0.0 (c (n "async-stdio") (v "0.0.0") (h "1pcfc70lllw8y39b6h94nngfjasb359bfq4k7y13cazjxyis99ip")))

(define-public crate-async-stdio-0.3.0-alpha.0 (c (n "async-stdio") (v "0.3.0-alpha.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "0hpvs8cq44xs8lhpnrm40vz96gxi5mjh19inbsg3jwa85aqm637g")))

(define-public crate-async-stdio-0.3.0-alpha.1 (c (n "async-stdio") (v "0.3.0-alpha.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "1h527l3525q1pxvs2qqw59rb80sc0sp6521f4fp8g15z2d9gw785")))

(define-public crate-async-stdio-0.3.0-alpha.2 (c (n "async-stdio") (v "0.3.0-alpha.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)))) (h "1kbifzxzjsip733kzm7fw4lma1r2cl5pdb7sys7zqhclf8ikpp1n")))

(define-public crate-async-stdio-0.3.0-alpha.3 (c (n "async-stdio") (v "0.3.0-alpha.3") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "thread_local") (r "^0.3.6") (d #t) (k 0)))) (h "1c7hfqijkkfawnms86w2sdmwyhppjhi9fgsd9x7c0s1zmwjh62zy")))

(define-public crate-async-stdio-0.3.0-alpha.4 (c (n "async-stdio") (v "0.3.0-alpha.4") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)) (d (n "thread_local") (r "^1.0.0") (d #t) (k 0)))) (h "1gb07vflx64n60vd4qzn3i854r2q4b4h1arg98cxy6i1zqg402l7")))

