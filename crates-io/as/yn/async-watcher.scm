(define-module (crates-io as yn async-watcher) #:use-module (crates-io))

(define-public crate-async-watcher-0.1.0 (c (n "async-watcher") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1zrv8ym0fbaysjaasaakzgm2l8qqlsvkfp9li67gmshlibigxmax")))

(define-public crate-async-watcher-0.1.1 (c (n "async-watcher") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0a7id7cxfmcxk8yhx92qr9imgiyi1j3ib6hn2ibkgag0nw2j6n8z")))

(define-public crate-async-watcher-0.2.0 (c (n "async-watcher") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "05wmddhr98h15y3q982psak5qvwws0khdbil0bg0giaxi8s0pjxs")))

(define-public crate-async-watcher-0.2.1 (c (n "async-watcher") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1yz1ffrp40sqqw7ynym40i80kyzd3z6s96f1njvxc3nz5y80vjy1")))

(define-public crate-async-watcher-0.3.0 (c (n "async-watcher") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1jx0pffyl6siyj0c2kyzskd655s6kbbp0wk4rwgrsv08inb5qxvl")))

