(define-module (crates-io as yn async-read-util) #:use-module (crates-io))

(define-public crate-async-read-util-0.1.0 (c (n "async-read-util") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "smol-potat") (r "^1.1.2") (d #t) (k 2)))) (h "1vx3952q6hs2y32bygjj0wk2xa8p6hfr2h0nni1r12fvjilwj8qg")))

(define-public crate-async-read-util-0.2.0 (c (n "async-read-util") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "smol-potat") (r "^1.1.2") (d #t) (k 2)))) (h "05cz3j5nc4lmnskr477zb17av8zjmpqvap7mf9f4pk4mvilcyk9c")))

