(define-module (crates-io as yn async_event_streams) #:use-module (crates-io))

(define-public crate-async_event_streams-0.1.0 (c (n "async_event_streams") (v "0.1.0") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "12xkkv61a8mvjc8apgqsw1pjg6xlh9i7gz8a9dbd20fiv37zag81")))

(define-public crate-async_event_streams-0.1.1 (c (n "async_event_streams") (v "0.1.1") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "0irf14shfmvjrgzp64dbql1c4dypjqrky2dxh1h2kvrn7pha38z8")))

(define-public crate-async_event_streams-0.1.2 (c (n "async_event_streams") (v "0.1.2") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "16n84l4v413rxgfr0s16wl1xhq1s1r1x3iy7ahlaldzkgzy4m8ih")))

(define-public crate-async_event_streams-0.1.3 (c (n "async_event_streams") (v "0.1.3") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "async_event_streams_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "1djxkhpnazvx7qw2h76j3pmm5k5ral35ps95szgxdm1qq0695flj")))

(define-public crate-async_event_streams-0.1.4 (c (n "async_event_streams") (v "0.1.4") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "async-std") (r "^1.11.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "async_event_streams_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "1yjl5ryrfz2shnfiyxvn6vnc6i1m2lbkk071kaky7kn4ipf5vyzr")))

