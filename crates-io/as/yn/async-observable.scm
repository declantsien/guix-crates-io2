(define-module (crates-io as yn async-observable) #:use-module (crates-io))

(define-public crate-async-observable-0.1.0 (c (n "async-observable") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)))) (h "0dbjdv6cvyi9v5w557l3llmns5dwvma779hbwdpzhvkb875k2617")))

(define-public crate-async-observable-0.2.0 (c (n "async-observable") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)))) (h "155h171p3xyibrjpqpnvgqibam36fnj6f1kal3sqm8jm1bp593g9")))

(define-public crate-async-observable-0.2.1 (c (n "async-observable") (v "0.2.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0qvbjar3jc8rvsp3vggwn3gilzlb4mvils560603605inmq02kcp")))

(define-public crate-async-observable-0.3.0 (c (n "async-observable") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0r8i39jgiykicc1wilbgk0wl0z3j6x58ypjaxmxfvqngdy091qm8")))

(define-public crate-async-observable-0.4.0 (c (n "async-observable") (v "0.4.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1pvj4aad758s07xnmr46xflgjfp2621avp76jzcj2wmvzr2y30jl")))

(define-public crate-async-observable-0.4.1 (c (n "async-observable") (v "0.4.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "152x04h30cvin4max2zs812ss7dxqr86lzzz75vpgzvzn555fvd7")))

(define-public crate-async-observable-0.4.2 (c (n "async-observable") (v "0.4.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "07j2kkg0fw5vq2ywb31ng8ya8yfr2fc0piy68bnrwkc4g010ll4y")))

