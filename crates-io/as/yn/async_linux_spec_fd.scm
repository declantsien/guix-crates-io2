(define-module (crates-io as yn async_linux_spec_fd) #:use-module (crates-io))

(define-public crate-async_linux_spec_fd-0.1.0 (c (n "async_linux_spec_fd") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("net" "rt" "macros"))) (d #t) (k 0)))) (h "0vdkhcsn95yx1qzym2mpnzl7fhlyg25yd99l0fynsmfgnlyvk2ik")))

(define-public crate-async_linux_spec_fd-0.1.1 (c (n "async_linux_spec_fd") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("net" "rt" "macros"))) (d #t) (k 0)))) (h "1l079423ys7icq2xzhxdlik8vdqxnx2z04m6c0cg8scrzss1slmv")))

(define-public crate-async_linux_spec_fd-0.1.2 (c (n "async_linux_spec_fd") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.7") (f (quote ("net" "rt" "macros"))) (d #t) (k 0)))) (h "07qfjdab3r0b7kp45d6mbkmn9fg7ni5c0knfk35svrdd61c0falp")))

