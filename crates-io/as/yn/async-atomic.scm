(define-module (crates-io as yn async-atomic) #:use-module (crates-io))

(define-public crate-async-atomic-0.1.0 (c (n "async-atomic") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "atomic") (r "^0.5.1") (k 0)) (d (n "futures") (r "^0.3.26") (k 0)))) (h "0j6c2chmjg0jm6cdbdsjg81rlxwmyrv71nvzpr7qmsa5cvy6xjvq") (f (quote (("std" "atomic/std" "futures/std") ("fallback" "atomic/fallback") ("default" "std" "fallback"))))))

(define-public crate-async-atomic-0.1.1 (c (n "async-atomic") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "atomic") (r "^0.5.1") (k 0)) (d (n "futures") (r "^0.3.26") (k 0)))) (h "1gjrw1zm4nd8kzf0h8aq1figbav2yz4qp5adi5k0484pbq90l1hh") (f (quote (("std" "alloc" "atomic/std" "futures/std") ("fallback" "atomic/fallback") ("default" "std" "fallback") ("alloc"))))))

(define-public crate-async-atomic-0.1.2 (c (n "async-atomic") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "atomic") (r "^0.5.1") (k 0)) (d (n "futures") (r "^0.3.26") (k 0)))) (h "0d938ng5j11hddyvh4rvl0ca79g5k8a9y3wladix47wv7xj05ar0") (f (quote (("std" "alloc" "atomic/std" "futures/std") ("fallback" "atomic/fallback") ("default" "std" "fallback") ("alloc"))))))

(define-public crate-async-atomic-0.1.3 (c (n "async-atomic") (v "0.1.3") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "atomic") (r "^0.5.1") (k 0)) (d (n "futures") (r "^0.3.26") (k 0)))) (h "0vzr3w747yqq0x57cp0wsnk9hmlx4592a9ycg5y2ism2qwpjwq35") (f (quote (("std" "alloc" "atomic/std" "futures/std") ("fallback" "atomic/fallback") ("default" "std" "fallback") ("alloc"))))))

(define-public crate-async-atomic-0.1.4 (c (n "async-atomic") (v "0.1.4") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "atomic") (r "^0.5.1") (k 0)) (d (n "futures") (r "^0.3.26") (k 0)))) (h "1jz7v669i01fih6pqcvs57psfdxmmsf2snhc7dsa5h0s25rb3wk6") (f (quote (("std" "alloc" "atomic/std" "futures/std") ("fallback" "atomic/fallback") ("default" "std" "fallback") ("alloc"))))))

(define-public crate-async-atomic-0.1.5 (c (n "async-atomic") (v "0.1.5") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "atomic") (r "^0.5.1") (k 0)) (d (n "futures") (r "^0.3.26") (k 0)))) (h "0a3kc6lf1mblwpdzvp2ffh0bcy9gbnzpfhz6fbik3b77zr4swz4r") (f (quote (("std" "alloc" "atomic/std" "futures/std") ("fallback" "atomic/fallback") ("default" "std" "fallback") ("alloc"))))))

