(define-module (crates-io as yn async-bybit-ws) #:use-module (crates-io))

(define-public crate-async-bybit-ws-0.0.1 (c (n "async-bybit-ws") (v "0.0.1") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-tls") (r "^0.11.0") (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.15.0") (f (quote ("async-std-runtime" "async-tls"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tungstenite") (r "^0.15.0") (d #t) (k 0)))) (h "1sqsfxkh219yhlr6jri96p3kgx9in2mp0z00xv18gvavr8lg1w1i")))

