(define-module (crates-io as yn async_logger) #:use-module (crates-io))

(define-public crate-async_logger-0.1.0 (c (n "async_logger") (v "0.1.0") (h "1g5p0rjx7gfqfrxq6gy9y2rh5zwb7zi27bmkf6r0psl18c0n4nkd")))

(define-public crate-async_logger-0.1.1 (c (n "async_logger") (v "0.1.1") (h "1vwfkdkfa6df2p1iajzwi5a3q5a8zr32s2xx5jzgn1lsqskvbc98")))

(define-public crate-async_logger-0.2.0 (c (n "async_logger") (v "0.2.0") (h "045vh1rjsw9c8ssqzcd5ipk7lg5ycwp18b6c808j836s6kzv26sx")))

(define-public crate-async_logger-0.2.1 (c (n "async_logger") (v "0.2.1") (h "1n652cn40y635yp7589xy40499faylv5nna771q7ab0sf582z3f5")))

(define-public crate-async_logger-0.2.2 (c (n "async_logger") (v "0.2.2") (h "1g1dpnp3233naykhi7121dxi98v8ygijgvp0jrw0aqnyicsqxy36") (y #t)))

(define-public crate-async_logger-0.2.3 (c (n "async_logger") (v "0.2.3") (h "1l7qm4fdk2h9m0vhvmhm521y14jq6mxvy1kss0irf4y0xy33r7xr")))

(define-public crate-async_logger-0.2.4 (c (n "async_logger") (v "0.2.4") (h "122d52ayyj1j0wvkd2b8yn8v68s6138bpl00hvg492gibk17vin7")))

(define-public crate-async_logger-0.2.5 (c (n "async_logger") (v "0.2.5") (h "1qjb5nrlirmxhakrjfy9mbh0k5lpanf2nfz7d8b6swx7x7bq600n") (f (quote (("metrics"))))))

(define-public crate-async_logger-0.2.6 (c (n "async_logger") (v "0.2.6") (h "0pgylmnri6wkkcb3wr031ap0w45cbhnf8czcf49a0jg867m74i6r") (f (quote (("metrics"))))))

(define-public crate-async_logger-0.2.7 (c (n "async_logger") (v "0.2.7") (h "1kj7zrfl8lh6avw1pfbamnqba6c02iif005hw9vb9bqk8av0q5d0") (f (quote (("metrics"))))))

(define-public crate-async_logger-0.3.0 (c (n "async_logger") (v "0.3.0") (h "0di8z9siqrpg0id07kwh0f9c857zl7ck424cln53bqxsnbria5v8") (f (quote (("metrics")))) (y #t)))

(define-public crate-async_logger-0.3.1 (c (n "async_logger") (v "0.3.1") (h "0xg0g3dkqb32nnz9nm7dxiw1gh720sk97h6129kz10rg6ljgdik2") (f (quote (("metrics"))))))

(define-public crate-async_logger-0.3.2 (c (n "async_logger") (v "0.3.2") (h "06j5kpkgy92b6hvfbajplai93fh8dpd9vw06wn0b1djgn970w8zf") (f (quote (("metrics"))))))

(define-public crate-async_logger-0.3.3 (c (n "async_logger") (v "0.3.3") (h "1gw08ciplhczbz0jaz3003ykvwb8b4pz9b2608xdyiy01qmwjmpj") (f (quote (("metrics"))))))

