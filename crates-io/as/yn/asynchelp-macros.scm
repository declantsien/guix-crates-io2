(define-module (crates-io as yn asynchelp-macros) #:use-module (crates-io))

(define-public crate-asynchelp-macros-0.1.0 (c (n "asynchelp-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1awrqkcpmhf02m9cazim1kk182w5g8cyryw2wx8ailvm1yziajvz") (f (quote (("tarpc") ("stackfuture"))))))

