(define-module (crates-io as yn async-shutdown) #:use-module (crates-io))

(define-public crate-async-shutdown-0.1.0-alpha1 (c (n "async-shutdown") (v "0.1.0-alpha1") (h "1nnmnykxxvq8al00y6l5nl978n70a8v18s809yq2441q0v8xd8k6")))

(define-public crate-async-shutdown-0.1.0-alpha2 (c (n "async-shutdown") (v "0.1.0-alpha2") (h "1fvqbvan076imm54b7ikdx626dklr60j66xjv4qrzmkcgvyv6i5f")))

(define-public crate-async-shutdown-0.1.0-alpha3 (c (n "async-shutdown") (v "0.1.0-alpha3") (h "0jhwd5rcs250ryy29hb6a9ji337xxvn7s72ify7bqrcapdzkn5n7")))

(define-public crate-async-shutdown-0.1.0-alpha4 (c (n "async-shutdown") (v "0.1.0-alpha4") (h "1q60fynym8yh6flfr6za3bkh3p46gcfk6ppdwymg0cmszn6rwjvh")))

(define-public crate-async-shutdown-0.1.0-beta1 (c (n "async-shutdown") (v "0.1.0-beta1") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0br3zqiilhkhhv2vki5mpg4pvhdrpkz7a9aa5h96pm3gskhkmj3r")))

(define-public crate-async-shutdown-0.1.0-beta2 (c (n "async-shutdown") (v "0.1.0-beta2") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 2)))) (h "06fnpjxydllwyvkxsvad8x5lphizqrchwci3463jzp66xrzifbsb")))

(define-public crate-async-shutdown-0.1.0 (c (n "async-shutdown") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("io-util" "macros" "net" "rt-multi-thread" "signal" "time"))) (d #t) (k 2)))) (h "01cwbycgk9h0z5s9jf0482adsh659l3md8g70132aq60hwch0qpl") (y #t)))

(define-public crate-async-shutdown-0.1.1 (c (n "async-shutdown") (v "0.1.1") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("io-util" "macros" "net" "rt-multi-thread" "signal" "time"))) (d #t) (k 2)))) (h "0igpa8hj1yp9q7gw8kdp0b93zlksi1250a65ca5mw6g9snigzca4") (y #t)))

(define-public crate-async-shutdown-0.1.2 (c (n "async-shutdown") (v "0.1.2") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("io-util" "macros" "net" "rt-multi-thread" "signal" "time"))) (d #t) (k 2)))) (h "1zr357rg5f45jaw4z4a0ga42q9cc23y7l7r04agniqvjjxc301ba") (y #t)))

(define-public crate-async-shutdown-0.1.3 (c (n "async-shutdown") (v "0.1.3") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("io-util" "macros" "net" "rt-multi-thread" "signal" "time"))) (d #t) (k 2)))) (h "1zkpabryz20cg8rsfcv13xlgv8wp9hwkrg04kwc8vwb71rfmpdh9") (y #t)))

(define-public crate-async-shutdown-0.2.0 (c (n "async-shutdown") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("io-util" "macros" "net" "rt-multi-thread" "signal" "time"))) (d #t) (k 2)))) (h "1dk30q89q90pdwd2mk7qsnz5527r7apmgzg2silzd9fkjz8ilpxb") (y #t)))

(define-public crate-async-shutdown-0.2.1 (c (n "async-shutdown") (v "0.2.1") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("io-util" "macros" "net" "rt-multi-thread" "signal" "time"))) (d #t) (k 2)))) (h "07b0wd7b5d7cvqazk1rwwxf426fm40yv8ycmp8a8j3bgdz942kv8") (y #t)))

(define-public crate-async-shutdown-0.2.2 (c (n "async-shutdown") (v "0.2.2") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("io-util" "macros" "net" "rt-multi-thread" "signal" "time"))) (d #t) (k 2)))) (h "0d794hfr4fg490wx9qd980rzl718hkbp6yp7dr61z84v7l6s0gzi")))

(define-public crate-async-shutdown-0.1.4 (c (n "async-shutdown") (v "0.1.4") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "tokio") (r "^1.12.0") (f (quote ("io-util" "macros" "net" "rt-multi-thread" "signal" "time"))) (d #t) (k 2)))) (h "10arfagbfhnzqgjsj0jl7x1lkiw5papf8k9fisqcfnp4dm2baxk9")))

