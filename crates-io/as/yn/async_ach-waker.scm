(define-module (crates-io as yn async_ach-waker) #:use-module (crates-io))

(define-public crate-async_ach-waker-0.1.0 (c (n "async_ach-waker") (v "0.1.0") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)))) (h "0z2ylpfh5816bmwdmxnkxmjlcf4gr1qw2zqfl9r0sg1av5akibly")))

(define-public crate-async_ach-waker-0.1.1 (c (n "async_ach-waker") (v "0.1.1") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)))) (h "1j75brr0s4m0rcp5cpcvkk15w1nds8rz6n40c79ib6hmg5flflbw")))

(define-public crate-async_ach-waker-0.1.2 (c (n "async_ach-waker") (v "0.1.2") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)))) (h "023ls5kg2b3zl34vq33yyn0c8czz2q2dn4hpzlqx4y1ny3qkawry")))

(define-public crate-async_ach-waker-0.1.3 (c (n "async_ach-waker") (v "0.1.3") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)))) (h "1b6rkrvq6qdqd7h5nj8j57rgykwhina82jpq673g8njfdmk09val")))

(define-public crate-async_ach-waker-0.2.1 (c (n "async_ach-waker") (v "0.2.1") (d (list (d (n "ach-array") (r "^0.1") (d #t) (k 0)) (d (n "ach-linked") (r "^0.2") (d #t) (k 0)) (d (n "ach-option") (r "^0.1") (d #t) (k 0)))) (h "1baln2xribvfbyyxnasf5sbkrj9m1gjpgg98bgm56mhr4hzws01y")))

