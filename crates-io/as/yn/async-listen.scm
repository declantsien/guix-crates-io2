(define-module (crates-io as yn async-listen) #:use-module (crates-io))

(define-public crate-async-listen-0.1.0 (c (n "async-listen") (v "0.1.0") (d (list (d (n "async-std") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1ckchy6kfppkp07wij96ydiwxmk74k8vhin0dg9g0v0qqc1im47g")))

(define-public crate-async-listen-0.1.1 (c (n "async-listen") (v "0.1.1") (d (list (d (n "async-std") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0g05hyzdf2cf1jqdmsc41gsi3yzbp9bkjn9qjk1x06979rsvi09l")))

(define-public crate-async-listen-0.1.2 (c (n "async-listen") (v "0.1.2") (d (list (d (n "async-std") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0jnmbjc8y1q46y5n91lrn5bxbcyfz3cx6fyql2729m527pj482dy")))

(define-public crate-async-listen-0.1.3 (c (n "async-listen") (v "0.1.3") (d (list (d (n "async-std") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0plbg2fwqkc5hng571d5sszsyk4p78sw20f6n62hinvx08swznqs") (y #t)))

(define-public crate-async-listen-0.1.4 (c (n "async-listen") (v "0.1.4") (d (list (d (n "async-std") (r "^1.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1vph0mkbvwp7r94zzhr0plm1knjniid603fwa9iari44k51wygvy")))

(define-public crate-async-listen-0.1.5 (c (n "async-listen") (v "0.1.5") (d (list (d (n "async-std") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1gz8rmc3nnfx2gniz0gvvsdlhr49mg0hjrarxlqqpf6dmhryr61d")))

(define-public crate-async-listen-0.2.0 (c (n "async-listen") (v "0.2.0") (d (list (d (n "async-std") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0l174y6pbj21ng9d88n4m46w662bf4s84ra10nw1g8zwk7nyghhh")))

(define-public crate-async-listen-0.2.1 (c (n "async-listen") (v "0.2.1") (d (list (d (n "async-std") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0aha1lrrn00pm2cfmmp63980pxdwl1acqljrna7q1grxglfz3vy0")))

