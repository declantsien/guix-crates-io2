(define-module (crates-io as yn async_fn) #:use-module (crates-io))

(define-public crate-async_fn-0.0.1-rc1 (c (n "async_fn") (v "0.0.1-rc1") (d (list (d (n "async_fn-proc_macros") (r "^0.0.1-rc1") (d #t) (k 0)))) (h "1ylnf39zr5qm2n7m6dvi7n4a8gwyjb0csncb8sbddhvb1qz2v9cd") (f (quote (("better-docs"))))))

(define-public crate-async_fn-0.0.1-rc2 (c (n "async_fn") (v "0.0.1-rc2") (d (list (d (n "async_fn-proc_macros") (r "^0.0.1-rc2") (d #t) (k 0)))) (h "1jzcpa8k72cfjn0bh9yhznzq5kjpnnvld73nlnyyi7p2nk22yan4") (f (quote (("better-docs"))))))

(define-public crate-async_fn-0.0.1 (c (n "async_fn") (v "0.0.1") (d (list (d (n "async_fn-proc_macros") (r "^0.0.1") (d #t) (k 0)))) (h "0lms3mv66b779lggvl394w2dm48zc4v97cwllanfmqkkap24jvnm") (f (quote (("better-docs"))))))

(define-public crate-async_fn-0.0.2-rc1 (c (n "async_fn") (v "0.0.2-rc1") (d (list (d (n "async_fn-proc_macros") (r "^0.0.2-rc1") (d #t) (k 0)) (d (n "extreme") (r "^666.666.666666") (d #t) (k 2)))) (h "1i8jmhdckjji7d3j7sabfzjw4kkp7z4kjn0j80jy6k4p8b7kz1wi") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

(define-public crate-async_fn-0.0.2 (c (n "async_fn") (v "0.0.2") (d (list (d (n "async_fn-proc_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "extreme") (r "^666.666.666666") (d #t) (k 2)))) (h "1ppg77855dvdayd5q7s74cdxzlqcq30kic3hm739hh2flr7q5373") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

