(define-module (crates-io as yn async-drop-guard) #:use-module (crates-io))

(define-public crate-async-drop-guard-0.0.1 (c (n "async-drop-guard") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "09i7my7cr0lnw2frh6jvhhww62m3baflayqbsgcmhb12idn9yqhn")))

(define-public crate-async-drop-guard-0.0.2 (c (n "async-drop-guard") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0y63341kp4h0jkj42yhs6p5lg01vklrsr30hgjw9w9lnshkjbcda")))

(define-public crate-async-drop-guard-0.0.3 (c (n "async-drop-guard") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "06xmnjg6slmrfbz42ysb2dxv80v6clskvhxm8np4qn9hxz033gqi")))

(define-public crate-async-drop-guard-0.0.4 (c (n "async-drop-guard") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "13ns685wc35ap6xxmyji059hdx7mn4n243jz37d2qk6b0y7kj040")))

(define-public crate-async-drop-guard-0.0.5 (c (n "async-drop-guard") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0jfg7q9b0z00xz45dr5pck98clpa7f0sbm2wfixq4yqsv3jan2kl")))

(define-public crate-async-drop-guard-0.0.6 (c (n "async-drop-guard") (v "0.0.6") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0xrbbai7f1gvb38s6siyc7gwbzigb51whp57clg85jblf2chcv4b")))

