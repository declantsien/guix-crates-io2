(define-module (crates-io as yn async-io-helper) #:use-module (crates-io))

(define-public crate-async-io-helper-0.1.0 (c (n "async-io-helper") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "net" "macros" "io-util"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "1whbk3nm0gwahax14nrr69ys5inz3bw3kv8x81nl57y1icdzlrhv")))

