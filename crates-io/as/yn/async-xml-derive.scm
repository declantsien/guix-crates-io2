(define-module (crates-io as yn async-xml-derive) #:use-module (crates-io))

(define-public crate-async-xml-derive-0.1.0 (c (n "async-xml-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rhgbrhfpaixvi98i1w1ccypibzsi9g20a2xlys1fm8kwgcadx4i")))

(define-public crate-async-xml-derive-0.1.1 (c (n "async-xml-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kqgwlnpg6zrhxmrl9zshplv4i4hn2sificyqashv8nr7pnrdkn2")))

(define-public crate-async-xml-derive-0.2.0 (c (n "async-xml-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kks39s1ch47bjqhg9mwbgxdlg7v7dp3xnnlnbckhddlx925jfy2")))

(define-public crate-async-xml-derive-0.2.1 (c (n "async-xml-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bhly5999hzn3v3ai97mli3m9lzrdyrp75zqkfgagvn6hnlr7lcq")))

(define-public crate-async-xml-derive-0.2.2 (c (n "async-xml-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v4b5z6w1byq5x4q0xgp97gsvsva1wk0a6811nw8d3dm8i787kfm")))

(define-public crate-async-xml-derive-0.2.3 (c (n "async-xml-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cw0027f64faddvb9q2lyf1025x145ldigas7n6aca5nhd1nfmlx")))

