(define-module (crates-io as yn async-generic) #:use-module (crates-io))

(define-public crate-async-generic-0.1.0 (c (n "async-generic") (v "0.1.0") (d (list (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "1k69bg15aqqdx7hwpfm05ksskppxs007yrgj87x475r067805a6j") (r "1.71.0")))

(define-public crate-async-generic-0.1.1 (c (n "async-generic") (v "0.1.1") (d (list (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "1l7yjjcbpwlrxsr8z5f917x98fh866qir5ahpwk70lb4lm7xmx0h") (r "1.71.0")))

(define-public crate-async-generic-0.1.2 (c (n "async-generic") (v "0.1.2") (d (list (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "1rqqhcncg9pbhx1q7ri4db865pihgjlpl19475jwfzm6bk8h3g46") (r "1.67.0")))

(define-public crate-async-generic-1.0.0 (c (n "async-generic") (v "1.0.0") (d (list (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "07a28db2d94j0nwy9gyfzr2vbm5l8hj8rcf8wkn6iyd4ynn37h55") (r "1.67.0")))

(define-public crate-async-generic-1.1.0 (c (n "async-generic") (v "1.1.0") (d (list (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "0y3py2y3ksw5b7yq0bql69dvg8pqisixx5pnz16pp45hjr4939jq") (r "1.70.0")))

