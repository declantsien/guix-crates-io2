(define-module (crates-io as yn async_flag) #:use-module (crates-io))

(define-public crate-async_flag-0.1.0 (c (n "async_flag") (v "0.1.0") (d (list (d (n "atomic") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "0ffsl0iks3fb1h5b92yd16pxl4f0k3k4803yksy1xy5mwmf2cxlh")))

(define-public crate-async_flag-0.1.1 (c (n "async_flag") (v "0.1.1") (d (list (d (n "atomic") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "1hkvlw9am0n5aif4hgnqr7820f3mmxqdn1pkwpjqz3y07gjqswk5")))

(define-public crate-async_flag-0.1.2 (c (n "async_flag") (v "0.1.2") (d (list (d (n "atomic") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "1xbya7jrmcmd5maqwhch6rwkx6pf2fwdnx14bz8m67i5xcn63c6i")))

(define-public crate-async_flag-0.2.0 (c (n "async_flag") (v "0.2.0") (d (list (d (n "atomic") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros" "time"))) (d #t) (k 2)))) (h "10n0d3ngs19hq6cw9axxikwigg5sx8rhbzgkghrlgbs1gh2vn8vq")))

