(define-module (crates-io as yn async-ready) #:use-module (crates-io))

(define-public crate-async-ready-1.0.0 (c (n "async-ready") (v "1.0.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 2)))) (h "188c9r7fxfrq19m6bl8k5p3zjwvdi8f49p53znb8slkarv6hf60c")))

(define-public crate-async-ready-1.0.1 (c (n "async-ready") (v "1.0.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 2)))) (h "1q786i7y4amxqqlhnkykzvv460f6k9v2fljswh824g38k3vwf7vf")))

(define-public crate-async-ready-2.0.0 (c (n "async-ready") (v "2.0.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 2)))) (h "1b90g9ss4dglyk5gy9dcyk45a0brml1bnxahwd00a0hmzq8s6nvr")))

(define-public crate-async-ready-2.0.1 (c (n "async-ready") (v "2.0.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 2)))) (h "16lfm26jkn1cjm76g4mald9c1kd19c4l1vykyfi90kiqf6vmy6ax")))

(define-public crate-async-ready-2.1.0 (c (n "async-ready") (v "2.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 2)))) (h "0lz7n0mcjldl30xkr7p3jf9y43vzx5sbrxci4p4dbvklys40ysdv")))

(define-public crate-async-ready-3.0.0 (c (n "async-ready") (v "3.0.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.14") (d #t) (k 2)))) (h "09xw34q0k48r1bvs3s1l2a1mglz98l7zjbkdcy861k8zsyfwfw4l")))

