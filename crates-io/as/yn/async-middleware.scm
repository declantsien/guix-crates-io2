(define-module (crates-io as yn async-middleware) #:use-module (crates-io))

(define-public crate-async-middleware-0.1.0 (c (n "async-middleware") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)))) (h "0cmchnw4vx4f8lcl3cwkqr8lbcag3zg7qd8fb01d7ld3yf9c8lcv")))

(define-public crate-async-middleware-1.0.0 (c (n "async-middleware") (v "1.0.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)))) (h "1qdf0ayn0yppi0xlp5cs3af9wni7g81pxipgradfw8a88lvckxs8")))

