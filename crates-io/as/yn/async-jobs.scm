(define-module (crates-io as yn async-jobs) #:use-module (crates-io))

(define-public crate-async-jobs-0.1.0 (c (n "async-jobs") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)))) (h "1p3g4lzk69gq7n3sh9k0cqrk3kxmhfiix7ny5m9gqr458wp55ggb")))

(define-public crate-async-jobs-0.2.0 (c (n "async-jobs") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "1x9wjh6cgj15i3dcvji7r9548733jg301mr17qmb2bqiv81a1ga6")))

