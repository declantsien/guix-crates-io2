(define-module (crates-io as yn async-ssh) #:use-module (crates-io))

(define-public crate-async-ssh-0.1.0 (c (n "async-ssh") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "thrussh") (r "^0.19") (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0gdsv8gjfih1yfz5dy4qcmz5si1w9fx49kl61pkr97dvj2wssl22")))

(define-public crate-async-ssh-0.1.1 (c (n "async-ssh") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "thrussh") (r "^0.19") (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0n5xnp5a015z00zkszpyf5a8x984s155vw6fy2lcs744b15xxa1y")))

(define-public crate-async-ssh-0.1.2 (c (n "async-ssh") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "thrussh") (r "^0.19") (d #t) (k 0)) (d (n "thrussh-keys") (r "^0.9") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0rj3capibr2nmqnqrzin117b43qj1fpj9x2fl1j8v9li0p3rx379")))

