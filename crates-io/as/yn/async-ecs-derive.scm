(define-module (crates-io as yn async-ecs-derive) #:use-module (crates-io))

(define-public crate-async-ecs-derive-0.1.0 (c (n "async-ecs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1s5l2f8mil8wfw6lzk82bhmk6c6gl39qlgp2ck3phxs0bwwqlycr")))

