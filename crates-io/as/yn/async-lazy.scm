(define-module (crates-io as yn async-lazy) #:use-module (crates-io))

(define-public crate-async-lazy-0.1.0 (c (n "async-lazy") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "tokio") (r "^1.28.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "test-util"))) (d #t) (k 2)))) (h "0n9bhp8dfhknhmr4zgngcdkmv0k4q1k88dmlm4pjgmr94kw7ylzq") (f (quote (("parking_lot" "tokio/parking_lot") ("nightly"))))))

