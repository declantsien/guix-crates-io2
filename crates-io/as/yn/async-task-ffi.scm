(define-module (crates-io as yn async-task-ffi) #:use-module (crates-io))

(define-public crate-async-task-ffi-4.1.0 (c (n "async-task-ffi") (v "4.1.0") (d (list (d (n "atomic-waker") (r "^1.0.0") (d #t) (k 2)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "flume") (r "^0.10") (k 2)) (d (n "once_cell") (r "^1.4.1") (d #t) (k 2)) (d (n "smol") (r "^1.0.1") (d #t) (k 2)))) (h "0l89vibl67lwjw7kgs3p3fczlg957p8ln1r2ljsdy91mq1a12xbf") (f (quote (("std") ("default" "std"))))))

(define-public crate-async-task-ffi-4.1.1 (c (n "async-task-ffi") (v "4.1.1") (d (list (d (n "atomic-waker") (r "^1.0.0") (d #t) (k 2)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "flume") (r "^0.10") (k 2)) (d (n "once_cell") (r "^1.4.1") (d #t) (k 2)) (d (n "smol") (r "^1.0.1") (d #t) (k 2)))) (h "0s7b6vg47gdr5mnn8604gwck1rnmww6jcdlgdrigdiaw4my947x4") (f (quote (("std") ("default" "std"))))))

