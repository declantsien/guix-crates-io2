(define-module (crates-io as yn async-ops) #:use-module (crates-io))

(define-public crate-async-ops-1.0.0 (c (n "async-ops") (v "1.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "001gv5r2fn3w63phf01sqx3vhvy4235ldipdv56assknnkamblrc")))

(define-public crate-async-ops-1.1.0 (c (n "async-ops") (v "1.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0) (p "futures-core")) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("async-await-macro"))) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "19rv9z1asa6w08p6mg76binmcw4bm3ds4jbdfwxw2jqm2849p8lg")))

