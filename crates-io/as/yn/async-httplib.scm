(define-module (crates-io as yn async-httplib) #:use-module (crates-io))

(define-public crate-async-httplib-0.1.0 (c (n "async-httplib") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "1grxzc9glka8j77p7mfjc5fpa7iqv4zapgqqwwr1n33rpq51bxp2")))

(define-public crate-async-httplib-0.1.1 (c (n "async-httplib") (v "0.1.1") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0rh9q9xwbbwncaq9713zl7ps9l0rg2jqdm32qbq818bcas6arpkr")))

(define-public crate-async-httplib-0.2.0 (c (n "async-httplib") (v "0.2.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "06zcdifkz0mw73dkrbgyc604zl529qd5wjcckqa3nyw7xc8lq3bf")))

(define-public crate-async-httplib-0.2.1 (c (n "async-httplib") (v "0.2.1") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "1pkvp99ji0101isszvxd0wkwn76nxakv2pxyx9p7dad24kg59d0n")))

(define-public crate-async-httplib-0.3.0 (c (n "async-httplib") (v "0.3.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0ialrids40g3cc0zg9hx1mhaxlyw9x23ynawlzbs9cgxzs16qqg3")))

(define-public crate-async-httplib-0.3.1 (c (n "async-httplib") (v "0.3.1") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0xwf7jrpqxzs80mp9nx9wfpyr6wbfs467lqama6xdsgdna9j1wl7")))

(define-public crate-async-httplib-0.4.0 (c (n "async-httplib") (v "0.4.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "1nagyxsgylhakb2hzb27n3ll9byliklz36f97n43p6kxd54d7p6l")))

(define-public crate-async-httplib-0.5.0 (c (n "async-httplib") (v "0.5.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0pcjnhm0pmhhlc6fp9ilpyvkgajdm8hgj4rj5kx1h2xg0fh2njj2")))

