(define-module (crates-io as yn async-wasm) #:use-module (crates-io))

(define-public crate-async-wasm-0.1.0 (c (n "async-wasm") (v "0.1.0") (h "0qfd6s729lwd618smdj0871vk41msaaylsx8lr8z8nfgyyj8fsxf") (y #t)))

(define-public crate-async-wasm-0.2.0 (c (n "async-wasm") (v "0.2.0") (h "1wzxyd2h4v9xw16wls2g5v586bmyc8ikimx17diwx3nkjqndvs7f")))

