(define-module (crates-io as yn async-liveliness-monitor) #:use-module (crates-io))

(define-public crate-async-liveliness-monitor-0.1.0 (c (n "async-liveliness-monitor") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "palaver") (r "^0.2.8") (d #t) (k 2)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "12563bk8z8k4q7xry20zv9f5br4fqf4d6fw94gv586m76slzfvr0")))

(define-public crate-async-liveliness-monitor-0.1.1 (c (n "async-liveliness-monitor") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "palaver") (r "^0.2.8") (d #t) (k 2)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "0wiw31jx8iirksrpamfpg1nvhnn477b8yi12spa3xdmqq6qp88ch")))

