(define-module (crates-io as yn async-tun) #:use-module (crates-io))

(define-public crate-async-tun-0.1.0 (c (n "async-tun") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1v4ab1qsg5873p83y839j2msfr5dfih9wbkfdgy1hb2gc09pqayg")))

(define-public crate-async-tun-0.2.0 (c (n "async-tun") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "06ynip32dmwn7acsfpykph7q7lbi4b78y38ahj2vgygcy73p6alx")))

(define-public crate-async-tun-0.2.1 (c (n "async-tun") (v "0.2.1") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1dw0pp5lmrks94hgi46bc3w4apjjpip17nf3wsiw1lsnsw08r17y")))

(define-public crate-async-tun-0.3.0 (c (n "async-tun") (v "0.3.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "04wgw0adar18rny1ppvg45mv49igckxm8ihgv8k37xmy7k0yg7p1")))

(define-public crate-async-tun-0.4.0 (c (n "async-tun") (v "0.4.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1kkk7xanv9lav2gxcsnf5yiag7zcv8zbsif5l430bya6j9axyqgm")))

(define-public crate-async-tun-0.5.0 (c (n "async-tun") (v "0.5.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "18mlngcw745g3hywwz7xv0rz2hwrl6cnln0nvgpqcv3zvb6qygmc")))

(define-public crate-async-tun-0.5.1 (c (n "async-tun") (v "0.5.1") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "18d7pa7h2lg0g48cp3h4ry15d70zk1bw4rfhray7vcmmj4fk0971")))

(define-public crate-async-tun-0.6.0 (c (n "async-tun") (v "0.6.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0g91bcdjxgyzm236qm6zny89iwxl5scxyfar8kklf257han8k80w")))

(define-public crate-async-tun-0.7.0 (c (n "async-tun") (v "0.7.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "io-util" "rt-threaded" "macros"))) (o #t) (d #t) (k 0)))) (h "0s5qq63xdglfgs1fpwi584h2bmlx7d9xn94qfabjfdl59k27g4dk") (f (quote (("use-tokio" "tokio"))))))

(define-public crate-async-tun-0.7.1 (c (n "async-tun") (v "0.7.1") (d (list (d (n "async-std") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "io-util" "rt-threaded" "macros"))) (o #t) (d #t) (k 0)))) (h "0mk4xq7cdg82a3mpwd8945gmdq3704n5wqh68f8bgykq9284gxb8") (f (quote (("use-tokio" "tokio") ("default" "async-std"))))))

(define-public crate-async-tun-0.7.2 (c (n "async-tun") (v "0.7.2") (d (list (d (n "async-std") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "io-util" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0mfnlb94ad1ydjs8sx6il6jx8r4i6c2i395n4g895brprm3mylhy") (f (quote (("default" "async-std"))))))

(define-public crate-async-tun-0.8.0 (c (n "async-tun") (v "0.8.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "1acnci5cq7gbzypwwfgvc25y1hnqi4i0bps7ly0jlq1bz3mcpgal")))

(define-public crate-async-tun-0.8.1 (c (n "async-tun") (v "0.8.1") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 0)) (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)))) (h "1farzs3hhpzj1dg65fjqhgsjnaibzkm9gca3m7gfr1w9mkvalbwj")))

(define-public crate-async-tun-0.8.2 (c (n "async-tun") (v "0.8.2") (d (list (d (n "async-std") (r "^1.8") (d #t) (k 0)) (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)))) (h "12iv1pamhg8sx1rb2243nypv6rjya1mvk3h7dim7c5wy6zhdir3l")))

(define-public crate-async-tun-0.8.3 (c (n "async-tun") (v "0.8.3") (d (list (d (n "async-std") (r "^1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)))) (h "1v8hb6cpg3400akinlcbz529aa35s65nf75ni7pdll82farhaqg9")))

(define-public crate-async-tun-0.8.4 (c (n "async-tun") (v "0.8.4") (d (list (d (n "async-std") (r "^1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)))) (h "0awkghdcgc21svw4gxdw230brcydgf7msq6fly96k6jwwq2sm51c")))

(define-public crate-async-tun-0.8.5 (c (n "async-tun") (v "0.8.5") (d (list (d (n "async-std") (r "^1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.21") (d #t) (k 0)))) (h "0fd9kxz0v172gcr8fg2cjjrwmdw0sy117x70612m5zailjpqxhpi")))

(define-public crate-async-tun-0.9.0 (c (n "async-tun") (v "0.9.0") (d (list (d (n "async-std") (r "^1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mac_address") (r "^1.1") (d #t) (k 0)) (d (n "nix") (r "^0.21") (d #t) (k 0)))) (h "1c18i3k4vhlhy5sz31z7lqp33pwrdmskcbg8kk75fjv2a2j2jjc6")))

(define-public crate-async-tun-0.10.0 (c (n "async-tun") (v "0.10.0") (d (list (d (n "async-std") (r "^1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mac_address") (r "^1.1") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1q3snxc8ij01f65h6vqhznkys65iipn71dpsz2zn28yk91d1rk94")))

(define-public crate-async-tun-0.10.1 (c (n "async-tun") (v "0.10.1") (d (list (d (n "async-std") (r "^1.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mac_address") (r "^1.1") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)))) (h "05y18cqby7mpr290wxkbdpzzm61llk7k3bc53182ypj1b5v03q51")))

