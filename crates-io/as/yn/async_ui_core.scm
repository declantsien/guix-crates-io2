(define-module (crates-io as yn async_ui_core) #:use-module (crates-io))

(define-public crate-async_ui_core-0.1.0 (c (n "async_ui_core") (v "0.1.0") (d (list (d (n "async-executor") (r "^1.4.1") (d #t) (k 0)) (d (n "async-task") (r "^4.3.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.8") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)) (d (n "scoped_async_spawn") (r "^0.1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.7") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (f (quote ("union" "const_generics"))) (d #t) (k 0)))) (h "1kavcgd3ydkgwkayk9im8v3yvizlym92gppszllx1lwypfvrkh9k")))

