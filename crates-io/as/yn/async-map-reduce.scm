(define-module (crates-io as yn async-map-reduce) #:use-module (crates-io))

(define-public crate-async-map-reduce-1.0.1 (c (n "async-map-reduce") (v "1.0.1") (h "12lpxqpp1zvaxv5p54p511bzqk0f1mvwww8wgicswbxkbk33pzjj")))

(define-public crate-async-map-reduce-1.0.2 (c (n "async-map-reduce") (v "1.0.2") (h "0b5j0gdz65yn5l1s2nws2mk8a2pz7m8zlwzwyrpc4jfr9xm8vflv")))

(define-public crate-async-map-reduce-1.1.0 (c (n "async-map-reduce") (v "1.1.0") (h "1qmcdhdzslpkswifpg96k22638sswzik00cd7f6n1m6rl0043slz")))

(define-public crate-async-map-reduce-1.1.1 (c (n "async-map-reduce") (v "1.1.1") (h "0svsxhfc5w2dagj0g7lp61mh8hhy72fa2ki9sfsdfcw184n11190")))

(define-public crate-async-map-reduce-1.1.2 (c (n "async-map-reduce") (v "1.1.2") (h "100mihnx4g4p0bjcfqqwik9j2c4x3h5dz4v9df7sb0r3jl7nv35l")))

