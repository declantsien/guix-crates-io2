(define-module (crates-io as yn async_tasks_recorder) #:use-module (crates-io))

(define-public crate-async_tasks_recorder-0.1.0 (c (n "async_tasks_recorder") (v "0.1.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "1frybjfava1dzfjzblys2xf2gfpiffi5k1vqh2pshanfclw48f32") (y #t)))

(define-public crate-async_tasks_recorder-0.2.0 (c (n "async_tasks_recorder") (v "0.2.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "09ps1npfa316mx20kvi3i8ixbbp121vavpa8ip78afxahsx6124y") (y #t)))

(define-public crate-async_tasks_recorder-0.2.1 (c (n "async_tasks_recorder") (v "0.2.1") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "09mhsplf3mlyfy8rbwps7xgr525scczirmvlhbkagzb27hpa48mi") (y #t)))

(define-public crate-async_tasks_recorder-0.2.2 (c (n "async_tasks_recorder") (v "0.2.2") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "1k4p78m4rnzap46xdsd2s9bp4wzaiihghfnhidg332qg10dk6y3w") (y #t)))

(define-public crate-async_tasks_recorder-0.3.0 (c (n "async_tasks_recorder") (v "0.3.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "0dms4fldcjvl7lnhbkdhn3wzxfh0c44a0yfjx0012z65k7iv1l9s") (y #t)))

(define-public crate-async_tasks_recorder-1.1.0 (c (n "async_tasks_recorder") (v "1.1.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "17292vb38lz8qyh1z8ylbzkxkxvhnp2m3fm6z7swmi8frl5d21md") (y #t)))

(define-public crate-async_tasks_recorder-1.1.1 (c (n "async_tasks_recorder") (v "1.1.1") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "16c56wdg1p0k0kg0zwj3g3r4i6kb510rnigxgg83dh8azcqqm0r7")))

(define-public crate-async_tasks_recorder-1.1.2 (c (n "async_tasks_recorder") (v "1.1.2") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "12ngicg7f8m7s99pw9j7jyhwxim39wawrcr49b5pqkfgdgy8y9nn")))

(define-public crate-async_tasks_recorder-1.1.3 (c (n "async_tasks_recorder") (v "1.1.3") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "0my03hgrwp7khciyrc07g1y2w4kys6fnjkvcz4jblzvksyz5b13z")))

(define-public crate-async_tasks_recorder-1.1.4 (c (n "async_tasks_recorder") (v "1.1.4") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "1xhmz9iy5ians92x6ckra6jj1cxlpcz3gjf9ic4vbg2y6s3ghzrw")))

(define-public crate-async_tasks_recorder-2.0.1 (c (n "async_tasks_recorder") (v "2.0.1") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "1v111jjgvdm1zwghzad9lijc34asf8mcy4cbc864bfrn81pdpd67")))

(define-public crate-async_tasks_recorder-2.0.2 (c (n "async_tasks_recorder") (v "2.0.2") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scc") (r "^2.0") (d #t) (k 0)) (d (n "serial_test") (r "^3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time" "rt-multi-thread" "parking_lot"))) (d #t) (k 2)))) (h "1959y8ffpkfnway586pp2g6as6xrn7ayqhissgrsnrlfbjv4f4i4")))

