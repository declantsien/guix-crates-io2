(define-module (crates-io as yn async-spawner) #:use-module (crates-io))

(define-public crate-async-spawner-1.0.0 (c (n "async-spawner") (v "1.0.0") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3.4") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0rq9smr89b71xgr03msqahhzsgh59wzkdbnkgjrjj5zzcdrjvdpg")))

(define-public crate-async-spawner-2.0.0 (c (n "async-spawner") (v "2.0.0") (d (list (d (n "async-std") (r ">=1.7.0, <2.0.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "futures") (r ">=0.3.8, <0.4.0") (d #t) (k 0)) (d (n "once_cell") (r ">=1.5.2, <2.0.0") (d #t) (k 0)) (d (n "tokio") (r ">=0.3.4, <0.4.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0s0hpf7hx2q35b0k44d5c6bj06cqhnpx9nnnxj3id2kh99s634y3")))

(define-public crate-async-spawner-2.1.0 (c (n "async-spawner") (v "2.1.0") (d (list (d (n "async-channel") (r "^1.5.1") (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (f (quote ("attributes" "unstable"))) (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3.8") (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "tokio") (r "^0.3.4") (f (quote ("macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3.4") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0n9vpdfw6mg5kgm5gk02as4rrd3wym4789a2irxdmhf5dfsqxh4i")))

