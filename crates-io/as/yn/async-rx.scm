(define-module (crates-io as yn async-rx) #:use-module (crates-io))

(define-public crate-async-rx-0.1.0 (c (n "async-rx") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3.28") (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "stream_assert") (r "^0.1.0") (d #t) (k 2)))) (h "1i08dybnjy52z986v81790bjz4l162arq8w4ffc0n24s2srwa3l9")))

(define-public crate-async-rx-0.1.1 (c (n "async-rx") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3.28") (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "stream_assert") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 2)))) (h "0xvm8gch64ymg4y9vy71a8pqf96r922kmzvh52f83fx3mfqcxdwp") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-async-rx-0.1.2 (c (n "async-rx") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.3.28") (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "stream_assert") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 2)))) (h "1zb6ji4wa85icd0iahv32fxv36cj08qv2z0acqyhpw7vslpwnlq7") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-async-rx-0.1.3 (c (n "async-rx") (v "0.1.3") (d (list (d (n "futures-core") (r "^0.3.28") (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "stream_assert") (r "^0.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 2)))) (h "18k88ihl7qkjgnpaxfmpbkm8z4qb1p5acf7pi7ilf2cs6bjy83d3") (f (quote (("default" "alloc") ("alloc"))))))

