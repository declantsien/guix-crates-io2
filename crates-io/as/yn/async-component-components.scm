(define-module (crates-io as yn async-component-components) #:use-module (crates-io))

(define-public crate-async-component-components-0.4.0 (c (n "async-component-components") (v "0.4.0") (d (list (d (n "async-component-core") (r "^0.4.0") (d #t) (k 0)))) (h "166j51aml09n7kn4s0cl2z9nj8fx1rxjpm7cbk84i82y3i41gyg8")))

(define-public crate-async-component-components-0.4.2 (c (n "async-component-components") (v "0.4.2") (d (list (d (n "async-component-core") (r "^0.4.2") (d #t) (k 0)))) (h "1v7j3gf2pfz84j87ava72yapbbxwn135qn3x2hzlvfmdi4vlxqmn")))

(define-public crate-async-component-components-0.5.0 (c (n "async-component-components") (v "0.5.0") (d (list (d (n "async-component-core") (r "^0.5.0") (d #t) (k 0)))) (h "1xwlspwyw5znjn2fdyim4cdy3rbi30wkz6b8cf4f60nk8x3jgqs9")))

(define-public crate-async-component-components-0.5.1 (c (n "async-component-components") (v "0.5.1") (d (list (d (n "async-component-core") (r "^0.5.1") (d #t) (k 0)))) (h "06khfivs1c7knzz5x9x0y374agi7542x8wxg1f4w6g02rknh137a")))

(define-public crate-async-component-components-0.5.2 (c (n "async-component-components") (v "0.5.2") (d (list (d (n "async-component-core") (r "^0.5.2") (d #t) (k 0)))) (h "0dgrqj4dfdjsl66i05gn3ayd5bsqhk0bz98icslxgfa30ghx1mn0")))

(define-public crate-async-component-components-0.5.3 (c (n "async-component-components") (v "0.5.3") (d (list (d (n "async-component-core") (r "^0.5.3") (d #t) (k 0)))) (h "0vympg92bv6q1acqmc9g4j9dbgfvnp710hpc65qk7vxxdwbhc6w0")))

(define-public crate-async-component-components-0.5.4 (c (n "async-component-components") (v "0.5.4") (d (list (d (n "async-component-core") (r "^0.5.4") (d #t) (k 0)))) (h "10ghcy7ryj2zr20gwqqki81p7zzf5d701wn8qzq81svmqvwzz988")))

(define-public crate-async-component-components-0.6.0 (c (n "async-component-components") (v "0.6.0") (d (list (d (n "async-component-core") (r "^0.6.0") (d #t) (k 0)))) (h "0azd0ghp2qfgjza5cf8dnhirbjxh98f4my70k7l43p8h9nds3k5w")))

(define-public crate-async-component-components-0.7.0 (c (n "async-component-components") (v "0.7.0") (d (list (d (n "async-component-core") (r "^0.7.0") (d #t) (k 0)))) (h "1nwh5gg1gbfs4smw6gd8b10b9p3csr7886716rvj2a8iqcda2hjq")))

(define-public crate-async-component-components-0.8.0 (c (n "async-component-components") (v "0.8.0") (d (list (d (n "async-component-core") (r "^0.8.0") (d #t) (k 0)))) (h "14wqfjmr35xv5yvkgr6vnh6m0dqb91vz2yhyy6hh4xxxkmh0a2sk")))

(define-public crate-async-component-components-0.8.1 (c (n "async-component-components") (v "0.8.1") (d (list (d (n "async-component-core") (r "^0.8.1") (d #t) (k 0)))) (h "16qhjqlr0spf9xjgqp6bfwqh4bzin8nab1a9x0rnw1lxciv921cn")))

(define-public crate-async-component-components-0.9.0 (c (n "async-component-components") (v "0.9.0") (d (list (d (n "async-component-core") (r "^0.9.0") (d #t) (k 0)))) (h "0hcpbinlqjby5g44d2y042pffndsgxldh9d2yk34x5ciaizvvghz")))

