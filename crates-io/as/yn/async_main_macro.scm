(define-module (crates-io as yn async_main_macro) #:use-module (crates-io))

(define-public crate-async_main_macro-0.1.0 (c (n "async_main_macro") (v "0.1.0") (h "0sdf4i7diyk0llyvswb8isvf8kz8imlfgdqfbm0mxkzsm55xf8vs")))

(define-public crate-async_main_macro-0.2.0 (c (n "async_main_macro") (v "0.2.0") (h "1vgx0xi34cajbk05m0z31xa9aax4yrj0m43drd53cgm5jhkf68zx") (f (quote (("web") ("default"))))))

(define-public crate-async_main_macro-0.3.0 (c (n "async_main_macro") (v "0.3.0") (h "1c8xrx9mc50fc6pqd6qp8rmsjwfqdqyh8271yqc2f8hhzmrz12bj") (f (quote (("web") ("default"))))))

(define-public crate-async_main_macro-0.4.0 (c (n "async_main_macro") (v "0.4.0") (h "11f7cmmp3rjpvrhh7ks6pl57qr7691chil2jkbg27nxj9afs2q9c") (f (quote (("web") ("default"))))))

