(define-module (crates-io as yn async-resource) #:use-module (crates-io))

(define-public crate-async-resource-0.1.0 (c (n "async-resource") (v "0.1.0") (d (list (d (n "concurrent-queue") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures-channel") (r "^0.3") (d #t) (k 2)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "multitask") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "option-lock") (r "^0.1") (d #t) (k 0)))) (h "0yb9zmql3hls2gqxda84939j79x6dhr1sgib4w87056bi44ynymf") (f (quote (("exec-multitask" "multitask") ("default" "exec-multitask"))))))

