(define-module (crates-io as yn async-test) #:use-module (crates-io))

(define-public crate-async-test-0.0.0 (c (n "async-test") (v "0.0.0") (h "00kp58qg896a1c5hs1p745i9rrl2gpmqnyly6nq8kmsks36a9nb0")))

(define-public crate-async-test-1.0.0 (c (n "async-test") (v "1.0.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1lwfwb1kh0l2ngdasydskx1z1x60ja4p98ig2wwczg5igq8y4x9k")))

