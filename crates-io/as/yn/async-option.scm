(define-module (crates-io as yn async-option) #:use-module (crates-io))

(define-public crate-async-option-0.1.0 (c (n "async-option") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-mock-task") (r "^0.1") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.1") (d #t) (k 0)))) (h "0h85ncr8jkg4zspqxc34y1ksf1jdawd1qygyzidih8licys4jp1a")))

(define-public crate-async-option-0.1.1 (c (n "async-option") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-mock-task") (r "^0.1") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.1") (d #t) (k 0)))) (h "0nv9bw4253529hnyxp3qxxsz7z5icqf46vy0rf5vzh2xjhsy7i4z")))

