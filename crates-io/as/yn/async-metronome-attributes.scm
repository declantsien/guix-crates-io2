(define-module (crates-io as yn async-metronome-attributes) #:use-module (crates-io))

(define-public crate-async-metronome-attributes-0.1.0 (c (n "async-metronome-attributes") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01p6a7q4gsb69qasjhlprgymaixcb02r6yzgc9jxf1jlvcbq5c5s")))

(define-public crate-async-metronome-attributes-0.1.1 (c (n "async-metronome-attributes") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kqdvhx74ngmvqm13syjzfvns08jzfbnb195wjjw7vv04phpmiyj")))

