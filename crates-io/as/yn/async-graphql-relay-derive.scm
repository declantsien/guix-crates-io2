(define-module (crates-io as yn async-graphql-relay-derive) #:use-module (crates-io))

(define-public crate-async-graphql-relay-derive-0.3.0 (c (n "async-graphql-relay-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "16qb3bfxi3pxk94f83ak1kdr0h1w5y25vbrlgi2fz3ki48m7vn4w")))

(define-public crate-async-graphql-relay-derive-0.4.0 (c (n "async-graphql-relay-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.13.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "136wgh7man7rsr89mchlhvx4368v8p5mp1627pp25jw6m92bc7fq")))

