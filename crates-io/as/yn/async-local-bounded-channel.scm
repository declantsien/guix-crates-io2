(define-module (crates-io as yn async-local-bounded-channel) #:use-module (crates-io))

(define-public crate-async-local-bounded-channel-0.1.0 (c (n "async-local-bounded-channel") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "typenum") (r "^1.11") (d #t) (k 2)))) (h "00ssa8rjbwmpwm01dz0ynyjcg5wx6zasijhhsbc6dn0rp5xwc8wz")))

