(define-module (crates-io as yn async-debounce) #:use-module (crates-io))

(define-public crate-async-debounce-0.1.0 (c (n "async-debounce") (v "0.1.0") (d (list (d (n "embassy-time") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)))) (h "0qxp84qryvdvjfiyz45md8rjc1lq43cciwrnhc8vbfd8ic040x2z")))

(define-public crate-async-debounce-0.1.1 (c (n "async-debounce") (v "0.1.1") (d (list (d (n "embassy-time") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)))) (h "0b12pgs248pz75zc8kc33pzvv210nnqf8hn1klxg6102mixnzn2g")))

