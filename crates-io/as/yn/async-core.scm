(define-module (crates-io as yn async-core) #:use-module (crates-io))

(define-public crate-async-core-0.1.0 (c (n "async-core") (v "0.1.0") (h "0qzd9hajyd0hh6mvq4hdgxpd4ld07vqyds5mcb0vbi5pyafrjx6a") (y #t)))

(define-public crate-async-core-0.2.0 (c (n "async-core") (v "0.2.0") (h "0d6vbx0nk35iv9976ahmvqkfjbhfsr14wgnbbslpxv34011qfb5j")))

(define-public crate-async-core-0.3.0 (c (n "async-core") (v "0.3.0") (h "1kkj8z9rky6dhhhqs9rcv246c28carv3ly02i7pc1mwa6x9xxfqf") (f (quote (("no_std"))))))

(define-public crate-async-core-0.3.1 (c (n "async-core") (v "0.3.1") (h "1hy9sg6zkv5qsyg5040kf91gs2lrd0p538qrjgxgk07ng2b9qkf4") (f (quote (("no_std"))))))

