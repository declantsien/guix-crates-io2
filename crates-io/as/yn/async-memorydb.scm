(define-module (crates-io as yn async-memorydb) #:use-module (crates-io))

(define-public crate-async-memorydb-0.1.0 (c (n "async-memorydb") (v "0.1.0") (d (list (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-lock") (r "^2.7.0") (d #t) (k 0)))) (h "0fdmbywg8rcgdb844vvbmgvngayd7l62khqdi680rhb1wqymk4iw")))

(define-public crate-async-memorydb-0.1.1 (c (n "async-memorydb") (v "0.1.1") (d (list (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-lock") (r "^2.7.0") (d #t) (k 0)))) (h "0paqil2jbyic09jp440idwkigci6p19yjq7v9jnvrmsk4mapaa9v")))

(define-public crate-async-memorydb-0.1.2 (c (n "async-memorydb") (v "0.1.2") (d (list (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-lock") (r "^2.7.0") (d #t) (k 0)))) (h "1qf8crkdplpiaf0g7mjv1rni3q91kjzj1c06dzsr7bfhrbrrirk2")))

(define-public crate-async-memorydb-0.1.3 (c (n "async-memorydb") (v "0.1.3") (d (list (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-lock") (r "^2.7.0") (d #t) (k 0)))) (h "1j8vsp785mzi3khql66acmxc3hwg7kw7h6myz848lc3y29xb2wp8")))

(define-public crate-async-memorydb-0.2.0 (c (n "async-memorydb") (v "0.2.0") (d (list (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-lock") (r "^2.7.0") (d #t) (k 0)))) (h "16lvm9n9xfdfy91m8drck4r61ccq85gqdpvnaiymscyyl7cqs782")))

(define-public crate-async-memorydb-0.3.0 (c (n "async-memorydb") (v "0.3.0") (d (list (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-lock") (r "^2.7.0") (d #t) (k 0)))) (h "1mlcwzxkk5xip3pbsxp5zc98yhdhc8sbjmkzfxhva4b0sl2x7r3g")))

(define-public crate-async-memorydb-0.3.2 (c (n "async-memorydb") (v "0.3.2") (d (list (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-lock") (r "^3") (d #t) (k 0)))) (h "0l00fdcxiivn0rdasxw54dhyq9q0qcjkhc14m168x7w3qj4hp6m3")))

