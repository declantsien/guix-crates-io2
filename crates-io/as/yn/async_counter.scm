(define-module (crates-io as yn async_counter) #:use-module (crates-io))

(define-public crate-async_counter-0.1.0 (c (n "async_counter") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)))) (h "1y2xyi41nzkp8jg858zgwpfik2bh008p9j850s9x1xlg5p4wq6zc")))

(define-public crate-async_counter-0.1.1 (c (n "async_counter") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)))) (h "1szbg6pr9zjqh3jqflzfg1lb2zzngwagix4bczf4gccwm662m5a7")))

(define-public crate-async_counter-0.1.2 (c (n "async_counter") (v "0.1.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)))) (h "14npbjacf5qb0w79hhi9bfhr1aw21fvc0aql9dizbz2lrl7m6nl7")))

(define-public crate-async_counter-0.1.3 (c (n "async_counter") (v "0.1.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)))) (h "1zvilxl12ddy3qyprnbrv513qfqdg4g8gia37llzhk4sz20qpvdb")))

