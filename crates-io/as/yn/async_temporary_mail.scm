(define-module (crates-io as yn async_temporary_mail) #:use-module (crates-io))

(define-public crate-async_temporary_mail-0.1.0 (c (n "async_temporary_mail") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1xz58jagfn8fdfd1nss99wy9n7932hv8c7k73zxfllyxyrhspyz4")))

(define-public crate-async_temporary_mail-0.1.1 (c (n "async_temporary_mail") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0k9kg830yfxljl6gclccybylxwlndnyfi6j1mf4j1v01rjzlw1mx")))

