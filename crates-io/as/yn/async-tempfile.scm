(define-module (crates-io as yn async-tempfile) #:use-module (crates-io))

(define-public crate-async-tempfile-0.1.0 (c (n "async-tempfile") (v "0.1.0") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0wjmhn3dr8n6vr2rmmhj4nldbyagxa2ifz6967cljfa6fb5xzr99")))

(define-public crate-async-tempfile-0.2.0 (c (n "async-tempfile") (v "0.2.0") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1w6z00b4483kxyvnch4brcky3ciq19q4z8gmgyybz9jm42yq04hj")))

(define-public crate-async-tempfile-0.3.0 (c (n "async-tempfile") (v "0.3.0") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "12p8bknsq2nyzjazn6lyw3l18q2f2mkb3xmv9f5hh3rhx2lvbpdg")))

(define-public crate-async-tempfile-0.4.0 (c (n "async-tempfile") (v "0.4.0") (d (list (d (n "tokio") (r "^1.28.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "16zx4qcwzq94n13pp6xwa4589apm5y8j20jb7lk4yzn42fqlnzdk") (f (quote (("default" "uuid")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

(define-public crate-async-tempfile-0.5.0 (c (n "async-tempfile") (v "0.5.0") (d (list (d (n "tokio") (r "^1.34.0") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (o #t) (d #t) (k 0)))) (h "025vvkzbrrdgljlg82rvfn1s2x5a064zp7irps28slg6hqr4lzf1") (f (quote (("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

