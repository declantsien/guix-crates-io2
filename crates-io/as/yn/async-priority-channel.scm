(define-module (crates-io as yn async-priority-channel) #:use-module (crates-io))

(define-public crate-async-priority-channel-0.1.0 (c (n "async-priority-channel") (v "0.1.0") (d (list (d (n "event-listener") (r "^2") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0h36m0avgs86pgh286xkvbnhdhb8bxgsnlxwwazvw88v5scph5n2")))

(define-public crate-async-priority-channel-0.2.0 (c (n "async-priority-channel") (v "0.2.0") (d (list (d (n "event-listener") (r "^4.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync" "rt-multi-thread" "macros" "time"))) (d #t) (k 2)))) (h "0n4pk6dgx0w3i43dsjgfn8f3wzdrhv3kvi65c3vk246k8ks9dpmc")))

