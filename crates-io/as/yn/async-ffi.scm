(define-module (crates-io as yn async-ffi) #:use-module (crates-io))

(define-public crate-async-ffi-0.1.0 (c (n "async-ffi") (v "0.1.0") (d (list (d (n "tokio") (r "^1.4.0") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 2)))) (h "0a22wb72argakjzb0ff4ss095vy7l4yhzf2kanqnfc9c2phc9lrz")))

(define-public crate-async-ffi-0.2.0 (c (n "async-ffi") (v "0.2.0") (d (list (d (n "tokio") (r "^1.4.0") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 2)))) (h "1xgiziadls7arlr66328qwpgqgf4cs3y8x2i6x8ykn4wi95jw09g")))

(define-public crate-async-ffi-0.2.1 (c (n "async-ffi") (v "0.2.1") (d (list (d (n "tokio") (r "^1.4.0") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 2)))) (h "013nd2lw4jj6l2kvcjx01f2x6nwhxndpi6ksh5426967mh67zj4c")))

(define-public crate-async-ffi-0.3.0 (c (n "async-ffi") (v "0.3.0") (d (list (d (n "tokio") (r "^1.4.0") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 2)))) (h "1wgxgzv52602az4zpfvnr1m9v8ibgvw6x30k83jf0nv14n35n25k")))

(define-public crate-async-ffi-0.3.1 (c (n "async-ffi") (v "0.3.1") (d (list (d (n "tokio") (r "^1.4.0") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 2)))) (h "11whzj4by6zd82a96z1vfbal0nqmgirynmlhm81bi0d1krjngm2p") (r "1.49")))

(define-public crate-async-ffi-0.4.0 (c (n "async-ffi") (v "0.4.0") (d (list (d (n "abi_stable") (r "^0.10") (o #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 2)))) (h "1c23b46nbq1y9vr84vrcf6c88h1d2knqi22y2labbaj4s1c3gry7") (r "1.49")))

(define-public crate-async-ffi-0.4.1 (c (n "async-ffi") (v "0.4.1") (d (list (d (n "abi_stable") (r "^0.11") (o #t) (k 0)) (d (n "macros") (r "=0.4.1") (o #t) (d #t) (k 0) (p "async-ffi-macros")) (d (n "tokio") (r "^1.4.0") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 2)))) (h "08ravjwdcl7jycm3lj3wdl71038f0rds3lvp3jy1wfc9lwvskm8y") (r "1.49")))

(define-public crate-async-ffi-0.5.0 (c (n "async-ffi") (v "0.5.0") (d (list (d (n "abi_stable") (r "^0.11") (o #t) (k 0)) (d (n "macros") (r "^0.5") (o #t) (d #t) (k 0) (p "async-ffi-macros")) (d (n "tokio") (r "^1, <1.30") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 2)))) (h "0l0s134bsiwwr5f7ifh0ygvh219zjmy7dbsidramlzpgzv023ppl") (r "1.56")))

