(define-module (crates-io as yn async-protocol) #:use-module (crates-io))

(define-public crate-async-protocol-0.1.0 (c (n "async-protocol") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "protocol") (r "^3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync" "io-util"))) (d #t) (k 0)))) (h "0j7pqhli2p0ws378qaizxpf60hp2xaiglrcn25m1308md53p05q1")))

(define-public crate-async-protocol-0.1.1 (c (n "async-protocol") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "protocol") (r "^3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync" "io-util"))) (d #t) (k 0)))) (h "0cjqdl59f9jywmcqcb7y8lnvmax77kpqgkaxgjnzigcxf6hmgk9i")))

