(define-module (crates-io as yn async-log) #:use-module (crates-io))

(define-public crate-async-log-0.0.0 (c (n "async-log") (v "0.0.0") (h "16axh3b7mdbx0xjqa53c4rywpcd4z0pq1sbmlsfh9l067rdzmv97")))

(define-public crate-async-log-1.0.0 (c (n "async-log") (v "1.0.0") (d (list (d (n "async-log-attributes") (r "^1.0.0") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.30") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "07yglhnlgdrlszjl14r9rpf232mqcdkck368cdm8fbdlkc67d7xc")))

(define-public crate-async-log-1.0.1 (c (n "async-log") (v "1.0.1") (d (list (d (n "async-log-attributes") (r "^1.0.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.30") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "03h98frbl92gham4mp9bdrxzf01w3a1hxh0axbrsyxgs58m1wyha")))

(define-public crate-async-log-1.0.2 (c (n "async-log") (v "1.0.2") (d (list (d (n "async-log-attributes") (r "^1.0.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1rs2mqj9lwphc84ix48qxn9z48ilxwff09fr3zlb5am17n15vgvx")))

(define-public crate-async-log-1.0.3 (c (n "async-log") (v "1.0.3") (d (list (d (n "async-log-attributes") (r "^1.0.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)))) (h "10dmda2gbgmfa03fr3qc6fp04nqmxjcawddnf7b0hz8qwp01xm5a")))

(define-public crate-async-log-1.0.4 (c (n "async-log") (v "1.0.4") (d (list (d (n "async-log-attributes") (r "^1.0.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)))) (h "0lbfbx327jx5avv848ib1n65lwdri2glhga1rdpd5212rpn6jri7")))

(define-public crate-async-log-1.1.0 (c (n "async-log") (v "1.1.0") (d (list (d (n "async-log-attributes") (r "^1.0.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (f (quote ("std"))) (d #t) (k 0)))) (h "16ymra7f8169br9ss9m9n4l6rjcav9ns6r9mv4nr4r9i9wq37fpm")))

(define-public crate-async-log-2.0.0 (c (n "async-log") (v "2.0.0") (d (list (d (n "async-log-attributes") (r "^1.0.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.34") (d #t) (k 0)) (d (n "femme") (r "^1.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (f (quote ("std" "kv_unstable"))) (d #t) (k 0)))) (h "0vdvkvjbsw9f6qka8sfdnxhglq6iwm42zhilvgxb3lis8syslfdc")))

