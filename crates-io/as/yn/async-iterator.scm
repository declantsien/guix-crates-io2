(define-module (crates-io as yn async-iterator) #:use-module (crates-io))

(define-public crate-async-iterator-1.0.0 (c (n "async-iterator") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)))) (h "1g5gy175m9555cwbrhfxiqbx6q9zwwzsanyzwcq0kfkm35q5h155")))

(define-public crate-async-iterator-1.0.1 (c (n "async-iterator") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)))) (h "0zda7rpsbs6wv4m9j4q6m6s81yzgl16amlgbp7avx8xny2nm9jvd")))

(define-public crate-async-iterator-2.0.0 (c (n "async-iterator") (v "2.0.0") (h "0jjrb061yb6f2nbb01953v1a3hkykr9lx0p0pq8zs6wjwms3yx6l")))

(define-public crate-async-iterator-2.1.0 (c (n "async-iterator") (v "2.1.0") (h "00icv1z5ygj53dx8d0hj0d2rhlif1dr4416s1qh5s6f52im6bgkv") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-async-iterator-2.2.0 (c (n "async-iterator") (v "2.2.0") (h "1r3sfzqspf64avqivm7l7qnx71cf5ib7n3sw8ld92z5qi0myfmzl") (f (quote (("std") ("default" "std") ("alloc"))))))

