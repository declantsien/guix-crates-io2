(define-module (crates-io as yn async-io-bridge) #:use-module (crates-io))

(define-public crate-async-io-bridge-0.1.0-alpha.1 (c (n "async-io-bridge") (v "0.1.0-alpha.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "16369cc4wb8lqkz9zaazzf4rczvpq80b2xjy1jdph0h0gqj98r39") (y #t)))

(define-public crate-async-io-bridge-0.1.0 (c (n "async-io-bridge") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1b44ciwa9anprcsc8wwcxz6rzc9anmcj28fm17wq1qnsnk4njyb6") (y #t)))

