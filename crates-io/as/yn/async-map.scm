(define-module (crates-io as yn async-map) #:use-module (crates-io))

(define-public crate-async-map-0.1.1 (c (n "async-map") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0xm1vdzhzrbnvh27s74q34r6llazr0xcwjj6jpgrjqm5h1wvr8bd")))

(define-public crate-async-map-0.2.0 (c (n "async-map") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0yj8mp5w9whk7q8vh5xdx7ha5vyn55wp84s45rsyv1ni4ykywjrg")))

(define-public crate-async-map-0.2.1 (c (n "async-map") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)))) (h "14a964bazwbjp7gdc6g1wds0fqkmkdplx9khp9ww9lr5a3nwjvcr")))

(define-public crate-async-map-0.2.2 (c (n "async-map") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "im") (r "^15.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1bdbbr8qfmyd5974g191ap0qs59h2d8gs649g9n6rbs5csick4nw")))

(define-public crate-async-map-0.2.3 (c (n "async-map") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "im") (r "^15") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pgk2lsq39ahr8ln7v19gg29xcg859vwh0my1a3s683qwr19dq02")))

