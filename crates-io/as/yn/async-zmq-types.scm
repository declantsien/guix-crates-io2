(define-module (crates-io as yn async-zmq-types) #:use-module (crates-io))

(define-public crate-async-zmq-types-0.1.0 (c (n "async-zmq-types") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "zmq") (r "^0.8") (d #t) (k 0)))) (h "1gpc27hwnx31c9agg188vrk4n7zmm4m0d42g4pdn5rf8vzfjsqb9")))

(define-public crate-async-zmq-types-0.2.0 (c (n "async-zmq-types") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "zmq") (r "^0.8") (d #t) (k 0)))) (h "02vmf8ywys0dgkzng8w4irgb5s0gk865df8q2bx32zg588byz3gl")))

(define-public crate-async-zmq-types-0.3.0 (c (n "async-zmq-types") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1d21vlx40giw6l79mcxv3j0g6r6bna65053v35d44z1nprs97wjd")))

(define-public crate-async-zmq-types-0.3.1 (c (n "async-zmq-types") (v "0.3.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "05kicr37l2jab4llyyfafj81qlbys4x712k6xnw55s0j06d8mq2l")))

(define-public crate-async-zmq-types-0.3.2 (c (n "async-zmq-types") (v "0.3.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1f50j3z99zczdvn3py7jakpk9i8k7nbqy9l2169lmc28pmb9g0ln")))

