(define-module (crates-io as yn async-debug-derive) #:use-module (crates-io))

(define-public crate-async-debug-derive-0.1.0 (c (n "async-debug-derive") (v "0.1.0") (d (list (d (n "bae") (r "~0.1.7") (d #t) (k 0)) (d (n "indexmap") (r "~1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.36") (d #t) (k 0)) (d (n "quote") (r "~1.0.15") (d #t) (k 0)) (d (n "syn") (r "~1.0.86") (d #t) (k 0)))) (h "1hs2r7zj5s4z1fxf0gibih5p8106wnray7dzjacldm87n6hxzrq0")))

(define-public crate-async-debug-derive-0.1.1 (c (n "async-debug-derive") (v "0.1.1") (d (list (d (n "bae") (r "~0.1.7") (d #t) (k 0)) (d (n "indexmap") (r "~1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.36") (d #t) (k 0)) (d (n "quote") (r "~1.0.15") (d #t) (k 0)) (d (n "syn") (r "~1.0.86") (d #t) (k 0)))) (h "1747vc2vbsc2isqmdlpyif6kws9wk6qdiqh0xiq9y96289ylhwb8")))

(define-public crate-async-debug-derive-0.1.2 (c (n "async-debug-derive") (v "0.1.2") (d (list (d (n "bae") (r "~0.1.7") (d #t) (k 0)) (d (n "indexmap") (r "~1.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.36") (d #t) (k 0)) (d (n "quote") (r "~1.0.15") (d #t) (k 0)) (d (n "syn") (r "~1.0.86") (d #t) (k 0)))) (h "001q1w7diygjg6cl3q8rs855mcm41jy0nls451ivqnm9ki099m8q")))

(define-public crate-async-debug-derive-0.1.3 (c (n "async-debug-derive") (v "0.1.3") (d (list (d (n "bae") (r "~0.1.7") (d #t) (k 0)) (d (n "convert_case") (r "~0.5.0") (d #t) (k 0)) (d (n "goldenfile") (r "^1.1.0") (d #t) (k 2)) (d (n "indexmap") (r "~1.8.0") (d #t) (k 0)) (d (n "prettyplease") (r "~0.1.7") (d #t) (k 2)) (d (n "proc-macro2") (r "~1.0.36") (d #t) (k 0)) (d (n "quote") (r "~1.0.15") (d #t) (k 0)) (d (n "syn") (r "~1.0.86") (d #t) (k 0)))) (h "1fl1lzycwvmidzr4910kl2z88mfxj6vnky4rvaf9164a476wyp7r")))

