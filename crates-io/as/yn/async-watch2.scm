(define-module (crates-io as yn async-watch2) #:use-module (crates-io))

(define-public crate-async-watch2-0.1.0 (c (n "async-watch2") (v "0.1.0") (d (list (d (n "async-executor") (r "^0.1.2") (d #t) (k 2)) (d (n "atomic-waker") (r "^1.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "1cw4ky9m5wx20y1jjbaccrnzqs9w6r5g75db404bb2qdrh2b802z")))

