(define-module (crates-io as yn async_ach-mpmc) #:use-module (crates-io))

(define-public crate-async_ach-mpmc-0.1.0 (c (n "async_ach-mpmc") (v "0.1.0") (d (list (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "0ir6gxmg48kwfnwlm4gc4nnr0f7xnss4923gwlxhpm8a0m0h00fg")))

(define-public crate-async_ach-mpmc-0.1.1 (c (n "async_ach-mpmc") (v "0.1.1") (d (list (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "0lfpj5gcbssfbkc8f3jg8sqcwk53dcy1rwpfdjbmfh5sxzb4k9f7") (f (quote (("default") ("alloc"))))))

(define-public crate-async_ach-mpmc-0.1.2 (c (n "async_ach-mpmc") (v "0.1.2") (d (list (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "1x7g14z4hd5jslh9yydyqkfbgwn19v9hcckj4ii7zhcpff0d162s") (f (quote (("default") ("alloc"))))))

(define-public crate-async_ach-mpmc-0.1.3 (c (n "async_ach-mpmc") (v "0.1.3") (d (list (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "09z9cxcm1gbkrv03pcfx2fby7gmnz6n7h9p2ya63cwmzqihw6nnh") (f (quote (("default") ("alloc"))))))

