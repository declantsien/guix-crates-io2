(define-module (crates-io as yn async-signals) #:use-module (crates-io))

(define-public crate-async-signals-0.1.0 (c (n "async-signals") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 2)) (d (n "signal-hook") (r "^0.1") (f (quote ("mio-support"))) (d #t) (k 0)))) (h "1ymhp0k1vh35162s6hqns9axzhb58b3hih17c9v94wvhi761y6yg")))

(define-public crate-async-signals-0.2.0 (c (n "async-signals") (v "0.2.0") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 2)) (d (n "signal-hook") (r "^0.1") (f (quote ("mio-support"))) (d #t) (k 0)))) (h "0zf5nrwc7kv146k1kgih1hq68gda4fisdihx1vgnc2kiq5rqxk4y")))

(define-public crate-async-signals-0.3.0-alpha.0 (c (n "async-signals") (v "0.3.0-alpha.0") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "1q78h6yvm4njs2v1x0q37wckpnpgdlfhr6mxh363axpsbb9p9syw")))

(define-public crate-async-signals-0.3.0 (c (n "async-signals") (v "0.3.0") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "1wlv2xqjh797xs20njcz03v8dx117b6dh771gk6x9i42ibqvkrhf")))

(define-public crate-async-signals-0.3.1 (c (n "async-signals") (v "0.3.1") (d (list (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "14b5mgm8lsqvwvv8y7qryvix5lw3qgvdw7wpq4igwh5pslkx7gzp")))

(define-public crate-async-signals-0.4.0 (c (n "async-signals") (v "0.4.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0c6cd4nqmzc900gvdms63f3w0wgwkhy8qcl7nh7s1z3ii170cr3p")))

