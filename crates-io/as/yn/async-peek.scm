(define-module (crates-io as yn async-peek) #:use-module (crates-io))

(define-public crate-async-peek-0.0.0 (c (n "async-peek") (v "0.0.0") (h "0ib4ibsvw2168axhhy7jyp6wg35a4w5dfxzv95zap3zwqibynlvy")))

(define-public crate-async-peek-0.1.0 (c (n "async-peek") (v "0.1.0") (d (list (d (n "async-io") (r "^0.1") (o #t) (k 0)) (d (n "async-net") (r "^0.1") (o #t) (k 0)) (d (n "async-std") (r "^1.6") (o #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "udp"))) (o #t) (k 0)) (d (n "ufut") (r "^0.2") (d #t) (k 0)))) (h "1nfw88a8vyw13lppsw35mc6i91qwn3axfr3aisg2q0c1z0mw5s78") (y #t)))

(define-public crate-async-peek-0.1.1 (c (n "async-peek") (v "0.1.1") (d (list (d (n "async-io") (r "^0.1") (o #t) (k 0)) (d (n "async-net") (r "^0.1") (o #t) (k 0)) (d (n "async-std") (r "^1.6") (o #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "udp"))) (o #t) (k 0)) (d (n "ufut") (r "^0.2") (d #t) (k 0)))) (h "06ggpyp84i7ll4ck4yld5f2za8k5n5l60x3ynfqp1sri03hz5c5a") (y #t)))

(define-public crate-async-peek-0.1.2 (c (n "async-peek") (v "0.1.2") (d (list (d (n "async-io") (r "^0.1") (o #t) (k 0)) (d (n "async-net") (r "^0.1") (o #t) (k 0)) (d (n "async-std") (r "^1.6") (o #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "udp"))) (o #t) (k 0)) (d (n "ufut") (r "^0.2") (d #t) (k 0)))) (h "05hicyj0dvli56lsahwiz4pl8siqqhvmy8apqn3jbjdmryr5f2yi") (y #t)))

(define-public crate-async-peek-0.1.3 (c (n "async-peek") (v "0.1.3") (d (list (d (n "async-io") (r "^0.1") (o #t) (k 0)) (d (n "async-net") (r "^0.1") (o #t) (k 0)) (d (n "async-std") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "udp"))) (o #t) (k 0)) (d (n "ufut") (r "^0.2") (d #t) (k 0)))) (h "0rzjij5my2bvxnviz9m6bbr2xi953fx18i6mnjb0ak11vaqq3z8v") (y #t)))

(define-public crate-async-peek-0.1.4 (c (n "async-peek") (v "0.1.4") (d (list (d (n "async-io") (r "^0.1") (o #t) (k 0)) (d (n "async-net") (r "^0.1") (o #t) (k 0)) (d (n "async-std") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "udp"))) (o #t) (k 0)) (d (n "ufut") (r "^0.2") (d #t) (k 0)))) (h "0y1fvj1g18wqvcidv9fpa2avw5nb1mzli6z740lz5pflw4pvl3di") (y #t)))

(define-public crate-async-peek-0.2.0 (c (n "async-peek") (v "0.2.0") (d (list (d (n "async-io") (r "^0.1") (o #t) (k 0)) (d (n "async-net") (r "^0.1") (o #t) (k 0)) (d (n "async-std") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 2)) (d (n "ufut") (r "^0.2") (d #t) (k 0)))) (h "0rx81d1xm6gvm3h6pz886pqzxr2qyibhcvva1dc4q5bsjz25qqxd") (y #t)))

(define-public crate-async-peek-0.2.1 (c (n "async-peek") (v "0.2.1") (d (list (d (n "async-io") (r "^0.1") (o #t) (k 0)) (d (n "async-net") (r "^0.1") (o #t) (k 0)) (d (n "async-std") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.3") (d #t) (k 2)) (d (n "ufut") (r "^0.2") (d #t) (k 0)))) (h "1fdz2ks3a7si3g3ndnpgcbha6blalil3k6m8a6yy7ssmcsxhw2gx")))

(define-public crate-async-peek-0.3.0 (c (n "async-peek") (v "0.3.0") (d (list (d (n "async-io") (r "^0.2") (o #t) (k 0)) (d (n "async-net") (r "^1.0") (o #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.4") (d #t) (k 2)) (d (n "ufut") (r "^0.2") (d #t) (k 0)))) (h "1js8485lw351db4rcdjdbws7clar65i5s5n2v359mrgxm255gaf6")))

(define-public crate-async-peek-0.3.1 (c (n "async-peek") (v "0.3.1") (d (list (d (n "async-io") (r "^0.2") (o #t) (k 0)) (d (n "async-net") (r "^1.0") (o #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.4") (d #t) (k 2)) (d (n "ufut") (r "^0.2") (d #t) (k 0)))) (h "13zzh0l715iq4f3xd67wbipbz2q4a8q9b2kwhrdyn8gyyryixzj8")))

(define-public crate-async-peek-0.3.2 (c (n "async-peek") (v "0.3.2") (d (list (d (n "async-io") (r "^0.2") (o #t) (k 0)) (d (n "async-net") (r "^1.0") (o #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.4") (d #t) (k 2)) (d (n "ufut") (r "^0.3") (d #t) (k 0)))) (h "1a8z1sb0xqn9213cvmxsk5xwvvy4x6jhn1g7irp6hpsqqfph9qzq")))

