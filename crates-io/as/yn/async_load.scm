(define-module (crates-io as yn async_load) #:use-module (crates-io))

(define-public crate-async_load-1.0.0 (c (n "async_load") (v "1.0.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0blpilgiqsyi653wirr6wwmf0di88fyw43p2gfqzqws2zghgx8k0") (f (quote (("extension"))))))

(define-public crate-async_load-1.0.1 (c (n "async_load") (v "1.0.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1vxqldl55168bh9widrk9wxqdi1l7b1i2hy60m4hzwad90ijwvsd") (f (quote (("extension"))))))

(define-public crate-async_load-1.0.2 (c (n "async_load") (v "1.0.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1cjzgq4nwam1lyjmma5cnjsq9jf5522g2ayyxrjl7czvxnlvznac") (f (quote (("extension"))))))

(define-public crate-async_load-1.0.3 (c (n "async_load") (v "1.0.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1pnix7c2drzaa8h7hwpybkwd97q6sbykdz3c3b7mgj28w6drhrxi") (f (quote (("extension"))))))

