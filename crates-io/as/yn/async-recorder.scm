(define-module (crates-io as yn async-recorder) #:use-module (crates-io))

(define-public crate-async-recorder-0.1.1 (c (n "async-recorder") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0fhwy91ph6ja8wbpqwckwypwph4qmgpx482vmzlkhrh5hcgkzcs3")))

(define-public crate-async-recorder-0.1.2 (c (n "async-recorder") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1l42d1kfi8w9x8dd635x2qfhdlfcgd6ji3xjkwk7rkgj7qcyy5q0")))

(define-public crate-async-recorder-0.2.0 (c (n "async-recorder") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0lz1lf7zic52qkyhw22x17prxb06zy4556ja8gylykg50gr0bcq5")))

(define-public crate-async-recorder-0.3.0 (c (n "async-recorder") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "03dmjcwg8xrhjl4wkzvw4cbra2ipwcbhqkx8d3x3cllbss26kzrp")))

(define-public crate-async-recorder-0.3.1 (c (n "async-recorder") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "17h98qfl3mczyriljwjrpss98d3i4ccpinra00gbwmvdqy6dqlcf")))

