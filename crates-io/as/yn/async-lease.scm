(define-module (crates-io as yn async-lease) #:use-module (crates-io))

(define-public crate-async-lease-0.1.0 (c (n "async-lease") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.1") (d #t) (k 0)))) (h "1f0nslqsiy7y41r4gjck0bmkya9yswx0dn6wcf3h5ws32fbpbbrg")))

(define-public crate-async-lease-0.1.1 (c (n "async-lease") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.1") (d #t) (k 0)))) (h "1bxqwq0vnni0phajzcxzyvxdbnrlmfj94g3wnp4d01922xvia33c")))

(define-public crate-async-lease-0.1.2 (c (n "async-lease") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-mock-task") (r "^0.1") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.1") (d #t) (k 0)))) (h "0p2fahfsp3y2zlbc9c7j15hadr5vckhhcgchh8i8kx7r3bayir3f")))

(define-public crate-async-lease-0.2.0-alpha.1 (c (n "async-lease") (v "0.2.0-alpha.1") (d (list (d (n "tokio-sync") (r "^0.2.0-alpha.6") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "0ipjnmbxrl0jzbql54h3xb5lwi9lh7dk0jwa2fxxz5qi7szs75a2")))

