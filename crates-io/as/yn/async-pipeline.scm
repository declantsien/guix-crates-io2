(define-module (crates-io as yn async-pipeline) #:use-module (crates-io))

(define-public crate-async-pipeline-0.0.1 (c (n "async-pipeline") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0smxrh3pwqdzp5lw7mgrbwyjic51h40mbabrjc49mx0a99kv1kva")))

(define-public crate-async-pipeline-0.0.2 (c (n "async-pipeline") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0fx0byxhq4nbmjc27bp6dq0j7q1hv6c60cgnd84hrads5k8v9418")))

