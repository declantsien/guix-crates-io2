(define-module (crates-io as yn async_cell) #:use-module (crates-io))

(define-public crate-async_cell-0.1.0 (c (n "async_cell") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1kdjjqg2yr7iammaajfbmmqbv2hvii081dy5l57yifiz7j0yjkwq") (f (quote (("no_std") ("default" "parking_lot"))))))

(define-public crate-async_cell-0.2.0 (c (n "async_cell") (v "0.2.0") (d (list (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1c0kp9bh868xpz87p1jp6ngwgfyd4hk6cs5gh98ikp5x325jw348") (f (quote (("no_std") ("default" "parking_lot") ("deadlock_detection" "parking_lot/deadlock_detection"))))))

(define-public crate-async_cell-0.2.1 (c (n "async_cell") (v "0.2.1") (d (list (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1lykxvavk33dfmylwvy7c9wf7nkpybxh2riyzm42wfyzp8g391hj") (f (quote (("no_std") ("default" "parking_lot") ("deadlock_detection" "parking_lot/deadlock_detection"))))))

(define-public crate-async_cell-0.2.2 (c (n "async_cell") (v "0.2.2") (d (list (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1g32g9ybgmnxl4kz44v6wxzbbmp98g69xw2s9lxhl4qqwnffwkl3") (f (quote (("no_std") ("deadlock_detection" "parking_lot" "parking_lot/deadlock_detection")))) (r "1.63")))

