(define-module (crates-io as yn async_progress) #:use-module (crates-io))

(define-public crate-async_progress-0.1.0 (c (n "async_progress") (v "0.1.0") (d (list (d (n "future-parking_lot") (r "^0.3.2-alpha") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pharos") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "0n931klff91smqyihcc7d63xdpx2mqhx334m2lhi6xxn95rr984n") (f (quote (("external_doc"))))))

(define-public crate-async_progress-0.1.1 (c (n "async_progress") (v "0.1.1") (d (list (d (n "future-parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pharos") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1zf04128c6s24p58ifq84qj52zk7741hsi8xywh25k2irldl9lz7") (f (quote (("external_doc"))))))

(define-public crate-async_progress-0.2.0 (c (n "async_progress") (v "0.2.0") (d (list (d (n "future-parking_lot") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pharos") (r "^0.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "03aqfl09m9kpca5a93qqpdzabqc8j4lbxzncfwfr0w3k78lgalh0") (f (quote (("external_doc"))))))

(define-public crate-async_progress-0.2.1 (c (n "async_progress") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pharos") (r "^0.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "1xn677qbhjwdr9h0chgyz488j572vq4v2a8iml9k776cx16h7m82") (f (quote (("external_doc"))))))

