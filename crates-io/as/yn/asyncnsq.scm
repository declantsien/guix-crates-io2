(define-module (crates-io as yn asyncnsq) #:use-module (crates-io))

(define-public crate-asyncnsq-0.1.0 (c (n "asyncnsq") (v "0.1.0") (d (list (d (n "async-std") (r "^1.4.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "08jj20c6hy21icwm3anzfy94kv7ckyi4lazcji97hvni2f8ipfcx")))

