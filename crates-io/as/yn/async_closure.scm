(define-module (crates-io as yn async_closure) #:use-module (crates-io))

(define-public crate-async_closure-0.1.0 (c (n "async_closure") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0pq6jdysf3bzgfrj83d9kqak5h3c9qsp5kmy1a2kslwy714dwyk6")))

(define-public crate-async_closure-0.1.1 (c (n "async_closure") (v "0.1.1") (d (list (d (n "async-lock") (r "^2") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "pollster") (r "^0.3") (f (quote ("macro"))) (d #t) (k 2)))) (h "0nl2fa4za6nb12rxs852253c06rcv35frk53jvhfji1b8vrbjap4")))

(define-public crate-async_closure-0.1.2 (c (n "async_closure") (v "0.1.2") (d (list (d (n "pollster") (r "^0.3") (f (quote ("macro"))) (d #t) (k 2)))) (h "0f3q209jc8i56yj7ib0fi64q2bbk7ak8syylvxnky3h3vhm33ybv")))

