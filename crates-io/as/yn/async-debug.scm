(define-module (crates-io as yn async-debug) #:use-module (crates-io))

(define-public crate-async-debug-0.1.0 (c (n "async-debug") (v "0.1.0") (d (list (d (n "async-debug-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "~1.17.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "~1.0.56") (d #t) (k 2)) (d (n "version-sync") (r "~0.9.4") (d #t) (k 2)))) (h "1nhyvl1l3m70icfyxrz8ga8c134jb728kd73k96hb6jbssqvpq9b")))

(define-public crate-async-debug-0.1.1 (c (n "async-debug") (v "0.1.1") (d (list (d (n "async-debug-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "~1.17.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "~1.0.56") (d #t) (k 2)) (d (n "version-sync") (r "~0.9.4") (d #t) (k 2)))) (h "0vj1fysphw1xwr14y9668b97j9fr0nc5n05iwh2g6945mcjmif3g")))

(define-public crate-async-debug-0.1.2 (c (n "async-debug") (v "0.1.2") (d (list (d (n "async-debug-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "~1.17.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "~1.0.56") (d #t) (k 2)) (d (n "version-sync") (r "~0.9.4") (d #t) (k 2)))) (h "1w83va72rd552vikj5sgfxwsxp5isv3mria8q421x1vllwsy89rl")))

(define-public crate-async-debug-0.1.3 (c (n "async-debug") (v "0.1.3") (d (list (d (n "async-debug-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "~1.17.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "trybuild") (r "~1.0.56") (d #t) (k 2)) (d (n "version-sync") (r "~0.9.4") (d #t) (k 2)))) (h "1afpbdl2lnf7lyqwnl9bw7b16ccc3rmaq1w51jx4qkn1jy988d1c")))

