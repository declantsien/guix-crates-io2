(define-module (crates-io as yn async-diesel) #:use-module (crates-io))

(define-public crate-async-diesel-0.1.0 (c (n "async-diesel") (v "0.1.0") (d (list (d (n "async-std") (r "^1.4.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.4.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("postgres" "uuidv07"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "1zhsdfwhz88jl2kaldd75v9hnxcbdavz46dyg8s0whlhrg6jxxg7")))

