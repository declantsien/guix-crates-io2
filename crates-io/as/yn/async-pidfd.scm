(define-module (crates-io as yn async-pidfd) #:use-module (crates-io))

(define-public crate-async-pidfd-0.1.0 (c (n "async-pidfd") (v "0.1.0") (d (list (d (n "async-io") (r "^0.1.11") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "04lvjm2rq5paikn1w8nwg08cn0n6gs3i0fcwl8dx6lgj8hnpb12b") (f (quote (("default" "async") ("async" "async-io"))))))

(define-public crate-async-pidfd-0.1.1 (c (n "async-pidfd") (v "0.1.1") (d (list (d (n "async-io") (r "^0.1.11") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "0jmhjx7amjm50rq3ngc9mh676hqj51n2nycnvimnypkvdki6hsan") (f (quote (("default" "async") ("async" "async-io"))))))

(define-public crate-async-pidfd-0.1.2 (c (n "async-pidfd") (v "0.1.2") (d (list (d (n "async-io") (r "^0.1.11") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.75") (d #t) (k 0)))) (h "1p424xpz32gjhgqr3bzn4jaj3pwylrnim49mkk3immxwz8yyqb9x") (f (quote (("default" "async") ("async" "async-io"))))))

(define-public crate-async-pidfd-0.1.3 (c (n "async-pidfd") (v "0.1.3") (d (list (d (n "async-io") (r "^0.1.11") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.78") (d #t) (k 0)))) (h "0ndlfv34az6qgfx19vvzigda9w4d7w6j2y2mnl7hjsb9d1mpjl97") (f (quote (("default" "async") ("async" "async-io"))))))

(define-public crate-async-pidfd-0.1.4 (c (n "async-pidfd") (v "0.1.4") (d (list (d (n "async-io") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.78") (d #t) (k 0)))) (h "168pylpf7n898szw32sva7kf9h3x1mnip54mfr8f7f4v55c705qj") (f (quote (("default" "async") ("async" "async-io"))))))

