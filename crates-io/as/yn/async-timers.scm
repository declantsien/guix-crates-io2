(define-module (crates-io as yn async-timers) #:use-module (crates-io))

(define-public crate-async-timers-0.1.0 (c (n "async-timers") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kiah0ifv2lgndr0ri39salswlpiww9f49g3zw3lzvw39ys3dgy3")))

(define-public crate-async-timers-0.1.1 (c (n "async-timers") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zj2jrdx7yllsm2qzzivhh137c9ykxymwfavb1x4nsrdm6sxz8q2")))

(define-public crate-async-timers-0.1.2 (c (n "async-timers") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("time"))) (d #t) (k 0)))) (h "1qd3b34k0382ffvlfibbymrshlsj7xiy0zkf87nxaixl9rs039qb") (y #t)))

(define-public crate-async-timers-0.1.3 (c (n "async-timers") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("time"))) (d #t) (k 0)))) (h "15wcpqhss2hf7qncgfh3yccpan9hygka1y85ns9aa5znj2r2qqh6")))

(define-public crate-async-timers-0.1.4 (c (n "async-timers") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("time"))) (d #t) (k 0)))) (h "10k7b419i7a1giicc286lvwlm0l20v14h99ifgwgzr5cibkpabl7")))

