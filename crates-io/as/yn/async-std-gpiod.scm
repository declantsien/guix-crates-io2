(define-module (crates-io as yn async-std-gpiod) #:use-module (crates-io))

(define-public crate-async-std-gpiod-0.2.0 (c (n "async-std-gpiod") (v "0.2.0") (d (list (d (n "async-io") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "gpiod-core") (r "^0.2.0") (k 0)))) (h "1i4vg2ivbbyf7y6xsjnf70cgid2wzb62vbizfshvdx21rpcndcrs") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap")))) (y #t)))

(define-public crate-async-std-gpiod-0.2.1 (c (n "async-std-gpiod") (v "0.2.1") (d (list (d (n "async-io") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "gpiod-core") (r "^0.2.1") (k 0)))) (h "0bbbbils5if8sdz8fcxpjlpj9fnzd2gq185xvq9sfmzygqkr77ia") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

(define-public crate-async-std-gpiod-0.2.2 (c (n "async-std-gpiod") (v "0.2.2") (d (list (d (n "async-io") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "gpiod-core") (r "^0.2.2") (k 0)))) (h "0a303nmmznljb5cpnha6c6vrprpyp93bpxix4s9h4zlxx1adbi8r") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

(define-public crate-async-std-gpiod-0.2.3 (c (n "async-std-gpiod") (v "0.2.3") (d (list (d (n "async-io") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "gpiod-core") (r "^0.2.3") (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 2)))) (h "1z5lsjy9jx79fsjq4gmdx2zqw5s20dyplq4rcvgm9z3wd9gijfdz") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

