(define-module (crates-io as yn async-events) #:use-module (crates-io))

(define-public crate-async-events-0.1.0 (c (n "async-events") (v "0.1.0") (d (list (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0kkizsjvyllkna3h63c1kx1b7c157psl364il0mxkjw5js7gaqjv")))

(define-public crate-async-events-0.1.1 (c (n "async-events") (v "0.1.1") (d (list (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 2)))) (h "16w5xs4qvyv31vgnplzgdm913ajvlp5d4mb2k1ivz458fx649f0y")))

(define-public crate-async-events-0.1.2 (c (n "async-events") (v "0.1.2") (d (list (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1gma4hnrb0d1lizg8i87pagd68ckc521sf2m0f5k7r17vjh2vzq7")))

(define-public crate-async-events-0.2.0 (c (n "async-events") (v "0.2.0") (d (list (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1syg55sipyanxbvqs4x9m3nnznidh0pybqvdffwgdq2jqbkb5r5b")))

