(define-module (crates-io as yn async-graphql-relay) #:use-module (crates-io))

(define-public crate-async-graphql-relay-0.1.0 (c (n "async-graphql-relay") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)))) (h "1vf9p8df384fb4rwn5k3ar5zmnmzb5k78ki2lagjg4lf5lyvhbiv")))

(define-public crate-async-graphql-relay-0.2.0 (c (n "async-graphql-relay") (v "0.2.0") (d (list (d (n "async-graphql") (r "^2.8.5") (d #t) (k 2)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "0ypjgv5j22ijjfhkbdkckk593vq6xkxxvqgn5mwf4cq2yhzm8svk")))

(define-public crate-async-graphql-relay-0.3.0 (c (n "async-graphql-relay") (v "0.3.0") (d (list (d (n "async-graphql") (r "^2.8.5") (d #t) (k 2)) (d (n "async-graphql-relay-derive") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "1lpw8bnzcycydc3qjhhwj67miyfalj2bajwj4q1i3hb7r9jfi3fi")))

(define-public crate-async-graphql-relay-0.4.0 (c (n "async-graphql-relay") (v "0.4.0") (d (list (d (n "async-graphql") (r "^3.0.38") (d #t) (k 0)) (d (n "async-graphql-relay-derive") (r "^0.4") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "sea-orm") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "070mghjfahz4g6y4niyzgqllb59ldabyfs2w0jbv2zpvf7ql4mq1")))

