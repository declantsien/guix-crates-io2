(define-module (crates-io as yn async_zip_futures) #:use-module (crates-io))

(define-public crate-async_zip_futures-0.0.12 (c (n "async_zip_futures") (v "0.0.12") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("clock"))) (o #t) (k 0)) (d (n "crc32fast") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("full"))) (d #t) (k 2)) (d (n "zip") (r "^0.6.3") (d #t) (k 2)))) (h "16dr8gi6f0vz84xq0ijyyv4ldwpn444pxljlk1isn7l77q06ajrh") (f (quote (("full" "chrono"))))))

