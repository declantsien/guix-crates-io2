(define-module (crates-io as yn async-ioutil) #:use-module (crates-io))

(define-public crate-async-ioutil-0.1.0 (c (n "async-ioutil") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)))) (h "1jcf7dlm8iymnksz2qv2bn810xarl45dvva1b766nfwiksi1a8ss")))

(define-public crate-async-ioutil-0.1.1 (c (n "async-ioutil") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)))) (h "0gmi982jaz4k1yglig00ndl8vpsr4878l38x2bc38b9a51i1qwgm")))

(define-public crate-async-ioutil-0.2.0 (c (n "async-ioutil") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)))) (h "1c28i3yvm0alwvyvyjz6v7fd5isabgfh4r1mz63i7fdbwpmawqc4")))

