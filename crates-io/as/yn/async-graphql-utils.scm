(define-module (crates-io as yn async-graphql-utils) #:use-module (crates-io))

(define-public crate-async-graphql-utils-0.1.0 (c (n "async-graphql-utils") (v "0.1.0") (d (list (d (n "async-graphql") (r "^5.0.9") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)))) (h "0x978nnfmmz2kpsglhnn9saz93vfgyakq62gm6rlkn57cpaih34m")))

(define-public crate-async-graphql-utils-0.1.1 (c (n "async-graphql-utils") (v "0.1.1") (d (list (d (n "async-graphql") (r "^5.0.9") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)))) (h "0v648s4502j0dl2zpgc52hhnpic4sdafswi0njd8g7n8rzwwkhql")))

(define-public crate-async-graphql-utils-0.1.2 (c (n "async-graphql-utils") (v "0.1.2") (d (list (d (n "async-graphql") (r "^5.0.9") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)))) (h "00w3q1piivpq9hi5jb0h9xj7ia7wcwk24wlg26y1146j0misgynj")))

(define-public crate-async-graphql-utils-0.1.3 (c (n "async-graphql-utils") (v "0.1.3") (d (list (d (n "async-graphql") (r "^5.0.9") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)))) (h "1xxi8v0pglcn969knfc44dn3zrzir6gy0pwhhcipk37r4j9y530v")))

(define-public crate-async-graphql-utils-0.1.4 (c (n "async-graphql-utils") (v "0.1.4") (d (list (d (n "async-graphql") (r "^5.0.9") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)))) (h "0yav5d123k8kw83s80v7alhidmq0m9h4x5jq7y0yb49d54cvjyf4")))

(define-public crate-async-graphql-utils-0.2.0 (c (n "async-graphql-utils") (v "0.2.0") (d (list (d (n "async-graphql") (r "^5.0.9") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "0ry8nm105vplycwwbq2yh4dmnmpgc70g4cyp2b8xqvhdz2dszhys")))

(define-public crate-async-graphql-utils-0.2.1 (c (n "async-graphql-utils") (v "0.2.1") (d (list (d (n "async-graphql") (r "^5.0.10") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "1kgx409rbg6ppg5jp66gvrs3qzccdli87gi2amf2np8bs4j6mxlf")))

(define-public crate-async-graphql-utils-0.2.2 (c (n "async-graphql-utils") (v "0.2.2") (d (list (d (n "async-graphql") (r "^5.0.10") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "1zn9km1k0ckph9f1hhvmk9zb70fpwyviq118q4sw2xswnsx3j81w")))

(define-public crate-async-graphql-utils-0.3.0 (c (n "async-graphql-utils") (v "0.3.0") (d (list (d (n "async-graphql") (r "^5.0.10") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "0xl19hdmlfsc5zg9n9kcg08ac8hsvnj9sa04ygw663vicwjik4b1")))

(define-public crate-async-graphql-utils-0.3.1 (c (n "async-graphql-utils") (v "0.3.1") (d (list (d (n "async-graphql") (r "^5.0.10") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "07dwalbhp1q7c85xgav1zxscd63grp01gn0i2h2d23s5aj1qn0dr")))

(define-public crate-async-graphql-utils-0.4.0 (c (n "async-graphql-utils") (v "0.4.0") (d (list (d (n "async-graphql") (r "^5.0.10") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "rs-keycloak") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "00cqpyyvf627j6d73w1m2qg3pwmcprf93ddmql08gma6iz6g6hyq")))

(define-public crate-async-graphql-utils-0.4.1 (c (n "async-graphql-utils") (v "0.4.1") (d (list (d (n "async-graphql") (r "^5.0.10") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "rs-keycloak") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "1ngn90pczrc68xnw3r4zik41lycgn075dmh0avwlvs6wwdvzxqlj")))

(define-public crate-async-graphql-utils-0.4.2 (c (n "async-graphql-utils") (v "0.4.2") (d (list (d (n "async-graphql") (r "^5.0.10") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "rs-keycloak") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "05gwwb7lg2bxl4np38bz8f2rfxwiivfabgcf8c37fw53gxqrbafg")))

(define-public crate-async-graphql-utils-0.4.3 (c (n "async-graphql-utils") (v "0.4.3") (d (list (d (n "async-graphql") (r "^5.0.10") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "rs-keycloak") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "1lcdsji5mannky4n6xv8jd8mv244383y4j9mdlrjbsvc341683xa")))

(define-public crate-async-graphql-utils-0.4.4 (c (n "async-graphql-utils") (v "0.4.4") (d (list (d (n "async-graphql") (r "^5.0.10") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "rs-keycloak") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0ckdi98f26722rn1sn5m08zzn07c2rx13zbk5sk9c5s039apdcna")))

(define-public crate-async-graphql-utils-0.5.0 (c (n "async-graphql-utils") (v "0.5.0") (d (list (d (n "async-graphql") (r "^5.0.10") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "rs-keycloak") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "15nwnrqzksz8bl802dnrpfba1cx5qs24spwk0cxr7vm6zny15y20")))

