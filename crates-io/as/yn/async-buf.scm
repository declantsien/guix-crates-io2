(define-module (crates-io as yn async-buf) #:use-module (crates-io))

(define-public crate-async-buf-0.1.0 (c (n "async-buf") (v "0.1.0") (d (list (d (n "async-io") (r "^1.6") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0j75vvwjn4n27rmfklgc2lry3fm1vdjfrzgw8az1bywlf2m5yyid")))

