(define-module (crates-io as yn async-trait-ext) #:use-module (crates-io))

(define-public crate-async-trait-ext-0.1.0 (c (n "async-trait-ext") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0abh7gz31vdybfjla7rky7dq9akchkjzarfig299ck1n9qg8rcs2")))

(define-public crate-async-trait-ext-0.1.1 (c (n "async-trait-ext") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "09rvzniqy5n6857xgka0h485kkqw62yzfbkgqhq53vczr7h0d7jv") (f (quote (("provided") ("default"))))))

(define-public crate-async-trait-ext-0.2.0 (c (n "async-trait-ext") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "macro-compose") (r "^0.1") (d #t) (k 0)) (d (n "macro-input") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "16p4knybfb30r8w8f9vsy3wmcb95mk7ymvck7agypys3fnlk9cx2") (f (quote (("provided") ("default"))))))

(define-public crate-async-trait-ext-0.2.1 (c (n "async-trait-ext") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "macro-compose") (r "^0.1") (d #t) (k 0)) (d (n "macro-input") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "1ll1b4kzwbgchwi6q2wfb039afips2c17f0allghlpa1ckgr95wj") (f (quote (("provided") ("default"))))))

