(define-module (crates-io as yn async-alloc-counter) #:use-module (crates-io))

(define-public crate-async-alloc-counter-0.1.0 (c (n "async-alloc-counter") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0n47fxgph2s0qzk4lzii8vf2zg6jlam7dq1hyjr4f1aim1y66gmj")))

(define-public crate-async-alloc-counter-0.2.0 (c (n "async-alloc-counter") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1lpxqdfxsxqbfqah3fshpnx54010wzjy680ywnsysxj45cncvzgm")))

(define-public crate-async-alloc-counter-0.2.1 (c (n "async-alloc-counter") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.7") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1jw73lndgp9qjvr75f1q0qy5vsl6wlf09gmgl8121lnzzbsr0sw3")))

