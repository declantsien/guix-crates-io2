(define-module (crates-io as yn async-thread) #:use-module (crates-io))

(define-public crate-async-thread-0.1.0 (c (n "async-thread") (v "0.1.0") (d (list (d (n "async-std") (r "^1.3.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-channel") (r "^0.3.1") (d #t) (k 0)))) (h "1rcd388pwmcm97rzcpy304ikkr88ghaa3f5p8c8gn3qxj57c9nnd")))

(define-public crate-async-thread-0.1.1 (c (n "async-thread") (v "0.1.1") (d (list (d (n "async-std") (r "^1.3.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-channel") (r "^0.3.1") (d #t) (k 0)))) (h "13bmsz9id7ysqc6a7v2549r20wrhzvarirz735dk38jwwva0mi9b")))

(define-public crate-async-thread-0.1.2 (c (n "async-thread") (v "0.1.2") (d (list (d (n "async-std") (r "^1.3.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-channel") (r "^0.3.1") (d #t) (k 0)))) (h "1zgvw00mpnnaw2r5fqa9nwp4qn0wz4iqd31fy64bgljacg82g09z")))

