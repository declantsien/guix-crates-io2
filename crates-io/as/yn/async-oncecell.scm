(define-module (crates-io as yn async-oncecell) #:use-module (crates-io))

(define-public crate-async-oncecell-0.1.0 (c (n "async-oncecell") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0q7ym0schcv8x3l450bz69j67hwyq7f0mrh0h5fswqsw9wfrq7xr")))

(define-public crate-async-oncecell-0.2.0 (c (n "async-oncecell") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "18s1wij2k2iyamzy3c617911vjjai6pj5iq2n2igiai3bcd310bf")))

