(define-module (crates-io as yn async-hash) #:use-module (crates-io))

(define-public crate-async-hash-0.1.0 (c (n "async-hash") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0kh7r1l6wzdd71i9jmchwnk8bf9ajmf8wg4axzh57k9f4f3dginr")))

(define-public crate-async-hash-0.2.0 (c (n "async-hash") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0x2lhvbpfqjwd6h5yrzshq1af5yjv52qb22d0ssc6xpxzc4z77kf")))

(define-public crate-async-hash-0.2.1 (c (n "async-hash") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0156dwzjd60pcxg2asp1pxk5j318knl7pin0ppia9fnp4jagf2fn")))

(define-public crate-async-hash-0.3.0 (c (n "async-hash") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1384mmqnriyqywx9r33zh4qhpick9d8b6c2lhfida4np11jd5yw9")))

(define-public crate-async-hash-0.3.1 (c (n "async-hash") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "11nim5jxaw2gmxkswccqi7b8cm1y85fq6ir8p573ywk5nxrd0m5p")))

(define-public crate-async-hash-0.4.0 (c (n "async-hash") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "16bwfpyjangdx32ghr2pzsxhj4jklpb3agv496nm1b94r79p9b76") (y #t)))

(define-public crate-async-hash-0.4.1 (c (n "async-hash") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "02xn83x8lc2bafm7zq8h9x3jhnr8chr1wki5z6mrxx8i3v5sdi2j")))

(define-public crate-async-hash-0.5.0 (c (n "async-hash") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "01mqz8w487r9m402l5bpxam5ykny7lbsx58a8a4cw9lrhppimiij")))

(define-public crate-async-hash-0.5.1 (c (n "async-hash") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0nlsbrpgf36r65bw36lqf9p1qhb176sihjjz7r5igy3vg7shn3w1")))

(define-public crate-async-hash-0.5.2 (c (n "async-hash") (v "0.5.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (o #t) (d #t) (k 0)))) (h "0q5lgqhwgngdmd8jm22j4g3kix4gdfjmg5z1v60z4nkvylg0fzwx") (f (quote (("all" "smallvec"))))))

(define-public crate-async-hash-0.5.3 (c (n "async-hash") (v "0.5.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (o #t) (d #t) (k 0)))) (h "0q1yzz4693c7qlvmn66zlql8fg758hkk1rhcmn26v47l1pxb1fn0") (f (quote (("all" "smallvec"))))))

