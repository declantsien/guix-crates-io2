(define-module (crates-io as yn async_value) #:use-module (crates-io))

(define-public crate-async_value-0.1.0 (c (n "async_value") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1sgwwvgk2mh6k78gjzjda0x24wswlk3xv225lx1kjz1dzwdrnbwq")))

(define-public crate-async_value-0.1.1 (c (n "async_value") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0pgcpzcy8l5y0xq8w2h8g1a232ansvny93agdz7rn0qh1dw7526p")))

(define-public crate-async_value-0.2.0 (c (n "async_value") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "objekt") (r "^0.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0h27rd9s5vsmwa3q8ja0r2g24rk9c73sjvxjkqnwqpg4yh5kmym8")))

(define-public crate-async_value-0.2.1 (c (n "async_value") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "objekt") (r "^0.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "01bv7bvz67h7ym4klcv7sx6d1q658l17r57i7487fyym9494q0x8")))

(define-public crate-async_value-0.2.2 (c (n "async_value") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "objekt") (r "^0.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "10q1m3iiank1kxav07jfiali4y3h0nqh431agdvfh894xy1xck0w")))

(define-public crate-async_value-0.2.3 (c (n "async_value") (v "0.2.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "051lz691iha269dh2jcc37vlmin3j29qki3z63bi5z1g41d2giz9")))

(define-public crate-async_value-0.2.4 (c (n "async_value") (v "0.2.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1h6cqyvhhnw3l6pam3b8j1qmikyc22b5p6k8wif4py560p5a17p5")))

(define-public crate-async_value-0.2.5 (c (n "async_value") (v "0.2.5") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0h31cr4mzql0zsx8f3frr9fnl9jr33d6rwsa3494n5s3krjkkibh")))

(define-public crate-async_value-0.2.6 (c (n "async_value") (v "0.2.6") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "1gj24xbxrajk4p26x8sy886lh069x9c1cc53g0p9dzbg8c22m9im")))

(define-public crate-async_value-0.2.7 (c (n "async_value") (v "0.2.7") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "1bl5929yp9jyzkfbl2rncyavgd43zk3azvaw4rbxbxmd7fm1pn42")))

