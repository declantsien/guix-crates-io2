(define-module (crates-io as yn async-socket) #:use-module (crates-io))

(define-public crate-async-socket-0.1.0 (c (n "async-socket") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)))) (h "1rz5ny2c7w0j01fb7ipbaipcpzr4pa61s8fcbparrq45xqjwkbrs")))

(define-public crate-async-socket-0.1.1 (c (n "async-socket") (v "0.1.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)))) (h "0agrpgbfnl4v0k2mbsxf09d9x8ms6va3yni1scb92ahp82hsmkgs")))

(define-public crate-async-socket-0.1.2 (c (n "async-socket") (v "0.1.2") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)))) (h "0l4qa7kxy99drsmb10c744w54xrq2c3hd9nsdqjsnq9xcdn1dm4f")))

