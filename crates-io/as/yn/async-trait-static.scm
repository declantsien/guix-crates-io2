(define-module (crates-io as yn async-trait-static) #:use-module (crates-io))

(define-public crate-async-trait-static-0.1.0 (c (n "async-trait-static") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0wnpa99f4cd63lbjqkhb2k0b2l0pka5yrmh8mj2x61qpvabzgk7i")))

(define-public crate-async-trait-static-0.1.1 (c (n "async-trait-static") (v "0.1.1") (d (list (d (n "async-std") (r "^1.0.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0lwscpjr8p3q4jvh04yqc0ip6kclshj1xvvv2rfh4axpaa9rzi2v")))

(define-public crate-async-trait-static-0.1.2 (c (n "async-trait-static") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1xzhsw5np6grc9v5wp19k44zzkhfmpgl025r0f28n7qznqxyzi81")))

(define-public crate-async-trait-static-0.1.3 (c (n "async-trait-static") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1mq4s4dcgph460n8qxjq9qd9280kz5vkhbf554jxwa53ay7gkdjb")))

(define-public crate-async-trait-static-0.1.4 (c (n "async-trait-static") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0x90w8v0nd328fc4vry8ay98nlr20g0fdym9wvrmf2lalp8iy7zb")))

