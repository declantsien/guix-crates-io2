(define-module (crates-io as yn async_ach) #:use-module (crates-io))

(define-public crate-async_ach-0.0.1 (c (n "async_ach") (v "0.0.1") (h "0y6f4w418wabj6x3fpzi8qxnqqdw8cpm6v41d05hzv1rnxl05r3l")))

(define-public crate-async_ach-0.1.0 (c (n "async_ach") (v "0.1.0") (d (list (d (n "async_ach-cell") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-mpmc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-watch") (r "^0.1") (d #t) (k 0)))) (h "0cx953m84b2zkgivzr65kc9pfk0471x2695zgfpp7yhj2bcq6b9c")))

(define-public crate-async_ach-0.1.1 (c (n "async_ach") (v "0.1.1") (d (list (d (n "async_ach-cell") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-mpmc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-spsc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-watch") (r "^0.1") (d #t) (k 0)))) (h "0z3lyfnxc9c5cy7kv3j7hm7ryxhy317kb2ad9rg94xa9mr38krg2") (f (quote (("alloc" "async_ach-spsc/alloc" "async_ach-mpmc/alloc"))))))

(define-public crate-async_ach-0.1.2 (c (n "async_ach") (v "0.1.2") (d (list (d (n "async_ach-cell") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-mpmc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-spsc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-watch") (r "^0.1") (d #t) (k 0)))) (h "1jil5l3hrqcmzwqsn4c9jzs9dk9f0sfbzjag0af85v4w1grs7pf8") (f (quote (("alloc" "async_ach-spsc/alloc" "async_ach-mpmc/alloc"))))))

(define-public crate-async_ach-0.2.0 (c (n "async_ach") (v "0.2.0") (d (list (d (n "async_ach-cell") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-mpmc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-pubsub") (r "^0.2") (d #t) (k 0)) (d (n "async_ach-ring") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-spsc") (r "^0.2") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-watch") (r "^0.1") (d #t) (k 0)))) (h "066bbc7mnbdq391vr3i9cf1qbd1kdsfvl8a9ikxkraq27himpfrw") (f (quote (("alloc" "async_ach-spsc/alloc" "async_ach-mpmc/alloc"))))))

