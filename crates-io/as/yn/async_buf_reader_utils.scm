(define-module (crates-io as yn async_buf_reader_utils) #:use-module (crates-io))

(define-public crate-async_buf_reader_utils-0.1.0 (c (n "async_buf_reader_utils") (v "0.1.0") (d (list (d (n "async-std") (r "^1.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "futures-core-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)) (d (n "memchr") (r "^2") (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "08s1v527c9zirh7mnyxjla29rcqj2358knqvlqq6993prlh6k9ix")))

