(define-module (crates-io as yn async_bus) #:use-module (crates-io))

(define-public crate-async_bus-0.1.0 (c (n "async_bus") (v "0.1.0") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "1dd8wdzywq6ay95izhws9cvykbw4dpxd9ihydragf07s37db9935")))

