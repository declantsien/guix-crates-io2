(define-module (crates-io as yn async-wakers) #:use-module (crates-io))

(define-public crate-async-wakers-0.0.0 (c (n "async-wakers") (v "0.0.0") (h "11vzsk94b841mdjb9j8iwy7c35f3mnj1z9ppcdkhmqk359c124fx")))

(define-public crate-async-wakers-0.1.0-alpha.1 (c (n "async-wakers") (v "0.1.0-alpha.1") (d (list (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "02p7ainb4rmqpbqk95wsh6xic2gnijqxr0c081m7qz87qwpk8aw7") (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.2 (c (n "async-wakers") (v "0.1.0-alpha.2") (d (list (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0wxpkhbz0z2wgg8pb0c8shg4wppkzkbjj0zrzmr65y2jdi41yln0") (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.3 (c (n "async-wakers") (v "0.1.0-alpha.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)))) (h "1wziq5pg6bavph8hcf6487ccq1838znvf8346nz5nsx5sfw1ln13") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.4 (c (n "async-wakers") (v "0.1.0-alpha.4") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)))) (h "0f19zw3w5bninn57ajym22p64rdljrk58h5lsrvwhbk1rfx4yb1n") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.5 (c (n "async-wakers") (v "0.1.0-alpha.5") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)))) (h "0kj8hmlpxjh738px0k3lsbaqx8rfx296l0h52mdmyxx68ar7hmkn") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.6 (c (n "async-wakers") (v "0.1.0-alpha.6") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)))) (h "1m4qxdxcxawcnf5936ys327hjairxk7jkyfnzvi1dsw6fac4sv4p") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.7 (c (n "async-wakers") (v "0.1.0-alpha.7") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)))) (h "0plig5972i8zw3chhv013c4ckg5y44y3dxs8dlrscpnqp5wrbcyd") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.8 (c (n "async-wakers") (v "0.1.0-alpha.8") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)))) (h "1dk992nscx3njslzqv11lnm05nps9iriy0xrwfpxnrp1glga1cf8") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.9 (c (n "async-wakers") (v "0.1.0-alpha.9") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)))) (h "155by61cbsvsriy44x1zj40sx13vq332ajss04f5sirdy32ziix1") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.10 (c (n "async-wakers") (v "0.1.0-alpha.10") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)))) (h "01zm6gfpjkxb9ymvqjrysdkrfh0risyjwkr9iwi0v0xnk2fvnl3d") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.11 (c (n "async-wakers") (v "0.1.0-alpha.11") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)))) (h "1m31g83qxpkx4fp1czpvrlllbzrqn8c1lw8mj3v1nk5x2lwhxb60") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.12 (c (n "async-wakers") (v "0.1.0-alpha.12") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)))) (h "1v8b4b6g4a1xv2qv9ab8cq5agh9dwmwifjbyf4n6mlbi3jcix02y") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.13 (c (n "async-wakers") (v "0.1.0-alpha.13") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "1snhwhahgg1hdsdh3g9r77y8i23k419lxcfj1jdjpvws4gv9cpnr") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.14 (c (n "async-wakers") (v "0.1.0-alpha.14") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "0z29clqivl0mh1llbxb28dx22b651y2hlfij1jspcapy39rk5l2z") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.15 (c (n "async-wakers") (v "0.1.0-alpha.15") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "0plbkrjmpbfcn230skbwpi2z634lcm278yjckn1wa8pbwwk9iz8q") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.16 (c (n "async-wakers") (v "0.1.0-alpha.16") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "0d21wsxjmpkrm9vrfyalqzj0ggcamcks11wnikmwdmk0hg9zklga") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.17 (c (n "async-wakers") (v "0.1.0-alpha.17") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "05408zdcglngbgbcxmxib3hzl78fv6k66s4cx7alf1r8hnnyrmr8") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.18 (c (n "async-wakers") (v "0.1.0-alpha.18") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "16a1jxw9aaijrs9ddnx2ghxjm7hcmc96br2lv24c0rh3xnwp86r2") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.19 (c (n "async-wakers") (v "0.1.0-alpha.19") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "1s5rcrwxqlq3kz0dqydm5vxa28l5czfscy2gqb0gr77l50zh6dpw") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.20 (c (n "async-wakers") (v "0.1.0-alpha.20") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "1rzbhcxywr763fjpg4rsh7vrk1k081an4lv3yk0xmlns09d99aql") (f (quote (("std" "crossbeam-utils/std") ("default" "std") ("alloc" "crossbeam-utils/alloc")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.21 (c (n "async-wakers") (v "0.1.0-alpha.21") (d (list (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "0ayipbjh4ng2f1skjvy9dhyhlwijmwjq72fvm6lc2mfy5lvmpbda") (f (quote (("default")))) (y #t)))

(define-public crate-async-wakers-0.1.0-alpha.22 (c (n "async-wakers") (v "0.1.0-alpha.22") (d (list (d (n "crossbeam-utils") (r "^0.7") (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "time"))) (d #t) (k 2)))) (h "1mj54qlrkv0q1jy4v4biph1kjda9d8fsc70h7h6wha931g9fw9xz") (f (quote (("default")))) (y #t)))

