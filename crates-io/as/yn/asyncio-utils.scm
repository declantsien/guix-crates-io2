(define-module (crates-io as yn asyncio-utils) #:use-module (crates-io))

(define-public crate-asyncio-utils-0.4.1 (c (n "asyncio-utils") (v "0.4.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nbql6gmc31rjvj1kwkaa4b18har5if8i66xwz5gypx5qi4b1i01")))

(define-public crate-asyncio-utils-0.4.2 (c (n "asyncio-utils") (v "0.4.2") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f4xgm5xhsywvxvn4qbchp6vj6my0hwjxr1kp634x1flzsd5xmv9")))

(define-public crate-asyncio-utils-0.4.3 (c (n "asyncio-utils") (v "0.4.3") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00p6x3xng149fwi7fcmd91v6qkzg0fqsqay97hcdw8h11a7fjlxx")))

(define-public crate-asyncio-utils-0.4.4 (c (n "asyncio-utils") (v "0.4.4") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0afarkk52077yqx181lvj7ia4qzc8i9qs17ha9a6sbd415cd9gd6")))

