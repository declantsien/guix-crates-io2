(define-module (crates-io as yn async) #:use-module (crates-io))

(define-public crate-async-0.0.1 (c (n "async") (v "0.0.1") (h "07km6kr899kxz1ckhm2mk6k3f2b3ghr1qibknq64qrnl20ifrrdp")))

(define-public crate-async-0.0.2 (c (n "async") (v "0.0.2") (h "1kxfr2y3fq3q7z99lclxk9lgr7n6xq4xwm49rm9nb5ccqakfbg1k")))

