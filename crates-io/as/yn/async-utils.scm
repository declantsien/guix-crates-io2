(define-module (crates-io as yn async-utils) #:use-module (crates-io))

(define-public crate-async-utils-0.1.0 (c (n "async-utils") (v "0.1.0") (h "0wgvv638vb3ydaaghykv09d2pn71zxyybi8j5zl7gyxgq0zxwx04") (y #t)))

(define-public crate-async-utils-0.2.0 (c (n "async-utils") (v "0.2.0") (h "10flica6fxd515k2lscidgva5r1jdhcj8cpyyyj1w5q4b2wd3066") (y #t)))

(define-public crate-async-utils-0.1.1 (c (n "async-utils") (v "0.1.1") (h "0n0fbd46i08mlf0nadqn79g2w0lay5rkai3p4rljgajk4p4rxzci")))

