(define-module (crates-io as yn async-cpupool) #:use-module (crates-io))

(define-public crate-async-cpupool-0.1.0 (c (n "async-cpupool") (v "0.1.0") (d (list (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.21.1") (d #t) (k 0)) (d (n "smol") (r "^1.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "19f52a5b3a635k6521ng2ywdpdqlrbapacgv9cjlabjzbrnm01ms")))

(define-public crate-async-cpupool-0.2.0 (c (n "async-cpupool") (v "0.2.0") (d (list (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.22.0") (d #t) (k 0)) (d (n "smol") (r "^2.0.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1j9nz2fbyhn6vpl3spwkvi01jv29407iwh7m3mcjfxprw773703f")))

(define-public crate-async-cpupool-0.2.1 (c (n "async-cpupool") (v "0.2.1") (d (list (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.22.0") (d #t) (k 0)) (d (n "smol") (r "^2.0.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0qv8gsmvnq015xkzd4sg1fcfzndgk2945d8cfyqzf3b8xgzslxby")))

(define-public crate-async-cpupool-0.2.2 (c (n "async-cpupool") (v "0.2.2") (d (list (d (n "metrics") (r "^0.22.0") (d #t) (k 0)) (d (n "smol") (r "^2.0.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1gfjmz6hw561pv22kqg78ih0h94fib17ww618q5fjk96k85cy86r")))

