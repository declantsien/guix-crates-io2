(define-module (crates-io as yn asyncapiv3) #:use-module (crates-io))

(define-public crate-asyncapiv3-0.1.1 (c (n "asyncapiv3") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ks4x0d4irwc2lv1cas6jvfakc850l5h6a5a0jm4mwnnq8aw3wrf") (f (quote (("builder_unstable" "log" "thiserror"))))))

(define-public crate-asyncapiv3-0.1.2 (c (n "asyncapiv3") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "196ib0xy43rdh5h9yjmch369zn8y2z8xmb3dn3545qwkbj78bmrf") (f (quote (("builder_unstable" "log" "thiserror"))))))

