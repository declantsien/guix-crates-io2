(define-module (crates-io as yn async-tcp) #:use-module (crates-io))

(define-public crate-async-tcp-0.0.0 (c (n "async-tcp") (v "0.0.0") (h "0vhjbgy21p148m55l5lqabpz5akkm3pik88ixygw3qpbhdylmfdc")))

(define-public crate-async-tcp-0.1.0 (c (n "async-tcp") (v "0.1.0") (d (list (d (n "async-io_") (r "^0.2") (o #t) (k 0) (p "async-io")) (d (n "async-net_") (r "^1.0") (o #t) (k 0) (p "async-net")) (d (n "async-peek") (r "^0.3") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.4") (d #t) (k 2)))) (h "0yhyzr6napa4ka2170hik36i9xclxid4lizxnr61jrdrx9hal72g") (f (quote (("async-net" "async-net_" "async-peek/async-net") ("async-io" "async-io_" "async-peek/async-io")))) (y #t)))

(define-public crate-async-tcp-0.2.0 (c (n "async-tcp") (v "0.2.0") (d (list (d (n "async-io_") (r "^0.2") (o #t) (k 0) (p "async-io")) (d (n "async-net_") (r "^1.0") (o #t) (k 0) (p "async-net")) (d (n "async-peek") (r "^0.3") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.4") (d #t) (k 2)))) (h "0rs9vwv9lvx6s4cpsjd60xzqgs6dim1rr9rca10q2wvc4r12xl5g") (f (quote (("async-net" "async-net_" "async-peek/async-net") ("async-io" "async-io_" "async-peek/async-io"))))))

