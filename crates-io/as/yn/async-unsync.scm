(define-module (crates-io as yn async-unsync) #:use-module (crates-io))

(define-public crate-async-unsync-0.1.0 (c (n "async-unsync") (v "0.1.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)))) (h "11lvgkrgzqkpvsm59h0v1jzzj78gv8kcqfw6vp2j4bfqjvx3k41c") (f (quote (("std") ("default" "std"))))))

(define-public crate-async-unsync-0.1.1 (c (n "async-unsync") (v "0.1.1") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)))) (h "0i7n9rmdfhsrxfri2db7jxg32zmzdjpq8gnc91yr320qx5lv5x17") (f (quote (("std") ("default" "std"))))))

(define-public crate-async-unsync-0.2.0 (c (n "async-unsync") (v "0.2.0") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "rt-multi-thread" "sync"))) (o #t) (d #t) (k 0)))) (h "0pqywr4f6mmxdcgxnjdmi68xxrvi7320fdgvnbwcssqlbjn08q1j") (f (quote (("std" "alloc") ("default" "std") ("bench" "tokio") ("alloc"))))))

(define-public crate-async-unsync-0.2.1 (c (n "async-unsync") (v "0.2.1") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "rt-multi-thread" "sync"))) (o #t) (d #t) (k 0)))) (h "0mabb07rlhlfrmrqwyv5pish530j6xzmlk5x3dc0hmxzzqq5p6zh") (f (quote (("std" "alloc") ("default" "std") ("bench" "tokio") ("alloc"))))))

(define-public crate-async-unsync-0.2.2 (c (n "async-unsync") (v "0.2.2") (d (list (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "rt-multi-thread" "sync"))) (o #t) (d #t) (k 0)))) (h "0735j4jbnl0cmmf8b0kqm1km0s9wnhw4iw711hm5ddcxj5rwbk03") (f (quote (("std" "alloc") ("default" "std") ("bench" "tokio") ("alloc"))))))

(define-public crate-async-unsync-0.3.0 (c (n "async-unsync") (v "0.3.0") (d (list (d (n "futures-lite") (r "^2.2.0") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "rt-multi-thread" "sync"))) (o #t) (d #t) (k 0)))) (h "1zkyzar7nhc8vxm8jv6wxwx222dnvq88y9nzfl2bp7bvsd79affs") (f (quote (("std" "alloc") ("default" "std") ("bench" "tokio") ("alloc"))))))

