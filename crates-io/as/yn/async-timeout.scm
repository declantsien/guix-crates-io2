(define-module (crates-io as yn async-timeout) #:use-module (crates-io))

(define-public crate-async-timeout-0.0.1 (c (n "async-timeout") (v "0.0.1") (h "12xz6zml7l04xihdar2ajc9dgnxrlm960zcjvgw67sr5dk3xxms6")))

(define-public crate-async-timeout-0.1.0 (c (n "async-timeout") (v "0.1.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)))) (h "1zdx7i3vi1nd68fy5yv0i73512jwalan8ahxhmvjy68g1prc8z1j")))

(define-public crate-async-timeout-0.1.1 (c (n "async-timeout") (v "0.1.1") (d (list (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)))) (h "0g03h5rdh3l7ap3aif9bi8y832p1fdwci4jci6nxa7xrfkj3j82c")))

(define-public crate-async-timeout-0.2.0 (c (n "async-timeout") (v "0.2.0") (d (list (d (n "tokio") (r ">=0.3.0, <0.4.0") (f (quote ("time"))) (d #t) (k 0)))) (h "1s115bdjk7r92c5xh8sla79v7yvavfcajfm5h3smg5752x7l5fvb")))

