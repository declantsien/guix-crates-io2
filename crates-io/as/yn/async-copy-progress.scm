(define-module (crates-io as yn async-copy-progress) #:use-module (crates-io))

(define-public crate-async-copy-progress-1.0.0 (c (n "async-copy-progress") (v "1.0.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "spin_on") (r "^0.1.0") (d #t) (k 2)))) (h "0ii9wlb4hbv6hff8nivfg59kwg4c1m9kk4p3s7q5shjl0vq22jgm")))

(define-public crate-async-copy-progress-1.0.1 (c (n "async-copy-progress") (v "1.0.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "spin_on") (r "^0.1.0") (d #t) (k 2)))) (h "1jjh8w9qynn36pjjck0xmpsj6xm8l476vd8qlhcjhkj5c2gl82b4")))

