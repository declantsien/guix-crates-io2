(define-module (crates-io as yn async-task-executor) #:use-module (crates-io))

(define-public crate-async-task-executor-0.1.2 (c (n "async-task-executor") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "084qjxgp8sn8hg2a55jbspn0pqgsia1bh5rmx01r2z33hx36wp6s")))

