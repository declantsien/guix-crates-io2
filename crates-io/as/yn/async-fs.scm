(define-module (crates-io as yn async-fs) #:use-module (crates-io))

(define-public crate-async-fs-0.0.0 (c (n "async-fs") (v "0.0.0") (h "1k9gbjzd3h0m412lndic1rpfvir2wq3r1bg6fy0jgl0dwmg59fai")))

(define-public crate-async-fs-1.0.0 (c (n "async-fs") (v "1.0.0") (d (list (d (n "blocking") (r "^0.5.0") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.9") (d #t) (k 0)))) (h "0l841hlrq8y1vxhkpwnrqkrq2lb60ji5r591w241rd6axmcs5mbs")))

(define-public crate-async-fs-1.0.1 (c (n "async-fs") (v "1.0.1") (d (list (d (n "blocking") (r "^0.5.0") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.9") (d #t) (k 0)))) (h "07ifl31ryp931faadycyaw7lc9k3f3vd3v5h838k0py3akxp0a34")))

(define-public crate-async-fs-1.1.0 (c (n "async-fs") (v "1.1.0") (d (list (d (n "blocking") (r "^0.5.0") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.9") (d #t) (k 0)))) (h "0526k6lxn6ca78whvplxpq4s92qh6qyxzhd6h9v72wawac587k23")))

(define-public crate-async-fs-1.1.1 (c (n "async-fs") (v "1.1.1") (d (list (d (n "blocking") (r "^0.6.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 0)))) (h "085xvqk9lch454zw12ll6ribbkj10lvfxk9yyqidyvppz2p4n3nr")))

(define-public crate-async-fs-1.1.2 (c (n "async-fs") (v "1.1.2") (d (list (d (n "blocking") (r "^0.6.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 0)))) (h "1xx67ql1qiw9z4avc7qjl0k5zf9r9bp7fz4276adzw3xydff9zsd")))

(define-public crate-async-fs-1.2.0 (c (n "async-fs") (v "1.2.0") (d (list (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "10yjpn140pcd9n78f8jqp1wxn9z3z295v7ps5hjw5qj2d995hspv")))

(define-public crate-async-fs-1.2.1 (c (n "async-fs") (v "1.2.1") (d (list (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "0x7fha7shavlnp24ysp5pirvmw2b8jwdkf91rgr6l0l8sa4d0q9k")))

(define-public crate-async-fs-1.3.0 (c (n "async-fs") (v "1.3.0") (d (list (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "00lh1yb46rdddymjbcasjb6w1bi0zmdbv83lnsi7q51pp8v24mz3")))

(define-public crate-async-fs-1.4.0 (c (n "async-fs") (v "1.4.0") (d (list (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.78") (d #t) (t "cfg(unix)") (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 2)))) (h "0na2xb93h19466fci23g9mw737apgnx9wksh0jn34z2wfgwz7561")))

(define-public crate-async-fs-1.5.0 (c (n "async-fs") (v "1.5.0") (d (list (d (n "async-lock") (r "^2.3.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.78") (d #t) (t "cfg(unix)") (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 2)))) (h "1qnsqg0jjpda590w8nvbhh5mcmdyx5f43xx2g313fz0izzwa8g4b")))

(define-public crate-async-fs-1.6.0 (c (n "async-fs") (v "1.6.0") (d (list (d (n "async-lock") (r "^2.3.0") (d #t) (k 0)) (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.78") (d #t) (t "cfg(unix)") (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 2)))) (h "01if2h77mry9cnm91ql2md595108i2c1bfy9gaivzvjfcl2gk717") (r "1.47")))

(define-public crate-async-fs-2.0.0 (c (n "async-fs") (v "2.0.0") (d (list (d (n "async-lock") (r "^2.3.0") (d #t) (k 0)) (d (n "blocking") (r "^1.3.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.78") (d #t) (t "cfg(unix)") (k 2)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 2)))) (h "1mj2mh6g00valgcqqbmkxkhhjsryi63dwqcg0zmqbbl72s11kn0r") (r "1.63")))

(define-public crate-async-fs-2.1.0 (c (n "async-fs") (v "2.1.0") (d (list (d (n "async-lock") (r "^3.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.3.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.78") (d #t) (t "cfg(unix)") (k 2)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 2)))) (h "06x09sl85m7nrks0m54ardsmvf1agzyz6irsz3qlvlxs6r0k87yx") (r "1.63")))

(define-public crate-async-fs-2.1.1 (c (n "async-fs") (v "2.1.1") (d (list (d (n "async-lock") (r "^3.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.3.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.78") (d #t) (t "cfg(unix)") (k 2)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 2)))) (h "18dbwan238f3xdisb5x6pnrw6j95xp97srq54hgr51zjf4qnh6dw") (r "1.63")))

(define-public crate-async-fs-2.1.2 (c (n "async-fs") (v "2.1.2") (d (list (d (n "async-lock") (r "^3.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.3.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.78") (d #t) (t "cfg(unix)") (k 2)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 2)))) (h "0jp0p7lg9zqy2djgdmivbzx0yqmfn9sm2s9dkhaws3zlharhkkgb") (r "1.63")))

