(define-module (crates-io as yn async-event-dispatch) #:use-module (crates-io))

(define-public crate-async-event-dispatch-0.1.1 (c (n "async-event-dispatch") (v "0.1.1") (d (list (d (n "deadqueue") (r "^0.2.4") (f (quote ("unlimited" "limited"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "0ayb1slkx1vwr29w8s73gkwbbay7l1lklnlsj3hyw8zwb0xslnya")))

