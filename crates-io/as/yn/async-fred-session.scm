(define-module (crates-io as yn async-fred-session) #:use-module (crates-io))

(define-public crate-async-fred-session-0.1.0 (c (n "async-fred-session") (v "0.1.0") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "fred") (r "^5.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1w9qyl5vdciwi81l7yf02kjyyadr10if14gvcg3l9i5rnv9s6d0f")))

(define-public crate-async-fred-session-0.1.1 (c (n "async-fred-session") (v "0.1.1") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "fred") (r "^5.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "074685yc502i5d2819bphl23p2vxkds4cczwz2vdbv0zp7igrmaf")))

(define-public crate-async-fred-session-0.1.2 (c (n "async-fred-session") (v "0.1.2") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "fred") (r "^5.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0z28jglla6q56w5qnrgv0mvmyg1452lb26qri3720bbm7nrh1dkm")))

(define-public crate-async-fred-session-0.1.3 (c (n "async-fred-session") (v "0.1.3") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "fred") (r "^6.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1r0xpgyv50wf2qwnhd7bil54l6r6g1y2q2d824d47zq0c3ayilx3")))

(define-public crate-async-fred-session-0.1.4 (c (n "async-fred-session") (v "0.1.4") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "fred") (r "^6.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0kh5mvdnlaj9n7aa2sviljcc1q9ym4m09q39nnyp2272pz67lfqj")))

(define-public crate-async-fred-session-0.1.5 (c (n "async-fred-session") (v "0.1.5") (d (list (d (n "async-session") (r "^3.0.0") (d #t) (k 0)) (d (n "fred") (r "^6.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1yja7kxlk5pak24xhgzsd2x90vjky3vhq7y7wyx0rpxf4jzsyh80")))

