(define-module (crates-io as yn async-tick) #:use-module (crates-io))

(define-public crate-async-tick-0.1.1 (c (n "async-tick") (v "0.1.1") (d (list (d (n "ach-lazy") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tick_clock") (r "^0.1") (d #t) (k 0)))) (h "1md4z60gw12l28bh7hg7jxnyjnwmlw5bgmxc4q90jpn0r7gdnnjr")))

(define-public crate-async-tick-0.1.2 (c (n "async-tick") (v "0.1.2") (d (list (d (n "ach-lazy") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tick_clock") (r "^0.1") (d #t) (k 0)))) (h "07r28il95k24aqw016n9ysbh7rd91p73f4sbm3fy0vqn6ic18ggd") (f (quote (("std") ("default" "std"))))))

(define-public crate-async-tick-0.1.3 (c (n "async-tick") (v "0.1.3") (d (list (d (n "ach-lazy") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tick_clock") (r "^0.1") (d #t) (k 0)))) (h "0nzmq6nmsddn3075qzg4zhlvy4l0gkbkk7va5kag6038djx2mgwc") (f (quote (("std") ("default" "std"))))))

(define-public crate-async-tick-0.1.4 (c (n "async-tick") (v "0.1.4") (d (list (d (n "ach-lazy") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tick_clock") (r "^0.1") (d #t) (k 0)))) (h "0gcw8iy3frfvaxhyzdaf8xsp3d5i9d50l9pywcflffhl0zyq7i56") (f (quote (("std") ("default" "std"))))))

(define-public crate-async-tick-0.1.5 (c (n "async-tick") (v "0.1.5") (d (list (d (n "ach-lazy") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "tick_clock") (r "^0.1") (d #t) (k 0)))) (h "179cyqpnfqxz1dzx80nwj0mgfkpqvjqh3d4501grym88bfgggd6x") (f (quote (("std") ("default" "std"))))))

(define-public crate-async-tick-0.1.6 (c (n "async-tick") (v "0.1.6") (d (list (d (n "ach-lazy") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "tick_clock") (r "^0.1") (d #t) (k 0)))) (h "0v94c3ybxyf7gddc7cf5g0mhix1makhzxschrj083dl2m0vyni3s") (f (quote (("std") ("default" "std"))))))

(define-public crate-async-tick-0.1.7 (c (n "async-tick") (v "0.1.7") (d (list (d (n "ach-lazy") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.1") (d #t) (k 0)) (d (n "atomic-polyfill") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "tick_clock") (r "^0.1") (d #t) (k 0)))) (h "1x2si1qbfidld3fkcf229b4718r98n04lg6qrdkjgns8w5h749z7") (f (quote (("std") ("default" "std"))))))

(define-public crate-async-tick-0.1.8 (c (n "async-tick") (v "0.1.8") (d (list (d (n "ach-lazy") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-waker") (r "^0.2") (d #t) (k 0)) (d (n "atomic-polyfill") (r "^1.0") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "tick_clock") (r "^0.1") (d #t) (k 0)))) (h "0s15648w0lb4aq7490z6fraj5w1j751l9as1gzx9q7k0zxb6lhwn") (f (quote (("std") ("default" "std"))))))

