(define-module (crates-io as yn async-event) #:use-module (crates-io))

(define-public crate-async-event-0.1.0 (c (n "async-event") (v "0.1.0") (d (list (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(async_event_loom)") (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "waker-fn") (r "^1.1") (d #t) (t "cfg(async_event_loom)") (k 2)))) (h "1nxv4lvh0yk26ya21s9951gmalx36lz75rbbc108ddpzlxfmjwj1") (r "1.56")))

(define-public crate-async-event-0.2.0 (c (n "async-event") (v "0.2.0") (d (list (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(async_event_loom)") (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "waker-fn") (r "^1.1") (d #t) (t "cfg(async_event_loom)") (k 2)))) (h "0yjrlqwjrnsyj83rm5ni4lgrjrb9mcj4li8sm1z130sh7y44dmp0") (r "1.77")))

