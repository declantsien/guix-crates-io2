(define-module (crates-io as yn async_wrr_queue) #:use-module (crates-io))

(define-public crate-async_wrr_queue-0.1.0 (c (n "async_wrr_queue") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync" "rt" "macros"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("default" "rt-multi-thread"))) (d #t) (k 2)))) (h "1r4lnkvc176idxbln9frxjigcg1nkhaqjmzhilzjjzd7zzf51wl9") (f (quote (("default" "tokio") ("blocking")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-async_wrr_queue-0.1.1 (c (n "async_wrr_queue") (v "0.1.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync" "rt" "macros"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("default" "rt-multi-thread"))) (d #t) (k 2)))) (h "0sjbxc88p2pi80vgar3ipkxqdfhc7krymb85qjjg9k0lcglkwcbw") (f (quote (("default" "tokio") ("blocking")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-async_wrr_queue-0.1.2 (c (n "async_wrr_queue") (v "0.1.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync" "rt" "macros"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("default" "rt-multi-thread"))) (d #t) (k 2)))) (h "0m4jakpwkzfn718pfdnd2n18kcqk4pyx4b65p0i8ac3bf4fqrr2s") (f (quote (("default" "tokio") ("blocking")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

