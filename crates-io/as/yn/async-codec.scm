(define-module (crates-io as yn async-codec) #:use-module (crates-io))

(define-public crate-async-codec-0.1.0 (c (n "async-codec") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "039jjmzf8yx8gv73p2zpklmg1xx2wcwx2mygriwhr6687rvsgixk")))

(define-public crate-async-codec-0.1.1 (c (n "async-codec") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "03gqf3pfyi8hbvh4arhh1x7zz3vpzhhkysvgxpkf5lpciwcb46lv")))

(define-public crate-async-codec-0.1.2 (c (n "async-codec") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "14l177rznyykhn7q3hpq4crn9l810pzh5nxm8qg6bp05iq4aj9bv")))

(define-public crate-async-codec-0.2.0 (c (n "async-codec") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0i8yj3qwqq2ibh35xhrzbcs2m43hj4lkkrawblpkszg390ayks44")))

(define-public crate-async-codec-0.3.0 (c (n "async-codec") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "075rxlp1wmy1b2gpjzr7ghmqlmarh9frp87zdamnfsvd2ni2lhfg")))

(define-public crate-async-codec-0.4.0-alpha.0 (c (n "async-codec") (v "0.4.0-alpha.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (k 0)) (d (n "runtime") (r "^0.3.0-alpha.6") (d #t) (k 2)))) (h "1xdzlafiz90sc5qd4rii6a7jk2shwkdw9aaznwcrhg37lha25a20") (f (quote (("std" "futures-preview/std") ("default" "std"))))))

(define-public crate-async-codec-0.4.0-alpha.1 (c (n "async-codec") (v "0.4.0-alpha.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.17") (k 0)) (d (n "runtime") (r "^0.3.0-alpha.6") (d #t) (k 2)))) (h "0q9z7ykfbd4dl7bbl5854984r95ic91mr7d1m2w1jp2546xx33sr") (f (quote (("std" "futures-preview/std") ("default" "std"))))))

(define-public crate-async-codec-0.4.0-alpha.2 (c (n "async-codec") (v "0.4.0-alpha.2") (d (list (d (n "bytes") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (k 0)) (d (n "runtime") (r "^0.3.0-alpha.6") (d #t) (k 2)))) (h "013l1k3rziyhz04valcy262yfz4201wr0vjaj6pwam9i8bapmpsd") (f (quote (("std" "futures-preview/std" "bytes") ("default" "std"))))))

(define-public crate-async-codec-0.4.0-alpha.3 (c (n "async-codec") (v "0.4.0-alpha.3") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (k 0)))) (h "1lzgmwybg64x4invgsvkgiwkc6zn73hjzwq5gwqa4yn3q22w5h48") (f (quote (("std" "futures/std" "bytes") ("default" "std"))))))

(define-public crate-async-codec-0.4.0-alpha.4 (c (n "async-codec") (v "0.4.0-alpha.4") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "bytes") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (k 0)))) (h "05c6j1vs7xaqbl1154mvmvws7b1wn48f6bwgyb552i5z4jw7adra") (f (quote (("std" "futures/std" "bytes") ("default" "std"))))))

(define-public crate-async-codec-0.4.0 (c (n "async-codec") (v "0.4.0") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "bytes") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (k 0)) (d (n "polymer-core") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0xm3wcf4slcl9rx0skw3rniy1vxhg9r7ids6havc7sj2xkpcyaa0") (f (quote (("std" "futures/std" "bytes") ("embrio" "polymer-core") ("default" "std"))))))

(define-public crate-async-codec-0.4.1 (c (n "async-codec") (v "0.4.1") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "bytes") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (k 0)) (d (n "polymer-core") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "199609dw01djyabfdqi50ra5x76r4fmrym2r49rap8v0bghjj3xg") (f (quote (("std" "futures/std" "bytes") ("embrio" "polymer-core") ("default" "std"))))))

