(define-module (crates-io as yn async-serialization) #:use-module (crates-io))

(define-public crate-async-serialization-0.1.0 (c (n "async-serialization") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0ynifc19dyj19qi8hldb9fynf8fhljw3llni97ngd3adkwxj5d79")))

(define-public crate-async-serialization-0.1.1 (c (n "async-serialization") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0l5m9bnqw1b8ai9c35rwz0wagk2s0k39q1v7ax6qp92c7k0kz71y")))

(define-public crate-async-serialization-0.2.0 (c (n "async-serialization") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "1p10dvvas7d9ykd9l0xiqrcs10v08vhz10zpvnxwzh7clrvj50js")))

(define-public crate-async-serialization-0.2.1 (c (n "async-serialization") (v "0.2.1") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0gdr6il81pfyifki2kcyfnhkxxfk0yb4k697qnk9dc7sgnvqbbgk")))

(define-public crate-async-serialization-0.2.2 (c (n "async-serialization") (v "0.2.2") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "188z100jswnmn060706alp02avnsy5bacqj40cd0xxd1br75ycai")))

