(define-module (crates-io as yn async-gemini) #:use-module (crates-io))

(define-public crate-async-gemini-0.0.1 (c (n "async-gemini") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wb74vfjh5kaxv8cz44kq59pyyly6zm3m32x2kfi05zp343xvdah")))

(define-public crate-async-gemini-0.0.2 (c (n "async-gemini") (v "0.0.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18j0pmkzjrh4vaxhxpqx7n53dp3fkyscswf9gli6wqcn9zdlcmq6")))

(define-public crate-async-gemini-0.0.4 (c (n "async-gemini") (v "0.0.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1x6l5yz1hr1n064r7vsz028d4xkb5bwkmgk0kp87h2i5cwfwd2vc") (f (quote (("util") ("models") ("default" "models" "util"))))))

(define-public crate-async-gemini-0.0.5 (c (n "async-gemini") (v "0.0.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1zxm93qlz4yrv0flsjfydy1yl8m79bdxx2a5645xlrjaa76yg4dk") (f (quote (("util") ("models") ("default" "models" "util"))))))

(define-public crate-async-gemini-0.1.0 (c (n "async-gemini") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15aafkgdgqvlwfqil147zwyw3nncab8dbg01j63jy9cf6fyisjcc") (f (quote (("util") ("models") ("default" "models" "util"))))))

