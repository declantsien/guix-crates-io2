(define-module (crates-io as yn async-attributes) #:use-module (crates-io))

(define-public crate-async-attributes-0.1.0 (c (n "async-attributes") (v "0.1.0") (h "0nmsv4a3vy0vwp70nv6064jc824akymq4cz3frc5mjip0s5akw8v")))

(define-public crate-async-attributes-0.3.0-alpha.5 (c (n "async-attributes") (v "0.3.0-alpha.5") (d (list (d (n "async-std") (r "^0.99.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mdr5drwjl2xwa6c6m4snh32kl41va1vmc94lcivd5f8mfd8z4yp")))

(define-public crate-async-attributes-1.0.0 (c (n "async-attributes") (v "1.0.0") (d (list (d (n "async-std") (r "^0.99.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01jdbnjy5djymb894b53vrx9yrvh79nzws9vg74fkv0kw1ixhrjj")))

(define-public crate-async-attributes-1.1.0 (c (n "async-attributes") (v "1.1.0") (d (list (d (n "async-std") (r "^0.99.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y5k39dg1bcrsgwmzna0zxwbjjs6099cqd6irqfml2grv7r52xac")))

(define-public crate-async-attributes-1.1.1 (c (n "async-attributes") (v "1.1.1") (d (list (d (n "async-std") (r "^0.99.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08w41342hybxhln7j7hjsf7v04p3r9d6qdczfwp8d53xj5bd3lzg")))

(define-public crate-async-attributes-1.1.2 (c (n "async-attributes") (v "1.1.2") (d (list (d (n "async-std") (r "^0.99.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rcnypqgmlcw9vwh0fk8bivvz8p5v8acy0zd2njdv6yxyiwkw853")))

