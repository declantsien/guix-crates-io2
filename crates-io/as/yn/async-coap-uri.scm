(define-module (crates-io as yn async-coap-uri) #:use-module (crates-io))

(define-public crate-async-coap-uri-0.1.0 (c (n "async-coap-uri") (v "0.1.0") (d (list (d (n "async-coap-uri-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1b4mp35jdjkccdxl5a9jplbqdyp7mzpqlrc2lc6507syan3w7nx6") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

