(define-module (crates-io as yn async_ach-pubsub) #:use-module (crates-io))

(define-public crate-async_ach-pubsub-0.1.0 (c (n "async_ach-pubsub") (v "0.1.0") (d (list (d (n "ach-pubsub") (r "^0.1") (d #t) (k 0)) (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0z17nnvpqivw7xnjl5iwlq0mds92bdqymsjadsjay84ml34b8gxb")))

(define-public crate-async_ach-pubsub-0.1.1 (c (n "async_ach-pubsub") (v "0.1.1") (d (list (d (n "ach-pubsub") (r "^0.1") (d #t) (k 0)) (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0c0840jps6nf7f9v4zsxssl7sl99hr9x6d11v1wk0z14yy9py751")))

(define-public crate-async_ach-pubsub-0.1.2 (c (n "async_ach-pubsub") (v "0.1.2") (d (list (d (n "ach-pubsub") (r "^0.1") (d #t) (k 0)) (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "0jnqjva0rqrqvcqxr4x9wnihfiw0cr039qg15wi22vx2binq0yzi")))

(define-public crate-async_ach-pubsub-0.2.0 (c (n "async_ach-pubsub") (v "0.2.0") (d (list (d (n "ach-pubsub") (r "^0.2") (d #t) (k 0)) (d (n "ach-util") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "0xcy1ndq9skj25ywy0wy4nm6wf2pb7gn4pcnsh50bzk0z833lvbb") (f (quote (("default") ("alloc" "ach-pubsub/alloc"))))))

