(define-module (crates-io as yn async-proto-derive) #:use-module (crates-io))

(define-public crate-async-proto-derive-0.2.0 (c (n "async-proto-derive") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1d2885j5w304bq4q74q59g342d64jg6wxkdkbmq07sx3al8s32ah") (f (quote (("blocking"))))))

(define-public crate-async-proto-derive-0.3.0 (c (n "async-proto-derive") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "062s2cnjs1jf4g89d1kmavqljm0jwp4z0yslw0mxk6cjwqgqzswa") (f (quote (("blocking"))))))

(define-public crate-async-proto-derive-0.3.1 (c (n "async-proto-derive") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0nsv9xbmxi71rrgynmrw2qm1bzqkrbz00s99kvw867b0w0zjbskh") (f (quote (("blocking"))))))

(define-public crate-async-proto-derive-0.4.0 (c (n "async-proto-derive") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07s8njkfpmrpl5hgj3vyaflkcwb338by13rbv525kqrkvkmw1cr0") (f (quote (("blocking"))))))

(define-public crate-async-proto-derive-0.5.0 (c (n "async-proto-derive") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0445gg8baql5rmchgkcjy8hn435xqaaqs1cldp8cx9lldc3ra01y") (f (quote (("blocking"))))))

(define-public crate-async-proto-derive-0.6.0 (c (n "async-proto-derive") (v "0.6.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01gczh1qvy1hayp421r4bzlg17i376jd2sj28m0h4237dd7v4z50") (f (quote (("blocking"))))))

(define-public crate-async-proto-derive-0.6.1 (c (n "async-proto-derive") (v "0.6.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1n0awd3l1qxaskw0aa1lhsra3i6lc4qxjry9ky7bvq35mf1srwyx") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.6.2 (c (n "async-proto-derive") (v "0.6.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10vj8dx0zb72hg01r6j0gx0rb1zd70ghld5qysi55v6pjqj0m25z") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.7.0 (c (n "async-proto-derive") (v "0.7.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14cxw2as3acdxal6cfbjfhs3xdl8925g7d1qi37v5jq6jinjxslj") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.7.1 (c (n "async-proto-derive") (v "0.7.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jyflygh3rq2jhg8dpsfybvrabd1y1sbpqpxrjfgb3bj8p32bri0") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.8.0 (c (n "async-proto-derive") (v "0.8.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mfaps2xlk4j5nns5xcvb46bpac4b1wp3adbcybh2dzblz5pm1g6") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.8.1 (c (n "async-proto-derive") (v "0.8.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0j7x9yziya9chv6y7s6jck9jg5g14mn3aqgibqz1vglx2r08x6lk") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.8.2 (c (n "async-proto-derive") (v "0.8.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1siwdgbanj2sbwf2jq7cx043fkqc770klm3yfdpz3naswvfzydib") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.8.3 (c (n "async-proto-derive") (v "0.8.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "116d09kmd4avgqjhx3qzd22b6lkkfhdjagymp70aywfp0cvkgf41") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.8.4 (c (n "async-proto-derive") (v "0.8.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mlz24cbhn6g9c1la4kb9n45hrpcjzn31a42hql1hnl8fq1fygcm") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.8.5 (c (n "async-proto-derive") (v "0.8.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "06rkn1zd2sfj9r6ws5hgydcdvp50ylc8vl232sp9z59rzizyc0jm") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.9.0 (c (n "async-proto-derive") (v "0.9.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1yig042niyjlbpkzlkxf7vanzq2prqdsk6idnfgf93r3hpg8v9ci") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.9.1 (c (n "async-proto-derive") (v "0.9.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10qgdg18alvz053sdvzf74hshsd3rrfx48d0xciflh78z7szcnpf") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.10.0 (c (n "async-proto-derive") (v "0.10.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0m07g13hq20rqsavbkfl9qvm76v1fz52lv89bbk3jqpmpkmnddrs") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.11.0 (c (n "async-proto-derive") (v "0.11.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "15jfxv638ch33hx38bmql1jmbsazy7nhqynn45qghqyf6w4lvm7x") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.11.1 (c (n "async-proto-derive") (v "0.11.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1x9nf7k40s5as93xzchznwax3s48vjkal3298r8dbn07jvsnrlpw") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.11.3 (c (n "async-proto-derive") (v "0.11.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0i7cjdiqq5k5kpsh1nnd18gg5g2b4dc56w1ipyvjxshmfh8mkh9x") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.11.4 (c (n "async-proto-derive") (v "0.11.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "17nxs7pavsrdv6mhqsqhrmyp92kywdav2sdaqzj2bbiij2d1ry16") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.11.5 (c (n "async-proto-derive") (v "0.11.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07dnj32yk5iyqczipf4619c4ghaylvp11ykq7g5hlbfqwna9x1m6") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.12.0 (c (n "async-proto-derive") (v "0.12.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01jjjsfwhjb700bal33dl8z924gk198j20a29izrb38bxhq8h75x") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.12.1 (c (n "async-proto-derive") (v "0.12.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rzmzdw6pffn5dwfvy2zrw29qqn7xp2qfllg9bq5ji7x770swab8") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.12.2 (c (n "async-proto-derive") (v "0.12.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vfis0j4rldykjpibiidn7m4in46hzvq67ah1n9y0kb4wycvffp1") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.12.3 (c (n "async-proto-derive") (v "0.12.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0sjfyx70fjq8wqx19xby9rzwyy3im8mkv0savh2qfng4kkyy58ig") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.13.0 (c (n "async-proto-derive") (v "0.13.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1maf132psgqsxp55dqd8gazp4jhvjspmnisb3w069gdni4fpfj4x") (f (quote (("write-sync") ("read-sync"))))))

(define-public crate-async-proto-derive-0.14.0 (c (n "async-proto-derive") (v "0.14.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g6ibrzzyzjkb5kzklxgamlax4wy83h62dn64xf4ka4c1dl00z0v")))

(define-public crate-async-proto-derive-0.14.1 (c (n "async-proto-derive") (v "0.14.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "021vm0dxapxmhpi2ql925gvqw1dnxykrlv5xfvjdqw6980jqgrmq")))

(define-public crate-async-proto-derive-0.14.2 (c (n "async-proto-derive") (v "0.14.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "072dm7i5a3rqhklr1yz4m799kh2vvwqdwd96kd8hnnzz4bia2bd9")))

(define-public crate-async-proto-derive-0.15.0 (c (n "async-proto-derive") (v "0.15.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0r6gam8j9dri67a5zg5b9h9rz60qad8k8bjqvzs9ccw0zxg6x5f9")))

(define-public crate-async-proto-derive-0.15.1 (c (n "async-proto-derive") (v "0.15.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1afwq7zab4z9qz0k170pfrarsn499y8vkqw0k9s7y2402v3warzz")))

(define-public crate-async-proto-derive-0.15.2 (c (n "async-proto-derive") (v "0.15.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nnmnrkzxypqmxlybbwdgv2vwiqrhzrliw4cqflrs4ihp5ixjp4n")))

(define-public crate-async-proto-derive-0.16.0 (c (n "async-proto-derive") (v "0.16.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04sxpqwagsqnzxcz917i6rf7r5m61jp3g6wm47qwz2flq9ag7fi8")))

(define-public crate-async-proto-derive-0.16.1 (c (n "async-proto-derive") (v "0.16.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0r7b681shyf63y5dqb2a55aj4w0zcs1zgk0pn3x1195iz8azch1b")))

(define-public crate-async-proto-derive-0.16.2 (c (n "async-proto-derive") (v "0.16.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06igxgm5z6iz1h9vb79a1nsvvssxixhss1a4psnpvfx9gryr7cv6")))

(define-public crate-async-proto-derive-0.16.3 (c (n "async-proto-derive") (v "0.16.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "001dg8spl5bhp0zqw1g4cds5ii1kvv9lf7hpppcwchd9ckhr3s38")))

(define-public crate-async-proto-derive-0.16.4 (c (n "async-proto-derive") (v "0.16.4") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1n0gda29w7ssx7d7a1wvc1nmxm0rlx2s256xslr5mhfiirwr73vr")))

(define-public crate-async-proto-derive-0.16.5 (c (n "async-proto-derive") (v "0.16.5") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "12aqnvjsi64a461q0qhwn5ll8c3d9ifpdw2vi9yd0ww3ghrvhrqx")))

(define-public crate-async-proto-derive-0.16.6 (c (n "async-proto-derive") (v "0.16.6") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "12ll4mpwrfaxrb12mfnizlsgszc1s53vxrx5vim5haj03s3vr5wd")))

(define-public crate-async-proto-derive-0.16.7 (c (n "async-proto-derive") (v "0.16.7") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03ajchhky4bh8d03gprzcv3ldz2wikpzlldvg3m9kh06mwmvzv8k")))

(define-public crate-async-proto-derive-0.16.8 (c (n "async-proto-derive") (v "0.16.8") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1vww0w9faaf3vf2ii97ymb1wiajnm6y4hwif1graiz6r26jaadrx")))

(define-public crate-async-proto-derive-0.16.9 (c (n "async-proto-derive") (v "0.16.9") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "122d7yjzd2w08vc6sbr76m6m3zxgkqklh4nfw29jgyad0fayx6pk")))

(define-public crate-async-proto-derive-0.16.10 (c (n "async-proto-derive") (v "0.16.10") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "078r8s6agx6hrb72s92v043xdf6nj0xd8xvy9krsswihj4s9mliq")))

(define-public crate-async-proto-derive-0.17.0 (c (n "async-proto-derive") (v "0.17.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "16d4crcw33zafigsgik6019x77pmqrasrlwfgxd59iz3nf5a8bpw")))

(define-public crate-async-proto-derive-0.18.0 (c (n "async-proto-derive") (v "0.18.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1h6v5agh523vrb0x8v46ys46k91zk6w8s0kfb3fh1999l81hwyqz")))

(define-public crate-async-proto-derive-0.19.0 (c (n "async-proto-derive") (v "0.19.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gvgr0wh6l3ldqr01i3ysxqqcn49nwaap1q4hc7575pv4h12slvg")))

(define-public crate-async-proto-derive-0.19.1 (c (n "async-proto-derive") (v "0.19.1") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11walhhv90km44aw6zhxcnk2sm7ya4xgc3rin6jyvg31fdss3mli")))

(define-public crate-async-proto-derive-0.19.2 (c (n "async-proto-derive") (v "0.19.2") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0cj22fcb2kxnm6zd2lv0bqzl1zdz4r70pkwpmvm4y5v6d1ds1a1l")))

(define-public crate-async-proto-derive-0.19.3 (c (n "async-proto-derive") (v "0.19.3") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1f9nf21scvj906qvhn9w8l6kpb466y7np0si2a3620mhmp0yybz9")))

(define-public crate-async-proto-derive-0.19.4 (c (n "async-proto-derive") (v "0.19.4") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "01kgb7zpfl0lr7kscp9lmdas03ivhqfi7wfk4nmzgzy14z3hdq0b")))

(define-public crate-async-proto-derive-0.19.5 (c (n "async-proto-derive") (v "0.19.5") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0i6nfy9530by3wk8lcrxzvm2256wagdzmixh6sssk8w93bdfhil2")))

(define-public crate-async-proto-derive-0.19.6 (c (n "async-proto-derive") (v "0.19.6") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0c2lznb9ga5hjb037ihh81pghvfgx45zm3wfhwr0ybmi06p10s38")))

(define-public crate-async-proto-derive-0.19.7 (c (n "async-proto-derive") (v "0.19.7") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gxhzi0ni64xvgizxd65lj97iz4yzylzidc46w2ppq8wynmnhsn8")))

(define-public crate-async-proto-derive-0.20.0 (c (n "async-proto-derive") (v "0.20.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1gdzq2p7mf9bmwl4j50m72ggxw9ri0iglx46ssgvfc8gj82g5m2y")))

(define-public crate-async-proto-derive-0.21.0 (c (n "async-proto-derive") (v "0.21.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0b6ml8x8z75llb6cma0zg580krq2iks5yfxn67qj1ihdaw8gr8w9")))

(define-public crate-async-proto-derive-0.21.1 (c (n "async-proto-derive") (v "0.21.1") (d (list (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "13yrwb6f369gjc1pdkc1p8n3p00ihq7mmqncv1zlyk9y4lp8mql0")))

(define-public crate-async-proto-derive-0.21.2 (c (n "async-proto-derive") (v "0.21.2") (d (list (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1hbc1c4z3qy7cv9d8zbrj9kl5nxzjiw08vhwy46knl4z18xgywiw")))

(define-public crate-async-proto-derive-0.22.0 (c (n "async-proto-derive") (v "0.22.0") (d (list (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0c006xqspa5s5majr8w9928x3pkvbq0a9dn8s6lv275ddkqfahrf")))

