(define-module (crates-io as yn asyncapi) #:use-module (crates-io))

(define-public crate-asyncapi-0.1.0 (c (n "asyncapi") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.7.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "0addnkmiivgq2wlgkag3n4wwjlcq14jfpf14fsqjjzz5l4snd1i5")))

(define-public crate-asyncapi-0.1.1 (c (n "asyncapi") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.7.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "0m202sbzbpj12pjb20lvnpz2hk25hll8aqgd207532pira1cas53")))

(define-public crate-asyncapi-0.1.2 (c (n "asyncapi") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.7.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "1m4wgjn8r43dkcvrw1aiv201d36yx2d9hyyljggzks0ql9psy7pm")))

(define-public crate-asyncapi-0.1.3 (c (n "asyncapi") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.7.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "01yri239wqdp1w1ykm0qa1w2pminqa7hn2hclzmyymaagj82lrl7")))

(define-public crate-asyncapi-0.2.0 (c (n "asyncapi") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.8.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "04x7qls5pd9d5niqgin691a4y67w059yhhx2s45s9nwlad5gkrwp")))

