(define-module (crates-io as yn async-gcode) #:use-module (crates-io))

(define-public crate-async-gcode-0.2.0 (c (n "async-gcode") (v "0.2.0") (d (list (d (n "either") (r "^1") (k 0)) (d (n "futures") (r "^0.3.5") (k 0)) (d (n "futures-executor") (r "^0.3.5") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.7") (d #t) (k 0)))) (h "0ggjgpdbngg665g772ii8qqy256sy8gcljhrl9qmhrkqp4z143rk") (f (quote (("string-value") ("std") ("parse-trailing-comment") ("parse-parameters") ("parse-expressions") ("parse-comments") ("parse-checksum") ("optional-value") ("default" "std"))))))

(define-public crate-async-gcode-0.3.0 (c (n "async-gcode") (v "0.3.0") (d (list (d (n "either") (r "^1") (k 0)) (d (n "futures") (r "^0.3.21") (k 0)) (d (n "futures-executor") (r "^0.3.21") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "0wydzzgskcw1cjr7w1dqhfhhbx22iwms7j69l2g4szdjfr293h3y") (f (quote (("string-value") ("std") ("parse-trailing-comment") ("parse-parameters") ("parse-expressions") ("parse-comments") ("parse-checksum") ("optional-value") ("default" "std"))))))

