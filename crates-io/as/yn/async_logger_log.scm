(define-module (crates-io as yn async_logger_log) #:use-module (crates-io))

(define-public crate-async_logger_log-0.1.1 (c (n "async_logger_log") (v "0.1.1") (d (list (d (n "async_logger") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 2)) (d (n "time") (r "^0.2.9") (f (quote ("std"))) (d #t) (k 0)))) (h "0vqknkw58cl1znq95h198vvzrjz5grlvvlv3x6l864gn6amr2mrv")))

(define-public crate-async_logger_log-0.1.2 (c (n "async_logger_log") (v "0.1.2") (d (list (d (n "async_logger") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 2)) (d (n "time") (r "^0.2.9") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0ayc2yij7s0xxi5jgqmgp11ywlgf3rmmx5dy8qkpsxmwm9ci3l5p") (f (quote (("default" "time"))))))

(define-public crate-async_logger_log-0.2.0 (c (n "async_logger_log") (v "0.2.0") (d (list (d (n "async_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 2)) (d (n "time") (r "^0.2.9") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "03akwk6c8vv9d4vfdfdgwi4w9jvlccqfw48s4ajf7r4dm5fdrcnn") (f (quote (("default" "time"))))))

