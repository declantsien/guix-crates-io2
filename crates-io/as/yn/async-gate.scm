(define-module (crates-io as yn async-gate) #:use-module (crates-io))

(define-public crate-async-gate-0.1.3 (c (n "async-gate") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("sync"))) (d #t) (k 0)))) (h "1rf12jllg2ha4lai4j1zy2lpv6c73wc1aiyifa8njniydpr5174l")))

(define-public crate-async-gate-0.1.4 (c (n "async-gate") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("sync"))) (d #t) (k 0)))) (h "03ki5974cb6bmszk9ay4c5yljpaj5xzc1d4vdva1wjf95sghmhw4")))

(define-public crate-async-gate-0.2.0 (c (n "async-gate") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("sync"))) (d #t) (k 0)))) (h "0hi24mrg6z3snc0vbp209nmasncmnpl2bsdr74c77ymzk1bqji91")))

(define-public crate-async-gate-0.2.1 (c (n "async-gate") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("sync"))) (d #t) (k 0)))) (h "109lks3mp77w0idbrmmy1yy70bb5q6jg5i0nq2fc98qp4n7mkg27")))

(define-public crate-async-gate-0.3.1 (c (n "async-gate") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "08axkrp7qd26p582m7b11rsqnqlg21ca8r93pkz8g745skl0bwkw")))

