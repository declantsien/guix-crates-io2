(define-module (crates-io as yn async_channel_io) #:use-module (crates-io))

(define-public crate-async_channel_io-0.3.0 (c (n "async_channel_io") (v "0.3.0") (d (list (d (n "async-channel") (r "^2.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 2)))) (h "179iargqybf7flm12r4jni8zd3ar3a06b9wgkcp76xyzxss7m0g2")))

