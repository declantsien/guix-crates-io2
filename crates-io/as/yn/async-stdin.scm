(define-module (crates-io as yn async-stdin) #:use-module (crates-io))

(define-public crate-async-stdin-0.1.0 (c (n "async-stdin") (v "0.1.0") (h "0h5y0cqcfvyr2sa02z6ryghw18k93zd2ln9m1z74af64acfb49xj") (y #t)))

(define-public crate-async-stdin-0.2.0 (c (n "async-stdin") (v "0.2.0") (h "1q23j0fbjk198l4mkkx8lhsxv5qd8q3m6nqmnb5qbc59yvh8g5kf") (y #t)))

(define-public crate-async-stdin-0.3.0 (c (n "async-stdin") (v "0.3.0") (d (list (d (n "tokio") (r "^1.27.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0pqq92xm1bmvdnc6l3d36zirms0bvgkr6bi2j3kxilhn7lc0qmix")))

(define-public crate-async-stdin-0.3.1 (c (n "async-stdin") (v "0.3.1") (d (list (d (n "tokio") (r "^1.27.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)))) (h "08k8m81hjf4a0hrh2ld8cy5qij5q8wcaffsq947rxhjykdfqpzxi")))

