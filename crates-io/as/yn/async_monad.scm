(define-module (crates-io as yn async_monad) #:use-module (crates-io))

(define-public crate-async_monad-0.1.0 (c (n "async_monad") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 2)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1c5bfcxk47kvx2flyh75rj77hjkhfm398rw6zjfs7y9jg0ajqg6j")))

(define-public crate-async_monad-0.2.0 (c (n "async_monad") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 2)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1qj81pjygb0bn4v5kj6dg030kflfkacbh14sfq07g1w14m42fvrj") (f (quote (("default" "async_trait") ("async_trait"))))))

