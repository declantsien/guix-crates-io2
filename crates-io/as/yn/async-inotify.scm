(define-module (crates-io as yn async-inotify) #:use-module (crates-io))

(define-public crate-async-inotify-0.1.0 (c (n "async-inotify") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "inotify") (r "^0.10.1") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread" "sync" "signal"))) (d #t) (k 0)))) (h "10grjm56l2zrcbrbf7mc8arsb2pcn30yppmv98hyx64yaq89va17")))

