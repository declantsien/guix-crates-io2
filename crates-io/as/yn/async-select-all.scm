(define-module (crates-io as yn async-select-all) #:use-module (crates-io))

(define-public crate-async-select-all-0.1.0 (c (n "async-select-all") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.8") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)))) (h "1yqx08awvxl9hj7hsrw8rfchh6f2pvwimagxlp82x9k827rxfq3l")))

