(define-module (crates-io as yn async-ffi-macros) #:use-module (crates-io))

(define-public crate-async-ffi-macros-0.0.0 (c (n "async-ffi-macros") (v "0.0.0") (h "03rvm4h8qal9xfl83flsfn9vbsmdhbwx36vjw2ivg40hd0iffb2z")))

(define-public crate-async-ffi-macros-0.4.1 (c (n "async-ffi-macros") (v "0.4.1") (d (list (d (n "async-ffi") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07kj5ysqani9zcbdslmqzlk6wcxs7cjp9z6xsiyfh5d083y3gvmy") (r "1.49")))

(define-public crate-async-ffi-macros-0.5.0 (c (n "async-ffi-macros") (v "0.5.0") (d (list (d (n "futures-task") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a9d8lm8d3s04d0vndb0jw401zqc7kz1y2y9vf82g8p6aky2gjng") (r "1.56")))

