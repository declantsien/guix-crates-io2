(define-module (crates-io as yn async-net) #:use-module (crates-io))

(define-public crate-async-net-0.0.0 (c (n "async-net") (v "0.0.0") (h "15y1w79siiyfn2f5phshqcfm9js11pzvbkj7az6dx42rzym859rh")))

(define-public crate-async-net-0.1.0 (c (n "async-net") (v "0.1.0") (d (list (d (n "async-io") (r "^0.1.1") (d #t) (k 0)) (d (n "blocking") (r "^0.4.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "futures-io") (r "^0.3.5") (f (quote ("std"))) (k 0)) (d (n "futures-util") (r "^0.3.5") (f (quote ("std" "io"))) (k 0)))) (h "0p4vdch4y53njmxwal86y5hdx3jq9j4rmf4z1dgyffqw4hw5qbwb")))

(define-public crate-async-net-0.1.1 (c (n "async-net") (v "0.1.1") (d (list (d (n "async-io") (r "^0.1.3") (d #t) (k 0)) (d (n "blocking") (r "^0.4.7") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.3") (d #t) (k 0)))) (h "1lcqxj0266xr5r9vy1aqh7pw0irilxm26ziwiav911s330d37a06")))

(define-public crate-async-net-0.1.2 (c (n "async-net") (v "0.1.2") (d (list (d (n "async-io") (r "^0.1.3") (d #t) (k 0)) (d (n "blocking") (r "^0.5.0") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1.3") (d #t) (k 0)))) (h "00sbp1iz098dqd24awjcbhfrm51gx7208vgddd0jlmldxbi90rsm")))

(define-public crate-async-net-1.0.0 (c (n "async-net") (v "1.0.0") (d (list (d (n "async-io") (r "^0.2.0") (d #t) (k 0)) (d (n "blocking") (r "^0.6.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 0)))) (h "1ci6y1asj8mr4ykzybyaqqbasnh9q8ikz3pdccvyc38yhgvi3nvr")))

(define-public crate-async-net-1.1.0 (c (n "async-net") (v "1.1.0") (d (list (d (n "async-io") (r "^0.2.0") (d #t) (k 0)) (d (n "blocking") (r "^0.6.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 0)))) (h "0sma42ca2v630jiak7l4ai4kfdq4a931fnhsw6pgp1x9ssiir2m6")))

(define-public crate-async-net-1.2.0 (c (n "async-net") (v "1.2.0") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "141xw2qv66pcdyx1vw4jy5m0xbbr3395031knkm198bvq9cd7hgr")))

(define-public crate-async-net-1.3.0 (c (n "async-net") (v "1.3.0") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "12wvvi8hidg5s54yxk2zj7jmnwca2jaxiidnvrmhamp8id1zb2m4")))

(define-public crate-async-net-1.4.0 (c (n "async-net") (v "1.4.0") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "0zq47z61gkvpml49k4ly418zikwpkal3cijhg6bz1wxvm1wipi1a")))

(define-public crate-async-net-1.4.1 (c (n "async-net") (v "1.4.1") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "09n54a44gvb8v1dc5sn4bsdg7fr9ywhhda9asmilb6ida4vziv05")))

(define-public crate-async-net-1.4.2 (c (n "async-net") (v "1.4.2") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "0rm7sqc3maapv7p35dri1qc5qmway29xn08zbcj3w7090nzfpkgj")))

(define-public crate-async-net-1.4.3 (c (n "async-net") (v "1.4.3") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "1kxxxhq6m3sh0fpyg2rbisr0lq3689xhmvkasmcqv8xsfwmlh17v")))

(define-public crate-async-net-1.4.4 (c (n "async-net") (v "1.4.4") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "1qk711d2a0hxsg5j0c26yka8b4mx0kdck09ikib94sqlrnqhap7l")))

(define-public crate-async-net-1.4.5 (c (n "async-net") (v "1.4.5") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "0n7326r1pr38gwgk6db2nizxc1q16b7i5abys4ipfj13pwx998w5")))

(define-public crate-async-net-1.4.6 (c (n "async-net") (v "1.4.6") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2.0") (d #t) (k 0)))) (h "0ap8ygzzfaa023vmxcclryxav24pvwvbk5dzapw2c62lar83798l")))

(define-public crate-async-net-1.4.7 (c (n "async-net") (v "1.4.7") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 0)))) (h "13s2di2mgh5lkfd791bs81ywfmxli596n0phjwg7h789xdl3ck7f")))

(define-public crate-async-net-1.5.0 (c (n "async-net") (v "1.5.0") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 0)))) (h "1rgvvqb1l86hawl1j0jfyzq35yracbbh29109131izmghmf4gph6")))

(define-public crate-async-net-1.6.0 (c (n "async-net") (v "1.6.0") (d (list (d (n "async-io") (r "^1.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 0)))) (h "1pzgxh2s5h8xlxw2picscl35fya4s3drwlxay663rbvhgx7agc39")))

(define-public crate-async-net-1.6.1 (c (n "async-net") (v "1.6.1") (d (list (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 0)))) (h "1psiqp553fhw116ww1kkh848c5y7kqv81c4g0sal76wvyx6k0wsk")))

(define-public crate-async-net-1.7.0 (c (n "async-net") (v "1.7.0") (d (list (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 0)))) (h "07qz85xjmm0zyiyy0sfdmm39jqzd69fxy8z7ixhgyzmw2rrycla0") (r "1.47")))

(define-public crate-async-net-1.8.0 (c (n "async-net") (v "1.8.0") (d (list (d (n "async-io") (r "^1.6.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 0)))) (h "0by1m21ciwq6kvd8jplb0mqr9yh17zil1icakdvga76f33nv2d04") (r "1.63")))

(define-public crate-async-net-2.0.0 (c (n "async-net") (v "2.0.0") (d (list (d (n "async-io") (r "^2.0.0") (d #t) (k 0)) (d (n "blocking") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 0)))) (h "1xyc5a5vcp3a7h1q2lbfh79wz8136dig4q4x6g4w2ws8ml7h0j5r") (r "1.63")))

