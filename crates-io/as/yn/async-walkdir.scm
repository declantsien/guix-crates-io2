(define-module (crates-io as yn async-walkdir) #:use-module (crates-io))

(define-public crate-async-walkdir-0.1.0 (c (n "async-walkdir") (v "0.1.0") (d (list (d (n "async-fs") (r "^1.1") (d #t) (k 0)) (d (n "futures-lite") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1xphsk89l809aj0vfj0siv9zwh46s1hhxvgrzmvdzxgll52zyw85")))

(define-public crate-async-walkdir-0.2.0 (c (n "async-walkdir") (v "0.2.0") (d (list (d (n "async-fs") (r "^1.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "145vfq41m9dzzigffn9g99gsd9psc5sl4vjvcd5m1rw77vbqhvc2")))

(define-public crate-async-walkdir-1.0.0 (c (n "async-walkdir") (v "1.0.0") (d (list (d (n "async-fs") (r "^2.1.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "1nwkj5aqmfqx7fh414faa4xiiwfwsk9q73nbxiahbz6b4f037xkk")))

