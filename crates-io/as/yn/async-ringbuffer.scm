(define-module (crates-io as yn async-ringbuffer) #:use-module (crates-io))

(define-public crate-async-ringbuffer-0.1.0 (c (n "async-ringbuffer") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "16v25yp84hj9v6z779vz9pknv49k0hkg624wlhhr8c42n1rclj6z")))

(define-public crate-async-ringbuffer-0.2.0 (c (n "async-ringbuffer") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "10vl7c3gp40zl7zr50pvff969pgn905gn91lxs2r7bawzgllqmjf")))

(define-public crate-async-ringbuffer-0.3.0 (c (n "async-ringbuffer") (v "0.3.0") (d (list (d (n "futures") (r "^0.2.0-alpha") (d #t) (k 2)) (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0gzgmnrl0hwqczvc5xpnn5irl9cy7126f4k91m42i6r6vjfxcsz3")))

(define-public crate-async-ringbuffer-0.3.1 (c (n "async-ringbuffer") (v "0.3.1") (d (list (d (n "futures") (r "^0.2.0-beta") (d #t) (k 2)) (d (n "futures-core") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-beta") (d #t) (k 0)))) (h "12ljgs39hj4anqwi0x64hb5jr0h7bfzp4cx82267kji4ybq15x43")))

(define-public crate-async-ringbuffer-0.4.0 (c (n "async-ringbuffer") (v "0.4.0") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "futures-io-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 2)))) (h "1rcna4w2fnidia4f83jr4psv9vvv92anw08cyxyfaa63il6x9pzf")))

(define-public crate-async-ringbuffer-0.5.0 (c (n "async-ringbuffer") (v "0.5.0") (d (list (d (n "futures-io-preview") (r "^0.3.0-alpha.15") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.15") (d #t) (k 2)))) (h "1w2al0sq7fdw9a6gqmgsym62asdyri8sr6k3w7ix9gb631pv7zdp")))

(define-public crate-async-ringbuffer-0.5.1 (c (n "async-ringbuffer") (v "0.5.1") (d (list (d (n "futures-io-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 2)))) (h "06sqhki5ivblx3m13cnxvfcc8skyqs05qv4nmkv7znm35knapgm6")))

(define-public crate-async-ringbuffer-0.5.2 (c (n "async-ringbuffer") (v "0.5.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-io") (r "^0.3.1") (d #t) (k 0)))) (h "138mpi05r7lfphky14xjf6zafis8crxjxzi7030qkkc3ni5kjchm")))

(define-public crate-async-ringbuffer-0.5.4 (c (n "async-ringbuffer") (v "0.5.4") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-io") (r "^0.3.1") (d #t) (k 0)))) (h "0kn1j0g5z1w0hdwmmxhzlhhsparvqcmpqd7hm4yp51fknd9bc87m")))

(define-public crate-async-ringbuffer-0.5.5 (c (n "async-ringbuffer") (v "0.5.5") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-io") (r "^0.3.1") (d #t) (k 0)))) (h "177z2ca9615jb56nxry3w6xrv5lnxrb5x0mr0hgfc3w6pi0hsmm8")))

