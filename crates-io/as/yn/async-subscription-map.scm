(define-module (crates-io as yn async-subscription-map) #:use-module (crates-io))

(define-public crate-async-subscription-map-0.1.0 (c (n "async-subscription-map") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-observable") (r "^0.2") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^2") (d #t) (k 2)))) (h "042s7i1y1l2wi0h1kxa3nbp8k7alis9vpd6bk77r3m00l1nd0bg7")))

