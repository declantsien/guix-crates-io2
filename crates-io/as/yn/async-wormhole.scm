(define-module (crates-io as yn async-wormhole) #:use-module (crates-io))

(define-public crate-async-wormhole-0.1.0 (c (n "async-wormhole") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)) (d (n "switcheroo") (r "^0.1.0") (d #t) (k 0)))) (h "06jhx8dhjmay6vvv57bcxzlwhqkaa3hk9mkyh028aqnrxd13nlci")))

(define-public crate-async-wormhole-0.1.1 (c (n "async-wormhole") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)) (d (n "switcheroo") (r "^0.1.1") (d #t) (k 0)))) (h "11r0s886a8rscx5yj577v9ds26s98128vqpgdwaj2wrpww7np04p")))

(define-public crate-async-wormhole-0.1.2 (c (n "async-wormhole") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)) (d (n "switcheroo") (r "^0.1.2") (d #t) (k 0)))) (h "1anms1msc5qbrng8f3d1xmzw6wjmb6yaid9f4f7ha7ifid8bhi5i")))

(define-public crate-async-wormhole-0.2.0 (c (n "async-wormhole") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "0dj50a880qalrqagsq8x00lbfpkf6b7mkbqf4psf8ghz5pvy26s9")))

(define-public crate-async-wormhole-0.2.1 (c (n "async-wormhole") (v "0.2.1") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "0nmafzfxnwz0p8izh9m1nw05c1c5s45jr8llj8a3vnfj890sx01b")))

(define-public crate-async-wormhole-0.2.2 (c (n "async-wormhole") (v "0.2.2") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "0is2knm9ki85s2vygwrd7m4ifqs19ar2szwcjp1413990s7r957v")))

(define-public crate-async-wormhole-0.3.0 (c (n "async-wormhole") (v "0.3.0") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "16panb343scdvpf65qwnciv2m56bgfx57c4z8wbfy1p7w70g3pcd")))

(define-public crate-async-wormhole-0.3.1 (c (n "async-wormhole") (v "0.3.1") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "04hz2byxs847g18cq5mqqwj2a01rl5klpphksp3khabninxac44m")))

(define-public crate-async-wormhole-0.3.2 (c (n "async-wormhole") (v "0.3.2") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "1n7wphw21lqsyg188nfjspk93wr1zi40bs0gzjc01x2byb8iq562")))

(define-public crate-async-wormhole-0.3.3 (c (n "async-wormhole") (v "0.3.3") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "1k303nyz8qwjv5dssylnb6nwa1man6i1a7byp8cmx6c095rbpv1n")))

(define-public crate-async-wormhole-0.3.4 (c (n "async-wormhole") (v "0.3.4") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "0iihlq2ddilnwv2g9wcpgaapa37y5hz135pzadi2540av2x2684n")))

(define-public crate-async-wormhole-0.3.5 (c (n "async-wormhole") (v "0.3.5") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "199f4hs4hnrfj2f2h5li1gxgj52swv7xaq1aallsx4qm9lsk8v90")))

(define-public crate-async-wormhole-0.3.6 (c (n "async-wormhole") (v "0.3.6") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "1c7sjgp3rji9j0wwm02n6j376nz2cwdgpm93i2jwi5zx07j7ijls")))

(define-public crate-async-wormhole-0.3.7 (c (n "async-wormhole") (v "0.3.7") (d (list (d (n "async-executor") (r "^1.4") (d #t) (k 2)) (d (n "backtrace") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "switcheroo") (r "^0.2") (d #t) (k 0)))) (h "0q8295m3v08677k1x047zp88yhz4pr5cykjz3an6j7f7kvq6zl7x")))

