(define-module (crates-io as yn async-oneshot) #:use-module (crates-io))

(define-public crate-async-oneshot-0.1.0 (c (n "async-oneshot") (v "0.1.0") (h "0dm4cqav7vi8a4k0nq7c39lrchkkshvr9sniqa6ccrlxzyl88kmc")))

(define-public crate-async-oneshot-0.2.0 (c (n "async-oneshot") (v "0.2.0") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 0)) (d (n "ping-pong-cell") (r "^0.1") (d #t) (k 0)))) (h "0kwnpfcb9833l067ag5cnyjippi7nqimr4m3qxac176s9jgpp5yh") (y #t)))

(define-public crate-async-oneshot-0.3.0 (c (n "async-oneshot") (v "0.3.0") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)) (d (n "futures-micro") (r "^0.1.1") (d #t) (k 0)))) (h "1sass141ywwzgmxw6qaihdcjf8mj45pd2ik67m4qrc1k3rgnknxq") (y #t)))

(define-public crate-async-oneshot-0.3.1 (c (n "async-oneshot") (v "0.3.1") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)) (d (n "futures-micro") (r "^0.2") (d #t) (k 0)))) (h "1cwl6d5d376m2dr74v0xxc0aybbp0ch5mzfbs71xi5jvq0395zgc") (y #t)))

(define-public crate-async-oneshot-0.3.2 (c (n "async-oneshot") (v "0.3.2") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)) (d (n "futures-micro") (r "^0.2") (d #t) (k 0)))) (h "0vzzhj9qim9k1hizqjz6mz2p7mqn2k68s578hihq7814wsrn1c7c") (y #t)))

(define-public crate-async-oneshot-0.3.3 (c (n "async-oneshot") (v "0.3.3") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)) (d (n "futures-micro") (r "^0.3.1") (d #t) (k 0)))) (h "01jb3124kr69gamh2g4gm531akms9jmxl4z32mlib06z6xz2qsgs") (y #t)))

(define-public crate-async-oneshot-0.4.0 (c (n "async-oneshot") (v "0.4.0") (d (list (d (n "futures-lite") (r "^0.1") (d #t) (k 2)) (d (n "futures-micro") (r "^0.3.1") (d #t) (k 0)))) (h "154q4gi0ylmfyzzgnl8cjbrsi0g7iw8kpghd39x6hmjyxh76hxmr") (y #t)))

(define-public crate-async-oneshot-0.4.1 (c (n "async-oneshot") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("real_blackbox"))) (d #t) (k 2)) (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "futures-micro") (r "^0.3.1") (d #t) (k 0)))) (h "15zd43xfdvjnxxhd2xq1vdq0s9w2hx1x7p315i2d0l495mqwgd5q") (y #t)))

(define-public crate-async-oneshot-0.4.2 (c (n "async-oneshot") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("real_blackbox"))) (d #t) (k 2)) (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "futures-micro") (r "^0.3.1") (d #t) (k 0)) (d (n "waker-fn") (r "^1") (d #t) (k 2)))) (h "0129w0371d4ixxwqqbr07s74vx3n77xngplim4q8r4pzpc67gx2h")))

(define-public crate-async-oneshot-0.5.9 (c (n "async-oneshot") (v "0.5.9") (d (list (d (n "criterion") (r "^0.3") (f (quote ("real_blackbox"))) (d #t) (k 2)) (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "futures-micro") (r "^0.5.0") (d #t) (k 0)) (d (n "waker-fn") (r "^1") (d #t) (k 2)))) (h "0qslzzfz3j0fb4lvsmq5nx6lkjfbdq5sjmsl7xgj0hym08mdwixf")))

(define-public crate-async-oneshot-0.5.0 (c (n "async-oneshot") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("real_blackbox"))) (d #t) (k 2)) (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "futures-micro") (r "^0.5.0") (d #t) (k 0)) (d (n "waker-fn") (r "^1") (d #t) (k 2)))) (h "0b1f0kvp2cfm53hh4y62w2j5qh0psqpnjc3z2zlkj0fbrddwgiqy")))

