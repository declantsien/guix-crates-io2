(define-module (crates-io as yn async-once-watch) #:use-module (crates-io))

(define-public crate-async-once-watch-0.1.0 (c (n "async-once-watch") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "event-listener") (r "^2.5.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04rqrsglimjnnc90n8d0chx0d7vgmhyab6d8m5wv8vlnwm8yx12h") (y #t)))

(define-public crate-async-once-watch-0.1.1 (c (n "async-once-watch") (v "0.1.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "1ydr2ydgbg0jaaiq1a936klzjlm8v57gcmw460d2y6qggskr5ynx")))

