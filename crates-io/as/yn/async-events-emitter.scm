(define-module (crates-io as yn async-events-emitter) #:use-module (crates-io))

(define-public crate-async-events-emitter-0.1.0 (c (n "async-events-emitter") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h1xvq91l7nm1xpfncmg0jjbfm1zp8i9hm932pmbb8lyl497mz1g")))

