(define-module (crates-io as yn async-di) #:use-module (crates-io))

(define-public crate-async-di-0.1.0 (c (n "async-di") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0xmf6g8pmca54wki54n2f5dzyb3pizggmka0qxli6z7a8klf6xi0")))

(define-public crate-async-di-0.2.0 (c (n "async-di") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1259pg41r9sba4fchcv6kqb0n5gd9fs75p8m84h2wdl6ssyvh9rk")))

