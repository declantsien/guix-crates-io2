(define-module (crates-io as yn async_object) #:use-module (crates-io))

(define-public crate-async_object-0.1.0 (c (n "async_object") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.17") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "1jgd5vhkq68y0yrg2xhpfdbq6gamjqyj40bbc4cr1alc4q9v84z2")))

(define-public crate-async_object-0.1.1 (c (n "async_object") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.17") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "0sldkdjlizk032lsd7hbypvnijgzimrgkh5h6m311a98fcdin00l")))

(define-public crate-async_object-0.1.2 (c (n "async_object") (v "0.1.2") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (f (quote ("thread-pool"))) (d #t) (k 0)))) (h "1awym9zbl878rkin540qv29rm38cznlj0zazbpgf2z3n1y1s1dws")))

