(define-module (crates-io as yn asyncified) #:use-module (crates-io))

(define-public crate-asyncified-0.1.0 (c (n "asyncified") (v "0.1.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0pgx0n7p1f7il9735dr6bpccwgqppwmd4nc9vfw1w4nchv1b66sd")))

(define-public crate-asyncified-0.2.0 (c (n "asyncified") (v "0.2.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0z9mgdv6r1g90bkwrn8vav8vfqm8zcg3xazhcsr0641fx4ybnb1l")))

(define-public crate-asyncified-0.3.0 (c (n "asyncified") (v "0.3.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0kiryd7qazslyv07glhy2qiwcbbgnr40jmprjasnakfjd8x69kji")))

(define-public crate-asyncified-0.3.1 (c (n "asyncified") (v "0.3.1") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1v6zc91a5cdzs97liv9s89c0hcynlfpchbj3h0sf31hq6ghwwfbk")))

(define-public crate-asyncified-0.4.0 (c (n "asyncified") (v "0.4.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1gk2myrr3z8fp7x5p937lw8gmx62zbc447nb0p1ndagxgjkmg7wn")))

(define-public crate-asyncified-0.5.0 (c (n "asyncified") (v "0.5.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "10w4cdj4lbffpfyzdz471291yrsic2v9mxys50gjabnj126hr6yc")))

(define-public crate-asyncified-0.6.0 (c (n "asyncified") (v "0.6.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros" "sync"))) (d #t) (k 2)))) (h "0rmlmv9ii02k646pvycklf7abwlkw08gm4sfgcnl6x5hzaxqv6m2")))

(define-public crate-asyncified-0.6.1 (c (n "asyncified") (v "0.6.1") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros" "sync"))) (d #t) (k 2)))) (h "112xpws9b33mq366dj5qdgjvmdwd71q7iajhrsagp4k7x07wwpk8")))

(define-public crate-asyncified-0.6.2 (c (n "asyncified") (v "0.6.2") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros" "sync"))) (d #t) (k 2)))) (h "1bncns9168firr5sdm7cy4q9kj3qqj12saqsg5z8ncz74k2z7g6x")))

