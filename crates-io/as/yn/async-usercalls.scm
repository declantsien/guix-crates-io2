(define-module (crates-io as yn async-usercalls) #:use-module (crates-io))

(define-public crate-async-usercalls-0.5.0 (c (n "async-usercalls") (v "0.5.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "fortanix-sgx-abi") (r "^0.5.0") (d #t) (k 0)) (d (n "ipc-queue") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0shkqk10ds930yfxdhqqia94vccz4wm83wdp1yzc0lsj7x5qh45i")))

