(define-module (crates-io as yn async_ach-spsc) #:use-module (crates-io))

(define-public crate-async_ach-spsc-0.1.0 (c (n "async_ach-spsc") (v "0.1.0") (d (list (d (n "ach-spsc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)))) (h "173yw0rwccxs6s9l32gzyqisfg78i2wvi8mzv11mid7cwzbc6i2v")))

(define-public crate-async_ach-spsc-0.1.1 (c (n "async_ach-spsc") (v "0.1.1") (d (list (d (n "ach-spsc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "1xfk7kb2f9a84b9hxpzl6pjz9lhzb6pw55v8hhx59fqpw6v7swjx")))

(define-public crate-async_ach-spsc-0.1.2 (c (n "async_ach-spsc") (v "0.1.2") (d (list (d (n "ach-spsc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "07y60w8r6z4biczx8i0ky8729a7av45r8cihcwjknqb2kyjc5glc")))

(define-public crate-async_ach-spsc-0.1.3 (c (n "async_ach-spsc") (v "0.1.3") (d (list (d (n "ach-spsc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "01liz9bfkq33vjag3bajd9yl4ikq4kbmnkqfk0158s4ywsfiw4w8") (f (quote (("default") ("alloc"))))))

(define-public crate-async_ach-spsc-0.1.4 (c (n "async_ach-spsc") (v "0.1.4") (d (list (d (n "ach-spsc") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "0hr1k6bc2cpysxknq9k81kbgfwv092fbhbqqq3ydgm0vvsp1hg95") (f (quote (("default") ("alloc"))))))

(define-public crate-async_ach-spsc-0.2.0 (c (n "async_ach-spsc") (v "0.2.0") (d (list (d (n "ach-spsc") (r "^0.2") (d #t) (k 0)) (d (n "async_ach-notify") (r "^0.1") (d #t) (k 0)) (d (n "futures-executor") (r "^0") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-test") (r "^0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (k 0)))) (h "1rmr3xs32k02gb2s9v6lb4fvinqpanxhc3994zw6l936zj6jnp3c") (f (quote (("default") ("alloc"))))))

