(define-module (crates-io as yn async-utp) #:use-module (crates-io))

(define-public crate-async-utp-0.8.0-alpha1 (c (n "async-utp") (v "0.8.0-alpha1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "macros" "rt" "io-util" "sync" "net"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("fmt"))) (d #t) (k 2)))) (h "14p2dm9yh572j7wq9zyxy1zrv69dw58wcwhlw0vyjyjg5lh64mym") (f (quote (("unstable"))))))

