(define-module (crates-io as yn async-sse-loader) #:use-module (crates-io))

(define-public crate-async-sse-loader-0.1.0 (c (n "async-sse-loader") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 0)) (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (f (quote ("stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iw33n9bqfhm5mwfps96sj3b2r6qh77cgmj2ahm8jccq0hcxd8cl")))

(define-public crate-async-sse-loader-0.1.1 (c (n "async-sse-loader") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.7") (f (quote ("stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0apqyzrgp3wdabymh49r6i9ffwy6xviaqhdwav5x1wm7wivx8a27")))

