(define-module (crates-io as yn async_event_streams_derive) #:use-module (crates-io))

(define-public crate-async_event_streams_derive-0.1.0 (c (n "async_event_streams_derive") (v "0.1.0") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.56") (d #t) (k 2)) (d (n "async_event_streams") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qf46jh28l6n70fy4wkvx7jgq7zb5ww95yhj84rm4h0pk75fb1fh")))

