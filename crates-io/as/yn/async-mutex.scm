(define-module (crates-io as yn async-mutex) #:use-module (crates-io))

(define-public crate-async-mutex-0.1.0 (c (n "async-mutex") (v "0.1.0") (h "1f1dwbwbq4044j8w27r6x14w91x1cr4zxkbh02827wskb0yh6jyh")))

(define-public crate-async-mutex-1.0.0 (c (n "async-mutex") (v "1.0.0") (d (list (d (n "event-listener") (r "^1.0.0") (d #t) (k 0)) (d (n "smol") (r "^0.1.6") (d #t) (k 2)))) (h "01i6x7wgjsn2ys1jkv34jnbn208k3bf9srwvx3vahlpjnlirsvk6")))

(define-public crate-async-mutex-1.0.1 (c (n "async-mutex") (v "1.0.1") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 2)) (d (n "event-listener") (r "^1.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.3.1") (d #t) (k 2)) (d (n "smol") (r "^0.1.6") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("sync"))) (d #t) (k 2)))) (h "0h2d0a294mkxvl312xpqfl3a6jbziwfss0acxsz11fp746l8s5gj")))

(define-public crate-async-mutex-1.1.0 (c (n "async-mutex") (v "1.1.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.3.1") (d #t) (k 2)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("sync" "parking_lot"))) (d #t) (k 2)))) (h "0g9s4ifxjzj4bq9ny69gvqlmflwja723zx6sqap6gmlyfkxrb947")))

(define-public crate-async-mutex-1.1.1 (c (n "async-mutex") (v "1.1.1") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.3.1") (d #t) (k 2)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("sync" "parking_lot"))) (d #t) (k 2)))) (h "1q07lqkmlfq50yxhf0n8vhz4qpr59z5m5c0d7d7vn3xfnh5y2kpp")))

(define-public crate-async-mutex-1.1.2 (c (n "async-mutex") (v "1.1.2") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.3.1") (d #t) (k 2)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("sync" "parking_lot"))) (d #t) (k 2)))) (h "0x6zr82qrwzq626f5avibp5nyjl0ah8g1vqrwxa92a4awnbgkg6d")))

(define-public crate-async-mutex-1.1.3 (c (n "async-mutex") (v "1.1.3") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 2)) (d (n "event-listener") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.3.1") (d #t) (k 2)) (d (n "smol") (r "^0.1.10") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("sync" "parking_lot"))) (d #t) (k 2)))) (h "15p1vwc32pz1jaa5rqg6g6m0610v6qbnxpb9lhqnf15w1fgwx1bn")))

(define-public crate-async-mutex-1.1.4 (c (n "async-mutex") (v "1.1.4") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "event-listener") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.3.1") (d #t) (k 2)) (d (n "smol") (r "^0.1.18") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("sync" "parking_lot"))) (d #t) (k 2)))) (h "1xhc4999b08ylb4y24ynk1p97l5yq4sbzs9gn1sz1j85i7pzycjv")))

(define-public crate-async-mutex-1.1.5 (c (n "async-mutex") (v "1.1.5") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "event-listener") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.3.1") (d #t) (k 2)) (d (n "smol") (r "^0.1.18") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("sync" "parking_lot"))) (d #t) (k 2)))) (h "1agklil5pi2sigghd2jkvhzn6llp30cr4b7wygglrs1lzj0mks10")))

(define-public crate-async-mutex-1.2.0 (c (n "async-mutex") (v "1.2.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "event-listener") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)) (d (n "smol") (r "^0.1.18") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("sync" "parking_lot"))) (d #t) (k 2)))) (h "1nzs3l7i1pz1xrykxajibk40f47ff2lp2figbkbx1002y76f2p86")))

(define-public crate-async-mutex-1.3.0 (c (n "async-mutex") (v "1.3.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "event-listener") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)) (d (n "smol") (r "^0.1.18") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("sync" "parking_lot"))) (d #t) (k 2)))) (h "0cj68cijldn73fz5bfgk4gb8i6y6hs7xxzg59hg3byn4fwjir536")))

(define-public crate-async-mutex-1.4.0 (c (n "async-mutex") (v "1.4.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 2)) (d (n "event-listener") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)) (d (n "smol") (r "^0.1.18") (d #t) (k 2)) (d (n "tokio") (r "^0.2.21") (f (quote ("sync" "parking_lot"))) (d #t) (k 2)))) (h "0vhmsscqx48dmxw0yir6az0pbwcq6qjvcv2f43vdpn95vd9bi7a7")))

