(define-module (crates-io as yn async-test-derive) #:use-module (crates-io))

(define-public crate-async-test-derive-0.1.0 (c (n "async-test-derive") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "05jxp85dd7m8z1gys3vpcabyi8xadn9xzq56bfqblm5kq2w35vr2")))

(define-public crate-async-test-derive-0.1.1 (c (n "async-test-derive") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0hzwqspdrpwki4y5s04wyzz0ll8c13nwdy8mk7a8l99dmwhcmskr")))

(define-public crate-async-test-derive-1.0.0 (c (n "async-test-derive") (v "1.0.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1hps9mq373jimsl0bapcr2vcggpijnbf4g9sj9rmkr6m67m2z0rv")))

