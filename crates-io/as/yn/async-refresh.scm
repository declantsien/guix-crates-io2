(define-module (crates-io as yn async-refresh) #:use-module (crates-io))

(define-public crate-async-refresh-0.1.0 (c (n "async-refresh") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0gpinyqra3ifpmgn01l57j2zp63j14s5hq08dx31d7a622qd0606")))

