(define-module (crates-io as yn async-ssh2) #:use-module (crates-io))

(define-public crate-async-ssh2-0.1.0-beta (c (n "async-ssh2") (v "0.1.0-beta") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "piper") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 0)) (d (n "ssh2") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver" "io-util" "macros" "rt-core"))) (d #t) (k 2)))) (h "0vzf4a2ajzfzqvbnrp1b4cmyl8mwa50yc5gyj4dp7fncw5x4ip8b") (f (quote (("vendored-openssl" "ssh2/vendored-openssl"))))))

(define-public crate-async-ssh2-0.1.1-beta (c (n "async-ssh2") (v "0.1.1-beta") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "piper") (r "^0.1") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 0)) (d (n "ssh2") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver" "io-util" "macros" "rt-core"))) (d #t) (k 2)))) (h "0mb76q200wa1lyb8yb240ag34dhvg95yi9wzj2wyl05f06xswm2z") (f (quote (("vendored-openssl" "ssh2/vendored-openssl"))))))

(define-public crate-async-ssh2-0.1.2-beta (c (n "async-ssh2") (v "0.1.2-beta") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "smol") (r "^0.1") (d #t) (k 0)) (d (n "ssh2") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver" "io-util" "macros" "rt-core"))) (d #t) (k 2)))) (h "1qzkacgcqiix6kg400lz8czhsx2dr0hr981jsb7b4xzmwxd8p2s9") (f (quote (("vendored-openssl" "ssh2/vendored-openssl"))))))

