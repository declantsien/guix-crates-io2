(define-module (crates-io as yn async-coap-tokio) #:use-module (crates-io))

(define-public crate-async-coap-tokio-0.1.0 (c (n "async-coap-tokio") (v "0.1.0") (d (list (d (n "async-coap") (r "^0.1") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.18") (f (quote ("async-await" "nightly"))) (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.4") (d #t) (k 2)) (d (n "tokio-net") (r "^0.2.0-alpha.4") (d #t) (k 0)))) (h "1lr1gbag83ygpvs3yn4xp06a1a4ayrs7j23rrjx4slnxwziihbvb")))

