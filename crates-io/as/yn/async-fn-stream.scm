(define-module (crates-io as yn async-fn-stream) #:use-module (crates-io))

(define-public crate-async-fn-stream-0.1.0 (c (n "async-fn-stream") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0abvawrgb5ag85liyc1ky2bnry3dz3x51pfpzn87fb2f5x4d9nbq")))

(define-public crate-async-fn-stream-0.2.0 (c (n "async-fn-stream") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1rvb05znanafqsp0svkfc3p2ssq6km4bx8sxrj37d37vdis3jpqx")))

(define-public crate-async-fn-stream-0.2.1 (c (n "async-fn-stream") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "futures-executor") (r "^0.3") (f (quote ("std" "thread-pool"))) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "12gsk392p05d3dp23l0yqvfsv0nbc5ddqrk84v94jqhb7dcwcx4d")))

(define-public crate-async-fn-stream-0.2.2 (c (n "async-fn-stream") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "futures-executor") (r "^0.3") (f (quote ("std" "thread-pool"))) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "16y2jx5abip237n8drn37dszw3m1029wbgjrq9lcf5hh5x2125z7")))

