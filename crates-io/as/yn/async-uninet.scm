(define-module (crates-io as yn async-uninet) #:use-module (crates-io))

(define-public crate-async-uninet-0.1.0 (c (n "async-uninet") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0m3v3kb7rw525f8rfqdhvz4vvl7fqjywsf7gb1hw5gn126vnpmz1")))

(define-public crate-async-uninet-0.1.1 (c (n "async-uninet") (v "0.1.1") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0r04xi8nx8d5w31ihscdsy56r5vvsmdmzqvkhq4lldpglvh9gfsv")))

(define-public crate-async-uninet-0.2.0 (c (n "async-uninet") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0c433qbmx8snx1bmapipfx9bdfrhlgm1kcm6ly9mxvjq8934zpjx")))

