(define-module (crates-io as yn async_static) #:use-module (crates-io))

(define-public crate-async_static-0.1.0 (c (n "async_static") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "15ar6yn42kv6v39vp162l9m2gzbif06388c0ycf8rfv2gsr86ni2") (y #t)))

(define-public crate-async_static-0.1.1 (c (n "async_static") (v "0.1.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0skqnj341891315hiqhw281cymagdn9zxrqcx35p2iflbj930vkm") (y #t)))

(define-public crate-async_static-0.1.2 (c (n "async_static") (v "0.1.2") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0fi2g3vh67kkv45j19h4yxmnm1dprlvh9b69lrnpd7hhyld9zhqz")))

(define-public crate-async_static-0.1.3 (c (n "async_static") (v "0.1.3") (d (list (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0mdw15lybwqdfmk194hzsw77mr5yl5q43wgiykdxx4ab6hjihlq7")))

