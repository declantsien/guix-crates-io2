(define-module (crates-io as yn async-socks) #:use-module (crates-io))

(define-public crate-async-socks-0.1.0 (c (n "async-socks") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("net" "tcp" "udp" "dns" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("net" "tcp" "udp" "dns" "io-util" "macros"))) (d #t) (k 2)))) (h "1b34311s435lbf0sx9mqxbyx3vv7hn10ndmb29qmfq0jlwd0g6zz") (y #t)))

(define-public crate-async-socks-0.1.1 (c (n "async-socks") (v "0.1.1") (h "1p31v9nayjd772fqv7c35g378bhzl6j7y3awvmnabk1wpv5sddxz") (y #t)))

(define-public crate-async-socks-0.1.2 (c (n "async-socks") (v "0.1.2") (h "0fmyk3y720nkn8q8lk6ij1694xkml50vj2r00wq5d33vl2f1iq2g")))

