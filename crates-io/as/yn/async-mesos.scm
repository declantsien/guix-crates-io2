(define-module (crates-io as yn async-mesos) #:use-module (crates-io))

(define-public crate-async-mesos-0.1.0 (c (n "async-mesos") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "protobuf") (r "^1.4") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 2)) (d (n "spectral") (r "^0.6") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "users") (r "^0.6") (d #t) (k 2)))) (h "0d2224116w4s30p131hzwcrx23xqydzaidgwyvyyl8zq1y00ynbb")))

