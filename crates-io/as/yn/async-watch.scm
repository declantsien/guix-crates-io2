(define-module (crates-io as yn async-watch) #:use-module (crates-io))

(define-public crate-async-watch-0.1.0 (c (n "async-watch") (v "0.1.0") (h "0w335431bcppwci3izr3g001ck6vpmyab34syn2amqfby08wanpg") (y #t)))

(define-public crate-async-watch-0.2.0 (c (n "async-watch") (v "0.2.0") (h "03sglnifcbbibqsvbn476r1iixcdwl2r7j8n1ram0dqgl0harz9g") (y #t)))

(define-public crate-async-watch-0.3.0 (c (n "async-watch") (v "0.3.0") (d (list (d (n "async-executor") (r "^0.1.2") (d #t) (k 2)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "0y1cyc4cv5fkn1pwfi9gybali7ksa64066i9q6wfrzmdbjz2anp7")))

(define-public crate-async-watch-0.3.1 (c (n "async-watch") (v "0.3.1") (d (list (d (n "async-executor") (r "^0.1.2") (d #t) (k 2)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "1cndrlj526djw3c02a5zisb09k6cknjms3mjxz06q33wwbsgly50")))

