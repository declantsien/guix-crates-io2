(define-module (crates-io as yn async-gpiod) #:use-module (crates-io))

(define-public crate-async-gpiod-0.3.0 (c (n "async-gpiod") (v "0.3.0") (d (list (d (n "async-fs") (r "^2") (d #t) (k 0)) (d (n "async-io") (r "^2") (d #t) (k 0)) (d (n "blocking") (r "^1") (d #t) (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 0)) (d (n "gpiod-core") (r "^0.3") (k 0)) (d (n "smol") (r "^2") (d #t) (k 2)) (d (n "smol-potat") (r "^1") (d #t) (k 2)))) (h "0l76ixhh8k4k9a9hi19739w59cmwrkicvv37qw5l0ih84qybgnhm") (f (quote (("v2" "gpiod-core/v2") ("serde" "gpiod-core/serde") ("default" "v2") ("clap" "gpiod-core/clap"))))))

