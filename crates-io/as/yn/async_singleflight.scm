(define-module (crates-io as yn async_singleflight) #:use-module (crates-io))

(define-public crate-async_singleflight-0.0.1 (c (n "async_singleflight") (v "0.0.1") (d (list (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0yj89hhfracjsrnmfhhc75pzzvy41zpmjl77hp3rwwdxjw7iysia")))

(define-public crate-async_singleflight-0.0.2 (c (n "async_singleflight") (v "0.0.2") (d (list (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wi7kyiv9rh89dyjxx5lp7dc92dfg1pzwd78rl0wk1s73a1cl683") (y #t)))

(define-public crate-async_singleflight-0.0.3 (c (n "async_singleflight") (v "0.0.3") (d (list (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "10zhxqq6dgg85ddaj0qazybr19m76y61c1bva9m10v9rj3cdzdjj")))

(define-public crate-async_singleflight-0.0.4 (c (n "async_singleflight") (v "0.0.4") (d (list (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "00wj2h3d12qj6050hk6imp080gkfj15h9ishlw13g4h7hd8npv8q")))

(define-public crate-async_singleflight-0.0.5 (c (n "async_singleflight") (v "0.0.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1gjxqrmgw0c012vdrjrs6rgw71x9nrfqh2hlwzrccz19v6j5p80c")))

(define-public crate-async_singleflight-0.0.6 (c (n "async_singleflight") (v "0.0.6") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1x8wc2dysffj5lqz2g7zq426mikipw3bdyy7h7pxik1wdb49v52a")))

(define-public crate-async_singleflight-0.1.0 (c (n "async_singleflight") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0x480bqn1bz46gl3jnq1mfj7rwkj57a3ndxkf1hzvpgc0a4g294i")))

(define-public crate-async_singleflight-0.2.0 (c (n "async_singleflight") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1m632zxsnawlxxwzvqi3v8m3nvqj3p46q5jvc9i60j0skkrsg0iv")))

(define-public crate-async_singleflight-0.3.0 (c (n "async_singleflight") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1c8y4y6dziq8labbby9cn2q46rmxpdiniavsnr5m2sw5vicnv6ps")))

(define-public crate-async_singleflight-0.4.0 (c (n "async_singleflight") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0qnvf8a7spw2014703xz622dc4g62845l4nffg300ix2jq5gd2dk")))

(define-public crate-async_singleflight-0.5.0 (c (n "async_singleflight") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "083kqxgg02q4c9x90d62yq0gwwbhzsv0nyh2r8mm90x56wqgs47g")))

(define-public crate-async_singleflight-0.5.1 (c (n "async_singleflight") (v "0.5.1") (d (list (d (n "async-recursion") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1bgm102k28qfxvaqkv9cl40fk1qg4d84igh5964765rg5zdg0c2l")))

(define-public crate-async_singleflight-0.5.2 (c (n "async_singleflight") (v "0.5.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0vdagr9gvnci05kw6mzn14hd20ndq7rcvj6pwd93ci5m16r3kcdb")))

