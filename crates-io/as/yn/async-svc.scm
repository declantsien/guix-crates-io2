(define-module (crates-io as yn async-svc) #:use-module (crates-io))

(define-public crate-async-svc-0.0.1 (c (n "async-svc") (v "0.0.1") (h "07nfkfgma5l46frqm0pbfh3zxcmwis0x01fbc4z3n3d3zmqr995m")))

(define-public crate-async-svc-0.0.2 (c (n "async-svc") (v "0.0.2") (d (list (d (n "futures-util") (r "^0.3.8") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("full"))) (d #t) (k 2)))) (h "1cwhw1c5nl68adxxg29zllf06hy6kdwn5jy8rrpnr1zb3k0gsakf")))

