(define-module (crates-io as yn async-gen-macros) #:use-module (crates-io))

(define-public crate-async-gen-macros-0.1.0 (c (n "async-gen-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "128r3zl67si1z0p0is0ym5cy7hgbhfsd962pc7lr462irds12njg")))

(define-public crate-async-gen-macros-0.2.0 (c (n "async-gen-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "11lihz68gxxvjvzdp8sljkc5anv66layqbryn1fgr6747wrrc3hh")))

(define-public crate-async-gen-macros-0.2.1 (c (n "async-gen-macros") (v "0.2.1") (h "0v666v7391ska49wdqhjqrhdpyvm0gdb5r3dkznjh4pyhp9m8f3s")))

(define-public crate-async-gen-macros-0.3.0 (c (n "async-gen-macros") (v "0.3.0") (h "08pirvnfi16npa9hbj2kd8qf9y4czidfbxm4zrkr8bp7phfp2yh1")))

