(define-module (crates-io as yn async_download) #:use-module (crates-io))

(define-public crate-async_download-0.1.0 (c (n "async_download") (v "0.1.0") (h "1mkqw49c2qx4wfw6f4y6n66higv7jjhj0m55kgl64vmgcizjd9fb") (y #t)))

(define-public crate-async_download-0.0.1 (c (n "async_download") (v "0.0.1") (h "1ynpdbgh8z3gwqdv4yxkdbjqik3cc6m56niiq33dnscysz98gw64") (y #t)))

