(define-module (crates-io as yn async_uws) #:use-module (crates-io))

(define-public crate-async_uws-0.0.1 (c (n "async_uws") (v "0.0.1") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.5") (f (quote ("native-access"))) (d #t) (k 0)))) (h "0diz2fp5n13xig44ccr86gyy445qzg9mf8pfzwphnfhs3mqvpw3p")))

(define-public crate-async_uws-0.0.2 (c (n "async_uws") (v "0.0.2") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.6") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1xpvkfz7mr47mcfm6s14wwb411y7cf0g231jzx2a5kfaf53av412")))

(define-public crate-async_uws-0.0.3 (c (n "async_uws") (v "0.0.3") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.6") (f (quote ("native-access"))) (d #t) (k 0)))) (h "12yi3x8yd70jjrca53fhkn490cjl5inbsghkjlif7v4w4fibjqrl")))

(define-public crate-async_uws-0.0.4 (c (n "async_uws") (v "0.0.4") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.6") (f (quote ("native-access"))) (d #t) (k 0)))) (h "0cjm1dxk5dr4zgdsykzp9dc0mzw53sl782w4a8vjcgbyf84fncay")))

(define-public crate-async_uws-0.0.5 (c (n "async_uws") (v "0.0.5") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.6") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1j89wx3zs9wbg18yrfk7hmajsbqfhvc6iziiff96yiyfk9acrm2x")))

(define-public crate-async_uws-0.0.6 (c (n "async_uws") (v "0.0.6") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.6") (f (quote ("native-access"))) (d #t) (k 0)))) (h "08h7sl2c2sxnjkqcsv9z22mxh41fhs2ls0aqbkgniynpb21icy1b")))

(define-public crate-async_uws-0.0.7 (c (n "async_uws") (v "0.0.7") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.6") (f (quote ("native-access"))) (d #t) (k 0)))) (h "05f5sxrw5j982nm3xlqm0h4mqk53q9jirnisimsp1l4dp4sd6isp")))

(define-public crate-async_uws-0.0.8 (c (n "async_uws") (v "0.0.8") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.7") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1sminzpx23kkyxxvafj9n56kz5i447vzdpm4xqm9cc20lqx30axl")))

(define-public crate-async_uws-0.0.9 (c (n "async_uws") (v "0.0.9") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.7") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1m1wp8r94gv622x0wxag2ay98cdf4xc7avcb3lrmgc0915n1ny39")))

(define-public crate-async_uws-0.0.10 (c (n "async_uws") (v "0.0.10") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.7") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1sf24v65kbz626vixfqk1d60jq6xwzs0vv2fy2mpa9zqblhicsxb")))

(define-public crate-async_uws-0.0.11 (c (n "async_uws") (v "0.0.11") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.7") (f (quote ("native-access"))) (d #t) (k 0)))) (h "0rkpj2jr0qz5jibpcnvg453g5h7fj22x09k3z3vq67q94cb0f300")))

(define-public crate-async_uws-0.0.12 (c (n "async_uws") (v "0.0.12") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.7") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1p98qlqai70r4g54d5g0yszw5g24larhzbping9ilbp775chg3j8")))

(define-public crate-async_uws-0.0.13 (c (n "async_uws") (v "0.0.13") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.8") (f (quote ("native-access"))) (d #t) (k 0)))) (h "0kc75453a42zvasrvl4p6k70hdl1vk17pnrpsyd84jv86iabsz1h")))

(define-public crate-async_uws-0.0.14 (c (n "async_uws") (v "0.0.14") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.8") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1l1cnbigv0mhhpi57w1pxh3fpkljw9g4msabjkfypndni83fpzks")))

(define-public crate-async_uws-0.0.15 (c (n "async_uws") (v "0.0.15") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.9") (f (quote ("native-access"))) (d #t) (k 0)))) (h "18aif3h27jm5yb793j0jsv3gs772k1h1i4h8saf31qw66v6hrz9z")))

(define-public crate-async_uws-0.0.16 (c (n "async_uws") (v "0.0.16") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.9") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1liqlxfv8xqy5f6mzrsnjx8sxgs8kprifnazak9nn3rp5w0f5bbd")))

(define-public crate-async_uws-0.0.17 (c (n "async_uws") (v "0.0.17") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.9") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1p8as2bls2dr687smvh8dmvynayki2pg6m7mjpp7f0cdjvl0s4ff")))

(define-public crate-async_uws-0.0.18 (c (n "async_uws") (v "0.0.18") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.9") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1lr96anscks5rdg3vm6vcqldw2wgc7xsrzsv4zs0wi2y3qlss7s8")))

(define-public crate-async_uws-0.0.19 (c (n "async_uws") (v "0.0.19") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.10") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1vknh2h2668pdjz4vzi92a5iiwlbzk15n6b2qkmx2hmw470a7kl4")))

(define-public crate-async_uws-0.0.20 (c (n "async_uws") (v "0.0.20") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.10") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1iib4w7yfx9g4fsi7sx6p9jc12hfg993l95g3r5ghqxq34r26dl8")))

(define-public crate-async_uws-0.0.21 (c (n "async_uws") (v "0.0.21") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.10") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1f2rqc1yn8k6l8sj7842lynlvc9xp2g2zc02wsds9bfdypky7sc9")))

(define-public crate-async_uws-0.0.22 (c (n "async_uws") (v "0.0.22") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.11") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1nxd036hw0zwbriy6rh138xmx52xlipjqgfzbxw3kjkjxw3m0yfz")))

(define-public crate-async_uws-0.0.23 (c (n "async_uws") (v "0.0.23") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.11") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1xf8rjrajzmm7qivqdq42l1x3lk9bvn646488m97yjxmgcz3w0h0")))

(define-public crate-async_uws-0.0.24 (c (n "async_uws") (v "0.0.24") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.11") (f (quote ("native-access"))) (d #t) (k 0)))) (h "0qqnxdgchvh34l66pwwirp2h1j99j49kwdcqp1mwr7wrxdqh13qg")))

(define-public crate-async_uws-0.0.25 (c (n "async_uws") (v "0.0.25") (d (list (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.11") (f (quote ("native-access"))) (d #t) (k 0)))) (h "03408217wqqhsbqynjniq7in7s3rswclq98cx1jazs26c7milz2m")))

(define-public crate-async_uws-0.0.26 (c (n "async_uws") (v "0.0.26") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uwebsockets_rs") (r "^0.0.11") (f (quote ("native-access"))) (d #t) (k 0)))) (h "1g6y10m0zrrvv9gflwdfq59axwaxpbl0csdaw2b7zilg6k4rpf3m")))

