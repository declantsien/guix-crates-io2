(define-module (crates-io as yn async-log-attributes) #:use-module (crates-io))

(define-public crate-async-log-attributes-1.0.0 (c (n "async-log-attributes") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4.29") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0x72pfz7x18ylrg4219yarv4wi5i6w23xy1rhynp6kz3sb900r1g")))

(define-public crate-async-log-attributes-1.0.1 (c (n "async-log-attributes") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^0.4.29") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0b9nysb5yxf772cinl5rsyhl2zazj2qfhbckv1kjz9qr3gkgi5ys")))

