(define-module (crates-io as yn async-object-pool) #:use-module (crates-io))

(define-public crate-async-object-pool-0.1.0 (c (n "async-object-pool") (v "0.1.0") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "038q1fdnjn7483hy15x72d9mhyjdax5h1d4b6ca5jmhd16sif131")))

(define-public crate-async-object-pool-0.1.1-alpha.0 (c (n "async-object-pool") (v "0.1.1-alpha.0") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1i0ik36f4qdbbnzyvin4xpnhgy1hi1lzp48rjm72gaqn9433bpgq")))

(define-public crate-async-object-pool-0.1.1-alpha.1 (c (n "async-object-pool") (v "0.1.1-alpha.1") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "18jg0yhq4y4d3j118frj1bq6ss17gx428zkcl1dbyyaxm9g6h53g")))

(define-public crate-async-object-pool-0.1.1 (c (n "async-object-pool") (v "0.1.1") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("unstable"))) (d #t) (k 0)))) (h "051sg4sv1gkiql8vmaw48ph4mp8xc84kycb9n65wp3q0gdn9hbdn")))

(define-public crate-async-object-pool-0.1.2 (c (n "async-object-pool") (v "0.1.2") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)))) (h "0f0xlh57p2j60acxhsmi2jyh4h5wlzz67zr9ahwbxxq9wn8rw95k")))

(define-public crate-async-object-pool-0.1.3 (c (n "async-object-pool") (v "0.1.3") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)))) (h "0nn5ki91kkjr6kg5x4gzjj14bsy92gg18bgjdy5c5pwnapadx26s")))

(define-public crate-async-object-pool-0.1.4 (c (n "async-object-pool") (v "0.1.4") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.8.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)))) (h "1g7kr9b3030rfmfn8lyqb5jw2hlmpayvynrr8smw8bxw1v1h3fdf")))

