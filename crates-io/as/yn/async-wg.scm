(define-module (crates-io as yn async-wg) #:use-module (crates-io))

(define-public crate-async-wg-0.1.0 (c (n "async-wg") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("full"))) (d #t) (k 2)))) (h "1151gwga3sa8c9iy1qhnwv7x3wp5r3gvx723p2kjnwlnsz98nycz") (y #t)))

(define-public crate-async-wg-0.1.1 (c (n "async-wg") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("full"))) (d #t) (k 2)))) (h "0k82cwr6si7wlby5hjlwhn7wkyxdj09ciirpp9cxg2ip49hqidvz") (y #t)))

(define-public crate-async-wg-0.1.2 (c (n "async-wg") (v "0.1.2") (d (list (d (n "futures-timer") (r "^3.0.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("full"))) (d #t) (k 2)))) (h "005a4sp8jpiv96g7fcfq19ldvlwkq294c9jjv4s755wqgaj98s1v") (y #t)))

