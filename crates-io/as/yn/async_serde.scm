(define-module (crates-io as yn async_serde) #:use-module (crates-io))

(define-public crate-async_serde-0.1.0 (c (n "async_serde") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1kyv7nb69la1x1irrmshg3f4j9iphrgqqkvbssq5cqim02fvglav")))

