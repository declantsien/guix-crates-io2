(define-module (crates-io as yn async-filedb) #:use-module (crates-io))

(define-public crate-async-filedb-0.1.0 (c (n "async-filedb") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1skkl8fswscpi9hlv8i5b6hlclr9l11i58zgrx81pp9gz2dxjals")))

(define-public crate-async-filedb-0.1.1 (c (n "async-filedb") (v "0.1.1") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0s7f066g65i490km593761h6wm0xxdvmn94j2klgv684199smfij")))

(define-public crate-async-filedb-0.1.2 (c (n "async-filedb") (v "0.1.2") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1i5ck0kcfid9irh7x2q54hhyviaa31xqpqdl4mgpmlxilyvcrhjp")))

(define-public crate-async-filedb-0.1.3 (c (n "async-filedb") (v "0.1.3") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vrznp56bhds8822v1wfsxschknizk35lb4pv4hv5pjvbza44zbp")))

(define-public crate-async-filedb-0.2.0 (c (n "async-filedb") (v "0.2.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zhfnwldf67iancv0m9x63w0bxjl8b7ilzv2lxj25hnb5jfghlia")))

(define-public crate-async-filedb-0.2.1 (c (n "async-filedb") (v "0.2.1") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b0k1nzdmdf2sz48a0vg9q6f7qjxx445ivz9dq9hc8mkdcjcl2rx")))

(define-public crate-async-filedb-0.2.2 (c (n "async-filedb") (v "0.2.2") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bwmsln7dzx7a6ppx472k3c8q9sigs1plpzx84z77379jasz7wl2") (f (quote (("auto_sync" "libc"))))))

(define-public crate-async-filedb-0.2.3 (c (n "async-filedb") (v "0.2.3") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1z1cnfx092k7jnqlbvs8zv7kz3rm4ydvh8qd3a0y3l6yafh6mkb4") (f (quote (("auto_sync"))))))

(define-public crate-async-filedb-0.2.4 (c (n "async-filedb") (v "0.2.4") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1id82sway3202hfh6xmjk395zx10xmmrs74zad6cv7kdif1gs18p") (f (quote (("auto_sync"))))))

(define-public crate-async-filedb-0.3.0 (c (n "async-filedb") (v "0.3.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kichj80awpfqnd7399pxl8dkmcklyracbp4ylabsanqsigd9h4b") (f (quote (("auto_sync"))))))

(define-public crate-async-filedb-0.3.2 (c (n "async-filedb") (v "0.3.2") (d (list (d (n "async-channel") (r "^2") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v40snmqpvmyrcxm62b2n5amh1rxzy14i930kjd61zixp5da0xy3") (f (quote (("auto_sync"))))))

(define-public crate-async-filedb-0.3.3 (c (n "async-filedb") (v "0.3.3") (d (list (d (n "async-channel") (r "^2") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16lg1aqklfban1198q834zcxsapxan2b94yzd4xgnd8fvr3jzcz9") (f (quote (("auto_sync"))))))

(define-public crate-async-filedb-0.3.4 (c (n "async-filedb") (v "0.3.4") (d (list (d (n "async-channel") (r "^2") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jjwfjx81iq3c7y9fs8rw3f6q1p2gnihqi8nhwpqdcj9mm7jly1b") (f (quote (("auto_sync"))))))

(define-public crate-async-filedb-0.3.5 (c (n "async-filedb") (v "0.3.5") (d (list (d (n "async-channel") (r "<2.2") (d #t) (k 0)) (d (n "async-kvdb") (r "^0") (k 0)) (d (n "async-memorydb") (r "^0") (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "112anbyp1gsw763q0fq2izx8p0czn2c0bcynp1kb59sxw0jcydn3") (f (quote (("auto_sync"))))))

