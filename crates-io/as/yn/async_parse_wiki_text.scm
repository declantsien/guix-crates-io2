(define-module (crates-io as yn async_parse_wiki_text) #:use-module (crates-io))

(define-public crate-async_parse_wiki_text-0.2.0 (c (n "async_parse_wiki_text") (v "0.2.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0cx98mfck45ca11fil4065rqdcszfbf1b2gdd4x53vvbikiwlwp7")))

(define-public crate-async_parse_wiki_text-0.2.1 (c (n "async_parse_wiki_text") (v "0.2.1") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1n9gdy41rmsnrcr3bicbpyij5gk7kdv1q095cl7dlgnhajbfcizb")))

