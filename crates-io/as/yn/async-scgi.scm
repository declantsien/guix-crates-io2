(define-module (crates-io as yn async-scgi) #:use-module (crates-io))

(define-public crate-async-scgi-0.1.0 (c (n "async-scgi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "futures-lite") (r "^1.12") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "smol") (r "^1.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "135395j5vz7rl5vqxxc7wb44acz699snpbyx3kwyn8d0hn0zqrwa")))

