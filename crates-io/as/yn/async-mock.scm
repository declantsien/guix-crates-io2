(define-module (crates-io as yn async-mock) #:use-module (crates-io))

(define-public crate-async-mock-0.1.0 (c (n "async-mock") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "12w1y3l2zhg0y8hcr4vjrzrnjwzq84dmqq8c71v0dlfcngafsi7i") (y #t)))

(define-public crate-async-mock-0.1.1 (c (n "async-mock") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0xms43m1cjn2pqdzrqwcf20wmylzpssfz9fa3g5yin9rhiigkspi") (y #t)))

(define-public crate-async-mock-0.1.2 (c (n "async-mock") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0m48igfbz2zqgqwx6zby58kdiw9hz1v64w9zjwgrxgav92xm4id7")))

(define-public crate-async-mock-0.1.3 (c (n "async-mock") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1i7ljd87g7pky8nn02xvg4fb39r9zndvbx8mis65811gqa1k0pq9")))

