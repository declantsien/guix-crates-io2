(define-module (crates-io as yn async-coap) #:use-module (crates-io))

(define-public crate-async-coap-0.1.0 (c (n "async-coap") (v "0.1.0") (d (list (d (n "async-coap-uri") (r "^0.1.0") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.18") (f (quote ("async-await" "nightly"))) (d #t) (k 0)) (d (n "futures-timer") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0s07ybpn9lxmsj5mxssg3jl8rysmjjgry3hqfikmj9q1zkqn315p") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

