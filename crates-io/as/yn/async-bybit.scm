(define-module (crates-io as yn async-bybit) #:use-module (crates-io))

(define-public crate-async-bybit-0.0.1 (c (n "async-bybit") (v "0.0.1") (d (list (d (n "async-std") (r "^1.10.0") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (d #t) (k 0)))) (h "129nnlzvz04pnlxm1accxajvl5k3hp9y2c4vfhgra9sszahqadcb")))

