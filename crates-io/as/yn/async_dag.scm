(define-module (crates-io as yn async_dag) #:use-module (crates-io))

(define-public crate-async_dag-0.1.0 (c (n "async_dag") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "1s6dwcdm8xswylzaby48c3a79f8796za8651mm9hd7p8h89s63rf")))

(define-public crate-async_dag-0.1.1 (c (n "async_dag") (v "0.1.1") (d (list (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1il022ck3j9ppqa5dhcy413pswyccmzmzijqiqk2vvicj24qhk8w") (y #t)))

(define-public crate-async_dag-0.1.2 (c (n "async_dag") (v "0.1.2") (d (list (d (n "daggy") (r "^0.8.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.0") (d #t) (k 0)))) (h "06hxrvygi6drp3d36rpwdqizljql0j02gh7cbk2sryskrabrr5pf")))

