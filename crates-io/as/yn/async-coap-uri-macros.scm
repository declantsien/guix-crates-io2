(define-module (crates-io as yn async-coap-uri-macros) #:use-module (crates-io))

(define-public crate-async-coap-uri-macros-0.1.0 (c (n "async-coap-uri-macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0j2lr843pa5fk5f95zyyz1rzjp5csjklqfxa2afdgj9dwpiaj2yy")))

