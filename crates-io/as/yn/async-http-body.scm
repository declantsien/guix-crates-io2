(define-module (crates-io as yn async-http-body) #:use-module (crates-io))

(define-public crate-async-http-body-0.1.5 (c (n "async-http-body") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "http-body") (r "^0.4.5") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "self-reference") (r "^0.1.5") (d #t) (k 0)))) (h "0vs56v6rrbvv8fjbm5kjz7m7zw1hgk026zsshai8v36rk13zbxzr")))

(define-public crate-async-http-body-0.1.6 (c (n "async-http-body") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "http-body") (r "^0.4.5") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "self-reference") (r "^0.1.5") (d #t) (k 0)))) (h "0q7yjc7ahhzkfqw1lif469lrwf2ypxg0ynnjzpf0lk5xlm9knc20")))

