(define-module (crates-io as yn async-git) #:use-module (crates-io))

(define-public crate-async-git-0.0.0-squat-name (c (n "async-git") (v "0.0.0-squat-name") (d (list (d (n "async-compression") (r "^0.3.2") (f (quote ("futures-bufread" "zlib"))) (k 0)) (d (n "futures") (r "^0.3.4") (f (quote ("io-compat"))) (k 0)) (d (n "sha-1") (r "^0.8.2") (k 0)) (d (n "structopt") (r "^0.3.13") (k 0)) (d (n "thiserror") (r "^1.0.15") (k 0)) (d (n "tokio") (r "^0.2.18") (f (quote ("fs" "io-util" "macros"))) (k 0)))) (h "0ldlfdp4dd7pnqnvh9ljrv0j4rfvrglh58b3y5q9jcqd5aclpfrz")))

