(define-module (crates-io as yn async-fuse) #:use-module (crates-io))

(define-public crate-async-fuse-0.3.0 (c (n "async-fuse") (v "0.3.0") (d (list (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0wcvmr5z0si0a3ihqrr3c3fm7dvs2bfiqminv5lsl1ld3flvr1i9")))

(define-public crate-async-fuse-0.3.1 (c (n "async-fuse") (v "0.3.1") (d (list (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "02h0sq0g0pnjn69mj7b45f26pkl899rfwpxh6rvq18mx3wh7rsks")))

(define-public crate-async-fuse-0.3.2 (c (n "async-fuse") (v "0.3.2") (d (list (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0nbrcvsm8c4z49hrd80l0504855ysxr867n6miqny1xbwp63qala")))

(define-public crate-async-fuse-0.3.3 (c (n "async-fuse") (v "0.3.3") (d (list (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "05h9h4mxz8hp3x8xpk1immaa4bxhmkcrc6h55za3r1w9f1hhmh56")))

(define-public crate-async-fuse-0.4.0 (c (n "async-fuse") (v "0.4.0") (d (list (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "05gw69kk2vsr7l0x8ggi0vv5s95lz0icc7yiwzb9x7y075d4pfnm")))

(define-public crate-async-fuse-0.5.0 (c (n "async-fuse") (v "0.5.0") (d (list (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1iga7qccph1aqwcdqi98f5851lzww6gnf69k81g1iv1cimm995dp") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.6.0 (c (n "async-fuse") (v "0.6.0") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0l7svf9a074bf1zbcza55ar4rjrc6ns00ajwqwnkwjv3ixnd4rxh") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.7.0 (c (n "async-fuse") (v "0.7.0") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1qj7y91h218y6lq4zyrq4ik39aws7ma8vl9w5r48l68f8lg1al2g") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.7.1 (c (n "async-fuse") (v "0.7.1") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1w98hvg63zmxa4if98xgy2ckgblgjbp80ccz3j82h7bs0ph7bdbz") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.7.2 (c (n "async-fuse") (v "0.7.2") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1ng0jjsbp6xqhxl3slvkc0khkm85n20dwqmzrimj67qx9k78krhh") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.7.3 (c (n "async-fuse") (v "0.7.3") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1v14dcw5n62vda0zb38c5bzy5ix5jzlgiddm846pxchd031g0f42") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.8.0 (c (n "async-fuse") (v "0.8.0") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "087j5m2l9gsq44k0f32xzzcj0kp1xh0sld686zy6w3wplp9pxhma") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.9.0 (c (n "async-fuse") (v "0.9.0") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "03i2wx5xqfvgfsn20fd317dvfmhbl5nxm5inwni7gb8zhd807ifr") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.10.0 (c (n "async-fuse") (v "0.10.0") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "10ys1j5cwvcq533r7pnlix7mlzmyddc1bqahbcg2bvcnwb619kvc") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.10.1 (c (n "async-fuse") (v "0.10.1") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "08qd1njsp5wvkcwizyjhhyc67zggnjizsc4hm5a8pd3bf9f4v4wa") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.11.0 (c (n "async-fuse") (v "0.11.0") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0al0wc7rslbaj9zzgijpqbv1ing38s75k5yjlibx4ym5i1zkz2sb") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.11.1 (c (n "async-fuse") (v "0.11.1") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0b23mzymmbfd4p02l7calqq7n7pb1k6wvlqk19hlf42ib5c69l25") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.11.2 (c (n "async-fuse") (v "0.11.2") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "04zmx2yp729im708ji92crf67abwvllpq9ym7kvz9j29vcdcrk9x") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.11.3 (c (n "async-fuse") (v "0.11.3") (d (list (d (n "async-stream") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1m39pqx1kzpis4wgm7ffkcwryd5r6l428qc3ncmpwcskmr2kk810") (f (quote (("stream" "futures-core"))))))

(define-public crate-async-fuse-0.11.4 (c (n "async-fuse") (v "0.11.4") (d (list (d (n "async-stream") (r "^0.3.4") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.27") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "10mxa9nv2yiq1r10g72j5w78z8j92fn3hy0nmqbv7nlxa0g3b1rb") (f (quote (("stream" "futures-core")))) (r "1.56")))

