(define-module (crates-io as yn async-embedded-traits) #:use-module (crates-io))

(define-public crate-async-embedded-traits-0.1.0 (c (n "async-embedded-traits") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0n3dcsdj7drmghchzlj07pwapn01yllwskr9wzn2hi2rhpjfv0rh")))

(define-public crate-async-embedded-traits-0.1.1 (c (n "async-embedded-traits") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0z113nz56x6knl5pc9mf842824v16ql1xkxpvyfiy0vaivwlrgg8")))

(define-public crate-async-embedded-traits-0.1.2 (c (n "async-embedded-traits") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0w8spj7xjbvnf1nzwaw5m2r3q5f7aywj2q8r1abyavbyffmicdbf")))

(define-public crate-async-embedded-traits-0.1.3 (c (n "async-embedded-traits") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1n40xmrw86na13mvp7sm5vcvh5amcba47sg5fk6qy3ghaaqpk5rj")))

