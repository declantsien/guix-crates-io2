(define-module (crates-io as yn async-hofs) #:use-module (crates-io))

(define-public crate-async-hofs-0.1.0 (c (n "async-hofs") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)))) (h "1q9g7653zbcm8x19x2hd1s761j4yb0acyw8p4zhph45hzar7lnmr")))

(define-public crate-async-hofs-0.1.1 (c (n "async-hofs") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)))) (h "1y5jdd2p0qwfvnrdx7zz2kqbmqv7l7n4crns5i0p5v25w5cv4f0w")))

