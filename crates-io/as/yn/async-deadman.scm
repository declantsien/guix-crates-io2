(define-module (crates-io as yn async-deadman) #:use-module (crates-io))

(define-public crate-async-deadman-1.0.0 (c (n "async-deadman") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "15ixmqgwxs5fb3i14ks4v5pmbb6fbi55c0qsw8jw3ncjx5s8wdvb")))

