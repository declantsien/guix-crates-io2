(define-module (crates-io as yn async-hwi-cli) #:use-module (crates-io))

(define-public crate-async-hwi-cli-0.0.13 (c (n "async-hwi-cli") (v "0.0.13") (d (list (d (n "async-hwi") (r "^0.0.13") (d #t) (k 0)) (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt" "rt-multi-thread" "io-util" "sync"))) (d #t) (k 0)))) (h "0dknj57nxi8jmlcx3fyalj4ppxrcwrf3kzfbxwmnix2qx4snah6j")))

(define-public crate-async-hwi-cli-0.0.14 (c (n "async-hwi-cli") (v "0.0.14") (d (list (d (n "async-hwi") (r "^0.0.14") (d #t) (k 0)) (d (n "bitcoin") (r "^0.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt" "rt-multi-thread" "io-util" "sync"))) (d #t) (k 0)))) (h "0fz39zdgfs8gaqxp3yrqfjjd1vjazy3mkbydnallwrzfz0s0z70j")))

