(define-module (crates-io as yn async-claude) #:use-module (crates-io))

(define-public crate-async-claude-0.0.1 (c (n "async-claude") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03vwpkp9s2ky5f6pd6xk0nsbdsx146kijbpyw7xwh69pjzah66px")))

(define-public crate-async-claude-0.0.2 (c (n "async-claude") (v "0.0.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0m4xgs6r52jdxp96803kv1zkg8sisq9rsfrlnwf1zi4dvkw239s5")))

(define-public crate-async-claude-0.0.3 (c (n "async-claude") (v "0.0.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0fq281g7vz91qs8zp3anjdcdj0jrjdr17m4frp4gmwr9vq29bf3w") (f (quote (("default"))))))

(define-public crate-async-claude-0.1.0 (c (n "async-claude") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ylig6pmi7c6sql10dij4fkjk4kgxkgz7xk79v4cqsvqwgrvcjbf") (f (quote (("default"))))))

(define-public crate-async-claude-0.2.0 (c (n "async-claude") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1iczkmswa6s6b13qsy8a76jcig49zys5jagfkjsikkr42by5ckkp") (f (quote (("default"))))))

(define-public crate-async-claude-0.3.0 (c (n "async-claude") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1g51063aq7zvbxd4f49bs0k34hfrsds2cn06b4s40jkpd8m2y1f5") (f (quote (("default"))))))

(define-public crate-async-claude-0.4.0 (c (n "async-claude") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "053l2s7rqig7b18ckriw6cj153mi5zv7275jmz62qsf1hjgacx61") (f (quote (("default"))))))

(define-public crate-async-claude-0.5.0 (c (n "async-claude") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1r16di6b6w4vazvkf0vqcmxb449jwpspaps85mr20wyd1i34qhpa") (f (quote (("price") ("default"))))))

(define-public crate-async-claude-0.6.0 (c (n "async-claude") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1a9f7q7l9vixmnbl6xrv6m4p1ppdpvj3sahba64990b20a1jm545") (f (quote (("price") ("default"))))))

(define-public crate-async-claude-0.7.0 (c (n "async-claude") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0y14px17q6wra9m5cw6x9vz9sp10lnr6zqvzykq8zgdiyfya8lph") (f (quote (("price") ("default"))))))

