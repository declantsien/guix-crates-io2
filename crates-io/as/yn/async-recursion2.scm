(define-module (crates-io as yn async-recursion2) #:use-module (crates-io))

(define-public crate-async-recursion2-1.0.5 (c (n "async-recursion2") (v "1.0.5") (d (list (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro" "clone-impls"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "09xhsfwmglirwpp63qcfnfjr2pga8kr60bbz83vp0d1qc6ci317j")))

