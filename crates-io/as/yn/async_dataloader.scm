(define-module (crates-io as yn async_dataloader) #:use-module (crates-io))

(define-public crate-async_dataloader-0.1.0 (c (n "async_dataloader") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (f (quote ("executor"))) (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0xry7rv8kan4rmvka62nhxkk1n0s74hp7cf4axj2ab5x324d0nkj")))

(define-public crate-async_dataloader-0.1.1 (c (n "async_dataloader") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (f (quote ("executor"))) (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0539bjmblzsbl24097mif001x7n5na5ynjhm26b57gk6gvxn0f60")))

(define-public crate-async_dataloader-0.1.2 (c (n "async_dataloader") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (f (quote ("executor"))) (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "18p164k9zqix5bpyhmyp79q9dj0a800gwgi18yilyfsd95gbsgf1")))

(define-public crate-async_dataloader-0.1.3 (c (n "async_dataloader") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (f (quote ("executor"))) (d #t) (k 2)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "00fcpy17aj395v6d39hn7ggjkz6zr1k1l4dbnd99d6pbzsigxadi")))

