(define-module (crates-io as yn async-http-client) #:use-module (crates-io))

(define-public crate-async-http-client-0.1.0 (c (n "async-http-client") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0027rhix7fzpq2m77p3qg4fygz7711fbymr3jmzcnk0nmp9la61l")))

(define-public crate-async-http-client-0.2.0 (c (n "async-http-client") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "0sbdpl4ns6a6kj70c0qmnn089b8l9i54d2vmc9j5mwgdwpmymp7h")))

