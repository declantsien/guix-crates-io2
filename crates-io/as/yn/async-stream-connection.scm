(define-module (crates-io as yn async-stream-connection) #:use-module (crates-io))

(define-public crate-async-stream-connection-1.0.0 (c (n "async-stream-connection") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "io-util"))) (d #t) (k 2)))) (h "13w7hdcf5x4h7fi23zsi33h4ny6rs8bzyrhsy2r39k9infw3ar79") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-async-stream-connection-1.0.1 (c (n "async-stream-connection") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "io-util"))) (d #t) (k 2)))) (h "1b3svp0hcbgd4ap4viwd9xjk13x86awdyjm812avi121jy762dh3") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

