(define-module (crates-io as yn asynchronous) #:use-module (crates-io))

(define-public crate-asynchronous-0.1.0 (c (n "asynchronous") (v "0.1.0") (h "0dsdc15kwa6am0hcs3fijqi25lmc2l0bvmj81gyd0z2h854q9180")))

(define-public crate-asynchronous-0.1.1 (c (n "asynchronous") (v "0.1.1") (h "1ay9k40iar3pir2a4zaha0766f8b3ir5vpampwshsjfw9adzcaka")))

(define-public crate-asynchronous-0.2.0 (c (n "asynchronous") (v "0.2.0") (h "0xf2j2ahg3cyp4yfcrir81wgggja303g4l48zh1phcx5636sfppv")))

(define-public crate-asynchronous-0.3.0 (c (n "asynchronous") (v "0.3.0") (d (list (d (n "num_cpus") (r "^0.2.4") (d #t) (k 0)))) (h "0zy2lgwd168kxv0si6782vambybfcrpbqaadb16viwckhgr35c1d")))

(define-public crate-asynchronous-0.3.1 (c (n "asynchronous") (v "0.3.1") (d (list (d (n "num_cpus") (r "^0.2.4") (d #t) (k 0)))) (h "1w84mpymvzmhamjg7f66w63b7ihh5qwrwg5aw50czclnqr7ig60l")))

(define-public crate-asynchronous-0.3.2 (c (n "asynchronous") (v "0.3.2") (d (list (d (n "num_cpus") (r "^0.2.4") (d #t) (k 0)))) (h "1j7awasn6iyvsvj7fqvn72py70357ykzszdkma8ifbf7pxvqmn96")))

(define-public crate-asynchronous-0.3.3 (c (n "asynchronous") (v "0.3.3") (d (list (d (n "num_cpus") (r "^0.2.4") (d #t) (k 0)))) (h "0brs74fzn62nvf66kcm90xaknjpbj0y30kbjghbqmmn1dbzp20xy")))

(define-public crate-asynchronous-0.4.0 (c (n "asynchronous") (v "0.4.0") (d (list (d (n "num_cpus") (r "^0.2.5") (d #t) (k 0)))) (h "03125zz23bixig9ws7fbcvfpw69h7z1w2pj79iv5ij39i2cchfff")))

(define-public crate-asynchronous-0.4.1 (c (n "asynchronous") (v "0.4.1") (d (list (d (n "num_cpus") (r "^0.2.5") (d #t) (k 0)))) (h "0jj6wks15f1gglxmgw5hcd4fq7gqr63k4lvz26k5z1fxq9h8lpg5")))

(define-public crate-asynchronous-0.4.2 (c (n "asynchronous") (v "0.4.2") (d (list (d (n "num_cpus") (r "^0.2.5") (d #t) (k 0)))) (h "0dlavfyhvg6nw21yd6x9vq8bkgs1hclghw6gimgbmha1677n0z0l")))

(define-public crate-asynchronous-0.4.3 (c (n "asynchronous") (v "0.4.3") (d (list (d (n "num_cpus") (r "^0.2.5") (d #t) (k 0)))) (h "0yqd1b0qpqwdvl99zz7gfzxa4ylnfxchacrl92jzc6gs032rd5in")))

(define-public crate-asynchronous-0.4.4 (c (n "asynchronous") (v "0.4.4") (d (list (d (n "ansi_term") (r "^0.5.2") (d #t) (k 2)) (d (n "docopt") (r "^0.6.64") (d #t) (k 2)) (d (n "num_cpus") (r "^0.2.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 2)))) (h "1kbmd8pn7g2c9fa3q4rf0y5lwsqsyn4mpagx0050l7k3g7hv0qcg")))

(define-public crate-asynchronous-0.4.5 (c (n "asynchronous") (v "0.4.5") (d (list (d (n "ansi_term") (r "^0.5.2") (d #t) (k 2)) (d (n "docopt") (r "^0.6.64") (d #t) (k 2)) (d (n "num_cpus") (r "^0.2.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 2)))) (h "0jwvr9ylrjhck848sbsy4va1r7cy03aaziqxmyipd643mvpcq4gb")))

