(define-module (crates-io as yn asynx) #:use-module (crates-io))

(define-public crate-asynx-0.1.0 (c (n "asynx") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1sg7lq51kx4826fqp7mvz9p7firglysjx1aaar7vsk1m5ixv6irz") (f (quote (("global") ("default" "global"))))))

