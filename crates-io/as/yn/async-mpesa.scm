(define-module (crates-io as yn async-mpesa) #:use-module (crates-io))

(define-public crate-async-mpesa-0.2.0 (c (n "async-mpesa") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0dy4vqj1qb2kb93p88gigivvpmff9f6wcypf8gql057269ck4wbc")))

