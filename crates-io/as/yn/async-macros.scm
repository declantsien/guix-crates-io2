(define-module (crates-io as yn async-macros) #:use-module (crates-io))

(define-public crate-async-macros-0.1.0 (c (n "async-macros") (v "0.1.0") (h "1jk9n8x5xj04vhfd018a6caqvwb3ravajd9w6192m0z1k04gld31")))

(define-public crate-async-macros-1.0.0 (c (n "async-macros") (v "1.0.0") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "1fib4wxiym9f045xqb8a2gyfa8yym3hb62g4jqjfmzn14jdxa8g4")))

(define-public crate-async-macros-2.0.0 (c (n "async-macros") (v "2.0.0") (d (list (d (n "futures") (r "^0.3.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.0") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "1cndfwmbjhxxk44pkh03w3v3hx1a99ad2z75wyhqa80gx26mljk4")))

