(define-module (crates-io as yn async_message_dispatcher) #:use-module (crates-io))

(define-public crate-async_message_dispatcher-0.1.0 (c (n "async_message_dispatcher") (v "0.1.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1vq05wqd7hh2ba3hp22qfbd2pz67ypbg1v9d46kxf2mbpp6wciz2")))

(define-public crate-async_message_dispatcher-0.1.1 (c (n "async_message_dispatcher") (v "0.1.1") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "071f0ij470mfb6kgjgnyfcg0g31fs4c5md94mzw2jrvvb9cw6dvl")))

(define-public crate-async_message_dispatcher-0.1.2 (c (n "async_message_dispatcher") (v "0.1.2") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0xc7jbj4vrsin59262ck0hwp6plihrwrrr2kqg2imbb4dmiwzfhv")))

(define-public crate-async_message_dispatcher-0.1.3 (c (n "async_message_dispatcher") (v "0.1.3") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "160d12qvs6qmfvmbanj3is3wl9lmmydmmbi7236zb8sxij86vjna")))

(define-public crate-async_message_dispatcher-0.2.0 (c (n "async_message_dispatcher") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0vyyzzcm61xrp0dgqs3l51xc639smcddaxhlzjqipmbh6h256v9x") (s 2) (e (quote (("utils" "dep:tokio" "dep:async-trait"))))))

(define-public crate-async_message_dispatcher-0.2.1 (c (n "async_message_dispatcher") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1ibs71r8wlv8s3gyscyc6jnza7k6b9v4wgm1abv5k80fjd4kf38y") (s 2) (e (quote (("utils" "dep:tokio" "dep:async-trait"))))))

(define-public crate-async_message_dispatcher-1.0.0 (c (n "async_message_dispatcher") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0zvbzwv45nwl3v11wcz8h4w8jhwzh6idpxkcy1p38z7jsvlgdv5p") (s 2) (e (quote (("utils" "dep:tokio" "dep:async-trait"))))))

(define-public crate-async_message_dispatcher-2.0.0 (c (n "async_message_dispatcher") (v "2.0.0") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1dx4qg9cwkq3m9sb3rwalp929vyna6xiyf8gccdx5xf4f5iivbb7") (s 2) (e (quote (("utils" "dep:tokio" "dep:async-trait"))))))

(define-public crate-async_message_dispatcher-3.0.0 (c (n "async_message_dispatcher") (v "3.0.0") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "14rn1agxs2ypmdnzy34a77rzq0vjrljij2l0ngj6jg4lird1vp90") (s 2) (e (quote (("utils" "dep:tokio" "dep:async-trait"))))))

(define-public crate-async_message_dispatcher-4.0.0 (c (n "async_message_dispatcher") (v "4.0.0") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1f17lcb81d7dsirj5rirfay4hfb3cr4n0jss0jar7x8q4yirxwlb") (s 2) (e (quote (("utils" "dep:tokio" "dep:async-trait"))))))

(define-public crate-async_message_dispatcher-5.0.0 (c (n "async_message_dispatcher") (v "5.0.0") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "133pndhgpyyvmdmnprw61k66cyi87n1w6cq9518hprhkiy1l35yh") (s 2) (e (quote (("utils" "dep:tokio" "dep:async-trait"))))))

(define-public crate-async_message_dispatcher-6.0.0 (c (n "async_message_dispatcher") (v "6.0.0") (d (list (d (n "async-trait") (r "^0.1.68") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1v1zr7x7n842dm1k9xa3i3v2wnbyfq0cmb9ashvjkyg5dc8ian7k") (s 2) (e (quote (("utils" "dep:tokio" "dep:async-trait"))))))

(define-public crate-async_message_dispatcher-7.0.0 (c (n "async_message_dispatcher") (v "7.0.0") (d (list (d (n "tokio") (r "^1.28.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0jzdivc24hszknjns3783pwz0id7bn2r6dsr493q0yqx543451ar")))

