(define-module (crates-io as yn async-variadic) #:use-module (crates-io))

(define-public crate-async-variadic-1.0.0 (c (n "async-variadic") (v "1.0.0") (h "09kpa4bpc28xlhn5hp59f2ryjgygjv8mibi85pmgc0gzanmhgchv")))

(define-public crate-async-variadic-1.1.0 (c (n "async-variadic") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)))) (h "0iy1nii39w6234g9cnxk6jmxcryh0wlrbbn4h259k9p4yjbyrvj7")))

(define-public crate-async-variadic-1.1.1 (c (n "async-variadic") (v "1.1.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)))) (h "02cf2gk7s9q8vcii1rp7gkbkm98djpj6yy72p1aqc1iak43b3k1h")))

