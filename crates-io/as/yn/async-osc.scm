(define-module (crates-io as yn async-osc) #:use-module (crates-io))

(define-public crate-async-osc-0.2.0 (c (n "async-osc") (v "0.2.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("unstable" "attributes"))) (d #t) (k 2)) (d (n "futures-lite") (r "^1.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rosc") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0qm5l8nzdhkr7vxgk9bn5gdn6sa2v12qyfj0mrgy613c27nq56ly")))

