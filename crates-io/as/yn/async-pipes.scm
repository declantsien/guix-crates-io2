(define-module (crates-io as yn async-pipes) #:use-module (crates-io))

(define-public crate-async-pipes-0.1.0 (c (n "async-pipes") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lg71flpcbxavc23dl80i6rj9pkpyny5pnr00dpjj3py5q7amrsq") (r "1.63.0")))

(define-public crate-async-pipes-0.2.0 (c (n "async-pipes") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "181367i9nllda8zx84ayvm7r04ccrk2l7vlhwp1pfrnnxirn8npl") (r "1.63.0")))

(define-public crate-async-pipes-0.2.1 (c (n "async-pipes") (v "0.2.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0257mi16p1bkiz6z2f2v7a9ayp0y9sd8l9xdi7xzirhwf1j9j3pq") (r "1.63.0")))

(define-public crate-async-pipes-0.2.2 (c (n "async-pipes") (v "0.2.2") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "02m70f2x498i5ilsw40c9z3cvf0pp5mc4703caxb4x54cspnw6jz") (r "1.63.0")))

(define-public crate-async-pipes-0.3.0 (c (n "async-pipes") (v "0.3.0") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 2)) (d (n "ulid") (r "^1.1") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0wqcm7d8fkbm580kx58ljad2n4gchpy3j3833s0pcglybzq3mkyi") (r "1.74.1")))

(define-public crate-async-pipes-0.3.1 (c (n "async-pipes") (v "0.3.1") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "time"))) (d #t) (k 2)) (d (n "ulid") (r "^1.1") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m3f8fhnvdv2ss6mlpacislz4lqs6i8jf53ilvafxnkp0bapyfv8") (r "1.74.1")))

