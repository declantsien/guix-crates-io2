(define-module (crates-io as yn async-backtrace-attributes) #:use-module (crates-io))

(define-public crate-async-backtrace-attributes-0.0.0 (c (n "async-backtrace-attributes") (v "0.0.0") (h "1w7hiq7y9klcg7jpc8h3y5v6rc3g6q8pidrhx45ij6avl49gy7y8")))

(define-public crate-async-backtrace-attributes-0.2.0 (c (n "async-backtrace-attributes") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "printing" "visit" "visit-mut" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "0vgfm23abb7xn9hq3j4ckxywp0i9w3mk41lyzr8wfzw9z4c7fqry") (r "1.59")))

(define-public crate-async-backtrace-attributes-0.2.1 (c (n "async-backtrace-attributes") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "printing" "visit" "visit-mut" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "0y7gbgn06clbg2gy20x6cj3nrkzj3zn2qzi8n3ap5qalcrrvscwq") (r "1.59")))

(define-public crate-async-backtrace-attributes-0.2.2 (c (n "async-backtrace-attributes") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing" "printing" "visit" "visit-mut" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "05zd4npzvb7kl431assw4ak1yivwjqpjmrv8yx48igl7afmfnn9n") (r "1.59")))

(define-public crate-async-backtrace-attributes-0.2.3 (c (n "async-backtrace-attributes") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "visit" "visit-mut" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "1hcalwii2qmj32jq7gjf903d56wb2jba0hrjy61mc24fcf9wip0m") (r "1.59")))

(define-public crate-async-backtrace-attributes-0.2.4 (c (n "async-backtrace-attributes") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "visit" "visit-mut" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "09gdip7l7q95k00h13i3w9a5phq7qya8z8vmcz6cq3had4r2401z") (r "1.59")))

(define-public crate-async-backtrace-attributes-0.2.5 (c (n "async-backtrace-attributes") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "visit" "visit-mut" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "1q66y77d5dyjyz3bc785f4wmv4v5gxxrswkfqqwhhrr0a9gbqjca") (r "1.59")))

(define-public crate-async-backtrace-attributes-0.2.6 (c (n "async-backtrace-attributes") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "visit" "visit-mut" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "0imdz29awagzdwq14skhgr4zz087y33fyy2r6rscj6n9vsg2w3c4") (r "1.59")))

(define-public crate-async-backtrace-attributes-0.2.7 (c (n "async-backtrace-attributes") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "visit" "visit-mut" "clone-impls" "extra-traits" "proc-macro"))) (k 0)))) (h "1my3av4hb92cg9p4iv7pa98c0ywjfmsrjw835930dpca8c6vmyxg")))

