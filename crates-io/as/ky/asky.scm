(define-module (crates-io as ky asky) #:use-module (crates-io))

(define-public crate-asky-0.1.0 (c (n "asky") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)))) (h "1f4z5v5p3cg1dqbzc8aypjlbfkxgp48nm7cpfgl18ina7haww4mr")))

(define-public crate-asky-0.1.1 (c (n "asky") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)))) (h "0y98g6135sm8fn2f4kb707fppk6wp83a2bagc1vj14kngd49mb6z")))

