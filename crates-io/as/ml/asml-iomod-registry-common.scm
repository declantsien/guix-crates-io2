(define-module (crates-io as ml asml-iomod-registry-common) #:use-module (crates-io))

(define-public crate-asml-iomod-registry-common-0.1.0 (c (n "asml-iomod-registry-common") (v "0.1.0") (d (list (d (n "asml_iomod_dynamodb") (r "^0.1.5") (d #t) (k 0) (p "assemblylift-iomod-dynamodb-guest")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1034rx8xgfv7swc6px95rx107c0f1ff2y50r3fn70f1516j6dlwl")))

(define-public crate-asml-iomod-registry-common-0.1.1 (c (n "asml-iomod-registry-common") (v "0.1.1") (d (list (d (n "asml_iomod_dynamodb") (r "^0.1.5") (d #t) (k 0) (p "assemblylift-iomod-dynamodb-guest")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19ya3j5pkd85yjkiagzcv9imvymlh4nhawb359p0rf1wvqrvcmp6")))

(define-public crate-asml-iomod-registry-common-0.1.2 (c (n "asml-iomod-registry-common") (v "0.1.2") (d (list (d (n "asml_iomod_dynamodb") (r "^0.1.5") (d #t) (k 0) (p "assemblylift-iomod-dynamodb-guest")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mx3jlai0fc4wp0j6n5spzz1z0bx1bbpkk1f9rlc0v74f7mm7c4z")))

(define-public crate-asml-iomod-registry-common-0.1.3 (c (n "asml-iomod-registry-common") (v "0.1.3") (d (list (d (n "asml_iomod_dynamodb") (r "^0.1.5") (d #t) (k 0) (p "assemblylift-iomod-dynamodb-guest")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i4jb7bv5kwsdpnjyl37yc8hm67qx9zkc0zcqwybf22b0ryi7yhb")))

(define-public crate-asml-iomod-registry-common-0.1.4 (c (n "asml-iomod-registry-common") (v "0.1.4") (d (list (d (n "asml_iomod_dynamodb") (r "^0.1") (d #t) (k 0) (p "assemblylift-iomod-dynamodb-guest")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05b3k17zhvhd10y0058lkb5dram6wfak2bkh4gykby9bdwhbfz2z")))

