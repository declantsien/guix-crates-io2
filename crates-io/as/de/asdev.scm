(define-module (crates-io as de asdev) #:use-module (crates-io))

(define-public crate-asdev-0.1.1 (c (n "asdev") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)))) (h "1hsi31d7dmvi3rm9f42mh61khvy12z4ypik0xrk6hsg0b6xf8rqy")))

(define-public crate-asdev-0.1.2 (c (n "asdev") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)))) (h "1i1gglvsz2czd33p3sr22hspr38qs3l7ldw33wy6zx2mv74vxji2")))

(define-public crate-asdev-0.1.3 (c (n "asdev") (v "0.1.3") (d (list (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)))) (h "05d00cm5vq98sk6d2q24ssccxg8y4hjjymlm62wnfrfribi6zvnf")))

(define-public crate-asdev-0.1.4 (c (n "asdev") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)))) (h "02l57m9m686nlry09rkhhs31q7ylfnxhss1awb0r77hf8imw5r2i")))

(define-public crate-asdev-0.1.5 (c (n "asdev") (v "0.1.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)))) (h "0kwmv6fwrl04ywk0my3aa3a971z2d9bd8skkicidpsv2py8hs3z7")))

