(define-module (crates-io as h- ash-layer) #:use-module (crates-io))

(define-public crate-ash-layer-0.0.1 (c (n "ash-layer") (v "0.0.1") (d (list (d (n "ash") (r "^0.37.1") (k 0)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)))) (h "17piai5bws9xwfrdhq6zbpajkyj86icxxxkmcl39635l4bhlaw2h") (f (quote (("default" "debug") ("debug" "ash/debug")))) (r "1.59.0")))

