(define-module (crates-io as h- ash-direct-entry) #:use-module (crates-io))

(define-public crate-ash-direct-entry-0.1.0 (c (n "ash-direct-entry") (v "0.1.0") (d (list (d (n "ash") (r "^0.32.1") (k 0)))) (h "13nnkx0sz9lb8w4ycvvgk5pkyj01fakyjqgkinxldb96gfjk3pm2") (l "vulkan")))

(define-public crate-ash-direct-entry-0.2.0 (c (n "ash-direct-entry") (v "0.2.0") (d (list (d (n "ash") (r "^0.33.0") (k 0)))) (h "0mq259jjk2y98319vdbrdrijmwd8ms2910gxlq3n47xq5jddcvxx") (l "vulkan")))

