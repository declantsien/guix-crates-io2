(define-module (crates-io as h- ash-window) #:use-module (crates-io))

(define-public crate-ash-window-0.1.0 (c (n "ash-window") (v "0.1.0") (d (list (d (n "ash") (r "^0.29") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)))) (h "1i3w278dr1l450v05xi23f1yxxs9fcv434vjsk8m88dhchwn39m8")))

(define-public crate-ash-window-0.2.0 (c (n "ash-window") (v "0.2.0") (d (list (d (n "ash") (r "^0.29") (d #t) (k 0)) (d (n "beryllium") (r "^0.1.2") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "0pczv7ss5lbwa61wlmmmnzgk0kridwb6cjsdvbpik4hyf45da1nw")))

(define-public crate-ash-window-0.3.0 (c (n "ash-window") (v "0.3.0") (d (list (d (n "ash") (r "^0.30") (d #t) (k 0)) (d (n "beryllium") (r "^0.1.2") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "0rbxncznzxlwfjgpd9xq91jqklprjmcd29nw9wra0phqrsg678pl")))

(define-public crate-ash-window-0.4.0 (c (n "ash-window") (v "0.4.0") (d (list (d (n "ash") (r "^0.31") (d #t) (k 0)) (d (n "beryllium") (r "^0.1.2") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "0afv5zcq1h7wlshnqqa0w813axb02hwv1xh99rg1ap648b8qvlz7")))

(define-public crate-ash-window-0.4.1 (c (n "ash-window") (v "0.4.1") (d (list (d (n "ash") (r "^0.31") (d #t) (k 0)) (d (n "beryllium") (r "^0.1.2") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.1") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "1gf51jiifmmwazzn6jidi6y1z59gw56ghxxaicwzs0kjjch90v8g")))

(define-public crate-ash-window-0.5.0 (c (n "ash-window") (v "0.5.0") (d (list (d (n "ash") (r "^0.31") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.1") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "1mmg8gh9gml17z4gpvszxb1y8xpq4lk8iwp8qfrplbkmbyi4qp4h")))

(define-public crate-ash-window-0.6.0 (c (n "ash-window") (v "0.6.0") (d (list (d (n "ash") (r "^0.32") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.1") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "18zzcsv9adcf5ky07hvisybvmsm67rv4wp32kdp0qhd8lcv8819d")))

(define-public crate-ash-window-0.7.0 (c (n "ash-window") (v "0.7.0") (d (list (d (n "ash") (r "^0.33") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.1") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "1bvjz3ny5pmisr9sxd83z7bcd6mfbdyxav1xknljn6myqvj1ry8j")))

(define-public crate-ash-window-0.8.0 (c (n "ash-window") (v "0.8.0") (d (list (d (n "ash") (r "^0.34") (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.1") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "1f7jk44c9fmvqd8n0n4vp09wynh8k4m01ny3yd5d5inx1ggbkhaq")))

(define-public crate-ash-window-0.9.0 (c (n "ash-window") (v "0.9.0") (d (list (d (n "ash") (r "^0.35") (k 0)) (d (n "ash") (r "^0.35") (f (quote ("linked"))) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.1") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "1rwyw73l3jcq9l2kz32fp8zj47y57fc67y5aia13z5l1vb311xf2")))

(define-public crate-ash-window-0.9.1 (c (n "ash-window") (v "0.9.1") (d (list (d (n "ash") (r ">=0.34, <=0.36") (k 0)) (d (n "ash") (r "^0.36") (f (quote ("linked"))) (k 2)) (d (n "raw-window-handle") (r "^0.3.4") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.1") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "14zz88w55gl08mda2dpp7cbq6l7y1kyw47jjcq2a9ad5rirh8m7d")))

(define-public crate-ash-window-0.10.0 (c (n "ash-window") (v "0.10.0") (d (list (d (n "ash") (r "^0.37") (k 0)) (d (n "ash") (r "^0.37") (f (quote ("linked"))) (k 2)) (d (n "raw-window-handle") (r "^0.3.4") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.1") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.19.4") (d #t) (k 2)))) (h "1fkqpvqb0yfghj11rjav1h63fr713rn5sgsvhs6mqsd1350rl6af")))

(define-public crate-ash-window-0.11.0 (c (n "ash-window") (v "0.11.0") (d (list (d (n "ash") (r "^0.37") (k 0)) (d (n "ash") (r "^0.37") (f (quote ("linked"))) (k 2)) (d (n "raw-window-handle") (r "^0.4.2") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.2") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "0mvamc4vr454f87hl1wdwxk19iz0z8rzsjwgndh15nmhpz8761bz") (r "1.59.0")))

(define-public crate-ash-window-0.12.0 (c (n "ash-window") (v "0.12.0") (d (list (d (n "ash") (r "^0.37") (k 0)) (d (n "ash") (r "^0.37") (f (quote ("linked"))) (k 2)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.3") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.27.1") (d #t) (k 2)))) (h "1armbqzr0x905yypvh9ywgjj91kn93y5mxd6gkwaiwr9gid2h4mr") (r "1.59.0")))

(define-public crate-ash-window-0.13.0 (c (n "ash-window") (v "0.13.0") (d (list (d (n "ash") (r "^0.38") (f (quote ("std"))) (k 0)) (d (n "ash") (r "^0.38") (f (quote ("linked"))) (k 2)) (d (n "raw-window-handle") (r "^0.6") (d #t) (k 0)) (d (n "raw-window-metal") (r "^0.4") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.29") (f (quote ("rwh_06"))) (d #t) (k 2)))) (h "10kcx905yglg2s06yxkrvfv3qw8z46w83pmb7dayb0fbc5xsdg2j") (r "1.69.0")))

