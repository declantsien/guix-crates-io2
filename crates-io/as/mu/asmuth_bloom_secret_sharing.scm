(define-module (crates-io as mu asmuth_bloom_secret_sharing) #:use-module (crates-io))

(define-public crate-asmuth_bloom_secret_sharing-0.2.0 (c (n "asmuth_bloom_secret_sharing") (v "0.2.0") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1b09ssd45q7qgq2xrpr9npxa0m00c2143nzbyqlvj00s03aq0kki")))

(define-public crate-asmuth_bloom_secret_sharing-0.2.1 (c (n "asmuth_bloom_secret_sharing") (v "0.2.1") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0x0vaxhbh12c4wrv3mlx8qz99zljyg0z49149w45l0cqaqbaxyb8")))

(define-public crate-asmuth_bloom_secret_sharing-0.2.2 (c (n "asmuth_bloom_secret_sharing") (v "0.2.2") (d (list (d (n "built") (r "^0.2") (d #t) (k 1)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "01m1n4vs8q4m0kybgwayi1yd0gcyhfac1wy350imbyif9cy33iv5")))

