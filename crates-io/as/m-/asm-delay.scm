(define-module (crates-io as m- asm-delay) #:use-module (crates-io))

(define-public crate-asm-delay-0.2.0 (c (n "asm-delay") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0a53gcr6gdj5h2kq3g3c47bjj0ic75gzr8drc7bl305468cjjy1c")))

(define-public crate-asm-delay-0.3.0 (c (n "asm-delay") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0761ha745lsbjgjw1lqhakks9crr4m1mswnsbz48snszb2xvghya")))

(define-public crate-asm-delay-0.3.1 (c (n "asm-delay") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "03qsg5yz4id0wb1bxiqd6hr4dbfvl4b4zcq1jb48iahagnxyp2ny")))

(define-public crate-asm-delay-0.4.0 (c (n "asm-delay") (v "0.4.0") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "068lhfvl8x46jnfhqlxg4dpmb1n6ka0d6jw1y3q9ns96p25vphyc")))

(define-public crate-asm-delay-0.5.0 (c (n "asm-delay") (v "0.5.0") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0bav1p5xf1xjqrm6ipy1pr3sdgjqps68ckfvbp9827shp0h6h7ky")))

(define-public crate-asm-delay-0.5.1 (c (n "asm-delay") (v "0.5.1") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0ca7fybxfrwzd8kvhqyilvk1czirp5i0fp1j2maxdgzxkz0qkhlh")))

(define-public crate-asm-delay-0.5.2 (c (n "asm-delay") (v "0.5.2") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1iba6gry965i4q1xh10ni58dm4ziyyhjx0xbvjv3kxx88217znna")))

(define-public crate-asm-delay-0.5.3 (c (n "asm-delay") (v "0.5.3") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "17kmrkn06piym60gs6x2p0jw17yywv9fmrd2nc51d2gic4lf537p")))

(define-public crate-asm-delay-0.6.0 (c (n "asm-delay") (v "0.6.0") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "15bp3ph9pnl3bl20jhdxjfx22l4zq0wakfk9szrlbymg7s7lzii5")))

(define-public crate-asm-delay-0.7.0 (c (n "asm-delay") (v "0.7.0") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "085fg2829qnp43girnsd423c4jrhfai0bx5gnaaawafyfgn8w30f")))

(define-public crate-asm-delay-0.7.1 (c (n "asm-delay") (v "0.7.1") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0h1lq45378hiscrbyxnkaicj37c86ss9r2dizcajwhm801i8ki0b")))

(define-public crate-asm-delay-0.7.2 (c (n "asm-delay") (v "0.7.2") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.5.8") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1xk475a24wgfqd419sxgkpa9x47qpc05kmrhslafykgaqq7cgdxq")))

(define-public crate-asm-delay-0.8.0 (c (n "asm-delay") (v "0.8.0") (d (list (d (n "cortex-m") (r ">=0.6") (d #t) (k 0)) (d (n "embedded-hal") (r ">=0.2") (d #t) (k 0)) (d (n "embedded-time") (r "^0.6.0") (d #t) (k 0)))) (h "1b7m4514lajz40f5lyk0c78vy3gz1rvwlidhhkg29rar6h0418h3")))

(define-public crate-asm-delay-0.8.1 (c (n "asm-delay") (v "0.8.1") (d (list (d (n "cortex-m") (r ">=0.6") (d #t) (k 0)) (d (n "embedded-hal") (r ">=0.2") (d #t) (k 0)) (d (n "embedded-time") (r "^0.6.0") (d #t) (k 0)))) (h "00mwc9asij1wjqb4agnfgv8v3xv9i9mmqak4fip2acjm08zcycd7")))

(define-public crate-asm-delay-0.9.0 (c (n "asm-delay") (v "0.9.0") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1ylbpf86amkribs3njrkz6d3avsplir4yll2sgyarpbh7fb9m9mr")))

(define-public crate-asm-delay-1.0.0 (c (n "asm-delay") (v "1.0.0") (d (list (d (n "bitrate") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)))) (h "0iwy9xjr8a0m5f6m0sm84lwqcjviki661amcmibasbgykkmnbsir")))

