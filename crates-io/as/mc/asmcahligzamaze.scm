(define-module (crates-io as mc asmcahligzamaze) #:use-module (crates-io))

(define-public crate-asmcahligzamaze-1.0.0 (c (n "asmcahligzamaze") (v "1.0.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("ttf" "mixer" "unsafe_textures"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jrhq7s6srv6g554pmxkxymn9q7qgf85z9hc95c5l6phmdcn89an")))

(define-public crate-asmcahligzamaze-1.0.2 (c (n "asmcahligzamaze") (v "1.0.2") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("ttf" "mixer" "unsafe_textures"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sfs4j3ljhxmqyaq71jz7fmv7ics8db0wappbhlp40g1mzb8jyrj")))

