(define-module (crates-io as #{3_}# as3_verifier) #:use-module (crates-io))

(define-public crate-as3_verifier-0.1.0 (c (n "as3_verifier") (v "0.1.0") (d (list (d (n "as3_parser") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "file_paths") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "09pxkmpyb27ashm5nbvz16s6xlawkbni9cd381si6vk6h32dc64q")))

