(define-module (crates-io as hi ashiba) #:use-module (crates-io))

(define-public crate-ashiba-0.1.0 (c (n "ashiba") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "10m49amp9i6m7n4iirb9f3n45sgf4lh0a2bc8f0rm6dww1g3rv3k")))

