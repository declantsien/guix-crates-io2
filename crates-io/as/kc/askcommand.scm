(define-module (crates-io as kc askcommand) #:use-module (crates-io))

(define-public crate-askcommand-0.1.0 (c (n "askcommand") (v "0.1.0") (d (list (d (n "async-openai") (r "^0.16.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12zi3vcp3kfvqyqfl704zwwinhnfams953sc3a43185hqsms1x3i")))

