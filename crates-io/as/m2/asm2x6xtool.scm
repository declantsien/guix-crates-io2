(define-module (crates-io as m2 asm2x6xtool) #:use-module (crates-io))

(define-public crate-asm2x6xtool-0.1.0 (c (n "asm2x6xtool") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rusb") (r "^0.9.3") (d #t) (k 0)))) (h "12jnp64w2lwx0p6km2hbk8gmmsvifvkiyw9iiivj4z4ggs1bv5vp")))

