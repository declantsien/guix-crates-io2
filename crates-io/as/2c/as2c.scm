(define-module (crates-io as #{2c}# as2c) #:use-module (crates-io))

(define-public crate-as2c-0.1.2 (c (n "as2c") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)) (d (n "ssimulacra2") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "yuvxyb") (r "^0.3.0") (d #t) (k 0)))) (h "184nkx23yzimiv5r1vrawzf22npw1h8950r0wxyxrsljrwjcg4qn")))

(define-public crate-as2c-0.1.3 (c (n "as2c") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)) (d (n "ssimulacra2") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "yuvxyb") (r "^0.3.0") (d #t) (k 0)))) (h "0vbbhzg71qxzx7637ziqy8plyiqsfm76czriaph9rckpc25mrq4n")))

