(define-module (crates-io as p_ asp_gui) #:use-module (crates-io))

(define-public crate-asp_gui-0.1.0 (c (n "asp_gui") (v "0.1.0") (d (list (d (n "asp_lib") (r "^0.1.7") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.4") (d #t) (k 0)))) (h "1qph41ymb95rs7d0qhcar4qg00ki76fcpw9y83z2rzypwp8hdlk9")))

(define-public crate-asp_gui-0.1.1 (c (n "asp_gui") (v "0.1.1") (d (list (d (n "asp_lib") (r "^0.1.7") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.4") (d #t) (k 0)))) (h "19fi7jkwac47i5p2hj4m3j7c8bw8n2d5n847rxr2slavriyg3bgk")))

(define-public crate-asp_gui-0.1.2 (c (n "asp_gui") (v "0.1.2") (d (list (d (n "asp_lib") (r "^0.1.7") (f (quote ("plotting"))) (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.4") (d #t) (k 0)))) (h "198as7x5kakdmdgxkr1zcgn8hlhq1kjp3adi09gvz235q02i08ls")))

(define-public crate-asp_gui-0.1.3 (c (n "asp_gui") (v "0.1.3") (d (list (d (n "asp_lib") (r "^0.1.10") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.4") (d #t) (k 0)))) (h "1ydglgbwy9v52pa0llm24vs293mwb2y9rdsq4bp8baxg32awdwf8")))

(define-public crate-asp_gui-0.1.4 (c (n "asp_gui") (v "0.1.4") (d (list (d (n "asp_lib") (r "^0.1.12") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rfd") (r "^0.11.4") (d #t) (k 0)))) (h "1pkm8p8lb0i4p0kgiwpmhbz50da93ncdzrr3fh45gqins05idamh")))

(define-public crate-asp_gui-0.1.5 (c (n "asp_gui") (v "0.1.5") (d (list (d (n "asp_lib") (r "^0.1.12") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "include_assets") (r "^1.0.0") (d #t) (k 0)) (d (n "rfd") (r "^0.11.4") (d #t) (k 0)))) (h "19blhcqq95j5sry12xl94xpqs4yc2xs2bp2qa4dhavmxbsd3j3d6")))

