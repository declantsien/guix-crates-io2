(define-module (crates-io as p_ asp_lib) #:use-module (crates-io))

(define-public crate-asp_lib-0.1.0 (c (n "asp_lib") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "polars") (r "^0.30.0") (f (quote ("csv"))) (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1cmzxgrcfc0d94kka0yhaqwyi86804895s3sykc4a8f0hf21vs3p")))

(define-public crate-asp_lib-0.1.1 (c (n "asp_lib") (v "0.1.1") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "polars") (r "^0.30.0") (f (quote ("csv"))) (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0lahmjyhsyl35kf8bfsr5y3pcx73r65v9ca0m1lmynfijgqikyh9") (y #t)))

(define-public crate-asp_lib-0.1.2 (c (n "asp_lib") (v "0.1.2") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "161656m84y2zhwl6b86kf0z1x4ip43c8bwlh9s1yy8lb6jwvrn1g") (y #t)))

(define-public crate-asp_lib-0.1.3 (c (n "asp_lib") (v "0.1.3") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0jp33h51qq2b2hg84ini17ziwmvs1l6504flakcs1kwdsxnhjnfk") (y #t)))

(define-public crate-asp_lib-0.1.4 (c (n "asp_lib") (v "0.1.4") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "165sy38qx01ys24sdb5qyf2dhdb20kizaqxnk85kdgya3zsaxx00") (y #t)))

(define-public crate-asp_lib-0.1.5 (c (n "asp_lib") (v "0.1.5") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0179473qaawpzx4qpqqiq5mvm6r9jld67ckp188n2dkkr54hl5n8")))

(define-public crate-asp_lib-0.1.6 (c (n "asp_lib") (v "0.1.6") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1agnkqbgn78rm0v3kgj79av9l3k3mfiibvkc50a1nm7z1xqk77rc")))

(define-public crate-asp_lib-0.1.7 (c (n "asp_lib") (v "0.1.7") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1m31c2r5v25yxcgi5172hcqsrihzksjgc0m0n2cfqhrphr8ppa3k")))

(define-public crate-asp_lib-0.1.8 (c (n "asp_lib") (v "0.1.8") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "plotly") (r "^0.8.3") (f (quote ("kaleido"))) (o #t) (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1sv1r7byhlsyryzq956h8k9pdf4zmwfcrn7h6hw4pq0y3v2qgkf4") (s 2) (e (quote (("plotting" "dep:plotly"))))))

(define-public crate-asp_lib-0.1.9 (c (n "asp_lib") (v "0.1.9") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "plotly") (r "^0.8.1") (f (quote ("kaleido"))) (o #t) (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "14rhk5iyrp2kp7d30v2qp3q2mb7p2r8hyyqpf1vywdi6x47r7byc") (s 2) (e (quote (("plotting" "dep:plotly"))))))

(define-public crate-asp_lib-0.1.10 (c (n "asp_lib") (v "0.1.10") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1db3vk32iqn3kx5cb5x2yvpv9fdip17gqdw46n22dmv4bns6g3nq")))

(define-public crate-asp_lib-0.1.11 (c (n "asp_lib") (v "0.1.11") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "03c1yv8igw29k694501j6q289jppr1lqnygw0g22ilsr3k23ilka")))

(define-public crate-asp_lib-0.1.12 (c (n "asp_lib") (v "0.1.12") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "split-iter") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1jl8jciwv2jwb17bv7zsrxkb1mdkhd0cfbxm1rdywm94v4z5v5sk")))

