(define-module (crates-io as -s as-symbols) #:use-module (crates-io))

(define-public crate-as-symbols-0.1.0 (c (n "as-symbols") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1zacf6kd2lgx4z2b0ka651jm7l54blww61ns0lxj2fz3qrr28w2w")))

(define-public crate-as-symbols-0.1.1 (c (n "as-symbols") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "1v3j82mxqw06ihwjnvsycsjrgli8gmvx2l00nij1wklaacmvwk8c")))

(define-public crate-as-symbols-0.2.0 (c (n "as-symbols") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "16zdhg7m400bmkjnc1xirhwysb21qa1z67ds6lzg6ksmkkkm3sjj")))

(define-public crate-as-symbols-0.2.1 (c (n "as-symbols") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "04bnwna79vwiff9b0mbl59yl2r46l5azq27i9k82zclqw9hbpkaw")))

(define-public crate-as-symbols-0.3.0 (c (n "as-symbols") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0g70bp5421kdar06zzx0kih1ra72ny30j1430ydr9i66smaxknkm")))

(define-public crate-as-symbols-0.4.0 (c (n "as-symbols") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "0lm9i77qqmx94f5y91f8sls69mr64v0z5yrzwxd611p3lghk96ng")))

(define-public crate-as-symbols-0.4.1 (c (n "as-symbols") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "13w4v43ylv6sy1a9gfakq7q93h3ccib1daha13c1bqizvgs83686")))

(define-public crate-as-symbols-0.4.2 (c (n "as-symbols") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "004yh5pc1311r3g5k1afrmrhgn79d4lf4dai25l8hp5rka4rrvbn")))

(define-public crate-as-symbols-0.5.0 (c (n "as-symbols") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)))) (h "18n9v0gg4wscjkqw8wiq04icva7b36y7bblb5rx9ylhsl9cy3zyp")))

