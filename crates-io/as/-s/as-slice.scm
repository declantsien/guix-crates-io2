(define-module (crates-io as -s as-slice) #:use-module (crates-io))

(define-public crate-as-slice-0.1.0 (c (n "as-slice") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "09xvwld3bg3hs8iaa7h86q0vg9irqigb1vz7jmpv1ykln9kaqg99")))

(define-public crate-as-slice-0.1.1 (c (n "as-slice") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.13.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "0abrwwl3zdlld564i68rnzbfsb820i91siq3yg1pnkr839f7r9mj") (y #t)))

(define-public crate-as-slice-0.2.0 (c (n "as-slice") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.13.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "12hgfmvrvvn2mbdscsqwb66gbs4vbk2zvbbcwzl5f4y31cr3nvc6") (y #t)))

(define-public crate-as-slice-0.1.2 (c (n "as-slice") (v "0.1.2") (d (list (d (n "ga13") (r "^0.13.0") (d #t) (k 0) (p "generic-array")) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "012cjlhf25mf6c5j7qz40jd198crx9fq4nmbkkqm7ms9mjapwsxy")))

(define-public crate-as-slice-0.1.3 (c (n "as-slice") (v "0.1.3") (d (list (d (n "ga13") (r "^0.13.0") (d #t) (k 0) (p "generic-array")) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "0w0f56lnd9l6369jh6iv1qb08zl1lqa4y017x1gcharvq1dvdprp")))

(define-public crate-as-slice-0.1.4 (c (n "as-slice") (v "0.1.4") (d (list (d (n "ga13") (r "^0.13.0") (d #t) (k 0) (p "generic-array")) (d (n "ga14") (r "^0.14.0") (d #t) (k 0) (p "generic-array")) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "1rmhdfj11va424163d6r79wbgf2043i2p37s59ky6x2v8wiiqkdv")))

(define-public crate-as-slice-0.1.5 (c (n "as-slice") (v "0.1.5") (d (list (d (n "ga13") (r "^0.13.3") (d #t) (k 0) (p "generic-array")) (d (n "ga14") (r "^0.14.4") (d #t) (k 0) (p "generic-array")) (d (n "generic-array") (r "^0.12.4") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "1q3a9494ikaq38zjg5px5gwwrbdgnyj23b505224njlmwd4knh25")))

(define-public crate-as-slice-0.2.1 (c (n "as-slice") (v "0.2.1") (d (list (d (n "stable_deref_trait") (r "^1.1.1") (k 0)))) (h "05j52y1ws8kir5zjxnl48ann0if79sb56p9nm76hvma01r7nnssi")))

