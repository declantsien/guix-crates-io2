(define-module (crates-io as tm astmaker) #:use-module (crates-io))

(define-public crate-astmaker-0.1.0 (c (n "astmaker") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0xfps8zl7rhhsjilkr4pfmszw3yr2jmbvivxm2j9mafcrh9c3mw4")))

(define-public crate-astmaker-0.1.1 (c (n "astmaker") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0x4q6avzkfzd00nh5qj7s4xrlf6gj91gasidbsy589wyq1i5ijp3")))

(define-public crate-astmaker-0.2.0 (c (n "astmaker") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0v0dj87p3m30zvvy5d1pp03xmkv23a4lz16373bcgvxkq1asmj2j")))

