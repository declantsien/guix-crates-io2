(define-module (crates-io as _v as_variant) #:use-module (crates-io))

(define-public crate-as_variant-1.0.0 (c (n "as_variant") (v "1.0.0") (h "0s1vva8zf05zrd4k2bibrgg9z3z23banrcda4vc2csi8hffv2skj")))

(define-public crate-as_variant-1.0.1 (c (n "as_variant") (v "1.0.1") (h "1brkbk68cn5n3115cxfd646mgn9k1jgq272fjbrwccki0skifks9")))

(define-public crate-as_variant-1.1.0 (c (n "as_variant") (v "1.1.0") (h "0a40x12s6fa0iwl59y4k05b0lmyr9rq41nxn0zgnckm51wxl7w7m")))

(define-public crate-as_variant-1.2.0 (c (n "as_variant") (v "1.2.0") (h "13vryhfcslf50f8j1msnxg5689bzwz56z45dgzxqd7r40wis53zk")))

