(define-module (crates-io as ep aseprite) #:use-module (crates-io))

(define-public crate-aseprite-0.1.0 (c (n "aseprite") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09d36kii6p315n4rvk0sgq5pq3aywph25zr2d3pghijagjkirqly")))

(define-public crate-aseprite-0.1.1 (c (n "aseprite") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x6ivf3qpy6a7h8m5azzxipzxll0ffw741w8pdrz9d7hdvglkgp2")))

(define-public crate-aseprite-0.1.2 (c (n "aseprite") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00djyyay80sd9xz2yc6dw6nq2rc56rjxwzay88k97i3l86rvf8c7")))

(define-public crate-aseprite-0.1.3 (c (n "aseprite") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03h7qrl9z8804g77aiki7j39wbsx3xf75m6q7ca20yi51m10zcj8")))

