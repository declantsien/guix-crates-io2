(define-module (crates-io as ep aseprite-loader) #:use-module (crates-io))

(define-public crate-aseprite-loader-0.2.0 (c (n "aseprite-loader") (v "0.2.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.25") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1sn1smnyp5vxi0hlhf9zx9vs9spywhdn6bkjil52pvz6xr5jhfmb")))

