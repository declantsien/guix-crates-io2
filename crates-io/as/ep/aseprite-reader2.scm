(define-module (crates-io as ep aseprite-reader2) #:use-module (crates-io))

(define-public crate-aseprite-reader2-0.1.0 (c (n "aseprite-reader2") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)))) (h "0c3n4gnkr1a89x063a5dlsj7fclcrsr33jlnc1yxdhxxb6qiz9li")))

