(define-module (crates-io as ep aseprite-packer) #:use-module (crates-io))

(define-public crate-aseprite-packer-0.1.0 (c (n "aseprite-packer") (v "0.1.0") (d (list (d (n "asefile") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "texture_packer") (r "^0.23.0") (d #t) (k 0)))) (h "02n4cf5b8wv61vjijggm2xl597pvzwqna77zrqck9fz693gw5q53")))

(define-public crate-aseprite-packer-0.1.1 (c (n "aseprite-packer") (v "0.1.1") (d (list (d (n "asefile") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "texture_packer") (r "^0.23.0") (d #t) (k 0)))) (h "14gj6mbhrsd7rxd4qn4pax6d2zcx0g61n6fjw3b6k1hzjjpj0fgj")))

(define-public crate-aseprite-packer-0.1.2 (c (n "aseprite-packer") (v "0.1.2") (d (list (d (n "asefile") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "texture_packer") (r "^0.23.0") (d #t) (k 0)))) (h "0hz36zfll4gpq34rmx0hr972w1nr4ky249nh92q2ijwb95qckxna")))

(define-public crate-aseprite-packer-0.1.3 (c (n "aseprite-packer") (v "0.1.3") (d (list (d (n "asefile") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "texture_packer") (r "^0.23.0") (d #t) (k 0)))) (h "1l41vlk5v6y5235jcmdjgvcsiv0kvfzvw90vpcp6pw7k644l5pd2")))

(define-public crate-aseprite-packer-0.1.4 (c (n "aseprite-packer") (v "0.1.4") (d (list (d (n "asefile") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "texture_packer") (r "^0.23.0") (d #t) (k 0)))) (h "0wcz4djpysbgl0lcc8iyqhb2cln4xr56ddj2gf4l95z0vg21m41d")))

