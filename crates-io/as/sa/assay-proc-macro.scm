(define-module (crates-io as sa assay-proc-macro) #:use-module (crates-io))

(define-public crate-assay-proc-macro-0.1.0 (c (n "assay-proc-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pcnniy2zspcnwpyzgqz92nxc4izj48nambjncjf4px7qqvxcpqb")))

(define-public crate-assay-proc-macro-0.1.1 (c (n "assay-proc-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07l8a02px058hh6hg4vpzz3g8r4wwmal205170divkrcawdi552x")))

