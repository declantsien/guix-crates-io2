(define-module (crates-io as sa assay) #:use-module (crates-io))

(define-public crate-assay-0.1.0 (c (n "assay") (v "0.1.0") (d (list (d (n "assay-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio") (r "^1.16.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "19ln9ic41glh83ilrzwj5px3rv38pq91k62wmv6fizawrnm85393")))

(define-public crate-assay-0.1.1 (c (n "assay") (v "0.1.1") (d (list (d (n "assay-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "0f0rw4vdnnbfdh10cvrqjl8xv44hdxr3vlm431zspznabiw00r4b")))

