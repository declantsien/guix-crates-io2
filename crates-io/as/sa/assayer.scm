(define-module (crates-io as sa assayer) #:use-module (crates-io))

(define-public crate-assayer-0.2.0 (c (n "assayer") (v "0.2.0") (h "02zfrcqv15pcy7jhkls4dr76kwyvg9172bz0r2nz23s8bk2wgdgi")))

(define-public crate-assayer-0.2.1 (c (n "assayer") (v "0.2.1") (h "1k7xjrwniv354kz0ds7fvdc2dpic9psk08n64738s3va1sp8hvdz")))

(define-public crate-assayer-0.2.2 (c (n "assayer") (v "0.2.2") (h "16wr39x0i6j0g4s4648pv6fdi1hnvb3fafxn3zvyjpy0diga71vy")))

