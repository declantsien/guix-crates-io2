(define-module (crates-io as _w as_warp_reply) #:use-module (crates-io))

(define-public crate-as_warp_reply-0.1.0 (c (n "as_warp_reply") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1yg0iwkjgw9nzvsh4n3cmap4xspf9ddfdvyxmfm0fdy0dbcihz7p")))

