(define-module (crates-io as _w as_with_bytes) #:use-module (crates-io))

(define-public crate-as_with_bytes-0.1.0 (c (n "as_with_bytes") (v "0.1.0") (h "0dmksw0cgvkcfnb6vnlvbff7sg7fzav01624p06sd1qiznwccgvw") (y #t)))

(define-public crate-as_with_bytes-0.1.1 (c (n "as_with_bytes") (v "0.1.1") (h "0zl0hqdd1pgqxlpmqxn45f5i7vbb96n11q2b0asb5s6zik0idw01") (y #t)))

(define-public crate-as_with_bytes-0.1.2 (c (n "as_with_bytes") (v "0.1.2") (h "15zgw803f6aaqr7a6vpz03r84gbp51x56l46riqm4w7q4d5p9wk7") (y #t)))

