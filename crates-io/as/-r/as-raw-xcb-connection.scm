(define-module (crates-io as -r as-raw-xcb-connection) #:use-module (crates-io))

(define-public crate-as-raw-xcb-connection-0.1.0 (c (n "as-raw-xcb-connection") (v "0.1.0") (h "0is9k1gsx2a7809qcvrxgv7r4d6z076axc13js30anl43l90w1s5")))

(define-public crate-as-raw-xcb-connection-1.0.0 (c (n "as-raw-xcb-connection") (v "1.0.0") (h "1am99fbsp5f5vnbvr0qnjma36q49c9zvdbn0czwwvian18mk2prd") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-as-raw-xcb-connection-1.0.1 (c (n "as-raw-xcb-connection") (v "1.0.1") (h "0sqgpz2ymv5yx76r5j2npjq2x5qvvqnw0vrs35cyv30p3pfp2m8p") (f (quote (("default" "alloc") ("alloc"))))))

