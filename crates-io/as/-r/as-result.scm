(define-module (crates-io as -r as-result) #:use-module (crates-io))

(define-public crate-as-result-0.1.0 (c (n "as-result") (v "0.1.0") (h "01v3kvi8s81scgwg92rk8j7scy2c2d4zvqvm80pgh5m6srgvycsl")))

(define-public crate-as-result-0.2.0 (c (n "as-result") (v "0.2.0") (h "0rpk7licrkfq5isw54j6ypdnjfvgglic9hzf3pzivylyb4264wq0")))

(define-public crate-as-result-0.2.1 (c (n "as-result") (v "0.2.1") (h "1dx8avnj0hnckb6x39nwhcqpqzy8qi81dr2scp6i0530q71wl0ip")))

