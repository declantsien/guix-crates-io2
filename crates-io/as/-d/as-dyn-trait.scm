(define-module (crates-io as -d as-dyn-trait) #:use-module (crates-io))

(define-public crate-as-dyn-trait-0.1.0 (c (n "as-dyn-trait") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "11zc5jakjbdhmzdr6fsrvpb98ddnjclw28jsb2svvmw2ydqb0rls")))

(define-public crate-as-dyn-trait-0.2.0 (c (n "as-dyn-trait") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "04g7nbmllg06z8062jzs8whyiznhs5cs0w2fq6cc4m9hbdrfc41w")))

