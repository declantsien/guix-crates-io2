(define-module (crates-io as mp asmpeach) #:use-module (crates-io))

(define-public crate-asmpeach-0.1.45 (c (n "asmpeach") (v "0.1.45") (d (list (d (n "elf-utilities") (r "^0.1.67") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "1jnkjyn96735x3g5rbd8ykdrl0jwk13daigzk35w3h8m434029k1")))

(define-public crate-asmpeach-0.1.46 (c (n "asmpeach") (v "0.1.46") (d (list (d (n "elf-utilities") (r "^0.1.73") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "14h77mpljm7ccsdx3lla6aw20m0jq1y5f9yh8ix34nh1iyz3zrq2")))

(define-public crate-asmpeach-0.1.47 (c (n "asmpeach") (v "0.1.47") (d (list (d (n "elf-utilities") (r "^0.1.77") (d #t) (k 0)) (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)))) (h "0q62hqsbf6yg3agcmxsqfqf95mi5h0ii353dczc4chh1d0gp6j77")))

