(define-module (crates-io as ca ascas) #:use-module (crates-io))

(define-public crate-ascas-0.1.0 (c (n "ascas") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)))) (h "1dvb9w74dic85jxh6qkm5mkhqwzb85q047wpb46f24w5hxlk1h8h") (y #t)))

(define-public crate-ascas-0.1.5 (c (n "ascas") (v "0.1.5") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)))) (h "08r4iq4hd4q4dbs6s2d75ss5bv4hqzc4m3lwn547kfwi2xjnp8hf") (y #t)))

