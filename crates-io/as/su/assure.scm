(define-module (crates-io as su assure) #:use-module (crates-io))

(define-public crate-assure-1.0.0 (c (n "assure") (v "1.0.0") (h "10qjzlr04m15cjh9n35wsd20n2py7npbknc6bjjsm9pjqb5jci91")))

(define-public crate-assure-1.1.0 (c (n "assure") (v "1.1.0") (h "0aa7i8kq6r45gf1g1c4g4paa95l03m3xm91hifpzv7kfkycfw3km")))

(define-public crate-assure-1.2.0 (c (n "assure") (v "1.2.0") (h "1h6lq1ph910x5z8iv3azn37zaqq53s22a7slp8vgywbcc8a2k7vz")))

(define-public crate-assure-1.3.0 (c (n "assure") (v "1.3.0") (h "1b3r48mj1nsi5mslskncaszbn404qbs41z11kjh3y87kj62qz8an")))

(define-public crate-assure-1.4.0 (c (n "assure") (v "1.4.0") (h "01nd19y7bffm7j5y4grlwyz0kbhwnlxkp62bbi96ihl88lyr0c1f")))

(define-public crate-assure-1.5.0 (c (n "assure") (v "1.5.0") (h "0ag2bl3s08hqa5zv2h2ghcf5h5a8d6yjbhgwbwpbpajjgy41wk7s")))

(define-public crate-assure-1.6.0 (c (n "assure") (v "1.6.0") (h "1p98w6wjf63w4lsj59w3dllqniqf8s9azdk7mc12g65wb6bma2qz")))

(define-public crate-assure-1.7.0 (c (n "assure") (v "1.7.0") (h "1rxzmcw7k1cvlcbvnkwc5fy728i3sz4sbb2bp51rwxrgvy0pzc2h")))

(define-public crate-assure-1.8.0 (c (n "assure") (v "1.8.0") (h "1pvqnsbdb6a5kj784sbszmdsv9fhm4xycvah34lygj1h43j0k2bv")))

(define-public crate-assure-1.9.0 (c (n "assure") (v "1.9.0") (h "08wwn7q041i8j4laiabbma5z43rhf8l5dsn626xf6zw87wzrmiz3")))

(define-public crate-assure-1.10.0 (c (n "assure") (v "1.10.0") (h "1yrh91q7sy92md2svhgqy2s3qpk1d52cl4nz59mh27igsgpc18qn")))

(define-public crate-assure-1.11.0 (c (n "assure") (v "1.11.0") (h "0crj749nbhvjjhwv6rgl83rf7ci2rz7mmp4ikbypk1y7dbw626vd")))

(define-public crate-assure-2.0.0 (c (n "assure") (v "2.0.0") (h "0qy5i5g8y23p72x7bimr55lgddy22kanl8i638va2amx5vx49rm8")))

(define-public crate-assure-2.1.0 (c (n "assure") (v "2.1.0") (h "09z4idnnw0087bm7kp46c39hns22qf4hz23n1m71g14bs2iiyclg")))

