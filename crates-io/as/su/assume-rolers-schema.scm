(define-module (crates-io as su assume-rolers-schema) #:use-module (crates-io))

(define-public crate-assume-rolers-schema-0.3.0 (c (n "assume-rolers-schema") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jy651lyhhn124ywzn1vvxa7v859x2v7bsnncwp7wk26rcwmrzhb")))

