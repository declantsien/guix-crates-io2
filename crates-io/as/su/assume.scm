(define-module (crates-io as su assume) #:use-module (crates-io))

(define-public crate-assume-0.1.0 (c (n "assume") (v "0.1.0") (h "0p7pxywv88yzngsrzyazjpcw0cm11c9zfk4jcl8k0y9b1r7admsc")))

(define-public crate-assume-0.2.0 (c (n "assume") (v "0.2.0") (h "15d7abqn706vgxaj5pxfhmlxmmpdbxp2i3ldnfbxaqn0bagzq93f")))

(define-public crate-assume-0.2.1 (c (n "assume") (v "0.2.1") (h "140j11hlcaqd8qwi2bdg2klr7ii06j1sa9aplikcmbx0297yy8vf")))

(define-public crate-assume-0.3.0 (c (n "assume") (v "0.3.0") (h "11n95bj0bgzn3n93mrw243qbi0zanxdqrqlh30zh29wrnb604anl")))

(define-public crate-assume-0.4.0 (c (n "assume") (v "0.4.0") (h "124slg194by4vy9a2dkj9hcksgp4ja1k8kd4wgchc7s5xxjxg8y8")))

(define-public crate-assume-0.5.0 (c (n "assume") (v "0.5.0") (h "0g8c6j1gnvjqwyjm6d1hf5w5hv3h3i0bc3kh8vq4xw802jhrqvsx")))

