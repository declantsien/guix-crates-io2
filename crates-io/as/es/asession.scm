(define-module (crates-io as es asession) #:use-module (crates-io))

(define-public crate-asession-0.1.0 (c (n "asession") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cookie_store") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "reqwest_cookie_store") (r "^0.5") (d #t) (k 0)))) (h "17lyqbzygk5p9rz17mic7mf5hpcx7nawl9aalxra29zyxn96x239")))

(define-public crate-asession-0.1.1 (c (n "asession") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cookie_store") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "reqwest_cookie_store") (r "^0.6.0") (d #t) (k 0)))) (h "14320hj5vi1jr2zf5akzbj33ib197g9s7svmpk3qqyjjxhk4k82v")))

(define-public crate-asession-0.1.2 (c (n "asession") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cookie_store") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "reqwest_cookie_store") (r "^0.6.0") (d #t) (k 0)))) (h "1dbc4pag4bwgza5vywsy6ks5yjc3a0q8yza9kj234nwqyd3n6j9m")))

