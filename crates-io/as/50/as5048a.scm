(define-module (crates-io as #{50}# as5048a) #:use-module (crates-io))

(define-public crate-as5048a-0.1.0 (c (n "as5048a") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 0)))) (h "0bmf07hvmf1a5qm8s8nss0r2p0lidb48x7hrrvshqz7k92d6siqg")))

(define-public crate-as5048a-0.2.0 (c (n "as5048a") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 0)))) (h "0f3h8pwyvmnpnkw6pyqca6szjm6rs3pxicvy9frdai4w7sd5sm90")))

(define-public crate-as5048a-0.2.1 (c (n "as5048a") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "0zqncl1bipfx504yznpmz0aspv6m7gh407fzmrrqigipsrajz1m3")))

