(define-module (crates-io as n1 asn1_der) #:use-module (crates-io))

(define-public crate-asn1_der-0.5.0 (c (n "asn1_der") (v "0.5.0") (d (list (d (n "etrace") (r "^0.2.7") (d #t) (k 0)))) (h "1x72kbwyk5z94ffn4j82iw5hkn8jn6rbfry817cbvijxh06wgy71")))

(define-public crate-asn1_der-0.5.1 (c (n "asn1_der") (v "0.5.1") (d (list (d (n "etrace") (r "^0.2.7") (d #t) (k 0)))) (h "1z0r0axacv579zl2hgbznmq7k1rzxi5yxj6r73sm7403pga0q4qa")))

(define-public crate-asn1_der-0.5.2 (c (n "asn1_der") (v "0.5.2") (d (list (d (n "etrace") (r "^0.2.7") (d #t) (k 0)))) (h "1d4831ad5v380japzb9kvz1d1y5900f6nk9z61zginxqq3h70bn0") (f (quote (("map") ("default"))))))

(define-public crate-asn1_der-0.5.3 (c (n "asn1_der") (v "0.5.3") (d (list (d (n "etrace") (r "^0.2.7") (d #t) (k 0)))) (h "1js39ahk55ls8ag5d5kzx47wx3nmx1zv94c79py0ybdlkz7wlc4z") (f (quote (("map") ("default"))))))

(define-public crate-asn1_der-0.5.4 (c (n "asn1_der") (v "0.5.4") (d (list (d (n "etrace") (r "^0.2.7") (d #t) (k 0)))) (h "10ac7ls9378xhys6ngpq7ybcdcsr09zzs8zcf4x2dqykwjcw3ppr") (f (quote (("map") ("default"))))))

(define-public crate-asn1_der-0.5.5 (c (n "asn1_der") (v "0.5.5") (d (list (d (n "etrace") (r "^1.0.0") (d #t) (k 0)))) (h "0941ix6hcbzwdlh14cassd5m24fjpvmyxqcn3c3vgqwzbdbzl16x") (f (quote (("map") ("default"))))))

(define-public crate-asn1_der-0.5.6 (c (n "asn1_der") (v "0.5.6") (d (list (d (n "etrace") (r "^1.0.0") (d #t) (k 0)))) (h "149gdfd3208lw5m1d44sl5mwwrr2l0hhpcvq7fh7dzm18arah81d") (f (quote (("map") ("default"))))))

(define-public crate-asn1_der-0.5.7 (c (n "asn1_der") (v "0.5.7") (d (list (d (n "etrace") (r "^1.1.0") (d #t) (k 0)))) (h "1gfv7r0fhln42azin4dmd4jsmwvyimrq3160zjm2g4lqhbaaaya7") (f (quote (("map") ("default"))))))

(define-public crate-asn1_der-0.5.8 (c (n "asn1_der") (v "0.5.8") (d (list (d (n "etrace") (r "^1.1.0") (d #t) (k 0)))) (h "0knp2fng39skhl8nja1xp4zmzg90qpn84qrl5f41dm5kin9d4i7y") (f (quote (("map") ("default"))))))

(define-public crate-asn1_der-0.5.9 (c (n "asn1_der") (v "0.5.9") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)))) (h "0rkh43c8k64g6ak4m063mr9fv8jz6c5nj1f989n9wdjl7ajzv0vv") (f (quote (("map") ("default"))))))

(define-public crate-asn1_der-0.5.10 (c (n "asn1_der") (v "0.5.10") (d (list (d (n "etrace") (r "^1.1.1") (d #t) (k 0)))) (h "11jq4dql4g5y0svaafrn5jbsmqqb6rgc3jg4mghmvhfpqv2zsskn") (f (quote (("map") ("default"))))))

(define-public crate-asn1_der-0.6.0 (c (n "asn1_der") (v "0.6.0") (d (list (d (n "asn1_der_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0viksi7ydl7bnbw77i8an0zs7pmrk0wvasn69aa7ky754yk9akss") (f (quote (("derive" "asn1_der_derive") ("default" "derive"))))))

(define-public crate-asn1_der-0.6.1 (c (n "asn1_der") (v "0.6.1") (d (list (d (n "asn1_der_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0a353ffmgzaxva1jzj5xnfvdh9rzlcv6ink7wqql5i5iqczxd4wq") (f (quote (("derive" "asn1_der_derive") ("default" "derive"))))))

(define-public crate-asn1_der-0.6.2 (c (n "asn1_der") (v "0.6.2") (d (list (d (n "asn1_der_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "06mgdsibz5pwxjcl0q6f1yhij8h2rb0x376amwizxc9k2n40x95y") (f (quote (("derive" "asn1_der_derive") ("default" "derive"))))))

(define-public crate-asn1_der-0.6.3 (c (n "asn1_der") (v "0.6.3") (d (list (d (n "asn1_der_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0f6nk2zhj1dryrpmd9gywliwblp880zrxrw75kcfpbzx1xm6pkkg") (f (quote (("derive" "asn1_der_derive") ("default" "derive"))))))

(define-public crate-asn1_der-0.7.0 (c (n "asn1_der") (v "0.7.0") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1anz36m89p8b0w552xjrjrs7fb8ik1zqn7nwh2bfynr0yy1gzhr5") (f (quote (("no_std") ("no_panic" "no-panic") ("native_types") ("default" "native_types")))) (y #t)))

(define-public crate-asn1_der-0.7.1 (c (n "asn1_der") (v "0.7.1") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0b25b763788ip307rbqs9yhvqfzlxbi4kv0vr1i296ccx00445kr") (f (quote (("no_std") ("no_panic" "no-panic") ("native_types") ("default" "native_types"))))))

(define-public crate-asn1_der-0.7.2 (c (n "asn1_der") (v "0.7.2") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "182r2rqjs7h7id0w11ajqkk61f1i129zyyagmnzlbzm3s6bjaan1") (f (quote (("no_std") ("no_panic" "no-panic") ("native_types") ("default" "native_types"))))))

(define-public crate-asn1_der-0.7.3 (c (n "asn1_der") (v "0.7.3") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dadfm5yy2yz74bfbyccfa7fsxqfl6i078ykh8f9583s1nd04r05") (f (quote (("no_std") ("no_panic" "no-panic") ("native_types") ("default" "native_types"))))))

(define-public crate-asn1_der-0.7.4 (c (n "asn1_der") (v "0.7.4") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1hvz3f5g530gw9jkypcvvg1bvr2kn2zp2qn492wm6379rk928vlx") (f (quote (("no_std") ("no_panic" "no-panic") ("native_types") ("default" "native_types"))))))

(define-public crate-asn1_der-0.7.5 (c (n "asn1_der") (v "0.7.5") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08aawbxps19a4cqgq86yhxsigb2z05491p4rgh18lacci15iybg2") (f (quote (("no_std") ("no_panic" "no-panic") ("native_types") ("default" "native_types"))))))

(define-public crate-asn1_der-0.7.6 (c (n "asn1_der") (v "0.7.6") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0iy22nmh8x21xmzbgdx54ybrw2lk7la1b2mqqxxbgij2bqc5lnhm") (f (quote (("std") ("no_panic" "no-panic") ("native_types") ("default" "std" "native_types"))))))

