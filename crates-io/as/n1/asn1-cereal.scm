(define-module (crates-io as n1 asn1-cereal) #:use-module (crates-io))

(define-public crate-asn1-cereal-0.1.0 (c (n "asn1-cereal") (v "0.1.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "0ma7fpwhxwh9xmdlasscb6ycgq3kgzh1wp4xradx7l7jjwl0j58p") (f (quote (("default"))))))

(define-public crate-asn1-cereal-0.2.0 (c (n "asn1-cereal") (v "0.2.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)))) (h "06cil70hdh048ynwalzdl883b1qdn5ihhbrj8isv5wm7z0awzq34") (f (quote (("default"))))))

