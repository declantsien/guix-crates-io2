(define-module (crates-io as n1 asn1-rs-derive) #:use-module (crates-io))

(define-public crate-asn1-rs-derive-0.1.0 (c (n "asn1-rs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1gzf9vab06lk0zjvbr07axx64fndkng2s28bnj27fnwd548pb2yv")))

(define-public crate-asn1-rs-derive-0.2.0 (c (n "asn1-rs-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0iv2vs9xmmnwabizayp9mia0grblp0dx3x0yavnlni1fd8mnzc9d")))

(define-public crate-asn1-rs-derive-0.3.0 (c (n "asn1-rs-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1b9cmvhdhi2f2c4p8wam0x3rwvnacbxlc2dfldvh3albbr0h543b")))

(define-public crate-asn1-rs-derive-0.4.0 (c (n "asn1-rs-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0v7fgmnzk7jjxv51grhwzcx5bf167nlqwk3vcmq7xblf5s4karbj")))

(define-public crate-asn1-rs-derive-0.5.0 (c (n "asn1-rs-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "1hdabswlsqwysmb1kpyyl2nvi67z1gzxxba4lycnx5kiymgmfy3k")))

