(define-module (crates-io as n1 asn1-rs-impl) #:use-module (crates-io))

(define-public crate-asn1-rs-impl-0.1.0 (c (n "asn1-rs-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1va27bn7qxqp4wanzjlkagnynv6jnrhnwmcky2ahzb1r405p6xr7")))

(define-public crate-asn1-rs-impl-0.2.0 (c (n "asn1-rs-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xv56m0wrwix4av3w86sih1nsa5g1dgfz135lz1qdznn5h60a63v")))

