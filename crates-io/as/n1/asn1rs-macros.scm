(define-module (crates-io as n1 asn1rs-macros) #:use-module (crates-io))

(define-public crate-asn1rs-macros-0.2.0-alpha1 (c (n "asn1rs-macros") (v "0.2.0-alpha1") (d (list (d (n "asn1rs-model") (r "^0.2.0-alpha1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0d6lr4snacn6pc7aaigccg67gqm1r7ki9hzgadpf8jmm4347knza") (f (quote (("default") ("debug-proc-macro"))))))

(define-public crate-asn1rs-macros-0.2.0-alpha2 (c (n "asn1rs-macros") (v "0.2.0-alpha2") (d (list (d (n "asn1rs-model") (r "^0.2.0-alpha2") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1sc7hi0ly4rkz6bqnv600h7ykqk1grc7d8ka7j2fayr463gnfdv9") (f (quote (("default") ("debug-proc-macro"))))))

(define-public crate-asn1rs-macros-0.2.0-alpha3 (c (n "asn1rs-macros") (v "0.2.0-alpha3") (d (list (d (n "asn1rs-model") (r "^0.2.0-alpha3") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0zbyrwk1l0qdlpm2mnf49vvg7yqmcwrr7gbki7qmmq136n94dgbm") (f (quote (("default") ("debug-proc-macro"))))))

(define-public crate-asn1rs-macros-0.2.0 (c (n "asn1rs-macros") (v "0.2.0") (d (list (d (n "asn1rs-model") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1yjssgbq5jbr85m5y83w8lwcl4p9xjyr93mfw27mw87dkhhh0zar") (f (quote (("default") ("debug-proc-macro"))))))

(define-public crate-asn1rs-macros-0.2.1 (c (n "asn1rs-macros") (v "0.2.1") (d (list (d (n "asn1rs-model") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "16xyabqrgnh4ypbnzp11dp730csc44nbpdf6bfrxkf74fxfgxhhs") (f (quote (("default") ("debug-proc-macro"))))))

(define-public crate-asn1rs-macros-0.2.2 (c (n "asn1rs-macros") (v "0.2.2") (d (list (d (n "asn1rs-model") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0nnsf3kc5g97dc552r4fyigq029liz3mg8v2jiilpygcc5wnjb6m") (f (quote (("default") ("debug-proc-macro"))))))

(define-public crate-asn1rs-macros-0.3.0 (c (n "asn1rs-macros") (v "0.3.0") (d (list (d (n "asn1rs-model") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1876gjlrjhhmbwgdwjlf4hv4rymag8b06qbbjmkdw3cldpjcj70h") (f (quote (("default") ("debug-proc-macro"))))))

(define-public crate-asn1rs-macros-0.3.1 (c (n "asn1rs-macros") (v "0.3.1") (d (list (d (n "asn1rs-model") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0k9w9gxp4rlk67p076xjb3rh8q8yx2wzg8w6hj2rxhdyhwyl653l") (f (quote (("default") ("debug-proc-macro"))))))

