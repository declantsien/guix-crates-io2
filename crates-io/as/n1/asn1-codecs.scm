(define-module (crates-io as n1 asn1-codecs) #:use-module (crates-io))

(define-public crate-asn1-codecs-0.1.1 (c (n "asn1-codecs") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ii597mfnzlvpms1rrs1y7b6yhq7vqa2s1rihqxqmgnn5jw0hkwj")))

(define-public crate-asn1-codecs-0.1.2 (c (n "asn1-codecs") (v "0.1.2") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f7ld751gscr2jbs302i23064afwkdsw5n23x9ah8syfzgjssv1d")))

(define-public crate-asn1-codecs-0.1.3 (c (n "asn1-codecs") (v "0.1.3") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17jw8snv3agysz76b57ik1cygqp1hmkvbc6k3igrh8gjcizd7dn6")))

(define-public crate-asn1-codecs-0.2.0 (c (n "asn1-codecs") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a1fk67a11a9ibk23w5w8ym02fb4rkqav8xkbqprhpimjjzk5nqd")))

(define-public crate-asn1-codecs-0.3.0 (c (n "asn1-codecs") (v "0.3.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y1zgcm1qpq61sgx9majrxi0y73zzc004mvyjnvkiycdxj6nvw7d")))

(define-public crate-asn1-codecs-0.3.1 (c (n "asn1-codecs") (v "0.3.1") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00slbfzj322753x5k7yx6jypik1qh508l01idd932w3kvsna72kn")))

(define-public crate-asn1-codecs-0.4.0 (c (n "asn1-codecs") (v "0.4.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f72g8ily2140gj2kl8kaa1jyb9hj9n1lpqzcxb84vbhalck8n9y")))

(define-public crate-asn1-codecs-0.4.1 (c (n "asn1-codecs") (v "0.4.1") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q0zx7m9j69pfh4p9mbimkx09rdqj7am6nphf59w7gyv1zb0rchh")))

(define-public crate-asn1-codecs-0.5.0 (c (n "asn1-codecs") (v "0.5.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ziwyscw1n6yj6ifabiy8b3i9fc2f516wk4apvvfpzab3962r8xm")))

(define-public crate-asn1-codecs-0.5.1 (c (n "asn1-codecs") (v "0.5.1") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jjwvi01mazmaf993n1ixzdiila2pqgxm4y9jmnpddyzmddcm608")))

(define-public crate-asn1-codecs-0.5.2 (c (n "asn1-codecs") (v "0.5.2") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0l74mnlxqxmhnzv3c8l7q0z4ihjbm1mybz06prajxjfx1ci01j1z")))

(define-public crate-asn1-codecs-0.5.3 (c (n "asn1-codecs") (v "0.5.3") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mnk262dkb8irpyw7s663sy7rz75466fffsba0rhl020l82m2wac")))

(define-public crate-asn1-codecs-0.5.4 (c (n "asn1-codecs") (v "0.5.4") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0icbk9kphfvp356kq9hgmkaaizg84x9p8laz3jiyzfgzbngh1c59")))

(define-public crate-asn1-codecs-0.5.5 (c (n "asn1-codecs") (v "0.5.5") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06xknlf5y7zd0flxavg6x1alaj5p1kbv3v3yk1msr9l91607gjwk")))

(define-public crate-asn1-codecs-0.5.6 (c (n "asn1-codecs") (v "0.5.6") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1msfzzz94fvhqzvsrllm3xfy6wqq67l4r8bc72dilkdgl9sczdxi")))

(define-public crate-asn1-codecs-0.5.7 (c (n "asn1-codecs") (v "0.5.7") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1glilk4xvcqa64v75y5zx22sa5h4w4i09na7g4pfmpkqxh2h7dq9")))

(define-public crate-asn1-codecs-0.5.8 (c (n "asn1-codecs") (v "0.5.8") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h5mkdh57fc5ardjqw4gayvdd0wmdlpc5fqaf7v8vapj1x4cxnc6")))

(define-public crate-asn1-codecs-0.5.9 (c (n "asn1-codecs") (v "0.5.9") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wndjfmnsnf2znrb6n4l58qnvgd8yqjgg8aygk9z2ray0qx69h45")))

(define-public crate-asn1-codecs-0.6.0 (c (n "asn1-codecs") (v "0.6.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k3j9hh7hbfchkk4b7n3x8wbl5mlv6ggcykpwy3r3vqf7y37jllm")))

(define-public crate-asn1-codecs-0.6.1 (c (n "asn1-codecs") (v "0.6.1") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hygvrjq7jmafa36spr8yyzgkxnhsd88z9vykxrkw8a70vzd2kjy")))

