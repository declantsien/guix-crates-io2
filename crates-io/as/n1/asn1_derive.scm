(define-module (crates-io as n1 asn1_derive) #:use-module (crates-io))

(define-public crate-asn1_derive-0.4.1 (c (n "asn1_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0jzzvxpci2389m4j0cks63ylsarkvc0lfg61vrgs80j4kifzsxff")))

(define-public crate-asn1_derive-0.4.2 (c (n "asn1_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0hfidxlzahypyrzap5cp8ymyd5n1z2d9n52i7ldpg9144lpx87si")))

(define-public crate-asn1_derive-0.5.0 (c (n "asn1_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "12f8jldbg6pcnbf46g19xl2ng0761sfv6m0ipbg3jnnnaylv3r2b")))

(define-public crate-asn1_derive-0.5.1 (c (n "asn1_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "00yjxy4x070qi08347rmm3z731dkccgw75h9yccyzdnxgpimq5qq")))

(define-public crate-asn1_derive-0.5.2 (c (n "asn1_derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0di580mhj68v4c0j2qs2rcia8hjddkc26vdhwd7h3gib14hbpca0")))

(define-public crate-asn1_derive-0.5.3 (c (n "asn1_derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0yghlqm4aaijbwpb4allnqwjdkgga79cq06fywl38dkfpmh36fmy")))

(define-public crate-asn1_derive-0.6.0 (c (n "asn1_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0nd87g7s732b40zm90gd10aqvmz3a8ldprda6mvk4j4mv665ja7q")))

(define-public crate-asn1_derive-0.6.1 (c (n "asn1_derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "15phvzn544n0sw000sax0dra03hhhpny6czk34bhym5rj1zlpz32")))

(define-public crate-asn1_derive-0.6.2 (c (n "asn1_derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0y83g3igdwzl7l1lmmx6yymxajajsgls5xyrsdjyjab73m8j1cs8")))

(define-public crate-asn1_derive-0.6.3 (c (n "asn1_derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1cv2l2abysc5k5jwibl2g7v3i7q5ld5w51m958n9iq2iayy07aa0")))

(define-public crate-asn1_derive-0.6.4 (c (n "asn1_derive") (v "0.6.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0gfc3svgiqkypks40pzdvflsd0nsww4kfjn0yplwljk4vd1hychh")))

(define-public crate-asn1_derive-0.6.5 (c (n "asn1_derive") (v "0.6.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0mphd0szvng9ipk1hqwl0i8rr2vblmb252n6n8iy5clwrgra8phd")))

(define-public crate-asn1_derive-0.7.0 (c (n "asn1_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0fgrbx46zznvg3zkxfnkr8g855pjbxssyda683lpy076di3s30nf")))

(define-public crate-asn1_derive-0.8.0 (c (n "asn1_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0zlqgrbmlk5603n7h35gcm0aw0i8953m1cik950lmzyx3mfbas5v")))

(define-public crate-asn1_derive-0.8.1 (c (n "asn1_derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1lsbncfq83z8yzawpxd1ij5qhy0fkpk3y1pl0yk46885nsscr54n")))

(define-public crate-asn1_derive-0.8.2 (c (n "asn1_derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0n07gibxw4l1p9chilj6rc36j1432rp1sb525ibfqdpyzcjancn9")))

(define-public crate-asn1_derive-0.8.3 (c (n "asn1_derive") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0r27303gqq9dxxcbk1kwd9139ina92fyhb3djs3s97cf8yjxds5f")))

(define-public crate-asn1_derive-0.8.4 (c (n "asn1_derive") (v "0.8.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1mxzlfa9a40akdr7sr7wj95snxk020zzsf59ypf1a582qqxdn79b")))

(define-public crate-asn1_derive-0.8.5 (c (n "asn1_derive") (v "0.8.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "18b9cf08k5p0af1rhg222b62gzgg926imyjr62pkxislnzvgbxr0")))

(define-public crate-asn1_derive-0.8.6 (c (n "asn1_derive") (v "0.8.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "18gsvfdwpzdqm284429kwr4fdza3dhas1kg56s7mzy7kafp6yjnd")))

(define-public crate-asn1_derive-0.8.7 (c (n "asn1_derive") (v "0.8.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "098w0mxz4bx9w7v72gsl5wva6f0qbvzyc52m0s0n8svqbyh4z2dw")))

(define-public crate-asn1_derive-0.9.0 (c (n "asn1_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "11qpmwy6nny1607v9y0y2jd8wxcb8c821zgim4xjvyvckj4fybaz")))

(define-public crate-asn1_derive-0.9.1 (c (n "asn1_derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1b94rmhqw6l5rbli7chp09jfx8vaxmcvl8ag0ljhyzrkx7vlvpqb")))

(define-public crate-asn1_derive-0.10.0 (c (n "asn1_derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0qv19a18pln0bx0npjrvvgb51l153hjv2nx7g2zysjrb7dz0jz5h")))

(define-public crate-asn1_derive-0.11.0 (c (n "asn1_derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "18z2vwl116nx6jdjlna2nr5hifyjkcq38jmpqmrph97b8z9fkp11")))

(define-public crate-asn1_derive-0.12.0 (c (n "asn1_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0nhqiqm9j14yvplslmpzvmy2jbvn96aq31ldsphyzfq6l3v7pgmj")))

(define-public crate-asn1_derive-0.12.1 (c (n "asn1_derive") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1sl7y0iawyn34hh88wmhny6az4w51rf643qlbd2pwc9qbqajyyan")))

(define-public crate-asn1_derive-0.12.2 (c (n "asn1_derive") (v "0.12.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1dz130zdi1k8azsp45vfjk2ncbrkvkif8nlg5z9fhh8jl1a1i3fl")))

(define-public crate-asn1_derive-0.12.3 (c (n "asn1_derive") (v "0.12.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1nr3125idrpcmf45szad24y2986y2djrirpqfyjyf35r4shqv86r")))

(define-public crate-asn1_derive-0.13.0 (c (n "asn1_derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1bvqriazb23gysygpzng1dhzjgnlv274q2yj5gpmlpl7jp0pkaxz")))

(define-public crate-asn1_derive-0.14.0 (c (n "asn1_derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0yk1jss80csywf6n0nab5kgag5xlkc49j9gzidbcjk06rjyrlkgf") (r "1.56.0")))

(define-public crate-asn1_derive-0.15.0 (c (n "asn1_derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "10gvn0f5z28iav3raarlzlf8zafg06jrpdni6ji9q18s5f5mqdm6") (r "1.56.0")))

(define-public crate-asn1_derive-0.15.1 (c (n "asn1_derive") (v "0.15.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "108w4hvh5b5pgghy9dnl6dq64iangnb34h25mp81i5b1cgaw893i") (r "1.56.0")))

(define-public crate-asn1_derive-0.15.2 (c (n "asn1_derive") (v "0.15.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "01wlcgafzdlrcc2qnscjfrp2jigngcdczrnidfl49wl9mp6c6id0") (r "1.56.0")))

(define-public crate-asn1_derive-0.15.3 (c (n "asn1_derive") (v "0.15.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1h99vwy8jq9zbq6xzwmh5399m1485xv6040m4drfs74ppj4c23zv") (r "1.56.0")))

(define-public crate-asn1_derive-0.15.4 (c (n "asn1_derive") (v "0.15.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0brlwyjr69fnsifaxyn2js73j4hxjqxsg9xpra1dkp9244da4vgw") (r "1.56.0")))

(define-public crate-asn1_derive-0.15.5 (c (n "asn1_derive") (v "0.15.5") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1771kfjycjs4g2acqvxpjy3igfcgg8hychczl1lsqq64za4gj6l6") (r "1.56.0")))

(define-public crate-asn1_derive-0.16.0 (c (n "asn1_derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "00y67phmr438fzmism3isw66ws3n69r8q87jvv498dyblchj44w7") (r "1.57.0")))

(define-public crate-asn1_derive-0.16.1 (c (n "asn1_derive") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "16dak48m8zy2fv291dlnz58kym9w11kigqmrqfwhj043kgn1q9z2") (r "1.57.0")))

