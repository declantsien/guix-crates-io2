(define-module (crates-io as n1 asn1obj_codegen) #:use-module (crates-io))

(define-public crate-asn1obj_codegen-0.1.0 (c (n "asn1obj_codegen") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1bbmvzb2l0ywm33rbmvj7sp2grvqrss8qlc6m0qyndvw5llcm2ja") (r "1.59.0")))

