(define-module (crates-io as n1 asn1) #:use-module (crates-io))

(define-public crate-asn1-0.1.0 (c (n "asn1") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "chrono") (r "*") (d #t) (k 0)))) (h "139g2261z1jdhg8q4zq5dsrk4abgmzzngfkp2bmijh31iq4spv6v")))

(define-public crate-asn1-0.3.0 (c (n "asn1") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "081vbzcz81k8czid6g7p1jzz7lvp0bkbck855998q207r6zyzlih") (f (quote (("std") ("default" "std") ("const-generics")))) (y #t)))

(define-public crate-asn1-0.3.1 (c (n "asn1") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "093dmabxfb54v3xwagkp9rhg59cmwhibmayfiz4zk8m146b3p339") (f (quote (("std") ("default" "std") ("const-generics")))) (y #t)))

(define-public crate-asn1-0.3.2 (c (n "asn1") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1x6zy6lxs6cr4jp48i03qcmmjjgq1pfvwxxx3ijgnbdg7ippb2hw") (f (quote (("std") ("default" "std") ("const-generics")))) (y #t)))

(define-public crate-asn1-0.3.3 (c (n "asn1") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0yl8y040j7fkda7dahbpbcndq57f9a47n1936jh1n1f7qfqb714c") (f (quote (("std") ("default" "std") ("const-generics")))) (y #t)))

(define-public crate-asn1-0.3.4 (c (n "asn1") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "07fljxdyjhy4k9kaxqx02krqi9l4pjhrkrn1zkm2nf4s4isla1pf") (f (quote (("std") ("default" "std") ("const-generics")))) (y #t)))

(define-public crate-asn1-0.3.5 (c (n "asn1") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1lfgibwr7x4vmzg4lfadldjndbipwgq8iihpxv10nl72syvdx2ps") (f (quote (("std") ("default" "std") ("const-generics"))))))

(define-public crate-asn1-0.3.6 (c (n "asn1") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0cfrcf87qc53di64jq2mhd1mfvhbjsjxn4csga2fmx1x20c0kzwq") (f (quote (("std") ("default" "std") ("const-generics"))))))

(define-public crate-asn1-0.3.7 (c (n "asn1") (v "0.3.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1a67wf9bh8cscwhagxbbg4cf6f602sis5xzlqa12clj4rmm7fwvf") (f (quote (("std") ("default" "std") ("const-generics"))))))

(define-public crate-asn1-0.3.8 (c (n "asn1") (v "0.3.8") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "05galqgv88x5dfxgvqw0h3m567yi6s4k5394jiy779a02v2jqaw4") (f (quote (("std") ("default" "std") ("const-generics"))))))

(define-public crate-asn1-0.4.1 (c (n "asn1") (v "0.4.1") (d (list (d (n "asn1_derive") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0hhlfgbpnjxg4ynm0raw3x69hks5jxj1ybfcx7zfph0yrlrwrgc5") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.4.2 (c (n "asn1") (v "0.4.2") (d (list (d (n "asn1_derive") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0r2qrbg7p3kcapl31vgmp2f9ykjvgh1b8w9pr0afi94l4dqyy3hm") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.5.0 (c (n "asn1") (v "0.5.0") (d (list (d (n "asn1_derive") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0c9c5g8bsgnkmqwmfbjm616v6if6707gyjlc9366hmzjwcv5s2rb") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.5.1 (c (n "asn1") (v "0.5.1") (d (list (d (n "asn1_derive") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0f8cs81ijqqg3wqrzpkamgzpxdyc3bavz7a4469y8x1n0wpyrqpl") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.5.2 (c (n "asn1") (v "0.5.2") (d (list (d (n "asn1_derive") (r "^0.5.2") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0np75saln702abq90h845vlmh6a2v4wi3rid3zpkj4ia10l4mvgq") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.5.3 (c (n "asn1") (v "0.5.3") (d (list (d (n "asn1_derive") (r "^0.5.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1q11pgsr8vp9fdps9yfyrwdvxab1s1c41fp1iqwxa66yjrvl2mjz") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.6.0 (c (n "asn1") (v "0.6.0") (d (list (d (n "asn1_derive") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1p60b0y1rbdn3b8ckrfr7aj4qrr3jydifcy27primjq9xa7s904n") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.6.1 (c (n "asn1") (v "0.6.1") (d (list (d (n "asn1_derive") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1zavjaqsfmhi455jpcz3pih1na6h6vz1wl8nzjcif7npkhy3mhd9") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.6.2 (c (n "asn1") (v "0.6.2") (d (list (d (n "asn1_derive") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1a0s8z46880grgdwjhl650gs4p845g69szsiq1q8yxakiz0frjgy") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.6.3 (c (n "asn1") (v "0.6.3") (d (list (d (n "asn1_derive") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0d9q4yxbxklq980jvy2pb0fj7s02lm0ppdchv3mxp6qgc5ccjicl") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.6.4 (c (n "asn1") (v "0.6.4") (d (list (d (n "asn1_derive") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1i0zxl0scgvf4fiizj3knc2yhjcpiq8aln2lxw29nahr6z2isg4r") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.6.5 (c (n "asn1") (v "0.6.5") (d (list (d (n "asn1_derive") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1mhk5jyi4550j1cmv68b1j6pp1yxg5h83ixv66kzfnrqsv98z16f") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.7.0 (c (n "asn1") (v "0.7.0") (d (list (d (n "asn1_derive") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1mw5slfy4wpmhpjxqr2nbrzigrd9yzkn2x2ggnlqk62bhnz08g8j") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.8.0 (c (n "asn1") (v "0.8.0") (d (list (d (n "asn1_derive") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1q0j171r5mx2hrj9z78lg52lsl48qmlvgkfqklrls8662svl2676") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.8.1 (c (n "asn1") (v "0.8.1") (d (list (d (n "asn1_derive") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "11pz4lil3gynkbb519a5lizvybyb22ds87dnjfy515d3xhnkdpig") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.8.2 (c (n "asn1") (v "0.8.2") (d (list (d (n "asn1_derive") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0wwzmszr5pgfg96vb8g8qy49dyxf02lcgps7kfcnq82qsdal0cd6") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.8.3 (c (n "asn1") (v "0.8.3") (d (list (d (n "asn1_derive") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1463cfy19bv61411hgbavppdim9xrg6qd75qcrw5mcsp4dha15vs") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.8.4 (c (n "asn1") (v "0.8.4") (d (list (d (n "asn1_derive") (r "^0.8.4") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "14kyy9641p04892idhnqn3zyb4w7514c2fsrvfy8vgwbz1cdajg9") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.8.5 (c (n "asn1") (v "0.8.5") (d (list (d (n "asn1_derive") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0zdgj14vll9pv3g00bpks4cprjrzzgcb4i6s6pg6d7602xd6nill") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.8.6 (c (n "asn1") (v "0.8.6") (d (list (d (n "asn1_derive") (r "^0.8.6") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "065cpqzas4czmyf4xrrnw65qx35khz9gfh7s76548jjxnqlrmj3h") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.8.7 (c (n "asn1") (v "0.8.7") (d (list (d (n "asn1_derive") (r "^0.8.7") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1caacmvgn463n1yc4ac6vl9phrh56ij7l3xgf6qgzbpyjm8v7zyg") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.9.0 (c (n "asn1") (v "0.9.0") (d (list (d (n "asn1_derive") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1rq20diik7fl1qvr70v1bandrqh9adridigaw4bmhyv7s5xmzwrg") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.9.1 (c (n "asn1") (v "0.9.1") (d (list (d (n "asn1_derive") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1hjghzgv3vg577m9b5mcvkdiy020ng4w2ilvnzs3pcp2ympdgq67") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.10.0 (c (n "asn1") (v "0.10.0") (d (list (d (n "asn1_derive") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0gbcrg9ja6s6w98bb4bamrlgvxzvmiyklhqwcma517bagz0ixk1j") (f (quote (("std") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.11.0 (c (n "asn1") (v "0.11.0") (d (list (d (n "asn1_derive") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.20") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "1lvp3a9zm040pkmxaszy7dmqlgpya5s66pdi4hpsp728yg9mj2aa") (f (quote (("std") ("fallible-allocations") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.12.0 (c (n "asn1") (v "0.12.0") (d (list (d (n "asn1_derive") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.20") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "112b29gm24v7dlnnnfp8gbaww7ddgpspclvxk3xd5l13hyd8nrqm") (f (quote (("std") ("fallible-allocations") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.12.1 (c (n "asn1") (v "0.12.1") (d (list (d (n "asn1_derive") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.20") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "159h5j83sci9q3y888i7wsz59l1a1pgkmfdinvq9hzhj1msb83jp") (f (quote (("std") ("fallible-allocations") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.12.2 (c (n "asn1") (v "0.12.2") (d (list (d (n "asn1_derive") (r "^0.12.2") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.20") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "047j8hsbzzvgy4597h3pj9wxzlkk5dssmhf7fd1vzhbirn2prhi2") (f (quote (("std") ("fallible-allocations") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.12.3 (c (n "asn1") (v "0.12.3") (d (list (d (n "asn1_derive") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.20") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "1b9b8d4i505ddzp87v6j7arkvmw04va8jvdgwqhywcaj980qjdx5") (f (quote (("std") ("fallible-allocations") ("derive" "asn1_derive") ("default" "std" "derive" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.13.0 (c (n "asn1") (v "0.13.0") (d (list (d (n "asn1_derive") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (f (quote ("alloc"))) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "1idxxw14h3dvrj72k4g0hx1aqigd986a00cg0yxfw2gfc9gbmzra") (f (quote (("std") ("fallible-allocations") ("default" "std" "const-generics") ("const-generics"))))))

(define-public crate-asn1-0.14.0 (c (n "asn1") (v "0.14.0") (d (list (d (n "asn1_derive") (r "^0.14.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "16jh9bkj2ys34qjcklrb4szzi7jbn7gg6wiw1cw3vr79rl14z8s8") (f (quote (("std") ("fallible-allocations") ("default" "std")))) (r "1.56.0")))

(define-public crate-asn1-0.15.0 (c (n "asn1") (v "0.15.0") (d (list (d (n "asn1_derive") (r "^0.15.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "1va1wnlf0aiwchj611cg3nd57adbl5i2mz7yv0yqsys0x2izarps") (f (quote (("std") ("fallible-allocations") ("default" "std")))) (r "1.56.0")))

(define-public crate-asn1-0.15.1 (c (n "asn1") (v "0.15.1") (d (list (d (n "asn1_derive") (r "^0.15.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "0lsm93smxawfvya4adngcd1xn7vq7a2ng30749lm3kw2wf2gqgyy") (f (quote (("std") ("fallible-allocations") ("default" "std")))) (r "1.56.0")))

(define-public crate-asn1-0.15.2 (c (n "asn1") (v "0.15.2") (d (list (d (n "asn1_derive") (r "^0.15.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "0wxrpaajvzmi5mf3vgqdvq4iny8jhc4ziq47cidq2nyy4j9rph98") (f (quote (("std") ("fallible-allocations") ("default" "std")))) (r "1.56.0")))

(define-public crate-asn1-0.15.3 (c (n "asn1") (v "0.15.3") (d (list (d (n "asn1_derive") (r "^0.15.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "0ym8d5dkg8w6n0kn5mn4y8s0wzpjdbg9yii5haxa1afy0gwnhib3") (f (quote (("std") ("fallible-allocations") ("default" "std")))) (r "1.56.0")))

(define-public crate-asn1-0.15.4 (c (n "asn1") (v "0.15.4") (d (list (d (n "asn1_derive") (r "^0.15.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "14z5zrw3v75rc8rkhg3np1v08ax23cz2f7n4amwnsdyfmnr4ynfy") (f (quote (("std") ("fallible-allocations") ("default" "std")))) (r "1.56.0")))

(define-public crate-asn1-0.15.5 (c (n "asn1") (v "0.15.5") (d (list (d (n "asn1_derive") (r "^0.15.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "1nzg1gjiyfvpvrf3i7i8j21165snf5livqg6x2sjf9m2i77cngmf") (f (quote (("std") ("fallible-allocations") ("default" "std")))) (r "1.56.0")))

(define-public crate-asn1-0.16.0 (c (n "asn1") (v "0.16.0") (d (list (d (n "asn1_derive") (r "^0.16.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "02px246fgjkh8l3dzyh6pkkacw6n2s6rawbwfidrhwixhjcxa9x2") (f (quote (("std") ("default" "std")))) (r "1.57.0")))

(define-public crate-asn1-0.16.1 (c (n "asn1") (v "0.16.1") (d (list (d (n "asn1_derive") (r "^0.16.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 2)))) (h "0s2ky345g1bwdpza79d2ncrgilfarh2ycab5j8clcd61ss7xr6l8") (f (quote (("std") ("default" "std")))) (r "1.57.0")))

