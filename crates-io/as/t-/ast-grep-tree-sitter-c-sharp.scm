(define-module (crates-io as t- ast-grep-tree-sitter-c-sharp) #:use-module (crates-io))

(define-public crate-ast-grep-tree-sitter-c-sharp-0.20.0 (c (n "ast-grep-tree-sitter-c-sharp") (v "0.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "0f25q05yryig36nlmqyfsr4d3pssysyp1sr8x22q1r3d02pngfv9")))

