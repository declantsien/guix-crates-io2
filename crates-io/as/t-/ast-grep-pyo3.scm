(define-module (crates-io as t- ast-grep-pyo3) #:use-module (crates-io))

(define-public crate-ast-grep-pyo3-0.12.5 (c (n "ast-grep-pyo3") (v "0.12.5") (d (list (d (n "pyo3") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "0ksf938k6r7098xnl2hhvvfx7qbdwb0y2ancz9v2v9cmblipd3y2") (r "1.63")))

(define-public crate-ast-grep-pyo3-0.13.0 (c (n "ast-grep-pyo3") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "ast-grep-config") (r "^0.13.0") (d #t) (k 0)) (d (n "ast-grep-core") (r "^0.13.0") (d #t) (k 0)) (d (n "ast-grep-language") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("anyhow"))) (o #t) (d #t) (k 0)) (d (n "pythonize") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "05s4nfywxwa2173ib3y5lbr5ch7w0rr402pivyna8pxapkbfcj9s") (f (quote (("python" "pythonize" "pyo3" "pyo3/extension-module")))) (r "1.63")))

