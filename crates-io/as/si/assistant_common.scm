(define-module (crates-io as si assistant_common) #:use-module (crates-io))

(define-public crate-assistant_common-0.1.1 (c (n "assistant_common") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1fr633vv8g7b14ycy6rp63algmfdwlqv0gkdwbp4a2gi1z4yfc6s")))

