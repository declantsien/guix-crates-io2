(define-module (crates-io as si assist-sys) #:use-module (crates-io))

(define-public crate-assist-sys-0.1.0 (c (n "assist-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "rebound-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1208wcp8x08nmajcwbqjsw2z73d8dqm0327ikwnp73isi2glhqqz") (l "assist")))

(define-public crate-assist-sys-0.2.0 (c (n "assist-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "rebound-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0hnr44vjq63z8lmvwciplcpyhw58fyx2k8l2angdqxvi1kfnsf7l") (l "assist")))

