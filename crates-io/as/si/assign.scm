(define-module (crates-io as si assign) #:use-module (crates-io))

(define-public crate-assign-1.0.0 (c (n "assign") (v "1.0.0") (h "1080z3hc2brprd9jcm0z46ij1dwr7k1g7jrhkjbb0vprpg7ddpla")))

(define-public crate-assign-1.0.1 (c (n "assign") (v "1.0.1") (h "114jym8yj98l9fw8qhvslzgvpgjq9kcx7bz88f90ayvnggrjx03l")))

(define-public crate-assign-1.1.0 (c (n "assign") (v "1.1.0") (h "0rqx3cj0r3cpfpxqaqsxbrg36gin1mgarjhlxxq5xv1swdznixaa")))

(define-public crate-assign-1.1.1 (c (n "assign") (v "1.1.1") (h "00h0r04iyyhhnjr8fwkmw3fpvpd41bn9x1dz8s9j5kdyg3nkw2az")))

