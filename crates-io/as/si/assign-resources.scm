(define-module (crates-io as si assign-resources) #:use-module (crates-io))

(define-public crate-assign-resources-0.1.0 (c (n "assign-resources") (v "0.1.0") (h "1lbrs3541rb2zyz6yv68pf388i9dz2ag64pz18x62yk0dr63xq6r")))

(define-public crate-assign-resources-0.2.0 (c (n "assign-resources") (v "0.2.0") (h "0yf1hygbklfbp3cjc3ndk1wc2x83ifmfvlbvcfh1p0rn84819si7")))

(define-public crate-assign-resources-0.3.0 (c (n "assign-resources") (v "0.3.0") (h "1g8w0lwyksxaqm57ingm87i0hmywrb9wqahr12sdn6zkyfpw9dv3")))

(define-public crate-assign-resources-0.4.0 (c (n "assign-resources") (v "0.4.0") (h "0wz1m6plkzdw1nn1y1fpivq9hdr01qhyym51i8xgd6p9zx0vv7dc")))

(define-public crate-assign-resources-0.4.1 (c (n "assign-resources") (v "0.4.1") (h "11bznwwn540p8zcb02wqmfnxyiv47fys3dl4s1qw3x94jq8wa3vc")))

