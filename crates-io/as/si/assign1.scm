(define-module (crates-io as si assign1) #:use-module (crates-io))

(define-public crate-assign1-0.1.0 (c (n "assign1") (v "0.1.0") (h "0pcgrfmlnzcp8zcrxsldzsd4mv194mv624k1wx8snx9313sd2yn4")))

(define-public crate-assign1-0.1.1 (c (n "assign1") (v "0.1.1") (h "0w7l6dn58fjl8rmj2n6g2z4iasdllq7r38i2vsfi21c4sk2liw4r")))

