(define-module (crates-io as si assimp) #:use-module (crates-io))

(define-public crate-assimp-0.0.1 (c (n "assimp") (v "0.0.1") (d (list (d (n "assimp-sys") (r "*") (d #t) (k 0)) (d (n "cgmath") (r "*") (d #t) (k 2)) (d (n "glium") (r "*") (d #t) (k 2)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 2)))) (h "0qaagjmchhznr4fy7713fzrnkgj31bnwjjyr03075c0ighww4wln")))

(define-public crate-assimp-0.0.3 (c (n "assimp") (v "0.0.3") (d (list (d (n "assimp-sys") (r "*") (d #t) (k 0)) (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "glium") (r "*") (d #t) (k 2)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1m5vf7rp9cj01rbf1rvn3fmwnhd71xcd2zwq4fq0xh9zi24agnh0")))

(define-public crate-assimp-0.0.4 (c (n "assimp") (v "0.0.4") (d (list (d (n "assimp-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "cgmath") (r "^0.1.5") (d #t) (k 0)) (d (n "glium") (r "^0.3.7") (d #t) (k 2)) (d (n "glutin") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "1wwqp48fn1a0v7r3kqkvmklmrnswmcsnn934251sshbs5h1gpncj")))

(define-public crate-assimp-0.0.5 (c (n "assimp") (v "0.0.5") (d (list (d (n "assimp-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "cgmath") (r "^0.1.5") (d #t) (k 0)) (d (n "glium") (r "^0.3.7") (d #t) (k 2)) (d (n "glutin") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "0dnwimv04rm2q70ga92w3nagydah907sxs79ygdkz7grq7jmzgk2")))

(define-public crate-assimp-0.0.6 (c (n "assimp") (v "0.0.6") (d (list (d (n "assimp-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "cgmath") (r "^0.1.5") (d #t) (k 0)) (d (n "glium") (r "^0.3.7") (d #t) (k 2)) (d (n "glutin") (r "^0.1.4") (d #t) (k 2)) (d (n "libc") (r "^0.1.7") (d #t) (k 0)))) (h "05mpnzfx2728yqlwh9fzxpxvl5i26mw513fzgqgzfjmbxwxja3nf")))

(define-public crate-assimp-0.0.7 (c (n "assimp") (v "0.0.7") (d (list (d (n "assimp-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "cgmath") (r "^0.2.0") (d #t) (k 0)) (d (n "glium") (r "^0.9.1") (d #t) (k 2)))) (h "156vnnkc0sllfaly5ard0ayyx9jfch805gh28z7fmzd8rh0crlb7")))

(define-public crate-assimp-0.0.8 (c (n "assimp") (v "0.0.8") (d (list (d (n "assimp-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "cgmath") (r "^0.4.0") (d #t) (k 0)) (d (n "glium") (r "^0.11.0") (d #t) (k 2)))) (h "04sirl96sbx3cajxqz3jxl4dji0481hqc7p0g7i2rkcryva5m7si")))

(define-public crate-assimp-0.1.0 (c (n "assimp") (v "0.1.0") (d (list (d (n "assimp-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "cgmath") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.5.0") (d #t) (k 2)) (d (n "glium") (r "^0.12.3") (d #t) (k 2)))) (h "0992pkgp74zx4328vj8g535kdqvb0cpi38b8a9xvhpn2vwljzpiv")))

(define-public crate-assimp-0.2.0 (c (n "assimp") (v "0.2.0") (d (list (d (n "assimp-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "cgmath") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.12.0") (d #t) (k 2)) (d (n "glium") (r "^0.15.0") (d #t) (k 2)))) (h "03jz3d4xzkgib2jc0bhd8ws039wnhcmyxnn02vywgxi9sjal21z8")))

(define-public crate-assimp-0.2.1 (c (n "assimp") (v "0.2.1") (d (list (d (n "assimp-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "cgmath") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.12.0") (d #t) (k 2)) (d (n "glium") (r "^0.15.0") (d #t) (k 2)))) (h "10kyzxk3ax2wd8vypvb0vjx6br77n14pqrn0ly0l2mg8zi68spr7")))

(define-public crate-assimp-0.3.0 (c (n "assimp") (v "0.3.0") (d (list (d (n "assimp-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.12.0") (d #t) (k 2)) (d (n "glium") (r "^0.15.0") (d #t) (k 2)))) (h "0n7sdyibpwfg3xwmdpkkg0882lim87w3ayjqa2d8pz31ma4y0hpp")))

(define-public crate-assimp-0.3.1 (c (n "assimp") (v "0.3.1") (d (list (d (n "assimp-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "cgmath") (r "^0.15.0") (d #t) (k 2)) (d (n "glium") (r "^0.18.0") (d #t) (k 2)))) (h "0zyqg7m2jsdwbai6lf2qqbi968nigmy90f0rd4wjjm0yq84k804a")))

