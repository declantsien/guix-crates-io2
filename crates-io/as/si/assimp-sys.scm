(define-module (crates-io as si assimp-sys) #:use-module (crates-io))

(define-public crate-assimp-sys-0.0.1 (c (n "assimp-sys") (v "0.0.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1x6bfxq69k1d8i857aqii33lw8g802xjxqp9p7xml5sd69wva083")))

(define-public crate-assimp-sys-0.0.2 (c (n "assimp-sys") (v "0.0.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0y1zm057cxxqnwc7191drhjsm8pf94mv3k3k7qs0f04g8ck3xjrv")))

(define-public crate-assimp-sys-0.0.3 (c (n "assimp-sys") (v "0.0.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0xdp9rgnz93zyfx79hh695ihil2aa3q2h1jxrllrjd734q9yj8lg")))

(define-public crate-assimp-sys-0.0.4 (c (n "assimp-sys") (v "0.0.4") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0swlnsn8lzc0wvq0fhlw10ivw5ylk8kbd7x6pv8lp8rnkhmxfzbn")))

(define-public crate-assimp-sys-0.0.5 (c (n "assimp-sys") (v "0.0.5") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09l5qarc7nf10p7jppxjpvanc2ribszzbbfy07x6fm7y4sqvmwi5")))

(define-public crate-assimp-sys-0.0.6 (c (n "assimp-sys") (v "0.0.6") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14wrdr6zjbl2v9xr6bv35x3zv6mw1j55pqw1ap916aqn133hpd0n") (y #t)))

(define-public crate-assimp-sys-0.0.7 (c (n "assimp-sys") (v "0.0.7") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)))) (h "04b4016bbg1iqfi5h3x22wfk3ymqa4vp2n4r0m7pc5a21pxs7fcn")))

(define-public crate-assimp-sys-0.1.0 (c (n "assimp-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1b8sr61j6rlnmpbx4hd0zlhyjpvifshvjrdqyq0pm28hjd7grj0d")))

(define-public crate-assimp-sys-0.2.0 (c (n "assimp-sys") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11aipx9840w3rz5ghlpassqjwxsnfjhx43kqwawysiaq0j6hfn3i")))

(define-public crate-assimp-sys-0.2.1 (c (n "assimp-sys") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07m50b4q0rgcdh2w7h69j5zdl3b6igxgfgvxlzcjx45dkwqdvzmf")))

(define-public crate-assimp-sys-0.2.2 (c (n "assimp-sys") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qr0hbklycx74w89y8nig1xgrkixvc821vy172j0cywm07vfiq6q")))

(define-public crate-assimp-sys-0.3.0 (c (n "assimp-sys") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0c0ks1j4g4ri3c9k15m88rq4mbyyvddn9djv4i5k7wws2dz0icq4")))

(define-public crate-assimp-sys-0.3.1 (c (n "assimp-sys") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0x8qs5bwyj44khya46dn5m4g52kchkip8w1gzyn7if5b73fszl1d")))

