(define-module (crates-io as #{3r}# as3razen) #:use-module (crates-io))

(define-public crate-as3razen-0.1.0 (c (n "as3razen") (v "0.1.0") (d (list (d (n "as3_parser") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "file_paths") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "smodel") (r "^1.0.8") (d #t) (k 0)))) (h "1nfcnxksbxqnwisavj1f6m0n1wrmi3z3yvv9hhamrlp04rd262ih")))

