(define-module (crates-io as -b as-bytes) #:use-module (crates-io))

(define-public crate-as-bytes-0.1.0 (c (n "as-bytes") (v "0.1.0") (h "1d1vl54z5v42g4gafl43h2daw4a4v2ia16az4csvj9wq1xqr808g")))

(define-public crate-as-bytes-0.2.0 (c (n "as-bytes") (v "0.2.0") (h "1rrmzm3bpmxshn9wy1x51z9nvz1a0kvha64l6p7fb6lj7s9jnxz8")))

