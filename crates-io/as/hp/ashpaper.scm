(define-module (crates-io as hp ashpaper) #:use-module (crates-io))

(define-public crate-ashpaper-0.1.0 (c (n "ashpaper") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "wordsworth") (r "^0.1.0") (d #t) (k 0)))) (h "07kfg1z65shx1ahl1bcbpk661g4gy5pcknbpjvskad71ndy1wgsk")))

(define-public crate-ashpaper-0.1.1 (c (n "ashpaper") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "wordsworth") (r "^0.1.0") (d #t) (k 0)))) (h "1ks4sqzkx75w326aywibxkfq4z0siix0h0a1al4vh6gsg0q8bmqc")))

(define-public crate-ashpaper-0.1.2 (c (n "ashpaper") (v "0.1.2") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "wordsworth") (r "^0.1.0") (d #t) (k 0)))) (h "18rrpihiqs81qhpmkn9llqc2slvzr2ippn3lw34vmydy4fxv93r9")))

(define-public crate-ashpaper-0.1.3 (c (n "ashpaper") (v "0.1.3") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "wordsworth") (r "^0.1.0") (d #t) (k 0)))) (h "1qckwdng782whs8kwfdwv76zb5dcfxbqmvjvm106bfv8m85qb6jr")))

(define-public crate-ashpaper-0.1.4 (c (n "ashpaper") (v "0.1.4") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "wordsworth") (r "^0.1.0") (d #t) (k 0)))) (h "0s0cssdvbflac17p3q1r5v6lcpmbdhvc3m4ma5bpfi584rs0r6ph")))

(define-public crate-ashpaper-0.2.4 (c (n "ashpaper") (v "0.2.4") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "wordsworth") (r "^0.1.0") (d #t) (k 0)))) (h "1hxlj3vc6icb51aksz0ispdgaf3nph5vb785p4jxvn127i4bjbf3")))

(define-public crate-ashpaper-0.2.5 (c (n "ashpaper") (v "0.2.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "wordsworth") (r "^0.1.0") (d #t) (k 0)))) (h "1p6sggppyql5sx41jrjw6k3yz77b3nlpqrh2ffqwmkb55f4qhfp4")))

(define-public crate-ashpaper-0.3.0 (c (n "ashpaper") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "wordsworth") (r "^0.1.0") (d #t) (k 0)))) (h "00q6amywiw7v5kwcigbnb9wxdp2vkgdigqykgj0h8lgaxds3yasd")))

