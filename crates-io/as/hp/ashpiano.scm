(define-module (crates-io as hp ashpiano) #:use-module (crates-io))

(define-public crate-ashpiano-0.0.1 (c (n "ashpiano") (v "0.0.1") (d (list (d (n "console") (r "^0.15.1") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "0ahaxm0l62zawk40kimp7lsa2w5ihqynikgpl8czyfin39vprlhd")))

(define-public crate-ashpiano-0.0.2 (c (n "ashpiano") (v "0.0.2") (d (list (d (n "console") (r "^0.15.1") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "03kjflf9x0mlm29br8x8x1an6md62p0jws6ca61j4yw0f96cmbpl")))

