(define-module (crates-io as hp ashpaper-bin) #:use-module (crates-io))

(define-public crate-ashpaper-bin-0.1.0 (c (n "ashpaper-bin") (v "0.1.0") (d (list (d (n "ashpaper") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0d0qbv30ryl43nzymdr1f6byvvxclkd6285c6ci38ffdmjz1vkpq")))

(define-public crate-ashpaper-bin-0.1.1 (c (n "ashpaper-bin") (v "0.1.1") (d (list (d (n "ashpaper") (r "^0.1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1cqpjgjwpgdvm3k8h53acznql208l9rrdbwh9858dgkawcm8gjr3")))

(define-public crate-ashpaper-bin-0.1.2 (c (n "ashpaper-bin") (v "0.1.2") (d (list (d (n "ashpaper") (r "^0.1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0icngw5mya2285qhs93ih38cy7y9bn2746i77rdhb8zb8v3p1r4k")))

(define-public crate-ashpaper-bin-0.1.3 (c (n "ashpaper-bin") (v "0.1.3") (d (list (d (n "ashpaper") (r "^0.1.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0drwp47y38gj10419anz8j18gx63fy807h3sizjrz9a4q3jqgz4x")))

(define-public crate-ashpaper-bin-0.1.4 (c (n "ashpaper-bin") (v "0.1.4") (d (list (d (n "ashpaper") (r "^0.2.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0dlv2sa3k8r1fwbww9s8ma02ppnml2qrbqddkwmpjkbchqyna7h7")))

(define-public crate-ashpaper-bin-0.2.0 (c (n "ashpaper-bin") (v "0.2.0") (d (list (d (n "ashpaper") (r "^0.2.5") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1niszdgm1l513d5m6jzw60ldn3a7m3cx2w41mc5mvra6hi4inm5v")))

(define-public crate-ashpaper-bin-0.2.1 (c (n "ashpaper-bin") (v "0.2.1") (d (list (d (n "ashpaper") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "197rjkf2z6nyzw1f1g6ijilslhq9svwcm0ywy7rlk6a11j5v7jwj")))

