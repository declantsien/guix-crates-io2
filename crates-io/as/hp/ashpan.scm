(define-module (crates-io as hp ashpan) #:use-module (crates-io))

(define-public crate-ashpan-0.1.0 (c (n "ashpan") (v "0.1.0") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "1yxp58lq3ll3i8hszabj5fwg9aclbbcaymfwzaiqhbnniw9df024")))

(define-public crate-ashpan-0.2.0 (c (n "ashpan") (v "0.2.0") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0bkg65xv0xx04jmldhiny6rfsmjarzwr2qc9kwh7d9qpbzlw05s3")))

(define-public crate-ashpan-0.3.0 (c (n "ashpan") (v "0.3.0") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0zpg4ggf7mm49gdivk1kx2ndbybmk6wnjhm4p4wib2nd7cg7jk01")))

(define-public crate-ashpan-0.4.0 (c (n "ashpan") (v "0.4.0") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "18gdq6j5hyywxqskv15fwl914xigp80inn8ipg0fh30dwzk5vxac")))

(define-public crate-ashpan-0.4.1 (c (n "ashpan") (v "0.4.1") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "1l8fb21dyrc6ijndkfasvdvx5qawqwkcix7b1s8hjnv93rgl7v3g")))

(define-public crate-ashpan-0.4.2 (c (n "ashpan") (v "0.4.2") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "04y1p5bhlvj1mx22np3lvs464s6a9k76p05g09j727br8d5d44h0")))

(define-public crate-ashpan-0.4.3 (c (n "ashpan") (v "0.4.3") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "19jyassrwk5kf0car5v9mk31q9hws092vxaf7cv2r4rh9ia5hvis")))

(define-public crate-ashpan-0.5.0 (c (n "ashpan") (v "0.5.0") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0d14lr0d82f95501pwcrxdvrnzyc6rjy1jqsnik0qkrpjqwj0mms")))

(define-public crate-ashpan-0.5.1 (c (n "ashpan") (v "0.5.1") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "02jg7pj0pmjzksg4gzs2lni729pgydkmd9vs0anhnj0dqdlxn19v")))

(define-public crate-ashpan-0.5.2 (c (n "ashpan") (v "0.5.2") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "1322gy3z0zb30axcqx8sqy3yn1za1di2z3n15hkdd80lf0yp7nhz")))

(define-public crate-ashpan-0.5.3 (c (n "ashpan") (v "0.5.3") (d (list (d (n "ash") (r "^0.34.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0amicnfjy8f4gir4p6in04rgrdr3q49fcpk21ybmc173f6q8yi4x") (y #t)))

(define-public crate-ashpan-0.6.0 (c (n "ashpan") (v "0.6.0") (d (list (d (n "ash") (r "^0.34.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "12zc752hfx9086q9b16zzwgv7ambl0ibjrkcfn4h3q78wy6708yi")))

(define-public crate-ashpan-0.6.1 (c (n "ashpan") (v "0.6.1") (d (list (d (n "ash") (r "^0.34.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "07l1rx57cgxyj8x3n0h3f32k4jdhicaq8cm4hb2i3frvbxj5kqn0")))

(define-public crate-ashpan-0.6.2 (c (n "ashpan") (v "0.6.2") (d (list (d (n "ash") (r "^0.34.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1kdva8bdibclg6lkxd7gql357rz1byj1283352y0xcirwmfkyb4r")))

(define-public crate-ashpan-0.7.0 (c (n "ashpan") (v "0.7.0") (d (list (d (n "ash") (r "^0.37.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1ha7vqcxqjqq6wd1kp6b1bmcy9swac4wrch7l0lqpxydg5nwb8s7")))

