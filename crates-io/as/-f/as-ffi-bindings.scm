(define-module (crates-io as -f as-ffi-bindings) #:use-module (crates-io))

(define-public crate-as-ffi-bindings-0.1.0 (c (n "as-ffi-bindings") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2.0") (d #t) (k 0)))) (h "1mdww4sawxa373484lrigx48iybbv198w8w601bir8vywxg5hcxv")))

(define-public crate-as-ffi-bindings-0.1.1 (c (n "as-ffi-bindings") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2.0") (d #t) (k 0)))) (h "1j1q4vb0629y0fsm0jly7pg1bq1x1ipmhyghydjybaww1b41l2zj")))

(define-public crate-as-ffi-bindings-0.2.0 (c (n "as-ffi-bindings") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2.0") (d #t) (k 0)))) (h "0a3cwgw2vs373ij1f240sr7zp3rkm1sn04fx2k5syj7axg5fa44k")))

(define-public crate-as-ffi-bindings-0.2.1 (c (n "as-ffi-bindings") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2.0") (d #t) (k 0)))) (h "06219jpx2f09hcrfg6j3rlq2klx34n5df3jl0b5j3hbyxll03nqm") (r "1.56.1")))

(define-public crate-as-ffi-bindings-0.2.2 (c (n "as-ffi-bindings") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2.1.1") (d #t) (k 0)))) (h "0ydv4ppx8fgqw6pmfbvbycic45a7b72hjn92l04xgdzi89f80xsj") (r "1.56.1")))

(define-public crate-as-ffi-bindings-0.2.3 (c (n "as-ffi-bindings") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^2.2.0-rc1") (d #t) (k 0)))) (h "1rl3ml517rdy3212w880d9a4sf4jlbgf980ngl22xqkrm21m2d4d") (r "1.56.1")))

(define-public crate-as-ffi-bindings-0.2.3-anyptr (c (n "as-ffi-bindings") (v "0.2.3-anyptr") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "wasmer") (r "^2.2.1") (d #t) (k 0)))) (h "0gyw9jhbdq9qzwd5dzx169xmlfzzy4jmr2ydi4j0qg22mwr88fvz") (r "1.56.1")))

(define-public crate-as-ffi-bindings-0.2.4 (c (n "as-ffi-bindings") (v "0.2.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "wasmer") (r "^2.2.1") (d #t) (k 0)))) (h "0im5idmjxpvz81jph53xy2zq1ynvhm89f7yw4f5cx43l12m8bylg") (r "1.56.1")))

(define-public crate-as-ffi-bindings-0.2.5 (c (n "as-ffi-bindings") (v "0.2.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "wasmer") (r "^2.3.0") (d #t) (k 0)))) (h "04jz0sl983y67wffvr8ga4ndc8vls5bi249h0bkhh5rrh0jzxiw2") (r "1.56.1")))

(define-public crate-as-ffi-bindings-0.2.6 (c (n "as-ffi-bindings") (v "0.2.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "wasmer") (r "^2.3.0") (d #t) (k 0)))) (h "183hwaly2fgj34064i9axniwc4mlndd09mrc509s64hvy016qwv3") (f (quote (("no_thread")))) (r "1.56.1")))

(define-public crate-as-ffi-bindings-0.2.7 (c (n "as-ffi-bindings") (v "0.2.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "wasmer") (r "^2.3.0") (d #t) (k 0)))) (h "0jdhfywr748ap9w2mj8zx4v6ddbcssg5mw1cwm3j3yp0cg9ys212") (f (quote (("no_thread")))) (r "1.56.1")))

