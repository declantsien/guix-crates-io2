(define-module (crates-io as dl asdl) #:use-module (crates-io))

(define-public crate-asdl-1.0.0 (c (n "asdl") (v "1.0.0") (d (list (d (n "insta") (r "^0.8.1") (d #t) (k 2)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "15bmgnl2h7b7ilvhdkcwnldixl58cbr1xcc5xn8jzja7mqqm58j3")))

(define-public crate-asdl-1.0.1 (c (n "asdl") (v "1.0.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "insta") (r "^0.8.1") (d #t) (k 2)) (d (n "nom") (r "^5.0.1") (f (quote ("std"))) (k 0)))) (h "0mmagv9dv1z34gjym8d6bgpw5s6nzhjs8fxnri9ab0whf509akww")))

