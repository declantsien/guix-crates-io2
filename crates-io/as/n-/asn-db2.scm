(define-module (crates-io as n- asn-db2) #:use-module (crates-io))

(define-public crate-asn-db2-0.2.0 (c (n "asn-db2") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "ipnet") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "06hplrk80signlnbbcizia3y7r642607yv06da3av60vdw0gk2zw")))

(define-public crate-asn-db2-0.2.2 (c (n "asn-db2") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "ipnet") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "00c9v441mljwiq55ycpha926plqgrsb4if0b48wz9gjc8fmaxk86")))

