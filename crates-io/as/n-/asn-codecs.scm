(define-module (crates-io as n- asn-codecs) #:use-module (crates-io))

(define-public crate-asn-codecs-0.1.0 (c (n "asn-codecs") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0358ndv0v0svngm66ds68f80z322xc645196v6f5hi9jsnf5rjc6")))

