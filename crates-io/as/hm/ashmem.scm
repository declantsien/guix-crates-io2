(define-module (crates-io as hm ashmem) #:use-module (crates-io))

(define-public crate-ashmem-0.1.0 (c (n "ashmem") (v "0.1.0") (d (list (d (n "ioctl-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15nzswpj2zy06ckwxpl7g8z4j1nc47h6fi6l9lqvcmgkkngm6fax") (y #t)))

(define-public crate-ashmem-0.1.1 (c (n "ashmem") (v "0.1.1") (d (list (d (n "ioctl-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dknicg708w4mgz2vi0kyqazmkj941m7c44vhnqzzip1365sagdl")))

(define-public crate-ashmem-0.1.2 (c (n "ashmem") (v "0.1.2") (d (list (d (n "ioctl-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b05c63zpb4p04q9dfrlfhggdkl9wm26d2vnlbd2jyhqj9vgfixr")))

