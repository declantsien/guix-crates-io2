(define-module (crates-io as ai asai-macro) #:use-module (crates-io))

(define-public crate-asai-macro-0.1.0 (c (n "asai-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09m7lz5scdcjh9bjdwngcwnb12gw67ccq0ms54k57rmrxps65gyw")))

