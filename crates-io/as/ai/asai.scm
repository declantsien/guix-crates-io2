(define-module (crates-io as ai asai) #:use-module (crates-io))

(define-public crate-asai-0.1.0 (c (n "asai") (v "0.1.0") (h "0qq8j90svicf5pq7vvf8fi3any44bsbah1bq8vca57gz1q3lbkbn") (y #t)))

(define-public crate-asai-0.1.1 (c (n "asai") (v "0.1.1") (d (list (d (n "asai-macro") (r "^0.1") (d #t) (k 0)))) (h "1kwn1ib85c9cp48c0jp2a7kd5s86ggls2nqal77xvil4r88cbgxb") (y #t)))

(define-public crate-asai-0.1.2 (c (n "asai") (v "0.1.2") (d (list (d (n "asai-macro") (r "^0.1") (d #t) (k 0)))) (h "0mshvrbbkav01776hbvia10mg8pajf4z584rg8f1hh8h28cjms7j")))

