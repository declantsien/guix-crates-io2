(define-module (crates-io as sh assh-auth) #:use-module (crates-io))

(define-public crate-assh-auth-0.0.0 (c (n "assh-auth") (v "0.0.0") (d (list (d (n "assh") (r "^0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter" "fmt" "tracing-log"))) (k 2)))) (h "14bxak2cn7fqh05y98bzmfrdbi8i6zsiinjnr6i020lj5x3gk494")))

