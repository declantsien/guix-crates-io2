(define-module (crates-io as sh assh) #:use-module (crates-io))

(define-public crate-assh-0.0.0 (c (n "assh") (v "0.0.0") (d (list (d (n "ssh-key") (r "^0.6.1") (d #t) (k 0)) (d (n "ssh-packet") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0akvz18ikp2fqasgz7hbfal8dl02kapji692dmq9ic25pixwjzgi")))

