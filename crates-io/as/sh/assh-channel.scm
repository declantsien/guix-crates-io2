(define-module (crates-io as sh assh-channel) #:use-module (crates-io))

(define-public crate-assh-channel-0.0.0 (c (n "assh-channel") (v "0.0.0") (d (list (d (n "assh") (r "^0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "unstable"))) (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter" "fmt" "tracing-log"))) (k 2)))) (h "0d6ij38d0pfsnpqh21sl3gbpbcf2hdyqvc1ahj3f3dz9wwk7iq5c") (y #t)))

