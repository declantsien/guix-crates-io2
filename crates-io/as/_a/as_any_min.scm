(define-module (crates-io as _a as_any_min) #:use-module (crates-io))

(define-public crate-as_any_min-1.0.0 (c (n "as_any_min") (v "1.0.0") (h "1l5s22ixsrk6d8hqbik4w51nr8bmpk71vqs8ljww49cl5jk4gxf1")))

(define-public crate-as_any_min-1.0.1 (c (n "as_any_min") (v "1.0.1") (h "1wc5m1p6sj8acqsf7011b65ifkn2lhfry55av005lg06rm88xfd7")))

(define-public crate-as_any_min-1.0.2 (c (n "as_any_min") (v "1.0.2") (h "057j6f0rdz1yhzlp7lgbq95rwasbscwsvlzjcg6c159r42kiykvf")))

