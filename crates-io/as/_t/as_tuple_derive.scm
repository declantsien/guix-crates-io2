(define-module (crates-io as _t as_tuple_derive) #:use-module (crates-io))

(define-public crate-as_tuple_derive-0.1.0 (c (n "as_tuple_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1l2yrnh6w02s3z4df2n5pwvyj2l11hazqcb5r22r6qsa4g33vhwr")))

