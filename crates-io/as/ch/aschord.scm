(define-module (crates-io as ch aschord) #:use-module (crates-io))

(define-public crate-aschord-0.1.0 (c (n "aschord") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "17dzh6m3xswvzy37g3hfzswfdx62pwgwxvkkgmfwaxfln4bfr73n")))

(define-public crate-aschord-0.2.0 (c (n "aschord") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z82khsvp5d0mvbyydg7bw9vby8szg9b9qhx5w3g5d9qs3wb6prd")))

(define-public crate-aschord-0.2.1 (c (n "aschord") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "06nn14halbdb6rzrrbc62s2jvn50dqdh1vp5pp7h64h6lni2ra6x")))

(define-public crate-aschord-0.3.1 (c (n "aschord") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)))) (h "0yp978x5cn0n91c03ih7l0brzfz69xibyc6lw7k5zj3r5ynk9ivv")))

(define-public crate-aschord-0.3.2 (c (n "aschord") (v "0.3.2") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)))) (h "00yin00ak5dgvy1cg2w03dmrzh8i0njacdpvvn0fpy4f2jqgiqqm")))

(define-public crate-aschord-0.3.3 (c (n "aschord") (v "0.3.3") (d (list (d (n "clap") (r "^3.2.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)))) (h "0an60av79lqiz2xgirb4zz91a975hgcmyvlijkssxfxdv9n9alwm")))

(define-public crate-aschord-0.3.4 (c (n "aschord") (v "0.3.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 0)))) (h "1z3aabrlx75nv9n9nxyfxpqy5dnfwhp4dd8viz1y2f7zxxngvrp9")))

