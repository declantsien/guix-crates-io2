(define-module (crates-io as se assert_matches2) #:use-module (crates-io))

(define-public crate-assert_matches2-0.1.0 (c (n "assert_matches2") (v "0.1.0") (h "0ijrkj7z6i463zgwaynpflzn6mw7bsyrqr8knffm32md285hsbxg")))

(define-public crate-assert_matches2-0.1.1 (c (n "assert_matches2") (v "0.1.1") (h "061wwmwngf9g3jcwfqvrsvzixsaw7s3nq3f4xgypp28qyjayjy35")))

(define-public crate-assert_matches2-0.1.2 (c (n "assert_matches2") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0mnz1wgz1s2s1985h44mdhy2m7615jjyr9pzq359injqqja2v0qm")))

