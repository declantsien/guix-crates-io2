(define-module (crates-io as se assert_hex) #:use-module (crates-io))

(define-public crate-assert_hex-0.1.0 (c (n "assert_hex") (v "0.1.0") (h "0wv4wgbvhxn2p4knazkvp980479lh24dimczks7d01drknzvqcbh")))

(define-public crate-assert_hex-0.1.1 (c (n "assert_hex") (v "0.1.1") (h "1fjlpghip793vc8c5nhw9vsprbaxqnpy80r3k2d4ccvz3s7ym52m")))

(define-public crate-assert_hex-0.2.0 (c (n "assert_hex") (v "0.2.0") (h "1pg14pqx4y88gsbcli54x8894s3mmc39y5n93n87npwcxld4570z")))

(define-public crate-assert_hex-0.2.1 (c (n "assert_hex") (v "0.2.1") (h "1yfkif2nhka1scwfgbw842wbmjrx5cxn4sqmk2hxgsipifz6f6hg")))

(define-public crate-assert_hex-0.2.2 (c (n "assert_hex") (v "0.2.2") (h "08y71m95naxgjzv00kamlqy4fc3c9c3f29a51ab3708bxlijhy5w")))

(define-public crate-assert_hex-0.3.0 (c (n "assert_hex") (v "0.3.0") (h "0ak768nfcja1c6hs6gdrs0hicfrb4m7dkljl064gb9yq3875awlh")))

(define-public crate-assert_hex-0.4.0 (c (n "assert_hex") (v "0.4.0") (h "0scmwcwfi90bd1v38847jw9vnf6z288rc2rfaxidys09knqgf1zm") (r "1.39.0")))

(define-public crate-assert_hex-0.4.1 (c (n "assert_hex") (v "0.4.1") (h "19xwlhz2swak1gw6rv38wai7d1xn9l3mspnv1fl8rz7h60a0y0gp") (r "1.39.0")))

