(define-module (crates-io as se asserter-macros) #:use-module (crates-io))

(define-public crate-asserter-macros-0.0.0 (c (n "asserter-macros") (v "0.0.0") (h "1shznv9y6ii0hnwp6mfss89bp4mygnmmin8qxhj5bnvbiwpknp1n")))

(define-public crate-asserter-macros-0.1.0 (c (n "asserter-macros") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.5.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold" "full" "visit" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1lzr5l3d0gi9k1an94rm5j49slkc0fkyddgzlxhg346wb8llg06y")))

