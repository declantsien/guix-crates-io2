(define-module (crates-io as se assertables) #:use-module (crates-io))

(define-public crate-assertables-3.0.0 (c (n "assertables") (v "3.0.0") (h "00nc5kn1vzgpagl0dabz27z9w1pz2pnzji9xr7a9i8a994pp2j6k")))

(define-public crate-assertables-3.0.1 (c (n "assertables") (v "3.0.1") (h "1cnkm5m08ybfks8m1s06igggvg1vfwxs07bplp7r782f6k6djrjx")))

(define-public crate-assertables-3.1.0 (c (n "assertables") (v "3.1.0") (h "0wiiym594hvlyccx5xg5rylx6v2yn3fhfmrxwx5sv4wr0pxys5ds")))

(define-public crate-assertables-3.2.1 (c (n "assertables") (v "3.2.1") (h "1325pb3nkpi06zisl3mghgjnpfkdi0iz968pfvxzlbdblwmbxk1r")))

(define-public crate-assertables-3.2.2 (c (n "assertables") (v "3.2.2") (h "1akissxzisbz3gwiyrsfnwj62jkw0p8dla27lyqw1rk77kb92jyq")))

(define-public crate-assertables-4.0.0 (c (n "assertables") (v "4.0.0") (h "0gyfw8bla0j735xaax6jzfgbv7fg4wk16qrmmazjinm8229j0si5")))

(define-public crate-assertables-4.0.1 (c (n "assertables") (v "4.0.1") (h "1nl29l9xs6bch3iih28lrxqknbqx2hrzcyn1psnl5wbgfl1i9pl0")))

(define-public crate-assertables-4.0.2 (c (n "assertables") (v "4.0.2") (h "0jka5i1d7lba9p30n3q00yf0bfb2cn6pab0cws671yb456i738pl")))

(define-public crate-assertables-4.0.3 (c (n "assertables") (v "4.0.3") (h "04f9bbcrmgdq1qcbryjha3d8m69y6zs27bf5lp34fs6a3gr6j84l")))

(define-public crate-assertables-4.0.4 (c (n "assertables") (v "4.0.4") (h "1fa2vx8ldm2ik45z15apya0bps6i3n2cjykiimhahgf4hb1x4smz")))

(define-public crate-assertables-4.0.5 (c (n "assertables") (v "4.0.5") (h "1axdpfgbihx3xnh268jlkyqx2z538ch6zsvv9d33r572fpds39h8")))

(define-public crate-assertables-4.0.6 (c (n "assertables") (v "4.0.6") (h "1ndvhm83zrixvd493z5rwhl0pqbgn0inznd4gwf4q40d06kq0iph")))

(define-public crate-assertables-4.1.0 (c (n "assertables") (v "4.1.0") (h "1m4fay8qg94f4s9j4qikvc3wpzhk9l4qrh0yravbj21rrcp6918g")))

(define-public crate-assertables-4.2.0 (c (n "assertables") (v "4.2.0") (h "17j8wh8z4lgikgrddmz2jrxylmrynz2sl4hcvqvblx394b8cfmmq")))

(define-public crate-assertables-4.4.0 (c (n "assertables") (v "4.4.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0y0jla2hv6xjv3nqnfhhilh3i1zvj4lx9x2jsm29qd0ys65hndba")))

(define-public crate-assertables-5.0.0 (c (n "assertables") (v "5.0.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1w7hpi5dsd1w9pfzrvrhv5ih7nqhw1kfrw09afv2pm6d07gz7hbq")))

(define-public crate-assertables-5.0.1 (c (n "assertables") (v "5.0.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1fbxfwihnz3spkigf4la80i3wqxhi8cvxhppz0gwnsldvdrgc4l6")))

(define-public crate-assertables-5.1.0 (c (n "assertables") (v "5.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "06qp96svj3qh88lxi510ikqgm1ljcivs461asj6l339c8ib1vpi8")))

(define-public crate-assertables-5.2.0 (c (n "assertables") (v "5.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0wsrjhnwn25rdb579m3x70dbs2xf7df6hm09xwvfki75385dh1yi")))

(define-public crate-assertables-6.0.0 (c (n "assertables") (v "6.0.0") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "17lb15l7rwr4hm2cizqpdi6w8lapccphx31wz6gm0l7yv34kn8ps")))

(define-public crate-assertables-6.0.1 (c (n "assertables") (v "6.0.1") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 2)))) (h "183g5w1hgcbns2r9hcj8v41c45kyfjl9diswms2xvk4cw3z6pa9d")))

(define-public crate-assertables-7.0.0 (c (n "assertables") (v "7.0.0") (d (list (d (n "cargo-dist") (r "^0.0.2") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)))) (h "1w2wv00lylnrsf22ynk6a5158f45x63a5ljxg9mcp69njp467zlp")))

(define-public crate-assertables-7.0.1 (c (n "assertables") (v "7.0.1") (d (list (d (n "cargo-dist") (r "^0.0.4") (d #t) (k 2)) (d (n "cargo-release") (r "^0.24.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)))) (h "0cgi090zwckp11h4f0fa3ydzsi669ag49zvbh0bbv7v6j3cyj90c")))

