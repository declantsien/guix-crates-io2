(define-module (crates-io as se assert2-macros) #:use-module (crates-io))

(define-public crate-assert2-macros-0.0.1 (c (n "assert2-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0k3057ql3aspbvxi7aglcar1lp09pfprci3z07kmkmxj0llxpdx3") (y #t)))

(define-public crate-assert2-macros-0.0.2 (c (n "assert2-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1paphilqy5wqkd33law4xdg9dcziad1a3p8lr9dhkf5mcr46b8mk")))

(define-public crate-assert2-macros-0.0.3 (c (n "assert2-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0i4m0q3znqh9wr2r8qviaknfyxyfr0q7ilay5y62q9cp97w0ad2d")))

(define-public crate-assert2-macros-0.0.4 (c (n "assert2-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1yy0lzx9yv0ghdx8y0nz4ahkmscrl14k2xl77jgy01q10svd7xck")))

(define-public crate-assert2-macros-0.0.6 (c (n "assert2-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1g7q6lkxj9y014mn4n66n83wn9jdjcpkssqz4bfcj524v4mlqya4")))

(define-public crate-assert2-macros-0.0.8 (c (n "assert2-macros") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "02791zq0c7d00zxiclxlckj7wvwsbnnfdk9ggxz5gbrpw22pz4i3")))

(define-public crate-assert2-macros-0.0.9 (c (n "assert2-macros") (v "0.0.9") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "031w62a613m368sfb7kaz8367gcmd0g1p92vzfkky6i4f55k2mxf")))

(define-public crate-assert2-macros-0.1.0 (c (n "assert2-macros") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "17223794cz6b1ik45vkl56i8dwavlc4qai05zlw8lagjial775pf")))

(define-public crate-assert2-macros-0.2.0 (c (n "assert2-macros") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "05bla4zbc0ly5pgk2g68vhbrmr523xvzxz4sajlzh5i5wppzhsak") (f (quote (("let-assert" "syn/visit"))))))

(define-public crate-assert2-macros-0.2.1 (c (n "assert2-macros") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1vpfp0mlc2lwwbmw5jbwgzzigk68mdakzh0v6abp7nyrpiq5vmyf") (f (quote (("let-assert" "syn/visit"))))))

(define-public crate-assert2-macros-0.3.0 (c (n "assert2-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1aidb7hd2n51jlrlq42d7196g4l81cmmg67s9qib99whnp5lm7y9")))

(define-public crate-assert2-macros-0.3.1 (c (n "assert2-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0q1s6b4kxxd0raqxb8nysxggzf29gkl2q9q2xdj82xx6vgnjrh2s")))

(define-public crate-assert2-macros-0.3.2 (c (n "assert2-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "13n8fxqs4hxwlydxgrp2ap0hkydiyyalw3nr049zpkrfsbq34hjp")))

(define-public crate-assert2-macros-0.3.3 (c (n "assert2-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1nf0hn5x6k5lqjnh1rzgdf8q6jjdd53r7f79am0p3m9mzdj6jp3i")))

(define-public crate-assert2-macros-0.3.4 (c (n "assert2-macros") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.3") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0wfn2gh32l89h2nbq9qlzj8kv6r0qy6fcgphk6716vwi9ijd2pn5")))

(define-public crate-assert2-macros-0.3.6 (c (n "assert2-macros") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0zj45kvgwn240mwyq78haz3sq6biw5d8lan2s61b45lg3kfjgb66")))

(define-public crate-assert2-macros-0.3.7 (c (n "assert2-macros") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "077nwvgwd8hjzn35xagbb8ipy5mzh5a0qd827xxqyw6jpc4pbis5") (r "1.65")))

(define-public crate-assert2-macros-0.3.8 (c (n "assert2-macros") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1l52c2p4r7ls8pr5277p654nwy5idixihz0za6cpslfdw5r7v7sg") (r "1.65")))

(define-public crate-assert2-macros-0.3.9 (c (n "assert2-macros") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0xv3xb03xlrvn407a1iv14q10348adck6mn3r2snlby5fqwykavz") (r "1.66")))

(define-public crate-assert2-macros-0.3.11 (c (n "assert2-macros") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0dcjbac962smxr6pmnsd6mdglh6any36ifshqzqzi4ppwvrvsmbc") (r "1.65")))

(define-public crate-assert2-macros-0.3.12 (c (n "assert2-macros") (v "0.3.12") (d (list (d (n "proc-macro2") (r "^1.0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1gaj1wmy6arwd69pj4z49snd7pdym3f0w0gg9i6bj234f1zlji96") (r "1.65")))

(define-public crate-assert2-macros-0.3.13 (c (n "assert2-macros") (v "0.3.13") (d (list (d (n "proc-macro2") (r "^1.0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0i1k2fi16bqljgcpk286klnys9mfcdgbw17zp1askgvgximsx0wd") (r "1.65")))

(define-public crate-assert2-macros-0.3.14 (c (n "assert2-macros") (v "0.3.14") (d (list (d (n "proc-macro2") (r "^1.0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "12dl13m0zlna5ywi5x46znhcwr37d343mrdxqxr8s77ys0my9h4y") (r "1.65")))

