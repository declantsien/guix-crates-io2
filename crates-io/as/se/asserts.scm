(define-module (crates-io as se asserts) #:use-module (crates-io))

(define-public crate-asserts-0.1.0 (c (n "asserts") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^0.2.2") (d #t) (k 0)) (d (n "assert_matches") (r "^1.0.0") (d #t) (k 0)) (d (n "assert_ne") (r "^0.2.4") (d #t) (k 0)))) (h "01kfzh7im6wrximybl7l6y63wc716v3hh2xvglxz7qgn8yv37fw4")))

