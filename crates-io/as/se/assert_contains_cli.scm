(define-module (crates-io as se assert_contains_cli) #:use-module (crates-io))

(define-public crate-assert_contains_cli-0.1.0 (c (n "assert_contains_cli") (v "0.1.0") (h "0mc12ky5rfkj09dzvfwnj6aawxadnqa6xplkmscz9f4zkaz2k632")))

(define-public crate-assert_contains_cli-0.1.1 (c (n "assert_contains_cli") (v "0.1.1") (h "1cysr81zhgv8nqgyzaqyzk054414nqp2j4h993fi58q45znymdwg")))

