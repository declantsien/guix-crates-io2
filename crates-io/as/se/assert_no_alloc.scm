(define-module (crates-io as se assert_no_alloc) #:use-module (crates-io))

(define-public crate-assert_no_alloc-1.0.0 (c (n "assert_no_alloc") (v "1.0.0") (h "0hyzw0gngzmwl0976hrzx00fih1rlgcidnammpzc9a4yxvd629vi") (f (quote (("warn_release") ("warn_debug") ("disable_release") ("default" "disable_release"))))))

(define-public crate-assert_no_alloc-1.1.0 (c (n "assert_no_alloc") (v "1.1.0") (h "0zcm1yl0bg5azf6w44m05xk4v99cglvsy3ca1xby4prqsbpdi617") (f (quote (("warn_release") ("warn_debug") ("disable_release") ("default" "disable_release"))))))

(define-public crate-assert_no_alloc-1.1.1 (c (n "assert_no_alloc") (v "1.1.1") (h "082midkwg6r3nqlpywx6lq5v61xs31cxqdlwrpmd8gf0jbq8nvh9") (f (quote (("warn_release") ("warn_debug") ("disable_release") ("default" "disable_release"))))))

(define-public crate-assert_no_alloc-1.1.2 (c (n "assert_no_alloc") (v "1.1.2") (h "18vqq5wmbzihlbzcmq0q9w09hkv85ajypcff2vcn2ba8g89q7jjm") (f (quote (("warn_release") ("warn_debug") ("disable_release") ("default" "disable_release"))))))

