(define-module (crates-io as se assertive) #:use-module (crates-io))

(define-public crate-assertive-0.0.0 (c (n "assertive") (v "0.0.0") (h "051zwwhghk2n7vlichg6hg7wsgqvl639gxq1x2v4nyysry4ph961")))

(define-public crate-assertive-0.0.2 (c (n "assertive") (v "0.0.2") (h "1qgnqb9bkrl7rvjznyyac36n5f8appiwzinpv30v86m3rqn47yvr")))

