(define-module (crates-io as se assemble-build) #:use-module (crates-io))

(define-public crate-assemble-build-0.1.1 (c (n "assemble-build") (v "0.1.1") (d (list (d (n "syn") (r "^1.0.97") (f (quote ("parsing" "full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0njv733wy72sxl6q688al4bj2w4sa17lqafq1h4hfd6d8kkxr6v3")))

(define-public crate-assemble-build-0.1.2 (c (n "assemble-build") (v "0.1.2") (d (list (d (n "syn") (r "^1.0.97") (f (quote ("parsing" "full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0sz0j2j0yqjmnd6rs1i1vfd722l54fs7bc2ylsjzwfa3w1g94r4q")))

(define-public crate-assemble-build-0.2.0 (c (n "assemble-build") (v "0.2.0") (d (list (d (n "syn") (r "^1.0.97") (f (quote ("parsing" "full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0w185fzadkn2wfln96v5kmdbkvwiw09ivbipvn2xyjr8k4gcn3c7") (r "1.64")))

