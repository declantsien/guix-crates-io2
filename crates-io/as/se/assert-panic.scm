(define-module (crates-io as se assert-panic) #:use-module (crates-io))

(define-public crate-assert-panic-1.0.0-preview1 (c (n "assert-panic") (v "1.0.0-preview1") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ab6im206ryqz5rjsnh6rrdx0y6xxnsi8ygf8rszfw0ay6a7gxdc")))

(define-public crate-assert-panic-1.0.0-preview.1 (c (n "assert-panic") (v "1.0.0-preview.1") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1lzvd39hvp048pvg0li7rsndfyj83klzcg1w9mfsnqi2x9zlrjz2")))

(define-public crate-assert-panic-1.0.0 (c (n "assert-panic") (v "1.0.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "07cyzmcfscasjisl29rlkwja43qwhg62178aj529w6as0v68xwz3")))

(define-public crate-assert-panic-1.0.1 (c (n "assert-panic") (v "1.0.1") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0hbz5nr2d3r993sqk9cydqwkhrf2h004g4n72inf8gz2ms12nfvn")))

