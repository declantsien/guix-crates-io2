(define-module (crates-io as se assemblylift-core-iomod-guest) #:use-module (crates-io))

(define-public crate-assemblylift-core-iomod-guest-0.1.0 (c (n "assemblylift-core-iomod-guest") (v "0.1.0") (h "058gwfjvs4l7ismnnw9nz6q8csmz7g8g50zxm4ri1vy6pmn6gn95")))

(define-public crate-assemblylift-core-iomod-guest-0.2.0 (c (n "assemblylift-core-iomod-guest") (v "0.2.0") (h "08xpqw4qq1vaimjbkb09m25hyrj2wqfpn488f037k34ps37l7rv3")))

(define-public crate-assemblylift-core-iomod-guest-0.3.0 (c (n "assemblylift-core-iomod-guest") (v "0.3.0") (h "0w0yrb3mn1hs4vrffmif427956pih0pb4dgmxvwh92r3vj0vyrzy")))

(define-public crate-assemblylift-core-iomod-guest-0.4.0-alpha.0 (c (n "assemblylift-core-iomod-guest") (v "0.4.0-alpha.0") (h "1xdz4zpk86smz75pwaxrdqfnpdw6bq6chfjcxfplld2r1bndvav3")))

