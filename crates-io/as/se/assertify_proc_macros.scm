(define-module (crates-io as se assertify_proc_macros) #:use-module (crates-io))

(define-public crate-assertify_proc_macros-0.5.0 (c (n "assertify_proc_macros") (v "0.5.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ih3i83f0msvjwpxyzvvrk2c8zk7aphvwjjg7zsplg7b2dng7dbd")))

(define-public crate-assertify_proc_macros-0.6.0 (c (n "assertify_proc_macros") (v "0.6.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n181224xddlhym203ijk8kkwbalgbyzqldz1dpwqgmw85yc6v9m")))

(define-public crate-assertify_proc_macros-0.6.1 (c (n "assertify_proc_macros") (v "0.6.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h297vgzrfcipjl29mhj2sffqa3b4wqg4wr3wlvks02g8x535fkd")))

(define-public crate-assertify_proc_macros-0.7.1 (c (n "assertify_proc_macros") (v "0.7.1") (h "1r78f1k0va91s16kl85r12ljqjdiixxkpdim5lxmlj8ciwfqaidm")))

