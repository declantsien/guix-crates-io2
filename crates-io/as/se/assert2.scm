(define-module (crates-io as se assert2) #:use-module (crates-io))

(define-public crate-assert2-0.0.2 (c (n "assert2") (v "0.0.2") (d (list (d (n "assert2-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "10bcgbxcflr0nfxc1lfmsy8sr4gn12jvjrn9kpb5haks5qy69spc")))

(define-public crate-assert2-0.0.3 (c (n "assert2") (v "0.0.3") (d (list (d (n "assert2-macros") (r "^0.0.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1342s1qwm814qrgc7qggkn3i18xb4n7834dpnsk928cvb20z1ng7")))

(define-public crate-assert2-0.0.4 (c (n "assert2") (v "0.0.4") (d (list (d (n "assert2-macros") (r "= 0.0.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "16m0myzgisx6hb9gkv4nl1igj71q6kdv8yf3bhwigph9k7wfzzdq")))

(define-public crate-assert2-0.0.5 (c (n "assert2") (v "0.0.5") (d (list (d (n "assert2-macros") (r "= 0.0.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1nbks4izhmvsnn8im7vljq5rz4243y1nssjdb9w6fh12s77952gi")))

(define-public crate-assert2-0.0.6 (c (n "assert2") (v "0.0.6") (d (list (d (n "assert2-macros") (r "= 0.0.6") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "018nl6b5ly4hrxzmwvj45423dk2kw207fmr6vd436h19qq41ahbn")))

(define-public crate-assert2-0.0.7 (c (n "assert2") (v "0.0.7") (d (list (d (n "assert2-macros") (r "= 0.0.6") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1hr437w4b2yscisdmpbvfrqkyjqcfpzrwpjyh0llnsa3c0523ay3")))

(define-public crate-assert2-0.0.8 (c (n "assert2") (v "0.0.8") (d (list (d (n "assert2-macros") (r "= 0.0.8") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1i08gi99l569zphkfd3pyd639akg51kd0vrw7ds4fb70rh5gazr0")))

(define-public crate-assert2-0.0.9 (c (n "assert2") (v "0.0.9") (d (list (d (n "assert2-macros") (r "= 0.0.9") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1g2ylv4gab6j6ach7hr7jd2zxr8aqk22j5yrqmx9x8pgf36g1cmh")))

(define-public crate-assert2-0.1.0 (c (n "assert2") (v "0.1.0") (d (list (d (n "assert2-macros") (r "= 0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "18gwc7vza9kdf36dxrj06j81g3swlk084k8893jkyrlipfws32qw")))

(define-public crate-assert2-0.1.1 (c (n "assert2") (v "0.1.1") (d (list (d (n "assert2-macros") (r "= 0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1lmvg97lwx8i9sa573bgb5lvzdg050cicim93fpipz1xqdccm4nv")))

(define-public crate-assert2-0.1.2 (c (n "assert2") (v "0.1.2") (d (list (d (n "assert2-macros") (r "= 0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "12n1rz2n186pj5f1kawlraa4szn4ijx320qfmm8pi8idv7ri05g6")))

(define-public crate-assert2-0.2.0 (c (n "assert2") (v "0.2.0") (d (list (d (n "assert2-macros") (r "= 0.2.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1wb123fxjhmf13afn4vpjz0qkndv15j9jlyys43gnv65hd158l38") (f (quote (("let-assert" "assert2-macros/let-assert") ("doc-cfg"))))))

(define-public crate-assert2-0.2.1 (c (n "assert2") (v "0.2.1") (d (list (d (n "assert2-macros") (r "= 0.2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "16q2yyxza6hwlf2jqck2jxa3xda6jw6idn91fss8z5f4iz671p8f") (f (quote (("let-assert" "assert2-macros/let-assert") ("doc-cfg"))))))

(define-public crate-assert2-0.3.0 (c (n "assert2") (v "0.3.0") (d (list (d (n "assert2-macros") (r "=0.3.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0nrcn272kv9sxh1yj6zd16xy1pkqx2vkp1m6knkq300h9cdd18bz")))

(define-public crate-assert2-0.3.1 (c (n "assert2") (v "0.3.1") (d (list (d (n "assert2-macros") (r "=0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0kh4kbsjs7q79j3jz9k529qbpp6kji4pan4zwbgl4blfnzfgl8hc")))

(define-public crate-assert2-0.3.2 (c (n "assert2") (v "0.3.2") (d (list (d (n "assert2-macros") (r "=0.3.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1fqa15zglkv5prfk4j267k0wr6igfpvki6mbzz1nrngiy9vmmqr3")))

(define-public crate-assert2-0.3.3 (c (n "assert2") (v "0.3.3") (d (list (d (n "assert2-macros") (r "=0.3.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0vz8a36s3c8wkyghljrlfjkg89ipbg4wmy2rjzpjn8qsxyfydlcn")))

(define-public crate-assert2-0.3.4 (c (n "assert2") (v "0.3.4") (d (list (d (n "assert2-macros") (r "=0.3.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1zz6s999rx305f75i9j4c82kzz1px9kwi1fc9yq8l4cf91sjk4bq")))

(define-public crate-assert2-0.3.5 (c (n "assert2") (v "0.3.5") (d (list (d (n "assert2-macros") (r "=0.3.4") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0h5kgwj62qsbilyakhjv0vj5fah0rl95ak557ix2c7m880mglcn9")))

(define-public crate-assert2-0.3.6 (c (n "assert2") (v "0.3.6") (d (list (d (n "assert2-macros") (r "=0.3.6") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0qb3k3ikr0ih3vvg619v3jr0hhmv27slny1bzx0wsjb1y5x1c6yf")))

(define-public crate-assert2-0.3.7 (c (n "assert2") (v "0.3.7") (d (list (d (n "assert2-macros") (r "=0.3.7") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0pfa4zbqygqyaw8jnqnxzic99fhkyd80wwxghsg8wp3wpxk6ni81") (r "1.65")))

(define-public crate-assert2-0.3.8 (c (n "assert2") (v "0.3.8") (d (list (d (n "assert2-macros") (r "=0.3.8") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0m34bkism6iyl5p4q3y12f92b03gqgpcbjs9sbcsiqfcc8shsn80") (r "1.65")))

(define-public crate-assert2-0.3.9 (c (n "assert2") (v "0.3.9") (d (list (d (n "assert2-macros") (r "=0.3.9") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0p9w53rjnfy5hny8ir6myvbn3qakfxgbl2qplk9910hwq2q66sdk") (y #t) (r "1.66")))

(define-public crate-assert2-0.3.10 (c (n "assert2") (v "0.3.10") (d (list (d (n "assert2-macros") (r "=0.3.9") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0cvygx5zrcba7b2vh6cf0pdsavbjqd9m642hz17fzaf0xqgi13xh") (y #t) (r "1.66")))

(define-public crate-assert2-0.3.11 (c (n "assert2") (v "0.3.11") (d (list (d (n "assert2-macros") (r "=0.3.11") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "15cfdn5sl8ls6234pv2ysflw2xcxh8j1ypjlif7wnva0hc8qvyga") (r "1.65")))

(define-public crate-assert2-0.3.12 (c (n "assert2") (v "0.3.12") (d (list (d (n "assert2-macros") (r "=0.3.12") (d #t) (k 0)) (d (n "diff") (r "^0.1.13") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0csf68kjhz1n1417y5nzy68xahvmvpn41527vndvi8j6c5z35hyc") (r "1.65")))

(define-public crate-assert2-0.3.13 (c (n "assert2") (v "0.3.13") (d (list (d (n "assert2-macros") (r "=0.3.13") (d #t) (k 0)) (d (n "diff") (r "^0.1.13") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0fczj5yk1yqrmjbxj68z239f89vbz4g8vg7dri7a26fkzl3a9hl4") (r "1.65")))

(define-public crate-assert2-0.3.14 (c (n "assert2") (v "0.3.14") (d (list (d (n "assert2-macros") (r "=0.3.14") (d #t) (k 0)) (d (n "diff") (r "^0.1.13") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1fpdwggm0m334r320z3q7wff1lzbyj06qfzn06fxszcj5lbs6k44") (r "1.65")))

