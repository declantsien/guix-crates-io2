(define-module (crates-io as se assemble-macros) #:use-module (crates-io))

(define-public crate-assemble-macros-0.1.0 (c (n "assemble-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1aqshnhmbik9vb0ajy2f0v8yk9v0l7z5x8p5pcsjdi5cj9zl9c2a")))

(define-public crate-assemble-macros-0.1.1 (c (n "assemble-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1pg2aa4yj6w8cr07ylcmzdqmzr5mzm24329ddhp247kdqijydjfd")))

(define-public crate-assemble-macros-0.1.2 (c (n "assemble-macros") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0m0ahki85nr4zvasqzlsirnv46ahn2w1wjzksprb3klbxmh9vp0h")))

(define-public crate-assemble-macros-0.2.0 (c (n "assemble-macros") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1y3k44qyyvfgqsflcaf8xnpq5ah9cw6jjw12kp5idbwn4b8ds0w7") (r "1.64")))

