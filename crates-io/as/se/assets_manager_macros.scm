(define-module (crates-io as se assets_manager_macros) #:use-module (crates-io))

(define-public crate-assets_manager_macros-0.1.0 (c (n "assets_manager_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0fvrkcy86yd1jgywbzg9fvaw7ysf4082xwa5x270926gpfnzgqyk")))

(define-public crate-assets_manager_macros-0.2.0 (c (n "assets_manager_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "095qg573qr6amqz9qsafv3941xqgcspgkjgsjkp4nlx68krxa41j")))

(define-public crate-assets_manager_macros-0.2.1 (c (n "assets_manager_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0p93q51qq55bqmpiyddc45ygypd51abis9i229wd978bpr80v4v8")))

(define-public crate-assets_manager_macros-0.2.2 (c (n "assets_manager_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0yfxnkhlja3dzf6qciapmpxkwacy5zfnc04s642rzh1g016s5pkw")))

(define-public crate-assets_manager_macros-0.2.3 (c (n "assets_manager_macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1fs0r59c3qbawblqzy50d2jbgsq3rm7832802wlmnqn9qg0pb907")))

(define-public crate-assets_manager_macros-0.2.4 (c (n "assets_manager_macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "083y219hil9d2bdi6mcyp8djhkafm90xmf52x5716h27xchg9xi6")))

