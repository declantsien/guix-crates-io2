(define-module (crates-io as se asset_procmac) #:use-module (crates-io))

(define-public crate-asset_procmac-0.1.0 (c (n "asset_procmac") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1h0xz2fr74civ4nc5jsa3dnzdiyaxxr6qss575q0nykggnicjx73")))

(define-public crate-asset_procmac-0.1.1 (c (n "asset_procmac") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "16lny3bba4ingivhclil6p1drfn1a15qld0rvmvxx16cjb101vbf")))

(define-public crate-asset_procmac-0.1.2 (c (n "asset_procmac") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1qys9pf39nxblp5idwpw0p55cbvhpxshihlcz8hk3i8p3jvh38wi")))

(define-public crate-asset_procmac-0.2.0 (c (n "asset_procmac") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1sy0x6y244x69kj5nlhcwbjn54wmm7spvh8ww3jnmwyjb6wy6yj5") (y #t)))

(define-public crate-asset_procmac-0.2.1 (c (n "asset_procmac") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "04y9lq7wfih83iwjj8llp0456ym90q7nw9gm526296mpmfwdb152")))

(define-public crate-asset_procmac-0.2.2 (c (n "asset_procmac") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "13211apfwqgrx47s6rcj62l82k1v5f4jfwgs3iaky1q9wi5m162q")))

(define-public crate-asset_procmac-0.2.3 (c (n "asset_procmac") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0w8xmfrddpcwpssmfi24w58r1ib6gjrc0lir4dpbzd51ggz7x58j")))

