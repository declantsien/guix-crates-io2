(define-module (crates-io as se assert_matches) #:use-module (crates-io))

(define-public crate-assert_matches-1.0.0 (c (n "assert_matches") (v "1.0.0") (h "1zcvmy7crki4jrvjx8xkw1cdzkkkcp0q99mxgrwb01d19k9d285v")))

(define-public crate-assert_matches-1.0.1 (c (n "assert_matches") (v "1.0.1") (h "091plncn8zadhwfphx1qpb3vmyy3w526alhmvz8201l2z2a5da4s")))

(define-public crate-assert_matches-1.1.0 (c (n "assert_matches") (v "1.1.0") (h "0mxz9j4nbpkrhf0g1lkkly09y659zkj49q1iihv1pwfdvi12jxwy")))

(define-public crate-assert_matches-1.2.0 (c (n "assert_matches") (v "1.2.0") (h "12r19ljsjlfxr65qkvs14lm1p5l25nhiqvpbq0qhzbhgy2mp0i36")))

(define-public crate-assert_matches-1.3.0 (c (n "assert_matches") (v "1.3.0") (h "1rar61v00gz2aniid0mylxcr4q98s6l77c3hvbszmg57kj10msvx")))

(define-public crate-assert_matches-1.4.0 (c (n "assert_matches") (v "1.4.0") (h "1wgdx6w4yj3m0795jkdgw3a5jr06vdd1wij0pds3f3sjybq7jmb9")))

(define-public crate-assert_matches-1.5.0 (c (n "assert_matches") (v "1.5.0") (h "1a9b3p9vy0msylyr2022sk5flid37ini1dxji5l3vwxsvw4xcd4v")))

