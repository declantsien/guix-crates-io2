(define-module (crates-io as se assert_ok) #:use-module (crates-io))

(define-public crate-assert_ok-1.0.0 (c (n "assert_ok") (v "1.0.0") (h "06hgrqkhr00n5vwbhsxrz0z5w65hhcqrlyd5fdav7kry1izrhyvy")))

(define-public crate-assert_ok-1.0.1 (c (n "assert_ok") (v "1.0.1") (h "18651p3v531q5vi2rmmipyn1va0jrqz19gjyrqynb3yn49xykxwf")))

(define-public crate-assert_ok-1.0.2 (c (n "assert_ok") (v "1.0.2") (h "117xzrsfw83vjz8iqz3x2n4yyzvk1vq2jyg5rh8xnha5cbvhwxvw")))

