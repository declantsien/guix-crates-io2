(define-module (crates-io as se assert_float_eq) #:use-module (crates-io))

(define-public crate-assert_float_eq-1.0.0 (c (n "assert_float_eq") (v "1.0.0") (h "02a55078caa2z347ggs0rjkbb62kyaavczdx13pw4qw71im36yz7") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_float_eq-1.0.1 (c (n "assert_float_eq") (v "1.0.1") (h "03pkiz04hviw2libkp7iq4agrr7c8gpmgnkrz0x3hn5p8vak2m49") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_float_eq-1.0.2 (c (n "assert_float_eq") (v "1.0.2") (h "05dpj5xq18i2lf3x7znfny9lwqs71bpq12nhy16hgfka5zz2xaiz") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_float_eq-1.0.4 (c (n "assert_float_eq") (v "1.0.4") (h "1wdi0d9jlh6rcaz2sppj0j52gpwn8mv9sxkn45ccfy8fi9pkpy5n") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_float_eq-1.1.0 (c (n "assert_float_eq") (v "1.1.0") (h "07r7pivfk5vrh1an7dmhjbhq8xw24xh55b0bdzknb68v20rgi2cf") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_float_eq-1.1.1 (c (n "assert_float_eq") (v "1.1.1") (h "1m50zsz1jpbrclchm69adikp1ykpqdxh5bl0x0hsbdkbn5vz4pna") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_float_eq-1.1.2 (c (n "assert_float_eq") (v "1.1.2") (h "000cm49n73yvna7li1xyw0yldvm2rvmq60fkiix0l5wfcc7zjjrp") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_float_eq-1.1.3 (c (n "assert_float_eq") (v "1.1.3") (h "0xryhlk2p012y109048c5jj5f448cv0b86ylkjgg5v7dzcpnbsjc") (f (quote (("std") ("default" "std"))))))

