(define-module (crates-io as se assemblyline-markings) #:use-module (crates-io))

(define-public crate-assemblyline-markings-0.1.0 (c (n "assemblyline-markings") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "13xc153iwd94c1abwcfv89ryjc8x87cg0ydsbywz3f1pn4x15hsi")))

(define-public crate-assemblyline-markings-0.1.1 (c (n "assemblyline-markings") (v "0.1.1") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0z2624bnfgsqhi2niqd2qns2j41wg34c34sk1qdmklaycxv7l3xm")))

(define-public crate-assemblyline-markings-0.1.2 (c (n "assemblyline-markings") (v "0.1.2") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "00grarm22wizd79xyx73v3gra77p65gy2qyw4337hvk6qx0fi5v3")))

(define-public crate-assemblyline-markings-0.1.3 (c (n "assemblyline-markings") (v "0.1.3") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "02rbp5h96dypbj0hdzymvfihd89xb99hm4b7z40qblczh7xrhf5s")))

(define-public crate-assemblyline-markings-0.1.4 (c (n "assemblyline-markings") (v "0.1.4") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1nmdm34v06sp6vmj76vj9cp4a5fhs362sihq7468xpqav9g2n2py")))

(define-public crate-assemblyline-markings-0.1.5 (c (n "assemblyline-markings") (v "0.1.5") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0mxldycp6zl49m14m3jzs0kba2b93jv5j52vvsrg75yq7ysy8zcc")))

(define-public crate-assemblyline-markings-0.1.6 (c (n "assemblyline-markings") (v "0.1.6") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1i5fw55rba0pp49n84xrhyapl7ggqfchmq8b2230gz355bxi3akn")))

(define-public crate-assemblyline-markings-0.1.7 (c (n "assemblyline-markings") (v "0.1.7") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "12n2asr949rzvp3gj2lnznrpgpb7ld4344mihbxqgahxbwrr91ah")))

(define-public crate-assemblyline-markings-0.1.8 (c (n "assemblyline-markings") (v "0.1.8") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0q3pnd1aii4qqkdzyhwgmn846p56nn3l2kqzq5fr3rksvh6czpac")))

