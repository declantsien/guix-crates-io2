(define-module (crates-io as se assert-impl) #:use-module (crates-io))

(define-public crate-assert-impl-0.1.0 (c (n "assert-impl") (v "0.1.0") (h "15plh4ar7lj2wyln0napp5qw11g80znr0j2ynn7dkh91qdlrvrs0")))

(define-public crate-assert-impl-0.1.1 (c (n "assert-impl") (v "0.1.1") (h "05xx3f9rx6qbbgwl90xsyv9yd36wh4dd2hrvisl2pzjqm1xh6l9i")))

(define-public crate-assert-impl-0.1.2 (c (n "assert-impl") (v "0.1.2") (h "1grxb8lf6ldd8d5acsj5llz0nmgb16n1q6qfg1mrcyffr4ch2kpk")))

(define-public crate-assert-impl-0.1.3 (c (n "assert-impl") (v "0.1.3") (h "17jvig9rwdc1sf1j5q8q9k69njg3k8g7x7g6wcb711hcvq9l6in3")))

