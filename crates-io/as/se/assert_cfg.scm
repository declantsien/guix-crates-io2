(define-module (crates-io as se assert_cfg) #:use-module (crates-io))

(define-public crate-assert_cfg-0.1.0 (c (n "assert_cfg") (v "0.1.0") (d (list (d (n "const_panic") (r "^0.2.0") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ch18pl137k88aw0bswyygalk6sk84afvz99fzwy6zkb6qgnbqh4") (f (quote (("foo") ("baz") ("bar") ("__test" "foo" "bar" "baz")))) (r "1.57")))

