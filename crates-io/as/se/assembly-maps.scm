(define-module (crates-io as se assembly-maps) #:use-module (crates-io))

(define-public crate-assembly-maps-0.1.0 (c (n "assembly-maps") (v "0.1.0") (d (list (d (n "assembly-core") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0l5knpzwnlvhdmaaj1qci6s156v7yfbjm1jq4fc6q9zg47pkva9h")))

(define-public crate-assembly-maps-0.2.0-beta.0 (c (n "assembly-maps") (v "0.2.0-beta.0") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 2)) (d (n "assembly-core") (r ">=0.2.0-beta.0, <0.3.0") (d #t) (k 0)) (d (n "displaydoc") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "num-traits") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structopt") (r ">=0.2.0, <0.3.0") (d #t) (k 2)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "118wr1khmba50ixb64yycyibbkzj5n6rlb8f4dk2cvg6a31nl4k0") (f (quote (("serde-derives" "serde" "assembly-core/serde-derives"))))))

(define-public crate-assembly-maps-0.2.0 (c (n "assembly-maps") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "assembly-core") (r "^0.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09vadbyy17lca0gqdh0icrcx2qlr7aygr7n8gmv3sipanxgdspfk") (f (quote (("serde-derives" "serde" "assembly-core/serde-derives"))))))

