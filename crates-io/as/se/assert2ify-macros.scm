(define-module (crates-io as se assert2ify-macros) #:use-module (crates-io))

(define-public crate-assert2ify-macros-0.1.0-alpha1 (c (n "assert2ify-macros") (v "0.1.0-alpha1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1xx89ip08n1jiafbippk1w6nisw8ckl3jf00cxwybrnl09j53c5l")))

