(define-module (crates-io as se assert-call) #:use-module (crates-io))

(define-public crate-assert-call-0.1.0 (c (n "assert-call") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "yansi") (r "^1.0.0-rc.1") (f (quote ("detect-tty" "detect-env"))) (d #t) (k 0)))) (h "1ilbs27khckfgymgfcvhxa6xanxfdwzxxj51v8ah7k4fay03vn8i")))

(define-public crate-assert-call-0.1.1 (c (n "assert-call") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "yansi") (r "^1.0.0-rc.1") (f (quote ("detect-tty" "detect-env"))) (d #t) (k 0)))) (h "19niaz5ml6cn021i6dpijjsy8mv1c3nlmdvqsdsydskbz0yccasv")))

