(define-module (crates-io as se assemblylift-iomod-crypto-guest) #:use-module (crates-io))

(define-public crate-assemblylift-iomod-crypto-guest-0.1.0 (c (n "assemblylift-iomod-crypto-guest") (v "0.1.0") (d (list (d (n "assemblylift_core_io_guest") (r "^0.1.0") (d #t) (k 0) (p "assemblylift-core-io-guest")) (d (n "assemblylift_core_iomod_guest") (r "^0.2.0") (d #t) (k 0) (p "assemblylift-core-iomod-guest")) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)))) (h "0bjzln48h9c7dbh9jdx6iqyca2ykl4drn45kxa7c2xgab1klgpb6")))

(define-public crate-assemblylift-iomod-crypto-guest-0.1.1 (c (n "assemblylift-iomod-crypto-guest") (v "0.1.1") (d (list (d (n "assemblylift_core_io_guest") (r "^0.2.1") (d #t) (k 0) (p "assemblylift-core-io-guest")) (d (n "assemblylift_core_iomod_guest") (r "^0.2.0") (d #t) (k 0) (p "assemblylift-core-iomod-guest")) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)))) (h "15r3bkhkawnny6n483c0z626y47hajapz4gis5qm0n7dr3b8b0h7")))

(define-public crate-assemblylift-iomod-crypto-guest-0.1.2 (c (n "assemblylift-iomod-crypto-guest") (v "0.1.2") (d (list (d (n "assemblylift_core_io_guest") (r "^0.3") (d #t) (k 0) (p "assemblylift-core-io-guest")) (d (n "assemblylift_core_iomod_guest") (r "^0.3") (d #t) (k 0) (p "assemblylift-core-iomod-guest")) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0a1akw7ychr98i0wafifabk64m5yd03ac1k7wq1q4549g12zgz83")))

