(define-module (crates-io as se assert-text) #:use-module (crates-io))

(define-public crate-assert-text-0.2.4 (c (n "assert-text") (v "0.2.4") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "06lnvn3av3451b27hjspb2bfl5r1i5s35zmz2gchb8xfqjwnhxvw")))

(define-public crate-assert-text-0.2.5 (c (n "assert-text") (v "0.2.5") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0a5gggj5qvv5lk9zps2ngqd73dvjn182av8x0ixmqsc9af897xr1")))

(define-public crate-assert-text-0.2.6 (c (n "assert-text") (v "0.2.6") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0jf9wfjwfszvwb03b43n9bfzn6v7ydlvh5a5yv4rxcg6xsqc7zx5")))

(define-public crate-assert-text-0.2.7 (c (n "assert-text") (v "0.2.7") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1sj8pks3sbnbp68cs412v06agz1hsgb0gax0mvf2n6xqysibxkwc")))

(define-public crate-assert-text-0.2.8 (c (n "assert-text") (v "0.2.8") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "139xrfhayrjjm9kdllb22hzh0sv7gh0bzlnv0ilgmh3l6kwjlkgn") (r "1.56.0")))

(define-public crate-assert-text-0.2.9 (c (n "assert-text") (v "0.2.9") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "012nmb20vqkv5pam9acx1kbpd8y0ykmwx6795va782rdz4f3as58") (r "1.56.0")))

