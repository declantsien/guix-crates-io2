(define-module (crates-io as se assertor) #:use-module (crates-io))

(define-public crate-assertor-0.0.1 (c (n "assertor") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.2.14") (o #t) (d #t) (k 0)))) (h "1z7hvhlgxd01a66rar0pxfmanm6zbs54rb687jbynz9icf0fmgf8") (f (quote (("testing") ("float" "num-traits") ("default" "float"))))))

(define-public crate-assertor-0.0.2 (c (n "assertor") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)))) (h "0gbnd3l7b95ig3r0zlv9xlaw5kl8k76f1py60i4pzq9gsyx9znki") (f (quote (("testing") ("float" "num-traits") ("default" "float")))) (r "1.67.0")))

