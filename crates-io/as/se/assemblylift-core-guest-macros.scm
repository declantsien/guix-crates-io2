(define-module (crates-io as se assemblylift-core-guest-macros) #:use-module (crates-io))

(define-public crate-assemblylift-core-guest-macros-0.0.0 (c (n "assemblylift-core-guest-macros") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aw4dbsxqbfzsv68ia3jcrpg2qx14rjw842mlwb5bd4fkgbzhk07")))

(define-public crate-assemblylift-core-guest-macros-0.4.0-alpha.0 (c (n "assemblylift-core-guest-macros") (v "0.4.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s5w72pkj4y60gg61fa7izsgzp3rvpwh14cqikfpv6vr742a6gb5")))

