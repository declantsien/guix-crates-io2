(define-module (crates-io as se assert-json-diff) #:use-module (crates-io))

(define-public crate-assert-json-diff-0.1.0 (c (n "assert-json-diff") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "1bngdpyby0ly3nhl5vx8s50qdqif5362246jckrp6qw2pw2nqjxj")))

(define-public crate-assert-json-diff-0.2.0 (c (n "assert-json-diff") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0ly7d80qvmr1yj98jgkb7i4akib6hvgd9apjnhkz3hqg2kyp9ahg")))

(define-public crate-assert-json-diff-0.2.1 (c (n "assert-json-diff") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1akgsv94r1fmai68v7xqd53z9v49cjk54cg5a68bcd5i76517fy4")))

(define-public crate-assert-json-diff-1.0.0 (c (n "assert-json-diff") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1cggzlf606460lyglxfq5fslfznbz43lk1kci4shw3fm65nnp51j")))

(define-public crate-assert-json-diff-1.0.1 (c (n "assert-json-diff") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1ll19y7fac114razawj7dgl62a3b99vjnr9d0p7yqmg7vq3d70cq")))

(define-public crate-assert-json-diff-1.0.2 (c (n "assert-json-diff") (v "1.0.2") (d (list (d (n "extend") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "06pl9bsgfwqwv4w0ybp79qr26q62lbql6cvr67qk1vz6prg0v3bg")))

(define-public crate-assert-json-diff-1.0.3 (c (n "assert-json-diff") (v "1.0.3") (d (list (d (n "extend") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1mjpmd0h0bpl9686bym5lff234f3hfn6d0iirf5ia5rlznbn8dcw")))

(define-public crate-assert-json-diff-1.1.0 (c (n "assert-json-diff") (v "1.1.0") (d (list (d (n "extend") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1h2w4n8f8a1n9sc8snka0arzw5x95ky5k8i7603z3lhkcplwnna2")))

(define-public crate-assert-json-diff-2.0.0 (c (n "assert-json-diff") (v "2.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1wb731pc7hfp58nsz5r3x2rn1y587m5zwhczgbdsbzpaiyssbs77")))

(define-public crate-assert-json-diff-2.0.1 (c (n "assert-json-diff") (v "2.0.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1nh65z26jfigjg7j73i3mkn9k40fjdl9216ay3bk4dfk7mqc7wah")))

(define-public crate-assert-json-diff-2.0.2 (c (n "assert-json-diff") (v "2.0.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "04mg3w0rh3schpla51l18362hsirl23q93aisws2irrj32wg5r27")))

