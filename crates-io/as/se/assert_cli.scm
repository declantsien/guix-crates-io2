(define-module (crates-io as se assert_cli) #:use-module (crates-io))

(define-public crate-assert_cli-0.1.0 (c (n "assert_cli") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0.19") (o #t) (d #t) (k 0)) (d (n "difference") (r "^0.4.0") (d #t) (k 0)))) (h "1lqsyjbd44ya6jp5wkrc5sqn3xgwa0427qmncsglyr9w1k41qc7s") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-assert_cli-0.2.0 (c (n "assert_cli") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0.21") (o #t) (d #t) (k 0)) (d (n "difference") (r "^0.4.0") (d #t) (k 0)))) (h "15spf2v4pgxr4cpcf1fpzvb9am3pb05si2is6b1b7dwqi1lz2yqf") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-assert_cli-0.2.1 (c (n "assert_cli") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0.23") (o #t) (d #t) (k 0)) (d (n "difference") (r "^0.4.0") (d #t) (k 0)))) (h "0f3kswq56a0nnq2n0yzd82mkisvwq2yy5fvcp0c4p6rizk2018j5") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-assert_cli-0.2.2 (c (n "assert_cli") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0.23") (o #t) (d #t) (k 0)) (d (n "difference") (r "^0.4.0") (d #t) (k 0)))) (h "05iqq601d88nj77fplhhp5lv5p22n4gh66jv1g86k4r5i9ywfajb") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-assert_cli-0.3.0 (c (n "assert_cli") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)) (d (n "clippy") (r "^0.0.23") (o #t) (d #t) (k 0)) (d (n "difference") (r "^0.4.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "0f1zrhvmf4cc6z6146w57c00rpgxkacv2142n3akj97yxz34a1i4") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-assert_cli-0.4.0 (c (n "assert_cli") (v "0.4.0") (d (list (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "difference") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "03y82qwmr44ymryqn1gcl9h9y1fny7xq9ir2x6kg2w369sg2imq3")))

(define-public crate-assert_cli-0.5.0 (c (n "assert_cli") (v "0.5.0") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "difference") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "17pll6wnch2wdhkqxh5qvl5l0w12y1zwvzym8r8jfpnxa6qrx9lc")))

(define-public crate-assert_cli-0.5.1 (c (n "assert_cli") (v "0.5.1") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "difference") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "18nhvywbwybl2v8nvgs4r6w51mzpjkmrsmf38xck3h5vhhrg43nk")))

(define-public crate-assert_cli-0.5.2 (c (n "assert_cli") (v "0.5.2") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "difference") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0j22x0rkpxgm7ic9xbmvvvfgi2kfiq80bq8mpgfi3afpz94mww83")))

(define-public crate-assert_cli-0.5.3 (c (n "assert_cli") (v "0.5.3") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "difference") (r "^1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "053a4xk5pr5iwjkhixsb97bwxg11h9jkpq760kjwb7jylrdfnqdl")))

(define-public crate-assert_cli-0.5.4 (c (n "assert_cli") (v "0.5.4") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "difference") (r "^1.0") (d #t) (k 0)) (d (n "environment") (r "^0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0c0rpn0has14ikx78vfk9m1radx8k5wvyjfqqbvvag3s0lhjqd3j")))

(define-public crate-assert_cli-0.5.6 (c (n "assert_cli") (v "0.5.6") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "environment") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l6f08qsyyqsqicg5cnwr97kdc8rk9vlm2mj0w2558s4mazad32c") (y #t)))

(define-public crate-assert_cli-0.6.0 (c (n "assert_cli") (v "0.6.0") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "environment") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02rk06qd6991pmn8mjq82ivrpnnf45r44nwjbdk64igminyrv9ax")))

(define-public crate-assert_cli-0.6.1 (c (n "assert_cli") (v "0.6.1") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "environment") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zj8y0s3jkdfqsp4hw3r16v2vv3aclhg44f72siw6plfbw8ilwrz")))

(define-public crate-assert_cli-0.6.2 (c (n "assert_cli") (v "0.6.2") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "environment") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lgchmgh3wlp1148mqgnwznfv6xpbcvbvvpwjl6m2v2s8q79nn4q")))

(define-public crate-assert_cli-0.6.3 (c (n "assert_cli") (v "0.6.3") (d (list (d (n "colored") (r "^1.5") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "environment") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jc1bh3cvnl66bl7s5gr1xnm0hl8d2l3gmil0pmhp5v2xp0bg6m2")))

