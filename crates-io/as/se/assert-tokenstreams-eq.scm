(define-module (crates-io as se assert-tokenstreams-eq) #:use-module (crates-io))

(define-public crate-assert-tokenstreams-eq-0.1.0 (c (n "assert-tokenstreams-eq") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 2)))) (h "17jymdl4i7cy5hf8w0h74yryamhhykkb3l7dyc6fabgi9j42ram5")))

