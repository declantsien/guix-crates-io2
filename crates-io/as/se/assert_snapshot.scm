(define-module (crates-io as se assert_snapshot) #:use-module (crates-io))

(define-public crate-assert_snapshot-0.1.0 (c (n "assert_snapshot") (v "0.1.0") (d (list (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "itertools") (r "~0.7.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "~1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.32") (d #t) (k 0)) (d (n "sorted-json") (r "~0.1.0") (d #t) (k 0)))) (h "1ymya1mza1srl8a44jfw55rx8lxd97md4cllqzdfqzqibd15adib")))

(define-public crate-assert_snapshot-0.1.1 (c (n "assert_snapshot") (v "0.1.1") (d (list (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "itertools") (r "~0.7.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "~1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.32") (d #t) (k 0)) (d (n "sorted-json") (r "~0.1.0") (d #t) (k 0)))) (h "1z6r2ivwa7n4mc6h4q1d7yrzl2746sp7rj6yk4s55z425l590lza")))

(define-public crate-assert_snapshot-0.1.2 (c (n "assert_snapshot") (v "0.1.2") (d (list (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "itertools") (r "~0.7.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "~1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.32") (d #t) (k 0)) (d (n "sorted-json") (r "~0.1.0") (d #t) (k 0)))) (h "1pn9l0i10961yahrc124xci5fh6dk5qsmj18da60275h0rjxmn4i")))

(define-public crate-assert_snapshot-0.1.3 (c (n "assert_snapshot") (v "0.1.3") (d (list (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "itertools") (r "~0.7.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "~1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.32") (d #t) (k 0)) (d (n "sorted-json") (r "~0.1.0") (d #t) (k 0)))) (h "167jzw4zwh01llnvwzin3bjw3jbn4kb707p4n827dq0qwrw51mz0")))

