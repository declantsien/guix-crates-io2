(define-module (crates-io as se asserteq_pretty) #:use-module (crates-io))

(define-public crate-asserteq_pretty-0.0.0 (c (n "asserteq_pretty") (v "0.0.0") (h "0ark9dg1191i1lrqbw85xd985qx9h66shf8na46kg0znflb3j5ji")))

(define-public crate-asserteq_pretty-0.0.1 (c (n "asserteq_pretty") (v "0.0.1") (d (list (d (n "asserteq_pretty_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)))) (h "1vd3r8rk204004q8rmsy9bqpbsf5rcxqwr7y9hwhf0mprga5lh8s")))

(define-public crate-asserteq_pretty-0.0.2 (c (n "asserteq_pretty") (v "0.0.2") (d (list (d (n "asserteq_pretty_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)))) (h "0ji2hbhb1v1fsvrfzsifdwh8n6v6l078ya3n8c7d2569ismv29i9")))

(define-public crate-asserteq_pretty-0.0.3 (c (n "asserteq_pretty") (v "0.0.3") (d (list (d (n "asserteq_pretty_macros") (r "^0.0.3") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)))) (h "1z1cyv3japcbrnbh913518w55ihwq987sdfgfn2jhjx1kisb3mxa")))

(define-public crate-asserteq_pretty-0.0.4 (c (n "asserteq_pretty") (v "0.0.4") (d (list (d (n "asserteq_pretty_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)))) (h "0ysxla55wa8sjzikwh1ifw10nnx4n0g2gv45ggbxfjdpzi5gfph0")))

(define-public crate-asserteq_pretty-0.0.5 (c (n "asserteq_pretty") (v "0.0.5") (d (list (d (n "asserteq_pretty_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.3") (d #t) (k 0)))) (h "0656mvg6ips5nnjh8kwsalr9193rv601xslhdz1kps9iflxcc9fc")))

