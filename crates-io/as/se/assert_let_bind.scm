(define-module (crates-io as se assert_let_bind) #:use-module (crates-io))

(define-public crate-assert_let_bind-0.1.0 (c (n "assert_let_bind") (v "0.1.0") (h "1zmkmby48yia44apzvvyfn5xdxdnyc5j2a3hw8f4yrvji45pbbyl")))

(define-public crate-assert_let_bind-0.1.1 (c (n "assert_let_bind") (v "0.1.1") (h "0xcjwzm1g242gagdqh3gc19bd0irxs2dabq4q90siv9r6p09pni6")))

