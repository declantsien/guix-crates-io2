(define-module (crates-io as se assertify) #:use-module (crates-io))

(define-public crate-assertify-0.5.0 (c (n "assertify") (v "0.5.0") (d (list (d (n "assertify_proc_macros") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "18a6pfwn0z82v2f106nsqvp998jdb7p0sw7bhvcc3423kn68j9lz")))

(define-public crate-assertify-0.6.0 (c (n "assertify") (v "0.6.0") (d (list (d (n "assertify_proc_macros") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1l4lfmcdl7d7j44lq36c1pvqk94inw50c34k37ln3xhs71k635z5")))

(define-public crate-assertify-0.6.1 (c (n "assertify") (v "0.6.1") (d (list (d (n "assertify_proc_macros") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ggxvdf1q1fwbx57xcmcladc3dy7cplh97bdhc3fv2a87x4g2dzg")))

(define-public crate-assertify-0.7.0 (c (n "assertify") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0911acpjj689rbqcv26k5xikcak621v6lg57jw4h5fr4h1h2cb9p")))

(define-public crate-assertify-0.7.1 (c (n "assertify") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0k89lk6c23p0p8hf1dg5fyb1ap4slz6rnh7niw4gaigk98bblzm0")))

