(define-module (crates-io as se asset-derive-macro) #:use-module (crates-io))

(define-public crate-asset-derive-macro-0.1.2 (c (n "asset-derive-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1asl4bvk5q94njhz759g8lbsbxcgha5gln9qsp1q4bk0jkrjdz9j")))

(define-public crate-asset-derive-macro-0.1.3 (c (n "asset-derive-macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1isp1225fj7qxhcx21ddsg82qapgmpz1p33gb7j47m2x7izfrbw7")))

