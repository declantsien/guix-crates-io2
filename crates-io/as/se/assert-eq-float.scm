(define-module (crates-io as se assert-eq-float) #:use-module (crates-io))

(define-public crate-assert-eq-float-0.1.0 (c (n "assert-eq-float") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0dyf7vmz7csmk2gnx5ar7c4vdkzmhzpf8v6scic8vc49232wz8ly") (y #t) (r "1.56")))

(define-public crate-assert-eq-float-0.1.1 (c (n "assert-eq-float") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1gp48sk0mslfcnwh747jgvg43zv4pi2ayr6g9rypaiq1lh57xh6s") (r "1.56")))

(define-public crate-assert-eq-float-0.1.2 (c (n "assert-eq-float") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "063a59hfqiqg2y2gv6pja8grq5r718xgvrjw0sl6mm16vw63ksgx") (r "1.56")))

(define-public crate-assert-eq-float-0.1.3 (c (n "assert-eq-float") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "13hnyia8mdv6namy8ppy44vwia5js86g55n5pfp5l3f45jyw5ibz") (r "1.56")))

(define-public crate-assert-eq-float-0.1.4 (c (n "assert-eq-float") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1y2hb46hm4cvxxy0bwng9gv4vq48i4scynflhqr5d2ijwzsd3c8w") (r "1.56")))

