(define-module (crates-io as se assert_size) #:use-module (crates-io))

(define-public crate-assert_size-0.1.0 (c (n "assert_size") (v "0.1.0") (h "0jr6nyqsk7ld1yrqvg8rn74dqv5vd4zxsbiliaf9p4pmdx9ycx2x")))

(define-public crate-assert_size-0.1.1 (c (n "assert_size") (v "0.1.1") (h "0iwx921wvrgyallqbwslanw2m164x2149axgqmg9r9yzbzadyjym")))

