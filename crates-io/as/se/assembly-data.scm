(define-module (crates-io as se assembly-data) #:use-module (crates-io))

(define-public crate-assembly-data-0.1.0 (c (n "assembly-data") (v "0.1.0") (d (list (d (n "assembly-core") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "hsieh-hash") (r "^0.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 2)))) (h "0llwpmckha3q311ci0aahz0yx0yvhx4y6vsmz6lfai1l02llklh0")))

(define-public crate-assembly-data-0.2.0-beta.0 (c (n "assembly-data") (v "0.2.0-beta.0") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 2)) (d (n "assembly-core") (r ">=0.2.0-beta, <0.3.0") (d #t) (k 0)) (d (n "derive-new") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "encoding_rs") (r ">=0.8.0, <0.9.0") (d #t) (k 0)) (d (n "getopts") (r ">=0.2.0, <0.3.0") (d #t) (k 2)) (d (n "hsieh-hash") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "memchr") (r ">=2.3.0, <3.0.0") (d #t) (k 0)) (d (n "memmap") (r ">=0.7.0, <0.8.0") (d #t) (k 2)) (d (n "prettytable-rs") (r ">=0.8.0, <0.9.0") (d #t) (k 2)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "068946yw1xhcrkdh22slrmw5yanhxxswi7bi3d6hxkw5vmsyb8d8")))

(define-public crate-assembly-data-0.2.0-beta.1 (c (n "assembly-data") (v "0.2.0-beta.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "assembly-core") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "hsieh-hash") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zg41gga7jdrvfqhmdlc2486b1z2161pbhkdhbd8w90icsz40nwl")))

(define-public crate-assembly-data-0.2.0-beta.2 (c (n "assembly-data") (v "0.2.0-beta.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "assembly-core") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "hsieh-hash") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1px6bgl37fx2pawz12w0sgiw12pirnd8icsgdk4pq6jixfz9wcza")))

(define-public crate-assembly-data-0.2.0-beta.3 (c (n "assembly-data") (v "0.2.0-beta.3") (d (list (d (n "assembly-core") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "hsieh-hash") (r "^0.1") (d #t) (k 0)) (d (n "mapr") (r "^0.8") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01p05wwvxz7qzs5dhq4yv2qw3k24dgic91bgicdrwv91yq3dxn64")))

(define-public crate-assembly-data-0.2.0 (c (n "assembly-data") (v "0.2.0") (d (list (d (n "assembly-core") (r "^0.2.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "hsieh-hash") (r "^0.1") (d #t) (k 0)) (d (n "mapr") (r "^0.8") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q78qlrqkivpk1gjj8kr8fdrv4mchyi61m37ndyg5p29zlxgbw2s")))

(define-public crate-assembly-data-0.2.1 (c (n "assembly-data") (v "0.2.1") (d (list (d (n "assembly-core") (r "^0.2.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "hsieh-hash") (r "^0.1") (d #t) (k 0)) (d (n "mapr") (r "^0.8") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 2)) (d (n "quick-xml") (r "^0.20") (f (quote ("encoding"))) (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jnis8vhi6mkqqwa9mad7cw19xmchmnik8dja1670ldx1gf2na4j")))

(define-public crate-assembly-data-0.2.2-beta.0 (c (n "assembly-data") (v "0.2.2-beta.0") (d (list (d (n "assembly-core") (r "^0.2.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "hsieh-hash") (r "^0.1") (d #t) (k 0)) (d (n "mapr") (r "^0.8") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 2)) (d (n "quick-xml") (r "^0.20") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nkwx5ljx0f3p2dgqhjf4h108is0iqc115w3bjfj0dq5ak269g25")))

(define-public crate-assembly-data-0.2.2-beta.1 (c (n "assembly-data") (v "0.2.2-beta.1") (d (list (d (n "assembly-core") (r "^0.2.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "hsieh-hash") (r "^0.1") (d #t) (k 0)) (d (n "mapr") (r "^0.8") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 2)) (d (n "quick-xml") (r "^0.20") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled"))) (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jk97cpaca6cm00mn63v2yagy24vmf5hk2r5mqvlln9qi8fa87r0") (f (quote (("sqlite" "rusqlite") ("default" "sqlite"))))))

(define-public crate-assembly-data-0.3.0-beta.0 (c (n "assembly-data") (v "0.3.0-beta.0") (d (list (d (n "assembly-core") (r "^0.2.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "hsieh-hash") (r "^0.1") (d #t) (k 0)) (d (n "mapr") (r "^0.8") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 2)) (d (n "quick-xml") (r "^0.20") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06h3jhjgyhs4kab8lvbqnbl2wl96mac5n2a3ywbs0a1042a4swjy") (f (quote (("sqlite" "rusqlite") ("serde-derives" "serde" "quick-xml/serialize") ("default" "sqlite" "serde-derives"))))))

(define-public crate-assembly-data-0.3.0 (c (n "assembly-data") (v "0.3.0") (d (list (d (n "assembly-core") (r "^0.2.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 2)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "hsieh-hash") (r "^0.1") (d #t) (k 0)) (d (n "mapr") (r "^0.8") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 2)) (d (n "quick-xml") (r "^0.20") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1znmwpky6hi4j9ci5p3jfaw9c8r969w8zqi8zjlp3b70aaqszymn") (f (quote (("sqlite" "rusqlite") ("serde-derives" "serde" "quick-xml/serialize") ("default" "sqlite" "serde-derives"))))))

