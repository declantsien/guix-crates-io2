(define-module (crates-io as se assert-not-modified) #:use-module (crates-io))

(define-public crate-assert-not-modified-0.1.0 (c (n "assert-not-modified") (v "0.1.0") (h "0mxldw2hkx979ga8353k3bg5yy5agacbqbq80041pnjnd9rd103w")))

(define-public crate-assert-not-modified-0.1.1 (c (n "assert-not-modified") (v "0.1.1") (h "0l9w3d91p0sb2gw4rzm1dva5rsh6bfh9rh7h6bxz3i8gv89w79w0")))

(define-public crate-assert-not-modified-0.1.2 (c (n "assert-not-modified") (v "0.1.2") (h "0xavrq6lbi8fz0q54anbj6sxbhwa8v41rh8j0caqyg93kqjaqj9a")))

(define-public crate-assert-not-modified-1.0.0 (c (n "assert-not-modified") (v "1.0.0") (h "00wxnwxknvvcghwaqr4ic0s5l69sk8agil2anxm8fzvnkkpvj2la")))

