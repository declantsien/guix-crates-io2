(define-module (crates-io as se assert_panic_free) #:use-module (crates-io))

(define-public crate-assert_panic_free-1.0.0 (c (n "assert_panic_free") (v "1.0.0") (h "10cs5wmrcms4n309k59zdmglq1bgny1agn1lrjdjzdfi5hpqj45g")))

(define-public crate-assert_panic_free-1.0.1 (c (n "assert_panic_free") (v "1.0.1") (h "0083nz7jccxsp485dgj8gr6p9wxzs3fxhqsd9s2vr01lp6vrmpxm")))

(define-public crate-assert_panic_free-1.0.2 (c (n "assert_panic_free") (v "1.0.2") (h "07m87v2i22bc0x8xyywhd5rxa6z3kqaj5496ikz3qvkh5chxfmrw")))

