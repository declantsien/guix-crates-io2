(define-module (crates-io as se assemblylift-iomod-lambda-guest) #:use-module (crates-io))

(define-public crate-assemblylift-iomod-lambda-guest-0.1.1 (c (n "assemblylift-iomod-lambda-guest") (v "0.1.1") (d (list (d (n "assemblylift_core_io_guest") (r "^0.3") (d #t) (k 0) (p "assemblylift-core-io-guest")) (d (n "assemblylift_core_iomod_guest") (r "^0.3") (d #t) (k 0) (p "assemblylift-core-iomod-guest")) (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18ashpi294zi7m6d5g89afpnzbvaj1wnvrvmbrjj25wyq32ih71n")))

