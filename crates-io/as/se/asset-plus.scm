(define-module (crates-io as se asset-plus) #:use-module (crates-io))

(define-public crate-asset-plus-0.1.0 (c (n "asset-plus") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.5") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "14rfssmcpa8486289vvdbkra0zd1jd93bhcxw7vmyqbp347ylwg9") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces")))) (y #t)))

