(define-module (crates-io as se assembunny_plus) #:use-module (crates-io))

(define-public crate-assembunny_plus-0.0.2 (c (n "assembunny_plus") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "maplit") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1zk99gb47f88is2v1vhqd35ff5g5flddaiyq2b377j7lib0vjwr2")))

(define-public crate-assembunny_plus-0.0.3 (c (n "assembunny_plus") (v "0.0.3") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "maplit") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0yf227px93f9xy9i5r5hxfvnhgmhc1jxim23ag0bd4ynxfyv2vs1")))

