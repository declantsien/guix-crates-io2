(define-module (crates-io as se asserteq_pretty_macros) #:use-module (crates-io))

(define-public crate-asserteq_pretty_macros-0.0.1 (c (n "asserteq_pretty_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)))) (h "064k3nk7z9zq8b5qxjg0ajxsk5gpwqih4sbs46ksy04y0zlplrz8")))

(define-public crate-asserteq_pretty_macros-0.0.2 (c (n "asserteq_pretty_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hr33yzbx3lqv14yjpdazhk5afj5pxzk54c7sl4njgh3zgwb4fz8")))

(define-public crate-asserteq_pretty_macros-0.0.3 (c (n "asserteq_pretty_macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0p3wd3rhp45l3isiri3k8xja6db9ibya0yx9jd4hq08ybn51jgzk")))

(define-public crate-asserteq_pretty_macros-0.0.4 (c (n "asserteq_pretty_macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0g209xxp0k07n31ih3gkky6paswcm9813kfd9masl6pbfzf1yirj")))

