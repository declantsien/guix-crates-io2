(define-module (crates-io as se assert_into) #:use-module (crates-io))

(define-public crate-assert_into-1.0.0 (c (n "assert_into") (v "1.0.0") (h "1y9kv3znrdx2d3lfyb2ajqykxaqgynfvk58axpkvii9y98jiv48j")))

(define-public crate-assert_into-1.0.1 (c (n "assert_into") (v "1.0.1") (h "0g3ni7d9lfz9c5sffml688c3ka0vdab9nxb42838pbr9wihjyznb")))

(define-public crate-assert_into-1.0.2 (c (n "assert_into") (v "1.0.2") (h "0870q8p5agv4fcqb37ymxyxrqs3hxmqlqz72s4d88rhmgy586jfs")))

(define-public crate-assert_into-1.0.3 (c (n "assert_into") (v "1.0.3") (h "0chwvmkl0gjfdbiy6zjhbli585x6mhmj6kmrc0m9iq307g3y2zfm")))

(define-public crate-assert_into-1.1.0 (c (n "assert_into") (v "1.1.0") (h "1dgj46w2lli8yvlkdsx1yinl30y197bm68yjbra3bh7jwwbnzsb3")))

