(define-module (crates-io as se asset-derive) #:use-module (crates-io))

(define-public crate-asset-derive-0.1.0 (c (n "asset-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "031mprgcwi40fvjk96cvymdd699wv43m79hkqimkd5qwqiakbb8s")))

(define-public crate-asset-derive-0.1.1 (c (n "asset-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hbvydn8gqjd6p2p4rpcg41z8qh08f32mnr00zj0z9a48h9df5ax")))

(define-public crate-asset-derive-0.1.2 (c (n "asset-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r0yn9ifb2v6qvkrpdn7v67lh4wdd450rc1zj0ncqyprvy7n0wrp")))

(define-public crate-asset-derive-0.1.3 (c (n "asset-derive") (v "0.1.3") (d (list (d (n "asset-derive-macro") (r "^0.1.2") (d #t) (k 0)))) (h "0da5i33190yygq5vikm5mxh4zjfcgznl21g8m6hdni6zy3msy013")))

(define-public crate-asset-derive-0.1.4 (c (n "asset-derive") (v "0.1.4") (d (list (d (n "asset-derive-macro") (r "^0.1.3") (d #t) (k 0)))) (h "0n58ciymjgmxyrvq3jgk40jq69lv4l9lh2k12zps0i05j587pmv4")))

