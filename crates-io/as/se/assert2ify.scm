(define-module (crates-io as se assert2ify) #:use-module (crates-io))

(define-public crate-assert2ify-0.1.0-alpha1 (c (n "assert2ify") (v "0.1.0-alpha1") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 0)) (d (n "assert2ify-macros") (r "^0.1.0-alpha1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0n7bsbn6yh2agx0isfr7di4y21vk6yyvxa0hlw4k0hxlzln21sgv")))

