(define-module (crates-io as se assert_infrequent) #:use-module (crates-io))

(define-public crate-assert_infrequent-0.1.0 (c (n "assert_infrequent") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "0l5vmgd3b48cxk4pck3md8g4j0lmdf26sccd8naz80c957w5ds18")))

