(define-module (crates-io as se asset-resolver) #:use-module (crates-io))

(define-public crate-asset-resolver-0.1.0 (c (n "asset-resolver") (v "0.1.0") (h "0y4l7ynpj457kj9l522vlfv0ig2iybnfyp5j0cidcv4j9lx6kbv1")))

(define-public crate-asset-resolver-0.1.1 (c (n "asset-resolver") (v "0.1.1") (h "1cv9p4jdnqwa7c6ks1ywqnbjf64v1q4yf7xhyg8w1r13zmpvnyhf")))

