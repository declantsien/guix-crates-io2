(define-module (crates-io as se assetmanager) #:use-module (crates-io))

(define-public crate-assetmanager-0.1.0 (c (n "assetmanager") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.26.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.4.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0n71am0w9c0lxswz6z4d1zrkljm4rzj3gnbxb39p937wasfsz29h") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

