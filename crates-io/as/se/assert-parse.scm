(define-module (crates-io as se assert-parse) #:use-module (crates-io))

(define-public crate-assert-parse-0.1.0 (c (n "assert-parse") (v "0.1.0") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 0)))) (h "0981drs7kamaf1np3239b2d5djdrmvy85vk928rn68cvg8j5b9f1")))

(define-public crate-assert-parse-0.2.0 (c (n "assert-parse") (v "0.2.0") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 0)) (d (n "assert-parse-register-assert-macro") (r "^1.0") (d #t) (k 0)))) (h "1sbyxp4sggdbr07339j5zx1w2bykkci33vrbl20shdifhady1jxa")))

(define-public crate-assert-parse-0.2.1 (c (n "assert-parse") (v "0.2.1") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 0)) (d (n "assert-parse-register-assert-macro") (r "^1.0") (d #t) (k 0)))) (h "0xznxa4ald0r5g5inqk7dd5m02idg1lbkv1q7bhvlqxci0q4c32w")))

(define-public crate-assert-parse-1.0.0 (c (n "assert-parse") (v "1.0.0") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 0)) (d (n "assert-parse-register-assert-macro") (r "^1.0") (d #t) (k 0)))) (h "1jbcyn8vpkymqk88kj36rpv0i5zl4mcwrbqf79bbyqfx7wsw9xx1")))

(define-public crate-assert-parse-1.0.1 (c (n "assert-parse") (v "1.0.1") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 0)) (d (n "assert-parse-register-assert-macro") (r "^1.0") (d #t) (k 0)))) (h "1ba63caik10ibggnlki5w0bps0d35lcgvg94q0gx3h1x437l3bgm")))

(define-public crate-assert-parse-1.0.2 (c (n "assert-parse") (v "1.0.2") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 0)) (d (n "assert-parse-register-assert-macro") (r "^1.0") (d #t) (k 0)))) (h "0f89q9aa3v28wpahb4kb9vi3gmaf9m9k2hbknxgc8jfwvkn15zdl")))

