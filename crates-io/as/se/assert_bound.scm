(define-module (crates-io as se assert_bound) #:use-module (crates-io))

(define-public crate-assert_bound-0.1.0 (c (n "assert_bound") (v "0.1.0") (h "1a64jws8q6sw1f7jiz3nnyv6h4r0aqvid4yzjnr1xwykdkasnllh")))

(define-public crate-assert_bound-0.1.1 (c (n "assert_bound") (v "0.1.1") (h "0xhsls953n8fzxn9vkrwzpnxm9kg69v94jlccc97w6m54vr03hsw")))

