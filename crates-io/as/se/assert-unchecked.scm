(define-module (crates-io as se assert-unchecked) #:use-module (crates-io))

(define-public crate-assert-unchecked-0.1.0 (c (n "assert-unchecked") (v "0.1.0") (h "1kkkwkmblzqbn0qmxxpbchm21bhyy2rggjz116y0yrw9r34lpkfp")))

(define-public crate-assert-unchecked-0.1.1 (c (n "assert-unchecked") (v "0.1.1") (h "0y29aqmk0y8rjq1xwmwagf100rqrd51jfgaibc4x98xd8migqzp5")))

(define-public crate-assert-unchecked-0.1.2 (c (n "assert-unchecked") (v "0.1.2") (h "15f7wp5svnckx7km91ipa7bs045l5pdv91rma7iy4zl4vwm5jc3k")))

