(define-module (crates-io as se assert-parse-register-assert-macro) #:use-module (crates-io))

(define-public crate-assert-parse-register-assert-macro-1.0.0 (c (n "assert-parse-register-assert-macro") (v "1.0.0") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02png460s3vhq5a68chp0y549bdckci5fawjm5d3p6f42cpbv8wx")))

(define-public crate-assert-parse-register-assert-macro-1.0.1 (c (n "assert-parse-register-assert-macro") (v "1.0.1") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s15h69db6n4jhhvsan1x8cv1snawz1fg6s9k6b5sj1a3vx6xsc6")))

(define-public crate-assert-parse-register-assert-macro-1.0.2 (c (n "assert-parse-register-assert-macro") (v "1.0.2") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0za3jc18xyb6lvc87z7fnpjkkq18ynz1si585qwkwfk6a8axi1nx")))

(define-public crate-assert-parse-register-assert-macro-1.0.3 (c (n "assert-parse-register-assert-macro") (v "1.0.3") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mfmzxwd5srg2s3svra1lbna6m77bnrpr1frx10qrhvzp6z3adx2")))

(define-public crate-assert-parse-register-assert-macro-1.0.4 (c (n "assert-parse-register-assert-macro") (v "1.0.4") (d (list (d (n "assert-parse-core") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.13") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qaravpznjng17qmnb4gi4vzwaqx20kh9a8nn1sf9qfk5nwbmz8j")))

