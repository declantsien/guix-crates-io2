(define-module (crates-io as se assert_tokens_eq) #:use-module (crates-io))

(define-public crate-assert_tokens_eq-0.1.0 (c (n "assert_tokens_eq") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.0") (d #t) (k 0)) (d (n "ctor") (r "^0.1.9") (d #t) (t "cfg(windows)") (k 0)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "output_vt100") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 2)) (d (n "snafu") (r "^0.4.3") (d #t) (k 0)))) (h "0jginhzxkawgws61imrbrjdr69grdlcmych62c1mb2swpb2z88bb")))

