(define-module (crates-io as se assert_unordered) #:use-module (crates-io))

(define-public crate-assert_unordered-0.1.0 (c (n "assert_unordered") (v "0.1.0") (h "05h5gg1y8b4mrv7qngp2sr232ifajcvhdicdjvcwghfdr7gkvsvd") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_unordered-0.1.1 (c (n "assert_unordered") (v "0.1.1") (h "1q2h1d72pvkf1vyrp74169iy7kf8vq39virbcw4la5qhjj04wil9") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_unordered-0.2.0 (c (n "assert_unordered") (v "0.2.0") (h "0jmi15jaz392487jl90m6090zi33zpc5mbnxb4f8gjmd060hh1s6") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_unordered-0.2.1 (c (n "assert_unordered") (v "0.2.1") (h "0mvqd962n7mmin336q0dj08jamg8pdhyf3qgpp67yzljh3mfghsb") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_unordered-0.3.0 (c (n "assert_unordered") (v "0.3.0") (h "0hnxrfznfn4qyzndfy1gw7wkvvkwml3ncv3qsjci6lax5122bmn5") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_unordered-0.3.1 (c (n "assert_unordered") (v "0.3.1") (h "0l22z2i9panxiqmlx2q5fjp4bh87az90ivmppljw65clr6k92nsi") (f (quote (("std") ("default" "std"))))))

(define-public crate-assert_unordered-0.3.2 (c (n "assert_unordered") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1l8wlfpxmania49kv6daxbl2z3jch81sbb5k4gi9y32yh0hdja5s") (f (quote (("std") ("default" "color") ("color" "ansi_term" "std"))))))

(define-public crate-assert_unordered-0.3.3 (c (n "assert_unordered") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)))) (h "012p0yi5ambb7vhiz9hwjy3zymgfn4wjcvyn6bpx6zlag27nip1g") (f (quote (("std") ("default" "color") ("color" "ansi_term" "std"))))))

(define-public crate-assert_unordered-0.3.4 (c (n "assert_unordered") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1z9685g8q33gs1bphpd0v0d25qh4rdp0j66s9wz7qg07hbrncixr") (f (quote (("std") ("default" "color") ("color" "ansi_term" "std"))))))

(define-public crate-assert_unordered-0.3.5 (c (n "assert_unordered") (v "0.3.5") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0r123d4az6rhivpsymwmg0k4hn9iagp0hkhka6rkwcl1g0xk4x0w") (f (quote (("std") ("default" "color") ("color" "ansi_term" "std"))))))

