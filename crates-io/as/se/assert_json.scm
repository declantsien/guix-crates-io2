(define-module (crates-io as se assert_json) #:use-module (crates-io))

(define-public crate-assert_json-0.1.0 (c (n "assert_json") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0aw2ivzqwd59m0i6mzi956vn46mm8rhs9pb5g93br1mg79dhsmg0")))

