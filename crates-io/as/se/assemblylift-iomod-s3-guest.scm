(define-module (crates-io as se assemblylift-iomod-s3-guest) #:use-module (crates-io))

(define-public crate-assemblylift-iomod-s3-guest-0.1.1 (c (n "assemblylift-iomod-s3-guest") (v "0.1.1") (d (list (d (n "assemblylift_core_io_guest") (r "^0.3") (d #t) (k 0) (p "assemblylift-core-io-guest")) (d (n "assemblylift_core_iomod_guest") (r "^0.3") (d #t) (k 0) (p "assemblylift-core-iomod-guest")) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0jdr3qicpqk2zkd3xdq6s9z8vw7pafvvm65mbh84cmr5yq8jbxj4")))

