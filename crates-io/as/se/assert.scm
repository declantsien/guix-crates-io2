(define-module (crates-io as se assert) #:use-module (crates-io))

(define-public crate-assert-0.0.1 (c (n "assert") (v "0.0.1") (h "01wxizab58nn8nc50c938qm79bhmys2b5139wi3cp1n5hkpvs3w3")))

(define-public crate-assert-0.0.2 (c (n "assert") (v "0.0.2") (h "1cg1fv9p6pk8k96rxf0d0lzhsysdlwjl2jd5r7zkclq6j71c02cd")))

(define-public crate-assert-0.0.3 (c (n "assert") (v "0.0.3") (h "0flavjnkcq10pyhlagy373a9ygy1hl3j51jy711qsclbc6v1525d")))

(define-public crate-assert-0.0.4 (c (n "assert") (v "0.0.4") (h "0i68v0yl6nznlrlh4qb85ad1h78kyym1wqq8wzp8qbwkcvy2rbfn")))

(define-public crate-assert-0.1.0 (c (n "assert") (v "0.1.0") (h "0kv9pp7326hj22yqydnac082g9h37irqni25m2gnbk5ds0w71gsp")))

(define-public crate-assert-0.1.1 (c (n "assert") (v "0.1.1") (h "1cbd1sy2v5zlc0qcyds3hw6sy3w6nvsfa0yqj2q4jq60kf58yxam")))

(define-public crate-assert-0.1.2 (c (n "assert") (v "0.1.2") (h "0fa8x3ppldnyk0vpbj2277m1gyi58xgx767aj80f49gd7036hiyz")))

(define-public crate-assert-0.1.3 (c (n "assert") (v "0.1.3") (h "0c5rbbp5g5mq4jlgzchbr0mqas21q0sb9af833p98llfz46x5zsi")))

(define-public crate-assert-0.1.4 (c (n "assert") (v "0.1.4") (h "1wgcsfl29gfn0irjnbmsgffz56qdnvzb1y2gmwbrgrf25wny4i8b")))

(define-public crate-assert-0.1.5 (c (n "assert") (v "0.1.5") (h "0mk84dggzpi0zmrplivlqapghw2xp534mv7jnkz58ylmbwm3rm56")))

(define-public crate-assert-0.1.6 (c (n "assert") (v "0.1.6") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "066blz9xqm61ycwhlsqnnl4i0dk4lsbv9gyfg8h207jk86w26d6b")))

(define-public crate-assert-0.2.0 (c (n "assert") (v "0.2.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1bylbk3qnqjviay9xxmjq8y0w1y7vywcyz5k5kyjv1njxdkfcgsd")))

(define-public crate-assert-0.2.1 (c (n "assert") (v "0.2.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1vl0i03638rn62ys8jyp0h3w0j150jkn9b49h85mx9anr464rf40")))

(define-public crate-assert-0.2.2 (c (n "assert") (v "0.2.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1hj8kqcw7kgzl0apprrhlngrrda1lrdwdqqwjzwmd5ixdbvfd0sc")))

(define-public crate-assert-0.2.3 (c (n "assert") (v "0.2.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1n3i1lvc7zfsnkq27391cgnhiq287f74b87flxc10mdyqrncyc9h")))

(define-public crate-assert-0.3.0 (c (n "assert") (v "0.3.0") (h "184lhwpapzi88crgbhixxfkcq9g7zkh6lsanj5bvl639cp5v9rk1")))

(define-public crate-assert-0.4.0 (c (n "assert") (v "0.4.0") (h "0p5rw3pr2rbv1gh7y714wyyfsk8m67k35jd7qxava32iqrgkwaf9")))

(define-public crate-assert-0.5.0 (c (n "assert") (v "0.5.0") (h "0ys6bnd1q65n10bh0kffijgi1wjx9f04jmv083jbz7wc1rsw9hng")))

(define-public crate-assert-0.5.1 (c (n "assert") (v "0.5.1") (h "1l8da6i7vwj1bfv94j32a658l29ngby8kxpwjy3d23sbv17xkcan")))

(define-public crate-assert-0.6.0 (c (n "assert") (v "0.6.0") (h "11hyynhp6691i2xjrxmgrpxrkgv38bapabf4b787sg1nlbq0l5nc")))

(define-public crate-assert-0.6.1 (c (n "assert") (v "0.6.1") (h "0r5cm8k01nf7xkajyzlfzy7hlcmsrdnhkchbnq1y8rlfn18kvvvv")))

(define-public crate-assert-0.7.0 (c (n "assert") (v "0.7.0") (h "12irzff5pq0gbymarcfdy3v7icsdcsiqld8zwzl5xnvky06xklr5")))

(define-public crate-assert-0.7.1 (c (n "assert") (v "0.7.1") (h "1m46k69spipavmrr6miy5jz6mnmgv2fc10sj3292mmp6a6k3vi74")))

(define-public crate-assert-0.7.2 (c (n "assert") (v "0.7.2") (h "1vgkqdb7qiv1n93kj8i9s2jb8q6hcvqcq499h9bgl8jqbnjzkynl")))

(define-public crate-assert-0.7.3 (c (n "assert") (v "0.7.3") (h "1fviyx6xqx6pmsycbv3nmdadajpas2s9km6smfz1mrax32kxrkax")))

(define-public crate-assert-0.7.4 (c (n "assert") (v "0.7.4") (h "1hnph97yz3hpf71s4rdzcrgd492d1m7rj0k48ymxl1w5d2g8v450")))

(define-public crate-assert-0.7.5 (c (n "assert") (v "0.7.5") (h "1fxwnw8ypr6i6bxsy4ggzpjb70aysl044nfzyl3q8kyyv25zqisn")))

