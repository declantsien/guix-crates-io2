(define-module (crates-io as se assemblylift-core-io-guest) #:use-module (crates-io))

(define-public crate-assemblylift-core-io-guest-0.1.0 (c (n "assemblylift-core-io-guest") (v "0.1.0") (d (list (d (n "assemblylift-core-io-common") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "03a4zpmn0qacnwfbc5862xj8savdr2lpq52nq7mrcfz9mnfnh82v")))

(define-public crate-assemblylift-core-io-guest-0.2.0 (c (n "assemblylift-core-io-guest") (v "0.2.0") (d (list (d (n "assemblylift-core-io-common") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0vp0iq32rhkzvqwamh6x6dk4jvvz8jgn6sgc573km47f0dhmvy1n")))

(define-public crate-assemblylift-core-io-guest-0.2.1 (c (n "assemblylift-core-io-guest") (v "0.2.1") (d (list (d (n "assemblylift-core-io-common") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0g5dldg6x3qa5zrhjvr872ikk2kayrbnsamz6li9jzqxmpi4sypd")))

(define-public crate-assemblylift-core-io-guest-0.3.0 (c (n "assemblylift-core-io-guest") (v "0.3.0") (d (list (d (n "assemblylift-core-io-common") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0f880srih5g3vdx4dvz8k12pq37x1fjh1b9n4kx4907l6fj2ddjn")))

(define-public crate-assemblylift-core-io-guest-0.3.1 (c (n "assemblylift-core-io-guest") (v "0.3.1") (d (list (d (n "assemblylift-core-io-common") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gf9v2225m9hf3h9vzz1c18hgqlaxfvv5rji4jh7iryzhnzqpczr")))

(define-public crate-assemblylift-core-io-guest-0.3.2 (c (n "assemblylift-core-io-guest") (v "0.3.2") (d (list (d (n "assemblylift-core-io-common") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "160kvkab1h8wr0gbbf2cpn7zg74rvqh7bq4ajp611bky5p3lp620")))

(define-public crate-assemblylift-core-io-guest-0.4.0-alpha.0 (c (n "assemblylift-core-io-guest") (v "0.4.0-alpha.0") (d (list (d (n "assemblylift-core-io-common") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0frhkc7fg9hijm0y8i3rvrr80q7ib73kfhq9mmwnhyrypafs63n6")))

(define-public crate-assemblylift-core-io-guest-0.4.0-alpha.10 (c (n "assemblylift-core-io-guest") (v "0.4.0-alpha.10") (d (list (d (n "assemblylift-core-io-common") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wb2689g8x9l0rzcf6jiqwygn0z43szlbp33qq7i3xxvmdixdsjn")))

