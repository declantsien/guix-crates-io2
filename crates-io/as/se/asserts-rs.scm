(define-module (crates-io as se asserts-rs) #:use-module (crates-io))

(define-public crate-asserts-rs-0.1.0 (c (n "asserts-rs") (v "0.1.0") (h "0m78qdskva6cqc427p38f0h6kl1pph8rja6jb1irq85h3jk48a1l")))

(define-public crate-asserts-rs-0.2.0 (c (n "asserts-rs") (v "0.2.0") (h "05wbvxwarl5n91mmz0j51zkrc720rfblbnslblfvqy6mcc1z000w")))

(define-public crate-asserts-rs-0.2.1 (c (n "asserts-rs") (v "0.2.1") (h "1dr84kk6l2nhaqmja518wrzg9myh0s5z0cs01f1haxvv261n721r")))

(define-public crate-asserts-rs-0.3.0 (c (n "asserts-rs") (v "0.3.0") (h "05q2s8hhix13fm9w1szs6l096nq89yls9frvhnwx396v3l39lca2")))

