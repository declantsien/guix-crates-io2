(define-module (crates-io as se assets_manager-rodio) #:use-module (crates-io))

(define-public crate-assets_manager-rodio-0.1.0 (c (n "assets_manager-rodio") (v "0.1.0") (d (list (d (n "assets_manager") (r "^0.11") (k 0)) (d (n "rodio") (r "^0.18") (k 0)))) (h "02bpjb4piak4g7khfa2l07k4x0n8ic883j9hymkg2j121jfd78h2") (f (quote (("wav" "rodio/wav") ("vorbis" "rodio/vorbis") ("symphonia-wav" "rodio/symphonia-wav") ("symphonia-vorbis" "rodio/symphonia-vorbis") ("symphonia-mp3" "rodio/symphonia-mp3") ("symphonia-flac" "rodio/symphonia-flac") ("minimp3" "rodio/minimp3") ("flac" "rodio/flac"))))))

