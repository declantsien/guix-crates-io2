(define-module (crates-io as se assembler) #:use-module (crates-io))

(define-public crate-assembler-0.1.0 (c (n "assembler") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0lc3hblm7x3hmsfmck1zszpk8ff3j7yhz79hlccxxhg3k4ykwiwb")))

(define-public crate-assembler-0.1.1 (c (n "assembler") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0djc8szd2w3wicr9ppi8s9jgpxnbww7hxd4192c9bl7j3inljzw0")))

(define-public crate-assembler-0.1.2 (c (n "assembler") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1zpjl0ffjw1nqvqbfl0nf6fym3d12b1sak182v898hi77x58wgzq")))

(define-public crate-assembler-0.1.3 (c (n "assembler") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1k3l8c5mf01pfl5zmh1cawzw41h06wi1jnsx4zyxxcdldc8jqajd") (y #t)))

(define-public crate-assembler-0.1.4 (c (n "assembler") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "04cjqxmb6rvn60pl1g5wp69zmj21giblwia68ilhnaqijx85sr46") (y #t)))

(define-public crate-assembler-0.1.5 (c (n "assembler") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1z7dk4nix2qsvdlskwa1nkm8ai0w3n41i19jhqypgyw8957ncn2s")))

(define-public crate-assembler-0.2.0 (c (n "assembler") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0csxhnp9vq514h3rv8azgjrf4wn6vn6b7f184sqldp4w4qcalrn6")))

(define-public crate-assembler-0.2.1 (c (n "assembler") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "01qfnqpwmw1ml98hnjfsgk2647z649k8acfiqkp8xz7x4a1l03mq")))

(define-public crate-assembler-0.3.0 (c (n "assembler") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1rlbcacsia0s19xjjk67k1f3kgsvd3fy3lqj20la5ixvlml9nisa")))

(define-public crate-assembler-0.4.0 (c (n "assembler") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0nnsy5k9amyfizyprlr1rf4rry7br6s28faqzfgq3aikaj1vmfzs")))

(define-public crate-assembler-0.5.0 (c (n "assembler") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1nca22qacm5s0ij54h68ziczac5ckkzh9gy64cqnpa5zkawzmnxr")))

(define-public crate-assembler-0.5.3 (c (n "assembler") (v "0.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1b5l9sfnc21wafngdnxzbk2yp8glr9nbds7pyynprjvs3hs13ynp")))

(define-public crate-assembler-0.5.4 (c (n "assembler") (v "0.5.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1d74ri5q4sgka53s0wp5x4m709frxzb5h0s4gmz3z927cilj40hl")))

(define-public crate-assembler-0.5.5 (c (n "assembler") (v "0.5.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0m1qzkb367abarh46pz3hi9y86m976wssbk2md9f41pgyxd77ib3")))

(define-public crate-assembler-0.5.6 (c (n "assembler") (v "0.5.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1lis82wkvzys631ckm333ss4lmdn0bavs5yl7yz6wsh7rymiy8qy")))

(define-public crate-assembler-0.6.0 (c (n "assembler") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "16nw3l7kvdcdafcpv85vfpwxdxgk71c2hz67mwcdmwv8mam5y8wb")))

(define-public crate-assembler-0.6.1 (c (n "assembler") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0smzpbf5fqzmrdynblsi847xvyxbpykls3s2jrlwl199dxprh0i9")))

(define-public crate-assembler-0.6.2 (c (n "assembler") (v "0.6.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1r5gadb13d7iwjwaqvcnswqc5jax9xjcbgngandmsh6yd4pcrp5n")))

(define-public crate-assembler-0.6.3 (c (n "assembler") (v "0.6.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "142vxaj60x2gmqf5zb1x4ndc8ixynyvvc9jpi046yivx3ys66f9c")))

(define-public crate-assembler-0.7.0 (c (n "assembler") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1ivfwksy0icvqf59lqb9wbg6lxj19n6lxb9j82k8k6lf92qfvhb6")))

(define-public crate-assembler-0.7.1 (c (n "assembler") (v "0.7.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "14w0ylax4sl9l54wnd5isijk93avn6jjrwyk2krpvw5cqk56hf6n")))

(define-public crate-assembler-0.7.2 (c (n "assembler") (v "0.7.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "19jv4mmdbyjwfad9fak3dw4zk0l26d5fj1d6qclscrdx4cjykc1w")))

(define-public crate-assembler-0.8.0 (c (n "assembler") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1vglc1x5n71mkl2p99swn1vc1k4xj88r7prf5h1fm7p5xbxj65r2")))

(define-public crate-assembler-0.9.0 (c (n "assembler") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0ld75h4f8innz72jd5vllbf378wcpbbvc3yp6wm96q7ssb9w4pvh") (y #t)))

(define-public crate-assembler-0.9.1 (c (n "assembler") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "023a3jyczwj9fliq4yw357dgggl99xavxi3pnmiy02k7x2fhg1x6") (y #t)))

(define-public crate-assembler-0.9.2 (c (n "assembler") (v "0.9.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "031kpa0s42jkvyfv8gy7vwsxv9x0k6na02sbpdzfw53zpm03cr5f") (y #t)))

(define-public crate-assembler-0.9.3 (c (n "assembler") (v "0.9.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "07nyfni07c70f16hx7vsljjv6faiz9bi53mcgm4va1l8rc7xngm3") (y #t)))

(define-public crate-assembler-0.9.4 (c (n "assembler") (v "0.9.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1lj5jbnjcl2spdjiq9ygg1q413k37n22qx2qrcb847irdijz1ngq") (y #t)))

(define-public crate-assembler-0.9.5 (c (n "assembler") (v "0.9.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0v51ar470kl9x9dcz9g8diy5hw3ai74l7lcv70csr4sdvxb2mzxr") (y #t)))

(define-public crate-assembler-0.9.6 (c (n "assembler") (v "0.9.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0g5kdh1zrfm9y6c7prz45z321v3ykjln8a1asgk7sjllbl3yx8h9") (y #t)))

(define-public crate-assembler-0.9.7 (c (n "assembler") (v "0.9.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0dazb0am1ji5wdrm90psb2zqim38vdmpmmg3dm4mnk1q5qprk2h4") (y #t)))

(define-public crate-assembler-0.9.8 (c (n "assembler") (v "0.9.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1ajnswx93c2vh2asqhpniihqpys1s0ac9hng70ywlrqvwd0h70ga") (y #t)))

(define-public crate-assembler-0.9.9 (c (n "assembler") (v "0.9.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "08smlyj9jm2x3s531qhh692grnz764yq4487jvqpx6d4gk4x8z8k") (y #t)))

(define-public crate-assembler-0.9.10 (c (n "assembler") (v "0.9.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1rg7bp5pndag7lan3g3iizcnfkjfi6qlalv559y2vjj2814qy003")))

(define-public crate-assembler-0.9.11 (c (n "assembler") (v "0.9.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0p4d74p19dfzbn8l5myg5ixdaqzwdm681vf0q18sz88syw9m230g")))

(define-public crate-assembler-0.9.12 (c (n "assembler") (v "0.9.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "1m7d9001m2lp263ipyvak1m6kdk2ks2vi5j5r2v62rxbhy982vv9")))

(define-public crate-assembler-0.10.0 (c (n "assembler") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "0bja1m4vvg0fjs2ixzm4jh7ynlx504i6j2icafaj818k988i6m87")))

(define-public crate-assembler-0.10.1 (c (n "assembler") (v "0.10.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)))) (h "11ra0gz53j4vc4ka00j9nnl2w6i54sbdnp1kmc4d2bl02dg4wfc7")))

