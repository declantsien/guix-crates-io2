(define-module (crates-io as se assert_approx_eq) #:use-module (crates-io))

(define-public crate-assert_approx_eq-0.1.0 (c (n "assert_approx_eq") (v "0.1.0") (h "1xaqlmrg8061qa9h5chji6jcynqaivv885g39qpjzv1a3vqjb3wr")))

(define-public crate-assert_approx_eq-0.2.0 (c (n "assert_approx_eq") (v "0.2.0") (h "07klgyc70b7sr1wi5yw3cdw1wj36g63bv7ksjscsyqq9x8g8sp8s")))

(define-public crate-assert_approx_eq-0.2.1 (c (n "assert_approx_eq") (v "0.2.1") (h "0qz3c8nm6lfxjs2xwlwq3kvkbrihn01n3kixx90l1mbn9h5fja7l")))

(define-public crate-assert_approx_eq-0.2.2 (c (n "assert_approx_eq") (v "0.2.2") (h "1qffb1axb0b2v93f4jqnvx5cy5s08anns3y1sjgh8f8bwy7pl19m")))

(define-public crate-assert_approx_eq-1.0.0 (c (n "assert_approx_eq") (v "1.0.0") (h "0mb6alf90c0xvljinzniddkmayccwr3wrhfpc654pws9azfs0f0s")))

(define-public crate-assert_approx_eq-1.1.0 (c (n "assert_approx_eq") (v "1.1.0") (h "1zagfwfad5wssmr830gk3489f97ppczv6xs627jxniwm6ssdl1rw")))

