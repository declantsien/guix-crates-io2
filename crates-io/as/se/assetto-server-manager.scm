(define-module (crates-io as se assetto-server-manager) #:use-module (crates-io))

(define-public crate-assetto-server-manager-0.0.1 (c (n "assetto-server-manager") (v "0.0.1") (h "15474vq8gcfg0l0a6w6n3jk4z3scznq90diljgyggyyz78af7220") (y #t) (r "1.69.0")))

(define-public crate-assetto-server-manager-0.0.1-rc.1 (c (n "assetto-server-manager") (v "0.0.1-rc.1") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "hyper") (r "^0.14.26") (f (quote ("server"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0clx0g3hp469rdhgacpwjj4yva2nddsl8i8fwb1gz4vzpr1zgf0d") (y #t) (r "1.69.0")))

