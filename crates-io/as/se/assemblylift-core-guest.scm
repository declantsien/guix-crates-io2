(define-module (crates-io as se assemblylift-core-guest) #:use-module (crates-io))

(define-public crate-assemblylift-core-guest-0.1.0 (c (n "assemblylift-core-guest") (v "0.1.0") (d (list (d (n "paste") (r "^0.1.12") (d #t) (k 0)))) (h "06l5phc24lhq87h1bw60l9zv3i14jl7hh57isy0xc8byz8jwar70")))

(define-public crate-assemblylift-core-guest-0.1.1 (c (n "assemblylift-core-guest") (v "0.1.1") (d (list (d (n "paste") (r "^0.1.12") (d #t) (k 0)))) (h "19dw0d5cdhrkqzgadh0pppi4jv0qmy49n129lcc26fbgfv4ahlw8")))

(define-public crate-assemblylift-core-guest-0.2.0 (c (n "assemblylift-core-guest") (v "0.2.0") (d (list (d (n "paste") (r "^0.1.12") (d #t) (k 0)))) (h "1zgv16i257927yqwcywsgcsvzkhwi3zcn3mg8ldsrazlpwbxc310")))

(define-public crate-assemblylift-core-guest-0.4.0-alpha.0 (c (n "assemblylift-core-guest") (v "0.4.0-alpha.0") (d (list (d (n "assemblylift-core-guest-macros") (r "^0.4.0-alpha.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ffkkvaih56zqmqcc49jl0pxzavlxfyvjs19l70dgh5b652zq9hx")))

