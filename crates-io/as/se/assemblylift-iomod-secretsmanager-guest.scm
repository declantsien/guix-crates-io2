(define-module (crates-io as se assemblylift-iomod-secretsmanager-guest) #:use-module (crates-io))

(define-public crate-assemblylift-iomod-secretsmanager-guest-0.1.0 (c (n "assemblylift-iomod-secretsmanager-guest") (v "0.1.0") (d (list (d (n "assemblylift_core_io_guest") (r "^0.4.0-alpha") (d #t) (k 0) (p "assemblylift-core-io-guest")) (d (n "assemblylift_core_iomod_guest") (r "^0.4.0-alpha") (d #t) (k 0) (p "assemblylift-core-iomod-guest")) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1dbyhib9lcs9hrj047cqhz7drgr17wjbx5ccjz16pvmnm2cqw6bw")))

