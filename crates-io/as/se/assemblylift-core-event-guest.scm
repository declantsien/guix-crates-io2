(define-module (crates-io as se assemblylift-core-event-guest) #:use-module (crates-io))

(define-public crate-assemblylift-core-event-guest-0.1.0 (c (n "assemblylift-core-event-guest") (v "0.1.0") (d (list (d (n "assemblylift-core-event-common") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0zw0p32x8ndbzs8vyhs0pjgc0y3jkc3i10wk0d7npg0yj17c2s54")))

(define-public crate-assemblylift-core-event-guest-0.1.1 (c (n "assemblylift-core-event-guest") (v "0.1.1") (d (list (d (n "assemblylift-core-event-common") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0k9rbl12s7w57q84v6v6q3vic2kvvbarq7yqcgmzkwpkn9mbkd23")))

