(define-module (crates-io as se assertx) #:use-module (crates-io))

(define-public crate-assertx-0.1.0 (c (n "assertx") (v "0.1.0") (h "1rkr2f95nhbqfni448c26nnshwbnqgv8knyjflfwrpap9lw96jj0")))

(define-public crate-assertx-0.1.1 (c (n "assertx") (v "0.1.1") (h "19f7yyw4kss18rsf8xn5y2z4k69b7c661ma9rq01zsnlf80lvjxl")))

(define-public crate-assertx-0.1.2 (c (n "assertx") (v "0.1.2") (h "1yb7d893c3sxr0dg5jshcgna7gvfb5k8cn5bynskfpq8zbf4x1aa")))

(define-public crate-assertx-0.1.3 (c (n "assertx") (v "0.1.3") (h "1r2zblblpnyr5s5yb5gxibydpil5aclx9y45isbqkyn6fx10jjr7")))

(define-public crate-assertx-1.1.0 (c (n "assertx") (v "1.1.0") (d (list (d (n "log") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1ibk47jwn2k546h6qd2xp87pjaaykixpapl1nmrkai5nsypj89mz") (f (quote (("logging" "log"))))))

(define-public crate-assertx-1.1.1 (c (n "assertx") (v "1.1.1") (d (list (d (n "log") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "15vpnzf0511fskgdyxd37vjl5n8wgwgnlmj8h168m3wzqwdd1nmk") (f (quote (("logging" "log"))))))

(define-public crate-assertx-1.1.2 (c (n "assertx") (v "1.1.2") (d (list (d (n "log") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1gf9m5mxxdx004k2b1dskgq1bsvhqdyayyhfwjibshqlcsqximfb") (f (quote (("logging" "log"))))))

(define-public crate-assertx-1.1.3 (c (n "assertx") (v "1.1.3") (d (list (d (n "log") (r "^0.4.11") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1bgj5h30js3n0fdn44s4mwqf0c5lplqqf6hg8jnby5bgmd0gkci3") (f (quote (("logging" "log"))))))

(define-public crate-assertx-1.1.4 (c (n "assertx") (v "1.1.4") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)))) (h "1gqbx1i6195m0j72n5vwq9r58j11dijlyk132a3v3c9q509vrwdb") (f (quote (("logging" "log"))))))

(define-public crate-assertx-1.1.5 (c (n "assertx") (v "1.1.5") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)))) (h "0zyr6dpz0f5d00dsc9dpwv5zwmm5gaxg3819dmsbmp5gc44yf6vx") (f (quote (("logging" "log"))))))

(define-public crate-assertx-1.1.6 (c (n "assertx") (v "1.1.6") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)))) (h "0ivl6l18jb1v701fsjl0rmhxzv5hxf3hbiwgxrcr6glyf8kw9wfj") (f (quote (("logging" "log"))))))

(define-public crate-assertx-1.1.7-rc6 (c (n "assertx") (v "1.1.7-rc6") (d (list (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)))) (h "17i90l0k96c00yb45kb5pg2lp5vjwn3s97jxgrhg4r010glxm7ds") (f (quote (("logging" "log")))) (y #t)))

(define-public crate-assertx-1.1.7 (c (n "assertx") (v "1.1.7") (d (list (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.41") (d #t) (k 2)))) (h "1yhy0bwhg27cbbs0nyan2q15bb54r621q1r48mlmy68gad4fb1xv") (f (quote (("logging" "log"))))))

