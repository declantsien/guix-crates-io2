(define-module (crates-io as se assert-cmp) #:use-module (crates-io))

(define-public crate-assert-cmp-0.0.0 (c (n "assert-cmp") (v "0.0.0") (h "0ppkg9jjvaj9jw2asjqascc05dh5rz0gl73ggi8hls30x7mn9mz5")))

(define-public crate-assert-cmp-0.1.0 (c (n "assert-cmp") (v "0.1.0") (h "1w6737jq8livpwb93z02n1dpppf4gi61jfrvbx04fbfbk5wz8nvv")))

(define-public crate-assert-cmp-0.2.0 (c (n "assert-cmp") (v "0.2.0") (h "17ivxv23hicb1zzbp1r95hawr0d28x5n3m67nqrn9npxsrizsrg2")))

(define-public crate-assert-cmp-0.2.1 (c (n "assert-cmp") (v "0.2.1") (h "1zcg0dk9hk5x0xq3xdf3sah5mk230d6kn8zwgshnk3zkdnmg8yvk")))

