(define-module (crates-io as se assert-be-close) #:use-module (crates-io))

(define-public crate-assert-be-close-1.0.0 (c (n "assert-be-close") (v "1.0.0") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "0sjxwk8xa0b3xmivbbh9wzdyxkd5dz0r2qm1a6qx8lg1125lp9v2")))

(define-public crate-assert-be-close-1.0.1 (c (n "assert-be-close") (v "1.0.1") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "1rzbcafikxl3ix0c54b53x23aggx3qyyp32cbfj97yg9gkbx3bll")))

(define-public crate-assert-be-close-1.0.2 (c (n "assert-be-close") (v "1.0.2") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "0bcc84rcrw2hkf12vmrsh096rmasvnlm829qp95fbs4kln4sii75")))

(define-public crate-assert-be-close-1.0.3 (c (n "assert-be-close") (v "1.0.3") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)))) (h "1jy2jx7bijxvnxq9sldwlgc7nb99svm6xn5fshayxz4r2q5bm76x")))

