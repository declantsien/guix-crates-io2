(define-module (crates-io as se assert_eq_all) #:use-module (crates-io))

(define-public crate-assert_eq_all-0.1.0 (c (n "assert_eq_all") (v "0.1.0") (h "1vmz6hmq82bi4415p334602hqg3iln3wskwg72cgjkzf7lmhlzxy")))

(define-public crate-assert_eq_all-0.1.1 (c (n "assert_eq_all") (v "0.1.1") (h "0n13vmnpnkyy2nsglyvghzc6rgmivah5xgza20ikwm7b13n90hrg")))

