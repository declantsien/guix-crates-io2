(define-module (crates-io as se assert_that) #:use-module (crates-io))

(define-public crate-assert_that-0.1.0 (c (n "assert_that") (v "0.1.0") (d (list (d (n "predicates") (r "^1.0") (d #t) (k 0)) (d (n "predicates-tree") (r "^1.0") (d #t) (k 0)))) (h "14wzycrkr05kn9sx5pjxhhbcpsjrb7jrbsq9m8q97crqqv449rgw")))

(define-public crate-assert_that-0.1.1 (c (n "assert_that") (v "0.1.1") (d (list (d (n "predicates") (r "^1.0") (d #t) (k 0)) (d (n "predicates-tree") (r "^1.0") (d #t) (k 0)))) (h "0f9ih8jpv633cllzvrw0za16l58mq8hg3af9mh41zdwgk2asmhl9")))

(define-public crate-assert_that-0.1.2 (c (n "assert_that") (v "0.1.2") (d (list (d (n "predicates") (r "^1.0") (d #t) (k 0)) (d (n "predicates-tree") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0pzpdvdxzqj699dayj3id42yhsnxfna1jv45214pz7wa9h8l0ay8")))

