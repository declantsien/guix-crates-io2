(define-module (crates-io as se assemblylift-core-io-common) #:use-module (crates-io))

(define-public crate-assemblylift-core-io-common-0.2.0 (c (n "assemblylift-core-io-common") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0chvk8x15sb9ayp82fgjs6fs87zgk1i4n3ly0afbxjrrnh2svn02")))

(define-public crate-assemblylift-core-io-common-0.3.0 (c (n "assemblylift-core-io-common") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vymv9h0jyp01nw0pw4bjzdfkr8kk2k1kjw4mqm8fdkmadypprj3")))

