(define-module (crates-io as se assert-graph-iso) #:use-module (crates-io))

(define-public crate-assert-graph-iso-0.1.0 (c (n "assert-graph-iso") (v "0.1.0") (d (list (d (n "gdl") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "trim-margin") (r "^0.1.0") (d #t) (k 2)))) (h "1gmkia8grfqrw6rj9a5bsd8dpia77rk8ndwqj35np2v2nzqqil3m")))

