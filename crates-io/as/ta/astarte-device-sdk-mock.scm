(define-module (crates-io as ta astarte-device-sdk-mock) #:use-module (crates-io))

(define-public crate-astarte-device-sdk-mock-0.8.0 (c (n "astarte-device-sdk-mock") (v "0.8.0") (d (list (d (n "astarte-device-sdk") (r "=0.8.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "chrono") (r "^0.4.20") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 0)))) (h "03142q3fxz070j9cgdxzvpbmr1p00dlq3zi18f9xc24838pksw06") (r "1.72.0")))

(define-public crate-astarte-device-sdk-mock-0.8.1 (c (n "astarte-device-sdk-mock") (v "0.8.1") (d (list (d (n "astarte-device-sdk") (r "=0.8.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "chrono") (r "^0.4.20") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 0)))) (h "02fl8xsmlqi7zxbsz0i3ylwdlwwmym87aqpgqh75sp9v0qyqzz74") (r "1.72.0")))

(define-public crate-astarte-device-sdk-mock-0.8.2 (c (n "astarte-device-sdk-mock") (v "0.8.2") (d (list (d (n "astarte-device-sdk") (r "=0.8.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "chrono") (r "^0.4.20") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 0)))) (h "1l96iib5cvg65dvjhinin68f632i4nkyw55ypykk0riwy5jm6cfg") (r "1.72.0")))

