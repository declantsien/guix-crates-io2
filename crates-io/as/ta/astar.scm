(define-module (crates-io as ta astar) #:use-module (crates-io))

(define-public crate-astar-0.0.1 (c (n "astar") (v "0.0.1") (h "1if923z7sbrm98pbld8yxk98b1wlr8s62xc1135r1z1pap9i2jl8")))

(define-public crate-astar-0.0.2 (c (n "astar") (v "0.0.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1vx5f5cmkx856825wh51npav7lcgs73pb7pb9prpyln29wb9p95a")))

(define-public crate-astar-0.0.3 (c (n "astar") (v "0.0.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0xff8dwlmy9z7mrd8f3fpv0dlbg6h6p7jyrp0jldx4v751nw3jxj")))

(define-public crate-astar-0.1.0 (c (n "astar") (v "0.1.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "typed-arena") (r "*") (d #t) (k 0)))) (h "1bkng5d2msnydimhs71brmxi5l96m0nzwxcbk53d56ck5d6fkjz7")))

(define-public crate-astar-0.1.1 (c (n "astar") (v "0.1.1") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "typed-arena") (r "*") (d #t) (k 0)))) (h "1sa4hyg90133gwac5anl1vhhmpakzbrmnar99069w3g3a7kiic9z")))

(define-public crate-astar-0.1.2 (c (n "astar") (v "0.1.2") (d (list (d (n "num-traits") (r "0.1.*") (d #t) (k 0)) (d (n "typed-arena") (r "1.2.*") (d #t) (k 0)))) (h "1wb1agh4wrvi9hm8mvvhr70m8j3fzjxh7fb07swhdyzmqvr3fnc6")))

(define-public crate-astar-1.0.0 (c (n "astar") (v "1.0.0") (d (list (d (n "fnv") (r "1.*.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)) (d (n "typed-arena") (r "1.2.*") (d #t) (k 0)))) (h "0cwj7p9m2p6nbq8m0r2v8mvq9dniirpn5n52fp6abvk8zv5wmk61")))

(define-public crate-astar-2.0.0 (c (n "astar") (v "2.0.0") (d (list (d (n "fnv") (r "1.*.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)) (d (n "typed-arena") (r "1.2.*") (d #t) (k 0)))) (h "0gkx6nwslfgd605bxpl8lxkl870znr3d5arkcnvmy63pi81v6hgn")))

(define-public crate-astar-3.0.0 (c (n "astar") (v "3.0.0") (d (list (d (n "fnv") (r "1.*.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)) (d (n "typed-arena") (r "1.2.*") (d #t) (k 0)))) (h "11lm1ifd7mixwwfn2g6wnhy9ncfy8ln044y62l2ypvh9bnw5395k")))

(define-public crate-astar-4.0.0 (c (n "astar") (v "4.0.0") (d (list (d (n "fnv") (r "1.*.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)) (d (n "typed-arena") (r "1.2.*") (d #t) (k 0)))) (h "1gs3j6bkpjdl238ch2xfzrsgynvgq4xs6v6w74p1wxk2a8fi5ah5")))

