(define-module (crates-io as ta astack) #:use-module (crates-io))

(define-public crate-astack-0.1.0 (c (n "astack") (v "0.1.0") (h "0xfwdadc9r9n5s48zzc8vynmhxk559namx146rrklxzbrfy80xvl") (y #t) (r "1.63")))

(define-public crate-astack-0.2.0 (c (n "astack") (v "0.2.0") (h "016n8cjydw331ynqizyw94k5mr7xi8j5wlfaqy3dzcfjk541lyxy") (y #t) (r "1.63")))

(define-public crate-astack-0.2.1 (c (n "astack") (v "0.2.1") (h "1jabnync9bhzmcsds69rh2fpfmz9dk8qg7d5j2i4k6v0fdzbdd2f") (y #t) (r "1.63")))

(define-public crate-astack-0.3.0 (c (n "astack") (v "0.3.0") (h "01ciqsqbccs2ai738a66nc4xzrhh3pbqmvbnc8wj4jnw78z9mnya") (f (quote (("std")))) (y #t) (r "1.63")))

(define-public crate-astack-0.3.1 (c (n "astack") (v "0.3.1") (h "1ls5rj2vv2mivkmbl9jmi062qx2fpky2di8y5f86bl63qlab7mrm") (f (quote (("std")))) (y #t) (r "1.63")))

(define-public crate-astack-0.3.2 (c (n "astack") (v "0.3.2") (h "0aphzmz7sns970cx86hrmb327k6dhfp8d8a7hv3pcykj1aygvn9m") (f (quote (("std")))) (y #t) (r "1.63")))

(define-public crate-astack-0.3.3 (c (n "astack") (v "0.3.3") (h "1agm5q2pxwgcp8s1q255dxkyzfg3qmb637vw02gfv06f5y3rdi5q") (f (quote (("std")))) (y #t) (r "1.63")))

(define-public crate-astack-0.3.4 (c (n "astack") (v "0.3.4") (h "1hbgswsrj42cf8v4w5v3wll9pzpxxpy8jjwhi99fdk4cwlv5lvk5") (f (quote (("std")))) (y #t) (r "1.63")))

(define-public crate-astack-0.3.5 (c (n "astack") (v "0.3.5") (h "1fd82pafwsm2x9gsghcm82khj6b4wjhhfhqkr7ihxkpj9kbb9pkb") (f (quote (("std")))) (r "1.63")))

