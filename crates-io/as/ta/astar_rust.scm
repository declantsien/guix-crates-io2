(define-module (crates-io as ta astar_rust) #:use-module (crates-io))

(define-public crate-astar_rust-0.1.0 (c (n "astar_rust") (v "0.1.0") (h "0j3zinx8yimg1cybhjb29k0gr1hxjwarhb3w0236cmvsih3vskly")))

(define-public crate-astar_rust-0.2.0 (c (n "astar_rust") (v "0.2.0") (h "0gjr37nldqsnbn8y799qrdbkbl3prrmryyjmsak05hbdlvbih0ks")))

