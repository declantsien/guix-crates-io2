(define-module (crates-io as ta astarte-device-sdk-derive) #:use-module (crates-io))

(define-public crate-astarte-device-sdk-derive-0.1.0 (c (n "astarte-device-sdk-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1vkw5c9z8im2pkgwl3c1bl4nbs85pcf6wc468fs4s38s6vgn0zai") (y #t)))

(define-public crate-astarte-device-sdk-derive-0.5.0 (c (n "astarte-device-sdk-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13qnyadbrz9d0q8nnwm6dd0i4nnwc0g231skb4hdshlf5xr46x6d")))

(define-public crate-astarte-device-sdk-derive-0.5.1 (c (n "astarte-device-sdk-derive") (v "0.5.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ny1mj9qxvn2vnw2jvkbs818h0zd1yhb4iz4x1ilnajyrd75ndf5")))

(define-public crate-astarte-device-sdk-derive-0.6.0 (c (n "astarte-device-sdk-derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xvk874ca4j6civ85i2kjr4g4jvvyzs4bxql5rjsjzrzafl71bmq")))

(define-public crate-astarte-device-sdk-derive-0.6.1 (c (n "astarte-device-sdk-derive") (v "0.6.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mkr4b2lm8ky27fgckmpr6z7wvs9wyjc8p6sbakwbgnmjww61ixn")))

(define-public crate-astarte-device-sdk-derive-0.6.2 (c (n "astarte-device-sdk-derive") (v "0.6.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1inkq3slchbfq3s9nf37l8k4wplf9h5vfz8s27xkd3l30wi7s42w")))

(define-public crate-astarte-device-sdk-derive-0.7.0 (c (n "astarte-device-sdk-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "0czvssjf8037agf5khmfmbg43zk8676zxc38mwmnx9wx6v44i3nw") (r "1.66.1")))

(define-public crate-astarte-device-sdk-derive-0.5.2 (c (n "astarte-device-sdk-derive") (v "0.5.2") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dh2sgnrn5n79lqvxyy2ia2b4bckjl8ipqfcjxkzd0lni35n2c2j") (r "1.59.0")))

(define-public crate-astarte-device-sdk-derive-0.6.3 (c (n "astarte-device-sdk-derive") (v "0.6.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qiml8ykl8n5g8z66xg06637k6mbxf75w2fq25zq1h9cmaj79l5x") (r "1.66.1")))

(define-public crate-astarte-device-sdk-derive-0.7.1 (c (n "astarte-device-sdk-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1ydzp0f3k9y0878gfxcczz2flwzq2sar2q2cixsl20mcybpjfzv9") (r "1.72.0")))

(define-public crate-astarte-device-sdk-derive-0.5.3 (c (n "astarte-device-sdk-derive") (v "0.5.3") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0d2qh31y0s1bg4wm2j8h0ziji6cr8dlb30wz7nlnq61032vvs8ys") (r "1.59.0")))

(define-public crate-astarte-device-sdk-derive-0.6.4 (c (n "astarte-device-sdk-derive") (v "0.6.4") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j6gfkmvwa9ldy7cfpb5y6myvc8s3jg64yfn0i0kgmzjq3352z2s") (r "1.66.1")))

(define-public crate-astarte-device-sdk-derive-0.7.2 (c (n "astarte-device-sdk-derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "12bkri7zpmq5x13iazlcig0g29ydl8699cy3svs6wsc8p8k6v3qr") (r "1.72.0")))

(define-public crate-astarte-device-sdk-derive-0.6.5 (c (n "astarte-device-sdk-derive") (v "0.6.5") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g42mrf86mq0hjbwyymn15wh37jqhw36vs15mwaaar5xaiq0p3lr") (r "1.66.1")))

(define-public crate-astarte-device-sdk-derive-0.7.3 (c (n "astarte-device-sdk-derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1rapsc09k0qsqdw2pqg5si7px0gb3s8w070h75b6z60gij7prb5v") (r "1.72.0")))

(define-public crate-astarte-device-sdk-derive-0.8.0 (c (n "astarte-device-sdk-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "15ppzw55f9drdd5q2z9grw0zfrqc373y7pix5jhpjmsajwf0kk53") (r "1.72.0")))

(define-public crate-astarte-device-sdk-derive-0.8.1 (c (n "astarte-device-sdk-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "068ip63p8jyw95ajqhi3swf5yb72cy67ykj8vrmv7c2mmz34y0ck") (r "1.72.0")))

(define-public crate-astarte-device-sdk-derive-0.5.4 (c (n "astarte-device-sdk-derive") (v "0.5.4") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xrmwxjp6lgz1pyqxhv0qyyirm6722bpnsascx5mysdzhqavbh8v") (r "1.59.0")))

(define-public crate-astarte-device-sdk-derive-0.6.6 (c (n "astarte-device-sdk-derive") (v "0.6.6") (d (list (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v3m8481nd5pfj95xhff7g85k9qzq2hr96rbcrngf24kv05f8nqc") (r "1.66.1")))

(define-public crate-astarte-device-sdk-derive-0.7.4 (c (n "astarte-device-sdk-derive") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "0qnc6b1idwxqhknrb51sgfm514sv5dzk4p7w10x6jjivfv4f0ml9") (r "1.72.0")))

(define-public crate-astarte-device-sdk-derive-0.8.2 (c (n "astarte-device-sdk-derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "0y2nvgxqi314bxygrckac1g6yrj4i8vgvcl8s6csh0h9s8rmmv5p") (r "1.72.0")))

