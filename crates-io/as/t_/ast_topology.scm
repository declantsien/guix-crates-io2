(define-module (crates-io as t_ ast_topology) #:use-module (crates-io))

(define-public crate-ast_topology-0.0.1 (c (n "ast_topology") (v "0.0.1") (h "1rl11wv1a3q3iqm0dkl2iiyn66kg3f6m4zqj2nqls572pr70g3br") (y #t)))

(define-public crate-ast_topology-0.0.2 (c (n "ast_topology") (v "0.0.2") (d (list (d (n "autograd") (r "^1.0") (f (quote ("mkl"))) (d #t) (k 0)))) (h "1cva9pvc4lnygr5547258p4w8gljd00l9r69w185590rydiydhhk") (y #t)))

(define-public crate-ast_topology-0.0.3 (c (n "ast_topology") (v "0.0.3") (d (list (d (n "autograd") (r "^1.0") (f (quote ("mkl"))) (d #t) (k 0)))) (h "1yl3w2akqbs2m5jwxzqb6m56fkrdqsl11b575qin28i3xrn63rja") (y #t)))

(define-public crate-ast_topology-0.0.4 (c (n "ast_topology") (v "0.0.4") (d (list (d (n "autograd") (r "^1.0") (f (quote ("mkl"))) (d #t) (k 0)))) (h "0cwhsigdmayjc36gx3brk89rhkdir7s6wkp65iq3ip76jjvbpaz5") (y #t)))

