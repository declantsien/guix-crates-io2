(define-module (crates-io as co ascon-core) #:use-module (crates-io))

(define-public crate-ascon-core-0.1.0 (c (n "ascon-core") (v "0.1.0") (h "0a7v84wfymrk218jh0yx7j0155lgnk5lldq3f5bmg85yzfg7nw5h") (r "1.56")))

(define-public crate-ascon-core-0.1.1 (c (n "ascon-core") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (k 2)))) (h "0v3vhqi71syw08f93jdn3c0cz3zq3md7pps0r53v4iwfa9lwnxx7") (r "1.56")))

(define-public crate-ascon-core-0.1.2 (c (n "ascon-core") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (k 2)))) (h "0ahhmgsy1h6kgs17yhiwl5s08hv857pcq7bhag6vjv42la14f57v") (f (quote (("no_unroll")))) (r "1.56")))

(define-public crate-ascon-core-0.1.3 (c (n "ascon-core") (v "0.1.3") (d (list (d (n "ascon") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("std_rng" "getrandom"))) (k 2)))) (h "0az2pv9kb3kfrdndlw5l2p9237mgdj01d95ggfly0m5avsxc8n3n") (f (quote (("no_unroll" "ascon/no_unroll")))) (r "1.60")))

(define-public crate-ascon-core-0.1.4 (c (n "ascon-core") (v "0.1.4") (d (list (d (n "ascon") (r "^0.3") (d #t) (k 0)))) (h "0nf939myxg25yzzrpnw1lsva5mm8afmh08hwg26gnz2v84i072xi") (f (quote (("no_unroll" "ascon/no_unroll")))) (r "1.56")))

