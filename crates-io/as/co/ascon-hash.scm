(define-module (crates-io as co ascon-hash) #:use-module (crates-io))

(define-public crate-ascon-hash-0.1.0 (c (n "ascon-hash") (v "0.1.0") (d (list (d (n "ascon-core") (r "^0.1.0") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("core-api"))) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "spectral") (r "^0.6") (d #t) (k 2)))) (h "0jj4isidwnf232wr0i9a2v0b025j52zbkdxwam83d0dmzs9q78sr") (r "1.56")))

(define-public crate-ascon-hash-0.1.1 (c (n "ascon-hash") (v "0.1.1") (d (list (d (n "ascon") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.10") (f (quote ("core-api"))) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("std_rng" "getrandom"))) (k 2)) (d (n "spectral") (r "^0.6") (d #t) (k 2)))) (h "1zrdjsi4pwwapwqq4kk929mx71rbbplxlq38g5ycsan0skvx6akk") (r "1.60")))

(define-public crate-ascon-hash-0.2.0 (c (n "ascon-hash") (v "0.2.0") (d (list (d (n "ascon") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("core-api"))) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "spectral") (r "^0.6") (k 2)))) (h "1dlhzplhsfsk95ap29gdwg6nhp9m5npg134rb1hl3782h3jvcafn") (f (quote (("std" "digest/std") ("default" "std")))) (r "1.56")))

