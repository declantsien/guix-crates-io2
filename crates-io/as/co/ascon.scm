(define-module (crates-io as co ascon) #:use-module (crates-io))

(define-public crate-ascon-0.1.0 (c (n "ascon") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0fwc4qq4l4qyjgiwykpdzl7pls96v130jnpgqipvyvvq5gf3rggd")))

(define-public crate-ascon-0.1.1 (c (n "ascon") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (k 0)))) (h "197jzzz73kzmrk8px9ci5wanjc8hz91y68rabpdrgppa7kwvpdim")))

(define-public crate-ascon-0.1.2 (c (n "ascon") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (k 0)))) (h "0q7kr4akmcq0kwgfd5r95279aqapng92m13lnmyq8i7vkyqga99k")))

(define-public crate-ascon-0.1.3 (c (n "ascon") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5") (k 0)))) (h "1038fmcrr3d8h3rz07rb2vw9c4cr71zxdp9v1k1l6j8rqpkv69pd")))

(define-public crate-ascon-0.1.4 (c (n "ascon") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0") (k 0)))) (h "0a0fv2yp4kxzw36rpn96mzs5x9rqwqkc0yybdb2ncnllclz9mfa7")))

(define-public crate-ascon-0.2.0 (c (n "ascon") (v "0.2.0") (d (list (d (n "aead") (r "^0.5") (o #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "subtle") (r "^2") (o #t) (k 0)))) (h "1v9b9mh0nhlcn431qmi4mq915grkrk6dw5lag9wyr4n12sbs40f6") (f (quote (("default" "aead")))) (s 2) (e (quote (("std" "aead?/std") ("alloc" "aead?/alloc") ("aead" "dep:aead" "alloc" "subtle")))) (r "1.60")))

(define-public crate-ascon-0.3.0 (c (n "ascon") (v "0.3.0") (h "1sjxbkqvasa8k4jljfmag5wgaqkp4j6fz3mdwp65wi1w0kiyl0p7") (f (quote (("no_unroll")))) (r "1.60")))

(define-public crate-ascon-0.3.1 (c (n "ascon") (v "0.3.1") (h "1hfjm4qspfxg0ncxqrdqh6d1xkxaja8ja3b7cs8mx1a1yb7s1scz") (f (quote (("no_unroll")))) (r "1.56")))

(define-public crate-ascon-0.4.0 (c (n "ascon") (v "0.4.0") (d (list (d (n "zeroize") (r "^1.6.0") (o #t) (k 0)))) (h "1wf5lx1wnzsn6ppxh7hff6fljfjnlycaykb8wk6311d191h723jz") (f (quote (("no_unroll")))) (r "1.56")))

