(define-module (crates-io as ce ascetic_cli) #:use-module (crates-io))

(define-public crate-ascetic_cli-0.0.1 (c (n "ascetic_cli") (v "0.0.1") (d (list (d (n "ascesis") (r "^0.0.5") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1l575crnybcswq1im5248gb2q4v15d67q2sspaw2jp4vl01739v8")))

(define-public crate-ascetic_cli-0.0.2 (c (n "ascetic_cli") (v "0.0.2") (d (list (d (n "ascesis") (r "^0.0.6") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1yawhjwkai5jfqiarb62yr7sk23qjjlxsza9x070q50i8y9gdkd6")))

