(define-module (crates-io as ce ascending_camera) #:use-module (crates-io))

(define-public crate-ascending_camera-0.1.0 (c (n "ascending_camera") (v "0.1.0") (d (list (d (n "glam") (r "^0.26.0") (f (quote ("bytemuck" "mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)))) (h "1vwifdpzi6r394xgvg5v0whli3ifj6k65c51v6fmay4gcjw0ah0x")))

(define-public crate-ascending_camera-0.1.1 (c (n "ascending_camera") (v "0.1.1") (d (list (d (n "glam") (r "^0.26.0") (f (quote ("bytemuck" "mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5.9") (d #t) (k 0)))) (h "1a7q04cpwg8cy6ka12sml03qbpyx1aix247zrnind7k6sz5k7igv")))

(define-public crate-ascending_camera-0.2.0 (c (n "ascending_camera") (v "0.2.0") (d (list (d (n "glam") (r "^0.27.0") (f (quote ("bytemuck" "mint"))) (d #t) (k 0)))) (h "0xmnc02g7iqw2pdjdhg1dqza71712lnvdsndicaxl1fxvjlswqxm")))

(define-public crate-ascending_camera-0.3.0 (c (n "ascending_camera") (v "0.3.0") (d (list (d (n "glam") (r "^0.27.0") (f (quote ("bytemuck" "mint"))) (d #t) (k 0)))) (h "0j55aia216slkz9d9p8cqckx3p1znhsbjsaz67z68341lxi1ccac")))

