(define-module (crates-io as ce ascent_base) #:use-module (crates-io))

(define-public crate-ascent_base-0.1.0 (c (n "ascent_base") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1m88pz2r1rzy100h05warcyfc942qxf82kc4vi84bs5xanav8a49")))

(define-public crate-ascent_base-0.1.1 (c (n "ascent_base") (v "0.1.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1j2g5rniz3ad61s0z64zhhqsjjx2sbpbf8jzjjxdd6l3yqvb03fz")))

(define-public crate-ascent_base-0.1.2 (c (n "ascent_base") (v "0.1.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0lvqd43vdyqlk0r36297y132a18fc239ylsqji9q66wnbh5nca3x")))

(define-public crate-ascent_base-0.1.3 (c (n "ascent_base") (v "0.1.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "004lvlj5hj4mhb43hl3bpfirnbyrhdz79kfby7w8h29hazfscn1a")))

(define-public crate-ascent_base-0.2.0 (c (n "ascent_base") (v "0.2.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1w5pcmbzf5w72nr6fnfk30qm6s0ygj6r71573pfbas2j8jmn8iwd")))

(define-public crate-ascent_base-0.2.1 (c (n "ascent_base") (v "0.2.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0c4x7hv029pbj1invfsw75ak32a1yvm3clxnlx6w3qjcaiqkhadl")))

(define-public crate-ascent_base-0.3.0 (c (n "ascent_base") (v "0.3.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "13l74zj2p8502czc7qi3v5d35j61pz33f8hkhiq0ki0g8n0mj6w5")))

(define-public crate-ascent_base-0.4.0 (c (n "ascent_base") (v "0.4.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "06bgfs6idz1jsgid086m74si3wx4jlhb1yjyr13d2lyi35vsgh33")))

(define-public crate-ascent_base-0.5.0 (c (n "ascent_base") (v "0.5.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0g470rnhmi0nkr1d223hzbkzvgjc5fx76f9iafbm7yp9vask5bza")))

(define-public crate-ascent_base-0.6.0-alpha (c (n "ascent_base") (v "0.6.0-alpha") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1qnq830n5kfq27jskb6wgzl6d7z96cm2vspp28j9s3qf9naxbq6i")))

(define-public crate-ascent_base-0.6.0-alpha.1 (c (n "ascent_base") (v "0.6.0-alpha.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1a0vll4ih70nfs3rk6l87hi56zjz7pvmy5aypi6wyd7aaj5nj9y9")))

(define-public crate-ascent_base-0.6.0-alpha.2 (c (n "ascent_base") (v "0.6.0-alpha.2") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0f5p0hpivvziszvv8f10abfdwn517i9aaykxjf43hzajxxxii2rh")))

(define-public crate-ascent_base-0.6.0 (c (n "ascent_base") (v "0.6.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1fci6y9ikkfbw0bhj0ghqykrmxnwkvgkhzqcb3a7fl46qalwyyv0")))

