(define-module (crates-io as ce ascending_input) #:use-module (crates-io))

(define-public crate-ascending_input-0.1.0 (c (n "ascending_input") (v "0.1.0") (d (list (d (n "ordered-float") (r "^4.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (f (quote ("serde" "rwh_05"))) (d #t) (k 0)))) (h "0chdpad19dkv332irqmf39k0rfxp7ljwsbppcrbbmw4vg71cixml")))

(define-public crate-ascending_input-0.1.1 (c (n "ascending_input") (v "0.1.1") (d (list (d (n "ordered-float") (r "^4.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (f (quote ("serde" "rwh_05"))) (d #t) (k 0)))) (h "06frh8n3hm3cxpvniadaacl8ij0dn6j20zmxfikdfa4dd5j1sq5c")))

(define-public crate-ascending_input-0.2.0 (c (n "ascending_input") (v "0.2.0") (d (list (d (n "ordered-float") (r "^4.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (f (quote ("serde"))) (d #t) (k 0)))) (h "0sfcvbh7psn9z19b17b3izdy81dywvp9lrc4k31wakp1pd6g11qk")))

(define-public crate-ascending_input-0.2.1 (c (n "ascending_input") (v "0.2.1") (d (list (d (n "ordered-float") (r "^4.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.15") (f (quote ("serde"))) (d #t) (k 0)))) (h "00n273sgvd6gcby29agzss93rnbllqfzikg53r7a58cqxsgafrj8")))

(define-public crate-ascending_input-0.3.0 (c (n "ascending_input") (v "0.3.0") (d (list (d (n "ahash") (r "^0.8.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.1") (d #t) (k 0)) (d (n "winit") (r "^0.30.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "11rqls28zsvjl4hyr0gc7jawg8ala0jdfsgik228qyx1ca4zlxy3")))

