(define-module (crates-io as ce ascend) #:use-module (crates-io))

(define-public crate-ascend-0.1.0 (c (n "ascend") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17iqzd15aawvmrgyqwgrrr63fd7z0xdv2rwhfpyf6dkca4fx3sz5")))

(define-public crate-ascend-0.1.1 (c (n "ascend") (v "0.1.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12jx3ajgh2sn7s125k68m7cdn8i4xc0v6y4ffkavf3hazcjkqiv6")))

(define-public crate-ascend-0.2.0 (c (n "ascend") (v "0.2.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18pgb73ns30pgxgfyiz9nmm2gg4dwjldmlipki2wx4v3md8bnhms")))

(define-public crate-ascend-0.3.0 (c (n "ascend") (v "0.3.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xic599jadb98gqv6cp6qyp7zamvnimqzag0338fx9cd7qlfhdxa")))

