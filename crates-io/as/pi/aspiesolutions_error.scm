(define-module (crates-io as pi aspiesolutions_error) #:use-module (crates-io))

(define-public crate-aspiesolutions_error-0.1.0 (c (n "aspiesolutions_error") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.13") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1fv62wvazz24dwrxyvd80v19ly1xamfx50nypbbrzwi8rmdclqkr") (s 2) (e (quote (("serde" "dep:serde") ("reqwest" "dep:reqwest"))))))

(define-public crate-aspiesolutions_error-0.1.1 (c (n "aspiesolutions_error") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.13") (o #t) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)) (d (n "sea-orm") (r "^0.10.5") (o #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0349qz24y30g8rkc2fjd01j0hza40hm59vafqr0m0dzpfk3qphsx") (s 2) (e (quote (("serde" "dep:serde") ("rocket" "dep:rocket") ("reqwest" "dep:reqwest"))))))

(define-public crate-aspiesolutions_error-0.1.2 (c (n "aspiesolutions_error") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.13") (o #t) (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)) (d (n "sea-orm") (r "^0.10.5") (o #t) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0hqq68hzs2anb1k2ls71354mmiawnhrxw0044d99sy405zg0586n") (s 2) (e (quote (("serde" "dep:serde") ("rocket" "dep:rocket") ("reqwest" "dep:reqwest"))))))

