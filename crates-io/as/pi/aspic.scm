(define-module (crates-io as pi aspic) #:use-module (crates-io))

(define-public crate-aspic-0.1.0 (c (n "aspic") (v "0.1.0") (d (list (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "petgraph") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "054h9j3ldr7qvls6cxj0i9wjpff42d8xb47l3lw0ysb966gcmkfp")))

