(define-module (crates-io as yg asygnal) #:use-module (crates-io))

(define-public crate-asygnal-0.0.1 (c (n "asygnal") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1b4c3n2plxk96sik96ra0yah8yksrw2h6lg31jcmq2lwqb91fpqi") (f (quote (("_docs"))))))

(define-public crate-asygnal-0.0.2 (c (n "asygnal") (v "0.0.2") (d (list (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1701jhlqhvx2kpryfvmfrrhr006r11rhzmhl7dxsx2kgllka2dpn") (f (quote (("stream" "futures") ("_docs"))))))

