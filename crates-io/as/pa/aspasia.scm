(define-module (crates-io as pa aspasia) #:use-module (crates-io))

(define-public crate-aspasia-0.1.0 (c (n "aspasia") (v "0.1.0") (d (list (d (n "buildstructor") (r "^0.5") (d #t) (k 0)) (d (n "chardetng") (r "^0.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "1v91m0hp48jvm2hvr4ck9wpcdibagwgjf6cyaayw3gv51ggdqrr0")))

(define-public crate-aspasia-0.2.0 (c (n "aspasia") (v "0.2.0") (d (list (d (n "buildstructor") (r "^0.5") (d #t) (k 0)) (d (n "chardetng") (r "^0.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "107wpw7xhpz23d32pzsgsj5f9q9585ilxfr9jh8apqj6k8c17big")))

