(define-module (crates-io as pa asparit) #:use-module (crates-io))

(define-public crate-asparit-0.1.0 (c (n "asparit") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "rayon-core") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "05qjmdh6fja5xns115igmzvd5mi9jyffl3cz93s6cz32rkv8ax3h") (f (quote (("tokio-executor" "futures" "num_cpus" "tokio" "default-executor") ("sequential-executor" "default-executor") ("rayon-executor" "rayon-core" "default-executor") ("default-executor") ("default" "sequential-executor"))))))

