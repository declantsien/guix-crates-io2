(define-module (crates-io as pa aspartam) #:use-module (crates-io))

(define-public crate-aspartam-0.1.0 (c (n "aspartam") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0j8l0kzr1h28cr1mxa9znli4q6y3h17dq4cjn565w5vnlcgpik2n")))

