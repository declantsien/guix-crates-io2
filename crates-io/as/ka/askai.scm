(define-module (crates-io as ka askai) #:use-module (crates-io))

(define-public crate-AskAI-0.1.0 (c (n "AskAI") (v "0.1.0") (d (list (d (n "ai21") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serenity") (r "^0.11.5") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "shuttle-secrets") (r "^0.7.2") (d #t) (k 0)) (d (n "shuttle-service") (r "^0.7.2") (f (quote ("bot-serenity"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1crhnsj9jbhn10758z60gisgdx5c7mp4wx5kr65larrd8cmyig7c")))

