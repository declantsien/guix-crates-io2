(define-module (crates-io as ka askama_warp) #:use-module (crates-io))

(define-public crate-askama_warp-0.10.0 (c (n "askama_warp") (v "0.10.0") (d (list (d (n "askama") (r "^0.10") (f (quote ("with-warp" "mime" "mime_guess"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)) (d (n "warp") (r "^0.2") (k 0)))) (h "09m27qs7k2c060b9cr28j4n6861427hpr87xnmq8z83zn4542vy9")))

(define-public crate-askama_warp-0.11.0 (c (n "askama_warp") (v "0.11.0") (d (list (d (n "askama") (r "^0.10") (f (quote ("with-warp" "mime" "mime_guess"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "warp") (r "^0.3") (k 0)))) (h "0m105sbi73sdxlva59w12sx4spgvgk0y1lrkhn4vcv5r2y49aj1v")))

(define-public crate-askama_warp-0.12.0 (c (n "askama_warp") (v "0.12.0") (d (list (d (n "askama") (r "^0.11.0") (f (quote ("with-warp" "mime" "mime_guess"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "warp") (r "^0.3") (k 0)))) (h "1wvy25n3i682jcpc83ax4v4fpbbbmmk4gdmmh004v4vp3pphigj2")))

(define-public crate-askama_warp-0.13.0 (c (n "askama_warp") (v "0.13.0") (d (list (d (n "askama") (r "^0.12") (f (quote ("with-warp" "mime" "mime_guess"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "warp") (r "^0.3") (k 0)))) (h "03pk90w7m841d32fl43arzwk7affix09lwkg5s0xr5mmqffdgdvr") (f (quote (("urlencode" "askama/urlencode") ("serde-yaml" "askama/serde-yaml") ("serde-json" "askama/serde-json") ("num-traits" "askama/num-traits") ("markdown" "askama/markdown") ("humansize" "askama/humansize") ("default" "askama/default") ("config" "askama/config")))) (r "1.58")))

