(define-module (crates-io as ka askama_tide) #:use-module (crates-io))

(define-public crate-askama_tide-0.10.0 (c (n "askama_tide") (v "0.10.0") (d (list (d (n "askama") (r "^0.10.2") (f (quote ("with-tide"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.11") (d #t) (k 0)))) (h "0i347f3a178ws1ifihiypl4fm7rvs92545xf80xvyh9ig2yvi896")))

(define-public crate-askama_tide-0.11.0 (c (n "askama_tide") (v "0.11.0") (d (list (d (n "askama") (r "^0.10.2") (f (quote ("with-tide"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.13") (d #t) (k 0)))) (h "1qywxrbany0d66ram1d9id91mq76vbnxm9qzgfli49v2ly64hpvx")))

(define-public crate-askama_tide-0.11.1 (c (n "askama_tide") (v "0.11.1") (d (list (d (n "askama") (r "^0.10.2") (f (quote ("with-tide"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "tide") (r "^0.13") (f (quote ("h1-server"))) (k 0)))) (h "1aiypsmwyzy07dg0883lyjwgsjmfnnywdyskmlrwyx2cjkkdzhl9")))

(define-public crate-askama_tide-0.11.2 (c (n "askama_tide") (v "0.11.2") (d (list (d (n "askama") (r "^0.10.2") (f (quote ("with-tide"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tide") (r "^0.14") (k 0)))) (h "000k2m77d8489pkckhxjrg695fg0zl0kbzjvkhxax03785aslczw")))

(define-public crate-askama_tide-0.12.0 (c (n "askama_tide") (v "0.12.0") (d (list (d (n "askama") (r "^0.10.2") (f (quote ("with-tide"))) (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tide") (r "^0.15") (k 0)))) (h "1sr7qg36fxhr4nlcyjp1q3s8lwfkn8zyiwh322lq7b1kl0qxbh8g")))

(define-public crate-askama_tide-0.13.0 (c (n "askama_tide") (v "0.13.0") (d (list (d (n "askama") (r "^0.10.2") (f (quote ("with-tide"))) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tide") (r "^0.16") (k 0)))) (h "1np5yiypscx01im809c08aqzz23686r8h6gs6cm2s7n5czpcplhj")))

(define-public crate-askama_tide-0.14.0 (c (n "askama_tide") (v "0.14.0") (d (list (d (n "askama") (r "^0.11.0") (f (quote ("with-tide"))) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tide") (r "^0.16") (k 0)))) (h "0h37dp6g4s3pbj784lgh98v99dzgybxwm1hk1szc9in993impga5")))

(define-public crate-askama_tide-0.15.0 (c (n "askama_tide") (v "0.15.0") (d (list (d (n "askama") (r "^0.12") (f (quote ("with-tide"))) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "tide") (r "^0.16") (k 0)))) (h "0q0q15wmjnvvbz6pxs3yh5bp7l5w2g0ljgkk0mshgj9zma1y0ij9") (f (quote (("urlencode" "askama/urlencode") ("serde-yaml" "askama/serde-yaml") ("serde-json" "askama/serde-json") ("num-traits" "askama/num-traits") ("markdown" "askama/markdown") ("humansize" "askama/humansize") ("default" "askama/default") ("config" "askama/config")))) (r "1.58")))

