(define-module (crates-io as ka askama-enum) #:use-module (crates-io))

(define-public crate-askama-enum-0.0.1 (c (n "askama-enum") (v "0.0.1") (d (list (d (n "askama") (r "^0.11.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gjli4s29y6ki5djf9wgg14ljl5xvl6q3dqapncahn4blap49n5x")))

(define-public crate-askama-enum-0.0.2 (c (n "askama-enum") (v "0.0.2") (d (list (d (n "askama") (r "^0.11.1") (o #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15mai3c3d01vqj2kwa6sinfnhf11vw6ydwdmgqh2xah4b0dvp3sw") (f (quote (("testing" "askama") ("docsrs" "askama"))))))

