(define-module (crates-io as ka askama_iron) #:use-module (crates-io))

(define-public crate-askama_iron-0.10.0 (c (n "askama_iron") (v "0.10.0") (d (list (d (n "askama") (r "^0.10") (f (quote ("with-iron"))) (d #t) (k 0)) (d (n "iron") (r ">=0.5, <0.7") (d #t) (k 0)))) (h "05f003j4w9bb0h2h0ba1dj6sklrm5q4v5csqlkxipsh6b81vxf3f")))

