(define-module (crates-io as ka askama_gotham) #:use-module (crates-io))

(define-public crate-askama_gotham-0.10.0 (c (n "askama_gotham") (v "0.10.0") (d (list (d (n "askama") (r "^0.10") (f (quote ("with-gotham" "mime" "mime_guess"))) (d #t) (k 0)) (d (n "gotham") (r "^0.4") (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "1bkcba0ca9z7s8jz7b8d70pyrpzzyxz4vx0lf9vckh10lkvk9947")))

(define-public crate-askama_gotham-0.11.0 (c (n "askama_gotham") (v "0.11.0") (d (list (d (n "askama") (r "^0.10") (f (quote ("with-gotham" "mime" "mime_guess"))) (d #t) (k 0)) (d (n "gotham") (r "^0.5") (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "0mgvm2fps8afck2yb65i1msw3fq9ifzx9jq1lvnc0kcp36dj205i")))

(define-public crate-askama_gotham-0.12.0 (c (n "askama_gotham") (v "0.12.0") (d (list (d (n "askama") (r "^0.10") (f (quote ("with-gotham" "mime" "mime_guess"))) (k 0)) (d (n "gotham") (r "^0.6") (k 0)) (d (n "hyper") (r "^0.14.4") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "1by6m76ggck8qm927fl01yilhc42j60pbaaj0m9sm7z1fkgmwgsh")))

(define-public crate-askama_gotham-0.13.0 (c (n "askama_gotham") (v "0.13.0") (d (list (d (n "askama") (r "^0.11.0") (f (quote ("with-gotham" "mime" "mime_guess"))) (k 0)) (d (n "gotham") (r "^0.7") (k 0)) (d (n "gotham") (r "^0.7") (f (quote ("testing"))) (d #t) (k 2)) (d (n "hyper") (r "^0.14.4") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "1x488zipc7xr7bywyd6vh96xrssjv7w5fg4yj7p7ajqjdc7jq8bh")))

(define-public crate-askama_gotham-0.14.0 (c (n "askama_gotham") (v "0.14.0") (d (list (d (n "askama") (r "^0.12") (f (quote ("with-gotham" "mime" "mime_guess"))) (k 0)) (d (n "gotham") (r "^0.7") (k 0)) (d (n "gotham") (r "^0.7") (f (quote ("testing"))) (d #t) (k 2)) (d (n "hyper") (r "^0.14") (d #t) (k 2)) (d (n "mime") (r "^0.3") (d #t) (k 2)))) (h "0simbjpnbxnlg9iw004mqmy6x3810v4968vnzlhfdym5zrlhdhfb") (f (quote (("urlencode" "askama/urlencode") ("serde-yaml" "askama/serde-yaml") ("serde-json" "askama/serde-json") ("num-traits" "askama/num-traits") ("markdown" "askama/markdown") ("humansize" "askama/humansize") ("default" "askama/default") ("config" "askama/config")))) (r "1.58")))

