(define-module (crates-io as ka askama_rocket) #:use-module (crates-io))

(define-public crate-askama_rocket-0.10.0 (c (n "askama_rocket") (v "0.10.0") (d (list (d (n "askama") (r "^0.10") (f (quote ("with-rocket" "mime" "mime_guess"))) (d #t) (k 0)) (d (n "rocket") (r "^0.4") (k 0)))) (h "10s50nfx20znh1gba6xjwmcip5prbfw2j5rrh7adyyh2pl9qqhj0")))

(define-public crate-askama_rocket-0.11.0-rc.1 (c (n "askama_rocket") (v "0.11.0-rc.1") (d (list (d (n "askama") (r "^0.10") (f (quote ("with-rocket" "mime" "mime_guess"))) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (k 0)))) (h "0cd55zf91652k77airfng2fifjd1h5ldzwl4qda38kgkx02wv6rh")))

(define-public crate-askama_rocket-0.11.0 (c (n "askama_rocket") (v "0.11.0") (d (list (d (n "askama") (r "^0.11.0-beta.1") (f (quote ("with-rocket" "mime" "mime_guess"))) (k 0)) (d (n "rocket") (r "^0.4") (k 0)))) (h "00nah184xn5w76m5ri725kg4sn0gm0k9q6zw3r6fwv42x9fyz3xm")))

(define-public crate-askama_rocket-0.12.0 (c (n "askama_rocket") (v "0.12.0") (d (list (d (n "askama") (r "^0.12") (f (quote ("with-rocket" "mime" "mime_guess"))) (k 0)) (d (n "futures-lite") (r "^2.0.0") (d #t) (k 2)) (d (n "rocket") (r "^0.5.0") (k 0)))) (h "1rhq36ss9lja1wvbhq3jf3qfqdypcdsss9869bmb4a49z6h19y7b") (f (quote (("urlencode" "askama/urlencode") ("serde-yaml" "askama/serde-yaml") ("serde-json" "askama/serde-json") ("num-traits" "askama/num-traits") ("markdown" "askama/markdown") ("humansize" "askama/humansize") ("default" "askama/default") ("config" "askama/config")))) (r "1.65")))

