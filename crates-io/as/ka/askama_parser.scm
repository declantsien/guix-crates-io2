(define-module (crates-io as ka askama_parser) #:use-module (crates-io))

(define-public crate-askama_parser-0.1.0 (c (n "askama_parser") (v "0.1.0") (d (list (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "0kbw58dskq2nysxl6sa7c23pxjbvim8ybnx612kb9jffbgksr0f2") (r "1.65")))

(define-public crate-askama_parser-0.1.1 (c (n "askama_parser") (v "0.1.1") (d (list (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "1d7pvbp4qivmky0kx9jyhxm00zkhf2jslwi4bj67ri5405pajs62") (r "1.65")))

(define-public crate-askama_parser-0.2.0 (c (n "askama_parser") (v "0.2.0") (d (list (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "03bplravyrphni3pyi9pwhs6jkfirjlys7lmyb2nj4p5gg7vjbi6") (r "1.65")))

(define-public crate-askama_parser-0.2.1 (c (n "askama_parser") (v "0.2.1") (d (list (d (n "nom") (r "^7") (f (quote ("alloc"))) (k 0)))) (h "1h00vcnqq9qqlayx1ass4an458rk4lm3q88867cc7lb4dcf1dcdc") (r "1.65")))

