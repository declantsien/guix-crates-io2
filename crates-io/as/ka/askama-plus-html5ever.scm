(define-module (crates-io as ka askama-plus-html5ever) #:use-module (crates-io))

(define-public crate-askama-plus-html5ever-0.1.0 (c (n "askama-plus-html5ever") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "askama") (r "^0.10") (d #t) (k 0)) (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "tendril") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "typed-arena") (r ">=1, <3") (d #t) (k 0)))) (h "10ghbj8mlyxrzywmwh4mlpwl0f72dj4hr2z48aadvi75v70pkgh4")))

(define-public crate-askama-plus-html5ever-0.2.0 (c (n "askama-plus-html5ever") (v "0.2.0") (d (list (d (n "askama") (r "^0.10") (d #t) (k 0)) (d (n "html5ever-arena-dom") (r "^0.1") (d #t) (k 0)))) (h "1q8i79cdyq8grpn8hjjykskkp7dbb58ir9hvibjlycz5yr327q0g")))

(define-public crate-askama-plus-html5ever-0.2.1 (c (n "askama-plus-html5ever") (v "0.2.1") (d (list (d (n "askama") (r "^0.11") (d #t) (k 0)) (d (n "html5ever-arena-dom") (r "^0.1") (d #t) (k 0)))) (h "0szmark3wy1ml78papnv4p4g1c5zywa0xj53cdgldy7pxkl4drch")))

