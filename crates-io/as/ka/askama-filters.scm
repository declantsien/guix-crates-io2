(define-module (crates-io as ka askama-filters) #:use-module (crates-io))

(define-public crate-askama-filters-0.1.0 (c (n "askama-filters") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "1h5llhz6ddar58nvlcj070az9lbz3jqpx8zqkwf4kgqfb2phxqrl") (f (quote (("markdown" "pulldown-cmark") ("default") ("date" "chrono"))))))

(define-public crate-askama-filters-0.1.1 (c (n "askama-filters") (v "0.1.1") (d (list (d (n "askama_shared") (r "^0.8.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "0mv36srhjfgi1r10xq79dsa4v6wbd96ky76v197rz5zvq5icxgjf") (f (quote (("markdown" "pulldown-cmark") ("default") ("date" "chrono"))))))

(define-public crate-askama-filters-0.1.2 (c (n "askama-filters") (v "0.1.2") (d (list (d (n "askama_shared") (r "^0.8.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "1jv2zy5r7m1qci6bkwxfzbzzvl0vfl0vwfxyqhxsbx16110g3j26") (f (quote (("markdown" "pulldown-cmark") ("default") ("date" "chrono"))))))

(define-public crate-askama-filters-0.1.3 (c (n "askama-filters") (v "0.1.3") (d (list (d (n "askama_shared") (r "^0.9.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (o #t) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)))) (h "10zs1j2wq77zdcy4s9swyszp6b0h08fc1n9dnvrs3s9glp34vjj5") (f (quote (("markdown" "pulldown-cmark") ("default") ("date" "chrono"))))))

