(define-module (crates-io as ka askama_escape) #:use-module (crates-io))

(define-public crate-askama_escape-0.1.0 (c (n "askama_escape") (v "0.1.0") (h "19r4qyrmb0s71ra6l2g0rqji2d59kc8640fpcx7mdhgskw1li6vi")))

(define-public crate-askama_escape-0.2.0 (c (n "askama_escape") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "07b2837wr6g8gf4q4yac5q09f77sfrdxyr0xksmfvgmm60i99pmh")))

(define-public crate-askama_escape-0.3.0 (c (n "askama_escape") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0nkr6fmcg89cyqarpgsfirc8p9rqrk7rhp8rkzxsmhgylpmplmvs")))

(define-public crate-askama_escape-0.10.0 (c (n "askama_escape") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1hc2rx61jjkwh9iv9h81mpxnbyr5hf0406yfqjmh2bl5568q1c8v")))

(define-public crate-askama_escape-0.10.1 (c (n "askama_escape") (v "0.10.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1ys6wcrkpzygk6r93zd0rhinhy89rraarl0m4afwi023m70hihch")))

(define-public crate-askama_escape-0.10.2 (c (n "askama_escape") (v "0.10.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1mdqyg42ia94ay76hng001vkrr1q0281bw3bfngxyvkyz4hb66ws")))

(define-public crate-askama_escape-0.10.3 (c (n "askama_escape") (v "0.10.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0hg3rz0cma5f6385z7qmqw3jbir76jndwd5s7dqfk92v9gil75v1") (f (quote (("json"))))))

