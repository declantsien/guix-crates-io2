(define-module (crates-io as ka askama_actix) #:use-module (crates-io))

(define-public crate-askama_actix-0.10.0 (c (n "askama_actix") (v "0.10.0") (d (list (d (n "actix-rt") (r "^1") (k 2)) (d (n "actix-web") (r "^2") (d #t) (k 0)) (d (n "askama") (r "^0.10") (f (quote ("with-actix-web" "mime" "mime_guess"))) (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0rmk9lvbb9rc55j8jymgp2gs21nz95fl401n6wqkwh4vj46j7g6x")))

(define-public crate-askama_actix-0.11.0 (c (n "askama_actix") (v "0.11.0") (d (list (d (n "actix-rt") (r "^1") (k 2)) (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "askama") (r "^0.10") (f (quote ("with-actix-web" "mime" "mime_guess"))) (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "1ifzm3fphg03rkmhkmiadv283x3vkzx1ac6s2aawh1a4ziylz14r")))

(define-public crate-askama_actix-0.11.1 (c (n "askama_actix") (v "0.11.1") (d (list (d (n "actix-rt") (r "^1") (k 2)) (d (n "actix-web") (r "^3") (k 0)) (d (n "askama") (r "^0.10") (f (quote ("with-actix-web" "mime" "mime_guess"))) (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "12jidz3s3bpvmzmfyryfk8sc5fy93yp6vsmhazakx8dsp6838r90")))

(define-public crate-askama_actix-0.12.0 (c (n "askama_actix") (v "0.12.0") (d (list (d (n "actix-rt") (r "^1") (k 2)) (d (n "actix-web") (r "^3") (k 0)) (d (n "askama") (r "^0.11.0") (f (quote ("with-actix-web" "mime" "mime_guess"))) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0zj0687p7q48aq52lan2b842kc108lvralkqzvzr6x4ays65aaab")))

(define-public crate-askama_actix-0.13.0 (c (n "askama_actix") (v "0.13.0") (d (list (d (n "actix-rt") (r "^2") (k 2)) (d (n "actix-test") (r "=0.1.0-beta.13") (d #t) (k 2)) (d (n "actix-web") (r "^4") (k 0)) (d (n "askama") (r "^0.11.1") (f (quote ("with-actix-web"))) (k 0)) (d (n "askama_shared") (r "^0.12.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)))) (h "0jcl2x5r3vg711c39zi0kq32qgy3mchhn42jq37jw51a73w78by5")))

(define-public crate-askama_actix-0.14.0 (c (n "askama_actix") (v "0.14.0") (d (list (d (n "actix-rt") (r "^2") (k 2)) (d (n "actix-test") (r "^0.1") (d #t) (k 2)) (d (n "actix-web") (r "^4") (k 0)) (d (n "askama") (r "^0.12") (f (quote ("with-actix-web"))) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 2)))) (h "0mcfqw3hccrk84j8nwwvfm6g5v2rlkxqjfl5lc5v00z2rwbxvc74") (f (quote (("urlencode" "askama/urlencode") ("serde-yaml" "askama/serde-yaml") ("serde-json" "askama/serde-json") ("num-traits" "askama/num-traits") ("markdown" "askama/markdown") ("humansize" "askama/humansize") ("default" "askama/default") ("config" "askama/config")))) (r "1.58")))

