(define-module (crates-io as #{39}# as3935-bbn) #:use-module (crates-io))

(define-public crate-as3935-bbn-0.1.0-pre.2 (c (n "as3935-bbn") (v "0.1.0-pre.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rppal") (r "^0.14.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "simple-signal") (r "^1.1.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 2)))) (h "0k1dv7p8z6ppravkzq05qixwhy92hs3hi426ryj78smlmj2r73gm")))

