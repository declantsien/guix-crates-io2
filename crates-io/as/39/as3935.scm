(define-module (crates-io as #{39}# as3935) #:use-module (crates-io))

(define-public crate-as3935-0.1.0-pre.1 (c (n "as3935") (v "0.1.0-pre.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)) (d (n "simple-signal") (r "^1.1.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 2)))) (h "01zcplafqd1pa0h3a0pzmrqcc96mm9l7ca22bg9ry7mm2xwxlyfd")))

