(define-module (crates-io as _n as_num) #:use-module (crates-io))

(define-public crate-as_num-0.1.0 (c (n "as_num") (v "0.1.0") (h "0fvgk5viqawcnjv0g79a9hrj0vqars94snjczj3wpr0gwfgi5pid")))

(define-public crate-as_num-0.1.1 (c (n "as_num") (v "0.1.1") (h "1j4jrxrlvqw8hpvjpbcgr48rxqf23aflhd5k8m8361zxib0vdd6n")))

(define-public crate-as_num-0.1.2 (c (n "as_num") (v "0.1.2") (h "046ib4wzvvvzbn1br27n0g0v4hvkmq7b0bi5q3rd710hxvwrjd30")))

(define-public crate-as_num-0.2.0 (c (n "as_num") (v "0.2.0") (h "1c1p4basslnwykw2xhmam5xa7fw6da828dpfckp8dz72cvdrmavw")))

(define-public crate-as_num-0.2.1 (c (n "as_num") (v "0.2.1") (h "0y2x24bnsjp87d708j86l77w74qxjc7haibrp6491kn7dsl2pkj8")))

(define-public crate-as_num-0.2.2 (c (n "as_num") (v "0.2.2") (h "1ydqgkcm769kx17s9lbpcfm2q5fsranzgbw29srjx7wsxk51lc6i")))

(define-public crate-as_num-0.2.3 (c (n "as_num") (v "0.2.3") (h "0pzp9klfvrid6j7va46fx94qbblffck7b1v1qpi87p17jy1v7dax")))

(define-public crate-as_num-0.2.4 (c (n "as_num") (v "0.2.4") (h "0w7nhzasjd4pvxl81fwkznqisfspr0p29l5zmag4v58p8imw01aq")))

(define-public crate-as_num-0.2.5 (c (n "as_num") (v "0.2.5") (h "1h381pql7imjj71nz66g7ng62dnm9madxdmryz13s6sy6ads3sk2")))

(define-public crate-as_num-0.3.1 (c (n "as_num") (v "0.3.1") (h "11npbfqxh7rrkrsfy6wksydg6184avmnq3gdj3acxaqhsz9pzc4z")))

