(define-module (crates-io as t2 ast2str) #:use-module (crates-io))

(define-public crate-ast2str-1.1.0 (c (n "ast2str") (v "1.1.0") (d (list (d (n "ast2str-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "ast2str-lib") (r "^1.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "0lh9gh0ggal51hawgi5k32sn3cp84vjriql864qjqmrhpqd5syzg")))

(define-public crate-ast2str-1.1.1 (c (n "ast2str") (v "1.1.1") (d (list (d (n "ast2str-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "ast2str-lib") (r "^1.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "0myxqb0simn9b5zr2kqq5ss198ch67dafzxk0y472sjs3v7r1d7i")))

(define-public crate-ast2str-1.2.1 (c (n "ast2str") (v "1.2.1") (d (list (d (n "ast2str-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "ast2str-lib") (r "^1.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "04d5bzj1r6z93wizcjn2p3h4081laa2hnz9p0yblx49cc2zi15ih")))

(define-public crate-ast2str-1.3.0 (c (n "ast2str") (v "1.3.0") (d (list (d (n "ast2str-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "ast2str-lib") (r "^1.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "1f1zv259h1137wc15f8y1p05cysr4g440wh0b80h6zbzxcsk9mxy") (f (quote (("impl_hashbrown" "ast2str-lib/impl_hashbrown") ("allocator_api" "ast2str-lib/allocator_api"))))))

(define-public crate-ast2str-1.4.0 (c (n "ast2str") (v "1.4.0") (d (list (d (n "ast2str-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "ast2str-lib") (r "^1.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "04y0pqblfqk1997v1xzsjnw5q05cfmf9gwxiffayqpq4xzzhzbv2") (f (quote (("impl_hashbrown" "ast2str-lib/impl_hashbrown") ("allocator_api" "ast2str-lib/allocator_api"))))))

(define-public crate-ast2str-1.4.1 (c (n "ast2str") (v "1.4.1") (d (list (d (n "ast2str-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "ast2str-lib") (r "^1.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "0q5pbil730jhlm50bw9nmcvp8pnbz25i9p4p71gx5xkqwdvqfs71") (f (quote (("impl_hashbrown" "ast2str-lib/impl_hashbrown") ("allocator_api" "ast2str-lib/allocator_api"))))))

