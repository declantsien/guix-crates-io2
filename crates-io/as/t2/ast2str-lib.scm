(define-module (crates-io as t2 ast2str-lib) #:use-module (crates-io))

(define-public crate-ast2str-lib-1.1.0 (c (n "ast2str-lib") (v "1.1.0") (d (list (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "0clqb6mjg5lmyqsmbchk399q786prdm0j52a03lf9jgmyyrg8xy2")))

(define-public crate-ast2str-lib-1.2.0 (c (n "ast2str-lib") (v "1.2.0") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "0apqcg0wcxjncvj9hvyndd3r0fv8larg78dyy4py8jmlaksgcfpi") (f (quote (("impl_hashbrown" "hashbrown") ("allocator_api" "hashbrown/nightly"))))))

(define-public crate-ast2str-lib-1.2.1 (c (n "ast2str-lib") (v "1.2.1") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "1dhq8pvnm1qhi7c4gw96gczmb7q3f4pl6nip1sh3ajvjl8l55ncf") (f (quote (("impl_hashbrown" "hashbrown") ("allocator_api" "hashbrown/nightly"))))))

(define-public crate-ast2str-lib-1.3.0 (c (n "ast2str-lib") (v "1.3.0") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "1i3apakqrkjxdd24182av8avj72axiw8l5n24fwifqnmvznv6xs4") (f (quote (("impl_hashbrown" "hashbrown") ("allocator_api" "hashbrown/nightly"))))))

