(define-module (crates-io as cn ascn-rs) #:use-module (crates-io))

(define-public crate-ascn-rs-0.1.0 (c (n "ascn-rs") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.23.0") (d #t) (k 0)) (d (n "shakmaty") (r "^0.24.0") (d #t) (k 0)))) (h "0a7i2nxh6hhm205mwg8l50dkxr9dn8v1g9qq6lxpdq1spaknm3h3")))

(define-public crate-ascn-rs-0.2.0 (c (n "ascn-rs") (v "0.2.0") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "pgn-rs") (r "^0.0.1") (d #t) (k 0)))) (h "1kkpb2pcl4563qwimh33x7b90imxyynhxprfllc1pwqxq32wy8h5")))

(define-public crate-ascn-rs-0.3.0 (c (n "ascn-rs") (v "0.3.0") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "pgn-rs") (r "^0.0.2") (d #t) (k 0)))) (h "0x1fbp0k6ks863i9w1rrxb6sms678kk2bq8spn6jh2q3yh4ginaa")))

