(define-module (crates-io as pm aspmatch) #:use-module (crates-io))

(define-public crate-aspmatch-0.1.0 (c (n "aspmatch") (v "0.1.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0iqx9v3xc3dk78b9pn44c384s5cl0q82b7y91pqfmx68vhm920lm")))

