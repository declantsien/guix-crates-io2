(define-module (crates-io as te asterisk-agi) #:use-module (crates-io))

(define-public crate-asterisk-agi-0.1.0 (c (n "asterisk-agi") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1slzg5s4348xysmjc7bpi9r9wryk53rb6q0cp6c0j6b1m8wjlm2d")))

