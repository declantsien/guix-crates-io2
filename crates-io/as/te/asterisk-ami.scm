(define-module (crates-io as te asterisk-ami) #:use-module (crates-io))

(define-public crate-asterisk-ami-0.1.0 (c (n "asterisk-ami") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "099a4rhaw9h26wzqb3gwy1ivgn3lhv9h879g8nvyzvbaybnni3qm")))

(define-public crate-asterisk-ami-0.1.1 (c (n "asterisk-ami") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zka253l687jklx569fcpdpbsqdspq1a2v78jmfg374srd5jwsv6")))

(define-public crate-asterisk-ami-0.1.2 (c (n "asterisk-ami") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1w3ji2abchskdfs8zivdfairznk4bfgdwhqv1lyn4pc3vvz2av1h")))

(define-public crate-asterisk-ami-0.1.3 (c (n "asterisk-ami") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "048r03l31i7s0adakla1ai0d2y173sz3dv33ab9h4v04hcfdwp4m")))

(define-public crate-asterisk-ami-0.1.4 (c (n "asterisk-ami") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yj5ygn7wm144n0i6mjv5ijygjz6ghinw4cs9sibicsalpaqfg83")))

(define-public crate-asterisk-ami-0.1.5 (c (n "asterisk-ami") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wmkgxg47hxhx26zv4hfm9caq6kvlnxyxyyka4cfdh27m8sy1ll3")))

(define-public crate-asterisk-ami-0.1.6 (c (n "asterisk-ami") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14qmf99mjkjv03plh2zz99g2z4lslb647sdr3i15jb5s5jk1wzxp")))

(define-public crate-asterisk-ami-0.1.7 (c (n "asterisk-ami") (v "0.1.7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h02j9sw3kfmg9bj11kf871gazrx4px4a0xaxj4nq2smrbs37xyz")))

