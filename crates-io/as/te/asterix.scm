(define-module (crates-io as te asterix) #:use-module (crates-io))

(define-public crate-asterix-0.1.1 (c (n "asterix") (v "0.1.1") (d (list (d (n "deku") (r "^0.7.1") (d #t) (k 0)))) (h "1y3nk9rx1nvyykf7cysgpzr8agcn2484wqygivbzdbmdjdci7h21")))

(define-public crate-asterix-0.2.0 (c (n "asterix") (v "0.2.0") (d (list (d (n "deku") (r "^0.7.1") (d #t) (k 0)))) (h "0760551h1i7i31cs0vib6hbhq7cm354yp7cs6r7pvd8s612kq8db")))

(define-public crate-asterix-0.2.1 (c (n "asterix") (v "0.2.1") (d (list (d (n "deku") (r "^0.7.1") (d #t) (k 0)))) (h "0hfbr0k7414662r2i1a628d6w0jxjfkdhbidy6dm3c38l7zpxa9v")))

(define-public crate-asterix-0.2.2 (c (n "asterix") (v "0.2.2") (d (list (d (n "asterix-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "deku") (r "^0.7.1") (d #t) (k 0)))) (h "1ksdi5225cgqb1ag7z2q6an8mr3lha279clad863di70i16vb9p3")))

(define-public crate-asterix-0.2.3 (c (n "asterix") (v "0.2.3") (d (list (d (n "asterix-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "deku") (r "^0.8.0") (d #t) (k 0)))) (h "066pv382sn63js2hpk9842syankvg9xj52q0q5wdmmvldi37jqch")))

(define-public crate-asterix-0.2.4 (c (n "asterix") (v "0.2.4") (d (list (d (n "asterix-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "deku") (r "^0.8.0") (d #t) (k 0)))) (h "12i201bimsqpcam5fmlxc4i1cdhcwpp16qvc1wcj5nqwzlwljx1l")))

(define-public crate-asterix-0.2.5 (c (n "asterix") (v "0.2.5") (d (list (d (n "asterix-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "deku") (r "^0.9.1") (d #t) (k 0)))) (h "1ws7lfiphbj5q663j0hk77hy6zgishxn2l2rwa56kyn0ml9yp31s")))

(define-public crate-asterix-0.2.6 (c (n "asterix") (v "0.2.6") (d (list (d (n "asterix-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "deku") (r "^0.10.0") (d #t) (k 0)))) (h "13fbmzcv03acbszdjw373ja52dxfgyy2gsz698f0pjjj89g07wdy")))

(define-public crate-asterix-0.3.0 (c (n "asterix") (v "0.3.0") (d (list (d (n "assert_hex") (r "^0.2.1") (d #t) (k 0)) (d (n "asterix-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "deku") (r "^0.15.0") (d #t) (k 0)))) (h "0l0av2gs737cbfmqr4pyxfvd13abza92pnr10y1m3394qr25iig9")))

(define-public crate-asterix-0.3.1 (c (n "asterix") (v "0.3.1") (d (list (d (n "assert_hex") (r "^0.4") (d #t) (k 0)) (d (n "asterix-derive") (r "^0.3") (d #t) (k 0)) (d (n "deku") (r "^0.16") (d #t) (k 0)))) (h "079axqk39sl20rwy6wcw4l417z711dz28iz3zryh4hmnv3n53ayf")))

(define-public crate-asterix-0.4.0 (c (n "asterix") (v "0.4.0") (d (list (d (n "assert_hex") (r "^0.4") (d #t) (k 0)) (d (n "asterix-derive") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.16") (d #t) (k 0)))) (h "0cgcgrvcy7zk1kwj9h5skfc2c51v2hc1nz2yixrza70jdwx61fga") (r "1.70.0")))

