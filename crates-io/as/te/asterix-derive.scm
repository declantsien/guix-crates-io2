(define-module (crates-io as te asterix-derive) #:use-module (crates-io))

(define-public crate-asterix-derive-0.2.2 (c (n "asterix-derive") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "0vpzsa4b84wqjy8mdzhx3a13qgaqf0c54dh55d5825bashnsz2c3")))

(define-public crate-asterix-derive-0.2.3 (c (n "asterix-derive") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "03071kxfrgiq9h88xwz6wjgbb2ri7p60m6rv4hm07hqb41fpy6sz")))

(define-public crate-asterix-derive-0.2.4 (c (n "asterix-derive") (v "0.2.4") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "1jqq3qvjww3y4if5i3r44j8k3360lvsyilkid8pn7p6by75mazbi")))

(define-public crate-asterix-derive-0.2.5 (c (n "asterix-derive") (v "0.2.5") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "1zjx3a9jya0m7sicl6qn8i9b4g9d1nxbph41jmxb9knyx3jdd2r2")))

(define-public crate-asterix-derive-0.2.6 (c (n "asterix-derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "1k7r81y3045i3cgsjplwi0a8bxaq1cibd6y9bx7x73abrdqm360q")))

(define-public crate-asterix-derive-0.3.0 (c (n "asterix-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "0hzyy53krwxrci66a6lif93gmyhfan69sx4nxscszmyjbfz75xja")))

(define-public crate-asterix-derive-0.4.0 (c (n "asterix-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "183nv42snxhg73gxs1bv4vfblv47pg9h6d08wxzd685igrm4f8gg")))

