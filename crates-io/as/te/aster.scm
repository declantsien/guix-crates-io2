(define-module (crates-io as te aster) #:use-module (crates-io))

(define-public crate-aster-0.1.2 (c (n "aster") (v "0.1.2") (h "1z10pnavgci2sbqhxym736igavvibd155pgv0h76rc98k1cpjjkz")))

(define-public crate-aster-0.1.3 (c (n "aster") (v "0.1.3") (h "1d8d168sy0dpgyxqfcqlk5gv46n1d0na15lcnfrwcr5ccsyqg6m8")))

(define-public crate-aster-0.1.4 (c (n "aster") (v "0.1.4") (h "1rdfah26rvb1y5h8b2h0hisgi56clmmi8g2ypxnkir3hfqk308qn")))

(define-public crate-aster-0.1.5 (c (n "aster") (v "0.1.5") (h "00qf9xab9bc9qdlx8w1r24dx1l6869izfsbh2dmbf63in4cvjniv")))

(define-public crate-aster-0.1.6 (c (n "aster") (v "0.1.6") (h "1a22n7zm2g3l0lv4cll88dm0rsli6lsgpfphbn86g9swa74vhlas")))

(define-public crate-aster-0.1.7 (c (n "aster") (v "0.1.7") (h "1asdh68alr7zvhdpc25kj3mwgffi6js201v03c322nym39cik4sl")))

(define-public crate-aster-0.1.8 (c (n "aster") (v "0.1.8") (h "1772fnjm4fadp7vhi8v1x5f96qp1rcz46z3n8s9myzxhgrg443if")))

(define-public crate-aster-0.1.9 (c (n "aster") (v "0.1.9") (h "0c97ddp96fy9sx2nm82lhbrhr8r3y1d56vm07lm4v1mydi58gd32")))

(define-public crate-aster-0.1.10 (c (n "aster") (v "0.1.10") (h "12nalin7xcp9xiias6k8lms4pb65sj9mmq0b7rab1ax1cf8xh9ql")))

(define-public crate-aster-0.2.0 (c (n "aster") (v "0.2.0") (h "0il30g17sdjf6mpy9y76pw1yb67lb969cw9h2ig09fwm7d3wvqhb")))

(define-public crate-aster-0.2.1 (c (n "aster") (v "0.2.1") (h "052r4yi6b1js5lnzsaadkxr8ffhgz2vs4h81xz891y10pa5fcrdf")))

(define-public crate-aster-0.2.2 (c (n "aster") (v "0.2.2") (h "1zxlcd4qaawsbj5ci7nxwgjb3gddj3lwhbs0vg2yzsw91rwlcyi5")))

(define-public crate-aster-0.3.0 (c (n "aster") (v "0.3.0") (d (list (d (n "syntex_syntax") (r "*") (o #t) (d #t) (k 0)))) (h "1iccdhlw660whz1qqn56mdipklnwvb462p6kjspa6hri3dcyri5k") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.3.1 (c (n "aster") (v "0.3.1") (d (list (d (n "syntex_syntax") (r ">= 0.6.0") (o #t) (d #t) (k 0)))) (h "0x9mf3dfb13gfkf2iwxpiy1pn7svfskbkl3dxiali01rbvkpk7bl") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.3.2 (c (n "aster") (v "0.3.2") (d (list (d (n "syntex_syntax") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1mr1qxcb6myvrw0aa68m90i2iw92vin6aalis0n3256z0ai0m9wx") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.3.3 (c (n "aster") (v "0.3.3") (d (list (d (n "syntex_syntax") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "1sdm37cy7fn98rzhl4r2ksr2rypqp40niws2vx31l03kyqr7yxq2") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.0 (c (n "aster") (v "0.4.0") (d (list (d (n "syntex_syntax") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1mm7iw3lq6nwzq9nj8xjzgxy0fv6sq997kdr984nkagcwa50pla2") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.1 (c (n "aster") (v "0.4.1") (d (list (d (n "syntex_syntax") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "06n67fc7za0n8svqhynxyarzlswki7gr0s2w4gswj60k7gcqn0g7") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.2 (c (n "aster") (v "0.4.2") (d (list (d (n "syntex_syntax") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "0fqmkw4civyhibrs84r9snkiqfjz6k7k9k63z8jh497s6m78kc1w") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.3 (c (n "aster") (v "0.4.3") (d (list (d (n "syntex_syntax") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0s3ybf6bh81qyz3bchd18q303fdzcc5w9w9a518y74276bcrw8a2") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.4 (c (n "aster") (v "0.4.4") (d (list (d (n "syntex_syntax") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "07kppxd97s9n9b3xdbda9f3qi411cdav1va6k3zw4636ncnwhxgy") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.5 (c (n "aster") (v "0.4.5") (d (list (d (n "syntex_syntax") (r ">= 0.13.0") (o #t) (d #t) (k 0)))) (h "1l7aa3kj7b9v1kwqpim9g5q3k56zkkxfjpfvphkgzm7266z7la90") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.6 (c (n "aster") (v "0.4.6") (d (list (d (n "syntex_syntax") (r ">= 0.13.0") (o #t) (d #t) (k 0)))) (h "0kvd0m41q3cv6z4q85g2rqm4rm42cxibmyhhv95mvla3isf0lzpx") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.7 (c (n "aster") (v "0.4.7") (d (list (d (n "syntex_syntax") (r ">= 0.13.0") (o #t) (d #t) (k 0)))) (h "0414iizxy111x2jhlh3cjdxn6ns59xz9h7iqxwb7g3igpi4kn380") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.8 (c (n "aster") (v "0.4.8") (d (list (d (n "syntex_syntax") (r ">= 0.13.0") (o #t) (d #t) (k 0)))) (h "0v69bzag5yhs9damp10y0zlrsa2ng0l18w42lhj1v0pilba8k5q3") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.9 (c (n "aster") (v "0.4.9") (d (list (d (n "syntex_syntax") (r ">= 0.16.0") (o #t) (d #t) (k 0)))) (h "19q20qkq9cdvqpwm3sxidinxidjgn8ngy5kkr8kvfb2d5jb2czay") (f (quote (("with-syntex" "syntex_syntax"))))))

(define-public crate-aster-0.4.10 (c (n "aster") (v "0.4.10") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r ">= 0.16.0") (o #t) (d #t) (k 0)))) (h "18nnn9hzmxlvm5ml2w31h4i8ksvj1r7zfr3k2qfzpg3b2nldzlp1") (f (quote (("with-syntex" "syntex_syntax") ("unstable" "clippy"))))))

(define-public crate-aster-0.5.0 (c (n "aster") (v "0.5.0") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "0q7dclwbp6ild69h59pkzxbmb7sbk2k4414p4c98bq37mq2wc1h6") (f (quote (("with-syntex" "syntex_syntax") ("unstable" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.6.0 (c (n "aster") (v "0.6.0") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "11dk9yv9bahi2d3nhrgscsb4vwbmrhyxda72xrlqxj2v60c04qgq") (f (quote (("with-syntex" "syntex_syntax") ("unstable" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.7.0 (c (n "aster") (v "0.7.0") (d (list (d (n "clippy") (r "^0.0.22") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.10") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "0y4ilp8db9wp30mrvrcys76fw989hxhfsq7a9xa8ccz3dxdk6aww") (f (quote (("with-syntex" "syntex_syntax") ("unstable" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.8.0 (c (n "aster") (v "0.8.0") (d (list (d (n "clippy") (r "^0.0.22") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.10") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.22.0") (o #t) (d #t) (k 0)))) (h "1s0s50j3zykjbpgd3zgmy41w8sfz7xdyqm6zb4dfq3vshgkpjri2") (f (quote (("with-syntex" "syntex_syntax") ("unstable" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.9.0 (c (n "aster") (v "0.9.0") (d (list (d (n "clippy") (r "^0.0.22") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.10") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.23.0") (o #t) (d #t) (k 0)))) (h "0fk6ak1hsvh5frw7v8viizni7vwvfqnn0kjqhgklvj0rrvwi50z4") (f (quote (("with-syntex" "syntex_syntax") ("unstable" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.9.1 (c (n "aster") (v "0.9.1") (d (list (d (n "clippy") (r "^0.0.22") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.10") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.23.0") (o #t) (d #t) (k 0)))) (h "1ibyg9wfq04d293n68a9aq5vzczfvy5v9cx2dah3132239fcnnv7") (f (quote (("with-syntex" "syntex_syntax") ("unstable" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.9.2 (c (n "aster") (v "0.9.2") (d (list (d (n "clippy") (r "^0.0.33") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.24.0") (o #t) (d #t) (k 0)))) (h "1zdaclmz6v98ycf08xk05nfjprzahvnnl33pzrbl007006wxmmpx") (f (quote (("with-syntex" "syntex_syntax") ("unstable" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.9.3 (c (n "aster") (v "0.9.3") (d (list (d (n "clippy") (r "^0.0.33") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "1q9ygfw8mcl3c7fi7byc2lsw57c2hi2cxhf7r3ll3kgg48l8imhx") (f (quote (("with-syntex" "syntex_syntax") ("unstable" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.10.0 (c (n "aster") (v "0.10.0") (d (list (d (n "clippy") (r "^0.0.35") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.26.0") (o #t) (d #t) (k 0)))) (h "0lklrpy0cjx3wsskfkpg3ggdlvgr82kls8i4bn67iiqrd78ylpk3") (f (quote (("with-syntex" "syntex_syntax") ("unstable" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.10.1 (c (n "aster") (v "0.10.1") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.26.0") (o #t) (d #t) (k 0)))) (h "0mq8qr3m3527rn2ivpcp237l7xvcwa0kzwa8x0m6y27nzqs2khw5") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.11.0 (c (n "aster") (v "0.11.0") (d (list (d (n "clippy") (r "^0.0.39") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.27.0") (o #t) (d #t) (k 0)))) (h "0ymm2ls2j6sshqpknnmz8k9iqcvw1vzsb5dhj4kar2jyfwqph6hp") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.12.0 (c (n "aster") (v "0.12.0") (d (list (d (n "clippy") (r "^0.0.41") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.28.0") (o #t) (d #t) (k 0)))) (h "1kx87p28grf565yq4nmbis21zx4dwxidacfhwa6c2fhm89wzjmrj") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.13.0 (c (n "aster") (v "0.13.0") (d (list (d (n "clippy") (r "^0.0.41") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.29.0") (o #t) (d #t) (k 0)))) (h "1fvrfz3wyxg0q49frisnakjs0lbxhc4970k0xafpbdpxgix18rgs") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.13.1 (c (n "aster") (v "0.13.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0.11") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.29.0") (o #t) (d #t) (k 0)))) (h "09vlaz47ly1rqzxjbmi03r4danbk1lbm9962s135y480wbyz60xa") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.14.0 (c (n "aster") (v "0.14.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.30.0") (o #t) (d #t) (k 0)))) (h "1c3vdgyjnb4bsnqnifl2iav03qn07lwfw9pslcagykza6dj8gb9g") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.15.0 (c (n "aster") (v "0.15.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.31.0") (o #t) (d #t) (k 0)))) (h "0082map8riqkciwhxli9584qdsm9rz0cqvz26qr2l6g7f848nxyx") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.16.0 (c (n "aster") (v "0.16.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.32.0") (o #t) (d #t) (k 0)))) (h "0x6d74n1ram6l5yzrd360xjzalwm7nnzxmfrrhirm206r8gaiz8h") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.17.0 (c (n "aster") (v "0.17.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.33.0") (o #t) (d #t) (k 0)))) (h "0sj9wcz1jva4infdpyqlhjjx64mrn6gkiaj8344my58a9ybl9lq7") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.18.0 (c (n "aster") (v "0.18.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.35.0") (o #t) (d #t) (k 0)))) (h "140zmqw59n4b7dw9pmagwn4lw44pfrqy39ab9zx66wr0myi83k87") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.19.0 (c (n "aster") (v "0.19.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.36.0") (o #t) (d #t) (k 0)))) (h "1w5v76d1pcxrn2m7jbyvrkrfbjzi37dqz9hc13qxs90zwxjw2dfd") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.20.0 (c (n "aster") (v "0.20.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.37.0") (o #t) (d #t) (k 0)))) (h "1qnxdzl48hfi6y205mhp08i3f57zyaizlnxgaaikbmfbssbfx875") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.21.0 (c (n "aster") (v "0.21.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38.0") (o #t) (d #t) (k 0)))) (h "016rn2yp72agw8c45nnaf35ff74arh4lxl52021l9gcpvn1hns0c") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.21.1 (c (n "aster") (v "0.21.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.38.0") (o #t) (d #t) (k 0)))) (h "08lyq74bjahv27c7rkydydhfihm6fv9rmby3j4lx0y1v5p6zb1qd") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.22.0 (c (n "aster") (v "0.22.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (o #t) (d #t) (k 0)))) (h "15rl7ss15cmwpx6j123d8mi1m6srdijrhz91abqy4m4rg68k2n5n") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.22.1 (c (n "aster") (v "0.22.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (o #t) (d #t) (k 0)))) (h "07kls3rhjsr20g6rqqc6kaf08kds3syxsq94fvhj4rggfvj22biw") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.23.0 (c (n "aster") (v "0.23.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.40.0") (o #t) (d #t) (k 0)))) (h "1x8sp5rygyrhcbj5knzwn2byvfi6dk7dkpqmx2awbnjxrk8xdy9s") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.24.0 (c (n "aster") (v "0.24.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.41.0") (o #t) (d #t) (k 0)))) (h "15wqcd6a6k3s6iv4n800ih2zxsbkldmv4ml1gcxbfnp2d9djin6z") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.25.0 (c (n "aster") (v "0.25.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42.0") (o #t) (d #t) (k 0)))) (h "022ajnin2y2makd7pfz4a6zzrxcmw4ai9b44g7gy2lla7qq97wjd") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.26.0 (c (n "aster") (v "0.26.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.43.0") (o #t) (d #t) (k 0)))) (h "1ppw1v0gbplffnz1dk7z33yfjzk0ii7cncnxnwwhjxznvaj3f01s") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.26.1 (c (n "aster") (v "0.26.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.43.0") (o #t) (d #t) (k 0)))) (h "1cvd2wlhp06dczqlr5l17gs9iyjfp1vpa4bndrvar6clxspqr8ds") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.27.0 (c (n "aster") (v "0.27.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.44.0") (o #t) (d #t) (k 0)))) (h "1jg5sm4lb7w7y34yqg23k6cpqis5scjx94pgxvha2mfjdn28k295") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.28.0 (c (n "aster") (v "0.28.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.44.0") (o #t) (d #t) (k 0)))) (h "037wg5m254pvg984vl9zgrbqdf3saznqgwk56vc1cy6s2n1nh659") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.29.0 (c (n "aster") (v "0.29.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.44.0") (o #t) (d #t) (k 0)))) (h "0x4dadh6xqwfx1l7q7zy0zriyh6gpvwbgp8xcjvwdvsbmryngisb") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.30.0 (c (n "aster") (v "0.30.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.45.0") (o #t) (d #t) (k 0)))) (h "12xvb3f5k0cxvlqvd0c63ysyn9zlxg7hskfd7jpcicy4l0w3qji9") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.31.0 (c (n "aster") (v "0.31.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.46.0") (o #t) (d #t) (k 0)))) (h "0zhrkc38sgl20ldwrs4igf0jvj82z3pr3c1g42i0933j7lihjnl9") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.32.0 (c (n "aster") (v "0.32.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.47.0") (o #t) (d #t) (k 0)))) (h "1ncjxvsiamddzdgydim8mrxjv19rcw71prqnyap48glxl983zld1") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.32.1 (c (n "aster") (v "0.32.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.47.0") (o #t) (d #t) (k 0)))) (h "0bv80r3a6jwvzx3gz6whfmpand9m264gljbvrc23rpihf17qa35n") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.33.0 (c (n "aster") (v "0.33.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.48.0") (o #t) (d #t) (k 0)))) (h "19rnm3qdfhb1nyjqdk1q5jzq4radbljamqa095fjimnbz6rmzyxp") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.33.1 (c (n "aster") (v "0.33.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.48.0") (o #t) (d #t) (k 0)))) (h "0qq5qm7s9nwv9x6n53bgkkhlmwmb0xi0qhk2d1pl8b40pkcfmnbz") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.34.0 (c (n "aster") (v "0.34.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.50.0") (o #t) (d #t) (k 0)))) (h "095ma0s6w158vf1cb66f8b547lw5w3synwnqpyvxvsm7yv6qxfw8") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.35.0 (c (n "aster") (v "0.35.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.51.0") (o #t) (d #t) (k 0)))) (h "0w83qbiizvbjlrbmbh446j3v61w3qnh0idvpg6xfzw5fsyzr1810") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.35.1 (c (n "aster") (v "0.35.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.51.0") (o #t) (d #t) (k 0)))) (h "0kdxldy0s1bw9iakxansll425rixxwnlg32fyxpvh0k71x3168ss") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.36.0 (c (n "aster") (v "0.36.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.52.0") (o #t) (d #t) (k 0)))) (h "0yzh3a0sx4dgpbw6in4faw36xxrxim6fa9h8lqndwfqmv2i88min") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.37.0 (c (n "aster") (v "0.37.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.53.0") (o #t) (d #t) (k 0)))) (h "0cl8rpz5xql269qgmwrx3d03nd54dx79g34cfs1gskh8svzqr128") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "compiletest_rs"))))))

(define-public crate-aster-0.38.0 (c (n "aster") (v "0.38.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.54.0") (o #t) (d #t) (k 0)))) (h "1cz54c0h7kyxi0r7vjjd0rw0kpi1fsrivfdcv1whp7245bj4k6rc") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.39.0 (c (n "aster") (v "0.39.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.55.0") (o #t) (d #t) (k 0)))) (h "0d85qxz0vqg4pfk1ch7ny3v2zz2b219dzyylv7z0m10wcbjj4a02") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.40.0 (c (n "aster") (v "0.40.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.57.0") (o #t) (d #t) (k 0)))) (h "1d6bznn0rxz96ggdcjbn6kpyzbx9bvjhz60n0b7pwfd64yp5mr0z") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

(define-public crate-aster-0.41.0 (c (n "aster") (v "0.41.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58") (o #t) (d #t) (k 0)))) (h "1q704kn23wnwnrxml7w1mxw6a3xb6386x5wgys6ibnyramrxzksc") (f (quote (("with-syntex" "syntex_syntax") ("unstable-testing" "clippy" "compiletest_rs"))))))

