(define-module (crates-io as te asteroid) #:use-module (crates-io))

(define-public crate-asteroid-0.1.0 (c (n "asteroid") (v "0.1.0") (d (list (d (n "nccl") (r "^0.4.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.30.0") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "0bj5j0pbi8lzi6binqjapgl8jdbp0nv59m8rzp7jgvfg06p67ddh") (y #t)))

(define-public crate-asteroid-0.2.0 (c (n "asteroid") (v "0.2.0") (d (list (d (n "nccl") (r "^0.4.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.30.0") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "1118qkpfh973lx1ay5bkwd3b0n76jbpy2x2z9xypglwjr4vkcxr1") (y #t)))

