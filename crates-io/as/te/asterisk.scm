(define-module (crates-io as te asterisk) #:use-module (crates-io))

(define-public crate-asterisk-0.1.0 (c (n "asterisk") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.94") (d #t) (k 1)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "168qlxzzkk9dazcn5hd0v5fn9kr51iz1v2dypl2da20adfl4p997")))

