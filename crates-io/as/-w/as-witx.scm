(define-module (crates-io as -w as-witx) #:use-module (crates-io))

(define-public crate-as-witx-0.1.0 (c (n "as-witx") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "0v1r2zglm6gnllvjiq3kja4fgw6mwr3b578vfs7fq65iwz4ij8ys")))

(define-public crate-as-witx-0.1.1 (c (n "as-witx") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "0jhf1jiwrklc514r3c9spdxzyk64jzs13x2dws1g716xz8ghlf6k")))

