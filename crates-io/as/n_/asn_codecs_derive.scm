(define-module (crates-io as n_ asn_codecs_derive) #:use-module (crates-io))

(define-public crate-asn_codecs_derive-0.1.0 (c (n "asn_codecs_derive") (v "0.1.0") (d (list (d (n "asn-codecs") (r "=0.1.0") (d #t) (k 0)) (d (n "bitvec") (r "^0.22") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "01k6qxpvw9qdgwbrz1qh0wid1wxsjgfzx16p022kdwv2ld0ask32")))

