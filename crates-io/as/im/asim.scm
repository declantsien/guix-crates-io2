(define-module (crates-io as im asim) #:use-module (crates-io))

(define-public crate-asim-0.1.0 (c (n "asim") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "time" "macros" "rt"))) (k 0)))) (h "18p7s8hbs0w7fvjdp9l9764900w20fp06fh5l9wxhx7gfl1s6578")))

