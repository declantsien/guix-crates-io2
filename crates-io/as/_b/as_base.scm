(define-module (crates-io as _b as_base) #:use-module (crates-io))

(define-public crate-as_base-0.1.0 (c (n "as_base") (v "0.1.0") (d (list (d (n "as_base_proc_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0m77wsrkvsx8ci0xax0x12b2k5xp4m066lkz01kmabnd6hzwianv")))

(define-public crate-as_base-0.1.1 (c (n "as_base") (v "0.1.1") (d (list (d (n "as_base_proc_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0kh8yfm2znmdd7565h7wvlwxrhnypal0q0wm8ijpibnic6s2rh17")))

(define-public crate-as_base-0.1.2 (c (n "as_base") (v "0.1.2") (d (list (d (n "as_base_proc_macro") (r "^0.1.1") (d #t) (k 0)))) (h "1l0cpyg1vnjh3jfqwrh5vamcg6sdv9fk00486k92qkcik2a8lybs")))

(define-public crate-as_base-0.1.3 (c (n "as_base") (v "0.1.3") (d (list (d (n "as_base_proc_macro") (r "^0.1.1") (d #t) (k 0)))) (h "0n1fcsxfcjjahlwcdjblwf2rf4fcprbimm7992wi017ifhls68xw")))

