(define-module (crates-io as _b as_base_proc_macro) #:use-module (crates-io))

(define-public crate-as_base_proc_macro-0.1.0 (c (n "as_base_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "13zv1f668m16ffw459y2a5vgkh5g71gwnlirwjv82226cxr1k927")))

(define-public crate-as_base_proc_macro-0.1.1 (c (n "as_base_proc_macro") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1xl3bmn2qajs2kvyq8bi1hfji4d4hf9n6gxnxj39cwdn29dfbdya")))

