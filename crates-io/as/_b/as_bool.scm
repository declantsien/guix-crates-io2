(define-module (crates-io as _b as_bool) #:use-module (crates-io))

(define-public crate-as_bool-0.1.0 (c (n "as_bool") (v "0.1.0") (h "0m0s6sanyym527lq7m59csjrjx9xmi288c1sbky2j768p7f8xdig")))

(define-public crate-as_bool-0.1.1 (c (n "as_bool") (v "0.1.1") (h "1w99bjfzviy8dvwsfyjwgccdxgr54ccwfi1pz3csicrii577gwbp")))

(define-public crate-as_bool-0.1.2 (c (n "as_bool") (v "0.1.2") (h "0p6xic5bjfypnzwfr0xbmixi2ggwz6avkfxcvh2p0wyx7pmhayc6")))

(define-public crate-as_bool-0.1.3 (c (n "as_bool") (v "0.1.3") (h "1kg8g9b8725s6lbr8w97y8d5zdv4j969wkznpz96hs0ryhcfx1hl")))

