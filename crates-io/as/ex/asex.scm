(define-module (crates-io as ex asex) #:use-module (crates-io))

(define-public crate-asex-0.1.0 (c (n "asex") (v "0.1.0") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "13pjjhkby7k8fxbfrhzv9j6m33mn9jg9wnk4jzi8fk474zi39x5l")))

(define-public crate-asex-0.2.0 (c (n "asex") (v "0.2.0") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0nxqh9ssnj81h6z954v481cfi858nwlnq4dywfggcd8qqd0w8mfi")))

(define-public crate-asex-0.3.0 (c (n "asex") (v "0.3.0") (d (list (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1yqx5lj4p7248miyw3mjxcsjrf6jcl9gbqliv937m57i1qgvkf6d")))

