(define-module (crates-io as #{-i}# as-is) #:use-module (crates-io))

(define-public crate-as-is-0.0.1-alpha (c (n "as-is") (v "0.0.1-alpha") (h "1bnihbd24pb46nk6b7aghvxgz210x24mhyxd32iarai9bgmiznnw") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-as-is-0.0.1-alpha.2 (c (n "as-is") (v "0.0.1-alpha.2") (h "00csw9z3f3silqv3ngh9px28z3n5khsgbysq4h3bnn4qd445k8vn") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-as-is-0.0.1-alpha.3 (c (n "as-is") (v "0.0.1-alpha.3") (h "0xm3vxkzj1hjjh2zbmyylnxxgqkzw4r7m3bqsxxjnqvzcx78ffwj") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-as-is-0.0.1-alpha.4 (c (n "as-is") (v "0.0.1-alpha.4") (h "1yr0wjy6qhvz6azng64jml91z7a5wfj18r48ai7xc8vj7m10vn19") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-as-is-0.0.2-alpha (c (n "as-is") (v "0.0.2-alpha") (h "0h9wa7q06z4mhzsh2jpdy8q8zyk8c6kbnrgjz9iarncnfr1r4479") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-as-is-0.0.3-alpha (c (n "as-is") (v "0.0.3-alpha") (h "0gbkqh1z7mknav3v1gn9vyarfkjpqrlm3gs1ibv6w39dq8i27zkq") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-as-is-0.0.4-alpha (c (n "as-is") (v "0.0.4-alpha") (h "1rgcn1l09fq8xdfh2ljiiw4yvmq0hgs4c6g85xslzmq5fhxbv10i") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.5-alpha (c (n "as-is") (v "0.0.5-alpha") (h "07z5np8s6ck86y5pl7v0k7dng58y0gk7ckdrn7kf607msb3n9znz") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.5-alpha.1 (c (n "as-is") (v "0.0.5-alpha.1") (h "08ay5q5ygalq5552py0c734i9bglqyk84gl2mgiiv40jnxkvk3yd") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.5-alpha.2 (c (n "as-is") (v "0.0.5-alpha.2") (h "0a9mk87wk8nsmlx9yvh457psaqr6rh809xfvzwqnxpk1pckapxzk") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.6 (c (n "as-is") (v "0.0.6") (h "0vd2gmgs08pjm23vlxd4g63ff84fxl6cpmclb5cqrp9j9j8kcqyw") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.7 (c (n "as-is") (v "0.0.7") (h "0rm9va65695c5m4318qmkdw4vahx9m55j7ggsn90nfy512gqgzcq") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.8 (c (n "as-is") (v "0.0.8") (h "1hgsc585s84aah23al574zpsz96dsyvmi1rspvh79ncw74g4vr92") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.9 (c (n "as-is") (v "0.0.9") (h "1a1jp5qigbz2vp1l7blazkmnf096srr6ygl932awrbljbnfc0f1z") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.10 (c (n "as-is") (v "0.0.10") (h "1krly27ls59dfgaxcl220ijv1sx6szbinr2kpxz0p9npxpzsyxar") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.11 (c (n "as-is") (v "0.0.11") (h "0d2r55110529dc00668iyxsdxv6p8y0lmyig1i27wbiq5jkrq6bw") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.12 (c (n "as-is") (v "0.0.12") (h "062dqn24wvc808bkr3yvansn8q1r3022pi07pcsjxhdhnk0d7gyb") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.13 (c (n "as-is") (v "0.0.13") (h "0h0v3ifl5sv6w85sw53a8fidj9iwc1rnlk7y56m8ypkf9x413a72") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.14 (c (n "as-is") (v "0.0.14") (h "1fcm3wkx1zda85li7jcj4in4kmaypa58n27fga3a3ihbzlh70rk3") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.15 (c (n "as-is") (v "0.0.15") (h "0bslj5qa91x14jy33qz60zjjffzfhd1miwdl51inkjp4rmajki3b") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.16 (c (n "as-is") (v "0.0.16") (h "0z8mnc4xymwd7xg07vpapnf3fz2b019rmdclrb12968gfyzwshxm") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.17 (c (n "as-is") (v "0.0.17") (h "11wqja0r1bya3c7d2n71v3gxzdzr30mxq0qk4jxb82lbfhmmvvdl") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.18 (c (n "as-is") (v "0.0.18") (h "0s0xfld19z30zhpl2x0f75x588rqzalnxd7cg8lajj2rrnj95lp2") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.19 (c (n "as-is") (v "0.0.19") (h "0qvxs3jvidjm9rpgcmhgq2c1r8n3psqpd9ihnzmi590g41k1szyr") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.20 (c (n "as-is") (v "0.0.20") (h "0vmjcvmmqd33h0wg0k9550mfbda8c5bc0x1rkdp84c25b4mryyrd") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.21 (c (n "as-is") (v "0.0.21") (h "08zlnzw7cwgfq8kwi65776i75mbvwyakrzl26g4f97wryzd6q2ya") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.22 (c (n "as-is") (v "0.0.22") (h "15mih4zqhc51cyggx07m9l8nmc0an2aw03r1pzsxdz3nr1c1s14k") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.23 (c (n "as-is") (v "0.0.23") (h "0gfkv7zbl2h3xlsw3abcx5485clxlrkhzqvjk3ih9x058vzp7vxx") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.24 (c (n "as-is") (v "0.0.24") (h "10lcsiy8ni4bv3lsf3vggf78q9gmyjvg8nyd0s8ccm01iswrcw6x") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.25 (c (n "as-is") (v "0.0.25") (d (list (d (n "ref-ops") (r "^0.2") (d #t) (k 0)))) (h "0cfm2jqmkwr4svwn4vvcflywvs3y8f3yllxambd5h73pvhhfimzz") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.26 (c (n "as-is") (v "0.0.26") (d (list (d (n "ref-ops") (r "^0.2") (d #t) (k 0)))) (h "0m6542rzj1h1s7r756lf28qj6bk0z9xid960kvg087yj6bdhdrvx") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.27 (c (n "as-is") (v "0.0.27") (d (list (d (n "ref-ops") (r "^0.2") (d #t) (k 0)))) (h "052vrbhzpricdhjwzr8663ppkn7wlfqxn48dlyvbygc94isf30x9") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.28 (c (n "as-is") (v "0.0.28") (d (list (d (n "ref-ops") (r "^0.2") (d #t) (k 0)) (d (n "siphasher") (r "^1") (d #t) (k 2)))) (h "0mmpk9ldq0y304apwb3lx0xs7p07iayrwkylpd74g9zisl007hq5") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-as-is-0.0.29 (c (n "as-is") (v "0.0.29") (d (list (d (n "char-buf") (r "^0.1") (d #t) (k 2)) (d (n "ref-ops") (r "^0.2") (d #t) (k 0)) (d (n "siphasher") (r "^1") (d #t) (k 2)))) (h "1d0na43kgnnnskwpx13flbbn8g1haimdpkgk3s2qbypsx3xqd382") (f (quote (("default" "alloc") ("alloc"))))))

