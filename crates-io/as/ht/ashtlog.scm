(define-module (crates-io as ht ashtlog) #:use-module (crates-io))

(define-public crate-ashtlog-0.1.0 (c (n "ashtlog") (v "0.1.0") (h "06lgbnsd772gq1ngzaipn7qdwv1n8lk694nm5w1k8wq15iv8172p")))

(define-public crate-ashtlog-0.2.0 (c (n "ashtlog") (v "0.2.0") (h "10sn7d8b1y495h7nkjr0mhv1fi1drjabvlcdjnc1jx78fxviipms")))

