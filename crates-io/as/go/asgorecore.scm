(define-module (crates-io as go asgorecore) #:use-module (crates-io))

(define-public crate-AsgoreCore-0.1.0 (c (n "AsgoreCore") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "const_format") (r "^0.2.30") (d #t) (k 0)) (d (n "csv") (r "^1.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0581v85mjsr42zik39isvvwdcwfslfxkzgxsf5yfzkk0h9qjv1i2")))

(define-public crate-AsgoreCore-0.1.1 (c (n "AsgoreCore") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "const_format") (r "^0.2.30") (d #t) (k 0)) (d (n "csv") (r "^1.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0j8fhqnyzxpwawccyyb7bl92vx56zsph8q1wskjsz82g368hxzm3")))

(define-public crate-AsgoreCore-0.1.2 (c (n "AsgoreCore") (v "0.1.2") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "const_format") (r "^0.2.30") (d #t) (k 0)) (d (n "csv") (r "^1.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1lywavh9v903m0vsn4nih5jznlc15nwvbdrfwcnj0345xivapva3")))

