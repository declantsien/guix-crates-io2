(define-module (crates-io as h_ ash_shader_creator) #:use-module (crates-io))

(define-public crate-ash_shader_creator-1.0.0 (c (n "ash_shader_creator") (v "1.0.0") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "0apfdl53x79qxj5c9d8j4i4if1m5hqc4fmgbnfpl5i07rd0zk1lm") (y #t)))

(define-public crate-ash_shader_creator-1.0.1 (c (n "ash_shader_creator") (v "1.0.1") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "183n0w7hgdq6p3kckbsfglfv85ijpd8if931fxq4fiig76f4xv7x") (y #t)))

(define-public crate-ash_shader_creator-1.0.2 (c (n "ash_shader_creator") (v "1.0.2") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "0qwncwyrfj7ag3gzxg3i489q4ndnfbqs7bqmrina5kfsavgdxxsn") (y #t)))

(define-public crate-ash_shader_creator-1.1.2 (c (n "ash_shader_creator") (v "1.1.2") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "1n73awlfrhpc4jx2grdn3pb3s8as2pi9la5vpx17qbf0xzldgnyk") (y #t)))

(define-public crate-ash_shader_creator-1.2.0 (c (n "ash_shader_creator") (v "1.2.0") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "1y9c79vwsq17ips4yb8ziqhyig47n8jnkr30x5aqdvya03dzhh72") (y #t)))

(define-public crate-ash_shader_creator-1.2.1 (c (n "ash_shader_creator") (v "1.2.1") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "1fg1nipcsrgcq652fp5zik1fgazr5m1lsyhy3anvm9hdqarryl99") (y #t)))

(define-public crate-ash_shader_creator-1.2.2 (c (n "ash_shader_creator") (v "1.2.2") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "122g7sq6ba21zi7bjf6kcy6kp50j0zqql914hdsw2x9v9rmngkba") (y #t)))

(define-public crate-ash_shader_creator-1.3.2 (c (n "ash_shader_creator") (v "1.3.2") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "1vqgvcf956a7zpb52bjnds5bqcg05hp3ny6n9gixj51v4fyd0g68") (y #t)))

(define-public crate-ash_shader_creator-1.3.3 (c (n "ash_shader_creator") (v "1.3.3") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "021wx22x8z8hg036qygalv6qindc2ii6gij5kgp6vpwzl6yfing0")))

(define-public crate-ash_shader_creator-1.4.0 (c (n "ash_shader_creator") (v "1.4.0") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "1b0rx2qhz928ja2mqlxblxaafnn9ayqwyw3nphprb6shxpcvg3lx") (y #t)))

(define-public crate-ash_shader_creator-1.4.1 (c (n "ash_shader_creator") (v "1.4.1") (d (list (d (n "ash") (r "^0.33.0") (d #t) (k 0)))) (h "1ahzlp2cnfxp82y447ar35f7va4q2q93gsf8xxbf97x41yg2q2l1")))

