(define-module (crates-io as m_ asm_unroll) #:use-module (crates-io))

(define-public crate-asm_unroll-0.1.0 (c (n "asm_unroll") (v "0.1.0") (h "0d1pa0kl54hw5x68wi2c2fxmi912dcfad1jlax6wa64myqiq26x8")))

(define-public crate-asm_unroll-0.1.1 (c (n "asm_unroll") (v "0.1.1") (h "16ygmzlg7v1qwpxkk6hqbfvvww4qf0qjyrw4pfqly9h1vc8hd2y4")))

