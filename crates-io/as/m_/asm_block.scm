(define-module (crates-io as m_ asm_block) #:use-module (crates-io))

(define-public crate-asm_block-0.1.0 (c (n "asm_block") (v "0.1.0") (h "1w8sjg4bg8ymfrqig9ccvrrhvgip0jglamgppq372cjcd5zfi0r3")))

(define-public crate-asm_block-0.1.1 (c (n "asm_block") (v "0.1.1") (h "0kyb8b6aqcy7ssc126mcp2zzf3dg6a84cd4z5x4y7i1w0v7l9mqp")))

(define-public crate-asm_block-0.1.2 (c (n "asm_block") (v "0.1.2") (h "1d3skiffa94xmhmi09karzrbm35677py3bxfk959ha3ar1dkyd2k")))

(define-public crate-asm_block-0.1.3 (c (n "asm_block") (v "0.1.3") (h "1aqi5777yx3blrq6qirpn9m60a82srpw3z8r3wrhzvqmry80jv26")))

