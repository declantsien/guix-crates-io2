(define-module (crates-io as m_ asm_riscv) #:use-module (crates-io))

(define-public crate-asm_riscv-0.1.0 (c (n "asm_riscv") (v "0.1.0") (h "0g1195d9fzgh23hacha7qzhss9r6zf2l8rvcqmsnxxyqrqp05b2s")))

(define-public crate-asm_riscv-0.2.0 (c (n "asm_riscv") (v "0.2.0") (h "1gh1xc9fr9mz3klcjlrz9pb9b9nbpz5wzysc8h2sffgjcpx8iy19")))

