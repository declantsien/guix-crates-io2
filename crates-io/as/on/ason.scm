(define-module (crates-io as on ason) #:use-module (crates-io))

(define-public crate-ason-0.1.0 (c (n "ason") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "hexfloat2") (r "^0.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "09cd1j7x7jrfy2nzcxrhdnwp197yc4dvnzkv9pmm7cdlik60v8qi")))

(define-public crate-ason-0.1.1 (c (n "ason") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "hexfloat2") (r "^0.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0lfqv7cf13zyyx2dh1hly7fahn5p72a1zhbrqvxbmhfjghidabay")))

(define-public crate-ason-0.1.2 (c (n "ason") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "hexfloat2") (r "^0.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "06k34zicrw5j3b4zpllpwf6fh2hc28b2qi7i6zifzxrhp5cz5djs")))

(define-public crate-ason-0.1.3 (c (n "ason") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "hexfloat2") (r "^0.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "08rrad80laxlsir8aqi0lkf1v9mc5rd74m4kx2iz5lp4kzs7al32")))

(define-public crate-ason-0.1.4 (c (n "ason") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "hexfloat2") (r "^0.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "066hinzl9j5wr16m9b7nvd6bykcx94skxxh2mr5njbfgqxj5qmvw")))

(define-public crate-ason-0.1.5 (c (n "ason") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "hexfloat2") (r "^0.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "0w4kymdnqiiimd60bx0f10j04jp7ry2addi9kgsss8lr2xdqfjan")))

(define-public crate-ason-0.1.6 (c (n "ason") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "hexfloat2") (r "^0.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "1h5973cx84y5x3rjy96nbik7i3m3jgvf6drdgx43sf12ir5gdq1i")))

(define-public crate-ason-0.2.0 (c (n "ason") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "hexfloat2") (r "^0.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)))) (h "07xly4paa6rg11ybpf4aav9m0ma4g4yfssqj7bl0aacwzva8f0s8")))

