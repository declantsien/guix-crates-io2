(define-module (crates-io as so associated_list) #:use-module (crates-io))

(define-public crate-associated_list-0.1.0 (c (n "associated_list") (v "0.1.0") (h "1y59wqn0x8qdlyf5q5yplfn50fjgwbspmv3yh1b92y084dkypasl") (f (quote (("nightly" "allocator_api") ("default") ("allocator_api")))) (r "1.75")))

(define-public crate-associated_list-0.2.0 (c (n "associated_list") (v "0.2.0") (d (list (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1rjip2l60j384pq5iw0v62fsi7qk45pzsww0ykmlqm8m8yrmihq2") (f (quote (("nightly" "allocator_api" "doc_auto_cfg") ("doc_auto_cfg") ("default") ("allocator_api")))) (r "1.75")))

