(define-module (crates-io as so associative-cache) #:use-module (crates-io))

(define-public crate-associative-cache-1.0.0 (c (n "associative-cache") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "18ipz352zs7fmyzxxqc32dhwckz2c2c7mhsn08c58qqlhx6xxy7w")))

(define-public crate-associative-cache-1.0.1 (c (n "associative-cache") (v "1.0.1") (d (list (d (n "rand") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "05lg0mwpqfqb9zh958x0358x1k5ngmmmbzjnp0imrd8vzhrn40a6")))

(define-public crate-associative-cache-2.0.0 (c (n "associative-cache") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "1b9bgf19c27sdch6c4x2qyf34i1cl8f328knv1yk1irbg9vcv4xr") (r "1.65")))

