(define-module (crates-io as so associated-derive) #:use-module (crates-io))

(define-public crate-associated-derive-0.1.0 (c (n "associated-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "10i2gk4gyk34qmcpviv6q29z9iy44bhf34va5vvgq6pb095z5cx9")))

(define-public crate-associated-derive-0.1.1 (c (n "associated-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "101bn8ncrvxpi77zc9yqw7r28m6njrcg7d1aqa61c652n4mk06mq")))

