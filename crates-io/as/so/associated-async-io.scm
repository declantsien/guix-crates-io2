(define-module (crates-io as so associated-async-io) #:use-module (crates-io))

(define-public crate-associated-async-io-1.0.0 (c (n "associated-async-io") (v "1.0.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 2)))) (h "1fpai73nfbk323jlvkayd4vx2naydcjj5p2z60a7431lqkxrfbb6")))

(define-public crate-associated-async-io-1.0.1 (c (n "associated-async-io") (v "1.0.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (d #t) (k 2)))) (h "01n92wlpvh41nvgbppzmblndn7rpmk63ack3d989n9y90s1xmmg9")))

