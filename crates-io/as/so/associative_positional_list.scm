(define-module (crates-io as so associative_positional_list) #:use-module (crates-io))

(define-public crate-associative_positional_list-0.1.0 (c (n "associative_positional_list") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "1z0dk6z4cll7mg5n25nq8sni2qwi4cj07q0z0vkzhnma8lfkq3vs")))

(define-public crate-associative_positional_list-0.1.1 (c (n "associative_positional_list") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0xl6x20vl7q60di49h1izl5q6317zw6wbpxgm0mhqjay0dv4q06s")))

(define-public crate-associative_positional_list-0.1.2 (c (n "associative_positional_list") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "1kwmcwkqnj9d2cjmxsbhlj9zvkglmprhahn8ixagvxq739gf58x4")))

(define-public crate-associative_positional_list-0.1.3 (c (n "associative_positional_list") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "1nlyr2w5m9ifq8i75zzjx4wqmcimggcdplpdsfdp9avc84b478p4")))

