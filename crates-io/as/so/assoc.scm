(define-module (crates-io as so assoc) #:use-module (crates-io))

(define-public crate-assoc-0.1.0 (c (n "assoc") (v "0.1.0") (h "1g089ixfiavwlkw56iv53pv1p13njbdyd1bhdr6afwwnmikhxvmx")))

(define-public crate-assoc-0.1.1 (c (n "assoc") (v "0.1.1") (h "1x52b8ybs7pm04rgpv31f83x1vccwk1icnnhzk7hwh7bvy7k9g6d")))

(define-public crate-assoc-0.1.2 (c (n "assoc") (v "0.1.2") (h "1a2krpl2syr0m38cy5vmsdp48yk9x5vhm5x3daq0p1dnmnwhn81p")))

(define-public crate-assoc-0.1.3 (c (n "assoc") (v "0.1.3") (h "1a2rk2fcazrhv8bszxiibf8pdj4hbzqk7dm4gwldgfdd7lcp1p5z")))

