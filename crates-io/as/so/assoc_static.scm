(define-module (crates-io as so assoc_static) #:use-module (crates-io))

(define-public crate-assoc_static-0.0.1 (c (n "assoc_static") (v "0.0.1") (h "0y9d6wx0hgkcial5ach701ixxr4j2iq5wsyfkxza3r95drbsbl04")))

(define-public crate-assoc_static-0.1.0 (c (n "assoc_static") (v "0.1.0") (h "02kvbx4krsa2lz4lgm1ff0kk674lq796hqi9f6yz96m63q3gbdlb")))

(define-public crate-assoc_static-0.1.1 (c (n "assoc_static") (v "0.1.1") (h "013c29hqgg249m033l18443x3ri9gs54w7swshz72fq8vh2xzsla")))

(define-public crate-assoc_static-0.2.0 (c (n "assoc_static") (v "0.2.0") (h "0vwnhiigjab0gz3515knwxzq8hsgv483315gvhzq4dk3x7b9n7hj")))

(define-public crate-assoc_static-0.2.1 (c (n "assoc_static") (v "0.2.1") (h "1qmhq6vjyga8zym7snl2vs3mdq3a928pa4aizn64jk38wmxmn6i9")))

(define-public crate-assoc_static-1.0.0-pre0 (c (n "assoc_static") (v "1.0.0-pre0") (h "1q90nzsqn3q00icp5v5xs09spsjpgrbs6swf43c4z8m59c7k460i")))

(define-public crate-assoc_static-1.0.0 (c (n "assoc_static") (v "1.0.0") (h "0bjxzkllcqjzpd0pccpx0a6a3d883704x0qsgssb967vjxm2450l")))

