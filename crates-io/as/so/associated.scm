(define-module (crates-io as so associated) #:use-module (crates-io))

(define-public crate-associated-0.1.0 (c (n "associated") (v "0.1.0") (d (list (d (n "associated-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0mahcb4zdw3y9m8g147178rfli2xl3bn0ff548lwfgv7s71jxy75") (f (quote (("derive" "associated-derive") ("default" "derive"))))))

(define-public crate-associated-0.2.0 (c (n "associated") (v "0.2.0") (d (list (d (n "associated-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0md2744qmm9bnlxr9fiz013pvahhbw05w7nl7cjc5scwd45jwmxf") (f (quote (("derive" "associated-derive"))))))

