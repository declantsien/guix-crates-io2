(define-module (crates-io as ap asap_deps_jsonwebtoken) #:use-module (crates-io))

(define-public crate-asap_deps_jsonwebtoken-4.0.1-dev (c (n "asap_deps_jsonwebtoken") (v "4.0.1-dev") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "ring") (r "^0.12.0") (f (quote ("rsa_signing" "dev_urandom_fallback"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "040ygykam8bl06y8x23vwjhxblff27kahsjq2f4ix6z8h21md1l0")))

