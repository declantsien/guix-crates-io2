(define-module (crates-io as e- ase-swatch) #:use-module (crates-io))

(define-public crate-ase-swatch-0.1.0 (c (n "ase-swatch") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structure") (r "^0.1.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "15jf4iwgjfk6ay68la3k8ipsvkwhwvj17h1rnwzmjpaw8bhcwlfv") (f (quote (("default" "console_error_panic_hook"))))))

