(define-module (crates-io as an asankov-rust-fundamentals) #:use-module (crates-io))

(define-public crate-asankov-rust-fundamentals-0.1.0 (c (n "asankov-rust-fundamentals") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1l142rbylh1m741d3isv38hghgamdlh41vcxy1jrgas58dbkikvy") (y #t)))

(define-public crate-asankov-rust-fundamentals-0.1.1 (c (n "asankov-rust-fundamentals") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "121s6xkbg7rhlbbq1d1032p4cs5m6byv7zwvijkaidbkfd68rmkz")))

