(define-module (crates-io as an asana) #:use-module (crates-io))

(define-public crate-asana-0.1.0 (c (n "asana") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.2") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "07j8fldd0wlb7pm7sd9i5b66plfm07yfv0l7gjsjmn6hj9nl5r97")))

(define-public crate-asana-0.1.1 (c (n "asana") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "io-util" "sync" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1f887qskw0w5rvfvvz3488ws7vsn0wfjzg1n80vhs5fw4mhlycp4")))

