(define-module (crates-io as tc astcenc-rs) #:use-module (crates-io))

(define-public crate-astcenc-rs-0.1.0 (c (n "astcenc-rs") (v "0.1.0") (d (list (d (n "astcenc-sys") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)))) (h "0gc2b9cqxxn2a85yh3902rrgrw8f8sndycay96d55s97n5xdhglz")))

