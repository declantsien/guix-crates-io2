(define-module (crates-io as tc astcenc-sys) #:use-module (crates-io))

(define-public crate-astcenc-sys-0.1.0 (c (n "astcenc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07axaln1ajm2rhjyab1dmjhjj90qbhzsvwjpg7vdahhy6q5xp13c")))

(define-public crate-astcenc-sys-0.1.2 (c (n "astcenc-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1fhs8m4799ljmssddr7hd4an69v4rrn9qmlvgmknp7bkzrxrkirl")))

(define-public crate-astcenc-sys-0.1.3 (c (n "astcenc-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "012gbm8w7crvqwq9qs38a2p098qw71hiasyil8xw252qy1r6ig1n")))

(define-public crate-astcenc-sys-0.1.4 (c (n "astcenc-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1l2zbb7cgv59589a9l5lks3aaf7k5j60b1h2s75qmdc461yaz1cm")))

(define-public crate-astcenc-sys-0.1.5 (c (n "astcenc-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07z0lj59qlplg9sngfh0ph3kl0aa8xkvshbbbl21xvhglasky6pd")))

