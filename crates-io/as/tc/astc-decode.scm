(define-module (crates-io as tc astc-decode) #:use-module (crates-io))

(define-public crate-astc-decode-0.1.0 (c (n "astc-decode") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "1p8sc4lc74b34l4v4lycvsq5h7ris2fs8xkw7wliramjxcr8r8zm")))

(define-public crate-astc-decode-0.2.0 (c (n "astc-decode") (v "0.2.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "04mi1gwkiysqkggxp8isgq94wbm15ssnvvlayxpbdm6z08a47vfa")))

(define-public crate-astc-decode-0.3.0 (c (n "astc-decode") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)))) (h "051jx5ls0xigpr6bzbv1ql6d95bd59ng15kdlihniymb43xi3sgy")))

(define-public crate-astc-decode-0.3.1 (c (n "astc-decode") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)))) (h "0srp1vvdjlx7kr3ws7q910jv13nafx5yr4jn8kxbf81f43k5hq1d")))

