(define-module (crates-io as i_ asi_opengl) #:use-module (crates-io))

(define-public crate-asi_opengl-0.1.0 (c (n "asi_opengl") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0amm3nyr8rfd64vwy122yjc2yn51wkq0w2c1k5v6qa6s410wqa9g")))

(define-public crate-asi_opengl-0.2.0 (c (n "asi_opengl") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zf000b38p5y1bjmn07c517gnxn7rp0iy5hvlv5hwqa5d21gjhcv")))

(define-public crate-asi_opengl-0.3.0 (c (n "asi_opengl") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pn97r1j1p00zsypqrv0vdqb7rgvh9kynppdz44hm23qwfjd6i0d")))

(define-public crate-asi_opengl-0.3.1 (c (n "asi_opengl") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mb0svpsq99hip2x85s7k3dfn21wgxp8rvxvq3k457j52hfznnd1")))

(define-public crate-asi_opengl-0.3.2 (c (n "asi_opengl") (v "0.3.2") (d (list (d (n "dl_api") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "175iwlv3v2is5ddmzl3v2sfs48c3y3pzccadjvczi2vqlkpin0a7")))

(define-public crate-asi_opengl-0.3.3 (c (n "asi_opengl") (v "0.3.3") (d (list (d (n "dl_api") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0cjyzw20h5367gm4v269zfqghyzbgl6wq8x3hwhafplq56z4b5ms")))

(define-public crate-asi_opengl-0.3.4 (c (n "asi_opengl") (v "0.3.4") (d (list (d (n "dl_api") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1r4gmnaxqknrbiwyri39w6rd9qq8hxlszcgqfvmwv5vm966c9crr")))

(define-public crate-asi_opengl-0.4.0 (c (n "asi_opengl") (v "0.4.0") (d (list (d (n "dl_api") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "038nwlpn6aqh1nqapdhb1sb2lggs8fa987xpss4gnpcv2002831k")))

(define-public crate-asi_opengl-0.5.0 (c (n "asi_opengl") (v "0.5.0") (d (list (d (n "dl_api") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "09i0mbskcmy1ds06ds9a3abyb530dip4yhdh2vqczb7fpa613g6r")))

(define-public crate-asi_opengl-0.5.1 (c (n "asi_opengl") (v "0.5.1") (d (list (d (n "dl_api") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0wsbbw3kpyiwmqald8cd4aw4r1920bslpd9nhn2a70z1cf9rwywz")))

(define-public crate-asi_opengl-0.6.0 (c (n "asi_opengl") (v "0.6.0") (d (list (d (n "dl_api") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "19ww27yp1y9cvx6v3n75vyfqly4dfhr8zqjad81b43mbxpjc9s2m")))

(define-public crate-asi_opengl-0.6.1 (c (n "asi_opengl") (v "0.6.1") (d (list (d (n "dl_api") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0i36j0njd8s6blvp6pnac6pzfn749mpp2gmpxwwa87f7n7gsa8rp")))

(define-public crate-asi_opengl-0.6.2 (c (n "asi_opengl") (v "0.6.2") (d (list (d (n "dl_api") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "1d018plvx2j392xml3yrjid344zrfldj910jzbzkbsfagix2g0ns")))

