(define-module (crates-io as i_ asi_vulkan) #:use-module (crates-io))

(define-public crate-asi_vulkan-0.1.0 (c (n "asi_vulkan") (v "0.1.0") (d (list (d (n "afi") (r "^0.3") (d #t) (k 0)) (d (n "ami") (r "^0.5") (d #t) (k 0)) (d (n "gcc") (r "^0.3.50") (d #t) (k 1)))) (h "0szzdwskkjqjki19sc1wsqp8fmqyaqhdvn7b60vv8y2y9nzkziyd") (f (quote (("validation") ("checks"))))))

(define-public crate-asi_vulkan-0.1.1 (c (n "asi_vulkan") (v "0.1.1") (d (list (d (n "ami") (r "^0.5") (d #t) (k 0)) (d (n "gcc") (r "^0.3.50") (d #t) (k 1)))) (h "13n4szxg6clda738z670zkxqwifi0vp3v97vqd76bqr6x7xxkxif") (f (quote (("validation") ("checks"))))))

(define-public crate-asi_vulkan-0.2.0 (c (n "asi_vulkan") (v "0.2.0") (d (list (d (n "ami") (r "^0.5") (d #t) (k 0)) (d (n "gcc") (r "^0.3.50") (d #t) (k 1)))) (h "0906xhvhp88nyy3bs5b9axqjys07ryldia3ha57qn7nxbdyw03y9") (f (quote (("validation") ("checks"))))))

(define-public crate-asi_vulkan-0.2.1 (c (n "asi_vulkan") (v "0.2.1") (d (list (d (n "ami") (r "^0.5") (d #t) (k 0)))) (h "1gsy97j6rv3aywq3zl01qxl2yfcd704wzqk2fjamfkbk9v4k9h22") (f (quote (("validation") ("checks"))))))

(define-public crate-asi_vulkan-0.3.0 (c (n "asi_vulkan") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vk43x6xch0g5h8rxy6798d1hf0jxd1kc7jr4i6qxwak3h3g4fia") (f (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.4.0 (c (n "asi_vulkan") (v "0.4.0") (d (list (d (n "ami") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xj38p50j901azbpzbnbhfbzp7rc21c40dc8x1js3p3lhkkriizy") (f (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.5.0 (c (n "asi_vulkan") (v "0.5.0") (d (list (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l04gc7xnsi4s9wi79if629nf41lfslcf3nyhy270jiavar71jvh") (f (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.6.0 (c (n "asi_vulkan") (v "0.6.0") (d (list (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "dl_api") (r "^0.1") (d #t) (k 0)))) (h "075yyp8wdw1prcapirnzpc60yckz8s96d1s6plahfxdpndd7ppzm") (f (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.7.0 (c (n "asi_vulkan") (v "0.7.0") (d (list (d (n "ami") (r "^0.8") (d #t) (k 0)) (d (n "dl_api") (r "^0.1") (d #t) (k 0)))) (h "0snqgq4qdzws9fmxij662d1rn4n8hw2ga62cgmcg8ijfznw60lvq") (f (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.7.1 (c (n "asi_vulkan") (v "0.7.1") (d (list (d (n "ami") (r "^0.9") (d #t) (k 0)) (d (n "dl_api") (r "^0.1") (d #t) (k 0)))) (h "1gmv1xvza9rm4mwdy5bbbq1r493w0y3pr0w0ynal1knakskmvn25") (f (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.7.2 (c (n "asi_vulkan") (v "0.7.2") (d (list (d (n "ami") (r "^0.10") (d #t) (k 0)) (d (n "dl_api") (r "^0.1") (d #t) (k 0)))) (h "0iixks5naff0zmw8w8xvmi657i93kcfa5qnrcl4962vyq4hhzabw") (f (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.8.0 (c (n "asi_vulkan") (v "0.8.0") (d (list (d (n "dl_api") (r "^0.2") (d #t) (k 0)))) (h "1l41mqhzscxsra9l0rh23mf3vk62kb4ayyyz53lcxsbjlj6cck6x") (f (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.9.0 (c (n "asi_vulkan") (v "0.9.0") (d (list (d (n "awi") (r "^0.8") (d #t) (k 0)) (d (n "dl_api") (r "^0.2") (d #t) (k 0)) (d (n "euler") (r "^0.4") (d #t) (k 0)))) (h "0g05jc99q4aw52fz91ch4a4nn14jh56pjszgh2dc26s5w7hp2bxg") (f (quote (("default") ("checks"))))))

