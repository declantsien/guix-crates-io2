(define-module (crates-io as m6 asm6502) #:use-module (crates-io))

(define-public crate-asm6502-0.1.0 (c (n "asm6502") (v "0.1.0") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "0ly1yyiydzrp6c1c2hl7k5hbiqkg8xm86s3bv8py6386wr16cw06")))

(define-public crate-asm6502-0.1.1 (c (n "asm6502") (v "0.1.1") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "02m81nf53rbqqdw1k4ws183fdw6309y9sw6qgah3iszdna5x0xn2")))

(define-public crate-asm6502-0.1.2 (c (n "asm6502") (v "0.1.2") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "0pj9pwxvba046mlkqlsc60bsjs18ryvxrkw8w5n0n9ml4r4yzfzk")))

