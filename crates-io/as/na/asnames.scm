(define-module (crates-io as na asnames) #:use-module (crates-io))

(define-public crate-asnames-0.1.0 (c (n "asnames") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "oneio") (r "^0.10.1") (f (quote ("remote"))) (k 0)))) (h "0w6s9nxf2avl7inl8kf3839pl1nkpdfwpk67wfigmpz5s1lbykn8")))

