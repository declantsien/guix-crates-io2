(define-module (crates-io as ur asura) #:use-module (crates-io))

(define-public crate-asura-0.1.0 (c (n "asura") (v "0.1.0") (d (list (d (n "oorandom") (r "^11.1") (d #t) (k 0)))) (h "058zlqgxmnx4b45gvzz5hf62g6gy832lz2030sjzvqr71r18phsk")))

(define-public crate-asura-0.2.0 (c (n "asura") (v "0.2.0") (d (list (d (n "oorandom") (r "^11.1") (d #t) (k 0)))) (h "013fybgjpnhvndwxmvw0zkcpwr476i0mps60kp5z2zq5f21km962")))

(define-public crate-asura-0.3.0 (c (n "asura") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "oorandom") (r "^11.1") (d #t) (k 0)))) (h "03c0gn4pj8mwrigbhw7vyvrfa9bip3a3c5a6y6rwkdynxvcacj7x")))

