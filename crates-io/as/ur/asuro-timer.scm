(define-module (crates-io as ur asuro-timer) #:use-module (crates-io))

(define-public crate-asuro-timer-0.2.0 (c (n "asuro-timer") (v "0.2.0") (d (list (d (n "win32console") (r "^0.1.4") (d #t) (k 0)) (d (n "winrt-notification") (r "^0.3.0") (d #t) (k 0)))) (h "1j3w7ycjjmqiq76l3lbb95ph0fihyqz6kxwk8cjjnfk1xhlidvmi")))

(define-public crate-asuro-timer-0.2.1 (c (n "asuro-timer") (v "0.2.1") (d (list (d (n "win32console") (r "^0.1.4") (d #t) (k 0)) (d (n "winrt-notification") (r "^0.3.0") (d #t) (k 0)))) (h "0krmlgpch5c9waf9bgchfgfya4h8y226iy12kqnqm840dlii6750")))

