(define-module (crates-io as ke asker) #:use-module (crates-io))

(define-public crate-asker-0.1.0 (c (n "asker") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0pd5syhwzi2jlnmr0b5bwqk3aj0a96mpwydzhwck7xyjayl9hxkp")))

(define-public crate-asker-0.2.0 (c (n "asker") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "safe_print") (r "^1.0.0") (d #t) (k 0)))) (h "13rwhw2p8xiax8bs35kbc1d0gqk4bpw7sphdq5i4k5dynn7gm6z6")))

(define-public crate-asker-0.3.0 (c (n "asker") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "safe_print") (r "^1.0.0") (d #t) (k 0)))) (h "18qqa1d999w6qiimhdfj23ncsb7rq0gs418biy6ahlp40ii6namd")))

