(define-module (crates-io as om asomeip) #:use-module (crates-io))

(define-public crate-asomeip-0.1.0 (c (n "asomeip") (v "0.1.0") (d (list (d (n "afibex") (r "^0.1.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.30") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0gz626nxk3llh85bwh9fhl9xy02fpbgzfbzdkj1iyx53c48wb4ln")))

(define-public crate-asomeip-0.1.1 (c (n "asomeip") (v "0.1.1") (d (list (d (n "afibex") (r "^0.2.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.30") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "102vazjggc8p64br1548hcdzj6krh5kav2dd8divipc2n9q6pasb")))

(define-public crate-asomeip-0.1.2 (c (n "asomeip") (v "0.1.2") (d (list (d (n "afibex") (r "^0.2.1") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.30") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0wd5lj5ph0xkq06lnbrbqg25nsl97yrc3rlwc6drk99vks79nfnr")))

(define-public crate-asomeip-0.1.3 (c (n "asomeip") (v "0.1.3") (d (list (d (n "afibex") (r "^0.3.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0xbkbw0qp9g4ja17rcd81i76y58ryrfls6wqwk106qrxg1n1w3pv")))

(define-public crate-asomeip-0.1.4 (c (n "asomeip") (v "0.1.4") (d (list (d (n "afibex") (r "^0.3.1") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "05812pwr5isjns051zpz0pzzar2kd6p9a46gvv5z96w9v8ixxa8i")))

(define-public crate-asomeip-0.1.5 (c (n "asomeip") (v "0.1.5") (d (list (d (n "afibex") (r "^0.3") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1qj5b29bxpf54004q73ks8g1xn48bri5r96y1gnhz3h1xm1sjrvv")))

(define-public crate-asomeip-0.2.0 (c (n "asomeip") (v "0.2.0") (d (list (d (n "afibex") (r "^0.4") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0l694n8i8w9gsj38sp0spnr94mcqkhzn6kwx2z4q66y16hb88829")))

(define-public crate-asomeip-0.2.1 (c (n "asomeip") (v "0.2.1") (d (list (d (n "afibex") (r "^0.4") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1vsvjhb0ysqbrp2ycksqg7495bazdahcaz9vn2vn0j182smy7i4j")))

(define-public crate-asomeip-0.3.0 (c (n "asomeip") (v "0.3.0") (d (list (d (n "afibex") (r "^0.6") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1qp0j24w077lqbjnfrd1cv1y26famwf26f73v5jp1i5g0nfbprfa")))

(define-public crate-asomeip-0.3.1 (c (n "asomeip") (v "0.3.1") (d (list (d (n "afibex") (r "^0.6") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1afyqn32c8256wkgalmyr03rimpgcj3ivla7y6g8nvxkf81hznxi")))

(define-public crate-asomeip-0.3.2 (c (n "asomeip") (v "0.3.2") (d (list (d (n "afibex") (r "^0.6") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "19lg1gah0gc8dl3plgnksppr385np19351zlxfkfyyl9kmxq0z2h")))

(define-public crate-asomeip-0.4.0 (c (n "asomeip") (v "0.4.0") (d (list (d (n "afibex") (r "^0.6") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1n1lh89821sgvvlay0b2maxvvqhsvjag3jbjp4r38q5ifdrfvx86")))

(define-public crate-asomeip-0.4.1 (c (n "asomeip") (v "0.4.1") (d (list (d (n "afibex") (r "^0.6") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1rmndc04pknj7wxc166sd275x581jxnmvg6lw2nkj8wg4i1zlz07")))

(define-public crate-asomeip-0.5.0 (c (n "asomeip") (v "0.5.0") (d (list (d (n "afibex") (r "^0.7.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1g179fqc0l2k45zqlsciy7amk0v5sdgv176smh0xvjx6yzkbyga1")))

(define-public crate-asomeip-0.5.1 (c (n "asomeip") (v "0.5.1") (d (list (d (n "afibex") (r "^0.8.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "15plwb8ln4xl29wncmkgbc5k8bdg3k1iq421xxv0rkrzncq3j3f3")))

(define-public crate-asomeip-0.6.0 (c (n "asomeip") (v "0.6.0") (d (list (d (n "afibex") (r "^0.9.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0bw1l6ndkn32jpfj7rz5hglzxb399ssybch43nd4dqv5fp5jri0c")))

(define-public crate-asomeip-0.7.0 (c (n "asomeip") (v "0.7.0") (d (list (d (n "afibex") (r "^0.10.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1b1zd7v5n5f1p3nvyvcn2jdp1dhwz7zw23lvxmvc223bj1142r1c")))

(define-public crate-asomeip-0.7.1 (c (n "asomeip") (v "0.7.1") (d (list (d (n "afibex") (r "^0.10.1") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "046drhn4w87mkkfhga00xdg46jdiccdmm2m780yxjqrv5j1wc8vs")))

(define-public crate-asomeip-0.8.0 (c (n "asomeip") (v "0.8.0") (d (list (d (n "afibex") (r "^0.10.1") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "092vipfx37ggl45zfd5ihf1ignw5y78d78d9sx72nq993wd2mjgn")))

(define-public crate-asomeip-0.9.0 (c (n "asomeip") (v "0.9.0") (d (list (d (n "afibex") (r "^0.11.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0007d0g133a7w1xa8lr932pv0l8jj52dv34d8a7ldcqkij20kjmx")))

(define-public crate-asomeip-0.9.1 (c (n "asomeip") (v "0.9.1") (d (list (d (n "afibex") (r "^0.11.1") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0hz03s613x15m8gyk52b2k9ldklm3i0kmvwzw9q9pdr7fx30fy5g")))

(define-public crate-asomeip-0.9.2 (c (n "asomeip") (v "0.9.2") (d (list (d (n "afibex") (r "^0.11.1") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1b0caac9wwvzf0c7ax27lq2akw5hxljk8yd7ps43ilym8bb8lg3d")))

