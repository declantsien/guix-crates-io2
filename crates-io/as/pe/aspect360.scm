(define-module (crates-io as pe aspect360) #:use-module (crates-io))

(define-public crate-aspect360-0.1.0 (c (n "aspect360") (v "0.1.0") (d (list (d (n "ring360") (r "^0.2.8") (d #t) (k 0)))) (h "0c2f1kn7a3hkp2ks3p7dj0r5hd5lr0kda9rbqsfkm8a7m5kjhs5v") (y #t)))

(define-public crate-aspect360-0.1.1 (c (n "aspect360") (v "0.1.1") (d (list (d (n "ring360") (r "^0.2.8") (d #t) (k 0)))) (h "0i54xkm8jjlqil4l8gr1a0xx6v6nld0s3cfscl4sb573xm7l3irn") (y #t)))

(define-public crate-aspect360-0.1.2 (c (n "aspect360") (v "0.1.2") (d (list (d (n "ring360") (r "^0.2.8") (d #t) (k 0)))) (h "0gkgb566rnn7bdkw3vnrliwgilf4p72qvsvjan2b1zy03i58syjz") (y #t)))

(define-public crate-aspect360-0.1.4 (c (n "aspect360") (v "0.1.4") (d (list (d (n "ring360") (r "0.2.*") (d #t) (k 0)))) (h "11k5ghsdkq3x72i3nvvxs6xycq1zmwr02839wh25zwchfyaqnp0q") (y #t)))

(define-public crate-aspect360-0.1.5 (c (n "aspect360") (v "0.1.5") (d (list (d (n "ring360") (r "0.2.*") (d #t) (k 0)))) (h "18kv621j7aky9qplrfc5v1rprn9qgn8jr0cfgcfhwz6fwz9lj5dp") (y #t)))

(define-public crate-aspect360-0.1.6 (c (n "aspect360") (v "0.1.6") (d (list (d (n "ring360") (r "0.2.*") (d #t) (k 0)))) (h "00a2zib0zmwh58gl2zdmvswbxdx3a68817x2m3iyjdchiz812ild")))

