(define-module (crates-io as pe aspect-weave) #:use-module (crates-io))

(define-public crate-aspect-weave-0.1.0 (c (n "aspect-weave") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synattra") (r "^0.1.0") (d #t) (k 0)))) (h "0b9jnb9svlwg1jqpkxvaqy7lymvrrl56j6klgmbhzqn9r24hnlm9")))

(define-public crate-aspect-weave-0.1.1 (c (n "aspect-weave") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synattra") (r "^0.1.1") (d #t) (k 0)))) (h "1qbjljcvj0l4ar53l7q2j4yx3j6jm172y6sbdh7mah4qiybynds0")))

(define-public crate-aspect-weave-0.2.0 (c (n "aspect-weave") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synattra") (r "^0.2.0") (d #t) (k 0)))) (h "17f1jvzrg92w15dqm03rxcry7qqss0mlkjkw5qasva5xx88jkcg6")))

(define-public crate-aspect-weave-0.2.1 (c (n "aspect-weave") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synattra") (r "^0.2") (d #t) (k 0)))) (h "0hcza4kr9gv9yi3xc218f70x51lsz5502lqlbrr740i9qlm6fkza")))

