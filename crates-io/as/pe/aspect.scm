(define-module (crates-io as pe aspect) #:use-module (crates-io))

(define-public crate-aspect-0.1.0 (c (n "aspect") (v "0.1.0") (h "09sm9yqx9cl8b56qhwqndk9lc81ca22yqwxpmiz6daf2rxnzdfgs")))

(define-public crate-aspect-0.1.1 (c (n "aspect") (v "0.1.1") (d (list (d (n "aspect-weave") (r "^0.1.1") (d #t) (k 0)))) (h "12nq1nh5s8ghgphrgd3qpdyzffm1wz9rqzcfl5das8m37yi99g0z")))

(define-public crate-aspect-0.2.0 (c (n "aspect") (v "0.2.0") (d (list (d (n "aspect-weave") (r "^0.2.0") (d #t) (k 0)))) (h "0y6zwn8pr425f9mh2h1wnsfabvh09k9s1n0ascjlav6mrb6bnivw")))

(define-public crate-aspect-0.2.1 (c (n "aspect") (v "0.2.1") (d (list (d (n "aspect-weave") (r "^0.2.0") (d #t) (k 0)))) (h "1jn6c70jyqd4bq9dir9gqyh08ws5bjdj7zjhd9swfvgkpnlmagxs")))

(define-public crate-aspect-0.3.0 (c (n "aspect") (v "0.3.0") (d (list (d (n "aspect-weave") (r "^0.2.0") (d #t) (k 0)))) (h "0k92khzn7mzw4g8x3si538800z9cflcjv1xgm8wqa25sbd0pp4mk")))

