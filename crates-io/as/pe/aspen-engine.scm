(define-module (crates-io as pe aspen-engine) #:use-module (crates-io))

(define-public crate-aspen-engine-0.1.0 (c (n "aspen-engine") (v "0.1.0") (h "1l06qr56jkb1acwfd2r5gkkk5m7za53frgf95awalasp3pv3abzj")))

(define-public crate-aspen-engine-0.1.1 (c (n "aspen-engine") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "vulkano") (r "^0.34.1") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.34.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.9") (f (quote ("rwh_05"))) (d #t) (k 0)))) (h "1k7xc0qmzrlz3q2vlnjhjznsnzs4hal9cmy92rp9n94lm3sqv5pp")))

(define-public crate-aspen-engine-0.1.11 (c (n "aspen-engine") (v "0.1.11") (d (list (d (n "msgbox") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "vulkano") (r "^0.34.1") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.34.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.9") (f (quote ("rwh_05"))) (d #t) (k 0)))) (h "09qcbcsfw27arjx6f22kn32zm5fvqlby7q55c898l50jmh7l23rr")))

(define-public crate-aspen-engine-0.1.12 (c (n "aspen-engine") (v "0.1.12") (d (list (d (n "winit") (r "^0.29.15") (d #t) (k 0)))) (h "0xh36ghyx1madydd1k6x6l2dr3py6l2rs0vccd2jcyda4037skqs")))

