(define-module (crates-io as pe aspen) #:use-module (crates-io))

(define-public crate-aspen-0.2.2 (c (n "aspen") (v "0.2.2") (h "18r3xjk057r3pzl90rdxzd0nd5jml5mqr94ikkbnnnqi60a735pa") (f (quote (("default"))))))

(define-public crate-aspen-0.2.3 (c (n "aspen") (v "0.2.3") (d (list (d (n "lcm") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "lcm_gen") (r "^0.1.0") (d #t) (k 1)))) (h "0k3y0sigh1ha4kdcph81z4pqhmwa28zshi61jaxm521zm47hr96m") (f (quote (("default"))))))

(define-public crate-aspen-0.2.4 (c (n "aspen") (v "0.2.4") (d (list (d (n "lcm") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "lcm_gen") (r "^0.1.0") (d #t) (k 1)))) (h "1p0sa65b6534wlhpv5asapd4gxlri24pxzi4idhhw5150ybf8g2p") (f (quote (("default"))))))

(define-public crate-aspen-0.2.5 (c (n "aspen") (v "0.2.5") (d (list (d (n "lcm") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "lcm_gen") (r "^0.1.1") (d #t) (k 1)))) (h "1fsfpw0iw4igk1n7isqsnibs4lhisw4q1kpl5kzghdwzwfvdpccp") (f (quote (("default"))))))

(define-public crate-aspen-0.3.0 (c (n "aspen") (v "0.3.0") (d (list (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0xh0i57rvcryx1dj1yl376cb1xcdaxdc8r9riznv3pfrml5kcfgq")))

