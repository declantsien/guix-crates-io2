(define-module (crates-io as pe aspen-loader) #:use-module (crates-io))

(define-public crate-aspen-loader-0.1.0 (c (n "aspen-loader") (v "0.1.0") (h "0qvlx2xqnpw264jvdkqkz87g8hfw9vq49bikqvs6jqj8akxd0yw9")))

(define-public crate-aspen-loader-0.1.1 (c (n "aspen-loader") (v "0.1.1") (h "04ij9h2aiqb41rjj60yzl5s4djrx4v0xmisswsr2inb8r0wba059")))

