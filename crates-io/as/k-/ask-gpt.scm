(define-module (crates-io as k- ask-gpt) #:use-module (crates-io))

(define-public crate-ask-gpt-0.1.0 (c (n "ask-gpt") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12b625kh65c5dkhp5v1ydypa7mxnv6iy732bxhw73q33zgnlsm3k")))

