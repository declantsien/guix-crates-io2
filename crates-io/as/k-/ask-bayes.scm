(define-module (crates-io as k- ask-bayes) #:use-module (crates-io))

(define-public crate-ask-bayes-0.1.0 (c (n "ask-bayes") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.54") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0mjzdsl0r1mifn3jarrxwvjgkhacgpnkzx69vc09px6aqj95qvsp") (y #t)))

(define-public crate-ask-bayes-0.1.1 (c (n "ask-bayes") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.54") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "1scwjk6ixn61k7bp8r05z37d1d4sjzq2w75ps319b55nb8znsvf5")))

(define-public crate-ask-bayes-0.1.2 (c (n "ask-bayes") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.54") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "064n9j8vxdlbwyhp02vh84ka8hlnprindc9pijlls2j30vwsqkk4")))

(define-public crate-ask-bayes-0.1.3 (c (n "ask-bayes") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.54") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "01682x4hjr5v6k7c80dp484ngj6wyvnlsqbfvs085lg3pdzd472x")))

(define-public crate-ask-bayes-0.2.0 (c (n "ask-bayes") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.54") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.2") (f (quote ("paris"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "12w3n65jbp50xim3rldy2m5jxajpwsndcz63s545bjh573ihf2g4")))

(define-public crate-ask-bayes-0.2.1 (c (n "ask-bayes") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.54") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.2") (f (quote ("paris"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)))) (h "0gv63qrxfwa3ig0dm2z30zq2s07647kpqh4hmfp2rhpk9q1rhvah")))

