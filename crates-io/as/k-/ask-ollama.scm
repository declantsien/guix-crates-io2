(define-module (crates-io as k- ask-ollama) #:use-module (crates-io))

(define-public crate-ask-ollama-0.1.0 (c (n "ask-ollama") (v "0.1.0") (h "04a9xm5rfcdnnm2c0s8741wysankgkd9l8pvn9g46h74f1bh17ad")))

(define-public crate-ask-ollama-1.0.0 (c (n "ask-ollama") (v "1.0.0") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "1nw3fy8g3ad31cfqyk6bid7hq5wvvl14d2bn9x3z4134z4waz1j8")))

(define-public crate-ask-ollama-1.0.1 (c (n "ask-ollama") (v "1.0.1") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "0q4g0si41ppchvd6vrb823hs8pqcxxa49hq75f1v6g59ja1qwcca")))

(define-public crate-ask-ollama-1.0.2 (c (n "ask-ollama") (v "1.0.2") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "0iz59z0njsrgyzl9y3c64wcjdp8g5pf2383dx9bazp1ans0mkjhx")))

(define-public crate-ask-ollama-1.0.3 (c (n "ask-ollama") (v "1.0.3") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "1rwa85y3q6cpp7zp001jh05hwpahxzbp0va8g7p5168gfdkhzg2x")))

(define-public crate-ask-ollama-1.0.4 (c (n "ask-ollama") (v "1.0.4") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "0hh016k5zq9hmbl5q3ck36gvr4f5i2f9qldfg8g65kw5wk1v4b6x")))

(define-public crate-ask-ollama-1.0.5 (c (n "ask-ollama") (v "1.0.5") (d (list (d (n "spinners-rs") (r "^2.3.0") (d #t) (k 0)))) (h "0nbnjhnddc7v6jp3jlbvdw92czhdx8d3p4hmhnnawmncqp26ca8r")))

