(define-module (crates-io as k- ask-cli) #:use-module (crates-io))

(define-public crate-ask-cli-0.1.0 (c (n "ask-cli") (v "0.1.0") (d (list (d (n "trycmd") (r "^0.14.1") (d #t) (k 2)))) (h "0zf8jlx6l4cpcc30r9k4w4bqjzsl3hhir8iqr8kxz6jib85mfbvv")))

(define-public crate-ask-cli-0.1.1 (c (n "ask-cli") (v "0.1.1") (d (list (d (n "trycmd") (r "^0.14.1") (d #t) (k 2)))) (h "1g2gk6s3rzhabapqgavd3ggs14k2amscaxxxgm5gyzyk8myx7h39")))

(define-public crate-ask-cli-0.1.2 (c (n "ask-cli") (v "0.1.2") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "public-api") (r "^0.23.0") (d #t) (k 2)) (d (n "rustdoc-json") (r "^0.6.0") (d #t) (k 2)) (d (n "trycmd") (r "^0.14.4") (d #t) (k 2)))) (h "1jax2qam5dfjwk9msz59jyis8lx6yl0kgkhkj7f5azsmk49241f9")))

(define-public crate-ask-cli-0.1.3 (c (n "ask-cli") (v "0.1.3") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "public-api") (r "^0.24.0") (d #t) (k 2)) (d (n "rustdoc-json") (r "^0.7.0") (d #t) (k 2)) (d (n "trycmd") (r "^0.14.5") (d #t) (k 2)))) (h "131gqcjibh6qxra99jkki6czjdypjgi31c3v0a4c824q43lvlrws")))

(define-public crate-ask-cli-0.1.4 (c (n "ask-cli") (v "0.1.4") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "public-api") (r "^0.25.0") (d #t) (k 2)) (d (n "rustdoc-json") (r "^0.7.4") (d #t) (k 2)) (d (n "trycmd") (r "^0.14.6") (d #t) (k 2)))) (h "1bsprbj4alyvpn4cwvrl91hf3c0xrsk5ngdayllkp9sbiajg72h3")))

(define-public crate-ask-cli-0.1.5 (c (n "ask-cli") (v "0.1.5") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "public-api") (r "^0.25.0") (d #t) (k 2)) (d (n "rustdoc-json") (r "^0.7.4") (d #t) (k 2)) (d (n "trycmd") (r "^0.14.6") (d #t) (k 2)))) (h "085arvyfwpmi38bpnpkn1vn53x0jlhk4bql9j5n8hynh9v7qzhx1")))

(define-public crate-ask-cli-0.1.6 (c (n "ask-cli") (v "0.1.6") (d (list (d (n "supercilex-tests") (r "^0.3.3") (d #t) (k 2)) (d (n "trycmd") (r "^0.14.16") (d #t) (k 2)))) (h "15ajn5h5hv7b98wmpsj8zdniq9r3viw0vnk42s60m0v8wcxx5y7y")))

(define-public crate-ask-cli-0.2.0 (c (n "ask-cli") (v "0.2.0") (d (list (d (n "supercilex-tests") (r "^0.3.4") (d #t) (k 2)) (d (n "trycmd") (r "^0.14.17") (d #t) (k 2)))) (h "1wd89z4x1bl22ll3wdwwmd5a00rlz8dkp4i9zvsqdh45yf8vlg95")))

(define-public crate-ask-cli-0.2.1 (c (n "ask-cli") (v "0.2.1") (d (list (d (n "supercilex-tests") (r "^0.4.0") (d #t) (k 2)) (d (n "trycmd") (r "^0.14.19") (d #t) (k 2)))) (h "1s0kgd5zyyi35hk62039x4lrxqk8gszklhsws660q130szq1s3s4")))

(define-public crate-ask-cli-0.3.0 (c (n "ask-cli") (v "0.3.0") (d (list (d (n "supercilex-tests") (r "^0.4.4") (f (quote ("api"))) (k 2)) (d (n "trycmd") (r "^0.15.0") (d #t) (k 2)))) (h "0xb5swsm42ar7a149xw3mw6h8k7d0w8xcflwac7gpzf3a7b7c1sd")))

