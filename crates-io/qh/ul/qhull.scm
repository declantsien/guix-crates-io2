(define-module (crates-io qh ul qhull) #:use-module (crates-io))

(define-public crate-qhull-0.1.0 (c (n "qhull") (v "0.1.0") (d (list (d (n "qhull-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0c0cmv2p2agyrmh5q1brxs7nkpcnrcmng4d9pa77fbbqrfm4k87l")))

(define-public crate-qhull-0.1.1 (c (n "qhull") (v "0.1.1") (d (list (d (n "qhull-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0wch17vm5hn4lknl74yhah9m8lkizxpprlrnp8i20wm48f5np87q")))

(define-public crate-qhull-0.2.0 (c (n "qhull") (v "0.2.0") (d (list (d (n "qhull-sys") (r "^0.2") (d #t) (k 0)))) (h "1sg29mcznjpcs3jlivws0hd4kqscbwcgnmqdzpq19cdznncv4w7z")))

(define-public crate-qhull-0.3.0 (c (n "qhull") (v "0.3.0") (d (list (d (n "qhull-sys") (r "^0.3") (f (quote ("include-programs"))) (d #t) (k 0)))) (h "0qfmyiwca3pk4r8gh5b6cbkbi5kzssfrp7ql8cfrh1k7pcc5m4lv")))

(define-public crate-qhull-0.3.1 (c (n "qhull") (v "0.3.1") (d (list (d (n "qhull-sys") (r "^0.3") (f (quote ("include-programs"))) (d #t) (k 0)))) (h "0vxhlr1q4i787zrkjnn0j1ipl751r8i32mwfmgxrn9pmjm0y6r8m")))

