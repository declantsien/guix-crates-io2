(define-module (crates-io qh ul qhull-sys) #:use-module (crates-io))

(define-public crate-qhull-sys-0.1.0 (c (n "qhull-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "18a0160bl1zncd8jbav787ip1ci30bp50k887lmfl5qynbmrlhz6") (f (quote (("default") ("all-headers"))))))

(define-public crate-qhull-sys-0.1.1 (c (n "qhull-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1cnh1yzk8kayzb3y7mh7i9xg9pvxk8kk8jk4z2w3zkd861r2smnb") (f (quote (("default") ("all-headers"))))))

(define-public crate-qhull-sys-0.2.0 (c (n "qhull-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1dq7a7fykji03ic2x4hg0cbx03vs6gm5kxfxb31cy3wvycy4a7cd") (f (quote (("default") ("all-headers"))))))

(define-public crate-qhull-sys-0.3.0 (c (n "qhull-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0vks70b9cs1zpalqngcl9z58897yrx2zymm1hqir7xm479zcm3x4") (f (quote (("include-programs") ("default") ("all-headers"))))))

(define-public crate-qhull-sys-0.3.1 (c (n "qhull-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0cr19hpy2ffafbb39p4pqj6xy9d8n2mm86ccbdc09izscxz43qaf") (f (quote (("include-programs") ("default") ("all-headers"))))))

