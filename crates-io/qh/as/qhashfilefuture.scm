(define-module (crates-io qh as qhashfilefuture) #:use-module (crates-io))

(define-public crate-qhashfilefuture-0.9.0 (c (n "qhashfilefuture") (v "0.9.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.0") (d #t) (k 0)))) (h "1sj29jwpm52cqcm89j8apikbc68ja1xrl4gj6w5yhvib7plzayxk")))

(define-public crate-qhashfilefuture-0.9.1 (c (n "qhashfilefuture") (v "0.9.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.0") (d #t) (k 0)))) (h "000wiisv3hfpq2nyyj7447sikb120s6p7s08cszcj9nx5kz5nhwf")))

