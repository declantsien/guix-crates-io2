(define-module (crates-io a1 #{21}# a121-sys) #:use-module (crates-io))

(define-public crate-a121-sys-0.1.0 (c (n "a121-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "09bzc159zw398mvfvk31xhldgdd3wznqk24x7rp8b03434wscgrv") (f (quote (("presence") ("distance")))) (r "1.75")))

(define-public crate-a121-sys-0.1.1 (c (n "a121-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1s458fnpq5mlqarhdwjvvzz496vcvwm7kih3vr76c5gqpqbcyj6n") (f (quote (("presence") ("distance")))) (r "1.75")))

(define-public crate-a121-sys-0.1.2 (c (n "a121-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0w9afkidcqlfc0n9xxx9znifj3zwspvz5sfygxlqz3mvxx5apddq") (f (quote (("presence") ("distance")))) (r "1.75")))

(define-public crate-a121-sys-0.1.3 (c (n "a121-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "12zpivvah51mi8mcd1n8kh9873m770c3l8kl0m3yvkdvkbzlbbc3") (f (quote (("presence") ("distance")))) (r "1.75")))

(define-public crate-a121-sys-0.2.0 (c (n "a121-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "106wkg27n6s7b241lpkrzc4xhkd23yq34dcrk8k5nb5r56lj5vwl") (f (quote (("presence") ("distance")))) (r "1.75")))

