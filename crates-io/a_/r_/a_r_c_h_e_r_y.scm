(define-module (crates-io a_ r_ a_r_c_h_e_r_y) #:use-module (crates-io))

(define-public crate-a_r_c_h_e_r_y-0.4.1 (c (n "a_r_c_h_e_r_y") (v "0.4.1") (d (list (d (n "compiletest_rs") (r "^0.5") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "14w0xq1527qwq31qfqpp2cl42lsj9alr7dkh4qq26gpbjcriwwgc") (f (quote (("fatal-warnings")))) (y #t)))

(define-public crate-a_r_c_h_e_r_y-0.4.2 (c (n "a_r_c_h_e_r_y") (v "0.4.2") (d (list (d (n "compiletest_rs") (r "^0.5") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "04qxlzyq36814my0xfk48fw1km77p7nzbpx8lrkaw3gnaq89xf69") (f (quote (("fatal-warnings"))))))

(define-public crate-a_r_c_h_e_r_y-0.4.3 (c (n "a_r_c_h_e_r_y") (v "0.4.3") (d (list (d (n "compiletest_rs") (r "^0.5") (f (quote ("tmp"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0jfci6gbnxivynnzs436d65dasrh3mvi106jraqziy2qbm3l2k3h") (f (quote (("fatal-warnings"))))))

