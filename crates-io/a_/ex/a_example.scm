(define-module (crates-io a_ ex a_example) #:use-module (crates-io))

(define-public crate-a_example-0.1.0 (c (n "a_example") (v "0.1.0") (h "14sskhfhl4lkvig448aq0s8mhjf5zfcpx2w5sal0jlak7y9lnnb7")))

(define-public crate-a_example-0.1.1 (c (n "a_example") (v "0.1.1") (h "02w5q1r4gpwxb2x4mnvqa3za7r9whr08705c7scjhk2qybq4pg90") (y #t)))

(define-public crate-a_example-0.1.2 (c (n "a_example") (v "0.1.2") (h "08r66d25gps8nx6w7jpb9b2ahh48n81hdap7ixzjfcxxvszv4n6r")))

