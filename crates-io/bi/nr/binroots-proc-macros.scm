(define-module (crates-io bi nr binroots-proc-macros) #:use-module (crates-io))

(define-public crate-binroots-proc-macros-0.1.0 (c (n "binroots-proc-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0w9k5jqap1i08h0rqwas58pjd48sibg1f2rqcabn35463nq6qp02")))

(define-public crate-binroots-proc-macros-0.1.1 (c (n "binroots-proc-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0k3lgwz8g3qs417a1h8nfaa08hhr7g70cv7pqd6b1wypm4mrp9p7")))

(define-public crate-binroots-proc-macros-0.2.0 (c (n "binroots-proc-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "binroots") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 2)))) (h "0jraxgcnv9hsg0bmrzakh0n2kw5xdljrsqf8w6iyycbf0qxmpy5y")))

