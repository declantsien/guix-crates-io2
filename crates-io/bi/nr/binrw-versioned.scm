(define-module (crates-io bi nr binrw-versioned) #:use-module (crates-io))

(define-public crate-binrw-versioned-0.1.0 (c (n "binrw-versioned") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "semver") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full"))) (d #t) (k 0)))) (h "0srwcsmnjbx5pgdyyri6vq66qwn50090bj8vx1iq35qaaakjx7px") (y #t)))

