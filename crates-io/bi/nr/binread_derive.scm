(define-module (crates-io bi nr binread_derive) #:use-module (crates-io))

(define-public crate-binread_derive-1.0.0 (c (n "binread_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0h6r69a5cxrnragvw7r0qdrqcn1mcx6qcf20z2h74b3jhv4spna1") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-1.0.1 (c (n "binread_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qgl6143ny9n35gwp13x442756g5yh4k24lym1zl8y2yd1rna892") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-1.0.2 (c (n "binread_derive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "04y0khqp8xq8ngpmzvh5y8zqsgi654xh7zcl6xcbw54zr10gqc60") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-1.1.0 (c (n "binread_derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1khm5xnn75ynbnnhaaz1qxw1dbxpv0wrcap8s3lhv5iblsgvbxhx") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-1.1.1 (c (n "binread_derive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "11w4qq7xhzzs3v00kw3p1z3fq00hznbv5mkkiwa0yfa6f7z0piap") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-1.2.0 (c (n "binread_derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18fa4cbfzd8b05payl627ffrgdvzq45j5cj6pc90nmvp3sab3hky") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-1.3.0 (c (n "binread_derive") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "08rh2nw8sh22642ci6pmbnszc0ganprjjam8d5s177v6i3wxjyyh") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-1.4.0 (c (n "binread_derive") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1dl21fk6ziznamnz8mdhglgzwslnq0y355z6bqffi5fc07raigv1") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-1.4.2 (c (n "binread_derive") (v "1.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1s8r6nfq6z3vkakdplvghmkjky5i2qmqqmrwxrw7dci7d0absjpi") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-1.4.3 (c (n "binread_derive") (v "1.4.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "115jg6mgxfyajn58zfkdhs9srdas3kp2ck45d2p11zarqh5chgnn") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-2.0.0-beta.1 (c (n "binread_derive") (v "2.0.0-beta.1") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xl3yhhsh7jf0nyhnm317546rzyff84b6mxcn32v922wh59mzchk") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-2.0.0 (c (n "binread_derive") (v "2.0.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "runtime-macros-derive") (r "^0.4.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1lnlnf87y9immwwafdcc8i0vk6b63hkm0gdj8wbxdhmlisidjxf5") (f (quote (("debug_template"))))))

(define-public crate-binread_derive-2.1.0 (c (n "binread_derive") (v "2.1.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "runtime-macros-derive") (r "^0.4.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1v8zxycf337qfbsaq1vs3w5skclfikvd9x5i0kl4wwgiklh755hx") (f (quote (("debug_template"))))))

