(define-module (crates-io bi nr binreader-macros) #:use-module (crates-io))

(define-public crate-binreader-macros-0.1.0 (c (n "binreader-macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "16p1w27lh5shl9zgnkqd38wq2ha5bjawfkk9730aqdvpv2clb6wl")))

(define-public crate-binreader-macros-0.2.0 (c (n "binreader-macros") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0h12gvqb6hsh3v431panl5x9hnq7b5ph7bhirw1mkzkdkmcgdw4h")))

(define-public crate-binreader-macros-0.2.1 (c (n "binreader-macros") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0fcpwlhjpvm7dnfgc2xwbsl1fwv1801d6h1kw303ybv3w32csfxj")))

