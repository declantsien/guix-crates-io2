(define-module (crates-io bi nr binroots) #:use-module (crates-io))

(define-public crate-binroots-0.1.0 (c (n "binroots") (v "0.1.0") (d (list (d (n "binroots-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0xs5szrjp03c0hxnn9m7w7ba0xws9j605ba14akw1mswnmgjdajh")))

(define-public crate-binroots-0.1.1 (c (n "binroots") (v "0.1.1") (d (list (d (n "binroots-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "12nxvmwnxiwl2m7wgxn2jrzd0cwvp922dysvr52cz163383pbcn1")))

(define-public crate-binroots-0.1.2 (c (n "binroots") (v "0.1.2") (d (list (d (n "binroots-proc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0dkl0pljch54pg9h8dqhn8g5y0s2vhjjbmnymim9wkbcp0jc2763")))

(define-public crate-binroots-0.2.0 (c (n "binroots") (v "0.2.0") (d (list (d (n "binroots-proc-macros") (r "^0.1") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "02iyggd9id8qq5gdfpb0906dxfy8xibim0wkhwsjd9l5g94zjzz6")))

(define-public crate-binroots-0.2.1 (c (n "binroots") (v "0.2.1") (d (list (d (n "binroots-proc-macros") (r "^0.2") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1k3839s3w02nkb00azf9c2qma39y6dbagqaqgmq73h9s6v5qmzzd")))

(define-public crate-binroots-0.2.2 (c (n "binroots") (v "0.2.2") (d (list (d (n "binroots-proc-macros") (r "^0.2") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "1pf8vch4zq5kf2vgs501j6s97xl93av1jif66xs3r0x3hj0ki3zj")))

