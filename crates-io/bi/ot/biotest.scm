(define-module (crates-io bi ot biotest) #:use-module (crates-io))

(define-public crate-biotest-0.1.0 (c (n "biotest") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vsfd3k9kydyvj9ibfr4kl62xmr0lk357bwxrx588pn7vinjmka2") (f (quote (("vcf") ("fastq") ("fasta")))) (r "1.74")))

