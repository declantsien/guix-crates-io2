(define-module (crates-io bi bl bible-reference-rs) #:use-module (crates-io))

(define-public crate-bible-reference-rs-0.1.0 (c (n "bible-reference-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0l764306hkw9npmzr4ds7fzfb0a6cwibkq026zvls6ydnbw0cmmh")))

(define-public crate-bible-reference-rs-0.1.1 (c (n "bible-reference-rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nwmn5nfg2waay6a2641dls4i37lfcm99py2v9a9aa5c2g81ngza")))

(define-public crate-bible-reference-rs-0.1.2 (c (n "bible-reference-rs") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dih6amvjcg90cl258xcvhh7r7vigknsr9pglbz72njh2fcxms2h")))

(define-public crate-bible-reference-rs-0.1.3 (c (n "bible-reference-rs") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0358jbhmyy1ya21h7sl8az4zqh7jzpnz66vyhi88l6srfk49j574")))

