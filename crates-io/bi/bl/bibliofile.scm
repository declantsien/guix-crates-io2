(define-module (crates-io bi bl bibliofile) #:use-module (crates-io))

(define-public crate-bibliofile-0.1.0 (c (n "bibliofile") (v "0.1.0") (d (list (d (n "cursive") (r "^0.20.0") (f (quote ("termion-backend"))) (k 0)) (d (n "epub") (r "^2.0.0") (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)))) (h "1vsi0akww41h6fry0d0silz71c35b5xwqqllzcj7g8hfgca6dws0") (r "1.73")))

