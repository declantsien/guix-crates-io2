(define-module (crates-io bi bl bibliographix) #:use-module (crates-io))

(define-public crate-bibliographix-0.1.0 (c (n "bibliographix") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.4") (d #t) (k 0)))) (h "17ay87kzfvq4ragwpfqi1xild5v60318vlwwr4z8qdjv1m63100q")))

(define-public crate-bibliographix-0.2.0 (c (n "bibliographix") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.4") (d #t) (k 0)))) (h "1j4pxp9qjm6db6p0gx65hn2rjnq8zdd8apvd21yxx8s875wwhfxs")))

(define-public crate-bibliographix-0.3.0 (c (n "bibliographix") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.4") (d #t) (k 0)))) (h "0jxddyciig5h400ki7yvz87v1faam121ksp4i2lrzkpp190sk02d")))

(define-public crate-bibliographix-0.3.1 (c (n "bibliographix") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.4") (d #t) (k 0)))) (h "139yak3inyfx707cdgancm09wvxjqp211mxnfssvlzivcq9xmzip")))

(define-public crate-bibliographix-0.3.2 (c (n "bibliographix") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.4") (d #t) (k 0)))) (h "02j2phj2fs80vf89xhnb32cimy25vxz2rrd8jr491b0i6q373213")))

(define-public crate-bibliographix-0.4.0 (c (n "bibliographix") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0d8a6pp7kl9lig6c7llm1bhd214p926vjqv76jcwh14izalfg952")))

(define-public crate-bibliographix-0.5.0 (c (n "bibliographix") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1hc58l49c1md2dixs6lna9yn3blvhrzi8xmxwpij5f0hqm2la0rr")))

(define-public crate-bibliographix-0.6.0 (c (n "bibliographix") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-english") (r "^0.1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1lskm61l9wlmhlz2lyh3a49yrq1mk96ay4mq1cqgzh0l28mk9ydy")))

