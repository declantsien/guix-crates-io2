(define-module (crates-io bi bl bibliotheque) #:use-module (crates-io))

(define-public crate-bibliotheque-0.1.0 (c (n "bibliotheque") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)) (d (n "tui") (r "^0.8.0") (d #t) (k 0)))) (h "0nv22my6wvk8gil0abd5hpmwm1xqh6vxh7az9nb58fh2qydmjj6q")))

