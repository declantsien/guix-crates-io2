(define-module (crates-io bi bl bible_ref) #:use-module (crates-io))

(define-public crate-bible_ref-0.1.0 (c (n "bible_ref") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "07vib2098vs71bjch94zpmp4hz6ad55gndh8477p8az6vc7rdyfp")))

(define-public crate-bible_ref-0.1.1 (c (n "bible_ref") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "11vywzbsi7ng33gh04d1fwv69p9dcd6gwa00c1hmldqr2w1hhf1h")))

