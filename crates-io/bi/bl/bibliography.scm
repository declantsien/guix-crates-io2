(define-module (crates-io bi bl bibliography) #:use-module (crates-io))

(define-public crate-bibliography-0.1.0 (c (n "bibliography") (v "0.1.0") (d (list (d (n "document") (r "^0.1.0") (d #t) (k 0)))) (h "0z0jv08if8skrgrm6qjc4dc0i66s5q917rvy98frxnd1z9bgr6hp")))

(define-public crate-bibliography-0.2.0 (c (n "bibliography") (v "0.2.0") (d (list (d (n "document") (r "^0.5.1") (d #t) (k 0)))) (h "02wqb8hl5m8h45wddwsqbqynypqlb7fq7bh2jdy7lbwpghsall7c")))

(define-public crate-bibliography-0.2.1 (c (n "bibliography") (v "0.2.1") (d (list (d (n "document") (r "^0.5.1") (d #t) (k 0)))) (h "0cvx4ybdkzvxhpbhk53bkhnkqrbdwjmp8dy3pxqlgs6886lk5j0r")))

(define-public crate-bibliography-0.2.2 (c (n "bibliography") (v "0.2.2") (d (list (d (n "document") (r "^0.5.1") (d #t) (k 0)))) (h "1jrhmr342nymkixr9r6xvsfnif544bclmy874a222sz550sf4qk6")))

