(define-module (crates-io bi bl bibliothek) #:use-module (crates-io))

(define-public crate-bibliothek-0.1.0 (c (n "bibliothek") (v "0.1.0") (d (list (d (n "biblatex") (r "^0.8.0") (d #t) (k 0)) (d (n "hayagriva") (r "^0.3.0") (d #t) (k 0)) (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "15gx1vmiak7kia135f8ir86s1650l21d8lplzm4zsn8kz8y33mw5")))

(define-public crate-bibliothek-0.2.0 (c (n "bibliothek") (v "0.2.0") (d (list (d (n "biblatex") (r "^0.8.0") (d #t) (k 0)) (d (n "hayagriva") (r "^0.3.0") (d #t) (k 0)) (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.46") (d #t) (k 0)))) (h "02w2hxrj5zwiap99n0wyriiddmn6m8by2nx98zm4iyd2m8fivyag")))

