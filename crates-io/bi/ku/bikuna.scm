(define-module (crates-io bi ku bikuna) #:use-module (crates-io))

(define-public crate-bikuna-0.1.0 (c (n "bikuna") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1dp7h3j1cm72jfzs3f5pag32m0g5vbrff7d2py07d6zs6sivji03")))

(define-public crate-bikuna-0.1.1 (c (n "bikuna") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0c14dgir7a1ri5jiiibdr215ph5hswgi157hkpy3vzy71l764gh9")))

