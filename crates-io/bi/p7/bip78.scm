(define-module (crates-io bi p7 bip78) #:use-module (crates-io))

(define-public crate-bip78-0.1.0-preview (c (n "bip78") (v "0.1.0-preview") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.26.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "1n856zjy35h48lggg7s8qhczmvq5bx6vp8hda1anbb7ghcgal6fg") (f (quote (("sender") ("receiver" "rand"))))))

