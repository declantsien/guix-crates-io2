(define-module (crates-io bi nw binwrite_derive) #:use-module (crates-io))

(define-public crate-binwrite_derive-0.1.0 (c (n "binwrite_derive") (v "0.1.0") (d (list (d (n "binwrite") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wrz4cz3x9pps21rycpgpwyfzrsxczdbh0gyllr21igxcbgm4yjf")))

(define-public crate-binwrite_derive-0.1.1 (c (n "binwrite_derive") (v "0.1.1") (d (list (d (n "binwrite") (r ">= 0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1rc125msrnm5f64gfv78n48jiknjqsdxfdpvj5a1xxzcy8xl70ap")))

(define-public crate-binwrite_derive-0.1.2 (c (n "binwrite_derive") (v "0.1.2") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1dcqdmzazsxv9x0nyizk6cla3xmcsihfvkk93l5cahxp54ry0n1s")))

(define-public crate-binwrite_derive-0.2.0 (c (n "binwrite_derive") (v "0.2.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "100gizfipqdi543ksy2fii39gbkbzxq3b6ig9hdsdi6wr4831y6l")))

(define-public crate-binwrite_derive-0.2.1 (c (n "binwrite_derive") (v "0.2.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lz3m8izfdfzv28yqi871qffzslnzgl9q8kwk6x2chp0kk0ifvs7")))

