(define-module (crates-io bi nw binwrite) #:use-module (crates-io))

(define-public crate-binwrite-0.1.0 (c (n "binwrite") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "0ggnf4yz73kkv4fakz4kza0jsaqwdm8xf89f5v0cv36lqkq7m5rf")))

(define-public crate-binwrite-0.1.1 (c (n "binwrite") (v "0.1.1") (d (list (d (n "binwrite_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "0qc8cc1n9fzjy2p34aggqi4f0618gm6cks3xz1fkba43xdda6h1h")))

(define-public crate-binwrite-0.1.2 (c (n "binwrite") (v "0.1.2") (d (list (d (n "binwrite_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "1xs03shxfqa2i54bcymh2crjgxin32rcwznlwzfjw970a5xkxc0i")))

(define-public crate-binwrite-0.1.3 (c (n "binwrite") (v "0.1.3") (d (list (d (n "binwrite_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "1nbzl06q2w7y943lwhzijywf3szvsjijr4vf7qn5svi6fj86ackj")))

(define-public crate-binwrite-0.1.4 (c (n "binwrite") (v "0.1.4") (d (list (d (n "binwrite_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "1sja9c9zl8g16inawmicdiqb8qvhqsndbkr366kh86ximv7yzgy8")))

(define-public crate-binwrite-0.1.5 (c (n "binwrite") (v "0.1.5") (d (list (d (n "binwrite_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "01p86x7nv2yl2l93kz9q1sg9p4wa08fb4gq4rhrniqswl9sjgyir")))

(define-public crate-binwrite-0.2.0 (c (n "binwrite") (v "0.2.0") (d (list (d (n "binwrite_derive") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "0hwszkkkgygc84j80gms0x14ny8cjd3cw0l8kfqlqv517q4d1nrj")))

(define-public crate-binwrite-0.2.1 (c (n "binwrite") (v "0.2.1") (d (list (d (n "binwrite_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 0)))) (h "1j3llkblbzirbkcqvvrvfpc7qalgy2j6pjr77rjav17kn4fxmhs2")))

