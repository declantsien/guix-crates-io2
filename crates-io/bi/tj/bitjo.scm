(define-module (crates-io bi tj bitjo) #:use-module (crates-io))

(define-public crate-bitjo-0.1.0 (c (n "bitjo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1njr4c11ixmi9s1kxz84vb5n8dfcvxl0sxl3cz650ql0sgz22p5j")))

(define-public crate-bitjo-0.1.1 (c (n "bitjo") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "02zcspllnli36fnl98s6s7frfi5mjdwv57k2pb8x3srpajiz5w27")))

