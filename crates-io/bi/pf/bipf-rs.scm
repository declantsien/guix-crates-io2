(define-module (crates-io bi pf bipf-rs) #:use-module (crates-io))

(define-public crate-bipf-rs-0.1.0 (c (n "bipf-rs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0pzgbpl0wwx8bxsawxl3qw6v4vkrwfkcplwmjf3w16kbcyb1ac2n")))

(define-public crate-bipf-rs-0.1.1 (c (n "bipf-rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0f1a7lskjaqb1ag4gfjs0sjlc0xp1chp7hg7mi32xs8ib29c9yv1")))

