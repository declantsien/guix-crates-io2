(define-module (crates-io bi nc bincode-typescript) #:use-module (crates-io))

(define-public crate-bincode-typescript-0.1.0 (c (n "bincode-typescript") (v "0.1.0") (d (list (d (n "askama") (r "^0.8") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "12xk8gq3yacbgbkakgc2h06hgvr0cwfm4wkv6bl7parimjg4vj6p")))

