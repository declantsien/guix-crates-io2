(define-module (crates-io bi nc bincode2) #:use-module (crates-io))

(define-public crate-bincode2-2.0.0 (c (n "bincode2") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 2)))) (h "0c6xd25qncsq87vm44kxqwn2ai4hh54d3bi34n6f1gf8wxvp8gjy")))

(define-public crate-bincode2-2.0.1 (c (n "bincode2") (v "2.0.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 2)))) (h "13rif03phkkzvvks6f49f9a7sv4ng1kfxbdvxdq1224f0f1n37zl")))

