(define-module (crates-io bi nc bincode_aes) #:use-module (crates-io))

(define-public crate-bincode_aes-1.0.0 (c (n "bincode_aes") (v "1.0.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)))) (h "1jsn64844zq77a8ryaab906y1nmdb5a2xpws9pdvgn816a13y940")))

(define-public crate-bincode_aes-1.0.1 (c (n "bincode_aes") (v "1.0.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)))) (h "1d2hjg2ckim02rav794f8xaxspskz34314xfjsj8hmd6xsw21b8i")))

