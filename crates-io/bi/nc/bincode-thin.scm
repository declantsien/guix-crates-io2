(define-module (crates-io bi nc bincode-thin) #:use-module (crates-io))

(define-public crate-bincode-thin-1.1.2 (c (n "bincode-thin") (v "1.1.2") (d (list (d (n "autocfg") (r "^0.1") (d #t) (k 1)) (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.3") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "0mdql0jnjvwnsfparb337wd691ywgajz4ml3ckjyrqkkbcy3dxc3") (f (quote (("i128"))))))

