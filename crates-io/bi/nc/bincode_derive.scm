(define-module (crates-io bi nc bincode_derive) #:use-module (crates-io))

(define-public crate-bincode_derive-2.0.0-alpha.0 (c (n "bincode_derive") (v "2.0.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1xdl0gii81qinalab5djrdwjm0xidn4wflq8a2ri89pqqjpwbpma")))

(define-public crate-bincode_derive-2.0.0-alpha.1 (c (n "bincode_derive") (v "2.0.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "17wcx0frx5bgj66jdznajr8h7h1v6bgclzdq3bvl4jxlgcclvnzv")))

(define-public crate-bincode_derive-2.0.0-alpha.2 (c (n "bincode_derive") (v "2.0.0-alpha.2") (d (list (d (n "virtue") (r "^0.0.3") (d #t) (k 0)))) (h "09ahbxgkiqqz7makq72vkh03vqb3f6z7yqqjd4xwvq5sizfxq16x")))

(define-public crate-bincode_derive-2.0.0-beta.0 (c (n "bincode_derive") (v "2.0.0-beta.0") (d (list (d (n "virtue") (r "^0.0.4") (d #t) (k 0)))) (h "06lldaa5jv7jnq76v18khbn2d8wkg3fky287ziry7lggjmz8q4nh")))

(define-public crate-bincode_derive-2.0.0-beta.1 (c (n "bincode_derive") (v "2.0.0-beta.1") (d (list (d (n "virtue") (r "^0.0.4") (d #t) (k 0)))) (h "0axkn3f49ix67gld874ar3b202czcpdsw038l6407sg27fq68kkc")))

(define-public crate-bincode_derive-2.0.0-beta.2 (c (n "bincode_derive") (v "2.0.0-beta.2") (d (list (d (n "virtue") (r "^0.0.4") (d #t) (k 0)))) (h "0ycja4q52l1y6ys9kcjwya3c9rsk49s14dhx9h5i6km6b9c75ppb")))

(define-public crate-bincode_derive-2.0.0-beta.3 (c (n "bincode_derive") (v "2.0.0-beta.3") (d (list (d (n "virtue") (r "^0.0.7") (d #t) (k 0)))) (h "0j9y0rkqsaa7279c7b7s413irna75b9lc1r5mhcgwyv2faj908yb")))

(define-public crate-bincode_derive-2.0.0-rc.1 (c (n "bincode_derive") (v "2.0.0-rc.1") (d (list (d (n "virtue") (r "^0.0.7") (d #t) (k 0)))) (h "1hsxldyqjp7sh71dzaky464npgffp2wyk1qvmv3v83g0yfl8fcli")))

(define-public crate-bincode_derive-2.0.0-rc.2 (c (n "bincode_derive") (v "2.0.0-rc.2") (d (list (d (n "virtue") (r "^0.0.8") (d #t) (k 0)))) (h "1lhrsx1rsr7h4y4hdb0cccmm4hrw7dx3mry8bp92sva4i4rs4i8a")))

(define-public crate-bincode_derive-2.0.0-rc.3 (c (n "bincode_derive") (v "2.0.0-rc.3") (d (list (d (n "virtue") (r "^0.0.13") (d #t) (k 0)))) (h "0k1ygyzqpw3h5pc70cf8w4l5rv9xbk423am3lw1bi8cr7fdpac3y")))

