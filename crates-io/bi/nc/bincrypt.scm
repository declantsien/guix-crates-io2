(define-module (crates-io bi nc bincrypt) #:use-module (crates-io))

(define-public crate-bincrypt-0.3.0 (c (n "bincrypt") (v "0.3.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rust_sodium") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(windows)") (k 1)))) (h "067q0kcwl4snxvkmsvlw24rps61wwc1w79w8czv2sfcyg9id6gp3")))

(define-public crate-bincrypt-0.3.1 (c (n "bincrypt") (v "0.3.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rust_sodium") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(windows)") (k 1)))) (h "0595j5ir50rgzmagyvvpbdb8823rys4gxswzc5y5fvc14hv3510b")))

