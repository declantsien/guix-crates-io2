(define-module (crates-io bi nc bincode) #:use-module (crates-io))

(define-public crate-bincode-0.0.1 (c (n "bincode") (v "0.0.1") (h "0r8ax008fdcybmhkb4d21q4rypjx3h4aqrj7p3pgw9qg85i92vb8")))

(define-public crate-bincode-0.0.2 (c (n "bincode") (v "0.0.2") (h "0gx5cg5bn03pn8liklrb5ifbdb82nn6pmdr64zr29qs1v2byv5xv")))

(define-public crate-bincode-0.0.3 (c (n "bincode") (v "0.0.3") (d (list (d (n "rustc-serialize") (r "^0.1.4") (d #t) (k 0)))) (h "1li14dmwrg5rvgd58gynp26ikjh0j97wvhwi13zg54s1a6vcka1p")))

(define-public crate-bincode-0.0.4 (c (n "bincode") (v "0.0.4") (d (list (d (n "rustc-serialize") (r "^0.2.2") (d #t) (k 0)))) (h "1z3icf64k6vdbz8snjs0fs8qxmy2q700qaxp47hdy3bbfix7hkp1")))

(define-public crate-bincode-0.0.5 (c (n "bincode") (v "0.0.5") (d (list (d (n "rustc-serialize") (r "^0.2.6") (d #t) (k 0)))) (h "05mp4yiz0yj44wwn5mhpnqnn7f156jiap7qbrqva2l57ypq9xyd6")))

(define-public crate-bincode-0.0.6 (c (n "bincode") (v "0.0.6") (d (list (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)))) (h "07gbjfcwclqbp39f9g16i19k399vygdngzjsyjri5dp80h8ffdg4")))

(define-public crate-bincode-0.0.7 (c (n "bincode") (v "0.0.7") (d (list (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)))) (h "049ynvmh4144skp3m6l5ag8yzzcx5nmnjh38dvjnkvw3sgfvw0p2")))

(define-public crate-bincode-0.0.8 (c (n "bincode") (v "0.0.8") (d (list (d (n "rustc-serialize") (r "^0.2.10") (d #t) (k 0)))) (h "1r75nv4y0659mbv3gc2918ndwfixw2da7pwb9mmb04s07xk0ibd0")))

(define-public crate-bincode-0.0.9 (c (n "bincode") (v "0.0.9") (d (list (d (n "rustc-serialize") (r "^0.2.10") (d #t) (k 0)))) (h "043n9n9jqpfhf40bfqi718rfrkl5sn7jdrhk7jyx4iy7xc6qiry7")))

(define-public crate-bincode-0.0.10 (c (n "bincode") (v "0.0.10") (d (list (d (n "byteorder") (r "^0.2.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "15lgy4xia61j5mlyassa421lgaq777vjm1whc3706j36pk7dwq2r")))

(define-public crate-bincode-0.0.11 (c (n "bincode") (v "0.0.11") (d (list (d (n "byteorder") (r "^0.2.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1r1whc37464a98h971mlabygfv9vqs24kga0glvwjywq7pdqb8zx")))

(define-public crate-bincode-0.0.12 (c (n "bincode") (v "0.0.12") (d (list (d (n "byteorder") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1jwp4iffw7d0zxdxyhd1mjfa53wm8lvwcpnmxzy8rk8l89lna7yq")))

(define-public crate-bincode-0.0.13 (c (n "bincode") (v "0.0.13") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1g6r5rva80k5x2a4azwmz3m7i7gq5nkawz321lv1k0ciaq71yvla")))

(define-public crate-bincode-0.0.14 (c (n "bincode") (v "0.0.14") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0lq8d6zclh73w7z54w6m6skvdska320wxf895hkmhdaij9lvbfvn")))

(define-public crate-bincode-0.1.0 (c (n "bincode") (v "0.1.0") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0kas29d6imfv089qkp4af2z20swf7ywmgw6ngnws2z6zq4zy0y4v")))

(define-public crate-bincode-0.1.1 (c (n "bincode") (v "0.1.1") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0igx75w6gxq0hqfg6p6by9a6c9crq52pv0p44p87wfskx8hp3ynq")))

(define-public crate-bincode-0.1.2 (c (n "bincode") (v "0.1.2") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1jj6pjdy7ccllgcih2vv0976d1crqyvfqa75qj5kwh9hlfirk4lb")))

(define-public crate-bincode-0.1.3 (c (n "bincode") (v "0.1.3") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1065arrcvmp6s4swhmfrz33my1nk2lan9h6crb3f204fp2sir9v7")))

(define-public crate-bincode-0.2.0 (c (n "bincode") (v "0.2.0") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "13a8ad1r675lr9bai5ryx8s42vcmqfyxw942jj4v2qb7c9nw83sl")))

(define-public crate-bincode-0.2.1 (c (n "bincode") (v "0.2.1") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0vjna0962wvqsl9713ky656xhii26bnigvnns302wydj8j7924wm")))

(define-public crate-bincode-0.2.2 (c (n "bincode") (v "0.2.2") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "11nxkl29sxvsyvxvnfcfzvy7n4dshgpcxxr6p748iwmg1x74g6r3")))

(define-public crate-bincode-0.3.0 (c (n "bincode") (v "0.3.0") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "1ddardbav3xy2ym9fqg62fkhivrx3imxvz3slqdkws5hd349h54m")))

(define-public crate-bincode-0.4.0 (c (n "bincode") (v "0.4.0") (d (list (d (n "byteorder") (r "0.3.*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 2)))) (h "0xh5qwm0yzn1r4mm4qxwy4xs6z7yqa7mcgy44jx8a92q7h66iwhw")))

(define-public crate-bincode-0.4.1 (c (n "bincode") (v "0.4.1") (d (list (d (n "byteorder") (r "0.4.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "serde") (r "0.6.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.6.*") (d #t) (k 2)))) (h "0zh5yasnxk3pw07lxskd6dr9x1lkhcmjl6gx9slyiwplr9l9vy30")))

(define-public crate-bincode-0.5.0 (c (n "bincode") (v "0.5.0") (d (list (d (n "byteorder") (r "0.4.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "serde") (r "0.7.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 2)))) (h "1nqf7cik8cpqhknkb47jq3wda45v8z7qjq5632blbfsnzg7xhyj5")))

(define-public crate-bincode-0.5.1 (c (n "bincode") (v "0.5.1") (d (list (d (n "byteorder") (r "0.4.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "serde") (r "0.7.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 2)))) (h "1bc4cp868bwsb00i3hfvvwx7h2blgl4fd4gixrzy5fjv2qx763ir")))

(define-public crate-bincode-0.5.2 (c (n "bincode") (v "0.5.2") (d (list (d (n "byteorder") (r "0.4.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "serde") (r "0.7.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 2)))) (h "1vxb6x8xvz86l3356zxnk7hpali7wmavbpfvlbrx63pm2ixffllj")))

(define-public crate-bincode-0.5.3 (c (n "bincode") (v "0.5.3") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "serde") (r "0.7.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 2)))) (h "02jgwcmzn6xisdv2acrziv8vyk45wd897ydzmkhbxii3falwb54p")))

(define-public crate-bincode-0.5.4 (c (n "bincode") (v "0.5.4") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "0.7.*") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 2)))) (h "1nfibv011kb8j9bypjy2a5lrvnjsy2amsp44kqlk0b3d27y0ng8f") (f (quote (("default" "rustc-serialize" "serde"))))))

(define-public crate-bincode-0.5.6 (c (n "bincode") (v "0.5.6") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "0.7.*") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 2)))) (h "0xqmw3nganr5l2wh4c8pqb9aw7j5qwhw4pr991dsf0sc51pikgsy") (f (quote (("default" "rustc-serialize" "serde"))))))

(define-public crate-bincode-0.5.7 (c (n "bincode") (v "0.5.7") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "0.7.*") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 2)))) (h "0z13pas4asg19rvxxc8kkfvm1g458ffdjwgj8sf53xc7k1y0fbaj") (f (quote (("default" "rustc-serialize" "serde"))))))

(define-public crate-bincode-0.5.8 (c (n "bincode") (v "0.5.8") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "0.7.*") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 2)))) (h "0v3s892i0kg721028xcm5yh3f867d5mkcf3637hwxy8lm9l5r8i7") (f (quote (("default" "rustc-serialize" "serde"))))))

(define-public crate-bincode-0.5.9 (c (n "bincode") (v "0.5.9") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "0.7.*") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "0.7.*") (d #t) (k 2)))) (h "0igp3l10bjvhb3hn3wcb6fcr1zrcscplc4wcjgqm8zfgh02klhda") (f (quote (("default" "rustc-serialize" "serde"))))))

(define-public crate-bincode-0.6.0 (c (n "bincode") (v "0.6.0") (d (list (d (n "byteorder") (r "0.5.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "0.8.*") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "0.8.*") (d #t) (k 2)))) (h "1g21p8qxp89bhmn7jva6dpkwq2x78ag6sjhx8fjp8gixyx0sdfwz") (f (quote (("default" "rustc-serialize" "serde"))))))

(define-public crate-bincode-0.6.1 (c (n "bincode") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "0.8.*") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "0.8.*") (d #t) (k 2)))) (h "1ba8bni0ak65918z0vjcicdf2502lzn7a3w6fw67nlh8s5zhpssm") (f (quote (("default" "rustc-serialize" "serde"))))))

(define-public crate-bincode-1.0.0-alpha1 (c (n "bincode") (v "1.0.0-alpha1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 2)))) (h "0kwnml0fyx2b8m63gw28ds9xlawh8qjyyp4ibch68irljfxzqh5r") (y #t)))

(define-public crate-bincode-1.0.0-alpha2 (c (n "bincode") (v "1.0.0-alpha2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 2)))) (h "0ri9sj8mgfz1c4k3401qyl78n6vrbljyzx7b1icg1a0vcnshnrb2") (y #t)))

(define-public crate-bincode-1.0.0-alpha3 (c (n "bincode") (v "1.0.0-alpha3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 2)))) (h "1lvlkvq0lk1516p9jc9h64qm95bx198gydqy947nj1558rw7is15") (y #t)))

(define-public crate-bincode-1.0.0-alpha4 (c (n "bincode") (v "1.0.0-alpha4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 2)))) (h "1cnqvvsy9yvbp6yc37x8r93mpcgz4aj6z1r6v04fd6jfxgik326v") (y #t)))

(define-public crate-bincode-1.0.0-alpha5 (c (n "bincode") (v "1.0.0-alpha5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 2)))) (h "0n05sf0a9s16bjgpbng2zg19vvv7ind2klzk36qybla2rrs7h6pi") (y #t)))

(define-public crate-bincode-1.0.0-alpha6 (c (n "bincode") (v "1.0.0-alpha6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 2)))) (h "0pgaby0f11705xasd9jvm0gcyjjf08y8brbs93dpymjx3jndw37v") (y #t)))

(define-public crate-bincode-1.0.0-alpha7 (c (n "bincode") (v "1.0.0-alpha7") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 2)))) (h "09zicml3b3rrhp8angzymxp67y5mkfj2s5mwajjjpwlhv54f8n8b") (y #t)))

(define-public crate-bincode-0.7.0 (c (n "bincode") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "0.9.*") (d #t) (k 0)) (d (n "serde_derive") (r "0.9.*") (d #t) (k 2)))) (h "0gplfx5i5cmf3zcr44qfh6l2c0qswqkl571h511q0mpdxq9yd9my")))

(define-public crate-bincode-0.8.0 (c (n "bincode") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_bytes") (r "0.10.*") (d #t) (k 2)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 2)))) (h "0vn3z9k5h7r4hfc1pialp2lmd0savh9p12s5j1lrr2mjk6rch0z1")))

(define-public crate-bincode-0.8.1 (c (n "bincode") (v "0.8.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_bytes") (r "0.10.*") (d #t) (k 2)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 2)))) (h "0nbj0lwykwa1a7sa4303rxgpng9p2hcz9s5d5qcrckrpmcxjsjkf") (y #t)))

(define-public crate-bincode-0.9.0 (c (n "bincode") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_bytes") (r "0.10.*") (d #t) (k 2)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 2)))) (h "0v5699s6hz7c62yfz5gczygq2ln2drzjsg3ypn0f8j20gvaia9mr")))

(define-public crate-bincode-0.9.1 (c (n "bincode") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_bytes") (r "0.10.*") (d #t) (k 2)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 2)))) (h "1z9a4224kvdimsr23l7waab4gcq14nx4wy980f1j5633mxlv6gwx")))

(define-public crate-bincode-0.9.2 (c (n "bincode") (v "0.9.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "1.*.*") (d #t) (k 0)) (d (n "serde_bytes") (r "0.10.*") (d #t) (k 2)) (d (n "serde_derive") (r "1.*.*") (d #t) (k 2)))) (h "1bzi5w0daikmxj9jn913yg6h2w8l93imlny13dan7ys91gdh2qws")))

(define-public crate-bincode-1.0.0 (c (n "bincode") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.3") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "1l4lxmqif0npg157mgifa3gr5lr0wb6bwixqhjxmq19kvy1k38dx")))

(define-public crate-bincode-1.0.1 (c (n "bincode") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.3") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "1xwgpp3jbb6pavv0d565h0ab0k36qxfkslvi430npzbjkvibjbwz") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-bincode-1.1.1 (c (n "bincode") (v "1.1.1") (d (list (d (n "autocfg") (r "^0.1") (d #t) (k 1)) (d (n "byteorder") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.3") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "1zxjmcw9x2ywknys1p39mwkaxsvppnw1fpyzn24hw2qg8vb0lisq") (f (quote (("i128"))))))

(define-public crate-bincode-1.1.2 (c (n "bincode") (v "1.1.2") (d (list (d (n "autocfg") (r "^0.1") (d #t) (k 1)) (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.3") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "140i22mw83npn4sx83ldhlig86x1lqwgaacwah08dvdfir60pziy") (f (quote (("i128"))))))

(define-public crate-bincode-1.1.3 (c (n "bincode") (v "1.1.3") (d (list (d (n "autocfg") (r "^0.1") (d #t) (k 1)) (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "0dgzpvs4hc9msd9gihmklx02sbg0mnf5mw4mxgz2yhddq5a8x74m") (f (quote (("i128"))))))

(define-public crate-bincode-1.1.4 (c (n "bincode") (v "1.1.4") (d (list (d (n "autocfg") (r "^0.1.2") (d #t) (k 1)) (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "1xx6bp39irvsndk6prnmmq8m1l9p6q2qj21j6mfks2y81pjsa14z") (f (quote (("i128"))))))

(define-public crate-bincode-1.2.0 (c (n "bincode") (v "1.2.0") (d (list (d (n "autocfg") (r "^0.1.2") (d #t) (k 1)) (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "14jzj61c145s9jwr1i213b7mdcmv1ny4z1lns9s8gvp34j9n7axq") (f (quote (("i128"))))))

(define-public crate-bincode-1.2.1 (c (n "bincode") (v "1.2.1") (d (list (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "1gvxm3n67xv1874fwxmnlircdlphlk1hcw75ykrrnw9l2nky4lsp") (f (quote (("i128"))))))

(define-public crate-bincode-1.3.0 (c (n "bincode") (v "1.3.0") (d (list (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "10a3ccbsah6365wxk85rnfalf7iwpgww37j84db2k7h83wmg15hm") (f (quote (("i128")))) (y #t)))

(define-public crate-bincode-1.3.1 (c (n "bincode") (v "1.3.1") (d (list (d (n "byteorder") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "0vc9pjh6hfp9vfq752sa88rxwg93ydhm0dvvy58rcvx2p8wkl3gk") (f (quote (("i128"))))))

(define-public crate-bincode-1.3.2 (c (n "bincode") (v "1.3.2") (d (list (d (n "byteorder") (r ">=1.3.0, <1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "0wip96004b3lnbyklkhqa5fxsfzs0g1c7dydqg20b6b1kskdyxfi") (f (quote (("i128"))))))

(define-public crate-bincode-1.3.3 (c (n "bincode") (v "1.3.3") (d (list (d (n "serde") (r "^1.0.63") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)))) (h "1bfw3mnwzx5g1465kiqllp5n4r10qrqy88kdlp3jfwnq2ya5xx5i") (f (quote (("i128"))))))

(define-public crate-bincode-2.0.0-alpha.0 (c (n "bincode") (v "2.0.0-alpha.0") (d (list (d (n "bincode_derive") (r "^2.0.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1yrj5xh6kbglz191ma1qcaajjllc2sv4vs9sjdv88fvcqpl75xcb") (f (quote (("std" "alloc") ("derive" "bincode_derive") ("default" "std" "derive" "atomic") ("atomic") ("alloc"))))))

(define-public crate-bincode-2.0.0-alpha.1 (c (n "bincode") (v "2.0.0-alpha.1") (d (list (d (n "bincode_derive") (r "^2.0.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 2)) (d (n "serde_incl") (r "^1.0.130") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0z2grfj6xvzq4gx7dznqgn2lkm0srwykl8r4b9nbrr6ji8zllppi") (f (quote (("std" "alloc") ("serde" "std" "serde_incl" "serde_incl/std") ("derive" "bincode_derive") ("default" "std" "derive" "atomic") ("atomic") ("alloc"))))))

(define-public crate-bincode-2.0.0-alpha.2 (c (n "bincode") (v "2.0.0-alpha.2") (d (list (d (n "bincode_derive") (r "^2.0.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 2)) (d (n "serde_incl") (r "^1.0.130") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "11v3klvccnrwr4zzgj5sxqbny85vis3blsbin5p72klpqsmw7l9n") (f (quote (("std" "alloc") ("serde" "std" "serde_incl" "serde_incl/std") ("derive" "bincode_derive") ("default" "std" "derive" "atomic") ("atomic") ("alloc"))))))

(define-public crate-bincode-2.0.0-beta.0 (c (n "bincode") (v "2.0.0-beta.0") (d (list (d (n "bincode_derive") (r "^2.0.0-beta.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 2)) (d (n "serde_incl") (r "^1.0.130") (o #t) (d #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "02cj3n51ihh75w8dw73n1ajwbn2jcc4z2p38yw6zan7ddhvmjwl1") (f (quote (("std" "alloc") ("serde" "std" "serde_incl" "serde_incl/std") ("derive" "bincode_derive") ("default" "std" "derive" "atomic") ("atomic") ("alloc"))))))

(define-public crate-bincode-2.0.0-beta.1 (c (n "bincode") (v "2.0.0-beta.1") (d (list (d (n "bincode_derive") (r "^2.0.0-beta.1") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_incl") (r "^1.0") (o #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0") (k 2)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde"))) (d #t) (k 2)))) (h "0ykmswfg8xx8djck65jp6mkg2yy94ici2vfnjnsk5wszyhhnipfw") (f (quote (("std" "alloc") ("serde_no_std" "serde_incl") ("serde_alloc" "serde_incl/alloc" "alloc") ("serde" "serde_incl/std" "std" "serde_alloc") ("derive" "bincode_derive") ("default" "std" "derive" "atomic") ("atomic") ("alloc"))))))

(define-public crate-bincode-2.0.0-beta.2 (c (n "bincode") (v "2.0.0-beta.2") (d (list (d (n "bincode_derive") (r "^2.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_incl") (r "^1.0") (o #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0") (k 2)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde"))) (d #t) (k 2)))) (h "0zbhl6g0sqvka8rddynxkaxrv1b6232ja4zjsg0w7g3ijzm9xc53") (f (quote (("std" "alloc") ("serde_no_std" "serde_incl") ("serde_alloc" "serde_incl/alloc" "alloc") ("serde" "serde_incl/std" "std" "serde_alloc") ("derive" "bincode_derive") ("default" "std" "derive" "atomic") ("atomic") ("alloc"))))))

(define-public crate-bincode-2.0.0-beta.3 (c (n "bincode") (v "2.0.0-beta.3") (d (list (d (n "bincode_derive") (r "^2.0.0-beta.3") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_incl") (r "^1.0") (o #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0") (k 2)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde"))) (d #t) (k 2)))) (h "07xnwidwcwjjw0sffx5b01zixhxrx99lmrfgr7hlkhgpbcxfn2yj") (f (quote (("std" "alloc") ("serde_no_std" "serde_incl") ("serde_alloc" "serde_incl/alloc" "alloc") ("serde" "serde_incl/std" "std" "serde_alloc") ("derive" "bincode_derive") ("default" "std" "derive" "atomic") ("atomic") ("alloc"))))))

(define-public crate-bincode-2.0.0-rc.1 (c (n "bincode") (v "2.0.0-rc.1") (d (list (d (n "bincode_derive") (r "^2.0.0-rc.1") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_incl") (r "^1.0") (o #t) (k 0) (p "serde")) (d (n "serde_json") (r "^1.0") (k 2)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde"))) (d #t) (k 2)))) (h "0cqvpd795lxy41rwdxw5ahymj57bwzq9wy2a65vh438vqjrcw2gn") (f (quote (("std" "alloc") ("serde_no_std" "serde_incl") ("serde_alloc" "serde_incl/alloc" "alloc") ("serde" "serde_incl/std" "std" "serde_alloc") ("derive" "bincode_derive") ("default" "std" "derive" "atomic") ("atomic") ("alloc"))))))

(define-public crate-bincode-2.0.0-rc.2 (c (n "bincode") (v "2.0.0-rc.2") (d (list (d (n "bincode_derive") (r "^2.0.0-rc.2") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glam") (r "^0.21") (f (quote ("serde"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (k 2)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 2)))) (h "1i63s9x0n9i8vz2gjafn281v497aa8xsgqvkmvkv3fgl5rd0rdbv") (f (quote (("derive" "bincode_derive") ("default" "std" "derive")))) (s 2) (e (quote (("std" "alloc" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-bincode-2.0.0-rc.3 (c (n "bincode") (v "2.0.0-rc.3") (d (list (d (n "bincode_derive") (r "^2.0.0-rc.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "bincode_1") (r "^1.3") (d #t) (k 2) (p "bincode")) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glam") (r "^0.21") (f (quote ("serde"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (k 2)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "uuid") (r "^1.1") (f (quote ("serde"))) (d #t) (k 2)))) (h "15ffn22hv950sy0x95j8r60w3bh3i835r9ili0cfz53b6jha27pi") (f (quote (("derive" "bincode_derive") ("default" "std" "derive")))) (s 2) (e (quote (("std" "alloc" "serde?/std") ("alloc" "serde?/alloc"))))))

