(define-module (crates-io bi nc bincode_macro) #:use-module (crates-io))

(define-public crate-bincode_macro-0.1.0 (c (n "bincode_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17zk2n34dzmlym8dzvdh6vb8zlb8shvyijv6i992lmmrhvrfwfvi")))

(define-public crate-bincode_macro-0.1.1 (c (n "bincode_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xfs4v3s5wf3475fp4zfkm6lmkb7wzzz7f4iykc7130px63sc556")))

(define-public crate-bincode_macro-0.1.2 (c (n "bincode_macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1gq70d63xag4jcv53q67jzkxajh5w5445grvfq2s1lis1ky9lf8z")))

