(define-module (crates-io bi nc bincode-grpc-macro) #:use-module (crates-io))

(define-public crate-bincode-grpc-macro-0.1.0 (c (n "bincode-grpc-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hnq4qw6yzfwhkpi2r523lw8ap3hb7s4chlwpq879j1gw2gdixya")))

(define-public crate-bincode-grpc-macro-0.2.0 (c (n "bincode-grpc-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10422hca2xdq25jsr949mng634gm1dfmdj2v6crw78cvxclc84rd")))

(define-public crate-bincode-grpc-macro-0.3.0 (c (n "bincode-grpc-macro") (v "0.3.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1360laazpfj108bffdl42jn8h1mls99impn5gqy0ki4yfjs22jdc")))

(define-public crate-bincode-grpc-macro-0.3.1 (c (n "bincode-grpc-macro") (v "0.3.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11w9l7aq1f6s5s3l4878y6pxc5027hh5sf8icpplff6biz7yzzbp")))

(define-public crate-bincode-grpc-macro-0.4.0 (c (n "bincode-grpc-macro") (v "0.4.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0avdd5nx2jvdwbs2mwmkhi19kmjn8a96pym9vq02xkjv5x28vcmy")))

(define-public crate-bincode-grpc-macro-0.5.0 (c (n "bincode-grpc-macro") (v "0.5.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v6j9iklb5b4d3hhm8z0y3w6g93f5332nwv5947430hvyz0jh2m4")))

(define-public crate-bincode-grpc-macro-0.5.1 (c (n "bincode-grpc-macro") (v "0.5.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bdhx6s1qiaxxgiq8ffmghbl881zm6ly7bhsnrn62rdxhcj8b5s9")))

(define-public crate-bincode-grpc-macro-0.5.2 (c (n "bincode-grpc-macro") (v "0.5.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xxa4v15d2dnkpdlh7z8ihgj1xgj3v20f34pagdy43jwj3l9l0gf")))

(define-public crate-bincode-grpc-macro-0.5.3 (c (n "bincode-grpc-macro") (v "0.5.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04rxkm3anzcxiii89skarikgb11igvrcywkdg96x1l1glapipzyb")))

(define-public crate-bincode-grpc-macro-0.5.4 (c (n "bincode-grpc-macro") (v "0.5.4") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ama0y8mgffmh9slzj4dkgar69gphzxjx42sqsspn2kc24fp9jqr")))

(define-public crate-bincode-grpc-macro-0.5.6 (c (n "bincode-grpc-macro") (v "0.5.6") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gdiv8psid5awj4zgqgdl6vy5pliq462606vrsn1vf8ayz04m16d")))

(define-public crate-bincode-grpc-macro-0.6.0 (c (n "bincode-grpc-macro") (v "0.6.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smol") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0asjv7dz72lssfp6njq5x57r2nkam1yhhvz07wfnswq3aln5bjc4")))

(define-public crate-bincode-grpc-macro-0.7.0 (c (n "bincode-grpc-macro") (v "0.7.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ddqayc445952pwd5xxb2xr6wfa4h7df5f4hnb2ycw7px3a6jvj0")))

(define-public crate-bincode-grpc-macro-0.7.1 (c (n "bincode-grpc-macro") (v "0.7.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y9mis8mjd2p5azyrfy9wx1d8y9kbzd0g7x0gyg3v4zmcykpb02c")))

(define-public crate-bincode-grpc-macro-0.7.2 (c (n "bincode-grpc-macro") (v "0.7.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bks2a6k39vxmkynp552736x0501vr8zqig7fjmvip99d2m9bkzm")))

