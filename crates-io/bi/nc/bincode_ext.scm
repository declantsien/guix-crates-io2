(define-module (crates-io bi nc bincode_ext) #:use-module (crates-io))

(define-public crate-bincode_ext-0.0.1 (c (n "bincode_ext") (v "0.0.1") (d (list (d (n "bincode") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1g2yq16637lqkcmgwv10k3bjrmd5fzmxn6j6zg4073l0b6zbjv1r")))

(define-public crate-bincode_ext-0.0.2 (c (n "bincode_ext") (v "0.0.2") (d (list (d (n "bincode") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0xmcgjlhrqqjzs031r1i87v2mwqaiqzgkw1rqmdxlj45vn9wxccn")))

(define-public crate-bincode_ext-0.0.3 (c (n "bincode_ext") (v "0.0.3") (d (list (d (n "bincode") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "15hhgdvrf7qqjc9di55718lj0djn1k088x88prx3vqa434mib5vn")))

(define-public crate-bincode_ext-0.0.4 (c (n "bincode_ext") (v "0.0.4") (d (list (d (n "bincode") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1b42p3cgxahs1pv91x981bdnc2ipj0yljppzfgc9m537555nd56y")))

(define-public crate-bincode_ext-0.0.5 (c (n "bincode_ext") (v "0.0.5") (d (list (d (n "bincode") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0acfikx04ar07ds7i9x1fkdsr1ag5yrgl6m303kv3d2nr946m6s6")))

(define-public crate-bincode_ext-0.0.6 (c (n "bincode_ext") (v "0.0.6") (d (list (d (n "bincode") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0fha77x48mj8swgwizgqdqwxmgfwn6bppxrd1dnlzfi2d51gk67l")))

(define-public crate-bincode_ext-0.0.7 (c (n "bincode_ext") (v "0.0.7") (d (list (d (n "bincode") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "01mz19d874qrcgygm2gd4cy6mpylss5m44aifrrn892yz1c5909p")))

