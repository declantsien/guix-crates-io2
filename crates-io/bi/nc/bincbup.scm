(define-module (crates-io bi nc bincbup) #:use-module (crates-io))

(define-public crate-bincbup-0.1.0 (c (n "bincbup") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)))) (h "06qyp5f6d5v6hg2v0x4y067dfggr6npq1c4agxpqfwk7d0p007yl")))

(define-public crate-bincbup-0.1.1 (c (n "bincbup") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a3sb2g9mpaakzsd6mnq1j0jm7m6jpi1nhsx1ay1514s3459c1f0")))

