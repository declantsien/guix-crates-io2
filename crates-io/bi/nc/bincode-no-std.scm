(define-module (crates-io bi nc bincode-no-std) #:use-module (crates-io))

(define-public crate-bincode-no-std-1.0.0 (c (n "bincode-no-std") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.2.0") (k 0)) (d (n "core_io") (r "^0.1.20180307") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (f (quote ("alloc"))) (k 0)) (d (n "serde_bytes") (r "^0.10.3") (k 2)) (d (n "serde_derive") (r "^1.0.27") (k 2)))) (h "1ifk371zpn7a8v9xbfpa110dg7037rdypja3ynk8jlr7s44cdyq5")))

