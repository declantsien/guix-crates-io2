(define-module (crates-io bi nc bincode_core) #:use-module (crates-io))

(define-public crate-bincode_core-0.6.0 (c (n "bincode_core") (v "0.6.0") (d (list (d (n "byteorder_core_io") (r "^0.5") (k 0)) (d (n "core_io") (r "^0.1") (f (quote ("collections"))) (d #t) (k 0)) (d (n "serde") (r "0.8.*") (f (quote ("collections"))) (k 0)) (d (n "serde_derive") (r "0.8.*") (d #t) (k 0)))) (h "0vabqh8ra7nhzs8krbwk6pv3n7zhgwwp8mq7k155zs80jkqhgcsj")))

