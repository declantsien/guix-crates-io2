(define-module (crates-io bi ns binstat) #:use-module (crates-io))

(define-public crate-binstat-0.0.1 (c (n "binstat") (v "0.0.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("std" "derive" "color"))) (k 0)) (d (n "rust-htslib") (r "^0.38.2") (d #t) (k 0)))) (h "1vyrw3lc0lv01jvfnb4v3vil9z2rr9v5afvm322if8sd7w0f5y09")))

