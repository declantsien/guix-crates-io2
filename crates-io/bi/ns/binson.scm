(define-module (crates-io bi ns binson) #:use-module (crates-io))

(define-public crate-binson-0.1.0 (c (n "binson") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.23") (d #t) (k 0)))) (h "1zafnp7vcvjvrncmq0vm3zfbbi9dih7sysh1hngs5k3791pxjjy9")))

