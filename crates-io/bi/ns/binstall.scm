(define-module (crates-io bi ns binstall) #:use-module (crates-io))

(define-public crate-binstall-0.1.0 (c (n "binstall") (v "0.1.0") (h "10k2qnamw6wffq08786w9ggkacyv6vl06x7m4lwcillnv97xkbb3")))

(define-public crate-binstall-0.2.0 (c (n "binstall") (v "0.2.0") (h "1pnj0nncdyr507p8q64rgxghkhj1y6jz9dfd557yf29dabi2xn2n")))

