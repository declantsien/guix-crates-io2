(define-module (crates-io bi ns binstring) #:use-module (crates-io))

(define-public crate-binstring-0.1.0 (c (n "binstring") (v "0.1.0") (h "1zdm5szv27vyqjkbryznz34y7rg4cbbzkwx3hd8s3jrhr0yqijw1")))

(define-public crate-binstring-0.1.1 (c (n "binstring") (v "0.1.1") (h "11bsghizyz2xwxmqvsj7hlxs6qp180kl2vr0n4n7484k7nbn03by")))

