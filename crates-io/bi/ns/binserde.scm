(define-module (crates-io bi ns binserde) #:use-module (crates-io))

(define-public crate-binserde-0.1.0 (c (n "binserde") (v "0.1.0") (d (list (d (n "binserde_derive") (r "=0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "050fhzayh09skzcyz2kf4cbrjz8dg7g4kppmvapgz0syd0ahyzcc") (y #t)))

(define-public crate-binserde-0.1.1 (c (n "binserde") (v "0.1.1") (d (list (d (n "binserde_derive") (r "=0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wbsiwhgjqfz96gz8n4xrk13c9r001pl78dcg9pqmvg9gb4syhi5")))

(define-public crate-binserde-0.1.2 (c (n "binserde") (v "0.1.2") (d (list (d (n "binserde_derive") (r "=0.1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kd2njzn3n55cz44gv0nlqhh45x2zb8sqcgzac5jk7yjx4mgby7a")))

(define-public crate-binserde-0.1.3 (c (n "binserde") (v "0.1.3") (d (list (d (n "binserde_derive") (r "=0.1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jbkp11pbr1jak4lfay7hsixsp3kbwc18g37a9pfsclmj23fj628")))

(define-public crate-binserde-0.1.4 (c (n "binserde") (v "0.1.4") (d (list (d (n "binserde_derive") (r "=0.1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02g3llhb7l3dk7jqzbmhklfrwqi0k4r26dw8gxzw7cqmf86ppndq")))

(define-public crate-binserde-0.1.5 (c (n "binserde") (v "0.1.5") (d (list (d (n "binserde_derive") (r "=0.1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r78m250i9dsws8qmcbx40g9dcr74n1qsf3xqaxbvkb455k3v24f")))

(define-public crate-binserde-0.1.6 (c (n "binserde") (v "0.1.6") (d (list (d (n "binserde_derive") (r "=0.1.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1abjn8ps9gsj98qrifcnzczq6fmxghmhzqnkxvmxnvn3g8nmy1z6")))

(define-public crate-binserde-0.1.7 (c (n "binserde") (v "0.1.7") (d (list (d (n "binserde_derive") (r "=0.1.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16d1a2pv54vj906gc7viwgxvyc2iv6nd16l5i85snp6z2zy1vq81")))

