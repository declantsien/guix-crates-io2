(define-module (crates-io bi ns binserde_derive) #:use-module (crates-io))

(define-public crate-binserde_derive-0.1.0 (c (n "binserde_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jcclimzmhd889hg3qpzf0xq09rdzrzcv4ckbpy7awlk1qdpxhwa")))

(define-public crate-binserde_derive-0.1.1 (c (n "binserde_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rmw9zr4f5qrsi9l5li404fcgzk383j8dclcj9sxwr33fv8kqiwf")))

(define-public crate-binserde_derive-0.1.2 (c (n "binserde_derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lnsci0v0jfypp79cl3vkbb8hy4iwm1pvnbpp25fg9khfkkqqbaj")))

(define-public crate-binserde_derive-0.1.3 (c (n "binserde_derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10hwq9aghk27shrpyvvyz0xxaqvzvhri21v8q3hccw19jd7h7fqw")))

(define-public crate-binserde_derive-0.1.4 (c (n "binserde_derive") (v "0.1.4") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19s2giaba8rskqlqgpby2jgskpb7agd5afdvnbss9q34kjracgrd")))

