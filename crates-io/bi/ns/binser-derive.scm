(define-module (crates-io bi ns binser-derive) #:use-module (crates-io))

(define-public crate-binser-derive-0.1.0 (c (n "binser-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1k4xmm6rv8lmlsp7d164s0mpl1zpv0ikxq7r173mf7shpq4b2nir")))

(define-public crate-binser-derive-0.1.1 (c (n "binser-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0jch6kkff8y8wj3q8sn4zv6p7xm4ba5wvcx6p3yn3g4l62651m0p")))

(define-public crate-binser-derive-0.1.2 (c (n "binser-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "083701w823j3xh4ac29nplq0bnj2q820rk2b15fm60fgkadhw6k1")))

(define-public crate-binser-derive-0.2.0 (c (n "binser-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1sa28cywk1xrn11612c1skid6vxksmsbz2vnraxd9gynls1dmarg")))

(define-public crate-binser-derive-0.2.1 (c (n "binser-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0m8604v2rg8dznmmc2yc821i18vc87avhbrgwn5ks8rbr09fzvr9")))

