(define-module (crates-io bi ns binstore) #:use-module (crates-io))

(define-public crate-binstore-0.2.0 (c (n "binstore") (v "0.2.0") (d (list (d (n "assert_matches") (r "~1.3") (d #t) (k 2)) (d (n "bincode") (r "~1.1") (d #t) (k 0)) (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (k 0)) (d (n "env_logger") (r "~0.6") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "lz4") (r "~1.23") (d #t) (k 0)) (d (n "proptest") (r "~0.9") (d #t) (k 2)) (d (n "rand") (r "~0.6") (d #t) (k 2)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "tempfile") (r "~3.0") (d #t) (k 2)))) (h "0hz79zs8a9jfcr15xf1wbdgy9ca1zj7qfim0d21x7679da7hkzc5")))

