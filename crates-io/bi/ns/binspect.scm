(define-module (crates-io bi ns binspect) #:use-module (crates-io))

(define-public crate-binspect-0.1.0 (c (n "binspect") (v "0.1.0") (h "1hdsr88ds0bcrw389vl26l92ngxm9vlv6wjqc5w0dx3rvl948zfv")))

(define-public crate-binspect-0.1.1 (c (n "binspect") (v "0.1.1") (h "1lfl1g6jms64028r30g9qqblscmv3qgdpwnhhxszv3j2g0nq2cns")))

