(define-module (crates-io bi ns binser) #:use-module (crates-io))

(define-public crate-binser-0.1.0 (c (n "binser") (v "0.1.0") (d (list (d (n "binser-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0hnsmxrr9rzzbg9wk1z52nvpxh1lb3bbyrg8zx5mmsf42rgy0rig")))

(define-public crate-binser-0.2.0 (c (n "binser") (v "0.2.0") (d (list (d (n "binser-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1mcnlm7szg5imi52vmz00i1aar74sb4azwq8zm49mqfv7pdryx4l")))

(define-public crate-binser-0.2.1 (c (n "binser") (v "0.2.1") (d (list (d (n "binser-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1axsc2jz7bx6v9jzkryp8yafhhwfxpdx90ncd19kpk9rv4l04ks9")))

(define-public crate-binser-0.3.0 (c (n "binser") (v "0.3.0") (d (list (d (n "binser-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0zd7mfm048snl06dx1ym7n91x3a8av1hhzfh93cm4ddapb6rmxwk")))

(define-public crate-binser-0.3.1 (c (n "binser") (v "0.3.1") (d (list (d (n "binser-derive") (r "^0.2.1") (d #t) (k 0)))) (h "1mzd9yl8hkxqh768w0bq3dlxx2xpz8nrzmsmbpfiaw7fbh70b5rl")))

