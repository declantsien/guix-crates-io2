(define-module (crates-io bi od biodome) #:use-module (crates-io))

(define-public crate-biodome-0.1.1 (c (n "biodome") (v "0.1.1") (d (list (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "1x7cz6zla4s0b7llg9ay322aafs15023bzi6i8714kfrd3kwf91j")))

(define-public crate-biodome-0.2.1 (c (n "biodome") (v "0.2.1") (d (list (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "1i2r91lfg0310mpng0s7n5b2v7y1v0rkggn62q85z6dhpx8rvhg0")))

