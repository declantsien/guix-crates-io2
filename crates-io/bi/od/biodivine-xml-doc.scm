(define-module (crates-io bi od biodivine-xml-doc) #:use-module (crates-io))

(define-public crate-biodivine-xml-doc-0.3.0 (c (n "biodivine-xml-doc") (v "0.3.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 2)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)))) (h "07v50bjrl1s7jq9r8zz2sfadgpczwd0wvfbpj45l6lr2hzgp1d0q")))

