(define-module (crates-io bi od biodivine-lib-bdd) #:use-module (crates-io))

(define-public crate-biodivine-lib-bdd-0.1.0 (c (n "biodivine-lib-bdd") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0cq2bb1lc2qdp6q9i7rfkxbxsq6zp9dp7ybrz74yszhqz5pqd1p9") (f (quote (("shields_up"))))))

(define-public crate-biodivine-lib-bdd-0.2.0 (c (n "biodivine-lib-bdd") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0c7sjdbmbap7jlam2lqa7n0syf3ch8hq9zyavj36cphc4jizrsgs") (f (quote (("shields_up"))))))

(define-public crate-biodivine-lib-bdd-0.2.1 (c (n "biodivine-lib-bdd") (v "0.2.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1hr2lvvq8lay1zvp436g28jpqc0kq5ybkgqryfw86arkrldkmdg7") (f (quote (("shields_up"))))))

(define-public crate-biodivine-lib-bdd-0.3.0 (c (n "biodivine-lib-bdd") (v "0.3.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1iaid18mc05440pyck3ndr805hcywh5wx5ypr2g74c8i1i3gpp5x")))

(define-public crate-biodivine-lib-bdd-0.4.0 (c (n "biodivine-lib-bdd") (v "0.4.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1pw637fdw2hjc4fh33n3sgwfrzc4yn7wb846haq1ip63rj25gyql")))

(define-public crate-biodivine-lib-bdd-0.4.1 (c (n "biodivine-lib-bdd") (v "0.4.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02d1q5bjppm3ssmr0dc3v1q996y0smgrjql294k3z41q1ss38ycn")))

(define-public crate-biodivine-lib-bdd-0.4.2 (c (n "biodivine-lib-bdd") (v "0.4.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qxslfdk7xblc2gs15npcvxggzpb1zhwdy12yss8k0bfr3rck237")))

(define-public crate-biodivine-lib-bdd-0.5.0 (c (n "biodivine-lib-bdd") (v "0.5.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17bc4y9zxn07v9sdqd2a4ijrdiilicasbdknk29jcmvd1z2s9kjy")))

(define-public crate-biodivine-lib-bdd-0.5.1 (c (n "biodivine-lib-bdd") (v "0.5.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "143xppc6cc96zj9zsnb6wf37cyf4gmvh6q0a2lpkwcb9m5cr8670")))

(define-public crate-biodivine-lib-bdd-0.5.2 (c (n "biodivine-lib-bdd") (v "0.5.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kk6a7wy0r055z37zbh2hpwaw6gq6mlysx9b17gl3fp5q1k54d90")))

(define-public crate-biodivine-lib-bdd-0.5.3 (c (n "biodivine-lib-bdd") (v "0.5.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dlfh9x08nk92gbwq4qx9m6jg0hg2dcvj8f3b58b1hgyd1r8fjzy")))

(define-public crate-biodivine-lib-bdd-0.5.4 (c (n "biodivine-lib-bdd") (v "0.5.4") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "073wrmh5cw5gwlcl9k8jbbpibzs4gnvskqwld145n79840gbfkcv")))

(define-public crate-biodivine-lib-bdd-0.5.5 (c (n "biodivine-lib-bdd") (v "0.5.5") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fpcqw1r4ngan0hwix3r5qdn3mzik6536ygdg4j63y5qjwcv4gih")))

(define-public crate-biodivine-lib-bdd-0.5.6 (c (n "biodivine-lib-bdd") (v "0.5.6") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kl1zjjjmji455sdvvzdz1b6ni175b7596arm3c68snwn8vdlz8h")))

(define-public crate-biodivine-lib-bdd-0.5.7 (c (n "biodivine-lib-bdd") (v "0.5.7") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wzgw1izhkk3chacdzpk324h4alkpi3hq8pdwd26fp5n8dhimnc1")))

(define-public crate-biodivine-lib-bdd-0.5.8 (c (n "biodivine-lib-bdd") (v "0.5.8") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12zlkxxwf2s3j6qvci8w8sk7ihfhg2h3x39q4869csisczn9rxpk")))

(define-public crate-biodivine-lib-bdd-0.5.9 (c (n "biodivine-lib-bdd") (v "0.5.9") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0v8qnhyraq7jank8px6xnpj42imcir296cmr0n2g1v8vmrq2dh40")))

(define-public crate-biodivine-lib-bdd-0.5.10 (c (n "biodivine-lib-bdd") (v "0.5.10") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0apns9pv2cqqs5gxd1lfwmxj2bmmvgjgyhvb5xv90abfgb776wjz")))

(define-public crate-biodivine-lib-bdd-0.5.11 (c (n "biodivine-lib-bdd") (v "0.5.11") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08wsdr9i84slxma00bmj4bbyf2pais1c60b8yk4zkqqba8vggwgr")))

(define-public crate-biodivine-lib-bdd-0.5.12 (c (n "biodivine-lib-bdd") (v "0.5.12") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hv1a99ss2av0iiadlw4rdivwkcg5gyb944qv42j7vabjn1qyggp")))

(define-public crate-biodivine-lib-bdd-0.5.13 (c (n "biodivine-lib-bdd") (v "0.5.13") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0sapvgrh9hkslwz4mvhj0rb7jag3350sv20zqri7gx2wdq0z76qk")))

(define-public crate-biodivine-lib-bdd-0.5.14 (c (n "biodivine-lib-bdd") (v "0.5.14") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mlzcvz63r4pgz9jgpzhbp0aa1gl6acdiqwz8y51xzy1446yxg55")))

(define-public crate-biodivine-lib-bdd-0.5.15 (c (n "biodivine-lib-bdd") (v "0.5.15") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04g3cc1lapnql4qq0xryhldc26zxwwdcqfrvvcgjglp65sdw5vcz")))

(define-public crate-biodivine-lib-bdd-0.5.16 (c (n "biodivine-lib-bdd") (v "0.5.16") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0i32zg5hbrvs287y0pkp29vkq29sav9wslflc2sa6jgn6icl5738")))

(define-public crate-biodivine-lib-bdd-0.5.17 (c (n "biodivine-lib-bdd") (v "0.5.17") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0phyz0hflis78irgyqixdryxbf7jmrq92ki3g52za9fxbmx51mm2") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

