(define-module (crates-io bi od biodiff-wfa2-sys) #:use-module (crates-io))

(define-public crate-biodiff-wfa2-sys-2.3.4-cf3eb92 (c (n "biodiff-wfa2-sys") (v "2.3.4-cf3eb92") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1lmhlwj9rrppdh6y6n504szcsf4n38n3jiqrjpzr5b8p4zdihl2d") (f (quote (("default" "bundle-wfa2") ("bundle-wfa2")))) (l "wfa2")))

(define-public crate-biodiff-wfa2-sys-2.3.5 (c (n "biodiff-wfa2-sys") (v "2.3.5") (d (list (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00mfxh77s8cfn78whj1k513n9v3j836ghqilhnq8la0s806qp3l4") (f (quote (("default" "bundle-wfa2") ("bundle-wfa2" "cmake")))) (l "wfa2")))

