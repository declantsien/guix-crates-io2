(define-module (crates-io bi p4 bip47) #:use-module (crates-io))

(define-public crate-bip47-0.1.0 (c (n "bip47") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.27") (d #t) (k 0)))) (h "0ys0avf9az6by72y77kwgnxlp8512gq9m6i2cwp9873x9hika2nk")))

(define-public crate-bip47-0.2.0 (c (n "bip47") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.27") (d #t) (k 0)))) (h "05wmsc5j8isz1l3n007zbzsbasazpy5cf5b18v869gy4yqjj2581")))

(define-public crate-bip47-0.3.0 (c (n "bip47") (v "0.3.0") (d (list (d (n "bitcoin") (r "^0.28") (d #t) (k 0)))) (h "0dk3j7dbxy67an6jsylmrmsyjjrbf3rbdiqc28pwyym142iakln8")))

