(define-module (crates-io bi bc bibcompiler) #:use-module (crates-io))

(define-public crate-bibcompiler-0.1.0 (c (n "bibcompiler") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mll7dl6b0qjg8g4s66mnznhgkv0mn3pd0mrp1z8nd6jvwa5p03d") (y #t)))

(define-public crate-bibcompiler-0.1.1 (c (n "bibcompiler") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sxh0d4d884gkj2lfk2lwrvdscafiwddrsqbib2hrp0fvljzgnyn") (y #t)))

(define-public crate-bibcompiler-0.1.2 (c (n "bibcompiler") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "11xgk95g08x5rgi2yylj56dfqj8rsi5r1ba3gyz7ykzbnbwjphq4") (y #t)))

(define-public crate-bibcompiler-0.1.3 (c (n "bibcompiler") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hamgw069g073bxw15v6c4h1nkhj45ra6c6xlixs8y4bvpm84rrd") (y #t)))

