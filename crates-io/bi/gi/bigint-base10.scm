(define-module (crates-io bi gi bigint-base10) #:use-module (crates-io))

(define-public crate-bigint-base10-0.1.0 (c (n "bigint-base10") (v "0.1.0") (h "081i3kr9sfvgxa2wlhg94fa3yjyw0jwj57a7k60lmxsmawkyqx7z")))

(define-public crate-bigint-base10-0.1.1 (c (n "bigint-base10") (v "0.1.1") (h "1pmk18awfy3sl5qm335a132c9zyzmzga7x26i6cd6gbmxch1nl90")))

