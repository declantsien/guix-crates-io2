(define-module (crates-io bi gi bigint-benchmark) #:use-module (crates-io))

(define-public crate-bigint-benchmark-0.1.0 (c (n "bigint-benchmark") (v "0.1.0") (d (list (d (n "ibig") (r "^0.2.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "ramp") (r "^0.5.9") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)))) (h "0f9c1fwcrbmdnvl3mrqhznm8mvjkjf7as8dsl6hjb8ckam23cwna")))

(define-public crate-bigint-benchmark-0.2.0 (c (n "bigint-benchmark") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ibig") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "ramp") (r "^0.5.9") (d #t) (k 0)) (d (n "rug") (r "^1.11.0") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)))) (h "11v5cvv8dmjl87pcpp6f16wv4vpb8mjjl5pajsyr78faqa9ic7bg")))

(define-public crate-bigint-benchmark-0.2.1 (c (n "bigint-benchmark") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ibig") (r "^0.3.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "ramp") (r "^0.6.0") (d #t) (k 0)) (d (n "rug") (r "^1.13.0") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)))) (h "1s3sdwcfy99rk367i222zgc71s9l1cjxrfpf0x4dlwfw47dc0mx1")))

(define-public crate-bigint-benchmark-0.2.2 (c (n "bigint-benchmark") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ibig") (r "^0.3.4") (d #t) (k 0)) (d (n "malachite-base") (r "^0.2.2") (d #t) (k 0)) (d (n "malachite-nz") (r "^0.2.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "ramp") (r "^0.7.0") (d #t) (k 0)) (d (n "rug") (r "^1.16.0") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)))) (h "1lf51lg95jfzndng5qb0hy2ds3izg0wjd42v5g3jx7hr6hw46nl2")))

(define-public crate-bigint-benchmark-0.3.0 (c (n "bigint-benchmark") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "ibig") (r "^0.3.4") (d #t) (k 0)) (d (n "malachite") (r "^0.3.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "rug") (r "^1.17.0") (d #t) (k 0)) (d (n "rust-gmp") (r "^0.5.0") (d #t) (k 0)))) (h "011csjf1j54mkg0jdh580l83f2l2fm227qqbcn682g1wlakk7qsg")))

(define-public crate-bigint-benchmark-0.4.0 (c (n "bigint-benchmark") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dashu") (r "=0.4.0") (d #t) (k 0)) (d (n "ibig") (r "=0.3.6") (d #t) (k 0)) (d (n "malachite") (r "=0.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "=0.4.4") (d #t) (k 0)) (d (n "rug") (r "=1.22.0") (d #t) (k 0)))) (h "0bqbilrdhqp9prawqkysj3kbnykjm79kp5zcgcf0xssnbpyyrvzl")))

