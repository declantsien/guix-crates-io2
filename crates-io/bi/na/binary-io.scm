(define-module (crates-io bi na binary-io) #:use-module (crates-io))

(define-public crate-binary-io-0.1.0 (c (n "binary-io") (v "0.1.0") (d (list (d (n "cryptostream") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "fake-enum") (r "^0.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0sa7wi2b023rzms7fk9gg371rcri2mvr37yl6i4d2lbbr2h3r3nl") (f (quote (("uuid_v1" "uuid" "uuid/v1" "rand") ("shade" "nbt") ("random_uuid" "rand") ("nbt") ("default" "nbt") ("crypto_shade" "shade" "cryptostream" "openssl" "zeroize"))))))

