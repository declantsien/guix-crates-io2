(define-module (crates-io bi na binary-space-partition) #:use-module (crates-io))

(define-public crate-binary-space-partition-0.1.0 (c (n "binary-space-partition") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1c9w873pajxx01x3jg3pwd7z5md07bhjbqx77md55h4acsvx2z1g")))

(define-public crate-binary-space-partition-0.1.1 (c (n "binary-space-partition") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0gg3idswpzhhrp4rvksrq8kzcqifbxgbpngvbcpk6p1bkcfjhrfz")))

(define-public crate-binary-space-partition-0.1.2 (c (n "binary-space-partition") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1zwphph7fb532sc1kr6bpngdv01prv9xg672fqlf9l2gdk8v1kl8")))

