(define-module (crates-io bi na binary_type_cast) #:use-module (crates-io))

(define-public crate-binary_type_cast-0.1.0 (c (n "binary_type_cast") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)))) (h "1x7iq247i048jbq8skn4038hx6q9rcpvgb2k8f0di37nr0cw4sqs") (y #t)))

(define-public crate-binary_type_cast-0.1.1 (c (n "binary_type_cast") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)))) (h "1wgya2502mvnadjkb4xx3p217da7h4izznmm14k5gwavr5x37klw") (y #t)))

(define-public crate-binary_type_cast-0.1.2 (c (n "binary_type_cast") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)))) (h "13c3w43xj4c9s449299k9hr3kagc2kmlsrw3sb3s73kpz672fzlg")))

