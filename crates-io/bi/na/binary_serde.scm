(define-module (crates-io bi na binary_serde) #:use-module (crates-io))

(define-public crate-binary_serde-0.1.0 (c (n "binary_serde") (v "0.1.0") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "17jxr1hmzl8iy3yp08mydkj97vcw0in20dxgxgdr96hvkjazv2db")))

(define-public crate-binary_serde-0.1.1 (c (n "binary_serde") (v "0.1.1") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1n5gcnkxak07i1almq9dz35z7909m76hkx93af9f7blj5dhdpqi1")))

(define-public crate-binary_serde-0.1.2 (c (n "binary_serde") (v "0.1.2") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1wb2da66zh8bi3cfgg284k15raq0rz5jrwwf3xwapzi2z36nyv2x")))

(define-public crate-binary_serde-0.1.3 (c (n "binary_serde") (v "0.1.3") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0nz8nyig0mffbf28y7wm5fy0ynllwc3903g0cz5ajnashnd7z4ws")))

(define-public crate-binary_serde-0.1.4 (c (n "binary_serde") (v "0.1.4") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "082wn2ygd2f65zmhxjqzvs4q6fsg15yvyz3cy0xdd52x74rfgrg8")))

(define-public crate-binary_serde-0.1.5 (c (n "binary_serde") (v "0.1.5") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^0.1.5") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0dvzwywg9f76qfpb78maspwmmy2hdvmyz87rbyxcz6dc93ip9cmb") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-0.1.6 (c (n "binary_serde") (v "0.1.6") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^0.1.6") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "04m3slx31z60cdqgn3r6z4gpy6qsgdnrv2zlxblp6m684k7ily1m") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.1 (c (n "binary_serde") (v "1.0.1") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.1") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "19vlyml0rwz340j7vbhnvgvffp7y33cfk0kg96jllq2ymhqn55ik") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.2 (c (n "binary_serde") (v "1.0.2") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.2") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1dayhyz2ihfmmla0yk33y5qnfamh8kfpivgf3150bwdl6jil3vdj") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.3 (c (n "binary_serde") (v "1.0.3") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.3") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0r79ivgmxn1vmba53knwzanr6zxn1k65ysvvz4rfgqvb024z70qa") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.4 (c (n "binary_serde") (v "1.0.4") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.4") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1f1j7qycl7r9pysggnrymkx9r104knv565yj5ny7hmh6s4cqfnyj") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.5 (c (n "binary_serde") (v "1.0.5") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.5") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1xr82gmxg4rg05y1c0rkwnwh9wjwc16x6ihwjmvg83x6x373dinh") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.6 (c (n "binary_serde") (v "1.0.6") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.6") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "18gi4jnfz4qgwjb081qvlvjdf1khqfn2q3xknjm3zzxdnpa4x0qh") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.7 (c (n "binary_serde") (v "1.0.7") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.7") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1y53pnfm20qlp0l6p2h64cdlxak00840j25xx317sj4d87n8gbkj") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.8 (c (n "binary_serde") (v "1.0.8") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.8") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0g2i62fib4sdr30wh2v60gr80wpygbvsmwz5i8829hifyfz97lrg") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.9 (c (n "binary_serde") (v "1.0.9") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.9") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1blwi8cxd94jhs6142nsfy5cx1cxnrhxlbgkpyrbg2s37j7dpb2p") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.10 (c (n "binary_serde") (v "1.0.10") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.10") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0ns325vl4jjpbr40lrqzxp99nkfvdn19km6a9nj3fd8k6kigqrid") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.11 (c (n "binary_serde") (v "1.0.11") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.11") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "05403py6r12ddf4qr23f7i3ylgw3yv0k2bjrvsapwzy3jd2yfyia") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.12 (c (n "binary_serde") (v "1.0.12") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.12") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "021xbz2ripgj2b61lyp5j8frmzb2g2vx512a08cgi0v0hqamlli9") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.13 (c (n "binary_serde") (v "1.0.13") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.13") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0plqm7rz5qwi1zy28f7c3222b9r7fymvx6klyld10x2djcdbmjrr") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.14 (c (n "binary_serde") (v "1.0.14") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.14") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "12hpafz3iy430lbdqpd5d72qiy97yg4xwpj38qnpky4dmzjc44xg") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.15 (c (n "binary_serde") (v "1.0.15") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.15") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1fihnmnggxmmnpya79win34lam95izn8cyzlvk9vcmq6n3qyvyda") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.16 (c (n "binary_serde") (v "1.0.16") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.16") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "07zzbhzqypmx7k3lgdgqx7myp33pgb7mxinq5p6rdpbk49lh37x7") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.17 (c (n "binary_serde") (v "1.0.17") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.17") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "081xa9i1di9pxfgk0jnf6l0bslikgi7289if38yk260jj27kspm7") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.18 (c (n "binary_serde") (v "1.0.18") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.18") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1kciga3k5kmqwsvm8d1sch6jjfg0d3jfxx6isrcrp6n6pz9ncga2") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.19 (c (n "binary_serde") (v "1.0.19") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.19") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0dvh07blr58a9x8x850rrz3csz7qg4ckcdyqnig1hg9nl4jzq09k") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.20 (c (n "binary_serde") (v "1.0.20") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.20") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1lfccsll1ddsmkp5mpai80a725jfcz7wchlz4c97k6sjv11fpl1x") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.21 (c (n "binary_serde") (v "1.0.21") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.21") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1amiva5g5djrkanamsca0c9rnhwp9zzgfwxk05zbx0was6bix0ib") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.22 (c (n "binary_serde") (v "1.0.22") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.22") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0jpy4a2xm3p594k4l80d9ngf5sp3ykzxmbjs4vhaby9a00rdvzjw") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.23 (c (n "binary_serde") (v "1.0.23") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.23") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "00yv9zd4986ispidx98w7hvn5a4cliigij7z0zs2xxpspghlxpx7") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-binary_serde-1.0.24 (c (n "binary_serde") (v "1.0.24") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "binary_serde_macros") (r "^1.0.24") (d #t) (k 0)) (d (n "recursive_array") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0sf5ki5h5nmcyg4b51b3jziz3sjda6kz742sf7pzzyy44awplyby") (f (quote (("std" "thiserror-no-std/std"))))))

