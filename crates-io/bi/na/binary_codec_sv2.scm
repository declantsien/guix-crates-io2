(define-module (crates-io bi na binary_codec_sv2) #:use-module (crates-io))

(define-public crate-binary_codec_sv2-0.1.0 (c (n "binary_codec_sv2") (v "0.1.0") (h "0gqly175d1fvsnng8aqbq25lm4zrlmp4iajms6z13f1wcn5nq1iq") (f (quote (("no_std") ("deafult" "no_std"))))))

(define-public crate-binary_codec_sv2-0.1.1 (c (n "binary_codec_sv2") (v "0.1.1") (h "02v5xizhba849afq0gp1bqv49lvy5rg6h3m6gpzqjhd5dry36gsv") (f (quote (("no_std") ("deafult" "no_std"))))))

(define-public crate-binary_codec_sv2-0.1.2 (c (n "binary_codec_sv2") (v "0.1.2") (d (list (d (n "quickcheck") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1zn8qgkbzph8z4n85h2c8479m3fys3bwfd1qmbd9rw4iybj77586") (f (quote (("prop_test" "quickcheck") ("no_std") ("deafult" "no_std"))))))

(define-public crate-binary_codec_sv2-0.1.3 (c (n "binary_codec_sv2") (v "0.1.3") (d (list (d (n "buffer_sv2") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "067h3dyzn9hsxfdhdri0pv50l1s0b7vqv5kypszsrawrwxps35wr") (f (quote (("with_buffer_pool" "buffer_sv2") ("prop_test" "quickcheck") ("no_std") ("default" "no_std"))))))

(define-public crate-binary_codec_sv2-0.1.5 (c (n "binary_codec_sv2") (v "0.1.5") (d (list (d (n "buffer_sv2") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0y4ad2pkylnjy7383b6pag6p7ng3bjg925phplvdbb5pa5b67ln3") (f (quote (("with_buffer_pool" "buffer_sv2") ("prop_test" "quickcheck") ("no_std") ("default" "no_std"))))))

(define-public crate-binary_codec_sv2-1.0.0 (c (n "binary_codec_sv2") (v "1.0.0") (d (list (d (n "buffer_sv2") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0qybykbr69s7ymcy2q2s2q6hblbq20dv9i973ahizihwly5bqs69") (f (quote (("with_buffer_pool" "buffer_sv2") ("prop_test" "quickcheck") ("no_std") ("default" "no_std"))))))

