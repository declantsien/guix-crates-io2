(define-module (crates-io bi na binator_base) #:use-module (crates-io))

(define-public crate-binator_base-0.0.0 (c (n "binator_base") (v "0.0.0") (d (list (d (n "binator_core") (r "^0.0.2") (k 0)) (d (n "binator_utils") (r "^0.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0impvm9i953yy7kqkplrdm44kvnb7vj3r7s1kckjxcw8g3wfgdpl") (f (quote (("integer_radix" "num-traits") ("default" "integer_radix"))))))

