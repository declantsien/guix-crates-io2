(define-module (crates-io bi na binary-layout) #:use-module (crates-io))

(define-public crate-binary-layout-0.1.0 (c (n "binary-layout") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "12f1wzra96dr5mbfnx56dygvvf6nya9v08m1pd2qyg12fgzqqqa4")))

(define-public crate-binary-layout-0.2.0 (c (n "binary-layout") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "0jabmy6r4wayn0drxql3ldl3xvnhrbpl3p6accj04x98gilq7wl4")))

(define-public crate-binary-layout-0.3.0 (c (n "binary-layout") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "1ws6x39hpwnkssc1616gn1p4qycfz6qsyasqk1nqf154827wv72n")))

(define-public crate-binary-layout-0.4.0 (c (n "binary-layout") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "04bx8awcx1m5d71w0m242q716ds521iix4wp4as0m991kfili8l9")))

(define-public crate-binary-layout-1.0.0 (c (n "binary-layout") (v "1.0.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "15ydyr7la3zbjz6dlasalpqfbcv0fr7y4i52qv9r8190vn51q8mi")))

(define-public crate-binary-layout-1.0.1 (c (n "binary-layout") (v "1.0.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "1vqpd4hq3y13zl1lq913yy7z1waag7gx0hs68lqc3hhn6l8gs7bg")))

(define-public crate-binary-layout-2.0.0 (c (n "binary-layout") (v "2.0.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0hri5wfcdg4s5mg3cfzszgnzm0a5n4lw5h5agwi0n19zv58f0iq6")))

(define-public crate-binary-layout-2.0.1 (c (n "binary-layout") (v "2.0.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0klq7nryf3iv49fq8nxzv5cqwf62di3dx2lq3sc14s4jkxcsz7hj")))

(define-public crate-binary-layout-2.1.0 (c (n "binary-layout") (v "2.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "01csg7ikzik767m7i38ankihs6cnd22449krc2ingw86vg5ppyq9") (f (quote (("std") ("default" "std"))))))

(define-public crate-binary-layout-3.0.0 (c (n "binary-layout") (v "3.0.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1170xfjyfyljrj6lnm5vbw4bkip9w3nwpc4v9a4vfi13227038ar") (f (quote (("std") ("default" "std"))))))

(define-public crate-binary-layout-3.1.0 (c (n "binary-layout") (v "3.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1m9hf3wxiv9ikwg52dfm67m4ah4hirs8v7rq0lq8v1zgcqrji1dx") (f (quote (("std") ("default" "std"))))))

(define-public crate-binary-layout-3.1.1 (c (n "binary-layout") (v "3.1.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1c6lmy6j1fbg0z90pqm358jbz744538xhpa1blmd2kk0q8qrz778") (f (quote (("std") ("default" "std"))))))

(define-public crate-binary-layout-3.1.2 (c (n "binary-layout") (v "3.1.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "19bsvg8lyijyvhrwpzqzpdd7k9nl8bw7nxdc6bn5w7ds8ww6az9f") (f (quote (("std") ("default" "std"))))))

(define-public crate-binary-layout-3.1.3 (c (n "binary-layout") (v "3.1.3") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "077cx7svh7v8mv69vgki91d3b2cyf7asjcmcm11489ybw2kwvf2f") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-binary-layout-3.1.4 (c (n "binary-layout") (v "3.1.4") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0dzggnpq8bf3rxndj9id93yyfc9g5y8769jy0a58pvcps643l5kz") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-binary-layout-3.2.0 (c (n "binary-layout") (v "3.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0gm8msyzcwffzpnc6zwnclz165q03awkkixx0j26afl550q46k5a") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-binary-layout-3.3.0 (c (n "binary-layout") (v "3.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "06gqcjg9mnj2f0hg3ad8p0aralgf4w7p297kzy49b6zm9i8f6iaq") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-binary-layout-4.0.0 (c (n "binary-layout") (v "4.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "02aafmp2h006ylshdn05dkxpb1dzsk2l21fs7w2s5y7m2gmp2i17") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-binary-layout-4.0.1 (c (n "binary-layout") (v "4.0.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1k2hcwf43wkad5rjlklwshp4qq6z3kx290rds2sg5y6df5676n7m") (f (quote (("std") ("default" "std")))) (r "1.59")))

(define-public crate-binary-layout-4.0.2 (c (n "binary-layout") (v "4.0.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (o #t) (d #t) (k 0)))) (h "1d17a5m1qkv2bqnr6l7caz8w1s5d77rnw8pn44sfzasn47dfiiv6") (f (quote (("std" "thiserror") ("default" "std")))) (r "1.59")))

