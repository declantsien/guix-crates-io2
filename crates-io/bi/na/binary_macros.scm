(define-module (crates-io bi na binary_macros) #:use-module (crates-io))

(define-public crate-binary_macros-0.1.0 (c (n "binary_macros") (v "0.1.0") (d (list (d (n "data-encoding") (r "^1.1.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "dotenv_macros") (r "^0.9") (d #t) (k 2)))) (h "0gjp38bg1l1w9cxinzs0ipg4f1w8szjd3kijrpb82jxb19mg48bz")))

(define-public crate-binary_macros-0.1.1 (c (n "binary_macros") (v "0.1.1") (d (list (d (n "data-encoding") (r "^1.1.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "dotenv_macros") (r "^0.9") (d #t) (k 2)))) (h "0953m85ivc4a437alnfac7r23yxk60d6rspn57zz33pc02hyb25f")))

(define-public crate-binary_macros-0.1.2 (c (n "binary_macros") (v "0.1.2") (d (list (d (n "data-encoding") (r "^1.1.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "dotenv_macros") (r "^0.9") (d #t) (k 2)))) (h "067285d53cjj4a07x4jscc2vmnxc6sx0fga78q0hv0cynh7x0951")))

(define-public crate-binary_macros-0.2.0 (c (n "binary_macros") (v "0.2.0") (d (list (d (n "data-encoding") (r "^1.1.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "dotenv_macros") (r "^0.9") (d #t) (k 2)))) (h "0mlib3fiz1w2781rsz0wq3byvvxilg1ah7l1wjj3v9qy3iyqad41")))

(define-public crate-binary_macros-0.4.0 (c (n "binary_macros") (v "0.4.0") (d (list (d (n "binary_macros_impl") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "0sgvq2yz6p3bwrw8yrbhkycaljkq7qs87cx8b7420rksxy30vikb")))

(define-public crate-binary_macros-0.5.0 (c (n "binary_macros") (v "0.5.0") (d (list (d (n "binary_macros_impl") (r "^0.5") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "016f6zhk3yhsjhicnl0ykxlxc6phbbx9qig9fypcjzpxypdhjsbi")))

(define-public crate-binary_macros-0.5.1 (c (n "binary_macros") (v "0.5.1") (d (list (d (n "binary_macros_impl") (r "^0.5") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.3") (k 0)))) (h "0d2687ik02r9q83b7w9jf5cjkwma8qydxhmi5n6l6yslr5rzaa56")))

(define-public crate-binary_macros-0.5.2 (c (n "binary_macros") (v "0.5.2") (d (list (d (n "binary_macros_impl") (r "^0.5") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.3") (k 0)))) (h "0xnm6lf5nyq3yjcjhlkf1mw61plzdhjxaibdmyafrwaak1jx2jzf")))

(define-public crate-binary_macros-0.5.3 (c (n "binary_macros") (v "0.5.3") (d (list (d (n "binary_macros_impl") (r "^0.5") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.3.2") (k 0)))) (h "19lfj70fs80b0n6wfny424ibwszh78hnlh2320cb1sw3l176302f")))

(define-public crate-binary_macros-0.5.4 (c (n "binary_macros") (v "0.5.4") (d (list (d (n "binary_macros_impl") (r "^0.5.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.3.2") (k 0)))) (h "09yc8zysx7mfmk0yhx1f9qiyd1blja26f6qfhcm0js0zwhbfrh11")))

(define-public crate-binary_macros-0.6.0 (c (n "binary_macros") (v "0.6.0") (d (list (d (n "binary_macros_impl") (r "^0.6") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.3.2") (k 0)))) (h "15spqjjynjff22d3n9archz1dzb6qk094a4aard49appjv6anrrd")))

(define-public crate-binary_macros-0.6.1 (c (n "binary_macros") (v "0.6.1") (d (list (d (n "binary_macros_impl") (r "^0.6") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.3.2") (k 0)))) (h "170b1q7fvyirmdh50rnif4wb1ylnpnlbxvkgphcjzq1279a3vl2z")))

(define-public crate-binary_macros-0.6.2 (c (n "binary_macros") (v "0.6.2") (d (list (d (n "binary_macros_impl") (r "^0.6") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.3.2") (k 0)))) (h "1lkp3g5gck6044jasmz7r34mnmf2qmci5rv8hcdkbnwhnw35nfd5")))

(define-public crate-binary_macros-0.6.3 (c (n "binary_macros") (v "0.6.3") (d (list (d (n "binary_macros_impl") (r "^0.6") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.3.2") (k 0)))) (h "0zyxgjip3b5s01awarqzc92j3ipp8jb2ygms4c7y0qsi6zf69hfk")))

(define-public crate-binary_macros-1.0.0 (c (n "binary_macros") (v "1.0.0") (d (list (d (n "binary_macros_impl") (r "^1.0.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5.18") (d #t) (k 0)))) (h "0vmyf53ng9mi1yhx0kaivh8jabkip87n6hyxmxh9mjadd07lrw4p")))

