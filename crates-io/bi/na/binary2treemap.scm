(define-module (crates-io bi na binary2treemap) #:use-module (crates-io))

(define-public crate-binary2treemap-0.0.1 (c (n "binary2treemap") (v "0.0.1") (d (list (d (n "addr2line") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "object") (r "^0.32.2") (f (quote ("read"))) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "069gh63nm6w6687b026bp21n1sdmck66mjs3acrhi9zlxphz3z4g")))

(define-public crate-binary2treemap-0.0.2 (c (n "binary2treemap") (v "0.0.2") (d (list (d (n "addr2line") (r "^0.21.0") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "object") (r "^0.32.2") (f (quote ("read"))) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt"))) (d #t) (k 0)))) (h "0q5d3dcm53qn32xqavbmvar36kqz3av90x8vvaivw5hxhxn88qiq")))

