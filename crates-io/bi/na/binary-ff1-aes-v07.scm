(define-module (crates-io bi na binary-ff1-aes-v07) #:use-module (crates-io))

(define-public crate-binary-ff1-aes-v07-0.1.0 (c (n "binary-ff1-aes-v07") (v "0.1.0") (d (list (d (n "aes") (r "^0.7.4") (d #t) (k 2)) (d (n "cipher") (r "^0.3.0") (d #t) (k 0)) (d (n "fpe") (r "^0.4") (d #t) (k 2) (p "fpe-v04")) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "09p65x5xr4fzpam5ynv4z07hy1kv3rgdwgdp0qk4xccm3bf6l7l8") (y #t)))

