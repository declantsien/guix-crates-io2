(define-module (crates-io bi na binary-util-derive) #:use-module (crates-io))

(define-public crate-binary-util-derive-0.1.0 (c (n "binary-util-derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "19zhwq0kiq6m9c152530aw7zhwby5dl4y800k3kxrhvva8sgjvga")))

(define-public crate-binary-util-derive-0.1.1 (c (n "binary-util-derive") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1swwjmcwxad2ifqdfpxxz34rz1kfp07fc4kpnnhsv7mb5p5gq2bj")))

