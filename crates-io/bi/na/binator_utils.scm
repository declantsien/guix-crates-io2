(define-module (crates-io bi na binator_utils) #:use-module (crates-io))

(define-public crate-binator_utils-0.0.0 (c (n "binator_utils") (v "0.0.0") (d (list (d (n "binator_core") (r "^0.0.2") (k 0)) (d (n "binator_core") (r "^0.0.2") (f (quote ("tracing"))) (d #t) (k 2)) (d (n "derive-new") (r "^0.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "109jk1x3zb24wblh27s0ypysprmgm1afwfsvfp849ipbzlcc53y3")))

