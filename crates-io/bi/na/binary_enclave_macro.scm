(define-module (crates-io bi na binary_enclave_macro) #:use-module (crates-io))

(define-public crate-binary_enclave_macro-0.1.0 (c (n "binary_enclave_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wqyyjmnh7m4df6knhirrq865dc1zkdxq3gdka4j2hg6zxlq0agv")))

(define-public crate-binary_enclave_macro-0.1.1 (c (n "binary_enclave_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rykjkyfkc0qyin7ff2cazciczb75qmznd7rbhsv0rbpxl33qqdw")))

