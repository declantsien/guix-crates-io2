(define-module (crates-io bi na binary-search) #:use-module (crates-io))

(define-public crate-binary-search-0.1.0 (c (n "binary-search") (v "0.1.0") (h "125clgaddwas1ykvdd92ji5zsa304klb4d75ffl5q1c5q7yzab6p")))

(define-public crate-binary-search-0.1.1 (c (n "binary-search") (v "0.1.1") (h "13injc0w3wg59cdqphwmcw9s9p3snz86vwv33v52g76jlc3xwd85")))

(define-public crate-binary-search-0.1.2 (c (n "binary-search") (v "0.1.2") (h "0xjp8gls81sq5rmvy85nbkivxz8pc93iz0qdmva08jl310lap85y")))

