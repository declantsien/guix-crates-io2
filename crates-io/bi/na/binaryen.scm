(define-module (crates-io bi na binaryen) #:use-module (crates-io))

(define-public crate-binaryen-0.1.0 (c (n "binaryen") (v "0.1.0") (d (list (d (n "binaryen-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0qy9hmilv3ckfrdd0q5drj93acb61s892nl5nnia3gz0n5625yrm")))

(define-public crate-binaryen-0.2.0 (c (n "binaryen") (v "0.2.0") (d (list (d (n "binaryen-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0lrcnvhw9gr9027h8k56f870vxx62fqhqac57z3hg8dmxn123byk")))

(define-public crate-binaryen-0.3.0 (c (n "binaryen") (v "0.3.0") (d (list (d (n "binaryen-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1k4m0p4jgnn5h69rdjxmflp5fryyd42xfnqxddp9l9zncf6a206v")))

(define-public crate-binaryen-0.4.0 (c (n "binaryen") (v "0.4.0") (d (list (d (n "binaryen-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0rkvb9m2glf0ywcpzldzvlvsmka6j1s9la61klm1a5lad3vhq9qg")))

(define-public crate-binaryen-0.5.0 (c (n "binaryen") (v "0.5.0") (d (list (d (n "binaryen-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0py1d11nlid3v11y2nzqdac4gnmkf0l5a0wbpbfpib1yyncs302c")))

(define-public crate-binaryen-0.6.0 (c (n "binaryen") (v "0.6.0") (d (list (d (n "binaryen-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1igg39n2qw376iax11jmnhnm02jjj0p712v64awpc5pv99za04jx")))

(define-public crate-binaryen-0.7.0 (c (n "binaryen") (v "0.7.0") (d (list (d (n "binaryen-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "wabt") (r "^0.7") (d #t) (k 2)))) (h "17ddwzcm9kk75iy2flsgfmdmip8i25m281zn97z2w0mw9jq4cs8l")))

(define-public crate-binaryen-0.8.0 (c (n "binaryen") (v "0.8.0") (d (list (d (n "binaryen-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "wabt") (r "^0.7") (d #t) (k 2)))) (h "1dxws4wds19iwvrmjgwwxcynih15cwjgbpknr5ir6ndlv5fzaadf") (y #t)))

(define-public crate-binaryen-0.8.1 (c (n "binaryen") (v "0.8.1") (d (list (d (n "binaryen-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "wabt") (r "^0.7") (d #t) (k 2)))) (h "038b399v6c93l1cjlwibdlj6z78zy5b5rnrxfqndn8narjd30b1g")))

(define-public crate-binaryen-0.8.2 (c (n "binaryen") (v "0.8.2") (d (list (d (n "binaryen-sys") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "wabt") (r "^0.9") (d #t) (k 2)))) (h "0a3ijwqcmsmw0pk0hkh2xcnds00qc8f9xwc1jxm17vzqasvh47z8")))

(define-public crate-binaryen-0.9.0 (c (n "binaryen") (v "0.9.0") (d (list (d (n "binaryen-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1hml2kkwxk2xl1xixg7zq4d3zcnh6pyngqji96hmv9gjlmhi5397")))

(define-public crate-binaryen-0.10.0 (c (n "binaryen") (v "0.10.0") (d (list (d (n "binaryen-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1pgj03qfc9mks07ja72n5q2z17l744cj0j59vbcnid3s7hxx46m5")))

(define-public crate-binaryen-0.11.0 (c (n "binaryen") (v "0.11.0") (d (list (d (n "binaryen-sys") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0wxwx2z3mvpc4y7i5a6kxh2mzppwjim37w6ryx1gwgnqnkj04c21") (y #t)))

(define-public crate-binaryen-0.12.0 (c (n "binaryen") (v "0.12.0") (d (list (d (n "binaryen-sys") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "16mw0jyznp9pkcq9ma8xyqav75hl68la7ms1xb31iffha9zl16qs")))

(define-public crate-binaryen-0.12.1 (c (n "binaryen") (v "0.12.1") (d (list (d (n "binaryen-sys") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0ykpz3s7imc3fanri1a5m4j8gmf45v8m9arzn5jsbdkmkl9ylfvq")))

(define-public crate-binaryen-0.13.0 (c (n "binaryen") (v "0.13.0") (d (list (d (n "binaryen-sys") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1rzq4kmz4p19bd4d4424yj7pngzv1khjj9vvws6fry3mfzgpj99d")))

