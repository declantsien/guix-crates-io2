(define-module (crates-io bi na binary-utils) #:use-module (crates-io))

(define-public crate-binary-utils-0.1.0 (c (n "binary-utils") (v "0.1.0") (h "0k5035rcf5l7kdz872sh2zvgwfy50ff0f2jwdqv84k5i96rch4jg")))

(define-public crate-binary-utils-0.1.1 (c (n "binary-utils") (v "0.1.1") (h "1qyhlxpvq6p7nsxryc2rn9hr4dwmb2za735i5hjdi7igiv7dl8mi")))

