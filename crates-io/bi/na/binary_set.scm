(define-module (crates-io bi na binary_set) #:use-module (crates-io))

(define-public crate-binary_set-0.1.0 (c (n "binary_set") (v "0.1.0") (d (list (d (n "idx_file") (r "^0.30") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "01lmf3gq7vz9gw0mb38kcdmxhxsg2w330lfhs3ghly8xw09pxa2m") (y #t)))

(define-public crate-binary_set-0.1.1 (c (n "binary_set") (v "0.1.1") (d (list (d (n "idx_file") (r "^0.31") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "0lr3f7jm0rj6q9zhzw8vw6zjxivfjq75i6cwf48s8vgg53hqs3mv") (y #t)))

(define-public crate-binary_set-0.1.2 (c (n "binary_set") (v "0.1.2") (d (list (d (n "idx_file") (r "^0.32") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "0ip9nkqnayrwq7i8j49c6bkian7ks2qgdldkhw27rd3xlc0bprcf") (y #t)))

(define-public crate-binary_set-0.1.3 (c (n "binary_set") (v "0.1.3") (d (list (d (n "idx_file") (r "^0.33") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "0xcvjgcj7mfyz6r3fby1d2vm2w5rclx28xii7w3pd5v37m6259wi") (y #t)))

(define-public crate-binary_set-0.2.0 (c (n "binary_set") (v "0.2.0") (d (list (d (n "idx_file") (r "^0.34") (d #t) (k 0)) (d (n "various_data_file") (r "^0.11") (d #t) (k 0)))) (h "0janrn96av4ryvjq2rqlmbcgnyrgwr9psz1gx3dg9in5v61r6g8d") (y #t)))

(define-public crate-binary_set-0.3.0 (c (n "binary_set") (v "0.3.0") (d (list (d (n "idx_file") (r "^0.35") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "0qzym9m5mrnhvhnwda9qi4sw4zk5072927gclgwvayxrhlvlg450") (y #t)))

(define-public crate-binary_set-0.3.1 (c (n "binary_set") (v "0.3.1") (d (list (d (n "idx_file") (r "^0.36") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "0r3kvk7zz1x5b02650hhkc2kqvnqx2nkjdzrawrr08hbxrzg31rf")))

(define-public crate-binary_set-0.3.2 (c (n "binary_set") (v "0.3.2") (d (list (d (n "idx_file") (r "^0.37") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1yp28vb7sqsh0056d49wkfi9l86aka47spgv654i6256sf8dw4dm")))

(define-public crate-binary_set-0.3.3 (c (n "binary_set") (v "0.3.3") (d (list (d (n "idx_file") (r "^0.38") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "05dq8bf9xd43jr714hsd7drbfmbrbjlvkvwq0awwjpsa12ayn6d0")))

(define-public crate-binary_set-0.3.4 (c (n "binary_set") (v "0.3.4") (d (list (d (n "idx_file") (r "^0.38") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "0zgsn2xjnyys0zz6wxcrgq8vaxqjf0z9nkahqdhy61i9h7lfbpqc")))

(define-public crate-binary_set-0.3.5 (c (n "binary_set") (v "0.3.5") (d (list (d (n "idx_file") (r "^0.39") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1dz1gk7bnkwhb1kfw5rb14hb08ws876rzfnlcs8qzs0ql3p27hab")))

(define-public crate-binary_set-0.4.0 (c (n "binary_set") (v "0.4.0") (d (list (d (n "idx_file") (r "^0.40") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "15pqdvzgq0jj48ifi3w6fiibs8d473zxkrpq7bdvi4nrdc423x03")))

(define-public crate-binary_set-0.5.0 (c (n "binary_set") (v "0.5.0") (d (list (d (n "idx_file") (r "^0.41") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "0p2gv5948lki094j45z1vam7ycxwcj5spq7pny8pqfc8p6irak3i")))

(define-public crate-binary_set-0.6.0 (c (n "binary_set") (v "0.6.0") (d (list (d (n "idx_file") (r "^0.42") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1wsx8lbygi8zf5flxlfcf5ri4aflw9axbfalx8nvmpi6yfczyinr")))

(define-public crate-binary_set-0.7.0 (c (n "binary_set") (v "0.7.0") (d (list (d (n "idx_file") (r "^0.43") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "1408djq30ahjygii0mdc082hfcqk8n45ilkg73x1jgb86s30985s")))

(define-public crate-binary_set-0.8.0 (c (n "binary_set") (v "0.8.0") (d (list (d (n "idx_file") (r "^0.44") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "0qkfg32hj3ps9jhcj05j7k6kf29v66xmhw7rz0pyiym5c6i3racs")))

(define-public crate-binary_set-0.9.0 (c (n "binary_set") (v "0.9.0") (d (list (d (n "idx_file") (r "^0.45") (d #t) (k 0)) (d (n "various_data_file") (r "^0.12") (d #t) (k 0)))) (h "0ni6fa5w014k9njfi8hinkbzh3hhwfbpj6lh93kq4yykwrnqlxri")))

(define-public crate-binary_set-0.9.1 (c (n "binary_set") (v "0.9.1") (d (list (d (n "idx_file") (r "^0.46") (d #t) (k 0)) (d (n "various_data_file") (r "^0.13") (d #t) (k 0)))) (h "0j3lgmg8gjiv8szb5x3s08fpa8qjg49pxls0ahli31snciy6k7dz")))

(define-public crate-binary_set-0.9.3 (c (n "binary_set") (v "0.9.3") (d (list (d (n "idx_file") (r "^0.46") (d #t) (k 0)) (d (n "various_data_file") (r "^0.13") (d #t) (k 0)))) (h "1h8r2sixl5g49z7i0dvhxj83i8j01czx4f3qmpihgc8sgdimj9vq")))

(define-public crate-binary_set-0.9.4 (c (n "binary_set") (v "0.9.4") (d (list (d (n "idx_file") (r "^0.47.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.13.3") (d #t) (k 0)))) (h "099m35n2kshz5faplf3sbsw4l2m95mfzjd624l5i500z2m5iyfbd")))

(define-public crate-binary_set-0.10.0 (c (n "binary_set") (v "0.10.0") (d (list (d (n "idx_file") (r "^0.47.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.14.0") (d #t) (k 0)))) (h "0rw06q7w05lpz2vnr4qqd6v49v26wbjwhpx1njgzxkpafqsc5yks")))

(define-public crate-binary_set-0.10.1 (c (n "binary_set") (v "0.10.1") (d (list (d (n "idx_file") (r "^0.48.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.14.0") (d #t) (k 0)))) (h "05gi8jkz5f9zcmjkv7s7rhj6c1xjv3sg957b1m0jlynm0mn633l4")))

(define-public crate-binary_set-0.10.2 (c (n "binary_set") (v "0.10.2") (d (list (d (n "idx_file") (r "^0.49.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.14.0") (d #t) (k 0)))) (h "1kagpvbafs69szrc02vid13wasja6zbql2y14pd87vx2020v1zzc")))

(define-public crate-binary_set-0.10.3 (c (n "binary_set") (v "0.10.3") (d (list (d (n "idx_file") (r "^0.49.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.15.0") (d #t) (k 0)))) (h "1r5h37rgq2ylwsrsb8rg4k33sxn6mdjbjvl82nz1lnx91fj8w5yz")))

(define-public crate-binary_set-0.10.4 (c (n "binary_set") (v "0.10.4") (d (list (d (n "idx_file") (r "^0.50.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.16.0") (d #t) (k 0)))) (h "076naqr2q5nls39rngcl2p41v3lfqsl8kjjgjk70sbqf6gd26wra")))

(define-public crate-binary_set-0.11.0 (c (n "binary_set") (v "0.11.0") (d (list (d (n "idx_file") (r "^0.51.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.16.0") (d #t) (k 0)))) (h "1si77z46kcdkw9z6anfp2kzamp1300nww5n2isr4yblbgw4yi15s")))

(define-public crate-binary_set-0.11.1 (c (n "binary_set") (v "0.11.1") (d (list (d (n "idx_file") (r "^0.52.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.16.1") (d #t) (k 0)))) (h "1v8h9g4z3xy6j0rp1i9313bvjwdgy00w7x0d42lxrh847321jm03")))

(define-public crate-binary_set-0.12.0 (c (n "binary_set") (v "0.12.0") (d (list (d (n "idx_file") (r "^0.53.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "061qxmsgxf1rrmhmlf5nxaskip0j491qpk5pr2pw41icicybv33b") (y #t)))

(define-public crate-binary_set-0.12.1 (c (n "binary_set") (v "0.12.1") (d (list (d (n "idx_file") (r "^0.53.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "07b3pvpjzr8ab19583zywgxgmf9k5knig4kk8v2hnhbfzc26n27p") (y #t)))

(define-public crate-binary_set-0.12.2 (c (n "binary_set") (v "0.12.2") (d (list (d (n "idx_file") (r "^0.54.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "089s8xzbd4avgz51qka7wf6rfgkr9nmib6mgzb7sj6amkwwsqwfn") (y #t)))

(define-public crate-binary_set-0.12.3 (c (n "binary_set") (v "0.12.3") (d (list (d (n "idx_file") (r "^0.55.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "00akqdg53kibwlzvir398di5l6v96clxksnacddqg19v53i9p4db") (y #t)))

(define-public crate-binary_set-0.12.4 (c (n "binary_set") (v "0.12.4") (d (list (d (n "idx_file") (r "^0.55.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "1xz8czni6wdqksp3bzn98dxqx0qrqgcv082mvqzpxbxv7nk79fmj")))

(define-public crate-binary_set-0.13.0 (c (n "binary_set") (v "0.13.0") (d (list (d (n "idx_file") (r "^0.56.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "1bgqjf9jks43dx6f1b4138j5h8rh99ykcr4q4ix9q0hgk3i49hj4")))

(define-public crate-binary_set-0.13.1 (c (n "binary_set") (v "0.13.1") (d (list (d (n "idx_file") (r "^0.57.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "1mhkjhrrpy2kilaqq9ddmq3yadb3sdnrsaj9zkg8057pblwa5dpv")))

(define-public crate-binary_set-0.13.2 (c (n "binary_set") (v "0.13.2") (d (list (d (n "idx_file") (r "^0.58.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "093xnx1srmr4ybdvhgqrkkggxd2y3ix0mwix5d6slcsdscpml26l")))

(define-public crate-binary_set-0.14.0 (c (n "binary_set") (v "0.14.0") (d (list (d (n "idx_file") (r "^0.58.0") (d #t) (k 0)) (d (n "various_data_file") (r "^0.17.0") (d #t) (k 0)))) (h "062mn8vjc805pg4ccrzh6sgkncxa06c8dnshn9x7fysz07hvlz0b")))

