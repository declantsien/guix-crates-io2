(define-module (crates-io bi na binary-merge) #:use-module (crates-io))

(define-public crate-binary-merge-0.1.0 (c (n "binary-merge") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1jjqxlqr13njy9q3ln94j0gnji8vzqkfq6h4az2g5ax67d9m3i45")))

(define-public crate-binary-merge-0.1.1 (c (n "binary-merge") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "04fvpzy43bjd7h1sgvm21saawa4dzvh2bnhyq081acirji4fgc7x")))

(define-public crate-binary-merge-0.1.2 (c (n "binary-merge") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1aslcmvvsa8k7zzddlfbji618jvpsylangxjh51nljx5h0fbhysr")))

