(define-module (crates-io bi na binaryutils) #:use-module (crates-io))

(define-public crate-binaryutils-0.1.0 (c (n "binaryutils") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "0jm0pibgnf0v8j7vi0891pnmvmzqq6l48rkmx6l2a151byb26fis") (y #t)))

(define-public crate-binaryutils-0.1.1 (c (n "binaryutils") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "0gp8f3ygbf7d13433g11n8a5vvqxs7cp95qi5g2iyji2ygpsf8hm") (y #t)))

(define-public crate-binaryutils-0.1.2 (c (n "binaryutils") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "0aymgd539yj5hch4zicydcn1s14qkjqpbkms0dnvbzd4swrb2cdh") (y #t)))

(define-public crate-binaryutils-0.1.3 (c (n "binaryutils") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "0p7b6wr466pd7dk47xg2hg0hbsngqy8kf5c5b2dn13vmhacsb1sr") (y #t)))

(define-public crate-binaryutils-0.1.4 (c (n "binaryutils") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "1yjv3zi2fjnr1dfzl95mgzppv73b24569f72b0qip5gihvikxjm7") (y #t)))

(define-public crate-binaryutils-0.1.5 (c (n "binaryutils") (v "0.1.5") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "0v3b0iy79a3cisqknxzgs7ywjiqi71ll0nav6wg1lhir04ljk8ab") (y #t)))

(define-public crate-binaryutils-0.1.6 (c (n "binaryutils") (v "0.1.6") (d (list (d (n "byteorder") (r "^0.3.11") (d #t) (k 0)))) (h "1rj89k0ivdkdqlmn4l25ckfckx7sh431cdp3j3zpkpl8r23yrzxk") (y #t)))

