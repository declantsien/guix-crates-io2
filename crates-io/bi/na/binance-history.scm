(define-module (crates-io bi na binance-history) #:use-module (crates-io))

(define-public crate-binance-history-0.1.0 (c (n "binance-history") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.28.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "09f6idzslsmal47fm86iy2d9amn4psawydf3r4m39nb2bn4ygb73")))

