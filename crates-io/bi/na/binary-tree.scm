(define-module (crates-io bi na binary-tree) #:use-module (crates-io))

(define-public crate-binary-tree-0.0.1 (c (n "binary-tree") (v "0.0.1") (h "16qp76di2gaf9izkcj3ncalx07w0fkvnqnxvms0idsmy6w9p5jfl")))

(define-public crate-binary-tree-0.1.0 (c (n "binary-tree") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0lprj50i1pxsh6nvrd99fz4ip5fl2v8zw64cd90ra8i44y7qd53r") (f (quote (("default"))))))

(define-public crate-binary-tree-0.2.0 (c (n "binary-tree") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1d09v47570ln6qwfy2w8pxgnn6j1jdwjc03mg1mzf9h1awnzwp0r") (f (quote (("qc_tests" "quickcheck" "quickcheck_macros") ("default"))))))

