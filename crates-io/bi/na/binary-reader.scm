(define-module (crates-io bi na binary-reader) #:use-module (crates-io))

(define-public crate-binary-reader-0.1.0 (c (n "binary-reader") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "14izzypvsxb3k19vcxrshvvm10871m13bn8dmqdsznsbvvvjfn76")))

(define-public crate-binary-reader-0.1.1 (c (n "binary-reader") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "1ypbiiwd7irwd9ii6by4w1z3wb07mqmssa61lpbmn31nv9mrsdjp")))

(define-public crate-binary-reader-0.1.2 (c (n "binary-reader") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "0p0lcr174l0cwd632jbhsqp3z7fl196laqwpprssy2l2m1aq8dlm")))

(define-public crate-binary-reader-0.1.3 (c (n "binary-reader") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "0hn0q94b1x0azgdzqf7y64lnv8kanrddl74gva26qdbjbq0hh44q")))

(define-public crate-binary-reader-0.2.0 (c (n "binary-reader") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "0vk46c4pdlfk1gaxcwr1cn2gn5smx4218rq431kjksxqmxhz0r80")))

(define-public crate-binary-reader-0.3.0 (c (n "binary-reader") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "1r0y6pz5n3l2d8ihshf7zhqk2772dqgdk58fz1zvi11fc59cx374")))

(define-public crate-binary-reader-0.4.0 (c (n "binary-reader") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "12xwibqfcx2b3wssq9adkaf23bnzihlg9afbgdkdmj1whf94hmiv")))

(define-public crate-binary-reader-0.4.1 (c (n "binary-reader") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "0qh2ab5bdn0pak0r1dvrl3mb95683rm23q4avdc9ja7i613xn7lx")))

(define-public crate-binary-reader-0.4.2 (c (n "binary-reader") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "0cswrgvzqq0mn51z5157npskbl7k2h7xsmjfkshfjkbvhcz0mn7r")))

(define-public crate-binary-reader-0.4.3 (c (n "binary-reader") (v "0.4.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "1z8cskpqxjy6yva7icczsp75aa627diqp5jim639lyw02ck5yhzb")))

(define-public crate-binary-reader-0.4.4 (c (n "binary-reader") (v "0.4.4") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "114f4iw5n56w79rirhxssr8n2wfmpd7nsqydhqna4kkzdl72wq3q")))

(define-public crate-binary-reader-0.4.5 (c (n "binary-reader") (v "0.4.5") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0piah4v4qhw2w4nxq07yp1v92fkyc5jd84vaxn42ar0xji8kq5qx")))

