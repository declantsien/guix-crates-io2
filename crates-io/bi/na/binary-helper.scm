(define-module (crates-io bi na binary-helper) #:use-module (crates-io))

(define-public crate-binary-helper-0.1.3 (c (n "binary-helper") (v "0.1.3") (h "0nawr1a84nmc3bshh5zvngxcq5rs7bggmhm44iw1lfp5sdrqi5s7")))

(define-public crate-binary-helper-0.2.0 (c (n "binary-helper") (v "0.2.0") (h "1340ailikyrcsb4hg4f2l4lmy9pnf10wi3v2m5ij22lqm20a798n")))

(define-public crate-binary-helper-0.2.1 (c (n "binary-helper") (v "0.2.1") (h "15xzn7xy0hgq759hc1flgwlrdwdxfa8lva27hvk2bad17yrpa8nh")))

(define-public crate-binary-helper-0.2.2 (c (n "binary-helper") (v "0.2.2") (h "1ilpg0bhaa22q090gzbvmd1d3zb3rw2z6gm2xwwrhpb21xk1gwmf")))

(define-public crate-binary-helper-0.2.3 (c (n "binary-helper") (v "0.2.3") (h "0hz6rlg6fklpcz3np8s5q8zsfin1g67q8dch3pgmbhxl1g5q48qr")))

