(define-module (crates-io bi na binary_vec_io) #:use-module (crates-io))

(define-public crate-binary_vec_io-0.1.1 (c (n "binary_vec_io") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "12gsvjn2v558vrpx4xxi0671ry2qx35s4z5368qcnwwg44s7l203")))

(define-public crate-binary_vec_io-0.1.2 (c (n "binary_vec_io") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0vz4v3v6592i2vmql51zsgvqk8n7risiwij8w0n83yw1ra1d6gm4")))

(define-public crate-binary_vec_io-0.1.3 (c (n "binary_vec_io") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0jz6c9r0sqwzq8cchsanb71yibibh1r71hs01x4fzc4naphxijwp")))

(define-public crate-binary_vec_io-0.1.4 (c (n "binary_vec_io") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.9") (d #t) (k 0)))) (h "0lix2klk21lhzbmxn61jajvy8i9455n6cq9cmfalc9l107pjjfbd")))

(define-public crate-binary_vec_io-0.1.5 (c (n "binary_vec_io") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.9") (d #t) (k 0)))) (h "1hkd3addlj933ckba73r8hry3xvcqjik5pb13ajidfnrb1ijsfx4")))

(define-public crate-binary_vec_io-0.1.6 (c (n "binary_vec_io") (v "0.1.6") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.9") (d #t) (k 0)))) (h "1f1spxx2i4cs3cc3yslvw7rbavyzyr789qq2qfzs6k410vzh1y6k")))

(define-public crate-binary_vec_io-0.1.7 (c (n "binary_vec_io") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)))) (h "1vj378l95l9crw06my7xgqv6by7bx336hl3jv05n9xjmrnwjrc3s")))

(define-public crate-binary_vec_io-0.1.8 (c (n "binary_vec_io") (v "0.1.8") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)))) (h "197jambjplwmbmxkd4xg7n9dp288d329l3x52r4cyf85qqi79x30")))

(define-public crate-binary_vec_io-0.1.9 (c (n "binary_vec_io") (v "0.1.9") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)))) (h "1vaqv5yvgk4fmcc0l763wxn1c5a00040vza1dr8q8pzqlpkkn16j")))

(define-public crate-binary_vec_io-0.1.10 (c (n "binary_vec_io") (v "0.1.10") (d (list (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)))) (h "1rxq86w0rgfmgwrmbx3ldmsdkj8x9jks634szzxpkbmiaxjkc12q")))

(define-public crate-binary_vec_io-0.1.11 (c (n "binary_vec_io") (v "0.1.11") (d (list (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)))) (h "1gmcn2a0lysv1rcn066nabgq3h69kj8hdax6293xrnphy2dlpn5y")))

(define-public crate-binary_vec_io-0.1.12 (c (n "binary_vec_io") (v "0.1.12") (d (list (d (n "itertools") (r ">=0.8, <=0.11") (d #t) (k 0)))) (h "0fnhic26dlzn8jjirrwji8klbzaybi17d2i8v9l7blwhv9j6mcp8")))

