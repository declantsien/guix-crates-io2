(define-module (crates-io bi na binate) #:use-module (crates-io))

(define-public crate-binate-0.0.0 (c (n "binate") (v "0.0.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "dashmap") (r "^4.0.2") (d #t) (k 0)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.6") (d #t) (k 0)))) (h "1kzdgnay7wac216xz4lnsmdrf2wkbdjcs5m0jcyi8jhwxib3hj34") (f (quote (("full" "frame") ("frame") ("default"))))))

(define-public crate-binate-0.0.1 (c (n "binate") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "dashmap") (r "^4.0.2") (d #t) (k 0)) (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.6") (d #t) (k 0)))) (h "1ljhgrjxh1ms9l8q36dd8yrlkwcr6rrgsjlqcqn9fk4g1qc7s02j") (f (quote (("full" "frame") ("frame") ("default"))))))

