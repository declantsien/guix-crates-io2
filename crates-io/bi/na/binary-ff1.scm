(define-module (crates-io bi na binary-ff1) #:use-module (crates-io))

(define-public crate-binary-ff1-0.1.0 (c (n "binary-ff1") (v "0.1.0") (d (list (d (n "aes") (r "^0.3.2") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6.2") (d #t) (k 0)) (d (n "fpe") (r "^0.2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num") (r "^0.2.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0cg7yrh9ahzjzvpyrmbk5qbr74w55ac4hk5yixl1322kqz3d6vsw")))

(define-public crate-binary-ff1-0.2.0 (c (n "binary-ff1") (v "0.2.0") (d (list (d (n "aes") (r "^0.7") (d #t) (k 2)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "fpe") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0mzgw8zx1b25z6672yk2h8vmi0dk7p3aifcnwhj85d20w4b6ih0d")))

