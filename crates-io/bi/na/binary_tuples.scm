(define-module (crates-io bi na binary_tuples) #:use-module (crates-io))

(define-public crate-binary_tuples-0.1.0 (c (n "binary_tuples") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0sw7jxpm486zar2kpj29ayyqp27193pfj3xn5xfnv3bd9fl67y8w")))

(define-public crate-binary_tuples-0.1.1 (c (n "binary_tuples") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hakfp52xmmb2230k71yn7qy3691hjwaxqz138p5kvva3xnr4yga")))

