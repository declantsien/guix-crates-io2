(define-module (crates-io bi na binaryhelper) #:use-module (crates-io))

(define-public crate-BinaryHelper-0.1.0 (c (n "BinaryHelper") (v "0.1.0") (h "1nww74mjxh1ah4l9yiyvvn9lszvi17w5zh0k896xy8rp4qdf152z")))

(define-public crate-BinaryHelper-0.1.1 (c (n "BinaryHelper") (v "0.1.1") (h "19p1jlbm7f37y36f46yg6q4sffz339g5cqmy4zrbm9iwpypri664")))

(define-public crate-BinaryHelper-0.1.2 (c (n "BinaryHelper") (v "0.1.2") (h "0sxqfjlbyyf8pyjwkmsa13nz2kj1k2dcsyl95z3hdl1v8ambh4vf")))

(define-public crate-BinaryHelper-0.1.3 (c (n "BinaryHelper") (v "0.1.3") (h "1qsa3sf1z0kmynakcxavpahhw8q6afazlp70nsqq8gkc2mmkm7xh")))

(define-public crate-BinaryHelper-0.2.3 (c (n "BinaryHelper") (v "0.2.3") (h "0aqsbq1c7rd1vxck8rpzm9jj9nrz80qzjgrfjzf3ry5bvyppq1m4")))

