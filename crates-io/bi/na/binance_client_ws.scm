(define-module (crates-io bi na binance_client_ws) #:use-module (crates-io))

(define-public crate-binance_client_ws-0.1.0 (c (n "binance_client_ws") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20") (f (quote ("rustls-tls-webpki-roots"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1s7p765b9gfprqavdfji238gf8narkjsqn97c4x99vrwrnvk73db")))

