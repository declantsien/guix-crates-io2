(define-module (crates-io bi na binary_macros_impl) #:use-module (crates-io))

(define-public crate-binary_macros_impl-0.3.0 (c (n "binary_macros_impl") (v "0.3.0") (d (list (d (n "data-encoding") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "0wiknv3vnqa166ms14s1jyfzs5whapfqx0xfj4a6gdkm0ms6cmzh")))

(define-public crate-binary_macros_impl-0.3.1 (c (n "binary_macros_impl") (v "0.3.1") (d (list (d (n "data-encoding") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "05g3qp5hww84h8jsklc70crb13awb5nn2pfapa9q9v563324l0l1")))

(define-public crate-binary_macros_impl-0.4.0 (c (n "binary_macros_impl") (v "0.4.0") (d (list (d (n "data-encoding") (r "^1.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "1v5f4cg4b1yp4c1mdxzhg1945bavp9y98fgjxz4msczla006lygj")))

(define-public crate-binary_macros_impl-0.5.0 (c (n "binary_macros_impl") (v "0.5.0") (d (list (d (n "data-encoding") (r "^1.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "1m3513nl808gqb09a4fjk2cckmshm9i2q2rp11xg87j3s2jma8g6")))

(define-public crate-binary_macros_impl-0.5.1 (c (n "binary_macros_impl") (v "0.5.1") (d (list (d (n "data-encoding") (r "^1.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "1nx8qgsdi7jj40557d8r51jia5k1z1497n1xghjvi4hi6xf4xxyr")))

(define-public crate-binary_macros_impl-0.5.2 (c (n "binary_macros_impl") (v "0.5.2") (d (list (d (n "data-encoding") (r "^1.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "032pqpm54sl8caa5cc6fghgaz0rk6alabc3nx9vrg1rmr3jc4rwz")))

(define-public crate-binary_macros_impl-0.5.3 (c (n "binary_macros_impl") (v "0.5.3") (d (list (d (n "data-encoding") (r "^1.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3.2") (k 0)))) (h "0ni0f20vg378vhbvxrh47d1wgr1fc84mi10q38rjkbvfy2qdv226")))

(define-public crate-binary_macros_impl-0.6.0 (c (n "binary_macros_impl") (v "0.6.0") (d (list (d (n "data-encoding") (r "^1.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3.2") (k 0)))) (h "0haszjh5j4lksxchmi2d9na8y9v91v4d55fzw6ap8yvsj4z24cjp")))

(define-public crate-binary_macros_impl-1.0.0 (c (n "binary_macros_impl") (v "1.0.0") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.16") (d #t) (k 0)))) (h "0gynaibsgvh29l9syz9hnv606rk8h29mvfsh5frfg2r3pnbqn4af")))

