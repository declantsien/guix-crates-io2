(define-module (crates-io bi na binaryen_capi-sys) #:use-module (crates-io))

(define-public crate-binaryen_capi-sys-116.0.0 (c (n "binaryen_capi-sys") (v "116.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "15jbhz37hmavg0v8gqflywysmqrji13najv4hnc3vhs0h19ydb7g") (y #t)))

(define-public crate-binaryen_capi-sys-0.116.1 (c (n "binaryen_capi-sys") (v "0.116.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "0bkkcmqv1s02fp6fg54gqczqna67q31krm2yva2fx6nrgabcfaxp")))

(define-public crate-binaryen_capi-sys-0.117.0 (c (n "binaryen_capi-sys") (v "0.117.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "0n4xa36vf7qd8lv4d05g3z8ildq8dq7b2l9019cknd3g2d9av45x")))

