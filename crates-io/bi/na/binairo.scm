(define-module (crates-io bi na binairo) #:use-module (crates-io))

(define-public crate-binairo-0.1.0 (c (n "binairo") (v "0.1.0") (h "1mpj4613j8x3j20qy6vkh5izj18pfipbkw907jafmxflvlv5g4ln")))

(define-public crate-binairo-0.1.1 (c (n "binairo") (v "0.1.1") (h "0m2g7y1zr3378rmq14pf7qlrpqjy3gaj0xncm8kwvnqqy3dyzygi")))

