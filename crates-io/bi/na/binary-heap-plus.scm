(define-module (crates-io bi na binary-heap-plus) #:use-module (crates-io))

(define-public crate-binary-heap-plus-0.1.0 (c (n "binary-heap-plus") (v "0.1.0") (h "1wp8hl1dj4svwrnki75qg7g0gnpp2syqbicdaykqypkm4sa8q5z7")))

(define-public crate-binary-heap-plus-0.1.1 (c (n "binary-heap-plus") (v "0.1.1") (h "0dwcd9hv8dda27qf50yrhz660dpd17axfcj79p0wvb4dajsmz6b0")))

(define-public crate-binary-heap-plus-0.1.2 (c (n "binary-heap-plus") (v "0.1.2") (h "0afkm13p1pnr0qdw435d7d62h5pwj78fx648yxg2r243ir0ihaj5")))

(define-public crate-binary-heap-plus-0.1.3 (c (n "binary-heap-plus") (v "0.1.3") (h "0fb9rs594852f9gxnqcgkynpd1iwkdsagadlrrq90li98512vik5")))

(define-public crate-binary-heap-plus-0.1.4 (c (n "binary-heap-plus") (v "0.1.4") (h "0v5s111fmdmmrdnlkcfjg4azf4idis55rhzkq0g28rf6mfsv3bgr")))

(define-public crate-binary-heap-plus-0.1.5 (c (n "binary-heap-plus") (v "0.1.5") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0a3lq56k9hc7ni7w8k1ixp9773lsclch1s674k1a37x0g2gsaanq") (f (quote (("serde1" "serde" "serde_derive"))))))

(define-public crate-binary-heap-plus-0.1.6 (c (n "binary-heap-plus") (v "0.1.6") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0xqxsajmg6q0qgwncxqywh3jwd80nl8x82pwy6zwwjqs1vq8jn41") (f (quote (("serde1" "serde" "serde_derive"))))))

(define-public crate-binary-heap-plus-0.2.0 (c (n "binary-heap-plus") (v "0.2.0") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "14mfbj1fckn21h1ixc2miyd0ryhvmhm2mxy6wp1jlm4n2nhlfzk2")))

(define-public crate-binary-heap-plus-0.3.0 (c (n "binary-heap-plus") (v "0.3.0") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "1kq0vxzdbmfx9f6ibnm9lffqliqvqblnx26k21r9sfxqaiakvhsx")))

(define-public crate-binary-heap-plus-0.3.1 (c (n "binary-heap-plus") (v "0.3.1") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "031zs59zmzya9d8jpp34ha25ykbz4kp9qnh9k39sj2xdi4yl1n3z")))

(define-public crate-binary-heap-plus-0.4.0 (c (n "binary-heap-plus") (v "0.4.0") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "0xmb3jvkgnsrvqhgfppc8ipqgi0qpbnwfncqisnsfpv0qi1f62lh")))

(define-public crate-binary-heap-plus-0.4.1 (c (n "binary-heap-plus") (v "0.4.1") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "1mq6qs7lyckjjazj7apcndzhwhgz3r0nmrk1jf5137pzz0w8c1jg")))

(define-public crate-binary-heap-plus-0.5.0 (c (n "binary-heap-plus") (v "0.5.0") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)))) (h "15hjy7gd3223zwvdaiwlw2dnd23rfw1bdzzhs30fq4g9ha1ismg4") (r "1.56.0")))

