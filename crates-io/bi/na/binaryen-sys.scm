(define-module (crates-io bi na binaryen-sys) #:use-module (crates-io))

(define-public crate-binaryen-sys-0.1.0 (c (n "binaryen-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "0b9k1ivixa8i7vmdm9z5x4bc7557gwlxgms4r3h3vxz3mcxrn2n4")))

(define-public crate-binaryen-sys-0.2.0 (c (n "binaryen-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "1q9bp6l2c99ljiznqbbwdwapl1a80k2w4gy8b0qr72ll58n8044p")))

(define-public crate-binaryen-sys-0.3.0 (c (n "binaryen-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "1z9blrb1ng51nxpfz7gv9m99s4793d9c35gbg0vb6hx86ag48jdg")))

(define-public crate-binaryen-sys-0.4.0 (c (n "binaryen-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "1gryr5y5vinjffvqb1p19z58za7xhx6i0lj3mym1z7lq8205l084")))

(define-public crate-binaryen-sys-0.5.0 (c (n "binaryen-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "140myc55rq5ingf9a8bxnssh1zkr18k4pl1r2kdkafy0h4fi61g4")))

(define-public crate-binaryen-sys-0.6.0 (c (n "binaryen-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "0i4ws8vri8inak9c0da6k0w31xx831fhr1h5p3ld11wxzlla4dp2")))

(define-public crate-binaryen-sys-0.7.0 (c (n "binaryen-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0xrzzwxwjlwkj5dgsgc6qpfb9rn724a1qgx1lfi8krgnm6lb1phw")))

(define-public crate-binaryen-sys-0.8.0 (c (n "binaryen-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "10pb5k4qc5mszlkfihpwdxs93ivi7pcy2iz0chd84rgn9sv7a3ks")))

(define-public crate-binaryen-sys-0.8.1 (c (n "binaryen-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "06v3nzr0l5k76p6jlypigzd2vxlkxnchdaw3a0yfvs4lk81fpkj6")))

(define-public crate-binaryen-sys-0.8.2 (c (n "binaryen-sys") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0i6fyi9aw1iwm828hxijnm8x0wxs2n752cr5vn13jfqwr85n03a4")))

(define-public crate-binaryen-sys-0.9.0 (c (n "binaryen-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0c5568091lsf26jr0izw8z8np4idks7r95146xa8qh6cqdx3f7kb")))

(define-public crate-binaryen-sys-0.10.0 (c (n "binaryen-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1ia8ihz4fqvjnrxyyihxbxj7y12afv5d759j4h6inj6gln6k2gh2")))

(define-public crate-binaryen-sys-0.10.1 (c (n "binaryen-sys") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 2)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "12nj36isa2wa3af49rf6fdfn72plzm6lww36i3jjfy4zr2kjjn6z")))

(define-public crate-binaryen-sys-0.11.0 (c (n "binaryen-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 2)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0m28qp4n3pfs3jn1xl665p1i97dbcm91chwamrmlhr2v46hdx3hs") (y #t)))

(define-public crate-binaryen-sys-0.12.0 (c (n "binaryen-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 2)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0qd5lp29r0jbzdr49fsm21y6h91pkrdc6affbps2sbxr05nn7sc6")))

(define-public crate-binaryen-sys-0.13.0 (c (n "binaryen-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 2)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0szbif6dclxa1vmbd0y3h9g4g6sckgl2v1rzyg8dxjnb45gvq485")))

