(define-module (crates-io bi na binary-heap-plus2) #:use-module (crates-io))

(define-public crate-binary-heap-plus2-0.3.1 (c (n "binary-heap-plus2") (v "0.3.1") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "1nml72sf2ngdnfds8ak86nb0j8y2r5jismq94xf2yly0gm3ar7zy") (y #t)))

(define-public crate-binary-heap-plus2-0.3.2 (c (n "binary-heap-plus2") (v "0.3.2") (d (list (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 2)))) (h "0lig3bcak8chg3974jzg5abgjy5g25hzcbdx85riazjj1sgwxlb4") (y #t)))

