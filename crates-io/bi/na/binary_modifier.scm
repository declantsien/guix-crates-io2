(define-module (crates-io bi na binary_modifier) #:use-module (crates-io))

(define-public crate-binary_modifier-0.2.1 (c (n "binary_modifier") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "101fqshi3jplnhw0zswzhjk80jb9fl5gmddvdych63sz0g8q8hl0")))

(define-public crate-binary_modifier-0.3.2 (c (n "binary_modifier") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0laydrf1damhgsjh70792ppbc4czv0bdz9s3hb1629h5anvrjzcm") (y #t)))

(define-public crate-binary_modifier-0.3.3 (c (n "binary_modifier") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1rfpr1vvhy54yl2zl2rldp00906jhf4jbm4sf6q1hjvmdnb9m9bh")))

