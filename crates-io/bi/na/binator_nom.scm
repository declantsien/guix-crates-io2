(define-module (crates-io bi na binator_nom) #:use-module (crates-io))

(define-public crate-binator_nom-0.0.0 (c (n "binator_nom") (v "0.0.0") (d (list (d (n "binator_core") (r "^0.0.2") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "14bjd0nbrk5i52b1arq6mcqcq5ywfjly1rfhx75y0plla7rys5yh")))

(define-public crate-binator_nom-0.0.1 (c (n "binator_nom") (v "0.0.1") (d (list (d (n "binator") (r "^0.3.0") (d #t) (k 0)) (d (n "binator") (r "^0.3.0") (d #t) (k 2)) (d (n "derive-new") (r "^0.5") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0gri5j6pv241yrw26l23rw4r8bdd0mlb64bc6j9bbxgx4izn6gka")))

