(define-module (crates-io bi na binarystream) #:use-module (crates-io))

(define-public crate-binarystream-1.1.1 (c (n "binarystream") (v "1.1.1") (d (list (d (n "napi") (r "^2.12.2") (f (quote ("napi8"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.12.2") (d #t) (k 0)))) (h "1cbdqqxgpxb4wyd07pl4r5km973f0h2i4lzb6ph4n1si2f9nd80z")))

(define-public crate-binarystream-1.1.2 (c (n "binarystream") (v "1.1.2") (d (list (d (n "napi") (r "^2.12.2") (f (quote ("napi8"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.12.2") (d #t) (k 0)))) (h "13090qh8xapx50p8y3b0qh4xbmqffd3cjaz67dmrvv3pa2fmwmg5")))

(define-public crate-binarystream-1.1.3 (c (n "binarystream") (v "1.1.3") (d (list (d (n "napi") (r "^2.12.2") (f (quote ("napi8"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.12.2") (d #t) (k 0)))) (h "0zmmyrpxbpw1i82pc9dnm59n86rsmgapckh07k9spg3rba3h0vrp")))

(define-public crate-binarystream-1.1.4 (c (n "binarystream") (v "1.1.4") (d (list (d (n "napi") (r "^2.12.2") (f (quote ("napi8"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.12.2") (d #t) (k 0)))) (h "0i8iqhdgrqggan2shxvpyv3k2v1xdpw7yd9a6gwrzl6kkakimjv4")))

(define-public crate-binarystream-1.2.0 (c (n "binarystream") (v "1.2.0") (d (list (d (n "napi") (r "^2.12.2") (f (quote ("napi8"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.12.2") (d #t) (k 0)))) (h "1qh99x158xxwxyam8iyqj53kz0q9vcjs9n2mk3d06p6qk2dfklal")))

(define-public crate-binarystream-1.2.1 (c (n "binarystream") (v "1.2.1") (d (list (d (n "napi") (r "^2.12.2") (f (quote ("napi8"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.12.2") (d #t) (k 0)))) (h "0xwjq07y0227s678106bd1dyxr3xj30vmrdz4j5ylb4agh5yhnww")))

(define-public crate-binarystream-1.2.2 (c (n "binarystream") (v "1.2.2") (d (list (d (n "napi") (r "^2.12.2") (f (quote ("napi8"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.12.2") (d #t) (k 0)))) (h "1wgidb5d0ld381vspz9q773rimdg7pgj999llajg9rhr28h92xvq")))

(define-public crate-binarystream-1.2.3 (c (n "binarystream") (v "1.2.3") (d (list (d (n "napi") (r "^2.12.2") (f (quote ("napi8"))) (k 0)) (d (n "napi-build") (r "^2.0.1") (d #t) (k 1)) (d (n "napi-derive") (r "^2.12.2") (d #t) (k 0)))) (h "10kgjy40jbkhg0a9lqppv5hqx2rdpgy7k7zimavv9nd7jkcnzaq0")))

