(define-module (crates-io bi na binary-util) #:use-module (crates-io))

(define-public crate-binary-util-0.3.0 (c (n "binary-util") (v "0.3.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "codegen") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "18pbwn4nj2l6khcvwldrfpxsc8c6aglc7jclzjg6x6a5ffjq4anm") (f (quote (("default" "codegen"))))))

(define-public crate-binary-util-0.3.1 (c (n "binary-util") (v "0.3.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "codegen") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1ml0adqb7qxy4s9i23qlq0m0xa1mvkxl3fv8ywkb84m6qfn82g8v") (f (quote (("default" "codegen"))))))

(define-public crate-binary-util-0.3.1-rc.1 (c (n "binary-util") (v "0.3.1-rc.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "codegen") (r "^0.1.0") (d #t) (k 0)))) (h "03m555rfsx3fp0abjzyphn5nmcv0jzfdab72v52f887saksx5xza") (y #t)))

(define-public crate-binary-util-0.3.1-rc.2 (c (n "binary-util") (v "0.3.1-rc.2") (d (list (d (n "binary-util-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)))) (h "1f5kgmfaj44ahr4sqjimmz6d47d09wkfs2bxrs76cigz9ijkns3g") (y #t)))

(define-public crate-binary-util-0.3.2 (c (n "binary-util") (v "0.3.2") (d (list (d (n "binary-util-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)))) (h "18czi492d5h45h4d23a35ygqr9gshmgxxgqls53sv9wf2dvai0k6")))

(define-public crate-binary-util-0.3.3 (c (n "binary-util") (v "0.3.3") (d (list (d (n "binary-util-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)))) (h "10ng9nry87zfdl1g77p49r4gzi24k3hyff4h2qpj5lkamxva2cz1")))

(define-public crate-binary-util-0.3.4 (c (n "binary-util") (v "0.3.4") (d (list (d (n "binary-util-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)))) (h "0gywq4wpz9shqanwpsbjh2bwfbq55ry5949gzm3y788p06gmjbqb")))

