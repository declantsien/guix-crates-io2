(define-module (crates-io bi na binary_serde_macros) #:use-module (crates-io))

(define-public crate-binary_serde_macros-0.1.0 (c (n "binary_serde_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1gxiz4icg4d3cw45hd2h7jidzz4jscba04slxbnvmq02c4jcxrrj")))

(define-public crate-binary_serde_macros-0.1.2 (c (n "binary_serde_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "181cddc0a0gzgr7i75r89b1kd9sxm1ql78nkxi5ixi53ry2gj0yk")))

(define-public crate-binary_serde_macros-0.1.3 (c (n "binary_serde_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0abinhnsy31vz3i2q9hv0mmp3dw06xmh3vn9a0wbj6n887l8s0c8")))

(define-public crate-binary_serde_macros-0.1.4 (c (n "binary_serde_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1775bnxhn9qq3p1j9l01d5qmkx9zb6ajn4r3w27rw6fm437qw14s")))

(define-public crate-binary_serde_macros-0.1.5 (c (n "binary_serde_macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "16wr6kq851hxrp776wl4w4p4k24rrx65g14phdfbn0dw97y0irbr")))

(define-public crate-binary_serde_macros-0.1.6 (c (n "binary_serde_macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1bkjamk0h9w8nxgzcbzlxvyw0ddrb9yx66rs4i45inscwx6rq59l")))

(define-public crate-binary_serde_macros-1.0.0 (c (n "binary_serde_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "06xiykwsmiz9z7llbarni02bj6x27dpn2k498x2k7wh6z823zq1g")))

(define-public crate-binary_serde_macros-1.0.1 (c (n "binary_serde_macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "07rif2icjiz92psccc5a5kzmhhj2bq3bpf75db7bykcpq969sn1a")))

(define-public crate-binary_serde_macros-1.0.2 (c (n "binary_serde_macros") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1l4pbywgmgqwz17hlhbwc4phnc4rdg3zld1wb6php80mfblm9j5n")))

(define-public crate-binary_serde_macros-1.0.3 (c (n "binary_serde_macros") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0jbik78pa206bzyif4amrf455164f6kxf3iyv4dkk0wx4vp5j5yw")))

(define-public crate-binary_serde_macros-1.0.4 (c (n "binary_serde_macros") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0665ql0gk9xpki05sdd1hj2i8iz2cpm1wjs7i3vhcbb3yv5j832w")))

(define-public crate-binary_serde_macros-1.0.5 (c (n "binary_serde_macros") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1x06n57r8y449fjz4971598n7z4gmh075d0k2hf29mvfpw7jfgwl")))

(define-public crate-binary_serde_macros-1.0.6 (c (n "binary_serde_macros") (v "1.0.6") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0wxbh8kqhm9afaznmjfljnx3nnzkxr183gcly7bqf93n0nnrp8gk")))

(define-public crate-binary_serde_macros-1.0.7 (c (n "binary_serde_macros") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0ilgnvy9vs82apf4il1yb7liidgg5s9bpl684w1r67q5mnbfg950")))

(define-public crate-binary_serde_macros-1.0.8 (c (n "binary_serde_macros") (v "1.0.8") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0qjdjqqbg54zgzf3gipcacrxi1szr4bhfa73m6hk29zk50bja7ra")))

(define-public crate-binary_serde_macros-1.0.9 (c (n "binary_serde_macros") (v "1.0.9") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1x75rj4n19k03ysqkkjapjrd5ihm091mxzzkw08yxcwsv7ayxggr")))

(define-public crate-binary_serde_macros-1.0.10 (c (n "binary_serde_macros") (v "1.0.10") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1zj9rzn89yn594qbpmf9g7n353zy5zyk77ixgd886dqzi4kf143d")))

(define-public crate-binary_serde_macros-1.0.11 (c (n "binary_serde_macros") (v "1.0.11") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0sj4k3s6v71m0wmviqh6f1di27h1zhdnm5fr0kz783hzxwi88lfl")))

(define-public crate-binary_serde_macros-1.0.12 (c (n "binary_serde_macros") (v "1.0.12") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0sl58yqss7ppwdpg27zp533a33afb69x1xmi2wn61mxk0mcw16b4")))

(define-public crate-binary_serde_macros-1.0.13 (c (n "binary_serde_macros") (v "1.0.13") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "00pij8bvr8mrmd4ahrjjw72s5dmznbvhimkzc9sc9nxbdg5qll3l")))

(define-public crate-binary_serde_macros-1.0.14 (c (n "binary_serde_macros") (v "1.0.14") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1macapr96nm7dvjmy58jakwr0bv0b99zgazab4v9wpxmjl1n1dv6")))

(define-public crate-binary_serde_macros-1.0.15 (c (n "binary_serde_macros") (v "1.0.15") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1j70lr2a1qr4f4ddy5xz27kn9wxpkn63iwr7ygvplgnbl5fa4x6q")))

(define-public crate-binary_serde_macros-1.0.16 (c (n "binary_serde_macros") (v "1.0.16") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0j76zgmxmq4ikldgxj3p5hpz4h336x4330sw0bfs6p9i0mbhvk1d")))

(define-public crate-binary_serde_macros-1.0.17 (c (n "binary_serde_macros") (v "1.0.17") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1g09bn0l3zapp3s79f5x321ic4bv1cs9qwa70v3f8i5mb6404kn4")))

(define-public crate-binary_serde_macros-1.0.18 (c (n "binary_serde_macros") (v "1.0.18") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1v44r3bsdzbmyhjvw0vlk7vq9m1816ivc63fpjv3c4bvq3is0r4b")))

(define-public crate-binary_serde_macros-1.0.19 (c (n "binary_serde_macros") (v "1.0.19") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "11yyhxipwbmn7khv1l78lr1s5iwc5i8g4phg1kd00vbii0g31szc")))

(define-public crate-binary_serde_macros-1.0.20 (c (n "binary_serde_macros") (v "1.0.20") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1h3bn52kls573yg74g0yqdjq4p6h4x8lxazj7pn14qijfjhfri6y")))

(define-public crate-binary_serde_macros-1.0.21 (c (n "binary_serde_macros") (v "1.0.21") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "04qy4xz2nl07lfa0zw1y509csxf6baa8wnzd0z753cb95hk6kkzj")))

(define-public crate-binary_serde_macros-1.0.22 (c (n "binary_serde_macros") (v "1.0.22") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0c8ndl02qx2fkn3pv4b5w6s37cdbkcmyalpyrbs2iyx2gjxz02sp")))

(define-public crate-binary_serde_macros-1.0.23 (c (n "binary_serde_macros") (v "1.0.23") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1lw01600vr5i0cic5gm41rp7wfsqhf507imfdl1nnbx4lidbsm4w")))

(define-public crate-binary_serde_macros-1.0.24 (c (n "binary_serde_macros") (v "1.0.24") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0pshp2liv1m0gd2v1dr9xahj8jmh3qnzfvn6vpw66v9nw71dgbax")))

