(define-module (crates-io bi na binary_search_tree) #:use-module (crates-io))

(define-public crate-binary_search_tree-0.1.0 (c (n "binary_search_tree") (v "0.1.0") (h "1qr7ianrdc6fil1i3qyvrb32ydx8p1wbxkkrfrgwr02m1abxs3za")))

(define-public crate-binary_search_tree-0.1.1 (c (n "binary_search_tree") (v "0.1.1") (h "0fbi470zd9kflz0wlwxyk3xlr89gnnzblpm1md02r077dgwhbgmi")))

(define-public crate-binary_search_tree-0.2.0 (c (n "binary_search_tree") (v "0.2.0") (h "03gdrcdim8pyzxanybvpq2j5s862azsmi2la2clrgb82sc9cd7wz")))

(define-public crate-binary_search_tree-0.2.1 (c (n "binary_search_tree") (v "0.2.1") (h "19yq80p30f516d8gnnq48099z76aikgmsw31ywwh71v8mm89i0ng") (y #t)))

(define-public crate-binary_search_tree-0.2.2 (c (n "binary_search_tree") (v "0.2.2") (h "0k4ygq8bp4cf8rsh7i224di61wwbp1xv1ggsaqi2y5lad5wla7ms")))

