(define-module (crates-io bi na binary_sort_tree) #:use-module (crates-io))

(define-public crate-binary_sort_tree-0.1.0 (c (n "binary_sort_tree") (v "0.1.0") (h "184qi6cwydqxk1kscw5zily730h9pxqqypmwrg75q4w6msb069sy")))

(define-public crate-binary_sort_tree-0.1.1 (c (n "binary_sort_tree") (v "0.1.1") (h "1z6jbanlzdn6phicz0xg1a5wjqv4pik3ad0fkmsqvj4vdsr8z6jv")))

