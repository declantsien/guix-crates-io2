(define-module (crates-io bi na binance-api) #:use-module (crates-io))

(define-public crate-binance-api-0.1.0 (c (n "binance-api") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1b6ldx3vkz6c5qgcjzsacnv6v5jjprqzh4p2lzsvza95cmp4i21l")))

