(define-module (crates-io bi na binator_stream) #:use-module (crates-io))

(define-public crate-binator_stream-0.0.0 (c (n "binator_stream") (v "0.0.0") (d (list (d (n "binator_core") (r "^0.0.2") (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1s4izwkcd0w6synfcikmp1sfill24wbhbpxwxbrlawzi86fkggvm") (f (quote (("std" "alloc") ("default" "bit_stream" "std") ("bit_stream" "num-traits") ("alloc"))))))

