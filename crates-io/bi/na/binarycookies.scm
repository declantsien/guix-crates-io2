(define-module (crates-io bi na binarycookies) #:use-module (crates-io))

(define-public crate-binarycookies-0.1.0 (c (n "binarycookies") (v "0.1.0") (h "0hvcn9prmdjga4v7fl9b5wqyqlbnh6n6ypc6s1c143byipd9ysr0")))

(define-public crate-binarycookies-0.1.1 (c (n "binarycookies") (v "0.1.1") (h "14hicmi023109wm27f3acd852187zlz476gi90345dyivnpy3sb6")))

(define-public crate-binarycookies-0.1.2 (c (n "binarycookies") (v "0.1.2") (h "1may70c9qnnzm5s42q743zysa249qf48frkiwakgxksva609a3z8")))

