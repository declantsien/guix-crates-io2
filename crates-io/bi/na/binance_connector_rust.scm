(define-module (crates-io bi na binance_connector_rust) #:use-module (crates-io))

(define-public crate-binance_connector_rust-0.1.0 (c (n "binance_connector_rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c7azxkf9ylg8chf6dglrhnxl6gn7aa0wwax2nl7nc7k7visybhb")))

(define-public crate-binance_connector_rust-0.1.1 (c (n "binance_connector_rust") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.25.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zfkvdvaj5b2kan4m2d6zb6zyb10ycx8n04hanln14hahvsbv091")))

