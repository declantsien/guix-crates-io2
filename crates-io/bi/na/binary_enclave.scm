(define-module (crates-io bi na binary_enclave) #:use-module (crates-io))

(define-public crate-binary_enclave-0.1.0 (c (n "binary_enclave") (v "0.1.0") (d (list (d (n "binary_enclave_macro") (r "=0.1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "goblin") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12qi8afax4cnb1adpljg93nkmxlzjdd0272b4jaq4vrns1w6ya8p")))

(define-public crate-binary_enclave-0.1.1 (c (n "binary_enclave") (v "0.1.1") (d (list (d (n "binary_enclave_macro") (r "=0.1.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "goblin") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hh2dx1syac89s9cmap39v2hrk0yhkjs60bl308dhpsbyp25xhbs")))

