(define-module (crates-io bi na binarized_image_object_detection) #:use-module (crates-io))

(define-public crate-binarized_image_object_detection-0.1.0 (c (n "binarized_image_object_detection") (v "0.1.0") (d (list (d (n "carrot_image_utils") (r "^0.1.5") (d #t) (k 0)) (d (n "carrot_utils") (r "^0.1.6") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "068jm2lzkycxdhx9ldb515s75b6z6brzcwdvbbi09xxji6nii0jz")))

