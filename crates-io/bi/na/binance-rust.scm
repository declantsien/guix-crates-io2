(define-module (crates-io bi na binance-rust) #:use-module (crates-io))

(define-public crate-binance-rust-0.1.0 (c (n "binance-rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "04lpd1p96j3v51dsmxpcbz6n7cs7fd9dyi60dzqsb7brjqv3xdpf")))

(define-public crate-binance-rust-0.1.1 (c (n "binance-rust") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0ca4ifd58w8g1f31w1w3m1mg6p8g9c8jki6fils40af75f3c81p8")))

(define-public crate-binance-rust-0.1.2 (c (n "binance-rust") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0njxzj80rf92r7jzcjv7kslgh2priqz2vi0dbj8j44fl3raqaf4y")))

