(define-module (crates-io bi na binascii) #:use-module (crates-io))

(define-public crate-binascii-0.1.0 (c (n "binascii") (v "0.1.0") (h "1vnn6c1pwm22kdghf898magcy49kvzq4wc40mwqjsa8ahp4akf1f") (y #t)))

(define-public crate-binascii-0.1.1 (c (n "binascii") (v "0.1.1") (h "0r4gzh10cdrlrf56sbb2rd5hfi467iirbhmvzid4mi9425nl56nd")))

(define-public crate-binascii-0.1.2 (c (n "binascii") (v "0.1.2") (h "0c03gizydpycbqbhhq0l7x7rk88i9yka3bz8w2w4310fzq4nrj9x")))

(define-public crate-binascii-0.1.3 (c (n "binascii") (v "0.1.3") (h "1xpfvmbz5fx3sixfc2mdabps9ksbbgvwj6p64jq9zxnnhfagalmw")))

(define-public crate-binascii-0.1.4 (c (n "binascii") (v "0.1.4") (h "0wnaglgl72pn5ilv61q6y34w76gbg7crb8ifqk6lsxnq2gajjg9q") (f (quote (("encode") ("default" "encode" "decode") ("decode"))))))

