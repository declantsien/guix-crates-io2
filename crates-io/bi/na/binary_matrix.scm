(define-module (crates-io bi na binary_matrix) #:use-module (crates-io))

(define-public crate-binary_matrix-0.1.0 (c (n "binary_matrix") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1lvgnx3hnjph4hiigi5l4zvm5k39aly7m986y7z7smxrir5q48vb") (f (quote (("simd") ("default" "rand")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-binary_matrix-0.1.1 (c (n "binary_matrix") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0i52qpiypv64qc97w43k11hzj4fdi3wy13k9a645xj95skflzzcj") (f (quote (("simd") ("default")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-binary_matrix-0.1.2 (c (n "binary_matrix") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0l4i9rs8zrxq4iipwm6qh9hq4ga8izwnyw048zsnsnnijnagwx1b") (f (quote (("simd") ("default")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-binary_matrix-0.1.3 (c (n "binary_matrix") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1sf0ns3xikp58zw6fmy106zzp8sc9lp9gpzv9xh4r5j39g0gvbls") (f (quote (("simd") ("default")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-binary_matrix-0.1.4 (c (n "binary_matrix") (v "0.1.4") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1qivb2myzdmnja7pdf950gsdn867jrg4y9bnc0ws0smzfr3hjh8w") (f (quote (("simd") ("default")))) (s 2) (e (quote (("rand" "dep:rand"))))))

