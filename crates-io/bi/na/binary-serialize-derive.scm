(define-module (crates-io bi na binary-serialize-derive) #:use-module (crates-io))

(define-public crate-binary-serialize-derive-0.0.0 (c (n "binary-serialize-derive") (v "0.0.0") (d (list (d (n "syn-helpers") (r "^0.4.2") (d #t) (k 0)))) (h "009hpqqgr168qwgbayvjynh5g2gxfwcpfspa70wf4cj2x7jicp8c")))

(define-public crate-binary-serialize-derive-0.0.1 (c (n "binary-serialize-derive") (v "0.0.1") (d (list (d (n "syn-helpers") (r "^0.4.2") (d #t) (k 0)))) (h "0m02wzwvy82l3yl2zp2q86ag4widzy0g75dgw0xckxa2wbf1bmp3")))

