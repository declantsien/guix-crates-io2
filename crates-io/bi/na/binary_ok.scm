(define-module (crates-io bi na binary_ok) #:use-module (crates-io))

(define-public crate-binary_ok-0.1.0 (c (n "binary_ok") (v "0.1.0") (h "07wwbkf1qvggf73m6f8sy46xjm94nd89ipqi65dd8vrdps9fh4ic") (y #t)))

(define-public crate-binary_ok-0.1.1 (c (n "binary_ok") (v "0.1.1") (h "0zijadlyhpkbjkl0avpf05ghhyfb0g2x1sr19gyw0ql6ycw2v8la")))

(define-public crate-binary_ok-0.2.0 (c (n "binary_ok") (v "0.2.0") (h "1zmvvghracqwjbd1wivf6q8qfj5r6k8f42gismybvqmpfklq5nwd")))

(define-public crate-binary_ok-0.2.1 (c (n "binary_ok") (v "0.2.1") (h "0b373bhyxhz6a092rs6idmsj447jaxqk7r3357ph0drqajfwg6d9")))

