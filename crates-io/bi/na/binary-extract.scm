(define-module (crates-io bi na binary-extract) #:use-module (crates-io))

(define-public crate-binary-extract-0.1.0 (c (n "binary-extract") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1vjaww29z05vmdcfc4g0nwjmxbmg9igdabqagc53dwpqig99mlkr")))

(define-public crate-binary-extract-0.2.0 (c (n "binary-extract") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0wnlzmpbfriiws1i3wn7s4irp3dgvpy515g4l2i0x5crlfaxicdr")))

(define-public crate-binary-extract-0.2.1 (c (n "binary-extract") (v "0.2.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "092ikscwwwl1i5mcqg19vcdydfymycq2rcfbmdvpysmm61f71ln8")))

(define-public crate-binary-extract-0.2.2 (c (n "binary-extract") (v "0.2.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0b1g5d5944zb6d1s6dga1xdilqg183g9r1dhl4vbirr7jhk3q8gs")))

(define-public crate-binary-extract-0.2.3 (c (n "binary-extract") (v "0.2.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1w3dy4hx18x8cw0csckhrv179l1bxrh9yv009y1jj59lnlmqdcxq")))

(define-public crate-binary-extract-0.3.0 (c (n "binary-extract") (v "0.3.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0i4lyrx8c9lvywpksyph2dr1dji9vbhcqvya3x5ayzb8h3fm0bc2")))

