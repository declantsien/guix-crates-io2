(define-module (crates-io bi na binary_byte) #:use-module (crates-io))

(define-public crate-binary_byte-0.1.0 (c (n "binary_byte") (v "0.1.0") (h "01i52a1rl5ky1r883ighpljxp2xzhdk0zxxj8b7dykbsv79afa51")))

(define-public crate-binary_byte-0.1.1 (c (n "binary_byte") (v "0.1.1") (h "1mn3qbq369np0dypdv4k4ivxrwq045p9di0nqch294228xxzb77p")))

(define-public crate-binary_byte-0.1.2 (c (n "binary_byte") (v "0.1.2") (h "1w8f8qs0srid2q9k1r4sgf7yx4x4yizb4c61x3b2kgr53ib72hk8")))

