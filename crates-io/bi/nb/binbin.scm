(define-module (crates-io bi nb binbin) #:use-module (crates-io))

(define-public crate-binbin-0.1.0 (c (n "binbin") (v "0.1.0") (h "17js99d6rbjbz6fhpivpm3b6rvc75n973fxzyib6ijhhksdhlsrp")))

(define-public crate-binbin-0.2.0 (c (n "binbin") (v "0.2.0") (h "0k29gg3vq98y2sw1r6f1v43prb0zk43jdnrrhklh0l0kygqchhqk")))

