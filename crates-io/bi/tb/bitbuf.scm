(define-module (crates-io bi tb bitbuf) #:use-module (crates-io))

(define-public crate-bitbuf-0.1.0 (c (n "bitbuf") (v "0.1.0") (h "0x1bvb9y15zhxmd8gfsw2bqddaj58mw3azj42k59s7m3rz6w5smp")))

(define-public crate-bitbuf-0.1.1 (c (n "bitbuf") (v "0.1.1") (h "0mn1izv3ng8gpv6w4qvfzvyzmdcvicz7gbgkbrc0akc7ycz1hpbs")))

(define-public crate-bitbuf-1.0.0 (c (n "bitbuf") (v "1.0.0") (h "0si2g4i84czg6p0idxgq01svazd7bl1l5xwxyw082fss8g1sn59b")))

