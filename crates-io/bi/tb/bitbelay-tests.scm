(define-module (crates-io bi tb bitbelay-tests) #:use-module (crates-io))

(define-public crate-bitbelay-tests-0.1.0 (c (n "bitbelay-tests") (v "0.1.0") (d (list (d (n "bitbelay-providers") (r "^0.1.0") (d #t) (k 0)) (d (n "bitbelay-report") (r "^0.1.0") (d #t) (k 0)) (d (n "bitbelay-statistics") (r "^0.1.0") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "byte-unit") (r "^5.1.2") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0gpbq7jd4wxgqs6xk2qj0wp9sl5lsqj0qw5scjv3f56rhg0f0vbi")))

