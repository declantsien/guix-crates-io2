(define-module (crates-io bi tb bitbit) #:use-module (crates-io))

(define-public crate-bitbit-0.1.0 (c (n "bitbit") (v "0.1.0") (h "1agr0flsqgd5p9y5sg1j6krp2fhlw7sa8sy8i7gi95biig498frp")))

(define-public crate-bitbit-0.1.1 (c (n "bitbit") (v "0.1.1") (h "1dzqgd28my9v0y3ydb9yalyqn3m53cpzzinj9jni8gn9d0dy1q56")))

(define-public crate-bitbit-0.1.2 (c (n "bitbit") (v "0.1.2") (h "1jax4m0ni26vcmgrjngwyygifwh3q5r0cxv5qvv5nvlnpqsc6wph")))

(define-public crate-bitbit-0.2.0 (c (n "bitbit") (v "0.2.0") (h "08m37qg4f18lihvf6lwsryv3j5b02nasvpi6vskkyn9yqlwy87il")))

