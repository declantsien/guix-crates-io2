(define-module (crates-io bi tb bitbadges-cosmwasm) #:use-module (crates-io))

(define-public crate-bitbadges-cosmwasm-0.0.1 (c (n "bitbadges-cosmwasm") (v "0.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1rs8qrw4cm1pb791zackv1p53qq53hlcpllwj8milawfrfwn1lw5") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-bitbadges-cosmwasm-0.0.2 (c (n "bitbadges-cosmwasm") (v "0.0.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "14b1vdydxrc2blja7vcmzg2qbxw7w5pq7zh9jak0wcj6l64w4hrf") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-bitbadges-cosmwasm-0.0.3 (c (n "bitbadges-cosmwasm") (v "0.0.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1y43ib4jnnaywas2yz9prsihkb5yvrjlqf55p4w1cadiiznv6vig") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-bitbadges-cosmwasm-0.0.5 (c (n "bitbadges-cosmwasm") (v "0.0.5") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1m01v6fb7aj8b7nswn4pr6lsr1s1yghn97nb2k6xf7g3lawn0i1n") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-bitbadges-cosmwasm-0.0.6 (c (n "bitbadges-cosmwasm") (v "0.0.6") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "09w63x7n2llyf724rvjia4z9qcw1h2wah8l4gyl01p53d9m0rzld") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-bitbadges-cosmwasm-0.0.7 (c (n "bitbadges-cosmwasm") (v "0.0.7") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0bbca1pdbrf47ib1w08khjwxa0icf64qp5wb4nj8msa3sn5gld58") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-bitbadges-cosmwasm-0.0.8 (c (n "bitbadges-cosmwasm") (v "0.0.8") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "026mnn65mm1qwg03q1drdqh6syx8kri9vww2m7c30cg61hldiwns") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-bitbadges-cosmwasm-0.0.9 (c (n "bitbadges-cosmwasm") (v "0.0.9") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1i1b6a4s94npaap5i82936ajrq3wpawnhpbs2qgv6nprwzh0vlj4") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

