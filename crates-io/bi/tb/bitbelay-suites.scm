(define-module (crates-io bi tb bitbelay-suites) #:use-module (crates-io))

(define-public crate-bitbelay-suites-0.1.0 (c (n "bitbelay-suites") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bitbelay-providers") (r "^0.1.0") (d #t) (k 0)) (d (n "bitbelay-report") (r "^0.1.0") (d #t) (k 0)) (d (n "bitbelay-statistics") (r "^0.1.0") (d #t) (k 0)) (d (n "bitbelay-tests") (r "^0.1.0") (d #t) (k 0)) (d (n "byte-unit") (r "^5.1.2") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1k0zhh5ymmy8wqq3g7lklgxsc07y50jb4ixvcgs5fy3485xbp79l")))

