(define-module (crates-io bi tb bitbelay-statistics) #:use-module (crates-io))

(define-public crate-bitbelay-statistics-0.1.0 (c (n "bitbelay-statistics") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0gpc55y38gzcq1svark2c43hyn59w1wwm4mj36hbwbasfv1hnhsj")))

