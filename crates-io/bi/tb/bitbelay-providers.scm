(define-module (crates-io bi tb bitbelay-providers) #:use-module (crates-io))

(define-public crate-bitbelay-providers-0.1.0 (c (n "bitbelay-providers") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nk1llfqhphm6hl3mlims5n5vx3iddryllri5n4wb8mi3rd1s0r5")))

