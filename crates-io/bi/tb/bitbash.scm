(define-module (crates-io bi tb bitbash) #:use-module (crates-io))

(define-public crate-bitbash-0.1.0 (c (n "bitbash") (v "0.1.0") (d (list (d (n "bitbash-macros") (r "= 0.1.0") (d #t) (k 0)))) (h "1qcjy2g3avkbmvydz2p83m02xhdrz54szzqygx70l3lpmjhbdm5g") (f (quote (("const"))))))

(define-public crate-bitbash-0.1.1 (c (n "bitbash") (v "0.1.1") (d (list (d (n "bitbash-macros") (r "= 0.1.0") (d #t) (k 0)))) (h "0v183amfqjjhwd0qlraf375y5i3kbj8xj4fp5l0jabl190pc2492") (f (quote (("const"))))))

(define-public crate-bitbash-0.2.0 (c (n "bitbash") (v "0.2.0") (d (list (d (n "bitbash-macros") (r "= 0.2.0") (d #t) (k 0)))) (h "1sshj5vbv1hh6i4p3f7cjrj5w87ajq6bchi4wg9bfkbqp2f77gwb") (f (quote (("const"))))))

(define-public crate-bitbash-0.3.0 (c (n "bitbash") (v "0.3.0") (d (list (d (n "bitbash-macros") (r "= 0.3.0") (d #t) (k 0)))) (h "0cckv7jj9g3myphpfzbav09jshapwqis4v5sm3f59z5j43mrp9dv") (f (quote (("const"))))))

(define-public crate-bitbash-0.4.0 (c (n "bitbash") (v "0.4.0") (d (list (d (n "bitbash-macros") (r "= 0.4.0") (d #t) (k 0)))) (h "0rfk8gfnpvkz56vzgwz2f88h3zv3nd7kg2859xq1185zbavmp291") (f (quote (("const"))))))

(define-public crate-bitbash-0.5.0 (c (n "bitbash") (v "0.5.0") (d (list (d (n "bitbash-macros") (r "= 0.5.0") (d #t) (k 0)))) (h "1m7vlla2pnbm31c8yq7g9f9glbja3i2q9bvp15c5xk4v3f1hmn68") (f (quote (("const"))))))

(define-public crate-bitbash-0.5.1 (c (n "bitbash") (v "0.5.1") (d (list (d (n "bitbash-macros") (r "= 0.5.1") (d #t) (k 0)))) (h "0mrf9c85b30xdwk8ilxvgz3k43w3qpd9dzd666wcpp76vcp3g458") (f (quote (("const"))))))

