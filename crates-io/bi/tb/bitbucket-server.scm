(define-module (crates-io bi tb bitbucket-server) #:use-module (crates-io))

(define-public crate-bitbucket-server-0.1.0-alpha.1 (c (n "bitbucket-server") (v "0.1.0-alpha.1") (d (list (d (n "hyper") (r "^0.14.13") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (d #t) (k 2)))) (h "1w8wws3rml4i3243lza3sh9phfcazw3a7ah22k4fdzxfg6gbyc0l")))

