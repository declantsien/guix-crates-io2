(define-module (crates-io bi tb bitboard) #:use-module (crates-io))

(define-public crate-bitboard-0.1.0 (c (n "bitboard") (v "0.1.0") (d (list (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "13sapv9prwmv6sm9ldqvv5a82wdif8qa7gxp8wwrs5lsnj3vy33b")))

(define-public crate-bitboard-0.1.1 (c (n "bitboard") (v "0.1.1") (d (list (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "0rf516579bk3pg50mvhh5s0k2fnfyc3qbwi49qqlv9jz857j3f12")))

(define-public crate-bitboard-0.1.2 (c (n "bitboard") (v "0.1.2") (d (list (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "1b0z59283b582kq6bljn4px06c4zbgkzzkgf24h53p51vsningzw")))

