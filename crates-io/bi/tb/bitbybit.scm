(define-module (crates-io bi tb bitbybit) #:use-module (crates-io))

(define-public crate-bitbybit-1.0.0 (c (n "bitbybit") (v "1.0.0") (d (list (d (n "arbitrary-int") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yy4fk311whl3w5l4yi9wj0m8pxxd3bpgqnfgf8rb3igxzqm8rxv")))

(define-public crate-bitbybit-1.1.0 (c (n "bitbybit") (v "1.1.0") (d (list (d (n "arbitrary-int") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1p8h0fhakpibwrzdpq53hh5mi6qyiyms324pcvmvbm14vxaxjmwr")))

(define-public crate-bitbybit-1.1.1 (c (n "bitbybit") (v "1.1.1") (d (list (d (n "arbitrary-int") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02nl6i57gd9av4mm57mkxwkc5axjgxglg8y0mn0rvl90yq8ghhsy")))

(define-public crate-bitbybit-1.1.2 (c (n "bitbybit") (v "1.1.2") (d (list (d (n "arbitrary-int") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04r1vgqaihns6lk4qa5diy5jjp1wjy1kaaaibcb4v52wij52bgxz")))

(define-public crate-bitbybit-1.1.3 (c (n "bitbybit") (v "1.1.3") (d (list (d (n "arbitrary-int") (r "^1.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bw575n19az7m32l8bbb8gd7yzv28cv4210sqq1h682bcgcrcvwf")))

(define-public crate-bitbybit-1.1.4 (c (n "bitbybit") (v "1.1.4") (d (list (d (n "arbitrary-int") (r "^1.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0k22x4b4cvl2z28wdq2hc22haip4i5hphy79kpwvsmsnip86dzm1")))

(define-public crate-bitbybit-1.1.5 (c (n "bitbybit") (v "1.1.5") (d (list (d (n "arbitrary-int") (r "^1.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ird2dnck34m6ai1a2vb5zlj948ivgi8clvc2d249kpdrnwcsmdx")))

(define-public crate-bitbybit-1.2.0 (c (n "bitbybit") (v "1.2.0") (d (list (d (n "arbitrary-int") (r "^1.2.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1yccxr7b3hzcwhivf9xac3bg50q6vyhgnmrb7wnkrvp0qa9fqkdv")))

(define-public crate-bitbybit-1.2.1 (c (n "bitbybit") (v "1.2.1") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1m4z5gfk97v4vcksg10hhn1s02r6y9v0fbdf49ljwf3m8bc48pgq") (f (quote (("experimental_builder_syntax"))))))

(define-public crate-bitbybit-1.2.2 (c (n "bitbybit") (v "1.2.2") (d (list (d (n "arbitrary-int") (r "^1.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0dza3cvjk9qspcgch1x7imla2dqpbb10142dp7vcw0nwylalh52f") (f (quote (("experimental_builder_syntax"))))))

(define-public crate-bitbybit-1.3.0 (c (n "bitbybit") (v "1.3.0") (d (list (d (n "arbitrary-int") (r "^1.2.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jksd81c0nv0998w1l8l4qvq1rfl3s4njh2z40mp2jd148q1lhkp")))

(define-public crate-bitbybit-1.3.1 (c (n "bitbybit") (v "1.3.1") (d (list (d (n "arbitrary-int") (r "^1.2.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fh01xyc8h3ik55ylhya9vjs1xnxnqc0fifqr0ia93yxas6innil")))

(define-public crate-bitbybit-1.3.2 (c (n "bitbybit") (v "1.3.2") (d (list (d (n "arbitrary-int") (r "^1.2.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ywrk3b9b7vs3l9cqssbnsvy2k5ziy9fspsg9b7xzkd7afbpy5gv")))

