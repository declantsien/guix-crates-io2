(define-module (crates-io bi tb bitbang-hal) #:use-module (crates-io))

(define-public crate-bitbang-hal-0.1.0 (c (n "bitbang-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "~0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "~0.1") (d #t) (k 0)))) (h "073vb5ykm5f387qdrj3b84nyp8hdppgdsj5i91364x7hlhmh3lmj")))

(define-public crate-bitbang-hal-0.2.0 (c (n "bitbang-hal") (v "0.2.0") (d (list (d (n "embedded-hal") (r "~0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "metro_m4") (r "~0.1") (f (quote ("unproven"))) (d #t) (k 2)) (d (n "nb") (r "~0.1") (d #t) (k 0)) (d (n "panic-halt") (r "~0.2") (d #t) (k 2)))) (h "0v2y8lybi4zsh9lknvr94b4cmkbw9fs5gx6bmyxnxalhwixnpwf1")))

(define-public crate-bitbang-hal-0.2.1 (c (n "bitbang-hal") (v "0.2.1") (d (list (d (n "embedded-hal") (r "~0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "metro_m4") (r "~0.1") (f (quote ("unproven"))) (d #t) (k 2)) (d (n "nb") (r "~0.1") (d #t) (k 0)) (d (n "panic-halt") (r "~0.2") (d #t) (k 2)))) (h "095nz7bw4khbp5xa3qb9c82ssdpcadqsbgv27i03m2r48rmfbl2f")))

(define-public crate-bitbang-hal-0.2.2 (c (n "bitbang-hal") (v "0.2.2") (d (list (d (n "embedded-hal") (r "~0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "metro_m4") (r "~0.1") (f (quote ("unproven"))) (d #t) (k 2)) (d (n "nb") (r "~0.1") (d #t) (k 0)) (d (n "panic-halt") (r "~0.2") (d #t) (k 2)))) (h "05jig258l0g6a7fjq86nl1bbhsadhlv8cv07hg39m1k3mvv99xv9")))

(define-public crate-bitbang-hal-0.2.3 (c (n "bitbang-hal") (v "0.2.3") (d (list (d (n "embedded-hal") (r "~0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "metro_m4") (r "~0.1") (f (quote ("unproven"))) (d #t) (k 2)) (d (n "nb") (r "~0.1") (d #t) (k 0)) (d (n "panic-halt") (r "~0.2") (d #t) (k 2)))) (h "028dgg6w7kv48jpz8kcfdc0zs1l23hjjwxhns37r5hx5arnjlkb4")))

(define-public crate-bitbang-hal-0.3.0 (c (n "bitbang-hal") (v "0.3.0") (d (list (d (n "embedded-hal") (r "~0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "metro_m4") (r "~0.1") (f (quote ("unproven"))) (d #t) (k 2)) (d (n "nb") (r "~0.1") (d #t) (k 0)) (d (n "panic-halt") (r "~0.2") (d #t) (k 2)))) (h "095wcv4i2bfhfq90qjs0m90n3fvgm5wc65qgj6y42piik23r4x5r")))

(define-public crate-bitbang-hal-0.3.2 (c (n "bitbang-hal") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "eeprom24x") (r "^0.3.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lm75") (r "^0.1") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f1xx-hal") (r "^0.5.3") (f (quote ("stm32f103" "rt" "medium"))) (d #t) (k 2)))) (h "0jiz0ds4144v4i0n7k2mi6qjqi3rsbajf3cq8micgr2ps8i1a3j7")))

(define-public crate-bitbang-hal-0.3.3 (c (n "bitbang-hal") (v "0.3.3") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "eeprom24x") (r "^0.5.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "lm75") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f1xx-hal") (r "^0.9") (f (quote ("stm32f103" "rt" "medium"))) (d #t) (k 2)))) (h "0a0a6rgx74v6bax9h3mwisrkjb5p0fllx0h67djb3789iba7hvla")))

