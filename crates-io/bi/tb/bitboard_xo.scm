(define-module (crates-io bi tb bitboard_xo) #:use-module (crates-io))

(define-public crate-bitboard_xo-0.1.0 (c (n "bitboard_xo") (v "0.1.0") (d (list (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "outcome") (r "^0.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 2)))) (h "0c4z1kixj8gd7yv9qgmdvw99za67gkkq79mj6jb1hpy25dkay1yx")))

(define-public crate-bitboard_xo-0.1.1 (c (n "bitboard_xo") (v "0.1.1") (d (list (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "outcome") (r "^0.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 2)))) (h "1dqzpfpb774gx9s43amwf8z48zjbriciqx4annmyjc03zc6aay3s")))

(define-public crate-bitboard_xo-0.2.0 (c (n "bitboard_xo") (v "0.2.0") (d (list (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "outcome") (r "^0.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 2)))) (h "1zv1c08lxqskzb3dwwyn69zm0zw166dcigcq52lqjnhnyydgzdc7")))

(define-public crate-bitboard_xo-1.0.0 (c (n "bitboard_xo") (v "1.0.0") (d (list (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "outcome") (r "^0.1") (d #t) (k 0)))) (h "0phal4r5jkwk4j90zjppzc3jfzda6bj068vb177r06d08gs9rh53")))

(define-public crate-bitboard_xo-1.1.0 (c (n "bitboard_xo") (v "1.1.0") (d (list (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "outcome") (r "^0.1") (d #t) (k 0)))) (h "0dkw6hp0qi0fvkb4rgkrcl77q1y80mi4gs0j46i87q77s2lbwjc6")))

