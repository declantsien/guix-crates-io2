(define-module (crates-io bi tb bitbelay-report) #:use-module (crates-io))

(define-public crate-bitbelay-report-0.1.0 (c (n "bitbelay-report") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "nonempty") (r "^0.9.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "1cm75gy786al40as9caxjz0lw5bfq7an1k6354xflf9pjcqdl9vv")))

