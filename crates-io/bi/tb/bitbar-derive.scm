(define-module (crates-io bi tb bitbar-derive) #:use-module (crates-io))

(define-public crate-bitbar-derive-0.4.0 (c (n "bitbar-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mwk3l56fhhqxjn73dxvz7glwmnw2q5l1f9vp5svavv7cg0lh115")))

(define-public crate-bitbar-derive-0.4.1 (c (n "bitbar-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z954c0hgjlg1ww1yv5xqcm1chwiplhyaak9x2i29rq1xfx6pvwp")))

(define-public crate-bitbar-derive-0.4.2 (c (n "bitbar-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bi5q2g45qlrvphhvwk76sgfinhkpv044qi48nnnsz05qnjdkgk5")))

(define-public crate-bitbar-derive-0.4.3 (c (n "bitbar-derive") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g06cy8q43xm07i41n2miqz9brzwl89lyfynd01bmwz0i9xnjx7q") (f (quote (("tokio02") ("tokio"))))))

(define-public crate-bitbar-derive-0.5.0 (c (n "bitbar-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0713h9kgvc6d95r489v3ai2i9n5v8vywcl3cfbmg0886a2jkv0p2") (f (quote (("tokio02") ("tokio"))))))

(define-public crate-bitbar-derive-0.5.1 (c (n "bitbar-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0iqphymgc81bpkf3gp7fhzb0y1yf8aawdfjq234w0nnp7i6j1nrx") (f (quote (("tokio02") ("tokio"))))))

(define-public crate-bitbar-derive-0.5.2 (c (n "bitbar-derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "134f0qm1h30s12yb6asb4iy3vsnyjgf0bi1wbn7bypfc5i7prqnp") (f (quote (("tokio02") ("tokio"))))))

(define-public crate-bitbar-derive-0.6.0 (c (n "bitbar-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00i169wwi73ckqdfbazj8f7v39qlkm27lb7iq5iwn9vaqc4kjysy") (f (quote (("tokio02") ("tokio"))))))

(define-public crate-bitbar-derive-0.7.0 (c (n "bitbar-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16k3vd5z5w99w2k76qybx6xnq3sfwz7mar596i08acyhcg6kvwyr") (f (quote (("tokio02") ("tokio"))))))

(define-public crate-bitbar-derive-0.7.1 (c (n "bitbar-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0si4c3da3fv282w162zc2i5hjrbhqjsnm1n72yy3pladqws3hc6n") (f (quote (("tokio02") ("tokio"))))))

(define-public crate-bitbar-derive-0.7.2 (c (n "bitbar-derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10k5ra541zl8asvanki94d3hnl8qk8m0gi6vy6zmlpi235m026py") (f (quote (("tokio02") ("tokio"))))))

(define-public crate-bitbar-derive-0.7.3 (c (n "bitbar-derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dvhl47dqwfdnzrxxvasfvv3zskhsz3q089lqc88z433a24bszvi") (f (quote (("tokio02") ("tokio"))))))

(define-public crate-bitbar-derive-0.8.0 (c (n "bitbar-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rc8v01igrkyxa80nirsnv773ggxjwx6gab32c3k22m60qszr565") (f (quote (("tokio"))))))

(define-public crate-bitbar-derive-0.8.1 (c (n "bitbar-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ci0mskrs9daz13z9r4dgmcfbscwdvxyyr2bglq1cay7406sg5qj") (f (quote (("tokio"))))))

(define-public crate-bitbar-derive-0.8.2 (c (n "bitbar-derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "178ckjf6hmd0phzl05lfjbsczpmada2fhjvd11a7ypqg0hias3j7") (f (quote (("tokio"))))))

(define-public crate-bitbar-derive-0.9.0 (c (n "bitbar-derive") (v "0.9.0") (d (list (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fc5kk6d0ckilm0had15b5x8m75wpkaqpnrdgxb2kilasqygy8yr") (f (quote (("tokio"))))))

