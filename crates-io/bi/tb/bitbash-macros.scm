(define-module (crates-io bi tb bitbash-macros) #:use-module (crates-io))

(define-public crate-bitbash-macros-0.1.0 (c (n "bitbash-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "083g5b5rpxxs1n0xibq58drcvphxfhxaalkswa89jgh2akmbd0dm")))

(define-public crate-bitbash-macros-0.2.0 (c (n "bitbash-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gyr8i42llgkv7v388kx7bsxwiyxhaz9s45bw13iw787z0lma3lv")))

(define-public crate-bitbash-macros-0.3.0 (c (n "bitbash-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cyjizh7yhlgpypsfkp9ww6lzas0wn3f71jqgblzqsr3lacgyva6")))

(define-public crate-bitbash-macros-0.4.0 (c (n "bitbash-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11lkdh7b8jfdlmkfqlj6xw95p66x9wh946isdvj458qwlj8pv7zd")))

(define-public crate-bitbash-macros-0.5.0 (c (n "bitbash-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1znv1z4zfgb7awv4yyair0q12sh72mbjshiy0056x35adwl446a7")))

(define-public crate-bitbash-macros-0.5.1 (c (n "bitbash-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16zz9yd08yjn05xraldc6w02nk0387gi2xswvj0xyxxf5l642z4a")))

