(define-module (crates-io bi tb bitbag_derive) #:use-module (crates-io))

(define-public crate-bitbag_derive-0.1.0 (c (n "bitbag_derive") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1w6m659nmaml3ab7cdf7q8zc4yb23wpf8wzf71ygzqkn486w1kvr")))

(define-public crate-bitbag_derive-0.2.0 (c (n "bitbag_derive") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ac20is84gmplyc0qyd4r81mps5asdsyg8zb6nakccbxhllxxdln")))

(define-public crate-bitbag_derive-0.2.1 (c (n "bitbag_derive") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1kqdfkx883ijy7n0zk5zfn0ar4b81z11nfhk5bcfdc36kvgqhpn2")))

