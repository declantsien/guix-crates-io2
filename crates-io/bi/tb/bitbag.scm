(define-module (crates-io bi tb bitbag) #:use-module (crates-io))

(define-public crate-bitbag-0.1.0 (c (n "bitbag") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dgx2drwvc7j322f5m3cm5k79l7msld0n9fm59cn8cfv62vzam4w")))

(define-public crate-bitbag-0.1.1 (c (n "bitbag") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "bitbag_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15wyc0ld8gx9jsv4z6xl5xcfc08kp9y95k47azbj3sa52qnvkpz6")))

(define-public crate-bitbag-0.1.2 (c (n "bitbag") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "bitbag_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nxmavhkzi2h0cw6zrqgry4sf336v40zaq9ja0y6v627nv4m642d")))

(define-public crate-bitbag-0.1.3 (c (n "bitbag") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "bitbag_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fgfvr4rdsqacd98vvd5r16jabr00yf6h6bk6azw3019havdvsxx")))

(define-public crate-bitbag-0.1.4 (c (n "bitbag") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "bitbag_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h6kk6wnv45nl1zcs423qannpw9z1k9kbd9pg0d8jhjxg06ja7ik")))

(define-public crate-bitbag-0.2.1 (c (n "bitbag") (v "0.2.1") (d (list (d (n "bit-iter") (r "^1.1.1") (d #t) (k 0)) (d (n "bitbag_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.42") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 2)) (d (n "indoc") (r "^1.0.8") (d #t) (k 2)))) (h "0f5jdm9sjp0zvaxwxg7a1b8lr5fjgfkqj68p452civkigf84gfk4")))

