(define-module (crates-io bi tb bitbank) #:use-module (crates-io))

(define-public crate-bitbank-0.1.0 (c (n "bitbank") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "04gszfh0bgzgjrl7x0imiggmpxz8nq9xcx8yn1lidk438fjrglkb")))

(define-public crate-bitbank-0.1.1 (c (n "bitbank") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "17jqw1n7vs9jpwaf5fsmazx5yx0gin5mc20flfjnbcmjb5xs0ff6")))

