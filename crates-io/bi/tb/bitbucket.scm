(define-module (crates-io bi tb bitbucket) #:use-module (crates-io))

(define-public crate-bitbucket-0.1.0-alpha (c (n "bitbucket") (v "0.1.0-alpha") (d (list (d (n "hyper") (r "^0.13.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "0gx2hyqyv9aq1kga0qqncq08p316jd3w5ygphc101x0v5zlpd0az") (y #t)))

(define-public crate-bitbucket-0.1.1-alpha (c (n "bitbucket") (v "0.1.1-alpha") (d (list (d (n "hyper") (r "^0.13.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "1p1f8989np306bkdwmc4ldspm43i0942b6ga9a46pc0z64v99nqh") (y #t)))

(define-public crate-bitbucket-0.1.2-alpha (c (n "bitbucket") (v "0.1.2-alpha") (d (list (d (n "hyper") (r "^0.13.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "0kszwrdwn40hl78gbd0534kbfzzihbpwvhi519grzzkar5qlkyjc") (y #t)))

(define-public crate-bitbucket-0.1.0-alpha.1 (c (n "bitbucket") (v "0.1.0-alpha.1") (d (list (d (n "hyper") (r "^0.13.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "0xrxjahm6b9f0f4qb5d19zzwqrp89p946pjxc8gl1rgza53d7vpx") (y #t)))

(define-public crate-bitbucket-0.1.2-alpha.1 (c (n "bitbucket") (v "0.1.2-alpha.1") (d (list (d (n "hyper") (r "^0.13.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("full"))) (d #t) (k 2)))) (h "0vnznzx53avngj2sifx46y14927s62li8qmracdnhhsj1d4l29sg") (y #t)))

