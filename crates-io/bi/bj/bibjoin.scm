(define-module (crates-io bi bj bibjoin) #:use-module (crates-io))

(define-public crate-bibjoin-0.1.2 (c (n "bibjoin") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.15.1") (d #t) (k 0)))) (h "1l8rddz7ayy27wjmaj0rm9hysq066s2cw46c9a0np4v9kpg1gni5") (y #t)))

(define-public crate-bibjoin-0.1.3 (c (n "bibjoin") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.15.1") (d #t) (k 0)))) (h "15a2xj86dhwv8b9v66pdvkra80h4xcalwhx2wgr48qxyc071ixiw") (y #t)))

(define-public crate-bibjoin-0.1.4 (c (n "bibjoin") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.15.1") (d #t) (k 0)))) (h "0ja68hmwlylag5w3viwl39f7d4n1ymq7dla1b82ihz182r8i1lxj") (y #t)))

(define-public crate-bibjoin-0.1.5 (c (n "bibjoin") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.15.1") (d #t) (k 0)))) (h "1066yxynmzvz928r4s8x3is9n80x9sjfbqmxmk52wz9xkayg138l") (y #t)))

(define-public crate-bibjoin-0.1.6 (c (n "bibjoin") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.15.1") (d #t) (k 0)))) (h "00zn6vdhmj6ss4vd5sp8f72wwvzb6dx4wi4ckgp0kafbsxz5hfyc") (y #t)))

(define-public crate-bibjoin-0.1.7 (c (n "bibjoin") (v "0.1.7") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.15.1") (d #t) (k 0)))) (h "11a3jxpxfb1hifqjgb25v8h471794fbpvalgnblbihkqh9p6nda7") (y #t)))

(define-public crate-bibjoin-0.1.8 (c (n "bibjoin") (v "0.1.8") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.15.1") (d #t) (k 0)))) (h "16m817kymh0lh9zzyxwi36lzkq103p78y6wpflcz51pvz1dxjy6g")))

(define-public crate-bibjoin-0.1.9 (c (n "bibjoin") (v "0.1.9") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.15.1") (d #t) (k 0)))) (h "04fb4cxalyqaxng95jfy9fca505489ilr8v6yr4gr44gc6g0qr4q")))

(define-public crate-bibjoin-0.2.0 (c (n "bibjoin") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.15.1") (d #t) (k 0)) (d (n "polars-io") (r "^0.15.1") (f (quote ("decompress"))) (d #t) (k 0)))) (h "1vly2z6ckfwpz12bayxqbyii2znp1skr42cg0mcyw6h38c5ani1s")))

(define-public crate-bibjoin-0.2.1 (c (n "bibjoin") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.15.1") (d #t) (k 0)) (d (n "polars-io") (r "^0.15.1") (f (quote ("decompress"))) (d #t) (k 0)))) (h "1g8lvn106w1mcqb8bbv3jvn178sjxz5qdkn2j41w75b95g46q5q6")))

(define-public crate-bibjoin-0.2.2 (c (n "bibjoin") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "polars") (r "^0.16") (d #t) (k 0)) (d (n "polars-io") (r "^0.16") (f (quote ("decompress"))) (d #t) (k 0)))) (h "1icdq7z6bg7hrin2sn3yar9c0yhbcf1a2m5l2mirwxgqpnwmsqq7")))

(define-public crate-bibjoin-0.3.0 (c (n "bibjoin") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "polars") (r "^0.16") (d #t) (k 0)) (d (n "polars-io") (r "^0.16") (f (quote ("decompress"))) (d #t) (k 0)))) (h "1nsbmszxbdf1madyhicdplzg12psza85parfwal7vlwdv1sl0239")))

