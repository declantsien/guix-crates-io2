(define-module (crates-io bi nj binjs_shared) #:use-module (crates-io))

(define-public crate-binjs_shared-0.1.1 (c (n "binjs_shared") (v "0.1.1") (d (list (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "17709z0gf3611f5gd1kvwzxqiqblc05wgk3jbi7vn2blgddrhxcn")))

