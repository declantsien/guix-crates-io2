(define-module (crates-io bi nj binjs_generate_library) #:use-module (crates-io))

(define-public crate-binjs_generate_library-0.2.1 (c (n "binjs_generate_library") (v "0.2.1") (d (list (d (n "binjs_meta") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.6") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "webidl") (r "^0.8") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 2)))) (h "0ymvddiwpzzk43rz9wrfydh59h6g87z2p1yvwc3lgk8sba9p7pz1")))

