(define-module (crates-io bi nj binjs_es6) #:use-module (crates-io))

(define-public crate-binjs_es6-0.2.1 (c (n "binjs_es6") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 0)) (d (n "binjs_generate_library") (r "^0.2") (d #t) (k 1)) (d (n "binjs_io") (r "^0.2") (d #t) (k 0)) (d (n "binjs_meta") (r "^0.4") (d #t) (k 1)) (d (n "binjs_shared") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "webidl") (r "^0.8") (d #t) (k 1)))) (h "0426xr5cc0ph7xi9bzdkcbb9qpgpxck8bv5jnnf1hpf93akvw5wa")))

