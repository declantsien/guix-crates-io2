(define-module (crates-io bi cu bicubic) #:use-module (crates-io))

(define-public crate-bicubic-0.1.0 (c (n "bicubic") (v "0.1.0") (h "0aiz7spm4b4xik222cxfbfshdbb2701j506kd0ld3j64l4k73a3x") (y #t)))

(define-public crate-bicubic-0.1.1 (c (n "bicubic") (v "0.1.1") (h "1sz1wli467ylrnafvxjzfhjcqqvs7a9frrplaflyzi8n0rkm842c")))

(define-public crate-bicubic-0.1.2 (c (n "bicubic") (v "0.1.2") (h "10xf9brx9fidwc037s80c01izw9xab8bp4b0ijivlf1f575rc4sq")))

