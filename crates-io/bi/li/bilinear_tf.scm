(define-module (crates-io bi li bilinear_tf) #:use-module (crates-io))

(define-public crate-bilinear_tf-0.1.0 (c (n "bilinear_tf") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4.6") (d #t) (k 0)))) (h "1y8fqczkjja3fagycml5wv41wh4rg9c9mi9cc7kipm5gi3qyq0pw") (y #t)))

(define-public crate-bilinear_tf-0.1.1 (c (n "bilinear_tf") (v "0.1.1") (d (list (d (n "num-complex") (r "^0.4.6") (d #t) (k 0)))) (h "1y6sr2yn3ssghs6bd69mnl1vad0ypnr0lywl0k7md3hdmr6m0lb3")))

(define-public crate-bilinear_tf-0.2.0 (c (n "bilinear_tf") (v "0.2.0") (d (list (d (n "no_denormals") (r "^0.1.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)))) (h "0jlvy59rm1vmk6p8qwkirkjnpq184zd7d8jgi3pxc6db040zahd3") (y #t)))

(define-public crate-bilinear_tf-0.2.1 (c (n "bilinear_tf") (v "0.2.1") (d (list (d (n "no_denormals") (r "^0.1.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)))) (h "0a76df2if2gljd8zxdpqd3yasi2iyfqhg8k4b7s2ls43ax9byc2a")))

