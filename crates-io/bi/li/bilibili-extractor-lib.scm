(define-module (crates-io bi li bilibili-extractor-lib) #:use-module (crates-io))

(define-public crate-bilibili-extractor-lib-0.1.0 (c (n "bilibili-extractor-lib") (v "0.1.0") (d (list (d (n "rsubs-lib") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "12k9s73q7l1sgsi3rwa3yx9w6py96cxf16y7r3j96bb7fbbv6bf2")))

(define-public crate-bilibili-extractor-lib-0.1.1 (c (n "bilibili-extractor-lib") (v "0.1.1") (d (list (d (n "rsubs-lib") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0lqlxyh9azr1fgrzc28173lwiyki0nccvp0c1d0lzw31qhygp6in")))

