(define-module (crates-io bi li bili_search) #:use-module (crates-io))

(define-public crate-bili_search-0.1.0 (c (n "bili_search") (v "0.1.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "06h85yvz3jqxzhhv4khkk9f7s676cx2janpcjpl2ls7irfz2m3ys")))

(define-public crate-bili_search-0.1.1 (c (n "bili_search") (v "0.1.1") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "15c5l16llxvj26dxyy6k3wsk64aaam797rlhf4w15phln78gc80x")))

(define-public crate-bili_search-0.1.2 (c (n "bili_search") (v "0.1.2") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "19wbyy33z6dbig1a64xri6d4cvwh77c47h4y2w1208s4g7ql1a4b")))

(define-public crate-bili_search-1.0.0 (c (n "bili_search") (v "1.0.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "1qhrjzax6r50v464k5h9wjcwif10jxfwb61pdb2raa910p3dxqcy")))

(define-public crate-bili_search-1.0.1 (c (n "bili_search") (v "1.0.1") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)))) (h "0vkkq2rh9n7fkbjnk95y9h3ikwms6d2ih8gvmhx2ac16px6x11v0")))

