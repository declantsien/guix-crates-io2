(define-module (crates-io bi li bilibili-extractor-cli) #:use-module (crates-io))

(define-public crate-bilibili-extractor-cli-0.2.0 (c (n "bilibili-extractor-cli") (v "0.2.0") (d (list (d (n "bilibili-extractor-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rsubs-lib") (r "^0.1.8") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "0fkbbzwqxjzc6almv0sb2pp5chs54sksmcj061ggrqb9g6x1qn7d")))

