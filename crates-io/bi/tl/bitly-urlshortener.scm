(define-module (crates-io bi tl bitly-urlshortener) #:use-module (crates-io))

(define-public crate-bitly-urlshortener-0.1.0 (c (n "bitly-urlshortener") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "jabba-lib") (r "^0.1.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "10mpvixwn7smsw482x82b27689x2md989hqxi8bbgdis8chkysfa")))

(define-public crate-bitly-urlshortener-0.1.1 (c (n "bitly-urlshortener") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "jabba-lib") (r "^0.1.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "1w8b4ijlsq2kp6d9n6ppwi8fqw3yvl3cxjjgdhnjpi3qi50pvw2x")))

