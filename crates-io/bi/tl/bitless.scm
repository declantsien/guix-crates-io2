(define-module (crates-io bi tl bitless) #:use-module (crates-io))

(define-public crate-bitless-0.1.0 (c (n "bitless") (v "0.1.0") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-journald") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1l5617097pzhvlgsp8j29znaiwzvi7dnc1sqlzvz65mkbfqxhxrj")))

(define-public crate-bitless-0.1.1 (c (n "bitless") (v "0.1.1") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-journald") (r "^0.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "10zsllgwz0s0xwqfkdnwm2yd9r0qrz9ac9n8fkisnnqkzqfn2q4y")))

(define-public crate-bitless-0.2.0 (c (n "bitless") (v "0.2.0") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-journald") (r "^0.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1yaywaz6jm4x62h6ad85rrpv716fnbqd5fbng1qgjkd4rdws7491")))

(define-public crate-bitless-0.3.0 (c (n "bitless") (v "0.3.0") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-journald") (r "^0.3") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "16c6kd4s0ds0bma6jrywznc2zg4hypyi7059z6s0rz81m76l975h")))

