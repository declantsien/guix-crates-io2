(define-module (crates-io bi tl bitlab) #:use-module (crates-io))

(define-public crate-bitlab-0.0.1 (c (n "bitlab") (v "0.0.1") (h "1jhg7rph0gmkhc3l8bbld654zpgdgx5jcgigb2x0qswr2q51bz8d")))

(define-public crate-bitlab-0.0.2 (c (n "bitlab") (v "0.0.2") (h "1p25336hgk3gs7h0q86f6rpkmc7mwpjs5x7cwc5sl7qdk0hdc7yr")))

(define-public crate-bitlab-0.0.3 (c (n "bitlab") (v "0.0.3") (h "1zzki3z1yjq9nn33cyk1cblnsxrqhmasb93izqdyr4q4xq07bfnm")))

(define-public crate-bitlab-0.1.0 (c (n "bitlab") (v "0.1.0") (h "1n5x0z0kx8isj0lmsq09q2ynwzg8y8b289qpamxm5d5wr73wpii9")))

(define-public crate-bitlab-0.1.1 (c (n "bitlab") (v "0.1.1") (h "0i3drrm48vd67s3pqqgd1nviydcrdvvzn5s2bm8df8bbkwd8kwfc")))

(define-public crate-bitlab-0.1.2 (c (n "bitlab") (v "0.1.2") (h "13zzyhcskzv4w7s8xdradslx867j1id6ci0rqmvlpw9sdqb2nqqm")))

(define-public crate-bitlab-0.1.3 (c (n "bitlab") (v "0.1.3") (h "1y03sj03ankbxm54mydc5izs5phhn5nf19s4w6m9ypqp1faa6zby")))

(define-public crate-bitlab-0.1.4 (c (n "bitlab") (v "0.1.4") (h "0l9872gn420yr6vgyrawnsimc3jqmarjg5gzw0lbb7yhwgv0i7qg")))

(define-public crate-bitlab-0.2.0 (c (n "bitlab") (v "0.2.0") (h "1d179g82jahznwjvs0aaprs8qdf60x5sh9cm95q6vpzh3z3gfhxr")))

(define-public crate-bitlab-0.2.1 (c (n "bitlab") (v "0.2.1") (h "0mzrhp3s9d1h2c21w5ydwkg4dja1x4il917sk7ak8j9mggjd2b9w")))

(define-public crate-bitlab-0.3.0 (c (n "bitlab") (v "0.3.0") (h "1dgyrjsqw4347ajkcdsdgx17d7ay7k0x3wb7r3crr4cr6j2g1jis")))

(define-public crate-bitlab-0.4.1 (c (n "bitlab") (v "0.4.1") (h "0kl5z96km9m498ckl4flsks0nzp9rhfmsx4br6z73zac45x4a60k")))

(define-public crate-bitlab-0.4.2 (c (n "bitlab") (v "0.4.2") (h "12fb489z97y3yh0px1l0n3l2vqxmv1wii3xsjvzm2kjyknlilbkb")))

(define-public crate-bitlab-0.4.5 (c (n "bitlab") (v "0.4.5") (h "01nvs3z7kqi19dnl2rr7vhkq96fs2l993882zk68in0y17pv39bc")))

(define-public crate-bitlab-0.5.0 (c (n "bitlab") (v "0.5.0") (h "0vy71gw4xnnp75463wsyjxwcp60k7amyh1k3p09m50xq5kbiv7fc")))

(define-public crate-bitlab-0.5.2 (c (n "bitlab") (v "0.5.2") (h "1vp3rbpd42w5x6ygng2lybcky2d6x2baf3mgqv9an2pxm29hsk6p")))

(define-public crate-bitlab-0.6.0 (c (n "bitlab") (v "0.6.0") (h "1i2bdd2yc030kcqxha41h2wzp4w7m0m38ka69i1kkv3bpvk57r7h")))

(define-public crate-bitlab-0.6.2 (c (n "bitlab") (v "0.6.2") (h "1cz4qlxyg0p76qjdai6znvmksdn7vjq1k7q0qw0hf2nvg01fmlzn")))

(define-public crate-bitlab-0.7.1 (c (n "bitlab") (v "0.7.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1v4x1d182kg8rd7bjscfpbq0zs09sdb1arjnq6l4n1lb7dxxahj5")))

(define-public crate-bitlab-0.8.0 (c (n "bitlab") (v "0.8.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "08657rvga36xsn54khxcx4jk861dksskgcbgxwv7jlbiy23mkc5m")))

(define-public crate-bitlab-0.8.1 (c (n "bitlab") (v "0.8.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1qp9c5yzcpz1c0r46j8nd2l3mfasr6kfdfnzbwxbjai9k2ziyz4r")))

(define-public crate-bitlab-0.8.2 (c (n "bitlab") (v "0.8.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1rzcbdadywh811pqz6ry6rq5vq28f5qbl085scv68zlfm28wyly0")))

(define-public crate-bitlab-1.0.0 (c (n "bitlab") (v "1.0.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "18qa7napfb5bynj68bqygbnrpy7vc5lbd4yzjwxmym52yjjd0src")))

(define-public crate-bitlab-1.1.0 (c (n "bitlab") (v "1.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0f9g9riv9l3d1ynvi439j9k8s40ln6wwhnnp8v6dvfjcbsj9phvn")))

