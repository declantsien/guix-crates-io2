(define-module (crates-io bi tl bitlight-core-derive) #:use-module (crates-io))

(define-public crate-bitlight-core-derive-0.1.0 (c (n "bitlight-core-derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bfddi2y8s8x7rmwzczd2mzm7asxwnas23qx9bmr4d9k8x2i1d34")))

