(define-module (crates-io bi du bidule) #:use-module (crates-io))

(define-public crate-bidule-0.1.0 (c (n "bidule") (v "0.1.0") (h "1kk6670kdqd109kiwck64cjdnzlpwagyalxq4hgfgzlfdn6sc9cq")))

(define-public crate-bidule-0.1.1 (c (n "bidule") (v "0.1.1") (h "1a9gzx48dx9i9jli6h20l7dcaxj7z0lnzzi2rv0n5bsjldhlvjiy")))

(define-public crate-bidule-0.1.2 (c (n "bidule") (v "0.1.2") (h "0lf3drvkvlibsavm2ixhgii1bw0d9vg9hh1hyw44im54kgpb1268")))

(define-public crate-bidule-0.1.3 (c (n "bidule") (v "0.1.3") (h "0bjvqjgqzma12lfx1w50pkggaxr8jadhf1z91i1lwcqvpywjm0ps")))

(define-public crate-bidule-0.1.4 (c (n "bidule") (v "0.1.4") (h "0w5wdpv531cpf9bmgm8m4mcz1h6pmv6vdvs42s24jz0539f02b63")))

(define-public crate-bidule-0.1.5 (c (n "bidule") (v "0.1.5") (h "1fkxl32xk1ydcgwz3xs5871y1mdqs4h67n9h98q0hswl9zmzqxqn")))

(define-public crate-bidule-0.2.0 (c (n "bidule") (v "0.2.0") (h "1ps2ibwlyha4n4v67kn4p8dn8sz0llrvwkxl3v076p41whxl50xy")))

