(define-module (crates-io bi p3 bip32_derivation) #:use-module (crates-io))

(define-public crate-bip32_derivation-1.0.0 (c (n "bip32_derivation") (v "1.0.0") (d (list (d (n "slip132") (r "^0.10") (d #t) (k 0)) (d (n "stratum-common") (r "^1.0.0") (f (quote ("bitcoin"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (k 2)))) (h "0gs01b8gr9g4brg0dh27sxdcqb737lykaqggghdklcgx0qx5arwp")))

