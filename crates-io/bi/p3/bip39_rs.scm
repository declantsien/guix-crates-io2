(define-module (crates-io bi p3 bip39_rs) #:use-module (crates-io))

(define-public crate-bip39_rs-0.6.0-beta.1 (c (n "bip39_rs") (v "0.6.0-beta.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.7") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^0.1.6") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "pbkdf2") (r "^0.3.0") (f (quote ("parallel"))) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1gq9aa6hiyd1gmj4azn3nqc9j0v3mbn2qxvakyk75sq1zw9nnwmn") (f (quote (("spanish") ("korean") ("japanese") ("italian") ("french") ("default" "chinese-simplified" "chinese-traditional" "french" "italian" "japanese" "korean" "spanish") ("chinese-traditional") ("chinese-simplified"))))))

