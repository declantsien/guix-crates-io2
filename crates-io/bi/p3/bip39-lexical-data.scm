(define-module (crates-io bi p3 bip39-lexical-data) #:use-module (crates-io))

(define-public crate-bip39-lexical-data-1.0.0-rc.1 (c (n "bip39-lexical-data") (v "1.0.0-rc.1") (h "1vgkqd59xbxk8a2s58d5fnfbhg7c6jv0sv7wgp38vlv8m0lr83rk")))

(define-public crate-bip39-lexical-data-1.0.0 (c (n "bip39-lexical-data") (v "1.0.0") (h "0mcy92y7dcck97z5fpnwqv9phbgx1r3fxkvdbivm30da426l0k39")))

