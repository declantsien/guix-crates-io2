(define-module (crates-io bi p3 bip351) #:use-module (crates-io))

(define-public crate-bip351-0.1.0 (c (n "bip351") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.29.1") (d #t) (k 0)))) (h "0bm41rl4wczqvz91xxr01749kfmlk7qjrvmd7ki344mmipzh7231")))

(define-public crate-bip351-0.2.0 (c (n "bip351") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.29.1") (d #t) (k 0)))) (h "0fpn7ll1waxk0kvrhk39y8ajfv8gp8qnhz0qxfq8svz1dh9hcd8q")))

(define-public crate-bip351-0.3.0 (c (n "bip351") (v "0.3.0") (d (list (d (n "bitcoin") (r "^0.29.2") (d #t) (k 0)))) (h "0430as1sa5gdgfr1a1i7k0wb2gshfarfw6ap4jaz145dljkgz8j3")))

(define-public crate-bip351-0.4.0 (c (n "bip351") (v "0.4.0") (d (list (d (n "bitcoin") (r "^0.29.2") (d #t) (k 0)))) (h "0sg5hd3s7j0xvl40svs81mhnwq6ahq729782rs7zhsfrghvm8pad")))

