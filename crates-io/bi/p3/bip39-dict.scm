(define-module (crates-io bi p3 bip39-dict) #:use-module (crates-io))

(define-public crate-bip39-dict-0.1.0 (c (n "bip39-dict") (v "0.1.0") (d (list (d (n "cryptoxide") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "0g9q1f9ka5j4pnhiiz67rx09lhhkj3cnwypyz663grx9b4vwiaj9") (f (quote (("latin") ("english") ("default" "english") ("cjk"))))))

(define-public crate-bip39-dict-0.1.1 (c (n "bip39-dict") (v "0.1.1") (d (list (d (n "cryptoxide") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "1n7nmgaf8ak6ym8bxaw33hl1i93hxxf58j0k7hg7lmvbblwlvdrk") (f (quote (("latin") ("english") ("default" "english") ("cjk"))))))

(define-public crate-bip39-dict-0.1.2 (c (n "bip39-dict") (v "0.1.2") (d (list (d (n "cryptoxide") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "0mpz729yk3arcy8d2dh8vid0jr8rc5arlf6ab5g1brag0m709390") (f (quote (("latin") ("english") ("default" "english") ("cjk"))))))

(define-public crate-bip39-dict-0.1.3 (c (n "bip39-dict") (v "0.1.3") (d (list (d (n "cryptoxide") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 2)))) (h "118ny3mii5lq762l34czx93x86vavkhxzdjci02gg69qf0kx5w99") (f (quote (("std") ("latin") ("english") ("default" "std" "english") ("cjk"))))))

