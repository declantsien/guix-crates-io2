(define-module (crates-io bi ni binio) #:use-module (crates-io))

(define-public crate-binio-0.1.0 (c (n "binio") (v "0.1.0") (h "082dj3fsira239cjy5yqfvh7rai3l6329x7hpjmp3b4hbbswsyj8") (y #t)))

(define-public crate-binio-0.1.1 (c (n "binio") (v "0.1.1") (h "0h72qqpf3g3cxphwg1r8j03s761w4dwfpba8cr2509sign3wi7r9") (y #t)))

(define-public crate-binio-0.1.2 (c (n "binio") (v "0.1.2") (h "0d90m0zzqanpwa9zg1xwl96fpxnwk154sdpfrjsplxrdi84mgss5") (y #t)))

(define-public crate-binio-0.1.3 (c (n "binio") (v "0.1.3") (h "03yfzrqpfh761p9j4yipw5fqmj9r6w6glx4syjwqpnfnyifp62y9") (y #t)))

(define-public crate-binio-0.1.4 (c (n "binio") (v "0.1.4") (h "05ca0f4f825frwhab0fwj58c6f4zrf4d8jvp9qm621d9692afvl2") (y #t)))

(define-public crate-binio-0.1.5 (c (n "binio") (v "0.1.5") (h "04asysmqz379768scn0bijivad7mv5rpabhqbka0y6r183mzsvbv") (y #t)))

(define-public crate-binio-0.1.6 (c (n "binio") (v "0.1.6") (h "1n6bf4g8r7ya85vfzwadbp5lydhjnvm0d951q27wf6qa0x0xv56p")))

