(define-module (crates-io bi nm binmarshal-macros) #:use-module (crates-io))

(define-public crate-binmarshal-macros-0.1.0 (c (n "binmarshal-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0ynq2ni0f8qb3mc6yan0716w0hxh49rp2rhqxg1cdz4kamf0a617")))

(define-public crate-binmarshal-macros-0.1.1 (c (n "binmarshal-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0f5f70k5pr5n4nv6njvidmyspgwmbkyadap0w2a22hwldz62znv1")))

(define-public crate-binmarshal-macros-0.1.2 (c (n "binmarshal-macros") (v "0.1.2") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1xri0p125vl23c14f7n38bvg450qiikn4d0js606ch3hl53b3j5k")))

(define-public crate-binmarshal-macros-0.1.3 (c (n "binmarshal-macros") (v "0.1.3") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "137gjqrr37sh9hwn039ilhaw28q1w51s9jiy0k3cl261r5rpsm1h")))

(define-public crate-binmarshal-macros-0.1.4 (c (n "binmarshal-macros") (v "0.1.4") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "06hhp3y0l5n8wl1x2mbkzihsjv341kd2mngsah4jy0jw7vigsn1h") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-0.1.5 (c (n "binmarshal-macros") (v "0.1.5") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "10jwvr65wiaa0cig1g5alf6fyh2xkdmsjc7chxdai237jybiflxb") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-0.1.6 (c (n "binmarshal-macros") (v "0.1.6") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0r3r43zwv6mlk87iynhci9n3i29iw65h3s2qiz61m0h26mpns4n6") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-0.1.7 (c (n "binmarshal-macros") (v "0.1.7") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "06d8wvcywlal6gz6dbhpjcch3fn01vp60mbddny7w8zqv28bby8b") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-0.1.8 (c (n "binmarshal-macros") (v "0.1.8") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0cx6520gachigxx4sp9krc7ir7pii1q39m4ywchapfz9ryfpr4ks") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-0.1.10 (c (n "binmarshal-macros") (v "0.1.10") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0aahqn7yvdahhv0hl1l50djk3szf0jb6ig7mf5sqm5l1xwn296ah") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-0.2.0 (c (n "binmarshal-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "01ha0w8dxwfyvyddl1akxxxg4xq3vrih0g8kbhnh1krqkmwy5npg") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-0.2.1 (c (n "binmarshal-macros") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1z67ibidm12pdhx31r3q8yil977diq4cryhwghqw83pxqvax4b1x") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-0.2.2 (c (n "binmarshal-macros") (v "0.2.2") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1gnwaszr36gclcay46fjj5vm5fdb4851fgbpcks3251rgx6nisp5") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-0.2.3 (c (n "binmarshal-macros") (v "0.2.3") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "17v2acya0z5zk9i1aww45nfrb8npcgnbsk5isd5xi20v0w6pbgmb") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-0.2.7 (c (n "binmarshal-macros") (v "0.2.7") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1nsd5m9ll4nr82j2hzv8dxz9ay9fd2b6drmgl97dqv8bfcpfx352") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-1.0.0 (c (n "binmarshal-macros") (v "1.0.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0z9c5g7z8dbv54rf2dc6l7hp65biff1r2hmkc1yj1744i7kqp4ls") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-1.1.0 (c (n "binmarshal-macros") (v "1.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0jylbhz8dzjichzw3kb418nj4sb42li6qc29dn6yqgy9z0aljfdh") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

(define-public crate-binmarshal-macros-1.1.3 (c (n "binmarshal-macros") (v "1.1.3") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0yjvl54b38y5z61q4rv9ngwsc5bngpkl60y8xbxc6c5n23gzhk0i") (f (quote (("serde") ("schema") ("default" "serde" "schema"))))))

