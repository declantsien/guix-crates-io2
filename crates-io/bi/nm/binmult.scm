(define-module (crates-io bi nm binmult) #:use-module (crates-io))

(define-public crate-binmult-0.1.0 (c (n "binmult") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cbfzjchmar6lnl9f9vhlmpsb4qrwmqhkab3zdjr6kpl3qbbswaq")))

(define-public crate-binmult-0.2.0 (c (n "binmult") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "131ppsp06wwb8pwmbc7iqcgj5q2i6ji70vi0kvw04250pg9z8l9f")))

