(define-module (crates-io bi nm binmatch) #:use-module (crates-io))

(define-public crate-binmatch-1.0.0 (c (n "binmatch") (v "1.0.0") (h "156v5hgdinca6ns2b82zy39fnzd75wxfynzfzazpisa51xi5cll2")))

(define-public crate-binmatch-1.0.1 (c (n "binmatch") (v "1.0.1") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1iid3nmvqjqk73bf500b12mn4d04s2xfrihgihlh6fs5f4s4bhxy")))

(define-public crate-binmatch-1.0.2 (c (n "binmatch") (v "1.0.2") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0g5ij104i70cbvfi69nb8s3hxvmjiq36spacba8bmna9nq6wmxwa")))

(define-public crate-binmatch-1.0.3 (c (n "binmatch") (v "1.0.3") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0p534pb05zn6kb21h5cqbkq9hrcbxs2h37h98d6bd03cqgbj6q3p")))

(define-public crate-binmatch-1.0.4 (c (n "binmatch") (v "1.0.4") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1vzwq6gxbmymmn59mgi2l1j8aaazjj8jm7rwr3snh9gccac067s7")))

(define-public crate-binmatch-1.0.5 (c (n "binmatch") (v "1.0.5") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "04a039pr39vq3yv0rwc32mz6ic8m7a4smvskklzcbdf0pcwx62bl")))

(define-public crate-binmatch-1.1.0 (c (n "binmatch") (v "1.1.0") (d (list (d (n "thiserror") (r "^1.0.58") (o #t) (d #t) (k 0)))) (h "01mhmsmla77h9zrnxxk2sgsv8yxxl44xrhvgp30cvi9x6zjq2l7l") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:thiserror"))))))

(define-public crate-binmatch-1.2.0 (c (n "binmatch") (v "1.2.0") (d (list (d (n "thiserror") (r "^1.0.58") (o #t) (d #t) (k 0)))) (h "1fmy1hgwxg1g8ajprdwljynrxfrp2cl4v5bjbb6xlpc1bslfway1") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:thiserror"))))))

