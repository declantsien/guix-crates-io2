(define-module (crates-io bi fr bifrost) #:use-module (crates-io))

(define-public crate-bifrost-0.1.0 (c (n "bifrost") (v "0.1.0") (h "1glx1kxbd9ik6fd3m7zjri9cb39v6yd67gp1pd2c5q3ipzhv4fy4") (y #t)))

(define-public crate-bifrost-0.1.1 (c (n "bifrost") (v "0.1.1") (h "06agz9y6mssgb2za46420x00k4pd44vxax3r9p0y41ih23mbn0kq") (y #t)))

(define-public crate-bifrost-0.1.2 (c (n "bifrost") (v "0.1.2") (h "0jifbhh95grp1z96wzlnvg15in20pzvc0nvchdq06lynar88r7ws") (y #t)))

(define-public crate-bifrost-0.2.0 (c (n "bifrost") (v "0.2.0") (h "16a166g3ggxj6y918mq3y9gy4krjds56s1k1ps7g6kw0x25sd3s1") (y #t)))

(define-public crate-bifrost-0.3.0 (c (n "bifrost") (v "0.3.0") (h "0flmdy3n259w7nm0bfgvxg59l1vx7ah573np718qiw8ckp9lnqks") (y #t)))

(define-public crate-bifrost-0.1.0-alpha (c (n "bifrost") (v "0.1.0-alpha") (h "0305yhbikqvgc26vzkn4qlksmg520k05bn0h1cykr38nyw0zmddq")))

(define-public crate-bifrost-0.4.0-alpha (c (n "bifrost") (v "0.4.0-alpha") (d (list (d (n "bifrost-ice") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "bifrost-sdp") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "bifrost-stun") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "0wln9zl93qg7bid6sw8w0zka8b9vxmcbf84l3sdvvyjkw1kdlmi5")))

(define-public crate-bifrost-0.4.0-alpha.1 (c (n "bifrost") (v "0.4.0-alpha.1") (d (list (d (n "bifrost-ice") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "bifrost-sdp") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "bifrost-stun") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "1vahlqrpzyxl0zkafq68hylycngmhdgvrgcay4kkzi8rdxw37px6")))

(define-public crate-bifrost-0.4.0-alpha.2 (c (n "bifrost") (v "0.4.0-alpha.2") (d (list (d (n "bifrost-ice") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "bifrost-sdp") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "bifrost-stun") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "1bn1v7s4gxrjnlszvn24g5y0q89grl5y4dx3dwk0223qcraj3rvr")))

