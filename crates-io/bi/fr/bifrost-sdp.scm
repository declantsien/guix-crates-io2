(define-module (crates-io bi fr bifrost-sdp) #:use-module (crates-io))

(define-public crate-bifrost-sdp-0.1.0-alpha (c (n "bifrost-sdp") (v "0.1.0-alpha") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "1xjx5g069bq91v8k43rwh334ghp8d9jg3c4n0smxg7m9f0v0fq89")))

(define-public crate-bifrost-sdp-0.1.0 (c (n "bifrost-sdp") (v "0.1.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "vec1") (r "^1.4") (d #t) (k 0)))) (h "0rsxgqk1a6jnzyzj7br00gnnn9wvk2s2a7p3khvbmnfp500sxada")))

