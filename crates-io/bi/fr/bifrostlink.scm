(define-module (crates-io bi fr bifrostlink) #:use-module (crates-io))

(define-public crate-bifrostlink-0.1.0 (c (n "bifrostlink") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt" "io-util" "process" "sync" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0fj67wj8diln40vdb2vikib6gz3wjcd5vxkga4dk9wyk9xfwdzj2")))

