(define-module (crates-io bi fi bifid) #:use-module (crates-io))

(define-public crate-bifid-1.0.0 (c (n "bifid") (v "1.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0wsx80x45ynwablcrbhzz28c0m56w26xzngpkl8d20vkb2gcwqac")))

