(define-module (crates-io bi gc bigcat) #:use-module (crates-io))

(define-public crate-bigcat-0.1.0 (c (n "bigcat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "09wn3pcb3q5hrkphwk8sxvpxpnzr97wclzli6d16v25zcgkriza0")))

(define-public crate-bigcat-0.1.1 (c (n "bigcat") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0vcvr2722qa57mypvlpfcdggaz6lw4zlyndf93rb8az365mzjlgs")))

(define-public crate-bigcat-0.1.2 (c (n "bigcat") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0sw362nhpficbywgx3hd39x085wiqb6q7d5zqilf42i33rq6aa66")))

