(define-module (crates-io bi nk binks) #:use-module (crates-io))

(define-public crate-binks-0.1.0 (c (n "binks") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)))) (h "1caygh0abm5qgn9ab81jldhrh3kkdfpqdzppxrkbla2mdb2w5zs4")))

(define-public crate-binks-0.1.1 (c (n "binks") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)))) (h "06isia543q1a1bg9p6pii571g99cxc9ng2wdzz5w111ybd9wrjq7")))

(define-public crate-binks-0.1.2 (c (n "binks") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)))) (h "02m8s3faikxfh6mc185bw5n9lai2644d5l0q6gz2wfgjjhzs2qkn")))

(define-public crate-binks-0.1.21 (c (n "binks") (v "0.1.21") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)))) (h "0k9daqapaa9767nhswgijxxglwmwpvaqddnasyr48hxq9pplmdxm")))

(define-public crate-binks-0.1.22 (c (n "binks") (v "0.1.22") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)))) (h "12hyk9xa0zp2pzq5pd45n8hplg10r047vjsxd7w0crc2brjpk9k9")))

(define-public crate-binks-0.1.23 (c (n "binks") (v "0.1.23") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.3") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)))) (h "1kvainxdyl2a17kmss81fv50qah13jnkpdq4zkf9gn0mg2w15fp2")))

