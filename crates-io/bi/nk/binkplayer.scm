(define-module (crates-io bi nk binkplayer) #:use-module (crates-io))

(define-public crate-binkplayer-0.9.0 (c (n "binkplayer") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "bladeink") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)))) (h "0abgg2sbyffn53gl352mbqc4r8dz7s8ffd90p04p00cshbrh30wi")))

(define-public crate-binkplayer-0.9.4 (c (n "binkplayer") (v "0.9.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "bladeink") (r "^0.9.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11jlri46lq3xg81an7ishfbyqw9ji7a79f22y9yyw0x72d2jh513")))

(define-public crate-binkplayer-1.0.0 (c (n "binkplayer") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "bladeink") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "047ygcq9cqkirkwi893vp0qncl915pgrmx6applj3171lfp1lpzl")))

(define-public crate-binkplayer-1.0.1 (c (n "binkplayer") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "bladeink") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15cr0ywyhfv6hcspj6kqxf4hpihy953kjlffph3vhqy0xdbhyx6i")))

(define-public crate-binkplayer-1.0.2 (c (n "binkplayer") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "bladeink") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qsrk1iw9qnsprvv5cja5ywvqkqdm22chwz6b1z8l7y3l760h80d")))

(define-public crate-binkplayer-1.0.3 (c (n "binkplayer") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "bladeink") (r "^1.0.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1p89v0xc1a97kp2fvpxsnp2l0y6s9fksra7fmzjmiyq29sdrfn3g")))

