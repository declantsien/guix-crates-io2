(define-module (crates-io bi ng bingogo) #:use-module (crates-io))

(define-public crate-bingogo-0.1.0 (c (n "bingogo") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1v51aagvc6jvdqmk7bxnnky724rmb6ini2cdapbxj4icc196llab")))

(define-public crate-bingogo-0.1.1 (c (n "bingogo") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1rnmm055b3pl0l0s452jcf11apkmz40a1xfl1gf8nw3af5glr2j0")))

(define-public crate-bingogo-0.2.0 (c (n "bingogo") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "0axxfbz2spnjx6j7h1gs9blp94ppmzansdbpvmprhcxfmm6nx2gj")))

(define-public crate-bingogo-0.2.1 (c (n "bingogo") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "0ic94mkxymkacylh0wa51i20n374ncr21fl6nj0sjn98za3virrb")))

