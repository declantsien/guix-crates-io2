(define-module (crates-io bi ng bingpot) #:use-module (crates-io))

(define-public crate-bingpot-0.1.0 (c (n "bingpot") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r0iifb2cmm28ffv6iyy7wj4f55mxzxvirhgwvzppnvjdvin9yb7")))

