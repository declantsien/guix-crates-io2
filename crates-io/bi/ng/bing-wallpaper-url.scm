(define-module (crates-io bi ng bing-wallpaper-url) #:use-module (crates-io))

(define-public crate-bing-wallpaper-url-0.1.0 (c (n "bing-wallpaper-url") (v "0.1.0") (d (list (d (n "html-extractor") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 0)))) (h "07syjridpxqn9vijxyf21xsmsdpjas642zc6hjza9ckzq73naaa7")))

(define-public crate-bing-wallpaper-url-0.2.0 (c (n "bing-wallpaper-url") (v "0.2.0") (d (list (d (n "html-extractor") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 0)))) (h "0jz248hyj75zsjsh7jahzsd78dgck3dc3dw3g7jc8258gnqyxzy1")))

(define-public crate-bing-wallpaper-url-0.3.0 (c (n "bing-wallpaper-url") (v "0.3.0") (d (list (d (n "html-extractor") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros"))) (d #t) (k 0)))) (h "112p3k0ynbmz302ngdgqm1rpal483m6bbkmhz4k07bc0c2rqj3nr")))

