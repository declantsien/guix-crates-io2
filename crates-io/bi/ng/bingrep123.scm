(define-module (crates-io bi ng bingrep123) #:use-module (crates-io))

(define-public crate-bingrep123-0.1.0 (c (n "bingrep123") (v "0.1.0") (h "0zdjkhws8p03vk10hljmfqdf83r74zk22r0xbxzj4dpapgnnfj8h")))

(define-public crate-bingrep123-0.1.1 (c (n "bingrep123") (v "0.1.1") (h "0s3sxj8xwfclprwqpmnwc94szy8pznnal7ld9fblzxkim1iykci8")))

(define-public crate-bingrep123-0.1.2 (c (n "bingrep123") (v "0.1.2") (h "0gy7z0q9gl0srkansx38hn4qy7lz2lnfwyrhag31lyc68vj1dq7h")))

