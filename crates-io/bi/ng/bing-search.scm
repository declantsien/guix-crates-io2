(define-module (crates-io bi ng bing-search) #:use-module (crates-io))

(define-public crate-bing-search-1.0.0 (c (n "bing-search") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c55nrx1zgjbvx5szl7sy3i96p2axw96xljxpiq2nf0nr3m0xb8l")))

(define-public crate-bing-search-2.0.0 (c (n "bing-search") (v "2.0.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qqm58wixs7c96gfnjnr7asm8gajpq2jv65qm5lfah1j5zhs2rv6")))

(define-public crate-bing-search-2.0.3 (c (n "bing-search") (v "2.0.3") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1av4i3higv2laccbw9s7xjym2s4ysrzhzj98rwjjsvwn1f40ylif")))

