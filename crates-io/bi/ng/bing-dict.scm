(define-module (crates-io bi ng bing-dict) #:use-module (crates-io))

(define-public crate-bing-dict-0.1.0 (c (n "bing-dict") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "subslice") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0zkynj8gr24i8bfxlgjyp5r4ml25jvqbj0irjyvf4idijz7mds8l")))

(define-public crate-bing-dict-0.1.1 (c (n "bing-dict") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "subslice") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0x30d2k22366bggnpgbz27kys1j5swz8y25mz8hxamk5zgjs5jm0")))

(define-public crate-bing-dict-0.2.0 (c (n "bing-dict") (v "0.2.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "subslice") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0w87q3qp8bvdswq3rj9nap7abb4rwcyx46s8x2q6pyqg3b0dk71n")))

(define-public crate-bing-dict-0.3.0 (c (n "bing-dict") (v "0.3.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "subslice") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0dllrd6zai9wd9bqwl8vcd7d3r6x60ll3274qvpych5i7r4bam4w") (r "1.59")))

