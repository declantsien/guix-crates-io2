(define-module (crates-io bi ng bingen) #:use-module (crates-io))

(define-public crate-bingen-0.1.0 (c (n "bingen") (v "0.1.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06mxkql7nnn177fybmg7p4s1mv36z8bnf16wnm0xaqshqpykvxva")))

(define-public crate-bingen-0.2.0 (c (n "bingen") (v "0.2.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "13xmm30q3i5sf92hnwza92lbq3fqwpzk454azl2qsn2adpgwg0g4")))

(define-public crate-bingen-0.3.0 (c (n "bingen") (v "0.3.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "0c1k5v19x6dvkxy00ldgnqvw4q6906jr4rj792ba6p9h2649zkcx")))

(define-public crate-bingen-0.3.1 (c (n "bingen") (v "0.3.1") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "0a3r7db9vjvck1ay3ywm4snzbbmzpyd9xp1d44wm1n0rnaw971gh")))

