(define-module (crates-io bi ng bingo) #:use-module (crates-io))

(define-public crate-bingo-0.1.0 (c (n "bingo") (v "0.1.0") (d (list (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0ah6zyc7iv1krfckpmv63fj7zpyd4msv9q04gv6xyqsppg3rai81")))

