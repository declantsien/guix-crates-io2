(define-module (crates-io bi g- big-hash) #:use-module (crates-io))

(define-public crate-big-hash-0.1.0 (c (n "big-hash") (v "0.1.0") (d (list (d (n "hmac-sha256") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "hmac-sha512") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "md5") (r "^0.7") (o #t) (k 0)))) (h "1fc9dfm93q0821lvn8zbggi2r0l1fj1rm1wdcig5x7v5ic62kksq") (f (quote (("hash-sha512" "hmac-sha512") ("hash-sha256" "hmac-sha256") ("hash-md5" "md5") ("default" "hash-md5" "hash-sha256" "hash-sha512"))))))

(define-public crate-big-hash-0.2.0 (c (n "big-hash") (v "0.2.0") (d (list (d (n "hmac-sha256") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "hmac-sha512") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "md5") (r "^0.7") (o #t) (k 0)))) (h "1kfxnb2mbzw4gy2krignaqcs8yfd9gibjsj8aqi74qbdd1vcfd9m") (f (quote (("hash-sha512" "hmac-sha512") ("hash-sha256" "hmac-sha256") ("hash-md5" "md5") ("default" "hash-md5" "hash-sha256" "hash-sha512"))))))

