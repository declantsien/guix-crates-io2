(define-module (crates-io bi g- big-int-proc) #:use-module (crates-io))

(define-public crate-big-int-proc-1.0.0 (c (n "big-int-proc") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1rdh1y4rllw80yqrq6n0hnfa79p1wz9snar5jza01jakvzkikj5p")))

(define-public crate-big-int-proc-1.1.0 (c (n "big-int-proc") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1grmpp4xmxxxr9phzqjgc7fawcxlbys2591rh2qc5z8p9jcyki8a")))

(define-public crate-big-int-proc-1.2.0 (c (n "big-int-proc") (v "1.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "100087vb6diisdvnrk2kmw4i0mg396bfr2di8blr9yf1lghphxi8")))

(define-public crate-big-int-proc-1.3.0 (c (n "big-int-proc") (v "1.3.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1lqwy73nwirds6bjv5n7xp1j894rlr1vv5jcnk08jsl2mm172dnq")))

(define-public crate-big-int-proc-1.4.0 (c (n "big-int-proc") (v "1.4.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1bilk3d2pr7kdpzani9fb2d0j57jv5byx3jgl1kjj5hbymyc1lsh")))

(define-public crate-big-int-proc-1.5.0 (c (n "big-int-proc") (v "1.5.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0fw6b6a94wz4fki8vpgp7z7rz8iv1jjh3cbl32xz4wbdnmpa1kvk")))

