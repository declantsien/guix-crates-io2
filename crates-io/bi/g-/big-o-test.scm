(define-module (crates-io bi g- big-o-test) #:use-module (crates-io))

(define-public crate-big-o-test-0.2.1 (c (n "big-o-test") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "ctor") (r "^0.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serial_test") (r "^0.8.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.0") (d #t) (k 2)))) (h "1lg0949w2m0lpi1zln9c84808c4m60j3mj3rc2h281znqwr2h8id") (f (quote (("dox"))))))

(define-public crate-big-o-test-0.2.2 (c (n "big-o-test") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "ctor") (r "^0.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serial_test") (r "^0.8.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.0") (d #t) (k 2)))) (h "0fxkyfksxwwd09s1x2r6n40rbyb1pjjacl3rvpxgcy4lqga705rb") (f (quote (("dox"))))))

(define-public crate-big-o-test-0.2.3 (c (n "big-o-test") (v "0.2.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "ctor") (r "^0.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serial_test") (r "^0.8.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.0") (d #t) (k 2)))) (h "1dn9d1j13gww6l9p78kcxkgs6w38qmm54q824jsap87l3qb19kqs") (f (quote (("dox"))))))

(define-public crate-big-o-test-0.2.4 (c (n "big-o-test") (v "0.2.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "ctor") (r "^0.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serial_test") (r "^0.8.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.0") (d #t) (k 2)))) (h "1i2g46pj8jj8sw25sm50db1hidraanvjr7bffsl5a2d3p3j6z3wi") (f (quote (("dox"))))))

(define-public crate-big-o-test-0.2.5 (c (n "big-o-test") (v "0.2.5") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "ctor") (r "^0.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serial_test") (r "^0.8.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.0") (d #t) (k 2)))) (h "1ayrgs55vwjd9bi9hllxdws6c9w6a186f9cn9yydbvcrslpx14mm") (f (quote (("dox"))))))

(define-public crate-big-o-test-0.2.6 (c (n "big-o-test") (v "0.2.6") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "ctor") (r "^0.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serial_test") (r "^0.8.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.0") (d #t) (k 2)))) (h "0fndw6xfv3r9jrg9drdngaq3jg089idzcc7a1a6s9kq4nbdwq4na")))

(define-public crate-big-o-test-0.2.7 (c (n "big-o-test") (v "0.2.7") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "keen-retry") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1") (d #t) (k 2)))) (h "16dz3wgkr500s3f99wxmvc9g4kg5k1gqr37k398xdb8flq8izjnj")))

(define-public crate-big-o-test-0.2.8 (c (n "big-o-test") (v "0.2.8") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "keen-retry") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1") (d #t) (k 2)))) (h "1c58rl4ac3a5jsci7wgp0nh7bv62nkifqp1dmb7f7b4ii28z5dsw")))

