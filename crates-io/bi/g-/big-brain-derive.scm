(define-module (crates-io bi g- big-brain-derive) #:use-module (crates-io))

(define-public crate-big-brain-derive-0.1.0 (c (n "big-brain-derive") (v "0.1.0") (d (list (d (n "big-brain") (r "^0.0.1") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0gqkkn68ghf271hkllz4kf8zscbbavzfx5nd4jp24q9hbaxgzmx4") (y #t)))

(define-public crate-big-brain-derive-0.1.1 (c (n "big-brain-derive") (v "0.1.1") (d (list (d (n "big-brain") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1gs181k7w9r2fnxlc9kbj3qjyvksng7rr40fqzpg8dbxb0r6ynqb") (y #t)))

(define-public crate-big-brain-derive-0.2.0 (c (n "big-brain-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0si39m8c7r6iz49gf6pb478b04ppp4mhd2f4pblc23jvwm6gdr97") (y #t)))

(define-public crate-big-brain-derive-0.2.1 (c (n "big-brain-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "1vx3xjdbhfaycjjf8hvn67nkn0a4gdfvhsy02biz990d17rn6m9h") (y #t)))

(define-public crate-big-brain-derive-0.16.0 (c (n "big-brain-derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "13468jwzqzpx1zb7jcq5dcpz19hkcrkqhw420lkcy4psij7yfm33")))

(define-public crate-big-brain-derive-0.17.0 (c (n "big-brain-derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "03cwyinsnyfk7ycczvd6n54z70pgdbp79wc8fa493q7ss5fzyri1")))

(define-public crate-big-brain-derive-0.18.0 (c (n "big-brain-derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "14j57sbhfybgdjv8l077a2dgdhczam86585majmb5bi9l2hq44gd")))

(define-public crate-big-brain-derive-0.19.0 (c (n "big-brain-derive") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1p6nik5azx45g3qpwy9nmaph234dfc2q3v61gz32payijgyf7c6z")))

(define-public crate-big-brain-derive-0.20.0 (c (n "big-brain-derive") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0yljag8lzs4hihs53ndrn4b6q86ig9xphas84lixq4gh1bxf94ch")))

