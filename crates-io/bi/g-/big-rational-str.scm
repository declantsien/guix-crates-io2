(define-module (crates-io bi g- big-rational-str) #:use-module (crates-io))

(define-public crate-big-rational-str-0.1.0 (c (n "big-rational-str") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)))) (h "1x9zqyncr6fdq41vrm94k4vlkcnsfvl9zq7qpxla8v1mnf5xd93i") (y #t)))

(define-public crate-big-rational-str-0.1.1 (c (n "big-rational-str") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)))) (h "0kxl9g23i47hq7l1s4l270q5886444ki85bs2ivfcm8x5gbhfca5") (y #t)))

(define-public crate-big-rational-str-0.1.2 (c (n "big-rational-str") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)))) (h "1c3mqnz6qh5b9ag2zwzq0m0vr3x4hn9hxfykp11qyv6is2ra959w") (y #t)))

(define-public crate-big-rational-str-0.1.3 (c (n "big-rational-str") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)))) (h "1mmknapxj18bcdg5fn5xbhjq4yn9lrxv7qkkrc6m8rm0nfgk3jfj")))

(define-public crate-big-rational-str-0.1.4 (c (n "big-rational-str") (v "0.1.4") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "num") (r "0.*") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)))) (h "0a6w9w763hc004pkvwqz1zhyyc1sq4mbjwvjdgb3c5la4nnfn4nd")))

(define-public crate-big-rational-str-0.1.5 (c (n "big-rational-str") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "19lrs11njm3jb0kqd2yhx2ivc65s6m880gbzxpylx15vr8hs4sjj")))

(define-public crate-big-rational-str-0.1.6 (c (n "big-rational-str") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1agyg6sswm2dy5yah7izrlvz7fs1vsdjf09nacsb6y5mi3ncava7")))

