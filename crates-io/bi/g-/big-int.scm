(define-module (crates-io bi g- big-int) #:use-module (crates-io))

(define-public crate-big-int-1.0.0 (c (n "big-int") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1yzqk8ji97117immb596ki3ap7zcfxkiaax6j8aylxh87yv022mh")))

(define-public crate-big-int-1.0.1 (c (n "big-int") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1ky6x3rkzdi369r7lz37jbks6q4hxf1pxr7nij0mfkhrs0mvnk72")))

(define-public crate-big-int-1.0.2 (c (n "big-int") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "15shxspx190pywbks6d0hbjxm7983rhqyi98pghhi7cawmkpwzma")))

(define-public crate-big-int-1.0.3 (c (n "big-int") (v "1.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1jxy6qvk6gnxxsrs7v5bjkibkafy5dqq2rmdk19g2v6x0nmwqxnv")))

(define-public crate-big-int-1.0.4 (c (n "big-int") (v "1.0.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1mvaw69g7mg2a0khr2y1xnwgzrxavaqms5jlzyynhkan6wgag8h2")))

(define-public crate-big-int-1.0.5 (c (n "big-int") (v "1.0.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "12ccqka9rk9zxlxrmj34xcv9sacpmwm7d9i1vi054qkflifj1q17")))

(define-public crate-big-int-2.0.0 (c (n "big-int") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0y6nw8aj59b4b3mi4gf57vld0pmslqwk87k5bp6nc3v77xj4ylfb")))

(define-public crate-big-int-2.1.0 (c (n "big-int") (v "2.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "08hy1ac8s7i8h2aa3il5n28yw1za7sxvf7r58kiyzfsm7nbd0wbv")))

(define-public crate-big-int-2.1.1 (c (n "big-int") (v "2.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1aimf5rv3pmlvyhysdd5ylxk6b5s2yia4zp9iqjzkql4dwza80ip")))

(define-public crate-big-int-2.1.2 (c (n "big-int") (v "2.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0f48zz7i78nglsygkwgki6a8xhhmasw26hkcsf32l30zfz9f4cq6")))

(define-public crate-big-int-2.1.3 (c (n "big-int") (v "2.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1c0rk6ygx86vhw2ijykawp2xxcnyaymr1qk5l969rgg8gqwd7f3a")))

(define-public crate-big-int-2.1.4 (c (n "big-int") (v "2.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "07yp7814rj3sf12xf813rkh6lar5hr1zsrfdnp8ndaczxf7x5w5d")))

(define-public crate-big-int-3.0.0 (c (n "big-int") (v "3.0.0") (d (list (d (n "big-int-proc") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "02w3m0m2qyx876biwpy087wi34g9824dnqiz5cn9iag0w8kk6d68")))

(define-public crate-big-int-3.0.1 (c (n "big-int") (v "3.0.1") (d (list (d (n "big-int-proc") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1k3c85w1liw13nbkn29cbbc96k74qi0xvsgkwll71gsbg0b5hbpp")))

(define-public crate-big-int-3.1.0 (c (n "big-int") (v "3.1.0") (d (list (d (n "big-int-proc") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0mms0y3s3f27glc2k2y71n5zvxkwp5bdl9c40815dk7wrgfnv7dm")))

(define-public crate-big-int-3.1.1 (c (n "big-int") (v "3.1.1") (d (list (d (n "big-int-proc") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1nyg91pqgya9rz8pm7cg2hvasrx6fdzk9c06zvjcd4dkk88iispj")))

(define-public crate-big-int-4.0.0 (c (n "big-int") (v "4.0.0") (d (list (d (n "big-int-proc") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0vyzccbin8jbdx9i4dnl9lqx2kqaymsn6m3s69wvi5r02927g653")))

(define-public crate-big-int-4.0.1 (c (n "big-int") (v "4.0.1") (d (list (d (n "big-int-proc") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0n019igk6bm6763bvfk0bbv147pa57h6miqj37jyd2bis1svc029")))

(define-public crate-big-int-5.0.0 (c (n "big-int") (v "5.0.0") (d (list (d (n "big-int-proc") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0j5gjrklhi6yb0f1lvcydxrpv5iifrh82skp7w8d5hzlzx94nyr6")))

(define-public crate-big-int-5.0.1 (c (n "big-int") (v "5.0.1") (d (list (d (n "big-int-proc") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "09x177byjmrc937bxazyx0a6hfij4ymnkx2liqdsf4s25gh9viap")))

(define-public crate-big-int-6.0.0 (c (n "big-int") (v "6.0.0") (d (list (d (n "big-int-proc") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1210sf7vpcxx4c0swd32l6g5iz93c1kw118pcx4jc9xgrav4ylxp")))

(define-public crate-big-int-6.0.1 (c (n "big-int") (v "6.0.1") (d (list (d (n "big-int-proc") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0r86nmlcqryrnzghyxjjcg8v9cmn469a2p207k8ydmalicrncl9l")))

(define-public crate-big-int-6.0.2 (c (n "big-int") (v "6.0.2") (d (list (d (n "big-int-proc") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0f5r7k7fabhlbgcgmy4765v04lcdx9y1gyww6p9lpz4pi2dvabfw")))

(define-public crate-big-int-6.0.3 (c (n "big-int") (v "6.0.3") (d (list (d (n "big-int-proc") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0xwfwk853r0glf5ccpvyn5lczzirhxh51361z7fnfsygh17h70qw")))

(define-public crate-big-int-7.0.0 (c (n "big-int") (v "7.0.0") (d (list (d (n "big-int-proc") (r "^1.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1migz1x3j8yf1d29mv2adshgpp597z4wphj44slv65hkgglmqdri")))

