(define-module (crates-io bi g- big-num-manager) #:use-module (crates-io))

(define-public crate-big-num-manager-0.0.1 (c (n "big-num-manager") (v "0.0.1") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)))) (h "016svbdjkjwiphwp5nqffqi1d2mrf29fw6y3sllsshjfb1f5wqii") (r "1.62")))

(define-public crate-big-num-manager-0.0.2 (c (n "big-num-manager") (v "0.0.2") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l9impxi3rr3qypws9pmii8jpbjjb6v0kkrkcixlbmzvjf5s4hxa") (r "1.64")))

