(define-module (crates-io bi sq bisq_client) #:use-module (crates-io))

(define-public crate-bisq_client-0.0.1 (c (n "bisq_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.43") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "03sycv2k591zrlxqvjc4kxrc19fbnx90bbrrlqnxfyjglj50mkys")))

