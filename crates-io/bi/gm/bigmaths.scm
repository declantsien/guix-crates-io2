(define-module (crates-io bi gm bigmaths) #:use-module (crates-io))

(define-public crate-bigmaths-0.0.1 (c (n "bigmaths") (v "0.0.1") (h "0d71qv1z962yf7x546nff38zk12z97m0k5xi37myqnv2z1s0cmwb")))

(define-public crate-bigmaths-0.0.2 (c (n "bigmaths") (v "0.0.2") (h "1dnxm1sjmxs4rgf94r1lrrk72jvi4a38805qixc3023rf3py6qm8")))

