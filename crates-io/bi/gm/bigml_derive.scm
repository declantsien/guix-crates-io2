(define-module (crates-io bi gm bigml_derive) #:use-module (crates-io))

(define-public crate-bigml_derive-0.1.0 (c (n "bigml_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (d #t) (k 0)))) (h "0f8l9fh7dczpr4z258kq169mchi2l5a5fina7894cf0fj55c65zx")))

(define-public crate-bigml_derive-0.2.0 (c (n "bigml_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.43") (d #t) (k 0)))) (h "0h1fwfwq72pg58vykvlhf7vv5x899b3m396ij1n8vflsc6db3scb")))

(define-public crate-bigml_derive-0.2.1 (c (n "bigml_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.43") (d #t) (k 0)))) (h "0mails1928yx0pjgg4nkiscxxs4r9plj0n1igdfiajnlwaqv0aan")))

(define-public crate-bigml_derive-0.2.2 (c (n "bigml_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0hzslgryafidw38hpklccgyv65kpn6i6x5wladlmg6s6gssq4djb")))

(define-public crate-bigml_derive-0.3.0 (c (n "bigml_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "17pldh2wrwrbv3k8idk26xax9r1723m5c1h3la11gxc3jp1jqi9q")))

(define-public crate-bigml_derive-0.4.0 (c (n "bigml_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0f0c4bhz3yk8nydb2gq5a5p6dyfswyn9w8v4jz43ix7pvklx7jll")))

(define-public crate-bigml_derive-0.4.1 (c (n "bigml_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "0j6iqkq57j8g36nv96rdckk37jbim16cdd1zvjng308k7nwj5dw5")))

