(define-module (crates-io bi di bidi) #:use-module (crates-io))

(define-public crate-bidi-0.1.0 (c (n "bidi") (v "0.1.0") (h "1qsswv6j93q2jiymvq2041j9jj8w76v34xmc80wdpzkgy9wpyizb")))

(define-public crate-bidi-0.1.1 (c (n "bidi") (v "0.1.1") (h "00lfaix1s842yv878hhnnb0743862l35r52qwv5pzxqy62zgdjbx")))

