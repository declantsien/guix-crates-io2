(define-module (crates-io bi di bidir_iter) #:use-module (crates-io))

(define-public crate-bidir_iter-0.1.0 (c (n "bidir_iter") (v "0.1.0") (h "0mdgp351lc39c8ra63xgb9ln48nl37lr1m2k3nndgv32whsswpsz")))

(define-public crate-bidir_iter-0.2.0 (c (n "bidir_iter") (v "0.2.0") (h "0yni7hda0dczhd0sbic5zajlkr71sk5my9d0rkd8ak7iv6mv99b7")))

(define-public crate-bidir_iter-0.2.1 (c (n "bidir_iter") (v "0.2.1") (h "1y5lzycl8wl7bplckdp3vywp3r206aycxck5crwwcfyiyynh2v58")))

