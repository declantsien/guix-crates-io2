(define-module (crates-io bi di bidir_termtree) #:use-module (crates-io))

(define-public crate-bidir_termtree-0.1.0 (c (n "bidir_termtree") (v "0.1.0") (h "03wv7j7dlnk5jvx9b1qsm8bh4d3msbk7jda92qmm454jlx95x85k")))

(define-public crate-bidir_termtree-0.1.1 (c (n "bidir_termtree") (v "0.1.1") (h "1hlyvfi71035p0cklcnn2qwi9b5gbv8wspxn5x8vyfl99g66dlbd")))

