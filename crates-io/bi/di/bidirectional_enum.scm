(define-module (crates-io bi di bidirectional_enum) #:use-module (crates-io))

(define-public crate-bidirectional_enum-0.1.0 (c (n "bidirectional_enum") (v "0.1.0") (h "02lkprjkc4rd28vxrkgp45kc4p15fkcplf13g266iczpbygyxxyg")))

(define-public crate-bidirectional_enum-0.2.0 (c (n "bidirectional_enum") (v "0.2.0") (h "0rqkzr0cvwdsc6ygjipsndcqrrqm82a01qfg6l8m6k6py6l5ap6n")))

