(define-module (crates-io bi di bidirectional-map) #:use-module (crates-io))

(define-public crate-bidirectional-map-0.1.0 (c (n "bidirectional-map") (v "0.1.0") (h "0f75h13fcifl72b7phq8w6xmvyrqmk1aa971a5z5iny174m9ivh7")))

(define-public crate-bidirectional-map-0.1.1 (c (n "bidirectional-map") (v "0.1.1") (h "00s60rmlvzq60la28plyi6i2cc81m6jpx4bi7ymhibvcs3pd1igc")))

(define-public crate-bidirectional-map-0.1.2 (c (n "bidirectional-map") (v "0.1.2") (h "1bhxygc5kl5mfnzi27wf58xcixg57662mznzmyc7fg7lg9drx240")))

(define-public crate-bidirectional-map-0.1.3 (c (n "bidirectional-map") (v "0.1.3") (h "1rk29240zfplsy5p8naz87wym223mnydcbz3xjkcqj643spc9zv8")))

(define-public crate-bidirectional-map-0.1.4 (c (n "bidirectional-map") (v "0.1.4") (h "0ij333wm3xk0irw2bx3i6dykqr935alx777hvw88bfasvf4shrmc")))

