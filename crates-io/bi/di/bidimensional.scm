(define-module (crates-io bi di bidimensional) #:use-module (crates-io))

(define-public crate-bidimensional-0.1.0 (c (n "bidimensional") (v "0.1.0") (h "0gqd603ksk7l4ph41wysmyphmc6casg158dpqcp0hx3rkw9vhzh8")))

(define-public crate-bidimensional-0.1.1 (c (n "bidimensional") (v "0.1.1") (h "0qb858vjjpmx47b5r72ghs6lccva7nl0s5iv5fpcvdpib5iykv9v")))

(define-public crate-bidimensional-0.1.2 (c (n "bidimensional") (v "0.1.2") (h "0yanmf26f97g8axy1fqvh4c7xni1qb50bfqpfq8f0ha9xhiy3044")))

(define-public crate-bidimensional-0.1.3 (c (n "bidimensional") (v "0.1.3") (h "098k11q3lxfdwkm49b9nbqxi1b9rkwx0fkzp9inbpmvn7vmrmij6")))

(define-public crate-bidimensional-0.2.0 (c (n "bidimensional") (v "0.2.0") (d (list (d (n "doc") (r "^0.0.0") (d #t) (k 0)))) (h "1q9hsgj2hz3chsx32hf5mavgzmv76f1jjjj3y3b27528qc52882h")))

(define-public crate-bidimensional-0.2.1 (c (n "bidimensional") (v "0.2.1") (d (list (d (n "doc") (r "^0.0.0") (d #t) (k 0)))) (h "0x3igixgflaki3ag1alga1s1nm2x05mzxjdnzk8nrx7x6i6lf90w")))

(define-public crate-bidimensional-0.3.0 (c (n "bidimensional") (v "0.3.0") (d (list (d (n "doc") (r "^0.0.0") (d #t) (k 0)))) (h "1ixr7hr5finv1rrgjhwbjx6p9hwi8y3npzbrz49l0aa0374si4s0")))

(define-public crate-bidimensional-0.3.1 (c (n "bidimensional") (v "0.3.1") (d (list (d (n "doc") (r "^0.0.0") (d #t) (k 0)))) (h "1h4n5nxi5ssgs7nv6h0ijz2yycrrvmns5nyl78mkjgiin9qjxhcm")))

(define-public crate-bidimensional-0.3.2 (c (n "bidimensional") (v "0.3.2") (d (list (d (n "doc") (r "^0.0.0") (d #t) (k 0)))) (h "18dcp0fllfz3ll19i29kj48l5apwhmdd841wyhch2spz5bs724za")))

(define-public crate-bidimensional-0.3.3 (c (n "bidimensional") (v "0.3.3") (d (list (d (n "doc") (r "^0.0.0") (d #t) (k 0)))) (h "1mxrax9k7fzpac22awsk132xfvdcnm85yklz169sj1vq1lgnc6fa")))

(define-public crate-bidimensional-0.3.4 (c (n "bidimensional") (v "0.3.4") (d (list (d (n "doc") (r "^0.0.0") (d #t) (k 0)))) (h "1133c8vf0zxdq9f8lk3fwv08v10bkvfl5wqxqq259alv14dpydjd")))

(define-public crate-bidimensional-0.3.5 (c (n "bidimensional") (v "0.3.5") (d (list (d (n "doc") (r "^0.0.0") (d #t) (k 0)))) (h "12s3h0wn77aiaq6yibbw5490bqhnz876nkzi61bdw7ay98ckk6ig")))

