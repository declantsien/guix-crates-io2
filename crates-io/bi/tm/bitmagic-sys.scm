(define-module (crates-io bi tm bitmagic-sys) #:use-module (crates-io))

(define-public crate-bitmagic-sys-0.1.0+bitmagic.7.3.1 (c (n "bitmagic-sys") (v "0.1.0+bitmagic.7.3.1") (d (list (d (n "bindgen") (r "^0.55.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1n8yp4b4p33hj7qw37a7bddsgpzw1n2r2yr7gz82pr3a8hrxbb5f")))

(define-public crate-bitmagic-sys-0.1.1+bitmagic.7.3.1 (c (n "bitmagic-sys") (v "0.1.1+bitmagic.7.3.1") (d (list (d (n "bindgen") (r "^0.55.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1xgxddpi444259f3mvnjj20j6jaxrnn3kc58xg4ci53z8p1vajw7")))

(define-public crate-bitmagic-sys-0.2.0+bitmagic.7.3.1 (c (n "bitmagic-sys") (v "0.2.0+bitmagic.7.3.1") (d (list (d (n "bindgen") (r "^0.55.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "02i0vn7kvyd6paikin8xm1ffp40yv3p7w1pdk4maga22kxdc2fxk") (l "bm")))

(define-public crate-bitmagic-sys-0.2.1+bitmagic.7.4.0 (c (n "bitmagic-sys") (v "0.2.1+bitmagic.7.4.0") (d (list (d (n "bindgen") (r "^0.55.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "0qr1v4vr5p1j1rc62xaqx9qw7rqls2k3a50lj2k7k3lgm43z1x7j") (l "bm")))

(define-public crate-bitmagic-sys-0.2.2+bitmagic.7.5.0 (c (n "bitmagic-sys") (v "0.2.2+bitmagic.7.5.0") (d (list (d (n "bindgen") (r "^0.55.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "0xfp695v0mymfy6575bp1wakwysgf6rvz8rdadfgw30i4v2ck1vs") (l "bm")))

(define-public crate-bitmagic-sys-0.2.3+bitmagic.7.6.0 (c (n "bitmagic-sys") (v "0.2.3+bitmagic.7.6.0") (d (list (d (n "bindgen") (r "^0.55.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "0wfrmrhblahmyk53vkrd49bdvwf3gb7dk4cw8qnyhkv9rh92n0fd") (l "bm")))

(define-public crate-bitmagic-sys-0.2.4+bitmagic.7.7.7 (c (n "bitmagic-sys") (v "0.2.4+bitmagic.7.7.7") (d (list (d (n "bindgen") (r "^0.55.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)))) (h "1zmpng0qqwwk7bddba2dcja1nickbrlb2l0d7crzzgiji75sqdam") (l "bm")))

