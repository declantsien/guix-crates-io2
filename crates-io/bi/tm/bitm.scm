(define-module (crates-io bi tm bitm) #:use-module (crates-io))

(define-public crate-bitm-0.1.0 (c (n "bitm") (v "0.1.0") (d (list (d (n "dyn_size_of") (r "^0.1") (d #t) (k 0)))) (h "0hnzqvp0nqcs9p7pn6fld6z6jqnqpifp8nwk99vyf1hcs5c3pbpb")))

(define-public crate-bitm-0.1.1 (c (n "bitm") (v "0.1.1") (d (list (d (n "dyn_size_of") (r ">=0.1") (d #t) (k 0)))) (h "1dsaxcyg7gp7asznq9dapyaajhi4llgs4nw252fjmid4nkk6f3xg")))

(define-public crate-bitm-0.1.2 (c (n "bitm") (v "0.1.2") (d (list (d (n "dyn_size_of") (r ">=0.1") (d #t) (k 0)))) (h "0yiqbizgflbbgz0mh50yb3ny4p2hmdi7dkazy1flsrqa4zz0p2ji")))

(define-public crate-bitm-0.1.3 (c (n "bitm") (v "0.1.3") (d (list (d (n "dyn_size_of") (r ">=0.1") (d #t) (k 0)))) (h "11gx9w1s52r4ccfcryp6xn84fq25zz2g9xlpjmacd0hachkz7jck")))

(define-public crate-bitm-0.2.0 (c (n "bitm") (v "0.2.0") (d (list (d (n "dyn_size_of") (r ">=0.1") (d #t) (k 0)))) (h "0n3nvibpf7vv5658ps6j6hhnixlvz9xpxfj31y0vf9kpa0cz166b")))

(define-public crate-bitm-0.2.1 (c (n "bitm") (v "0.2.1") (d (list (d (n "dyn_size_of") (r ">=0.1") (d #t) (k 0)))) (h "0m19yj6fzpxxidsr3m041yibvjhx12p0f6lhk5wrpdi0d5x0xw8m")))

(define-public crate-bitm-0.2.2 (c (n "bitm") (v "0.2.2") (d (list (d (n "dyn_size_of") (r "^0.4") (d #t) (k 0)))) (h "1qwxn666953f1rx01xsk6gdi8ga1fqx4wsc12z41iy1sh1gfh7a6")))

(define-public crate-bitm-0.2.3 (c (n "bitm") (v "0.2.3") (d (list (d (n "dyn_size_of") (r "^0.4") (d #t) (k 0)))) (h "09qdikqz80c1lj2i113ww6p214qdk893gv15n1z5072wabxxkv3v")))

(define-public crate-bitm-0.3.0 (c (n "bitm") (v "0.3.0") (d (list (d (n "dyn_size_of") (r "^0.4") (d #t) (k 0)))) (h "1bbbmcvrw8vm2z50dn3k0brvv4fl4iwhyg9l9zmm23x3rnbkm5hw")))

(define-public crate-bitm-0.3.1 (c (n "bitm") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "dyn_size_of") (r "^0.4") (d #t) (k 0)) (d (n "iai-callgrind") (r "^0.9") (d #t) (k 2)))) (h "1k8hmahcbj76r5kc292ssx0cyilvvvmv5ajsw34zlm22k1wfpx5h")))

(define-public crate-bitm-0.4.0 (c (n "bitm") (v "0.4.0") (d (list (d (n "aligned-vec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "dyn_size_of") (r "^0.4") (d #t) (k 0)) (d (n "iai-callgrind") (r "^0.10") (d #t) (k 2)))) (h "12lwn211rmkq7v98s7czng1g4rvibf581cppv68kp26v4py37yya")))

(define-public crate-bitm-0.4.1 (c (n "bitm") (v "0.4.1") (d (list (d (n "aligned-vec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "dyn_size_of") (r "^0.4") (d #t) (k 0)) (d (n "iai-callgrind") (r "^0.10") (d #t) (k 2)))) (h "11lm00b4dryddg7spqzm0mn9ix9jiyzyiq4x3im85bqg7wkfmf9i")))

