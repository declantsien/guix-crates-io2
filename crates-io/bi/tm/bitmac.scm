(define-module (crates-io bi tm bitmac) #:use-module (crates-io))

(define-public crate-bitmac-0.1.0 (c (n "bitmac") (v "0.1.0") (h "19gixi98x9ai1hpwnlbp07by6h8q9vxcg565m6cwvaf7r7mlkr0k") (r "1.56.0")))

(define-public crate-bitmac-0.2.0 (c (n "bitmac") (v "0.2.0") (d (list (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.9") (o #t) (d #t) (k 0)))) (h "0jgzs1y8lzm9vj66y6rmpbczn9563fndld9hds6s502rbdrx2lpg") (f (quote (("default")))) (s 2) (e (quote (("smallvec" "dep:smallvec") ("bytes" "dep:bytes")))) (r "1.56.0")))

(define-public crate-bitmac-0.3.0 (c (n "bitmac") (v "0.3.0") (d (list (d (n "bytes") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15yls647j6h109ppsncncqhmbwrzgg9xywfb04xizvr240zdyl3f") (f (quote (("default")))) (s 2) (e (quote (("smallvec" "dep:smallvec") ("bytes" "dep:bytes")))) (r "1.56.0")))

