(define-module (crates-io bi tm bitmatch) #:use-module (crates-io))

(define-public crate-bitmatch-0.1.0 (c (n "bitmatch") (v "0.1.0") (d (list (d (n "boolean_expression") (r "^0.3.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "19qvs6iwbl02pfw7vwdbrp5sdvdwgkh3zmf1jj6rmz5rz1q9qg9m")))

(define-public crate-bitmatch-0.1.1 (c (n "bitmatch") (v "0.1.1") (d (list (d (n "boolean_expression") (r "^0.3.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1bd3h49s3y4s0h5qhr1nr0yfc6p8z1y3p3jbb6scjrhrsh2y2lva")))

