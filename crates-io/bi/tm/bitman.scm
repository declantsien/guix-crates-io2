(define-module (crates-io bi tm bitman) #:use-module (crates-io))

(define-public crate-bitman-1.0.0 (c (n "bitman") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1p217qwazpsnz6dqs94v65d69mwajmz528xa6fln2k80gdda04dz") (r "1.61")))

(define-public crate-bitman-2.0.0 (c (n "bitman") (v "2.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "flamegraph") (r "^0.6.1") (d #t) (k 2)) (d (n "libc-print") (r "^0.1.19") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0p280b7vwjh7zlcqz8zrnz3nz6gq0zf3ypjnrxgf65a4s97fy866") (r "1.61")))

(define-public crate-bitman-2.0.1 (c (n "bitman") (v "2.0.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "flamegraph") (r "^0.6.3") (d #t) (k 2)) (d (n "libc-print") (r "^0.1.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)))) (h "0a5y0fxkxhdf0dr31zd8ps1qg842cswnpmc2r74w1z0sz5v30l5b") (r "1.61")))

