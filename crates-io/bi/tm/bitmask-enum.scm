(define-module (crates-io bi tm bitmask-enum) #:use-module (crates-io))

(define-public crate-bitmask-enum-1.0.0 (c (n "bitmask-enum") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15kszkl4i66p1ydl93r6bsyz19zabjwkmhkzd4aal6h1kayjh99b")))

(define-public crate-bitmask-enum-1.0.1 (c (n "bitmask-enum") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "187fh86qg36v400saikvvyna8v469hjk88zic53737ib7sdv6kki")))

(define-public crate-bitmask-enum-1.0.2 (c (n "bitmask-enum") (v "1.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "192gsyxcix6kq7v51vwgiq094rqrwfcwz37pshpzd51k76kxivzs")))

(define-public crate-bitmask-enum-1.1.0 (c (n "bitmask-enum") (v "1.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00kk4bfy3hny2g3pabvbrifgbbda3pgl3swgbbk34bcfkljdifb1")))

(define-public crate-bitmask-enum-1.1.1 (c (n "bitmask-enum") (v "1.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l1k1s038xh494y4hr73w407kg5wkbvp7jic0ysm0w5ghi7s4ng1")))

(define-public crate-bitmask-enum-1.1.2 (c (n "bitmask-enum") (v "1.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07kap39crj76420201rzp0jdfnck5v57jp79jy34rlxyid1wk2mv")))

(define-public crate-bitmask-enum-1.1.3 (c (n "bitmask-enum") (v "1.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gmh166lpv8d696zbf20s63hlnmia56dkf5v0k8jf4qpdziz461n")))

(define-public crate-bitmask-enum-2.0.0 (c (n "bitmask-enum") (v "2.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wwwcga0qbj9yfvrcimbmg872sv3zfk4kgm18384blwpcpj7sj3n")))

(define-public crate-bitmask-enum-2.0.1 (c (n "bitmask-enum") (v "2.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sbbikm1xy61rmbc47cr8zx4hs4lmyrbl27xnjpqa6pdhvb40gz3")))

(define-public crate-bitmask-enum-2.1.0 (c (n "bitmask-enum") (v "2.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cj2ycc8mh7rx6gkallbq6mgxvjf7i3b5r87h5g0b18c8bbk57px")))

(define-public crate-bitmask-enum-2.2.0 (c (n "bitmask-enum") (v "2.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kaxc9r8kvwa0ql7h2zf8ch9w24i8v14wh0qr3hqxvvz8r7vds1i")))

(define-public crate-bitmask-enum-2.2.1 (c (n "bitmask-enum") (v "2.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02mbf34qism9ajr0y5738s5pyggckxclw8cq4szxw7sc3gwmdm3q")))

(define-public crate-bitmask-enum-2.2.2 (c (n "bitmask-enum") (v "2.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qq14wq34jmlhdj2as3hwwrmr2jkvnp64gd3mnjras6amcl8bys9")))

(define-public crate-bitmask-enum-2.2.3 (c (n "bitmask-enum") (v "2.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h6g42ji9chy51l3jqpnj31k1jqm618g1h5vrm8zyh2pdmx7744r")))

(define-public crate-bitmask-enum-2.2.4 (c (n "bitmask-enum") (v "2.2.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08c77ab534v9jr19hzr41b5vf8gzzmj462rcb5j1y1w8x10mbcdg")))

