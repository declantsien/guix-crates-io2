(define-module (crates-io bi tm bitmap) #:use-module (crates-io))

(define-public crate-bitmap-1.0.0 (c (n "bitmap") (v "1.0.0") (h "0p342hqibnlzaca2hbv8n8nb1sksimvk76h3hp2q05qhv8ypfy35")))

(define-public crate-bitmap-1.0.1 (c (n "bitmap") (v "1.0.1") (h "0dxmipsn0fml6kvrdm8smdybfs1w9xv4gfr2kw473pfqi6a47czq")))

(define-public crate-bitmap-1.0.1-cargo1 (c (n "bitmap") (v "1.0.1-cargo1") (h "1q0gkr896bpb6xdivh1pb28pq8vpj4yjpb7ifikij58h0lcspjrz")))

(define-public crate-bitmap-1.0.2 (c (n "bitmap") (v "1.0.2") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0w74k2xccagkafapnf3l8nfmjzvcj5vy28ydi4isvxcdmpkgi97w")))

(define-public crate-bitmap-2.0.0 (c (n "bitmap") (v "2.0.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0ww5sxrdz362lhphbs2g6l6blmd3y8r61wvy9j8kjqbirbqx0h59") (y #t)))

(define-public crate-bitmap-3.0.0 (c (n "bitmap") (v "3.0.0") (d (list (d (n "quickcheck") (r "^0.2.26") (d #t) (k 2)))) (h "0lf5s60kjs6n5ahnw02707js4nv5v93zzdgqf7g5jb33d5gg1sgd") (f (quote (("no_std"))))))

(define-public crate-bitmap-3.0.1 (c (n "bitmap") (v "3.0.1") (d (list (d (n "quickcheck") (r "^0.2.26") (d #t) (k 2)))) (h "1v8palymvpzdvb0h2zx7gh6p6smgskra5y1b90n6aim3xgcvlyrb") (f (quote (("no_std"))))))

(define-public crate-bitmap-3.1.0 (c (n "bitmap") (v "3.1.0") (d (list (d (n "quickcheck") (r "^0.2.26") (d #t) (k 2)))) (h "1fii5nkxcll7mig94hxxg7cx0sq5wwgk0l7b74xyjhv4c9z999pk") (f (quote (("no_std")))) (y #t)))

(define-public crate-bitmap-3.1.1 (c (n "bitmap") (v "3.1.1") (d (list (d (n "quickcheck") (r "^0.2.26") (d #t) (k 2)))) (h "04i23kyhf15ylnv4c7pjr4zlk6sv5n52d03v7l8v50d4iz4k0q4l") (f (quote (("no_std"))))))

(define-public crate-bitmap-3.1.2 (c (n "bitmap") (v "3.1.2") (d (list (d (n "quickcheck") (r "^0.2.26") (d #t) (k 2)))) (h "0sf2myh0k6rx1fc6c3fvd8ff7k1qvd4bfgbrjc9zs33xxag24yaj") (f (quote (("no_std") ("collections"))))))

(define-public crate-bitmap-3.1.3 (c (n "bitmap") (v "3.1.3") (d (list (d (n "quickcheck") (r "^0.2.26") (d #t) (k 2)))) (h "1s815q2nqfd1b62sgd82mq27pdzvw28y88y4j8hb9yk785b8fzp9") (f (quote (("no_std") ("collections"))))))

