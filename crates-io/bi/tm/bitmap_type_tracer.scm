(define-module (crates-io bi tm bitmap_type_tracer) #:use-module (crates-io))

(define-public crate-bitmap_type_tracer-1.0.0 (c (n "bitmap_type_tracer") (v "1.0.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "182zj6cl9v5b4rj52g5bxl6zr6pn6gj3ihc0zzj94agn039ziv1l") (r "1.65")))

(define-public crate-bitmap_type_tracer-1.1.0 (c (n "bitmap_type_tracer") (v "1.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "locale_config") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kv0vr6i6k76dk8ajwygy2g23n3yxfxcfrwm13cnd0mnd794r05x") (r "1.65")))

