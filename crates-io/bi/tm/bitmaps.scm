(define-module (crates-io bi tm bitmaps) #:use-module (crates-io))

(define-public crate-bitmaps-1.0.0 (c (n "bitmaps") (v "1.0.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1rgmj9kcgdzf6bhfwynqpm2l3zzf6w5z822fs1y6nrgz6asbn475")))

(define-public crate-bitmaps-2.0.0 (c (n "bitmaps") (v "2.0.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "02xf994ak5bvgmh9jm9xj6ng04n2jdvfya77zlvmnchl16l3kq41")))

(define-public crate-bitmaps-2.1.0 (c (n "bitmaps") (v "2.1.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "18k4mcwxl96yvii5kcljkpb8pg5j4jj1zbsdn26nsx4r83846403") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitmaps-3.0.0 (c (n "bitmaps") (v "3.0.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "061fpcc5iz96npbynlf7h2x6a7sjapiw0mzan655dahasa5fpp9x") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitmaps-3.1.0 (c (n "bitmaps") (v "3.1.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "1jxvjsy3w7pah4j85k1qajxshlib2hpr06q69fadwpwwrmayqg1h") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitmaps-3.2.0 (c (n "bitmaps") (v "3.2.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)))) (h "00ql08pm4l9hizkldyy54v0pk96g7zg8x6i72c2vkcq0iawl4dkh") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitmaps-3.2.1 (c (n "bitmaps") (v "3.2.1") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.4.0") (d #t) (k 2)))) (h "1mivd3wyyham6c8y21nq3ka29am6v8hqn7lzmwf91aks2fq89l51") (f (quote (("std") ("default" "std"))))))

