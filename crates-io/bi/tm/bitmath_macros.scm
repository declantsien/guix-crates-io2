(define-module (crates-io bi tm bitmath_macros) #:use-module (crates-io))

(define-public crate-bitmath_macros-0.0.1 (c (n "bitmath_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1w49jxpvjmlzf2vvl5kympfmvsjk239q0s5i4c345bf0780l2wis")))

