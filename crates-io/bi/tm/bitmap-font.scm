(define-module (crates-io bi tm bitmap-font) #:use-module (crates-io))

(define-public crate-bitmap-font-0.0.1 (c (n "bitmap-font") (v "0.0.1") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)))) (h "0v7qyn2b7li50hi5cahn4wysz80g61ddqk77ayw95kc4xc2fcdjb")))

(define-public crate-bitmap-font-0.1.0 (c (n "bitmap-font") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)))) (h "1imy3pr8pm7j39liqpghs186gd6j5vnz4am281mjfkdcpnvjilfk")))

(define-public crate-bitmap-font-0.1.1 (c (n "bitmap-font") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)))) (h "094djxkq8a501bwdx51rq9f1aqrckpdw89zzjixihgy8g3s4h8j2")))

(define-public crate-bitmap-font-0.2.0 (c (n "bitmap-font") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)))) (h "1yikaxk3nqh9jlzkc6z6bv1qa1c6f45j2l4ryfi0j0vwg0lgpngg")))

(define-public crate-bitmap-font-0.2.1 (c (n "bitmap-font") (v "0.2.1") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)))) (h "1al8x0bdvzsrnql3kilkw7igi8id3zvm1wf195hxbf4vq5zpwqfd")))

(define-public crate-bitmap-font-0.2.2 (c (n "bitmap-font") (v "0.2.2") (d (list (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)))) (h "1c2rcgiygr3pkbfgr7s9s98j1iq2aw4gx2kr6zkm290qk77kddbm") (r "1.57")))

(define-public crate-bitmap-font-0.3.0 (c (n "bitmap-font") (v "0.3.0") (d (list (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)))) (h "079ibwp87dgvixk9zwbw7lfjl2vkg7ighjhd9qkfvgp25cm7a4x9") (r "1.61")))

