(define-module (crates-io bi tm bitmap_copy) #:use-module (crates-io))

(define-public crate-bitmap_copy-0.1.0 (c (n "bitmap_copy") (v "0.1.0") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0l88g1cgwhs4rkis4cn0lmfp0xj2m25smkvrv12qgvq6162gn9f7")))

(define-public crate-bitmap_copy-0.1.1 (c (n "bitmap_copy") (v "0.1.1") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0z6licvhkk6wiibp3ravdmg1n8k12wfi3smwjmbiq8pfjncmdj6h")))

(define-public crate-bitmap_copy-0.1.2 (c (n "bitmap_copy") (v "0.1.2") (d (list (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1hhb40460924hj4ik71kvvgznvjla7xpbjn47wxx6lagbhakry2j")))

