(define-module (crates-io bi tm bitmatrix) #:use-module (crates-io))

(define-public crate-bitmatrix-0.1.0 (c (n "bitmatrix") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bch58slab0w02lv403nwi01anbcyii4k8p90hzrns2pmgm0wv36") (f (quote (("serde_support" "serde" "bitvec/serde"))))))

