(define-module (crates-io bi tm bitmaptrie) #:use-module (crates-io))

(define-public crate-bitmaptrie-0.9.0 (c (n "bitmaptrie") (v "0.9.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1zp52cbl5gwa26675z50sr2r02v6xf6y54hmk7b8pa49qsnq8c66")))

(define-public crate-bitmaptrie-1.0.0 (c (n "bitmaptrie") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1n73b305s56177b0jrpa5gvhz6miqdsbyf4gcq3z4w53ixfjdfs3")))

(define-public crate-bitmaptrie-1.0.1 (c (n "bitmaptrie") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0yd6b2p0lvykc80cj9a7dh362bd4w1a4n6ql134hfj69zl9cg784")))

(define-public crate-bitmaptrie-1.1.0 (c (n "bitmaptrie") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0ygsiglv7dz65p6qqjid4wxswh7aa2k986kpi3g1dgn55x7s9j5r") (y #t)))

(define-public crate-bitmaptrie-1.1.1 (c (n "bitmaptrie") (v "1.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1dj7vzyfi2hjjw2042x940li9lri8f2c77jjj8x9y9ap8q50p52j")))

(define-public crate-bitmaptrie-1.1.2 (c (n "bitmaptrie") (v "1.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1g3kinkaap79hxqqssjmr51p4yyizwj0pnqzpxvlfj1azdppvgln")))

(define-public crate-bitmaptrie-1.3.0 (c (n "bitmaptrie") (v "1.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0fbz2jj07pbhav7l08cc4x8x1ggkh498qnrd0ng5x3dag4f0kdic")))

(define-public crate-bitmaptrie-1.3.1 (c (n "bitmaptrie") (v "1.3.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0wlmmjlmwr5vgy8xllxq4wllw9jc97amwcbc3rm2zi2pa67bds04")))

(define-public crate-bitmaptrie-2.0.0 (c (n "bitmaptrie") (v "2.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0id455pcq4d00lav0i4xl0k6q0n2gmwhw0g67d6am174p255n7j2")))

