(define-module (crates-io bi tm bitmapx) #:use-module (crates-io))

(define-public crate-bitmapx-0.1.0 (c (n "bitmapx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "resvg") (r "^0.18.0") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.6.1") (d #t) (k 0)) (d (n "usvg") (r "^0.18.0") (d #t) (k 0)))) (h "0zyq820s99lvkqym8yy5n13jjy1p1ffa8vbkgkspx494akwhvz7m") (y #t)))

