(define-module (crates-io bi tm bitmath) #:use-module (crates-io))

(define-public crate-bitmath-0.0.1 (c (n "bitmath") (v "0.0.1") (d (list (d (n "bitmath_macros") (r "^0.0.1") (d #t) (k 0)))) (h "04dff2b5c4ymq318vcw41cz35a4gqpm82l7sw82n5bfs9qpi42mk")))

(define-public crate-bitmath-0.0.2 (c (n "bitmath") (v "0.0.2") (h "0xlw3p5qfmi30fid1l7wdq5vqn8wy743bsfxfpfj0nqpqwslspwk")))

