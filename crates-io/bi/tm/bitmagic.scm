(define-module (crates-io bi tm bitmagic) #:use-module (crates-io))

(define-public crate-bitmagic-0.1.1+bitmagic.7.3.1 (c (n "bitmagic") (v "0.1.1+bitmagic.7.3.1") (d (list (d (n "bitmagic-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0zzxgb19wlvyqjsx2vlnidrz32nyck5x3vnll2336pcmg1kjdqm0") (f (quote (("bindgen" "bitmagic-sys/bindgen"))))))

(define-public crate-bitmagic-0.1.2+bitmagic.7.3.1 (c (n "bitmagic") (v "0.1.2+bitmagic.7.3.1") (d (list (d (n "bitmagic-sys") (r "^0.1.1") (d #t) (k 0)))) (h "130rrwgyn5wqmg10xj1r8djwm3cq6k8krzklzw10w4x5xvyfgc0g") (f (quote (("bindgen" "bitmagic-sys/bindgen"))))))

(define-public crate-bitmagic-0.2.0+bitmagic.7.3.1 (c (n "bitmagic") (v "0.2.0+bitmagic.7.3.1") (d (list (d (n "bitmagic-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0w754dmjs7cxhivwir1sv790v7f2302096fhaym65flj6ksx70nr") (f (quote (("bindgen" "bitmagic-sys/bindgen"))))))

(define-public crate-bitmagic-0.2.1+bitmagic.7.4.0 (c (n "bitmagic") (v "0.2.1+bitmagic.7.4.0") (d (list (d (n "bitmagic-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1fdr2fcsss4cjwh4m8fv7j69vbfdc0y3ld9lh4h3hmpmghk77677") (f (quote (("bindgen" "bitmagic-sys/bindgen"))))))

(define-public crate-bitmagic-0.2.2+bitmagic.7.5.0 (c (n "bitmagic") (v "0.2.2+bitmagic.7.5.0") (d (list (d (n "bitmagic-sys") (r "^0.2.0") (d #t) (k 0)))) (h "01j2bhk3q080d0m2g1h1kjgl4i3axsrv5283vwkq5yshva89p2l6") (f (quote (("bindgen" "bitmagic-sys/bindgen"))))))

(define-public crate-bitmagic-0.2.3+bitmagic.7.6.0 (c (n "bitmagic") (v "0.2.3+bitmagic.7.6.0") (d (list (d (n "bitmagic-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1va27wck10bhkpsn4qjjrl4bigy8238xpxfvfd582bigm4cyhp0i") (f (quote (("bindgen" "bitmagic-sys/bindgen"))))))

(define-public crate-bitmagic-0.2.4+bitmagic.7.7.7 (c (n "bitmagic") (v "0.2.4+bitmagic.7.7.7") (d (list (d (n "bitmagic-sys") (r "^0.2.0") (d #t) (k 0)))) (h "070j5a46jgkhfqcf240v28a830cd4f5mswp3zxfj2791ciny8gx3") (f (quote (("bindgen" "bitmagic-sys/bindgen"))))))

