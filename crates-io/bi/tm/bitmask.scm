(define-module (crates-io bi tm bitmask) #:use-module (crates-io))

(define-public crate-bitmask-0.1.0 (c (n "bitmask") (v "0.1.0") (h "1da48ipi6qpdscbzp9fmdywk76h02jdbm120qq48p94vg9hb2glq")))

(define-public crate-bitmask-0.1.1 (c (n "bitmask") (v "0.1.1") (h "0xd6i3xhx10xrgj1kyhqr4psj820953b245mimg97b91d8fn28j6")))

(define-public crate-bitmask-0.2.0 (c (n "bitmask") (v "0.2.0") (h "0zldkskq19lkzl97zy0l9l6n3ynbqyn2ayzx1rxsxzsbncpwvqr4")))

(define-public crate-bitmask-0.3.0 (c (n "bitmask") (v "0.3.0") (h "110vlzxmm0x0nbrhzg9prpvb37b44p24xlxplsvxssyp4p4gpagk")))

(define-public crate-bitmask-0.4.0 (c (n "bitmask") (v "0.4.0") (h "1frjkzvz3a61rpind5gly4yk1bghhwp1qnmkvziwqgxxsn6bplbm")))

(define-public crate-bitmask-0.5.0 (c (n "bitmask") (v "0.5.0") (h "1bbyd12wclwz446c05bxhb7ncrdbvzwg8wx4hy91k1gmyvcv7aax") (f (quote (("std") ("default" "std"))))))

