(define-module (crates-io bi ll bill) #:use-module (crates-io))

(define-public crate-bill-0.1.1 (c (n "bill") (v "0.1.1") (d (list (d (n "claude") (r "^0.1") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^0.4") (d #t) (k 0)))) (h "0jdjpiz0cb770w9xywgpm2r7lynb4m8w0dala99fbihsa9pc3xvz")))

(define-public crate-bill-0.2.0 (c (n "bill") (v "0.2.0") (d (list (d (n "claude") (r "^0.2") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "maplit") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qlwlzxjriaiigj07zvzc58ds3385a1zng0b5kjfjm40h4rnidf2") (f (quote (("serialization" "serde" "serde_json" "serde_derive") ("default"))))))

(define-public crate-bill-0.3.0 (c (n "bill") (v "0.3.0") (d (list (d (n "claude") (r "^0.3") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "maplit") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fywjlx7my2hkjk61j17arz5rpzb5y31iypbkagbm4gjfx87z7sg") (f (quote (("serialization" "serde" "serde_json" "serde_derive") ("default"))))))

(define-public crate-bill-0.3.1 (c (n "bill") (v "0.3.1") (d (list (d (n "claude") (r "^0.3") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "05pwmq3hc29h8421x89ig1qn6db12xrjlgdwx11wbs2hr0d9rssf") (f (quote (("serialization" "serde" "serde_json") ("default"))))))

(define-public crate-bill-0.4.0 (c (n "bill") (v "0.4.0") (d (list (d (n "claude") (r "^0.3") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qzjkm6hma8pjfidn9z202zq1hw52bwx2hbl2wvb6qnhwyidadnh") (f (quote (("serialization" "serde" "serde_json") ("default"))))))

(define-public crate-bill-0.4.1 (c (n "bill") (v "0.4.1") (d (list (d (n "claude") (r "^0.3") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1h5ls75l2mvad0147j653ykc7a85yxsn8ikgxbgqr35vmbxbwgmv") (f (quote (("serialization" "serde" "serde_json") ("default"))))))

(define-public crate-bill-0.4.2 (c (n "bill") (v "0.4.2") (d (list (d (n "claude") (r "^0.3") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0lfdv3i3w14zb2l8ngryjsb9byhvw622yf7vd04icrf7jq7lf990") (f (quote (("serialization" "serde" "serde_json") ("default"))))))

