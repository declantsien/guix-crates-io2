(define-module (crates-io bi ll billios) #:use-module (crates-io))

(define-public crate-billios-0.1.0 (c (n "billios") (v "0.1.0") (h "056aa5vrm5rx5mj10grbhj3hd1szzjphxz55c7hg00iw8vmwm8v9")))

(define-public crate-billios-0.1.1 (c (n "billios") (v "0.1.1") (h "1drz55xcwk0crvm04f9vwp11h27s6kjqy4qi39vkf3vyygj6sn4c")))

(define-public crate-billios-0.2.0 (c (n "billios") (v "0.2.0") (h "19cmwachi7k1nb5pcd4c5p4v3hq2795iv3875m1w83c1a3a028ii")))

