(define-module (crates-io bi ll billy) #:use-module (crates-io))

(define-public crate-billy-0.1.0 (c (n "billy") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1z1c850yyghjhwb47nfnhsrl0012far36cka5fyy90kqmjh02drp")))

(define-public crate-billy-0.1.1 (c (n "billy") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "01srbgvlk68akl375r3ksxchr9h5dg3cyq5axmki68jksnwrwcrd")))

(define-public crate-billy-0.1.2 (c (n "billy") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "05g5sm29n1gmryfvvxhy2544274vpwq25lzcd1wjh52986gzmx85")))

(define-public crate-billy-0.1.3 (c (n "billy") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0fg8hzwvx4bpyz0w26aig4k26bn3jlxag3qr6bpcn9rng8rx1v50")))

