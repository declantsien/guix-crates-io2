(define-module (crates-io bi ll billboard) #:use-module (crates-io))

(define-public crate-billboard-0.1.0 (c (n "billboard") (v "0.1.0") (d (list (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1vk5xq9zvkc1qjm9vvlxkkxwdb1y843c7ngvxh1wm1w8nxk9m4ac")))

(define-public crate-billboard-0.2.0 (c (n "billboard") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0lgd1ngja8c0mlagrqz26shrgf3slk1hrg0ziikjyi6miqdji8j3")))

