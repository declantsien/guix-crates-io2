(define-module (crates-io bi ll billow) #:use-module (crates-io))

(define-public crate-billow-0.1.0 (c (n "billow") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "gif") (r "^0.11.4") (d #t) (k 2)) (d (n "image") (r "^0.24.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0v4d0gbjcna17dqpdag8s77mwwjzavby64nsa5iw1mhrdqss7a7g") (f (quote (("default" "image")))) (s 2) (e (quote (("image" "dep:image") ("bevy" "dep:bevy"))))))

(define-public crate-billow-1.0.0 (c (n "billow") (v "1.0.0") (d (list (d (n "bevy") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "gif") (r "^0.11.4") (d #t) (k 2)) (d (n "image") (r "^0.24.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0yv2l6bp93481bc1qpdd94mxiwvd1dqswkk6ny7l70w43dg0gm1q") (f (quote (("default" "image")))) (y #t) (s 2) (e (quote (("image" "dep:image") ("bevy" "dep:bevy"))))))

(define-public crate-billow-0.2.0 (c (n "billow") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "gif") (r "^0.11.4") (d #t) (k 2)) (d (n "image") (r "^0.24.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1x7mwl42cyw8azx9774p171n11wvwzm18avla87jaazz932fdfj6") (f (quote (("default" "image")))) (s 2) (e (quote (("image" "dep:image") ("bevy" "dep:bevy"))))))

