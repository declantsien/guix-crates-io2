(define-module (crates-io bi ll billig) #:use-module (crates-io))

(define-public crate-billig-0.0.1 (c (n "billig") (v "0.0.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)))) (h "13xwgjc3zn6xfmvnycdg8zq7vakggdrxrb7fkgqikls2h6kqwf6f")))

(define-public crate-billig-0.1.0 (c (n "billig") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)))) (h "1z8vglb7m631zcfvgri0dc8pisr86g3lnfsfgdl5gz0ixbmlh0zl")))

(define-public crate-billig-0.2.0 (c (n "billig") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)))) (h "16cjchxqrva04nf6p9r3kdjzfr5ihg2s4xhy9dc41vc89h2a4aig")))

