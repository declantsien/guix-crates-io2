(define-module (crates-io bi og biogarden) #:use-module (crates-io))

(define-public crate-biogarden-0.1.0 (c (n "biogarden") (v "0.1.0") (d (list (d (n "allwords") (r "^0.1.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_isaac") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "statrs") (r "^0.15") (d #t) (k 0)))) (h "1f1ira4srcz87hnsb1n4y77zqx7igmargbmgfh331x82iy2957wq")))

