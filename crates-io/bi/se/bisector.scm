(define-module (crates-io bi se bisector) #:use-module (crates-io))

(define-public crate-bisector-0.1.0 (c (n "bisector") (v "0.1.0") (d (list (d (n "semver") (r "^1") (d #t) (k 2)))) (h "0l5nkjqi2q155giyc0hgfhq0grgwcr25synk782wp2m8awd6z0ih")))

(define-public crate-bisector-0.2.0 (c (n "bisector") (v "0.2.0") (d (list (d (n "semver") (r "^1") (d #t) (k 2)))) (h "1yp338q736frik8jz72c3pa06gimf3a60jwbz1ysvwp11wmvdxdf")))

(define-public crate-bisector-0.3.0 (c (n "bisector") (v "0.3.0") (d (list (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "0fl450j6y549prv7d80qyia5ifqkj66pza0czvmgc887dgpg1fx2") (f (quote (("testing_external_program_ewc"))))))

(define-public crate-bisector-0.4.0 (c (n "bisector") (v "0.4.0") (d (list (d (n "semver") (r "^1") (d #t) (k 2)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "0vrxz3w3nw1w9f5ds1nymhbqdypl0pdzgh2ssrzc0vppglag3vnz") (f (quote (("testing_external_program_ewc"))))))

