(define-module (crates-io bi se bisetmap) #:use-module (crates-io))

(define-public crate-bisetmap-0.1.0 (c (n "bisetmap") (v "0.1.0") (h "1gv3mbvskd5b3sxfxni81hwx75fiyfx6i4sysqdxcdhmiw148pn3")))

(define-public crate-bisetmap-0.1.1 (c (n "bisetmap") (v "0.1.1") (h "01nnn7jdf0jqv4n0faj6nzp2fpcvil2k2z32h0kb1w55dwcxvw29")))

(define-public crate-bisetmap-0.1.2 (c (n "bisetmap") (v "0.1.2") (h "0fjj15g3gbmbwfk8vjnyp09mx9nmhr2ijlvgnvbl8c3137wkiz40")))

(define-public crate-bisetmap-0.1.3 (c (n "bisetmap") (v "0.1.3") (h "1m6ha6ni8xzk2280fn6ba8z23yclk98dfrp81zppa36z7phpvq1y")))

(define-public crate-bisetmap-0.1.4 (c (n "bisetmap") (v "0.1.4") (h "0wfihpknrlxjjwxqwjbgxvwxj13g60f2mh8qlda2pc25h92c32cp")))

(define-public crate-bisetmap-0.1.5 (c (n "bisetmap") (v "0.1.5") (h "0rirw39jark9y92ayllk13cw0w0ihrl14rgblrq61fa6zk3rvzii")))

(define-public crate-bisetmap-0.1.6 (c (n "bisetmap") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06nav7bh8sd3c2sy2wx0d92jy2h57zikzbmh75jvv4ppivfp9j92")))

