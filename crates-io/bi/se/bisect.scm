(define-module (crates-io bi se bisect) #:use-module (crates-io))

(define-public crate-bisect-0.1.0 (c (n "bisect") (v "0.1.0") (h "0rf7wxq1x9dbpxldqhklwhnkrjy24vz4s6ycmpfqv4iwxv6h79z3")))

(define-public crate-bisect-0.2.0 (c (n "bisect") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0nmvi7lyl0ha7mfm6jlvb4wgdi2ml587b32asbn239vq31vf0xrh")))

