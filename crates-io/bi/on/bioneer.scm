(define-module (crates-io bi on bioneer) #:use-module (crates-io))

(define-public crate-bioneer-0.1.0 (c (n "bioneer") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.0") (d #t) (k 0)))) (h "0w1ghnr9lz42cmw5dpx70zdw81mynciv4wvc8xkbv00id0glzq4j")))

(define-public crate-bioneer-0.1.1 (c (n "bioneer") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.0") (d #t) (k 0)))) (h "0l7b50rpia3paqyvcl46459i20y7nbb0c8clvl61nmavs354xnnm")))

