(define-module (crates-io bi on bionic-ebooks) #:use-module (crates-io))

(define-public crate-bionic-ebooks-0.1.0 (c (n "bionic-ebooks") (v "0.1.0") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1ziivl1miy1k8y0cckskiw9sl1vi64fymn3x5gjd005y120mhfcj")))

(define-public crate-bionic-ebooks-0.1.1 (c (n "bionic-ebooks") (v "0.1.1") (d (list (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("deflate"))) (k 0)))) (h "1zz5ync9nahqhlk1n7z09f7bkn66k8cxhs670mlp1gnzap6ikwsd")))

