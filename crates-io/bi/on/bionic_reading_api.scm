(define-module (crates-io bi on bionic_reading_api) #:use-module (crates-io))

(define-public crate-bionic_reading_api-0.1.0 (c (n "bionic_reading_api") (v "0.1.0") (d (list (d (n "html2md") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "visible") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "06nn9qgqyhl5vswj13872pr8bci8jm93vmj5qwqwz9k0lnv75a26") (f (quote (("doc-tests" "visible"))))))

(define-public crate-bionic_reading_api-0.1.1 (c (n "bionic_reading_api") (v "0.1.1") (d (list (d (n "html2md") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "visible") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "1nm3xr00748c8ml9gfnsvbm945c21qbwwabqvnpr4rvv2y586gi2") (f (quote (("doc-tests" "visible"))))))

