(define-module (crates-io bi n2 bin2src) #:use-module (crates-io))

(define-public crate-bin2src-0.0.56 (c (n "bin2src") (v "0.0.56") (h "0m2i5ild6i9nwhaz81lxkrkiajfhq36qzmanvj5xfg7l7z5s1asn") (y #t)))

(define-public crate-bin2src-0.0.57 (c (n "bin2src") (v "0.0.57") (h "1k6lqq04ksf5ymbsp5ps4n2phnmdvqailp40313g9ib9sdka7m47")))

(define-public crate-bin2src-0.0.58 (c (n "bin2src") (v "0.0.58") (h "1fj6lym6qipxqdp9dmrci05k3d1l9jsq09s4csh9cp9vpjh0gghs")))

