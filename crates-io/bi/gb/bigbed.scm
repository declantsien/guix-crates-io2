(define-module (crates-io bi gb bigbed) #:use-module (crates-io))

(define-public crate-bigbed-0.1.0 (c (n "bigbed") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.13") (d #t) (k 0)))) (h "0r018av2c6nh3pzi1a3jccn0grwgiwqqc5a16c90bq3ln4vy0qhm")))

(define-public crate-bigbed-0.1.1 (c (n "bigbed") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)))) (h "0908znh702ywijx5pj2ckk4p5ykgfiw9jjv6d8am2pll9ivpchfx") (f (quote (("binary" "clap"))))))

(define-public crate-bigbed-0.1.2 (c (n "bigbed") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)))) (h "1wzcsmy7is1m370ikgla6k9idcvk3wmkjljqgdpx5az3r21ccqxf") (f (quote (("binary" "clap"))))))

(define-public crate-bigbed-0.1.3 (c (n "bigbed") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)))) (h "1dx6ssvspz5phaazbfmn3pmir8jmgig52a6rb5hx4zjyaapfnl82") (f (quote (("binary" "clap"))))))

(define-public crate-bigbed-0.2.0 (c (n "bigbed") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)))) (h "1kbcjb9gygsld38zs0sbvcqx5y1nl4sh5szhwidgvd8rz44b2w2r") (f (quote (("binary" "clap"))))))

