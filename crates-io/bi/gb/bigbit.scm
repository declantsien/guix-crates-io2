(define-module (crates-io bi gb bigbit) #:use-module (crates-io))

(define-public crate-bigbit-0.0.0 (c (n "bigbit") (v "0.0.0") (h "1di4jc981fybyajwkrb0d3n7wf5pvrc96r0v4rqi7qf73c9ynjpv") (y #t)))

(define-public crate-bigbit-0.0.1 (c (n "bigbit") (v "0.0.1") (h "19gnxmnbsi9bzkajwkr64mxizi48plchbwwzxwawabm2yy84c25g") (y #t)))

(define-public crate-bigbit-0.0.2 (c (n "bigbit") (v "0.0.2") (h "1g64m985kc8kcppjj29ixlp0z2m0ilg8bfnwrafc0y4dpk6c8b4j") (y #t)))

(define-public crate-bigbit-0.0.3 (c (n "bigbit") (v "0.0.3") (h "0zfyp89giwc4lfi05dmndmzaq03bqalxfxb7x7alvl48gj6ia86f")))

(define-public crate-bigbit-0.0.4 (c (n "bigbit") (v "0.0.4") (h "0m7x1cjgn7wkdjjmfq5hdckbsgwpnrfcxy707nkbi4y32r84pyrz") (f (quote (("default" "clippy") ("clippy"))))))

(define-public crate-bigbit-0.0.5 (c (n "bigbit") (v "0.0.5") (h "1m0aj2smr07qbarzsdgmbks8i42r2j6i8g46glm28g6a9k2qr4hv") (f (quote (("default" "clippy") ("clippy")))) (y #t)))

(define-public crate-bigbit-0.0.5-r (c (n "bigbit") (v "0.0.5-r") (h "0cr43pgciwkywhxplv4j86qkq4l45g5qypnm4k29xvx5wrn3skh2") (f (quote (("default" "clippy") ("clippy"))))))

(define-public crate-bigbit-0.0.6 (c (n "bigbit") (v "0.0.6") (h "08rn21y45qdgfrbwrxzkq4s0681ywklqcgrsh21razkmxz0ik4im") (f (quote (("default" "clippy") ("clippy"))))))

(define-public crate-bigbit-0.0.7 (c (n "bigbit") (v "0.0.7") (h "03fglbq06a8b4kwjc0pmybf3jmbx46ddxm4fwvnwcy7mkb1a7kw2") (f (quote (("default" "clippy") ("clippy"))))))

(define-public crate-bigbit-0.0.8 (c (n "bigbit") (v "0.0.8") (h "1is8sk05l6sxk50pc94vbyflixgwk8h3vr43s378lk9kz6hwqfy5") (f (quote (("std") ("default" "clippy" "std") ("clippy"))))))

