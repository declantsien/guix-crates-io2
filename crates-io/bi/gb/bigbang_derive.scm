(define-module (crates-io bi gb bigbang_derive) #:use-module (crates-io))

(define-public crate-bigbang_derive-0.1.0 (c (n "bigbang_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17g0l79whd9ffdmhqqp5asbyj68vxzh8s0m16819nvnnia0nvcj0")))

