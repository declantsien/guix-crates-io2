(define-module (crates-io bi gb bigbro) #:use-module (crates-io))

(define-public crate-bigbro-0.1.0 (c (n "bigbro") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jxx2plpmk3ab04nm8salm2693kzlhrifpd5zv4m87xk2a331f5b") (f (quote (("strict"))))))

(define-public crate-bigbro-0.2.0 (c (n "bigbro") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1147xmchx4hmw13dkz0p15dm4f5lmjzgwnq32rzrw3bw4sj84dil") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.0 (c (n "bigbro") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06hg22h2wcsl9l3yv94cpw9kyd79sfjlbd26a95i9n7n6y9ikv97") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.1 (c (n "bigbro") (v "0.3.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nv1r2czhylwxrz0k11s2ygb60avncxbqkwqbnj0b5j2n7rf7cm0") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.2 (c (n "bigbro") (v "0.3.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11hc47884im1p9vzjq4zzkarg1kmrjcg1cyhai8hb46rqvhffnpw") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.3 (c (n "bigbro") (v "0.3.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07jpc0pc50mxsmbcvmfh81kfbi9spgp698wag57npmcwp9527r7n") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.4 (c (n "bigbro") (v "0.3.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16i69p4bjk5ivrw6mjcw6sm1m96af8nmqjrkzrf5ncznb3j888rc") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.5 (c (n "bigbro") (v "0.3.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01k6zaf7zf9nj4xm20g0r54v8wn16i5g12q2vccmmyqiyym16xln") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.6 (c (n "bigbro") (v "0.3.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jds4b89yh5dwf9q42kyz2fh8wbk4w019gib77qcjwh73b9y9c3g") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.7 (c (n "bigbro") (v "0.3.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lzcgvlzi2mxnz2gbmr3yzyrpszl58ydbv8xdc1xrp8zsxwi4i09") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.8 (c (n "bigbro") (v "0.3.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11awgrsqarnxc66k96xshlcnjai2j3daz3is9qny93k46nla4adg") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.9 (c (n "bigbro") (v "0.3.9") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10f0k9wchzrh8b0g1pirg9i17zyrk74x5fs2ahszzii96j589kmr") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.10 (c (n "bigbro") (v "0.3.10") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0y02670bh7dm2l6c4i0v85cypr7r57c5fxgjlmsiyzp27dmsfzh2") (f (quote (("strict"))))))

(define-public crate-bigbro-0.3.11 (c (n "bigbro") (v "0.3.11") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h1r4zx7rnp0gbdhmq4jhrza14qf6msf95s7jhf07jrwskm578ki") (f (quote (("strict") ("noprofile" "cpuprofiler"))))))

(define-public crate-bigbro-0.3.12 (c (n "bigbro") (v "0.3.12") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0040h4nr1kjdky1ls5rvnn2jvz4p5nv4ddv4jfamgx1d8zbv1aq4") (f (quote (("strict") ("noprofile" "cpuprofiler"))))))

(define-public crate-bigbro-0.3.13 (c (n "bigbro") (v "0.3.13") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0igl1b201af8crimsx0kjjdpnsnhv78mdfwwkrhbz4i8xpcv7j4m") (f (quote (("strict") ("noprofile" "cpuprofiler"))))))

(define-public crate-bigbro-0.3.14 (c (n "bigbro") (v "0.3.14") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k3lxgq7kazm8bm1yh47p7ilg8afka1d4yaqvqz8v0148rk9c00w") (f (quote (("strict") ("noprofile" "cpuprofiler"))))))

(define-public crate-bigbro-0.3.15 (c (n "bigbro") (v "0.3.15") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ihlnpcrqcmkxz7kwjdi3gqhz94g1b04cc35mh9qpj6z22002vbz") (f (quote (("strict") ("noprofile" "cpuprofiler"))))))

(define-public crate-bigbro-0.3.16 (c (n "bigbro") (v "0.3.16") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13w48r9233akyvyq9zgadl6kmnkf8i96r512fx2c0mq6441dv0f4") (f (quote (("strict") ("noprofile" "cpuprofiler"))))))

(define-public crate-bigbro-0.4.0 (c (n "bigbro") (v "0.4.0") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "seccomp-droundy") (r "^0.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0iwyqg29nj9vsc0rfay94w7fi7jdjhg9pyy4bfph9ckx21nfw086") (f (quote (("strict") ("noprofile" "cpuprofiler"))))))

(define-public crate-bigbro-0.5.0 (c (n "bigbro") (v "0.5.0") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "seccomp-droundy") (r "^0.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0a7a6sipy897gxs0w5h4z1gi1n9xcw56f8iysafn24cirgk88qqs") (f (quote (("strict") ("noprofile" "cpuprofiler"))))))

(define-public crate-bigbro-0.5.1 (c (n "bigbro") (v "0.5.1") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "seccomp-droundy") (r "^0.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1xs6mmpxbzji8gkhkvvr16smhhl3cm86y8c2zl5p31ia074ydy8z") (f (quote (("strict") ("noprofile" "cpuprofiler"))))))

(define-public crate-bigbro-0.5.2 (c (n "bigbro") (v "0.5.2") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "seccomp-droundy") (r "^0.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1rfwbi0x4427x8lnsf9apa9jh010vxzpra9c23i33kb27rxc1aiy") (f (quote (("strict") ("noprofile" "cpuprofiler"))))))

