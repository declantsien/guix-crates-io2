(define-module (crates-io bi gw bigwig2bam) #:use-module (crates-io))

(define-public crate-bigwig2bam-0.1.1 (c (n "bigwig2bam") (v "0.1.1") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "bigtools") (r "^0.1.11") (d #t) (k 0)) (d (n "bio") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0vb23wbxwhk771g3ap8nbkpdw2xrbs94r0wgrl46rwj9x46lwipx")))

(define-public crate-bigwig2bam-0.1.2 (c (n "bigwig2bam") (v "0.1.2") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "bigtools") (r "^0.1.11") (d #t) (k 0)) (d (n "bio") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0bfs0gj19371v9mrnlb80v83av2ka3zhnfryjvjh5jpdws9b2v49")))

(define-public crate-bigwig2bam-0.1.3 (c (n "bigwig2bam") (v "0.1.3") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "bigtools") (r "^0.1.11") (d #t) (k 0)) (d (n "bio") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1gcvapaql5c0i90jxp13p2nyji7jcm7kj31ijnm5gdp9j3isqyyg")))

(define-public crate-bigwig2bam-0.1.4 (c (n "bigwig2bam") (v "0.1.4") (d (list (d (n "bam") (r "^0.1.4") (d #t) (k 0)) (d (n "bigtools") (r "^0.1.11") (d #t) (k 0)) (d (n "bio") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "01z4ffphjlzbbangsiawja3gzvcdynadnzk5wkkkcny3f79sndlq")))

