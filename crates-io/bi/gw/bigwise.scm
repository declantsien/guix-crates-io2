(define-module (crates-io bi gw bigwise) #:use-module (crates-io))

(define-public crate-bigwise-0.1.0 (c (n "bigwise") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "12hkadq7bbjc8djn9la1xxqmjf8fc6fwssnxv21ykn2kvr3k5x0c")))

(define-public crate-bigwise-0.2.0 (c (n "bigwise") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0p01rd1m69ha5wyw0z7lyybla2x9jqs1xac25yl07dw3hqk29q4x")))

(define-public crate-bigwise-0.3.0 (c (n "bigwise") (v "0.3.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1j5mkn0qjyg5yv25f56rj4px6fhqy1ib814c4npm3khaqij5460d")))

(define-public crate-bigwise-0.4.0 (c (n "bigwise") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0jawy9dacdkp3szd5g48a31rqsbf0q6pfh92v9ss44k16sn53hgn")))

