(define-module (crates-io bi ma bimap_plus_map) #:use-module (crates-io))

(define-public crate-bimap_plus_map-0.1.0 (c (n "bimap_plus_map") (v "0.1.0") (d (list (d (n "bimap") (r "^0.5.2") (d #t) (k 0)))) (h "0brd62xxd1xxlpr03h6jh76m5v395r2y8lansfqaw4k6rv6mb2vy")))

(define-public crate-bimap_plus_map-0.1.1 (c (n "bimap_plus_map") (v "0.1.1") (d (list (d (n "bimap") (r "^0.5.2") (d #t) (k 0)))) (h "001q3sji4wh8k5fb96vqc4gvmp37hnni3sgdyzl8z84csy2qryyn")))

