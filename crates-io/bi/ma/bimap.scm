(define-module (crates-io bi ma bimap) #:use-module (crates-io))

(define-public crate-bimap-0.1.0 (c (n "bimap") (v "0.1.0") (h "1sh432p0717r54kx31hg38gv6l7iy8hyi42gpq2w2wa2bwbgzhp4")))

(define-public crate-bimap-0.1.1 (c (n "bimap") (v "0.1.1") (h "0dgs1ff3hsfpqydwq3ym48sh231q28s5dsmzifd0zczv6cy11fy0")))

(define-public crate-bimap-0.1.2 (c (n "bimap") (v "0.1.2") (h "1n4vykhz4bsrlgyg5040ylqqkybxkz9l5k93adnpa1q988448mgg")))

(define-public crate-bimap-0.1.3 (c (n "bimap") (v "0.1.3") (h "01ifbwgr1l4jygmna56axw6l4ki19vhid6xb24si5l750smimamh")))

(define-public crate-bimap-0.1.4 (c (n "bimap") (v "0.1.4") (h "0l7sh25586cjyrbl2bjmh7yimgrchnd80x8c5ndgvkgwxikq5in7")))

(define-public crate-bimap-0.1.5 (c (n "bimap") (v "0.1.5") (h "0g9xj8qzv5hbnyafg9mrm3par1gik0hqm50sqvx8n1rp4ac2na3b")))

(define-public crate-bimap-0.2.0 (c (n "bimap") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "0rljijs8cxhgxw7v3s1c5c164gnqybnqbs4gaksn450amyz4apjh") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.3.0 (c (n "bimap") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dcfl3jd9k73gwbgpi6aycbl4ckmzcm8g1ni8pp9zcgwgi5a73jj") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.3.1 (c (n "bimap") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "07wpz69nw2fa1x4nmxayhmm3yv8snp798qcj4wm3kjifqzjpfqa4") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.4.0 (c (n "bimap") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06srhxkzbxnzm9l1lmmhgc21hq3azaf629ykfji4wwnp9zr08ckq") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.5.0 (c (n "bimap") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1z5d3l25q7s6wm4ag7aprbxs2jlhan47926b08hp6lsvi8cv4767") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.5.1 (c (n "bimap") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12gysa3hfx755sznzy2f6631b9a6rv7s90drpz7y58x6yrh8j8la") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.5.2 (c (n "bimap") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10g765qns7hr14xgqgwiia6fwnh7n2kkb7y19bw8xv1xrpb6m023") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.5.3 (c (n "bimap") (v "0.5.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "175kym82ibwazg69s3dg98dyrjpmbgf4vnh46pfsmchyz2vc8a1m") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.6.0 (c (n "bimap") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1grj1hwsyisz7691z9ir5jhx5wq5ja5l2x5zg0r7fa1iy2w74azr") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.6.1 (c (n "bimap") (v "0.6.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01vb8id3lpc6ipx9c22dq4nnf2cs9rk6l6jc7qg8m8y8pg51gbjh") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.6.2 (c (n "bimap") (v "0.6.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0jyl53bi572kxaayc2py797vx8g12p4bmn258m69dimm9qjma15w") (f (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.6.3 (c (n "bimap") (v "0.6.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1xx4dns6hj0mf1sl47lh3r0z4jcvmhqhsr7qacjs69d3lqf5y313") (f (quote (("std") ("default" "std"))))))

