(define-module (crates-io bi je bijection) #:use-module (crates-io))

(define-public crate-bijection-0.1.0 (c (n "bijection") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1n3kfn0vzij05nqb12175cp9p00fa8gry0039yfxl35c0fbzsv7j")))

(define-public crate-bijection-0.1.1 (c (n "bijection") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "16pxqfp44gx3q37c0f91vzg1742hd0md8wrnjpifla1qqd8w13kq")))

(define-public crate-bijection-0.1.2 (c (n "bijection") (v "0.1.2") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "matches") (r "^0.1.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0zvzbji1wa320qal34a19jyq40q8yigzs3dz74ycjc8p3g0k4clr")))

