(define-module (crates-io bi tu bitutils) #:use-module (crates-io))

(define-public crate-bitutils-2.0.0 (c (n "bitutils") (v "2.0.0") (h "0yhg2s6gys4b27yv5lm0aqsj78hp2l598lw6705q3an0nvqzn213") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bitutils-2.0.1 (c (n "bitutils") (v "2.0.1") (h "1iapmlgn3maqxs06bvl31lmrzsdziws0jyy7qdksp7aagpridqn5") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bitutils-3.0.0 (c (n "bitutils") (v "3.0.0") (d (list (d (n "bf-impl") (r "^1.0") (d #t) (k 0)))) (h "07jx5azn1wm7ws4xxwqjb7b8nclj97frv2h94ga67pgpcmp54jg9") (f (quote (("use_std" "bf-impl/use_std") ("default" "use_std"))))))

(define-public crate-bitutils-3.0.1 (c (n "bitutils") (v "3.0.1") (d (list (d (n "bf-impl") (r "^1.0") (k 0)))) (h "1nqclzh5xdk5s1lmy4hsryj4s8lb0zgd4vhy8r2jxk9z9qw1v8a9") (f (quote (("use_std" "bf-impl/use_std") ("default" "use_std"))))))

