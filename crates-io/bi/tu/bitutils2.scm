(define-module (crates-io bi tu bitutils2) #:use-module (crates-io))

(define-public crate-bitutils2-0.1.0 (c (n "bitutils2") (v "0.1.0") (h "10jx13dl7pr95k4fkd9r2fspzzympml9vxqjbwdrkn9qiq93jsq2")))

(define-public crate-bitutils2-0.1.1 (c (n "bitutils2") (v "0.1.1") (h "0lk16sap59g7q14az9iinh0dm9ypiyd1sq6gr1bv3jksasgylpb3")))

