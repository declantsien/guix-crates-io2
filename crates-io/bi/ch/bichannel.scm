(define-module (crates-io bi ch bichannel) #:use-module (crates-io))

(define-public crate-bichannel-0.0.1 (c (n "bichannel") (v "0.0.1") (h "03nv1mdhy0107q7m4qzmx6aayfmjdxcw6in5k2rxcaykkh3prcf1")))

(define-public crate-bichannel-0.0.2 (c (n "bichannel") (v "0.0.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0hpnv3ry6z8wyd6n6zak4x0jx5b79g87a0brp985ryp02ijvzr76") (f (quote (("crossbeam" "crossbeam-channel"))))))

(define-public crate-bichannel-0.0.3 (c (n "bichannel") (v "0.0.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "11s9fjshbaxmff5wg5a5z2jmnnkc84lbkzxw2ji6y02fdsv81bck") (f (quote (("crossbeam" "crossbeam-channel"))))))

(define-public crate-bichannel-0.0.4 (c (n "bichannel") (v "0.0.4") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0fr19i46q0qs5vmidpfjhyl3n4mdhjmyvyyy9n9ndl9n7m3zbgd4") (f (quote (("crossbeam" "crossbeam-channel"))))))

