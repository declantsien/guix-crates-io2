(define-module (crates-io bi nn binn-ir) #:use-module (crates-io))

(define-public crate-binn-ir-0.0.2 (c (n "binn-ir") (v "0.0.2") (h "0vg506vnaiglvf3dajh1nrxwyh01mwi1v6wn19z0f0jqja4pvndn")))

(define-public crate-binn-ir-0.1.0 (c (n "binn-ir") (v "0.1.0") (h "11kn7sfzms10gz007rgqy1wmx4csvv99wyzpx5i2w6gpcrdsid25")))

(define-public crate-binn-ir-0.2.0 (c (n "binn-ir") (v "0.2.0") (h "0qbpivx9d0hirbb253icgvdmz4r07534a09hw33xg52a9bywv8k1")))

(define-public crate-binn-ir-0.3.0 (c (n "binn-ir") (v "0.3.0") (h "0wp930ri1zjg53raimp2pm9hld8bp0zbnvd0fwpr6ipwpkpjwxpk")))

(define-public crate-binn-ir-0.4.0 (c (n "binn-ir") (v "0.4.0") (h "00p25280aj1xiva1g709q9w3qgarc41cfchnfkanx3kyrxfp9fv2")))

(define-public crate-binn-ir-0.5.0 (c (n "binn-ir") (v "0.5.0") (h "0yz3gdw4azn12hk0prx514nvdgf2x917m1622wkzgm3hx5iqfdbp")))

(define-public crate-binn-ir-0.5.1 (c (n "binn-ir") (v "0.5.1") (h "0a2003vrwhv3sgdgcsv4cb9zqz617fk3lrj14swshxdgws2h6sgx")))

(define-public crate-binn-ir-0.6.0 (c (n "binn-ir") (v "0.6.0") (h "0gdsqgyxy4yzqxwy5p3x0q5fs71dlws4jqz7mqvks6mzwkgsbgcq")))

(define-public crate-binn-ir-0.7.0 (c (n "binn-ir") (v "0.7.0") (h "1pw2yaqax18yzxb2lzddd14s060q1hi3w3g74j0f9vn14x64qc7b")))

(define-public crate-binn-ir-0.8.0 (c (n "binn-ir") (v "0.8.0") (h "033ymz0danzvwllgfvnxpyqap2zbkywnzzzs4389xxrm5y53ma9q")))

(define-public crate-binn-ir-0.9.0 (c (n "binn-ir") (v "0.9.0") (h "0sr94kas5hpk2hra7cd15lrzym6wzlvn2njd4h8490b8d76s47pl")))

(define-public crate-binn-ir-0.10.0 (c (n "binn-ir") (v "0.10.0") (h "18k9jqf4q9bm3bppm2diqb03k9ra83phpqmm870nfxy7ynj9bm1q")))

(define-public crate-binn-ir-0.11.0 (c (n "binn-ir") (v "0.11.0") (h "0hk37aad6mgwxjd5c7kxxs3xnm73r4zjvcddq8ks7bicfg5671hv")))

(define-public crate-binn-ir-0.12.0 (c (n "binn-ir") (v "0.12.0") (h "1r5068mpc0gxfs3bzryrgm72yys6a9iq9l61nyg395mws6gdwaw5")))

(define-public crate-binn-ir-0.13.0 (c (n "binn-ir") (v "0.13.0") (h "01bckp5m6v7z0wgyqprl6ras342bxskzqp9nn2py99xqpw0jzh6q")))

(define-public crate-binn-ir-0.13.1 (c (n "binn-ir") (v "0.13.1") (h "1n1wgjlrlqkfx11zyjaa7c26nnqc1wc92qqjkd6n29dpniznh2aq")))

(define-public crate-binn-ir-0.14.0 (c (n "binn-ir") (v "0.14.0") (h "1dndj35mcygphkbbyx612pg08ksyw6a925w5rdg04srw9bk1ikjv") (f (quote (("std"))))))

(define-public crate-binn-ir-0.14.1 (c (n "binn-ir") (v "0.14.1") (h "1xlx1hhmwbqnd45l5mjp58w4vm2fapbv75m7wq81a0vm0zxhfb4q") (f (quote (("std"))))))

(define-public crate-binn-ir-0.14.2 (c (n "binn-ir") (v "0.14.2") (d (list (d (n "kib") (r "^3") (d #t) (k 2)))) (h "1jblxsdln063xzsqvyi6hkvk984l1vbdl6jgi3zwv9mvig8a4g2l") (f (quote (("std"))))))

(define-public crate-binn-ir-0.14.3 (c (n "binn-ir") (v "0.14.3") (d (list (d (n "kib") (r "^4") (d #t) (k 2)))) (h "1vrqk2pn5jbk301gibkjpxrdqf0vi2vfwmc6z2fa1s923ar10bf3") (f (quote (("std"))))))

(define-public crate-binn-ir-0.15.0 (c (n "binn-ir") (v "0.15.0") (d (list (d (n "kib") (r "^4") (d #t) (k 2)))) (h "0c19jfnzygg7hhnz263kh3hmadqajpilj318nxk26z7i6is91q9a") (f (quote (("std"))))))

(define-public crate-binn-ir-0.16.0 (c (n "binn-ir") (v "0.16.0") (d (list (d (n "kib") (r "^4") (d #t) (k 2)))) (h "1xqy5qs8idvxvcl9s520pls221xmz7iymrnkymxnrxlh14iqjb5b") (f (quote (("std"))))))

(define-public crate-binn-ir-0.17.0 (c (n "binn-ir") (v "0.17.0") (d (list (d (n "kib") (r ">=5, <6") (d #t) (k 2)))) (h "1c2gpk17sw1rd1c4ps85d2z7kcpnkmwc3h7ii2capabrmfrdm60q") (f (quote (("std"))))))

(define-public crate-binn-ir-0.17.1 (c (n "binn-ir") (v "0.17.1") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)))) (h "1zj332q4f2fy2hl5pp65sv5rv0asainf31ryfqmk8xnsvnwzhb89") (f (quote (("std"))))))

(define-public crate-binn-ir-0.17.2 (c (n "binn-ir") (v "0.17.2") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)))) (h "0gnhdd3wbqc87zrqplpxf8zlrlchxc8ikd336wpwn9nf2baji13j") (f (quote (("std"))))))

(define-public crate-binn-ir-0.17.3 (c (n "binn-ir") (v "0.17.3") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)))) (h "1srwnnaf624jdqrikwbx9j5zd0wyv1fffl99q5sjjc65x1137fsw") (f (quote (("std"))))))

