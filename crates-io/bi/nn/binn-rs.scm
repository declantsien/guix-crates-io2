(define-module (crates-io bi nn binn-rs) #:use-module (crates-io))

(define-public crate-binn-rs-0.1.0 (c (n "binn-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "12r0iqjs1xgxg75r5ymlbhnryg9q227z5gn8fgphszpmj5zjy7ls")))

