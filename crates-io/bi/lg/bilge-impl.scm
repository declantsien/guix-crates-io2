(define-module (crates-io bi lg bilge-impl) #:use-module (crates-io))

(define-public crate-bilge-impl-0.1.0 (c (n "bilge-impl") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1inqglhjhqyznbh7canrlxrfnlyx49b8k4as5016rhlbihd2bg5y")))

(define-public crate-bilge-impl-0.1.1 (c (n "bilge-impl") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02n45g0qrgksdp63znn4jvd7ylzypdma4p4ad0nc4r6vm1xc9r8n") (f (quote (("nightly") ("default"))))))

(define-public crate-bilge-impl-0.1.2 (c (n "bilge-impl") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kajxzf0hy3yzgd5v6ca52h1apq8jcw69j61j0fc7shglsbyc3d7") (f (quote (("nightly") ("default"))))))

(define-public crate-bilge-impl-0.1.3 (c (n "bilge-impl") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "196idpxbw2s4794zfn34vyimxc537zl5akdrinv7qhsfxbimvjkm") (f (quote (("nightly") ("default"))))))

(define-public crate-bilge-impl-0.1.4 (c (n "bilge-impl") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j29l1gcai3pg9phia2ksl9fydy522ia98zsia875dqcdvxdlnc2") (f (quote (("nightly") ("default"))))))

(define-public crate-bilge-impl-0.1.5 (c (n "bilge-impl") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09vfr90kllrqrj1g6l9k07cb7jz8wrq008k0jaqra5q0qcyhmzb0") (f (quote (("nightly") ("default"))))))

(define-public crate-bilge-impl-0.2.0 (c (n "bilge-impl") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-path") (r "^2.0") (d #t) (k 2)))) (h "1n5jml0c1z0np76ms0h5rxx19krblz46h84wycx29b9q4001xcgy") (f (quote (("nightly") ("default"))))))

