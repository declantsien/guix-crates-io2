(define-module (crates-io bi tw bitwarden2lastpass) #:use-module (crates-io))

(define-public crate-bitwarden2lastpass-0.1.0 (c (n "bitwarden2lastpass") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0fp5yvp9d0gn5j1dzyj8p5q62fgn0s2d267g5mcnw47fqspkc7i9")))

