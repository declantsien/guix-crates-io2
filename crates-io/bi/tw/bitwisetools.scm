(define-module (crates-io bi tw bitwisetools) #:use-module (crates-io))

(define-public crate-bitwisetools-1.0.0 (c (n "bitwisetools") (v "1.0.0") (h "00k5bw3j3f5ilhhaqiyinms49sd7ryawvqs6ivann9p1xi2gs3zj")))

(define-public crate-bitwisetools-1.0.1 (c (n "bitwisetools") (v "1.0.1") (h "1cbif6py0gp9asfv9qcpwhl296lfq44548p4mcds43sp9xvrp1lv")))

(define-public crate-bitwisetools-1.0.2 (c (n "bitwisetools") (v "1.0.2") (h "1djqfabvq7llvc9ac08cgpqfh6p350gz9b3a1yw3dvwrni1kxk85")))

(define-public crate-bitwisetools-1.0.3 (c (n "bitwisetools") (v "1.0.3") (h "1rpq93zydzzgds3zvpr9098yab2zhk0lw0p5v95skyh0i51lnb9g")))

