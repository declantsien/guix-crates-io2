(define-module (crates-io bi tw bitwrought) #:use-module (crates-io))

(define-public crate-bitwrought-0.1.0 (c (n "bitwrought") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "xattr") (r "^1.0.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1a7w292zysvbwv11b6n489dvkq0l95jdh5dlzppdd3drvrlmg3r8")))

(define-public crate-bitwrought-0.1.1 (c (n "bitwrought") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "xattr") (r "^1.0.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "10nsbaq9j76cg7f1612knaczpfvr85id634cdv0lkgvv7jvyv6k9")))

