(define-module (crates-io bi tw bitwuzla-sys) #:use-module (crates-io))

(define-public crate-bitwuzla-sys-0.1.0 (c (n "bitwuzla-sys") (v "0.1.0") (d (list (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "11hk8bm9xip0b724giw3px8h2sbbg5n49zpkqwisy71rp9w9l56l") (f (quote (("vendor-cadical" "copy_dir")))) (l "bitwuzla")))

(define-public crate-bitwuzla-sys-0.1.1 (c (n "bitwuzla-sys") (v "0.1.1") (d (list (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "1va12j4v2kpgfrcwl20cxhkz4a4dm7q13qjnsy31qmmcm74vpv3z") (f (quote (("vendor-cadical" "copy_dir")))) (l "bitwuzla")))

(define-public crate-bitwuzla-sys-0.2.0 (c (n "bitwuzla-sys") (v "0.2.0") (d (list (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "1ichgvz5lnn4gyl5fafd78qh8g4k68g6scyvs2r6kg8s7wq4c0c3") (f (quote (("vendor-cadical" "copy_dir")))) (l "bitwuzla")))

