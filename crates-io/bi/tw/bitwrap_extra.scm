(define-module (crates-io bi tw bitwrap_extra) #:use-module (crates-io))

(define-public crate-bitwrap_extra-2.0.1 (c (n "bitwrap_extra") (v "2.0.1") (d (list (d (n "bitwrap_derive_extra") (r "^2.0.0") (d #t) (k 0)))) (h "153f5xmv55m0q846lv696lkh8k1nc819mxvcjnhm9llr9y1w0qvc") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitwrap_extra-2.0.3 (c (n "bitwrap_extra") (v "2.0.3") (d (list (d (n "bitwrap_derive_extra") (r "^2.0.3") (d #t) (k 0)))) (h "1djwxaqv3gmfrhz48qbp2yl2dj5pdbcrsdpf1g87mig09yrzap4i") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitwrap_extra-2.0.4 (c (n "bitwrap_extra") (v "2.0.4") (d (list (d (n "bitwrap_derive_extra") (r "^2.0.3") (d #t) (k 0)))) (h "1hfk584pjhicw5anhwcp3y4cmz5rrjd957j0l2vip1ddrdg9fjrb") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitwrap_extra-2.0.5 (c (n "bitwrap_extra") (v "2.0.5") (d (list (d (n "bitwrap_derive_extra") (r "^2.0.5") (d #t) (k 0)))) (h "0l81v2a2rk15nndzdcs4s0k7s8ly7h0d4qg9kxq285dlwjv16x5w") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitwrap_extra-2.0.6 (c (n "bitwrap_extra") (v "2.0.6") (d (list (d (n "bitwrap_derive_extra") (r "^2.0.6") (d #t) (k 0)))) (h "12fx3y5qd3j4h012lv0xxb46hapaa1vjlrq52z6fjlk7sflyzk3d") (f (quote (("std") ("default" "std"))))))

