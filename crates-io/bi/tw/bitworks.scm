(define-module (crates-io bi tw bitworks) #:use-module (crates-io))

(define-public crate-bitworks-0.0.1 (c (n "bitworks") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18hb8ibzfzjcy0dkx8h2vxasybsyrxp0kb3z6lal6bip94lzhp22") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bitworks-0.1.0 (c (n "bitworks") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ahancx2l3q4sg8vbbz87lbmb4n5hc8xzvh4niqninndq3yy6ism") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bitworks-0.1.1 (c (n "bitworks") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x3qhnwgbzds1qjn4dmnp02pqddld77mfhx5wcmc62n5sj72j81h") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bitworks-0.1.2 (c (n "bitworks") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ksb31fz8r283l08233paf7wpfwbgb1615a44b8s0bl1ccnz6q2d") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bitworks-0.1.3 (c (n "bitworks") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00aw7lab54rxfm9ma5bps712y9wxp6ylywp0v7jcinz3nj6q34vc") (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bitworks-0.1.4 (c (n "bitworks") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gjpg9rc006vcix1wmscrnll0sqmm4vix50xwf2b2mvsy6w45hqx") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bitworks-0.1.5 (c (n "bitworks") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19igr49n50ssin1lmqpxvrwk71sag0vxvsz21ybb9m9gr4jdzp5r") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bitworks-0.2.0 (c (n "bitworks") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ky1waf5lrws9cnwf3nbgwmvsi643pz0c38rfwbaqqlg9v84cks2") (s 2) (e (quote (("serde" "dep:serde"))))))

