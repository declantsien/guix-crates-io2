(define-module (crates-io bi tw bitwarden-secrets) #:use-module (crates-io))

(define-public crate-bitwarden-secrets-0.1.0 (c (n "bitwarden-secrets") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rpassword") (r "^7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.8") (d #t) (k 0)))) (h "1nn7i6kwnlcgikdxzwbjz55xhyf8vxsshpa42vww4vvw80n5zqsh")))

(define-public crate-bitwarden-secrets-0.2.0 (c (n "bitwarden-secrets") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rpassword") (r "^7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.8") (d #t) (k 0)))) (h "1x3c6jwj67llx9h746ar4llc4pr19d1v050lb1xbmccjzcbwzbrn")))

(define-public crate-bitwarden-secrets-0.3.0 (c (n "bitwarden-secrets") (v "0.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rpassword") (r "^7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.8") (d #t) (k 0)))) (h "00m7kmivs0ixphg12914salkgxpvsvmh26bc6nvawi8ns94hc7ps")))

