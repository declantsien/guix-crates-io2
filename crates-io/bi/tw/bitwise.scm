(define-module (crates-io bi tw bitwise) #:use-module (crates-io))

(define-public crate-bitwise-0.1.0 (c (n "bitwise") (v "0.1.0") (d (list (d (n "bencher") (r "0.1.*") (d #t) (k 2)) (d (n "bitintr") (r "0.1.*") (d #t) (k 0)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "06mi69j7c274cl6zavg4f5jsfspwl62xgnhl2craxnphyaqz70hz")))

(define-public crate-bitwise-0.1.1 (c (n "bitwise") (v "0.1.1") (d (list (d (n "bencher") (r "0.1.*") (d #t) (k 2)) (d (n "bitintr") (r "0.1.*") (d #t) (k 0)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "14j7z703dh401m5s22c5jcvm100r1iqag4aspjxss4qdbl71xpl7")))

