(define-module (crates-io bi tw bitwarden-cli) #:use-module (crates-io))

(define-public crate-bitwarden-cli-0.5.0 (c (n "bitwarden-cli") (v "0.5.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)))) (h "00iqh83mh8m89pz16nq058k71na6b9j1in0csb24c3k3aaqn5ayg") (r "1.71")))

