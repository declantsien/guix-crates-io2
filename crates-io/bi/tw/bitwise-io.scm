(define-module (crates-io bi tw bitwise-io) #:use-module (crates-io))

(define-public crate-bitwise-io-0.1.0 (c (n "bitwise-io") (v "0.1.0") (h "1jlsm178g2v3cnqvkkymdz7rrarkbzpwimm94bjmmgj9ys11gd6r") (y #t)))

(define-public crate-bitwise-io-0.1.1 (c (n "bitwise-io") (v "0.1.1") (h "0ii5jp8jyx3q5ff91479398rgjzp6lhvl4wbmmsa5svblzjc2q2w") (y #t)))

(define-public crate-bitwise-io-0.1.2 (c (n "bitwise-io") (v "0.1.2") (h "1mil47wysachn53ihfgyq4l229yc7yxdilami79ipk3s46575m4f")))

