(define-module (crates-io bi tw bitwrap_derive_extra) #:use-module (crates-io))

(define-public crate-bitwrap_derive_extra-2.0.2 (c (n "bitwrap_derive_extra") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dqg4n1abp377zpcrgyfnly9fdyjbafrq8s4qcdc9p022ca317ha")))

(define-public crate-bitwrap_derive_extra-2.0.3 (c (n "bitwrap_derive_extra") (v "2.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mmzfqvjimcc3kd708yvkl5pfnwk1k6x91mhgm9lahfzibpfgw6r")))

(define-public crate-bitwrap_derive_extra-2.0.4 (c (n "bitwrap_derive_extra") (v "2.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19qsdg6hknmba7msg8hdvrfjzjkkv4a8z3y9srzjqnym1x4ajmjc")))

(define-public crate-bitwrap_derive_extra-2.0.5 (c (n "bitwrap_derive_extra") (v "2.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14zfhghvgkcg9qnwl07a6jqj69wfmm5s1zcglwslpc2vazc068zc")))

(define-public crate-bitwrap_derive_extra-2.0.6 (c (n "bitwrap_derive_extra") (v "2.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07h6miv7rl746cpldqfhlw3np0qvskbbrxh5iyrs6bi5wdswxbbj")))

