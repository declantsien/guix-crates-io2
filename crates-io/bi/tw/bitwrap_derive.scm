(define-module (crates-io bi tw bitwrap_derive) #:use-module (crates-io))

(define-public crate-bitwrap_derive-0.1.0 (c (n "bitwrap_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i3jd0vvnsqv7naldnpa8rca790k1693h6lx4r6fqla3jbc44ilp")))

(define-public crate-bitwrap_derive-0.1.1 (c (n "bitwrap_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1771vvn8hl8s5xkiabzh4n10qb573mdpmx5d360vvblz4w73gm26")))

(define-public crate-bitwrap_derive-0.2.1 (c (n "bitwrap_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1937qjh9vzscmdnv7jpvvmcl8n36qg3i2p770y0pl1mc21jd4qhi")))

(define-public crate-bitwrap_derive-0.2.2 (c (n "bitwrap_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kydcnk460819wykifiashz6r5aji6zv0887n6binqj1mb1kzkb8")))

(define-public crate-bitwrap_derive-0.2.3 (c (n "bitwrap_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xmhxbl3cpv59sllkqga8vs5jhjnmba7cww9px8aa2601ajd31if")))

(define-public crate-bitwrap_derive-0.3.0 (c (n "bitwrap_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15dvnddx1n0gpgz73072kwh64y0wsivr2fqjxq0igx28byglj27q")))

(define-public crate-bitwrap_derive-0.4.0 (c (n "bitwrap_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g5xnpwkm5lvg1hhxccmipwl231n18zqw4ch15i06icr49qiv3pm")))

(define-public crate-bitwrap_derive-0.4.1 (c (n "bitwrap_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f9xbjxyhkmsrvbjc2jd72iml39c7ybnvs7pp82pgflw5vnialb7")))

(define-public crate-bitwrap_derive-0.5.1 (c (n "bitwrap_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x6q61h577gjzzs686i51p692gjabi8x1829v4irk62g2p6xjayx")))

(define-public crate-bitwrap_derive-0.5.3 (c (n "bitwrap_derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z328dzd5dqnzgx2acnsxd9yxiipbl5sz5cs1bhxj8zpp4hhgy8c")))

(define-public crate-bitwrap_derive-0.5.4 (c (n "bitwrap_derive") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z5mg06zjagx66w885bayfxz9n8znvrwqq605ckwh8x0c1jr11ks")))

(define-public crate-bitwrap_derive-0.5.5 (c (n "bitwrap_derive") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x6c6v4r476w01w6h27d6fj61c42wybr5j4x13qkl3896p47k8s3")))

(define-public crate-bitwrap_derive-0.5.6 (c (n "bitwrap_derive") (v "0.5.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ygl3xbmqfw8ly8sp8a2h56d7ji3l1ancc4ac0s4kr5k32pinpdn")))

(define-public crate-bitwrap_derive-0.5.7 (c (n "bitwrap_derive") (v "0.5.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bgq1sw883nmgnfhkjxlkiw23xhv70b7ffc3gicz7w9xdc53ajfv")))

(define-public crate-bitwrap_derive-0.5.8 (c (n "bitwrap_derive") (v "0.5.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nwq89qi2j4vh6x1xpfrz7iz4gqm9q4xpckw9fwsydx486f7dsnz")))

(define-public crate-bitwrap_derive-0.5.9 (c (n "bitwrap_derive") (v "0.5.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1npdpdh6l924zk4053x01d8bhrfzj2bng3xm73aspiqxvpa13rzn")))

(define-public crate-bitwrap_derive-0.5.10 (c (n "bitwrap_derive") (v "0.5.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r2ag07dzdzk3iyksgpq9d1a54byd8z05x5v8rxi6p1qfp91vzrl")))

(define-public crate-bitwrap_derive-0.5.11 (c (n "bitwrap_derive") (v "0.5.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0m7v1wmm6vgc0wm5bxf7fw2ysb4ka3gznqbgmjqnxjcbahrm95yw")))

(define-public crate-bitwrap_derive-1.0.0 (c (n "bitwrap_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jcdg6ynpfk663f57mwa3dwbczpq7fi2nn2jdg05bpzqn41p0rfr")))

(define-public crate-bitwrap_derive-1.0.1 (c (n "bitwrap_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wjmq86pzjmn6gq4fsii2qg9pb33f5cy8nzrcfy0qxy80jn1nb04")))

(define-public crate-bitwrap_derive-1.1.0 (c (n "bitwrap_derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16czawsvw5lp0q9j0jkpci8xqq4bb87z2bqgmw800y6l87yj1cr6")))

(define-public crate-bitwrap_derive-2.0.0 (c (n "bitwrap_derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m6jky5lnka16i7rh1awm4ksdpzj2q16i7s39dr5bf8pncgf0ax8")))

