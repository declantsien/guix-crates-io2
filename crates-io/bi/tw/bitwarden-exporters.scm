(define-module (crates-io bi tw bitwarden-exporters) #:use-module (crates-io))

(define-public crate-bitwarden-exporters-0.5.0 (c (n "bitwarden-exporters") (v "0.5.0") (d (list (d (n "base64") (r ">=0.21.2, <0.22") (d #t) (k 0)) (d (n "bitwarden-crypto") (r "=0.5.0") (d #t) (k 0)) (d (n "chrono") (r ">=0.4.26, <0.5") (f (quote ("clock" "serde" "std"))) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.96, <2.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.40, <2.0") (d #t) (k 0)) (d (n "uuid") (r ">=1.3.3, <2.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1sqdafa6gn2dnzdqcynwfi16iwz8svjypxpga40sdjxpg2jsjck0") (r "1.71")))

