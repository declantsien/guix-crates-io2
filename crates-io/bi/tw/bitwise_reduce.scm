(define-module (crates-io bi tw bitwise_reduce) #:use-module (crates-io))

(define-public crate-bitwise_reduce-0.1.0 (c (n "bitwise_reduce") (v "0.1.0") (h "1x362s858jdavhlf9f4m1wmf78s5vxd2nmbz3vj8qlap5xkpig5c")))

(define-public crate-bitwise_reduce-0.1.1 (c (n "bitwise_reduce") (v "0.1.1") (h "0cp0zp29igzk4bs0ylqy5vrp6d7r8q10f147iylg2z5353ia7i3r")))

(define-public crate-bitwise_reduce-0.1.2 (c (n "bitwise_reduce") (v "0.1.2") (h "120ix1fvy1gjgsfrdic8vbhzpsi74g9pjg3bna486cl3bz2jzf0z")))

(define-public crate-bitwise_reduce-0.2.0 (c (n "bitwise_reduce") (v "0.2.0") (h "13iyir5gvcmxjksm3iclxszgv0jj0q4l990c1i41b3bb528an7d8")))

