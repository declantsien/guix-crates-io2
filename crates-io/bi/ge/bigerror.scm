(define-module (crates-io bi ge bigerror) #:use-module (crates-io))

(define-public crate-bigerror-0.1.0 (c (n "bigerror") (v "0.1.0") (d (list (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "08ds3009kp3j75xz9v3gzf503p696pcl65l1103xrd9vqald16ni")))

(define-public crate-bigerror-0.2.0 (c (n "bigerror") (v "0.2.0") (d (list (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1k8h168mjxnzbxs552a9pgfbr05a3gsm4x2jkq1kavr7bnzm87l5")))

(define-public crate-bigerror-0.3.0 (c (n "bigerror") (v "0.3.0") (d (list (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0h3n29qh77rdb7hj40694lk2m0x7p177q5zarr97jgnar7vid9c1")))

(define-public crate-bigerror-0.4.0 (c (n "bigerror") (v "0.4.0") (d (list (d (n "error-stack") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1z53w7dmzly7n7b7rfhxzw7wwi6knslwc5wi6vs7y055cy0nx5l6")))

(define-public crate-bigerror-0.5.0 (c (n "bigerror") (v "0.5.0") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0f4q90dd1wpmr2ld978qd67zvwqi4frfx1sf5f9d2ll2dngzb2wd") (f (quote (("std" "error-stack/std" "error-stack/anyhow") ("spantrace" "error-stack/spantrace") ("serde" "error-stack/serde") ("hooks" "error-stack/hooks") ("eyre" "error-stack/eyre") ("default" "std"))))))

(define-public crate-bigerror-0.6.0 (c (n "bigerror") (v "0.6.0") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "091b36h88156nl8rzygcm8im42v8q1xszmznh3jalmici9zz4f9d") (f (quote (("std" "error-stack/std" "error-stack/anyhow") ("spantrace" "error-stack/spantrace") ("serde" "error-stack/serde") ("hooks" "error-stack/hooks") ("eyre" "error-stack/eyre") ("default" "std"))))))

(define-public crate-bigerror-0.7.0 (c (n "bigerror") (v "0.7.0") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1jfmmbhzba8rsk49n8w63g0kp4sk8hdp0wfn4jm0pc8m3sk43n6n") (f (quote (("std" "error-stack/std" "error-stack/anyhow") ("spantrace" "error-stack/spantrace") ("serde" "error-stack/serde") ("hooks" "error-stack/hooks") ("eyre" "error-stack/eyre") ("default" "std"))))))

(define-public crate-bigerror-0.8.0 (c (n "bigerror") (v "0.8.0") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tonic-types") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ildfy93nvagy1yjkf4fak1zypnbpb865w85dhs55ngzsnaqzky5") (f (quote (("std" "error-stack/std" "error-stack/anyhow") ("spantrace" "error-stack/spantrace") ("serde" "error-stack/serde") ("hooks" "error-stack/hooks") ("grpc" "tonic" "tonic-types") ("eyre" "error-stack/eyre") ("default" "std"))))))

(define-public crate-bigerror-0.8.1 (c (n "bigerror") (v "0.8.1") (d (list (d (n "error-stack") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "tonic-types") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "11vky6smzv0z3lnp6pji6ha3fxmmarpp0v8qzscj0xfz29dd9fs1") (f (quote (("std" "error-stack/std" "error-stack/anyhow") ("spantrace" "error-stack/spantrace") ("serde" "error-stack/serde") ("hooks" "error-stack/hooks") ("grpc" "tonic" "tonic-types") ("eyre" "error-stack/eyre") ("default" "std"))))))

