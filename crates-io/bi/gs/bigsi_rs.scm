(define-module (crates-io bi gs bigsi_rs) #:use-module (crates-io))

(define-public crate-bigsi_rs-0.1.0 (c (n "bigsi_rs") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "bv") (r "^0.11.0") (d #t) (k 0)) (d (n "fasthash") (r "^0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "170rj69qm3wn1fg72ivjyvjrah0rn302dg376pbknd7rwkqlfvm3") (f (quote (("default" "bv/serde"))))))

(define-public crate-bigsi_rs-0.1.1 (c (n "bigsi_rs") (v "0.1.1") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "bv") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "0r0lv00k3v7lr6q8mz6bzx9gmmq6nwlmr43ldrw857nsq0rq9ahw") (f (quote (("default" "bv/serde"))))))

