(define-module (crates-io bi gs bigshot) #:use-module (crates-io))

(define-public crate-bigshot-0.1.0 (c (n "bigshot") (v "0.1.0") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "sdl3") (r "^0.5.0") (f (quote ("bundled" "static-link"))) (d #t) (k 0)))) (h "0gwkxxpfwibc7q44kkjwclpii1ijwa7a20as5mal2s0bqnbg0b6s")))

