(define-module (crates-io bi p_ bip_select) #:use-module (crates-io))

(define-public crate-bip_select-0.0.0 (c (n "bip_select") (v "0.0.0") (h "0gsh1y7dbki9kv5i13ld7cmd4nxzhymqshsry6gapssmm3rbnmar") (f (quote (("unstable"))))))

(define-public crate-bip_select-0.1.0 (c (n "bip_select") (v "0.1.0") (d (list (d (n "bip_handshake") (r "^0.7") (d #t) (k 0)) (d (n "bip_metainfo") (r "^0.11") (d #t) (k 0)) (d (n "bip_peer") (r "^0.5") (d #t) (k 0)) (d (n "bip_utracker") (r "^0.3") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mndhi0ya8znr0cjhvm7wcnax7ibfmj8b0yq5mzxx4x0ms4qaqnj")))

