(define-module (crates-io bi p_ bip_bencode) #:use-module (crates-io))

(define-public crate-bip_bencode-0.1.0 (c (n "bip_bencode") (v "0.1.0") (h "11rn5lb8fjvjid4inijkssnxakzciyjmsb47bmsxkx3hl55bgk59")))

(define-public crate-bip_bencode-0.1.1 (c (n "bip_bencode") (v "0.1.1") (h "165gxf79mh3r9hvwca5n74964r7vqiizrnkbbzy9pd89h0lmciq0") (f (quote (("unstable"))))))

(define-public crate-bip_bencode-0.1.2 (c (n "bip_bencode") (v "0.1.2") (h "0da2vw8mznwxsng7j63fabq85qwxb9ingzia98jc3irnh52idqkn") (f (quote (("unstable"))))))

(define-public crate-bip_bencode-0.1.3 (c (n "bip_bencode") (v "0.1.3") (h "0271y20113blqqpy86fqv0y67h30iy4r7m0gljh5y9mz7k3g58c1") (f (quote (("unstable"))))))

(define-public crate-bip_bencode-0.2.0 (c (n "bip_bencode") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.7.0") (d #t) (k 0)))) (h "1495a4hawg2pnfii7s4mbv7fgwraqf8ggj8aabyxjgv2y6qj3ask") (f (quote (("unstable"))))))

(define-public crate-bip_bencode-0.3.0 (c (n "bip_bencode") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)))) (h "1rhq3k9inz6mm5qvqri81xj41izlpmr73lylxbvbpxyxr7wrkjzz") (f (quote (("unstable"))))))

(define-public crate-bip_bencode-0.3.1 (c (n "bip_bencode") (v "0.3.1") (d (list (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)))) (h "1m4m7c9zv4vslfldsfj5x1sj3xdz6w8gxp10jbs83n1a8wl7h4r4") (f (quote (("unstable"))))))

(define-public crate-bip_bencode-0.4.0 (c (n "bip_bencode") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.9.0") (d #t) (k 0)))) (h "0r0q1hr38a2kw4rgvz1zncdk2wz7dqwqknkvhzyndkgcvhvnwgkd") (f (quote (("unstable"))))))

(define-public crate-bip_bencode-0.4.1 (c (n "bip_bencode") (v "0.4.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1rwahqgvn69n9w26czx8z70ljhbqgnp6x9pi1ixf0rxcv5rx4y5p") (f (quote (("unstable"))))))

(define-public crate-bip_bencode-0.4.2 (c (n "bip_bencode") (v "0.4.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "1hbqmn7r2mlwq688gqz5z7pqmfyils25jxripja0j89j7gpbzylg") (f (quote (("unstable"))))))

(define-public crate-bip_bencode-0.4.3 (c (n "bip_bencode") (v "0.4.3") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "01c6b3r57wa0ca38iyvgcfnf8wl77906isxsrxdnyprrvm3hs1pa") (f (quote (("unstable"))))))

(define-public crate-bip_bencode-0.4.4 (c (n "bip_bencode") (v "0.4.4") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)))) (h "05m41snmndkhdf4vk1vnr69f5nppbn2d5452k184lm40jrfwqj30") (f (quote (("unstable"))))))

