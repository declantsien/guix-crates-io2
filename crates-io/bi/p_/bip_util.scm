(define-module (crates-io bi p_ bip_util) #:use-module (crates-io))

(define-public crate-bip_util-0.1.0 (c (n "bip_util") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "sha1") (r "^0.1.0") (d #t) (k 0)))) (h "09nm7sp7lizfmgw2ybd0lbs8qvwfi4wysla8whjhrrh6w7g93x10")))

(define-public crate-bip_util-0.2.0 (c (n "bip_util") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "sha1") (r "^0.1.0") (d #t) (k 0)))) (h "0skc2jhp4sfxppf6ihvrmn178pkkk7jq35g25rzs0ilqlsn6g9p7") (f (quote (("unstable"))))))

(define-public crate-bip_util-0.2.1 (c (n "bip_util") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)))) (h "0xcij6kkg0srafjx49iz50avxxs2275hgpnfcc9bdkd3ljhn9df6") (f (quote (("unstable"))))))

(define-public crate-bip_util-0.2.2 (c (n "bip_util") (v "0.2.2") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)))) (h "1sw5wgjs1rik4kydbjy6dp1k3q3mgajnpwyfs033w8sgq1fjl0vz") (f (quote (("unstable"))))))

(define-public crate-bip_util-0.2.3 (c (n "bip_util") (v "0.2.3") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)))) (h "16xlldwkfwchz4g3vw94c4fg51mvi59f4bzblws430l1b5nd3wna") (f (quote (("unstable"))))))

(define-public crate-bip_util-0.2.4 (c (n "bip_util") (v "0.2.4") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)))) (h "05hr11f51m5blywf38ddc9kwapci1l8jrndr4vqy3syasvsaxj7k") (f (quote (("unstable"))))))

(define-public crate-bip_util-0.3.4 (c (n "bip_util") (v "0.3.4") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)))) (h "0jkz6pzjdb02zya4sgxqrlw83qddi93z80mf0dslg0s23q0m7yp2") (f (quote (("unstable")))) (y #t)))

(define-public crate-bip_util-0.3.0 (c (n "bip_util") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)))) (h "07nk676nhlp6palpd8y1zd7b3wi0ylzrgx5q0i17iq22iyh51is2") (f (quote (("unstable"))))))

(define-public crate-bip_util-0.3.5 (c (n "bip_util") (v "0.3.5") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)))) (h "0b9jdhx3kaz246ckrllc5fh8k52ls3dkab9rc3g9a2gw75842hcz") (f (quote (("unstable"))))))

(define-public crate-bip_util-0.4.0 (c (n "bip_util") (v "0.4.0") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)))) (h "01fksknpln09xhqspwsdj96km98jlcrl0df5f3sys1dik5jw4p1y") (f (quote (("unstable"))))))

(define-public crate-bip_util-0.5.0 (c (n "bip_util") (v "0.5.0") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)))) (h "1xv19bpv9jv8y32cx7s5sw2m3a8z9l1j4yjkvi99hgq6vyx4i1zl") (f (quote (("unstable"))))))

