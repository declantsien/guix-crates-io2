(define-module (crates-io bi nd bindgen_plugin) #:use-module (crates-io))

(define-public crate-bindgen_plugin-0.14.0 (c (n "bindgen_plugin") (v "0.14.0") (d (list (d (n "bindgen") (r "*") (d #t) (k 0)))) (h "0hcb2173rqlxxwddfkcbz2sv9z8h4gy4qsh9n519r889hymdscd0") (f (quote (("static"))))))

(define-public crate-bindgen_plugin-0.15.0 (c (n "bindgen_plugin") (v "0.15.0") (d (list (d (n "bindgen") (r "*") (d #t) (k 0)))) (h "0cbksbyf55k15san649smsm5fmijffgxx6pi3mb9c5lwgqwra64x") (f (quote (("static"))))))

(define-public crate-bindgen_plugin-0.16.0 (c (n "bindgen_plugin") (v "0.16.0") (d (list (d (n "bindgen") (r "^0.16.0") (d #t) (k 0)))) (h "16i4gjiis7k3ry5anjhhadpyli4k1lfwrbrwzm6c3dcfl7djvzv6") (f (quote (("static"))))))

