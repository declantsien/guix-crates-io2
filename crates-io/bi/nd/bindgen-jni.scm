(define-module (crates-io bi nd bindgen-jni) #:use-module (crates-io))

(define-public crate-bindgen-jni-0.0.0 (c (n "bindgen-jni") (v "0.0.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "bugsalot") (r "^0.1.4") (d #t) (k 2)) (d (n "jni-sys") (r "^0.3.0") (d #t) (k 2)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "117zmn3fmkq68sdhrj5a82jjp6h0v3f5li1gsiy8khdy6qcv6shn")))

(define-public crate-bindgen-jni-0.0.1 (c (n "bindgen-jni") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "bugsalot") (r "^0.1.4") (d #t) (k 2)) (d (n "jni-sys") (r "^0.3.0") (d #t) (k 2)) (d (n "zip") (r "^0.5.2") (d #t) (k 0)))) (h "1sqmm1iwc80sjmspvz5nkhix4nv2wrc0q8jdqasim6q2nbm4rvw4")))

