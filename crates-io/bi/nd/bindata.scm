(define-module (crates-io bi nd bindata) #:use-module (crates-io))

(define-public crate-bindata-0.1.0 (c (n "bindata") (v "0.1.0") (d (list (d (n "bindata-impl") (r "^0.1.0") (d #t) (k 0)))) (h "03dis3pkfq0pr9vacbcpa83v9rwrr7483alfqx5dxp0g0m0gxy34")))

(define-public crate-bindata-0.1.1 (c (n "bindata") (v "0.1.1") (d (list (d (n "bindata-impl") (r "^0.1.0") (d #t) (k 0)))) (h "18kl1zwrl01q80ywd5wm3p0n8d56m093f2f366mpxrkfpzcg8d56")))

