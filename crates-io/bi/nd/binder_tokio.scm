(define-module (crates-io bi nd binder_tokio) #:use-module (crates-io))

(define-public crate-binder_tokio-0.1.3 (c (n "binder_tokio") (v "0.1.3") (d (list (d (n "binder") (r "^0") (d #t) (k 0) (p "binder_ndk")) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-runtime") (r "^0.0.0") (d #t) (k 0)))) (h "1r0bdldc1hdlapymfl4pjdlmfa4bln6qnj65xm1r4b2i61kgxwi4") (r "1.67")))

(define-public crate-binder_tokio-0.1.4 (c (n "binder_tokio") (v "0.1.4") (d (list (d (n "binder") (r "^0") (d #t) (k 0) (p "binder_ndk")) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-runtime") (r "^0.0.0") (d #t) (k 0)))) (h "0aqpkbnzbm3yw7fn0cvjnv9b6x2h97bwx88fnjiz1ngik2xb621p") (r "1.67")))

(define-public crate-binder_tokio-0.2.0 (c (n "binder_tokio") (v "0.2.0") (d (list (d (n "binder") (r "^0") (d #t) (k 0) (p "binder_ndk")) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-runtime") (r "^0.0.0") (d #t) (k 0)))) (h "0bf34qp1pvyzkwfbj39szr81hd32cybwk548z099yb8ydjwwq3bv") (r "1.67")))

