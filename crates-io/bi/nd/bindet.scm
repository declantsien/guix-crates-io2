(define-module (crates-io bi nd bindet) #:use-module (crates-io))

(define-public crate-bindet-0.1.3 (c (n "bindet") (v "0.1.3") (h "1d7mg7l7vg9yc6b9bc3asy6m4fxwxncxn3ri2yacq2iz9lzwfmrz")))

(define-public crate-bindet-0.1.4 (c (n "bindet") (v "0.1.4") (h "1l5xgv760dfp0hx4kw1hajak13wl4bpfxawfkpr6s370mxfnw5ny")))

(define-public crate-bindet-0.1.5 (c (n "bindet") (v "0.1.5") (h "14qnwxvy53rsdc9jxx3yslav547v370b2xjmf3gi9fhdg0brkbcm")))

(define-public crate-bindet-0.1.6 (c (n "bindet") (v "0.1.6") (h "0w1cx7ngd0dl7x4j7z6jycksvwljhvl3930ra3i8rlbjll9wfdqg")))

(define-public crate-bindet-0.1.7 (c (n "bindet") (v "0.1.7") (h "1g42kfpd8yg03zaza42fd14bxap68pv24a2b2ppm8kjicab05crn")))

(define-public crate-bindet-0.1.8 (c (n "bindet") (v "0.1.8") (h "1wv9fc8mjqfrcjs1v1wd39jzwh1750334y73d9zm36p3zggrf70r")))

(define-public crate-bindet-0.1.9 (c (n "bindet") (v "0.1.9") (h "03amlw91sa805a9gzbnk9plss1jh551p4q097adaw1ip6y8yvla0")))

(define-public crate-bindet-0.2.0 (c (n "bindet") (v "0.2.0") (d (list (d (n "mediatype") (r "^0.19.1") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (o #t) (d #t) (k 0)))) (h "19vnvqilc5mf1xxyc4f9hqmg1ag02h82wcz5cxi7f5xl1nnpvmsi") (f (quote (("default"))))))

(define-public crate-bindet-0.3.0 (c (n "bindet") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "mediatype") (r "^0.19.1") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "11h5wv1k3svxa74mjsm232iffixrdwsbamvsqbppxg533sgj74nd") (f (quote (("nightly") ("default"))))))

(define-public crate-bindet-0.3.1 (c (n "bindet") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "mediatype") (r "^0.19.1") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0g4vpimc5yabankk4m3fm8c2js85pp7pc9khxc6rh18cxgnjls48") (f (quote (("nightly") ("default"))))))

(define-public crate-bindet-0.3.2 (c (n "bindet") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "mediatype") (r "^0.19.1") (o #t) (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18q0g6z3ks1n7bwd9c9ysi3zlj9p8iflzgzj8hwpylppyngfkzjs") (f (quote (("nightly") ("default"))))))

