(define-module (crates-io bi nd bindocs) #:use-module (crates-io))

(define-public crate-bindocs-0.1.0 (c (n "bindocs") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "libcorn") (r "^0.8.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "04ff8mq5sr20zkbhfsyv14saggh4ppib8z99y239cxqs5n3xxgcy")))

