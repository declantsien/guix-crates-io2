(define-module (crates-io bi nd bindiff) #:use-module (crates-io))

(define-public crate-bindiff-0.0.1-pre (c (n "bindiff") (v "0.0.1-pre") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 0)) (d (n "goblin") (r "^0.0.12") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1nf9vfynqa9707j5c4741ba2gcmfjhmjl3ii5w2gs4l7jkcpacqn")))

