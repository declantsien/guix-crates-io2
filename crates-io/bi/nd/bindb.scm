(define-module (crates-io bi nd bindb) #:use-module (crates-io))

(define-public crate-bindb-0.0.1 (c (n "bindb") (v "0.0.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.4") (k 0)))) (h "1ygfnh3zzfv0rqs65lrnm7s1fmabzz3g6w4q5m31dghghglrlpdm")))

(define-public crate-bindb-0.0.3 (c (n "bindb") (v "0.0.3") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.4") (k 0)))) (h "0zh7yrkf18i2ny35gbln8x4wf420nsb51hs3j9zk7c6rpk07ykn8")))

