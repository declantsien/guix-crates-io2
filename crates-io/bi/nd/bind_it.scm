(define-module (crates-io bi nd bind_it) #:use-module (crates-io))

(define-public crate-bind_it-0.1.0 (c (n "bind_it") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nspvbk3iidxnw9ffwrylwqq1c2p928c0i4hs2qr3f6lz959qxf9")))

(define-public crate-bind_it-0.1.1 (c (n "bind_it") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0nrpp6l01v3s4rgx5fm2nfb2z5qjcc296idwaw5bl1fapjwp22ja")))

(define-public crate-bind_it-0.1.3 (c (n "bind_it") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0kfby41xc99p0y3g3xy91frm8czpis4jcm6377b8ixc089x62ki2")))

