(define-module (crates-io bi nd binding_powers_impl) #:use-module (crates-io))

(define-public crate-binding_powers_impl-0.0.0 (c (n "binding_powers_impl") (v "0.0.0") (h "1zzmrk58yr1kj1idwqrwp6dyrcby03ky2i3x02dqcwfhpqny09zk") (y #t)))

(define-public crate-binding_powers_impl-0.0.1 (c (n "binding_powers_impl") (v "0.0.1") (h "13wbhrcr8g8a1m7bdlmflzwgwy9q3hgyfjxyyq3lj2xi652jpprw") (y #t)))

(define-public crate-binding_powers_impl-0.0.2 (c (n "binding_powers_impl") (v "0.0.2") (h "0zi3yhiixzxd9rg5i9b1nq7l460dwfxr4fzp4ch1byvg8y0lyass") (y #t)))

