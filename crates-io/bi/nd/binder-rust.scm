(define-module (crates-io bi nd binder-rust) #:use-module (crates-io))

(define-public crate-binder-rust-0.1.0 (c (n "binder-rust") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "02bqcnilqqblkc22lmwamrci8wxcky2130lqw2klyqb523j81r79")))

(define-public crate-binder-rust-0.1.1 (c (n "binder-rust") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0qal9j2jsx2pq72rnpks31yc7hmac7v91ian370gcdfvm0cby7ga")))

(define-public crate-binder-rust-0.1.2 (c (n "binder-rust") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0h2g3c1ay1q06xzsk5sasik5wxnh6ah7743m68ykdb0iyxsdr8b3")))

