(define-module (crates-io bi nd binder_ndk_sys) #:use-module (crates-io))

(define-public crate-binder_ndk_sys-0.1.0 (c (n "binder_ndk_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0k23rxvgsinx8b2cqcqv8vkb0k972b2h5b53f17l0bvl2cvf62nq") (r "1.67")))

(define-public crate-binder_ndk_sys-0.1.1 (c (n "binder_ndk_sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "19r7wvpb27hgqij5mgbahja9sx4g6ksjr73bkxr027qj0h1jrx98") (r "1.67")))

(define-public crate-binder_ndk_sys-0.1.2 (c (n "binder_ndk_sys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "1q9qbxlbc4jb0862k1xz38p47dqbdjp0r8vgq5lzcabn30ch60n6") (r "1.67")))

(define-public crate-binder_ndk_sys-0.1.3 (c (n "binder_ndk_sys") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0f3qfv9azvnn0q83k7vc4gsf6swg92k9i35mc6yldhhnn925wzm9") (r "1.67")))

(define-public crate-binder_ndk_sys-0.1.4 (c (n "binder_ndk_sys") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "08rd9l3jvn50kqkfmj7ky70vgmmlhp18z4fcsyym0dfgjncgbgm1") (r "1.67")))

(define-public crate-binder_ndk_sys-0.2.0 (c (n "binder_ndk_sys") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0milhk162hcrhi9p4whrdhzwjqhcysab5lvjf97xcm14h7683y2v") (r "1.67")))

