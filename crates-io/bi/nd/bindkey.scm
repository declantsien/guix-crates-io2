(define-module (crates-io bi nd bindkey) #:use-module (crates-io))

(define-public crate-bindkey-0.2.0 (c (n "bindkey") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.14") (f (quote ("xlib"))) (d #t) (k 0)))) (h "1rwbjwbnj5m5gpx3lksnrpklphfmczc1hklg27kyn0frpqjs5x1s")))

