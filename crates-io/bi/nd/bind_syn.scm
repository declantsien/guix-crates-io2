(define-module (crates-io bi nd bind_syn) #:use-module (crates-io))

(define-public crate-bind_syn-0.1.0 (c (n "bind_syn") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1fc5rdhznlm2xzy1lxd3vxf2nnan76b7db2gfx3whjrb0yx0hrkx")))

