(define-module (crates-io bi nd binding_powers) #:use-module (crates-io))

(define-public crate-binding_powers-0.1.0 (c (n "binding_powers") (v "0.1.0") (d (list (d (n "binding_powers_impl") (r "^0.0.1") (d #t) (k 0)))) (h "0pdl6rbsxcpvv5c5m2vgdgv239w5c5g36jclymb7v3k6yaymr72p") (y #t)))

(define-public crate-binding_powers-0.1.1 (c (n "binding_powers") (v "0.1.1") (d (list (d (n "binding_powers_impl") (r "=0.0.2") (d #t) (k 0)))) (h "16hlb7zjpv292mjqm4f9v33slzd8vzhd1lfiqv1ipmy42bjwkz91") (y #t)))

