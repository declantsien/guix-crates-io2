(define-module (crates-io bi nd bind-args) #:use-module (crates-io))

(define-public crate-bind-args-0.1.0 (c (n "bind-args") (v "0.1.0") (h "1kzpnnwmv5478wrb8h3bl485q89b1yns8sjggcankpk3icp0vz09")))

(define-public crate-bind-args-0.2.0 (c (n "bind-args") (v "0.2.0") (h "1qmz9pm4vp75ny22kkxijw8nj1m9c4mbvnl9d4jalhs9h21ykvh9")))

