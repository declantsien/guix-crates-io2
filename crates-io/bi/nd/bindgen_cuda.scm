(define-module (crates-io bi nd bindgen_cuda) #:use-module (crates-io))

(define-public crate-bindgen_cuda-0.1.0 (c (n "bindgen_cuda") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0dj5gzfiwps8j2pfq08hmg49ffgy5zgzxj5mp0a6acpiwyifqjqj")))

(define-public crate-bindgen_cuda-0.1.1 (c (n "bindgen_cuda") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1ld9a5id8hijwi3xl9vbk5b20yzgbjv649bzjl7bgmh20mgqr2xm")))

(define-public crate-bindgen_cuda-0.1.2 (c (n "bindgen_cuda") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "10hk9y7hvjgnqwhn55f8i1790hh6mkywm6wrnd8vc66ws7iynqf1")))

(define-public crate-bindgen_cuda-0.1.3 (c (n "bindgen_cuda") (v "0.1.3") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1b4vra23hgipjx55115c5f0w49b23p35g5lfp7bxhfn5k7s4kczw")))

(define-public crate-bindgen_cuda-0.1.4 (c (n "bindgen_cuda") (v "0.1.4") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "07rwndic0smbxq68p3r0a1cx26yj920b1lvxriljbs148ynjagw5")))

(define-public crate-bindgen_cuda-0.1.5 (c (n "bindgen_cuda") (v "0.1.5") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0z92l6amvhr2gx20b79jhxyfhk8ydr6hyzp3zwdsh5vxbfpqk10z")))

