(define-module (crates-io bi nd bindgen-cfg) #:use-module (crates-io))

(define-public crate-bindgen-cfg-0.1.0 (c (n "bindgen-cfg") (v "0.1.0") (d (list (d (n "bindgen") (r ">=0.59") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0kkq359s5dwkvdxlniml7xrihdbzfahp2wav53ng93d8a6p9x282")))

(define-public crate-bindgen-cfg-0.1.1 (c (n "bindgen-cfg") (v "0.1.1") (d (list (d (n "bindgen") (r ">=0.59") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0dzlqq9mnwgjnfpkkk1wd9gcir7jmpwgp2hdjivxcxxcavpicwwi")))

