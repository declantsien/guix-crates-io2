(define-module (crates-io bi nd bindata-derive) #:use-module (crates-io))

(define-public crate-bindata-derive-0.1.0 (c (n "bindata-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "14mfcsppvqa4j4vanzim0r6fafzklvb8wr04142q4nd0dmai332s")))

