(define-module (crates-io bi nd bind) #:use-module (crates-io))

(define-public crate-bind-0.1.0 (c (n "bind") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0vd94j3nvrbz4s6mbacjzyx0ksfna8qj8inhlvaznxib1ha4w95a")))

(define-public crate-bind-0.1.1 (c (n "bind") (v "0.1.1") (d (list (d (n "bind_syn") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i576jvaqnpd05iwk1csqlwm4rgdi3nk1llzc6bpyrwzglzi4s3j")))

