(define-module (crates-io bi nd binder_ndk) #:use-module (crates-io))

(define-public crate-binder_ndk-0.1.1 (c (n "binder_ndk") (v "0.1.1") (d (list (d (n "binder_ndk_sys") (r "^0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1ifrfr0xchqlyjjk4bx9nw257kxva40ndbwkamljshmyaqh7qhzz") (r "1.67")))

(define-public crate-binder_ndk-0.1.2 (c (n "binder_ndk") (v "0.1.2") (d (list (d (n "binder_ndk_sys") (r "^0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0ajv2cxn5flxkcp1q2wvj2iq17qbkqp6x6cnni7jhdwavczsi04m") (r "1.67")))

(define-public crate-binder_ndk-0.1.3 (c (n "binder_ndk") (v "0.1.3") (d (list (d (n "binder_ndk_sys") (r "^0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1d9vz54g5n5wykrgqch7zw6g55l1mypdakpasxlp1frl41qz46fw") (r "1.67")))

(define-public crate-binder_ndk-0.1.4 (c (n "binder_ndk") (v "0.1.4") (d (list (d (n "binder_ndk_sys") (r "^0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "168cp7szs0qk7g0adshz6x9xk4lavxp0dwqy1fdpynpdps79vspb") (r "1.67")))

(define-public crate-binder_ndk-0.2.0 (c (n "binder_ndk") (v "0.2.0") (d (list (d (n "binder_ndk_sys") (r "^0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0rlxry3mkdaylz3lbxkpb1b7yp6dzs6fwzhxq9bcd4yj3xmb69jg") (r "1.67")))

