(define-module (crates-io bi nd bindata-impl) #:use-module (crates-io))

(define-public crate-bindata-impl-0.1.0 (c (n "bindata-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1iz3r3cyq4xllgg3c1nkysvi164mnh46jk4mm1sadlp5i8smnsih")))

