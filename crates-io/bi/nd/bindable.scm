(define-module (crates-io bi nd bindable) #:use-module (crates-io))

(define-public crate-bindable-0.1.0 (c (n "bindable") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15nj234sj3fql7ddr4qdj2dxj5xnfwhza1bpk55q8w3bpcb203zy") (f (quote (("with-serde" "serde") ("with-actix" "actix-web") ("default" "with-serde"))))))

(define-public crate-bindable-0.1.1 (c (n "bindable") (v "0.1.1") (d (list (d (n "actix-http") (r "^3") (o #t) (d #t) (k 0)) (d (n "actix-service") (r "^2") (o #t) (d #t) (k 0)) (d (n "actix-web") (r "^4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y4w33l797i4a6z7rp4b2cfkdqch5fv5jipnawmm50iykvk02pca") (f (quote (("with-serde" "serde") ("with-actix" "actix-http" "actix-service" "actix-web") ("default" "with-serde"))))))

(define-public crate-bindable-0.1.2 (c (n "bindable") (v "0.1.2") (d (list (d (n "actix-http") (r "^3") (o #t) (d #t) (k 0)) (d (n "actix-service") (r "^2") (o #t) (d #t) (k 0)) (d (n "actix-web") (r "^4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "066axv2kh5ca9wb19480qhffzfhs8iy2f7cjgyspxphjw9wk31if") (f (quote (("with-serde" "serde") ("with-actix" "actix-http" "actix-service" "actix-web") ("default" "with-serde"))))))

