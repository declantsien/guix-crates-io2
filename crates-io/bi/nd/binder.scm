(define-module (crates-io bi nd binder) #:use-module (crates-io))

(define-public crate-binder-0.0.1 (c (n "binder") (v "0.0.1") (h "1r7lydvpv31byfqv21i2v9fxdhq0aqdslnwqnjyi5h9ra8iqlqcn")))

(define-public crate-binder-0.0.2 (c (n "binder") (v "0.0.2") (h "005jjzh2vwmdmf8bxffbgd8p4xbrp65ypwkj8fqkda0fr76zpw9f")))

