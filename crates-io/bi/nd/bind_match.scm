(define-module (crates-io bi nd bind_match) #:use-module (crates-io))

(define-public crate-bind_match-0.1.0 (c (n "bind_match") (v "0.1.0") (h "11w6px6av77645vbw8wspwjzxnxah8a0k2jvr18qckjcznkypcbc")))

(define-public crate-bind_match-0.1.1 (c (n "bind_match") (v "0.1.1") (h "0qz9c0yvdv17ljb8ln3bs467qbjpffgvixhdgc8hlr8zp7fhf529")))

(define-public crate-bind_match-0.1.2 (c (n "bind_match") (v "0.1.2") (h "1k96hccydcf9km6cyvimxlyh756ypb13j1866agyjyvcyqv047qp")))

