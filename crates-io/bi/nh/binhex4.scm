(define-module (crates-io bi nh binhex4) #:use-module (crates-io))

(define-public crate-binhex4-0.1.0 (c (n "binhex4") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1p9xwc229dr9axz809zlk9f78dmb6h8rpkf89x22s5k537xcwd7l")))

(define-public crate-binhex4-0.1.1 (c (n "binhex4") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "14j66kksah752a06cblifldj5pzcr2ryi3vz4ik7y4h8qzab2j6m")))

(define-public crate-binhex4-0.1.2 (c (n "binhex4") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0awdibf4y5211vh3w6afffgpwcb7dw9sxl0dlj5dn4j9qbp83v4v")))

