(define-module (crates-io bi nh binhoc_core) #:use-module (crates-io))

(define-public crate-binhoc_core-0.1.0 (c (n "binhoc_core") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "axum-core") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14.20") (d #t) (k 0)))) (h "1wiyf7h4wp3icabr8843sqk6iii6g0lhvbk0p258hkdhnyzdbdsk")))

(define-public crate-binhoc_core-0.1.1 (c (n "binhoc_core") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "axum-core") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "binhoc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.20") (d #t) (k 0)))) (h "1lbklvfw8wmh3d27szspx7ngp75ppfdrvfr963xpvdiy45kjg31j")))

