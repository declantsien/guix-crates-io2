(define-module (crates-io bi nh binhost) #:use-module (crates-io))

(define-public crate-binhost-0.1.0 (c (n "binhost") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1n00cwsdzay2ahvx5jxbr6lbx08nsclqx48wc9h7f9jmbp0v3z7h")))

(define-public crate-binhost-0.2.0 (c (n "binhost") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (o #t) (d #t) (k 0)))) (h "1wfmi1b4x8g3v215q0kw9l3wcm82xq72p4x18imyrssl1v4a1skh") (f (quote (("default" "sha256")))) (s 2) (e (quote (("sha256" "dep:sha2"))))))

(define-public crate-binhost-0.2.1 (c (n "binhost") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (o #t) (d #t) (k 0)))) (h "0k5l7mdx88b7dmd93qvw01jqlnqrbn2jd8isajfvvq4clqihaah2") (f (quote (("default" "sha256")))) (s 2) (e (quote (("sha256" "dep:sha2"))))))

(define-public crate-binhost-0.2.2 (c (n "binhost") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (o #t) (d #t) (k 0)))) (h "1v0fw55b44zcagvnzdkx8nshg3vklaslv81rmv03av40rb0km06f") (f (quote (("default" "sha256")))) (s 2) (e (quote (("sha256" "dep:sha2"))))))

