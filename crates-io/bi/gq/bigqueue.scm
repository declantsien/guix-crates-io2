(define-module (crates-io bi gq bigqueue) #:use-module (crates-io))

(define-public crate-bigqueue-0.0.2 (c (n "bigqueue") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lru") (r "^0.1.13") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0g0l42gbwgnmmll7zjxc0s1ij0kqp1205dmh2pgpk0v0k527cc85")))

