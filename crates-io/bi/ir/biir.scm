(define-module (crates-io bi ir biir) #:use-module (crates-io))

(define-public crate-biir-1.0.0 (c (n "biir") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "16mjx5yh8rdnwbc0dkadjxqhlw49m28p5h776f3yv67bsn68g1mp")))

(define-public crate-biir-1.1.0 (c (n "biir") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0qmlhahnpcwm3xng9l8hq9k7dgg45i3bdcd2i7wnwiwk7i5m54bw")))

(define-public crate-biir-1.1.1 (c (n "biir") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0ppbq7apbhxmx41548p2jgpmyjmi9d2f3h77rf5ci3y9y5gi8lic")))

(define-public crate-biir-1.2.0 (c (n "biir") (v "1.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.1.0") (d #t) (k 0)))) (h "1h1vaiq7pnz5rh1n84pj4h6r0z9ikrd4i9xnjabjrw5k0s2crkdb")))

(define-public crate-biir-1.2.1 (c (n "biir") (v "1.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.1.0") (d #t) (k 0)))) (h "0qndlhvma8s2yhmq814mnlplazlamxzzwdwmfly1v4sf844bl680")))

(define-public crate-biir-1.2.2 (c (n "biir") (v "1.2.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.1.0") (d #t) (k 0)))) (h "00yf2c3n1m78yacw19di887xqgc0gyw6yjmc649qi0dwbc3j8nwc")))

