(define-module (crates-io bi nv binverse) #:use-module (crates-io))

(define-public crate-binverse-0.1.0 (c (n "binverse") (v "0.1.0") (h "08wn2vnakiqqjgga5s5f0nr9msidijvnlxdds6dvqc8bs24cc821")))

(define-public crate-binverse-0.2.0 (c (n "binverse") (v "0.2.0") (h "096887c872pcy7i2j7k76l0dg8fkrvaywfirvhdz0k40gdhln2q4")))

(define-public crate-binverse-0.3.0 (c (n "binverse") (v "0.3.0") (d (list (d (n "binverse_derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1zfkv5zjjbkxsphy1pdzc5h1cz0kpql0lihgyalif397hj6qh8p3") (f (quote (("derive" "binverse_derive") ("default_features" "binverse_derive"))))))

(define-public crate-binverse-0.4.0 (c (n "binverse") (v "0.4.0") (d (list (d (n "binverse_derive") (r "=0.4.0") (o #t) (d #t) (k 0)))) (h "00byp5afikvxfim42r9l84d8qm2vqxx3h4bidbdwbglaq0j6wjdr") (f (quote (("derive" "binverse_derive") ("default_features" "binverse_derive"))))))

(define-public crate-binverse-0.4.1 (c (n "binverse") (v "0.4.1") (d (list (d (n "binverse_derive") (r "=0.4.1") (o #t) (d #t) (k 0)))) (h "1h6qgn18vz1li6rjbv3df6xabkw7bjb2j3mzvyy7vjfl48yz02dr") (f (quote (("derive" "binverse_derive") ("default_features" "binverse_derive"))))))

(define-public crate-binverse-0.5.0 (c (n "binverse") (v "0.5.0") (d (list (d (n "binverse_derive") (r "=0.5.0") (o #t) (d #t) (k 0)))) (h "13mm86axcfv829ldngl590kgmijx0f1f36vizixw9bm5k20qabkq") (f (quote (("derive" "binverse_derive") ("default_features" "binverse_derive"))))))

(define-public crate-binverse-0.5.1 (c (n "binverse") (v "0.5.1") (d (list (d (n "binverse_derive") (r "=0.5.1") (o #t) (d #t) (k 0)))) (h "05rw93df9228py2l619y7glbr719x9qnkhr7bxf7zz7366wnzynr") (f (quote (("derive" "binverse_derive") ("default_features" "binverse_derive"))))))

(define-public crate-binverse-0.5.2 (c (n "binverse") (v "0.5.2") (d (list (d (n "binverse_derive") (r "=0.5.2") (d #t) (k 0)))) (h "1dahzilin2fmjq1b3wpr64xzcrd9i28kf5bczfkf5gj69g2fc0bq") (f (quote (("inline"))))))

(define-public crate-binverse-0.5.3 (c (n "binverse") (v "0.5.3") (d (list (d (n "binverse_derive") (r "=0.5.3") (d #t) (k 0)))) (h "0v4h0sls1j8s76l09sz3pws689wsv14xraxn6yj7i47i0wnw21cq") (f (quote (("inline"))))))

(define-public crate-binverse-0.5.4 (c (n "binverse") (v "0.5.4") (d (list (d (n "binverse_derive") (r "=0.5.4") (d #t) (k 0)))) (h "004d2yvqrkkwh0v5a0mxqs5na3vdadcqw61nvk6najyb40pxgxh4") (f (quote (("inline"))))))

(define-public crate-binverse-0.5.5 (c (n "binverse") (v "0.5.5") (d (list (d (n "binverse_derive") (r "=0.5.5") (d #t) (k 0)))) (h "0wpg3r6vwwcbbrr3xz89ig21yn57xjs4ki0knza21wylhax1kcrk") (f (quote (("inline"))))))

(define-public crate-binverse-0.6.0 (c (n "binverse") (v "0.6.0") (d (list (d (n "binverse_derive") (r "=0.6.0") (d #t) (k 0)))) (h "02n12a5gk10pk0sqknppnwn63rmlllk2rap2jcmx9gq4bkqqjwji")))

(define-public crate-binverse-0.6.1 (c (n "binverse") (v "0.6.1") (d (list (d (n "binverse_derive") (r "=0.6.1") (d #t) (k 0)))) (h "0m78ih4hlzfpkl8s091is854cjsn4xcb8mcjk6bw2arc4wawvzqp")))

(define-public crate-binverse-0.6.2 (c (n "binverse") (v "0.6.2") (d (list (d (n "binverse_derive") (r "=0.6.2") (d #t) (k 0)))) (h "1f5wg7k7jb629cv3ii3iyd9flw54kdkhzcym4x8jldv2rgjxnmyn")))

