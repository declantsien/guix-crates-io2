(define-module (crates-io bi nv binver_derive) #:use-module (crates-io))

(define-public crate-binver_derive-0.0.1 (c (n "binver_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "extra-traits"))) (k 0)))) (h "1vz7pklswslmrp3mwl8x9qyqbwp5c1nq5rz9cfhjd1dllzyganfy")))

(define-public crate-binver_derive-0.1.0 (c (n "binver_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "extra-traits"))) (k 0)))) (h "0359rbwi4l4rg8s0myfza48ia0ylqlmisr3svk9z8cmhi1afj4rw")))

(define-public crate-binver_derive-0.1.1 (c (n "binver_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "extra-traits"))) (k 0)))) (h "1wrk12sspibvz45f5rfwsyv2pmwi0jwckmkpnr9ay3w7gpwhrrnd")))

