(define-module (crates-io bi nv binverse_derive) #:use-module (crates-io))

(define-public crate-binverse_derive-0.3.0 (c (n "binverse_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0k30bry7qsy8gqpk2ndsafhlav4lj2jm59qwmkgqa7hhkylmnczq")))

(define-public crate-binverse_derive-0.4.0 (c (n "binverse_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00il13zqb42955a2xdnn1di2jj8rmwdgmxcsjl54v1cfjslnywng")))

(define-public crate-binverse_derive-0.4.1 (c (n "binverse_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k6ygl3cwy49wmp4j15v9aq28g0xk152xl7jhp353hpygjrdn7bf")))

(define-public crate-binverse_derive-0.5.0 (c (n "binverse_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rbqh4cp32f5av8cbxbxs2xzqqgf2d0v26dsxd697n9mxcp1dm8f")))

(define-public crate-binverse_derive-0.5.1 (c (n "binverse_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07jrqllx7478j2b1wzbw8386zwqv789zlkxpggj68xq3y0a3rscj")))

(define-public crate-binverse_derive-0.5.2 (c (n "binverse_derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w3lz1w9b5qpxmqs0gbg7m2j6z2yfnilcd32cwdbgk0zcq18lxay")))

(define-public crate-binverse_derive-0.5.3 (c (n "binverse_derive") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nny2n1n3m624bf2kqhxikr78zj5jdvbbzg4krdf04809jn9wnw5")))

(define-public crate-binverse_derive-0.5.4 (c (n "binverse_derive") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dgnc2ivigdqbb9xsz6qbdz35lym1pp8jwnyngnyfc1xlnwcvr19")))

(define-public crate-binverse_derive-0.5.5 (c (n "binverse_derive") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1my8z59xbm44s7w26188xxfflzprab51fzhp1d2gzxakq30gl8cg")))

(define-public crate-binverse_derive-0.6.0 (c (n "binverse_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08qmjzias947y3ml930yhvf5fjinfr90yb62p0gl3fz9n3wq7l0l")))

(define-public crate-binverse_derive-0.6.1 (c (n "binverse_derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sya5had9j5vqnx2xjy3nabxdzf4x0scg1y58h2i6zbqw64a1ji5")))

(define-public crate-binverse_derive-0.6.2 (c (n "binverse_derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zzl9majz2a0lfn925c665g4n4md6b9p29hc9rrq6m357qqg7s27")))

