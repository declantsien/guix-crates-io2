(define-module (crates-io bi nv binver) #:use-module (crates-io))

(define-public crate-binver-0.1.0 (c (n "binver") (v "0.1.0") (d (list (d (n "binver_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (k 0)) (d (n "semver") (r "^1.0") (k 0)))) (h "0qh0sxhhx1ry2sjz5pb5vkls7x0kki2ka10ra3rhqj9x9rpwbn8b") (f (quote (("std") ("default" "std"))))))

(define-public crate-binver-0.1.1 (c (n "binver") (v "0.1.1") (d (list (d (n "binver_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (k 0)) (d (n "semver") (r "^1.0") (k 0)))) (h "1sjgw5j2kw0icbdr4zxk213wlr226ivm7s7726kw9xrbwnfkbcc6") (f (quote (("std") ("default" "std"))))))

