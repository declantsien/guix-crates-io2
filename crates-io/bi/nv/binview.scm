(define-module (crates-io bi nv binview) #:use-module (crates-io))

(define-public crate-binview-0.1.0 (c (n "binview") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.9") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vxsacgajyi8j5fcp6rm9w1vv3n39k9hx0m8w692rwp04g652fhj") (y #t)))

(define-public crate-binview-0.1.1 (c (n "binview") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.9") (f (quote ("derive"))) (d #t) (k 0)))) (h "1din0wrqcr1wylw3cmyqdmpz9s1xz7y0h34zn304g3kxca5h8rgk")))

