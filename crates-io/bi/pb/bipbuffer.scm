(define-module (crates-io bi pb bipbuffer) #:use-module (crates-io))

(define-public crate-bipbuffer-0.1.0 (c (n "bipbuffer") (v "0.1.0") (h "1dghi85klb20h0fq8yf3ck6fjxxs3gcgqh0rmpxpgq8qk4bdlvxk")))

(define-public crate-bipbuffer-0.1.1 (c (n "bipbuffer") (v "0.1.1") (h "0hzcxskwq4ynpjnw3zm86kybwbxkbqranxkibcvrzfafgsj390xa")))

(define-public crate-bipbuffer-0.1.2 (c (n "bipbuffer") (v "0.1.2") (h "0c9v6plpibbc924zqbyh091m6r8jb8y496pgqxjmm08avi5f6cwf")))

