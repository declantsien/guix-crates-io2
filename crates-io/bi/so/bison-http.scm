(define-module (crates-io bi so bison-http) #:use-module (crates-io))

(define-public crate-bison-http-0.0.0 (c (n "bison-http") (v "0.0.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "cookie") (r "^0.14") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "http2" "runtime" "server"))) (d #t) (k 0)))) (h "0b31q13ang7mgk662ailz5v1j62m5d2fj2fygb8ha3q6nbxsa7pa")))

