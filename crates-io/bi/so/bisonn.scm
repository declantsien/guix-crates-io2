(define-module (crates-io bi so bisonn) #:use-module (crates-io))

(define-public crate-bisonn-0.1.0 (c (n "bisonn") (v "0.1.0") (d (list (d (n "bson") (r "^2.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1cl3f9ws5zl8b99zp5i6r6xvq1l4vny3axn96bxxba1qc4h1467d")))

(define-public crate-bisonn-1.0.0 (c (n "bisonn") (v "1.0.0") (d (list (d (n "bson") (r "^2.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1xi49kc9wy3jycx1ymz6nhjm9wgyjzv43aslb36k3r7682sn45qv")))

(define-public crate-bisonn-1.0.1 (c (n "bisonn") (v "1.0.1") (d (list (d (n "bson") (r "^2.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0y3qwbqm7agyq3ffhzzs0vxrrn7612ashnzy8h5zvi0yhkcx43hf")))

