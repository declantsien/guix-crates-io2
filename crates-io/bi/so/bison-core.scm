(define-module (crates-io bi so bison-core) #:use-module (crates-io))

(define-public crate-bison-core-0.0.0 (c (n "bison-core") (v "0.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bison-http") (r "^0.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0cqycfsik71n0fjd3cfpmc9q6xp1ycmcnrw706kdl7dpi9l3c60f")))

