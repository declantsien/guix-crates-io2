(define-module (crates-io bi n- bin-layout) #:use-module (crates-io))

(define-public crate-bin-layout-0.1.0 (c (n "bin-layout") (v "0.1.0") (d (list (d (n "data-view") (r "^4") (d #t) (k 0)))) (h "0fcl3gkz5nsk6p1mmphhmgks7rg4ln4l72pvc542zyn03y964s25") (f (quote (("nightly") ("NE" "data-view/NE") ("BE" "data-view/BE"))))))

(define-public crate-bin-layout-0.2.0 (c (n "bin-layout") (v "0.2.0") (d (list (d (n "data-view") (r "^4") (d #t) (k 0)))) (h "0cgrjbx9fpiai1lg995gqp0km237fqbg9spz6qcjryr7wg2n83hg") (f (quote (("nightly") ("NE" "data-view/NE") ("BE" "data-view/BE"))))))

(define-public crate-bin-layout-1.0.0 (c (n "bin-layout") (v "1.0.0") (d (list (d (n "data-view") (r "^5") (d #t) (k 0)) (d (n "derive") (r "^0.1") (d #t) (k 0)))) (h "0w5g93z9rnyd63hz158f58rhz0br72lrc8ky7basfa8q4w534hy6") (f (quote (("nightly") ("NE" "data-view/NE") ("BE" "data-view/BE"))))))

(define-public crate-bin-layout-2.0.0 (c (n "bin-layout") (v "2.0.0") (d (list (d (n "derive") (r "^1") (d #t) (k 0)))) (h "1s6a5m78gmwj0mddardw79fhcf3jy6sscg52s4h55lwm6s3q1ihq") (f (quote (("nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-2.1.0 (c (n "bin-layout") (v "2.1.0") (d (list (d (n "derive") (r "^1") (d #t) (k 0)))) (h "1lalavcrvj0vw6ph69kywqd24c0rcsbfwaqvgndkwxh8ag37y5x1") (f (quote (("nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-3.0.0 (c (n "bin-layout") (v "3.0.0") (d (list (d (n "derive") (r "^1") (d #t) (k 0)))) (h "1npyzki9nxbkz2vsbxbg1nj0szy73kzvqzijll85kwr7bdjhji2b") (f (quote (("nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-3.0.1 (c (n "bin-layout") (v "3.0.1") (d (list (d (n "derive") (r "^1") (d #t) (k 0)))) (h "073wn91cxrz01biwkqp2rl4bgy0q3aagm6mipp89dp20vr4c8x1s") (f (quote (("nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-3.1.0 (c (n "bin-layout") (v "3.1.0") (d (list (d (n "bin-layout-derive") (r "^0.1") (d #t) (k 0)))) (h "09352xjrlar8axz9j8p89bw4xg3wp67nx80cm3l8j86ryhnwivw4") (f (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-4.0.0 (c (n "bin-layout") (v "4.0.0") (d (list (d (n "bin-layout-derive") (r "^0.2") (d #t) (k 0)))) (h "0kp6m8izd5sdn3mrf19w6fpk7xwm3hhl4dg1xmg1jsh22nq0b5n5") (f (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE")))) (y #t)))

(define-public crate-bin-layout-4.0.1 (c (n "bin-layout") (v "4.0.1") (d (list (d (n "bin-layout-derive") (r "^0.2") (d #t) (k 0)))) (h "1gmvgnrqskbvhja25zqsqr0bzsh4924vvz1a1k21q60jhmvwfzpj") (f (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE")))) (y #t)))

(define-public crate-bin-layout-5.0.0 (c (n "bin-layout") (v "5.0.0") (d (list (d (n "bin-layout-derive") (r "^0.3") (d #t) (k 0)) (d (n "stack-array") (r "^0.4") (d #t) (k 0)) (d (n "util-cursor") (r "^0.1") (d #t) (k 0)))) (h "0y0fqg09xlyan81gm9sfvqbaa9gjp7y8ha53aslg1ida9x0pm839") (f (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-5.1.0 (c (n "bin-layout") (v "5.1.0") (d (list (d (n "bin-layout-derive") (r "^0.3") (d #t) (k 0)) (d (n "stack-array") (r "^0.4") (d #t) (k 0)) (d (n "util-cursor") (r "^0.1") (d #t) (k 0)))) (h "1hc0sm6d7p3c7ibslh5j576msl0fh4ll7d62zmw6khl3csxlbzqf") (f (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-6.0.0 (c (n "bin-layout") (v "6.0.0") (d (list (d (n "bin-layout-derive") (r "^0.4") (d #t) (k 0)) (d (n "stack-array") (r "^0.4") (d #t) (k 0)) (d (n "util-cursor") (r "^0.1") (d #t) (k 0)))) (h "1fliq4nsaip0q9s06cxw2r1pwg6vx5mnq1kncss3cqdnz19yl211") (f (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-7.0.0 (c (n "bin-layout") (v "7.0.0") (d (list (d (n "bin-layout-derive") (r "^0.7") (d #t) (k 0)))) (h "152jbws91zs3c04w7dm0vb07mdpivwiik2shsx5rnmrjf2bp4jvl") (f (quote (("NE") ("L2") ("BE"))))))

(define-public crate-bin-layout-7.1.0 (c (n "bin-layout") (v "7.1.0") (d (list (d (n "bin-layout-derive") (r "^0.7") (d #t) (k 0)))) (h "1dicb1bifyap33f09df63ip5qmqwrmcmk44312kbf2d7arjl7152") (f (quote (("NE") ("L2") ("BE"))))))

