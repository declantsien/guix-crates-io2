(define-module (crates-io bi n- bin-tree) #:use-module (crates-io))

(define-public crate-bin-tree-0.1.0 (c (n "bin-tree") (v "0.1.0") (h "1m4sf6hh1z4l8wpj7isz16s10nf2vi2sj1v2sqxwprmns3x50ciw")))

(define-public crate-bin-tree-0.2.0 (c (n "bin-tree") (v "0.2.0") (h "0bl74ldlshsax646qxdm6zhgyjgzagx3085f40c8hifsz33s7nqf")))

(define-public crate-bin-tree-0.2.1 (c (n "bin-tree") (v "0.2.1") (h "0065zmm49f6lmphrpj7jj8cwvdxgpw92wfxdm6893pni7wh902jf")))

(define-public crate-bin-tree-0.3.0 (c (n "bin-tree") (v "0.3.0") (h "1simknp200c1np1vnkmjblyrg3galii30s7gfca9983af7kz83da")))

(define-public crate-bin-tree-0.4.0 (c (n "bin-tree") (v "0.4.0") (h "0dv8x7lvqxb94l7m5ym5znlk6ihp2h9i190010xs2lmdh1yv467y")))

(define-public crate-bin-tree-0.5.0 (c (n "bin-tree") (v "0.5.0") (h "1fphqfz7bcn4494xiqfgkkk683i16p65vbr1j3i0mmhrz4nhzkg3")))

(define-public crate-bin-tree-0.6.0 (c (n "bin-tree") (v "0.6.0") (d (list (d (n "uints") (r "^0.8.0") (d #t) (k 0)))) (h "0291axgfyn4k8mh004bnnr49hvg5lry6g6ns3sprx5a92y0x4lfq")))

(define-public crate-bin-tree-0.6.1 (c (n "bin-tree") (v "0.6.1") (d (list (d (n "uints") (r "^0.8.1") (d #t) (k 0)))) (h "1hcbl5kczx1lhaz6y3g29yjdgx6q55zca5jw3155d1qvcn8a5vag")))

(define-public crate-bin-tree-0.6.2 (c (n "bin-tree") (v "0.6.2") (d (list (d (n "uints") (r "^0.8.1") (d #t) (k 0)))) (h "07y7r0my2g4kccwva9i1963z26s2wmkk4fc5vlw9sfkvsw01bclb")))

(define-public crate-bin-tree-0.7.0 (c (n "bin-tree") (v "0.7.0") (d (list (d (n "uints") (r "^0.9.0") (d #t) (k 0)))) (h "14hx99rniklwdq5l3582qprrflxqad56qfzic22q37bjg0dlfacp")))

(define-public crate-bin-tree-0.7.1 (c (n "bin-tree") (v "0.7.1") (d (list (d (n "uints") (r "^0.9.0") (d #t) (k 0)))) (h "0q6jhj0zk5gz190cna7hdbf2r0549r71f12y0zkysr920y1npdii")))

(define-public crate-bin-tree-0.8.0 (c (n "bin-tree") (v "0.8.0") (d (list (d (n "uints") (r "^0.10.0") (d #t) (k 0)))) (h "1a9vp9gbir51w22s86sm4y4pm4w021nnfvvsy22ysf96ybvw90s9")))

(define-public crate-bin-tree-0.9.0 (c (n "bin-tree") (v "0.9.0") (d (list (d (n "uints") (r "^0.11.0") (d #t) (k 0)))) (h "0rfzg3hn0sgd328x54ynr894cx5lygc3ky65whdpk4g3m9nkaixk")))

(define-public crate-bin-tree-0.9.1 (c (n "bin-tree") (v "0.9.1") (d (list (d (n "uints") (r "^0.11.0") (d #t) (k 0)))) (h "1wslmdgk0ih4s1qsagvjrah2l52p11p9hxvd0jcyaswf4j6b18px")))

(define-public crate-bin-tree-0.9.2 (c (n "bin-tree") (v "0.9.2") (d (list (d (n "uints") (r "^0.11.0") (d #t) (k 0)))) (h "1rlbfixy8yf28q474zgdjz1j2yz17sbip6klyb8z4dyg3jlh303b")))

(define-public crate-bin-tree-0.10.0 (c (n "bin-tree") (v "0.10.0") (d (list (d (n "uints") (r "^0.11.1") (d #t) (k 0)))) (h "07gh78wvrnlpg3rvbwa8j71ik5q3xnfgyhci1pyv79vbawfcwbxs")))

(define-public crate-bin-tree-0.10.1 (c (n "bin-tree") (v "0.10.1") (d (list (d (n "uints") (r "^0.11.2") (d #t) (k 0)))) (h "1jyz4sg2yjw3zr039gh6vla5kvvzl5ggw8bcr7xwzbji1ww0n7gg")))

