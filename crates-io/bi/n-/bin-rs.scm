(define-module (crates-io bi n- bin-rs) #:use-module (crates-io))

(define-public crate-bin-rs-0.0.1 (c (n "bin-rs") (v "0.0.1") (d (list (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (o #t) (d #t) (k 0)))) (h "0j0zn00jgv0kfxhh5s0xkrgmar71z65xby86p9j6lxbbfiiaj08n") (f (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (y #t) (r "1.58")))

(define-public crate-bin-rs-0.0.2 (c (n "bin-rs") (v "0.0.2") (d (list (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (o #t) (d #t) (k 0)))) (h "16i4jxjg9mr0p4h3zb3mj3l2limaxqs1gg7ksyafj5f71znd1d9x") (f (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (y #t) (r "1.58")))

(define-public crate-bin-rs-0.0.3 (c (n "bin-rs") (v "0.0.3") (d (list (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (o #t) (d #t) (k 0)))) (h "0ji4cgfpzvi5cp8z9w8vrw34jgf9n2lrnzsxzzgw74002818k4kn") (f (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (y #t) (r "1.58")))

(define-public crate-bin-rs-0.0.4 (c (n "bin-rs") (v "0.0.4") (d (list (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (o #t) (d #t) (k 0)))) (h "1m1238bb4avpkmi2s6qw6wxm2cdjnqbpmfmwqjhv72mshl27sd90") (f (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (r "1.58")))

(define-public crate-bin-rs-0.0.5 (c (n "bin-rs") (v "0.0.5") (d (list (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (o #t) (d #t) (k 0)))) (h "1gqj6ngd27axwyny8nh66ghadsq4142zgc643m4350z6ddr97c9x") (f (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (r "1.58")))

(define-public crate-bin-rs-0.0.6 (c (n "bin-rs") (v "0.0.6") (d (list (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (o #t) (d #t) (k 0)))) (h "0m8kfp92nr9hwhi3fg755v4pj21aw8h0pkkr3pak09i11q4y56xh") (f (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (r "1.58")))

(define-public crate-bin-rs-0.0.7 (c (n "bin-rs") (v "0.0.7") (d (list (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (o #t) (d #t) (k 0)))) (h "0gcw7nrpxyg9496arfwa70ga0031m14z4psvxqp1a9wiw7d14bp6") (f (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (r "1.58")))

(define-public crate-bin-rs-0.0.8 (c (n "bin-rs") (v "0.0.8") (d (list (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (o #t) (d #t) (k 0)))) (h "0i05jgp1mmfqajz6qia1ghajkp5sxxdyp59spi1imqsdsmk46dsv") (f (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (y #t) (r "1.71")))

(define-public crate-bin-rs-0.0.9 (c (n "bin-rs") (v "0.0.9") (d (list (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "rt"))) (o #t) (d #t) (k 0)))) (h "0q94gq21pr2br43z28idlgfiiyq4d4anfkvyj1rm9whwsmz3km0v") (f (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (r "1.71")))

