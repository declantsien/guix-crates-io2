(define-module (crates-io bi n- bin-cpuflags-x86) #:use-module (crates-io))

(define-public crate-bin-cpuflags-x86-1.0.0 (c (n "bin-cpuflags-x86") (v "1.0.0") (d (list (d (n "iced-x86") (r "^1.20") (f (quote ("std" "decoder" "instr_info"))) (k 0)) (d (n "object") (r "^0.32") (d #t) (k 0)))) (h "1zfhdk8blpp105qa4daa8nc47vxlzcfa9sxm5a3y31m7vv04wshw")))

(define-public crate-bin-cpuflags-x86-1.0.1 (c (n "bin-cpuflags-x86") (v "1.0.1") (d (list (d (n "iced-x86") (r "^1.20") (f (quote ("std" "decoder" "instr_info"))) (k 0)) (d (n "object") (r "^0.32") (d #t) (k 0)))) (h "1j259z2jk43sj03ybflw1kg0mvbqwywfvnmwxramf9530is39nfa")))

(define-public crate-bin-cpuflags-x86-1.0.2 (c (n "bin-cpuflags-x86") (v "1.0.2") (d (list (d (n "iced-x86") (r "^1.20") (f (quote ("std" "decoder" "instr_info"))) (k 0)) (d (n "object") (r "^0.34") (d #t) (k 0)))) (h "1zrzkywgmrp663p4lfd3nry0rhs9qg97myrkpgxr1hplfjij7xfh")))

