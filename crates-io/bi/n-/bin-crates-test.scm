(define-module (crates-io bi n- bin-crates-test) #:use-module (crates-io))

(define-public crate-bin-crates-test-0.1.0 (c (n "bin-crates-test") (v "0.1.0") (h "00ajssr0dlr7fig3rm3aj7255i43f6q160ddmbyqjk6kdv4vybdf") (y #t)))

(define-public crate-bin-crates-test-0.1.1 (c (n "bin-crates-test") (v "0.1.1") (h "19518imdz5iy1k0vr6g4dgl750wlgz8qad4c0zvdkrlar8s1xcmm") (y #t)))

(define-public crate-bin-crates-test-0.1.11 (c (n "bin-crates-test") (v "0.1.11") (h "1lf95ldagf851wjn4ahrsyv0cgd07cx5fgdy9gr0anlp9fnb7jbk") (y #t)))

