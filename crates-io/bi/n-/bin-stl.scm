(define-module (crates-io bi n- bin-stl) #:use-module (crates-io))

(define-public crate-bin-stl-0.1.0 (c (n "bin-stl") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1d32s8p50xfd1g9l3q5k99yxf21ixrgf1pgsggy1hpjrzhfi3ybd")))

(define-public crate-bin-stl-0.1.1 (c (n "bin-stl") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1qj2xcgjlnzgmqh435qj14dijgjz63r8a09j73an26vf4nz8rjdf")))

