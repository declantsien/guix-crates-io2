(define-module (crates-io bi n- bin-pool) #:use-module (crates-io))

(define-public crate-bin-pool-0.1.0 (c (n "bin-pool") (v "0.1.0") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("alloc"))) (d #t) (k 0)))) (h "12w8im60v4razb9w05k4rmhypykn6rabdrhgr2856zxyc9ki011n") (f (quote (("std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-bin-pool-0.1.1 (c (n "bin-pool") (v "0.1.1") (h "1cilac4983qc8vr80maxn911r06zj5kwjaffwvyinj6y04vmg1z2")))

