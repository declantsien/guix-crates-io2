(define-module (crates-io bi n- bin-layout-derive) #:use-module (crates-io))

(define-public crate-bin-layout-derive-0.1.0 (c (n "bin-layout-derive") (v "0.1.0") (d (list (d (n "virtue") (r "^0.0.8") (d #t) (k 0)))) (h "0vmkij3qff2jj0kpbg7fc4n44jqb100lr39k60bfwnhdxrgfsn7m")))

(define-public crate-bin-layout-derive-0.2.0 (c (n "bin-layout-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1qlqcxq7ij8m98d89f79d8jc9hwplb89m50qjggmpn5avm6c17dx")))

(define-public crate-bin-layout-derive-0.3.0 (c (n "bin-layout-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1sbx9w3pvcwpcj60a8jlr4c3w70nbwydax6iz5xrkfvhi2rp0ff3")))

(define-public crate-bin-layout-derive-0.4.0 (c (n "bin-layout-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ka7b6yqfh2hqx3r32lyy07z2k8q2yv3klkizz9409cwfy672340")))

(define-public crate-bin-layout-derive-0.7.0 (c (n "bin-layout-derive") (v "0.7.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jxk7xa1jqzalyi7lpjidriy8b0cy79zzaqp1gsdpxq2xpycpifl") (f (quote (("sizehint"))))))

