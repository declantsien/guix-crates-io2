(define-module (crates-io bi n- bin-proto) #:use-module (crates-io))

(define-public crate-bin-proto-0.1.0 (c (n "bin-proto") (v "0.1.0") (d (list (d (n "bin-proto-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "0000lh8yc1l97kh1sx1whvcjw5xsc71szffcmnxzjs3284v1f8h9") (f (quote (("derive" "bin-proto-derive") ("default" "derive" "uuid"))))))

(define-public crate-bin-proto-0.2.0 (c (n "bin-proto") (v "0.2.0") (d (list (d (n "bin-proto-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "0fq9yviifj37x8bl03hrr013xq3rsvi3l2g3pps8cpy8akbhri48") (f (quote (("derive" "bin-proto-derive") ("default" "derive" "uuid")))) (y #t)))

(define-public crate-bin-proto-0.2.1 (c (n "bin-proto") (v "0.2.1") (d (list (d (n "bin-proto-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "11ay36br2x96wi0na96wg8dzzw39xvh4m5gj4632x38r5708bjc6") (f (quote (("derive" "bin-proto-derive") ("default" "derive" "uuid"))))))

(define-public crate-bin-proto-0.3.0 (c (n "bin-proto") (v "0.3.0") (d (list (d (n "bin-proto-derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "02pi462fqqgm549aw9sh0z3mdpc6hmib8i46hvqhyc7fr6zn9q8n") (f (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-0.3.1 (c (n "bin-proto") (v "0.3.1") (d (list (d (n "bin-proto-derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "14jdjjzjfsvp7n60dzd0lm5zn2rczm7i2mly1i6l39kv9v6hfq5r") (f (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-0.3.2 (c (n "bin-proto") (v "0.3.2") (d (list (d (n "bin-proto-derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1inyp6120yx3d17lc5wh2ginlynnpp79ww973p6n21ncf0qk916m") (f (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-0.3.3 (c (n "bin-proto") (v "0.3.3") (d (list (d (n "bin-proto-derive") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0xfdm1qh6iimg2680hm22d5vi22ljs7pcgyqcw6qm4q24r65wwvk") (f (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-0.3.4 (c (n "bin-proto") (v "0.3.4") (d (list (d (n "bin-proto-derive") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "00ws64f9ailpr6l7z31kydhsqwbcfirf4hs2hy2gnx2hkj2b09y0") (f (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-0.4.0 (c (n "bin-proto") (v "0.4.0") (d (list (d (n "bin-proto-derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "01qfgq2hfd267vplxnpcrnvbr83gp74390hagxg5ya390kibaz5b") (f (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

