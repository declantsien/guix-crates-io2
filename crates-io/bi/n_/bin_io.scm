(define-module (crates-io bi n_ bin_io) #:use-module (crates-io))

(define-public crate-bin_io-0.1.0 (c (n "bin_io") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "1p7cb765r2179gizjllhyg0sxbls4i6nmnmwvp55ppn8991p24ig")))

(define-public crate-bin_io-0.1.1 (c (n "bin_io") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "11xi53l9m61ipyjsa3hn1zdj3p0g1mm92646y6vnjy64gplckdhj")))

(define-public crate-bin_io-0.1.2 (c (n "bin_io") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "049sxvzw1ry19n8y6a9371zcr4a3zs6zyph0aabb4a1xd06xxwav")))

(define-public crate-bin_io-0.2.0 (c (n "bin_io") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "16yppfhln6bjml7c5ki8drf95176hj6x233b00p8l4g89py6anwh")))

