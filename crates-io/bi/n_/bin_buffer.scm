(define-module (crates-io bi n_ bin_buffer) #:use-module (crates-io))

(define-public crate-bin_buffer-0.1.0 (c (n "bin_buffer") (v "0.1.0") (h "1zncq17h35vi3d0plfr80gqp27mxk083b29w9lky2b7s4v4kdi8q") (y #t)))

(define-public crate-bin_buffer-0.1.1 (c (n "bin_buffer") (v "0.1.1") (h "0v09l9i3gzjk78058ymlcshcsaji9ykssx90bdssadygsfii0kig") (y #t)))

(define-public crate-bin_buffer-0.1.2 (c (n "bin_buffer") (v "0.1.2") (h "058njn5g2zhcjwnl8xqf5y5bggvrsy0y000q3b5zn9hcdadn3ral") (y #t)))

(define-public crate-bin_buffer-0.1.3 (c (n "bin_buffer") (v "0.1.3") (h "04y6af2il794qwjccdw8p6nma364sqikv40hfxjxay05v6370721") (y #t)))

(define-public crate-bin_buffer-0.1.4 (c (n "bin_buffer") (v "0.1.4") (h "0l0v8wq8p4dbg717mx9rqfndxs5lvi9n8mnplqxwrss9m39ch3h7") (y #t)))

(define-public crate-bin_buffer-0.1.5 (c (n "bin_buffer") (v "0.1.5") (h "1ndy1fjprir3jil00ljx3v624nvkmj3s27yqnb50n6357rb91rv9") (y #t)))

(define-public crate-bin_buffer-0.1.6 (c (n "bin_buffer") (v "0.1.6") (h "07ki8zx9lw6zlbpw7ylb2s254bpb9n8hzv6xxcyrdz38k0lnca1m") (y #t)))

(define-public crate-bin_buffer-0.1.7 (c (n "bin_buffer") (v "0.1.7") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)))) (h "1kdlfrsyk9mlqvhjjfs8cm3r9bb6k9jg450dzjdqhdyk3jilb73c") (y #t)))

(define-public crate-bin_buffer-0.1.8 (c (n "bin_buffer") (v "0.1.8") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)))) (h "13n2xpr64r246m8d19czbggsrqyf6zmcc2mb2120qk55lsjcd1bc") (y #t)))

(define-public crate-bin_buffer-0.1.9 (c (n "bin_buffer") (v "0.1.9") (d (list (d (n "fnrs") (r "^0.1.4") (d #t) (k 0)))) (h "1wclmg19hpp93l03x5lmi79p8xfxl7pnrddykx534qkp83pqc93p") (y #t)))

(define-public crate-bin_buffer-0.1.10 (c (n "bin_buffer") (v "0.1.10") (h "1b1kpz8f62zpasmn9gc3hnll572mrpxqqfb8hdvp1rnwkbiny8kc") (y #t)))

(define-public crate-bin_buffer-0.1.11 (c (n "bin_buffer") (v "0.1.11") (h "1kcv5nl0r06h58zgd9j2az0q3rfyc8j2597q4kr2siylhhb2vja6") (y #t)))

(define-public crate-bin_buffer-0.1.12 (c (n "bin_buffer") (v "0.1.12") (h "0wgim7sp2daxh6yjshkpbg5w1lrydibb751dnl52cs3n86hws0gd")))

