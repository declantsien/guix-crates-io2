(define-module (crates-io bi n_ bin_chicken) #:use-module (crates-io))

(define-public crate-bin_chicken-0.1.0 (c (n "bin_chicken") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "trash") (r "^2.1.5") (d #t) (k 0)))) (h "1m2mms43f5i4c1pp4vzrwy2n7mj55wmzvbn67zavy6ggbr0ifydw")))

