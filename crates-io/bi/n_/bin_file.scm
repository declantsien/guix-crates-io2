(define-module (crates-io bi n_ bin_file) #:use-module (crates-io))

(define-public crate-bin_file-0.1.0 (c (n "bin_file") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fvq8nisl5v7x1xx1mklb1hydvzksjiir71dwfvx23laf259y01g") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-bin_file-0.1.1 (c (n "bin_file") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qb3s201nkwkzjigzxb50psp0bf9jmylxavnn6np4hwvv1rai4k9") (f (quote (("std" "serde/std") ("default" "std"))))))

