(define-module (crates-io bi n_ bin_common) #:use-module (crates-io))

(define-public crate-bin_common-0.2.0 (c (n "bin_common") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (f (quote ("cpp_demangle"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xz2") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1j2q90cz049yfr5vh6rqki9hfbmavp6am0d9ny4f99c8m8p4xrff") (f (quote (("panic_handler" "backtrace") ("log_rotation" "xz2") ("default" "log_rotation" "panic_handler"))))))

