(define-module (crates-io bi n_ bin_codec_derive) #:use-module (crates-io))

(define-public crate-bin_codec_derive-0.1.0 (c (n "bin_codec_derive") (v "0.1.0") (d (list (d (n "bin_codec") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qbrzf6kf8awvmf16f9wdqlsdawi16bkhd2askwh15q5wbcgmycc")))

