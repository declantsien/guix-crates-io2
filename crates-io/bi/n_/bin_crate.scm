(define-module (crates-io bi n_ bin_crate) #:use-module (crates-io))

(define-public crate-bin_crate-0.1.0 (c (n "bin_crate") (v "0.1.0") (h "1djrqwklcxnsv73a0qnz87dw9d7cfi7h8z3mnzkfw4wip9vcnaxy")))

(define-public crate-bin_crate-0.1.1 (c (n "bin_crate") (v "0.1.1") (h "1b9wm8xv9x9wkjkd24b6vpf0ryd0lmnc8q1mdmf1fsrfkv925295")))

