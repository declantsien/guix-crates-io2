(define-module (crates-io bi n_ bin_packer_3d) #:use-module (crates-io))

(define-public crate-bin_packer_3d-0.1.0 (c (n "bin_packer_3d") (v "0.1.0") (h "1na9phygc4890f4d670fnwhj313i4gap1j8z3np9cqslar7hdyjf")))

(define-public crate-bin_packer_3d-0.1.1 (c (n "bin_packer_3d") (v "0.1.1") (h "1ir6bvvnxb8khynzbl88p8yxrhx15w2y7gs82hnl9npk2wamxa45")))

(define-public crate-bin_packer_3d-0.1.2 (c (n "bin_packer_3d") (v "0.1.2") (h "0lgmzcxlhsjnflvx8pynkp5264m7d6mn4qfxww0jp0s0qf7y58fd")))

(define-public crate-bin_packer_3d-1.0.0 (c (n "bin_packer_3d") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "09xgxxq354kav1ljir3dglp7gbmi94vy70m0mdlmv20pv4f553cr")))

(define-public crate-bin_packer_3d-2.0.0-beta-1 (c (n "bin_packer_3d") (v "2.0.0-beta-1") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0sqdkzix79s0h1w7rrf6q8wib26xxddcffa7im1n18103p6lcagx")))

