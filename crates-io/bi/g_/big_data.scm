(define-module (crates-io bi g_ big_data) #:use-module (crates-io))

(define-public crate-big_data-0.1.0 (c (n "big_data") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "ordermap") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rustler") (r "^0.22.0-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "146k8yld6hxwwr207r88wdz28pfr0y5d3j9g30488rxdjbyc1ba7")))

(define-public crate-big_data-0.1.1 (c (n "big_data") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "ordermap") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rustler") (r "^0.22.0-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rspxrcv3w98iw5fa795j4finzsfnavwk6cd8sx2755811pxgr2g")))

(define-public crate-big_data-0.1.2 (c (n "big_data") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "ordermap") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rustler") (r "^0.22.0-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04pwjl04357pr5gii0h0z1blcl6f278xaqflgl1h07f9zg65g3al")))

