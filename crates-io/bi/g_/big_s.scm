(define-module (crates-io bi g_ big_s) #:use-module (crates-io))

(define-public crate-big_s-1.0.0 (c (n "big_s") (v "1.0.0") (h "1wrwvzs2i8aq95hkrxvcxwahccn0y94jkcrvd4sjr7rv7dcidiyc")))

(define-public crate-big_s-1.0.1 (c (n "big_s") (v "1.0.1") (h "0qii7fq4256zqcgs72789l23q5r6fb06fmrfj85n1i14r22wfjl9")))

(define-public crate-big_s-1.0.2 (c (n "big_s") (v "1.0.2") (h "1a2igv4pddakvshmp69jyxxrhzcbpjhfc8i41jqq64k3j1xxp7hr")))

