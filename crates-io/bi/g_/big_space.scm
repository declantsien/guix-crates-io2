(define-module (crates-io bi g_ big_space) #:use-module (crates-io))

(define-public crate-big_space-0.1.0 (c (n "big_space") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_render" "bevy_winit" "x11"))) (k 2)) (d (n "bevy_polyline") (r "^0.4") (d #t) (k 0)))) (h "12vx29smx9hrrczvi3svhh7hb8xjplih2nwhg8f35fb6xvyhwzsq")))

(define-public crate-big_space-0.1.1 (c (n "big_space") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_render" "bevy_winit" "x11"))) (k 2)) (d (n "bevy_polyline") (r "^0.4") (o #t) (d #t) (k 0)))) (h "01ysqa7qzr6c75axmjljjbql1fwl37cq7zs19fsn4lkvzlfg65y3") (f (quote (("default" "debug") ("debug" "bevy_polyline"))))))

(define-public crate-big_space-0.1.2 (c (n "big_space") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_render" "bevy_winit" "x11"))) (k 2)) (d (n "bevy_polyline") (r "^0.4") (o #t) (d #t) (k 0)))) (h "099c3x2krrvv4d9qr1x72g0hfaxw7s6mmvl46fb4i7cghzyrksb1") (f (quote (("default" "debug") ("debug" "bevy_polyline"))))))

(define-public crate-big_space-0.1.3 (c (n "big_space") (v "0.1.3") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (f (quote ("bevy_render" "bevy_winit" "x11"))) (k 2)) (d (n "bevy_polyline") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1bvh9zkgxphdhd1nd52l87x3dnsbc2bpwagjqfxlsxbzqxn56gzi") (f (quote (("default" "debug") ("debug" "bevy_polyline"))))))

(define-public crate-big_space-0.2.0 (c (n "big_space") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_text" "bevy_ui" "bevy_render" "bevy_winit" "x11"))) (k 2)) (d (n "bevy_framepace") (r "^0.12") (d #t) (k 2)) (d (n "bevy_polyline") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1gc8nnznqylw1rd00b4qczqp7shinj85hqlgy4a4h2wa72pnr07w") (f (quote (("default" "debug") ("debug" "bevy_polyline"))))))

(define-public crate-big_space-0.3.0 (c (n "big_space") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (d #t) (k 2)) (d (n "bevy_framepace") (r "^0.13") (k 2)))) (h "0siwh353906jiifzdg7j3rqq4wz7mgjji04gpkf0v98awwza6fw9") (f (quote (("default" "debug" "camera") ("debug" "bevy/bevy_gizmos") ("camera" "bevy/bevy_render"))))))

(define-public crate-big_space-0.4.0 (c (n "big_space") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_winit" "default_font" "bevy_ui" "bevy_pbr" "x11" "tonemapping_luts"))) (k 2)) (d (n "bevy_framepace") (r "^0.14") (k 2)))) (h "0kg7mfv2k7fhina1clzjbrlw1fzsyscjjsmjfmwf1zgzxxwzv271") (f (quote (("default" "debug" "camera" "bevy_render") ("debug" "bevy/bevy_gizmos") ("camera" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-big_space-0.5.0 (c (n "big_space") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_winit" "default_font" "bevy_ui" "bevy_pbr" "x11" "tonemapping_luts"))) (k 2)) (d (n "bevy_framepace") (r "^0.15") (k 2)))) (h "0244ry5jynr7aj3xcr4bbzk4g9sky6w3hgv57y0wpbnih5rmf5a8") (f (quote (("default" "debug" "camera" "bevy_render") ("debug" "bevy/bevy_gizmos") ("camera" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-big_space-0.6.0 (c (n "big_space") (v "0.6.0") (d (list (d (n "bevy") (r "^0.13") (k 0)) (d (n "bevy") (r "^0.13") (f (quote ("bevy_winit" "default_font" "bevy_ui" "bevy_pbr" "x11" "tonemapping_luts"))) (k 2)) (d (n "bevy_framepace") (r "^0.15") (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vwm39gvdfd6125v02sfbk1idc735q67jibiv5kikdljhrsp0bjb") (f (quote (("default" "debug" "camera" "bevy_render") ("debug" "bevy/bevy_gizmos") ("camera" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

