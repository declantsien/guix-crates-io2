(define-module (crates-io bi g_ big_unsigned_ints) #:use-module (crates-io))

(define-public crate-big_unsigned_ints-0.1.0 (c (n "big_unsigned_ints") (v "0.1.0") (h "1mqha3k301vkwhcvprz2pl4yvwmz8nyqhbj1kmyd8zj98z9dhr5k")))

(define-public crate-big_unsigned_ints-0.1.1 (c (n "big_unsigned_ints") (v "0.1.1") (h "18pah3zjpcvw36rq0m7yycn1ix4wn37lf0wb98dy4hgx3wj9mvzw")))

(define-public crate-big_unsigned_ints-0.1.2 (c (n "big_unsigned_ints") (v "0.1.2") (h "0crw469mqwwnng0fn7saaizzw8r68aaylz3zp0x2r15rpcdnnnsz")))

(define-public crate-big_unsigned_ints-0.1.3 (c (n "big_unsigned_ints") (v "0.1.3") (h "1w85mw2acx206vfnyb3rp1saghh6xwqyqaq3z3jpcwmjlf0fzn6j")))

(define-public crate-big_unsigned_ints-0.1.4 (c (n "big_unsigned_ints") (v "0.1.4") (h "1d0832qcw55s57s9h90ha5lkkf3js27s6a207x1bcr44biavg5ab")))

(define-public crate-big_unsigned_ints-0.1.5 (c (n "big_unsigned_ints") (v "0.1.5") (h "1f6p8v3vw3p8zmyrz9lk7fniqdlgc0m68lpw8y8j3ff2w4vr5df5")))

(define-public crate-big_unsigned_ints-0.1.6 (c (n "big_unsigned_ints") (v "0.1.6") (h "0vg0fym8dgnv8wnlwwq2d25v9mpy7qr4y238fm1pcqjd0g24r91v")))

