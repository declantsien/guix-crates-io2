(define-module (crates-io bi g_ big_mac) #:use-module (crates-io))

(define-public crate-big_mac-0.1.0 (c (n "big_mac") (v "0.1.0") (h "1ki1b5m9z8lb529zrqk8fcr55z2c69z5a2xg46vl40463rim8gqc") (y #t)))

(define-public crate-big_mac-0.1.1 (c (n "big_mac") (v "0.1.1") (h "0k46ykq8kxy3rw6gal1d15q4vj2czd6cpizchjrin0800d3h52vg") (y #t)))

(define-public crate-big_mac-0.1.2 (c (n "big_mac") (v "0.1.2") (h "016kf3gjfv6n53mpp0g0ac9ffwx2a8h1fjvhjc9j38yd6ai03dkd") (y #t)))

(define-public crate-big_mac-0.1.3 (c (n "big_mac") (v "0.1.3") (h "1cdyzzl35is44wkpfz42raly57viqc2wzirfxwnn9v00s3ckiab0") (y #t)))

(define-public crate-big_mac-0.1.4 (c (n "big_mac") (v "0.1.4") (h "19r3qpryvdqqzq0y6nq1x05q523yb4bhgv72wwi29n9mi1f8n3sf") (y #t)))

(define-public crate-big_mac-0.1.5 (c (n "big_mac") (v "0.1.5") (h "1h2jw8wd302n18mxplkaisamc1cgbrsax26k8yfp7l7a77b96x9q") (y #t)))

(define-public crate-big_mac-0.1.6 (c (n "big_mac") (v "0.1.6") (h "00clrwy6gh54wf0d5lyhy4c6za1w38dg56whh1dxp3dxnqdz2d84") (y #t)))

(define-public crate-big_mac-0.1.7 (c (n "big_mac") (v "0.1.7") (h "0rwkmyxxwhlf4in1h8pk3hg5nanxvh14wz04xvxdkc99dp4zfil2")))

