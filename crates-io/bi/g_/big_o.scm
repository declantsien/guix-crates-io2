(define-module (crates-io bi g_ big_o) #:use-module (crates-io))

(define-public crate-big_o-0.1.0 (c (n "big_o") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "lstsq") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10x3pz395lnnl3lakifl2lywl9p2v6h0wgzksz82ky7c7lxps2lg")))

(define-public crate-big_o-0.1.1 (c (n "big_o") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "lstsq") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1cck8dknlqa6akz1kk3gkfxvr249jxv3mbigsglbzvwg8nidkv1b")))

(define-public crate-big_o-0.1.2 (c (n "big_o") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "lstsq") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1xxwj7y2ldfajh1avb5mxysvm7mxvsc3yjxkb0q1makhgxyns9dx")))

(define-public crate-big_o-0.1.3 (c (n "big_o") (v "0.1.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "lstsq") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0xg8q1zxl5lj349h9g4qdfjjdwvj50pbgph5rcz52lbra4l51b08")))

(define-public crate-big_o-0.1.4 (c (n "big_o") (v "0.1.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "lstsq") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03pzqd1m6a35q16p0k1flpn0cz1f5rfbx02xj9729k8sc1qj9mxy")))

