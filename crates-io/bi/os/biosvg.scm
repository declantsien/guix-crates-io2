(define-module (crates-io bi os biosvg) #:use-module (crates-io))

(define-public crate-biosvg-0.1.0 (c (n "biosvg") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15bxyqampgkysc58s7l4b12s4fpv3ksxn3aakim1dwlzn8yzdrw1")))

(define-public crate-biosvg-0.1.1 (c (n "biosvg") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n68m26bl2kzz2vl0fmxyw4gaqlpd4n7i1fpbvfjb3vg0fgd2i2p")))

(define-public crate-biosvg-0.1.2 (c (n "biosvg") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0niv441ijiid6qpgcvf8c9zzwgl15f9xs6ki3cc8zxra2a78n7sm")))

(define-public crate-biosvg-0.1.3 (c (n "biosvg") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y77b6f87x6x4cjz11hj0fsq7b2x77gax8qp8kf8ijlap22hgiai")))

(define-public crate-biosvg-0.1.4 (c (n "biosvg") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xk2imdms3wzjfrq2hgi8kc0cb4pgk1q0n356v450vf00mn8rvcp")))

