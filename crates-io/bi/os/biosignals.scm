(define-module (crates-io bi os biosignals) #:use-module (crates-io))

(define-public crate-biosignals-0.1.0 (c (n "biosignals") (v "0.1.0") (d (list (d (n "dsp") (r "^0.7.1") (d #t) (k 0)) (d (n "matfile") (r "^0.2") (d #t) (k 0)))) (h "0ivnyynaclcdv0yn5z1i4xlrzma6x1qvqbqbyi30wpz4padlwdzg") (y #t)))

