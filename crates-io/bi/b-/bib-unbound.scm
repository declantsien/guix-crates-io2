(define-module (crates-io bi b- bib-unbound) #:use-module (crates-io))

(define-public crate-bib-unbound-0.1.0 (c (n "bib-unbound") (v "0.1.0") (h "1sq7l21sr64swp60vss5y4hb4fcpy5hznyki64f89wpan0gb4wm1")))

(define-public crate-bib-unbound-0.2.0 (c (n "bib-unbound") (v "0.2.0") (h "14bacsgj4hcz0a5zy1drxrcv4s708jlf59gpz9ksclir8hmxcq67")))

(define-public crate-bib-unbound-0.3.0 (c (n "bib-unbound") (v "0.3.0") (h "1p17b7rqffdnq8p9i7af0wxckvx49sc3i4zqjwhy0q2kl2d6qkvr")))

