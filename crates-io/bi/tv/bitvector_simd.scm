(define-module (crates-io bi tv bitvector_simd) #:use-module (crates-io))

(define-public crate-bitvector_simd-0.1.0 (c (n "bitvector_simd") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 2)) (d (n "bitvec") (r "^0.22") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.4") (d #t) (k 0) (p "packed_simd_2")))) (h "0zdkjb1ixx042anrqad640xzm5rw5nzi04i37hshp78jv37mi491")))

(define-public crate-bitvector_simd-0.1.1 (c (n "bitvector_simd") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 2)) (d (n "bitvec") (r "^0.22") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.4") (d #t) (k 0) (p "packed_simd_2")))) (h "1rnz07y3ddaxqzvl0nxbl75wlvx6zvq2yamx74dw6krib6adq9m5")))

(define-public crate-bitvector_simd-0.1.2 (c (n "bitvector_simd") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 2)) (d (n "bitvec") (r "^0.22") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.4") (d #t) (k 0) (p "packed_simd_2")))) (h "1ii4d74rlahhciw0k2b22wjn6wbqczmlsj8c55243vrrs0agg1p8")))

(define-public crate-bitvector_simd-0.2.0 (c (n "bitvector_simd") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 2)) (d (n "bitvec") (r "^0.22") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.4") (d #t) (k 0) (p "packed_simd_2")))) (h "05px0aia8lz8zxv08m3bwsh909lvxdcynrkb1sm7bjnlpagiwkkw")))

(define-public crate-bitvector_simd-0.2.1 (c (n "bitvector_simd") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 2)) (d (n "bitvec") (r "^0.22") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.4") (d #t) (k 0) (p "packed_simd_2")))) (h "1clprlf2l79kbkmimx07k1ffli565smf0rmyz37bhmn12fg91cr9")))

(define-public crate-bitvector_simd-0.2.2 (c (n "bitvector_simd") (v "0.2.2") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 2)) (d (n "bitvec") (r "^0.22") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.6") (d #t) (k 0) (p "packed_simd_2")))) (h "04n8z0443s8bwlryx57mvkf77ai4ny0c166cvggc7f48db7684rr")))

(define-public crate-bitvector_simd-0.2.3 (c (n "bitvector_simd") (v "0.2.3") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 2)) (d (n "bitvec") (r "^0.22") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.7") (d #t) (k 0) (p "packed_simd_2")))) (h "03kvx55j9s3r6fn7mfyy1hsbxpgs7i8gd8d76mmcj6zjs54d4bay")))

