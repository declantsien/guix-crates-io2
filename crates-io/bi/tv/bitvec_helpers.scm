(define-module (crates-io bi tv bitvec_helpers) #:use-module (crates-io))

(define-public crate-bitvec_helpers-0.1.0 (c (n "bitvec_helpers") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.20.1") (d #t) (k 0)))) (h "0nqc7s1xlyfa5x45mj96gjxmccj45w7qfarpawb21iw86d6h9j3j")))

(define-public crate-bitvec_helpers-0.1.1 (c (n "bitvec_helpers") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)))) (h "15dd8cb92qqgpir1liv1dh3mpj4mh36d2p2asgmmq8pqj1xs0bcj")))

(define-public crate-bitvec_helpers-1.0.0 (c (n "bitvec_helpers") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)))) (h "086895cyb9kgjl0z2pzq1z4hqj088s2jypxd0lg27sajij36k017")))

(define-public crate-bitvec_helpers-1.0.1 (c (n "bitvec_helpers") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)))) (h "0b2ncxbzp4lwlnnqpczh4w31nvlcm09x4zsqsv0y1k074jh5i4xv") (r "1.56.0")))

(define-public crate-bitvec_helpers-1.0.2 (c (n "bitvec_helpers") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)))) (h "0gpyxg4xiffrps7rpadrfxk4wdqqxl09spnxh01igspxhl6x79qv") (r "1.56.0")))

(define-public crate-bitvec_helpers-2.0.0 (c (n "bitvec_helpers") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)))) (h "0bm7dagsw8xczy2k1xsjs9hi2lnl2myc8xlyc0d5ac4kf743fdrg") (r "1.56.1")))

(define-public crate-bitvec_helpers-2.0.1 (c (n "bitvec_helpers") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)))) (h "1x4vpi00hbasqppih6b0hqffvys19vdn3czqr25r58if02an1ngr") (r "1.56.1")))

(define-public crate-bitvec_helpers-3.0.0 (c (n "bitvec_helpers") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (d #t) (k 0)))) (h "1z55qgxggb3xhdi2s87p56f6p1a5qxgblzll5mv79q5iwqgzw6r0") (r "1.56.1")))

(define-public crate-bitvec_helpers-3.1.0 (c (n "bitvec_helpers") (v "3.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "094w9qn01ildbfg4wm92cqqzvaraq6sc48b6ii1669njzpyyglr6") (f (quote (("default" "bitvec")))) (s 2) (e (quote (("bitvec" "dep:bitvec" "dep:funty" "dep:anyhow") ("bitstream-io" "dep:bitstream-io")))) (r "1.56.1")))

(define-public crate-bitvec_helpers-3.1.1 (c (n "bitvec_helpers") (v "3.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "0agjkwc3gafpwkrjc0nsigcn6n3m1ac7hl698byq250gi080kz5m") (f (quote (("default" "bitvec")))) (s 2) (e (quote (("bitvec" "dep:bitvec" "dep:funty" "dep:anyhow") ("bitstream-io" "dep:bitstream-io")))) (r "1.56.1")))

(define-public crate-bitvec_helpers-3.1.2 (c (n "bitvec_helpers") (v "3.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "0jpvkq386k283mj6xgp77c3c9rldcahy77g1dfsi4hbbv0xqixiy") (f (quote (("default" "bitvec")))) (s 2) (e (quote (("bitvec" "dep:bitvec" "dep:funty" "dep:anyhow") ("bitstream-io" "dep:bitstream-io")))) (r "1.56.1")))

(define-public crate-bitvec_helpers-3.1.3 (c (n "bitvec_helpers") (v "3.1.3") (d (list (d (n "anyhow") (r "^1.0.77") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "1wj6daxs3mw6r4dyc0c44s50sx1bmp4ff6kxvb627ml30m3gcb9f") (f (quote (("default" "bitvec")))) (s 2) (e (quote (("bitvec" "dep:bitvec" "dep:funty" "dep:anyhow") ("bitstream-io" "dep:bitstream-io")))) (r "1.60.0")))

(define-public crate-bitvec_helpers-3.1.4 (c (n "bitvec_helpers") (v "3.1.4") (d (list (d (n "anyhow") (r "^1.0.81") (o #t) (d #t) (k 0)) (d (n "bitstream-io") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "funty") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "16h9kj4p0j9bpad73dyi9j03rwm7snq0gwnpvs3bpap8044fl468") (f (quote (("default" "bitvec")))) (s 2) (e (quote (("bitvec" "dep:bitvec" "dep:funty" "dep:anyhow") ("bitstream-io" "dep:bitstream-io")))) (r "1.60.0")))

