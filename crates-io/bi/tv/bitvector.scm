(define-module (crates-io bi tv bitvector) #:use-module (crates-io))

(define-public crate-bitvector-0.1.0 (c (n "bitvector") (v "0.1.0") (h "1z9yharlg640vz8bc9nw28fb1gjp09qqk2gb5012bfs969idgzjp") (f (quote (("unstable"))))))

(define-public crate-bitvector-0.1.1 (c (n "bitvector") (v "0.1.1") (h "0dccmlxmwpbif8zm6vmiv278bcgv9v2sjm6wa2vxb7h0sybmbkz5") (f (quote (("unstable"))))))

(define-public crate-bitvector-0.1.2 (c (n "bitvector") (v "0.1.2") (h "1im0961kvynj7sx0gk998dk05wm6fx2mkm8l450m9hgd6clyi39q") (f (quote (("unstable"))))))

(define-public crate-bitvector-0.1.3 (c (n "bitvector") (v "0.1.3") (h "00y2fskgw36gvkr4g16sm5vq0vda9gq971pgip0lii7p0qz9nx5p") (f (quote (("unstable"))))))

(define-public crate-bitvector-0.1.4 (c (n "bitvector") (v "0.1.4") (h "0b153qqk4y3z61vpkld99jv0fqdzqyvl47aifm3zgn7m4cpjh36c") (f (quote (("unstable"))))))

(define-public crate-bitvector-0.1.5 (c (n "bitvector") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1jr8zi66fmd2rprsfqb18ggfq42ibyx7cnak1cy88bdis35gvrad") (f (quote (("unstable"))))))

