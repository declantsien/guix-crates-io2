(define-module (crates-io bi tv bitvavo-api) #:use-module (crates-io))

(define-public crate-bitvavo-api-0.1.0 (c (n "bitvavo-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "08fg3v62r0cxf7vnxsqp1gkvplwp1aflfz88zs0vnihbdfhnhpn8")))

(define-public crate-bitvavo-api-0.1.1 (c (n "bitvavo-api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1spfa4myjy82sxj99vzm1qi82j0vg44jvgnkgxax8kwvnhksrx3v")))

(define-public crate-bitvavo-api-0.2.0 (c (n "bitvavo-api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1ifn0yhx2077vmi1x45mhc2l80z4r08ihh4bfpkf7jsj2hnhydym")))

(define-public crate-bitvavo-api-0.3.0 (c (n "bitvavo-api") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0pkrv6i34kyd7msqqi5mps4mgibwcl4x8vb021dxmcik756smv4r")))

(define-public crate-bitvavo-api-0.4.0 (c (n "bitvavo-api") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0a13xhnz75kbs2ygc6l37qc5pcqlngl9xa00p55cmb7lcvxsmzs6")))

