(define-module (crates-io bi tv bitvecbuf) #:use-module (crates-io))

(define-public crate-bitvecbuf-0.1.0 (c (n "bitvecbuf") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)))) (h "1qxr01wfj2k3s7wdvzarjbpphba61f5v7lvjwqa0mhmflnjvfgkg") (f (quote (("nightly"))))))

(define-public crate-bitvecbuf-0.2.0 (c (n "bitvecbuf") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)))) (h "16lh42yy7x1k1868mzy2v5zqj11vgic5nmnzcdvzmy4y07c8ymzs") (f (quote (("nightly"))))))

(define-public crate-bitvecbuf-1.0.0 (c (n "bitvecbuf") (v "1.0.0") (d (list (d (n "bitvec") (r "^0.22") (d #t) (k 0)))) (h "1syfgnmm36z4w3pklrhv9n83jj458najmfd8diazswxdqvxycy2v") (f (quote (("nightly"))))))

