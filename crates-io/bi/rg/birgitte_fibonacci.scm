(define-module (crates-io bi rg birgitte_fibonacci) #:use-module (crates-io))

(define-public crate-birgitte_fibonacci-0.1.0 (c (n "birgitte_fibonacci") (v "0.1.0") (d (list (d (n "memoize") (r "^0.3.3") (d #t) (k 0)))) (h "1mfwwh4prbcrwds9d7141c6igapymfg7p0zic3fwgr5fv6cdh9cx")))

(define-public crate-birgitte_fibonacci-0.2.0 (c (n "birgitte_fibonacci") (v "0.2.0") (d (list (d (n "memoize") (r "^0.3.3") (d #t) (k 0)))) (h "0ahgw7xqc5f1l9ckk8svf3ir5sgsp6j1ag4r6gyy1v0fr7djdg3h")))

