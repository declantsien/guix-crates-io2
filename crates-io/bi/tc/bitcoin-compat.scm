(define-module (crates-io bi tc bitcoin-compat) #:use-module (crates-io))

(define-public crate-bitcoin-compat-0.1.10-alpha.0 (c (n "bitcoin-compat") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "18991nmk96lq4wls0jb8by43dh6yca1dmyc2g2cs8s26ih4mmnc6")))

(define-public crate-bitcoin-compat-0.1.12-alpha.0 (c (n "bitcoin-compat") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0p58ajk7rs20bsq744iy79752as2i38q7q97q77zhlr8vkq3af0p")))

(define-public crate-bitcoin-compat-0.1.13-alpha.0 (c (n "bitcoin-compat") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "181qfw0yikic14j82drfjqpsipwzf4vkg3m6jswnvplbpir7wss2")))

(define-public crate-bitcoin-compat-0.1.16-alpha.0 (c (n "bitcoin-compat") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0gvd33hmfl64jxr98s86svxcdzsngn4vfb5x3kd05zlc5yvapnnz")))

