(define-module (crates-io bi tc bitcoin-sha1) #:use-module (crates-io))

(define-public crate-bitcoin-sha1-0.1.12-alpha.0 (c (n "bitcoin-sha1") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0vl6g31fvp51d57fykvdr94pmhlbivr9yh1hmq6zmnk7advln7m2")))

(define-public crate-bitcoin-sha1-0.1.16-alpha.0 (c (n "bitcoin-sha1") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0vmq71qijsn23xip3s973cvma74m94vqv9cmvhyf0nm1bf10p4f5")))

