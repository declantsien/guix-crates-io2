(define-module (crates-io bi tc bitcoin-foundblock) #:use-module (crates-io))

(define-public crate-bitcoin-foundblock-0.1.12-alpha.0 (c (n "bitcoin-foundblock") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-block") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "0b2zw0b48lzx6dg8kaa35f10gyrz8gm6dxbzm46c6lr942z9cnsx")))

(define-public crate-bitcoin-foundblock-0.1.16-alpha.0 (c (n "bitcoin-foundblock") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-block") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "1qjzbhiahvdbgyznvmqdpz4x1l4z4hwh4af3p6k7v6hp8jnhgyfr")))

