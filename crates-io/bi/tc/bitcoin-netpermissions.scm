(define-module (crates-io bi tc bitcoin-netpermissions) #:use-module (crates-io))

(define-public crate-bitcoin-netpermissions-0.1.12-alpha.0 (c (n "bitcoin-netpermissions") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-service") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-string") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-subnet") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "159jsckg151ag892qb0xjn3kczvlb8p08sp2dkwipb73z1c8gzqj")))

(define-public crate-bitcoin-netpermissions-0.1.16-alpha.0 (c (n "bitcoin-netpermissions") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-service") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-string") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-subnet") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0c1cwqc1n8kwwpgd3axyglx71zq5c4b8hmm2fdalaw5qf8m6w4k7")))

