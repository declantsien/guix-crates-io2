(define-module (crates-io bi tc bitcoinwallet-context) #:use-module (crates-io))

(define-public crate-bitcoinwallet-context-0.1.12-alpha.0 (c (n "bitcoinwallet-context") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-argsman") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinchain-interface") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinwallet-interface") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "1jac8b3wxf1w0hrirdmv07ighgi64ic6kaykwqhg6fq1pgmx5m0a")))

(define-public crate-bitcoinwallet-context-0.1.16-alpha.0 (c (n "bitcoinwallet-context") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-argsman") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinchain-interface") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinwallet-interface") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "0hfm3i6axy7nicq0ypqirrxy61nlkqzl2yni687p3inl06snm2sk")))

