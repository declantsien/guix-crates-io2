(define-module (crates-io bi tc bitchomp) #:use-module (crates-io))

(define-public crate-bitchomp-0.1.0 (c (n "bitchomp") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "17vazly6v794pmrmni69p3zgdfg6w6jbrl0kghna9m0x904sngsj")))

(define-public crate-bitchomp-0.2.0 (c (n "bitchomp") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "07hsgmdwnzyk18idp8hhnfvl34ffk228lg046vzdw6hf60ijrd54")))

(define-public crate-bitchomp-0.2.1 (c (n "bitchomp") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1bnjjzb8nnaydii6x15c557mnbisy0aq7apqlrb0srpfv3808r3n")))

(define-public crate-bitchomp-0.2.2 (c (n "bitchomp") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1bvq5kkpk7jnaph2j050rmadz078s6k7rdwnv39hhh2paaigp7n5")))

