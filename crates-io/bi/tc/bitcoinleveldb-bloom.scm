(define-module (crates-io bi tc bitcoinleveldb-bloom) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-bloom-0.1.10-alpha.0 (c (n "bitcoinleveldb-bloom") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-filter") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0ska7l77qz08qv3np0rmcc7dsk57grdx7hlxn5w5cjnywdwcac37")))

(define-public crate-bitcoinleveldb-bloom-0.1.12-alpha.0 (c (n "bitcoinleveldb-bloom") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-filter") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0kx4baqaq88m03a172l9xnrq5v3ma22xk9mxigihwysgaph36w9i")))

(define-public crate-bitcoinleveldb-bloom-0.1.16-alpha.0 (c (n "bitcoinleveldb-bloom") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-filter") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "15mxal4z12y03q41i870cx2979js1d9apf3ncw6zmqmyrs9ylhyb")))

