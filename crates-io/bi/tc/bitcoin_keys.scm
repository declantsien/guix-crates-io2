(define-module (crates-io bi tc bitcoin_keys) #:use-module (crates-io))

(define-public crate-bitcoin_keys-0.1.0 (c (n "bitcoin_keys") (v "0.1.0") (d (list (d (n "secp256k1") (r "^0.24.1") (d #t) (k 0)))) (h "006652nr76kaxxkr5a7wq2r5gr0g45g0y01sy14xv0hwk525gvri") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

