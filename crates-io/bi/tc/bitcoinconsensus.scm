(define-module (crates-io bi tc bitcoinconsensus) #:use-module (crates-io))

(define-public crate-bitcoinconsensus-0.16.0 (c (n "bitcoinconsensus") (v "0.16.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "secp256k1") (r "^0.8") (d #t) (k 0)))) (h "0qzgdkmpkfwm9xy517pjkkf58pdgfdlyqsxkzqk66q6xhdfp98jh")))

(define-public crate-bitcoinconsensus-0.16.1 (c (n "bitcoinconsensus") (v "0.16.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "secp256k1") (r "^0.8") (d #t) (k 0)))) (h "0wd8yv66qwpm3jn6rx3zb5pbkb7xhvc4kskai6wh7gbw1156070k")))

(define-public crate-bitcoinconsensus-0.16.2-0.16.0 (c (n "bitcoinconsensus") (v "0.16.2-0.16.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "secp256k1") (r "^0.8") (d #t) (k 0)))) (h "19cs9w131j2x82yl7s08kya4nsgv0cpsyg3vg9m8xx7rjmcpxwa3")))

(define-public crate-bitcoinconsensus-0.16.2 (c (n "bitcoinconsensus") (v "0.16.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "secp256k1") (r "^0.8") (d #t) (k 0)))) (h "1xfdib9v14fidvwn0gkmdysgzpyhi79388rfwc4x2pyl6kss0blq")))

(define-public crate-bitcoinconsensus-0.16.3 (c (n "bitcoinconsensus") (v "0.16.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "secp256k1") (r "^0.9") (d #t) (k 0)))) (h "1y70d6w3p77yig71hzwif5x2fpbjbsbnqb63nppkxa7np8vnp25k")))

(define-public crate-bitcoinconsensus-0.16.4 (c (n "bitcoinconsensus") (v "0.16.4") (d (list (d (n "cc") (r "= 1.0.26") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "secp256k1") (r "^0.12") (d #t) (k 2)))) (h "18xq8vy8fnpd0pblmbxfgmi3xfqkr26nmjdhq8c0b21qpm6hvry9")))

(define-public crate-bitcoinconsensus-0.17.0 (c (n "bitcoinconsensus") (v "0.17.0") (d (list (d (n "cc") (r ">= 1.0.36") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "secp256k1") (r "^0.15") (f (quote ("recovery"))) (d #t) (k 2)))) (h "0ilvlbi54cfam1ai4qihl78im6nxip7jcyx03lqny0zavm4kwlm6") (y #t)))

(define-public crate-bitcoinconsensus-0.17.1 (c (n "bitcoinconsensus") (v "0.17.1") (d (list (d (n "cc") (r ">= 1.0.36") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "secp256k1") (r "^0.15") (f (quote ("recovery"))) (d #t) (k 0)))) (h "16gzjdnqv61kw0a263w8ynp4pzwsqfz96pkd56k5mwxxh9i9k24r")))

(define-public crate-bitcoinconsensus-0.19.0-1 (c (n "bitcoinconsensus") (v "0.19.0-1") (d (list (d (n "cc") (r ">= 1.0.36, <= 1.0.41") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0pfwkn9pfhdfv0sfl3q5rv0163jsj88rnqvirr7az9qk9s09h49h") (f (quote (("external-secp"))))))

(define-public crate-bitcoinconsensus-0.19.0-2 (c (n "bitcoinconsensus") (v "0.19.0-2") (d (list (d (n "cc") (r ">=1.0.36, <=1.0.41") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "03n97y5qm5q1hpsc6mds4md03wk4l0vyg5zpg9hcnzv3rm4vlwzs") (f (quote (("external-secp"))))))

(define-public crate-bitcoinconsensus-0.19.0-3 (c (n "bitcoinconsensus") (v "0.19.0-3") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1dd1wdgjz7v4pq3i9pblvl7r3f3pks0sza96n5n8abyhbhxs92hs") (f (quote (("external-secp"))))))

(define-public crate-bitcoinconsensus-0.19.0-0.4.0 (c (n "bitcoinconsensus") (v "0.19.0-0.4.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1gkx8h2kb3vnm6cjs9lhmavv5mrfr4np13vqprg7c7kmdb9p6ilc") (f (quote (("std") ("external-secp") ("default" "std"))))))

(define-public crate-bitcoinconsensus-0.19.2-0.4.1 (c (n "bitcoinconsensus") (v "0.19.2-0.4.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "04rnc3gbhr4l61vya2258rlz50mfaj35hyhhg3fwals9i8wgwwd1") (f (quote (("std") ("external-secp") ("default" "std"))))))

(define-public crate-bitcoinconsensus-0.20.2-0.5.0 (c (n "bitcoinconsensus") (v "0.20.2-0.5.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "173g61ibgv808vpl7i8vifn25nz9pafb79wi4hds42kynxc5al2l") (f (quote (("std") ("external-secp") ("default" "std"))))))

(define-public crate-bitcoinconsensus-0.20.2-0.6.0 (c (n "bitcoinconsensus") (v "0.20.2-0.6.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1b8haqdaazlan1ql541b7ss8zy4pgmmfdqygd3f195mzzvn5i4n9") (f (quote (("std") ("external-secp") ("default" "std")))) (y #t)))

(define-public crate-bitcoinconsensus-0.20.2-0.6.1 (c (n "bitcoinconsensus") (v "0.20.2-0.6.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0yac43qdb8f48lcw8r03l28g06hq0y5hr3b6wdw622k29099x2xq") (f (quote (("std") ("external-secp") ("default" "std")))) (y #t)))

(define-public crate-bitcoinconsensus-0.100.0+0.20.2 (c (n "bitcoinconsensus") (v "0.100.0+0.20.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0kzmf5351cpwv60g8m0vchi6vhh31cickzqdyjq4i51sml661vfr") (f (quote (("std") ("external-secp") ("default" "std"))))))

(define-public crate-bitcoinconsensus-0.101.0+0.21.2 (c (n "bitcoinconsensus") (v "0.101.0+0.21.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1zmgin7cjfz2hzamcnajgih63q3jjby3ahzwv21g91841f3h4l31") (f (quote (("std") ("external-secp") ("default" "std"))))))

(define-public crate-bitcoinconsensus-0.101.1+0.21-final (c (n "bitcoinconsensus") (v "0.101.1+0.21-final") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0ij9aq0yyin3gmva6rlql392pcw4hc5l253d9244nr7ck8i5wcp0") (f (quote (("std") ("external-secp") ("default" "std"))))))

(define-public crate-bitcoinconsensus-0.102.0+22.1 (c (n "bitcoinconsensus") (v "0.102.0+22.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1qir87n5rbxhxzrr6sabd0zhwjl8726vv50k46csxfa7ylvrsc20") (f (quote (("std") ("external-secp") ("default" "std"))))))

(define-public crate-bitcoinconsensus-0.103.0+23.2 (c (n "bitcoinconsensus") (v "0.103.0+23.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "07g4clcjzdqafwfpc67nsafqdjqdmjb58wy672vm94a09jh5dqah") (f (quote (("std") ("external-secp") ("default" "std"))))))

(define-public crate-bitcoinconsensus-0.104.0+24.2 (c (n "bitcoinconsensus") (v "0.104.0+24.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0fz31r7v2mndljdsyiqxqmpska7id2bg9svl2vxsjq0r74nvxs08") (f (quote (("std") ("external-secp") ("default" "std"))))))

(define-public crate-bitcoinconsensus-0.105.0+25.1 (c (n "bitcoinconsensus") (v "0.105.0+25.1") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "064yih4pj0gdjn8cpjrdal9cbz20r5qx63pw2f8348f6na7sqq7j") (f (quote (("std") ("external-secp") ("default" "std"))))))

(define-public crate-bitcoinconsensus-0.106.0+26.0 (c (n "bitcoinconsensus") (v "0.106.0+26.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1s8sa8zcyxdm1fjaj9xa705npv05dpgvj1wfjvdcshshrsfblb71") (f (quote (("std") ("external-secp") ("default" "std"))))))

