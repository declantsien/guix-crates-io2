(define-module (crates-io bi tc bitconch-jsonrpc-tcp-server) #:use-module (crates-io))

(define-public crate-bitconch-jsonrpc-tcp-server-0.1.0 (c (n "bitconch-jsonrpc-tcp-server") (v "0.1.0") (d (list (d (n "bitconch-jsonrpc-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bitconch-jsonrpc-server-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "1fm04splvcx4zp73rybd08l8filnx0npyck9hxp157r99qblxk75")))

