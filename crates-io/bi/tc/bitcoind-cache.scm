(define-module (crates-io bi tc bitcoind-cache) #:use-module (crates-io))

(define-public crate-bitcoind-cache-0.1.1 (c (n "bitcoind-cache") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.29") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "rust-s3") (r "^0.32") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "0q2hi4vx5cypqdwnnavflybgmk5cr4g0dnj8z4mcpxiws8hw5p2q") (f (quote (("r2" "rust-s3") ("http" "reqwest") ("fs" "tokio") ("default" "r2" "http" "fs"))))))

