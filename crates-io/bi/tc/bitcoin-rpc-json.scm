(define-module (crates-io bi tc bitcoin-rpc-json) #:use-module (crates-io))

(define-public crate-bitcoin-rpc-json-0.1.0 (c (n "bitcoin-rpc-json") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "strason") (r "^0.4") (d #t) (k 0)))) (h "1vgqr04a7s47583dfwjhlr0drb6qfzsdxi0s8rgk97pkmnrghq3r")))

(define-public crate-bitcoin-rpc-json-0.2.0 (c (n "bitcoin-rpc-json") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "strason") (r "^0.4") (d #t) (k 0)))) (h "016w30by1gknp4msrzliy7m8zj4bngqdbzs2529b92w9jgp6qllk")))

