(define-module (crates-io bi tc bitcoin-bech32m) #:use-module (crates-io))

(define-public crate-bitcoin-bech32m-0.1.12-alpha.0 (c (n "bitcoin-bech32m") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1ycgs24l3lnyrfgfmhf6z34isx2s6gsrrr606zk9hkbjn41kw7yg")))

(define-public crate-bitcoin-bech32m-0.1.16-alpha.0 (c (n "bitcoin-bech32m") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0qf9lzvqy8vqwzl88zfswkaxh8kif40np2mygbzg3z0rrx92y5j9")))

