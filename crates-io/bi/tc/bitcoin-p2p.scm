(define-module (crates-io bi tc bitcoin-p2p) #:use-module (crates-io))

(define-public crate-bitcoin-p2p-0.1.0 (c (n "bitcoin-p2p") (v "0.1.0") (h "1vd0fim17f1w1wgxfcpmjkk3ah1m5nvpvk3r14n2azhijbg8jc3f") (y #t)))

(define-public crate-bitcoin-p2p-0.0.1 (c (n "bitcoin-p2p") (v "0.0.1") (d (list (d (n "bitcoin") (r "^0.25.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "lru") (r "^0.4.1") (d #t) (k 0)) (d (n "mio") (r "^0.7.5") (f (quote ("tcp" "os-poll"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)))) (h "0gp5ygf2w6f802zza4w8gywmj1jfq1m5ynvvw2ki2zbzmknl3k8a")))

(define-public crate-bitcoin-p2p-0.0.2 (c (n "bitcoin-p2p") (v "0.0.2") (d (list (d (n "bitcoin") (r ">=0.25.0, <0.26.0") (d #t) (k 0)) (d (n "chrono") (r ">=0.4.9, <0.5.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r ">=0.4.0, <0.5.0") (o #t) (d #t) (k 0)) (d (n "fern") (r ">=0.5.0, <0.6.0") (d #t) (k 2)) (d (n "log") (r ">=0.4.8, <0.5.0") (d #t) (k 0)) (d (n "lru") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "mio") (r ">=0.7.5, <0.8.0") (f (quote ("tcp" "os-poll"))) (d #t) (k 0)) (d (n "rand") (r ">=0.7.2, <0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "1kml1anfgajzn38lvb9fn6ij7b58gpm1wyhpxwi502x9g29dikpg")))

