(define-module (crates-io bi tc bitcoin-hdchain) #:use-module (crates-io))

(define-public crate-bitcoin-hdchain-0.1.12-alpha.0 (c (n "bitcoin-hdchain") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-key") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "1n2ds8hmrdxcg2k5cmwcfm00yrkprj68l4zqsvjy03xbpj01fvjx")))

(define-public crate-bitcoin-hdchain-0.1.16-alpha.0 (c (n "bitcoin-hdchain") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-key") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "1wmsdk15g05amfgi6djk2r6f2zsmaawg19r9wkz5lxbqz6x980fx")))

