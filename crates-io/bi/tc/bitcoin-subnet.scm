(define-module (crates-io bi tc bitcoin-subnet) #:use-module (crates-io))

(define-public crate-bitcoin-subnet-0.1.12-alpha.0 (c (n "bitcoin-subnet") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-network") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1f1c4930vz4dm5p4nzmiagrl0ys6ka3v3ra7khzgpd2q2jspkq1n")))

(define-public crate-bitcoin-subnet-0.1.16-alpha.0 (c (n "bitcoin-subnet") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-network") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1gsynl6plmwrax4p0ckhisgsa0h5npp6p4rsxhs7f08p6pisbnax")))

