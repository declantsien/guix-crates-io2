(define-module (crates-io bi tc bitcoin_client) #:use-module (crates-io))

(define-public crate-bitcoin_client-0.0.1 (c (n "bitcoin_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "bitcoincore-rpc") (r "^0.9.1") (d #t) (k 0)) (d (n "bitcoincore-rpc-json") (r "^0.9.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.43") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "12a9dbxf5a66y8awfvfp72d0pmpr98w8c3iwbp3rm89n7ah1xjkp")))

(define-public crate-bitcoin_client-0.0.2 (c (n "bitcoin_client") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "bitcoincore-rpc") (r "^0.9.1") (d #t) (k 0)) (d (n "bitcoincore-rpc-json") (r "^0.9.1") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.43") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "15fvrq7byq7bl7raznir3nk1fbfxqn2pmcs1yn4zsr1mqgq45skd")))

