(define-module (crates-io bi tc bitcoinsecp256k1-parse) #:use-module (crates-io))

(define-public crate-bitcoinsecp256k1-parse-0.1.12-alpha.0 (c (n "bitcoinsecp256k1-parse") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-secp256k1") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-ec") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0j09v7yaijix5prqbl1rf5zwsrmsjmz1krbdzvgxcfkri395zk6q")))

(define-public crate-bitcoinsecp256k1-parse-0.1.16-alpha.0 (c (n "bitcoinsecp256k1-parse") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-secp256k1") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-ec") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0n3jss33lk0i51xgbf7qbxand8fshqamzwhdsjliixnz3hxsw6q4")))

