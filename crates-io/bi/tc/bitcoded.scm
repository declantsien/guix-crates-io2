(define-module (crates-io bi tc bitcoded) #:use-module (crates-io))

(define-public crate-bitcoded-0.1.1 (c (n "bitcoded") (v "0.1.1") (h "1r10s509c2bf5kgsmy3qb7lfflqgh67rlkn5889fn0ypsz23kz8h")))

(define-public crate-bitcoded-0.1.2 (c (n "bitcoded") (v "0.1.2") (h "1778z093gr72njjwdhcw54ml9zbig0p0prwam3jf8k4fxr016hcv")))

(define-public crate-bitcoded-0.1.3 (c (n "bitcoded") (v "0.1.3") (h "1a202jbm4nc5gbqywmacji81nb7fi35sfprg18l3acgk7q4ncvhp")))

