(define-module (crates-io bi tc bitcoin-service-flags) #:use-module (crates-io))

(define-public crate-bitcoin-service-flags-0.1.10-alpha.0 (c (n "bitcoin-service-flags") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0yn502r04zllxdc3cfs0zzyqzb11k3vgai20yzxlj9hsfr6a8kgd")))

(define-public crate-bitcoin-service-flags-0.1.12-alpha.0 (c (n "bitcoin-service-flags") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0pgrq5d4l7pg9pf5ffbpdx1x69k97rg6f3izlrg77jm99iwx17wc")))

(define-public crate-bitcoin-service-flags-0.1.13-alpha.0 (c (n "bitcoin-service-flags") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0cdqwqglnjyyvijsryb59dc4j318xzwdd4if9z1ari6wwi4zg99b")))

(define-public crate-bitcoin-service-flags-0.1.16-alpha.0 (c (n "bitcoin-service-flags") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0mmn0nkbxqiphnsvar3ny2zhxv808dj0nl6sf7rnhv37n458aa4s")))

