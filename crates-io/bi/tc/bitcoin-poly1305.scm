(define-module (crates-io bi tc bitcoin-poly1305) #:use-module (crates-io))

(define-public crate-bitcoin-poly1305-0.1.12-alpha.0 (c (n "bitcoin-poly1305") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0ii0g0gl2ch0phkp84l0s05pjz0y68cppm1cpswvlk7646d3nwc1")))

(define-public crate-bitcoin-poly1305-0.1.16-alpha.0 (c (n "bitcoin-poly1305") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "12aymxinqrjxwgl5pck73l4067spv0vs4ap5gaadvlg3zipsayby")))

