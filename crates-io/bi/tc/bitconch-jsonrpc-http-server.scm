(define-module (crates-io bi tc bitconch-jsonrpc-http-server) #:use-module (crates-io))

(define-public crate-bitconch-jsonrpc-http-server-0.1.0 (c (n "bitconch-jsonrpc-http-server") (v "0.1.0") (d (list (d (n "bitconch-jsonrpc-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bitconch-jsonrpc-server-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "1vnhvphlz2i2i4058avys2b49ssq7rbcf66ff8p6bdry5lgcs5g4")))

