(define-module (crates-io bi tc bitcoin-muhash) #:use-module (crates-io))

(define-public crate-bitcoin-muhash-0.1.12-alpha.0 (c (n "bitcoin-muhash") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0adfyjrgy0lg90f6m4jsf8s2i6k17hi73w5dg3k4kxwn0x9sysa2")))

(define-public crate-bitcoin-muhash-0.1.16-alpha.0 (c (n "bitcoin-muhash") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "05l8835qivsd099vmhqxh5qwl92x90wzbfb55rdbz3818zsxb3kz")))

