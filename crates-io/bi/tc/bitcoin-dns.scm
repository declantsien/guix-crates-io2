(define-module (crates-io bi tc bitcoin-dns) #:use-module (crates-io))

(define-public crate-bitcoin-dns-0.1.12-alpha.0 (c (n "bitcoin-dns") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-network") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-proxy") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-service") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "03d87p5921zldv9wdldn2wgfllznnvwaiq9sdqxr1iyvapivnlpq")))

(define-public crate-bitcoin-dns-0.1.16-alpha.0 (c (n "bitcoin-dns") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-network") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-proxy") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-service") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1k3k7pa0g1vb90lir9ybd4asyjfjrq45syfx2a80fdzrk54pm6rq")))

