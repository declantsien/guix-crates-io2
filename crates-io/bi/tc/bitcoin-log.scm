(define-module (crates-io bi tc bitcoin-log) #:use-module (crates-io))

(define-public crate-bitcoin-log-0.1.10-alpha.0 (c (n "bitcoin-log") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0pcjvp2m1dblxsp8b72889dfi6q4rix2xyjx5qsgkbc91gf40blx")))

(define-public crate-bitcoin-log-0.1.12-alpha.0 (c (n "bitcoin-log") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1yi40hcjfs9i2b6bfi1nj3pwxywg5hyfbskszpqyfz6kz3kv6df0")))

(define-public crate-bitcoin-log-0.1.13-alpha.0 (c (n "bitcoin-log") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "00zqarz58ql9ifpqpl18jf684547vdxba6qwz02cbqd0x5a160w2")))

(define-public crate-bitcoin-log-0.1.16-alpha.0 (c (n "bitcoin-log") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1cd447gjsikvwvhvjd6j80cl2pvka69xhsvi87gqjhw5mnhyfnsq")))

