(define-module (crates-io bi tc bitcoin-zmq) #:use-module (crates-io))

(define-public crate-bitcoin-zmq-0.1.0 (c (n "bitcoin-zmq") (v "0.1.0") (d (list (d (n "bus_queue") (r "^0.4.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "futures-zmq") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "tokio") (r "^0.1.22") (d #t) (k 2)) (d (n "zmq") (r "^0.9.1") (d #t) (k 0)))) (h "1vc5i5shp1xnfrpz8pvcj1ba07gsdyj88wgxr3a8mwg25ikq77g8")))

(define-public crate-bitcoin-zmq-0.1.1 (c (n "bitcoin-zmq") (v "0.1.1") (d (list (d (n "bus_queue") (r "^0.4.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "futures-zmq") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "tokio") (r "^0.1.22") (d #t) (k 2)) (d (n "zmq") (r "^0.9.1") (d #t) (k 0)))) (h "1xq0dyw55jjyyra19cir3xk6njc29s6z6hzw5d5h0k1p04961ip6")))

(define-public crate-bitcoin-zmq-0.1.2 (c (n "bitcoin-zmq") (v "0.1.2") (d (list (d (n "bus_queue") (r "^0.4.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "futures-zmq") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "tokio") (r "^0.1.22") (d #t) (k 2)) (d (n "zmq") (r "^0.9.1") (d #t) (k 0)))) (h "07a7s7w77jh3c9176hd81fcpm6v1q7cwf780a10lrlspk6x2vm88")))

(define-public crate-bitcoin-zmq-0.1.3 (c (n "bitcoin-zmq") (v "0.1.3") (d (list (d (n "bus_queue") (r "^0.4.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "futures-zmq") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "tokio") (r "^0.1.22") (d #t) (k 2)) (d (n "zmq") (r "^0.9.1") (d #t) (k 0)))) (h "1fg523l16sbf96rhgnybfjmjgzzvpma741kppjj8cbgyibp7b31j")))

(define-public crate-bitcoin-zmq-0.2.0-alpha.0 (c (n "bitcoin-zmq") (v "0.2.0-alpha.0") (d (list (d (n "futures-preview") (r "= 0.3.0-alpha.19") (f (quote ("compat"))) (d #t) (k 0)) (d (n "futures-zmq") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)) (d (n "zmq") (r "^0.9.1") (d #t) (k 0)))) (h "1zd2xi8xxd8lyzraf5m8lrn03wkdc5w1m9i1csp57q3nz11v5fgi")))

(define-public crate-bitcoin-zmq-0.2.0-alpha.1 (c (n "bitcoin-zmq") (v "0.2.0-alpha.1") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("compat"))) (d #t) (k 0)) (d (n "futures-zmq") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)) (d (n "zmq") (r "^0.9.1") (d #t) (k 0)))) (h "0jl3ingvp8b69rsgy6rmah1319qaf5bvf1063aj1rqj0lnzwyvzq")))

