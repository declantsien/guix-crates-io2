(define-module (crates-io bi tc bitcoin-private) #:use-module (crates-io))

(define-public crate-bitcoin-private-0.0.0 (c (n "bitcoin-private") (v "0.0.0") (h "0kc0dc6yfbv7i6gkylb1g11gmvvn9h23blip2k81kjjcfq0a9lnd") (f (quote (("std" "alloc") ("default") ("alloc"))))))

(define-public crate-bitcoin-private-0.1.0 (c (n "bitcoin-private") (v "0.1.0") (h "0mvskwi8hndf1p4d875qli4sfzmbhw1ms5jyqa7g750n05vh2abk") (f (quote (("std" "alloc") ("default") ("alloc"))))))

