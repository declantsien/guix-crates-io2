(define-module (crates-io bi tc bitcoinleveldb-crc32) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-crc32-0.1.10-alpha.0 (c (n "bitcoinleveldb-crc32") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1h7aqvl1yrhrlihhbk61kp3wbnvb777k6wf050lap2sqx43qvs4k")))

(define-public crate-bitcoinleveldb-crc32-0.1.12-alpha.0 (c (n "bitcoinleveldb-crc32") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1fdprgl3ax9liij3hfvq338iwf7w8y8v737374p1dss8vwasf2nr")))

(define-public crate-bitcoinleveldb-crc32-0.1.16-alpha.0 (c (n "bitcoinleveldb-crc32") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1gl4mfxr6ajszhmmg1zkn5ywmjik86k246vbrfv7v2jmz5rn9cc3")))

