(define-module (crates-io bi tc bitcode_derive) #:use-module (crates-io))

(define-public crate-bitcode_derive-0.1.0 (c (n "bitcode_derive") (v "0.1.0") (d (list (d (n "packagemerge") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18c4c6bbbq1nawnksl7h3zn6bxyvnhx0ji55sl7v09k6746xww8z")))

(define-public crate-bitcode_derive-0.1.1 (c (n "bitcode_derive") (v "0.1.1") (d (list (d (n "packagemerge") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0h91rhx1zcv3wgva9x49js6r2gfvf7dij3213mcams2a17k3sxsb")))

(define-public crate-bitcode_derive-0.1.2 (c (n "bitcode_derive") (v "0.1.2") (d (list (d (n "packagemerge") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bj1dkr4dfi1iin2nfvq3sgx43knyi226ibwjipm02v59h1rvp71")))

(define-public crate-bitcode_derive-0.1.3 (c (n "bitcode_derive") (v "0.1.3") (d (list (d (n "packagemerge") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04b963r3f9shjm383p747avbpmkh2jziwjsa4k5v4884711icm3d")))

(define-public crate-bitcode_derive-0.1.4 (c (n "bitcode_derive") (v "0.1.4") (d (list (d (n "packagemerge") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0glk5lh9rf6fwfm8sr85ym1pa7g460n1wdki60y6ybsfd0g4gax2")))

(define-public crate-bitcode_derive-0.5.0 (c (n "bitcode_derive") (v "0.5.0") (d (list (d (n "packagemerge") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12mk1i2a461pav6c296ngym9lif07g71dl43j14hgy6bjg1gy9ac")))

(define-public crate-bitcode_derive-0.6.0-alpha.1 (c (n "bitcode_derive") (v "0.6.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0qx79bd52kxa14p3026zslxwp3547kyp39570m4zikklcpmans5s")))

(define-public crate-bitcode_derive-0.6.0-beta.1 (c (n "bitcode_derive") (v "0.6.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "04miszxhbbfvn3dclr6i2xzr568v0cxppj411d8f53pjbplx8mv5")))

(define-public crate-bitcode_derive-0.6.0 (c (n "bitcode_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1k010wh0gazxiqb75v0qv2cplrichi42s7p9mbi9xmda35d7ari9")))

