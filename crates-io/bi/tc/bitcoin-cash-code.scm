(define-module (crates-io bi tc bitcoin-cash-code) #:use-module (crates-io))

(define-public crate-bitcoin-cash-code-1.0.0-beta.0 (c (n "bitcoin-cash-code") (v "1.0.0-beta.0") (d (list (d (n "bitcoin-cash") (r "^0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m2rz07jf78sy850a2kldh8prn7fcv8xmgl6lwz2mzrpm92b08k4")))

