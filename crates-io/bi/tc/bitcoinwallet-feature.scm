(define-module (crates-io bi tc bitcoinwallet-feature) #:use-module (crates-io))

(define-public crate-bitcoinwallet-feature-0.1.12-alpha.0 (c (n "bitcoinwallet-feature") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-signingprovider") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "0wpdqlnnwvkmks835706wjls00cv4zi15hjib6dh85qwjki6lzqs")))

(define-public crate-bitcoinwallet-feature-0.1.16-alpha.0 (c (n "bitcoinwallet-feature") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-signingprovider") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "1qch1yq7k4180pamw6l4f7ammrzgndajb7lzln16xxiwzfygi98k")))

