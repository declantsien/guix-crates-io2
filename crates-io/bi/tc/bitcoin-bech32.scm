(define-module (crates-io bi tc bitcoin-bech32) #:use-module (crates-io))

(define-public crate-bitcoin-bech32-0.1.0 (c (n "bitcoin-bech32") (v "0.1.0") (d (list (d (n "bech32") (r "^0.2.1") (d #t) (k 0)))) (h "0ra5hf28h7hs5pv726i6qhr5d9yc22r0pvf23g7lvr10x9kpy67h")))

(define-public crate-bitcoin-bech32-0.2.0 (c (n "bitcoin-bech32") (v "0.2.0") (d (list (d (n "bech32") (r "^0.2.1") (d #t) (k 0)))) (h "10khs87xjr3hwb901ndxwwiwm68vlndv2zbravcfwin5xm8bn7h4")))

(define-public crate-bitcoin-bech32-0.3.0 (c (n "bitcoin-bech32") (v "0.3.0") (d (list (d (n "bech32") (r "^0.2.1") (d #t) (k 0)))) (h "0mf7hk4yarbxni81chr5z7c96w5iplm2sx222q41gg9cpdqzrj79")))

(define-public crate-bitcoin-bech32-0.3.1 (c (n "bitcoin-bech32") (v "0.3.1") (d (list (d (n "bech32") (r "^0.2.2") (d #t) (k 0)))) (h "0g2nq1a32l7ffqpi70gkd2kqfifnsflm1v28hdlbqk8vcxjyyhdn")))

(define-public crate-bitcoin-bech32-0.4.0 (c (n "bitcoin-bech32") (v "0.4.0") (d (list (d (n "bech32") (r "^0.3.0") (d #t) (k 0)))) (h "1yizxrykyfwwhg65kbnfh7akinyqrd2h2j92k3r4xniqgj4m8px8")))

(define-public crate-bitcoin-bech32-0.5.0 (c (n "bitcoin-bech32") (v "0.5.0") (d (list (d (n "bech32") (r "^0.3.0") (d #t) (k 0)))) (h "06yywgh976yhahj46ahdr6r0l1fzg3q535dl07b81fnv91cvqghm")))

(define-public crate-bitcoin-bech32-0.5.1 (c (n "bitcoin-bech32") (v "0.5.1") (d (list (d (n "bech32") (r "^0.3.0") (d #t) (k 0)))) (h "0b5kz5g06gv6apkv7mzpmp23q9h8jp74fdprrd6xdm7pwxl5vnn9")))

(define-public crate-bitcoin-bech32-0.6.0 (c (n "bitcoin-bech32") (v "0.6.0") (d (list (d (n "bech32") (r "^0.4.0") (d #t) (k 0)))) (h "1sq59ka6h1panljx649mxiplcjxcg3sjvyj26isqn165rw71q294")))

(define-public crate-bitcoin-bech32-0.6.1 (c (n "bitcoin-bech32") (v "0.6.1") (d (list (d (n "bech32") (r "^0.4.1") (d #t) (k 0)))) (h "0p7s73m7ydllxw3al41f1v2hn030444gkiqb4a0gsmy9jj8mdsm2")))

(define-public crate-bitcoin-bech32-0.7.0 (c (n "bitcoin-bech32") (v "0.7.0") (d (list (d (n "bech32") (r "^0.4.1") (d #t) (k 0)))) (h "018i6mqimi45skj75wy8gx40daigbx824b59f3hbvxiwr1wifydm")))

(define-public crate-bitcoin-bech32-0.8.0 (c (n "bitcoin-bech32") (v "0.8.0") (d (list (d (n "bech32") (r "^0.5.0") (d #t) (k 0)))) (h "0vh10xgndn1kipl6fj91asarbvf3dvj760l27b5visv81p2a88mb")))

(define-public crate-bitcoin-bech32-0.8.1 (c (n "bitcoin-bech32") (v "0.8.1") (d (list (d (n "bech32") (r "^0.5.0") (d #t) (k 0)))) (h "0k2bjw2wfkicbx3mk6wmszmyz3r8jnxara2fdnrl0l6bmgjwz9gh")))

(define-public crate-bitcoin-bech32-0.9.0 (c (n "bitcoin-bech32") (v "0.9.0") (d (list (d (n "bech32") (r "^0.6") (d #t) (k 0)))) (h "0m15kcp655gbhdp52j5hf9zqfy395addpavcbqa12f36zk6fhrsf")))

(define-public crate-bitcoin-bech32-0.10.0 (c (n "bitcoin-bech32") (v "0.10.0") (d (list (d (n "bech32") (r "^0.6") (d #t) (k 0)))) (h "1rf4xyhn671m6rv6p6fs3x24c0rzj4x8rg0g4fqzmj927p9hnca0")))

(define-public crate-bitcoin-bech32-0.11.0 (c (n "bitcoin-bech32") (v "0.11.0") (d (list (d (n "bech32") (r "^0.7.1") (d #t) (k 0)))) (h "0h4cfvf3cpslh9hhsagfk4bna3v2z0yb0fcmfx7ns2lyfgi28dpm") (f (quote (("strict"))))))

(define-public crate-bitcoin-bech32-0.12.0 (c (n "bitcoin-bech32") (v "0.12.0") (d (list (d (n "bech32") (r "^0.8.0") (d #t) (k 0)))) (h "0kmmb53az7kh28dxkk6f34wlan0x93ym2zix7a5c40nfx467kqck") (f (quote (("strict"))))))

(define-public crate-bitcoin-bech32-0.12.1 (c (n "bitcoin-bech32") (v "0.12.1") (d (list (d (n "bech32") (r "^0.8.0") (d #t) (k 0)))) (h "0mys6lzjl1dbg3pd6dz67nfmqahrqjs5ghjwr4riy0swyg3v8kkm") (f (quote (("strict"))))))

(define-public crate-bitcoin-bech32-0.13.0 (c (n "bitcoin-bech32") (v "0.13.0") (d (list (d (n "bech32") (r "^0.9.1") (k 0)))) (h "11s0xi7bh7ydwikhg180c8iby3kyw6cpr7gs2w1mndhl2ap4lxhg") (f (quote (("strict") ("std" "bech32/std") ("default" "std"))))))

