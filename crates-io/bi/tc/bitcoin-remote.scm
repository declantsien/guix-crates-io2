(define-module (crates-io bi tc bitcoin-remote) #:use-module (crates-io))

(define-public crate-bitcoin-remote-0.1.12-alpha.0 (c (n "bitcoin-remote") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-univalue") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "17d9g9yzxh7b8rrz7mi12ni8b8kfd5ivp4gcynhvjdfm56gj1dyd")))

(define-public crate-bitcoin-remote-0.1.16-alpha.0 (c (n "bitcoin-remote") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-univalue") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "181802jdpkwawxz31jxlv5a2bdsv5wa94apc8yrn86nqqsm1av11")))

