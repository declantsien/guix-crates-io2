(define-module (crates-io bi tc bitcoin-cash-slp) #:use-module (crates-io))

(define-public crate-bitcoin-cash-slp-0.1.1 (c (n "bitcoin-cash-slp") (v "0.1.1") (d (list (d (n "bitcoin-cash") (r "^0.1.1") (d #t) (k 0)))) (h "1jvsvl2qffvhr22ja3s07j0h0sbna7xzp5lpfm0k7pjjbpf94m2y")))

(define-public crate-bitcoin-cash-slp-0.1.2 (c (n "bitcoin-cash-slp") (v "0.1.2") (d (list (d (n "bitcoin-cash") (r "^0.1.2") (d #t) (k 0)))) (h "15gmyfx52v613z4vf316k9rjfkx6ix29zf9i3as7b2hxif5n9sms")))

(define-public crate-bitcoin-cash-slp-0.1.3 (c (n "bitcoin-cash-slp") (v "0.1.3") (d (list (d (n "bitcoin-cash") (r "^0.1.3") (d #t) (k 0)))) (h "1wfd4rlir8s6lfafidba5m4mgfddrx51qba0vnz7b8k2pd7b4k88")))

(define-public crate-bitcoin-cash-slp-0.2.0 (c (n "bitcoin-cash-slp") (v "0.2.0") (d (list (d (n "bitcoin-cash") (r "^0.2.0") (d #t) (k 0)))) (h "0mkmnv1ws2wxqccdya8j9li8l60dsf3j60ca80sldmaz3lcsa60i")))

(define-public crate-bitcoin-cash-slp-0.3.0 (c (n "bitcoin-cash-slp") (v "0.3.0") (d (list (d (n "bitcoin-cash") (r "^0.2.1") (d #t) (k 0)))) (h "1pf1k772kvn4jfcswvivrkg873k5xdyyymsal2s1ha34vlhxm8jj")))

(define-public crate-bitcoin-cash-slp-0.3.1 (c (n "bitcoin-cash-slp") (v "0.3.1") (d (list (d (n "bitcoin-cash") (r "^0.2") (d #t) (k 0)))) (h "02d3zgwiwlg6jijy1rrdq339daiajkpa58r28x78bassfga3fj3l")))

(define-public crate-bitcoin-cash-slp-1.0.0-beta.0 (c (n "bitcoin-cash-slp") (v "1.0.0-beta.0") (d (list (d (n "bitcoin-cash") (r "^1.0.0-beta.0") (d #t) (k 0)))) (h "0gz6hc8y6lq9z69cyhf9vlr5zycdsk8afj5dkn8dikzb5zk7lpl9")))

