(define-module (crates-io bi tc bitcoin-validation) #:use-module (crates-io))

(define-public crate-bitcoin-validation-0.1.10-alpha.0 (c (n "bitcoin-validation") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1axddsip8lssj1hxl6c1v312kvpxv5kggchaz2pb9cf5c34g51k3")))

(define-public crate-bitcoin-validation-0.1.12-alpha.0 (c (n "bitcoin-validation") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0aalkfmhs19k0n7blw45awxp4h7w2g4zw38bwk7w6hgp15pihb5q")))

(define-public crate-bitcoin-validation-0.1.16-alpha.0 (c (n "bitcoin-validation") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1kvzfvl68d8hf18alvgjqjnzzk4bbwxr0nbdcsaxckyhkdwdjrkz")))

