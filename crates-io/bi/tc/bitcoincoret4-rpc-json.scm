(define-module (crates-io bi tc bitcoincoret4-rpc-json) #:use-module (crates-io))

(define-public crate-bitcoincoret4-rpc-json-0.17.0 (c (n "bitcoincoret4-rpc-json") (v "0.17.0") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "bitcoint4") (r "^0.30.1") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1yyph9nms274x8n6m6av0qa2l4vmbl5351rxll2q9bvmx821fvjr")))

(define-public crate-bitcoincoret4-rpc-json-0.17.1 (c (n "bitcoincoret4-rpc-json") (v "0.17.1") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "bitcoint4") (r "^0.30.1") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1pcn6npjbcj48z1w5xmbz7k5v3ixjwwz91scn1vqn7zghqb4q0sn")))

