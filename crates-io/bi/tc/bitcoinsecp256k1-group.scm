(define-module (crates-io bi tc bitcoinsecp256k1-group) #:use-module (crates-io))

(define-public crate-bitcoinsecp256k1-group-0.1.10-alpha.0 (c (n "bitcoinsecp256k1-group") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-field") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1hwb75d5m70b0sk11q2q1rn4b4jnmlg30dsgfq1zdwsc1ssdzxsh")))

(define-public crate-bitcoinsecp256k1-group-0.1.12-alpha.0 (c (n "bitcoinsecp256k1-group") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-field") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "04j3fjd4z84a08k3q6fnx17fslskc5r7zmbxqw09awzf7a4i894k")))

(define-public crate-bitcoinsecp256k1-group-0.1.13-alpha.0 (c (n "bitcoinsecp256k1-group") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-field") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0yd0617qhx10iij2dviw5wk795h8kskkpxla65i23c2af639q22f")))

(define-public crate-bitcoinsecp256k1-group-0.1.16-alpha.0 (c (n "bitcoinsecp256k1-group") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-field") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "18ji504779j7pvlfg4ny0k76ld8gjxv0jf4lw5a2p6gzq783hzhq")))

