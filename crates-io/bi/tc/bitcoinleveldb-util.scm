(define-module (crates-io bi tc bitcoinleveldb-util) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-util-0.1.10-alpha.0 (c (n "bitcoinleveldb-util") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-status") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0jzj4mylahd9j7i57pbzyrkz4d0acbpbjncjij6p3ph7q36wr159")))

(define-public crate-bitcoinleveldb-util-0.1.12-alpha.0 (c (n "bitcoinleveldb-util") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-status") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0ylasw5fn1r6rkydd0q1g7501y658m1kclljd3vncs03zhvwsic8")))

(define-public crate-bitcoinleveldb-util-0.1.13-alpha.0 (c (n "bitcoinleveldb-util") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-status") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0ksyci2cg3wkmbg7iykysk5q8y5qdmy8l09gvr98v2p5idgj6bzx")))

(define-public crate-bitcoinleveldb-util-0.1.16-alpha.0 (c (n "bitcoinleveldb-util") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-status") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "15ixr7j21ijbbl8m871wa6cmqbyaxy2hm7vhk0sjh21xvn0yxk70")))

