(define-module (crates-io bi tc bitcoinwallet-salvage) #:use-module (crates-io))

(define-public crate-bitcoinwallet-salvage-0.1.12-alpha.0 (c (n "bitcoinwallet-salvage") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-string") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "106ww6yni67f3ykpwxlcdfba7fqqn8qw00jvscqi4ag0a6852zbh")))

(define-public crate-bitcoinwallet-salvage-0.1.16-alpha.0 (c (n "bitcoinwallet-salvage") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-string") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0l37qa5fy8zaffd3d1p6gx2rw9p5ipdbvgxmb6gsawkn9i0dbwgz")))

