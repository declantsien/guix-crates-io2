(define-module (crates-io bi tc bitcoin-node-query) #:use-module (crates-io))

(define-public crate-bitcoin-node-query-0.1.0 (c (n "bitcoin-node-query") (v "0.1.0") (d (list (d (n "bitcoind-request") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)))) (h "07cmf2yc5cbh2wn43mfirci8s51gnrnnbr9b7ag6f4jyj5kjk73j")))

(define-public crate-bitcoin-node-query-0.1.1 (c (n "bitcoin-node-query") (v "0.1.1") (d (list (d (n "bitcoind-request") (r "^0.1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)))) (h "1z30v33lz1y3w3m1vw2cdrj3qvrm7gfqp8ngqgffb441jc75wd8l")))

(define-public crate-bitcoin-node-query-0.1.2 (c (n "bitcoin-node-query") (v "0.1.2") (d (list (d (n "bitcoind-request") (r "^0.1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)))) (h "1pprz36awgn3d7hjrccybhc2b424pgzrs5jiv0dx0qqckmmb77f7")))

(define-public crate-bitcoin-node-query-0.1.3 (c (n "bitcoin-node-query") (v "0.1.3") (d (list (d (n "bitcoin-address") (r "=0.1.1") (d #t) (k 0)) (d (n "bitcoin-transaction-utils") (r "=0.1.0") (d #t) (k 0)) (d (n "bitcoind-request") (r "=0.1.11") (d #t) (k 0)) (d (n "chrono") (r "=0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "=0.13.0") (d #t) (k 0)))) (h "1kgcw0yn3102h2vz9nkhbim184rfam18q2bfswpcdi1gqi87f1pm")))

(define-public crate-bitcoin-node-query-0.1.4 (c (n "bitcoin-node-query") (v "0.1.4") (d (list (d (n "bitcoin-address") (r "=0.1.1") (d #t) (k 0)) (d (n "bitcoin-transaction-utils") (r "=0.1.0") (d #t) (k 0)) (d (n "bitcoind-request") (r "=0.1.11") (d #t) (k 0)) (d (n "chrono") (r "=0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "=0.13.0") (d #t) (k 0)))) (h "035bq4qwjyiq0d8px26xpa5kiidlarzfyyhz0hnj0p2hbivp81w4")))

(define-public crate-bitcoin-node-query-0.1.5 (c (n "bitcoin-node-query") (v "0.1.5") (d (list (d (n "bitcoin-address") (r "=0.1.1") (d #t) (k 0)) (d (n "bitcoin-transaction-utils") (r "=0.1.0") (d #t) (k 0)) (d (n "bitcoind-request") (r "=0.1.12") (d #t) (k 0)) (d (n "chrono") (r "=0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "=0.13.0") (d #t) (k 0)))) (h "0sq95maak7i3lc07h8ddmr39dpbpwg33sh648vmw9n3ird6aaaif")))

(define-public crate-bitcoin-node-query-0.1.6 (c (n "bitcoin-node-query") (v "0.1.6") (d (list (d (n "bitcoin-address") (r "=0.1.1") (d #t) (k 0)) (d (n "bitcoin-transaction-utils") (r "=0.1.0") (d #t) (k 0)) (d (n "bitcoind-request") (r "=0.1.12") (d #t) (k 0)) (d (n "chrono") (r "=0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "=0.13.0") (d #t) (k 0)))) (h "1sh1k29lzmj94f1jxm6i2qh625d8689jnxzm1d6fh1rz4l1cm80i")))

(define-public crate-bitcoin-node-query-0.1.7 (c (n "bitcoin-node-query") (v "0.1.7") (d (list (d (n "bitcoin-address") (r "=0.1.1") (d #t) (k 0)) (d (n "bitcoin-transaction-utils") (r "=0.1.0") (d #t) (k 0)) (d (n "bitcoind-request") (r "=0.1.12") (d #t) (k 0)) (d (n "chrono") (r "=0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "=0.13.0") (d #t) (k 0)))) (h "0bigg8z92vm0zdb5iz7pyqbd1acz93vyczj4kxlcwp1h3xxi05j7")))

(define-public crate-bitcoin-node-query-0.1.8 (c (n "bitcoin-node-query") (v "0.1.8") (d (list (d (n "bitcoin-address") (r "=0.1.1") (d #t) (k 0)) (d (n "bitcoin-transaction-utils") (r "=0.1.0") (d #t) (k 0)) (d (n "bitcoind-request") (r "=0.1.12") (d #t) (k 0)) (d (n "chrono") (r "=0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "=0.13.0") (d #t) (k 0)))) (h "04j8xnmhczrgyz2vmmxaxphc6h8xnas7cdrn90bs593hzg3lw339")))

(define-public crate-bitcoin-node-query-0.1.9 (c (n "bitcoin-node-query") (v "0.1.9") (d (list (d (n "bitcoin-address") (r "=0.1.1") (d #t) (k 0)) (d (n "bitcoin-transaction-utils") (r "=0.1.0") (d #t) (k 0)) (d (n "bitcoind-request") (r "=0.1.14") (d #t) (k 0)) (d (n "chrono") (r "=0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "=0.13.0") (d #t) (k 0)))) (h "0igswfwnvbfph2347v1hzbr50sqqkibwqizykkqpili57dhw111b")))

