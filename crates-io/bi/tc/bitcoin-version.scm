(define-module (crates-io bi tc bitcoin-version) #:use-module (crates-io))

(define-public crate-bitcoin-version-0.1.10-alpha.0 (c (n "bitcoin-version") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1ggx27yhwkq3nfpw4l367fwn6pv7n3fkny4bnbsp7l2ihgi2zcc3")))

(define-public crate-bitcoin-version-0.1.12-alpha.0 (c (n "bitcoin-version") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1v2sygdplj6072whmpc4l3cmhz25y567cpqfzyfrfmiiad301jrw")))

(define-public crate-bitcoin-version-0.1.13-alpha.0 (c (n "bitcoin-version") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0a4flmizbzhsnx85g02gnsm6c2j5fgfrq0bghw303076kxqlp0s5")))

(define-public crate-bitcoin-version-0.1.16-alpha.0 (c (n "bitcoin-version") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1f8nbrc254v29azn2g4ibnkkq9d04qypps9fg85vs93gdcwb9x7v")))

