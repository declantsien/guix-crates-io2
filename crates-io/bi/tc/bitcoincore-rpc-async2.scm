(define-module (crates-io bi tc bitcoincore-rpc-async2) #:use-module (crates-io))

(define-public crate-bitcoincore-rpc-async2-4.0.1 (c (n "bitcoincore-rpc-async2") (v "4.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bitcoincore-rpc-json") (r "^0.15.0") (d #t) (k 0)) (d (n "jsonrpc-async") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "031j4n11izhqghh7ki9caw87j45hzq9sfdaifm9pg9xg8zxakw18")))

(define-public crate-bitcoincore-rpc-async2-4.0.2 (c (n "bitcoincore-rpc-async2") (v "4.0.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bitcoincore-rpc-json") (r "^0.16.0") (d #t) (k 0)) (d (n "jsonrpc-async") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1sy34lbfnd0jhb5va1wzgi4qalxih1p1xf03hc8jig76l9sd7639")))

