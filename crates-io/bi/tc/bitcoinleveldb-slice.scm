(define-module (crates-io bi tc bitcoinleveldb-slice) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-slice-0.1.10-alpha.0 (c (n "bitcoinleveldb-slice") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0mmi72qr87dhycfngph12adn7xdxwclwf8fijx2c3d8imzpglsfx")))

(define-public crate-bitcoinleveldb-slice-0.1.12-alpha.0 (c (n "bitcoinleveldb-slice") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "040m5dxb8vxs0h5m9gv097qd0bvm9li6prlk1zd51q3h6r9gqq8w")))

(define-public crate-bitcoinleveldb-slice-0.1.13-alpha.0 (c (n "bitcoinleveldb-slice") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0wdy10n3yif6x5lalrgb4d509h63wn28yd3w7qw1qjx14alyrfry")))

(define-public crate-bitcoinleveldb-slice-0.1.16-alpha.0 (c (n "bitcoinleveldb-slice") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "06rv95vdr57spmdbzxxjg3y7271nkk3jw54p43338l74drip3r6m")))

