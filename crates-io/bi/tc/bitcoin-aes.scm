(define-module (crates-io bi tc bitcoin-aes) #:use-module (crates-io))

(define-public crate-bitcoin-aes-0.1.12-alpha.0 (c (n "bitcoin-aes") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1sgbqi14i96gz3f3xb7q6icsa07xw5av1y324lfhbyfw34xmjvil")))

(define-public crate-bitcoin-aes-0.1.16-alpha.0 (c (n "bitcoin-aes") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0az4qj8dx2k1sc50cirnrbd6jbr7i8nnckw11pddca0dc160gm0r")))

