(define-module (crates-io bi tc bitcoinleveldb-status) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-status-0.1.10-alpha.0 (c (n "bitcoinleveldb-status") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1cfwkdkx2bwh0hpwd8n0pdnabjz0ayfsiv0cisf18cg7nmx473ks")))

(define-public crate-bitcoinleveldb-status-0.1.12-alpha.0 (c (n "bitcoinleveldb-status") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0fa03304pvl8c1j2xyjcnbrpxjs37rvjgnhll8k6s9fb3c2wnpk1")))

(define-public crate-bitcoinleveldb-status-0.1.13-alpha.0 (c (n "bitcoinleveldb-status") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0yn6nl82kpf3d19gkmv4qw34bh5x581mz0vxp1mm1acp7va36qmk")))

(define-public crate-bitcoinleveldb-status-0.1.16-alpha.0 (c (n "bitcoinleveldb-status") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "00w2dh3kkp3fap5nk22qbf7f4rr2iig8rhx9cip1fxa69n0xa105")))

