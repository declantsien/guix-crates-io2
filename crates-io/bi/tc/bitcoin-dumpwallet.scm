(define-module (crates-io bi tc bitcoin-dumpwallet) #:use-module (crates-io))

(define-public crate-bitcoin-dumpwallet-0.1.12-alpha.0 (c (n "bitcoin-dumpwallet") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-string") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinwallet-library") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0zrnss6d2klp8ny6a09qibwpybixkayk82bkvqgklw62iqrzi187")))

(define-public crate-bitcoin-dumpwallet-0.1.16-alpha.0 (c (n "bitcoin-dumpwallet") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-string") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinwallet-library") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "00397wjcxm558ggscy4y135mwgkah88519w6i0azkigh49g65qhw")))

