(define-module (crates-io bi tc bitcoin_address_miner) #:use-module (crates-io))

(define-public crate-bitcoin_address_miner-0.1.0 (c (n "bitcoin_address_miner") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.31.0") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0k4cs84zb8vr2mlh1nal07zphmvh3jf36ii6nagf4vmc5dycqgkl")))

(define-public crate-bitcoin_address_miner-0.1.1 (c (n "bitcoin_address_miner") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.31.0") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06wwzsh64yn1cwa7gqknnmip1lmbgmdvw010vflm9pvv6z6d8jbw")))

(define-public crate-bitcoin_address_miner-0.1.2 (c (n "bitcoin_address_miner") (v "0.1.2") (d (list (d (n "bitcoin") (r "^0.31.0") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1263iy00a7g55q5as6fc0b6lyq8hnv5gw6rp5qqqi2962wpa2nzr")))

