(define-module (crates-io bi tc bitconch-jsonrpc-ws-server) #:use-module (crates-io))

(define-public crate-bitconch-jsonrpc-ws-server-0.1.0 (c (n "bitconch-jsonrpc-ws-server") (v "0.1.0") (d (list (d (n "bitconch-jsonrpc-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bitconch-jsonrpc-server-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "hawk-ws") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1gggavwg0dnkpxfkp46x15vjb4cjdz9s9hdlj60swfbkyknydwqs")))

