(define-module (crates-io bi tc bitcoin_num) #:use-module (crates-io))

(define-public crate-bitcoin_num-0.1.0-beta-1 (c (n "bitcoin_num") (v "0.1.0-beta-1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "14m1636pxs8s6wf257fsy41vqmc8cm9qlp5z9s1r2p0g1gphiyfc") (f (quote (("unstable") ("std") ("serde-std" "serde/std") ("fuzztarget") ("default" "std"))))))

(define-public crate-bitcoin_num-0.1.0-beta-2 (c (n "bitcoin_num") (v "0.1.0-beta-2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "1fvdy7vxv9d5agrgsp7zaqw8gdll7zrdxxpy8q7i62drj6dm4mnk") (f (quote (("unstable") ("std") ("serde-std" "serde/std") ("fuzztarget") ("default" "std"))))))

(define-public crate-bitcoin_num-0.1.0-beta-3 (c (n "bitcoin_num") (v "0.1.0-beta-3") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "1j59v75j3bgk8xc6svg1fdclmw332r7x8g34wwkg6s38dg37l132") (f (quote (("unstable") ("std") ("serde-std" "serde/std") ("fuzztarget") ("default" "std"))))))

(define-public crate-bitcoin_num-0.1.0 (c (n "bitcoin_num") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "01mjms5j9wrr6ff1yspgrazhlnkx4x22dl1mkz8jzj1s7sv4iprm") (f (quote (("unstable") ("std") ("serde-std" "serde/std") ("fuzztarget") ("default" "std"))))))

(define-public crate-bitcoin_num-0.2.0 (c (n "bitcoin_num") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "1w9qch7w16pja0xz2zbwkvjwxxjn8y8d26mcz4gzlgchbk5qidqj") (f (quote (("unstable") ("std") ("serde-std" "serde/std") ("fuzztarget") ("default" "std")))) (y #t)))

(define-public crate-bitcoin_num-0.2.1 (c (n "bitcoin_num") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "047snpdnjjlq8ijwcgc3ccvz4xjzbf4bqns8h6awgckpyglimkvx") (f (quote (("unstable") ("std") ("serde-std" "serde/std") ("fuzztarget") ("default" "std") ("all" "std" "serde" "serde-std"))))))

(define-public crate-bitcoin_num-0.2.2 (c (n "bitcoin_num") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "1zx067pcv241j5zq7kn5ggy79s7mlsgzf2hdd8p0niz3aaja40l0") (f (quote (("unstable") ("std") ("serde-std" "serde/std") ("fuzztarget") ("default" "std") ("all" "std" "serde" "serde-std"))))))

(define-public crate-bitcoin_num-0.2.3 (c (n "bitcoin_num") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "wasm32-unknown-unknown") (k 2)))) (h "0jjqpmyxhbr08h6ywxyg8r34rwpz2ys1sj7nc8j5yz5s5016lnqj") (f (quote (("unstable") ("std") ("serde-std" "serde/std") ("fuzztarget") ("default" "std") ("all" "std" "serde" "serde-std"))))))

