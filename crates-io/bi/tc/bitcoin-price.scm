(define-module (crates-io bi tc bitcoin-price) #:use-module (crates-io))

(define-public crate-bitcoin-price-0.1.0 (c (n "bitcoin-price") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nmm4akl9zkq7p7zqqrd8gzrp56f6xypsd5khna5h7hq43zpa16b")))

(define-public crate-bitcoin-price-0.1.1 (c (n "bitcoin-price") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "11jrf50yp7zhxvflwpsd0wamg6w5w3vxmij3xs46fmjrl4wpyw7l")))

(define-public crate-bitcoin-price-0.1.2 (c (n "bitcoin-price") (v "0.1.2") (d (list (d (n "coinbase-bitcoin") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12rg1llj29f8c6kqvcq33lys9678k4d52nm0f2vc80schz81am4b")))

(define-public crate-bitcoin-price-0.1.3 (c (n "bitcoin-price") (v "0.1.3") (d (list (d (n "coinbase-bitcoin") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l6g400571jr8qb5gac21ai6qqyrhwp91xna4lwd1mrw8yr9wlyj")))

(define-public crate-bitcoin-price-0.1.4 (c (n "bitcoin-price") (v "0.1.4") (d (list (d (n "coinbase-bitcoin") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19l61yryymh7x4jc6nv6jlpilxhys4s9p4r3mx3jmmkhk5d9hajd")))

(define-public crate-bitcoin-price-0.1.5 (c (n "bitcoin-price") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "coinbase-bitcoin") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "03gq11gbsi5jnavdy76v8p40ibfm5h09yyz8ycrs3snpxznpyfz4")))

(define-public crate-bitcoin-price-0.1.6 (c (n "bitcoin-price") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "coinbase-bitcoin") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "156q238y8q77nlw17sh9b52v1sdbhp9h6xw1vdff1jpq3h0h3kxn")))

(define-public crate-bitcoin-price-0.1.7 (c (n "bitcoin-price") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "coinbase-bitcoin") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "04lh1b11nrziwm46pibi5fk0qiwkib4s8ka1ssv1rm7qbfw3rva5")))

