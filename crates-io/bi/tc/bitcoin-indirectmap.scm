(define-module (crates-io bi tc bitcoin-indirectmap) #:use-module (crates-io))

(define-public crate-bitcoin-indirectmap-0.1.12-alpha.0 (c (n "bitcoin-indirectmap") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-mem") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1jlhk0n1vilzkl8d09c1nwy129i3k1a1d2vjjl32rf4my4by3n0b")))

(define-public crate-bitcoin-indirectmap-0.1.16-alpha.0 (c (n "bitcoin-indirectmap") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-mem") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1zj71acb05yh9x0bylkl6nfm283fhvxmr9jp3jsy9i6nw2j2h9vl")))

