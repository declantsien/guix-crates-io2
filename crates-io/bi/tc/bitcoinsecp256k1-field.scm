(define-module (crates-io bi tc bitcoinsecp256k1-field) #:use-module (crates-io))

(define-public crate-bitcoinsecp256k1-field-0.1.10-alpha.0 (c (n "bitcoinsecp256k1-field") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-modinv") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "061lbgrr1vgi0fmnrjfc3pkan80yk70sm158v8i4j9267iyc4006")))

(define-public crate-bitcoinsecp256k1-field-0.1.12-alpha.0 (c (n "bitcoinsecp256k1-field") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-modinv") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "166k24z0gnw1rjpr1wqq3q3pdnhdjsjz1xkysm01yk17h59mlx30")))

(define-public crate-bitcoinsecp256k1-field-0.1.13-alpha.0 (c (n "bitcoinsecp256k1-field") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-modinv") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0jhq3qhh82ph2pi7gfa7mi3z7x4xblkxyy36ccbc6pwzkxv8p7jd")))

(define-public crate-bitcoinsecp256k1-field-0.1.16-alpha.0 (c (n "bitcoinsecp256k1-field") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-modinv") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "10wznccbkwykh86s7b7iaiy04vdlp9wlqskg3fa3b100rhsddy3j")))

