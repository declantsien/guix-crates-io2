(define-module (crates-io bi tc bitcoin-script) #:use-module (crates-io))

(define-public crate-bitcoin-script-0.1.0 (c (n "bitcoin-script") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.23.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "073phb7c2rh01c7hjjjsnqckha947sxg0sq1gycn3a6i8w17jz6l")))

(define-public crate-bitcoin-script-0.1.1 (c (n "bitcoin-script") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.23.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)))) (h "0xk4s1lc75l0hv20alqyxcx3lizfhpy9kqxs7z5s0dqi50wv8klq")))

(define-public crate-bitcoin-script-0.1.2 (c (n "bitcoin-script") (v "0.1.2") (d (list (d (n "bitcoin") (r "^0.27.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "18rgbsmynpxyl4g03hlgzbgvaamzaymxq7kgx0jbf5g4vxkxy5mh")))

(define-public crate-bitcoin-script-0.1.3 (c (n "bitcoin-script") (v "0.1.3") (d (list (d (n "bitcoin") (r "^0.29.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)))) (h "0gz6di3168jhvj2i3pfmajdfsg6spqcbkib0p95ydxjv8cixvqp5")))

