(define-module (crates-io bi tc bitcoincoret4-rpc) #:use-module (crates-io))

(define-public crate-bitcoincoret4-rpc-0.17.0 (c (n "bitcoincoret4-rpc") (v "0.17.0") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "bitcoincoret4-rpc-json") (r "^0.17.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0fjc5pf79hr2zkidf02xhalxb6yrya1dbykzxbgqp22g86b4j72r")))

