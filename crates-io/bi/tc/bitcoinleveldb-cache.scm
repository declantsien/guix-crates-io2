(define-module (crates-io bi tc bitcoinleveldb-cache) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-cache-0.1.10-alpha.0 (c (n "bitcoinleveldb-cache") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0g17xsn41723hxbnj8nngpl2bgdf92f9wjhv9iwr6q9r8azrakn8")))

(define-public crate-bitcoinleveldb-cache-0.1.12-alpha.0 (c (n "bitcoinleveldb-cache") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "15kkv9ffy820xxl4vf21i06ppr511s1mn9qczlss7vqnb3llq7ps")))

(define-public crate-bitcoinleveldb-cache-0.1.13-alpha.0 (c (n "bitcoinleveldb-cache") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "17j9sbqyvxkzdrzd1pr0dfnqk8zyq1pny6vqv3g9nbm0jgkmywxw")))

(define-public crate-bitcoinleveldb-cache-0.1.16-alpha.0 (c (n "bitcoinleveldb-cache") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0cpqf3p8izngda9sjq046sjrswl9b4kgk0ss23bq3ii6nfr2wr5s")))

