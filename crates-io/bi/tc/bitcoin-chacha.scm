(define-module (crates-io bi tc bitcoin-chacha) #:use-module (crates-io))

(define-public crate-bitcoin-chacha-0.1.10-alpha.0 (c (n "bitcoin-chacha") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "01apkxbsp6h4163qkdhn5c0a2pjns5fbjq3az90vl8ai01gxhsi9")))

(define-public crate-bitcoin-chacha-0.1.12-alpha.0 (c (n "bitcoin-chacha") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "13pidz36697pk5n8msn103wdyvz7az1vx6nwh18hax33m01zz2xc")))

(define-public crate-bitcoin-chacha-0.1.13-alpha.0 (c (n "bitcoin-chacha") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0811xqc6p0da1qmkqsdl69j9i7d52m8pd0grs9235gl0xnr5qayd")))

(define-public crate-bitcoin-chacha-0.1.16-alpha.0 (c (n "bitcoin-chacha") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "094bdhysnnia9d1a5dmdrv464ac0xnbb2qrwy6rrmabdi7nn3a4w")))

