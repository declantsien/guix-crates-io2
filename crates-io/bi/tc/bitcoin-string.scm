(define-module (crates-io bi tc bitcoin-string) #:use-module (crates-io))

(define-public crate-bitcoin-string-0.1.7-alpha.0 (c (n "bitcoin-string") (v "0.1.7-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.7-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.7-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.7-alpha.0") (d #t) (k 1)))) (h "1nkfxhq1xyyrnmv6b4rkzqx7n1k2f7ca83h1xr45g4vdd1lz89xc")))

(define-public crate-bitcoin-string-0.1.8-alpha.0 (c (n "bitcoin-string") (v "0.1.8-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.7-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.7-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.7-alpha.0") (d #t) (k 1)))) (h "0kc1wbrpdyyks4x3xyyj7aj0cznbwz6h671yf1d5h0ccwiq8nf2z")))

(define-public crate-bitcoin-string-0.1.10-alpha.0 (c (n "bitcoin-string") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "17wh85i7qgipdjdm2dhlkcppj21dxj8cs7d0xrggh50jh2sr8jvi")))

(define-public crate-bitcoin-string-0.1.12-alpha.0 (c (n "bitcoin-string") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0z1q7gxmhrmfb0sj7lgg2kkmwy3ndbb50ffg1vxignlc91crqx4w")))

(define-public crate-bitcoin-string-0.1.13-alpha.0 (c (n "bitcoin-string") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0qnx10h6dnw8d6bh2xri1a6jri1hg8j86xqjw3c3mwzx0xfg1mfm")))

(define-public crate-bitcoin-string-0.1.14-alpha.0 (c (n "bitcoin-string") (v "0.1.14-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.14-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "1p49qkfkad0i36il1w5k10rxdr0dp9phwbyasgi83677h1sbifln")))

(define-public crate-bitcoin-string-0.1.16-alpha.0 (c (n "bitcoin-string") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "18yj60pncx2xkv21313a4qyf5hg2higy1k3nr8fgz2mgn5165g9g")))

