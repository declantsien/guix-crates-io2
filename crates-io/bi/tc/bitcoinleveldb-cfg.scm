(define-module (crates-io bi tc bitcoinleveldb-cfg) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-cfg-0.1.10-alpha.0 (c (n "bitcoinleveldb-cfg") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "14b7pzgc9sm1j0dryd12afmpf1v3sxbs8v0c400akgqdhx2qi90z")))

(define-public crate-bitcoinleveldb-cfg-0.1.12-alpha.0 (c (n "bitcoinleveldb-cfg") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0q2z9pbv6pc92d95dvh7di2qwg6cmh0paas63cyl7m3a5i9kg26p")))

(define-public crate-bitcoinleveldb-cfg-0.1.16-alpha.0 (c (n "bitcoinleveldb-cfg") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "12p11fxjywi6b4hbg74sbsrk2iysdvcabnlgvj11qa0d3g24fbjq")))

