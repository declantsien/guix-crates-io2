(define-module (crates-io bi tc bitcoincore-zmq) #:use-module (crates-io))

(define-public crate-bitcoincore-zmq-1.0.0 (c (n "bitcoincore-zmq") (v "1.0.0") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0z2pv0ldlwdf8qxgzaij68zziiykbpr74ww9c8y533lb75al1pfd") (y #t)))

(define-public crate-bitcoincore-zmq-1.0.1 (c (n "bitcoincore-zmq") (v "1.0.1") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.17.0") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "1r8lcg2jkvk7jsl1kjl0n48vdbk3dgb742zf7njmi7qpq0zvy0ji")))

(define-public crate-bitcoincore-zmq-1.0.2 (c (n "bitcoincore-zmq") (v "1.0.2") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.17.0") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "1dd4hjfjvsh95n7mag4cf80kk18mxa57jp8g6vx1p1cr193zqny7")))

(define-public crate-bitcoincore-zmq-1.0.3 (c (n "bitcoincore-zmq") (v "1.0.3") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.17.0") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0lw6iv6kzm5p7jpvvcfdra89x8d5fkpmbdkg1r9wx4k6yq38dkzx")))

(define-public crate-bitcoincore-zmq-1.0.4 (c (n "bitcoincore-zmq") (v "1.0.4") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.17.0") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0vrwf15bs7nlwgj62p0ciy214hvgky3wjv0x0wlk1yclf67iw8p7")))

(define-public crate-bitcoincore-zmq-1.0.5 (c (n "bitcoincore-zmq") (v "1.0.5") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.17.0") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0xyiz2f9mc9wwr1k2dvyp462fkfw3k6139ksf2xhghps40xsxk4s")))

(define-public crate-bitcoincore-zmq-1.0.6 (c (n "bitcoincore-zmq") (v "1.0.6") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.17.0") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0fcdjld998378yj5kbjns8mxgj0bp6rqq3r8v2jl2k14m3pysgi6")))

(define-public crate-bitcoincore-zmq-1.0.7 (c (n "bitcoincore-zmq") (v "1.0.7") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.17.0") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "12rgh01mqpzfwvdfby8cavbk43qg3cmgfw12qldasiph7imiad89")))

(define-public crate-bitcoincore-zmq-1.0.8 (c (n "bitcoincore-zmq") (v "1.0.8") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.17.0") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "1q1l3fdv8q1mir8dq42irpgrml0mak9fd0phmmxlw6h1lhpimpsf")))

(define-public crate-bitcoincore-zmq-1.1.0 (c (n "bitcoincore-zmq") (v "1.1.0") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.17.0") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0n8h0znypw44lllq0my7cx3rmppx5f5sz0j05fl0h9p0l481jm0p") (y #t)))

(define-public crate-bitcoincore-zmq-1.1.1 (c (n "bitcoincore-zmq") (v "1.1.1") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.17.0") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "1knby4bdsh12al5yyc6krc61qjzx2gydxcmdwpc4vpikkzbfc9q3")))

(define-public crate-bitcoincore-zmq-1.2.0 (c (n "bitcoincore-zmq") (v "1.2.0") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0d7dzv6w0bvm1dsgrzfy9r7rsm89gxi6whly19apqcx9n1pdlfnq")))

(define-public crate-bitcoincore-zmq-1.3.0 (c (n "bitcoincore-zmq") (v "1.3.0") (d (list (d (n "async_zmq") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "05pc5ac3rd5d3zi2a2pycrf3a5sm6m2wkcdg86jglljh22kjqcb8") (s 2) (e (quote (("async" "dep:async_zmq" "dep:futures-util"))))))

(define-public crate-bitcoincore-zmq-1.3.1 (c (n "bitcoincore-zmq") (v "1.3.1") (d (list (d (n "async_zmq") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "1ivs0sv9m7cnvpakh8d5d57y4815wjyfzskfzw4ykii7iap435xv") (s 2) (e (quote (("async" "dep:async_zmq" "dep:futures-util"))))))

(define-public crate-bitcoincore-zmq-1.4.0 (c (n "bitcoincore-zmq") (v "1.4.0") (d (list (d (n "async_zmq") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "bitcoin") (r "^0.31.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)) (d (n "zmq-sys") (r "^0.12.0") (d #t) (k 0)))) (h "0ipgavbykkz2y1k9zjixa6yhi6pfmlc0yd8jz4ryrd93xan227pz") (s 2) (e (quote (("async" "dep:async_zmq" "dep:futures-util"))))))

(define-public crate-bitcoincore-zmq-1.5.0 (c (n "bitcoincore-zmq") (v "1.5.0") (d (list (d (n "async_zmq") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "bitcoin") (r "^0.32.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("time" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)) (d (n "zmq-sys") (r "^0.12.0") (d #t) (k 0)))) (h "0c7m6hi51lbzm40j7ls7j6vnpjydyr783nnl406n5yan8b14y95a") (s 2) (e (quote (("async" "dep:async_zmq" "dep:futures-util"))))))

