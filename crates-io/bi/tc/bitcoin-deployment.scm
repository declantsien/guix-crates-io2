(define-module (crates-io bi tc bitcoin-deployment) #:use-module (crates-io))

(define-public crate-bitcoin-deployment-0.1.12-alpha.0 (c (n "bitcoin-deployment") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-block") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-chain-consensus") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "06z8lxc6h160ysvr4f89scy91lfhcrnpf5xp6znnig6cjg6wwy1l")))

(define-public crate-bitcoin-deployment-0.1.16-alpha.0 (c (n "bitcoin-deployment") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-block") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-chain-consensus") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "0dv6zmdj733xfyl3y7j6nkwyj441srmc4rafgjs8ql8za9c25dx7")))

