(define-module (crates-io bi tc bitcoin-portmap) #:use-module (crates-io))

(define-public crate-bitcoin-portmap-0.1.12-alpha.0 (c (n "bitcoin-portmap") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "0p37zni3sx47492ph7cgxmi8xniqd49bia39zxwlx9c8hcfxnh9p")))

(define-public crate-bitcoin-portmap-0.1.16-alpha.0 (c (n "bitcoin-portmap") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "0qzm4myq4p4lcbsvvp955wjw57qvfrcqnxyl8whw8nmyh2mx12q3")))

