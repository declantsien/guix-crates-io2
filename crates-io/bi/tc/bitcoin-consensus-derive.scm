(define-module (crates-io bi tc bitcoin-consensus-derive) #:use-module (crates-io))

(define-public crate-bitcoin-consensus-derive-0.1.0-beta.1 (c (n "bitcoin-consensus-derive") (v "0.1.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "138j759bpj8nl6jxfhznv2cpk9kql3mj334j6ly9cg10ampdslhd")))

(define-public crate-bitcoin-consensus-derive-0.1.0-beta.2 (c (n "bitcoin-consensus-derive") (v "0.1.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0znanlyjwvk9dmsdvn46srsc8v2x3v2v2r7qbzf0gw3sii2kd6sp")))

(define-public crate-bitcoin-consensus-derive-0.1.0 (c (n "bitcoin-consensus-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pd56c0442k70z4jrgwwb28cw366pjlrvdkvgc4hnp0zzv3kcq6m")))

