(define-module (crates-io bi tc bitcointx) #:use-module (crates-io))

(define-public crate-bitcointx-0.0.1 (c (n "bitcointx") (v "0.0.1") (h "0baabbkvn5b5jfz1b9nfd6ln0s99ar42mcsv96vxdf3s7pz00v7h")))

(define-public crate-bitcointx-0.0.2 (c (n "bitcointx") (v "0.0.2") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1acrmmdpla7skihplgk6awifn9fgy4fs4c26qryfvbx8lnhll97d")))

(define-public crate-bitcointx-0.0.3 (c (n "bitcointx") (v "0.0.3") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1zcgc8pqaa35va24m78l65xsf34884kjq9381r5zk60nsjaqbihz")))

(define-public crate-bitcointx-0.0.4 (c (n "bitcointx") (v "0.0.4") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "061xqb3x5ls8r345scfa4iwvwg4q6nsj8bldd3i3q6g9k0v846aj")))

(define-public crate-bitcointx-0.0.5 (c (n "bitcointx") (v "0.0.5") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1g5l2dbpvbxv7rrl4bc0z777b06b2dg4hj4h01248z8nz418vfsf")))

(define-public crate-bitcointx-0.0.8 (c (n "bitcointx") (v "0.0.8") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0n190b247vh9a5bczrngv9ql739qzj601c883m3m1jhp5zqbijmx")))

(define-public crate-bitcointx-0.0.9 (c (n "bitcointx") (v "0.0.9") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0d7ri21c3vwbc0n2h5hn7da8k7vwc653rp6fm3mmcz5j9isg5irn")))

(define-public crate-bitcointx-0.0.10 (c (n "bitcointx") (v "0.0.10") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0cjx2ik21dblysfi86hf8c4sckar5z752v7kkbn3h556949ars47")))

(define-public crate-bitcointx-0.0.11 (c (n "bitcointx") (v "0.0.11") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0kl7gi736k7i2cs1jxkgxc4fvrxs4l4s1av124if7bivqh2cf9f0")))

(define-public crate-bitcointx-0.0.12 (c (n "bitcointx") (v "0.0.12") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1baf8ljv4dvvr4pavr11p64b89q7l1gvrik3n3ks5lh16bm12zp4")))

(define-public crate-bitcointx-0.0.13 (c (n "bitcointx") (v "0.0.13") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0aaw2q86l96qb2l6sdvr77x3y6n0vdjfv35cdimh127s7mzckq6a")))

(define-public crate-bitcointx-0.0.14 (c (n "bitcointx") (v "0.0.14") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1ccxdvi0q1k6d7dylkfs098nr2qn1x2s8hyl99iw3qh7wqqzhga8")))

(define-public crate-bitcointx-0.0.15 (c (n "bitcointx") (v "0.0.15") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0vk0mj04631xfjfr5iab39blc4k4laqfp5q3fgkbzz316vw4i0j3")))

