(define-module (crates-io bi tc bitcoin-push-decoder) #:use-module (crates-io))

(define-public crate-bitcoin-push-decoder-0.1.0 (c (n "bitcoin-push-decoder") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.29") (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "1ffvdswl9cpby8m1gsf0b24c6yb01j3l5khsdsxcmcjqlg15byj0") (f (quote (("std" "bitcoin/std") ("no-std" "bitcoin/no-std") ("default" "std")))) (r "1.60.0")))

(define-public crate-bitcoin-push-decoder-0.2.0 (c (n "bitcoin-push-decoder") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.29") (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "1bv9q0my1y3k6cg2wrh59wpk8zg6fypckbn23qainkjdfsvf12xa") (f (quote (("std" "bitcoin/std") ("no-std" "bitcoin/no-std") ("default" "std")))) (r "1.60.0")))

(define-public crate-bitcoin-push-decoder-0.2.1 (c (n "bitcoin-push-decoder") (v "0.2.1") (d (list (d (n "bitcoin") (r "^0.29") (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "1znc58s4vgrlz5wapazfzxrfpxcn1mygayvfq0j2rkgyq4qin1b0") (f (quote (("std" "bitcoin/std") ("no-std" "bitcoin/no-std") ("default" "std")))) (r "1.60.0")))

(define-public crate-bitcoin-push-decoder-0.2.2 (c (n "bitcoin-push-decoder") (v "0.2.2") (d (list (d (n "bitcoin") (r "^0.29") (k 0)) (d (n "core2") (r "^0.3") (o #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "1gkba99wsgnjrrypa1sh6581ip4hwr55448c1yl8hhwycxnghcym") (f (quote (("std" "bitcoin/std") ("no-std" "bitcoin/no-std" "core2/alloc") ("default" "std")))) (r "1.60.0")))

