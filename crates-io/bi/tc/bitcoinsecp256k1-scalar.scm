(define-module (crates-io bi tc bitcoinsecp256k1-scalar) #:use-module (crates-io))

(define-public crate-bitcoinsecp256k1-scalar-0.1.10-alpha.0 (c (n "bitcoinsecp256k1-scalar") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-modinv") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1pg8gbvm1brr4167dn2yfhg990pi44bglj0wrmbd27xrqgacwbqg")))

(define-public crate-bitcoinsecp256k1-scalar-0.1.12-alpha.0 (c (n "bitcoinsecp256k1-scalar") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-modinv") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1lj0hbf3ir2hs0znw4ar8m880kzl8j9xsi8mpv188irazn84jblz")))

(define-public crate-bitcoinsecp256k1-scalar-0.1.13-alpha.0 (c (n "bitcoinsecp256k1-scalar") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-modinv") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "1di9im97pv8i3gy6jwwp040y55fnrbf0xccvr448xr1ch6mkbwis")))

(define-public crate-bitcoinsecp256k1-scalar-0.1.16-alpha.0 (c (n "bitcoinsecp256k1-scalar") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinsecp256k1-modinv") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0acsbjcrz0lmyyydr82qz7n1q4db5jwr78qwbbszmdydyfydjd7m")))

