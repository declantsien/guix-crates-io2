(define-module (crates-io bi tc bitcoin_opcodes) #:use-module (crates-io))

(define-public crate-bitcoin_opcodes-0.1.0 (c (n "bitcoin_opcodes") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (k 0)))) (h "1hifj0wz0pfsbjxz0wsmp7g5chhvbdgsm810mgwsiqjbc0c7sqkk") (f (quote (("std" "serde/std" "strum/std") ("default" "std") ("alloc" "serde/alloc"))))))

