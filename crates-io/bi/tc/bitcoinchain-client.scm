(define-module (crates-io bi tc bitcoinchain-client) #:use-module (crates-io))

(define-public crate-bitcoinchain-client-0.1.12-alpha.0 (c (n "bitcoinchain-client") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-scheduler") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "1kk7ls3ws7ia4y99n01k0ihak1pdlmm6arbxgrxkx0kgszw8h6c4")))

(define-public crate-bitcoinchain-client-0.1.16-alpha.0 (c (n "bitcoinchain-client") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-scheduler") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "0kz57xfq1x545pnyf3w9h8ckyqil4pg2dwzwc06rnnd047mm9pyd")))

