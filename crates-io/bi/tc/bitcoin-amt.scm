(define-module (crates-io bi tc bitcoin-amt) #:use-module (crates-io))

(define-public crate-bitcoin-amt-0.1.7-alpha.0 (c (n "bitcoin-amt") (v "0.1.7-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.7-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.7-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.7-alpha.0") (d #t) (k 1)))) (h "0vjwlcrvbqqxq51ifb0w3f04fw5xnwrnhl399snlv48idhj0l9wl")))

(define-public crate-bitcoin-amt-0.1.10-alpha.0 (c (n "bitcoin-amt") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0rhvgphw6l52zlprlvivrfqblmj0pnjls5sibij7bl2439sykw4g")))

(define-public crate-bitcoin-amt-0.1.12-alpha.0 (c (n "bitcoin-amt") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1ddb5lmmfk39mr3cs1kvqgra7fypwzjqq0hw4fprij0rbn1lsk16")))

(define-public crate-bitcoin-amt-0.1.13-alpha.0 (c (n "bitcoin-amt") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "04sgqdpsgj954yb891x6l7w741clb11rn7c9fdavc1c575w9rpm9")))

(define-public crate-bitcoin-amt-0.1.14-alpha.0 (c (n "bitcoin-amt") (v "0.1.14-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0m27kj3df7f6wasv4m8fai4zjr8d5r1i33r5xgalhjrvwd17m3c9")))

(define-public crate-bitcoin-amt-0.1.15-alpha.0 (c (n "bitcoin-amt") (v "0.1.15-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.15-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.15-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.15-alpha.0") (d #t) (k 1)))) (h "09hhwh5fyfl7d8byq2bv4ayiqfw0ayv91hqziprzkxdbwsgi8w7n")))

(define-public crate-bitcoin-amt-0.1.16-alpha.0 (c (n "bitcoin-amt") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0llc2y5ay1kcgkwvxn5y13adc9zpr5rgiwll09q5i1fjgaih31h0")))

