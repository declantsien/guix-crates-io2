(define-module (crates-io bi tc bitcoin-keypool) #:use-module (crates-io))

(define-public crate-bitcoin-keypool-0.1.12-alpha.0 (c (n "bitcoin-keypool") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-key") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "0g86vpb7342mwcc2fqqypa75js5sq4cgikdrj3fgvlf42pp0vdzx")))

