(define-module (crates-io bi tc bitcoincash-addr) #:use-module (crates-io))

(define-public crate-bitcoincash-addr-0.1.0 (c (n "bitcoincash-addr") (v "0.1.0") (d (list (d (n "bitcoin_hashes") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)))) (h "04cqa54v0fzzsf4jcxsw3ws2igzk56sx5rmdvw8ywr80jhdcwkvw")))

(define-public crate-bitcoincash-addr-0.1.1 (c (n "bitcoincash-addr") (v "0.1.1") (d (list (d (n "bitcoin_hashes") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 0)))) (h "09mnhsqx72rz9z5056b7c2hyvrpcm3vqhc4q8dbjczcgjz9w2pbw")))

(define-public crate-bitcoincash-addr-0.2.0 (c (n "bitcoincash-addr") (v "0.2.0") (d (list (d (n "bitcoin_hashes") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)))) (h "107ggrwm0n0vh8zqi9h3rk9nabxxv6prx1p211mfwld1kac2d8vz")))

(define-public crate-bitcoincash-addr-0.2.1 (c (n "bitcoincash-addr") (v "0.2.1") (d (list (d (n "bitcoin_hashes") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)))) (h "1kjj4gbnyyzz98434mb7y5z0s8wxhww5mhvmcpd6vm028hxkb97c")))

(define-public crate-bitcoincash-addr-0.3.0 (c (n "bitcoincash-addr") (v "0.3.0") (d (list (d (n "bitcoin_hashes") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)))) (h "01cql37xzpflpyrl62mf9aw0p2p4b2p2bhzrag57mq1whnhcrlz4")))

(define-public crate-bitcoincash-addr-0.4.0 (c (n "bitcoincash-addr") (v "0.4.0") (d (list (d (n "bitcoin_hashes") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)))) (h "15bdmbbsfgvlwmqxn39xqalg3a2fwsxk2m0c0d2x0lp7jblyjjcw")))

(define-public crate-bitcoincash-addr-0.5.0 (c (n "bitcoincash-addr") (v "0.5.0") (d (list (d (n "bitcoin_hashes") (r "^0.7.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)))) (h "055zqi6nqmdrz3jsj7hc7yf901wycsvcqzs5x5pjbvqmivkrq7id")))

(define-public crate-bitcoincash-addr-0.5.1 (c (n "bitcoincash-addr") (v "0.5.1") (d (list (d (n "bitcoin_hashes") (r "^0.7.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 2)))) (h "1gyg5cngf27wycmsn51b24lf7amj3q7xd3g2r2j0fsjzqhiycwdy")))

(define-public crate-bitcoincash-addr-0.5.2 (c (n "bitcoincash-addr") (v "0.5.2") (d (list (d (n "bitcoin_hashes") (r "^0.7.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)))) (h "0hz5fnv80j3ni1nw260si26xmsbyb8v8l6cbjby55z3ysazsyydd")))

