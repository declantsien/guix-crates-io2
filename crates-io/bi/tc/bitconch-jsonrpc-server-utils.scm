(define-module (crates-io bi tc bitconch-jsonrpc-server-utils) #:use-module (crates-io))

(define-public crate-bitconch-jsonrpc-server-utils-0.1.0 (c (n "bitconch-jsonrpc-server-utils") (v "0.1.0") (d (list (d (n "bitconch-jsonrpc-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0ccziw56wrj2cz6vxsyhlza3dqwhzbpy6134mihchrfgnrfh3577")))

