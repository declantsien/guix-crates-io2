(define-module (crates-io bi tc bitcoin-rpc) #:use-module (crates-io))

(define-public crate-bitcoin-rpc-0.1.0 (c (n "bitcoin-rpc") (v "0.1.0") (d (list (d (n "jsonrpc-v1") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "10hdpbbglaqi1yb131j6s328hhr57d5vg8skbdhd55vh0qrsa5ms")))

(define-public crate-bitcoin-rpc-0.1.1 (c (n "bitcoin-rpc") (v "0.1.1") (d (list (d (n "jsonrpc-v1") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "08ww2jg4rj8zqc8w88522jfynlil7rd0gb3bnbk0513d2r9jr8g5")))

(define-public crate-bitcoin-rpc-0.1.2 (c (n "bitcoin-rpc") (v "0.1.2") (d (list (d (n "jsonrpc-v1") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "strason") (r "^0.3") (d #t) (k 0)))) (h "07ni41qwhx041nfvfjigcnkgn0dphkicmvbbs4k0v01hs2dgj1m5")))

(define-public crate-bitcoin-rpc-0.2.0 (c (n "bitcoin-rpc") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.13") (d #t) (k 0)) (d (n "bitcoin-rpc-json") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "strason") (r "^0.4") (d #t) (k 0)))) (h "0yg1wa5s243sbr61y0ss1fb45kdcjalxmnqf47nyikhq9rc5fcgb")))

