(define-module (crates-io bi tc bitcoin-test-data) #:use-module (crates-io))

(define-public crate-bitcoin-test-data-0.1.0 (c (n "bitcoin-test-data") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "electrsd") (r "^0.23.3") (f (quote ("bitcoind_23_0" "electrs_0_9_11"))) (d #t) (k 2)))) (h "1g6jayy4sybwjhz6z761bhyjmdw90wvrmmc2d6vhsd04m92zkdvp")))

(define-public crate-bitcoin-test-data-0.2.0 (c (n "bitcoin-test-data") (v "0.2.0") (d (list (d (n "electrsd") (r "^0.23.3") (f (quote ("bitcoind_23_0" "electrs_0_9_11"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0d1zc08fxl79adginhkysg54nlddfybw9adzw5nbrqywz5a8c60c")))

