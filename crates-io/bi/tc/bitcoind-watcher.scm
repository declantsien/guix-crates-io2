(define-module (crates-io bi tc bitcoind-watcher) #:use-module (crates-io))

(define-public crate-bitcoind-watcher-0.1.1 (c (n "bitcoind-watcher") (v "0.1.1") (d (list (d (n "bitcoind-log-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "logwatcher") (r "^0.1") (d #t) (k 0)))) (h "1z5dchrv33cnfcsyb8vhy2zm2rki4awz4m8ml29bnaykvylj1dvg")))

(define-public crate-bitcoind-watcher-0.1.2 (c (n "bitcoind-watcher") (v "0.1.2") (d (list (d (n "bitcoind-log-parser") (r "^0.1.3") (d #t) (k 0)) (d (n "logwatcher") (r "^0.1") (d #t) (k 0)))) (h "0dywwx40b1w5ywp07icwww9hj271vxc88cr299sf42pvyqx1s475")))

