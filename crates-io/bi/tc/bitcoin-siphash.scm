(define-module (crates-io bi tc bitcoin-siphash) #:use-module (crates-io))

(define-public crate-bitcoin-siphash-0.1.10-alpha.0 (c (n "bitcoin-siphash") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0y0ccfrbs0dlbmpacnh59xrah6k7f2dfmz85lk1bf3p98xk0h324")))

(define-public crate-bitcoin-siphash-0.1.12-alpha.0 (c (n "bitcoin-siphash") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "18xlvpjly9a24gnj9a91xhgilbks8ag06ld99rgb8kg45bdz2xiy")))

(define-public crate-bitcoin-siphash-0.1.16-alpha.0 (c (n "bitcoin-siphash") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "12yj9pycq4pclkh023sv5wkjfhxr9r1qhxbm0k29fdq373mq2k9v")))

