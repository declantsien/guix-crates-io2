(define-module (crates-io bi tc bitcoin-io) #:use-module (crates-io))

(define-public crate-bitcoin-io-0.1.0 (c (n "bitcoin-io") (v "0.1.0") (h "0hravkn480x0rx9qq622yzfzvhzxv6g9hhhz7cmivc6g22b6x0vq") (r "1.56.1")))

(define-public crate-bitcoin-io-0.1.1 (c (n "bitcoin-io") (v "0.1.1") (h "12drqncwkkx6w36gdfwh0ik3p15dh0lqi8gfgc414x36i1mvgr8p") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56.1")))

(define-public crate-bitcoin-io-0.1.2 (c (n "bitcoin-io") (v "0.1.2") (h "0mpfma9hyipnm3x7yhbk8whwk85yb2mgcnj95y8xhyww77l0j3il") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56.1")))

