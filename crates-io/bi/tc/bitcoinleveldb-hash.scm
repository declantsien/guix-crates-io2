(define-module (crates-io bi tc bitcoinleveldb-hash) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-hash-0.1.10-alpha.0 (c (n "bitcoinleveldb-hash") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1fjlrfqwardpywz9nsvh3mn93flyk53wcfgbldqkzpqsrsay2irc")))

(define-public crate-bitcoinleveldb-hash-0.1.12-alpha.0 (c (n "bitcoinleveldb-hash") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "104warrv9plqsrmxhwwyh0rjski9j0z8m5kgmf03wmh6qwn1fb2z")))

(define-public crate-bitcoinleveldb-hash-0.1.16-alpha.0 (c (n "bitcoinleveldb-hash") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0sdqzpar3sr12w4igicb0lif3v0zhaq190as3va62d41jlj5i30x")))

