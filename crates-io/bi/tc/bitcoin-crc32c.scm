(define-module (crates-io bi tc bitcoin-crc32c) #:use-module (crates-io))

(define-public crate-bitcoin-crc32c-0.1.12-alpha.0 (c (n "bitcoin-crc32c") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0x5x7rfkjjc2xm3m8cn5i51khrg7hx7ny8sqnmxnkkn880v4c2qs")))

(define-public crate-bitcoin-crc32c-0.1.16-alpha.0 (c (n "bitcoin-crc32c") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "03vkrp5cd3ga035zn7ppf4g7xzm8cwdrhhj6i55ddj3cmndnnv1g")))

