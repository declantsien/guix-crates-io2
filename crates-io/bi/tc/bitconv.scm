(define-module (crates-io bi tc bitconv) #:use-module (crates-io))

(define-public crate-bitconv-0.1.0 (c (n "bitconv") (v "0.1.0") (h "1zfpv1zh2q1s0y3hgaiy8l0v49dhksifj6cs6nw5x74wvzjr5qhw")))

(define-public crate-bitconv-0.1.1 (c (n "bitconv") (v "0.1.1") (h "1y6nrdng1hi9qacdflih2j7i6jsc3d66bmwsbpib8hw1l3al9zx2")))

(define-public crate-bitconv-0.1.2 (c (n "bitconv") (v "0.1.2") (h "13y1jszxps92l29j23dlm6830iq09wb29mawjkscq1jzjjdb4q4s")))

(define-public crate-bitconv-0.1.3 (c (n "bitconv") (v "0.1.3") (h "1qf5v9xa8v5gdjvixk97kcsi9nhjmrix86g28wk3r6cacjcmc57n")))

(define-public crate-bitconv-0.1.4 (c (n "bitconv") (v "0.1.4") (h "0jk1y1ya2iqs6i325vzsxqlv814kvywfw0wf5cgx42784hyykpra")))

