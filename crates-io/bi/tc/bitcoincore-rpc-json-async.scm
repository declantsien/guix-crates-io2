(define-module (crates-io bi tc bitcoincore-rpc-json-async) #:use-module (crates-io))

(define-public crate-bitcoincore-rpc-json-async-2.12.0 (c (n "bitcoincore-rpc-json-async") (v "2.12.0") (d (list (d (n "bitcoin") (r "^0.25") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0a265irnn20kzrpwm747ha1wkp2rbvdg1z4v1g65rrbkqgl39d09")))

(define-public crate-bitcoincore-rpc-json-async-3.0.0 (c (n "bitcoincore-rpc-json-async") (v "3.0.0") (d (list (d (n "bitcoin") (r "^0.26") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0qn6fac52sva0kijbnrskbb1sbxm4fa1zzwywv1cakfhv1b689s6")))

(define-public crate-bitcoincore-rpc-json-async-3.0.1 (c (n "bitcoincore-rpc-json-async") (v "3.0.1") (d (list (d (n "bitcoin") (r "^0.26") (f (quote ("use-serde"))) (d #t) (k 0) (p "sapio-bitcoin")) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1r4vs17mdrqb290w2qzhjny1s206byskkjrx3vjma741lqyv20r7")))

(define-public crate-bitcoincore-rpc-json-async-4.0.0-alpha.0 (c (n "bitcoincore-rpc-json-async") (v "4.0.0-alpha.0") (d (list (d (n "bitcoin") (r "^0.28.0-rc.3") (f (quote ("use-serde"))) (d #t) (k 0) (p "sapio-bitcoin")) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1dx59pzkh7q32mi8jgq2x0frh94ch4b184l5s2nm640zpsrr5dfl")))

(define-public crate-bitcoincore-rpc-json-async-4.0.1-alpha.2 (c (n "bitcoincore-rpc-json-async") (v "4.0.1-alpha.2") (d (list (d (n "bitcoin") (r "^0.28.0-rc.3") (f (quote ("use-serde"))) (d #t) (k 0) (p "sapio-bitcoin")) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13jf0hsqrkp28a36r6ril6angi8r4jp4l5kixdc6wx1l3a30fviv")))

