(define-module (crates-io bi tc bitcoin-tokenpipe) #:use-module (crates-io))

(define-public crate-bitcoin-tokenpipe-0.1.12-alpha.0 (c (n "bitcoin-tokenpipe") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "051jgnzr4dnl8hq92zy6hkf85jfxwdrza1s8fv0i5arbamqmmn8c")))

(define-public crate-bitcoin-tokenpipe-0.1.16-alpha.0 (c (n "bitcoin-tokenpipe") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "1sz7qaip1c87kd2di20d2xdfkrk6whz94fx21300j2l2i4a27223")))

