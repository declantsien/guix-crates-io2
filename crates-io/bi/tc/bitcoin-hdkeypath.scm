(define-module (crates-io bi tc bitcoin-hdkeypath) #:use-module (crates-io))

(define-public crate-bitcoin-hdkeypath-0.1.12-alpha.0 (c (n "bitcoin-hdkeypath") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "0rnalpifk1k4b2gx1v204dq5d7vz658yqrllvxxjsj54j673x4vr")))

