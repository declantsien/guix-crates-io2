(define-module (crates-io bi tc bitcoin-epoch) #:use-module (crates-io))

(define-public crate-bitcoin-epoch-0.1.12-alpha.0 (c (n "bitcoin-epoch") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1a0dy6kharx8f9k9q0xxy02a8j6vhdv6jqh49n2xy9hp4nnksdlr")))

(define-public crate-bitcoin-epoch-0.1.16-alpha.0 (c (n "bitcoin-epoch") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0pr92zza76gyaaar2lr7j5k0q0yxh6g5xga1y4s8az1prk41d8fb")))

