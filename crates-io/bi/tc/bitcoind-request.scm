(define-module (crates-io bi tc bitcoind-request) #:use-module (crates-io))

(define-public crate-bitcoind-request-0.1.0 (c (n "bitcoind-request") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16k5kz380avgja7ar8yrpg8y34ykvs48p3qg5pri0sph5yjyqmg9")))

(define-public crate-bitcoind-request-0.1.1 (c (n "bitcoind-request") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03c332nih130lxp6nri72q6748l0875pdw8h5fvl3673qg69j8gx")))

(define-public crate-bitcoind-request-0.1.2 (c (n "bitcoind-request") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d6jbhl248zf66kg59msy7zz4bcr2wv8pcnkn9hxdqmns0v5rcvn")))

(define-public crate-bitcoind-request-0.1.3 (c (n "bitcoind-request") (v "0.1.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "179327mvjfz6xzqzxmal15y76wnzbyha407jjw8v08maxbs6ab43")))

(define-public crate-bitcoind-request-0.1.4 (c (n "bitcoind-request") (v "0.1.4") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f023nap76h5nl28c7xr9hvaiw6k4fch4g18v8ybsv9knycmnhfx")))

(define-public crate-bitcoind-request-0.1.5 (c (n "bitcoind-request") (v "0.1.5") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yvnjpv9q7avaypy7rfyd8cnpcj6ls8s5zcvk35818w8q4j8jksy")))

(define-public crate-bitcoind-request-0.1.6 (c (n "bitcoind-request") (v "0.1.6") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b4bfm697dpc8q0mpadi7ir501m763sv9kj2w1knfz2q3gcwbbbl")))

(define-public crate-bitcoind-request-0.1.7 (c (n "bitcoind-request") (v "0.1.7") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04paqrvw226gbqf31y1qg3xz28ngk56mvhv93ip9c5yd21pc32k8")))

(define-public crate-bitcoind-request-0.1.8 (c (n "bitcoind-request") (v "0.1.8") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06k8rw7libzyw8vmzzvsacm24yaf2lvfjjn0sgwv8w9q2a9acjpg")))

(define-public crate-bitcoind-request-0.1.9 (c (n "bitcoind-request") (v "0.1.9") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "144bwzchmlxwj6v9mljfwyavbmmy820cg3v9zlp7diyw2mkcvici")))

(define-public crate-bitcoind-request-0.1.10 (c (n "bitcoind-request") (v "0.1.10") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w48cvs31mw29czsxfsknnwv082n48p6aymw8qccyfmkcr08ay5d")))

(define-public crate-bitcoind-request-0.1.11 (c (n "bitcoind-request") (v "0.1.11") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f5s1xsnysrvzr6dvgwcvgma2584g5y5pharg9v58apn6l6jvwm9")))

(define-public crate-bitcoind-request-0.1.12 (c (n "bitcoind-request") (v "0.1.12") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "162xhcgc41w8vp59kx9913xq49wxf8a3m3q8fdgm5f8avqwgl85g")))

(define-public crate-bitcoind-request-0.1.13 (c (n "bitcoind-request") (v "0.1.13") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qxm849xgk2m5j66qc3qkxwbsg2vh3zazk0588d7nmmr2zsc3si3")))

(define-public crate-bitcoind-request-0.1.14 (c (n "bitcoind-request") (v "0.1.14") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z4si1d4z3lx20s6acf21cp0yg0p8b7gfxzajj3jpz6g4yzva4wq")))

(define-public crate-bitcoind-request-0.1.15 (c (n "bitcoind-request") (v "0.1.15") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dsg7wrhghdpzqxsd5npxwyavfh10y1y988vgpv0z0ykmpji7pgx")))

(define-public crate-bitcoind-request-0.1.16 (c (n "bitcoind-request") (v "0.1.16") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r4iwanvdbs438npmpm2z3f0rmrhng02zl75nvffyphcb5dwqg8n")))

(define-public crate-bitcoind-request-0.1.17 (c (n "bitcoind-request") (v "0.1.17") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qxixkiqhahw93i7fsp0rcz9v7h6f7q4my4qkgg1gzaklbp8wci4")))

