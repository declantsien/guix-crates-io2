(define-module (crates-io bi tc bitcoin-time) #:use-module (crates-io))

(define-public crate-bitcoin-time-0.1.10-alpha.0 (c (n "bitcoin-time") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0qfmgpvj40nnjadlly90palwsxjjd59wkbjazk7gwiq710i8zz5w")))

(define-public crate-bitcoin-time-0.1.12-alpha.0 (c (n "bitcoin-time") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1j0hnwsdilvhzclqb9nppg3ilb67wj76pkazvh6r70b8nh33cknn")))

(define-public crate-bitcoin-time-0.1.13-alpha.0 (c (n "bitcoin-time") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "1c8v59cdpjx562x5k16w5dh6y11mnnbngcj69xxj7wl84ivf9qnl")))

(define-public crate-bitcoin-time-0.1.16-alpha.0 (c (n "bitcoin-time") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1r4sn26ixrvxkdn5i0fly38dl2j8ldasykv1xaa7k3k084iprarl")))

