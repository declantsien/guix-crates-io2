(define-module (crates-io bi tc bitcoind-log-parser) #:use-module (crates-io))

(define-public crate-bitcoind-log-parser-0.1.1 (c (n "bitcoind-log-parser") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "logwatcher") (r "^0.1") (d #t) (k 0)))) (h "05qzhq8wvi65rb6zhmpkhnyi6x8vsqrjx1krqnpn5bkh6ahdr6gy")))

(define-public crate-bitcoind-log-parser-0.1.2 (c (n "bitcoind-log-parser") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "logwatcher") (r "^0.1") (d #t) (k 0)))) (h "0spj4sj558xhnm3b5jjrkhclj9kwchgc6vjyrk4lhz0ccmcgli20")))

(define-public crate-bitcoind-log-parser-0.1.3 (c (n "bitcoind-log-parser") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "logwatcher") (r "^0.1") (d #t) (k 0)))) (h "1i2ph8x1p74nz1zc16ap7p365ik7p1inardsg12i3y3c8vq9ajk4")))

