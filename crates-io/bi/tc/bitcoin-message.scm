(define-module (crates-io bi tc bitcoin-message) #:use-module (crates-io))

(define-public crate-bitcoin-message-0.1.12-alpha.0 (c (n "bitcoin-message") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-key") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "18yid0xjpi52pmz763kg6sxq823cwryiswwn3k3iim68aw7zakfz")))

(define-public crate-bitcoin-message-0.1.16-alpha.0 (c (n "bitcoin-message") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-key") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "12lmwv9klzq708ifp938zri6hzpj963n7lnil4v1nh8xdhf9l1lg")))

