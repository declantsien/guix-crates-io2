(define-module (crates-io bi tc bitcoinnode-txrelay) #:use-module (crates-io))

(define-public crate-bitcoinnode-txrelay-0.1.12-alpha.0 (c (n "bitcoinnode-txrelay") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-bloom") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "0rm6xc6mpvdcsp5bsxpsrfbkvawpdw45jczzn36zqnqsl6h6s9ba")))

(define-public crate-bitcoinnode-txrelay-0.1.16-alpha.0 (c (n "bitcoinnode-txrelay") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-bloom") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "0wns27vz3liq5c9lwxwkw84zy587a1zng6aphv7ax7vkj4ghayb0")))

