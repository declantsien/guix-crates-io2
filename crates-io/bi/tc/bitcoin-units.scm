(define-module (crates-io bi tc bitcoin-units) #:use-module (crates-io))

(define-public crate-bitcoin-units-0.0.0 (c (n "bitcoin-units") (v "0.0.0") (h "13y06njzs6fnalc0q9xc2wxz97f2aj7p6525z7dc0j6dg5z1hl8z")))

(define-public crate-bitcoin-units-0.1.0 (c (n "bitcoin-units") (v "0.1.0") (d (list (d (n "internals") (r "^0.3.0") (d #t) (k 0) (p "bitcoin-internals")) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "12z5lhfrn673dcj8j3gqjx67shw6r2r1xrymdxnqc7174zbpyhwd") (f (quote (("std" "alloc" "internals/std") ("default" "std") ("alloc" "internals/alloc")))) (r "1.56.1")))

(define-public crate-bitcoin-units-0.1.1 (c (n "bitcoin-units") (v "0.1.1") (d (list (d (n "internals") (r "^0.3.0") (f (quote ("alloc"))) (d #t) (k 0) (p "bitcoin-internals")) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1x9nw2vmf6v8h1ac2axmyjv53q1k819r2w9s4193qbw9505xlm6b") (f (quote (("std" "alloc" "internals/std") ("default" "std") ("alloc" "internals/alloc")))) (r "1.56.1")))

