(define-module (crates-io bi tc bitcoin-proptest) #:use-module (crates-io))

(define-public crate-bitcoin-proptest-0.0.1-alpha.1 (c (n "bitcoin-proptest") (v "0.0.1-alpha.1") (d (list (d (n "bitcoin") (r "^0.31") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "miniscript") (r "^11.0") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.28") (f (quote ("rand"))) (d #t) (k 0)))) (h "15jgbp9bac8s91xy9cdx5l82ydi18a49jm35b5xgs54s60akrwgz")))

(define-public crate-bitcoin-proptest-0.0.1-alpha.2 (c (n "bitcoin-proptest") (v "0.0.1-alpha.2") (d (list (d (n "bitcoin") (r "^0.31") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "miniscript") (r "^11.0") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.28") (f (quote ("rand"))) (d #t) (k 0)))) (h "09q6w7j7qx041xhgw5ssb4lvz8m77plz6qzrl3jmxyjc1d7ywnvz")))

