(define-module (crates-io bi tc bitcursor) #:use-module (crates-io))

(define-public crate-bitcursor-0.1.0 (c (n "bitcursor") (v "0.1.0") (h "17yvrphsaj8z0r55rjp6vnwr7kw0vvfhm7ncc0vdgmqqcsrkgiw9")))

(define-public crate-bitcursor-1.0.0 (c (n "bitcursor") (v "1.0.0") (h "05nb4fypxijnnapjjdc7xxwq6wi326vsqv4xif3hfl42f9i6r93i")))

