(define-module (crates-io bi tc bitconch-jsonrpc-core) #:use-module (crates-io))

(define-public crate-bitconch-jsonrpc-core-0.1.0 (c (n "bitconch-jsonrpc-core") (v "0.1.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zqhvw4vnb6kiq8yfb1rkk1i5xv0q7gz9dn9w49pjvxgkk7slarl")))

