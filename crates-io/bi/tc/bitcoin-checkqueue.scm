(define-module (crates-io bi tc bitcoin-checkqueue) #:use-module (crates-io))

(define-public crate-bitcoin-checkqueue-0.1.12-alpha.0 (c (n "bitcoin-checkqueue") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "1wzgf7qv3d2vm5z0zmj22qbpanch7arm789ml3x0y6ylx4g1c2ls")))

(define-public crate-bitcoin-checkqueue-0.1.16-alpha.0 (c (n "bitcoin-checkqueue") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "15haddfggwisaxr146n62q4dg3a29d9zmhvqf32pn0rng72sv6g5")))

