(define-module (crates-io bi tc bitcoin-consensus) #:use-module (crates-io))

(define-public crate-bitcoin-consensus-0.1.1 (c (n "bitcoin-consensus") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.4") (d #t) (k 1)))) (h "081hja6xbkfp3xfiscbm4j3gj84ichmxrmk5zx6sqi05frp56b22") (f (quote (("default"))))))

(define-public crate-bitcoin-consensus-0.2.0 (c (n "bitcoin-consensus") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.4") (d #t) (k 1)))) (h "035lpvlgb904b177xlzamjm17drmvfg7pacr57kj6bs914nj0m2p") (f (quote (("default"))))))

(define-public crate-bitcoin-consensus-0.2.1 (c (n "bitcoin-consensus") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.4") (d #t) (k 1)))) (h "0674fli7lbln2lgcminq8kzi719bdi6lg06xxqkvldj2yndfj3wd") (f (quote (("default"))))))

(define-public crate-bitcoin-consensus-0.2.2 (c (n "bitcoin-consensus") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.5") (d #t) (k 1)))) (h "108ybwy9vsvwph9ck5scs40bfx1hg02yg9dpwvlxf50d04hdzra8") (f (quote (("default"))))))

(define-public crate-bitcoin-consensus-0.3.0+0.18.0 (c (n "bitcoin-consensus") (v "0.3.0+0.18.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 1)))) (h "0l13g4ldyn7fnymlc7a9xbzy25ysh2a2vbnz2q9a8zhbfxr37d0a")))

