(define-module (crates-io bi tc bitcoinleveldb-arena) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-arena-0.1.10-alpha.0 (c (n "bitcoinleveldb-arena") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1c3rbahnxjl7vswrp5mbifwpmjzdj7rhfcvijkzbalwly8ias269")))

(define-public crate-bitcoinleveldb-arena-0.1.12-alpha.0 (c (n "bitcoinleveldb-arena") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "167vcikyfr3h17h09vbxwp7z6iil583m9ws86bylx6k4mw77wr8v")))

(define-public crate-bitcoinleveldb-arena-0.1.16-alpha.0 (c (n "bitcoinleveldb-arena") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1xyxl88753chkn0r1kr78svb91p0zaxx2qcb09vaf442rr3av0vx")))

