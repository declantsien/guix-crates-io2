(define-module (crates-io bi tc bitcoinsecp256k1-scratch) #:use-module (crates-io))

(define-public crate-bitcoinsecp256k1-scratch-0.1.10-alpha.0 (c (n "bitcoinsecp256k1-scratch") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0i1fdqn1gkx8hg6sl82cfihc5xxs6bmnfxx123bbjsy11j187pd0")))

(define-public crate-bitcoinsecp256k1-scratch-0.1.12-alpha.0 (c (n "bitcoinsecp256k1-scratch") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0f884wzgcwy6a5lkv8sv6v3hhii59xrrjlx532qwymwpry2pwd8q")))

(define-public crate-bitcoinsecp256k1-scratch-0.1.13-alpha.0 (c (n "bitcoinsecp256k1-scratch") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0f3x5klcgmqp53lk2m487266d0xbhlnh040mjy63g27by3vdyp2n")))

(define-public crate-bitcoinsecp256k1-scratch-0.1.16-alpha.0 (c (n "bitcoinsecp256k1-scratch") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "18pyxinbhh0qgcjv260d9sk7g4dna9q6ydk588c10kkayl0hyfm6")))

