(define-module (crates-io bi tc bitcoint4-private) #:use-module (crates-io))

(define-public crate-bitcoint4-private-0.1.0 (c (n "bitcoint4-private") (v "0.1.0") (h "12jsh101vbpm8sass1kxha31l5xdl3s4lxp7573kc93zaqdd3qky") (f (quote (("std" "alloc") ("default") ("alloc"))))))

(define-public crate-bitcoint4-private-0.1.1 (c (n "bitcoint4-private") (v "0.1.1") (h "1d0xfxashk61nbbvbi3d4bdnv4ka8vhmhbgbp8bl38jr6cw7fmmj") (f (quote (("std" "alloc") ("default") ("alloc"))))))

(define-public crate-bitcoint4-private-0.1.2 (c (n "bitcoint4-private") (v "0.1.2") (h "1srqlz80pbf17306rky5jd1rhcx8lxcv4w6ys4bp0s97ikynhp9n") (f (quote (("std" "alloc") ("default") ("alloc"))))))

