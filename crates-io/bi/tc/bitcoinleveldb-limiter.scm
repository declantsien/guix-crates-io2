(define-module (crates-io bi tc bitcoinleveldb-limiter) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-limiter-0.1.10-alpha.0 (c (n "bitcoinleveldb-limiter") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0z3cj5c17hvsyzar8kpg8dwi4cf5ng00ashy8m27nqc3m66zg15y")))

(define-public crate-bitcoinleveldb-limiter-0.1.12-alpha.0 (c (n "bitcoinleveldb-limiter") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "041p0cj662byn1llhl8271qbzgf1313p43mdfs0giiby6h76g65d")))

(define-public crate-bitcoinleveldb-limiter-0.1.16-alpha.0 (c (n "bitcoinleveldb-limiter") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0m1anbh3cfirqrynbnnjk0w95kkb53rzpqq4h97mqz7panbsdyb2")))

