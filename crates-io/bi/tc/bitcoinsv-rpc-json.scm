(define-module (crates-io bi tc bitcoinsv-rpc-json) #:use-module (crates-io))

(define-public crate-bitcoinsv-rpc-json-0.19.0 (c (n "bitcoinsv-rpc-json") (v "0.19.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitcoinsv") (r "^0.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0f9w8zpf3q5kh4rhinrpks5vzg246hh9vlszy9hfgbfzabvjr8zq")))

(define-public crate-bitcoinsv-rpc-json-0.19.1 (c (n "bitcoinsv-rpc-json") (v "0.19.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitcoinsv") (r "^0.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hr2wafbf5ndpa456dla1fjx4axqslz5p6fp6k0zrfm55im0g9rm")))

(define-public crate-bitcoinsv-rpc-json-0.19.2 (c (n "bitcoinsv-rpc-json") (v "0.19.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitcoinsv") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0dj5ii89i20svl300xvpy9hl4fvb52pym3fjfz07zsfjx1qak8vy")))

(define-public crate-bitcoinsv-rpc-json-0.19.3 (c (n "bitcoinsv-rpc-json") (v "0.19.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitcoinsv") (r "^0.2.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xir5dv5phzi6h95z2k0bll2211r25hld1nrd5c5f1wi2cz1x2ri")))

(define-public crate-bitcoinsv-rpc-json-0.19.4 (c (n "bitcoinsv-rpc-json") (v "0.19.4") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitcoinsv") (r "^0.2.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1p5pb8ksls9cnmj0y18q5194g4drdsfy3p7jdah0anlgj6pdlh5n")))

(define-public crate-bitcoinsv-rpc-json-0.19.5 (c (n "bitcoinsv-rpc-json") (v "0.19.5") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitcoinsv") (r "^0.2.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04wridifqh0an1pxpxn6ch3bi5v0kz5bxdajm89cdcmas7wg59pp")))

(define-public crate-bitcoinsv-rpc-json-0.19.6 (c (n "bitcoinsv-rpc-json") (v "0.19.6") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitcoinsv") (r "^0.2.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1mc73sic25n0davv5qa4xazjz1xggldkibcc9nlaimh015pvl2na")))

