(define-module (crates-io bi tc bitcoin-mem) #:use-module (crates-io))

(define-public crate-bitcoin-mem-0.1.10-alpha.0 (c (n "bitcoin-mem") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0fdwk9ig7rxhlqrym03f3icczi6sq7zip4zsg2wb8rihp8nflyqq")))

(define-public crate-bitcoin-mem-0.1.12-alpha.0 (c (n "bitcoin-mem") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0ny3m2avgzigg22w8krfyp1rq031g8h7z5wa26vg1kd54qcyyrn1")))

(define-public crate-bitcoin-mem-0.1.13-alpha.0 (c (n "bitcoin-mem") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0jl33v41vcy7049abjprp5hr9jiqswgwvbmx7iqyc673m798cbch")))

(define-public crate-bitcoin-mem-0.1.16-alpha.0 (c (n "bitcoin-mem") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1gynd98ddv55mb9n8llcnpl8wyxjwm9nn76jdc71ccyr5wcpm372")))

