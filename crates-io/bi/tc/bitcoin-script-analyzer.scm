(define-module (crates-io bi tc bitcoin-script-analyzer) #:use-module (crates-io))

(define-public crate-bitcoin-script-analyzer-0.1.0 (c (n "bitcoin-script-analyzer") (v "0.1.0") (d (list (d (n "bitcoin_hashes") (r "^0.12.0") (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("formatting"))) (d #t) (k 0)))) (h "1mplqfwjgmkkjkdvmj9gy8mj0vm834i69ihywdmicl63scmq1pxs") (f (quote (("threads"))))))

