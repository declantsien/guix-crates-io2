(define-module (crates-io bi tc bitcoin-sha512) #:use-module (crates-io))

(define-public crate-bitcoin-sha512-0.1.10-alpha.0 (c (n "bitcoin-sha512") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "01mfvnmrh5pvrsgmbs7p2qrv52rw8a742w1lbxxzshndqrjs109y")))

(define-public crate-bitcoin-sha512-0.1.12-alpha.0 (c (n "bitcoin-sha512") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1xqvna17p3p2y94pdii32y88vqqz7lbphpd72krmk7jffy231bnx")))

(define-public crate-bitcoin-sha512-0.1.13-alpha.0 (c (n "bitcoin-sha512") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0vwkc5qzjy50qrav173z8zarlyx57pf2whba1s3qnzvlmjh4f8dv")))

(define-public crate-bitcoin-sha512-0.1.16-alpha.0 (c (n "bitcoin-sha512") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "04rsamw3pgrk73r9hyfas0pzwnmx11ifc1c4g1yvcbra9b162r53")))

