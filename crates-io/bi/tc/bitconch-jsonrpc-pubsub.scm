(define-module (crates-io bi tc bitconch-jsonrpc-pubsub) #:use-module (crates-io))

(define-public crate-bitconch-jsonrpc-pubsub-0.1.0 (c (n "bitconch-jsonrpc-pubsub") (v "0.1.0") (d (list (d (n "bitconch-jsonrpc-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bitconch-jsonrpc-tcp-server") (r "^0.1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "0h4055r0k4njbyc1hk3yhjiigc1f1sfcngpvg59kp3dqm85qdy1m")))

