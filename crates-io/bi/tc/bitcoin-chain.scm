(define-module (crates-io bi tc bitcoin-chain) #:use-module (crates-io))

(define-public crate-bitcoin-chain-0.1.1 (c (n "bitcoin-chain") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.13") (d #t) (k 0)))) (h "154y5hhaghghvym9g8ziyd9d5210g4dakq4cvdnwbh972rz1535i")))

(define-public crate-bitcoin-chain-0.2.0 (c (n "bitcoin-chain") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.14") (d #t) (k 0)))) (h "0h7rkknpxipl6q4g4rv12jqhj5jnq9wax367nl75whdg3hbvg180")))

(define-public crate-bitcoin-chain-0.2.1 (c (n "bitcoin-chain") (v "0.2.1") (d (list (d (n "bitcoin") (r "^0.14") (d #t) (k 0)))) (h "0dbc91gpxl5clix1kka1s6azmrdw5i8z60p0g16q0mm2x7lwhdmr")))

(define-public crate-bitcoin-chain-0.3.1 (c (n "bitcoin-chain") (v "0.3.1") (d (list (d (n "bitcoin") (r "^0.15") (d #t) (k 0)))) (h "1zj42axi5130y30qjz4nrg15f6xdya4sqywb7rd3bzfd3kc7jrcd")))

