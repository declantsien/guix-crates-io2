(define-module (crates-io bi tc bitcoin-client-ui) #:use-module (crates-io))

(define-public crate-bitcoin-client-ui-0.1.12-alpha.0 (c (n "bitcoin-client-ui") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-block") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-string") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0j12arhic9d2chwg9wrd2z2pxymqz1858ba4syjh3vbgip37hb9c")))

(define-public crate-bitcoin-client-ui-0.1.16-alpha.0 (c (n "bitcoin-client-ui") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-block") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-string") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1609zaprk8hwn8c8md9m1zh039x3vym8dmm0lc6z9rxwfqpnj0a1")))

