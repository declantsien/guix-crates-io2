(define-module (crates-io bi tc bitcoinleveldb-coding) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-coding-0.1.10-alpha.0 (c (n "bitcoinleveldb-coding") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "12yl3vzss0yxijyn582qq0dvg3qrk3y7im6mjr60xq2rd94za4hr")))

(define-public crate-bitcoinleveldb-coding-0.1.12-alpha.0 (c (n "bitcoinleveldb-coding") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0rh2zybj0bi9wgl9zig8k9ahrqhzfjj6y7vgmzhm3clhgdkkp0xw")))

(define-public crate-bitcoinleveldb-coding-0.1.16-alpha.0 (c (n "bitcoinleveldb-coding") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "101wcpmzpjjlvmvypwqdry6q8n2b79i1ha8dghlh81rl1mp9ymzq")))

