(define-module (crates-io bi tc bitcoinleveldb-snapshot) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-snapshot-0.1.10-alpha.0 (c (n "bitcoinleveldb-snapshot") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-key") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1aqyim1b9984lw7ikr8zk52bnbzf7yp8jzpf2ip2b9fxfw25w7i6")))

(define-public crate-bitcoinleveldb-snapshot-0.1.12-alpha.0 (c (n "bitcoinleveldb-snapshot") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-key") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0zv1p9k94xqd2i01i4005r4lyhwdmgcgq0zbv6nkhddn4w549q6v")))

(define-public crate-bitcoinleveldb-snapshot-0.1.13-alpha.0 (c (n "bitcoinleveldb-snapshot") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-key") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "01kvrg173afhmyli2d83mcq9583g0yrhs6zhzkvvimpj6v3cm3y9")))

(define-public crate-bitcoinleveldb-snapshot-0.1.16-alpha.0 (c (n "bitcoinleveldb-snapshot") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-key") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1k6p1k0ai53fzzdx7qqiasn33dbjjjxscncj3h86mihddyc7d3ky")))

