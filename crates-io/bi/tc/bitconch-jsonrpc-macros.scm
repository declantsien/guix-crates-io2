(define-module (crates-io bi tc bitconch-jsonrpc-macros) #:use-module (crates-io))

(define-public crate-bitconch-jsonrpc-macros-0.1.0 (c (n "bitconch-jsonrpc-macros") (v "0.1.0") (d (list (d (n "bitconch-jsonrpc-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bitconch-jsonrpc-pubsub") (r "^0.1.0") (d #t) (k 0)) (d (n "bitconch-jsonrpc-tcp-server") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16d6inh7cjf4cricfq8v8w03c9py6dlab82hiwlspfjrr41ivcm1")))

