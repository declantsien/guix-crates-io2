(define-module (crates-io bi tc bitcoin-keymetadata) #:use-module (crates-io))

(define-public crate-bitcoin-keymetadata-0.1.12-alpha.0 (c (n "bitcoin-keymetadata") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-key") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "0wf6a4s2wshjqwfpdjzpl4g8lcvkj54qz03hpl3gxk8dkjvga5r5")))

