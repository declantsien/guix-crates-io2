(define-module (crates-io bi tc bitcoin-address) #:use-module (crates-io))

(define-public crate-bitcoin-address-0.1.0 (c (n "bitcoin-address") (v "0.1.0") (h "0ikjyvmyisjyrq8f473ydwwrz1ls1047wznymw1xdhs0ss237g0z")))

(define-public crate-bitcoin-address-0.1.1 (c (n "bitcoin-address") (v "0.1.1") (h "11w45acfw8h834mbyz79gmxbiba6jr0cxgk96bhlws8c39jjq7r1")))

(define-public crate-bitcoin-address-0.1.2 (c (n "bitcoin-address") (v "0.1.2") (h "1vp73nq0jbcgi46gpq63ia21r8w2hcjp8xsxl5qfvx5xpmfys4bm")))

(define-public crate-bitcoin-address-0.1.3 (c (n "bitcoin-address") (v "0.1.3") (h "1rck95z0c3nia325yzqw7kn79q3p2c6725iy11id6qylcrm9v814")))

(define-public crate-bitcoin-address-0.1.4 (c (n "bitcoin-address") (v "0.1.4") (h "05pwi03lkvzg7hfv288vwmx5jv3avrzadnqacasr2bhzwabwspbi")))

(define-public crate-bitcoin-address-0.1.5 (c (n "bitcoin-address") (v "0.1.5") (h "09zipwa8328lc5x509y8lx79k1y67qvsnnh8yim6gsbw6h97azcb")))

