(define-module (crates-io bi tc bitcoin-amount) #:use-module (crates-io))

(define-public crate-bitcoin-amount-0.1.0 (c (n "bitcoin-amount") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("arbitrary_precision"))) (o #t) (d #t) (k 0)))) (h "19adb0x0kysmxql3d0jcvg3f3g6mg54xj2p7r1bsf7ppgmw7q94s") (f (quote (("serde_json_number" "serde_json") ("serde_impl" "serde"))))))

(define-public crate-bitcoin-amount-0.1.1 (c (n "bitcoin-amount") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("arbitrary_precision"))) (o #t) (d #t) (k 0)))) (h "1kpcfhwnhkm1mxzd6amix7x8dqp989nz6b5jskpdnd5m0iq8l4n7") (f (quote (("serde_json_number" "serde_json") ("serde_impl" "serde"))))))

(define-public crate-bitcoin-amount-0.1.2 (c (n "bitcoin-amount") (v "0.1.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("arbitrary_precision"))) (o #t) (d #t) (k 0)))) (h "0v8b9374bjafyy1sw61x20yqjmjll8a1n993abqqk2chkfkyand8") (f (quote (("serde_json_number" "serde_json"))))))

(define-public crate-bitcoin-amount-0.1.3 (c (n "bitcoin-amount") (v "0.1.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("arbitrary_precision"))) (o #t) (d #t) (k 0)) (d (n "strason") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0asmn7v6ajfd59w0nl7a1994wkdd9zzxvhw09j0q5ax7010cphl7")))

(define-public crate-bitcoin-amount-0.1.4 (c (n "bitcoin-amount") (v "0.1.4") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("arbitrary_precision"))) (o #t) (d #t) (k 0)) (d (n "strason") (r "^0.4") (o #t) (d #t) (k 0)))) (h "17mvg82x686f95kqw40s09mlf7w0cpcb25x9vx7aqgqi29ksxm8b")))

