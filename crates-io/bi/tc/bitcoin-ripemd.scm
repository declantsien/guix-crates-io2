(define-module (crates-io bi tc bitcoin-ripemd) #:use-module (crates-io))

(define-public crate-bitcoin-ripemd-0.1.10-alpha.0 (c (n "bitcoin-ripemd") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0lsnhs1d64dyw0qn83pp3mxlh5j5zsq00w28c74hz5sxqqcpvd0y")))

(define-public crate-bitcoin-ripemd-0.1.12-alpha.0 (c (n "bitcoin-ripemd") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1zs9pqsi020h1i7iq7zp7n99lmxnhi7qp1w1v6vawh0xy5g43yl8")))

(define-public crate-bitcoin-ripemd-0.1.13-alpha.0 (c (n "bitcoin-ripemd") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0wkd5ar1b4nzrnga3j4c2smcz5a2vwjn1gigifikkpc56xa7r5jh")))

(define-public crate-bitcoin-ripemd-0.1.16-alpha.0 (c (n "bitcoin-ripemd") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "1iaqx8mvj2bbbmgxz9vhz6sypyw39ksm00b09jq4y3c94gn9inin")))

