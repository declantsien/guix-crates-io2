(define-module (crates-io bi tc bitcoin-univalue) #:use-module (crates-io))

(define-public crate-bitcoin-univalue-0.1.10-alpha.0 (c (n "bitcoin-univalue") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1chpikflciva69q80shy02drimy8jxzp1dcph1whvfm6lm6bn7vb")))

(define-public crate-bitcoin-univalue-0.1.12-alpha.0 (c (n "bitcoin-univalue") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0v0virw48d8lzlnawmc3ylr2zxb9picam7qw9ps4n9nk6ahbz2c3")))

(define-public crate-bitcoin-univalue-0.1.13-alpha.0 (c (n "bitcoin-univalue") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "0r9swbv0bbl2175gy5nanc2d0spvyvnn2f6h0nmlksvn9hlp1k0g")))

(define-public crate-bitcoin-univalue-0.1.16-alpha.0 (c (n "bitcoin-univalue") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0lbcsbddii1ang6zrqd5lwi7hwakzyzlfh9qjy64brazgm7yf8fp")))

