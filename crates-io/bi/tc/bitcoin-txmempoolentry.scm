(define-module (crates-io bi tc bitcoin-txmempoolentry) #:use-module (crates-io))

(define-public crate-bitcoin-txmempoolentry-0.1.12-alpha.0 (c (n "bitcoin-txmempoolentry") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-block") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-epoch") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-tx") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "1i5rf12yp10d5b486pavhahrwspydj59qpyrax9plwy1n7w1q9f6")))

(define-public crate-bitcoin-txmempoolentry-0.1.16-alpha.0 (c (n "bitcoin-txmempoolentry") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-amt") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-block") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-epoch") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-tx") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "0n7d2s35klyf3vcfriyqx8zxrmx4lwhkfcxghnkzwwp69v6h479v")))

