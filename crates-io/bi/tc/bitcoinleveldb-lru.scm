(define-module (crates-io bi tc bitcoinleveldb-lru) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-lru-0.1.12-alpha.0 (c (n "bitcoinleveldb-lru") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-cache") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0kn43ka58m58xz7b8n6k74mjsv7v3gph3nd2gwhqhjcnvn6gdnlb")))

(define-public crate-bitcoinleveldb-lru-0.1.16-alpha.0 (c (n "bitcoinleveldb-lru") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-cache") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoinleveldb-slice") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0bav3vf519cxma7h0nfy7nh81fxdsynahwsj5cga3afg8snzzhzm")))

