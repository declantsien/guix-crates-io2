(define-module (crates-io bi tc bitcoinleveldb-compat) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-compat-0.1.10-alpha.0 (c (n "bitcoinleveldb-compat") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "090ilcbri5pdig48fpfahvh89bi9fjmyblf2yb0dzmsrpayn4m44")))

(define-public crate-bitcoinleveldb-compat-0.1.12-alpha.0 (c (n "bitcoinleveldb-compat") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "1q68vr6afc9yz4i5wxh7i7di9450g4d9rjzmzd7gb93kgn94i6b0")))

(define-public crate-bitcoinleveldb-compat-0.1.16-alpha.0 (c (n "bitcoinleveldb-compat") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0lj9n6ffvi56418c88vs6q7lnm37sn4xqi8izzdwmf13ph4vj53l")))

