(define-module (crates-io bi tc bitcoinleveldb-rand) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-rand-0.1.10-alpha.0 (c (n "bitcoinleveldb-rand") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1f1i4ayiyc940g5wjhzjk3vdhskn80dddriyi3s6654mkllqx0m0")))

(define-public crate-bitcoinleveldb-rand-0.1.12-alpha.0 (c (n "bitcoinleveldb-rand") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0q9nplg6mhps18af447fpip2zwq4agsvkbvfc8pzlil8d5q5xm4p")))

(define-public crate-bitcoinleveldb-rand-0.1.13-alpha.0 (c (n "bitcoinleveldb-rand") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "117wn8l44jpd3ikzdzpf0wazpj044r947v6133jc54hw81kp1723")))

(define-public crate-bitcoinleveldb-rand-0.1.16-alpha.0 (c (n "bitcoinleveldb-rand") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "184s9zc2kdmhzzqxfb0m9gj7rqn704w26qz0dadh31in8msy576j")))

