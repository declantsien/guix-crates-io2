(define-module (crates-io bi tc bitcoinleveldb-sync) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-sync-0.1.12-alpha.0 (c (n "bitcoinleveldb-sync") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0j85q0fym4p932ma0171d44b8wb51mfnwxzw8j22jxqbbjb59p32")))

(define-public crate-bitcoinleveldb-sync-0.1.16-alpha.0 (c (n "bitcoinleveldb-sync") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0hk67hplg5wm1yljn493y4bfydwvx5208ax141yrccng8sh5adw0")))

