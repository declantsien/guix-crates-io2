(define-module (crates-io bi tc bitcoin-parser) #:use-module (crates-io))

(define-public crate-bitcoin-parser-0.1.0 (c (n "bitcoin-parser") (v "0.1.0") (d (list (d (n "arrayref") (r "~0.3") (d #t) (k 0)) (d (n "byteorder") (r "~1.2") (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "1rndqdszkza8ail3j3rsqpbvzn0sm761p4aj1l9c4di0636lj2pv")))

