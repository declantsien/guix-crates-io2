(define-module (crates-io bi tc bitcoincore-zmqsequence) #:use-module (crates-io))

(define-public crate-bitcoincore-zmqsequence-0.1.0 (c (n "bitcoincore-zmqsequence") (v "0.1.0") (d (list (d (n "bitcoincore-rpc") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "1q21mdmr8wckbjdvvbcdawrmicwqvwpy2wcf917ka7nfvb2y99l0") (s 2) (e (quote (("check_node" "dep:bitcoincore-rpc"))))))

