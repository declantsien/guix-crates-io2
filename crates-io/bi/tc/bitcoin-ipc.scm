(define-module (crates-io bi tc bitcoin-ipc) #:use-module (crates-io))

(define-public crate-bitcoin-ipc-0.1.12-alpha.0 (c (n "bitcoin-ipc") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-index") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "0b9fk839w0ja4x0zdbk66p57ihbfsrg51g4n9s7gsmy8ksgs6j71")))

(define-public crate-bitcoin-ipc-0.1.16-alpha.0 (c (n "bitcoin-ipc") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-index") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-primitives") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0bhnpznkm2zhkgd5cx1d7bcxvb92k5gg441800w9j2v0g8n0h66b")))

