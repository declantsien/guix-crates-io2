(define-module (crates-io bi tc bitcoinsecp256k1-modinv) #:use-module (crates-io))

(define-public crate-bitcoinsecp256k1-modinv-0.1.10-alpha.0 (c (n "bitcoinsecp256k1-modinv") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "0fkg75cl4r09gpknkzah11yxznj3rm22nzhc1ylyj8pg7ikfyd5i")))

(define-public crate-bitcoinsecp256k1-modinv-0.1.12-alpha.0 (c (n "bitcoinsecp256k1-modinv") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "06577vp6dwxq35q45zzhqsp4whcff6fmxjrjd4symv8sh11chaqr")))

(define-public crate-bitcoinsecp256k1-modinv-0.1.13-alpha.0 (c (n "bitcoinsecp256k1-modinv") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.13-alpha.0") (d #t) (k 1)))) (h "1rbhgszflxf4g0843avgis6vhx49b709gxd802n3zl3ywvjg4386")))

(define-public crate-bitcoinsecp256k1-modinv-0.1.16-alpha.0 (c (n "bitcoinsecp256k1-modinv") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "06vxakzknilr0z6q4bd3f3yravwza2nz4q4cjfnmvvj98bc58bpj")))

