(define-module (crates-io bi tc bitcoin-asmap) #:use-module (crates-io))

(define-public crate-bitcoin-asmap-0.1.12-alpha.0 (c (n "bitcoin-asmap") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "014rqb487qw4lwxak1xh77pciy3njz2xavmscqsq1j9v3g8w2xby")))

(define-public crate-bitcoin-asmap-0.1.16-alpha.0 (c (n "bitcoin-asmap") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "1s0n5q2jf5n2qcw2v6kiqcljhsa3j92qgckbfs4272g78hfyysvd")))

