(define-module (crates-io bi tc bitcask-engine-rs) #:use-module (crates-io))

(define-public crate-bitcask-engine-rs-0.1.0 (c (n "bitcask-engine-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0glp2pb7i33iq8xhixsbqah7iw30rr09bzyh33979rq5k71d5bh3")))

