(define-module (crates-io bi tc bitcoinleveldb-histogram) #:use-module (crates-io))

(define-public crate-bitcoinleveldb-histogram-0.1.10-alpha.0 (c (n "bitcoinleveldb-histogram") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.10-alpha.0") (d #t) (k 1)))) (h "1jq995znpp3jy0k3fv94mlkw2m03x03khz9djjapll46yzf6r14x")))

(define-public crate-bitcoinleveldb-histogram-0.1.12-alpha.0 (c (n "bitcoinleveldb-histogram") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.12-alpha.0") (d #t) (k 1)))) (h "12xm2p950c7gkahds9mw8fwihzdjvrsdy05pkz2z86s77jy84whv")))

(define-public crate-bitcoinleveldb-histogram-0.1.16-alpha.0 (c (n "bitcoinleveldb-histogram") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-derive") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "bitcoin-cfg") (r "^0.1.16-alpha.0") (d #t) (k 1)))) (h "0hlldn56wb6sn0lqsx5953rmr0khkd1zsi89y2cmwb290mplkvy3")))

