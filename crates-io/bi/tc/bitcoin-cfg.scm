(define-module (crates-io bi tc bitcoin-cfg) #:use-module (crates-io))

(define-public crate-bitcoin-cfg-0.1.6-alpha.0 (c (n "bitcoin-cfg") (v "0.1.6-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.6-alpha.0") (d #t) (k 0)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "1z9j36wv8wfxbpr4dc1manlsx302jzysnyaz3raiccib7i768b11")))

(define-public crate-bitcoin-cfg-0.1.7-alpha.0 (c (n "bitcoin-cfg") (v "0.1.7-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.7-alpha.0") (d #t) (k 0)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "00m4gzr3rrr5ii3j2gxl7rj0x8adlagni4aj8kp18b096y9q7jpw")))

(define-public crate-bitcoin-cfg-0.1.10-alpha.0 (c (n "bitcoin-cfg") (v "0.1.10-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.10-alpha.0") (d #t) (k 0)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "0qlf4s71n597by2zchbs9zw64ldbad4x3y9qvrvw5b1zi0pgr2kd")))

(define-public crate-bitcoin-cfg-0.1.11-alpha.0 (c (n "bitcoin-cfg") (v "0.1.11-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.11-alpha.0") (d #t) (k 0)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "0i9a3z2zlpa6pndfdwcwnir24fbfymi1vn33kdx5v6lrpm2c1y3h")))

(define-public crate-bitcoin-cfg-0.1.12-alpha.0 (c (n "bitcoin-cfg") (v "0.1.12-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "1n808sylqn5bym2i66c4s944yqnk94izj7aysbbfh3lh4j88p7rq")))

(define-public crate-bitcoin-cfg-0.1.13-alpha.0 (c (n "bitcoin-cfg") (v "0.1.13-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "043nf9dl45cvmv67r4rbica7wljr63ismkjsl6gjd8bviby5xl0g")))

(define-public crate-bitcoin-cfg-0.1.15-alpha.0 (c (n "bitcoin-cfg") (v "0.1.15-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.15-alpha.0") (d #t) (k 0)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "0qnm9mi1vy4dq1kji9a5knpx5zgj7npgn5f7pfid06xw0aq31mdl")))

(define-public crate-bitcoin-cfg-0.1.16-alpha.0 (c (n "bitcoin-cfg") (v "0.1.16-alpha.0") (d (list (d (n "bitcoin-imports") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "cfg_aliases") (r "^0.1.0") (d #t) (k 0)))) (h "1g987lk39vqjnk4mnxjn1h9z9qx5i51aip53bdvghm7dip140z6l")))

