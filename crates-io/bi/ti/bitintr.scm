(define-module (crates-io bi ti bitintr) #:use-module (crates-io))

(define-public crate-bitintr-0.1.0 (c (n "bitintr") (v "0.1.0") (h "1dcc7khpxfn6iicxwf7bspxf211wda5jc19585qvc80f98nh9wxm")))

(define-public crate-bitintr-0.1.1 (c (n "bitintr") (v "0.1.1") (h "01r9wqafpn5j5azywkgkz21lymf6xq6k8xakcgxk6077s05c92k1")))

(define-public crate-bitintr-0.1.2 (c (n "bitintr") (v "0.1.2") (d (list (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "07x8nmcxfx89cqrsifay5vpq5ahczl7lw8v4bd2qq7p3clcjrnjn")))

(define-public crate-bitintr-0.1.3 (c (n "bitintr") (v "0.1.3") (d (list (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "09p7fnn6cva76cxsypfcmdfckm97hcnd9sifzvd8nr4nbczvf7vh")))

(define-public crate-bitintr-0.1.4 (c (n "bitintr") (v "0.1.4") (d (list (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "0xpnqkdx9gibv8l61y8rf3nky09mlzm5s4r8if7zjyqf6sir5kvl")))

(define-public crate-bitintr-0.1.5 (c (n "bitintr") (v "0.1.5") (d (list (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "06azn0k8pzaxd5crkwf5ryn8v52mmr5mv3fyrx8l33y5z7j8zsl6")))

(define-public crate-bitintr-0.1.6 (c (n "bitintr") (v "0.1.6") (d (list (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "1n091asmrzb7ib0csq18agm1hl0lf8a41yi3qvjsiq045svzkyx0")))

(define-public crate-bitintr-0.1.7 (c (n "bitintr") (v "0.1.7") (d (list (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "1981xfv8g8kld6ff29wxg801hddssshgsmxp6i1s73a5aqakik08")))

(define-public crate-bitintr-0.1.8 (c (n "bitintr") (v "0.1.8") (d (list (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "137jx4nq4gbvphpb9jwrd80wd206zsg1nnvy7rljk46n8bb2d83a")))

(define-public crate-bitintr-0.1.9 (c (n "bitintr") (v "0.1.9") (d (list (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "0c1j86rhr40c1ihzqn8hlq1gkd4d8iqwrhhgj3k4pv30nbxcgb06")))

(define-public crate-bitintr-0.1.10 (c (n "bitintr") (v "0.1.10") (d (list (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "0y5q0q32fs3yhzwv5gj264c3z3hz0niiiiwl3a8myhw41r7vvq23")))

(define-public crate-bitintr-0.1.11 (c (n "bitintr") (v "0.1.11") (d (list (d (n "bencher") (r "0.1.*") (d #t) (k 2)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "1ky9mka8gla4f1nd58w883p92gl6yz6aqrzyyxy1vlch7ya0ah8k")))

(define-public crate-bitintr-0.1.12 (c (n "bitintr") (v "0.1.12") (d (list (d (n "bencher") (r "0.1.*") (d #t) (k 2)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "0hhy9r8jybb51jqmylblhmhyshn86xz2z4y2c23lnfinhrmj65q9")))

(define-public crate-bitintr-0.1.13 (c (n "bitintr") (v "0.1.13") (d (list (d (n "bencher") (r "0.1.*") (d #t) (k 2)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "01j0v94knxczq0akgyy5j4xlv2kd17i96c6g5437k9ibnrpc6bq1")))

(define-public crate-bitintr-0.1.14 (c (n "bitintr") (v "0.1.14") (d (list (d (n "bencher") (r "0.1.*") (d #t) (k 2)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "1cqfml5mhj66fqpib1rk0j50pv3g9s51f59r7y56iilgai84w8wq")))

(define-public crate-bitintr-0.1.15 (c (n "bitintr") (v "0.1.15") (d (list (d (n "bencher") (r "0.1.*") (d #t) (k 2)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "1n15qk45h3fair0ghx5qix49swxy376a89mj0cpzgfvwjy9mgg36")))

(define-public crate-bitintr-0.1.16 (c (n "bitintr") (v "0.1.16") (d (list (d (n "bencher") (r "0.1.*") (d #t) (k 2)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "053vzhf32nbv04hp0qr88qfwba47nv6s52r6bbcmhm77ka320j6p")))

(define-public crate-bitintr-0.1.17 (c (n "bitintr") (v "0.1.17") (d (list (d (n "bencher") (r "0.1.*") (d #t) (k 2)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "0njwapbi4byijdwkdscahh7f5lhmnadggs04k2iplwqxkdllzw2k")))

(define-public crate-bitintr-0.1.18 (c (n "bitintr") (v "0.1.18") (d (list (d (n "bencher") (r "0.1.*") (d #t) (k 2)) (d (n "rustc_version") (r "0.1.*") (d #t) (k 1)))) (h "00r75phz87l8vjkjb7f6cpiil398gf1ahsamrd75cw6mpihigrj1")))

(define-public crate-bitintr-0.2.0 (c (n "bitintr") (v "0.2.0") (h "0id1rdbns1rmpv6aqj9v25ssfc8yzhizpfrl85av81zgjm27a37q") (f (quote (("unstable"))))))

(define-public crate-bitintr-0.3.0 (c (n "bitintr") (v "0.3.0") (h "1pbkn59pb6kbyvmianidybbm6f6fy4z493v948zngj4avz2ab9bv")))

