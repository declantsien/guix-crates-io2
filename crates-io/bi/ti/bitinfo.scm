(define-module (crates-io bi ti bitinfo) #:use-module (crates-io))

(define-public crate-bitinfo-0.1.0 (c (n "bitinfo") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "parse_int") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "010xpiqj3ym6k73mnw25xkk7v2m7f4r99dp7khq9v658pcg5s9ya")))

(define-public crate-bitinfo-0.1.1 (c (n "bitinfo") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "parse_int") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "10jrcdv1pqnfl7dn8nhbzc2z2p7akn1g9lx19alhj6fg93ylir1i")))

