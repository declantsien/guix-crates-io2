(define-module (crates-io bi ti bitint-macros) #:use-module (crates-io))

(define-public crate-bitint-macros-0.1.0 (c (n "bitint-macros") (v "0.1.0") (d (list (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1alpmnpkcmi1ayv19cwvcc60c5y08qqm7yax52ryvvysjrzrvzwq")))

(define-public crate-bitint-macros-0.1.1 (c (n "bitint-macros") (v "0.1.1") (d (list (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1jrg6bscxa30pqg7b6fyc00by41w5aa55jdxrr44czdh9f1zl2pr")))

