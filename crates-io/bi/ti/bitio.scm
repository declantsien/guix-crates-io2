(define-module (crates-io bi ti bitio) #:use-module (crates-io))

(define-public crate-bitio-0.0.1-alpha (c (n "bitio") (v "0.0.1-alpha") (h "0a67x1kjpwpmhhag1dn351v9f5hyxc7islvgfj6yf8krvixvfzyx")))

(define-public crate-bitio-0.0.1 (c (n "bitio") (v "0.0.1") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1b3gri60aywggiv4lcaby644z0lldp3148aq1yklzxd357dpqhrb")))

(define-public crate-bitio-0.0.2 (c (n "bitio") (v "0.0.2") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "1xlw5psv7rfzabm5d8ncjw1jm9iapb31qwjps06wkzjdnaw9gb1x")))

(define-public crate-bitio-0.0.3 (c (n "bitio") (v "0.0.3") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "064pgml45n6b0dav8jbxwzfbc6hz3dp8631if466rbjvsa4v5zzk")))

(define-public crate-bitio-0.0.4 (c (n "bitio") (v "0.0.4") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "1g9z1csn1v18h5czh8wsd4hzp83jnlrzlccz2mkirpdm3la1avn7")))

