(define-module (crates-io bi ti bitint) #:use-module (crates-io))

(define-public crate-bitint-0.0.0 (c (n "bitint") (v "0.0.0") (h "1vgrdqhpg1gmxjykrqq0xhga5i9ys19yznjg6lav1s791a962zha")))

(define-public crate-bitint-0.1.0 (c (n "bitint") (v "0.1.0") (d (list (d (n "assume") (r "^0.5") (d #t) (k 0)) (d (n "bitint-macros") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0y3rf81kqwy49p921x7cnlf45z7pnx5r2pap3gjlqh4dspck1dc0") (f (quote (("unchecked_math") ("_trybuild_tests") ("_nightly"))))))

(define-public crate-bitint-0.1.1 (c (n "bitint") (v "0.1.1") (d (list (d (n "assume") (r "^0.5") (d #t) (k 0)) (d (n "bitint-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0ppp5z4xbq9fiizr4css1g7n42f1xlzcf1cny5h6kzkykxkm3bym") (f (quote (("unchecked_math") ("_trybuild_tests") ("_nightly"))))))

