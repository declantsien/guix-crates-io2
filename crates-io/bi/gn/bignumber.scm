(define-module (crates-io bi gn bignumber) #:use-module (crates-io))

(define-public crate-bignumber-0.0.1 (c (n "bignumber") (v "0.0.1") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)))) (h "0gj1kafkgy61pjdxc3qvb5ck6h7hib1fmnma2wadvdg4h0s96irn") (r "1.61")))

(define-public crate-bignumber-0.0.2 (c (n "bignumber") (v "0.0.2") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)))) (h "05b892ya30n85cyygd3ihmbq0bc10mwsrqc9jy82p6im10g0r6qy") (r "1.61")))

(define-public crate-bignumber-0.0.3 (c (n "bignumber") (v "0.0.3") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)))) (h "1fwyzzdsfchi9xpzm5pxdqvq7rnkajyfngwaghj0claf14v2ppyv") (r "1.61")))

(define-public crate-bignumber-0.0.4 (c (n "bignumber") (v "0.0.4") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.13.1") (k 0)))) (h "0andn0vws6bw11akyc5f04l3vj5sgcwizyhsl4ymar9kmw5xs208") (r "1.61")))

(define-public crate-bignumber-0.0.5 (c (n "bignumber") (v "0.0.5") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.13.1") (k 0)))) (h "1jnfshiw2w46n1fhxyajmrhl0rbz5djicf2vwh97mbycr5mv03w4") (r "1.61")))

(define-public crate-bignumber-0.0.6 (c (n "bignumber") (v "0.0.6") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.13.1") (k 0)))) (h "1j9dnnawqh4j0zyix49xpplq3kk97iv5jbdpjh285xcfyjp1dzm9") (r "1.61")))

(define-public crate-bignumber-0.0.7 (c (n "bignumber") (v "0.0.7") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.13.1") (k 0)))) (h "0pig0gsw5s9jx8ap9q8qg1azlmvvqknff8fg1rbflbwm703a6lgr") (r "1.61")))

(define-public crate-bignumber-0.0.9 (c (n "bignumber") (v "0.0.9") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)))) (h "0g56hrj0icwjb6yr0bl2c6r13qm5d27cf0l85clnf74jrfqmi1kg") (r "1.61")))

(define-public crate-bignumber-0.0.10 (c (n "bignumber") (v "0.0.10") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.14.0") (d #t) (k 0)) (d (n "ethers") (r "^1.0.0") (o #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "11k263pp0fgaf23bfg6iyvd2458ms1g6lpsdzl92iay31d2j0j6f") (f (quote (("default")))) (s 2) (e (quote (("ethers" "dep:ethers")))) (r "1.61")))

(define-public crate-bignumber-0.0.11 (c (n "bignumber") (v "0.0.11") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.14.0") (d #t) (k 0)) (d (n "ethers") (r "^1.0.0") (o #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "1yg83zb8mdpfhf2gqgpf4vh535582jkxj0883cy41y16yyspkxvr") (f (quote (("default")))) (s 2) (e (quote (("ethers" "dep:ethers")))) (r "1.61")))

(define-public crate-bignumber-0.0.12 (c (n "bignumber") (v "0.0.12") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.14.0") (d #t) (k 0)) (d (n "ethers") (r "^1.0.0") (o #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "0ncfm3w2ij36qcmvn67xqjgzs23yb1fxr5as0i2qkdfnl6g4qh74") (f (quote (("precision-512") ("precision-256") ("precision-1024") ("default")))) (s 2) (e (quote (("ethers" "dep:ethers")))) (r "1.61")))

(define-public crate-bignumber-0.0.13 (c (n "bignumber") (v "0.0.13") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.14.0") (d #t) (k 0)) (d (n "ethers") (r "^1.0.0") (o #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "1zw7yf9p5csj5dz9al97b8kn3c9nb95lf1mysjp17nnijam7pv08") (f (quote (("precision-512") ("precision-256") ("precision-1024") ("default")))) (s 2) (e (quote (("ethers" "dep:ethers")))) (r "1.61")))

(define-public crate-bignumber-0.0.14 (c (n "bignumber") (v "0.0.14") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.14.0") (d #t) (k 0)) (d (n "ethers") (r "^1.0.0") (o #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "1sb2h7lrmrqzkw7yqmggyzwv6pwgak4ahq3w46xkw7av97v8lq8j") (f (quote (("precision-512") ("precision-256") ("precision-1024") ("default")))) (s 2) (e (quote (("ethers" "dep:ethers")))) (r "1.61")))

(define-public crate-bignumber-0.0.15 (c (n "bignumber") (v "0.0.15") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.14.0") (d #t) (k 0)) (d (n "ethers") (r "^1.0.0") (o #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "0znxlf8zz123cplkdjb81yq3yxj121j6k3s0997rq80l4x3a59my") (f (quote (("precision-512") ("precision-256") ("precision-1024") ("default")))) (s 2) (e (quote (("ethers" "dep:ethers")))) (r "1.61")))

(define-public crate-bignumber-0.0.16 (c (n "bignumber") (v "0.0.16") (d (list (d (n "dashu-float") (r "^0.2.1") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.14.0") (d #t) (k 0)) (d (n "ethers") (r "^1.0.0") (o #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "0x0adazfxb0bs63cr8snkf5fc6f4w9qxbp7ii5jxrwb1bcazrcdq") (f (quote (("precision-512") ("precision-256") ("precision-1024") ("default")))) (s 2) (e (quote (("ethers" "dep:ethers")))) (r "1.61")))

(define-public crate-bignumber-0.1.0 (c (n "bignumber") (v "0.1.0") (d (list (d (n "dashu-float") (r "^0.4.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.14.0") (d #t) (k 0)) (d (n "ethers") (r "^2.0.11") (o #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "05p7xyiiy22yhdk72bpd0j1vqh60jszswwi3g8cv0h4bqn3byclm") (f (quote (("precision-512") ("precision-256") ("precision-1024") ("default")))) (s 2) (e (quote (("ethers" "dep:ethers")))) (r "1.61")))

(define-public crate-bignumber-0.1.1 (c (n "bignumber") (v "0.1.1") (d (list (d (n "dashu-float") (r "^0.4.0") (d #t) (k 0)) (d (n "primitive-types") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)))) (h "0p1ax9jx5f20ig8m49pvg6f5z9fr7a3hhw0lhikals7bbsmxw41h") (f (quote (("precision-512") ("precision-256") ("precision-1024") ("default")))) (r "1.61")))

