(define-module (crates-io bi ji biji-ui) #:use-module (crates-io))

(define-public crate-biji-ui-0.1.0 (c (n "biji-ui") (v "0.1.0") (d (list (d (n "leptos") (r "^0.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.92") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (d #t) (k 0)))) (h "0xl60j76sg538p2bvmi6b4haw0d53r4dj2sjd6gq5i634znwk3wv")))

(define-public crate-biji-ui-0.1.1 (c (n "biji-ui") (v "0.1.1") (d (list (d (n "leptos") (r "^0.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.92") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (d #t) (k 0)))) (h "1pgr7zr25i2g6falx8lv16z74dq7hmhi46hzyf33xbm6wkkqglpf")))

(define-public crate-biji-ui-0.1.2 (c (n "biji-ui") (v "0.1.2") (d (list (d (n "leptos") (r "^0.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.10") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.69") (d #t) (k 0)))) (h "0wd844ayi2bzsa766pas7x3x1mvxxcr87cz61dprvxjw58lpjf9s")))

