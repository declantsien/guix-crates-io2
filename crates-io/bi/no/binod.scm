(define-module (crates-io bi no binod) #:use-module (crates-io))

(define-public crate-binod-0.1.0 (c (n "binod") (v "0.1.0") (h "0qlkwmdr8mr4ghvy5sk838dbnzbni3qdaz8cywhxvn0jpgbn512h")))

(define-public crate-binod-0.1.1 (c (n "binod") (v "0.1.1") (h "14c4ak1hv5ay8cqhwpa50pjhhjk2v8gbpfr8a2k90rg4w67vfyff")))

(define-public crate-binod-0.1.2 (c (n "binod") (v "0.1.2") (h "1zcbp8hxdg06zaasshrybaphi5zmdxn15p6fzpbcx2jnh2c94ppx")))

