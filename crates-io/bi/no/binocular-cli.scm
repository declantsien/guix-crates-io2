(define-module (crates-io bi no binocular-cli) #:use-module (crates-io))

(define-public crate-binocular-cli-0.1.0 (c (n "binocular-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nkrq3qsa7cq0j2q3id87bm72r3czpylzzfh660gng74njbjqq43") (y #t)))

(define-public crate-binocular-cli-0.1.1 (c (n "binocular-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wffpbsncbiv0fhz5yypfw2agyc31qpj4crprf8imb1gcd8mizy2")))

