(define-module (crates-io bi no binomial-heap) #:use-module (crates-io))

(define-public crate-binomial-heap-0.1.0 (c (n "binomial-heap") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0q2y437b41wjd6cdzwznmxg3npg6fi2954hdfsnmwaadz5fkxl39") (f (quote (("binary_heap_extras"))))))

(define-public crate-binomial-heap-0.2.0 (c (n "binomial-heap") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "18k1k53zl6kkapg1i871wqm7biwp1x42209365mp735y2pn7x9j4") (f (quote (("binary_heap_extras"))))))

