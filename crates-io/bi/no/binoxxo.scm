(define-module (crates-io bi no binoxxo) #:use-module (crates-io))

(define-public crate-binoxxo-0.1.0 (c (n "binoxxo") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1r43pkgm8k5qa2mgjs67vlp9apaf677l41knyjsych5rci3g47ds")))

(define-public crate-binoxxo-0.1.1 (c (n "binoxxo") (v "0.1.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0b6rd8gmfg8c6a5zz62nk4afclzxm1d614jfw3qyh2m6v1j3q8rc")))

(define-public crate-binoxxo-0.1.2 (c (n "binoxxo") (v "0.1.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1h3wk9cyc1y8f523xb58dmyjl3kfaj5scm6qkp0l7wn3hxkppv60")))

(define-public crate-binoxxo-0.1.3 (c (n "binoxxo") (v "0.1.3") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "161ws9fvzlyq2scj7bh7rzl1hy7mbw7jj9pdx5h6hn4hibdcyl79")))

(define-public crate-binoxxo-0.1.4 (c (n "binoxxo") (v "0.1.4") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0n4vgslayr59lyj6v97mi701nhgvyar163dcq2ylqry0wyc6nmqb")))

(define-public crate-binoxxo-0.1.5 (c (n "binoxxo") (v "0.1.5") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0si6zqjzgha9165sqb2ljq66xdgb665wmnmgprza10bkw33gsj1a") (f (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.1.6 (c (n "binoxxo") (v "0.1.6") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1nd861djsl3li14mf4dx6ngi0hc1885fr6y17zail4yyad09m6h4") (f (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.1.7 (c (n "binoxxo") (v "0.1.7") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "167hgga6gbsy80wghfyw3qk705g6j226m82z1zfbzzl5bhckgzgn") (f (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.1.8 (c (n "binoxxo") (v "0.1.8") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1ag6gpcsxwhy5lk04fpinbcxv10lnrj1mvb4grn7m9gm89jgi1vm") (f (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.2.0 (c (n "binoxxo") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0gsnraxvzrfg6fq8wd13503mbipbv5vsg5p9kxlzhc5w09hmjixl") (f (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.2.1 (c (n "binoxxo") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "09j1lxw7b38jjh2jg10cmbimkwbwlff7yb7321pv48s3ql5pzilg") (f (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.3.0 (c (n "binoxxo") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "09vm6k6ip3ah02hwva2sl6ivi0kh13chigx6b4ccljhxrdj2apa6") (f (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.4.0 (c (n "binoxxo") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0lh2gczp8zppxqc9zf8qb2fqcyjqd0xvnmq28wy2lmg972sgylhx") (f (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.5.0 (c (n "binoxxo") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1kibn51mqv5026nd565lqqkb9lq38x1bp6cz9k8vclnmdgaalb93")))

