(define-module (crates-io bi no binomial_tree) #:use-module (crates-io))

(define-public crate-binomial_tree-0.1.0 (c (n "binomial_tree") (v "0.1.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.2.3") (d #t) (k 2)))) (h "048q0x2kjkg3m7x6q6kcwc5qcbivs7xnvdm8q1spw008h8354p01")))

(define-public crate-binomial_tree-0.2.0 (c (n "binomial_tree") (v "0.2.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.2.3") (d #t) (k 2)))) (h "1chdwrsn42g1959gixck6vxgx34cc21nd67240lwp7yg5kw6d7la")))

(define-public crate-binomial_tree-0.3.0 (c (n "binomial_tree") (v "0.3.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.2.3") (d #t) (k 2)))) (h "1frw4nqmxq19s33v5daf8b1y6212w3qi3ig1m7a6i40hmhr9hxy6")))

(define-public crate-binomial_tree-0.3.1 (c (n "binomial_tree") (v "0.3.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.2.3") (d #t) (k 2)))) (h "0irln3a69caxgv0m0img3zvwfqkqs36f1fw50js4p291a6x8kafr")))

(define-public crate-binomial_tree-0.3.2 (c (n "binomial_tree") (v "0.3.2") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.2.3") (d #t) (k 2)))) (h "1mw8lkb5k9l2jm41gbh7r270wia5ywzqnhibrlliqksl1ypp98yv")))

(define-public crate-binomial_tree-0.3.3 (c (n "binomial_tree") (v "0.3.3") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.2.3") (d #t) (k 2)))) (h "1mrz5pj8sm9w5z1q655f05zhlxdgxyzpb4gbrzg11gkdc607k16p")))

(define-public crate-binomial_tree-0.3.4 (c (n "binomial_tree") (v "0.3.4") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.2.3") (d #t) (k 2)))) (h "141jdyl55gg93vcln1q5xdjrn52jdw2khw4739ifzya10r4s7mxf")))

(define-public crate-binomial_tree-0.3.5 (c (n "binomial_tree") (v "0.3.5") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.2.3") (d #t) (k 2)))) (h "1faf030hlkayyjb9dmsryygg3x7zvca1l3ff0diqwwwbk79v3pw3")))

(define-public crate-binomial_tree-0.3.6 (c (n "binomial_tree") (v "0.3.6") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "black_scholes") (r "^0.2.3") (d #t) (k 2)))) (h "0c29z95fb75md40jk88flx7q5nbf6x6nnfmr26fqa1zn59p0pj4a")))

(define-public crate-binomial_tree-0.4.0 (c (n "binomial_tree") (v "0.4.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "black_scholes") (r "^0.2.3") (d #t) (k 2)))) (h "14mx926acz1qik5yzma1as8rxakgr9c528kr17lxv09hvi1d8v2n")))

(define-public crate-binomial_tree-0.5.0 (c (n "binomial_tree") (v "0.5.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "black_scholes") (r "^0.5.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1ywq19ma186h0knpnixy4yavhwks07bfm4f2l70v2lq87iax4iwv")))

