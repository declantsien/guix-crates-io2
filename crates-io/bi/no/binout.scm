(define-module (crates-io bi no binout) #:use-module (crates-io))

(define-public crate-binout-0.1.0 (c (n "binout") (v "0.1.0") (h "1ra10ak5y9bvx7b764p8f2sfblw09apd0klwdrfd43wwfrw5lps9")))

(define-public crate-binout-0.1.1 (c (n "binout") (v "0.1.1") (h "0i27g4f05rvllfdwjnjqk5i2bcd3kvwrgcs5li1i6jlh48qb2l2x")))

(define-public crate-binout-0.2.0 (c (n "binout") (v "0.2.0") (h "0pqf627jny8mjn1wdz51sg8fsc2amnnxh8n8gnxmjsam00f7p318")))

(define-public crate-binout-0.2.1 (c (n "binout") (v "0.2.1") (h "1spj4hh4xk23f3si8xpm1zw18a65bdrpvbi4wigwm3sqibw1l2xn")))

