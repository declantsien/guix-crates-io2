(define-module (crates-io bi -d bi-directional-pipe) #:use-module (crates-io))

(define-public crate-bi-directional-pipe-0.1.0 (c (n "bi-directional-pipe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1arsk3qxwpqjk105jvfdfqaxii7w49y76bv9vm6gxk9v5a2rmrm8")))

(define-public crate-bi-directional-pipe-0.1.1 (c (n "bi-directional-pipe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0g0ddhcvywrl5q82zqbpcmn9sg3wvy5cb8sxlk653dfdzmnxx9si")))

(define-public crate-bi-directional-pipe-0.1.2 (c (n "bi-directional-pipe") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1w6yhrjs3ljilqdj0a0za7x12lyl9f9ach9y39mavbgsz22phd19")))

(define-public crate-bi-directional-pipe-0.1.3 (c (n "bi-directional-pipe") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "175ha2vchx04p6injm6hrcid33zgsbv6hfr4xnkhwjchf0j23rzd")))

(define-public crate-bi-directional-pipe-0.1.4 (c (n "bi-directional-pipe") (v "0.1.4") (d (list (d (n "atomic-waker") (r "^1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0wjrh3a3d6ig1nyxvvwn8gc46z0mwn4mqinpvriik4ijmj0z00g5")))

