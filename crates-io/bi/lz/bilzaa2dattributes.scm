(define-module (crates-io bi lz bilzaa2dattributes) #:use-module (crates-io))

(define-public crate-bilzaa2dattributes-0.1.0 (c (n "bilzaa2dattributes") (v "0.1.0") (h "05x0mwjf2h06nh7l2hn7fadqrkfllq0iw37vpd43z1i4ndkhj712") (y #t)))

(define-public crate-bilzaa2dattributes-0.1.1 (c (n "bilzaa2dattributes") (v "0.1.1") (h "0yvcpjnxjvh4dg6ks1kd33dbdjgymy9q1jgajdi35xngp92azi05") (y #t)))

(define-public crate-bilzaa2dattributes-0.1.2 (c (n "bilzaa2dattributes") (v "0.1.2") (h "1j956pccmv4q31935nfswz08i5h32sp998l3j18m3brk9gilgm7a") (y #t)))

(define-public crate-bilzaa2dattributes-0.1.3 (c (n "bilzaa2dattributes") (v "0.1.3") (h "1vjg8r58sc8yldq131z4dqkszk7yslgh6ky0cg3ncd8rzfhp3y2c") (y #t)))

(define-public crate-bilzaa2dattributes-0.1.4 (c (n "bilzaa2dattributes") (v "0.1.4") (h "1bmkqczn4xip7rqshbknk7w7pfi9sps61s8cg7alwxnb4ydln659") (y #t)))

(define-public crate-bilzaa2dattributes-0.1.5 (c (n "bilzaa2dattributes") (v "0.1.5") (h "0ks1whc0760xpb17qb43wmbxg2p75b1rdh6lvr6vsjikj9lq9cxp") (y #t)))

(define-public crate-bilzaa2dattributes-0.1.6 (c (n "bilzaa2dattributes") (v "0.1.6") (h "13xhfx3iiknzm1jw37qzipn4gczfdlfhx54yaibn4af34gxvlapi")))

