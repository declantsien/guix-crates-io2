(define-module (crates-io bi lz bilzaa2dcounter) #:use-module (crates-io))

(define-public crate-bilzaa2dcounter-0.1.0 (c (n "bilzaa2dcounter") (v "0.1.0") (h "0gs14ycn57cm6n0daqsykhdlnxnpg8ik0i87i4396k9ihjkfla24") (y #t)))

(define-public crate-bilzaa2dcounter-0.1.1 (c (n "bilzaa2dcounter") (v "0.1.1") (h "0n5gsm08v528ihvz57lk6lmjhwk16fkxzmwygg95sy227318rr0v") (y #t)))

(define-public crate-bilzaa2dcounter-0.1.2 (c (n "bilzaa2dcounter") (v "0.1.2") (h "1cb7vi1sdgm12vfjhrjndh816bf3jsra8hi0p7w087b6h6s831mn") (y #t)))

(define-public crate-bilzaa2dcounter-0.1.3 (c (n "bilzaa2dcounter") (v "0.1.3") (h "1ammixkpfszaadqmywwf18xllwrzaidnic33mk48w4s4s9x57y5g") (y #t)))

(define-public crate-bilzaa2dcounter-0.1.4 (c (n "bilzaa2dcounter") (v "0.1.4") (d (list (d (n "bilzaa2dattributes") (r "^0.1.6") (d #t) (k 0)))) (h "1l2d8xdrz0pacwmnlvi81jbaiydrfzq93z3nfbdmk2k8c8dfbh6y") (y #t)))

