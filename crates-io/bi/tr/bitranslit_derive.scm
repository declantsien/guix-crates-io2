(define-module (crates-io bi tr bitranslit_derive) #:use-module (crates-io))

(define-public crate-bitranslit_derive-0.1.0 (c (n "bitranslit_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fs95ybvr19jb6kgflnlkggriqgcvjry0d23skm63mxwz2aspj4h")))

(define-public crate-bitranslit_derive-0.2.0 (c (n "bitranslit_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05i1q48khxqv26pbrzq6j34dvcpii5xc5w87hl5abmwv5l5n9l65")))

(define-public crate-bitranslit_derive-0.3.0 (c (n "bitranslit_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09g382hvkdrx1y4gbcjbpqip4jpzz3wza0ch9gvaqb4iyrz36xz3")))

(define-public crate-bitranslit_derive-0.3.1 (c (n "bitranslit_derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cd4iv3c6p1j09nzxs332k0cr2iingx19f74ki1pssjgyqk3z40n")))

