(define-module (crates-io bi tr bitreel) #:use-module (crates-io))

(define-public crate-bitreel-0.1.0 (c (n "bitreel") (v "0.1.0") (d (list (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0a9385namkw2lkmywjjlxv3mmmqw9ha54y2hfrn0x6j94iz47qmi")))

(define-public crate-bitreel-0.1.1 (c (n "bitreel") (v "0.1.1") (d (list (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0ksq06h8as3c2i5xwlcvysm60rk95gdcsal2dhfaf9j34vfy4hza")))

(define-public crate-bitreel-0.2.0 (c (n "bitreel") (v "0.2.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0gf4cad3rg0dcwqwjzq4yafac26af519f03k22lbnvdpbc49s5cf") (f (quote (("default-connector" "reqwest") ("default" "default-connector"))))))

