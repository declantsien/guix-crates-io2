(define-module (crates-io bi tr bitread) #:use-module (crates-io))

(define-public crate-bitread-0.1.6 (c (n "bitread") (v "0.1.6") (d (list (d (n "bitread_macro") (r "^0.1.3") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)))) (h "0slhmmzxsf3r837y5qs4y6pbzz076wm9jb9mss1asnrpwp0472vn")))

(define-public crate-bitread-0.1.9 (c (n "bitread") (v "0.1.9") (d (list (d (n "bitread_macro") (r "^0.1.7") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)))) (h "15w72p4f2h8j8bqv1nzd1bdl61pdhbc1raglh27l5h2lia9fhx8q")))

