(define-module (crates-io bi tr bitrate) #:use-module (crates-io))

(define-public crate-bitrate-0.1.0 (c (n "bitrate") (v "0.1.0") (h "0k9svk6q7z23pqm5dyqn6qhh6w83yvlp4nm5sqjny2k6b4q3m9vw")))

(define-public crate-bitrate-0.1.1 (c (n "bitrate") (v "0.1.1") (h "0ga7j6m2d2nmm08s72vdlhlida3clxlsgz98g1rfyjyh29lxhiy1")))

