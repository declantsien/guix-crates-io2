(define-module (crates-io bi tr bitrange) #:use-module (crates-io))

(define-public crate-bitrange-0.1.0 (c (n "bitrange") (v "0.1.0") (d (list (d (n "bitrange_plugin") (r "^0.1.0") (d #t) (k 0)))) (h "1wizia69ffxrcginni25c0hjnj2hqpxyiqqs7102b36kfhmbi6p8")))

(define-public crate-bitrange-0.1.1 (c (n "bitrange") (v "0.1.1") (d (list (d (n "bitrange_plugin") (r "^0.1.0") (d #t) (k 0)))) (h "06bzsgcjyh3sfxvpb8c0aykdiw3iwka1gjg0vvbngcssalbx90sp")))

(define-public crate-bitrange-0.1.2 (c (n "bitrange") (v "0.1.2") (d (list (d (n "bitrange_plugin") (r "^0.1.0") (d #t) (k 0)))) (h "1ab316dl8rv160crsfdscjjp94is9njrd2hk0kw0hiy0p9kb6jq8")))

(define-public crate-bitrange-0.2.0 (c (n "bitrange") (v "0.2.0") (d (list (d (n "bitrange_plugin") (r "^0.1.2") (d #t) (k 0)))) (h "19hbv3yai8pg6a4kk9nrcjwzaid2mr6zsd5ic30v7kczjcylv7ll") (f (quote (("std") ("panic") ("default" "std"))))))

(define-public crate-bitrange-0.3.0 (c (n "bitrange") (v "0.3.0") (d (list (d (n "bitrange_plugin") (r "^0.3.0") (d #t) (k 0)))) (h "0pl600d6p1a5q3anrriznadjwi81q6fr1qf6jr33ifz2lk4i463c") (f (quote (("std") ("panic") ("default" "std"))))))

