(define-module (crates-io bi tr bitreader_async) #:use-module (crates-io))

(define-public crate-bitreader_async-0.1.1 (c (n "bitreader_async") (v "0.1.1") (d (list (d (n "async-std") (r "^1.2.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1swv8dh8ywdszv85vkk3vgjirvms4dmk6qgpwpm64xz63z8163m0")))

(define-public crate-bitreader_async-0.2.0 (c (n "bitreader_async") (v "0.2.0") (d (list (d (n "async-std") (r "^1.2.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.21") (d #t) (k 0)) (d (n "bit_reverse") (r "^0.1.8") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "0xpa0ay11biciscvgkxlxa4rdknq4ypwvwjghg8bil4b5y947xq5")))

