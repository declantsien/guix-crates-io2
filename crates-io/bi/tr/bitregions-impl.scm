(define-module (crates-io bi tr bitregions-impl) #:use-module (crates-io))

(define-public crate-bitregions-impl-0.1.0 (c (n "bitregions-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0a2xqqyx1xhdi2qfk1k7bf4swlnfzdzc741cwxxkwvmankn6r5r4")))

(define-public crate-bitregions-impl-0.1.1 (c (n "bitregions-impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1n15mm867lq5gp0vk57rikab23619wv138rsmbirfzdx57mnqkiv")))

(define-public crate-bitregions-impl-0.1.2 (c (n "bitregions-impl") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1xx0xfqrp0ybpnsvhvxkr7z619ckh0scnpr5jn42zrp5al1rmv85")))

(define-public crate-bitregions-impl-0.1.3 (c (n "bitregions-impl") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0wway44g31v0gdhbj7q7gyvzwdikllxh2cfrchsvn4ndf4ypdzd2")))

(define-public crate-bitregions-impl-0.2.0 (c (n "bitregions-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0waawkch2vbxcn0bgc8q66142gjl32vizs28l4irxkqflzzs7rs1")))

(define-public crate-bitregions-impl-0.2.1 (c (n "bitregions-impl") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0q907zv2cqgjk5q33rrf8rgmm7hr3x9n77md5vfjgfx5d97v1hmx")))

(define-public crate-bitregions-impl-0.2.2 (c (n "bitregions-impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0mq2gy5wyn3bia5q1fnjmg9m3kfww5mdzlvb6wbxsrgfx72v4vvv")))

(define-public crate-bitregions-impl-0.2.3 (c (n "bitregions-impl") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1m51mdch63wb5q3wyw0m3iblbrpa2awj3n518468mni2mk6d5awl")))

(define-public crate-bitregions-impl-0.2.4 (c (n "bitregions-impl") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "017889pp791ablyy4fn2d01hd018kbh2583iqvrilamxvnc62zsj")))

(define-public crate-bitregions-impl-0.2.5 (c (n "bitregions-impl") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0jl8m6vf73pa662pg91shijawqqqfn3xvpbnicqjkzcz6iz49xcy")))

