(define-module (crates-io bi tr bitreader) #:use-module (crates-io))

(define-public crate-bitreader-0.1.0 (c (n "bitreader") (v "0.1.0") (h "13cfjys502z8c7aiayh5fb8k8vddz9m5npgsicdha6mxvng06idj")))

(define-public crate-bitreader-0.2.0 (c (n "bitreader") (v "0.2.0") (h "0yns8s2r92r914janyyjv57a6c671p3rw01ml4cww364i1jsl6c3")))

(define-public crate-bitreader-0.3.0 (c (n "bitreader") (v "0.3.0") (h "01qpnpm67wk7ikwza8cqi4vxq8lq7x9zy7mzpnh3mzv4n0m3xcc0")))

(define-public crate-bitreader-0.3.1 (c (n "bitreader") (v "0.3.1") (h "1f6mrrvvsrh7p8sar7vdq8p38ams13hgbaqkm43ybw04dqpp2gm1")))

(define-public crate-bitreader-0.3.2 (c (n "bitreader") (v "0.3.2") (d (list (d (n "cfg-if") (r "^0.1.9") (d #t) (k 0)))) (h "16z0csk5a251yvz4rpqh7n90m261i6zg87wajwn4gmbwyfnz19sz") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3.3 (c (n "bitreader") (v "0.3.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "19licyksbjfsa3jvz39cm3xkjrsqla2lc1g6z8jr3j2z06c7m9bh") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3.4 (c (n "bitreader") (v "0.3.4") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "06wlzf8ra7k8ya0m71dq0kxbaalaan48dymairn9q8s4gld1hy4i") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3.5 (c (n "bitreader") (v "0.3.5") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1xcp1pxyd3si180w7x9wx485dgxmyhk957lzd1sx81nwgjhzhnxx") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3.6 (c (n "bitreader") (v "0.3.6") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "0hcrn2s483si9j6v3gykn3029g4m2s5rifd9czz9iznihlfafknq") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3.7 (c (n "bitreader") (v "0.3.7") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "16w68lcdax243ibv4937jj9ssdl60723x6bz35zmx5sdhvj4607i") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3.8 (c (n "bitreader") (v "0.3.8") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1zxf5qkjhqzsqilnkvlh3sxvs4n4rjp3anvraa14cz3zv74mkn5x") (f (quote (("std") ("default" "std"))))))

