(define-module (crates-io bi tr bitregions) #:use-module (crates-io))

(define-public crate-bitregions-0.1.0 (c (n "bitregions") (v "0.1.0") (d (list (d (n "bitregions-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "1ym0fmmw81dngna0fmj0njpjflagrmha10shs573lp7skgiykx8g")))

(define-public crate-bitregions-0.1.1 (c (n "bitregions") (v "0.1.1") (d (list (d (n "bitregions-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "0vl9a1mwb3gy94nlw3hcskw7v91n4syy1gav25q0fj9ks63ixj4q")))

(define-public crate-bitregions-0.1.2 (c (n "bitregions") (v "0.1.2") (d (list (d (n "bitregions-impl") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "19mycz9av2siikh8mzkzia2y8vfklk7jj3syd1hxysf7jyyrq82b")))

(define-public crate-bitregions-0.1.3 (c (n "bitregions") (v "0.1.3") (d (list (d (n "bitregions-impl") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "1m95nhxfkqjm7lckzknl8wqpyh71p0inmf6698k76zb9kv6sipq0")))

(define-public crate-bitregions-0.2.0 (c (n "bitregions") (v "0.2.0") (d (list (d (n "bitregions-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "0dhk1m8hc9x9wmhpgbnznmk9slgy191jkm95llr19p0hj62r9m6x")))

(define-public crate-bitregions-0.2.1 (c (n "bitregions") (v "0.2.1") (d (list (d (n "bitregions-impl") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "154bf46lvgcyb6gy13by20g7cisacp1csb8vh230z43maz22mpfh")))

(define-public crate-bitregions-0.2.2 (c (n "bitregions") (v "0.2.2") (d (list (d (n "bitregions-impl") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)))) (h "19787198vr0wn1607plawb1jf7mlp4m0a1ph36kk6b5a4vz7cfiw")))

(define-public crate-bitregions-0.2.3 (c (n "bitregions") (v "0.2.3") (d (list (d (n "bitregions-impl") (r "^0.2.3") (d #t) (k 0)))) (h "05z3iqr0808w6klmfvs6cfrwwds7czr93qdbwv4q0hkzm9f9ji75")))

(define-public crate-bitregions-0.2.4 (c (n "bitregions") (v "0.2.4") (d (list (d (n "bitregions-impl") (r "^0.2.4") (d #t) (k 0)))) (h "074lzn7ry0g9pdzwxi5pdi4jidsbhiipqvnpgp6z6hjfv3p4748g")))

(define-public crate-bitregions-0.2.5 (c (n "bitregions") (v "0.2.5") (d (list (d (n "bitregions-impl") (r "^0.2.5") (d #t) (k 0)))) (h "150klrkab891x1r0v59sbhhl864b3gr6awc3dvgd1xav8q0qn8sv")))

