(define-module (crates-io bi tr bitr) #:use-module (crates-io))

(define-public crate-bitr-0.1.0 (c (n "bitr") (v "0.1.0") (h "1zn5vmy62ligqgrrri8ghv2jasr6mznfjh2jky198n42a9jkf5nd")))

(define-public crate-bitr-0.1.1 (c (n "bitr") (v "0.1.1") (h "1is85j9z04024m1rvprsinl9js4r22h5f480an9dhwj1jkhghfq0")))

