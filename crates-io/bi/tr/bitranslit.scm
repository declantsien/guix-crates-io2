(define-module (crates-io bi tr bitranslit) #:use-module (crates-io))

(define-public crate-bitranslit-0.1.0 (c (n "bitranslit") (v "0.1.0") (d (list (d (n "bitranslit_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 1)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0wpn08m70an6s2vi4ybp24qkgjhg3cckh8vm2sssqrbhdsz9lcn2")))

(define-public crate-bitranslit-0.2.0 (c (n "bitranslit") (v "0.2.0") (d (list (d (n "bitranslit_derive") (r "=0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 1)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0vcn9h5nw9xxxsir99zw31vf1lsmmy1valch7p9q586ln42s18gh")))

(define-public crate-bitranslit-0.3.0 (c (n "bitranslit") (v "0.3.0") (d (list (d (n "bitranslit_derive") (r "=0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 1)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "10srrlw70kppg2aibqc54x1dkkfm23rkra5hi4p5jvgzy5r6hrd9")))

(define-public crate-bitranslit-0.3.1 (c (n "bitranslit") (v "0.3.1") (d (list (d (n "bitranslit_derive") (r "=0.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 1)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0n76zi64jbmnp91459x3f4swbnxyzkl9ygm4x20xpfwmwxmnp6nh")))

