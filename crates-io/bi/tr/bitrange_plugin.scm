(define-module (crates-io bi tr bitrange_plugin) #:use-module (crates-io))

(define-public crate-bitrange_plugin-0.1.0 (c (n "bitrange_plugin") (v "0.1.0") (h "04g4ilg3infnhsqqz7ffqd7a4sdv66aa95c7s2qg4kfk6vn1ynwq")))

(define-public crate-bitrange_plugin-0.1.1 (c (n "bitrange_plugin") (v "0.1.1") (h "1hb1ri5inimdwmcgg5bw2l0rw3igs5gybcw5drlnbvzmi12z1xbj")))

(define-public crate-bitrange_plugin-0.1.2 (c (n "bitrange_plugin") (v "0.1.2") (h "16ixn8zsm2r31i109ksnix0dkfpr9p13b5pgjdpfdz8lz74001hb")))

(define-public crate-bitrange_plugin-0.3.0 (c (n "bitrange_plugin") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1iazf93x1bddqiw33bs6yg648y9cghaa2imjg7xhbch3879rywf6")))

