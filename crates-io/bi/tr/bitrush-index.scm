(define-module (crates-io bi tr bitrush-index) #:use-module (crates-io))

(define-public crate-bitrush-index-0.1.0 (c (n "bitrush-index") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1w6jdzmjw5s5hni5yb6263rlijb619vi1k7jzllpq30aq3a6g6w6")))

(define-public crate-bitrush-index-0.1.1 (c (n "bitrush-index") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1yf4vf6x5bdswbrsnfc4c88q4n4g0av8bdg642bb17f01q00vwr9")))

