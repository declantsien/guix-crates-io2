(define-module (crates-io bi tr bitrw) #:use-module (crates-io))

(define-public crate-bitrw-0.1.0 (c (n "bitrw") (v "0.1.0") (d (list (d (n "base2") (r "^0.1.0") (d #t) (k 0)))) (h "0rpr3qwr670625p1ra6yy61zrwn2057qd0ki98k6lj4l9xq4bg53")))

(define-public crate-bitrw-0.1.1 (c (n "bitrw") (v "0.1.1") (d (list (d (n "base2") (r "^0.1.1") (d #t) (k 0)))) (h "1n7vas5fbaws5s9y20xbpkh043kj4n2bqipwwc1sjl09gzd3xgyj")))

(define-public crate-bitrw-0.1.2 (c (n "bitrw") (v "0.1.2") (d (list (d (n "base2") (r "^0.2.2") (d #t) (k 0)))) (h "1cx04mr2lfdh8nj0vq4vh58macpm2n4xw3sb8kpdd6z5qr8i1yky")))

(define-public crate-bitrw-0.2.0 (c (n "bitrw") (v "0.2.0") (d (list (d (n "base2") (r "^0.2.2") (d #t) (k 0)) (d (n "int") (r "^0.2.10") (d #t) (k 0)))) (h "07xnpd41ibk76hcr13q1xzwmr8xrsc80zfjh5y74nl62mfny3zx8")))

(define-public crate-bitrw-0.2.1 (c (n "bitrw") (v "0.2.1") (d (list (d (n "base2") (r "^0.2.2") (d #t) (k 0)) (d (n "int") (r "^0.2.10") (d #t) (k 0)))) (h "061827kj8dmqb8nxd8avjiqa8g1w9l6pwdif3fv87pbadj3464ai")))

(define-public crate-bitrw-0.3.0 (c (n "bitrw") (v "0.3.0") (d (list (d (n "base2") (r "^0.2.2") (d #t) (k 0)) (d (n "int") (r "^0.2.10") (d #t) (k 0)))) (h "1xfxyxxcknjpslnkl9xj69zds2alm9r0id68a995r33pcmwzk1dh")))

(define-public crate-bitrw-0.4.0 (c (n "bitrw") (v "0.4.0") (d (list (d (n "base2") (r "^0.2.2") (d #t) (k 0)) (d (n "int") (r "^0.2.10") (d #t) (k 0)))) (h "0bgjhlf7bysmyhddzh8ifmzv84ny32bpac84gab7gvzcd8qrmvai")))

(define-public crate-bitrw-0.4.1 (c (n "bitrw") (v "0.4.1") (d (list (d (n "base2") (r "^0.2.2") (d #t) (k 0)) (d (n "int") (r "^0.2.10") (d #t) (k 0)))) (h "0q2z9nhgmy4whxr02anbh77hra1lr49cczhjdl165yim9fxdyjxa")))

(define-public crate-bitrw-0.5.0 (c (n "bitrw") (v "0.5.0") (d (list (d (n "base2") (r "^0.2.2") (d #t) (k 0)) (d (n "int") (r "^0.2.10") (d #t) (k 0)))) (h "0r2wxc659sbv8yqhiknavkfpv5dgbgipp0569anzrlpwpssbgcjz")))

(define-public crate-bitrw-0.6.0 (c (n "bitrw") (v "0.6.0") (d (list (d (n "base2") (r "^0.2.2") (d #t) (k 0)) (d (n "int") (r "^0.2.10") (d #t) (k 0)))) (h "13mi07dhsxwbgajj350k8l45y32ydjrhhp3azha1wizlqlig97zk")))

(define-public crate-bitrw-0.7.0 (c (n "bitrw") (v "0.7.0") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "int") (r "^0.2.11") (d #t) (k 0)))) (h "0hpzfsinwixq2wsqh43miw1ypfg85pga2rl85k5fxfk234y868s2")))

(define-public crate-bitrw-0.7.1 (c (n "bitrw") (v "0.7.1") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "int") (r "^0.2.11") (d #t) (k 0)))) (h "1vbwcn7q2rmrjdkkk1b60hgdmib40lm3s9bicrsmr8y4z9f4g9m9")))

(define-public crate-bitrw-0.8.0 (c (n "bitrw") (v "0.8.0") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "int") (r "^0.2.11") (d #t) (k 0)))) (h "1xk9miciq8vxvazy0kkmm8dglgw62mmq1jx8gzl9vhjpl4xbpb2w")))

(define-public crate-bitrw-0.8.1 (c (n "bitrw") (v "0.8.1") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "int") (r "^0.2.11") (d #t) (k 0)))) (h "07nx0pb31rmhs1rl4v7pxam8b6bbxah71gby53h8vw1prqg72pa7")))

(define-public crate-bitrw-0.8.2 (c (n "bitrw") (v "0.8.2") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "int") (r "^0.3.0") (d #t) (k 0)))) (h "168h8rzh3f8b7p8x912ragvi53inm60lh8xg61k9j2avi58wzgm9")))

(define-public crate-bitrw-0.8.3 (c (n "bitrw") (v "0.8.3") (d (list (d (n "base2") (r "^0.3.1") (d #t) (k 0)) (d (n "int") (r "^0.3.0") (d #t) (k 0)))) (h "1z9vylh5qmaidpb1k2x6c0a1xw8yzib9hssdvdcrhvafhggrl0m5")))

