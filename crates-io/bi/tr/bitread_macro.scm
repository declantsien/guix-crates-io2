(define-module (crates-io bi tr bitread_macro) #:use-module (crates-io))

(define-public crate-bitread_macro-0.1.6 (c (n "bitread_macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1pgr4vd1rldfjjf2bk317lfn28lk8gyv4vf8brqgwq3kjpbjn81s")))

(define-public crate-bitread_macro-0.1.7 (c (n "bitread_macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "04xhfpcxf1a812v71d57nsa11nisrpmpk6zhl14jd13wyzh8n1nz")))

