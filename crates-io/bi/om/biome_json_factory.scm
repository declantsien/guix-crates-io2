(define-module (crates-io bi om biome_json_factory) #:use-module (crates-io))

(define-public crate-biome_json_factory-0.0.1 (c (n "biome_json_factory") (v "0.0.1") (d (list (d (n "biome_json_syntax") (r "^0.0.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "1jbbrp628pq89mpmkkrx86scxksp12k5ni7xi0ciwwa9c56jgri1")))

(define-public crate-biome_json_factory-0.0.2 (c (n "biome_json_factory") (v "0.0.2") (d (list (d (n "biome_json_syntax") (r "^0.0.2") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.2") (d #t) (k 0)))) (h "14sdr1fxzb70bn53ilzic915by67wbf0ik6za16n6nib217c8b5z")))

(define-public crate-biome_json_factory-0.1.0 (c (n "biome_json_factory") (v "0.1.0") (d (list (d (n "biome_json_syntax") (r "^0.1.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)))) (h "12kfkvyb47lngfi8zs2isw58a4rs5gpm71ibxf4akihfml3qwkh3")))

(define-public crate-biome_json_factory-0.3.1 (c (n "biome_json_factory") (v "0.3.1") (d (list (d (n "biome_json_syntax") (r "^0.3.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.3.1") (d #t) (k 0)))) (h "07mlyv93h0din6vh3kyq9z3xwd1by0n4jjfbyb2bc48z72a0zhvy")))

(define-public crate-biome_json_factory-0.4.0 (c (n "biome_json_factory") (v "0.4.0") (d (list (d (n "biome_json_syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.4.0") (d #t) (k 0)))) (h "104jb9n7hparwhp7pxih0dd168a860g8ma3fpnxsjsy7li1x180l")))

(define-public crate-biome_json_factory-0.5.0 (c (n "biome_json_factory") (v "0.5.0") (d (list (d (n "biome_json_syntax") (r "^0.5.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.0") (d #t) (k 0)))) (h "18zsg2yaah526zd0zhdgnxyafhj33bxjdsaq5aac7ssdpvf9ldd1")))

(define-public crate-biome_json_factory-0.5.5 (c (n "biome_json_factory") (v "0.5.5") (d (list (d (n "biome_json_syntax") (r "^0.5.5") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.5") (d #t) (k 0)))) (h "05mr08mv020hpb4jwdl133s7hgzf0s4kp0iz9xx9g1gvvjx01j3a")))

(define-public crate-biome_json_factory-0.5.6 (c (n "biome_json_factory") (v "0.5.6") (d (list (d (n "biome_json_syntax") (r "^0.5.6") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.6") (d #t) (k 0)))) (h "00q280d10p34mz6950ljalmkv78iqr78pj23fpgpyly0sqf5yb6p")))

(define-public crate-biome_json_factory-0.5.7 (c (n "biome_json_factory") (v "0.5.7") (d (list (d (n "biome_json_syntax") (r "^0.5.7") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.7") (d #t) (k 0)))) (h "1krpck9v7djwycjq5zgzlylwzj3a1fq7ilfsi436dws0j0lfn2g4")))

