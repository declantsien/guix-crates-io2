(define-module (crates-io bi om biome_css_factory) #:use-module (crates-io))

(define-public crate-biome_css_factory-0.0.1 (c (n "biome_css_factory") (v "0.0.1") (d (list (d (n "biome_css_syntax") (r "^0.0.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "18k2z1b2054xbhza8qz10walij49ya3j38mm4pxdrj934zr74w47")))

(define-public crate-biome_css_factory-0.0.2 (c (n "biome_css_factory") (v "0.0.2") (d (list (d (n "biome_css_syntax") (r "^0.0.2") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.2") (d #t) (k 0)))) (h "0bm2lq6xv4p3d9wg38bby1fbm0sgvic10pnc9bj3vrq051qj4vp8")))

(define-public crate-biome_css_factory-0.1.0 (c (n "biome_css_factory") (v "0.1.0") (d (list (d (n "biome_css_syntax") (r "^0.1.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)))) (h "0q4hfvck5dbpc9rqpi8n750x59xn0vxaxkanv9ladziz7pa7lbxh")))

(define-public crate-biome_css_factory-0.3.1 (c (n "biome_css_factory") (v "0.3.1") (d (list (d (n "biome_css_syntax") (r "^0.3.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.3.1") (d #t) (k 0)))) (h "1gmsw90g7c2wrf99d5qrniypcw28nv2q0brwc0c2hws25fqkmjk3")))

(define-public crate-biome_css_factory-0.4.0 (c (n "biome_css_factory") (v "0.4.0") (d (list (d (n "biome_css_syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.4.0") (d #t) (k 0)))) (h "1c483qfp1q3zy25b26s5nq9fpll0d52zzqpd07jkq7l27w8f0f0y")))

(define-public crate-biome_css_factory-0.5.0 (c (n "biome_css_factory") (v "0.5.0") (d (list (d (n "biome_css_syntax") (r "^0.5.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.0") (d #t) (k 0)))) (h "06lwkabfsq48ch4i5rk6bhc58ni2khw7qnllis8ddclmi81ixgam")))

(define-public crate-biome_css_factory-0.5.5 (c (n "biome_css_factory") (v "0.5.5") (d (list (d (n "biome_css_syntax") (r "^0.5.5") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.5") (d #t) (k 0)))) (h "0z95lr5zqxb128xyh7vz8jwsy53bj0nz3m3my1ma9ncci5g8lrgv")))

(define-public crate-biome_css_factory-0.5.6 (c (n "biome_css_factory") (v "0.5.6") (d (list (d (n "biome_css_syntax") (r "^0.5.6") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.6") (d #t) (k 0)))) (h "1sf1081yh97b60j12pyfzkmhvzxfhw5fg5836yfw3hjj4hbr0ia9")))

(define-public crate-biome_css_factory-0.5.7 (c (n "biome_css_factory") (v "0.5.7") (d (list (d (n "biome_css_syntax") (r "^0.5.7") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.7") (d #t) (k 0)))) (h "13qdrh8048hx8b8qq21q0zm67fhsqqr58kfphk5w2qamfdp79wl7")))

