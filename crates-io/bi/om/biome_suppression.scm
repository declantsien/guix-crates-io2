(define-module (crates-io bi om biome_suppression) #:use-module (crates-io))

(define-public crate-biome_suppression-0.0.1 (c (n "biome_suppression") (v "0.0.1") (d (list (d (n "biome_console") (r "^0.0.1") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.0.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "18bb8q9ylmabvi9r9ggw6rbwalkfnn37gi58dk2gcxgdn110khw8")))

(define-public crate-biome_suppression-0.0.2 (c (n "biome_suppression") (v "0.0.2") (d (list (d (n "biome_console") (r "^0.0.2") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.0.2") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.2") (d #t) (k 0)))) (h "0qda25q21a52z8rmmrna5sls4qzznjzvl2c5h4k67zqrxkzjwb0k")))

(define-public crate-biome_suppression-0.1.0 (c (n "biome_suppression") (v "0.1.0") (d (list (d (n "biome_console") (r "^0.1.0") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.1.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)))) (h "0qfsvyssxhadsipggd5h3p3wz5gzi21bcbjycfrgkzp6y6jap19b")))

(define-public crate-biome_suppression-0.3.1 (c (n "biome_suppression") (v "0.3.1") (d (list (d (n "biome_console") (r "^0.3.1") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.3.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.3.1") (d #t) (k 0)))) (h "12f5mvcmqk0cld17vmmnrxjfhd613ybqfmjqy0nf0xr6vds7a322")))

(define-public crate-biome_suppression-0.4.0 (c (n "biome_suppression") (v "0.4.0") (d (list (d (n "biome_console") (r "^0.4.0") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.4.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.4.0") (d #t) (k 0)))) (h "1hzx82is4hdrbmvds81p9aaigcgairm9dkgrdw5cb5k88ahwfkrb")))

(define-public crate-biome_suppression-0.5.0 (c (n "biome_suppression") (v "0.5.0") (d (list (d (n "biome_console") (r "^0.5.0") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.5.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.0") (d #t) (k 0)))) (h "04p4f0h238yz48zw1y16x5wzipp589wk4j6cpi46dk32ri7zz5yz")))

(define-public crate-biome_suppression-0.5.5 (c (n "biome_suppression") (v "0.5.5") (d (list (d (n "biome_console") (r "^0.5.5") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.5.5") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.5") (d #t) (k 0)))) (h "10hmslh6njsg3nzcvfkssfadwnd5nxpn8dmlf6qlnkcv6m77n6gc")))

(define-public crate-biome_suppression-0.5.6 (c (n "biome_suppression") (v "0.5.6") (d (list (d (n "biome_console") (r "^0.5.6") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.5.6") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.6") (d #t) (k 0)))) (h "0503kg8s9hw4xbga1mbj2fcn7jphxabzsb6r0pvmkvfk1vrrn4cb")))

(define-public crate-biome_suppression-0.5.7 (c (n "biome_suppression") (v "0.5.7") (d (list (d (n "biome_console") (r "^0.5.7") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.5.7") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.7") (d #t) (k 0)))) (h "1m9rmjaw5xcb3yyhr66nykr394x3rln4p6ar5j6ackj57619rd7g")))

