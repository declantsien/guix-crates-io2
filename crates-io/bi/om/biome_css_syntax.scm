(define-module (crates-io bi om biome_css_syntax) #:use-module (crates-io))

(define-public crate-biome_css_syntax-0.0.1 (c (n "biome_css_syntax") (v "0.0.1") (d (list (d (n "biome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "1lzq2b5arwx0ir71cf9m9hqk5m0arv9iri98clkdff0nf3x80zdp")))

(define-public crate-biome_css_syntax-0.0.2 (c (n "biome_css_syntax") (v "0.0.2") (d (list (d (n "biome_rowan") (r "^0.0.2") (d #t) (k 0)))) (h "1li3p3z2ajfsjmvs9j377kiiq9ffssxv4zqa4b4dvi1498ff84y7")))

(define-public crate-biome_css_syntax-0.1.0 (c (n "biome_css_syntax") (v "0.1.0") (d (list (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)))) (h "0g39flhjc45y338609m2zjfgmy8wc3dr43p2awkdlvla2dxxgrlm")))

(define-public crate-biome_css_syntax-0.3.1 (c (n "biome_css_syntax") (v "0.3.1") (d (list (d (n "biome_rowan") (r "^0.3.1") (d #t) (k 0)))) (h "1b1mwchrln5qp5q7yp0n9j9dyciq9camxajhxlx6fc86z8vffsm4")))

(define-public crate-biome_css_syntax-0.4.0 (c (n "biome_css_syntax") (v "0.4.0") (d (list (d (n "biome_rowan") (r "^0.4.0") (d #t) (k 0)))) (h "1a34if71jlqqxpd1d8wkarh2wnx7p3qarrd302rjfphnzl7ddyzz")))

(define-public crate-biome_css_syntax-0.5.0 (c (n "biome_css_syntax") (v "0.5.0") (d (list (d (n "biome_rowan") (r "^0.5.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "0f62y094h0wij9hk9idbz09kx681pjbgmpzckjlnqdl53iv0f5fx") (f (quote (("schema" "schemars" "biome_rowan/serde"))))))

(define-public crate-biome_css_syntax-0.5.5 (c (n "biome_css_syntax") (v "0.5.5") (d (list (d (n "biome_rowan") (r "^0.5.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "0yrni96qip6b9ymfkqlgcvrjlf7mn020hck43zhffd001rvpyf65") (f (quote (("schema" "schemars" "biome_rowan/serde"))))))

(define-public crate-biome_css_syntax-0.5.6 (c (n "biome_css_syntax") (v "0.5.6") (d (list (d (n "biome_rowan") (r "^0.5.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "0nyzpiirfdh49dsnlhjn33b20yka25lshp82d5f53jjbfcb6vxc6") (f (quote (("schema" "schemars" "biome_rowan/serde"))))))

(define-public crate-biome_css_syntax-0.5.7 (c (n "biome_css_syntax") (v "0.5.7") (d (list (d (n "biome_rowan") (r "^0.5.7") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "1276kdyin70hda9pvdcvx966q323q56zhrjsybzaqam1qdprhyh1") (f (quote (("schema" "schemars" "biome_rowan/serde"))))))

