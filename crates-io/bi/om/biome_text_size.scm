(define-module (crates-io bi om biome_text_size) #:use-module (crates-io))

(define-public crate-biome_text_size-0.0.1 (c (n "biome_text_size") (v "0.0.1") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "11zy0cz3sdjc2hcxlfws9w3ds4aih3kjgh7c2biw7953qhmqd812")))

(define-public crate-biome_text_size-0.0.2 (c (n "biome_text_size") (v "0.0.2") (d (list (d (n "schemars") (r "=0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "=1.0.164") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "11a5q136rx7bpilx37j5iah94z59imvdg1ikdxi3h4pffh7v8pqd")))

(define-public crate-biome_text_size-0.0.3 (c (n "biome_text_size") (v "0.0.3") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "02irq9iqs4wkba1gpm1bnhp7pxggh2p6q6gzxz55d34ibih6rky6")))

(define-public crate-biome_text_size-0.0.4 (c (n "biome_text_size") (v "0.0.4") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "06fn6q66ipl3lam3blyw576h5fm7ihp0pqvrlb62iqi1glbi4fcg")))

(define-public crate-biome_text_size-0.1.0 (c (n "biome_text_size") (v "0.1.0") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "1jdqjdzzld05lpi7w9wfl56xq20d79bd6xzvl7dwqvg40n86smfq")))

(define-public crate-biome_text_size-0.3.0 (c (n "biome_text_size") (v "0.3.0") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "192nkyh1srfbmpnv794i29w7zrvfir6rdrhmb9i4np8lpgjbvg6i")))

(define-public crate-biome_text_size-0.3.1 (c (n "biome_text_size") (v "0.3.1") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "1xl2h8mqpqp8brxpy91mx75s3rhl609y0syacdjxckgilxymz54y")))

(define-public crate-biome_text_size-0.4.0 (c (n "biome_text_size") (v "0.4.0") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "1f5f1945ryj18wph9whz7cf0qwv7mf9bzjv9r9zsc4n2jnzgdamq")))

(define-public crate-biome_text_size-0.5.0 (c (n "biome_text_size") (v "0.5.0") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "14bjd6j1da2mx2j3am3fhhhqy23advwkpzgjr2y0a9y63a6y8zvy")))

(define-public crate-biome_text_size-0.5.5 (c (n "biome_text_size") (v "0.5.5") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "003zcqjmfgampmasc4vlf0mx4hzkrr9bwrcwb53g23xw07xl3zds")))

(define-public crate-biome_text_size-0.5.6 (c (n "biome_text_size") (v "0.5.6") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "1q3licqwqs05gdyvwv9cllsi394madf9zz0zw8nd14k8bv6if0mp")))

(define-public crate-biome_text_size-0.5.7 (c (n "biome_text_size") (v "0.5.7") (d (list (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "16riy3khsam3gby5wi3r3rfymzdcmrch6h2mc9iyvp7gbk8h9ijy")))

