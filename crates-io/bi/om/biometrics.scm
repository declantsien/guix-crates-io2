(define-module (crates-io bi om biometrics) #:use-module (crates-io))

(define-public crate-biometrics-0.1.0 (c (n "biometrics") (v "0.1.0") (h "1b0i8cih7m0hj9cb1mgnamsvi2ihcizf1xgxgdixbz5ik60ms4qp")))

(define-public crate-biometrics-0.1.1 (c (n "biometrics") (v "0.1.1") (h "1fl110a4chd50iz04msyzvc63n1r0l9mhlrsgpzdjvh6fpygdl50")))

(define-public crate-biometrics-0.2.0 (c (n "biometrics") (v "0.2.0") (h "0lj65xjnbgw4ypbcfsgibhphggm459mwrrrfbihx59j33p67pbbk")))

(define-public crate-biometrics-0.3.0 (c (n "biometrics") (v "0.3.0") (h "09b3fisp6ndh00966b1nax85j1l5nkk0iw9ihh89f0w3p7d7qdyp")))

(define-public crate-biometrics-0.4.0 (c (n "biometrics") (v "0.4.0") (d (list (d (n "utilz") (r "^0.2") (d #t) (k 0)))) (h "1gddazknhgnx7855n136b9ii01kvx3xn9m2mazlr4zdaf4bw8gg3")))

(define-public crate-biometrics-0.5.0 (c (n "biometrics") (v "0.5.0") (d (list (d (n "utilz") (r "^0.3") (d #t) (k 0)))) (h "00mz6h14hncgnj1miyhp4582wdxnkyhrxzd3myy8l33bdnqhipcp")))

(define-public crate-biometrics-0.6.0 (c (n "biometrics") (v "0.6.0") (h "0nhiiqy6spkgpjfcns0kpzj2jv1ylvv188fggqnp9i594w7vsjaa")))

