(define-module (crates-io bi om biome_json_syntax) #:use-module (crates-io))

(define-public crate-biome_json_syntax-0.0.1 (c (n "biome_json_syntax") (v "0.0.1") (d (list (d (n "biome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "19ns41di93sk4x4ls7p08c6ncyx7vwf48hfilyr8iwsw93cbl97l")))

(define-public crate-biome_json_syntax-0.0.2 (c (n "biome_json_syntax") (v "0.0.2") (d (list (d (n "biome_rowan") (r "^0.0.2") (d #t) (k 0)))) (h "0x31s065aq6h2y9d4v2bxyciixzfyps2147r48f1b4c6zdrcihfr")))

(define-public crate-biome_json_syntax-0.1.0 (c (n "biome_json_syntax") (v "0.1.0") (d (list (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)))) (h "169xdarmn9x0fqvq8xfymf1bs4x91bk3cz5kbw34zgk99vm1ak9j")))

(define-public crate-biome_json_syntax-0.3.1 (c (n "biome_json_syntax") (v "0.3.1") (d (list (d (n "biome_rowan") (r "^0.3.1") (d #t) (k 0)))) (h "0clvna4a6pf1l0dc51x6c4rijqk85323kn80a570mb1l42aq3fwp")))

(define-public crate-biome_json_syntax-0.4.0 (c (n "biome_json_syntax") (v "0.4.0") (d (list (d (n "biome_rowan") (r "^0.4.0") (d #t) (k 0)))) (h "111h6lrjl7i67hiywjbwx37mgwinlr4qzhja4cawpjh2hfgq57ln")))

(define-public crate-biome_json_syntax-0.5.0 (c (n "biome_json_syntax") (v "0.5.0") (d (list (d (n "biome_rowan") (r "^0.5.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "01y7ga7cin3i5y7ynzdzrcwld4b0h5vvv3blayjv21qb2yhg1bss") (f (quote (("schema" "biome_rowan/serde" "schemars"))))))

(define-public crate-biome_json_syntax-0.5.5 (c (n "biome_json_syntax") (v "0.5.5") (d (list (d (n "biome_rowan") (r "^0.5.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "0jz6s9xfmmv871f8wk0i7myvspfvf6gx7pks3b7vhfk1zlrv68ph") (f (quote (("schema" "biome_rowan/serde" "schemars"))))))

(define-public crate-biome_json_syntax-0.5.6 (c (n "biome_json_syntax") (v "0.5.6") (d (list (d (n "biome_rowan") (r "^0.5.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "0hiq68cw68kfibr0c1c97w52y7m6la75him5myg990algqxvk2wz") (f (quote (("schema" "biome_rowan/serde" "schemars"))))))

(define-public crate-biome_json_syntax-0.5.7 (c (n "biome_json_syntax") (v "0.5.7") (d (list (d (n "biome_rowan") (r "^0.5.7") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "15xhpm55zw8j38arc7wyhhfhp1dmbp1q490b74yhss3mgyjmqr7j") (f (quote (("schema" "biome_rowan/serde" "schemars"))))))

