(define-module (crates-io bi om biome_control_flow) #:use-module (crates-io))

(define-public crate-biome_control_flow-0.0.1 (c (n "biome_control_flow") (v "0.0.1") (d (list (d (n "biome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "0b557ldxrrc82572c9rms8xyzm261ypdyq2l6m9fvvnjf9vh14ry")))

(define-public crate-biome_control_flow-0.0.2 (c (n "biome_control_flow") (v "0.0.2") (d (list (d (n "biome_rowan") (r "^0.0.2") (d #t) (k 0)))) (h "0p2jd54ncfp6xjx7m3z7ci1cgwjji4yn1jfp3vrxhkkim6gyl1av")))

(define-public crate-biome_control_flow-0.1.0 (c (n "biome_control_flow") (v "0.1.0") (d (list (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)))) (h "115ladkvcgbr8i5pzkkhh78099531z5acrrhn53l2599b3a06dm3")))

(define-public crate-biome_control_flow-0.2.0 (c (n "biome_control_flow") (v "0.2.0") (d (list (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "036xzki96hw9dj1q33yrrs3fn6xaqni4gpbcjpig51cv8g37zxk1")))

(define-public crate-biome_control_flow-0.3.1 (c (n "biome_control_flow") (v "0.3.1") (d (list (d (n "biome_rowan") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1vy51pcvn5wbi732r4wjya1z8fw8xh4gpk62ilzw5k27m4a8c931")))

(define-public crate-biome_control_flow-0.4.0 (c (n "biome_control_flow") (v "0.4.0") (d (list (d (n "biome_rowan") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "07zrhh3dwcsqqfbr1nyy04ijf4n1j7rhhp4f4yc3ps1lnwg1jahn")))

(define-public crate-biome_control_flow-0.5.0 (c (n "biome_control_flow") (v "0.5.0") (d (list (d (n "biome_rowan") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "01klycr12b5540agvqydgdw5wvwhs1vy2b9hvc7zh8kils8qhhca")))

(define-public crate-biome_control_flow-0.5.5 (c (n "biome_control_flow") (v "0.5.5") (d (list (d (n "biome_rowan") (r "^0.5.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1wnixjbcjrxb3azy9bp1kq1alx0nnc90wpcf02m3yy66pq5n5jf0")))

(define-public crate-biome_control_flow-0.5.6 (c (n "biome_control_flow") (v "0.5.6") (d (list (d (n "biome_rowan") (r "^0.5.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0c68l6vqipg2lxsh91j0yzswj07mm33d4b4mkn448f2hfdkv8jzv")))

(define-public crate-biome_control_flow-0.5.7 (c (n "biome_control_flow") (v "0.5.7") (d (list (d (n "biome_rowan") (r "^0.5.7") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0zz996j09cqnvnljx1slqh9h84k541m59519dlsz3x83fgsrfg8m")))

