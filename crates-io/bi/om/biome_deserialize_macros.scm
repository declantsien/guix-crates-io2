(define-module (crates-io bi om biome_deserialize_macros) #:use-module (crates-io))

(define-public crate-biome_deserialize_macros-0.5.3 (c (n "biome_deserialize_macros") (v "0.5.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0q9wipifq0287aadv3ya6256fbdxh9swcj1n6vwbw966vbkj85yr")))

(define-public crate-biome_deserialize_macros-0.5.4 (c (n "biome_deserialize_macros") (v "0.5.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "198kcq1bsg34vx3ikgh11wlicvjmk2p7dx66rkkbc72l1g006lqp")))

(define-public crate-biome_deserialize_macros-0.5.5 (c (n "biome_deserialize_macros") (v "0.5.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15nxpr3gvw136ylxgxbmg43hcrl9ddcyqkrxmq0zclhfhy706wl7")))

(define-public crate-biome_deserialize_macros-0.5.6 (c (n "biome_deserialize_macros") (v "0.5.6") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ni2phrq9idb93rkx4i0hvnp0vgxiavd9x7qq23wyfyw2d80gfs5")))

(define-public crate-biome_deserialize_macros-0.5.7 (c (n "biome_deserialize_macros") (v "0.5.7") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ya4r0fn9pn24xza1fwmsfjcclb8rnf1wq3bs9xjdk7hqm629h9z")))

(define-public crate-biome_deserialize_macros-0.6.0 (c (n "biome_deserialize_macros") (v "0.6.0") (d (list (d (n "biome_string_case") (r "^0.5.7") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0glf0v7wmjsqywkd816sj3wgsp92ynyzib5vcfgw0ypqzwk2ih87")))

