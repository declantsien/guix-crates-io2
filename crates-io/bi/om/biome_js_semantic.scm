(define-module (crates-io bi om biome_js_semantic) #:use-module (crates-io))

(define-public crate-biome_js_semantic-0.0.1 (c (n "biome_js_semantic") (v "0.0.1") (d (list (d (n "biome_js_syntax") (r "^0.0.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.1") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1sdpc6gv5nqqcs27yx7ms054lszjwfg68b2yvfjgjpz31wsnhhsq")))

(define-public crate-biome_js_semantic-0.0.2 (c (n "biome_js_semantic") (v "0.0.2") (d (list (d (n "biome_js_syntax") (r "^0.0.2") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.2") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0kcacjcxb12w5hmwg7np2jnxd9kj85y7b7bblk27nxx0k2mdps72")))

(define-public crate-biome_js_semantic-0.1.0 (c (n "biome_js_semantic") (v "0.1.0") (d (list (d (n "biome_js_syntax") (r "^0.1.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1xgzmqk49zz4dyvdr6nb5v2incdfhadqr13yifcal4h9gdjhyv5p")))

(define-public crate-biome_js_semantic-0.3.1 (c (n "biome_js_semantic") (v "0.3.1") (d (list (d (n "biome_js_syntax") (r "^0.3.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.3.1") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1n3nngxv36m8c0k96k2kg6f3wcrvkb3dnc93j6d4kvd68w3afal0")))

(define-public crate-biome_js_semantic-0.4.0 (c (n "biome_js_semantic") (v "0.4.0") (d (list (d (n "biome_js_syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0878rdsai3ks4vamw4qx5p06s2p4p5rj76rxs3xbwbjryfcinl55")))

(define-public crate-biome_js_semantic-0.5.5 (c (n "biome_js_semantic") (v "0.5.5") (d (list (d (n "biome_js_syntax") (r "^0.5.5") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.5") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "10ha6knywi07wa9925gj809235dqhsksj9q8rdl1w26hj74m1cxk")))

(define-public crate-biome_js_semantic-0.5.7 (c (n "biome_js_semantic") (v "0.5.7") (d (list (d (n "biome_js_syntax") (r "^0.5.7") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.7") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1yy2axmgvrbfpczrcsnv186xr2bcza9pp3vhgllq9d425457mzgk")))

