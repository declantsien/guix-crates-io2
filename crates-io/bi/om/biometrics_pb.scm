(define-module (crates-io bi om biometrics_pb) #:use-module (crates-io))

(define-public crate-biometrics_pb-0.1.0 (c (n "biometrics_pb") (v "0.1.0") (d (list (d (n "biometrics") (r "^0.1") (d #t) (k 0)) (d (n "buffertk") (r "^0.2") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.2") (d #t) (k 0)))) (h "0p49bgrsypfbr6sn0l97yxv62ld3pxdc5k056jb5ympb9l5ng4xn")))

(define-public crate-biometrics_pb-0.1.1 (c (n "biometrics_pb") (v "0.1.1") (d (list (d (n "biometrics") (r "^0.2") (d #t) (k 0)) (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.2") (d #t) (k 0)))) (h "1l8nz0jap0ksilwyl0c1k1lg1k3sqqy61wra3xr7la2wjgbgxi3j")))

(define-public crate-biometrics_pb-0.1.2 (c (n "biometrics_pb") (v "0.1.2") (d (list (d (n "biometrics") (r "^0.3") (d #t) (k 0)) (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.3") (d #t) (k 0)))) (h "1z9is1va7126g63yx279q0biwv5d75sxgam5l34ib6k9c8yg3x1y")))

(define-public crate-biometrics_pb-0.3.0 (c (n "biometrics_pb") (v "0.3.0") (d (list (d (n "biometrics") (r "^0.5") (d #t) (k 0)) (d (n "buffertk") (r "^0.5") (d #t) (k 0)) (d (n "one_two_eight") (r "^0.4") (d #t) (k 0)) (d (n "prototk") (r "^0.5") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.5") (d #t) (k 0)) (d (n "tuple_key") (r "^0.4") (d #t) (k 0)) (d (n "tuple_key_derive") (r "^0.4") (d #t) (k 0)))) (h "1v15kf91n9acb9yh1d95q4vpd447ib7jcbw7rsj08vz6y6jhg2hc")))

(define-public crate-biometrics_pb-0.4.0 (c (n "biometrics_pb") (v "0.4.0") (d (list (d (n "biometrics") (r "^0.6") (d #t) (k 0)) (d (n "buffertk") (r "^0.6") (d #t) (k 0)) (d (n "one_two_eight") (r "^0.4") (d #t) (k 0)) (d (n "prototk") (r "^0.6") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.6") (d #t) (k 0)) (d (n "tuple_key") (r "^0.5") (d #t) (k 0)) (d (n "tuple_key_derive") (r "^0.5") (d #t) (k 0)))) (h "1zbzl2ays1mb3fwbc78v9nvjmrwyrm97zsihx3k6c3xf3ldyhm80")))

