(define-module (crates-io bi om biome_diagnostics_macros) #:use-module (crates-io))

(define-public crate-biome_diagnostics_macros-0.0.1 (c (n "biome_diagnostics_macros") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "0fm13r9ipr56z30q6azwwbyn65dcvkvsyz0jzvs7z8qy38w4ixsv")))

(define-public crate-biome_diagnostics_macros-0.0.2 (c (n "biome_diagnostics_macros") (v "0.0.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "02lljglgkvax9v3dfmzj3vdh1ihmgx1mab18v1khzncgwdnw3naf")))

(define-public crate-biome_diagnostics_macros-0.1.0 (c (n "biome_diagnostics_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "1rf495acv7s4jy8z59c44yhwxlnaknnxx3p2zfp42qzvb2gfdzl6")))

(define-public crate-biome_diagnostics_macros-0.3.0 (c (n "biome_diagnostics_macros") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "0cxpf3jhzd1qlhk6nnfb4z3qrgjw32ffv5kp5lr5ysk91zr33wzb")))

(define-public crate-biome_diagnostics_macros-0.3.1 (c (n "biome_diagnostics_macros") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "1i6mf4divbdmm04zk0xh5v4hvs0jmm42n7d97vnxhys583mlx6ng")))

(define-public crate-biome_diagnostics_macros-0.4.0 (c (n "biome_diagnostics_macros") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "1dh6xl53p7ddzns9gbpvxz9j766n9va7n4vv8sr9zvjcr5wxhfhp")))

(define-public crate-biome_diagnostics_macros-0.5.0 (c (n "biome_diagnostics_macros") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "1sn8hqvr0bbc6zzxg6d8hjg5gqllm1dax3f2ysjjmalm3l4vh6g8")))

(define-public crate-biome_diagnostics_macros-0.5.1 (c (n "biome_diagnostics_macros") (v "0.5.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "1z3cf0yr1aczj7f421c8f83bzd8k23chf7x055f2ng6qlrz4qwfl")))

(define-public crate-biome_diagnostics_macros-0.5.2 (c (n "biome_diagnostics_macros") (v "0.5.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "1wjlghvz7pg0hsvah06fibiz0cvd65npzk716gahph271a0a5syj")))

(define-public crate-biome_diagnostics_macros-0.5.3 (c (n "biome_diagnostics_macros") (v "0.5.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "0164vnw64pxhqvs736gl1qz6r9jjrxddavg329g6i6ay8i430z6m")))

(define-public crate-biome_diagnostics_macros-0.5.4 (c (n "biome_diagnostics_macros") (v "0.5.4") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "0zzdm8skirp6lqclw59qm16v03ygzhkqpvrdnaiajz36afl0yzxw")))

(define-public crate-biome_diagnostics_macros-0.5.5 (c (n "biome_diagnostics_macros") (v "0.5.5") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "1hxn6kkhvibg9x1ns2k2v605j7w8mf5r7zjmm0r5iw8b03d1ismy")))

(define-public crate-biome_diagnostics_macros-0.5.6 (c (n "biome_diagnostics_macros") (v "0.5.6") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "13pnqgsqcx9ya465y0k06p7j3ngfzsw684j0blygxxn0mbap3i3j")))

(define-public crate-biome_diagnostics_macros-0.5.7 (c (n "biome_diagnostics_macros") (v "0.5.7") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "0fvx2vsxzj167z5z3zzz98kk61v53c8kvii846czp2g7s82fq3sl")))

