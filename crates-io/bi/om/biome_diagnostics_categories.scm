(define-module (crates-io bi om biome_diagnostics_categories) #:use-module (crates-io))

(define-public crate-biome_diagnostics_categories-0.0.1 (c (n "biome_diagnostics_categories") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (k 0)))) (h "1mzwrbmbkfafaf8cfcijxh69rhdhhdvqahgd4inl7kcjpg7sj6fs")))

(define-public crate-biome_diagnostics_categories-0.0.2 (c (n "biome_diagnostics_categories") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hkbxsyhb8khzvgxgv9hza7adf61lniz4c7j61213wgrir9z5gj0")))

(define-public crate-biome_diagnostics_categories-0.1.0 (c (n "biome_diagnostics_categories") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04nxc53kxwhixnwywxmwvm1q9qy21f9n1ksfay5xazwf8v2936da")))

(define-public crate-biome_diagnostics_categories-0.2.0 (c (n "biome_diagnostics_categories") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hs981bwdqdy804lzlfxkqlbi50pl9qpbg53jfmjibivxd7wc90s")))

(define-public crate-biome_diagnostics_categories-0.3.0 (c (n "biome_diagnostics_categories") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1k2k5p4ay55bdfw6mrg7863d0c97grqn6g8m9rbx1xk1by5b2c3k")))

(define-public crate-biome_diagnostics_categories-0.3.1 (c (n "biome_diagnostics_categories") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1m53ar040swy1jr1wbk7qdj4gsyfcrnrk376qsr3hd38i8gyzs54")))

(define-public crate-biome_diagnostics_categories-0.4.0 (c (n "biome_diagnostics_categories") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z45sfgjrxhd1hbamygklschs0g615qn8x6ai9hqgnj4sq1i8y9l")))

(define-public crate-biome_diagnostics_categories-0.5.0 (c (n "biome_diagnostics_categories") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0skfysaacm4avg4vggfbjrqm53z1czwpdl462ir2simr2m9mwlb5")))

(define-public crate-biome_diagnostics_categories-0.5.1 (c (n "biome_diagnostics_categories") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "126q9lnivkz97zjj7a3yn8ryxzfkwyi8j867gwsjnypkj6kz4532")))

(define-public crate-biome_diagnostics_categories-0.5.2 (c (n "biome_diagnostics_categories") (v "0.5.2") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04hsbd1054lxp38ivqc229fiifrz58x4xb06lw1k4xgqbv92hp87")))

(define-public crate-biome_diagnostics_categories-0.5.3 (c (n "biome_diagnostics_categories") (v "0.5.3") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11d0knhd0vvcxq9cb9am342cpd17rknghqycpv39vpn82b782kkn")))

(define-public crate-biome_diagnostics_categories-0.5.4 (c (n "biome_diagnostics_categories") (v "0.5.4") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rhpwhdwzmp6pg2p11zl6frnkg4940j3rji6c1z091d3gw61wdsc")))

(define-public crate-biome_diagnostics_categories-0.5.5 (c (n "biome_diagnostics_categories") (v "0.5.5") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cpnvn61xi20ysby6vwgdaaj754wjsa44c3mk1qi9dl1v0l9wdrp")))

(define-public crate-biome_diagnostics_categories-0.5.6 (c (n "biome_diagnostics_categories") (v "0.5.6") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gaya43m4wyq5ycibg5d9v1z2hcwph3q29l52b7cfimgnqd3r5vx")))

(define-public crate-biome_diagnostics_categories-0.5.7 (c (n "biome_diagnostics_categories") (v "0.5.7") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 1)) (d (n "schemars") (r "^0.8.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06adcq3ywjd0lh2x2wzp0xlmdhpmq1px5xcgv6cg3qifibb80843")))

