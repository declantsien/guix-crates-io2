(define-module (crates-io bi om biome_markup) #:use-module (crates-io))

(define-public crate-biome_markup-0.0.1 (c (n "biome_markup") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "0s6pnnbwf0g2svpf5k5gaxd8bdhdiwaywqbclm061jhsdxm3xqpz")))

(define-public crate-biome_markup-0.0.2 (c (n "biome_markup") (v "0.0.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "0m2j0qfqhsrjbs2774xa7x6shqjhm859dvd7qci9kxkwhw81r5ch")))

(define-public crate-biome_markup-0.1.0 (c (n "biome_markup") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "0zdqna2dwcb4va20x3960pp6hsrbbcc0wqj9qz0hpr7ngypdl00q")))

(define-public crate-biome_markup-0.3.0 (c (n "biome_markup") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "1kc3995v9ry69bbz666wpfgpxpab06qm21mi13frjr4g0vp0kw46")))

(define-public crate-biome_markup-0.3.1 (c (n "biome_markup") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "00jy0j280i4dn1r5vv0b427c2cr3a18qwfb8b6jsfilk3hj5pdwl")))

(define-public crate-biome_markup-0.4.0 (c (n "biome_markup") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "1xf1hmc9q51z588hkrv83w40f8j9xsyh3xrm0q7awzgfxmbj5dan")))

(define-public crate-biome_markup-0.5.0 (c (n "biome_markup") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "1g4z3qng74g58ali4xrc622lan18411b2k6m3z3x996gd2m3wb4z")))

(define-public crate-biome_markup-0.5.5 (c (n "biome_markup") (v "0.5.5") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "0xrrlrmyxk53lm9gw7yf39znp2bn2pia4qg2v95a51qxd4rw9z57")))

(define-public crate-biome_markup-0.5.6 (c (n "biome_markup") (v "0.5.6") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "1mp25ayc4y058mhlwahsxx29dsnnzm4qj9vr8kmgxv5x2c87xgjz")))

(define-public crate-biome_markup-0.5.7 (c (n "biome_markup") (v "0.5.7") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "177xwapwn5gj17ci4wij0fhi1r7l9q21dllpir9995arj77i2zsa")))

