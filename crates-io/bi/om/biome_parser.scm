(define-module (crates-io bi om biome_parser) #:use-module (crates-io))

(define-public crate-biome_parser-0.0.1 (c (n "biome_parser") (v "0.0.1") (d (list (d (n "biome_console") (r "^0.0.1") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.0.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.1") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)))) (h "11r34d65r8cgcfb4a55dikccpcdavvj9wxxd7630y1svzilijz39")))

(define-public crate-biome_parser-0.0.2 (c (n "biome_parser") (v "0.0.2") (d (list (d (n "biome_console") (r "^0.0.2") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.0.2") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.2") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)))) (h "0yx3i3hv9zb2r3z42rjks88qvh3kvx5x40gfbn64vyga065g21g1")))

(define-public crate-biome_parser-0.1.0 (c (n "biome_parser") (v "0.1.0") (d (list (d (n "biome_console") (r "^0.1.0") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.1.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)))) (h "026jk8mbfw3n0fvsv4jcsxll1alkm3n2x9cpir5dzqf7x2krvnwa")))

(define-public crate-biome_parser-0.2.0 (c (n "biome_parser") (v "0.2.0") (d (list (d (n "biome_console") (r "^0.1.0") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.1.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)))) (h "1jz8dwqfbaq9mj32vqvji0pjbhl0ds3lcd5rm03nxfx7f8yv6jfw")))

(define-public crate-biome_parser-0.3.1 (c (n "biome_parser") (v "0.3.1") (d (list (d (n "biome_console") (r "^0.3.1") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.3.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.3.1") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)))) (h "0qgp5djgkblm9lc1643hnkcqa6f4gqb8l9zk439wz06f5651nlrk")))

(define-public crate-biome_parser-0.4.0 (c (n "biome_parser") (v "0.4.0") (d (list (d (n "biome_console") (r "^0.4.0") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.4.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.4.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)))) (h "1jhw4h2mpkdpbpwkx9ryl1i3f90i2xr1nfzk5kg6sk2iamr0h49z")))

(define-public crate-biome_parser-0.5.0 (c (n "biome_parser") (v "0.5.0") (d (list (d (n "biome_console") (r "^0.5.0") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.5.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)))) (h "01k708ylsvdffa6sm50b4dgb4cpc7yp49ap6vpi0q57x9yhgnr4k")))

(define-public crate-biome_parser-0.5.5 (c (n "biome_parser") (v "0.5.5") (d (list (d (n "biome_console") (r "^0.5.5") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.5.5") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.5") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)))) (h "1zlz1j303apkwadr90ybljc5n742mnd7x7x4f8mcailbhchsavfb")))

(define-public crate-biome_parser-0.5.6 (c (n "biome_parser") (v "0.5.6") (d (list (d (n "biome_console") (r "^0.5.6") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.5.6") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.6") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)))) (h "138vxvr0ysk4mgcs81n04pwg9hdrc1yjzn7b3w0k987hr50pmkka")))

(define-public crate-biome_parser-0.5.7 (c (n "biome_parser") (v "0.5.7") (d (list (d (n "biome_console") (r "^0.5.7") (d #t) (k 0)) (d (n "biome_diagnostics") (r "^0.5.7") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.7") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "drop_bomb") (r "^0.1.5") (d #t) (k 0)))) (h "16z1ilclgvqpzsws6c8ziz4m03x5l574prn0smqn621cyfcxjpcm")))

