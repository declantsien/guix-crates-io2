(define-module (crates-io bi om biome_unicode_table) #:use-module (crates-io))

(define-public crate-biome_unicode_table-0.4.0 (c (n "biome_unicode_table") (v "0.4.0") (h "00ilvfmv86zgk9dn4g4pxzvyyhbvpgpgfp7yfgzgq1dp658g77ir")))

(define-public crate-biome_unicode_table-0.5.0 (c (n "biome_unicode_table") (v "0.5.0") (h "01v3a8x2h4qwizf9kq8gs4i94jzf9c1kl10f91wi1s2q2ikmwkhc")))

(define-public crate-biome_unicode_table-0.5.5 (c (n "biome_unicode_table") (v "0.5.5") (h "132fn0awlnyivnkf6jlj420s0s259a37ad7bqryabpsq5s3p28pk")))

(define-public crate-biome_unicode_table-0.5.6 (c (n "biome_unicode_table") (v "0.5.6") (h "1x0zh05ynzrq7w82dv2irlvgm0dfffcflryy8wva4sj6iqxqv2sj")))

(define-public crate-biome_unicode_table-0.5.7 (c (n "biome_unicode_table") (v "0.5.7") (h "0lh9hy1ny953890r6w2kgzr0a63g2sndmnziiajq08dh6i6n1s47")))

