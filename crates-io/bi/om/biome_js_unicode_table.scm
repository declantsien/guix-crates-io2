(define-module (crates-io bi om biome_js_unicode_table) #:use-module (crates-io))

(define-public crate-biome_js_unicode_table-0.0.1 (c (n "biome_js_unicode_table") (v "0.0.1") (h "023cd8n56jgh1wv6y1hbqxrrdw7lsb1h50qs9i1aif8apls2hyi7")))

(define-public crate-biome_js_unicode_table-0.0.2 (c (n "biome_js_unicode_table") (v "0.0.2") (h "19apwgvw3pwc2ppz72fiyg2lrsd96qcq1357zp0i1gw36nsdw948")))

(define-public crate-biome_js_unicode_table-0.1.0 (c (n "biome_js_unicode_table") (v "0.1.0") (h "068a87h19kk146k002n2vljdp1z61ymz9js573xg6mapjchy3gvf")))

(define-public crate-biome_js_unicode_table-0.3.0 (c (n "biome_js_unicode_table") (v "0.3.0") (h "19yrsiy07glmy8bijazpbzaf0vkqdznqhp31cr9g3xfps16v6c51")))

(define-public crate-biome_js_unicode_table-0.3.1 (c (n "biome_js_unicode_table") (v "0.3.1") (h "1fbmhs56hd9ncm95i3zl7f7c72dz46q9wnh4ccblxmz5l394s5vb")))

