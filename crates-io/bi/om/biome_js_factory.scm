(define-module (crates-io bi om biome_js_factory) #:use-module (crates-io))

(define-public crate-biome_js_factory-0.0.1 (c (n "biome_js_factory") (v "0.0.1") (d (list (d (n "biome_js_syntax") (r "^0.0.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.1") (d #t) (k 0)))) (h "11d9av3xqc261d0pz9kra0pwjb4lmamj5yvxdyd0mv0442cqyjpa")))

(define-public crate-biome_js_factory-0.0.2 (c (n "biome_js_factory") (v "0.0.2") (d (list (d (n "biome_js_syntax") (r "^0.0.2") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.0.2") (d #t) (k 0)))) (h "0zzv3mim10wfbxsh31av2dgyxjc2lw51z4kppxd6vl1ilvjny4xk")))

(define-public crate-biome_js_factory-0.1.0 (c (n "biome_js_factory") (v "0.1.0") (d (list (d (n "biome_js_syntax") (r "^0.1.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.1.0") (d #t) (k 0)))) (h "15vqalwdvh7xm0l30rc33f9d2pyk73i6h63j628270cjigz65yry")))

(define-public crate-biome_js_factory-0.3.1 (c (n "biome_js_factory") (v "0.3.1") (d (list (d (n "biome_js_syntax") (r "^0.3.1") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.3.1") (d #t) (k 0)))) (h "0alpar2bjnps18ld519kq371xyd2bprchbayfrxp6g6509qdj7pb")))

(define-public crate-biome_js_factory-0.4.0 (c (n "biome_js_factory") (v "0.4.0") (d (list (d (n "biome_js_syntax") (r "^0.4.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.4.0") (d #t) (k 0)))) (h "1rcw92ayb07kkg1g55z63f1r829krf2ijbq0ia6yknmvxhd4dria")))

(define-public crate-biome_js_factory-0.5.0 (c (n "biome_js_factory") (v "0.5.0") (d (list (d (n "biome_js_syntax") (r "^0.5.0") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.0") (d #t) (k 0)))) (h "132hknjqxxcvdryl8az3rdgh2nhk0i05q7f0r8zrwzf6dz0lvzcp")))

(define-public crate-biome_js_factory-0.5.5 (c (n "biome_js_factory") (v "0.5.5") (d (list (d (n "biome_js_syntax") (r "^0.5.5") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.5") (d #t) (k 0)))) (h "1yswg5s2drsj21m00gswb1qnpr79yqqi02ss31z96acy7cjdvfy3")))

(define-public crate-biome_js_factory-0.5.6 (c (n "biome_js_factory") (v "0.5.6") (d (list (d (n "biome_js_syntax") (r "^0.5.6") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.6") (d #t) (k 0)))) (h "01qdy15gg57kh2saplkm2g3cf14q11qppskfxzlg1awaf1zisbiv")))

(define-public crate-biome_js_factory-0.5.7 (c (n "biome_js_factory") (v "0.5.7") (d (list (d (n "biome_js_syntax") (r "^0.5.7") (d #t) (k 0)) (d (n "biome_rowan") (r "^0.5.7") (d #t) (k 0)))) (h "1dqpwzmlxqa5q2swgsn4vww35crfddpzk41bs51f4vnivzs4g61w")))

