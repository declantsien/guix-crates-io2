(define-module (crates-io bi pe bipe) #:use-module (crates-io))

(define-public crate-bipe-0.1.0 (c (n "bipe") (v "0.1.0") (d (list (d (n "async-global-executor") (r "^1") (d #t) (k 2)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.16") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "rtrb") (r "^0.1.4") (d #t) (k 0)))) (h "0jhgphzbcp8kfz0k45qq631fr3m3d9j0a4j09blm47kq15czpsxa")))

(define-public crate-bipe-0.1.1 (c (n "bipe") (v "0.1.1") (d (list (d (n "async-global-executor") (r "^1") (d #t) (k 2)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.16") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "rtrb") (r "^0.1.4") (d #t) (k 0)))) (h "0vd5g6j9fzma1s2pxz3fny75jn1dfyv23znsxzcmfk6bgblw6z89")))

(define-public crate-bipe-0.2.0 (c (n "bipe") (v "0.2.0") (d (list (d (n "async-global-executor") (r "^1") (d #t) (k 2)) (d (n "concurrent-queue") (r "^1.2.2") (d #t) (k 0)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.16") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "rtrb") (r "^0.1.4") (d #t) (k 0)))) (h "1222m6s7w11cxmvwj1f42z9hkqys25cywvmw3f2qjf25wqylxsxg")))

(define-public crate-bipe-0.2.1 (c (n "bipe") (v "0.2.1") (d (list (d (n "async-global-executor") (r "^1") (d #t) (k 2)) (d (n "concurrent-queue") (r "^1.2.2") (d #t) (k 0)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.16") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "rtrb") (r "^0.1.4") (d #t) (k 0)))) (h "0p8mq40a9z21nmdfgbxmfz6nf3h0yhqkmjrilh6x68brflmbnmkb")))

(define-public crate-bipe-0.2.2 (c (n "bipe") (v "0.2.2") (d (list (d (n "async-global-executor") (r "^1") (d #t) (k 2)) (d (n "concurrent-queue") (r "^1.2.2") (d #t) (k 0)) (d (n "event-listener") (r "^2.5.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.16") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 0)) (d (n "rtrb") (r "^0.2") (d #t) (k 0)))) (h "0pllygq691sg8bf1ykr593kg4qidggscvk54f1wz1ls3qdz4fayq")))

