(define-module (crates-io bi ts bits-cli) #:use-module (crates-io))

(define-public crate-bits-cli-0.1.0 (c (n "bits-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "half") (r "^2.3.1") (o #t) (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "0bggxak5c9mbp22bk3zgli58idpvm32g2s6g1cj8f33lzmifd7n1") (f (quote (("default" "half")))) (s 2) (e (quote (("half" "dep:half"))))))

