(define-module (crates-io bi ts bitset) #:use-module (crates-io))

(define-public crate-bitset-0.1.0 (c (n "bitset") (v "0.1.0") (h "1dlrdqmwdq3gbycn5imhzja0ardbz8h797fhn3d8gdqq1fw58p4c")))

(define-public crate-bitset-0.1.1 (c (n "bitset") (v "0.1.1") (h "0yqd2z2mr77gcgfhl1a1f7nmj07q1wfh6cwg8sw83pi5ad4w5lrm")))

(define-public crate-bitset-0.1.2 (c (n "bitset") (v "0.1.2") (h "0svz7wza3b9sphr86f135qmkxvhjyvln604v5hckmzs4l0sh8ifd")))

