(define-module (crates-io bi ts bits128) #:use-module (crates-io))

(define-public crate-bits128-0.1.0 (c (n "bits128") (v "0.1.0") (h "0zanznxp21l1f6k4h87q3h0wwwdnl9s9qnirqd9xbypzy7rjn7pm")))

(define-public crate-bits128-0.1.1 (c (n "bits128") (v "0.1.1") (h "1643s1wbi1ahgd728qv837zmdvgzhn0xc6h29ssbbd2jvna88msq")))

(define-public crate-bits128-0.1.3 (c (n "bits128") (v "0.1.3") (h "1la168pxqqqy57kl4jmpbgq6s75l9rp5gmwdwbhjz3mb7879nky1")))

