(define-module (crates-io bi ts bitset-core) #:use-module (crates-io))

(define-public crate-bitset-core-0.1.0 (c (n "bitset-core") (v "0.1.0") (h "0l1036w0069r3nn9w2p9yamwpdsy59xzf1fr5kyknyicjfr3g8g0") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitset-core-0.1.1 (c (n "bitset-core") (v "0.1.1") (h "17mqz0i7khszgdcmlr46aw6r5jlnbnmj0a9wl18xia8anfyg28gl") (f (quote (("std") ("default" "std"))))))

