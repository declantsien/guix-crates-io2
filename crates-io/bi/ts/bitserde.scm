(define-module (crates-io bi ts bitserde) #:use-module (crates-io))

(define-public crate-bitserde-0.1.0 (c (n "bitserde") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.18.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)))) (h "10r5w55s36vk9aw8lzcj9kilxawmi182h272s0w4l5dc772mcnj4") (y #t)))

(define-public crate-bitserde-0.1.1 (c (n "bitserde") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.18.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j7yz62vs69af4nx2jvsv49qspg44izmfwvsgjqvclds0kmmj6v6") (y #t)))

