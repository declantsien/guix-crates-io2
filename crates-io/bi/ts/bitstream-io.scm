(define-module (crates-io bi ts bitstream-io) #:use-module (crates-io))

(define-public crate-bitstream-io-0.2.0 (c (n "bitstream-io") (v "0.2.0") (h "02yi9vapx8x92npc1ry2i1f9smqncc455xks70ng02q3xc3rvfmf")))

(define-public crate-bitstream-io-0.3.0 (c (n "bitstream-io") (v "0.3.0") (h "19dqqrm5qhfysf6agdc5ls95a02b7yrs3gnm0h5a1fkgjqkgw7bw")))

(define-public crate-bitstream-io-0.3.1 (c (n "bitstream-io") (v "0.3.1") (h "0s38hbwjv8jr8g2wnxjnfj81d980fy8nikdmb5pbrihrcnk8kmhq")))

(define-public crate-bitstream-io-0.4.0 (c (n "bitstream-io") (v "0.4.0") (h "12y6wr479j172hngmm7g0s82zcpxjbwpfz6k2dl5ff7f8rfks9zl")))

(define-public crate-bitstream-io-0.5.0 (c (n "bitstream-io") (v "0.5.0") (h "19vwspdbk4ifpkw7npglba185rrckjzi39a8mvsw0kh297ravwm6")))

(define-public crate-bitstream-io-0.5.1 (c (n "bitstream-io") (v "0.5.1") (h "1ii1k9451bwbp2kr0k0qs5g2gjch3j58k6x0q9glwy6cmdqhm9dd")))

(define-public crate-bitstream-io-0.6.0 (c (n "bitstream-io") (v "0.6.0") (h "12dvr768msykj9jq3g36cj2sxvpqcjnivpkfy7vy7gkgps4a92rj")))

(define-public crate-bitstream-io-0.6.1 (c (n "bitstream-io") (v "0.6.1") (h "1lyw2wsdwzbms7p7n9p6azv7c2dci3i6vyasw5mdsmhqm8agwwpp")))

(define-public crate-bitstream-io-0.6.3 (c (n "bitstream-io") (v "0.6.3") (h "0rqdr4bvvv21s7sv75g8wvv524cppdmm9isn145i4nif0iznvwj8")))

(define-public crate-bitstream-io-0.6.4 (c (n "bitstream-io") (v "0.6.4") (h "1jqhxi3adxj6ri4rvgf1vsls6fsggskckrbbdrwjsc37cm4x71jk")))

(define-public crate-bitstream-io-0.6.5 (c (n "bitstream-io") (v "0.6.5") (h "1aq0ms239hxj0i320ywavgsci1dk07hsd5i008yy1y8x33ibif3k")))

(define-public crate-bitstream-io-0.7.0 (c (n "bitstream-io") (v "0.7.0") (h "0m7n59jp90f8nyin186qhsigbq857xkn3qf338vzcqz9kr4d2nlp")))

(define-public crate-bitstream-io-0.8.0 (c (n "bitstream-io") (v "0.8.0") (h "1r7gf1kbrwgda0wbw11sp7a0zsbfnd7264qp6fk86zis8mch82zw")))

(define-public crate-bitstream-io-0.8.1 (c (n "bitstream-io") (v "0.8.1") (h "1gfg7zxzk1f5p7245dxqc0mm5w259539manq2nxh2hcdbplg8c5w")))

(define-public crate-bitstream-io-0.8.2 (c (n "bitstream-io") (v "0.8.2") (h "19v1m8dmdk5p4wm3jiwx3yzil56d51p5gbkgppdazm33m2iqaf9j")))

(define-public crate-bitstream-io-0.8.3 (c (n "bitstream-io") (v "0.8.3") (h "0a36nlpasgkr5vrq3xmvand6fqxklvjpc8bdz8h50r406d779p40")))

(define-public crate-bitstream-io-0.8.4 (c (n "bitstream-io") (v "0.8.4") (h "0lyn1mb8y0bkh7c1nz7q9cixilqnwvvs1gkd0kk9jqwr7mlannrr")))

(define-public crate-bitstream-io-0.8.5 (c (n "bitstream-io") (v "0.8.5") (h "00a6wy54s1dmadm5xz8k2cbsd7ixvm48mlc45bk0fdy0pbra6jk1")))

(define-public crate-bitstream-io-0.9.0 (c (n "bitstream-io") (v "0.9.0") (h "0gvp9n8njh5a5hgs75l4w7a60aikrl331ss1zlx3zn5nwv26vdki")))

(define-public crate-bitstream-io-1.0.0 (c (n "bitstream-io") (v "1.0.0") (h "01pyk3pipwcbaghi7f0lmp3izjl902cv21yf4b1v5nipkrrrqlq3")))

(define-public crate-bitstream-io-1.1.0 (c (n "bitstream-io") (v "1.1.0") (h "1k4bpqicna7vdkad22qicq3bfcdfzwkb0is7d39s9bmsjc810nhz")))

(define-public crate-bitstream-io-1.2.0 (c (n "bitstream-io") (v "1.2.0") (h "12z6qd7i9dfcxq4hz4vm4j0i0asa231rhqsqk2y567hrfpxrq7sj")))

(define-public crate-bitstream-io-1.3.0 (c (n "bitstream-io") (v "1.3.0") (h "18ncp4wdf44hx099l3wi9rlsffci10h7appy9a555bivyq2rjhis")))

(define-public crate-bitstream-io-1.4.0 (c (n "bitstream-io") (v "1.4.0") (h "1a5zlppsij850s5flf2dr7h8qk1gcal3y1q6fck6g9l6r23khan2")))

(define-public crate-bitstream-io-1.5.0 (c (n "bitstream-io") (v "1.5.0") (h "15sqv9z1l8gdy09kxbhp1bg2x5mlwi1j1h645mnxrxlbnzyj9mcp")))

(define-public crate-bitstream-io-1.6.0 (c (n "bitstream-io") (v "1.6.0") (h "0kjnrfra7p5ngxhr1q3jk7fjqv6076vjj7xxs0gyzx5afl4hfa4x")))

(define-public crate-bitstream-io-1.7.0 (c (n "bitstream-io") (v "1.7.0") (h "1mriigaykjvfgnh4hcngav932ycb2mmdsssdqpr2v8l5rdllfw42")))

(define-public crate-bitstream-io-1.8.0 (c (n "bitstream-io") (v "1.8.0") (h "0q6gq66dlysp78fw2x1jjz60faxnand10i086v8bw22r9gkzkgh2")))

(define-public crate-bitstream-io-1.9.0 (c (n "bitstream-io") (v "1.9.0") (h "17lb5890isl7p8xbzrawvgf2razd6qdf3biz2w7bcrcvp9a18h4b")))

(define-public crate-bitstream-io-1.10.0 (c (n "bitstream-io") (v "1.10.0") (h "0kq7hgc66pkbg9kwp8qazaz5x0dagaihml6b8ixsa17xb5k5fig4")))

(define-public crate-bitstream-io-2.0.0 (c (n "bitstream-io") (v "2.0.0") (h "1fvlsb04vby8h9v4xk97fmvczlv3n7xwrh4dj4a16l7qrh57dsx5")))

(define-public crate-bitstream-io-2.1.0 (c (n "bitstream-io") (v "2.1.0") (h "0glf75c14l23bhb0fg3h7nbk317whzivvabnhv2x7mg2awjkljcp")))

(define-public crate-bitstream-io-2.2.0 (c (n "bitstream-io") (v "2.2.0") (h "1nkny66c4hagpc6l656jvvlnz212msv6icca0f0jw7hpa6d9ij86")))

(define-public crate-bitstream-io-2.3.0 (c (n "bitstream-io") (v "2.3.0") (d (list (d (n "core2") (r "^0.4") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0vj4f4kg3ls3j41180a7ia65rj2p762mbzimm0bxiw22ds2x24kw") (f (quote (("std") ("default" "std") ("alloc" "core2"))))))

