(define-module (crates-io bi ts bitstruct) #:use-module (crates-io))

(define-public crate-bitstruct-0.1.0 (c (n "bitstruct") (v "0.1.0") (d (list (d (n "bitstruct_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1rr8ir6pwvzi8badz7zwinnsdlf8zv453jcgby5h2fi197gvzgd7")))

(define-public crate-bitstruct-0.1.1 (c (n "bitstruct") (v "0.1.1") (d (list (d (n "bitstruct_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1d0pj062fwp322v8wr9j595kgvdmxl3y7bqxx92ay2dg28whrcd1")))

