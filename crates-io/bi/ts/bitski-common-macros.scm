(define-module (crates-io bi ts bitski-common-macros) #:use-module (crates-io))

(define-public crate-bitski-common-macros-0.1.0 (c (n "bitski-common-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.45") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "0n66j3i85r5csnzvplw2653jz6j3z1vyyj0np52m3cb21b2abad9") (f (quote (("doc" "tracing-subscriber" "uuid") ("default")))) (y #t)))

