(define-module (crates-io bi ts bitski-chain-models) #:use-module (crates-io))

(define-public crate-bitski-chain-models-0.1.0 (c (n "bitski-chain-models") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g913lginwbp14cgnx6qa4qh1vv3r5jfr1x8r4n4dnan0li0j2wd")))

(define-public crate-bitski-chain-models-0.2.0 (c (n "bitski-chain-models") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d6cgrjxrxvnpk73p647igc35zgf1887crf0lpaq31hjil2sany0")))

(define-public crate-bitski-chain-models-0.2.1 (c (n "bitski-chain-models") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y3g83xmlgs26nf4gc8jq3zapk0c9c5pwk5grczzjd5a50ybzf5q")))

(define-public crate-bitski-chain-models-0.2.2 (c (n "bitski-chain-models") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dj2dq9gnsd5ig61dp7lfb931w7vj95njxkm0gq25gb0l0l4rmb6")))

(define-public crate-bitski-chain-models-0.2.3 (c (n "bitski-chain-models") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14zbbypi5sqda6vzkdra7877siyac115jf5k5bdb2wv2db1dpsyj")))

(define-public crate-bitski-chain-models-0.2.4 (c (n "bitski-chain-models") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1129xrql3j6vgc4i32glnp81gp2v8lcnnfrmf8cixjnlni0d37a5")))

