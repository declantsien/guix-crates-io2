(define-module (crates-io bi ts bitsock) #:use-module (crates-io))

(define-public crate-bitsock-0.1.0 (c (n "bitsock") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)))) (h "0fbky31bnp1wviycn64kjgsyl9s0hpqgq95bv5fq7pdgmzv997a9")))

(define-public crate-bitsock-0.1.1 (c (n "bitsock") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)))) (h "03kdr0sml494dav8kb2c9mwlxxax26bvbwml5k0py9v34bm2s01h")))

