(define-module (crates-io bi ts bits_rs) #:use-module (crates-io))

(define-public crate-bits_rs-0.1.0 (c (n "bits_rs") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "01ra5hjj0dsrbyznb0b6q2s6c3q6f0gc5cak77qn65cmvnvigwfn")))

(define-public crate-bits_rs-0.1.1 (c (n "bits_rs") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "02fvbc42fvgk3q6dqa2p3irfchxixv8rf34x2ns0kx96jk4pjpy2")))

