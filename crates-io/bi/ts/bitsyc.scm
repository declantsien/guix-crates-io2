(define-module (crates-io bi ts bitsyc) #:use-module (crates-io))

(define-public crate-bitsyc-0.1.0 (c (n "bitsyc") (v "0.1.0") (h "1wx9ls5sff685rhwfafjyphhx1ng60q9lcisaslic42807gdm5k3")))

(define-public crate-bitsyc-0.2.0 (c (n "bitsyc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pw0vklir0xaj6b9q9razzg50p18lmb9jwlmrc3zk8zrlv7qccsr")))

