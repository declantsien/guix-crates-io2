(define-module (crates-io bi ts bitstream) #:use-module (crates-io))

(define-public crate-bitstream-0.1.0 (c (n "bitstream") (v "0.1.0") (h "1qzffxljzk6892ih21ngwiyrk8x73lmykpxvxfgbaqn9dm39nf2s")))

(define-public crate-bitstream-0.1.1 (c (n "bitstream") (v "0.1.1") (h "171h9w3fga61ka9wgf6va09lki99nw8383mlk5dfyhzfax0m91cf") (f (quote (("unstable"))))))

