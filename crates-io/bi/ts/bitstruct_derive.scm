(define-module (crates-io bi ts bitstruct_derive) #:use-module (crates-io))

(define-public crate-bitstruct_derive-0.1.0 (c (n "bitstruct_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09zdxlzjz7wadm33nnqhf12m9dwas02c494pxca0sx9b5h11kz9m")))

