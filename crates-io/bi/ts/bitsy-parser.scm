(define-module (crates-io bi ts bitsy-parser) #:use-module (crates-io))

(define-public crate-bitsy-parser-0.65.0 (c (n "bitsy-parser") (v "0.65.0") (h "1s3vmpkqfi510xm3sj90dv8lpylvnrsvv16fwvcj2mx5389nql3n")))

(define-public crate-bitsy-parser-0.65.1 (c (n "bitsy-parser") (v "0.65.1") (h "08fi5ds5yszpqj5yjp261jhjdcl7ww85xr52yfwxw7ir8a5n5rq9")))

(define-public crate-bitsy-parser-0.65.2 (c (n "bitsy-parser") (v "0.65.2") (h "18cl02x09532vrbfxx1c2wwf7fyygm137ikqmxqjjlbv2cdavn0s")))

(define-public crate-bitsy-parser-0.65.3 (c (n "bitsy-parser") (v "0.65.3") (h "0lggsgxl3ynkc51biglaf8p3zqkfrjixlgswp6hzsng5rb89qhig")))

(define-public crate-bitsy-parser-0.65.4 (c (n "bitsy-parser") (v "0.65.4") (d (list (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "10j9gj39ca93npmp6q3ha1hdqkx2f87z0q1hg5968gdbxp8p2783")))

(define-public crate-bitsy-parser-0.65.5 (c (n "bitsy-parser") (v "0.65.5") (d (list (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1grdhzisdq6cs05jif5w5b1q4xm1fk52d0plkfvkzfz668qxjava")))

(define-public crate-bitsy-parser-0.65.6 (c (n "bitsy-parser") (v "0.65.6") (d (list (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "11iz3g1rxj95xxk5h33w28nq28b1k3skfsb5mdbkd3mf68i0605l")))

(define-public crate-bitsy-parser-0.65.7 (c (n "bitsy-parser") (v "0.65.7") (d (list (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1xs3b766flamqm3bf5a0srrhfzyqrvm5i0slf1rqb3yz0zah4w3l")))

(define-public crate-bitsy-parser-0.65.8 (c (n "bitsy-parser") (v "0.65.8") (d (list (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1g7j2s0i0z046dxvakay9ilmijg7k78h5cf1mwyjqvjwhifys6x3")))

(define-public crate-bitsy-parser-0.65.9 (c (n "bitsy-parser") (v "0.65.9") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0mpfv69bc79j2l2iqm1fpjnn3p0b718krc51n80adpf6j3lg8vsm")))

(define-public crate-bitsy-parser-0.65.10 (c (n "bitsy-parser") (v "0.65.10") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0d4xgf8m22jzniwd746m65fczcl8j3g9xjm3gckdry8hqgwpg8ay")))

(define-public crate-bitsy-parser-0.65.11 (c (n "bitsy-parser") (v "0.65.11") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1cy31fw6lnad7106dgra713nyca19cgmq7pfbrm7phl1l69xh886")))

(define-public crate-bitsy-parser-0.65.12 (c (n "bitsy-parser") (v "0.65.12") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0xln7i76ilpn2zxri0ihylnqn82rgdsp8s3f1mhhw4c9hv4cqnwj")))

(define-public crate-bitsy-parser-0.65.13 (c (n "bitsy-parser") (v "0.65.13") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1xkiyfxgwsnym274qkm65w1affs9f73jgmswyqc6v0v2bzcqlwif")))

(define-public crate-bitsy-parser-0.70.0 (c (n "bitsy-parser") (v "0.70.0") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0n40bhd3dk6h4ydbsja0nxlp4iqdyd54v1nv0vbk8mf6gax1x5bl")))

(define-public crate-bitsy-parser-0.70.1 (c (n "bitsy-parser") (v "0.70.1") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0wf6wpz4mcqqsyp4svdd9vw02yi68hnw0v47j5f9cvmqav0n1xz6")))

(define-public crate-bitsy-parser-0.70.2 (c (n "bitsy-parser") (v "0.70.2") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0jscr0grqms4i761wiznm1dmpvdisf8pamfmi7ffx9jdii7wxv8a")))

(define-public crate-bitsy-parser-0.71.1 (c (n "bitsy-parser") (v "0.71.1") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0r6nkd5fijkaw7hffwjmg5jg2lj2682p6ak1fsdi61kwnh3x8y8q")))

(define-public crate-bitsy-parser-0.71.2 (c (n "bitsy-parser") (v "0.71.2") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1hbqn6xhk186v8k88nnpm0jbasaw0ckmpk3w26m2cmzlhas3riaf")))

(define-public crate-bitsy-parser-0.71.3 (c (n "bitsy-parser") (v "0.71.3") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "11wqcwhdzz933wwpb51r2hn0ldqjv838cwh03a6rd46wgzw5a0nl")))

(define-public crate-bitsy-parser-0.71.4 (c (n "bitsy-parser") (v "0.71.4") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0vgh4rj8b14rdp7ks6j6lxg3scrc88py2dmv05ysv1v6wl2ppvzb")))

(define-public crate-bitsy-parser-0.71.5 (c (n "bitsy-parser") (v "0.71.5") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1bbs2aspaplqwvr0d9bxxl2w4a7laik9l7ny6p8r00mr9ai2v94c")))

(define-public crate-bitsy-parser-0.71.6 (c (n "bitsy-parser") (v "0.71.6") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0fjvb283dbhi75dqyix4vfa1af4w6c8501bx50r1drflk81ifamz")))

(define-public crate-bitsy-parser-0.71.7 (c (n "bitsy-parser") (v "0.71.7") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0x3l58ss9zb3syhswhqfffzrldgnjj2klvr0bsf78mrnnzidqcrn")))

(define-public crate-bitsy-parser-0.72.0 (c (n "bitsy-parser") (v "0.72.0") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1i85iy012350bp2mpw48pvb9c97sn7ixj7a6gfqjqjwng10q7xd1")))

(define-public crate-bitsy-parser-0.72.1 (c (n "bitsy-parser") (v "0.72.1") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1l5f69fld5mn52smpzwvlx0ikyjgdaf6myd14d6s4f68siibcmm2")))

(define-public crate-bitsy-parser-0.72.2 (c (n "bitsy-parser") (v "0.72.2") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0x4md4g163cldl0ykydmgy93lrsca7qzaycvrbrn75gq9cyslfi2")))

(define-public crate-bitsy-parser-0.72.3 (c (n "bitsy-parser") (v "0.72.3") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "050vvmwvkyqf355wnk3r00fxd91x97lh3n3wdnk5h30wx648b7cd")))

(define-public crate-bitsy-parser-0.72.4 (c (n "bitsy-parser") (v "0.72.4") (d (list (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1bqih1z37gb7dcn7rqcixfzy9rg2ig7vgs5anri02gli6ygy33aq")))

(define-public crate-bitsy-parser-0.72.5 (c (n "bitsy-parser") (v "0.72.5") (d (list (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1s7m15sdyazyiah8qmxfv3afndvjc8m6a0lrhz568p90wr9llwma")))

(define-public crate-bitsy-parser-0.75.0 (c (n "bitsy-parser") (v "0.75.0") (d (list (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "1a6kblqzsb68aa7dwhz1lr1qyl1k4dckqg0na745dc1azdpcwy73")))

(define-public crate-bitsy-parser-0.710.0 (c (n "bitsy-parser") (v "0.710.0") (d (list (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "0zqd325lwz7i7riry0gl3wigrq3rd3aikd7mzkikn1mx4l87y9b5")))

(define-public crate-bitsy-parser-0.710.1 (c (n "bitsy-parser") (v "0.710.1") (d (list (d (n "data-encoding") (r "^2.3.1") (d #t) (k 0)) (d (n "loe") (r "^0.2.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)))) (h "14w8760z0sf9qi8n99dda98ww41wxdq7h3kn32d4qiw30s1p7ygk")))

