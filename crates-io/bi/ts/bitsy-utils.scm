(define-module (crates-io bi ts bitsy-utils) #:use-module (crates-io))

(define-public crate-bitsy-utils-0.1.0 (c (n "bitsy-utils") (v "0.1.0") (d (list (d (n "bytes") (r "^1.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)))) (h "1qrgl4q1rlg351zv9pq566zv0yzxd8hjqwajcy91ajvvb8zdn9q0")))

