(define-module (crates-io bi ts bitsign) #:use-module (crates-io))

(define-public crate-bitsign-0.1.0 (c (n "bitsign") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bitcoin") (r "^0.21") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.15") (f (quote ("recovery" "endomorphism"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1awmk6n7mxidzlrj0lka8x5m8iwc9bjjb14faknkn5rgwqdvahzy")))

(define-public crate-bitsign-0.1.1 (c (n "bitsign") (v "0.1.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bitcoin") (r "^0.21") (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.15") (f (quote ("recovery" "endomorphism"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "10dfssgv6r333s8bqhs2qagb2ck7kip91plnn2a7i68n96xzq2nr")))

