(define-module (crates-io bi ts bitstamp_ws_client) #:use-module (crates-io))

(define-public crate-bitstamp_ws_client-0.3.0 (c (n "bitstamp_ws_client") (v "0.3.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20") (f (quote ("rustls-tls-webpki-roots"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "10wmr7ikkz6fxj1jdv66zbcmb8k6mjm6dscir0qq01322568dq2m")))

