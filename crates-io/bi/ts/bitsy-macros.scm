(define-module (crates-io bi ts bitsy-macros) #:use-module (crates-io))

(define-public crate-bitsy-macros-0.1.0 (c (n "bitsy-macros") (v "0.1.0") (d (list (d (n "bitsy-utils") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hcqxicam1qk9jwsck4hqp2xgvd2235w75v7vmq2z7xxaxahvi7l")))

