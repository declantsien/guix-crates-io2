(define-module (crates-io bi ts bitsparrow-derive) #:use-module (crates-io))

(define-public crate-bitsparrow-derive-0.1.0 (c (n "bitsparrow-derive") (v "0.1.0") (d (list (d (n "bitsparrow") (r "^2.0.0-rc1") (d #t) (k 2)) (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0mk8jf82y13rsz2hms6vxbvcr0ssspshd4ijpig6jwjxi8hwi1x3") (f (quote (("unstable"))))))

(define-public crate-bitsparrow-derive-0.1.1 (c (n "bitsparrow-derive") (v "0.1.1") (d (list (d (n "bitsparrow") (r "^2.0.0-rc1") (d #t) (k 2)) (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0y7m8bqqmx9i2c9cyp5lcvpj9rndwvas4hn9pxw6kfx28p6k9gxp")))

(define-public crate-bitsparrow-derive-2.0.0-rc2 (c (n "bitsparrow-derive") (v "2.0.0-rc2") (d (list (d (n "bitsparrow") (r "^2.0.0-rc2") (d #t) (k 2)) (d (n "quote") (r "^0.3.8") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0b298dwnkkyj77h82s00fn38ylxjjxfqwc00nxy842ydfc13snw0")))

