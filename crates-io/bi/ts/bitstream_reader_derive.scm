(define-module (crates-io bi ts bitstream_reader_derive) #:use-module (crates-io))

(define-public crate-bitstream_reader_derive-0.3.0 (c (n "bitstream_reader_derive") (v "0.3.0") (d (list (d (n "bitstream_reader") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "13w48y0gvb069z256pcys2mlydgc76x4k2bs1x3953g8v4vpbyc7")))

(define-public crate-bitstream_reader_derive-0.3.1 (c (n "bitstream_reader_derive") (v "0.3.1") (d (list (d (n "bitstream_reader") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1qz4d96ns6b43hvyraxza9pqkr2y7h162rpdjgm0m1vx76mn2my5")))

(define-public crate-bitstream_reader_derive-0.4.0 (c (n "bitstream_reader_derive") (v "0.4.0") (d (list (d (n "bitstream_reader") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1rz3n5x5j43licwm3qyr2sjps49b1q7ay2lr78b5wg2p5q9s2z2n")))

(define-public crate-bitstream_reader_derive-0.5.0 (c (n "bitstream_reader_derive") (v "0.5.0") (d (list (d (n "bitstream_reader") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1cdx0i5jkj5v34gbg9g9rz0vp65zarinhlz09r813wd3r8vgzqbs")))

(define-public crate-bitstream_reader_derive-0.6.0 (c (n "bitstream_reader_derive") (v "0.6.0") (d (list (d (n "bitstream_reader") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "syn_util") (r "^0.3") (d #t) (k 0)))) (h "071rzahxyw7lzg5wa98mrih6ym2yf2wiszl4fa3rdk5020r5gr0l")))

(define-public crate-bitstream_reader_derive-0.7.0 (c (n "bitstream_reader_derive") (v "0.7.0") (d (list (d (n "bitstream_reader") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "syn_util") (r "^0.3") (d #t) (k 0)))) (h "0r2h39lpbczsysvr6dndm14qg1lr3dfxj4f56dla3hqia0avp56c")))

(define-public crate-bitstream_reader_derive-0.7.1 (c (n "bitstream_reader_derive") (v "0.7.1") (d (list (d (n "bitstream_reader") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn_util") (r "^0.4") (d #t) (k 0)))) (h "105ld4cjsrg4n3a6v9jddwn4hfkw692laas6zp48azk99s9g57qp")))

