(define-module (crates-io bi ts bits_array) #:use-module (crates-io))

(define-public crate-bits_array-0.1.0 (c (n "bits_array") (v "0.1.0") (h "07fqlpjji99x3hnljyp7g8598kc4zm24ps0q1i1da0clv292c5fz") (y #t)))

(define-public crate-bits_array-0.1.1 (c (n "bits_array") (v "0.1.1") (h "0qd2p9hfr33yc1zqyyc9xj4nd7r7hgd2d335a0s65p43naiiqxil")))

