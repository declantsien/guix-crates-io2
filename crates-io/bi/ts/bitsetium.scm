(define-module (crates-io bi ts bitsetium) #:use-module (crates-io))

(define-public crate-bitsetium-0.0.0 (c (n "bitsetium") (v "0.0.0") (h "0cx1mdp2pn4ij64rm2sspfng4m3hqyhdmyj1w28i46s9c8kvvacx") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-bitsetium-0.0.1 (c (n "bitsetium") (v "0.0.1") (h "101ca1lm3v1kj8cnj3h8n54g6m4b16wmfkcpr5b2fxyvhpba1gq2") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-bitsetium-0.0.2 (c (n "bitsetium") (v "0.0.2") (h "0sln1kv9kwms4802p1y9i33fhpzd92lb30wzc2lhwcijwgkc20w6") (f (quote (("default" "alloc") ("alloc"))))))

