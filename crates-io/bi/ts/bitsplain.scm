(define-module (crates-io bi ts bitsplain) #:use-module (crates-io))

(define-public crate-bitsplain-0.1.0-alpha.1 (c (n "bitsplain") (v "0.1.0-alpha.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "bech32") (r "^0.9.1") (d #t) (k 0)) (d (n "bitcoin") (r "^0.29.2") (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "inventory") (r "^0.3.3") (d #t) (k 0)) (d (n "lightning") (r "^0.0.113") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.27.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting"))) (d #t) (k 0)))) (h "029xs902lq3rd1wj3avrm33ndhp9ys01di0pdfkdwybb3ana5kcm")))

