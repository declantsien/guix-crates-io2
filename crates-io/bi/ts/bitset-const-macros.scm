(define-module (crates-io bi ts bitset-const-macros) #:use-module (crates-io))

(define-public crate-bitset-const-macros-0.1.0 (c (n "bitset-const-macros") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.77") (d #t) (k 0)))) (h "0yq2kbrrah7i86v6wsr1riyq7lipbacwax4a59v4fmmqv888xd37")))

