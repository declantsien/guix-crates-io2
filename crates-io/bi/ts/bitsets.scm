(define-module (crates-io bi ts bitsets) #:use-module (crates-io))

(define-public crate-bitsets-0.1.0 (c (n "bitsets") (v "0.1.0") (h "0qyhhxz25iz3lh0aassdni5hjfk0q6zyw5253m4mzw54ybys9s1m")))

(define-public crate-bitsets-0.1.1 (c (n "bitsets") (v "0.1.1") (h "0rvz43npmm9h411hki5frrdlb52kg9ichq0vqsh9p8bpacxf4hzi")))

