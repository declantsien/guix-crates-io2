(define-module (crates-io bi ts bitstring-trees) #:use-module (crates-io))

(define-public crate-bitstring-trees-0.1.0 (c (n "bitstring-trees") (v "0.1.0") (d (list (d (n "bitstring") (r "^0.1") (d #t) (k 0)))) (h "0avvl51x6bsnhimfwy8ckvvyqxw9i5mrxq6c13qvvnskbs94sday")))

(define-public crate-bitstring-trees-0.1.1 (c (n "bitstring-trees") (v "0.1.1") (d (list (d (n "bitstring") (r "^0.1") (d #t) (k 0)))) (h "06al87gizr74xcxi8mib944xzn73h9zf4x90m7sxz9xin4wrk0az")))

