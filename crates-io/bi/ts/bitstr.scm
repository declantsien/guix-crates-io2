(define-module (crates-io bi ts bitstr) #:use-module (crates-io))

(define-public crate-bitstr-0.1.0 (c (n "bitstr") (v "0.1.0") (h "1qbvfrns1znmgq2w1ginsy1vkhm3ijgzzqybi3f2wcq8wzxwzan5")))

(define-public crate-bitstr-0.1.1 (c (n "bitstr") (v "0.1.1") (h "0grpgc05gdlf5z8c3vlyq76pgx3n72hymq2zhq0dkcw5gfrfffpw")))

(define-public crate-bitstr-0.1.2 (c (n "bitstr") (v "0.1.2") (h "07c2qmnh7mjzpbjmlys2k50mjnpf8h0gk5r18ysxj6jf3hhfc4bi")))

