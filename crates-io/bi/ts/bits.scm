(define-module (crates-io bi ts bits) #:use-module (crates-io))

(define-public crate-bits-0.0.0 (c (n "bits") (v "0.0.0") (h "0yxqbl594zy5gaqjkwsxly7kkbxj1y0ch9agvf8bx3wmf324x6yv")))

(define-public crate-bits-0.0.1 (c (n "bits") (v "0.0.1") (h "1xf8zwi8r0193714sf0hd57pjj903y35ifxj4ar1gqs0ja8vi3r2")))

(define-public crate-bits-0.0.2 (c (n "bits") (v "0.0.2") (h "1yxhbqmf74ql05pjmd0vmvrmvjn1rp51q02z4sxnz3cl7x63qkzm")))

(define-public crate-bits-0.0.3 (c (n "bits") (v "0.0.3") (h "1a589dby0c9vk41464x9mp6g7hai5afp1d69cniikj7pv996syvv")))

(define-public crate-bits-0.0.4 (c (n "bits") (v "0.0.4") (h "11w839skv7mbi17v6rsjyfjr03rdmp1180pr92rrnrnhrvaap59j")))

