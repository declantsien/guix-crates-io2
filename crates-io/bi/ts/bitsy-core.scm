(define-module (crates-io bi ts bitsy-core) #:use-module (crates-io))

(define-public crate-bitsy-core-0.1.0 (c (n "bitsy-core") (v "0.1.0") (d (list (d (n "async-std") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bitsy-macros") (r "^0.1") (d #t) (k 0)) (d (n "bitsy-utils") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1ni8nxd6xgwgk2ls13l0jivzry2pj7zl56nrnvrz5bxhs8dm31qi")))

