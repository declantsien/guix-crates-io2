(define-module (crates-io bi ts bitstring) #:use-module (crates-io))

(define-public crate-bitstring-0.1.0 (c (n "bitstring") (v "0.1.0") (h "0bsqvlgj1y6dzvfm7n2v11ajpzw79p6kz3qxq56lyvmf4adil1n2")))

(define-public crate-bitstring-0.1.1 (c (n "bitstring") (v "0.1.1") (h "0b97f3yf33i6xnn512y6jwaqmyk1aab84b8ynhz1hyvdljvzfm1y")))

(define-public crate-bitstring-0.1.2 (c (n "bitstring") (v "0.1.2") (h "1k3fkd8q1lr6smlpk1r20wcwzp1v2xqymndny5zfgkq0nq6rr8r2")))

