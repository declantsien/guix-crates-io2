(define-module (crates-io bi ts bitstream_reader) #:use-module (crates-io))

(define-public crate-bitstream_reader-0.1.0 (c (n "bitstream_reader") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1la0xm9jd7c383yd0frmvqdzdrkxkk0siml8gy5lj0177ykkpphd")))

(define-public crate-bitstream_reader-0.2.0 (c (n "bitstream_reader") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1pg18dyjz291nzd2wjkqf9yir4lv7gk6mcxpz516wir6hdfhwfdj")))

(define-public crate-bitstream_reader-0.3.0 (c (n "bitstream_reader") (v "0.3.0") (d (list (d (n "bitstream_reader_derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ndzb2aa47sa849b9pr1035fp1ihcidd01wypahsqazd3m1r3b0j")))

(define-public crate-bitstream_reader-0.3.1 (c (n "bitstream_reader") (v "0.3.1") (d (list (d (n "bitstream_reader_derive") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19s96vg0csxfgw5j9q45i57dsnpj2cs1i6r6rpi50idibbbvbh0s")))

(define-public crate-bitstream_reader-0.4.0 (c (n "bitstream_reader") (v "0.4.0") (d (list (d (n "bitstream_reader_derive") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19y2qjlwkr8qqaa2i3ya4ns4hhxfci2ncwa1a4zp95f3v3d36d3x")))

(define-public crate-bitstream_reader-0.5.0 (c (n "bitstream_reader") (v "0.5.0") (d (list (d (n "bitstream_reader_derive") (r "^0.5") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0nv2xbl7wjj799gx6s04b5isialagap524lmp2ji9qg05dip161p") (f (quote (("unchecked_utf8"))))))

(define-public crate-bitstream_reader-0.5.1 (c (n "bitstream_reader") (v "0.5.1") (d (list (d (n "bitstream_reader_derive") (r "^0.5") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0xwwhg2jdscj5p1666j3i047ch0ccid56x8p4bh7jkvzwaxxh66f") (f (quote (("unchecked_utf8"))))))

(define-public crate-bitstream_reader-0.6.0 (c (n "bitstream_reader") (v "0.6.0") (d (list (d (n "bitstream_reader_derive") (r "^0.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "133pgfa670pqradf64dlv41bzwvflwmhmx487isqnbrw64riqili")))

(define-public crate-bitstream_reader-0.6.1 (c (n "bitstream_reader") (v "0.6.1") (d (list (d (n "bitstream_reader_derive") (r "^0.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0v6lyanpwjyflq91cy3ax0d6caaxjw5jd78n9mh7g1nxb1sshxs6")))

(define-public crate-bitstream_reader-0.6.2 (c (n "bitstream_reader") (v "0.6.2") (d (list (d (n "bitstream_reader_derive") (r "^0.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0cs61n2yyndxgdfp309h10dbvnawh07vl5fcj0iblrxwcnr2infd") (f (quote (("unsafe"))))))

(define-public crate-bitstream_reader-0.6.3 (c (n "bitstream_reader") (v "0.6.3") (d (list (d (n "bitstream_reader_derive") (r "^0.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1h1lg0yzql6jxyksvp24f8cwq4ijzmxxr8wwikxi1g4kkxmx7pdl") (f (quote (("unsafe"))))))

(define-public crate-bitstream_reader-0.6.4 (c (n "bitstream_reader") (v "0.6.4") (d (list (d (n "bitstream_reader_derive") (r "^0.6") (d #t) (k 0)) (d (n "bytecount") (r "^0.5") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07dwwcvggnyv7hrg3hvfcaicxwh3sl8ba544lr4xff6q1r4byscr") (f (quote (("unsafe"))))))

(define-public crate-bitstream_reader-0.6.5 (c (n "bitstream_reader") (v "0.6.5") (d (list (d (n "bitstream_reader_derive") (r "^0.6") (d #t) (k 0)) (d (n "bytecount") (r "^0.5") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0wkzzxf6gz9x7nbf445px01f71rm07lc2s0sfs2nhfk061z74yib") (f (quote (("unsafe"))))))

(define-public crate-bitstream_reader-0.6.6 (c (n "bitstream_reader") (v "0.6.6") (d (list (d (n "bitstream_reader_derive") (r "^0.6") (d #t) (k 0)) (d (n "bytecount") (r "^0.5") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1f2npv3y2lxsgjndp5q111a9wynrqbfwis5vbxpsqi388pln06mg") (f (quote (("unsafe"))))))

(define-public crate-bitstream_reader-0.6.7 (c (n "bitstream_reader") (v "0.6.7") (d (list (d (n "bitstream_reader_derive") (r "^0.6") (d #t) (k 0)) (d (n "bytecount") (r "^0.5") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1765x8dqqmzszyi2zpndsqj4m6dylr5rvdf70jmm3a06sfhklimw")))

(define-public crate-bitstream_reader-0.6.8 (c (n "bitstream_reader") (v "0.6.8") (d (list (d (n "bitstream_reader_derive") (r "^0.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "15dn5l5fvzqahvr05dzjisbc7g2b9fwr7y8jp0fadp8vv3rgs3ba")))

(define-public crate-bitstream_reader-0.6.9 (c (n "bitstream_reader") (v "0.6.9") (d (list (d (n "bitstream_reader_derive") (r "^0.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1kp93c50qvm8hq0x4j0g8yjb36rzvh5zi236ab4137j6w9l8xxx6")))

(define-public crate-bitstream_reader-0.7.0 (c (n "bitstream_reader") (v "0.7.0") (d (list (d (n "bitstream_reader_derive") (r "^0.7") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "035c416w9z0w15a94qc2c08qzv7k21asvqydifdwr2diin1bq7g6")))

(define-public crate-bitstream_reader-0.7.1 (c (n "bitstream_reader") (v "0.7.1") (d (list (d (n "bitstream_reader_derive") (r "^0.7") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 2)) (d (n "memchr") (r "^2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0xwbw97fg2nw8fl690raxafj8ysvrsj2ca66xcn7ip0wdg2f578a")))

