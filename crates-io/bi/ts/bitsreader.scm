(define-module (crates-io bi ts bitsreader) #:use-module (crates-io))

(define-public crate-bitsreader-0.1.0 (c (n "bitsreader") (v "0.1.0") (h "03qlcyic744lwgvzycxvr0sfh6lp35n9z68qd80p80cxb8fj6lr5")))

(define-public crate-bitsreader-0.1.1 (c (n "bitsreader") (v "0.1.1") (h "0i0h8yd0nar9h5wvxb56nkmlwws1wfnxsiws1rm133xriba4q0m3")))

