(define-module (crates-io bi ts bitslice) #:use-module (crates-io))

(define-public crate-bitslice-0.1.0 (c (n "bitslice") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)))) (h "1wz5964jaz32a4qijj3l32q8ixkcypikrlxbspq9xfh7xpgv1y6n")))

(define-public crate-bitslice-0.1.1 (c (n "bitslice") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)))) (h "0zlfz41sfl61kmy5db7l1630944k6i03d29dw8vhcarndjhcz6qq")))

