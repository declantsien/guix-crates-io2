(define-module (crates-io bi ts bitstream-rs) #:use-module (crates-io))

(define-public crate-bitstream-rs-0.1.1 (c (n "bitstream-rs") (v "0.1.1") (h "19xfnv68nkd5r4fzh951q0m5bb5wlxbnba2hfrd57wdd8qx0p9ps")))

(define-public crate-bitstream-rs-0.1.2 (c (n "bitstream-rs") (v "0.1.2") (h "1yd7rdc9d7mhrxs1snhwhsrad9px94diwbp19dhrzf93m14r712l")))

(define-public crate-bitstream-rs-0.2.0 (c (n "bitstream-rs") (v "0.2.0") (h "1j1wmjwg7vl40d9sr40pp8ig3kc0001z9w1ym64s8aha565pym7b")))

