(define-module (crates-io bi ts bitsparrow) #:use-module (crates-io))

(define-public crate-bitsparrow-0.2.0 (c (n "bitsparrow") (v "0.2.0") (h "10lbhkaffs8sqbhm4p5vn876a1i98n1k9zrz0602nvzsqrwh1l5j")))

(define-public crate-bitsparrow-0.2.1 (c (n "bitsparrow") (v "0.2.1") (h "11lj8gvbgfq3b9b12zjgw43cskzynkn7vllwrv99zl1l6lzkq18d")))

(define-public crate-bitsparrow-0.2.2 (c (n "bitsparrow") (v "0.2.2") (h "0sapz2nhia5xdwm0piiqwvgbdmak8vbagmh06i11aymn0913pgf8")))

(define-public crate-bitsparrow-0.3.0 (c (n "bitsparrow") (v "0.3.0") (h "0a7wlhgnj3g5gndn5yjw34sxsvc53vmfwhah7br4jl673i3zgfnp")))

(define-public crate-bitsparrow-0.3.1 (c (n "bitsparrow") (v "0.3.1") (h "1was1kv5kih2zy7x6nwc5flhk580rbpjjsz826b55wvsvazizbf2")))

(define-public crate-bitsparrow-0.3.2 (c (n "bitsparrow") (v "0.3.2") (h "1qy2b7y1s8gb481klrgrf7iahqafgjyx9fw6pflfwj5rysa84r0b")))

(define-public crate-bitsparrow-0.4.0 (c (n "bitsparrow") (v "0.4.0") (h "0y7gwcav3yqgfnrr0sc5jk08vgyhjgx42g8i2315nj68aizs1qa9")))

(define-public crate-bitsparrow-1.0.0 (c (n "bitsparrow") (v "1.0.0") (h "08ndpsfihj4lpnwrzdgy1minm7v82i6l5wj192gxxk774h8pb5pn")))

(define-public crate-bitsparrow-2.0.0-rc1 (c (n "bitsparrow") (v "2.0.0-rc1") (h "08iy1sk46gckn5jgc8mcj63n9rc08lnzk4457czzzl1nwn0nb7kq")))

(define-public crate-bitsparrow-2.0.0-rc2 (c (n "bitsparrow") (v "2.0.0-rc2") (h "1gm60acpb1g46znc7l0clv9aapl56njcn4i0zyk6dqbpa1dfnczv")))

(define-public crate-bitsparrow-2.0.0-rc3 (c (n "bitsparrow") (v "2.0.0-rc3") (h "1i6fvyxa76rcc0gd0c354cg7iajmzr2lhiq3as77zprxjdilvw24")))

(define-public crate-bitsparrow-2.0.0-rc4 (c (n "bitsparrow") (v "2.0.0-rc4") (h "05swpfrygb8ksjwbl2ff0bz4lzq78rr007xnqwnpa0vz0rsdm5kk")))

