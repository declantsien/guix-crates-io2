(define-module (crates-io bi t- bit-parallelism) #:use-module (crates-io))

(define-public crate-bit-parallelism-0.1.0 (c (n "bit-parallelism") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1ywwilim5f4npy65vifiiw3hck9mcsjlxkgb1csjxkvy65x9ynwc") (r "1.64")))

(define-public crate-bit-parallelism-0.1.1 (c (n "bit-parallelism") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "15aad3i27kj6iv5sz94kz4cl0lzk2m5ifmdaw5b3s46p6pcw9kvj") (r "1.64")))

(define-public crate-bit-parallelism-0.1.2 (c (n "bit-parallelism") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0r8xy6rllj55xk80r0na4qhbpxp9yx63g70dxiqkpvsvwqqvz89k") (r "1.64")))

(define-public crate-bit-parallelism-0.1.3 (c (n "bit-parallelism") (v "0.1.3") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "182arbqnx2z4k7pyn9rv7fzbmvnw1lvbn23d6hjz8dqp5hsvx9qq") (r "1.64")))

