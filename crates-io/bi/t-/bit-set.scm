(define-module (crates-io bi t- bit-set) #:use-module (crates-io))

(define-public crate-bit-set-0.1.0 (c (n "bit-set") (v "0.1.0") (d (list (d (n "bit-vec") (r "*") (d #t) (k 0)))) (h "19kbi6zlsxczclmmr0ybnva4syy29g0napdgrhlf6pjmkvi881hs")))

(define-public crate-bit-set-0.2.0 (c (n "bit-set") (v "0.2.0") (d (list (d (n "bit-vec") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1nl9igldhlc28cb3mw925bsl2gm0x9s6l8apxk6nygcy3kxydqg6") (f (quote (("nightly"))))))

(define-public crate-bit-set-0.3.0 (c (n "bit-set") (v "0.3.0") (d (list (d (n "bit-vec") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "13a29n67gcshal1pif985cfmcdd4cv9p43h1ri2jbwjj0ixpqll4") (f (quote (("nightly"))))))

(define-public crate-bit-set-0.4.0 (c (n "bit-set") (v "0.4.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0320hhcbr73yzjpj2237vw2zq728yg7vmzb8dardg04ff4263gyr") (f (quote (("nightly"))))))

(define-public crate-bit-set-0.5.0 (c (n "bit-set") (v "0.5.0") (d (list (d (n "bit-vec") (r "^0.5.0") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "02kkjpv4hdynkrp11wrls1cklvgia335rk4gqdlml90qdk2gq7kg") (f (quote (("std" "bit-vec/std") ("nightly" "bit-vec/nightly") ("default" "std"))))))

(define-public crate-bit-set-0.5.1 (c (n "bit-set") (v "0.5.1") (d (list (d (n "bit-vec") (r "^0.5.0") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "100ac8867bvbx9kv634w4xjk98b71i8nq4wdcvpf3cf4ha4j6k78") (f (quote (("std" "bit-vec/std") ("nightly" "bit-vec/nightly") ("default" "std"))))))

(define-public crate-bit-set-0.5.2 (c (n "bit-set") (v "0.5.2") (d (list (d (n "bit-vec") (r "^0.6.1") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1pjrmpsnad5ipwjsv8ppi0qrhqvgpyn3wfbvk7jy8dga6mhf24bf") (f (quote (("std" "bit-vec/std") ("default" "std"))))))

(define-public crate-bit-set-0.5.3 (c (n "bit-set") (v "0.5.3") (d (list (d (n "bit-vec") (r "^0.6.1") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1wcm9vxi00ma4rcxkl3pzzjli6ihrpn9cfdi0c5b4cvga2mxs007") (f (quote (("std" "bit-vec/std") ("default" "std"))))))

