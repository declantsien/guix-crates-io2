(define-module (crates-io bi t- bit-long-vec) #:use-module (crates-io))

(define-public crate-bit-long-vec-0.1.0 (c (n "bit-long-vec") (v "0.1.0") (h "14n20izjvbiqfv936hpbq0f72127zs2lvjx3frb4jhsqc4zz9xj8")))

(define-public crate-bit-long-vec-0.2.0 (c (n "bit-long-vec") (v "0.2.0") (h "192f2ihy20glz1rjvb02nwavmzcf612z20l5iaff400n5j5h9c4k")))

(define-public crate-bit-long-vec-0.2.1 (c (n "bit-long-vec") (v "0.2.1") (h "0a89d18kpp907xx66agijxx2bvx4cafvjdlna9xm43r3cwjiwbl9")))

