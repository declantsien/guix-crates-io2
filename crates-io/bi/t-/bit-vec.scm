(define-module (crates-io bi t- bit-vec) #:use-module (crates-io))

(define-public crate-bit-vec-0.1.0 (c (n "bit-vec") (v "0.1.0") (h "0zqqjnld6ji869rpc8vmn52gfj0kxfvndr7nzarsr23lnsxc1k65")))

(define-public crate-bit-vec-0.1.1 (c (n "bit-vec") (v "0.1.1") (h "0fg8m826psqy5c4jf75h3l634vclys44a57bayyg91a6zb96k11q")))

(define-public crate-bit-vec-0.1.3 (c (n "bit-vec") (v "0.1.3") (h "115hb6dmi475xkjbzbvqqnhnp6rh71cmsn9qg4w2pnivarhxdgw1")))

(define-public crate-bit-vec-0.1.4 (c (n "bit-vec") (v "0.1.4") (h "1jbj1lq3d2k5vrjga7kmlqv4lq8y346wwh91fr7wlgi9hspppr96")))

(define-public crate-bit-vec-0.1.5 (c (n "bit-vec") (v "0.1.5") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "074c7rwzmyl137jc6yyxdvz819j790pb7464prkbzyp8zbqm4z62") (f (quote (("nightly"))))))

(define-public crate-bit-vec-0.2.0 (c (n "bit-vec") (v "0.2.0") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "15vn48hnyfnk5f3n0dhdybcg9xsm71f912a6gqp98xwsal1fzad4") (f (quote (("nightly"))))))

(define-public crate-bit-vec-0.3.0 (c (n "bit-vec") (v "0.3.0") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0dn3xbj2cqh1yw9f4adn5xzmy633z1nkalfd064l4d6kfy2qrddc") (f (quote (("nightly"))))))

(define-public crate-bit-vec-0.4.0 (c (n "bit-vec") (v "0.4.0") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0hcgj0j0vdk4ybxi5f7b0jz0lh2fv698dqs3xfv3xgfa9d44r4kr") (f (quote (("nightly"))))))

(define-public crate-bit-vec-0.4.1 (c (n "bit-vec") (v "0.4.1") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "1kxdjmvysn8i2ia3n8bxm7w5l9mdk42i0kgwgq8rirxy5cjdaz05") (f (quote (("nightly"))))))

(define-public crate-bit-vec-0.4.2 (c (n "bit-vec") (v "0.4.2") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "19zkzhx38a0cd1jvzcc076av3lpidr7n2nvysmciwkd0gkzxpxrp") (f (quote (("nightly"))))))

(define-public crate-bit-vec-0.4.3 (c (n "bit-vec") (v "0.4.3") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "1788cljz3kbnln29003x7j2n9cigya5dyngmahbjbd5vx34c55sv") (f (quote (("nightly"))))))

(define-public crate-bit-vec-0.4.4 (c (n "bit-vec") (v "0.4.4") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "0pw902a8ail0k64a7092a8vngfzsq7xkj2r22hz6q1z62s5zzd02") (f (quote (("nightly"))))))

(define-public crate-bit-vec-0.5.0 (c (n "bit-vec") (v "0.5.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1kv7brj661psiccsxqzqc2c9c48iqsv0pv3zw853kdrvcb5xah24") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-bit-vec-0.5.1 (c (n "bit-vec") (v "0.5.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1fyh8221s6cxlmng01v8v2ljhavzawqqs8r1xjc66ap5sjavx6zm") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-bit-vec-0.6.0 (c (n "bit-vec") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fla8gn8ffay69674cqzsp1f5s48m4f8iahk73jr5fpargsk7y2w") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-bit-vec-0.6.1 (c (n "bit-vec") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1xs6yba2jqlmzcm9ahzggrlb933c08ik9ah8zdsybylzhc83llm4") (f (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-vec-0.6.2 (c (n "bit-vec") (v "0.6.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1lwws9l5x4j2m1aw9qvr7pbhsk0v02xmhy6419jqa6la5mgwa3az") (f (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-vec-0.6.3 (c (n "bit-vec") (v "0.6.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ywqjnv60cdh1slhz67psnp422md6jdliji6alq0gmly2xm9p7rl") (f (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

