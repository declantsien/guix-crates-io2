(define-module (crates-io bi t- bit-struct) #:use-module (crates-io))

(define-public crate-bit-struct-0.1.0 (c (n "bit-struct") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1ga9bvx9iz6jdpk6mfxpdbv44wik7di99gxbscmfkyhsk5nlyjjx")))

(define-public crate-bit-struct-0.1.1 (c (n "bit-struct") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0fk4b5n6n5q38wivdadiq3r8klp3nii7l8pp3rcjjg9gi3vky5vf")))

(define-public crate-bit-struct-0.1.2 (c (n "bit-struct") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0a15756q32rlkfzl5w1igq79jh76khnzb0kyzfbbs9yizbabfk4r")))

(define-public crate-bit-struct-0.1.3 (c (n "bit-struct") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0af3hq8vab5m8rbwqk7rkjp791r4j5f0c49shsdg21cdmgn004sf")))

(define-public crate-bit-struct-0.1.4 (c (n "bit-struct") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1yfl161587c9pbih94gpi93qfkyzg9zjp06jrr9i6haiznpyx9lp")))

(define-public crate-bit-struct-0.1.5 (c (n "bit-struct") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0knb6qzjrh22l8drp4ihb5c0vkfi6lmlli519f9xzb7v71zl494x")))

(define-public crate-bit-struct-0.1.6 (c (n "bit-struct") (v "0.1.6") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1j9gljz6y1ai7iz3vanykfjkl1qy3jvnfdijdmkswacwkzs553zi")))

(define-public crate-bit-struct-0.1.7 (c (n "bit-struct") (v "0.1.7") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "102a2a06c293qgkcr3v6lmin4znnnzxh9k99czi1vw7cscfzkg5j")))

(define-public crate-bit-struct-0.1.8 (c (n "bit-struct") (v "0.1.8") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0xd2xsrhpxl4ifkflc876mdafr6lbx7sasqzw96gpc4v658ncd8b")))

(define-public crate-bit-struct-0.1.9 (c (n "bit-struct") (v "0.1.9") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0pnzc35f69jfpkfblzbwd0m0vfwc8kirsaxslbzrydhcxxb92nf9")))

(define-public crate-bit-struct-0.1.10 (c (n "bit-struct") (v "0.1.10") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1yh6frjfln5xdrjg581lwdv9kd4dknq0z78lh79m8lw8x2khaph5")))

(define-public crate-bit-struct-0.1.11 (c (n "bit-struct") (v "0.1.11") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "17p6cb1i2ywasj4mhgwx3wf4y1n4j6gx6kxlib2dpf65r6mkmn45")))

(define-public crate-bit-struct-0.1.12 (c (n "bit-struct") (v "0.1.12") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "06wb28j1fxxld234vddrijkz9bpcd72r679x13fhbf54qbwzc8jj")))

(define-public crate-bit-struct-0.1.13 (c (n "bit-struct") (v "0.1.13") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1fckyrrjzhrr1dyl0y30j6f146h80a5c22pg03rj412jsr09xyr8")))

(define-public crate-bit-struct-0.1.14 (c (n "bit-struct") (v "0.1.14") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0i83rx3fmvyhk8g1jzvg4m7iw6l6xls5yzn6n9zcqgrh10s03fjv")))

(define-public crate-bit-struct-0.1.15 (c (n "bit-struct") (v "0.1.15") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0yscnqffi3r4sxqn7wk5vz5dxsfgjf14ic4zrrk1ziris8zjl4p8")))

(define-public crate-bit-struct-0.1.16 (c (n "bit-struct") (v "0.1.16") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "07vx698wid3k418hycsi3n2k2lsc8df8888ypcwwlfjfv9ys9q4y")))

(define-public crate-bit-struct-0.1.17 (c (n "bit-struct") (v "0.1.17") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1fl6hpbf7mnm1618p8nd8ss79xqkn27anm16n7s3ljv5qm3ajwia")))

(define-public crate-bit-struct-0.1.18 (c (n "bit-struct") (v "0.1.18") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "02z5zldwjwqfmwsf1ziaz74rlhw58d1gh5si0w748bhjim8kq0d8")))

(define-public crate-bit-struct-0.1.19 (c (n "bit-struct") (v "0.1.19") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1v6yq8rs5qzq0hwxhvmal0sxa2mq40bbaqf4j1459qh1swkbwn26")))

(define-public crate-bit-struct-0.1.20 (c (n "bit-struct") (v "0.1.20") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1dm0w70ncfvdxjflkqyv0k8x1jkib2sg0da55w8m0sjs41cfv7ff")))

(define-public crate-bit-struct-0.1.21 (c (n "bit-struct") (v "0.1.21") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "02yc06qcznnm5f968ch5k35r394mcps5m2kr6dxcfxkmr7i1q59i")))

(define-public crate-bit-struct-0.1.22 (c (n "bit-struct") (v "0.1.22") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "00m3r34hlz0bk5fm6csi7hw3f8ih9ac043q9r5chrnh39asnd5zq")))

(define-public crate-bit-struct-0.1.23 (c (n "bit-struct") (v "0.1.23") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "08syymsxzvy1l62bl09wn7qy4scac8g434bi31qj3ivypy24a6r5")))

(define-public crate-bit-struct-0.1.24 (c (n "bit-struct") (v "0.1.24") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1w2r4v5m6xpailk622i1lhanqfxl758pcpjyg6jqbfvj31ixf9zr")))

(define-public crate-bit-struct-0.1.25 (c (n "bit-struct") (v "0.1.25") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1r150srzp6mngqp3pj52fkg73hyx9njh9cd252iaz9wv9znvh8vr")))

(define-public crate-bit-struct-0.1.26 (c (n "bit-struct") (v "0.1.26") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "13dz2jil188mdn4ykdyphi0ivbnlwm5qly40z9pzgsw5x7hmksja")))

(define-public crate-bit-struct-0.1.27 (c (n "bit-struct") (v "0.1.27") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1whznczdkhvvww0xcp45isdwkshhjzz4xa0arykgm8mkymrj4isc")))

(define-public crate-bit-struct-0.1.28 (c (n "bit-struct") (v "0.1.28") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0k71g62ih5w0snafjkdcr6pfazvy2wrbcdqm6bxb3ca5qfpsxjwh")))

(define-public crate-bit-struct-0.1.29 (c (n "bit-struct") (v "0.1.29") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "05lc2jpbbdizbgfp5wk3z0rpxj6im48my0p7rj6xsnkgnfgbf4hj")))

(define-public crate-bit-struct-0.1.30 (c (n "bit-struct") (v "0.1.30") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ag8dhsl8swhx44rr2b4zk0lkzmgidkdy6pyf249hcal8qlrayah")))

(define-public crate-bit-struct-0.1.31 (c (n "bit-struct") (v "0.1.31") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1lnnk303q3qvd83xdm2pwnxl641adpffq2r7nnml9jrxkwb7dhbc")))

(define-public crate-bit-struct-0.1.32 (c (n "bit-struct") (v "0.1.32") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0hcxckri292pnk03q4r54b6ydaj9iajfw14fz8ahcalgg8v6cpp3")))

(define-public crate-bit-struct-0.2.0 (c (n "bit-struct") (v "0.2.0") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1w37zzrd7jhlyaf6ffkc93ssy15m03gbha50c9xm8ls3h379903p") (r "1.62.1")))

(define-public crate-bit-struct-0.3.0 (c (n "bit-struct") (v "0.3.0") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "17xd2972ixk9hhwxrbb0wln6ni30740gygsrllzh48jfji605b48") (r "1.62.1")))

(define-public crate-bit-struct-0.3.1 (c (n "bit-struct") (v "0.3.1") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1yhzsg6pjx4m6qxgikg7fkk2i85h53yrvxbpk5wca89z66g4g926") (r "1.62.1")))

(define-public crate-bit-struct-0.4.0 (c (n "bit-struct") (v "0.4.0") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "03dw81h99bjf21br0dfx4xl2w3wrdp6nl24x1qygp5sk99pklcsh") (y #t) (r "1.62.1")))

(define-public crate-bit-struct-0.3.2 (c (n "bit-struct") (v "0.3.2") (d (list (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0885b4g61baspg1vl61g8ny5qz85fh5zmcf9lbvnfx202jqbn3r7") (f (quote (("default" "serde")))) (r "1.62.1")))

