(define-module (crates-io bi t- bit-byte-structs) #:use-module (crates-io))

(define-public crate-bit-byte-structs-0.0.3 (c (n "bit-byte-structs") (v "0.0.3") (d (list (d (n "cortex-m") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "051qy7qhsp2j0vq826gvvsps3vqrhg2rs4fk48lfq2xac8lfylq4") (f (quote (("cortex-m-debuging" "cortex-m-semihosting" "cortex-m"))))))

