(define-module (crates-io bi t- bit-bounds) #:use-module (crates-io))

(define-public crate-bit-bounds-0.1.0 (c (n "bit-bounds") (v "0.1.0") (d (list (d (n "const-assert") (r "^1.0.0") (d #t) (k 0)))) (h "0pnxg0iccxsp6f7c0nq8pilxnbngg8rzx71f8n82mbi7dbwdl3ya")))

(define-public crate-bit-bounds-1.0.0 (c (n "bit-bounds") (v "1.0.0") (d (list (d (n "const-assert") (r "^1.0.0") (d #t) (k 0)))) (h "089y5badd4524kmdijryqfysd0vg7ymbd6b239vzb5b6na6ir9f8")))

