(define-module (crates-io bi t- bit-iter) #:use-module (crates-io))

(define-public crate-bit-iter-0.1.0 (c (n "bit-iter") (v "0.1.0") (h "1h1idx8s70r9hlm478924ib3w2hihx5g58qblrdfdljhr2a5ghh3") (y #t)))

(define-public crate-bit-iter-0.1.1 (c (n "bit-iter") (v "0.1.1") (h "0q2v2dawmg437iw0dzsnk9jykq4di1w663jdkz6v0c0qs01sg4vk") (y #t)))

(define-public crate-bit-iter-0.1.2 (c (n "bit-iter") (v "0.1.2") (h "0c7bn0gj602jlpai37b632lfpgr6jivhiz2qihmqn7jnfca9y9rs") (y #t)))

(define-public crate-bit-iter-0.1.3 (c (n "bit-iter") (v "0.1.3") (h "1qgb421gg0ikld5ri636vhb90jpf6z4ivcn19zxl0fvhfls36r3q") (y #t)))

(define-public crate-bit-iter-0.1.4 (c (n "bit-iter") (v "0.1.4") (h "184iii48d7rzgsh0brmkr6flhfskrc7c6r6x9vbv0zyhkwhmmhdj") (y #t)))

(define-public crate-bit-iter-0.1.5 (c (n "bit-iter") (v "0.1.5") (h "0lvrvxr9cv81apch33blj94x1pajzrl8c6qqr2knmrgpsdkvsjji")))

(define-public crate-bit-iter-0.1.6 (c (n "bit-iter") (v "0.1.6") (h "05v1nnvgdrf14bqdc7f0rvavn19jdlhqxqfqyh5xkss144n7rgnp")))

(define-public crate-bit-iter-1.0.0 (c (n "bit-iter") (v "1.0.0") (h "18p3zadpkb7rgz82aqpsbmvqfxfm9prmy2inb84p776d56wj17j9")))

(define-public crate-bit-iter-1.1.0 (c (n "bit-iter") (v "1.1.0") (h "0mi3k8c24w4r1829ax9pkv8hrnrdrw4zfysa4vk9rzhhhwlya9j8")))

(define-public crate-bit-iter-1.1.1 (c (n "bit-iter") (v "1.1.1") (h "14gf41fcxdsgw9rnkgshk8zmlnaygy36a676fvbcac8611vpl1i3") (r "1.33.0")))

(define-public crate-bit-iter-1.2.0 (c (n "bit-iter") (v "1.2.0") (h "11zbpqlwg6p39cx966z49gqkrqbxpbfzfkjq7i7whc9rxsbc7hw0") (r "1.53.0")))

