(define-module (crates-io bi t- bit-array) #:use-module (crates-io))

(define-public crate-bit-array-0.4.3 (c (n "bit-array") (v "0.4.3") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 2)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "1jx3mwmqiqzb33hgg6xrx3n1mgy6yr7ml6pqh4pagl6diazghdvx") (f (quote (("nightly"))))))

(define-public crate-bit-array-0.4.4 (c (n "bit-array") (v "0.4.4") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "068asaggf0n00577rvcd3lffah8vg8iw1zw56jqnk8xssj3ls4l7") (f (quote (("nightly"))))))

