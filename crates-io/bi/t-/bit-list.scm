(define-module (crates-io bi t- bit-list) #:use-module (crates-io))

(define-public crate-bit-list-0.1.0 (c (n "bit-list") (v "0.1.0") (d (list (d (n "list-fn") (r "^0.3.0") (d #t) (k 0)) (d (n "uints") (r "^0.2.0") (d #t) (k 0)))) (h "1yjglyg4jhb4vldldhm4vpa9xlw45zb80iinpagvg9vaa6c47awj")))

(define-public crate-bit-list-0.1.1 (c (n "bit-list") (v "0.1.1") (d (list (d (n "list-fn") (r "^0.3.0") (d #t) (k 0)) (d (n "uints") (r "^0.2.0") (d #t) (k 0)))) (h "0k7zbnlbjjcb3q7465z16hkj9babc529sgds6jrh8p3p21c15ahy")))

(define-public crate-bit-list-0.2.0 (c (n "bit-list") (v "0.2.0") (d (list (d (n "list-fn") (r "^0.4.0") (d #t) (k 0)) (d (n "uints") (r "^0.2.0") (d #t) (k 0)))) (h "04y4b1py07096qxvjzlc9k11jkdz4302j1v1a9pncv0wcf7cr24p")))

(define-public crate-bit-list-0.2.1 (c (n "bit-list") (v "0.2.1") (d (list (d (n "list-fn") (r "^0.4.0") (d #t) (k 0)) (d (n "uints") (r "^0.2.0") (d #t) (k 0)))) (h "1f3x8z86lh199sjf55zdn9r9rp0gx6pnk950adqc0768g74rhwq1")))

(define-public crate-bit-list-0.2.2 (c (n "bit-list") (v "0.2.2") (d (list (d (n "list-fn") (r "^0.4.0") (d #t) (k 0)) (d (n "uints") (r "^0.2.0") (d #t) (k 0)))) (h "0jdhl853hl30kjgzlz433nskramqx5z1sd536vypnmcka51s0v34")))

(define-public crate-bit-list-0.3.0 (c (n "bit-list") (v "0.3.0") (d (list (d (n "list-fn") (r "^0.4.0") (d #t) (k 0)) (d (n "uints") (r "^0.3.0") (d #t) (k 0)))) (h "17m5md9vc2qbzz2rcxhzf7gqkfcq51y13a68kag4hjqszx2frhsb")))

(define-public crate-bit-list-0.4.0 (c (n "bit-list") (v "0.4.0") (d (list (d (n "list-fn") (r "^0.4.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "0qm4vvj89533cm40hnjsrmmnl3h1rx3yxq2r4bkflnw8gkw3261b")))

(define-public crate-bit-list-0.5.0 (c (n "bit-list") (v "0.5.0") (d (list (d (n "list-fn") (r "^0.5.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "1jwyixf69y1xvx72i9nyabkkir4nqyp81wr3h8182vwxz8vky80k")))

(define-public crate-bit-list-0.6.0 (c (n "bit-list") (v "0.6.0") (d (list (d (n "list-fn") (r "^0.6.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "1k3qdybbqm1xg1bkq9vd1l8cgnc3m5rpggaxlrn3qi4kx5dzqrr4")))

(define-public crate-bit-list-0.7.0 (c (n "bit-list") (v "0.7.0") (d (list (d (n "list-fn") (r "^0.7.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "0jkyar5hisy9q0dc3cpi8s0fvh5vfc45n6sl863vifc5dlx75pg6")))

(define-public crate-bit-list-0.8.0 (c (n "bit-list") (v "0.8.0") (d (list (d (n "list-fn") (r "^0.8.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "0b310pmfsaq4hgmwhb939jar7bk15v4zz5zjqgbgndrhs86kwv4y")))

(define-public crate-bit-list-0.9.0 (c (n "bit-list") (v "0.9.0") (d (list (d (n "list-fn") (r "^0.9.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "1y8q834lddjknapxjfan6xf2vxgsp73pbnf8jqj3mfb6w559mp6m")))

(define-public crate-bit-list-0.10.0 (c (n "bit-list") (v "0.10.0") (d (list (d (n "list-fn") (r "^0.10.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "0aqgnizb8bkg9rrm99vrh13vgk37d6s796aipy79ag5rlsh5b28g")))

(define-public crate-bit-list-0.10.1 (c (n "bit-list") (v "0.10.1") (d (list (d (n "list-fn") (r "^0.10.1") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "0z5qc5z9v0x5mb71vsm5y5vp28cxgyfgsmqd7mipwmpfcfmrr217")))

(define-public crate-bit-list-0.11.0 (c (n "bit-list") (v "0.11.0") (d (list (d (n "list-fn") (r "^0.10.2") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "0zmp4s8m4ghpjnxzzcljv2rc102swcs4w5jhx1fwj4nrswfr637p")))

(define-public crate-bit-list-0.11.1 (c (n "bit-list") (v "0.11.1") (d (list (d (n "list-fn") (r "^0.10.3") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "1ibl4i6l98srwi1vvpvawl5cn9wa4867c52fn46r2y2lragrks8g")))

(define-public crate-bit-list-0.12.0 (c (n "bit-list") (v "0.12.0") (d (list (d (n "list-fn") (r "^0.11.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.0") (d #t) (k 0)))) (h "0zb06038539p86bida4z2h4h29azyys32gpjl85q4g7cj0r0n1v0")))

(define-public crate-bit-list-0.12.2 (c (n "bit-list") (v "0.12.2") (d (list (d (n "list-fn") (r "^0.11.1") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "16s990hg8dyl5wnqd0lxcp6qkdj48dkiwywrr8jjf8z3zfqcbybv")))

(define-public crate-bit-list-0.12.3 (c (n "bit-list") (v "0.12.3") (d (list (d (n "list-fn") (r "^0.11.2") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "055qxscrng4al4j6m79j6jz4q0vs8k27bls9hrg1p464nhl5q93s")))

(define-public crate-bit-list-0.12.4 (c (n "bit-list") (v "0.12.4") (d (list (d (n "list-fn") (r "^0.11.3") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "131i4kxi1vqf1wl6yn0c1j3av0ij676nnh697x3sp7irjc1xdgs5")))

(define-public crate-bit-list-0.12.5 (c (n "bit-list") (v "0.12.5") (d (list (d (n "list-fn") (r "^0.11.4") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "1mi3iv5h0x617fppfx3m9l8nzmjdv2zwwi887ram2yzvhdbn0fw3")))

(define-public crate-bit-list-0.12.6 (c (n "bit-list") (v "0.12.6") (d (list (d (n "list-fn") (r "^0.11.5") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "0cq3j9qdx395zfh0ymkxwb69vfvgad3dcvff219yry8kbbi2zwi2")))

(define-public crate-bit-list-0.13.0 (c (n "bit-list") (v "0.13.0") (d (list (d (n "list-fn") (r "^0.12.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "1k3iv5n52is9f673vgpw3lrgc4kj2d4hx63cmvgkdj62b2195haj")))

(define-public crate-bit-list-0.14.0 (c (n "bit-list") (v "0.14.0") (d (list (d (n "list-fn") (r "^0.13.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "1px0rla222i465ylxs0w25y6izp8miia1c959mmxbbpxhnz8x50q")))

(define-public crate-bit-list-0.15.0 (c (n "bit-list") (v "0.15.0") (d (list (d (n "list-fn") (r "^0.14.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "16jf5fli6cppzh30k9cgcgqj3him30d87j9alpn2hnpqlpm09in1")))

(define-public crate-bit-list-0.15.1 (c (n "bit-list") (v "0.15.1") (d (list (d (n "list-fn") (r "^0.14.1") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "11nkrgzyrws2zsr63f7qgpvgys9jzjrs75kpyxncfakr9p61mhlr")))

(define-public crate-bit-list-0.16.0 (c (n "bit-list") (v "0.16.0") (d (list (d (n "list-fn") (r "^0.15.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "0fz6cxnfcvjknxdbrwk7vqpq4ckvxjdbjjwnbfvnrjxdspb14prx")))

(define-public crate-bit-list-0.17.0 (c (n "bit-list") (v "0.17.0") (d (list (d (n "list-fn") (r "^0.16.0") (d #t) (k 0)) (d (n "uints") (r "^0.5.1") (d #t) (k 0)))) (h "1gmpm3n5swj7fy7qldk907zcp2g9wacz05npfyg98a5mgw2w3qll")))

(define-public crate-bit-list-0.18.0 (c (n "bit-list") (v "0.18.0") (d (list (d (n "list-fn") (r "^0.16.0") (d #t) (k 0)) (d (n "uints") (r "^0.6.0") (d #t) (k 0)))) (h "11cg5v96jydysc8lxnbnjw9d76497zqhlfw59wysg13f9yhvkkas")))

(define-public crate-bit-list-0.19.0 (c (n "bit-list") (v "0.19.0") (d (list (d (n "list-fn") (r "^0.17.0") (d #t) (k 0)) (d (n "uints") (r "^0.6.0") (d #t) (k 0)))) (h "0501594mds19mvwpl1clhlpzir71p2ccgbrahafbny0az4igp634")))

(define-public crate-bit-list-0.19.1 (c (n "bit-list") (v "0.19.1") (d (list (d (n "list-fn") (r "^0.17.0") (d #t) (k 0)) (d (n "uints") (r "^0.6.1") (d #t) (k 0)))) (h "15hnn1s5n37y7wksa2ybrvlk9pbh7683vpay6wm32ggqyjf9ji0b")))

(define-public crate-bit-list-0.20.0 (c (n "bit-list") (v "0.20.0") (d (list (d (n "list-fn") (r "^0.17.0") (d #t) (k 0)) (d (n "uints") (r "^0.6.1") (d #t) (k 0)))) (h "0k9151xw5y88gggw6yfv2yvgczf9lm4sv32j63sxziqy7jwjg5n6")))

(define-public crate-bit-list-0.20.1 (c (n "bit-list") (v "0.20.1") (d (list (d (n "list-fn") (r "^0.17.0") (d #t) (k 0)) (d (n "uints") (r "^0.6.1") (d #t) (k 0)))) (h "05djlbri254r650ja6glsag37llp54rrcisl8r9qlad5mad4sqar")))

(define-public crate-bit-list-0.21.0 (c (n "bit-list") (v "0.21.0") (d (list (d (n "lim-bit-vec") (r "^0.1.0") (d #t) (k 0)) (d (n "list-fn") (r "^0.17.0") (d #t) (k 0)) (d (n "uints") (r "^0.6.1") (d #t) (k 0)))) (h "0iwaaxgfvqwn1f235b8z69mnxvjc6h3d6n4v5jpx5nm536w6akwa")))

(define-public crate-bit-list-0.21.1 (c (n "bit-list") (v "0.21.1") (d (list (d (n "lim-bit-vec") (r "^0.1.1") (d #t) (k 0)) (d (n "list-fn") (r "^0.17.0") (d #t) (k 0)) (d (n "uints") (r "^0.6.1") (d #t) (k 0)))) (h "16xyca3aa9r8hs5478gahknphx5z75ybrbdn055700gx60993nz0")))

(define-public crate-bit-list-0.21.2 (c (n "bit-list") (v "0.21.2") (d (list (d (n "lim-bit-vec") (r "^0.1.2") (d #t) (k 0)) (d (n "list-fn") (r "^0.17.1") (d #t) (k 0)) (d (n "uints") (r "^0.6.1") (d #t) (k 0)))) (h "042dgrq3pxfb72flx3a05xppnjqgb1q4yx2g78ypcg8y2l4ssp3y")))

(define-public crate-bit-list-0.22.0 (c (n "bit-list") (v "0.22.0") (d (list (d (n "lim-bit-vec") (r "^0.2.0") (d #t) (k 0)) (d (n "list-fn") (r "^0.18.0") (d #t) (k 0)) (d (n "uints") (r "^0.6.1") (d #t) (k 0)))) (h "105chfgxgpprv70kfwn7xp87iz6597d8c5mb03i1mly253s74fws")))

(define-public crate-bit-list-0.23.0 (c (n "bit-list") (v "0.23.0") (d (list (d (n "lim-bit-vec") (r "^0.3.0") (d #t) (k 0)) (d (n "list-fn") (r "^0.18.0") (d #t) (k 0)) (d (n "uints") (r "^0.7.0") (d #t) (k 0)))) (h "0gdvpa3kb5m2dqgqshlhrpkhq3c5n7ndakqwbq5sfgf0mcfqvak4")))

(define-public crate-bit-list-0.23.1 (c (n "bit-list") (v "0.23.1") (d (list (d (n "lim-bit-vec") (r "^0.3.1") (d #t) (k 0)) (d (n "list-fn") (r "^0.18.1") (d #t) (k 0)) (d (n "uints") (r "^0.7.0") (d #t) (k 0)))) (h "1cwrc9dljxx63lxjc08ldqpj3rvwyzhcbwny38lmqaz3945dvlzk")))

(define-public crate-bit-list-0.24.0 (c (n "bit-list") (v "0.24.0") (d (list (d (n "lim-bit-vec") (r "^0.4.0") (d #t) (k 0)) (d (n "list-fn") (r "^0.19.0") (d #t) (k 0)) (d (n "uints") (r "^0.7.0") (d #t) (k 0)))) (h "14ywmc8jfvrml4sirh3sb6as0ahkcf7rw0cympkqkg3p9wyb69h2")))

(define-public crate-bit-list-0.24.1 (c (n "bit-list") (v "0.24.1") (d (list (d (n "lim-bit-vec") (r "^0.4.1") (d #t) (k 0)) (d (n "list-fn") (r "^0.19.1") (d #t) (k 0)) (d (n "uints") (r "^0.7.0") (d #t) (k 0)))) (h "03y4g33gxg8m0bz9g8nmblbk10dyryk19la68cdiw0cmdzb84wx0")))

(define-public crate-bit-list-0.25.0 (c (n "bit-list") (v "0.25.0") (d (list (d (n "lim-bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "list-fn") (r "^0.19.1") (d #t) (k 0)) (d (n "uints") (r "^0.8.0") (d #t) (k 0)))) (h "1hn3jd68f1pnazv93yqavj7hv80p2fmz3dvk1s7375brj0w17l36")))

(define-public crate-bit-list-0.25.1 (c (n "bit-list") (v "0.25.1") (d (list (d (n "lim-bit-vec") (r "^0.5.1") (d #t) (k 0)) (d (n "list-fn") (r "^0.19.1") (d #t) (k 0)) (d (n "uints") (r "^0.8.1") (d #t) (k 0)))) (h "13k2b7n9p292m0rbgzdhqqgn5jsr3hmxn0cz9hssrs72bkg6q98g")))

(define-public crate-bit-list-0.26.0 (c (n "bit-list") (v "0.26.0") (d (list (d (n "lim-bit-vec") (r "^0.6.0") (d #t) (k 0)) (d (n "list-fn") (r "^0.19.1") (d #t) (k 0)) (d (n "uints") (r "^0.9.0") (d #t) (k 0)))) (h "0v420czap6ya5bg1skx5q9hnks44p8c6nnmp008sf3p4vm4xrvbr")))

(define-public crate-bit-list-0.27.0 (c (n "bit-list") (v "0.27.0") (d (list (d (n "lim-bit-vec") (r "^0.7.0") (d #t) (k 0)) (d (n "list-fn") (r "^0.20.0") (d #t) (k 0)) (d (n "uints") (r "^0.10.0") (d #t) (k 0)))) (h "0sjfg9sa425704i1bd5xknwpw3lbh05yqfcf2rsln6ili5wz7g7s")))

(define-public crate-bit-list-0.28.0 (c (n "bit-list") (v "0.28.0") (d (list (d (n "lim-bit-vec") (r "^0.8.0") (d #t) (k 0)) (d (n "list-fn") (r "^0.20.0") (d #t) (k 0)) (d (n "uints") (r "^0.11.0") (d #t) (k 0)))) (h "1z3yr7494iwir5ri1qr35c9kfrw1njr3rviwgw6jqbibp06x07vs")))

(define-public crate-bit-list-0.28.1 (c (n "bit-list") (v "0.28.1") (d (list (d (n "lim-bit-vec") (r "^0.8.1") (d #t) (k 0)) (d (n "list-fn") (r "^0.20.0") (d #t) (k 0)) (d (n "uints") (r "^0.11.1") (d #t) (k 0)))) (h "0p1m16by8604jiqdhy976skc20ak5qwmmxb0bs4ng8dkld494h6p")))

(define-public crate-bit-list-0.28.2 (c (n "bit-list") (v "0.28.2") (d (list (d (n "lim-bit-vec") (r "^0.8.2") (d #t) (k 0)) (d (n "list-fn") (r "^0.20.1") (d #t) (k 0)) (d (n "uints") (r "^0.11.2") (d #t) (k 0)))) (h "0qrki2wd5ggq5w0i6wc17lyvdv87i13hffn385mk6a1zrib3mirf")))

