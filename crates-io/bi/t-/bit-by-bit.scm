(define-module (crates-io bi t- bit-by-bit) #:use-module (crates-io))

(define-public crate-bit-by-bit-0.1.0 (c (n "bit-by-bit") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0irsj8aw3xkggcz9m3mks0prs89l75xzbypvrv60zqhmzn72ss59")))

