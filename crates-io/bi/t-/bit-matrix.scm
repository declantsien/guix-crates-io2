(define-module (crates-io bi t- bit-matrix) #:use-module (crates-io))

(define-public crate-bit-matrix-0.0.1 (c (n "bit-matrix") (v "0.0.1") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)))) (h "0caw86j1l28ygrgl0jy053xcgvq7ard51gycpr9912s9vgr1n2qf")))

(define-public crate-bit-matrix-0.1.0 (c (n "bit-matrix") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)))) (h "1fs8r28brqi4wlsqrzr9hv06mv6y4xlpn99fddzxq13s34x3g8qq")))

(define-public crate-bit-matrix-0.2.0 (c (n "bit-matrix") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)))) (h "04ghm7qjkl171bdbh6cmxgdg2g6f3frl5qy6chd7z91asw634kyn")))

(define-public crate-bit-matrix-0.3.0 (c (n "bit-matrix") (v "0.3.0") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "073ikl01mavgay19m0w8lz3jpnnl6dkcbr1ahdyarj618qvkdplf") (f (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.4.0 (c (n "bit-matrix") (v "0.4.0") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1q4474m1g8p4iycddxdff6p8zv4v0agi7y9mwkycgd57b24yigr7") (f (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.5.0 (c (n "bit-matrix") (v "0.5.0") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bzpr53y7hkly27cn6gjvrnhl274mixg114c883lk7hmsf0dx82j") (f (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.6.0 (c (n "bit-matrix") (v "0.6.0") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11p28qj3qydjvi1qkqrccl45x9a2aciddb88q0wvmikg5d9hkmfs") (f (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.6.1 (c (n "bit-matrix") (v "0.6.1") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19l0m8x9xp1qnfidfm1nbsq5fqap728laffhxwn5clsid8iqbxjr") (f (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.7.0 (c (n "bit-matrix") (v "0.7.0") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "memusage") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gscn70djavgalyn2agprqgfjrxv4dkhdaji6yz4h1sqhg63f2rr") (f (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.7.1 (c (n "bit-matrix") (v "0.7.1") (d (list (d (n "bit-vec") (r "^0.6") (k 0)) (d (n "memusage") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "004w3cpzbdrys86m4bibrzyiijrlszjd95rph75hqpfn65blhh4w") (f (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

