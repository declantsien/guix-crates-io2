(define-module (crates-io bi t- bit-index) #:use-module (crates-io))

(define-public crate-bit-index-0.1.0 (c (n "bit-index") (v "0.1.0") (h "1mcbn2mcbl1jjwwrja4bgaa9gaj2bgzhhrg3kl8r34yvaij0l100") (y #t)))

(define-public crate-bit-index-0.2.0 (c (n "bit-index") (v "0.2.0") (h "12664n52z4y25znacipw0z7byxhwzcqw4m5zknxchl4ccg0x9m3l")))

