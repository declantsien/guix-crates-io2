(define-module (crates-io bi nt bintest) #:use-module (crates-io))

(define-public crate-bintest-0.0.0 (c (n "bintest") (v "0.0.0") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "18rlskc4ycfq5095swpmdmadz4dng83bgm183q7ycg5zmylagdq5")))

(define-public crate-bintest-0.0.1 (c (n "bintest") (v "0.0.1") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "1nwbvyzn9zzd5i1lwk3c5rdbaxniiaxjlr87nh0c9azixz8p3lv7")))

(define-public crate-bintest-0.0.2 (c (n "bintest") (v "0.0.2") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "0wq6mgv88jv79jcyli4l2yn56vkbza61m0vzr8iwaj3cl4k0v6nz")))

(define-public crate-bintest-0.1.0 (c (n "bintest") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "1yqqbmw25njj7xr0lr4vv6n1ig2hkq4aij380z5r5z9g8d0gxiwk")))

(define-public crate-bintest-0.1.1 (c (n "bintest") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "0jzh9cqka56frsp3qigqlicwdbkz9nfim4vhxnz8h02pm0v72raa")))

(define-public crate-bintest-1.0.0 (c (n "bintest") (v "1.0.0") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "0058mp33wkf8ws0gw0wnr6gmzwniba7q0dkmwnmv9fs8kwgr1xx5")))

(define-public crate-bintest-1.0.1 (c (n "bintest") (v "1.0.1") (d (list (d (n "cargo_metadata") (r "^0.13") (d #t) (k 0)))) (h "1nm24h7lvsc0sy8yjk59sqjdwwcl7b452krwqw557wvg6z972cb4")))

(define-public crate-bintest-1.0.2 (c (n "bintest") (v "1.0.2") (d (list (d (n "cargo_metadata") (r ">=0.13, <=0.15") (d #t) (k 0)))) (h "0zbnlg8sn917g2n6l3kf7iqxlp5zsgm9aj5y8gcny7sj59insj69")))

(define-public crate-bintest-1.0.3 (c (n "bintest") (v "1.0.3") (d (list (d (n "cargo_metadata") (r ">=0.13, <=0.16") (d #t) (k 0)))) (h "1dbw1ap8xkkfbgqhdb26xg7blwx91d1x9nwfvd35lnryh5b7z7jn")))

(define-public crate-bintest-2.0.0 (c (n "bintest") (v "2.0.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)))) (h "1av5vvqhcx8617brfpgqg40ziww3klalchgflsnqd4j2s51mrk7j") (r "1.70.0")))

