(define-module (crates-io bi nt bint) #:use-module (crates-io))

(define-public crate-bint-0.1.0 (c (n "bint") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)))) (h "1sx1l5vvjrrwbqr470lf2q5qi8bfzdrv10ciqviw1jsf6n0fn13x") (f (quote (("default"))))))

(define-public crate-bint-0.1.2 (c (n "bint") (v "0.1.2") (h "1rh8n4qswdsrprl2vasnhq33fkbq5krlzd7ndbm84v2n8ww3dfdq")))

(define-public crate-bint-0.1.3 (c (n "bint") (v "0.1.3") (h "13dp4dryqp0xz7zgqvyhhnriifvpanf7fcnjaaprz85ahf2h41k7")))

(define-public crate-bint-0.1.4 (c (n "bint") (v "0.1.4") (h "0bipk89qhvcg0pdd9pmspx7lpc2rbdl6vpzgnahh78qx4jh8akph") (r "1.34")))

(define-public crate-bint-0.1.5 (c (n "bint") (v "0.1.5") (h "1aby4030rjp5824d2fjfcn299w9888ym3i7pncin7zk5bc43rr1s") (r "1.34")))

