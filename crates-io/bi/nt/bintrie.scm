(define-module (crates-io bi nt bintrie) #:use-module (crates-io))

(define-public crate-bintrie-0.1.0 (c (n "bintrie") (v "0.1.0") (h "0aj0q60wq00h297lqzs81ippaizpgfrfmmy8nmwv69nlixr9i0cj")))

(define-public crate-bintrie-0.2.0 (c (n "bintrie") (v "0.2.0") (h "01j9yx0835aq4ps3yix6yccdyvp0mf9ylx32sfvcr6vv11bwjpyk")))

