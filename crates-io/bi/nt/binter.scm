(define-module (crates-io bi nt binter) #:use-module (crates-io))

(define-public crate-binter-1.0.0 (c (n "binter") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "1b7x7h1jvz04yxhq5fnz9qv05qw7fw4d8iszah0b8cnvd3czyxgq")))

