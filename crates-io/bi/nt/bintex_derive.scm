(define-module (crates-io bi nt bintex_derive) #:use-module (crates-io))

(define-public crate-bintex_derive-0.1.0 (c (n "bintex_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.0") (d #t) (k 0)) (d (n "deku") (r "^0.16.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04fdgwldil9lvdfj5d98cykav9qxfnz4ls2126ikyggjshswbnv4")))

