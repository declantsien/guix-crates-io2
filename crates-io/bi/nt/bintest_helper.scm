(define-module (crates-io bi nt bintest_helper) #:use-module (crates-io))

(define-public crate-bintest_helper-0.1.0 (c (n "bintest_helper") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p17y31179gc7kk15hmwpxcx1gkjl1r2njqb78ynz4xhqdkripj0")))

