(define-module (crates-io bi nt bintex) #:use-module (crates-io))

(define-public crate-bintex-0.1.0 (c (n "bintex") (v "0.1.0") (d (list (d (n "bintex_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.0") (d #t) (k 0)) (d (n "deku") (r "^0.16.0") (d #t) (k 0)))) (h "09jrkqj6x8gya5qasxmf3a4b087bq8b5d33df3i1lbiq470q69nc")))

