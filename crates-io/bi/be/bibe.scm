(define-module (crates-io bi be bibe) #:use-module (crates-io))

(define-public crate-bibe-0.1.0 (c (n "bibe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "hyraigne") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1vrbzq3s4ldk2xvlnsbglp1s4626l22cs73hcph35qq2vxxiz326")))

(define-public crate-bibe-0.1.1 (c (n "bibe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "hyraigne") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "19m4v5y9f92qasbg2i635bpas3b82a0j3bdx1a490dxpm8vh0vlw")))

(define-public crate-bibe-0.1.2 (c (n "bibe") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "hyraigne") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1s8857vc322ydkkh6m9b6waxkxbwrpb009y5674g7pq5mj63yi2i")))

(define-public crate-bibe-0.1.3 (c (n "bibe") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "hyraigne") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0jygqx6bjhgw6xviyagqsnkd0hkhz5vih64qfxla2bz0i9yv9g6g")))

(define-public crate-bibe-0.1.4 (c (n "bibe") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "hyraigne") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1n69278mzwv8k2j54d7z1nlsks8l81lni2a5q56n5mkladxan41f")))

