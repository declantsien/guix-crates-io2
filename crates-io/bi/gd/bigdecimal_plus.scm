(define-module (crates-io bi gd bigdecimal_plus) #:use-module (crates-io))

(define-public crate-bigdecimal_plus-1.0.0 (c (n "bigdecimal_plus") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "126l3klcdmnmhadklcisplmnl9cj4wrlashi65m8m83slq58j8wm")))

