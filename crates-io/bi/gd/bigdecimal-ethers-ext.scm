(define-module (crates-io bi gd bigdecimal-ethers-ext) #:use-module (crates-io))

(define-public crate-bigdecimal-ethers-ext-0.1.0 (c (n "bigdecimal-ethers-ext") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.4.1") (d #t) (k 0)) (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1x8qjgrr1g3lp0snlx3prakxdg7q5dd1wdspld0jwxf7cafd2gz6")))

(define-public crate-bigdecimal-ethers-ext-0.2.0 (c (n "bigdecimal-ethers-ext") (v "0.2.0") (d (list (d (n "bigdecimal") (r "^0.4.1") (d #t) (k 0)) (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0hxp2ivyzcdnhnbgj6z5fwgg4cdxsy1j5drw720xwnjp698a8ggk")))

(define-public crate-bigdecimal-ethers-ext-0.2.1 (c (n "bigdecimal-ethers-ext") (v "0.2.1") (d (list (d (n "bigdecimal") (r "^0.4.1") (d #t) (k 0)) (d (n "ethers") (r "^2.0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "1nzfxzbgaaj59d47w2587ivmi1v1w6jdlqm6x9plxzxv5qisj8kc")))

