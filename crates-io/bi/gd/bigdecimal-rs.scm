(define-module (crates-io bi gd bigdecimal-rs) #:use-module (crates-io))

(define-public crate-bigdecimal-rs-0.2.1 (c (n "bigdecimal-rs") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.43") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "035i2r07zrv9r71z11ipn2lw9rdns39ig8mqnl5afgv3in85ldw5") (f (quote (("string-only"))))))

