(define-module (crates-io bi to bitou-sdk-agent) #:use-module (crates-io))

(define-public crate-bitou-sdk-agent-0.0.1 (c (n "bitou-sdk-agent") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1kvqnpzk3mrrqsc1k39c31h5j3lnl15bnq9y65kdz2rkmpwgz5rs") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

