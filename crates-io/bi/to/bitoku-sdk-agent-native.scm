(define-module (crates-io bi to bitoku-sdk-agent-native) #:use-module (crates-io))

(define-public crate-bitoku-sdk-agent-native-0.1.0 (c (n "bitoku-sdk-agent-native") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ri22xw31xdimxs2zqjyazbqmvkbkz0hx90q6yb2sql5h8d2sdsx")))

(define-public crate-bitoku-sdk-agent-native-0.1.1 (c (n "bitoku-sdk-agent-native") (v "0.1.1") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "127fs6glna48mdjy40yr23hq0xhjgn72fx6gshiyjqjgxdbsi9wy") (f (quote (("no-entrypoint"))))))

(define-public crate-bitoku-sdk-agent-native-0.1.2 (c (n "bitoku-sdk-agent-native") (v "0.1.2") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0cig1pr3swy34xcmk7cpj2g9cqr1k45pfdnqw7g7ibdc003yd1la") (f (quote (("no-entrypoint"))))))

(define-public crate-bitoku-sdk-agent-native-0.1.9 (c (n "bitoku-sdk-agent-native") (v "0.1.9") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "solana-program") (r "^1.13.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1flc2qa63hbcki0df6hnw6djlj1lbndj59nn4q9ddiadxyx594na") (f (quote (("no-entrypoint"))))))

(define-public crate-bitoku-sdk-agent-native-0.1.8 (c (n "bitoku-sdk-agent-native") (v "0.1.8") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1wcpm894gsqdw1ywxp1xjjp9yp842ghmv9rzwjpnylwmqh90x2vg") (f (quote (("no-entrypoint"))))))

