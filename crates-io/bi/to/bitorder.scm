(define-module (crates-io bi to bitorder) #:use-module (crates-io))

(define-public crate-bitorder-0.0.1 (c (n "bitorder") (v "0.0.1") (h "038bnb9kvd62l1xg4dx7rrw859wgkl4bhx8s00xvlm0pd2d7x0gd")))

(define-public crate-bitorder-0.0.2 (c (n "bitorder") (v "0.0.2") (d (list (d (n "deku") (r "^0.12.5") (d #t) (k 2)))) (h "0n3whmzkh5ng6idl914xj8c2g7nl3cxd08wg8sl7frc1hcwxrmmz")))

