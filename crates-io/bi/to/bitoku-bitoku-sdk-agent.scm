(define-module (crates-io bi to bitoku-bitoku-sdk-agent) #:use-module (crates-io))

(define-public crate-bitoku-bitoku-sdk-agent-0.2.2 (c (n "bitoku-bitoku-sdk-agent") (v "0.2.2") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "05yhk63sg81q99ynq5va9v3kqmn5zl8k2k10mrd9k5hcnjc94nrk") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

