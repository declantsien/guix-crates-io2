(define-module (crates-io bi to bitonic) #:use-module (crates-io))

(define-public crate-bitonic-0.1.0 (c (n "bitonic") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "087y5v58ipw8l76cxlah4kcdbp2gj4phrdy9qa6q95n1iahhq90v")))

(define-public crate-bitonic-0.2.0 (c (n "bitonic") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "1dib1kqgkdysa83vc8iglzyc6i1rychn0wd554s1rpp128vf4w15")))

