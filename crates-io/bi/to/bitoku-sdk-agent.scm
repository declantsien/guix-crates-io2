(define-module (crates-io bi to bitoku-sdk-agent) #:use-module (crates-io))

(define-public crate-bitoku-sdk-agent-0.1.0 (c (n "bitoku-sdk-agent") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1n3k8w5irryvxf78hb70jvbpvgi04qy7ayx1wsll4pza4chgl842") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bitoku-sdk-agent-0.1.1 (c (n "bitoku-sdk-agent") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1zbkyyb5c5x4icwci9yb65grkvhwwfvfd8f8zh7gf3r5mjxaadq8") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-bitoku-sdk-agent-0.1.2 (c (n "bitoku-sdk-agent") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1xjakwrsfc0qn0fqk4zihwmadd93qwxwrbyhygzq7hqzpxi8v68h") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-bitoku-sdk-agent-0.1.3 (c (n "bitoku-sdk-agent") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0n3vdw3g1qk3vafdpr4rkdg0az60246fd5igr0q0ya3gwddq89j9") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-bitoku-sdk-agent-0.1.4 (c (n "bitoku-sdk-agent") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0580icrwsmmj7h8wvscwn7zghwkmh16qipc3n0fjp4m1271qpzlh") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-bitoku-sdk-agent-0.1.5 (c (n "bitoku-sdk-agent") (v "0.1.5") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1mjc6qxdn74k6mpdxxi8mm9l1r8mzpzzsl93l6cfb584f5wdff9x") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

(define-public crate-bitoku-sdk-agent-0.1.6 (c (n "bitoku-sdk-agent") (v "0.1.6") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1mim7fs852f88gb26a8iym00llarvpk8lapqzva1l847p4wpgdhl") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

