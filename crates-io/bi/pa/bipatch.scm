(define-module (crates-io bi pa bipatch) #:use-module (crates-io))

(define-public crate-bipatch-0.1.0 (c (n "bipatch") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.5") (d #t) (k 0)))) (h "1r8jxs4kvkiks70kslsp6m4lg3rzvb7s9s21y2qhkjin42nb5xh5")))

(define-public crate-bipatch-0.1.1 (c (n "bipatch") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.7") (k 0)))) (h "1c14g89513c5q03fwd1f3js13w5ixr7zyw6qf11gxzhwvp1rq6yb")))

(define-public crate-bipatch-1.0.0 (c (n "bipatch") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "integer-encoding") (r "^2.0.0") (k 0)))) (h "0mr26nwvir8fjvmqw8q17x2awm123bgxsxznlln8rwils70n779g")))

