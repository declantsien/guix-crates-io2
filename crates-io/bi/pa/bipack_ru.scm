(define-module (crates-io bi pa bipack_ru) #:use-module (crates-io))

(define-public crate-bipack_ru-0.1.0 (c (n "bipack_ru") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1hiv88sfkv6gb12c31kijgavp4xkgprzaifvg3li5gbsivya42r7")))

(define-public crate-bipack_ru-0.2.0 (c (n "bipack_ru") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0209vspiyfvki7b69ck1bsd3b10jsf8k66481w4xz322d1lhq3dz")))

(define-public crate-bipack_ru-0.2.1 (c (n "bipack_ru") (v "0.2.1") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1nzsd9drg62zp1daf68s8dw8w1pzvlqzcsjkwm6pb4iz573pxwng")))

(define-public crate-bipack_ru-0.2.2 (c (n "bipack_ru") (v "0.2.2") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1gl706k4wjkv4rzgrxh3f1w8g754yr6ybv25bw3k6x60hsq9j8yb")))

(define-public crate-bipack_ru-0.3.1 (c (n "bipack_ru") (v "0.3.1") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y36gjf6znsay1r8cxgj9y900y15k2cd1d1aw9frkxrf7khvfrl1")))

(define-public crate-bipack_ru-0.3.3 (c (n "bipack_ru") (v "0.3.3") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g1594jlvz0pbhd66w7ll5fmclvfkhnx727zjl60mcmvj2xzdkiq")))

(define-public crate-bipack_ru-0.4.1 (c (n "bipack_ru") (v "0.4.1") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lnbwf0bm8jb1jwvxxxcgm55mf5q0hk1b0n5amjm078yj9a6zvr7")))

(define-public crate-bipack_ru-0.4.2 (c (n "bipack_ru") (v "0.4.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05h8lj9xq6j9pl7nklggga5208fslmk01ql3kdsjwy0i37yl1b4s")))

(define-public crate-bipack_ru-0.4.3 (c (n "bipack_ru") (v "0.4.3") (d (list (d (n "base64") (r "^0.21") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09hv00lzhj2n1gx1jg5id5zpiiyjrf4i2zxckzmsqhd2fwmwasai")))

(define-public crate-bipack_ru-0.4.4 (c (n "bipack_ru") (v "0.4.4") (d (list (d (n "base64") (r "^0.21") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09y05r2bc8y09xmnm0vjqcdd8xb67lrj3sc1k1lnfbddqqqrvb8n")))

