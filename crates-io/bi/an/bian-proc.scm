(define-module (crates-io bi an bian-proc) #:use-module (crates-io))

(define-public crate-bian-proc-0.1.1 (c (n "bian-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1nqgr5p3xdvy6xlgk96a8alj4n8cynmws20yi3bmaa0hj4p4474p")))

(define-public crate-bian-proc-0.2.0 (c (n "bian-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0a1xrv9h0fpyqw09nw1x9ri4df9ydl49kfjg62rmjmxvv2mwbwz3")))

