(define-module (crates-io bi ta bitalloc) #:use-module (crates-io))

(define-public crate-bitalloc-0.0.1 (c (n "bitalloc") (v "0.0.1") (d (list (d (n "ilog2") (r "*") (d #t) (k 0)))) (h "1psz9zw90qb3i39f7g4s5ibv8pjpciqycbivfmbcx7z6lpc9lmhf")))

(define-public crate-bitalloc-0.0.1-proto (c (n "bitalloc") (v "0.0.1-proto") (d (list (d (n "ilog2") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "01a351qr65kfgfza5vsb92in8cz7bahg9fc6q9l3lv0xs734smc5")))

