(define-module (crates-io bi ta bitarray-set) #:use-module (crates-io))

(define-public crate-bitarray-set-0.4.0 (c (n "bitarray-set") (v "0.4.0") (d (list (d (n "bit-array") (r "^0.4") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "0jf43dvb74czp2rl90ps2aqba1jl7x7iyj0a0dcg73k13k9y5gka") (f (quote (("nightly"))))))

(define-public crate-bitarray-set-0.4.1 (c (n "bitarray-set") (v "0.4.1") (d (list (d (n "bit-array") (r "^0.4") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "0wdchfpvpflmi5c887npgsz8xhfkr2q383rmr5mzk500dj9b0siv") (f (quote (("nightly"))))))

