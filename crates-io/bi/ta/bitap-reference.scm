(define-module (crates-io bi ta bitap-reference) #:use-module (crates-io))

(define-public crate-bitap-reference-0.1.0 (c (n "bitap-reference") (v "0.1.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "strsim") (r "^0.9.2") (d #t) (k 0)))) (h "15mnw87l5nvph5sbj2amb0y19whb0lr42qdpb4q4zzpsp17i2gjv")))

(define-public crate-bitap-reference-0.2.0 (c (n "bitap-reference") (v "0.2.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "strsim") (r "^0.9.2") (d #t) (k 0)))) (h "0ass79bbvq0acf88agkp712aizhv9mn72kk0ffvwc6a0z3g4in2d")))

