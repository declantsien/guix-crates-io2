(define-module (crates-io bi ta bitarr) #:use-module (crates-io))

(define-public crate-bitarr-0.1.1 (c (n "bitarr") (v "0.1.1") (h "03iw96a9njcxpij3s5hrkkcgiq6xyw1xkhzqxdhz6sfj9d3xxpsw") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-bitarr-0.1.2 (c (n "bitarr") (v "0.1.2") (h "19wk2prraw73vmq82i0mcpd0n2r42ig3q9n54xc3h86ysg67mxnh") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-bitarr-0.2.0 (c (n "bitarr") (v "0.2.0") (h "027sm72hcnnv3z1dngqa1w9mwdfm7h3lzr0kyqgams4dhvcn2bw9") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

