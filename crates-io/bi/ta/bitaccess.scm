(define-module (crates-io bi ta bitaccess) #:use-module (crates-io))

(define-public crate-bitaccess-0.1.0 (c (n "bitaccess") (v "0.1.0") (d (list (d (n "bitaccess_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1n1xvbkds690ayzviarwx9xwvml6l13nqn00c7h8vj5xwdhvm566")))

(define-public crate-bitaccess-0.2.0 (c (n "bitaccess") (v "0.2.0") (d (list (d (n "bitaccess_macros") (r "^0.2.0") (d #t) (k 0)))) (h "12rgx269izn7rq1wj6zc5dx75lrgrckl1clcwjwqkmrf9lcvdgdc")))

(define-public crate-bitaccess-0.3.0 (c (n "bitaccess") (v "0.3.0") (d (list (d (n "bitaccess_macros") (r "^0.3.0") (d #t) (k 0)))) (h "0ayc2q85c6ih5hg2rl8l16ng9n8r0jaybnknqh26nvrwy1mwmx64")))

(define-public crate-bitaccess-0.4.0 (c (n "bitaccess") (v "0.4.0") (d (list (d (n "bitaccess_macros") (r "^0.4.0") (d #t) (k 0)))) (h "0ii4mlbm4d5f36lm141qpc7k4r3amvq99dydj1mi123hmv15hvfi")))

