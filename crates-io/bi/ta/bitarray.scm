(define-module (crates-io bi ta bitarray) #:use-module (crates-io))

(define-public crate-bitarray-0.1.0 (c (n "bitarray") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1an6cygc2fhymb3skw3i16xkvp5cw17b3zb05qdilkfqq1a1xfdl") (f (quote (("nightly"))))))

(define-public crate-bitarray-0.1.1 (c (n "bitarray") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "01hpanvbndbq98756fc0ag1n3b7cx5wc8f8idgmwdbmp0z7j6c37") (f (quote (("nightly"))))))

(define-public crate-bitarray-0.1.2 (c (n "bitarray") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1z7pwwnyadpqk3iy8q91hp09n03x3q73bzwczpiap5s8yqrkyy99") (f (quote (("nightly"))))))

(define-public crate-bitarray-0.2.0 (c (n "bitarray") (v "0.2.0") (h "1avjzjkl87n08bnyxjm16hfv1w801ifh32zq0snvcwk3gvfq22na")))

(define-public crate-bitarray-0.2.1 (c (n "bitarray") (v "0.2.1") (d (list (d (n "space") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "1p13lnfp3b3149rkmivyzixy9jwrs9kpmjmda2ga1h4j4kip0vkq")))

(define-public crate-bitarray-0.2.2 (c (n "bitarray") (v "0.2.2") (d (list (d (n "space") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "03d8vl1bsfmhqr6my427l2sb2wb1xcpasnwqpk0vbg2r1ypxn4zd")))

(define-public crate-bitarray-0.2.3 (c (n "bitarray") (v "0.2.3") (d (list (d (n "space") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "17q54y5sd1c0ymmj3631v0cd4vvja0w8g3lkagxpaqiqxg2fcxsl")))

(define-public crate-bitarray-0.2.4 (c (n "bitarray") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "space") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "1r6wnrnrcssn6rjllb47pvx7459sqldw7sgf4mazccv252v0qi4j")))

(define-public crate-bitarray-0.2.5 (c (n "bitarray") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (k 0)) (d (n "space") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "0qc4mz1ahp5qk8j9dnljhza6ndg2wql3dfwdd2iww4z8balwz3bl")))

(define-public crate-bitarray-0.2.6 (c (n "bitarray") (v "0.2.6") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (o #t) (k 0)) (d (n "space") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "0bv5gimgwip1fp9wrg2gmy2zrskrzw1136bppm97zx9f70ri9c9y") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.3.0 (c (n "bitarray") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "space") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0k7fdmls66749535wygcm1jxn6yyq59wv66d8zyf7fr60x3sb5xp") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.4.0 (c (n "bitarray") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "space") (r "^0.13.1") (o #t) (d #t) (k 0)))) (h "03g0jszkj97sndsz4rjbjs47kin55d4g2k0695rxczspl6ra75dj") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.5.0 (c (n "bitarray") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "space") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "02vyjk3lmrjmbryy9j3v7gzix77r0b14nmwkmzxi3xym4vvk6vrb") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.5.1 (c (n "bitarray") (v "0.5.1") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "space") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "05zdd5iva9ff34pgl5dvhnr7h5xa0m8dlx39wcpbqfikqqpirv0a") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.6.0 (c (n "bitarray") (v "0.6.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "space") (r "^0.15.0") (o #t) (d #t) (k 0)))) (h "1aly58vv35xdrbidlbkf124gdzb3g51xdsyw2p7cy6y3npy9dbqf") (f (quote (("unstable-512-bit-simd")))) (y #t)))

(define-public crate-bitarray-0.7.0 (c (n "bitarray") (v "0.7.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "space") (r "^0.15.0") (o #t) (d #t) (k 0)))) (h "1yvqhz6zwlrg1dx5k3cm8fgl769rk6bswwgfkni1awhfm8qjfr6p") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.7.1 (c (n "bitarray") (v "0.7.1") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "space") (r "^0.15.0") (o #t) (d #t) (k 0)))) (h "13kpa685saaw5vm792968h9b0kdzjy8d6ikdnk8qf772qzw9ssvc") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.8.0 (c (n "bitarray") (v "0.8.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "space") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "1a2pk4c4d775kaqfh54rgyycrz3dq9g6910inkw0j64kfy05j05n") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.9.0 (c (n "bitarray") (v "0.9.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "space") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "0mv13ll8wfr7wy7lvzaqnhhfxr58f9pdqwfcc6f6nd70kpwhiaw9") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.9.1 (c (n "bitarray") (v "0.9.1") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "space") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "1q6cx4kkm3nij5l2z30r95pyhypfk40zfr828s7nq4bh3kbflh61") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.9.2 (c (n "bitarray") (v "0.9.2") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "space") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "0idv7wsl412qn6k6wjlgng66m5h92qp5v1073qjwikdf26w913wz") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.9.3 (c (n "bitarray") (v "0.9.3") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("alloc"))) (k 2)) (d (n "space") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "0qhjavmqz13slxky43876534nzwl63sxwf281f6wjjympnww5mg1") (f (quote (("unstable-512-bit-simd"))))))

(define-public crate-bitarray-0.10.0 (c (n "bitarray") (v "0.10.0") (d (list (d (n "bincode") (r "^1.3.3") (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (f (quote ("alloc"))) (k 2)) (d (n "space") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "08j4b0rrhq9jwm2037imw2hqd69qq9c1qdl6676fm6fl5c2ahfw9") (f (quote (("unstable-512-bit-simd"))))))

