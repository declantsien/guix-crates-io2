(define-module (crates-io bi ta bitap) #:use-module (crates-io))

(define-public crate-bitap-0.1.0 (c (n "bitap") (v "0.1.0") (d (list (d (n "bitap-reference") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "0kv1i6xlgb8h215kwzhcach60jb3s1188mbxapywbp3lms7jgi3h")))

(define-public crate-bitap-0.2.0 (c (n "bitap") (v "0.2.0") (d (list (d (n "bitap-reference") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "0kna15gd852cl4mhsxjqm0i41lkslnr3jh1agqa35221s4j4i26f")))

