(define-module (crates-io bi ta bitaccess_macros) #:use-module (crates-io))

(define-public crate-bitaccess_macros-0.1.0 (c (n "bitaccess_macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h54sr7adgf4ddaf2g75m0w6rrxmzaks02jwimzi6252zfidp8cd")))

(define-public crate-bitaccess_macros-0.2.0 (c (n "bitaccess_macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09lpkkrvmwrljkcgr14cwd72icv122045hjbsg2y4j6cyl5hh9qc")))

(define-public crate-bitaccess_macros-0.3.0 (c (n "bitaccess_macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pqd80jg43sh3r3m1hprwx5a9i4r8wxli2p3qplbmcalg2n52wdv")))

(define-public crate-bitaccess_macros-0.4.0 (c (n "bitaccess_macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xppwpkvswqvcxc1sfiwb5501i3yk19fy4w9q3xlgf97b3qf4q27")))

