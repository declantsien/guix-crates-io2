(define-module (crates-io bi ta bitarray-naive) #:use-module (crates-io))

(define-public crate-bitarray-naive-0.1.0 (c (n "bitarray-naive") (v "0.1.0") (h "08pqf4z9nxr8l9mhckpcrrzs8kpinhjk5y492f5vfnq6qildisxp")))

(define-public crate-bitarray-naive-0.1.1 (c (n "bitarray-naive") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wr7vvg6f6d3w6qgwsk0gipfaaw0n78zwrzzkxr5cs5mgi04m158")))

(define-public crate-bitarray-naive-0.1.2 (c (n "bitarray-naive") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p00086d2fgvy2bcjijgwzflmlw7bpf338nyhigq17igr3vk2k5i")))

