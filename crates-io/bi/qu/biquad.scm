(define-module (crates-io bi qu biquad) #:use-module (crates-io))

(define-public crate-biquad-0.1.0 (c (n "biquad") (v "0.1.0") (h "1cvjl3dqhr66ilgwcs43mvmdil9hn49lyp2bf9kzci37qswkyydb")))

(define-public crate-biquad-0.2.0 (c (n "biquad") (v "0.2.0") (h "19w5pbzhr3sl0dsc8hi8df4nicpsfl50bbk2ddxy10aqasn8gz4a")))

(define-public crate-biquad-0.3.0 (c (n "biquad") (v "0.3.0") (d (list (d (n "libm") (r "^0.1.4") (d #t) (k 0)))) (h "0ia44nz025zxm7z2hqnx3xzh768157lq9l5zhgz95lxmib4w35qs")))

(define-public crate-biquad-0.3.1 (c (n "biquad") (v "0.3.1") (d (list (d (n "libm") (r "^0.1.4") (d #t) (k 0)))) (h "0kz1rrcriyq4v3grllj0r5q974nbybrjqid17y9daxd1hd95qd0m")))

(define-public crate-biquad-0.4.0 (c (n "biquad") (v "0.4.0") (d (list (d (n "libm") (r "^0.1.4") (d #t) (k 0)))) (h "0q4khlxqi1nk2n328czqpqcyx1qy6hg4hkqzxdcl153n00w4zba0")))

(define-public crate-biquad-0.4.1 (c (n "biquad") (v "0.4.1") (d (list (d (n "libm") (r "^0.1.4") (d #t) (k 0)))) (h "1sps9irdmpyzw1lhrrj8a86790ysg8w8j4g33n4r28sn53w70xq8")))

(define-public crate-biquad-0.4.2 (c (n "biquad") (v "0.4.2") (d (list (d (n "libm") (r "^0.1.4") (d #t) (k 0)))) (h "0gpc13lag439nmq077wfwz055qbjaxbpk7znvnbddbg3wgsj81c2")))

