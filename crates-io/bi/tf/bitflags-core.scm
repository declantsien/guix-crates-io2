(define-module (crates-io bi tf bitflags-core) #:use-module (crates-io))

(define-public crate-bitflags-core-0.3.3 (c (n "bitflags-core") (v "0.3.3") (h "14idjk692js2qpikdy97jpkc487rm1n4z7wnqbb8792x3qvr29gw")))

(define-public crate-bitflags-core-0.3.4 (c (n "bitflags-core") (v "0.3.4") (h "0wyzg3walq7h4lmrz6vzapygmxg4isblyb2k78b8n5kkp1rsyjw9")))

