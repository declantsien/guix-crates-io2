(define-module (crates-io bi tf bitformat) #:use-module (crates-io))

(define-public crate-bitformat-0.0.1 (c (n "bitformat") (v "0.0.1") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)))) (h "05g0qmibr5gh7sbla767wk835s13yxzrk7gd63nwy0hpaangxc09")))

(define-public crate-bitformat-0.0.2 (c (n "bitformat") (v "0.0.2") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)))) (h "1bckfm1vmz16si5j463c27l7nv5hr9f1v60q1jxkhawcrsmgs4jv")))

(define-public crate-bitformat-0.0.3 (c (n "bitformat") (v "0.0.3") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)))) (h "1yg9wgspms0m94f51iva5f90pk6cx43apqaz4rriz7l0cn3k5ql3")))

(define-public crate-bitformat-0.0.4 (c (n "bitformat") (v "0.0.4") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0i1hvw3kl8njaa56kgs2vkffnnvvhhqh3ahnqg4vsqfxl90fc1bm")))

(define-public crate-bitformat-0.0.5 (c (n "bitformat") (v "0.0.5") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1kqq3kv3zzm8wz7i86a284x1k10dchy2kk8v6bn81993dkdjzlkw")))

