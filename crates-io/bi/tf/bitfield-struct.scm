(define-module (crates-io bi tf bitfield-struct) #:use-module (crates-io))

(define-public crate-bitfield-struct-0.1.0 (c (n "bitfield-struct") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11q04b3rkjr03x13idw4jrxckldd69gp2b5j67ivq9q334f51hv2")))

(define-public crate-bitfield-struct-0.1.1 (c (n "bitfield-struct") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gbq1sp0pc2lng8lf1kcgfwsb6gwg6bkq11zs26irmbf0fqlhx0r")))

(define-public crate-bitfield-struct-0.1.2 (c (n "bitfield-struct") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06m67lf5b7vqb07hlj8xxyg2cqijxaiwbpdi5ya6j6ny2fszzfpp")))

(define-public crate-bitfield-struct-0.1.3 (c (n "bitfield-struct") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18bnwn940y6ybg1gdydikylwp3mypv062s3xlxi2fwic4d2p5fc3")))

(define-public crate-bitfield-struct-0.1.4 (c (n "bitfield-struct") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wr27hqd6aka6xica9kxz790ibpqirm9v2z4kdpbwqfvnsvqg1y4")))

(define-public crate-bitfield-struct-0.1.5 (c (n "bitfield-struct") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qwxikzcvhjx73xmwqjmy66k8223bjswlxr4ka7hf7bkxva79sx0")))

(define-public crate-bitfield-struct-0.1.6 (c (n "bitfield-struct") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fah1jkphy4q031d5yw8j6w0sb77yygaax03chh4barpmb9m2js4")))

(define-public crate-bitfield-struct-0.1.7 (c (n "bitfield-struct") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yimgrrz62k8b8nclmshscmnxwykhvh0fn0x41ll7sdnsnvsrfnh")))

(define-public crate-bitfield-struct-0.1.8 (c (n "bitfield-struct") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08brix13g3gxfk0yrcxp98fgd82anc6l31spl180k99dqhk3kl1j")))

(define-public crate-bitfield-struct-0.2.0 (c (n "bitfield-struct") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ifx4nfzbn5iwy1kvq7azgphzmg8hj9nvj0q11ayskfkpcwr0rfg")))

(define-public crate-bitfield-struct-0.3.0 (c (n "bitfield-struct") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ljmxvhf72n8ysfwhhgq8xbvfrig40aa1dh8gb05z3i6i20pdxwq")))

(define-public crate-bitfield-struct-0.3.1 (c (n "bitfield-struct") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15lgcsyqgkzdp8js91sa0x9jl2lcgrw4zqfdw8ddb1qv00qgc2nh")))

(define-public crate-bitfield-struct-0.3.2 (c (n "bitfield-struct") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qap0pxixkbivigqn1yvjni8yn9fj2n0gj84pwp5ii3f1bjndrx4")))

(define-public crate-bitfield-struct-0.4.0 (c (n "bitfield-struct") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mz4bj1j2y4kh80z1d4rwxj9qp1ly0zzqa30v4342v1wks3qcim0") (y #t)))

(define-public crate-bitfield-struct-0.4.1 (c (n "bitfield-struct") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1im94pylc6574k1zbdrjqmbxawxd558hyi348ympd0cgmjkbd4nw")))

(define-public crate-bitfield-struct-0.4.2 (c (n "bitfield-struct") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jqq1rzi2gx9wkfnqygh9yyd5kwvvbqf4r54ispf3cln8nlvfb8a")))

(define-public crate-bitfield-struct-0.4.3 (c (n "bitfield-struct") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "075803z80ic6h2rmd6hvwgd177sx2p9p5bw7is21pxw7l0aanpfl")))

(define-public crate-bitfield-struct-0.4.4 (c (n "bitfield-struct") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b7w19pvd7bhjk7n6l3prkw9yrpkxwwil4crflcayn4h5isy69h5")))

(define-public crate-bitfield-struct-0.5.0 (c (n "bitfield-struct") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fil718crkbjdaijxgnpnv883h2nl6cwxpq6xmzjvla6x3zmh03q") (y #t)))

(define-public crate-bitfield-struct-0.5.1 (c (n "bitfield-struct") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "003ln86pp3hl5c38d9khng4wad0jr8c70gz5d8whd4ssqyjzl9ym")))

(define-public crate-bitfield-struct-0.5.2 (c (n "bitfield-struct") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b93xa1bhjwla1w73gpqprk9109lx6xbabxyw547zj23rh7rzx8f")))

(define-public crate-bitfield-struct-0.5.3 (c (n "bitfield-struct") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a33zg2vj17dd2nx2r733ww745zjri4bxka7vcnhpzz32m2z6xm1")))

(define-public crate-bitfield-struct-0.5.4 (c (n "bitfield-struct") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17d20v3p606b9806q73a12i3w688ich4l2z37hsk7ks35av2vhza")))

(define-public crate-bitfield-struct-0.5.5 (c (n "bitfield-struct") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sinnnhp0yn3f8lb57wrwfni4qi28acx2hgl68yh53n62f9vzba8")))

(define-public crate-bitfield-struct-0.5.6 (c (n "bitfield-struct") (v "0.5.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g9j0n8fxpp1kgr4x42pn1nshpzhjvhbj0s6m1sipa5nigm8qsx2")))

(define-public crate-bitfield-struct-0.6.0 (c (n "bitfield-struct") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kx9vswk240xxn90bbk515jxngcskwnss210x6qf2d0m1gcy63sb")))

(define-public crate-bitfield-struct-0.6.1 (c (n "bitfield-struct") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x4gyx68w76r466mj009hgfhbjphjrm8g4hay4d94ksp8khxqmqn")))

(define-public crate-bitfield-struct-0.6.2 (c (n "bitfield-struct") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qwr4m9db4mhlsybwqa7w59c0d4r1xhlashk9svkhrm5jdjq9h5d")))

