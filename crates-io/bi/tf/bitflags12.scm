(define-module (crates-io bi tf bitflags12) #:use-module (crates-io))

(define-public crate-bitflags12-1.2.1 (c (n "bitflags12") (v "1.2.1") (h "12djghp4b0lilbd1jl4s4l0zwahblgvqvfw6aw19adhfcry636kc") (f (quote (("example_generated") ("default"))))))

(define-public crate-bitflags12-1.2.1-h1.wl.5g.r1 (c (n "bitflags12") (v "1.2.1-h1.wl.5g.r1") (h "1n8grlhwchdmz772smiilsdbnlz6bxniv9cplrcwrxqvarhgk061") (f (quote (("example_generated") ("default"))))))

(define-public crate-bitflags12-1.2.2-h1.wl.5g.r1 (c (n "bitflags12") (v "1.2.2-h1.wl.5g.r1") (h "16avivmh60ydkpl0w2v1rngf6441df1ilxq4xjwf88s58jzdi036") (f (quote (("example_generated") ("default"))))))

(define-public crate-bitflags12-1.2.2 (c (n "bitflags12") (v "1.2.2") (h "056la52jm99pdxd2x9rx0hwx4fbnszsp7d4zddqv4gcmzw2kqhii") (f (quote (("example_generated") ("default"))))))

