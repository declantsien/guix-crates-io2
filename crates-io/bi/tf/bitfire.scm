(define-module (crates-io bi tf bitfire) #:use-module (crates-io))

(define-public crate-bitfire-2.0.1 (c (n "bitfire") (v "2.0.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)))) (h "1nvrmpd37iw11cfvqp6k08psld4rjp31f6pmdyg9ks2gsxy4ywc8")))

