(define-module (crates-io bi tf bitflags_serde_shim) #:use-module (crates-io))

(define-public crate-bitflags_serde_shim-0.2.0 (c (n "bitflags_serde_shim") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1z7462vwx9w9fvaca79m3m7nwarf5cvgr51dz9ax2p0c9q0pvphp")))

(define-public crate-bitflags_serde_shim-0.2.1 (c (n "bitflags_serde_shim") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1j3liivqxd5xxdq65yhpxrlgfmf2gir9x3rinkgp52gy2b9br1sf")))

(define-public crate-bitflags_serde_shim-0.2.2 (c (n "bitflags_serde_shim") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09ghkdjad5yivr0fwjp98562nhv721n5rz566fdw63i8y0kddhr5") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitflags_serde_shim-0.2.3 (c (n "bitflags_serde_shim") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "00w2ddy73s4x57qi2bpr0cy1rmlkx6n353avgdpixbv9sqkzc3ac") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-bitflags_serde_shim-0.2.4 (c (n "bitflags_serde_shim") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0i7jqshs8z0yh0h5x38bk6f9ycq99v0f26zqm40w0mw5q51x0y76") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitflags_serde_shim-0.2.5 (c (n "bitflags_serde_shim") (v "0.2.5") (d (list (d (n "bitflags") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1j8av37mcigrxr55631q4jg6fx0y6nggm0zqrb7963rbqf01f0yz") (f (quote (("std") ("default" "std"))))))

