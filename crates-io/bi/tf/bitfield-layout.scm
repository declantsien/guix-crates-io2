(define-module (crates-io bi tf bitfield-layout) #:use-module (crates-io))

(define-public crate-bitfield-layout-0.1.0 (c (n "bitfield-layout") (v "0.1.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "0jij4i7wd2rl37vza4b7889vh9fbj5j07yvgdfdvs29iv4dwcmir")))

(define-public crate-bitfield-layout-0.1.1 (c (n "bitfield-layout") (v "0.1.1") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "12kg1q6mdnkv98kzfwyv1anqbzbry9sf3kbhkrik070s78wcl84r")))

(define-public crate-bitfield-layout-0.1.2 (c (n "bitfield-layout") (v "0.1.2") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "0zzsn0w1fzwp8cdp2nxq0grswnn17iwvp42n6i22vh911qpv3fkw")))

(define-public crate-bitfield-layout-0.2.0 (c (n "bitfield-layout") (v "0.2.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "1ahrvckjdx0fc0f9sgx23pmcl5c9frhsd7mzzqfs4j37pb5svhgn")))

(define-public crate-bitfield-layout-0.2.1 (c (n "bitfield-layout") (v "0.2.1") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "1flk1i0sar0dxxbzgp9r8n2gnpjgc84n4frgz1fpvpyafi8vyjyv")))

(define-public crate-bitfield-layout-0.3.0 (c (n "bitfield-layout") (v "0.3.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "19fa73ssf9dwa4h2qz4nhr7nrixmhn9661v7ryzslmnfln6xxyj7")))

(define-public crate-bitfield-layout-0.4.0 (c (n "bitfield-layout") (v "0.4.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)))) (h "1vr57mm86znr1mflj7wiyd0mvy56h1b11zm0ykpdba3nh0b3qiz1")))

