(define-module (crates-io bi tf bitflow) #:use-module (crates-io))

(define-public crate-bitflow-0.3.7 (c (n "bitflow") (v "0.3.7") (h "1byn494jv417bzq9731jfh5kj7mg20qlrgiq5sl1mdibnhydn6in")))

(define-public crate-bitflow-0.5.1 (c (n "bitflow") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dotenv") (r "^0.13") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures-fs") (r "^0.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mimesniff") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.39") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.39") (d #t) (k 0)) (d (n "sentry") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "slog") (r "^2.4") (d #t) (k 0)) (d (n "slog-json") (r "^2.3") (d #t) (k 0)) (d (n "slog-scope") (r "^4.1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ras51ll9slrxdpadmp2yhx504czfa1nkvfmyx56brdsg1r78n7g")))

