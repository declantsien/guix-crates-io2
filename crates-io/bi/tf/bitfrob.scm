(define-module (crates-io bi tf bitfrob) #:use-module (crates-io))

(define-public crate-bitfrob-0.1.0 (c (n "bitfrob") (v "0.1.0") (h "1ikgx1ywgpr8c2c2crghswjv311mkl0q9fd0j0axa0m0zyfs21g4")))

(define-public crate-bitfrob-0.1.1 (c (n "bitfrob") (v "0.1.1") (h "1lj75dlg0xf3bwr17bavjmb1rqfkfr15sp6p397a5qm32sxs9bk7")))

(define-public crate-bitfrob-0.2.0 (c (n "bitfrob") (v "0.2.0") (h "0vdc26a5g6i8vypy686m6byz9w4ncbn0533ql423z4bkqhsrq2xw")))

(define-public crate-bitfrob-0.2.1 (c (n "bitfrob") (v "0.2.1") (h "1iw02q8ax5n87fv2jdp79cniksv7zpj2d2bhzkgr362as5k6qpc1")))

(define-public crate-bitfrob-0.2.2 (c (n "bitfrob") (v "0.2.2") (h "0mlfcg1ckg1fc0x3r9zj7nvas4zqbsm4zr67wk010h8709xsjsfl")))

(define-public crate-bitfrob-0.2.3 (c (n "bitfrob") (v "0.2.3") (h "0pzpc3lxncsx0knl37dcvb641c6mdsxrhwzl8mdbki3lx7rgy67v") (f (quote (("track_caller"))))))

(define-public crate-bitfrob-1.0.0 (c (n "bitfrob") (v "1.0.0") (h "0177a6pj1g8baqxr75dbms6d84jp8cwzd3b9irg71wal5qqi8fza") (f (quote (("track_caller"))))))

(define-public crate-bitfrob-1.1.0 (c (n "bitfrob") (v "1.1.0") (h "0giisng87par9ac113pld8vz5iir27gvqpdk18rxbjpl0z822wyd") (f (quote (("track_caller"))))))

(define-public crate-bitfrob-1.2.0 (c (n "bitfrob") (v "1.2.0") (h "047gm2n5kr3x78327haarnjn228236nb23d4zmdf467r6b0ihrih") (f (quote (("track_caller"))))))

(define-public crate-bitfrob-1.3.0 (c (n "bitfrob") (v "1.3.0") (h "0fx6n7ldh731dhrpm8jww4d0z5k8h19gb2czwiz7ybxi5r01yb5c") (f (quote (("track_caller"))))))

(define-public crate-bitfrob-1.3.1 (c (n "bitfrob") (v "1.3.1") (h "0851fs3b7l918g15zysgp9pfvq3x9jx2rpc236xhg26w334cg5js") (f (quote (("track_caller"))))))

