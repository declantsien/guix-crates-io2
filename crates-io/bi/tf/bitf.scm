(define-module (crates-io bi tf bitf) #:use-module (crates-io))

(define-public crate-bitf-0.9.0 (c (n "bitf") (v "0.9.0") (d (list (d (n "proc-macro2") (r ">=1.0.34") (d #t) (k 0)) (d (n "quote") (r ">=1.0.10") (d #t) (k 0)) (d (n "syn") (r ">=1.0.84") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0jqslh2flql5994s2rlanw485i3h9gjwhsw949fnqc0sx7g3v0ab")))

(define-public crate-bitf-0.10.0 (c (n "bitf") (v "0.10.0") (d (list (d (n "proc-macro2") (r ">=1.0.34") (d #t) (k 0)) (d (n "quote") (r ">=1.0.10") (d #t) (k 0)) (d (n "syn") (r ">=1.0.84") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0zf2wi0vi9l6raabl9x1dn0fa2m50plys8dyhkwq2ihgl300k45d")))

(define-public crate-bitf-0.10.1 (c (n "bitf") (v "0.10.1") (d (list (d (n "proc-macro2") (r ">=1.0.34") (d #t) (k 0)) (d (n "quote") (r ">=1.0.10") (d #t) (k 0)) (d (n "syn") (r ">=1.0.84") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "17bwvr3fvnsbisqbrz51axmnizsg8rdw344613lya4lrzf38a3qv")))

(define-public crate-bitf-0.11.1 (c (n "bitf") (v "0.11.1") (d (list (d (n "proc-macro2") (r ">=1.0.34") (d #t) (k 0)) (d (n "quote") (r ">=1.0.10") (d #t) (k 0)) (d (n "syn") (r ">=1.0.84") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "11dk4pdfd0dwqjqi9cvs4b2p0bda87qq1kcmilkwl6kwh6j039g4")))

(define-public crate-bitf-1.0.0 (c (n "bitf") (v "1.0.0") (d (list (d (n "proc-macro2") (r ">=1.0.34") (d #t) (k 0)) (d (n "quote") (r ">=1.0.10") (d #t) (k 0)) (d (n "syn") (r ">=1.0.84") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "002rsyx26gxlcd3nkrqimaich82m1r4250l2w178gc0z60qdj933")))

(define-public crate-bitf-1.0.1 (c (n "bitf") (v "1.0.1") (d (list (d (n "proc-macro2") (r ">=1.0.34") (d #t) (k 0)) (d (n "quote") (r ">=1.0.10") (d #t) (k 0)) (d (n "syn") (r ">=1.0.84") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "06b4wdh5w9jpfx39z6crxnwiywa1shx0szsrqqq99y8bja9ki9qk")))

(define-public crate-bitf-1.1.0 (c (n "bitf") (v "1.1.0") (d (list (d (n "proc-macro2") (r ">=1.0.34") (d #t) (k 0)) (d (n "quote") (r ">=1.0.10") (d #t) (k 0)) (d (n "syn") (r ">=1.0.84") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0nbjck3gk26g0g85psh2xwiiv1j37hhqbnxk96wxnnyz7bb9i86y")))

(define-public crate-bitf-1.2.0 (c (n "bitf") (v "1.2.0") (d (list (d (n "proc-macro2") (r ">=1.0.34") (d #t) (k 0)) (d (n "quote") (r ">=1.0.10") (d #t) (k 0)) (d (n "syn") (r ">=1.0.84") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "12xn5p52k8sdxjcpq7pfrf7i585a3ijq6acspm8zs1jiwxfhcmgd")))

(define-public crate-bitf-1.3.0 (c (n "bitf") (v "1.3.0") (d (list (d (n "proc-macro2") (r ">=1.0.34") (d #t) (k 0)) (d (n "quote") (r ">=1.0.10") (d #t) (k 0)) (d (n "syn") (r ">=1.0.84") (f (quote ("full"))) (d #t) (k 0)))) (h "05m5mkcbdnic7yg0h8307f7if9mhi9xzixdxdzp6mwcamga78ccd")))

