(define-module (crates-io bi tf bitfield-derive) #:use-module (crates-io))

(define-public crate-bitfield-derive-0.1.0 (c (n "bitfield-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "02933522h5jc96bxhmzn0m29xmq8g4w5d7w1hv13anmqck3qqc3s")))

(define-public crate-bitfield-derive-0.1.1 (c (n "bitfield-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10mjk92kipm9b03cq14csbs5j12c9lhqp5gdsll89rgdm0dhxivc")))

(define-public crate-bitfield-derive-0.1.2 (c (n "bitfield-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0gii7lqrw4q35h3jbhjpya5bsb03r6r3765l3ap7vdxfdvjxy0wy")))

(define-public crate-bitfield-derive-0.2.0 (c (n "bitfield-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0x6qrm1l2sw6s499vd361gcayg1sf9f7rrzqphgmblv0jd1n0a12")))

(define-public crate-bitfield-derive-0.1.3 (c (n "bitfield-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "10p3ijgyi9294h24g54vbiizyv0f4pbqglr7yvqxf08m1fq5a00b")))

(define-public crate-bitfield-derive-0.2.1 (c (n "bitfield-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12q4p2xnp7zs6y1fzfpxnb7zcpzcls6sy9p4zqi7iis1wb9wyc4w")))

