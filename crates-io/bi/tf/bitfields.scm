(define-module (crates-io bi tf bitfields) #:use-module (crates-io))

(define-public crate-bitfields-0.1.0 (c (n "bitfields") (v "0.1.0") (h "16ckyyfnqn3f7acwlh2vdr4p6bqq9l2dhyyw3hp2mwnjawx5incb")))

(define-public crate-bitfields-0.2.0 (c (n "bitfields") (v "0.2.0") (h "02mz07dkjgd1v8wpg2bwaj4z6xcv1y6m899y0nz5fr7nvqzxv81m")))

