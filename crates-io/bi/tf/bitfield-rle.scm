(define-module (crates-io bi tf bitfield-rle) #:use-module (crates-io))

(define-public crate-bitfield-rle-0.0.0 (c (n "bitfield-rle") (v "0.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "sparse-bitfield") (r "^0.3.3") (d #t) (k 2)) (d (n "varinteger") (r "^1.0.3") (d #t) (k 0)))) (h "1f73j8yghbamxxk3qh04nfcsxnbilsx9rj4856jqhhmrim2qbpi9")))

(define-public crate-bitfield-rle-0.1.0 (c (n "bitfield-rle") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "varinteger") (r "^1.0.6") (d #t) (k 0)))) (h "15df2lkfnczp0l916f0pzg4i4nw1qyb6hgx5arv5i6xvm248xjac")))

(define-public crate-bitfield-rle-0.1.1 (c (n "bitfield-rle") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "varinteger") (r "^1.0.6") (d #t) (k 0)))) (h "19n989rsjwvwlxjsxwfvyq0by5pdvvanx9f1k8xc5p8lkgcp99sx")))

(define-public crate-bitfield-rle-0.2.0 (c (n "bitfield-rle") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "varinteger") (r "^1.0.6") (d #t) (k 0)))) (h "1l3xypl9k93irw408bjv41rdma7jngix6ymvwihyvlvvbc8cr2iz")))

(define-public crate-bitfield-rle-0.2.1 (c (n "bitfield-rle") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "varinteger") (r "^1.0.6") (d #t) (k 0)))) (h "1qc4v53fv4wpfy1m46fk747qmvmpy281cx4w78jdkmnhpxs5ap91")))

