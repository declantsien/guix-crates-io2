(define-module (crates-io bi tf bitflag-attr) #:use-module (crates-io))

(define-public crate-bitflag-attr-0.1.0 (c (n "bitflag-attr") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1w6zvay1xykrh8q3k22p8z156p9icsda5vr70ba7sxv6mbi3d3dl") (r "1.70.0")))

(define-public crate-bitflag-attr-0.2.0 (c (n "bitflag-attr") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "19kmp50rlbf1p7m597bpb0ylvjgdzk3325cqjw26p8dlg7z6n70n") (r "1.70.0")))

(define-public crate-bitflag-attr-0.3.0 (c (n "bitflag-attr") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1pwmm785x8d96xdwsna8l5lwdyaic72xywanvw3r4yljzl6jcww9") (r "1.70.0")))

