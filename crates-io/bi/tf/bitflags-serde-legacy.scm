(define-module (crates-io bi tf bitflags-serde-legacy) #:use-module (crates-io))

(define-public crate-bitflags-serde-legacy-0.1.0 (c (n "bitflags-serde-legacy") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "bitflags1") (r "^1") (d #t) (k 2) (p "bitflags")) (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "03k8f6bvilsa3bv5y94j29aadnj0f41df28c2a0fj3a0fbxgkv6z")))

(define-public crate-bitflags-serde-legacy-0.1.1 (c (n "bitflags-serde-legacy") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "bitflags1") (r "^1") (d #t) (k 2) (p "bitflags")) (d (n "serde") (r "^1") (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1k309b7bm5njan07kj2zs10jp8cszw0phdlb5vcmmlmn506fcr2v")))

