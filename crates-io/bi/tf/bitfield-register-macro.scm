(define-module (crates-io bi tf bitfield-register-macro) #:use-module (crates-io))

(define-public crate-bitfield-register-macro-0.2.0 (c (n "bitfield-register-macro") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1cn37y40gdszx3pvaj54xj4bsnyc88dpslb3b82fghvw8jrmqscz")))

(define-public crate-bitfield-register-macro-0.2.1 (c (n "bitfield-register-macro") (v "0.2.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1ji9n8hiqfgnhb650nlz7n6wnfl6ynj1p87s7wai8ak9sy3fdy3d")))

