(define-module (crates-io bi tf bitfield-register) #:use-module (crates-io))

(define-public crate-bitfield-register-0.1.0 (c (n "bitfield-register") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0cm7jfsm9798q0wrq8s08lnq0yl48k9wlx6ghjwyvb8wfak3vbb1")))

(define-public crate-bitfield-register-0.1.1 (c (n "bitfield-register") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "111cf71sjfniwgc4pdsri4pj4nnz3zk2pvqamzf4jvzmhp5131qk")))

(define-public crate-bitfield-register-0.1.2 (c (n "bitfield-register") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0z348xa79hnbq2mbr1d0r9kw2pypwsbwb8hidw0ahsqsi8fh6xxj")))

(define-public crate-bitfield-register-0.1.4 (c (n "bitfield-register") (v "0.1.4") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1i1cbx7hprzmlpq2rxrpvsz32wl1cnzfb4znqf3wq16jh18zkpjp")))

(define-public crate-bitfield-register-0.1.5 (c (n "bitfield-register") (v "0.1.5") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0nmkr4rr6b63yngacfk0cjjga3wd93g5v0r7xxsjnxs58g1lgmzd")))

(define-public crate-bitfield-register-0.2.0 (c (n "bitfield-register") (v "0.2.0") (d (list (d (n "bitfield-register-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0vigwh8pwvaq2nwqq78h580rrzzprm1mbwjc2iwb06p1cdl1f8rh")))

(define-public crate-bitfield-register-0.2.1 (c (n "bitfield-register") (v "0.2.1") (d (list (d (n "bitfield-register-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0qx8aw81f8j5ysbghcralgh5i358k6hmypjd317gqimimk94fqjr")))

