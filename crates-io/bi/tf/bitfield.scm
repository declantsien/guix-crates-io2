(define-module (crates-io bi tf bitfield) #:use-module (crates-io))

(define-public crate-bitfield-0.0.1 (c (n "bitfield") (v "0.0.1") (h "1nbdzxyfllvmh4iml269cpsadbmv402hs70mhamsqxi95qyighaa")))

(define-public crate-bitfield-0.0.2 (c (n "bitfield") (v "0.0.2") (h "11s92krpx7mslwh9w5hfppr6glpbi1h1rzy0ays31rk32xradhva")))

(define-public crate-bitfield-0.0.3 (c (n "bitfield") (v "0.0.3") (h "0mxv2i4wr7zag68489663v3axdwqix3pv9mi84n2lm01yaf0cgib")))

(define-public crate-bitfield-0.0.4 (c (n "bitfield") (v "0.0.4") (h "099ap80lvhf0hkszdl3g2iig7a0pjccygd42kdjd1k40020jzkj0")))

(define-public crate-bitfield-0.0.5 (c (n "bitfield") (v "0.0.5") (h "16c0z6m1q4rk75xrncjccijmwqxi58if56x3pdvp4ki1zbwvk0l4")))

(define-public crate-bitfield-0.0.6 (c (n "bitfield") (v "0.0.6") (h "0xflx8akhpdbw44xfh239dcijm3imbmq7jbm368rbfpffkbwb0yf")))

(define-public crate-bitfield-0.0.7 (c (n "bitfield") (v "0.0.7") (h "0q0rdyfwx8m6v23z0k6lzmd1jvqz8xn3dk40chsrnmm209ys7m2d")))

(define-public crate-bitfield-0.0.8 (c (n "bitfield") (v "0.0.8") (h "0xbc5h6mvd9j5fdqgavb80g1sk8vwyil652zf8fss27lwm7g07sd")))

(define-public crate-bitfield-0.0.9 (c (n "bitfield") (v "0.0.9") (h "0sdx3bzc2bcc04d5irhgh3lbn13v1nc5b75qd96yxgsd88p9p1n2")))

(define-public crate-bitfield-0.0.10 (c (n "bitfield") (v "0.0.10") (h "11pw53ps25s53yx7b3f0b9pd6j9bw4qgbxw99baazi461q6icvq8")))

(define-public crate-bitfield-0.0.11 (c (n "bitfield") (v "0.0.11") (h "16lvspjhi13khdf449dl0r8q91lsij57nf69a62mrw9q029bd57z")))

(define-public crate-bitfield-0.0.12 (c (n "bitfield") (v "0.0.12") (h "0z1zsnad9j3d1qdvmz5pwf9vki0lk7h351x1s8i3q6054v6004kp")))

(define-public crate-bitfield-0.0.13 (c (n "bitfield") (v "0.0.13") (h "1dxmf1nas9s9idbbw8dwjbahcn7v0cr7xdhfv9la0lj2p1h22hhf")))

(define-public crate-bitfield-0.10.0 (c (n "bitfield") (v "0.10.0") (h "0jipdwidlis2k7vppzd3d4c5vaq4q00qzbk4nyvsb7fpbdxrc15b")))

(define-public crate-bitfield-0.11.0 (c (n "bitfield") (v "0.11.0") (h "0sngfr5as1ncvx6hfh6bfqsdsihcgzjgq2d32akp2cpzkydsx2gr")))

(define-public crate-bitfield-0.12.0 (c (n "bitfield") (v "0.12.0") (h "03jq6srsygimckk0ii8jgmaqg2r9w2fdv0cqj4kswaa43088r67b")))

(define-public crate-bitfield-0.12.1 (c (n "bitfield") (v "0.12.1") (h "1b053s88f953wgs56d83fbbgiz77scrbhi2cbqfamzfbh88am0nf")))

(define-public crate-bitfield-0.12.2 (c (n "bitfield") (v "0.12.2") (h "1zqbmr0p7mpjy48in1hyhd93h91a3yha912g4i5yscky4k3b16hh")))

(define-public crate-bitfield-0.13.0 (c (n "bitfield") (v "0.13.0") (h "0ihs8sxy8abf7z16hm3n2nzg0f3wf9xgjgp67m56k6l9bqkvkpqc")))

(define-public crate-bitfield-0.13.1 (c (n "bitfield") (v "0.13.1") (h "10sfib2wwy29kzriwfz6sflw0fsss4f1n2rri51nm89wkxmysq52")))

(define-public crate-bitfield-0.13.2 (c (n "bitfield") (v "0.13.2") (h "06g7jb5r2b856vnhx76081fg90jvmy61kjqcfjysgmd5hclvvbs6")))

(define-public crate-bitfield-0.14.0 (c (n "bitfield") (v "0.14.0") (h "1b26k9acwss4qvrl60lf9n83l17d4hj47n5rmpd3iigf9j9n0zid")))

(define-public crate-bitfield-0.15.0 (c (n "bitfield") (v "0.15.0") (h "1lamr3dxhkg6frwz856936wqy0xixa423k3wj1nvazhr4khsc8f8")))

