(define-module (crates-io bi nu binutils) #:use-module (crates-io))

(define-public crate-binutils-0.1.0 (c (n "binutils") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 1)))) (h "1q7zm4a6k213b1lkf2aqdyxsyrsdxdvrbz796lisj13zkvgwq73n")))

(define-public crate-binutils-0.1.1 (c (n "binutils") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 1)))) (h "030fvv6grh7c7chdwn78377jnk3nq3cs4wwm4cvcx06f5j1yrxyi")))

