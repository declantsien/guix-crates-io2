(define-module (crates-io bi p2 bip21) #:use-module (crates-io))

(define-public crate-bip21-0.1.0 (c (n "bip21") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.27.1") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "percent-encoding-rfc3986") (r "^0.1.3") (d #t) (k 0)))) (h "1yx8907iy2q2a29fkhy40wyllqf42wi6c21y8ckcczkz11s5lbc4") (f (quote (("std" "percent-encoding-rfc3986/std") ("non-compliant-bytes" "either"))))))

(define-public crate-bip21-0.1.1 (c (n "bip21") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.27.1") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "percent-encoding-rfc3986") (r "^0.1.3") (d #t) (k 0)))) (h "03dlbzn0fmsnn18hql9hq56m7c03aqzdrsi9b1kw3y8f9dwmzg0r") (f (quote (("std" "percent-encoding-rfc3986/std") ("non-compliant-bytes" "either"))))))

(define-public crate-bip21-0.1.2 (c (n "bip21") (v "0.1.2") (d (list (d (n "bitcoin") (r "^0.27.1") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "percent-encoding-rfc3986") (r "^0.1.3") (d #t) (k 0)))) (h "12pj13wlxixd4kqpc4q2qxck9yzmm8d5b9xg64qlbp2m7qy1q6j2") (f (quote (("std" "percent-encoding-rfc3986/std") ("non-compliant-bytes" "either"))))))

(define-public crate-bip21-0.2.0 (c (n "bip21") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "percent-encoding-rfc3986") (r "^0.1.3") (d #t) (k 0)))) (h "1p5f6k0pn9nfkjy14q1n20ak5z01wlb4lqmvc73vgkwwy9d4g60r") (f (quote (("std" "percent-encoding-rfc3986/std") ("non-compliant-bytes" "either"))))))

(define-public crate-bip21-0.3.0 (c (n "bip21") (v "0.3.0") (d (list (d (n "bitcoin") (r "^0.30.0") (d #t) (k 0)) (d (n "either") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "percent-encoding-rfc3986") (r "^0.1.3") (d #t) (k 0)))) (h "1k5n233b8p0i3bccadf8mslc5qf3ycnnm8b8rlngyranpayz14bk") (f (quote (("std" "percent-encoding-rfc3986/std") ("non-compliant-bytes" "either"))))))

(define-public crate-bip21-0.3.1 (c (n "bip21") (v "0.3.1") (d (list (d (n "bitcoin") (r "^0.30.0") (k 0)) (d (n "bitcoin") (r "^0.30.0") (f (quote ("std"))) (d #t) (k 2)) (d (n "either") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "percent-encoding-rfc3986") (r "^0.1.3") (d #t) (k 0)))) (h "1piylkck6vw3fdxk5g8bpwfyrqmn4s8k0plg8xdf8s5h6b3355cb") (f (quote (("std" "percent-encoding-rfc3986/std" "bitcoin/std") ("non-compliant-bytes" "either"))))))

(define-public crate-bip21-0.4.0 (c (n "bip21") (v "0.4.0") (d (list (d (n "bitcoin") (r "^0.31.0") (k 0)) (d (n "bitcoin") (r "^0.31.0") (f (quote ("std"))) (d #t) (k 2)) (d (n "either") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "percent-encoding-rfc3986") (r "^0.1.3") (d #t) (k 0)))) (h "04llh8wwygwlg5sz65n6azqrr2pnpb0md3mif9glywxxhyc4br2d") (f (quote (("std" "percent-encoding-rfc3986/std" "bitcoin/std") ("non-compliant-bytes" "either"))))))

