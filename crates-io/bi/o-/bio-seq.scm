(define-module (crates-io bi o- bio-seq) #:use-module (crates-io))

(define-public crate-bio-seq-0.1.0 (c (n "bio-seq") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)))) (h "1q26qn9gmg0nih9icdy7a97l1nnk0d0zslhxssligm8343g7lnyh") (y #t)))

(define-public crate-bio-seq-0.3.0 (c (n "bio-seq") (v "0.3.0") (d (list (d (n "bio-seq-derive") (r "^1.0.0") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)))) (h "0llim2wpvm8jfr902rhf9qmqzsnbjwy0zginwzda8fw3r0mw8ldv") (y #t)))

(define-public crate-bio-seq-0.8.1 (c (n "bio-seq") (v "0.8.1") (d (list (d (n "bio-seq-derive") (r "^1.5.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)))) (h "1pcjmbq7fw8hql94kypsjlyx038ckkidrfqyj2yjljbp53yfwkxi")))

(define-public crate-bio-seq-0.8.3 (c (n "bio-seq") (v "0.8.3") (d (list (d (n "bio-seq-derive") (r "^1.5.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)))) (h "0ynh4qvycxz9dxpx2kjqan2xx7508s2algz5qfw9nvh5xrrk1ljq")))

(define-public crate-bio-seq-0.8.4 (c (n "bio-seq") (v "0.8.4") (d (list (d (n "bio-seq-derive") (r "^1.5.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)))) (h "0iq15bqw9wi5bw6yspjclr4z8v4p483nfricba574crvl4jhxnmr")))

(define-public crate-bio-seq-0.8.5 (c (n "bio-seq") (v "0.8.5") (d (list (d (n "bio-seq-derive") (r "^1.5.0") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)))) (h "0fkk6w3xbydyz8l4l2jiqh7xrz3lwnrqfxrqyjskcdvp9c9kfhhp")))

(define-public crate-bio-seq-0.9.0 (c (n "bio-seq") (v "0.9.0") (d (list (d (n "bio-seq-derive") (r "^1.6.4") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)))) (h "0xmzxs4k0i4pnwaij7vn6pmr9657wpc6jkqn6036s0k0fhz91sq4")))

(define-public crate-bio-seq-0.10.0 (c (n "bio-seq") (v "0.10.0") (d (list (d (n "bio-seq-derive") (r "^1.6.4") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)))) (h "0grdk7sy0xr2fdvyzhj22grnb58ynra40xi72kjqr9dvg800kdgk")))

(define-public crate-bio-seq-0.11.2 (c (n "bio-seq") (v "0.11.2") (d (list (d (n "bio-seq-derive") (r "^1.6.4") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0zm5dsn36zi37z4kaw4shjwl7k56v20kssnbhk8nivqwmd7vlxy9")))

(define-public crate-bio-seq-0.12.1 (c (n "bio-seq") (v "0.12.1") (d (list (d (n "bio-seq-derive") (r "^2.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "16ynm7sfj35pafdvpdd06483xwa0y8z14l8irjlnrmjkj7b3jnq1") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-bio-seq-0.12.2 (c (n "bio-seq") (v "0.12.2") (d (list (d (n "bio-seq-derive") (r "^2.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1wvkgkl5kfjqi65i9bpkn4ifqv3cnrn2931wnfsf2c72d1s5dr1d") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-bio-seq-0.12.3 (c (n "bio-seq") (v "0.12.3") (d (list (d (n "bio-seq-derive") (r "^2.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0k6zacd958a89mvav7gxlk575mnpy82n1nk2dl3q8cw9pb9j76b3") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

(define-public crate-bio-seq-0.12.4 (c (n "bio-seq") (v "0.12.4") (d (list (d (n "bio-seq-derive") (r "^2.0.2") (d #t) (k 0)) (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1c7pxl12v8f421i8ff5jx1kl1cxq1c344i30dzrxqaph4jfq1hjy") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_derive"))))))

