(define-module (crates-io bi o- bio-seq-derive) #:use-module (crates-io))

(define-public crate-bio-seq-derive-1.0.0 (c (n "bio-seq-derive") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "1l0i37z4vnx1jwgv22g4pil0aaff8gz6xsjq13d32kbib1nn5s7m")))

(define-public crate-bio-seq-derive-1.5.0 (c (n "bio-seq-derive") (v "1.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sma1pd5462g4d1wa4dcbqldmgfsxwpi8j8lcb3b0lqdpffgli9b")))

(define-public crate-bio-seq-derive-1.6.0 (c (n "bio-seq-derive") (v "1.6.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qayqm2l67qi1mxzxgjj6xx07zgy5hvhxl07rq8rln5gh5s49108")))

(define-public crate-bio-seq-derive-1.6.3 (c (n "bio-seq-derive") (v "1.6.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rwv95wy8vi6nm9nyfslk0qcps1igqx9npq84b30cf3m939p4n5d")))

(define-public crate-bio-seq-derive-1.6.4 (c (n "bio-seq-derive") (v "1.6.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "175z06w1c59bqx16lp1xkrlfas2ixafpbx90snh8mnn4w6vh70zq")))

(define-public crate-bio-seq-derive-2.0.1 (c (n "bio-seq-derive") (v "2.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zy7g5lnbm6slr0d72mzilkada182c441s8fw1irgzzs1rx9wdix")))

(define-public crate-bio-seq-derive-2.0.2 (c (n "bio-seq-derive") (v "2.0.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1kh5qd1513knypwadh027bbrmfjlpn0yr8q9khby2rars3bazkkk")))

