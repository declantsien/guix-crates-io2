(define-module (crates-io bi o- bio-seq-algos) #:use-module (crates-io))

(define-public crate-bio-seq-algos-0.1.0 (c (n "bio-seq-algos") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0f6v5qhsnn2wfs9iniwz73mp4wa9p196drcdnc2z0b5zvkzgmc19")))

(define-public crate-bio-seq-algos-0.1.1 (c (n "bio-seq-algos") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0cf6b8vib37ym4a5bjks85778rlc1rv4b7kxs6l9mdiid1v4b6wl")))

(define-public crate-bio-seq-algos-0.1.2 (c (n "bio-seq-algos") (v "0.1.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0aqli4mfm92nbh4j49ywbvkhc5i39jmy67w0dnndh42ghydw24lh")))

(define-public crate-bio-seq-algos-0.1.3 (c (n "bio-seq-algos") (v "0.1.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0r9wh7vz50j84s9cdr1747ghfb3ddx6zfq8jcrp941g9gyhfaj3j")))

(define-public crate-bio-seq-algos-0.1.4 (c (n "bio-seq-algos") (v "0.1.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1myl4jm19c9wjkhgl4szs2xymf04j5gwvcs5cf2zr5di3a6nzp90")))

(define-public crate-bio-seq-algos-0.1.5 (c (n "bio-seq-algos") (v "0.1.5") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "07n4r5sxg6lr5y8kjz9c1sqxgzhdwzprkimmblwvp642gsgn1a5x")))

(define-public crate-bio-seq-algos-0.1.6 (c (n "bio-seq-algos") (v "0.1.6") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0h1wka8pm1inr1iskiw3c144fp2yn11cz570rdq4wlgjqnlymb44")))

(define-public crate-bio-seq-algos-0.1.7 (c (n "bio-seq-algos") (v "0.1.7") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1nqj44yqd5j3ipah9jycracl8935znamz6458wi98n5hdwl50772")))

(define-public crate-bio-seq-algos-0.1.8 (c (n "bio-seq-algos") (v "0.1.8") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1hfz015pbwbj0y72wl5jdd4wavlvwxik01pwj1armj33djni8h4b")))

(define-public crate-bio-seq-algos-0.1.9 (c (n "bio-seq-algos") (v "0.1.9") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1088la8k5q0xw3whvyndbxl03rblrdiywhbvxh3graayylgvm8c0")))

(define-public crate-bio-seq-algos-0.1.10 (c (n "bio-seq-algos") (v "0.1.10") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "0kh02nfwpscnfq8dxzj33cfgb98j0ss1hfqrzksbcg94zlaljc4d")))

(define-public crate-bio-seq-algos-0.1.11 (c (n "bio-seq-algos") (v "0.1.11") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "1z58298mjdha6iidg5ai1lv6ndrzlvd29xzwrbdraqvv6134i1aw")))

(define-public crate-bio-seq-algos-0.1.12 (c (n "bio-seq-algos") (v "0.1.12") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "11y5b5hn51ywc0ybs2ppnabav3y196sxaclng8x3g1fpsirz0qq4")))

(define-public crate-bio-seq-algos-0.1.13 (c (n "bio-seq-algos") (v "0.1.13") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "095mgggmkkgj0migarb6g6grrypgm8gkar6yj2scs32vklbfbj2g")))

(define-public crate-bio-seq-algos-0.1.14 (c (n "bio-seq-algos") (v "0.1.14") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0g9cplgryvm6kdph47l6m8m1phlbfdv2q4ramk7yypaq7mi3rphg")))

(define-public crate-bio-seq-algos-0.1.15 (c (n "bio-seq-algos") (v "0.1.15") (d (list (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1f1f4b0l2lyhvvschzl90zhi8xjia4qy5dbwxaczzd7g0c7jspnw")))

(define-public crate-bio-seq-algos-0.1.17 (c (n "bio-seq-algos") (v "0.1.17") (d (list (d (n "hashbrown") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "131vwb06lpa17y40sizzwgjimv2f0d7qdkwd63zngqi5h2ijwyxs")))

