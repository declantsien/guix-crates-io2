(define-module (crates-io bi o- bio-streams) #:use-module (crates-io))

(define-public crate-bio-streams-0.3.0 (c (n "bio-streams") (v "0.3.0") (d (list (d (n "bio-seq") (r "^0.12") (d #t) (k 0)) (d (n "bio-seq") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 0)))) (h "0f0jmr3dgr3awb9bsgsbgymzv7dg572syaiv6s1fdqsqprlrl4lx")))

(define-public crate-bio-streams-0.3.1 (c (n "bio-streams") (v "0.3.1") (d (list (d (n "bio-seq") (r "^0.12") (d #t) (k 0)) (d (n "bio-seq") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 0)))) (h "1xq5w1hcryyq416y1igzh5yizj3singifpq2cf90yfvx9asb7ccd")))

