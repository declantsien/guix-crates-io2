(define-module (crates-io bi lr bilrost-derive) #:use-module (crates-io))

(define-public crate-bilrost-derive-0.0.1 (c (n "bilrost-derive") (v "0.0.1") (h "1f612jykyygyad6f2g96harjyq2nkc21p5d9wyc5vbyiqvkqnwc0")))

(define-public crate-bilrost-derive-0.1000.0 (c (n "bilrost-derive") (v "0.1000.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05ifn6pij9mgqgnaavfd3bvdx6m5pgaf4wk70d8fwj9wjg5wjag8") (r "1.65")))

(define-public crate-bilrost-derive-0.1001.0 (c (n "bilrost-derive") (v "0.1001.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wg258ikhk6d9nzpgy2ic5ila64cais9sy7c9kpgykmzmhmsdhh4") (r "1.65")))

(define-public crate-bilrost-derive-0.1002.0 (c (n "bilrost-derive") (v "0.1002.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lgfvr294pn9h71k8q7ak00br1k64xz0fb735qrfar19yi40na56") (r "1.65")))

(define-public crate-bilrost-derive-0.1002.1 (c (n "bilrost-derive") (v "0.1002.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "090vrk89xnli03gyrnw6mwaihl9mbksfmg8f84g9bc2mwmjr1dyf") (r "1.65")))

(define-public crate-bilrost-derive-0.1003.0 (c (n "bilrost-derive") (v "0.1003.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hb7mb4n17vqsi8gpwlqn3qm14iiajahxj27g6pvf24z9fnndqy7") (r "1.65")))

(define-public crate-bilrost-derive-0.1003.1 (c (n "bilrost-derive") (v "0.1003.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lyi2xw3c2i0lk47313504jy39sxz6sv5fsq2prs7fwycsic0waz") (r "1.65")))

(define-public crate-bilrost-derive-0.1004.0 (c (n "bilrost-derive") (v "0.1004.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1n6y5fbpjsbad5rkk6zc2wi8v1c0q8apnizx8dm8si530aqvz8nc") (r "1.65")))

(define-public crate-bilrost-derive-0.1005.0 (c (n "bilrost-derive") (v "0.1005.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04l68vpp48k9kzpapg0iiv1zhggbvywzip0fk30ymaq0w5kx9h89") (r "1.65")))

(define-public crate-bilrost-derive-0.1005.1 (c (n "bilrost-derive") (v "0.1005.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1iffrc2z63ym47ncyb9d7d6kcyzsx6839ky27lj1qkh5djng90md") (r "1.65")))

(define-public crate-bilrost-derive-0.1006.0 (c (n "bilrost-derive") (v "0.1006.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1n394q6g7af38l1h2dgimpzi0m50w546ay3d06b1avxws8p6ydmr") (r "1.65")))

(define-public crate-bilrost-derive-0.1006.1 (c (n "bilrost-derive") (v "0.1006.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0p3fv00d9g6gjlh3awjyhbryljgnyxg2i312fh24zzy93lgs43iz") (r "1.65")))

(define-public crate-bilrost-derive-0.1007.0 (c (n "bilrost-derive") (v "0.1007.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r ">=0.10, <0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0mz4nrlz3256w9ilm618cdm9b65rj7hwlaz4dm4biva55wxq87mj") (r "1.65")))

(define-public crate-bilrost-derive-0.1008.0 (c (n "bilrost-derive") (v "0.1008.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.13") (f (quote ("use_alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ndlgriz62ykd47yfmci367bljp20gpa7bggka6d87p9qcz99d34") (r "1.65")))

