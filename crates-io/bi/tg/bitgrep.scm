(define-module (crates-io bi tg bitgrep) #:use-module (crates-io))

(define-public crate-bitgrep-0.1.0 (c (n "bitgrep") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1jmjh1va3ylkv01yx93ygi8nbw5bsfcfb1qkk4nf29859mr7ddyn") (y #t)))

(define-public crate-bitgrep-0.1.1 (c (n "bitgrep") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0v9bywkni336bbp8533y4bn5iwsddgpg57mia2q4k995v7ahywfw") (y #t)))

(define-public crate-bitgrep-0.1.2 (c (n "bitgrep") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0jq3z81hdxic7k0pgh0wl56lfb88mwdni974kbqz1gz82snivldr") (y #t)))

(define-public crate-bitgrep-0.1.3 (c (n "bitgrep") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "19bjdgmjgi79nbw07br6bp4q5jiy3r7gl2vmk0808l257lbx06ja") (y #t)))

(define-public crate-bitgrep-0.1.4 (c (n "bitgrep") (v "0.1.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0x6mqk4qxzfjzx95ck3zn70hv8ra1rzvf73lw2xbvyb97m9cl43r") (y #t)))

(define-public crate-bitgrep-0.1.5 (c (n "bitgrep") (v "0.1.5") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "assertor") (r "^0.0.2") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0gkmznd7gh58b3bld8vjcrdz73a8fp1y7pm0h4iy35yydbrbkaa5")))

