(define-module (crates-io bi tg bitgesellcore-rpc) #:use-module (crates-io))

(define-public crate-bitgesellcore-rpc-0.1.0 (c (n "bitgesellcore-rpc") (v "0.1.0") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "bitcoincore-rpc-json") (r "^0.17.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ak489pak2b0rlry10wkr1i8cnm2d0iqqyblck6hrzqz45rf9lq9")))

(define-public crate-bitgesellcore-rpc-0.1.1 (c (n "bitgesellcore-rpc") (v "0.1.1") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "bitcoincore-rpc-json") (r "^0.17.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.14.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0mqnvihqjqjpvac1yf2pwnsvj76q23rhqiir10nm0idp080ppi6c")))

