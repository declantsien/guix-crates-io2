(define-module (crates-io bi tg bitgesellcore-rpc-json) #:use-module (crates-io))

(define-public crate-bitgesellcore-rpc-json-0.17.0 (c (n "bitgesellcore-rpc-json") (v "0.17.0") (d (list (d (n "bitcoin") (r "^0.30.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0i24gr7sx477rs4r4zhhmlfw8xmn8qlv635gw4g0af49r2nxp1p1")))

(define-public crate-bitgesellcore-rpc-json-0.17.1 (c (n "bitgesellcore-rpc-json") (v "0.17.1") (d (list (d (n "bitcoin") (r "^0.30.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kfblrzijdg9ggpp4ilh1lhvab4x48sdwxlgvb4fg02z8c2kxggq")))

