(define-module (crates-io bi pp bippy) #:use-module (crates-io))

(define-public crate-bippy-0.1.0 (c (n "bippy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "0yzpvibn7hj5si32im4mafkkp7skqbsxprgwxdyiyyj2xapw2mpf")))

(define-public crate-bippy-0.1.1 (c (n "bippy") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "15gi83mbvva8fza8d16f14jdfq7nxlidvl6h0kvgdpqc2ig79vn2")))

