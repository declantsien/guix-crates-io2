(define-module (crates-io bi te bitenum) #:use-module (crates-io))

(define-public crate-bitenum-0.1.0 (c (n "bitenum") (v "0.1.0") (h "1r1pf94ky9fkcgyvrgn42j31p11bxipxh01hx3mqhix9snjisgzk") (y #t)))

(define-public crate-bitenum-0.2.0 (c (n "bitenum") (v "0.2.0") (d (list (d (n "bitenum_macro") (r "^0.2.0") (d #t) (k 0)))) (h "0mwg9idr2al27578hk99ax39i0hbspw326ca8rhq0vld277igr83")))

(define-public crate-bitenum-0.2.1 (c (n "bitenum") (v "0.2.1") (d (list (d (n "bitenum_macro") (r "^0.2.0") (d #t) (k 0)))) (h "0cz9bh1qwfnd3lj3b01aafz2z5m82cqh6vqip74lxsbqgsayy15f")))

(define-public crate-bitenum-0.2.2 (c (n "bitenum") (v "0.2.2") (d (list (d (n "bitenum_macro") (r "^0.2.1") (d #t) (k 0)))) (h "0mamplvfp6dz036pzgga8yvcsppjy4bkxl09dfv754m0217jfjkb")))

