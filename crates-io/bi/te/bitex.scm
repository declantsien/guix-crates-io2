(define-module (crates-io bi te bitex) #:use-module (crates-io))

(define-public crate-bitex-0.1.1 (c (n "bitex") (v "0.1.1") (d (list (d (n "curs") (r "^0.1.3") (d #t) (k 0)) (d (n "http_stub") (r "^0.1.2") (d #t) (k 2)) (d (n "mime_guess") (r "= 1.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8.3") (d #t) (k 0)))) (h "0kqjxm0skgz67c5a3xq5bazayy19h96jwz4q7xmpvwbyr8c1mw8d")))

