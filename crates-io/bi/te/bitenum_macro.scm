(define-module (crates-io bi te bitenum_macro) #:use-module (crates-io))

(define-public crate-bitenum_macro-0.2.0 (c (n "bitenum_macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yihwv0j4h4nivcx0a9q5a8ry4fpwkhfkqi877659j5cfrbm9bh4")))

(define-public crate-bitenum_macro-0.2.1 (c (n "bitenum_macro") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j39kcnckrwcn2d4pwygssqf6cjhvy67ywc7kwzkf7c3drd6zik2")))

