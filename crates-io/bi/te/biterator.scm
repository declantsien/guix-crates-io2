(define-module (crates-io bi te biterator) #:use-module (crates-io))

(define-public crate-biterator-0.1.0 (c (n "biterator") (v "0.1.0") (h "12jkkynqhh7b9x91qgf492hy20msi3ip2bhzkhj7rxiy4p03w140")))

(define-public crate-biterator-0.2.0 (c (n "biterator") (v "0.2.0") (h "0h3kgldl2f69wsjjn162c2vdb5p6bxjlhsfz6422vnjxll0gvnwh")))

(define-public crate-biterator-0.3.0 (c (n "biterator") (v "0.3.0") (h "0pc66hmm47pkbbhgfhwyq2r8zpk28d5k9s2i0ws3wb84dv7qnai5")))

