(define-module (crates-io bi te bite) #:use-module (crates-io))

(define-public crate-bite-0.0.1 (c (n "bite") (v "0.0.1") (h "12frcj0xf6vk949z77vs960h0981dbfjr9csj0qa07x4jbxk2anh")))

(define-public crate-bite-0.0.2 (c (n "bite") (v "0.0.2") (h "0lp3ldddj70243chba01nvjqz6rgxqd8hv2saqy2d0hnl825k3dq")))

(define-public crate-bite-0.0.3 (c (n "bite") (v "0.0.3") (h "0cnffzk6v7d3flfabnd0is6y9djrpp7gxx0hj6wyyvckr5qyrx04")))

(define-public crate-bite-0.0.4 (c (n "bite") (v "0.0.4") (h "1gm0rqql1i6m8igcx7m352353jn0ixbzx44fxa94amwjqdrycqj1")))

(define-public crate-bite-0.0.5 (c (n "bite") (v "0.0.5") (h "1n323n63qnkm14finnypvcg0jp22bj02g0ngmwmzpgx117w1dfb7")))

