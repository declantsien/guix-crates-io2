(define-module (crates-io bi o_ bio_edit) #:use-module (crates-io))

(define-public crate-bio_edit-0.1.1 (c (n "bio_edit") (v "0.1.1") (d (list (d (n "bio-types") (r "^0.12") (d #t) (k 0)) (d (n "bit-set") (r "^0.5") (d #t) (k 0)))) (h "1fn0jv0rkiw42wvnvrhfp302vd3dypa3rfrzsvd1ksv1iivb1d6n")))

