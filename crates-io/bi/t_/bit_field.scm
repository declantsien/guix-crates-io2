(define-module (crates-io bi t_ bit_field) #:use-module (crates-io))

(define-public crate-bit_field-0.1.0 (c (n "bit_field") (v "0.1.0") (h "1df6hz5irscav4r3hv3nx9mkj0xabnkchvlsx66143cvw6xfb21z")))

(define-public crate-bit_field-0.2.0 (c (n "bit_field") (v "0.2.0") (h "0rz0ixhxqpl6s7dhqzp85iblmfh2l6508qmdrg36r7g6f5f0dsjs")))

(define-public crate-bit_field-0.2.1 (c (n "bit_field") (v "0.2.1") (h "1pbjkscyylp5wsrw1vysawr2fra27w2yn1b35wj470876i7bc3l8")))

(define-public crate-bit_field-0.4.0 (c (n "bit_field") (v "0.4.0") (h "1xk161c93r5mlpcpp7wrlwrpv6xcz0im5cw3sgw7n8k4q0k2czwc")))

(define-public crate-bit_field-0.5.0 (c (n "bit_field") (v "0.5.0") (h "1fyyc3xg8xr03xi2xpsnshh5xbgpkiz1g95qhv4pkid0sfb555cw")))

(define-public crate-bit_field-0.6.0 (c (n "bit_field") (v "0.6.0") (h "1yigf79qx1xxj7z7bxdcmgfwa1y7lw9w4sc4mvxg116nfd209k9q")))

(define-public crate-bit_field-0.6.1 (c (n "bit_field") (v "0.6.1") (h "0p9ga9pc6d8w3kckms0h1fk5hx5164ks6dv9v027h1givg10h9ig")))

(define-public crate-bit_field-0.7.0 (c (n "bit_field") (v "0.7.0") (h "1ba9d0kfjl757rgjjj095bcq02azmg4z481rcjzm7g712i0ad4gz")))

(define-public crate-bit_field-0.8.0 (c (n "bit_field") (v "0.8.0") (h "0jg6327ck6vccwf2rrqwg1dcks20a4rn6dn5ji7r4fyg7lf9i0jz")))

(define-public crate-bit_field-0.9.0 (c (n "bit_field") (v "0.9.0") (h "0mjxkfcz2biq469iwsrj7jrj1cr54qrpssxbfiwn22chky86b1zd")))

(define-public crate-bit_field-0.9.1 (c (n "bit_field") (v "0.9.1") (h "108qfwjrykz85v8ahvi4z38vrrsxdj2nfbmw4r37j8fg7d3a7lxf") (y #t)))

(define-public crate-bit_field-0.10.0 (c (n "bit_field") (v "0.10.0") (h "1h0x72mv5c6xryc916fdyqqvvc0ykdpgna1smka42iq8rw3dcrd1")))

(define-public crate-bit_field-0.10.1 (c (n "bit_field") (v "0.10.1") (h "192rsg8g3ki85gj8rzslblnwr53yw5q4l8vfg6bf1lkn4cfdvdnw")))

(define-public crate-bit_field-0.10.2 (c (n "bit_field") (v "0.10.2") (h "0qav5rpm4hqc33vmf4vc4r0mh51yjx5vmd9zhih26n9yjs3730nw")))

