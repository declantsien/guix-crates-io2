(define-module (crates-io bi t_ bit_mutator) #:use-module (crates-io))

(define-public crate-bit_mutator-0.1.0 (c (n "bit_mutator") (v "0.1.0") (h "13cjyascvdra0rqs7kfigl7356kh80wwifalw1k8pqirn3j9z8yr") (y #t)))

(define-public crate-bit_mutator-0.1.1 (c (n "bit_mutator") (v "0.1.1") (h "1qncxlamyjkgcs0vi0667pv819z4r3nbmd0dmzrgbfy5scdgckcq")))

