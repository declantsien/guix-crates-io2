(define-module (crates-io bi t_ bit_mask_ring_buf) #:use-module (crates-io))

(define-public crate-bit_mask_ring_buf-0.1.0 (c (n "bit_mask_ring_buf") (v "0.1.0") (h "0shpzim6bai2c19aa4smg3vjzglg2gigbjq43f21nchr25b8p4k8")))

(define-public crate-bit_mask_ring_buf-0.1.1 (c (n "bit_mask_ring_buf") (v "0.1.1") (h "1x9dlswxxj59xg4dnaijzw4z8clh18vq498isgy2w351x4gbg0mv")))

(define-public crate-bit_mask_ring_buf-0.1.2 (c (n "bit_mask_ring_buf") (v "0.1.2") (h "0xpg0mi0kmid66hhcqqpch0dissyjw9dn99xd8drwgwbwdjx4v9m")))

(define-public crate-bit_mask_ring_buf-0.1.3 (c (n "bit_mask_ring_buf") (v "0.1.3") (h "14mi6fkv02rcakw0ghs3nyap1jcffhvdifryj6p2ivfyarp22v7s")))

(define-public crate-bit_mask_ring_buf-0.1.4 (c (n "bit_mask_ring_buf") (v "0.1.4") (h "14arjhd27mcnar3bjfvnzn1ah7idc78cvwrw6l8niagcd674sdy2")))

(define-public crate-bit_mask_ring_buf-0.1.5 (c (n "bit_mask_ring_buf") (v "0.1.5") (h "1x79ibq5calsq78xgz38wiyv3kh6aqbal3b9hfhx0w68bqiqllw5")))

(define-public crate-bit_mask_ring_buf-0.1.6 (c (n "bit_mask_ring_buf") (v "0.1.6") (h "0zvai5039w191r3rf71grrqk7k7gy0ymx0nddgbs55kv61lbm4lx")))

(define-public crate-bit_mask_ring_buf-0.2.0 (c (n "bit_mask_ring_buf") (v "0.2.0") (h "133h47m07cjjkjdaiqfqnm9wsvrvd95skcxlpjndpyinqjq45a7b")))

(define-public crate-bit_mask_ring_buf-0.2.1 (c (n "bit_mask_ring_buf") (v "0.2.1") (h "10hy0hbpniwx89k4lyzaa6hcvzz0kmzrzwywdm1ysy7n14acx84v")))

(define-public crate-bit_mask_ring_buf-0.2.2 (c (n "bit_mask_ring_buf") (v "0.2.2") (h "1wb1w80fsg7s1s11a7zxvsrj2kmzncs824xhgwljlhx97rx464nz")))

(define-public crate-bit_mask_ring_buf-0.2.3 (c (n "bit_mask_ring_buf") (v "0.2.3") (h "0qbw71371zpgkwhd4q3zj5xf42vwx163b8s3q920cz4v80myh177")))

(define-public crate-bit_mask_ring_buf-0.2.4 (c (n "bit_mask_ring_buf") (v "0.2.4") (h "161zrj30wb075mszidrirsn5i0xm45nh3yfhq5i3gfik81wwssg6")))

(define-public crate-bit_mask_ring_buf-0.2.5 (c (n "bit_mask_ring_buf") (v "0.2.5") (h "11a9wamp673hjw7iybhprxyn4a910c5qg2fn9nwsmfzl2fqb83b9")))

(define-public crate-bit_mask_ring_buf-0.2.6 (c (n "bit_mask_ring_buf") (v "0.2.6") (h "0p6z2xyw8xfalcn1myj8yrghhqax7khb07bsvv31bjp2lhrmfm3h")))

(define-public crate-bit_mask_ring_buf-0.2.7 (c (n "bit_mask_ring_buf") (v "0.2.7") (h "1hyiabci68sf5xrm26ck9s6blbjfi191082bl135144wxykvwp5b")))

(define-public crate-bit_mask_ring_buf-0.3.0 (c (n "bit_mask_ring_buf") (v "0.3.0") (h "1zara8rxlr4ncnkwz99xi0dymcphkqc2iabwn2f2yaciib54wslm")))

(define-public crate-bit_mask_ring_buf-0.3.1 (c (n "bit_mask_ring_buf") (v "0.3.1") (h "0rkrryalzgnlijl2d11p7v1j5xg86qbnz633w9xy8pk0bairx6bq")))

(define-public crate-bit_mask_ring_buf-0.4.0 (c (n "bit_mask_ring_buf") (v "0.4.0") (h "15409zbgdjrfy7r2nxv36i99w1752mcgwd5bapqafikwc1z8661x")))

(define-public crate-bit_mask_ring_buf-0.4.1 (c (n "bit_mask_ring_buf") (v "0.4.1") (h "0lz6hjbdkp17aqgynp3clah5c5y8lmys15vpciy6bjkhn3g9n9bq")))

(define-public crate-bit_mask_ring_buf-0.4.2 (c (n "bit_mask_ring_buf") (v "0.4.2") (h "1739nfqd5r5qzpbyavjcz7fq7dkld054bsjzjs37xdri1rfw3zvk")))

(define-public crate-bit_mask_ring_buf-0.4.3 (c (n "bit_mask_ring_buf") (v "0.4.3") (h "1b58znmqrvqmrfmhcdq9a2ldjr3fj12kn8z7rnrk3cxfl9ga342q")))

(define-public crate-bit_mask_ring_buf-0.4.4 (c (n "bit_mask_ring_buf") (v "0.4.4") (h "02kg4vpp5abyfqdlh0l50graqw88ic8z9fya8bq6ld619pxn07q6")))

(define-public crate-bit_mask_ring_buf-0.4.5 (c (n "bit_mask_ring_buf") (v "0.4.5") (h "0s52aa39lk71134d67ivlh4fjqvspa5a002b1y5j1f4ifm2r5zyj")))

(define-public crate-bit_mask_ring_buf-0.4.6 (c (n "bit_mask_ring_buf") (v "0.4.6") (h "0aah9i8i0j56zpw5ai1gsapgwq9mv883ib4rc02ai01f8fc7bbr7")))

(define-public crate-bit_mask_ring_buf-0.5.0 (c (n "bit_mask_ring_buf") (v "0.5.0") (h "0yqv7562k9c9id8ab38bdm8j3110pgnml985kslihnjldr0wn3k1")))

(define-public crate-bit_mask_ring_buf-0.5.1 (c (n "bit_mask_ring_buf") (v "0.5.1") (h "1v9rvwndc2acbsgsi6p9j0yz749grngszzaz5wz9sblgz2356gxl")))

(define-public crate-bit_mask_ring_buf-0.5.2 (c (n "bit_mask_ring_buf") (v "0.5.2") (h "02zbh4k4qqiv3hnrlk1sparch38iw41brqljypc3a0x71hn34kc1")))

(define-public crate-bit_mask_ring_buf-0.5.3 (c (n "bit_mask_ring_buf") (v "0.5.3") (h "0rfkm2sqc11nl29vkhc39yfb6ijavx2w4v5w213672sjmpk59vmk")))

