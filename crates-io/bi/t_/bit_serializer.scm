(define-module (crates-io bi t_ bit_serializer) #:use-module (crates-io))

(define-public crate-bit_serializer-0.0.1 (c (n "bit_serializer") (v "0.0.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "10xzzpwvc19h1bwcw1j27kkxx4wj41yb5chg5sj5nglpqrxnjkch")))

