(define-module (crates-io bi t_ bit_roles_macros) #:use-module (crates-io))

(define-public crate-bit_roles_macros-0.1.0 (c (n "bit_roles_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "17ikgnivh6rj6l1sbg61cb3xjrdlc3x87lqbnxppr7c4f7jhc19i")))

(define-public crate-bit_roles_macros-0.2.0 (c (n "bit_roles_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1b9bi78igbkm9h9lcjs2769kdbfbgwndfjknskkgh6q6i7pkmx69")))

