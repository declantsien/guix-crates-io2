(define-module (crates-io bi t_ bit_manager) #:use-module (crates-io))

(define-public crate-bit_manager-0.1.0 (c (n "bit_manager") (v "0.1.0") (h "1yf5l3ivpv3n2spa9mfdqfknk1k9pywb9bq25ays84dlrgh6dq2k")))

(define-public crate-bit_manager-0.1.1 (c (n "bit_manager") (v "0.1.1") (h "05axmpxqdyh0i3llq0zjnkwklfnngfhca1wmzld2jq1g1d3c669l")))

(define-public crate-bit_manager-0.2.0 (c (n "bit_manager") (v "0.2.0") (h "1x7hkx8qszqz332a84h3gpcaf39wckl00f3pr6xv4181lpiqawrv")))

(define-public crate-bit_manager-0.3.0 (c (n "bit_manager") (v "0.3.0") (h "0g1xp59q0hrx0gra9pw6fsyfs7dynbvm94j443ckzq3smzgaskkm")))

(define-public crate-bit_manager-0.4.0 (c (n "bit_manager") (v "0.4.0") (h "1dijhmxhbsgx9yc06zmdn07varcvjda18v7gs9ph2rrls7l70q0v")))

(define-public crate-bit_manager-0.5.0 (c (n "bit_manager") (v "0.5.0") (h "02dh1aphkygp0f6n8bfrlfp3rcc7fh8rzqjxikkl3qcxqwpj1hsx")))

(define-public crate-bit_manager-0.5.1 (c (n "bit_manager") (v "0.5.1") (h "1xvfw6v3qw76v7hiygj55gr1s5lrwwsjzbh5f18mfgldl19r2hgs")))

(define-public crate-bit_manager-0.5.2 (c (n "bit_manager") (v "0.5.2") (h "0p048hh438f7mbs10dnlqaknba8xvj5lnc30fffp3h759x3ic746")))

(define-public crate-bit_manager-0.5.3 (c (n "bit_manager") (v "0.5.3") (h "0ncvvf6lfi8lr04i64shv4id4a9hks69h8zjr5zhdly71dpkzh0z")))

