(define-module (crates-io bi t_ bit_collection_derive) #:use-module (crates-io))

(define-public crate-bit_collection_derive-0.1.0 (c (n "bit_collection_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1iv1w7nfsd2d0j2drw4pzj8p6bm7zv423ybz2j71l9rii9rnp7dx")))

(define-public crate-bit_collection_derive-0.1.1 (c (n "bit_collection_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "10d24ic6asqif5f4vss543w8pgjgvgy35gc8jlpxzcqgy6ilqbng") (f (quote (("std") ("default" "std"))))))

(define-public crate-bit_collection_derive-0.2.0 (c (n "bit_collection_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1a59hwzhj1haqb0pdqp8bnk9w4yjpci04qk1b5sq4czsrnsd83sj") (f (quote (("std") ("default" "std"))))))

(define-public crate-bit_collection_derive-0.2.1 (c (n "bit_collection_derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0xp0n3d8bjf53957m3jkcf20adfbhv1gsrg20320zhf8kg16h6q4") (f (quote (("std") ("default" "std"))))))

(define-public crate-bit_collection_derive-0.2.2 (c (n "bit_collection_derive") (v "0.2.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1spjfxwxair9j6b3qkv2j2mi685625z76xin3fbhf6wysqd816vr") (f (quote (("std") ("default" "std"))))))

