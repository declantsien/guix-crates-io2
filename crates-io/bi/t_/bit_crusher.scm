(define-module (crates-io bi t_ bit_crusher) #:use-module (crates-io))

(define-public crate-bit_crusher-0.0.1 (c (n "bit_crusher") (v "0.0.1") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)))) (h "0c784mzj6lhgbzya6fw80iw424mr60a66j5mkfhi01vil0inz13y")))

(define-public crate-bit_crusher-0.0.2 (c (n "bit_crusher") (v "0.0.2") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)))) (h "0j7mvnjm3vg03wp1dzvmjbx2rj2mkm9ig4xbmckk98xg8p547aj5")))

(define-public crate-bit_crusher-0.0.3 (c (n "bit_crusher") (v "0.0.3") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)))) (h "190n17ds8w5bn6z0bxvq77n4hib19mviavl8vl736i09xjblzvs5")))

(define-public crate-bit_crusher-0.1.0 (c (n "bit_crusher") (v "0.1.0") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)))) (h "1cc1pln170bgwsj17qa5lclngm379hm5czkcvy47hk55bin9s5gj")))

(define-public crate-bit_crusher-0.1.1 (c (n "bit_crusher") (v "0.1.1") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 2)))) (h "0mvy7yw167k1l3s575ixccchsh2bidsg26qwsnm0s213zby71x9b")))

(define-public crate-bit_crusher-0.1.2 (c (n "bit_crusher") (v "0.1.2") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 2)))) (h "0zb4ak7khv0rna8p9n6gma09jykvd17cvg561d9ifk5yi3v2vlr1")))

(define-public crate-bit_crusher-0.2.0 (c (n "bit_crusher") (v "0.2.0") (d (list (d (n "dsp-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 2)))) (h "17j4ymrsq3ch3s7n7prmlj517x9npy1f6p84nlzms65jw5lsfpm6")))

