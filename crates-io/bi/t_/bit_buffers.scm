(define-module (crates-io bi t_ bit_buffers) #:use-module (crates-io))

(define-public crate-bit_buffers-0.1.0 (c (n "bit_buffers") (v "0.1.0") (h "1lzlh4ghip5syk4ikyyzj4xy67kbph4h943nf5bp1nsaqwwxhb56")))

(define-public crate-bit_buffers-0.1.1 (c (n "bit_buffers") (v "0.1.1") (h "1lm6dj7c4avghzscrcjixm61z6hain7r4ib4fh74xmqxn06wnin8")))

(define-public crate-bit_buffers-0.1.2 (c (n "bit_buffers") (v "0.1.2") (h "0j9i1ikj6xa98nshybi9mx9bc5v3a42d7d4bw6vzhzvpxc6lr34p")))

