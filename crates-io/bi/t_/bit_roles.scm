(define-module (crates-io bi t_ bit_roles) #:use-module (crates-io))

(define-public crate-bit_roles-0.1.0 (c (n "bit_roles") (v "0.1.0") (d (list (d (n "bit_roles_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0rhcdizg2sxxjzlqx62hsw8cwxqj095wd3vls744a783dclx7fqd")))

(define-public crate-bit_roles-0.2.0 (c (n "bit_roles") (v "0.2.0") (d (list (d (n "bit_roles_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0d9b1jfiiqv8nfffw70cipm5rcq229swi6cs8ifzfvwv9hskgs1w")))

