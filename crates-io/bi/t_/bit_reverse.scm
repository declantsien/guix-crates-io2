(define-module (crates-io bi t_ bit_reverse) #:use-module (crates-io))

(define-public crate-bit_reverse-0.1.0 (c (n "bit_reverse") (v "0.1.0") (h "0zydkicj909viza71yvvvx9maw1mi5drn5lvzi5gxfwhfrd4k54f")))

(define-public crate-bit_reverse-0.1.1 (c (n "bit_reverse") (v "0.1.1") (h "0rqyysai9f2iv51q2xb9lv60i8p4g0gvnx8baqkr8qabhax8d2yp")))

(define-public crate-bit_reverse-0.1.2 (c (n "bit_reverse") (v "0.1.2") (h "16g02b7lp3s1pl2740kc6r0gnxnc2ivx400xsaiyi47vv8y6abb1")))

(define-public crate-bit_reverse-0.1.3 (c (n "bit_reverse") (v "0.1.3") (h "1ll826byi7jah2f66jxn2q7s72ywwzc2sj4nqcw5j317z64phlwq")))

(define-public crate-bit_reverse-0.1.4 (c (n "bit_reverse") (v "0.1.4") (h "0kd6prg2vvrypja601al0rjw7shmshc6m7k9kj01j4gp1vbisfwb") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bit_reverse-0.1.5 (c (n "bit_reverse") (v "0.1.5") (h "1plns8k071kg510xzkm0c821wb5d8fjbwp302k5h0c5wqp0p88sw") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bit_reverse-0.1.6 (c (n "bit_reverse") (v "0.1.6") (h "1yjk4js4bvlpsdhpdg8lshpjcg55a6d1f8y0v40bhmmjiddv9s4n") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bit_reverse-0.1.7 (c (n "bit_reverse") (v "0.1.7") (h "135282g1prcxd7nzdgksnji6qad8blfh7mpkfw1rr2d2nlny15sy") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bit_reverse-0.1.8 (c (n "bit_reverse") (v "0.1.8") (h "15kj151hy2s7zq4jj3zw80hccmxjfwqprgq6w73rb55v1aiqqllr") (f (quote (("use_std") ("default" "use_std"))))))

