(define-module (crates-io bi t_ bit_manager_derive) #:use-module (crates-io))

(define-public crate-bit_manager_derive-0.1.0 (c (n "bit_manager_derive") (v "0.1.0") (d (list (d (n "bit_manager") (r "^0.5.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0gzxir29bxpk51dk2nb91vxhxd4cmgvpks174197z0g3zyl193bb")))

(define-public crate-bit_manager_derive-0.1.1 (c (n "bit_manager_derive") (v "0.1.1") (d (list (d (n "bit_manager") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "02ds9blr4dsi2mh2cln0z8iakvwdci3ipg1ycgp1mi3dpg0ny6yq")))

