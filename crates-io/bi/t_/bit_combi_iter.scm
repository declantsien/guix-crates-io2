(define-module (crates-io bi t_ bit_combi_iter) #:use-module (crates-io))

(define-public crate-bit_combi_iter-0.1.0 (c (n "bit_combi_iter") (v "0.1.0") (h "0rklv0zcp7ynban1c2rrjgczvckavqx8h2zq1n48k1d553076yqj")))

(define-public crate-bit_combi_iter-1.0.0 (c (n "bit_combi_iter") (v "1.0.0") (h "0xj1shnxjnvx1wiggahr3f0xy4056p6nkxha9ymj525jcnnng1i7")))

(define-public crate-bit_combi_iter-1.0.1 (c (n "bit_combi_iter") (v "1.0.1") (h "0qgpk8y61i21lxpgkxm5nxfipa8adnsvdaddf439ki67z2jjzpm7")))

(define-public crate-bit_combi_iter-1.0.2 (c (n "bit_combi_iter") (v "1.0.2") (h "1ybpl1p9qshr3sy69r2rayqszjxcgbljh1rqwlw0p6xxg6hfzj35")))

