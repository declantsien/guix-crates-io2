(define-module (crates-io bi t_ bit_op) #:use-module (crates-io))

(define-public crate-bit_op-0.1.0 (c (n "bit_op") (v "0.1.0") (h "1iz4xm1gv08ml0dsvzmh9c1k2mcvmpvqrjykqd9y97x3m9j9f9vg")))

(define-public crate-bit_op-0.1.1 (c (n "bit_op") (v "0.1.1") (h "0g5x1qh8yif3521p5q6f6il22z5pi1pn2ybz0pls3sxcbj2ip435")))

