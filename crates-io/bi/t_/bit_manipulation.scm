(define-module (crates-io bi t_ bit_manipulation) #:use-module (crates-io))

(define-public crate-bit_manipulation-0.1.0 (c (n "bit_manipulation") (v "0.1.0") (h "18q6wjgi8l1cab1vpsrd2g8651cw6j9scyfryk18f1h01p1zs466")))

(define-public crate-bit_manipulation-0.1.1 (c (n "bit_manipulation") (v "0.1.1") (h "1fj30cp2vcimwdh46lnm95wf1gkwd2h1h7y2bprmizl7l4rdqll8")))

(define-public crate-bit_manipulation-0.1.2 (c (n "bit_manipulation") (v "0.1.2") (h "1r57h5shr2hyxqlx6nknpzld6zpy2jj5micxxqsk4wrka64pc687")))

(define-public crate-bit_manipulation-0.2.0 (c (n "bit_manipulation") (v "0.2.0") (h "14mz9fip8gyhmfymbxizzpcaxjgjjl4iii6891fd852alqscrvh6")))

(define-public crate-bit_manipulation-0.2.1 (c (n "bit_manipulation") (v "0.2.1") (h "04i9hvjx0mb5rvggby0plpsx94a1kfhq02ay244xbhnxify5crp1")))

(define-public crate-bit_manipulation-0.2.15 (c (n "bit_manipulation") (v "0.2.15") (h "1b2ik8jr2hpssbr49zhryycfs82khcmiyv6yn1zlrisxzd8y725a")))

(define-public crate-bit_manipulation-0.2.2 (c (n "bit_manipulation") (v "0.2.2") (h "0gp85il3vnr88vh0kx0km69azj9mciw1dmn5gi46l0vzzy0fy65y")))

(define-public crate-bit_manipulation-0.2.25 (c (n "bit_manipulation") (v "0.2.25") (h "070wqz83ni2d7gqydy7k9a5ak285x0yp4ynfcvi604gl7f1qrnbn")))

(define-public crate-bit_manipulation-0.2.3 (c (n "bit_manipulation") (v "0.2.3") (h "1ki9sickybkgqzjbahrrhb21jag7ap0l72qaca34fkxs0w2frw2g")))

(define-public crate-bit_manipulation-0.2.35 (c (n "bit_manipulation") (v "0.2.35") (h "07qfnwj1f5cz8yiyqyb1vxxbaxf0ky0mnxgd3smsyc0nzz4k9q1a")))

(define-public crate-bit_manipulation-0.2.4 (c (n "bit_manipulation") (v "0.2.4") (h "0nibg6pdipzcqs3skh6wqk3nf4vnqidrrb03wr362nai136r22ra")))

(define-public crate-bit_manipulation-0.2.40 (c (n "bit_manipulation") (v "0.2.40") (h "0f6vi16anja91zhj40b3zgpx0c32bkyfwj7xfrjdwpxjjzi04gml")))

