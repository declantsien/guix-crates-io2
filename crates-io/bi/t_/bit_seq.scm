(define-module (crates-io bi t_ bit_seq) #:use-module (crates-io))

(define-public crate-bit_seq-0.1.0 (c (n "bit_seq") (v "0.1.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1g014i5mamhrd69w4lfmbfdqsx0df4pr4ixb5k58vajpliabil6x")))

(define-public crate-bit_seq-0.1.1 (c (n "bit_seq") (v "0.1.1") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "1c0dw1jsp0bzzpya6pb779s8gnjpimpczwgd5kyfjcvakahgcymk")))

(define-public crate-bit_seq-0.1.2 (c (n "bit_seq") (v "0.1.2") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0wdc9j41jdkyg4005zbqvr85kzbhcbc5n7kv5rl0a4vyjsas4ac1")))

(define-public crate-bit_seq-0.2.0 (c (n "bit_seq") (v "0.2.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "10khqy6d4z7n2vvzky19li5fp1xc30zwhx8d461q2pp55vz3872y")))

(define-public crate-bit_seq-0.2.1 (c (n "bit_seq") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0zczkcap0mmx2pdzw87as9b4w6n4dcifgdp014574v8w1qz8harx")))

