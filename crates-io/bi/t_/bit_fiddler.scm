(define-module (crates-io bi t_ bit_fiddler) #:use-module (crates-io))

(define-public crate-bit_fiddler-0.1.0 (c (n "bit_fiddler") (v "0.1.0") (h "16x81s5x64f5ahkj2wig265kpbzfc7dzws8cv9k0syki3bv6lmgi")))

(define-public crate-bit_fiddler-1.0.0 (c (n "bit_fiddler") (v "1.0.0") (h "17vdkqk8asanj4y1fbcp447qxbn06kfnvhi6kdxg9il6bgg9qz9i")))

(define-public crate-bit_fiddler-2.0.0 (c (n "bit_fiddler") (v "2.0.0") (h "11ckj4i2fbh3snvyhmky16pa9zhk71ls95yckcwamig899fi3s7i")))

(define-public crate-bit_fiddler-2.1.0 (c (n "bit_fiddler") (v "2.1.0") (h "04jw89z18cra6gcsichpwvfj6npxccsk0kp5z7wiv3s94g90zfl5")))

(define-public crate-bit_fiddler-2.1.1 (c (n "bit_fiddler") (v "2.1.1") (h "077kqzdnlby2kiwi0593gs2isw3c1vj13w7x3shpmqwjc3nnd9n3")))

