(define-module (crates-io bi t_ bit_blend) #:use-module (crates-io))

(define-public crate-bit_blend-1.0.0 (c (n "bit_blend") (v "1.0.0") (h "1nn7q23cc5fgrx6jflj2wbzxbpg9lm31ajdcnn7nh3gvbwr74ygg")))

(define-public crate-bit_blend-1.1.0 (c (n "bit_blend") (v "1.1.0") (h "1mfrn0sm8q9g8ax495kd5gwwz8cwls89rxkwhpl4y3wn7r3742ic")))

