(define-module (crates-io bi t_ bit_utils) #:use-module (crates-io))

(define-public crate-bit_utils-0.0.0 (c (n "bit_utils") (v "0.0.0") (h "0kb9m17glfzk0gbwiibnrci3h8rbr9hf1f1rm574j8q1p3k7pd6i")))

(define-public crate-bit_utils-0.1.0 (c (n "bit_utils") (v "0.1.0") (h "1qlsyhhm2jkda5mgagsfh9fi8q42cvf5bs315f2mvkx090dc0sgn")))

(define-public crate-bit_utils-0.1.1 (c (n "bit_utils") (v "0.1.1") (h "0vf25jgilhydhhfrfzvm4x49bkri65m9gs9xy3d4njm08p54s4gq")))

