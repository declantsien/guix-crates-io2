(define-module (crates-io bi dj bidjei-shell) #:use-module (crates-io))

(define-public crate-bidjei-shell-0.1.0 (c (n "bidjei-shell") (v "0.1.0") (h "1y8h833acf3km5yckl441rghc9asvn1vq70v5fy0gs6gaqh6ad1k")))

(define-public crate-bidjei-shell-0.1.1 (c (n "bidjei-shell") (v "0.1.1") (h "1al4sh99s8z8yprj837s2f2l6z7aiz4spm5sawgi08zznhvxa2fs")))

