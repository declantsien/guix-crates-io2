(define-module (crates-io bi bu bibutils-sys) #:use-module (crates-io))

(define-public crate-bibutils-sys-0.1.0 (c (n "bibutils-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "05f5x5rn70vcwi6p05qcxfv68rl8b8hd1zd22s2dranl9ma9d7l1") (l "bibutils")))

(define-public crate-bibutils-sys-0.1.1 (c (n "bibutils-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0p2gzcxa96j2vmiw57zhnd4r9di13kzzwaa7gyw1llj7n7gph4bh") (l "bibutils")))

