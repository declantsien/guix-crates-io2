(define-module (crates-io bi rd bird_tool_utils) #:use-module (crates-io))

(define-public crate-bird_tool_utils-0.1.0 (c (n "bird_tool_utils") (v "0.1.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.10") (d #t) (k 0)))) (h "08b0i967r77wj4c8mkfbazx2bmiz28py4850f3i46fgyzfw5a7ry")))

(define-public crate-bird_tool_utils-0.2.0 (c (n "bird_tool_utils") (v "0.2.0") (d (list (d (n "bird_tool_utils-man") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.10") (d #t) (k 0)))) (h "18s4v029cg95gigc0ssk5wxmdfxh2322x2ywisvms3yz8ydxcg5p")))

(define-public crate-bird_tool_utils-0.3.0 (c (n "bird_tool_utils") (v "0.3.0") (d (list (d (n "bird_tool_utils-man") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "env_logger") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.10") (d #t) (k 0)))) (h "1m2grlcnx929vag7w7qh5ng42kr14azvsh2l5gdan8vyx6lhbxy6")))

(define-public crate-bird_tool_utils-0.4.0 (c (n "bird_tool_utils") (v "0.4.0") (d (list (d (n "bird_tool_utils-man") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "env_logger") (r "0.10.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)) (d (n "version-compare") (r "0.1.*") (d #t) (k 0)))) (h "0hgx53m0hi03c7rwirzfw02njm32qyd1iw98c6x4qfa63nxsclbd")))

(define-public crate-bird_tool_utils-0.4.1 (c (n "bird_tool_utils") (v "0.4.1") (d (list (d (n "bird_tool_utils-man") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "env_logger") (r "0.10.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)) (d (n "version-compare") (r "0.1.*") (d #t) (k 0)))) (h "0mnzldx1garjkc74b9x552la7n8rsvx0k3aanqhk94gg49d4fpvg")))

(define-public crate-bird_tool_utils-0.4.2 (c (n "bird_tool_utils") (v "0.4.2") (d (list (d (n "bird_tool_utils-man") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "env_logger") (r "0.10.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)) (d (n "version-compare") (r "0.1.*") (d #t) (k 0)))) (h "0hbhf0bxhs8k7hqmid0syrrvsidn54brgsn3n9lqyy9k8xhl7zdp") (y #t)))

(define-public crate-bird_tool_utils-0.5.0 (c (n "bird_tool_utils") (v "0.5.0") (d (list (d (n "bird_tool_utils-man") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "env_logger") (r "0.10.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "tempfile") (r "3.*") (d #t) (k 0)) (d (n "version-compare") (r "0.1.*") (d #t) (k 0)))) (h "0s0vnmk1d5jvrr69bn07k24641yb22zk4p8m43mk1hf1x1an4v4c")))

