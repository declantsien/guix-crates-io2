(define-module (crates-io bi rd birdc) #:use-module (crates-io))

(define-public crate-birdc-0.1.0 (c (n "birdc") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.17") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "net" "rt" "sync"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "0llqxi5vp8dgpksl8g6krz8b18jz94bk2g3jm9fcnrc8f3h8s1bx")))

(define-public crate-birdc-0.2.0 (c (n "birdc") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.17") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "net" "rt" "sync"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "1bggy71hj082xn4h6hdy5qm0i7wsr2wlysswvsr4jz93nnqpimhi")))

(define-public crate-birdc-0.3.0 (c (n "birdc") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.17") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "net" "rt" "sync"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "0n72b0wk7aj42zxpwkqi68gv2xcp0akfx6jqm10iff7yz6bihkmc")))

