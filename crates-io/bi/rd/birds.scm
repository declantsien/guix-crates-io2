(define-module (crates-io bi rd birds) #:use-module (crates-io))

(define-public crate-birds-0.1.0 (c (n "birds") (v "0.1.0") (h "0izlhqm6l04xwfn008r0x1pbmsb1wdahwy1x4lsr5bgfj8xx8gwj")))

(define-public crate-birds-0.2.0 (c (n "birds") (v "0.2.0") (h "0m74bp4nd20dlka0difcb9i9jw6qwng6x1mlx3li499vh7amk2bv")))

