(define-module (crates-io bi rd bird-machine-macros) #:use-module (crates-io))

(define-public crate-bird-machine-macros-0.0.1 (c (n "bird-machine-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gw4mpps0jq4p66x1ymqplb6zx7wbkgpdhp6cg9pd6msy0wydhr6")))

