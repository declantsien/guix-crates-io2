(define-module (crates-io bi rd bird-chat) #:use-module (crates-io))

(define-public crate-bird-chat-0.1.0 (c (n "bird-chat") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.80") (d #t) (k 0)))) (h "0wvfhill1xrp9fyq8c1djb28rsvc9r9jjrvd6vpw7lscjw42q7za")))

(define-public crate-bird-chat-0.1.3 (c (n "bird-chat") (v "0.1.3") (d (list (d (n "either") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.80") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "17y5sy6xq93762d2bsgr4hzkqybm1ci21acrrw3mv38nbhkr5nhn")))

