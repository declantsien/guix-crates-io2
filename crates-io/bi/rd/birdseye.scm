(define-module (crates-io bi rd birdseye) #:use-module (crates-io))

(define-public crate-birdseye-0.3.6 (c (n "birdseye") (v "0.3.6") (d (list (d (n "bytesize") (r "^1.0.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "diskspace_insight") (r "^0.1.7") (d #t) (k 0)) (d (n "eframe") (r "^0.13.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0r87r99cfgkdhvgyz1msb13lj6g4c90msvd2f75h4rr6d47bd4hq")))

(define-public crate-birdseye-0.3.9 (c (n "birdseye") (v "0.3.9") (d (list (d (n "bytesize") (r "^1.0.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "diskspace_insight") (r "^0.1.7") (d #t) (k 0)) (d (n "eframe") (r "^0.13.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "07a6iii0srmi901jvl3cpk2pmycc40hgz12jph7sp0znmzwygf18")))

