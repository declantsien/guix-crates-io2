(define-module (crates-io bi rd bird-protocol-derive) #:use-module (crates-io))

(define-public crate-bird-protocol-derive-0.1.0 (c (n "bird-protocol-derive") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "1kmfcjm6iqxs58icd9gsxb65hm8z0rlj69d8380kh7qdlawjg2s8")))

(define-public crate-bird-protocol-derive-0.1.1 (c (n "bird-protocol-derive") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0darl0wiy7dvdpskk16nhfihji2aqfh1ip6v23hp09g76br48shw")))

(define-public crate-bird-protocol-derive-0.1.2 (c (n "bird-protocol-derive") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "06lgsj817v8bb216vhml00m8ijlaw8ffxdmly7w79c8xphfcx329")))

(define-public crate-bird-protocol-derive-0.1.3 (c (n "bird-protocol-derive") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0md20b7jncsk93zgx6qd7l9lcvbhq1wzbxarldcl6al84nmhqvgp")))

