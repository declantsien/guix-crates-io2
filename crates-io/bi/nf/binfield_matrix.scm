(define-module (crates-io bi nf binfield_matrix) #:use-module (crates-io))

(define-public crate-binfield_matrix-0.1.0 (c (n "binfield_matrix") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.41") (d #t) (k 0)))) (h "1wbs26wxkiamqps5wg5ypxbf05qvb8km97nz5frch6jzxqc82axa")))

(define-public crate-binfield_matrix-0.1.1 (c (n "binfield_matrix") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1.41") (d #t) (k 0)))) (h "1za4rqw42nb414yj5f56by4a67c5hk67mdjxcbx903v80007cj4d")))

(define-public crate-binfield_matrix-0.1.2 (c (n "binfield_matrix") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.1.41") (d #t) (k 0)))) (h "1djfqfijg01xwndzcdwb4g7zg7d1bis2vcy6h2wjb5bzbxr8p40w")))

(define-public crate-binfield_matrix-0.2.0 (c (n "binfield_matrix") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0l0b4fzcpglshayfkj9vk4rxwq2yym7kzn5k8z491g88m83z3087")))

