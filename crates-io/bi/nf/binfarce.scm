(define-module (crates-io bi nf binfarce) #:use-module (crates-io))

(define-public crate-binfarce-0.0.1 (c (n "binfarce") (v "0.0.1") (h "0rcqqc2zqb8iz88wl0hicibynp1c637krsylafrf30ajhn6x4zbm")))

(define-public crate-binfarce-0.1.0-rc1 (c (n "binfarce") (v "0.1.0-rc1") (h "0v40fjlvwjj7f0lyhqs4ah27kfn3aq0788a3iy3qd58g9jd2gb19")))

(define-public crate-binfarce-0.1.0 (c (n "binfarce") (v "0.1.0") (h "1wx3h7mzkbpa98ip97cw2i52jrn3i6zrhd5rx63h3gw1dx72qfid")))

(define-public crate-binfarce-0.1.1 (c (n "binfarce") (v "0.1.1") (h "04f7a8grg66ada4gfl2z9k0bl653zy14a1n14xcjp89rgplj31m3")))

(define-public crate-binfarce-0.2.0 (c (n "binfarce") (v "0.2.0") (h "1zxqg43xqz6xmyv68wc6xzigczhrq3hfcg2nx5jh4db468lkin9r")))

(define-public crate-binfarce-0.2.1 (c (n "binfarce") (v "0.2.1") (h "18hnqqir3gk5sx1mlndzgpxs0l4rviv7dk3h1piyspayp35lqihq")))

