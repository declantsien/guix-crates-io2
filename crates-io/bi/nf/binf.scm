(define-module (crates-io bi nf binf) #:use-module (crates-io))

(define-public crate-binf-0.1.0 (c (n "binf") (v "0.1.0") (h "12mpdx1mh8arl373y40naksqmnb8lrxfdz6lkdgfl0hqf36v3r6l") (y #t)))

(define-public crate-binf-0.1.1 (c (n "binf") (v "0.1.1") (h "17ffgxfpiv60m56zfch4fx4haa4k7dzrqh3cdjm35qzvngzn7i5i")))

(define-public crate-binf-0.1.2 (c (n "binf") (v "0.1.2") (h "0lbkm2jg5g440ix450w1kcca559xr5pisp8l6q8r0n3gps1b8rk0")))

(define-public crate-binf-0.2.0 (c (n "binf") (v "0.2.0") (h "0mihh1nprn362n8wznrbkhlxvxdqi30jyi902d0f05acf8ynv9hg")))

(define-public crate-binf-0.2.1 (c (n "binf") (v "0.2.1") (h "0lfszfqjwwl4yf0fdwyzkmgw8mh3r0y5shin8npgga96d6cvqkqh")))

(define-public crate-binf-0.2.2 (c (n "binf") (v "0.2.2") (h "0zli869nfggnz91fl0n4y7s7lwz3k08r1zjklk1vswbpd5sw9w8z")))

(define-public crate-binf-0.2.3 (c (n "binf") (v "0.2.3") (h "1884llglr0j6b7bd7c6s83zaadnk0bjpv3hgxwfrwfvadpbbks7y")))

(define-public crate-binf-1.0.0-rc1 (c (n "binf") (v "1.0.0-rc1") (d (list (d (n "binf_macros") (r "^0.1.0") (d #t) (k 0)))) (h "18055kh3gcvm2s32wkhkvplwckcidczf5gc1nqqqkl3w4mj4za2i")))

(define-public crate-binf-1.0.0 (c (n "binf") (v "1.0.0") (d (list (d (n "binf_macros") (r "^0.1.1") (d #t) (k 0)))) (h "13sabzkc90fbjvvd3m0cpnr6dx1c3k033rd99lj2jjp8ryc4swkd")))

(define-public crate-binf-1.0.1 (c (n "binf") (v "1.0.1") (d (list (d (n "binf_macros") (r "^1.0.1") (d #t) (k 0)))) (h "12aqlpwrbwc18y3jj2bi7v244y7ld4jn577n3csw1h9za8y45643")))

(define-public crate-binf-1.0.2 (c (n "binf") (v "1.0.2") (d (list (d (n "binf_macros") (r "^1.0.2") (d #t) (k 0)))) (h "1afp7nm4yfxy6357sn4jvhcpyjs9i3a7qnbki05zhj6i8ic5jlzw")))

(define-public crate-binf-1.1.0 (c (n "binf") (v "1.1.0") (d (list (d (n "binf_macros") (r "^1.1.0") (d #t) (k 0)))) (h "1h545vg4jcgb0whwav3qclbll2wmlcvkjs0lsjjirawihdy0zk6f")))

(define-public crate-binf-1.1.1 (c (n "binf") (v "1.1.1") (d (list (d (n "binf_macros") (r "^1.1.1") (d #t) (k 0)))) (h "1d277zj48dwnzx2ndqx2l60bl34mm9y0ka6mihw3fk5mhgk5wm6k")))

