(define-module (crates-io bi nf binfire-lib) #:use-module (crates-io))

(define-public crate-binfire-lib-0.0.0 (c (n "binfire-lib") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18b93rrznq17ampr7nz1wffapivjr1xpsq85vh410fz6mxkk4lqd")))

