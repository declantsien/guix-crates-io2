(define-module (crates-io bi nf binfire) #:use-module (crates-io))

(define-public crate-binfire-0.0.0 (c (n "binfire") (v "0.0.0") (d (list (d (n "binfire-lib") (r "^0.0") (k 0)) (d (n "regex") (r "^1.6") (f (quote ("std"))) (k 0)))) (h "07p6vwaj8lrj0qx6fyrr1gij9hzi9xj5vr65w8b57cwshbg0dgqd")))

