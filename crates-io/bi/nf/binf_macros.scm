(define-module (crates-io bi nf binf_macros) #:use-module (crates-io))

(define-public crate-binf_macros-0.1.0 (c (n "binf_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1jgd6pz7lxgvwg6775a1vs2rvh2d4l8c8jsjrbs20gry1wlrc9rj")))

(define-public crate-binf_macros-0.1.1 (c (n "binf_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1n63acl9jm0y7rw5gxmlv5kwqvvl6f8igcfcg8fkq2nnzxxwyrfb")))

(define-public crate-binf_macros-1.0.1 (c (n "binf_macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0dgprk4fbp6bvsz37swwy4cza87dfc2k6k8ga3l586xv1n2h60x4")))

(define-public crate-binf_macros-1.0.2 (c (n "binf_macros") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0sbzk518zs3c46m1c0ixi4frdlf39r6jiz6ppp8qqsj676ad7kpa")))

(define-public crate-binf_macros-1.1.0 (c (n "binf_macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1c4xn1xg0qci018yvcfii9bk0hg4npxd4km1w887m2hqzh706527")))

(define-public crate-binf_macros-1.1.1 (c (n "binf_macros") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "19dlkyhdm3ii1bx3kdc8hkd1v4fxl0mk577cpwl3115177x0aqpr")))

