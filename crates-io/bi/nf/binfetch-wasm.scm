(define-module (crates-io bi nf binfetch-wasm) #:use-module (crates-io))

(define-public crate-binfetch-wasm-1.0.0 (c (n "binfetch-wasm") (v "1.0.0") (d (list (d (n "js-sys") (r "^0.3.67") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("XmlHttpRequest"))) (d #t) (k 0)))) (h "1yf28f6wb2zc94n0mkifgs4kllp8vr48kx2bzlc1qinwx3grca3q")))

(define-public crate-binfetch-wasm-1.0.1 (c (n "binfetch-wasm") (v "1.0.1") (d (list (d (n "js-sys") (r "^0.3.67") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.67") (f (quote ("XmlHttpRequest"))) (d #t) (k 0)))) (h "1fnbmnljbpj6my0izn3yibh6cvxks4yzciqhx1v0vijqr793kwgh")))

