(define-module (crates-io bi bi bibicode) #:use-module (crates-io))

(define-public crate-bibicode-0.1.0 (c (n "bibicode") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1dndqx5v2fi4df5582jxajhs02l954mc4w6g78cqnki3rmgn77b7")))

(define-public crate-bibicode-0.1.1 (c (n "bibicode") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "002rj30jkn212mjpi48yz7i6g1madg6hnfjz964qflkdpilg05zs")))

(define-public crate-bibicode-0.1.2 (c (n "bibicode") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "0fs8abnaclivvjx407j0v1v5m3jskb3kxc0lciiqc14g86s0n74z")))

(define-public crate-bibicode-0.1.3 (c (n "bibicode") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "0cjd6s1sijg06g52as11hd8is37qmpwsfk61mrmnwwvj193nzqwh")))

(define-public crate-bibicode-0.1.4 (c (n "bibicode") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "0mv6xvjfg112dk44vkz1rrmcjii00minv89lw4ds4swasmffx23z")))

(define-public crate-bibicode-0.1.5 (c (n "bibicode") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1i27ylbl4w243yppd8m6fn2yhn637jp2g7k3vrjmza7fbq67cznj")))

(define-public crate-bibicode-0.2.0 (c (n "bibicode") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zsy380rkqdhibsryhvrfkqc9s6b6dd4m8ra3bgd92qmps9hrs92")))

(define-public crate-bibicode-0.2.1 (c (n "bibicode") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zjpaxdssf1fpkdammmfz241gbgaghrd1232sz050rnvv6wq5f7g")))

(define-public crate-bibicode-0.2.2 (c (n "bibicode") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "1f3lsgc2isv8n2x5wwwn6krf01bh40z0xdhvz47wpjnjblglllx4")))

(define-public crate-bibicode-0.3.0 (c (n "bibicode") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "0a5k9zkg5zgbar0bsqb2amypajz72sbv515x4z10n3yfkdnnwxmx")))

(define-public crate-bibicode-0.3.1 (c (n "bibicode") (v "0.3.1") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "1axdy2xlabm67cy9cl2y0z0lzgnk58kgjiwpb4z8wnxvfhxazs4g")))

