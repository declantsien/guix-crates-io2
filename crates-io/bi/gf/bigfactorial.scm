(define-module (crates-io bi gf bigfactorial) #:use-module (crates-io))

(define-public crate-bigfactorial-0.1.1 (c (n "bigfactorial") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 2)))) (h "18zbbdpc4w62lkfiqcw6fsrn6yvc9ay5ziy2vdi0kb07rxjfg02a")))

(define-public crate-bigfactorial-0.1.2 (c (n "bigfactorial") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 2)))) (h "1c9m5hw9rvf1pdcid3wg8anplnng7yxspapfhnr09rwsm9ykqbj4")))

