(define-module (crates-io bi co bicoro) #:use-module (crates-io))

(define-public crate-bicoro-0.1.0 (c (n "bicoro") (v "0.1.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "0lzzkf03sc434c06qmx4isj745yrqd9a9scska3dzkm47s0gh5ni")))

(define-public crate-bicoro-0.1.1 (c (n "bicoro") (v "0.1.1") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "112vnb4a07mxhfzjv9z13nylf570gcmydac4dfmz5m87pb72nmzy")))

(define-public crate-bicoro-0.1.2 (c (n "bicoro") (v "0.1.2") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "02kxgzhnmxdz16jcxm7b3v17j37nhjhfc0jckh01hq6fn6yi9rwc")))

(define-public crate-bicoro-0.2.0 (c (n "bicoro") (v "0.2.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1f90lrgsb2q0pcycg0fy62376bf2fxi3707bmq6as7hgxc74jk14")))

(define-public crate-bicoro-0.2.1 (c (n "bicoro") (v "0.2.1") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "0i2gni57qh75kyrpl646ysdns8phapxkdqb1zdypx2wn2k7g5795")))

(define-public crate-bicoro-0.2.2 (c (n "bicoro") (v "0.2.2") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1yqjq81940z1v7yvnc461r9j8b0v4rk8m6j8gh4aw9fzlsl1h9n2")))

(define-public crate-bicoro-0.2.3 (c (n "bicoro") (v "0.2.3") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1mkh11psihnplqdffvw9lprnyah342a7kw7jgic8yg12rkc6rcdn")))

(define-public crate-bicoro-0.3.0 (c (n "bicoro") (v "0.3.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "16lx18vjy5va5p3r7wm7fx2frmk39cvcnr74k9gy4dbk1v3772bk")))

(define-public crate-bicoro-0.4.0 (c (n "bicoro") (v "0.4.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "14fjjfwll4ginbz32r0bgcz1cd48xvwikdsipgffikxd4c0avfsv")))

(define-public crate-bicoro-0.5.0 (c (n "bicoro") (v "0.5.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "026gxjc7kka25dpz59lfm6d41lssgxk4l1q8kh2gb6cxsw8nqq79")))

(define-public crate-bicoro-0.6.0 (c (n "bicoro") (v "0.6.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "0baysvgi8wj5l6i1bcidfyzn4sigj4irrj1zb7mv7r2zfmc3finm")))

(define-public crate-bicoro-0.7.0 (c (n "bicoro") (v "0.7.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "0ii7rhsf0dzsh4xr6g43di0ks4lr3048dfrddzdggsab57h7a161")))

(define-public crate-bicoro-0.8.0 (c (n "bicoro") (v "0.8.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1l6w80cachi0hps48hg8iy3vpy4p7msda3zga97w0i0z780433h5")))

(define-public crate-bicoro-0.8.1 (c (n "bicoro") (v "0.8.1") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1jc13lx9qlq1ixpgbklc7hh0fwz3h82jycfs9crdqhhk7lkw08hh")))

(define-public crate-bicoro-0.8.2 (c (n "bicoro") (v "0.8.2") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "01n6ambkk4xd5910mz8zcb6vlca31wjl29qxv0m9p56vrplscm9i")))

(define-public crate-bicoro-0.9.0 (c (n "bicoro") (v "0.9.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1wzqi72gy97inm4gdf0sfw25s4mdn66gwk7cdjghqd50cx545zcl")))

(define-public crate-bicoro-0.9.1 (c (n "bicoro") (v "0.9.1") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "0jqn7gc4nxg9yfds8c67f1hby5fbm2xvfn2ay716k6bxi14j74ps")))

(define-public crate-bicoro-0.9.2 (c (n "bicoro") (v "0.9.2") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1jbs7s4jxjyhwy9f1248krd4ddxb5y3s36va03dzjzjj4zj21qj0")))

(define-public crate-bicoro-0.9.3 (c (n "bicoro") (v "0.9.3") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1k7i358md3gyr933kb8fkcn57dmmwbd3vcfx8jz89szx3bi9gasy")))

(define-public crate-bicoro-0.10.0 (c (n "bicoro") (v "0.10.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1f94zyg004pic0crys68yplyz2416vik3ayl6bq256myv0x5zqjv")))

(define-public crate-bicoro-0.11.0 (c (n "bicoro") (v "0.11.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "02ajl525n3p8bvqh42ynqi2idqkaazrs14mv8q6w0a67c8jzb6xd")))

(define-public crate-bicoro-0.12.0 (c (n "bicoro") (v "0.12.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "0y2g5as24wlz6clz2xzxk7iwwwnf6qfycxikz59bs6s745p7rzwm")))

(define-public crate-bicoro-0.13.0 (c (n "bicoro") (v "0.13.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "10c31sih2r04zil9102857kmrmlwa04rmmm162cjp8k1g19i8fch")))

(define-public crate-bicoro-0.14.0 (c (n "bicoro") (v "0.14.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "0xhihlm4vjzd79qyahg9ccmxzbca0xfqn5pznd63nbfahijbi58y")))

(define-public crate-bicoro-0.15.0 (c (n "bicoro") (v "0.15.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1m7hlwscxnm2wnp6grpmcb0sfn9c5fk1367dh9kqg6shj9q384dv")))

(define-public crate-bicoro-0.16.0 (c (n "bicoro") (v "0.16.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "108rarxj35f1038gvhvmmlxdi05xk9w7b37l7bhqw3g97zj69vgw")))

(define-public crate-bicoro-0.17.0 (c (n "bicoro") (v "0.17.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "1ss3dadk5psjjvd85bx043bk7a9dp3vyyziw23nkszicdz8386dh")))

(define-public crate-bicoro-0.18.0 (c (n "bicoro") (v "0.18.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "15fz5za7742vb9hm1f69i078i8wcs9fk654x8398p1cb423796ww")))

(define-public crate-bicoro-0.19.0 (c (n "bicoro") (v "0.19.0") (d (list (d (n "do-notation") (r "^0.1.3") (d #t) (k 0)))) (h "023yrwdgr3qij0rahx18wahjpxsskr071h6ga7244xhp01a1v5lb")))

