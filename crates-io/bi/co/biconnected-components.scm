(define-module (crates-io bi co biconnected-components) #:use-module (crates-io))

(define-public crate-biconnected-components-0.1.0 (c (n "biconnected-components") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0blprwfa7mgn4ifcmjj8c8q57dv9ap0c77fkfwib7sjfyi2rbyip")))

(define-public crate-biconnected-components-0.2.0 (c (n "biconnected-components") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "002mfhqlk656zlpralmwhp28wmmmmh2x1jskd9y1bjnr9nvnd8l6")))

(define-public crate-biconnected-components-0.3.0 (c (n "biconnected-components") (v "0.3.0") (d (list (d (n "petgraph") (r "^0.6") (d #t) (k 0)))) (h "0s7102k34b5iz1vsr6wv7viwvgn2gsb8cb1qx7kiyr9z05h4nqya")))

