(define-module (crates-io bi cy bicycle-plugin-echo) #:use-module (crates-io))

(define-public crate-bicycle-plugin-echo-0.1.0 (c (n "bicycle-plugin-echo") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "033vwy7qh3xbc1pff1khdpsbky9v78aff5wi8xpj896b25kpl0ch")))

