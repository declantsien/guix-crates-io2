(define-module (crates-io bi cy bicycle_proto) #:use-module (crates-io))

(define-public crate-bicycle_proto-0.1.0 (c (n "bicycle_proto") (v "0.1.0") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "1h2z0rnsglyjk9sxrsig2ckg9h9rr4y47z2s92hc8kc68wynz4f8")))

(define-public crate-bicycle_proto-0.2.0-alpha.0 (c (n "bicycle_proto") (v "0.2.0-alpha.0") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "1njri51pf7z2w9zvgwdyx2nc9652wimfzfwbpcj250iiabwc3115")))

(define-public crate-bicycle_proto-0.2.0-alpha.1 (c (n "bicycle_proto") (v "0.2.0-alpha.1") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "1nxa8mpjahdzkghdr8qf7bnlr7r0pa683wi1ad606lz59r6a9mdp")))

(define-public crate-bicycle_proto-0.2.0-alpha.2 (c (n "bicycle_proto") (v "0.2.0-alpha.2") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "1xvvgbxcgv8kwilyf7q5h5c43dscn6kz86q55pp4i14n3sz5a8kr")))

(define-public crate-bicycle_proto-0.2.0-alpha.3 (c (n "bicycle_proto") (v "0.2.0-alpha.3") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "0k2v9g8yq1p4x12xymbqvmmrv358nf586gsacq92m3vrx8rb8317")))

(define-public crate-bicycle_proto-0.2.0 (c (n "bicycle_proto") (v "0.2.0") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "09qzdinpfg8m35vr6w0fmvrhf087ssqsykh9bqcglblgpc7xr5v0")))

(define-public crate-bicycle_proto-0.2.1 (c (n "bicycle_proto") (v "0.2.1") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.11.0") (d #t) (k 1)))) (h "0lakpkp5dmc8zhwspv923aqm41q8dxc008j2nizvh4m5rbmrfcx1")))

(define-public crate-bicycle_proto-0.2.2 (c (n "bicycle_proto") (v "0.2.2") (d (list (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.11.0") (d #t) (k 0)) (d (n "tonic-build") (r "^0.11.0") (d #t) (k 1)))) (h "1q17xjd321zyaqqfdkbaprcnxxsddri8fp523bfwqmb67zxfy0vm")))

