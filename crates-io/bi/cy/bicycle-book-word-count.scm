(define-module (crates-io bi cy bicycle-book-word-count) #:use-module (crates-io))

(define-public crate-bicycle-book-word-count-0.1.0 (c (n "bicycle-book-word-count") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0ra8h7bd5m4lfzqdxk816kk52kk75rbi1bwwv6xgbyddz880fxx8")))

(define-public crate-bicycle-book-word-count-0.1.1 (c (n "bicycle-book-word-count") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1qrz1g64xqpwhb81b9jvdid1m0vpp39qbygmgn0a7izvc1pffmid")))

