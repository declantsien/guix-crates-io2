(define-module (crates-io bi cy bicycle_rocksdb) #:use-module (crates-io))

(define-public crate-bicycle_rocksdb-0.1.0 (c (n "bicycle_rocksdb") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)))) (h "14z2p9py9gkh28xph28b286l7vrgdqa8h14bf8ails7hkyphy4fr")))

(define-public crate-bicycle_rocksdb-0.2.0-alpha.0 (c (n "bicycle_rocksdb") (v "0.2.0-alpha.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)))) (h "1i45ndaamkvc5khh7c9sl37kpbivsxhml1r28s9a0xp5z4wwx4mk")))

(define-public crate-bicycle_rocksdb-0.2.0-alpha.1 (c (n "bicycle_rocksdb") (v "0.2.0-alpha.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)))) (h "1pzr6lvmmfs0sm8iy0payxsbsmnhbn7znra4zvp1pkrskdnq8lqg")))

(define-public crate-bicycle_rocksdb-0.2.0-alpha.3 (c (n "bicycle_rocksdb") (v "0.2.0-alpha.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)))) (h "0w1blf6q0ynm11km4dq7smjwrk92hf27cv10dxf2ra8xf1sl5g8x")))

(define-public crate-bicycle_rocksdb-0.2.0 (c (n "bicycle_rocksdb") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)))) (h "1q1biz6qwvpmrs0xji4nsv0cjdvyb7hm8x9imfbimacj8xy9vyx8")))

