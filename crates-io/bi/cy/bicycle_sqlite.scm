(define-module (crates-io bi cy bicycle_sqlite) #:use-module (crates-io))

(define-public crate-bicycle_sqlite-0.1.0 (c (n "bicycle_sqlite") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.24.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1sg57r3n3ha5jakqx1g6rav6y37arjbi1q1ik61nhsbp1k07mvbh")))

(define-public crate-bicycle_sqlite-0.2.0-alpha.2 (c (n "bicycle_sqlite") (v "0.2.0-alpha.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.24.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0dw36cinfh4jxwf08k067l4wffd1f338k00a9lwq159vikl06vlc")))

(define-public crate-bicycle_sqlite-0.2.0-alpha.3 (c (n "bicycle_sqlite") (v "0.2.0-alpha.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.24.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "05pzq5w5gn3xbf5h2ykp3jklh7r0kgd3hj7whwgkny736fn7rvhm")))

(define-public crate-bicycle_sqlite-0.2.0 (c (n "bicycle_sqlite") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.24.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0z37102vkgk1zhs9yir63v512lj7nwc5klhn1yvafvzbq1ds4hci")))

(define-public crate-bicycle_sqlite-0.2.2 (c (n "bicycle_sqlite") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.24.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0rylry5hd3nh2plfzf40nmxfl6m7zmfhz8iqivx5qmxzn0080qml")))

