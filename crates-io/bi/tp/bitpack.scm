(define-module (crates-io bi tp bitpack) #:use-module (crates-io))

(define-public crate-bitpack-0.1.0 (c (n "bitpack") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "176f5av2029vhs9h8mfqzralhjjpk1vmjvrchn936kfyygz2jsqm")))

(define-public crate-bitpack-0.1.1 (c (n "bitpack") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (k 0)))) (h "0xjgb2a8mnlvf70d0rdyy0h10pzzhdb13xr2rn7gddmbj4f9sg0g")))

(define-public crate-bitpack-0.2.0 (c (n "bitpack") (v "0.2.0") (h "1dxf5pldh0088nwrn8wlsph90qmrlsyspq3p5nps1v81j6kksgn4") (f (quote (("use_vec") ("use_std"))))))

(define-public crate-bitpack-0.2.1 (c (n "bitpack") (v "0.2.1") (h "0dpnzajpvn7p2dq4cp2fk34fvd7inyfvdyqnr0k51n8n34bs58b1") (f (quote (("use_std"))))))

