(define-module (crates-io bi tp bitpat) #:use-module (crates-io))

(define-public crate-bitpat-0.1.0 (c (n "bitpat") (v "0.1.0") (d (list (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "137qvh82ggdi2j36m6z75nynsnqs056r90459y9sagi9zwq8kg6x")))

(define-public crate-bitpat-0.1.1 (c (n "bitpat") (v "0.1.1") (d (list (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1iaxdfl1xii8y67zfcq8k08vynapznh9n64nwc9yhav69wwr1i6d")))

