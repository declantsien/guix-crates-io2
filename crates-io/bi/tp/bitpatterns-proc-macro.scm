(define-module (crates-io bi tp bitpatterns-proc-macro) #:use-module (crates-io))

(define-public crate-bitpatterns-proc-macro-0.1.0 (c (n "bitpatterns-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "13k428z204yy23ap547jaiay4sgan116lfxnh8n126vndfxwsgdz")))

(define-public crate-bitpatterns-proc-macro-0.1.1 (c (n "bitpatterns-proc-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "12jq315v4i688j9whpf7cid56iprk2r6nmi2zncr0sa90jpq0mcg")))

