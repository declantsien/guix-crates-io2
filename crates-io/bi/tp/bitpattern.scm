(define-module (crates-io bi tp bitpattern) #:use-module (crates-io))

(define-public crate-bitpattern-0.1.0 (c (n "bitpattern") (v "0.1.0") (d (list (d (n "paste") (r "^0.1.18") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0d89bd38myyqp77rbbbx4sx3iddzwy45rmi8ahp55zdm8g2kdi1z")))

