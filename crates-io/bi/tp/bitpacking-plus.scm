(define-module (crates-io bi tp bitpacking-plus) #:use-module (crates-io))

(define-public crate-bitpacking-plus-0.1.0 (c (n "bitpacking-plus") (v "0.1.0") (d (list (d (n "bitpacking") (r "^0.8.4") (f (quote ("bitpacker1x" "bitpacker4x" "bitpacker8x"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1n2mjmi5zbk4v4dyp1vrl3iwr28jap4dwihaq7mkklls6jmgqd2j")))

(define-public crate-bitpacking-plus-0.2.0 (c (n "bitpacking-plus") (v "0.2.0") (d (list (d (n "bitpacking") (r "^0.8.4") (f (quote ("bitpacker1x" "bitpacker4x" "bitpacker8x"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1hz40s2cxsfhvafh6sp5finik26fchgl96k897fvbmdgabkih3zh")))

