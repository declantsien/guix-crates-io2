(define-module (crates-io bi tp bitpacking) #:use-module (crates-io))

(define-public crate-bitpacking-0.1.0 (c (n "bitpacking") (v "0.1.0") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0h4aqck5p1sfvnd3l94kqkljld8n9zns8d72glshn0iagh7n341b")))

(define-public crate-bitpacking-0.2.0 (c (n "bitpacking") (v "0.2.0") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "11cvxvrwqj00bns41j3fidmxq7k0rc1zmshhs07zk6hn37nia49c") (f (quote (("sse3") ("default" "sse3") ("avx2"))))))

(define-public crate-bitpacking-0.3.0 (c (n "bitpacking") (v "0.3.0") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "13q73v6mjhl8l4rs12ykjykk29md5nh7b8yyrvwczha52hhky23b")))

(define-public crate-bitpacking-0.3.1 (c (n "bitpacking") (v "0.3.1") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0h918f0h5fjf4y6d491z842d8k2k8nlp9gpvyaz55ns1fgrqiwmm")))

(define-public crate-bitpacking-0.4.0 (c (n "bitpacking") (v "0.4.0") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "002z91m16adixg6yfgb6z91z4bdasdh8lw9g27q92pm296vw01pi") (f (quote (("unstable" "simd") ("simd") ("default"))))))

(define-public crate-bitpacking-0.5.0 (c (n "bitpacking") (v "0.5.0") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0gq0hw0p8lyr0sw4hk8xymb1ajpjq7amr9z4zc9ljsqf8545mjqj")))

(define-public crate-bitpacking-0.5.1 (c (n "bitpacking") (v "0.5.1") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1rr1zg9dw9z68bh6iw2nzrlf9yvyb5ld1x1hd8na4i5lsa1lph3m")))

(define-public crate-bitpacking-0.6.0 (c (n "bitpacking") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "06x6m76lny7lficypxrr1pwfk55p5vfscihvi5jczcjqhd8kyzv6")))

(define-public crate-bitpacking-0.7.0 (c (n "bitpacking") (v "0.7.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "12jxqhbz1pzjw46vvnc7sy3z6di5rh53ix9y1l7rbwailqvcr65x")))

(define-public crate-bitpacking-0.8.0 (c (n "bitpacking") (v "0.8.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "02p1a37vpb3bbnspwsb6kk4a30v3sfjgqml98162bw9izn53h639") (f (quote (("default" "bitpacker1x" "bitpacker4x" "bitpacker8x") ("bitpacker8x") ("bitpacker4x") ("bitpacker1x")))) (y #t)))

(define-public crate-bitpacking-0.8.1 (c (n "bitpacking") (v "0.8.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "01f4rly7kqv1vkviv88ghg4zcbapzgszn1fcyasaldxmzsbfk53g") (f (quote (("default" "bitpacker1x" "bitpacker4x" "bitpacker8x") ("bitpacker8x") ("bitpacker4x") ("bitpacker1x"))))))

(define-public crate-bitpacking-0.8.2 (c (n "bitpacking") (v "0.8.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0s6bb9zdaf2vx6bvijbq0icvziv0kqz77dqbpjgajdrl1brayi1p") (f (quote (("default" "bitpacker1x" "bitpacker4x" "bitpacker8x") ("bitpacker8x") ("bitpacker4x") ("bitpacker1x"))))))

(define-public crate-bitpacking-0.8.3 (c (n "bitpacking") (v "0.8.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1jvdc0p3bbiyx037ldzpydixrr4836av1vxplmzysp0mjdkrdh3w") (f (quote (("default" "bitpacker1x" "bitpacker4x" "bitpacker8x") ("bitpacker8x") ("bitpacker4x") ("bitpacker1x")))) (y #t)))

(define-public crate-bitpacking-0.8.4 (c (n "bitpacking") (v "0.8.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1mw2xbpkw9zhfq5lir6bgkalhhfqb5p7xwx5yimc0ry1ffnd5ix8") (f (quote (("default" "bitpacker1x" "bitpacker4x" "bitpacker8x") ("bitpacker8x") ("bitpacker4x") ("bitpacker1x"))))))

(define-public crate-bitpacking-0.9.1 (c (n "bitpacking") (v "0.9.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "09z7dxrcd7si1sdaicrgs34mdknhj17rmsmbwjmpyhd151mc7jdi") (f (quote (("default" "bitpacker1x" "bitpacker4x" "bitpacker8x") ("bitpacker8x") ("bitpacker4x") ("bitpacker1x"))))))

(define-public crate-bitpacking-0.9.2 (c (n "bitpacking") (v "0.9.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crunchy") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "14pa6bksmxjsqq6v1rw50fhzz3hqqfpifywz2y5081ldzlmkw7ac") (f (quote (("default" "bitpacker1x" "bitpacker4x" "bitpacker8x") ("bitpacker8x") ("bitpacker4x") ("bitpacker1x"))))))

