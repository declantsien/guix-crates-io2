(define-module (crates-io bi tp bitpack-vec) #:use-module (crates-io))

(define-public crate-bitpack-vec-0.0.1 (c (n "bitpack-vec") (v "0.0.1") (d (list (d (n "deepsize") (r "^0.2.0") (d #t) (k 0)) (d (n "divrem") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vi9s5b4vm52caq8dzr6jxiiks8lwl3yjkp5mgk7b5m1195z2h7z")))

