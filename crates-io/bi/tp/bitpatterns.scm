(define-module (crates-io bi tp bitpatterns) #:use-module (crates-io))

(define-public crate-bitpatterns-0.1.0 (c (n "bitpatterns") (v "0.1.0") (d (list (d (n "bitpatterns-proc-macro") (r "=0.1.0") (d #t) (k 0)))) (h "1f0wgdbkwlf0v6xhg6ck0949mmjy7frdxhkdar8xvbm53mfxkwvf")))

(define-public crate-bitpatterns-0.1.1 (c (n "bitpatterns") (v "0.1.1") (d (list (d (n "bitpatterns-proc-macro") (r "=0.1.1") (d #t) (k 0)))) (h "0nx18j1wb47shqyk1v6xksz1ybhxp13na7qqmcspr5b83kmb2k3x")))

