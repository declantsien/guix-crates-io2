(define-module (crates-io bi sh bishop-cli) #:use-module (crates-io))

(define-public crate-bishop-cli-0.1.0 (c (n "bishop-cli") (v "0.1.0") (d (list (d (n "bishop") (r "^0.1.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0iiyvq17pmi9b9hrlwbsk3109wys39hc3v4p4pxgwx0vckfmf9b3")))

(define-public crate-bishop-cli-0.1.1 (c (n "bishop-cli") (v "0.1.1") (d (list (d (n "bishop") (r "^0.1.1") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "01wjy99w3cfjw7pq12ls8hwkgfjwnx4llj0m3psw6f3kj8l433p7")))

(define-public crate-bishop-cli-0.2.0 (c (n "bishop-cli") (v "0.2.0") (d (list (d (n "bishop") (r "^0.2.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1dwkrnaldx6fbjd1xpmm8xk9wk2m3vckdypafsznh0rc2cwpc4af")))

(define-public crate-bishop-cli-0.2.1 (c (n "bishop-cli") (v "0.2.1") (d (list (d (n "bishop") (r "^0.3.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1fw8j0kf9in0vcqlif70r1k2036gywq8d6p3s50qxwk22klm4mak")))

(define-public crate-bishop-cli-0.2.2 (c (n "bishop-cli") (v "0.2.2") (d (list (d (n "bishop") (r "^0.3.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "07d8v1f7xnxxhmyjbgy2nkvgin5y0q2pyz7hhka4llyqiij1gm1n")))

(define-public crate-bishop-cli-0.2.3 (c (n "bishop-cli") (v "0.2.3") (d (list (d (n "bishop") (r "^0.3.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "0vvg5kjd2raqfn7fd1bxiimwzv201d5xl2qam1i31dk855jj79jj")))

(define-public crate-bishop-cli-0.2.4 (c (n "bishop-cli") (v "0.2.4") (d (list (d (n "bishop") (r "^0.3.0") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "0vn2gpm08sjv7kg2grqjq6gqgjk637dk4cg6lnyfvj91c3wvizg9")))

(define-public crate-bishop-cli-0.2.5 (c (n "bishop-cli") (v "0.2.5") (d (list (d (n "bishop") (r "^0.3.4") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "1zglxa8fhi7h105qjl84x2kp8s0h127s2aa8035qw6d7qbz8dxc3")))

(define-public crate-bishop-cli-0.2.6 (c (n "bishop-cli") (v "0.2.6") (d (list (d (n "bishop") (r "^0.3.4") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "031bs1xrl8n90w5jxkdl64x8dj4gwd3infgbvpb7gbrfhrfvp24b")))

