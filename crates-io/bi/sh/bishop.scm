(define-module (crates-io bi sh bishop) #:use-module (crates-io))

(define-public crate-bishop-0.1.0 (c (n "bishop") (v "0.1.0") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.6") (d #t) (k 0)))) (h "1vqh53l8lbasnz27696nffazlbsccp1643j3rsfbrxvhmqkai79x")))

(define-public crate-bishop-0.1.1 (c (n "bishop") (v "0.1.1") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.6") (d #t) (k 0)))) (h "1098xhaw51a1v3vx26zk7bilhxxs9jwsf031la81mgl6amhrnlrz")))

(define-public crate-bishop-0.2.0 (c (n "bishop") (v "0.2.0") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.6") (d #t) (k 0)))) (h "1w10752p15aq2p3l03ns51dir2kqhgigygylr96rpb5pc4qs7a62")))

(define-public crate-bishop-0.3.0 (c (n "bishop") (v "0.3.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.6") (d #t) (k 0)))) (h "16idnm146sqns7wij7rnbnxg1k1ai0zpxafyznymvj80xhv58b2d") (y #t)))

(define-public crate-bishop-0.3.1 (c (n "bishop") (v "0.3.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.6") (d #t) (k 0)))) (h "1h2xaylgvq240r1077j7ngx5155l38s6nh9k81f8sl17gvmv6b5d")))

(define-public crate-bishop-0.3.2 (c (n "bishop") (v "0.3.2") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.6") (d #t) (k 0)))) (h "03rkrhxrwz6r4s50c25njxi043axwps201w3fx4jd8143kfdsz8q")))

(define-public crate-bishop-0.3.3 (c (n "bishop") (v "0.3.3") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.6") (d #t) (k 0)))) (h "18cwq1556n5c3r9i3lskd0fp4bamcjlc7y49dscbd9zlxlxf3cc3")))

(define-public crate-bishop-0.3.4 (c (n "bishop") (v "0.3.4") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.6") (d #t) (k 0)))) (h "1lvz7vhn0ga6lf2sk1jxiknni6bjygjb9ffy00zykcvj9cyxpp09")))

