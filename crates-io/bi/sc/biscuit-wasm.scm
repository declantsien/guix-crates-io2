(define-module (crates-io bi sc biscuit-wasm) #:use-module (crates-io))

(define-public crate-biscuit-wasm-0.1.3 (c (n "biscuit-wasm") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "biscuit-auth") (r "^2.1.0") (f (quote ("wasm" "serde-error"))) (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-logger") (r "^0.2.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "18s6fickpzv1lix7l7d6wvim6d57wmm86l7i8v6gcivw1szly91c")))

