(define-module (crates-io bi ne binext) #:use-module (crates-io))

(define-public crate-binext-1.0.0 (c (n "binext") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("min_const_gen"))) (d #t) (k 2)))) (h "15kw9fijg851y7w4j3jmh82qyk16kv2jncgfp7z81qph5p2w10y0")))

