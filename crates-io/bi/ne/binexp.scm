(define-module (crates-io bi ne binexp) #:use-module (crates-io))

(define-public crate-binexp-0.1.0 (c (n "binexp") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1lla70d4hyhpy8irwbjdk32zam56dbmm4hpyylmjm8lp7izgxigv") (r "1.68")))

(define-public crate-binexp-0.1.1 (c (n "binexp") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1k4j4jbszg5yj7ildz6yss2ca0pshizhg74cr11f9kd1hj6s84lm") (r "1.68")))

