(define-module (crates-io bi tt bitterlemon) #:use-module (crates-io))

(define-public crate-bitterlemon-0.2.0 (c (n "bitterlemon") (v "0.2.0") (d (list (d (n "arrayvec") (r ">= 0.3") (d #t) (k 0)) (d (n "random") (r ">= 0.12") (d #t) (k 0)))) (h "13h7p2ypjinw6ffx0qbw46hwk33f7yyi4dikjxb6lwdmmjwazcj0")))

(define-public crate-bitterlemon-0.2.1 (c (n "bitterlemon") (v "0.2.1") (d (list (d (n "arrayvec") (r ">= 0.3") (d #t) (k 0)) (d (n "random") (r ">= 0.12") (d #t) (k 2)))) (h "0c6m624l6ydza7k2s01s70kvbh7fvfl53b30ks582zc93chniabh")))

(define-public crate-bitterlemon-0.2.2 (c (n "bitterlemon") (v "0.2.2") (d (list (d (n "arrayvec") (r ">= 0.4") (d #t) (k 0)) (d (n "random") (r ">= 0.12") (d #t) (k 2)))) (h "14vkkpj0dpxj7ch5xmda097flxaxm7vmw40jxh9grc9hw6hr1158")))

(define-public crate-bitterlemon-0.2.3 (c (n "bitterlemon") (v "0.2.3") (d (list (d (n "arrayvec") (r ">=0.4") (d #t) (k 0)) (d (n "random") (r ">=0.12") (d #t) (k 2)))) (h "1f6van57g5v3i0sbl14n50aa1jmh4lbn8xif3z6z4pckz9i540dc")))

(define-public crate-bitterlemon-0.2.4 (c (n "bitterlemon") (v "0.2.4") (d (list (d (n "arrayvec") (r ">=0.7") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "random") (r ">=0.13") (d #t) (k 2)))) (h "10n0v66q65bc6n9s0fpn5rpx2kcks96p7lag5cimzff86qfnxhv8")))

