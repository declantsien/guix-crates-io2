(define-module (crates-io bi tt bitter) #:use-module (crates-io))

(define-public crate-bitter-0.1.0 (c (n "bitter") (v "0.1.0") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 2)) (d (n "bitstream-io") (r "^0.6") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.1.2") (d #t) (k 2)))) (h "050z960hzid81s18lg6734lwsdplpyrg0x6vdsc4k61pw602id7g")))

(define-public crate-bitter-0.2.0 (c (n "bitter") (v "0.2.0") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 2)) (d (n "bitstream-io") (r "^0.6") (d #t) (k 2)) (d (n "criterion") (r "^0.2.7") (d #t) (k 2)) (d (n "nom") (r "^4") (d #t) (k 2)))) (h "1yv5p3j0krwmhv1arfj74nv77p6zj6j4710mm0zphqzyrxjshzdx")))

(define-public crate-bitter-0.3.0 (c (n "bitter") (v "0.3.0") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 2)) (d (n "bitstream-io") (r "^0.6") (d #t) (k 2)) (d (n "bitterv1") (r "= 0.1.0") (d #t) (k 2) (p "bitter")) (d (n "criterion") (r "^0.2.7") (d #t) (k 2)) (d (n "nom") (r "^4") (d #t) (k 2)))) (h "1i11blnik26cc78czyr1r53y2w6aysrgcc8gx1q5niy8kf80c6w6")))

(define-public crate-bitter-0.3.1 (c (n "bitter") (v "0.3.1") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 2)) (d (n "bitstream-io") (r "^0.6") (d #t) (k 2)) (d (n "bitterv1") (r "= 0.1.0") (d #t) (k 2) (p "bitter")) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "0a46y30mvp1yg21dxykp5q9f63ffg2fi82y37c34m1a4qp3fhkkx")))

(define-public crate-bitter-0.3.2 (c (n "bitter") (v "0.3.2") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 2)) (d (n "bitstream-io") (r "^0.6") (d #t) (k 2)) (d (n "bitterv1") (r "= 0.1.0") (d #t) (k 2) (p "bitter")) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nom") (r "^4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1vbj50wyb2lk5v761zmsifsfqrkl8b1j3zhiymvzxniy4rsywysk")))

(define-public crate-bitter-0.4.0 (c (n "bitter") (v "0.4.0") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 2)) (d (n "bitstream-io") (r "^0.6") (d #t) (k 2)) (d (n "bitterv1") (r "= 0.1.0") (d #t) (k 2) (p "bitter")) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1csgj604bq5s7767gh2xi1dhmmnf9b9xfil2w29hdhwy3sbagvgm")))

(define-public crate-bitter-0.4.1 (c (n "bitter") (v "0.4.1") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 2)) (d (n "bitstream-io") (r "^0.6") (d #t) (k 2)) (d (n "bitterv1") (r "=0.1.0") (d #t) (k 2) (p "bitter")) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1x8p5najv2mygcwy7gmfbv6s7q9910115iiaav1pblmlg3dbpkdq")))

(define-public crate-bitter-0.5.0 (c (n "bitter") (v "0.5.0") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 2)) (d (n "bitstream-io") (r "^0.6") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "1ybahyvvq6g03lqhwhjpmihwgdpmlqlnjbzcl6j03rcwy87cv0bf") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitter-0.5.1 (c (n "bitter") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "089069fygwzchmavcxyj45jnzspxb969qf5sb834bgy0zxrx9mjg") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitter-0.6.0 (c (n "bitter") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1q9cidbfwcibqrkbam4pzxrdbvhhagmam0rn9r15wzz56ph8ipvi") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitter-0.6.1 (c (n "bitter") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "010sp82sydcbyw86x03kvzkn3lg8j6wdd0vdhnw1l9c47dbglgw0") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitter-0.6.2 (c (n "bitter") (v "0.6.2") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0gd7n09qsl9jvp89mx74j273cwp6pj32bfbas4d6a449r2jmhn8y") (f (quote (("std") ("default" "std"))))))

(define-public crate-bitter-0.7.0 (c (n "bitter") (v "0.7.0") (d (list (d (n "no-panic") (r "^0.1") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "01sy3djqny29i5d8s4fmbj9mcnv5c2r7drgb0262xacn2jvi6fpg") (f (quote (("std") ("default" "std"))))))

