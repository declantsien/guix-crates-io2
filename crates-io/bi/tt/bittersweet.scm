(define-module (crates-io bi tt bittersweet) #:use-module (crates-io))

(define-public crate-bittersweet-0.1.0 (c (n "bittersweet") (v "0.1.0") (h "1qgwff5pf9nq9qc4sn9g85ywrjj0xnhc4zpsd8msikn1l4b15csz")))

(define-public crate-bittersweet-0.1.1 (c (n "bittersweet") (v "0.1.1") (h "1kjz2vjr20qcyzgbc49j8vhaxp0bjx9sslzvvpqz457naa7qc4va")))

(define-public crate-bittersweet-0.1.2 (c (n "bittersweet") (v "0.1.2") (h "126iaclqpa3lw74jdaa8vf754fd43q1c71sq7hmx47kvqdag13cp")))

