(define-module (crates-io bi tt bittle) #:use-module (crates-io))

(define-public crate-bittle-0.1.0 (c (n "bittle") (v "0.1.0") (h "0273l74fi8435vbngw85k5gqj74i4fcdjnwmb17f5794wlg9mybh")))

(define-public crate-bittle-0.1.1 (c (n "bittle") (v "0.1.1") (h "1c54jxwqajbrvzdmhdfgvjlaphhah4ww5cv9lz223pf20x45xcr6")))

(define-public crate-bittle-0.2.0 (c (n "bittle") (v "0.2.0") (h "06dy4wmrnpn7rxs3izi0892hbh34qhcb1jlf6ki62sm34yvn31am")))

(define-public crate-bittle-0.2.1 (c (n "bittle") (v "0.2.1") (h "1vmv7aqkziz3vg0nc5w0hhrywsc2q6qszj9b4f3c5sk8nhg9hbbz")))

(define-public crate-bittle-0.2.2 (c (n "bittle") (v "0.2.2") (h "0da1yh7dwlhcaaail0qq3hwz7y26xl2ap09b9gkvwy6pdp05sxjy")))

(define-public crate-bittle-0.3.0 (c (n "bittle") (v "0.3.0") (h "0s07gknc97vq3c2b6da2130846m6b1kfwhizmn9ygc5nfr0xn3d9") (y #t)))

(define-public crate-bittle-0.3.1 (c (n "bittle") (v "0.3.1") (h "1n5jdbwi038yjyzhh0n70i4r3420qhkkn5sidvgwyv008nyijgyx") (y #t) (r "1.65.0")))

(define-public crate-bittle-0.3.2 (c (n "bittle") (v "0.3.2") (h "18aljlqn19irdqx1w1il90b7zsx7pd90qbxvjfbjf8iygg46sib4") (y #t) (r "1.65.0")))

(define-public crate-bittle-0.3.3 (c (n "bittle") (v "0.3.3") (h "06648xmdbivf68xnay9w16cqd7y294rg7kdh64lhqrclyrccryl5") (y #t) (r "1.65.0")))

(define-public crate-bittle-0.3.4 (c (n "bittle") (v "0.3.4") (h "1f75qyarihnk7lswb9mzg5z263g6lqkkgl21bzd6wdibqwgq6byr") (r "1.65.0")))

(define-public crate-bittle-0.3.5 (c (n "bittle") (v "0.3.5") (h "05q8z4cb70nb6azfqixhzr2y29rpszviqrmfbpcxwky8js5m8fkl") (r "1.65.0")))

(define-public crate-bittle-0.4.0 (c (n "bittle") (v "0.4.0") (h "1kirmb51vhwpgwl6l1w85zrrxn2948dwbawff47x7dcljjgph6w2") (r "1.65.0")))

(define-public crate-bittle-0.4.1 (c (n "bittle") (v "0.4.1") (h "1gw1smjb9qda2qwap32fywxbiihcn1j5irq88ksqxsz6xbpmvsrd") (r "1.65.0")))

(define-public crate-bittle-0.4.2 (c (n "bittle") (v "0.4.2") (h "07piamcf9nfvjd2gp8p24pmhrrcgjl6673a5jf92lr8wrc5izvr4") (r "1.65.0")))

(define-public crate-bittle-0.4.3 (c (n "bittle") (v "0.4.3") (h "0nnvnx46kkwyp70yzwzary0wvy8ma8j6nfq8k10i9bl835iivcyi") (r "1.65.0")))

(define-public crate-bittle-0.4.4 (c (n "bittle") (v "0.4.4") (h "1m3dbz6phwghxdbjlqzs2ycszznbxyvs4pc32ngxaji4g6xhfdg3") (r "1.65.0")))

(define-public crate-bittle-0.5.0 (c (n "bittle") (v "0.5.0") (h "1s1j7w53kgrf85xyjxq6iz9clnns6gqggdi4q675vp0jy0an5jzy") (r "1.65.0")))

(define-public crate-bittle-0.5.1 (c (n "bittle") (v "0.5.1") (h "1hsb54l50gqbcy0k3vhrq8dhk03mpl0dy7pf0jd3l73szg905ad9") (r "1.65.0")))

(define-public crate-bittle-0.5.2 (c (n "bittle") (v "0.5.2") (h "0lq6wzrk7mb5jm1f93g4qcw3zc4mb16ccimvfi2kw1li55lhgp0q") (r "1.65.0")))

(define-public crate-bittle-0.5.3 (c (n "bittle") (v "0.5.3") (h "0bw6vsnd6bxwxf2s9nqsg0cyl517d9a3vprws5apzyszm3wysmkb") (r "1.65.0")))

(define-public crate-bittle-0.5.4 (c (n "bittle") (v "0.5.4") (h "0pblba81kv4i35rfmr9i02hppc1hihsm4pk6gzsnrmq9phrg7b3d") (r "1.65.0")))

(define-public crate-bittle-0.5.5 (c (n "bittle") (v "0.5.5") (h "01i785g5ps6la4vs2kl9hsrinkq8ri46cpsxc6d7krhqg4yqgnfm") (r "1.65")))

(define-public crate-bittle-0.5.6 (c (n "bittle") (v "0.5.6") (h "1y49cffq5grvajnlm1qmq0jiqh4j4hykvymv1zqf7kki6443y5ql") (r "1.65")))

(define-public crate-bittle-0.5.7 (c (n "bittle") (v "0.5.7") (h "0nxch0ysp9s0afcva8fw4l9wrrhyb92s1g7vinnvfy207x8vql26") (r "1.65")))

