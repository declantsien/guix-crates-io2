(define-module (crates-io bi tt bitty_write_macro) #:use-module (crates-io))

(define-public crate-bitty_write_macro-0.1.0 (c (n "bitty_write_macro") (v "0.1.0") (d (list (d (n "format_args_conditional") (r "^0.1.1") (d #t) (k 0)))) (h "0gyjny6rvkh30cl0b2cb8rq8xz9nn5jwwbksxpr5yyg62qldphb3") (f (quote (("std"))))))

(define-public crate-bitty_write_macro-0.1.1 (c (n "bitty_write_macro") (v "0.1.1") (d (list (d (n "format_args_conditional") (r "^0.1.1") (d #t) (k 0)))) (h "0alj54vli57kcwzd3krybq58yvsqbdkmaqi00d0aaizkmy5yf0cm") (f (quote (("std"))))))

(define-public crate-bitty_write_macro-0.1.2 (c (n "bitty_write_macro") (v "0.1.2") (d (list (d (n "format_args_conditional") (r "^0.1.1") (d #t) (k 0)))) (h "1f58p8sxghjbv39a9shkjlkkx8srgj0ngklp2zdjk0prj4ygmxqp") (f (quote (("std"))))))

