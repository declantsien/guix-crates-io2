(define-module (crates-io bi tt bittwiddler) #:use-module (crates-io))

(define-public crate-bittwiddler-0.0.1 (c (n "bittwiddler") (v "0.0.1") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1kg57lkamazarws8i2kfbn2lpsknlqvhp8h7nvjhnqmyad92lbjr")))

(define-public crate-bittwiddler-0.0.2 (c (n "bittwiddler") (v "0.0.2") (d (list (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1m93rppq85f194yqbsi7xj3y6csqbka401lvbj6f4pqg15dz74f4")))

