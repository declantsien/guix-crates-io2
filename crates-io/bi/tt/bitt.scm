(define-module (crates-io bi tt bitt) #:use-module (crates-io))

(define-public crate-bitt-0.1.0 (c (n "bitt") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "02j9jkfj3bhvb8w98hcd1sc6m3x43g4g7avc4qs5ggkca6qpixyf")))

(define-public crate-bitt-0.2.0 (c (n "bitt") (v "0.2.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "05696a3whgql1aviryjmy11iancw2pcpla5ddph4w5hxh7ylkaij")))

(define-public crate-bitt-0.3.0 (c (n "bitt") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1s3ahnny7hwqiq9kgjfbg1396iy9rxwr34qfpzblkq3cdgwcaq31")))

(define-public crate-bitt-0.4.0 (c (n "bitt") (v "0.4.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ppsbi88fgvgjj0lg9xpa9nw60jfhs4izpgv0c6nmgg733dgd1bx")))

(define-public crate-bitt-0.4.1 (c (n "bitt") (v "0.4.1") (d (list (d (n "bevy") (r "^0.13") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cj41nspycqs2nj9a54fv9wz79vwnm7i1w87dvf4nv4wiv7dq9h0")))

(define-public crate-bitt-0.5.0 (c (n "bitt") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n1c5bghnlw4piz23ls3cvq6q0l16s5wf9zwwcbzzpdrh4dmdkdv")))

