(define-module (crates-io bi tt bitter-cost) #:use-module (crates-io))

(define-public crate-bitter-cost-0.1.0 (c (n "bitter-cost") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "xattr") (r "^1.3.1") (d #t) (k 0)))) (h "0rwlj4fmpkq6lamg1392klkz5wlbjhmyc3vwdlqnvr01vhj8jqhc") (f (quote (("xattrs") ("miscellany") ("default" "all") ("datetime") ("all" "datetime" "xattrs" "miscellany")))) (y #t)))

