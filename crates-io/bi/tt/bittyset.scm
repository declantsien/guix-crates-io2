(define-module (crates-io bi tt bittyset) #:use-module (crates-io))

(define-public crate-bittyset-0.1.0 (c (n "bittyset") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.5.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "gensym") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1pk9fadliwsb08kbqh5y0n0flk2lkv0y08dbrcvqkr0s7sv9wwn9")))

