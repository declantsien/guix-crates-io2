(define-module (crates-io bi gr bigraph) #:use-module (crates-io))

(define-public crate-bigraph-0.1.0-alpha.3 (c (n "bigraph") (v "0.1.0-alpha.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "1s3krh0yjxgb9hxl89lhsxz5s97dcvm8l4c6gw42f3r5sa3his0m")))

(define-public crate-bigraph-0.1.0-alpha.4 (c (n "bigraph") (v "0.1.0-alpha.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "08jzj4lr0ij1zcv1mydhv41mvwz25301pqrywmqlrr8q2yrf9vmr")))

(define-public crate-bigraph-0.1.0-alpha.5 (c (n "bigraph") (v "0.1.0-alpha.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "00zmi1nl0cmjb2sq6ysbvjgzzd0vagnm9s8dc2zp7iqhncb5d5mq")))

(define-public crate-bigraph-0.1.0-alpha.6 (c (n "bigraph") (v "0.1.0-alpha.6") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "0vp0smlxqab62w8rs2lp7xbdk4a0qdk775x9q6j206mcx2ncsx30")))

(define-public crate-bigraph-0.1.0-rc.1 (c (n "bigraph") (v "0.1.0-rc.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "10b2rq9xmkdf5q5izgfav11pccg5kf7mmkpwl7smq6smw7p8agnv")))

(define-public crate-bigraph-0.1.0 (c (n "bigraph") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.1.0") (d #t) (k 0)))) (h "0njpccqk7rbqnp4yzyg50ll6a47nd7kgm6ydszzsxk8axds3c73k")))

(define-public crate-bigraph-0.2.0 (c (n "bigraph") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.2.0") (d #t) (k 0)))) (h "0ixb01zy1h11md4v93ngxgzaw1m2ngysyr7hgj84m94p9lc9szf6")))

(define-public crate-bigraph-0.3.0 (c (n "bigraph") (v "0.3.0") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.3.0") (d #t) (k 0)))) (h "0sybsywc4rvcx10qwgyzpp22m36kgavk8x74w2n8fyb04x2cfkr0")))

(define-public crate-bigraph-0.3.1 (c (n "bigraph") (v "0.3.1") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.3.1") (d #t) (k 0)))) (h "15hnaby28fakrmp9b381ivd54mzq1lrxpywlbm5adlbzy0an76kf")))

(define-public crate-bigraph-0.3.1-alpha.1 (c (n "bigraph") (v "0.3.1-alpha.1") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.3.1-alpha.0") (d #t) (k 0)))) (h "1nvvxqaqj6rplj8g35rcykx2xacrjr8yj6smc194g85ifl6qp3kh")))

(define-public crate-bigraph-0.4.0 (c (n "bigraph") (v "0.4.0") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.4.0") (d #t) (k 0)))) (h "0kklkyidmsyg76l7r9srbbac7vdpp7316pnvcn8aigxg7czp9z4v")))

(define-public crate-bigraph-0.4.1 (c (n "bigraph") (v "0.4.1") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.4.0") (d #t) (k 0)))) (h "0h0nz39zbl5d0wdgl4881pq7mn0l8gqhbw865wq9y2lggqh0z2sb")))

(define-public crate-bigraph-0.4.3 (c (n "bigraph") (v "0.4.3") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.4.0") (d #t) (k 0)))) (h "08z44njz8pw5cq0mkd30m0f12pk6p3zkqx828zfpsr0fcbmygcca")))

(define-public crate-bigraph-0.4.4 (c (n "bigraph") (v "0.4.4") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.4.0") (d #t) (k 0)))) (h "1lp019jv8c5hn21xdyia3gdn0cab451q2as4c659vp1amz624qmw")))

(define-public crate-bigraph-0.4.5 (c (n "bigraph") (v "0.4.5") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.4.0") (d #t) (k 0)))) (h "18slc2hylzklykky41dbyypylba2zy9zizhc3l2b7w6b7fgcw4n2")))

(define-public crate-bigraph-0.5.0 (c (n "bigraph") (v "0.5.0") (d (list (d (n "bitvector") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "traitgraph") (r "^0.5.0") (d #t) (k 0)))) (h "0s5jxx8g88jcfhji2vgyscxkyymvsbbn1f9rff2apd9fig7ip5xy")))

(define-public crate-bigraph-0.6.0 (c (n "bigraph") (v "0.6.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitgraph") (r "^0.6.1") (d #t) (k 0)))) (h "14h2yrik17rciifjk7vwzw2wlgxhn8122w7da2y4sygi2xb5vqlz")))

(define-public crate-bigraph-0.6.1 (c (n "bigraph") (v "0.6.1") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitgraph") (r "^0.7.0") (d #t) (k 0)))) (h "07dwsljb8wwm1gaq5ps3cdycvdcysarrda9x02bv3n6injvgicbc")))

(define-public crate-bigraph-0.7.0 (c (n "bigraph") (v "0.7.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitgraph") (r "^0.7.0") (d #t) (k 0)))) (h "17z1cclvcmy6aw985cxzpsp6c2pyrd9ag73jw3aw1db4aqjcadfs") (r "1.58.1")))

(define-public crate-bigraph-0.8.0 (c (n "bigraph") (v "0.8.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitgraph") (r "^0.10.0") (d #t) (k 0)))) (h "0ria1brpxklcrgahpfmj30gcdslbcwwrrxijxdhkrlrgaxh2ji6k") (r "1.58.1")))

(define-public crate-bigraph-1.0.0 (c (n "bigraph") (v "1.0.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitgraph") (r "^1.0.0") (d #t) (k 0)))) (h "1igp94q9safc2djwp5jpzn7d9ing0p8c21ilv4b7j9bdadw5rmsi") (r "1.58.1")))

(define-public crate-bigraph-2.0.0 (c (n "bigraph") (v "2.0.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "opaque_typedef") (r "^0.0.5") (d #t) (k 0)) (d (n "opaque_typedef_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "0h63lrjcql2cswn3x76sal299n498pi45q7l3mgvz5hs5yq2g9l0") (r "1.58.1")))

(define-public crate-bigraph-2.0.1 (c (n "bigraph") (v "2.0.1") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "15bpa975lm3w6px7rlvszy995qh7fsgxh46g6bmsnqgjawj372dy") (r "1.58.1")))

(define-public crate-bigraph-2.0.2 (c (n "bigraph") (v "2.0.2") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^2.0.0") (d #t) (k 0)))) (h "1vc9rjv5035as97548jgkr1jrlnnhhagzmm18qfr7lyb0hcarvkl") (r "1.58.1")))

(define-public crate-bigraph-2.1.0 (c (n "bigraph") (v "2.1.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^2.2.0") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^5.3.0") (d #t) (k 0)))) (h "1ay8i5mkqa27jhg4k6vimdkmfrm45ffmal4z71fkdxc17n36sg16") (r "1.58.1")))

(define-public crate-bigraph-2.1.1 (c (n "bigraph") (v "2.1.1") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^2.2.0") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^5.3.0") (d #t) (k 0)))) (h "0y25yblgi7nld6yv2adk1n897svsgaycp64mfq6j9cqk9wmhgb1d") (r "1.58.1")))

(define-public crate-bigraph-3.0.0 (c (n "bigraph") (v "3.0.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^3.0.0") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^6.0.0") (d #t) (k 0)))) (h "1zidf55x9sj4zjxcp79061al5nx4pvj7fly32b3ldyq9wdc21vpw") (r "1.65.0")))

(define-public crate-bigraph-4.0.0-alpha.1 (c (n "bigraph") (v "4.0.0-alpha.1") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0-alpha.2") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^7.0.0-alpha.1") (d #t) (k 0)))) (h "17hxc47biz1yfsgjs3vl40zlv09kghdb9n0w97vw1b6yjzg6ry3i") (r "1.65.0")))

(define-public crate-bigraph-4.0.0-alpha.2 (c (n "bigraph") (v "4.0.0-alpha.2") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0-alpha.3") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^7.0.0-alpha.2") (d #t) (k 0)))) (h "0pg8q8apqppf90da6bbgr01crxv0a6ahpzn23pbz02lg51k8wp6x") (r "1.65.0")))

(define-public crate-bigraph-4.0.0-alpha.3 (c (n "bigraph") (v "4.0.0-alpha.3") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0-alpha.4") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^7.0.0-alpha.3") (d #t) (k 0)))) (h "0jc867pk2frmq3lpbslkrkd1dqffjzqcy3y1nxs18r5i5nsxy5lr") (r "1.65.0")))

(define-public crate-bigraph-4.0.0-alpha.4 (c (n "bigraph") (v "4.0.0-alpha.4") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0-alpha.5") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^7.0.0-alpha.4") (d #t) (k 0)))) (h "0mlgn0hpx13alr60jqw7xvq3z25k0hf5l1wnbc91af5gmpagdqyp") (r "1.65.0")))

(define-public crate-bigraph-4.0.0-alpha.5 (c (n "bigraph") (v "4.0.0-alpha.5") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0-alpha.6") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^7.0.0-alpha.5") (d #t) (k 0)))) (h "1kwx2gbw877kx8snybqdgrw24fawpj5nh89yzb53jvc7s0g57nm8") (r "1.65.0")))

(define-public crate-bigraph-4.0.0 (c (n "bigraph") (v "4.0.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^7.0.0") (d #t) (k 0)))) (h "0s6232cp36bbhmdalyl3qi8h48w966p8gxxsvb58k96wsb2yxxpz") (r "1.65.0")))

(define-public crate-bigraph-4.1.0 (c (n "bigraph") (v "4.1.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^4.0.0") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^7.0.0") (d #t) (k 0)))) (h "0cdn3dclfvk0mwncxz2xxascq3sy9w5f0y5d2ayz0abj9h128p62") (r "1.65.0")))

(define-public crate-bigraph-5.0.0 (c (n "bigraph") (v "5.0.0") (d (list (d (n "bitvector") (r "^0.1.5") (d #t) (k 0)) (d (n "traitgraph") (r "^5.0.0") (d #t) (k 0)) (d (n "traitgraph-algo") (r "^8.0.0") (d #t) (k 0)))) (h "04mr145vc3zpfh2hwzh0djdmzp4q2kbyd2jpkva3h8jvqhg47dmc") (r "1.65.0")))

