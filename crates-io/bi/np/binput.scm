(define-module (crates-io bi np binput) #:use-module (crates-io))

(define-public crate-binput-0.1.0 (c (n "binput") (v "0.1.0") (h "0q9d2i52138r2fmpq7hr96ihhzzgfkpdvzqbh12bi67z5pa1hbm1")))

(define-public crate-binput-0.1.1 (c (n "binput") (v "0.1.1") (h "1saqdsgq5giqcz4qis6312kivlfxa7zkgrdvydbvrackz6ggicpk")))

(define-public crate-binput-0.1.2 (c (n "binput") (v "0.1.2") (h "18xk9j7jc8464chs117s6ac9xg9wmcrm87bgk8gdgakca0nc6wn5")))

(define-public crate-binput-0.1.3 (c (n "binput") (v "0.1.3") (h "0fn5disvkvl7h37ncidd0wwsz495zlpkqs8bs81yy14pjj0jdi6x")))

(define-public crate-binput-0.2.0 (c (n "binput") (v "0.2.0") (h "136iyiy5rjp8h8slxx9hqv95jbadzp0w0ksq5ii9rznym1igjy98")))

