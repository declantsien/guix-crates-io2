(define-module (crates-io bi np binprot_derive) #:use-module (crates-io))

(define-public crate-binprot_derive-0.1.0 (c (n "binprot_derive") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lxdxn40q3g77xd05z026f9s2ajzly3nsw4jzfqk1vdg1va8ssrl")))

(define-public crate-binprot_derive-0.1.2 (c (n "binprot_derive") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0l6k4s0rxlv64z4a52sv4pnbfj5v2n17pmg33dzflb41gmd4n9yz") (f (quote (("async"))))))

(define-public crate-binprot_derive-0.1.3 (c (n "binprot_derive") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kw4gnh2yqx4prjvjrp31xwx9yw579ibdqcz3i9wnyn8ssiszacx") (f (quote (("async"))))))

(define-public crate-binprot_derive-0.1.4 (c (n "binprot_derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y47sif8n6g627ffaw3wnxg8591747lg5z6zyl59iyrc29gij3rv") (f (quote (("async"))))))

(define-public crate-binprot_derive-0.1.5 (c (n "binprot_derive") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jl027wvl1v35wbpwx5swkcm17xmgmki68ll0hp83w10mgmk789g") (f (quote (("async"))))))

(define-public crate-binprot_derive-0.1.6 (c (n "binprot_derive") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ya23320cvh4n2m54kcj2yc6iyrfmm2rqcd5zn197cgf0vks3yk3") (f (quote (("async"))))))

(define-public crate-binprot_derive-0.1.7 (c (n "binprot_derive") (v "0.1.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rbx2fjc36xw75djp57bsazavz1fns5irv03bh0hmkz13v2ydvp3") (f (quote (("async"))))))

