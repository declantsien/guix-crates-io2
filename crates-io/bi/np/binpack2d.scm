(define-module (crates-io bi np binpack2d) #:use-module (crates-io))

(define-public crate-binpack2d-1.0.0 (c (n "binpack2d") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1myjyjswi575ab482r8kn6nn09a0l9pgfkxw30lsp1243nv5lkzl")))

(define-public crate-binpack2d-1.0.1 (c (n "binpack2d") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0krrfhjhn3rjnlnxjikh858jg8qcl09kflf9x8s959wy3bdr6dsc")))

