(define-module (crates-io bi np binper) #:use-module (crates-io))

(define-public crate-binper-0.1.0 (c (n "binper") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "scroll") (r "^0.10") (k 0)))) (h "0bmaf6k4v49afd1yphyg0ksycmg0zrnai6ba3y6j2m5l5srw0ycz") (f (quote (("std" "alloc" "scroll/std") ("default" "std") ("alloc" "scroll/derive" "log")))) (y #t)))

(define-public crate-binper-0.0.1 (c (n "binper") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "scroll") (r "^0.10") (k 0)))) (h "0vv87g75vmbmplj4yx9x1vaby87h7km4c381dwwycann55bw4h33") (f (quote (("std" "alloc" "scroll/std") ("default" "std") ("alloc" "scroll/derive" "log"))))))

(define-public crate-binper-0.0.2 (c (n "binper") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "scroll") (r "^0.10") (k 0)))) (h "10574a77ws8va33zdrxv0f3p65p883069y87i17zyfz583r14q8w") (f (quote (("std" "alloc" "scroll/std") ("default" "std") ("alloc" "scroll/derive" "log"))))))

(define-public crate-binper-0.1.1 (c (n "binper") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "scroll") (r "^0.10") (k 0)))) (h "00ixgwvbma17nk6xjy955h5fxh1nxnivh7d6nb64i6hpn7qf2vvf") (f (quote (("std" "alloc" "scroll/std") ("default" "std") ("alloc" "scroll/derive" "log"))))))

(define-public crate-binper-0.1.2 (c (n "binper") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "scroll") (r "^0.10") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0wv322rfq17ipmv7sccmk5w5zz4sdscmfaak0zhh1kjwg05nj0xp") (f (quote (("std" "alloc" "scroll/std") ("default" "std") ("alloc" "scroll/derive" "log"))))))

(define-public crate-binper-0.1.3 (c (n "binper") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "scroll") (r "^0.10") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1zvvm05vs4sjcra0ybq3sz4jri265fnbx9g0v02bkxvbv63jvy5g") (f (quote (("std" "alloc" "scroll/std") ("default" "std") ("alloc" "scroll/derive" "log"))))))

(define-public crate-binper-0.1.4 (c (n "binper") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "scroll") (r "^0.10") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1s3p0ic9kblbpyc5g9an9vhb7dwh8ixbiz9dkwd1j9xajm4ay0ix") (f (quote (("std" "alloc" "scroll/std") ("default" "std") ("alloc" "scroll/derive" "log"))))))

