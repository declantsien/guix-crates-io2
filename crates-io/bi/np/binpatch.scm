(define-module (crates-io bi np binpatch) #:use-module (crates-io))

(define-public crate-binpatch-0.1.0 (c (n "binpatch") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.168") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.2") (d #t) (k 0)))) (h "1f04vpcwmdd3ylk813lvb9zh2qad1mhq51yxkyfzgs4rk0b1n9v4") (f (quote (("default"))))))

(define-public crate-binpatch-0.1.1 (c (n "binpatch") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.168") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.2") (d #t) (k 0)))) (h "13ahgian4d5i0bhgjzcbdmy0y21v76rr2pmmj8lbqs4vv79aaps7") (f (quote (("default"))))))

(define-public crate-binpatch-0.1.2 (c (n "binpatch") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.168") (o #t) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.2") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "07cyz211xbms8yg7rdkwd5k51as1y2b1gj4r096ydi80zrmpqllg") (f (quote (("default"))))))

