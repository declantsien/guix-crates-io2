(define-module (crates-io me sc mesc) #:use-module (crates-io))

(define-public crate-mesc-0.1.0 (c (n "mesc") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.191") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0x1j355fjs21lyyfwmaf4pnqqav8zxhz3pfs4awq2vdl8ac85iib")))

(define-public crate-mesc-0.1.1 (c (n "mesc") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.191") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1ghl3p3qpprligra5jgifqwl4zwj4i7ndv7nv44wqvb3ah7nq8gw")))

(define-public crate-mesc-0.1.2 (c (n "mesc") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.191") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1bdqvbh5vj3z60a7k6k0rk4pxjiwrhk3lrjw22wc3i24x1321kcf")))

(define-public crate-mesc-0.1.3 (c (n "mesc") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j9x3s4h738vikq075mzkd85706wzix3av4kq2b88abn2i68vi4n")))

(define-public crate-mesc-0.1.4 (c (n "mesc") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09vfpgmyak33l3ar4wpwck501w3y2gvrrldgf345gh34langmvyl")))

(define-public crate-mesc-0.2.0 (c (n "mesc") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15r8jj3riv8v04aicnl06id58ap7kn7lmdjf1h6fys9z3qa33lyi")))

