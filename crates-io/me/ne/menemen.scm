(define-module (crates-io me ne menemen) #:use-module (crates-io))

(define-public crate-menemen-0.1.0-alpha (c (n "menemen") (v "0.1.0-alpha") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.8") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)))) (h "0fixc0bh1pnbsjyqwxcr951r46x0bl6069gy8h50w3shlc7hn208")))

(define-public crate-menemen-0.2.0-alpha (c (n "menemen") (v "0.2.0-alpha") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)))) (h "17zzfr6vja2pj1hh4rww5w658sdsrr7bb7n92c46k83q831ggqnr")))

(define-public crate-menemen-0.2.1-alpha (c (n "menemen") (v "0.2.1-alpha") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)))) (h "0778wg092w5iqh4v6mqd60wylf4mc0cwmrssdy547n52z4rxn4fz")))

(define-public crate-menemen-0.2.2-alpha (c (n "menemen") (v "0.2.2-alpha") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)))) (h "1hfp1k9dg4qr4b509mvnp7y8hnmq383ypz8hs5lvdcyp4sracz0h")))

(define-public crate-menemen-0.3.2-alpha (c (n "menemen") (v "0.3.2-alpha") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (o #t) (d #t) (k 0)))) (h "157kcxg4hr4y0dyc6zbfq75pzgzrpkzqy7vgkkprr3vn0nmmqc38") (f (quote (("https" "openssl") ("default"))))))

(define-public crate-menemen-1.0.0 (c (n "menemen") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "1lxjzxwwyp8754jdxwb4amx75jn6ikaq2xb7pzb7f913l65n38ij")))

(define-public crate-menemen-1.0.1 (c (n "menemen") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "1i25wav9w9grc2k79z0l1xa5cb5lyrp2bc9synbwfsbxjr1xnlrk")))

(define-public crate-menemen-1.0.2 (c (n "menemen") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "libflate") (r "^1.3.0") (d #t) (k 2)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "0n5d6xsll27hx6zp1dn2f9r4r8qbyv94xp9ngywn4wpn53zadb73") (y #t)))

(define-public crate-menemen-1.0.3 (c (n "menemen") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "libflate") (r "^1.3.0") (d #t) (k 2)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)))) (h "1i4sb17y51da1xfmh398pw1xi2h6isc6a2g7c6lydmcmkybl3zbw")))

