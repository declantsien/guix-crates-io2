(define-module (crates-io me nv menva) #:use-module (crates-io))

(define-public crate-menva-0.1.0 (c (n "menva") (v "0.1.0") (h "0afq4dqr1i3p899msvq6ai8d5abcm4788wkl6mgp4agnvzrpiw3m")))

(define-public crate-menva-0.1.1 (c (n "menva") (v "0.1.1") (h "1gmz7cg70aa3hrlcs36qi0ywiw72lp4md0wiwz788lmvb9c1cgha")))

(define-public crate-menva-1.0.0 (c (n "menva") (v "1.0.0") (h "0b38kwsaib1xq44vpq1rs2184pa150i48zli5pmkkmz4cxxzivsa")))

