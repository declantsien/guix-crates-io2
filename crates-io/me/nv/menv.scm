(define-module (crates-io me nv menv) #:use-module (crates-io))

(define-public crate-menv-0.1.0 (c (n "menv") (v "0.1.0") (h "0zkq7cqilkpbd32ivfid2igkyvb0l6v4bsdmgghn3v4q5bm6hxn2")))

(define-public crate-menv-0.2.0 (c (n "menv") (v "0.2.0") (h "0awp4sa1n5fgknrq6v6dh5jdljyrap2h931b66yffvzx90jzldi1")))

(define-public crate-menv-0.2.1 (c (n "menv") (v "0.2.1") (h "00bvbi2g57kr6d524grh25qa2i4wk5gs4dzzlc5w1sy9s7ds2vhx")))

(define-public crate-menv-0.2.2 (c (n "menv") (v "0.2.2") (h "0gnd6qviq7j48pbmlar4b66h0zvp8g8skp3g75sny2mmd28mfawp")))

(define-public crate-menv-0.2.3 (c (n "menv") (v "0.2.3") (h "1j56isxqrw5yk1zn7awk2mhb9c0dnhrc83m9gdwjd3ccqnlqwnxn")))

(define-public crate-menv-0.2.4 (c (n "menv") (v "0.2.4") (h "0hsp4gbx9yhw940lyk9ij3shvpx0gj8d7155v9h6pqgxwq3sagrs")))

(define-public crate-menv-0.2.5 (c (n "menv") (v "0.2.5") (h "189qw42m2hyqrax5lhilv86r7q1gzzw7qgj3cxm3jkfnhfr3g2cs")))

(define-public crate-menv-0.2.6 (c (n "menv") (v "0.2.6") (d (list (d (n "menv_proc_macro") (r "^0.1.0") (d #t) (k 0)))) (h "18n7ih8zvy3cn3f2862p7lc0a55c6hgg11pa5ybr1x22w4m0sdlf")))

(define-public crate-menv-0.2.7 (c (n "menv") (v "0.2.7") (d (list (d (n "menv_proc_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1rh8sijqpk0jwdzk12pzjra11zhrr9brhyvy1f4jr0ngjn1ry17c")))

