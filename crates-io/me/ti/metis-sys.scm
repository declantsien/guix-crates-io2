(define-module (crates-io me ti metis-sys) #:use-module (crates-io))

(define-public crate-metis-sys-0.1.0 (c (n "metis-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58") (k 1)))) (h "0npx72gn0nf8ryj1jl4wkr18xx6p6pkyqqyjf0b3grhk7x631915")))

(define-public crate-metis-sys-0.1.1 (c (n "metis-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58") (k 1)))) (h "0yxxcz0kfw6q11v1cphjdgdhiqz2q49mmalc3hvk9asyr1x94l5b")))

(define-public crate-metis-sys-0.2.0 (c (n "metis-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65") (k 1)))) (h "1xdskfbvalarqqlr3kzr1xwys555mgm2dkhjb4bacgb2bn2q1gs9")))

(define-public crate-metis-sys-0.2.1 (c (n "metis-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.66") (f (quote ("runtime"))) (k 1)))) (h "0lqqz2v1b6lskdw4s241fbwd7vf7va9g1dalza544bnsz48qp2fc")))

(define-public crate-metis-sys-0.3.0 (c (n "metis-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)))) (h "00vysnmfjhkc3nyd8s0b15xwaywbks109bmammlbjx5lwzldwh9v") (f (quote (("use-system" "bindgen") ("generate-bindings" "vendored" "bindgen") ("default" "vendored")))) (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-metis-sys-0.3.1 (c (n "metis-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)))) (h "10bmmb9m0vlkwmnq3rlcnsm0ff0k5g36v0q3wx6q142f8681hvmp") (f (quote (("use-system" "bindgen") ("generate-bindings" "vendored" "bindgen") ("force-optimize-vendor" "vendored") ("default" "vendored" "force-optimize-vendor")))) (s 2) (e (quote (("vendored" "dep:cc"))))))

