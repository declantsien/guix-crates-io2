(define-module (crates-io me ti meticulous) #:use-module (crates-io))

(define-public crate-meticulous-0.1.0-pre.1 (c (n "meticulous") (v "0.1.0-pre.1") (h "0qynwr5h0sb6z2m7b8hzfli06yds823zisbnlxd1s0p224mzsljd") (f (quote (("disallow-todo-on-release") ("default"))))))

(define-public crate-meticulous-0.1.0 (c (n "meticulous") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1sghzx8zn9qvm588gybljqrrc729f43lsqm0imii5mgvbk0rqj6g") (f (quote (("disallow-todo-on-release") ("default"))))))

(define-public crate-meticulous-0.2.0-pre.1 (c (n "meticulous") (v "0.2.0-pre.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0l73ln4chxyqqfh73cfkq6j0jxac4nl06p1q0c4g8prk44bdlz0v") (f (quote (("disallow-todo-on-release") ("default"))))))

