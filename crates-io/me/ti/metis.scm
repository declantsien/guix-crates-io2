(define-module (crates-io me ti metis) #:use-module (crates-io))

(define-public crate-metis-0.1.0 (c (n "metis") (v "0.1.0") (d (list (d (n "metis-sys") (r "^0.1") (d #t) (k 0)))) (h "09k7maw4ms7b2iz7khal8g9nmb17djk3crkip4j1p2ls6sdybh0j")))

(define-public crate-metis-0.1.1 (c (n "metis") (v "0.1.1") (d (list (d (n "metis-sys") (r "^0.1") (d #t) (k 0)))) (h "0p35d5rwkpcm56pfp1faf7ymvfcxwynxs9l2qf26sxiik514x74n")))

(define-public crate-metis-0.1.2 (c (n "metis") (v "0.1.2") (d (list (d (n "metis-sys") (r "^0.2") (d #t) (k 0)))) (h "15y3h806x1668xjlv3i4wwhr8d38x4zlai4497jk9hamzmlm3mk2")))

(define-public crate-metis-0.2.0 (c (n "metis") (v "0.2.0") (d (list (d (n "metis-sys") (r "^0.3") (k 0)))) (h "1hzvra2a694af9nb8w0sfg7bq2xacd7d4pxzrry46ma322v67ad8") (f (quote (("vendored" "metis-sys/vendored") ("use-system" "metis-sys/use-system") ("default" "metis-sys/default"))))))

(define-public crate-metis-0.2.1 (c (n "metis") (v "0.2.1") (d (list (d (n "metis-sys") (r "^0.3") (k 0)))) (h "0pq4z1p3ww5597040hmj3fd3imlm9ark3g4pclkcvycvcpf05pm9") (f (quote (("vendored" "metis-sys/vendored") ("use-system" "metis-sys/use-system") ("default" "metis-sys/default"))))))

