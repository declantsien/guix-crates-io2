(define-module (crates-io me ti metio) #:use-module (crates-io))

(define-public crate-metio-0.1.0 (c (n "metio") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0a565mj427mi4ihd4llc94c8mw8hayahmvfsxd3xh5qkw7lljzsl") (f (quote (("serde" "serde/derive" "chrono/serde"))))))

