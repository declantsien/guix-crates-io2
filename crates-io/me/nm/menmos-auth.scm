(define-module (crates-io me nm menmos-auth) #:use-module (crates-io))

(define-public crate-menmos-auth-0.2.4 (c (n "menmos-auth") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "axum") (r "^0.5.1") (f (quote ("headers"))) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "branca") (r "^0.10.1") (d #t) (k 0)) (d (n "menmos-apikit") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.33") (f (quote ("max_level_trace" "release_max_level_debug"))) (d #t) (k 0)))) (h "0x477i749zad1cy2hz1sypcfgp8bg8b9yqbnmf0lwy0fcz03j310")))

(define-public crate-menmos-auth-0.2.5 (c (n "menmos-auth") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "axum") (r "^0.5.1") (f (quote ("headers"))) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "branca") (r "^0.10.1") (d #t) (k 0)) (d (n "menmos-apikit") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.33") (f (quote ("max_level_trace" "release_max_level_debug"))) (d #t) (k 0)))) (h "03h7i0bdq82vxrdhgbsm2nhcksk7wnpajfc1zhz9qzxlj1a6nmlp")))

(define-public crate-menmos-auth-0.2.6 (c (n "menmos-auth") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "axum") (r "^0.5.1") (f (quote ("headers"))) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "branca") (r "^0.10.1") (d #t) (k 0)) (d (n "menmos-apikit") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.33") (f (quote ("max_level_trace" "release_max_level_debug"))) (d #t) (k 0)))) (h "1a9ir7w245gslvgcwvk6va85h434sax1lwxnjs41w18f4dqaaqsy")))

