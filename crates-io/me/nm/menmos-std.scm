(define-module (crates-io me nm menmos-std) #:use-module (crates-io))

(define-public crate-menmos-std-0.0.7 (c (n "menmos-std") (v "0.0.7") (d (list (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0n7hhml9d0ficya53p276rq9ay8460sbf6z9h67prkn655kfq825")))

(define-public crate-menmos-std-0.0.8 (c (n "menmos-std") (v "0.0.8") (d (list (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gd0qsiax5rn2vr0740mmp0scszqcmds8r7x9d0pjj8h4945shf5")))

(define-public crate-menmos-std-0.0.9 (c (n "menmos-std") (v "0.0.9") (d (list (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 0)))) (h "02ksdgjx1vwgqwsrc0v5nihvvrghp2aq1iq1zl46wjin6pawdxlv")))

(define-public crate-menmos-std-0.0.10 (c (n "menmos-std") (v "0.0.10") (d (list (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1a6salw6wj1yly5b4abfx9qdms4hgxqs2cdcbv3lak0saa41fzk9")))

(define-public crate-menmos-std-0.1.0 (c (n "menmos-std") (v "0.1.0") (d (list (d (n "tokio") (r "^1.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0jgr2apgzik4991190i5la8bj87kv8ylxrl56c7m8k1z2p5d2i8i")))

(define-public crate-menmos-std-0.1.1 (c (n "menmos-std") (v "0.1.1") (d (list (d (n "tokio") (r "^1.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0zcwd3r2ryh3741dpnykfd9zi2di5nar5zwbdrnsgi9w4abxpa8m")))

(define-public crate-menmos-std-0.2.4 (c (n "menmos-std") (v "0.2.4") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("full"))) (d #t) (k 0)))) (h "1xmq0rkqrxk80wy67xhjr5zk8j1k5lljwqkgyzqp683clsxxizyh")))

(define-public crate-menmos-std-0.2.5 (c (n "menmos-std") (v "0.2.5") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0b6zq6rnjl2f7rbai26mf1ghf2ap9z5lwnfg1j02sqvx4sn97d3a")))

(define-public crate-menmos-std-0.2.6 (c (n "menmos-std") (v "0.2.6") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("full"))) (d #t) (k 0)))) (h "10vd1difb2q5m5hwhgk24g14n2s1w4xs332d4m3djx37s48drjwi")))

