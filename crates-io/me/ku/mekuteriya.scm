(define-module (crates-io me ku mekuteriya) #:use-module (crates-io))

(define-public crate-mekuteriya-0.1.0 (c (n "mekuteriya") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ethiopic-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0gdnyixfp04pk5gaq9r3saa0ylmsx2lqh8m1w4kkg34srfsxf5g4")))

(define-public crate-mekuteriya-0.1.2 (c (n "mekuteriya") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ethiopic-calendar") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "15c1ciihg9nr998lss719a4gkzfkqyq6d1yk5kk68b7vndvyjfvs")))

(define-public crate-mekuteriya-0.1.4 (c (n "mekuteriya") (v "0.1.4") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ethiopic-calendar") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0ldv46d3mx7lksq7lb9971jjlq3prd46wkij9d9rr7k8m7k2rgvv")))

