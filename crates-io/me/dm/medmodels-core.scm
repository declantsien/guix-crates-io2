(define-module (crates-io me dm medmodels-core) #:use-module (crates-io))

(define-public crate-medmodels-core-0.0.1-a1 (c (n "medmodels-core") (v "0.0.1-a1") (d (list (d (n "medmodels-utils") (r "^0.0.1-a1") (d #t) (k 0)) (d (n "polars") (r "^0.39.2") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fq3l36rfnlkwnhnx4v2mf0xkflvkshyrnjxhdq2ap9bj9603b4w")))

