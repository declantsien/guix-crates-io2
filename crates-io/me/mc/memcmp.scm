(define-module (crates-io me mc memcmp) #:use-module (crates-io))

(define-public crate-memcmp-0.0.1 (c (n "memcmp") (v "0.0.1") (h "0qskhgwwmyppmmclhx1135xzrj0n1ammr8zslxjfgg3qc6bi3gfc")))

(define-public crate-memcmp-0.0.2 (c (n "memcmp") (v "0.0.2") (h "067xzbwh55dw6swcf3ls1lm57sb63is99dp0hzld72zsn4v0xl8g")))

(define-public crate-memcmp-0.0.4 (c (n "memcmp") (v "0.0.4") (h "0caap8yba1d3h8l56xx6m96wnczhh62yaf8yzkv0y69xhk7049mv")))

(define-public crate-memcmp-0.0.5 (c (n "memcmp") (v "0.0.5") (h "1ry5wfi7vjg6hcq166hansrahwjary55adgpj6w4f0217v90n323")))

(define-public crate-memcmp-0.0.6 (c (n "memcmp") (v "0.0.6") (h "1qa4b56avh3s4nah46aq9f25gghvdknlsm92krda1djpwgz486ws") (f (quote (("nightly"))))))

