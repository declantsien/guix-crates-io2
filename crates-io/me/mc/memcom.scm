(define-module (crates-io me mc memcom) #:use-module (crates-io))

(define-public crate-memcom-0.1.0 (c (n "memcom") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "lapp") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "shared-mem-queue") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.6.0") (d #t) (k 0)) (d (n "try-ascii") (r "^1.0.0") (d #t) (k 0)))) (h "0m81bn9p9v4g7g8v194c3xscm3vgb0ylyiddklz3lcz3mrbig0d0")))

