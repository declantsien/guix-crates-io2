(define-module (crates-io me mc memconstruct_macros) #:use-module (crates-io))

(define-public crate-memconstruct_macros-0.1.0 (c (n "memconstruct_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1bpikgakx1b4242ff0xmzh7baw8wjiq8jm851a5fwsphn4nym8h6")))

