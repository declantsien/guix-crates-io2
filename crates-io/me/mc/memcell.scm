(define-module (crates-io me mc memcell) #:use-module (crates-io))

(define-public crate-memcell-0.1.0 (c (n "memcell") (v "0.1.0") (h "0d96cx3g4j82fjnjs6cyfnzzz0xaqkjbqanaav9vxvccyn8l8jib")))

(define-public crate-memcell-0.1.1 (c (n "memcell") (v "0.1.1") (h "0psfd6gi1fcq9sfm4b4fcpck530qknff21k1zvswgzmjx10qfjmq")))

