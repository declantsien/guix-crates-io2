(define-module (crates-io me mc memcached-protocal) #:use-module (crates-io))

(define-public crate-memcached-protocal-0.1.0 (c (n "memcached-protocal") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.1.12") (d #t) (k 0)))) (h "07j4x419n2waz9wqz1w1x7mjqhv0300v4jcihgsa4xhlcg1dwmpd")))

(define-public crate-memcached-protocal-0.1.1 (c (n "memcached-protocal") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.1.12") (d #t) (k 0)))) (h "0rfcqfhpjpk42fckz9v83q9avr94cs5slx1lkh3jlwmy396v3f3h")))

(define-public crate-memcached-protocal-0.1.2 (c (n "memcached-protocal") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.1.12") (d #t) (k 0)))) (h "18wa4d97m6b7mai2a0aa9fx547ni5d18bn6nh5nn4sdfyg0jj2b7")))

(define-public crate-memcached-protocal-0.1.3 (c (n "memcached-protocal") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.1.12") (d #t) (k 0)))) (h "1jzcjd8q6d2ay2k9hapn0yhq1ha201b5y5w8664z9g9ydskvjaq3")))

(define-public crate-memcached-protocal-0.1.4 (c (n "memcached-protocal") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.1.12") (d #t) (k 0)))) (h "0fycfdx2d897wxyapn5pw0fjdd2n5h4b0v4sds9wk7psm03fmj9c")))

(define-public crate-memcached-protocal-0.1.5 (c (n "memcached-protocal") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.1.12") (d #t) (k 0)))) (h "1vyyklkp7rw9wjd1g80g0qkraymzkr89g97g2b8m2xjxsd9lkffc")))

(define-public crate-memcached-protocal-0.1.6 (c (n "memcached-protocal") (v "0.1.6") (d (list (d (n "error-chain") (r "^0.2") (d #t) (k 0)))) (h "0r89arj2qsd0n9sk8by3gr1pr204nql6qr7kbsiy5wg6886y96bm")))

(define-public crate-memcached-protocal-0.1.7 (c (n "memcached-protocal") (v "0.1.7") (d (list (d (n "error-chain") (r "^0.2") (d #t) (k 0)))) (h "1kzfzsdabrlr1niv14g9vyyhfb0n9v33bgf02iwk4dp3sa2ln79d")))

(define-public crate-memcached-protocal-0.1.8 (c (n "memcached-protocal") (v "0.1.8") (d (list (d (n "error-chain") (r "^0.2") (d #t) (k 0)))) (h "1j31xmmid7c1jrllair6n63fq41gn3w64akipk0pvww5j4askf9d")))

(define-public crate-memcached-protocal-0.1.9 (c (n "memcached-protocal") (v "0.1.9") (d (list (d (n "error-chain") (r "^0.2") (d #t) (k 0)))) (h "1b93w1jcmz4k8y8lqbw4srvvz2i84il6shjnc69vhbv2h9xv23yb")))

(define-public crate-memcached-protocal-0.1.10 (c (n "memcached-protocal") (v "0.1.10") (d (list (d (n "error-chain") (r "^0.2") (d #t) (k 0)))) (h "0mz2d1mmprhyajjqaxpkn4w4vip7bns4bmav1qnq1x5cg1v963gm")))

(define-public crate-memcached-protocal-0.1.11 (c (n "memcached-protocal") (v "0.1.11") (d (list (d (n "error-chain") (r "^0.2") (d #t) (k 0)))) (h "0rh3fmpxip8bv3mzzafp4jpkz68p8ij1g0w3fg9am8vybkg9wx4j")))

