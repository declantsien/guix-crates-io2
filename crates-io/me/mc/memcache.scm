(define-module (crates-io me mc memcache) #:use-module (crates-io))

(define-public crate-memcache-0.0.1 (c (n "memcache") (v "0.0.1") (h "1zgbkclg779gwy5bm1ii439w15jl1lqgqdhhknv5llgdk6vkcj5g")))

(define-public crate-memcache-0.0.2 (c (n "memcache") (v "0.0.2") (h "1pgmm3gl1h61f68g84xv8abdl4fmygw5idf9zrc06hxm7b281j9b")))

(define-public crate-memcache-0.0.3 (c (n "memcache") (v "0.0.3") (h "18hasi7z6aq4qq0mvibcm0qgqcbnsnyy1grr7rci9bn3c215g5fl")))

(define-public crate-memcache-0.0.4 (c (n "memcache") (v "0.0.4") (h "1d3rp4spz1fkidcpmn7rax4sn11xbgp3xlvswi51cbj2cm6fnjpp")))

(define-public crate-memcache-0.0.5 (c (n "memcache") (v "0.0.5") (h "06sp30sv1yj986cj02ypscjgcd65d275mww4givk3sf8ibfj601k")))

(define-public crate-memcache-0.0.6 (c (n "memcache") (v "0.0.6") (h "1v6dndlc9k3x71ijvrm97lgbxvnv630z2l73p0nb33vx3rqpdna0")))

(define-public crate-memcache-0.0.7 (c (n "memcache") (v "0.0.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "00c97iw8zy46nv3lpvy1px6wm9fixacyjmqm4pm6grv9hamyd7rf")))

(define-public crate-memcache-0.0.8 (c (n "memcache") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0vmzx3jgccv1ps8pjpsb8xnhcrg5lgklniydkslzgsdnvaxc3wim")))

(define-public crate-memcache-0.0.9 (c (n "memcache") (v "0.0.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1wgbvni5s4y2grg7d73dpqy9n0pzn9bgvl6dap6lf6l2lajypvch")))

(define-public crate-memcache-0.0.10 (c (n "memcache") (v "0.0.10") (h "1a80wrzh4i1szndgnly8vlssmg3x096v9d046gi72lhghdrrlwl0")))

(define-public crate-memcache-0.0.11 (c (n "memcache") (v "0.0.11") (h "1rsj2zixqiav4rasyl3hfid128ll1hk64a1ni4sb4wl12s5w6688")))

(define-public crate-memcache-0.0.12 (c (n "memcache") (v "0.0.12") (h "11f1x8jpk9sqlz0j5isfcwx4pxw4krh3v43v01yr7ry12fmndn9g")))

(define-public crate-memcache-0.0.13 (c (n "memcache") (v "0.0.13") (h "1pjgxjj9fvr7iqyijaq5pl0x09f7nfj5mh0mpbgw9kljw6xiy2hn")))

(define-public crate-memcache-0.0.14 (c (n "memcache") (v "0.0.14") (h "0cni6drvnpap5a7q9y7yw0rg3snr13bzxgyz9zcl7kidssa399sf")))

(define-public crate-memcache-0.0.15 (c (n "memcache") (v "0.0.15") (h "0v3mh66rh5d8zjqymhk4fag1zf4bzfj6phf85vrz3wd3x9146yhg")))

(define-public crate-memcache-0.1.0 (c (n "memcache") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0r8m35acw5l4r0r5x3hwvbyf562kb9wwgicwvjclczz14c9nih22")))

(define-public crate-memcache-0.2.0 (c (n "memcache") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1w0d5c6nl6w51g761qgjva946402knmgpsl51xkjim7jkwnanhpq")))

(define-public crate-memcache-0.3.0 (c (n "memcache") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "063vsn6vwskcarf2axmsrkbsgp40xggn6j4h4xgjzrnlmzx1imrj")))

(define-public crate-memcache-0.3.1 (c (n "memcache") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0x7caw0xz2d8cwhjhnvgfbhym61na5bsgdc1y6ia24hkp8jgn25r")))

(define-public crate-memcache-0.4.0 (c (n "memcache") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1g0238af2x80qza8bzqb3pm2yg3xxlpg0wprrcpd35vf6bp4cl5g")))

(define-public crate-memcache-0.5.0 (c (n "memcache") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1cl7i9d2c8rv3vfrqdzg32p2ssm5vq8a5qw8sq90m94jdlhc6apk")))

(define-public crate-memcache-0.6.0 (c (n "memcache") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "08ffag9zyfw19fczs5lbb77nc4vjlqniw7m07bwpx1ybi5k7l15s")))

(define-public crate-memcache-0.7.0 (c (n "memcache") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1vabmlfh24lj0fq43k2sfhf1z5bs90l1m5q607b5wy9xfjkxqqz3")))

(define-public crate-memcache-0.7.1 (c (n "memcache") (v "0.7.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "11yfszcmkbr5iyb5691pjy2xh0l225h2mzy36vx4vq566ngikkln")))

(define-public crate-memcache-0.8.0 (c (n "memcache") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0qx016w7yzz446mbqirrkjv9bv0qyvaynqym98y348ypacwsgrwv")))

(define-public crate-memcache-0.9.0 (c (n "memcache") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0v59kfivl0s6k1w533rnk0kybvl9xars48ip96gqb3vcwc5mvz6a")))

(define-public crate-memcache-0.9.1 (c (n "memcache") (v "0.9.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0msdg3qjvhc51h17k4pfb3r3rly8wxfc048vh3xk69wxda9vxi2w")))

(define-public crate-memcache-0.9.2 (c (n "memcache") (v "0.9.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1dgspvb1kkdhfkbb09zibvxryjbmclczwsv2fbzkj2c6h3rpqjsq")))

(define-public crate-memcache-0.10.0 (c (n "memcache") (v "0.10.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0rmy6fzzbhhr97nscbiz9x9f9y4ymdlmmsb2g8q6rxii4sd08vxx")))

(define-public crate-memcache-0.11.0 (c (n "memcache") (v "0.11.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0z85ph78v548r9i2q0320jcbkc5i39zjp3n584di0n32glfsi4kq")))

(define-public crate-memcache-0.12.0 (c (n "memcache") (v "0.12.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0s9n6s4c58xyxi3ckgrjbbzgjx312zva1x6fax4iyk43c1f4a2lp")))

(define-public crate-memcache-0.12.1 (c (n "memcache") (v "0.12.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1p97ajfmgbv523ak6x4fc01gmcq7bhcvl9w50rxw2ykq4ifa0yqc")))

(define-public crate-memcache-0.12.2 (c (n "memcache") (v "0.12.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "17cb4m1fg8wh36sszm267xdhm6p8xfa1h7dny6ddgdgrqyi1fa4q")))

(define-public crate-memcache-0.12.3 (c (n "memcache") (v "0.12.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "10zkkb23x76xqqqkr1hlwyci9gf8wmnmbymagn05fszqkfdmx44x")))

(define-public crate-memcache-0.13.0 (c (n "memcache") (v "0.13.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0zmlylf8wglv49qdsinfzmc39y7kzhnnv4w43ahnv0xllkzmb8jr")))

(define-public crate-memcache-0.13.1 (c (n "memcache") (v "0.13.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "04cqnwrci6g0zsfk7m48mkbykndxfjysh26aznhb0ds380nqi845")))

(define-public crate-memcache-0.14.0 (c (n "memcache") (v "0.14.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "01wwls5jn5fwqw8dhglfpvlbv0s3wm2c9kakmgnfhrkim3a126y7") (f (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.15.0 (c (n "memcache") (v "0.15.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "18a8rna0zqrzhcjfqydznspw9psd6683y988d6yzqdbkvs7dd1bw") (f (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.15.1 (c (n "memcache") (v "0.15.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0w8cvicyw87vf5f7s5w7s019wd9g0giavpk51735is33y83px3l6") (f (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.15.2 (c (n "memcache") (v "0.15.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "10ws99f51x2kqfbrgd31mplgscyb0jk9g1br9a95d1w1f6kq7nmb") (f (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.16.0 (c (n "memcache") (v "0.16.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0qgs0hbapka0q62j1prbid3afxsr60mji2n9xzs3zihp53h4syvm") (f (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.17.0 (c (n "memcache") (v "0.17.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0lnbxgaflmadny0p8afnscv5m00yryhkp5sxv5h79lnkrkqasg06") (f (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.17.1 (c (n "memcache") (v "0.17.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1f4m7g38cqqjh62fzwcsbc7dj6flqacclm5yiqk5xdgpmjyi93zv") (f (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.17.2 (c (n "memcache") (v "0.17.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0fhmqw9crjzzpgp14wk52j7qjwwndssk66d5h32g4bjybm60i5h3") (f (quote (("tls" "openssl") ("default" "tls"))))))

