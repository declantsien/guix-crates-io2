(define-module (crates-io me mc memcache-async) #:use-module (crates-io))

(define-public crate-memcache-async-0.1.0 (c (n "memcache-async") (v "0.1.0") (d (list (d (n "tokio") (r "^0.1") (f (quote ("async-await-preview"))) (d #t) (k 0)))) (h "1l1cs6psphljd3wsgh9s37f373fv0hm9b6k6z18c02hn5kxw03i0")))

(define-public crate-memcache-async-0.2.0 (c (n "memcache-async") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat"))) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1kbmb3jcrbraq3igjkgrds7a4fqjyh4wy2pfp237wa5s5llgsijl")))

(define-public crate-memcache-async-0.2.1 (c (n "memcache-async") (v "0.2.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat"))) (d #t) (k 0)))) (h "0yqfyqspymkbb85ndbgci3866yzfm3b1kr83bmyzqv6g2wsrvy5z")))

(define-public crate-memcache-async-0.2.2 (c (n "memcache-async") (v "0.2.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat"))) (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat" "io-compat"))) (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1bccscpkw11ar4p8ds6s9cwfnpywq0l0hfwdkksb6vmgdlnf4ad6")))

(define-public crate-memcache-async-0.3.0 (c (n "memcache-async") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1mv5cm0xhrlkyv6xhby1hly7p6gkifq7qhmrz3ar8aci4wc92qn4")))

(define-public crate-memcache-async-0.3.1 (c (n "memcache-async") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1vrzwgdgbi7nh53rvfap7ps6lv39pa4s6z2n5i24iyhln6hjghpn")))

(define-public crate-memcache-async-0.4.0 (c (n "memcache-async") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0010v9614yzx3x7ysvdg352sab7aym9ms71j4f5j0ia6xdfxmbb6")))

(define-public crate-memcache-async-0.4.1 (c (n "memcache-async") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0l51prdkfyphmdllmbfcacin4n7zbpkikiz0320618gll57nhdrx")))

(define-public crate-memcache-async-0.4.2 (c (n "memcache-async") (v "0.4.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1qxayvsfln85k49rkib22d29w70zy49qlxcy991c90agg9f7qcxd")))

(define-public crate-memcache-async-0.4.3 (c (n "memcache-async") (v "0.4.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "08a81gzrypysqs15sqs059gx8bi0l095hjmhs83gy5w2kfynn32k")))

(define-public crate-memcache-async-0.5.0 (c (n "memcache-async") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "148cqr0ivwxl455aash08g4m1z3mn0bgfpcyy5k3674j51fr6hff")))

(define-public crate-memcache-async-0.6.0 (c (n "memcache-async") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "090vq6vxw89rs4b7c653y65714l0rwaqbzhima01cl20v5shg338")))

(define-public crate-memcache-async-0.6.1 (c (n "memcache-async") (v "0.6.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0q0vmmn8dp79s13z9svjnd44cbjrjq59gs0vifjgj2al14dpjk7g")))

(define-public crate-memcache-async-0.6.2 (c (n "memcache-async") (v "0.6.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "13i03d7x4x4mka3z5mkc6w6y5xk6l9qbi5yxppx3b280lw2d1m1z")))

(define-public crate-memcache-async-0.6.3 (c (n "memcache-async") (v "0.6.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0irn5zblfv6iwlx13pab813h7c727ha9r7f3d3bvmcpdmzc904l9")))

(define-public crate-memcache-async-0.6.4 (c (n "memcache-async") (v "0.6.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1rsfxacwkipjw97igizc5jgj5b632fp19kbln4vln08kd88198b0")))

(define-public crate-memcache-async-0.7.0 (c (n "memcache-async") (v "0.7.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1lk6a0aa8wll68hj0gndsa81fndnx2vk9hvlfc778xccpa39xzwa")))

