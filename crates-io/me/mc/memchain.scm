(define-module (crates-io me mc memchain) #:use-module (crates-io))

(define-public crate-memchain-0.1.0 (c (n "memchain") (v "0.1.0") (d (list (d (n "blockchain-traits") (r "^0.1") (d #t) (k 0)) (d (n "mantle-types") (r "^0.1") (d #t) (k 0)))) (h "1fklbm4vmdnz1vnlml48fxpvxg07k72z52xyrfyy04x9k68g91zx") (f (quote (("ffi"))))))

(define-public crate-memchain-0.2.0 (c (n "memchain") (v "0.2.0") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "mantle-types") (r "^0.2") (d #t) (k 0)))) (h "0xqpjqlxfvak4xwkkp0drrdj3s9w5mdcbiw28fb4k6c73yfv5bs6") (f (quote (("ffi"))))))

(define-public crate-memchain-0.2.1 (c (n "memchain") (v "0.2.1") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "oasis-types") (r "^0.2") (d #t) (k 0)))) (h "1l044px255rpdnalbvarkvpjlzmkk1w05z6wsz0yrzpcihmwwmhl") (f (quote (("ffi"))))))

(define-public crate-memchain-0.2.2 (c (n "memchain") (v "0.2.2") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "oasis-types") (r "^0.2") (d #t) (k 0)))) (h "07qyamjdvr44y5zirmk85dccba8d4pi9wzb0rmxa0z8hi23lfz4y") (f (quote (("ffi"))))))

(define-public crate-memchain-0.2.3 (c (n "memchain") (v "0.2.3") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "oasis-types") (r "^0.2") (d #t) (k 0)))) (h "05zl94fz9rkx4qpqyhivfq079x6pn2pxifbpna5s0j78fda6iwj3") (f (quote (("ffi"))))))

(define-public crate-memchain-0.2.4 (c (n "memchain") (v "0.2.4") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "oasis-types") (r "^0.3") (d #t) (k 0)))) (h "0vxlrr9b52914xdlq8iavv9m8yvs2069cf8zgck7rrg4gh4x1s15") (f (quote (("ffi"))))))

(define-public crate-memchain-0.3.0 (c (n "memchain") (v "0.3.0") (d (list (d (n "blockchain-traits") (r "^0.3") (d #t) (k 0)) (d (n "oasis-types") (r "^0.3") (d #t) (k 0)))) (h "0z1f3lpf0flpxi76r2a4y4nd4b73akwsdhm81jv5cn1rsczhi8wn") (f (quote (("ffi"))))))

(define-public crate-memchain-0.4.0 (c (n "memchain") (v "0.4.0") (d (list (d (n "blockchain-traits") (r "^0.4") (d #t) (k 0)) (d (n "oasis-types") (r "^0.3") (d #t) (k 0)))) (h "014szhaxb6mxkwxbzq6jqgvmj142407ngq8g87ah7crlpww53773") (f (quote (("ffi"))))))

(define-public crate-memchain-0.4.1 (c (n "memchain") (v "0.4.1") (d (list (d (n "blockchain-traits") (r "^0.4") (d #t) (k 0)) (d (n "oasis-types") (r "^0.4") (d #t) (k 0)))) (h "02s7371gbsv7im2z23sf3qdknxrc116y82mgi64gkdb8dygzin2h") (f (quote (("ffi"))))))

