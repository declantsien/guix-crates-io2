(define-module (crates-io me rf merfolk_frontend_logger) #:use-module (crates-io))

(define-public crate-merfolk_frontend_logger-0.1.0 (c (n "merfolk_frontend_logger") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.0-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "merfolk") (r "^0") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 2)) (d (n "wildmatch") (r "^1.0") (d #t) (k 0)))) (h "02bbb0ljajgrd1f1jjyqnq68a69rfllphxicwhdg1a5czr428rmb")))

