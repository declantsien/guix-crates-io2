(define-module (crates-io me rf merfolk_middleware_router) #:use-module (crates-io))

(define-public crate-merfolk_middleware_router-0.1.0 (c (n "merfolk_middleware_router") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.0-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "merfolk") (r "^0") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 2)))) (h "1aa9jxnms9cs75wjyghh9jm0wkdqhvsriqbv5cb72aynai54il7n")))

