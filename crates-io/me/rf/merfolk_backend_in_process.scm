(define-module (crates-io me rf merfolk_backend_in_process) #:use-module (crates-io))

(define-public crate-merfolk_backend_in_process-0.1.0 (c (n "merfolk_backend_in_process") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.0-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "merfolk") (r "^0") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "0l7iq4130k8bsqidblndp0bdk578wk6kwcajaaizxgjyc6bbm90c")))

