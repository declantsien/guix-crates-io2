(define-module (crates-io me rf merfolk_frontend_derive_macros) #:use-module (crates-io))

(define-public crate-merfolk_frontend_derive_macros-0.1.0 (c (n "merfolk_frontend_derive_macros") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.0-alpha") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ggayxxfzn2z4i4ppv2046rrd9if88a82sx9qqngaqdlds2jdrb2") (f (quote (("std" "log/std") ("default" "std"))))))

