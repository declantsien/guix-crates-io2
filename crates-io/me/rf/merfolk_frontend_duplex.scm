(define-module (crates-io me rf merfolk_frontend_duplex) #:use-module (crates-io))

(define-public crate-merfolk_frontend_duplex-0.1.0 (c (n "merfolk_frontend_duplex") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.0-alpha") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "merfolk") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (d #t) (k 2)))) (h "1ay45qnhkhwb3kc33yakh3djgvn9h90fadzxfrsn8qqpy8m33rpm") (f (quote (("std" "log/std" "merfolk/std") ("default" "std"))))))

