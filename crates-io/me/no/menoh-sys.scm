(define-module (crates-io me no menoh-sys) #:use-module (crates-io))

(define-public crate-menoh-sys-0.1.0 (c (n "menoh-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1k6lzs2rj0rnb86di75fi7wm8iy7ci5wryhxp8gdk93dhddp1zz9")))

(define-public crate-menoh-sys-0.1.1 (c (n "menoh-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1xdlx02ng2gypkv99yvv5x86sm6dlh6v0npip8l8l20ifl6b7432")))

(define-public crate-menoh-sys-0.1.2 (c (n "menoh-sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15hyp6gfz274qbfy2kdcb4mm545gjyb3a8hw4i62p3bbmg8d5za1")))

(define-public crate-menoh-sys-0.1.3 (c (n "menoh-sys") (v "0.1.3") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19dmx7d9x84bwxngnl99zy8081lf32qk1a2ja64k2m0mfqf1jn74")))

(define-public crate-menoh-sys-0.1.4 (c (n "menoh-sys") (v "0.1.4") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1b26a46941vpwsvc42ya8zq698y73zqby531a1rqcdrlhpsi1yn8")))

(define-public crate-menoh-sys-0.2.0 (c (n "menoh-sys") (v "0.2.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1afr298b31xw3852dj3ppb1fwgvc5f5fby2wjcyars2x8ihin19v")))

(define-public crate-menoh-sys-0.2.1 (c (n "menoh-sys") (v "0.2.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1f3xq2lr6g96i9afp7vxb6a3mr3a49qihf0xv7l9hkpjr0k6nq14")))

(define-public crate-menoh-sys-0.2.2 (c (n "menoh-sys") (v "0.2.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "019i152ab044h68xl7ifhhqr0pd9ic6a19c29n6jfysjn3w79pa9")))

