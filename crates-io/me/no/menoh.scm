(define-module (crates-io me no menoh) #:use-module (crates-io))

(define-public crate-menoh-0.1.0 (c (n "menoh") (v "0.1.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.19") (d #t) (k 2)) (d (n "menoh-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1gp0giixic63cwqgs4bzjk80phzsiy17is8lwr67qfxqmq3ifvas")))

(define-public crate-menoh-0.1.1 (c (n "menoh") (v "0.1.1") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.19") (d #t) (k 2)) (d (n "menoh-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "156fnwm9c3i2w1p8ig2hjyilpy3s45d66m2bz9pn0s6xbiz9vcm7")))

(define-public crate-menoh-0.1.2 (c (n "menoh") (v "0.1.2") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.19") (d #t) (k 2)) (d (n "menoh-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1i4jr0j5yivnyqcsiz2hri767ya2dmrfl6nmyr6301w0pas3wvhn")))

(define-public crate-menoh-0.1.3 (c (n "menoh") (v "0.1.3") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.19") (d #t) (k 2)) (d (n "menoh-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1gvbcg0fykvq02drry2w6k3daxk58ka92rac3m8gx8yxn6dwik8c")))

(define-public crate-menoh-0.1.4 (c (n "menoh") (v "0.1.4") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.19") (d #t) (k 2)) (d (n "menoh-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0ki4r77mgb83ffymfpc2kzy1xmz1v7ysz9rp0xfxmbd1wm38nk40")))

(define-public crate-menoh-0.2.0 (c (n "menoh") (v "0.2.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.20") (d #t) (k 2)) (d (n "menoh-sys") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ykish1zlbpfvz4bqcknx5dyhj48f7n32xgfg3wyn3k1ffkfs3gq")))

(define-public crate-menoh-0.2.1 (c (n "menoh") (v "0.2.1") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.21") (d #t) (k 2)) (d (n "menoh-sys") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1zh3ywkwsgr9fj614m0iij9ra3b9zz7ri864apypr7jw5yhhih1w")))

(define-public crate-menoh-0.2.2 (c (n "menoh") (v "0.2.2") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.21") (d #t) (k 2)) (d (n "menoh-sys") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1ddn1ln64gzj806x9w9xw0i9df2cp61fdqjjh9sk8arj8bbljh1v")))

