(define-module (crates-io me ml memlib) #:use-module (crates-io))

(define-public crate-memlib-0.1.0 (c (n "memlib") (v "0.1.0") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "dataview") (r "^0.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (o #t) (d #t) (k 0)))) (h "0fwx53g96gqj4n278rlrrby9w70by9in43rci7cpg8rngwn5k030") (f (quote (("test" "env_logger" "log") ("render") ("kernel"))))))

(define-public crate-memlib-0.1.1 (c (n "memlib") (v "0.1.1") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "dataview") (r "^0.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (o #t) (d #t) (k 0)))) (h "1d7fxmpiwsxl8yi9k9pmzq85mpijz96ffhjb3cczrd3yffncd05l") (f (quote (("test" "env_logger" "log") ("render") ("kernel"))))))

(define-public crate-memlib-0.1.2 (c (n "memlib") (v "0.1.2") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "dataview") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (o #t) (d #t) (k 0)))) (h "1p7wnsi3mvcw082ci5c04nznhrgbpyg8ybiw87ad50kh4hff3ykv") (f (quote (("test" "env_logger" "log") ("render") ("kernel"))))))

(define-public crate-memlib-0.1.3 (c (n "memlib") (v "0.1.3") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "dataview") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (o #t) (d #t) (k 0)))) (h "00z71zrbdw6cir2cbqkzyxbj1wls4rii2496kj6snavvpq2d7346") (f (quote (("test" "env_logger" "log") ("render") ("kernel"))))))

