(define-module (crates-io me nd mendia-types) #:use-module (crates-io))

(define-public crate-mendia-types-1.0.0 (c (n "mendia-types") (v "1.0.0") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ts-rs") (r "^6.2") (d #t) (k 0)))) (h "08vsfmdjn7drpnrbj1l3anv2lajga50nmx064hj5na3zcj7gj4m8")))

(define-public crate-mendia-types-1.1.1 (c (n "mendia-types") (v "1.1.1") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ts-rs") (r "^6.2") (d #t) (k 0)))) (h "1843krkybcykwgbmal35clja37xfyq520c2b4ilw2wjdsbicxzgg")))

(define-public crate-mendia-types-1.2.0 (c (n "mendia-types") (v "1.2.0") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ts-rs") (r "^6.2") (d #t) (k 0)))) (h "1xp1656hz744w9xa79j0avgavq7jvp4cacs8nn9zm7zrsd8jjwmx")))

(define-public crate-mendia-types-1.2.1 (c (n "mendia-types") (v "1.2.1") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ts-rs") (r "^6.2") (d #t) (k 0)))) (h "0lziqvp8kka000x7k53k66w8x8ir6gfs700hmdr4as0s3hb9y56v")))

(define-public crate-mendia-types-1.2.2 (c (n "mendia-types") (v "1.2.2") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ts-rs") (r "^6.2") (d #t) (k 0)))) (h "1ma4h7183chyxjkzsrz1wpc5ad1r0gpm9xxvdzv0qg4dkzkn98mv")))

(define-public crate-mendia-types-1.3.0 (c (n "mendia-types") (v "1.3.0") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ts-rs") (r "^6.2") (d #t) (k 0)))) (h "0v32aa4g20q5s3j4rzpzyic8l65rg3kq755mwgb599bq6nkrpv8r")))

