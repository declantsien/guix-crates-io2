(define-module (crates-io me nd mendes-derive) #:use-module (crates-io))

(define-public crate-mendes-derive-0.0.1 (c (n "mendes-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rqa10xdj9dnl1aqcygfd9r6ddqvjf8q0sjxgpmi9sk83rsj1w23")))

(define-public crate-mendes-derive-0.0.2 (c (n "mendes-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dinx3fmd7nmbk7wmrz4a13nd3wlnkq6r6kf46bwcr411gbahbpb")))

(define-public crate-mendes-derive-0.0.3 (c (n "mendes-derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "082j5f029zlfgyzgbjl21sk39n1ghb0zgviqzl5bdqw7m0ryb94w")))

(define-public crate-mendes-derive-0.0.4 (c (n "mendes-derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16mj57d52czf2w21885rbyazrpfs0f3qs0f00s9lhcy3rjasxnr4")))

(define-public crate-mendes-derive-0.0.5 (c (n "mendes-derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ji3700kyy45i00pmzsjff8n2iya3d56vivfrpl4jfkk2akfxfxq")))

(define-public crate-mendes-derive-0.0.6 (c (n "mendes-derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14brvk41b8781bhh3q349dpri8kx0iwb2va3js91lvzs26s9mhr9")))

