(define-module (crates-io me nd mendia-db) #:use-module (crates-io))

(define-public crate-mendia-db-1.0.0 (c (n "mendia-db") (v "1.0.0") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0khxn7gbrzhd0s1i8ricd0yp6wlnsqlgaxrzzmijpkshf3phl76g")))

(define-public crate-mendia-db-1.1.0 (c (n "mendia-db") (v "1.1.0") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "18xh7s52zh3mwnbmaikcdd385yhwqhga1q0xqbl1jlzlg7ndvwp1")))

(define-public crate-mendia-db-1.1.1 (c (n "mendia-db") (v "1.1.1") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1514mbxzwczck3alxd5p0sysn9s4drpdzp4wxm8hj68n90m299x0")))

(define-public crate-mendia-db-1.2.0 (c (n "mendia-db") (v "1.2.0") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1pyznwap33daqv73dl6sg5wclwx2jfj3j1p4ick3lj92zncbygb1")))

(define-public crate-mendia-db-1.2.1 (c (n "mendia-db") (v "1.2.1") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "094rkfaizjd0aaads5s6mmiy7imhm8m2p6vgj94x4cf6fnlliy90")))

(define-public crate-mendia-db-1.2.2 (c (n "mendia-db") (v "1.2.2") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "14jy0yjsq95xpc2mlp48sxf6fj2r1wvvm1mrkzc483pabxxz747b")))

(define-public crate-mendia-db-1.3.0 (c (n "mendia-db") (v "1.3.0") (d (list (d (n "diesel") (r "^2.0.2") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1ygsgjja4l3f4ahamvkd9z94kz9rdlzgxnjshglpvx8pmgdjf9g3")))

