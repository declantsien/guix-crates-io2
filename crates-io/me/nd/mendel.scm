(define-module (crates-io me nd mendel) #:use-module (crates-io))

(define-public crate-mendel-0.0.1 (c (n "mendel") (v "0.0.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0j417pf8d69xgbncc9gibic63khkji932i0jd89a6gkgp4k0daw4")))

(define-public crate-mendel-0.0.2 (c (n "mendel") (v "0.0.2") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1ynja6hka2npig4mvx4f1277a37hrinskn0hx92msbvy7rbi2r4b")))

