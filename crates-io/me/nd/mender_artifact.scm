(define-module (crates-io me nd mender_artifact) #:use-module (crates-io))

(define-public crate-mender_artifact-0.1.1 (c (n "mender_artifact") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0nvcjkj957iyymi5fjys6831hhy176cbszzjwh7ass540ahix9rm")))

(define-public crate-mender_artifact-0.1.2 (c (n "mender_artifact") (v "0.1.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "1f2f34s24lcd25dcgkas8dyv1mzrk3q2b7l3pqzqgwr41y6m3rdv")))

(define-public crate-mender_artifact-0.1.3 (c (n "mender_artifact") (v "0.1.3") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "12pchmzddd55bpb1jf6pag1nd3sd1x7xdmc584hda3h2vpgd661b")))

