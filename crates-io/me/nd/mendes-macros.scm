(define-module (crates-io me nd mendes-macros) #:use-module (crates-io))

(define-public crate-mendes-macros-0.0.7 (c (n "mendes-macros") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17binx93zhg4nm13xzqi2x26dqvfkqadvjs009rky86s0fdzi9vw")))

(define-public crate-mendes-macros-0.0.8 (c (n "mendes-macros") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1clx9xxafb9302jlwbil5j5y6bsaq6smv25ismcrlvjxzp7hkhaf")))

(define-public crate-mendes-macros-0.0.9 (c (n "mendes-macros") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15lqsfdrb1v3lk4xywq0bfbl9zdmfkf9n5a5xwr3m2dw7asy6cfk")))

(define-public crate-mendes-macros-0.0.10 (c (n "mendes-macros") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1h0z5yzn3abj3y5qbi1idnbk82dhx6z0k8h3x3d1sj8pw95pvbcf")))

(define-public crate-mendes-macros-0.0.11 (c (n "mendes-macros") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0a1186anras78z6294yvhj0vc6523bm24qh5l363z3gy04w5qw1d")))

(define-public crate-mendes-macros-0.0.12 (c (n "mendes-macros") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1xy5zgmljh0f7zwwh1pvm71ww7ddv5xjcr2gkdf3d669d43zsm29")))

(define-public crate-mendes-macros-0.0.13 (c (n "mendes-macros") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0wyfm9jhklnfn1iaf7sz5mvj0ycwzrszl4034kzj14x9qmyvyyl1")))

(define-public crate-mendes-macros-0.0.14 (c (n "mendes-macros") (v "0.0.14") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0jbc3l2yj1bqcdank3qvx0kw0gg1fxwaf8j5gkyygsywppbx6cyi")))

(define-public crate-mendes-macros-0.0.15 (c (n "mendes-macros") (v "0.0.15") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1lf1ij8li1kn0yx485hp8h6pknslkwgqmy34vrrr6vaw9yfrdcja")))

(define-public crate-mendes-macros-0.0.16 (c (n "mendes-macros") (v "0.0.16") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1mczi6bhijsajqay15klrvf6wr8czgilgrxhza4zvv9lmwsh8rm3")))

(define-public crate-mendes-macros-0.0.17 (c (n "mendes-macros") (v "0.0.17") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1h432g4g9nxlw208ijj58dbivgjp5g4q4zkh9psj2xw07icn3h2r")))

(define-public crate-mendes-macros-0.0.18 (c (n "mendes-macros") (v "0.0.18") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "138k64j8970wcdhhyz2v4yqavbbk9qbw8y46ha6dn33h1gw77hxr")))

(define-public crate-mendes-macros-0.0.19 (c (n "mendes-macros") (v "0.0.19") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0sds2y41hl4k3rglfivpzyh7qlkprk1cclvr5xgp2i7sw9si9kvh")))

(define-public crate-mendes-macros-0.0.20 (c (n "mendes-macros") (v "0.0.20") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1rixiz9plvjvzmwgip7gcw3ylfbzv89hs3f3xzsviv0c663wdg4k")))

(define-public crate-mendes-macros-0.0.21 (c (n "mendes-macros") (v "0.0.21") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "04x84c2ikzknd9qb5fcqvm7hjsk9qh774gbybmbl6qmm1cf2q1f8")))

(define-public crate-mendes-macros-0.0.22 (c (n "mendes-macros") (v "0.0.22") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0kd0yl1fh3id80q2579j5cs1plrml956pvs5c2g0055kjfl9yk3d")))

(define-public crate-mendes-macros-0.0.23 (c (n "mendes-macros") (v "0.0.23") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0kgg9chmr8bfa9as7hb6ab7zzdrimzgz34r3ifgn4rjyzn5m0fh3")))

(define-public crate-mendes-macros-0.0.24 (c (n "mendes-macros") (v "0.0.24") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1gj59nqza9150hvhy7djqwy5fchf6sn8rvdznikb5nk7q0b373bq")))

(define-public crate-mendes-macros-0.0.25 (c (n "mendes-macros") (v "0.0.25") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0cf9zn29qpaylnjhg51cjw54c2hjmgq5fihxrcmqz3irxi3p90mz")))

(define-public crate-mendes-macros-0.0.26 (c (n "mendes-macros") (v "0.0.26") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0mkvhz7cjwn3fq56ll7agpbil07znfn5k9vyrvdx5v7m784b0hps")))

(define-public crate-mendes-macros-0.0.27 (c (n "mendes-macros") (v "0.0.27") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14jsc2i5x1zwpvgjsy376zy4hpasphjwbh91ympqccgf70mcmq0c")))

(define-public crate-mendes-macros-0.0.28 (c (n "mendes-macros") (v "0.0.28") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w7cdg7gv1pv68p6d1mqdmv5x65hn9m7mxhvamg5dh0c3k2v4wvq")))

(define-public crate-mendes-macros-0.0.29 (c (n "mendes-macros") (v "0.0.29") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "06dqvdx9im1xpk7n1g88dap7034l69hzmvdlfqig23jc943sby34")))

(define-public crate-mendes-macros-0.0.30 (c (n "mendes-macros") (v "0.0.30") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0fp04gqgh5vjjmj7rs0dhahqrjbyajgdmk9rlvgcf1flxi4z0rq1")))

(define-public crate-mendes-macros-0.0.31 (c (n "mendes-macros") (v "0.0.31") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "151pkj48v2g88jnh6alfr3g1p7j8a1dnj398kvk8g90y79sxv54x")))

(define-public crate-mendes-macros-0.0.32 (c (n "mendes-macros") (v "0.0.32") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1ah4pqa14x4ba63f3xb4kac5r41i6d64npw0jfgh0ygphwpyjdb4")))

(define-public crate-mendes-macros-0.0.33 (c (n "mendes-macros") (v "0.0.33") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1n2wi08ymyc2czm6gg8jn0mhijdw1r2lm0isizbxksf1cwj44wgn")))

(define-public crate-mendes-macros-0.0.34 (c (n "mendes-macros") (v "0.0.34") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1hjgf89s96qnam20ad8wpbhdxq7142alkq93wbmaqqhdsxwk3nva")))

(define-public crate-mendes-macros-0.0.35 (c (n "mendes-macros") (v "0.0.35") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p5s0mhzgy0hhnjjcr4jb5q7yxvqfvkggh8bidp2lvlj0yzga5b1")))

(define-public crate-mendes-macros-0.0.36 (c (n "mendes-macros") (v "0.0.36") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0wb42kbmkmw31inyf2lm5wn98z758389dfbxn9jxdjmc10s4l9q6")))

(define-public crate-mendes-macros-0.0.37 (c (n "mendes-macros") (v "0.0.37") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0dgn8qdfl32vhxy68hy28s0hr4svi9v7x00hwnwpnli8g4nrhzn0")))

(define-public crate-mendes-macros-0.0.38 (c (n "mendes-macros") (v "0.0.38") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0sg6wlk8w9qwpqxl9cbmbah9qqssfhl8wycy6pj075y47fmj1gz5")))

(define-public crate-mendes-macros-0.0.39 (c (n "mendes-macros") (v "0.0.39") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1fwhmpvnr2nmwc5xwqx431vs0naybaw5hjfnrk8mll8rdc1z3i19")))

(define-public crate-mendes-macros-0.0.40 (c (n "mendes-macros") (v "0.0.40") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1ib7qa55zjrl6bgwvqjyn21sqc16ldlcxxh9xa15cz1irzblc4hy")))

(define-public crate-mendes-macros-0.0.41 (c (n "mendes-macros") (v "0.0.41") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1d9bbis3x4j3nzxi70naajk18v8xzs2d873h14hcbqklv1s3dadp")))

(define-public crate-mendes-macros-0.0.42 (c (n "mendes-macros") (v "0.0.42") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0hasf70vyaycg0dn7lw96sli30ssib1d0rypvsqjlp6pxv3cnyc2")))

(define-public crate-mendes-macros-0.1.0 (c (n "mendes-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0phlzkdfq878gkfyd2lhqn9ng247xxxcaj9qw2prw6zniwa77wc8")))

(define-public crate-mendes-macros-0.1.1 (c (n "mendes-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1kf03j05ci08v59daij4m8xlb0kbjyc6nxw9smxzbm4cwr8ikr0i")))

(define-public crate-mendes-macros-0.2.0 (c (n "mendes-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1i528f9g8zpz7j1l4nc4d5dfdra7f759rdkiwzpg9zkhg8r4gyxz") (r "1.58")))

(define-public crate-mendes-macros-0.2.1 (c (n "mendes-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ww7npl3j05891z87xpzw4m28rz9y74m7n04yy2q9hhc9fksgndf") (r "1.60")))

(define-public crate-mendes-macros-0.3.0 (c (n "mendes-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1359j0zv25cg9vwfx8hqk3x1pv4flh0dyd9gpkrycxsw1g67zpif") (r "1.63")))

(define-public crate-mendes-macros-0.4.0 (c (n "mendes-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gn8ck9irdi8gka39y2zc3xfj5rb0194c9mgp5xzc5cihxlhzwkz") (r "1.63")))

