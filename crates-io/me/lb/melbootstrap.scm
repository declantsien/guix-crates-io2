(define-module (crates-io me lb melbootstrap) #:use-module (crates-io))

(define-public crate-melbootstrap-0.8.0 (c (n "melbootstrap") (v "0.8.0") (d (list (d (n "melstructs") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1702wss5fa9sg12ix5p5hachlrlwl9kqfp612swc58nkir9n1qvq")))

(define-public crate-melbootstrap-0.8.1 (c (n "melbootstrap") (v "0.8.1") (d (list (d (n "melstructs") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1qf79gphiv25rlzpwsfapnzcx2s3zhp020x2xg2cazzrzbpq9qca")))

(define-public crate-melbootstrap-0.8.2 (c (n "melbootstrap") (v "0.8.2") (d (list (d (n "melstructs") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0lfilx68vln55n1dr8x6wda1a9hjmiqgz1jh5zaybc02v6zd6ma0")))

(define-public crate-melbootstrap-0.8.3 (c (n "melbootstrap") (v "0.8.3") (d (list (d (n "melstructs") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "09hs26g2pnk2ksxnikqsgbb31shxfnaw9gza6y1rc8zffv3b9jiw")))

(define-public crate-melbootstrap-0.8.4 (c (n "melbootstrap") (v "0.8.4") (d (list (d (n "melstructs") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "12k69hhm4yvg81fvw3qz5y7lv7vx5bvx18v70yjb4kka9cz8sdi4")))

(define-public crate-melbootstrap-0.8.5 (c (n "melbootstrap") (v "0.8.5") (d (list (d (n "melstructs") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "12r2i81z2lw07xbjrdsyrfiz4dzr0ip1b8758zjw66i3cp91lln0")))

