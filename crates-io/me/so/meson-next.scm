(define-module (crates-io me so meson-next) #:use-module (crates-io))

(define-public crate-meson-next-1.0.0 (c (n "meson-next") (v "1.0.0") (h "1db5a989v9frbari1p8xy6m9nh52s0d2wzbkrffv5hvyc8k9pifi")))

(define-public crate-meson-next-1.0.1 (c (n "meson-next") (v "1.0.1") (h "0pvhvv2ipclhiwia9visqwvc9xdm1nhmxfzwv57a1jwm6b6hi16f")))

(define-public crate-meson-next-1.1.0 (c (n "meson-next") (v "1.1.0") (h "1l3sy54yyi0946b5srs07xw5r2anbzz3qbqymcwji0mll4sqbjkh")))

(define-public crate-meson-next-1.1.1 (c (n "meson-next") (v "1.1.1") (h "1lmvblqw16ni6pa6fgdacvfhpjbapdxvjisjf3apzjrn62dsxzja")))

(define-public crate-meson-next-1.1.2 (c (n "meson-next") (v "1.1.2") (h "1bpglhjamwm7j1i3q6r6033x9vl1czz6h88807a24kcxw4x9l5hs")))

(define-public crate-meson-next-1.1.3 (c (n "meson-next") (v "1.1.3") (h "1bn0f9zql9y8lsmaxaf6l2bi5z3yvqmmkdqfq8v78w702y1awzi8")))

(define-public crate-meson-next-1.2.0 (c (n "meson-next") (v "1.2.0") (h "1pp3jviby4s341f3qnzych3j50w7z6zlwghypvz3n4f2dilmrj4n")))

(define-public crate-meson-next-1.2.1 (c (n "meson-next") (v "1.2.1") (h "1gic2k1180k6v0xs9r20j1s197a8y2gr2dgmvgiwhfnp6jag9ang")))

(define-public crate-meson-next-1.2.2 (c (n "meson-next") (v "1.2.2") (h "1q6jksxa90masdxwmkrc75jf1kfifpdx1zixywf9i0g5z9inib20")))

