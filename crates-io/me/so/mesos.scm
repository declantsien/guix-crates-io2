(define-module (crates-io me so mesos) #:use-module (crates-io))

(define-public crate-mesos-0.2.3 (c (n "mesos") (v "0.2.3") (d (list (d (n "hyper") (r "^0.6.15") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.4") (d #t) (k 0)))) (h "14qd7fh744z09360ckii53mg36zma2rghw8di3gz6wnb7f7kznp8")))

(define-public crate-mesos-0.2.4 (c (n "mesos") (v "0.2.4") (d (list (d (n "hyper") (r "^0.6.15") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.4") (d #t) (k 0)))) (h "0g8irqpwn0q34jl8kj1in3bghc80k3smswcyb0779cmmziqdya4j")))

(define-public crate-mesos-0.2.5 (c (n "mesos") (v "0.2.5") (d (list (d (n "hyper") (r "^0.6.15") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.4") (d #t) (k 0)))) (h "050ak8y8qfpaap9x9qn20ng6g1x6kski4bgk1nn1hxy5kaysipyb")))

(define-public crate-mesos-0.2.6 (c (n "mesos") (v "0.2.6") (d (list (d (n "hyper") (r "^0.6.15") (d #t) (k 0)) (d (n "itertools") (r "^0.4.3") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.4") (d #t) (k 0)))) (h "1r0gic0iv5brzdy16yxmx3kfv47x19q77jdrvprvzij9wlihcj69")))

(define-public crate-mesos-0.2.8 (c (n "mesos") (v "0.2.8") (d (list (d (n "hyper") (r "^0.6.15") (d #t) (k 0)) (d (n "itertools") (r "^0.4.3") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.4") (d #t) (k 0)))) (h "1qzxbqlck6iaisiqq35ps2y0hwz2l64m4ab9gafw2rfb77hxym0l")))

(define-public crate-mesos-0.2.9 (c (n "mesos") (v "0.2.9") (d (list (d (n "hyper") (r "^0.6.15") (d #t) (k 0)) (d (n "itertools") (r "^0.4.3") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.4") (d #t) (k 0)))) (h "1clfx914hp77rlpd28fpqnjl0g2xj2pjkk171a4xla5pmas41chh")))

(define-public crate-mesos-0.2.10 (c (n "mesos") (v "0.2.10") (d (list (d (n "hyper") (r "^0.7.1") (d #t) (k 0)) (d (n "itertools") (r "^0.4.3") (d #t) (k 0)) (d (n "protobuf") (r "^1.0.10") (d #t) (k 0)))) (h "03lpqa5js3m4yg55y6i7njsl2v3842l4ajk6cn79gk1h95j6bx5b")))

