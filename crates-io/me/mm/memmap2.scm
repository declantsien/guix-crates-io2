(define-module (crates-io me mm memmap2) #:use-module (crates-io))

(define-public crate-memmap2-0.1.0 (c (n "memmap2") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0nmymqy9q62x577ydja0ysfyir7h5qa0n5fwcnvchfhhlsi0rdyr")))

(define-public crate-memmap2-0.2.0 (c (n "memmap2") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1vjf35vzg5368b7c0qh3pimikivc286xb8gy6glj60aas2vy6fz7")))

(define-public crate-memmap2-0.2.1 (c (n "memmap2") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1imwbxrwa9cpjjsgg9sdql8iq1c7813r41fpwqm0wr8djxdyiqq4")))

(define-public crate-memmap2-0.2.2 (c (n "memmap2") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1panpy15vy2pf86f8a78ypnq9izn5ikaxg9b8vsw0qq5dmnilz9r")))

(define-public crate-memmap2-0.2.3 (c (n "memmap2") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1d26q3sh0z5dvwpilqqvq5bqi7vqfd2kc5gk3pdj7h65rnykwgkj")))

(define-public crate-memmap2-0.3.0 (c (n "memmap2") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1xcg3vv6rg8vhl0wdfy085gx4xsp2dah7anvn5816h6wgczj1zr0")))

(define-public crate-memmap2-0.3.1 (c (n "memmap2") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0mz3fdcq443m3a1phrhp2yvd1h9vrvbhinzmi23ik031zzmw5dh0")))

(define-public crate-memmap2-0.4.0 (c (n "memmap2") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1y7cn4mq9cfm6gsxnpbr47569s71nymaw6q8c3jqrmc0q0932pfy")))

(define-public crate-memmap2-0.5.0 (c (n "memmap2") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0vp9pxd20gyq8196v73chxdw6gfxz3g4lkdkvffd5slgawds2is6")))

(define-public crate-memmap2-0.5.1 (c (n "memmap2") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0x968fccyfpa65jf5ysx9h6pnfld2rhz2drmgmv8vr1xzgj9b2wy")))

(define-public crate-memmap2-0.5.2 (c (n "memmap2") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0vf5h5mdarn3zq5mz4hvahph18a5bsvsvsyb8x2b3n0zbsw7jcgy")))

(define-public crate-memmap2-0.5.3 (c (n "memmap2") (v "0.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "13y09b9fgyfsj3zkv62yd01wp3spdaqgwnbs3a16gj4r76r3syh5")))

(define-public crate-memmap2-0.5.4 (c (n "memmap2") (v "0.5.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1bjqvnpd3rgc606l2flgbarxj6b5yf917rakvm1zyhrhq982n5ym")))

(define-public crate-memmap2-0.5.5 (c (n "memmap2") (v "0.5.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1irzc4skrr8a2i0x9kdisq59w6rzyigkp8prx9za59d7jffb6y9s")))

(define-public crate-memmap2-0.5.6 (c (n "memmap2") (v "0.5.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0xr5s80aswaskdh2amrhllvadwrh9cii03bzwz2yh410pral8blf")))

(define-public crate-memmap2-0.5.7 (c (n "memmap2") (v "0.5.7") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "163lwxgmjdsnjdxjfd8vxj66ydxwp07himpar3pz4ymi8pribbwm")))

(define-public crate-memmap2-0.5.8 (c (n "memmap2") (v "0.5.8") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1p61bhsr16zny2rlfpkn6bpc6d1vk35ah76fqh3xh64balr2662b")))

(define-public crate-memmap2-0.5.9 (c (n "memmap2") (v "0.5.9") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1hjy5dl1v1dj8d7mj918lrwa69vwi8zad742wdzsclp5fm9wdwia")))

(define-public crate-memmap2-0.5.10 (c (n "memmap2") (v "0.5.10") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "09xk415fxyl4a9pgby4im1v2gqlb5lixpm99dczkk30718na9yl3")))

(define-public crate-memmap2-0.6.0 (c (n "memmap2") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "14qnambl8cvhw7260nvbfzdfg5mr8maz8mgfl7y4bipx5qnz17vz")))

(define-public crate-memmap2-0.6.1 (c (n "memmap2") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "15k0rn1aks4xbvq038l2z1j8x8qpkakvdckx07dhmjzfb981pam0")))

(define-public crate-memmap2-0.6.2 (c (n "memmap2") (v "0.6.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0wm8avdjma6j3x5fjdqwxcj89h52pzmwanw46xkn9rnz9albna3d")))

(define-public crate-memmap2-0.7.0 (c (n "memmap2") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.143") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gbwkq39madagz43ykd9w5fzz9zcw4mbvgyis6937lw3pqsln38q")))

(define-public crate-memmap2-0.7.1 (c (n "memmap2") (v "0.7.1") (d (list (d (n "libc") (r "^0.2.143") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1il82b0mw304jlwvl0m89aa8bj5dgmm3vbb0jg8lqlrk0p98i4zl")))

(define-public crate-memmap2-0.8.0 (c (n "memmap2") (v "0.8.0") (d (list (d (n "libc") (r "^0.2.143") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1vf3djv9s917fbvw5vclllpl22g12iph6cz11gn57ndhxwya19a3")))

(define-public crate-memmap2-0.9.0 (c (n "memmap2") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.143") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0xckkh1i45g6y2g2lkb6b292pfj2wlrfk2fc4754q7dzga6s7ayy")))

(define-public crate-memmap2-0.9.1 (c (n "memmap2") (v "0.9.1") (d (list (d (n "libc") (r "^0.2.143") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0pmq5jq3835i30vlwap81d95jkff2701bv843fxjn0j1mxbh31cg") (y #t)))

(define-public crate-memmap2-0.9.2 (c (n "memmap2") (v "0.9.2") (d (list (d (n "libc") (r "^0.2.143") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1zr2sqsr7694p4gyap3j0xdd2a6a1a165nh363w1ir4s31y9r9ir") (y #t)))

(define-public crate-memmap2-0.9.3 (c (n "memmap2") (v "0.9.3") (d (list (d (n "libc") (r "^0.2.143") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "14kwkwh1cr790dhrdwzyjxp2f5k1jp7w1swc7z38py0vhdbkmza5")))

(define-public crate-memmap2-0.9.4 (c (n "memmap2") (v "0.9.4") (d (list (d (n "libc") (r "^0.2.151") (d #t) (t "cfg(unix)") (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "08hkmvri44j6h14lyq4yw5ipsp91a9jacgiww4bs9jm8whi18xgy")))

