(define-module (crates-io me mm memmap) #:use-module (crates-io))

(define-public crate-memmap-0.1.0 (c (n "memmap") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "07ypzw7w21js696hnbf4dpk1hdpawf71f3v2a0ancryyip3ay91j")))

(define-public crate-memmap-0.2.0 (c (n "memmap") (v "0.2.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "fs2") (r "*") (d #t) (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0f7h4l1arblj0qqws9j8f83yx6wamrka8icid3pc3k0xqq7lk9xn")))

(define-public crate-memmap-0.2.1 (c (n "memmap") (v "0.2.1") (d (list (d (n "fs2") (r "*") (d #t) (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0iviqlwdk7jxdfxd4s868s7hc3257pm60k334wj12y1mkb9ix0k2")))

(define-public crate-memmap-0.2.2 (c (n "memmap") (v "0.2.2") (d (list (d (n "fs2") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "11dxld7kid7krgwnw38mjvx823mq9q26a47kp5krl3k8nj3s2bdf")))

(define-public crate-memmap-0.2.3 (c (n "memmap") (v "0.2.3") (d (list (d (n "fs2") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0li737lakqcbbgd87x7h8d4vp0r1fqcbn5lb5vi746i9jgnp43zj")))

(define-public crate-memmap-0.3.0 (c (n "memmap") (v "0.3.0") (d (list (d (n "fs2") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1lrfpdvnj6aqjdmlrxr6w9za9qkb44q0p9m479xlmgxvxbbijg4v")))

(define-public crate-memmap-0.4.0 (c (n "memmap") (v "0.4.0") (d (list (d (n "fs2") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1skgf49pasjqkq639frhg4fjpz039blxpscgx9ahh1qhm8j349b9")))

(define-public crate-memmap-0.5.0 (c (n "memmap") (v "0.5.0") (d (list (d (n "fs2") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1lmbkx92xlqb5yqh4cj2k4qfrm3x4jkbs04i84nfl60wyfdfap06")))

(define-public crate-memmap-0.5.1 (c (n "memmap") (v "0.5.1") (d (list (d (n "fs2") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "058y1raj9578wdsjbj38j4sycs93ay0xiy51z2k876j0895dgg4f")))

(define-public crate-memmap-0.5.2 (c (n "memmap") (v "0.5.2") (d (list (d (n "fs2") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "16y9h084k6vmrs125jkx7fi91znx8x84xbkx52cikcr8j0swgws6")))

(define-public crate-memmap-0.6.0 (c (n "memmap") (v "0.6.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1nc655dn4m71s26cakzvbz0wwirfhb2vf6yzw4qw2vjnh563lnd1")))

(define-public crate-memmap-0.6.1 (c (n "memmap") (v "0.6.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0q140kvlfyin4fhqfwji666j85csz1dxpp3lkfdlw5nq4b5h1014")))

(define-public crate-memmap-0.6.2 (c (n "memmap") (v "0.6.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "minwindef" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zy6s0ni0lx9rjzq3gq2zz9r8zgjmbp02332g3gsj4fyhv4s5zz2")))

(define-public crate-memmap-0.7.0 (c (n "memmap") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "minwindef" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ns7kkd1h4pijdkwfvw4qlbbmqmlmzwlq3g2676dcl5vwyazv1b5")))

