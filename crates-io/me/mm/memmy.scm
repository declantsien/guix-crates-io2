(define-module (crates-io me mm memmy) #:use-module (crates-io))

(define-public crate-memmy-0.1.0 (c (n "memmy") (v "0.1.0") (d (list (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_Foundation" "Win32_System_SystemServices" "Win32_UI_WindowsAndMessaging" "Win32_System_Console" "Win32_System_Threading" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_LibraryLoader" "Win32_System_ProcessStatus"))) (d #t) (k 0)))) (h "1cf8jy2ksz0ypvfli5v09ri1r0cym7pphwcgc7h854lmssqizspq")))

(define-public crate-memmy-0.2.0 (c (n "memmy") (v "0.2.0") (d (list (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_UI_WindowsAndMessaging" "Win32_System_Console" "Win32_System_Threading" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_LibraryLoader" "Win32_System_ProcessStatus" "Win32_System_SystemInformation" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)))) (h "0nh4pxqgdqsdd2bx4qy6zlrrq3n0hgcwf8vr0ahviaxsgmkryqis")))

