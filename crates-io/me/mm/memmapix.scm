(define-module (crates-io me mm memmapix) #:use-module (crates-io))

(define-public crate-memmapix-0.6.0 (c (n "memmapix") (v "0.6.0") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.35") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1q6nydm2blrnjkc91c2r7fii6q2wgv4lx4shwqi8i8q9lgbs3fm2")))

(define-public crate-memmapix-0.6.1 (c (n "memmapix") (v "0.6.1") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.35") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1yynzran3safa9cx6zr8bwl9y1zzvim0956ml69rb08xwk2qm1im")))

(define-public crate-memmapix-0.6.2 (c (n "memmapix") (v "0.6.2") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.35") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0pr78vcrmspg3zrrxinr7mkppirp5mzkqd1m8cry96q4bc57d3xs")))

(define-public crate-memmapix-0.6.3 (c (n "memmapix") (v "0.6.3") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.35") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1lazdrhbzrrrabnk05l7186vzbh0p4jw5s4yinj8ns4369hal85a")))

(define-public crate-memmapix-0.7.0 (c (n "memmapix") (v "0.7.0") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.37") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "13g0xjbsz81hmf2j570gw1i742lsi566vl70rrp4sva7g8zdiqpw") (y #t)))

(define-public crate-memmapix-0.7.1 (c (n "memmapix") (v "0.7.1") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.37") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1qh1zd20qf2j8hmifzz1jc6wq1dy7fqznia92llfsbkndf4y5r84") (y #t)))

(define-public crate-memmapix-0.7.2 (c (n "memmapix") (v "0.7.2") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.37") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ammzldai4d2rysmlwi1hnjmc8wpfpmcl5p82dyyfjhgsrnvihqb") (y #t)))

(define-public crate-memmapix-0.7.3 (c (n "memmapix") (v "0.7.3") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.37") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1h7r97f1hsmxqy38mv8c8n3mxalqiq2hsrrjciyls9id6pqjkpyg") (y #t)))

(define-public crate-memmapix-0.7.4 (c (n "memmapix") (v "0.7.4") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.38") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0syqdpzkdwmabpvnkhfcg1wjamj7z0a5lc5i0i6hih4bgd10185r") (y #t)))

(define-public crate-memmapix-0.7.5 (c (n "memmapix") (v "0.7.5") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.38") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0yxhdlwmqw44hrrfjcmv7iz8c9vz77k8jaa23fwzip253gzy2bna") (y #t)))

(define-public crate-memmapix-0.7.6 (c (n "memmapix") (v "0.7.6") (d (list (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "rustix") (r "^0.38") (f (quote ("fs" "mm" "param"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1lx1q6fdx59fsy266g4bjnip52nhbdadk16jbdsz3m95890sn5zm")))

