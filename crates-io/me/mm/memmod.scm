(define-module (crates-io me mm memmod) #:use-module (crates-io))

(define-public crate-memmod-1.2.1 (c (n "memmod") (v "1.2.1") (d (list (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "1a8ybqplk9cvsscrsj095iyjsp0lkq9saspzsv9ga5hkgyjcyi1w")))

(define-public crate-memmod-1.2.2 (c (n "memmod") (v "1.2.2") (d (list (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "1sdajzpnq5jwf5rmi3zyym8h0frnr2k8wrfa41lib19wirgn3ysr")))

(define-public crate-memmod-1.3.0 (c (n "memmod") (v "1.3.0") (d (list (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "165v8hgsgh300k1asfd059bjz35xp0nwwqdinz5d32acr6kjxddx")))

(define-public crate-memmod-1.3.1 (c (n "memmod") (v "1.3.1") (d (list (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "1g5gfgbp2wl0b72n5asf8kav755j211knxmyfn3x55ffjd0bf0vw")))

(define-public crate-memmod-1.4.0 (c (n "memmod") (v "1.4.0") (d (list (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "08mjbgx4xmqwqsc67hw58i56b5i7icdm3bzvsj3lffn4j95njm45")))

(define-public crate-memmod-1.4.1 (c (n "memmod") (v "1.4.1") (d (list (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "0g7qv2izm45i23i6z9y2w8bxcygnvjaag2dn6si66r8h82h10f5p")))

(define-public crate-memmod-2.0.0 (c (n "memmod") (v "2.0.0") (d (list (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "138hq90garlz1mib4jkq2vfm8jd147fngqq29fbja0aaiml2gh7h")))

