(define-module (crates-io me mm memmem) #:use-module (crates-io))

(define-public crate-memmem-0.1.0 (c (n "memmem") (v "0.1.0") (h "0sm17sbrab824k08yj9wf5vg50vxxnkv3wzzjl0ahhjmjzvr3g66")))

(define-public crate-memmem-0.1.1 (c (n "memmem") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "05ccifqgxdfxk6yls41ljabcccsz3jz6549l1h3cwi17kr494jm6")))

