(define-module (crates-io me nh menhirdb) #:use-module (crates-io))

(define-public crate-menhirdb-0.1.0 (c (n "menhirdb") (v "0.1.0") (d (list (d (n "bloomfilter") (r "^1.0") (d #t) (k 0)) (d (n "ofilter") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1lgbsnkyma3a4gzmq9wr4gqrwfbf5jwwlwr9xa4bm2ylqxw5kx6p")))

(define-public crate-menhirdb-0.1.1 (c (n "menhirdb") (v "0.1.1") (h "0lszazayr1k570b620vqndspnr2adfk1jnarjr3yrzamjxx3kav2")))

