(define-module (crates-io me io meio-protocol) #:use-module (crates-io))

(define-public crate-meio-protocol-0.63.0 (c (n "meio-protocol") (v "0.63.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)))) (h "1wpy5nx1s517sfsg494zkwinhill4l2130jcd29cwz9vgbc4rb4k")))

(define-public crate-meio-protocol-0.64.0 (c (n "meio-protocol") (v "0.64.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)))) (h "14s3ipbgs65y3rcpbms3rps3raxyl8b3w76q2yzqq0zfjgc7ycxb")))

(define-public crate-meio-protocol-0.65.0 (c (n "meio-protocol") (v "0.65.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)))) (h "0i7f7xvszgg1b8ans0190m4g4lljxb1b6r7fxwy2jpy58x9agp4z")))

(define-public crate-meio-protocol-0.70.0 (c (n "meio-protocol") (v "0.70.0") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)))) (h "1jn4xkn9p2mhk2m0882cclxl7rxpyd0mc8bdg3jn8lv08dpxlyna")))

(define-public crate-meio-protocol-0.71.0 (c (n "meio-protocol") (v "0.71.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)))) (h "0d8xzh48d90sv02kd9vz7hajdjbkqv46h95mbrhr8i8yy5lh2iwh")))

(define-public crate-meio-protocol-0.72.0 (c (n "meio-protocol") (v "0.72.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)))) (h "1zl2bdghxl6kfd8cjrxh2m8x6mv9lv0j6dygclvqp755h0x40zy0")))

(define-public crate-meio-protocol-0.73.0 (c (n "meio-protocol") (v "0.73.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)))) (h "0f26yw4cpyk6k03knam55pcknjf7ksd1mz220izc4xlzl3xf5cm6")))

(define-public crate-meio-protocol-0.74.0 (c (n "meio-protocol") (v "0.74.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)))) (h "1rvz29a0m20qirgq00z0zdpmycg59n8d043lk01c61vjg4bk5s66")))

(define-public crate-meio-protocol-0.74.1 (c (n "meio-protocol") (v "0.74.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)))) (h "1gczjjsvzlgmijml7x7lv6cdqjibcs7ynz9gcw7kgj97ar1vqnm8")))

(define-public crate-meio-protocol-0.74.3 (c (n "meio-protocol") (v "0.74.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)))) (h "15i23lkq0dqvs9am8c53flz2f1ms60lniaabksdaa274alfchi3w")))

(define-public crate-meio-protocol-0.74.4 (c (n "meio-protocol") (v "0.74.4") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0v3sd9cmf0lhfcmdq2izkm0dis6xgxfrjkvq089xjp5i7dvwdpvr")))

(define-public crate-meio-protocol-0.74.5 (c (n "meio-protocol") (v "0.74.5") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "034yzysffcv8fbk00dzpxpraqiifcm4h8c9dz6pij6bqacznn42w")))

(define-public crate-meio-protocol-0.80.0 (c (n "meio-protocol") (v "0.80.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "1rbdz6an6rgk61hpvd379vx5mc93az9qzqi2fk5y918azhamcrg0")))

(define-public crate-meio-protocol-0.81.0 (c (n "meio-protocol") (v "0.81.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0fqm4zfrpd436m4w7bc90n7dsly00285cz318qyj9xdh57ngbnq8")))

(define-public crate-meio-protocol-0.82.0 (c (n "meio-protocol") (v "0.82.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "08apkjmlyqn00c3856pldpfpx1i8l9fq7rljkbcg7wdb3b3vdan4")))

(define-public crate-meio-protocol-0.83.0 (c (n "meio-protocol") (v "0.83.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "1v8yg3w2jxxqawbc00lwzh8bwv2ax3b28v6f3cx7yqm4kkm3gqc8")))

(define-public crate-meio-protocol-0.84.0 (c (n "meio-protocol") (v "0.84.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)))) (h "1m6rpqrxq44qbkn08pcpxk0gmaszw67jmi20iv1vl1nwhiaff5qp")))

(define-public crate-meio-protocol-0.84.1 (c (n "meio-protocol") (v "0.84.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)))) (h "0maqna1pv9kydgan7zbgqxdp88j8w14lkgyb1w64h2h5b75s4yny")))

(define-public crate-meio-protocol-0.84.2 (c (n "meio-protocol") (v "0.84.2") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)))) (h "0880q4mal66jv0756m01kdn2kwwmabqxx8sap1101rh75xxzzmk1")))

(define-public crate-meio-protocol-0.85.0 (c (n "meio-protocol") (v "0.85.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)))) (h "10mvyhqwgc5linh8p3yb7xgz2kpdk4qcv46518810i8kaz54rjya")))

(define-public crate-meio-protocol-0.86.0 (c (n "meio-protocol") (v "0.86.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)))) (h "1s9pb1plbsv7wn79052rcgqpcp6bnj1gxzfyr6vbsrm5lv9dzxx0")))

(define-public crate-meio-protocol-0.86.1 (c (n "meio-protocol") (v "0.86.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)))) (h "0ic36zsaw69g44hbidilrvqww4knra3b0g2la40sqzarqjlmjl6i")))

(define-public crate-meio-protocol-0.87.0 (c (n "meio-protocol") (v "0.87.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)))) (h "17xbbdc31g67r2gywz70wsrx9sdvqzhmg06lxbwy0hd9v00rpc0i")))

(define-public crate-meio-protocol-0.88.0 (c (n "meio-protocol") (v "0.88.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1z8rrfii45r9g6jxfiqwgnz09i53sim056mfdy5z9ib56w98ls79")))

(define-public crate-meio-protocol-0.88.1 (c (n "meio-protocol") (v "0.88.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "18dgizbhfka8dhwy9d3mwgh0v4r268mirrs28phdb9vhj27wzzxc")))

(define-public crate-meio-protocol-0.89.0 (c (n "meio-protocol") (v "0.89.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0hdy0hdv60297l3w1wn4a2a8qny6pxkqayjwww63gg9icf2ishfp")))

(define-public crate-meio-protocol-0.89.1 (c (n "meio-protocol") (v "0.89.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "17p3y7w99km33sz16irsmmhh8rnnh6iba74k7d1lk902w4h01d5i")))

(define-public crate-meio-protocol-0.89.2 (c (n "meio-protocol") (v "0.89.2") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0in7b60339xiyn6fhf3yvdkjshhzrr2fnsrjyk7k87999zkgpnpi")))

(define-public crate-meio-protocol-0.89.3 (c (n "meio-protocol") (v "0.89.3") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "12rmfl2lvxnbvdbqvg4ms2s7r1lqjpc31scm3ba3kvksd79zbb9j")))

(define-public crate-meio-protocol-0.89.4 (c (n "meio-protocol") (v "0.89.4") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1wcr5rh4k099lkchag6xbizgpp002h3qnrsnvbwa9gdy47rkq3vl")))

(define-public crate-meio-protocol-0.90.0 (c (n "meio-protocol") (v "0.90.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0xbckvgfcpshcmz28ll0q7r0iav2xk0mpcx2vrs61a40bv48mz2d")))

(define-public crate-meio-protocol-0.91.0 (c (n "meio-protocol") (v "0.91.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "02lskkfcc2jz8zah8dg7jb7hfmbjlyx1n9n3zd46z37kk4bgwxyg")))

(define-public crate-meio-protocol-0.91.1 (c (n "meio-protocol") (v "0.91.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0lpc6hk49qk4hv7h4xal7g1n40fgrf34s0ppk4al2iaxy0b7qngl")))

(define-public crate-meio-protocol-0.91.2 (c (n "meio-protocol") (v "0.91.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1ic3dbnizlbwq2kk1b16ikdaa48w0pzqq2c8149m1j2cmsmdv0k0")))

(define-public crate-meio-protocol-0.92.0 (c (n "meio-protocol") (v "0.92.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1c7zcdhsqs98llxl3i163j7q13fb3yi9idkndd5ba7qipxfwp6l2")))

(define-public crate-meio-protocol-0.93.0 (c (n "meio-protocol") (v "0.93.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "15dp8qgri2dwj3i83875iknzmjrklsxq28a9nkslck5xgggmyl5i")))

(define-public crate-meio-protocol-0.93.1 (c (n "meio-protocol") (v "0.93.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)))) (h "1b1pprp7cwr5gyg43aq4wcc5hrz4x0vsshwiq8b7ap5wfxal0gf1")))

(define-public crate-meio-protocol-0.93.2 (c (n "meio-protocol") (v "0.93.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)))) (h "0mk1d2dmpvdnf6hyzf3kfazzq1f3krc7myf0mjxmbhr3vdbzhliq")))

(define-public crate-meio-protocol-0.93.3 (c (n "meio-protocol") (v "0.93.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)))) (h "0cj3y76qqhd1hs2gjjh2g5qdi29f2bv0xgjsf94f6i11f24sxrfi")))

(define-public crate-meio-protocol-0.93.4 (c (n "meio-protocol") (v "0.93.4") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (d #t) (k 0)))) (h "0g6dv1k74gc5zj73p3csgjq1snwmfi723vqjv4pq0kzn7m5jfd36")))

(define-public crate-meio-protocol-0.93.5 (c (n "meio-protocol") (v "0.93.5") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "serde") (r "^1.0.129") (d #t) (k 0)))) (h "1p4fi00rcjgvjn8w2s6fidnmfb3pf98ig04p0z599xdj4nba65iv")))

(define-public crate-meio-protocol-0.93.6 (c (n "meio-protocol") (v "0.93.6") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "1l619aw1zwxcymr59cf8k1awh33nfgrkjp94bvyvs1ak9b3jx7hn")))

(define-public crate-meio-protocol-0.94.0 (c (n "meio-protocol") (v "0.94.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "10gr1fcr2yamxrnr9pglzzg1j7szpssab44cwdhj53ag0j5ias4m")))

(define-public crate-meio-protocol-0.95.0 (c (n "meio-protocol") (v "0.95.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "0873vi146wwlfqdys9p66s0jaasf4alpq0grrg8fg3993kz4lymy")))

(define-public crate-meio-protocol-0.95.1 (c (n "meio-protocol") (v "0.95.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "1578m95y0nf423n5igpr8w4c1da7hmzrgq7fv5j71yhvq1958sdm")))

(define-public crate-meio-protocol-0.95.2 (c (n "meio-protocol") (v "0.95.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "1csf1mfbsh2066v3dqvfi69kzy0b6nd5hgysbg5y7x4ls8isjk4c")))

(define-public crate-meio-protocol-0.96.0 (c (n "meio-protocol") (v "0.96.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "1cvqibdfcmsvi62qywyrjcbvf745335kk9klmcly5xjg53nwk67z")))

(define-public crate-meio-protocol-0.97.0 (c (n "meio-protocol") (v "0.97.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "04icdgwwrfmdn67z94fwjd8906hkpg8g1j2pjb677a7gyvlap67k")))

