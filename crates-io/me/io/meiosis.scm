(define-module (crates-io me io meiosis) #:use-module (crates-io))

(define-public crate-meiosis-0.0.0 (c (n "meiosis") (v "0.0.0") (h "1s58ckw6lsgdxw5bw2973k3pk2d48vsbz8l8kjc1ni0zk40ffk5b")))

(define-public crate-meiosis-0.1.0 (c (n "meiosis") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1ckylnq04l2zzxipjs42v645gw0x7b6h3cg5smc2k44rvhmnq09n") (f (quote (("default"))))))

