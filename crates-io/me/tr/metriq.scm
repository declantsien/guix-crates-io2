(define-module (crates-io me tr metriq) #:use-module (crates-io))

(define-public crate-metriq-0.1.0 (c (n "metriq") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)))) (h "1qf6zk4v285qfy4qqq4m7h03vybg2jbpl8v0631i8sk0phpnkq5s")))

(define-public crate-metriq-0.1.1 (c (n "metriq") (v "0.1.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "solana-program") (r "^1.8") (d #t) (k 0)))) (h "1fh9mcsnfzy72l8268xxxa751vcnj81wxjh755y0517d9c80bafj")))

