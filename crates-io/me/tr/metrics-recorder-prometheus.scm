(define-module (crates-io me tr metrics-recorder-prometheus) #:use-module (crates-io))

(define-public crate-metrics-recorder-prometheus-0.1.0 (c (n "metrics-recorder-prometheus") (v "0.1.0") (d (list (d (n "hdrhistogram") (r "^6.1") (d #t) (k 0)) (d (n "metrics-core") (r "^0.2") (d #t) (k 0)) (d (n "metrics-util") (r "^0.1") (d #t) (k 0)))) (h "033hdwf0v9hykf1jn2a26cccwc8rm23ywljmgd972q3nfckv6bds")))

(define-public crate-metrics-recorder-prometheus-0.2.0 (c (n "metrics-recorder-prometheus") (v "0.2.0") (d (list (d (n "hdrhistogram") (r "^6.1") (d #t) (k 0)) (d (n "metrics-core") (r "^0.3") (d #t) (k 0)) (d (n "metrics-util") (r "^0.1") (d #t) (k 0)))) (h "0r1y25424lkyx2grvv4ld58wxaah7l976npm1ikrc02h5xhsg2h2")))

(define-public crate-metrics-recorder-prometheus-0.2.2 (c (n "metrics-recorder-prometheus") (v "0.2.2") (d (list (d (n "hdrhistogram") (r "^6.1") (d #t) (k 0)) (d (n "metrics-core") (r "^0.4") (d #t) (k 0)) (d (n "metrics-util") (r "^0.2") (d #t) (k 0)))) (h "0vzpr90zi3dig45p7c7dmc7bzzjjxhj5f43l8dbd2wxih3s5c3s4")))

(define-public crate-metrics-recorder-prometheus-0.2.3 (c (n "metrics-recorder-prometheus") (v "0.2.3") (d (list (d (n "hdrhistogram") (r "^6.1") (d #t) (k 0)) (d (n "metrics-core") (r "^0.4") (d #t) (k 0)) (d (n "metrics-util") (r "^0.2") (d #t) (k 0)))) (h "16xlslfd9hj8lscdkrcaxa8xdf89xrn4i9npjfhcl0d3f54l5y1b")))

