(define-module (crates-io me tr metrics-exporter-http) #:use-module (crates-io))

(define-public crate-metrics-exporter-http-0.1.0 (c (n "metrics-exporter-http") (v "0.1.0") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metrics-core") (r "^0.3") (d #t) (k 0)))) (h "0gdb8mgfwbbnaqfbsdap5qkkzsb07qah0il1pflmi1par0hafyiz")))

(define-public crate-metrics-exporter-http-0.1.2 (c (n "metrics-exporter-http") (v "0.1.2") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metrics-core") (r "^0.4") (d #t) (k 0)))) (h "0v7m9r7cqx6n7pjbrz9wpbfnrawfwz0ikh7bvgvwivdlm0pbgchc")))

(define-public crate-metrics-exporter-http-0.2.0 (c (n "metrics-exporter-http") (v "0.2.0") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)))) (h "12gcfb19wqa1dlhxsb99pw54hk7w4m2wm2dvhy02p0w96jq10fmh")))

(define-public crate-metrics-exporter-http-0.3.0 (c (n "metrics-exporter-http") (v "0.3.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)))) (h "09sfflzz1xrjg436pjq52cc316ydplyiwclaqv2js1mf0k91fh71")))

