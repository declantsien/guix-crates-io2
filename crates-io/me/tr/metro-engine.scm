(define-module (crates-io me tr metro-engine) #:use-module (crates-io))

(define-public crate-metro-engine-0.0.0 (c (n "metro-engine") (v "0.0.0") (d (list (d (n "metro-agent") (r "^0.0.0") (d #t) (k 0)) (d (n "metro-blackboard") (r "^0.0.0") (d #t) (k 0)) (d (n "metro-macros") (r "^0.0.0") (d #t) (k 0)))) (h "0npd1q6x9znngxskph59j36h1gnajwnkz944bb4rfg5nz5dsjmiz")))

