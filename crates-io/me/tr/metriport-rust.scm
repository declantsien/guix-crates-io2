(define-module (crates-io me tr metriport-rust) #:use-module (crates-io))

(define-public crate-metriport-rust-0.1.0 (c (n "metriport-rust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "1c08bpfl4cgyh2394r207wnjn26dhkxmaifgp19ipxy5djfj3yh9")))

