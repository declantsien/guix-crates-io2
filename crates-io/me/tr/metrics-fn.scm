(define-module (crates-io me tr metrics-fn) #:use-module (crates-io))

(define-public crate-metrics-fn-0.1.1 (c (n "metrics-fn") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics-fn-codegen") (r "^0.1") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (o #t) (d #t) (k 0)))) (h "17ddsdil3j9yp6c2hfi2nix3v7q3zg5508abnnpn0gcvs4iwwdis") (f (quote (("record-prometheus" "prometheus" "lazy_static") ("record-log" "log") ("default"))))))

