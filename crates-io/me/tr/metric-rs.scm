(define-module (crates-io me tr metric-rs) #:use-module (crates-io))

(define-public crate-metric-rs-0.1.0 (c (n "metric-rs") (v "0.1.0") (h "14658c02qahwsy7qhsm7iakxiv42fmnbn3c1xk3l4ln8mfj4130s") (y #t)))

(define-public crate-metric-rs-0.1.1 (c (n "metric-rs") (v "0.1.1") (h "0bpxby3qhjcjnq2mfjr35jkmkdzv5qncc0k814xffda3ihf08bpf") (y #t)))

(define-public crate-metric-rs-0.1.2 (c (n "metric-rs") (v "0.1.2") (h "1hhxm7msj9gn057g8zbjqhlj7rn1v4a66dfag2n0f0fykcq6smkx") (y #t)))

