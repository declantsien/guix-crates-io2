(define-module (crates-io me tr metromc) #:use-module (crates-io))

(define-public crate-metromc-0.1.0 (c (n "metromc") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)))) (h "05746yxaaddyqmp8d8lk211nws2fyzj3wjzhxn66ccfc6a9l21pi")))

(define-public crate-metromc-0.2.0 (c (n "metromc") (v "0.2.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)))) (h "1l43gg48qrndlqjxhd00s2z84jsgrny377ynjwg3vsm69yx9gyw6")))

