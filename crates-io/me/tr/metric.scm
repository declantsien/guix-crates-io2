(define-module (crates-io me tr metric) #:use-module (crates-io))

(define-public crate-metric-0.1.0 (c (n "metric") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "dimensioned") (r "^0.6.0") (d #t) (k 2)) (d (n "uom") (r "^0.14.0") (d #t) (k 2)))) (h "10hlin1cs6jqw10vc4hsy4whkg61z27pqvgdzc89z1vnp90swa01") (f (quote (("std") ("default" "std"))))))

(define-public crate-metric-0.1.1 (c (n "metric") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "dimensioned") (r "^0.6.0") (d #t) (k 2)) (d (n "uom") (r "^0.14.0") (d #t) (k 2)))) (h "13sjwksp3c77mplzl2j51g5bn51lhjwfw70ssa6bxpha72b93i38") (f (quote (("std") ("default" "std"))))))

(define-public crate-metric-0.1.2 (c (n "metric") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "dimensioned") (r "^0.6.0") (d #t) (k 2)) (d (n "uom") (r "^0.14.0") (d #t) (k 2)))) (h "0bc360q0y5bdszw11cz1yc4lz72jp5z8w4j2r31iyhqra282m6zp") (f (quote (("std") ("default" "std"))))))

