(define-module (crates-io me tr metrics-exporter-sentry) #:use-module (crates-io))

(define-public crate-metrics-exporter-sentry-0.1.0 (c (n "metrics-exporter-sentry") (v "0.1.0") (d (list (d (n "metrics") (r "^0.22") (d #t) (k 0)) (d (n "sentry") (r "^0.32") (f (quote ("UNSTABLE_metrics"))) (k 0)) (d (n "sentry") (r "^0.32") (d #t) (k 2)))) (h "0hj9y1j8xx9wxylz8jkfrkxqsihlh3gfhnhadrhgyy3fp098qn56")))

