(define-module (crates-io me tr metrum) #:use-module (crates-io))

(define-public crate-metrum-0.1.0 (c (n "metrum") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0a44fcfsw0gj23fhsafpizff0mnlm52da20qxjw0lwqgw651f3ak") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-metrum-0.2.0 (c (n "metrum") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0pg0sp8l1ry0013l2hjw0g1q1dzbi7jbpm4c9mgvpgv6b53vvx4p") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-metrum-0.3.0 (c (n "metrum") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "07nahagq89nw0hf1appkb045amh9h203w378qv476nnq5pal4z16") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-metrum-0.4.0 (c (n "metrum") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "0v70l6k5ddj90w6i9l072x5lfnjq7zfkmn11smd2q4g79blqm08k") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-metrum-0.5.0 (c (n "metrum") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.188") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "1cjgr8xhihn728rb9mc2fw0sv8j50j1jksmsnsn0f15kvwpilfri") (s 2) (e (quote (("serde" "dep:serde" "serde/derive"))))))

