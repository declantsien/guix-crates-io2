(define-module (crates-io me tr metrics-exporter-async_std) #:use-module (crates-io))

(define-public crate-metrics-exporter-async_std-0.1.0 (c (n "metrics-exporter-async_std") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-runtime") (r "^0.13") (d #t) (k 2)))) (h "1iz6gxg2x5k2avzw545zslns7lm2925zqx0rzgsnidmsw0a86w4h") (y #t)))

