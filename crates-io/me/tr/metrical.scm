(define-module (crates-io me tr metrical) #:use-module (crates-io))

(define-public crate-metrical-0.1.1 (c (n "metrical") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-pickle") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1559fnvccg2afnzwrgd0sfp2qfraq30sz8hzlhhgj5c6rbiig7p9") (f (quote (("pickle" "serde" "serde-pickle"))))))

