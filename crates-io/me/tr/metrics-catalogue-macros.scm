(define-module (crates-io me tr metrics-catalogue-macros) #:use-module (crates-io))

(define-public crate-metrics-catalogue-macros-0.1.0 (c (n "metrics-catalogue-macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qwk6q6g588za2fp80w7agp404d4s93aii4wqa27sq5zf5fkgy8g")))

(define-public crate-metrics-catalogue-macros-0.2.0 (c (n "metrics-catalogue-macros") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a3w73mdml0zvqrycn2vhrnzfmjk4cgqq8lrr1gqyn4n97m8jcck") (f (quote (("prometheus") ("default"))))))

(define-public crate-metrics-catalogue-macros-0.2.1 (c (n "metrics-catalogue-macros") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lny2bsfpnnwrrbk9ncxa4gj1r0sxzqjp4pjb4fk0qgnq9dxcsmw") (f (quote (("prometheus") ("default"))))))

(define-public crate-metrics-catalogue-macros-0.3.0 (c (n "metrics-catalogue-macros") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qwp6qp7y850kvx90867v1cc3437m57ygsghjw35ip3p5dfydwzz") (f (quote (("prometheus") ("default"))))))

(define-public crate-metrics-catalogue-macros-0.4.0 (c (n "metrics-catalogue-macros") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qfqkn2pjw55jws9465w0gmbq1zwgm0lfizz2f7l45s0wyyvqck6") (f (quote (("prometheus") ("default"))))))

(define-public crate-metrics-catalogue-macros-0.4.1 (c (n "metrics-catalogue-macros") (v "0.4.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11kchswdriyx692p5n515lwqywh0vm21spsv5w4fd1f8158av824") (f (quote (("prometheus") ("default"))))))

