(define-module (crates-io me tr metrics-observer-yaml) #:use-module (crates-io))

(define-public crate-metrics-observer-yaml-0.1.0 (c (n "metrics-observer-yaml") (v "0.1.0") (d (list (d (n "hdrhistogram") (r "^6.1") (d #t) (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-util") (r "^0.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1my0j3yf9b6spn3mn1jibv1fgq4cd500ng65szz03n2c0b2gwrmp")))

(define-public crate-metrics-observer-yaml-0.1.1 (c (n "metrics-observer-yaml") (v "0.1.1") (d (list (d (n "hdrhistogram") (r "^6.3") (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-util") (r "^0.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0db3xknx3d4z3pjqbw2j3fhibx72ss95mmvmzh75d4im048nixl3")))

