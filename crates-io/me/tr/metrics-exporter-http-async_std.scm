(define-module (crates-io me tr metrics-exporter-http-async_std) #:use-module (crates-io))

(define-public crate-metrics-exporter-http-async_std-0.1.0 (c (n "metrics-exporter-http-async_std") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5") (d #t) (k 0)) (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-runtime") (r "^0.13") (d #t) (k 2)))) (h "0qqa80yllz9i6v6qjzl51p63dw87xwnvycg198qm3js3ib6rb4ag")))

