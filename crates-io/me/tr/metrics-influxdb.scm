(define-module (crates-io me tr metrics-influxdb) #:use-module (crates-io))

(define-public crate-metrics-influxdb-0.1.0 (c (n "metrics-influxdb") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "metrics") (r "^0.21.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)))) (h "0173vv2c4n6jhcra091ssr54ahdnc22vjywi59al976fkl6avq7f")))

