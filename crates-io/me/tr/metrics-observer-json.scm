(define-module (crates-io me tr metrics-observer-json) #:use-module (crates-io))

(define-public crate-metrics-observer-json-0.1.0 (c (n "metrics-observer-json") (v "0.1.0") (d (list (d (n "hdrhistogram") (r "^6.1") (d #t) (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-util") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sfavpwxb46rc10bibs797xgvbk2ddklx115mxxd7d3870svgxb7")))

(define-public crate-metrics-observer-json-0.1.1 (c (n "metrics-observer-json") (v "0.1.1") (d (list (d (n "hdrhistogram") (r "^6.3") (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-util") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lzwv6qpyb04ygm7psnvp86zs1gqlf6v5krxhy7nncvc19331sdv")))

