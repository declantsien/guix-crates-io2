(define-module (crates-io me tr metrics-observer-prometheus) #:use-module (crates-io))

(define-public crate-metrics-observer-prometheus-0.1.0 (c (n "metrics-observer-prometheus") (v "0.1.0") (d (list (d (n "hdrhistogram") (r "^6.1") (d #t) (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-util") (r "^0.3") (d #t) (k 0)))) (h "0ypgm1gfdrpwxl2kmkxcyw7f7j7bfzhk7c63kfii13s4xbmmiy9d")))

(define-public crate-metrics-observer-prometheus-0.1.1 (c (n "metrics-observer-prometheus") (v "0.1.1") (d (list (d (n "hdrhistogram") (r "^6.3") (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-util") (r "^0.3") (d #t) (k 0)))) (h "0bcdxda8dficcyj5dlbmr8fsf6hjq6kslg3p8v026qia54q7hw4h")))

(define-public crate-metrics-observer-prometheus-0.1.2 (c (n "metrics-observer-prometheus") (v "0.1.2") (d (list (d (n "hdrhistogram") (r "^6.3") (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-util") (r "^0.3") (d #t) (k 0)))) (h "1nppsn3fjibnlq2nvsqyz6a3xslh6p7gxjq18y59ax4prqbq8nxk")))

(define-public crate-metrics-observer-prometheus-0.1.3 (c (n "metrics-observer-prometheus") (v "0.1.3") (d (list (d (n "hdrhistogram") (r "^6.3") (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-util") (r "^0.3") (d #t) (k 0)))) (h "1mikkb8rpxqm3ywfnxpy52fsnjdr2n4cgw8fyxyci2g1817vk6sg")))

(define-public crate-metrics-observer-prometheus-0.1.4 (c (n "metrics-observer-prometheus") (v "0.1.4") (d (list (d (n "hdrhistogram") (r "^6.3") (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "metrics-util") (r "^0.3") (d #t) (k 0)))) (h "1flfn3w3ivsbgy8giymvj13gmicwz1jml4rjj8iqpvw5hanj9zjb")))

