(define-module (crates-io me tr metriki-tokio) #:use-module (crates-io))

(define-public crate-metriki-tokio-0.1.0 (c (n "metriki-tokio") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 2)) (d (n "metriki-core") (r "^1.0") (d #t) (k 0)) (d (n "metriki-log-reporter") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-metrics") (r "^0.1") (d #t) (k 0)))) (h "0pl1igwdsn0zg7dh8p9lnrcc5slqrhnss3z0m2bvi23a8h4xhn91") (f (quote (("rt") ("default" "rt"))))))

(define-public crate-metriki-tokio-0.2.0 (c (n "metriki-tokio") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 2)) (d (n "metriki-core") (r "^1.7") (d #t) (k 0)) (d (n "metriki-log-reporter") (r "^0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-metrics") (r "^0.3") (d #t) (k 0)))) (h "0cjhc6bac1pcxnnkkqw0hf198vyfq9inpsnnsspxdp5jxh8s02py") (f (quote (("rt" "tokio-metrics/rt") ("default"))))))

