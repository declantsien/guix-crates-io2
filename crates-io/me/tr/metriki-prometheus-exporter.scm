(define-module (crates-io me tr metriki-prometheus-exporter) #:use-module (crates-io))

(define-public crate-metriki-prometheus-exporter-0.1.1 (c (n "metriki-prometheus-exporter") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.12") (d #t) (k 0)) (d (n "tiny_http") (r "^0.8") (d #t) (k 0)))) (h "1k2751iqfml86gaz7yqh58ijjp7clgg7k48csmxy5qkb1q5cx1wd")))

(define-public crate-metriki-prometheus-exporter-0.2.0 (c (n "metriki-prometheus-exporter") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^1.8") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12") (d #t) (k 0)))) (h "11q5gmk2nzickjfbvlzgxzyrqyl9bd2sci4s5za8vlgwwwjj483p")))

