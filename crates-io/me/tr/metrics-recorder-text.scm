(define-module (crates-io me tr metrics-recorder-text) #:use-module (crates-io))

(define-public crate-metrics-recorder-text-0.1.0 (c (n "metrics-recorder-text") (v "0.1.0") (d (list (d (n "hdrhistogram") (r "^6.1") (d #t) (k 0)) (d (n "metrics-core") (r "^0.2") (d #t) (k 0)) (d (n "metrics-util") (r "^0.1") (d #t) (k 0)))) (h "04vcqmcgy5kb0dmmvawdhcqvf43vfp2l6lly29p57idznc5ijqv2")))

(define-public crate-metrics-recorder-text-0.2.0 (c (n "metrics-recorder-text") (v "0.2.0") (d (list (d (n "hdrhistogram") (r "^6.1") (d #t) (k 0)) (d (n "metrics-core") (r "^0.3") (d #t) (k 0)) (d (n "metrics-util") (r "^0.1") (d #t) (k 0)))) (h "084n7fdnh0p2nph8j9rzcwf1rc6vkgh4hm4i41wfimmm32r212c9")))

(define-public crate-metrics-recorder-text-0.2.2 (c (n "metrics-recorder-text") (v "0.2.2") (d (list (d (n "hdrhistogram") (r "^6.1") (d #t) (k 0)) (d (n "metrics-core") (r "^0.4") (d #t) (k 0)) (d (n "metrics-util") (r "^0.2") (d #t) (k 0)))) (h "05larbvimj6ad2ra0r4h6rwnkvh3gcvnpamyvacg5g8y6zhab376")))

