(define-module (crates-io me tr metric_daemon) #:use-module (crates-io))

(define-public crate-metric_daemon-0.2.0 (c (n "metric_daemon") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 0)) (d (n "retry") (r "^1.2") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.5") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.5") (d #t) (k 1)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.1") (d #t) (k 0)))) (h "087q9rbiv5z0g76bwc2j2x6rgflw49w5dxilmspgdlszjbl52jbj")))

