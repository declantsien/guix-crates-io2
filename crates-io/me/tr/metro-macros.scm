(define-module (crates-io me tr metro-macros) #:use-module (crates-io))

(define-public crate-metro-macros-0.0.0 (c (n "metro-macros") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0jmcc536bwnzgs2z2d6ni4fn1xzjrzhhq98b7vpngwi1h1nravb7") (f (quote (("serde") ("default" "serde"))))))

