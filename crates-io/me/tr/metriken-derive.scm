(define-module (crates-io me tr metriken-derive) #:use-module (crates-io))

(define-public crate-metriken-derive-0.0.0 (c (n "metriken-derive") (v "0.0.0") (h "1saxknf9fkfrzj1ldpkb4nh39lw6f01sg4gvpdvqzfk24gwwsrna")))

(define-public crate-metriken-derive-0.1.0 (c (n "metriken-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1p75g2aw2c84sxlpj8kqbpvlfppnkkgnk9v4x9w770ixpcg98n1y")))

(define-public crate-metriken-derive-0.2.0 (c (n "metriken-derive") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0cpl09g0wk4ajbyln2pa80skgcklglniwjgks5bn7ih15shsf4q2")))

(define-public crate-metriken-derive-0.4.0 (c (n "metriken-derive") (v "0.4.0") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "17jvpcccrm6h12iyyr33m17sagfwhc4y28kjd5hf72pxrxpl1569")))

(define-public crate-metriken-derive-0.5.0 (c (n "metriken-derive") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "02pzgjsmswa9cdnl7gihfnw6snh6l1diahrfhavlaja31jn85n10")))

(define-public crate-metriken-derive-0.5.1 (c (n "metriken-derive") (v "0.5.1") (d (list (d (n "proc-macro-crate") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1nvigc3xz1nm53lqqkllxv0z02dd0rhdn104wlkrmrkkk163fhrn")))

(define-public crate-metriken-derive-0.3.4 (c (n "metriken-derive") (v "0.3.4") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "14j998sr5kyyrp15a5m4i4831msabvmzdcjwcs57xdal78pq8f0p")))

(define-public crate-metriken-derive-0.4.1 (c (n "metriken-derive") (v "0.4.1") (d (list (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1kqx7shyzaavk9p87imrv42f6iv6pfqdadbb8x9qmgiprn704jdv")))

