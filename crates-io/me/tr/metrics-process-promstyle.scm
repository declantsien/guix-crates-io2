(define-module (crates-io me tr metrics-process-promstyle) #:use-module (crates-io))

(define-public crate-metrics-process-promstyle-0.18.0 (c (n "metrics-process-promstyle") (v "0.18.0") (d (list (d (n "metrics") (r "^0.18.1") (d #t) (k 0)) (d (n "metrics-exporter-prometheus") (r "^0.9.0") (d #t) (k 2)) (d (n "procfs") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "12l5b4x7dlf8rpp6l4sagri9sdr5g6wcynaizsi32wvp8shknwq8")))

