(define-module (crates-io me tr metriki-tracing) #:use-module (crates-io))

(define-public crate-metriki-tracing-0.1.0 (c (n "metriki-tracing") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "metriki-core") (r "^1.7") (d #t) (k 0)) (d (n "metriki-log-reporter") (r "^0.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (f (quote ("registry"))) (k 0)))) (h "0qdzglp80cisidv79aw0b7r3wkzq6c0ijpiwyzg39kglfwhf76nw")))

(define-public crate-metriki-tracing-0.2.0 (c (n "metriki-tracing") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "metriki-core") (r "^1.7") (d #t) (k 0)) (d (n "metriki-log-reporter") (r "^0.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("registry"))) (k 0)))) (h "09044wk57f9giz6kjm90fddrwiwf0j1cxf8z3mckq0aa0vfms9fc")))

