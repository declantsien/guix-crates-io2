(define-module (crates-io me tr metrics-exporter-statsd) #:use-module (crates-io))

(define-public crate-metrics-exporter-statsd-0.1.0 (c (n "metrics-exporter-statsd") (v "0.1.0") (d (list (d (n "cadence") (r "^0.29") (d #t) (k 0)) (d (n "metrics") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09x3hs1mbvij541aard38dz0w7bm40m3msx3r1qjlh2kj5n088lz") (y #t)))

(define-public crate-metrics-exporter-statsd-0.2.0 (c (n "metrics-exporter-statsd") (v "0.2.0") (d (list (d (n "cadence") (r "^0.29") (d #t) (k 0)) (d (n "metrics") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w0m6a6383j8s2v64jl5fn5clpxxc9jz64cq1b2sqgs0r8546sva")))

(define-public crate-metrics-exporter-statsd-0.3.0 (c (n "metrics-exporter-statsd") (v "0.3.0") (d (list (d (n "cadence") (r "^0.29") (d #t) (k 0)) (d (n "metrics") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rkna1m2wm1ifxyrfh304wkz4wvaazrsbnrl74nlbz95ml5rg3fa")))

(define-public crate-metrics-exporter-statsd-0.4.0 (c (n "metrics-exporter-statsd") (v "0.4.0") (d (list (d (n "cadence") (r "^0.29") (d #t) (k 0)) (d (n "metrics") (r "^0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h5jargnwfg1s9pvfzhhx7zprb1y8659vr7ra5nzdwi8dp59448w")))

(define-public crate-metrics-exporter-statsd-0.5.0 (c (n "metrics-exporter-statsd") (v "0.5.0") (d (list (d (n "cadence") (r "^0.29") (d #t) (k 0)) (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0njph6zczwhnd3nrwi4apyqkph5dajx9kmd1m9qahabn48x6cgpz")))

(define-public crate-metrics-exporter-statsd-0.6.0 (c (n "metrics-exporter-statsd") (v "0.6.0") (d (list (d (n "cadence") (r "^0.29") (d #t) (k 0)) (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0644j6lalvz6gjc2spxn267hjbnjgj72z3zgpcg35r7rxh764jp3")))

(define-public crate-metrics-exporter-statsd-0.7.0 (c (n "metrics-exporter-statsd") (v "0.7.0") (d (list (d (n "cadence") (r "^1.0") (d #t) (k 0)) (d (n "metrics") (r "^0.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hws3lf9332q386lbd57gm19vkc193iqxcb1dbaia7s3dsqppgc2")))

