(define-module (crates-io me tr metrics-exporter-log) #:use-module (crates-io))

(define-public crate-metrics-exporter-log-0.1.0 (c (n "metrics-exporter-log") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metrics") (r "^0.9") (d #t) (k 0)) (d (n "metrics-core") (r "^0.2") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "19d2qia5n43jfcbbqv2cim762x48z2njrmrvb3jrkf40cvs5jml1")))

(define-public crate-metrics-exporter-log-0.2.0 (c (n "metrics-exporter-log") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metrics-core") (r "^0.3") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "11v4ljgna3wm1qq2c8cv5zppz2ydkg5fm0y76y4214fgh48cg25d")))

(define-public crate-metrics-exporter-log-0.2.1 (c (n "metrics-exporter-log") (v "0.2.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metrics-core") (r "^0.4") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "11rn6d6q34y2r6ypi4j12zhb1r7zxin6hfkwbi6qn96fmj69nc2b")))

(define-public crate-metrics-exporter-log-0.3.0 (c (n "metrics-exporter-log") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.2") (d #t) (k 0)))) (h "1vxwnz6hq80aaq9mfv1d4vx0p6ygk061wgnsscaism8zz15b7w62")))

(define-public crate-metrics-exporter-log-0.4.0 (c (n "metrics-exporter-log") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metrics-core") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 0)))) (h "1az8cgicfpsd3ccl8ny5l6dwdb9kjqglgkiixgjvvy6mdf0n7z7k")))

