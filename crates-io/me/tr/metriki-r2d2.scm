(define-module (crates-io me tr metriki-r2d2) #:use-module (crates-io))

(define-public crate-metriki-r2d2-0.1.0 (c (n "metriki-r2d2") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("r2d2"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "metriki-core") (r "^1.7") (d #t) (k 0)) (d (n "metriki-log-reporter") (r "^0.1") (d #t) (k 2)) (d (n "r2d2") (r "^0.8") (d #t) (k 0)))) (h "01c5083c88qamib5jp5yky5aaz5wyxhg8n6x0sqanz7c5lkgy1nb")))

