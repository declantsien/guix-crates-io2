(define-module (crates-io me tr metriki-statsd-reporter) #:use-module (crates-io))

(define-public crate-metriki-statsd-reporter-0.1.0 (c (n "metriki-statsd-reporter") (v "0.1.0") (d (list (d (n "cadence") (r "^0.25") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^1") (d #t) (k 0)))) (h "11dvn7d33cb2srwjdbqjlwjxmsff6hs4v089h75d4szss6rkh90r")))

(define-public crate-metriki-statsd-reporter-0.1.1 (c (n "metriki-statsd-reporter") (v "0.1.1") (d (list (d (n "cadence") (r "^0.26") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^1") (d #t) (k 0)))) (h "0iwzymna2hw8212m5ni6v2rngl5kzns07c847fyr25jmdmwfkzgg")))

(define-public crate-metriki-statsd-reporter-0.2.0 (c (n "metriki-statsd-reporter") (v "0.2.0") (d (list (d (n "cadence") (r "^0.29") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^1.8") (d #t) (k 0)))) (h "1p0ckxm77lr0wzfa697y8ifasn8wb15nivsidqc6c9q4afv4fg3i")))

