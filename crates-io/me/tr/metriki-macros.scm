(define-module (crates-io me tr metriki-macros) #:use-module (crates-io))

(define-public crate-metriki-macros-1.0.0 (c (n "metriki-macros") (v "1.0.0") (d (list (d (n "metriki-core") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0myh5jn2146mqs5ni1xzs07bicwxbk5q4q96nr39fsrca1ynaili")))

