(define-module (crates-io me tr metro-blackboard) #:use-module (crates-io))

(define-public crate-metro-blackboard-0.0.0 (c (n "metro-blackboard") (v "0.0.0") (d (list (d (n "metro-macros") (r "^0.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "04mdr3kvzzhi6jhlv6hkgmm4rqqdl59alc619jr8jrqdcdfblg0g") (f (quote (("serde" "serde/derive" "uuid/serde" "metro-macros/serde") ("derive" "metro-macros") ("default" "derive" "serde" "btree") ("btree"))))))

