(define-module (crates-io me tr metriki-riemann-reporter) #:use-module (crates-io))

(define-public crate-metriki-riemann-reporter-0.1.0 (c (n "metriki-riemann-reporter") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^0.1.2-alpha.0") (d #t) (k 0)) (d (n "rustmann") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "0wjg7i6zjxzh0h8p3rij955imhq8pzvxx7xi5dkms8gf8d2wcyc2")))

(define-public crate-metriki-riemann-reporter-0.1.1 (c (n "metriki-riemann-reporter") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^1.0") (d #t) (k 0)) (d (n "rustmann") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "12pjm4y6k6sq3xhlrvhyrv84dh97h6yrx0fj87pgwgn8z4x506l6")))

(define-public crate-metriki-riemann-reporter-0.2.0 (c (n "metriki-riemann-reporter") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^1.8") (d #t) (k 0)) (d (n "rustmann") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "1q20qdcmlpnq6s92hi8l6ri2a51k2jir8jal3mlxqsm4a4ml30mn")))

