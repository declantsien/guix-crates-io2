(define-module (crates-io me tr metrix) #:use-module (crates-io))

(define-public crate-metrix-0.1.0 (c (n "metrix") (v "0.1.0") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18facyv0sw8sjnhr1g6l23wfvz8vqsdd0n0acin8b2qflg396r0b")))

(define-public crate-metrix-0.1.1 (c (n "metrix") (v "0.1.1") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ppbjv6d30lwhprr9z6j7gfd5fsrffcx0c0r712rps1714lbkxcq")))

(define-public crate-metrix-0.1.2 (c (n "metrix") (v "0.1.2") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03pj8ardix92ypzcyy77hygb031z4gys48zlmlncclh70y57307d")))

(define-public crate-metrix-0.1.3 (c (n "metrix") (v "0.1.3") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04pcp5g4hqw8vwnyx98zhk00ar4qylyim8kf3gsvsvbrzz0visb1")))

(define-public crate-metrix-0.1.4 (c (n "metrix") (v "0.1.4") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bb7g760mh0nz5xbicqvdwaz1y60ppjmh9cylzg7iiwzmrhcgcgs")))

(define-public crate-metrix-0.1.5 (c (n "metrix") (v "0.1.5") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vnmd4mswgbgb4835cq0jm0bzbhf9zpk0qf0mfqykvg9lmb24cia")))

(define-public crate-metrix-0.1.6 (c (n "metrix") (v "0.1.6") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ijq0hm1a4dpmfspgdzygjxhviiahcrrk0g9zyjbh9yqzbpxmjn3")))

(define-public crate-metrix-0.2.0 (c (n "metrix") (v "0.2.0") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "08bhz7lg95mgxf3s7bbzdbq5sscqgyq0mmziqxp12cyk9xyrx0zw")))

(define-public crate-metrix-0.3.0 (c (n "metrix") (v "0.3.0") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0bpvy6gm41cmcg0ki88a072759zgbmvypndbykzqxp6vgvr2qs3z")))

(define-public crate-metrix-0.4.0 (c (n "metrix") (v "0.4.0") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0wv9xksfzqbgmiizl5n8r0lbf93p5yr4n5iqhwhrmjbnid9cqy7l")))

(define-public crate-metrix-0.4.1 (c (n "metrix") (v "0.4.1") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1y9d7wkyywm8mnsbq30m7x0vgdbjjqx5sjkvhym3hrfa7sp9b3cw")))

(define-public crate-metrix-0.4.2 (c (n "metrix") (v "0.4.2") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0l7k5d6r7l7s3rsbk5rg7x2narsl2v41vnd01mm9ap9a24ziqj1a")))

(define-public crate-metrix-0.4.3 (c (n "metrix") (v "0.4.3") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0b2pr0ll8s5pksmyj11lbmsvaaqla1p6wr6z5d5sszhg9hwxjhdv")))

(define-public crate-metrix-0.4.4 (c (n "metrix") (v "0.4.4") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1w5bki09f2ahwgd2v42bmsdcq9n1c5ys5ciip01ljx189qb2hw24")))

(define-public crate-metrix-0.4.5 (c (n "metrix") (v "0.4.5") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0i4a1yqdxvfvxhvkdm89hw4in23zd9f8m1yid900k15j96fy3n4y")))

(define-public crate-metrix-0.4.6 (c (n "metrix") (v "0.4.6") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1jffgw3qjq8y3vbydp8p37zmlp5dxm4m2v1nmgw5l97vbd1zvrw2")))

(define-public crate-metrix-0.5.0 (c (n "metrix") (v "0.5.0") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0744mpglilmf5vy6lq2xb8vglc50afar8nbbl6gqcaj6b0fl0djz")))

(define-public crate-metrix-0.5.2 (c (n "metrix") (v "0.5.2") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0b5v6j61h17zbmmcp58smzz34ydbqdvpq0miw2df6vrym9wj3p5y")))

(define-public crate-metrix-0.5.3 (c (n "metrix") (v "0.5.3") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "089a3p07yp0pigy7njvhbrgrzgqj30zx1ash0pfi1wk4lwxggq0i")))

(define-public crate-metrix-0.5.4 (c (n "metrix") (v "0.5.4") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "070k1ac90plf0qz0grhqlxc15yxkw39f5y4k9smmxkg5xb0nvv5p")))

(define-public crate-metrix-0.6.0 (c (n "metrix") (v "0.6.0") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "135wj00ddxda2x5gj95rhrpy6p794rmnvj67x6q0ij9pnkgjyvmc")))

(define-public crate-metrix-0.6.1 (c (n "metrix") (v "0.6.1") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "00r0r2185gffy6hrxa6xw0rih0gskskc5mjj2j1a5hnsdgb00v30")))

(define-public crate-metrix-0.6.2 (c (n "metrix") (v "0.6.2") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1dfyx7vlnbs5zmzgh829ri22zs2c7y8sms5s86vskia9wjw99rwm")))

(define-public crate-metrix-0.6.3 (c (n "metrix") (v "0.6.3") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0m54k7bl8ph34fb2k1qqg2pr59l3zzmw8rhm4rd7s7bagzzhargp")))

(define-public crate-metrix-0.6.4 (c (n "metrix") (v "0.6.4") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0zk85hg5xkgdqhzdw230q88zcjjl6vdyq96wz0x43aqvx7vsrnaj")))

(define-public crate-metrix-0.6.5 (c (n "metrix") (v "0.6.5") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1s8c7vl43w6gbwysyl98zd3xv2v3y825f99awvxs6zgv25c3njsp")))

(define-public crate-metrix-0.7.0 (c (n "metrix") (v "0.7.0") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0x12s410p8qynf9na3rvpjji63lgkyh357xljakwq191hvrlb17z")))

(define-public crate-metrix-0.7.1 (c (n "metrix") (v "0.7.1") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0pwjxcszjrzzggdfcs1c4x4yrxqlwrfmrvf2v5wml8paq99rfix6")))

(define-public crate-metrix-0.7.2 (c (n "metrix") (v "0.7.2") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0bijnhmf7zwy7mpv90nmxhqaz9bpy36m1vjfxgzgbxckd58xvnki")))

(define-public crate-metrix-0.7.3 (c (n "metrix") (v "0.7.3") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0c3bhkymnphwxdc361g5n9clca2g13zm6bn4vd0czmklrb6cg6pg")))

(define-public crate-metrix-0.7.5 (c (n "metrix") (v "0.7.5") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "04q2a7x7gxlv3492xnfrsz5qw5ck21mbydhl38g1mcxx37m22pcr")))

(define-public crate-metrix-0.7.6 (c (n "metrix") (v "0.7.6") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "05wcdx0dvfsgylvx6zkm1jqig9v353hzh3jkrbmcvgr9y165mqhf")))

(define-public crate-metrix-0.8.0 (c (n "metrix") (v "0.8.0") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1ghv25ncrbwchsd8x7cn07p17006nlf50f42iwjg896ra4i5ym8w")))

(define-public crate-metrix-0.8.1 (c (n "metrix") (v "0.8.1") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1jdyharfjbfdwkbbz0v1spbwgg231b44c1jk44vldc2pr2lcfy27")))

(define-public crate-metrix-0.8.2 (c (n "metrix") (v "0.8.2") (d (list (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "153cpxkggn3i618gkkfx5qgcflnjsiwl87x9q0gsa4wf2qkk9bb0")))

(define-public crate-metrix-0.8.3 (c (n "metrix") (v "0.8.3") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1zvxz2lr2c2jf5dd5hmyhk0xn4v0wbk9mk66gy950fchpclwmcys")))

(define-public crate-metrix-0.9.0 (c (n "metrix") (v "0.9.0") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1zfy5f30cw93abq02iccbv7n6z89afrx15y35snz25p5j3bc1kww")))

(define-public crate-metrix-0.9.1 (c (n "metrix") (v "0.9.1") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0c7xkq5qxb1nkz8j6m8gb2v1wn1ims9x2ccvsy1yzla7nb0inpwr")))

(define-public crate-metrix-0.9.2 (c (n "metrix") (v "0.9.2") (d (list (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0n3zg4f0gxl11rljn17js8ddw3n3n0mk07p51316hpqjj8c2i16f")))

(define-public crate-metrix-0.9.3 (c (n "metrix") (v "0.9.3") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0bc92zsyd9jgbk3z424nd23qff0ayg2ypw6lf9zhmjjayj0zlp9s")))

(define-public crate-metrix-0.9.4 (c (n "metrix") (v "0.9.4") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1yyk5w74qlbpiznaprw6miw79zwjg5c6nvs08sv1spfjk9nkj0lr")))

(define-public crate-metrix-0.9.5 (c (n "metrix") (v "0.9.5") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "105z4v8d57jmnafcd3hcyzyl4sz7q5y0888vi1w81as2il3dwp81")))

(define-public crate-metrix-0.9.6 (c (n "metrix") (v "0.9.6") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0g1x1c5cv3pdag0f7y9fpdqdwpaccg94sc2d1war0gki8s5q84yd")))

(define-public crate-metrix-0.9.7 (c (n "metrix") (v "0.9.7") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1nn09xryn8xpnwm31caaw2k9ha73x61n6sfacw4hsiiig4zkknl4")))

(define-public crate-metrix-0.9.8 (c (n "metrix") (v "0.9.8") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0akbjlkm6ys0xsc04pgg06a30lb0jid7ay78byssjzqvn7ymb2h4")))

(define-public crate-metrix-0.9.9 (c (n "metrix") (v "0.9.9") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.0") (d #t) (k 0)) (d (n "json") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1zmr8b9ywnr62h16rj1r9y2lr4r3xjsdcgh4x361k6mwjs36xf20")))

(define-public crate-metrix-0.9.10 (c (n "metrix") (v "0.9.10") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "15w8nppxmhnkws5nf7gi8sp2k8ay3lmj8p002yzr7q313km1m507")))

(define-public crate-metrix-0.9.11 (c (n "metrix") (v "0.9.11") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0l1qx9qr0590p1iql8ikckzzdl2xai4qlm38c33k9g1rq9kqngx3")))

(define-public crate-metrix-0.9.12 (c (n "metrix") (v "0.9.12") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0ql3ss2ia0q4q60iq4j7rhwkpsb91ahwzf8157r9g81f622qrqvf")))

(define-public crate-metrix-0.9.13 (c (n "metrix") (v "0.9.13") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "173w226dcbzrfnl3hajrk1v0pvaam1ngvf17544zv1bnr6zf0id4")))

(define-public crate-metrix-0.9.14 (c (n "metrix") (v "0.9.14") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "187isrz9g9k9bjiyxfvrj2zhqmj99k1mipz83yaafz98farardhz")))

(define-public crate-metrix-0.9.15 (c (n "metrix") (v "0.9.15") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0ynzs3acjbp5mivfg0i9m0mz0i4i6k4f11zjl0p5xyym2f8rmq5s")))

(define-public crate-metrix-0.9.16 (c (n "metrix") (v "0.9.16") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1y98rzvcvmdv5kdg2hadjcz1xsmp3mc17cqsc5jbxzn7ylny4s61")))

(define-public crate-metrix-0.9.17 (c (n "metrix") (v "0.9.17") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0p7wv8iawv0fqgw9ac81njsd3ddaqkx4gnkngpg1iifdf4x341k6")))

(define-public crate-metrix-0.9.18 (c (n "metrix") (v "0.9.18") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1xa9y7v5h89gq9ix293f05s1qq8l9wc47pa18gzdpxm9pmi1s3kp")))

(define-public crate-metrix-0.9.19 (c (n "metrix") (v "0.9.19") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1irgd4qvv7hq9db3c4h9cxm2rvbn1pvd32y844x2lj438177w63s")))

(define-public crate-metrix-0.9.20 (c (n "metrix") (v "0.9.20") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "17i61bian1k94snhkm672g2piz8w6mjnlva1xvky18gcssbm3arc")))

(define-public crate-metrix-0.9.21 (c (n "metrix") (v "0.9.21") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0s6kxkhn48s8mwcahcv6zganm7m2w1qdqs87d72g80qfbsxgbh6m")))

(define-public crate-metrix-0.9.22 (c (n "metrix") (v "0.9.22") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "03kjarxpz92lhv0gp80cbyc1c90zb2zz0qd3ln4a4vwj691z430i")))

(define-public crate-metrix-0.9.23 (c (n "metrix") (v "0.9.23") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1vjhqwmfwh2hyzsbb637nmiwn2pqqrp1i0mwdwz9vkq5dvb4w3cd")))

(define-public crate-metrix-0.9.24 (c (n "metrix") (v "0.9.24") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0v3ararb7dc2z2qd4xhnzpxly6kj5yqk5m8w4544dwj6vf45nlqs")))

(define-public crate-metrix-0.10.0 (c (n "metrix") (v "0.10.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1cmw500bz958x2jzan9687s89fv10rqz5irp144iajb0hialsjrp")))

(define-public crate-metrix-0.10.1 (c (n "metrix") (v "0.10.1") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1aa21g6w8sbki9yl4x29f4qn85dydj8nliqxjcwlnajz17kx8062")))

(define-public crate-metrix-0.10.2 (c (n "metrix") (v "0.10.2") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "026q83br8asbgrwxs3ik9kjnjhrmn9kgn75a67nvy05wfzd26bkf")))

(define-public crate-metrix-0.10.3 (c (n "metrix") (v "0.10.3") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "04kh9mx58ab1xdyfy7w1frzd17wn54svbz4gdwlw2x7qfzbk2a3i")))

(define-public crate-metrix-0.10.4 (c (n "metrix") (v "0.10.4") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1yld88rk5rbjarv0p75fimzjgcn880n07y4dw9nfqsa13cjybfa3")))

(define-public crate-metrix-0.10.5 (c (n "metrix") (v "0.10.5") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "134hrz7hqyad5wx1alqwlv7bd0w1bpagzfhgfpcv4p5xy3vpmsyx")))

(define-public crate-metrix-0.10.6 (c (n "metrix") (v "0.10.6") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "079q2kw89dm4lhxqx26d5mpz86rsw989jsdccfsy5azr8619a1zk")))

(define-public crate-metrix-0.10.7 (c (n "metrix") (v "0.10.7") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1wicrzqc3399k96jpq4flmrm05qqa0q6047vj672lla00rxklzdp")))

(define-public crate-metrix-0.10.8 (c (n "metrix") (v "0.10.8") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0pqv76aqqdzzx10i7psf2hfyh1vsns692jqvb31r6klrpbgvdcpb")))

(define-public crate-metrix-0.10.9 (c (n "metrix") (v "0.10.9") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "0r4v28lf5sj26lalnjskqx5m7k2xl7fdhhp4r0hbxb89jdjmpri8")))

(define-public crate-metrix-0.10.10 (c (n "metrix") (v "0.10.10") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "metrics") (r "^0.2.0") (d #t) (k 0)))) (h "1sn9lgsnnc9k47wvh86x2ci020wmm20byi3fk4qxr8mdv9a1zcz2")))

(define-public crate-metrix-0.10.11 (c (n "metrix") (v "0.10.11") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "184sxhssrxbvqr92jx5q7plcclkiwsmvwkhk1089afpawhbj1fcx")))

(define-public crate-metrix-0.10.12 (c (n "metrix") (v "0.10.12") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1dj0kq06nb25v3p5ixkly3vika9nxn814mf6dq52czxc76sj8v3v")))

(define-public crate-metrix-0.10.14 (c (n "metrix") (v "0.10.14") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "01y2wldlmcwf50fpingzbgnzd9j6b549wrrd9axkvkrd9x089h33")))

(define-public crate-metrix-0.10.15 (c (n "metrix") (v "0.10.15") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "117dnh12xvlqiarzwf7wns6sjjk8xs9gxpqq6mh104ib4m5jikxg")))

(define-public crate-metrix-0.11.0 (c (n "metrix") (v "0.11.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1dmcdr8q5b8a2qls9gvs4qb1lg23h7bbmh7c9l6lh3li2liszp20")))

(define-public crate-metrix-0.12.0 (c (n "metrix") (v "0.12.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1h73pz6rgxgqpxs14161mql3yxib890ivv77rdqr7jbq06mn22kc")))

(define-public crate-metrix-0.13.0 (c (n "metrix") (v "0.13.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "11yyrzz6vw18z463p0r2sxj72kbmi1izaw3qlv5kllsby80ijlrm")))

(define-public crate-metrix-0.13.1 (c (n "metrix") (v "0.13.1") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "09vp5mwlx54wyqg3cbwrjcfb4x49jbx9j1aa2dkdrknblsv9jpqz")))

(define-public crate-metrix-0.13.2 (c (n "metrix") (v "0.13.2") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "083r47cvz9k0xb90d3kp8wnbfgzi7xsmhz0bghg1mj5m80q1rwg8")))

(define-public crate-metrix-0.13.3 (c (n "metrix") (v "0.13.3") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "11n9xmpymgs8fdydf6sdqh263wrvb6la91jzm2kazy8glg29fkwh")))

(define-public crate-metrix-0.13.4 (c (n "metrix") (v "0.13.4") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1gydi9pp68y7fvy4k5v3zfhg8v616rcdhxlran4xgrcqzykyj4ba")))

(define-public crate-metrix-0.13.6 (c (n "metrix") (v "0.13.6") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1xjskg6n7avmmx106r1ccx4pyzfnrk0z8gpqd6ylm1kidr3ln8w2")))

(define-public crate-metrix-0.13.7 (c (n "metrix") (v "0.13.7") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1yngcp6zmm4nczrc5wy5v54kw3a1fq4vrbhc5j3wysgsrx1f8lzq") (y #t)))

(define-public crate-metrix-0.13.8 (c (n "metrix") (v "0.13.8") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "14l6i47bkk05wdw09vmz82yvwmxcn3yp0spnl99mjaj5m1khljm0")))

(define-public crate-metrix-0.13.9 (c (n "metrix") (v "0.13.9") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0609nz892x3aqagd7xm2dkphblaizfq0ydi0gpynghrfzijfyyvk")))

(define-public crate-metrix-0.13.10 (c (n "metrix") (v "0.13.10") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0wg0r7ancfmbg03ivlb4n61bd2p3lfj6hwwydi87m1pz7hm4hwqa")))

(define-public crate-metrix-0.13.11 (c (n "metrix") (v "0.13.11") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0) (p "futures")) (d (n "futures01") (r "^0.1") (o #t) (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0mimr4pbvfscdhqzhiqjgwgqy5bvjkhzbkdr2ck4qlqrzhgj3rlz")))

(define-public crate-metrix-0.13.12 (c (n "metrix") (v "0.13.12") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1lz0wjrw40his9xiwrlgaavix7r613a26w2k6lip04jaqcyvchfp")))

(define-public crate-metrix-0.13.13 (c (n "metrix") (v "0.13.13") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "exponential-decay-histogram") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0) (p "futures")) (d (n "jemalloc-ctl") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0yh3p7nbv56w06g7skv5nszfb8gvy66shkhwnl7p0jz4ykvdaly2")))

