(define-module (crates-io me tr metro-agent) #:use-module (crates-io))

(define-public crate-metro-agent-0.0.0 (c (n "metro-agent") (v "0.0.0") (d (list (d (n "metro-blackboard") (r "^0.0.0") (d #t) (k 0)) (d (n "metro-macros") (r "^0.0.0") (d #t) (k 0)))) (h "1nxhlp55w05lyyhmm2z6rw0y3qnzh6sx5mvh02g5vkrs22hkmacr") (f (quote (("serde" "metro-blackboard/serde" "metro-macros/serde") ("default" "serde"))))))

