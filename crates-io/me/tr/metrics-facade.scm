(define-module (crates-io me tr metrics-facade) #:use-module (crates-io))

(define-public crate-metrics-facade-0.1.0 (c (n "metrics-facade") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "metrics-core") (r "^0.4") (d #t) (k 0)))) (h "0lnvq5sc8m22v45lqqxbvys1h3531x6jva000rbn787nmwz1sc2r") (f (quote (("std"))))))

(define-public crate-metrics-facade-0.1.1 (c (n "metrics-facade") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "metrics-core") (r "^0.4") (d #t) (k 0)))) (h "0m3k6xdk3fjlcc6szh63j92nplmnywwpdrpidynx04nf1860dlp4") (f (quote (("std"))))))

