(define-module (crates-io me tr metrohash) #:use-module (crates-io))

(define-public crate-metrohash-0.1.0 (c (n "metrohash") (v "0.1.0") (d (list (d (n "fnv") (r "*") (d #t) (k 2)) (d (n "twox-hash") (r "*") (d #t) (k 2)))) (h "0rwqbl2ny2icl9a7s75bdmhp7qqy1qkwsxrgrf3lz77ldp4ib3ll")))

(define-public crate-metrohash-0.2.0 (c (n "metrohash") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "twox-hash") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1ndf6j7k0nd8sdrd7i20isqk6vldcv53k2vpq4g10182hpjbv5l3")))

(define-public crate-metrohash-1.0.0 (c (n "metrohash") (v "1.0.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "twox-hash") (r "^1.1.0") (d #t) (k 2)))) (h "10fgcqyy35km40lb059pzxlzbpad9q8ac41573sdg4wyc6nbj1zy")))

(define-public crate-metrohash-1.0.1 (c (n "metrohash") (v "1.0.1") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "twox-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0x0q8jk8kzqkl853hydla20m3nflg0n9yjnd1yw1id1nr11dh6n9")))

(define-public crate-metrohash-1.0.2 (c (n "metrohash") (v "1.0.2") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "twox-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0zca5b227kaf9cd1ahwdws3ym9zr63hwm21915b3cibcnciqx5fi")))

(define-public crate-metrohash-1.0.3 (c (n "metrohash") (v "1.0.3") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "twox-hash") (r "^1.1.0") (d #t) (k 2)))) (h "18zys1rsm2hdgjw3ypa5zl69dpvhkrf02rbznhqbayb1mv96wnv8")))

(define-public crate-metrohash-1.0.4 (c (n "metrohash") (v "1.0.4") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "twox-hash") (r "^1.1.0") (d #t) (k 2)))) (h "1lxigrm4ply8lli4vz9v6bi497a21qrzggsp1ggi7dwysb94alc5")))

(define-public crate-metrohash-1.0.5 (c (n "metrohash") (v "1.0.5") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "twox-hash") (r "^1.1.0") (d #t) (k 2)))) (h "14yra09i90x47aq1ak26g1c0s46sh8dil3ny784vk0rs0b3znp1c")))

(define-public crate-metrohash-1.0.6 (c (n "metrohash") (v "1.0.6") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "twox-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0v2hn33ypx79naimfcz58pz46qhj2prawvx1p9abrb72375m799v")))

