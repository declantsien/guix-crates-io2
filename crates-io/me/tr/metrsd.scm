(define-module (crates-io me tr metrsd) #:use-module (crates-io))

(define-public crate-metrsd-0.1.0 (c (n "metrsd") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "metrs_stubs") (r "^0.1.0") (f (quote ("serde" "sysinfo"))) (d #t) (k 0)) (d (n "ntex") (r "^0.6.3") (f (quote ("tokio" "rustls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27.7") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)))) (h "0mxdmar7g7bh24nsd9i5n8pj7zxq08s7krih63qibpfcpzz1c918")))

