(define-module (crates-io me tr metrics-jemalloc) #:use-module (crates-io))

(define-public crate-metrics-jemalloc-0.1.0 (c (n "metrics-jemalloc") (v "0.1.0") (d (list (d (n "jemalloc-ctl") (r "^0.3") (d #t) (k 0)) (d (n "metrics") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0yy67wss1xhk54d2xczaq2di8z47wxbgi3g0dlaz9hkl1ckkqf5b")))

(define-public crate-metrics-jemalloc-0.2.0 (c (n "metrics-jemalloc") (v "0.2.0") (d (list (d (n "metrics") (r "^0.18") (d #t) (k 0)) (d (n "tikv-jemalloc-ctl") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1gbi78svc7df8cjqxi1mhd4zy102xnl583vprd8r53cyxhrars2q") (y #t)))

(define-public crate-metrics-jemalloc-0.3.0 (c (n "metrics-jemalloc") (v "0.3.0") (d (list (d (n "metrics") (r "^0.18") (d #t) (k 0)) (d (n "tikv-jemalloc-ctl") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "16q2kqhiifrya8sklxc7hys9az0lg1l1j5zql42d5x7lv7x4ymvi")))

(define-public crate-metrics-jemalloc-0.2.1 (c (n "metrics-jemalloc") (v "0.2.1") (d (list (d (n "metrics") (r "^0.17") (d #t) (k 0)) (d (n "tikv-jemalloc-ctl") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09c0wqgzm7fpcpwwc2j37m54d98swp8b7s4szm6xx0lgwv2l0di3")))

(define-public crate-metrics-jemalloc-0.5.0 (c (n "metrics-jemalloc") (v "0.5.0") (d (list (d (n "metrics") (r "^0.19") (d #t) (k 0)) (d (n "tikv-jemalloc-ctl") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ljakss802wj63sfisnz0afnp0kcygar1lpl9rj0hpy48abfvyvy")))

(define-public crate-metrics-jemalloc-0.5.1 (c (n "metrics-jemalloc") (v "0.5.1") (d (list (d (n "metrics") (r "^0.20.1") (d #t) (k 0)) (d (n "tikv-jemalloc-ctl") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0ik4wy78iv0p1lr4049asah7dq9wdd24lwlqyskkzvrziv46fcy4")))

(define-public crate-metrics-jemalloc-0.5.2 (c (n "metrics-jemalloc") (v "0.5.2") (d (list (d (n "metrics") (r "^0.21.1") (d #t) (k 0)) (d (n "tikv-jemalloc-ctl") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1wanfidzp787vcirrrz080jzyai8ddk9wkvwv6s5clp9mbjjww84")))

