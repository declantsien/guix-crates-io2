(define-module (crates-io me tr metriki-log-reporter) #:use-module (crates-io))

(define-public crate-metriki-log-reporter-0.1.0 (c (n "metriki-log-reporter") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^0.1.0") (d #t) (k 0)))) (h "1gm6g5vr233adhhl4rv3acpr7nsbfvf5hix7hsax69rfrp3zd65b")))

(define-public crate-metriki-log-reporter-0.1.1 (c (n "metriki-log-reporter") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^0.1.1") (d #t) (k 0)))) (h "0msymirknc3jffa454a8m0a7vpgr6xlks8cndylhx7ihqimah0a0")))

(define-public crate-metriki-log-reporter-0.1.3 (c (n "metriki-log-reporter") (v "0.1.3") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^1.0") (d #t) (k 0)))) (h "02kh3ffzpmrdq8kmw7jyg6nn7nbwy668w4jv1m3zgiwc095vfaqn")))

(define-public crate-metriki-log-reporter-0.1.4 (c (n "metriki-log-reporter") (v "0.1.4") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^1.0") (d #t) (k 0)))) (h "14nr21s5wz483f2r9pszzw1w2m1icf6pq6h1n12nzcy2q8wl3lkq")))

(define-public crate-metriki-log-reporter-0.2.0 (c (n "metriki-log-reporter") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metriki-core") (r "^1.8") (d #t) (k 0)))) (h "07xlw681n9rh55938mi11f9953miv1m7j4v89r829ll8r9cwwx4p")))

