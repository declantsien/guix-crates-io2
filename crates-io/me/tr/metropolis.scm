(define-module (crates-io me tr metropolis) #:use-module (crates-io))

(define-public crate-metropolis-0.1.0 (c (n "metropolis") (v "0.1.0") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "168q011434kwlbf2w504ldqj58mp48pz7xsqnbdbkfngjqyrdjc5")))

(define-public crate-metropolis-0.1.1 (c (n "metropolis") (v "0.1.1") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "08xxpnzi97pm364mki8n6fl2j92yf0yn5fm6q4kdv6wlhgq24bl4")))

(define-public crate-metropolis-0.1.2 (c (n "metropolis") (v "0.1.2") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "1sjb7q32kwnzavi4sp20sb615k09j2f1pijyny4iklak19aij6vh")))

(define-public crate-metropolis-0.1.3 (c (n "metropolis") (v "0.1.3") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "0mbs6zj85m32cyfqy0nm7gdp8n95g5idqrgh7hgc937wyh8hrr0j")))

(define-public crate-metropolis-0.1.4 (c (n "metropolis") (v "0.1.4") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "16bps7y2pm2r16ckpk9q1agyj99pfli0zfybv0n3gpwsji6miq38")))

(define-public crate-metropolis-0.1.5 (c (n "metropolis") (v "0.1.5") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "0xjrpwfvw663gxya66faym3w31yag537nzrvka86a72hr88999dz")))

(define-public crate-metropolis-0.1.6 (c (n "metropolis") (v "0.1.6") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "0rcsk0yry86smvwsah4yc1qfnf0l2dbfchf5qjyj72w8ypf47pk5")))

(define-public crate-metropolis-0.1.7 (c (n "metropolis") (v "0.1.7") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "0k8rzh9zsl3208w29j5i8j9z96pvl9isrd2qsgkn29hkp5vcv3zf")))

(define-public crate-metropolis-0.1.8 (c (n "metropolis") (v "0.1.8") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "0hps6vik07vff9yf4qgj650rvlfsw9yiy51m67fw31c3jp46ics3")))

(define-public crate-metropolis-0.2.0 (c (n "metropolis") (v "0.2.0") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "1wswfiqj72q303bcknksilj2z41lw0s479jszzpf8r2jd97h88p6")))

(define-public crate-metropolis-0.3.0 (c (n "metropolis") (v "0.3.0") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "0mklnr95v11sxv2kzhfn6zh93xl684ajhhrzr1al3r56anflgkgd")))

(define-public crate-metropolis-0.3.1 (c (n "metropolis") (v "0.3.1") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "1kdnbhpbn2n24b4bhcy0w67kqcb4z67300jpdcg8mvvc7qdr7999")))

(define-public crate-metropolis-0.3.2 (c (n "metropolis") (v "0.3.2") (d (list (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "1gqhi2xxfzh9qjdnf8mkyqm2kjm1yim47vmq2w49fravq9gakprd")))

(define-public crate-metropolis-0.3.3 (c (n "metropolis") (v "0.3.3") (d (list (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "1bc4d9phgggyx80zyljnagvgfmajglfhl9qx8yx51xfsy283lx4s")))

(define-public crate-metropolis-0.4.0 (c (n "metropolis") (v "0.4.0") (d (list (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "14jmqzkzvw8g5szh9p7vjipf09dz7yn3ykxabgiap7jxhdzzbdr1")))

(define-public crate-metropolis-0.4.1 (c (n "metropolis") (v "0.4.1") (d (list (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "1xjb47i4bfafvlb57626s5ri5ivhfjxkvahcnky6ds096bcfva68")))

(define-public crate-metropolis-0.5.0 (c (n "metropolis") (v "0.5.0") (d (list (d (n "png") (r "^0.15.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "0015qp7djs0qjdygyb56mmxic7dhhqj3m7yjzvw3fysi6wjknzhm")))

(define-public crate-metropolis-0.5.1 (c (n "metropolis") (v "0.5.1") (d (list (d (n "png") (r "^0.15.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "0ffgc5i7ny8zvgyxvprjhd0y9h8viihd94gxknmzv735scqcddx8")))

(define-public crate-metropolis-0.6.0 (c (n "metropolis") (v "0.6.0") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "1nxvlg6wrz1b0jw33idpbkk8h5b40wgayc5396x6zyb14p04wlnj")))

(define-public crate-metropolis-0.7.0 (c (n "metropolis") (v "0.7.0") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "11pksdii40v5v4yf09350maan35x8myxnrm8sss6rxzhvsklg7yk")))

(define-public crate-metropolis-0.8.0 (c (n "metropolis") (v "0.8.0") (d (list (d (n "png") (r "^0.15") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "1323b83capqbync3isr2n57p4jm1pkhv7gzwxivw0llz5zfzgqjf")))

(define-public crate-metropolis-0.9.0 (c (n "metropolis") (v "0.9.0") (d (list (d (n "png") (r "^0.15") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "0jq570cz6aik8kjvfhacfsb48h1vrzs2dvvkb99qm0fs0r45lwzd")))

(define-public crate-metropolis-0.9.1 (c (n "metropolis") (v "0.9.1") (d (list (d (n "png") (r "^0.15") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.16.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.16.0") (d #t) (k 0)) (d (n "winit") (r "^0.19.5") (d #t) (k 0)))) (h "0kndbrh20na9hm2x06iafpiwrh6s0dm7cca05pzmgflhaa8vy51i")))

