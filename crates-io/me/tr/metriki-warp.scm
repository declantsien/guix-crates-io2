(define-module (crates-io me tr metriki-warp) #:use-module (crates-io))

(define-public crate-metriki-warp-0.1.0 (c (n "metriki-warp") (v "0.1.0") (d (list (d (n "metriki-core") (r "^0.1.2-alpha.0") (d #t) (k 0)) (d (n "warp") (r "^0.3") (k 0)))) (h "10aw9rlnxprsq5fx0zwxpaw3ywzmznbwhg3shrki7d4gd3g5fr4a")))

(define-public crate-metriki-warp-0.1.1 (c (n "metriki-warp") (v "0.1.1") (d (list (d (n "metriki-core") (r "^1.0") (d #t) (k 0)) (d (n "warp") (r "^0.3") (k 0)))) (h "05nkbhjbs9xzqvmq1h8cfrimi5rgrdlcy7yxncf7ygi9mznjwanx")))

