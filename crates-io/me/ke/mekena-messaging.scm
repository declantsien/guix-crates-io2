(define-module (crates-io me ke mekena-messaging) #:use-module (crates-io))

(define-public crate-mekena-messaging-0.1.0 (c (n "mekena-messaging") (v "0.1.0") (d (list (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "miette") (r "^5.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1spq8h383y46hq4kwhxyz9hqjz94x950fmfcwqfb6wzmdizbwf5b") (r "1.62")))

