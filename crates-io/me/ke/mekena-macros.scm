(define-module (crates-io me ke mekena-macros) #:use-module (crates-io))

(define-public crate-mekena-macros-0.1.0 (c (n "mekena-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1ql8hx8gj71z82zwhlpxw8jip6983nx971bsiz16dbrks2wkimz5") (r "1.62")))

