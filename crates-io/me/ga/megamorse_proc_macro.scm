(define-module (crates-io me ga megamorse_proc_macro) #:use-module (crates-io))

(define-public crate-megamorse_proc_macro-0.1.0 (c (n "megamorse_proc_macro") (v "0.1.0") (d (list (d (n "megamorse_core") (r "^0.1.0") (d #t) (k 0)))) (h "1pzy1n3lmsafc1gxpi2dq8yxwchyc5c0hi1ss8s57ak29nhsdqp3")))

(define-public crate-megamorse_proc_macro-1.0.0 (c (n "megamorse_proc_macro") (v "1.0.0") (h "1jr0rcf75wij9irsa8n2gsi1wd3gr223vwlclimgyldblz649a8n")))

(define-public crate-megamorse_proc_macro-1.0.1 (c (n "megamorse_proc_macro") (v "1.0.1") (h "0bh4havl9x4hfrmq87y4hajg3k1gdi8ljm800wg0z4nw4afp2cgl")))

