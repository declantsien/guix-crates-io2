(define-module (crates-io me ga megalock) #:use-module (crates-io))

(define-public crate-megalock-0.1.0 (c (n "megalock") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "14cry2ngl0wajkl5id032rbhqaga61xcbf35xpvj4dbncvg9wl1k")))

