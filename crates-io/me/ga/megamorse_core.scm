(define-module (crates-io me ga megamorse_core) #:use-module (crates-io))

(define-public crate-megamorse_core-0.1.0 (c (n "megamorse_core") (v "0.1.0") (h "1bjq5f6dnavl5f8szks7f6zmyj61s04chpfiaqqxainnbm94pnhd")))

(define-public crate-megamorse_core-1.0.0 (c (n "megamorse_core") (v "1.0.0") (h "0dxhisc5z5wiqfqqrjyycdpv0jyhs6hwrnhwb0gw2dr6xirym9h7")))

(define-public crate-megamorse_core-1.0.1 (c (n "megamorse_core") (v "1.0.1") (h "1x9g3kyzf6bh0nvs0lvq0n6piccfagzm1zywxf4wlgvisgd6mzgx")))

