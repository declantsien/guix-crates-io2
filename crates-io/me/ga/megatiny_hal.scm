(define-module (crates-io me ga megatiny_hal) #:use-module (crates-io))

(define-public crate-megatiny_hal-0.1.0 (c (n "megatiny_hal") (v "0.1.0") (d (list (d (n "bare-metal") (r "0.2.*") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "0.1.*") (d #t) (k 0)))) (h "027jrv7gj4r0kw8laik8yq5yd0835iqrrpccx8aphmzbgnzwnb6b")))

(define-public crate-megatiny_hal-0.2.0 (c (n "megatiny_hal") (v "0.2.0") (d (list (d (n "bare-metal") (r "0.2.*") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "vcell") (r "0.1.*") (d #t) (k 0)))) (h "00gqs0h7v3nz63w9na0y9zq6rgv47js8pxim5bm54l196bpvkv61")))

