(define-module (crates-io me ga mega_etl) #:use-module (crates-io))

(define-public crate-mega_etl-0.1.0 (c (n "mega_etl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "hyper_wasi") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mysql_async_wasi") (r "^0.30") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio_wasi") (r "^1.21") (f (quote ("net"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "07vhxj8npyha11qj19ai5795p9x6g6kzc5fggpxzr7s28vd629wx")))

(define-public crate-mega_etl-0.1.1 (c (n "mega_etl") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "hyper_wasi") (r "^0.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mysql_async_wasi") (r "^0.30") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio_wasi") (r "^1.21") (f (quote ("net"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1a0gz9lfk4n69hxrlzp77bv4r6x5i99bljf4j4mxjfwmhnkrhb5h")))

