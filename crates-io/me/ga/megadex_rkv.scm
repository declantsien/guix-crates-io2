(define-module (crates-io me ga megadex_rkv) #:use-module (crates-io))

(define-public crate-megadex_rkv-0.1.0 (c (n "megadex_rkv") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "rkv") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 0)))) (h "1ahqajqr5m6pwnrg23izywrn9bs5cx5wqhbw21aazxmn7g65wpvr")))

(define-public crate-megadex_rkv-0.1.1 (c (n "megadex_rkv") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "rkv") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 0)))) (h "1mm9jfyb06d5pahsphjcrqa5za77lq74291wszvsq5bzkkmbkw2g")))

