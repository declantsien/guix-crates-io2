(define-module (crates-io me ga megaui-macroquad) #:use-module (crates-io))

(define-public crate-megaui-macroquad-0.1.0 (c (n "megaui-macroquad") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "megaui") (r "=0.2.14") (d #t) (k 0)))) (h "06myaz3z938sqv19chbkan6c87p63msx0y3692llqrqm45mivsx5")))

(define-public crate-megaui-macroquad-0.1.1 (c (n "megaui-macroquad") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "megaui") (r "=0.2.14") (d #t) (k 0)))) (h "0l7z1wrf3j0px6hnn0sfilaffsgx4mhz2qp5zjynvzhy30a67zwz")))

(define-public crate-megaui-macroquad-0.1.2 (c (n "megaui-macroquad") (v "0.1.2") (d (list (d (n "macroquad") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "megaui") (r "=0.2.14") (d #t) (k 0)))) (h "1xc54gnl7g62m4ql74ypbbyz2ad9dnphyp7smhk6cghlaygr39bk")))

(define-public crate-megaui-macroquad-0.1.3 (c (n "megaui-macroquad") (v "0.1.3") (d (list (d (n "macroquad") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "megaui") (r "=0.2.15") (d #t) (k 0)))) (h "1h7c8rk7w6ga2zpg4gg5jai1g6iz0ap5yxw6ka43r0jjnmp4sfgq")))

(define-public crate-megaui-macroquad-0.1.4 (c (n "megaui-macroquad") (v "0.1.4") (d (list (d (n "macroquad") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "megaui") (r "^0.2") (d #t) (k 0)))) (h "07w33sryga93h0knp9n98fwlpi2x50r657rsvl99i0jdhxdh6kbd")))

