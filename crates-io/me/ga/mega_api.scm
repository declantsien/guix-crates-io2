(define-module (crates-io me ga mega_api) #:use-module (crates-io))

(define-public crate-mega_api-0.1.0 (c (n "mega_api") (v "0.1.0") (d (list (d (n "aes") (r "^0.7.4") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "0wpn67sv5kai1n1igy5x43s0j6ln075k9dvhv7cs5m3qpyz06014")))

(define-public crate-mega_api-0.1.1 (c (n "mega_api") (v "0.1.1") (d (list (d (n "aes") (r "^0.7.4") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)))) (h "1w5671a2y19wmc8vcarlpc57634av7r1zxm66lb3gyqkgfbiyyh1")))

