(define-module (crates-io me ga megamorse) #:use-module (crates-io))

(define-public crate-megamorse-0.1.0 (c (n "megamorse") (v "0.1.0") (d (list (d (n "megamorse_core") (r "^0.1.0") (d #t) (k 0)) (d (n "megamorse_proc_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1zfr1sadsb9rf5pw88fvdxdc5sfnangxwgpn18iq3jwp93464chy")))

(define-public crate-megamorse-1.0.0 (c (n "megamorse") (v "1.0.0") (d (list (d (n "megamorse_core") (r "^1.0.0") (d #t) (k 0)) (d (n "megamorse_proc_macro") (r "^1.0.0") (d #t) (k 0)))) (h "0mwc97m9q8pnrx016f9hpd194x8fw03k11ymmwbxk16clx0msndz")))

(define-public crate-megamorse-1.0.1 (c (n "megamorse") (v "1.0.1") (d (list (d (n "megamorse_core") (r "^1.0.1") (d #t) (k 0)) (d (n "megamorse_proc_macro") (r "^1.0.1") (d #t) (k 0)))) (h "04v3fxd7hig99lxqbdn71hwp936js5p6bcs1s0qq253rr35l1ycv")))

