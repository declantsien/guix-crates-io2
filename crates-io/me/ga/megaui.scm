(define-module (crates-io me ga megaui) #:use-module (crates-io))

(define-public crate-megaui-0.1.0 (c (n "megaui") (v "0.1.0") (h "1raknc7hxdg30hhnpz4yy4czm5qxxllawxgc1msy9hd65csqhixf")))

(define-public crate-megaui-0.1.1 (c (n "megaui") (v "0.1.1") (h "1ppv5bvzr0q6wjsg1lq843dhq5b6r3z6pdmp9awvqp1i4ilnp09b")))

(define-public crate-megaui-0.1.3 (c (n "megaui") (v "0.1.3") (h "0n924vbf7ang9451ldgl03mj3igk1ld8p9i8dx998k6d8wzi9xf6")))

(define-public crate-megaui-0.1.4 (c (n "megaui") (v "0.1.4") (h "1jmhljdfw14i21hi6r0lxikb7dg15zhmj9m0aqwma2fwzr1pyadl")))

(define-public crate-megaui-0.1.5 (c (n "megaui") (v "0.1.5") (h "1v8bixblr46wbc952a6kqz6qpnfs85w73kh5rbs8bmmdplf3a2dg")))

(define-public crate-megaui-0.2.0 (c (n "megaui") (v "0.2.0") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (d #t) (k 0)))) (h "0h3h0c7v2m6h240x698lp1kslk3rrladshlkb0zdfx4p1350nqq3")))

(define-public crate-megaui-0.2.1 (c (n "megaui") (v "0.2.1") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (d #t) (k 0)))) (h "1b9csigaqdkbx2hypfjn9ppjn114likjnd8ir4bvsnzngva02isi")))

(define-public crate-megaui-0.2.2 (c (n "megaui") (v "0.2.2") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (d #t) (k 0)))) (h "1jcfb3hs3cbnkszsdc6sd9pfwynj675vkmrga0vqz31wj78nf9gd")))

(define-public crate-megaui-0.2.3 (c (n "megaui") (v "0.2.3") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (d #t) (k 0)))) (h "08w26ggv7vhiixx40zjbmvd0kq32qz2v04y23s5x4afhb0v6gfyp")))

(define-public crate-megaui-0.2.4 (c (n "megaui") (v "0.2.4") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (d #t) (k 0)))) (h "1lr3yqpk6nxm9qmgd464bk68g1jbzbk398y6knf8izky97slg41z")))

(define-public crate-megaui-0.2.5 (c (n "megaui") (v "0.2.5") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (d #t) (k 0)))) (h "1ayly31sc1myvwrhwm9aa2wyfg6m8libi0kckvqr7wq59p2vxz20")))

(define-public crate-megaui-0.2.6 (c (n "megaui") (v "0.2.6") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "0w5hwxcwv1d44v2advzpr522ksxm16dyaqx6a0aazp5g18bg42nq")))

(define-public crate-megaui-0.2.7 (c (n "megaui") (v "0.2.7") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "1v0gsbmls4ci8l7vv3vpnpnrxhsvhgwzfcdwr1kcq5701spkjgaf")))

(define-public crate-megaui-0.2.8 (c (n "megaui") (v "0.2.8") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "0z67xkqvy77xh9f6raq58bbl3wiy2v2cf284kc460wfqvclxdpb6")))

(define-public crate-megaui-0.2.9 (c (n "megaui") (v "0.2.9") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "0c15km8k6n2mfc2jgd8di3gxa0hsxvshmmyrr8kq1xyb95asfmd3")))

(define-public crate-megaui-0.2.10 (c (n "megaui") (v "0.2.10") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "1qkg8g7lc99f6h4lwjnl2x7vn3ksfa2sv2wywzwvh8kim7bc0y0p")))

(define-public crate-megaui-0.2.11 (c (n "megaui") (v "0.2.11") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "183kj5vvrypzmfyglpffhf9ian551r45klpxhxsi60adkicqyc22")))

(define-public crate-megaui-0.2.12 (c (n "megaui") (v "0.2.12") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "1r5gdavz47adqq45ng1c8rvw2g3z96ppgs90vl8mz2n7ajpv2w1l")))

(define-public crate-megaui-0.2.13 (c (n "megaui") (v "0.2.13") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "11wrq7gc1cbgw3cl9l84gg0187vr081h7wncfmdk5wj78wi4i24f")))

(define-public crate-megaui-0.2.14 (c (n "megaui") (v "0.2.14") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "1mqp1655w3hk3jqdgihmiy2aj81f4brwffv3ghxmhrkp694gjl4j")))

(define-public crate-megaui-0.2.15 (c (n "megaui") (v "0.2.15") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "0xnm6qgxmsp0bmb12m18k36v2yzjzx8zm19zdy7hv5kzsy0r0vq5")))

(define-public crate-megaui-0.2.16 (c (n "megaui") (v "0.2.16") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "0pv0kpd6jfc71jpgahfqm96rqjwgbh8d7n8jxzv5b30kh7h6myx1")))

(define-public crate-megaui-0.2.17 (c (n "megaui") (v "0.2.17") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "1nw4jyd43ng28cpic0i1jsp2b868yryywwqa3bg63m0a2mqdqgvp")))

(define-public crate-megaui-0.2.18 (c (n "megaui") (v "0.2.18") (d (list (d (n "miniquad_text_rusttype") (r "^0.1") (k 0)))) (h "08hfyps55rnhmmhg1r0ywppbalq1jxlpahdy57kwbvqi8nslr9p2")))

