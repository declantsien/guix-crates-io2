(define-module (crates-io me ga megam_rustyprint) #:use-module (crates-io))

(define-public crate-megam_rustyprint-0.1.1 (c (n "megam_rustyprint") (v "0.1.1") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "term-painter") (r "^0.1.1") (d #t) (k 0)))) (h "1mrl602qgh1kyw2x00v3hhyaac3hnzddixpvrrnzab2xy0ra5688")))

(define-public crate-megam_rustyprint-0.2.0 (c (n "megam_rustyprint") (v "0.2.0") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "term-painter") (r "^0.1.1") (d #t) (k 0)))) (h "1nqgj9vk1k55fcbm2b7z99r8lfphszhpz6cq6ias4mqvfbx307qa")))

(define-public crate-megam_rustyprint-0.2.1 (c (n "megam_rustyprint") (v "0.2.1") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "term-painter") (r "^0.1.1") (d #t) (k 0)))) (h "1dq0isng1rd9nab52zn6mckh5gsp4my7b6bp15ckini7r1hvf56f")))

