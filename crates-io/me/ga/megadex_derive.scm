(define-module (crates-io me ga megadex_derive) #:use-module (crates-io))

(define-public crate-megadex_derive-0.1.0 (c (n "megadex_derive") (v "0.1.0") (d (list (d (n "megadex_rkv") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.24") (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 2)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "1gclvllphj7jw2mc95v40gzsvlasscyaz7ywr3mawsv3x59ycvvy")))

