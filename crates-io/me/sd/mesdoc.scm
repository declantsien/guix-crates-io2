(define-module (crates-io me sd mesdoc) #:use-module (crates-io))

(define-public crate-mesdoc-0.1.0 (c (n "mesdoc") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0g7q7p0379q8dxv8qxg7akj0ndh5hg41aaljz3hmq1fqq75s96zb")))

(define-public crate-mesdoc-0.1.1 (c (n "mesdoc") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1rw1r219np0myf88jbhnmapxvby54cw50aj2sc2pclxcmmlvvgkh")))

(define-public crate-mesdoc-0.1.2 (c (n "mesdoc") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0hgl5vi5ra70sms069ygdzf0k724j7ndk226iiawl11y1rhdp1g0")))

(define-public crate-mesdoc-0.1.3 (c (n "mesdoc") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1i8lny2w4z6w7y186yzzdlwmi2l98n78h0m35fkdla9ngilpkmp5")))

(define-public crate-mesdoc-0.1.4 (c (n "mesdoc") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1i0jr6v8zv0cxg4sigqy0904wfqw2930gmlmskp9m80f0x8jwhsl")))

(define-public crate-mesdoc-0.1.5 (c (n "mesdoc") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1y61mm22mdf7s51f9p4ldrjx62rm3czndg0la9dw3hvqj681ssfj")))

(define-public crate-mesdoc-0.1.6 (c (n "mesdoc") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0hgp28mjv3x3sap4g0fdqa6v5gyrldpvl7jwb4rhxi79qkpp63gw")))

(define-public crate-mesdoc-0.1.7 (c (n "mesdoc") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "15svmhymm1yii8s9lx42s8150np0inmxq6hmzdvvyw2rgnv8ci36")))

(define-public crate-mesdoc-0.1.8 (c (n "mesdoc") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0cqawx1wcm8g149ks9iq8khx8mhrdgaps7hanrbiz05jkdj1cm6v")))

(define-public crate-mesdoc-0.1.9 (c (n "mesdoc") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1if57g4iga6k7w9absn55ggva2ffnxpx8gcqv8a9r1wc0vpnkq4j")))

(define-public crate-mesdoc-0.1.10 (c (n "mesdoc") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0grwqdqf11nw9wsafvnxc93g8m0bb0333980wfnkxvpwxjb56j0i")))

(define-public crate-mesdoc-0.1.11 (c (n "mesdoc") (v "0.1.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0k9h48qvlljxifdywsc4wiy07lvvavwzq6jabsqhnirb2hah5lm9")))

(define-public crate-mesdoc-0.1.12 (c (n "mesdoc") (v "0.1.12") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0q8hm6xdij8zv0dwk7ggzk8xd16z5n0n5g3vwm9c20f61fda2hfr")))

(define-public crate-mesdoc-0.1.13 (c (n "mesdoc") (v "0.1.13") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "13slghi6a5390rrqlklp5b80ms6kzvj8ifsl1vimi2hp2d9dfld2")))

(define-public crate-mesdoc-0.1.14 (c (n "mesdoc") (v "0.1.14") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1w4a1mzwmhlajrr5plr5gv03wy97nkn0mnpihq7r3sd7m6vfvaic")))

(define-public crate-mesdoc-0.1.15 (c (n "mesdoc") (v "0.1.15") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1s14hrm26a77vk7ia2045kkkx8907llap9aisx78d3r5yk66f5cp")))

(define-public crate-mesdoc-0.1.16 (c (n "mesdoc") (v "0.1.16") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1qpjm6aqip1g60lfcyg4krk298mgr6ic2fxkcl01fdb6z10n6ajz")))

(define-public crate-mesdoc-0.1.17 (c (n "mesdoc") (v "0.1.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1kb8cqzvh2ws3zm6sv32h1b2l6vrmwvgx0x6qmfvi5fh2pcy73wa")))

(define-public crate-mesdoc-0.1.18 (c (n "mesdoc") (v "0.1.18") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1aslk54s0dx2wpnswsbx7vdw1fin3316avwqanx1y54jm8pbs5xw")))

(define-public crate-mesdoc-0.1.19 (c (n "mesdoc") (v "0.1.19") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1g65cmylrpm7wpjdnyrk9qbkyjk3pfvp4m3fibpfl12c4sri9nfn")))

(define-public crate-mesdoc-0.1.20 (c (n "mesdoc") (v "0.1.20") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0bgi3533sx520v2znc76g2mqcnf5fhs0523gm2nbdz4p6n7j16q9")))

(define-public crate-mesdoc-0.1.21 (c (n "mesdoc") (v "0.1.21") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0czvig8xxqx6h6llmr2r6m6ksjadc2nspr08ys973kpxdw6y7rp3")))

(define-public crate-mesdoc-0.1.22 (c (n "mesdoc") (v "0.1.22") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0qccg4c1g6yfblpms4k2hlpcr21ki67rq04wdm9r7scjllmzjq7p")))

(define-public crate-mesdoc-0.1.23 (c (n "mesdoc") (v "0.1.23") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1ashzdlp5z3bkh25yifvb3ciqw07i67yl77zsisb13p5s4lvz50h")))

(define-public crate-mesdoc-0.1.24 (c (n "mesdoc") (v "0.1.24") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "02ggdb3nsvg06kxva7wia3pi51ja45xjs44lwbdqw007k23ha4g7")))

(define-public crate-mesdoc-0.2.0 (c (n "mesdoc") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0jx8w623dqj218rb2mlikp1dliqmibjdlm2v8l9x6w38zzkj2x7g")))

(define-public crate-mesdoc-0.2.1 (c (n "mesdoc") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1x5nzc218ylakrfyya68qfnlbxxhzx0h2lw745zqkfmn54vbnidl")))

