(define-module (crates-io me ri meritrank) #:use-module (crates-io))

(define-public crate-meritrank-0.4.0 (c (n "meritrank") (v "0.4.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (d #t) (k 0)))) (h "1ddvx5pm5c881fmjq85xc1l00fpv76dcickz4r0mykrq6nq28jvm")))

