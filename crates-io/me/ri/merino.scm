(define-module (crates-io me ri merino) #:use-module (crates-io))

(define-public crate-merino-0.1.0 (c (n "merino") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "snafu") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0s424lblqq5xdqcskj454pca2p238azmbrlc67dyrnai2cz5m53f")))

(define-public crate-merino-0.1.2 (c (n "merino") (v "0.1.2") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1gw8qxl6x3zdqh9j1shaq6lhlgfx9bw91gvipwh83lrlrblxkkk1")))

(define-public crate-merino-0.1.3 (c (n "merino") (v "0.1.3") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1k8hwcmkzaqd4z7hybbf5qwz5xxd4v5zsri63l6p1zfgj1sqd9wz")))

