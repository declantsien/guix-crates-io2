(define-module (crates-io me lc melcloud-api) #:use-module (crates-io))

(define-public crate-melcloud-api-0.1.0 (c (n "melcloud-api") (v "0.1.0") (d (list (d (n "mockito") (r "^0.17.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1r0qi85aglh1wrm1idaf7ylnjg5mrsp70c4nx330vh9dqgkdzfcl")))

(define-public crate-melcloud-api-0.1.1 (c (n "melcloud-api") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "mockito") (r "^0.17.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "193la7khbkl4f3pgh5y0ywxj401j10nqq671p6b78mmigk5a0yxj")))

