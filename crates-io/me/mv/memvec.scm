(define-module (crates-io me mv memvec) #:use-module (crates-io))

(define-public crate-memvec-0.0.1 (c (n "memvec") (v "0.0.1") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1y5mdq78dhv8lk32phvb3fczsxcj0rjwj7pk6pb8pxxm9i4dvpbx") (y #t)))

(define-public crate-memvec-0.0.2 (c (n "memvec") (v "0.0.2") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "14w6gm2m7sdghj1sizar2j5irlh4i30cyn99i0yjrpnmlsgxddw3")))

(define-public crate-memvec-0.0.3 (c (n "memvec") (v "0.0.3") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1pasx5bxh1brip5cvrx3jydfaj29fldpn8wjqp5ki475h7rkxadq")))

(define-public crate-memvec-0.0.4 (c (n "memvec") (v "0.0.4") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "004q8w9ngd4cqsbg37aisnxzif281sd33fkfzgdmx8g70bc691jg")))

(define-public crate-memvec-0.0.5 (c (n "memvec") (v "0.0.5") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1f09lx668is7hq6v82bl4s5nk6mx6q2swgdibk85avwjcw7pvwal")))

(define-public crate-memvec-0.1.0 (c (n "memvec") (v "0.1.0") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1pnwr50h6jdx5xc3xdz0971xz4j41ifkx3jzj2pnzx97w21q61wm")))

