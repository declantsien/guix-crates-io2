(define-module (crates-io me ta meta_fetcher) #:use-module (crates-io))

(define-public crate-meta_fetcher-0.1.0 (c (n "meta_fetcher") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "select") (r "^0.6.0") (d #t) (k 0)) (d (n "texting_robots") (r "^0.2.2") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "08i8j4a7nqs98w03y3vp1bqjc3xlq7792r2hq9rhjcng4n1svszb") (f (quote (("network-tests"))))))

(define-public crate-meta_fetcher-0.1.1 (c (n "meta_fetcher") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "select") (r "^0.6.0") (d #t) (k 0)) (d (n "texting_robots") (r "^0.2.2") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)))) (h "1jvpid9zxc8hc3dnjylv09g15ixn99nyxd1chpkjghayfkws578v") (f (quote (("network-tests"))))))

