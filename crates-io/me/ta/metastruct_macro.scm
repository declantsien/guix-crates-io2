(define-module (crates-io me ta metastruct_macro) #:use-module (crates-io))

(define-public crate-metastruct_macro-0.1.0 (c (n "metastruct_macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "0g8p8hfjdz6xjgz3mr3njivpnl3lilf3lv1yl83sbhkhfmgd3pj2")))

(define-public crate-metastruct_macro-0.1.1 (c (n "metastruct_macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (d #t) (k 0)))) (h "029rqyyhq7qfnpj627687i743dnm603mvjzq6yjpsyv7sm2l1jrp")))

