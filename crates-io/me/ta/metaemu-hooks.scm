(define-module (crates-io me ta metaemu-hooks) #:use-module (crates-io))

(define-public crate-metaemu-hooks-0.3.0 (c (n "metaemu-hooks") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fugue") (r "^0.2.8") (d #t) (k 0)) (d (n "metaemu-state") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0yj4g6grvcmsqc23hx9d03aqz268jmlfgck4slgyx9p7symv9f71")))

