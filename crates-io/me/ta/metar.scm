(define-module (crates-io me ta metar) #:use-module (crates-io))

(define-public crate-metar-0.1.0 (c (n "metar") (v "0.1.0") (h "1fryqkv87293yxfvy94rj02rxd3m6p5z2mngfi951wk62xs20v2w")))

(define-public crate-metar-0.1.1 (c (n "metar") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wqiq2sbfvrizyfy9dalnaigzgi376sbswm3nhh82isl93ib06q7")))

(define-public crate-metar-0.1.2 (c (n "metar") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kfcj3xwcrx8f4hvcnnwnj4rfc8dl6g87w7hc5zjx8wh26278236")))

(define-public crate-metar-0.1.3 (c (n "metar") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nraqzmgjjxcfiqkqbl5i6iv5mahs9hidyvpa7v098xnap1axjfh")))

(define-public crate-metar-0.1.4 (c (n "metar") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sbnhanakagypslng1l761s4r3bm4m62ng0ncgy868nzqwaqf1a1")))

(define-public crate-metar-0.1.5 (c (n "metar") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1x5vslysajsc2w26y7p9kwqi9y80nda9ihhs7jk2liqim2gnjlxf")))

(define-public crate-metar-0.2.0 (c (n "metar") (v "0.2.0") (h "10b5xxgi8gg2cxiz04mhwlpkdy0lslad57a398hggp1igv8j6vlm")))

(define-public crate-metar-0.2.1 (c (n "metar") (v "0.2.1") (h "0lhgpfc5dh0z9l5avxaydmshl05wbgry8hpx5qlb4a50s4xfvps8")))

(define-public crate-metar-0.2.2 (c (n "metar") (v "0.2.2") (h "1d0krxjms9mlknncfj7lsmxhqniywx37x4n1d3wck8x2wmvzskvn")))

(define-public crate-metar-0.2.3 (c (n "metar") (v "0.2.3") (h "1dajg7006yqs12hi27vz3gnh7zxy8kbqw58sxxbcim8x4d7xfym5")))

(define-public crate-metar-0.2.4 (c (n "metar") (v "0.2.4") (h "1hmyn4q1xmxi3irdx92axrfdsznr957fqyiswjjjzgvvb5pl54ma")))

(define-public crate-metar-0.3.0 (c (n "metar") (v "0.3.0") (h "0r0wmwq8my5k82vvhyrmqxfibxm1lnzv1l12i12x7dfn3xr9s3rp")))

(define-public crate-metar-0.4.0 (c (n "metar") (v "0.4.0") (h "1pgfbg5k13bmgchp2x5nycwlk8k5jlc0jcln4c59ap2msk71af19")))

(define-public crate-metar-0.4.1 (c (n "metar") (v "0.4.1") (h "1a7nnfcld5my8rfgs1pk4g0ksiw6fw9m1j4yxb0bqlm7gbq6k37r")))

(define-public crate-metar-0.5.0 (c (n "metar") (v "0.5.0") (h "1qwyd2d31idnwhnxm0y7vp52dxqy6m342qd3ly38yxf36mi94imy")))

(define-public crate-metar-0.6.0 (c (n "metar") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0azh1qwhsjdl5bl7km6n1r7cjfdkxpshm1c37k8za1m1i76y7sij")))

(define-public crate-metar-0.6.1 (c (n "metar") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "12b6kc3f6s08g74b9zyshnhr3lrn92bpkvkajr3ia4033c02zhl9")))

(define-public crate-metar-0.6.2 (c (n "metar") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1f8jjjkq5b0h5x34l6ngxssnya3d2nlncq69f0d82fhyp1pqy2wq")))

(define-public crate-metar-0.6.3 (c (n "metar") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1yxfnrfnxidfksd77kx6hqkd8shjs7hz3mjr8hwxaz0l660ggk8v")))

(define-public crate-metar-0.6.4 (c (n "metar") (v "0.6.4") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1kzhyl681qiyc6hr7pwj07y96fdjlshrq3dyj2mj3qvk25f4bb86")))

(define-public crate-metar-0.6.5 (c (n "metar") (v "0.6.5") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1rp9nrpga8imdx6pxasaaxz4n2xr7hzlf9w7qrv0y4qy42kr1srd")))

(define-public crate-metar-0.6.6 (c (n "metar") (v "0.6.6") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "03aszzapnpp5n7bx9l0fcry5i876zpnsk2wkxw4iyk2av6irwsp9")))

(define-public crate-metar-0.6.7 (c (n "metar") (v "0.6.7") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "04yq2gpbb5h23akp2fsw9v6hzkas4ixanlx7vc5q6zzhn3ggb59g")))

(define-public crate-metar-0.7.0 (c (n "metar") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0zwaly6k9jgv6p1prz81q3j2hb9hfavbkp0bkh6dwlyr7gygaky8")))

(define-public crate-metar-0.7.1 (c (n "metar") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "cucumber") (r "^0.13") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1bnh8565xdyx1w7rln8pmfxakl83cb4l3adbwnxfm6z8fdjymxfs")))

(define-public crate-metar-0.7.2 (c (n "metar") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "cucumber") (r "^0.13") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1zvx9l3l58y8x96bvmi9p7msm2z74wqf4hb02w5fg2hxpknib7rq")))

(define-public crate-metar-0.7.3 (c (n "metar") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "cucumber") (r "^0.13") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0iqbhmdl3jbvy1kif4ylppc2wnwrjx0jic4bmhw427ckp8p2d78x")))

(define-public crate-metar-0.7.4 (c (n "metar") (v "0.7.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "cucumber") (r "^0.13") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0sky516528z3yaq6mzg46kfag5wz1lh2zijrcydpkaznppslsfpw")))

(define-public crate-metar-0.7.5 (c (n "metar") (v "0.7.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "cucumber") (r "^0.13") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1l9irmxzw5d1fm68jkfifmmj9k68fl68vhy5i4sxcmvgp1finlkq")))

(define-public crate-metar-0.7.6 (c (n "metar") (v "0.7.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "cucumber") (r "^0.19.1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "04as2nnsnw5xwrd1wba076ic7if8cwh2k73ln8h0rcyfhpvrpmmg")))

(define-public crate-metar-0.7.7 (c (n "metar") (v "0.7.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "cucumber") (r "^0.20.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.4") (d #t) (k 0)))) (h "0zz3abd6ikqd6jg5xq8azbh1g8c9fgqr5f98z227p7jyk39c9ra2")))

