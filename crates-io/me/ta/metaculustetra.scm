(define-module (crates-io me ta metaculustetra) #:use-module (crates-io))

(define-public crate-metaculustetra-0.0.1 (c (n "metaculustetra") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1sr1n8rcpk32q1p3k3nmpibl7js4czv1r01grwbiyyiy9nfzwzl4")))

(define-public crate-metaculustetra-0.0.2 (c (n "metaculustetra") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0qrm2kaqfw3wcjy82caaachhd4mq080d09phknsidjdn9qfl9m36")))

(define-public crate-metaculustetra-0.0.3 (c (n "metaculustetra") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("json"))) (d #t) (k 0)))) (h "0122limzgy3nsjkdjjllp8s5xas4k3h46kybh9sbcx35cijl1c6h")))

(define-public crate-metaculustetra-0.0.4 (c (n "metaculustetra") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("json"))) (d #t) (k 0)))) (h "0l8ajr9dhprhav2inhjk94wg9kam3ph938bv21v06fmijk5i9csx")))

(define-public crate-metaculustetra-0.0.5 (c (n "metaculustetra") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("json"))) (d #t) (k 0)))) (h "1bl8jjvcsml1x2d59kdqycdkh3nzjsmdhjs7qr0rpw9ck36s4y3m")))

