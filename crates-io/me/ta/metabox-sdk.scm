(define-module (crates-io me ta metabox-sdk) #:use-module (crates-io))

(define-public crate-metabox-sdk-0.1.0-alpha.0.0 (c (n "metabox-sdk") (v "0.1.0-alpha.0.0") (d (list (d (n "candid") (r "^0.7.16") (d #t) (k 0)) (d (n "garcon") (r "^0.2.3") (d #t) (k 0)) (d (n "ic-agent") (r "^0.20.0") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5.6") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)))) (h "1lkjs958jd66nzjydxwpir02gnc3xciy10izap9y3ki8zqhf0f0f")))

(define-public crate-metabox-sdk-0.1.0-alpha.0.1 (c (n "metabox-sdk") (v "0.1.0-alpha.0.1") (d (list (d (n "candid") (r "^0.8.2") (d #t) (k 0)) (d (n "garcon") (r "^0.2.3") (d #t) (k 0)) (d (n "ic-agent") (r "^0.21.0") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)))) (h "0y13fy8nsxjz9q9mxs5m6lm6808r6kz66zsq7nyisllr50ql94w3")))

