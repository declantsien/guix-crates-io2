(define-module (crates-io me ta metasophiea_minigrep) #:use-module (crates-io))

(define-public crate-metasophiea_minigrep-0.1.0 (c (n "metasophiea_minigrep") (v "0.1.0") (h "1qq9wazl1c8qnnmyrg1wwp3qd4v734xb1pwcy2333nc5ahqb4jnp")))

(define-public crate-metasophiea_minigrep-0.1.1 (c (n "metasophiea_minigrep") (v "0.1.1") (h "0ldk4byibnfj0hgajc2yfsjw521bg3jy0cl5msq0pm8776kxy78g")))

