(define-module (crates-io me ta metaemu-microx) #:use-module (crates-io))

(define-public crate-metaemu-microx-0.3.0 (c (n "metaemu-microx") (v "0.3.0") (d (list (d (n "fugue") (r "^0.2.8") (d #t) (k 0)) (d (n "metaemu-hooks") (r "^0.3") (d #t) (k 0)) (d (n "metaemu-state") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1i0l55hv1fnqj1j420i966mff514fd61hmaw6vnprfrn766kbqrd")))

