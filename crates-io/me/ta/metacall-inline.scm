(define-module (crates-io me ta metacall-inline) #:use-module (crates-io))

(define-public crate-metacall-inline-0.1.0 (c (n "metacall-inline") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "0zrszsspgssqwwyc12p992s01lzqhwmgphzg7mdi7gzzigq66h00")))

(define-public crate-metacall-inline-0.2.0 (c (n "metacall-inline") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)))) (h "05mnvs99f1aj10k8qka92q442yv1vbp1y59w40h3w42lppdz587f")))

