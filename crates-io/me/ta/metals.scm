(define-module (crates-io me ta metals) #:use-module (crates-io))

(define-public crate-metals-0.1.0 (c (n "metals") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1kqji0nqvl8anjxlcr4wkl51a5m96qw1h7rppah29vs1lwm2ia52")))

(define-public crate-metals-0.1.1 (c (n "metals") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1xjmysnk5jp0dfw1dhgymlymp163rp1r9kgyfwajf8w72fk9zb46")))

(define-public crate-metals-0.1.2 (c (n "metals") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "metals-poly") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0f4s2jpy2w8y4mz1ipza96xmnhlnwz6hv525xhahhqd2q6h2b6y0")))

(define-public crate-metals-0.1.3 (c (n "metals") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "metals-poly") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "01yfi5nlkcsamn5x2l4cxksx5s2fcyyhpgsb9zywfj3c0rsb3pg0")))

