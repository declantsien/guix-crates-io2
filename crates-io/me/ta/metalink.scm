(define-module (crates-io me ta metalink) #:use-module (crates-io))

(define-public crate-metalink-0.1.0 (c (n "metalink") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0pz7vrbhxvcfghm0x4lblls54xl41xyc2f2vkwprpyvg1vma3c82")))

