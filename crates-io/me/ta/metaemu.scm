(define-module (crates-io me ta metaemu) #:use-module (crates-io))

(define-public crate-metaemu-0.0.0 (c (n "metaemu") (v "0.0.0") (h "1d1hi85yvpjy638cqq00a0b4glk6mdzh6wymj3a9b3yq1zivn4g4")))

(define-public crate-metaemu-0.3.0 (c (n "metaemu") (v "0.3.0") (d (list (d (n "metaemu-concrete") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "metaemu-hooks") (r "^0.3") (d #t) (k 0)) (d (n "metaemu-intrinsics") (r "^0.3") (d #t) (k 0)) (d (n "metaemu-loader") (r "^0.3") (d #t) (k 0)) (d (n "metaemu-machine") (r "^0.3") (d #t) (k 0)) (d (n "metaemu-microx") (r "^0.3") (d #t) (k 0)) (d (n "metaemu-state") (r "^0.3") (d #t) (k 0)))) (h "1sjdazgn94cd20714z24iisxrq3xx1b7qxn0cii7d3g0jfgw4l9r") (f (quote (("loader-idapro" "metaemu-loader/idapro") ("loader-ghidra" "metaemu-loader/ghidra") ("loader-all" "metaemu-loader/all") ("concrete" "metaemu-concrete"))))))

