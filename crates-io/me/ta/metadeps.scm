(define-module (crates-io me ta metadeps) #:use-module (crates-io))

(define-public crate-metadeps-1.0.0 (c (n "metadeps") (v "1.0.0") (d (list (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "0vzbg0hpypga0xx2mhjbp9n57igr6w3zxrgcagjgymn34wcgvvz1")))

(define-public crate-metadeps-1.0.1 (c (n "metadeps") (v "1.0.1") (d (list (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "19risysk6cla8iwym9b6nksn18918xy3q8cr0k32maacll00phly")))

(define-public crate-metadeps-1.0.2 (c (n "metadeps") (v "1.0.2") (d (list (d (n "error-chain") (r "^0.7.1") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "0zly1ny5hmvpagsz7f203dcqdsdsd878qsdg820pr5v4zx6wk5my")))

(define-public crate-metadeps-1.1.0 (c (n "metadeps") (v "1.1.0") (d (list (d (n "error-chain") (r "^0.7.1") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "10xxshbh9w4j621xlq6sa39s6nps7k2iyn3ffjxkqnffk0dqqcm8")))

(define-public crate-metadeps-1.1.1 (c (n "metadeps") (v "1.1.1") (d (list (d (n "error-chain") (r "^0.7.1") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 0)) (d (n "toml") (r "^0.2") (k 0)))) (h "0l818461bslb7nrs7r1amkqv45n53fcp5sabyqipwx0xxbkzz7w2")))

(define-public crate-metadeps-1.1.2 (c (n "metadeps") (v "1.1.2") (d (list (d (n "error-chain") (r "^0.10") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 0)) (d (n "toml") (r "^0.2") (k 0)))) (h "1hjla9ypycqw1snd2qf87cckcc0d5z5qvxpcijn5yrrs3f825cbk")))

