(define-module (crates-io me ta meta-table) #:use-module (crates-io))

(define-public crate-meta-table-0.0.0 (c (n "meta-table") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "1l6kr71nvz6j7z05qv6ldv3flfza2m6q61np251zlni7c6yqj97s") (f (quote (("default"))))))

