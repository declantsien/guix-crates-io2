(define-module (crates-io me ta metatype) #:use-module (crates-io))

(define-public crate-metatype-0.1.0 (c (n "metatype") (v "0.1.0") (h "0zmcgdaj5bfrb7339c5hblvxr13d2gsacya8inzncx4z0apinwqa")))

(define-public crate-metatype-0.1.1 (c (n "metatype") (v "0.1.1") (h "0266bii8p0h26c58b96vy5a02z99i9jys0xn22ks0j8pna0k499c")))

(define-public crate-metatype-0.1.2 (c (n "metatype") (v "0.1.2") (h "1bq3izbc7zhx3w77h3ax25frai29rf5iz8hia9nk5f6z051qjiyn")))

(define-public crate-metatype-0.2.0 (c (n "metatype") (v "0.2.0") (h "0mzwzcsajh8mg2m8414djlghh0a4pl2a153xihq2jppfg5dwld3i")))

(define-public crate-metatype-0.2.1 (c (n "metatype") (v "0.2.1") (h "0ijrf09bjzhacz5xaamq8w674nxvlmwmsnjspppvqf16qgkwrpi3")))

