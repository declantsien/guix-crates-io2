(define-module (crates-io me ta metals-poly) #:use-module (crates-io))

(define-public crate-metals-poly-0.1.0 (c (n "metals-poly") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "06cwz7caz90z8knck1nxk8mx7xqfr2kjr7p7rdj46v624mkn6xcp")))

(define-public crate-metals-poly-0.1.1 (c (n "metals-poly") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1pjx3svhjimzhss1s8islx8fbpfaqgpa78vv90van7ly9mywdz6g")))

