(define-module (crates-io me ta metaheuristics) #:use-module (crates-io))

(define-public crate-metaheuristics-0.0.1 (c (n "metaheuristics") (v "0.0.1") (h "04qf487w6ghr819x294xfvx050vdi235y2hc8l57nam7q583c2ps")))

(define-public crate-metaheuristics-0.0.2 (c (n "metaheuristics") (v "0.0.2") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0s37k6a0blamra45lcx9f7v64wx8izpl16jxdgjcailv7qh19cc5")))

(define-public crate-metaheuristics-0.0.3 (c (n "metaheuristics") (v "0.0.3") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "10r4y1w1abfabmj1gah7wf063xpw5nrbqg4k1ql8r13nl7ylm2ix")))

(define-public crate-metaheuristics-0.0.4 (c (n "metaheuristics") (v "0.0.4") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "1irc5j9cvmqyq5krgz8sp2qqgz6w96v2qfyxpla1q9d05rjwyh4m")))

(define-public crate-metaheuristics-0.0.5 (c (n "metaheuristics") (v "0.0.5") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "1m4jjcfnlgr7y3qfll03gl0y724yp4mmivy50adgxi86lyaaag9w")))

(define-public crate-metaheuristics-0.0.6 (c (n "metaheuristics") (v "0.0.6") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "19b82wl2fy4x3yyk84kyzpnfzgl4gri40ympw6xx76lsm0c4xsyz")))

(define-public crate-metaheuristics-0.0.7 (c (n "metaheuristics") (v "0.0.7") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "037238rqaz7kas7qyas1g2mkp4pjbz0qv51ip4fa6pp40nyz6icd")))

(define-public crate-metaheuristics-0.0.8 (c (n "metaheuristics") (v "0.0.8") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "11cg4awl963gd23z0xiygq3a6b3krzqrjpi3k2s3ic5ip7h220da")))

(define-public crate-metaheuristics-0.0.9 (c (n "metaheuristics") (v "0.0.9") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "0jpk7znlp8i1ac1qcyy7jcl4vlg1ggkwaca99lmidvj6c1k8klsj")))

(define-public crate-metaheuristics-0.0.10 (c (n "metaheuristics") (v "0.0.10") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "0yp7rj52f3rrs8rz39vz5nmfiqpdmjij4cwh0qmdfypbad19zk4v")))

(define-public crate-metaheuristics-0.0.11 (c (n "metaheuristics") (v "0.0.11") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "15pdzaypkx4623i8vzj67qn9pspsimzgax7wanbhwnj1nc6xaqb4")))

(define-public crate-metaheuristics-0.0.12 (c (n "metaheuristics") (v "0.0.12") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "18bdy8n3zjfz2bfi63kbpki5xnqnvanq0a4c9l1qrqq4837cvv2p")))

(define-public crate-metaheuristics-0.0.13 (c (n "metaheuristics") (v "0.0.13") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "1ppwj8vcyd6isk67slp2vi4zgr7fpprk01fbpj58lwnsf8y2lxvp")))

(define-public crate-metaheuristics-0.0.14 (c (n "metaheuristics") (v "0.0.14") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "1sk30jk5s1nbi4zhf0sx0lj6bx15fswdw4mfq1k6zxc5jwd0sclq")))

(define-public crate-metaheuristics-1.0.15 (c (n "metaheuristics") (v "1.0.15") (d (list (d (n "rand") (r "^0.3.10") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "1sspij02h4h73xv1v19w5gs5w38w1g520hn2q9mr5n9fb2qfakn5")))

(define-public crate-metaheuristics-1.0.16 (c (n "metaheuristics") (v "1.0.16") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1gfp7yi6b3vf20gfh18wbmq0wds64fqm95i449bjd2q5qc12hw4y")))

(define-public crate-metaheuristics-1.0.17 (c (n "metaheuristics") (v "1.0.17") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1sdiv5m8m5ijw12jm9bzgg8jwyqwv5dqg5wrcjldfi9zy50hvpp0")))

(define-public crate-metaheuristics-1.1.18 (c (n "metaheuristics") (v "1.1.18") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "0g18xp906sr6gc7gp5yar8x6x2wv758wr8ixsw5q8mgc5h86kww4")))

(define-public crate-metaheuristics-1.1.19 (c (n "metaheuristics") (v "1.1.19") (d (list (d (n "rand") (r "=0.8.4") (d #t) (k 0)) (d (n "time") (r "=0.3.5") (d #t) (k 0)))) (h "0q4rzakrf48a4sgg521mhfdf02p9lw3iv4khjfi80x5rb89741r9")))

(define-public crate-metaheuristics-1.1.20 (c (n "metaheuristics") (v "1.1.20") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1isjdlj9gz88h8kcadflvbj0daf9ikm3yywlcc0slwkvyc9hhwvp")))

(define-public crate-metaheuristics-1.1.21 (c (n "metaheuristics") (v "1.1.21") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1xxhhph1q4azynv2m7f32vff82nqz4xcby5363b0krvrhzzjw1my")))

(define-public crate-metaheuristics-1.1.22 (c (n "metaheuristics") (v "1.1.22") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (d #t) (k 0)))) (h "1rrdvbvgiia7hr6nymjna007bz9hc5i1i3hz07ybgvr2f38696aq")))

