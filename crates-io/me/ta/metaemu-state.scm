(define-module (crates-io me ta metaemu-state) #:use-module (crates-io))

(define-public crate-metaemu-state-0.3.1 (c (n "metaemu-state") (v "0.3.1") (d (list (d (n "fugue") (r "^0.2.8") (f (quote ("extra-integer-types"))) (d #t) (k 0)) (d (n "iset") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metaemu-state-derive") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ustr") (r "^0.8") (d #t) (k 0)) (d (n "ux") (r "^0.1") (d #t) (k 0)))) (h "1n77h9c8qkmvkkpjxa5pcix1a4zmrh9wqrv4v0yph0zdiyn045s7")))

