(define-module (crates-io me ta metaemu-state-derive) #:use-module (crates-io))

(define-public crate-metaemu-state-derive-0.3.0 (c (n "metaemu-state-derive") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01jz9jdpf7l99m106ly7pswcx2g33f8z0zi6551l0rby34hdq00s")))

