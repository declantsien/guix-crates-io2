(define-module (crates-io me ta metal-sys) #:use-module (crates-io))

(define-public crate-metal-sys-0.0.0 (c (n "metal-sys") (v "0.0.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "block") (r "0.*") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "cocoa") (r "^0.2.4") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-foundation") (r "0.*") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "core-graphics") (r "0.*") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "objc") (r "^0.1.8") (d #t) (t "x86_64-apple-darwin") (k 0)))) (h "1aja7b6arfngd3ss33mvbxybi7z7sxkqbrli55cv0k9hx90pvisk")))

