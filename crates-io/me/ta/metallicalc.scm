(define-module (crates-io me ta metallicalc) #:use-module (crates-io))

(define-public crate-metallicalc-1.0.0 (c (n "metallicalc") (v "1.0.0") (h "1cab7kbqrqg0y67r4d7ps30lpxg03kv61ahk8mmznkq267y0yf7n")))

(define-public crate-metallicalc-1.0.1 (c (n "metallicalc") (v "1.0.1") (h "1fqhr4v5ylig8m12n84wifsmpi0m2mfn4gdrmk96p4pjwwagrnjz")))

(define-public crate-metallicalc-2.0.0 (c (n "metallicalc") (v "2.0.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)))) (h "0i6bs89ahqb7ydz006rpqnrkf32vs93kykr5l11wkmrkv5fj8x8r")))

(define-public crate-metallicalc-2.1.0 (c (n "metallicalc") (v "2.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)))) (h "0mh2whnx2wfvcn1av1q5cn8rb5mbq2bp7n1z1p2lcqnc5faw545f")))

(define-public crate-metallicalc-2.1.1 (c (n "metallicalc") (v "2.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)))) (h "12d698rapf5r5ilhpnhdapmh234a9s1c8m7dyk99famzx5wlgfwi")))

(define-public crate-metallicalc-2.1.2 (c (n "metallicalc") (v "2.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)))) (h "1h0raxv69sy2qak8w6q48ds48v5ivwaki3lagdymai3j7b0bzxhl") (y #t)))

