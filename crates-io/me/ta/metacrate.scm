(define-module (crates-io me ta metacrate) #:use-module (crates-io))

(define-public crate-metacrate-0.1.0 (c (n "metacrate") (v "0.1.0") (h "0j0qiiwr1hji5xl2djbfcj641raz62hkxhn0alwl33ps9wgq237i")))

(define-public crate-metacrate-0.1.1 (c (n "metacrate") (v "0.1.1") (h "1g91ifnlr6bgkd3i9r26nsmjiqra1a01ixhpa7bzm98x7jxglk31")))

(define-public crate-metacrate-0.1.2 (c (n "metacrate") (v "0.1.2") (h "02cxi3v70d2gwgsnkawd2vgwk9r0fyscra325dd1m4pk77ykg1yw")))

