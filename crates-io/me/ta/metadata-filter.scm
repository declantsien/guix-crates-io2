(define-module (crates-io me ta metadata-filter) #:use-module (crates-io))

(define-public crate-metadata-filter-0.1.0 (c (n "metadata-filter") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "100lfmv9r0flmly54j22ikm4q6i6700lis6i4kigfzmfj13n3rpp")))

(define-public crate-metadata-filter-0.1.1 (c (n "metadata-filter") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1070gf2y9ybdg3mcqyx9fib5c3qrdjv6hq55xm36jr43zxhf04da")))

(define-public crate-metadata-filter-0.2.0 (c (n "metadata-filter") (v "0.2.0") (d (list (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1yla9m9y130a70ahzx1gzsvhkqvkrsrjc1j0697l3a30cjj69vyr")))

(define-public crate-metadata-filter-0.3.0 (c (n "metadata-filter") (v "0.3.0") (d (list (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "15955mpn3n4vds4qp1brs0s11ig69i9frl75racssdg536rv8pcc")))

