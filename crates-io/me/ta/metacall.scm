(define-module (crates-io me ta metacall) #:use-module (crates-io))

(define-public crate-metacall-0.1.0 (c (n "metacall") (v "0.1.0") (h "091lkm3p6f6bbh1p06hl699b9ipcw7fn8k9fbni0752j6amyyvy9")))

(define-public crate-metacall-0.1.1 (c (n "metacall") (v "0.1.1") (h "1yj0dl5jy861myaa866w3ps4bbam37q8phs97kvfyqa8cmgg3v1g")))

(define-public crate-metacall-0.1.2 (c (n "metacall") (v "0.1.2") (h "00xxbjvj9jsixrg0va22f16xkq7n4ar9a9sm22m8gkgwngxqi35k")))

(define-public crate-metacall-0.1.3 (c (n "metacall") (v "0.1.3") (h "0n7c5z69jx978ns5425pvqxkrgszs2p6apk9w93rsvphqcs06z8b")))

(define-public crate-metacall-0.2.0 (c (n "metacall") (v "0.2.0") (h "0nlwshysps16i0v21x7a4dgpnhinxk8fw2cxscvwapld0bb4fxv5")))

(define-public crate-metacall-0.2.1 (c (n "metacall") (v "0.2.1") (h "1x30kclaniwng96qbp2akizgjaxb3imv0bzrxfmxrc1717j29ci9")))

(define-public crate-metacall-0.3.0 (c (n "metacall") (v "0.3.0") (d (list (d (n "metacall-inline") (r "^0.1.0") (d #t) (k 0)))) (h "1wwriaz5k1v0pc0dlbpjgl45z01my8kcxrjg0bz666ri17m16zw7")))

(define-public crate-metacall-0.4.0 (c (n "metacall") (v "0.4.0") (d (list (d (n "metacall-inline") (r "^0.2.0") (d #t) (k 0)))) (h "07awbzy7hlwqs5jzzqqqvl2fcxr7ms95fyb85xi6rv418brxrxkx")))

