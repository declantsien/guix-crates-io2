(define-module (crates-io me ta metaphone) #:use-module (crates-io))

(define-public crate-metaphone-0.1.0 (c (n "metaphone") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1qp5gnap117rwxy0mjhv3s59m7k4jjyqzbiqxapnghawmf15ycw5") (y #t)))

(define-public crate-metaphone-0.1.1 (c (n "metaphone") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1k1gqd6vriqcr1s7fwaai5jxcgqv9w626j7f0ncnh0m7b7pp97lc")))

