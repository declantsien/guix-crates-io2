(define-module (crates-io me ta metaflac) #:use-module (crates-io))

(define-public crate-metaflac-0.1.0 (c (n "metaflac") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0k5mswwpw2jm79b6b673sn36yyvcpjba23frsjp186wrx8vzlsmm") (y #t)))

(define-public crate-metaflac-0.1.1 (c (n "metaflac") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1vy9zwbn8j4y0ljnmz7jcals2bvh1zsv0kzsl8x58nizl0iv6wja")))

(define-public crate-metaflac-0.1.2 (c (n "metaflac") (v "0.1.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "174di7axdrysq10cjik3imhkc92z9sqqp4cf57sb47vnpmk5bmsw")))

(define-public crate-metaflac-0.1.3 (c (n "metaflac") (v "0.1.3") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0ly689hpxq1wh6rf5caq6kps7i2ijhy0m941sgxv8k3qhkpfgi5z")))

(define-public crate-metaflac-0.1.4 (c (n "metaflac") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "12y5x7hirg5w8nb8frzg8im12djj3586v3h0mp67pnq3c5dqdmb6")))

(define-public crate-metaflac-0.1.5 (c (n "metaflac") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "1i1sil3q6nn7zr9bbqqh13a5p6iqqzhg4m2i123ynlsqaga45kfw")))

(define-public crate-metaflac-0.1.6 (c (n "metaflac") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "1xc39y0v7fpwmk940yc73nkcihvv4q6r39lvjh41h8dh3ai8d10c")))

(define-public crate-metaflac-0.1.7 (c (n "metaflac") (v "0.1.7") (d (list (d (n "byteorder") (r "~1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "1w2apa2xaavi4iazwrvk4n6jyd4q8djbmavhkhgd7xr3l994vdxj")))

(define-public crate-metaflac-0.1.8 (c (n "metaflac") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "068jc92qcw6145db5pcc4ai84wj0vbf41hfi8ybgnlf661zaaf8q")))

(define-public crate-metaflac-0.1.9 (c (n "metaflac") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0zgc7nw0nx68r9y9zs962366z3yw2bbqdmrvm4kkh4lcciij4mjr")))

(define-public crate-metaflac-0.1.10 (c (n "metaflac") (v "0.1.10") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1l40plnyn9rhkpjlycfqhzbdarfiyj64vpi3ixn847dfffbsxgvi")))

(define-public crate-metaflac-0.2.0 (c (n "metaflac") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0ijl7syjqggvpra8v3lx0y62g4mf0bmm02gii5g0rzvw2ldzzrgr")))

(define-public crate-metaflac-0.2.1 (c (n "metaflac") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "018rbn3afdnk6jsgsc6fl41nw1mbj827a74d56d57jwd4kf25h29")))

(define-public crate-metaflac-0.2.2 (c (n "metaflac") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "075skmki75r373ld4mpsf3p6yw0229c416kls7vnx6dww9h7cq9m")))

(define-public crate-metaflac-0.2.3 (c (n "metaflac") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0g4av2a5flgz34lxwbrsxhp7sad94hy6r7ifzxa4j1xr3hkgh0mk")))

(define-public crate-metaflac-0.2.4 (c (n "metaflac") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0j576qncws64f2yrqlcgwpf6ilaq3jx1qa5v5nf93lm9740bz1a6")))

(define-public crate-metaflac-0.2.5 (c (n "metaflac") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0l57bcm1f8k33i72rhwizl4vmf1h8dcznfpbylm6j3dvq4y0siz1")))

(define-public crate-metaflac-0.2.6 (c (n "metaflac") (v "0.2.6") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0vr19p9bms3lfxci5m8435202fasgvl67cr9zgyn9rqh4kx51vcv")))

(define-public crate-metaflac-0.2.7 (c (n "metaflac") (v "0.2.7") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "16yv8j4jsk510l8f5m5z4pvk3yhl3h6j5a7xn6nga8aamvnq7w6h")))

