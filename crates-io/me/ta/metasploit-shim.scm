(define-module (crates-io me ta metasploit-shim) #:use-module (crates-io))

(define-public crate-metasploit-shim-0.1.0 (c (n "metasploit-shim") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "11hx2kx9xwwrivjj8rk8sbysx4lzrwrd7qdwafnj9a5qpgx2n67v")))

(define-public crate-metasploit-shim-0.1.1 (c (n "metasploit-shim") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1s9jsxm9mza0q6hmzwb2rcywnilvj01dvgpwskr7v150acp5m4gz")))

(define-public crate-metasploit-shim-0.1.2 (c (n "metasploit-shim") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0ynniyx2mkahwja2w577dyhiq6xlz9yihhdn9k3ss5bgyb24mj16")))

(define-public crate-metasploit-shim-0.1.3 (c (n "metasploit-shim") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1vwdladl8hq7xd7c4klmd0xzmzs9c3fs4kpb7cy64zh95mr9ir62")))

(define-public crate-metasploit-shim-0.1.4 (c (n "metasploit-shim") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0vnxz250xmk462miwcvikxrq06xv9rkm1lym67q44rj7k25z7fnk")))

