(define-module (crates-io me ta metascraper) #:use-module (crates-io))

(define-public crate-metascraper-0.1.0 (c (n "metascraper") (v "0.1.0") (d (list (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0ja6814i8bdcwrycq2m4n7ibw0dvbgp4fjmsmq3aaal4hdzq45ql")))

(define-public crate-metascraper-0.2.0 (c (n "metascraper") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0v9dhaqp3rpgyggqvq335lwm7mh7w0hn3zwb87lrr2hq4lgi6ljw")))

(define-public crate-metascraper-0.2.1 (c (n "metascraper") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0jnv9hralb9047dn66gpqwg34yjxlbzfiha2ybrhwnqb3lzfhiqh")))

(define-public crate-metascraper-0.2.2 (c (n "metascraper") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tl") (r "^0.7.7") (d #t) (k 0)))) (h "0ssplq5sqw7qg897r4d0ap9kchxc106fz6p4pqsam0nn4gp5vbms")))

