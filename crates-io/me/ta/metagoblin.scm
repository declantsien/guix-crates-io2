(define-module (crates-io me ta metagoblin) #:use-module (crates-io))

(define-public crate-metagoblin-0.0.1 (c (n "metagoblin") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "goblin") (r "^0.0.14") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "memrange") (r "^0.1.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "^0.7.1") (d #t) (k 0)))) (h "1ww0vxc1wiagw8gm9hf2zbrxrjv36fj9bkigxp7iim5j9wmsw8wb")))

(define-public crate-metagoblin-0.1.0 (c (n "metagoblin") (v "0.1.0") (d (list (d (n "goblin") (r "^0.0.18") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memrange") (r "^0.1.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "^0.7.1") (d #t) (k 0)))) (h "06scxl34jsmnlf9hj2jqcyrbn7asvfjl35r0azx5b6dpdzalixkr")))

(define-public crate-metagoblin-0.1.1 (c (n "metagoblin") (v "0.1.1") (d (list (d (n "goblin") (r "^0.0.19") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memrange") (r "^0.1.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "^0.7.1") (d #t) (k 0)))) (h "1m01r6fs1x8r5ac3ilixc41zgqs43113vymfwgaf7pnprcvaqwd4")))

(define-public crate-metagoblin-0.1.2 (c (n "metagoblin") (v "0.1.2") (d (list (d (n "goblin") (r "^0.0.21") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memrange") (r "^0.1.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "^0.7.1") (d #t) (k 0)))) (h "147riwnv39r0znl1amr80p67hwsz5h9jx7235qw4r7xww455pf54")))

(define-public crate-metagoblin-0.2.0 (c (n "metagoblin") (v "0.2.0") (d (list (d (n "goblin") (r "^0.0.23") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "memrange") (r "^0.1.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "^0.7.1") (d #t) (k 0)))) (h "0hpjzq8rppzj2knixgv7qil84xc5qzcgr3hkjd2rfammq7lp87wb")))

(define-public crate-metagoblin-0.3.0 (c (n "metagoblin") (v "0.3.0") (d (list (d (n "goblin") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memrange") (r "^0.1.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "^0.7.1") (d #t) (k 0)))) (h "0fzjvkdk26imr9pj9j4s0rijpqzl98ijlhk8xslhli694qfsb7ym")))

(define-public crate-metagoblin-0.3.1 (c (n "metagoblin") (v "0.3.1") (d (list (d (n "goblin") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memrange") (r "^0.1.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "^0.7.1") (d #t) (k 0)))) (h "087v6x85946nw8pj81hxflak7xdpgi3i2zn05hhqdn9pinjdbd80")))

(define-public crate-metagoblin-0.4.0 (c (n "metagoblin") (v "0.4.0") (d (list (d (n "goblin") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memrange") (r "^0.1.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "^0.7.1") (d #t) (k 0)))) (h "1c8j5r90bgr64zh9wsq69rn5c916qsqnfpn5xa7l8y13jhh9lv4l")))

(define-public crate-metagoblin-0.5.0 (c (n "metagoblin") (v "0.5.0") (d (list (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memrange") (r "^0.1.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "^0.7.1") (d #t) (k 0)))) (h "01v6hr4nbap0yhizib804gx3w1s6m8ysmqrcjr5mmq9snqz5awia")))

(define-public crate-metagoblin-0.6.0 (c (n "metagoblin") (v "0.6.0") (d (list (d (n "goblin") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memrange") (r "^0.1.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "^0.7.1") (d #t) (k 0)))) (h "00iwfnjdsls3j93vkd1kvdlaksxqk5yf3bwrrcq197jmgzxmziks")))

(define-public crate-metagoblin-0.7.0 (c (n "metagoblin") (v "0.7.0") (d (list (d (n "goblin") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09gvakix71p9b57nj57jilw7baka6yc2976nr9l6cqnflx5y99h9")))

(define-public crate-metagoblin-0.8.0 (c (n "metagoblin") (v "0.8.0") (d (list (d (n "goblin") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0zacl8f06v3x3qd83iqg63jzr8xskrla3gg86pqfqvw8gsbi5709")))

(define-public crate-metagoblin-0.9.0 (c (n "metagoblin") (v "0.9.0") (d (list (d (n "goblin") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "18fhg7kb6a796lch8hzlxaxy1hj3f77lyjw26bsl266xrpxi7kg0")))

(define-public crate-metagoblin-0.10.0 (c (n "metagoblin") (v "0.10.0") (d (list (d (n "goblin") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lk2zn5x1cy6zv7hfm7wc5y6kb12x3rq9p8l2bzja12zfswhsjsq")))

