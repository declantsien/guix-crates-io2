(define-module (crates-io me ta metaldb-derive) #:use-module (crates-io))

(define-public crate-metaldb-derive-1.0.0 (c (n "metaldb-derive") (v "1.0.0") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wqpz7rrphaqisnhz52cdwbys63amwhvpsvg0947w6ndyz2vxrrp")))

