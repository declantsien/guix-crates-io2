(define-module (crates-io me ta metaverse_login) #:use-module (crates-io))

(define-public crate-metaverse_login-0.0.0 (c (n "metaverse_login") (v "0.0.0") (d (list (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.2") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15.1") (d #t) (k 0)))) (h "074avclgz80by10cmd0xf5cbhygngcxqh7j0vay477vaqzqvn8qj")))

(define-public crate-metaverse_login-0.0.1 (c (n "metaverse_login") (v "0.0.1") (d (list (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.2") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15.1") (d #t) (k 0)))) (h "0pqjb0pmxlmv0jvba7x1c09c2dmlkmqp83jxzy0aj5ylq9j9lrc1")))

(define-public crate-metaverse_login-0.0.2 (c (n "metaverse_login") (v "0.0.2") (d (list (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mac_address") (r "^1.1.2") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.15.1") (d #t) (k 0)))) (h "19ziwkn9x75ns9l01svf2qfhkqk4k8iy8ww41sii2dr8ia20sagk")))

