(define-module (crates-io me ta metadata-transferer) #:use-module (crates-io))

(define-public crate-metadata-transferer-0.1.0 (c (n "metadata-transferer") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zs6i3v0z790mp9bn2sy7yvb3hi356lir1izj3liz7qd9qrdhkaa")))

(define-public crate-metadata-transferer-0.2.0 (c (n "metadata-transferer") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive" "cargo"))) (d #t) (k 0)))) (h "0g1aasa6vpiqhmqf3x99g75yz3z1plgij1h6ligl1lr0xphgbapi")))

(define-public crate-metadata-transferer-0.3.0 (c (n "metadata-transferer") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive" "cargo"))) (d #t) (k 0)))) (h "1w1bbh3qizki5qx1vq9acv5bfw9969vx4xvdwj540rjclsmvyxmf")))

(define-public crate-metadata-transferer-0.4.0 (c (n "metadata-transferer") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive" "cargo"))) (d #t) (k 0)))) (h "0zrwykssa2jbdyz6y2s39cwaiqaxa1n8hamqxi239f2aixjn8qd9")))

