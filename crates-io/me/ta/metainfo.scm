(define-module (crates-io me ta metainfo) #:use-module (crates-io))

(define-public crate-metainfo-0.1.0 (c (n "metainfo") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "0bmvm8cxic1a9alh5qmqqiyqwhhi7kzzkmmn7cbqlasn11lvdmnq")))

(define-public crate-metainfo-0.2.0 (c (n "metainfo") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "09ra52h5slxsfijn63vmcv6z5r6b888sdim2lfw1m16qgil2ccwr")))

(define-public crate-metainfo-0.2.1 (c (n "metainfo") (v "0.2.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)))) (h "1m3llaiz31n7y0p3b6hxsd26nm1qmpm7mvb9535ggxmfg0j5gzj0")))

(define-public crate-metainfo-0.3.0 (c (n "metainfo") (v "0.3.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1xq2zrbsp4k5ang3rcp3g5k3f4qhmdq3cb52rnlw7fmg0gnjscpd") (y #t)))

(define-public crate-metainfo-0.4.0 (c (n "metainfo") (v "0.4.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0ggv60a0d6nqw370981lqv364k10nz8zkmyvfi9bkqhmp8vp95b5") (y #t)))

(define-public crate-metainfo-0.4.1 (c (n "metainfo") (v "0.4.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1vs87ag8d36c8wcj9wlvmpv5ij81a6l33hqx73a3a1sxail4maf7") (y #t)))

(define-public crate-metainfo-0.4.2 (c (n "metainfo") (v "0.4.2") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1z76ls0bi18himmfd939w4bc1j7dx2i1bi4sqlxvkq9fsmdgpvyr") (y #t)))

(define-public crate-metainfo-0.4.3 (c (n "metainfo") (v "0.4.3") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "10hhkd3k18l9n4pikqadmwj0psb5x8m9r9jsyy5b5k6barkmcmr8")))

(define-public crate-metainfo-0.4.4 (c (n "metainfo") (v "0.4.4") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0mhldgmrnkc371ylcb5z1h1fpd708frrqi87nnbvq5a2hs4n2gqs")))

(define-public crate-metainfo-0.4.5 (c (n "metainfo") (v "0.4.5") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "04anknvixjw5qph0fq04d72xgnza9x3cwq2ai8xkiv4sc71r966b")))

(define-public crate-metainfo-0.5.0 (c (n "metainfo") (v "0.5.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0jakq3lpfapmlhi1sm7cipsmqdpqd1ghjw5vanhhrs6wm87x1vvl")))

(define-public crate-metainfo-0.6.0 (c (n "metainfo") (v "0.6.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "1k2dd6x607vwhwm1hdskb4s0bwszas6nwcjxbqgldv7w7wg7c3sw") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.6.1 (c (n "metainfo") (v "0.6.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "06kbmyzaw530bhjz2jkydshddfkc2qni2p6pqig3ma69xzyqcgkx") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.6.2 (c (n "metainfo") (v "0.6.2") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "18qqyilny4vsfpijmigbbqcs0bb483z9vf1kpmvpxixpqjjz9inx") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.6.3 (c (n "metainfo") (v "0.6.3") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "0avg660awvrprkshn4ynqwchiivrk95phbr4chgz6zxz6j49aq00") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.0 (c (n "metainfo") (v "0.7.0") (d (list (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "1i909ig3zl3ijsdq65rbln55g4cb0n2m9kvs0v4iaf2iwfpqzkm5") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.1 (c (n "metainfo") (v "0.7.1") (d (list (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "170wz0zvv1bc4fqwjzaqynmgkys5v4qffzha34zbgb3l13l1yi2w") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.2 (c (n "metainfo") (v "0.7.2") (d (list (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "14jiyyi6wdbgjma72jrj3hacj24hcziy951gkirb9i1d26di6l6j") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.3 (c (n "metainfo") (v "0.7.3") (d (list (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "0k7rzmlh8k1lp4916wxr6j3147r214dz3qb6nz9xgdiymy95s6qc") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.4 (c (n "metainfo") (v "0.7.4") (d (list (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "09kp4qk1g80jci9dwak1w6xq0hzjrlg31ax5j9pmaib7namjv1j6") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.5 (c (n "metainfo") (v "0.7.5") (d (list (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "0jj0pywpnv8g2kn5fa5h3g71y7fxv0j5y569ii1vpy5igqlx1276") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.6 (c (n "metainfo") (v "0.7.6") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "047hc1ff6zirwhfjfl896pzpn3nld1dccb5w0wcp8ng2sww811aj") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.7 (c (n "metainfo") (v "0.7.7") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "08cd58lj25ka7wgqp72wc0n35kllgcb6ckm5rf87lc1417f3l0d8") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.8 (c (n "metainfo") (v "0.7.8") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "0b1493ddywv8a7y14acfs7i5cwgfs3db9i1bs0ch6bh6cf6flq62") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.9 (c (n "metainfo") (v "0.7.9") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "1f8zd50f9ka9ck2hcavaf13j0kf8fv7fxi5adv9v83cn49p790s2") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

(define-public crate-metainfo-0.7.10 (c (n "metainfo") (v "0.7.10") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (o #t) (d #t) (k 0)))) (h "0s33cmq2gpccvzz4mimpw93kw66c6zinnp8kx45wy2pdadjhs5rl") (f (quote (("task_local" "tokio" "tokio/rt") ("default" "task_local"))))))

