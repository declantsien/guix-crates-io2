(define-module (crates-io me ta metadata-backup) #:use-module (crates-io))

(define-public crate-metadata-backup-0.1.0 (c (n "metadata-backup") (v "0.1.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "csv") (r "~1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.1.2") (d #t) (k 2)) (d (n "rayon") (r "~1.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strmode") (r "~1") (d #t) (k 0)) (d (n "structopt") (r "~0.3") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 2)) (d (n "walkdir") (r "~2.2") (d #t) (k 0)) (d (n "zip") (r "~0.5") (d #t) (k 0)))) (h "1mxglpxj2k2n7nlfdgywhgrc1d695z0splyqzpdx8mnlcryanfi6")))

