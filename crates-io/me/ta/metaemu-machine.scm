(define-module (crates-io me ta metaemu-machine) #:use-module (crates-io))

(define-public crate-metaemu-machine-0.3.0 (c (n "metaemu-machine") (v "0.3.0") (d (list (d (n "fugue") (r "^0.2.8") (d #t) (k 0)) (d (n "metaemu-state") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1k2f2gk3n3i2xxg32rgf335yjr7i4pgfmrz3m5qcc7rb7ip7vg9k")))

