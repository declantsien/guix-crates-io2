(define-module (crates-io me ta metaplex-auction) #:use-module (crates-io))

(define-public crate-metaplex-auction-0.0.1 (c (n "metaplex-auction") (v "0.0.1") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.7.11") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.7.11") (d #t) (k 2)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19wwqly7rg7267xcqr8jclr43rffr9lwllmihkl00y25r72ccv7z") (f (quote (("test-bpf") ("no-entrypoint"))))))

