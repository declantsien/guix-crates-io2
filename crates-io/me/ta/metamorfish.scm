(define-module (crates-io me ta metamorfish) #:use-module (crates-io))

(define-public crate-Metamorfish-0.0.1 (c (n "Metamorfish") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protofish") (r "^0.2") (d #t) (k 0)) (d (n "rpds") (r "^0.8") (d #t) (k 0)) (d (n "test-env-log") (r "^0.2") (d #t) (k 2)))) (h "1pbdg3ns7p3z4dymps8vlmn1cspi0gsslmgvfwrh4136i0h7jfd4")))

