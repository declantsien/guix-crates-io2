(define-module (crates-io me ta metafactory) #:use-module (crates-io))

(define-public crate-metafactory-0.0.1 (c (n "metafactory") (v "0.0.1") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "1cjrh3h1f3bcmw9038nx1bmj05x1zhlm68hqghsqna887p4jk4pr")))

(define-public crate-metafactory-0.0.2 (c (n "metafactory") (v "0.0.2") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "0ar5lfln6prad7fmcl8g55nilpwljpb3jphirngkpdd16f49i45k")))

(define-public crate-metafactory-0.0.3 (c (n "metafactory") (v "0.0.3") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "0akiqnlvvwdvd855pdblpwsglc3gmhkzwr7w2lkr0c347amf6lqp")))

(define-public crate-metafactory-0.1.0 (c (n "metafactory") (v "0.1.0") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "0jx8dxcfwdqal2c4cb7y66ik8nm92aglchdjvzfsk0w7ns37z0ks")))

(define-public crate-metafactory-0.1.1 (c (n "metafactory") (v "0.1.1") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "1cmlwxmfc0avs2h9i9gm5chq0ywqk5iyzq953x5gg74vqk39d8gv")))

(define-public crate-metafactory-0.1.2 (c (n "metafactory") (v "0.1.2") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "0n88dyksdhhjmnqx4l3vh5cqzcz6kgs610b0hb08166zm1acwbv9")))

(define-public crate-metafactory-0.1.3 (c (n "metafactory") (v "0.1.3") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "08yqjlz4b27rcv6h2svqa3a2707rrhmjg502bi7xgsgg9i54m14s")))

(define-public crate-metafactory-0.1.4 (c (n "metafactory") (v "0.1.4") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "0cf3qflqpqhvr96r6zaafqjx6phvpgjcx295qmnlmr8dal277kf8")))

(define-public crate-metafactory-0.1.5 (c (n "metafactory") (v "0.1.5") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "1zdn9ydz6bk9hdpnlw2nymsk4s296rmhw3dmwi321f41xbg70y89")))

(define-public crate-metafactory-0.2.0 (c (n "metafactory") (v "0.2.0") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "1f5hkdf3wxlhsk85qfk8f8aq9mqaw2s4v9vjq8a0dk5a2phyvxp8")))

(define-public crate-metafactory-0.2.1 (c (n "metafactory") (v "0.2.1") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "1x3pr0yasqg7mxdnqdyl44wq36k4aczwhy9azbfr79ljwpvzqkbk")))

(define-public crate-metafactory-0.2.2 (c (n "metafactory") (v "0.2.2") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "0mbprk3hrr80bmmrbr5kdibqr7zf1r5mwv8z5xwsqh6yf6i75f7h")))

(define-public crate-metafactory-0.2.3 (c (n "metafactory") (v "0.2.3") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "1l7hchggbq0nw3m1lbxn1p92sgdn60gxpy4jafrn3dsiwpfiy96w")))

(define-public crate-metafactory-0.3.0 (c (n "metafactory") (v "0.3.0") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "07ba1bxgbvn5wc821pfky4bf4x19dwzylxyic47fbbh6n6myvzkd")))

(define-public crate-metafactory-0.4.0 (c (n "metafactory") (v "0.4.0") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "0wjxn0xgnwhnzsfn5kriicc5iam71cr2skr7kh0m7ffsx9xr27xg")))

(define-public crate-metafactory-0.4.1 (c (n "metafactory") (v "0.4.1") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "0h6v785qba3ixq4zxikabcc01bbxb40fx0bn1nkdvfqflvm9mah9")))

(define-public crate-metafactory-0.4.2 (c (n "metafactory") (v "0.4.2") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "0hqa4bjgvb453iw4y35vrgga987p3pkj82x6p5rmc9msmqbjwn1i")))

(define-public crate-metafactory-0.4.3 (c (n "metafactory") (v "0.4.3") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "1sbgj61ixzbi32cg3hc277b54vwahm6ikxmizg6f2x6jp811sxln")))

(define-public crate-metafactory-0.4.4 (c (n "metafactory") (v "0.4.4") (d (list (d (n "typedef") (r "*") (d #t) (k 0)))) (h "0bshnn91s031v8d11825nljvip5q21lj54mlqvk5kbzhncs1islc")))

