(define-module (crates-io me ta metaprofile) #:use-module (crates-io))

(define-public crate-metaprofile-0.3.0 (c (n "metaprofile") (v "0.3.0") (d (list (d (n "alphabeta") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "methylome") (r "^0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "progress_bars") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1vk1325gp8gnblf52y8w9wn6vsaxlw63ccza5lsw5050q9l97880")))

