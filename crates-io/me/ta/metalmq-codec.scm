(define-module (crates-io me ta metalmq-codec) #:use-module (crates-io))

(define-public crate-metalmq-codec-0.2.0 (c (n "metalmq-codec") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "1klprnw1aqm4137r5llxzyhv8r45rsfc94cbq30qal98vhczvypd")))

(define-public crate-metalmq-codec-0.2.1 (c (n "metalmq-codec") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "07x30wjc9gw1z9f3wj4h3q78zr7m7a8ifx5ix0mpd4fxqx6bgcfg")))

(define-public crate-metalmq-codec-0.2.2 (c (n "metalmq-codec") (v "0.2.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0h4nzrdrkmsazml3k8hjkajc9bipwjn3rfwmrp7iql815c2qj32r")))

(define-public crate-metalmq-codec-0.3.0 (c (n "metalmq-codec") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1xl7ivwir1cj7vnqxpl7k81s9hi1fjadx99hasbak8hq5ldg1ddg")))

