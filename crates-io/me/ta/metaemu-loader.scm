(define-module (crates-io me ta metaemu-loader) #:use-module (crates-io))

(define-public crate-metaemu-loader-0.3.0 (c (n "metaemu-loader") (v "0.3.0") (d (list (d (n "either") (r "^1") (d #t) (k 0)) (d (n "fugue") (r "^0.2.8") (d #t) (k 0)) (d (n "fugue-db") (r "^0.2") (d #t) (k 0)) (d (n "fugue-ghidra") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "fugue-idapro") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "metaemu-state") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14qwaayck9gpcirafz6qzdr266nkvkw16p07758v1a9ag6rx1yh5") (f (quote (("idapro" "fugue-idapro") ("ghidra" "fugue-ghidra") ("default") ("all" "idapro" "ghidra"))))))

