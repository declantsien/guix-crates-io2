(define-module (crates-io me ta metaquery) #:use-module (crates-io))

(define-public crate-metaquery-0.1.0 (c (n "metaquery") (v "0.1.0") (d (list (d (n "sqlparser") (r "^0.10.0") (d #t) (k 0)))) (h "1ags8za4ryyi023hj733m3i1hzymxanw2ilrifdfcjxgs1yji0ky")))

(define-public crate-metaquery-0.1.1 (c (n "metaquery") (v "0.1.1") (d (list (d (n "sqlparser") (r "^0.10.0") (d #t) (k 0)))) (h "05f7r2lgibcxlkfnqmwd0p9ji3fclc1j26ylk69ivki4vjapnf6p")))

(define-public crate-metaquery-0.2.0 (c (n "metaquery") (v "0.2.0") (d (list (d (n "sqlparser") (r "^0.10.0") (d #t) (k 0)))) (h "1ckika75g2qzv5vwkhbgnqxwf3ww6wl85vl8yqgi23zx9bdsrww6")))

(define-public crate-metaquery-0.2.1 (c (n "metaquery") (v "0.2.1") (d (list (d (n "sqlparser") (r "^0.10.0") (d #t) (k 0)))) (h "02wsn5qap3k5ghyyhbcsd9nqxi0y2azhph6y0c4vfksnq03p8vdb")))

(define-public crate-metaquery-0.3.0 (c (n "metaquery") (v "0.3.0") (d (list (d (n "sqlparser") (r "^0.10.0") (d #t) (k 0)))) (h "0j6l98a5kp7r7yn63p524210y1rhj0jbj7ag2msl2x7xlc4jwcj0")))

(define-public crate-metaquery-0.3.1 (c (n "metaquery") (v "0.3.1") (d (list (d (n "sqlparser") (r "^0.10.0") (d #t) (k 0)))) (h "1cgxkikj28iwlda20r4x8nycj9pbr8ijm390xr2s9p355pdj5rk3")))

(define-public crate-metaquery-0.4.0 (c (n "metaquery") (v "0.4.0") (d (list (d (n "sqlparser") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1l7i21vwhnyjxns08xgh5v886mmjiw1nxnh23k17sh0flscnhf8r")))

(define-public crate-metaquery-0.5.0 (c (n "metaquery") (v "0.5.0") (d (list (d (n "sqlparser") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "07z40psq30smiqgsp92lfsy8fdwmv9lw9vg5jryxq0rg45fh3kwj")))

