(define-module (crates-io me ta metac) #:use-module (crates-io))

(define-public crate-metac-0.1.0 (c (n "metac") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)))) (h "1cw2n3sv1dd6cd8k41jdysqmbrf2y255x1if1xgr7h3w2h31bz4r")))

(define-public crate-metac-0.1.1 (c (n "metac") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1nm2ycvhzlfqwpd4ydjic40q0nq9sw86vmpl6z3mf1vvj1w26hsf")))

