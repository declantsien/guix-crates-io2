(define-module (crates-io me ta metaemu-arch-additional) #:use-module (crates-io))

(define-public crate-metaemu-arch-additional-0.1.0 (c (n "metaemu-arch-additional") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fugue") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "metaemu") (r "^0.3") (f (quote ("concrete"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ux") (r "^0.1") (d #t) (k 0)))) (h "0zfmi6iclcmb5yar98j3wcvk4wrnf4laldgnvhk81ycqvd049903")))

