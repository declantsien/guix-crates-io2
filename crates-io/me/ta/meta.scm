(define-module (crates-io me ta meta) #:use-module (crates-io))

(define-public crate-meta-0.0.2 (c (n "meta") (v "0.0.2") (h "1rckf9sin1lnvlx46qws5q14641crzmx398v8bvyhvqaim6lyl1x") (y #t)))

(define-public crate-meta-0.1.0 (c (n "meta") (v "0.1.0") (h "04f4589i22xpkv5lxjzpcyzqminpqm5v6an860gqlab3zim2sp51") (y #t)))

(define-public crate-meta-0.1.1 (c (n "meta") (v "0.1.1") (h "08yq6mm4pg7ry7cgqmw7ly5ppwh78fcay1c7a18y6ny4g0ki2fnj")))

