(define-module (crates-io me ta metastruct) #:use-module (crates-io))

(define-public crate-metastruct-0.1.0 (c (n "metastruct") (v "0.1.0") (d (list (d (n "metastruct_macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "02ziqr2ijb1znvmvyggzk8n4lkwrlwpclc4mlfpyj7q9qbg8hivk") (f (quote (("macro" "metastruct_macro") ("default" "macro"))))))

(define-public crate-metastruct-0.1.1 (c (n "metastruct") (v "0.1.1") (d (list (d (n "metastruct_macro") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "01hgqsp19nw3bawnwx2bglgnzcdbyry97832pc2rpc16ca1biyyc") (f (quote (("macro" "metastruct_macro") ("default" "macro"))))))

