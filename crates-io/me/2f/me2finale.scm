(define-module (crates-io me #{2f}# me2finale) #:use-module (crates-io))

(define-public crate-me2finale-0.1.0 (c (n "me2finale") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 2)))) (h "1lvhwp6px4c5plzcpq01wbawfzd5lvxfd19f9334xppb2pb48jm5") (s 2) (e (quote (("generate" "dep:dashmap" "dep:itertools" "dep:rayon"))))))

(define-public crate-me2finale-0.2.0 (c (n "me2finale") (v "0.2.0") (d (list (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 2)))) (h "05p3d82mpirhi28nyrkhd2k0f2f22k6kqbrlglgqrizin17812ys") (s 2) (e (quote (("generate" "dep:dashmap" "dep:itertools" "dep:rayon"))))))

