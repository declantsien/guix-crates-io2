(define-module (crates-io me ka mekadb) #:use-module (crates-io))

(define-public crate-mekadb-0.1.0 (c (n "mekadb") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (f (quote ("prost"))) (d #t) (k 1)))) (h "0cym8835a61yr9yk85qhfdb4kh5s87vklllqf3qaxvf4i09zzh0f") (r "1.64")))

