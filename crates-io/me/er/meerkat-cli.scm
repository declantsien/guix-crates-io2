(define-module (crates-io me er meerkat-cli) #:use-module (crates-io))

(define-public crate-meerkat-cli-0.1.0 (c (n "meerkat-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1p9h53h8yhqvi0wnsr0h0ah6vpgrwmjzdvb0gadfwqj2gjvp28l3")))

(define-public crate-meerkat-cli-0.1.1 (c (n "meerkat-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1gsshhgsmlsi6gsmrkkyz4hmnvv7ida84z3rw091lm8y66kv8m0w")))

