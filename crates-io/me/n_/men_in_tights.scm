(define-module (crates-io me n_ men_in_tights) #:use-module (crates-io))

(define-public crate-men_in_tights-0.0.1 (c (n "men_in_tights") (v "0.0.1") (h "1gscix00x6rskfy8simqgqix1lawx53i99fm4scgdfap2jqx6w22")))

(define-public crate-men_in_tights-0.0.2 (c (n "men_in_tights") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19dy7v20jh52rj3wjqq83lmyd94jwna32jdka50qyhbbywkqgxln")))

