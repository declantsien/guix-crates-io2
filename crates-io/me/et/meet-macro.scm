(define-module (crates-io me et meet-macro) #:use-module (crates-io))

(define-public crate-meet-macro-0.1.0 (c (n "meet-macro") (v "0.1.0") (d (list (d (n "boolinator") (r "^2.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xwjv5nyz5dwk0fvl0s2vwp7qn6p7v22nwvj9sr4m2z1yj5p8vlw")))

