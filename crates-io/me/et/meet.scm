(define-module (crates-io me et meet) #:use-module (crates-io))

(define-public crate-meet-0.1.0 (c (n "meet") (v "0.1.0") (h "18sqcxdr47jh0552prmxp3zvwbpxv8wm0vnaihf6jjpi3yndjwwm")))

(define-public crate-meet-0.2.0 (c (n "meet") (v "0.2.0") (d (list (d (n "notify") (r "^4.0.10") (d #t) (k 0)))) (h "0xvhc7npshdw6m2ml6524b9fcbda96i1vgx8hkhar7cvzn5whi47")))

