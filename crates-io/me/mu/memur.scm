(define-module (crates-io me mu memur) #:use-module (crates-io))

(define-public crate-memur-0.1.0 (c (n "memur") (v "0.1.0") (h "0b9v7iwjdb2pp1smhyhrhx93c4825z3hcigbga3i97jy5ilgz90i")))

(define-public crate-memur-0.1.1 (c (n "memur") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0935qga30hg4xqbh4mpg1fn5mb0gzav8kkk74mhf54xy60gldpva") (f (quote (("logging" "log") ("default"))))))

(define-public crate-memur-0.1.2 (c (n "memur") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0nxak50wa0hv5mi91k9xymv1k9fayzzwa8smfmy3rqwqnh1bjqzk") (f (quote (("logging" "log") ("default"))))))

