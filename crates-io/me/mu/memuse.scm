(define-module (crates-io me mu memuse) #:use-module (crates-io))

(define-public crate-memuse-0.0.0 (c (n "memuse") (v "0.0.0") (h "0b67yw68s64xz74b2w205299xpa2vqzh1035hvk3afy6yb29h4qa") (y #t)))

(define-public crate-memuse-0.1.0 (c (n "memuse") (v "0.1.0") (d (list (d (n "nonempty") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1fgxr4yrmpsm0qbsyxh8bckdjc2baz2a3n81yfm8aqmdbijiqp91")))

(define-public crate-memuse-0.2.0 (c (n "memuse") (v "0.2.0") (d (list (d (n "nonempty") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0fsqbd6a8l8396pi4l0n4f5zyjvp8awrxscpv39rlxi8fp6jb7gn")))

(define-public crate-memuse-0.2.1 (c (n "memuse") (v "0.2.1") (d (list (d (n "nonempty") (r "^0.7") (o #t) (d #t) (k 0)))) (h "06kgsfv8fnhqbwnq3q841ndfq5wkanz5jpykldpfmrdc6na8ci91")))

