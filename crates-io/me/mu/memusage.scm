(define-module (crates-io me mu memusage) #:use-module (crates-io))

(define-public crate-memusage-0.1.0 (c (n "memusage") (v "0.1.0") (h "0k9ajmgv5ni5kfpmmfkmp5sh5lk7qz9xl09px2p9y7pv8a0qnmma")))

(define-public crate-memusage-0.2.0 (c (n "memusage") (v "0.2.0") (h "1aqxq1l51cdf8j2spp9b4vjnms1rq3hzk8rg4ydamp2qlxmi21js") (f (quote (("std") ("default" "alloc" "std") ("alloc"))))))

