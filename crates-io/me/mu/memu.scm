(define-module (crates-io me mu memu) #:use-module (crates-io))

(define-public crate-memu-0.1.0 (c (n "memu") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ir7zj1dq5j1vbjm6nhrghjwds2x855c7lf2wfg08rcy91fidp4l") (f (quote (("units") ("serde") ("macro") ("default" "macro" "units"))))))

(define-public crate-memu-0.1.1 (c (n "memu") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)))) (h "15k1zzqzmmi79ljgjgjf1ng2my0rhymy0asxf90aa54i7rwq2rr1") (f (quote (("units") ("serde") ("macro") ("default" "macro" "units"))))))

(define-public crate-memu-0.1.3 (c (n "memu") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)))) (h "1icycihssvp4ama3cimyzxyyk75y4ndpkibkda5zv16fgk2zx9hh") (f (quote (("units") ("serde") ("macro") ("default" "macro" "units"))))))

