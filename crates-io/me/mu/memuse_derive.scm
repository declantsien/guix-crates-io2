(define-module (crates-io me mu memuse_derive) #:use-module (crates-io))

(define-public crate-memuse_derive-0.0.0 (c (n "memuse_derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "05v8xjdabx57iyl195h4gy6nm5m0s87vr5vj8d1k1s8h174jnaig")))

