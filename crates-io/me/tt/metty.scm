(define-module (crates-io me tt metty) #:use-module (crates-io))

(define-public crate-metty-2024.3.0 (c (n "metty") (v "2024.3.0") (d (list (d (n "ascii_table") (r "^4.0.3") (f (quote ("color_codes"))) (d #t) (k 0)) (d (n "cadmium-yellow") (r "^2024.3.2") (d #t) (k 0)) (d (n "clap") (r "~4.4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "17jh9ixj1j9f9b5xia7ijpn70ikpv6aaq4b70mv23fxy3rbxndg6")))

(define-public crate-metty-2024.3.1 (c (n "metty") (v "2024.3.1") (d (list (d (n "ascii_table") (r "^4.0.3") (f (quote ("color_codes"))) (d #t) (k 0)) (d (n "cadmium-yellow") (r "^2024.3.2") (d #t) (k 0)) (d (n "clap") (r "~4.4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap_complete") (r "~4.4.0") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1cj8n33v15g5vr0x38ajdmbg98wnkj7z2bdikwfqqrvm5cq456pa")))

