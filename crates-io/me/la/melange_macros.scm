(define-module (crates-io me la melange_macros) #:use-module (crates-io))

(define-public crate-melange_macros-0.1.0 (c (n "melange_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0h81nx2vw41dyxi80vs90hwkranrcr71l2ngh0zc0yhwzr8apjak")))

