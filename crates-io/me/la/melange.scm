(define-module (crates-io me la melange) #:use-module (crates-io))

(define-public crate-melange-0.1.0 (c (n "melange") (v "0.1.0") (d (list (d (n "cblas") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)) (d (n "melange_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "openblas-src") (r "^0.9") (f (quote ("cblas"))) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "0yg88cd5d7471zjk6sf03d3axrnrjwjxjgb97bvwfw8g7b2dwym1")))

