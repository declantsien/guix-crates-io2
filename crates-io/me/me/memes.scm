(define-module (crates-io me me memes) #:use-module (crates-io))

(define-public crate-memes-0.1.0 (c (n "memes") (v "0.1.0") (h "1al7fh1kxisr23p9d6lhivzsm7f0r6adr0m2mysli5iixprsbmky")))

(define-public crate-memes-0.1.1 (c (n "memes") (v "0.1.1") (h "02iapwv04jwzlrwwdzqcnk0d8c7jkq93ddw8d697375nim97bbq6")))

