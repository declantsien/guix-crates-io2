(define-module (crates-io me me meme-cli) #:use-module (crates-io))

(define-public crate-meme-cli-0.1.0 (c (n "meme-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "arboard") (r "^2") (d #t) (t "cfg(not(target_os = \"android\"))") (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "memeinator") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "07jpmpgknpyg4w9n24srq4ifjqvc3rmqi8cpqjf5jrsx1q9z2c5w")))

(define-public crate-meme-cli-0.1.1 (c (n "meme-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "arboard") (r "^2") (d #t) (t "cfg(not(target_os = \"android\"))") (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "memeinator") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0c9qaz6k7bzn9706mx2g1zlhcqcsgqgxpl78dwx67cn6k6fgylx3")))

(define-public crate-meme-cli-0.1.2 (c (n "meme-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "arboard") (r "^2") (d #t) (t "cfg(not(target_os = \"android\"))") (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "memeinator") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1cli2ln65nh2iq1sg5lcks7xk1fpczpq3nvjarl2mfdgdl9zi516")))

