(define-module (crates-io me me memedb_core) #:use-module (crates-io))

(define-public crate-memedb_core-1.0.0 (c (n "memedb_core") (v "1.0.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (f (quote ("max_level_debug" "release_max_level_off"))) (d #t) (k 0)))) (h "0z1llk24vrdxdzf0ii38xv6cdqipl7kc31njqz0zkncfqx0x7z61")))

(define-public crate-memedb_core-1.0.1 (c (n "memedb_core") (v "1.0.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (f (quote ("max_level_debug" "release_max_level_off"))) (d #t) (k 0)))) (h "05b8p44dzhngw8xmqvqs682kjd0b4s5nwwdpqv5bv4j6k2xi1rkh")))

(define-public crate-memedb_core-1.0.2 (c (n "memedb_core") (v "1.0.2") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (f (quote ("max_level_debug" "release_max_level_off"))) (d #t) (k 0)))) (h "05yq60l9nibx1irvf1mi279hmkz38vm36m4zdqiphyj22zc18yyr")))

(define-public crate-memedb_core-2.0.0 (c (n "memedb_core") (v "2.0.0") (d (list (d (n "crc") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "13gccbxi3l0i42jbnwgxjqiacpirzkz2al2cx8y62cmis0d2yh0n") (f (quote (("riff") ("png" "crc") ("jpeg" "memchr") ("isobmff") ("gif") ("default" "gif" "isobmff" "jpeg" "png" "riff"))))))

(define-public crate-memedb_core-2.0.1 (c (n "memedb_core") (v "2.0.1") (d (list (d (n "crc") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0aayx356c822mrijqy4axwbaslrw9qin7b2vxr5rzxk78myjm08q") (f (quote (("riff") ("png" "crc") ("jpeg" "memchr") ("isobmff") ("gif") ("default" "gif" "isobmff" "jpeg" "png" "riff"))))))

