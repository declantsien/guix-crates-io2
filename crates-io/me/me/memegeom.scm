(define-module (crates-io me me memegeom) #:use-module (crates-io))

(define-public crate-memegeom-0.1.0 (c (n "memegeom") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "earcutr") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "ordered-float") (r "^2.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rust-dense-bitset") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "1gy8bhlkk6x3f0qmcf8wdf3i2nd8kdrhxsrr46b24g3022lsphbi")))

