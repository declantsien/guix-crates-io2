(define-module (crates-io me me memedit) #:use-module (crates-io))

(define-public crate-memedit-0.1.0 (c (n "memedit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "procfs") (r "^0.15.1") (d #t) (k 0)))) (h "1vyx3zr9j0qx80kr97958gjlszzqigpn8k36xbzaf27c8x1j5qc4")))

