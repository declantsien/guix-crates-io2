(define-module (crates-io me me memeinator) #:use-module (crates-io))

(define-public crate-memeinator-0.1.0 (c (n "memeinator") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "fontdue") (r "^0.6.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0q7zhq0f7p9nmm1yvhgbkjg83wjdaviydjg85i2f9l8d9r10m159")))

