(define-module (crates-io me me memestr) #:use-module (crates-io))

(define-public crate-memestr-0.1.0 (c (n "memestr") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "eyre") (r "^0.6.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a7fljgwvnscfhhkia50wcymvamiqqffanchi3h106nbzn8wly03")))

