(define-module (crates-io me me memestat) #:use-module (crates-io))

(define-public crate-memestat-0.1.0 (c (n "memestat") (v "0.1.0") (d (list (d (n "average") (r "^0.13.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "eyre") (r "^0.6.7") (d #t) (k 0)) (d (n "mathru") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1x2qyxgxf1jy9nayhkqlbdgh729z1q3czmm2ll1wp97dl6pjpxga")))

