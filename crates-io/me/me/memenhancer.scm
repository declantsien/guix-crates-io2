(define-module (crates-io me me memenhancer) #:use-module (crates-io))

(define-public crate-memenhancer-0.1.0 (c (n "memenhancer") (v "0.1.0") (d (list (d (n "handlebars") (r "^0.21") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0nq3z4kava9jhjxxl93vcqq2w65g89f0z8zhzsbh5pypamwwvqf6")))

