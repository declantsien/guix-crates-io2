(define-module (crates-io me me memexec) #:use-module (crates-io))

(define-public crate-memexec-0.1.0 (c (n "memexec") (v "0.1.0") (h "1iy8vk41icq7jdabw78c6nn0d12jrky4zp04c6g00dhgmqlia5sm")))

(define-public crate-memexec-0.1.1 (c (n "memexec") (v "0.1.1") (h "18pdpqjqcfabv9i4ldvh13gn7j1xnpg9nxwkamh65fl57pc4g6gg")))

(define-public crate-memexec-0.1.2 (c (n "memexec") (v "0.1.2") (h "01b1v9l7acyq4gn30rlzsdlkxpx2xxwnm4sjc23gf9avmw5imqdd")))

(define-public crate-memexec-0.2.0 (c (n "memexec") (v "0.2.0") (h "0z0bamg4m94xp1bja2zz59xqk95lixjak8ydc8c5vnl192qwqqmw") (f (quote (("hook"))))))

