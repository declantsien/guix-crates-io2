(define-module (crates-io me me mememo) #:use-module (crates-io))

(define-public crate-mememo-0.1.0 (c (n "mememo") (v "0.1.0") (h "0zabrqdlqs1nc1v0lgpdhcm68850wbdjqp7jk5ibz43x0bi4y2k5")))

(define-public crate-mememo-0.1.1 (c (n "mememo") (v "0.1.1") (h "0zjamdwnzd7rzdmcml2k0lll86jcf7wy32asmrh2d8slm0nq3frh")))

