(define-module (crates-io me me memento) #:use-module (crates-io))

(define-public crate-memento-0.1.0 (c (n "memento") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("bytes" "net" "io-util"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "1fqgp99smj3yb2zd8v4c13vhgjlrcxlj4y3g1g3pnkxlm4ny2dxc")))

(define-public crate-memento-0.2.0 (c (n "memento") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("bytes" "net" "io-util"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "1p8125v01ih9na79053hm7a96nyb7hbi6ah4f5l8sfzgssdfkx1y")))

(define-public crate-memento-0.2.1 (c (n "memento") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("bytes" "net" "io-util"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "05z3arcgf7za6xbhlbaqbspacari3kgq84axddvlr5jii7g8b80h")))

