(define-module (crates-io me za meza) #:use-module (crates-io))

(define-public crate-meza-0.1.0 (c (n "meza") (v "0.1.0") (h "0bd3ka9id3phhrxfm93dcb1xaqm03bic6nlh8ra1j09sa1frja3n")))

(define-public crate-meza-0.2.0 (c (n "meza") (v "0.2.0") (h "0brmn4gjmir5jdx3v2diy3y0syfv2hbj25kgpvkm95v55zg2i9vd")))

(define-public crate-meza-0.2.1 (c (n "meza") (v "0.2.1") (h "0ccml2kia7hxn8vbfm8mcb85m3f4jfcdhwvbxzxp89nzpgrvmpz2")))

