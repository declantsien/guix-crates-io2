(define-module (crates-io me ge megenginelite-rs) #:use-module (crates-io))

(define-public crate-megenginelite-rs-1.7.0 (c (n "megenginelite-rs") (v "1.7.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atomic-waker") (r "^1") (d #t) (k 0)) (d (n "megenginelite-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "megenginelite-sys") (r "^1.7.0") (d #t) (k 0)))) (h "0cy43qdjfxgkvx67r46z4aphfr3dyhiaxf7jih5iv1njhih56k90")))

(define-public crate-megenginelite-rs-1.7.0-1 (c (n "megenginelite-rs") (v "1.7.0-1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atomic-waker") (r "^1") (d #t) (k 0)) (d (n "megenginelite-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "megenginelite-sys") (r "^1.7.0-1") (d #t) (k 0)))) (h "0jby15lsp7fd1swangfqaym5r9najvwdnnmqwd0jyc30z0w64rby")))

(define-public crate-megenginelite-rs-1.8.2 (c (n "megenginelite-rs") (v "1.8.2") (d (list (d (n "async-channel") (r "^1") (d #t) (k 0)) (d (n "atomic-waker") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "megenginelite-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "megenginelite-sys") (r "^1.8.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1drqqzmdmiigv3b5zv2x1r2fljd6w5ihlwjrn9k4qhw23vkn4qbp") (f (quote (("ndarray-basis" "ndarray") ("default" "auto-load") ("auto-load"))))))

