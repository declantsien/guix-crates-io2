(define-module (crates-io me ge megenginelite-derive) #:use-module (crates-io))

(define-public crate-megenginelite-derive-0.1.0 (c (n "megenginelite-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mkq75xinr6z36cy688l1vcsrvqr9vf0shvnfnfnac986a0i8g4g")))

(define-public crate-megenginelite-derive-0.2.0 (c (n "megenginelite-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nhx4lx67r4l39qsx3cqylsdwjzwyfrm76q0v9m7c99pi0h7gpla")))

