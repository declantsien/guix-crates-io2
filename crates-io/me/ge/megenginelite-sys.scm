(define-module (crates-io me ge megenginelite-sys) #:use-module (crates-io))

(define-public crate-megenginelite-sys-1.7.0 (c (n "megenginelite-sys") (v "1.7.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmd_lib") (r "^1.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1nk5836q54viwnxgp31sj7hwf3g8yixbjvfnby732miy9cxnb45m")))

(define-public crate-megenginelite-sys-1.7.0-1 (c (n "megenginelite-sys") (v "1.7.0-1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmd_lib") (r "^1.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0qch5mmkkkdsxc0svxxgxbf1dn3jyqayp683a0siky5fp9mza9j5")))

(define-public crate-megenginelite-sys-1.8.2 (c (n "megenginelite-sys") (v "1.8.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "whoami") (r "^1") (d #t) (k 1)))) (h "01ps6nw92vlj1yb0inrjn4549qgqhn9s008g29y32b8mq287ldzh")))

