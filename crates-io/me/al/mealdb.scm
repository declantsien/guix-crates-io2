(define-module (crates-io me al mealdb) #:use-module (crates-io))

(define-public crate-mealdb-0.1.0 (c (n "mealdb") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 2)))) (h "17s3lgcx02fl55jy51wb4p427sbdds2p4qxz31m4hm5acqkifpyp")))

(define-public crate-mealdb-0.1.1 (c (n "mealdb") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0zr0psq7iq6gd1q9lxxy7drbgsa112q61hmw8kh7xqa9fm7p4dpi")))

