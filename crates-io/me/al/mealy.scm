(define-module (crates-io me al mealy) #:use-module (crates-io))

(define-public crate-mealy-0.1.0 (c (n "mealy") (v "0.1.0") (h "1282axj21fqvixxca7faxxnnckjh8xi7hzy5mpg7abi3baj3n4wr")))

(define-public crate-mealy-0.1.1 (c (n "mealy") (v "0.1.1") (h "0w7wswdnji4ii14b7wz6kba4siv1h06nz5j21qwx51zhkp8pjz4b")))

(define-public crate-mealy-0.1.2 (c (n "mealy") (v "0.1.2") (h "0n17p8rylgcr90rzvl7wpgk6fvrxjm87f5ld0mm51g9nhms29mcz")))

