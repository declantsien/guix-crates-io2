(define-module (crates-io me al meal) #:use-module (crates-io))

(define-public crate-meal-1.0.0 (c (n "meal") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "05h255yamil79vq51xkj1c9rzlqdlrj3s3v76bb1j5gvwyhwy6xg")))

