(define-module (crates-io me rx merx) #:use-module (crates-io))

(define-public crate-merx-0.0.1 (c (n "merx") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("real_blackbox"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1y3167vicr8nydn518y2aq0n67xzqly53f8wyh32p43vc9ijgnlp")))

