(define-module (crates-io me mq memquery) #:use-module (crates-io))

(define-public crate-memquery-0.1.0 (c (n "memquery") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "0dhqcf0xrja25aykgy2cvmnllp52kg40iywwdmccmhyzdw2yi1dw") (f (quote (("sync") ("default" "tokio"))))))

(define-public crate-memquery-0.1.1 (c (n "memquery") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "1q184jc7jkjifglbyzbgnzx4pwm8zjh2yy61i9aly3xgx6wchwvw") (f (quote (("sync") ("default" "tokio"))))))

