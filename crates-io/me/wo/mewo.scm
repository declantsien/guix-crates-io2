(define-module (crates-io me wo mewo) #:use-module (crates-io))

(define-public crate-mewo-0.1.1 (c (n "mewo") (v "0.1.1") (d (list (d (n "mewo_ecs") (r "^0.1.1") (d #t) (k 0)) (d (n "mewo_ecs_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1a3xm39l6awq9hnbv6pyhqfgpf6m5mqfrd2plx1a3sjil53qcnr7") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:mewo_ecs_derive"))))))

(define-public crate-mewo-0.1.2 (c (n "mewo") (v "0.1.2") (d (list (d (n "mewo_ecs") (r "^0.1.2") (d #t) (k 0)) (d (n "mewo_ecs_derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0lmix3v4lm9g3z0v01adz8945gak0pb3700mq8nac93b1vijiixw") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:mewo_ecs_derive"))))))

(define-public crate-mewo-0.1.3 (c (n "mewo") (v "0.1.3") (d (list (d (n "mewo_ecs") (r "^0.1.3") (d #t) (k 0)) (d (n "mewo_ecs_derive") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1fp804djxgqsz2q744840gxjr3fgkn5nrvq9pc5jirx6xmxhkm64") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:mewo_ecs_derive"))))))

(define-public crate-mewo-0.1.4 (c (n "mewo") (v "0.1.4") (d (list (d (n "mewo_ecs") (r "^0.1.4") (d #t) (k 0)) (d (n "mewo_ecs_derive") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0c85x6931rmqmg0jxbnchz8wd733wcnpzy3vdi1zcdgb4xgsd9hz") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:mewo_ecs_derive"))))))

