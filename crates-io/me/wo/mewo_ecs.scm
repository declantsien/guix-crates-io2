(define-module (crates-io me wo mewo_ecs) #:use-module (crates-io))

(define-public crate-mewo_ecs-0.1.0 (c (n "mewo_ecs") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0k8cxjz2y50af3923nhycg27h7153jhjgyas0cnsslag97nn6c0v") (y #t)))

(define-public crate-mewo_ecs-0.1.1 (c (n "mewo_ecs") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0n83cm2cpf1s9zj47vnby18k5p39vhdyk61jaw0whs2ax1is1gn1")))

(define-public crate-mewo_ecs-0.1.2 (c (n "mewo_ecs") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0ykydgbyas6fxz67l1ljvhq9s2cr4iwvf1qqhms7hpf0ph2ydplx")))

(define-public crate-mewo_ecs-0.1.3 (c (n "mewo_ecs") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "09y4w1iv4n3sz4w2i0295h9v69aj332824zzycly9jarfg4ln793")))

(define-public crate-mewo_ecs-0.1.4 (c (n "mewo_ecs") (v "0.1.4") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0kqbxpkws7r7swrf06kprzx84kda72xp8x19mgn0bh0jdb5mxl6c")))

