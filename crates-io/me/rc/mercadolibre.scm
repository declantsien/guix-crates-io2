(define-module (crates-io me rc mercadolibre) #:use-module (crates-io))

(define-public crate-mercadolibre-0.0.0 (c (n "mercadolibre") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1z9b2ihd0w1aq57a6fcl7kaq46x68pw2nmi3pbb1xlgwllsi8b0v")))

