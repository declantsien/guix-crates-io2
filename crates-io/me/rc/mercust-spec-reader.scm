(define-module (crates-io me rc mercust-spec-reader) #:use-module (crates-io))

(define-public crate-mercust-spec-reader-0.0.0 (c (n "mercust-spec-reader") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mercust") (r "^0.0.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.1") (d #t) (k 0)))) (h "0akyb7dj64pcf9wq47nmz5hx68pwv76gzmxq1jsglra4c2w9jj36")))

(define-public crate-mercust-spec-reader-0.0.1 (c (n "mercust-spec-reader") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mercust") (r "^0.0.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.1") (d #t) (k 0)))) (h "0bmpcfx0s08hfkrmawgillgaalry4kcjjv0538v66n9cpwqbfd94")))

