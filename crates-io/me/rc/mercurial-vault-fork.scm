(define-module (crates-io me rc mercurial-vault-fork) #:use-module (crates-io))

(define-public crate-mercurial-vault-fork-0.5.1 (c (n "mercurial-vault-fork") (v "0.5.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "1k0rvdk7hxawyrfrikscs03az43c97zgz5zjbzqmmwgcf7sn7kgz") (f (quote (("test-bpf") ("staging") ("no-idl") ("no-entrypoint") ("no-capture") ("devnet") ("default") ("cpi" "no-entrypoint"))))))

