(define-module (crates-io me rc mercurial_stable_swap_n_pool_instructions) #:use-module (crates-io))

(define-public crate-mercurial_stable_swap_n_pool_instructions-0.1.0 (c (n "mercurial_stable_swap_n_pool_instructions") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.6.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "06f9h8b7j682bjdy7y95nnkys5nv7qakygl6p8xzd71madx3yfzh")))

