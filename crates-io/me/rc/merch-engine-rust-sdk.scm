(define-module (crates-io me rc merch-engine-rust-sdk) #:use-module (crates-io))

(define-public crate-merch-engine-rust-sdk-0.1.0 (c (n "merch-engine-rust-sdk") (v "0.1.0") (h "0af3c71cnvpr2mxz6f2sdv36mg1qin50nd9ixiswq91vmrpfs346")))

(define-public crate-merch-engine-rust-sdk-0.2.0 (c (n "merch-engine-rust-sdk") (v "0.2.0") (h "0w9dmzpm21xx4pdgi3wn060k8li6yj2c2d9n4wqdk305bjvydhlz")))

