(define-module (crates-io me rc mercy-cli) #:use-module (crates-io))

(define-public crate-mercy-cli-0.1.7 (c (n "mercy-cli") (v "0.1.7") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mercy") (r "^1.2.17") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)))) (h "16cgmi30q93w5bnrfmmsvcax15bnxqm5757x4ndy8fbgjvskpyz8")))

(define-public crate-mercy-cli-0.1.8 (c (n "mercy-cli") (v "0.1.8") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mercy") (r "^1.2.17") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)))) (h "0vlgbcygj2dffc6c6b4hp73bdijw6hlgmalfx6yafgys1q4j87rb")))

