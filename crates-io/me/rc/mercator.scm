(define-module (crates-io me rc mercator) #:use-module (crates-io))

(define-public crate-mercator-0.1.0 (c (n "mercator") (v "0.1.0") (h "0pfd00y5vlbn0vgjxf7w12cgcwyjv2r2kl14aba19jsdvzcg96r3")))

(define-public crate-mercator-0.1.1 (c (n "mercator") (v "0.1.1") (h "1p9kc03pggvqk0vkbccvs2hl68dcb5s1mhvb3yjp9fah1wh7xl89")))

(define-public crate-mercator-0.1.2 (c (n "mercator") (v "0.1.2") (h "1gqc8n2y5pi8q0qmzhlf6as6nka3d0ahmhrmqdbjbff1bhn1cz9k")))

