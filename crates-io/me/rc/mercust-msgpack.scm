(define-module (crates-io me rc mercust-msgpack) #:use-module (crates-io))

(define-public crate-mercust-msgpack-0.0.0 (c (n "mercust-msgpack") (v "0.0.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "mercust") (r "^0.0.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)))) (h "1k6wkhhpigfvyl105dqsc68jg9iiwgn32zk1ab0g9m2rhgss1yg1")))

(define-public crate-mercust-msgpack-0.0.1 (c (n "mercust-msgpack") (v "0.0.1") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "mercust") (r "^0.0.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)))) (h "10jp4pw7jqq3nwwn1gvbnafb1bbvc8xkczjqfccwksf96532mc8w")))

