(define-module (crates-io me rc mercury) #:use-module (crates-io))

(define-public crate-mercury-0.1.0 (c (n "mercury") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.10.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)))) (h "1cw0cxsmvilp4pclpg0qja2vr5i68g65b9v6jd5j85j9pfcbkljr")))

