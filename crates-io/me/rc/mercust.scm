(define-module (crates-io me rc mercust) #:use-module (crates-io))

(define-public crate-mercust-0.0.0 (c (n "mercust") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.189") (d #t) (k 0)))) (h "0636kq8chwalvy491giiss58gi6n7vfd5jm3rwilxqrj4kg7cdvc")))

(define-public crate-mercust-0.0.1 (c (n "mercust") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.189") (d #t) (k 0)))) (h "02d6kwndr50zy0lv04mrhhi7fy13f82mmm59fsyj3wy6xx5qaf1p")))

