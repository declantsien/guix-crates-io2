(define-module (crates-io me ru meru-interface) #:use-module (crates-io))

(define-public crate-meru-interface-0.1.0 (c (n "meru-interface") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bevy_input") (r "^0.7") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "egui") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "08vpiymw4iwyrzcgaj604d6vhivmrg8f5p7z8zx4xql9vlb1y2rm")))

(define-public crate-meru-interface-0.2.0 (c (n "meru-interface") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "04k8z5rrvi7vwy50lj69igfp9p3ahijp18jzyhrdi2cc3wj2glfa")))

(define-public crate-meru-interface-0.3.0 (c (n "meru-interface") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "base64-serde") (r "^0.6.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "15mcqh0l3w4kgsnn00nvxph33kfzivrskwbls8jc16c3m7q1zw5g")))

