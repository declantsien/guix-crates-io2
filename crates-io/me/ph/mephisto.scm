(define-module (crates-io me ph mephisto) #:use-module (crates-io))

(define-public crate-mephisto-0.1.0 (c (n "mephisto") (v "0.1.0") (d (list (d (n "mephisto-raft") (r "^0.1.0") (d #t) (k 0)))) (h "1q2sqh8p6fahlnavifnk1wxnr6b7kv7cic29xma88wnvqnqzhh11")))

(define-public crate-mephisto-0.2.0 (c (n "mephisto") (v "0.2.0") (h "1ggqr064icqy5x6w78id9k74p42mzszn61m1q424rzyscb0rayji")))

