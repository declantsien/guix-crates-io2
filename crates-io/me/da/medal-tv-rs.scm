(define-module (crates-io me da medal-tv-rs) #:use-module (crates-io))

(define-public crate-medal-tv-rs-0.0.1 (c (n "medal-tv-rs") (v "0.0.1") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1rfalnnvw9ivfqhp0kq9185pdf278z5axg05k3hnsh0bm1cmfyzp") (r "1.77")))

