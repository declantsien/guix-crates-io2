(define-module (crates-io me da medallion) #:use-module (crates-io))

(define-public crate-medallion-1.0.0 (c (n "medallion") (v "1.0.0") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1rva500hh0kyqc5kx2vcs32dgvnrqf9aam63zn96g42l75xvdvnk")))

(define-public crate-medallion-1.0.1 (c (n "medallion") (v "1.0.1") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0mik5rp8gj4imhk41azgg318cnhlv9byz8naj2hbv0r4rw56pk6w")))

(define-public crate-medallion-1.1.0 (c (n "medallion") (v "1.1.0") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0lkd75vgvfdn44rrzswcvmrvsvp79dwf8y613rpj6jzsv07bizwn")))

(define-public crate-medallion-1.1.1 (c (n "medallion") (v "1.1.1") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1mvxq87z4jcmjry78sc3bb2b4013vq8pr6rf8517jjh6qgwjzr6l")))

(define-public crate-medallion-2.0.0 (c (n "medallion") (v "2.0.0") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0bbih6b3qn320pp5agkpla0r72fmkxishr8srhpfjd6xmax03b5k")))

(define-public crate-medallion-2.0.1 (c (n "medallion") (v "2.0.1") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "12nz0jvslp880n82lqrlmkg5kf62xv6j49lq8d8qs0w5r9x4wzi1")))

(define-public crate-medallion-2.0.2 (c (n "medallion") (v "2.0.2") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1iqddxvi1cmy378jy1862bnlxfi9bgdkcahv9vmkbaqw49y2ijzx")))

(define-public crate-medallion-2.1.0 (c (n "medallion") (v "2.1.0") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1wj0j61ab7rg7qzw7k3vyl9q3crkxwswc0xj4qd11dix90hsj8p7")))

(define-public crate-medallion-2.2.0 (c (n "medallion") (v "2.2.0") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "04aks28dqql9lp6wqz8mii05s04kwq6b8xl5cvvfvs7ywb1ifr3h")))

(define-public crate-medallion-2.2.1 (c (n "medallion") (v "2.2.1") (d (list (d (n "base64") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.9.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "158qs6rzv81k65x7zsd6c63j6x29mahhlchp4xw92iwjb6aw4nw9")))

(define-public crate-medallion-2.2.2 (c (n "medallion") (v "2.2.2") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "08r6cmvaj8cmimihbzk69nlr94kxx2knlrsv2cvhps7183ncyqb4")))

(define-public crate-medallion-2.2.3 (c (n "medallion") (v "2.2.3") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "148q3j180vcw19nf1j5bm28y3knzcc53mdp97py7909q7dqz1rmj")))

(define-public crate-medallion-2.3.0 (c (n "medallion") (v "2.3.0") (d (list (d (n "base64") (r "~0.10.0") (d #t) (k 0)) (d (n "failure") (r "~0.1.3") (d #t) (k 0)) (d (n "openssl") (r "~0.10.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "time") (r "~0.1.40") (d #t) (k 0)))) (h "0zx2pqmy9m4j58mq2kzn1via4zg3lcid3ppm0p0dygl8084ld420")))

(define-public crate-medallion-2.3.1 (c (n "medallion") (v "2.3.1") (d (list (d (n "base64") (r "~0.10.0") (d #t) (k 0)) (d (n "failure") (r "~0.1.3") (d #t) (k 0)) (d (n "openssl") (r "~0.10.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "time") (r "~0.1.40") (d #t) (k 0)))) (h "16bv88qk7galyvr8yk7k0l1wzcq99z9d0ishb88vwg5q408wgnmn")))

(define-public crate-medallion-2.4.0 (c (n "medallion") (v "2.4.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "base64") (r "~0.12.2") (d #t) (k 0)) (d (n "chrono") (r "~0.4.11") (d #t) (k 0)) (d (n "openssl") (r "~0.10.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)))) (h "0jbv0jhn88jwcqhka6vrlb0yn4f3dirfn508b9r8sqh96imsg36a")))

(define-public crate-medallion-2.4.1 (c (n "medallion") (v "2.4.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "base64") (r "~0.13.0") (d #t) (k 0)) (d (n "chrono") (r "~0.4.11") (d #t) (k 0)) (d (n "openssl") (r "~0.10.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)))) (h "1d7gdl4b17vx1i4hz8855dh1wimshq4705qrzx095dl674hn49fz")))

(define-public crate-medallion-2.4.2 (c (n "medallion") (v "2.4.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "base64") (r "~0.13.0") (d #t) (k 0)) (d (n "chrono") (r "~0.4.11") (d #t) (k 0)) (d (n "openssl") (r "~0.10.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)))) (h "1gm1sfc2hynb7q87s32mqfaf1bdjh807ipy4c0n35lybrv1c5ans")))

(define-public crate-medallion-2.5.0 (c (n "medallion") (v "2.5.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "base64") (r "~0.13.0") (d #t) (k 0)) (d (n "openssl") (r "~0.10.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "time") (r "~0.3") (d #t) (k 0)))) (h "1232y3rd3zyqml2p9k0mny2jy5r12cy4xckf7asj5mvp6863rf1m")))

