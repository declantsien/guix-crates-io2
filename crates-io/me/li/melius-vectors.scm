(define-module (crates-io me li melius-vectors) #:use-module (crates-io))

(define-public crate-melius-vectors-0.1.0 (c (n "melius-vectors") (v "0.1.0") (h "13hsnz8g46427408iywhyasvvg50s8bh2y89f1pln94ik1bhbgca")))

(define-public crate-melius-vectors-0.1.1 (c (n "melius-vectors") (v "0.1.1") (h "02av0j0g9mr7nyk45pkyd7hsp4mvk2msvd41970m8xddk4v0pxls")))

(define-public crate-melius-vectors-0.1.2 (c (n "melius-vectors") (v "0.1.2") (h "0i89bmba7haazj4h66y9j4gbr6pk9bm2xjx3q5djzaamnl9az3p6")))

