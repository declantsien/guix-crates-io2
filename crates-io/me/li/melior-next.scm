(define-module (crates-io me li melior-next) #:use-module (crates-io))

(define-public crate-melior-next-0.4.0 (c (n "melior-next") (v "0.4.0") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "mlir-sys") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1zvlcj7rw9qns6hh1nzscwr5fk29dizh03zy0p2a341ld05xvy6k")))

(define-public crate-melior-next-0.4.1 (c (n "melior-next") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1wlizbq6zvx28ijw3kxz0m57yw26qh2lf4bjlcphch422ls2sgmx")))

(define-public crate-melior-next-0.4.2 (c (n "melior-next") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0j0ndavnxpcnvsnqhk4b3i0jm415p9fafdkk5fx2dl9sfa9rpnj5")))

(define-public crate-melior-next-0.4.3 (c (n "melior-next") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1zw87f5gi45cqvpsdj2wff7qc8zchjaszmazqnscs927725rv1zr")))

(define-public crate-melior-next-0.4.4 (c (n "melior-next") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "04z1wlas51a0dbfj2bfpxl8g55x3k7w6id9hlwil2nb0g9zb0xrx") (f (quote (("cuda"))))))

(define-public crate-melior-next-0.4.5 (c (n "melior-next") (v "0.4.5") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "16in7rprxr8ha64k9v1myvp7rhy80pzsrcyps5vzvw9qlvg2inqj") (f (quote (("cuda"))))))

(define-public crate-melior-next-0.4.6 (c (n "melior-next") (v "0.4.6") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "05r4yz7d943kf3932g5xp13qjzlp3432rjdjwkqcd8m7hsl4w28y") (f (quote (("cuda"))))))

(define-public crate-melior-next-0.5.0 (c (n "melior-next") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0yk021a6fkb5pzdlnb2qa2ng4kf42wa1dyrzl8qp2z2cgc9i1zry") (f (quote (("cuda"))))))

(define-public crate-melior-next-0.5.1 (c (n "melior-next") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1iyl3m9dq1fr5dalhsm97s7hvacxaa2kg2r8562my41fdbjpy9qw") (f (quote (("cuda"))))))

(define-public crate-melior-next-0.5.2 (c (n "melior-next") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0m0drr3yl8hk0fi9crb6rc8pmd7ns321fllgzbnappz5f1fc6j61") (f (quote (("cuda"))))))

(define-public crate-melior-next-0.5.4 (c (n "melior-next") (v "0.5.4") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.28.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1p69h47dysayx2whfw9w7q1i6hxks695rla4jn5vvk1k781cj5lb") (f (quote (("cuda"))))))

(define-public crate-melior-next-0.5.5 (c (n "melior-next") (v "0.5.5") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1d081nz3910dkxy61wwx13xxdcx396w0b0lz0na6c26bi59spr3l") (f (quote (("cuda"))))))

(define-public crate-melior-next-0.5.6 (c (n "melior-next") (v "0.5.6") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1rdixnv8qxlcbwbh91z84z4jawkr2gbfnlvifjjbsfhkvr7vv8k1") (f (quote (("cuda"))))))

(define-public crate-melior-next-0.5.7 (c (n "melior-next") (v "0.5.7") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 2)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1rppwpf1x788nf9znc744m5rfw154c4c4hvj6zw6pwmz80qk6mkk") (f (quote (("cuda"))))))

(define-public crate-melior-next-0.6.0 (c (n "melior-next") (v "0.6.0") (h "0kfcfzdh9sqyvjyq432ib4093yhriarq28y43pqllk9d3nzd2b1y")))

(define-public crate-melior-next-0.6.1 (c (n "melior-next") (v "0.6.1") (h "0cmyybac1sjwws0smplk3c2szn9728r1qxb4gg86ydg86qlhxvvp")))

