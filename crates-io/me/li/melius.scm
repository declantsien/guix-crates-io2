(define-module (crates-io me li melius) #:use-module (crates-io))

(define-public crate-melius-0.1.0 (c (n "melius") (v "0.1.0") (h "15raxwz1kwcxb7g7k84invmi2s29pb4kfhi8w0i6fa1fr0p9ramn")))

(define-public crate-melius-0.2.0 (c (n "melius") (v "0.2.0") (h "0dm50s7rqcci4z8704rhyc56pqjc2v2ghd5bpfhw3jpjix8azkh8")))

(define-public crate-melius-0.3.0 (c (n "melius") (v "0.3.0") (h "0li8kzh4pbsjxj33ccqnd2hdkb1sdva66rsfvpp6acpsc8bb36yw")))

(define-public crate-melius-0.4.0 (c (n "melius") (v "0.4.0") (h "195q8di6wkjilj7a43wg1wbjrsmi9132d25yk7gp948jinp7ylpy")))

(define-public crate-melius-0.5.0 (c (n "melius") (v "0.5.0") (h "0hbzjk3hz62mpw2dqs78p776mcs7zhr8v33zwh6684jyl0hgnqc3")))

(define-public crate-melius-0.6.0 (c (n "melius") (v "0.6.0") (h "1vd6nq9vvy4v4nd2z9p9g9fdd0n5dkxx24a5pgh024q6b52zgy0q")))

(define-public crate-melius-0.7.0 (c (n "melius") (v "0.7.0") (h "0s9p1l3xnl21r7iad0bdmgkfdfh6cl931y29fv4m6x0kqn6vih21")))

