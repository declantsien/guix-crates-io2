(define-module (crates-io me li meliusgfx) #:use-module (crates-io))

(define-public crate-meliusgfx-0.1.0 (c (n "meliusgfx") (v "0.1.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0v1wnvmhlkq8kiyv7grnfs7wlsxpq07m32kqw7bjmmvr4r4ximlr")))

(define-public crate-meliusgfx-0.1.1 (c (n "meliusgfx") (v "0.1.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0wzlfbr50x72ak1pvh62z9116dybjk63956sgkvwb0x9j4xkr8b3")))

(define-public crate-meliusgfx-1.0.0 (c (n "meliusgfx") (v "1.0.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.41.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "03x4r9qx6pfg37484ixik46mmrizx7d0vn8796yidp09xalirzwr")))

(define-public crate-meliusgfx-1.1.0 (c (n "meliusgfx") (v "1.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.41.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)))) (h "1aialy3jwlv55zgkkqhpamagv55l6qss1kbqglgh0lcqx4ms3m69")))

(define-public crate-meliusgfx-1.1.1 (c (n "meliusgfx") (v "1.1.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)))) (h "1wjp264nzsr63zn9xg0vhsgxbpnf9h9x6x1cgpx5pkmciwrar1zl")))

(define-public crate-meliusgfx-1.1.2 (c (n "meliusgfx") (v "1.1.2") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.41.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)))) (h "0n7wsxh6a4qx5ipwblkh83mg6b1jji7kw7x48vz7bfl5zp2zdr2y")))

