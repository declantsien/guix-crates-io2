(define-module (crates-io me rs mers_lib) #:use-module (crates-io))

(define-public crate-mers_lib-0.3.1 (c (n "mers_lib") (v "0.3.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "079a8kz0zli024yb480k5d8mx2gzd4jy34lywd92381hb6xl4kf2") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.3.2 (c (n "mers_lib") (v "0.3.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "0jh200zxapyjjxzc8qx60x0wp8iv8nklk8812p79b0vy7n5pj2nk") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.3.3 (c (n "mers_lib") (v "0.3.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "1fhnz6x7w4s2qigd9ywgpdf9fi6ddp507ika40cwmha376r95djb") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.3.4 (c (n "mers_lib") (v "0.3.4") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "1s31wnlq7sb0zhmw5bf8pq0l780bpqp004znc26z6zmryh6vb6if") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.3.5 (c (n "mers_lib") (v "0.3.5") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "03lxci5ibfg0h549qdhl64imjprs15zks7dpmdwvsq31qrqdd22d") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.3.7 (c (n "mers_lib") (v "0.3.7") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "0m8qzdr4i3pv6rqw7lhrpxwyjyh9ghv926wkbmx9l6l36l2q3rzw") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.4.0 (c (n "mers_lib") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "16f9af0bwcb1gh8n0zyavhb6id6zi7cd6j0gzq7vil2yvachnn37") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.5.0 (c (n "mers_lib") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "1wnbf2ljjrvbs58l435kg10whg4a8b3dmava974z8xg6zqx7y0si") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.6.0 (c (n "mers_lib") (v "0.6.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "0l098ln7xg91hz2i8s7h37zdcgh8rvbpdwkls3m1rns5g2daxym7") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.6.1 (c (n "mers_lib") (v "0.6.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "1dwx4ryg210h7bgrxbkq2dy5r95x3xyzhxcmd94a96p61wsffhvv") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.7.0 (c (n "mers_lib") (v "0.7.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "0983d9mkmmwav6fal304bch0fvg6mgmsswhdhk2brz8vvkg7qj1d") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.7.1 (c (n "mers_lib") (v "0.7.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "0gkc7p8l1hkz28rh0p7m1nffn4h84nk39mll1jc3rybqlgznmjqn") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.7.2 (c (n "mers_lib") (v "0.7.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "1jgi4kwvk61zy76nl0i2lwgagn8s372z5p38dh3g5qsz4xw4cklg") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.7.3 (c (n "mers_lib") (v "0.7.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "0hcmqi7dszqifd8aa4ddsv465x6c41y9iv3q5aqkyf6lcj5xk6dx") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.8.0 (c (n "mers_lib") (v "0.8.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "07ihhnh9766y3xxnjqhii7wwzn0hfgf826kl2kl0cn9r9fg3igq1") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.8.1 (c (n "mers_lib") (v "0.8.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "1rgzwy2xjvh10xamh4zxk7i75rq27nx0p35cb2sz6y9mlf0jixm2") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.8.2 (c (n "mers_lib") (v "0.8.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "0siap9v71b2cqqxnw2a9j3gykd42d13f6s5vvcdgz00dabyb7dij") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.8.3 (c (n "mers_lib") (v "0.8.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "line-span") (r "^0.1.5") (d #t) (k 0)))) (h "1frvwwns5fgcxxw4x9n601v5sv63svgpl9hkrf1vp536fi8978kn") (f (quote (("run") ("parse" "run") ("default" "parse"))))))

