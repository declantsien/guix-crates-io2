(define-module (crates-io me rs mersenne-prime-number-miller-rabin) #:use-module (crates-io))

(define-public crate-mersenne-prime-number-miller-rabin-0.1.0 (c (n "mersenne-prime-number-miller-rabin") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vnfy5m535kfmk4srii06gkkjbh6q61qhq19jlzlvdgpiaf392kr")))

