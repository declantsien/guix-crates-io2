(define-module (crates-io me rs mersenne-twister-m) #:use-module (crates-io))

(define-public crate-mersenne-twister-m-0.1.0 (c (n "mersenne-twister-m") (v "0.1.0") (h "0wax48a1zapl3m2hgq00h67hzhpxh0zlbc9igr41bp9bb3nzwym1")))

(define-public crate-mersenne-twister-m-0.1.1 (c (n "mersenne-twister-m") (v "0.1.1") (h "1i1wxnrd9fl6cnrngphbmalp61vfbc4qvy9fr05w7qm0j7ln0g29")))

(define-public crate-mersenne-twister-m-0.2.0 (c (n "mersenne-twister-m") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)))) (h "0xdlzbvb2s04ibrwvzpjr2ix5jxs39spjfckx6cahkf9jg01dxyz")))

(define-public crate-mersenne-twister-m-0.3.0 (c (n "mersenne-twister-m") (v "0.3.0") (d (list (d (n "rand_core") (r "^0.6.4") (o #t) (d #t) (k 0)))) (h "19v11xq877nvia6xzv68h4kjjmqdpjssb32g4k4pkakr47b4d123") (s 2) (e (quote (("rand" "dep:rand_core"))))))

