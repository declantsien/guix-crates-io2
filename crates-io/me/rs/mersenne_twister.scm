(define-module (crates-io me rs mersenne_twister) #:use-module (crates-io))

(define-public crate-mersenne_twister-0.1.0 (c (n "mersenne_twister") (v "0.1.0") (h "06zqf717gsw48niscba8gimqb92wa9zpl39zqzaizsh2x2nyq8ic")))

(define-public crate-mersenne_twister-0.1.1 (c (n "mersenne_twister") (v "0.1.1") (h "0lyf2p1k3nard2aqsl29myfhjbcx5qflardp4ykkjiqjymjcwdli")))

(define-public crate-mersenne_twister-0.1.2 (c (n "mersenne_twister") (v "0.1.2") (h "1ij3071ykxl5vbzd257i5q3211l5819xh5yzpcpka0z49c9c2vfr")))

(define-public crate-mersenne_twister-0.1.3 (c (n "mersenne_twister") (v "0.1.3") (h "0g4g2314bfc3ngh0vh2q53z5dpknbimyi9wa3hb8vd61xd21wds9")))

(define-public crate-mersenne_twister-0.1.4 (c (n "mersenne_twister") (v "0.1.4") (h "0fbhzl90f0cydq7zacxd5hacbr24mil8k2p2hcm0ljm09bzpfisf")))

(define-public crate-mersenne_twister-0.1.5 (c (n "mersenne_twister") (v "0.1.5") (h "133wvk12d9zav4803akp7gmbi9r065ahrbz4dhcx1ml25sq5v53f")))

(define-public crate-mersenne_twister-0.2.0 (c (n "mersenne_twister") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "168slr58qpw8dr54p3n17lqcbck22xn1b7c6dcpd72f26c4804h0")))

(define-public crate-mersenne_twister-0.3.0 (c (n "mersenne_twister") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mfy6l96n3i5c15b60fxmcnj0ydbzgcmpmjz9rp4h9153v27nbb4")))

(define-public crate-mersenne_twister-1.0.0 (c (n "mersenne_twister") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1g83n706f381m9mrkgl81fnmzjybg0pglzp1y1r6mv80j1qql8is")))

(define-public crate-mersenne_twister-1.1.0 (c (n "mersenne_twister") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "135b3r50p6iiaxzixpz3n9ibqnbl5gczgxsjrfcvii69b5xpmhsk")))

(define-public crate-mersenne_twister-1.1.1 (c (n "mersenne_twister") (v "1.1.1") (d (list (d (n "rand") (r ">= 0.3, < 0.5") (d #t) (k 0)))) (h "1c0w7l2b7yi5nfis3fg88pp10sw9g5jikiglr2m2dh6zd0pvnpdq")))

