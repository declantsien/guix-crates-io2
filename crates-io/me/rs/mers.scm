(define-module (crates-io me rs mers) #:use-module (crates-io))

(define-public crate-mers-0.3.1 (c (n "mers") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mers_lib") (r "^0.3.1") (d #t) (k 0)))) (h "12bb11ypszhdkq3yj065555gk4f37npzr1f1h3cc8iyrcywmd0p6")))

(define-public crate-mers-0.3.2 (c (n "mers") (v "0.3.2") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mers_lib") (r "^0.3.2") (d #t) (k 0)))) (h "14njx3phyifv55zqj4bn2zpzm8hgc8xb1fnjigy7ap29655612wd")))

(define-public crate-mers-0.3.3 (c (n "mers") (v "0.3.3") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mers_lib") (r "^0.3.3") (d #t) (k 0)))) (h "1317xncjf4m2dbhycsp4v9zapwfgy0blx95bdnhi222mvqlcasri")))

(define-public crate-mers-0.3.4 (c (n "mers") (v "0.3.4") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mers_lib") (r "^0.3.4") (d #t) (k 0)))) (h "12y3357j0pjia6yf298l0jj30w6j7xc1nm409dbb9arvcknajsk1")))

(define-public crate-mers-0.3.5 (c (n "mers") (v "0.3.5") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mers_lib") (r "^0.3.4") (d #t) (k 0)))) (h "1gi1lqk8618934kmknbv76isa58sdj92jrnji1v2nw6qwnjwzz7b")))

(define-public crate-mers-0.3.6 (c (n "mers") (v "0.3.6") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mers_lib") (r "^0.3.5") (d #t) (k 0)))) (h "0gwjlrf0z13mb14vg558mj584sr1j2jxmx9w852cshksd1ngydl0")))

(define-public crate-mers-0.3.7 (c (n "mers") (v "0.3.7") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mers_lib") (r "^0.3.7") (d #t) (k 0)))) (h "0qqzcxh9dxlsbbgzdfd9amszbv2s74afs8ip0k58w9hsnsiws8sw")))

(define-public crate-mers-0.4.0 (c (n "mers") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mers_lib") (r "^0.4.0") (d #t) (k 0)))) (h "01xivb2ky8f5p80yhljvs7ar6xvw08911cl1lilkxar36ad0499b")))

(define-public crate-mers-0.5.0 (c (n "mers") (v "0.5.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mers_lib") (r "^0.5.0") (d #t) (k 0)))) (h "1v8c2j5y2hw5wqli22ny1sn0gvrxaf055dnirh12ffxdl8n04cgj")))

(define-public crate-mers-0.6.0 (c (n "mers") (v "0.6.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "mers_lib") (r "^0.6.0") (d #t) (k 0)))) (h "1nyiqmh3db28x4c8gvv8k7dbxwjbghnl0rc8jmyx4ll0anzcpda4")))

(define-public crate-mers-0.7.0 (c (n "mers") (v "0.7.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "mers_lib") (r "^0.7.0") (d #t) (k 0)))) (h "0fyfr137rsny1x25rdas8qjrmdngilzyn4d9z5vjnd0y5dprlm9d")))

(define-public crate-mers-0.7.3 (c (n "mers") (v "0.7.3") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "mers_lib") (r "^0.7.0") (d #t) (k 0)))) (h "1a9zmjf0ag9hypimqml78k24kyd7m5aka7zrd55knij1qma6018j")))

(define-public crate-mers-0.8.0 (c (n "mers") (v "0.8.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "mers_lib") (r "^0.8.0") (d #t) (k 0)))) (h "0ck2d7mg5vg4h4ms21c7dill2x89y9gr69646iynxx7kmbaa0zn4")))

(define-public crate-mers-0.8.1 (c (n "mers") (v "0.8.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "mers_lib") (r "^0.8.1") (d #t) (k 0)))) (h "0vpg4cpmhw9ihs87hr429vvrdfckbhnq7lrysh0l8cnl952c222d")))

(define-public crate-mers-0.8.2 (c (n "mers") (v "0.8.2") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "mers_lib") (r "^0.8.2") (d #t) (k 0)))) (h "17sydhq00645jl3xl4zlfdlfksnkcpvjqxa1j0k4gyaf1qvpjrgg")))

(define-public crate-mers-0.8.3 (c (n "mers") (v "0.8.3") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "mers_lib") (r "^0.8.3") (d #t) (k 0)))) (h "0ly21b8i0fb5gg9psxqdmrkkbhnx71sxc5ajqwwdibx4fbfv82rd")))

