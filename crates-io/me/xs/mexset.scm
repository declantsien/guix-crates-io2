(define-module (crates-io me xs mexset) #:use-module (crates-io))

(define-public crate-mexset-0.1.0 (c (n "mexset") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "03xn80wb34z79mwnn9gkgph47j3gr450vygfng25ysy66s67w70q")))

