(define-module (crates-io me qu mequeue) #:use-module (crates-io))

(define-public crate-mequeue-0.0.0 (c (n "mequeue") (v "0.0.0") (h "0pp27acm6qf3s2nynlz51pz366fy1mrm1v30lnln5qynab2prbqj")))

(define-public crate-mequeue-0.0.1 (c (n "mequeue") (v "0.0.1") (d (list (d (n "async-channel") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync"))) (d #t) (k 0)))) (h "0fci519wp989c11r51ysya8sdkrp120kkj4d3rnpb6b5xnaggfzb")))

