(define-module (crates-io me du medusa-proto) #:use-module (crates-io))

(define-public crate-medusa-proto-0.5.0 (c (n "medusa-proto") (v "0.5.0") (d (list (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0b23xpbk140kmn937fi2dywbsxhrhjqfmyfaxcgyq1v9scypgcr1") (y #t)))

(define-public crate-medusa-proto-0.6.0 (c (n "medusa-proto") (v "0.6.0") (d (list (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "10pwaris9z8nw1vfxaf726yh63qrkvsn0i9k9cpln7cc2msjcpah") (y #t)))

(define-public crate-medusa-proto-0.7.0 (c (n "medusa-proto") (v "0.7.0") (d (list (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "18d6b2ql2c8krn7iml41p6fkqyvqya2nk58m2fk8pp4ygpraw3nq") (y #t)))

(define-public crate-medusa-proto-0.8.0 (c (n "medusa-proto") (v "0.8.0") (d (list (d (n "bytes") (r "^1.2.1") (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1qrg95gd439ip22n16v0d44g1rhwx9s69vay6sc3zss8dz3k25q0") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-medusa-proto-0.9.0 (c (n "medusa-proto") (v "0.9.0") (d (list (d (n "bytes") (r "^1.2.1") (k 0)))) (h "07v3dik95py6483z2b7kgf2qs99wsww47sd47mbx0pdkzvkb9cqa") (f (quote (("nightly") ("default")))) (y #t)))

