(define-module (crates-io me du medusa) #:use-module (crates-io))

(define-public crate-medusa-0.1.0 (c (n "medusa") (v "0.1.0") (h "0rjg1f71n1vc7l2bgp0aaqkcpsvpggqm4f0bkqwxia7zz4dma6yh")))

(define-public crate-medusa-0.2.0 (c (n "medusa") (v "0.2.0") (h "147akb4xj8kzrzxl8x989yfncx3vpzrg97zqwk8vk47gd8la8jvc")))

(define-public crate-medusa-0.2.1 (c (n "medusa") (v "0.2.1") (h "12fv6bqdk508gxm1x9bgm7s1z9i4d58qnjz1m7km312kndvp5iln")))

(define-public crate-medusa-0.2.2 (c (n "medusa") (v "0.2.2") (h "0xf4yp7501llb2sw3rr0fi1hwlzg0wxxdcfnb2c7cmj2xir2mv7f")))

(define-public crate-medusa-0.2.3 (c (n "medusa") (v "0.2.3") (h "1345sg3wzzsm9pyfmncq8p82kvgmmifyhmr0f32a25xwjhsl8v2y")))

(define-public crate-medusa-0.3.0 (c (n "medusa") (v "0.3.0") (h "061rkq07q8q09vs834l8k9rqzdyxrdibmnkx2msrq2ky1zli97fq")))

