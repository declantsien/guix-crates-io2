(define-module (crates-io me il meilies-client) #:use-module (crates-io))

(define-public crate-meilies-client-0.1.0 (c (n "meilies-client") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "meilies") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.16") (d #t) (k 0)) (d (n "tokio-retry") (r "^0.2.0") (d #t) (k 0)))) (h "07i20nm6nqd9b2w60h1r34hmjlb33id809nb514bdpi4varqdnin")))

(define-public crate-meilies-client-0.1.1 (c (n "meilies-client") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "meilies") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.16") (d #t) (k 0)) (d (n "tokio-retry") (r "^0.2.0") (d #t) (k 0)))) (h "0h5b3h36bkpik4vww2qyv8wcff7gb78jhgkjipnvdyp9cqgfwk4q")))

(define-public crate-meilies-client-0.2.0 (c (n "meilies-client") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "meilies") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.19") (d #t) (k 0)) (d (n "tokio-retry") (r "^0.2.0") (d #t) (k 0)))) (h "0xmm26m11s6zim6c6kjdcdq3q7rvcsc6xmh95d44w03s37vimssm")))

