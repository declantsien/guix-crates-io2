(define-module (crates-io me il meilimelo-macros) #:use-module (crates-io))

(define-public crate-meilimelo-macros-0.1.0 (c (n "meilimelo-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r37fbpdfd63xrs7my0k8hnlr53a63y8jyl4icvbyn7gylhcnxfc")))

