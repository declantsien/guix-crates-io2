(define-module (crates-io me il meilisearch-index-setting-macro) #:use-module (crates-io))

(define-public crate-meilisearch-index-setting-macro-0.1.0 (c (n "meilisearch-index-setting-macro") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "meilisearch-sdk") (r "^0.20.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hfdcxrhgiv0cqss56rw3cjg8bg7wh87789mqvzd55h8q59a52gf")))

(define-public crate-meilisearch-index-setting-macro-0.22.0 (c (n "meilisearch-index-setting-macro") (v "0.22.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0w5x1p9gqja7q0zvwkhbh31kg90qj4fs41i7sdvpn3lrr3z7n01n")))

(define-public crate-meilisearch-index-setting-macro-0.22.1 (c (n "meilisearch-index-setting-macro") (v "0.22.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1nh8yica5hi61nql3j47xb4pzsj8svv7cgczcy8wryxb5xzdvzn9")))

(define-public crate-meilisearch-index-setting-macro-0.23.0 (c (n "meilisearch-index-setting-macro") (v "0.23.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "109l79bgpdgm9fs1ddwdjswxgax6l3pfxmcgvjdnsizcg4d5qakq")))

(define-public crate-meilisearch-index-setting-macro-0.23.1 (c (n "meilisearch-index-setting-macro") (v "0.23.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1f94y21l34q8s8mh2b11hg2jdahzx5lbsfh20yckc7ik1j1dfdvz")))

(define-public crate-meilisearch-index-setting-macro-0.23.2 (c (n "meilisearch-index-setting-macro") (v "0.23.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dk50d9zx3hgs2k13dlgwsiwg0hdsavlpmbhddiwih341mms9x92")))

(define-public crate-meilisearch-index-setting-macro-0.24.0 (c (n "meilisearch-index-setting-macro") (v "0.24.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xs2h2w4fnrjzmjgg607hc31swj62w9ah3vnxdzwa5nh8qgi2m8m")))

(define-public crate-meilisearch-index-setting-macro-0.24.1 (c (n "meilisearch-index-setting-macro") (v "0.24.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0amfnz4i0jkgx121r27wanmhy5gpi37hkg33cpynqrrh00wbffwj")))

(define-public crate-meilisearch-index-setting-macro-0.24.2 (c (n "meilisearch-index-setting-macro") (v "0.24.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0smypfjzfnnq53nfwp8axd8f2as9gr9frs4l1s1gj80piiv1kq5n")))

(define-public crate-meilisearch-index-setting-macro-0.24.3 (c (n "meilisearch-index-setting-macro") (v "0.24.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0g61hskfy0gl6llzghmx7hrm2jl39r7qaa4bl3k2ijxram5i5wmi")))

(define-public crate-meilisearch-index-setting-macro-0.25.0 (c (n "meilisearch-index-setting-macro") (v "0.25.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "structmeta") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "193klvy6r45ad49087m2gqpdycna7c4rmjglbd0gr35yx2jl13j9")))

(define-public crate-meilisearch-index-setting-macro-0.26.0 (c (n "meilisearch-index-setting-macro") (v "0.26.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "structmeta") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11843i9b9vvvmwbyhdflzb51g7c6saalarnmc20w7m8by0g4nzzv")))

(define-public crate-meilisearch-index-setting-macro-0.26.1 (c (n "meilisearch-index-setting-macro") (v "0.26.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "structmeta") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0h6kxbg33psd2yk5v8ilaygv5hj8sjqq7vqv08fdfs4krjzrqzsi")))

