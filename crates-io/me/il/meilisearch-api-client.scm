(define-module (crates-io me il meilisearch-api-client) #:use-module (crates-io))

(define-public crate-meilisearch-api-client-0.0.1 (c (n "meilisearch-api-client") (v "0.0.1") (d (list (d (n "actix-web") (r "^3.0.0-alpha.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)) (d (n "actix-rt") (r "^1") (d #t) (k 2)))) (h "0jh066jqqvshzjwpz6mkjr286zfp8vixdhk6n935sid8070h471w")))

