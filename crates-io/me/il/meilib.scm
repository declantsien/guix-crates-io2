(define-module (crates-io me il meilib) #:use-module (crates-io))

(define-public crate-meilib-0.1.0 (c (n "meilib") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)))) (h "17bh9kbrz103x04j73qxiazif9lzk15lgnvaj3gjkr0ggq384hq3")))

(define-public crate-meilib-0.1.1 (c (n "meilib") (v "0.1.1") (d (list (d (n "actix-rt") (r "^1") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.0-alpha.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)))) (h "08wwxhj5yhxrq472z4qzjz52jq71bb8100v1zc2q5xdwypn33lwf")))

(define-public crate-meilib-0.1.2 (c (n "meilib") (v "0.1.2") (d (list (d (n "actix-rt") (r "^1") (d #t) (k 2)) (d (n "actix-web") (r "^3.0.0-alpha.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.50") (d #t) (k 0)))) (h "1417da7zgb33rcp6skwn1c5n8wwlkvy4xz0qgbfy6n9678qbpmcv")))

