(define-module (crates-io me il meilies) #:use-module (crates-io))

(define-public crate-meilies-0.1.0 (c (n "meilies") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 0)) (d (n "subslice") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.16") (d #t) (k 0)))) (h "0gzjhykqg0xsngk1cvr20smmlwp2rjgnqvkd7mpnshbp4n1qlg51")))

(define-public crate-meilies-0.2.0 (c (n "meilies") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "subslice") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.19") (d #t) (k 0)))) (h "0gcmdxzb9373grdygv4whk8700p8cjph94wy1zhlk994bfwkyikc")))

