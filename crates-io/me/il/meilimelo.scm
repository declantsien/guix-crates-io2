(define-module (crates-io me il meilimelo) #:use-module (crates-io))

(define-public crate-meilimelo-0.1.0 (c (n "meilimelo") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "meilimelo-macros") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "05v5z7qllsvaf7v5fak1drnc3mwafnk3i4prdiqq7xky90dkhlpf")))

