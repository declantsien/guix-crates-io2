(define-module (crates-io me mx memx-cdy) #:use-module (crates-io))

(define-public crate-memx-cdy-0.1.0 (c (n "memx-cdy") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "126fnnb2l64hpkp8b9hwdl9imc32cy5qrzjpqh941m5awbzixdsx") (f (quote (("default") ("debian_build"))))))

(define-public crate-memx-cdy-0.1.1 (c (n "memx-cdy") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "0wni3m603j8c9mwjk6x58mp8zw1n2zfpcfbs2aqzzprwmrj349bx") (f (quote (("default") ("debian_build"))))))

(define-public crate-memx-cdy-0.1.2 (c (n "memx-cdy") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "0xicx794v1wcia03754n9xxl0z7b6wdh7lq5jbsrzc68wbw91s43") (f (quote (("default") ("debian_build"))))))

(define-public crate-memx-cdy-0.1.3 (c (n "memx-cdy") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "17q929rkvr99qc2zpi7h7a72ywv136d43s4m8csknxh9vlv4rxq5") (f (quote (("default") ("debian_build"))))))

(define-public crate-memx-cdy-0.1.4 (c (n "memx-cdy") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "0bpisfzb669a1slvsz8vaccrz25rmf6sqgik9xw0xybikmy9y8cd") (f (quote (("default") ("debian_build"))))))

(define-public crate-memx-cdy-0.1.5 (c (n "memx-cdy") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "08pganb5h9zsvdply1hyrlpsr2iar7jrdrivd1z7fzs6076hp61m") (f (quote (("default") ("debian_build")))) (y #t)))

(define-public crate-memx-cdy-0.1.6 (c (n "memx-cdy") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "1l64280nwhhmr1r8nyr5mkg43ly1nqgy4wj5z5bcmzl42cq1qjjj") (f (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build"))))))

(define-public crate-memx-cdy-0.1.7 (c (n "memx-cdy") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "0jc9ra2a10zfzq1hax2xczrk5jsf64mwa4lghxh7d1i4kjd3ipg3") (f (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build"))))))

(define-public crate-memx-cdy-0.1.8 (c (n "memx-cdy") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "1g9p1ymg4njcn63cj0jf8krsxqr7izkq6i6whssg798ankgf7bdm") (f (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build"))))))

(define-public crate-memx-cdy-0.1.9 (c (n "memx-cdy") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "0gna4v7gpxy2qxxb8qjdn2rml0mx4h5sf7wp6m6nc546h6pmcyj2") (f (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build"))))))

(define-public crate-memx-cdy-0.1.10 (c (n "memx-cdy") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "0sj8jl7f62ll89jlxca1mx7gsfhklqf3jx9qli3rryig0za36czd") (f (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build"))))))

(define-public crate-memx-cdy-0.1.11 (c (n "memx-cdy") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "07rk7sanip6nlyg6z8yaadi6140gp51nf3fp9wq8vlajzvsd1i50") (f (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build")))) (r "1.56.0")))

(define-public crate-memx-cdy-0.1.12 (c (n "memx-cdy") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "0cg2bx35f4vvzr4m9mwxd579ja212q0js9xlw50d1a1r2pqslghw") (f (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build")))) (r "1.56.0")))

(define-public crate-memx-cdy-0.1.13 (c (n "memx-cdy") (v "0.1.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memx") (r "^0.1") (d #t) (k 0)))) (h "1gfy18m97br7896py04y2548dmaiyl7rj4xk592r0xkc6cq8lp21") (f (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build")))) (r "1.56.0")))

