(define-module (crates-io me mx memx) #:use-module (crates-io))

(define-public crate-memx-0.1.0 (c (n "memx") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "03pmcrsdl3rfxx647wjvhriapbpv4vhjvx31rlqhq5xbhsvk6nny") (f (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1.1 (c (n "memx") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "08vy45bjvadrgkpza8j2mmy2s400868ldi2bqffby6qqlp9r5727") (f (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1.2 (c (n "memx") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 2)))) (h "0i4kcz0ss1xmcgc5lgkw1ay003ywlzblvpa7g2h8afwax7zii1aa") (f (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1.3 (c (n "memx") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 2)))) (h "1yhi3ry7qllgdm6j8jyw7vkv2l4yk8wsy7z6kqld527yps0h4bvh") (f (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1.4 (c (n "memx") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 2)))) (h "1sx2f9n82fypsz5kaqh57i40kcw1mbs40pcrv62zziyfmxqjsiza") (f (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1.5 (c (n "memx") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 2)))) (h "1k87fanka48rb9smmkjqqiws46028pa1nghlg2q99c6gkf4zqnpa") (f (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1.6 (c (n "memx") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 2)))) (h "0x7f61pg2h1ms2dl7v3qq6d65z7phfm77hzan2ikzxvm5m4jshmb") (f (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1.7 (c (n "memx") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 2)))) (h "0j8fwyj7dzm4hnahzbx84i90l8m7j7gyq0fsq4i3c7ddi50rc7di") (f (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1.8 (c (n "memx") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 2)))) (h "1kygjccx0ffhby8x7chp5hws4nshwj6f4msmf47r84fsz4j5ynwk") (f (quote (("std") ("default" "std") ("cmp_bytes"))))))

(define-public crate-memx-0.1.9 (c (n "memx") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 2)))) (h "0k3xclwx6bsm3ffk3wrssi401qq43p60v6m6fbhj39q9dbl8q5al") (f (quote (("std") ("default" "std") ("cmp_bytes"))))))

(define-public crate-memx-0.1.10 (c (n "memx") (v "0.1.10") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.4") (d #t) (k 2)))) (h "1v7wvv4ngsxs4pxqhv2vvljqiv9j3awv4ryn93kmjbf3l5qxh117") (f (quote (("std") ("default" "std") ("cmp_bytes"))))))

(define-public crate-memx-0.1.12 (c (n "memx") (v "0.1.12") (h "1iqz82j3lmwapbnc9fvgyq7j8kp4vrisqvq8zayml13i0vl5nn60") (f (quote (("std") ("default" "std") ("cmp_bytes"))))))

(define-public crate-memx-0.1.13 (c (n "memx") (v "0.1.13") (h "0cqag11809k13m733bzbb757x5qqyaw4xm7la254bcxpdn9mqzxm") (f (quote (("std") ("default" "std") ("cmp_bytes"))))))

(define-public crate-memx-0.1.14 (c (n "memx") (v "0.1.14") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "0mf3kw6l2ygiapxv6q6787vs7k2znjryscfm8nv2x2aba0ilzsns") (f (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1.15 (c (n "memx") (v "0.1.15") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "17s2mk3y08ai2gc4i8fy4yha4qfai39a2a5j3j3j4rzhw4y3131x") (f (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1.16 (c (n "memx") (v "0.1.16") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "1vw7hvfvnhk15yllj8226mpgx4pa41cqxfbk0i561h4mfss80g75") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-memx-0.1.17 (c (n "memx") (v "0.1.17") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "0nlwcqypnhm39cln4y361lpnpv6qdhs7nhmnnh3i2gzg9w7p4b0c") (f (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1.18 (c (n "memx") (v "0.1.18") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "06p0597sra6jzwvjfnjphd9p5yirlrp2mfs9szr1h3mia74gwawh") (f (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1.19 (c (n "memx") (v "0.1.19") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "1ky023qgvyd7pw9dpdb6c9rk7idamvm07rnq4xaidzafq0j59hg3") (f (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1.20 (c (n "memx") (v "0.1.20") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "07v0r577idb3qyd2d5hyzp8yprvsk09fi4vzlb1lnkikwx7jad6q") (f (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1.21 (c (n "memx") (v "0.1.21") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "0y79ns080jyjy9a44fiy1qmamd01cbsxzk5prrdik641gqhxyr3l") (f (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1.22 (c (n "memx") (v "0.1.22") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "0rhg560mvdayn6hba26z0wqm2i2cfv3lbmc542vlwyj0gqpqrpcs") (f (quote (("default"))))))

(define-public crate-memx-0.1.23 (c (n "memx") (v "0.1.23") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "1aigb2n7a4ks9c38b5s86cigb0266xvk97hibdrvwy9a7rqn8nmp") (f (quote (("default"))))))

(define-public crate-memx-0.1.24 (c (n "memx") (v "0.1.24") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "001pqv4yypj8jyk5ziwgk7ibchksvznprlc4f02nrc37bkriycfd") (f (quote (("default")))) (r "1.56.0")))

(define-public crate-memx-0.1.25 (c (n "memx") (v "0.1.25") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "1yp2gv2b7x2hza6p1pq0i9gfv1gxm8psiazbm7a7p91cfxqa4lqq") (f (quote (("default")))) (r "1.56.0")))

(define-public crate-memx-0.1.26 (c (n "memx") (v "0.1.26") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "0lda79p1zwzxw6hdc1ad9dcgf6200ypkm7xacsd3083kpzndy744") (f (quote (("default")))) (r "1.56.0")))

(define-public crate-memx-0.1.27 (c (n "memx") (v "0.1.27") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "0c577h7n1isii2djf1f0g0s368grjql5qkyql3ifyip5dzfs96bf") (f (quote (("test_pointer_width_64") ("test_pointer_width_32") ("test_pointer_width_128") ("default")))) (r "1.56.0")))

(define-public crate-memx-0.1.28 (c (n "memx") (v "0.1.28") (d (list (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)))) (h "0n1j8mf6hhjhf0dd3k6rkgbdyg6waqfr6m4rimbykp6ad6052sbp") (f (quote (("test_pointer_width_64") ("test_pointer_width_32") ("test_pointer_width_128") ("default")))) (r "1.56.0")))

(define-public crate-memx-0.1.29 (c (n "memx") (v "0.1.29") (d (list (d (n "cfg-iif") (r "^0.2") (d #t) (k 2)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "x86-alignment-check") (r "^0.1") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 2)))) (h "1pnwjndsbqsqgc1r8dssn4kp6jikkmihs1zyqdsknnpr1c69lhjz") (f (quote (("test_pointer_width_64" "test_pointer_width") ("test_pointer_width_32" "test_pointer_width") ("test_pointer_width_128" "test_pointer_width") ("test_pointer_width" "test") ("test_alignment_check" "test") ("test") ("default")))) (r "1.56.0")))

(define-public crate-memx-0.1.30 (c (n "memx") (v "0.1.30") (d (list (d (n "cfg-iif") (r "^0.2") (d #t) (k 2)) (d (n "cpufeatures") (r "^0.2") (d #t) (t "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "x86-alignment-check") (r "^0.1") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 2)))) (h "1j1h3q3025vbqcna9n4yw5h3v153k4xf2yg0pbjdxpmfl8pmjjs9") (f (quote (("test_pointer_width_64" "test_pointer_width") ("test_pointer_width_32" "test_pointer_width") ("test_pointer_width_128" "test_pointer_width") ("test_pointer_width" "test") ("test_alignment_check" "test") ("test") ("default")))) (r "1.56.0")))

