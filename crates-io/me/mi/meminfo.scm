(define-module (crates-io me mi meminfo) #:use-module (crates-io))

(define-public crate-meminfo-0.1.0 (c (n "meminfo") (v "0.1.0") (h "12jfr8gi1qb64838qwl4y9l6dvxs7cid3f8105bf5qby91sy2qmf")))

(define-public crate-meminfo-0.1.1 (c (n "meminfo") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qkgrbdyj8nfbwvpf3h9n9fibvrjjdg0hv3c57mvylh322b9lhkh") (f (quote (("default"))))))

