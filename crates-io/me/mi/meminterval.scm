(define-module (crates-io me mi meminterval) #:use-module (crates-io))

(define-public crate-meminterval-0.1.0 (c (n "meminterval") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "0syjnk4871q0qqdw4aj0y7dvm90i36ldzmzm8zpw7xp0a5grzljb")))

(define-public crate-meminterval-0.2.0 (c (n "meminterval") (v "0.2.0") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "1jjbz7i2vp7b7dk6h8qj2ifdr308992w7fxf40qp0yb4gnk81pzf")))

(define-public crate-meminterval-0.3.0 (c (n "meminterval") (v "0.3.0") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "09i6w7mbrhks6fvwix8gngcxs80nd5sx76hnk3z4zng2mi4ps7jz")))

(define-public crate-meminterval-0.4.0 (c (n "meminterval") (v "0.4.0") (d (list (d (n "num") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "1bw0x0sj205gncjfwdid7fjqgirnm5vvzfkd26fklciam321pbyp") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-meminterval-0.4.1 (c (n "meminterval") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "0j7jkd4sqn3gigydrh6yzlilj4sgq0qd6f113jz53ljmz1663y66") (s 2) (e (quote (("serde" "dep:serde"))))))

