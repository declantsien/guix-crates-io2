(define-module (crates-io me lp melpow) #:use-module (crates-io))

(define-public crate-melpow-0.1.0 (c (n "melpow") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (f (quote ("const_generics" "union"))) (d #t) (k 0)) (d (n "tmelcrypt") (r "^0.2.4") (d #t) (k 0)))) (h "1cjg2qc84x07b3pyjmzampdgpgf3s84y33cp27r9gg2fwyklbigy") (y #t)))

(define-public crate-melpow-0.1.1 (c (n "melpow") (v "0.1.1") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (f (quote ("const_generics" "union"))) (d #t) (k 0)) (d (n "tmelcrypt") (r "^0.2.4") (d #t) (k 0)))) (h "1xhr1z5vdchhiaif77vcixf4ivl14jhv5bpmlnbj3cq416mgp844")))

