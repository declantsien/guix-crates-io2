(define-module (crates-io me zm mezmo) #:use-module (crates-io))

(define-public crate-mezmo-0.1.0 (c (n "mezmo") (v "0.1.0") (d (list (d (n "gethostname") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1waaad3jjjn0454ancxfkxvhidrjp8n7jga8kn40a24k5y6zwr52")))

