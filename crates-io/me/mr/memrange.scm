(define-module (crates-io me mr memrange) #:use-module (crates-io))

(define-public crate-memrange-0.1.0 (c (n "memrange") (v "0.1.0") (h "0raxylxhx0ikz9gm2qsjqcg9z0gf3r69h7601h37b76417rvqvx6")))

(define-public crate-memrange-0.1.1 (c (n "memrange") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0jsw6lz7gyp1j6rc7agi5jz1njigqwph5lx2f0h92p20pnffplfq")))

(define-public crate-memrange-0.1.2 (c (n "memrange") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0gr7sk6pa1k3z0bzkpvdkw9q7iig8b1mspizsplv46n5bljnnvzk")))

(define-public crate-memrange-0.1.3 (c (n "memrange") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)))) (h "0c35jy29jml3r3p4dfr5palc2dszjb9ircrc4pf4zp4fi5jvlafc")))

