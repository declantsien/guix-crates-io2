(define-module (crates-io me mr memreader) #:use-module (crates-io))

(define-public crate-memreader-0.1.0 (c (n "memreader") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0nfsm8n5qlifslr66bgy50cngpaiby9311m9n57vfbqp1cbp0bfg")))

(define-public crate-memreader-0.1.1 (c (n "memreader") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1svafgvszzsk4ddx2221g25b6dn0cciqd13sjvkqj10kmx4vqg6d")))

