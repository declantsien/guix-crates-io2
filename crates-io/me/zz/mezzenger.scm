(define-module (crates-io me zz mezzenger) #:use-module (crates-io))

(define-public crate-mezzenger-0.1.0 (c (n "mezzenger") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0lpxhkx82w6rm6b1iq35gidv330bg51sqkahw28c922l7q2m1zgh")))

(define-public crate-mezzenger-0.1.1 (c (n "mezzenger") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0bfsjcfpa5hngah9i0kw39n4jm32ja899l6999gl0vc6yy1z42pz") (y #t)))

(define-public crate-mezzenger-0.1.2 (c (n "mezzenger") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0dsby9il4d1494ial0ih0y63mb4flyv4n4szff760kyf90p6c0dv")))

(define-public crate-mezzenger-0.1.3 (c (n "mezzenger") (v "0.1.3") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)))) (h "0c1n273w6q5my1mw7bqjdx6p7f71dmw0s1cf0ip1zcn2ag78lfjn")))

(define-public crate-mezzenger-0.1.4 (c (n "mezzenger") (v "0.1.4") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "0lykqj619n1p3aijliskvn9z58y3mra8m1cipqnck1irr92yb1xj")))

