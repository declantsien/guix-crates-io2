(define-module (crates-io me zz mezzenger-channel) #:use-module (crates-io))

(define-public crate-mezzenger-channel-0.1.0 (c (n "mezzenger-channel") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "mezzenger") (r "^0.1.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0yllx7vl14ay1kyasg456vdb06bnmf6nnmblvsh6zaylxa1y635i")))

(define-public crate-mezzenger-channel-0.1.1 (c (n "mezzenger-channel") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "mezzenger") (r "^0.1.4") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1xw81lapxp6waxji02qngn2z0p9vpjflah5rrnk5r9cnqkil2i3s")))

