(define-module (crates-io me rm mermaid) #:use-module (crates-io))

(define-public crate-mermaid-0.1.0 (c (n "mermaid") (v "0.1.0") (h "0sdwbbjs6ca3n352zyjm9yz05dhpwywa1yqiv4ag6q3dwdl12rl0")))

(define-public crate-mermaid-0.2.0 (c (n "mermaid") (v "0.2.0") (h "00glszz4ls49zl38l19x9hj59yji2kdgvwnjdig9xm40a8i2d2dh")))

