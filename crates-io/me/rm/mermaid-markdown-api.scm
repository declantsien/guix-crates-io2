(define-module (crates-io me rm mermaid-markdown-api) #:use-module (crates-io))

(define-public crate-mermaid-markdown-api-0.1.0 (c (n "mermaid-markdown-api") (v "0.1.0") (d (list (d (n "enum-as-inner") (r "^0.4.0") (d #t) (k 0)) (d (n "scanner-syn") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1cyiink7akfmxg79xh7nbv3q5kdl9xn88dzzc7za3crqj4wx2yr5")))

(define-public crate-mermaid-markdown-api-0.2.0 (c (n "mermaid-markdown-api") (v "0.2.0") (d (list (d (n "enum-as-inner") (r "^0.4.0") (d #t) (k 0)) (d (n "scanner-syn") (r "^0.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1k5a0rhr25a2w5sxxf8wsc0l9d8h9dn94bkh5ikd1i16lnkhh9pk")))

