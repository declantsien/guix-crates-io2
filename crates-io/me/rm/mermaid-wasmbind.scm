(define-module (crates-io me rm mermaid-wasmbind) #:use-module (crates-io))

(define-public crate-mermaid-wasmbind-0.1.0 (c (n "mermaid-wasmbind") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.0") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Element"))) (d #t) (k 0)))) (h "0c9bsyrc3gwzgd1grbrsjz0a8ngawx7jyn2b6p5czp1rxgpl837w")))

