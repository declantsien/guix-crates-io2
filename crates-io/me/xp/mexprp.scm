(define-module (crates-io me xp mexprp) #:use-module (crates-io))

(define-public crate-mexprp-0.1.0 (c (n "mexprp") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.1") (d #t) (k 2)))) (h "14w9b11xazqx5c748r0dhc75m3m8n99v4hfbhbm1zbkd39vi3p0w")))

(define-public crate-mexprp-0.2.0 (c (n "mexprp") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "14nrsxg5p87x8qi04x68gwypd0li9dp5dm90ywzimk2bbi9130x8")))

(define-public crate-mexprp-0.3.0 (c (n "mexprp") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rug") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00kml57301jggi4n1ppvnc2pxahb6s6sx5ml84qkf5v439583s5s") (f (quote (("default" "rug"))))))

(define-public crate-mexprp-0.3.1 (c (n "mexprp") (v "0.3.1") (d (list (d (n "rug") (r "^1.9") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0s0hdg1gk1yq8vzms2yvsvakmg1kdgyag4xn13qrcp0713q1dfsh") (f (quote (("default" "rug"))))))

