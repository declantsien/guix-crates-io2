(define-module (crates-io me lt melt) #:use-module (crates-io))

(define-public crate-melt-0.1.0 (c (n "melt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "16lyy6c61jc2xmrag6876gj8mj8hwx2g4w8f8pd3wyfidva2hb6r")))

(define-public crate-melt-0.1.1 (c (n "melt") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "010m1fmd453gn8vj757lk8sza8x2zd0fy5bb2h6bd2fwwwff01kg")))

(define-public crate-melt-0.1.2 (c (n "melt") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0qlh84nw39bp1as6rlsq0d8fgkl9fyz5vv4wbsvw91nrvh6v5c4n")))

(define-public crate-melt-0.1.3 (c (n "melt") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1dl1gay2wdjiqsi7mqcg1a67w5q7fmnkplwkrvfrl0n8dhs3bjca")))

(define-public crate-melt-0.1.4 (c (n "melt") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "14dmx6hfr8aa72kf1jfxgvz4j1ykyqmfi1s3p12qan96gxlcqs4x")))

(define-public crate-melt-0.1.5 (c (n "melt") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0x4b4jqlms2ymj4zrcprd39rp64pgv2zz3chgip203v3x5zqvp7f")))

(define-public crate-melt-0.1.6 (c (n "melt") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "02s9i1266sm1mkqy6j7cjn3a23w56gx4l1rsa084lg210sn0whw4")))

