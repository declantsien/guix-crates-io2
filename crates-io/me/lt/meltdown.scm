(define-module (crates-io me lt meltdown) #:use-module (crates-io))

(define-public crate-meltdown-0.0.0 (c (n "meltdown") (v "0.0.0") (h "0lqnzciqpvwlmls74a39wpx2cnj2qgyc75wicqz2q3s7dpwvkmzq")))

(define-public crate-meltdown-0.1.0 (c (n "meltdown") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)))) (h "158hs4la7b7352m1raf0c6z36sqbfk8rcxjiv7g835c2w7iijwmx")))

(define-public crate-meltdown-0.1.1 (c (n "meltdown") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)))) (h "1p7m1y5nd705bymhwqawslfm68wy1xk2l89diklqsqizc0289lqb")))

(define-public crate-meltdown-0.2.0 (c (n "meltdown") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.24") (d #t) (k 0)))) (h "0z7vd4pd1hhiqplgc01nmzgljshl55i9bsan68msvh9fkwa3zvh7")))

