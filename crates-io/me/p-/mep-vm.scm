(define-module (crates-io me p- mep-vm) #:use-module (crates-io))

(define-public crate-mep-vm-0.1.0 (c (n "mep-vm") (v "0.1.0") (d (list (d (n "ethereum-types") (r "^0.8.0") (d #t) (k 0)) (d (n "ethjson") (r "^0.1.0") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "parity-bytes") (r "^0.1") (d #t) (k 0)) (d (n "patricia-trie-ethereum") (r "^0.1.0") (d #t) (k 0)) (d (n "rlp") (r "^0.4.0") (d #t) (k 0)))) (h "0ddsghjr9kfp3bd88chs0hxz1m9z6nlyjj3qvv5zgljlhmnz3r6s")))

