(define-module (crates-io me lv melvin) #:use-module (crates-io))

(define-public crate-melvin-0.1.0 (c (n "melvin") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3.10") (d #t) (k 0)) (d (n "crc") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "unix_socket") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0kwf62rbbvyha6hf55kpbkzc7an74z1g8i22vgmgdl2hcrxb0w4r") (y #t)))

