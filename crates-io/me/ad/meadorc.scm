(define-module (crates-io me ad meadorc) #:use-module (crates-io))

(define-public crate-meadorc-0.1.0 (c (n "meadorc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "00gwjb0wp2anpzyavqcxhfn7s7aq6lqf3lh3fh3xda9vim8kq3ld")))

