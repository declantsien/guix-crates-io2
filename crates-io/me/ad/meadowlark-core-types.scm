(define-module (crates-io me ad meadowlark-core-types) #:use-module (crates-io))

(define-public crate-meadowlark-core-types-0.1.0 (c (n "meadowlark-core-types") (v "0.1.0") (h "1vm8nqdys635chcv5g5q7kaafxqdgh7j6n33nn8y6zkkqqwr7a3s")))

(define-public crate-meadowlark-core-types-0.1.1 (c (n "meadowlark-core-types") (v "0.1.1") (h "1h5srr7kas3sv2qz1yswgzqf2lgz27chjxd03ljvnrjjcizgh5ld")))

(define-public crate-meadowlark-core-types-0.3.0 (c (n "meadowlark-core-types") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rd226i0c7aba3lzw0hrgm3if3qghxsy0n1494gl3nhf93y6fa4q") (f (quote (("serde-derive" "serde") ("default"))))))

