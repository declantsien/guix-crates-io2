(define-module (crates-io me rg merge-source-map) #:use-module (crates-io))

(define-public crate-merge-source-map-0.1.0 (c (n "merge-source-map") (v "0.1.0") (d (list (d (n "sourcemap") (r "^7.0.1") (d #t) (k 0)))) (h "1rjm1ll4j22ds750mmhf13pwizdnpmf7s76qdvlm3lm8lv9dm57n")))

(define-public crate-merge-source-map-0.1.1 (c (n "merge-source-map") (v "0.1.1") (d (list (d (n "sourcemap") (r "^7.0.1") (d #t) (k 0)))) (h "0vadasmfhw97af4cqw1v5pd21qqcvxbwmlyahyzd723azi77zig0")))

(define-public crate-merge-source-map-1.0.0 (c (n "merge-source-map") (v "1.0.0") (d (list (d (n "sourcemap") (r "^7.0.1") (d #t) (k 0)))) (h "1crdaqsnpqhg1vw54b1n96wg01cwr0xzzw7kfkd7ahw3836q3kwh") (y #t)))

(define-public crate-merge-source-map-1.1.0 (c (n "merge-source-map") (v "1.1.0") (d (list (d (n "sourcemap") (r "^7.0.1") (d #t) (k 0)))) (h "1i7pzi461waag7i2vk7qmk7hzihqvx4wv4lwza51gkgmi2h7iz9n") (y #t)))

(define-public crate-merge-source-map-1.2.0 (c (n "merge-source-map") (v "1.2.0") (d (list (d (n "sourcemap") (r "^7.0.1") (d #t) (k 0)))) (h "1p9lq262p2zns9iqrn3afhfkpbfgz6p185fm1x7pp3cvh7x461gj")))

