(define-module (crates-io me rg mergle) #:use-module (crates-io))

(define-public crate-mergle-0.1.0 (c (n "mergle") (v "0.1.0") (d (list (d (n "bromberg_sl2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0y0djwixz6iynyjbyzny529z9lk7f76lrxvkhanzf3ic13dz844g")))

(define-public crate-mergle-0.1.1 (c (n "mergle") (v "0.1.1") (d (list (d (n "bromberg_sl2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0djrv2d6ygsqygvb7zfkc3lii7ky9bn54q3kgzc7p4g1sj85fln8")))

(define-public crate-mergle-0.1.2 (c (n "mergle") (v "0.1.2") (d (list (d (n "bromberg_sl2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0hidrzsv1h42jlxdhbl1fpjqvnnzwxcflr9v23qd58yk2fgxxz5a")))

(define-public crate-mergle-0.1.3 (c (n "mergle") (v "0.1.3") (d (list (d (n "bromberg_sl2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1y3l1wyixx6xnhv5847xfgihm6dy75n3x8fj2ssbm9azpxjmlc8k")))

(define-public crate-mergle-0.1.4 (c (n "mergle") (v "0.1.4") (d (list (d (n "bromberg_sl2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0rgccrf25lglhr3dzxy76hx2g0wi62754jkmpzmnjlw30sxx0ggk")))

(define-public crate-mergle-0.1.5 (c (n "mergle") (v "0.1.5") (d (list (d (n "bromberg_sl2") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0497yqpw3qkfh32yqhb59ccjiyvclvhm1v5xd3w8dqwf8q59rlzc")))

(define-public crate-mergle-0.2.0 (c (n "mergle") (v "0.2.0") (d (list (d (n "bromberg_sl2") (r "^0.4.4") (d #t) (k 0)) (d (n "fingertrees") (r "^0.2.4") (d #t) (k 0)) (d (n "im-rc") (r "^15.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "17k7x9ya8gwsx72jn5mxph2jwx2xzg7vwksmzfr716cw5x48ikab")))

(define-public crate-mergle-0.3.0 (c (n "mergle") (v "0.3.0") (d (list (d (n "bromberg_sl2") (r "^0.4.4") (d #t) (k 0)) (d (n "fingertrees") (r "^0.2.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1mvnyjbb74lll42k1cjfn5qsjksfpglgk0ybpshz0r2swmi7adlh")))

(define-public crate-mergle-0.3.1 (c (n "mergle") (v "0.3.1") (d (list (d (n "bromberg_sl2") (r "^0.4.4") (d #t) (k 0)) (d (n "fingertrees") (r "^0.2.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1280w05f3rd04wxbiqcpf31j0j2k2xk0akf4hnrisyj0yfwdyf8h")))

(define-public crate-mergle-0.3.2 (c (n "mergle") (v "0.3.2") (d (list (d (n "bromberg_sl2") (r "^0.4.4") (d #t) (k 0)) (d (n "fingertrees") (r "^0.2.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0cnknyvs26a0lbp81ws23xa192mb3f0q7fwn2c4537mzmqd0xaxy")))

(define-public crate-mergle-0.3.3 (c (n "mergle") (v "0.3.3") (d (list (d (n "bromberg_sl2") (r "^0.5.0") (d #t) (k 0)) (d (n "fingertrees") (r "^0.2.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1wsd3da9f1m1sxl73znryd10i80y617id67y265pr3mnz4wx6lri")))

(define-public crate-mergle-0.4.0 (c (n "mergle") (v "0.4.0") (d (list (d (n "bromberg_sl2") (r "^0.5.0") (d #t) (k 0)) (d (n "fingertrees") (r "^0.2.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0pi8fyyahpribia80r6hdl9p1345hn5xklqkxy27hajmg7vk3d3s")))

(define-public crate-mergle-0.5.0 (c (n "mergle") (v "0.5.0") (d (list (d (n "bromberg_sl2") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "167jggb51qi44j3jsvaskh74g04vhvf9rgzf41sljc1n6cnlsn48")))

(define-public crate-mergle-0.4.1 (c (n "mergle") (v "0.4.1") (d (list (d (n "bromberg_sl2") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0jxssrjcivhri1jgn45nvbgmvllhvfmc4rpiihd2fwsakvg6g310") (y #t)))

(define-public crate-mergle-0.5.1 (c (n "mergle") (v "0.5.1") (d (list (d (n "bromberg_sl2") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0pa7ljrixllhlc10bwh17i871arqz8xdy9miclfz89aqiffji7gl")))

(define-public crate-mergle-0.5.2 (c (n "mergle") (v "0.5.2") (d (list (d (n "bromberg_sl2") (r "^0.5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1c0i37bil1k9j3vwwk4pb2wqz723pi63lh3c80jid6cqkklhpar4")))

