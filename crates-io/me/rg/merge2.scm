(define-module (crates-io me rg merge2) #:use-module (crates-io))

(define-public crate-merge2-0.2.0 (c (n "merge2") (v "0.2.0") (d (list (d (n "merge2_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "002rbw1cr3sadqpbj4vd7izq7rxbhr4sxccx6b3m2mdyzlmbn37f") (f (quote (("std") ("num" "num-traits") ("derive" "merge2_derive") ("default" "derive" "std"))))))

(define-public crate-merge2-0.3.0 (c (n "merge2") (v "0.3.0") (d (list (d (n "merge2_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1vc9cji5w65yws0bvj6a3c5w13wbz6r1fix1nir0vz0qwjx5xrfw") (f (quote (("std") ("num" "num-traits") ("derive" "merge2_derive") ("default" "derive" "std"))))))

