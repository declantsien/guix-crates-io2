(define-module (crates-io me rg mergable) #:use-module (crates-io))

(define-public crate-mergable-0.0.0 (c (n "mergable") (v "0.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1rxxbjpc7jx08z30ym6wbicvqv1zz9x42zq5d0mxa6naz6g8v334")))

(define-public crate-mergable-0.2.0 (c (n "mergable") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "199sng0nnkxll0yqxq5k8nl8rsrgrqc8wqja7nmbijv59ns7bryq")))

(define-public crate-mergable-0.4.0 (c (n "mergable") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1f9jzhj9w11fqyfdd3w6km65mbvr130y3av2sdhhf6hkgzqmx683")))

(define-public crate-mergable-0.6.0 (c (n "mergable") (v "0.6.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0c8kmc4f8qs6w42qmbrfp7mim9cqcm2nvv8bl3sfcd8j9sjb22gk")))

(define-public crate-mergable-0.8.0 (c (n "mergable") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "1igy6qya36j2yg0knzpmghbkvavbxa3mf01zrgc0n14sy0b9i70n")))

(define-public crate-mergable-0.10.0 (c (n "mergable") (v "0.10.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1dq4fvpl6dcnn9nhc78ykgvid1kcrwyv1l8a2hnc6dqdscm7zfsq")))

(define-public crate-mergable-0.11.0 (c (n "mergable") (v "0.11.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1ayqak638r7qmxya6schz1qvs8zcisf0ssf4ykl800c4y96ixvfn")))

(define-public crate-mergable-0.12.0 (c (n "mergable") (v "0.12.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "011p3bnijc4lv96y3x4s626k32b9xasg1h39d3v2nryn5v7azs7w")))

(define-public crate-mergable-0.14.0 (c (n "mergable") (v "0.14.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0i6nfn363q23a30y90snkfvv8lzch8i5da5r5ihryn3nmn5l17z6")))

(define-public crate-mergable-0.15.0 (c (n "mergable") (v "0.15.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0iflqqpd2m9p9gy1qid9kf5bp37q9gdgbyh9r7abmmjrmhrmr5rx")))

(define-public crate-mergable-0.17.0 (c (n "mergable") (v "0.17.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "075hq9yvaxjsr0969rlgfq5dlpzl5x20f8qcc1sg6yy03c1ridr3")))

(define-public crate-mergable-0.19.0 (c (n "mergable") (v "0.19.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0mwkhd9g4va5zdlibk8rlp5hsqb1wj0cvbnlzsn50ma28sihy1gd")))

(define-public crate-mergable-0.20.0 (c (n "mergable") (v "0.20.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1gqajk36wyy2l13jm8z5skl67fsr1zm5nab1q0ilja5d20c5kicc")))

(define-public crate-mergable-0.21.0 (c (n "mergable") (v "0.21.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "15arirrv8g82w13fd495pb96sv47pmn20ip4zrhiay31dk4n1n7p")))

(define-public crate-mergable-0.23.0 (c (n "mergable") (v "0.23.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (d #t) (k 0)))) (h "1gpvp4r6z2wrpr67ln2j1wpjci8iymf21qkphaz7i8lscbk334dx")))

(define-public crate-mergable-0.25.0 (c (n "mergable") (v "0.25.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0g7fpv7ivfqb2siza4kpi4i0jhiszsv5sr129hbj0vf25zf55lj3")))

(define-public crate-mergable-0.27.0 (c (n "mergable") (v "0.27.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1ca1x4yzd985k2ssfbhkx3ywy7h6l3bdqkn31ayqky7ghkikgj3v")))

(define-public crate-mergable-0.29.0 (c (n "mergable") (v "0.29.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "1p7pkq87881kil2yabr1cczzrcccc1sv02bjsx717apvklrb07fl")))

(define-public crate-mergable-0.31.0 (c (n "mergable") (v "0.31.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "08cd6qf0vvkzf2j0672ybq15nxalq26mrydr6w7xax1dlcn8vgnq")))

(define-public crate-mergable-0.33.0 (c (n "mergable") (v "0.33.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1wkrma55sqmd7kdhmnav66ljy3d8q5w681c8nhycbfwszw68gd6q")))

(define-public crate-mergable-0.35.0 (c (n "mergable") (v "0.35.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1macr7s6x7qyg670x5ldjvnk5hq7f0agi24g822l27m55dp29f0j")))

(define-public crate-mergable-0.37.0 (c (n "mergable") (v "0.37.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1iz58kpn0cdarlbcdj51bsk2nmi980skvn4c4ccrbwym8akal4yh")))

(define-public crate-mergable-0.39.0 (c (n "mergable") (v "0.39.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0mnf5zf4wwwkr3ah2xjh5njkh8gi1ylyykaql1y5fgwikfif5g34")))

(define-public crate-mergable-0.40.0 (c (n "mergable") (v "0.40.0") (d (list (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1ggczx78mscnqmg1wdiy7jzkzbqnqfwb27yrwpkzlpg8dq2qryfb")))

(define-public crate-mergable-0.41.0 (c (n "mergable") (v "0.41.0") (d (list (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1dqsqdbz6bm001n8a2ri9jfsbqw6708nq7zwfkc1bzckv6f8ibhh")))

(define-public crate-mergable-0.43.0 (c (n "mergable") (v "0.43.0") (d (list (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "073jh9a8gfxlaypci9ypjnjign586lki7babfiklwibhvbk9dymy")))

