(define-module (crates-io me rg merge_derive-hashmap) #:use-module (crates-io))

(define-public crate-merge_derive-hashmap-0.1.1 (c (n "merge_derive-hashmap") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1202xgy6pd3bf4wm9ihr0ksnbqdxla9j1z5x3591q64lkqn7qyhm")))

