(define-module (crates-io me rg merge-io) #:use-module (crates-io))

(define-public crate-merge-io-0.1.0 (c (n "merge-io") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)))) (h "004hnwrg4pyqdim0z5lbazj09h7q6jgswa11c85sc7pj5fjqdi4y")))

(define-public crate-merge-io-0.1.1 (c (n "merge-io") (v "0.1.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)))) (h "0y08v62b6jlhqy2rj34jif86zq0wgcxbg0d11k02pbi3mb3hws2y")))

(define-public crate-merge-io-0.1.2 (c (n "merge-io") (v "0.1.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)))) (h "0wbcgjsbpdf9cq9vvfdydlb9f6z2dmkw5dxr6vidll5gbxi0n0hy")))

(define-public crate-merge-io-0.2.0 (c (n "merge-io") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)))) (h "1v4j0mm6qh9kjvf8b93mh0rv1zq2y3n7yzjj6kgf6jv4a9by8dyc")))

(define-public crate-merge-io-0.3.0 (c (n "merge-io") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)))) (h "0mv357fzs854jw4g7g32s27ww7nwqfchsb60i3q6mcszs26dl234")))

