(define-module (crates-io me rg mergui) #:use-module (crates-io))

(define-public crate-mergui-0.0.1 (c (n "mergui") (v "0.0.1") (d (list (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "quicksilver") (r "^0.3.20") (d #t) (k 0)))) (h "0vr7mib4jlv7xl33b516391j76gnhplz9mkbs629hzq989bz9mc5")))

(define-public crate-mergui-0.0.2 (c (n "mergui") (v "0.0.2") (d (list (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "quicksilver") (r "^0.3.20") (d #t) (k 0)))) (h "1idacqymb596g73z6kh8wdk67dk3lmzkzaiq9r8hvjk78ynp5ivl")))

(define-public crate-mergui-0.0.3 (c (n "mergui") (v "0.0.3") (d (list (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "quicksilver") (r "^0.3.20") (d #t) (k 0)))) (h "16ldmyxrhsnbf8mp0dk2fll33cqdcx16nhszna4xmj5cn5v87wi6")))

(define-public crate-mergui-0.0.4 (c (n "mergui") (v "0.0.4") (d (list (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "quicksilver") (r "^0.3.20") (d #t) (k 0)))) (h "1vv2sm5i1favd3n59q2k8byyij06yipmjr2h7h6lrwd69g2lry40")))

(define-public crate-mergui-0.0.5 (c (n "mergui") (v "0.0.5") (d (list (d (n "indexmap") (r "^1.3.0") (d #t) (k 0)) (d (n "quicksilver") (r "^0.3.20") (d #t) (k 0)))) (h "0d8k07y4dgl905735c7c0qaddxpzgvfwpwcws7v4b2cb62jjs1qi")))

(define-public crate-mergui-0.1.0-alpha.1 (c (n "mergui") (v "0.1.0-alpha.1") (d (list (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "quicksilver") (r "^0.4.0-alpha0.3") (f (quote ("ttf" "font"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (d #t) (k 0)))) (h "0iyzh9a9n56601sz0mv6wwlqbzd7szdq38rwqjw2n9gmmhbl8shv") (f (quote (("web-sys" "quicksilver/web-sys") ("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1.0-alpha0.2 (c (n "mergui") (v "0.1.0-alpha0.2") (d (list (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "quicksilver") (r "^0.4.0-alpha0.4") (f (quote ("ttf" "font"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (d #t) (k 0)))) (h "1gfmvgqdg69g0zkb3irsivjcir4bq4fd770yy63a0kxhwwcgbwm6") (f (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1.0-alpha0.3 (c (n "mergui") (v "0.1.0-alpha0.3") (d (list (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "quicksilver") (r "^0.4.0-alpha0.4") (f (quote ("ttf" "font"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (d #t) (k 0)))) (h "0np345271v6zzfpk2l485m10qqkciybc8ly7hgn7ldij6ymi3yi9") (f (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1.0-alpha0.4 (c (n "mergui") (v "0.1.0-alpha0.4") (d (list (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "quicksilver") (r "^0.4.0-alpha0.5") (f (quote ("ttf" "font"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (d #t) (k 0)))) (h "0fxrqk2vshfsajl2025724fjgjkphvpw9pni2bh19snisa04573j") (f (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1.0-alpha0.5 (c (n "mergui") (v "0.1.0-alpha0.5") (d (list (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "quicksilver") (r "^0.4.0-alpha0.5") (f (quote ("ttf" "font"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (d #t) (k 0)))) (h "1djn3rjlfbbwwlvn0nm1bj5qk73py0riwiv52bjcb5qbsv0vc049") (f (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1.0-alpha0.6 (c (n "mergui") (v "0.1.0-alpha0.6") (d (list (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "quicksilver") (r "^0.4.0-alpha0.6") (f (quote ("ttf" "font"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (d #t) (k 0)))) (h "0i36h0dl70z9livqswdg2m8l7hqmfg1vqiyiiwvw4hjbdlkqg69y") (f (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1.0-alpha0.7 (c (n "mergui") (v "0.1.0-alpha0.7") (d (list (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "quicksilver") (r "^0.4.0-alpha0.7") (f (quote ("ttf" "font"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (d #t) (k 0)))) (h "15x65c39xzyd2ikf7kqh7hxhgv91m1cnlssdriaz569j9hrmh0ym") (f (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1.0-alpha0.8 (c (n "mergui") (v "0.1.0-alpha0.8") (d (list (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "quicksilver") (r "^0.4.0") (f (quote ("ttf" "font"))) (k 0)) (d (n "unicode-normalization") (r "^0.1.12") (d #t) (k 0)))) (h "1jkyp9s76mdq96xcmrbhj1dnxj8bcs3mi5crb383v947fvjh3xxv") (f (quote (("stdweb" "quicksilver/stdweb"))))))

