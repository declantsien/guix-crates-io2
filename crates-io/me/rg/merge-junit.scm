(define-module (crates-io me rg merge-junit) #:use-module (crates-io))

(define-public crate-merge-junit-0.1.0 (c (n "merge-junit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "12hfm4rizmivpcn67ypal182yadix9ikfn19bf65x28ancplffxw")))

(define-public crate-merge-junit-0.1.1 (c (n "merge-junit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "0c3jx5a538d772afkj7siapbyb9m5zbsc34c16s85bpcmy3xd42q")))

(define-public crate-merge-junit-0.1.3 (c (n "merge-junit") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "1kpymp5cakjz1d2vhz6bq703k76bi8xdzmq8mid8rnhlpq9camw9")))

(define-public crate-merge-junit-0.1.4 (c (n "merge-junit") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "08wp5h0925wlxvcmbdjav0xbil7xqkra7v5q8wswg2w4nha1q0mc")))

