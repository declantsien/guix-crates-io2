(define-module (crates-io me rg merge_config_files) #:use-module (crates-io))

(define-public crate-merge_config_files-0.1.0 (c (n "merge_config_files") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1qgqc00y6pzr3n10byis6l3zl9k0gwr6zkijrpssyi2zgfzs9q65")))

(define-public crate-merge_config_files-0.1.1 (c (n "merge_config_files") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0mrg7rg601zsss8gi71rncm18rgpn0kw2wxxrlpd8gajrfgdnfsv")))

(define-public crate-merge_config_files-0.1.2 (c (n "merge_config_files") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "12iwrhpjn5h70nykpxvbpvxcbc63b9cmx1l4pga1rf27ih7hxf8f")))

(define-public crate-merge_config_files-0.1.3 (c (n "merge_config_files") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "19drr90mq42aq5ix5dm6ziq22xnpvyxvprjifqm76kz0d98prva8")))

(define-public crate-merge_config_files-0.1.5 (c (n "merge_config_files") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "106skpymlm4ml04c6qbpf6qcwn1krjc67v94cr1gsf6znyii92i6")))

