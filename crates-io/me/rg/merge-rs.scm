(define-module (crates-io me rg merge-rs) #:use-module (crates-io))

(define-public crate-merge-rs-0.1.0 (c (n "merge-rs") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)))) (h "1cc27h0kg15dpw719z1xr2cmssxrf71h9344w8zhxblgkwlmggxf") (y #t) (r "1.58.1")))

(define-public crate-merge-rs-0.1.1 (c (n "merge-rs") (v "0.1.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)))) (h "0i4vv8gdbmp06j9blygd0w5nnhvjslwbzw2nqb92q6rlwl27ni90") (y #t) (r "1.58.1")))

(define-public crate-merge-rs-0.1.2 (c (n "merge-rs") (v "0.1.2") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)))) (h "1brqfsd50a549qynsr26277yvzqjgl92568dhz0hsaiqg7i0i6a9") (r "1.58.1")))

(define-public crate-merge-rs-0.2.0 (c (n "merge-rs") (v "0.2.0") (d (list (d (n "concat-idents") (r "^1.1.3") (d #t) (k 2)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)))) (h "0vnr5mbsh1iz1d96gh21y9c6dagyxn32w3d3q366qaqyc0p7rkjd") (r "1.58.1")))

(define-public crate-merge-rs-0.3.0 (c (n "merge-rs") (v "0.3.0") (d (list (d (n "concat-idents") (r "^1.1.3") (d #t) (k 2)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)))) (h "1b4bhj8ps3bqwg3kgqqkh16ggdkak0x3w90zffb2q9nvfj8bcywm") (r "1.58.1")))

(define-public crate-merge-rs-0.4.0 (c (n "merge-rs") (v "0.4.0") (d (list (d (n "concat-idents") (r "^1.1.3") (d #t) (k 2)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "merge-rs-derive") (r "^0.4.0") (d #t) (k 0)))) (h "0rvizsah7lgdl7pi4jawl4h5mzn3zriqqj11x4h4x1nyrjk76wa3") (r "1.68.2")))

