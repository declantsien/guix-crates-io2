(define-module (crates-io me rg merge-yaml-hash) #:use-module (crates-io))

(define-public crate-merge-yaml-hash-0.1.1 (c (n "merge-yaml-hash") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0af6yg6qw4m2jj7fbk3rvj9pf1lz0fv4czjnk0k1fggm4xz090rl")))

(define-public crate-merge-yaml-hash-0.1.2 (c (n "merge-yaml-hash") (v "0.1.2") (d (list (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0i0vwq9w8y1hmafvnva7rj28ak7r0avgbdwdymvrhbv2ay20isns")))

(define-public crate-merge-yaml-hash-0.1.3 (c (n "merge-yaml-hash") (v "0.1.3") (d (list (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "00q4izqhhrv1202qby5aim2zfxzd6fayi3y67xg7yl05m24k34sc")))

(define-public crate-merge-yaml-hash-0.2.0 (c (n "merge-yaml-hash") (v "0.2.0") (d (list (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0kjmr36dhki1a174cb62avcz8m68sqdqbqknv05954xxkyjgn7qk")))

(define-public crate-merge-yaml-hash-0.3.0 (c (n "merge-yaml-hash") (v "0.3.0") (d (list (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "1pcj6nm296lbqrabrmb188wxhd7s1fbmfbzrfg5xfw4j67vsairq")))

