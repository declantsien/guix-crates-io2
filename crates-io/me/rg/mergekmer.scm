(define-module (crates-io me rg mergekmer) #:use-module (crates-io))

(define-public crate-mergekmer-0.1.0 (c (n "mergekmer") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a6s7x4qw3p28myzmfgnxv6ry2kh11nac8r8s530h03l913abxiz")))

(define-public crate-mergekmer-0.1.1 (c (n "mergekmer") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xp23z790c49v945jm66rw2gn3l45w02vsr6r7mzg0w6niyjzacz")))

(define-public crate-mergekmer-0.1.2 (c (n "mergekmer") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10j0aq406mx911y4jirh7x0mi141738kncqh3a76krh0bpa3pp9g")))

