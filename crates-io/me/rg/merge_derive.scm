(define-module (crates-io me rg merge_derive) #:use-module (crates-io))

(define-public crate-merge_derive-0.1.0 (c (n "merge_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01wxhi7mqmp34l540wcfb24hb13vcbps4wlynas66bnsfra0g790")))

