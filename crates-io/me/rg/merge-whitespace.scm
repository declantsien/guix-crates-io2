(define-module (crates-io me rg merge-whitespace) #:use-module (crates-io))

(define-public crate-merge-whitespace-0.1.0 (c (n "merge-whitespace") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (d #t) (k 0)))) (h "1vqx120h9yv38ll0n2c01hi420ryzr1y1xjjwiby1sfdnqxpis5d")))

