(define-module (crates-io me rg merger) #:use-module (crates-io))

(define-public crate-merger-0.1.0 (c (n "merger") (v "0.1.0") (d (list (d (n "merge-yaml-hash") (r "^0.2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "0rdrgifm3mvgiyjxwala069z44yvi1i28j48fpnxm95nivhrljmm")))

