(define-module (crates-io me rg merge-struct) #:use-module (crates-io))

(define-public crate-merge-struct-0.1.0 (c (n "merge-struct") (v "0.1.0") (d (list (d (n "insta") (r "^1.17.1") (f (quote ("backtrace" "redactions"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0jnx0inklv865ny9h507835z0zrbcayvxfdn76w3ahg244nh30hx")))

