(define-module (crates-io me rg merge-cfg) #:use-module (crates-io))

(define-public crate-merge-cfg-0.1.0 (c (n "merge-cfg") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1chl8wggsb29hd0mpqkbh6dc8sbnq0r18hyfd89kbaa00pxar0zm")))

(define-public crate-merge-cfg-0.1.1 (c (n "merge-cfg") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1bzajhk86cwil8vhyy7nw1xpy27cldc92g59dlslslb69ncs0kcg")))

