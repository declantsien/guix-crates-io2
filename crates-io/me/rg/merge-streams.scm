(define-module (crates-io me rg merge-streams) #:use-module (crates-io))

(define-public crate-merge-streams-0.1.0 (c (n "merge-streams") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.16") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.0.8") (d #t) (k 0)))) (h "0vp6ffq39j61z58mapnh4w0vxb1pmxvq3sfm1a3pssbmnal4dv9z")))

(define-public crate-merge-streams-0.1.1 (c (n "merge-streams") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.16") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.0.8") (d #t) (k 0)))) (h "0z63w34pzn4k47ydnwd029fy5pandz8gq573qnqyz1bzl8w5vb19")))

(define-public crate-merge-streams-0.1.2 (c (n "merge-streams") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.16") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.0.8") (d #t) (k 0)))) (h "1hm7nvry0dzgkbgrin48xkl7cv4hfpzf87xcwx3d5av9552zd12g")))

