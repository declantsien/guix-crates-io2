(define-module (crates-io me rg merge-lang) #:use-module (crates-io))

(define-public crate-merge-lang-0.1.0 (c (n "merge-lang") (v "0.1.0") (d (list (d (n "bytestring") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "18cn7wkh513j7vlq8wm8rwsw24a9yj9yqv755c5ip6lgpnwfgvgr")))

(define-public crate-merge-lang-0.1.1 (c (n "merge-lang") (v "0.1.1") (d (list (d (n "bytestring") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0170isl0w0sv5swp0ysp8rmcydpngxgc9gf3fl52dqv46j8dhpk4")))

(define-public crate-merge-lang-0.1.2 (c (n "merge-lang") (v "0.1.2") (d (list (d (n "bytestring") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "19alwfvpha12skbzk63is6346bvwll876xqw7rlmgnhyq4alydxj")))

