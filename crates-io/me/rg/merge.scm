(define-module (crates-io me rg merge) #:use-module (crates-io))

(define-public crate-merge-0.1.0 (c (n "merge") (v "0.1.0") (d (list (d (n "envy") (r "^0.4") (d #t) (k 2)) (d (n "merge_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1sck0vhi9lk8a6mgky0rgn842fj7yspywidwbd963nmimf9yzfqh") (f (quote (("std") ("num" "num-traits") ("derive" "merge_derive") ("default" "derive" "num" "std"))))))

