(define-module (crates-io me rg merging-iterator) #:use-module (crates-io))

(define-public crate-merging-iterator-1.0.0 (c (n "merging-iterator") (v "1.0.0") (d (list (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "1622cxsivvdbaaif1vd4p3z07xcvzpavhghpr26022gdzmpjfgr2")))

(define-public crate-merging-iterator-1.1.0 (c (n "merging-iterator") (v "1.1.0") (d (list (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "1rhkszcy47dpzmam53zkhv7nf3isgr7p66in9k9zlqwdmmm1c0sk")))

(define-public crate-merging-iterator-1.2.0 (c (n "merging-iterator") (v "1.2.0") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "0bmjvfvnrq1n35dis1wns7psmliczcin4zvlmh7v9q07p3jk7w9y")))

(define-public crate-merging-iterator-1.3.0 (c (n "merging-iterator") (v "1.3.0") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "1bkylvliks45693drwmiyanq3bxypmf37d6hv7gjiq2h40bmbnhf")))

