(define-module (crates-io me rg merge_json) #:use-module (crates-io))

(define-public crate-merge_json-0.1.0 (c (n "merge_json") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "thousands") (r "^0") (d #t) (k 0)))) (h "0mr4j17m7dsdmb72n4h3iyh1nhvbv79g2vyyjixmp525zzjb5h8d")))

