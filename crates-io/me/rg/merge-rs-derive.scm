(define-module (crates-io me rg merge-rs-derive) #:use-module (crates-io))

(define-public crate-merge-rs-derive-0.4.0 (c (n "merge-rs-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "09x5k6mvlycw3zbnsca73iy0439yqva541g3973dnbjgjhknz9wz") (r "1.68.2")))

