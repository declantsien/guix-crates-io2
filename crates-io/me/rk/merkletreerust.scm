(define-module (crates-io me rk merkletreerust) #:use-module (crates-io))

(define-public crate-merkletreerust-0.1.4 (c (n "merkletreerust") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0yws9insvv0kp9044w8x9ghp07m8a8kms7ws8i74i7l3jmy1wa6l")))

(define-public crate-merkletreerust-0.1.5 (c (n "merkletreerust") (v "0.1.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0nkbsj709rc2igyqdw7pbwm8alamm8bnbzn26vsvy6mq6mvgg0ip")))

(define-public crate-merkletreerust-0.1.6 (c (n "merkletreerust") (v "0.1.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1cdikcwisnb6wv2ajrkfknk4yjz8947pwnxc1pk3hbxynyi2jn1h")))

(define-public crate-merkletreerust-0.1.7 (c (n "merkletreerust") (v "0.1.7") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1xg0mpj9spq3xgiwjma0nc772gh29idqzhgkb3mv5ys5sqyvjfqp")))

