(define-module (crates-io me rk merkle-tree-bulletin-board-backend-mysql) #:use-module (crates-io))

(define-public crate-merkle-tree-bulletin-board-backend-mysql-0.1.0 (c (n "merkle-tree-bulletin-board-backend-mysql") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "merkle-tree-bulletin-board") (r "^0.1") (d #t) (k 0)) (d (n "mysql") (r "^21.0") (d #t) (k 0)))) (h "14ffadlrxn5wxqfnjhm12v8snah58mp0dzdx675l8xinr0w57jyh")))

(define-public crate-merkle-tree-bulletin-board-backend-mysql-0.1.1 (c (n "merkle-tree-bulletin-board-backend-mysql") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "merkle-tree-bulletin-board") (r "^0.1") (d #t) (k 0)) (d (n "mysql") (r "^21.0") (d #t) (k 0)))) (h "022nzka8qa160qyqfnmw0zc1g38i5hg9smzrkxg5h1abxpnc2sa1")))

(define-public crate-merkle-tree-bulletin-board-backend-mysql-0.2.0 (c (n "merkle-tree-bulletin-board-backend-mysql") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "merkle-tree-bulletin-board") (r "^0.2") (d #t) (k 0)) (d (n "mysql") (r "^23") (d #t) (k 0)))) (h "165fvyf2sgk8fqslgcydz5hgkk474pjgvdm7rd5jisnjnmrqq7r1")))

(define-public crate-merkle-tree-bulletin-board-backend-mysql-0.3.0 (c (n "merkle-tree-bulletin-board-backend-mysql") (v "0.3.0") (d (list (d (n "merkle-tree-bulletin-board") (r "^0.3") (d #t) (k 0)) (d (n "mysql") (r "^23") (d #t) (k 0)))) (h "0vb54s47xncbsdx80hwm3mck2wxl72rw0yrlvjgb1wnydv62b32n")))

