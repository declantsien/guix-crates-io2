(define-module (crates-io me rk merkle-heapless) #:use-module (crates-io))

(define-public crate-merkle-heapless-0.0.1 (c (n "merkle-heapless") (v "0.0.1") (d (list (d (n "mmr-macro") (r "^0.0.1") (d #t) (k 0)))) (h "1vimzg6vdrkwzy17z34a5byfqqq69hqvvwgwv46s0wakfpy45van") (f (quote (("mmr_macro")))) (y #t)))

(define-public crate-merkle-heapless-0.0.2 (c (n "merkle-heapless") (v "0.0.2") (d (list (d (n "mmr-macro") (r "^0.0.2") (d #t) (k 0)))) (h "0kcc9hy5j97hgsvhsaj1r5h4b05ackiyygwfvs2i0r6ihsw6v6p6") (f (quote (("mmr_macro")))) (y #t)))

(define-public crate-merkle-heapless-0.0.3 (c (n "merkle-heapless") (v "0.0.3") (d (list (d (n "mmr-macro") (r "^0.0.3") (d #t) (k 0)))) (h "0l4kq8476cslpys22ix04aaxgd5gfcanydcwagf1i8chiap61fwa") (f (quote (("mmr_macro")))) (y #t)))

(define-public crate-merkle-heapless-0.0.4 (c (n "merkle-heapless") (v "0.0.4") (d (list (d (n "mmr-macro") (r "^0.0.4") (d #t) (k 0)))) (h "12l1xpqznbkb7rba5dcmrj7n7k300h1ql34i757w2f4bjmyigw1c") (f (quote (("mmr_macro"))))))

(define-public crate-merkle-heapless-0.0.5 (c (n "merkle-heapless") (v "0.0.5") (d (list (d (n "mmr-macro") (r "^0.0.5") (d #t) (k 0)))) (h "1ykrgn1rp9shs9nx8951w8k6gi63igpdhpf3xx5i02hbvir0n3f6") (f (quote (("mmr_macro"))))))

(define-public crate-merkle-heapless-0.0.6 (c (n "merkle-heapless") (v "0.0.6") (d (list (d (n "mmr-macro") (r "^0.0.6") (d #t) (k 0)))) (h "06ikjyc78s7mdhkfbd0k1n5946c83j7nsg5xigd3s8c4zan7p59c") (f (quote (("mmr_macro"))))))

(define-public crate-merkle-heapless-0.0.7 (c (n "merkle-heapless") (v "0.0.7") (d (list (d (n "mmr-macro") (r "^0.0.7") (d #t) (k 0)))) (h "130mfsm95ifaz4y32sqqbq2jpqv5h6j7mzlpkx14ajmr6yv9am5n") (f (quote (("mmr_macro"))))))

