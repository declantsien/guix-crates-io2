(define-module (crates-io me rk merkletree-rs) #:use-module (crates-io))

(define-public crate-merkletree-rs-0.0.1 (c (n "merkletree-rs") (v "0.0.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0.0") (d #t) (k 0)) (d (n "rusty-leveldb") (r "^0.2.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "06q3l58nn3rwjakqvfqy6pjp0q163291iib6bc7lxy9x1h2s5xkx")))

(define-public crate-merkletree-rs-0.0.2 (c (n "merkletree-rs") (v "0.0.2") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0.0") (d #t) (k 0)) (d (n "rusty-leveldb") (r "^0.2.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "1w45vfrbpx5zjdi3bm00bsgviad9y81bl38h32kj0n94r1b86wns")))

(define-public crate-merkletree-rs-0.0.3 (c (n "merkletree-rs") (v "0.0.3") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0.0") (d #t) (k 0)) (d (n "rusty-leveldb") (r "^0.2.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)))) (h "0nd8xxb478ak96rz7jbm6p977d7c6wrrw6pcrhaif1qhrp2ri2p5")))

