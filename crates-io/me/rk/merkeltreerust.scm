(define-module (crates-io me rk merkeltreerust) #:use-module (crates-io))

(define-public crate-merkeltreerust-0.1.0 (c (n "merkeltreerust") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "web3") (r "^0.18.0") (d #t) (k 0)))) (h "07fwmqgjvjhh1xbmnwkh50mam77x4pfdvxz3w7r9vin6ky8p1dka")))

(define-public crate-merkeltreerust-0.1.1 (c (n "merkeltreerust") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "19kzypjxmxzb4y5fmrvvgnm6kbw6mddxy5n0qpq16q0c52yzvzci")))

(define-public crate-merkeltreerust-0.1.2 (c (n "merkeltreerust") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0mpcglp1ym8j0hqpcyn6bgc3shxh11f2c08ydv4jjrf128zf1nyi")))

(define-public crate-merkeltreerust-0.1.3 (c (n "merkeltreerust") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1g5pxgjjc2wfrgpn57s3q1a2gssfpmyg5g3pzkw0wsgv5j8rb8w9")))

(define-public crate-merkeltreerust-0.1.4 (c (n "merkeltreerust") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "14lf2rz1c6sg55nn1w3nla0yi2iqskjmpakwnbqk6wzagdhddibz")))

