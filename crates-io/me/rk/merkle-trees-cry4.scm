(define-module (crates-io me rk merkle-trees-cry4) #:use-module (crates-io))

(define-public crate-merkle-trees-cry4-0.1.0 (c (n "merkle-trees-cry4") (v "0.1.0") (d (list (d (n "one-time-signatures-cry4") (r "^0.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0wqq0hflf2azydvy48zns87a51xsa9k4pqql6ff1cz2zmnbb8bag")))

(define-public crate-merkle-trees-cry4-0.1.1 (c (n "merkle-trees-cry4") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.3") (o #t) (d #t) (k 0)) (d (n "one-time-signatures-cry4") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "09yngy6mjwqydflr1sjzarws6ff6rm3ipi465m12g4274wh422ws") (f (quote (("build-binary" "rand" "itertools"))))))

