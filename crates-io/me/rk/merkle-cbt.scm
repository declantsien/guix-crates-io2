(define-module (crates-io me rk merkle-cbt) #:use-module (crates-io))

(define-public crate-merkle-cbt-0.1.0 (c (n "merkle-cbt") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "numext-fixed-hash") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "1a936gc2nc1mnhiy17md3p5jjvbrzhxw1jcdnzha4mc7sbfs057p") (f (quote (("sha3") ("default") ("blake2b"))))))

(define-public crate-merkle-cbt-0.1.1 (c (n "merkle-cbt") (v "0.1.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "numext-fixed-hash") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "13hagakpf6fnp1dznlm920a7hg7fqffh7c6cbnyrj80albjb920b") (f (quote (("sha3") ("default") ("blake2b"))))))

(define-public crate-merkle-cbt-0.2.0 (c (n "merkle-cbt") (v "0.2.0") (d (list (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0zpfbjsm3d57x699sw4rrmd190k175v0x15ibwrfbv465qfqbvjf")))

(define-public crate-merkle-cbt-0.2.1 (c (n "merkle-cbt") (v "0.2.1") (d (list (d (n "proptest") (r "^0.8") (d #t) (k 2)))) (h "0qr8smid1cbrgjqsdql7jbmr71m11b6kpjpvrkkl09b4rakvb2fm")))

(define-public crate-merkle-cbt-0.2.2 (c (n "merkle-cbt") (v "0.2.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)))) (h "1jlkwq6wcjbx3v4k26agyjp3skmv25bz0g32nzcplz65ildcg58c") (f (quote (("std") ("default" "std"))))))

(define-public crate-merkle-cbt-0.3.0 (c (n "merkle-cbt") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)))) (h "1gl1197i03c6mal2j16z01xdrmc43n2mxhbrbam6qbf0ir1ch6kg") (f (quote (("std") ("default" "std"))))))

(define-public crate-merkle-cbt-0.3.2 (c (n "merkle-cbt") (v "0.3.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0misvlrpslxzi8a8ibn7sl7haylqh848iw6c0hxiq4im11q2y78p") (f (quote (("std") ("default" "std"))))))

