(define-module (crates-io me rk merkle-rs) #:use-module (crates-io))

(define-public crate-merkle-rs-0.0.1 (c (n "merkle-rs") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 2)) (d (n "digest") (r "^0.7.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 2)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.23") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 2)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "0x265221f8mkcb053yly9nzs2s9zvl70zzx39ha5zm56xi3l9d62") (f (quote (("default" "ring"))))))

