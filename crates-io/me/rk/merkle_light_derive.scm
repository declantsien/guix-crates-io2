(define-module (crates-io me rk merkle_light_derive) #:use-module (crates-io))

(define-public crate-merkle_light_derive-0.1.0 (c (n "merkle_light_derive") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.172") (o #t) (d #t) (k 0)) (d (n "merkle_light") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0allx5bbpybbc3anwbc20y9x4h3xy3qapzckxb1rzp16im69v8ij") (f (quote (("default"))))))

(define-public crate-merkle_light_derive-0.2.0 (c (n "merkle_light_derive") (v "0.2.0") (d (list (d (n "merkle_light") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0awvnmws5nidgfc5l8lp359r7rny2ajfgcbk8fx5iv8dqk0hwza2")))

(define-public crate-merkle_light_derive-0.3.0 (c (n "merkle_light_derive") (v "0.3.0") (d (list (d (n "merkle_light") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "03kzdqrkqbhzrm8z9sxhr4cxbl6011ckai2b48abnbmyk84gnnaw")))

(define-public crate-merkle_light_derive-0.3.1 (c (n "merkle_light_derive") (v "0.3.1") (d (list (d (n "merkle_light") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "10bzjim2pap1r0cd49mylv7qbmx9xchplkvnd0r9kdll5y3lq139")))

(define-public crate-merkle_light_derive-0.4.0 (c (n "merkle_light_derive") (v "0.4.0") (d (list (d (n "merkle_light") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1zf46hd9jr229xmy7yms6497g6v52rq4p9zi8vbn508xal5zw57j")))

