(define-module (crates-io me rk merkle_light) #:use-module (crates-io))

(define-public crate-merkle_light-0.1.0 (c (n "merkle_light") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.172") (o #t) (d #t) (k 0)))) (h "1n0hqp49gr88jqq2xrbapvy6cjca8k1nj1vvibw84g14v2qvvz75") (f (quote (("default"))))))

(define-public crate-merkle_light-0.2.0 (c (n "merkle_light") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (o #t) (d #t) (k 0)))) (h "19v7ql3zzicchacnxynzrmxs8xcwfh5nkm8pnsdk1q39dhr2fvpx") (f (quote (("default") ("crypto_bench" "rust-crypto" "rand") ("chaincore" "rust-crypto") ("bitcoin" "ring" "rust-crypto"))))))

(define-public crate-merkle_light-0.3.0 (c (n "merkle_light") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (o #t) (d #t) (k 0)))) (h "1qnss8idnqw0f9s0bqvf8lnbg9abfl0psq7rni3b5d2crg2wlvc5") (f (quote (("default") ("crypto_bench" "rust-crypto" "rand") ("chaincore" "rust-crypto") ("bitcoin" "ring" "rust-crypto"))))))

(define-public crate-merkle_light-0.3.1 (c (n "merkle_light") (v "0.3.1") (d (list (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (o #t) (d #t) (k 0)))) (h "0sf2xdj19dqfhyad9a3y5sc6z5qp4pnvqh88rix7ni3idaggw8i5") (f (quote (("default") ("crypto_bench" "rust-crypto" "ring" "rand") ("chaincore" "rust-crypto") ("bitcoin" "ring" "rust-crypto"))))))

(define-public crate-merkle_light-0.4.0 (c (n "merkle_light") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (o #t) (d #t) (k 0)))) (h "1a96g81k88hv5r6b6lq97hv55700ffinqdah3b0xjairxhbzcydq") (f (quote (("std") ("default" "std") ("crypto_bench" "rust-crypto" "ring" "rand") ("chaincore" "rust-crypto") ("bitcoin" "ring" "rust-crypto"))))))

