(define-module (crates-io me rk merkle-sha3) #:use-module (crates-io))

(define-public crate-merkle-sha3-0.1.0 (c (n "merkle-sha3") (v "0.1.0") (d (list (d (n "protobuf") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0s0qx9fbkjlr2b3220fd02vyzh8ijl6swr69xm6c6j6jr6g55max") (f (quote (("serialization-protobuf" "protobuf"))))))

