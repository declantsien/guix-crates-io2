(define-module (crates-io me rk merkle_test) #:use-module (crates-io))

(define-public crate-merkle_test-0.1.0 (c (n "merkle_test") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1jym8s0h5hz97q2zyvpq91c8k34gzw9xxgkzha2a4r8mqfmbn738")))

(define-public crate-merkle_test-0.1.1 (c (n "merkle_test") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "08p4mpi485r3zjnaggxgi2idy2hm1r6l35x0gpz41xns7d2j2pmj")))

