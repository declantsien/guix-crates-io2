(define-module (crates-io me rk merkle_tree) #:use-module (crates-io))

(define-public crate-merkle_tree-0.1.0 (c (n "merkle_tree") (v "0.1.0") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-slice") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rayon") (r "^0.7.1") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1irhr3nz8b16g3h22194z0l0zh180aws5f7aymf9xspsbilmzc6q")))

