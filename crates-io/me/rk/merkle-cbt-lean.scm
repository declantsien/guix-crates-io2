(define-module (crates-io me rk merkle-cbt-lean) #:use-module (crates-io))

(define-public crate-merkle-cbt-lean-0.1.0 (c (n "merkle-cbt-lean") (v "0.1.0") (d (list (d (n "blake3") (r "^1.5.0") (k 2)) (d (n "external-memory-tools") (r "^0.1.0") (k 0)) (d (n "merkle-cbt") (r "^0.3.2") (k 2)))) (h "1mdxbhxa0aa1f2rx7lgrdmy39gvwz9wzj9r6x5w52iz1rr3wc3x9") (f (quote (("std") ("proof-gen") ("default" "blake3/std" "external-memory-tools/std" "proof-gen" "std"))))))

(define-public crate-merkle-cbt-lean-0.1.1 (c (n "merkle-cbt-lean") (v "0.1.1") (d (list (d (n "blake3") (r "^1.5.0") (k 2)) (d (n "external-memory-tools") (r "^0.1.1") (k 0)) (d (n "merkle-cbt") (r "^0.3.2") (k 2)))) (h "0s4n9a9hvjhwhxgvfp0gaqviicga7cgrdr0kvjbh130j5dfhyyjs") (f (quote (("std") ("proof-gen") ("default" "blake3/std" "external-memory-tools/std" "proof-gen" "std"))))))

