(define-module (crates-io me rk merklebtree) #:use-module (crates-io))

(define-public crate-merklebtree-0.1.0 (c (n "merklebtree") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0n4z3z1spd2jk3q7wyqxx3pqxyy0a6abhan2j98j85w275j8bn8m")))

(define-public crate-merklebtree-0.1.1 (c (n "merklebtree") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "19ipny70zv3rc36cq25nygz4l9mg8xazx5w2m8953kk2rnjdga4n")))

(define-public crate-merklebtree-0.1.2 (c (n "merklebtree") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1as87ps36jl4fa7wkm00vv7b5a3s8l0pcavjr2ksbmbxj6kxba8s")))

(define-public crate-merklebtree-0.1.3 (c (n "merklebtree") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "13sfq0rljfdgg2733fc1lzp2qijy0m38hwlx7j2iq4kq0qrnzdj5")))

(define-public crate-merklebtree-0.1.4 (c (n "merklebtree") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1n43mj5876m8y7i4k3sryrjpljz7mzydf2znyz4d2pr66hwahm85")))

(define-public crate-merklebtree-0.1.5 (c (n "merklebtree") (v "0.1.5") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1apys2bl8a96awflxx6pd8la2rv7d5xs3fnsag0bfk132b4fr445")))

