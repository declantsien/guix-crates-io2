(define-module (crates-io me rk merkletreers) #:use-module (crates-io))

(define-public crate-merkletreers-0.1.0 (c (n "merkletreers") (v "0.1.0") (h "1qyrwx07ywmqz98pcnyz4amdgqs7hyqr3f8mwywdx0a8ba1x0pry")))

(define-public crate-merkletreers-0.2.0 (c (n "merkletreers") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1nlrlcswld2xfwp7af1awq7j7p1grv23fxvhnr52h4chqmf1gq8p")))

(define-public crate-merkletreers-0.2.1 (c (n "merkletreers") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "15qf5j1qjnsgabiflgbgshf5m8sfsp0492n4z428yiiyigwn4iln")))

(define-public crate-merkletreers-0.3.0 (c (n "merkletreers") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1rjwh35x2mzbpdal58vdn2m435zdrsam2fmcx7wll49yzwp3ax21")))

(define-public crate-merkletreers-0.3.1 (c (n "merkletreers") (v "0.3.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "18pqzx7kabgqs4cycjrkb823sn88swpbbv1az6yjs9zc39v9k3kf")))

(define-public crate-merkletreers-0.4.0 (c (n "merkletreers") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "02730dg8yp9f1pvcw50shc66b53kd9mqc127bgz3di93396rr9f3")))

(define-public crate-merkletreers-0.4.1 (c (n "merkletreers") (v "0.4.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0y9mnmc6rgdifw645z8v86l6qc4l0vyv8c4z9yl2s8ga9ra1j4d8")))

(define-public crate-merkletreers-1.0.0 (c (n "merkletreers") (v "1.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0h8xrz23rfd68i4r8az4dyas4c8c9hmv93jr2g41ajq60m2ixwh3")))

(define-public crate-merkletreers-1.2.0 (c (n "merkletreers") (v "1.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "05wy93gn0m53dc6bk5h2y93rqgwwgpjjqqf283mlp4wd1dx8mn0d")))

