(define-module (crates-io me rk merklemountainrange) #:use-module (crates-io))

(define-public crate-merklemountainrange-0.0.1 (c (n "merklemountainrange") (v "0.0.1") (d (list (d (n "blake2") (r "^0.8.0") (d #t) (k 2)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "tari_utilities") (r "^0.0.1") (d #t) (k 0)))) (h "1ck1sna4yslpilkpbwljdz8sspcf3s8mjzfz5f04ksc0wd89n02w")))

