(define-module (crates-io me rk merkleized-metadata) #:use-module (crates-io))

(define-public crate-merkleized-metadata-0.1.0 (c (n "merkleized-metadata") (v "0.1.0") (d (list (d (n "array-bytes") (r "^6.2.2") (k 0)) (d (n "blake3") (r "^1.5.0") (k 0)) (d (n "codec") (r "^3.6.9") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-metadata") (r "^16.0.0") (f (quote ("current"))) (k 0)) (d (n "frame-metadata") (r "^16.0.0") (f (quote ("current" "decode"))) (k 2)) (d (n "scale-decode") (r "^0.13.0") (k 0)) (d (n "scale-info") (r "^2.10.0") (k 0)))) (h "0ym1lxsrmqp2vlbs2rq4ppvxaxqdagvhp87avsicljra3pzzq4zk")))

