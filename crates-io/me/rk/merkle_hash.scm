(define-module (crates-io me rk merkle_hash) #:use-module (crates-io))

(define-public crate-merkle_hash-1.0.0 (c (n "merkle_hash") (v "1.0.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1n34qzhbx1w3ss7yryyx9fjhzwbirr7rfg4wmikqx35rzd1xcq2v") (y #t)))

(define-public crate-merkle_hash-1.0.1 (c (n "merkle_hash") (v "1.0.1") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1zpd282wy889iyxn7sxj3gmf0a6zb5yzg9ycpkyv14zrl3051f9n") (y #t)))

(define-public crate-merkle_hash-1.0.2 (c (n "merkle_hash") (v "1.0.2") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "058016fnayrngj39qnjvlw7z52c0fmy0a95mq7p41fix3gxw4047") (y #t)))

(define-public crate-merkle_hash-1.0.3 (c (n "merkle_hash") (v "1.0.3") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "03hbl73x3kr66wws51h3jy1pcv3rl4c5pk1g001bgxiz37jjjb2m") (y #t)))

(define-public crate-merkle_hash-1.0.4 (c (n "merkle_hash") (v "1.0.4") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0x5gn7njxkgwr1403jckvz6irz1cd1zcilz09c17sb9sx72k5x4v") (y #t)))

(define-public crate-merkle_hash-1.0.5 (c (n "merkle_hash") (v "1.0.5") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "058mcfhai8z00pz1fdx5h58hksrff2w8z4hxwb2iv3ib0fdslnaz")))

(define-public crate-merkle_hash-1.0.6 (c (n "merkle_hash") (v "1.0.6") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1v3v3g1gixvqpfy9zlv798mbqczbsc6m2jv3wz9sxk5z7vn2ds1r") (y #t)))

(define-public crate-merkle_hash-1.1.0 (c (n "merkle_hash") (v "1.1.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1xk0bhqid5d09ql9f9yicfp9zgdmqqxxvlvbqyc40wkczw87qm43")))

(define-public crate-merkle_hash-1.1.1 (c (n "merkle_hash") (v "1.1.1") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1awqcn39wmyvss2zavkklxh6za5vd4f2hxsj8bcjlbq07jkgkiw1")))

(define-public crate-merkle_hash-2.0.0 (c (n "merkle_hash") (v "2.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0l3afmrjgslpzp6h4206wz674k86k1fpsmj6xqzh87ib7dx6q3cn")))

(define-public crate-merkle_hash-2.1.0 (c (n "merkle_hash") (v "2.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "01l6qsyrxjcw3axp4slqddmpjbrzjr011n4zb4df71zd7939qwg7")))

(define-public crate-merkle_hash-3.0.0 (c (n "merkle_hash") (v "3.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0r0s3rp0cl94479xaccg5avmbjq3my4bls97f58qpqnflkfgk320")))

(define-public crate-merkle_hash-3.0.1 (c (n "merkle_hash") (v "3.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "01sxc1g4s07vdm8phvhxnvbanwx4z3d3mgqq4hwds7jpdsw425qz")))

(define-public crate-merkle_hash-3.0.2 (c (n "merkle_hash") (v "3.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0ra8ys5wlg92apg166wis4zl6qd4r0scj3s3xdi46arp77apn13b")))

(define-public crate-merkle_hash-3.1.0 (c (n "merkle_hash") (v "3.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 0)))) (h "07laa2iz1xfjn39wynj4m6si455c5lz0xxd1jlh4zq47yas9gs2h")))

(define-public crate-merkle_hash-3.1.1 (c (n "merkle_hash") (v "3.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 0)))) (h "0vnjg63l7czy7mlrc20w3zfd6a0j84i8iaqrfs0pysn217xmr58j")))

(define-public crate-merkle_hash-3.2.0 (c (n "merkle_hash") (v "3.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (k 0)))) (h "0xxry8s3l4g7mayysw3y8azxhdkllqpij818qkn252yhjalsbyyr")))

(define-public crate-merkle_hash-3.3.0 (c (n "merkle_hash") (v "3.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (k 0)))) (h "111pnfwcb0n0hfglqsajccygdv4hni98x078j0swpjy4hghlc98j") (f (quote (("sha2-hash" "sha2")))) (y #t)))

(define-public crate-merkle_hash-3.3.1 (c (n "merkle_hash") (v "3.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (k 0)))) (h "0mddynx5a6lrng7q4b91hcg4fdhj9scw94q5g7m2c8f379y4zbyv") (f (quote (("sha2-hash" "sha2"))))))

(define-public crate-merkle_hash-3.4.0 (c (n "merkle_hash") (v "3.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (k 0)))) (h "0bsy53by0ryfizfwbzm3qmx3g39hanfyn5r1sx1sk3sx3arnlyvy") (f (quote (("sha" "sha2"))))))

(define-public crate-merkle_hash-3.5.0 (c (n "merkle_hash") (v "3.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (k 0)))) (h "0pcrfs3clap6l844hkvayldfnab9286ia6k7kda01yh7vli9q0f2") (f (quote (("sha" "sha2") ("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-merkle_hash-3.6.0 (c (n "merkle_hash") (v "3.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (k 0)))) (h "1lmswykc63fljyjixbd8896np2hi4vk56xc0rgfcy6d95xpx2yvr") (f (quote (("sha" "sha2") ("retain") ("parallel" "rayon") ("encode") ("default" "parallel" "encode"))))))

(define-public crate-merkle_hash-3.6.1 (c (n "merkle_hash") (v "3.6.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "camino") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (k 0)))) (h "1rg458za5ps0g1l1vawd5q4vf3fmqvhmx2kla4bnzakp4k2qvb8g") (f (quote (("sha" "sha2") ("retain") ("parallel" "rayon") ("encode") ("default" "parallel" "encode"))))))

