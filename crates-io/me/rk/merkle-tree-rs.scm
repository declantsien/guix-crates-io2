(define-module (crates-io me rk merkle-tree-rs) #:use-module (crates-io))

(define-public crate-merkle-tree-rs-0.1.0 (c (n "merkle-tree-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ethers") (r "^1.0.0") (d #t) (k 0)) (d (n "render-tree") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1isq8ldrshlgvvvmlsn0phmp2i728847saqv8mbv2vdzhv693jix") (r "1.65.0")))

