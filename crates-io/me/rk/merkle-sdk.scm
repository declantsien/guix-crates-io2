(define-module (crates-io me rk merkle-sdk) #:use-module (crates-io))

(define-public crate-merkle-sdk-0.0.1 (c (n "merkle-sdk") (v "0.0.1") (d (list (d (n "merkle-sdk-transactions") (r "^0.0.1") (d #t) (k 0)))) (h "00f9fsljchq2m3i1kihkz7s564savapgrqrvcc1v5giabp2dji9d")))

(define-public crate-merkle-sdk-0.0.2 (c (n "merkle-sdk") (v "0.0.2") (d (list (d (n "merkle-sdk-transactions") (r "^0.0.2") (d #t) (k 0)))) (h "1yrkqzzkgxyd4q3dldgp4v12y129r62bk311flanfp5k13lyvwb3")))

(define-public crate-merkle-sdk-0.0.3 (c (n "merkle-sdk") (v "0.0.3") (d (list (d (n "merkle-sdk-transactions") (r "^0.0.3") (d #t) (k 0)))) (h "0pa4f4xp1nv97lr0nc9lfh9wbf4zsp42b46ykinyi1jm2zg5qs62")))

(define-public crate-merkle-sdk-0.0.4 (c (n "merkle-sdk") (v "0.0.4") (d (list (d (n "merkle-sdk-transactions") (r "^0.0.3") (d #t) (k 0)))) (h "1h9dmzbhxcbglj4qfgyzjcn77dr0gjb9nch0fzjwv89sslac4dfk")))

(define-public crate-merkle-sdk-0.0.5 (c (n "merkle-sdk") (v "0.0.5") (d (list (d (n "merkle-sdk-transactions") (r "^0.0.5") (d #t) (k 0)))) (h "0hfs1yidp8r9yypm1zgi57f2qczbrg1wq42x7vai22sqw1mibryy")))

(define-public crate-merkle-sdk-0.0.6 (c (n "merkle-sdk") (v "0.0.6") (d (list (d (n "merkle-sdk-transactions") (r "^0.0.6") (d #t) (k 0)))) (h "02jlmcya8nmg8ps9gwr491ksf46mikk3lb7b57zcq4ba9p0hb9cm")))

(define-public crate-merkle-sdk-0.0.7 (c (n "merkle-sdk") (v "0.0.7") (d (list (d (n "merkle-sdk-transactions") (r "^0.0.7") (d #t) (k 0)))) (h "0cajrcnc21g3ap312jcglg9fhspr76kxninaqp7zcqn699nc7gv9")))

