(define-module (crates-io me rk merkle) #:use-module (crates-io))

(define-public crate-merkle-0.1.0 (c (n "merkle") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0rzdvdm14fb6xbhzjry6413vvair4xx4i9qrq9nhg3yd9nsqa787")))

(define-public crate-merkle-1.0.0 (c (n "merkle") (v "1.0.0") (d (list (d (n "protobuf") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.6.0-alpha") (d #t) (k 0)))) (h "04maz0f0h7g2p4ysbgg3z46jc60hvf8sqqjkpfqi5m2vp9zafck1") (f (quote (("serialization-protobuf" "protobuf"))))))

(define-public crate-merkle-1.1.0 (c (n "merkle") (v "1.1.0") (d (list (d (n "protobuf") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.6.0") (d #t) (k 0)))) (h "0nx3lfcq9yb474ydln86wsjfvs1mzvvi3s172zf5bylp486h31p4") (f (quote (("serialization-protobuf" "protobuf"))))))

(define-public crate-merkle-1.2.0 (c (n "merkle") (v "1.2.0") (d (list (d (n "protobuf") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.9") (d #t) (k 0)))) (h "1axj17jkpgnchqqs49wl9wkpl9ld6s66y8jw0s33k6nbvqrfl3rx") (f (quote (("serialization-protobuf" "protobuf"))))))

(define-public crate-merkle-1.3.0 (c (n "merkle") (v "1.3.0") (d (list (d (n "protobuf") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)))) (h "14prwr54svqz5dc36gdkvaii24yny04f2qrzxhn6rk77adlvnjr0") (f (quote (("serialization-protobuf" "protobuf"))))))

(define-public crate-merkle-1.4.0 (c (n "merkle") (v "1.4.0") (d (list (d (n "protobuf") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)))) (h "1m2s5asln6ka1b8cjng5z368xz4bxp2bl459br4ha6mnhrjdvx54") (f (quote (("serialization-protobuf" "protobuf"))))))

(define-public crate-merkle-1.4.1 (c (n "merkle") (v "1.4.1") (d (list (d (n "protobuf") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)))) (h "1pph41qkpw9dwzc4wb9pc1gia37snvnyg5s6f1yisagjc2czjrz1") (f (quote (("serialization-protobuf" "protobuf"))))))

(define-public crate-merkle-1.5.0 (c (n "merkle") (v "1.5.0") (d (list (d (n "protobuf") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.12.0") (d #t) (k 0)))) (h "1zm5yrkf5brbm56w9zc5652msj6a7w8zdifz46058waw3jm1qcd6") (f (quote (("serialization-protobuf" "protobuf"))))))

(define-public crate-merkle-1.5.1 (c (n "merkle") (v "1.5.1") (d (list (d (n "protobuf") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.12.0") (d #t) (k 0)))) (h "0378j2vpcckgznixx4sjdp5f23v4g0wl0j6axw5lgsb9mabjmjx7") (f (quote (("serialization-protobuf" "protobuf")))) (y #t)))

(define-public crate-merkle-1.6.0 (c (n "merkle") (v "1.6.0") (d (list (d (n "protobuf") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.12.0") (d #t) (k 0)))) (h "1wwxk9hw0cy45x8fj1jzdjdgip0h9mjk4n8x54a3rdxaffqvhsgj") (f (quote (("serialization-protobuf" "protobuf"))))))

(define-public crate-merkle-1.7.0 (c (n "merkle") (v "1.7.0") (d (list (d (n "protobuf") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 2)))) (h "01cax2x1xf63x94a39nwjnadhpds6yl0w941jmigbgnd682yfy92") (f (quote (("serialization-serde" "serde" "serde_derive") ("serialization-protobuf" "protobuf"))))))

(define-public crate-merkle-1.8.0 (c (n "merkle") (v "1.8.0") (d (list (d (n "protobuf") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0.2") (o #t) (d #t) (k 1)) (d (n "ring") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 2)))) (h "0hn8vn4cq61i9mrnxapz2zn8bff92v18zivbrkwdfbkbg38mz35v") (f (quote (("serialization-serde" "serde" "serde_derive") ("serialization-protobuf" "protobuf" "protoc-rust"))))))

(define-public crate-merkle-1.9.0 (c (n "merkle") (v "1.9.0") (d (list (d (n "protobuf") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0.2") (o #t) (d #t) (k 1)) (d (n "ring") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 2)))) (h "0j8v475nigci357p78gl65brbnccv8y7s8685qibxrf6h7xgn37q") (f (quote (("serialization-serde" "serde" "serde_derive") ("serialization-protobuf" "protobuf" "protoc-rust"))))))

(define-public crate-merkle-1.10.0 (c (n "merkle") (v "1.10.0") (d (list (d (n "protobuf") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0.2") (o #t) (d #t) (k 1)) (d (n "ring") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 2)))) (h "0yls1qzj35zprcy6p7fndixm455afn72jribw75bc7kb7njxzkin") (f (quote (("serialization-serde" "serde" "serde_derive") ("serialization-protobuf" "protobuf" "protoc-rust"))))))

(define-public crate-merkle-1.11.0 (c (n "merkle") (v "1.11.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "protobuf") (r "^2.8.0") (o #t) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.8.0") (o #t) (d #t) (k 1)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "ring") (r "^0.16.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)))) (h "0i9ndfhfp2mbz9q5g0yfb8iyrljvyyhg8axahq3qfymwwiqq47fn") (f (quote (("serialization-serde" "serde" "serde_derive") ("serialization-protobuf" "protobuf" "protoc-rust"))))))

