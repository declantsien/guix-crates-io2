(define-module (crates-io me rk merkle_sigs) #:use-module (crates-io))

(define-public crate-merkle_sigs-1.1.0 (c (n "merkle_sigs") (v "1.1.0") (d (list (d (n "lamport_sigs") (r "^0.2.0") (d #t) (k 0)) (d (n "merkle") (r "^1.1.0") (f (quote ("serialization-protobuf"))) (d #t) (k 0)) (d (n "ring") (r "^0.9.7") (d #t) (k 0)))) (h "141kla8g6m9xh0bgndckdrqdpqx1b8d2fjgl19jzy94znp706za7")))

(define-public crate-merkle_sigs-1.2.1 (c (n "merkle_sigs") (v "1.2.1") (d (list (d (n "lamport_sigs") (r "^0.3.0") (d #t) (k 0)) (d (n "merkle") (r "^1.3.0") (f (quote ("serialization-protobuf"))) (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)))) (h "0m9j9lwp8vi04dn9pv9k4x8nrr3rpx2bvimncjvrs2h6k9ysaiml")))

(define-public crate-merkle_sigs-1.3.0 (c (n "merkle_sigs") (v "1.3.0") (d (list (d (n "lamport_sigs") (r "^0.4.0") (d #t) (k 0)) (d (n "merkle") (r "^1.4.0") (f (quote ("serialization-protobuf"))) (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)))) (h "16gdga9y7fdhdmsvdhk50bkdz3zfa93b9vs49k53s6k0sd2cq8sq")))

(define-public crate-merkle_sigs-1.4.0 (c (n "merkle_sigs") (v "1.4.0") (d (list (d (n "lamport_sigs") (r "^0.5.0") (d #t) (k 0)) (d (n "merkle") (r "^1.5.0") (f (quote ("serialization-protobuf"))) (d #t) (k 0)) (d (n "ring") (r "^0.12.0") (d #t) (k 0)))) (h "09lzwbn77zgc8yf8d0jraacg83fsaxq0lzf650f7hfwx05j7hdll")))

(define-public crate-merkle_sigs-1.5.0 (c (n "merkle_sigs") (v "1.5.0") (d (list (d (n "lamport_sigs") (r "^0.5.0") (d #t) (k 0)) (d (n "merkle") (r "^1.7.0") (f (quote ("serialization-protobuf"))) (d #t) (k 0)) (d (n "ring") (r "^0.12.0") (d #t) (k 0)))) (h "0258w6637fx7lhg56vn2famfbd5z19byixhncdjag39kl9abi6gm")))

(define-public crate-merkle_sigs-1.6.0 (c (n "merkle_sigs") (v "1.6.0") (d (list (d (n "lamport_sigs") (r "^0.6.0") (d #t) (k 0)) (d (n "merkle") (r "^1.10.0") (f (quote ("serialization-protobuf"))) (d #t) (k 0)) (d (n "ring") (r "^0.13.0") (d #t) (k 0)))) (h "06z0ln17nz1m1frkkk8nvbq5zljz7wjcrvpvlrns5vw8x6j59xlb")))

(define-public crate-merkle_sigs-1.7.0 (c (n "merkle_sigs") (v "1.7.0") (d (list (d (n "lamport_sigs") (r "^0.7.0") (d #t) (k 0)) (d (n "merkle") (r "^1.11.0") (f (quote ("serialization-protobuf"))) (d #t) (k 0)) (d (n "ring") (r "^0.16.1") (d #t) (k 0)))) (h "1bx3b30s09547nszv84nm59mqwwvjglj5a6qrnvbscwbhgbkyss1")))

