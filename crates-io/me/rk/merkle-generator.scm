(define-module (crates-io me rk merkle-generator) #:use-module (crates-io))

(define-public crate-merkle-generator-0.1.0 (c (n "merkle-generator") (v "0.1.0") (d (list (d (n "flat-tree") (r "^1.0.0") (d #t) (k 0)) (d (n "ring") (r "^0.6.2") (d #t) (k 0)))) (h "1k0z5x8f4xgcj498qk0m6a3ry500pvxg5z3r8bsla2qw63wwfr9f")))

(define-public crate-merkle-generator-0.1.1 (c (n "merkle-generator") (v "0.1.1") (d (list (d (n "flat-tree") (r "^1.0.0") (d #t) (k 0)) (d (n "ring") (r "^0.6.2") (d #t) (k 0)))) (h "1607ai6bx38qlw0yqgm6zrfngh17rwq5n4qc95dvq0q2383zx6f7")))

(define-public crate-merkle-generator-0.1.2 (c (n "merkle-generator") (v "0.1.2") (d (list (d (n "flat-tree") (r "^1.0.0") (d #t) (k 0)) (d (n "ring") (r "^0.6.2") (d #t) (k 0)))) (h "1ggc4k8sgm1d5fx3690jrli6dww59p7pvxal9m90wldpwir79yqa")))

(define-public crate-merkle-generator-0.2.0 (c (n "merkle-generator") (v "0.2.0") (d (list (d (n "flat-tree") (r "^1.0.0") (d #t) (k 0)) (d (n "ring") (r "^0.9.4") (d #t) (k 0)))) (h "0a6ynnz4fvps2wlkvqjn4aa15vdx6xmrx7wsja2q6mm0703d9rvb")))

