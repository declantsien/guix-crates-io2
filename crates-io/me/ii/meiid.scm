(define-module (crates-io me ii meiid) #:use-module (crates-io))

(define-public crate-meiid-0.1.0 (c (n "meiid") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c94g93qh1bcmr9z3n92gpjr2mi1k34jqaj32bkfl44adwd8pl1g") (y #t)))

(define-public crate-meiid-0.1.1 (c (n "meiid") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00k902p346486wxjm231ap2j3g7iaynwcy8w1m0k68h8xphglyvb")))

(define-public crate-meiid-0.1.2 (c (n "meiid") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nj7ji35xfbakzzd5l821iz4l6vbfjsbwzhvadh7fchl3y7qdjgf")))

