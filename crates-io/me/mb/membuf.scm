(define-module (crates-io me mb membuf) #:use-module (crates-io))

(define-public crate-membuf-0.0.1 (c (n "membuf") (v "0.0.1") (h "0l7gnbcd1z93dgs29x2iav74094b6xwksszxjq50v5n03m4r2wa3")))

(define-public crate-membuf-0.0.2 (c (n "membuf") (v "0.0.2") (h "1bgzjrxa8z5bmyrnwwqa9wi28rx82714m0mw0lsac1c0nscq249z")))

(define-public crate-membuf-0.0.3 (c (n "membuf") (v "0.0.3") (h "0vwf6z2m1bs3s5x1rkgb06mpnb0nh31iim142zx2c8zf9xcmih57")))

(define-public crate-membuf-0.0.4 (c (n "membuf") (v "0.0.4") (h "1vbgzbba24l5x4zk7x8i9qi9yr5x6nxv2lfmkwxrpb87kps2f5l9")))

(define-public crate-membuf-0.0.5 (c (n "membuf") (v "0.0.5") (h "1q77fc4k69iba2jm47rrz4xwyrsdpgcgsr0ldkw118fw1m0syqik")))

