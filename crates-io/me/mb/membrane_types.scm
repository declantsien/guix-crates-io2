(define-module (crates-io me mb membrane_types) #:use-module (crates-io))

(define-public crate-membrane_types-0.1.0 (c (n "membrane_types") (v "0.1.0") (h "1am0yxqa585gxqf5glbjzix77l8vwymk7q23jaqyvdsd3rm79s40") (y #t)))

(define-public crate-membrane_types-0.3.0 (c (n "membrane_types") (v "0.3.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16awddffq7a46kxflk48nnqsgg9x4p3y6vckw2w3ipvllbn20l73")))

(define-public crate-membrane_types-0.4.0 (c (n "membrane_types") (v "0.4.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08pgr2c9ppqzjgkdcg72748jzrrrkmm25hrricidv890mzrh3w1c")))

(define-public crate-membrane_types-0.5.0 (c (n "membrane_types") (v "0.5.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0baqz5rjcqlh1xy6r2qlgwim724mkfclip8g0dlnbg88yc2qbb3a")))

(define-public crate-membrane_types-0.5.1 (c (n "membrane_types") (v "0.5.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m86hcvhcbnmnnzxhzqhh8lm3rzm0063gvbikzmr23j2hsaddqrb")))

