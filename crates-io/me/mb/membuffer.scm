(define-module (crates-io me mb membuffer) #:use-module (crates-io))

(define-public crate-membuffer-0.1.0 (c (n "membuffer") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)))) (h "14bpvgs6vkk51v5gqjvjk069g43xkkp7bd96mzmm6471232vsvfc")))

(define-public crate-membuffer-0.1.1 (c (n "membuffer") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fghndbklnaz5sl9ck6ap4rh02ib74ymhk2kqa6vfwkkckj6mh97") (f (quote (("bench"))))))

(define-public crate-membuffer-0.2.0 (c (n "membuffer") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v65nxb8s76p1ghzvb73734w5q2iczzq7zg1n0q175hhkjx7py2n") (f (quote (("bench"))))))

(define-public crate-membuffer-0.3.0 (c (n "membuffer") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rsiv73j0r8g5hki1ly67l3g63l68s5ncp6jhblqvqn143wkv23h") (f (quote (("bench"))))))

