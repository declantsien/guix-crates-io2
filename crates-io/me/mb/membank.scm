(define-module (crates-io me mb membank) #:use-module (crates-io))

(define-public crate-membank-0.1.0 (c (n "membank") (v "0.1.0") (h "1n85l2c75mzhhl47am49ykjm7wfi8mg7ib0disxlld1sqyc64965") (f (quote (("std") ("default" "std"))))))

(define-public crate-membank-0.2.0 (c (n "membank") (v "0.2.0") (h "0pc4c0bzpwy25p53q4jppc3gp8mm92imk3k9npw0d0203pcg2j7d") (f (quote (("std") ("default" "std"))))))

(define-public crate-membank-0.3.0 (c (n "membank") (v "0.3.0") (h "104ixajy9m75ch49c3w94rk8f51h1kn8jza2rr3r4pnvy5sc5in6") (f (quote (("std") ("default" "std"))))))

(define-public crate-membank-0.4.0 (c (n "membank") (v "0.4.0") (h "02wvxkxbqchry2ygz0d9flah2a1x55bddlzr3fvmjgjnfqf7bfjq") (f (quote (("std") ("default" "std"))))))

(define-public crate-membank-0.4.1 (c (n "membank") (v "0.4.1") (h "1a2mp6fcpclhjjhg0fcnh4hnnzp3nampzaflg1nza45czzmy1yjj") (f (quote (("std") ("default" "std"))))))

(define-public crate-membank-0.5.0 (c (n "membank") (v "0.5.0") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (f (quote ("std"))) (k 0)))) (h "044ng4fib82mxglrqm46pfnbp3krlgxgf06mg8dgqrx5gp9sm4rj") (f (quote (("std" "crossbeam-channel/std") ("default" "std"))))))

(define-public crate-membank-0.5.1 (c (n "membank") (v "0.5.1") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (f (quote ("std"))) (k 0)))) (h "00c6g5hfywr3va30ypb4353v2rq2kivvaf4fyh9c7hlydfl3010g") (f (quote (("std" "crossbeam-channel/std") ("default" "std"))))))

