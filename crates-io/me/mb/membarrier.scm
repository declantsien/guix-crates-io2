(define-module (crates-io me mb membarrier) #:use-module (crates-io))

(define-public crate-membarrier-0.1.0 (c (n "membarrier") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i589sd4nlar0rdpf1cswac2yj0i5jlk7pll79063iny1bnms99v") (f (quote (("linux_membarrier") ("default"))))))

(define-public crate-membarrier-0.2.0 (c (n "membarrier") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "192207iwmsgjpx84gg53jf0g23pjgzsd6vfxpd6p9zq29a60h67s")))

(define-public crate-membarrier-0.2.1 (c (n "membarrier") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08600l3ykx47b85sganx0fq2imsh2x88qgiwdal9wa6hn63ybabz")))

(define-public crate-membarrier-0.2.2 (c (n "membarrier") (v "0.2.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "072vjmfdr0hggcdxbi7dg2mkmhkygc2ca1jrddk2pyz5sw8hhnwj")))

(define-public crate-membarrier-0.2.3 (c (n "membarrier") (v "0.2.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_System_Threading"))) (d #t) (k 0)))) (h "045g50pjalv6z8x7cvcndv5z3vxy03cgpddwrj09lk8887qdc4a3")))

