(define-module (crates-io me mb memblock) #:use-module (crates-io))

(define-public crate-memblock-0.1.0 (c (n "memblock") (v "0.1.0") (h "1f00fnwg1k8g75hsh2hcpzdy6h0pv0933wv7fyp2lxkyqkmhamqj") (y #t)))

(define-public crate-memblock-0.1.1 (c (n "memblock") (v "0.1.1") (h "0z2i6fz7b2ar6fx5wz31vp9vw4d5yhly6mp4jy615mnxjia42d6y")))

