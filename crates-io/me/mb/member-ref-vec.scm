(define-module (crates-io me mb member-ref-vec) #:use-module (crates-io))

(define-public crate-member-ref-vec-0.1.0 (c (n "member-ref-vec") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.7") (d #t) (k 2)))) (h "052l1w0anhzspwjm8fvz5warzgi3bmjwkak3rwlhnqkd4ps7nq1b") (y #t)))

(define-public crate-member-ref-vec-0.1.1 (c (n "member-ref-vec") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.7") (d #t) (k 2)))) (h "0p6bzaj8vssmxhlcnxvz857nn9w26x41syi32l3ljg4shpmh1k78") (y #t)))

(define-public crate-member-ref-vec-0.1.2 (c (n "member-ref-vec") (v "0.1.2") (d (list (d (n "smallvec") (r "^1.7") (d #t) (k 2)))) (h "0fm26z1ixjb573pk7kphk5682cavw9wpqy3w2mgr24dfpw8rw7nc") (y #t)))

(define-public crate-member-ref-vec-0.1.3 (c (n "member-ref-vec") (v "0.1.3") (d (list (d (n "smallvec") (r "^1.7") (d #t) (k 2)))) (h "0wyviipxvkhf0l9lphh64w845bc79jnmmffx1axzm5f3lapk1c65") (y #t)))

(define-public crate-member-ref-vec-0.1.4 (c (n "member-ref-vec") (v "0.1.4") (d (list (d (n "smallvec") (r "^1.7") (d #t) (k 2)))) (h "0axmiigdiyqxslpfp37riv3wc0pffm6zdcj0qyrrcrxjsv5vxi7d") (y #t)))

(define-public crate-member-ref-vec-0.1.5 (c (n "member-ref-vec") (v "0.1.5") (d (list (d (n "smallvec") (r "^1.7") (d #t) (k 2)))) (h "153rmhlv0ak1qxdvp2vml00f0jfgx2j3m4x59kqhhd39hw4qh7yi") (y #t)))

