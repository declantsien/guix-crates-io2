(define-module (crates-io me mb membership-token-client) #:use-module (crates-io))

(define-public crate-membership-token-client-0.1.4 (c (n "membership-token-client") (v "0.1.4") (d (list (d (n "ic-cdk") (r "^0.3.1") (d #t) (k 0)) (d (n "ic-event-hub") (r "^0.1.10") (d #t) (k 0)) (d (n "ic-event-hub-macros") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1ypch5l8vagv1f6k6zngy0i6y5y3bv2nhrpdcv5p66m0zi30ja8s")))

