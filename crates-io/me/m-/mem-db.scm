(define-module (crates-io me m- mem-db) #:use-module (crates-io))

(define-public crate-mem-db-0.0.0 (c (n "mem-db") (v "0.0.0") (h "0q030w6xny2aivmi1gj1waprscs5fpcy374x5a1qbfiq7f8cncid")))

(define-public crate-mem-db-0.0.1 (c (n "mem-db") (v "0.0.1") (h "1z16885l4vdmcwp8vkjpfcj2qapqc2hdyhkpd99qkffcygb119xc")))

(define-public crate-mem-db-0.0.2 (c (n "mem-db") (v "0.0.2") (h "0nxbg1fcx22izasd48rdf3cvlw0p8xxa24qki21gmqa950rwdkpy")))

(define-public crate-mem-db-0.0.3 (c (n "mem-db") (v "0.0.3") (h "0f6b0c50pc6krr98sw70xsjksqzb1iqbz951rb67lyqbbykql5fc")))

