(define-module (crates-io me m- mem-query) #:use-module (crates-io))

(define-public crate-mem-query-0.0.1 (c (n "mem-query") (v "0.0.1") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tylisp") (r "^0.1.0") (d #t) (k 0)))) (h "0barad02zq35qdblnlxdykxpmncv292xwhwswwr852rhzimyynd1") (f (quote (("slow_tests") ("const" "tylisp/const"))))))

