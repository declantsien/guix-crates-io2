(define-module (crates-io me m- mem-aead-mrs) #:use-module (crates-io))

(define-public crate-mem-aead-mrs-0.1.0 (c (n "mem-aead-mrs") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "subtle") (r "^0.6") (k 0)))) (h "049vlirvmak0ql7mcdf0j1z667289i42jbs4drn5nwdiflwi0vh2")))

(define-public crate-mem-aead-mrs-0.1.1 (c (n "mem-aead-mrs") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "0di4335rk9vpjfdvmzj29i7hxp5zaibk1l2zagkaln9rkm3x8211")))

