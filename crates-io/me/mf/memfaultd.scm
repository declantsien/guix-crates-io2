(define-module (crates-io me mf memfaultd) #:use-module (crates-io))

(define-public crate-memfaultd-0.0.0 (c (n "memfaultd") (v "0.0.0") (h "1j0m6ra1ysdxkgybk9liklrycrn89vmrvzim3zlw9m0hnymh5d0q") (y #t)))

(define-public crate-memfaultd-0.0.1 (c (n "memfaultd") (v "0.0.1") (h "0p0f0zli7a1rvgk9ip7z16slsbjb6wzpdwlkf2wfdjf3vsa4kd02")))

