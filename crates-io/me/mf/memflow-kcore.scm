(define-module (crates-io me mf memflow-kcore) #:use-module (crates-io))

(define-public crate-memflow-kcore-0.2.0-beta5 (c (n "memflow-kcore") (v "0.2.0-beta5") (d (list (d (n "goblin") (r "^0.3") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta7") (d #t) (k 0)))) (h "1m9k3az5gj742qgyv8wvw2fag743c4zzjfn50pqj4j2xx3zmc2cc") (f (quote (("plugins" "memflow/plugins"))))))

(define-public crate-memflow-kcore-0.2.0-beta11 (c (n "memflow-kcore") (v "0.2.0-beta11") (d (list (d (n "goblin") (r "^0.7") (d #t) (k 0)) (d (n "memflow") (r "=0.2.0-beta11") (d #t) (k 0)))) (h "15xqwnc61m7x1xgd3l5nxfc5w2zmf5slvw8jxvam67d68cy6y15y") (f (quote (("plugins" "memflow/plugins"))))))

(define-public crate-memflow-kcore-0.2.0 (c (n "memflow-kcore") (v "0.2.0") (d (list (d (n "goblin") (r "^0.7") (d #t) (k 0)) (d (n "memflow") (r "^0.2") (d #t) (k 0)))) (h "0faqgd3r5iifr2k47g5y6cb0jybh24q5vh5cvgcl2m8mg4fwsvp6") (f (quote (("plugins" "memflow/plugins"))))))

