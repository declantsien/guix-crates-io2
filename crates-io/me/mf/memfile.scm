(define-module (crates-io me mf memfile) #:use-module (crates-io))

(define-public crate-memfile-0.1.0 (c (n "memfile") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)))) (h "1fb7s3g0jwww3rbsb4si0h9m0ib7krv6pkg2l1n9arfmnjrchbsf")))

(define-public crate-memfile-0.1.1 (c (n "memfile") (v "0.1.1") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)))) (h "15mlvwq6725k16wba2rf1zx20anz4z918n19s5glp0kd60ibdki8")))

(define-public crate-memfile-0.1.2 (c (n "memfile") (v "0.1.2") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)))) (h "1qwd06ag5v2gcsmhl8w3qh1i7b6nh5vgrxfrcc7xm2qzrrvk47ji")))

(define-public crate-memfile-0.1.3 (c (n "memfile") (v "0.1.3") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)))) (h "1q3j1naqjw742azxk43xjr63js142fyzrxd0k36b0l7p3k99w8ll")))

(define-public crate-memfile-0.2.0 (c (n "memfile") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.84") (d #t) (k 0)))) (h "112d81g4ww508vgcj728xvmyq42xskjlly690dxp7g32arryqdg7")))

(define-public crate-memfile-0.2.1 (c (n "memfile") (v "0.2.1") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.105") (d #t) (k 0)))) (h "1dzxrz37vxds9ax4w4sxcyhdpws7wc3spiki12smnqp5r9frr618")))

(define-public crate-memfile-0.3.0 (c (n "memfile") (v "0.3.0") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.105") (d #t) (k 0)))) (h "0w5nzkk7pfyqpjlz1d8g94ki3gkb55pw3ihp5sni9jmgsln7bwkc")))

(define-public crate-memfile-0.3.1 (c (n "memfile") (v "0.3.1") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.105") (d #t) (k 0)))) (h "0x77v4x7v4q3xcdccavfck20jgnb8dap7lccax4p3wklpygms6f3")))

(define-public crate-memfile-0.3.2 (c (n "memfile") (v "0.3.2") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "libc") (r "^0.2.105") (d #t) (k 0)))) (h "03nljyc7603hsav18ng24lgafxmv7lgjii10z500fpssnvykcipn")))

