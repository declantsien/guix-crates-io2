(define-module (crates-io me mf memflow-kvm) #:use-module (crates-io))

(define-public crate-memflow-kvm-0.0.0 (c (n "memflow-kvm") (v "0.0.0") (h "00rl5xixsh37wzmwgysba2kwdib57fqc1bbl72wy5r5dw5fsnhyr")))

(define-public crate-memflow-kvm-0.1.0 (c (n "memflow-kvm") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "memflow-derive") (r "^0.1") (d #t) (k 0)) (d (n "memflow-kvm-ioctl") (r "^0.1") (d #t) (k 0)))) (h "1470n9c2fwx878fs65bwy0fc4kgxii337cgkik6fy46jdz3zanib") (f (quote (("inventory") ("default"))))))

(define-public crate-memflow-kvm-0.1.1 (c (n "memflow-kvm") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "memflow-derive") (r "^0.1") (d #t) (k 0)) (d (n "memflow-kvm-ioctl") (r "^0.1") (d #t) (k 0)))) (h "16md8k030344f9jpjlbp4makwg6m31bj5wvlp3h3pircpa8gn724") (f (quote (("inventory") ("default"))))))

(define-public crate-memflow-kvm-0.2.0-beta1 (c (n "memflow-kvm") (v "0.2.0-beta1") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "^0.2.0-beta") (f (quote ("plugins"))) (d #t) (k 0)) (d (n "memflow-kvm-ioctl") (r "^0.1") (d #t) (k 0)))) (h "1lrz35sf60vvzbllf7dcg3i4yhsl8x2k6iacq4y59ldqwdf5qj6s") (f (quote (("inventory") ("default"))))))

(define-public crate-memflow-kvm-0.2.0-beta2 (c (n "memflow-kvm") (v "0.2.0-beta2") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "^0.2.0-beta") (f (quote ("plugins"))) (d #t) (k 0)) (d (n "memflow-kvm-ioctl") (r "^0.1") (d #t) (k 0)))) (h "1bh1hd55grdvfbzx6shyhfnr81pcdf534rsgpdwv4f5xaa822p7w") (f (quote (("inventory") ("default"))))))

(define-public crate-memflow-kvm-0.2.0-beta7 (c (n "memflow-kvm") (v "0.2.0-beta7") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "^0.2.0-beta7") (f (quote ("plugins"))) (d #t) (k 0)) (d (n "memflow-kvm-ioctl") (r "^0.1") (d #t) (k 0)))) (h "0f7s5v4cz4zidr3g4nqvkbnslmhzxp3gy7hxsfc8vvkyjqg6dxsq") (f (quote (("inventory") ("default"))))))

(define-public crate-memflow-kvm-0.2.0-beta10 (c (n "memflow-kvm") (v "0.2.0-beta10") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "=0.2.0-beta10") (f (quote ("plugins"))) (d #t) (k 0)) (d (n "memflow-kvm-ioctl") (r "^0.1.1") (d #t) (k 0)))) (h "16550ml44xca8ir86m7z1pjamhb4gzw1axnz02w3mani7bpmw9s3") (f (quote (("inventory") ("default")))) (r "1.65.0")))

(define-public crate-memflow-kvm-0.2.0-beta11 (c (n "memflow-kvm") (v "0.2.0-beta11") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "=0.2.0-beta11") (f (quote ("plugins"))) (d #t) (k 0)) (d (n "memflow-kvm-ioctl") (r "^0.1.1") (d #t) (k 0)))) (h "0cjryhwq9m074qph1jvbj3cl4iyf2cbrqvdj9lx4rdpsvyb64hki") (f (quote (("inventory") ("default")))) (r "1.65.0")))

(define-public crate-memflow-kvm-0.2.0 (c (n "memflow-kvm") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "^0.2") (f (quote ("plugins"))) (d #t) (k 0)) (d (n "memflow-kvm-ioctl") (r "^0.1.1") (d #t) (k 0)))) (h "0zlfiqapi8wcqispjbn3f141vfh2izdscgpb67lyga3qs9swzl3b") (f (quote (("inventory") ("default")))) (r "1.65.0")))

