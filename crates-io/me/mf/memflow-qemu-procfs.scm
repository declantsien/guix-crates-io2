(define-module (crates-io me mf memflow-qemu-procfs) #:use-module (crates-io))

(define-public crate-memflow-qemu-procfs-0.0.0 (c (n "memflow-qemu-procfs") (v "0.0.0") (h "00z3rd95f0kxabyfmg09v7s80fz6gpms2i1r299y4az3hvx42g2q")))

(define-public crate-memflow-qemu-procfs-0.1.0 (c (n "memflow-qemu-procfs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "^0.1") (f (quote ("inventory"))) (d #t) (k 0)) (d (n "memflow-derive") (r "^0.1") (d #t) (k 0)) (d (n "procfs") (r "^0.7") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "08nl7kvsankn60fflbazgjd3z33sf9bs87i7dvd3z2pbm9ss2djb") (f (quote (("inventory") ("default"))))))

(define-public crate-memflow-qemu-procfs-0.1.1 (c (n "memflow-qemu-procfs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "^0.1") (f (quote ("inventory"))) (d #t) (k 0)) (d (n "memflow-derive") (r "^0.1") (d #t) (k 0)) (d (n "procfs") (r "^0.7") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 2)))) (h "1qfwbax7kj7pj9z47pjxy3x69v0vfi1ghf38iin76k5wrdfh27nz") (f (quote (("inventory") ("default"))))))

