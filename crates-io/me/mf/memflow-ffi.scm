(define-module (crates-io me mf memflow-ffi) #:use-module (crates-io))

(define-public crate-memflow-ffi-0.0.0 (c (n "memflow-ffi") (v "0.0.0") (h "1m3pisrnvj0iz4rrgm44rgv0ynaswsjrdjjvyhi029wqx9zh1m3i")))

(define-public crate-memflow-ffi-0.1.0 (c (n "memflow-ffi") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 0)))) (h "1b9052w5nrnqmafc68z479hk7madl0xdajr4yxmramhdpvsf2gp1") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.1.1 (c (n "memflow-ffi") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.0") (d #t) (k 0)))) (h "12aa3srpay9qxrfyrf45329y04p5k8fbcfjnkssqpn9cgmhvcmqf") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.1.3 (c (n "memflow-ffi") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.9") (d #t) (k 0)))) (h "1ifmp6lzmrgcnnj4vgwnlhhch1f4b7zmh9fcgpb946dnmicka8x3") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.1.4 (c (n "memflow-ffi") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.9") (d #t) (k 0)))) (h "126pbj50mhxisshf8i4h0jb41crmfya9hvypnf7vcq78jrv72ysb") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.1.5 (c (n "memflow-ffi") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "simple_logger") (r "^1.9") (d #t) (k 0)))) (h "11qqg87ywhsjyqqd4rhc3s4iba260bmjhfi78klm9xmw3c9npnib") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.2.0-beta1 (c (n "memflow-ffi") (v "0.2.0-beta1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta1") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.1") (d #t) (k 0)))) (h "1za1rdxzxs6x3fprhmzjh56pzjvr1rqa7s1l303cls4kfkybzw0s") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.2.0-beta2 (c (n "memflow-ffi") (v "0.2.0-beta2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.1") (d #t) (k 0)))) (h "1qhwhy1d7phzrx1d8r1mg8mnnvfd8gfhhqnbkd8wdfvicz7269qy") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.2.0-beta4 (c (n "memflow-ffi") (v "0.2.0-beta4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.1") (d #t) (k 0)))) (h "1i8y3z783rlz673fqz356x7miwc6bcb7gh4pb73jbkrp2g5gjhjd") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.2.0-beta5 (c (n "memflow-ffi") (v "0.2.0-beta5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.1") (d #t) (k 0)))) (h "0fbz7mqfxqxl6hhacs637nl5x81ig4cfmwl7vrwfwag5ds6sdry4") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.2.0-beta6 (c (n "memflow-ffi") (v "0.2.0-beta6") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta6") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.1") (d #t) (k 0)))) (h "1b60malgv31hdsa12dkaw8dz8d8m12p2l62mm1bn9n70am07p12c") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.2.0-beta7 (c (n "memflow-ffi") (v "0.2.0-beta7") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta7") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.1") (d #t) (k 0)))) (h "1d3ddhz45n6fgvrb07fxdnlfq6kci8cygx43j2h34d6qwnm1cm5s") (f (quote (("default")))) (y #t)))

(define-public crate-memflow-ffi-0.2.0-beta8 (c (n "memflow-ffi") (v "0.2.0-beta8") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta8") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "08vmyq2jjx17nvymgh86n5j08p1g02mvs1nv1hjllp7jy2rihcfs") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.2.0-beta10 (c (n "memflow-ffi") (v "0.2.0-beta10") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memflow") (r "^0.2.0-beta10") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "0jrrxmdxvy046qbdv6yhfphr5762513zid3n0mhr0ghnshxhgl5d") (f (quote (("default"))))))

(define-public crate-memflow-ffi-0.2.0 (c (n "memflow-ffi") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "memflow") (r "^0.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)))) (h "05ifk0dw6ikgd0sm55xzy95gks8xqysb64imydih0w238nkvh31a") (f (quote (("default"))))))

