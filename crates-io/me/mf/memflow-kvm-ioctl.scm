(define-module (crates-io me mf memflow-kvm-ioctl) #:use-module (crates-io))

(define-public crate-memflow-kvm-ioctl-0.1.0 (c (n "memflow-kvm-ioctl") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1xzcmlqz4sjvqh32wrh9lqs9bm1y9qnw6n3knxqif0scwrwr72bg")))

(define-public crate-memflow-kvm-ioctl-0.1.1 (c (n "memflow-kvm-ioctl") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0q2m5dzfhnv3qhymvhbyrpq6nxl1w9knbbqkc1h7vnvi3vpv8wia")))

(define-public crate-memflow-kvm-ioctl-0.1.2 (c (n "memflow-kvm-ioctl") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "1q7mwwfaw4kpxj0ab774yjkgrgc9lprlpr8fspc6bqwgfx77vmy3")))

