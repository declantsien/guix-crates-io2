(define-module (crates-io me mf memflow-win32-ffi) #:use-module (crates-io))

(define-public crate-memflow-win32-ffi-0.0.0 (c (n "memflow-win32-ffi") (v "0.0.0") (h "02r916gm4k55p7p1w2238jkyfzmnzf0cgz37f476jid76h12xrzz")))

(define-public crate-memflow-win32-ffi-0.1.1 (c (n "memflow-win32-ffi") (v "0.1.1") (d (list (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "memflow-ffi") (r "^0.1") (d #t) (k 0)) (d (n "memflow-win32") (r "^0.1") (d #t) (k 0)))) (h "079ybyqmzq0salrjcfldy4m30gv6fa1fkb3pwvi1v1b01qrki3d7")))

(define-public crate-memflow-win32-ffi-0.1.3 (c (n "memflow-win32-ffi") (v "0.1.3") (d (list (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "memflow-ffi") (r "^0.1") (d #t) (k 0)) (d (n "memflow-win32") (r "^0.1") (d #t) (k 0)))) (h "10k3winlgci6jkkx7c24hn572fr7jjlhmw5az546yhv660a36sax")))

(define-public crate-memflow-win32-ffi-0.1.4 (c (n "memflow-win32-ffi") (v "0.1.4") (d (list (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "memflow-ffi") (r "^0.1") (d #t) (k 0)) (d (n "memflow-win32") (r "^0.1") (d #t) (k 0)))) (h "0wsajgpv3b6djqqhk4p35vg8m9sckqfh8vbq1wq58fj804z21qfb")))

(define-public crate-memflow-win32-ffi-0.1.5 (c (n "memflow-win32-ffi") (v "0.1.5") (d (list (d (n "memflow") (r "^0.1") (d #t) (k 0)) (d (n "memflow-ffi") (r "^0.1") (d #t) (k 0)) (d (n "memflow-win32") (r "^0.1") (d #t) (k 0)))) (h "0p13a8x8d3jkg9a6yl75jm96fm49hmpvd3mrnb8k25sdhz5k98aa")))

