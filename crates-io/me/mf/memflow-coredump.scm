(define-module (crates-io me mf memflow-coredump) #:use-module (crates-io))

(define-public crate-memflow-coredump-0.0.0 (c (n "memflow-coredump") (v "0.0.0") (h "19zrrk9vr4c21as1gxp82dgkkrli2cl68py42a2n3azg9cmaz6b2")))

(define-public crate-memflow-coredump-0.1.1 (c (n "memflow-coredump") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "dataview") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "memflow") (r "^0.1") (f (quote ("inventory" "filemap"))) (d #t) (k 0)) (d (n "memflow-derive") (r "^0.1") (d #t) (k 0)) (d (n "memflow-win32") (r "^0.1") (d #t) (k 2)) (d (n "simple_logger") (r "^1.0.1") (d #t) (k 2)))) (h "14iajgqggvqwzz5bfdghjinznzcfg07n634vr1sawf73gffrnvnd") (f (quote (("inventory") ("filemap") ("default" "filemap"))))))

(define-public crate-memflow-coredump-0.2.0-beta3 (c (n "memflow-coredump") (v "0.2.0-beta3") (d (list (d (n "log") (r "^0.4.14") (k 0)) (d (n "memflow") (r "^0.2.0-beta") (f (quote ("plugins" "filemap"))) (d #t) (k 0)) (d (n "memflow-win32") (r "^0.2.0-beta") (d #t) (k 2)) (d (n "simplelog") (r "^0.11.1") (d #t) (k 2)))) (h "0nqibh5v5ryj67cdckcki64pns3qzsdrwm1vn9h40703l2aky522") (f (quote (("inventory") ("filemap") ("default" "filemap"))))))

(define-public crate-memflow-coredump-0.2.0-beta7 (c (n "memflow-coredump") (v "0.2.0-beta7") (d (list (d (n "log") (r "^0.4.14") (k 0)) (d (n "memflow") (r "^0.2.0-beta7") (f (quote ("plugins" "filemap"))) (d #t) (k 0)) (d (n "memflow-win32") (r "^0.2.0-beta") (d #t) (k 2)) (d (n "simplelog") (r "^0.11.1") (d #t) (k 2)))) (h "13dhrdvkp5bzddczs033hlxwzyaliavs98rvyhd8jxhm5hgjgg49") (f (quote (("inventory") ("filemap") ("default" "filemap"))))))

(define-public crate-memflow-coredump-0.2.0-beta11 (c (n "memflow-coredump") (v "0.2.0-beta11") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "memflow") (r "=0.2.0-beta11") (f (quote ("plugins" "filemap"))) (d #t) (k 0)) (d (n "memflow-win32") (r "=0.2.0-beta11") (d #t) (k 2)) (d (n "simplelog") (r "^0.12") (d #t) (k 2)))) (h "0r34hy3jylhy9sa19ian48clysy1gfp1nzjsygr679s2vkpw4n2k") (f (quote (("inventory") ("filemap") ("default" "filemap"))))))

