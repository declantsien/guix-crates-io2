(define-module (crates-io me mf memfd_path) #:use-module (crates-io))

(define-public crate-memfd_path-0.1.0 (c (n "memfd_path") (v "0.1.0") (d (list (d (n "memfd") (r "^0.6.0") (d #t) (k 0)))) (h "1r3q88mhl96qcpbz2x8z7ia65cqlmksm71gmhn4md5vyvq57bcqi")))

(define-public crate-memfd_path-0.1.1 (c (n "memfd_path") (v "0.1.1") (d (list (d (n "memfd") (r "^0.6.0") (d #t) (k 0)))) (h "0sb8b4s1pscaaa36b53v409fq1wqgapwn8g987fjys5qa2zcyl2d")))

(define-public crate-memfd_path-0.1.2 (c (n "memfd_path") (v "0.1.2") (d (list (d (n "memfd") (r "^0.6.0") (d #t) (k 0)))) (h "0caybi848nj46kyl3l2jx6dw27ab0ff2jhxmzxv4dxrrx5njbl1d")))

