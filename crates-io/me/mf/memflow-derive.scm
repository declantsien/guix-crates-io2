(define-module (crates-io me mf memflow-derive) #:use-module (crates-io))

(define-public crate-memflow-derive-0.0.0 (c (n "memflow-derive") (v "0.0.0") (h "033al406p9w6vkiyhlwyknmfxyjx1zqf91lkh5a1r930m2sspl4y")))

(define-public crate-memflow-derive-0.1.0 (c (n "memflow-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "memflow") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p4h9cb801z4w8lklwf6a970h7iymzsbki769zw4pc3zxfzwdw96")))

(define-public crate-memflow-derive-0.1.4 (c (n "memflow-derive") (v "0.1.4") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "memflow") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kal5hcfvxscmjdr01iv8rmhdvjp3p3djw9rsf1mna9z28f6pb3v")))

(define-public crate-memflow-derive-0.1.5 (c (n "memflow-derive") (v "0.1.5") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "memflow") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10zazja76cxcp2pqrrzkpydh7mbvlv25dck5f8sy31n6b39jyaxy")))

(define-public crate-memflow-derive-0.2.0-beta1 (c (n "memflow-derive") (v "0.2.0-beta1") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "1ysii2vwpgjqnsxdj0lfabb0a7a478cpxcrzz2i6f42w6m3bhqmi")))

(define-public crate-memflow-derive-0.2.0-beta2 (c (n "memflow-derive") (v "0.2.0-beta2") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "0lshmjfgrcivyg9a4cd313x40kki41bdgls28mfgxvx1vq661kw9")))

(define-public crate-memflow-derive-0.2.0-beta3 (c (n "memflow-derive") (v "0.2.0-beta3") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "1xxy9z49jak659h9f3kzijx54ndjk6wgl1y1ny9rr2jshmpycl7f")))

(define-public crate-memflow-derive-0.2.0 (c (n "memflow-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0cjdx4in1cdpgi68ifzlimbf5srrj34lpz2r0gmr534n3xlgcrnp")))

