(define-module (crates-io me mf memfd) #:use-module (crates-io))

(define-public crate-memfd-0.0.1-alpha.1 (c (n "memfd") (v "0.0.1-alpha.1") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0axkgyq11gcd2nlkklhcl0ryr3027wln38a683di6z77d3k2s4zl")))

(define-public crate-memfd-0.1.0 (c (n "memfd") (v "0.1.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lh1v4n5fya7mr3ajngrka76w3s0d3p4d5n01jkwbsp82ck152rq")))

(define-public crate-memfd-0.2.0 (c (n "memfd") (v "0.2.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1md7ffd2p4hqvpgf5a04yjp3fhbjcam93552sgk68j80gzsdidql")))

(define-public crate-memfd-0.3.0 (c (n "memfd") (v "0.3.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00sgrzp68m63bw49aaha9mdlwfxqn6r99aalqqbf2mkdp4sv7hsr")))

(define-public crate-memfd-0.4.0 (c (n "memfd") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cawcp2gq8s3mfza6h79fwgd4qlhqp6q8iwf87ng36yywsh3znfr")))

(define-public crate-memfd-0.4.1 (c (n "memfd") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.99") (d #t) (k 0)))) (h "0yifm67qsgmzpw9x8ir51mpfaaw2f7b5w417mpb4jjspaz37sqpn")))

(define-public crate-memfd-0.5.0 (c (n "memfd") (v "0.5.0") (d (list (d (n "rustix") (r "^0.34.0") (d #t) (k 0)))) (h "16b78wv6xy9q656x48548f6578w1dzjzdy2i750c8pwy2diwwjrp")))

(define-public crate-memfd-0.5.1 (c (n "memfd") (v "0.5.1") (d (list (d (n "rustix") (r "^0.34.1") (d #t) (k 0)))) (h "0f14gcw5j1pkdf20gk0x7y94ffnskasagzfv7y8q38jrg7cfwq3r")))

(define-public crate-memfd-0.6.0 (c (n "memfd") (v "0.6.0") (d (list (d (n "rustix") (r "^0.34.1") (d #t) (k 0)))) (h "0zd62vs7iilp1d4i1pnnjh9ii59868y762nlgjgj6jkk02zm3inb")))

(define-public crate-memfd-0.6.1 (c (n "memfd") (v "0.6.1") (d (list (d (n "rustix") (r "^0.35.6") (f (quote ("fs"))) (d #t) (k 0)))) (h "102xw72dqp31yr3w9bs7xx1fkdcqigf0p5cm67qizlamx1fml2s8")))

(define-public crate-memfd-0.6.2 (c (n "memfd") (v "0.6.2") (d (list (d (n "rustix") (r "^0.36.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "1ywfj3l0f0m4ll393kz6idnmiy4rfb3llmk4xxd4lvjqhpcmj2mj")))

(define-public crate-memfd-0.6.3 (c (n "memfd") (v "0.6.3") (d (list (d (n "rustix") (r "^0.37.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "17hg5jv1smxxfkm64kif1m3d7if5xcvkax8g8l3nj3g1qv6rrj7z")))

(define-public crate-memfd-0.6.4 (c (n "memfd") (v "0.6.4") (d (list (d (n "rustix") (r "^0.38.8") (f (quote ("fs"))) (d #t) (k 0)))) (h "0r5cm3wzyr1x7768h3hns77b494qbz0g05cb9wgpjvrcsm5gmkxj")))

