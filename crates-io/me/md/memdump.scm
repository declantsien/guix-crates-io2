(define-module (crates-io me md memdump) #:use-module (crates-io))

(define-public crate-memdump-0.1.0 (c (n "memdump") (v "0.1.0") (h "0ddk3dvlh0q74dzirp7mz967b8amjbilbi5m3nm1dx6332zwi4jp") (y #t)))

(define-public crate-memdump-0.1.1 (c (n "memdump") (v "0.1.1") (h "1bwwl3ifkpm94jmyh5bblz8j89l4r6sm1q15biggckwaj2y1hflg") (y #t)))

(define-public crate-memdump-0.1.2 (c (n "memdump") (v "0.1.2") (h "14s0w7fxlnxf1ifyf0gp79d2jy917r5a04210czi42qvylwp0b9p")))

(define-public crate-memdump-0.1.3 (c (n "memdump") (v "0.1.3") (h "1fpngxpnfcrbp1w7nz4i7p6y4708sk9hm26n0fjbnh5j37nkg5b6")))

