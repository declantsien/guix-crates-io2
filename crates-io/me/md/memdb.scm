(define-module (crates-io me md memdb) #:use-module (crates-io))

(define-public crate-memdb-0.1.0 (c (n "memdb") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6.3") (d #t) (k 0)))) (h "1ql7ccfy96259znacmc566gx1jxpvd1kw7x782w7m93anr7n2m0c")))

(define-public crate-memdb-1.0.0 (c (n "memdb") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.6.3") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.4") (d #t) (k 2)))) (h "0zpsn1zlikmgxnkc7n7skjg80mn03rr6msd7479ha212ic4mm701")))

