(define-module (crates-io me md memdbg) #:use-module (crates-io))

(define-public crate-memdbg-0.1.0 (c (n "memdbg") (v "0.1.0") (h "0hrniz1iq8cylsymw1rnq64pl3hgiwwgklc2jg74lkcajhl8lqvl") (f (quote (("stringify") ("default" "debug") ("debug"))))))

(define-public crate-memdbg-0.1.1 (c (n "memdbg") (v "0.1.1") (h "12ll2zpz56qzf52y51z9fccbb8qkwkdv8zsn0w1mla9wp9mnlp21") (f (quote (("stringify") ("default" "debug") ("debug"))))))

(define-public crate-memdbg-0.1.2 (c (n "memdbg") (v "0.1.2") (h "1g4xr09y4q5imh170q162f1j9sdpi2zab0wrm3faad13m3nyid6v") (f (quote (("stringify") ("default" "debug") ("debug"))))))

(define-public crate-memdbg-0.1.3 (c (n "memdbg") (v "0.1.3") (h "0aqjrrh9hdh86d2dnymx7v2gyyl8f6ryr4w4686y3zk3gkxv2hap") (f (quote (("stringify") ("default" "debug") ("debug"))))))

(define-public crate-memdbg-0.1.4 (c (n "memdbg") (v "0.1.4") (h "0z55knq197ayxd8ip03dyzgy765fihginpn82a6qsfvl7zqdmjrw") (f (quote (("stringify") ("default" "debug") ("debug"))))))

