(define-module (crates-io me mo memory_library) #:use-module (crates-io))

(define-public crate-memory_library-0.0.1 (c (n "memory_library") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "0b43mgjmgj9rmp0fwwj3jllcbwrdfvw9rnr253sxvcbfl5k41q4w")))

(define-public crate-memory_library-0.0.2 (c (n "memory_library") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1iyr3szwixcbdj2p2faq4wy04xckcii26sq2q0kmihppnkq58vvw")))

(define-public crate-memory_library-0.0.3 (c (n "memory_library") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "nix") (r "^0.8.0") (d #t) (k 0)))) (h "12sl3sr6nm7a2faabd1337kdb9jwgvpnv0jpwlij8507qlnr8d3k")))

