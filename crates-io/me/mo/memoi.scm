(define-module (crates-io me mo memoi) #:use-module (crates-io))

(define-public crate-memoi-0.1.0-dev (c (n "memoi") (v "0.1.0-dev") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "text_io") (r "^0.1.10") (d #t) (k 0)))) (h "19n2jc3zcj5wqwr38q38mbc9bvplmsmcy31k6bcfiab9vxgcs8m2")))

