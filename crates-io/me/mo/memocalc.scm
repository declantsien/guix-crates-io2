(define-module (crates-io me mo memocalc) #:use-module (crates-io))

(define-public crate-memocalc-1.0.0 (c (n "memocalc") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0p80xsbl81yxz7idfw6azk9zjixdcg0jb76wcyq0zxx8367m8isf")))

