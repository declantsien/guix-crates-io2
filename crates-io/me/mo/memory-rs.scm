(define-module (crates-io me mo memory-rs) #:use-module (crates-io))

(define-public crate-memory-rs-0.2.5 (c (n "memory-rs") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "windows-sys") (r "^0.35") (f (quote ("Win32_System_LibraryLoader" "Win32_Foundation" "Win32_System_Threading" "Win32_System_ProcessStatus" "Win32_System_Memory" "Win32_System_Diagnostics_Debug" "Win32_System_Diagnostics_ToolHelp" "Win32_System_SystemServices" "Win32_Security"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1axrik3wnggj073wv8nxjpkxaz1k4ispdav9y0vxvnfdgd88ffvm") (f (quote (("impl-drop") ("default" "impl-drop"))))))

(define-public crate-memory-rs-0.2.6 (c (n "memory-rs") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "windows-sys") (r "^0.35") (f (quote ("Win32_System_LibraryLoader" "Win32_Foundation" "Win32_System_Threading" "Win32_System_ProcessStatus" "Win32_System_Memory" "Win32_System_Diagnostics_Debug" "Win32_System_Diagnostics_ToolHelp" "Win32_System_SystemServices" "Win32_Security"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rmspbzc37x417xfswyd7icyvqj9h9xk0nih80v06wx9mi5zc95k") (f (quote (("impl-drop") ("default" "impl-drop"))))))

