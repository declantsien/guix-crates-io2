(define-module (crates-io me mo memory-cache-rs) #:use-module (crates-io))

(define-public crate-memory-cache-rs-0.1.0 (c (n "memory-cache-rs") (v "0.1.0") (h "0ydpywj4f1fpvkbpf5r06lphhwssh80zwsmv4niz1kmw1qpm9z28")))

(define-public crate-memory-cache-rs-0.1.1 (c (n "memory-cache-rs") (v "0.1.1") (h "1z01a6k6vxkibnpvn5d7qk9fx68fyv7icpf6h8x6h7dsxyxw5j5i")))

(define-public crate-memory-cache-rs-0.1.2 (c (n "memory-cache-rs") (v "0.1.2") (h "1krd0q4609kd5b0gk50k26m8d3xgcb0gqf58jwjy9igwswhf60i7")))

(define-public crate-memory-cache-rs-0.2.0 (c (n "memory-cache-rs") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)))) (h "079vlddyf6bj5dj2jvr27x5y1cf929c156kbs1xfb3qayns9hh0s")))

