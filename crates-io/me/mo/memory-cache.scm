(define-module (crates-io me mo memory-cache) #:use-module (crates-io))

(define-public crate-memory-cache-0.1.0 (c (n "memory-cache") (v "0.1.0") (d (list (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "tetsy-util-mem") (r "^0.3.0") (d #t) (k 0)))) (h "17r2kx2dl2wp1w27hnzdrzrjkhadxbpcmldy324ai670cy7p797m")))

