(define-module (crates-io me mo memory-socket) #:use-module (crates-io))

(define-public crate-memory-socket-0.0.0 (c (n "memory-socket") (v "0.0.0") (h "0r07c8vk2znainiqijmdggf1cl6yrs4xagllkc3b2am0a8qg7530")))

(define-public crate-memory-socket-0.1.0 (c (n "memory-socket") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "flume") (r "^0.7") (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0ynibiig532l5pdhzz2j4l1x23w3wa4sxgqgpji53hjx91qwl88i") (f (quote (("default") ("async" "futures" "flume/async"))))))

(define-public crate-memory-socket-0.2.0 (c (n "memory-socket") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "flume") (r "^0.7") (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "10bv7vs0khahbd006xl6alrvqjwr1z741fbyf5an94kxc2wsqbnp") (f (quote (("default") ("async" "futures" "flume/async"))))))

