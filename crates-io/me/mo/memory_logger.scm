(define-module (crates-io me mo memory_logger) #:use-module (crates-io))

(define-public crate-memory_logger-0.1.0 (c (n "memory_logger") (v "0.1.0") (d (list (d (n "flume") (r "^0.7") (o #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.3") (o #t) (d #t) (k 0)))) (h "01a6hvfy3aigz1s5x4xg3amxfjgfmw70c880fyv5b96cqwbxnmh6") (f (quote (("target" "regex") ("blocking") ("asynchronous" "flume"))))))

(define-public crate-memory_logger-0.1.1 (c (n "memory_logger") (v "0.1.1") (d (list (d (n "flume") (r "^0.7") (o #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0dj4m695c6i5jayfh2nhdds6pvqlnl0w5av4x0as6rihb7lbiw5r") (f (quote (("target" "regex") ("blocking") ("asynchronous" "flume"))))))

