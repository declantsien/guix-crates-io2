(define-module (crates-io me mo memory_mapper) #:use-module (crates-io))

(define-public crate-memory_mapper-0.1.0 (c (n "memory_mapper") (v "0.1.0") (h "04wg7b5nhxxdy5fcwwcmv1s7f2vyddf55pia4v9r02600xkg6347")))

(define-public crate-memory_mapper-0.1.1 (c (n "memory_mapper") (v "0.1.1") (h "004kx0iaqyxs2qnhx4482myjx6m01y2rq8wki858n1a5bilclai9")))

