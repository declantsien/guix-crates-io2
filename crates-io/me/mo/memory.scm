(define-module (crates-io me mo memory) #:use-module (crates-io))

(define-public crate-memory-1.0.0 (c (n "memory") (v "1.0.0") (d (list (d (n "common_macros") (r "^0.1.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "15g200lrhaxnsv1r5wncv6fcafq7n1pg17xg5yidm0bfmcyy179h")))

(define-public crate-memory-1.1.0 (c (n "memory") (v "1.1.0") (d (list (d (n "common_macros") (r "^0.1.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "0q3qy7bxxkd3zc80kw0lgx9nah1s8vk1mz2hfhnwyb55a9m33ys6")))

(define-public crate-memory-1.1.1 (c (n "memory") (v "1.1.1") (d (list (d (n "common_macros") (r "^0.1.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "11km1rw3bmfiqwgcz4hximcd1hg8ppr471dpvnvj3wadlhrcbs0d")))

(define-public crate-memory-1.1.2 (c (n "memory") (v "1.1.2") (d (list (d (n "common_macros") (r "^0.1.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "1n0a2gr1m9lz6ank0vaalaia5dgp5ifg687d7l7v9yzr0ynmiyi2")))

(define-public crate-memory-1.1.3 (c (n "memory") (v "1.1.3") (d (list (d (n "common_macros") (r "^0.1.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "1sbw2la0q6gvnip9hj0dwwly2fjmp1kamkcv4llkz8w0awgd2inp")))

