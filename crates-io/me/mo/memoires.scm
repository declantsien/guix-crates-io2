(define-module (crates-io me mo memoires) #:use-module (crates-io))

(define-public crate-memoires-0.1.0 (c (n "memoires") (v "0.1.0") (h "10g7al7izspfxcccrbkcmfir873rj330pcilvx21a08m8bxh8cjl")))

(define-public crate-memoires-0.1.1 (c (n "memoires") (v "0.1.1") (h "1pvgjv3rn13y06df3pijwxigml5gjplp0as80jis3mdzh5fyxfj4")))

