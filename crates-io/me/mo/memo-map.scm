(define-module (crates-io me mo memo-map) #:use-module (crates-io))

(define-public crate-memo-map-0.1.0 (c (n "memo-map") (v "0.1.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "0w1j8id67mms11s44afq0c2ay9n63q3gxaf84s4fyhfla0nz6v25") (y #t) (r "1.43")))

(define-public crate-memo-map-0.2.0 (c (n "memo-map") (v "0.2.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1yvmxqjqfw3309sbs184v16wrkdq5821h9lwsa21zjn874jgjqgb") (y #t) (r "1.43")))

(define-public crate-memo-map-0.2.1 (c (n "memo-map") (v "0.2.1") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1l9vmqfkr7nc26gm3bpphsdjwj3h7laszhbhh0k7nh0gg2bq1b6m") (y #t) (r "1.43")))

(define-public crate-memo-map-0.3.0 (c (n "memo-map") (v "0.3.0") (h "1jp6wclqv81dw4sdb0liy65lsvpbg0ny05ijgykr23408hg8s9mb") (r "1.43")))

(define-public crate-memo-map-0.3.1 (c (n "memo-map") (v "0.3.1") (h "0h0c80ilf74872nfn1dx65zdj60cxcczrbks113l9kk0jp07dhmf") (r "1.43")))

(define-public crate-memo-map-0.3.2 (c (n "memo-map") (v "0.3.2") (h "10va9wc8jbc6vs0924ak0ac10rdw7i3h6c9jrga657pi5mdk6k1p") (r "1.43")))

