(define-module (crates-io me mo memoir-logger) #:use-module (crates-io))

(define-public crate-memoir-logger-0.1.0 (c (n "memoir-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (d #t) (k 0)))) (h "0wnn0z3dkwq1scr7bh0n8siwk7dfa0f6lf2v14snrw0nyl167vyy")))

(define-public crate-memoir-logger-0.1.1 (c (n "memoir-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (d #t) (k 0)))) (h "0lbq8sc2lbc1pnrh09020ns51m4agsvhzip917jm2jdqhijl9nai")))

(define-public crate-memoir-logger-0.1.2 (c (n "memoir-logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (d #t) (k 0)))) (h "1s4w4m7sdaqvsnh24wc8p9s7g8dqly8s3s9y1jiirp85sbzc31v9")))

(define-public crate-memoir-logger-1.2.1 (c (n "memoir-logger") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (d #t) (k 0)))) (h "0m68w4dg7xb9h63mrn6zh9n84s2a9p9gk9nsb5i22lnkm4ycr2c9")))

(define-public crate-memoir-logger-1.2.2 (c (n "memoir-logger") (v "1.2.2") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (d #t) (k 0)))) (h "0w7qx45z8pd2m48z86jwq8vb5lvakp07hqni3wqm10slj781ar4d")))

(define-public crate-memoir-logger-1.2.3 (c (n "memoir-logger") (v "1.2.3") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (d #t) (k 0)))) (h "1zqndjaiqyp0pl3iqkm9g93gwr8r53zbkm5209bigqr62nfrbmbx")))

