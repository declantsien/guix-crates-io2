(define-module (crates-io me mo memorize) #:use-module (crates-io))

(define-public crate-memorize-1.0.0 (c (n "memorize") (v "1.0.0") (d (list (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)))) (h "0k26rq69syj9ayrz7xyhyswj22j88c2f07gick4k6x562lw0vwzl")))

(define-public crate-memorize-2.0.0 (c (n "memorize") (v "2.0.0") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)))) (h "0ba43h558mznc43i00zpln5m6axg68650sclw8bl9dl87aymsl3b")))

