(define-module (crates-io me mo memory_pages) #:use-module (crates-io))

(define-public crate-memory_pages-0.1.0 (c (n "memory_pages") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bwxqkslwvvzlrgr60sv3igcmh0159himdzs6axrdajcmf3ql01a") (f (quote (("deny_xw") ("deafault" "deny_xw") ("allow_exec"))))))

