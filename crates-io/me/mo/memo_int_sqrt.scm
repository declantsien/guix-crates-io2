(define-module (crates-io me mo memo_int_sqrt) #:use-module (crates-io))

(define-public crate-memo_int_sqrt-0.1.0 (c (n "memo_int_sqrt") (v "0.1.0") (h "09ald4xg0rrlgdj4khmj4j0yxi3wsapj4ig8si56hmiwymqnd6n7")))

(define-public crate-memo_int_sqrt-0.1.1 (c (n "memo_int_sqrt") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1anrr6pvplci0a62l3zv05raj1433hp282rk7vnlvrv3xn3fsqcv")))

