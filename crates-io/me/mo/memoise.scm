(define-module (crates-io me mo memoise) #:use-module (crates-io))

(define-public crate-memoise-0.1.0 (c (n "memoise") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vrnv9g271srhzabjbxhrmwgclgmpidcipz8lszsn14r1405n26l")))

(define-public crate-memoise-0.1.1 (c (n "memoise") (v "0.1.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k42j0h82z3qd3mi938d3rrnxnla2cykj32b3hxhhinz1dp2hh83")))

(define-public crate-memoise-0.2.0 (c (n "memoise") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cpyrc6bjjdpg5b31ngwil373q675qhy8k9d60x074825j7gkrz8")))

(define-public crate-memoise-0.3.0 (c (n "memoise") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bl46w4yrd30qakz3qr92ar6qkfzdggzzf87wizbff3vs118p34n")))

(define-public crate-memoise-0.3.1 (c (n "memoise") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vygj4n5sifxrs09xmp7jf0i13njz90frl2k9nj3np4vlxacxvsg")))

(define-public crate-memoise-0.3.2 (c (n "memoise") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nwrclyii4pbcpr4bh4vi132d9khnbb22zp53ibxqafqkbfvxm04")))

