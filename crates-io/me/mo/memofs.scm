(define-module (crates-io me mo memofs) #:use-module (crates-io))

(define-public crate-memofs-0.1.0 (c (n "memofs") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)))) (h "1w1c7kll96migay0llp7ssg1qsmy76zlbdfh14lxj3zj00qxcp98")))

(define-public crate-memofs-0.1.1 (c (n "memofs") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "fs-err") (r "^2.3.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)))) (h "0qmsvl12dvn37hn3acqi0qvjmwj4n5dcij7mycyd1aksh9mmi8hh")))

(define-public crate-memofs-0.1.2 (c (n "memofs") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "fs-err") (r "^2.3.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yy20v270aa8i4lrcgcqn6lahm1r3vni493cv76pjq3gfi1ahp2p")))

(define-public crate-memofs-0.1.3 (c (n "memofs") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "fs-err") (r "^2.3.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16vvgmq27viq5g2mhp1ds51bw0gy3z23drab3r9yafv4fpyivjyx")))

(define-public crate-memofs-0.2.0 (c (n "memofs") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "fs-err") (r "^2.3.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rxmndhqxjai9vyaf9jjijzx2sf73hg5ig8la88sc1pmk2hsbp2h")))

