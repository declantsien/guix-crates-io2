(define-module (crates-io me mo memoffset) #:use-module (crates-io))

(define-public crate-memoffset-0.1.0 (c (n "memoffset") (v "0.1.0") (h "0g55j7gkn990zhhrj45ah94i3d592zg79c3m3vkkk871xjxfaqz1") (f (quote (("std") ("default" "std"))))))

(define-public crate-memoffset-0.2.0 (c (n "memoffset") (v "0.2.0") (h "1imr3if19vld03bmr52h0363yjpyhrwx4l7i63a8jkkr491r2y1r")))

(define-public crate-memoffset-0.2.1 (c (n "memoffset") (v "0.2.1") (h "1cvm2z7dy138s302ii7wlzcxbka5a8yfl5pl5di7lbdnw9hw578g")))

(define-public crate-memoffset-0.3.0 (c (n "memoffset") (v "0.3.0") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "1q0c4sci6x7x5hr5vz889kwskqc52p2psyzj488gq4m69j8wryky") (y #t)))

(define-public crate-memoffset-0.4.0 (c (n "memoffset") (v "0.4.0") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0dr519c8vynf5ynwi4d59y1kvky3lnjp5h7l4la0dhb5s21g2ngc") (y #t)))

(define-public crate-memoffset-0.5.0 (c (n "memoffset") (v "0.5.0") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "1cp3pz0w1l6prphw7885lad4vqmjfdd0qax15d6cdaf6wlmbz96y")))

(define-public crate-memoffset-0.5.1 (c (n "memoffset") (v "0.5.1") (d (list (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0zqpz1apkxvzbi41q07vaxpn3bmvhqqkmg8bbbpbgfrv0gdpaq6f")))

(define-public crate-memoffset-0.5.2 (c (n "memoffset") (v "0.5.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "1k67azvd1wc6kv3s2crmaab91is7lvf16wsd0cvi9w99qflc31aa")))

(define-public crate-memoffset-0.5.3 (c (n "memoffset") (v "0.5.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "1fblqzc25hfaym8m0pj112s66pqq87avvaqm5hp5rskib2w9w63m")))

(define-public crate-memoffset-0.5.4 (c (n "memoffset") (v "0.5.4") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1n236rn8yxbi4ckh1qkz34822bgp24qijnp99sfhjx73lw12rz5l") (f (quote (("unstable_const") ("default"))))))

(define-public crate-memoffset-0.5.5 (c (n "memoffset") (v "0.5.5") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0pz55g763c113fffqbhdbdzjd8n4kq7wdillgs9qmw5vw4kb1661") (f (quote (("unstable_raw") ("unstable_const") ("default"))))))

(define-public crate-memoffset-0.5.6 (c (n "memoffset") (v "0.5.6") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1ahi51aa650s2p9ib1a4ifgqv0pzmsxlm9z4xdgvi9zdd7q7ac84") (f (quote (("unstable_raw") ("unstable_const") ("default"))))))

(define-public crate-memoffset-0.6.0 (c (n "memoffset") (v "0.6.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1sl1dkcpcqyvmqv952b5jzsmxp5r9fz7hp3nhpzgmyxrgb6f4pwp") (f (quote (("unstable_raw") ("unstable_const") ("default")))) (y #t)))

(define-public crate-memoffset-0.6.1 (c (n "memoffset") (v "0.6.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "11yxgw330cf8g4wy0fnb20ag8gg1b33fsnfmg2g8z6h5wc444yqm") (f (quote (("unstable_raw") ("unstable_const") ("default"))))))

(define-public crate-memoffset-0.6.2 (c (n "memoffset") (v "0.6.2") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0f7xzgnzh0y7llgmbq1cxb0gp6fh8hzcryhk84mlgd0jm1agq56c") (f (quote (("unstable_const") ("default"))))))

(define-public crate-memoffset-0.6.3 (c (n "memoffset") (v "0.6.3") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "079nahsp9sr5dhahd173i6a8k0sl82w6s4awsigzilcf3rcbcgzq") (f (quote (("unstable_const") ("default"))))))

(define-public crate-memoffset-0.6.4 (c (n "memoffset") (v "0.6.4") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1yfx2v8kmkhr2d4gwk8ghihdwg73vapn3vvp0im06f0kgx8crb2r") (f (quote (("unstable_const") ("default"))))))

(define-public crate-memoffset-0.6.5 (c (n "memoffset") (v "0.6.5") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1kkrzll58a3ayn5zdyy9i1f1v3mx0xgl29x0chq614zazba638ss") (f (quote (("unstable_const") ("default"))))))

(define-public crate-memoffset-0.7.0 (c (n "memoffset") (v "0.7.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "10p82dhqzj7bvphd6hjg0bhihpklfky0b537vnjwav24yk7f6lw2") (f (quote (("unstable_const") ("default"))))))

(define-public crate-memoffset-0.7.1 (c (n "memoffset") (v "0.7.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1x2zv8hv9c9bvgmhsjvr9bymqwyxvgbca12cm8xkhpyy5k1r7s2x") (f (quote (("unstable_const") ("default"))))))

(define-public crate-memoffset-0.8.0 (c (n "memoffset") (v "0.8.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1qcdic88dhgw76pafgndpz04pig8il4advq978mxdxdwrydp276n") (f (quote (("unstable_const") ("default"))))))

(define-public crate-memoffset-0.9.0 (c (n "memoffset") (v "0.9.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0v20ihhdzkfw1jx00a7zjpk2dcp5qjq6lz302nyqamd9c4f4nqss") (f (quote (("unstable_offset_of") ("unstable_const") ("default"))))))

(define-public crate-memoffset-0.9.1 (c (n "memoffset") (v "0.9.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "12i17wh9a9plx869g7j4whf62xw68k5zd4k0k5nh6ys5mszid028") (f (quote (("unstable_offset_of") ("unstable_const") ("default"))))))

