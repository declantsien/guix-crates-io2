(define-module (crates-io me mo memoizer) #:use-module (crates-io))

(define-public crate-memoizer-0.1.0 (c (n "memoizer") (v "0.1.0") (h "09zkalrp2vclnzg3dfknz3hihwyigl6ncfdcsf4h0kjlbkmjycy5")))

(define-public crate-memoizer-0.1.1 (c (n "memoizer") (v "0.1.1") (h "0hxqwbcx36489mza3xzw3z41wklcr1a9q3l969b5xlzmcjf4fyn9")))

(define-public crate-memoizer-0.2.0 (c (n "memoizer") (v "0.2.0") (h "0k6g4br6nifmn1h946d1lhahdjrn00a5nwykq990wjb3zpisswl8")))

(define-public crate-memoizer-0.2.1 (c (n "memoizer") (v "0.2.1") (h "1wjs4gf7fzdgqr6irgk6zszp2r5jzj2jpr35s8cxa0d2hg511ggw")))

(define-public crate-memoizer-0.2.2 (c (n "memoizer") (v "0.2.2") (h "0nfp1ckc2ysc94jh5rn5iq31za6wc1m6xjm5ghxczsc6lkqblm52")))

