(define-module (crates-io me mo memory-layout-codegen) #:use-module (crates-io))

(define-public crate-memory-layout-codegen-0.1.0 (c (n "memory-layout-codegen") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1y3nrc9mn440kr79gc6fpf1r9g9i8l4kn0wlnv6naf7ynf4jj326")))

(define-public crate-memory-layout-codegen-0.2.0 (c (n "memory-layout-codegen") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0f3aalgrizq61aj0i9mhl8yj8s8c90mr7cqrrddrl7cws80jm59r")))

(define-public crate-memory-layout-codegen-0.3.0 (c (n "memory-layout-codegen") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1zdvrz0mf2nfi8392iqyagi9syjaddgxrw7g502i1wmd9fl2rzdm") (f (quote (("offset_of"))))))

