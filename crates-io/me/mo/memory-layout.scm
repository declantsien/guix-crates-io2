(define-module (crates-io me mo memory-layout) #:use-module (crates-io))

(define-public crate-memory-layout-0.1.0 (c (n "memory-layout") (v "0.1.0") (d (list (d (n "memory-layout-codegen") (r "^0.1") (d #t) (k 0)))) (h "1a8zklng9nz8zwc908j0s0l1p0klcrbr6xi2kajpf5sf6yrjhxdk") (r "1.57.0")))

(define-public crate-memory-layout-0.2.0 (c (n "memory-layout") (v "0.2.0") (d (list (d (n "memory-layout-codegen") (r "^0.2") (d #t) (k 0)))) (h "0r0j3akwg2kkm12fdxxbin93hsgr2ypk98na3rycsis76kxq2l2h") (r "1.57.0")))

(define-public crate-memory-layout-0.3.0 (c (n "memory-layout") (v "0.3.0") (d (list (d (n "memory-layout-codegen") (r "^0.3") (d #t) (k 0)))) (h "0f5ah449n6i2fshhqmr8rnssw436ijcq793pfqpy7jjzlbnmnd1b") (f (quote (("offset_of" "memory-layout-codegen/offset_of")))) (r "1.57.0")))

