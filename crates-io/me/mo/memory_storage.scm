(define-module (crates-io me mo memory_storage) #:use-module (crates-io))

(define-public crate-memory_storage-0.9.0 (c (n "memory_storage") (v "0.9.0") (h "154q098jraxk1k1m0r01df6c2rqcjkiby43qmdxd61ccrypcqbpy") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-memory_storage-0.9.1 (c (n "memory_storage") (v "0.9.1") (h "0c0n05jkbx2z0dh6carc62wagadp8sndyzh39lncvsys6n9yd2a7") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-memory_storage-0.9.2 (c (n "memory_storage") (v "0.9.2") (h "0ixl6ki76hcky6hzr81wq6j6r9h4h922fak6lbkakb2lvyhh8zpq") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-memory_storage-0.9.21 (c (n "memory_storage") (v "0.9.21") (h "168rhkhwm2v23q0dgslkz20kmrlp0pkj1wla1rp8xq6qpcgcrmj0") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-memory_storage-0.9.22 (c (n "memory_storage") (v "0.9.22") (h "08qdnqcgh4mm7sa80ffnx9dharyaaq8j6vkxb0kimvvy67spmavd") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-memory_storage-0.9.23 (c (n "memory_storage") (v "0.9.23") (h "0id7hz920q8z2rxpnf5w5vr3pkwlzd06cwk36wzv8d8y4a8808r8") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-memory_storage-0.9.3 (c (n "memory_storage") (v "0.9.3") (h "1mqcygismclb43q94idw0vd3zirdsh0xq2rfmi704qizmfwb7raq") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-memory_storage-0.9.30 (c (n "memory_storage") (v "0.9.30") (h "0z3m6y6db91rigv4s0f51i0yzrrv7xz9dkqj3jsdp6d7bphmghyb") (f (quote (("default" "alloc") ("alloc"))))))

