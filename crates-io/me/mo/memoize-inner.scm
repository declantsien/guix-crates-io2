(define-module (crates-io me mo memoize-inner) #:use-module (crates-io))

(define-public crate-memoize-inner-0.2.0 (c (n "memoize-inner") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10pq9lhjskn5limjvf2549rdlgwc6m20f0vyn1rp2al0zbh73vng") (f (quote (("full") ("default"))))))

(define-public crate-memoize-inner-0.2.1 (c (n "memoize-inner") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18zq2chd872ixwmcny5kr58f203pdyrdz8l7vjj6783bb1ziwyi1") (f (quote (("full") ("default"))))))

(define-public crate-memoize-inner-0.3.2 (c (n "memoize-inner") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08x7vp6sp3cq27sj8nhsr5dhddy5iqikh72p9jfwbz3g1qbdbdxq") (f (quote (("full") ("default"))))))

(define-public crate-memoize-inner-0.4.0 (c (n "memoize-inner") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16vik6vq9n7bhfqlgfg3cl88vb0gj6ihp7n3wlnqrv0qqdjf5zcb") (f (quote (("full") ("default"))))))

(define-public crate-memoize-inner-0.4.3 (c (n "memoize-inner") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b3k695dvgy8k81s24r1ci70vslkg8cfqikvvwriw38zx7kyrg97") (f (quote (("full") ("default"))))))

