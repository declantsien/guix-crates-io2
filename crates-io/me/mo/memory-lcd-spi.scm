(define-module (crates-io me mo memory-lcd-spi) #:use-module (crates-io))

(define-public crate-memory-lcd-spi-0.0.1 (c (n "memory-lcd-spi") (v "0.0.1") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (f (quote ("defmt"))) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "=1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")))) (h "04wl38m7r47f3nsnw0sgwyldpqvm9v4jsjlva049i2gb25sam0ga")))

(define-public crate-memory-lcd-spi-0.0.2 (c (n "memory-lcd-spi") (v "0.0.2") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (f (quote ("defmt"))) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "=1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")))) (h "1c35xvhb5wqzm5lc1nxlxv5s3hc2wzkjsihf6d1j7hxf4vdfwxmp")))

(define-public crate-memory-lcd-spi-0.0.4 (c (n "memory-lcd-spi") (v "0.0.4") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (f (quote ("defmt"))) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "=1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")))) (h "1070sk2ajm680qiwfdwngvjmyvaa8awxfar4c8fagjjq8rbc39sc")))

(define-public crate-memory-lcd-spi-0.0.5 (c (n "memory-lcd-spi") (v "0.0.5") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (f (quote ("defmt"))) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0) (p "embedded-hal")) (d (n "embedded-hal-1") (r "=1.0.0-alpha.10") (d #t) (k 0) (p "embedded-hal")))) (h "05s2arsqf9n5gg2ap0pdbjll0i9znk7yhb73l2jbcch3gwnhxx95")))

(define-public crate-memory-lcd-spi-0.0.6 (c (n "memory-lcd-spi") (v "0.0.6") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (f (quote ("defmt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "12wdljya05xmwq68343xrqmfsghg0jhbdfrdc8n96lvwqbbg4c63")))

(define-public crate-memory-lcd-spi-0.0.7 (c (n "memory-lcd-spi") (v "0.0.7") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics-core") (r "^0.4.0") (f (quote ("defmt"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0y64wkjz856vndbwxx8dhdx3n39sh9gw44xxg34lq91263a4i00h")))

