(define-module (crates-io me mo memodb) #:use-module (crates-io))

(define-public crate-memodb-0.1.0 (c (n "memodb") (v "0.1.0") (d (list (d (n "concread") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "051y8z9cr9kblpgax8n6766nav063rh5ha4f9dh6rc3bhy1nimhp")))

(define-public crate-memodb-0.2.0 (c (n "memodb") (v "0.2.0") (d (list (d (n "concread") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1hd0q8b56iprfq5fwas0dkm6riz1yia46gjlzcr3lrl0c4rcy88h")))

(define-public crate-memodb-0.3.0 (c (n "memodb") (v "0.3.0") (d (list (d (n "concread") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "11mhhkqdpi0q3l7zpwmdv2rlmgb8c2a1bpzm4yhiff47cpgi97ma")))

