(define-module (crates-io me mo memorymodule-rs) #:use-module (crates-io))

(define-public crate-memorymodule-rs-0.0.1 (c (n "memorymodule-rs") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "1hzmn78mavadmkynnkj7an0isawjzwmrlhm9npvv5ss82g75rkkr")))

(define-public crate-memorymodule-rs-0.0.2 (c (n "memorymodule-rs") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.58") (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "1icwqpdj7ph7bss6ksr0ipwaffhs6p3wv4kkwbk0hmq34sbww5l6")))

(define-public crate-memorymodule-rs-0.0.3 (c (n "memorymodule-rs") (v "0.0.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10d6p1wafdg541j50ra8z5r4063r389d8m7f5l562c3x49daskhc")))

