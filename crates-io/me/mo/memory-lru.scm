(define-module (crates-io me mo memory-lru) #:use-module (crates-io))

(define-public crate-memory-lru-0.1.0 (c (n "memory-lru") (v "0.1.0") (d (list (d (n "lru") (r "^0.6") (d #t) (k 0)))) (h "02l6laghba4gi7m4a7bmmp6c6hq2kaabmdc1pma00b7ds6rrisxy")))

(define-public crate-memory-lru-0.1.1 (c (n "memory-lru") (v "0.1.1") (d (list (d (n "lru") (r "^0.8.0") (d #t) (k 0)))) (h "15b3j0b5a4n5gsf7pjw0k9wqy6yiwfg94yw52bixgfj0542ax5ff")))

