(define-module (crates-io me mo memoeyes) #:use-module (crates-io))

(define-public crate-memoeyes-0.2.0 (c (n "memoeyes") (v "0.2.0") (d (list (d (n "lru") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04pk5kspshszhqybn1kbgk1s15il90c5rh2ndfdzi0bjzzkvrb3n")))

(define-public crate-memoeyes-0.2.1 (c (n "memoeyes") (v "0.2.1") (d (list (d (n "lru") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f913s648dbrs9mdrvfzvi0yhiv7isjwg4xlkqzxv6z71bf13a1z")))

