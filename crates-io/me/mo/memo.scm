(define-module (crates-io me mo memo) #:use-module (crates-io))

(define-public crate-memo-0.1.0 (c (n "memo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "090j6pqvbpxrnlb0n8ii40wnkracasmzbgb87yky2k1y775i9aks")))

(define-public crate-memo-0.2.0 (c (n "memo") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "0a8sz8fzs1x9vahpnvw1w64zapkpq7xzf0k7vq3nvwzn1qxspm04")))

(define-public crate-memo-0.3.0 (c (n "memo") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "0fa186abar1snh733alqilz6jzc5dc49189d27yw1kp9179ys30y")))

(define-public crate-memo-0.3.1 (c (n "memo") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1wzrpx6r86647cvy0wam9jkj1wwbwlkdlsdnb3h9dm8nci5mb6cv")))

(define-public crate-memo-0.4.0 (c (n "memo") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "0zj5zm9g0b80zkfsblqfbp875bi6hwq73cknj2sb3ghcgzhpj5ka")))

(define-public crate-memo-0.5.0 (c (n "memo") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1jd4cg1knycaa645qah22ibbvp20zxh4mq4niwjlxamcb9s6wflb")))

