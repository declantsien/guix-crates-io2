(define-module (crates-io me mo memoiter) #:use-module (crates-io))

(define-public crate-memoiter-1.0.0 (c (n "memoiter") (v "1.0.0") (h "1gz49d2nb3bvrbwmdxkxhpwqhq22yrz7m7hjf8g5zkwpxhfp2k3f")))

(define-public crate-memoiter-1.0.1 (c (n "memoiter") (v "1.0.1") (h "19hxxlkva5pg3n2h5k3glvnl6x9af8x1k1ha0c74d9f95drh6rrl")))

(define-public crate-memoiter-2.0.0 (c (n "memoiter") (v "2.0.0") (h "1bb9nyybyh29q7l3pylwr5s8fg599zc3wfj4fdv9b1lyvf8r33x3")))

