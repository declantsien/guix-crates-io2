(define-module (crates-io me mo memoryhttpd) #:use-module (crates-io))

(define-public crate-memoryhttpd-0.1.0 (c (n "memoryhttpd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "091nsp92k0sydyr222yfjy91gfjpy8l9438cvpaqqpb02wgy458m")))

(define-public crate-memoryhttpd-0.2.0 (c (n "memoryhttpd") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "server" "runtime"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "time" "macros"))) (d #t) (k 0)))) (h "063k2llg97yb3qfjkzb4ws7ppbpskl03cx9v98cw0dqf060r4swf")))

