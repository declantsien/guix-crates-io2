(define-module (crates-io me mo memonitor-sys) #:use-module (crates-io))

(define-public crate-memonitor-sys-0.1.0 (c (n "memonitor-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1aw039n9ljh4h3nyypc0yykyf5inqnrrg83br17g6faj5rmcxh9w") (f (quote (("vulkan") ("default" "vulkan")))) (l "memonitor-vk")))

(define-public crate-memonitor-sys-0.1.1 (c (n "memonitor-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "06qbqs8cyaal8jjwam3lpd8prqyz05d9vgay81j34nhpgsnqrc50") (f (quote (("vulkan") ("default" "vulkan")))) (l "memonitor-vk")))

(define-public crate-memonitor-sys-0.2.0 (c (n "memonitor-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cfg_aliases") (r "^0.2.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0g9rv1prhbb4gh0xrdi5vhrcpfy2l41pnjqgmk13zhrs27fag2zr") (f (quote (("vulkan") ("default" "vulkan" "cuda") ("cuda")))) (l "memonitor-vk")))

(define-public crate-memonitor-sys-0.2.1 (c (n "memonitor-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cfg_aliases") (r "^0.2.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0an49pic5hzd1yhd2l2iys3nal5vy71sg37ifs19306l6q97hj2l") (f (quote (("vulkan") ("default" "vulkan" "cuda") ("cuda")))) (l "memonitor-vk")))

(define-public crate-memonitor-sys-0.2.2 (c (n "memonitor-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cfg_aliases") (r "^0.2.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "08kkvr9hf55l8gr9jx1jjnh70vwv6b271vkhg755jd9kpahmhsh1") (f (quote (("vulkan") ("default" "vulkan" "cuda") ("cuda")))) (l "memonitor-vk")))

(define-public crate-memonitor-sys-0.2.3 (c (n "memonitor-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cfg_aliases") (r "^0.2.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "00fn917kvjhl614iwq1c83igv8dlm0ya0kcmn9mhpgrmzy835h4y") (f (quote (("vulkan") ("default" "vulkan" "cuda") ("cuda")))) (l "memonitor-vk")))

(define-public crate-memonitor-sys-0.2.4 (c (n "memonitor-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cfg_aliases") (r "^0.2.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "163p8cd702p4c5d40jngkh9pfr7f245ii92mvgj4wvx2mqd7d1sz") (f (quote (("vulkan") ("default" "vulkan" "cuda") ("cuda")))) (l "memonitor-vk")))

