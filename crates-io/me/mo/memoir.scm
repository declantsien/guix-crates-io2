(define-module (crates-io me mo memoir) #:use-module (crates-io))

(define-public crate-memoir-0.1.0 (c (n "memoir") (v "0.1.0") (d (list (d (n "nonempty") (r "^0.2") (d #t) (k 0)))) (h "0c1qidwk2wivcl4lyms0zb54vmccz9npdcjw5920dm93g35gcawk")))

(define-public crate-memoir-0.2.0 (c (n "memoir") (v "0.2.0") (h "0lh8xbnvy349hcfpcn8rn9j97hbgyng24im4dpsiiwngkzsnq3cm")))

(define-public crate-memoir-0.2.1 (c (n "memoir") (v "0.2.1") (h "1p4682m3s69c527a321wy32hkisl1fg2nrfr8qs64hiq938aw3qy")))

(define-public crate-memoir-0.3.0 (c (n "memoir") (v "0.3.0") (h "090b7j5zp0sxcc6s3vapa3vcg0jm5m9yrk99hzz3ls96sliy8pqp")))

