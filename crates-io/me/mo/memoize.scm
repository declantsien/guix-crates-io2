(define-module (crates-io me mo memoize) #:use-module (crates-io))

(define-public crate-memoize-0.1.0 (c (n "memoize") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nhrcaw0jf911sxbr6am1wvxasc4ifyqkcnr6x43kpkh6zmlfn7f")))

(define-public crate-memoize-0.1.1 (c (n "memoize") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17qa8ja12x9qx0l6y0dah8zdq04985myprpd6698xfs7ix40zhz0")))

(define-public crate-memoize-0.1.2 (c (n "memoize") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yzzjqfy2690bzhc4w3mkgvrbg51z8hnr0snssz8qbbl8g2z1404")))

(define-public crate-memoize-0.1.3 (c (n "memoize") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s193hadqrafibb87vin2sazi95dcglqamz3m7hkrghx6dqisspg") (f (quote (("full" "lru") ("default"))))))

(define-public crate-memoize-0.1.4 (c (n "memoize") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l40f349aql1n6la26r218mq80i0sh60qv4pwg4a9sq7gjf36cln") (f (quote (("full" "lru") ("default"))))))

(define-public crate-memoize-0.1.5 (c (n "memoize") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00lv12f54yc5nd771y49ws01vg6rfsvrx9h55najkw88cg879xiv") (f (quote (("full" "lru") ("default"))))))

(define-public crate-memoize-0.1.6 (c (n "memoize") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0synjpbpsdjnnllllb7ppfclc0p3iq3awqh5davm8hij9mkq8ir1") (f (quote (("full" "lru") ("default"))))))

(define-public crate-memoize-0.1.7 (c (n "memoize") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "<1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0x47rgac6nmy9ai304ywvvx1zqz9i53zhilsa8lylshc8bz43alr") (f (quote (("full" "lru") ("default"))))))

(define-public crate-memoize-0.1.8 (c (n "memoize") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0, <1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0bsqws8rs1ffd6fq0cfd1nw5y3vysbizr2fpwmw9s8h563z50irw") (f (quote (("full" "lru") ("default"))))))

(define-public crate-memoize-0.1.9 (c (n "memoize") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ngmd7ypirfwwp29yldwbr62ajxvn58xm8fls4gzsd3m3hvf8jdv") (f (quote (("full" "lru") ("default"))))))

(define-public crate-memoize-0.2.0 (c (n "memoize") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memoize-inner") (r "^0.2.0") (d #t) (k 0)))) (h "0cgidbn318c15jj4zyrar68dgpi9l9cv59lkbdhfq3m4lf3xbsch") (f (quote (("full" "lru" "memoize-inner/full") ("default"))))))

(define-public crate-memoize-0.2.1 (c (n "memoize") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memoize-inner") (r "^0.2.1") (d #t) (k 0)))) (h "12l7xjivpkz7dvbfi9scgh4xwrbayfva9g1993736030r5rq7xbn") (f (quote (("full" "lru" "memoize-inner/full") ("default"))))))

(define-public crate-memoize-0.3.0 (c (n "memoize") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "memoize-inner") (r "^0.2.1") (d #t) (k 0)))) (h "1kb2l7r37z1vkkvrhillbln7zxaz35v65avbkga5a5s1mwack2nx") (f (quote (("full" "lru" "memoize-inner/full") ("default"))))))

(define-public crate-memoize-0.3.1 (c (n "memoize") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "memoize-inner") (r "^0.2.1") (d #t) (k 0)))) (h "0nvfwyjdc6g5m3q1ar0f9n83sagm7ygbfdp0rhhgps3bqq1a3mg7") (f (quote (("full" "lru" "memoize-inner/full") ("default"))))))

(define-public crate-memoize-0.3.2 (c (n "memoize") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "memoize-inner") (r "^0.2.1") (d #t) (k 0)))) (h "1gb01fri98whmb2kivq7yjhyab0iqya67pipwqs0fa5pvlfx2lg5") (f (quote (("full" "lru" "memoize-inner/full") ("default"))))))

(define-public crate-memoize-0.3.3 (c (n "memoize") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "memoize-inner") (r "^0.3") (d #t) (k 0)))) (h "1zqv8aq51fhj3hia9yax15br5mmkwmcgdj471lq17wv381g14pf2") (f (quote (("full" "lru" "memoize-inner/full") ("default"))))))

(define-public crate-memoize-0.4.0 (c (n "memoize") (v "0.4.0") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "memoize-inner") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0daivn2m9ifiwbhix5rzvgb11zg92d8cd9cjsjxdkvzvr6vycaiv") (f (quote (("full" "lru" "memoize-inner/full") ("default" "full"))))))

(define-public crate-memoize-0.4.1 (c (n "memoize") (v "0.4.1") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "memoize-inner") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "1727h6jk78ds797v547n09iys479bzbn347viffwn3w29cryn03m") (f (quote (("full" "lru" "memoize-inner/full") ("default" "full"))))))

(define-public crate-memoize-0.4.2 (c (n "memoize") (v "0.4.2") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lru") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "memoize-inner") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "1f42bqqg57wdb9jd1x9rjc4sx1b346m3nv8r4g7ic21xn4fhbx2x") (f (quote (("full" "lru" "memoize-inner/full") ("default" "full"))))))

