(define-module (crates-io me mo memorable-wordlist) #:use-module (crates-io))

(define-public crate-memorable-wordlist-0.1.0 (c (n "memorable-wordlist") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "10z1fyzkzsyba931c56vvz2wkxs8pncja12h7v84hmhl08izyrl9")))

(define-public crate-memorable-wordlist-0.1.1 (c (n "memorable-wordlist") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "02iyszmibwjgz99zq8ma1vzi6ckj6p6cc9pv6rwfwqjr97xkapsy")))

(define-public crate-memorable-wordlist-0.1.2 (c (n "memorable-wordlist") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "148c3g89fxp7dl9rkqs64m79ll6ccs7pvhqyzv2np2bhylkspr8x")))

(define-public crate-memorable-wordlist-0.1.3 (c (n "memorable-wordlist") (v "0.1.3") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1z4173inl4glr69xfg2qsgsw607csj7hsdadjahww7fyr0vh3gg9")))

(define-public crate-memorable-wordlist-0.1.4 (c (n "memorable-wordlist") (v "0.1.4") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "08h9msfzhqbzv8yhzgvrbz76wpqqdvwcvdqiy93zjjv8v4zcinb4")))

(define-public crate-memorable-wordlist-0.1.5 (c (n "memorable-wordlist") (v "0.1.5") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "166i2cin05fz6xxzr7idm4rm01gzf9b1n1z2064lc14bx45vxbr2")))

(define-public crate-memorable-wordlist-0.1.6 (c (n "memorable-wordlist") (v "0.1.6") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0bnkadgmgl9jfrpx5yawc6kgq2cg6x5rrls1rqnwq56xr7a01758")))

(define-public crate-memorable-wordlist-0.1.7 (c (n "memorable-wordlist") (v "0.1.7") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1h9nxn8c9dirvnqkh2n8a7myx5h52fl6kcxgayjbrw3j5r26lg37")))

