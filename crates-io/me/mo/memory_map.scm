(define-module (crates-io me mo memory_map) #:use-module (crates-io))

(define-public crate-memory_map-0.0.2 (c (n "memory_map") (v "0.0.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1j5kwnkj5yjjx1938llqyj09kxxyldgzz2hi36fnnkpw2hp8mscw") (y #t)))

(define-public crate-memory_map-0.0.3 (c (n "memory_map") (v "0.0.3") (d (list (d (n "kernel32-sys") (r "~0.2.1") (d #t) (k 0)) (d (n "libc") (r "~0.2.2") (d #t) (k 0)) (d (n "tempdir") (r "~0.3.4") (d #t) (k 2)) (d (n "winapi") (r "~0.2.5") (d #t) (k 0)))) (h "11jly7ckb2b586dgyfcss4miimqznp2nk70zdcgi29ypyszgx4na") (y #t)))

