(define-module (crates-io me mo memoverlay) #:use-module (crates-io))

(define-public crate-memoverlay-0.1.0 (c (n "memoverlay") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09w3ss82v1blfhpk66bwc97kin03chvs67g4vgpl0kina0447zms")))

(define-public crate-memoverlay-0.1.1 (c (n "memoverlay") (v "0.1.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z1s85wqsbsllqw5ravvk0wbrxj64cdvk3090wcfb58mfycby2bn")))

(define-public crate-memoverlay-0.1.2 (c (n "memoverlay") (v "0.1.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1asc4z8vw0sf64kp3b1mad5lw6hr4ykr08n28bin23bd6sqlc09j")))

(define-public crate-memoverlay-0.1.3 (c (n "memoverlay") (v "0.1.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fg91c2q6ryfyhl0qazpj568iy5zbxpq8l7gprfaqrbnn8yjihsg")))

