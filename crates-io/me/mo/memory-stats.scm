(define-module (crates-io me mo memory-stats) #:use-module (crates-io))

(define-public crate-memory-stats-1.0.0 (c (n "memory-stats") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "processthreadsapi" "psapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0xh23ysy6fndgpy5i4jgf58ncappyjsqqfwl4zh3gqvn6254agsx")))

(define-public crate-memory-stats-1.1.0 (c (n "memory-stats") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "processthreadsapi" "psapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1k5amihbjv30wbz2z2kcqp9gh4hr7wka3k9s952rap2cjvwrrxrl") (f (quote (("always_use_statm"))))))

