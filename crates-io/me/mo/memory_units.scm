(define-module (crates-io me mo memory_units) #:use-module (crates-io))

(define-public crate-memory_units-0.1.0 (c (n "memory_units") (v "0.1.0") (h "1q4h09z630mv5dmyy7l0l7rbdbbmnypxmawxwbnyx6kz9r300sdf")))

(define-public crate-memory_units-0.2.0 (c (n "memory_units") (v "0.2.0") (h "18m9pq417b1gi718y8pw5nnfphqqr7fp5iw180fsqwb7jbd2bhra")))

(define-public crate-memory_units-0.3.0 (c (n "memory_units") (v "0.3.0") (h "10h89f2ldiaf22x3hyf6nvf3w66biar36g6qrkc2aqqb7hznxnbi")))

(define-public crate-memory_units-0.4.0 (c (n "memory_units") (v "0.4.0") (h "1hyk1alsdpcw5r33c5yn7pk9h259klfxv4vhzx08y1j7l1di0ll4")))

