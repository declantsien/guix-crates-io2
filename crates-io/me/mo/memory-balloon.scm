(define-module (crates-io me mo memory-balloon) #:use-module (crates-io))

(define-public crate-memory-balloon-0.1.0 (c (n "memory-balloon") (v "0.1.0") (d (list (d (n "number_prefix") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "region") (r "^2.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0lha27m6c3q3bx1hqq0rz644q8l9lld9dkh3j3pkqypw83vsmlhv")))

