(define-module (crates-io me ai meain) #:use-module (crates-io))

(define-public crate-meain-0.1.0 (c (n "meain") (v "0.1.0") (h "1bcd93b2nzjkzvdfv0q5lw975i7n38pzka32zkz7b9drc6r2x7ic")))

(define-public crate-meain-0.1.1 (c (n "meain") (v "0.1.1") (h "1m7669qspvbp4nsz6khw82xapxm0ciyxgnnki0c25cwwrh0s6phg")))

