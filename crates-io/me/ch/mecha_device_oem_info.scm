(define-module (crates-io me ch mecha_device_oem_info) #:use-module (crates-io))

(define-public crate-mecha_device_oem_info-0.1.0 (c (n "mecha_device_oem_info") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0c8vzhlmjk0nh47i2h4jv36fczmycyi38aqp6sh7kmdg4h84d25q")))

(define-public crate-mecha_device_oem_info-0.1.1 (c (n "mecha_device_oem_info") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "mac_address") (r "^1.1.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "03shzxn7mmcqgh3xw4yvqh4an7sm3f5ns4f09avx3wl575i00j50")))

