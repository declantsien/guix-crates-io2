(define-module (crates-io me ch mech-math) #:use-module (crates-io))

(define-public crate-mech-math-0.0.4 (c (n "mech-math") (v "0.0.4") (d (list (d (n "libm") (r "^0.1.2") (d #t) (k 0)) (d (n "mech-core") (r "^0.0.4") (d #t) (k 0)) (d (n "mech-utilities") (r "^0.0.4") (d #t) (k 0)))) (h "01wvlvcm1mr68cmzfycn02rzfvsbymmnlarvv64awd87dfx2rab3")))

(define-public crate-mech-math-0.0.5 (c (n "mech-math") (v "0.0.5") (d (list (d (n "libm") (r "^0.1.2") (d #t) (k 0)) (d (n "mech-core") (r "^0.0.5") (d #t) (k 0)) (d (n "mech-utilities") (r "^0.0.5") (d #t) (k 0)))) (h "1xq66871s6c8gwgahcm9qirf1zc9pgcg0195jrxq9vpq16x5mnqm")))

(define-public crate-mech-math-0.1.0 (c (n "mech-math") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "mech-core") (r "^0.1") (k 0)) (d (n "mech-utilities") (r "^0.1") (k 0)))) (h "1m727098vcf0ckk6r0x6p1xi2rkwavhvl0f5k82h7jmd6nfb64gs")))

