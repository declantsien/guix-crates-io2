(define-module (crates-io me ch mecha_display_ctl) #:use-module (crates-io))

(define-public crate-mecha_display_ctl-0.1.0 (c (n "mecha_display_ctl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "14zj0qmxq15kljvjlf5fi5bz6i763dc637742d5nr5lbg9smg2d3")))

