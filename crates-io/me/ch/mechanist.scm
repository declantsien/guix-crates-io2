(define-module (crates-io me ch mechanist) #:use-module (crates-io))

(define-public crate-mechanist-0.1.0 (c (n "mechanist") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "0vgxdmpwpfqfwpa9nrjvhwg2k9cyvdmzb9rl45wra2izy0l9xcqh")))

