(define-module (crates-io me ch mech-system) #:use-module (crates-io))

(define-public crate-mech-system-0.1.0 (c (n "mech-system") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mech-core") (r "^0.1") (k 0)) (d (n "mech-utilities") (r "^0.1") (d #t) (k 0)))) (h "12kq0n0pwav5l5y6ba5gaspwaflnm6dj5j68p1m1cb4qkwx3xi9b")))

