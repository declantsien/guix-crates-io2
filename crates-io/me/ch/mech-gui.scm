(define-module (crates-io me ch mech-gui) #:use-module (crates-io))

(define-public crate-mech-gui-0.1.0 (c (n "mech-gui") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "eframe") (r "^0.18.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (f (quote ("ico" "jpeg" "png"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mech-core") (r "^0.1") (d #t) (k 0)) (d (n "mech-syntax") (r "^0.1") (d #t) (k 0)) (d (n "mech-utilities") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)) (d (n "tracing-wasm") (r "^0.2.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "0bvpwhlvm2whz8c5gmyv50iv49n25vlma4cva1z91kh2z60m4f9w")))

