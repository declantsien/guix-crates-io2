(define-module (crates-io me ch mechanical-engineering) #:use-module (crates-io))

(define-public crate-mechanical-engineering-0.0.0 (c (n "mechanical-engineering") (v "0.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)))) (h "02rmid45nsbyl52q7q6anm5bv6m882n0jvgwm1bwhgvz68x0mji2")))

