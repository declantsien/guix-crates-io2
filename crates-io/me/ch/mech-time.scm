(define-module (crates-io me ch mech-time) #:use-module (crates-io))

(define-public crate-mech-time-0.1.0 (c (n "mech-time") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mech-core") (r "^0.1") (k 0)) (d (n "mech-utilities") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "1rvmj7zypj6wzxb53280a4cxqk5d6zlhyjmiyd16md00drghpjw8")))

