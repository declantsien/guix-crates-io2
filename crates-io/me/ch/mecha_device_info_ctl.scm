(define-module (crates-io me ch mecha_device_info_ctl) #:use-module (crates-io))

(define-public crate-mecha_device_info_ctl-0.1.0 (c (n "mecha_device_info_ctl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01g6wvri9vhm456iv82sn1zqfjan2mh5rv0978hnhyazs0fynmq3")))

