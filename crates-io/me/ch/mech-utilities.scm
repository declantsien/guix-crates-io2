(define-module (crates-io me ch mech-utilities) #:use-module (crates-io))

(define-public crate-mech-utilities-0.0.3 (c (n "mech-utilities") (v "0.0.3") (d (list (d (n "mech-core") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.96") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.96") (d #t) (k 0)))) (h "0ygpv25702b9spwdvy3pk8kcvjc2s59rix0f5ralk9h45j3vwcl1")))

(define-public crate-mech-utilities-0.0.4 (c (n "mech-utilities") (v "0.0.4") (d (list (d (n "mech-core") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)))) (h "0b3654ran8njrlj36rkcsp42rl2xrc3d4a9caffiihsr09hv80hh")))

(define-public crate-mech-utilities-0.0.5 (c (n "mech-utilities") (v "0.0.5") (d (list (d (n "hashbrown") (r "^0.7.1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "mech-core") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.105") (d #t) (k 0)))) (h "08gk5r3y1x27znhlpdvzjvhwf0a1rlr7s708vni3y7h5syzfxbaw")))

(define-public crate-mech-utilities-0.1.0 (c (n "mech-utilities") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "mech-core") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("alloc"))) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "websocket") (r "^0.26.5") (f (quote ("sync"))) (o #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "02bg4rnii4g9ni8n1g36v6krnbhwq19ygyjw484r0dw4fz12s7iy") (f (quote (("web" "websocket") ("default" "web"))))))

