(define-module (crates-io me ch mecha_battery_ctl) #:use-module (crates-io))

(define-public crate-mecha_battery_ctl-0.1.0 (c (n "mecha_battery_ctl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0fhcgd3bh4zv8ci92n9zpqjlhjpyrr7ippd6qwss21a5zk0sxy6d")))

