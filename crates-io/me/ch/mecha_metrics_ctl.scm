(define-module (crates-io me ch mecha_metrics_ctl) #:use-module (crates-io))

(define-public crate-mecha_metrics_ctl-0.1.0 (c (n "mecha_metrics_ctl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0kl2nb156i3z81s1jpd7zaxsx54b60wbp6md7m7iqg3ivswymz01")))

