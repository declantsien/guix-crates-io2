(define-module (crates-io me ch mechtron-common) #:use-module (crates-io))

(define-public crate-mechtron-common-0.1.0 (c (n "mechtron-common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "starlane-resources") (r "^0.1.0") (d #t) (k 0)))) (h "1gic4hdg2iagzcdlppv3ga7mj2ica5zqwjwzk6ifnbka091qjiqv")))

(define-public crate-mechtron-common-0.2.0-rc1 (c (n "mechtron-common") (v "0.2.0-rc1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "mesh-portal") (r "^0.2.0-rc1") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1cpw847h2zibzzl7cdxnbzb0n2jgvvzs0j89jkva4m2jpd2sksrm")))

