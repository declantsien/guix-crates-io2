(define-module (crates-io me ch mecha_led_ctl) #:use-module (crates-io))

(define-public crate-mecha_led_ctl-0.1.0 (c (n "mecha_led_ctl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0fmsm6kdw4rhczmixbp6579nclfqmsvd2bms726qn8fxfwfl628p")))

