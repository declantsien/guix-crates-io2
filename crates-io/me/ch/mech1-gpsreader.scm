(define-module (crates-io me ch mech1-gpsreader) #:use-module (crates-io))

(define-public crate-mech1-gpsreader-0.1.0 (c (n "mech1-gpsreader") (v "0.1.0") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "12qwscbpwiia705dpvyk2yq968pghn3j56q9nskbm7bzd3qm0hrw")))

(define-public crate-mech1-gpsreader-0.1.1 (c (n "mech1-gpsreader") (v "0.1.1") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0scql2bi01jyx1vwjyvhfn0gic4rlipil1qnb22834fy90dj2n2d")))

(define-public crate-mech1-gpsreader-0.2.0 (c (n "mech1-gpsreader") (v "0.2.0") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0npj2cj0f0cd501aixd3flki62mncmaga9anpb4p1x41675m1x5n")))

(define-public crate-mech1-gpsreader-0.3.0 (c (n "mech1-gpsreader") (v "0.3.0") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0h9pmvifl8srrn7xqjmgrdhmxy01w92z24dd75qzva2smq9qq6j8")))

(define-public crate-mech1-gpsreader-0.4.0 (c (n "mech1-gpsreader") (v "0.4.0") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1dkg8vfdr2wm29vdsafckpkgvrqyvxl26rzg6a8l2b7jqha2s5an")))

