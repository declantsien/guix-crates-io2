(define-module (crates-io me ch mecha_bluetooth_ctl) #:use-module (crates-io))

(define-public crate-mecha_bluetooth_ctl-0.1.0 (c (n "mecha_bluetooth_ctl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "bluer") (r "^0.16.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0mcx1zfjcsci2xb2a6pn6yhl45rxx8q2k5s0m6j81mja844z43fm")))

