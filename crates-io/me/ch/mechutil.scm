(define-module (crates-io me ch mechutil) #:use-module (crates-io))

(define-public crate-mechutil-0.1.5 (c (n "mechutil") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0h4mc6a9zlabkx7paxnvqnlvr871djk5mzwl0rvhliaab37gk9ab")))

(define-public crate-mechutil-0.1.7 (c (n "mechutil") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "068jqn9sfnhb1bjxs7nq9zrajli9fzk1xjyszjf0a9c908sqwxp7")))

(define-public crate-mechutil-0.2.0 (c (n "mechutil") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0kmv3i1qgmaivgklngn51bn6fnr7fkqp8n5gfzsbs07f85wxa8ar")))

(define-public crate-mechutil-0.2.1 (c (n "mechutil") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0vl8q0nbgv25xp2wmj41brw18fgblz9lvw0ncxfc8zm5q6jksfbv")))

(define-public crate-mechutil-0.2.2 (c (n "mechutil") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "028q782gc527azm47dvwz0zkrjlimbmqjinp97sydy5fqx49gd5v")))

