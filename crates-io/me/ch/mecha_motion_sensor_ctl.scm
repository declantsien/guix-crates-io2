(define-module (crates-io me ch mecha_motion_sensor_ctl) #:use-module (crates-io))

(define-public crate-mecha_motion_sensor_ctl-0.1.0 (c (n "mecha_motion_sensor_ctl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0s80p6ygv6ib1jk493rsvbfk9arz90fx8adnysv5s1pg89vhbi9b")))

