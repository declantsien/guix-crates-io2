(define-module (crates-io me tf metfor) #:use-module (crates-io))

(define-public crate-metfor-0.2.0 (c (n "metfor") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0jwch13j00l07syd833f4j3wqw7q7zcvybgxj2c76hn27gwiwlln")))

(define-public crate-metfor-0.2.1 (c (n "metfor") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0lzvk365ybxh10zv6ln3kwpfklh865r4z3xg6j05d4yrsyv7friy")))

(define-public crate-metfor-0.2.2 (c (n "metfor") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0z31hlxhmfc4v727jsb7gwinii5z1yps05pqr2haz9q6x6nm5x9x")))

(define-public crate-metfor-0.2.3 (c (n "metfor") (v "0.2.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "122i5ghnmygczvdxrbbd1mppk4xn6ng72clvh09vvsz8s8m7xzw1")))

(define-public crate-metfor-0.2.4 (c (n "metfor") (v "0.2.4") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0svbwr76rs30399rzrlq442phvcw1lmn7a5nj2dkd4n6vss2hyg3")))

(define-public crate-metfor-0.3.0 (c (n "metfor") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "067zbyashcr8yplbd0si4kx7p6ls3kcp3kjyqnqrl5hym2bkggcj")))

(define-public crate-metfor-0.3.1 (c (n "metfor") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0csyrrclw1dcw8pi83c6rs10vp5m62w7nmh4nqpjxf78dvzh529x")))

(define-public crate-metfor-0.3.2 (c (n "metfor") (v "0.3.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0dikascbf386zhnglagi8526grg73hl8zrcvh4cc44vsh4rqkcvd")))

(define-public crate-metfor-0.4.0 (c (n "metfor") (v "0.4.0") (h "0iw6x99zkpn4v9slryb4g1wfsbkvcmmc197ss20sjil2fqf97amj")))

(define-public crate-metfor-0.4.1 (c (n "metfor") (v "0.4.1") (h "15jvz695m0dhin679av1snrzrz8inqw2qyvi3y21idabmwd5p8vf")))

(define-public crate-metfor-0.5.0 (c (n "metfor") (v "0.5.0") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0vil0779sbks9z2cr52r70rk501y4wf6vwbi6p8j9hm5yn3i1by6") (f (quote (("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.6.0 (c (n "metfor") (v "0.6.0") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "192jp3gxjin6m3jkqy0l29m94q34hxswb9fyp83hj8lh454wvbb9") (f (quote (("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.6.1 (c (n "metfor") (v "0.6.1") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0sw1rx0v43s0ilb5yrwf0s78zhcx8kp1f50f0b8f19sayny5r0wf") (f (quote (("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.6.2 (c (n "metfor") (v "0.6.2") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1759l0vhghgbl2i1gmg3zc12r3sc0ich17kxd45qpxnr96zkf905") (f (quote (("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.6.3 (c (n "metfor") (v "0.6.3") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1ij9vv0471kazn3bncrm5mkb24zld8wh26lpcyynkr2mzvgbg37f") (f (quote (("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7.0 (c (n "metfor") (v "0.7.0") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13d229a1cqacfbm6zn3chs6x020xjj42qmlfdmy989d3j2rvbcbv") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7.1 (c (n "metfor") (v "0.7.1") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1pbcjw385fj6h2ih0iik433rnnbich8l4izrdm7ydz9p94pdm3w0") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7.2 (c (n "metfor") (v "0.7.2") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0w4x9gmkws47hqgrh78idw5pqp0jsr7ln527zc1s4mx8gscjawl4") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7.3 (c (n "metfor") (v "0.7.3") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "143wa5k985ylkpdf7dpx027xrxcrsqdwc66lbvp048hdpwv48qlg") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default")))) (y #t)))

(define-public crate-metfor-0.7.4 (c (n "metfor") (v "0.7.4") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1raiq8775g0pf3bqikkk1mkf8mbibmx9n9iyqq7vcdwv710dv6k9") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7.5 (c (n "metfor") (v "0.7.5") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0z7rh5iy8qmbbjzj53ycn33sf3jcsadd82hshi81h6kbak3nljw3") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7.6 (c (n "metfor") (v "0.7.6") (d (list (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03yvq97l4x7phkn3ww519dzqxaicv0nzi6fwzxini2kzmpv6i7i6") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.8.1 (c (n "metfor") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "02vbzzhvc4xnp3rh2nics6rfkp1gprlk2pf2xh5zd35qrlyx4msa") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.8.2 (c (n "metfor") (v "0.8.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qfbf1yjsrrv4csafkka0kqcxf3wnzr2cysp1gia1ky5fxyb70jk") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.8.3 (c (n "metfor") (v "0.8.3") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ilrnz1r0p8dxg8w19717cq69xk2vdpa9hmm33gih9bzbhl92r38") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.9.0 (c (n "metfor") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "optional") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1gpkwhgf016i8h5y40zcb055vazi22l5jm30z5p0sygx1bkirpaw") (f (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

