(define-module (crates-io me de medea-reactive) #:use-module (crates-io))

(define-public crate-medea-reactive-0.1.0 (c (n "medea-reactive") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core" "rt-util" "test-util" "time"))) (d #t) (k 2)))) (h "1164zfbpzhdlvxi56q1qd0wbbaz49bw4mixmp45imswc688kqjd8")))

(define-public crate-medea-reactive-0.1.1 (c (n "medea-reactive") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core" "rt-util" "test-util" "time"))) (d #t) (k 2)))) (h "1knv3449lv23nli7nmhfmbj1k382i77jwhf8mppckkpxrdz2qyiw")))

(define-public crate-medea-reactive-0.1.2 (c (n "medea-reactive") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("macros" "rt" "test-util" "time"))) (d #t) (k 2)))) (h "09x0pwpiwb9frg5zzx6r3ijmsc2h708ar38129k2dy81a9n0w5vx") (r "1.64")))

