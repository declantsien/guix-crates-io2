(define-module (crates-io me de medea-macro) #:use-module (crates-io))

(define-public crate-medea-macro-0.1.0 (c (n "medea-macro") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0bvcjgclwkhvlmmc5kry0sh3flvm5i14wk1bfqh5z3633q9db8bh")))

(define-public crate-medea-macro-0.2.0 (c (n "medea-macro") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "08s58mfx7dsf55pi898wsrc373v3hi0r40hs7404qmbka73p7vc5")))

(define-public crate-medea-macro-0.2.1 (c (n "medea-macro") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0bd5v6cbghddn8i98vbrzdk4z8w22nfjgj8qvfx1imcb2q2pvqyx")))

(define-public crate-medea-macro-0.3.0 (c (n "medea-macro") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)))) (h "0703ag7yxnxh6xrgbs9av1whm09nrvqp6h6rpfmazlppfi20b7b6") (f (quote (("dart-codegen")))) (r "1.65")))

