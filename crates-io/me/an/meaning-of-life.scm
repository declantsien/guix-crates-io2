(define-module (crates-io me an meaning-of-life) #:use-module (crates-io))

(define-public crate-meaning-of-life-0.1.0 (c (n "meaning-of-life") (v "0.1.0") (h "1ds514w2dy3m7aqccn0rpdld4lps6m7a400ndzdky21qx2lnhdjl") (y #t)))

(define-public crate-meaning-of-life-42.42.42 (c (n "meaning-of-life") (v "42.42.42") (h "0xq2x354gmm9k8p15gn18z82g4b117wkh6araw92z8mif61d5776")))

