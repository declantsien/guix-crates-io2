(define-module (crates-io me an meaningsearch) #:use-module (crates-io))

(define-public crate-meaningsearch-0.1.0 (c (n "meaningsearch") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "18c6j8w2nz81mgib899yarhq5z07sj5dhgmrdv1bjah0krmwarh6") (y #t)))

(define-public crate-meaningsearch-0.1.1 (c (n "meaningsearch") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1hgnrsg34wk5jkd1savjhj6d27aqg01gqcnqq1rrddmx6pk94y8h") (y #t)))

(define-public crate-meaningsearch-0.1.2 (c (n "meaningsearch") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1dpc6mqmpc7vi1s4bry7835j3igfc4a3rmhvkp8swgcq7i20chnr") (y #t)))

(define-public crate-meaningsearch-0.1.3 (c (n "meaningsearch") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0zn3k0ljy6dfn3xfkg2r837lc7g3w7ib6ykj64k7imnpccyvd4hg")))

(define-public crate-meaningsearch-0.1.4 (c (n "meaningsearch") (v "0.1.4") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0zqinvsjw7fk4n3bh4azr488aa8balzg8xr1vihbnhdwvsbyc2rw")))

