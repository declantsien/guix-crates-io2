(define-module (crates-io me an meansd) #:use-module (crates-io))

(define-public crate-meansd-1.0.0 (c (n "meansd") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.6") (d #t) (k 2)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (k 0)))) (h "1n16kq9fw7imkipba1m4904clzbi8f6pcdk20l8d439m3h8miyax")))

(define-public crate-meansd-1.1.0 (c (n "meansd") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (k 0)))) (h "0vwhawp7vb0s6nia8iry3yzcsmy506a1w7wz5c33jgn0px3hzkqk")))

(define-public crate-meansd-1.1.1 (c (n "meansd") (v "1.1.1") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (k 0)))) (h "00i8x0pc4gvzv16fl294cq5xsrr4x4nddmyvxqlzdbzrrfamgvcj")))

(define-public crate-meansd-1.2.0 (c (n "meansd") (v "1.2.0") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)))) (h "10wz0j62kxc4zrki1xlivxz7gchh8a0bhya4mam9qd2366nfr47n")))

(define-public crate-meansd-2.0.0 (c (n "meansd") (v "2.0.0") (d (list (d (n "float-cmp") (r "^0.8") (d #t) (k 2)) (d (n "ordered-float") (r "^2") (d #t) (k 0)))) (h "0gishhcpn3a9070ai4a567f3a2n4z89qwy7wi7ssgq0nb1myxlqd")))

(define-public crate-meansd-2.1.0 (c (n "meansd") (v "2.1.0") (d (list (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "ordered-float") (r "^3") (d #t) (k 0)))) (h "18w6gssz6wyibg718qgvxigrlqkpas00ld6a8xpb0w0rpfj9zszb") (r "1.68.2")))

