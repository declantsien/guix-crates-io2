(define-module (crates-io me an meansd-cli) #:use-module (crates-io))

(define-public crate-meansd-cli-1.2.0 (c (n "meansd-cli") (v "1.2.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "meansd") (r "^1.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (k 0)))) (h "07hm26dc5zdy37mzb5qixmji2qwai9nhii636c92fnnpj801jwcf")))

(define-public crate-meansd-cli-1.3.0 (c (n "meansd-cli") (v "1.3.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "meansd") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.8") (k 0)) (d (n "smooth") (r "^0.1") (d #t) (k 0)))) (h "05ws1d1j85jifjkb3nlyhnbsndaj1b7g1r6l0k36c8sygxyd7pgh")))

(define-public crate-meansd-cli-1.4.0 (c (n "meansd-cli") (v "1.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("cargo" "wrap_help"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "meansd") (r "^2") (d #t) (k 0)) (d (n "ordered-float") (r "^3") (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.10") (k 0)) (d (n "smooth") (r "^0.2") (d #t) (k 0)))) (h "0a4z0gkqpb91bycddr62cqclacy2jsr3l656v9pd1wwh7pnkmh68") (r "1.70.0")))

