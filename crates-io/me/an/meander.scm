(define-module (crates-io me an meander) #:use-module (crates-io))

(define-public crate-meander-0.1.0 (c (n "meander") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0a1yjgxpmwyqcqk7cb7jdhgh0gh2b6snx6is8xywmva7ml8fzdyq")))

(define-public crate-meander-0.1.1 (c (n "meander") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0b6xf6mcb4fb9b9nq42kwz4d6g3bfxcvyarr0ygnkk0qv11a3mva")))

