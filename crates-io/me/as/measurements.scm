(define-module (crates-io me as measurements) #:use-module (crates-io))

(define-public crate-measurements-0.1.0 (c (n "measurements") (v "0.1.0") (h "02m2kycv39nac33fjpzqwm17cn4dxhrpsgzl168ha940hsn0i82n")))

(define-public crate-measurements-0.2.0 (c (n "measurements") (v "0.2.0") (h "1x36nbpzjaaxyk8chh7g60b0swvdqllnv73icv3kab52wbsfhbxv")))

(define-public crate-measurements-0.2.1 (c (n "measurements") (v "0.2.1") (h "15hqig0gnkz7qjb36vrbyw7pqkq8smr8s3k8v6y058i1yrqh689y")))

(define-public crate-measurements-0.3.0 (c (n "measurements") (v "0.3.0") (h "1yph3p3jl40fzbr1j45mp2qpnhn6w2gbx00lw47l3a9xwnmpkpqh")))

(define-public crate-measurements-0.4.0 (c (n "measurements") (v "0.4.0") (h "0b78z4ras2p08hy8b7pkjvcb5bdw96av4kwh1fgpc3lbwa1vmh06")))

(define-public crate-measurements-0.6.0 (c (n "measurements") (v "0.6.0") (h "175cky043s998n2gcjfb34iv9vk4wy2dl2plsh5idhjl1n3rf28r")))

(define-public crate-measurements-0.7.0 (c (n "measurements") (v "0.7.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "00da1pvd5lk7kbk6faii6d3k6n4as8771six5s4m67y3fqxg2j9h")))

(define-public crate-measurements-0.8.0 (c (n "measurements") (v "0.8.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1hq8yjwymwnnjvaxn9s94lp7xg895agm62h1cxd7bs5mpj63lvs4")))

(define-public crate-measurements-0.9.0 (c (n "measurements") (v "0.9.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0cim67narcvvcvj1q2ddkqvlgmlzml8hyc9ph8wrbig35klc5zis")))

(define-public crate-measurements-0.10.0 (c (n "measurements") (v "0.10.0") (h "1dls5g5dlivwq5489i4cx6madgp58rvchk64vap3s2rlaph0ah7q") (f (quote (("no_std"))))))

(define-public crate-measurements-0.10.1 (c (n "measurements") (v "0.10.1") (h "0znd3xh4l1sps6mfv3v8ild7k9l2h7zk20l7zy5c4rwj3binjg9z") (f (quote (("no_std"))))))

(define-public crate-measurements-0.10.2 (c (n "measurements") (v "0.10.2") (h "1fyysjylzy59ylxfwgs0f3r6dwa4a6mjc1c63c8si87hfysjzxvd") (f (quote (("no_std"))))))

(define-public crate-measurements-0.10.3 (c (n "measurements") (v "0.10.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jrsvgh4r6idjhbi0np9mzqylnfrhis5x7wr0sp02p5pph7jn47v") (f (quote (("no_std"))))))

(define-public crate-measurements-0.10.4 (c (n "measurements") (v "0.10.4") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dgqfi686qrr5gasm7s9rbvl7kgw02y78zinmg1bl4h4x1yjj7r8") (f (quote (("no_std" "libm") ("from_str" "regex"))))))

(define-public crate-measurements-0.11.0 (c (n "measurements") (v "0.11.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0jwhrb0ayb7kba7v8q90wj6pv8kh15phi762gdvsazhqx2s39dzm") (f (quote (("std") ("from_str" "regex" "std"))))))

