(define-module (crates-io me as measured-tokio) #:use-module (crates-io))

(define-public crate-measured-tokio-0.0.21 (c (n "measured-tokio") (v "0.0.21") (d (list (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "measured") (r "^0.0.21") (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)))) (h "182j08865yvjrmdhk9c6lq64372l6i4hj3lxwf2kmnnv7mvx0khb") (f (quote (("net" "tokio/net") ("default"))))))

