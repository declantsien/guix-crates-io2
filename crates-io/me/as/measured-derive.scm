(define-module (crates-io me as measured-derive) #:use-module (crates-io))

(define-public crate-measured-derive-0.0.5 (c (n "measured-derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05081m2d3n7c5vgrqlkdjajih715sjcjml934d7lyrji4ipnf07m")))

(define-public crate-measured-derive-0.0.6 (c (n "measured-derive") (v "0.0.6") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "189ncpgq1gvbnj08qizi2lzww4kkq2qfv27zzaxkmxrbw3p6zz0r")))

(define-public crate-measured-derive-0.0.7 (c (n "measured-derive") (v "0.0.7") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ni7lpqqxhp9bz8jq3h2gdcldknadyvn61z6493p0qxxdf44rxf4")))

(define-public crate-measured-derive-0.0.8 (c (n "measured-derive") (v "0.0.8") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kf4q35arlrdzy93jx2iy2gy7j2p310zf7bs1lpzbymkaakvc5kb")))

(define-public crate-measured-derive-0.0.9 (c (n "measured-derive") (v "0.0.9") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ryyrpz5gcs4nyyfk2xq2w4h5gfk8pd72426np9frljmy0lpwqww")))

(define-public crate-measured-derive-0.0.10 (c (n "measured-derive") (v "0.0.10") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i2d1b3rcz8wzga5nncy98zkcbqq00sfsm4jscv66lm780s8jgfl")))

(define-public crate-measured-derive-0.0.11 (c (n "measured-derive") (v "0.0.11") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "179c94m18m14kj6fnws41db876lb09l9izndmia39i2v3darn4y7")))

(define-public crate-measured-derive-0.0.12 (c (n "measured-derive") (v "0.0.12") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cczp2b0i22hn193z8ldg0z02762r5iz70gdsv1vsakpvrp2b4ma")))

(define-public crate-measured-derive-0.0.13 (c (n "measured-derive") (v "0.0.13") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "104vbl0fvlpcmnhn4p8v5z13903zgrgmnfwwv7bxdmcr5p15rapd")))

(define-public crate-measured-derive-0.0.14 (c (n "measured-derive") (v "0.0.14") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17wx4kam13hvi53jf3ym68n2l3dgc1jp00fka709p7w72ar6aszz")))

(define-public crate-measured-derive-0.0.15 (c (n "measured-derive") (v "0.0.15") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05mzgjzbgkl07m0dmcb108k6q82n16k2vxfvkd8xsla0qm17fd8j")))

(define-public crate-measured-derive-0.0.16 (c (n "measured-derive") (v "0.0.16") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hfmshfkbgwkphn5id1f6fc73dgvsskyafq1wlw33ch5ivw733ix")))

(define-public crate-measured-derive-0.0.17 (c (n "measured-derive") (v "0.0.17") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18qid04db1lvj7gisg605chq3qbjawnpqb4bcf4x28qfcqdbmgnq")))

(define-public crate-measured-derive-0.0.18 (c (n "measured-derive") (v "0.0.18") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kmfjfsh9q72xchy9l45700yfh3zc2fgf802m8mzfrcib2kypai5")))

(define-public crate-measured-derive-0.0.19 (c (n "measured-derive") (v "0.0.19") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05sg9n9dcin378fwqb0z2sxs4i72mh4japkblvp7c3zz83i8m398")))

(define-public crate-measured-derive-0.0.20 (c (n "measured-derive") (v "0.0.20") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vvxlngssdq6a3rzc8k8l3sskcaa0mah95lzm3r8m3xkhav2k7my")))

(define-public crate-measured-derive-0.0.21 (c (n "measured-derive") (v "0.0.21") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pb898db0hpqk6ii5ydigmckrgd0d5pr3b9jdhvnm18y7vrrg93f")))

