(define-module (crates-io me as measure) #:use-module (crates-io))

(define-public crate-measure-1.0.0 (c (n "measure") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.31.2") (f (quote ("suggestions" "color"))) (d #t) (k 0)))) (h "1mbyr1j60ybfwkh5dfbmx7k8w0iamb5a3g51k8f3la18xbcdz7p9")))

(define-public crate-measure-1.0.1 (c (n "measure") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.31.2") (f (quote ("suggestions" "color"))) (d #t) (k 0)))) (h "1xx682r8rx3df4xdnwyryaljwf7b69w4kqnk7cp59k5bj5vbbxjm")))

(define-public crate-measure-1.1.0 (c (n "measure") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.31.2") (f (quote ("suggestions" "color"))) (d #t) (k 0)))) (h "0nxjm2f075g3n1y1zx6fydiagpp91mymalqn54snriphz370pqj7")))

(define-public crate-measure-1.1.1 (c (n "measure") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.31.2") (f (quote ("suggestions" "color"))) (d #t) (k 0)))) (h "0kpfzp9gx5w1rc3yjizbxyw8pgrbnb9bdm74azvv3pxpwjyvg4md")))

(define-public crate-measure-1.1.2 (c (n "measure") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.31.2") (f (quote ("suggestions" "color"))) (d #t) (k 0)))) (h "1x7hngg0qaadl7v1pxr3lwqksp3hcyvsdsnpq0vs74jbahfslm7c")))

(define-public crate-measure-1.1.3 (c (n "measure") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.31.2") (f (quote ("suggestions" "color"))) (d #t) (k 0)))) (h "0hlhwalslbd92cwyi4kr9l2csrx6zrc491d6j48d24hqpfg7ljyh")))

