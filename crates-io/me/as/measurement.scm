(define-module (crates-io me as measurement) #:use-module (crates-io))

(define-public crate-measurement-0.1.0 (c (n "measurement") (v "0.1.0") (h "06c19h9x8065zxl0g0wzkhcwys0y5sharr73hsdfirs473vqr51z") (y #t)))

(define-public crate-measurement-0.1.1 (c (n "measurement") (v "0.1.1") (h "183hqdbks6hqf8j3n7k0w6d0v7jxig1mj8bllayi09snqf8r3vhq") (y #t)))

(define-public crate-measurement-0.2.0 (c (n "measurement") (v "0.2.0") (h "0csz9h3qvv9xqv7swabihkprhiw31rzghfgvcqs3gqa4i2cz88fj")))

(define-public crate-measurement-0.3.0 (c (n "measurement") (v "0.3.0") (h "0fygl8pcf1kp48yqr5w1idwdp4x7mbi4c9w58zlns527ihn17q87")))

