(define-module (crates-io me as measured-future-rs) #:use-module (crates-io))

(define-public crate-measured-future-rs-0.1.0 (c (n "measured-future-rs") (v "0.1.0") (d (list (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0q1zkpykp5qkb5bnqpd0xic52hwxi5swk46w20pydlacabmra7hv")))

(define-public crate-measured-future-rs-0.1.1 (c (n "measured-future-rs") (v "0.1.1") (d (list (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1bzm5rybdqvwdl0mi0gnnb9ig3y822i1dirdisslpwf06i5940wq")))

(define-public crate-measured-future-rs-0.1.2 (c (n "measured-future-rs") (v "0.1.2") (d (list (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0vm5780nymlwc4w7nvflh23784g2lfwxd0g92r0pkwrf64waa5nj")))

(define-public crate-measured-future-rs-0.2.0 (c (n "measured-future-rs") (v "0.2.0") (d (list (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0w7k122qb84bl5n06mldys0rv5py8xi0bv13mgsf5cbck6lgmv7x")))

(define-public crate-measured-future-rs-0.2.1 (c (n "measured-future-rs") (v "0.2.1") (d (list (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1d9s53ag1qcgf81ylaphr8w1476ahcqis31w8kz8zqyjwgv5diy7")))

(define-public crate-measured-future-rs-0.2.2 (c (n "measured-future-rs") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1qv7r3v5i9d2j4f1amd9ifwsxrvyfby19aa2lahw7l27n0h63p88")))

(define-public crate-measured-future-rs-0.3.0 (c (n "measured-future-rs") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "stat") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "16gqn6g1xlp66hf26gz709h6586xxjcb6insbam8jpz5sg5c3b7z")))

(define-public crate-measured-future-rs-0.3.1 (c (n "measured-future-rs") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "stat") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1866xrvy85pbkr5n76r0j1pg63nw3h6lifrf1mrm4y8ygdn8vrbm")))

(define-public crate-measured-future-rs-0.3.2 (c (n "measured-future-rs") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "stat") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "02jwhr1zwdknnchp58b6r5d0h39l30vgrs921brff80gq7h9cvar")))

(define-public crate-measured-future-rs-0.4.0 (c (n "measured-future-rs") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "stat") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "18mibzbxkk9hyih49gczw8gmmizmi2j6ggym71rfkqxnn25lbd1h") (y #t)))

(define-public crate-measured-future-rs-0.4.1 (c (n "measured-future-rs") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "polymap") (r "^0.3") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stat") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1fzpz2k1175bpyhflfdcvgz20mybc2hr8ksjvnf3cxbwn3lzq7di") (f (quote (("default") ("debug-logs" "log"))))))

(define-public crate-measured-future-rs-0.4.2 (c (n "measured-future-rs") (v "0.4.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "polymap") (r "^0.3") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "stat") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "10ihsr193n1j1pvh6i8lnbci7m72ja7h2m73wwhkkhb3xj5hk1fj") (f (quote (("default") ("debug-logs" "log"))))))

