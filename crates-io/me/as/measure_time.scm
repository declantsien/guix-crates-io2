(define-module (crates-io me as measure_time) #:use-module (crates-io))

(define-public crate-measure_time-0.1.0 (c (n "measure_time") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0ra83z9zwmma9ddw55m4ri9sfqgbnf7l001g3jbxvfafaiim8qdv")))

(define-public crate-measure_time-0.1.1 (c (n "measure_time") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0400db6qxhhw6kdvc9a51rj70fpmvvf77478kahsa3sz85lvvxw8")))

(define-public crate-measure_time-0.1.2 (c (n "measure_time") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1s1ypbkn3qs7hw35cbv6m6dbri316h8283qy1iay8dzhvxfgxp7j")))

(define-public crate-measure_time-0.1.3 (c (n "measure_time") (v "0.1.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1rg27k3b58ccwhmhbcscvhgqdmr8vpzmqy01dk71b0dwsk9xnppd")))

(define-public crate-measure_time-0.1.4 (c (n "measure_time") (v "0.1.4") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0fpyh16mxgklbiqddq69vdihz5wd7w1xsm0hrm62kx3khjxzp66m")))

(define-public crate-measure_time-0.1.5 (c (n "measure_time") (v "0.1.5") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "08img0ps9pmkjj3niygbn4b0payaddr55r2ng4wbdsr0vw02ng7w")))

(define-public crate-measure_time-0.1.6 (c (n "measure_time") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17kca26s8z5w2a3hs7cqlv3s5cx36d1n71xsdywnfbr2y1ak2ixn")))

(define-public crate-measure_time-0.4.0 (c (n "measure_time") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1d2l3idfh44780jaz0zdmrzd196q2cqh8p299r6vk3wkd1fibl7y")))

(define-public crate-measure_time-0.4.1 (c (n "measure_time") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1mz5d4qdnghbf11mwgwdblnlp84jx3l4l3vjfkn8hg528zzydrih")))

(define-public crate-measure_time-0.4.2 (c (n "measure_time") (v "0.4.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0krybzypp9cv3g5ippcllxjqbcv9h5i5ww570kipy5iqfbh4b8i8")))

(define-public crate-measure_time-0.5.0 (c (n "measure_time") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1i9ykbp1bxmawslyarcf7n78x1l9k3x5bipm4dwj62xabfl7zwix")))

(define-public crate-measure_time-0.6.0 (c (n "measure_time") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01jb8p7anv4r4y7dpf9jgvk37cky93p928lvf1a6k46xrawyy7ir")))

(define-public crate-measure_time-0.7.0 (c (n "measure_time") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11c0rd692xshxdnnrp81fnv228f4nvl02d600wrsnm6x0cbqm366")))

(define-public crate-measure_time-0.8.0 (c (n "measure_time") (v "0.8.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ydxap267y2fyjq1k2xlngfsfvpywfsg07gm48v2nmnqh1j9c1sz")))

(define-public crate-measure_time-0.8.1 (c (n "measure_time") (v "0.8.1") (d (list (d (n "instant") (r "^0.1") (f (quote ("stdweb"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f0yxgh3zb5s5qaa4bjw1zc972g3bj0ag2lrds8pks42pg77q7v9")))

(define-public crate-measure_time-0.8.2 (c (n "measure_time") (v "0.8.2") (d (list (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen" "inaccurate"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0lnqsi6ngf5nr5zjk7iyzzqv5wwamvxjbgynxj4kg4m0y400j8jn")))

(define-public crate-measure_time-0.8.3 (c (n "measure_time") (v "0.8.3") (d (list (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen" "inaccurate"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1k3may55x7868972ck61i0lrf5p1hkbf30gj4qb1ipdan0sx5vyv")))

