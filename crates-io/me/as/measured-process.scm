(define-module (crates-io me as measured-process) #:use-module (crates-io))

(define-public crate-measured-process-0.0.19 (c (n "measured-process") (v "0.0.19") (d (list (d (n "measured") (r "^0.0.19") (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("process" "feature"))) (d #t) (k 0)) (d (n "procfs") (r "^0.16") (k 0)))) (h "15wxv7pk4mlyrxkxy9jxj7r1mjqzrfx9m0k3lhygl69ppjadjcda") (f (quote (("default"))))))

(define-public crate-measured-process-0.0.20 (c (n "measured-process") (v "0.0.20") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "measured") (r "^0.0.20") (d #t) (k 0)) (d (n "procfs") (r "^0.16") (k 0)))) (h "1wkyras4h2zzrwvjjal4l544d4268l25jmaniylddi84vnn4j252") (f (quote (("default"))))))

(define-public crate-measured-process-0.0.21 (c (n "measured-process") (v "0.0.21") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "measured") (r "^0.0.21") (d #t) (k 0)) (d (n "procfs") (r "^0.16") (k 0)))) (h "000hfyq6pcszh8v40531qxw5m7bs5wd1sxddnav19a1pd6vcqr5k") (f (quote (("default"))))))

