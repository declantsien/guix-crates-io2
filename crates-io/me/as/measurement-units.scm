(define-module (crates-io me as measurement-units) #:use-module (crates-io))

(define-public crate-measurement-units-0.1.0 (c (n "measurement-units") (v "0.1.0") (h "1xwzc447kr2fibqvkc92740jk6zhyb30x2sclp7cac9x0bdx4h71") (y #t)))

(define-public crate-measurement-units-0.1.1 (c (n "measurement-units") (v "0.1.1") (h "0cd35af93sx9al11il7vd3xm6qlkk28d0l0m9kkvnd45wagz4dmh")))

