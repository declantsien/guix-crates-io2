(define-module (crates-io me as meas) #:use-module (crates-io))

(define-public crate-meas-0.1.0 (c (n "meas") (v "0.1.0") (d (list (d (n "duct") (r "^0.13.5") (d #t) (k 0)) (d (n "hhmmss") (r "^0.1.0") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0r83d5nf0az23wcni3idihw5dlsasgn3r4m88qjf0ganlk6bbf0i")))

