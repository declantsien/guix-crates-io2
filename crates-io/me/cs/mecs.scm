(define-module (crates-io me cs mecs) #:use-module (crates-io))

(define-public crate-mecs-0.1.0 (c (n "mecs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "149acbjnr2ixp57qkf0pwyywa0xlkm6kaw21svfm385mszs3axwl") (f (quote (("serde-serialize" "serde") ("default"))))))

(define-public crate-mecs-0.1.1 (c (n "mecs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1p19j7cwzdrsy2m6xd5inx447r00q924h08p42v500b7xs9zhpfg") (f (quote (("serde-serialize" "serde") ("default"))))))

