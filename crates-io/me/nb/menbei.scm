(define-module (crates-io me nb menbei) #:use-module (crates-io))

(define-public crate-menbei-0.1.0 (c (n "menbei") (v "0.1.0") (d (list (d (n "clap") (r "^2.23.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0xip60gbn809l4sfp48albkyy0s83npq98x4bwv3vdxgx82dch3g")))

