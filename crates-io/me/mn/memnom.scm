(define-module (crates-io me mn memnom) #:use-module (crates-io))

(define-public crate-memnom-0.1.0 (c (n "memnom") (v "0.1.0") (d (list (d (n "sysinfo") (r "^0.5") (d #t) (k 0)))) (h "0zlgbyq9qlp17g4kvg7jklv8ql8byd9z2yy2h1w7z31p6zhh87az")))

(define-public crate-memnom-0.2.0 (c (n "memnom") (v "0.2.0") (d (list (d (n "circular-queue") (r "^0.2") (d #t) (k 0)) (d (n "human-size") (r "^0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.5") (d #t) (k 0)))) (h "1vqqaa9hi6c0ladqx5vg00zxh7icqmz4sr9938ibk8fqvj8bpc22")))

