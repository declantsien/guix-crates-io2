(define-module (crates-io me tl metl) #:use-module (crates-io))

(define-public crate-metl-0.0.0 (c (n "metl") (v "0.0.0") (d (list (d (n "cocoa") (r "^0.2.4") (d #t) (k 0)) (d (n "core-foundation") (r "0.*") (d #t) (k 0)) (d (n "metal-sys") (r "^0.0.0") (d #t) (k 0)) (d (n "objc") (r "^0.1.8") (d #t) (k 0)))) (h "1fzi1gq1fja0nbfqv4kdndckiw1n4p551vsvwqhvf2ll0m4p70kb")))

