(define-module (crates-io me ck meck_grrs) #:use-module (crates-io))

(define-public crate-meck_grrs-0.1.0 (c (n "meck_grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1vl6g6iiacb37nk9mf9nijidq46a4sajm6xy2rv434d10z39lz2w")))

