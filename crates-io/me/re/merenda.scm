(define-module (crates-io me re merenda) #:use-module (crates-io))

(define-public crate-merenda-0.1.0 (c (n "merenda") (v "0.1.0") (d (list (d (n "arboard") (r "^3.2.1") (f (quote ("wayland-data-control"))) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1svbk40j59rcyvyvda188008pvyaw3nf4982sh3nl4vddhngar5a")))

