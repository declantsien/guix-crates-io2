(define-module (crates-io me re mere_memory_types) #:use-module (crates-io))

(define-public crate-mere_memory_types-0.9.1 (c (n "mere_memory_types") (v "0.9.1") (d (list (d (n "hdk") (r "^0.0.109") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1jffm4wfxcv886kf2jdbks2a5040g14dxxh8z0kjcm6j5a3j6asf")))

(define-public crate-mere_memory_types-0.9.2 (c (n "mere_memory_types") (v "0.9.2") (d (list (d (n "hdk") (r "^0.0.109") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0mnw9s7hmci4kfamvgaqbfaskiw6r95xsnrm3x4ms70kj7hnvla5")))

(define-public crate-mere_memory_types-0.16.0 (c (n "mere_memory_types") (v "0.16.0") (d (list (d (n "hdk") (r "^0.0.116") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0djbrx6g1ydaz10bxdsk016x0wb881515bm8yvyzz59zsi26bwq6")))

(define-public crate-mere_memory_types-0.20.0 (c (n "mere_memory_types") (v "0.20.0") (d (list (d (n "hdk") (r "^0.0.120") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0nsqgiwbda0vldvdjps4kl3d09fm6vairq4f3zbikjkg6xxh43r1")))

(define-public crate-mere_memory_types-0.23.0 (c (n "mere_memory_types") (v "0.23.0") (d (list (d (n "hdk") (r "^0.0.123") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0d2xc8asbyffyaglcf0z9wyz45kb44m0m2rjpk1jps132mba1qid")))

(define-public crate-mere_memory_types-0.27.0 (c (n "mere_memory_types") (v "0.27.0") (d (list (d (n "hdk") (r "^0.0.127") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "1f1rvmq1z9bv0rgx56qlhljmb97qydvlv7n81xyxccyk2bgzb1ys")))

(define-public crate-mere_memory_types-0.32.0 (c (n "mere_memory_types") (v "0.32.0") (d (list (d (n "hdk") (r "^0.0.132") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "1wsdcpm02cqk83vzrfc1ivd1ifjk3mzc9m9rbxknii3bzrb93yzh")))

(define-public crate-mere_memory_types-0.36.0 (c (n "mere_memory_types") (v "0.36.0") (d (list (d (n "hdk") (r "^0.0.136") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "1kxbk28nmmj0g8ghlr4jhf5zypg95pch1d94h0sfwrrqgxsygb1w")))

(define-public crate-mere_memory_types-0.42.0 (c (n "mere_memory_types") (v "0.42.0") (d (list (d (n "hdi") (r "^0.0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0bn2rk0rvrd92gcs4xslfsgkbjhp33n8hjblsycap2qvw4zq34ri")))

(define-public crate-mere_memory_types-0.42.1 (c (n "mere_memory_types") (v "0.42.1") (d (list (d (n "hdi") (r "^0.0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0mcxwhj31bna18mnm6b38b27qgg79bc6b41v23pdqzi2m00vlr28")))

(define-public crate-mere_memory_types-0.45.0 (c (n "mere_memory_types") (v "0.45.0") (d (list (d (n "hdi") (r "^0.0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "128v8928zqvi15nxcm4vdwg3k4rbfx62wwwm7g8haglafw49i3zg")))

(define-public crate-mere_memory_types-0.47.0 (c (n "mere_memory_types") (v "0.47.0") (d (list (d (n "hdi") (r "^0.0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "1yfswyjy2n6cgclnb9ic1nwigfsj86i3frskmid606smxdclkn3m")))

(define-public crate-mere_memory_types-0.51.0 (c (n "mere_memory_types") (v "0.51.0") (d (list (d (n "hdi") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0d5bn226acxdkrl5crqgvcrm6d5asn0h3lapvq73xfd239ga8f64")))

(define-public crate-mere_memory_types-0.60.0 (c (n "mere_memory_types") (v "0.60.0") (d (list (d (n "hdi") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0w1vbbkmknnkg8wgzh3qs5k7mqjqpxx0gl7ag3a7bgp1slk2fxbz")))

(define-public crate-mere_memory_types-0.75.0 (c (n "mere_memory_types") (v "0.75.0") (d (list (d (n "hdi") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "1di75n9aghhr8wyra0yr9naxvwb0cmqj416xy6xwh88laihfs1kv")))

(define-public crate-mere_memory_types-0.60.1 (c (n "mere_memory_types") (v "0.60.1") (d (list (d (n "hdi") (r "^0.1.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "1qirwgn4pgrr32ayljjs91hcxsz3jqx747y11b9gzrgdysaw7xn5")))

(define-public crate-mere_memory_types-0.76.0 (c (n "mere_memory_types") (v "0.76.0") (d (list (d (n "hdi") (r "^0.2.0-beta-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0g241sz2y0949s22s61wg332y2496m89sc300kwp4fxmj1brcbgj")))

(define-public crate-mere_memory_types-0.77.0 (c (n "mere_memory_types") (v "0.77.0") (d (list (d (n "hdi") (r "^0.2.0-beta-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "1myq54ywb722ywbwa3qa0k0ajs8rxbf0nva75dhfivkvrc74a61a")))

(define-public crate-mere_memory_types-0.78.0 (c (n "mere_memory_types") (v "0.78.0") (d (list (d (n "hdi") (r "^0.2.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "09gsk72pgw29ahhkradxqvcm8x6nmn5z3mpi7lnp7c3r53xgxwc5")))

(define-public crate-mere_memory_types-0.79.0 (c (n "mere_memory_types") (v "0.79.0") (d (list (d (n "hdi") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "130qanj08q5g4sg8i1wxf0hq794v0pjkvpdxyx37wa4xh2mcf2ir")))

(define-public crate-mere_memory_types-0.80.0 (c (n "mere_memory_types") (v "0.80.0") (d (list (d (n "hdi") (r "^0.3.0-beta-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "1v9x3w9cfpq6vml85b41w9r47vif347gryna9avv75hbi15pgyy7")))

(define-public crate-mere_memory_types-0.81.0 (c (n "mere_memory_types") (v "0.81.0") (d (list (d (n "hdi") (r "^0.3.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0dsvaiyhhcz2431wyaj16yy2ywj2ywkw5122x2vi21sr3r6chq0w")))

(define-public crate-mere_memory_types-0.82.0 (c (n "mere_memory_types") (v "0.82.0") (d (list (d (n "hdi") (r "^0.3.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0sm7zpl7i5l40h8hawjrdip5k8033h92d89zychy1ww9m9idzbi2")))

(define-public crate-mere_memory_types-0.83.0 (c (n "mere_memory_types") (v "0.83.0") (d (list (d (n "hdi") (r "^0.3.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "039mphrim6jpwzs0ry0ah49jh9a4h6qf6zgkgyarpap1l3w5b7w1")))

(define-public crate-mere_memory_types-0.83.1 (c (n "mere_memory_types") (v "0.83.1") (d (list (d (n "hdi") (r "^0.3.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0sfgjhqp67nacgsdrxxgw7kln0xjdd5w5cgprwb1qkxif6p79fz4")))

(define-public crate-mere_memory_types-0.84.0 (c (n "mere_memory_types") (v "0.84.0") (d (list (d (n "hdi") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "029vaj6vk3vbfh2g6rclh3lrnx7bss0w4glv9v91p9vfbf4p3s0h")))

(define-public crate-mere_memory_types-0.85.0 (c (n "mere_memory_types") (v "0.85.0") (d (list (d (n "hdi") (r "^0.4.0-beta-dev.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0dlr7h05p0inkk8j39ir69kr4b4nm7s5wmlf12fnqrcjgwnmm011")))

(define-public crate-mere_memory_types-0.86.0 (c (n "mere_memory_types") (v "0.86.0") (d (list (d (n "getrandom") (r "=0.2.7") (f (quote ("custom"))) (d #t) (k 0)) (d (n "hdi") (r "^0.4.0-beta-dev.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "15cpyavj31k1vf1a179c1h03k3k6kqi6z67xiyihi1kv8q44d43x")))

(define-public crate-mere_memory_types-0.87.0 (c (n "mere_memory_types") (v "0.87.0") (d (list (d (n "getrandom") (r "=0.2.7") (f (quote ("custom"))) (d #t) (k 0)) (d (n "hdi") (r "^0.3.1-beta-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0rn23pl2vcgibgvwxaxv0dcj3d6mnikvl5a3cl9x5j308h2xphm7")))

(define-public crate-mere_memory_types-0.88.0 (c (n "mere_memory_types") (v "0.88.0") (d (list (d (n "getrandom") (r "=0.2.7") (f (quote ("custom"))) (d #t) (k 0)) (d (n "hdi") (r "=0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0g8x6434i6abqxw5n0pj5xbphywznil3sj3a9i3rmp8kwdnd87ah")))

(define-public crate-mere_memory_types-0.88.1 (c (n "mere_memory_types") (v "0.88.1") (d (list (d (n "getrandom") (r "=0.2.7") (f (quote ("custom"))) (d #t) (k 0)) (d (n "hdi") (r "=0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "0xs372349hf6gw83rkz1w442zza8z6n9a0hcnpmsl1kv9aq03l1f")))

(define-public crate-mere_memory_types-0.89.0 (c (n "mere_memory_types") (v "0.89.0") (d (list (d (n "getrandom") (r "=0.2.7") (f (quote ("custom"))) (d #t) (k 0)) (d (n "hdi") (r "=0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "020jxv4ykk8z8085bi1k6jc61nvf6kxrz15bhpccsmqjijmkwwvs")))

(define-public crate-mere_memory_types-0.89.1 (c (n "mere_memory_types") (v "0.89.1") (d (list (d (n "getrandom") (r "=0.2.7") (f (quote ("custom"))) (d #t) (k 0)) (d (n "hdi") (r "=0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)))) (h "10g1lgsr03078gm852sh5aysv3fk8qq4x80m1kxhzr0j8697w6f6")))

(define-public crate-mere_memory_types-0.90.0 (c (n "mere_memory_types") (v "0.90.0") (d (list (d (n "getrandom") (r "=0.2.7") (f (quote ("custom"))) (d #t) (k 0)) (d (n "hdi") (r "=0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "10sqfwwhivls8iad1kxzj21q372ix96gwg19r0m5pxh4wlb9za77")))

(define-public crate-mere_memory_types-0.91.0 (c (n "mere_memory_types") (v "0.91.0") (d (list (d (n "getrandom") (r "=0.2.7") (f (quote ("custom"))) (d #t) (k 0)) (d (n "hdi") (r "^0.3.2") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.2") (f (quote ("encoding"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0hglw0l98n91h4fbrfsfkg5x9n45all9rij267wqzva1dsnz557y")))

(define-public crate-mere_memory_types-0.92.0 (c (n "mere_memory_types") (v "0.92.0") (d (list (d (n "getrandom") (r "=0.2.7") (f (quote ("custom"))) (d #t) (k 0)) (d (n "hdi") (r "^0.3.7-rc.0") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.6") (f (quote ("encoding"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "157wajxpbv8wfganb8kqxwplkr0c0l8fqvfjhs2i0igd0zmr9scd")))

(define-public crate-mere_memory_types-0.93.0 (c (n "mere_memory_types") (v "0.93.0") (d (list (d (n "hdi") (r "^0.4.0-beta-dev.30") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.24") (f (quote ("encoding"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1syqh46hdbrz32bcmqzi133cdhmrxfwfqk6xzd7a8scnvndx6jmx")))

(define-public crate-mere_memory_types-0.94.0 (c (n "mere_memory_types") (v "0.94.0") (d (list (d (n "hdi") (r "^0.4.0-beta-dev.36") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.28") (f (quote ("encoding"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1knl84m26n8qfv9xj6n32d51mc7cwjcj8i4ib7bqakkfdnyw4i5h")))

(define-public crate-mere_memory_types-0.95.0 (c (n "mere_memory_types") (v "0.95.0") (d (list (d (n "hdi") (r "^0.5.0-dev.1") (d #t) (k 0)) (d (n "holo_hash") (r "^0.4.0-dev.1") (f (quote ("encoding"))) (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0pyjqggvn2ra78r36l5affnzwd30g73n24iag75dvwmzfzi8v1fd")))

