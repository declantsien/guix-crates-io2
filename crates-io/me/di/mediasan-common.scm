(define-module (crates-io me di mediasan-common) #:use-module (crates-io))

(define-public crate-mediasan-common-0.3.2 (c (n "mediasan-common") (v "0.3.2") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (f (quote ("io"))) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1h59v0ap1i6k6z3n8f0388zqk8dh3a4h0jx8ph6qvbg1d2s46dda") (r "1.61.0")))

(define-public crate-mediasan-common-0.4.0 (c (n "mediasan-common") (v "0.4.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (f (quote ("io"))) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "15mjjkz59xgh3fkxqdzpba0n4jd7rxr0cnw2vs8rzwahy75dyxw1") (r "1.61.0")))

(define-public crate-mediasan-common-0.5.0 (c (n "mediasan-common") (v "0.5.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (f (quote ("io"))) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1smw63d3ncxi1j5x50ndk2ssdjzq3r355366kmha9pwa9dgh3c3y") (r "1.61.0")))

(define-public crate-mediasan-common-0.5.1 (c (n "mediasan-common") (v "0.5.1") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (f (quote ("io"))) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0cyw0jm9mahnp87ahzq91gklsxjlljmig31bam374crqv6qfd551") (r "1.61.0")))

