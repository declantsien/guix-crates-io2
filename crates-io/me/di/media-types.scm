(define-module (crates-io me di media-types) #:use-module (crates-io))

(define-public crate-media-types-0.1.0 (c (n "media-types") (v "0.1.0") (d (list (d (n "charsets") (r "^0.1.0") (d #t) (k 0)))) (h "0g9bnyqcpb9f0jvxlwcmmqysbkwl1l1q5a3xafvrcnv0b83l2ms1")))

(define-public crate-media-types-0.1.1 (c (n "media-types") (v "0.1.1") (d (list (d (n "charsets") (r "^0.1.1") (d #t) (k 0)))) (h "09cf6spvq5kb1n57m3rjzivhk4hay1pv5pp4wsk4qaqs2qm0rs3n")))

(define-public crate-media-types-0.1.2 (c (n "media-types") (v "0.1.2") (d (list (d (n "charsets") (r "^0.1.1") (d #t) (k 0)))) (h "17ldh8d753c0b5g6hz2p9rnly7cixlv1ywjhnj7dicjvzyml325l")))

(define-public crate-media-types-0.2.0 (c (n "media-types") (v "0.2.0") (d (list (d (n "charsets") (r "^0.1.1") (d #t) (k 0)) (d (n "clippy") (r "^0.0.33") (o #t) (d #t) (k 0)))) (h "19nf6gzd9zndaw1fsfd08h4h0f941d8y9ffbrw9xhph12jkpjb28") (f (quote (("dev" "clippy") ("default"))))))

