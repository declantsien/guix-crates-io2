(define-module (crates-io me di media-controller) #:use-module (crates-io))

(define-public crate-media-controller-0.1.0 (c (n "media-controller") (v "0.1.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "gtk") (r "^0.18.1") (d #t) (k 0)))) (h "1zfah3xhx1s4gv3ilaipdzw204qqy5y59d5k7jx35npjpwipbii8")))

(define-public crate-media-controller-0.2.0 (c (n "media-controller") (v "0.2.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "gtk") (r "^0.18.1") (d #t) (k 0)))) (h "0c8yczxshc3qiqrpsxxzm12sk86gvmmn05fx6d9v8gxiys5r1zkq")))

(define-public crate-media-controller-0.2.1 (c (n "media-controller") (v "0.2.1") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "gtk") (r "^0.18.1") (d #t) (k 0)))) (h "0jlmq4nchf3azxdqyknshynmlk1l2f9yf6vcq7mf66dqh66f5rqc")))

