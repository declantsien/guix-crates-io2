(define-module (crates-io me di medit) #:use-module (crates-io))

(define-public crate-medit-0.1.0 (c (n "medit") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q832gghrifm16hi3bcb5wb45z77yckd9g051bjc25k50q3g6wk5") (y #t)))

(define-public crate-medit-0.1.1 (c (n "medit") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q2cv4j9x5qp5rx90x9f473nkmyf98g0davpx1i9xqdfvr5382jq") (y #t)))

(define-public crate-medit-1.0.0 (c (n "medit") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sfrm4jhpwihi2dy21vbx6ifcq9z28zj8nlwvi8kcw7km4q0nyfj") (y #t)))

