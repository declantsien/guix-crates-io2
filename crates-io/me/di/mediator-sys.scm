(define-module (crates-io me di mediator-sys) #:use-module (crates-io))

(define-public crate-mediator-sys-0.1.0 (c (n "mediator-sys") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (o #t) (d #t) (k 0)))) (h "02x2vjxr4rmi9x2flk8gjbyps68j0n5g5aw1i65pxrm8q2gs4843") (f (quote (("default") ("async" "async-trait" "async-std")))) (y #t)))

(define-public crate-mediator-sys-1.0.0 (c (n "mediator-sys") (v "1.0.0") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (o #t) (d #t) (k 0)))) (h "1hjr2i1aggaw8h5l0m5hs17hla6ai7v1m4pajsrbhk8iba1ppkmv") (f (quote (("default") ("async" "async-trait" "async-std")))) (y #t)))

(define-public crate-mediator-sys-2.0.0 (c (n "mediator-sys") (v "2.0.0") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (o #t) (d #t) (k 0)))) (h "04p65rghcad25dyi0yi61whhl95nsahm0b3q818n9y9308v9jgc3") (f (quote (("default") ("async" "async-trait" "async-std")))) (y #t)))

(define-public crate-mediator-sys-2.0.1 (c (n "mediator-sys") (v "2.0.1") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (o #t) (d #t) (k 0)))) (h "1zbw52svl6qh7ya4ch5a9swl66qg1fxayh4l1iw8zd8zf05dmyhj") (f (quote (("default") ("async" "async-trait" "async-std")))) (y #t)))

(define-public crate-mediator-sys-2.0.2 (c (n "mediator-sys") (v "2.0.2") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (o #t) (d #t) (k 0)))) (h "1wx4sacdzfl9bi5i54qh6637q1vm4sjv9dplbp16ib3wnklipcbd") (f (quote (("default") ("async" "async-trait" "async-std"))))))

