(define-module (crates-io me di media_catalogue) #:use-module (crates-io))

(define-public crate-media_catalogue-0.1.0 (c (n "media_catalogue") (v "0.1.0") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1wxhdwilyqb5i9nvigjvi1kwq64fcxcilgb2lny54y25h5al06yc")))

(define-public crate-media_catalogue-0.1.1 (c (n "media_catalogue") (v "0.1.1") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "05yhcbmxbll44wsfd26g2vj65klqa6jyw7hh6dy638rdvc8ndgvz")))

(define-public crate-media_catalogue-0.1.2 (c (n "media_catalogue") (v "0.1.2") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1rf6jch29xdd3gz1bmcqpw7zfy94hkcps3w5z8iyrhb8909vw33j")))

(define-public crate-media_catalogue-0.1.3 (c (n "media_catalogue") (v "0.1.3") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1rmgppmhdxdgbfmwjz7dc65iv8ylqczr3p16d8lwpbjip1zryb15")))

(define-public crate-media_catalogue-0.1.4 (c (n "media_catalogue") (v "0.1.4") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1jjsk74ky8psq6ybx1k30ky8qpi3dn5y0a946pyjrm7rv4m85xx6")))

(define-public crate-media_catalogue-0.1.5 (c (n "media_catalogue") (v "0.1.5") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1504wc78g3iliha7qs5bzj3hc7k90i4bx0nrrk5gg3j5r4h1kwb5")))

