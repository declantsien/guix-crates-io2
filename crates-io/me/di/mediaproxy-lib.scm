(define-module (crates-io me di mediaproxy-lib) #:use-module (crates-io))

(define-public crate-mediaproxy-lib-0.1.0 (c (n "mediaproxy-lib") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iq7w3j3gl8anhnsa86xjhwzhflm8lh5b04i59khza4c4b5zsjpq") (y #t)))

