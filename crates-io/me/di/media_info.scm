(define-module (crates-io me di media_info) #:use-module (crates-io))

(define-public crate-media_info-0.1.0 (c (n "media_info") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "ffmpeg-next") (r "^6.0.0") (d #t) (k 0)) (d (n "id3") (r "^1.7.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)))) (h "0rwnmncnmlnyiyzgql8mi6j4nn8f03mif29zyaav9s81qkgg7df9")))

(define-public crate-media_info-0.1.1 (c (n "media_info") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "ffmpeg-next") (r "^6.0.0") (d #t) (k 0)) (d (n "id3") (r "^1.7.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)))) (h "0qpbrlck962fbba7yk6gm57mfnhlmy5hrk1dcfj8x1s0p1nc0bxj")))

(define-public crate-media_info-0.2.0 (c (n "media_info") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "ffmpeg-next") (r "^6.0.0") (d #t) (k 0)) (d (n "fs_metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "id3") (r "^1.7.0") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)))) (h "0mfwghc0l8hvrq7bmflvchi010nr8wj1zmyn03g3mbygx05nif2h")))

(define-public crate-media_info-0.3.0 (c (n "media_info") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "ffmpeg-next") (r "^6.0.0") (o #t) (d #t) (k 0)) (d (n "fs_metadata") (r "^0.3.0") (d #t) (k 0)) (d (n "id3") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (o #t) (d #t) (k 0)))) (h "06c9mi89l1rhh3vx5zhibly43gnxslvvls2v3vfx1903lcac9srq") (f (quote (("all" "ffmpeg" "exif" "id3")))) (s 2) (e (quote (("id3" "dep:id3") ("ffmpeg" "dep:ffmpeg-next") ("exif" "dep:kamadak-exif"))))))

(define-public crate-media_info-0.3.1 (c (n "media_info") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "ffmpeg-next") (r "^7.0.0") (o #t) (d #t) (k 0)) (d (n "fs_metadata") (r "^0.3.1") (d #t) (k 0)) (d (n "id3") (r "^1.13.1") (o #t) (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (o #t) (d #t) (k 0)))) (h "0kj3dvrbw7v8xdz60az6r6gqfg7573apcynz5l9lfg3i1q054mba") (f (quote (("all" "ffmpeg" "exif" "id3")))) (s 2) (e (quote (("id3" "dep:id3") ("ffmpeg" "dep:ffmpeg-next") ("exif" "dep:kamadak-exif"))))))

