(define-module (crates-io me di mediatrix) #:use-module (crates-io))

(define-public crate-mediatrix-1.0.0 (c (n "mediatrix") (v "1.0.0") (d (list (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.58") (o #t) (d #t) (k 0)))) (h "0ma5wl1mr1yvdpckwxdk9k1madzmvbylsp2sz48haxkfw2kkqklx") (f (quote (("default") ("async" "async-trait" "async-std"))))))

