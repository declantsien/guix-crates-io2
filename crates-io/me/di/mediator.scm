(define-module (crates-io me di mediator) #:use-module (crates-io))

(define-public crate-mediator-0.1.0 (c (n "mediator") (v "0.1.0") (h "0nzvpx4wxph6a7xnf0xz6sx33gwwiwll1lf25sqi943pvnh9j7s9") (f (quote (("impls") ("default" "impls"))))))

(define-public crate-mediator-0.1.1 (c (n "mediator") (v "0.1.1") (h "0kglg38ri7zbh3nh8m5cm18h8sm0pf0z700r1445b4pc2jhgxaal") (f (quote (("impls") ("default" "impls"))))))

(define-public crate-mediator-0.1.2 (c (n "mediator") (v "0.1.2") (h "0rpacmh0gd6ll4l1z2j8y0yrzbwqn8wpz3q2makrh9p31ckq65qy") (f (quote (("impls") ("default" "impls"))))))

(define-public crate-mediator-0.2.0 (c (n "mediator") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.52") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "064cn70x0av2gikjlwnhs8hss7f10blmac6ad0alls3i57safm8r") (f (quote (("streams" "tokio" "tokio-stream") ("impls") ("default" "impls") ("async" "tokio" "async-trait"))))))

(define-public crate-mediator-0.2.1 (c (n "mediator") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.52") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "0ijwrvqkwaba2b9rdvgjvmdi2zss6qm5xs1j8aanvbadiqijp3gy") (f (quote (("streams" "async" "tokio-stream") ("interceptors" "async") ("impls") ("full" "default" "async" "streams" "interceptors") ("default" "impls") ("async" "tokio" "tokio/sync" "async-trait"))))))

(define-public crate-mediator-0.2.2 (c (n "mediator") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.52") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "0yk7i94jhsi4lwnhjzsr1jb8rnqi9m6shm1945lrq5lpqi085gaz") (f (quote (("streams" "async" "tokio-stream") ("interceptors" "async") ("impls") ("full" "default" "async" "streams" "interceptors") ("default" "impls") ("async" "tokio" "tokio/sync" "async-trait"))))))

