(define-module (crates-io me di medic) #:use-module (crates-io))

(define-public crate-medic-0.2.0 (c (n "medic") (v "0.2.0") (d (list (d (n "anstream") (r "^0.6.13") (d #t) (k 0)) (d (n "anstyle") (r "^1.0.6") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 2)) (d (n "os_info") (r "^3.7.0") (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version_runtime") (r "^0.3.0") (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive" "std"))) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0kc5hfwmxq5q8iivqshynhd44f3lws2df7wxfrpd7smiqyfdqqs9") (r "1.75.0")))

(define-public crate-medic-0.3.0 (c (n "medic") (v "0.3.0") (d (list (d (n "anstream") (r "^0.6.14") (d #t) (k 0)) (d (n "anstyle") (r "^1.0.7") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "os_info") (r "^3.8.2") (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rustc_version_runtime") (r "^0.3.0") (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive" "std"))) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "1slv4w5a3r8bsxqy9q06i8z7gs7gi1d65qbr8213rmmcp6yjkdwj") (r "1.75.0")))

