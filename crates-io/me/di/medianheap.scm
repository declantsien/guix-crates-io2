(define-module (crates-io me di medianheap) #:use-module (crates-io))

(define-public crate-medianheap-0.1.0 (c (n "medianheap") (v "0.1.0") (d (list (d (n "ordered-float") (r "^1.0") (o #t) (d #t) (k 0)))) (h "12prrhxnxllhj1771zbjbzz0jisgj46szrg8axyfgbphjl0hjfr4") (f (quote (("default" "ordered-float"))))))

(define-public crate-medianheap-0.2.0 (c (n "medianheap") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "min-max-heap") (r "^1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (o #t) (d #t) (k 0)))) (h "079lad1rszcgs2z0b1h4qva0w9fybqm76b6zmzmr13xv562fn52l") (f (quote (("default" "ordered-float"))))))

(define-public crate-medianheap-0.3.0 (c (n "medianheap") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "min-max-heap") (r "^1.3") (d #t) (k 0)) (d (n "ordered-float") (r "^2.10") (o #t) (d #t) (k 0)))) (h "0jvjw3gfwsx5l0sab3bbr8hzqgl3nq7qmnmdcwr7i69c5ni5j74c") (f (quote (("default" "ordered-float"))))))

(define-public crate-medianheap-0.4.0 (c (n "medianheap") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "min-max-heap") (r "^1.3") (d #t) (k 0)))) (h "1xfilhc8vrh1j87rjnrrd0r0fmad4rhqpsbx86mk6g3ppcbr589w") (f (quote (("default"))))))

(define-public crate-medianheap-0.4.1 (c (n "medianheap") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "min-max-heap") (r "^1.3") (d #t) (k 0)))) (h "0bk4a38kpc3345smsahc6b4h6bvra6bsp8vwlyhn4z3hj017fn51") (f (quote (("default"))))))

