(define-module (crates-io me di median-accumulator) #:use-module (crates-io))

(define-public crate-median-accumulator-0.1.0 (c (n "median-accumulator") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "medianheap") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0k6pfqk02vbz3pd2cyayxaqpqp4r1ad7i1isqblqf7bn3igp6fpm")))

(define-public crate-median-accumulator-0.2.0 (c (n "median-accumulator") (v "0.2.0") (d (list (d (n "cc-traits") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "medianheap") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 2)))) (h "0fmp5rhgqh9q141gsa481q99khsf8xbghqdbba3p6ca5ridhaxwp") (f (quote (("nostd" "cc-traits/nostd")))) (s 2) (e (quote (("smallvec" "dep:smallvec" "cc-traits/smallvec"))))))

(define-public crate-median-accumulator-0.4.0 (c (n "median-accumulator") (v "0.4.0") (d (list (d (n "cc-traits") (r "^2.0.0") (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "medianheap") (r "^0.4.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 2)))) (h "0p8mpd1vb6kgq1dmna7155pgnlrzfwg2sbishv0g37xj68lrfdz3") (f (quote (("std" "cc-traits/alloc" "cc-traits/std") ("default" "std")))) (s 2) (e (quote (("smallvec" "dep:smallvec" "cc-traits/smallvec"))))))

