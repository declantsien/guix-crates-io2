(define-module (crates-io me di mediawalker) #:use-module (crates-io))

(define-public crate-mediawalker-1.0.0 (c (n "mediawalker") (v "1.0.0") (d (list (d (n "infer") (r "^0.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1phr070rr3jyjg0l3vvnr0wpmljn90hz1y6xp7i0irhj2jhn69qk")))

(define-public crate-mediawalker-1.1.0 (c (n "mediawalker") (v "1.1.0") (d (list (d (n "infer") (r "^0.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1i9w26qrrvrmr8w4s1g4cc9h5w316cv4xxgzrmlp6qnmh45lgy1y")))

(define-public crate-mediawalker-1.2.0 (c (n "mediawalker") (v "1.2.0") (d (list (d (n "infer") (r "^0.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1nwmyzfmjvf79k1ax616xzx8ail37la640y1qz9xyqiqmg91llzw")))

