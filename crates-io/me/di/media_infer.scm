(define-module (crates-io me di media_infer) #:use-module (crates-io))

(define-public crate-media_infer-1.0.0 (c (n "media_infer") (v "1.0.0") (h "1p8m7yp7kq6h4gjrgfpfd1vdrhc448hvf7mkz1n5bz7r4kzq31vb") (y #t)))

(define-public crate-media_infer-1.0.1 (c (n "media_infer") (v "1.0.1") (h "1mnc4h3rgh75aqxyxjjpg7kfanbqssdwdz4w55k4pbgnwh0jkc32")))

