(define-module (crates-io me di medifax) #:use-module (crates-io))

(define-public crate-medifax-0.1.0 (c (n "medifax") (v "0.1.0") (d (list (d (n "lazycell") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tauri") (r "^1.6.1") (d #t) (k 0)) (d (n "tauri-build") (r "^1.3.0") (d #t) (k 1)))) (h "07knsl6y2i58b8v5m9dxg592ip4817bmr7g60vwgdrv7avmy095q") (f (quote (("custom-protocol" "tauri/custom-protocol"))))))

