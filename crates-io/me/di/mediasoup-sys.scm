(define-module (crates-io me di mediasoup-sys) #:use-module (crates-io))

(define-public crate-mediasoup-sys-0.0.0 (c (n "mediasoup-sys") (v "0.0.0") (h "0vksd2aq1kfddx37d3sss01mv7281j8ycs3bmlrg2awpv6y8i9sx")))

(define-public crate-mediasoup-sys-0.1.0 (c (n "mediasoup-sys") (v "0.1.0") (h "0ka0nrgaziicszwnqa0m9sa2niw3fw5442b4aj9qx1ivdlp96aq4")))

(define-public crate-mediasoup-sys-0.1.1 (c (n "mediasoup-sys") (v "0.1.1") (h "1hc62hk11apcps7dc2s1vfl5plzlz8gijb88cgpsw12crsvsi9jv")))

(define-public crate-mediasoup-sys-0.1.2 (c (n "mediasoup-sys") (v "0.1.2") (h "0wxsgg3qxb1mcv45gsf7dmal97g2q14jpivww8pqy40hi922zlbz")))

(define-public crate-mediasoup-sys-0.1.3 (c (n "mediasoup-sys") (v "0.1.3") (h "1a2522pgj9py564ap6q93n2v2zwgqim4wvjjf9axc7wbhqndbhjr")))

(define-public crate-mediasoup-sys-0.1.4 (c (n "mediasoup-sys") (v "0.1.4") (h "03ylv7a13rzd4v8c8jjkvc165k1sggfr5s77q5lg1n8y03lm8iia")))

(define-public crate-mediasoup-sys-0.2.0 (c (n "mediasoup-sys") (v "0.2.0") (h "0qdcb79apf1pl0jn4njf199q9chgzjl8xzw3hin33vhlddkdh0zr")))

(define-public crate-mediasoup-sys-0.2.1 (c (n "mediasoup-sys") (v "0.2.1") (h "1b9940p7lpzhk25j5x95qbsfyg3w70z9mn8mdqa0a304qgamzkkd")))

(define-public crate-mediasoup-sys-0.2.2 (c (n "mediasoup-sys") (v "0.2.2") (h "178kr24y46pli1ird6ldapigwg1hinhyq77amcpafk5hyim0b2qg")))

(define-public crate-mediasoup-sys-0.2.3 (c (n "mediasoup-sys") (v "0.2.3") (h "0s83ipzsadd37jam3lx7168a9kx6512g90gndjz0fszkj8894cny")))

(define-public crate-mediasoup-sys-0.2.4 (c (n "mediasoup-sys") (v "0.2.4") (h "03md7map93nws8x5yfl9izhk2q299dxbjjn4zvaq8dz4gx4qm839")))

(define-public crate-mediasoup-sys-0.2.5 (c (n "mediasoup-sys") (v "0.2.5") (h "1hgvlvmbmhmm2l3hhhrk82z56ccm398gz850bm6vhxwdr9w8mc0j")))

(define-public crate-mediasoup-sys-0.3.0 (c (n "mediasoup-sys") (v "0.3.0") (h "0ixbr0wsr9pb6c9b2cz21ljxhr52cw43fgr8w3jq0g4rdjqm8lh3")))

(define-public crate-mediasoup-sys-0.3.1 (c (n "mediasoup-sys") (v "0.3.1") (h "1bdaz6swzdn4f4ps6lbpn1y04ia5c52fm9f98maq5lacgp93p1nh")))

(define-public crate-mediasoup-sys-0.3.2 (c (n "mediasoup-sys") (v "0.3.2") (h "00fknqvq5csrqikyrdg4id2zfd26kj55jg9v6sym1yyzpmrdlgn8")))

(define-public crate-mediasoup-sys-0.3.3 (c (n "mediasoup-sys") (v "0.3.3") (h "13grnzdilgcdsy0w4n97jcky5fsdb8avngczcqszyr956f0waxa4")))

(define-public crate-mediasoup-sys-0.4.0 (c (n "mediasoup-sys") (v "0.4.0") (h "0zqq5jmpq1z1n4f7hjhc7wm0dandf3sdf5lasv0a3vr5n167gh6w")))

(define-public crate-mediasoup-sys-0.4.1 (c (n "mediasoup-sys") (v "0.4.1") (h "1wsb41rvg27c4n61sn0hr0rpxsp6mhxgn5bp2jzhffdi4k1maadz")))

(define-public crate-mediasoup-sys-0.4.2 (c (n "mediasoup-sys") (v "0.4.2") (h "1pvhbn6qi4l5hmmc2qiyxqvm1yp3zxw8i5b4r3cz4x30p7k3ahqd")))

(define-public crate-mediasoup-sys-0.4.3 (c (n "mediasoup-sys") (v "0.4.3") (h "0pja2s8iwf350r9ggwpavznh7hl5l8xhzdxi5mbmhfnndb6vhlfp")))

(define-public crate-mediasoup-sys-0.5.0 (c (n "mediasoup-sys") (v "0.5.0") (h "12dpfxxvhcbxwyw67m7z5is1xwayvy4jb5vl6q0m6iqka5jb7m22")))

(define-public crate-mediasoup-sys-0.5.1 (c (n "mediasoup-sys") (v "0.5.1") (h "0gcpvribm52nrn244r1cp45l3adi9c1zzg1gfww0h7z9a74bi4nn")))

(define-public crate-mediasoup-sys-0.5.3 (c (n "mediasoup-sys") (v "0.5.3") (h "0mcvfmyhw7z2vz6ggvlsnsw0zh3yccr35p012s2rc8y3ah1m62fy")))

(define-public crate-mediasoup-sys-0.5.4 (c (n "mediasoup-sys") (v "0.5.4") (h "03svs0hln9ga5skbsxzfxpzx7mb2f48wrk1daz371b9wf8n1qpfw")))

(define-public crate-mediasoup-sys-0.6.0 (c (n "mediasoup-sys") (v "0.6.0") (h "1cq882dk19r9w0qwf7iqif60j8rs8ly12bqz6pvbfx20z8iqhvn1")))

(define-public crate-mediasoup-sys-0.7.0 (c (n "mediasoup-sys") (v "0.7.0") (d (list (d (n "planus") (r "^0.4.0") (d #t) (k 0)) (d (n "planus-codegen") (r "^0.4.0") (d #t) (k 1)) (d (n "planus-translation") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "09q1ib2v2x3fj71sw0j6jgab65fzhsw7xk6125dh7xphcb0l8bri")))

(define-public crate-mediasoup-sys-0.7.1 (c (n "mediasoup-sys") (v "0.7.1") (d (list (d (n "planus") (r "^0.4.0") (d #t) (k 0)) (d (n "planus-codegen") (r "^0.4.0") (d #t) (k 1)) (d (n "planus-translation") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k21f6knrqh0sxla33qpwk7jr162avznin7bvzzib2v2ypkvjxwd")))

(define-public crate-mediasoup-sys-0.7.2 (c (n "mediasoup-sys") (v "0.7.2") (d (list (d (n "planus") (r "^0.4.0") (d #t) (k 0)) (d (n "planus-codegen") (r "^0.4.0") (d #t) (k 1)) (d (n "planus-translation") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "07rkl9f3nd3w4g2wbcifvn7b36zwyarh4dn45kdgawzcn4kfivf0")))

(define-public crate-mediasoup-sys-0.8.0 (c (n "mediasoup-sys") (v "0.8.0") (d (list (d (n "planus") (r "^0.4.0") (d #t) (k 0)) (d (n "planus-codegen") (r "^0.4.0") (d #t) (k 1)) (d (n "planus-translation") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jd7hklrdnvgwgzhrvyl8f059ig9dbq3vhsqlp12ckinhwkia5gp")))

(define-public crate-mediasoup-sys-0.9.0 (c (n "mediasoup-sys") (v "0.9.0") (d (list (d (n "planus") (r "^0.4.0") (d #t) (k 0)) (d (n "planus-codegen") (r "^0.4.0") (d #t) (k 1)) (d (n "planus-translation") (r "^0.4.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vw1khmpn3di8ydd1q67mpwz0zr9hif41k33zkxvv9bxk8gnh9w2")))

