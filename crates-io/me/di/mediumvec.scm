(define-module (crates-io me di mediumvec) #:use-module (crates-io))

(define-public crate-mediumvec-1.0.0 (c (n "mediumvec") (v "1.0.0") (h "0dxpk7769zlzlh34kdyaxy7makyzfygr4l6wmjxvx20sp8fw4xb7")))

(define-public crate-mediumvec-1.0.1 (c (n "mediumvec") (v "1.0.1") (h "0q6afcviplrxq1l3rbzy2cbx2by2xfzlkslf3rj487frrm2vfcaq")))

(define-public crate-mediumvec-1.0.2 (c (n "mediumvec") (v "1.0.2") (h "10xvrbmky9kch0g6qqdvsca4v0pc8qg31bajah6cnjb8mcvfhazp")))

(define-public crate-mediumvec-1.0.3 (c (n "mediumvec") (v "1.0.3") (h "022p70pjdvhmwhxpnlbqflk62101i1a2x3jh42lk5wlg56is6nhk")))

(define-public crate-mediumvec-1.0.4 (c (n "mediumvec") (v "1.0.4") (h "1bgwzf42ai53rjbgid4mr6jn9ffzhmj4lw9d291knczba1k7k549")))

(define-public crate-mediumvec-1.1.0 (c (n "mediumvec") (v "1.1.0") (h "1m4x51s2y3kqd0v18v0w4a2bv7sdhjabcfknif8phlkipzr4cvzj")))

(define-public crate-mediumvec-1.2.0 (c (n "mediumvec") (v "1.2.0") (h "13h2wlkk1mywzgq0qkzjpkbsdn17m7943hi1shicqxbyvf1y1f58")))

(define-public crate-mediumvec-1.3.0 (c (n "mediumvec") (v "1.3.0") (h "0r4w2v76b5k6kwpv8jljjv1g9yj4rgm6m0701lacdbdgv2bfz95x")))

