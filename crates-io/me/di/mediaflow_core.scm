(define-module (crates-io me di mediaflow_core) #:use-module (crates-io))

(define-public crate-mediaflow_core-0.6.1 (c (n "mediaflow_core") (v "0.6.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15zz5iyka6izzpq29wyrvb50gyzj4zsb93mkw0rrpqqizbmwy6a4")))

(define-public crate-mediaflow_core-0.6.2 (c (n "mediaflow_core") (v "0.6.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r5csz6jf41fy17gmcnr87ikf9rlhpws3ra5zik102bc8aqjcmva")))

(define-public crate-mediaflow_core-0.7.0 (c (n "mediaflow_core") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cbl7mp8axh4qnjsb25nkk29ki6fvv4n0ik9w606ck48vbi0gccb")))

(define-public crate-mediaflow_core-0.8.0 (c (n "mediaflow_core") (v "0.8.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "189za09fjvrlxjlzanp3xsc06878libxpz37v4fjhyvdki9j1d5c")))

(define-public crate-mediaflow_core-0.9.0 (c (n "mediaflow_core") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q5ngsvhly1pqgm1yv3cs17zwzv88lhw410zgvck5g0gs135pj14")))

