(define-module (crates-io me di medium-to-markdown) #:use-module (crates-io))

(define-public crate-medium-to-markdown-0.1.0 (c (n "medium-to-markdown") (v "0.1.0") (d (list (d (n "html_parser") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x50zbs2rsbdrjhnz2yzgh6pkabc5qavdnavdk0nj6ygzh2vrd38")))

(define-public crate-medium-to-markdown-0.1.1 (c (n "medium-to-markdown") (v "0.1.1") (d (list (d (n "html_parser") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v88nlgfg729fdysg9g622c626x7ihyk9y5ji6dm5s3x8ksfj02f")))

