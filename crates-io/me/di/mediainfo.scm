(define-module (crates-io me di mediainfo) #:use-module (crates-io))

(define-public crate-mediainfo-0.1.3 (c (n "mediainfo") (v "0.1.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1llhxai82ryrbxkhhbdk34v7bcwl1ampnj62vkz7950hjd1yb387")))

(define-public crate-mediainfo-0.2.0 (c (n "mediainfo") (v "0.2.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "delegate") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1h01wvnck7552a8lfv4ag5q2hbqv6i6fjdzy16nm27ycxhnxv071") (l "mediainfo")))

