(define-module (crates-io me di median) #:use-module (crates-io))

(define-public crate-median-0.1.0 (c (n "median") (v "0.1.0") (d (list (d (n "arraydeque") (r "^0.4.2") (d #t) (k 2)) (d (n "noisy_float") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "1zalpwn6dxvc0cnn1lrc2r7gncaxbkilvw24z7yxgpv4lhishs13") (f (quote (("std") ("default" "std"))))))

(define-public crate-median-0.2.0 (c (n "median") (v "0.2.0") (d (list (d (n "arraydeque") (r "^0.4.2") (d #t) (k 2)) (d (n "arrayvec") (r "^0.4.7") (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0ic77dajj5i3vghxkv1af1b34hql9y5qjcglc7f4c5pc9nsmmzwg") (f (quote (("std") ("default" "std"))))))

(define-public crate-median-0.3.0 (c (n "median") (v "0.3.0") (d (list (d (n "arraydeque") (r "^0.4.2") (d #t) (k 2)) (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0c5ifqwms3x33j31hxac1j6g165ahf6nigqnv10fkw5kisn9ypsk") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-median-0.3.1 (c (n "median") (v "0.3.1") (d (list (d (n "arraydeque") (r "^0.4.2") (d #t) (k 2)) (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0xyjqb5p0b1lmiw10mlg2ipypvn31lc1ia017g11mk3q7a8z18m7") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

(define-public crate-median-0.3.2 (c (n "median") (v "0.3.2") (d (list (d (n "arraydeque") (r "^0.4.2") (d #t) (k 2)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "missing_mpl") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "noisy_float") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "0m4ps5cnzm5d2r8djdibm6y2d9bhvnysfyywaiypv4vqq7m82gwr") (f (quote (("std") ("nightly" "missing_mpl") ("default" "std"))))))

