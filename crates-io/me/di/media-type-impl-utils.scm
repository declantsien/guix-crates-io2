(define-module (crates-io me di media-type-impl-utils) #:use-module (crates-io))

(define-public crate-media-type-impl-utils-0.3.0-unstable (c (n "media-type-impl-utils") (v "0.3.0-unstable") (d (list (d (n "lut") (r "^0.1.0-unstable") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)) (d (n "quoted-string") (r "^0.6") (d #t) (k 0)))) (h "0cnjzkyvqrivz91dcswp2difj1xmvmkbmwcbgbp4s0rb4n76psdr") (f (quote (("default"))))))

(define-public crate-media-type-impl-utils-0.3.1-unstable (c (n "media-type-impl-utils") (v "0.3.1-unstable") (d (list (d (n "lut") (r "^0.1.0-unstable") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)) (d (n "quoted-string") (r "^0.6") (d #t) (k 0)))) (h "0bz6ri8wq3xh56hazfsm9zmwrga6n2x4317qbks04kkrk3nv9ghq") (f (quote (("default"))))))

