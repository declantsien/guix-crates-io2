(define-module (crates-io me di mediaflow_derive) #:use-module (crates-io))

(define-public crate-mediaflow_derive-0.1.0 (c (n "mediaflow_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n87s25z93a9zd6r5lpcba03s9p5cm8jl87a4v3khnd496yph1iy")))

(define-public crate-mediaflow_derive-0.1.1 (c (n "mediaflow_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pagmpwjpi7wka50h6fr151f2xchb99ny92zd11xapvylmxf9i0q")))

(define-public crate-mediaflow_derive-0.2.0 (c (n "mediaflow_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q2w9wd3n94c5ycbdvq3jrrjlflh3cd6wvrk8n4m5bkxq7xp2c50")))

(define-public crate-mediaflow_derive-0.3.0 (c (n "mediaflow_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ayb352lc97pffdyxs0518gfdxn2i2fnj6m56s78sk089wbx3395")))

(define-public crate-mediaflow_derive-0.4.0 (c (n "mediaflow_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ich27qkax1b0ln21sw99mic720z2yv5kpgjzq2inbr6kymrkvrk")))

(define-public crate-mediaflow_derive-0.4.1 (c (n "mediaflow_derive") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12dmvj15zjgfwbpqd1pmc0vj8c5a3m36dzfdbkk36kl4rrrsd877")))

(define-public crate-mediaflow_derive-0.5.0 (c (n "mediaflow_derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jz09fm5zdqp8ml602mbi1lhx4sn6lsmdvj56r92ci6if0mgzs2m")))

(define-public crate-mediaflow_derive-0.6.0 (c (n "mediaflow_derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16q75h88qcqnabjv6rk5kx1p85pymfxyr41d6aaiyz6ydl7w1g33")))

(define-public crate-mediaflow_derive-0.6.1 (c (n "mediaflow_derive") (v "0.6.1") (d (list (d (n "mediaflow_core") (r "^0.6.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jhspmkmd9i1ym0gq3s54ddrnik0ssbr05780153aifhvn031f05")))

(define-public crate-mediaflow_derive-0.6.2 (c (n "mediaflow_derive") (v "0.6.2") (d (list (d (n "mediaflow_core") (r "^0.6.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j07x8dkil52xk9bq4f6vnihhab317g5i01mmyg2qhwarbkhgjzx")))

(define-public crate-mediaflow_derive-0.7.0 (c (n "mediaflow_derive") (v "0.7.0") (d (list (d (n "mediaflow_core") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i9s299pc85fqzyaxnxdck6nxfda14z4kmf0y92mjwy5hxyk1h54")))

(define-public crate-mediaflow_derive-0.8.0 (c (n "mediaflow_derive") (v "0.8.0") (d (list (d (n "mediaflow_core") (r "^0.8.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hwbq31x6n5kszqq93nc7g2l21is5ixibln168rrki2sbh52imr2")))

(define-public crate-mediaflow_derive-0.9.0 (c (n "mediaflow_derive") (v "0.9.0") (d (list (d (n "mediaflow_core") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fmqbrfmb663i1laa3my1vhi6d9s1cmwsf90yw04dv1gskyk9klm")))

