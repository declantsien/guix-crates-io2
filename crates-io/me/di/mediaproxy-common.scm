(define-module (crates-io me di mediaproxy-common) #:use-module (crates-io))

(define-public crate-mediaproxy-common-0.1.0 (c (n "mediaproxy-common") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "166kgmvn6pn84py6nnarx6gp9fy7c6cmi7gd4kcnvp18qnv9y8kh")))

(define-public crate-mediaproxy-common-0.1.1 (c (n "mediaproxy-common") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17py1hm3llv1dn6nccrps2b6fryqbjc2w0mp3127ds83cccy0x5g")))

(define-public crate-mediaproxy-common-0.1.2 (c (n "mediaproxy-common") (v "0.1.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16i3zrw2gf3lfs96flb6fvv0fcc4zs1412m8ff8gryj8s6fy2krr")))

(define-public crate-mediaproxy-common-0.1.3 (c (n "mediaproxy-common") (v "0.1.3") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04l08j9884nxv8527pwgj860chb305x6ar7s3dq526rdy0m588p3")))

(define-public crate-mediaproxy-common-0.1.4 (c (n "mediaproxy-common") (v "0.1.4") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xa3mffv7cp444diyca1iq0zc77skhkd05wyj5s6x6v8y8lk3104")))

(define-public crate-mediaproxy-common-0.1.5 (c (n "mediaproxy-common") (v "0.1.5") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vwxmmhpfravdbly9qdqmx9si5i2538xv5s6h6r311zmc3ixm1y1")))

(define-public crate-mediaproxy-common-0.2.0 (c (n "mediaproxy-common") (v "0.2.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "custom_error") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xwpj3h66s3812vdxx3byf8hyfzjq2ir33pfhcz8dh1z0dsr0zi5")))

