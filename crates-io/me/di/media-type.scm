(define-module (crates-io me di media-type) #:use-module (crates-io))

(define-public crate-media-type-0.0.1 (c (n "media-type") (v "0.0.1") (h "05gs41n8rsymx5lkp7c80i1kj04jd5vm4rp7lcmp9nfr9gjf559m")))

(define-public crate-media-type-0.4.0-unstable (c (n "media-type") (v "0.4.0-unstable") (d (list (d (n "lut") (r "^0.1.0-unstable") (f (quote ("media-type-chars"))) (d #t) (k 0)) (d (n "media-type-impl-utils") (r "^0.3.0-unstable") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)) (d (n "quoted-string") (r "^0.6") (d #t) (k 0)))) (h "1y5k0f44p440h8i5r3i88w28q8ihhyiqys71dr431i6217fi77ph") (f (quote (("inner-bench") ("expose-param-utils"))))))

(define-public crate-media-type-0.4.1-unstable (c (n "media-type") (v "0.4.1-unstable") (d (list (d (n "lut") (r "^0.1.0-unstable") (f (quote ("media-type-chars"))) (d #t) (k 0)) (d (n "media-type-impl-utils") (r "^0.3.0-unstable") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)) (d (n "quoted-string") (r "^0.6") (d #t) (k 0)))) (h "1ymiwkw0vq9zcs7kcgx5jn07d0dhd6gm9nx42490r9mj4r07idb2") (f (quote (("inner-bench") ("expose-param-utils"))))))

