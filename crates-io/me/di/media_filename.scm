(define-module (crates-io me di media_filename) #:use-module (crates-io))

(define-public crate-media_filename-0.1.0 (c (n "media_filename") (v "0.1.0") (d (list (d (n "itertools") (r "0.4.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "regex") (r "0.1.*") (d #t) (k 0)))) (h "13v6p1xrrag5p69047arfcq7szanqbbjkw511lnshgaxiqnih5xc")))

(define-public crate-media_filename-0.1.1 (c (n "media_filename") (v "0.1.1") (d (list (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "1dwa8cxg20vx15a36fg80r11j3zwxljnk8lb146qsy0l8vvsa90f")))

(define-public crate-media_filename-0.1.2 (c (n "media_filename") (v "0.1.2") (d (list (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "0cy0lbaq274llhrvnxdd62404vz1fsw4s77xhrr8p4n6xpmmqiwl")))

(define-public crate-media_filename-0.1.3 (c (n "media_filename") (v "0.1.3") (d (list (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "10xymcgnx3svpmfy5165g1kip53vi92b0z24av8asgzq70ibc0hy")))

(define-public crate-media_filename-0.1.4 (c (n "media_filename") (v "0.1.4") (d (list (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "17rlaak7w602xfk5n6n6ygwpf1igbgzij20fay3kdwdbpwcwrsm8")))

