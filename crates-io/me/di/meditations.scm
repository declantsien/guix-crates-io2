(define-module (crates-io me di meditations) #:use-module (crates-io))

(define-public crate-meditations-0.1.0 (c (n "meditations") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19khbmvjw509p7fhv5sxzlwnvhih4n65ds45sszg9ib7iml2hyr6")))

(define-public crate-meditations-0.1.1 (c (n "meditations") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "035d2fk2gkch31smi4x20wi607n0ycfpqms82zgnxavkshwrj1z6")))

