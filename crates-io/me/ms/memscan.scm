(define-module (crates-io me ms memscan) #:use-module (crates-io))

(define-public crate-memscan-0.1.0 (c (n "memscan") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "subslice_index") (r "^0.5") (d #t) (k 0)))) (h "0ish7vjn1llb2bsb9fgr0nc7ykyvl0xfw08q1xjxdmn09pxi7b3k")))

