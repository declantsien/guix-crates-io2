(define-module (crates-io me ms memsize) #:use-module (crates-io))

(define-public crate-memsize-0.1.0 (c (n "memsize") (v "0.1.0") (h "11305aiggppvicn63nbhy1san28w9qkdjpxd4pmbkkknfn2fghw8")))

(define-public crate-memsize-0.1.1 (c (n "memsize") (v "0.1.1") (h "1480w68zs9y70mzas4lhy14yvlf6mj6jxl982f7bp1zhpsw87pff")))

(define-public crate-memsize-0.1.2 (c (n "memsize") (v "0.1.2") (h "17rbjng6ywfvppd4lgdbv3kwm9zfvjl0an5m2lafrvsrxwa0kvrm")))

(define-public crate-memsize-0.1.3 (c (n "memsize") (v "0.1.3") (h "1qk4qnbhzxfdvvdpjv27lzbn2sn853d8qwv4vl1dqw9w7nxp4lxr")))

(define-public crate-memsize-0.1.4 (c (n "memsize") (v "0.1.4") (h "0qh7gg72jvhbprph4sxswp497xhh5jf0pgxl0zgyzb8m8qldvy8d")))

