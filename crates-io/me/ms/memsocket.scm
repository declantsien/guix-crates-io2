(define-module (crates-io me ms memsocket) #:use-module (crates-io))

(define-public crate-memsocket-0.1.0 (c (n "memsocket") (v "0.1.0") (d (list (d (n "tokio") (r "^0.1.7") (d #t) (k 0)))) (h "138w2h51mf7bpyhayv97ppbc5qqxv83hyzf2sqqf4zw7ds87941x")))

(define-public crate-memsocket-0.1.1 (c (n "memsocket") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)))) (h "1n4vmnwmhcb3mlhrxy3imjnig19a36mn6hmwd8bwzpjldy5xqrnj")))

(define-public crate-memsocket-0.1.2 (c (n "memsocket") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)))) (h "0g3nphf8f9b8rjjq0rjxv6kjj7r300wnp4xg1acc0b7v0c5603ac")))

(define-public crate-memsocket-0.1.3 (c (n "memsocket") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)))) (h "1k64nsvpnxscjpgq9b2vcvmgsl2yh8ksk5rnv494b4kl56yj84wz")))

