(define-module (crates-io me mp mempool) #:use-module (crates-io))

(define-public crate-mempool-0.1.0 (c (n "mempool") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "syncbox") (r "^0.2") (d #t) (k 2)))) (h "0ji11yhzbcp1p4xf805sj833xd5mmw381a4jw3xna30rq0kf65n7") (f (quote (("nightly"))))))

(define-public crate-mempool-0.1.1 (c (n "mempool") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "syncbox") (r "^0.2") (d #t) (k 2)))) (h "0m6nibwjd8f3s06k8q63hapdd9s9v5s1i3x5qk6cw22h2ffpkgpk") (f (quote (("nightly"))))))

(define-public crate-mempool-0.1.2 (c (n "mempool") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "syncbox") (r "^0.2") (d #t) (k 2)))) (h "1i7828y8rxyxkg698wpndd4zw8lhn4c161sqdrw39d6nh0ds6c9z") (f (quote (("nightly"))))))

(define-public crate-mempool-0.2.0 (c (n "mempool") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "syncbox") (r "^0.2") (d #t) (k 2)))) (h "0s9j3mcikz0wnb7lyvl0hfw0wyf935jdlgr11szfys70d69f2pvd") (f (quote (("nightly"))))))

(define-public crate-mempool-0.3.0 (c (n "mempool") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "syncbox") (r "^0.2") (d #t) (k 2)))) (h "0snkdr95n55hyw9ksdl51j2i3ifmkvg7g957ay2nyypbwdgyd5zr") (f (quote (("nightly"))))))

(define-public crate-mempool-0.3.1 (c (n "mempool") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 2)) (d (n "syncbox") (r "^0.2") (d #t) (k 2)))) (h "152mx5i624qxz5z7krjxfciky8b63w8682my3s6iv2p80g0ajqs1") (f (quote (("nightly"))))))

