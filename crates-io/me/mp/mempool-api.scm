(define-module (crates-io me mp mempool-api) #:use-module (crates-io))

(define-public crate-mempool-api-0.1.0 (c (n "mempool-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09yyjng9fg0zcvlp02phcaghjrrc0gz2rjrmgncrxi9f4ydrhlx9")))

(define-public crate-mempool-api-0.2.0 (c (n "mempool-api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pm3ydx6zd6lybyhz6zvsx2rsjxwh1j3rlxy4s81lqcmcvlgiwsq")))

(define-public crate-mempool-api-0.2.1 (c (n "mempool-api") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gm2grsdscdkvdwcyqrl9h0xy3axpvwaxgmpgs1dagqmsksinhrs")))

(define-public crate-mempool-api-0.2.2 (c (n "mempool-api") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xk4qflglxxrik1dkbzy6y8ky0xy85p0gnay67vrhdrw786xk94h")))

(define-public crate-mempool-api-0.2.3 (c (n "mempool-api") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gjsbxlp096xcn8nnzbzzlklwmza03m2m19vi7r3jn7b38p3c2qm")))

