(define-module (crates-io me mp memprocfs) #:use-module (crates-io))

(define-public crate-memprocfs-5.4.0 (c (n "memprocfs") (v "5.4.0") (d (list (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19692njwysfpc1ysi2k40s5dmwpxqvwpsbp0h9akvw7fh3p66riz")))

(define-public crate-memprocfs-5.4.1 (c (n "memprocfs") (v "5.4.1") (d (list (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1djh89rnn0hyx7vksfqy0nvzdyaa88mgmghx8naxgpnfm98i88p9")))

(define-public crate-memprocfs-5.4.2 (c (n "memprocfs") (v "5.4.2") (d (list (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0121jqplz5l5xih5lxash60iqxfs8g0lrwkhs4q92acanckxsv75")))

(define-public crate-memprocfs-5.4.3 (c (n "memprocfs") (v "5.4.3") (d (list (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pmjxh8qfaq71jyrdx6p14pf8hkqvgnvbkx1m44gkv840xgxb5jl")))

(define-public crate-memprocfs-5.4.4 (c (n "memprocfs") (v "5.4.4") (d (list (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z8piv4ndqkb2bi42dc69q7rhc97qif6i8mwdan05a1jjnld1v3n")))

(define-public crate-memprocfs-5.5.0 (c (n "memprocfs") (v "5.5.0") (d (list (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pimnpm9makgik2f49i7a0yxras1zin9jqhg2z5m3q74snxiimwa")))

(define-public crate-memprocfs-5.6.0 (c (n "memprocfs") (v "5.6.0") (d (list (d (n "libloading") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iavycwb4rac06bknlgwnnws6pgbjljdmx4cw3jiy9r0jcisjqg3")))

(define-public crate-memprocfs-5.6.1 (c (n "memprocfs") (v "5.6.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v278ggkclx86cxk6zjxsn6s6hqqqidikjvzlzz385cb7v9891df")))

(define-public crate-memprocfs-5.7.0 (c (n "memprocfs") (v "5.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05z1v8m0pfq7kimh1y23nx96x4wkfhl3f29qiqclikcraiwq445y")))

(define-public crate-memprocfs-5.8.0 (c (n "memprocfs") (v "5.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1iydxfi7mzya8kys5ikjbd6702gsmq0hhjskbrkvi3snpkn0b3jh")))

(define-public crate-memprocfs-5.8.1 (c (n "memprocfs") (v "5.8.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0553ha3y9w6b3ivksifk56d96rxx6fsqf6hkh0g1p2m2k6dhgikj")))

(define-public crate-memprocfs-5.8.14 (c (n "memprocfs") (v "5.8.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1379g2scxb1fcips6jra0vikihiad3kb94ylqhllpcqkhkbdlkdh")))

(define-public crate-memprocfs-5.9.0 (c (n "memprocfs") (v "5.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1aqgi3b01n5zv4n9wcq41mb53gj4f5pnjcl3jjvjm2vihg722lgi")))

(define-public crate-memprocfs-5.9.4 (c (n "memprocfs") (v "5.9.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02j64f1035m0n03357ldrxs770y7zq4bacc2kwbv2x76znrgghby")))

(define-public crate-memprocfs-5.9.5 (c (n "memprocfs") (v "5.9.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nik00lhfizs89pmkh9drbak352rqzi53m6v7yzxmgfwxkn40f69")))

(define-public crate-memprocfs-5.9.6 (c (n "memprocfs") (v "5.9.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mih20v9y01ykxnns8mapbzcyzgcq8vmbd458md3kidzcn0hx7p5")))

(define-public crate-memprocfs-5.9.13 (c (n "memprocfs") (v "5.9.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zanph2lh7qvyn70h4cn33lgi3yldn056q670ghpkzxwj8qq0rga")))

