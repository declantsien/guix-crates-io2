(define-module (crates-io me mp mempeek) #:use-module (crates-io))

(define-public crate-mempeek-0.1.0 (c (n "mempeek") (v "0.1.0") (d (list (d (n "libprocmem") (r "^0.1") (d #t) (k 0)) (d (n "quoted_strings") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)))) (h "040i3i5jfafsk8b5ycbq07iavy02nxr2w73hchh6rx0sh4gd91ax")))

(define-public crate-mempeek-0.1.1 (c (n "mempeek") (v "0.1.1") (d (list (d (n "libprocmem") (r "^0.1") (d #t) (k 0)) (d (n "quoted_strings") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)))) (h "1lbcl5gyqfrflsl1hbyhv3zp3wwq6w77paq1fzz2n6rsqg0p6v2j")))

(define-public crate-mempeek-0.1.2 (c (n "mempeek") (v "0.1.2") (d (list (d (n "libprocmem") (r "^0.1") (d #t) (k 0)) (d (n "quoted_strings") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)))) (h "07kln32hpa74abnrqkxnviqkvadjm9dgzaf1wnb65v4rfcxjhvrm")))

(define-public crate-mempeek-0.1.3 (c (n "mempeek") (v "0.1.3") (d (list (d (n "libprocmem") (r "^0.1") (d #t) (k 0)) (d (n "quoted_strings") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)))) (h "0i7i6yd6jxlqjv9cjy63h6429j2n3n9qyb3l3yhbv9c3lxz9xk4v")))

(define-public crate-mempeek-0.1.4 (c (n "mempeek") (v "0.1.4") (d (list (d (n "libprocmem") (r "^0.1") (d #t) (k 0)) (d (n "quoted_strings") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)))) (h "1w46pxasq9plqhlg99jpbc5n2xrbr21ik2n4xbzlylwlv7zk7x1z")))

(define-public crate-mempeek-0.1.5 (c (n "mempeek") (v "0.1.5") (d (list (d (n "libprocmem") (r "^0.1") (d #t) (k 0)) (d (n "quoted_strings") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)))) (h "14z8g8dpdac6n3z6ijv38cps1k49hblvk46xbsw9cmi7ignv6z79")))

