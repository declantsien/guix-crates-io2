(define-module (crates-io me mp memprobe) #:use-module (crates-io))

(define-public crate-memprobe-0.1.0 (c (n "memprobe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.4") (d #t) (k 0)))) (h "1m37dqqmvbbfrq11vnn5852f66nmcarr4nsvak8d0y680ylpzqgq")))

