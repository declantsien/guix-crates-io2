(define-module (crates-io me sl meslin-derive) #:use-module (crates-io))

(define-public crate-meslin-derive-0.0.1 (c (n "meslin-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0v6jiwssv36dydxbv4c1aqpil3r5vsm77fmq5ycmh4syx3fq3bhy")))

(define-public crate-meslin-derive-0.0.2 (c (n "meslin-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "05g59gpazv55w43db34nhdvax39nncbsv91n81pq294qqq3i4lpp")))

(define-public crate-meslin-derive-0.0.3 (c (n "meslin-derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1501bzcvllxwn3470rcql93pf5b848wh6qxjsr2yhvmqbzw10gkj")))

