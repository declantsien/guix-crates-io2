(define-module (crates-io me ma memas-sdk) #:use-module (crates-io))

(define-public crate-memas-sdk-0.1.0 (c (n "memas-sdk") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0l2gcl1c5clxkbzgr5bp22i8ksnrr9fy19p7mcwvgx11c2x86lpc")))

