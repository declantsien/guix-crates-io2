(define-module (crates-io me ma memadvise) #:use-module (crates-io))

(define-public crate-memadvise-0.1.1 (c (n "memadvise") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "libc") (r "^0.2.30") (d #t) (t "cfg(unix)") (k 0)) (d (n "page_size") (r "^0.4.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "03hf89yqxaclyw7fqxay1l6m53m9lschbbw5k4rylqhcnkc02xnf") (f (quote (("no_std" "page_size/no_std" "spin"))))))

(define-public crate-memadvise-0.1.2 (c (n "memadvise") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "libc") (r "^0.2.30") (d #t) (t "cfg(unix)") (k 0)) (d (n "page_size") (r "^0.4.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1x0cd2wcj05jdmj4j3yn8z9azhpaq050h5sxnq3v6w3j0rpzixma") (f (quote (("no_std" "page_size/no_std" "spin"))))))

