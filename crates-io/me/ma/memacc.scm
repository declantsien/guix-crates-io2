(define-module (crates-io me ma memacc) #:use-module (crates-io))

(define-public crate-memacc-0.1.0 (c (n "memacc") (v "0.1.0") (h "06mjpkjjla4mbdjh4mc89z0fmgc5vp2z21n0im3hyzxg3qdq7l08")))

(define-public crate-memacc-0.1.1 (c (n "memacc") (v "0.1.1") (h "17lygx2qjyb0ankgdm47645v71h2175xll3sckkyyw7mh06n00j4")))

(define-public crate-memacc-0.1.2 (c (n "memacc") (v "0.1.2") (h "0aq9ivgnwyfrqbv2zqlqkyf4n7rvaqg7nzxrqpha8mh95v883vky") (f (quote (("no-std"))))))

(define-public crate-memacc-0.1.3 (c (n "memacc") (v "0.1.3") (h "13xf5d4sr6yh94zhkrvvzc99sy2hs0gahp7rqwklpylc6iik4mk6") (f (quote (("no-std"))))))

(define-public crate-memacc-0.1.4 (c (n "memacc") (v "0.1.4") (h "0a4nyrprinhqxaicng2svflys7mps0k24yc3lk5mf34k0rc4yd7a") (f (quote (("no-std"))))))

(define-public crate-memacc-0.1.5 (c (n "memacc") (v "0.1.5") (h "08dy1rwjyf9y56cki5q122ys9k0vz0v308yjxhz8kirj2vfga999") (f (quote (("no-std"))))))

(define-public crate-memacc-0.1.6 (c (n "memacc") (v "0.1.6") (h "1ic4k9bwb11g39k24haq76y04ca7qc40z3q4q548xxnln7i4ib36") (f (quote (("no-std"))))))

(define-public crate-memacc-0.1.8 (c (n "memacc") (v "0.1.8") (h "0fmwdwhqs3v76yanhb79mnngnj1453whw66cl34k3qq1b8agl0b1") (f (quote (("no-std"))))))

(define-public crate-memacc-0.1.9 (c (n "memacc") (v "0.1.9") (h "1v407dxpq3p5zmlv6jd8cxn0rc5nhxw6j0fxxj98nkhfflm4bxsw") (f (quote (("no-std"))))))

(define-public crate-memacc-0.1.10 (c (n "memacc") (v "0.1.10") (h "1sh0lk5npd9ylhlsv2w2vhxmamng31s7a1iiynsaz7lhqsfbyiin") (f (quote (("no-std"))))))

(define-public crate-memacc-0.1.11 (c (n "memacc") (v "0.1.11") (h "11zjbp3835g8s22c7scjrvjncak1hi9z6yjlqmm3kxd8gdlgmn92") (f (quote (("no-std"))))))

