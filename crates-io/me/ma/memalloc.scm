(define-module (crates-io me ma memalloc) #:use-module (crates-io))

(define-public crate-memalloc-0.0.1 (c (n "memalloc") (v "0.0.1") (h "0icr0m6maywb0w0v2pc5y8a3n25gn7icmvnkg7g99x4rw79hixqx")))

(define-public crate-memalloc-0.1.0 (c (n "memalloc") (v "0.1.0") (h "1hgc0didnclhzkq5c7nb0m0ha3155wn9j5h2q68hh2y4ylrd4ffz")))

