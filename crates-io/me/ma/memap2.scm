(define-module (crates-io me ma memap2) #:use-module (crates-io))

(define-public crate-memap2-0.1.0 (c (n "memap2") (v "0.1.0") (h "04q99c72j6nmd8ca9j46niii47i0gx2391qlmwwvp7cizrx7476j")))

(define-public crate-memap2-0.1.2 (c (n "memap2") (v "0.1.2") (h "0xl5k09b94l02j6pn2xlcnb1qj4p9aldfma7n9cr086x9r8jl1bc")))

