(define-module (crates-io me ma memac) #:use-module (crates-io))

(define-public crate-memac-0.3.0 (c (n "memac") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "synctools") (r "^0.3.0") (d #t) (k 0)))) (h "14bngsnl04w0cnhq5l4cmvf5cwmi00hs6avf1xda19pzhs2blljz") (f (quote (("default" "buddy_32m") ("buddy_8t") ("buddy_8g") ("buddy_64m") ("buddy_64g") ("buddy_512m") ("buddy_512g") ("buddy_4t") ("buddy_4g") ("buddy_32m") ("buddy_32g") ("buddy_2t") ("buddy_2g") ("buddy_256m") ("buddy_256g") ("buddy_1t") ("buddy_1g") ("buddy_16g") ("buddy_128m") ("buddy_128g"))))))

(define-public crate-memac-0.3.1 (c (n "memac") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "synctools") (r "^0.3") (d #t) (k 0)))) (h "1qcfg5zwvqvfg2v0d7di4slds569c552im0mdgjh3kaaqv44cipa") (f (quote (("default" "buddy_32m") ("buddy_8t") ("buddy_8g") ("buddy_64m") ("buddy_64g") ("buddy_512m") ("buddy_512g") ("buddy_4t") ("buddy_4g") ("buddy_32m") ("buddy_32g") ("buddy_2t") ("buddy_2g") ("buddy_256m") ("buddy_256g") ("buddy_1t") ("buddy_1g") ("buddy_16g") ("buddy_128m") ("buddy_128g"))))))

(define-public crate-memac-0.3.2 (c (n "memac") (v "0.3.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "synctools") (r "^0.3") (d #t) (k 0)))) (h "1dyf6sbdws55v5sgf668b9ri8jqrc72a3591dawvx2zjh1l2c6kl") (f (quote (("default" "buddy_32m") ("buddy_8t") ("buddy_8g") ("buddy_64m") ("buddy_64g") ("buddy_512m") ("buddy_512g") ("buddy_4t") ("buddy_4g") ("buddy_32m") ("buddy_32g") ("buddy_2t") ("buddy_2g") ("buddy_256m") ("buddy_256g") ("buddy_1t") ("buddy_1g") ("buddy_16g") ("buddy_128m") ("buddy_128g"))))))

(define-public crate-memac-0.3.3 (c (n "memac") (v "0.3.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "synctools") (r "^0.3") (d #t) (k 0)))) (h "0x101g9714fnfcvvsqpdlww25qhmjh6q1jc720zrs8n3imvz3a0n") (f (quote (("default" "buddy_32m") ("buddy_8t") ("buddy_8g") ("buddy_64m") ("buddy_64g") ("buddy_512m") ("buddy_512g") ("buddy_4t") ("buddy_4g") ("buddy_32m") ("buddy_32g") ("buddy_2t") ("buddy_2g") ("buddy_256m") ("buddy_256g") ("buddy_1t") ("buddy_1g") ("buddy_16g") ("buddy_128m") ("buddy_128g"))))))

(define-public crate-memac-0.4.0 (c (n "memac") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "synctools") (r "^0.3") (d #t) (k 0)))) (h "118i4gg67yp1slvm85havpih8p4324bb05ay6x032ha7hhyr5ljf") (f (quote (("default" "buddy_32m") ("buddy_8t") ("buddy_8g") ("buddy_64m") ("buddy_64g") ("buddy_512m") ("buddy_512g") ("buddy_4t") ("buddy_4g") ("buddy_32m") ("buddy_32g") ("buddy_2t") ("buddy_2g") ("buddy_256m") ("buddy_256g") ("buddy_1t") ("buddy_1g") ("buddy_16g") ("buddy_128m") ("buddy_128g"))))))

(define-public crate-memac-0.5.0 (c (n "memac") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "synctools") (r "^0.3") (d #t) (k 0)))) (h "0v165w2xzqrfda8l0g1n0fhhc4n6ld1s2awmfq80626j5kbql2h4")))

(define-public crate-memac-0.5.1 (c (n "memac") (v "0.5.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "synctools") (r "^0.3") (d #t) (k 0)))) (h "1fv2c0gjfynsy9apj8dwmyvi33pr8srw17n6nqb4awmfdxin46za")))

(define-public crate-memac-0.5.2 (c (n "memac") (v "0.5.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "synctools") (r "^0.4") (d #t) (k 0)))) (h "1db9z32dqffzwlzc0bzsiyk2hxz98wgrs5i00d615jrqkz4hlsx7")))

(define-public crate-memac-0.5.3 (c (n "memac") (v "0.5.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "synctools") (r "^0.3") (d #t) (k 0)))) (h "150hwmp7giahd99zsbjml47952wx8bcxjk4p2rbwk0rp5v4pw15n")))

