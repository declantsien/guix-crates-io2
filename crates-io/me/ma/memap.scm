(define-module (crates-io me ma memap) #:use-module (crates-io))

(define-public crate-memap-0.1.0 (c (n "memap") (v "0.1.0") (h "1g6l9f1mlfv3h603jrp5yx53hwwm7b57syx1i6gb3dnki7rmwkmj")))

(define-public crate-memap-0.1.1 (c (n "memap") (v "0.1.1") (h "0nhvdjjgyxqmdrnhymkdrzbcfkcg5dx3xhkvxhazcavigkh1x6zl")))

(define-public crate-memap-0.1.2 (c (n "memap") (v "0.1.2") (h "00s0hx667dzajgc98a8b9k6g2njc5g1pk10qdjc63is82cb3dbjl")))

