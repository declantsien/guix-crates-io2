(define-module (crates-io me ss messagepack-rs-macros) #:use-module (crates-io))

(define-public crate-messagepack-rs-macros-0.1.0 (c (n "messagepack-rs-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1barmhjdlayz847rrq1vqcsvfgk5pifnb71km22ngn90ibsdi7dq")))

(define-public crate-messagepack-rs-macros-0.1.1 (c (n "messagepack-rs-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0fljk5jcayvsf852qmiqxd02d53vlhzz2hql7kqg6bar0jwkd11m")))

(define-public crate-messagepack-rs-macros-0.1.2 (c (n "messagepack-rs-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0vyry37wff2gpg4l69q95a6nxj52x4hzh259kq1lly2a14p8wihb")))

