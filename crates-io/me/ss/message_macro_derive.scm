(define-module (crates-io me ss message_macro_derive) #:use-module (crates-io))

(define-public crate-message_macro_derive-0.1.0 (c (n "message_macro_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1bii8sk50wcwcycysxmki44cgblhwfj6dkbi90yhb4ahfd1xwlnn")))

(define-public crate-message_macro_derive-0.1.1 (c (n "message_macro_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "02299aiqmsslsswrxl38aykb1rgdz0f0lcrc2jhhai9i4aix93sa")))

(define-public crate-message_macro_derive-0.1.2 (c (n "message_macro_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "04wv80kwc89sjyxwrf649qcs9kdzr35nxkial8qfc3bs4d529giv")))

