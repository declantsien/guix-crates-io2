(define-module (crates-io me ss message_verifier) #:use-module (crates-io))

(define-public crate-message_verifier-0.1.0 (c (n "message_verifier") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cb0n8g4cj1mprzaxxqb8007y5g0jw0f0fwclnq83lsdj8i8n4r0")))

(define-public crate-message_verifier-0.1.1 (c (n "message_verifier") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qa0hfpcwjvr12d28b33x1j8wm0jljrvpzg2q8l5jdh7a9yldvi3")))

(define-public crate-message_verifier-0.2.0 (c (n "message_verifier") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0mf7cfpli63q5jrqrci06xmyiq6lxcx9xfsmz063swnfy4y5i7pc")))

(define-public crate-message_verifier-1.0.0 (c (n "message_verifier") (v "1.0.0") (d (list (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0vq1i35pvhr76imcjxwq16wimp3rzggqi0gj9wnhi20mlrnp5apq")))

(define-public crate-message_verifier-1.0.1 (c (n "message_verifier") (v "1.0.1") (d (list (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1y111zp38fc7x2l8kqvb2x7s0li83mlv1gqc7vaqbgc0zxvqc5q8")))

(define-public crate-message_verifier-1.1.0 (c (n "message_verifier") (v "1.1.0") (d (list (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1866j8vp96xpsh9fdp847gqjnj7wsl3c6wiclwfvrmbwjp2z8jy9")))

(define-public crate-message_verifier-2.0.0 (c (n "message_verifier") (v "2.0.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "block-padding") (r "^0.3.3") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "digest") (r "^0.10.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "subtle") (r "^2.5.0") (d #t) (k 0)))) (h "1n03vglibc08lx0j1pagdy9k92vq0nhaqxgraas8j5agz77dqw5a")))

