(define-module (crates-io me ss messloc) #:use-module (crates-io))

(define-public crate-messloc-0.0.0 (c (n "messloc") (v "0.0.0") (h "02j00g9lgji7p8b0wvbgha1mn3fdimdkhg2ba3n3p5y8z9zhby0a")))

(define-public crate-messloc-0.0.1 (c (n "messloc") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.9.4") (f (quote ("mutex"))) (d #t) (k 0)))) (h "0ymdypwz4rk8hyyl4wfg0am7ang4b4h8dw463dhk9n3bi3ya3l16") (f (quote (("allocator-api")))) (r "1.68.0")))

