(define-module (crates-io me ss messagepack-cli) #:use-module (crates-io))

(define-public crate-messagepack-cli-0.1.0 (c (n "messagepack-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "error-stack") (r "^0.4.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-transcode") (r "^1.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1r1pgzsxmxibpi89asswjf0dg3bls13v6ckd54s19xspdxvibhsk")))

