(define-module (crates-io me ss message-me) #:use-module (crates-io))

(define-public crate-message-me-0.1.0 (c (n "message-me") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "083fb30iw6y4aibl3xpgcqvf2cx50lwq61cf8zdpha402bq8vjv7")))

