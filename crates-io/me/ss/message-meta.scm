(define-module (crates-io me ss message-meta) #:use-module (crates-io))

(define-public crate-message-meta-0.1.0 (c (n "message-meta") (v "0.1.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1v2isl5a2bwfqhgai7mrikf0c6n7p9l56zvv318w2ngkv0vp18c9")))

(define-public crate-message-meta-0.2.0 (c (n "message-meta") (v "0.2.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "083zs5pzqlv62bw24xhmik4wx186qx69ibhl48678mkdl1c3r26k")))

