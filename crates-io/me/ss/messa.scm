(define-module (crates-io me ss messa) #:use-module (crates-io))

(define-public crate-messa-2.0.0 (c (n "messa") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1aqsg6n1d683x684fhhqm2kb83xcz50z53p9dd94c4hnr2hpc0mf")))

(define-public crate-messa-2.0.1 (c (n "messa") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "1n33i8ya4n0k51dlicpls28pclvbavah5y68mq8ppvaz6g80w0dy")))

(define-public crate-messa-2.1.0 (c (n "messa") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "054ycljc98x89kdr7bs9glnikg854pfq5wzkqqajq4jkfl828cwp")))

