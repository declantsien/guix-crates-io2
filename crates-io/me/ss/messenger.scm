(define-module (crates-io me ss messenger) #:use-module (crates-io))

(define-public crate-messenger-0.1.0 (c (n "messenger") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0f5schmj5vxsmxf5kal8zkfnb4qwvca6q474r99ihm4907rjsrvz")))

