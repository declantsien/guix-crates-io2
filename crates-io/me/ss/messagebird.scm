(define-module (crates-io me ss messagebird) #:use-module (crates-io))

(define-public crate-messagebird-0.0.1 (c (n "messagebird") (v "0.0.1") (h "1ghhwg6r5z3lngqsma49sr50svzpxyw6bgchyik1n1sw3ykz10y5") (y #t)))

(define-public crate-messagebird-0.0.2 (c (n "messagebird") (v "0.0.2") (h "0wdcfv7z3y8dkc383k6ry1m1nm5hrid5iy4r3mkyhkd4p3y20y9n")))

