(define-module (crates-io me ss messagebus_derive) #:use-module (crates-io))

(define-public crate-messagebus_derive-0.1.0 (c (n "messagebus_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dxkdn3nh2h0kka76xd9x8pxmsl38fqfyfks1wq6xiww2j3w3isy")))

(define-public crate-messagebus_derive-0.2.0 (c (n "messagebus_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m2kwpfz2csai0afdfkr53k1pcn02w3gv2igfl9iis98xqjz00xz")))

(define-public crate-messagebus_derive-0.2.1 (c (n "messagebus_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00bm2pm37p8k0hhi31dl9pr6lz344a9hgq42q7avghm7bsw3hyzp")))

(define-public crate-messagebus_derive-0.2.2 (c (n "messagebus_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "107yg3c44207103ryqkhq351nn5c8mhg34qll04vrf71xy2sf150")))

(define-public crate-messagebus_derive-0.2.3 (c (n "messagebus_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0aa6hmjg9cw63fi24zl8c4vnv836620qsdanbd5gay1r6hvq51ry")))

(define-public crate-messagebus_derive-0.2.4 (c (n "messagebus_derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s2dvw8hw3qf3kmypvdpbhr2a3vnrcwiq3f115f1gngij7i9nyxm")))

(define-public crate-messagebus_derive-0.2.5 (c (n "messagebus_derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zp5fj52canbwjx7zsa64mzg6b1yywdrxsb3gfcv9235nyin9ws9")))

(define-public crate-messagebus_derive-0.3.0 (c (n "messagebus_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0na2rv4zdzriqk2bdwv03y3w6ssyg5bkqdvabm6xvps139pl5v7n")))

