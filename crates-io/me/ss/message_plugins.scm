(define-module (crates-io me ss message_plugins) #:use-module (crates-io))

(define-public crate-message_plugins-0.1.0 (c (n "message_plugins") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "rt-core" "sync"))) (o #t) (d #t) (k 0)))) (h "18m21ipxcphg4cv8d3qa1vdmrflqfw54kilmaqn57ly6b1lsd8l5") (f (quote (("tokio-host" "tokio" "futures"))))))

(define-public crate-message_plugins-0.1.1 (c (n "message_plugins") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "rt-core" "sync"))) (o #t) (d #t) (k 0)))) (h "0sfydyi31k831zy9arpjaa95p2xgcp8w3dfcdsjmrn07hhjxqp9a") (f (quote (("tokio-host" "tokio" "futures"))))))

(define-public crate-message_plugins-0.1.2 (c (n "message_plugins") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "rt-core" "sync"))) (o #t) (d #t) (k 0)))) (h "1khrn3jbpmks1df4c04qrx65f3x7jf6dq767849iyggzadvak4nk") (f (quote (("tokio-host" "tokio" "futures"))))))

(define-public crate-message_plugins-0.2.0 (c (n "message_plugins") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded" "rt-core" "sync"))) (o #t) (d #t) (k 0)))) (h "0bvh67vckryhvwnmm79s9pv3rnsjbh3y4kzincjmc0ff53blqn9z") (f (quote (("tokio-host" "tokio" "futures"))))))

