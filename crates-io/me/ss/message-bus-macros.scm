(define-module (crates-io me ss message-bus-macros) #:use-module (crates-io))

(define-public crate-message-bus-macros-0.1.0 (c (n "message-bus-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.73") (d #t) (k 2)))) (h "0bpckfwzzr2byhkrg571wzl9hlj06a57bjpy481sg1ab6k79jmi6")))

(define-public crate-message-bus-macros-0.1.1 (c (n "message-bus-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.73") (d #t) (k 2)))) (h "07x5x4w6ry6xqbpbf10wxjsifqg6mg543l4x9v2yzx1zrw74lqjs")))

