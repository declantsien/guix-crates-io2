(define-module (crates-io me ss mess_protector) #:use-module (crates-io))

(define-public crate-mess_protector-0.0.1 (c (n "mess_protector") (v "0.0.1") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "lincheck") (r "^0.2.1") (d #t) (t "cfg(lincheck)") (k 0)) (d (n "loom") (r "^0.6") (d #t) (t "cfg(lincheck)") (k 0)) (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 0)) (d (n "parking_lot") (r "^0.12.1") (f (quote ("arc_lock"))) (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (t "cfg(lincheck)") (k 0)) (d (n "proptest") (r "^1.4") (d #t) (t "cfg(loom)") (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0pmm2zz1aqr5pq9sba07mps6392smi6wqwgrlbkzyd4s4ignfvx9")))

