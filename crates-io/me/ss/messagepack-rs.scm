(define-module (crates-io me ss messagepack-rs) #:use-module (crates-io))

(define-public crate-messagepack-rs-0.1.0 (c (n "messagepack-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 0)))) (h "0g4r4xr7w8vx8bkkrv1yr5nlglb95fprr35is440w8bj1kwm2f10")))

(define-public crate-messagepack-rs-0.1.1 (c (n "messagepack-rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 0)))) (h "0mf9fxpsihywbxd1lbvh1qsxzgx7jmzsjxwr2n6ajl5zhrpv29sg")))

(define-public crate-messagepack-rs-0.2.0 (c (n "messagepack-rs") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 0)))) (h "0gics2155h8xdpnwfvwsvv25wy0287x0vfmmn3726di9wyhn5gxg")))

(define-public crate-messagepack-rs-0.3.0 (c (n "messagepack-rs") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "messagepack-rs-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1cq7yy2l09xzclvkncryzswc8zggqm65b5lwq9jh3g7d384nmhrm")))

(define-public crate-messagepack-rs-0.3.1 (c (n "messagepack-rs") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "messagepack-rs-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1cig41x5ln006486xn0zhpxw9xmqc6dgmn8wphg1kcdjqay6kfx0")))

(define-public crate-messagepack-rs-0.3.2 (c (n "messagepack-rs") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "messagepack-rs-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1nx9ni5xn56ikxzcly7m16lixnz39syf8zflizhqywapkplf1j5j")))

(define-public crate-messagepack-rs-0.4.0 (c (n "messagepack-rs") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "messagepack-rs-macros") (r "^0.1.1") (d #t) (k 0)))) (h "193ad4293cnskxx270sz1swm29yxwdij6s5bva9wfx514fkkwph2")))

(define-public crate-messagepack-rs-0.4.1 (c (n "messagepack-rs") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "messagepack-rs-macros") (r "^0.1.1") (d #t) (k 0)))) (h "179mwfswcm0nbghf384llf39p6w74zs9p3wdg21g6a2jwvxcdclx")))

(define-public crate-messagepack-rs-0.5.0 (c (n "messagepack-rs") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "messagepack-rs-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0fd62dbbpifh403kmrq2fwinfg1ncyn18bcq9xzsp3pg1bc3f3fz")))

(define-public crate-messagepack-rs-0.6.0 (c (n "messagepack-rs") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "messagepack-rs-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0750qzi5x8k5xp3x7wz3cax5lxlgszpyszz0kb75frbasavc62hs")))

(define-public crate-messagepack-rs-0.7.0 (c (n "messagepack-rs") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "messagepack-rs-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1ddm5acm85hljqndhpjxjkljxnjv647xxh8q9cq04px0hwv958gv")))

(define-public crate-messagepack-rs-0.8.0 (c (n "messagepack-rs") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "messagepack-rs-macros") (r "^0.1.1") (d #t) (k 0)))) (h "18pjvxj4mj9gvwivmnifkg0s0853820ysxcbpvlhqqw4irmcxf2c")))

(define-public crate-messagepack-rs-0.8.1 (c (n "messagepack-rs") (v "0.8.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "messagepack-rs-macros") (r "^0.1.2") (d #t) (k 0)))) (h "0i0kg8v7g94s8kgpy9vlk1jw0nr36bchhxnb8kdyfjiz7msa12m5")))

