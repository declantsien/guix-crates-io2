(define-module (crates-io me ss message-sink) #:use-module (crates-io))

(define-public crate-message-sink-0.1.0 (c (n "message-sink") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (f (quote ("std"))) (k 0)) (d (n "futures_ringbuf") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "15z9q2adr4c23i0manjji7y71vild5kidvd9mnf3z257wd58sf7j")))

