(define-module (crates-io me ss message-locator) #:use-module (crates-io))

(define-public crate-message-locator-1.0.0 (c (n "message-locator") (v "1.0.0") (d (list (d (n "language-objects") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08db4c94kaqlp1m02xgjcbfapkj7h05178pnr7yqbc4zmna3mrsc")))

(define-public crate-message-locator-1.0.1 (c (n "message-locator") (v "1.0.1") (d (list (d (n "language-objects") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy-regex") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jcdawz2g59sign36mr4vnkxvq3fijy8r752bd2ximr05g8xdzqj")))

