(define-module (crates-io me ss messyenv) #:use-module (crates-io))

(define-public crate-messyenv-0.1.0 (c (n "messyenv") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "1rb2rjjd0khc3sclfvgbap2159jnipycs11ij1y4kyf78jpjl3cx")))

