(define-module (crates-io me ss message_protocol) #:use-module (crates-io))

(define-public crate-message_protocol-0.1.0 (c (n "message_protocol") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "18bgzs18lwirsm5zbn0jzwca4mmzgmwyjnki5bx4vkvrj0wawiv5")))

(define-public crate-message_protocol-0.1.1 (c (n "message_protocol") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "0w6f5i0ajw9mflfgq5yqa9vblw94204k45j9p5w08bm8fw7k1z45")))

