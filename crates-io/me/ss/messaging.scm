(define-module (crates-io me ss messaging) #:use-module (crates-io))

(define-public crate-messaging-0.1.0 (c (n "messaging") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "108g6agkbydnszh6v1ls0mh5cj92myl01vnq33f09dcg0y5yav8g")))

(define-public crate-messaging-1.0.1 (c (n "messaging") (v "1.0.1") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "096idw638l45kb4hsb8xclj1v87i59s9lz7jy59apsc5zkhnjym9")))

(define-public crate-messaging-1.0.2 (c (n "messaging") (v "1.0.2") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1p8sblaz8hjypv6v3pd6wmwifx0cfdsnpla8a95cay8nv7zzv5cg")))

(define-public crate-messaging-1.0.3 (c (n "messaging") (v "1.0.3") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "02a3psr4flwbsma8yl3hgc01vkfshk8priyvbnbfmkvbmmms3nxm")))

(define-public crate-messaging-1.0.4 (c (n "messaging") (v "1.0.4") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0vp9cjkx57p609lly540czvqhr1mg7917nsrc2y147x3d1nsicw6")))

(define-public crate-messaging-1.0.5 (c (n "messaging") (v "1.0.5") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "11q6qcqva5yr36dcalgz3yl35fm0k1msh5zi279lpb96mj1p4qb2")))

(define-public crate-messaging-1.0.6 (c (n "messaging") (v "1.0.6") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1s08xc6i1zjwh7613hi04hpzpi52kpc9kmndxnrfqlxgb74i5z0m")))

