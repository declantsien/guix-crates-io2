(define-module (crates-io me ss message_filter) #:use-module (crates-io))

(define-public crate-message_filter-0.0.1 (c (n "message_filter") (v "0.0.1") (h "1ab7aljk6kq4b1kaqd9znpgszqn09xs5mhkr9969gph3p510cf9d")))

(define-public crate-message_filter-0.1.0 (c (n "message_filter") (v "0.1.0") (d (list (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0hcd666d71ax8mp43v022z7j5q9rfd1m054gf0ksraa224zhjkpv")))

(define-public crate-message_filter-0.1.1 (c (n "message_filter") (v "0.1.1") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1b11b7i9xlwxxk48qzs6ydaaq1hnf87bmlnxsa61g49v2ah9r8n7")))

(define-public crate-message_filter-0.1.2 (c (n "message_filter") (v "0.1.2") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "05cqgiia0chg56pxfv34wwjhxsqc2ajv0rlgmq0wl2i2w96j4lib")))

(define-public crate-message_filter-0.1.3 (c (n "message_filter") (v "0.1.3") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1lirc8cir18ccxk0dc71ah9qdjvyrhgy16yfvp25mz9zrfwixhny")))

(define-public crate-message_filter-0.1.4 (c (n "message_filter") (v "0.1.4") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "17pw5rd0sqsrlmfb52c96d27xzx32rzqfh31k5fbj7vkjx9x7x2j")))

(define-public crate-message_filter-0.1.5 (c (n "message_filter") (v "0.1.5") (d (list (d (n "rand") (r "~0.3.12") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 0)))) (h "1vm5l7rg94qdjscd7z8lzbgchvmj3nsbrsyr42h9wz515drpffzj")))

(define-public crate-message_filter-0.2.0 (c (n "message_filter") (v "0.2.0") (d (list (d (n "rand") (r "~0.3.12") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 0)))) (h "149jc9dcdrgpbg7fl329p9pgip5i7kmm9gq6ql3xysinrax003ch")))

(define-public crate-message_filter-0.3.0 (c (n "message_filter") (v "0.3.0") (d (list (d (n "rand") (r "~0.3.12") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 0)))) (h "0ry0mhih89famxfp4i50ny6q9kk8da2xrj9zkasc47cgfgh398xw")))

(define-public crate-message_filter-0.3.1 (c (n "message_filter") (v "0.3.1") (d (list (d (n "clippy") (r "~0.0.45") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 2)) (d (n "time") (r "~0.1.34") (d #t) (k 0)))) (h "05mq8142hm49h1r49ckqhmnl3gjvbl0blgxdsv5fn0k1brfz895x")))

(define-public crate-message_filter-0.4.0 (c (n "message_filter") (v "0.4.0") (d (list (d (n "clippy") (r "~0.0.63") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 2)) (d (n "time") (r "~0.1.35") (d #t) (k 0)))) (h "02sylw1idphv4wrc9li7immgj1dgyhxwdg8cbi5dc5qakmv8g8hn")))

(define-public crate-message_filter-0.5.0 (c (n "message_filter") (v "0.5.0") (d (list (d (n "clippy") (r "~0.0.63") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 2)))) (h "104cx36as7hqwfd21k5kyiswf9rgx6694fx3f6fn4kix44lcsy08")))

(define-public crate-message_filter-0.6.0 (c (n "message_filter") (v "0.6.0") (d (list (d (n "clippy") (r "~0.0.68") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 2)))) (h "0lhlmkm5879i8av1cqigfhlv5kyviz7l9va3xx6hw9gzm3p81kln")))

