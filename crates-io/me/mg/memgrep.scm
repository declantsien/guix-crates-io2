(define-module (crates-io me mg memgrep) #:use-module (crates-io))

(define-public crate-memgrep-0.1.0 (c (n "memgrep") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "1b70qrx3zkx8ayamvymq7g0bl7xib6xpdf1fzriah9bnagk7yji9")))

(define-public crate-memgrep-0.2.0 (c (n "memgrep") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "087anlnk3pi71ydibcgcgj24vycpi2rd21sga091j0h78m7lmvys")))

