(define-module (crates-io me rr merriam-webster-model) #:use-module (crates-io))

(define-public crate-merriam-webster-model-0.1.0 (c (n "merriam-webster-model") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 2)))) (h "019bkjvgh1nqw9g1m12l4pdlpimj9vnmnjpr2blvv6d30hqafas7")))

(define-public crate-merriam-webster-model-0.2.0 (c (n "merriam-webster-model") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 2)))) (h "08w99kzgb28wadg9izfmf5knyb7pv6d2b3yqacv7wwjyrxq5w3m7")))

