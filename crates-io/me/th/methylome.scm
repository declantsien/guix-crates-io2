(define-module (crates-io me th methylome) #:use-module (crates-io))

(define-public crate-methylome-0.3.0 (c (n "methylome") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "10xi3m3cvzj13zkhsgz1mydbydjlnwmb7w23c1nhr7l7qqxkn3h3")))

