(define-module (crates-io me th methods-enum) #:use-module (crates-io))

(define-public crate-methods-enum-0.1.1 (c (n "methods-enum") (v "0.1.1") (h "02nnijj26ba4kq4gn90h8rmsrpy20g6i7bghqi48iw4ykz1sgd5s") (y #t)))

(define-public crate-methods-enum-0.1.2 (c (n "methods-enum") (v "0.1.2") (h "1a3jwq1mnss2l6vc01m6q7nwpqxfkzf72yryzhlpgvapw6lr39yd") (y #t)))

(define-public crate-methods-enum-0.1.3 (c (n "methods-enum") (v "0.1.3") (h "0fjl6j414laq1ppk1izswfr0if6xsj71c934m65ijx7wizaffxws")))

(define-public crate-methods-enum-0.1.4 (c (n "methods-enum") (v "0.1.4") (h "1xyr012rsybyzbsz0zpsk6iqgcway3crclwpwsynzb1xwqqpk1nh")))

(define-public crate-methods-enum-0.1.5 (c (n "methods-enum") (v "0.1.5") (h "1kwp077hbp1chk5yi5rn4jbkisbqzvjg5vk1j2i0wz08l5jn9vxx")))

(define-public crate-methods-enum-0.2.0 (c (n "methods-enum") (v "0.2.0") (h "1rjmhmbxxiksz1clphpf47dv4x4il549ihqdpvvfqrjq00lx7rxk")))

(define-public crate-methods-enum-0.2.1 (c (n "methods-enum") (v "0.2.1") (h "1vx6id09yscbhjdvyrqxb84nzfk3zsp6hmgs76r6rh9zlfigkvyq")))

(define-public crate-methods-enum-0.2.2 (c (n "methods-enum") (v "0.2.2") (h "0ij0hhqagv7f2kc0a6dqfa8v814s7hb8j72s66fvsnax7q72gzjv")))

(define-public crate-methods-enum-0.2.3 (c (n "methods-enum") (v "0.2.3") (h "09449c6apl8qzhp09cc75iisf5fvv2kl17zq6ixz7qi9a0zn6l7j")))

(define-public crate-methods-enum-0.2.4 (c (n "methods-enum") (v "0.2.4") (h "0pjhiax8xqb3vh52v06iwni63496z5rhlq1zz6h32gdjfnmm08ih")))

(define-public crate-methods-enum-0.3.0 (c (n "methods-enum") (v "0.3.0") (h "0v2cmm2f83vzdl2g3apcvshq8nzk45j3s3rnljdan0gcg0fgda5k")))

(define-public crate-methods-enum-0.3.1 (c (n "methods-enum") (v "0.3.1") (h "1bwknq3cib0bm5kvx8v5jji3qz0lnvwhnawjdd4kq0i541bpfcil")))

(define-public crate-methods-enum-0.3.2 (c (n "methods-enum") (v "0.3.2") (h "07acpl67wnnphxjzhph1vk3sn6jb5xw0kqn774shgi0gg4452qi8")))

