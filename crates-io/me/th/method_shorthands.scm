(define-module (crates-io me th method_shorthands) #:use-module (crates-io))

(define-public crate-method_shorthands-0.1.0 (c (n "method_shorthands") (v "0.1.0") (h "0ksfbnwq7z67mqkzdv2fmbv4q8p428p8qv5h1gaj3d806fzzz9n7")))

(define-public crate-method_shorthands-0.2.0 (c (n "method_shorthands") (v "0.2.0") (h "0rgj7a5717qqkhvsz7c81cp1wmr6cjzb4g2dyns3r11zkqzwsxyz")))

(define-public crate-method_shorthands-0.2.1 (c (n "method_shorthands") (v "0.2.1") (h "1vpgimd0gdms97ak5nmsdcgpgnb6cc2xv3mdyn6n917b0jnn8gpc")))

