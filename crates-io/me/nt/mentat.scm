(define-module (crates-io me nt mentat) #:use-module (crates-io))

(define-public crate-mentat-0.0.1 (c (n "mentat") (v "0.0.1") (d (list (d (n "rand") (r "^0.6.3") (d #t) (k 0)))) (h "1gjf78c6npn4rh5w22fkym3pl40q1j6zhq2w4mznds73rzyvrlvp")))

(define-public crate-mentat-0.0.2 (c (n "mentat") (v "0.0.2") (d (list (d (n "rand") (r "^0.6.4") (d #t) (k 0)))) (h "05sac1m484rv23008bh4m4djlxnhmqf9fypr7s610grll63mcvwa")))

(define-public crate-mentat-0.0.3 (c (n "mentat") (v "0.0.3") (d (list (d (n "matplotrust") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)))) (h "01in1016j3gaiv1pxyq0wqd7yzwjnfcprvcnma4nwm40zxfcvy9f")))

(define-public crate-mentat-0.0.4 (c (n "mentat") (v "0.0.4") (d (list (d (n "matplotrust") (r "^0.1.5") (d #t) (k 2)) (d (n "probability") (r "^0.15.12") (d #t) (k 2)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)))) (h "091s83nz87apxh2y2xw5s3ipdm56a7284kaxwybbas42q0l415wz")))

