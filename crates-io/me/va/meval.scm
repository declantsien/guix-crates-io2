(define-module (crates-io me va meval) #:use-module (crates-io))

(define-public crate-meval-0.0.2 (c (n "meval") (v "0.0.2") (d (list (d (n "gnuplot") (r "^0.0.18") (d #t) (k 2)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)))) (h "0jlzwb1kkfzk8c78d85l0jqsaf016aicwiq620wd16j896xn1464")))

(define-public crate-meval-0.0.3 (c (n "meval") (v "0.0.3") (d (list (d (n "gnuplot") (r "^0.0.18") (d #t) (k 2)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)))) (h "07gb0vyljqzxrplnla362dy9yvbajmbf66dqd2qn7qky1x01i65f")))

(define-public crate-meval-0.0.4 (c (n "meval") (v "0.0.4") (d (list (d (n "gnuplot") (r "^0.0.18") (d #t) (k 2)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)))) (h "1kyrj7qn760flwih08w46q5qr9kj88q8d2z5a7hwrbj5rgqv7p40")))

(define-public crate-meval-0.0.5 (c (n "meval") (v "0.0.5") (d (list (d (n "gnuplot") (r "^0.0.18") (d #t) (k 2)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)))) (h "1583i8a5x2gm1cvdkni5xhs0dbqv0vl4gvblbsnxp6namhqkh811")))

(define-public crate-meval-0.0.6 (c (n "meval") (v "0.0.6") (d (list (d (n "gnuplot") (r "^0.0.18") (d #t) (k 2)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)))) (h "034i5s19sph2qhjr3sxvh7w838v2kyfq1g5c6qq3g3b8bjr8gc09")))

(define-public crate-meval-0.0.7 (c (n "meval") (v "0.0.7") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.18") (d #t) (k 2)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)))) (h "150arhylj3jg19f7rsp3d8ay1vqwyfnfgd4az2c3a8kvj99nw3j2")))

(define-public crate-meval-0.0.8 (c (n "meval") (v "0.0.8") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.18") (d #t) (k 2)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_test") (r "^0.9") (d #t) (k 2)) (d (n "toml") (r "^0.3") (d #t) (k 2)))) (h "04xbziamw4dfqfrsm67iw51nr544pxh7yy7fx3spr59b61hc749p") (f (quote (("default" "serde")))) (y #t)))

(define-public crate-meval-0.0.9 (c (n "meval") (v "0.0.9") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.18") (d #t) (k 2)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_test") (r "^0.9") (d #t) (k 2)) (d (n "toml") (r "^0.3") (d #t) (k 2)))) (h "1mfp1jfs1l1kmikns4zcz058kksp1aq783g4gm24cidxdr6xb258") (f (quote (("default" "serde"))))))

(define-public crate-meval-0.1.0 (c (n "meval") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.23") (d #t) (k 2)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.4.5") (d #t) (k 2)))) (h "1ypnp4fg8ij9x7m82g10dvl16g1vbfh66h9pbkmk148f3hbdfvhl") (f (quote (("default"))))))

(define-public crate-meval-0.2.0 (c (n "meval") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.5") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.23") (d #t) (k 2)) (d (n "nom") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0.4.5") (d #t) (k 2)))) (h "1ncj0fv9q3b4his7g947fmgkskpfly6dsniw0g6mg38wcnjrd57p") (f (quote (("default"))))))

