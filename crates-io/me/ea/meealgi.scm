(define-module (crates-io me ea meealgi) #:use-module (crates-io))

(define-public crate-meealgi-0.0.1 (c (n "meealgi") (v "0.0.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)))) (h "1d6cgkwmz75d6k8mgnbkrff4h64j1jcjrj9jf574jhc68b48cmwx")))

(define-public crate-meealgi-0.0.2 (c (n "meealgi") (v "0.0.2") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)))) (h "0fm86cfmm6svgwfqf2l9pf732bg41p97zixd1zrci0y75xf7yx7c")))

(define-public crate-meealgi-0.0.3 (c (n "meealgi") (v "0.0.3") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)))) (h "0zxmfxkqcz66brfb3f1sci8ghlfz63i1yzhy6phcxlh3kxsqcvc8")))

(define-public crate-meealgi-0.0.5 (c (n "meealgi") (v "0.0.5") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "clippy") (r "~0.0") (o #t) (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1vn9pg3p8izr9x3c8l5irkr9ycmivfrn1pshwxxlakmnkhyij7hr") (f (quote (("default"))))))

(define-public crate-meealgi-0.0.6 (c (n "meealgi") (v "0.0.6") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "clippy") (r "~0.0") (o #t) (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0w7dlwsb7ys1576k1njpakv801cy17sf0nq1a21nq1my123azk63") (f (quote (("default"))))))

