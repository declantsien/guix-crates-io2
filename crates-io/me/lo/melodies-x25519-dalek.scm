(define-module (crates-io me lo melodies-x25519-dalek) #:use-module (crates-io))

(define-public crate-melodies-x25519-dalek-0.1.0 (c (n "melodies-x25519-dalek") (v "0.1.0") (d (list (d (n "melodies-core") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.0-pre.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (d #t) (k 0)))) (h "1bbshfsfqhpwli32a1sshpjmmrdmaxz71m5889akgg9zmdhsk052")))

