(define-module (crates-io me lo melodies-core) #:use-module (crates-io))

(define-public crate-melodies-core-0.1.0 (c (n "melodies-core") (v "0.1.0") (d (list (d (n "zeroize") (r "^1.5.7") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "1qxfkc5434lfccamiaxzbq0lm0883acxmjp0nnapihw3r5wz0jg2")))

