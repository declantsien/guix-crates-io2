(define-module (crates-io me lo melodium-doc) #:use-module (crates-io))

(define-public crate-melodium-doc-0.6.0 (c (n "melodium-doc") (v "0.6.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "melodium-common") (r "^0.6.0") (d #t) (k 0)))) (h "0hg9p10sc3q1y0aypib8nb4gs593fnbm1g05jh9829zch1iq5s87") (r "1.60")))

(define-public crate-melodium-doc-0.7.0-rc1 (c (n "melodium-doc") (v "0.7.0-rc1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "melodium-common") (r "=0.7.0-rc1") (d #t) (k 0)))) (h "1f6n4shycm1z62a9br9mdk2qx4agps9klc9agxfblm0kgxfclhna") (r "1.60")))

(define-public crate-melodium-doc-0.7.0 (c (n "melodium-doc") (v "0.7.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "melodium-common") (r "=0.7.0") (d #t) (k 0)))) (h "0ihy0n1qnz6hamcq8yz044grvrcv6c35lh2kcxdffq2l034ny9v3") (r "1.60")))

(define-public crate-melodium-doc-0.7.1 (c (n "melodium-doc") (v "0.7.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "melodium-common") (r "^0.7.1") (d #t) (k 0)))) (h "1yi2b9rj5z6hfjqk0fnm2nf62qmv5bs8abkj8g9grrr9j8gi7f6n") (r "1.60")))

(define-public crate-melodium-doc-0.8.0-rc1 (c (n "melodium-doc") (v "0.8.0-rc1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "melodium-common") (r "=0.8.0-rc1") (d #t) (k 0)))) (h "1jbm2xza4v7h51y55pxh43yf9a8vmjzmc598nzfxfbmln44fvmkp") (r "1.60")))

(define-public crate-melodium-doc-0.8.0-rc2 (c (n "melodium-doc") (v "0.8.0-rc2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "melodium-common") (r "=0.8.0-rc2") (d #t) (k 0)))) (h "1vyaacpb7n7rijhly5j2qbb94agigv9m1aagfy7z2f9s3vm2g624") (r "1.60")))

(define-public crate-melodium-doc-0.8.0-rc3 (c (n "melodium-doc") (v "0.8.0-rc3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "melodium-common") (r "=0.8.0-rc3") (d #t) (k 0)))) (h "13yk11fi5chb2lswyqz0wvcvhbzc73wgxzlk3vgda5k5njji6b9q") (r "1.60")))

(define-public crate-melodium-doc-0.8.0 (c (n "melodium-doc") (v "0.8.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "melodium-common") (r "^0.8.0") (d #t) (k 0)))) (h "0p1q2y6zpk9lbc4k4ydpzpl77libj5vvz38c0flllg9z8i82yljn") (r "1.60")))

