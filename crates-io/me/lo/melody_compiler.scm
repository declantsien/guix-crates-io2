(define-module (crates-io me lo melody_compiler) #:use-module (crates-io))

(define-public crate-melody_compiler-0.1.0 (c (n "melody_compiler") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1zdb96i7vs79qsk2dz27hyk7kqvzs1vxmkna5gmq1rkvwbddh9f6") (r "1.58.0")))

(define-public crate-melody_compiler-0.1.1 (c (n "melody_compiler") (v "0.1.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "06c0n7jq59zagq6v3blczvc1njbmfxw2hsq1rblvjyic9l9acniv") (r "1.58.0")))

(define-public crate-melody_compiler-0.2.0 (c (n "melody_compiler") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0bn2cwqmfjz8lzwn69l3sf33n5ppclnqhy4z6ns27zs8w0b80ayr") (r "1.58.0")))

(define-public crate-melody_compiler-0.3.0 (c (n "melody_compiler") (v "0.3.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1fd90cw3yg85h6ynfv225i1r03dn9fs48r4lix3gx88npm26d1y4") (r "1.58.0")))

(define-public crate-melody_compiler-0.4.0 (c (n "melody_compiler") (v "0.4.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "10j467hg2srmjzxc30xmsjdkz8i7pi3bp7ccd0xjj14bkiy6qbgj") (r "1.58.0")))

(define-public crate-melody_compiler-0.4.1 (c (n "melody_compiler") (v "0.4.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0qjifwbwwfyv09zdbflidnzb6glqs8z8nzgixyvxdpk8spcp6p44") (r "1.58.0")))

(define-public crate-melody_compiler-0.4.2 (c (n "melody_compiler") (v "0.4.2") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0jy4frvgcsdigaa1yh6nmsh4p5bqcvg9si2mwnkygxgc3wrm6yx6") (r "1.58.0")))

(define-public crate-melody_compiler-0.5.0 (c (n "melody_compiler") (v "0.5.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1av8wdfnxl0s5rvw52bqfvwgpaf78r2gmw6ca220widw6fjd5viy") (r "1.58.0")))

(define-public crate-melody_compiler-0.6.0 (c (n "melody_compiler") (v "0.6.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "19vzsg4530wzjxpl88i0d5iimyg6rvssf641bhwqzbgmac37hz4i") (r "1.58.0")))

(define-public crate-melody_compiler-0.7.0 (c (n "melody_compiler") (v "0.7.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0nsgpff0kfz7hw63d7h7jgsc8lajbpqb96nq5zwis10xr1vn6hsl") (r "1.58.0")))

(define-public crate-melody_compiler-0.8.0 (c (n "melody_compiler") (v "0.8.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "16naw81mqw558447cigmbl0hs30gs84g6gmjjfnxvifl26pzgl03") (r "1.58.0")))

(define-public crate-melody_compiler-0.9.0 (c (n "melody_compiler") (v "0.9.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1zj1375vwvdxhc53rjp28956zqji6k9dzh9s9hbkm66iwq2l2qhc") (r "1.58.0")))

(define-public crate-melody_compiler-0.11.0 (c (n "melody_compiler") (v "0.11.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0n2q4ksmzysdvsxdffm7prjfg5syvbw0vhy1gr8xjvcm1h917r4f") (r "1.58.0")))

(define-public crate-melody_compiler-0.11.1 (c (n "melody_compiler") (v "0.11.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1fsn9hchs06yixjizbqszn1z2v0khvf5v8m149ixqng4dlvqwvhs") (r "1.58.0")))

(define-public crate-melody_compiler-0.12.0 (c (n "melody_compiler") (v "0.12.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1cvjrik9c1wwb1f9sj5myi9il6w9253l1viwnd79axvdynvhhdhs") (r "1.58.0")))

(define-public crate-melody_compiler-0.12.2 (c (n "melody_compiler") (v "0.12.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "09c2adc2z279lad0vq8l3hkqw8y4fwfvr7pxp854dn3j0vyksb12") (r "1.58.0")))

(define-public crate-melody_compiler-0.12.3 (c (n "melody_compiler") (v "0.12.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0fr5pwmfkqgfb0li5y75kxn0j1afhhyr4fy14r6rwwr2gvx175y1") (r "1.58.0")))

(define-public crate-melody_compiler-0.12.4 (c (n "melody_compiler") (v "0.12.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "176zg39rq6nsmhl855jk28czpynl1sz2f60al3yqd1f6bgqs15yl") (r "1.58.0")))

(define-public crate-melody_compiler-0.13.0 (c (n "melody_compiler") (v "0.13.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0i8x206ci1mfymgggb0mz0a0n9sibx8lj2vmjj0jqdd864wl1pmg") (r "1.58.0")))

(define-public crate-melody_compiler-0.13.1 (c (n "melody_compiler") (v "0.13.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1a5ghwqxi4h8v0a0pwkplpxjly7sklsqdps714di948f1c9xpqd5") (r "1.58.0")))

(define-public crate-melody_compiler-0.13.2 (c (n "melody_compiler") (v "0.13.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0kr97hsn8i3da9r07rpqj6b14nvldsk7si9cngvb2cp736jjkwb8") (r "1.58.0")))

(define-public crate-melody_compiler-0.13.3 (c (n "melody_compiler") (v "0.13.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0npmak6fc6mi1dqnbxsrxj54y9jwisb3v7qq65r45cvwly0qi1w3") (r "1.58.0")))

(define-public crate-melody_compiler-0.13.4 (c (n "melody_compiler") (v "0.13.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "05mxzmr7f4vyhsig86lncydri6777w9s8l4xv52g0zl70xfzlx4a") (r "1.58.0")))

(define-public crate-melody_compiler-0.13.5 (c (n "melody_compiler") (v "0.13.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1gzj5gnrbrg1q9wi9wg0aqw2vv5lppypsa3gismwl3jfl2p40sai") (r "1.58.0")))

(define-public crate-melody_compiler-0.13.6 (c (n "melody_compiler") (v "0.13.6") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "10m818slcw1hd396mzn6s5n6007n1nkm63mcidxg6yiymg76yq1h") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.13.7 (c (n "melody_compiler") (v "0.13.7") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "04kw4r6krr289cvhiw6a7hgjgwdmlvl01hgc8rp5kd1lih8a5l7i") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.13.8 (c (n "melody_compiler") (v "0.13.8") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0aaxdxnirfbb3ccl6z2f3fqc19jdw6dn91bbhz20k1ikw15a82d3") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.13.9 (c (n "melody_compiler") (v "0.13.9") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1k95jpk4cad25mrpg151l27cflizlvs2i4jl2b0mir78yrgh3rmf") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.13.10 (c (n "melody_compiler") (v "0.13.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01vxsxqzm5qkw86vbkd9pd3s8yrm97b7231mihyszcabh3b3v3fm") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.14.0 (c (n "melody_compiler") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rcjrzz3vyykqvyayhwzfp48150qawclznwk3af78x948arq143m") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.15.0 (c (n "melody_compiler") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "162kkv9mn5a4gx27dzfnh7b42knvz76i0pv1qlalqn31q5g11p3n") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.16.0 (c (n "melody_compiler") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yrpa5q7lkax9ib47wj89nxj8i6yy7bbgzh78vx7b4gx520c83ri") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.16.1 (c (n "melody_compiler") (v "0.16.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bxdv4wgmhhy44ni5qsk0sbc4hn6rj45l766gwc4qm23x69d4vlf") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.17.0 (c (n "melody_compiler") (v "0.17.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0aj6ynspmqyh0b9cjan7wdhbdxl30iw72xhkwpbrmnppi1wqpjwq") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.18.0 (c (n "melody_compiler") (v "0.18.0") (d (list (d (n "arbitrary") (r "^1.1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1zvxlxy1gpl5y3yg0d2kg77sjg3wwhwci0bvq5ph6av1hvg4y3a4") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.18.1 (c (n "melody_compiler") (v "0.18.1") (d (list (d (n "arbitrary") (r "^1.1.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "indoc") (r "^1.0.6") (d #t) (k 2)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1mkqsf9zbnykxqbai7hzjwxgrzwwr7dkwa2ff6mbjg4nj7nxyrbc") (f (quote (("fuzzer" "arbitrary")))) (r "1.58.0")))

(define-public crate-melody_compiler-0.19.0 (c (n "melody_compiler") (v "0.19.0") (d (list (d (n "arbitrary") (r "^1.1.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "indoc") (r "^2.0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1y3gdhmg0fpfbp3ha3yycq48v5mr1sd3fqgxs1j65snw98yj313n") (f (quote (("fuzzer" "arbitrary"))))))

