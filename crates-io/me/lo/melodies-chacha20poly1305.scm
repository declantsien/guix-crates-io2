(define-module (crates-io me lo melodies-chacha20poly1305) #:use-module (crates-io))

(define-public crate-melodies-chacha20poly1305-0.1.0 (c (n "melodies-chacha20poly1305") (v "0.1.0") (d (list (d (n "chacha20poly1305") (r "^0.10") (d #t) (k 0)) (d (n "melodies-core") (r "^0.1") (d #t) (k 0)))) (h "1dcq7s5f532s2lwknhl8lksk3yrdw2v22c4mc10h8bvyp9fa2ax5")))

