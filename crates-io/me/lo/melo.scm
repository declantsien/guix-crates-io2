(define-module (crates-io me lo melo) #:use-module (crates-io))

(define-public crate-melo-0.1.0 (c (n "melo") (v "0.1.0") (d (list (d (n "ansi_term") (r "~0.10.2") (d #t) (k 0)) (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "~1.0.0") (d #t) (k 0)) (d (n "mktemp") (r "~0.3.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "~0.4.1") (d #t) (k 2)) (d (n "regex") (r "~0.2.5") (d #t) (k 0)) (d (n "rimd") (r "~0.0.1") (d #t) (k 0)) (d (n "structopt") (r "~0.1.6") (d #t) (k 0)) (d (n "structopt-derive") (r "~0.1.6") (d #t) (k 0)))) (h "0yyag8clx2z405ym371lhmskwr08zcqlh2cljzjf0afs1b69nsfs")))

