(define-module (crates-io me lo melodies) #:use-module (crates-io))

(define-public crate-melodies-0.1.0 (c (n "melodies") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (f (quote ("serde"))) (d #t) (k 2)) (d (n "melodies-blake2") (r "^0.1") (d #t) (k 0)) (d (n "melodies-chacha20poly1305") (r "^0.1") (d #t) (k 2)) (d (n "melodies-core") (r "^0.1") (d #t) (k 0)) (d (n "melodies-ring") (r "^0.1") (d #t) (k 0)) (d (n "melodies-x25519-dalek") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0v3qhgc3g3kn0397sbyfxr7fy7bjg5a8xs9fapci2896xx2xa2sf")))

