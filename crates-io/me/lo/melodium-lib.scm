(define-module (crates-io me lo melodium-lib) #:use-module (crates-io))

(define-public crate-melodium-lib-0.0.0-reserved (c (n "melodium-lib") (v "0.0.0-reserved") (h "1mm8nj39as6l9zqwzqnzy8s8ckn45hwp9izsjkdcm2sna4sr19k3")))

(define-public crate-melodium-lib-0.0.1-reserved (c (n "melodium-lib") (v "0.0.1-reserved") (h "1ig3pa7dr6a29sp0ysmp4m798r7j5hlwla4kwjxm9ab7c8dcp65l")))

