(define-module (crates-io me lo melodies-blake2) #:use-module (crates-io))

(define-public crate-melodies-blake2-0.1.0 (c (n "melodies-blake2") (v "0.1.0") (d (list (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (f (quote ("zeroize"))) (d #t) (k 0)) (d (n "melodies-core") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "15m1pw1whj2y60c8zzkrp7a5wwavdipp4vkc5s6nvahn2gx2gwpy")))

