(define-module (crates-io me lo melodies-ring) #:use-module (crates-io))

(define-public crate-melodies-ring-0.1.0 (c (n "melodies-ring") (v "0.1.0") (d (list (d (n "melodies-core") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (d #t) (k 0)))) (h "1x3q1n3lifnzn97ziw3i80p99hrnbgsc5n0il6awaxlx9myfbbwh")))

