(define-module (crates-io me lo melodium-core) #:use-module (crates-io))

(define-public crate-melodium-core-0.6.0 (c (n "melodium-core") (v "0.6.0") (d (list (d (n "melodium-common") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0lg1ijd35scmgwsh6hjs2pnylak99snya67ldxnmv77zz1i3rccl") (f (quote (("doc") ("default" "doc")))) (r "1.60")))

(define-public crate-melodium-core-0.7.0-rc1 (c (n "melodium-core") (v "0.7.0-rc1") (d (list (d (n "melodium-common") (r "=0.7.0-rc1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0hld7mk2vmjmzapf7a554cr0mcg80f1jb5gzrgz0n8ihy9avirwy") (f (quote (("doc") ("default")))) (r "1.60")))

(define-public crate-melodium-core-0.7.0 (c (n "melodium-core") (v "0.7.0") (d (list (d (n "melodium-common") (r "=0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "188vz5xk05aq23zym6dzr7q23gb0kmhv6ryj1rhq3qn92c545i3b") (f (quote (("doc") ("default")))) (r "1.60")))

(define-public crate-melodium-core-0.7.1 (c (n "melodium-core") (v "0.7.1") (d (list (d (n "melodium-common") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0yzas6yg2d3n0fgv4q4drg6fd4m1wfjm70wv6gyq6d8f8c5gc2fz") (f (quote (("doc") ("default")))) (r "1.60")))

(define-public crate-melodium-core-0.8.0-rc1 (c (n "melodium-core") (v "0.8.0-rc1") (d (list (d (n "erased-serde") (r "^0.4") (d #t) (k 0)) (d (n "melodium-common") (r "=0.8.0-rc1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "035zyxm903r8mlyhx2i6a6k997xn5p8dmp8p75imk36mpkdlqnli") (f (quote (("doc") ("default")))) (r "1.60")))

(define-public crate-melodium-core-0.8.0-rc2 (c (n "melodium-core") (v "0.8.0-rc2") (d (list (d (n "erased-serde") (r "^0.4") (d #t) (k 0)) (d (n "melodium-common") (r "=0.8.0-rc2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xlpwmrsqywl3r0wmpmlyrxfppa26x2j2r3kxbgnnfy3f297n7cr") (f (quote (("doc") ("default")))) (r "1.60")))

(define-public crate-melodium-core-0.8.0-rc3 (c (n "melodium-core") (v "0.8.0-rc3") (d (list (d (n "erased-serde") (r "^0.4") (d #t) (k 0)) (d (n "melodium-common") (r "=0.8.0-rc3") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00k2snncshwwaabapl7zf00ghgf2410dwimm8hhad4i75r2rznd7") (f (quote (("doc") ("default")))) (r "1.60")))

(define-public crate-melodium-core-0.8.0 (c (n "melodium-core") (v "0.8.0") (d (list (d (n "erased-serde") (r "^0.4") (d #t) (k 0)) (d (n "melodium-common") (r "^0.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yvxa297lx2c8nv3brf4ryjvldab9lvs75dfi4v0n6i7s9n6d2zj") (f (quote (("doc") ("default")))) (r "1.60")))

