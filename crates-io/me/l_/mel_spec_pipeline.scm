(define-module (crates-io me l_ mel_spec_pipeline) #:use-module (crates-io))

(define-public crate-mel_spec_pipeline-0.2.1 (c (n "mel_spec_pipeline") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "mel_spec") (r "^0.2.1") (d #t) (k 0)) (d (n "mel_spec_audio") (r "^0.2.1") (d #t) (k 0)))) (h "19p019n4dafdc0vjgfr3d239xxxdf2nhfmwicrrmijpdxmjyly8b")))

(define-public crate-mel_spec_pipeline-0.2.2 (c (n "mel_spec_pipeline") (v "0.2.2") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "mel_spec") (r "^0.2.2") (d #t) (k 0)) (d (n "mel_spec_audio") (r "^0.2.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0m14cz504nqqlf8s93a9fbzfsddvf9vhh6bziysniqyy2a50b860")))

