(define-module (crates-io me l_ mel_filter) #:use-module (crates-io))

(define-public crate-mel_filter-0.1.0 (c (n "mel_filter") (v "0.1.0") (d (list (d (n "itertools-num") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "08n59fw8ka1pp5s06ds4pfvs306ars3vrill8v8c5xqkqrv72qr1")))

(define-public crate-mel_filter-0.1.1 (c (n "mel_filter") (v "0.1.1") (d (list (d (n "itertools-num") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1fl2ncj9vf73xw7bsmwlv63arm7dxbrdgh91z9j5nan0298pa2l6")))

