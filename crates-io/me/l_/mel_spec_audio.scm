(define-module (crates-io me l_ mel_spec_audio) #:use-module (crates-io))

(define-public crate-mel_spec_audio-0.2.1 (c (n "mel_spec_audio") (v "0.2.1") (h "1pgg1kclk2h837xilj08fsazm7kshlg5vyxmmqrgjlq6syn0lrsg")))

(define-public crate-mel_spec_audio-0.2.2 (c (n "mel_spec_audio") (v "0.2.2") (h "1vzj9crlajzi6h3dp2ij67z6xqa2bf0da8p6scxg8z2qk2crvxlq")))

