(define-module (crates-io me l_ mel_spec) #:use-module (crates-io))

(define-public crate-mel_spec-0.1.0 (c (n "mel_spec") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0kmwm54zxdxmh0rybwd0jv1yagqc8vqdcymr0ia5im1mcxbr8jgg")))

(define-public crate-mel_spec-0.1.1 (c (n "mel_spec") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "1sm1xjhznxl8ni0dlk21hqg2wcdprbsrj4mpj5mp2k5blpxbafrc")))

(define-public crate-mel_spec-0.1.2 (c (n "mel_spec") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0cbxvxxc2sr9mppszpndg35x86lp2lj50bjzhj9r75mykfhhhk0r")))

(define-public crate-mel_spec-0.2.0 (c (n "mel_spec") (v "0.2.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "074gqi6ry15c1v6cj0lp1gjrhpz760hkmibkqf4337p1n3g0p94b")))

(define-public crate-mel_spec-0.2.1 (c (n "mel_spec") (v "0.2.1") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0m9i1bnk20fbibzpm2mdb32mhqxaipqr07968r1f040li07pk37p")))

(define-public crate-mel_spec-0.2.2 (c (n "mel_spec") (v "0.2.2") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0gaaq3444rvsgylbd31fy75l43pa5brhadmib2c3ygj443b7aajw")))

(define-public crate-mel_spec-0.2.3 (c (n "mel_spec") (v "0.2.3") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (f (quote ("wasm_simd"))) (d #t) (k 0)))) (h "0z193csh05cm5n774d1sar7a8ls213z7dimz03shmd4rmxwlkcj0")))

