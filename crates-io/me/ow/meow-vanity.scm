(define-module (crates-io me ow meow-vanity) #:use-module (crates-io))

(define-public crate-meow-vanity-0.4.10 (c (n "meow-vanity") (v "0.4.10") (d (list (d (n "blake2") (r "^0.7.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^1.2.1") (d #t) (k 0)) (d (n "digest") (r "^0.7.2") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1.43") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8.0") (d #t) (k 0)) (d (n "ocl") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1ma67pq44x5syds8xxigyyn1ivssg7phxjbwi69nc19fxqcw8mas") (f (quote (("gpu" "ocl") ("default" "gpu"))))))

