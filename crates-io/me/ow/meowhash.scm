(define-module (crates-io me ow meowhash) #:use-module (crates-io))

(define-public crate-meowhash-0.1.0 (c (n "meowhash") (v "0.1.0") (d (list (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "05ygyvvbsl80bapqjwk0qij3l8ynxmaf33sj9p6z6prlm0k8wib9")))

(define-public crate-meowhash-0.1.1 (c (n "meowhash") (v "0.1.1") (d (list (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "1g478sqas1p3avn2bkzm4a6jnj0fx4n2ywl38nlxcpaxfrkaizyz")))

(define-public crate-meowhash-0.1.2 (c (n "meowhash") (v "0.1.2") (d (list (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "02r3m1h5k27160z7pfnp01qmidbar6z8fyvf1qzmqlm35yp8a2mh")))

(define-public crate-meowhash-0.1.3 (c (n "meowhash") (v "0.1.3") (d (list (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)))) (h "1qdkp40j8sgbc6iyz90rb2nlbr55p5ln6qmj40myd0nnnqd84xb6")))

(define-public crate-meowhash-0.2.0 (c (n "meowhash") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.50") (o #t) (d #t) (k 1)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)))) (h "17qcng6zpa738qrv22ahwlkzzkl57hxp9p2nngyid02w2wdary07") (f (quote (("ffi" "libc" "cc"))))))

(define-public crate-meowhash-0.3.0 (c (n "meowhash") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.50") (o #t) (d #t) (k 1)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "16q8hfl6467yc7bazp8zrf9sngq8kxgbv8v6m9fjhj6wq4jc277k") (f (quote (("ffi" "libc" "cc"))))))

