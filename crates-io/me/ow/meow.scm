(define-module (crates-io me ow meow) #:use-module (crates-io))

(define-public crate-meow-0.0.0 (c (n "meow") (v "0.0.0") (h "0n8bfl7vhflqjijj3ydhivgfjv31xpsqm2nr60wncdp5lgz5rkqj")))

(define-public crate-meow-0.2.0 (c (n "meow") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0j8c9cqva14jjg6vl1zfr2m373kbmgik29jr650mwjid4s93dyir") (y #t)))

(define-public crate-meow-0.2.1 (c (n "meow") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 0)))) (h "00dcx5w2ws3abg4kmp9fm0k1r9f5i82396ng60rvp3xcjny6z9h8")))

(define-public crate-meow-0.2.2 (c (n "meow") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 0)))) (h "0wp0i5p9cmz1n8qk3qaykvrx57scflgqvvk3pln5l0b46iyblz8z")))

