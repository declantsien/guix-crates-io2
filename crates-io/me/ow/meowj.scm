(define-module (crates-io me ow meowj) #:use-module (crates-io))

(define-public crate-meowj-0.1.0 (c (n "meowj") (v "0.1.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vh35b6a8265qn5wgq67r1f2zdy68akibx8l4knmwqdc25q6lck5")))

(define-public crate-meowj-0.2.0 (c (n "meowj") (v "0.2.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1vgyybcx96vmi6dy2vf4r3b764qwhm794456y8rp10bxgblh5dc6") (f (quote (("default" "console_error_panic_hook"))))))

