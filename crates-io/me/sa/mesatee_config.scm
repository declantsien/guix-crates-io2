(define-module (crates-io me sa mesatee_config) #:use-module (crates-io))

(define-public crate-mesatee_config-0.0.1 (c (n "mesatee_config") (v "0.0.1") (h "09a2x751paw6zk92h78i40pgwmmvmxp1f5cpzzyxd5pm3mx68rbx")))

(define-public crate-mesatee_config-0.1.0 (c (n "mesatee_config") (v "0.1.0") (h "1ka44jldbd3mxlkf25nfds2ld4szzy6s871x6rcczzgw4fa6zw3k")))

