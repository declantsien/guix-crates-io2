(define-module (crates-io me sa mesatee_core) #:use-module (crates-io))

(define-public crate-mesatee_core-0.0.1 (c (n "mesatee_core") (v "0.0.1") (h "1wyiqbi83kkx75l59lavykcbszlj3f30lp8p3z5pkrrhzcbqb164")))

(define-public crate-mesatee_core-0.1.0 (c (n "mesatee_core") (v "0.1.0") (h "0qd1if2y9qdh044rf1rbn92sk6zakxnx5dfmqqvcynsf2k6cf07y")))

