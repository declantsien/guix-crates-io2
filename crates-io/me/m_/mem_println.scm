(define-module (crates-io me m_ mem_println) #:use-module (crates-io))

(define-public crate-mem_println-0.1.0 (c (n "mem_println") (v "0.1.0") (h "1r5mn3k7xkgqarx72p6wcy8gn3kcnr6zdgxj6p7yn4782dkx2dwd") (f (quote (("println_mem") ("default" "println_mem")))) (y #t) (r "1.60.0")))

(define-public crate-mem_println-0.1.1 (c (n "mem_println") (v "0.1.1") (h "1a52sjxdw10yfp1zdki7q492zqiqkj8gjcji1i7r2gr71biyms2n") (f (quote (("println_mem") ("default" "println_mem")))) (y #t) (r "1.60.0")))

(define-public crate-mem_println-0.1.2 (c (n "mem_println") (v "0.1.2") (h "0611fqac9w7cw9hf0kx8gqa7fahn0v66mil3ny0pk0qhindpwaa3") (f (quote (("println_mem") ("default" "println_mem")))) (y #t) (r "1.60.0")))

(define-public crate-mem_println-0.1.3 (c (n "mem_println") (v "0.1.3") (h "1lqf2y1pnclai891hw39b69fp9fdycnj1wqb24lzswgxf3ny7lq3") (f (quote (("println_mem") ("default" "println_mem")))) (y #t) (r "1.60.0")))

(define-public crate-mem_println-0.1.4 (c (n "mem_println") (v "0.1.4") (h "1znbr62h2pycrmalixx6g9p7k6400yc26rkwcjjx4c5a1najcgqs") (f (quote (("println_mem") ("default" "println_mem")))) (r "1.60.0")))

(define-public crate-mem_println-0.1.5 (c (n "mem_println") (v "0.1.5") (h "1sg7ysm2cdszidlfpi70nbkkggdqc2dxj21nqzmlm77hfmqvwnlg") (f (quote (("println_mem") ("get_size_of_val") ("default" "println_mem" "get_size_of_val")))) (r "1.60.0")))

