(define-module (crates-io me m_ mem_tools) #:use-module (crates-io))

(define-public crate-mem_tools-0.1.0 (c (n "mem_tools") (v "0.1.0") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1p6yni1qy5ri0hb7b4mkr5ax85krxg4qq57p7l02h6lgg8kzk3f4") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mem_tools-0.1.1 (c (n "mem_tools") (v "0.1.1") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0irifav33gml0bc36x48sq7rkc1rr6dp04cjl7w2066r7569l8zh") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mem_tools-0.2.0 (c (n "mem_tools") (v "0.2.0") (d (list (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "16plyimiz7vfwnkrmafs2q23s3rsf1g3fr4npyj82cqf8rr0sj57") (f (quote (("use_alloc") ("no_std") ("full" "use_alloc" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mem_tools-0.3.0 (c (n "mem_tools") (v "0.3.0") (d (list (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "0xa1ap45cwhwlsks7jsd86f4skzynpg8k9cmpi8639h0dsddras4") (f (quote (("use_alloc") ("no_std") ("full" "use_alloc" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mem_tools-0.4.0 (c (n "mem_tools") (v "0.4.0") (d (list (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "0ir4dqghz4d5wf2m170mkgm0v3pmwhvfzxqzb0ha4l92sjbrd095") (f (quote (("use_alloc") ("no_std") ("full" "use_alloc" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mem_tools-0.5.0 (c (n "mem_tools") (v "0.5.0") (d (list (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1lvgq6wasdi9ayca64nc1y10abvifz0a0632rnkl1g13lwjfn0h0") (f (quote (("use_alloc") ("no_std") ("full" "use_alloc" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mem_tools-0.6.0 (c (n "mem_tools") (v "0.6.0") (d (list (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "1wyr0wg7gab1qn4jl5as6z2g1k5rj4dz8qh4cx03hixffxsgysqj") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "use_alloc" "enabled") ("enabled") ("default" "enabled"))))))

