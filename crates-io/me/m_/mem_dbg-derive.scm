(define-module (crates-io me m_ mem_dbg-derive) #:use-module (crates-io))

(define-public crate-mem_dbg-derive-0.1.0 (c (n "mem_dbg-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1jqgqzqmikdha69i0lxy83kknygw2qhr2q05lv3mxd1ilb8zhjqj")))

(define-public crate-mem_dbg-derive-0.1.2 (c (n "mem_dbg-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "063adl1sk1812yccvy4qp566impqyz73dqswhdpib9930wvsd43r")))

(define-public crate-mem_dbg-derive-0.1.3 (c (n "mem_dbg-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "18q9izy1995il7brn82sm628wbqdqkx28mkzngvk40z0mbf1fjkf")))

(define-public crate-mem_dbg-derive-0.1.4 (c (n "mem_dbg-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "00aykaliqqsr3isg6dmlmqsc87jx2c3237f6jy8b06w8xcwl9daq")))

