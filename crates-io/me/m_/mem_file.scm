(define-module (crates-io me m_ mem_file) #:use-module (crates-io))

(define-public crate-mem_file-0.1.0 (c (n "mem_file") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.10") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "handleapi" "fileapi" "memoryapi" "errhandlingapi" "synchapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11naqc0psgh0pg26893s1yfnpvql63vfbdwx98x1a1jjz6ql5csi") (y #t)))

