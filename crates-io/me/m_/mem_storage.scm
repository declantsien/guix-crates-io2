(define-module (crates-io me m_ mem_storage) #:use-module (crates-io))

(define-public crate-mem_storage-0.1.0 (c (n "mem_storage") (v "0.1.0") (h "1jj8afq4j98fzm5ajf03j4cpv8z9nrprs3gjzp4hyx00h15g9bl9")))

(define-public crate-mem_storage-0.1.1 (c (n "mem_storage") (v "0.1.1") (h "1kiw25p3ydaw7wzd0nm0pkrm7j6hi5s04nvjg2jc8vic5pf28ili")))

