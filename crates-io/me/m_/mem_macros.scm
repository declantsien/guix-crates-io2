(define-module (crates-io me m_ mem_macros) #:use-module (crates-io))

(define-public crate-mem_macros-0.1.0 (c (n "mem_macros") (v "0.1.0") (h "0cipinkyccbbih56ccz0zxy7f2n2m9qmdy022qkhvxljlaza47wf")))

(define-public crate-mem_macros-0.1.1 (c (n "mem_macros") (v "0.1.1") (h "14a70r9lska4l5xpnlh3jkb8z0wcgv33qg3hrfsa2d6pzndm8vr5")))

(define-public crate-mem_macros-0.1.2 (c (n "mem_macros") (v "0.1.2") (h "0wfn3wzdn6i42mnhjcf1305xpd5qj4m7mwkjbm6mligxvh3c3830")))

(define-public crate-mem_macros-1.0.0 (c (n "mem_macros") (v "1.0.0") (h "1x0zinj3i8m7c8nrkicwc643dmgr7aqyg2r125x4jpqq2q4wgc1i")))

(define-public crate-mem_macros-1.0.1 (c (n "mem_macros") (v "1.0.1") (h "05x059m29hb20j9rh8ziy7pqvlw8dkz8123i8jx5x7s81baghcv5")))

