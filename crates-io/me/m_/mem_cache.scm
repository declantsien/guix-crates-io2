(define-module (crates-io me m_ mem_cache) #:use-module (crates-io))

(define-public crate-mem_cache-0.1.0 (c (n "mem_cache") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1k8vk419hwkayf3gwi179ik8sdbajyhlyk9iz0p5qiba1w38kx6v") (y #t)))

(define-public crate-mem_cache-0.1.1 (c (n "mem_cache") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0a9g4qz4vi0n03c6ynwzdrg66prg220lc6py1s6z7d25iq9r752k") (y #t)))

(define-public crate-mem_cache-0.1.2 (c (n "mem_cache") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1gi8vfmyh2xs6cc6icl475cmkhd21zr1wkha7q0rl93zamd3if1v")))

