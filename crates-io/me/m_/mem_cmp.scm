(define-module (crates-io me m_ mem_cmp) #:use-module (crates-io))

(define-public crate-mem_cmp-0.1.0 (c (n "mem_cmp") (v "0.1.0") (h "10p9xv6z0w3ldj4zj8kac92rywimrsdhazxspkgpxn58aan1ilnk") (f (quote (("specialization") ("nightly" "specialization"))))))

(define-public crate-mem_cmp-0.1.1 (c (n "mem_cmp") (v "0.1.1") (h "165gka00awxy3fwkmf0n5mgq7kdnfz6rny9ywna5p412flfik1zy") (f (quote (("specialization") ("nightly" "specialization"))))))

(define-public crate-mem_cmp-0.1.2 (c (n "mem_cmp") (v "0.1.2") (h "0424wfr9wjfgw3miv44ang8jc3nbyyhi830hm2mm43ibvbyhgd5f") (f (quote (("specialization") ("nightly" "specialization"))))))

(define-public crate-mem_cmp-0.1.3 (c (n "mem_cmp") (v "0.1.3") (d (list (d (n "simd") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0mb5pm7mx5qlr4mmj7bavyd250rnqlnbdmw2z21xap7csys04gys") (f (quote (("specialization") ("nightly" "specialization") ("avx" "simd"))))))

(define-public crate-mem_cmp-0.1.4 (c (n "mem_cmp") (v "0.1.4") (d (list (d (n "simd") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0rqdyzr46ryksly1lb8c3z9m7fdy4hva3p506mdji699w0rf8afg") (f (quote (("specialization") ("nightly" "specialization") ("avx" "simd"))))))

