(define-module (crates-io me m_ mem_btree) #:use-module (crates-io))

(define-public crate-mem_btree-0.1.0 (c (n "mem_btree") (v "0.1.0") (d (list (d (n "memory-stats") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "00gx9iqk2f3hb9n7rada3nxcyigpj926pfvz4pbfdvkq5c5x4slm")))

(define-public crate-mem_btree-0.2.0 (c (n "mem_btree") (v "0.2.0") (d (list (d (n "memory-stats") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1iwzkcnvjcav80bwax0h9nnaqbrzriz7lbvbrg31a68gimjf2zhh")))

(define-public crate-mem_btree-0.2.1 (c (n "mem_btree") (v "0.2.1") (d (list (d (n "memory-stats") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0d2mggr5i644ph3i3nf2m5c981ki38sgpzd55gppwq06hrgx7yw2")))

(define-public crate-mem_btree-0.2.2 (c (n "mem_btree") (v "0.2.2") (d (list (d (n "memory-stats") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "01cyi8947cvsgk03k8n5r0yvdigsa91svzz4q9pzk79hxq1j5nxk")))

(define-public crate-mem_btree-0.3.0 (c (n "mem_btree") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0s0viy5dm7gz6r8g34fj56z6ipb3sbl9lcl0h5xkzrfhsqlggxia")))

(define-public crate-mem_btree-0.3.1 (c (n "mem_btree") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "08ypswyv237a5b0isl98y4n04ws3fyi16j8v02g25s8wfcvrsxvq")))

