(define-module (crates-io me ld meld-config-manager) #:use-module (crates-io))

(define-public crate-meld-config-manager-0.1.0 (c (n "meld-config-manager") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0i56p5izir3ri0320wvfz95x3pir75vd91m9lls8rx43136qc2ys")))

