(define-module (crates-io me rl merlin-std-derive) #:use-module (crates-io))

(define-public crate-merlin-std-derive-0.16.1 (c (n "merlin-std-derive") (v "0.16.1") (d (list (d (n "cosmwasm-std") (r "^1.1.2") (f (quote ("stargate"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 2)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ghnkhn1jimhs9vspvsaw6yd4bnih86936ybkr91zrqxphpxjaww") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

