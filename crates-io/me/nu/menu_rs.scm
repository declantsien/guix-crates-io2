(define-module (crates-io me nu menu_rs) #:use-module (crates-io))

(define-public crate-menu_rs-0.1.0 (c (n "menu_rs") (v "0.1.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)))) (h "1q987z26nkb30kkks677anldglci7gm0ig1qnvm8xk70h56l2wi2") (y #t)))

(define-public crate-menu_rs-0.2.0 (c (n "menu_rs") (v "0.2.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)))) (h "0f6xhr9dnqmlrph15d82glnihw4h7gfk3rhs3hq9swq6c38hvn35") (y #t)))

(define-public crate-menu_rs-0.2.1 (c (n "menu_rs") (v "0.2.1") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)))) (h "12lzkd78clz19j1x3dy5i9i5lpwphhzjhnwlshr27558ydqdzv1v") (y #t)))

(define-public crate-menu_rs-0.2.2 (c (n "menu_rs") (v "0.2.2") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)))) (h "03ws3m809przrmnz901jyp4nnzd8fmiah2ilrh8adqbhz9v4ih5l")))

(define-public crate-menu_rs-0.3.0 (c (n "menu_rs") (v "0.3.0") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)))) (h "0a8fgnkd9fdifckmn3z0dvvrcpdg87g1h3v3lsi044v1jfy30any")))

(define-public crate-menu_rs-0.3.1 (c (n "menu_rs") (v "0.3.1") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)))) (h "1qfgny5j5k1mllcjh37qfhn2csz200hg6qd43xannxms2i1l9snn")))

