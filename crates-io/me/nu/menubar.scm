(define-module (crates-io me nu menubar) #:use-module (crates-io))

(define-public crate-menubar-0.0.1 (c (n "menubar") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "libloaderapi" "wincon"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 2)))) (h "1vh131rl273zrlbb5vc439a3dhwfb88l5s4hqyqk3crhmyzhdzdj")))

(define-public crate-menubar-0.0.2 (c (n "menubar") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "objc") (r "=0.3.0-alpha.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0) (p "objc2")) (d (n "objc_foundation") (r "=0.2.0-alpha.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0) (p "objc2-foundation")) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "libloaderapi" "wincon"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 2)))) (h "0rhr9p1m51l27hpmqvnxyarnqv1ivlr9fvigxw8lzrwjv0ws6cgf")))

