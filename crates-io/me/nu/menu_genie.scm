(define-module (crates-io me nu menu_genie) #:use-module (crates-io))

(define-public crate-menu_genie-0.1.0 (c (n "menu_genie") (v "0.1.0") (h "0b93ajypq3966sqlwjfpzsmmjvz8cwgxzwdz2ck057xm1h3pxdbb")))

(define-public crate-menu_genie-0.1.1 (c (n "menu_genie") (v "0.1.1") (h "0dppif1lvmgmnxycrcrfrnr36ykjnjhc9ym653gha2p3z79mf1br")))

