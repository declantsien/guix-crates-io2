(define-module (crates-io me nu menu) #:use-module (crates-io))

(define-public crate-menu-0.1.0 (c (n "menu") (v "0.1.0") (h "01xl0g5c6bz8pwqa4hs3g88gz96v96cg0z5jwqwc1945nxqzx9bq")))

(define-public crate-menu-0.1.1 (c (n "menu") (v "0.1.1") (h "1my04ckpxd6rpip52ni6m54xnvwv8yalzpwq881r1y6r18j4nnz7")))

(define-public crate-menu-0.2.1 (c (n "menu") (v "0.2.1") (h "1f4x6848rgpbak1g8zwb5mvzbjfzxs40plq04y9swx5406x9904f")))

(define-public crate-menu-0.2.0 (c (n "menu") (v "0.2.0") (d (list (d (n "pancurses") (r "^0.16") (d #t) (k 2)))) (h "1bbnnn6v6sifhq4ig904n358if19a447a7696gcisx54r9qsqilc") (y #t)))

(define-public crate-menu-0.3.0 (c (n "menu") (v "0.3.0") (d (list (d (n "pancurses") (r "^0.16") (d #t) (k 2)))) (h "00ib9wpj9v2ywh0cngrv14aav64q73l2pmfnj6vi9fvbnmyyg1zh")))

(define-public crate-menu-0.3.1 (c (n "menu") (v "0.3.1") (d (list (d (n "pancurses") (r "^0.16") (d #t) (k 2)))) (h "0z59vb66kiarihzhhzw8x7psim0nnf1a7cmpgj8slziarqzdd387")))

(define-public crate-menu-0.3.2 (c (n "menu") (v "0.3.2") (d (list (d (n "pancurses") (r "^0.16") (d #t) (k 2)))) (h "1mazqy65xlida0871n1pdf497v1f2i8pk4yzssd355zyidwpygdh")))

(define-public crate-menu-0.4.0 (c (n "menu") (v "0.4.0") (d (list (d (n "pancurses") (r "^0.16") (d #t) (k 2)))) (h "0gdms9kbdhrp8r6flnvpj2f7mp59n6h0p5hdyih7fb04c3b8n86m") (f (quote (("echo") ("default" "echo"))))))

(define-public crate-menu-0.5.0 (c (n "menu") (v "0.5.0") (d (list (d (n "pancurses") (r "^0.16") (d #t) (k 2)))) (h "0ban35a1y325x7afmdsbh2vbr6pfxf49078qvmspgfdkk441366f") (f (quote (("echo") ("default" "echo"))))))

