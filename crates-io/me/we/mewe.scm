(define-module (crates-io me we mewe) #:use-module (crates-io))

(define-public crate-mewe-0.1.0 (c (n "mewe") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "scraper") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1cd35ar3v2frgspq70dw17vh0mldddqgn0qczyj5l8q83xabadiy")))

