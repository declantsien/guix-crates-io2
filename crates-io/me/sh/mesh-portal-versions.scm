(define-module (crates-io me sh mesh-portal-versions) #:use-module (crates-io))

(define-public crate-mesh-portal-versions-0.2.0-rc1 (c (n "mesh-portal-versions") (v "0.2.0-rc1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.1") (d #t) (k 0)))) (h "0fi9clmxidzf626dv91iqrgbfl3sn8mfwcvpq4gkg4abmrldg75y")))

