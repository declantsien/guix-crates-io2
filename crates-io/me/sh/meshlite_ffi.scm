(define-module (crates-io me sh meshlite_ffi) #:use-module (crates-io))

(define-public crate-meshlite_ffi-0.1.0 (c (n "meshlite_ffi") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "meshlite") (r "^0.2") (d #t) (k 0)))) (h "0ls5gjlpk9iimfsr2awp127fag7ywk99gz3hd7swn4a3ncgcvzv6")))

(define-public crate-meshlite_ffi-0.1.1 (c (n "meshlite_ffi") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "meshlite") (r "^0.2") (d #t) (k 0)))) (h "0iz44l2xfkd7a9dz4rbs72pk4fdm1fj52rmwggclha42zfmbaq3b")))

