(define-module (crates-io me sh meshvox) #:use-module (crates-io))

(define-public crate-meshvox-0.1.0 (c (n "meshvox") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0z4psw9q1ryxsh6q4l478z666c8sf445xdg3jfwnfw9kd7611js9")))

(define-public crate-meshvox-0.2.0 (c (n "meshvox") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1nqlji0fysz3phb7ldy1jbr9hyr483a83b04aabbn34f7hk5qb7c")))

(define-public crate-meshvox-0.2.1 (c (n "meshvox") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "11ksrm0jcjr93zqvrpk7jm51nywspniblaa1w4q2dlf1581hzgpy")))

(define-public crate-meshvox-0.2.2 (c (n "meshvox") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1xnc6ip1dd6xyf3xxlmik5y49grdlh77ckdjb2y3kkg86wb3xky8") (y #t)))

(define-public crate-meshvox-0.2.3 (c (n "meshvox") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "03bs38fb9j7r7vzb47gf9rk9i8wyvp2fn56rk8pb222i944mx3wi")))

