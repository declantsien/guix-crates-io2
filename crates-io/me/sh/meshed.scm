(define-module (crates-io me sh meshed) #:use-module (crates-io))

(define-public crate-meshed-0.1.0 (c (n "meshed") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1rmxwx49bihikg89zhqlvvfmfz86w8v4574921f5j9wxrgfri1xv")))

(define-public crate-meshed-0.1.1 (c (n "meshed") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0yn7yd0l2ic509axvpwl1fv530h1zy8l3js2ig5mpgz0nylsnc34")))

(define-public crate-meshed-0.1.2 (c (n "meshed") (v "0.1.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "08fm7p9c6alr1i7jfgwcgz73pqk8hgn80s12ln37p1hq9wnrpscm")))

(define-public crate-meshed-0.2.0 (c (n "meshed") (v "0.2.0") (d (list (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1kgfmmrbnafwwq8yqcs0q9f191vwmbhcjmk3x72abhhpg8p89q1d")))

(define-public crate-meshed-0.2.1 (c (n "meshed") (v "0.2.1") (d (list (d (n "dot") (r "^0.1.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0vhbbnfvfywf0b5ss10gyjx8f8rwy9q9z3cl080yj3mg76dc9ngc")))

