(define-module (crates-io me sh mesh-generation) #:use-module (crates-io))

(define-public crate-mesh-generation-0.1.0 (c (n "mesh-generation") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "noise") (r "^0.6.0") (d #t) (k 0)))) (h "0wdv9lwlz3gd855rlpxvw9b5y5s52vk45417v9281c3cgsbn3vzm")))

(define-public crate-mesh-generation-0.1.1 (c (n "mesh-generation") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "noise") (r "^0.6.0") (d #t) (k 0)))) (h "0adyr3jd9m2lrkzjdnhviwjh5yp7rwc4snq0pxjb7lw5mfyk6v9p")))

(define-public crate-mesh-generation-0.1.2 (c (n "mesh-generation") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "noise") (r "^0.6.0") (d #t) (k 0)))) (h "1751b65w6x9gfj6dhg474i8isccphxkp549fiyrar4f41srvmmki")))

(define-public crate-mesh-generation-0.1.3 (c (n "mesh-generation") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "noise") (r "^0.6.0") (d #t) (k 0)))) (h "14scm845srmzlhf9mzcr0xqhx3lqp66w3g58a2nfhbv7c95kdsdj")))

