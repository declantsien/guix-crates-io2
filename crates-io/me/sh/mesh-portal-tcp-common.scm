(define-module (crates-io me sh mesh-portal-tcp-common) #:use-module (crates-io))

(define-public crate-mesh-portal-tcp-common-0.2.0-rc1 (c (n "mesh-portal-tcp-common") (v "0.2.0-rc1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "mesh-portal") (r "^0.2.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y98ackcalmw6c5fr0gi3vsp09dg8hckkvd2myg4qw1zhlhmkwrz")))

