(define-module (crates-io me sh meshx-derive) #:use-module (crates-io))

(define-public crate-meshx-derive-0.1.0 (c (n "meshx-derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1375zxn6v0rpanh1b0j9lh9i5aki7pzkxi4sbknacf7nhkaygfa2")))

