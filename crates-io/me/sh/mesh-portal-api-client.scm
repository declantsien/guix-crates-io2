(define-module (crates-io me sh mesh-portal-api-client) #:use-module (crates-io))

(define-public crate-mesh-portal-api-client-0.2.0-rc1 (c (n "mesh-portal-api-client") (v "0.2.0-rc1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "dashmap") (r "^4.0.2") (d #t) (k 0)) (d (n "mesh-portal") (r "^0.2.0-rc1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4" "wasm-bindgen"))) (d #t) (k 0)))) (h "14ad7bqahf6shpih6xaz316s8dlsa9iws265573qjccy2r1zb3y1")))

