(define-module (crates-io me sh mesh-portal) #:use-module (crates-io))

(define-public crate-mesh-portal-0.2.0-rc1 (c (n "mesh-portal") (v "0.2.0-rc1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mesh-portal-versions") (r "^0.2.0-rc1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6.0") (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.1") (d #t) (k 0)))) (h "025bp9j6dadlpblf802h9rsk685pk3r6znzyfb4pkk9syzbk817b")))

