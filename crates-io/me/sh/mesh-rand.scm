(define-module (crates-io me sh mesh-rand) #:use-module (crates-io))

(define-public crate-mesh-rand-0.1.0 (c (n "mesh-rand") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0gjb1pbf5h7jx2s74mm5780wvcm82j6gdmkwgkdb24shldbjnld0")))

