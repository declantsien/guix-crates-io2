(define-module (crates-io me sh meshpulse_derive) #:use-module (crates-io))

(define-public crate-meshpulse_derive-0.1.0 (c (n "meshpulse_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xf590pqj8k6az7n1k0g8nh7vksf7dmayijzlnrwjrbjshp1vp3y") (f (quote (("mqtt") ("grpc") ("default" "mqtt") ("amqp"))))))

(define-public crate-meshpulse_derive-0.1.1 (c (n "meshpulse_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "10gh5wbylma499cxa54h4058458f9y2yz9dsvilhv6n7a2d0pvcd") (f (quote (("mqtt") ("grpc") ("default" "mqtt") ("amqp"))))))

(define-public crate-meshpulse_derive-0.1.2 (c (n "meshpulse_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0d3kw2g4sxkl801svabk9fla3c6f0w3q7s43zrvq9gzn7bnxndah") (f (quote (("mqtt") ("grpc") ("default" "mqtt") ("amqp"))))))

(define-public crate-meshpulse_derive-0.1.3 (c (n "meshpulse_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08pnmvfzbwfwbni631kr656x9vd8227ljjr5dqv3m6sx3j1d0y82") (f (quote (("mqtt") ("grpc") ("default" "mqtt") ("amqp"))))))

(define-public crate-meshpulse_derive-0.2.0 (c (n "meshpulse_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08fn36pvszzfals7x8r1mbbq5hy9lnjfc8rvcpl9zlwmxg2wbi0q") (f (quote (("mqtt") ("grpc") ("default" "mqtt") ("amqp"))))))

