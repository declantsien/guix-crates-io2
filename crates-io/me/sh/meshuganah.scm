(define-module (crates-io me sh meshuganah) #:use-module (crates-io))

(define-public crate-meshuganah-0.1.0 (c (n "meshuganah") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zswp3rl5b6bpx13f91kycv1ivzpd1fj247f72wfcs2j7gdvqyv9") (y #t)))

(define-public crate-meshuganah-0.1.1 (c (n "meshuganah") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nfik2g0184kgcmlq10pkaq4i9na6711z5amqr17vbxbw0g2v3bz") (y #t)))

(define-public crate-meshuganah-0.1.2 (c (n "meshuganah") (v "0.1.2") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fkcmf70ga08s1rzd6q1n2hcy3z1ay1sk7ljldy9y9d45gx2jdml") (y #t)))

(define-public crate-meshuganah-0.1.3 (c (n "meshuganah") (v "0.1.3") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b453a13h8vdbfdx2dgjdl9rnzfna0ynk2hvgmx8zwgghr6dcb7x") (y #t)))

(define-public crate-meshuganah-0.1.4 (c (n "meshuganah") (v "0.1.4") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14c61cai1m78p38bc54532w2968dmn3gqi3s3v4lpyajw3pkvh9v") (y #t)))

(define-public crate-meshuganah-0.1.5 (c (n "meshuganah") (v "0.1.5") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fyhrmn3lmws66p2gmww8c68zpv163pp31p4s7g5ixr70kkdzira") (y #t)))

(define-public crate-meshuganah-0.2.0 (c (n "meshuganah") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d1v6p05rmqszc115bdw11jlr50irp5r9mgchxv5michzprhrfag")))

(define-public crate-meshuganah-0.2.1 (c (n "meshuganah") (v "0.2.1") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "132qjcl6zjiw8p53f8z826f3afvy0qq3p27ly8cpr95x8lazv3wy")))

(define-public crate-meshuganah-0.2.2 (c (n "meshuganah") (v "0.2.2") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "grcov") (r "^0.5.15") (d #t) (k 2)) (d (n "mongodb") (r "^1.1.1") (f (quote ("async-std-runtime"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04yym7yvijmgj3ls9fdbhkz4fmqa40r01pf82bag6fkx9sbv88mn")))

