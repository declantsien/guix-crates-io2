(define-module (crates-io me sh meshgridrs) #:use-module (crates-io))

(define-public crate-meshgridrs-0.1.0 (c (n "meshgridrs") (v "0.1.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0qrw93nfxfg9mifadvlg31m2ims5gd446i8nsc6srrvzz7iai87h")))

(define-public crate-meshgridrs-0.1.1 (c (n "meshgridrs") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0pi8b7n2xnrnw4h7njhaiajkz9alwcmk6xx43yiaxh2iqpnkzk0p")))

