(define-module (crates-io me sh mesh-portal-tcp-client) #:use-module (crates-io))

(define-public crate-mesh-portal-tcp-client-0.2.0-rc1 (c (n "mesh-portal-tcp-client") (v "0.2.0-rc1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "dashmap") (r "^4.0.2") (d #t) (k 0)) (d (n "mesh-portal") (r "^0.2.0-rc1") (d #t) (k 0)) (d (n "mesh-portal-api-client") (r "^0.2.0-rc1") (d #t) (k 0)) (d (n "mesh-portal-tcp-common") (r "^0.2.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yi152gfq1vm96i3q2angy8yvwlfjsripjivb5l9lx9hr2hx1xl8")))

