(define-module (crates-io me sh mesh_to_vox) #:use-module (crates-io))

(define-public crate-mesh_to_vox-0.1.0 (c (n "mesh_to_vox") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gltf") (r "^1.4.0") (f (quote ("import" "utils" "KHR_materials_pbrSpecularGlossiness"))) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (k 0)) (d (n "nalgebra") (r "^0.32.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "vox_writer") (r "^0.1.9") (d #t) (k 0)))) (h "022wip1xsgy2z0xz74djdambcva98czwisd15xpaiwzf81m0b919")))

(define-public crate-mesh_to_vox-0.1.1 (c (n "mesh_to_vox") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gltf") (r "^1.4.0") (f (quote ("import" "utils" "KHR_materials_pbrSpecularGlossiness"))) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (k 0)) (d (n "nalgebra") (r "^0.32.0") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "vox_writer") (r "^0.1.9") (d #t) (k 0)))) (h "0pkf6sbx219j8f6ba98cil752ndgrkwwqcmi6bql2qpgjxhpbjb3")))

