(define-module (crates-io me sh mesh-sweeper) #:use-module (crates-io))

(define-public crate-mesh-sweeper-0.1.0 (c (n "mesh-sweeper") (v "0.1.0") (d (list (d (n "plotpy") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "voronator") (r "^0.2.1") (d #t) (k 0)))) (h "1ahv949cf9sjz1522wh1jgmlhcc0mz2w4r7ksjw5vb4mj76rl5hn")))

(define-public crate-mesh-sweeper-0.1.1 (c (n "mesh-sweeper") (v "0.1.1") (d (list (d (n "plotpy") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "voronator") (r "^0.2.1") (d #t) (k 0)))) (h "0rj0lqj1b1k1j8gz170zx988gmqvmsdq2z5jhsa5zbs5mjhb07ic")))

