(define-module (crates-io me sh meshlite) #:use-module (crates-io))

(define-public crate-meshlite-0.2.0 (c (n "meshlite") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0cq7isxjh560il2pma14xv81d1cn72dwjbr4yh41kh91v1nvwp8s")))

