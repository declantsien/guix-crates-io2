(define-module (crates-io me sh meshopt-rs) #:use-module (crates-io))

(define-public crate-meshopt-rs-0.1.0 (c (n "meshopt-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.6.2") (d #t) (k 2)) (d (n "tobj") (r "^3.2.3") (d #t) (k 2)))) (h "19mkmlhkfbfavndybvaqqhnpsbl5b16z8y7gh861wq2qyd4flql9") (f (quote (("experimental"))))))

(define-public crate-meshopt-rs-0.1.1 (c (n "meshopt-rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.6.2") (d #t) (k 2)) (d (n "tobj") (r "^3.2.3") (d #t) (k 2)))) (h "1pvq9qq217rxcw9ls47ranqg6ncsds5zbi9i6yzmfq3cph319zy8") (f (quote (("experimental"))))))

(define-public crate-meshopt-rs-0.1.2 (c (n "meshopt-rs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.6.2") (d #t) (k 2)) (d (n "tobj") (r "^3.2.3") (d #t) (k 2)))) (h "019gr7clz85nw2zykd1572d315qr332jr0lagn7402zzfz7f1qf3") (f (quote (("experimental"))))))

