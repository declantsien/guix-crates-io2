(define-module (crates-io me sh meshellaneous) #:use-module (crates-io))

(define-public crate-meshellaneous-0.1.0 (c (n "meshellaneous") (v "0.1.0") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "0rqq74qcxv2plmgp5d3riydc8hhn3a5iw0a1577zakg7da1lar1j") (y #t)))

(define-public crate-meshellaneous-0.0.1 (c (n "meshellaneous") (v "0.0.1") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "0vv0r703kj6ygqb36j4p7a5cwscqq1mvapkvgvb77zbpnr96rgcy")))

(define-public crate-meshellaneous-0.0.2 (c (n "meshellaneous") (v "0.0.2") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "1kq08x4af37gf9lgmvc12ym4gpirk8gbjpzx1i89ha5nanbbnvkv")))

(define-public crate-meshellaneous-0.0.3 (c (n "meshellaneous") (v "0.0.3") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "0gsvlfq6gapf7xqpsrk5nms166nx84hmylkypfl68njwv4cc8ipi")))

(define-public crate-meshellaneous-0.0.4 (c (n "meshellaneous") (v "0.0.4") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "1yczil53d6zlp85d1kgcxkfg9kajabdj2qdsixfzwakcm7mq0i58")))

(define-public crate-meshellaneous-0.0.5 (c (n "meshellaneous") (v "0.0.5") (d (list (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "0gzxf73jz833f5yycgqxz1f0mj4brgs1nqmz10sdlm0mahfm04bj")))

