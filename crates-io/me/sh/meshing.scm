(define-module (crates-io me sh meshing) #:use-module (crates-io))

(define-public crate-meshing-0.0.1 (c (n "meshing") (v "0.0.1") (d (list (d (n "plotters") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10vvqr0afjfx18blvm3z6zgb44rwkh8v7jspffffwb6lix95l1q4")))

