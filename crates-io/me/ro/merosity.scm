(define-module (crates-io me ro merosity) #:use-module (crates-io))

(define-public crate-merosity-0.1.0 (c (n "merosity") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("android_shared_stdcxx" "bevy_asset" "bevy_audio" "bevy_core_pipeline" "bevy_gilrs" "bevy_gizmos" "bevy_render" "bevy_sprite" "bevy_text" "bevy_winit" "default_font" "hdr" "multi-threaded" "png" "tonemapping_luts" "vorbis" "webgl2" "x11" "zstd"))) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.11") (d #t) (k 0)))) (h "04xcb4hh0j73hpsrr8ib6hs2vfbzafrd7j07nj9lgznpc1mihg2m") (f (quote (("dev" "bevy/dynamic_linking" "bevy/file_watcher"))))))

