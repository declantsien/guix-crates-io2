(define-module (crates-io me te meter) #:use-module (crates-io))

(define-public crate-meter-0.1.0 (c (n "meter") (v "0.1.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "keyed_priority_queue") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "1wdipvslq0sgarvqdz2nnc2rybqa69adxmkldkbzw5nd51k8d8i3")))

(define-public crate-meter-0.1.1 (c (n "meter") (v "0.1.1") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "keyed_priority_queue") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "0g2zvfpzyxzbsbi1lr8qqlrssim9f0cm0268yzfkfq3lw35kb3cf")))

(define-public crate-meter-0.1.2 (c (n "meter") (v "0.1.2") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "keyed_priority_queue") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "1117hf5wl40p2r8spnmhak1592l3fp9dfcpkpzgf7cng3njvw1jp")))

(define-public crate-meter-0.1.3 (c (n "meter") (v "0.1.3") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "keyed_priority_queue") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "1qlvdbg709bz77nplpahg1l58v05kw2sq1skx3wav8shprhjwa12")))

(define-public crate-meter-0.1.4 (c (n "meter") (v "0.1.4") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "keyed_priority_queue") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "0221a6q7jk9bqp9x1dfijgqccyqzm221an7wii95s9hzhvk0skhi")))

(define-public crate-meter-0.1.5 (c (n "meter") (v "0.1.5") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "cpal") (r "^0.13.5") (d #t) (k 0)) (d (n "keyed_priority_queue") (r "^0.4.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (d #t) (k 0)))) (h "0mfysn6fibra005gnhpyfdqg7sx0cxbgvm05n6ksg4adzzrlswwk")))

