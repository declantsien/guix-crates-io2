(define-module (crates-io me te meteoritus) #:use-module (crates-io))

(define-public crate-meteoritus-0.1.0 (c (n "meteoritus") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1a1mfibavyciq4p00ip0q4bbbs0p8qr0mk33gygrm2fn50rxgqp0") (y #t)))

(define-public crate-meteoritus-0.1.1 (c (n "meteoritus") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1dg3p5l1lcwaql1xccj6gr30hw6dfjs8dbpiwv6fz34lqpas3aj8")))

(define-public crate-meteoritus-0.2.0 (c (n "meteoritus") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1y6a8mjpz2ibg38mn5q974s2j98xr8aw76igvh26f06nklrxpqs0")))

