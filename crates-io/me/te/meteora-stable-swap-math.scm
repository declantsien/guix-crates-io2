(define-module (crates-io me te meteora-stable-swap-math) #:use-module (crates-io))

(define-public crate-meteora-stable-swap-math-1.8.1 (c (n "meteora-stable-swap-math") (v "1.8.1") (d (list (d (n "borsh") (r "^0.9.2") (d #t) (k 0)) (d (n "meteora-stable-swap-client") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "stable-swap-sim") (r "^0.1") (d #t) (k 2)) (d (n "uint") (r "=0.9.1") (k 0)))) (h "18h7fgpxv4szsgwljn9qbawz102kh8lc5xgvdcffnlq3k1ian72d")))

