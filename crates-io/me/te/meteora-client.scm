(define-module (crates-io me te meteora-client) #:use-module (crates-io))

(define-public crate-meteora-client-0.1.0 (c (n "meteora-client") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.7") (f (quote ("secure"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "meteora-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "meteora-server") (r "^0.1.0") (d #t) (k 0)) (d (n "raft") (r "^0.4.3") (d #t) (k 0)))) (h "1pma0s9k51ijnk8bxarwswqv7h1g6yr906afvlvrwpnwh3hrfiyp")))

