(define-module (crates-io me te meteofrance) #:use-module (crates-io))

(define-public crate-meteofrance-0.1.0 (c (n "meteofrance") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zy8jfn6cq05qnnna8a38kjjig3dp4415yrzhmbnlrcyba5vhx9m")))

(define-public crate-meteofrance-0.2.0 (c (n "meteofrance") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1njippm7nrn5mb4b0v9hphirfr783r7996pg3j6jmgl75cqd6xkb")))

