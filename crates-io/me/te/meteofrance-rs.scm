(define-module (crates-io me te meteofrance-rs) #:use-module (crates-io))

(define-public crate-meteofrance-rs-0.2.1 (c (n "meteofrance-rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (f (quote ("tls" "json"))) (o #t) (d #t) (k 0)))) (h "0v2wrc58i2nb0dz2g7q4ssxbd0yp7hqvajpggf9j4mzpb85awdj0") (s 2) (e (quote (("ureq" "dep:ureq"))))))

(define-public crate-meteofrance-rs-0.2.2 (c (n "meteofrance-rs") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (f (quote ("tls" "json"))) (o #t) (d #t) (k 0)))) (h "1xggawymdd5zcdn3209r6cy8dvab2mrlmn210kc3vvymmrf01aka") (s 2) (e (quote (("ureq" "dep:ureq"))))))

(define-public crate-meteofrance-rs-0.2.3 (c (n "meteofrance-rs") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (f (quote ("tls" "json"))) (o #t) (d #t) (k 0)))) (h "0idwlvdb5na09gxn81gspjl9v68bl32wrrph78j6a341sxg5ws6n") (s 2) (e (quote (("ureq" "dep:ureq"))))))

