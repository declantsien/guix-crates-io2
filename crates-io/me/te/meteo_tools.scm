(define-module (crates-io me te meteo_tools) #:use-module (crates-io))

(define-public crate-meteo_tools-0.1.0 (c (n "meteo_tools") (v "0.1.0") (h "0551663lb10yvazqa091y8gwn9xajzsig8f1wxsgsgwcqajfi9fw") (y #t) (r "1.75")))

(define-public crate-meteo_tools-1.0.0 (c (n "meteo_tools") (v "1.0.0") (h "0ab3d6pyc9zxh2agsy8f5r4v1by37ng72z5y056mkv470y9m5v5k") (y #t) (r "1.75")))

(define-public crate-meteo_tools-1.1.0 (c (n "meteo_tools") (v "1.1.0") (h "114y0hbkbvil7bna4fdjz06gdchz62h2jmsv866y2spi6r20kppa") (r "1.75")))

