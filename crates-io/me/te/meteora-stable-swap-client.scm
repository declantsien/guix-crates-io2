(define-module (crates-io me te meteora-stable-swap-client) #:use-module (crates-io))

(define-public crate-meteora-stable-swap-client-1.8.1 (c (n "meteora-stable-swap-client") (v "1.8.1") (d (list (d (n "arbitrary") (r "^1.0.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z6sv8syvpppdf24qx3ql8xjpyaxiqrmz1kg6pc81y64wfcp5n5j") (f (quote (("fuzz" "arbitrary"))))))

