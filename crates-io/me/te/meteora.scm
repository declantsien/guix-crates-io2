(define-module (crates-io me te meteora) #:use-module (crates-io))

(define-public crate-meteora-0.1.0 (c (n "meteora") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.7") (f (quote ("secure"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "meteora-client") (r "^0.1.0") (d #t) (k 0)) (d (n "meteora-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "meteora-server") (r "^0.1.0") (d #t) (k 0)) (d (n "raft") (r "^0.4.3") (d #t) (k 0)))) (h "11n0vbhcww7jkfqfpq2pqxfvaimwfb9j7ipfi3zzyrfm8y201k7x")))

