(define-module (crates-io me te meterproxy) #:use-module (crates-io))

(define-public crate-meterproxy-0.1.0 (c (n "meterproxy") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1gs7nhpb2r0ddnp16j9qdyiy20pzzsfaihm4a5aqba4v1df34b04") (y #t)))

(define-public crate-meterproxy-0.1.1 (c (n "meterproxy") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "113arj14nrpqg0nlp8lzdkwx0jdm5iy8vmcdy93zzjsrl6z1rl1r") (y #t)))

