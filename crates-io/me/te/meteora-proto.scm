(define-module (crates-io me te meteora-proto) #:use-module (crates-io))

(define-public crate-meteora-proto-0.1.0 (c (n "meteora-proto") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "grpcio") (r "^0.4.7") (f (quote ("secure"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "raft") (r "^0.4.3") (d #t) (k 0)))) (h "03kf41wfwbdcb87n5znar9v88lxy4zfyfhmbq9iwv1k40q58i5rs")))

