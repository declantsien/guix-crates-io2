(define-module (crates-io me te meter_proxy) #:use-module (crates-io))

(define-public crate-meter_proxy-0.2.0 (c (n "meter_proxy") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "17bqz55zvrmigd94i9ap6bicjrk69spbd3ijvli6kl8nwppc6jws")))

(define-public crate-meter_proxy-0.2.1 (c (n "meter_proxy") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0i2shl198c8f5mc1lmnr0slnvhq7dp00hss9gc42q55a2vq6wghq")))

(define-public crate-meter_proxy-0.2.2 (c (n "meter_proxy") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1d4l8lazrngzafsvmc1ysx3pmpzdkdwfg2x07nr6chrm2z3s598z")))

(define-public crate-meter_proxy-0.2.3 (c (n "meter_proxy") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0vxdya66fqmjriy7grx3dn4vvq1vspb6hdvs12qfy1ki8asym7sn")))

