(define-module (crates-io me te meteora-marinade-sdk) #:use-module (crates-io))

(define-public crate-meteora-marinade-sdk-0.1.0 (c (n "meteora-marinade-sdk") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.17") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (d #t) (k 0)))) (h "107dcfnrsq0ilyinxc5qq5yb582lpl8mjhg6bnqqs0rjzgirvh2h") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-meteora-marinade-sdk-0.1.1 (c (n "meteora-marinade-sdk") (v "0.1.1") (d (list (d (n "borsh") (r ">=0.9, <0.11") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.13") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (d #t) (k 0)))) (h "08qadm6h90q1h2rlva03rih656km7naa090b6gzz9334a3xf0370") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

