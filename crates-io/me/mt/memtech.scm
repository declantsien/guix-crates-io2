(define-module (crates-io me mt memtech) #:use-module (crates-io))

(define-public crate-memtech-0.1.0 (c (n "memtech") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)))) (h "03y7wiqjpq3yrmibk8zynvqdwccvva015p5gvj5496mss9p3pf2f")))

