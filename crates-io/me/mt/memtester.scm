(define-module (crates-io me mt memtester) #:use-module (crates-io))

(define-public crate-memtester-1.0.0 (c (n "memtester") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmemtester") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "025cs04wlc041l4vxjjq8ipb7x26yn50dvcz3ldl2gd2mkpiwqxg")))

(define-public crate-memtester-1.0.1 (c (n "memtester") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmemtester") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sa3j7paq3fswhfgb5hhj56knk1cfrks2jr29dinlbcax0mcz5fq")))

(define-public crate-memtester-1.0.2 (c (n "memtester") (v "1.0.2") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmemtester") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1px88viribjgl645b8486r8v796yz4m73cxxp023qg6sgpaw941l")))

