(define-module (crates-io f- tr f-trak) #:use-module (crates-io))

(define-public crate-f-trak-0.1.0 (c (n "f-trak") (v "0.1.0") (d (list (d (n "opencv") (r "^0.46.3") (f (quote ("opencv-4" "buildtime-bindgen"))) (k 0)))) (h "1s8qn963i0l61cb0i0wbxsjk71kjaas68rfa9brd91g6a48jpb0s")))

(define-public crate-f-trak-0.2.0 (c (n "f-trak") (v "0.2.0") (d (list (d (n "opencv") (r "^0.83.0") (f (quote ("dnn" "highgui" "imgproc" "videoio"))) (k 0)))) (h "1j3m6zn4cdjs742942gsg2qwqgxavv4mvvv7qngq4xwx12hwzxrw")))

