(define-module (crates-io f- tr f-tree) #:use-module (crates-io))

(define-public crate-f-tree-0.1.0 (c (n "f-tree") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rzhpwpdl850yhf5525y8m2r7s7im4nl4mri7qxhf9mwaasbrayl")))

(define-public crate-f-tree-0.1.1 (c (n "f-tree") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qp4a82rqnnbcq68262zykpz7wfk4s7ik3v9sjhgjfi0a4z6xqp8")))

(define-public crate-f-tree-0.1.2 (c (n "f-tree") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06hc7iibjlsl17s9lhbi1i3szgrb7rkbvkk3igc2va8301n0zlmc")))

(define-public crate-f-tree-0.1.3 (c (n "f-tree") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1q125s2k6y80flbn4k37ywq97fx54i46g97gjhg1l9pa9nrndcr1")))

(define-public crate-f-tree-0.1.4 (c (n "f-tree") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1r2jdyl6ccifimpgsr4b0ql1931wb0xj619rqc8r45gwf65wkd5x")))

(define-public crate-f-tree-0.1.5 (c (n "f-tree") (v "0.1.5") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1z1mzfj5d45817bn3s8j69ahzj7jcgq789s20fx7kk1dnr1zkgg3")))

(define-public crate-f-tree-0.1.6 (c (n "f-tree") (v "0.1.6") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "07mk9jjqa6n95p80ywcrrg5kqbigvsvgi9psi1bllzqz3aj9zgd5")))

(define-public crate-f-tree-0.1.7 (c (n "f-tree") (v "0.1.7") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wdg2zrhw40zksw7q2aqm9qfa0xs9qf8zq2hjllimg76xqlc01r0")))

(define-public crate-f-tree-0.1.8 (c (n "f-tree") (v "0.1.8") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0spy0l89l9khli7rfry38isd00f83d2klicn744hpj15803f2nwj")))

(define-public crate-f-tree-0.1.9 (c (n "f-tree") (v "0.1.9") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0l4sswxk7zx2vb0kd3j19hhwxlw9ihq0v6gdjzbp6bk5s0s2vfwv")))

(define-public crate-f-tree-0.2.0 (c (n "f-tree") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.1.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1g99frvbk2h3y9z5pkj2cclwpas5q37jsb9qyyswp0rhyqi7d37z")))

(define-public crate-f-tree-0.2.1 (c (n "f-tree") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0aw8v0n48diq59j6dv16w2sh9qg2yph86n479w0qbqxbhfp2kp5f")))

(define-public crate-f-tree-0.2.2 (c (n "f-tree") (v "0.2.2") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0j6cbvpinbwbchvsx5mclpfrqzil0m6lwfdlsdrdssyyfg7nbczk")))

(define-public crate-f-tree-0.2.3 (c (n "f-tree") (v "0.2.3") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0p6bqjhajpv7sy0wpfnlnwn0qg8cdbnqk64g4vx76f92r2qgpmyz")))

(define-public crate-f-tree-0.2.4 (c (n "f-tree") (v "0.2.4") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1k528bwzxq8ydr0515zafxdkgbf2vzknrkq6pri0pkn7b36d4kck")))

(define-public crate-f-tree-0.2.5 (c (n "f-tree") (v "0.2.5") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "forester-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0cp7frl6crpvyacjyv93ssq3cf1wqglvcqgin1w0l9dzf2p8f5dy")))

