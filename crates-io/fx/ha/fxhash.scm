(define-module (crates-io fx ha fxhash) #:use-module (crates-io))

(define-public crate-fxhash-0.1.0 (c (n "fxhash") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 2)))) (h "04xxzwbgkyrj1ac2h78bdqcrdq5vjkh90nr4a38j3zkjzsyv67n2")))

(define-public crate-fxhash-0.1.1 (c (n "fxhash") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 2)))) (h "1xq7d7jgdr8qwkasgzvc7qhgx36v7fqvrpg6ykbjcam7wcjn2qxs")))

(define-public crate-fxhash-0.1.2 (c (n "fxhash") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 2)))) (h "0xga2acbjcfhnm53f47bdbn56c9i6yxlc0ljxr7b0p8b5l48bapv")))

(define-public crate-fxhash-0.2.0 (c (n "fxhash") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 2)))) (h "1yvwfagz2ssq9j5qj9mrls88nkzhs40abw5fa5zwy34rid87af43") (y #t)))

(define-public crate-fxhash-0.2.1 (c (n "fxhash") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "seahash") (r "^3.0.5") (d #t) (k 2)))) (h "037mb9ichariqi45xm6mz0b11pa92gj38ba0409z3iz239sns6y3")))

