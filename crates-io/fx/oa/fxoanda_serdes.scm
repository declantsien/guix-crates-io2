(define-module (crates-io fx oa fxoanda_serdes) #:use-module (crates-io))

(define-public crate-fxoanda_serdes-0.1.0 (c (n "fxoanda_serdes") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1954j28h19q71pl4g5s1hj88cm8yrqk20j42xi3y48v6kmr5f7cz")))

(define-public crate-fxoanda_serdes-0.1.1 (c (n "fxoanda_serdes") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (d #t) (k 0)))) (h "0qip43qvb5bhkr60xdw4mv0968fpxq9xx038502sli5nzfmn57s2")))

