(define-module (crates-io fx oa fxoanda_definitions) #:use-module (crates-io))

(define-public crate-fxoanda_definitions-0.1.0 (c (n "fxoanda_definitions") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "fxoanda_serdes") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "111xn5k0sg2p9bi153jbzxq52hlvzlj76qf5pq3dvj755z923wrz")))

(define-public crate-fxoanda_definitions-0.1.1 (c (n "fxoanda_definitions") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "fxoanda_serdes") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (d #t) (k 0)))) (h "1ra40zqwds682bpps7xrcdhl5y35f7f27s3yds93z46rhdyzfqh4")))

