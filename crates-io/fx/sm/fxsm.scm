(define-module (crates-io fx sm fxsm) #:use-module (crates-io))

(define-public crate-fxsm-0.1.0 (c (n "fxsm") (v "0.1.0") (h "0q5ybvdmz9wxmyd7v0gk254nfbngwywa75ybm4wrr0glhgnl868y")))

(define-public crate-fxsm-0.2.0 (c (n "fxsm") (v "0.2.0") (h "0gfwxw0iq1c8qxrm1civh963a70lrwcj8jrmb1jwxjpfd4am2i5m")))

