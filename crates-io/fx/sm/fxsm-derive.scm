(define-module (crates-io fx sm fxsm-derive) #:use-module (crates-io))

(define-public crate-fxsm-derive-0.1.0 (c (n "fxsm-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0ccr8g1c87j07ppwhsxchjvrq91kcpnxapi76nqaimqm8ynggggm")))

(define-public crate-fxsm-derive-0.2.0 (c (n "fxsm-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "14gdmxzfdq2qb5fbchwdnfc1wjkgg6ifcfl3qwllf4ya4qb6ikan")))

(define-public crate-fxsm-derive-0.3.0 (c (n "fxsm-derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "01wgpl0h1p3dllny7cvwzgq1zms33yp0dyp55z3lzx5sz4y2kqxq")))

