(define-module (crates-io fx da fxdatapi) #:use-module (crates-io))

(define-public crate-fxdatapi-0.1.5 (c (n "fxdatapi") (v "0.1.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g93yg1nafg8si8dvsdlfv7avnmmq9d3l1asaybcmfhpg08rbrcl")))

(define-public crate-fxdatapi-0.1.6 (c (n "fxdatapi") (v "0.1.6") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gk6zchlcffmx3z18mgh97sqrm1l6camdpag14iq0qrh4ssrki9i")))

(define-public crate-fxdatapi-0.1.7 (c (n "fxdatapi") (v "0.1.7") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cn8kk4avk7606b3lpml4gq2ng1yafs8kiydip15a66i1v5cvaz7")))

