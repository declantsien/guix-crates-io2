(define-module (crates-io fx ta fxtabs) #:use-module (crates-io))

(define-public crate-fxtabs-0.1.0 (c (n "fxtabs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "1s1j5zzmdpzihpfqkdrr32xccp39wq6626zrscamdcg4xpm32x2l")))

(define-public crate-fxtabs-0.2.0 (c (n "fxtabs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "051xjai6lghb28cca1dkraa3yhlk1a3kyrfv1hkbr8ibj9vzy1x6")))

