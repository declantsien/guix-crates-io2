(define-module (crates-io fx bo fxbot) #:use-module (crates-io))

(define-public crate-fxbot-0.1.0 (c (n "fxbot") (v "0.1.0") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serenity") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c8gmns08ii27rg0pyp6j0hvvvv8snfqrnfn2g5kww677gcmffqg")))

(define-public crate-fxbot-0.1.1 (c (n "fxbot") (v "0.1.1") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serenity") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11669k00bvpkcxpf3wzzajbf4v46wa3l6mdq893hm9j9l9ik238x")))

