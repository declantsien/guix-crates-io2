(define-module (crates-io fx pr fxprof-processed-profile) #:use-module (crates-io))

(define-public crate-fxprof-processed-profile-0.1.0 (c (n "fxprof-processed-profile") (v "0.1.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "debugid") (r "^0.8.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yn57c8q1yylq759lqqri8igknx8lc7dq36bymsbd1c083qcivrj")))

(define-public crate-fxprof-processed-profile-0.2.0 (c (n "fxprof-processed-profile") (v "0.2.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "debugid") (r "^0.8.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ig73xnkpi1d2m8aaydhf1vjq5d5vm39yzh9iddwx7170v4d6a7g")))

(define-public crate-fxprof-processed-profile-0.3.0 (c (n "fxprof-processed-profile") (v "0.3.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "debugid") (r "^0.8.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fzjwf3znl13czhwziwnchrm91j0ndkzh84vxn9jdp2h3vyz4h7z")))

(define-public crate-fxprof-processed-profile-0.4.0 (c (n "fxprof-processed-profile") (v "0.4.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "debugid") (r "^0.8.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0654k0wl265pm451lb4ff55vjfmgkfwds8kg467ldzgiqzifwxa1")))

(define-public crate-fxprof-processed-profile-0.5.0 (c (n "fxprof-processed-profile") (v "0.5.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "debugid") (r "^0.8.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12ks0a394wylap32zqycj7sh73qajs0a5ifkk482rk1qcjipwq2l")))

(define-public crate-fxprof-processed-profile-0.6.0 (c (n "fxprof-processed-profile") (v "0.6.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "debugid") (r "^0.8.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ggsn3im2bfcnxic0jzk00qgiacfrg2as6i4d8kj87kzxl52rl97") (r "1.58")))

(define-public crate-fxprof-processed-profile-0.7.0 (c (n "fxprof-processed-profile") (v "0.7.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "debugid") (r "^0.8.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08jsn3la3i6kypn8bcvbiipzah3yw4vgwicbj2j8nq28hasbn86f") (r "1.60")))

