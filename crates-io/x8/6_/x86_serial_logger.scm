(define-module (crates-io x8 #{6_}# x86_serial_logger) #:use-module (crates-io))

(define-public crate-x86_serial_logger-0.1.1-rc.1 (c (n "x86_serial_logger") (v "0.1.1-rc.1") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "0mdj8ziyfqp6hj726w25yv5k80i68v10slp431yix69szivs7bnl") (f (quote (("no_std"))))))

(define-public crate-x86_serial_logger-0.1.1-rc.2 (c (n "x86_serial_logger") (v "0.1.1-rc.2") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "17n82chmp7spm12y6h82rixvsrx2s286mbaw6fli4bv5g33r7p42") (f (quote (("no_std"))))))

(define-public crate-x86_serial_logger-1.0.0 (c (n "x86_serial_logger") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "111gl9a3y0jx6vgqk6gji7z333a1c31dq7a5r6bbwyf7xid7mhn3") (f (quote (("no_std"))))))

(define-public crate-x86_serial_logger-1.1.0 (c (n "x86_serial_logger") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "17x55x46643p0n4bpz7pmyix8540w7zbn55r37m9jpzij2k5fwlw") (f (quote (("no_std"))))))

