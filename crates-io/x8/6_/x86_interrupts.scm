(define-module (crates-io x8 #{6_}# x86_interrupts) #:use-module (crates-io))

(define-public crate-x86_interrupts-0.1.0 (c (n "x86_interrupts") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "pic8259") (r "^0.10.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "1djxhhiby0n25n0iki2786ylrmnigp7b0qfp86cqwl9isbyr2vj7") (f (quote (("no_std"))))))

(define-public crate-x86_interrupts-0.1.1-rc.1 (c (n "x86_interrupts") (v "0.1.1-rc.1") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "pic8259") (r "^0.10.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "1v3rr61zayx7czkhprw79bynhysxka5lppj2wzzbq9gcyv3csswn") (f (quote (("no_std"))))))

(define-public crate-x86_interrupts-1.0.0 (c (n "x86_interrupts") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "pic8259") (r "^0.10.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "08rjjxrn9dmrxp256jmd3lr9r861sbmf5nhggzvsgqiss6nyrfjm") (f (quote (("no_std"))))))

(define-public crate-x86_interrupts-1.0.1-rc.1 (c (n "x86_interrupts") (v "1.0.1-rc.1") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "pic8259") (r "^0.10.0") (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "134rb340bmn29c95pa5yniffb4y16jz7silmssywdp4kqkshxr4i") (f (quote (("no_std"))))))

