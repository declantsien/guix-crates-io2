(define-module (crates-io x8 #{6_}# x86_64-linux-nolibc) #:use-module (crates-io))

(define-public crate-x86_64-linux-nolibc-0.1.0 (c (n "x86_64-linux-nolibc") (v "0.1.0") (d (list (d (n "rcrt1") (r "^2.0.0") (d #t) (k 2)))) (h "1g8w0fkclwfa5v4l75sj7c58ssn9p4vz8kc4a8rp6dhchf3j1zgv") (r "1.56")))

