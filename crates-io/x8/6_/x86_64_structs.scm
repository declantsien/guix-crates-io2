(define-module (crates-io x8 #{6_}# x86_64_structs) #:use-module (crates-io))

(define-public crate-x86_64_structs-0.11.5 (c (n "x86_64_structs") (v "0.11.5") (d (list (d (n "array-init") (r "^0.1.1") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)))) (h "0iwl9isvmp09q1wrns2np7hvv9n25rhfqw13pfkbcfsxc29gv4z7") (f (quote (("const_fn")))) (y #t)))

(define-public crate-x86_64_structs-0.11.5-1 (c (n "x86_64_structs") (v "0.11.5-1") (d (list (d (n "array-init") (r "^0.1.1") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)))) (h "1cx4i2q196kg1q460v3r66irl6bxwsvncz92svwqampxfrpy466x") (f (quote (("const_fn")))) (y #t)))

(define-public crate-x86_64_structs-0.11.6 (c (n "x86_64_structs") (v "0.11.6") (d (list (d (n "array-init") (r "^0.1.1") (d #t) (k 0)) (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)))) (h "0h70nznpmg1nb00aavvrp8d67q88dn4c7ak6nxhhh90mvz9ppcrk") (f (quote (("const_fn")))) (y #t)))

