(define-module (crates-io x8 #{6_}# x86_testbench) #:use-module (crates-io))

(define-public crate-x86_testbench-0.1.1-rc.2 (c (n "x86_testbench") (v "0.1.1-rc.2") (d (list (d (n "bootloader") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "1p521h1600s005fs7f7s2crlz4az8bh7wvl0if4r76x3dknzabvi") (f (quote (("ext_panic"))))))

(define-public crate-x86_testbench-0.1.1-rc.3 (c (n "x86_testbench") (v "0.1.1-rc.3") (d (list (d (n "bootloader") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "1wc553b5sh1qamnki3zl35iiz0igqz28znxjiq0vxpdw09a391zs") (f (quote (("ext_panic"))))))

(define-public crate-x86_testbench-0.1.1-rc.4 (c (n "x86_testbench") (v "0.1.1-rc.4") (d (list (d (n "bootloader") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "1c1a3qnn4wfrjnar3n3sjl0l27lzhj5qgmfr42ah06kczz6pnp89") (f (quote (("ext_panic"))))))

(define-public crate-x86_testbench-0.1.1-rc.5 (c (n "x86_testbench") (v "0.1.1-rc.5") (d (list (d (n "bootloader") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "1h0whkj2m1mm84hjx1mb8skrn2l32j26wz09ka66dh7l42swg3bd") (f (quote (("ext_panic"))))))

(define-public crate-x86_testbench-0.1.1-rc.6 (c (n "x86_testbench") (v "0.1.1-rc.6") (d (list (d (n "bootloader") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "1qkv7mmrra0iiivcn8m0g83garjnh3cgcg91z1a2ggjl9xkm6xjx") (f (quote (("ext_panic"))))))

(define-public crate-x86_testbench-0.1.0 (c (n "x86_testbench") (v "0.1.0") (d (list (d (n "bootloader") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "1vqwgz44sazb177vgqrq12mnrnah834giw3k73nqf2agyk5h13q1") (f (quote (("ext_panic"))))))

(define-public crate-x86_testbench-0.1.1 (c (n "x86_testbench") (v "0.1.1") (d (list (d (n "bootloader") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "1fyqxpavzsqdadhhcnr8n4n987hiyxfs3sfb95p5pfrzxqr769w5") (f (quote (("ext_panic"))))))

(define-public crate-x86_testbench-0.1.2 (c (n "x86_testbench") (v "0.1.2") (d (list (d (n "bootloader") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "uart_16550") (r "^0.2.15") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.4") (d #t) (k 0)))) (h "004dps58a1rdn3p98l89nsaha4c1hqpj7202h8z8h98p8xj11mb3") (f (quote (("ext_panic"))))))

