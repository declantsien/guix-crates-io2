(define-module (crates-io x8 #{6_}# x86_ata) #:use-module (crates-io))

(define-public crate-x86_ata-0.1.0 (c (n "x86_ata") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.3") (d #t) (k 0)))) (h "1bi9gvnw2n6r1gkfjqrmgg0bjnk32701k3bc7vr71p861wza9p0q")))

