(define-module (crates-io x8 #{6_}# x86_64-xsave) #:use-module (crates-io))

(define-public crate-x86_64-xsave-0.1.0 (c (n "x86_64-xsave") (v "0.1.0") (h "1ddi4gbas8yw5k4ivnfabizd0izb06m3hg64a86vnd50gapmpikj")))

(define-public crate-x86_64-xsave-0.1.1 (c (n "x86_64-xsave") (v "0.1.1") (h "0fsfa4813pa16c9ng1qx765i7hjwg0kpn8s4gqwmvmzpr3l8a78c")))

(define-public crate-x86_64-xsave-0.1.2 (c (n "x86_64-xsave") (v "0.1.2") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)))) (h "1c48k0d9h9v5p84mfnk8l2jbar3nwvpkmv1ygm2jxgg78lx5v1pn")))

(define-public crate-x86_64-xsave-0.1.3 (c (n "x86_64-xsave") (v "0.1.3") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)))) (h "1rf1plcfp1krdbpmm5wm8l0kahijp8lsk42rfa2fj077x0gdichj")))

