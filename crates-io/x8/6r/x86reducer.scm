(define-module (crates-io x8 #{6r}# x86reducer) #:use-module (crates-io))

(define-public crate-x86reducer-0.1.0 (c (n "x86reducer") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "1wmzjnpl2cpqwbhbh34dqbw550z02y13idiicg82vvgsgdxrlm00")))

