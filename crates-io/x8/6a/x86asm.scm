(define-module (crates-io x8 #{6a}# x86asm) #:use-module (crates-io))

(define-public crate-x86asm-0.1.0 (c (n "x86asm") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.3.23") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)))) (h "1k0pw8ans5zs8ir0xmwfhjx72s18lli6pb4li2l6z7ccbw8lqkb5")))

