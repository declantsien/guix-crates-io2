(define-module (crates-io x8 #{6i}# x86intrin) #:use-module (crates-io))

(define-public crate-x86intrin-0.1.0 (c (n "x86intrin") (v "0.1.0") (h "0x80zmm7x9pyf9bvjj9cm08jjr17ql1x05z5fawmv5j7bpqh8h0w")))

(define-public crate-x86intrin-0.2.0 (c (n "x86intrin") (v "0.2.0") (h "1jpg58mnmdpw92f8fxlwnvwf2insvvqxicjlhlyac7yzyi7s6z1w")))

(define-public crate-x86intrin-0.2.1 (c (n "x86intrin") (v "0.2.1") (h "1kds5cz9bkx72nmb1pqw64v5jd9137mrwighgznzal5dlldfz6fp")))

(define-public crate-x86intrin-0.3.0 (c (n "x86intrin") (v "0.3.0") (h "0fqic6igblnzrssdsygb6jpn3swd5arifinfafqjalh6wiv1bsxd") (f (quote (("doc"))))))

(define-public crate-x86intrin-0.3.1 (c (n "x86intrin") (v "0.3.1") (h "1769fzsjlgia8ain61wn7sw1ns7q271bj4r67pbksfba329qz8dl") (f (quote (("doc"))))))

(define-public crate-x86intrin-0.4.0 (c (n "x86intrin") (v "0.4.0") (h "0v0yp3y3yxxw6gvww4m7gkhzf2y55rz4cyjclqabrnrxvgv4snkg") (f (quote (("doc"))))))

(define-public crate-x86intrin-0.4.1 (c (n "x86intrin") (v "0.4.1") (h "1m9s5fhba89mz7kpg7374hzhkcp4m66zccrgynjy7d69wgdz22lj") (f (quote (("doc"))))))

(define-public crate-x86intrin-0.4.2 (c (n "x86intrin") (v "0.4.2") (h "1d3w4f14rimggf5nvpxvrdhfvwv2486li0w272sb3q90lyw56y9x") (f (quote (("doc"))))))

(define-public crate-x86intrin-0.4.3 (c (n "x86intrin") (v "0.4.3") (h "0vbhi3azdpclapnls22c4h4r6z4l1w8favgky54581hr9jkyfraz") (f (quote (("doc"))))))

(define-public crate-x86intrin-0.4.4 (c (n "x86intrin") (v "0.4.4") (h "0b1vw9rq2c1mq88vjzp8z3c46l39vsyzfsw4pdfmhxkkh215vdw9") (f (quote (("doc"))))))

(define-public crate-x86intrin-0.4.5 (c (n "x86intrin") (v "0.4.5") (h "07nzn2zccdm3aklq3azvb6kw7k8zfwn3wzyix5q9p1zy44vm5v78") (f (quote (("doc"))))))

