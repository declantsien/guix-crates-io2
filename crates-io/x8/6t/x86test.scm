(define-module (crates-io x8 #{6t}# x86test) #:use-module (crates-io))

(define-public crate-x86test-0.0.1 (c (n "x86test") (v "0.0.1") (d (list (d (n "kvm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "x86") (r "^0.19") (d #t) (k 0)) (d (n "x86test-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "x86test-types") (r "^0.0.1") (d #t) (k 0)))) (h "1jiz7k0iyss974668s2z4q4pkbwkq4k8aqyy13hvrijc74mxy9qx")))

(define-public crate-x86test-0.0.2 (c (n "x86test") (v "0.0.2") (d (list (d (n "klogger") (r "^0.0.4") (f (quote ("use_ioports"))) (d #t) (k 0)) (d (n "kvm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "x86") (r "^0.19") (d #t) (k 0)) (d (n "x86test-macro") (r "^0.0.2") (d #t) (k 0)) (d (n "x86test-types") (r "^0.0.2") (d #t) (k 0)))) (h "0xyp0lljwijnysci082xac9pa4w95xhghmvwhcrnd46w25ps7nzp")))

(define-public crate-x86test-0.0.3 (c (n "x86test") (v "0.0.3") (d (list (d (n "klogger") (r "^0.0.4") (f (quote ("use_ioports"))) (d #t) (k 0)) (d (n "kvm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "x86") (r "^0.32") (d #t) (k 0)) (d (n "x86test-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "x86test-types") (r "^0.0.3") (d #t) (k 0)))) (h "00y3mc3sah5r197658dm8ndknwlhycbxjly7klskm29li8zn3jns")))

(define-public crate-x86test-0.0.4 (c (n "x86test") (v "0.0.4") (d (list (d (n "klogger") (r "^0.0.9") (f (quote ("use_ioports"))) (d #t) (k 0)) (d (n "kvm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "x86") (r "^0.45") (d #t) (k 0)) (d (n "x86test-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "x86test-types") (r "^0.0.4") (d #t) (k 0)))) (h "1xwz9z7v9i309b3xl2z9w9via7vgaxymsj79sfhywyr51ib0cvhv")))

(define-public crate-x86test-0.0.5 (c (n "x86test") (v "0.0.5") (d (list (d (n "klogger") (r "^0.0.10") (f (quote ("use_ioports"))) (d #t) (k 0)) (d (n "kvm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "x86") (r "^0.47") (d #t) (k 0)) (d (n "x86test-macro") (r "^0.0.5") (d #t) (k 0)) (d (n "x86test-types") (r "^0.0.5") (d #t) (k 0)))) (h "09siicd6xdz3afglv5gjjykj759fl3b10mryhx87pqid6bxii4ja")))

