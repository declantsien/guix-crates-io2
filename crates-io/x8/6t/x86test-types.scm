(define-module (crates-io x8 #{6t}# x86test-types) #:use-module (crates-io))

(define-public crate-x86test-types-0.0.1 (c (n "x86test-types") (v "0.0.1") (d (list (d (n "x86") (r "^0.20") (d #t) (k 0)))) (h "19p4a8mngf6vlvp7nkggvfc0rzpah6zggcq5q04rq2fvqd1i0a0j")))

(define-public crate-x86test-types-0.0.2 (c (n "x86test-types") (v "0.0.2") (d (list (d (n "x86") (r "^0.20") (d #t) (k 0)))) (h "0nn5fkm2ljhmpd5rjl97h7fl1np3n5ri63hqnvian8c8pnrfpwsn")))

(define-public crate-x86test-types-0.0.3 (c (n "x86test-types") (v "0.0.3") (d (list (d (n "x86") (r "^0.32") (d #t) (k 0)))) (h "1mzf90i6pcwx2jr77c2l5690q9kz9n480ykv4m8hckxs4zasf1s9")))

(define-public crate-x86test-types-0.0.4 (c (n "x86test-types") (v "0.0.4") (d (list (d (n "x86") (r "^0.45") (d #t) (k 0)))) (h "0l3i5vf2v7lryvxlj864vy8shj5qhmw65b9dy2g8dy4yx8m7spm5")))

(define-public crate-x86test-types-0.0.5 (c (n "x86test-types") (v "0.0.5") (d (list (d (n "x86") (r "^0.47") (d #t) (k 0)))) (h "0wvgigc4i1qszgd6vzsk9r25wqii7d4hmb3rac1dr4l9hkdhw8mg")))

