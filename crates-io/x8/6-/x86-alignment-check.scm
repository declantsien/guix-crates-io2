(define-module (crates-io x8 #{6-}# x86-alignment-check) #:use-module (crates-io))

(define-public crate-x86-alignment-check-0.1.1 (c (n "x86-alignment-check") (v "0.1.1") (h "0075vi0k0lpwvymwp6i8p553vygxai0kiphfialgv7v7h461s0c3") (y #t) (r "1.59.0")))

(define-public crate-x86-alignment-check-0.1.2 (c (n "x86-alignment-check") (v "0.1.2") (h "0xmnnprl89dxxmnplrif4lli96x1j0gdvjm19z2jasz25bw1y9jp") (r "1.59.0")))

(define-public crate-x86-alignment-check-0.1.3 (c (n "x86-alignment-check") (v "0.1.3") (h "1ah5iv1k6jg8j7gjjlxgqzplkf5zsqchy6jyqdwr0svam9amgzrm") (r "1.59.0")))

(define-public crate-x86-alignment-check-0.1.4 (c (n "x86-alignment-check") (v "0.1.4") (h "1f0lz5km7ssh4havrl1wh4c0x9vya8bwqg42j4ddkmxwxd24ib93") (r "1.59.0")))

(define-public crate-x86-alignment-check-0.1.5 (c (n "x86-alignment-check") (v "0.1.5") (h "075pq72jffz5w1ymq8b7qbblgp5c2ia2qmr1a0d4cpv2wnzi8a0l") (r "1.59.0")))

(define-public crate-x86-alignment-check-0.1.6 (c (n "x86-alignment-check") (v "0.1.6") (h "137l2qa4sik3m4lylzngqmnni89vkmlzz2s07n8w363d9hym65rz") (r "1.59.0")))

