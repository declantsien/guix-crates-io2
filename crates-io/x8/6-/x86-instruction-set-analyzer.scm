(define-module (crates-io x8 #{6-}# x86-instruction-set-analyzer) #:use-module (crates-io))

(define-public crate-x86-instruction-set-analyzer-0.1.0 (c (n "x86-instruction-set-analyzer") (v "0.1.0") (d (list (d (n "iced-x86") (r "^1.21.0") (d #t) (k 0)) (d (n "object") (r "^0.35.0") (d #t) (k 0)))) (h "0bvb901gw1xya16l0lszdspd8y2n30d65a4wvxw9zls89kcq20nr")))

