(define-module (crates-io us as usask_cba_calc) #:use-module (crates-io))

(define-public crate-usask_cba_calc-0.2.1 (c (n "usask_cba_calc") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1d5p090xrmk71i830zg4qbfbg48bx1vfsa6f2dxyg32izr23a8wv")))

(define-public crate-usask_cba_calc-0.2.3 (c (n "usask_cba_calc") (v "0.2.3") (d (list (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0njqrlvp13lvgimnk7hqfmz02199x3l9xnfn9c6gwlk3bfvzv61z")))

(define-public crate-usask_cba_calc-0.2.4 (c (n "usask_cba_calc") (v "0.2.4") (d (list (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1mirfdh279s2b50r1vznb6iarnr2wdmxwd288piwxr5ys68qlwdk")))

(define-public crate-usask_cba_calc-0.2.6 (c (n "usask_cba_calc") (v "0.2.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1yv4qwx5g38b4jk876m6vd7vsh5xp3n46lh58b6wbzimqin2yw48")))

(define-public crate-usask_cba_calc-0.2.7 (c (n "usask_cba_calc") (v "0.2.7") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0djbkk9pys95jfhvrhba22cgii95b6ymhmqzsrxz8iawcbkkccb3")))

(define-public crate-usask_cba_calc-0.2.8 (c (n "usask_cba_calc") (v "0.2.8") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "08l5wwzdhwbrj83qy8ckfq1aky2i38d69hhmdhjmh1qh7ddkdcg6")))

(define-public crate-usask_cba_calc-0.2.9 (c (n "usask_cba_calc") (v "0.2.9") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0pkgs2f235r5wv5gcvwxgia0ddlanh01z0h9cmgr5vjmzja97laz")))

(define-public crate-usask_cba_calc-0.3.0 (c (n "usask_cba_calc") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "07qyq4mwlgxvg6gidwh1j2yw6bqaf80pbhh8idn578r2rkvxh09n")))

(define-public crate-usask_cba_calc-0.3.1 (c (n "usask_cba_calc") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1816m2rr5l3nvhynncfd0slsaq7iw7rg3vg23acfgn4l7l7ni2g7")))

(define-public crate-usask_cba_calc-0.3.2 (c (n "usask_cba_calc") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "05fx300qfrasdvil7pjbnkbdf1h79n2bw8qap4x0chiw9yzgrrvf")))

(define-public crate-usask_cba_calc-0.3.3 (c (n "usask_cba_calc") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "14n8cn592pcxdl010grd87fhn413k3p8jbjkykdxl604lgj03c9i")))

(define-public crate-usask_cba_calc-0.3.4 (c (n "usask_cba_calc") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "01sg1llr7ml0p1849jwqixlph1fdy482pfgzdfx8vvzd6iqqgiww")))

(define-public crate-usask_cba_calc-0.3.5 (c (n "usask_cba_calc") (v "0.3.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1mm9315frg4dkk3y75g767lx237mgzlva16q94046zajjmc5c0qb")))

(define-public crate-usask_cba_calc-0.3.6 (c (n "usask_cba_calc") (v "0.3.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1y14imsym3zrsn7mavj3v8s5gr93abc770gf9p6ivr62c0gav5c1")))

(define-public crate-usask_cba_calc-0.4.0 (c (n "usask_cba_calc") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("io-std" "time" "rt-multi-thread" "macros" "io-util"))) (d #t) (k 0)))) (h "1va3xvf8wcavb3qcqha73vkvl5dg90hi0jp2cqnfv45cz3fy62s9")))

(define-public crate-usask_cba_calc-0.4.1 (c (n "usask_cba_calc") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("io-std" "time" "rt-multi-thread" "macros" "io-util"))) (d #t) (k 0)))) (h "17jgbl4l73f2yfrg12q96wm1j8lg96maybf0lxim0aqwynxcd85g")))

(define-public crate-usask_cba_calc-0.4.2 (c (n "usask_cba_calc") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("io-std" "time" "rt-multi-thread" "macros" "io-util"))) (d #t) (k 0)))) (h "140z35n7834s9zbsz5w1pwwjhq4cl7qk5fjvvysgwanj551kqnb4")))

