(define-module (crates-io us dz usdz) #:use-module (crates-io))

(define-public crate-usdz-0.0.0 (c (n "usdz") (v "0.0.0") (h "03b4glqhmd875q2y2v5qxk6wqkzgn6inpp65bkpmyjqgq0lbzvm6")))

(define-public crate-usdz-0.0.1 (c (n "usdz") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0j13fm3ykkdd3gq6rva8jq2fy3zqikq24pcx7lcjmbb3xc8cgan0")))

