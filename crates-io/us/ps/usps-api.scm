(define-module (crates-io us ps usps-api) #:use-module (crates-io))

(define-public crate-usps-api-0.0.1 (c (n "usps-api") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "roxmltree") (r "^0.7.0") (d #t) (k 0)))) (h "12l7ljmq95rqb7a9l8vhjzas3197w37mryjgga3zqf4zf8bcp272")))

