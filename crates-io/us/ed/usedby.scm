(define-module (crates-io us ed usedby) #:use-module (crates-io))

(define-public crate-usedby-0.1.0 (c (n "usedby") (v "0.1.0") (d (list (d (n "procfs") (r "^0.16.0") (d #t) (k 0)))) (h "02vwgc9my31pqncnhj50k4c5dlv2d78nn1zyh27w8q0dbxiy0h2y")))

(define-public crate-usedby-0.1.1 (c (n "usedby") (v "0.1.1") (d (list (d (n "procfs") (r "^0.16.0") (d #t) (k 0)))) (h "0pkm80sbv9g7sbsr9vryndzgnhi2jw8nf881zs71zr17w793ral5")))

(define-public crate-usedby-0.1.2 (c (n "usedby") (v "0.1.2") (d (list (d (n "procfs") (r "^0.16.0") (d #t) (k 0)))) (h "0a0yi9wjb3dzpc8kd68sdpxdgfx5wq0b0ckbwpbh75hf9yp7qmfw")))

(define-public crate-usedby-0.1.3 (c (n "usedby") (v "0.1.3") (d (list (d (n "procfs") (r "^0.16.0") (d #t) (k 0)))) (h "1wc5iabw0qfg7cwvhxajjm9dcp57njp9sbgscnk42s24hpjldkax")))

(define-public crate-usedby-0.1.4 (c (n "usedby") (v "0.1.4") (d (list (d (n "procfs") (r "^0.16.0") (d #t) (k 0)))) (h "189rdjfl0w5m2ly74nnb5k9xn11v9ps03k3s252impkpfdxaz4s9")))

(define-public crate-usedby-0.2.0 (c (n "usedby") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.0") (d #t) (k 0)) (d (n "procfs") (r "^0.16.0") (d #t) (k 0)))) (h "0cwlnhhg7qxml859sqykmi3w7y4si1pi4r4a46mkkp6409pd9ac4")))

