(define-module (crates-io us le uslegalpro) #:use-module (crates-io))

(define-public crate-uslegalpro-0.1.0 (c (n "uslegalpro") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1q8cx7ds39c6wjxmbfj16s097j3sv2fxmxaq5rz74maklh7na2wd")))

