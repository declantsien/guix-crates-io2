(define-module (crates-io us e_ use_css) #:use-module (crates-io))

(define-public crate-use_css-0.1.0 (c (n "use_css") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "0bclfk04lasr2g6fbhf0yyiphd3p1bj8nq55cwwplz39iygrv8zp") (y #t) (r "1.67.1")))

(define-public crate-use_css-0.2.0 (c (n "use_css") (v "0.2.0") (d (list (d (n "stylist") (r "^0.12.0") (f (quote ("yew" "parser"))) (d #t) (k 0)) (d (n "use_css_procmacros") (r "0.*") (d #t) (k 0)))) (h "0mwlcmyyhp7mckwa76b683i8h0ldn3hnj4cawkg2lmfl11jvxgjc")))

