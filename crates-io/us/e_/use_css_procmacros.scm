(define-module (crates-io us e_ use_css_procmacros) #:use-module (crates-io))

(define-public crate-use_css_procmacros-0.2.0 (c (n "use_css_procmacros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "1ins610ib308gcxsr3r6fj923g8m2yz6iywn1sk6mbply4dncgj5")))

