(define-module (crates-io us bs usbsdmux) #:use-module (crates-io))

(define-public crate-usbsdmux-0.1.0 (c (n "usbsdmux") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "usb2642-i2c") (r "^0.1") (d #t) (k 0)))) (h "1a9adha0jzng5gnlskzlwrwdkmjkbzb2nb3mq50i2wb5px1vlw5h")))

