(define-module (crates-io us n- usn-journal-watcher) #:use-module (crates-io))

(define-public crate-usn-journal-watcher-0.1.0 (c (n "usn-journal-watcher") (v "0.1.0") (d (list (d (n "windows") (r "^0.29.0") (f (quote ("alloc" "Win32_Foundation" "Win32_System_Time" "Win32_System_SystemInformation" "Win32_Security" "Win32_System_IO" "Win32_Storage_FileSystem" "Win32_System_Ioctl"))) (d #t) (k 0)))) (h "14s7404vsrhknwh0v1db6wf9yxaj6qywjg8pp3wgg7vnc1bj9cpc") (y #t)))

(define-public crate-usn-journal-watcher-0.1.1 (c (n "usn-journal-watcher") (v "0.1.1") (d (list (d (n "windows") (r "^0.29.0") (f (quote ("alloc" "Win32_Foundation" "Win32_System_Time" "Win32_System_SystemInformation" "Win32_Security" "Win32_System_IO" "Win32_Storage_FileSystem" "Win32_System_Ioctl"))) (d #t) (k 0)))) (h "10zhsfj0p9vi4kcffc7p5qgwjklg1askfmwhpbp34n4lzlcjav5c") (y #t)))

(define-public crate-usn-journal-watcher-0.1.2 (c (n "usn-journal-watcher") (v "0.1.2") (d (list (d (n "windows") (r "^0.29.0") (f (quote ("alloc" "Win32_Foundation" "Win32_System_Time" "Win32_System_SystemInformation" "Win32_Security" "Win32_System_IO" "Win32_Storage_FileSystem" "Win32_System_Ioctl"))) (d #t) (k 0)))) (h "09ml2aql68nzbh8lf56c25g255h2v92i8v7afb9j9m2cc2nw491j") (y #t)))

(define-public crate-usn-journal-watcher-0.1.3 (c (n "usn-journal-watcher") (v "0.1.3") (d (list (d (n "windows") (r "^0.29.0") (f (quote ("alloc" "Win32_Foundation" "Win32_System_Time" "Win32_System_SystemInformation" "Win32_Security" "Win32_System_IO" "Win32_Storage_FileSystem" "Win32_System_Ioctl"))) (d #t) (k 0)))) (h "045xjbnvhyadzy6yh3wwlkbdp31md9y7sqb39kskf9z1kgv8xsm9") (y #t)))

