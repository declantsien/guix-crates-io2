(define-module (crates-io us br usbrh) #:use-module (crates-io))

(define-public crate-usbrh-0.1.0 (c (n "usbrh") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1p7sbn3fnjfxg99sw0vz2x2h7bnz9fq1skrjs9m1hn9ra2r4gcn2")))

