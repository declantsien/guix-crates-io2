(define-module (crates-io us br usbredirhost-sys) #:use-module (crates-io))

(define-public crate-usbredirhost-sys-0.0.1 (c (n "usbredirhost-sys") (v "0.0.1") (d (list (d (n "libusb1-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)) (d (n "usbredirparser-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0z9czj5an631fymi0if9pr1x82crmz7qp538bmk1nq7pl5fk0z5j") (l "usbredirhost")))

(define-public crate-usbredirhost-sys-0.2.0 (c (n "usbredirhost-sys") (v "0.2.0") (d (list (d (n "libusb1-sys") (r "^0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)) (d (n "usbredirparser-sys") (r "^0.2") (d #t) (k 0)))) (h "02yr9a78bmfcikdlgphgg1jfkb9rghjwqppdpghz9bgay9jaia16") (l "usbredirhost")))

