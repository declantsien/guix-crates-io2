(define-module (crates-io us br usbredirparser) #:use-module (crates-io))

(define-public crate-usbredirparser-0.0.1 (c (n "usbredirparser") (v "0.0.1") (d (list (d (n "ffi") (r "^0.0.1") (d #t) (k 0) (p "usbredirparser-sys")) (d (n "libc") (r "^0.2.92") (d #t) (k 0)))) (h "0mkarx7ahab1s4vjf73ka1pi27q0mjli4wk7nh43xfvw3ljbby70")))

(define-public crate-usbredirparser-0.2.0 (c (n "usbredirparser") (v "0.2.0") (d (list (d (n "ffi") (r "^0.2") (d #t) (k 0) (p "usbredirparser-sys")) (d (n "libc") (r "^0.2.155") (d #t) (k 0)))) (h "1px3mhqdndzm22ii7r9l94s0wknqg5bx75pcplmbx20y0pkw1a2f")))

