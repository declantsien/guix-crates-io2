(define-module (crates-io us br usbredirparser-sys) #:use-module (crates-io))

(define-public crate-usbredirparser-sys-0.0.1 (c (n "usbredirparser-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "148diqg188dkv9x8ykqkigjhwjj59vbckkdwcb3nz4c7w4sfic7q") (l "usbredirparser")))

(define-public crate-usbredirparser-sys-0.2.0 (c (n "usbredirparser-sys") (v "0.2.0") (d (list (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "0xlmbjh865cargbn67c4q726isa4khw447bgvc2z9h9czkhqn8zs") (l "usbredirparser")))

