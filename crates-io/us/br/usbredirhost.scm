(define-module (crates-io us br usbredirhost) #:use-module (crates-io))

(define-public crate-usbredirhost-0.0.1 (c (n "usbredirhost") (v "0.0.1") (d (list (d (n "color-eyre") (r "^0.5.11") (d #t) (k 2)) (d (n "ffi") (r "^0.0.1") (d #t) (k 0) (p "usbredirhost-sys")) (d (n "lexopt") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.92") (d #t) (k 0)) (d (n "parser") (r "^0.0.1") (d #t) (k 0) (p "usbredirparser")) (d (n "rusb") (r "^0.9.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 2)))) (h "1mckjf75jp6mxsv14fqladq87b9fxl8ny26imw1n45xhzr6mwj47")))

(define-public crate-usbredirhost-0.2.0 (c (n "usbredirhost") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "ffi") (r "^0.2") (d #t) (k 0) (p "usbredirhost-sys")) (d (n "lexopt") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.155") (d #t) (k 0)) (d (n "parser") (r "^0.2") (d #t) (k 0) (p "usbredirparser")) (d (n "rusb") (r "^0.9.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "15r8r7mciwckxh6im18zz0iw3jrmabsybq2hz6ml0gksay8wz5a1")))

