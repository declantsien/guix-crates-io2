(define-module (crates-io us _t us_time_parser) #:use-module (crates-io))

(define-public crate-us_time_parser-0.1.0 (c (n "us_time_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "036rgnllivmm3p6508m3x1k69x1vb59zsm024b5h2zyy86sw9jsa")))

(define-public crate-us_time_parser-0.1.1 (c (n "us_time_parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "0bv0bw5lh4v6nab56qvr68zja17xamd1r94wpz1i4my689ljhbqs")))

(define-public crate-us_time_parser-0.1.2 (c (n "us_time_parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "0rc09djszrvmjqf0yd9yndipb2vy2flzi2wx5wf7pswdmy4xlpnh")))

