(define-module (crates-io us bd usbd-webusb) #:use-module (crates-io))

(define-public crate-usbd-webusb-1.0.0 (c (n "usbd-webusb") (v "1.0.0") (d (list (d (n "usb-device") (r "^0.2.4") (d #t) (k 0)))) (h "1zmxy6c5dgx6fg6f6n92405mgbspgfvf1qpnkyz3p2hi4qlv74vk")))

(define-public crate-usbd-webusb-1.0.1 (c (n "usbd-webusb") (v "1.0.1") (d (list (d (n "usb-device") (r "^0.2.7") (d #t) (k 0)))) (h "0grsqypgv5zcyh04v93ilgca9v6m49c8dqwjwm64apffclc8l6l0") (y #t)))

(define-public crate-usbd-webusb-1.0.2 (c (n "usbd-webusb") (v "1.0.2") (d (list (d (n "usb-device") (r "^0.2.7") (d #t) (k 0)))) (h "1n2b8qvyflaw4mnz96alarbsb97h4gx3nxcy0l9mydi6gamfqczd")))

