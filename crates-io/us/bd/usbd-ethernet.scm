(define-module (crates-io us bd usbd-ethernet) #:use-module (crates-io))

(define-public crate-usbd-ethernet-0.1.0 (c (n "usbd-ethernet") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (f (quote ("defmt-impl"))) (d #t) (k 0)) (d (n "smoltcp") (r "^0.9") (f (quote ("defmt" "socket-raw" "socket-udp" "socket-tcp" "socket-icmp" "socket-dhcpv4" "socket-dns" "proto-ipv4" "proto-ipv6"))) (k 0)) (d (n "usb-device") (r "^0.2") (f (quote ("defmt"))) (d #t) (k 0)))) (h "1g8jxh8lic4j3dgxdynm4bbw0lq6ldg8z1jspyi9w7g97v5h8fbq")))

(define-public crate-usbd-ethernet-0.1.1 (c (n "usbd-ethernet") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (f (quote ("defmt-impl"))) (d #t) (k 0)) (d (n "smoltcp") (r "^0.9") (f (quote ("defmt" "socket-raw" "socket-udp" "socket-tcp" "socket-icmp" "socket-dhcpv4" "socket-dns" "proto-ipv4" "proto-ipv6"))) (k 0)) (d (n "usb-device") (r "^0.2") (f (quote ("defmt"))) (d #t) (k 0)))) (h "1mnc8nqj9m5cwr5f5xlhr6j17kivs1zbskkamawn9l0mbslpn5z7")))

(define-public crate-usbd-ethernet-0.1.2 (c (n "usbd-ethernet") (v "0.1.2") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (f (quote ("defmt-impl"))) (d #t) (k 0)) (d (n "smoltcp") (r "^0.9") (f (quote ("defmt" "proto-ipv4" "medium-ethernet" "socket-udp"))) (k 0)) (d (n "usb-device") (r "^0.2") (f (quote ("defmt"))) (d #t) (k 0)))) (h "0gldyv8y5v35kc2ddspplx7w1jlrzkjn7bvd81swjwjclkm7ls4w")))

