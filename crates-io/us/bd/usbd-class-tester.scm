(define-module (crates-io us bd usbd-class-tester) #:use-module (crates-io))

(define-public crate-usbd-class-tester-0.1.0 (c (n "usbd-class-tester") (v "0.1.0") (d (list (d (n "usb-device") (r "^0.3.2") (d #t) (k 0)))) (h "1yhw5wyb8w46632076bqj3zng0xywkd0vvpdn2319scixw3chlsj")))

(define-public crate-usbd-class-tester-0.2.0 (c (n "usbd-class-tester") (v "0.2.0") (d (list (d (n "usb-device") (r "^0.3.2") (d #t) (k 0)))) (h "0ypgjl5n3bpg8384x1rapgks6k4686xpqcmywjv9x1c6gf9zm301")))

(define-public crate-usbd-class-tester-0.2.1 (c (n "usbd-class-tester") (v "0.2.1") (d (list (d (n "usb-device") (r "^0.3.2") (d #t) (k 0)))) (h "0cbygng6sxai63950xvd6y199nbpa4fhklplrmf83ixdzhhy3k8c")))

(define-public crate-usbd-class-tester-0.3.0 (c (n "usbd-class-tester") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "usb-device") (r "^0.3.2") (d #t) (k 0)))) (h "0a8sjivbis36bwfxp5fhjn5w8860rjwcr2m6q40ccwyq3lnfkpbr") (f (quote (("default" "initlog")))) (s 2) (e (quote (("initlog" "dep:env_logger"))))))

