(define-module (crates-io us bd usbd-picotool-reset) #:use-module (crates-io))

(define-public crate-usbd-picotool-reset-0.1.0 (c (n "usbd-picotool-reset") (v "0.1.0") (d (list (d (n "cortex-m-rt") (r "^0.7.3") (d #t) (k 2)) (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "rp-pico") (r "^0.7.0") (d #t) (k 2)) (d (n "rp2040-hal") (r "^0.8.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 0)))) (h "119604jhyn9midq693m5n1i29112585q1xdyk4c09swwv7gm6c26")))

(define-public crate-usbd-picotool-reset-0.1.1 (c (n "usbd-picotool-reset") (v "0.1.1") (d (list (d (n "cortex-m-rt") (r "^0.7.3") (d #t) (k 2)) (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "rp-pico") (r "^0.7.0") (d #t) (k 2)) (d (n "rp2040-hal") (r "^0.8.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 0)))) (h "0d5dhx5mq20fm28lp92sp8iqygw4lpk56cq613lgya7v593sc465")))

(define-public crate-usbd-picotool-reset-0.2.0 (c (n "usbd-picotool-reset") (v "0.2.0") (d (list (d (n "cortex-m-rt") (r "^0.7.3") (d #t) (k 2)) (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "rp-pico") (r "^0.8.0") (d #t) (k 2)) (d (n "rp2040-hal") (r "^0.9.0") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 0)))) (h "12yw32p3dpz3wrifb0r046d3bx31djspwy4k757mg9fjnk8g81ia")))

(define-public crate-usbd-picotool-reset-0.3.0 (c (n "usbd-picotool-reset") (v "0.3.0") (d (list (d (n "cortex-m-rt") (r "^0.7.3") (d #t) (k 2)) (d (n "defmt") (r ">=0.2.0, <0.4") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "rp-pico") (r "^0.9.0") (d #t) (k 2)) (d (n "rp2040-hal") (r "^0.10.1") (d #t) (k 0)) (d (n "usb-device") (r "^0.3.2") (d #t) (k 0)))) (h "0pxz3hwkrvswmh1a1ay12xm4g4sfrjq0aviggkrhlfwflq4826m0")))

