(define-module (crates-io us bd usbd-blaster) #:use-module (crates-io))

(define-public crate-usbd-blaster-0.1.0 (c (n "usbd-blaster") (v "0.1.0") (d (list (d (n "arduino_mkrvidor4000") (r "~0.1") (f (quote ("default" "usb" "unproven"))) (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "usb-device") (r "~0.2") (d #t) (k 0)))) (h "14ma0v6l5nm0ysa9w6vbw5wl4hj7g2iz3wpm5wwanzmya3hnfkkz") (y #t)))

(define-public crate-usbd-blaster-0.1.1 (c (n "usbd-blaster") (v "0.1.1") (d (list (d (n "arduino_mkrvidor4000") (r "~0.1") (f (quote ("default" "usb" "unproven"))) (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "usb-device") (r "~0.2") (d #t) (k 0)))) (h "0jblm206b8gqcnsfy5xsaj4l23x7kfci1ym55dbimh5442frkayk") (y #t)))

(define-public crate-usbd-blaster-0.1.2 (c (n "usbd-blaster") (v "0.1.2") (d (list (d (n "arduino_mkrvidor4000") (r "~0.1") (f (quote ("default" "usb" "unproven"))) (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "usb-device") (r "~0.2") (d #t) (k 0)))) (h "0gy0kk337ddm4ggaf5rpg3wc0pa6dspnw3qd3hhs55r5fxdj5jmy") (y #t)))

(define-public crate-usbd-blaster-0.1.3 (c (n "usbd-blaster") (v "0.1.3") (d (list (d (n "arduino_mkrvidor4000") (r "~0.1") (f (quote ("default" "usb" "unproven"))) (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "usb-device") (r "~0.2") (d #t) (k 0)))) (h "1l5xbcvhipnj0km6y3710h8gsiip6x7gzh21sbgdkbcasr3w966y")))

