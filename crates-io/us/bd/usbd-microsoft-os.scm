(define-module (crates-io us bd usbd-microsoft-os) #:use-module (crates-io))

(define-public crate-usbd-microsoft-os-0.1.0 (c (n "usbd-microsoft-os") (v "0.1.0") (d (list (d (n "usb-device") (r "^0.2") (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "1q127kqby8qzkcv5xy6h627ihkmx3bq2n9av4k8x8a53vn4k8hb5")))

