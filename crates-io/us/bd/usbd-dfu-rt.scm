(define-module (crates-io us bd usbd-dfu-rt) #:use-module (crates-io))

(define-public crate-usbd-dfu-rt-0.1.0 (c (n "usbd-dfu-rt") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "196cg83w86arqvpwkhrn2s4pcf6nvmjqzy2pxgpq9r6z9jwx9ksj")))

(define-public crate-usbd-dfu-rt-0.2.0 (c (n "usbd-dfu-rt") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "1ng59zp8y894k568m6bh0bpx23a6d8iilgzw268b52fy1xrzjjzz")))

(define-public crate-usbd-dfu-rt-0.3.0 (c (n "usbd-dfu-rt") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "14ylp3rcviixchv7g6gi57xjn96h7n563zlhzc87f337kvsr0xf1")))

(define-public crate-usbd-dfu-rt-0.3.1 (c (n "usbd-dfu-rt") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "usb-device") (r "^0.2") (d #t) (k 0)))) (h "08z1h2n9wf0fnxv65xwzylbzy3iz84sc6fzy5c4008jqf4jzr5ji")))

