(define-module (crates-io us bd usbd-dfu) #:use-module (crates-io))

(define-public crate-usbd-dfu-0.1.0 (c (n "usbd-dfu") (v "0.1.0") (d (list (d (n "stm32f1xx-hal") (r "^0.7.0") (f (quote ("rt" "stm32f103" "medium" "stm32-usbd"))) (d #t) (k 2)) (d (n "usb-device") (r "^0.2.8") (d #t) (k 0)))) (h "13r7blmwpyf8nfxs12b3sxpd0c11qbmw5kn3rqfg5b8zmjhfjgf5")))

(define-public crate-usbd-dfu-0.1.1 (c (n "usbd-dfu") (v "0.1.1") (d (list (d (n "stm32f1xx-hal") (r "^0.7.0") (f (quote ("rt" "stm32f103" "medium" "stm32-usbd"))) (d #t) (k 2)) (d (n "usb-device") (r "^0.2.8") (d #t) (k 0)))) (h "0jmnavi4sa5wx09sdffwp21m4w474yk0pz4b4s6d5dv1912qmlqq")))

(define-public crate-usbd-dfu-0.2.0 (c (n "usbd-dfu") (v "0.2.0") (d (list (d (n "stm32f1xx-hal") (r "^0.7.0") (f (quote ("rt" "stm32f103" "medium" "stm32-usbd"))) (d #t) (k 2)) (d (n "usb-device") (r "^0.2.8") (d #t) (k 0)))) (h "1rb2mv9cmayjbvxa4h00jr9vmy8cggc4fnd4adaq1x4jpppd3i2f")))

(define-public crate-usbd-dfu-0.3.0 (c (n "usbd-dfu") (v "0.3.0") (d (list (d (n "stm32f1xx-hal") (r "^0.7.0") (f (quote ("rt" "stm32f103" "medium" "stm32-usbd"))) (d #t) (k 2)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 0)))) (h "0f0250jiiivpqr9ascgb6hls5a0vnvsn0q09lbjmcfiqwdkz82hs")))

(define-public crate-usbd-dfu-0.0.3 (c (n "usbd-dfu") (v "0.0.3") (d (list (d (n "stm32f1xx-hal") (r "^0.7.0") (f (quote ("rt" "stm32f103" "medium" "stm32-usbd"))) (d #t) (k 2)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 0)))) (h "1wvpxlm29x2vqda0d5z7h191ffsbbd5c3ag6l28kvwx4pagjc7mf") (y #t)))

(define-public crate-usbd-dfu-0.3.1 (c (n "usbd-dfu") (v "0.3.1") (d (list (d (n "stm32f1xx-hal") (r "^0.7.0") (f (quote ("rt" "stm32f103" "medium" "stm32-usbd"))) (d #t) (k 2)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 0)))) (h "187dlyqy7gvs08pqwifrhlyv2825zbshh0rjflghxml4miimbzgw")))

(define-public crate-usbd-dfu-0.4.0 (c (n "usbd-dfu") (v "0.4.0") (d (list (d (n "usb-device") (r "^0.3.2") (d #t) (k 0)))) (h "085qg0x9gkhcgjkz9j3w616i6wlm1c6wm28s6vylc1231a1b19j8")))

