(define-module (crates-io us bd usbd_mass_storage) #:use-module (crates-io))

(define-public crate-usbd_mass_storage-0.1.0 (c (n "usbd_mass_storage") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "itm_logger") (r "^0.1.0") (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "packing") (r "^0.1.0") (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.4") (d #t) (k 0)))) (h "1ifycziv55lzvqvffwph1qqc3dmmvsjsrz99x8k2yy2i5cnrm79d") (f (quote (("trace-usb-control") ("trace-all" "trace-usb-control"))))))

