(define-module (crates-io us bd usbd-ctaphid) #:use-module (crates-io))

(define-public crate-usbd-ctaphid-0.0.0-unreleased (c (n "usbd-ctaphid") (v "0.0.0-unreleased") (h "1z314k6sb9c4a920bh5kwdc72nzjp7ff5jnz9ky1d9rb75v6d93v")))

(define-public crate-usbd-ctaphid-0.1.0 (c (n "usbd-ctaphid") (v "0.1.0") (d (list (d (n "ctap-types") (r "^0.1.0") (d #t) (k 0)) (d (n "ctaphid-dispatch") (r "^0.1.0") (d #t) (k 0)) (d (n "delog") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "heapless-bytes") (r "^0.3") (d #t) (k 0)) (d (n "interchange") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.3") (d #t) (k 0)))) (h "1lh33ly49pqw359ny4756nxd0aj16g280msn47mkc8ln1xk9zdnx") (f (quote (("log-warn") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("default"))))))

