(define-module (crates-io us bd usbd-hid) #:use-module (crates-io))

(define-public crate-usbd-hid-0.1.0 (c (n "usbd-hid") (v "0.1.0") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.1") (d #t) (k 0)))) (h "1c4w19y7qfjh0s2papprlzbbsdrc9zcljxixfj8raqc7a8lnl93s")))

(define-public crate-usbd-hid-0.1.2 (c (n "usbd-hid") (v "0.1.2") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.1") (d #t) (k 0)))) (h "1k3mlkfzdmlvi9kwv1a6w9fkk7a8z2bvdn6f72lqzy9fclxfif1z")))

(define-public crate-usbd-hid-0.1.3 (c (n "usbd-hid") (v "0.1.3") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0iab72i75q365140qm82xsp844vnlkj7qww98kdbgni6c1sjf4fj")))

(define-public crate-usbd-hid-0.1.4 (c (n "usbd-hid") (v "0.1.4") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0khnf35cna20rhwzvshbv1r77r9lz4xxwl3h7f3g94145gp1lfy1")))

(define-public crate-usbd-hid-0.2.0 (c (n "usbd-hid") (v "0.2.0") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.1.2") (d #t) (k 0)))) (h "0a0lb3fg9f8v6w3q18jwmcbcaflqhbw111z12xras04lvrsminh4")))

(define-public crate-usbd-hid-0.2.1 (c (n "usbd-hid") (v "0.2.1") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.1.2") (d #t) (k 0)))) (h "1i5n0vflq23qsijib2l7yk6whdnb3a2xx8vwlksr98m1qp9362jv")))

(define-public crate-usbd-hid-0.3.0 (c (n "usbd-hid") (v "0.3.0") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0cjgzl6lv387q6p99lknxly0z1zmsxs39irxgyxz8xbmcl902sa7")))

(define-public crate-usbd-hid-0.3.1 (c (n "usbd-hid") (v "0.3.1") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.2.1") (d #t) (k 0)))) (h "1fq9c1m59jjnlpjwv69zabl7klypqn9iwdb1y81cwf5rx5d3359g")))

(define-public crate-usbd-hid-0.3.4 (c (n "usbd-hid") (v "0.3.4") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.2.4") (d #t) (k 0)))) (h "03rm1yjsqmrpzrf5c88xnyr3gdzr7ksqaidm23cz1ddf1vcy0d2b")))

(define-public crate-usbd-hid-0.3.5 (c (n "usbd-hid") (v "0.3.5") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.2.5") (d #t) (k 0)))) (h "1rnyiclq83v22hqiqs56hg27s1rf78kvk16qpl7z3pi9qsgziy0m")))

(define-public crate-usbd-hid-0.3.6 (c (n "usbd-hid") (v "0.3.6") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.2.6") (d #t) (k 0)))) (h "08a8ng66rfqnvnj0skgk5sp4w9n7mrsn0xgxlkyg1znmbm2cv3ls")))

(define-public crate-usbd-hid-0.3.7 (c (n "usbd-hid") (v "0.3.7") (d (list (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.2.6") (d #t) (k 0)))) (h "0xm9iccn0b14l5hlrac0m9fa351dpnx9h3dngcay1p2sxy47pwdx")))

(define-public crate-usbd-hid-0.4.0 (c (n "usbd-hid") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.4.0") (d #t) (k 0)))) (h "1p5ngqlr1zzpn53yla6j4xmnpmp8aivcaxk1ryfpwjjc5pf7djbg")))

(define-public crate-usbd-hid-0.4.1 (c (n "usbd-hid") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.4.1") (d #t) (k 0)))) (h "0scfyicbc7s722qv51nva0dq8f874c6gkb9isvxqg05bxcp2lhxq")))

(define-public crate-usbd-hid-0.4.2 (c (n "usbd-hid") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.4.2") (d #t) (k 0)))) (h "15h0j5905l2vymrpgq4v4wpqgy6s2fb2q2lpsczh7pi4j0ni6xwh")))

(define-public crate-usbd-hid-0.4.3 (c (n "usbd-hid") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.4.3") (d #t) (k 0)))) (h "19iwpxbcw7rkwzczw9pgzf965sr2h8ikri2b99k8r9xlgsbyc5js")))

(define-public crate-usbd-hid-0.4.4 (c (n "usbd-hid") (v "0.4.4") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.4.4") (d #t) (k 0)))) (h "1c152fybiwfm04qbs1m29vnf3bs0cpbnacj66a1an0wr8r97hzgd")))

(define-public crate-usbd-hid-0.4.5 (c (n "usbd-hid") (v "0.4.5") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.4.5") (d #t) (k 0)))) (h "1kf72c0fnhnkvbz6vxicdr9kkwy6vii3x378v29a1mm4csxlqh73")))

(define-public crate-usbd-hid-0.5.0 (c (n "usbd-hid") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.5.0") (d #t) (k 0)))) (h "00ny1v13h99ighxsxalvkpwzyz3g31xbwr3vsygnvmn0pvpwbc5s")))

(define-public crate-usbd-hid-0.5.1 (c (n "usbd-hid") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.5.1") (d #t) (k 0)))) (h "1brv3lag52830w0mbdc0ms8v378s7hy1b5i4ddqcfqf34k2gbq1x")))

(define-public crate-usbd-hid-0.5.2 (c (n "usbd-hid") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.5.2") (d #t) (k 0)))) (h "0zsx3lz51s2ry50zwycb27ilmiwljflfs395vyp14mfppdjpwfqq")))

(define-public crate-usbd-hid-0.6.0 (c (n "usbd-hid") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.5.2") (d #t) (k 0)))) (h "1p91hcd3mx9rpga9m53mim450wqd287h715v38v17mwardck7ar8")))

(define-public crate-usbd-hid-0.6.1 (c (n "usbd-hid") (v "0.6.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.6.0") (d #t) (k 0)))) (h "0ch1znj9jaac9g80ilbfbk9c8izs4hm9j2gaa5krhfd9yh8x8nwp") (s 2) (e (quote (("defmt" "dep:defmt" "usb-device/defmt"))))))

(define-public crate-usbd-hid-0.7.0 (c (n "usbd-hid") (v "0.7.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "ssmarshal") (r "^1.0") (k 0)) (d (n "usb-device") (r "^0.3.0") (d #t) (k 0)) (d (n "usbd-hid-macros") (r "^0.6.0") (d #t) (k 0)))) (h "0mkjzwybsdgbfavxcv2xqczbzslzyb5fb12mm33abrm3diad98j1") (s 2) (e (quote (("defmt" "dep:defmt" "usb-device/defmt"))))))

