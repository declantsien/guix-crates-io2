(define-module (crates-io us bd usbd-serial) #:use-module (crates-io))

(define-public crate-usbd-serial-0.1.0 (c (n "usbd-serial") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.1") (d #t) (k 0)))) (h "17pnnx12b49hcfbksav6dv64yr6fbbsd7cdcra2nypn29fz52l5l")))

(define-public crate-usbd-serial-0.1.1 (c (n "usbd-serial") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.7") (d #t) (k 0)))) (h "1zhxksam5kngqh574fwzzr4r30yc9v7wfwfiy3f14zr8hsdm2xfv")))

(define-public crate-usbd-serial-0.2.0 (c (n "usbd-serial") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "usb-device") (r "^0.3") (d #t) (k 0)))) (h "1gxirdl11a5xd6bb0q9yqx1xc79yk7isz4vslxhvqsm2avdgafn7")))

(define-public crate-usbd-serial-0.2.1 (c (n "usbd-serial") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-io") (r "^0.6") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "usb-device") (r "^0.3") (d #t) (k 0)))) (h "14ppwhy6b48c1qv0sc90h657lj95wzgr1wdf6kxg8za1n1pglcy2")))

(define-public crate-usbd-serial-0.2.2 (c (n "usbd-serial") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-io") (r "^0.6") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "usb-device") (r "^0.3") (d #t) (k 0)))) (h "1z40rzqq68wp9d9qjzzs82v4qcfsz3wcxnc2mjnxb0fvjfplwph6")))

