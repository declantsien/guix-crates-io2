(define-module (crates-io us bd usbd-hid-descriptors) #:use-module (crates-io))

(define-public crate-usbd-hid-descriptors-0.1.0 (c (n "usbd-hid-descriptors") (v "0.1.0") (d (list (d (n "bitfield") (r "~0.13") (d #t) (k 0)))) (h "0dzhshlwhh4690as8r7q4n1mp2nmhwsc7p7l68an6lq6y847dl5r")))

(define-public crate-usbd-hid-descriptors-0.1.1 (c (n "usbd-hid-descriptors") (v "0.1.1") (d (list (d (n "bitfield") (r "~0.13") (d #t) (k 0)))) (h "0r3davwkbzpiid0l3297q0d0xrzq4hl2nv5sgaaz7jir1c64604y")))

(define-public crate-usbd-hid-descriptors-0.1.2 (c (n "usbd-hid-descriptors") (v "0.1.2") (d (list (d (n "bitfield") (r "~0.13") (d #t) (k 0)))) (h "1l9xi9n1xpf4bphmd201ar93rz8i3v20nxq4p97qk42yfg3fignw")))

