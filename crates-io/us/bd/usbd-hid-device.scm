(define-module (crates-io us bd usbd-hid-device) #:use-module (crates-io))

(define-public crate-usbd-hid-device-0.1.0 (c (n "usbd-hid-device") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "stm32f3xx-hal") (r "^0.4.0") (f (quote ("stm32f303xc" "rt" "stm32-usbd"))) (d #t) (k 2)) (d (n "usb-device") (r "^0.2.3") (d #t) (k 0)))) (h "0g71wlr1ydql33hslqpk08g3f74lnf5p9f0bxjc66hdsxc0881wd")))

(define-public crate-usbd-hid-device-0.1.1 (c (n "usbd-hid-device") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "stm32f3xx-hal") (r "^0.4.0") (f (quote ("stm32f303xc" "rt" "stm32-usbd"))) (d #t) (k 2)) (d (n "usb-device") (r "^0.2.3") (d #t) (k 0)))) (h "1pcqdbdz6nx6d2dxm359xbfh7yj1lsrzcjj0zim113nn35qqlid6")))

