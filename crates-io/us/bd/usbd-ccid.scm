(define-module (crates-io us bd usbd-ccid) #:use-module (crates-io))

(define-public crate-usbd-ccid-0.0.0-unreleased (c (n "usbd-ccid") (v "0.0.0-unreleased") (h "0llfvhfkrhmhvfwzjm1i2kl89fsj8cac9x6wysbjyra2x1m5fkd0")))

(define-public crate-usbd-ccid-0.1.0 (c (n "usbd-ccid") (v "0.1.0") (d (list (d (n "delog") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "interchange") (r "^0.2") (d #t) (k 0)) (d (n "iso7816") (r "^0.1") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.3") (f (quote ("control-buffer-256"))) (d #t) (k 0)))) (h "0f8qnf8gxh3hzpd4nr4m11s0sw9z156ai54gvlfl25wmc6vc3fz2") (f (quote (("log-warn") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("highspeed-usb") ("default"))))))

(define-public crate-usbd-ccid-0.2.0 (c (n "usbd-ccid") (v "0.2.0") (d (list (d (n "delog") (r "^0.1.0") (d #t) (k 0)) (d (n "embedded-time") (r "^0.12") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "interchange") (r "^0.2") (d #t) (k 0)) (d (n "iso7816") (r "^0.1") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.3") (f (quote ("control-buffer-256"))) (d #t) (k 0)))) (h "1120jpiy401b5w2wng5zc83zw51xglpzrc94v663bng62aralp45") (f (quote (("log-warn") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("highspeed-usb") ("default"))))))

