(define-module (crates-io us bd usbd-ms) #:use-module (crates-io))

(define-public crate-usbd-ms-0.0.1 (c (n "usbd-ms") (v "0.0.1") (h "163lzga7wn0qq2npd10jzasgvqqhdw3wr1i0w3qg7iyjf1s2m9nm") (y #t)))

(define-public crate-usbd-ms-0.0.2 (c (n "usbd-ms") (v "0.0.2") (h "03a3m6i5d1cbhgmscia67g8757aaxqjpkskavgxvgkcsdqlh2q0s")))

