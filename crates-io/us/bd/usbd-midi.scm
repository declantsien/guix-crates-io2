(define-module (crates-io us bd usbd-midi) #:use-module (crates-io))

(define-public crate-usbd-midi-0.1.0 (c (n "usbd-midi") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.3") (d #t) (k 0)))) (h "034j392ly1bikq5nakpgwrsxx45vla4fd5q9vj9fqb4s5vcb85w1")))

(define-public crate-usbd-midi-0.2.0 (c (n "usbd-midi") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.3") (d #t) (k 0)))) (h "0k4zw3bxgm83z1mm1v7wll34w9kscq05hrp1b0rnh107gbm40c8z")))

(define-public crate-usbd-midi-0.3.0 (c (n "usbd-midi") (v "0.3.0") (d (list (d (n "num_enum") (r "^0.7.2") (k 0)) (d (n "usb-device") (r "^0.3") (d #t) (k 0)))) (h "1p48bngb5jsnns4p7a1g8xcq1860hgd4sg7wisx2aci8wpczxdjz")))

