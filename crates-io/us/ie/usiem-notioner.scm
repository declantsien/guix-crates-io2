(define-module (crates-io us ie usiem-notioner) #:use-module (crates-io))

(define-public crate-usiem-notioner-0.0.1 (c (n "usiem-notioner") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0") (d #t) (k 0)))) (h "1nhgfpjrm5fiqymydn2887gvrnsbnbj3ivs6l05r38gsfj1cpxbx")))

(define-public crate-usiem-notioner-0.0.2 (c (n "usiem-notioner") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "u-siem") (r "^0") (d #t) (k 0)))) (h "0vqqn25fpa6vpb669g2rf5y04h5c36y07szf8n2xdjifylsamhh7")))

