(define-module (crates-io us ie usiem-utils) #:use-module (crates-io))

(define-public crate-usiem-utils-0.1.0 (c (n "usiem-utils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "macros"))) (d #t) (k 0)) (d (n "u-siem") (r "^0") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)))) (h "1j6crj2zgy4ky4d9lw2dz34hwxpf7nk9wznyx5navdp8g3j3bvm7") (f (quote (("slow_geoip" "u-siem/slow_geoip") ("default"))))))

