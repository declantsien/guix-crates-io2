(define-module (crates-io us el uselog-rs) #:use-module (crates-io))

(define-public crate-uselog-rs-0.1.0 (c (n "uselog-rs") (v "0.1.0") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "1a9np6bsic0fm86hs9pi335f0wgk7qvn0z8nhqkq0568r7ccya6x")))

(define-public crate-uselog-rs-0.2.0 (c (n "uselog-rs") (v "0.2.0") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "02vk1bmysc89zk0xg7nvg7bhsjc39w3pzj4kg98dd3cr8s2iqlzg")))

(define-public crate-uselog-rs-0.3.0 (c (n "uselog-rs") (v "0.3.0") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "0wwghrfvh3dq2wd82akh5jhxqzm3vnfrk23qhv55irqzan5dgvc6")))

