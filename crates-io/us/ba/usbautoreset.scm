(define-module (crates-io us ba usbautoreset) #:use-module (crates-io))

(define-public crate-usbautoreset-0.1.0 (c (n "usbautoreset") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1v0r2vsrbi9i8bxk3982v7026ms4cjhv66xn66qzc4vwj8jk3zfa")))

(define-public crate-usbautoreset-0.1.1 (c (n "usbautoreset") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1w033f8h9b0x28bhxn7qarl5rjib3yz22llsyvawxzmp7kvmvxks")))

(define-public crate-usbautoreset-0.1.2 (c (n "usbautoreset") (v "0.1.2") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1jfi05n678d6incnh9zl4jzqxvxxx8hkqy9027asng6gfiy7mlij")))

(define-public crate-usbautoreset-0.1.3 (c (n "usbautoreset") (v "0.1.3") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1jh2nww9iqjp0gk454kgcl73rfb3ifxf79hlsx7r718vjqbhg8l4")))

