(define-module (crates-io us nr usnrs) #:use-module (crates-io))

(define-public crate-usnrs-0.1.0 (c (n "usnrs") (v "0.1.0") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "mft") (r "^0.6.1") (d #t) (k 0)))) (h "1bj75pcwc21b1b8ygf4qhvmgcs4n9gq2cdymmm9am1g4qnhw37ym") (f (quote (("usnrs-cli" "clap"))))))

