(define-module (crates-io us be usbenum) #:use-module (crates-io))

(define-public crate-usbenum-0.1.0 (c (n "usbenum") (v "0.1.0") (d (list (d (n "core-foundation") (r "^0.6") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "io-kit-sys") (r "^0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "mach") (r "^0.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "serde") (r "^1.0.128") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "udev") (r "^0.6") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("setupapi" "impl-default"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "166sbh3y51kydncgwkp2n1yz11bgxmhciqz51lspblj4lkfryl1c")))

