(define-module (crates-io us tu ustulation-test1) #:use-module (crates-io))

(define-public crate-ustulation-test1-0.1.0 (c (n "ustulation-test1") (v "0.1.0") (d (list (d (n "ustulation-test0") (r "= 0.1.0") (d #t) (k 0)))) (h "09dwmc2znpnrh2zhn9d8k3xpvlfj37l9y28ldccv740xdfm7cz68")))

(define-public crate-ustulation-test1-0.2.0 (c (n "ustulation-test1") (v "0.2.0") (d (list (d (n "ustulation-test0") (r "= 0.1.0") (d #t) (k 0)))) (h "1dnp5dcvpim54gv2axxg8jlhqmp3qw2259cmdv9jcmjy5fd21ixw")))

