(define-module (crates-io us iz usize_cast) #:use-module (crates-io))

(define-public crate-usize_cast-1.0.0 (c (n "usize_cast") (v "1.0.0") (h "0hlz49vanysx3nxb563gbvqahlgj527wa79n6a893pvcm88pm7d3")))

(define-public crate-usize_cast-1.1.0 (c (n "usize_cast") (v "1.1.0") (h "0lcp7y70b0y3hfr0myk55n00wjb6minslc8k5any6isim2c3l3w1")))

