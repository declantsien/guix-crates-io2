(define-module (crates-io us iz usize_conversions) #:use-module (crates-io))

(define-public crate-usize_conversions-0.1.0 (c (n "usize_conversions") (v "0.1.0") (h "0nlrp42zndnlllqm0aclr0kmlsvgmd8r8gn2zsm2al82569088h1")))

(define-public crate-usize_conversions-0.2.0 (c (n "usize_conversions") (v "0.2.0") (h "1r95im6miszgiyhs5dgd3179mk9l1kaalb8ilnbnqpg4rgi2j0zp")))

