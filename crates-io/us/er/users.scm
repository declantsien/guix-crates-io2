(define-module (crates-io us er users) #:use-module (crates-io))

(define-public crate-users-0.1.0 (c (n "users") (v "0.1.0") (h "127y18a6pg18lpnch37d413i3yjfvwhs4sapkmdz9wkzjk4x3y68")))

(define-public crate-users-0.1.1 (c (n "users") (v "0.1.1") (h "1h2bz8rknj6m6x4lbc481z6q2ri9aghjzbrihgxr9hnrdlcmc5hv")))

(define-public crate-users-0.2.0 (c (n "users") (v "0.2.0") (h "1kb4hjigc2wic408l8rb5fdjq6nrzmmwkqsafmdjb2333phbajsn")))

(define-public crate-users-0.2.1 (c (n "users") (v "0.2.1") (h "1rikn2d7q8302w2va2f0jrrs42iyr3l7p3llbfr0zd4gw7p3rg6n")))

(define-public crate-users-0.2.2 (c (n "users") (v "0.2.2") (h "0g6584121zgjhx0x95l0mbld3znxkp56fzqc3hkkdmnxrdk2r50j")))

(define-public crate-users-0.2.3 (c (n "users") (v "0.2.3") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "08w9gsvhkf68nv1walvlzalrc18p7jz2gd879fwm2dkx2qmqjx6d")))

(define-public crate-users-0.3.0 (c (n "users") (v "0.3.0") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "0wi5z72z4jj4l5p1wasqygi90cqlbrygalq9w4jh4gjlvgd61v6f")))

(define-public crate-users-0.3.1 (c (n "users") (v "0.3.1") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "1rmwbvdkcc51dl6zgak73brq42w1zj856x7r6z7pmqwyhc6zjf9d")))

(define-public crate-users-0.3.2 (c (n "users") (v "0.3.2") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "1lca21rif3qmgpbv9z4c1vxabphwmxhkggj63fy4fkhlmzx76phq")))

(define-public crate-users-0.4.0 (c (n "users") (v "0.4.0") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "0znl8a9na97g004s7nkjslczp56a1w1s3lma496bv22xrcmw6742")))

(define-public crate-users-0.4.1 (c (n "users") (v "0.4.1") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "1asx3hlp2ayvhilg04g057g22j65byp7pizsmjngzgh685pn1j05")))

(define-public crate-users-0.4.2 (c (n "users") (v "0.4.2") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "1ad6206fvxj2g9aks2wxyg2q6g9dqpp2mxwkgpl1si1w2d2s7gr2")))

(define-public crate-users-0.4.3 (c (n "users") (v "0.4.3") (d (list (d (n "libc") (r "^0.1.1") (d #t) (k 0)))) (h "0zifp1yjysyhb55plfp1v7qvdahcb106p4hzagq9yv2bm2ax828l")))

(define-public crate-users-0.4.4 (c (n "users") (v "0.4.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a2qjfazr2v5qnqys37f66gz6hq4r1c238098ynhv0vibcsha8w0")))

(define-public crate-users-0.5.0 (c (n "users") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qm3pl9v5wwzhhlnx1jcgb4xlkglcxsfzflc2a4msjh644c9siiq")))

(define-public crate-users-0.5.1 (c (n "users") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wf92pc799w5p8x8r0paj274f9zyimrk22vfiwrfcmvadbr3zmyn")))

(define-public crate-users-0.5.2 (c (n "users") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z2clz5xxxihg676p7klrhpbkzj8jr2qd66914hnbf9wg3gqzbm7")))

(define-public crate-users-0.5.3 (c (n "users") (v "0.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1psw930gzrm5vk4ldwzk4ndbwpyvivq3kki5iahydq3wy4bgpn77")))

(define-public crate-users-0.6.0 (c (n "users") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nm6pkmixdwdppchvsivshh4vs48l58v1a54gpspb7zwmx9ipawr")))

(define-public crate-users-0.6.1 (c (n "users") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rmsjiy6n0r1xmvbcipxbxl3pi0qhc449xzq1nz5p5kzccvdi650")))

(define-public crate-users-0.7.0 (c (n "users") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1prr84zk82ynnk7rx26ky1ilczl2d5firm1m58nax9hhrh7pd8na")))

(define-public crate-users-0.8.0 (c (n "users") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mxn4r7bs827hhl53rnalqi0dmgvkal0z2ig80dpxrz5b9i80g5v") (f (quote (("mock") ("default" "cache" "mock") ("cache"))))))

(define-public crate-users-0.8.1 (c (n "users") (v "0.8.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dss2l4x3zgjq26mwa97aa5xmsb5z2x3vhhhh3w3azan284pvvbz") (f (quote (("mock") ("default" "cache" "mock") ("cache"))))))

(define-public crate-users-0.9.0 (c (n "users") (v "0.9.0") (d (list (d (n "libc") (r "^0.2.50") (d #t) (k 0)))) (h "1w7ys3z8cj1r8cfvlkprsv35zifqpl89n693a89x3mgrhmbkmd8h") (f (quote (("mock") ("default" "cache" "mock") ("cache"))))))

(define-public crate-users-0.9.1 (c (n "users") (v "0.9.1") (d (list (d (n "libc") (r "^0.2.50") (d #t) (k 0)))) (h "1kxl3y2hcrqqip7jpqn5mz7xlpbwmmpfmaza0xnyrhx0mrkl4by7") (f (quote (("mock") ("default" "cache" "mock") ("cache"))))))

(define-public crate-users-0.10.0 (c (n "users") (v "0.10.0") (d (list (d (n "env_logger") (r "^0.7") (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "11plda5r3dl8hs0sl0jskazam4ayv3a06vmhzk4l7914agljfhma") (f (quote (("mock") ("logging" "log") ("default" "cache" "mock" "logging") ("cache"))))))

(define-public crate-users-0.11.0 (c (n "users") (v "0.11.0") (d (list (d (n "env_logger") (r "^0.7") (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)))) (h "0cmhafhhka2yya66yrprlv33kg7rm1xh1pyalbjp6yr6dxnhzk14") (f (quote (("mock") ("logging" "log") ("default" "cache" "mock" "logging") ("cache"))))))

