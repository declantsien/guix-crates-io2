(define-module (crates-io us er user_utils) #:use-module (crates-io))

(define-public crate-user_utils-0.1.0 (c (n "user_utils") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_Security_Authorization" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0wx1ghbgax22kwjvjbsxis9m8sdmiqw0fr9ah90481pa9cdmlg44") (r "1.70.0")))

(define-public crate-user_utils-0.2.0 (c (n "user_utils") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_Security_Authorization" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0m3wjdsdli55xiqjp53w3mw1s310qz2hjkj5pkfy77zgzqn6sia6") (r "1.70.0")))

