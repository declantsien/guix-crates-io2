(define-module (crates-io us er user_doc) #:use-module (crates-io))

(define-public crate-user_doc-0.0.1 (c (n "user_doc") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "doc_data") (r "^0.0.1") (d #t) (k 0) (p "user_doc-doc_data")) (d (n "doc_proc_macro") (r "^0.0.1") (d #t) (k 0) (p "user_doc-doc_proc_macro")))) (h "15p6bqahiy25585s8mdwaxa1gr20xb1s7xw8n9f768ig8siglnhz")))

(define-public crate-user_doc-0.1.0 (c (n "user_doc") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "doc_data") (r "^0.1.0") (d #t) (k 0) (p "user_doc-doc_data")) (d (n "doc_proc_macro") (r "^0.1.0") (d #t) (k 0) (p "user_doc-doc_proc_macro")))) (h "1f1v49vv6gp8jqh2nvnqfrpk3gdy0phmlngw98r0j54p8f2c0iiz")))

(define-public crate-user_doc-0.1.2 (c (n "user_doc") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "doc_data") (r "^0.1.1") (d #t) (k 0) (p "user_doc-doc_data")) (d (n "doc_proc_macro") (r "^0.1.1") (d #t) (k 0) (p "user_doc-doc_proc_macro")))) (h "03kdwzz3gf5mkjf93930li19dmkq1z9v87c44h1b7sjh4c510r5k")))

(define-public crate-user_doc-1.0.3 (c (n "user_doc") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "doc_data") (r "^1.0.2") (d #t) (k 0) (p "user_doc-doc_data")) (d (n "doc_proc_macro") (r "^1.0.2") (d #t) (k 0) (p "user_doc-doc_proc_macro")))) (h "0riqjif03xsj497ynk3yjd3651662mxrjpwc8rqg2asd1vxz3h67")))

