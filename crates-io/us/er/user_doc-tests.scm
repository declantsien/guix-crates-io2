(define-module (crates-io us er user_doc-tests) #:use-module (crates-io))

(define-public crate-user_doc-tests-0.0.1 (c (n "user_doc-tests") (v "0.0.1") (d (list (d (n "doc_data") (r "^0.0.1") (d #t) (k 0) (p "user_doc-doc_data")) (d (n "user_doc") (r "^0.0.1") (d #t) (k 0) (p "user_doc")))) (h "1km2img37sz5swq1dy6dh48ns0533n7qqbwanwjh8v9fv9h1c02x")))

(define-public crate-user_doc-tests-0.1.0 (c (n "user_doc-tests") (v "0.1.0") (d (list (d (n "doc_data") (r "^0.1.0") (d #t) (k 0) (p "user_doc-doc_data")) (d (n "user_doc") (r "^0.1.0") (d #t) (k 0) (p "user_doc")))) (h "1wcsff0n5fahk9h7634w9qrfg9m10w62ikrv7i24xibrxycy757m")))

(define-public crate-user_doc-tests-0.1.2 (c (n "user_doc-tests") (v "0.1.2") (d (list (d (n "doc_data") (r "^0.1.1") (d #t) (k 0) (p "user_doc-doc_data")) (d (n "user_doc") (r "^0.1.1") (d #t) (k 0) (p "user_doc")))) (h "08hkz1gssdf4a6397w1y7sxpafk4p5b44x2ys0jvpw4rzvr6bbsq")))

(define-public crate-user_doc-tests-1.0.3 (c (n "user_doc-tests") (v "1.0.3") (d (list (d (n "doc_data") (r "^1.0.2") (d #t) (k 0) (p "user_doc-doc_data")) (d (n "user_doc") (r "^1.0.2") (d #t) (k 0) (p "user_doc")))) (h "0y4qnnr1vi7398n933zib3jiwla1ppkzdknadqpbxnax73z6aahr")))

