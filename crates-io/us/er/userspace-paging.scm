(define-module (crates-io us er userspace-paging) #:use-module (crates-io))

(define-public crate-userspace-paging-0.0.1 (c (n "userspace-paging") (v "0.0.1") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("mman" "signal" "feature"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1yr2f732cnmhpk1k5abp6q0sgb0wx25iyyg8pcllqfzxx7aj81ih")))

