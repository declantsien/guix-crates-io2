(define-module (crates-io us er user-validator) #:use-module (crates-io))

(define-public crate-user-validator-0.1.0 (c (n "user-validator") (v "0.1.0") (d (list (d (n "actix-session") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1md1z9j9ffkrihlnb1vpvnvcws45dj3qg5i911fym0q2mrab43np") (y #t)))

(define-public crate-user-validator-0.1.1 (c (n "user-validator") (v "0.1.1") (d (list (d (n "actix-session") (r "^0.4.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "mongodb") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0z92jf2rklh452fpga6zvj3mlk7s2cshhqbma2rqdcn4wfdq61ii") (y #t)))

