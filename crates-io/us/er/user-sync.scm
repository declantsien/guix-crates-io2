(define-module (crates-io us er user-sync) #:use-module (crates-io))

(define-public crate-user-sync-0.1.0 (c (n "user-sync") (v "0.1.0") (d (list (d (n "syscall") (r "0.2.*") (d #t) (k 0)))) (h "1c7hrg816gf376ny1yxvsjn91g9p93qs5ncax5hghrb532hgwgdw")))

(define-public crate-user-sync-0.1.1 (c (n "user-sync") (v "0.1.1") (d (list (d (n "system-call") (r "^0.1") (d #t) (k 0)))) (h "19ilfjwv2bb7hqgyvg1c7dn6kdh5qhjr8746r805i7mah17840w8")))

