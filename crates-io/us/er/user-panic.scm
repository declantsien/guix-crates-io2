(define-module (crates-io us er user-panic) #:use-module (crates-io))

(define-public crate-user-panic-0.1.0 (c (n "user-panic") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1ncdfqy8hpcv1l1538vzhbgndk2mjzxi7zymfb9acficmjxz1qdz")))

