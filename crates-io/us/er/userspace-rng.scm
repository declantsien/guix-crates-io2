(define-module (crates-io us er userspace-rng) #:use-module (crates-io))

(define-public crate-userspace-rng-0.1.0 (c (n "userspace-rng") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "00x04hvsyf51j3az0zkqshizvnyxvr8ssz3vsp43nc5bh3gb3pwp")))

(define-public crate-userspace-rng-0.1.2 (c (n "userspace-rng") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "156c8v6jbsax2pfxscah61691z72csszdzx76mkc2wkh0sjkn68h")))

(define-public crate-userspace-rng-0.1.3 (c (n "userspace-rng") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1j8843h2l6x8irjqx86vfm2kz1qi7svcyc1f5pyas19dzr5vwgb8")))

(define-public crate-userspace-rng-0.1.4 (c (n "userspace-rng") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1vswz4qn5sm2kl4gmk1sbjgbw1h8kjvvh5y8px7f2vzillwz0r5q")))

(define-public crate-userspace-rng-0.1.5 (c (n "userspace-rng") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)))) (h "0skynwkqmqfrp6vhh062rbi48vq2k1lkiazf9jdg97j8lxjf8wc0") (y #t)))

(define-public crate-userspace-rng-0.1.6 (c (n "userspace-rng") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)))) (h "1cd7zq777z11im8j27p4895y1rlnilj0z3zb14gr868wjiswajm5") (y #t)))

(define-public crate-userspace-rng-1.0.0 (c (n "userspace-rng") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)))) (h "17rl8by2apf4y410jy1wsch51gwiwnvcv1n443dyy2153f2d5h8h") (y #t)))

(define-public crate-userspace-rng-1.0.1 (c (n "userspace-rng") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)))) (h "1fqrnhlkjmgywng96l5lzry8ya2yzb6d23zlkn00ly911ch7z9d3") (y #t)))

(define-public crate-userspace-rng-1.0.2 (c (n "userspace-rng") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)))) (h "0j61xjvr72hb6w0qfg8v14gd1qjwsag3gjqw80ykr3y8vgnxrb2r")))

(define-public crate-userspace-rng-1.0.3 (c (n "userspace-rng") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js" "std"))) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)))) (h "0bsn27qxw0jxvksr0pv6p463y310ms33fn981cx69v7kr6b1m4cn")))

