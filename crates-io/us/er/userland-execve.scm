(define-module (crates-io us er userland-execve) #:use-module (crates-io))

(define-public crate-userland-execve-0.1.0 (c (n "userland-execve") (v "0.1.0") (d (list (d (n "goblin") (r "^0.7.1") (f (quote ("elf32" "elf64" "endian_fd"))) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("mman" "feature" "user"))) (k 0)))) (h "1q10166bdc0y501k55ws6c4akacv9a1k81rzqrpynzvlldfvscz2")))

(define-public crate-userland-execve-0.1.1 (c (n "userland-execve") (v "0.1.1") (d (list (d (n "goblin") (r "^0.7.1") (f (quote ("elf32" "elf64" "endian_fd"))) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("mman" "feature" "user"))) (k 0)))) (h "1r278c2qhb3alm6za6zf1v03i182rfva8qwdn9i734i80yj1q136")))

(define-public crate-userland-execve-0.1.2 (c (n "userland-execve") (v "0.1.2") (d (list (d (n "goblin") (r "^0.7.1") (f (quote ("elf32" "elf64" "endian_fd"))) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("mman" "feature" "user"))) (k 0)))) (h "0djgw5pvbp6780d1kr5f5a0ynxs3c82k7sd1213afryf05s2y95d")))

(define-public crate-userland-execve-0.2.0 (c (n "userland-execve") (v "0.2.0") (d (list (d (n "goblin") (r "^0.8.0") (f (quote ("elf32" "elf64" "endian_fd"))) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("mman" "feature" "user"))) (k 0)))) (h "1fk1igw3mya5jx2gjiqssz12qqbf3l8zq81bzvp216bm5v598lza")))

