(define-module (crates-io us er user-agent-parser) #:use-module (crates-io))

(define-public crate-user-agent-parser-0.1.0 (c (n "user-agent-parser") (v "0.1.0") (d (list (d (n "onig") (r "^4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1mx144129nkc6wn9dsslkvqhk6qr19asfk75yj4vvywyx32j4x7h")))

(define-public crate-user-agent-parser-0.2.0 (c (n "user-agent-parser") (v "0.2.0") (d (list (d (n "onig") (r "^4") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0gzb2lv68pv394ri9fanszwj5jmi4cgfgvd7rfp9b6aml3ax0g6z") (f (quote (("rocketly" "rocket"))))))

(define-public crate-user-agent-parser-0.2.1 (c (n "user-agent-parser") (v "0.2.1") (d (list (d (n "onig") (r "^5") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1fy95rbjzskn7xvb303s6kxdz5c6rvcidprnd9jdv9bq7cs3aca8") (f (quote (("rocketly" "rocket"))))))

(define-public crate-user-agent-parser-0.2.2 (c (n "user-agent-parser") (v "0.2.2") (d (list (d (n "onig") (r "^5") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0l758wc3xp969kkpjm9yz10fqr798170pay6cxsgz7qyirzssc1z") (f (quote (("rocketly" "rocket"))))))

(define-public crate-user-agent-parser-0.2.3 (c (n "user-agent-parser") (v "0.2.3") (d (list (d (n "onig") (r "^5") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "090d4clzl6f2n7md9q89fazjx5yvb860g8drrr9r36rnbsppaxs3") (f (quote (("rocketly" "rocket"))))))

(define-public crate-user-agent-parser-0.2.4 (c (n "user-agent-parser") (v "0.2.4") (d (list (d (n "onig") (r "^5") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "05i7098cvr4q3p0z51hcvp2nwsj3a4226fkwc4dwfmdjqyvpx6p7") (f (quote (("rocketly" "rocket"))))))

(define-public crate-user-agent-parser-0.2.5 (c (n "user-agent-parser") (v "0.2.5") (d (list (d (n "onig") (r "^5") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1jr5vci9z9w4lixnd0mvy5hyr56fvig0fn2bk60f0nfq88hib5wx") (f (quote (("rocketly" "rocket"))))))

(define-public crate-user-agent-parser-0.2.6 (c (n "user-agent-parser") (v "0.2.6") (d (list (d (n "onig") (r "^5") (d #t) (k 0)) (d (n "rocket") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1kjcd2jqbly0ck772rp29m64dsj2iha6l6x1mnms5i02drj03ps4") (f (quote (("rocketly" "rocket"))))))

(define-public crate-user-agent-parser-0.2.7 (c (n "user-agent-parser") (v "0.2.7") (d (list (d (n "onig") (r "^6") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0mphs2f90lqvqjhqjfr2p32p6kz4fbirddbd3cxywd6p9fnwzd9q") (f (quote (("rocketly" "rocket"))))))

(define-public crate-user-agent-parser-0.2.8 (c (n "user-agent-parser") (v "0.2.8") (d (list (d (n "onig") (r "^6") (d #t) (k 0)) (d (n "rocket") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0x5g49dpn1lx28dj4vg7cjy3fxllzq77kzl7qii1x7139r67bqf4")))

(define-public crate-user-agent-parser-0.3.0 (c (n "user-agent-parser") (v "0.3.0") (d (list (d (n "onig") (r "^6") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0xca3jxn9n2hkry2zdql1j7kmihpwd9gwdfnr1xsfqrjvfrly3lh")))

(define-public crate-user-agent-parser-0.3.1 (c (n "user-agent-parser") (v "0.3.1") (d (list (d (n "onig") (r "^6") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1mzawvva8xfdg90n6y9rpwsyibav29nyr7baswg2xbzf8wh6gif8")))

(define-public crate-user-agent-parser-0.3.2 (c (n "user-agent-parser") (v "0.3.2") (d (list (d (n "onig") (r "^6") (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1f2smhnk59ihs77fk5wds6b586h3nvp71xjml6wz86s4k9majgpg")))

(define-public crate-user-agent-parser-0.3.3 (c (n "user-agent-parser") (v "0.3.3") (d (list (d (n "onig") (r "^6") (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0vzvydmy78azba1bd7x9k0a2c8rp3wn03yivvv1xhqq4r7aamxaw")))

(define-public crate-user-agent-parser-0.3.4 (c (n "user-agent-parser") (v "0.3.4") (d (list (d (n "onig") (r "^6") (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0ljhm3bingp97i8hg7rcbg1lsfj5371wqqfcapqw9nwfmhvirp34")))

(define-public crate-user-agent-parser-0.3.5 (c (n "user-agent-parser") (v "0.3.5") (d (list (d (n "onig") (r "^6") (k 0)) (d (n "rocket") (r "^0.5.0-rc.3") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1jb6rvlqfbp9rfz053ysg8nij45gh5dygb1xp40g1vsdbc7611qq") (r "1.67")))

(define-public crate-user-agent-parser-0.3.6 (c (n "user-agent-parser") (v "0.3.6") (d (list (d (n "onig") (r "^6") (k 0)) (d (n "rocket") (r "^0.5.0-rc.4") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "163ypxgwszhaana0181zxc18zx7bjms27nrp8lv6afgj917zfcxn") (r "1.69")))

