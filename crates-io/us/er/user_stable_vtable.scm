(define-module (crates-io us er user_stable_vtable) #:use-module (crates-io))

(define-public crate-user_stable_vtable-0.1.0 (c (n "user_stable_vtable") (v "0.1.0") (h "1kwa7n82mcks56f0rr1f98b2161c8wl1zmjaz37xcalhn8vn9ilw") (f (quote (("default" "box") ("box" "alloc") ("alloc"))))))

(define-public crate-user_stable_vtable-0.2.0 (c (n "user_stable_vtable") (v "0.2.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "141fgcdcp5m67zvq46r1nafj4vvyrjhy97s07pfdycnfmswdzal6") (f (quote (("default" "box") ("box" "alloc") ("alloc"))))))

(define-public crate-user_stable_vtable-0.2.1 (c (n "user_stable_vtable") (v "0.2.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1c4d5x3myf4m5nwfdhzd6z9dlgi0062l5bnbwzpmvyb9g5d3ydyh") (f (quote (("default" "box") ("box" "alloc") ("alloc"))))))

(define-public crate-user_stable_vtable-0.3.0 (c (n "user_stable_vtable") (v "0.3.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "07jg9rq5afdw9gxzy9a9gmj0gcryl3fdc0d1jwwqnx1jkzyxb4yf") (f (quote (("default" "box") ("box" "alloc") ("alloc"))))))

