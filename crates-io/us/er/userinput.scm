(define-module (crates-io us er userinput) #:use-module (crates-io))

(define-public crate-userinput-0.1.0 (c (n "userinput") (v "0.1.0") (h "1c2cwclsnvxi9ff9hz4rj181isdnn5d4nz0fk0zy7skkn7ngifm3")))

(define-public crate-userinput-0.1.1 (c (n "userinput") (v "0.1.1") (h "1kqbm983j307ycwrmadpvy9g8n5z47161w82f17ic1ffmpzb1d1a")))

(define-public crate-userinput-0.1.2 (c (n "userinput") (v "0.1.2") (h "1gxwk9cnmprrwqcgmsasck9yrlsrfi92kmhnng593ss54zb9dhzm")))

(define-public crate-userinput-0.1.3 (c (n "userinput") (v "0.1.3") (h "0qsxp5cwv9jmalpcask364l888kc0c93wj47vwdvs5a353cn4jlj")))

(define-public crate-userinput-0.1.4 (c (n "userinput") (v "0.1.4") (h "1nrsxwwg26nia86ck63p6pikxq0if0ydhnxk7l98ni29r8xr626a")))

(define-public crate-userinput-0.1.5 (c (n "userinput") (v "0.1.5") (h "1p26jqp7ynp8gzc2c8srwlza7j1zzlz5mibs1b9kkhmziv78bcab")))

