(define-module (crates-io us er user_lookup) #:use-module (crates-io))

(define-public crate-user_lookup-0.2.0 (c (n "user_lookup") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "10gj9cr8vy7j4g5byqlpc04jq5rx9ss6ybkzvsqfvls5dcliql8g")))

(define-public crate-user_lookup-0.3.0 (c (n "user_lookup") (v "0.3.0") (d (list (d (n "tokio") (r "^1") (f (quote ("time" "fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "fs" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "02g24i8pf52cpv2dsrq23k8z08lrpha113brwljzbb6i215qn8ff") (f (quote (("sync") ("default" "sync" "async") ("async" "tokio"))))))

