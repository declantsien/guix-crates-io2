(define-module (crates-io us er user-backtrace) #:use-module (crates-io))

(define-public crate-user-backtrace-0.1.0 (c (n "user-backtrace") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0ng9nacvzihvxfplfzqba0c8dr7q29d77n2gw47ilprzhnnvw0b1")))

(define-public crate-user-backtrace-0.2.0 (c (n "user-backtrace") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "1x8jr25m413ghsr19hjb91irqa5d51322nhyrcwpm7425ay5v93a")))

(define-public crate-user-backtrace-0.2.1 (c (n "user-backtrace") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.66") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "07gqq5qn7h5cg8vf79ywf421p2dvr6yv9vxpdbaqy5q2cibc4ncv")))

(define-public crate-user-backtrace-0.2.2 (c (n "user-backtrace") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.66") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "1zpjljdcahkpylsq7czkbqz0wvfwg12gxxxpd8j70w8wyys8ch6i")))

