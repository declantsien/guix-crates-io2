(define-module (crates-io us er user_input_with_autocomplete) #:use-module (crates-io))

(define-public crate-user_input_with_autocomplete-0.1.1 (c (n "user_input_with_autocomplete") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "1hwxai93a7h4i8ni08xx6cj1h6yi9sa9yrg1xgpi5w30nzr6zabw")))

(define-public crate-user_input_with_autocomplete-0.1.2 (c (n "user_input_with_autocomplete") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "ureq") (r "^2.3.1") (d #t) (k 0)))) (h "1ay0njffb3rxvdrnw7m1rql7p3a21m6r40rwhppmxkh7w34bsisx")))

