(define-module (crates-io us er user) #:use-module (crates-io))

(define-public crate-user-0.1.0 (c (n "user") (v "0.1.0") (d (list (d (n "advapi32-sys") (r ">= 0.1.2") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.4") (d #t) (k 0)) (d (n "winapi") (r ">= 0.2.5") (d #t) (k 0)))) (h "0irhgc5blngp10kna0s15pq7dvk0wanns04b3xnbhghksp9rq7hi")))

(define-public crate-user-0.1.1 (c (n "user") (v "0.1.1") (d (list (d (n "advapi32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "199yh48620l3zkjbln9x6hihk52a93kxlwckaq4rzwcrqccv8v6g")))

