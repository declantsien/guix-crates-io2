(define-module (crates-io us er userenv-sys) #:use-module (crates-io))

(define-public crate-userenv-sys-0.0.1 (c (n "userenv-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0q0f9bvsfi9pjg8xkikghb2x8fcibc5miq1mzkwr4hal5rbmg63z")))

(define-public crate-userenv-sys-0.2.0 (c (n "userenv-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1wxj34cybdjkbnpas978mrp9invdz4wrpynrbgbr54dxdfiqxlki")))

