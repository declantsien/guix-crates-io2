(define-module (crates-io us er userland) #:use-module (crates-io))

(define-public crate-userland-0.1.1 (c (n "userland") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "maplit") (r "^0.1.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18984fpwx7pi4aicynfkglq1bxrcg4j2ix918z1yw8hh4kc0hlk5")))

