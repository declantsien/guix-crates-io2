(define-module (crates-io us er user-error) #:use-module (crates-io))

(define-public crate-user-error-1.0.0 (c (n "user-error") (v "1.0.0") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)))) (h "0r5503cnjb9f8l94sdyp8ak1ykypks9dnfh0kkdzvd29shhldhhh")))

(define-public crate-user-error-1.0.1 (c (n "user-error") (v "1.0.1") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)))) (h "027rn7w1jlr43dyvza1hmsw6cs0xcryj677vqhha4rq2p3c188yx")))

(define-public crate-user-error-1.0.2 (c (n "user-error") (v "1.0.2") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.19.0") (d #t) (k 0)))) (h "0p5xjp7rpazlvrncfz727935z9gfphkf5j4qjaj7b80kjsc71352")))

(define-public crate-user-error-1.0.3 (c (n "user-error") (v "1.0.3") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.19.0") (d #t) (k 0)) (d (n "scrawl") (r "^0.1.1") (d #t) (k 0)))) (h "0l46zza39x8s8472v8d8r6nq4r0c7yc8533fgvzllvfn0h8a6z6w")))

(define-public crate-user-error-1.0.4 (c (n "user-error") (v "1.0.4") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.19.0") (d #t) (k 0)) (d (n "scrawl") (r "^0.1.2") (d #t) (k 0)))) (h "0g7byw55lqf58i8si47n0dwjnbdb4p4p08l81bn2vpkxjify7qhd")))

(define-public crate-user-error-1.0.5 (c (n "user-error") (v "1.0.5") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.19.0") (d #t) (k 0)) (d (n "scrawl") (r "^0.9.0") (d #t) (k 0)))) (h "0sn71ibvcd71spljmahnpm0a2al4i7izcc1355r1jx0r3fhqb45i")))

(define-public crate-user-error-1.1.0 (c (n "user-error") (v "1.1.0") (h "04wncr2r67gd0391i1f7p2m53aqazvn24l4ph1idflq6l6p4qmif")))

(define-public crate-user-error-1.2.0 (c (n "user-error") (v "1.2.0") (h "0q501m0fc434ky22w0w69hbgybrzfg57lkfg3z17ngvwvb3nykvh")))

(define-public crate-user-error-1.2.1 (c (n "user-error") (v "1.2.1") (h "0z39vvw1j5lvjxg8a4cmvp72rgk3qbz33pq22gxyx0dvqs5sq5b6")))

(define-public crate-user-error-1.2.2 (c (n "user-error") (v "1.2.2") (h "1hn3vsm55s2xw5cnl4vlrhypvwg4i8xbh7hh20j280psj4p2b1z1")))

(define-public crate-user-error-1.2.3 (c (n "user-error") (v "1.2.3") (h "13c50yhkwm00d5df5zk5dcxc43j7apf7j22rq8h6z8s9n4i6dbm0")))

(define-public crate-user-error-1.2.4 (c (n "user-error") (v "1.2.4") (h "091jgv0xhh7lx8f71xg9yh9is8z77nbh31yachpjax2qx2djc1mm")))

(define-public crate-user-error-1.2.5 (c (n "user-error") (v "1.2.5") (d (list (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (d #t) (k 0)))) (h "1zdgcw7q35p57q1nckw3qxsbcgcf17v5hrlp5h9g6p1hq5v7kmx9")))

(define-public crate-user-error-1.2.6 (c (n "user-error") (v "1.2.6") (h "16qf0a532zkypqyx2l4wh8aczbczhvp0br3jfixf6zs3q8y34i76")))

(define-public crate-user-error-1.2.7 (c (n "user-error") (v "1.2.7") (h "1lxpvv2h6y9czikys9wcwi67jxzgqfvh1l3gvqi98ndpqq84rwqk")))

(define-public crate-user-error-1.2.8 (c (n "user-error") (v "1.2.8") (h "0qv8wgr6i5xwkqadhxfz6rnydkxcj4kjk0hwchns0qnq9031va87")))

