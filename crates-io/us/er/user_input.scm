(define-module (crates-io us er user_input) #:use-module (crates-io))

(define-public crate-user_input-0.1.0 (c (n "user_input") (v "0.1.0") (h "1fp1lyc92qs16r6jzwwm3qb6sjcjc12jnakff8igfn6ff6ilf18c")))

(define-public crate-user_input-0.1.1 (c (n "user_input") (v "0.1.1") (h "1lndnfzmgwbzqlbkbic5icg1cphpc4k99ij75cxpbyrkw74k3fcr")))

(define-public crate-user_input-0.1.2 (c (n "user_input") (v "0.1.2") (h "1z3ahw3flc2l36rq2s3lyk7a1zd99pf3jml5dfvjyzdjx41a6knd")))

