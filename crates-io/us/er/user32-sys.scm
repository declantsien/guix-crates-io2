(define-module (crates-io us er user32-sys) #:use-module (crates-io))

(define-public crate-user32-sys-0.0.1 (c (n "user32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0wqfsfwhqv7fwax1wj68zzmjfhp8nq098hk3vps7qmapd1qcz4ff")))

(define-public crate-user32-sys-0.0.2 (c (n "user32-sys") (v "0.0.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0x0g7pv32swjy1af4sx2yhcr1s09iygzyd3s0fn2ighdc3dy2n7j")))

(define-public crate-user32-sys-0.0.3 (c (n "user32-sys") (v "0.0.3") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0631q0xivlnl2rg0wd5hlnz4kzaif6acrng7m5c5wnxf36dnm0l3")))

(define-public crate-user32-sys-0.0.4 (c (n "user32-sys") (v "0.0.4") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "04d8ihn82dyxryb0jwf6ikggg4ypxwx500ylfdyzqxi7apgc7h5j")))

(define-public crate-user32-sys-0.0.5 (c (n "user32-sys") (v "0.0.5") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0nfc5wsz6f4k8xy3xdxhlqzvvfd9xpvwq9wsxwhyqn5mdd2pw5yl")))

(define-public crate-user32-sys-0.0.6 (c (n "user32-sys") (v "0.0.6") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "056hvy48g7hfs266x39254dxxhl6rfi71hinw98l4ly2c1l4j2y7")))

(define-public crate-user32-sys-0.0.7 (c (n "user32-sys") (v "0.0.7") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0wy72gpaa9iaahajz6slbr9sm2sdw8ivl12v2zn19v25pwbp665w")))

(define-public crate-user32-sys-0.0.8 (c (n "user32-sys") (v "0.0.8") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1kffd21aj02g315l25apk371zjvjyw3scgbq9wnglsnp76gyzwjq")))

(define-public crate-user32-sys-0.0.9 (c (n "user32-sys") (v "0.0.9") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "11pqnl37hijafnypq1cqvgr8ykm0p1gcly9jaf3hhzf0sc1y2pww")))

(define-public crate-user32-sys-0.0.10 (c (n "user32-sys") (v "0.0.10") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1i0m6rlqsnpv0nmzcl3fj67g1hjjcssfrwalg7i9jpcykh5fnpnl")))

(define-public crate-user32-sys-0.0.11 (c (n "user32-sys") (v "0.0.11") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1d1k5dwzk8h2qdffafi6y9rgysx21jh5xadzi00fcy99njy5j7hj")))

(define-public crate-user32-sys-0.1.0 (c (n "user32-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1nxlncgp5wz3vxawb399d0k0vjnihjwsn23vwkvndpv6zz853576")))

(define-public crate-user32-sys-0.1.1 (c (n "user32-sys") (v "0.1.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "0g9ai4kji1a320sffn1mglscg19iqp4chlh14k0p6g4j2ri53mwr")))

(define-public crate-user32-sys-0.1.2 (c (n "user32-sys") (v "0.1.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "012mrxqnwzxfkd7cjzv17pszkrkx1hfsay7w89b3y9dcwnfi45v7")))

(define-public crate-user32-sys-0.2.0 (c (n "user32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0ivxc7hmsxax9crdhxdd1nqwik4s9lhb2x59lc8b88bv20fp3x2f")))

(define-public crate-user32-sys-0.1.3 (c (n "user32-sys") (v "0.1.3") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "02pqzgm7qfdvlb0zqry98h76zwvaq19idd99i0ch8b4m7fc1kdz6")))

