(define-module (crates-io us dp usdpl-build) #:use-module (crates-io))

(define-public crate-usdpl-build-0.11.0 (c (n "usdpl-build") (v "0.11.0") (d (list (d (n "nrpc-build") (r "^0.10") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1gjjpwgagnmxzd0zbgg3ipk641ipxp4gy5zzqfnb4vl7lfpxycmp")))

