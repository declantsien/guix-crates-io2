(define-module (crates-io us dp usdpl-core) #:use-module (crates-io))

(define-public crate-usdpl-core-0.1.0 (c (n "usdpl-core") (v "0.1.0") (h "0dy7l15lrrprnvb4pwrsdz2v9wln68fd85d0yl63pnm2l77yi8xq")))

(define-public crate-usdpl-core-0.3.0 (c (n "usdpl-core") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)))) (h "1zv8582llmjsalh5zgcz3yr3j6m6d3j9y113r03qfd723471z3k8")))

(define-public crate-usdpl-core-0.4.0 (c (n "usdpl-core") (v "0.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)))) (h "1f119p4i4h9nil3wpk0s4k9rk4ywddd93z1vz0x7q6zwsf4zs0zb") (f (quote (("default") ("decky") ("crankshaft"))))))

(define-public crate-usdpl-core-0.5.0 (c (n "usdpl-core") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)))) (h "198fdx3y8wxinwrih0wlhi2a186yhqsyfg1f114l878jy2wjdmsx") (f (quote (("default") ("decky") ("crankshaft"))))))

(define-public crate-usdpl-core-0.6.0 (c (n "usdpl-core") (v "0.6.0") (d (list (d (n "aes-gcm-siv") (r "^0.10") (f (quote ("alloc" "aes"))) (o #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "152knhsbv1lm3vbwgd2wcvhyxhbida8faj8za9c689mc3xc568c6") (f (quote (("encrypt" "aes-gcm-siv") ("default") ("decky") ("crankshaft"))))))

(define-public crate-usdpl-core-0.9.0 (c (n "usdpl-core") (v "0.9.0") (d (list (d (n "aes-gcm-siv") (r "^0.10") (f (quote ("alloc" "aes"))) (o #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "000p7iwfprra04cf3d3n6h061k777pvnr1xwlrl9q66aiailr47k") (f (quote (("translate") ("encrypt" "aes-gcm-siv") ("default") ("decky") ("crankshaft"))))))

(define-public crate-usdpl-core-0.10.0 (c (n "usdpl-core") (v "0.10.0") (d (list (d (n "aes-gcm-siv") (r "^0.10") (f (quote ("alloc" "aes"))) (o #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "0q0r6cppx40650vlr89807h8ihvnbmjalnmir9i5ivavvz1vwwxx") (f (quote (("translate") ("encrypt" "aes-gcm-siv") ("default") ("decky") ("crankshaft"))))))

(define-public crate-usdpl-core-0.11.0 (c (n "usdpl-core") (v "0.11.0") (d (list (d (n "aes-gcm-siv") (r "^0.10") (f (quote ("alloc" "aes"))) (o #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "1xc0qkkjyh3liahf10n7n56mmmk83lc7gj640rvna5429xrgzlfs") (f (quote (("encrypt" "aes-gcm-siv") ("default") ("decky"))))))

