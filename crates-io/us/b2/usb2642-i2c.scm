(define-module (crates-io us b2 usb2642-i2c) #:use-module (crates-io))

(define-public crate-usb2642-i2c-0.1.0 (c (n "usb2642-i2c") (v "0.1.0") (d (list (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "156vnx26p8dz9bbs54l38ghcv1841nmp4qnklx5wkw28iz4ld24x")))

(define-public crate-usb2642-i2c-0.1.1 (c (n "usb2642-i2c") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 2)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "15wp9ym7f279ziaxbl3x8d5rjl1v3l8x7s75p2b05z0npy5swj38")))

