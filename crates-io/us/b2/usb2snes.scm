(define-module (crates-io us b2 usb2snes) #:use-module (crates-io))

(define-public crate-usb2snes-0.1.0 (c (n "usb2snes") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.4.2") (f (quote ("async-std-runtime"))) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parse_int") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0zbccbym2mvp5mvzvbw6s5b8mpa8n95r3a1hlrmn680cicd5rxhq")))

