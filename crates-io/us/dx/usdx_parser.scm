(define-module (crates-io us dx usdx_parser) #:use-module (crates-io))

(define-public crate-usdx_parser-0.1.0 (c (n "usdx_parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "19vz7g2523cbyx353kg7r2fqf5254q8fa93naarqnyzibxzc731z")))

(define-public crate-usdx_parser-0.2.0 (c (n "usdx_parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "09cx4hvbccnjhl8nnik97mfv7n7mm0swq3qri9h5b20nm35pc50d")))

(define-public crate-usdx_parser-0.2.1 (c (n "usdx_parser") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1anmxxinbcl50whgzxz97q381sly5d1dp10jkr213jjl0ykd2rjg")))

(define-public crate-usdx_parser-0.2.2 (c (n "usdx_parser") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "00yvck352fl7pcpnfy6fwyr9k6xvjbspswj538by58h3lk9ghxix")))

