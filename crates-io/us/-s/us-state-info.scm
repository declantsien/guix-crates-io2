(define-module (crates-io us -s us-state-info) #:use-module (crates-io))

(define-public crate-us-state-info-0.2.0 (c (n "us-state-info") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1pzahqz5ffwx91yq9xijj47vfw2064dfj7smm4ys62181sbc8wvd") (f (quote (("serde_abbreviation" "serde") ("serde1" "serde"))))))

(define-public crate-us-state-info-0.2.1 (c (n "us-state-info") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1z6i6wgz9wpr9h6n9pdlrn4apw3h67mf92613xj2mklfppf6aqrl") (f (quote (("serde_abbreviation" "serde") ("serde1" "serde"))))))

(define-public crate-us-state-info-0.2.2 (c (n "us-state-info") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0a6qbhalzw7cqv2p7jyiqrqgdwi9zhfvxs5v5h7d37b8rlj8mnc1") (f (quote (("serde_abbreviation" "serde") ("serde1" "serde"))))))

(define-public crate-us-state-info-0.2.3 (c (n "us-state-info") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0pb1f2465gxkcyy4p4j0q0vpwzhabn7hd7nxmwl5mwyr5qqzijya") (f (quote (("serde_abbreviation" "serde") ("serde1" "serde"))))))

(define-public crate-us-state-info-0.2.4 (c (n "us-state-info") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0a8ng1cg7rxad60mljwyv0914sb0fahjdlg9y4scvywdla26p12z") (f (quote (("serde_abbreviation" "serde") ("serde1" "serde"))))))

