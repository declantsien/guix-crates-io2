(define-module (crates-io us -s us-state-info-rs) #:use-module (crates-io))

(define-public crate-us-state-info-rs-0.1.0 (c (n "us-state-info-rs") (v "0.1.0") (h "1hjlhdbsqz85dydhiyfj5cxm2hmzramakqkrp316jf4hns85xaw9")))

(define-public crate-us-state-info-rs-0.1.1 (c (n "us-state-info-rs") (v "0.1.1") (h "1p4jxwrmsdyq67azsnbw5szxzbx2ibayagn6k8f0l7yyahvyv5x6")))

