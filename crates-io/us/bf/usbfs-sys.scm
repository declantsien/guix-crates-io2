(define-module (crates-io us bf usbfs-sys) #:use-module (crates-io))

(define-public crate-usbfs-sys-0.1.0 (c (n "usbfs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.46") (d #t) (k 1)) (d (n "nix") (r "^0.12") (d #t) (k 0)))) (h "08xqslz2bwckd8zcjq58l60zwcpgkdjvr90kxlffpdzs4zph5wf8")))

