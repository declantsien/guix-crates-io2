(define-module (crates-io us bf usbfs) #:use-module (crates-io))

(define-public crate-usbfs-0.1.0 (c (n "usbfs") (v "0.1.0") (d (list (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "0g53s7vpz70sx4n8kpxq4fdfwhnadyy3lz8s96y2gxrqfk1i8lvy")))

(define-public crate-usbfs-0.1.1 (c (n "usbfs") (v "0.1.1") (d (list (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "1aslwilpk40ax0ags2gyln2qcrf8c78ihy23mjgmxwk5grxl7jf7")))

