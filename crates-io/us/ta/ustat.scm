(define-module (crates-io us ta ustat) #:use-module (crates-io))

(define-public crate-ustat-0.1.0 (c (n "ustat") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1nyz4bwy4ywckayw9jbmb987f94x0g8p6gv7nyrld12qlzag3y0i")))

(define-public crate-ustat-0.2.0 (c (n "ustat") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "quickersort") (r "^3.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "snailquote") (r "^0.1.1") (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "07f3m4z2fmqna5givp115nvf0my8l1wrj6ka6739w0glmrmc4r3x") (y #t)))

(define-public crate-ustat-0.2.1 (c (n "ustat") (v "0.2.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "quickersort") (r "^3.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "snailquote") (r "^0.1.1") (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "0zq3q7klv5v2y25m613ahlj7wp1xkgzs9pg4m5synz5mqffflnb3") (y #t)))

(define-public crate-ustat-0.2.2 (c (n "ustat") (v "0.2.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "quickersort") (r "^3.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "snailquote") (r "^0.1.1") (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)))) (h "0lbsi11m7z0ralv09sx6lpq8zh7s0v11n024gkl7qvn8zgjmqyqn")))

