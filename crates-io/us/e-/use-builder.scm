(define-module (crates-io us e- use-builder) #:use-module (crates-io))

(define-public crate-use-builder-0.1.0 (c (n "use-builder") (v "0.1.0") (d (list (d (n "assert_unordered") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "extra-traits" "full" "parsing" "printing"))) (k 0)))) (h "0pijbgh8kk5gzd56yxwr5nlhdf1l6vhyxr9laxi8jlpidca67mq0")))

