(define-module (crates-io us ec usec) #:use-module (crates-io))

(define-public crate-usec-0.2.0 (c (n "usec") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0zngl1d9zgx0nq9mf2vig07i96gp456f201b01gwy45vy3306xf8")))

(define-public crate-usec-0.2.1 (c (n "usec") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0s1hd0y723zyhq4cjc1mbkyzr7dzgkf9casc3z6qfsgg0f3djb7g")))

(define-public crate-usec-0.2.2 (c (n "usec") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "07i5pdxbh7cl57xgnfn3r3wk2bjxvg6i29lzl54n190kahdx9chi")))

(define-public crate-usec-0.2.3 (c (n "usec") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0rxwx79r6q2nqmch4w5wdqr3hkmvqb2h5knjcpnfljf1a3gryjnl")))

(define-public crate-usec-0.2.4 (c (n "usec") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1jf38gkk0da26dczqv5h2h77k8c9ms4ibqrx31wjc6bm5mdqvxbi")))

(define-public crate-usec-0.3.4 (c (n "usec") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1nv5mma8y9qxq0xj6icspdzspw86szmlfkm10rgzqikizaf47f44")))

