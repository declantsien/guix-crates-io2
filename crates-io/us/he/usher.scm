(define-module (crates-io us he usher) #:use-module (crates-io))

(define-public crate-usher-0.1.0 (c (n "usher") (v "0.1.0") (d (list (d (n "criterion") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "http") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 2)))) (h "1jyg9k8pji1sgrclzsi5nnbpvlhyn6gpdcd2ivk7n7k9qrj4yxyb") (f (quote (("web" "http") ("default"))))))

(define-public crate-usher-0.1.1 (c (n "usher") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "http") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 2)))) (h "1018q2q8xqg14vz3gzb5gsnwg8xfy5s2pv8mb3xqcjrj5ra8zw1n") (f (quote (("web" "http") ("default"))))))

(define-public crate-usher-0.2.0 (c (n "usher") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 2)))) (h "1fppqccwk71bwacarbj1yazgyjrc9wvfb8plw9ql83pqiqm1sig2") (f (quote (("web" "http") ("default"))))))

(define-public crate-usher-0.2.1 (c (n "usher") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 2)))) (h "0vmnw3bnnkb9773wb53jhfgv5ibzblfbf5vb08hhrsxcgjv207z4") (f (quote (("web" "http") ("default"))))))

