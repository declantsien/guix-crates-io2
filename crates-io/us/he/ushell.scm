(define-module (crates-io us he ushell) #:use-module (crates-io))

(define-public crate-ushell-0.0.1 (c (n "ushell") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0i0vwhfddvmzwk16an5ccrzi07zh6gd9ygyhxl0w27x44bkm65xw")))

(define-public crate-ushell-0.1.0 (c (n "ushell") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "0pg55y8aqg51a89py5rqxjsi98ngbhs1624g577n43hg7gr87c9v")))

(define-public crate-ushell-0.1.1 (c (n "ushell") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "1bmfvl249i6d985mwzda4iz72x8s5wbspfbxpxxy1vm46318prnv")))

(define-public crate-ushell-0.2.0 (c (n "ushell") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "0xns1gqa56ng934lrlp67rx5dxpdr8nxjgzjahwgq03dv8qhjkxl")))

(define-public crate-ushell-0.2.1 (c (n "ushell") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "082x94dm3c92nz6a39n0crbg1rrgmgs8vg7hdlikix5hjlvs5z49")))

(define-public crate-ushell-0.2.2 (c (n "ushell") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "0knkwsbwglyziybfzyylswirnmh1mr0040wmqx3iphh282syl01p")))

(define-public crate-ushell-0.3.0 (c (n "ushell") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "1v6vvfdwl7m0bik6vyhsmbksq9ip35wks6369n30rdnl2ly87w10")))

(define-public crate-ushell-0.3.1 (c (n "ushell") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "07kz45ikdgjqa0mqgp6zl6n3pnk48faszyzvcbzjwf6qhk4vzk47")))

(define-public crate-ushell-0.3.2 (c (n "ushell") (v "0.3.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "0z4anjc0rqamlqv4mw17vh5b28ws5d67y9syhk1vk3n5wksyvg50")))

(define-public crate-ushell-0.3.3 (c (n "ushell") (v "0.3.3") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "0gnkcj0rkzz3z0883rrb9gcsi9m6njchfcdqsr32xdfh8z2hcaxv")))

(define-public crate-ushell-0.3.4 (c (n "ushell") (v "0.3.4") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "0bcdjj9g9jnhwblwb238qlwkb1pk1fxnbv0hzzz7g70pykrj0f0z")))

(define-public crate-ushell-0.3.5 (c (n "ushell") (v "0.3.5") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "0852gz6cvd7d527phqjn5py3id0cdzbqiil1qq6rsji0pzr617mi")))

(define-public crate-ushell-0.3.6 (c (n "ushell") (v "0.3.6") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "uluru") (r "^2.1.1") (d #t) (k 0)))) (h "195bqybd4x1hsgq5m0yglk0ngzp43xw0l2n6g16xinlxmylxrhad")))

