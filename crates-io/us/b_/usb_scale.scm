(define-module (crates-io us b_ usb_scale) #:use-module (crates-io))

(define-public crate-usb_scale-0.1.0 (c (n "usb_scale") (v "0.1.0") (d (list (d (n "hidapi") (r "^2.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xiks3dzcjrzrznwflrb2v9nskdhmz3ai057kbb2b1g2bpi5f9yi") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

