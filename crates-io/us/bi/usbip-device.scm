(define-module (crates-io us bi usbip-device) #:use-module (crates-io))

(define-public crate-usbip-device-0.1.0 (c (n "usbip-device") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (k 2)) (d (n "usb-device") (r "^0.2.7") (k 0)) (d (n "usbd-hid") (r "^0.4.5") (k 2)) (d (n "usbd-serial") (r "^0.1.1") (k 2)))) (h "068ibb0d63mwpypcmjqkd3qphyahz66ysvnpkplv9ql3azkmfzdv")))

(define-public crate-usbip-device-0.1.1 (c (n "usbip-device") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (k 2)) (d (n "usb-device") (r "^0.2.7") (k 0)) (d (n "usbd-hid") (r "^0.5.0") (k 2)) (d (n "usbd-serial") (r "^0.1.1") (k 2)))) (h "1kxg5dv9s57z1qqncgyd7ks6jx62krw8kw9mils7cg27yqky9y5y")))

(define-public crate-usbip-device-0.1.2 (c (n "usbip-device") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (k 2)) (d (n "usb-device") (r "^0.2.7") (k 0)) (d (n "usbd-hid") (r "^0.5.0") (k 2)) (d (n "usbd-serial") (r "^0.1.1") (k 2)))) (h "0ld7xfmsw6awh4n996vg0mpyp4hib6qn0zh1fca9qkqnm44q4k4q")))

(define-public crate-usbip-device-0.1.3 (c (n "usbip-device") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (k 2)) (d (n "usb-device") (r "^0.2.7") (k 0)) (d (n "usbd-hid") (r "^0.5.0") (k 2)) (d (n "usbd-serial") (r "^0.1.1") (k 2)))) (h "1fpvm9wn3w77hxv9jz2nlkhxgarf8l7czvb3w6f3bp8scdxyxyxk")))

(define-public crate-usbip-device-0.1.4 (c (n "usbip-device") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2.1") (k 0)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (k 2)) (d (n "usb-device") (r "^0.2.7") (k 0)) (d (n "usbd-hid") (r "^0.5.0") (k 2)) (d (n "usbd-serial") (r "^0.1.1") (k 2)))) (h "0ccpl2fvq544cfic5zqbmimggv1lwcfvaf4jb1sr4llgxxh0qdnp")))

(define-public crate-usbip-device-0.1.5 (c (n "usbip-device") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.3.2") (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (k 2)) (d (n "usb-device") (r "^0.2.9") (k 0)) (d (n "usbd-hid") (r "^0.6.0") (k 2)) (d (n "usbd-serial") (r "^0.1.1") (k 2)))) (h "1zd2wwl69br40s56a4dni7myi5j1zcr8mzlbbp46qrldsrbhnyif")))

(define-public crate-usbip-device-0.1.6 (c (n "usbip-device") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.3.2") (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (k 2)) (d (n "usb-device") (r "^0.3.0") (k 0)) (d (n "usbd-hid") (r "^0.7.0") (k 2)) (d (n "usbd-serial") (r "^0.2.0") (k 2)))) (h "1b1rwha4cwf2jm82dl112siia8y8ij4ybjpb31kwzdr409f92w2s") (y #t)))

(define-public crate-usbip-device-0.2.0 (c (n "usbip-device") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (k 0)) (d (n "log") (r "^0.4.17") (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (k 2)) (d (n "usb-device") (r "^0.3.0") (k 0)) (d (n "usbd-hid") (r "^0.7.0") (k 2)) (d (n "usbd-serial") (r "^0.2.0") (k 2)))) (h "1aj0f4h6hxpnf12179mj0m96vhgm46z6g71va0virk4y1vw8jbbb")))

