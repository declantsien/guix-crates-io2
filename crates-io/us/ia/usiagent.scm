(define-module (crates-io us ia usiagent) #:use-module (crates-io))

(define-public crate-usiagent-0.6.0 (c (n "usiagent") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "queuingtask") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1rkbbvys16mhbqzbgwfc57g2i9zgjrmqrb97h8fii9i2xz5yrfya")))

(define-public crate-usiagent-0.6.1 (c (n "usiagent") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "queuingtask") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "03gzj677fgy4r5gslkiz3187h5rfd5yqa4dzh2wdcvyjkihqqfl7")))

(define-public crate-usiagent-0.6.2 (c (n "usiagent") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "queuingtask") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0n3jsjlgxsbmljv4ppr4yfv7q1mja0db67bs8lc9ip3as1y4i3cf")))

(define-public crate-usiagent-0.6.3 (c (n "usiagent") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "queuingtask") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "03raw6rp36s5mas5561j1kqpdamvl8b36lkrbnam5q0i6x5wywx0")))

(define-public crate-usiagent-0.6.5 (c (n "usiagent") (v "0.6.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "queuingtask") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1l01029zx028raxh1jwm4m62kdz6vsz78r9j4qwidrn28zp4x31x")))

(define-public crate-usiagent-0.6.6 (c (n "usiagent") (v "0.6.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "queuingtask") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0s69g8rk6w9rklpcds6b6f6jdciqqjiahg3i3kkspqc38ypviiwp")))

(define-public crate-usiagent-0.6.7 (c (n "usiagent") (v "0.6.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "queuingtask") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "16mf5z6z8rjsxlw6sabzcq3ygbrf0whk0xn65b1v9m4rsq6j875a")))

(define-public crate-usiagent-0.6.8 (c (n "usiagent") (v "0.6.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "queuingtask") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0f8c3bnr5ndyfkfb1f73qlqr3rrh6nparyac2bdhxz56m72wicya")))

(define-public crate-usiagent-0.6.9 (c (n "usiagent") (v "0.6.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "queuingtask") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 0)))) (h "0rll4bdwqxp94pqa135akc8qnjj75aszfz2mlmmxzdw4wzhw8k2l")))

