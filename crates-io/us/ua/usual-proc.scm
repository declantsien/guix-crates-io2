(define-module (crates-io us ua usual-proc) #:use-module (crates-io))

(define-public crate-usual-proc-0.1.0 (c (n "usual-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "198wrjmawjm9kc6m3lhnn75s6xka2f9fb2na84580xwri6cqqrwv")))

(define-public crate-usual-proc-0.1.1 (c (n "usual-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0nmy7pahjk18s0a2m8ak4jpw82a9n4p24dfx3v6ppy4z5k9h1jnz")))

(define-public crate-usual-proc-0.1.2 (c (n "usual-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "067v0h7wfk8v4ixb9xcdyla8sbr713jmfzygfh4s02si7d8s4as2")))

