(define-module (crates-io us vg usvg-cli) #:use-module (crates-io))

(define-public crate-usvg-cli-0.5.0 (c (n "usvg-cli") (v "0.5.0") (d (list (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "gumdrop") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.5.0") (d #t) (k 0)))) (h "04vnzpb5i7mcd8ir5a1wnk714n0l6qyllx0jwz6f44634gdcqjix")))

(define-public crate-usvg-cli-0.6.0 (c (n "usvg-cli") (v "0.6.0") (d (list (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "gumdrop") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.6.0") (d #t) (k 0)))) (h "0p33qr9mdx1dcai1mfwlwhvfgr5h5kl0sfcfjjqgl4gcgwsms0vx")))

(define-public crate-usvg-cli-0.6.1 (c (n "usvg-cli") (v "0.6.1") (d (list (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "gumdrop") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.6.0") (d #t) (k 0)))) (h "10f9c7w3nk088k8vjr6j2qicgl6z1rkb4ab2chfsbb30nl8i9hk9")))

(define-public crate-usvg-cli-0.7.0 (c (n "usvg-cli") (v "0.7.0") (d (list (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "gumdrop") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "usvg") (r "^0.7.0") (d #t) (k 0)))) (h "15db0jjqlfj9w24aklrpdp9xdzwlywb0zx1ydg9q1pvxh9pqmkpi")))

(define-public crate-usvg-cli-0.8.0 (c (n "usvg-cli") (v "0.8.0") (d (list (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.2") (d #t) (k 0)) (d (n "usvg") (r "^0.8") (d #t) (k 0)))) (h "09v98mplg2x399xl33v147mb22l9l56saks4cs5kqk42rz3yhvfg")))

(define-public crate-usvg-cli-0.9.1 (c (n "usvg-cli") (v "0.9.1") (d (list (d (n "fern") (r "=0.5.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.2") (d #t) (k 0)) (d (n "usvg") (r "^0.9") (k 0)))) (h "0xzq91kq6ykifzpnh1iza0aai3gm43k6vmyh37d0qllvmvgjvyf3") (f (quote (("text" "usvg/text") ("default" "text"))))))

(define-public crate-usvg-cli-0.9.2 (c (n "usvg-cli") (v "0.9.2") (d (list (d (n "fern") (r "=0.5.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.2") (d #t) (k 0)) (d (n "usvg") (r "^0.9") (k 0)))) (h "1p2wj8mnbicd2bsafdqs9cbiqxv6gc7zkrs28n7aqn11289z17rs") (f (quote (("text" "usvg/text") ("default" "text"))))))

