(define-module (crates-io us vg usvg-tree) #:use-module (crates-io))

(define-public crate-usvg-tree-0.30.0 (c (n "usvg-tree") (v "0.30.0") (d (list (d (n "kurbo") (r "^0.9") (d #t) (k 0)) (d (n "rctree") (r "^0.5") (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.11") (d #t) (k 0)))) (h "135dbk9c0vyr5d5bda3dhx9xhsbh6awqknqw1c6rf2jyz2gck2m5")))

(define-public crate-usvg-tree-0.31.0 (c (n "usvg-tree") (v "0.31.0") (d (list (d (n "kurbo") (r "^0.9") (d #t) (k 0)) (d (n "rctree") (r "^0.5") (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.11") (d #t) (k 0)))) (h "0aa8b44h0l89p9r6s0bacxn5ysba4xz1hj8ks1gv9zz083z95jzr")))

(define-public crate-usvg-tree-0.32.0 (c (n "usvg-tree") (v "0.32.0") (d (list (d (n "kurbo") (r "^0.9") (d #t) (k 0)) (d (n "rctree") (r "^0.5") (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.11") (d #t) (k 0)))) (h "0naigczk6fxzhx8x5h0pyywmys8c64zjwpk5qg6f1nkw8rf2cwbk")))

(define-public crate-usvg-tree-0.33.0 (c (n "usvg-tree") (v "0.33.0") (d (list (d (n "kurbo") (r "^0.9") (d #t) (k 0)) (d (n "rctree") (r "^0.5") (d #t) (k 0)) (d (n "strict-num") (r "^0.1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.11") (d #t) (k 0)))) (h "02y26zrkmhqj144mnjxs3py97lsj5nw9azj78iknvsz57g6yjmis")))

(define-public crate-usvg-tree-0.34.0 (c (n "usvg-tree") (v "0.34.0") (d (list (d (n "rctree") (r "^0.5") (d #t) (k 0)) (d (n "strict-num") (r "^0.1.1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.11") (d #t) (k 0)) (d (n "tiny-skia-path") (r "^0.10") (d #t) (k 0)))) (h "1nf66lj87zkywri568z6wnkxa2633hji55nj2wcczlz3nq9yp19i")))

(define-public crate-usvg-tree-0.35.0 (c (n "usvg-tree") (v "0.35.0") (d (list (d (n "rctree") (r "^0.5") (d #t) (k 0)) (d (n "strict-num") (r "^0.1.1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.11") (d #t) (k 0)) (d (n "tiny-skia-path") (r "^0.10") (d #t) (k 0)))) (h "14lbzk9qbr40prjh1ml17r6f5hw10rrkjqqx65fxpji1xpjaffbr")))

(define-public crate-usvg-tree-0.36.0 (c (n "usvg-tree") (v "0.36.0") (d (list (d (n "rctree") (r "^0.5") (d #t) (k 0)) (d (n "strict-num") (r "^0.1.1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.12") (d #t) (k 0)) (d (n "tiny-skia-path") (r "^0.11.2") (d #t) (k 0)))) (h "0qj01xfpwl06cry9mz5y31ix7hac03ab1xgwb87fiwzaxp2v1b3c")))

(define-public crate-usvg-tree-0.37.0 (c (n "usvg-tree") (v "0.37.0") (d (list (d (n "rctree") (r "^0.5") (d #t) (k 0)) (d (n "strict-num") (r "^0.1.1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.13") (d #t) (k 0)) (d (n "tiny-skia-path") (r "^0.11.3") (d #t) (k 0)))) (h "1ly7dsbvl5k478vjxkr3svpj947gsssgbf04aqhsd5yvxc1d5qwf")))

(define-public crate-usvg-tree-0.38.0 (c (n "usvg-tree") (v "0.38.0") (d (list (d (n "strict-num") (r "^0.1.1") (d #t) (k 0)) (d (n "svgtypes") (r "^0.13") (d #t) (k 0)) (d (n "tiny-skia-path") (r "^0.11.3") (d #t) (k 0)))) (h "01czj22qlzdy5p7y6i1j58k4z7yv8q8mnb1narp3s5gd0h23x1hq")))

