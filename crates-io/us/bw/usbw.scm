(define-module (crates-io us bw usbw) #:use-module (crates-io))

(define-public crate-usbw-0.0.1 (c (n "usbw") (v "0.0.1") (d (list (d (n "driver_async") (r "^0.0.2") (d #t) (k 0)) (d (n "rusb") (r "^0.5.5") (d #t) (k 0)))) (h "12lhnpq0r6y2vkbw83k8svi5sm7hswrymzvhf09vmdqpcxkd34kd")))

(define-public crate-usbw-0.0.2 (c (n "usbw") (v "0.0.2") (d (list (d (n "driver_async") (r "^0.0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (k 0)) (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "libusb1-sys") (r "^0.3.7") (o #t) (k 0)) (d (n "tokio") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.8") (o #t) (k 0)))) (h "1d5rlbh54yw3m6i2p8x27xf17dnqyi5m492idjm3d4vm3mgb4fx6") (f (quote (("winusb" "winapi/winusb" "std") ("std") ("libusb" "libusb1-sys" "std" "libc") ("default" "libusb"))))))

