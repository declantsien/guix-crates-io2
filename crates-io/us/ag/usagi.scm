(define-module (crates-io us ag usagi) #:use-module (crates-io))

(define-public crate-usagi-0.0.0 (c (n "usagi") (v "0.0.0") (h "0dny1i0xgcsf5hgcigch1lm3qzgj516hl9fr0h73s9xk01pd2yzy")))

(define-public crate-usagi-0.1.0 (c (n "usagi") (v "0.1.0") (h "1wzjh5ij0w8dgb9c9p2b8l7i87wdkwa0dflc89wggmspif7cship")))

(define-public crate-usagi-0.2.0 (c (n "usagi") (v "0.2.0") (h "1z3zc323kvg9a755hg8vl2gvwi63nxanmyn7flqmz9gh1v279357")))

