(define-module (crates-io us d- usd-cpp) #:use-module (crates-io))

(define-public crate-usd-cpp-0.1.0 (c (n "usd-cpp") (v "0.1.0") (h "1sx7bv645qr7kjxlw5vq58l2zl8ymgn6dsr5i10jzn3271vi6k05")))

(define-public crate-usd-cpp-0.1.1 (c (n "usd-cpp") (v "0.1.1") (h "06a5rvj18fmf1yq84magkvg7ljn7jqhmbxh3ikw8z5jrznn98c8z")))

