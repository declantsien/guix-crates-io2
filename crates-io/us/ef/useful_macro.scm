(define-module (crates-io us ef useful_macro) #:use-module (crates-io))

(define-public crate-useful_macro-0.1.0 (c (n "useful_macro") (v "0.1.0") (h "1zz2d4fm2syqdykflyw9qchad1i93jp7pvjy0nwrwj731c6xfxm6")))

(define-public crate-useful_macro-0.1.1 (c (n "useful_macro") (v "0.1.1") (h "06qi5bv5mmcb0q0ilxg353gs3dhz7i8d7z3xb1mlkkdm8d7wd8a5")))

(define-public crate-useful_macro-0.1.2 (c (n "useful_macro") (v "0.1.2") (h "0crmhwn6w0ypinafvc1az4rjgwxgaayjgwdwacjih6m66pkyhxxa")))

(define-public crate-useful_macro-0.1.3 (c (n "useful_macro") (v "0.1.3") (h "06i67k5ci04605db0ylrzrsj32sk5x9klf8gm0cmci7qb91azm61")))

(define-public crate-useful_macro-0.1.4 (c (n "useful_macro") (v "0.1.4") (h "0gpv3v7b86b7q982mx0757i8g6nynlsdhdvcjyz0kpbm0gx4fqf5")))

(define-public crate-useful_macro-0.1.5 (c (n "useful_macro") (v "0.1.5") (h "0g1rvdyidb1dygwsw0q0m84czls2ms7pa12r1q2m0zx7cmnk358h")))

(define-public crate-useful_macro-0.1.6 (c (n "useful_macro") (v "0.1.6") (h "15gy9kxxc04zfs0flf31raqbckablbx6mar81b0y8cnigwa6r99f")))

(define-public crate-useful_macro-0.1.7 (c (n "useful_macro") (v "0.1.7") (h "04g2fjllq97cv05czx25gg49xqh8vv2p07fm4wsbkhgar1scky28")))

(define-public crate-useful_macro-0.1.8 (c (n "useful_macro") (v "0.1.8") (h "0i3lb24p7lh7aqks9rxvbdnnbm3bm0vbdd57c7x5ls5w4fzahxq7")))

(define-public crate-useful_macro-0.1.9 (c (n "useful_macro") (v "0.1.9") (h "0bhwwqxp9p78xnh2b9vr4cxgzx83vw10a89gpjg86fhmzkhkbp5l")))

(define-public crate-useful_macro-0.1.10 (c (n "useful_macro") (v "0.1.10") (h "0g8c7hb36lasrbn1f3s44afhw6w88dmszqf3zfqni26fbjf15zyp")))

(define-public crate-useful_macro-0.1.11 (c (n "useful_macro") (v "0.1.11") (h "0cpr8skgrzl3d419qpl1s6kwflcqs1ybxj2ymh3bdx4gypf89vsq")))

(define-public crate-useful_macro-0.1.12 (c (n "useful_macro") (v "0.1.12") (h "0xgvpqhzjbgf7fpgv65r4vm4xyr42125h4hqry2mdq98ab0hc7a1")))

(define-public crate-useful_macro-0.1.13 (c (n "useful_macro") (v "0.1.13") (h "0n4iblqjqmwddh0b693accalr589g9vp6hg2qkgbylv40lc982gr")))

(define-public crate-useful_macro-0.1.14 (c (n "useful_macro") (v "0.1.14") (h "1adx6m86jhm14m1ibjzwgz781x5d4ya9fra8788w8i8niy46abx5")))

(define-public crate-useful_macro-0.1.15 (c (n "useful_macro") (v "0.1.15") (h "0q6nf5xay1hjmqwk29vxh9naqvlrkdpsrpsks7slk7yc3g64figy")))

(define-public crate-useful_macro-0.1.16 (c (n "useful_macro") (v "0.1.16") (h "1b6rrdbqc2757bdgcqgl3q6k3xjzr7iy5b364hvg927djms394dl")))

(define-public crate-useful_macro-0.1.17 (c (n "useful_macro") (v "0.1.17") (h "0x3k9a9fp2ipgxabl1g22645gqqqwaggqznkgz41c37kv7v3mj4c")))

(define-public crate-useful_macro-0.1.18 (c (n "useful_macro") (v "0.1.18") (h "0j8fa3h66wad7rg7fi0x4szpxzb47pg5zzdjksf0zndnhnqb2l9r")))

(define-public crate-useful_macro-0.1.19 (c (n "useful_macro") (v "0.1.19") (h "1z5rdcb39fasy285v09fkspmvy33nhgxvqy0w6cg1dxv5s28va2s")))

(define-public crate-useful_macro-0.1.20 (c (n "useful_macro") (v "0.1.20") (h "17agsp9ilx5lifplsb8r4nfg0icvhdjl6v85sz0kql8cx7vi0900")))

(define-public crate-useful_macro-0.1.21 (c (n "useful_macro") (v "0.1.21") (h "09j7dys4wf7ik2iigxwyiry48ih6k8zvxskhkcyngyrm051hi97i")))

(define-public crate-useful_macro-0.1.22 (c (n "useful_macro") (v "0.1.22") (h "0l7k20n55pm7f14g0qqwv1585ny06gihkgcr4ihm75yxvhmgkp86")))

(define-public crate-useful_macro-0.1.23 (c (n "useful_macro") (v "0.1.23") (h "0fdbqfqd1svgnwsjcrcs2ya98w2hfjmixiv0154xw6z86hpyksn9")))

(define-public crate-useful_macro-0.1.24 (c (n "useful_macro") (v "0.1.24") (h "0fikb4mh27hyklns3hk51czip47rs0amd60l76qh8pbs0qdr0b7g")))

(define-public crate-useful_macro-0.1.25 (c (n "useful_macro") (v "0.1.25") (h "1x37mzrqbycrg2c0jk6pnyrxyllrwlr1db4x78i6g7f6j8h7mvnd")))

(define-public crate-useful_macro-0.1.26 (c (n "useful_macro") (v "0.1.26") (h "0cb91dr3kimqfas2bd5z24s0829isz32rcbpbv5bglwhqzcfdklp")))

(define-public crate-useful_macro-0.1.27 (c (n "useful_macro") (v "0.1.27") (h "11a90zqdwz3w9z56l147n40rfx951q295vh6vlgp74bbh745zbs9")))

(define-public crate-useful_macro-0.1.28 (c (n "useful_macro") (v "0.1.28") (h "0rc71yh6vlbliyn41f1lk0gn3g0skw0h1pnlni1wkg5v7wsmy0rf")))

(define-public crate-useful_macro-0.1.29 (c (n "useful_macro") (v "0.1.29") (h "0wakc0h1yk98abdrgd7zaa0737297s6969ya83pl6x5r5m1305zz")))

(define-public crate-useful_macro-0.1.30 (c (n "useful_macro") (v "0.1.30") (h "165bcc17yfmb1lakggzlszrc87hxlwgis6sjrspgpgv0b8gqvx7s")))

(define-public crate-useful_macro-0.1.31 (c (n "useful_macro") (v "0.1.31") (h "09zbc6hirxncb5y6xvncr3z23b70h9and36szc2k4bd2x2imvp0x")))

(define-public crate-useful_macro-0.1.32 (c (n "useful_macro") (v "0.1.32") (h "1vp6skjbijyjjy5z1p71c515z35vwq32bkpadw2prfswd82hilja")))

(define-public crate-useful_macro-0.1.33 (c (n "useful_macro") (v "0.1.33") (h "0arwfzx04aq9wbjsa6vq1n4yg8hp081vjylw5p7sj6nsf6ynh4m8")))

(define-public crate-useful_macro-0.1.34 (c (n "useful_macro") (v "0.1.34") (h "15la33kjr0pnvwpbad3ix6a066irma6x7zm7m4682v1pdqh3dsby")))

(define-public crate-useful_macro-0.1.35 (c (n "useful_macro") (v "0.1.35") (h "16xw099cmik111a8g1728d7ylix7h65j0ifs7av8xyyparfhcji5")))

(define-public crate-useful_macro-0.1.37 (c (n "useful_macro") (v "0.1.37") (h "111zk68gs39nhyxrhmcg9cxldsrzqfwh4dc588bg49yzf33ph7gj")))

(define-public crate-useful_macro-0.1.38 (c (n "useful_macro") (v "0.1.38") (h "1ljb0rhnw1yb094zhdpzilra1yvqgk18bi503ns0jxjcqgv3ng4k")))

(define-public crate-useful_macro-0.2.0 (c (n "useful_macro") (v "0.2.0") (h "1yy4q1lyhksh77792asr1jl73ahh30iigps7hqajr7j01q96nn36")))

(define-public crate-useful_macro-0.2.1 (c (n "useful_macro") (v "0.2.1") (h "1imy68rqi3hb5lvyyrdjdl9zfrd8bhjbb3gwv6cln7rvzy98m9jk")))

(define-public crate-useful_macro-0.2.2 (c (n "useful_macro") (v "0.2.2") (h "19a1g9rjqx6a2p4kwqcl15qiqxi0pqaic64znim38j9ybikpxm3d")))

(define-public crate-useful_macro-0.2.3 (c (n "useful_macro") (v "0.2.3") (h "0bncs16p0fxg6v8pzilzdp2g1f6rcgvrd89fky2zx8ijji9fwyks")))

(define-public crate-useful_macro-0.2.4 (c (n "useful_macro") (v "0.2.4") (h "02pn8x8m7lg55cc4v3bmf5s6xbzqplzd5kc7rlb600h66f1jf9cd")))

(define-public crate-useful_macro-0.2.5 (c (n "useful_macro") (v "0.2.5") (h "0pc888cmhbs7d645g6aqahq0ghsbicdbx8pc6a0l4l3zh97miszw")))

(define-public crate-useful_macro-0.2.6 (c (n "useful_macro") (v "0.2.6") (h "0j74h9rhp1mipsiq9q6cjg59cmd5rsw1p9qlw09r1a280bvbcr19")))

(define-public crate-useful_macro-0.2.8 (c (n "useful_macro") (v "0.2.8") (h "08plq4d4hnyhn1fnvk3lnhwbgnnsdw1vhi54rribx242s5g4zf67")))

(define-public crate-useful_macro-0.2.9 (c (n "useful_macro") (v "0.2.9") (h "0dn83s86d6zxr6hxlqnm8wl7jlifj9ybpg6l6mrlc9bkm103ad7y")))

(define-public crate-useful_macro-0.2.10 (c (n "useful_macro") (v "0.2.10") (h "00jph9rfql3z9l6jbhdhs7cal8xdvhvicyg4v5lclpk61kjprlgx")))

(define-public crate-useful_macro-0.2.11 (c (n "useful_macro") (v "0.2.11") (h "1gpwrz9j7bpd763nc50672r14klkdrnx88wwii56jg0qxyxzss28")))

(define-public crate-useful_macro-0.2.12 (c (n "useful_macro") (v "0.2.12") (h "00h5amzzrszdhsjbq10iadngpz0cyngkibl330qn27c29jgamdsz")))

(define-public crate-useful_macro-0.2.13 (c (n "useful_macro") (v "0.2.13") (h "0x9svsc2h3d5y3d4iyx0sm6fbfgkxn7skppr2pklia4wfcwci3wb")))

(define-public crate-useful_macro-0.2.14 (c (n "useful_macro") (v "0.2.14") (h "07ylb768qnkg8dkc3fcfm84z2jk9m6agwrvkvhamjslnd5440zly")))

(define-public crate-useful_macro-0.2.15 (c (n "useful_macro") (v "0.2.15") (h "01gn3hd8p03769243agdnikg1iw7dp1byadjq7gq1m9pmwznl5yj")))

(define-public crate-useful_macro-0.2.16 (c (n "useful_macro") (v "0.2.16") (h "07y3xczvw6bn1cy2blf5id0l7z0cdhlrd1npyryn9yqcnfpmr76b")))

(define-public crate-useful_macro-0.2.17 (c (n "useful_macro") (v "0.2.17") (h "1cs6l3cs71ifm6sc8lwshzjna68ckd74gx7k9hbw3yahp9q5x6yf")))

(define-public crate-useful_macro-0.2.18 (c (n "useful_macro") (v "0.2.18") (h "1al3x74kgglghq26wb0z2q46aqkjh0fj1yr9kgyk7gacc86aka50")))

(define-public crate-useful_macro-0.2.19 (c (n "useful_macro") (v "0.2.19") (h "0zrpa5sw75h1md8mraa76iksxjm5jyrf80ijbc9x1f7ii36pls4l")))

(define-public crate-useful_macro-0.2.20 (c (n "useful_macro") (v "0.2.20") (h "16zrzqbqj98s0qchnapmj3syjfmxka94aiwqs1zvakgq7a0k0p8x")))

(define-public crate-useful_macro-0.2.21 (c (n "useful_macro") (v "0.2.21") (h "03lm443rc0hcvjmvb9n2hrddr3lrc1w8z2l8ycky6vjbmvmk4imy")))

(define-public crate-useful_macro-0.2.22 (c (n "useful_macro") (v "0.2.22") (h "0fv4nnfx6yf5rpkjw68vg49x77ciwwbjxkypgb9hvhxpryqyf657")))

(define-public crate-useful_macro-0.2.23 (c (n "useful_macro") (v "0.2.23") (h "1q080dg6il9rx1cgkmmk5rgv50mv9dhva5lcax5zjkkvdz20sykl")))

(define-public crate-useful_macro-0.2.24 (c (n "useful_macro") (v "0.2.24") (h "042lin7rfb0wq6cjkqddapnk1af0m3pw54a285dv9h78kv6bl0f2")))

(define-public crate-useful_macro-0.2.25 (c (n "useful_macro") (v "0.2.25") (h "1hs4ncclb5kdji2y8gd2sy5yxq6az29049gvr42idhxl7zi6cx2n")))

(define-public crate-useful_macro-0.2.26 (c (n "useful_macro") (v "0.2.26") (h "1rjzsc9k489j8w52p8viigvcdflpqx8samv55m2kpszszkvpb3mz")))

(define-public crate-useful_macro-0.2.27 (c (n "useful_macro") (v "0.2.27") (h "1jfqbb5hwks77l5wjki05rq260vgpfjniglmbg47v3iv9dnf32l1")))

(define-public crate-useful_macro-0.2.28 (c (n "useful_macro") (v "0.2.28") (h "17ng5jj1jbz6cfd4kj5z3jxcfmg7bf0lcvcc3b1l8ykp97i7kym3")))

(define-public crate-useful_macro-0.2.29 (c (n "useful_macro") (v "0.2.29") (h "15zvbl1ljxqvjgydqmm7y9s9lggm7ibamaywdhpd8cczj8jph0cs")))

