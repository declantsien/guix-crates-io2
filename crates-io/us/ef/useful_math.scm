(define-module (crates-io us ef useful_math) #:use-module (crates-io))

(define-public crate-Useful_Math-0.0.1 (c (n "Useful_Math") (v "0.0.1") (h "0brfa0vhlkkzkj7flgdprsvp7l64pbp86bslnlbcy6i2267ids94")))

(define-public crate-Useful_Math-0.0.2 (c (n "Useful_Math") (v "0.0.2") (h "1agrhrbh2vf6rfk88rz6rykmzsyisqm4b2c6jg8phd5j57c0hjjk")))

(define-public crate-Useful_Math-0.0.3 (c (n "Useful_Math") (v "0.0.3") (h "0j7630acynqp78scwmv81myi3x6gsg7x9mg6z1i4533imdbpvxhk")))

(define-public crate-Useful_Math-0.0.4 (c (n "Useful_Math") (v "0.0.4") (h "0xkpwdqpjcf4md1bkw16kq2xl0jwxyvixc53px45x38jgpgpm0kj")))

(define-public crate-Useful_Math-0.0.5 (c (n "Useful_Math") (v "0.0.5") (h "0mwb9plq0gdqiqf8nyry66s8k74gha2ziwbj77jzmryc8zv4lkjv")))

(define-public crate-Useful_Math-0.0.6 (c (n "Useful_Math") (v "0.0.6") (h "05csxfznk0r4m1i4glb93ri9svlp0nfcc60v3ns5ifwxkrz7yxz6")))

(define-public crate-Useful_Math-0.0.7 (c (n "Useful_Math") (v "0.0.7") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0bdbr60d4nr8apqh89jwiszc0gw2mhlg5ld3lqg2b6n6zfnhij3s")))

(define-public crate-Useful_Math-0.0.8 (c (n "Useful_Math") (v "0.0.8") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1cibivpynq41g3lmxm2hxwgw62ilhr6p179aahwhdi390kp0a11n")))

