(define-module (crates-io us ef useful_static) #:use-module (crates-io))

(define-public crate-useful_static-0.1.0 (c (n "useful_static") (v "0.1.0") (h "03qqmghmz19g6qigzkpcxr38b9jyp6b03ifk0h0b1vj07v0vm6dl")))

(define-public crate-useful_static-0.1.1 (c (n "useful_static") (v "0.1.1") (h "0vqpbsj765gdam5ycf8qq8sncmx1fxa2cjq7jcqmd1qxmhqdjhd1")))

(define-public crate-useful_static-0.2.0 (c (n "useful_static") (v "0.2.0") (h "0v335prvvwncvm9bfggjhgg91h3qn1xcv2cx4xikyzvsl39ax6y1")))

(define-public crate-useful_static-0.2.1 (c (n "useful_static") (v "0.2.1") (h "197h76fbxldw45hyjdqwzmnmiwdqc67q8vyz8hx2npfjhz20f2cf")))

