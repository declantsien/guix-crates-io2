(define-module (crates-io us ef useful-serenity) #:use-module (crates-io))

(define-public crate-useful-serenity-0.1.0 (c (n "useful-serenity") (v "0.1.0") (d (list (d (n "serenity") (r "^0.10") (f (quote ("client" "gateway" "model" "rustls_backend" "unstable_discord_api"))) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lw5av4q3ixjha5yv861y6g2rkhp5mhbc6qs5d59g4yhp1w5r90q")))

(define-public crate-useful-serenity-0.1.2 (c (n "useful-serenity") (v "0.1.2") (d (list (d (n "serenity") (r "^0.10") (f (quote ("client" "gateway" "model" "rustls_backend" "unstable_discord_api"))) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m5af40f4midnkv2aca2lgg4xq6wkdhaxzrycxlrxb1nld1m5j2n")))

(define-public crate-useful-serenity-0.1.3 (c (n "useful-serenity") (v "0.1.3") (d (list (d (n "serenity") (r "^0.10") (f (quote ("client" "gateway" "model" "rustls_backend" "unstable_discord_api"))) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a41cx5amvlwfd78fwq327q4mnm5i2zj0lxbbbadx58gm8ic1w5h")))

(define-public crate-useful-serenity-0.1.4 (c (n "useful-serenity") (v "0.1.4") (d (list (d (n "serenity") (r "^0.10") (f (quote ("client" "gateway" "model" "rustls_backend" "unstable_discord_api"))) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kffij58a71kqprn6r259iv219g8hy3s1c6sa67ikh76hfhxppgp")))

