(define-module (crates-io us tr ustr-fxhash) #:use-module (crates-io))

(define-public crate-ustr-fxhash-1.0.0 (c (n "ustr-fxhash") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "string-interner") (r "^0.13.0") (d #t) (k 2)) (d (n "string_cache") (r "^0.8.1") (d #t) (k 2)))) (h "026v25k0565b19i07clgp71zry99v110s4iqwwnx26745ph8mqxv")))

