(define-module (crates-io us ea usearch) #:use-module (crates-io))

(define-public crate-usearch-0.1.4 (c (n "usearch") (v "0.1.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0c24favscbg9q4pk2hj9f4xk14iylig729q54w82cf2axz5h0b1f")))

(define-public crate-usearch-0.1.5 (c (n "usearch") (v "0.1.5") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "059d3mm0143y5kwqzn7zksbdl4a1spdsm404cx459rp028pb3isj")))

(define-public crate-usearch-0.1.6 (c (n "usearch") (v "0.1.6") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0cqfbagcllx2gqqcil20202d8m1i0c0lhpfmbb5gqf2s870r88xk")))

(define-public crate-usearch-0.1.7 (c (n "usearch") (v "0.1.7") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1278gmg0cjkl9ygzz6l7zdjzlggnl9qcqxw5mrbcswm76dp6alfx")))

(define-public crate-usearch-0.1.8 (c (n "usearch") (v "0.1.8") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1bxk712506wjsikzv3i7d1zdj8wkkw0gappsb6519b6apshlp7v6")))

(define-public crate-usearch-0.1.9 (c (n "usearch") (v "0.1.9") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0riw9c4qdbfacpbcpyji8wc0wrzf6w82gfrg16n8qgdyj6ch0q2f")))

(define-public crate-usearch-0.1.10 (c (n "usearch") (v "0.1.10") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "05f525wg8xxd86k42jk8lqwfj8r4ynzicbzz4l3fcx18zpgjac3n")))

(define-public crate-usearch-0.2.0 (c (n "usearch") (v "0.2.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0c8l6xrz69jddp23fb51jsl1c474vi6dqsp3saqz2vw3j0znz4ib")))

(define-public crate-usearch-0.2.1 (c (n "usearch") (v "0.2.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0ah6y8llilnw60g9qnfd0zgj26xk93vagjak4kkkyc1q5lldvml5")))

(define-public crate-usearch-0.2.2 (c (n "usearch") (v "0.2.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1iggdqh1mbd5zaxl5aw6d4ndj8g41qs84pxn98kincfd7kqz8l7x")))

(define-public crate-usearch-0.2.3 (c (n "usearch") (v "0.2.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "01a61si1cqqjmna7wds28m1mis8bknbhp507f9q26dm24jhzmh7i")))

(define-public crate-usearch-0.2.4 (c (n "usearch") (v "0.2.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1fb11bj784azpf6hw70k9zza4fkfzy454vy7qpxhl0ljb0wf4pzy")))

(define-public crate-usearch-0.3.0 (c (n "usearch") (v "0.3.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "08jnrhyb70zx3xpk2yfc8xxzjczd2blpi8ynyrfmmc3d7abcz2jm")))

(define-public crate-usearch-0.4.0 (c (n "usearch") (v "0.4.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1g5n9apkwyv26c50klsm8cvyi5rq10dakxbw75lswzpas0ixl077")))

(define-public crate-usearch-0.5.0 (c (n "usearch") (v "0.5.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0pb254yfcjrpi866q0mhfvicpz7cmginccs3m6782crvrkpdv5a9")))

(define-public crate-usearch-0.5.1 (c (n "usearch") (v "0.5.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0c5lw2m80yb9sjbhycq0g8di5hq5bjk8f913fhzc5cj9sw1qh4g7")))

(define-public crate-usearch-0.6.0 (c (n "usearch") (v "0.6.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1lpmk1fj2wv72hi2zf37bcb93r1nyiy12y3prwsmcg8wzrzdik7n")))

(define-public crate-usearch-0.6.1 (c (n "usearch") (v "0.6.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "058ilil39xdp6c8hj1fvs80aqfmqbi46lp0sasdracqb463qg9qb")))

(define-public crate-usearch-0.7.0 (c (n "usearch") (v "0.7.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1xzm2gk05zf146z88i2wcz47i1f8ag20z1k5bzf87rgkng30sa7y")))

(define-public crate-usearch-0.8.0 (c (n "usearch") (v "0.8.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "07xj1z2yyw7kqk7mmcblsr3nkrfbpqg0a95yalkanxjhrvcgig95")))

(define-public crate-usearch-0.8.1 (c (n "usearch") (v "0.8.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "15j11qjdmv7cfv5jnyk02i4s8i9jb130nf59vd5rvx73mgm31rj5")))

(define-public crate-usearch-0.9.1 (c (n "usearch") (v "0.9.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "13kkhqqhv6k59dy7za9vlm9i9wqy2i137w6vafxbvc3j3irn9mx9")))

(define-public crate-usearch-0.9.2 (c (n "usearch") (v "0.9.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0y56wj1lw1yqg2mqnyhmxqyal1x0h6gb1r99s15rjs8k9fm0jfkl")))

(define-public crate-usearch-0.9.3 (c (n "usearch") (v "0.9.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1xxvm9w2q3fndpd3wwzxddzxklawxgb8yikckv5klr51sq404jmd")))

(define-public crate-usearch-0.9.4 (c (n "usearch") (v "0.9.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "19bcxk204ji25ibglk1bddc9spxkfzwr3x3x4bs09rcg1w76w885")))

(define-public crate-usearch-0.9.5 (c (n "usearch") (v "0.9.5") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0jdwfm4j5iqks7g3y78sw2bgvnlkx2s4wnvx5vi67h2nf2qc3zm2")))

(define-public crate-usearch-0.9.6 (c (n "usearch") (v "0.9.6") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "000cdj93q7faq0nf8fh2l5y7rai46lr7jk36dagmhsml9r56grrw")))

(define-public crate-usearch-0.9.7 (c (n "usearch") (v "0.9.7") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "00hh0dacmzpb0412qnd3bvx4mr75arzd7wz1n588m4kny0z36d9p")))

(define-public crate-usearch-0.10.0 (c (n "usearch") (v "0.10.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1njqq3ys10dd90w9ixrpbr70w082fa618kid2gsqg4m64wym0r79")))

(define-public crate-usearch-0.11.0 (c (n "usearch") (v "0.11.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0msqk15ckfkx4s7i3mi4dwmhvmaz9cb44mhvfhbwf01x3k4npzar")))

(define-public crate-usearch-0.11.1 (c (n "usearch") (v "0.11.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0qh0cfazdiajxgcpwf59pmn76y9bc0lz2vcic7gl7c7fh37y9r8f")))

(define-public crate-usearch-0.12.0 (c (n "usearch") (v "0.12.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1n49bx6pkx1n8sms1998l25w7323n7cimhbn4fwgxn48vjg37srq")))

(define-public crate-usearch-0.12.1 (c (n "usearch") (v "0.12.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1mzjmksm34ghk5k1w56qzhm14v7a4zps86949ggx45qc3gap6kj9")))

(define-public crate-usearch-0.12.2 (c (n "usearch") (v "0.12.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "17fm22jibyfmkfai78n62wqxbk99g6sfsl9v0cs5c9ddyxvgk2hq")))

(define-public crate-usearch-0.13.0 (c (n "usearch") (v "0.13.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1fbgxgjj8pzxn5k7sc8advaxgbyzf3p2j9q5i9qdvsrhlzl1lsmb")))

(define-public crate-usearch-0.14.0 (c (n "usearch") (v "0.14.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "18bd9rggcy7i4rfpsib2r4qmlivdww9n1zrphndw0jhfvcv7cwc7")))

(define-public crate-usearch-0.15.0 (c (n "usearch") (v "0.15.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "10fk492ay4w3k56c68fksr1hqzkf8q3syia4sczdxvbaw4p68h52")))

(define-public crate-usearch-0.15.1 (c (n "usearch") (v "0.15.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1fbs66zb2np5iva2rwvdkj1s665avk4kglxbv678cz83f7fvx6dy")))

(define-public crate-usearch-0.16.0 (c (n "usearch") (v "0.16.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0gr6ai703swhraw0gljss7cizv6vc1kx91g4ip51bxmwby3pb919")))

(define-public crate-usearch-0.16.1 (c (n "usearch") (v "0.16.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "05dr93l421hj87r44hv0n39s1vvv8wxiyvrr5nwz43pyadg56lni")))

(define-public crate-usearch-0.16.2 (c (n "usearch") (v "0.16.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0fjbpggv72nkn69g7fgbd6s10pzvfh9164njr3gwhd37jqa7xga4")))

(define-public crate-usearch-0.16.3 (c (n "usearch") (v "0.16.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0dqfbbmrhhk8kbngn4s4fal30paiphgrfd2jy47gnr3vw9dfhjlv")))

(define-public crate-usearch-0.17.0 (c (n "usearch") (v "0.17.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0xlsa0hrhkll56382v73h6ydwm9ynza0sq1mlsi8h0mdfckizy9l")))

(define-public crate-usearch-0.17.1 (c (n "usearch") (v "0.17.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "00gb13gjgigdlk1d786k98dycvcfalsmmaz635yrn7z311sysff4")))

(define-public crate-usearch-0.18.0 (c (n "usearch") (v "0.18.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "11vwqfy4nkl4abdf0bpmw36ynqcljf3kirff69db06mwqi04pgbj")))

(define-public crate-usearch-0.18.1 (c (n "usearch") (v "0.18.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "06bxqk9sayk9wiz5nmdbswhgn06biq2rx8r23ivag4dnw65s7hcs")))

(define-public crate-usearch-0.18.2 (c (n "usearch") (v "0.18.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0bml4hy2pqi0chly0wp1dx1hcmy1ppfng1i3zv0ppphs1br8k46d")))

(define-public crate-usearch-0.18.3 (c (n "usearch") (v "0.18.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0xffhw1m2wwh1icp1n6xrpis7a51286cix86aqmyk216dgc1z68p")))

(define-public crate-usearch-0.18.4 (c (n "usearch") (v "0.18.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1wsrnrf20xjgdq06p2c88gpc9zdbya3gpw5gk94xxd0sa83k8zcn")))

(define-public crate-usearch-0.18.5 (c (n "usearch") (v "0.18.5") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0h1s0q1226an67r1p592jay1j289v19rqnj5snp57lvjgk2wggjm")))

(define-public crate-usearch-0.18.6 (c (n "usearch") (v "0.18.6") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0ssb1mhj5fnh7l9w9cks2096g21fd07yl5pzxcbs5w4sf0pxx6r4")))

(define-public crate-usearch-0.18.8 (c (n "usearch") (v "0.18.8") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1scvs05g94np3sgf0gncn494jxdj2dgi9cqq5dr9lc09kz00yyc3")))

(define-public crate-usearch-0.19.0 (c (n "usearch") (v "0.19.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0gz3bfp8lf0wrm09signr4135pnh0fbc9j6xxm8g70rby1nmidpc")))

(define-public crate-usearch-0.19.1 (c (n "usearch") (v "0.19.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0pkq216lrfwrql18sc3mivk7zsq1jjim6rhy98ns97nfpfkwn596")))

(define-public crate-usearch-0.19.2 (c (n "usearch") (v "0.19.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1fkpxdfsj2sk4py2m58xh2nxz5dd4a8kapv1m1ak04w82584wzan")))

(define-public crate-usearch-0.19.3 (c (n "usearch") (v "0.19.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1jncsr5k2h5sqq77d77bwyy7fn8mh67gzwryr3xixjxk9yml105k")))

(define-public crate-usearch-0.20.0 (c (n "usearch") (v "0.20.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1grpfp8sq0a9m92wzi2d222hjklj93n8zhcq9zzx6wkrymqlkc5v")))

(define-public crate-usearch-0.21.0 (c (n "usearch") (v "0.21.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "07gksbd1dlys3bl9bz74f7mc2w27mrjf1q44j1f04jsr2s578wjf")))

(define-public crate-usearch-0.22.0 (c (n "usearch") (v "0.22.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0jsa2yp1qp262rkv6iyrnhqgv6cy0zqbldq2hc34rqfjd5w41dwj")))

(define-public crate-usearch-0.22.1 (c (n "usearch") (v "0.22.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1lc4a5bmy2z5xszlli2n4c3g4zh8c5b3cqggzmp03sh0labgn06v")))

(define-public crate-usearch-0.22.2 (c (n "usearch") (v "0.22.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "071m2rfxv0gpc7skxair6vy369pr9m4aclc5j6by75r3g6ywjdx2")))

(define-public crate-usearch-0.22.3 (c (n "usearch") (v "0.22.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0q85qcwkxrm40jrllhkja28qy5ilc9p08n1vr0pcc3pn0j3x9ckf")))

(define-public crate-usearch-1.1.0 (c (n "usearch") (v "1.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1jr942p61b7k915m3k5shricnylkmzkklvrk9cv3kp4q9j2kq5km")))

(define-public crate-usearch-1.1.1 (c (n "usearch") (v "1.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1dvr30jfjijzsqwvmy6sysyknsf148vgxxgaj8zh63bpwdvniqas")))

(define-public crate-usearch-1.2.0 (c (n "usearch") (v "1.2.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0iy3a1sx1bkx21x9ydbb6w4qjzpi2wd6sg0ba1bhrn9i2wc8qnlw")))

(define-public crate-usearch-1.2.1 (c (n "usearch") (v "1.2.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0qnw3gr7716j2n4vih3s5z6n0sc2bqy3jn1f3pir1znb8kfxj78n")))

(define-public crate-usearch-1.2.2 (c (n "usearch") (v "1.2.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1kc6dfzw4g378y0hlcs1ra1g56fpa3732p4pjajhipj2ywvfskjj")))

(define-public crate-usearch-1.3.0 (c (n "usearch") (v "1.3.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "15dv8l4j2zdhiqh8sbik8304pascwagmw6bsk3sgbwdfqkxqz2l9")))

(define-public crate-usearch-2.0.0 (c (n "usearch") (v "2.0.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1drvv2g2qlndsivw63qv1bbsh2a9v1vx9c3r8b8vd6006f519kjx")))

(define-public crate-usearch-2.0.1 (c (n "usearch") (v "2.0.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0qx7c4wqpcb7mfy6704himqlvf9adgmkp5a1v5hlf1q4q8r4nci9")))

(define-public crate-usearch-2.0.2 (c (n "usearch") (v "2.0.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0iskvaqb6f60iiyb9ifxb3653milfds4blcprxiffnxws7z2zkpy")))

(define-public crate-usearch-2.1.0 (c (n "usearch") (v "2.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "122kbh75np1ikjsdm7smrbnybzcpl88jlfac2jxsr0bv227d5c31")))

(define-public crate-usearch-2.1.1 (c (n "usearch") (v "2.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0rzarzs68f2jhy4akxzq9zy290sfblbypv7dfwik30p0a8v4c1f8")))

(define-public crate-usearch-2.1.2 (c (n "usearch") (v "2.1.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0w28icjr0jf84ixslf1m7drfj87a4p6mnzl2lfv7csk32mm8l2a8")))

(define-public crate-usearch-2.1.3 (c (n "usearch") (v "2.1.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1mn6jzpcw292hhki0xy05h6kfvbhdcz98awpvqzmwf767basfkxp")))

(define-public crate-usearch-2.2.0 (c (n "usearch") (v "2.2.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "10fah3rywcv16m1b6djd2k9pwq5nlraksr1jjwx1cr5wdi19bg9n")))

(define-public crate-usearch-2.2.1 (c (n "usearch") (v "2.2.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1d61yhwmr4r3ps3xsfwliz4hxsv7laqw5cn7bxja9ag60gxdngc6")))

(define-public crate-usearch-2.3.0 (c (n "usearch") (v "2.3.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0w6fzkg725k6qj22vcqnvx14r5zrmc67kqk6ik3qf857ng7d7bd8")))

(define-public crate-usearch-2.3.1 (c (n "usearch") (v "2.3.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0w8xggz3lg1pdjrck1sh06hrv517h514q1sf4hvsfx38lvdrpwlf")))

(define-public crate-usearch-2.3.2 (c (n "usearch") (v "2.3.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0g4ibn4901b2idmkkwz780pvgvanhi97s1bkb5hd30llyzr8qbr3")))

(define-public crate-usearch-2.5.0 (c (n "usearch") (v "2.5.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0kj4kb0v2rvxvmbf2rl2x2im74gzizsk52lrjmj8653qk7n36cj3")))

(define-public crate-usearch-2.5.1 (c (n "usearch") (v "2.5.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0zvhgnqcxhvvkd1vdgjsy9wbqalq7ynb74szhvc2jnm1w3z6v7m4")))

(define-public crate-usearch-2.6.0 (c (n "usearch") (v "2.6.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0l2ff5915wzxpv5c5bkh27s4sxjnr5qsw34mz5yv32s401189yq2")))

(define-public crate-usearch-2.6.1 (c (n "usearch") (v "2.6.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1sbhlmivz82s8jn0z6pvqqpl3winvldap73wsvpn0iw49lxw2pz8")))

(define-public crate-usearch-2.7.0 (c (n "usearch") (v "2.7.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1snd8wv5rpqv7qf2qw69vnb5hfkw1z5xb4pg9z84mkikd0vz6hz5")))

(define-public crate-usearch-2.7.1 (c (n "usearch") (v "2.7.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1vrnvpq1xczg0pfpflirpyfvyp8gxgldhkc83fs44j7y4pm1sn3j")))

(define-public crate-usearch-2.7.2 (c (n "usearch") (v "2.7.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0rkgvlql9ss0ak8hz1x8jc9hmdb53xnfnj5mm5awk11ijikr9lgd")))

(define-public crate-usearch-2.7.3 (c (n "usearch") (v "2.7.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "07k2ngabf2mrvchr2q045xkdn574675cfqzwapl0z7406br3lg62")))

(define-public crate-usearch-2.7.5 (c (n "usearch") (v "2.7.5") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "06jm7k6rl0r1dbi2cd1cglccb8bp1jbmvga39r2ajrr8nddvf3k4")))

(define-public crate-usearch-2.7.7 (c (n "usearch") (v "2.7.7") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1zn3zgfwix4qqclag8hkvljy7sh5vxxam86r8yz58148ms3ihlfh")))

(define-public crate-usearch-2.7.8 (c (n "usearch") (v "2.7.8") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1k9ww6d5nq07zn152d4z41nnnczlfpqrjbghjrnvk8igns6ccb00")))

(define-public crate-usearch-2.8.1 (c (n "usearch") (v "2.8.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "17wqfalv0dr7r5fp24d940xrkxlnf6lc10qnw82dz6wwbgb8cpm2")))

(define-public crate-usearch-2.8.2 (c (n "usearch") (v "2.8.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1xbvincij4vj098ib9kgd0cgjk5jj2fkz2m3y1ifpah01nbfp4fd")))

(define-public crate-usearch-2.8.3 (c (n "usearch") (v "2.8.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0bnm9373133hvcgipwyr3r849gmkjxbh1z8kl94b5p5nzypq6m49")))

(define-public crate-usearch-2.8.4 (c (n "usearch") (v "2.8.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1mr52sav1w23y5wsbha18if34l055a4hzc45cy795bsyfz7yk79y")))

(define-public crate-usearch-2.8.5 (c (n "usearch") (v "2.8.5") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1qipgiqlj9z5bc0px6ql6qyzlisw3f6gbnx5zryi9k8hl2r9nmv5")))

(define-public crate-usearch-2.8.6 (c (n "usearch") (v "2.8.6") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0jdxnzv5kl1v0h65ggmgwk3qf8viz12l8b0vf40c5isqy9yphv3n")))

(define-public crate-usearch-2.8.7 (c (n "usearch") (v "2.8.7") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "10ncks7f759k5p9l1wh09z25rcwfdnh7lvdb32byd30ava31cvsf")))

(define-public crate-usearch-2.8.8 (c (n "usearch") (v "2.8.8") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "05wivlaslhmfpxggvcxx90y6320iv7ibl16svs56w0j3ln0y67p9")))

(define-public crate-usearch-2.8.9 (c (n "usearch") (v "2.8.9") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "073d9lykg0fff6vzndhh1pwhfh15l8zzr5la8z3z8yjlpqzd6vl6")))

(define-public crate-usearch-2.8.10 (c (n "usearch") (v "2.8.10") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0gs448b28ad3bpzsrgsbj0m7wlizl568zd3v54zsbs1gjsdf3f1j")))

(define-public crate-usearch-2.8.11 (c (n "usearch") (v "2.8.11") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1640c4gmhipr02zah7iix99rkryfxd1l2v83anhzkxmhalhg4mii")))

(define-public crate-usearch-2.8.12 (c (n "usearch") (v "2.8.12") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0g7clqd7y61xz3fiiangyzbkdh0vhdk3lckhxvnw6gpripvdsnhk")))

(define-public crate-usearch-2.8.13 (c (n "usearch") (v "2.8.13") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "043r7q7amryr8dw90nwfbl4g141ngpgqqs3iqcisizjnv750ab8f")))

(define-public crate-usearch-2.8.14 (c (n "usearch") (v "2.8.14") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "023whq2z1h8iz26p8vwpxdmzdn8ijrn8rdrhsb3csfjgxlgh9v2l")))

(define-public crate-usearch-2.8.15 (c (n "usearch") (v "2.8.15") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1dywdbx6fi52ynhkb621gbw2lni29fdg7n3lv5qqjy63q8nb0m95")))

(define-public crate-usearch-2.8.16 (c (n "usearch") (v "2.8.16") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0ajm449i2j63wwwnfr1fmdzxyn8gaz9s3vs8gh2h35cc141xwhxr")))

(define-public crate-usearch-2.9.0 (c (n "usearch") (v "2.9.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1a1mc2321b58mvb20jgy2xgn2byjbahm48nsnw464258zagpw67b")))

(define-public crate-usearch-2.9.1 (c (n "usearch") (v "2.9.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0f3zs585535g1ai2jwqdsjpxjprkzf90iwyi508sjrapzy57kyfg")))

(define-public crate-usearch-2.9.2 (c (n "usearch") (v "2.9.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "17awhn5mmgcq9fz4s5cy9vkiix36yzm3ch3bw3bkfwas5173xz64")))

(define-public crate-usearch-2.10.0 (c (n "usearch") (v "2.10.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "02bnlg9l9611lpx2y5z2jll0vwm9b6gs6h14hlqb740w687vws0r")))

(define-public crate-usearch-2.10.1 (c (n "usearch") (v "2.10.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1vg0xrgb8zd3c6d140zcq5y7gpw5823w6w7cyzp5p16irm73mp5f")))

(define-public crate-usearch-2.10.2 (c (n "usearch") (v "2.10.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "066n27rbgjl24rg9bpk5cms873ygavfkcl4irzp1yzw3j4q1cxlz")))

(define-public crate-usearch-2.10.3 (c (n "usearch") (v "2.10.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "08pbysk6n9fci3gg9cf579m9jdvb34gbdbp30nc13h99sq399jv4")))

(define-public crate-usearch-2.10.4 (c (n "usearch") (v "2.10.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1pnmsim4f0hcn0v6agvk9mlahg5xrjybbiyqm3s80bmzxrnmjm9s")))

(define-public crate-usearch-2.11.0 (c (n "usearch") (v "2.11.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "18hx8khgha35aj404w1m4qf3nkfxzmgrf9askdi601xhywcvqczj") (f (quote (("simsimd") ("openmp") ("fp16lib") ("default" "simsimd"))))))

(define-public crate-usearch-2.11.1 (c (n "usearch") (v "2.11.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1xxdrpp5accgn855qz0c4gx4y4k4ybrn49lk82rp8xqpmnpxsrn3") (f (quote (("simsimd") ("openmp") ("fp16lib") ("default" "simsimd"))))))

(define-public crate-usearch-2.11.4 (c (n "usearch") (v "2.11.4") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0aypqlcij75riw7xzmh2crxn91jgr5ivy7q3p06gc6hmqzgx7736") (f (quote (("simsimd") ("openmp") ("fp16lib") ("default" "simsimd"))))))

(define-public crate-usearch-2.11.5 (c (n "usearch") (v "2.11.5") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1zqfn8aggq9m904a6rmna4ks460v3yjsqhvprpjykjvxwgmivdq0") (f (quote (("simsimd") ("openmp") ("fp16lib") ("default" "simsimd"))))))

(define-public crate-usearch-2.11.6 (c (n "usearch") (v "2.11.6") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0md4mhm39bjp2zfaxz41y1i7cx2nbwf3ns5ivmm9vbwdz703vdbv") (f (quote (("simsimd") ("openmp") ("fp16lib") ("default" "simsimd"))))))

(define-public crate-usearch-2.11.7 (c (n "usearch") (v "2.11.7") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1nm1g3qdbwp5h556shwj0syf4hpyswzh0qciwd6bvjnpdc7f6a4m") (f (quote (("simsimd") ("openmp") ("fp16lib") ("default" "simsimd"))))))

(define-public crate-usearch-2.12.0 (c (n "usearch") (v "2.12.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1681lspx1rd4abl4rny57imn5cj39fyy7yq462c66pprs3zz7whf") (f (quote (("simsimd") ("openmp") ("fp16lib") ("default" "simsimd"))))))

