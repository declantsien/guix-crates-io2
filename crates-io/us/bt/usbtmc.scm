(define-module (crates-io us bt usbtmc) #:use-module (crates-io))

(define-public crate-usbtmc-0.1.0 (c (n "usbtmc") (v "0.1.0") (h "1xnf6fx61k4c8fc49svf1cznk1cjsyi9nllmizdj51srr61ljiaq")))

(define-public crate-usbtmc-0.0.1 (c (n "usbtmc") (v "0.0.1") (h "0z7nfkby0kkk4fkgdlkmw1wx87vp9kigncrprkaakp2qn7ca70kd")))

