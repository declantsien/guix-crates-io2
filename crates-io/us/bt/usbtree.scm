(define-module (crates-io us bt usbtree) #:use-module (crates-io))

(define-public crate-usbtree-0.1.0 (c (n "usbtree") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rusb") (r "^0.8") (d #t) (k 0)) (d (n "usb-ids") (r "^0.2.1") (d #t) (k 0)))) (h "16x85zb8z8a3bfv0xg5d24p65zk4nhbmbsqis199k0s932viv3x3")))

