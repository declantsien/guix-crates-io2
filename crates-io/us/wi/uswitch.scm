(define-module (crates-io us wi uswitch) #:use-module (crates-io))

(define-public crate-uswitch-0.1.0 (c (n "uswitch") (v "0.1.0") (d (list (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "12wsyd7x4aagxayygrr9wdr6phfq2qxciypn4q755hr76y0i5sfh") (y #t)))

(define-public crate-uswitch-0.1.1 (c (n "uswitch") (v "0.1.1") (d (list (d (n "users") (r "^0.9") (d #t) (k 0)))) (h "1pwd4b1clj9qr0db6j42zqn44dzjsfz7f101fnp83z6l6izrwpv1")))

