(define-module (crates-io us os usos-rs) #:use-module (crates-io))

(define-public crate-usos-rs-1.0.0 (c (n "usos-rs") (v "1.0.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "oauth1-request") (r "^0.3.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "reqwest-oauth1") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1ijchm1lar0fl2xr5ygxxwqvdd9vb9i3l3xapaxwb9vl98ryr4ys") (s 2) (e (quote (("serde" "dep:serde"))))))

