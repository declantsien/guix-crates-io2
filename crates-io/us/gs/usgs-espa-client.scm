(define-module (crates-io us gs usgs-espa-client) #:use-module (crates-io))

(define-public crate-usgs-espa-client-0.5.0 (c (n "usgs-espa-client") (v "0.5.0") (d (list (d (n "mockito") (r "^0.27") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0kfpg2n9nxrcvm3dwazq5z8mcnim118s7silwmivf9yr3wjl6mjx")))

