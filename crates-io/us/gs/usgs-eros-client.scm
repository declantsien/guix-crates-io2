(define-module (crates-io us gs usgs-eros-client) #:use-module (crates-io))

(define-public crate-usgs-eros-client-0.5.0 (c (n "usgs-eros-client") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mockito") (r "^0.27") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "12cpll818w887fcbzf303w1whpkaxv8kcqdylb7a1kxpba8qfkh6")))

