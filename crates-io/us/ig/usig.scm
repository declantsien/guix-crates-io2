(define-module (crates-io us ig usig) #:use-module (crates-io))

(define-public crate-usig-0.1.0 (c (n "usig") (v "0.1.0") (h "01gy7gpjhxxndz2dncp9lj75rsm8m6ppgzn4i9yj6bfwn5brwqyq") (y #t)))

(define-public crate-usig-0.0.0-reserved (c (n "usig") (v "0.0.0-reserved") (h "0dkmgsfxlvsz62wzg9pih29zv1agrww2ahy2h7qs3mhg0ll9mb3m") (y #t)))

(define-public crate-usig-0.10.0 (c (n "usig") (v "0.10.0") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0") (f (quote ("serde" "rand_core"))) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "shared-ids") (r "^0.10.0") (d #t) (k 0)) (d (n "signature") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trait-alias-macro") (r "^0.10.0") (d #t) (k 0)))) (h "1mxzzjypjs5vfdvm8m40vy3afjmdgxgq2cq6basw5w7mn1kysvdn")))

(define-public crate-usig-0.11.0 (c (n "usig") (v "0.11.0") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0") (f (quote ("serde" "rand_core"))) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "shared-ids") (r "^0.11.0") (d #t) (k 0)) (d (n "signature") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trait-alias-macro") (r "^0.10.0") (d #t) (k 0)))) (h "1imxa7yyz4z4hg9jx5lnqfbzzycq9dyyv2f0fyv961np6wasx4ay")))

