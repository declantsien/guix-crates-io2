(define-module (crates-io us et uset) #:use-module (crates-io))

(define-public crate-uset-0.1.0 (c (n "uset") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0gczccx5p73c5ydrw6w9ryjk71glmj4jbgy9wv8ikrglrlsz3igf")))

