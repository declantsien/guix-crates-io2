(define-module (crates-io us b- usb-pd) #:use-module (crates-io))

(define-public crate-usb-pd-0.0.1 (c (n "usb-pd") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "defmt") (r "^0.3.5") (d #t) (k 0)) (d (n "fugit") (r "^0.3.7") (d #t) (k 0)) (d (n "heapless") (r "^0.7.16") (f (quote ("defmt-impl"))) (d #t) (k 0)) (d (n "proc-bitfield") (r "^0.2.4") (d #t) (k 0)))) (h "0hc267qjcri7gwkyiw0xhcq6557x869h1lcahacfglj4imnn4cm2")))

