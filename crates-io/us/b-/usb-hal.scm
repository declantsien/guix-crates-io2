(define-module (crates-io us b- usb-hal) #:use-module (crates-io))

(define-public crate-usb-hal-0.0.0-zero.0 (c (n "usb-hal") (v "0.0.0-zero.0") (d (list (d (n "libusb") (r "^0.3") (d #t) (k 2)))) (h "0rl745shpys74f31wlw528jhmw7sqpq6al685c3vcyx9f3m0na2f")))

(define-public crate-usb-hal-0.0.0-zero.1 (c (n "usb-hal") (v "0.0.0-zero.1") (d (list (d (n "libusb") (r "^0.3") (d #t) (k 2)))) (h "12bvd73ci2qazanzbv3b6w3hyd7m3807l2br38n8695rvgn3mbqs")))

