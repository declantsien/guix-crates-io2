(define-module (crates-io us b- usb-rfid-decoder) #:use-module (crates-io))

(define-public crate-usb-rfid-decoder-0.1.0 (c (n "usb-rfid-decoder") (v "0.1.0") (h "1j0x0l6fmmarfiaciwkbpfn7nj8rv8jkyh3y9i7x8vy1267hmix5")))

(define-public crate-usb-rfid-decoder-0.2.0 (c (n "usb-rfid-decoder") (v "0.2.0") (h "1xfkaza7ywn8a5w46zjgwdafm9plhd9d9w5w6dg50v4ki5npzwwv")))

(define-public crate-usb-rfid-decoder-0.2.1 (c (n "usb-rfid-decoder") (v "0.2.1") (h "1b8mgvlngrc3hqy865vs9j3qjhnj45arqqhdzlsglmfznj642mxh")))

