(define-module (crates-io us b- usb-host) #:use-module (crates-io))

(define-public crate-usb-host-0.1.0 (c (n "usb-host") (v "0.1.0") (h "1la467kp3df82vk3c4cxx7spyh64wmqwia00qkx4153n9l9s93dj")))

(define-public crate-usb-host-0.1.1 (c (n "usb-host") (v "0.1.1") (h "0wcwp7yqjlzxpg9ghjp0wm8iyg1inb2n8ls8mk128xx8c7gwjngk")))

(define-public crate-usb-host-0.1.2 (c (n "usb-host") (v "0.1.2") (h "080vhz0nfp0iiwlhl0ka1192ykzypng33nbvjqsnbckm9d1jlvl2")))

(define-public crate-usb-host-0.1.3 (c (n "usb-host") (v "0.1.3") (h "1wxqfb6bkpcl4nsf8lvglcwh7i2fdqi76chirkwsv8b9h1fbgii2")))

