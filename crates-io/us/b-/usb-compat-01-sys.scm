(define-module (crates-io us b- usb-compat-01-sys) #:use-module (crates-io))

(define-public crate-usb-compat-01-sys-0.1.0 (c (n "usb-compat-01-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1d2ikvkb0gaw270hfx1b2w03irjgfn56dbkzavlm368sia7pl857") (l "usb-0.1")))

(define-public crate-usb-compat-01-sys-0.1.1 (c (n "usb-compat-01-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12xm549cczxb0x09gkabrcx73nppimpa3p77r3wa81qssxlx96b9") (l "usb-0.1")))

(define-public crate-usb-compat-01-sys-0.2.0 (c (n "usb-compat-01-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.5") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1zkq4dx0g45q4nifh1vghgz1xmpvk5pl8lqrkd39ncrkmgb3fyn4") (l "usb-0.1")))

(define-public crate-usb-compat-01-sys-0.2.2 (c (n "usb-compat-01-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.5") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0bnn3fp00ck0g6spdh575sg3mwk3dzkqm4gd6i0cjazpdja1fr78") (l "usb-0.1")))

(define-public crate-usb-compat-01-sys-0.2.3 (c (n "usb-compat-01-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.5") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "082jmc0p2klr391bj458krj1q4kk8ng8j0wyiml9j84g5yjlna55") (f (quote (("logging") ("default")))) (l "usb-0.1")))

(define-public crate-usb-compat-01-sys-0.2.4 (c (n "usb-compat-01-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.5") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jrpj7papr3500yzvg2xgds8x85gh9cbh9w3j06lrxnivp44zpvl") (f (quote (("logging") ("default")))) (l "usb-0.1")))

