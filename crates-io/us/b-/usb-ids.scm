(define-module (crates-io us b- usb-ids) #:use-module (crates-io))

(define-public crate-usb-ids-0.0.1 (c (n "usb-ids") (v "0.0.1") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)) (d (n "regex") (r "^1.4") (d #t) (k 1)))) (h "0j9hl7ad1ysya1qik2cb7bqj26skd7c7l8hsnvj4vcha2cq0nk3b")))

(define-public crate-usb-ids-0.0.2 (c (n "usb-ids") (v "0.0.2") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)) (d (n "regex") (r "^1.4") (d #t) (k 1)))) (h "1949dj6w38aw4kvhny7mwyk55skwj4yv7flyxpxxlpxwkvi50ijn")))

(define-public crate-usb-ids-0.0.3 (c (n "usb-ids") (v "0.0.3") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)) (d (n "regex") (r "^1.4") (d #t) (k 1)))) (h "1hr89v3hf06s78njkcygjqr8nyhgny06i45m330n1dzqd4wx5dic")))

(define-public crate-usb-ids-0.1.0 (c (n "usb-ids") (v "0.1.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)) (d (n "regex") (r "^1.4") (d #t) (k 1)))) (h "16mszqc0z6yk3cw6f4h9zvhy7qailal9rwpbvhvwgargnb68hrjd")))

(define-public crate-usb-ids-0.2.0 (c (n "usb-ids") (v "0.2.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1.4") (d #t) (k 1)))) (h "05a5j9lqb4kq4shbisz6ylvnx8kshksrj991l0lp1vk0w8fyivzl")))

(define-public crate-usb-ids-0.2.1 (c (n "usb-ids") (v "0.2.1") (d (list (d (n "nom") (r "^6.2") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "1x5nyldq6x0izjcsy8wh13ymjmi7l8f6l710s0xswdhm1pz5bxdx")))

(define-public crate-usb-ids-0.2.2 (c (n "usb-ids") (v "0.2.2") (d (list (d (n "nom") (r "^6.2") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "0n3kq7zg5pdpzsp5skcpz6s8n011knr65gxhb4cjdn3i43a6bnx2")))

(define-public crate-usb-ids-0.2.3 (c (n "usb-ids") (v "0.2.3") (d (list (d (n "nom") (r "^6.2") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "192rl30zi2xbsglpy2kgmin5lddj0pmy0g407mira5kgb7q2442s")))

(define-public crate-usb-ids-0.2.4 (c (n "usb-ids") (v "0.2.4") (d (list (d (n "nom") (r "^6.2") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "0mjfsih608969silhrp1wnmw0q90iwh8rd4x7y6im02b88l1zidc")))

(define-public crate-usb-ids-0.2.5 (c (n "usb-ids") (v "0.2.5") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "067cz7kyjiv4cr14aim5lsknwlcc41nwq8p9ax05c9mdzykyq5aa")))

(define-public crate-usb-ids-2022.12.24 (c (n "usb-ids") (v "2022.12.24") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "10i50hnw7pvy2jd5sj6pb1dd72090mkng4ypwxzwjvnkvsba2cxn") (y #t)))

(define-public crate-usb-ids-1.2022.1224 (c (n "usb-ids") (v "1.2022.1224") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "03prnvxkw8alhh5bn00hsw9030y3ps2cyinmq6wmwfwz0irbk41b")))

(define-public crate-usb-ids-1.2023.0 (c (n "usb-ids") (v "1.2023.0") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "0rd7x1g410xhi8hdpsbpqjjf924zy7mfb29f8fircv4cwbwyfvhz")))

(define-public crate-usb-ids-1.2023.1 (c (n "usb-ids") (v "1.2023.1") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "18vwh8biy71i0wz62pa1m7j1nzj7z12qiy4cmvvbhxkl2f5kkcn3")))

(define-public crate-usb-ids-1.2023.2 (c (n "usb-ids") (v "1.2023.2") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "1wf46dd1r7j0iqjwd1157lmf9yr9vm8x57ya9zqg0hhfskxsz00g")))

(define-public crate-usb-ids-1.2023.3 (c (n "usb-ids") (v "1.2023.3") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "0d2di69sk3ninvb2k7fmfh2hniz0wnzar2dxi5n5qbwgi0p6cdm9")))

(define-public crate-usb-ids-1.2023.4 (c (n "usb-ids") (v "1.2023.4") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "1v4z6d03vyfz2y3p92pdk64kmc33783nkiwrx7qb7pg7riswwv00")))

(define-public crate-usb-ids-1.2023.5 (c (n "usb-ids") (v "1.2023.5") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "06c3g0wzpzj049057d9c8ragkkr9h23q44sizfqmgf4x6744g5vl")))

(define-public crate-usb-ids-1.2023.6 (c (n "usb-ids") (v "1.2023.6") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "0d2wq0m8kli4k13gh6nvqlvjby2dxxzj2s5wca52calba2j3fg9i")))

(define-public crate-usb-ids-1.2023.7 (c (n "usb-ids") (v "1.2023.7") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "1jqnn5pirzgdgnnx3d8s23rhgg24r3428q6f307bqvif18bz92ms")))

(define-public crate-usb-ids-1.2024.1 (c (n "usb-ids") (v "1.2024.1") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "0vmsw4wldqip28vnx4f5pfa05984vsq8rgl2fg31bmp2dihwp02x")))

(define-public crate-usb-ids-1.2024.2 (c (n "usb-ids") (v "1.2024.2") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "1mi4hi59z80nkb28brns7ph4ml6hscn2a818fid6hfgn86w55xf0")))

(define-public crate-usb-ids-1.2024.3 (c (n "usb-ids") (v "1.2024.3") (d (list (d (n "nom") (r "^7.0") (k 1)) (d (n "phf") (r "^0.11") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0") (d #t) (k 1)))) (h "11ap3yh985iw892pfdxggasdrv3cbbma1vjz8d690ksiyr8a7bnc")))

