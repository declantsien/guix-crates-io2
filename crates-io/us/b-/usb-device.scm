(define-module (crates-io us b- usb-device) #:use-module (crates-io))

(define-public crate-usb-device-0.1.0 (c (n "usb-device") (v "0.1.0") (d (list (d (n "heapless") (r "^0.4.1") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1ab7nnxq60k8chbfndm2r5ajr8qm0hawjppf0kzzjrklb25r8n10")))

(define-public crate-usb-device-0.2.0 (c (n "usb-device") (v "0.2.0") (d (list (d (n "libusb") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "0yidqzpz9sc3q3yizanbcrapcq5d911imvwfr1mxp20f1wk59x2d")))

(define-public crate-usb-device-0.2.1 (c (n "usb-device") (v "0.2.1") (d (list (d (n "libusb") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "151dfk3ykh13yan6xbpw1l64j2l4m2bq50whzkzq2qwx9hm3ddg4")))

(define-public crate-usb-device-0.2.2 (c (n "usb-device") (v "0.2.2") (d (list (d (n "libusb") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1s53z2pgy17cfxyagfag2y1zy5475x4ad5ap68awrvb67y8nlys4") (f (quote (("control-buffer-256"))))))

(define-public crate-usb-device-0.2.3 (c (n "usb-device") (v "0.2.3") (d (list (d (n "libusb") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1a0jc5jc062a78rzsc8lc0higljq1vgzlm378n3va7jrn917m7z0") (f (quote (("control-buffer-256"))))))

(define-public crate-usb-device-0.2.4 (c (n "usb-device") (v "0.2.4") (d (list (d (n "libusb") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "0fxabmqlxrr26lpnxb36wi7x251l4aj20ap21q7jm8affi04wxfg") (f (quote (("control-buffer-256")))) (y #t)))

(define-public crate-usb-device-0.2.5 (c (n "usb-device") (v "0.2.5") (d (list (d (n "libusb") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1kba1pfg32r3qyzl4rcil527kmn9h5jqnjaflgvrw39zladjnphf") (f (quote (("control-buffer-256"))))))

(define-public crate-usb-device-0.2.6 (c (n "usb-device") (v "0.2.6") (d (list (d (n "libusb") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1srkwrhqj1b13gk2grj7ld4y06j9vjjhw62hf5zg446rzvagcn6a") (f (quote (("test-class-high-speed") ("control-buffer-256"))))))

(define-public crate-usb-device-0.2.7 (c (n "usb-device") (v "0.2.7") (d (list (d (n "libusb") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1g9gy7pxa7i8xc3aaf5a1i0vk5dhrrw50yhxp8biy6n69ndyv7l4") (f (quote (("test-class-high-speed") ("control-buffer-256"))))))

(define-public crate-usb-device-0.2.8 (c (n "usb-device") (v "0.2.8") (d (list (d (n "libusb") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "0ib257k6yskvxyk23vkynna0p4k57dsjwpjj97m7883psh809sbb") (f (quote (("test-class-high-speed") ("control-buffer-256"))))))

(define-public crate-usb-device-0.2.9 (c (n "usb-device") (v "0.2.9") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusb") (r "^0.9.1") (d #t) (k 2)))) (h "0205a850jhw9gb96scwfx1k4iwpjvighvz3m80mjkda9r2nw6v0z") (f (quote (("test-class-high-speed") ("control-buffer-256"))))))

(define-public crate-usb-device-0.3.0 (c (n "usb-device") (v "0.3.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)) (d (n "portable-atomic") (r "^1.2.0") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusb") (r "^0.9.1") (d #t) (k 2)))) (h "0yqxin3hajab0fq7xj6ylfd7bv7kjs9bpmjdxlai2npm4xmbl34w") (f (quote (("test-class-high-speed") ("control-buffer-256"))))))

(define-public crate-usb-device-0.3.1 (c (n "usb-device") (v "0.3.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)) (d (n "portable-atomic") (r "^1.2.0") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusb") (r "^0.9.1") (d #t) (k 2)))) (h "18mn08hds4d233i44k58mvsv9yi1g2b708rgk2rpymkyaa7l6gp7") (f (quote (("test-class-high-speed") ("control-buffer-256"))))))

(define-public crate-usb-device-0.3.2 (c (n "usb-device") (v "0.3.2") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "portable-atomic") (r "^1.2.0") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusb") (r "^0.9.1") (d #t) (k 2)))) (h "19h6bxdk2lsydd387n0rz95hny9xx4khzfb8a440kfxgrhd6p0cq") (f (quote (("test-class-high-speed") ("control-buffer-256")))) (s 2) (e (quote (("log" "dep:log"))))))

