(define-module (crates-io us bh usbhid) #:use-module (crates-io))

(define-public crate-usbhid-0.1.0 (c (n "usbhid") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)))) (h "1xjp8ik2alx7087pqjshp1dxhmpr7cxkzbnanfcwrxpmd88mwk3j")))

