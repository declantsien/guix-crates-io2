(define-module (crates-io us bh usbh) #:use-module (crates-io))

(define-public crate-usbh-0.1.0 (c (n "usbh") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.5") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "fugit") (r "^0.3.7") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (k 0)) (d (n "usb-device") (r "^0.2.9") (f (quote ("defmt"))) (d #t) (k 0)))) (h "0s2rrrqyqzgpcaqbn07j2zd86z2z49k43inndikk6z6lg7h1wsi2")))

