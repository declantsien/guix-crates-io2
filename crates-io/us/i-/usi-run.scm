(define-module (crates-io us i- usi-run) #:use-module (crates-io))

(define-public crate-usi-run-0.1.0 (c (n "usi-run") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "shogi") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)))) (h "0f2wlqf9837m6mc5rkmf1ynyl6qll310v9rwy6c61026qwa5jcmq")))

(define-public crate-usi-run-0.2.0 (c (n "usi-run") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "shogi") (r "^0.4") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)))) (h "1bd09sg38qpmgzz1nfiykk9kccsj11wg3hdwxcg6k5ng637jynvv")))

(define-public crate-usi-run-0.2.1 (c (n "usi-run") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "shogi") (r "^0.6") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)))) (h "1r9qac4c35ikrlzp4b8jvnx9vfry8q9d6s8k6d29j6c5z91zhmib")))

(define-public crate-usi-run-0.2.2 (c (n "usi-run") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "shogi") (r "^0.6") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)))) (h "0fywgz6108jgilh39c5h1vipgrzw05phcj7ni6bn6rbriynhw9sk")))

(define-public crate-usi-run-0.3.0 (c (n "usi-run") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.3") (d #t) (k 0)) (d (n "shogi") (r "^0.6") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)))) (h "1brmn7fgbh11n02n381znxcwn148fiq1255jy2vdcjd3q2nxmlp0")))

(define-public crate-usi-run-0.4.0 (c (n "usi-run") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.3") (d #t) (k 0)) (d (n "shogi") (r "^0.7") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)) (d (n "usi") (r "^0.1") (d #t) (k 0)))) (h "0m99bg0sinpiix3g24v9nzpybfsl59jav3fgnipc6psvzan1svy9")))

(define-public crate-usi-run-0.5.0 (c (n "usi-run") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csa") (r "^0.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.3") (d #t) (k 0)) (d (n "shogi") (r "^0.7") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)) (d (n "usi") (r "^0.1") (d #t) (k 0)))) (h "07bvfxpnkgaiag43594sv0wclrw3f15g4ynxq996zs1bvpqg4mph")))

(define-public crate-usi-run-0.6.0 (c (n "usi-run") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.7") (d #t) (k 0)) (d (n "csa") (r "^0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "shogi") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "usi") (r "^0.2") (d #t) (k 0)))) (h "0maj7ygdcwlwxllpygfzgi0ids9wrpdyi7byxmdpzbnwcnvxzh3m")))

(define-public crate-usi-run-0.7.0 (c (n "usi-run") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "csa") (r "^0.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "shogi") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "usi") (r "^0.3") (d #t) (k 0)))) (h "1fr2pc70gw9gd7v8gpzpv0x9dkxl3xzryn7ywks1yn4b0qp9czpx")))

(define-public crate-usi-run-0.8.0 (c (n "usi-run") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "csa") (r "^0.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "shogi") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "usi") (r "^0.3") (d #t) (k 0)))) (h "1jzwja737720f2zsszvwi7nccy0jjxyz17zpxm1ppni5p2b976b4")))

(define-public crate-usi-run-0.8.1 (c (n "usi-run") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "csa") (r "^0.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "shogi") (r "^0.10") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "usi") (r "^0.4") (d #t) (k 0)))) (h "11m0xsh3zn8xqbsclkkvdnsxschmhpx6wfl23kj2gdk8z74lgmn3")))

(define-public crate-usi-run-0.9.0 (c (n "usi-run") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "csa") (r "^0.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "shogi") (r "^0.10") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "usi") (r "^0.5") (d #t) (k 0)))) (h "1wm80wdf5r4i5h0rk3psi1xwvpvsx8pxp1i9ql13x874djs9ffsq")))

(define-public crate-usi-run-0.9.1 (c (n "usi-run") (v "0.9.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "csa") (r "^1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.11") (d #t) (k 0)) (d (n "shogi") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "usi") (r "^0.5") (d #t) (k 0)))) (h "1h4izzg3198yq2jbpc421zbdg6nl5qjxajsv0sgs000lafyp3ybm")))

(define-public crate-usi-run-0.9.2 (c (n "usi-run") (v "0.9.2") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "console") (r "^0.9") (d #t) (k 0)) (d (n "csa") (r "^1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "shogi") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "usi") (r "^0.6") (d #t) (k 0)))) (h "16422f6w4rcfhi1c8sh7hg4byyl2cs168lpv7smczl2bpdqb6ayc")))

