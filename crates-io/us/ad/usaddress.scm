(define-module (crates-io us ad usaddress) #:use-module (crates-io))

(define-public crate-usaddress-0.1.0 (c (n "usaddress") (v "0.1.0") (d (list (d (n "crfs") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1h9bp1fndqpxk9sw88664rd6ydvv6844700w9pv896jrznfqfc4d")))

