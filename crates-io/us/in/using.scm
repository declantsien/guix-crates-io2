(define-module (crates-io us in using) #:use-module (crates-io))

(define-public crate-using-0.0.0 (c (n "using") (v "0.0.0") (h "0v6ixrjgs1rh4ffqlr9gvzf1586qmplswvh96fd472yiv6harr6j")))

(define-public crate-using-0.1.0 (c (n "using") (v "0.1.0") (h "0bijl96fndqqzilbmqmwfw6i0kjl0flflr0faw79mv308byxgjx0")))

