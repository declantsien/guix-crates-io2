(define-module (crates-io us rs usrsctp-sys) #:use-module (crates-io))

(define-public crate-usrsctp-sys-0.1.0 (c (n "usrsctp-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0x2qizw1nhrasv5qrak1nvw6q4g9r96p8gkw2zz88amj79cvh9cz") (l "usrsctp")))

(define-public crate-usrsctp-sys-0.2.0 (c (n "usrsctp-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0qxykc93filzy4vc6f2483acg95160bxzx88h4a874gm7n4wli83") (l "usrsctp")))

