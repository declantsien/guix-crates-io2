(define-module (crates-io us yn usync) #:use-module (crates-io))

(define-public crate-usync-0.0.0 (c (n "usync") (v "0.0.0") (h "1nmxdafizl36l5x3bsyzbhnv56j91bppl1m00bvzv99s6d4pbf4w")))

(define-public crate-usync-0.1.0 (c (n "usync") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1q977v79hwa9iyjmb5xlq9vjnvlgw6r44zl5qcdjvcsgdxyn7v1w") (f (quote (("send_guard") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-usync-0.1.1 (c (n "usync") (v "0.1.1") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1bykn0d4pdh5xi3iyhj9lankgy1g7czh3wayhd2lwgal4nfs07mk") (f (quote (("send_guard") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-usync-0.2.0 (c (n "usync") (v "0.2.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0h0af0q45p0cz29slnk690v3ls7k6yk9nf583kzyhw78gx0a95dr") (f (quote (("send_guard") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-usync-0.2.1 (c (n "usync") (v "0.2.1") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1q00zvd82b1c90bpwrm5d3rvmwvjq06bv2diaskilarxd8zbdhq8") (f (quote (("send_guard") ("nightly" "lock_api/nightly") ("default"))))))

