(define-module (crates-io gs d- gsd-parser) #:use-module (crates-io))

(define-public crate-gsd-parser-0.1.0 (c (n "gsd-parser") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "insta") (r "^1.24.1") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "01q5q1nikn17ymwwwr3c5j1xc0k75pzsqhywhqkm2ml78qhgjvm6")))

(define-public crate-gsd-parser-0.1.1 (c (n "gsd-parser") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "insta") (r "^1.24.1") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "1sa4b8bk8y7c1adm6gj2iqina1bdhmhlhv226942aq973mjx8rhn")))

(define-public crate-gsd-parser-0.2.0 (c (n "gsd-parser") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "insta") (r "^1.24.1") (d #t) (k 2)) (d (n "pest") (r "^2.5.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "148m10dyk8yhzqwsyh80f3bvl4rgnrggfl5klbm32w8bnrmcj48j")))

