(define-module (crates-io gs li gslib) #:use-module (crates-io))

(define-public crate-gslib-0.1.0 (c (n "gslib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "google_auth") (r "^0.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("stream"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1n61svjfaziqhb8hhlwarqqf7m316c4cw2n0d97bpp6hm3cc8d7m")))

