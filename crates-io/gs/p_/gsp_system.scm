(define-module (crates-io gs p_ gsp_system) #:use-module (crates-io))

(define-public crate-gsp_system-0.1.0 (c (n "gsp_system") (v "0.1.0") (h "03am9mr52bd6gs21j6wgzzsb77j8aszsaaklmn66msgcfp8qgm5y")))

(define-public crate-gsp_system-0.1.1 (c (n "gsp_system") (v "0.1.1") (h "1xphahn18i9gn53c0zsjw3ps1mgz75ja8fiv2k12l50bhkwncsrq")))

(define-public crate-gsp_system-0.1.2 (c (n "gsp_system") (v "0.1.2") (d (list (d (n "objc2") (r "^0.5.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_LibraryLoader" "Win32_Graphics_Gdi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1rqi77h9d6rwvi84irdw8crp032nwz29hrwmlklw44clry8ccy5g")))

