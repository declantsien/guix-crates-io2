(define-module (crates-io gs kr gskrm) #:use-module (crates-io))

(define-public crate-gskrm-0.1.0 (c (n "gskrm") (v "0.1.0") (d (list (d (n "gskrm-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0y9xmk9792v2nzcb2i9k1w6bzbqkq754ijm9cxy1sgdk8fl4jcaz")))

(define-public crate-gskrm-0.1.1 (c (n "gskrm") (v "0.1.1") (d (list (d (n "gskrm-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1wi9c3pa3589ymlnqk7zb6r8hkv77ihchjkfzk8wmkl3gc85pvmq")))

(define-public crate-gskrm-0.1.2 (c (n "gskrm") (v "0.1.2") (d (list (d (n "gskrm-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19lczw8cnpx6zqk5v3k5xnvvsg411jfs2agq5p02anj2kdbg4nc2")))

(define-public crate-gskrm-0.1.3 (c (n "gskrm") (v "0.1.3") (d (list (d (n "gskrm-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0q220n8flsyrq5rinfwjg8r5z90m2dj95yfa0c4jc8ayks5lq2fn")))

(define-public crate-gskrm-0.1.4 (c (n "gskrm") (v "0.1.4") (d (list (d (n "gskrm-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1p7yg1i9yr739db8ryhvazjzfwjdjijymh5avlcnrwm902qv2kwk")))

