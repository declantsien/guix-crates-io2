(define-module (crates-io gs tr gstreamer-analytics-sys) #:use-module (crates-io))

(define-public crate-gstreamer-analytics-sys-0.0.0 (c (n "gstreamer-analytics-sys") (v "0.0.0") (h "13ms3fhdv7zmx885sqr5fy0c4adrkdgx4x5v7ksfb62mld8m73b6")))

(define-public crate-gstreamer-analytics-sys-0.22.0 (c (n "gstreamer-analytics-sys") (v "0.22.0") (d (list (d (n "glib-sys") (r "^0.19") (d #t) (k 0)) (d (n "gstreamer-sys") (r "^0.22") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0zgc1j6lzmj45vwj4l4vcry6qwdhy3rp0n1z9prqgnnrilxbablq") (r "1.70")))

(define-public crate-gstreamer-analytics-sys-0.22.3 (c (n "gstreamer-analytics-sys") (v "0.22.3") (d (list (d (n "glib-sys") (r "^0.19") (d #t) (k 0)) (d (n "gstreamer-sys") (r "^0.22") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1hvmk0j1k4cncwl0z3i868jivlabmwg614r5d82wl93v2hdshnli") (r "1.70")))

(define-public crate-gstreamer-analytics-sys-0.22.5 (c (n "gstreamer-analytics-sys") (v "0.22.5") (d (list (d (n "glib-sys") (r "^0.19") (d #t) (k 0)) (d (n "gstreamer-sys") (r "^0.22") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1jcw45q40kapkvzvaj6crk7yfb0pavkc1n0j25jy7h3fm9j7s0dc") (r "1.70")))

