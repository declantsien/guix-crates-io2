(define-module (crates-io gs tr gstreamer-tag) #:use-module (crates-io))

(define-public crate-gstreamer-tag-0.0.0 (c (n "gstreamer-tag") (v "0.0.0") (h "1drzl0zkbd4f3sk7nhjhm352pln8c70zy8xj00jns2pmai6cpl9v")))

(define-public crate-gstreamer-tag-0.22.0 (c (n "gstreamer-tag") (v "0.22.0") (d (list (d (n "ffi") (r "^0.22") (d #t) (k 0) (p "gstreamer-tag-sys")) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "gst") (r "^0.22") (d #t) (k 0) (p "gstreamer")) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s2c6ifnh3mx53hzr2343zg9ggrvfjdkdl57qhsk2x82hb9n55vy") (f (quote (("v1_24" "gst/v1_24" "ffi/v1_24" "v1_22") ("v1_22" "gst/v1_22" "ffi/v1_22" "v1_20") ("v1_20" "gst/v1_20" "ffi/v1_20" "v1_18") ("v1_18" "gst/v1_18" "ffi/v1_18" "v1_16") ("v1_16" "gst/v1_16" "ffi/v1_16") ("default")))) (r "1.70")))

