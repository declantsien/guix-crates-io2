(define-module (crates-io gs tr gstreamer-analytics) #:use-module (crates-io))

(define-public crate-gstreamer-analytics-0.0.0 (c (n "gstreamer-analytics") (v "0.0.0") (h "1a8c92yshbv0b82by5mjvdnkp8xsymwd6iphzc48slml7avv319b")))

(define-public crate-gstreamer-analytics-0.22.0 (c (n "gstreamer-analytics") (v "0.22.0") (d (list (d (n "ffi") (r "^0.22") (d #t) (k 0) (p "gstreamer-analytics-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "gst") (r "^0.22") (d #t) (k 0) (p "gstreamer")) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xqhks3y7nalh54gax8j5vbvx8jah2rmr2mfqcin7i1wq79n67mj") (f (quote (("default")))) (r "1.70")))

(define-public crate-gstreamer-analytics-0.22.1 (c (n "gstreamer-analytics") (v "0.22.1") (d (list (d (n "ffi") (r "^0.22") (d #t) (k 0) (p "gstreamer-analytics-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "gst") (r "^0.22") (d #t) (k 0) (p "gstreamer")) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l674fnccs5912xc62fs9x7d8rvns06jmdw13cny740x5gwky5qs") (f (quote (("default")))) (r "1.70")))

(define-public crate-gstreamer-analytics-0.22.3 (c (n "gstreamer-analytics") (v "0.22.3") (d (list (d (n "ffi") (r "^0.22") (d #t) (k 0) (p "gstreamer-analytics-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "gst") (r "^0.22") (d #t) (k 0) (p "gstreamer")) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0frw9wva9fb4b96bj394qyljv76ywv3blq718qksid1r84yslcsg") (f (quote (("default")))) (r "1.70")))

