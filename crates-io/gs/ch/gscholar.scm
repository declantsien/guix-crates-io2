(define-module (crates-io gs ch gscholar) #:use-module (crates-io))

(define-public crate-gscholar-0.1.0 (c (n "gscholar") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kzk9z3b72pzri35i75vi355wcsr46a4krx6rn2g3g0vvgxz6plv")))

