(define-module (crates-io gs v- gsv-culture-ships) #:use-module (crates-io))

(define-public crate-gsv-culture-ships-0.1.0 (c (n "gsv-culture-ships") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1zm282aaqmiaj2dz0klqqyb0sgx1c0pfbpj5s06fz28b1av4hdlp")))

(define-public crate-gsv-culture-ships-0.2.0 (c (n "gsv-culture-ships") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03hjgjvrs7010ahq3xwyfg7k7cv03mhcnz7vbiy9np6ipzaxlff3")))

