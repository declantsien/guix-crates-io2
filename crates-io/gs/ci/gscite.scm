(define-module (crates-io gs ci gscite) #:use-module (crates-io))

(define-public crate-gscite-1.0.0 (c (n "gscite") (v "1.0.0") (d (list (d (n "cssparser") (r "^0.29.6") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("gzip" "rustls-tls" "cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("rt-multi-thread" "macros" "signal" "time"))) (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "13dmaxb6n5asjkb07x1ra2d04zjh87z7fagargz4wcc02p5ibxvz")))

