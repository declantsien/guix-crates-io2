(define-module (crates-io gs e_ gse_do_not_disturb) #:use-module (crates-io))

(define-public crate-gse_do_not_disturb-1.0.0 (c (n "gse_do_not_disturb") (v "1.0.0") (d (list (d (n "dconf_rs") (r "^0.1.0") (d #t) (k 0)))) (h "1vg16rlzn198jb2xc2ddx2ihkrnn2vba332lb5bxrav4jndh3s28")))

(define-public crate-gse_do_not_disturb-1.0.1 (c (n "gse_do_not_disturb") (v "1.0.1") (d (list (d (n "dconf_rs") (r "^0.1.0") (d #t) (k 0)))) (h "1lnvrfdh3xx14jrfhdf0s72rqj7rzb812817kxmh7yjvs53zlzxs")))

(define-public crate-gse_do_not_disturb-1.0.2 (c (n "gse_do_not_disturb") (v "1.0.2") (d (list (d (n "dconf_rs") (r "^0.1.0") (d #t) (k 0)))) (h "0p77bd2da6q3ic7ja5fzb6hm042rlcad6qqjyx4pc1h9fsyvrcg9")))

(define-public crate-gse_do_not_disturb-1.1.0 (c (n "gse_do_not_disturb") (v "1.1.0") (d (list (d (n "dconf_rs") (r "^0.2.0") (d #t) (k 0)))) (h "0gahwxpblykd6vwvdaxh5mpkwghg3v3wggdwxwcjv6w3r6i649cl")))

(define-public crate-gse_do_not_disturb-1.2.0 (c (n "gse_do_not_disturb") (v "1.2.0") (d (list (d (n "dconf_rs") (r "^0.3.0") (d #t) (k 0)))) (h "0gjd3d4mljjiwcnm31h2g2xxpzw90r8slgxkzv9fcnabfqzmd096")))

