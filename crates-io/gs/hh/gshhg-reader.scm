(define-module (crates-io gs hh gshhg-reader) #:use-module (crates-io))

(define-public crate-gshhg-reader-0.1.1 (c (n "gshhg-reader") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1rgid3sfwvrdlmq4pfjcijjb82jfp6v5lwcy6wmqy39iwba1rq03")))

(define-public crate-gshhg-reader-0.2.0 (c (n "gshhg-reader") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0iyh8b5s0cghcyc1pqlngzl227w5ldy83lv162l5mz201ri8w3hw")))

(define-public crate-gshhg-reader-0.2.1 (c (n "gshhg-reader") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "19vysx7fdihj271akcigiiaqwnw2sa33xmh1gz129pcby0hqnn3n")))

