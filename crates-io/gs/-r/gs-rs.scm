(define-module (crates-io gs -r gs-rs) #:use-module (crates-io))

(define-public crate-gs-rs-0.1.0 (c (n "gs-rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "kiss3d") (r "^0.25.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 2)) (d (n "nalgebra") (r "^0.22.0") (f (quote ("sparse"))) (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0nkppswyg96zxpr9y36bhwxwx7zcgvmnc9rbnfhvyck7qqikib7m")))

