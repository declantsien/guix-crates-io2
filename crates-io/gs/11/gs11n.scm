(define-module (crates-io gs #{11}# gs11n) #:use-module (crates-io))

(define-public crate-gs11n-0.1.0 (c (n "gs11n") (v "0.1.0") (d (list (d (n "ctor") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.2") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "test-cdylib") (r "^1.1.0") (d #t) (k 2)))) (h "08fh5pvfyhrg968vpfdq3rfdgdv2yix6xgx6mkb7pc88ly7xx5bi")))

(define-public crate-gs11n-0.2.0 (c (n "gs11n") (v "0.2.0") (d (list (d (n "ctor") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.2") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "test-cdylib") (r "^1.1.0") (d #t) (k 2)))) (h "0wiai71cs696sn6ga9l47j3q3gsvayhrfvhz0pbfzp1fx4hpc2y0")))

(define-public crate-gs11n-0.3.0 (c (n "gs11n") (v "0.3.0") (d (list (d (n "ctor") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.2") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "test-cdylib") (r "^1.1.0") (d #t) (k 2)))) (h "1v17ky3sbm14vs48ba07hhkdnqasgnj22zzmc76d9id4fl3z71x9")))

(define-public crate-gs11n-0.3.1 (c (n "gs11n") (v "0.3.1") (d (list (d (n "ctor") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.2") (d #t) (k 2)) (d (n "test-cdylib") (r "^1.1.0") (d #t) (k 2)))) (h "01plxhfm2ccsgkrb57qjwnq88ai8337l2z4kcld031glqfdqijx6")))

