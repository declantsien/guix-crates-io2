(define-module (crates-io gs s- gss-api) #:use-module (crates-io))

(define-public crate-gss-api-0.0.0 (c (n "gss-api") (v "0.0.0") (h "06f467gawrjmqh2qdy8r702dm2jxvhhgkjakk53d731adilhbznj") (r "1.56")))

(define-public crate-gss-api-0.1.0 (c (n "gss-api") (v "0.1.0") (d (list (d (n "der") (r "^0.7") (f (quote ("oid" "alloc"))) (d #t) (k 0)) (d (n "der") (r "^0.7") (f (quote ("oid" "pem" "alloc"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "spki") (r "^0.7") (d #t) (k 0)) (d (n "x509-cert") (r "^0.2") (k 0)) (d (n "x509-cert") (r "^0.2") (f (quote ("pem"))) (k 2)))) (h "0nmannc0r8h5vx1mka6v2pnwhnl6yi5bwdkhgwkmr4yyslpkdmzf") (f (quote (("rfc2478")))) (r "1.65")))

