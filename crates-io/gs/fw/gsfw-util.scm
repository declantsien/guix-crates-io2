(define-module (crates-io gs fw gsfw-util) #:use-module (crates-io))

(define-public crate-gsfw-util-0.1.0 (c (n "gsfw-util") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0cg7r1kg8bkslkwl0xdvf370ba046nhypn06rny3azs1c7aw85ik")))

