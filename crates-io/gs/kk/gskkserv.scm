(define-module (crates-io gs kk gskkserv) #:use-module (crates-io))

(define-public crate-gskkserv-0.1.2 (c (n "gskkserv") (v "0.1.2") (d (list (d (n "daemonize") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^0.8.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1pifbsll9nvgda8v7y1abn5mh4kj1qs9wx398kfvzqbjw3382hkz")))

(define-public crate-gskkserv-0.1.3 (c (n "gskkserv") (v "0.1.3") (d (list (d (n "daemonize") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^0.8.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1ms2b4j4wr2i198llmrq7vv539y0hawmzb1byrych12yg2qb3c7n")))

(define-public crate-gskkserv-0.1.4 (c (n "gskkserv") (v "0.1.4") (d (list (d (n "daemonize") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^1.1.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0y9jxycw6c3ns6sa56qckjk9slqqabbj3wh4ksxxfvifibk3fgr9")))

