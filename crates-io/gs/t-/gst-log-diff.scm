(define-module (crates-io gs t- gst-log-diff) #:use-module (crates-io))

(define-public crate-gst-log-diff-0.1.0 (c (n "gst-log-diff") (v "0.1.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "gst-log-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2.16") (d #t) (k 0)) (d (n "term") (r "^0.5.2") (d #t) (k 0)))) (h "050nz0xf59kyd2cq4r6fd467jxwrpkhr5rgs2ja5bzy2vcyv7qj1")))

(define-public crate-gst-log-diff-0.1.1 (c (n "gst-log-diff") (v "0.1.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "gst-log-parser") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2.16") (d #t) (k 0)) (d (n "term") (r "^0.5.2") (d #t) (k 0)))) (h "01jv0gaxq82xr5z044v56wbri632v52aigjhhpwxadlc4c5pna59")))

(define-public crate-gst-log-diff-0.1.2 (c (n "gst-log-diff") (v "0.1.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "gst-log-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.2.16") (d #t) (k 0)) (d (n "term") (r "^0.5.2") (d #t) (k 0)))) (h "04vbbf7rv669ylsib2287xmipvplqrzjrljw0h6wz3awi8vcnw7a")))

