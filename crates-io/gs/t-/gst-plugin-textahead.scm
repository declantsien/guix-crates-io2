(define-module (crates-io gs t- gst-plugin-textahead) #:use-module (crates-io))

(define-public crate-gst-plugin-textahead-0.8.0 (c (n "gst-plugin-textahead") (v "0.8.0") (d (list (d (n "gst") (r "^0.18") (d #t) (k 0) (p "gstreamer")) (d (n "gst-plugin-version-helper") (r "^0.7") (d #t) (k 1)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)))) (h "0lac1rgjdpri6h2hl4bd6sn7k6bwiw3cw7d3w9h5vvs0mxwhjism") (f (quote (("static" "gst/v1_14") ("capi")))) (r "1.56")))

(define-public crate-gst-plugin-textahead-0.8.1 (c (n "gst-plugin-textahead") (v "0.8.1") (d (list (d (n "gst") (r "^0.18") (d #t) (k 0) (p "gstreamer")) (d (n "gst-plugin-version-helper") (r "^0.7") (d #t) (k 1)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)))) (h "1xqzn5zjn0qjwidi4sbykdm1zbhyx0rplg3pakdp4fxnp8z6fv58") (f (quote (("static" "gst/v1_14") ("capi")))) (r "1.56")))

(define-public crate-gst-plugin-textahead-0.9.0 (c (n "gst-plugin-textahead") (v "0.9.0") (d (list (d (n "gst") (r "^0.19.1") (d #t) (k 0) (p "gstreamer")) (d (n "gst-plugin-version-helper") (r "^0.7") (d #t) (k 1)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)))) (h "1v2rwr65ixp8vn1n88msj2ab4dl6mrlpf8jmzld8m2hmcvd0kjn0") (f (quote (("static") ("doc" "gst/v1_18") ("capi")))) (r "1.63")))

(define-public crate-gst-plugin-textahead-0.9.3 (c (n "gst-plugin-textahead") (v "0.9.3") (d (list (d (n "gst") (r "^0.19.1") (d #t) (k 0) (p "gstreamer")) (d (n "gst-plugin-version-helper") (r "^0.7") (d #t) (k 1)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)))) (h "08qm34gg8fn5kf6jpigapfnwv0y0w8844xxj8mmf70aflwc09wsi") (f (quote (("static") ("doc" "gst/v1_18") ("capi")))) (r "1.63")))

(define-public crate-gst-plugin-textahead-0.9.5 (c (n "gst-plugin-textahead") (v "0.9.5") (d (list (d (n "gst") (r "^0.19.1") (d #t) (k 0) (p "gstreamer")) (d (n "gst-plugin-version-helper") (r "^0.7") (d #t) (k 1)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)))) (h "0khy1kx77z1wihn87f4kzm74djksc8zlsyirffm1s77f1p6wykdk") (f (quote (("static") ("doc" "gst/v1_18") ("capi")))) (r "1.63")))

(define-public crate-gst-plugin-textahead-0.10.0 (c (n "gst-plugin-textahead") (v "0.10.0") (d (list (d (n "gst") (r "^0.20") (d #t) (k 0) (p "gstreamer")) (d (n "gst-plugin-version-helper") (r "^0.7") (d #t) (k 1)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)))) (h "0q8n4bzn2iyhs5idying9ipz9kmsa24jrg7hrsbz146zx3y4vwpj") (f (quote (("static") ("doc" "gst/v1_18") ("capi")))) (r "1.63")))

(define-public crate-gst-plugin-textahead-0.11.0 (c (n "gst-plugin-textahead") (v "0.11.0") (d (list (d (n "gst") (r "^0.21") (d #t) (k 0) (p "gstreamer")) (d (n "gst-plugin-version-helper") (r "^0.7") (d #t) (k 1)))) (h "0ffqxmqb6axl7wrfh2kddx11q6jg4h4zpzznqhdbr7hgv2rb8qc8") (f (quote (("static") ("doc" "gst/v1_18") ("capi")))) (r "1.70")))

(define-public crate-gst-plugin-textahead-0.11.1 (c (n "gst-plugin-textahead") (v "0.11.1") (d (list (d (n "gst") (r "^0.21") (d #t) (k 0) (p "gstreamer")) (d (n "gst-plugin-version-helper") (r "^0.8") (d #t) (k 1)))) (h "0cg4vvi3wi61smh0gnsz48zcvg0rxqbn9cz3q822a7xxpdayw8cg") (f (quote (("static") ("doc" "gst/v1_18") ("capi")))) (r "1.70")))

(define-public crate-gst-plugin-textahead-0.12.0 (c (n "gst-plugin-textahead") (v "0.12.0") (d (list (d (n "gst") (r "^0.22") (d #t) (k 0) (p "gstreamer")) (d (n "gst-plugin-version-helper") (r "^0.8") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "1j3pjdbngh4nahk8an2gf1mx0cyp1fmjq87bfd0xr1c7pf3rdqb2") (f (quote (("static") ("doc" "gst/v1_18") ("capi")))) (r "1.70")))

