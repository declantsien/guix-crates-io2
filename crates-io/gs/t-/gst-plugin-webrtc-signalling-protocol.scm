(define-module (crates-io gs t- gst-plugin-webrtc-signalling-protocol) #:use-module (crates-io))

(define-public crate-gst-plugin-webrtc-signalling-protocol-0.9.0 (c (n "gst-plugin-webrtc-signalling-protocol") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gik9l0zh2zc9anhrb8rccx7z63v9kzg96h9hngc066zvsz3la3m") (r "1.63")))

(define-public crate-gst-plugin-webrtc-signalling-protocol-0.10.0 (c (n "gst-plugin-webrtc-signalling-protocol") (v "0.10.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16j0l1ppc706azhswmyfy1cpgpv09abxwd6cfy21vpcp77i2glcd") (r "1.63")))

(define-public crate-gst-plugin-webrtc-signalling-protocol-0.11.0 (c (n "gst-plugin-webrtc-signalling-protocol") (v "0.11.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "069r08gyaj31ng2603dr2acif7knqvjxwd3f94v1d9qv9yjv9jl3") (r "1.70")))

(define-public crate-gst-plugin-webrtc-signalling-protocol-0.12.0 (c (n "gst-plugin-webrtc-signalling-protocol") (v "0.12.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0pm242clmjs4kffdx07wqlfz4jw0llh80psijr9i1bymsafzb7i9") (r "1.70")))

