(define-module (crates-io gs t- gst-plugin-version-helper) #:use-module (crates-io))

(define-public crate-gst-plugin-version-helper-0.1.0 (c (n "gst-plugin-version-helper") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "git2") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5") (k 0)))) (h "1jzcn29vci7nxxm2bv4vaz8711mp6czc9rhv84xn8ns4gmxjknwh")))

(define-public crate-gst-plugin-version-helper-0.2.0 (c (n "gst-plugin-version-helper") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "toml") (r "^0.5") (k 0)))) (h "1f7r4j7am1xw6g0jw97qzm24pspdvbjm0bqj7gmn2dzj14xax0fc")))

(define-public crate-gst-plugin-version-helper-0.7.0 (c (n "gst-plugin-version-helper") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "04z35v5gizv9nyns1fp047mz6dds031axvknz58wsg4c9x0n12dy")))

(define-public crate-gst-plugin-version-helper-0.7.1 (c (n "gst-plugin-version-helper") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)))) (h "0d338qq1rskr9bf8b3dai63hpwbm7j136kvzg28vwmmm2vaplsjr")))

(define-public crate-gst-plugin-version-helper-0.7.2 (c (n "gst-plugin-version-helper") (v "0.7.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std"))) (k 0)))) (h "1ga30v2jjhamwfzqyxrk2i0fpb525ri5qliv7lpcbas3c84lbzw3")))

(define-public crate-gst-plugin-version-helper-0.7.3 (c (n "gst-plugin-version-helper") (v "0.7.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std" "clock"))) (k 0)))) (h "0gqj88asgws2v4ah4dq7bdmy9bmk467scm7kkascc74krg8lssis")))

(define-public crate-gst-plugin-version-helper-0.7.4 (c (n "gst-plugin-version-helper") (v "0.7.4") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std" "clock"))) (k 0)))) (h "0pc6ip17bg2j8rnb7cism63lay81345bxjzpsscgpb37807gwzvl") (r "1.63")))

(define-public crate-gst-plugin-version-helper-0.7.5 (c (n "gst-plugin-version-helper") (v "0.7.5") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std" "clock"))) (k 0)))) (h "1jf41h475qj5dwhg15hqcwkx2nynz8ah244ahh4whpayjh4i54l7") (r "1.63")))

(define-public crate-gst-plugin-version-helper-0.8.0 (c (n "gst-plugin-version-helper") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std" "clock"))) (k 0)) (d (n "toml_edit") (r "^0.20") (k 0)))) (h "06w15y22hsh9b68mrz6y00hal4gh41pgzj2rh6nn2f52bjhkfk1k") (r "1.66")))

(define-public crate-gst-plugin-version-helper-0.8.1 (c (n "gst-plugin-version-helper") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std" "clock"))) (k 0)) (d (n "toml_edit") (r "^0.22") (f (quote ("parse"))) (k 0)))) (h "0j874fjf2wldlvxms9g906kwim1h87w1g7idxs9sq918d0lv11c1") (r "1.69")))

(define-public crate-gst-plugin-version-helper-0.8.2 (c (n "gst-plugin-version-helper") (v "0.8.2") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("std" "clock"))) (k 0)) (d (n "toml_edit") (r "^0.22.8") (f (quote ("parse"))) (k 0)))) (h "0alv0v7jfg7ryybb3qnbdwx3nqzkdl305il1xk92y9b02r7qfpjf") (r "1.69")))

