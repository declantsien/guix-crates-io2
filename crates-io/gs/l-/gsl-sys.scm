(define-module (crates-io gs l- gsl-sys) #:use-module (crates-io))

(define-public crate-GSL-sys-2.0.0 (c (n "GSL-sys") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "01l2n3j6hl15s4z6qzfn1348bw4igqiri2s2nwhfqqk7jsmcb4y0") (f (quote (("v2_7" "v2_6") ("v2_6" "v2_5") ("v2_5" "v2_4") ("v2_4" "v2_3") ("v2_3" "v2_2") ("v2_2" "v2_1") ("v2_1") ("dox" "v2_7"))))))

(define-public crate-GSL-sys-2.0.1 (c (n "GSL-sys") (v "2.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rblf63ik5phadc09ngahyfhb67vi463ipmqv6asmph8h9627l0y") (f (quote (("v2_7" "v2_6") ("v2_6" "v2_5") ("v2_5" "v2_4") ("v2_4" "v2_3") ("v2_3" "v2_2") ("v2_2" "v2_1") ("v2_1") ("dox" "v2_7"))))))

(define-public crate-GSL-sys-2.0.2 (c (n "GSL-sys") (v "2.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rdcipahi4ncm26bv19jjlpfwb7p1xc0mlpaw9fnhsxscg9lrx7v") (f (quote (("v2_7" "v2_6") ("v2_6" "v2_5") ("v2_5" "v2_4") ("v2_4" "v2_3") ("v2_3" "v2_2") ("v2_2" "v2_1") ("v2_1") ("dox" "v2_7"))))))

(define-public crate-GSL-sys-3.0.0 (c (n "GSL-sys") (v "3.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17dx066l1pbjwp9syjkzqb6fiajyb4wc814zqdfrj807rh6nfxs5") (f (quote (("v2_7" "v2_6") ("v2_6" "v2_5") ("v2_5" "v2_4") ("v2_4" "v2_3") ("v2_3" "v2_2") ("v2_2" "v2_1") ("v2_1") ("dox" "v2_7"))))))

