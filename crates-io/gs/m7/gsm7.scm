(define-module (crates-io gs m7 gsm7) #:use-module (crates-io))

(define-public crate-gsm7-0.1.0 (c (n "gsm7") (v "0.1.0") (d (list (d (n "bitstream-io") (r "^0.8.2") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 0)))) (h "0bhlddm0xqmcf4x4vzfrsym5565hwn2p9k9k6k0c7h4b5xh42ghz")))

(define-public crate-gsm7-0.1.1 (c (n "gsm7") (v "0.1.1") (d (list (d (n "bitstream-io") (r "^0.8.4") (d #t) (k 0)))) (h "0lkp490w7fw7xswr354pw7sbjynw2pq6nivf3c41q828n8q86xy0")))

(define-public crate-gsm7-0.3.0 (c (n "gsm7") (v "0.3.0") (d (list (d (n "bitstream-io") (r "^0.9.0") (d #t) (k 0)))) (h "028bwg5c9bihj3jarqkwdy2xa34hpj4i3rkb9bnkb7gsyjqx14y2")))

