(define-module (crates-io gs m- gsm-sys) #:use-module (crates-io))

(define-public crate-gsm-sys-0.0.1 (c (n "gsm-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jr756a5drm5pxpid6s834jpqzz0rnyv2ah1csh62axbjvfdkyas") (l "gsm")))

(define-public crate-gsm-sys-0.0.2 (c (n "gsm-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1m28j7gyia47ar6pfr6dgwh9snyl067x3fflsdwih21gz543hmn1") (l "gsm")))

