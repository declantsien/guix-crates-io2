(define-module (crates-io gs et gsettings-desktop-schemas-sys) #:use-module (crates-io))

(define-public crate-gsettings-desktop-schemas-sys-0.3.0 (c (n "gsettings-desktop-schemas-sys") (v "0.3.0") (d (list (d (n "glib") (r "^0.16") (d #t) (k 0) (p "glib-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "120vaqm764mgji4ywy6l825a9iwljigc0hn4nzj8d67gim5dw305") (f (quote (("dox"))))))

(define-public crate-gsettings-desktop-schemas-sys-0.3.1 (c (n "gsettings-desktop-schemas-sys") (v "0.3.1") (d (list (d (n "glib") (r "^0.18") (d #t) (k 0) (p "glib-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0n7fgknsi1pr1bvrnz3gxsq7hcbiqaa5dpmb17879gdq3bpsmb9v")))

