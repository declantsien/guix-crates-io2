(define-module (crates-io gs et gsettings-desktop-schemas) #:use-module (crates-io))

(define-public crate-gsettings-desktop-schemas-0.3.0 (c (n "gsettings-desktop-schemas") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "ffi") (r "^0.3") (d #t) (k 0) (p "gsettings-desktop-schemas-sys")) (d (n "glib") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hbcj0ycv6b6wv2vk9dwdjmvpk5jc50b0z15331a29smwq17lj69") (f (quote (("default"))))))

(define-public crate-gsettings-desktop-schemas-0.3.1 (c (n "gsettings-desktop-schemas") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "ffi") (r "^0.3.1") (d #t) (k 0) (p "gsettings-desktop-schemas-sys")) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dapb171qhnc726h44wcl35as45nwk9ysfvnpzkjspk9nj15ap47") (f (quote (("default"))))))

