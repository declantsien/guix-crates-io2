(define-module (crates-io gs as gsasl-sys) #:use-module (crates-io))

(define-public crate-gsasl-sys-0.1.0 (c (n "gsasl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "1fap00mrr575kbr9v5r4rhi89lgxayckzdclvfxmxf3vdaxi5c5w") (l "gsasl")))

(define-public crate-gsasl-sys-0.2.0 (c (n "gsasl-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0cxw6a0irm89sgkiqprgm0pwqa2chjkhpr3svb40jvz8k20lc8ws") (l "gsasl")))

(define-public crate-gsasl-sys-0.2.1 (c (n "gsasl-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "0gdbila0a93nq299c29bci0nfgz29pg1b3hq9kjzgylkilz8cgrd") (l "gsasl")))

(define-public crate-gsasl-sys-0.2.2 (c (n "gsasl-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "1ir8x97yb0h8jnxch5msx7h4l68nffbmxg9fkk4bqb7b74im1l4h") (l "gsasl")))

(define-public crate-gsasl-sys-0.2.3 (c (n "gsasl-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.55") (o #t) (d #t) (k 1)))) (h "0ddqdcxdwyg5xdfn8jkrwvq6kpnf71sg27n8vl66jmi3gjr954fq") (f (quote (("default" "build_bindgen") ("build_bindgen" "bindgen")))) (l "gsasl")))

(define-public crate-gsasl-sys-0.2.4 (c (n "gsasl-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.55") (o #t) (d #t) (k 1)))) (h "1gaxpnv1s2kms3rl1vx9w8jawxbsj2lnnsjc3dcn3rh7c0h7ydyd") (f (quote (("default" "build_bindgen") ("build_bindgen" "bindgen")))) (l "gsasl")))

