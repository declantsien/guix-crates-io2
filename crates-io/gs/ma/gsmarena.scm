(define-module (crates-io gs ma gsmarena) #:use-module (crates-io))

(define-public crate-gsmarena-0.1.0 (c (n "gsmarena") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cal49l4isxd4ghky10qivi292k1ch2sxxhx1nlk6ngphfn0gm5m")))

(define-public crate-gsmarena-0.1.1 (c (n "gsmarena") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nxhji00aybcpfkcb3v0xpmhk8zr17wqf72y01vyr57a2pk8hwbk")))

