(define-module (crates-io gs br gsbrs) #:use-module (crates-io))

(define-public crate-gsbrs-0.0.1 (c (n "gsbrs") (v "0.0.1") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "0.2.*") (d #t) (k 0)))) (h "04zn7a4xvbdmfirwgc55p4kq9li4h0abs1r36cyzkw44flbmy88j")))

(define-public crate-gsbrs-0.1.0 (c (n "gsbrs") (v "0.1.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "0.2.*") (d #t) (k 0)))) (h "1lrqvixbydrprrsxgwkcwk14kqm12kw05clqqbqdgxmqb4ql2fi1")))

(define-public crate-gsbrs-0.2.0 (c (n "gsbrs") (v "0.2.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "0.2.*") (d #t) (k 0)))) (h "1h9yf3dlwfkn0s2pny028fw26nbzn2yqmr0h52q9az0gy7bf3i1y")))

(define-public crate-gsbrs-0.3.0 (c (n "gsbrs") (v "0.3.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "0.2.*") (d #t) (k 0)))) (h "1fds5iq2nwz8qqr13pqg79km6dhsah3zwdyd5570cxgc8sraflx2")))

(define-public crate-gsbrs-0.4.0 (c (n "gsbrs") (v "0.4.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "0.2.*") (d #t) (k 0)))) (h "0i16ank3cvic8kayi5dr7ih4dhbir7xpfz8h8brsdjagayzbkxch")))

(define-public crate-gsbrs-0.5.0 (c (n "gsbrs") (v "0.5.0") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "0.2.*") (d #t) (k 0)))) (h "0syzr9rj3fs3k0mi87lhp8rsjav5909mp15niq1vdq1j27k082xb")))

(define-public crate-gsbrs-0.6.0 (c (n "gsbrs") (v "0.6.0") (d (list (d (n "hyper") (r "0.7.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "0.5.*") (d #t) (k 0)))) (h "1v51smwdg96q5v1xy94kfd453in3kbi7dxrdiwnd28vgcl7q3xzv")))

(define-public crate-gsbrs-0.7.0 (c (n "gsbrs") (v "0.7.0") (d (list (d (n "hyper") (r "0.7.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "0.5.*") (d #t) (k 0)))) (h "0n5kmbvfyhpb4mfqx1ri8bqjrzr91bhimzcrzxwa185n6lq5v13a")))

(define-public crate-gsbrs-0.7.1 (c (n "gsbrs") (v "0.7.1") (d (list (d (n "hyper") (r "~0.10.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "0.5.*") (d #t) (k 0)))) (h "0wjsjk68k9dvplmi4nbfpz0k2j2abqc6afzqzbb8r4zgfgp83i37")))

