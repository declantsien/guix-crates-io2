(define-module (crates-io gs sa gssapi-sys) #:use-module (crates-io))

(define-public crate-gssapi-sys-0.1.0 (c (n "gssapi-sys") (v "0.1.0") (d (list (d (n "krb5-sys") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1jglhhzh06was5gcr4wj906gpkf9vm7i6vrvmdhqmylvxznbps5g") (f (quote (("om_string") ("krb5" "krb5-sys") ("default" "krb5"))))))

(define-public crate-gssapi-sys-0.2.0 (c (n "gssapi-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)))) (h "0i6r7r9vwbv2vidfgnkpcxzcdczkzj72pizrx49m4x0c96wzjmby") (l "gssapi")))

(define-public crate-gssapi-sys-0.2.1 (c (n "gssapi-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)))) (h "0nqi8zhyrdjbb6z42fqah8mzhx5dys8w0p5n0ykgmhh314jmiwsf") (l "gssapi")))

