(define-module (crates-io gs sa gssapi) #:use-module (crates-io))

(define-public crate-gssapi-0.0.1 (c (n "gssapi") (v "0.0.1") (d (list (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "gssapi-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.4") (d #t) (k 0)))) (h "0dwr5aaacc96p02q3ml4qjc96j39j0mi3pw2hi63wn947z8jfi69")))

