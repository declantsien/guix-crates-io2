(define-module (crates-io gs ca gscaler) #:use-module (crates-io))

(define-public crate-gscaler-1.2.0 (c (n "gscaler") (v "1.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "02qss3rhd4fly14jjsmbjakvb79kmv8n8fpyfzs5q1q24ly8gkal")))

(define-public crate-gscaler-1.3.0 (c (n "gscaler") (v "1.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zk7s8lrpd2s76ppijhjnn218k8vzsc16c4ps5k4wafdafxmm5j7")))

