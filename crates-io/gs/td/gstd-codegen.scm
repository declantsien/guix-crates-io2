(define-module (crates-io gs td gstd-codegen) #:use-module (crates-io))

(define-public crate-gstd-codegen-0.0.0 (c (n "gstd-codegen") (v "0.0.0") (h "0m46iw9xb84g821hqj35l1ka4d8j2ksacrfy67p5dig2p42gzp6g")))

(define-public crate-gstd-codegen-1.0.2-354d660.3 (c (n "gstd-codegen") (v "1.0.2-354d660.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1q3rn6m6g7r9q5b9m3gpxg5rjspgf9g1cm7ajnifd2mcsxzxyssp")))

(define-public crate-gstd-codegen-1.0.2-354d660.4 (c (n "gstd-codegen") (v "1.0.2-354d660.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1j6zxi6pd3v2iqy6sfypj63amdjvcg0i8jp7h15zhmby6msjzqz1")))

(define-public crate-gstd-codegen-1.0.2-354d660.6 (c (n "gstd-codegen") (v "1.0.2-354d660.6") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1ls9s1mpcxl9hi2yfq23v5ydgz44xb2a082y4xzlwj1bqf8lbxka")))

(define-public crate-gstd-codegen-1.0.2-354d660.5 (c (n "gstd-codegen") (v "1.0.2-354d660.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0l3vh4wrrvw91c2bsgi6w32585cf3bv49xmdap1yl0gan1n2hqjj")))

(define-public crate-gstd-codegen-1.0.2-354d660.8 (c (n "gstd-codegen") (v "1.0.2-354d660.8") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "19pfqvsmnicf53s9759cfglqxzgd4qm56ghxv3m2hbsdrmamz8ha")))

(define-public crate-gstd-codegen-1.0.2-354d660.9 (c (n "gstd-codegen") (v "1.0.2-354d660.9") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "13d9qs1hccs3znnsyaj4d81glfqpklihx4c7zd70z8j8l4ha3h14")))

(define-public crate-gstd-codegen-1.0.2-354d660.10 (c (n "gstd-codegen") (v "1.0.2-354d660.10") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1h7nazibqvl55ihi1xbggjrgm4lailq85vwxm41zncnph6m6rc75")))

(define-public crate-gstd-codegen-1.0.2 (c (n "gstd-codegen") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "03i63fa7hma7ff6wpl2mgmpck9alrj8fj684m0m5hm9rcn1z4bi6")))

(define-public crate-gstd-codegen-1.0.2-gtest-dev (c (n "gstd-codegen") (v "1.0.2-gtest-dev") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.40") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0akzbzb5w9j3c42hrm44mmwfss67qp4f659hb04bvgbl9w7axc81")))

(define-public crate-gstd-codegen-1.0.2-dev.0 (c (n "gstd-codegen") (v "1.0.2-dev.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0m9ns3p8arqyqjmwy3jq5i7rv87kq0yqdch8j0b13xinbxffbrcb")))

(define-public crate-gstd-codegen-1.0.3 (c (n "gstd-codegen") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1iqw8xpfx4yihwizvk9dn325y7jfybvs0zrr2mynn0aiq5mb3811")))

(define-public crate-gstd-codegen-1.0.4-rc.0 (c (n "gstd-codegen") (v "1.0.4-rc.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0s1b2k68ahc40jzafy0k3bb44y4rb5lggnahzsiw67kcgbji9ky0")))

(define-public crate-gstd-codegen-1.0.4 (c (n "gstd-codegen") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1lx8dp861r1yx1niar85zrfggvi7h3s95rfr0gv2v2ws39zqzxcc")))

(define-public crate-gstd-codegen-1.0.5 (c (n "gstd-codegen") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "107gnv9i0w0nlgvpn953vwnk853ayrb5aa79sga3gkp432ia1q2x")))

(define-public crate-gstd-codegen-1.1.0 (c (n "gstd-codegen") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1c3hs6nc88216ijfbswdcnrcm71zl58d3n3lwx8sddm16nyy0m95")))

(define-public crate-gstd-codegen-1.1.1-rc.0 (c (n "gstd-codegen") (v "1.1.1-rc.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0br1hxx124w811f7knc6gjaq2mh55hc2shnnvaml7921c1ircn2l")))

(define-public crate-gstd-codegen-1.1.1 (c (n "gstd-codegen") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0fr76zqszblkpcm8kkyzq51420adi72g8pnm9vpq6jigz1y9a2d7")))

(define-public crate-gstd-codegen-1.2.0-pre.2 (c (n "gstd-codegen") (v "1.2.0-pre.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1l9fxq5fmxvbx1h7qnxjxianb4dlnniv09h3xl1aigzfd1gdvidh")))

(define-public crate-gstd-codegen-1.3.1-pre.2 (c (n "gstd-codegen") (v "1.3.1-pre.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0l6sf8l6z7lfdmjrf5kfh2wpq8rfyfazqzjjs3pkz7r1sw8ndzym")))

(define-public crate-gstd-codegen-1.3.1-pre.3 (c (n "gstd-codegen") (v "1.3.1-pre.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "17v9ja8xj680x8b9rw3ndsay91bqka8yxjprbdjwiqa457c9cqz9")))

(define-public crate-gstd-codegen-1.2.0-pre.3 (c (n "gstd-codegen") (v "1.2.0-pre.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0cnl80j7xqlgqvdc2iad4281n6c9qnda31y73k5ikf699623ilyd")))

(define-public crate-gstd-codegen-1.2.0 (c (n "gstd-codegen") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1g9962m40k573zdl6k4d38mlyv9sd8rbwhygwwbwkf06hb4j9b63")))

(define-public crate-gstd-codegen-1.2.1 (c (n "gstd-codegen") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "05zqcxa5i5df5i3n5zm631wbgbj7b0aiq06pz20xq6ihdh895dsi")))

(define-public crate-gstd-codegen-1.3.0-pre.1 (c (n "gstd-codegen") (v "1.3.0-pre.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "09h2k1rzvkxklzfga3wnpli6ryj4hb1yxln6yn9r1lkwhpp5l33p")))

(define-public crate-gstd-codegen-1.3.0 (c (n "gstd-codegen") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "09l4fz0lvnbd70jnsmfa18rm98xjc0k4ra87hxbfyzksgyw3cih5")))

(define-public crate-gstd-codegen-1.3.1 (c (n "gstd-codegen") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "13dvh4gyx1a0gnbwdkmh2darmf5a0pawzl3sfl5rl31djab47rgx")))

(define-public crate-gstd-codegen-1.4.0 (c (n "gstd-codegen") (v "1.4.0") (d (list (d (n "gear-ss58") (r "^1.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "03cnibv5japfx4zcyhf4n7mj1bi5zrc7zmakch25nw9brk1hjfxj")))

(define-public crate-gstd-codegen-1.4.1 (c (n "gstd-codegen") (v "1.4.1") (d (list (d (n "gear-ss58") (r "^1.4.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1kpk4c4pc8bgs7s0pwh143g9d4p5c2jlqr3pnj48qfaf2mdjl4ph")))

