(define-module (crates-io gs i- gsi-cs2) #:use-module (crates-io))

(define-public crate-gsi-cs2-0.1.0 (c (n "gsi-cs2") (v "0.1.0") (d (list (d (n "poem") (r "^1.3.48") (f (quote ("server"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "1rg71mfpip9qf3pl63ybxbfnd9375yzf4n5ba1sgi5dd4kl4n2vv")))

(define-public crate-gsi-cs2-0.1.1 (c (n "gsi-cs2") (v "0.1.1") (d (list (d (n "poem") (r "^1.3.48") (f (quote ("server"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "00b6m6fi0wdsvj8ncxm4h9vl5kcdb89mi5vi7ghbq6nval0ksv3q")))

