(define-module (crates-io gs i- gsi-csgo) #:use-module (crates-io))

(define-public crate-gsi-csgo-0.1.0 (c (n "gsi-csgo") (v "0.1.0") (d (list (d (n "poem") (r "^1.3.48") (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "1dkxdvl7zyiji3y116xm6957hf13b300m57mmqwr22llj3zyva8f")))

(define-public crate-gsi-csgo-0.1.1 (c (n "gsi-csgo") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "poem") (r "^1.3.48") (d #t) (k 2)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "1fr72sibdgj046k96ipfgdnf30lb5jiaagisycfi6fgvpw6dn8k0")))

