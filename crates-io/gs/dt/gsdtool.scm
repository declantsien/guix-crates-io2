(define-module (crates-io gs dt gsdtool) #:use-module (crates-io))

(define-public crate-gsdtool-0.1.0 (c (n "gsdtool") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (k 0)) (d (n "dialoguer") (r "^0.10.4") (f (quote ("editor" "fuzzy-select"))) (k 0)) (d (n "gsd-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)))) (h "1wl4pl6hvjaijda40rhzqn1wwn644mil6ky582x60plm6c499a5i")))

(define-public crate-gsdtool-0.1.1 (c (n "gsdtool") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (k 0)) (d (n "dialoguer") (r "^0.10.4") (f (quote ("editor" "fuzzy-select"))) (k 0)) (d (n "gsd-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)))) (h "0iijpvkzq4sm2wz3hnj70w7zyhk9c4qisz4hb2bci6s29b6an275")))

(define-public crate-gsdtool-0.2.1 (c (n "gsdtool") (v "0.2.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (k 0)) (d (n "dialoguer") (r "^0.10.4") (f (quote ("editor" "fuzzy-select"))) (k 0)) (d (n "gsd-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)))) (h "1qzbs6y6df7gbx9axdsbswp9ihws0f39fd1blhpva2vgnfvr2b2r")))

