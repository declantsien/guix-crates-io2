(define-module (crates-io gs dk gsdk-codegen) #:use-module (crates-io))

(define-public crate-gsdk-codegen-0.1.0 (c (n "gsdk-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "19vqkk5zd49k1gw6apqajg466545cjw060wf10j1pzm43yhwfk3l")))

(define-public crate-gsdk-codegen-0.2.1-alpha.0 (c (n "gsdk-codegen") (v "0.2.1-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full"))) (k 0)))) (h "19m2n6qlbyh9gkc196hy4r61z6sfx1pycblzc2rgnlcwcxx37zyg")))

(define-public crate-gsdk-codegen-0.2.1-alpha.1 (c (n "gsdk-codegen") (v "0.2.1-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full"))) (k 0)))) (h "0yz9b5sn3dpisy0zx5frhwv19z92kv5jband2b8l6xl1i6qw615m")))

(define-public crate-gsdk-codegen-0.2.1-alpha.2 (c (n "gsdk-codegen") (v "0.2.1-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full"))) (k 0)))) (h "0n2wvwfj39nk3il9lkl7q15bwyrp3r8mphga4as9rb1sl92b5qmg")))

(define-public crate-gsdk-codegen-0.3.1-alpha.0 (c (n "gsdk-codegen") (v "0.3.1-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full"))) (k 0)))) (h "0965gwbmszdi4ci999x3gcsx8glb5xjfwndyq7r21pnx83n1p69l")))

(define-public crate-gsdk-codegen-0.3.1-alpha.1 (c (n "gsdk-codegen") (v "0.3.1-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full"))) (k 0)))) (h "03kfj90bj0nxbczr7imc92y6cr2bpjjlq26w4zrvlwi572d8d6k8")))

(define-public crate-gsdk-codegen-0.3.1-alpha.2 (c (n "gsdk-codegen") (v "0.3.1-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full"))) (k 0)))) (h "1q3vw20w4l4yfxy5apql4llf3y1sc5znmmj1yp099pmv85ya1x8m")))

(define-public crate-gsdk-codegen-0.3.2 (c (n "gsdk-codegen") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full"))) (k 0)))) (h "0pq6qx6941sm88zw0g6jaq98w9rdw908gmwry657l341hi4dq1q8")))

(define-public crate-gsdk-codegen-0.3.2-alpha.1 (c (n "gsdk-codegen") (v "0.3.2-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("default" "full"))) (k 0)))) (h "02hh8k08112m44423pjmr3q2vdv19ln5dk4618nf93jhxriyyxmd")))

(define-public crate-gsdk-codegen-0.3.2-alpha-2 (c (n "gsdk-codegen") (v "0.3.2-alpha-2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("default" "full"))) (k 0)))) (h "1jrw9lb6qy1cxiaqf6wvrg3d752h44s4dk5cb7psl18znis0z9fv")))

(define-public crate-gsdk-codegen-0.3.2-alpha.3 (c (n "gsdk-codegen") (v "0.3.2-alpha.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("default" "full"))) (k 0)))) (h "1kw72zq5fpkn9r549sf19n7ic8nbwdbhivqrpnqw7jnbgib53pim")))

(define-public crate-gsdk-codegen-0.3.2-alpha.4 (c (n "gsdk-codegen") (v "0.3.2-alpha.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("default" "full"))) (k 0)))) (h "0h21pgqv335r2q2bvzcv4cg62dr0n4j0h1sx5mz5l0qrgx46iklm")))

(define-public crate-gsdk-codegen-1.0.2-pre.0 (c (n "gsdk-codegen") (v "1.0.2-pre.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "102p0ad2q9di6sgpk5hshnyk3m02zb2ilkp7lcgyndhvxjvhhkgi")))

(define-public crate-gsdk-codegen-1.0.2-pre.1 (c (n "gsdk-codegen") (v "1.0.2-pre.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "191iz7rm1m9n2q59wvjzsp2696mmvkkmr9idyc0wh93smh6cfaav")))

(define-public crate-gsdk-codegen-1.0.2-pre.2 (c (n "gsdk-codegen") (v "1.0.2-pre.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "029rj9qxnf9lyphhb0g71bn5r4a4gd9kz76y91876dcy0yymg4v3")))

(define-public crate-gsdk-codegen-1.0.2-pre.3 (c (n "gsdk-codegen") (v "1.0.2-pre.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "0byjml34h1mksyazhzpq15rh88sfnx8y5scna79b6p01nzvryzg7")))

(define-public crate-gsdk-codegen-1.0.2-pre.4 (c (n "gsdk-codegen") (v "1.0.2-pre.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "0hkdqw50qjmdvnqwb5rqp96xmjikd56p6k9bfdmh8d697cpydbdd")))

(define-public crate-gsdk-codegen-1.0.2-pre.5 (c (n "gsdk-codegen") (v "1.0.2-pre.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "1qqbq8lp81b8wllk6mhss255wng2sngabqpc94wmalgfkga0wvkx")))

(define-public crate-gsdk-codegen-1.0.2-pre.6 (c (n "gsdk-codegen") (v "1.0.2-pre.6") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "0zgnza5iy2pdmazzzs5rkhin8saywpk67hn17kmk73w9bci2ip4k")))

(define-public crate-gsdk-codegen-1.0.2-pre.7 (c (n "gsdk-codegen") (v "1.0.2-pre.7") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "15b3vd65jmap9xkv4ssg8idgcnhgzkadr3hsskf1srj761376ai4")))

(define-public crate-gsdk-codegen-1.0.2-pre.8 (c (n "gsdk-codegen") (v "1.0.2-pre.8") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "1va9k1sbrby70glbb8hi398xsp3m3kmlpxavy0wdmi65wm2jmyvg")))

(define-public crate-gsdk-codegen-1.0.2-pre.9 (c (n "gsdk-codegen") (v "1.0.2-pre.9") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "04ajlr6v6b438m73wk983c6p519awqq4xcb07xxbdbd2p5858ajl")))

(define-public crate-gsdk-codegen-1.0.2-pre.10 (c (n "gsdk-codegen") (v "1.0.2-pre.10") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "1y187kr9r990lqrkacl4mj8hhamyrddz3mq5vsbr7v8qgrxikl1i")))

(define-public crate-gsdk-codegen-1.0.2-pre.12 (c (n "gsdk-codegen") (v "1.0.2-pre.12") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "0y71gd1qw00hglly3is9sd8f79yripdmsq2nvqbb1wmhkchyvkx8")))

(define-public crate-gsdk-codegen-1.0.2-pre.13 (c (n "gsdk-codegen") (v "1.0.2-pre.13") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("default" "full"))) (k 0)))) (h "0121mv2zd2bd3gbazklbn1zqghp5wbg5x9pi6bl3yp6s9zsn1vlz")))

(define-public crate-gsdk-codegen-1.0.2-pre.14 (c (n "gsdk-codegen") (v "1.0.2-pre.14") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "1n5f3jg88xgijxw6fkrwckn4md6f7731fbaixrhna74ky0449gsk")))

(define-public crate-gsdk-codegen-1.0.2-pre.15 (c (n "gsdk-codegen") (v "1.0.2-pre.15") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "168ivrrj9g4gzxh0w7k1hc6x8wryj8ppa7vv9zd7g41qn7m3pp72")))

(define-public crate-gsdk-codegen-1.0.2-354d660.0 (c (n "gsdk-codegen") (v "1.0.2-354d660.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "0nrc8rigc7dyldvqd12vmjbcw7c603ls8iqnfd6hkr6y4f193a1p")))

(define-public crate-gsdk-codegen-1.0.2-354d660.1 (c (n "gsdk-codegen") (v "1.0.2-354d660.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "0krgbcqjd7562wmxads4qxxbqxf040yyffvhi5rldsmixbwh30r9")))

(define-public crate-gsdk-codegen-1.0.2-354d660.3 (c (n "gsdk-codegen") (v "1.0.2-354d660.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "0s1hh8hdvnz1kj41p66qlrn1syv798rqblya11zfv8jfdlqyjawv")))

(define-public crate-gsdk-codegen-1.0.2-354d660.4 (c (n "gsdk-codegen") (v "1.0.2-354d660.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "09w94x3bqa0mk8rispnrrdqynmzbp4f4mj53rl7kqqciw43yxalc")))

(define-public crate-gsdk-codegen-1.0.2-354d660.6 (c (n "gsdk-codegen") (v "1.0.2-354d660.6") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "1qjv31z63zn6vfn6hkimmxv4fny69pg0jd8am7yv3wj8diay2raq")))

(define-public crate-gsdk-codegen-1.0.2-354d660.5 (c (n "gsdk-codegen") (v "1.0.2-354d660.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "1b9fk51l9gaz0wwcs3ly708hb9wr6k00l0mp3w3xva7577jqigfc")))

(define-public crate-gsdk-codegen-1.0.2-354d660.8 (c (n "gsdk-codegen") (v "1.0.2-354d660.8") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "15ivnv63qkrmqjc56mk45bw33wzl5sdixiq3m95gdi1cw28yv0ra")))

(define-public crate-gsdk-codegen-1.0.2-354d660.9 (c (n "gsdk-codegen") (v "1.0.2-354d660.9") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "00kr262vm43frkkacblsbbw7kqk8j95pc9ag8bj9y9qa5dwbpw9g")))

(define-public crate-gsdk-codegen-1.0.2-354d660.10 (c (n "gsdk-codegen") (v "1.0.2-354d660.10") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "00dl7b2m8bmyc8lz24nk364wm8g28v8rais2clm8vfxw6kdgl56p")))

(define-public crate-gsdk-codegen-1.0.2 (c (n "gsdk-codegen") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("default" "full"))) (k 0)))) (h "1njw9bv9m6j9vb8343kdyy4cicjb9fy4400bwyxnqmic8ahgjjyz")))

(define-public crate-gsdk-codegen-1.0.2-gtest-dev (c (n "gsdk-codegen") (v "1.0.2-gtest-dev") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.40") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "0cbxi7w4aa7ij3i286mpg8l7bmx1ly577v595i2zyyr2qg365f6w")))

(define-public crate-gsdk-codegen-1.0.2-dev.0 (c (n "gsdk-codegen") (v "1.0.2-dev.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "0464lj2r5l4pv391zqylkkpp2w6xaq22qfzwzbk1k6awi194yyiz")))

(define-public crate-gsdk-codegen-1.0.3 (c (n "gsdk-codegen") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1flv7f5bp0cscj5z8617qhn0yqwndpy80qhnjnqnc5k9i407alqv")))

(define-public crate-gsdk-codegen-1.0.4-rc.0 (c (n "gsdk-codegen") (v "1.0.4-rc.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "14n0ywssvar94zn4wkw8azz67nlhr2735rpaz32azwr9k098jm2s")))

(define-public crate-gsdk-codegen-1.0.4 (c (n "gsdk-codegen") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1liq5wy4palspa0mg5cz325vjvm41m51gfih28q5i4xmb194j4ak")))

(define-public crate-gsdk-codegen-1.0.5 (c (n "gsdk-codegen") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1jgydszy7b236lwq8mfi8jv0sznldm7q4k1x63i9msqiz0j62127")))

(define-public crate-gsdk-codegen-1.1.0 (c (n "gsdk-codegen") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "0g6a6ah4s5dlq633a2dwfn11rnlk1fmidyrr133ixfm0k7bmvqkc")))

(define-public crate-gsdk-codegen-1.1.1-rc.0 (c (n "gsdk-codegen") (v "1.1.1-rc.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1r37sdn5fk8vwwgw9l6p56k3w2968ppfh625i9biibd5kprjg7pw")))

(define-public crate-gsdk-codegen-1.1.1 (c (n "gsdk-codegen") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "10gy1xrgw6p3rsdx2lpdmsg7sn2qx41y2r3jf438iqrhj4w6d91c")))

(define-public crate-gsdk-codegen-1.2.0-pre.2 (c (n "gsdk-codegen") (v "1.2.0-pre.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "17qb3yxga0g5n0dndx9j0p4p5b1c9k63xjkqkmdbzim4300fh57v")))

(define-public crate-gsdk-codegen-1.3.1-pre.2 (c (n "gsdk-codegen") (v "1.3.1-pre.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1h4iiwdq9jxkdzgcjsbnn3z0in4g1h598iy03lrkq99mg28hs8x0")))

(define-public crate-gsdk-codegen-1.3.1-pre.3 (c (n "gsdk-codegen") (v "1.3.1-pre.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1iwql5085sx3imfwzaqypmqkb34ifwqq00r2bbirpxp98bpwz3yx")))

(define-public crate-gsdk-codegen-1.2.0-pre.3 (c (n "gsdk-codegen") (v "1.2.0-pre.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "0gxbcmr8dihzia676m5kqygk0d14g229hsjrvqbndkngk1p3nln5")))

(define-public crate-gsdk-codegen-1.2.0 (c (n "gsdk-codegen") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1c0zvprvhqbc4ydijbx1945lkzizn4m1vkx28ldbmaz4awayzii3")))

(define-public crate-gsdk-codegen-1.2.1 (c (n "gsdk-codegen") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "0hspcqaw8qhl4b3jv41h5i2xq0myf1z5p8m0jld2paj59nwmjw7n")))

(define-public crate-gsdk-codegen-1.3.0-pre.1 (c (n "gsdk-codegen") (v "1.3.0-pre.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1smnv1i6zb7mv1yl81xjwm2ccxdcbvix9m3fcqa6z9xqndr2nz83")))

(define-public crate-gsdk-codegen-1.3.0 (c (n "gsdk-codegen") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "10q09y8p2978kzrmf2qkz3wdx37qnlp05fcsmnpxig3bck815xah")))

(define-public crate-gsdk-codegen-1.3.1 (c (n "gsdk-codegen") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1mk5vdj957zsz20mk858ddnz2cdyzx3467fhl3glam71qnlsn1d1")))

(define-public crate-gsdk-codegen-1.4.0 (c (n "gsdk-codegen") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1a00a3mzvg09phddz4aiyn13828xxswkwkf9asldqn5drhwvqyyd")))

(define-public crate-gsdk-codegen-1.4.1 (c (n "gsdk-codegen") (v "1.4.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1d08h21jc5qm8lsfy9v11drrc9iaa9y1bv2fscvgdmrn4zv5p93b")))

