(define-module (crates-io gs rs gsrs) #:use-module (crates-io))

(define-public crate-gsrs-0.1.0 (c (n "gsrs") (v "0.1.0") (d (list (d (n "typed-arena") (r "2.0.*") (d #t) (k 2)))) (h "10n6c71xabrkcys8d4dzq3r3kwrxmzl5xxv8q8z42df713rwps8h") (y #t)))

(define-public crate-gsrs-0.1.1 (c (n "gsrs") (v "0.1.1") (d (list (d (n "typed-arena") (r "2.0.*") (d #t) (k 2)))) (h "0jnaqwxgdqrwh11wf54cgb9ab1c332j0xfwh659wkhf9k4pzvfy6") (y #t)))

(define-public crate-gsrs-0.1.2 (c (n "gsrs") (v "0.1.2") (d (list (d (n "typed-arena") (r "2.0.*") (d #t) (k 2)))) (h "0qj42r7r4gppar2p8y3vqfcmnyhwznjkal0bkg6n3mxcnkfyb214") (y #t)))

(define-public crate-gsrs-0.1.3 (c (n "gsrs") (v "0.1.3") (d (list (d (n "rustversion") (r "1.0.*") (d #t) (k 2)) (d (n "typed-arena") (r "2.0.*") (d #t) (k 2)))) (h "1akbd1gs5s0b0xv8dygw01jx60xdhk826zgzcsclza0ia56bb1rk")))

(define-public crate-gsrs-0.1.4 (c (n "gsrs") (v "0.1.4") (d (list (d (n "rustversion") (r "1.0.*") (d #t) (k 2)) (d (n "typed-arena") (r "2.0.*") (d #t) (k 2)))) (h "0qs35av7zc9pjfjw0x39si27gb2a873893spylck2c675r2rc6xi")))

