(define-module (crates-io gs ta gstats) #:use-module (crates-io))

(define-public crate-gstats-0.1.2 (c (n "gstats") (v "0.1.2") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)))) (h "17zxjy0ir411iggqg0gp3c53dja2vzqvchq7x8l78xw2g0xji4lj")))

(define-public crate-gstats-0.1.3 (c (n "gstats") (v "0.1.3") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)))) (h "1a6slzx9l1jrwngl1983f1jaaqcsl4dj4qrcqyidm0dv9ri4710q")))

(define-public crate-gstats-0.1.4 (c (n "gstats") (v "0.1.4") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)))) (h "0h19k9ap08pw90rf0g9aik8j8p6scp29ih916zj08klm5m3vh64h")))

