(define-module (crates-io tt s- tts-tui) #:use-module (crates-io))

(define-public crate-tts-tui-0.0.0 (c (n "tts-tui") (v "0.0.0") (h "1a1a1hd6mrdlpvq201x1aqkgbr04xxy8c9dvwfr22x9ix14bbm7y")))

(define-public crate-tts-tui-0.0.1 (c (n "tts-tui") (v "0.0.1") (h "0j4qp88xvmlvliqca39c5pvigf4bhyiaiw1rdmx33hb8364qfi77")))

(define-public crate-tts-tui-0.1.0 (c (n "tts-tui") (v "0.1.0") (d (list (d (n "arboard") (r "^3.2.0") (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "tts") (r "^0.25.5") (k 0)) (d (n "tui") (r "^0.20.1") (d #t) (k 0) (p "ratatui")))) (h "1w3pgni9dals109m0z3rcdfszz8d4qaimb1cmw4lqzic3n5y1cif") (f (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-tui-0.1.1 (c (n "tts-tui") (v "0.1.1") (d (list (d (n "arboard") (r "^3.2.0") (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "tts") (r "^0.25.5") (k 0)) (d (n "tui") (r "^0.20.1") (d #t) (k 0) (p "ratatui")))) (h "0q9dwr5v2ihb8ivx8yp7qbglpgbnjaycry1yqqnxdyqklkm4b6fv") (f (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-tui-0.1.2 (c (n "tts-tui") (v "0.1.2") (d (list (d (n "arboard") (r "^3.2.0") (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "tts") (r "^0.25.5") (k 0)) (d (n "tui") (r "^0.21.0") (d #t) (k 0) (p "ratatui")))) (h "1v822by85963iskvapnyf30i7rr32f4w9nm21nhzgrvv5ak5lm5h") (f (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-tui-0.1.3 (c (n "tts-tui") (v "0.1.3") (d (list (d (n "arboard") (r "^3.2.0") (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "tts") (r "^0.25.5") (k 0)) (d (n "tui") (r "^0.21.0") (d #t) (k 0) (p "ratatui")))) (h "1q3w64kw169hi6vzj28nsgn7nwhwbj7whf7ya9nx8dpyadp04d64") (f (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-tui-0.1.4 (c (n "tts-tui") (v "0.1.4") (d (list (d (n "arboard") (r "^3.2.0") (k 0)) (d (n "crossterm") (r "^0.26.1") (k 0)) (d (n "tts") (r "^0.25.5") (k 0)) (d (n "tui") (r "^0.21.0") (d #t) (k 0) (p "ratatui")))) (h "0ay8dnp667dhf0ls6h7zhffd6ywnb6zmi5c2cr7mgjha7a5bq2zd") (f (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

