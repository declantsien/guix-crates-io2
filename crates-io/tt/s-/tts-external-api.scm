(define-module (crates-io tt s- tts-external-api) #:use-module (crates-io))

(define-public crate-tts-external-api-0.1.0 (c (n "tts-external-api") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "141grbydqdp8f5bqdnvcci43basp782q9d8zc5200wdqxvcnk3jl")))

(define-public crate-tts-external-api-0.1.1 (c (n "tts-external-api") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "137z70aqpqf6wjccvjsbs1pqj2sys5zc35j3jgpnwz90gqsmg25x")))

(define-public crate-tts-external-api-0.1.2 (c (n "tts-external-api") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "038iwlj6xjfi9x7a21xyvmdb856dxl3vz4shj8sv2jf1yfb8sdgp")))

(define-public crate-tts-external-api-0.1.3 (c (n "tts-external-api") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1rlb49l30imq1vymh54pvr16kl1f1a0ln28rjybrqyp9cj46fi3z")))

(define-public crate-tts-external-api-0.1.4 (c (n "tts-external-api") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0kqg8455sbrx65apfypwp6d9h07fnxac0712rdzm460mnp271w84")))

