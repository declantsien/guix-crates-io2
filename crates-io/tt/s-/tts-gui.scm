(define-module (crates-io tt s- tts-gui) #:use-module (crates-io))

(define-public crate-tts-gui-0.0.0 (c (n "tts-gui") (v "0.0.0") (h "0c2jlcn51yak6n1d2f4107disc5jlnc1786kch9l8saamc5imj80")))

(define-public crate-tts-gui-0.0.1 (c (n "tts-gui") (v "0.0.1") (h "1pfir5r4y8c1g3551pbj1hqdm5fayp145vrzqhday6b5sgizvq6n")))

