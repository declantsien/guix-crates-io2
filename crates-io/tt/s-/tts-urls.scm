(define-module (crates-io tt s- tts-urls) #:use-module (crates-io))

(define-public crate-tts-urls-0.2.0 (c (n "tts-urls") (v "0.2.0") (d (list (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "0h8kaalfnzvlzszmm2klqgnkl7kwffaf04099i2s17cwkicq49cx")))

(define-public crate-tts-urls-0.2.1 (c (n "tts-urls") (v "0.2.1") (d (list (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "0ln6mag1ip82chs5r5fqr75s4y1m9l5rd91wp73arkywjcfnyn8b")))

(define-public crate-tts-urls-0.3.0 (c (n "tts-urls") (v "0.3.0") (d (list (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "0zbffxdv3q304c0dp5gsk3ps65plpil2hsyicwd8nmza2bhfkb42")))

(define-public crate-tts-urls-1.0.0 (c (n "tts-urls") (v "1.0.0") (d (list (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "1zlpvg8x6s962zx19shp7qmsfzl5m0lljkwdb5m0hhlmd8am68g7")))

