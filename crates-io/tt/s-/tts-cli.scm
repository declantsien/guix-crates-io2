(define-module (crates-io tt s- tts-cli) #:use-module (crates-io))

(define-public crate-tts-cli-0.1.0 (c (n "tts-cli") (v "0.1.0") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "tts") (r "^0.25.5") (k 0)))) (h "0ncky4qi0hp7wvbxwsl0grdcxpvyb7hq95vmcjmf8xs2cgf3qmzs")))

(define-public crate-tts-cli-0.1.1 (c (n "tts-cli") (v "0.1.1") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "tts") (r "^0.25.5") (k 0)))) (h "05xd4fi6qd5mvc5yzldq6gwd7k7hz7fxhbflarvci25lgnbmihy9")))

(define-public crate-tts-cli-0.1.2 (c (n "tts-cli") (v "0.1.2") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "tts") (r "^0.25.5") (k 0)))) (h "0b58hm62w1qgndihv1vgr2bql4npfi07i8r2y6yacv04q7bpn82s") (f (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-cli-0.1.3 (c (n "tts-cli") (v "0.1.3") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "tts") (r "^0.25.5") (k 0)))) (h "0r7kikl2qrdgy4sh13cs1fjkzwa7nbzksly4by1l5ra8ba3fgx3p") (f (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-cli-0.1.4 (c (n "tts-cli") (v "0.1.4") (d (list (d (n "arboard") (r "^3.2.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "tts") (r "^0.25.5") (k 0)))) (h "04510l9ys4p023wisk11n4h0w4745xbgkch93qs0vb7ky9il38ls") (f (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

