(define-module (crates-io tt yd ttydo) #:use-module (crates-io))

(define-public crate-ttydo-0.1.1 (c (n "ttydo") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "commandstream") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0gymyiwwi4wcgfyy3ml34m5g281jx0ynx5brf3fzhdvr42xa5f70")))

