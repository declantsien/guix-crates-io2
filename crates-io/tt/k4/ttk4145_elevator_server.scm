(define-module (crates-io tt k4 ttk4145_elevator_server) #:use-module (crates-io))

(define-public crate-ttk4145_elevator_server-0.1.0 (c (n "ttk4145_elevator_server") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0zqqvnb35w8kzfs4ifqh0jirzhcpmkd79q3fx8y52xinxjfy5fnv")))

(define-public crate-ttk4145_elevator_server-0.1.1 (c (n "ttk4145_elevator_server") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "109x61zqfib5ib6valvvmbsqjpklb6gxqmb0hhfjmfganmk6nczf")))

(define-public crate-ttk4145_elevator_server-0.1.2 (c (n "ttk4145_elevator_server") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0xhkqbsqjfb4px1rmwax7rnlyn5iiw96r6257721d3d80rsqr4gi")))

(define-public crate-ttk4145_elevator_server-0.1.3 (c (n "ttk4145_elevator_server") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1shgjmpxgz191g00wh263zfl4km7sjr6qn16mj0sb1z3ws2zi4mb")))

