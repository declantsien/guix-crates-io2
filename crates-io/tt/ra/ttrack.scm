(define-module (crates-io tt ra ttrack) #:use-module (crates-io))

(define-public crate-ttrack-0.1.0 (c (n "ttrack") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qy6lrgmyydsm07kixk67pa4clrfhbdzdiinr2v5rdc53mplyslg")))

