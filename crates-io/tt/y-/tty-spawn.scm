(define-module (crates-io tt y- tty-spawn) #:use-module (crates-io))

(define-public crate-tty-spawn-0.3.0 (c (n "tty-spawn") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (f (quote ("fs" "process" "term" "ioctl" "signal" "poll"))) (k 0)) (d (n "signal-hook") (r "^0.3.14") (k 0)))) (h "1k66l1pi9p86h2z5n4s2qqnl2lrl913ilcfl4f9z4k1hnmd27f6k") (r "1.60.0")))

(define-public crate-tty-spawn-0.3.1 (c (n "tty-spawn") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (f (quote ("fs" "process" "term" "ioctl" "signal" "poll"))) (k 0)) (d (n "signal-hook") (r "^0.3.14") (k 0)))) (h "0yc3b3vy0y7m6ryar1f61k840d26pcfnbr9wwh31gr5ijb8x192p") (r "1.60.0")))

