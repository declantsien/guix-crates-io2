(define-module (crates-io tt y- tty-form) #:use-module (crates-io))

(define-public crate-tty-form-1.0.0 (c (n "tty-form") (v "1.0.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "tty-interface") (r "^3.0.0") (d #t) (k 0)) (d (n "tty-text") (r "^1.0.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1j92w0mazsr2r2w19gf56zh3k6q5hk8kzxyh1dmw8d1za3flps8v")))

(define-public crate-tty-form-2.0.0-beta.0 (c (n "tty-form") (v "2.0.0-beta.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tty-interface") (r "^4.0.0-beta.2") (d #t) (k 0)) (d (n "tty-text") (r "^2.0.0-beta.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1qwp92143fnl411z5d4nk4n97qk843xr60iapgclqfzqsx15chyx")))

(define-public crate-tty-form-2.0.0-beta.1 (c (n "tty-form") (v "2.0.0-beta.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tty-interface") (r "^4.0.0-beta.3") (d #t) (k 0)) (d (n "tty-text") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1445w9bwk5mbqmk3m3jcijf0bz4lvsk7mcr9nm3pxm5jq2b11fdv")))

(define-public crate-tty-form-2.0.0-beta.2 (c (n "tty-form") (v "2.0.0-beta.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tty-interface") (r "^4.0.0-beta.3") (d #t) (k 0)) (d (n "tty-text") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "17paxda3lhlkzqrc22wm0z9615fjzy0yf0jvnhwqajb00n0qlsra")))

(define-public crate-tty-form-2.0.0-beta.3 (c (n "tty-form") (v "2.0.0-beta.3") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tty-interface") (r "^4.0.0-beta.5") (d #t) (k 0)) (d (n "tty-text") (r "^2.0.0-beta.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1crdjv42ilrxrcb8g8pdvr0cd00ackhkwvs43mcjylj0z2p9y87y")))

(define-public crate-tty-form-2.0.0-beta.4 (c (n "tty-form") (v "2.0.0-beta.4") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tty-interface") (r "^4.0.0-beta.6") (d #t) (k 0)) (d (n "tty-text") (r "^2.0.0-beta.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0qrynayvl3fgn3wqpph9iwihbqdvik2ppskr30kas6nz2j2kfgfw")))

(define-public crate-tty-form-2.0.0-beta.5 (c (n "tty-form") (v "2.0.0-beta.5") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tty-interface") (r "^4.0.0-beta.6") (d #t) (k 0)) (d (n "tty-text") (r "^2.0.0-beta.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0wz3g14yf50fw5gqkpvd1cy4f2cnzfpg9r2iydacwlh4dsrl212i")))

(define-public crate-tty-form-2.0.0-beta.6 (c (n "tty-form") (v "2.0.0-beta.6") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tty-interface") (r "^4.0.0-beta.6") (d #t) (k 0)) (d (n "tty-text") (r "^2.0.0-beta.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "00hdi2557bzspnk00074p4rwh0a2qyigrxnz56azw62sm3pcfwgx")))

(define-public crate-tty-form-2.0.0-beta.7 (c (n "tty-form") (v "2.0.0-beta.7") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tty-interface") (r "^4.0.0-beta.6") (d #t) (k 0)) (d (n "tty-text") (r "^2.0.0-beta.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "03340kzh05hdfz6x2ajh4z9whn4ggdr0g6b8wn882hq93nlqw7m7")))

(define-public crate-tty-form-2.0.0 (c (n "tty-form") (v "2.0.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tty-interface") (r "^4.0.0") (d #t) (k 0)) (d (n "tty-text") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0qprxmnbw6z7n3zgzi9mzmk5lpqpydpgigpy29gr8mbrynl06hx9")))

