(define-module (crates-io tt y- tty-interface) #:use-module (crates-io))

(define-public crate-tty-interface-1.0.0 (c (n "tty-interface") (v "1.0.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1ad75lmpipskd7fdhaj921dq7if72xhi5cbqzbg1dw9x2ibvbhn7") (y #t)))

(define-public crate-tty-interface-1.0.1 (c (n "tty-interface") (v "1.0.1") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0n3bymipipq0b9wrgrvrhcpxrd609kx29xr9029badvjl5g4x1gz") (y #t)))

(define-public crate-tty-interface-2.0.0 (c (n "tty-interface") (v "2.0.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0875vhxdhbciy0ycbzn5bpgy7q9h41lssknmj8frp56cww9qcl8k") (y #t)))

(define-public crate-tty-interface-3.0.0 (c (n "tty-interface") (v "3.0.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0aic0p59n6byqdh2p4rhn9jgs43ky20zh1iiclc7xhhyin7yhr61")))

(define-public crate-tty-interface-4.0.0-beta.0 (c (n "tty-interface") (v "4.0.0-beta.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "vt100") (r "^0.15.1") (d #t) (k 2)))) (h "1c6zc6pz726z13l9hkxz42v8lihc22fmc92ma4yjzpfdfixrzs4p")))

(define-public crate-tty-interface-4.0.0-beta.1 (c (n "tty-interface") (v "4.0.0-beta.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "vt100") (r "^0.15.1") (d #t) (k 0)))) (h "0xd1wznjghaff3ps7z5napqnwa8p2r53hhf2vx44jd32l0wb6daa")))

(define-public crate-tty-interface-4.0.0-beta.2 (c (n "tty-interface") (v "4.0.0-beta.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "vt100") (r "^0.15.1") (d #t) (k 0)))) (h "0qfzwdfv15prja3v5yx91r3qg6c9r64ys8vw02v2siszr9fd02iz")))

(define-public crate-tty-interface-4.0.0-beta.3 (c (n "tty-interface") (v "4.0.0-beta.3") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "vt100") (r "^0.15.1") (d #t) (k 0)))) (h "1h1glycz5n19agxdcqywpa5y7hrdq0gx5aqz9h3m9f2ni59ha2fi")))

(define-public crate-tty-interface-4.0.0-beta.4 (c (n "tty-interface") (v "4.0.0-beta.4") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "vt100") (r "^0.15.1") (d #t) (k 0)))) (h "1r8gv5h6gcf80lhksdg2rdhq1bigvzxzyza91canzl9w898apwk6")))

(define-public crate-tty-interface-4.0.0-beta.5 (c (n "tty-interface") (v "4.0.0-beta.5") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "vt100") (r "^0.15.1") (d #t) (k 0)))) (h "1v41h019z5dbmf78pvq086i6pw8w44qf86b74p10k39qv1clrw6p")))

(define-public crate-tty-interface-4.0.0-beta.6 (c (n "tty-interface") (v "4.0.0-beta.6") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "vt100") (r "^0.15.1") (d #t) (k 0)))) (h "0s52cyvhwqxpjrrlk5d3cab8p4566bij59kd7rfkf315dxd0ibdr")))

(define-public crate-tty-interface-4.0.0 (c (n "tty-interface") (v "4.0.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "vt100") (r "^0.15.1") (d #t) (k 0)))) (h "0hv8nw9s75nix90mxlan8rbqyg1y1lnf64s6kbd4bsb66pvhv8il")))

