(define-module (crates-io tt y- tty-overwriter) #:use-module (crates-io))

(define-public crate-tty-overwriter-0.1.0 (c (n "tty-overwriter") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "term_size") (r "^0.3.2") (d #t) (k 2)))) (h "1w8lnqj0r3lx387ja0rgqxminrmsgslplyqk0d0jjpi9gr8rmrvd")))

