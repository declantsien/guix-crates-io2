(define-module (crates-io tt y- tty-text) #:use-module (crates-io))

(define-public crate-tty-text-1.0.0 (c (n "tty-text") (v "1.0.0") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0hick1ny1px66fqkqz95i2sws3z4d2307as73fwwgqfj7z55pvqv")))

(define-public crate-tty-text-2.0.0-beta.0 (c (n "tty-text") (v "2.0.0-beta.0") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "16l1yxjr35504hn1bab9a4p6swwwy60cq8hf1vcyzc3iv852pakk")))

(define-public crate-tty-text-2.0.0-beta.1 (c (n "tty-text") (v "2.0.0-beta.1") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1iiga687axdqqv7l69srqxwr72ik1fbx232hj59s2s2rla2c75rs")))

(define-public crate-tty-text-2.0.0-beta.2 (c (n "tty-text") (v "2.0.0-beta.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1iwqavazwd8glkfp03dmiqi92gij1rvxy7cp0966nkkhxr014913")))

(define-public crate-tty-text-2.0.0 (c (n "tty-text") (v "2.0.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "004wx5bz9v8ldag44fixsys0m14508iijif62k0n59p51x5rwbmf")))

