(define-module (crates-io tt #{21}# tt21100) #:use-module (crates-io))

(define-public crate-tt21100-0.1.0 (c (n "tt21100") (v "0.1.0") (d (list (d (n "bondrewd") (r "^0.1.14") (f (quote ("derive"))) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1ipgi3jgkiw7ml7vmibdgf0mlq0qqw290fjgil3vh3h948ia78vg") (r "1.58")))

