(define-module (crates-io tt #{21}# tt21100-async) #:use-module (crates-io))

(define-public crate-tt21100-async-0.1.0 (c (n "tt21100-async") (v "0.1.0") (d (list (d (n "bondrewd") (r "^0.1.14") (f (quote ("derive"))) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "1h6cvv9ncrwp51701l18hxjgbdn0inj9v3hdrdyg7wkgfyxak23j") (r "1.58")))

