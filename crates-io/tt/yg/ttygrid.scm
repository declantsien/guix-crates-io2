(define-module (crates-io tt yg ttygrid) #:use-module (crates-io))

(define-public crate-ttygrid-0.1.0 (c (n "ttygrid") (v "0.1.0") (d (list (d (n "rand") (r ">=0") (d #t) (k 2)) (d (n "termion") (r ">=0") (d #t) (k 0)))) (h "08zybm6ziacmxlibfg93x315nlz2jwip72694v3hmi5l89lihma6")))

(define-public crate-ttygrid-0.2.0 (c (n "ttygrid") (v "0.2.0") (d (list (d (n "anyhow") (r ">=0") (d #t) (k 0)) (d (n "rand") (r ">=0") (d #t) (k 2)) (d (n "termion") (r ">=0") (d #t) (k 0)))) (h "0jrdznz3x1l6q3v9512b5s8kjjnmnl3m60h19i36pb2dqxcxy1r1")))

(define-public crate-ttygrid-0.2.1 (c (n "ttygrid") (v "0.2.1") (d (list (d (n "anyhow") (r ">=0") (d #t) (k 0)) (d (n "rand") (r ">=0") (d #t) (k 2)) (d (n "termion") (r ">=0") (d #t) (k 0)))) (h "06q9wm6rasn8gc3pra8j45v4d2vwdm0aiwmsjp86p20zrvpmcnpd")))

(define-public crate-ttygrid-0.2.2 (c (n "ttygrid") (v "0.2.2") (d (list (d (n "anyhow") (r ">=0") (d #t) (k 0)) (d (n "rand") (r ">=0") (d #t) (k 2)) (d (n "termion") (r ">=0") (d #t) (k 0)))) (h "11f2nqs2fmkx09x76r2434xrzwsj8qwqhyrc7x21x2smkwwf2q07")))

(define-public crate-ttygrid-0.3.0 (c (n "ttygrid") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "rand") (r ">=0") (d #t) (k 2)))) (h "12nz5skm6khg0wgy267q6rjfrs51dj3s8an55illwsvdyn795m70")))

