(define-module (crates-io tt km ttkmd-if) #:use-module (crates-io))

(define-public crate-ttkmd-if-0.1.0 (c (n "ttkmd-if") (v "0.1.0") (d (list (d (n "bitfield-struct") (r "^0.4.4") (d #t) (k 0)) (d (n "luwen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1xpw40bn6ggy0smz5pmzrivv0rj3v2a8c9jnfv2gvsbv1hpbjlfr")))

