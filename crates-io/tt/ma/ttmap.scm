(define-module (crates-io tt ma ttmap) #:use-module (crates-io))

(define-public crate-ttmap-0.1.0 (c (n "ttmap") (v "0.1.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0cbqh88f4j38l62z1f3l0ikzd2bkfz83fkln8965q3fc02132kdc")))

(define-public crate-ttmap-0.1.1 (c (n "ttmap") (v "0.1.1") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (f (quote ("alloc"))) (d #t) (k 0)))) (h "06yr2pi12sxv88c44vrcj1112pd6mr7xs2p8csshgv7rddjgyrz5")))

(define-public crate-ttmap-0.1.2 (c (n "ttmap") (v "0.1.2") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "smart-ptr") (r "^0.4") (f (quote ("alloc"))) (d #t) (k 0)))) (h "10g6jfrbqiib40hw84zk5kydgr6z35wljk1854qbv9di4fx3ainh")))

(define-public crate-ttmap-0.2.0 (c (n "ttmap") (v "0.2.0") (h "1cdlfzzanjnzkj1nynyfja12zgw222bhf1vbx1j9iq6xpk7awx9p")))

(define-public crate-ttmap-0.2.1 (c (n "ttmap") (v "0.2.1") (h "18hyrqh31j83irdqsdgaj3q9slqn87l7fg6w93rpna04napjjmax")))

(define-public crate-ttmap-0.3.0 (c (n "ttmap") (v "0.3.0") (h "10j1dmlar66rdg21m55y28k3i8zj984czhrhxb94dh72v22qhd0h")))

