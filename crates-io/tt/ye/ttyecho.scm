(define-module (crates-io tt ye ttyecho) #:use-module (crates-io))

(define-public crate-ttyecho-0.1.0 (c (n "ttyecho") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.60") (d #t) (k 0)))) (h "0775vfkcjy9c0q82z57xpmvzgyr0wp4xx4smfkpv2m31z80yz9bx")))

(define-public crate-ttyecho-0.1.1 (c (n "ttyecho") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.60") (d #t) (k 0)))) (h "06xlb9c0sx1ly63rb95zyfg7ih6gk1bilffiggdsqn1qb3m27dwy")))

(define-public crate-ttyecho-0.1.2 (c (n "ttyecho") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.60") (d #t) (k 0)))) (h "1fj0f9vbiai8w5yhcv594m9c0apa44g7mvx7yg4rdbcppnadklaa")))

