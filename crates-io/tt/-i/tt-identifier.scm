(define-module (crates-io tt #{-i}# tt-identifier) #:use-module (crates-io))

(define-public crate-tt-identifier-0.1.0 (c (n "tt-identifier") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1vddf9q94phlk9nl7d5fas4hln3wgxnzqkb9fj4i7h6q9jgrraiy") (y #t)))

(define-public crate-tt-identifier-0.1.1 (c (n "tt-identifier") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0qzmmh0582axpagrq8x2j3l4cm97f0qbplmhyg9pngz448bd1n1c")))

