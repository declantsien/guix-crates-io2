(define-module (crates-io tt f2 ttf2mesh-sys) #:use-module (crates-io))

(define-public crate-ttf2mesh-sys-0.0.1 (c (n "ttf2mesh-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "16c68mlfila1h6s0fiy4lhh9sb98wib8fcihzqhbxpsgnnb5py9f")))

(define-public crate-ttf2mesh-sys-0.0.2 (c (n "ttf2mesh-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1wpywsrddqwm4kqayw1pnpd1m3ckvg0kpkj1k2pxhaciyqrrfxyc")))

(define-public crate-ttf2mesh-sys-0.1.0 (c (n "ttf2mesh-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "00y0712i7zvzyjgcgdils4q510gmb7jcr8ml5qb7z0pkv6k5cfvx")))

(define-public crate-ttf2mesh-sys-0.1.1 (c (n "ttf2mesh-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0rrpvdhndj2cz7m4865c4az3xb68k2n3ym8hgxwr4af1g32wpx1g")))

(define-public crate-ttf2mesh-sys-0.1.2 (c (n "ttf2mesh-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1mhchaqaq1hb9qzw4bf3cafq3bfl1dmq8b5hch6q0pic4w80fpmg")))

(define-public crate-ttf2mesh-sys-0.1.3 (c (n "ttf2mesh-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0krs4vfirkpk5skzrx5a18rqg7v480zdswp0xc928d37vfp2a7ij")))

