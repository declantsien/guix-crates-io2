(define-module (crates-io tt f2 ttf2mesh) #:use-module (crates-io))

(define-public crate-ttf2mesh-0.0.1 (c (n "ttf2mesh") (v "0.0.1") (d (list (d (n "ttf2mesh-sys") (r "^0.0.1") (d #t) (k 0)))) (h "175zirmh3kcb84i81n816680f7syyl2h25i433c99d5g27f8m4cc")))

(define-public crate-ttf2mesh-0.0.2 (c (n "ttf2mesh") (v "0.0.2") (d (list (d (n "ttf2mesh-sys") (r "^0.0.1") (d #t) (k 0)))) (h "055val8ywn9dllypi63393dh86mp5dx1xm9sksx5qyab0x5mw37y")))

(define-public crate-ttf2mesh-0.0.3 (c (n "ttf2mesh") (v "0.0.3") (d (list (d (n "ttf2mesh-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0nwiphzxx1c54baizad8wc91rklj1vxpq9fbmw23cayja2j962b6")))

(define-public crate-ttf2mesh-0.0.4 (c (n "ttf2mesh") (v "0.0.4") (d (list (d (n "ttf2mesh-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0zgyz1hr065jxqn9a8m745sm5nnsvqxnisfk6gvmf2hgcbawi1pg")))

(define-public crate-ttf2mesh-0.0.5 (c (n "ttf2mesh") (v "0.0.5") (d (list (d (n "ttf2mesh-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0inki38vxy9fvhnc9213z6388af9fqb8a5xjwgxsxdh80nhxnvrg")))

(define-public crate-ttf2mesh-0.0.6 (c (n "ttf2mesh") (v "0.0.6") (d (list (d (n "ttf2mesh-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1dflka44hnz63vszdmi3va2bf7hpyp7yifcbp38mq3dirmkb8w6z") (f (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.0.7 (c (n "ttf2mesh") (v "0.0.7") (d (list (d (n "ttf2mesh-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1akfz8azg5qfdczsvi6cdii2ncfvmvh2zm32fc90783dp0vcdbw7") (f (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.0.8 (c (n "ttf2mesh") (v "0.0.8") (d (list (d (n "ttf2mesh-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1270srn3m3n01fw1645dfv9kmqa70hqm1hxfz85idf78lfkzq562") (f (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.0.9 (c (n "ttf2mesh") (v "0.0.9") (d (list (d (n "ttf2mesh-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1q43380ic6r46n7vnramds8wkj0aspm759qh166crc2c3bbfm39r") (f (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.0.10 (c (n "ttf2mesh") (v "0.0.10") (d (list (d (n "ttf2mesh-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1pj1nanaadyw8axhqvygwz0clmbp8qasgc11xgh8diw3kml4766y") (f (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.0.11 (c (n "ttf2mesh") (v "0.0.11") (d (list (d (n "ttf2mesh-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1bnzgf9p4gjfhmzh96wv96fkr83mdffrim1dzjj2m6xrm21194br") (f (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.1.0 (c (n "ttf2mesh") (v "0.1.0") (d (list (d (n "ttf2mesh-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0sc9qwp51hfggi6qp8c1f6x61ng6pcxq5hjjii5zfw5fyqsj4n26") (f (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.1.2 (c (n "ttf2mesh") (v "0.1.2") (d (list (d (n "ttf2mesh-sys") (r "^0.1.2") (d #t) (k 0)))) (h "071anqz3vs09n797v73qk6zb2zsymhccv3ha1njjpc1za13bnmir") (f (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.2.0 (c (n "ttf2mesh") (v "0.2.0") (d (list (d (n "ttf2mesh-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1ywfrc31lpqvq1ikc9c8nflbg4jpcwkv187p4j9rxf82cjnq8abv") (f (quote (("unstable") ("default")))) (r "1.43")))

(define-public crate-ttf2mesh-0.2.1 (c (n "ttf2mesh") (v "0.2.1") (d (list (d (n "ttf2mesh-sys") (r "^0.1.2") (d #t) (k 0)))) (h "00dvx0x4225sd9azqi5xhlyxnczvfmp42hcssj7f259sjvnng3pa") (f (quote (("unstable") ("default")))) (r "1.43")))

