(define-module (crates-io tt f2 ttf2cp437) #:use-module (crates-io))

(define-public crate-ttf2cp437-1.0.0 (c (n "ttf2cp437") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rusttype") (r "^0.8") (d #t) (k 0)))) (h "1r7cc87f5jmqrriv60smdj8yiw5p93x8wgn5adj3yn453v62dn2l")))

