(define-module (crates-io tt yp ttype-app) #:use-module (crates-io))

(define-public crate-ttype-app-0.1.0 (c (n "ttype-app") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "1cy4k6bknknm1a8vashwx0hh1j0q4qhkshx8sx147zvq47sy3f53")))

(define-public crate-ttype-app-0.2.0 (c (n "ttype-app") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)))) (h "0xbrimdx7m9b4h7nvyp2fq7viwm3h1cp7gik254xfp4791ah110q")))

