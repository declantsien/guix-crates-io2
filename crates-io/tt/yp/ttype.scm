(define-module (crates-io tt yp ttype) #:use-module (crates-io))

(define-public crate-ttype-0.1.0 (c (n "ttype") (v "0.1.0") (h "1vgy4h9zx45kdsif98zsk27m1k5gyzyfhd1fag773x564rfzzy5h")))

(define-public crate-ttype-0.1.1 (c (n "ttype") (v "0.1.1") (h "1q76knvczhdddv5cnv388wg7krb6m16cg4a1fifbvin1gnd7hbaw")))

