(define-module (crates-io tt yp ttypass) #:use-module (crates-io))

(define-public crate-ttypass-0.3.0 (c (n "ttypass") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "termios") (r "^0.3.2") (d #t) (k 0)))) (h "1ch7m1na49qik6b2zvwscjlka442m5b9abrb3dmxb9rbhc3bcbwk")))

(define-public crate-ttypass-0.3.1 (c (n "ttypass") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "termios") (r "^0.3.2") (d #t) (k 0)))) (h "1d266cnf73pcf6plck5kb9yy5ydpgyl9jpighwrjhgycj7l9bq7c")))

