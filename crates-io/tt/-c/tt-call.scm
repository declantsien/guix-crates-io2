(define-module (crates-io tt -c tt-call) #:use-module (crates-io))

(define-public crate-tt-call-1.0.0 (c (n "tt-call") (v "1.0.0") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1rd18kxngr94y8yd0al5ps9jrssbmrqnih8rpxzm2wc9wsvqf3rp")))

(define-public crate-tt-call-1.0.1 (c (n "tt-call") (v "1.0.1") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0r79rkgy3l9k5v51h1g2vwyk9xhf2wzfzc6d7bxdzr3cvh3r380x")))

(define-public crate-tt-call-1.0.2 (c (n "tt-call") (v "1.0.2") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1qynn11379n8izg9kzyfh9ad5h4fsm34k487l57iaxrrndsm2hid")))

(define-public crate-tt-call-1.0.3 (c (n "tt-call") (v "1.0.3") (d (list (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1d8nhcwnn9gxfkpa7c84bvl609warf2vvqcqa2yr57smix5xvh0q")))

(define-public crate-tt-call-1.0.4 (c (n "tt-call") (v "1.0.4") (d (list (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "01jrm4nqxwabrcrw5697310qdlhc5glgg0645lixkv6m4r5060lm")))

(define-public crate-tt-call-1.0.5 (c (n "tt-call") (v "1.0.5") (d (list (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0mkmvqds3nian5anlsc8170vhq7mrdz8k4fwh356a0yym3nkizq6")))

(define-public crate-tt-call-1.0.6 (c (n "tt-call") (v "1.0.6") (d (list (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19jb7b6drkn0ipwylaizj51gk5r66p4kvrk0xgj4w3045brm6x1n")))

(define-public crate-tt-call-1.0.7 (c (n "tt-call") (v "1.0.7") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0vl324csl0b1hdvi58alkf6a1r7pp4v7znsv5wvyhjhpzcbzhyd8")))

(define-public crate-tt-call-1.0.8 (c (n "tt-call") (v "1.0.8") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0md03vvmq18k3hsx8vcw237cg9cy8v17czn50gfnj319qjzdqrjy")))

(define-public crate-tt-call-1.0.9 (c (n "tt-call") (v "1.0.9") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1pw1k05bc73gdqryvl7w928kj1jg5fxw8n2sxr76408rhpyrbwgl") (r "1.31")))

