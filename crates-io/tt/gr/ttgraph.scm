(define-module (crates-io tt gr ttgraph) #:use-module (crates-io))

(define-public crate-ttgraph-0.1.0 (c (n "ttgraph") (v "0.1.0") (d (list (d (n "ttgraph_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)) (d (n "visible") (r "^0.0.1") (d #t) (k 0)))) (h "19gphcbnfgmaayvbj7f5bqj7qva2b32nqkhs6ml89ss20a3wv4ah")))

(define-public crate-ttgraph-0.2.0 (c (n "ttgraph") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "ttgraph_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "serde"))) (d #t) (k 0)) (d (n "visible") (r "^0.0.1") (d #t) (k 0)))) (h "0hykdr7glgbd3lr866871ni8xq0yxl5k6k8hg4p6g6v3z11v8i13") (f (quote (("default" "debug") ("debug"))))))

(define-public crate-ttgraph-0.2.1 (c (n "ttgraph") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "ttgraph_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "serde"))) (d #t) (k 0)) (d (n "visible") (r "^0.0.1") (d #t) (k 0)))) (h "1wl6x75lqzairjzhli8y69da3kq749kj8iv7zrqq0j0i2kyyfrpm") (f (quote (("default" "debug") ("debug"))))))

(define-public crate-ttgraph-0.2.2 (c (n "ttgraph") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "ttgraph_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "serde"))) (d #t) (k 0)) (d (n "visible") (r "^0.0.1") (d #t) (k 0)))) (h "11rrx1jfm67bndm7wncpb9yyfsx3myrwnfw9ij0y6ak1yqpnzpby") (f (quote (("default" "debug") ("debug"))))))

(define-public crate-ttgraph-0.2.3 (c (n "ttgraph") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "ttgraph_macros") (r "^0.2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "fast-rng" "serde"))) (d #t) (k 0)) (d (n "visible") (r "^0.0.1") (d #t) (k 0)))) (h "13ppig05w9r2z4847js7jk3cb1gbvyqpyhqdcz7qwcz0wyvj9s7c") (f (quote (("default" "debug") ("debug"))))))

