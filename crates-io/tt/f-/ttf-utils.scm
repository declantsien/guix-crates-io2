(define-module (crates-io tt f- ttf-utils) #:use-module (crates-io))

(define-public crate-ttf-utils-0.1.0 (c (n "ttf-utils") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.10") (d #t) (k 0)))) (h "0gx8fbw6zav364w05ibfpjzm26nvbq88c9bzdgy7sdgcma1331qp")))

(define-public crate-ttf-utils-0.1.1 (c (n "ttf-utils") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.10") (d #t) (k 0)))) (h "057lc9f06cij60wcskbci43bd8ppdi04v2xzd1fqf2sgnsl2cfl9") (y #t)))

(define-public crate-ttf-utils-0.1.2 (c (n "ttf-utils") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.10") (d #t) (k 0)))) (h "13l38g7dfixfjqmrc0payrmcak8hg15883q3qflla1mszm47ccy7")))

(define-public crate-ttf-utils-0.1.3 (c (n "ttf-utils") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.11") (d #t) (k 0)))) (h "140d0w5xvd7wdvxi0yi3qrp2az90y29pgk24y0f894sb94ajiw8z")))

