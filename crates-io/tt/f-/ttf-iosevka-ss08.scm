(define-module (crates-io tt f- ttf-iosevka-ss08) #:use-module (crates-io))

(define-public crate-ttf-iosevka-ss08-0.1.0 (c (n "ttf-iosevka-ss08") (v "0.1.0") (h "0vqklqbnznad4sagb2ddgrl3r5rpwmi4zh0b4ip6kgsp2gbdcqsx") (f (quote (("thinoblique") ("thinitalic") ("thin") ("regular") ("oblique") ("mediumoblique") ("mediumitalic") ("medium") ("lightoblique") ("lightitalic") ("light") ("italic") ("heavyoblique") ("heavyitalic") ("heavy") ("extralightoblique") ("extralightitalic") ("extralight") ("boldoblique") ("bolditalic") ("bold"))))))

