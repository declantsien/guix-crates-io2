(define-module (crates-io tt f- ttf-noto-sans) #:use-module (crates-io))

(define-public crate-ttf-noto-sans-0.1.0 (c (n "ttf-noto-sans") (v "0.1.0") (d (list (d (n "conrod") (r "^0.51") (f (quote ("piston"))) (d #t) (k 2)) (d (n "piston_window") (r "^0.63") (d #t) (k 2)))) (h "1dbpyhy8l7zbs84hvkw2dnq11bmppcahp4n1rny2q3m694swlyff") (f (quote (("regular") ("italic") ("default" "regular") ("bold_italic") ("bold"))))))

(define-public crate-ttf-noto-sans-0.1.1 (c (n "ttf-noto-sans") (v "0.1.1") (d (list (d (n "conrod") (r "^0.51") (f (quote ("piston"))) (d #t) (k 2)) (d (n "piston_window") (r "^0.63") (d #t) (k 2)))) (h "1dnnzb3d2f72rqz1as5s9f612da5nwpsgz15lz93qxspzn8rp3dg") (f (quote (("regular") ("italic") ("default" "regular") ("bold_italic") ("bold"))))))

(define-public crate-ttf-noto-sans-0.1.2 (c (n "ttf-noto-sans") (v "0.1.2") (d (list (d (n "conrod") (r "^0.51") (f (quote ("piston"))) (d #t) (k 2)) (d (n "piston_window") (r "^0.63") (d #t) (k 2)))) (h "1jawribdqhia2cwgd5xwdv1flccssz67znxx781kmpaiwkcps32y") (f (quote (("regular") ("italic") ("default" "regular") ("bold_italic") ("bold"))))))

