(define-module (crates-io tt f- ttf-inter) #:use-module (crates-io))

(define-public crate-ttf-inter-0.1.0 (c (n "ttf-inter") (v "0.1.0") (h "0jh6kkk0a6rln2az5gsbr63270i52jicbbbghsx1wk68w834wlz9") (f (quote (("v") ("thinitalic") ("thin") ("semibolditalic") ("semibold") ("regular") ("mediumitalic") ("medium") ("lightitalic") ("light") ("italic") ("extralightitalic") ("extralight") ("extrabolditalic") ("extrabold") ("bolditalic") ("bold") ("blackitalic") ("black"))))))

