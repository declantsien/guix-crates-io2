(define-module (crates-io tt f- ttf-firacode) #:use-module (crates-io))

(define-public crate-ttf-firacode-0.1.0 (c (n "ttf-firacode") (v "0.1.0") (h "1x37bxrxjf4i68f3dvyv93h77hraalir3q9ic3kf8drnyj60sday") (f (quote (("retina") ("regular") ("medium") ("light") ("default" "regular") ("bold"))))))

