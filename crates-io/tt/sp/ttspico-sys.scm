(define-module (crates-io tt sp ttspico-sys) #:use-module (crates-io))

(define-public crate-ttspico-sys-0.1.0 (c (n "ttspico-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x1f69hick1bj4h5grx5bbr3ic60cmhj3j91dcfw239yfhkvjdl2") (l "svoxpico")))

(define-public crate-ttspico-sys-0.1.1 (c (n "ttspico-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "178r9sxia9shcpfmdb8xq34kl37pg67vknwdc5jd66d9cr86cpzn") (l "svoxpico")))

(define-public crate-ttspico-sys-0.1.3 (c (n "ttspico-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07qkscj7k04a9z06qjcav6vz4fql6g2m3mgiykpx03k8gpzxjs5k") (l "svoxpico")))

(define-public crate-ttspico-sys-0.1.4 (c (n "ttspico-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q6zyqr815w77ikmi0qaaw16hi5rhhcn9vivah51q2vb94s6zcz1") (y #t) (l "svoxpico")))

(define-public crate-ttspico-sys-0.1.5 (c (n "ttspico-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zqzvhxsfah6l6s5sfd5qqwxd45alzp7c3k6id94nfwnmbsl3kax") (l "svoxpico")))

