(define-module (crates-io tt sp ttspico) #:use-module (crates-io))

(define-public crate-ttspico-0.1.0 (c (n "ttspico") (v "0.1.0") (d (list (d (n "cpal") (r "^0.10") (d #t) (k 2)) (d (n "ttspico-sys") (r "^0.1") (d #t) (k 0)))) (h "1vfc7y7kn394pijw1qhsfb5cg57rr9zh1ri380mrwf637zwb44w4")))

(define-public crate-ttspico-0.1.1 (c (n "ttspico") (v "0.1.1") (d (list (d (n "cpal") (r "^0.10") (d #t) (k 2)) (d (n "ttspico-sys") (r "^0.1") (d #t) (k 0)))) (h "0n4p6hl947fr932yk5rq96279gs0b93d46z0zdsv1b28cx9r4581")))

(define-public crate-ttspico-0.1.2 (c (n "ttspico") (v "0.1.2") (d (list (d (n "cpal") (r "^0.10") (d #t) (k 2)) (d (n "ttspico-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("audiosessiontypes" "audioclient" "coml2api" "combaseapi" "debug" "devpkey" "handleapi" "ksmedia" "mmdeviceapi" "objbase" "std" "synchapi" "winbase" "winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 2)))) (h "17q8d9kkdv5s0fdi7yha1f27qp9iyi1ja2kafb2f101z4yw1iszf")))

(define-public crate-ttspico-0.1.3 (c (n "ttspico") (v "0.1.3") (d (list (d (n "cpal") (r "^0.10") (d #t) (k 2)) (d (n "ttspico-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("audiosessiontypes" "audioclient" "coml2api" "combaseapi" "debug" "devpkey" "handleapi" "ksmedia" "mmdeviceapi" "objbase" "std" "synchapi" "winbase" "winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 2)))) (h "186q1il8xbjgh89dqw9na4s31gphb9id0ah8x0znkjgqf0sqd8in")))

(define-public crate-ttspico-0.1.4 (c (n "ttspico") (v "0.1.4") (d (list (d (n "cpal") (r "^0.11") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "ttspico-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("audiosessiontypes" "audioclient" "coml2api" "combaseapi" "debug" "devpkey" "handleapi" "ksmedia" "mmdeviceapi" "objbase" "std" "synchapi" "winbase" "winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 2)))) (h "0diglnlg29ydbqkk3s2lkzz5wp648c8q0yx1cp2by1abp4k2551j") (y #t)))

(define-public crate-ttspico-0.1.5 (c (n "ttspico") (v "0.1.5") (d (list (d (n "cpal") (r "^0.11") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "ttspico-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("audiosessiontypes" "audioclient" "coml2api" "combaseapi" "debug" "devpkey" "handleapi" "ksmedia" "mmdeviceapi" "objbase" "std" "synchapi" "winbase" "winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 2)))) (h "0kdha9kn2h77x2g0890nbz45dgjx0nabhc6apakk9djs1halfbqn")))

