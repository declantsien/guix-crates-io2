(define-module (crates-io tt co ttcore_def) #:use-module (crates-io))

(define-public crate-ttcore_def-0.2.10 (c (n "ttcore_def") (v "0.2.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1207ha9kwwwrjwh7myvh1xiws20807jyp7l31xfb96g5k1p7b94c")))

(define-public crate-ttcore_def-0.2.11 (c (n "ttcore_def") (v "0.2.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ki1r3k6mncr9azwzaq835l6ahp40r526fsijgs61yva53cjli5m")))

(define-public crate-ttcore_def-0.2.12 (c (n "ttcore_def") (v "0.2.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10slr6wc24f8ci0dyj9ircdqby6yq601c5qkmrfbxm3rnrvxhp8h")))

(define-public crate-ttcore_def-0.2.14 (c (n "ttcore_def") (v "0.2.14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n6br987n78yj0xvfr5zb1lvjfygs4b2afxy63k3lj7nbxj5g2pv")))

(define-public crate-ttcore_def-0.2.15 (c (n "ttcore_def") (v "0.2.15") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vkflpgf9p5ihqlay9ap0mkmx2l1an99pbgy2jnngarkg6r21icg")))

(define-public crate-ttcore_def-0.2.16 (c (n "ttcore_def") (v "0.2.16") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19kh8z3zzs2j7dd275hc556ncmk409hfvcv67crlg25mzvbvaxn2")))

(define-public crate-ttcore_def-0.2.17 (c (n "ttcore_def") (v "0.2.17") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "024azfhhqkz01vw8mn2wsx52fbhk6m0sf0b96c937a5cbbnr2anj")))

(define-public crate-ttcore_def-0.2.18 (c (n "ttcore_def") (v "0.2.18") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14ng0a4cypr8xjwyk509ph6bvxy7vk2xf2jlvm5q8sy8565kw0iq")))

(define-public crate-ttcore_def-0.2.19 (c (n "ttcore_def") (v "0.2.19") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lfqkpzb4ylvxd5rqg0k6x7hzbn0caj5lzspnk0q3alwiw222678")))

(define-public crate-ttcore_def-0.2.20 (c (n "ttcore_def") (v "0.2.20") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02m3j68vp1723w08jqxc20h723k1rn4g56746a2q026crzxrnpig")))

(define-public crate-ttcore_def-0.2.21 (c (n "ttcore_def") (v "0.2.21") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0znlyn8snl1mz2dx2dv9l7d1nraf307gqysl35r799gj1k4xyhp2")))

(define-public crate-ttcore_def-0.2.22 (c (n "ttcore_def") (v "0.2.22") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zi3wc8jzyai5gl717hngqy9di0wvy622naidby9h35srjnsq1m1")))

(define-public crate-ttcore_def-0.2.23 (c (n "ttcore_def") (v "0.2.23") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dnvyfj9klggqw1rg08ayxp97rls4lhfvxmnybgnhcn0iqhk9kqy")))

(define-public crate-ttcore_def-0.2.24 (c (n "ttcore_def") (v "0.2.24") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03cgkax5g25djgp2wz4vwfzfyv3c40chff07ag3ddzmgd3agha06")))

(define-public crate-ttcore_def-0.2.25 (c (n "ttcore_def") (v "0.2.25") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11saf4gqpfv2xgclfqjzrgk47zypw04s4cfjfwy3naqjiinc3yaz")))

(define-public crate-ttcore_def-0.2.26 (c (n "ttcore_def") (v "0.2.26") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yr98hs3xdkhx65lvircwh5s99fdf5y1w87z7kqq36lzcq3vpz5y")))

(define-public crate-ttcore_def-0.2.27 (c (n "ttcore_def") (v "0.2.27") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w2kcpsbkpkgglg9iv8i0gmy5lb8pmrghcqb8mz6jjw7897fvfsr")))

(define-public crate-ttcore_def-0.2.28 (c (n "ttcore_def") (v "0.2.28") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wavjifdpwxsp4agb9y84ijwyi6zhgwzanw7ll191zyqaimpgqs8")))

(define-public crate-ttcore_def-0.2.29 (c (n "ttcore_def") (v "0.2.29") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03igbz2zipxs94q40jrs9aif4rxsfsx3p7vmvmh61f1nf9a7b48l")))

(define-public crate-ttcore_def-0.2.30 (c (n "ttcore_def") (v "0.2.30") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x805xcrpgsmrf170vqggqrglzyq4a6y45przlp8lfwj73hayjwk")))

(define-public crate-ttcore_def-0.2.31 (c (n "ttcore_def") (v "0.2.31") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b9m1fih0w6f7bsabsr9jsbxcg31yg6xgv2h5ynanx6b33kpgzp7")))

(define-public crate-ttcore_def-0.2.32 (c (n "ttcore_def") (v "0.2.32") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ijm3ww5znrdz0q0qh7jdr48sgfn6vl0qyscb71panyy09d4f2iw")))

(define-public crate-ttcore_def-0.2.33 (c (n "ttcore_def") (v "0.2.33") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m7xd3qncm7bs4viliay0pavka1avrwb22ifxk6vjvm94p4xm8kr")))

(define-public crate-ttcore_def-0.2.34 (c (n "ttcore_def") (v "0.2.34") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14xa1fj6z02g3vw2fdlad5nbhbh1klk8k9yxd0yqnmwr54ivz8gb")))

(define-public crate-ttcore_def-0.2.35 (c (n "ttcore_def") (v "0.2.35") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hgw4p5kqcyjjnf76ydwxd0ry7gxi0lc0amhpsfp3p2zpkl6dmbw")))

