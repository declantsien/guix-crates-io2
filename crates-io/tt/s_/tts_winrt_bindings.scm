(define-module (crates-io tt s_ tts_winrt_bindings) #:use-module (crates-io))

(define-public crate-tts_winrt_bindings-0.1.0 (c (n "tts_winrt_bindings") (v "0.1.0") (d (list (d (n "winrt") (r "^0.7") (d #t) (k 0)) (d (n "winrt") (r "^0.7") (d #t) (k 1)))) (h "1h5ckj6am5f22gvfbwdsrycn90z2sm5342af4h41gf118w227992")))

(define-public crate-tts_winrt_bindings-0.2.0 (c (n "tts_winrt_bindings") (v "0.2.0") (d (list (d (n "winrt") (r "^0.7") (d #t) (k 0)) (d (n "winrt") (r "^0.7") (d #t) (k 1)))) (h "0q8l3rm1glcgjlphmc92lfh4yzd9r7mb60b0g9phfry002r17ky0")))

