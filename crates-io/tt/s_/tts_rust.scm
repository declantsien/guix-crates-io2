(define-module (crates-io tt s_ tts_rust) #:use-module (crates-io))

(define-public crate-tts_rust-0.1.0 (c (n "tts_rust") (v "0.1.0") (d (list (d (n "gtts") (r "^0.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "1gvasihsq4dnzxwbk6gqdby76nk9ybff6mnd0amj83li6zxhm9h2") (y #t)))

(define-public crate-tts_rust-0.1.1 (c (n "tts_rust") (v "0.1.1") (d (list (d (n "gtts") (r "^0.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "0f29kl3wy3jna5kqchdx1vf3bjyljrc71pw5cka574wdsn1lsvc4")))

(define-public crate-tts_rust-0.1.2 (c (n "tts_rust") (v "0.1.2") (d (list (d (n "gtts") (r "^0.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "15va31h2vp7sf4r7lzvr680k9v2syzv89jc8nzipbhx4lxi2x09v")))

(define-public crate-tts_rust-0.1.3 (c (n "tts_rust") (v "0.1.3") (d (list (d (n "gtts") (r "^0.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "1jzmd0srb0ixfykabcnmcgd35mw40fl907l8hnn2icvyx45iv6b6")))

(define-public crate-tts_rust-0.1.4 (c (n "tts_rust") (v "0.1.4") (d (list (d (n "gtts") (r "^0.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "1dhw8a0ykbx57xq6n94rfp2msn48myzb7anwb0ydqn8w72742h03")))

(define-public crate-tts_rust-0.2.0 (c (n "tts_rust") (v "0.2.0") (d (list (d (n "gtts") (r "^0.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "0ilr2r3pg23jn04mnss5rzhf2lxaj1ws508m2grcc4mn16dwk3qi")))

(define-public crate-tts_rust-0.3.0 (c (n "tts_rust") (v "0.3.0") (d (list (d (n "minreq") (r "^2.0.3") (f (quote ("https"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "0gjqh4qrny9r51j61sm4wbz1bnr69dn9y4h25hqhvgiziggf16m5")))

(define-public crate-tts_rust-0.3.1 (c (n "tts_rust") (v "0.3.1") (d (list (d (n "minreq") (r "^2.0.3") (f (quote ("https"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "0d81fc4af0inp059d2i30xai4xvxdhb1kiq530idvdlyk438s8kh")))

(define-public crate-tts_rust-0.3.2 (c (n "tts_rust") (v "0.3.2") (d (list (d (n "minreq") (r "^2.0.3") (f (quote ("https"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "1z0lyggwd85bxwfhr7y04ixawzkmas6mhhwm964b8iwbpq9hrj62")))

(define-public crate-tts_rust-0.3.3 (c (n "tts_rust") (v "0.3.3") (d (list (d (n "minreq") (r "^2.0.3") (f (quote ("https"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "0kdym9rrcvjl6cfrmllbb13f26jknimspcigakvv291223ds4cc8")))

(define-public crate-tts_rust-0.3.5 (c (n "tts_rust") (v "0.3.5") (d (list (d (n "minreq") (r "^2.0.3") (f (quote ("https"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1w75f64jmzzabdjzhrm2n5k3vq2msmpw0igxfkzi9rcspfnwvmrd")))

