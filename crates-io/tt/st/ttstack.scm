(define-module (crates-io tt st ttstack) #:use-module (crates-io))

(define-public crate-ttstack-0.0.1 (c (n "ttstack") (v "0.0.1") (d (list (d (n "ruc") (r "^0.4") (d #t) (k 0)))) (h "1pbvcwslnq16iidk2myphjiqjvw0hpv54kmzs6v03ql22jck5yqp")))

(define-public crate-ttstack-0.0.2 (c (n "ttstack") (v "0.0.2") (d (list (d (n "ruc") (r "^0.4") (d #t) (k 0)))) (h "0kq8lqjarfkzyrnaxff30kyzs2iilbhv3a3c2yif37bzf721rsz1")))

(define-public crate-ttstack-0.0.3 (c (n "ttstack") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "ruc") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ih9vbxj38xj6i2vwd3zwdw00v255mh0mcfkirl50sk0h6r0fv99")))

