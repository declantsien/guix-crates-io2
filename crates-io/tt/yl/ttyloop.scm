(define-module (crates-io tt yl ttyloop) #:use-module (crates-io))

(define-public crate-ttyloop-0.1.0 (c (n "ttyloop") (v "0.1.0") (d (list (d (n "aglet") (r "^0.5.1") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "0l8h6li5qp7x886hcg6yhljaknz4q2jdbdr37hnv54cwskhf2wsn")))

(define-public crate-ttyloop-0.1.1 (c (n "ttyloop") (v "0.1.1") (d (list (d (n "aglet") (r "^0.5.1") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "172b08dni2780jpynmzbym0qbhvdqawfc5dh2ysvhhkf5g4jhbqp")))

(define-public crate-ttyloop-0.2.0 (c (n "ttyloop") (v "0.2.0") (d (list (d (n "aglet") (r "^0.5.1") (k 0)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "13n818v5lfjc405a397yy39fsjlddizraxsnjlp29dq0w4rw7yl5")))

