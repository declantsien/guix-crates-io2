(define-module (crates-io tt aw ttaw) #:use-module (crates-io))

(define-public crate-ttaw-0.1.0 (c (n "ttaw") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "183pnr1fkizw5c783gaqy5ifi0sxrzqd38svl87piaph4f67vvna")))

(define-public crate-ttaw-0.2.0 (c (n "ttaw") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0i2vkfnycydpza8kx3r4br7maqd4ngp9r7fl1xvb44fp4a2ph1qg")))

(define-public crate-ttaw-0.3.0 (c (n "ttaw") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "170a00ndn5wsvnwq6mns674ssfcr6vlr2izf8g8brbiwjd41zhd9")))

