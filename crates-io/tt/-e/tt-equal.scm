(define-module (crates-io tt -e tt-equal) #:use-module (crates-io))

(define-public crate-tt-equal-0.1.0 (c (n "tt-equal") (v "0.1.0") (d (list (d (n "tt-call") (r "^1.0") (d #t) (k 0)))) (h "01izrip8spc6k7njdqmwrnkw184ai5wrk6npp2s4n7wvhibfz5mw")))

(define-public crate-tt-equal-0.1.1 (c (n "tt-equal") (v "0.1.1") (d (list (d (n "tt-call") (r "^1.0") (d #t) (k 0)))) (h "05cabr3x3xp128v8j0l3z9b235826akfbjmvlzmc413mj9xpq2va")))

(define-public crate-tt-equal-0.1.2 (c (n "tt-equal") (v "0.1.2") (d (list (d (n "tt-call") (r "^1.0") (d #t) (k 0)))) (h "16qj9az2fq3n2m5cmppg1d2xm3khx2q8qwkd1f135bqch30mlimy")))

