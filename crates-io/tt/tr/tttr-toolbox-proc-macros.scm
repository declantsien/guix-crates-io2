(define-module (crates-io tt tr tttr-toolbox-proc-macros) #:use-module (crates-io))

(define-public crate-tttr-toolbox-proc-macros-0.1.0 (c (n "tttr-toolbox-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xxhk4qlq3xqk3f5mjbxfv1iascp0gcssaf0zf1vyf3xz7gq81hn")))

(define-public crate-tttr-toolbox-proc-macros-0.1.1 (c (n "tttr-toolbox-proc-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pdpf6wn344lp10bmy61jyc8q2cmpxrjc94w60zzd6wsyfj3c621")))

