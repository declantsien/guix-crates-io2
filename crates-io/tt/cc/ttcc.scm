(define-module (crates-io tt cc ttcc) #:use-module (crates-io))

(define-public crate-ttcc-0.0.0 (c (n "ttcc") (v "0.0.0") (h "0ph56nxzqqnqcvna1gpl6ldlyr1b4vzchnh660mc1b99abvn9d7v") (y #t)))

(define-public crate-ttcc-0.0.1 (c (n "ttcc") (v "0.0.1") (h "05f4kmx2q0zca0jyvlqqpdmvq4vpvfas9wr5fzak3848n0x9s251") (y #t)))

