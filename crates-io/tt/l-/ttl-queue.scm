(define-module (crates-io tt l- ttl-queue) #:use-module (crates-io))

(define-public crate-ttl-queue-0.1.0 (c (n "ttl-queue") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("time"))) (o #t) (k 0)))) (h "1pdxjzqzky3ihdxy3lpyx0dyw308qyv5dkwhhl89cdns1dy6fdn0") (f (quote (("vecdeque") ("doublestack") ("default" "vecdeque")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-ttl-queue-0.2.0 (c (n "ttl-queue") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("time"))) (o #t) (k 0)))) (h "1v6yk5f9yzm1ilr7fw0pnp1422jdhh63f4j0vs4mfay4q4nwvv3z") (f (quote (("vecdeque") ("doublestack") ("default" "vecdeque")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

