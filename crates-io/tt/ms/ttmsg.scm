(define-module (crates-io tt ms ttmsg) #:use-module (crates-io))

(define-public crate-ttmsg-0.1.0 (c (n "ttmsg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "13yd106pajraw9xm05j2rs8k18yr00c2kliixz0c7svn9qsnhi58")))

(define-public crate-ttmsg-0.1.1 (c (n "ttmsg") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "198rkpx316wkm69zgcxd7xrjyn5pzdd29pw0asyw8vf2rlnpf3dm")))

