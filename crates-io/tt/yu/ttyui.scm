(define-module (crates-io tt yu ttyui) #:use-module (crates-io))

(define-public crate-ttyui-0.1.0 (c (n "ttyui") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "19fji19hijhdlg934y4ksgwbnpzycn7gn673r7052km2vqf9zkk5")))

(define-public crate-ttyui-0.1.1 (c (n "ttyui") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "1fsg9fw57kwmwhv594yry5xa76dn73hi4brmv81f16fgx92jyrac")))

(define-public crate-ttyui-0.1.2 (c (n "ttyui") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0zzirz7mjm4mfi4pdy2k15xjmiqavgznxw42j7mnlgibd9b1k4l8")))

(define-public crate-ttyui-0.1.3 (c (n "ttyui") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "08kx7lld4l8fkg82d86hzb5rzniww4dbxp22mrsnxhadkcjl85l4")))

(define-public crate-ttyui-0.1.4 (c (n "ttyui") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)))) (h "0m7qrsp0cpr00q5n7vdxq9g251pw7ayhxqbxczx8j8schpkjj26b")))

