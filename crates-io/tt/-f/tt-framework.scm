(define-module (crates-io tt -f tt-framework) #:use-module (crates-io))

(define-public crate-tt-framework-0.0.3 (c (n "tt-framework") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "ruc") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fr8fisnswsgszw644bph7gikii1068rz9pansd4q5cyys1n6847")))

