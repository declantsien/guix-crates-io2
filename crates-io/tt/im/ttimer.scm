(define-module (crates-io tt im ttimer) #:use-module (crates-io))

(define-public crate-ttimer-0.1.0 (c (n "ttimer") (v "0.1.0") (h "020f4rxajhwyz7rs8rfb1ik8fr9bsgkgcvy29ix07k5j7fwb4g8y")))

(define-public crate-ttimer-0.1.1 (c (n "ttimer") (v "0.1.1") (h "0bicfkg3bg2w9ici1mrd7b9wws45zp2k27v1r25jpxzjspfis1m6")))

(define-public crate-ttimer-0.1.2 (c (n "ttimer") (v "0.1.2") (h "0vwdkf9gg7r4c0kymfbiz2rg8j8j2jsx0msa1d404w7qa88j2cgg")))

(define-public crate-ttimer-0.1.3 (c (n "ttimer") (v "0.1.3") (h "0y7pc73blsmimzqh6bg6g5yfg6h86c0f4mzs50chm5pjxqnvdyfa")))

