(define-module (crates-io tt v- ttv-extensions) #:use-module (crates-io))

(define-public crate-ttv-extensions-0.1.0 (c (n "ttv-extensions") (v "0.1.0") (h "1h9kxh3h2ps3ix6fpciqfj76q89pkwy7ar02bxxlvlfq57pc8di7")))

(define-public crate-ttv-extensions-0.1.1 (c (n "ttv-extensions") (v "0.1.1") (h "13vyqvnz2h1498fz8j920s96va877yjjliwv669bv416mgid3qqw") (f (quote (("default") ("all"))))))

