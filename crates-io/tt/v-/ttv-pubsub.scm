(define-module (crates-io tt v- ttv-pubsub) #:use-module (crates-io))

(define-public crate-ttv-pubsub-0.1.0 (c (n "ttv-pubsub") (v "0.1.0") (h "00bah32rkannagqfyvjdr88y5iaj9ydpj3fr6z1x96xcvm38cyl0")))

(define-public crate-ttv-pubsub-0.1.1 (c (n "ttv-pubsub") (v "0.1.1") (d (list (d (n "twitch_api2") (r "^0.5.0-alpha.2") (f (quote ("pubsub"))) (k 0)))) (h "1gbc81mkpy9gi6avn888hc726h7fx4dbqzgv0g7r3hasgw0rkwqs") (f (quote (("unofficial" "twitch_api2/unsupported") ("default") ("allow_unknown_fields" "twitch_api2/allow_unknown_fields") ("all" "unofficial"))))))

