(define-module (crates-io tt v- ttv-v5) #:use-module (crates-io))

(define-public crate-ttv-v5-0.1.0 (c (n "ttv-v5") (v "0.1.0") (h "1mjmd1l2lwq70win5sj85hjy7yvk67525kcx33gs8k1nc0jjhyh1")))

(define-public crate-ttv-v5-0.1.1 (c (n "ttv-v5") (v "0.1.1") (h "0hgzdlc09jxihlk2hazz1ds336br549iiqc611r8m8wa90fm3wpl") (f (quote (("default") ("all"))))))

