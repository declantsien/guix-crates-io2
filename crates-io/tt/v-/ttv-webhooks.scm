(define-module (crates-io tt v- ttv-webhooks) #:use-module (crates-io))

(define-public crate-ttv-webhooks-0.1.0 (c (n "ttv-webhooks") (v "0.1.0") (h "1zamav1whqss7dvvmdgj42whlznapm46k8w94d6zsj0bmra256l7")))

(define-public crate-ttv-webhooks-0.1.1 (c (n "ttv-webhooks") (v "0.1.1") (h "0drs85kg4768nqw1i9cmmjmbwjk9fxac4mqyz1yb298h7llyn8rr") (f (quote (("default") ("all"))))))

