(define-module (crates-io tt v- ttv-tmi) #:use-module (crates-io))

(define-public crate-ttv-tmi-0.1.0 (c (n "ttv-tmi") (v "0.1.0") (d (list (d (n "twitch_api2") (r "^0.5.0-alpha.1") (f (quote ("tmi"))) (k 0)))) (h "1fawm3vd0xwjbd2jsm2rsdj6my5g4zgb3gczaw0i950gw17wcc1m")))

(define-public crate-ttv-tmi-0.1.1 (c (n "ttv-tmi") (v "0.1.1") (d (list (d (n "twitch_api2") (r "^0.5.0-alpha.2") (f (quote ("tmi"))) (k 0)))) (h "0xqi39m0pw9k6kk98snacz9zzk1skf40c2x1gwvh7cn8is24v6c4") (f (quote (("surf_client" "twitch_api2/surf_client") ("reqwest_client" "twitch_api2/reqwest_client") ("allow_unknown_fields" "twitch_api2/allow_unknown_fields") ("all" "reqwest_client" "surf_client"))))))

