(define-module (crates-io tt v- ttv-auth) #:use-module (crates-io))

(define-public crate-ttv-auth-0.1.0 (c (n "ttv-auth") (v "0.1.0") (d (list (d (n "twitch_oauth2") (r "^0.5.0-alpha.1") (d #t) (k 0)))) (h "0y51iyb934qfwk7xc15bgybyafhpjp2mpzr7wy48ilbn0yq3zds3")))

(define-public crate-ttv-auth-0.1.1 (c (n "ttv-auth") (v "0.1.1") (d (list (d (n "twitch_oauth2") (r "^0.5.0-alpha.2") (k 0)))) (h "0mcpvhn5s66mzl6r86a05xjayap85mrcmdhlq63hhmxjbn5nh3al") (f (quote (("surf_client" "twitch_oauth2/surf_client") ("reqwest_client" "twitch_oauth2/reqwest_client") ("all" "twitch_oauth2/all"))))))

