(define-module (crates-io tt v- ttv-graphql) #:use-module (crates-io))

(define-public crate-ttv-graphql-0.1.0 (c (n "ttv-graphql") (v "0.1.0") (h "0k90a6hxf6g89m24rnpcnwvfp5gkhzmrq4b05g730907wr3dhsh4")))

(define-public crate-ttv-graphql-0.1.1 (c (n "ttv-graphql") (v "0.1.1") (h "0867n7i2qp8b33yjjhx2qg8b7ny5j4w5s7zv0fvv4js2l41kbc1v") (f (quote (("default") ("all"))))))

