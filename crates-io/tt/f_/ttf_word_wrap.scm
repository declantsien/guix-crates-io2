(define-module (crates-io tt f_ ttf_word_wrap) #:use-module (crates-io))

(define-public crate-ttf_word_wrap-0.1.0 (c (n "ttf_word_wrap") (v "0.1.0") (d (list (d (n "ttf-parser") (r "^0.11.0") (d #t) (k 0)))) (h "0a45c2lq6hidf0z2z6khc9a9f80nafjw1qcg03gh7i0b1sxk144a")))

(define-public crate-ttf_word_wrap-0.2.0 (c (n "ttf_word_wrap") (v "0.2.0") (d (list (d (n "ttf-parser") (r "^0.11.0") (d #t) (k 0)))) (h "1068105g0qj2f9iqacfbx2wj0lnp5s86716q3gmfbhgvw2f0l881")))

(define-public crate-ttf_word_wrap-0.2.1 (c (n "ttf_word_wrap") (v "0.2.1") (d (list (d (n "ttf-parser") (r "^0.11.0") (d #t) (k 0)))) (h "0vs3k4qazn7c5w95acyld1z3dzxpzdhlxcbbl7hbgm7wykb1zk3l")))

(define-public crate-ttf_word_wrap-0.2.2 (c (n "ttf_word_wrap") (v "0.2.2") (d (list (d (n "ttf-parser") (r "^0.11.0") (d #t) (k 0)))) (h "1565n862lfg92ddygpl21hmxbcyjx424qqd56r0wj77pd5l735s6")))

(define-public crate-ttf_word_wrap-0.3.0 (c (n "ttf_word_wrap") (v "0.3.0") (d (list (d (n "ttf-parser") (r "^0.11.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0f986l1bchxgmsgllhgrvplirvwq7qy63fskai2a3qbfvg37xqkw")))

(define-public crate-ttf_word_wrap-0.4.0 (c (n "ttf_word_wrap") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.11.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1zs8ka28vvbq31s6i6xvbhgagv9ji65ffrrx2lj86bb50gg65rsz")))

(define-public crate-ttf_word_wrap-0.4.1 (c (n "ttf_word_wrap") (v "0.4.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.11.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "02xliiham74cc49ig31s0hxp2ipma371v3n2bwa7nkbfsj3q6y0s")))

(define-public crate-ttf_word_wrap-0.4.2 (c (n "ttf_word_wrap") (v "0.4.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.12.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1f4h9piyhh77m328diliz9mnrzwc8n08bq5bsrh47g291y5albf2")))

(define-public crate-ttf_word_wrap-0.5.0 (c (n "ttf_word_wrap") (v "0.5.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.12.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "113yhhqjak15cr8dx1x455c4b6cjaa59gkny92gxr194dl549wwg")))

