(define-module (crates-io tt l_ ttl_cache) #:use-module (crates-io))

(define-public crate-ttl_cache-0.1.0 (c (n "ttl_cache") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0cdrbhh2nw0jrz9g2x1zmg4xz1rhy01jhr7h2x2ld8ggkwrwyl7x") (y #t)))

(define-public crate-ttl_cache-0.1.1 (c (n "ttl_cache") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0k8kjjmm01c9fgslkkdkvq5sxmdzfma15r6r3wsj83kwk54pq2cc")))

(define-public crate-ttl_cache-0.2.0 (c (n "ttl_cache") (v "0.2.0") (d (list (d (n "linked-hash-map") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0lv44cdyri65px36m1ih34jc7984yyp3cmvvcsnbkkcvwan24vwv")))

(define-public crate-ttl_cache-0.2.1 (c (n "ttl_cache") (v "0.2.1") (d (list (d (n "linked-hash-map") (r "^0.3") (d #t) (k 0)))) (h "0xy1rl98k1a3xxpj7jnvn76625pi4vmc3zxzq6xj5vvg5ww1l41x")))

(define-public crate-ttl_cache-0.3.0 (c (n "ttl_cache") (v "0.3.0") (d (list (d (n "linked-hash-map") (r "^0.4") (d #t) (k 0)))) (h "1n5vmi0ss0k90dar7w48g86c49cw51xbkksgh8harmclk1ri4fk9")))

(define-public crate-ttl_cache-0.4.0 (c (n "ttl_cache") (v "0.4.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "15m67lfyz4vm25fqagkn6qix24jdb8p2d3pk952wrmwhcmwabivc") (y #t)))

(define-public crate-ttl_cache-0.4.1 (c (n "ttl_cache") (v "0.4.1") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "12habcj2k4dn9hdmdjckh2zj2w3xclf7xqpm1sqv5rmifsmbc64l") (f (quote (("stats") ("default"))))))

(define-public crate-ttl_cache-0.4.2 (c (n "ttl_cache") (v "0.4.2") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1nv8rifvc23plw2397rnzwx1j7ap0lhaj9lsivzxz5kqm779glnl") (f (quote (("stats") ("default"))))))

(define-public crate-ttl_cache-0.5.0 (c (n "ttl_cache") (v "0.5.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1xmwhgqdkmyj1ibhhfkdwq8bcc3plrxrkapkpf71gn4zdhq3pqra") (f (quote (("stats") ("default"))))))

(define-public crate-ttl_cache-0.5.1 (c (n "ttl_cache") (v "0.5.1") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "0sh9jajbl9nb6g4yx31i2j5c8q0lvypcwrgfnq88f5ph4q2qk2a1") (f (quote (("stats") ("default"))))))

