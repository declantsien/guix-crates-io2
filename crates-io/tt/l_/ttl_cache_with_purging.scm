(define-module (crates-io tt l_ ttl_cache_with_purging) #:use-module (crates-io))

(define-public crate-ttl_cache_with_purging-0.1.0 (c (n "ttl_cache_with_purging") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "0k8qlwp8h4wczc5dj52ipvh1hvg960y7myh2aafhbcd6caasknqs")))

