(define-module (crates-io tt ya ttyaskpass) #:use-module (crates-io))

(define-public crate-ttyaskpass-1.0.0 (c (n "ttyaskpass") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "seckey") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.2") (d #t) (k 0)))) (h "1d2zcfgb9lhbiycy8bxbvm8pcw61wvqr9x7fv4qp35c3skwagr4g") (y #t)))

(define-public crate-ttyaskpass-1.0.1 (c (n "ttyaskpass") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "seckey") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.2") (d #t) (k 0)))) (h "1p13dkk4nlxmki2lvjkfh28skp0i5qdib3f3kh49gjajy1vp5rff")))

(define-public crate-ttyaskpass-1.2.0 (c (n "ttyaskpass") (v "1.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "seckey") (r "^0.5") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.2") (d #t) (k 0)) (d (n "url") (r "^1.4") (o #t) (d #t) (k 0)))) (h "098q0fms799wm6ssqvhh2dwvwqn0q4nifg89iv36i82sshsppjfn") (f (quote (("pinentry" "nom" "url") ("default" "pinentry"))))))

(define-public crate-ttyaskpass-1.2.1 (c (n "ttyaskpass") (v "1.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "seckey") (r "^0.5") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.2") (d #t) (k 0)) (d (n "url") (r "^1.4") (o #t) (d #t) (k 0)))) (h "17qc2ri09zgp89fzrlqv5kip9ny31nhx9y02lc2im3fkpb922d05") (f (quote (("pinentry" "nom" "url") ("default" "pinentry"))))))

(define-public crate-ttyaskpass-2.0.0-alpha (c (n "ttyaskpass") (v "2.0.0-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "seckey") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)))) (h "0h0nfjq10z1s2vqdph9ih6gq3y8fqqjnvncsa8v9gr74a7nq62fw")))

(define-public crate-ttyaskpass-2.0.0-alpha.1 (c (n "ttyaskpass") (v "2.0.0-alpha.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "seckey") (r "^0.7.9") (d #t) (k 0)) (d (n "sha3") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)))) (h "09fg1vlmjd9q19ismyyxx9i4ch2xyqkckadhr26m2944049fzcsl")))

(define-public crate-ttyaskpass-2.0.0-alpha.2 (c (n "ttyaskpass") (v "2.0.0-alpha.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "seckey") (r "^0.8") (d #t) (k 0)) (d (n "sha3") (r "^0.7") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)))) (h "0dl0q87lm36hx5ya5a83fh3w72jq9lm3hbpvw6l1q40y07f3fdl7")))

(define-public crate-ttyaskpass-2.0.0-alpha.5 (c (n "ttyaskpass") (v "2.0.0-alpha.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mortal") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "seckey") (r "^0.9") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "1bmzwir33wglgz44fdqgrh3g8wfky85h66nqvrgkb5s45kjp7lgr") (f (quote (("nightly" "seckey/nightly"))))))

(define-public crate-ttyaskpass-2.0.0 (c (n "ttyaskpass") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mortal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "1q6gzjpm28vkimaqs5nr7i89jdl23nfk80h6kl3vzbcblrxdvpvs")))

(define-public crate-ttyaskpass-2.0.1 (c (n "ttyaskpass") (v "2.0.1") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mortal") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "1j5bvd1hz91lfgd0dkvrrm72imyzvnv1v487jnn2j47drja2gq82")))

