(define-module (crates-io tt yt ttytheme) #:use-module (crates-io))

(define-public crate-ttytheme-0.1.0 (c (n "ttytheme") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "human-panic") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.58") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.58") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1py7b3bzbqimzm3s8i2p2z98ikdvxpgr7xgg4pxbvpfpc3y7564y")))

(define-public crate-ttytheme-0.1.1 (c (n "ttytheme") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "human-panic") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.58") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.58") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1ghgbnpj5l010zpyyx0k4wib9dwiigzsdcifpyc2v25kvymmkh8v")))

