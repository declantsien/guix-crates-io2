(define-module (crates-io tt vm ttvm_core) #:use-module (crates-io))

(define-public crate-ttvm_core-0.1.0 (c (n "ttvm_core") (v "0.1.0") (h "1lrsapbq013cyhlasl5xcf3agh1xyr5kw4y5fg5fqvryjlmwyafv") (y #t)))

(define-public crate-ttvm_core-0.2.0 (c (n "ttvm_core") (v "0.2.0") (h "0krgydbliaq841ygf9w4rzcyzb30b8gh5xp1gaab376fip3zhjw6") (y #t)))

(define-public crate-ttvm_core-0.2.1 (c (n "ttvm_core") (v "0.2.1") (h "0jr6wmvf6npy2s2wcf853j95g34r1dw5n76nd8dhxbvx0708aql8") (y #t)))

(define-public crate-ttvm_core-0.2.2 (c (n "ttvm_core") (v "0.2.2") (h "0f88cdp0krc940ngx7nzf7zz3dvmw6wn109fl3lllayi91lflxaa") (y #t)))

(define-public crate-ttvm_core-0.2.3 (c (n "ttvm_core") (v "0.2.3") (h "1bw97nn9hrfldjqjsa6wsf11jmdidzrz30jlssxlj84fp5xwc3xi") (y #t)))

(define-public crate-ttvm_core-0.2.4 (c (n "ttvm_core") (v "0.2.4") (h "1rp33j04rhxc90lj84qkcl5scsjmgdx5d8zl9zyd1nk4z0lrzpjd") (y #t)))

(define-public crate-ttvm_core-0.2.5 (c (n "ttvm_core") (v "0.2.5") (h "1nqr3zx14spagkdym032vjirs97kwxs0f76y6m10zds6kjj892gi") (y #t)))

(define-public crate-ttvm_core-0.2.6 (c (n "ttvm_core") (v "0.2.6") (h "1d964qyhklghwbn41p0jlz92y70pi6f01fz5d59hz1d981swvjpa") (y #t)))

(define-public crate-ttvm_core-0.2.7 (c (n "ttvm_core") (v "0.2.7") (h "0qhkb6239p5p5ajrjy81fwxxcx3x1lz53fyiqk4y66i35mv827y3") (y #t)))

(define-public crate-ttvm_core-0.2.8 (c (n "ttvm_core") (v "0.2.8") (h "1r05h4rfhrw6bncr2892bx3pi94xqkqar3y6cgkcp801bxfkqqdf") (y #t)))

(define-public crate-ttvm_core-0.2.9 (c (n "ttvm_core") (v "0.2.9") (h "0jz0mqzi4x0lgzq9ng5xca97xpswsyf0igb842kwxdjnzsyi5pig") (y #t)))

(define-public crate-ttvm_core-0.3.0 (c (n "ttvm_core") (v "0.3.0") (h "0xai7fn87p1p8japf92yym7q49vhacpk9vmvsqc5rchb9fg3pfww") (y #t)))

(define-public crate-ttvm_core-0.3.1 (c (n "ttvm_core") (v "0.3.1") (h "1834l0rpjy15vcjxzq3a0xxcl7nrv6adjcdqnsh4r4qf3bmr0gj2") (y #t)))

(define-public crate-ttvm_core-0.3.2 (c (n "ttvm_core") (v "0.3.2") (h "0ahb7fdp8j6170d2zp2dnpkm502ss0838yb0czkavwvx1jgbx7b2") (y #t)))

(define-public crate-ttvm_core-0.3.3 (c (n "ttvm_core") (v "0.3.3") (h "1vh08qrqvlfmx7wqfrk5vrj8a1r0vdvkdw15slhh23f4dkj1b016") (y #t)))

(define-public crate-ttvm_core-0.3.4 (c (n "ttvm_core") (v "0.3.4") (h "1fh3n1hv1wb89r082nl24h438df6v79q2k8nshh8gh7k5dvspkc9") (y #t)))

(define-public crate-ttvm_core-0.3.5 (c (n "ttvm_core") (v "0.3.5") (h "1nlfv344rc752q7hrq1lk2y9hzn1wvxckj4j9rlrwi85mshhhskj") (y #t)))

