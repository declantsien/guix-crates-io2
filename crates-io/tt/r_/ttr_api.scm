(define-module (crates-io tt r_ ttr_api) #:use-module (crates-io))

(define-public crate-ttr_api-1.0.0 (c (n "ttr_api") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16s90y91bwsdr2vanlj7xx2ry56q35fa4xwp4pyz47cyq6jd30vk")))

(define-public crate-ttr_api-1.1.0 (c (n "ttr_api") (v "1.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jkwyr4kz0l2w2n8i00b48kyv4wv4phfja2sxr7f74az532zrdd9")))

(define-public crate-ttr_api-1.2.0 (c (n "ttr_api") (v "1.2.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g3xjc7p0xc66yprfkxgqpm848d2p2083q6df1xx9903ghfl98p8")))

(define-public crate-ttr_api-1.2.1 (c (n "ttr_api") (v "1.2.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kgjzx9xk59pa26i9vr6030yqy0iv41nnfv70zayamclh1kl34bm")))

(define-public crate-ttr_api-1.3.0 (c (n "ttr_api") (v "1.3.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xzdxmg0ya80mmnl7fd0y0cgam23j8bx9l7kcz788z54b1biz96q")))

