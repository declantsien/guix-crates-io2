(define-module (crates-io tt ir ttir) #:use-module (crates-io))

(define-public crate-ttir-0.1.1 (c (n "ttir") (v "0.1.1") (d (list (d (n "mtk") (r "^0.1.2") (d #t) (k 0)) (d (n "ttvm") (r "^0.2.0") (d #t) (k 0)))) (h "01xmlvpi7pmbkgdmbhazwg3xm0d356p3088k5xd261kn70arw6h1") (y #t)))

(define-public crate-ttir-0.1.2 (c (n "ttir") (v "0.1.2") (d (list (d (n "mtk") (r "^0.1.2") (d #t) (k 0)) (d (n "rsap") (r "^0.3.1") (d #t) (k 0)) (d (n "ttvm") (r "^0.2.0") (d #t) (k 0)))) (h "102yklcfkrsqrixmjlqpw747nd6n9zwgb8nijac9a9lgwvxsswkx") (y #t)))

(define-public crate-ttir-0.1.3 (c (n "ttir") (v "0.1.3") (d (list (d (n "mtk") (r "^0.1.2") (d #t) (k 0)) (d (n "rsap") (r "^0.3.1") (d #t) (k 0)) (d (n "ttvm") (r "^0.2.1") (d #t) (k 0)))) (h "0433zkv3i5grs9q53666l7ppdka4lj18n4r1nyb8hx6zz8pfy3gm") (y #t)))

(define-public crate-ttir-0.1.4 (c (n "ttir") (v "0.1.4") (d (list (d (n "mtk") (r "^0.1.2") (d #t) (k 0)) (d (n "rsap") (r "^0.3.1") (d #t) (k 0)) (d (n "ttvm") (r "^0.2.3") (d #t) (k 0)))) (h "001hj6vrra34h1595pia3z7bccxshrajw2nx5ls4ya9i8ksm2wf2") (y #t)))

