(define-module (crates-io tt yc ttycolor) #:use-module (crates-io))

(define-public crate-ttycolor-0.1.0 (c (n "ttycolor") (v "0.1.0") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1r2dlp5qzhpxqhrz5zz3mzjxmf55hhycinsa1g6jq5x6gi2fdyn9")))

(define-public crate-ttycolor-0.1.1 (c (n "ttycolor") (v "0.1.1") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "0h4lkqysjkjc1srbmsj6nyb04gqk2n5a414snfdbcbn0zq1ggq1h")))

(define-public crate-ttycolor-0.1.2 (c (n "ttycolor") (v "0.1.2") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "1y50x3rbkkg1p97vkhzikg50vgar1fzr65wg8ndx2i8q6h5cm983")))

(define-public crate-ttycolor-0.1.3 (c (n "ttycolor") (v "0.1.3") (d (list (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "00ccj8yh49myy41pnrci1sp5gazgqi2c2vqcpw7dv3m77n2bkyh4")))

