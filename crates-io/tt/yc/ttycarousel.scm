(define-module (crates-io tt yc ttycarousel) #:use-module (crates-io))

(define-public crate-ttycarousel-0.1.0 (c (n "ttycarousel") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("io-std" "io-util" "rt" "time" "sync"))) (o #t) (d #t) (k 0)))) (h "1g89zrq0d925snpcb4aiik543g70xzvnjm9az433aqwn4wbclzkk") (f (quote (("tokio1" "tokio") ("sync"))))))

(define-public crate-ttycarousel-0.1.1 (c (n "ttycarousel") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("io-std" "io-util" "rt" "time" "sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1k24214assx7dssgqk98cmnh6wwbi67bar4z7m2iadnz63wc6xx3") (f (quote (("tokio1" "tokio") ("sync"))))))

(define-public crate-ttycarousel-0.1.2 (c (n "ttycarousel") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("io-std" "io-util" "rt" "time" "sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "176mr3x3s6gbfnp4d160cgnsc2l7yd9hpb9344xg6b4rnvb7wmyw") (f (quote (("tokio1" "tokio") ("sync"))))))

(define-public crate-ttycarousel-0.1.3 (c (n "ttycarousel") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("io-std" "io-util" "rt" "time" "sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1xlsa6xgrkyl481ahggad1fd2svzkpx0x497a7ssqbfzxbplqlbs") (f (quote (("tokio1" "tokio") ("sync"))))))

(define-public crate-ttycarousel-0.1.4 (c (n "ttycarousel") (v "0.1.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("io-std" "io-util" "rt" "time" "sync"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1q2p0hvqydmpaci31k31plvhzhmm38sbmxnawrzxyxsm0z64xp9g") (f (quote (("tokio1" "tokio") ("sync"))))))

