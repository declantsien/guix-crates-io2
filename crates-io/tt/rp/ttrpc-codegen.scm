(define-module (crates-io tt rp ttrpc-codegen) #:use-module (crates-io))

(define-public crate-ttrpc-codegen-0.1.1 (c (n "ttrpc-codegen") (v "0.1.1") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.14.0") (d #t) (k 0)) (d (n "ttrpc-compiler") (r "^0.3") (d #t) (k 0)))) (h "0zd95w23isg3zz299pjr95r6gfii8ld5k77wvyibv0hsb19mw9ml")))

(define-public crate-ttrpc-codegen-0.1.2 (c (n "ttrpc-codegen") (v "0.1.2") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.14.0") (d #t) (k 0)) (d (n "ttrpc-compiler") (r "^0.3") (d #t) (k 0)))) (h "19g59iv3yccg19y0mrikn8hc1gh1cj11z0ga7f1zllc3g96q9s0j")))

(define-public crate-ttrpc-codegen-0.2.0 (c (n "ttrpc-codegen") (v "0.2.0") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.14.0") (d #t) (k 0)) (d (n "ttrpc-compiler") (r "^0.4.0") (d #t) (k 0)))) (h "0204m4i6fvjyjmbl1pvarvhwkrmv85mivdp40iqj684q8m7dm7l0")))

(define-public crate-ttrpc-codegen-0.3.0 (c (n "ttrpc-codegen") (v "0.3.0") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.14.0") (d #t) (k 0)) (d (n "ttrpc-compiler") (r "^0.5.0") (d #t) (k 0)))) (h "116arqz22hh4ay7cjyjwnjdnqvy218dg94fxsjlq2ffvj5g0fqr6")))

(define-public crate-ttrpc-codegen-0.4.0 (c (n "ttrpc-codegen") (v "0.4.0") (d (list (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.14.0") (d #t) (k 0)) (d (n "ttrpc-compiler") (r "^0.6.0") (d #t) (k 0)))) (h "1k21b551qc30y5xshmghqf2bikwla1grc98lfzq877as3facivyz") (y #t)))

(define-public crate-ttrpc-codegen-0.4.1 (c (n "ttrpc-codegen") (v "0.4.1") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.1.0") (d #t) (k 0)) (d (n "protobuf-support") (r "^3.1.0") (d #t) (k 0)) (d (n "ttrpc-compiler") (r "^0.6.1") (d #t) (k 0)))) (h "0843dg6g3vh3bzzavlhz6zl0l6pgp5wyfgaiqpbdi5nvqgkadvpk")))

(define-public crate-ttrpc-codegen-0.2.1 (c (n "ttrpc-codegen") (v "0.2.1") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.27.3") (d #t) (k 0) (p "protobuf-codegen-pure3")) (d (n "ttrpc-compiler") (r "^0.4.0") (d #t) (k 0)))) (h "1i50pz3fyh1qlb4s5nd7hhds2kpxlypqi3i2z2jwjmdjp27vdv0x")))

(define-public crate-ttrpc-codegen-0.3.1 (c (n "ttrpc-codegen") (v "0.3.1") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.27.3") (d #t) (k 0) (p "protobuf-codegen-pure3")) (d (n "ttrpc-compiler") (r "^0.5.0") (d #t) (k 0)))) (h "0k7kg508bqs3rha9amc2jjjspk53plx2c2w7b2h7p5xxlb8fxaml")))

(define-public crate-ttrpc-codegen-0.3.2 (c (n "ttrpc-codegen") (v "0.3.2") (d (list (d (n "protobuf") (r "^2.28.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.28.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.28.1") (d #t) (k 0) (p "protobuf-codegen-pure3")) (d (n "ttrpc-compiler") (r "^0.5.0") (d #t) (k 0)))) (h "0f48q5wml64a2zjrrmf4gjgsdxxjfr42jfbnwb2qckzrh6wrvmic")))

(define-public crate-ttrpc-codegen-0.2.2 (c (n "ttrpc-codegen") (v "0.2.2") (d (list (d (n "protobuf") (r "^2.28.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.28.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.28.1") (d #t) (k 0) (p "protobuf-codegen-pure3")) (d (n "ttrpc-compiler") (r "^0.4.0") (d #t) (k 0)))) (h "1h3qzfvai5slc426df189szjwwl8ia72pfh55cbqkigq5kyaz06z")))

(define-public crate-ttrpc-codegen-0.2.3 (c (n "ttrpc-codegen") (v "0.2.3") (d (list (d (n "protobuf") (r "^2.28.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.28.2") (d #t) (k 0) (p "protobuf-codegen3")) (d (n "protobuf-codegen-pure") (r "^2.28.2") (d #t) (k 0) (p "protobuf-codegen-pure3")) (d (n "ttrpc-compiler") (r "^0.4.3") (d #t) (k 0)))) (h "1yaq4ghxg2yzrzcfvgsahjkn2n5b89sjq5mhl6yjjayqk8bwjr93")))

(define-public crate-ttrpc-codegen-0.4.2 (c (n "ttrpc-codegen") (v "0.4.2") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.2.0") (d #t) (k 0)) (d (n "protobuf-support") (r "^3.2.0") (d #t) (k 0)) (d (n "ttrpc-compiler") (r "^0.6.1") (d #t) (k 0)))) (h "1hj08c1c1ghvjyc1bh2f3npcgjvjc35x8z542pbvx7ks3mizgmwl")))

(define-public crate-ttrpc-codegen-0.2.4 (c (n "ttrpc-codegen") (v "0.2.4") (d (list (d (n "protobuf") (r "^2.28.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.28.2") (d #t) (k 0) (p "protobuf-codegen3")) (d (n "protobuf-codegen-pure") (r "^2.28.2") (d #t) (k 0) (p "protobuf-codegen-pure3")) (d (n "ttrpc-compiler") (r "^0.4.4") (d #t) (k 0)))) (h "1wf6y9dz4bck2f3drf4zc7azkrqj4xi4wiiy3i7jb5h3cbpr63x0")))

