(define-module (crates-io tt se ttserver_def) #:use-module (crates-io))

(define-public crate-ttserver_def-0.2.10 (c (n "ttserver_def") (v "0.2.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1pykm81dnfqd6xjwnhh3airz97vpcfgnxnbbx3j1r0zi2yc24brg")))

(define-public crate-ttserver_def-0.2.11 (c (n "ttserver_def") (v "0.2.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "0yf66n99sh66nmp6xb1nv94j5b73zmwzk8lkm9jzqnn5mfg52jf3")))

(define-public crate-ttserver_def-0.2.14 (c (n "ttserver_def") (v "0.2.14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "0cfglsfxjx2z38xwllpak5l2cwc5587mr1mx9z1zcj6p8xlr1d8g")))

(define-public crate-ttserver_def-0.2.15 (c (n "ttserver_def") (v "0.2.15") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "0dfjgbf9pqwmjgl315d46lz5jmi5kd1746kc0kfgj5nkgcrwk52k")))

(define-public crate-ttserver_def-0.2.16 (c (n "ttserver_def") (v "0.2.16") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "14yl080d96wn7w98p63bha9j6315whl2783fv9ksar6rzivcbz58")))

(define-public crate-ttserver_def-0.2.17 (c (n "ttserver_def") (v "0.2.17") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1xj00k5w2ar6g8gc4j6jrrf7xlfzwbmm8fszkh5ikhcw3y6rkhdz")))

(define-public crate-ttserver_def-0.2.18 (c (n "ttserver_def") (v "0.2.18") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "00jsmfjphmpb6mgzjjqh0357n763ac5g8bm4d42jdvmkkz2j7yr5")))

(define-public crate-ttserver_def-0.2.19 (c (n "ttserver_def") (v "0.2.19") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1ixmab48fsw3i4y9iiac0cvi48rfvsf9jgi0qqd0niv5bcix2mkl")))

(define-public crate-ttserver_def-0.2.20 (c (n "ttserver_def") (v "0.2.20") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1psrrb4nnv1yab0jv2s6fwsc0q9m381244jfh2k479zs1rdf3664")))

(define-public crate-ttserver_def-0.2.21 (c (n "ttserver_def") (v "0.2.21") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1kq256xankrxczij70w7yz915hc7nkcjynaj78b51jc78w5z5kj9")))

(define-public crate-ttserver_def-0.2.22 (c (n "ttserver_def") (v "0.2.22") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "0nvvki81z9jj6gfaffh0aj9i4a0baxaairsrksz7f1sys4b0hbk8")))

(define-public crate-ttserver_def-0.2.23 (c (n "ttserver_def") (v "0.2.23") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "12fj8wa0ina629iylcr71sd89b0ww7zjnm086g2bdxakr321mi33")))

(define-public crate-ttserver_def-0.2.24 (c (n "ttserver_def") (v "0.2.24") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "0yckymivk91r9m6rhwb69bcn6w8jgvmr3c0rs62vrlm2xz0gljnq")))

(define-public crate-ttserver_def-0.2.25 (c (n "ttserver_def") (v "0.2.25") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1md38bqgz5nimvaxqsp50vnhzwds0gcs9i18x1p58p2n1ikn2cnm")))

(define-public crate-ttserver_def-0.2.26 (c (n "ttserver_def") (v "0.2.26") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1a34m870r1jyqckg5dqdljgp2vl90aj61fqwf2nbl317zqfxihgq")))

(define-public crate-ttserver_def-0.2.27 (c (n "ttserver_def") (v "0.2.27") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "0pj5hs22k2251gg4pygcp7c1fd8gqfjcssalfanhzdv22gz1kav3")))

(define-public crate-ttserver_def-0.2.28 (c (n "ttserver_def") (v "0.2.28") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "0jjl033km55ab94gmjdddh285fp2dh9y3h93cc4hhfxg2jm5vv97")))

(define-public crate-ttserver_def-0.2.29 (c (n "ttserver_def") (v "0.2.29") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "0n7qxvkh0p5hav5frvbkxak0knv4wr5figckvvbb9j8i1gh9xv1i")))

(define-public crate-ttserver_def-0.2.30 (c (n "ttserver_def") (v "0.2.30") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1pr3fg0fvi33a97k1ylkf60ckb0nprrh8zy69xga81f43ymnbk3c")))

(define-public crate-ttserver_def-0.2.31 (c (n "ttserver_def") (v "0.2.31") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1j0rd4nz9r8f6bhwy6fkqjf8ppqfimqvrw7makxymbwsj5jp6dyq")))

(define-public crate-ttserver_def-0.2.32 (c (n "ttserver_def") (v "0.2.32") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1ihicwpjyjcjibf3lnl2r1cqra104jajqwgfjzkvh3yqdxdk48bq")))

(define-public crate-ttserver_def-0.2.33 (c (n "ttserver_def") (v "0.2.33") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1vyghszlcl1xir006dmpmrm5ddzn6x4c87mgl0pypfib2yh1vji6")))

(define-public crate-ttserver_def-0.2.34 (c (n "ttserver_def") (v "0.2.34") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "1a84hvs2g0mfbpn69i5ji6j319r65dbw555kmm5yl7jrhrvmpqzg")))

(define-public crate-ttserver_def-0.2.35 (c (n "ttserver_def") (v "0.2.35") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ttcore_def") (r "^0.2") (d #t) (k 0)))) (h "17545vcqr5fkibhmqp9d4w6q69v7cn10gddxy6swip3lxqkd2n49")))

