(define-module (crates-io tt yr ttyrec) #:use-module (crates-io))

(define-public crate-ttyrec-0.1.0 (c (n "ttyrec") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.29") (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (o #t) (d #t) (k 0)))) (h "1inayplh7kazm2d1xap7lnx74bla70lvig39x98acf5p4i50wk03") (f (quote (("default" "async") ("async" "tokio" "futures"))))))

(define-public crate-ttyrec-0.2.0 (c (n "ttyrec") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.29") (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (o #t) (d #t) (k 0)))) (h "019ck27kpr72v2d5bf2r8r9hpy1kjsbsl7wf4fsy5c7gnicqgqi4") (f (quote (("default" "async") ("async" "tokio" "futures"))))))

(define-public crate-ttyrec-0.3.0 (c (n "ttyrec") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.18") (o #t) (d #t) (k 0)))) (h "07iqxq10ccgb83di8ncixqdqzkm1f3a7f0pk0yxgjsanyhb8yrkd") (f (quote (("default" "async") ("async" "futures"))))))

(define-public crate-ttyrec-0.3.1 (c (n "ttyrec") (v "0.3.1") (d (list (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "0mxrkhvflj4gw8wk2apsc3vi3sa0q92cxmzczranhp52fg2wcqhd") (f (quote (("default" "async") ("async" "futures-lite"))))))

(define-public crate-ttyrec-0.3.2 (c (n "ttyrec") (v "0.3.2") (d (list (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "0mc6jfy5af0n6n6w90rj2d7wyyx5c7nwi7kqqk48amj0pkl54xzg") (f (quote (("default" "async") ("async" "futures-lite"))))))

(define-public crate-ttyrec-0.3.3 (c (n "ttyrec") (v "0.3.3") (d (list (d (n "futures-lite") (r "^1.12.0") (o #t) (d #t) (k 0)))) (h "0md73w7phssxgd5jkpmh1kymggrc4lrib4b10kj43nsfz3zmqj2m") (f (quote (("default" "async") ("async" "futures-lite"))))))

(define-public crate-ttyrec-0.4.0 (c (n "ttyrec") (v "0.4.0") (d (list (d (n "tokio") (r "^1.26.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)))) (h "0kxqvn5qk8phvg6hxmvvpn6qq0mbmnnxb00ca14p3sl65v5nfw55") (f (quote (("default") ("async" "tokio"))))))

