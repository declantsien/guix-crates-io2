(define-module (crates-io tt xc ttxcat) #:use-module (crates-io))

(define-public crate-ttxcat-0.1.0 (c (n "ttxcat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "teletext") (r "^0.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0qz8i56hlp06px3f9938qy4qdkn41ybvf1k9lmb0888kisvgjbj0")))

(define-public crate-ttxcat-0.1.1 (c (n "ttxcat") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "teletext") (r "^0.1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "19jkz82bsyq7iq36rv2mf29qh5rma17091hhv4qd2jid1ch3hl31")))

