(define-module (crates-io ab ox abox) #:use-module (crates-io))

(define-public crate-abox-0.1.0 (c (n "abox") (v "0.1.0") (h "0i9n30z099dmklba7cg0b4rj7z2fi6vvpssdhm1cin37cgv8jc4a")))

(define-public crate-abox-0.2.0 (c (n "abox") (v "0.2.0") (h "14pfljr4i0ljghmx85ijyz8873wmihp92nk7wfv78zq5yxvyw4ki")))

(define-public crate-abox-0.3.0 (c (n "abox") (v "0.3.0") (h "056mj9b8lq1cw6m5hw0dk5jpah9jbs7im9k9pw9ddrfwmnr2iy7p")))

(define-public crate-abox-0.4.0 (c (n "abox") (v "0.4.0") (h "13rq2h50ycmyd0xxbcyr40rbyi5mv2h76s8z6j8rwi2va0x48dr4")))

(define-public crate-abox-0.4.1 (c (n "abox") (v "0.4.1") (h "0n1gf8nr8jdj068h6a7nsklnbds64l0b9z2k099qxxcmk3f72faa")))

