(define-module (crates-io ab le ablescript_cli) #:use-module (crates-io))

(define-public crate-ablescript_cli-0.2.0 (c (n "ablescript_cli") (v "0.2.0") (d (list (d (n "ablescript") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)))) (h "0ivdx66yms5ia7mb1i1n1mqg8g29nfrsp2nik3hb9afggnb1asd6")))

(define-public crate-ablescript_cli-0.3.0 (c (n "ablescript_cli") (v "0.3.0") (d (list (d (n "ablescript") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)))) (h "00f2sc27vffwq6z42ifrlw2dxcq8blxr2wi100cb5jqj6hz1w7k1")))

(define-public crate-ablescript_cli-0.4.0 (c (n "ablescript_cli") (v "0.4.0") (d (list (d (n "ablescript") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)))) (h "0m1756mb13j3v1qgk1dwcl4gzydy9zcq5zb7fl9agq1mqwd1q70z")))

(define-public crate-ablescript_cli-0.5.0 (c (n "ablescript_cli") (v "0.5.0") (d (list (d (n "ablescript") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)))) (h "02ag7dnrl2m0da2waalkl3rpjsfbp7kmjcrb1w68111v4jpq9yxk")))

(define-public crate-ablescript_cli-0.5.2 (c (n "ablescript_cli") (v "0.5.2") (d (list (d (n "ablescript") (r "^0.5.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "rustyline") (r "^9.1") (d #t) (k 0)))) (h "1cbg35jyc9avwf97p4w4ks3815553795npmvin576s6g4hx1c129")))

(define-public crate-ablescript_cli-0.5.3 (c (n "ablescript_cli") (v "0.5.3") (d (list (d (n "ablescript") (r "^0.5.3") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustyline") (r "^11.0") (d #t) (k 0)))) (h "07l5ps0kd4z6gwbl798hyn7y05rxnf2c7bni550dl6ssxxrn806s")))

(define-public crate-ablescript_cli-0.5.4 (c (n "ablescript_cli") (v "0.5.4") (d (list (d (n "ablescript") (r "^0.5.3") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rustyline") (r "^11.0") (d #t) (k 0)))) (h "1bha6wwkdcn27h3vyl0m9pyf2fy041apikyi8gwp836j1dh7y2w9")))

