(define-module (crates-io ab le abletime) #:use-module (crates-io))

(define-public crate-abletime-0.1.0 (c (n "abletime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)))) (h "1pmgh9nna05p9a6yy5qdrv32girsas82yrn6drf8blja6zcx62qm")))

(define-public crate-abletime-0.1.1 (c (n "abletime") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "semver") (r "^0.10.0") (d #t) (k 0)))) (h "0nyvvjwxa5b6yp62zha6x9pd007qaq4wrdnb5yv9ivj6k25mq8xs")))

