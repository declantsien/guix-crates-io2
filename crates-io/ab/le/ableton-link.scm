(define-module (crates-io ab le ableton-link) #:use-module (crates-io))

(define-public crate-ableton-link-0.1.0 (c (n "ableton-link") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1vfw9aa8q5rdk9dqkvd4pdq4hm8bv42ggj526yaf6lxjb8rg28iq")))

