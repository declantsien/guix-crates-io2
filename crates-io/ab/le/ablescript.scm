(define-module (crates-io ab le ablescript) #:use-module (crates-io))

(define-public crate-ablescript-0.2.0 (c (n "ablescript") (v "0.2.0") (d (list (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0ir8clp4i8jc1c6wq724lwpl21xxvn3j1qsq97d3im8qdqqracw4")))

(define-public crate-ablescript-0.3.0 (c (n "ablescript") (v "0.3.0") (d (list (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0fkcm1dq238pnapj7i3g7q51rx1clp0ab3n2iacnmj1bwrcjj8xk")))

(define-public crate-ablescript-0.4.0 (c (n "ablescript") (v "0.4.0") (d (list (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "15jdpv9drayp53xsdfy3spyrrk71xw5g23kcpybnjgq7wkxmls0l")))

(define-public crate-ablescript-0.5.0 (c (n "ablescript") (v "0.5.0") (d (list (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "07flbk058rbxqi6y2v5kh0kr8ig60xv9q648wqigszz3gm5d7v48")))

(define-public crate-ablescript-0.5.2 (c (n "ablescript") (v "0.5.2") (d (list (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1yr7blycgixah87y87b0assj482jkxm6nvkv5wq3rwi8z24syw1g")))

(define-public crate-ablescript-0.5.3 (c (n "ablescript") (v "0.5.3") (d (list (d (n "logos") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0rm58nxhxc7lafcbs0i6h5vijvcbdqwrb20h3bvag731bwsciyyf")))

(define-public crate-ablescript-0.5.4 (c (n "ablescript") (v "0.5.4") (d (list (d (n "logos") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0j92l9086yq6hzhc1j21dcm2aaq0wwr5gxanhrqm0d7akl37cgsz")))

