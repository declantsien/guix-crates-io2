(define-module (crates-io ab ru abrute) #:use-module (crates-io))

(define-public crate-abrute-0.0.1 (c (n "abrute") (v "0.0.1") (d (list (d (n "digits") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "1jvi16j6ddnpzzfbj7ha0rcl3zfcib93l8v9qqpxks5gqiv9bbql")))

(define-public crate-abrute-0.0.2 (c (n "abrute") (v "0.0.2") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "digits") (r "^0.3.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "1hrvi6m80v66iim3pacs6m77wpwxzs9c6lbnjin54jh93p5yv0sl")))

(define-public crate-abrute-0.0.3 (c (n "abrute") (v "0.0.3") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "digits") (r "^0.3.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "1k7423pdk5bkkcwkissy2wkcy8glgs4g66f2q6q3yf0g4kzkbsn4")))

(define-public crate-abrute-0.0.5 (c (n "abrute") (v "0.0.5") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "digits") (r "^0.3.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "058fs29yjfy75xpwa0nv4v731xhhih2bi7dwzdyccyf2fy2g66nj")))

(define-public crate-abrute-0.0.6 (c (n "abrute") (v "0.0.6") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "digits") (r "^0.3.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "1ldabh3g0ij84kac8f73dd7hd6wp069bfvxbrjj14xjzk08fscwr")))

(define-public crate-abrute-0.1.0 (c (n "abrute") (v "0.1.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "digits") (r "^0.3.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "18cwhpjvb1qcp474vazl6z62ibrwj316kl6pvyggp8afkvipzc6x")))

(define-public crate-abrute-0.1.1 (c (n "abrute") (v "0.1.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "digits") (r "^0.3.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)))) (h "0jdcnv89wra6xwqbir6q2g1chh3nr44md3xb0xx08qahn20glksh")))

(define-public crate-abrute-0.1.2 (c (n "abrute") (v "0.1.2") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "digits") (r "^0.3.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0ca5z7djqhipp8sya6pxgcfwq8dqaa797gaspxc70m9ahj1a4ilv")))

(define-public crate-abrute-0.1.3 (c (n "abrute") (v "0.1.3") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "digits") (r "^0.3.6") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "14nggamcafw8dv9krhvfhxl6j0sqy82dmdad5a7y8i4bn4zcy096")))

(define-public crate-abrute-0.1.5 (c (n "abrute") (v "0.1.5") (d (list (d (n "array_tool") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "digits") (r "~1.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "02n129baqphs08x9q9an1kgzw7czgldbm9z60c1565a19s2r2k9f")))

(define-public crate-abrute-0.1.6 (c (n "abrute") (v "0.1.6") (d (list (d (n "array_tool") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "digits") (r "~1.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1kz7iajcg4dnhx5yk42a5prnha64ryp2zvdq0nmq5pxs6bkxxsay")))

(define-public crate-abrute-0.1.7 (c (n "abrute") (v "0.1.7") (d (list (d (n "array_tool") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "~2.26") (d #t) (k 0)) (d (n "digits") (r "~1.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "~1.7") (d #t) (k 0)) (d (n "rayon") (r "~0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)) (d (n "tiny_http") (r "~0.5") (d #t) (k 0)))) (h "1fflpzfhnlmdq9pi2lsghbrg65ndibxck2dmvv4rkhm9q75x4smv")))

(define-public crate-abrute-0.1.8 (c (n "abrute") (v "0.1.8") (d (list (d (n "array_tool") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "~2.26") (d #t) (k 0)) (d (n "digits") (r "~1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "~1.7") (d #t) (k 0)) (d (n "rayon") (r "~0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)) (d (n "tiny_http") (r "~0.5") (d #t) (k 0)))) (h "02srqqabkxddl51fxaqza4cvxp5idn04fxg5rmbrk9gjp6lxawa2")))

(define-public crate-abrute-0.1.9 (c (n "abrute") (v "0.1.9") (d (list (d (n "array_tool") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "~3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "digits") (r "~1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "num_cpus") (r "~1.7") (d #t) (k 0)) (d (n "rayon") (r "~0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)) (d (n "tiny_http") (r "~0.5") (d #t) (k 0)))) (h "09r5m5ghj97cwmfc9x93f1rl1rg4srsab3a49n1nzms4pqw9yrv0")))

(define-public crate-abrute-0.1.10 (c (n "abrute") (v "0.1.10") (d (list (d (n "array_tool") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "~4.3.0") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "digits") (r "~1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "num_cpus") (r "~1.7") (d #t) (k 0)) (d (n "rayon") (r "~0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "tempdir") (r "~0.3") (d #t) (k 0)) (d (n "tiny_http") (r "~0.8") (d #t) (k 0)))) (h "03w7v65ila98z68y834g1ja8ks0vpinprbkc130aiya7r2fz28q1")))

