(define-module (crates-io ab il abilists) #:use-module (crates-io))

(define-public crate-abilists-0.1.0 (c (n "abilists") (v "0.1.0") (d (list (d (n "arcstr") (r "^1.1.3") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "ascii") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "eio") (r "^0.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "format") (r "^0.2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1zgdyzpsggp197l480ad5k2jkpzlpbryffn3m2qcdd4i0884j1sb")))

