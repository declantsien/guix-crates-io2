(define-module (crates-io ab il ability) #:use-module (crates-io))

(define-public crate-ability-0.1.0 (c (n "ability") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0fy8058k9964218lqd396pmg8j310l823pl9wiavi38a8j6kpm5m")))

