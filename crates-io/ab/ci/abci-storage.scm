(define-module (crates-io ab ci abci-storage) #:use-module (crates-io))

(define-public crate-abci-storage-0.0.3 (c (n "abci-storage") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "kvdb") (r "^0.7.0") (d #t) (k 0)) (d (n "kvdb-rocksdb") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "mockall") (r "^0.8.1") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)))) (h "03nlymlw4qcyv8r3kdq4wllhn0k77kgxa6cj138x08kvq4fy8szf")))

