(define-module (crates-io ab ci abci2) #:use-module (crates-io))

(define-public crate-abci2-0.1.0 (c (n "abci2") (v "0.1.0") (d (list (d (n "integer-encoding") (r "^1.0.7") (d #t) (k 0)) (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.8.1") (o #t) (d #t) (k 0)))) (h "1g1cxn98s3s4i97q2a2jasb6951w3hsc12a5lsflr3qs733566l5") (f (quote (("codegen" "protobuf-codegen-pure"))))))

(define-public crate-abci2-0.1.1 (c (n "abci2") (v "0.1.1") (d (list (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.8.1") (o #t) (d #t) (k 0)))) (h "0vffpj2f13rsb9qpvnlpr0vr69xqbi92ca9bpq18qlqr555p98hb") (f (quote (("codegen" "protobuf-codegen-pure"))))))

(define-public crate-abci2-0.1.2 (c (n "abci2") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "protobuf") (r "= 2.10.1") (d #t) (k 0)))) (h "0sz01v1y443jzhq2mqp85fywl6fq28a5zxy3k28sx03nz95s8khk")))

(define-public crate-abci2-0.1.3 (c (n "abci2") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "tendermint-proto") (r "^0.23.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0653lgmqx9d3w3m6smcyhhgmm71qsva6h6i55abbl6iwpcfad4yz")))

