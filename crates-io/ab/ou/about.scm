(define-module (crates-io ab ou about) #:use-module (crates-io))

(define-public crate-about-0.1.0 (c (n "about") (v "0.1.0") (h "0rbqgy3zgk1rbc7nxcpqxcsbf6g8360pgjg9svz8girccb61r4ac") (y #t)))

(define-public crate-about-0.1.1 (c (n "about") (v "0.1.1") (h "0w0jvw5bxnfaxavp17xycvxb7zhdkbgm6fl317pa6y9ssk27yh56") (y #t)))

