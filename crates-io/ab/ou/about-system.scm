(define-module (crates-io ab ou about-system) #:use-module (crates-io))

(define-public crate-about-system-0.1.0 (c (n "about-system") (v "0.1.0") (d (list (d (n "cpuid") (r "^0.1.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "rs-release") (r "^0.1.7") (d #t) (t "cfg(unix)") (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.3.17") (d #t) (t "cfg(unix)") (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "win32-error") (r "^0.9.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "13r87p7l0ddpkivqv7vy67p6r0v3fnkzlc9kzx77dryswz0pyzlb")))

