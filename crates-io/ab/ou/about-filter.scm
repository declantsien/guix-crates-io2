(define-module (crates-io ab ou about-filter) #:use-module (crates-io))

(define-public crate-about-filter-0.1.0 (c (n "about-filter") (v "0.1.0") (d (list (d (n "comrak") (r "^0.11.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "rst_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "rst_renderer") (r "^0.4.0") (d #t) (k 0)))) (h "0ady9rhbdbkxy2h72s9w88g9yq3hpxi72pvpx86b814dmi6gj33f")))

(define-public crate-about-filter-0.1.1 (c (n "about-filter") (v "0.1.1") (d (list (d (n "comrak") (r "^0.13.0") (k 0)) (d (n "html-escape") (r "^0.2.11") (d #t) (k 0)) (d (n "rst_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "rst_renderer") (r "^0.4.0") (d #t) (k 0)))) (h "1b4az2kw25wpvy1jlq7kwv10c0nvjj94rgwcl4r8xvflw1vg5jgk")))

