(define-module (crates-io ab oo abook) #:use-module (crates-io))

(define-public crate-abook-0.1.0 (c (n "abook") (v "0.1.0") (h "0fyqdvkdrq58i3i7w040kmp6w1mx8mb0q076j0f2yxjsywm0mzn0")))

(define-public crate-abook-0.1.1 (c (n "abook") (v "0.1.1") (h "1i8fvb8q7l7ws4543m2kvarv6cfij83lqhgjs77xyyh84b5h8xij")))

