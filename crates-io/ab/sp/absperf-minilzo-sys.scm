(define-module (crates-io ab sp absperf-minilzo-sys) #:use-module (crates-io))

(define-public crate-absperf-minilzo-sys-0.1.0 (c (n "absperf-minilzo-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ry9gab5nma8sgz0n52bfb2z6plqz0n82bq5c24c46d9m8lyil5m")))

(define-public crate-absperf-minilzo-sys-2.10.0 (c (n "absperf-minilzo-sys") (v "2.10.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1bnsc1ksiszv3kr3ahm90vzjv4irj5qpqddpzbp92k2in4xppn2s")))

