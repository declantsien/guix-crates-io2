(define-module (crates-io ab ib abibool) #:use-module (crates-io))

(define-public crate-abibool-0.5.0 (c (n "abibool") (v "0.5.0") (h "1kdaj3pmgn1i0vbq9cmsq6h81a2b390gavj5mzmqrhwzskixlcj1")))

(define-public crate-abibool-0.5.1 (c (n "abibool") (v "0.5.1") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)))) (h "1ixhili7bllj17l3was25qfl7dbi9rspx7licz6vy9w59w1f33sh")))

(define-public crate-abibool-0.5.2 (c (n "abibool") (v "0.5.2") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)))) (h "1k4hhdycpclfmpnhhzl1fgqbnbnfdyb5arpb33zmif73p1kxwi2g")))

(define-public crate-abibool-0.5.3 (c (n "abibool") (v "0.5.3") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)))) (h "0pifmpzwx9n7x404gr9x0vyy21ajaivm22116kd0y0b6w1chzlqb")))

