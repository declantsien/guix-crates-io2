(define-module (crates-io ab om abomonation_derive) #:use-module (crates-io))

(define-public crate-abomonation_derive-0.1.0 (c (n "abomonation_derive") (v "0.1.0") (d (list (d (n "abomonation") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.9") (d #t) (k 0)) (d (n "synstructure") (r "^0.2") (d #t) (k 0)))) (h "1mqsawpxw8rb42s5fpcyvl98c4fmq8qz4ndfwxlxbzv5bcgq4ql5")))

(define-public crate-abomonation_derive-0.1.2 (c (n "abomonation_derive") (v "0.1.2") (d (list (d (n "abomonation") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)) (d (n "synstructure") (r "^0.4") (d #t) (k 0)))) (h "08lmb5s5ska7fwgnqfwkvyw16lz8as756404fhp75zbqd5wydz83")))

(define-public crate-abomonation_derive-0.2.0 (c (n "abomonation_derive") (v "0.2.0") (d (list (d (n "abomonation") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.5") (d #t) (k 0)))) (h "0csffn1mqhjm4242fqf4midwzg1c3cs4r6sgsniw4ss4p000x4ph")))

(define-public crate-abomonation_derive-0.2.1 (c (n "abomonation_derive") (v "0.2.1") (d (list (d (n "abomonation") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.5") (d #t) (k 0)))) (h "1s424l30gkpgk49yqzj0wha9d15vibv77yyafrm65zy7g1m0gbpq")))

(define-public crate-abomonation_derive-0.2.2 (c (n "abomonation_derive") (v "0.2.2") (d (list (d (n "abomonation") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.5") (d #t) (k 0)))) (h "0w5mx3vfg1h2sfl848f262afyrkq048692mq0gnfklhzpg6xf0m0")))

(define-public crate-abomonation_derive-0.2.3 (c (n "abomonation_derive") (v "0.2.3") (d (list (d (n "abomonation") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6") (d #t) (k 0)))) (h "1lr7yqzxfk39w1qcqn9dxjl646c0sxavv9k9zqw7y27vzpi9vmhl")))

(define-public crate-abomonation_derive-0.3.0 (c (n "abomonation_derive") (v "0.3.0") (d (list (d (n "abomonation") (r "^0.5") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6") (d #t) (k 0)))) (h "0g373spwjw311904p45cijv8v372hd96pi9rz27cv1qbvf4iifz0")))

(define-public crate-abomonation_derive-0.4.0 (c (n "abomonation_derive") (v "0.4.0") (d (list (d (n "abomonation") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "1xs900c4dagc8rxgnx192433i0j6i5c21wgfgjmbz6961y1fyiq8")))

(define-public crate-abomonation_derive-0.5.0 (c (n "abomonation_derive") (v "0.5.0") (d (list (d (n "abomonation") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1yhydqgq4dipjv3v1qbbcip5jb57zm9p2yyrc968cspmd822l3p5")))

