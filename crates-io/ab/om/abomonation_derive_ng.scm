(define-module (crates-io ab om abomonation_derive_ng) #:use-module (crates-io))

(define-public crate-abomonation_derive_ng-0.1.0 (c (n "abomonation_derive_ng") (v "0.1.0") (d (list (d (n "abomonation") (r "^0.7.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (d #t) (k 0)))) (h "1w6902lmrm2c0629c8ysb2idpz13dv50pbz9i0f42409dq9mch63") (r "1.63")))

