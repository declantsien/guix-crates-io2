(define-module (crates-io ab om abomonation) #:use-module (crates-io))

(define-public crate-abomonation-0.1.0 (c (n "abomonation") (v "0.1.0") (h "16afpsa28m2kbcgcjmvrwq8yyfgdqrza9df374jnl2627xydq1dz")))

(define-public crate-abomonation-0.2.0 (c (n "abomonation") (v "0.2.0") (h "13yij06rrda3ca4dpppiis9yywwpcydjdbpsz6wjwrckp39a2a1y")))

(define-public crate-abomonation-0.2.1 (c (n "abomonation") (v "0.2.1") (h "1k54s9458c2w7h42xjp459lm9znqpi6zv85vwqzklzi2b7vnkv3q")))

(define-public crate-abomonation-0.2.2 (c (n "abomonation") (v "0.2.2") (h "03qgjdwk2661aq54vpcg79sqf11dgf2jhqj5z62gf7g19l3xyjjv")))

(define-public crate-abomonation-0.2.3 (c (n "abomonation") (v "0.2.3") (h "1ldwgr4i3pv0h53ic8z7mw7nvsggk4kjsfhsvmvjknhqfw0xj3wg")))

(define-public crate-abomonation-0.3.0 (c (n "abomonation") (v "0.3.0") (h "07qn7w6qvsr86w6xcx47lv2li5pjrpsvsai984wslmz9csi3738p")))

(define-public crate-abomonation-0.3.1 (c (n "abomonation") (v "0.3.1") (h "0afqaqisd3m4z4s4lz867njfbx687y0v8czwjbbgzjkih2pdfdrs")))

(define-public crate-abomonation-0.3.2 (c (n "abomonation") (v "0.3.2") (h "161g4cbw82qc1nkah7hi4lakfpnpxfffk7427gkd5gywh8873rwb")))

(define-public crate-abomonation-0.3.3 (c (n "abomonation") (v "0.3.3") (h "0n2yja7673zlmx128jsz7p3wjsd8sd0iy02jchib5ya8shnvy3y5")))

(define-public crate-abomonation-0.4.0 (c (n "abomonation") (v "0.4.0") (d (list (d (n "recycler") (r "*") (d #t) (k 2)))) (h "18ywl5kma0yd2b15b7497fqznhr3z9vi76jyn2vy63mr7d9dv3xk")))

(define-public crate-abomonation-0.4.1 (c (n "abomonation") (v "0.4.1") (d (list (d (n "recycler") (r "*") (d #t) (k 2)))) (h "1caxkl7r9j0xqn1bvrdfpcjal5w5dk3zi340nkhbgy1s607w8f90")))

(define-public crate-abomonation-0.4.2 (c (n "abomonation") (v "0.4.2") (d (list (d (n "recycler") (r "*") (d #t) (k 2)))) (h "0jm7fgzjdfz3ghh7y1jqbx5q1aisazpwb0vnkbnkqgy5jhh9xfb4")))

(define-public crate-abomonation-0.4.3 (c (n "abomonation") (v "0.4.3") (d (list (d (n "recycler") (r "^0.1.4") (d #t) (k 2)))) (h "1hrlzx9jcrn05ln20ivy1xdwjc90mp2mdabvf0ghi75l6b0y4ajv")))

(define-public crate-abomonation-0.4.4 (c (n "abomonation") (v "0.4.4") (d (list (d (n "recycler") (r "^0.1.4") (d #t) (k 2)))) (h "0z5z6s4p5ll68pf46lym91vmq245fsdbbdf11aplq4kwr23chxib")))

(define-public crate-abomonation-0.4.5 (c (n "abomonation") (v "0.4.5") (d (list (d (n "recycler") (r "^0.1.4") (d #t) (k 2)))) (h "02g405la85ixyd91qb3qzrf946hcla81pllkvkvn1idqjwxn2cb3")))

(define-public crate-abomonation-0.4.6 (c (n "abomonation") (v "0.4.6") (d (list (d (n "recycler") (r "^0.1.4") (d #t) (k 2)))) (h "0mgghcpwmzpx47wmm5c5yvliazpjm60cmqz77ng53ds273r5yxi6")))

(define-public crate-abomonation-0.5.0 (c (n "abomonation") (v "0.5.0") (d (list (d (n "recycler") (r "^0.1.4") (d #t) (k 2)))) (h "19h4s5ai8pbaap7n8pcd6yinqp22hx29ls9d2gdwsjka3m9xy6gv")))

(define-public crate-abomonation-0.7.0 (c (n "abomonation") (v "0.7.0") (d (list (d (n "recycler") (r "^0.1.4") (d #t) (k 2)))) (h "00c6mmwb6jlmw5his875li988rrz9vr2x08mhn9lg1rx498jidzn")))

(define-public crate-abomonation-0.7.1 (c (n "abomonation") (v "0.7.1") (d (list (d (n "recycler") (r "^0.1.4") (d #t) (k 2)))) (h "0577h48ji1dn31xmrz1psg86h39hr1ib03az3pnsps43gvlw4crr")))

(define-public crate-abomonation-0.7.2 (c (n "abomonation") (v "0.7.2") (d (list (d (n "recycler") (r "^0.1.4") (d #t) (k 2)))) (h "09v1byr09icxw8aiccs3izmd1pgijrmv56jnky48i9c7lqh67fvk")))

(define-public crate-abomonation-0.7.3 (c (n "abomonation") (v "0.7.3") (d (list (d (n "recycler") (r "^0.1.4") (d #t) (k 2)))) (h "1cjg3hjf028n447pdj7zcdgrkngx30as8ndxlxx947wvr49jkrsn")))

