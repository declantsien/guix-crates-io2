(define-module (crates-io ab in abin) #:use-module (crates-io))

(define-public crate-abin-0.1.0 (c (n "abin") (v "0.1.0") (d (list (d (n "rayon") (r ">=1.4") (d #t) (k 2)) (d (n "serde") (r ">=1.0.96") (o #t) (k 0)) (d (n "serde") (r ">=1.0.96") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_cbor") (r ">=0.11") (d #t) (k 2)) (d (n "smallvec") (r ">=1.4") (d #t) (k 0)) (d (n "stats_alloc") (r ">=0.1") (d #t) (k 2)))) (h "07f2ilc475wycwm0jvngk35jnrz771lxc50yp47py3dyk2ddmz20")))

(define-public crate-abin-0.1.1 (c (n "abin") (v "0.1.1") (d (list (d (n "rayon") (r ">=1.4") (d #t) (k 2)) (d (n "serde") (r ">=1.0.96") (o #t) (k 0)) (d (n "serde") (r ">=1.0.96") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_cbor") (r ">=0.11") (d #t) (k 2)) (d (n "smallvec") (r ">=1.4") (d #t) (k 0)) (d (n "stats_alloc") (r ">=0.1") (d #t) (k 2)))) (h "09bg0wbb93b2wvq9zpdzg6v8cbn29nbfj1saajki2zxqk8k7ng7a")))

(define-public crate-abin-0.1.5 (c (n "abin") (v "0.1.5") (d (list (d (n "rayon") (r ">=1.4") (d #t) (k 2)) (d (n "serde") (r ">=1.0.96") (o #t) (k 0)) (d (n "serde") (r ">=1.0.96") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_cbor") (r ">=0.11") (d #t) (k 2)) (d (n "smallvec") (r ">=1.4") (d #t) (k 0)) (d (n "stats_alloc") (r ">=0.1") (d #t) (k 2)))) (h "1jm4sy4g4j6kwbvaqj8q34g8rbl6hfgwxf1qgsv05nfxa0chq3ww")))

(define-public crate-abin-0.1.6 (c (n "abin") (v "0.1.6") (d (list (d (n "rayon") (r ">=1.4") (d #t) (k 2)) (d (n "serde") (r ">=1.0.96") (o #t) (k 0)) (d (n "serde") (r ">=1.0.96") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_cbor") (r ">=0.11") (d #t) (k 2)) (d (n "smallvec") (r ">=1.4") (d #t) (k 0)) (d (n "stats_alloc") (r ">=0.1") (d #t) (k 2)))) (h "01x63yi8qnvwyqy0qj4w0sg7m75j78q30dhjaxla4d301gryjhbc")))

