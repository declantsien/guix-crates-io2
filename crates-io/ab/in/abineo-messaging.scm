(define-module (crates-io ab in abineo-messaging) #:use-module (crates-io))

(define-public crate-abineo-messaging-1.0.0 (c (n "abineo-messaging") (v "1.0.0") (d (list (d (n "email_address") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x8f8m236g5lz3jnlqlg48wyjqmkmk99nx8llcyy872nylm6dvkh")))

