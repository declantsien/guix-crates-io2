(define-module (crates-io ab io abio) #:use-module (crates-io))

(define-public crate-abio-0.3.0 (c (n "abio") (v "0.3.0") (d (list (d (n "abio_derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (d #t) (k 2)) (d (n "libloading") (r "^0.8.0") (d #t) (k 2)))) (h "02hcfv4hs56p6d318cbm1n3p4wgzp4r88y709hb4121jcs394hpw") (f (quote (("std") ("derive" "abio_derive") ("default" "derive"))))))

