(define-module (crates-io ab io abio_derive) #:use-module (crates-io))

(define-public crate-abio_derive-0.1.1 (c (n "abio_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (f (quote ("nightly" "span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "11hp5xrfc1knc35vzcwm9jy7yblqkgnr4z5qahwlgbcr7nf4agbl")))

