(define-module (crates-io ab e_ abe_policy) #:use-module (crates-io))

(define-public crate-abe_policy-0.1.0 (c (n "abe_policy") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lb4dzd7bam999l5dzldymypicxxy4x6k5im6j7k3grxhhbzjgha")))

(define-public crate-abe_policy-0.1.1 (c (n "abe_policy") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m8kqgriqvm716qdapxhs21zmgk3rs90cw3s8zg14zvg5zbnpifd")))

(define-public crate-abe_policy-1.0.0 (c (n "abe_policy") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pdyi5r0l5s5cmsm8g8mhrsc0nydr0nhx14zmkv8psbvbwfklv7b")))

(define-public crate-abe_policy-1.0.1 (c (n "abe_policy") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1plzwspjhaqclznnmsvhx05y95h68fhjki8psqm2iqhmab0g8zqc")))

(define-public crate-abe_policy-2.0.0 (c (n "abe_policy") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d5nipcvalwjy791xy4wlhk1v0mvd4zbaik00spwx2k6s0g1afvp")))

(define-public crate-abe_policy-3.0.0 (c (n "abe_policy") (v "3.0.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0j4cxx969sa0rryzxmha2zwch3681lm1azs0mzjsw86cwykgxadn") (f (quote (("wasm_bindgen" "interface") ("interface") ("ffi" "interface"))))))

(define-public crate-abe_policy-3.0.1 (c (n "abe_policy") (v "3.0.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0f3yzy8sdaz8mbvixr0r08xfspnvjglq2wf3fh0my0b6p81ba65z") (f (quote (("wasm_bindgen" "interface") ("interface") ("ffi" "interface"))))))

(define-public crate-abe_policy-3.0.2 (c (n "abe_policy") (v "3.0.2") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "12v419wb8jaq7axn1937wxam7n4indh3r7jlw9yc1vc265c2kwkf") (f (quote (("wasm_bindgen" "interface") ("interface") ("ffi" "interface"))))))

(define-public crate-abe_policy-3.0.3 (c (n "abe_policy") (v "3.0.3") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "19z13lwi5zvgdfg2y8ma57d433z047r43jfbff2m6d7l2g0070qz") (f (quote (("wasm_bindgen" "interface") ("interface") ("ffi" "interface")))) (y #t)))

(define-public crate-abe_policy-3.0.4 (c (n "abe_policy") (v "3.0.4") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0l29nr130vsbv894ji083wi5pa1j1xavkzwjkm8234dral1wl321") (f (quote (("wasm_bindgen" "interface") ("interface") ("ffi" "interface")))) (y #t)))

(define-public crate-abe_policy-3.0.5 (c (n "abe_policy") (v "3.0.5") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0rfwymmnvda784vk7f43cr8cn2wm9j8zmsgbbbm8m3gwx5rzpdhg") (f (quote (("wasm_bindgen" "interface") ("interface") ("ffi" "interface"))))))

