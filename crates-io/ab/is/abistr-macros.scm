(define-module (crates-io ab is abistr-macros) #:use-module (crates-io))

(define-public crate-abistr-macros-0.1.0 (c (n "abistr-macros") (v "0.1.0") (h "0m1y59kpsxqwmdxw00bjcy6hyagci5lry30j44vplin19mclkd3q")))

(define-public crate-abistr-macros-0.2.0-rc1 (c (n "abistr-macros") (v "0.2.0-rc1") (h "181p9ml51md1wqqwmgrs5ng76wl1x1w42knijfivxi6sd60g1c4g")))

(define-public crate-abistr-macros-0.2.0-rc2 (c (n "abistr-macros") (v "0.2.0-rc2") (h "14fvfcq5mgfj1nvwx11gyqs7snwvj7mm83s67654m90gpa40vwhk")))

(define-public crate-abistr-macros-0.2.0-rc3 (c (n "abistr-macros") (v "0.2.0-rc3") (h "05cfvzhh7ds7gd196b77bqxjvp2fv227kmflifr5alls6qprbswa")))

