(define-module (crates-io ab is abistr) #:use-module (crates-io))

(define-public crate-abistr-0.1.0 (c (n "abistr") (v "0.1.0") (d (list (d (n "abistr-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)))) (h "02sng8llijvm5wbj169y54kmzs1a1npgifsdl45gcffs0jx80iz1")))

(define-public crate-abistr-0.1.1 (c (n "abistr") (v "0.1.1") (d (list (d (n "abistr-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)))) (h "08qjlvb4qn85vm6l6hw1vrbmp86qn0km2s42b4clj1sm3xkjfp99")))

(define-public crate-abistr-0.2.0-rc1 (c (n "abistr") (v "0.2.0-rc1") (d (list (d (n "abistr-macros") (r "^0.2.0-rc1") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "widestring-0-4") (r "^0.4") (o #t) (d #t) (k 0) (p "widestring")))) (h "1baw3dc7aqlpq7bngz4xcjz4xnr511iwvrqzbfqailm7sp7czw5z")))

(define-public crate-abistr-0.2.0-rc2 (c (n "abistr") (v "0.2.0-rc2") (d (list (d (n "abistr-macros") (r "^0.2.0-rc2") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "widestring") (r "^1") (o #t) (d #t) (k 0) (p "widestring")))) (h "0kgxq6yi1dp0nf59s911fqa77asl535agax9700vb59480b2fr2q") (f (quote (("std") ("default" "std"))))))

(define-public crate-abistr-0.2.0-rc3 (c (n "abistr") (v "0.2.0-rc3") (d (list (d (n "abistr-macros") (r "^0.2.0-rc3") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "widestring") (r "^1") (o #t) (d #t) (k 0) (p "widestring")))) (h "0llza5cd7cacnb5p8c3pz2nk6w3zz6flp5xcyj14nmnj3aa77pp7") (f (quote (("std") ("default" "std"))))))

