(define-module (crates-io ab -r ab-radix-trie) #:use-module (crates-io))

(define-public crate-ab-radix-trie-0.1.0 (c (n "ab-radix-trie") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "116j45dcic33a332r97d74algyj487r8xpnri9gn37abfgyc15i6") (f (quote (("tracing"))))))

(define-public crate-ab-radix-trie-0.2.0 (c (n "ab-radix-trie") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1ncgdd1mnn9gxlib75r1bvdlq7x9hg3mynqfybbgcl3i2m86px10") (f (quote (("tracing"))))))

(define-public crate-ab-radix-trie-0.2.1 (c (n "ab-radix-trie") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0rk8fiv3f1zly59lx617jxh0mg5r43k8kjhb51wkb5aks6ks15hr") (f (quote (("tracing"))))))

