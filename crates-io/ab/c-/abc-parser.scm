(define-module (crates-io ab c- abc-parser) #:use-module (crates-io))

(define-public crate-abc-parser-0.1.0 (c (n "abc-parser") (v "0.1.0") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "0fz42scby7bkgxb67zg7qx1da8ai33gmyl7844zsks5dypwaka2d") (y #t)))

(define-public crate-abc-parser-0.1.1 (c (n "abc-parser") (v "0.1.1") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "1vsj7dindjgdgip937plgf5cihsdvhwjd8j6r887jv14nai4w6n0") (y #t)))

(define-public crate-abc-parser-0.1.2 (c (n "abc-parser") (v "0.1.2") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "1d28n9wkmqfkwixqvhd7i181qkapm95742jv5ygvxrccyfiq8xx8")))

(define-public crate-abc-parser-0.1.3 (c (n "abc-parser") (v "0.1.3") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "00w0977kdg2yjl6mhnza50hxdxg4aqrd4rhahwp75a0mh2wzr4pi")))

(define-public crate-abc-parser-0.1.4 (c (n "abc-parser") (v "0.1.4") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "07dfb0hadhsbfq89xaz2ph42igs09hvrj36wiz6rsddciaqm3a1c")))

(define-public crate-abc-parser-0.1.5 (c (n "abc-parser") (v "0.1.5") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "1iv3k39jgim2dpyhkdh19db186pkh4kczqbbphapq44b70g7qcdf")))

(define-public crate-abc-parser-0.2.0 (c (n "abc-parser") (v "0.2.0") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "10xwkvzgxwwjnczlb7k15l3cpl8jng4pfky693dx1gl20fiikmr1")))

(define-public crate-abc-parser-0.3.0 (c (n "abc-parser") (v "0.3.0") (d (list (d (n "peg") (r "^0.7.0") (d #t) (k 0)))) (h "0h2gcncb8xj5jjc46igh79gppcgjlsz6rs5ln4wgwpgcr4ih9rk5")))

