(define-module (crates-io ab c- abc-ecs) #:use-module (crates-io))

(define-public crate-ABC-ECS-0.1.0 (c (n "ABC-ECS") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1w5ifpydr1ll4w1fnmvx2knhpalxk530i2p36ibh1w0kr0pvjafv") (y #t)))

(define-public crate-ABC-ECS-0.1.1 (c (n "ABC-ECS") (v "0.1.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1f7ilxqd5hgvk2cmvlyjz6iqxzyygcc9g1h013bri6imhspb2p7y") (y #t)))

(define-public crate-ABC-ECS-0.1.2 (c (n "ABC-ECS") (v "0.1.2") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1kwba0wgafl1kf1mmy3fp5v1x93wich9y2vydl6isxfxkvq3wyg0") (y #t)))

(define-public crate-ABC-ECS-0.1.3 (c (n "ABC-ECS") (v "0.1.3") (d (list (d (n "anymap") (r "^1.0.0-beta.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "0flx80vq528rnnhxly3wr4k7gqq9fkd6f1hm2wsjinkp0vhbry30")))

(define-public crate-ABC-ECS-0.1.4 (c (n "ABC-ECS") (v "0.1.4") (d (list (d (n "anymap") (r "^1.0.0-beta.1") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "04dr2xfkldvxzhwd0pqnb84f7v44k6mrr1mdrmwv8dygdb2yfkb3")))

(define-public crate-ABC-ECS-0.1.5 (c (n "ABC-ECS") (v "0.1.5") (d (list (d (n "anymap") (r "^1.0.0-beta.1") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "17nz15dyrrg4cfdfdz10hf8nbpkdslx798pbnxlmmjr7w7il022h")))

(define-public crate-ABC-ECS-0.2.0 (c (n "ABC-ECS") (v "0.2.0") (d (list (d (n "anymap") (r "^1.0.0-beta.1") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1jrz2asc7f8k3fdcjm8iaq1fh1svxkahlxm1yk1wcb3c4wxhfrgb")))

