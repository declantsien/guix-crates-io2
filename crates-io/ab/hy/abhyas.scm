(define-module (crates-io ab hy abhyas) #:use-module (crates-io))

(define-public crate-abhyas-1.0.0 (c (n "abhyas") (v "1.0.0") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "0nlcr2sd428javzr21d87g5m8zl3d0q27gqciq4g7h707942rsv2")))

(define-public crate-abhyas-1.0.1 (c (n "abhyas") (v "1.0.1") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "1s1bsw510jnki65mkcrqrwqkhcypax53jynqqrmzcla6kf59j08n")))

(define-public crate-abhyas-1.1.0 (c (n "abhyas") (v "1.1.0") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "1ixxiimkjaljdr57pw78zddr90v05m6j0qw9m4jz84p6r8a6cr6i")))

(define-public crate-abhyas-1.2.0 (c (n "abhyas") (v "1.2.0") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "0qz28hxg1wf5ijpwg7byj3miq1963sj0vr9gqlbsmq5xgx5fmr7x")))

(define-public crate-abhyas-1.4.0 (c (n "abhyas") (v "1.4.0") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "06hzg6b7fsskjsvvzpsp3kxrxkz97p0ww64calv53l239383gqkl")))

(define-public crate-abhyas-1.5.0 (c (n "abhyas") (v "1.5.0") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.4.0") (d #t) (k 0)))) (h "0qw6zkalygmmndc2z68r9bwk38idky3s0hs7pwa92a5v10qjlzpw")))

