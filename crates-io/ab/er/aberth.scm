(define-module (crates-io ab er aberth) #:use-module (crates-io))

(define-public crate-aberth-0.0.0 (c (n "aberth") (v "0.0.0") (h "0hx3bvcp0d1l81izg4pls0myd8458ddkxjy40638rqq4sh6l2l6s")))

(define-public crate-aberth-0.0.1 (c (n "aberth") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ji8i2njy0vh41fk47rd71m5y1gzkikmw4dakq64mvw46dmvdf0p") (y #t)))

(define-public crate-aberth-0.0.2 (c (n "aberth") (v "0.0.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12ynkh5mv6fhzycipz3pqq78wmv40wfia8h6qmhjlq6yd7nd2p7j") (y #t)))

(define-public crate-aberth-0.0.3 (c (n "aberth") (v "0.0.3") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1za0wwvyzjcg54qsf58y7m3gxiwpx080mlbqh0x80kkpkqj99fab")))

(define-public crate-aberth-0.0.4 (c (n "aberth") (v "0.0.4") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "num-complex") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1y7nd7k0s2a4mbcnx1wz2a3m35w96zramxqmrxxif5f0gkzwl6y2") (f (quote (("std" "num-traits/default" "num-complex/default") ("libm" "num-traits/libm" "num-complex/libm") ("default" "std"))))))

(define-public crate-aberth-0.4.1 (c (n "aberth") (v "0.4.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "num-complex") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1yr6hr0lmmld1634zvgk3xwr3fgd4dyxrva1ynl7k0fqzgx87dww") (f (quote (("std" "num-traits/default" "num-complex/default") ("libm" "num-traits/libm" "num-complex/libm") ("default" "std"))))))

