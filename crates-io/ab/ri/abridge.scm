(define-module (crates-io ab ri abridge) #:use-module (crates-io))

(define-public crate-abridge-1.0.0 (c (n "abridge") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "substring") (r "^1.4.0") (d #t) (k 0)))) (h "0dbfqnqf929d7lm5iv9sciyyzxi2drlfb8w43qr3hv6i5zmp93j1") (y #t)))

(define-public crate-abridge-1.0.1 (c (n "abridge") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.0") (d #t) (k 0)))) (h "0qxisj4qiwv42l62v0605n2b991jc5ry255q32papdnmniqrh3i0") (y #t)))

(define-public crate-abridge-1.1.0 (c (n "abridge") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.0") (d #t) (k 0)))) (h "0yci12spw95rb9q3cj92vg5g0h604x9dg5wjb5rl04b5xvl6rfhm") (y #t)))

(define-public crate-abridge-1.1.1 (c (n "abridge") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.0") (d #t) (k 0)))) (h "1zcfkl5kd3icgp40yyc5zqkn7jwclbck09j93j2myq39f9z5xf4a") (y #t)))

(define-public crate-abridge-1.1.2 (c (n "abridge") (v "1.1.2") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.0") (d #t) (k 0)))) (h "0mzbrbirhk42k8lzwp1wmcs56lvf3i5cx96npvhyqrzhlzaf4261") (y #t)))

(define-public crate-abridge-1.1.3 (c (n "abridge") (v "1.1.3") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.0") (d #t) (k 0)))) (h "06ych4xf31lm7bk0b9yik8qhb8bppnv4w35d5azifj3sf7xdrymx") (y #t)))

(define-public crate-abridge-1.1.4 (c (n "abridge") (v "1.1.4") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.0") (d #t) (k 0)))) (h "1kr949as5zz722ysh5lvflhvfl02m6zgpmlrkwsr37sd0zg5wicn") (y #t)))

(define-public crate-abridge-1.1.5 (c (n "abridge") (v "1.1.5") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.0") (d #t) (k 0)))) (h "15lix2pj9x1k5p00scv4d9g6i7ai817dxsyiz6nw8l5a07rllq00") (y #t)))

(define-public crate-abridge-1.1.6 (c (n "abridge") (v "1.1.6") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "substring") (r "^1.4.0") (d #t) (k 0)))) (h "1wfp6rm0v19wfpw565fvw0j61pbjkrzz4vqf4ar2bmwbz5q5b8gw")))

