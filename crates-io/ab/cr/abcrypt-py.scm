(define-module (crates-io ab cr abcrypt-py) #:use-module (crates-io))

(define-public crate-abcrypt-py-0.1.0 (c (n "abcrypt-py") (v "0.1.0") (d (list (d (n "abcrypt") (r "^0.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.3") (d #t) (k 0)))) (h "1asncg2ii8ayjnxp72hhy7c8m8y8cm7ky9125wkxms6w5zg50sry") (r "1.74.0")))

(define-public crate-abcrypt-py-0.1.1 (c (n "abcrypt-py") (v "0.1.1") (d (list (d (n "abcrypt") (r "^0.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.3") (d #t) (k 0)))) (h "1n2jw0xqkkhk1gls8yp8dxv7jxhixa0whl61b8343aydfgxhmaqs") (r "1.74.0")))

(define-public crate-abcrypt-py-0.1.2 (c (n "abcrypt-py") (v "0.1.2") (d (list (d (n "abcrypt") (r "^0.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.3") (d #t) (k 0)))) (h "0zg0ri80cgl60bb0bwa81z8vc4grlw93m306ssbkah4mpgsnbc30") (r "1.74.0")))

(define-public crate-abcrypt-py-0.1.3 (c (n "abcrypt-py") (v "0.1.3") (d (list (d (n "abcrypt") (r "^0.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.3") (d #t) (k 0)))) (h "1k9w1mhvpvb3nrq8q1znd8k93p4j0w3f8mnigqdvx4j5bdprmx1c") (r "1.74.0")))

(define-public crate-abcrypt-py-0.1.4 (c (n "abcrypt-py") (v "0.1.4") (d (list (d (n "abcrypt") (r "^0.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.3") (d #t) (k 0)))) (h "1z9laif25mwg2wrryff1b0sw7pcgq25v14wvvyzbslfdmzxzjgr2") (r "1.74.0")))

