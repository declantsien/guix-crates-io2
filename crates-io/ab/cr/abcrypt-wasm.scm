(define-module (crates-io ab cr abcrypt-wasm) #:use-module (crates-io))

(define-public crate-abcrypt-wasm-0.1.0 (c (n "abcrypt-wasm") (v "0.1.0") (d (list (d (n "abcrypt") (r "^0.2.10") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.40") (d #t) (k 2)))) (h "0q05qk8fi322jbwp5887mkl8fk8c1i7hs9ks431cgx4dwa2chr97") (r "1.70.0")))

(define-public crate-abcrypt-wasm-0.1.1 (c (n "abcrypt-wasm") (v "0.1.1") (d (list (d (n "abcrypt") (r "^0.2.10") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.40") (d #t) (k 2)))) (h "1s6pmviwcd90xnasn1fhy79nlnssm4hj09hhgjj0ngp8jyns01f2") (r "1.70.0")))

(define-public crate-abcrypt-wasm-0.2.0 (c (n "abcrypt-wasm") (v "0.2.0") (d (list (d (n "abcrypt") (r "^0.2.10") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.40") (d #t) (k 2)))) (h "0z11sq92cd83y5g6qi5nw9rvfqryxg7bg98xbnf7pc6d2l7jbbkb") (r "1.70.0")))

(define-public crate-abcrypt-wasm-0.3.0 (c (n "abcrypt-wasm") (v "0.3.0") (d (list (d (n "abcrypt") (r "^0.3.2") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.41") (d #t) (k 2)))) (h "0mxc9dqzz24a7sc9r70h89lyb124b0j73bcayg2v24mln7lwg48f") (r "1.74.0")))

(define-public crate-abcrypt-wasm-0.3.1 (c (n "abcrypt-wasm") (v "0.3.1") (d (list (d (n "abcrypt") (r "^0.3.2") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.41") (d #t) (k 2)))) (h "0285ncz8yxgw78234ff0fg3kij92i2imzdbjng7vgfp40ld56jdx") (r "1.74.0")))

(define-public crate-abcrypt-wasm-0.3.2 (c (n "abcrypt-wasm") (v "0.3.2") (d (list (d (n "abcrypt") (r "^0.3.2") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.41") (d #t) (k 2)))) (h "0l50khv2vlw2rvqgmjshr08cgpfwrc47bfg05lvh1ys5pz6ghgkz") (r "1.74.0")))

