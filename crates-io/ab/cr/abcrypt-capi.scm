(define-module (crates-io ab cr abcrypt-capi) #:use-module (crates-io))

(define-public crate-abcrypt-capi-0.1.0 (c (n "abcrypt-capi") (v "0.1.0") (d (list (d (n "abcrypt") (r "^0.2.3") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "17p7krg5j6xmf7l7pg0iwl93rwwspy5jfy04y7gik1byfk7mcay9") (y #t) (r "1.65.0")))

(define-public crate-abcrypt-capi-0.1.1 (c (n "abcrypt-capi") (v "0.1.1") (d (list (d (n "abcrypt") (r "^0.2.4") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "02g30jmvfzcvbyh590gp5wj091zzivb651q0piqa8z3n4q46xfd2") (y #t) (r "1.65.0")))

(define-public crate-abcrypt-capi-0.1.2 (c (n "abcrypt-capi") (v "0.1.2") (d (list (d (n "abcrypt") (r "^0.2.4") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "0z2mkyxvxnghrdwvc2x8xdpzrqg0dd15ych029gv1d6rziy9zbcv") (r "1.65.0")))

(define-public crate-abcrypt-capi-0.2.0 (c (n "abcrypt-capi") (v "0.2.0") (d (list (d (n "abcrypt") (r "^0.2.5") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "0yxnbhsbj2l19bg20bhi1mbq249kalxab589d1cc3axwsid2qh0i") (r "1.65.0")))

(define-public crate-abcrypt-capi-0.2.1 (c (n "abcrypt-capi") (v "0.2.1") (d (list (d (n "abcrypt") (r "^0.2.6") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "1acg842mml0dbkcjd8a318xlqflr69g42b0bf5rqll78d8nl6345") (r "1.65.0")))

(define-public crate-abcrypt-capi-0.2.2 (c (n "abcrypt-capi") (v "0.2.2") (d (list (d (n "abcrypt") (r "^0.2.7") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "1q0x1rlc47bvl37pvhwyggn801wl4yqmll4j2s3d22gb277mv8sw") (r "1.70.0")))

(define-public crate-abcrypt-capi-0.2.3 (c (n "abcrypt-capi") (v "0.2.3") (d (list (d (n "abcrypt") (r "^0.2.8") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "141flv7w0gh1dgpha4lajxsw3q5fsipy6sf3fjcl2pahqy7hin9p") (r "1.70.0")))

(define-public crate-abcrypt-capi-0.2.4 (c (n "abcrypt-capi") (v "0.2.4") (d (list (d (n "abcrypt") (r "^0.2.9") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "13w5jaxk73vwhzsalw2nwnlmg8469p2b50bjlg7j49kh7g481wnk") (r "1.70.0")))

(define-public crate-abcrypt-capi-0.2.5 (c (n "abcrypt-capi") (v "0.2.5") (d (list (d (n "abcrypt") (r "^0.2.10") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "0xvfdn929wshx9dvr60irk2y5ysp5x090zvrdf5ws7ha9525h02s") (r "1.70.0")))

(define-public crate-abcrypt-capi-0.2.6 (c (n "abcrypt-capi") (v "0.2.6") (d (list (d (n "abcrypt") (r "^0.2.10") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "0qz8aja1scvha8cw2gqcgx059ic72hifclr82qvghc2zaq3svc2w") (r "1.70.0")))

(define-public crate-abcrypt-capi-0.3.0 (c (n "abcrypt-capi") (v "0.3.0") (d (list (d (n "abcrypt") (r "^0.3.1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "13q4c2ri16xza1liw1hyrh1fpz82qc6xx7if57sy5rpci6502qan") (r "1.74.0")))

(define-public crate-abcrypt-capi-0.3.1 (c (n "abcrypt-capi") (v "0.3.1") (d (list (d (n "abcrypt") (r "^0.3.4") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "0dw2d1jv21x5ya74ggd22iniw4117xhijw8hgkfp3w1dmykxbc02") (r "1.74.0")))

(define-public crate-abcrypt-capi-0.3.2 (c (n "abcrypt-capi") (v "0.3.2") (d (list (d (n "abcrypt") (r "^0.3.4") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26.0") (k 1)))) (h "0sis3adrzh522jyr9f0m1fgjdr6jma6f4mf9dg6vkqhd56nkr191") (r "1.74.0")))

