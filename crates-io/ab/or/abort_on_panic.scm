(define-module (crates-io ab or abort_on_panic) #:use-module (crates-io))

(define-public crate-abort_on_panic-0.0.1 (c (n "abort_on_panic") (v "0.0.1") (h "1h8gwggc535kksh6jdcs485j3pci330jj9l6ipzgd4byd6fzmvjz")))

(define-public crate-abort_on_panic-0.0.2 (c (n "abort_on_panic") (v "0.0.2") (h "1razqdzhdgsfx63q2ww92bqi7kn0j7pxganj22kl9ci2smcasswy")))

(define-public crate-abort_on_panic-0.0.3 (c (n "abort_on_panic") (v "0.0.3") (h "0s4cxy8kl6rqixvw2p03lla369xsky8q5myj67rallgprla1i939")))

(define-public crate-abort_on_panic-0.0.4 (c (n "abort_on_panic") (v "0.0.4") (h "1wg4ka2ck4xizn1fkdq3ckqlxdqbgn41rn3h0lrsiws4kvxmp76p")))

(define-public crate-abort_on_panic-0.0.5 (c (n "abort_on_panic") (v "0.0.5") (h "0lxbipnlbqgwb3bvhclayyyf5p7w0ds74k4bcbsx4mzn4z1ywpym")))

(define-public crate-abort_on_panic-0.0.6 (c (n "abort_on_panic") (v "0.0.6") (h "0c47d52f1k8kc1dlq8slxags1zwlda93rr2dwmqd1akgr7ldq3m1")))

(define-public crate-abort_on_panic-0.0.7 (c (n "abort_on_panic") (v "0.0.7") (h "0bhkg6byz4cs92kr8gbncv4g5hjmr5lf2rsybrgmjy06yy6iyqir")))

(define-public crate-abort_on_panic-0.0.8 (c (n "abort_on_panic") (v "0.0.8") (h "0dwzq2zzv1dima8dl7bqzy1n86bfpkb0fdq8a9yy4p638bfdkszw")))

(define-public crate-abort_on_panic-1.0.0 (c (n "abort_on_panic") (v "1.0.0") (h "06gr9ryabrcg59b4496c6gwlwxy51b84zgpgap4mq2czxkwliacz") (f (quote (("unstable"))))))

(define-public crate-abort_on_panic-2.0.0 (c (n "abort_on_panic") (v "2.0.0") (h "05k1dbkab092ini24x5x54i2ifnclikapj47qsx1c95gb2n3fpwm")))

