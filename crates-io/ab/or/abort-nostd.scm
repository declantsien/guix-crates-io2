(define-module (crates-io ab or abort-nostd) #:use-module (crates-io))

(define-public crate-abort-nostd-0.1.0 (c (n "abort-nostd") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "0ybd6x0zjn04vhnnb2jhsx23h829gaz0a0clshsk5ymmj9y9k04q") (f (quote (("std")))) (y #t)))

