(define-module (crates-io ab or abort) #:use-module (crates-io))

(define-public crate-abort-0.1.0 (c (n "abort") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1hfwdlam3jw9yvjkjh13mzwdxgnih8ksjjblnwalj2525z30ca83") (y #t)))

(define-public crate-abort-0.1.1 (c (n "abort") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0gh91h3ab950m695s8pqqbj1nqxw05xmnnqzsaif0vs34lgryv08")))

(define-public crate-abort-0.1.2 (c (n "abort") (v "0.1.2") (h "1z1hm9bdlc7ar79aga8jlxsjbhbr5j3w9093grlqvir11vk0rhgz")))

(define-public crate-abort-0.1.3 (c (n "abort") (v "0.1.3") (h "1fkp2asvn6yb35f82cybnxi38y5ig223yaflijm58hhhs7vy3pdd") (f (quote (("nightly"))))))

