(define-module (crates-io ab or abort-on-drop) #:use-module (crates-io))

(define-public crate-abort-on-drop-0.1.0 (c (n "abort-on-drop") (v "0.1.0") (d (list (d (n "futures-util") (r "^0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "0fxvhjqv2szdx85ccgag077z3k5d7yf6l8hf2z9b7q3cj81ipdfp")))

(define-public crate-abort-on-drop-0.2.0 (c (n "abort-on-drop") (v "0.2.0") (d (list (d (n "futures-util") (r "^0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "06hfcyh0y74jf12wl783d2k0i342zflhi9wb8y8qd7mg2j9xp82p")))

(define-public crate-abort-on-drop-0.2.1 (c (n "abort-on-drop") (v "0.2.1") (d (list (d (n "futures-util") (r "^0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "1gfvhj6zascj4b6wgpk2cghmkgvk31ygf2vyvg6n8csc130akcq2")))

(define-public crate-abort-on-drop-0.2.2 (c (n "abort-on-drop") (v "0.2.2") (d (list (d (n "futures-util") (r "^0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "0ahl6b4d6j4lh4pv3r8n611x3r1d1nc6fz8z1i4l3xlsml0dgmjx")))

