(define-module (crates-io ab or abortable_parser) #:use-module (crates-io))

(define-public crate-abortable_parser-0.1.0 (c (n "abortable_parser") (v "0.1.0") (h "03bw4hsh4ns4w6v5cip3nfnzpm3rcdf5bcki5fkfwphnd49f6a56")))

(define-public crate-abortable_parser-0.2.0 (c (n "abortable_parser") (v "0.2.0") (h "034hil5ynga4yfp4fkw24crc4dp2zaxb7pbgmb32661lrrb0ha5x")))

(define-public crate-abortable_parser-0.2.1 (c (n "abortable_parser") (v "0.2.1") (h "01jr1n4z6q9gp5v3z380rgch49fbkx8j3rl6m2ghfjjyicvzbk89")))

(define-public crate-abortable_parser-0.2.2 (c (n "abortable_parser") (v "0.2.2") (h "04rj1s2rb17dc6gzkxi6caiy9708760dc2abyfvxcrimraxrjpb8")))

(define-public crate-abortable_parser-0.2.3 (c (n "abortable_parser") (v "0.2.3") (h "1r27wxx3z6jq7lrl1rihw74g9ggv4ikkvn90q56ag30kaq2q5dgi")))

(define-public crate-abortable_parser-0.2.4 (c (n "abortable_parser") (v "0.2.4") (h "1v6l4xc41hbdsg09q08argvf7j32in9xb02m4m3pi17dmqrbrzn2")))

(define-public crate-abortable_parser-0.2.5 (c (n "abortable_parser") (v "0.2.5") (h "0dg4zx0yb9xmn33lgcrzq3n19yblk1s1saxc5xkvxcl6v58hallg")))

(define-public crate-abortable_parser-0.2.6 (c (n "abortable_parser") (v "0.2.6") (h "1lg9wlzs43p8g3vyvdl1pnnj8gngsfr9hj4inwj8x0qx865v7ixy")))

