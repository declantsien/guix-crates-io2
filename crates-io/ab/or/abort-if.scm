(define-module (crates-io ab or abort-if) #:use-module (crates-io))

(define-public crate-abort-if-0.1.0 (c (n "abort-if") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("visit-mut" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0rmq3r1jsb78fvq0yjm9v265j2qby54masc64g5jmgwgvw4jwk8x") (f (quote (("keep_going" "custom_abort") ("default_abort") ("default" "default_abort") ("custom_abort"))))))

(define-public crate-abort-if-0.1.1 (c (n "abort-if") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("visit-mut" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "05dzdmw7pp71f9k6ch0v3gmiq3k67jl55zljzlmpwcc0gw9w15jg") (f (quote (("keep_going" "custom_abort") ("default_abort") ("default" "default_abort") ("custom_abort"))))))

(define-public crate-abort-if-0.1.2 (c (n "abort-if") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("visit-mut" "full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "11rzpq0hq8vylzfb98ihhnikxw2ldv80llnfw4yhcygn5vg1p6c9") (f (quote (("keep_going" "custom_abort") ("default_abort") ("default" "default_abort") ("custom_abort"))))))

