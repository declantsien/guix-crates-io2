(define-module (crates-io ab i_ abi_stable_shared) #:use-module (crates-io))

(define-public crate-abi_stable_shared-0.5.0 (c (n "abi_stable_shared") (v "0.5.0") (d (list (d (n "core_extensions") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "0ksvq4ifp58w94g3p9kz19fyc3q8ns161vd6wcvg8h0qdixklvsb")))

(define-public crate-abi_stable_shared-0.6.0 (c (n "abi_stable_shared") (v "0.6.0") (d (list (d (n "core_extensions") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "098a84p3ccvd8646y2pbgb93v12gkfw4gwadagzyrh36p27yqqwk")))

(define-public crate-abi_stable_shared-0.7.0 (c (n "abi_stable_shared") (v "0.7.0") (d (list (d (n "core_extensions") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "1bx3nbnxffmyjjhkn43zhrxja52ccdvph1fcwgg00nwabgx9xp2z")))

(define-public crate-abi_stable_shared-0.8.0 (c (n "abi_stable_shared") (v "0.8.0") (d (list (d (n "core_extensions") (r "^0.1") (f (quote ("std"))) (k 0)))) (h "1ps4i5xm5pxmga9q2wjs9j34pkvr9gb1ymr8xb1jj703g475nqry")))

(define-public crate-abi_stable_shared-0.9.0 (c (n "abi_stable_shared") (v "0.9.0") (d (list (d (n "core_extensions") (r "^0.1.18") (f (quote ("std"))) (k 0)))) (h "0j4brvz70aj3hxwnxhzfnrvd3hxp29zpzw52q8fdsdrzl7b14qd9")))

(define-public crate-abi_stable_shared-0.10.0 (c (n "abi_stable_shared") (v "0.10.0") (d (list (d (n "core_extensions") (r "^1.4.2") (f (quote ("std" "self_ops" "slices" "type_level_bool"))) (k 0)))) (h "10hzww5wsbj2ldxr28iy96xz7wlij8q21q3k6gf1qxjnjzpc21vc")))

(define-public crate-abi_stable_shared-0.10.2 (c (n "abi_stable_shared") (v "0.10.2") (d (list (d (n "core_extensions") (r "^1.4.2") (f (quote ("std" "self_ops" "slices" "type_level_bool"))) (k 0)))) (h "1g56f13a56ja9vx4wx1hni79wg1k9pabb6fpzfdsdrrmrra7xa80")))

(define-public crate-abi_stable_shared-0.10.3 (c (n "abi_stable_shared") (v "0.10.3") (d (list (d (n "core_extensions") (r "^1.4.2") (f (quote ("std" "self_ops" "slices" "type_level_bool"))) (k 0)))) (h "0psaz0vghdz84vrb311g4b74d2nhrlbmwxa8if88s0bf0s4xmsgc")))

(define-public crate-abi_stable_shared-0.11.0 (c (n "abi_stable_shared") (v "0.11.0") (d (list (d (n "core_extensions") (r "^1.5.2") (f (quote ("std" "self_ops" "slices" "type_level_bool"))) (k 0)))) (h "0qrbmlypvxx3zij1c6w6yykpp5pjcfx9qr2d9lzyc8y1i1vdzddj")))

