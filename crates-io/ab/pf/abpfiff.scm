(define-module (crates-io ab pf abpfiff) #:use-module (crates-io))

(define-public crate-abpfiff-0.1.0 (c (n "abpfiff") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7") (k 0)) (d (n "libc") (r "^0.2.108") (k 0)))) (h "04n5i6ngfkdlwf9pjc1zzpq53s6vg51r5qpdzcbqd852j5393cn2") (r "1.65.0")))

(define-public crate-abpfiff-0.1.1-alpha.with.debug (c (n "abpfiff") (v "0.1.1-alpha.with.debug") (d (list (d (n "bytemuck") (r "^1.7") (k 0)) (d (n "libc") (r "^0.2.108") (k 0)))) (h "1dz4bmsj65c6msvvanra337bl3ak5c4f0khd4pbalyr2r6bmg57a") (r "1.65.0")))

