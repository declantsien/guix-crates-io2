(define-module (crates-io ab si absinthe-macros) #:use-module (crates-io))

(define-public crate-absinthe-macros-0.1.0 (c (n "absinthe-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0m9jzlygbya3k9mg5bfh1jmd58qlkpzlazwsg6yq40qxm4rqkc14")))

(define-public crate-absinthe-macros-0.2.0 (c (n "absinthe-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0xpbj0a4rw3l8y855zsaxh031yc65nnijycb2f3gx5j3bkwpjx61")))

(define-public crate-absinthe-macros-0.3.0 (c (n "absinthe-macros") (v "0.3.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0i58rw1r0xpz4gv5k2i55w5b6mlvyl1msd5z2j8rwbdwkl8lpwg7")))

