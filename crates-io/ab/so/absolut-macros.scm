(define-module (crates-io ab so absolut-macros) #:use-module (crates-io))

(define-public crate-absolut-macros-0.1.1 (c (n "absolut-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)) (d (n "z3") (r "^0.11.2") (f (quote ("static-link-z3"))) (d #t) (k 0)))) (h "1ym60mrr0qv18waigrs8aiiy11dn8y37pk58ks6zcdl1kmypbkzi")))

(define-public crate-absolut-macros-0.2.0 (c (n "absolut-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "varisat") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "132xpjxznakcp3lvp1n5hpvm5dmsxsx3pxbna2qci1xkzdhlw2q7") (y #t) (s 2) (e (quote (("sat" "dep:varisat"))))))

(define-public crate-absolut-macros-0.2.1 (c (n "absolut-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "varisat") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "1z7210ycdpw562yg7ylah9grbscx17hnnv4shb5924ipzmd1kwn2") (s 2) (e (quote (("sat" "dep:varisat"))))))

