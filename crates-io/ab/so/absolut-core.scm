(define-module (crates-io ab so absolut-core) #:use-module (crates-io))

(define-public crate-absolut-core-0.1.0 (c (n "absolut-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "z3") (r "^0.11.2") (f (quote ("static-link-z3"))) (d #t) (k 0)))) (h "16xfraj8faz0gyfjah14ykp6945jfnqlfvxblc4815l7g8vkp1v6") (y #t)))

