(define-module (crates-io ab so absolut) #:use-module (crates-io))

(define-public crate-absolut-0.1.1 (c (n "absolut") (v "0.1.1") (d (list (d (n "absolut-macros") (r "^0.1.1") (d #t) (k 0)))) (h "033nsdvvc06igwpj63qahphbmvxfs9ax6vv7qpdxqb7c1prjmfav")))

(define-public crate-absolut-0.2.0 (c (n "absolut") (v "0.2.0") (d (list (d (n "absolut-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0wkhx5w9ccmgfkqabcissnbxp2yx4z5jzl25s1chkcbdqzyl20h6") (f (quote (("sat" "absolut-macros/sat")))) (y #t)))

(define-public crate-absolut-0.2.1 (c (n "absolut") (v "0.2.1") (d (list (d (n "absolut-macros") (r "^0.2.1") (d #t) (k 0)))) (h "1ma63sbwbzyl7il3l3m6s6a7d7vg1yyp97iblcdyyl4sdm7m1azz") (f (quote (("sat" "absolut-macros/sat"))))))

