(define-module (crates-io ab so absolut-derive) #:use-module (crates-io))

(define-public crate-absolut-derive-0.1.0 (c (n "absolut-derive") (v "0.1.0") (d (list (d (n "absolut-core") (r "^0.1.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0azr0ggvmhar28w676i4c1d77ngb4r58awn2rpz3xaadwg3239b5") (y #t)))

