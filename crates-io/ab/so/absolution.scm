(define-module (crates-io ab so absolution) #:use-module (crates-io))

(define-public crate-absolution-0.0.1 (c (n "absolution") (v "0.0.1") (h "11fvg9vj660vdyhkz3cn1kcl8rhl3fqp1dpxpa0p4975zk6ijixy")))

(define-public crate-absolution-0.1.0 (c (n "absolution") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)))) (h "18jrnpahzpa67mnqnpschi2jmmi1wmpcbx9i1gdlkj4cj8n264vw") (f (quote (("nightly-doc"))))))

(define-public crate-absolution-0.1.1 (c (n "absolution") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)))) (h "0wbyp93pny6jnzc6xp7j0bnp3yfyyz2ynqlkpb5zjim2hjw76qfn") (f (quote (("nightly-doc"))))))

