(define-module (crates-io ab a- aba-cache) #:use-module (crates-io))

(define-public crate-aba-cache-0.1.0 (c (n "aba-cache") (v "0.1.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "1mq0m0knmgm8xb84zkl334dbw78b6wxfwvwz3p6ahb1ag2bjachf") (f (quote (("default" "asynchronous") ("asynchronous" "tokio"))))))

(define-public crate-aba-cache-0.1.1 (c (n "aba-cache") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "01066rc8sa23ahqirhqzjq0i028xbjsxsxl35bx4yhybyrkh9akv") (f (quote (("default" "asynchronous") ("asynchronous" "tokio"))))))

