(define-module (crates-io ab lf ablf) #:use-module (crates-io))

(define-public crate-ablf-0.2.0 (c (n "ablf") (v "0.2.0") (d (list (d (n "binrw") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "zune-inflate") (r "^0.2") (d #t) (k 0)))) (h "0qn8yyrng45nfzr370qipn319q8hk88pzcvsabslcmalj8d7f0cg")))

