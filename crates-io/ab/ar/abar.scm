(define-module (crates-io ab ar abar) #:use-module (crates-io))

(define-public crate-abar-0.2.1 (c (n "abar") (v "0.2.1") (h "1fwmwfmscs1r0piakg7pc6b9l8g48c0is5ka2x255izl1hx5gw64")))

(define-public crate-abar-0.2.2 (c (n "abar") (v "0.2.2") (h "1vb6vwymxybzn33lg1aszdf8dn1ird4p4vjkn9bi8l096ia0788d")))

(define-public crate-abar-0.3.1 (c (n "abar") (v "0.3.1") (h "06166flcnfg9vjrp67rflxx8fk9rz9m1lp5hk857i67c07grxcw8")))

(define-public crate-abar-0.4.0 (c (n "abar") (v "0.4.0") (d (list (d (n "spmc") (r "^0.3") (d #t) (k 0)))) (h "0qvnvjhzcw6lbpanmdgcjnsivir8bmmzxvxn0g7283hlsv3v5ypb")))

(define-public crate-abar-0.5.0 (c (n "abar") (v "0.5.0") (d (list (d (n "spmc") (r "^0.3") (d #t) (k 0)))) (h "19l1i8i0536hbqix8mjj64lyp1h6icsxqswg4jin7bxsk9afh1ya")))

(define-public crate-abar-0.6.0 (c (n "abar") (v "0.6.0") (d (list (d (n "flume") (r "^0.10.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0a5gv942vsai5rxhq11xbx5c053l2gzq1s2whn6wzqqv38piwyrd")))

(define-public crate-abar-0.6.1 (c (n "abar") (v "0.6.1") (d (list (d (n "flume") (r "^0.10.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1k0vd7jnxywc6900cg45mlfjcrrag41a50xcnb37gg547mbrp1xh")))

(define-public crate-abar-0.6.2 (c (n "abar") (v "0.6.2") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "1znv2yn6nr86vdshk6s0gcrdw5j7qwsdqqykf7yazy6r53izqm12")))

