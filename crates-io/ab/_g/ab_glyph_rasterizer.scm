(define-module (crates-io ab _g ab_glyph_rasterizer) #:use-module (crates-io))

(define-public crate-ab_glyph_rasterizer-0.1.0 (c (n "ab_glyph_rasterizer") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "118rmldw2qpq20scv249hd123zfl3psl6xk8911y1gxw4r12zb7a") (f (quote (("std") ("default" "std"))))))

(define-public crate-ab_glyph_rasterizer-0.1.1 (c (n "ab_glyph_rasterizer") (v "0.1.1") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0i37j7ijf2h1rdc0sg6jk6lxkqsip47f6c6mc9yy5ypzdsivzshr") (f (quote (("std") ("default" "std"))))))

(define-public crate-ab_glyph_rasterizer-0.1.2 (c (n "ab_glyph_rasterizer") (v "0.1.2") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1182xxkmkv762d4vm1qfn7pz8kd7kja3js74cm1q3nvqyy64wzib") (f (quote (("std") ("default" "std"))))))

(define-public crate-ab_glyph_rasterizer-0.1.3 (c (n "ab_glyph_rasterizer") (v "0.1.3") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "05iyxk0cwgbaf22y442vasmp9prpkh8kc055zswd49r5c06q14i6") (f (quote (("std") ("default" "std"))))))

(define-public crate-ab_glyph_rasterizer-0.1.4 (c (n "ab_glyph_rasterizer") (v "0.1.4") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1zzz78231w849xslz9s0pwjj6gp02wfbbxdpysqhwwq1vqr5xznr") (f (quote (("std") ("default" "std"))))))

(define-public crate-ab_glyph_rasterizer-0.1.5 (c (n "ab_glyph_rasterizer") (v "0.1.5") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0viv49mqy966w6s3v1kcyjzghwpkz7zsv2yj1sxj5gbz2zbkjdx1") (f (quote (("std") ("default" "std"))))))

(define-public crate-ab_glyph_rasterizer-0.1.6 (c (n "ab_glyph_rasterizer") (v "0.1.6") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1fwb3fpfa4hw5zlvh9q3zl3fkpzrlg4n5xy81gl3pbykza49nfrn") (f (quote (("std") ("default" "std"))))))

(define-public crate-ab_glyph_rasterizer-0.1.7 (c (n "ab_glyph_rasterizer") (v "0.1.7") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1mp8qlcflaiii3gskbnqvgr8wzxlr68r6vljp5bqfc6cmshj60ik") (f (quote (("std") ("default" "std"))))))

(define-public crate-ab_glyph_rasterizer-0.1.8 (c (n "ab_glyph_rasterizer") (v "0.1.8") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0ikhgzig59q8b1a1iw83sxfnvylg5gx6w2y8ynbnf231xs9if6y7") (f (quote (("std") ("default" "std"))))))

