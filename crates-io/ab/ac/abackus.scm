(define-module (crates-io ab ac abackus) #:use-module (crates-io))

(define-public crate-abackus-0.1.5 (c (n "abackus") (v "0.1.5") (d (list (d (n "earlgrey") (r "^0.2.0") (d #t) (k 0)) (d (n "lexers") (r "^0.0.6") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "0shm8j8x81wvblfig1w611qlp0xwi6v2c980sgndhqv3vh22cn74")))

(define-public crate-abackus-0.1.6 (c (n "abackus") (v "0.1.6") (d (list (d (n "earlgrey") (r "^0.2.1") (d #t) (k 0)) (d (n "lexers") (r "^0.0.7") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "08js66xvbcxxbnfyw9c3ymhxs18ks48d3clvncz57slbqjirbaqs")))

(define-public crate-abackus-0.1.7 (c (n "abackus") (v "0.1.7") (d (list (d (n "earlgrey") (r "^0.2.1") (d #t) (k 0)) (d (n "lexers") (r "^0.0.7") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "038v6npvf093sycykxsh8w7cjxv86iqlzicphv64k4vfw4v71vl2")))

(define-public crate-abackus-0.1.9 (c (n "abackus") (v "0.1.9") (d (list (d (n "earlgrey") (r "^0.2.3") (d #t) (k 0)) (d (n "lexers") (r "^0.0.8") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "0wpqsh3p0s6fkq859q52fyzcg5qravn3jkd75r36gkl6y0j3rml9") (f (quote (("debug"))))))

(define-public crate-abackus-0.2.0 (c (n "abackus") (v "0.2.0") (d (list (d (n "earlgrey") (r "^0.2.4") (d #t) (k 0)) (d (n "lexers") (r "^0.1.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "1s3agnally3h6pwrfzy7g37sk5n9rgn0vlqrj3s6y5lkcz1rwfkh") (f (quote (("debug"))))))

(define-public crate-abackus-0.2.1 (c (n "abackus") (v "0.2.1") (d (list (d (n "earlgrey") (r "^0.2.4") (d #t) (k 0)) (d (n "lexers") (r "^0.1.2") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "11kqggcvmh6jfw6wrdw5w8xdk12ly06f65djlz5bvvabrdbg7llx") (f (quote (("debug"))))))

(define-public crate-abackus-0.2.2 (c (n "abackus") (v "0.2.2") (d (list (d (n "earlgrey") (r "^0.3") (d #t) (k 0)) (d (n "lexers") (r "^0.1.2") (d #t) (k 0)))) (h "0hd2fmamqfpskv0msym9ywd2l97c1fgjfi2kml4yigpdrqi4hyv1") (f (quote (("debug"))))))

(define-public crate-abackus-0.2.4 (c (n "abackus") (v "0.2.4") (d (list (d (n "earlgrey") (r "^0.3.1") (d #t) (k 0)) (d (n "lexers") (r "^0.1.4") (d #t) (k 0)))) (h "1ihx2mlllfcpys3zzv3h08716kplfzjn578hjh5cxjdn4p36mxa0") (f (quote (("debug"))))))

(define-public crate-abackus-0.2.6 (c (n "abackus") (v "0.2.6") (d (list (d (n "earlgrey") (r "^0.3") (d #t) (k 0)) (d (n "lexers") (r "^0.1") (d #t) (k 0)))) (h "1hcc5sdw93dkdfr5qi447yjshxwpiy3sd24ps2vkv6p93iyd1cz0") (f (quote (("debug"))))))

