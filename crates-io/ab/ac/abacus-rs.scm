(define-module (crates-io ab ac abacus-rs) #:use-module (crates-io))

(define-public crate-abacus-rs-0.2.0 (c (n "abacus-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "0n56wqv37f42xrkmgvdq30d1x1nx0i9rgppv71cixs81j8zbjwc2")))

(define-public crate-abacus-rs-0.2.1 (c (n "abacus-rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "073mrpywy818b1j3wp45bfxry5q80xlzykrk2l3wnygigy9cd82s")))

