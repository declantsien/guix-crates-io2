(define-module (crates-io ab ac abacuz) #:use-module (crates-io))

(define-public crate-abacuz-0.1.0 (c (n "abacuz") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^3") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rustls") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1j10f65hicw3jr3dgh48hymry9gngyrsyx30l0v885dv4kr3xdz4")))

(define-public crate-abacuz-0.1.1 (c (n "abacuz") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "qinpel-srv") (r "^0.2.1") (d #t) (k 0)))) (h "06bhjsv6b20a740nnj6vbl1p5j1x6x908vbm61cnj1v6pl2vqgp9")))

