(define-module (crates-io ab st abstractapi) #:use-module (crates-io))

(define-public crate-abstractapi-0.1.0 (c (n "abstractapi") (v "0.1.0") (d (list (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror_lite") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2.3") (f (quote ("json"))) (d #t) (k 0)))) (h "0mwp0zvbwf7k16pz1z70mcfv69wpc61hx4xla5plaz38jsnnnwkc")))

(define-public crate-abstractapi-0.1.1 (c (n "abstractapi") (v "0.1.1") (d (list (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror_lite") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2.3") (f (quote ("json"))) (d #t) (k 0)))) (h "1p00nr67lfypdxrln0sq00kp4kwrj03hl6rp79x6sxy5hkx40dh2")))

(define-public crate-abstractapi-0.1.2 (c (n "abstractapi") (v "0.1.2") (d (list (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror_lite") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2.3") (f (quote ("json"))) (d #t) (k 0)))) (h "1x62rh3shnl0w266l5nwl0sdbk0pyli79qvimgsvkfid7a4cqvgv")))

(define-public crate-abstractapi-0.1.3 (c (n "abstractapi") (v "0.1.3") (d (list (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror_lite") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2.4") (f (quote ("json"))) (d #t) (k 0)))) (h "0a4vg6qbwzvgs0m7csj7if2gw3hbqadnbdx2khrm374j94nsl3sm")))

