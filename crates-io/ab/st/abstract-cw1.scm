(define-module (crates-io ab st abstract-cw1) #:use-module (crates-io))

(define-public crate-abstract-cw1-1.2.2 (c (n "abstract-cw1") (v "1.2.2") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "1frybdayn8z5fkmvcmp23p2bdjfcr4z9hgs5789nalbamdvc1588")))

