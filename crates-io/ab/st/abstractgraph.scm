(define-module (crates-io ab st abstractgraph) #:use-module (crates-io))

(define-public crate-abstractgraph-0.1.0 (c (n "abstractgraph") (v "0.1.0") (h "03zhyiklrkhzmm4y05k6ymqb3qs448ij99dvhvcpy3a5ysgn8sjs")))

(define-public crate-abstractgraph-0.1.1 (c (n "abstractgraph") (v "0.1.1") (h "10v792fimkqlj3an0hr2sy0qh0jj15yllghfxckc8z7xmmis1nrb")))

(define-public crate-abstractgraph-0.1.2 (c (n "abstractgraph") (v "0.1.2") (h "1x29frw95cgsv67p615y935p30scwn48wjwa0zclm3cij8pqar8m")))

(define-public crate-abstractgraph-0.1.3 (c (n "abstractgraph") (v "0.1.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.5") (d #t) (k 0)))) (h "0vzp3hggppmvsapwmfw9bcq64s68cj2w8h14ml0hs4f76by47ja8")))

(define-public crate-abstractgraph-0.1.4 (c (n "abstractgraph") (v "0.1.4") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.5") (d #t) (k 0)))) (h "1blxw8kq867ayam3m943jq6v3a7lj024vrws5ysibsy10hswavwg")))

