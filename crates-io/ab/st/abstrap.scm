(define-module (crates-io ab st abstrap) #:use-module (crates-io))

(define-public crate-abstrap-0.1.0 (c (n "abstrap") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11jzv12vg748lrs9rna178f53pi1blcgn5jbv2z2v9skgvzv4nnp") (f (quote (("mlir")))) (y #t)))

