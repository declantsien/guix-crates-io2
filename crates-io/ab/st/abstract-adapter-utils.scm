(define-module (crates-io ab st abstract-adapter-utils) #:use-module (crates-io))

(define-public crate-abstract-adapter-utils-0.16.1 (c (n "abstract-adapter-utils") (v "0.16.1") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0) (p "abstract-cw20")))) (h "1wppl9kagjgzij4xwvfidmfcjbf4alqfli3hfssn9x7fvrvc7h9d") (y #t)))

(define-public crate-abstract-adapter-utils-0.16.0 (c (n "abstract-adapter-utils") (v "0.16.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0) (p "abstract-cw20")))) (h "0j1d4grm6kgf8wqmpsasik9i51h8b8968a67525qmzj60c9mi0aj")))

(define-public crate-abstract-adapter-utils-0.17.0 (c (n "abstract-adapter-utils") (v "0.17.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0) (p "abstract-cw20")))) (h "1n9d470m01rmb2r4ymqjz7iniwg6hck781z5d0kr75gxc0kii1f4") (y #t)))

(define-public crate-abstract-adapter-utils-0.17.1 (c (n "abstract-adapter-utils") (v "0.17.1") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)))) (h "1w1d1dp04zn8j2w5g690qbrwy9d5s0mf2lf4xz5fvzdfq46p9vgx")))

(define-public crate-abstract-adapter-utils-0.17.2 (c (n "abstract-adapter-utils") (v "0.17.2") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)))) (h "0c15yncyd542b1y20768pkhnii47d081p30nxpj3khvhcwla5v1z")))

(define-public crate-abstract-adapter-utils-0.18.0 (c (n "abstract-adapter-utils") (v "0.18.0") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)))) (h "1ayfdr570md503r01z6ypn9ycdf6aiwcj6kvjslpl0cb0s3kx7ah")))

(define-public crate-abstract-adapter-utils-0.19.0-rc.1 (c (n "abstract-adapter-utils") (v "0.19.0-rc.1") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)))) (h "1v3hi88jhmp8skjq1xk6jp0v1ki14zh4011zz1is7kfa6q8ix969")))

(define-public crate-abstract-adapter-utils-0.19.0 (c (n "abstract-adapter-utils") (v "0.19.0") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)))) (h "10kqfdqq884fl7wg1qlxz44dzy4m9pn1wjg2s8wzr8624ja8hfn4")))

(define-public crate-abstract-adapter-utils-0.19.2 (c (n "abstract-adapter-utils") (v "0.19.2") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)))) (h "17bw46zwh9zdsf26jasabylq2k7q7zil76k741zgnfl3c0v482sn")))

(define-public crate-abstract-adapter-utils-0.20.0 (c (n "abstract-adapter-utils") (v "0.20.0") (d (list (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("cosmwasm_1_2"))) (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)))) (h "1k0hhkjgrxx9ifqhskisgsigbqqsl25wakml8s288irx5mgwfgdp")))

(define-public crate-abstract-adapter-utils-0.21.0 (c (n "abstract-adapter-utils") (v "0.21.0") (d (list (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("cosmwasm_1_2"))) (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)))) (h "0azy4qhzb0xj60hbqmin094wg5qqa8f3yc7ikpbdqi3mmqa54nhb")))

(define-public crate-abstract-adapter-utils-0.22.1 (c (n "abstract-adapter-utils") (v "0.22.1") (d (list (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("cosmwasm_1_2"))) (d #t) (k 0)) (d (n "cw-asset") (r "^3.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0") (d #t) (k 0)))) (h "0nfz7xbcsifbip0ajabxlifgilm28kpsdicv0pf7vggnv3xm0hn7")))

