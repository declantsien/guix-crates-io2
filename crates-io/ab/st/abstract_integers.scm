(define-module (crates-io ab st abstract_integers) #:use-module (crates-io))

(define-public crate-abstract_integers-0.1.0 (c (n "abstract_integers") (v "0.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0h5k83azs004200bqyyywmqhq6908c7ydf6r2w36fvkp4lpbc4af")))

(define-public crate-abstract_integers-0.1.1 (c (n "abstract_integers") (v "0.1.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1pf3m50854y634br6qcwwi7aab6hz2g5ykl8k3ln2byb9jc6sc5y")))

(define-public crate-abstract_integers-0.1.2 (c (n "abstract_integers") (v "0.1.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0ly3wbydqi8iblznhbs2g4zh5kba5nppi08y7lklm0xgdr8d8lg9")))

(define-public crate-abstract_integers-0.1.3 (c (n "abstract_integers") (v "0.1.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1q5df9m9mx68ay65bam8nhq68cxj59lx56hw9p04bccsyjk22n69")))

(define-public crate-abstract_integers-0.1.5 (c (n "abstract_integers") (v "0.1.5") (d (list (d (n "num") (r "^0.4") (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)))) (h "0mnz02m0hkyfmvaahid7qqx9fygsnsxb8sfs2dpyi2vgmhmjg6z5") (f (quote (("std" "num/std" "num-bigint/std") ("default" "std"))))))

