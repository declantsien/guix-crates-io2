(define-module (crates-io ab st abstract-polytone) #:use-module (crates-io))

(define-public crate-abstract-polytone-1.0.2 (c (n "abstract-polytone") (v "1.0.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0pk712rymixkrqllqbmsli5xmx3fra7g1mswksrlvrh3r6bpch44") (r "1.67")))

(define-public crate-abstract-polytone-1.0.5 (c (n "abstract-polytone") (v "1.0.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1z0cq7vmkll5wk0m4m5wy6xl8qd9yga4f30lfvbxbj35din6lrq5") (r "1.67")))

