(define-module (crates-io ab st abstract-macros) #:use-module (crates-io))

(define-public crate-abstract-macros-0.10.0 (c (n "abstract-macros") (v "0.10.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lr0vmaqlijqc5s3d1glal374wzx97f1gwlfkic1kkxxs98nzghb")))

(define-public crate-abstract-macros-0.10.3 (c (n "abstract-macros") (v "0.10.3") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a4nvqb72x172q8hb64g6iz5m1n8adgp8zqcl4riad0pid8h4jbc")))

(define-public crate-abstract-macros-0.10.2 (c (n "abstract-macros") (v "0.10.2") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s4wyh9fcq1q607g9z4qhjf7m4hm0pwpdmlk72y0iykimqkqvypg")))

(define-public crate-abstract-macros-0.11.0 (c (n "abstract-macros") (v "0.11.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09bdcz2mcr2li50ap6b1whq2b97jpqb5vd3ljdz3f73l20kis62g")))

(define-public crate-abstract-macros-0.13.0 (c (n "abstract-macros") (v "0.13.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0463jw24a1gsxf5yxbr8l8xymb9xdfzqcjx2w47f8jzjp7659yf7")))

(define-public crate-abstract-macros-0.13.1 (c (n "abstract-macros") (v "0.13.1") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rf9gy274xhl2h729ba7ipy05r7j5l1x0p3wj956mn4r1gixlimv")))

(define-public crate-abstract-macros-0.13.2 (c (n "abstract-macros") (v "0.13.2") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lpfshms21kkyx15x5xbmzrs16dj2q2a6wmqlw9b9iwmjh5pxai6")))

(define-public crate-abstract-macros-0.14.0 (c (n "abstract-macros") (v "0.14.0") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y6ralvsfjcr64717wz52mwm03pgg8vglxarpkrfvrx76lmfkbnv")))

(define-public crate-abstract-macros-0.0.1 (c (n "abstract-macros") (v "0.0.1") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v8xcny0c98qbia2ipgwdv2plbvlz5g0qav04h2s58apqwz7yaid")))

(define-public crate-abstract-macros-0.14.1 (c (n "abstract-macros") (v "0.14.1") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18r1pka13gq4kqraf400x5k0k2fc49ish2xssvbr6a142b26zs0i")))

(define-public crate-abstract-macros-0.14.3 (c (n "abstract-macros") (v "0.14.3") (d (list (d (n "cosmwasm-std") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bz18fm6i7rv74zvs77ymvzw8h7ij329a8l044cmkgc7mmyj1b7f")))

(define-public crate-abstract-macros-0.15.1 (c (n "abstract-macros") (v "0.15.1") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ayyifysl9mh28ji7nbb23ya4i53nbpr6a6js22ayn2r4n7pm4bz")))

(define-public crate-abstract-macros-0.15.3 (c (n "abstract-macros") (v "0.15.3") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qk59hvqaip3a1j26xmyfw4nzdbb6p1628vlwrl96b6h4srbrpam")))

(define-public crate-abstract-macros-0.15.4 (c (n "abstract-macros") (v "0.15.4") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bjn8by5asdxsfmvncsg4jk84xgm2s2c4xvc7h1cwhcjlss565rv")))

(define-public crate-abstract-macros-0.16.0 (c (n "abstract-macros") (v "0.16.0") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zs131gyqbirla97s659s641wgmnl9xy9pfnjpr9b67jb60yybak")))

(define-public crate-abstract-macros-0.16.1 (c (n "abstract-macros") (v "0.16.1") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15lf7aq4hb40hp6nhdci1n9w7pdrg59xm4ajshvmyqanjfvgw5wq")))

(define-public crate-abstract-macros-0.17.0 (c (n "abstract-macros") (v "0.17.0") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p95gg8yl79lfl7rjfyn0grsx81my2hd7njqqlmhny4dyiabhhnr")))

(define-public crate-abstract-macros-0.17.1 (c (n "abstract-macros") (v "0.17.1") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1s25v3qb73b83qy3z8n9l4yj2sxp2iahggjmj8hw5k0xn54fnv1d") (y #t)))

(define-public crate-abstract-macros-0.17.2 (c (n "abstract-macros") (v "0.17.2") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qb63lvhlrjdx3jy5acb2qm3bxf6dcs22lyblp78g8lfzc88vnp7")))

(define-public crate-abstract-macros-0.18.0 (c (n "abstract-macros") (v "0.18.0") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18k07yx7vi5qcgixgxlmwa627rrzrhs3rpgif4grdi74c07d4mf5")))

(define-public crate-abstract-macros-0.19.0-rc.1 (c (n "abstract-macros") (v "0.19.0-rc.1") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00wqvmbh21p7h04ixy748pv6k7lbg4z7z67akwh872xxi9h78zzb")))

(define-public crate-abstract-macros-0.19.0 (c (n "abstract-macros") (v "0.19.0") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d81lw6na2ck497ryyrp12fkvkndvaqy0nchq5sb3im251wc4yjm")))

(define-public crate-abstract-macros-0.19.2 (c (n "abstract-macros") (v "0.19.2") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15rydaamvrir4hs92qxinvyxlb8jbgnnvmyw8bvh5b4f42zd793a")))

(define-public crate-abstract-macros-0.20.0-rc.1 (c (n "abstract-macros") (v "0.20.0-rc.1") (d (list (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("cosmwasm_1_2"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yn2vsi77q22r48bdz89yd2sj2fw78jp6fbqs8472xxzwmcqlzr3")))

(define-public crate-abstract-macros-0.20.0 (c (n "abstract-macros") (v "0.20.0") (d (list (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("cosmwasm_1_2"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fb70b7hdksy8k5flimd1jfig5x0lz2g7xhpil2id0kf9q6d7jca")))

(define-public crate-abstract-macros-0.21.0 (c (n "abstract-macros") (v "0.21.0") (d (list (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("cosmwasm_1_2"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qk0afv6blhxy1xs8miih5y91qb8gzlw0xjfwrbmxxjf6ag2wsyl")))

(define-public crate-abstract-macros-0.22.1 (c (n "abstract-macros") (v "0.22.1") (d (list (d (n "cosmwasm-std") (r "^1.5.0") (f (quote ("cosmwasm_1_2"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w9723zn98x9nr4dviqqfaa8sqh3648ykbkx19wl21id5dj0yl30")))

