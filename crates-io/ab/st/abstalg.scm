(define-module (crates-io ab st abstalg) #:use-module (crates-io))

(define-public crate-abstalg-0.1.0 (c (n "abstalg") (v "0.1.0") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "1bf0wzx40j9v9bb2v9fbnsyqyvbhhcz504gr7awpxzry4y9r1v35")))

(define-public crate-abstalg-0.1.1 (c (n "abstalg") (v "0.1.1") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "1mvf0nmaysr89d0x6vyy7hicz29ix5rnd4mqmxkw43i8sw4m2gs8")))

(define-public crate-abstalg-0.1.2 (c (n "abstalg") (v "0.1.2") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "1g8dlyxwsv9xqc35xlpzbllvnm36d30gvks2ibl5313qz0vfvr4x")))

(define-public crate-abstalg-0.1.3 (c (n "abstalg") (v "0.1.3") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "07c34ppya3xbj1fnv3dlpln4k5mcc40jmbyzp2ri79z6gxcv5xqs")))

(define-public crate-abstalg-0.1.4 (c (n "abstalg") (v "0.1.4") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "1gn7qnrwvj7r4g9556r2jc4d92i4w61w8i65ngw54jbq1vwm8b45")))

(define-public crate-abstalg-0.1.5 (c (n "abstalg") (v "0.1.5") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "1iilzyd5dms4q21gk127vvlvvxf39azsv53a9n2ahiz3i0cvd9wc")))

(define-public crate-abstalg-0.1.6 (c (n "abstalg") (v "0.1.6") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)))) (h "0d0rvrc182rw2qinzpdldf5g87298q8bq2ds9g7b50cp23lhwgr9")))

