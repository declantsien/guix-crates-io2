(define-module (crates-io ab st abstract-cw4) #:use-module (crates-io))

(define-public crate-abstract-cw4-1.2.2 (c (n "abstract-cw4") (v "1.2.2") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "04l99avnldknxaahz63q2rdadfmwhm16hdjn5dpywdyf9l9b1b3s")))

