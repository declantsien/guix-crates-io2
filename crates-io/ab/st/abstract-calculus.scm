(define-module (crates-io ab st abstract-calculus) #:use-module (crates-io))

(define-public crate-abstract-calculus-0.1.0 (c (n "abstract-calculus") (v "0.1.0") (h "0423wyplw09qgw65h4zbg48ym852bn9zr7i4nxn80v2byj8mbqrr")))

(define-public crate-abstract-calculus-0.1.1 (c (n "abstract-calculus") (v "0.1.1") (h "0rqfs1vnz8ynd773xg714p93a2x1y9jpsaaafgzlpa0ggw64prbi")))

(define-public crate-abstract-calculus-0.1.2 (c (n "abstract-calculus") (v "0.1.2") (h "04lk1l9mllb9wkyywfnva6k4jvjm0xn15j2106ljmfag9jp7s7ni")))

(define-public crate-abstract-calculus-0.1.3 (c (n "abstract-calculus") (v "0.1.3") (h "0n83ywjnfyr7zhl38yvd82p4sqn19mlmza1yg7zgfijajc96xb7m")))

(define-public crate-abstract-calculus-0.1.4 (c (n "abstract-calculus") (v "0.1.4") (h "1981rhphfmvw7lj85fgi6chbjhwpxf620rzvqvmp4x4yc0hdvmnn")))

(define-public crate-abstract-calculus-0.1.5 (c (n "abstract-calculus") (v "0.1.5") (h "1h5v8ymhs9dm0vrlqcvj34paskcbdm2b7xhx81fha9kmk9l2fygd")))

