(define-module (crates-io ab st abstract-macros-testing) #:use-module (crates-io))

(define-public crate-abstract-macros-testing-0.15.0 (c (n "abstract-macros-testing") (v "0.15.0") (d (list (d (n "cosmwasm-std") (r "^1.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wc9nc9paq85072pqb2m44pnpli5a82jz9rpwp5b3d92x7y98x0b")))

