(define-module (crates-io ab st abstract-ica-testing) #:use-module (crates-io))

(define-public crate-abstract-ica-testing-0.15.0 (c (n "abstract-ica-testing") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2") (f (quote ("ibc3"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h3k8k6gbn5zha3zz7z1br4bg1mji4hwp1qlkn0760lf58w7g3jj") (f (quote (("default"))))))

