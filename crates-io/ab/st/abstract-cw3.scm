(define-module (crates-io ab st abstract-cw3) #:use-module (crates-io))

(define-public crate-abstract-cw3-1.2.2 (c (n "abstract-cw3") (v "1.2.2") (d (list (d (n "abstract-cw20") (r "^1.2.2") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0017pdpzps069755ycgj9j7z3mw09vgxp7rda7v4cx3z4s5ad9cv")))

(define-public crate-abstract-cw3-2.0.0-rc (c (n "abstract-cw3") (v "2.0.0-rc") (d (list (d (n "abstract-cw20") (r "^2.0.0-rc") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "14qf6apcpa9wy0ywzy3065c3qalmddal7a09h3jgvha9swrarak6")))

(define-public crate-abstract-cw3-2.0.0 (c (n "abstract-cw3") (v "2.0.0") (d (list (d (n "abstract-cw20") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "05zmkga16a91g1x4ml2q3qgncvkym8csrbjpg13x7l9k0dvcr060")))

