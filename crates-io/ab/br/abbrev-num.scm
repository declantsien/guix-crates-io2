(define-module (crates-io ab br abbrev-num) #:use-module (crates-io))

(define-public crate-abbrev-num-0.1.0 (c (n "abbrev-num") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.35.0") (k 0)))) (h "1h700n442jkrdq6zidk7j3jv9qk90p3zdc6iwwc9kbgyllqb50lc")))

