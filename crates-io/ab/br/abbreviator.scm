(define-module (crates-io ab br abbreviator) #:use-module (crates-io))

(define-public crate-abbreviator-0.1.0 (c (n "abbreviator") (v "0.1.0") (h "04k4vcw5628kdlrw555dwmxfkfp1hzxn0fcc09awxchjkii92wjz")))

(define-public crate-abbreviator-0.1.1 (c (n "abbreviator") (v "0.1.1") (h "11cww2m7hjvsf5sbnrmlqkfxxfgf8nh8hb87ip80h33464w392mk")))

(define-public crate-abbreviator-0.1.2 (c (n "abbreviator") (v "0.1.2") (h "13wd1lv9mvrjkv4138zq9aw85lvvjsxmk53fvy76azb3izr6khnj")))

(define-public crate-abbreviator-0.1.3 (c (n "abbreviator") (v "0.1.3") (h "1dj3y5xdrgrhalxmjyg8dw3np3f2zkfnbm6346hp1dyska37zhqk")))

(define-public crate-abbreviator-0.1.4 (c (n "abbreviator") (v "0.1.4") (h "14i3gshql2mzkxmqb71440l1f8ni4qqpnfpzddhs4lci4mw5anmp")))

(define-public crate-abbreviator-0.1.5 (c (n "abbreviator") (v "0.1.5") (h "00ihwbwr6p16wia3q65zds098bz89n4bnk0nr4ir6gp0ay84cqhh")))

(define-public crate-abbreviator-0.1.6 (c (n "abbreviator") (v "0.1.6") (h "0v89dpbp6bch2kayk5gij64d8rnf8zlgq76slyp36l6wkfn17fcw")))

(define-public crate-abbreviator-0.1.7 (c (n "abbreviator") (v "0.1.7") (d (list (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "12n78jr9m76wfc5vsyvbsnmiclpc38wnw408mhd42x9ddh62fbd8")))

(define-public crate-abbreviator-0.1.8 (c (n "abbreviator") (v "0.1.8") (d (list (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "118bnkp261jg2zrpfwk21bp3qby7w1mbaij70l9acxrf8y6r0gxd")))

(define-public crate-abbreviator-0.1.9 (c (n "abbreviator") (v "0.1.9") (d (list (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0807d92n9sfrzqd3ian043ryk2kc5bc0ivxapfp4qs8c7wjdq9jj")))

