(define-module (crates-io ab br abbrev) #:use-module (crates-io))

(define-public crate-abbrev-0.1.0 (c (n "abbrev") (v "0.1.0") (h "1v8xbckahf94rd0p58cp5181rx3gjbhvv0pbkhmzhj4hb8lgzwn6")))

(define-public crate-abbrev-0.2.0 (c (n "abbrev") (v "0.2.0") (h "0d3jjqbpx3g7rdnyl58k4hlb8xv8lazafgh3lq4v4kbm4vg2bqnx")))

(define-public crate-abbrev-0.2.1 (c (n "abbrev") (v "0.2.1") (h "133dd1lxndpm035m5rfj6177my8f78r1zqs32md06gk122vs2xdc")))

(define-public crate-abbrev-0.2.2 (c (n "abbrev") (v "0.2.2") (h "1w6lb01mbj3bpzpffq0alanx16fsh9gd27hakz0540s12zkl1yvq")))

