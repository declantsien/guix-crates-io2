(define-module (crates-io ab br abbrev-tree) #:use-module (crates-io))

(define-public crate-abbrev-tree-0.1.0 (c (n "abbrev-tree") (v "0.1.0") (h "1xbv0zq614x7y1wdhqhcn7mqw4r24d8698jhd9cfk0nlispx42sz")))

(define-public crate-abbrev-tree-0.1.1 (c (n "abbrev-tree") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qjcmab3k0sh2mihqn0y425npd25n6kvq0l83dsgawfwnhcd32i1")))

