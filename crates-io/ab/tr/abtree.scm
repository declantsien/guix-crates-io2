(define-module (crates-io ab tr abtree) #:use-module (crates-io))

(define-public crate-ABtree-0.1.0 (c (n "ABtree") (v "0.1.0") (h "0kkl8qiylpa5z10izljpb8zgn561gfg04b8c8yw1d5xjy7hkhbi1") (y #t)))

(define-public crate-ABtree-0.1.1 (c (n "ABtree") (v "0.1.1") (h "1xndm2d4mdqh1msv3mljknm8b44whaikx0xy4kmpi9hf62mcclb9") (y #t)))

(define-public crate-ABtree-0.1.2 (c (n "ABtree") (v "0.1.2") (h "13vly9952h2kasr376zk58x4fsh5spqrcxjrm2625p92j0rpw0fz") (y #t)))

(define-public crate-ABtree-0.2.0 (c (n "ABtree") (v "0.2.0") (h "0q5d4pknayp3x7y5kk0sb0i8s5fsjgxjg638wx304wpa06vn6l0w") (y #t)))

(define-public crate-ABtree-0.3.0 (c (n "ABtree") (v "0.3.0") (h "1fqbwdql9yaj8kcf57zfrlwygjb7vf2f7alavz7ygzbbxsja8gcz") (y #t)))

(define-public crate-ABtree-0.4.0 (c (n "ABtree") (v "0.4.0") (h "0s50nz0pr7g7x9hlpgbls169cbi1azdw289vbq29xpwm3p2y59p4") (y #t)))

(define-public crate-ABtree-0.5.0 (c (n "ABtree") (v "0.5.0") (h "10k57yj36qm2pikap8z19v0vnnmikpv5pbwfwvwil4vld4v060jk") (y #t)))

(define-public crate-ABtree-0.6.0 (c (n "ABtree") (v "0.6.0") (h "16wws14q7v2v6w9xz24l8by0g38hhpwi38qdjcph7nv0xf52qcaj") (y #t)))

(define-public crate-ABtree-0.7.0 (c (n "ABtree") (v "0.7.0") (h "0l0qy8z9gdg9ni5lkz3yhggwmskh118k6fr2cmk1skiyv21x89jv") (y #t)))

(define-public crate-ABtree-0.8.0 (c (n "ABtree") (v "0.8.0") (h "1ywhxp8ak0h6qif0kpaskpxyb9gjc67ypnn5j30viqg5k7hw3jmq")))

