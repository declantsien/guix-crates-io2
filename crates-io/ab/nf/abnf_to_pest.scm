(define-module (crates-io ab nf abnf_to_pest) #:use-module (crates-io))

(define-public crate-abnf_to_pest-0.1.0 (c (n "abnf_to_pest") (v "0.1.0") (d (list (d (n "abnf") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty") (r "^0.5.2") (d #t) (k 0)))) (h "14sif549hpiddsbcm6xrnywqsgcig6ckkkzng5hp06j1zixg4f3g")))

(define-public crate-abnf_to_pest-0.1.1 (c (n "abnf_to_pest") (v "0.1.1") (d (list (d (n "abnf") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty") (r "^0.5.2") (d #t) (k 0)))) (h "0cy4110dkmy1fkslz236wglz8a4p6fa0kx9jka9w4ik8hncmin3a")))

(define-public crate-abnf_to_pest-0.1.2 (c (n "abnf_to_pest") (v "0.1.2") (d (list (d (n "abnf") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty") (r "^0.5.2") (d #t) (k 0)))) (h "17clqr4rsk600q7xhk67nizxahj3awza6jkpiyz7902kk5gjh3ph")))

(define-public crate-abnf_to_pest-0.2.0 (c (n "abnf_to_pest") (v "0.2.0") (d (list (d (n "abnf") (r "^0.6.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty") (r "^0.5.2") (d #t) (k 0)))) (h "0yvgmjh6f3z4g4fki7zhzg6w9d3xyyszxb4sx19sinbmpfnyym1i")))

(define-public crate-abnf_to_pest-0.5.0 (c (n "abnf_to_pest") (v "0.5.0") (d (list (d (n "abnf") (r "^0.6.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "pretty") (r "^0.5.2") (d #t) (k 0)))) (h "0bmypcs1cjpkpb1vrv7p2j38q1r0q6rcsfsidf0xh8m4sfjslarp")))

(define-public crate-abnf_to_pest-0.5.1 (c (n "abnf_to_pest") (v "0.5.1") (d (list (d (n "abnf") (r "^0.12.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "pretty") (r "^0.11.3") (d #t) (k 0)))) (h "0cg37yi8k083slnlzrifflq7c44w9k9bj4jk7959d9yrdmk5k7ck")))

