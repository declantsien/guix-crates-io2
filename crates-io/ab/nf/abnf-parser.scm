(define-module (crates-io ab nf abnf-parser) #:use-module (crates-io))

(define-public crate-abnf-parser-0.1.0 (c (n "abnf-parser") (v "0.1.0") (d (list (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)))) (h "0xi7g0r2f9wxswfa1yz2xjam2mnil5v0x0dz2l2whcngpkmi7j1z")))

(define-public crate-abnf-parser-0.1.1 (c (n "abnf-parser") (v "0.1.1") (d (list (d (n "quick-error") (r "^2.0.1") (d #t) (k 0)))) (h "0q65nc2w20zh8zi5r695ihczjzs06r18fx9wc9s4yw3p9qj1sn6f")))

