(define-module (crates-io ab nf abnf) #:use-module (crates-io))

(define-public crate-abnf-0.1.1 (c (n "abnf") (v "0.1.1") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "022jqlx9m1j975wfmbbb4cazaj4kcbbvrmrvafl8sd2ijdxdcm41")))

(define-public crate-abnf-0.1.2 (c (n "abnf") (v "0.1.2") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1dkp4k8ivkvpsllpp42h5f9wzgfzv0zra82nnbikk2wgqp72rm9w")))

(define-public crate-abnf-0.1.3 (c (n "abnf") (v "0.1.3") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1nw2y5gvi5fw6diiskssayanr5k3sxmm8m184wxnkm6jz80swwnr")))

(define-public crate-abnf-0.2.0 (c (n "abnf") (v "0.2.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1v3bgkv6k3qvl18a8zqwbnhjkrhimshjwy00zqgm6aky6dkiahzr")))

(define-public crate-abnf-0.3.0 (c (n "abnf") (v "0.3.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1734h4p44mfxiwihnmsxsv6v01q1m0ygfnx1pl6brbg0vd5qia6q")))

(define-public crate-abnf-0.4.0 (c (n "abnf") (v "0.4.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1d8wgr42c4zfrmnamh1zz51pza0m6hi1i3hj834k0xjgr1qgm2jz")))

(define-public crate-abnf-0.4.1 (c (n "abnf") (v "0.4.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0l6rchqvagr7bh2kvr1qdy22dbaj6xj1c9jkzgxyh5x89nm6fvin")))

(define-public crate-abnf-0.5.0 (c (n "abnf") (v "0.5.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0if7vp2z453ln85s3jvza2lq53v1j96w3y9k5y6hryi0dmg6b1b7")))

(define-public crate-abnf-0.6.0 (c (n "abnf") (v "0.6.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "055chbk42cgch85p4whvf236nr9595vqxk2l75cqlhzryaf02rg0")))

(define-public crate-abnf-0.6.1 (c (n "abnf") (v "0.6.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0qjlh11h3j8mgcifwxd004d1df7ahwmcl14fyag6607prvxvkzj7")))

(define-public crate-abnf-0.7.0 (c (n "abnf") (v "0.7.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0a10zchv11c0i5q15s4dpcw11sbs4y3vy78y4h606gflnip6p7dz")))

(define-public crate-abnf-0.8.0 (c (n "abnf") (v "0.8.0") (d (list (d (n "abnf-core") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "13sjy1g0i61a9lckmpvylpgg5q0r3xd8wyxk3fwwlbhx9q962xlh")))

(define-public crate-abnf-0.9.0 (c (n "abnf") (v "0.9.0") (d (list (d (n "abnf-core") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1d9hrdgggg83z9qmr5b708qbx992plrrf2k63dqdavr3s9izkmbn")))

(define-public crate-abnf-0.10.0 (c (n "abnf") (v "0.10.0") (d (list (d (n "abnf-core") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0saz7cx667l0822r7lvl2rcg7mfav14ixszrzz7kkciinkcsn2q2")))

(define-public crate-abnf-0.10.1 (c (n "abnf") (v "0.10.1") (d (list (d (n "abnf-core") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1drbrg15ps64v6qjriy65b0x826nv0cn26kxpm961d7wmiwc68zb")))

(define-public crate-abnf-0.10.2 (c (n "abnf") (v "0.10.2") (d (list (d (n "abnf-core") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1k7sc7lyvqwgzjrr9lyb8nnp4frl942rpqbn0gapli23vgkn727x")))

(define-public crate-abnf-0.11.3 (c (n "abnf") (v "0.11.3") (d (list (d (n "abnf-core") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "148prid9r3w3pbyb3f5ifff71d22shyn0bbm3s9qf3aj1hr6krln")))

(define-public crate-abnf-0.12.0 (c (n "abnf") (v "0.12.0") (d (list (d (n "abnf-core") (r "^0.5.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0-alpha1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0swyvqkzbnqv5hwgpjq6mm3jij3agk5gzs65vqzy91id8sm1nx1k")))

(define-public crate-abnf-0.13.0 (c (n "abnf") (v "0.13.0") (d (list (d (n "abnf-core") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)))) (h "0fh314g36l1n2yszms7w6barklf7fq25vv8fhljcxbfra2yi6w88")))

