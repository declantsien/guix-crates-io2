(define-module (crates-io ab nf abnf-core) #:use-module (crates-io))

(define-public crate-abnf-core-0.1.0 (c (n "abnf-core") (v "0.1.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1fj1lf959xrcfmsls1ybgjf0jpmmp6sknjqz8223z0nmmw88j3xa")))

(define-public crate-abnf-core-0.2.0 (c (n "abnf-core") (v "0.2.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0wv2215f8lxrjwh9qgy76mjbpsyg8l4q1v2ca7mkzb85vd7qjz0k")))

(define-public crate-abnf-core-0.3.0 (c (n "abnf-core") (v "0.3.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "167swynvkl3d1zwjwhc3lnnppcmr4qvlkpk1xpy75jc215fz3g34")))

(define-public crate-abnf-core-0.4.0 (c (n "abnf-core") (v "0.4.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1d6xwf1cllh6wgdy7y0bshvlygkacxcbq1j4yl0l570rnx69855m")))

(define-public crate-abnf-core-0.4.1 (c (n "abnf-core") (v "0.4.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1v1fmz2s4adn5igv1yc6rnbc7rg3fza5fv96wqayr11fsm75f6l7")))

(define-public crate-abnf-core-0.5.0 (c (n "abnf-core") (v "0.5.0") (d (list (d (n "nom") (r "^7.0.0-alpha1") (d #t) (k 0)))) (h "0zfxh7kfs54w5279w6vyrwk7q26hf9j5cfm0j7xnihz17b20jkn4")))

(define-public crate-abnf-core-0.6.0 (c (n "abnf-core") (v "0.6.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1shdbi2ffzvyf6v3wwlgwp5sa58m22pqk716b6gnm40v0wgjs67c")))

