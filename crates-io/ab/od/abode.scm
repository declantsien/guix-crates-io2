(define-module (crates-io ab od abode) #:use-module (crates-io))

(define-public crate-abode-0.0.0 (c (n "abode") (v "0.0.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "opendal") (r "^0.27") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "sluice") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0l8iy986vfvipd6l3basllji13v83bg8synq04mzd55vqbn851yr")))

