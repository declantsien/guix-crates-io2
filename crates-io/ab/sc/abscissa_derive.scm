(define-module (crates-io ab sc abscissa_derive) #:use-module (crates-io))

(define-public crate-abscissa_derive-0.0.1 (c (n "abscissa_derive") (v "0.0.1") (d (list (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "synstructure") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0w6x2mmi7i9p3jmpwvg3mxwd8y2r5ivym9h0vqqpdjj92y7gx22f") (f (quote (("options") ("errors" "failure" "synstructure") ("default" "errors" "options"))))))

(define-public crate-abscissa_derive-0.0.2 (c (n "abscissa_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0548297rf2b8c0hg5cknxnbzpqjm2iy1jmljsjk3gws027zx6k7g") (f (quote (("options") ("default" "options"))))))

(define-public crate-abscissa_derive-0.1.0-pre.1 (c (n "abscissa_derive") (v "0.1.0-pre.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0hc311nfzya758fpl048qgmpqf2fam88yf06g0x63ilv85b64jm1")))

(define-public crate-abscissa_derive-0.1.0-pre.2 (c (n "abscissa_derive") (v "0.1.0-pre.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0ahvnb5nl3p4k2v4ngvzx27h02gazii0xx1kym1ha4jgrmzdzaad")))

(define-public crate-abscissa_derive-0.1.0-rc.0 (c (n "abscissa_derive") (v "0.1.0-rc.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "14l17411s5bda201jkh2c5a1assmsrvkkfwxmhzdf21bcn8f5xjw")))

(define-public crate-abscissa_derive-0.1.0 (c (n "abscissa_derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "148carpvmzrcgdd0nggghb77af0swm8qfzsw8acrvs6473dr8mrr")))

(define-public crate-abscissa_derive-0.2.0-rc.0 (c (n "abscissa_derive") (v "0.2.0-rc.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0c9lj1ffbz5rw03p8433ygkvw4jmpmsy85s7id4ijxpa0l0vzpzw")))

(define-public crate-abscissa_derive-0.2.0-rc.1 (c (n "abscissa_derive") (v "0.2.0-rc.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0mk2yjh06agffch8snqn2ibgb8v0a6bd8b4f1siahrsrlixza79b")))

(define-public crate-abscissa_derive-0.2.0 (c (n "abscissa_derive") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0n883aibnfwc7i7ibkrsyl1s7c31f0gvzhn1g3mcd237r0rxhb58")))

(define-public crate-abscissa_derive-0.2.1 (c (n "abscissa_derive") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "1waq4clncjkkkxlnz1d18s992w4ifrlh2zs1g3xkkm6wvdamlzlc")))

(define-public crate-abscissa_derive-0.3.0 (c (n "abscissa_derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0n2vnkj4pc0cgs56a1f54d12m7sh5w96hfafb45qwvm2r76p182q")))

(define-public crate-abscissa_derive-0.4.0 (c (n "abscissa_derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "11p50q5b09ialwcf3wyw87lyxhnjjdhvjg8qahx5jr01mrn0n7gh")))

(define-public crate-abscissa_derive-0.5.0-rc.0 (c (n "abscissa_derive") (v "0.5.0-rc.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "13jbf95jlasv7nc7m00y0lfz3y872ch63prq4zk9qi2dls9im30p")))

(define-public crate-abscissa_derive-0.5.0 (c (n "abscissa_derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1k7cxjxgkvi91968xkrjcqpq98kabfh7qhnqh6fwnqw7qhmp5xbl")))

(define-public crate-abscissa_derive-0.6.0-pre.2 (c (n "abscissa_derive") (v "0.6.0-pre.2") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0zlwdl9pb5cj702519yinkhyfvgj2pijx4j22nnzaw96rk16cmqh")))

(define-public crate-abscissa_derive-0.6.0-beta.1 (c (n "abscissa_derive") (v "0.6.0-beta.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "04r6wxi40x3qfhqy859rvzdrk7xqy3wsz1hw97xz4zn7z7w8dkm7")))

(define-public crate-abscissa_derive-0.6.0-rc.0 (c (n "abscissa_derive") (v "0.6.0-rc.0") (d (list (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1rnar9jf276v1nx2vzp3r4yr7z799jxw13p7jcc8km5ncjwn0bgb")))

(define-public crate-abscissa_derive-0.6.0-rc.1 (c (n "abscissa_derive") (v "0.6.0-rc.1") (d (list (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1im1drxixbm254614qqvgn3pmp6l4livam5zf7a5g5857nhcdq33")))

(define-public crate-abscissa_derive-0.6.0 (c (n "abscissa_derive") (v "0.6.0") (d (list (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0gvxag862yj9h92vyb89ybcsfm2am8m108xp0rd8d41fcnm76d0s") (r "1.56")))

(define-public crate-abscissa_derive-0.7.0 (c (n "abscissa_derive") (v "0.7.0") (d (list (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0qlbv97is50ii477c5mrz83qzk6xvinq4w65hbj0cg6iaxpbigsm") (r "1.60")))

