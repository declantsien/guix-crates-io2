(define-module (crates-io ab sc abscissa_tokio) #:use-module (crates-io))

(define-public crate-abscissa_tokio-0.0.0 (c (n "abscissa_tokio") (v "0.0.0") (h "0gfb66q81gwzha8a9gqkib3jmqsq9li454pdk3p4qdgngs75rfl8")))

(define-public crate-abscissa_tokio-0.5.0 (c (n "abscissa_tokio") (v "0.5.0") (d (list (d (n "abscissa_core") (r "^0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "tokio") (r "^0.2") (k 0)))) (h "0i5rd7mkrhk948ic4rbssq7z0xzvmvqwwb6pn86xiiff716mnp5i") (y #t)))

(define-public crate-abscissa_tokio-0.5.1 (c (n "abscissa_tokio") (v "0.5.1") (d (list (d (n "abscissa_core") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)))) (h "1p45x8b6l6mhz0wrk8yl4622s0kq3r4pnig942z0f2v7majx3dsh")))

(define-public crate-abscissa_tokio-0.6.0-pre.1 (c (n "abscissa_tokio") (v "0.6.0-pre.1") (d (list (d (n "abscissa_core") (r "=0.6.0-pre.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "0l3ah1d61xqwp1ml51kkpq9jylm3fc9lb2021h2ghcqmpbqdx0sl")))

(define-public crate-abscissa_tokio-0.6.0-pre.2 (c (n "abscissa_tokio") (v "0.6.0-pre.2") (d (list (d (n "abscissa_core") (r "=0.6.0-pre.1") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "1zyv5qhgymxp9jwayz8ayflg9an635xjh56yqhh456jdi3aky1l0") (f (quote (("actix" "actix-rt"))))))

(define-public crate-abscissa_tokio-0.6.0-pre.3 (c (n "abscissa_tokio") (v "0.6.0-pre.3") (d (list (d (n "abscissa_core") (r "^0.6.0-pre.2") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "1zfiw9d0ll4w24garhhi6pgkvyhdcxq2bwirfdm8a8sfkkf927p6") (f (quote (("actix" "actix-rt"))))))

(define-public crate-abscissa_tokio-0.6.0-beta.1 (c (n "abscissa_tokio") (v "0.6.0-beta.1") (d (list (d (n "abscissa_core") (r "=0.6.0-beta.1") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "04n07w9da7j487m06425x20gs8wgb62rxhyc49w4mwyshj8dyf2g") (f (quote (("actix" "actix-rt"))))))

(define-public crate-abscissa_tokio-0.6.0-rc.0 (c (n "abscissa_tokio") (v "0.6.0-rc.0") (d (list (d (n "abscissa_core") (r "=0.6.0-rc.0") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "0n4jqdwskjv78xk95yha3gf5r8riq46vkw14x9plnqxy5wkp8x6w") (f (quote (("actix" "actix-rt"))))))

(define-public crate-abscissa_tokio-0.6.0-rc.1 (c (n "abscissa_tokio") (v "0.6.0-rc.1") (d (list (d (n "abscissa_core") (r "=0.6.0-rc.1") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "1n1hb4d63b8g7zs5aby7cnjvl7270g613kvqcqh7h22x6cqkmc5q") (f (quote (("actix" "actix-rt"))))))

(define-public crate-abscissa_tokio-0.6.0 (c (n "abscissa_tokio") (v "0.6.0") (d (list (d (n "abscissa_core") (r "^0.6") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "0ghs7y59paspx32r52a3agf3h6i2lmxxvjp1q0rap84iykplikij") (f (quote (("actix" "actix-rt")))) (r "1.56")))

(define-public crate-abscissa_tokio-0.7.0 (c (n "abscissa_tokio") (v "0.7.0") (d (list (d (n "abscissa_core") (r "^0.7") (d #t) (k 0)) (d (n "actix-rt") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "0r99dnr71hm1z95j49i11s81v9vxcfnxbgi5s3rph42ymrynf2y1") (f (quote (("actix" "actix-rt")))) (r "1.60")))

