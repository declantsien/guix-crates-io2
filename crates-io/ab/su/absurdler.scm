(define-module (crates-io ab su absurdler) #:use-module (crates-io))

(define-public crate-absurdler-0.1.0 (c (n "absurdler") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "11s2w7q1rnc4rk726jipla604cfv2awvn81p1idmj03ilg1xwiyv")))

(define-public crate-absurdler-0.1.1 (c (n "absurdler") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "1qx9fwws2bgwrc9xvl7w24gq11gr0xsdqyg560yhcl2x1iz1w4p1")))

