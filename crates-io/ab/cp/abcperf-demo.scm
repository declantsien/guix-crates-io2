(define-module (crates-io ab cp abcperf-demo) #:use-module (crates-io))

(define-public crate-abcperf-demo-0.10.0 (c (n "abcperf-demo") (v "0.10.0") (d (list (d (n "abcperf") (r "^0.10.0") (d #t) (k 0)) (d (n "abcperf-generic-client") (r "^0.10.0") (d #t) (k 0)) (d (n "abcperf-minbft") (r "^0.10.0") (d #t) (k 0)) (d (n "abcperf-noop") (r "^0.10.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("max_level_debug" "release_max_level_info"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.10") (f (quote ("max_level_debug" "release_max_level_info"))) (d #t) (k 0)) (d (n "usig") (r "^0.10.0") (d #t) (k 0)) (d (n "vergen") (r "^8.0.0") (f (quote ("git" "gitcl"))) (d #t) (k 1)))) (h "0bdpnxvrb5gy6h7qny3r0p6lxx7ibhzf6dli5yl1nyibcj3cnfcp")))

