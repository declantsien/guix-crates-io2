(define-module (crates-io ab cp abcperf-client-proxy) #:use-module (crates-io))

(define-public crate-abcperf-client-proxy-0.10.0 (c (n "abcperf-client-proxy") (v "0.10.0") (d (list (d (n "abcperf") (r "^0.10.0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0") (f (quote ("serde" "rand_core" "digest"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "shared-ids") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (d #t) (k 0)) (d (n "tracing") (r "^0.1.10") (d #t) (k 0)))) (h "1jdd32pxicnsv6w64lp4r6kdj8d7zwk4hsg43dc2b1g652g8h0sz")))

