(define-module (crates-io ab cp abcperf-minbft) #:use-module (crates-io))

(define-public crate-abcperf-minbft-0.10.0 (c (n "abcperf-minbft") (v "0.10.0") (d (list (d (n "abcperf") (r "^0.10.0") (d #t) (k 0)) (d (n "abcperf-client-proxy") (r "^0.10.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "minbft") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "shared-ids") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.10") (d #t) (k 0)) (d (n "usig") (r "^0.10.0") (d #t) (k 0)))) (h "003n6cfv030lbk7qqbyggd3vgl164qc8irpsna5rn4l2f5pxmc3p")))

