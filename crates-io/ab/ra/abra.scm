(define-module (crates-io ab ra abra) #:use-module (crates-io))

(define-public crate-abra-0.0.1 (c (n "abra") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "maplit") (r "^0.1.3") (d #t) (k 0)) (d (n "roaring") (r "^0.4.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^0.1.2") (d #t) (k 0)))) (h "1xp4xr8bf96v0dkqr7bb5hhrjg53rxygg8psb6jkxp0rws1zf7y4")))

