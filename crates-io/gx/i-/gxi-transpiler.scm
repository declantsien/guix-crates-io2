(define-module (crates-io gx i- gxi-transpiler) #:use-module (crates-io))

(define-public crate-gxi-transpiler-0.4.0 (c (n "gxi-transpiler") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1g9dplgv3y2ndsm7xq55rbix595xhf559wz55dqr6ccpghj15y28") (f (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-transpiler-0.5.0 (c (n "gxi-transpiler") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0l8mylps2c0hv532zs029n5lagrwk6svs04mq1wx4c86ibpkd6z6") (f (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-transpiler-0.6.0 (c (n "gxi-transpiler") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "08sib21pgb6nc6ikhcax1bda6pq04sbpijvq5jlp2r2bgvrif0fa") (f (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-transpiler-0.6.1 (c (n "gxi-transpiler") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0g2f5yxbvb45lfc98s8a88m3hfmy5agizqfhh6vmcj76pwx5pyds") (f (quote (("web") ("desktop") ("default"))))))

