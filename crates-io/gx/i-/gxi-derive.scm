(define-module (crates-io gx i- gxi-derive) #:use-module (crates-io))

(define-public crate-gxi-derive-0.4.0 (c (n "gxi-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0vv00lxm58qzggjziycd3brjbpnxrzyrq95gy7im5hs763x9aydm") (f (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-derive-0.5.0 (c (n "gxi-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1las986axh9ks2wdhc643ac00af22z1ysn0jmfm9j011al0r87d3") (f (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-derive-0.6.0 (c (n "gxi-derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0v50vxsq7cxd4pxgzgxsvwgn2nb26g6irkq6jxph0zzlsmfr8023") (f (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-derive-0.6.1 (c (n "gxi-derive") (v "0.6.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "0vd55ycac75s62n90v98zf6samrifdk8cwkw4vnp341aqq56n172") (f (quote (("web") ("desktop") ("default"))))))

