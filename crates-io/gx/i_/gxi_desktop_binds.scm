(define-module (crates-io gx i_ gxi_desktop_binds) #:use-module (crates-io))

(define-public crate-gxi_desktop_binds-0.1.0 (c (n "gxi_desktop_binds") (v "0.1.0") (d (list (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("desktop"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("sync" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0bszxgw1xbcncv1vissx6a1m95r5s6i7z95hd0wv5rnnj31s6lnv")))

(define-public crate-gxi_desktop_binds-0.1.1 (c (n "gxi_desktop_binds") (v "0.1.1") (d (list (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("desktop"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("sync" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "11a3fd3qmgk8ahwjikwja7xf73dhhc0jnci1msm40hb308v8p3nf")))

(define-public crate-gxi_desktop_binds-0.1.2 (c (n "gxi_desktop_binds") (v "0.1.2") (d (list (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("desktop"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("sync" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1f7n3jmr0zsj82x4bq0kf2gfy092bc2vsn5d1g6x2agqs35yp99f")))

(define-public crate-gxi_desktop_binds-0.1.3 (c (n "gxi_desktop_binds") (v "0.1.3") (d (list (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("desktop"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("sync" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0asiwssjc7y1r3c9xkx5jg26r98b9h154rkbdr2jxvmrn5nykgn0")))

(define-public crate-gxi_desktop_binds-0.1.4 (c (n "gxi_desktop_binds") (v "0.1.4") (d (list (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("desktop"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5") (f (quote ("sync" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "09ml9avfcci4vr8qc8a723najf3zvli82yg6qwqiznfl41gv7gac")))

