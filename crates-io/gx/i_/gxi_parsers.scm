(define-module (crates-io gx i_ gxi_parsers) #:use-module (crates-io))

(define-public crate-gxi_parsers-0.1.0 (c (n "gxi_parsers") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08ill6q6qvg94lypbv5w17yyw26f29x1p4xq0rcqkplic56nxqmi") (f (quote (("web") ("desktop"))))))

(define-public crate-gxi_parsers-0.1.1 (c (n "gxi_parsers") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16jqc42qfgq883dvfg64pbpwmiz813afrvgl82nlrk9alwjajbmm") (f (quote (("web") ("desktop"))))))

(define-public crate-gxi_parsers-0.1.2 (c (n "gxi_parsers") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1szmj8q6ca61dp40r53rdgxkkbvqzjcwmpqxcz3r5c21rm6rf3md") (f (quote (("web") ("desktop"))))))

(define-public crate-gxi_parsers-0.1.3 (c (n "gxi_parsers") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "052rwb3p1n5ba7452i01sm1j20dvhgnnpfrxnpjrmp0gi3jfimmj") (f (quote (("web") ("desktop"))))))

