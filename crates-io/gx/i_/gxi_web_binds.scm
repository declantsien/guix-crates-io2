(define-module (crates-io gx i_ gxi_web_binds) #:use-module (crates-io))

(define-public crate-gxi_web_binds-0.1.0 (c (n "gxi_web_binds") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("web"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.23") (d #t) (k 0)))) (h "1dzrz4g0rwfdf8b24fxinqqxgjnyn6a4mb7l0mxz3xrlgvr0wa1r")))

(define-public crate-gxi_web_binds-0.1.1 (c (n "gxi_web_binds") (v "0.1.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("web"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.23") (d #t) (k 0)))) (h "0qq5g9jcf437jrr78z51dknqg9vnzzxdcqnv5gcj0xymc70z3by9")))

(define-public crate-gxi_web_binds-0.1.2 (c (n "gxi_web_binds") (v "0.1.2") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("web"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.23") (d #t) (k 0)))) (h "1mll8cwm64f0nr07b29g0vscy1vc1sm6k7k414lknh4bmrgp8zjk")))

(define-public crate-gxi_web_binds-0.1.3 (c (n "gxi_web_binds") (v "0.1.3") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("web"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.23") (d #t) (k 0)))) (h "1qskmsiay01bv9asm9fpr2ckbgjhj01wl0s19zxw6nqw8m9bfwl8")))

(define-public crate-gxi_web_binds-0.1.4 (c (n "gxi_web_binds") (v "0.1.4") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("web"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.23") (d #t) (k 0)))) (h "1214g30x2420szyalvp7rvbp801ibd80vg9wayj2ahnpx34c2vgf")))

(define-public crate-gxi_web_binds-0.1.5 (c (n "gxi_web_binds") (v "0.1.5") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (k 0)) (d (n "gxi_interface") (r "^0") (f (quote ("web"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.23") (d #t) (k 0)))) (h "0nxzgg5rj41h7mzxpqx7dbysgckwjbn0d8p1shkx2fx2j8abaq9w")))

