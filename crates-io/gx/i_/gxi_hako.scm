(define-module (crates-io gx i_ gxi_hako) #:use-module (crates-io))

(define-public crate-gxi_hako-0.1.0 (c (n "gxi_hako") (v "0.1.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "opencv") (r "^0.91.1") (d #t) (k 0)))) (h "1k01wy2chp885fyjy3plqh5birn10hhly9vpifhzgslf9422zkgp") (y #t) (r "1.77.2")))

(define-public crate-gxi_hako-0.1.1 (c (n "gxi_hako") (v "0.1.1") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "opencv") (r "^0.91.1") (d #t) (k 0)))) (h "1kg1idcaalsrbq70sa4zn7c9i8svq1f6v62r2wzzq56vvd9jh2fa") (y #t) (r "1.77.2")))

(define-public crate-gxi_hako-0.1.2 (c (n "gxi_hako") (v "0.1.2") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "opencv") (r "^0.91.1") (d #t) (k 0)))) (h "0mzx2lw19jrw73dw8sqxv5q7q28bfxvrg9f58r2j4887wjwm1iqp") (r "1.77.0")))

