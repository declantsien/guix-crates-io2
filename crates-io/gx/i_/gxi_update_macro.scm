(define-module (crates-io gx i_ gxi_update_macro) #:use-module (crates-io))

(define-public crate-gxi_update_macro-0.1.0 (c (n "gxi_update_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "076ik8q450mf0qrrmfwlfy7fi3q9bqp5k7vywcc6qp54dyjpv3wz") (f (quote (("web") ("desktop")))) (y #t)))

(define-public crate-gxi_update_macro-0.1.1 (c (n "gxi_update_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "059kch15qx3i6kk2dyi2pdhg6r8s2arh63s1vdhimm5w14jj0hfd") (f (quote (("web") ("desktop")))) (y #t)))

