(define-module (crates-io gx i_ gxi_comp_macro) #:use-module (crates-io))

(define-public crate-gxi_comp_macro-0.1.0 (c (n "gxi_comp_macro") (v "0.1.0") (d (list (d (n "gxi_parsers") (r "^0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03a4cqlqb5xgmb0x78hn6v007kahf3gbsd2qv9bw5ai1sr7qmi8z") (f (quote (("web" "gxi_parsers/web") ("desktop" "gxi_parsers/desktop")))) (y #t)))

