(define-module (crates-io gx rf gxrfetch) #:use-module (crates-io))

(define-public crate-gxrfetch-0.8.0 (c (n "gxrfetch") (v "0.8.0") (d (list (d (n "fsi") (r "^0.1.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0cg0ld0frark6shaggaja5qzg736q1h1wqq7vn1n79n9wg62wylr")))

(define-public crate-gxrfetch-0.8.1 (c (n "gxrfetch") (v "0.8.1") (d (list (d (n "fsi") (r "^0.1.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nixinfo") (r "^0.3.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.10") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "03hn44bpddlq0sbd798vji3y9xm8fkbzglqj4r0yvl75hhxqmc2s")))

