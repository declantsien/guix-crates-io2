(define-module (crates-io gx f2 gxf2chrom) #:use-module (crates-io))

(define-public crate-gxf2chrom-0.1.0 (c (n "gxf2chrom") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_cpus") (r ">1.14") (d #t) (k 0)) (d (n "rayon") (r ">1.7") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02cr5bkib7bh6w8zbs6987ifqcswqn336pl4v7hsxdylcm3gihcy")))

