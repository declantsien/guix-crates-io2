(define-module (crates-io h_ ti h_time) #:use-module (crates-io))

(define-public crate-h_time-0.1.0 (c (n "h_time") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0iyldgg9r8yks874znhzpjpsl3syals6xrc1c48dy29j2n98q2rq")))

