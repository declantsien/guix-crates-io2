(define-module (crates-io h_ en h_encrypt) #:use-module (crates-io))

(define-public crate-h_encrypt-0.1.0 (c (n "h_encrypt") (v "0.1.0") (h "19f64q8bpv7qvk1i0c1qzhffng7ihy6c8as188bjp2lm28k6c4wx")))

(define-public crate-h_encrypt-0.1.1 (c (n "h_encrypt") (v "0.1.1") (h "06nj0sj6m4lhh9hnlf3jmjf4ngqnazwgxlkim8cy4iiaqx0clcbl")))

(define-public crate-h_encrypt-0.2.0 (c (n "h_encrypt") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0kkldq9jdj0bz7j79yqnw7ibl978g3ajv7hykmkghcvrrmaacd2v")))

(define-public crate-h_encrypt-0.2.1 (c (n "h_encrypt") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0w2fk893b3vwqxd56vpq6a6wp24cwahfr7glpk13gqxx071i5i6v")))

(define-public crate-h_encrypt-0.2.2 (c (n "h_encrypt") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1x0p96dlgl19r8racrhza8brivngc1hn1mpbrm8gzswy58qy3jpd")))

(define-public crate-h_encrypt-0.2.3 (c (n "h_encrypt") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1xyfdclxs5cids56myxy64wxvnh0iwzbnp33n52746pfzxk9xjna")))

