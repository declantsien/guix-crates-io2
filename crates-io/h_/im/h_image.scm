(define-module (crates-io h_ im h_image) #:use-module (crates-io))

(define-public crate-h_image-0.1.0 (c (n "h_image") (v "0.1.0") (d (list (d (n "font-kit") (r "^0.11") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "pathfinder_geometry") (r "^0.5") (d #t) (k 0)))) (h "1lvzi4rjapslbixcssidvy1wk0pnid9sn52gss7k375f64qba2ij")))

