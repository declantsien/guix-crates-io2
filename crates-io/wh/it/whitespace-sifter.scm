(define-module (crates-io wh it whitespace-sifter) #:use-module (crates-io))

(define-public crate-whitespace-sifter-0.1.0 (c (n "whitespace-sifter") (v "0.1.0") (h "0sh16br4jy4zc0p90nqkjaa8f7a9y5cnq3r8ymg14z517mxqp39j")))

(define-public crate-whitespace-sifter-0.1.1 (c (n "whitespace-sifter") (v "0.1.1") (h "0qazfaa9pn8490kb3ba77rryjx1j0j5yy7aghar7n7gkh06l9ld1")))

(define-public crate-whitespace-sifter-0.2.0 (c (n "whitespace-sifter") (v "0.2.0") (h "1xj4324asgdnh65n32cvfghz0ydqzq12d77x20w4kx0d3vdzrjjw") (f (quote (("preserve_newline") ("default" "preserve_newline"))))))

(define-public crate-whitespace-sifter-0.2.1 (c (n "whitespace-sifter") (v "0.2.1") (h "1lsddp3g8ra3pn5j07fyis3hn7ab2n6wv401w3jrfjll7w5a696l") (f (quote (("preserve_newline") ("default" "preserve_newline"))))))

(define-public crate-whitespace-sifter-0.2.2 (c (n "whitespace-sifter") (v "0.2.2") (h "1k6537a12sf7viib8waa8c0waspl7gr6bcfx5i6wdqi1mzjx7w8h") (f (quote (("preserve_newline") ("default" "preserve_newline"))))))

(define-public crate-whitespace-sifter-0.3.0 (c (n "whitespace-sifter") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "144z7syardn0pjpcf5jmhmlgzmj3hsrzh38wwq8wq8b2c3yr589v") (f (quote (("preserve_newline") ("default" "preserve_newline"))))))

(define-public crate-whitespace-sifter-1.0.0 (c (n "whitespace-sifter") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0pk1vl8n7d9fl766qppqa4ly196ax1kpbzs6ahrcdp3j19xj2b15") (f (quote (("preserve_newline") ("default" "preserve_newline")))) (y #t)))

(define-public crate-whitespace-sifter-1.0.1 (c (n "whitespace-sifter") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1x2fbvwqwqwp15pk159r6b997pbd7l3g7ckxi7g5m7i7yf7rcrav") (f (quote (("preserve_newline") ("default" "preserve_newline"))))))

