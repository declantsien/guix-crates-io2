(define-module (crates-io wh it whitespace_text_steganography) #:use-module (crates-io))

(define-public crate-whitespace_text_steganography-0.1.0 (c (n "whitespace_text_steganography") (v "0.1.0") (h "13fxgyf4b5d1cx28kznava9rpmxywffx5lzqiikbmfnvr0r2fgqb")))

(define-public crate-whitespace_text_steganography-0.1.1 (c (n "whitespace_text_steganography") (v "0.1.1") (h "11rxb18ypwwklvznp9934y7235ahkxj9cv53w87fxirfs64z4c1h")))

(define-public crate-whitespace_text_steganography-0.1.2 (c (n "whitespace_text_steganography") (v "0.1.2") (h "1izy089f1qcyssvvpym4p2l8zavii62ic0r9bqdybaa5cid5yg4i")))

(define-public crate-whitespace_text_steganography-0.1.3 (c (n "whitespace_text_steganography") (v "0.1.3") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0kmhawmll4w9xsy8x5zq8jxx2zbklndb1h3mqcdxf0xsjddk5xzp")))

(define-public crate-whitespace_text_steganography-0.2.0 (c (n "whitespace_text_steganography") (v "0.2.0") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "1gf00h7mpawp5ib1ryh9l3r2gzdq7cbb4asmpag2f48ajx3mrvrk")))

(define-public crate-whitespace_text_steganography-0.2.1 (c (n "whitespace_text_steganography") (v "0.2.1") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "15sk4qkp6pv2ciw31h7p626mwa5waapvh5v57nhgry2k6nqr5yb9")))

