(define-module (crates-io wh it white_rabbit) #:use-module (crates-io))

(define-public crate-white_rabbit-0.1.0 (c (n "white_rabbit") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0m610srmrqx18zw40b9qxpb3liakli8b3m42z93xqhmzirqy27s4")))

(define-public crate-white_rabbit-0.1.1 (c (n "white_rabbit") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0yzz9nby6p5cd2k5jyz45w8dlpni2q12v0vd5zggh2qxlfh1wy35")))

