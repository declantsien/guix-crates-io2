(define-module (crates-io wh it whitespace) #:use-module (crates-io))

(define-public crate-whitespace-1.0.0 (c (n "whitespace") (v "1.0.0") (h "02v8lqsvxn1jwbn5l0rgwwzdh2dj3cw5sgzs46iyzkq15ncjilya")))

(define-public crate-whitespace-2.0.0 (c (n "whitespace") (v "2.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d078sz3cwlhlc5adg4g1430d80gxzkqjsdwxq7fqzpn6bwx8gq7")))

