(define-module (crates-io wh it whiteout) #:use-module (crates-io))

(define-public crate-whiteout-0.1.0 (c (n "whiteout") (v "0.1.0") (h "184pivnnvmrzasf4mqlkpr4ai358hkrc29nycsl6bn30hnfnsz5k")))

(define-public crate-whiteout-1.0.0 (c (n "whiteout") (v "1.0.0") (h "0gvfpq9dm93j7p9fddp8qizbi31yfky0193cd13x7fdk6nn90s7m")))

(define-public crate-whiteout-1.0.1 (c (n "whiteout") (v "1.0.1") (h "1npdxwzyj1ygz0cr8dlbvvka54i2drhr2rvk9al0hxd76ns4n8aj")))

