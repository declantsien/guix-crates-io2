(define-module (crates-io wh it whitespace-rs) #:use-module (crates-io))

(define-public crate-whitespace-rs-1.0.0 (c (n "whitespace-rs") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 0)))) (h "0a1j0j8m5p8h8a0rfy5w09hpvxn98ac6zhw6mikprkz5wkaxhagz") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-whitespace-rs-1.0.1 (c (n "whitespace-rs") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 0)))) (h "1dszb3vn4bgax0fz1sg6hshmf9wf9axbbx2658zcmryfrfspzf39") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-whitespace-rs-2.0.0 (c (n "whitespace-rs") (v "2.0.0") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 0)))) (h "0a4z9b5id6n6a1l4dsmq12skmvcjav9bs0q4pczn0729nya1m746") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-whitespace-rs-2.1.0 (c (n "whitespace-rs") (v "2.1.0") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 0)))) (h "03j2n5czzrfmgjjycv3vpljp35j7c7vghj0gxl2aq5nmkjanwd9m") (f (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-whitespace-rs-2.1.2 (c (n "whitespace-rs") (v "2.1.2") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "utf8-decode") (r "^1.0") (d #t) (k 0)))) (h "1z5zdbq90qvln1cg5jiba8ifzl1m6hdadl9h7a5q3q8d2kif0bwk") (f (quote (("default" "cli") ("cli" "clap"))))))

