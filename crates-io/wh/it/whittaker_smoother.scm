(define-module (crates-io wh it whittaker_smoother) #:use-module (crates-io))

(define-public crate-whittaker_smoother-0.1.0 (c (n "whittaker_smoother") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0g6s3iji44lh48gsa9jg2s1gkdzbf6w8yn72l9z5plbfl70cwh59")))

