(define-module (crates-io wh it whitepool) #:use-module (crates-io))

(define-public crate-whitepool-1.0.0 (c (n "whitepool") (v "1.0.0") (d (list (d (n "tokio") (r "^1.18.0") (f (quote ("rt-multi-thread" "macros" "time" "sync"))) (d #t) (k 0)))) (h "1ksval3gdbzkp0jj7ncf192n1cha4cklvh4yvhwjg3ghhavily5x")))

