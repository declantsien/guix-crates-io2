(define-module (crates-io wh it white-balancer) #:use-module (crates-io))

(define-public crate-white-balancer-0.1.0 (c (n "white-balancer") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23.1") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "14yv4qwl089wb59pp6hwjrjsxp60ralvpsz7wf4s7cyk1xa6fahj")))

(define-public crate-white-balancer-0.2.0 (c (n "white-balancer") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23.1") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0fgg682hz08a1x41zffm9rpylpg3s5d5bcixdga1fi0hm63mmzhx")))

(define-public crate-white-balancer-0.3.0 (c (n "white-balancer") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23.1") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1qlfzafqjk5sgjbcfz2fjmzbj4059kilvc0i0xa5xyp1bvry2hxz")))

(define-public crate-white-balancer-0.4.0 (c (n "white-balancer") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23.1") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)))) (h "186f0qs2ki2il8v83fgbxqm9kayd8prc5fyj929x2c5h75gqrdsx")))

