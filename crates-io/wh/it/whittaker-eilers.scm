(define-module (crates-io wh it whittaker-eilers) #:use-module (crates-io))

(define-public crate-whittaker-eilers-0.1.0 (c (n "whittaker-eilers") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "plotly") (r "^0.8.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "sprs") (r "^0.11.1") (d #t) (k 0)) (d (n "sprs-ldl") (r "^0.10.0") (d #t) (k 0)))) (h "1hbldx4n9jvxm8rkz56khg04dxz088y8ydnvnbqnbbm176biw9gm")))

(define-public crate-whittaker-eilers-0.1.1 (c (n "whittaker-eilers") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "plotly") (r "^0.8.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "sprs") (r "^0.11.1") (d #t) (k 0)) (d (n "sprs-ldl") (r "^0.10.0") (d #t) (k 0)))) (h "1k495blgbx5v0yid0paf5hpcr7yvm3f9py2wk9bsgs30v3li3dxz")))

(define-public crate-whittaker-eilers-0.1.3 (c (n "whittaker-eilers") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "plotly") (r "^0.8.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "sprs") (r "^0.11.1") (d #t) (k 0)) (d (n "sprs-ldl") (r "^0.10.0") (d #t) (k 0)))) (h "03blij6dklz2nmrpn2kz8y727gzqbg4wvx4yvglq60mlrqwxinx9")))

