(define-module (crates-io wh it whiteread) #:use-module (crates-io))

(define-public crate-whiteread-0.1.0 (c (n "whiteread") (v "0.1.0") (h "1z2cjwa71a5inygcjl1mpazxmyi6zhri3zihh2k756xbzai830g4")))

(define-public crate-whiteread-0.2.0 (c (n "whiteread") (v "0.2.0") (h "1nmqz76jql01qmyl3c5yxm6n0wrvzmy90xi070mm2w1ri8r4a17l")))

(define-public crate-whiteread-0.3.0 (c (n "whiteread") (v "0.3.0") (h "1wdd3mvff2qx76c4sggxdiv1wvgi8ndyl500vj3y5irfnvwq5xq2")))

(define-public crate-whiteread-0.4.0 (c (n "whiteread") (v "0.4.0") (d (list (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "1ykkl5cz8nfdkhjpjg80vmz5wq3w2qxmfvskyldarcm72ldpfn8c") (y #t)))

(define-public crate-whiteread-0.4.1 (c (n "whiteread") (v "0.4.1") (d (list (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "10h8v4qrlids43zflkcgif21ayjcb4faqk81x89qkfisl3k228n9") (y #t)))

(define-public crate-whiteread-0.4.2 (c (n "whiteread") (v "0.4.2") (d (list (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "0d60cnz3mfp2dpqapwsrl9ngs6m681sd7pwhf091py0dggnxzc83")))

(define-public crate-whiteread-0.4.3 (c (n "whiteread") (v "0.4.3") (d (list (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "10l6j5212alvnr8gxps4s92nf075nw9mf1hp4hr51yr28j3m9nac")))

(define-public crate-whiteread-0.4.4 (c (n "whiteread") (v "0.4.4") (d (list (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "0x20zkf3pmmvknwkyll0rcdvmyg9cfvv7j9qzajjmmalxvyn7y84")))

(define-public crate-whiteread-0.5.0 (c (n "whiteread") (v "0.5.0") (d (list (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "1cqljwpm81j90vybh1vrxlb35v7pgljsf5xmlqi56xb8m7h5vhlb")))

