(define-module (crates-io wh it whitewash) #:use-module (crates-io))

(define-public crate-whitewash-0.2.0 (c (n "whitewash") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "rexiv2") (r "^0.8.0") (d #t) (k 0)))) (h "1204rfwlyf36ba05lkjmj860fkill565jgrlqnp7wyq7ljqas407")))

