(define-module (crates-io wh it whittaker_map_generator) #:use-module (crates-io))

(define-public crate-whittaker_map_generator-0.1.0 (c (n "whittaker_map_generator") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.3") (d #t) (k 0)))) (h "0r5gg23dm8ik0h28gc8vj381p5h1lg7467kgiv8i8kyn5pynm46s")))

(define-public crate-whittaker_map_generator-0.1.1 (c (n "whittaker_map_generator") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.3") (d #t) (k 0)))) (h "0m6vpigs4x6i778jjnys601p6dadblkbrq87qd7zhb45srxx7vsa")))

