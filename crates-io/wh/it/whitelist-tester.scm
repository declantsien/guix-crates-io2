(define-module (crates-io wh it whitelist-tester) #:use-module (crates-io))

(define-public crate-whitelist-tester-0.1.8 (c (n "whitelist-tester") (v "0.1.8") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "locked-voter") (r "^0.1") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1r3r4kwss18v9gcqv40qn5jbij9qlxna3g7gvsvmhpgyv4jxr7n4") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.1.9 (c (n "whitelist-tester") (v "0.1.9") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "locked-voter") (r "^0.1") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1zi7nw7sm0h5mcm2l038cwkip8q4vh33kgh92jcvqcsyfhlv41fr") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.1.10 (c (n "whitelist-tester") (v "0.1.10") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "locked-voter") (r "^0.1") (f (quote ("cpi"))) (d #t) (k 0)))) (h "00palgbjs28hyz80r89vajjq6rl35w7k6m99hbz70czkx7199dl5") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.1.11 (c (n "whitelist-tester") (v "0.1.11") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "locked-voter") (r "^0.1") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1b67ajcgardgkb8n121xgcvdd3l1jx8pg8r666bff8g53kn5436j") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.3.0 (c (n "whitelist-tester") (v "0.3.0") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "locked-voter") (r "^0.3") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1pmx0ba82ldky8nq9j0src213mddanbagqfbg7a9qnpkdgrnpq03") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.3.2 (c (n "whitelist-tester") (v "0.3.2") (d (list (d (n "anchor-lang") (r ">=0.17") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.17") (d #t) (k 0)) (d (n "locked-voter") (r "^0.3") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0mxg7sy8aqpaj40iw8qpn905l2v32ksqg78v00gbn2dzz2isf3jz") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.4.0 (c (n "whitelist-tester") (v "0.4.0") (d (list (d (n "anchor-lang") (r ">=0.21") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.21") (d #t) (k 0)) (d (n "locked-voter") (r "^0.4") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1rl3vn6cxbgr3syyj263r9za22m4g752bc3jqki6vlxdx18b9zfm") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.5.1 (c (n "whitelist-tester") (v "0.5.1") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "locked-voter") (r "^0.5") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0lzzj7wmghjx4v137p15h6zc650f4pvm2993spshfjx4znxbfx8p") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.5.3 (c (n "whitelist-tester") (v "0.5.3") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "locked-voter") (r "^0.5") (f (quote ("cpi"))) (d #t) (k 0)))) (h "19zz3ic1cf45qnz3f6kmzbma0hjmxmqlwpl6i8daxdm0pvkwqqwz") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.5.4 (c (n "whitelist-tester") (v "0.5.4") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "locked-voter") (r "^0.5") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1aj87wkxqga4zyfs92zi6zn3ma13k11zqz4yk4f8xhsbn020697p") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.5.5 (c (n "whitelist-tester") (v "0.5.5") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "locked-voter") (r "^0.5") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0lvsc3gixbbccyd1w8xs3a1rq1psldhcqpml5165gc77lrvvzl5i") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-whitelist-tester-0.5.6 (c (n "whitelist-tester") (v "0.5.6") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "locked-voter") (r "^0.5") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0b4j3a2x3zfsrdngqf78swzsyh5sf9d8l29qi4qrlyngqxdqb93i") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

