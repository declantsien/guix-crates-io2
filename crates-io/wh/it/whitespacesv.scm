(define-module (crates-io wh it whitespacesv) #:use-module (crates-io))

(define-public crate-whitespacesv-0.1.0 (c (n "whitespacesv") (v "0.1.0") (h "017yxk848n15md3721nbzzx0y07m8xwsfa94kyy681gilkndbim0")))

(define-public crate-whitespacesv-0.1.1 (c (n "whitespacesv") (v "0.1.1") (h "1zvcxj882avvf8mc5aqyjx8i330z53ka70spfq698xcj1cmldydj")))

(define-public crate-whitespacesv-0.1.2 (c (n "whitespacesv") (v "0.1.2") (h "0srh7q8pdg57817rmwhyjdsx3sad7406anq3ncaqpqi9ns0azxd4")))

(define-public crate-whitespacesv-0.1.3 (c (n "whitespacesv") (v "0.1.3") (h "0x609n56s3lfml1s5lf3p5cpvilhki61406pzz2by6jrj68qik5x")))

(define-public crate-whitespacesv-0.2.0 (c (n "whitespacesv") (v "0.2.0") (h "0gk72cp2hz7lv49g1rknrrvivqf1xafvazn18gja95vh981mhfdz")))

(define-public crate-whitespacesv-1.0.0 (c (n "whitespacesv") (v "1.0.0") (d (list (d (n "utf8-chars") (r "^3.0.1") (d #t) (k 2)))) (h "1w0m6s3x0gpa14fz66cgvq5ysv0vzbxb23mrcir3w84ajw0q1z5j")))

(define-public crate-whitespacesv-1.0.1 (c (n "whitespacesv") (v "1.0.1") (d (list (d (n "utf8-chars") (r "^3.0.1") (d #t) (k 2)))) (h "02vfd05yvvrff051iz8ymf0q8l99j1k5cskbh7lxnpd3navxkz3x")))

(define-public crate-whitespacesv-1.0.2 (c (n "whitespacesv") (v "1.0.2") (d (list (d (n "utf8-chars") (r "^3.0.1") (d #t) (k 2)))) (h "07z13d000ci4iyh4f8xg6ppvfgzanq4q5dxd7cc93layp15ghizj")))

