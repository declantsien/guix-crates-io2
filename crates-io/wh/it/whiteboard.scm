(define-module (crates-io wh it whiteboard) #:use-module (crates-io))

(define-public crate-whiteboard-0.0.1 (c (n "whiteboard") (v "0.0.1") (h "0gwh0zriw2y38ynn31328wz0pq64ya0wycpy0qpwv3abm3sl9q3f")))

(define-public crate-whiteboard-0.0.2 (c (n "whiteboard") (v "0.0.2") (h "1rm6650p5q0lc4n2idkp14vcj16k00ww3yxj0f310f92rfk72j22")))

