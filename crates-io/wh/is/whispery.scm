(define-module (crates-io wh is whispery) #:use-module (crates-io))

(define-public crate-whispery-0.0.0 (c (n "whispery") (v "0.0.0") (h "0ahv062jf90s0f5pnl86aq0crqdicgg3ya2678bh35rxacvfb7s5") (y #t)))

(define-public crate-whispery-0.0.1 (c (n "whispery") (v "0.0.1") (h "0h5vsx3vp2pp98vmlkh40phkqcr33rgqhi3miiwa2gm0jfg13b4c")))

