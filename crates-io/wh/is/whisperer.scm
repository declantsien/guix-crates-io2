(define-module (crates-io wh is whisperer) #:use-module (crates-io))

(define-public crate-whisperer-0.0.7 (c (n "whisperer") (v "0.0.7") (d (list (d (n "blake3") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "0ypmppfc1mr4ydllbi9fp110xgbyd3hmxakshgpl8f1a58cd5g7k")))

