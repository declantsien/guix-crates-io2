(define-module (crates-io wh is whisper-rs) #:use-module (crates-io))

(define-public crate-whisper-rs-0.1.0 (c (n "whisper-rs") (v "0.1.0") (d (list (d (n "whisper-rs-sys") (r "^0.1") (d #t) (k 0)))) (h "1gggjnrr1kcsjqrcbmzbqsv6xa33nadyq6dvjgrwwg549790zhsa") (f (quote (("simd"))))))

(define-public crate-whisper-rs-0.1.1 (c (n "whisper-rs") (v "0.1.1") (d (list (d (n "whisper-rs-sys") (r "^0.1") (d #t) (k 0)))) (h "0d1pm2kbz6c1pp8l11ik27k06g6300wgx96rm8ywz4scr7d1j6yc") (f (quote (("simd"))))))

(define-public crate-whisper-rs-0.1.2 (c (n "whisper-rs") (v "0.1.2") (d (list (d (n "whisper-rs-sys") (r "^0.1") (d #t) (k 0)))) (h "0svnvi8xarg5lx778hxpnwd1gcwkjj5ip7q9s3z4swag86h66jxv") (f (quote (("simd"))))))

(define-public crate-whisper-rs-0.1.3 (c (n "whisper-rs") (v "0.1.3") (d (list (d (n "whisper-rs-sys") (r "^0.1") (d #t) (k 0)))) (h "0k7q31wq9nsf5bgyh3r813vjl3sqz01cwmijflqrhpsh287h2ski") (f (quote (("simd"))))))

(define-public crate-whisper-rs-0.2.0 (c (n "whisper-rs") (v "0.2.0") (d (list (d (n "whisper-rs-sys") (r "^0.2") (d #t) (k 0)))) (h "0sknras2shavjqp73b108rkh5a4jq9lrff19znjb4lwc2h7kww7r") (f (quote (("simd"))))))

(define-public crate-whisper-rs-0.3.0 (c (n "whisper-rs") (v "0.3.0") (d (list (d (n "whisper-rs-sys") (r "^0.2") (d #t) (k 0)))) (h "1xbyvkfw0kwalspamr07hmzwsfi8s2g912vkw5c3fjgyda54apvn") (f (quote (("simd"))))))

(define-public crate-whisper-rs-0.4.0 (c (n "whisper-rs") (v "0.4.0") (d (list (d (n "whisper-rs-sys") (r "^0.3") (d #t) (k 0)))) (h "07yhk35p4qjhjv96mympqjcm05ckld6ms5d2y0g4map8hhdn8k8j") (f (quote (("simd"))))))

(define-public crate-whisper-rs-0.5.0 (c (n "whisper-rs") (v "0.5.0") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 2)) (d (n "whisper-rs-sys") (r "^0.3") (d #t) (k 0)))) (h "0xqn79qly6i5hy2y66zd3kas2si6csaj2sa41rd2i8rs02dinzps") (f (quote (("simd"))))))

(define-public crate-whisper-rs-0.6.0 (c (n "whisper-rs") (v "0.6.0") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "hound") (r "^3.5.0") (d #t) (k 2)) (d (n "whisper-rs-sys") (r "^0.4") (d #t) (k 0)))) (h "1fn937dp096mvn0zzqn3hi3j9l8r20k72wpd2ap8fvfzmkla7ads") (f (quote (("simd"))))))

(define-public crate-whisper-rs-0.7.0 (c (n "whisper-rs") (v "0.7.0") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 2)) (d (n "whisper-rs-sys") (r "^0.5") (d #t) (k 0)))) (h "01bxdcq1v3x7h9fmlascza4l7dlrxz1ylnpgnkr1bcpyznfzc9x0") (f (quote (("test-with-tiny-model") ("simd") ("coreml" "whisper-rs-sys/coreml"))))))

(define-public crate-whisper-rs-0.8.0 (c (n "whisper-rs") (v "0.8.0") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 2)) (d (n "whisper-rs-sys") (r "^0.6") (d #t) (k 0)))) (h "0f869b3axi0gxdqk633lpqh61v819pyqhnv1p99v0mnmiaqhz59c") (f (quote (("test-with-tiny-model") ("simd") ("opencl" "whisper-rs-sys/opencl") ("default") ("cuda" "whisper-rs-sys/cuda") ("coreml" "whisper-rs-sys/coreml"))))))

(define-public crate-whisper-rs-0.9.0-rc.1 (c (n "whisper-rs") (v "0.9.0-rc.1") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 2)) (d (n "whisper-rs-sys") (r "^0.7") (d #t) (k 0)))) (h "189mjh53c142hfrgd03b9rsrj41lmcqxhhjk8rj4hph4bp7r22px") (f (quote (("test-with-tiny-model") ("simd") ("opencl" "whisper-rs-sys/opencl") ("default") ("cuda" "whisper-rs-sys/cuda") ("coreml" "whisper-rs-sys/coreml"))))))

(define-public crate-whisper-rs-0.9.0-rc.2 (c (n "whisper-rs") (v "0.9.0-rc.2") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 2)) (d (n "whisper-rs-sys") (r "^0.7") (d #t) (k 0)))) (h "1l2qp5lm09w6lad0c2ndn2p9pdzhcxn3w9cd8x850zszd1fcqb7s") (f (quote (("test-with-tiny-model") ("simd") ("opencl" "whisper-rs-sys/opencl") ("openblas" "whisper-rs-sys/openblas") ("default") ("cuda" "whisper-rs-sys/cuda") ("coreml" "whisper-rs-sys/coreml"))))))

(define-public crate-whisper-rs-0.10.0 (c (n "whisper-rs") (v "0.10.0") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 2)) (d (n "whisper-rs-sys") (r "^0.8") (d #t) (k 0)))) (h "08sy6q3fflyfblykqcfxy0k767g6gg3cpf69bk0nyx73cv69rq9q") (f (quote (("test-with-tiny-model") ("simd") ("opencl" "whisper-rs-sys/opencl") ("openblas" "whisper-rs-sys/openblas") ("metal" "whisper-rs-sys/metal" "_gpu") ("default") ("cuda" "whisper-rs-sys/cuda" "_gpu") ("coreml" "whisper-rs-sys/coreml") ("_gpu"))))))

(define-public crate-whisper-rs-0.11.0 (c (n "whisper-rs") (v "0.11.0") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "whisper-rs-sys") (r "^0.9") (d #t) (k 0)))) (h "01jv8wc56ig6gfgbhg0yx5qpwp1xjvynkqx1la0il190h5snkwg9") (f (quote (("test-with-tiny-model") ("raw-api") ("opencl" "whisper-rs-sys/opencl") ("openblas" "whisper-rs-sys/openblas") ("metal" "whisper-rs-sys/metal" "_gpu") ("default") ("cuda" "whisper-rs-sys/cuda" "_gpu") ("coreml" "whisper-rs-sys/coreml") ("_gpu")))) (s 2) (e (quote (("whisper-cpp-tracing" "dep:tracing") ("whisper-cpp-log" "dep:log"))))))

(define-public crate-whisper-rs-0.11.1 (c (n "whisper-rs") (v "0.11.1") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "whisper-rs-sys") (r "^0.9") (d #t) (k 0)))) (h "0qfilwhrvfz94ai8kp3ixjnpb5qmzwfsni4j09nxk2g1g081jypd") (f (quote (("test-with-tiny-model") ("raw-api") ("opencl" "whisper-rs-sys/opencl") ("openblas" "whisper-rs-sys/openblas") ("metal" "whisper-rs-sys/metal" "_gpu") ("default") ("cuda" "whisper-rs-sys/cuda" "_gpu") ("coreml" "whisper-rs-sys/coreml") ("_gpu")))) (s 2) (e (quote (("whisper-cpp-tracing" "dep:tracing") ("whisper-cpp-log" "dep:log"))))))

