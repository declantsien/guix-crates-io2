(define-module (crates-io wh is whisper-rs-2) #:use-module (crates-io))

(define-public crate-whisper-rs-2-0.2.0 (c (n "whisper-rs-2") (v "0.2.0") (d (list (d (n "libffi") (r "^3.0.1") (d #t) (k 0)) (d (n "libffi-sys") (r "^2.0.1") (f (quote ("system"))) (d #t) (k 0)) (d (n "whisper-rs-sys2") (r "^0.2") (d #t) (k 0)))) (h "0bwhc6v618dyi5c0harbigh81dqs5d4v6ddsb3qrlk2ksfln2v8h")))

(define-public crate-whisper-rs-2-0.2.1 (c (n "whisper-rs-2") (v "0.2.1") (d (list (d (n "libffi") (r "^3.0.1") (d #t) (k 0)) (d (n "libffi-sys") (r "^2.0.1") (f (quote ("system"))) (d #t) (k 0)) (d (n "whisper-rs-sys2") (r "^0.2") (d #t) (k 0)))) (h "1h3fds5dm79ch8r0xw6mbfy35f9qqscbyh6q49rnvkar8fzbdarg")))

