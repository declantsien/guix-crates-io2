(define-module (crates-io wh is whisper) #:use-module (crates-io))

(define-public crate-whisper-0.1.0 (c (n "whisper") (v "0.1.0") (h "1fxjr3r44dr07dijfbf08q6zwwams29d793m3cw85kks4shdbxd1")))

(define-public crate-whisper-0.1.1 (c (n "whisper") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)))) (h "0qa4c6nvw3c1yhq063mvdm007nlaz1xmlykvhm1h6vfjij92nm16")))

(define-public crate-whisper-0.1.2 (c (n "whisper") (v "0.1.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)))) (h "18hfp12f2lbrg8x8f63fn3lry8zpymi52f3a7kppg73hdq7bhkmg")))

(define-public crate-whisper-0.1.3 (c (n "whisper") (v "0.1.3") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)))) (h "067dl2nz9xzypylphmvlzm3bmxydh1xhhd70a9y2mwzfkyaba8n3")))

(define-public crate-whisper-0.1.4 (c (n "whisper") (v "0.1.4") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)))) (h "0jpclvmzv4xj8xm40vkvwc5z99aa907gsrzpji7bxmqxrsrs380j")))

(define-public crate-whisper-0.1.5 (c (n "whisper") (v "0.1.5") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)))) (h "14hh8j2wzv8lsaihl0xamyshjzr81y8kilvx75jyxgzgxjjjig0f")))

(define-public crate-whisper-0.1.6 (c (n "whisper") (v "0.1.6") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)))) (h "1q1p19nr0n4fy8z6283fycci9wzp7jc6gixd26vl3dxknvcd7nvy")))

(define-public crate-whisper-0.1.7 (c (n "whisper") (v "0.1.7") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0s346j537j8y2d5gsyqz3kbmz7idswfxihk6x7w2gmzqpkfy6piy")))

(define-public crate-whisper-0.1.8 (c (n "whisper") (v "0.1.8") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "lru-cache") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0p5pr3v5rzw3k5j1qdp1awbvanmql2gch8x9mz8xxzj7vklkfgv6")))

(define-public crate-whisper-0.1.9 (c (n "whisper") (v "0.1.9") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "lru-cache") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0lgsiy9194lcqis2q98wl5gxg0nq7gsbz59fkw4yd2bnqmc26i64")))

(define-public crate-whisper-0.1.10 (c (n "whisper") (v "0.1.10") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "lru-cache") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1vdisxzqw9813fjqkw6p96mb9lil7ibh64yx089sar8r7p6m9s6s")))

(define-public crate-whisper-0.1.11 (c (n "whisper") (v "0.1.11") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "lru-cache") (r "*") (d #t) (k 0)) (d (n "memmap") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0s3f6jrg4bdbv8c5br14g37l9ar3gvrlfm323rrjs9d7rmqs3wy7")))

