(define-module (crates-io wh is whist) #:use-module (crates-io))

(define-public crate-whist-0.0.1 (c (n "whist") (v "0.0.1") (h "14kcahwl3k1mg0yby933m28p4fpy7gw4n7jd3qfww2833k48j6hb")))

(define-public crate-whist-0.0.2 (c (n "whist") (v "0.0.2") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2") (d #t) (k 0)))) (h "18i8h63q8wns168pm2ij5m3lr5wn8ck5kvm5fcgygll0m0031xdb")))

