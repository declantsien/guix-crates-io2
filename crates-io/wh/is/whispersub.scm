(define-module (crates-io wh is whispersub) #:use-module (crates-io))

(define-public crate-whispersub-0.1.0 (c (n "whispersub") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "0w2dwi3wcfzw4p0f8in19yr6md0r2rzgs80piq5czhrd8yjajx3j")))

