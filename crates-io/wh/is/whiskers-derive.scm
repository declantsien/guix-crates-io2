(define-module (crates-io wh is whiskers-derive) #:use-module (crates-io))

(define-public crate-whiskers-derive-0.1.0-alpha.0 (c (n "whiskers-derive") (v "0.1.0-alpha.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1l7wvkl2j0irm4a43sv1qidc4gpx129rq0a1xdldczvwv6khfd37")))

(define-public crate-whiskers-derive-0.1.0-alpha.1 (c (n "whiskers-derive") (v "0.1.0-alpha.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1z8hvhjzrnich4wvn1f42cd8dv4cwq3jf82v10x2rykfx3dsrif4")))

(define-public crate-whiskers-derive-0.1.0-alpha.2 (c (n "whiskers-derive") (v "0.1.0-alpha.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0g27zqfiz5xgjphgg8k8mc625qjydbrf825jabyl3q8dsji9cv5f")))

(define-public crate-whiskers-derive-0.1.0-alpha.3 (c (n "whiskers-derive") (v "0.1.0-alpha.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0m72j4xjdjk4fxrlh4jsw9ijjjzg1c2rfly7mihjdlsbw0fn3d6j")))

(define-public crate-whiskers-derive-0.1.0 (c (n "whiskers-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0ih3piqzh9hb4scaipf1x0xrrpp1rr023zr807kpd39wqa1kyhpz")))

(define-public crate-whiskers-derive-0.2.0-rc.0 (c (n "whiskers-derive") (v "0.2.0-rc.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1y2hivhd5hfb7xwv3dv96nlh8szkl9r3fnhcxbq74hpd102g2pb5")))

(define-public crate-whiskers-derive-0.2.0 (c (n "whiskers-derive") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1cziv6gj5i6wfp0dzlr6zcp9jyf4vfvd3nb4z8cg8z8nirdsdq86")))

(define-public crate-whiskers-derive-0.3.0-rc.1 (c (n "whiskers-derive") (v "0.3.0-rc.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "108xhxzdx2j98lbm42wq7abq7qajhrd2iv6hdn3y2lvadddl88sz")))

(define-public crate-whiskers-derive-0.3.0-rc.2 (c (n "whiskers-derive") (v "0.3.0-rc.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "185ifpy4ck3gb0lkxsq7a3yn39p94q34w2585zy50bawwiq197nm")))

(define-public crate-whiskers-derive-0.3.0 (c (n "whiskers-derive") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0kq9gcys3i0xa3sgvhsvjy5kxh6kijvm192fhn1gz95ycjcibrn0")))

(define-public crate-whiskers-derive-0.4.0-rc.0 (c (n "whiskers-derive") (v "0.4.0-rc.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1f7sg2fi01j1n2lv3k2qpy41xwiq7lzdf9h5xihiiand78gdkl5w")))

(define-public crate-whiskers-derive-0.4.0 (c (n "whiskers-derive") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0bynj1arm78xnwmcljwv1ng1anpymrcdq5acqk01axbky5zrgsaf")))

