(define-module (crates-io wh is whistlinoak) #:use-module (crates-io))

(define-public crate-whistlinoak-0.1.0 (c (n "whistlinoak") (v "0.1.0") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "05jvc82j75h7xg3w9wz1zgwf31z11ks71jby70nnw1dl91n9wkwj")))

(define-public crate-whistlinoak-0.1.1 (c (n "whistlinoak") (v "0.1.1") (d (list (d (n "blake2b_simd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "11plj9wpvxhn0gcfrw5k6v713qs4zr0d9l4lzppmwj4y9bmfhzmz") (f (quote (("test"))))))

(define-public crate-whistlinoak-0.2.0 (c (n "whistlinoak") (v "0.2.0") (d (list (d (n "blake2b_simd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0pzviijg5ssjyk3rfs6bhvzrvsra9g9z18y3sxfsvlwdcd1wnxr8") (f (quote (("test")))) (y #t)))

(define-public crate-whistlinoak-0.2.1 (c (n "whistlinoak") (v "0.2.1") (d (list (d (n "blake2b_simd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0m9vwsvwimb70fyk2721kk1j7rxsmpdjcni813jd74kaqa4vrk49") (f (quote (("test"))))))

