(define-module (crates-io wh is whisper_cpp_sys) #:use-module (crates-io))

(define-public crate-whisper_cpp_sys-0.2.0 (c (n "whisper_cpp_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0.9") (d #t) (k 0)))) (h "0lql066qpr9dxilf3al11hww2rny4ymasqdzlzrx9649ipyfzvm6") (f (quote (("openvino") ("native" "avx" "avx2" "fma" "f16c" "accel") ("metal") ("hipblas") ("fma") ("f16c") ("default" "compat" "native") ("cuda") ("compat") ("clblast") ("blas") ("avx2") ("avx") ("accel")))) (l "whisper")))

(define-public crate-whisper_cpp_sys-0.2.1 (c (n "whisper_cpp_sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0.9") (d #t) (k 0)))) (h "1q7879yd27a7jkz9inkbph1sp96npxrswp50sxmiv7m33iz2fppx") (f (quote (("openvino") ("native" "avx" "avx2" "fma" "f16c" "accel") ("metal") ("hipblas") ("fma") ("f16c") ("default" "compat" "native") ("cuda") ("compat") ("clblast") ("blas") ("avx2") ("avx") ("accel")))) (l "whisper")))

