(define-module (crates-io wh is whistle) #:use-module (crates-io))

(define-public crate-whistle-0.1.0 (c (n "whistle") (v "0.1.0") (d (list (d (n "chacha20") (r "^0.8.0") (f (quote ("rng"))) (d #t) (k 2)) (d (n "chacha20poly1305") (r "^0.9.0") (o #t) (k 0)) (d (n "curve25519-dalek") (r "^3.2.1") (f (quote ("u64_backend"))) (o #t) (k 0)) (d (n "hkdf") (r "^0.12.3") (o #t) (k 0)) (d (n "poly1305") (r "^0.7.2") (o #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (o #t) (k 0)))) (h "1frfy57k2mrh3vmm6dv1rv7y0fpg0g5p4jj7c4cs6jr3lx0d5c66") (f (quote (("default-sp" "curve25519-dalek" "hkdf" "chacha20poly1305" "sha2" "poly1305") ("default" "default-sp"))))))

