(define-module (crates-io wh en whenever-parser) #:use-module (crates-io))

(define-public crate-whenever-parser-0.1.0 (c (n "whenever-parser") (v "0.1.0") (h "0mxk0hqzfwbc09yb886h5hhzvkaaqwffwqfd4x9766z9p6359rg3")))

(define-public crate-whenever-parser-0.2.0 (c (n "whenever-parser") (v "0.2.0") (h "1w2mp0cv3rkkcgmz3h3233cvcv1s88y6wd8pir69cm69yxc9hmwg")))

