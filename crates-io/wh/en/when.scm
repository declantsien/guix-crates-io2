(define-module (crates-io wh en when) #:use-module (crates-io))

(define-public crate-when-0.1.3 (c (n "when") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.8.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tuple") (r "^0.4") (d #t) (k 0)))) (h "1xpigj50f9s6ppw54ya6dlzxjajva081p611y870kgfkd9g942ra")))

(define-public crate-when-0.1.4 (c (n "when") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strsim") (r "^0.8.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tuple") (r "^0.4") (d #t) (k 0)))) (h "0h3qmmwfmzd75ri3847icwivizq15c5vxh4qcg5xi38g1im98w5k")))

