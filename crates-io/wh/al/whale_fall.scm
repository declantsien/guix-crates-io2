(define-module (crates-io wh al whale_fall) #:use-module (crates-io))

(define-public crate-whale_fall-1.0.0 (c (n "whale_fall") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gosyn") (r "^0.2.8") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "05vb1swpajp1i3zrqnwbbxbpkr0yzmi8z1wbcjlpk6icwmbbqxvr")))

