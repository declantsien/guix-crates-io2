(define-module (crates-io wh al whalesim) #:use-module (crates-io))

(define-public crate-whalesim-0.1.0 (c (n "whalesim") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "09qjdjj0zypdxk571403zbfzig7hr6hl35zc23583yqbgrb4d6xc")))

(define-public crate-whalesim-0.1.1 (c (n "whalesim") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1rb2s9fl50bp37vr8yb78v0b9b3c80h7nm625rrk1m2h9ri0fx9v")))

