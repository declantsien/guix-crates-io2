(define-module (crates-io wh os whosly) #:use-module (crates-io))

(define-public crate-whosly-0.1.0 (c (n "whosly") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snmalloc-rs") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "0i52mdhfygdp75mhvfnsnc4zi0akq7iw01nmzc2ba3v64b3dk64f")))

(define-public crate-whosly-0.1.1 (c (n "whosly") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snmalloc-rs") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "1kpyjgdcy88wz2j6rsf63cps78kncmvp0d4n9dlj2q96v7z037hh")))

(define-public crate-whosly-0.1.2 (c (n "whosly") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snmalloc-rs") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "0s0n3q5xflsm69k7ac6lc2vjcw6f6bciwvppanxj042k4r1a97pg")))

(define-public crate-whosly-0.1.3 (c (n "whosly") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snmalloc-rs") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "19zvk7jn2a1qvjb7mrlq0169vw1gscwlnsgzzjq7x3gk81pwr2k5")))

(define-public crate-whosly-0.1.4 (c (n "whosly") (v "0.1.4") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snmalloc-rs") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "1sxh964bw4vxpvvy9wxp5da3jnkkp2gbjb4h0bsxxlsgnvlgzm8y")))

(define-public crate-whosly-0.1.5 (c (n "whosly") (v "0.1.5") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snmalloc-rs") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "0v42b9023lxbn2q1z09fnba8m1w66f9a2svcp9n9498vmrkdnsmy")))

(define-public crate-whosly-0.1.6 (c (n "whosly") (v "0.1.6") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "snmalloc-rs") (r "^0.3.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "1id35bq61xw87x3r5693ckzvq7g8b3dvkg2hxrjxqxdczfmgh33k")))

(define-public crate-whosly-0.1.8 (c (n "whosly") (v "0.1.8") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "~1.35.1") (d #t) (k 0)))) (h "1y38njlraspz29vh6n4bdm39cb469cklgy55k1xzn7nqab2cl5ap")))

