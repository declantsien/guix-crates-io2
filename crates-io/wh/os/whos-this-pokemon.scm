(define-module (crates-io wh os whos-this-pokemon) #:use-module (crates-io))

(define-public crate-whos-this-pokemon-0.1.0 (c (n "whos-this-pokemon") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gfbj3b1kih7nkf52dicz1d04ic421qs1i9i272nzikhbbrih4l1")))

(define-public crate-whos-this-pokemon-0.2.0 (c (n "whos-this-pokemon") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01d283s9jal5jg64aqdxyipba0afssczjmq96i6bw7gm2dfq005n")))

