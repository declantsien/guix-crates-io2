(define-module (crates-io wh at whats-a-png) #:use-module (crates-io))

(define-public crate-whats-a-png-0.1.0 (c (n "whats-a-png") (v "0.1.0") (h "0ssymgww1lmjcdhh4rh3gjnqiackjbq45dvk28cm9mr85l5d70ai")))

(define-public crate-whats-a-png-0.1.1 (c (n "whats-a-png") (v "0.1.1") (h "0jq6n8860qma96kdfqcwc58pq8ci42lwwdd852392zk4s9r6nc0v")))

