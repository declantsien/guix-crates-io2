(define-module (crates-io wh at what-the-dog-doin) #:use-module (crates-io))

(define-public crate-what-the-dog-doin-0.1.0 (c (n "what-the-dog-doin") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "14cr6ff2v4nqs5khnhg32q50q8j5pnw7iv86ajwap79k6yz0h73x")))

(define-public crate-what-the-dog-doin-0.1.1 (c (n "what-the-dog-doin") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)))) (h "1iwy1gcs1xxfz51ffrfy3da8hgpbwr9hyadchb4ksvl3m1h3n66w")))

