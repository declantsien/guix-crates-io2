(define-module (crates-io wh at whaterror_macros) #:use-module (crates-io))

(define-public crate-whaterror_macros-0.1.0 (c (n "whaterror_macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.38") (d #t) (k 2)))) (h "1370dc9slx0fz1m66157glz0zj78rz4g7q4k8w3q84cbffhbxlvd")))

