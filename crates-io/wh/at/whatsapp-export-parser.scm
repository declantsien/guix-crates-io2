(define-module (crates-io wh at whatsapp-export-parser) #:use-module (crates-io))

(define-public crate-whatsapp-export-parser-0.1.0 (c (n "whatsapp-export-parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1428j0l83ph4jwx12fpzs18bb2gvx5kcqwrjv1rwxyilxkw0jdm1") (f (quote (("serde-1" "serde" "chrono/serde") ("default"))))))

