(define-module (crates-io wh at what-time) #:use-module (crates-io))

(define-public crate-what-time-0.1.0 (c (n "what-time") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0dlfamzcgaxq6yaphr4n8ba019b4hcfcxc7rab531k618lywmy9l") (y #t)))

(define-public crate-what-time-0.1.1 (c (n "what-time") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "157b76745x95sbkskbgml7j0z7j76l4wv3phsmh47p7rikb5357m")))

(define-public crate-what-time-0.2.0 (c (n "what-time") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0awcbmpv5z4wilbpxl7q4biwlq6fl332cwhla1chlilqk70mdx0k")))

