(define-module (crates-io wh at whatfeatures) #:use-module (crates-io))

(define-public crate-whatfeatures-0.5.6 (c (n "whatfeatures") (v "0.5.6") (d (list (d (n "attohttpc") (r "^0.4.5") (f (quote ("json"))) (d #t) (k 0)) (d (n "lexical_bool") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1f2xq8mslgd6pb6wa7hi406k8c4xxj037kyj99grvay9qn3jd6kw") (y #t)))

