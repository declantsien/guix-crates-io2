(define-module (crates-io wh at whatwg-datetime) #:use-module (crates-io))

(define-public crate-whatwg-datetime-0.1.0 (c (n "whatwg-datetime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std" "wasmbind"))) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "iai") (r "^0.1.0") (d #t) (k 2)) (d (n "whatwg-infra") (r "^0.2.1") (d #t) (k 0)))) (h "1jmcsww3mz487nv9gpydhm49s4wychs1wgadnnk26ak1j14iabgh")))

