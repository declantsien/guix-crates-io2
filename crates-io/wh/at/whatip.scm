(define-module (crates-io wh at whatip) #:use-module (crates-io))

(define-public crate-whatip-0.1.0 (c (n "whatip") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1" "tcp"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1mgji6zj9skarn42xx4lxwmkbdpis7n6vbm24crzhw18p8mrkl4j")))

(define-public crate-whatip-0.2.0 (c (n "whatip") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1" "tcp"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "119dzynq20y8rr99qsg2m5rjx13lncqpnls62hb538mwgl5m5z6x")))

(define-public crate-whatip-0.3.0 (c (n "whatip") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2") (k 0)))) (h "1yi4w9yn6sfgwnp9wjxkai5jsi26ryzqrf3iqzmysk38v9yy6sf6")))

(define-public crate-whatip-0.4.1 (c (n "whatip") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2") (k 0)))) (h "1q28av2lnh1fj3w3p6x8zh8l8asg8j6imrxmi84d30smc131a4dh")))

(define-public crate-whatip-0.4.2 (c (n "whatip") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2") (k 0)))) (h "1m9cary82r8fy92g3p3xwaaai5x44n4lxxbd9m6jmjpd5c12cjg7")))

