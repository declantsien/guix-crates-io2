(define-module (crates-io wh at what-the-async) #:use-module (crates-io))

(define-public crate-what-the-async-0.1.0 (c (n "what-the-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "wta-executor") (r "=0.1.0") (d #t) (k 0)) (d (n "wta-reactor") (r "=0.1.0") (d #t) (k 0)))) (h "0182fdd0ki8k31625bjydfc7ndraqy7cpbm00136bnfrfpnrshkn")))

