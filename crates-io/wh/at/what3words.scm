(define-module (crates-io wh at what3words) #:use-module (crates-io))

(define-public crate-what3words-0.1.0 (c (n "what3words") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yinw9hhadkzmmlqr0swhri75npc6fq5dfavplrzci92qd8wz5hn")))

(define-public crate-what3words-0.1.1 (c (n "what3words") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p980ki1mgxmpghpzfyh6q8d4ykgfxbgb63wsdrj8wmmjzb1lfh2")))

