(define-module (crates-io wh at whatspp-cloud-api-rs) #:use-module (crates-io))

(define-public crate-whatspp-cloud-api-rs-0.1.0 (c (n "whatspp-cloud-api-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0b06zib6ihxgndv7zaa0zaid2jxd75fjwcl5chpyp3pvzjfdf4h0") (y #t)))

