(define-module (crates-io wh at whatsys) #:use-module (crates-io))

(define-public crate-whatsys-0.0.0 (c (n "whatsys") (v "0.0.0") (h "1nridgmlirfsf2kyz6yg0jlaa0dz0s04h7l31k5lv1n58ynagzi1")))

(define-public crate-whatsys-0.1.0 (c (n "whatsys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)))) (h "1ssbkif2svc4yvha7ran31xmc12pjnz7xhkdn292p81651qlq6r7")))

(define-public crate-whatsys-0.1.1 (c (n "whatsys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.88") (d #t) (k 0)))) (h "1zhak7k7g29klblgfqmd6s164rrq3qik4hlaydxcicvk056qj758")))

(define-public crate-whatsys-0.1.2 (c (n "whatsys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pbn58j0713gsfxwpq07wpmcv8hgn7l9ba93p9j3k5z0l5dgyky2")))

(define-public crate-whatsys-0.2.0 (c (n "whatsys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fl28hi1cw76hypdnk9llk2dph2gd6cqc74a0gpz7py91nplx0y9")))

(define-public crate-whatsys-0.2.1 (c (n "whatsys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06wjpf18110s5jdm0p06l5h34r9p9m9mj3pmfnyadrzvg349bxhx")))

(define-public crate-whatsys-0.3.0 (c (n "whatsys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jbkrnpgky5irjjzvh2g13pmayf21gjzbrdazd3y709a36jvlj2s")))

(define-public crate-whatsys-0.3.1 (c (n "whatsys") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0dkayj10qys8fxq53bmr206whdj7sg5rzjh8388k0ih2fq02qqxv")))

