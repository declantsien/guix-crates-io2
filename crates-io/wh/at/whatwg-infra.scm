(define-module (crates-io wh at whatwg-infra) #:use-module (crates-io))

(define-public crate-whatwg-infra-0.1.0 (c (n "whatwg-infra") (v "0.1.0") (h "1436kfsa3s7780whasd5djwb1dh76h3xk1babww36129kx4m5lw6") (r "1.63.0")))

(define-public crate-whatwg-infra-0.1.1 (c (n "whatwg-infra") (v "0.1.1") (h "0ld4srjbrf9k3dw1vn94g7pccsc8kky7jyrnxdbshz8wxzmln7yq") (r "1.63.0")))

(define-public crate-whatwg-infra-0.2.0 (c (n "whatwg-infra") (v "0.2.0") (h "12724fzg3jk33hl94dx3gqr719k5cjx387svs7j7yjn8m3vwzsb4") (r "1.63.0")))

(define-public crate-whatwg-infra-0.2.1 (c (n "whatwg-infra") (v "0.2.1") (h "1nxp0013vhrck467y9bmn9j682sz6aja83icmm9wnjbc73wyafil") (r "1.63.0")))

(define-public crate-whatwg-infra-0.2.2 (c (n "whatwg-infra") (v "0.2.2") (h "1i7m5hgfj0ny0nc0iknka1hyqgvil683n5fz88dn9dmb9rzchpz2") (r "1.63.0")))

(define-public crate-whatwg-infra-1.0.0 (c (n "whatwg-infra") (v "1.0.0") (h "1br6hd8k0xfc5isj26z33hzqjlbm91nwmxz6wkby1n1an341cz99") (r "1.63.0")))

