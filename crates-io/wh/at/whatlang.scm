(define-module (crates-io wh at whatlang) #:use-module (crates-io))

(define-public crate-whatlang-0.1.0 (c (n "whatlang") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1k6d9yd8zszqijvy0fphaiv73j3q0c8qwyvwigh9fahwnvp0y9a5")))

(define-public crate-whatlang-0.1.1 (c (n "whatlang") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1iqbahmgibn36wsnbaxv2s9j76lv3gdqx9fzfasm1d13y09d0r95")))

(define-public crate-whatlang-0.1.2 (c (n "whatlang") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "15n7gcsrp6b43yllmnqzlnsqd09bszkgq30mxd872wp4xxa6kj5h")))

(define-public crate-whatlang-0.1.3 (c (n "whatlang") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "052kf0bx3d744yrk3whjc9p2y4n2cp0f6bk4vscr6fylgaawwr5x")))

(define-public crate-whatlang-0.1.4 (c (n "whatlang") (v "0.1.4") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0gcjblcscvk71l05p9msx8jpkx6bjscdkvywn9i9qprbhzylp4yg")))

(define-public crate-whatlang-0.2.0 (c (n "whatlang") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1i2pkm0i8kdk89hzpwhfq15dxrqy8c3by61kwwccrw4g1bkv3xlv")))

(define-public crate-whatlang-0.2.1 (c (n "whatlang") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1fia11iz1i5r7swha2y627yvrwksr76f1yfivlphyi53g24sasqx")))

(define-public crate-whatlang-0.3.0 (c (n "whatlang") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "07lny172ng1sfs48wccyvc2lrbi65d6dv90ppgqhfdwjkqi7lp1i")))

(define-public crate-whatlang-0.3.1 (c (n "whatlang") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1a5z9qsh1vk122ybgps0s2hw7jch9243mmr5jscr7z2pj64sr6xi")))

(define-public crate-whatlang-0.3.2 (c (n "whatlang") (v "0.3.2") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1r50sfxy32khvd7017g4w9nhx3jl82by2lgnl0nx7sdzarrb2f39")))

(define-public crate-whatlang-0.3.3 (c (n "whatlang") (v "0.3.3") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1zpxz7cj8nf6ncp67amia8ppzcsipdg6w9aqn4bpl1zs4p9k96vf")))

(define-public crate-whatlang-0.4.0 (c (n "whatlang") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1ssnwb7a34hp1pn8mk2y8i3q9vj7hxmz1mrk05f77xv0qxx85gxw")))

(define-public crate-whatlang-0.4.1 (c (n "whatlang") (v "0.4.1") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1yx526dgry5svazpc1x6c50pnaa3hxqbmy21hwhwzwgrdrygpwjp")))

(define-public crate-whatlang-0.5.0 (c (n "whatlang") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1hmk2dwvw1v129zfhwa7k25qm3l1bi977869lf4j2mlj771ydmm9")))

(define-public crate-whatlang-0.6.0 (c (n "whatlang") (v "0.6.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "csv") (r "^1.0.2") (d #t) (k 1)) (d (n "hashbrown") (r "^0.1.5") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 2)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)) (d (n "tera") (r "^0.11.18") (d #t) (k 1)))) (h "1a0p3vy1svzvghna0yaj9ridxd6bn08y9c06vwwvgcnwikj4f44p")))

(define-public crate-whatlang-0.7.0 (c (n "whatlang") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0dfcdqsw4jw0w2ljbaiv4kbds7r7sy4h97p3hlqcymy46njrc67w")))

(define-public crate-whatlang-0.7.1 (c (n "whatlang") (v "0.7.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.3.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0v418qv3dkgqm2w83z7ramnfg5fnc6w0j5mp2wln3w5hhqlh8qdz")))

(define-public crate-whatlang-0.7.2 (c (n "whatlang") (v "0.7.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0ilar2rl4wsg4n34izzjcvr4zjnk8w4az64by756rx70g3n84798")))

(define-public crate-whatlang-0.7.3 (c (n "whatlang") (v "0.7.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1sbqwfvasawdqfk6lavgmmwgl044mskd58liy81wvkxchp6ahdrl")))

(define-public crate-whatlang-0.7.4 (c (n "whatlang") (v "0.7.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "16p6akawylccn6lnikibbk1bbzwvnf54wdxv0amkzrl5fd7nkd2y") (y #t)))

(define-public crate-whatlang-0.8.0 (c (n "whatlang") (v "0.8.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "18jjy0y1iwpc3nw2ykrrjdrlny3niyi9vw89rrhi93biwqqzmjbq")))

(define-public crate-whatlang-0.9.0 (c (n "whatlang") (v "0.9.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0xchm3vxphvw84r0qapcg72nkig933hq6raycjji912ls70qj0mw")))

(define-public crate-whatlang-0.10.0 (c (n "whatlang") (v "0.10.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1jrl6n5pj72k5fz2gsw4a02njkxmcidfdwa56qfycs542jk5aspp")))

(define-public crate-whatlang-0.11.0 (c (n "whatlang") (v "0.11.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0lsjm28w3adaqrqwnd8hvs28813civfr6id14g68maflgqkv678k")))

(define-public crate-whatlang-0.11.1 (c (n "whatlang") (v "0.11.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "01gzlvmmb6m05klqjdw02w4g1z7ldrhlzyi94v6d0l7n1lb6cc3q")))

(define-public crate-whatlang-0.12.0 (c (n "whatlang") (v "0.12.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "enum-map") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "00zp7h1iwinpd6b8prcan0n3y36d3afs497djf3620wwn8p6sd3s") (f (quote (("dev"))))))

(define-public crate-whatlang-0.13.0 (c (n "whatlang") (v "0.13.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "enum-map") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0bydwlcarr2w8ww6p5z5ngvag05fb9d4qjns0asdqjghy3ymg4rl") (f (quote (("dev"))))))

(define-public crate-whatlang-0.14.0 (c (n "whatlang") (v "0.14.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "enum-map") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1rcfm4460wm6rqh8vzvnz4m4dw1hfh2jdjd02ys3ps55xyxsnv9r") (f (quote (("dev"))))))

(define-public crate-whatlang-0.15.0 (c (n "whatlang") (v "0.15.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "enum-map") (r "^2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1n2vkilyia95g08837g7x2hkgy2j1gg1kwrfa6h2qz1xk1i2zqmc") (f (quote (("dev"))))))

(define-public crate-whatlang-0.16.0 (c (n "whatlang") (v "0.16.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "enum-map") (r "^2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1ffkdi9sxql9bvbgxys4p4qhkgmi61018yg871jag5nbsglxg299") (f (quote (("dev"))))))

(define-public crate-whatlang-0.16.1 (c (n "whatlang") (v "0.16.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "enum-map") (r "^2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1l26mjss20y5rnpg5szpwjpbmpjgw10iaskqj9nf04ldc38qs132") (f (quote (("dev"))))))

(define-public crate-whatlang-0.16.2 (c (n "whatlang") (v "0.16.2") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "arbtest") (r "^0.1") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "enum-map") (r "^2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0hx038f4q5nch3r5pmfvkq71sqjfxxzc1qlbg0rvhqn4qhnillww") (f (quote (("dev"))))))

(define-public crate-whatlang-0.16.3 (c (n "whatlang") (v "0.16.3") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "arbtest") (r "^0.2") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "enum-map") (r "^2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1nbbbz41rfg7j98dh81j3dia8dkz4y0yia3dj981x1svb8cx1p6w") (f (quote (("dev"))))))

(define-public crate-whatlang-0.16.4 (c (n "whatlang") (v "0.16.4") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "arbtest") (r "^0.2") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "enum-map") (r "^2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1c57cj3przzm156a10g64pb8vdcgd9wb2l0n59wfnqfk8lb1q7a7") (f (quote (("dev"))))))

