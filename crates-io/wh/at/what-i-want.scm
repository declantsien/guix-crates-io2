(define-module (crates-io wh at what-i-want) #:use-module (crates-io))

(define-public crate-what-i-want-0.1.0 (c (n "what-i-want") (v "0.1.0") (h "1c7jnld7kvji2nmrrxy2iqmrjadylmzb9bmi7mlkafklsxmv7qa4") (y #t)))

(define-public crate-what-i-want-0.1.1 (c (n "what-i-want") (v "0.1.1") (h "1bg9irka1wbycaxj3y152m332lw43d8lx6vk0dvscjw3b4i54x7d")))

