(define-module (crates-io wh at whaterror) #:use-module (crates-io))

(define-public crate-whaterror-0.0.0 (c (n "whaterror") (v "0.0.0") (h "1nacsg2a0fqrxa7ir4z6dp5ih1r9khiri2hs0c45fjmx5dfswmvz")))

(define-public crate-whaterror-0.1.0 (c (n "whaterror") (v "0.1.0") (d (list (d (n "whaterror_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0j9bxw6xphqayb6cbvbs5gxsv5vv36x8ac7hlkjqxj4szx67nqcw")))

