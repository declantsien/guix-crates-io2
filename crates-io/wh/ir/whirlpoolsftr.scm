(define-module (crates-io wh ir whirlpoolsftr) #:use-module (crates-io))

(define-public crate-whirlpoolsftr-0.3.0 (c (n "whirlpoolsftr") (v "0.3.0") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "1syjg9c5mz4p8cjv1qhh3pj6l06faah4a2k7kn1mfnh0k6m0dfz2") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-whirlpoolsftr-0.3.1 (c (n "whirlpoolsftr") (v "0.3.1") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)))) (h "0yjn1rspv4gx03vyznmln5bizld2r2jb2yhslc6jvfkbvvgvnr1q") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

