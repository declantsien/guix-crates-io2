(define-module (crates-io wh ir whir) #:use-module (crates-io))

(define-public crate-whir-0.1.0 (c (n "whir") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "0n37k4v8hq5r318rz7qw5wg6s9pij5d84mypi10xr7h90zkf7wgd")))

(define-public crate-whir-0.2.0 (c (n "whir") (v "0.2.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "04maapcia49pcrinl5akacv3yy14y2qqbf4k0qnm6kw7xymx6vbl")))

(define-public crate-whir-0.3.0 (c (n "whir") (v "0.3.0") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "0l79avyqvsz64xiz0mb1b9kb0g8qdcq7ypg7kjia54clpc2g54br")))

(define-public crate-whir-0.3.1 (c (n "whir") (v "0.3.1") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "18rsgib06nxhbnp4pzmmgzx1fahdnzyd1dpl0njfnsv7wx90rhf7")))

(define-public crate-whir-0.3.2 (c (n "whir") (v "0.3.2") (d (list (d (n "rocket") (r "^0.5.0") (d #t) (k 0)))) (h "000wakyk3p2cgkbdvlc7r9dglsd6sisz9s1s0bx73zplfplamqp7")))

