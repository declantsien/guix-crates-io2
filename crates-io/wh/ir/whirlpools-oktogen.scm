(define-module (crates-io wh ir whirlpools-oktogen) #:use-module (crates-io))

(define-public crate-whirlpools-oktogen-0.1.0 (c (n "whirlpools-oktogen") (v "0.1.0") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "0aj8nl46p3lkjvxw7rxph4nq876756y45panqdqyp8j0286ls0yn") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

