(define-module (crates-io wh ir whirlybird) #:use-module (crates-io))

(define-public crate-whirlybird-0.1.0 (c (n "whirlybird") (v "0.1.0") (d (list (d (n "redmaple") (r "^0.3.0") (d #t) (k 0)))) (h "0m512pflpdj4p0bnkzlv0q3dsxvlpsls0f12flbii3g1sr25179p")))

(define-public crate-whirlybird-0.1.1 (c (n "whirlybird") (v "0.1.1") (d (list (d (n "redmaple") (r "^0.3.0") (d #t) (k 0)))) (h "1qbslcy3n38xnbzmvn4kzh62qnpnzcfar2fl0in8s03lnm9wkj0f")))

(define-public crate-whirlybird-0.1.2 (c (n "whirlybird") (v "0.1.2") (d (list (d (n "redmaple") (r "^0.4.1") (d #t) (k 0)))) (h "11769g9z0vbjkij538j5lm4wpv6dp3fxqsndwklvaa6cfqwklcqp")))

(define-public crate-whirlybird-0.1.3 (c (n "whirlybird") (v "0.1.3") (d (list (d (n "redmaple") (r "^0.4.1") (d #t) (k 0)))) (h "0kgwkc3j26yif6ys9y201yyp12lr4sxj72pwqf6fg34rf01qqlgi")))

(define-public crate-whirlybird-0.1.4 (c (n "whirlybird") (v "0.1.4") (d (list (d (n "redmaple") (r "^0.4.1") (d #t) (k 0)))) (h "16483p426jsvnmnypa4jx376bd4mk1l6zfdw86hicy43zjrr7mng")))

(define-public crate-whirlybird-0.1.5 (c (n "whirlybird") (v "0.1.5") (d (list (d (n "redmaple") (r "^0.5.1") (d #t) (k 0)))) (h "1df4j1y4lpqd9i0kgg9xalm3hxkc58yzb3iqj510nb5cr5ka6ix3")))

(define-public crate-whirlybird-0.2.0 (c (n "whirlybird") (v "0.2.0") (d (list (d (n "redmaple") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1lkqsr37nf8rrla6qrw0f75pdyjmpbbfri2y0xwjsb4xpqfxa7pj")))

(define-public crate-whirlybird-0.2.1 (c (n "whirlybird") (v "0.2.1") (d (list (d (n "redmaple") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0aaw8899yl99clqkayvw4kl85ykafi5h56bzfwp3fafjdcaldl07")))

(define-public crate-whirlybird-0.3.0 (c (n "whirlybird") (v "0.3.0") (d (list (d (n "redmaple") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "09crwhjrpjxlxhyc49z5zzlmv8v7s15qk5xx3mnvm1n75g8q310r")))

(define-public crate-whirlybird-0.3.1 (c (n "whirlybird") (v "0.3.1") (d (list (d (n "redmaple") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0w07hqcsfhjisxwprr78y6ps8c8vqbrh7c6vl7wad9jdk8kcc35v")))

(define-public crate-whirlybird-0.3.2 (c (n "whirlybird") (v "0.3.2") (d (list (d (n "redmaple") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0gzlbk3skpak85qxz6x1lbcx49v6jpxmj5h4g7xqzxycnp13bsfm")))

(define-public crate-whirlybird-0.3.3 (c (n "whirlybird") (v "0.3.3") (d (list (d (n "redmaple") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1xr1apbjn7w9fvpsxwg1ya7m7pviy3pabydy67hp6awk59gwm0q1")))

(define-public crate-whirlybird-0.3.4 (c (n "whirlybird") (v "0.3.4") (d (list (d (n "redmaple") (r "^0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "097l6nqzwll0a4mi2zf3i3vrfr0i8xkrydmchjyj0fd8j5jav235")))

(define-public crate-whirlybird-0.4.0 (c (n "whirlybird") (v "0.4.0") (d (list (d (n "getset-scoped") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "redmaple") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "0ib2rdw8ygjc2n3v3130amrn79cayv663zh4jaihsh097dx467l6") (f (quote (("journey") ("default") ("argument") ("all" "default" "argument"))))))

(define-public crate-whirlybird-0.5.0 (c (n "whirlybird") (v "0.5.0") (d (list (d (n "getset-scoped") (r "^0.1.2") (d #t) (k 0)) (d (n "redmaple") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "16bm0bnahmi5r5h4n7714mibjd7ylss23zmpxxjn47j0nv07fwjb") (f (quote (("journey") ("default" "journey" "argument") ("argument") ("all" "journey" "argument"))))))

(define-public crate-whirlybird-0.6.0 (c (n "whirlybird") (v "0.6.0") (d (list (d (n "getset-scoped") (r "^0.1.2") (d #t) (k 0)) (d (n "redmaple") (r "^0.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "1815fsl4xbzygz7z43dbnxj3sf9w5fwbcw4hj1zfcl03prn18dqk") (f (quote (("journey") ("default" "journey" "argument") ("argument") ("all" "journey" "argument"))))))

(define-public crate-whirlybird-0.6.1 (c (n "whirlybird") (v "0.6.1") (d (list (d (n "getset-scoped") (r "^0.1.2") (d #t) (k 0)) (d (n "redmaple") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "0anigshbgf8ksp0rarh1zps4qf27b4809lywn138hr7khymvcmhx") (f (quote (("journey") ("default" "journey" "argument") ("argument") ("all" "journey" "argument"))))))

(define-public crate-whirlybird-0.6.2 (c (n "whirlybird") (v "0.6.2") (d (list (d (n "getset-scoped") (r "^0.1.2") (d #t) (k 0)) (d (n "redmaple") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "1y9334ndzxiw2ssrz1vgylxxsg9k86yadajj8sp9v0234y94fwn7") (f (quote (("journey") ("default" "journey" "argument") ("argument") ("all" "journey" "argument"))))))

(define-public crate-whirlybird-0.7.0 (c (n "whirlybird") (v "0.7.0") (d (list (d (n "getset-scoped") (r "^0.1.2") (d #t) (k 0)) (d (n "redmaple") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "0yjdrs1dgcgxn357mhzcb25d2sbvdas6qf4isd1rdxbxad29cqhr") (f (quote (("journey") ("default" "journey" "argument") ("argument") ("all" "journey" "argument"))))))

(define-public crate-whirlybird-0.8.0 (c (n "whirlybird") (v "0.8.0") (d (list (d (n "redmaple") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "1npifyr4d5l846swz96699zfvx69i8p9qbgqla73vfmic9lbh4gr") (f (quote (("journey") ("default" "journey" "argument") ("argument") ("all" "journey" "argument"))))))

(define-public crate-whirlybird-0.8.1 (c (n "whirlybird") (v "0.8.1") (d (list (d (n "redmaple") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1jy3lyp578kaarnpwqy6lkkqs8y874bgr0yk4wmwqwh8gs6wggh7") (f (quote (("journey") ("default" "journey" "argument") ("argument") ("all" "journey" "argument"))))))

(define-public crate-whirlybird-0.8.2 (c (n "whirlybird") (v "0.8.2") (d (list (d (n "redmaple") (r "^0.12.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "uuid") (r "^1.3.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "1jl9k9f8grk2jbs4znq6ynq7sdr9qzxn4vj1sza0idzrzaa5mgg5") (f (quote (("journey") ("default" "journey" "argument") ("argument") ("all" "journey" "argument"))))))

(define-public crate-whirlybird-0.9.0 (c (n "whirlybird") (v "0.9.0") (d (list (d (n "redmaple") (r "^0.12.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 2)))) (h "0mnr25n2szz2vcckicw18xmyhxng82niwj7w31qhcmgmx9l6xcqi") (f (quote (("journey") ("default" "journey" "argument") ("argument") ("all" "journey" "argument"))))))

(define-public crate-whirlybird-0.10.0 (c (n "whirlybird") (v "0.10.0") (d (list (d (n "redmaple") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "0s09xid53m925cvw6xvbxh64rx7kkjnv7far49d9p8y1ddhgfb1i") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.10.1 (c (n "whirlybird") (v "0.10.1") (d (list (d (n "redmaple") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "0nwk1mqflgsqdymf89k8bg58zjwhg5ravcpx787vwikrqhc5y6wa") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.11.0 (c (n "whirlybird") (v "0.11.0") (d (list (d (n "redmaple") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "04cgwyw2f7wwdhalgcilmnxzagdj78cf349614r4y9v3r68r5y8c") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.11.1 (c (n "whirlybird") (v "0.11.1") (d (list (d (n "redmaple") (r "^0.14.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "1rwcy3ygaw2306lrbvr1jypd17glmvy6xa31580xwvkqi2pfbybg") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.11.2 (c (n "whirlybird") (v "0.11.2") (d (list (d (n "redmaple") (r "^0.14.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)))) (h "0xh7aaksb5iym6a0ix5bkg1a1gcadqhfb331wh120f63nll4x144") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.12.0 (c (n "whirlybird") (v "0.12.0") (d (list (d (n "redmaple") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)))) (h "0nkaqfnjrzsq84zxbwka8rb0qn7xphp9zb03k45dhb53pv1ckdsq") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.12.1 (c (n "whirlybird") (v "0.12.1") (d (list (d (n "redmaple") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)))) (h "1nnw1psr3a7a26kmxi0snvc6hp0nxrzl6lp53s17nysccr8hb1j2") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.12.2 (c (n "whirlybird") (v "0.12.2") (d (list (d (n "redmaple") (r "^0.16.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)))) (h "04rh6hchcvpbfybpn19y6kd8wsbcbvvp0vyq16l5gn14g6jbbwbw") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.13.0 (c (n "whirlybird") (v "0.13.0") (d (list (d (n "redmaple") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)) (d (n "time") (r "^0.3.22") (f (quote ("serde"))) (d #t) (k 0)))) (h "06s2fa44l5ylwsm9malj3g1y2ag585k3crhb58ka3il0x6q18yar") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.13.1 (c (n "whirlybird") (v "0.13.1") (d (list (d (n "redmaple") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (f (quote ("serde"))) (d #t) (k 0)))) (h "1g60pvg71srcbwczz069nw9i9wa3i5mj15694wzik7cb6wkdlf9j") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.13.2 (c (n "whirlybird") (v "0.13.2") (d (list (d (n "redmaple") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (f (quote ("serde"))) (d #t) (k 0)))) (h "1vgyjz8jdvzb38vc8jq36412fysysphx0aq2a0rqg02d6vg46hw3") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.13.3 (c (n "whirlybird") (v "0.13.3") (d (list (d (n "redmaple") (r "^0.18.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (f (quote ("serde"))) (d #t) (k 0)))) (h "1hyj8cp3xi1smc8j80jgz31p5jnqdaapyh2y9mz3xi2bm6y2ncn0") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

(define-public crate-whirlybird-0.13.4 (c (n "whirlybird") (v "0.13.4") (d (list (d (n "redmaple") (r "^0.18.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "time") (r "^0.3.23") (f (quote ("serde"))) (d #t) (k 0)))) (h "0b6n2fp40f9m7ap9rspr1p6lhy7cqsj034iy4g13vjnl7ya20ym0") (f (quote (("journey") ("default" "journey") ("all" "journey"))))))

