(define-module (crates-io wh ir whirlpools) #:use-module (crates-io))

(define-public crate-whirlpools-0.3.0 (c (n "whirlpools") (v "0.3.0") (d (list (d (n "anchor-gen") (r "^0.3.0") (d #t) (k 0)) (d (n "anchor-lang") (r ">=0.20") (d #t) (k 0)))) (h "025xyl59ccrcj5as65hlacxxmckk4zvjxckmv7a8pns3j72a6xij") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

