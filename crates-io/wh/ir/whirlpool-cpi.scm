(define-module (crates-io wh ir whirlpool-cpi) #:use-module (crates-io))

(define-public crate-whirlpool-cpi-0.1.0 (c (n "whirlpool-cpi") (v "0.1.0") (d (list (d (n "anchor-lang") (r "=0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "=0.29.0") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "default-boxed") (r "^0.2.0") (d #t) (k 0)) (d (n "solana-program") (r ">=1.16, <1.18") (d #t) (k 0)))) (h "06p6ybmk93z7blg6zryj0sv6y648sig4shcwykgb7866ljhhvavi") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-whirlpool-cpi-0.1.1 (c (n "whirlpool-cpi") (v "0.1.1") (d (list (d (n "anchor-lang") (r "=0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "=0.29.0") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "default-boxed") (r "^0.2.0") (d #t) (k 0)) (d (n "solana-program") (r ">=1.16, <1.18") (d #t) (k 0)))) (h "11z3fqhairrrhcmdwa3h74ihgbsj2wh9z01bkd8ry0jmg5pw0fsw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

