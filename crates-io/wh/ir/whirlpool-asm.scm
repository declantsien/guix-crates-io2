(define-module (crates-io wh ir whirlpool-asm) #:use-module (crates-io))

(define-public crate-whirlpool-asm-0.1.0 (c (n "whirlpool-asm") (v "0.1.0") (d (list (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "0n5xcjz2jzc6dvwr0f0vk5q1y69fxy4x0mvwd80r4h2nm94yx0kk")))

(define-public crate-whirlpool-asm-0.2.0 (c (n "whirlpool-asm") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1ajghw2w3j2zh9ljbxfxgpprchbvhjk07nzb4ibmwpl0zp65ragp")))

(define-public crate-whirlpool-asm-0.2.1 (c (n "whirlpool-asm") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "17745qh0cs1scnr8m1wvz80bpzbngf6093vdi9w0d2z5jzvngj50") (y #t)))

(define-public crate-whirlpool-asm-0.2.2 (c (n "whirlpool-asm") (v "0.2.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "170lw28dlfqfrazg5rhvsdhvyk8pd7q98h45q7l8i0siw4akvlpy")))

(define-public crate-whirlpool-asm-0.3.0 (c (n "whirlpool-asm") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "1qrmrsx8ydrjc4absxc3rl00avs9knkq46dmmm15jgq1pj3w5qid")))

(define-public crate-whirlpool-asm-0.4.0 (c (n "whirlpool-asm") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "generic-array") (r "^0.10") (d #t) (k 0)))) (h "05r8qn70rh41j95vif4f5bwrv57d0h6nl67rlc7zaf9j92cl58pf")))

(define-public crate-whirlpool-asm-0.5.0 (c (n "whirlpool-asm") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1qvxnrg65s6knfrs8m0c61fgxmmq99a6xckdvvh5gvw7z2myjvz8")))

(define-public crate-whirlpool-asm-0.5.1 (c (n "whirlpool-asm") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "03pbdxag7f8r9r47r85l7la21fnvvcj80rpyvj2fhns5yf3ah5gb")))

(define-public crate-whirlpool-asm-0.5.2 (c (n "whirlpool-asm") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15w0qsjhxdfqkk4mpvcp58zw7gwbr4pc8qs5y7zgminyn3l3pcnb")))

(define-public crate-whirlpool-asm-0.6.0 (c (n "whirlpool-asm") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1142988f6lail2vh7kpsx45piw6i4m5lk6nhiz3rf2l0ds2302ab")))

(define-public crate-whirlpool-asm-0.6.1 (c (n "whirlpool-asm") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1zi95snipjlclkcrz0hvdgd24y66dyr8619lb0kqldmvalsw7yv2")))

(define-public crate-whirlpool-asm-0.6.2 (c (n "whirlpool-asm") (v "0.6.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "061b51ajgf3f9rwgiw8zz0g3bmrwx6yw5jvlr9675cxz88j4fivd")))

