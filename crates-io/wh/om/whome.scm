(define-module (crates-io wh om whome) #:use-module (crates-io))

(define-public crate-whome-0.1.0 (c (n "whome") (v "0.1.0") (d (list (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "whoami") (r "^0.4") (d #t) (k 0)))) (h "14wf4pfcrssqg6ca0vyq0665ixhlm5jf8b835vvfrnwphggn7as2")))

(define-public crate-whome-0.1.1 (c (n "whome") (v "0.1.1") (d (list (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "whoami") (r "^0.4") (d #t) (k 0)))) (h "0ls5nlfb0i1wdkbjxvz5c8rjh9qcn700wp7x1fl0w362gardahym")))

(define-public crate-whome-0.2.0 (c (n "whome") (v "0.2.0") (d (list (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "whoami") (r "^0.5") (d #t) (k 0)))) (h "0ngc96mayz8zl5wxmfnkmbm8cdgyv7wn1bjgcv0x1y6ipr1i12x7")))

(define-public crate-whome-0.2.1 (c (n "whome") (v "0.2.1") (d (list (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "whoami") (r "^0.8") (d #t) (k 0)))) (h "0pgfs03nyw53icpnh6a7iqlrji37xkc8v1mvxs331h8r02am4naa")))

(define-public crate-whome-0.3.0 (c (n "whome") (v "0.3.0") (d (list (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "whoami") (r "^0.9") (d #t) (k 0)))) (h "0zpx46ya1lxnsngi94kmda486q5m3g0kqzsn9js7l2b7crn9qldx")))

(define-public crate-whome-0.4.0 (c (n "whome") (v "0.4.0") (d (list (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "whoami") (r "^1.0") (d #t) (k 0)))) (h "1vrvc33d3w0byk9r2y56jdnas9g119df5zlqk8kawrn1m8rlmfnz")))

(define-public crate-whome-0.5.0 (c (n "whome") (v "0.5.0") (d (list (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "whoami") (r "^1.2") (d #t) (k 0)))) (h "11m4j0y6bii1nw9jwzv3fwf4zpcx4cz6abgjwc0hdv0qv126bl0z")))

(define-public crate-whome-0.6.0 (c (n "whome") (v "0.6.0") (d (list (d (n "term") (r "^0.7") (d #t) (k 0)) (d (n "whoami") (r "^1.5.0") (d #t) (k 0)))) (h "13lhcavs8flmgcyr46vxf1cfax8q2l3y0hbk5h6ildrxj9c9gkhi")))

