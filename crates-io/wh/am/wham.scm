(define-module (crates-io wh am wham) #:use-module (crates-io))

(define-public crate-wham-0.9.0 (c (n "wham") (v "0.9.0") (d (list (d (n "GSL") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1xplk3pyjil47njk0ca0klwy4ifpkvv36s9n693z6lxkq1gimbl2") (f (quote (("default" "GSL/v2"))))))

(define-public crate-wham-0.9.1 (c (n "wham") (v "0.9.1") (d (list (d (n "GSL") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1gzskddl5xv1kp2ijmghmj721ybbb1jang7slp3sq9xhcqvxh377") (f (quote (("default" "GSL/v2"))))))

(define-public crate-wham-0.9.2 (c (n "wham") (v "0.9.2") (d (list (d (n "GSL") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1ir2z3arl6pjgwk9jljc3c2j3c2ksls12v81zhrxa0bli3wjx5w3") (f (quote (("default" "GSL/v2"))))))

(define-public crate-wham-0.9.3 (c (n "wham") (v "0.9.3") (d (list (d (n "GSL") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "07mp177nkgwjy3vywfld5zh1qxkray5k2k1m5lg65x24dbpclnjy") (f (quote (("default" "GSL/v2"))))))

(define-public crate-wham-0.9.4 (c (n "wham") (v "0.9.4") (d (list (d (n "GSL") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "02954kgzqraz0mpa8rmrjcshfz5qxhm7z5d6mbl0alpq2fxi37gs") (f (quote (("default" "GSL/v2"))))))

(define-public crate-wham-0.9.6 (c (n "wham") (v "0.9.6") (d (list (d (n "GSL") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0fmfbzv11jv7g3930r8pjjw0y57v6pcisjc03r6pz59abxh3llfx") (f (quote (("default" "GSL/v2"))))))

(define-public crate-wham-0.9.7 (c (n "wham") (v "0.9.7") (d (list (d (n "GSL") (r "^1.0.0") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "11zpqwjfxnpqpnrxzpqrzn4gsixawikh53lknn0rbp7g1rhcgam4") (f (quote (("default" "GSL/v2"))))))

(define-public crate-wham-0.9.8 (c (n "wham") (v "0.9.8") (d (list (d (n "GSL") (r "^1.0.0") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1fsb33x3944bcjff24p4md9zhvs29bl8nn2ykfi9sxpiyw3bfvcd") (f (quote (("default" "GSL/v2"))))))

(define-public crate-wham-0.9.9 (c (n "wham") (v "0.9.9") (d (list (d (n "GSL") (r "^1.1") (d #t) (k 0)) (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "0.7.*") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0a7vhp187g0wx944f10d2v9j75v1p3lfng086r4p1ady1hink5rb")))

(define-public crate-wham-1.0.0 (c (n "wham") (v "1.0.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "0.7.*") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0y6y381c18rmq4ihkgncmm8h52zpsgziaamnrxpl0vaz5qqfdy1g")))

(define-public crate-wham-1.1.0 (c (n "wham") (v "1.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "0.7.*") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0r99543gqjqwkrph5pmg3xjv5jxh9kynzc9im2y4x2c7748b5nnf")))

(define-public crate-wham-1.1.1 (c (n "wham") (v "1.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "0.7.*") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1n34zmzb67fsgn8c00p6sp8wi30hmkrndsshlaz9z4663kbs9lx5")))

(define-public crate-wham-1.1.2 (c (n "wham") (v "1.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "0.7.*") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1rciq442y6qigj68v22804ym811ydl7b2bsqn4h5vp3h9ira20km")))

(define-public crate-wham-1.1.3 (c (n "wham") (v "1.1.3") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "0.7.*") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "14hdl6ik0bms45cn72z9q0h79s69i861wvaz680rszsld0dmsbwg")))

(define-public crate-wham-1.1.4 (c (n "wham") (v "1.1.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^3.2.25") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0m9d3iyggpw1il9a5vikrgnbibr6gfcf70rv5yiy2czl96l6jsg0")))

