(define-module (crates-io wh oa whoa-german-numbers) #:use-module (crates-io))

(define-public crate-whoa-german-numbers-0.0.1 (c (n "whoa-german-numbers") (v "0.0.1") (h "1l26yh1lqmbdpvf8lalipv43amxfk65pp9gsqbn0w2n06pg9yrxb")))

(define-public crate-whoa-german-numbers-0.0.2 (c (n "whoa-german-numbers") (v "0.0.2") (h "1sb2p4lwkpr8c90x8ws0z1zwh6wb3wa5i651mh76wj94kh2hprra")))

