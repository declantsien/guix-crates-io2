(define-module (crates-io wh oa whoami) #:use-module (crates-io))

(define-public crate-whoami-0.1.0 (c (n "whoami") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.28") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (o #t) (d #t) (k 0)))) (h "0icgwng3slaglgh8j1ax8s165gc2l629v2wk1pf1g3d4dp6b7gla")))

(define-public crate-whoami-0.1.1 (c (n "whoami") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (o #t) (d #t) (k 0)))) (h "0cf9abc0lc93jny1zs3yk47h2gp44c68nndwd5v00g3pyfr1l456")))

(define-public crate-whoami-0.2.0 (c (n "whoami") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (o #t) (d #t) (k 0)))) (h "001k73v38dwhrvy9l23n4j0bivfhzlaads3c9p2gf2p4b4p06j7l")))

(define-public crate-whoami-0.2.1 (c (n "whoami") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0k6p1pmipahp7ckil8n7ff9r6pv403h6kab2ij5mh1zjl3cz3h6w")))

(define-public crate-whoami-0.2.2 (c (n "whoami") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1wak5w0cxfq7py3jhkb8v4iriinqyw7yv6ywgzl4zdsgjnzjna58")))

(define-public crate-whoami-0.2.3 (c (n "whoami") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.5") (o #t) (d #t) (k 0)))) (h "02rq3f53wf4kxg1giv3rxkbm5wisq0by0srj0yjxk6wx9dmf5hkq")))

(define-public crate-whoami-0.2.4 (c (n "whoami") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0lghdcq4cmv1p3j25b0vidvdrfvm3vcsh1iiamgjf1gc5cm5c39s")))

(define-public crate-whoami-0.3.0 (c (n "whoami") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1rh8bjfgd2mv31hwfz1kpm94liipy16cjc1v1x6m7lxcpfy1dbq7")))

(define-public crate-whoami-0.4.0 (c (n "whoami") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m17lwx60nhbkm2scy567ryp9hp7xmmmdpl6vj9n6c9yfdvddg7y")))

(define-public crate-whoami-0.4.1 (c (n "whoami") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x1bzf7bvc74vrglzhcsaklas43d38xzxrlcal65da06jq7j370v")))

(define-public crate-whoami-0.5.0 (c (n "whoami") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "106v852gxxgj6h1vh8mxdkzf5bv7lvqz22nj8y0f20q9y9ipga96")))

(define-public crate-whoami-0.5.1 (c (n "whoami") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l4nz4p39qxz8fcl7xk29pis0dkx1id84ml6ymd2a32bcqnhvnvy")))

(define-public crate-whoami-0.5.2 (c (n "whoami") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ygvnddglilpf9cm0ncn2hy0a0y9w7q3j4nr3lm5ng056kxgff96")))

(define-public crate-whoami-0.5.3 (c (n "whoami") (v "0.5.3") (h "1wvj97y0hlsh84nc273k7gj7nkj4vwrkdmnzi60s6wc093sg6m3i")))

(define-public crate-whoami-0.6.0 (c (n "whoami") (v "0.6.0") (d (list (d (n "stdweb") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1azw17wadmxxmw9n62yxbijvifrndx7xyfns9w71l3qvwgr0syy4")))

(define-public crate-whoami-0.7.0 (c (n "whoami") (v "0.7.0") (h "1sg8lj61j9hy489760qixbdd4lqc5m9zp2klk3hplaz18ly6cak2")))

(define-public crate-whoami-0.8.0 (c (n "whoami") (v "0.8.0") (h "16y6nvb3rsvlvi0wijfy4rx93r5v0nqy77mnmpy9zxkn3pmf2gsi")))

(define-public crate-whoami-0.8.1 (c (n "whoami") (v "0.8.1") (h "14f3vqv6x7pli2nnm5vv9ip30zkzvvp5cidrh4g8isjqn52bi3m0")))

(define-public crate-whoami-0.8.2 (c (n "whoami") (v "0.8.2") (h "0z18m6w2q8a6rivd61sh3f00pdhyvxiwycs2j5088gvgdxb5bfqq")))

(define-public crate-whoami-0.9.0 (c (n "whoami") (v "0.9.0") (d (list (d (n "cala_core") (r "^0.1") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "012mw2q72gpmf354yw2qc5w105ziac75shpqp1f62x4hnqx7g13q") (f (quote (("wasm-bindgen" "cala_core/wasm-bindgen") ("stdweb" "cala_core/stdweb") ("default") ("cala" "cala_core/cala"))))))

(define-public crate-whoami-1.0.0 (c (n "whoami") (v "1.0.0") (d (list (d (n "wasm-bindgen") (r ">=0.2.0, <0.3.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r ">=0.3.0, <0.4.0") (f (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1n42rcsi8bbd00inm1sw9kxrlh8aiww9pn5av2bm3x4n3661pf70") (f (quote (("default"))))))

(define-public crate-whoami-1.0.1 (c (n "whoami") (v "1.0.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0d90722ay0dlbgakxghmx5wpiibsqgcjb5wf382mgiplzbkram73") (f (quote (("default"))))))

(define-public crate-whoami-1.0.2 (c (n "whoami") (v "1.0.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0mvj18k7463g2armwf3n7p8wjg0nv1vany405ld9grh5zpgk4xzd") (f (quote (("default"))))))

(define-public crate-whoami-1.0.3 (c (n "whoami") (v "1.0.3") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0dmlxqq6758wysx46zr64b4q9g72d0ax9f4hlpb86qgk8vhv55fm") (f (quote (("default"))))))

(define-public crate-whoami-1.1.0 (c (n "whoami") (v "1.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "07pf27fnx54awhwn2l6sc46yj4gdy2arpfqbdh5ir9bqsl51r4hs") (f (quote (("default"))))))

(define-public crate-whoami-1.1.1 (c (n "whoami") (v "1.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0w5w785g94fwr657lyyrlcpjnmimnc7xm9gbqg2s5jwk15anya8y") (f (quote (("default"))))))

(define-public crate-whoami-1.1.2 (c (n "whoami") (v "1.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1xiaddvcd54yl9jyk3lnw3gvc0d9y8viv4s622pzx3cmbhrczfja") (f (quote (("default"))))))

(define-public crate-whoami-1.1.3 (c (n "whoami") (v "1.1.3") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1qbs5lvc076xc85pg3scrxw43yjfais5bykdr5ksh002lihi2x7p") (f (quote (("default"))))))

(define-public crate-whoami-1.1.4 (c (n "whoami") (v "1.1.4") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1w0zpl1imagklg2dmbmrpyr55b3j0knxjnhbgsai2rlklhmf5gya") (f (quote (("default"))))))

(define-public crate-whoami-1.1.5 (c (n "whoami") (v "1.1.5") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "05v1nsf82sf9mmgkm2s7pi70zway66s2xg08xf8fqgx9w7z5jfj8") (f (quote (("default"))))))

(define-public crate-whoami-1.2.0 (c (n "whoami") (v "1.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1xzmdsp0d8kvjm99l5a4ly9ivif0dhnf2rw9r7rgnkka4gpcafn3") (f (quote (("default"))))))

(define-public crate-whoami-1.2.1 (c (n "whoami") (v "1.2.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1s355zs8ir1li29cwvzgaqm6jyb51svmhqyx2hqgp8i0bbx5hjsj") (f (quote (("default"))))))

(define-public crate-whoami-1.2.2 (c (n "whoami") (v "1.2.2") (d (list (d (n "bumpalo") (r ">= 3.0, <= 3.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r ">= 0.2, <= 0.2.78") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "05cz6s3rpq2x1c5xpkanadffzw42c47rw4f4vbxa7i0mc53gmnaz") (f (quote (("default"))))))

(define-public crate-whoami-1.2.3 (c (n "whoami") (v "1.2.3") (d (list (d (n "bumpalo") (r "^3.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0wfmrfaaqh41rrj08n1ddyjg07i4mnkz3s12nr0ii6ym5xm1nqyn") (f (quote (("default"))))))

(define-public crate-whoami-1.3.0 (c (n "whoami") (v "1.3.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (t "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (o #t) (d #t) (t "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (k 0)))) (h "1qr6ais0yikqyf8bh05ry854lkhpxkfkggd9c717v8nw1hgwgns5") (f (quote (("web" "web-sys" "wasm-bindgen") ("default" "web"))))))

(define-public crate-whoami-1.4.0 (c (n "whoami") (v "1.4.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (t "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (o #t) (d #t) (t "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (k 0)))) (h "0s4zdcmikx7blgavwkilw7skgz5h7i98jkl69v09qh6a29226w1c") (f (quote (("web" "web-sys" "wasm-bindgen") ("default" "web"))))))

(define-public crate-whoami-1.4.1 (c (n "whoami") (v "1.4.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (t "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (o #t) (d #t) (t "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (k 0)))) (h "0l6ca9pl92wmngsn1dh9ih716v216nmn2zvcn94k04x9p1b3gz12") (f (quote (("web" "web-sys" "wasm-bindgen") ("default" "web"))))))

(define-public crate-whoami-1.5.0-pre.0 (c (n "whoami") (v "1.5.0-pre.0") (d (list (d (n "wasite") (r "^0.1") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"wasi\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (t "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (o #t) (d #t) (t "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (k 0)))) (h "0bm06f8w7zp1gkww621kz22fnyc4j05m7miywpw307z6s1x4pmd7") (f (quote (("web" "web-sys" "wasm-bindgen") ("default" "web"))))))

(define-public crate-whoami-1.5.0 (c (n "whoami") (v "1.5.0") (d (list (d (n "redox_syscall") (r "^0.4") (d #t) (t "cfg(all(target_os = \"redox\", not(target_arch = \"wasm32\")))") (k 0)) (d (n "wasite") (r "^0.1") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"wasi\"))") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (o #t) (d #t) (t "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (k 0)))) (h "0gl9f1rd8yyyiv459kprgn9i4k46qa7qxlbf89iq27xl90fpiv0g") (f (quote (("web" "web-sys") ("default" "web")))) (r "1.40")))

(define-public crate-whoami-1.5.1 (c (n "whoami") (v "1.5.1") (d (list (d (n "redox_syscall") (r "^0.4") (d #t) (t "cfg(all(target_os = \"redox\", not(target_arch = \"wasm32\")))") (k 0)) (d (n "wasite") (r "^0.1") (d #t) (t "cfg(all(target_arch = \"wasm32\", target_os = \"wasi\"))") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Navigator" "Document" "Window" "Location"))) (o #t) (d #t) (t "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (k 0)))) (h "1aafr70h2zlqr73i58bj84hdf9rgplxbpygqbgsqhkk3mngv8jm4") (f (quote (("web" "web-sys") ("default" "web")))) (r "1.40")))

