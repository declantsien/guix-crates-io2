(define-module (crates-io wh y- why-not) #:use-module (crates-io))

(define-public crate-why-not-0.1.0 (c (n "why-not") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "18788j37ylna97b8awr0lwfkb0vhd84f0b1v54014s4nmzibn84x")))

