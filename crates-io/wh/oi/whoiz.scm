(define-module (crates-io wh oi whoiz) #:use-module (crates-io))

(define-public crate-whoiz-0.1.0 (c (n "whoiz") (v "0.1.0") (d (list (d (n "resolve") (r "^0.2") (d #t) (k 0)))) (h "1d9y8b1i27xwmiwsqjnfq1cv2144bq5zj26akkv93yrhqhnvxfml")))

(define-public crate-whoiz-0.1.1 (c (n "whoiz") (v "0.1.1") (d (list (d (n "resolve") (r "^0.2") (d #t) (k 0)))) (h "0ka4fc7gnl2aj59h26a9r8hdyj5f4pry6p5yidpc7b8p4bify6hm")))

(define-public crate-whoiz-0.1.2 (c (n "whoiz") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "resolve") (r "^0.2") (d #t) (k 0)))) (h "168lrabg8d819z9whdlhgndm5d70kg2kd80m0ammjw5qdfg3v8rl")))

