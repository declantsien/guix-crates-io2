(define-module (crates-io wh oi whoisthere) #:use-module (crates-io))

(define-public crate-whoisthere-0.1.2 (c (n "whoisthere") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "153p9lc0hknyapq2sharfag8azmhfy49aclksl32xmdjg0am2yr0")))

(define-public crate-whoisthere-0.1.3 (c (n "whoisthere") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "04wiln65b6r4yd43blzb8lf1hhc6pf2icd4lrpmmdsqvmbi9596y")))

