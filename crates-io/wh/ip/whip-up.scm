(define-module (crates-io wh ip whip-up) #:use-module (crates-io))

(define-public crate-whip-up-0.1.0 (c (n "whip-up") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1msk7rdc3cisjy3130j6yqifjs2y8zp5c1rl0qig9cziklk258yd")))

