(define-module (crates-io wh as whasm-grammar-derive) #:use-module (crates-io))

(define-public crate-whasm-grammar-derive-0.1.0 (c (n "whasm-grammar-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0cms0p8rdcvg458nnavjsskx6wrvyprrqar703v2v98h1z3k0bq3")))

(define-public crate-whasm-grammar-derive-0.1.1 (c (n "whasm-grammar-derive") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "08d652471lbydm7dg7h5vba44nflqls3p5ff7vyb5x26p5xrawzk")))

