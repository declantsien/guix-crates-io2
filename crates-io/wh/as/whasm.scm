(define-module (crates-io wh as whasm) #:use-module (crates-io))

(define-public crate-whasm-0.0.1 (c (n "whasm") (v "0.0.1") (h "0fzya6c5cq0r3b3vsx5n2ldcjv6dl0irlv8glmjcznvs3qd9b69i")))

(define-public crate-whasm-0.1.0 (c (n "whasm") (v "0.1.0") (d (list (d (n "err-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "whasm-grammar-derive") (r "^0.1") (d #t) (k 0)))) (h "0cmyb6bj73x7i4m1mjkyh2vrbh6j09dymh134398rnc5mz17v2pm")))

(define-public crate-whasm-0.1.1 (c (n "whasm") (v "0.1.1") (d (list (d (n "err-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "whasm-grammar-derive") (r "^0.1") (d #t) (k 0)))) (h "1w9yjmpil77gl2rb08i3hdlmq0q568r887xd1nxciaca37l2lfs1")))

