(define-module (crates-io wh dp whdp) #:use-module (crates-io))

(define-public crate-whdp-1.0.0 (c (n "whdp") (v "1.0.0") (h "0xxq6icdnl0x9y9gjg0vw7x2yx10sgvfn7f4ngfgph6q9kayg270")))

(define-public crate-whdp-1.0.1 (c (n "whdp") (v "1.0.1") (h "1k5dsp7xwg0b4jwwv5ii6z1578mnd8r9pch0s974p9v7za8qci5y")))

(define-public crate-whdp-1.1.1 (c (n "whdp") (v "1.1.1") (h "10j4g8ga0sjc730vmkfqdnp0vmhfjjwhyhfr5vkviia79a3nrbkl")))

(define-public crate-whdp-1.1.2 (c (n "whdp") (v "1.1.2") (h "1qxzfgiwj8ki6iswa66jp41lmkwlqm06329s6csg3bibaw6dwjkr")))

(define-public crate-whdp-1.1.3 (c (n "whdp") (v "1.1.3") (h "0pixjqvqwgbf823ip36fbygynrwxbx2h9w44f1adxidn58mqcppd")))

(define-public crate-whdp-1.1.4 (c (n "whdp") (v "1.1.4") (h "0ajn3knha6cmmrf5j7kfl390b8mi5cbqzphb8qrccckjf1zdnvk7")))

(define-public crate-whdp-1.1.5 (c (n "whdp") (v "1.1.5") (h "1m02qzm29kym0k4cpkwzay7yizi1wil30j6i52npc51qdgivls3m")))

(define-public crate-whdp-1.1.6 (c (n "whdp") (v "1.1.6") (h "05illmgz40bqx56s0823awam0332crvgiflpfgg5b2jjr1v5iw51")))

(define-public crate-whdp-1.1.7 (c (n "whdp") (v "1.1.7") (d (list (d (n "wjp") (r "^1.0.1") (d #t) (k 0)))) (h "1y4gi9p6cman8xjqfji0q9kskif62dc7qfp85jp114bx0g7pzsgq")))

(define-public crate-whdp-1.1.8 (c (n "whdp") (v "1.1.8") (d (list (d (n "wjp") (r "^1.0.1") (d #t) (k 0)))) (h "097r0nc6rs3zpc6hhijyqbzqzxmxfd4kcagya9m3k6dkh1vgl1j6")))

(define-public crate-whdp-1.1.9 (c (n "whdp") (v "1.1.9") (d (list (d (n "wjp") (r "^1.0.2") (d #t) (k 0)))) (h "0nsc367c0fqbc7sl6yxkrdypxpcb3scq2k0cvl3p730rxb78llkm")))

(define-public crate-whdp-1.1.10 (c (n "whdp") (v "1.1.10") (d (list (d (n "wjp") (r "^1.0.2") (d #t) (k 0)))) (h "0qa5scm24rbmiinf8p1lgq29vdvpa783bmix2lq637fby98qby86")))

(define-public crate-whdp-1.1.11 (c (n "whdp") (v "1.1.11") (d (list (d (n "wjp") (r "^1.1.1") (d #t) (k 0)))) (h "1lkylbnzkbf2xyalq5f8kiz1r5pv0m09arzwl3khsa4nmnxk8rjc")))

(define-public crate-whdp-1.2.0 (c (n "whdp") (v "1.2.0") (d (list (d (n "wjp") (r "^1.1.3") (d #t) (k 0)))) (h "1gkyn2nprvk0pfinq02nwbpx6kviz1hy37iv2zilh4wky0gvd69n")))

