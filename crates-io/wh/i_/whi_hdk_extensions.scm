(define-module (crates-io wh i_ whi_hdk_extensions) #:use-module (crates-io))

(define-public crate-whi_hdk_extensions-0.1.0 (c (n "whi_hdk_extensions") (v "0.1.0") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.1") (d #t) (k 0)))) (h "05zaffzv1fbkvd58xca8ld7p4b9a8pajylcwc2f5rrjjhqdbyh8x")))

(define-public crate-whi_hdk_extensions-0.1.1 (c (n "whi_hdk_extensions") (v "0.1.1") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.1") (d #t) (k 0)))) (h "174f2y5j2im938zir7s8cqhifqxy6w5iwk8nhw6d0gb5xvl8m001")))

(define-public crate-whi_hdk_extensions-0.1.2 (c (n "whi_hdk_extensions") (v "0.1.2") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.1") (d #t) (k 0)))) (h "1q76zlnwgk76rqrp7xi7f7fz9rpixi84dsrnhzxsj1qd29f6af4a")))

(define-public crate-whi_hdk_extensions-0.1.3 (c (n "whi_hdk_extensions") (v "0.1.3") (d (list (d (n "hdk") (r "=0.3.0-beta-dev.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.1") (d #t) (k 0)))) (h "19xjggbg2wk9b9mivm5lyzf50sjgz559ymlwqczbdy3ncyp7935p")))

(define-public crate-whi_hdk_extensions-0.2.0 (c (n "whi_hdk_extensions") (v "0.2.0") (d (list (d (n "hdk") (r "=0.2.1-beta-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.2") (d #t) (k 0)))) (h "1fmsf6yhkyvpqww4iqwsp1cspslxddgrmpsnxdwxfghgji1q55zl")))

(define-public crate-whi_hdk_extensions-0.2.1 (c (n "whi_hdk_extensions") (v "0.2.1") (d (list (d (n "hdk") (r "=0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.2") (d #t) (k 0)))) (h "1z3m561yv8i7iyfanj72iwpcv7hq1l597njmbjm37qvy5v3sf05n")))

(define-public crate-whi_hdk_extensions-0.3.0 (c (n "whi_hdk_extensions") (v "0.3.0") (d (list (d (n "hdk") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.3.0") (d #t) (k 0)))) (h "17avbk1v2nc1gw9gqmpq226b1imw55d80jlr8h7l8vqz4is14iqn")))

(define-public crate-whi_hdk_extensions-0.4.0 (c (n "whi_hdk_extensions") (v "0.4.0") (d (list (d (n "hdk") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.4.0") (d #t) (k 0)))) (h "0w08m13p3j5bpjj4x0g1ivqqj0154fb2nl0837xd66ryya5ycmd1")))

(define-public crate-whi_hdk_extensions-0.4.1 (c (n "whi_hdk_extensions") (v "0.4.1") (d (list (d (n "hdk") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.4.0") (d #t) (k 0)))) (h "1s0agdm8660r3gghmxqh05305l2yzbjisqhbyjk85mw28ibnvsr9")))

(define-public crate-whi_hdk_extensions-0.5.0 (c (n "whi_hdk_extensions") (v "0.5.0") (d (list (d (n "hdk") (r "^0.2.7-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.5.0") (d #t) (k 0)))) (h "08vaf3l6jgc0wskv11qr9wka4zmxxs1d02dvf60wgpkx7ahrf8f7")))

(define-public crate-whi_hdk_extensions-0.6.0 (c (n "whi_hdk_extensions") (v "0.6.0") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.34") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.6") (d #t) (k 0)))) (h "1g1lm86dkx5mg9ckacja6a3k3qm0s5z3144n2d3973shrm7vgsy6")))

(define-public crate-whi_hdk_extensions-0.7.0 (c (n "whi_hdk_extensions") (v "0.7.0") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.38") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.7") (d #t) (k 0)))) (h "1fwy3z8rm2wvp7bbvzr3cvpki941iijr813hnf55jxiqdm2k57z2")))

(define-public crate-whi_hdk_extensions-0.8.0-dev.0 (c (n "whi_hdk_extensions") (v "0.8.0-dev.0") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.41") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.8.0-dev.0") (d #t) (k 0)))) (h "0zmg0yp96w44k0df7gpkx696swmzbjrj1mbkvrhmrrckv7dqj9nx")))

(define-public crate-whi_hdk_extensions-0.8.0 (c (n "whi_hdk_extensions") (v "0.8.0") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.41") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.8.0") (d #t) (k 0)))) (h "1f3x26hg5md9d625jgc9pi4y9iqx11vrjibc9y3dfhrx139y4mmm")))

(define-public crate-whi_hdk_extensions-0.9.0 (c (n "whi_hdk_extensions") (v "0.9.0") (d (list (d (n "hdk") (r "^0.4.0-dev.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "whi_hdi_extensions") (r "^0.9") (d #t) (k 0)))) (h "0r48wx7wlcg9vx2awwxg6r253qd1yrzm1slq3z4l6lapz6sczlk1")))

