(define-module (crates-io wh i_ whi_hdi_extensions) #:use-module (crates-io))

(define-public crate-whi_hdi_extensions-0.1.0 (c (n "whi_hdi_extensions") (v "0.1.0") (d (list (d (n "hdi") (r "^0.4.0-beta-dev.1") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.1") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1fpygx8xig37gpcf6xy7si3kinpyanyqm4l35xl64k2gb9xgykk1")))

(define-public crate-whi_hdi_extensions-0.1.1 (c (n "whi_hdi_extensions") (v "0.1.1") (d (list (d (n "hdi") (r "^0.4.0-beta-dev.1") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.1") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0q3c9dycxwglljhsv0japzya2bxsh1d2wyd5m2wjr3n2bcdrx5b2")))

(define-public crate-whi_hdi_extensions-0.1.2 (c (n "whi_hdi_extensions") (v "0.1.2") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.1") (d #t) (k 0)) (d (n "holo_hash") (r "=0.3.0-beta-dev.1") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "12hfp2ch8rxsgmkh7a03w75n8gs435jg04mm145bkx82r39c72m1")))

(define-public crate-whi_hdi_extensions-0.2.0 (c (n "whi_hdi_extensions") (v "0.2.0") (d (list (d (n "hdi") (r "=0.3.1-beta-rc.0") (d #t) (k 0)) (d (n "holo_hash") (r "=0.2.1-beta-rc.0") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1kg13ph27bvr0k7ffn9h9nzlz6cnjfkrk37wr1xd07gkz17vzvhx")))

(define-public crate-whi_hdi_extensions-0.2.1 (c (n "whi_hdi_extensions") (v "0.2.1") (d (list (d (n "hdi") (r "=0.3.1") (d #t) (k 0)) (d (n "holo_hash") (r "=0.2.1") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1h3p3zzi7arlh194l76ajc3zd7mfdg4n73y17r60kx4h3ipsvqai")))

(define-public crate-whi_hdi_extensions-0.3.0 (c (n "whi_hdi_extensions") (v "0.3.0") (d (list (d (n "hdi") (r "=0.3.2") (d #t) (k 0)) (d (n "holo_hash") (r "=0.2.2") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "14dgi87zzn6pfkd5xm9q8lcg8j74fy49lfpd06zj5n7v7zplfh67")))

(define-public crate-whi_hdi_extensions-0.4.0 (c (n "whi_hdi_extensions") (v "0.4.0") (d (list (d (n "hdi") (r "=0.3.2") (d #t) (k 0)) (d (n "holo_hash") (r "=0.2.2") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "17id6csws6y4h3vb37ami2lyr4rd4r7lpf1jxxl8sw4i3zjdqm1f")))

(define-public crate-whi_hdi_extensions-0.4.1 (c (n "whi_hdi_extensions") (v "0.4.1") (d (list (d (n "hdi") (r "^0.3.2") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.2") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1llr2pqfc2g3jbhkp1dm1vg2vhimp6kxk0rsw1g0zp3l8wkblq5h")))

(define-public crate-whi_hdi_extensions-0.4.2 (c (n "whi_hdi_extensions") (v "0.4.2") (d (list (d (n "hdi") (r "^0.3.2") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.2") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1yzd7hs9klvs8k3mdx8577swm4ficrsglixrqazm0l052gqxp7vm")))

(define-public crate-whi_hdi_extensions-0.5.0 (c (n "whi_hdi_extensions") (v "0.5.0") (d (list (d (n "hdi") (r "^0.3.7-rc.0") (d #t) (k 0)) (d (n "holo_hash") (r "^0.2.6") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "10ilaxsh5aq8syhicsnmmrqzy04z9vcp3g3q2aya6xv8c6913p5z")))

(define-public crate-whi_hdi_extensions-0.6.0 (c (n "whi_hdi_extensions") (v "0.6.0") (d (list (d (n "hdi") (r "^0.4.0-beta-dev.30") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.24") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0max1c6l26h75fgvm1qcyigiwryrai1zk6h3m481vycirlggd5ag")))

(define-public crate-whi_hdi_extensions-0.7.0 (c (n "whi_hdi_extensions") (v "0.7.0") (d (list (d (n "hdi") (r "^0.4.0-beta-dev.34") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.26") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "09zbzh2g2b54b167ahxg5gbzml5775xx3vcxx3rrdcdlrfss5s05")))

(define-public crate-whi_hdi_extensions-0.8.0-dev.0 (c (n "whi_hdi_extensions") (v "0.8.0-dev.0") (d (list (d (n "hdi") (r "^0.4.0-beta-dev.36") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.28") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1l0slqgq9i58pfmf4290ib9dir179mz5vnshrjj0j89czvlcc16m")))

(define-public crate-whi_hdi_extensions-0.8.0 (c (n "whi_hdi_extensions") (v "0.8.0") (d (list (d (n "hdi") (r "^0.4.0-beta-dev.36") (d #t) (k 0)) (d (n "holo_hash") (r "^0.3.0-beta-dev.28") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "10r9vd48516i8nj4bg5ij17v9pf7zkmfqymamg2pskkj2pw3rc38")))

(define-public crate-whi_hdi_extensions-0.9.0 (c (n "whi_hdi_extensions") (v "0.9.0") (d (list (d (n "hdi") (r "^0.5.0-dev.1") (d #t) (k 0)) (d (n "holo_hash") (r "^0.4.0-dev.1") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "09biirkd9mjwgj1giad7c7vc9piz9a37wk3npmajmfdvwyv0nb07")))

