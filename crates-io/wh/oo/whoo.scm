(define-module (crates-io wh oo whoo) #:use-module (crates-io))

(define-public crate-whoo-0.1.0 (c (n "whoo") (v "0.1.0") (h "101rjwpr0wzxqs8x5rp96jlx65i5rh8mxgq8wz0wdl1ih28v7wf1")))

(define-public crate-whoo-0.1.1 (c (n "whoo") (v "0.1.1") (h "0n389zpsf7fpaa6rxdz6a6k9iqbbfl040kpmq9kzvyj1pmfmcahg")))

(define-public crate-whoo-0.1.2 (c (n "whoo") (v "0.1.2") (h "09p6pph7hjn1wqx7mzpj5fz7z9rqb8ly7p4kk24cyg4gb7xf6nh7")))

(define-public crate-whoo-0.1.3 (c (n "whoo") (v "0.1.3") (h "0aj1043d1m51s1h20wmw1mankhh3fwjrs2jg8p64bh4zprvg1pwd")))

