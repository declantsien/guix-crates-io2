(define-module (crates-io wh il whiledb) #:use-module (crates-io))

(define-public crate-whiledb-0.1.0 (c (n "whiledb") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "santiago") (r "^1.3.1") (d #t) (k 0)))) (h "0al17impq90i0ld3qjqwxd8f1b4pwk9yvhrqh8lzkmy59sffzrjq")))

(define-public crate-whiledb-0.1.1 (c (n "whiledb") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "santiago") (r "^1.3.1") (d #t) (k 0)))) (h "0qsbpbq6214wwrkir5bkdyr1dxjkx80mxzccm4b07gfw7kvm9nw8")))

(define-public crate-whiledb-0.1.2 (c (n "whiledb") (v "0.1.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "santiago") (r "^1.3.1") (d #t) (k 0)))) (h "1j7z58vvmklp5hj3hp804f1w5m2lcpx7zsp2iympkd8c6jk6fryx")))

(define-public crate-whiledb-0.1.3 (c (n "whiledb") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "santiago") (r "^1.3.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0fq22wmr3l4wq1gpbpydybvgjfajdn3x4iahwvjdxhia8gk4qlqm")))

(define-public crate-whiledb-0.1.4 (c (n "whiledb") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "santiago") (r "^1.3.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "16994j89m8pjc5c7rd56lkniyxjgaj05cs3l5291nwgpyfnqyiqj")))

