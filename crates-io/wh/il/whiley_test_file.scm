(define-module (crates-io wh il whiley_test_file) #:use-module (crates-io))

(define-public crate-whiley_test_file-0.5.0 (c (n "whiley_test_file") (v "0.5.0") (h "1g4cxh9l8xpy70h22vcxwdlh0115fnr33hxfc66fih2v77yqrhkc")))

(define-public crate-whiley_test_file-0.6.0 (c (n "whiley_test_file") (v "0.6.0") (h "16jhp9rqd5p9vsbq00q4vrv5pah0v6dkqg8l58wn4054kgg0xra8")))

(define-public crate-whiley_test_file-0.6.1 (c (n "whiley_test_file") (v "0.6.1") (h "14z9nnwy4aly2ylizhlrrdcr61m8mvz3rqj0vx3f07w0dcyz3hba")))

(define-public crate-whiley_test_file-0.6.2 (c (n "whiley_test_file") (v "0.6.2") (h "1jr6mcplzjwm5pdkgl8wcmgw45x0n5kqr4c2biz67ahrjknjswzv")))

