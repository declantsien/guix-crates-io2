(define-module (crates-io wh il whiley) #:use-module (crates-io))

(define-public crate-whiley-0.1.0 (c (n "whiley") (v "0.1.0") (h "0cmdyqx3d8l2wqlyw8lr7pwhsg66b11jryw1k71il4gp05adkhyd")))

(define-public crate-whiley-0.2.0 (c (n "whiley") (v "0.2.0") (h "1f0raz3chm94acgjiwcfr6kx44fmhycv2040yrp4b30h2cyddx4i")))

(define-public crate-whiley-0.3.0 (c (n "whiley") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0jqdp02zs8s5s22h5fz6bbp0a2x5c8apivscc6a29qyij104afks")))

(define-public crate-whiley-0.3.1 (c (n "whiley") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "j4rs") (r "^0.13") (d #t) (k 0)))) (h "04i56bnbxqrxgy7j8ndxkv3bpdak2a10lhckxsbw6shdv2b64q99")))

(define-public crate-whiley-0.3.2 (c (n "whiley") (v "0.3.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0lkzdf33v3x9ssahcf0vg776akk9ljh8px5jm59d2l5xh7s7fn1n")))

(define-public crate-whiley-0.3.3 (c (n "whiley") (v "0.3.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1mzxrbd92pimp676v1vf8px6dd0bjwvrqk9ccznam7xswjmij365")))

(define-public crate-whiley-0.3.4 (c (n "whiley") (v "0.3.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "045a8kb44pcrlygas9bd892vcy08vn7s1awhls3jflxy5mp11ivc")))

(define-public crate-whiley-0.3.5 (c (n "whiley") (v "0.3.5") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1984x9d3rlzxxl7araqavn8zqmr0zipn79860ii6a1qgcc5lz5wv")))

(define-public crate-whiley-0.3.6 (c (n "whiley") (v "0.3.6") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0k55i6lz3hr0kn6xvgb2rd677bzs44gj3wjmmf7myq0d49ljpbpc")))

(define-public crate-whiley-0.6.0 (c (n "whiley") (v "0.6.0") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0g24sqm8cpb3h1sylx52fkvx1zbamn80xrhxyaw9hng20qg9n1vq")))

(define-public crate-whiley-0.6.1 (c (n "whiley") (v "0.6.1") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0g45gpx7d88yiaxahb58a86zi69mk3zfrljv4l5mxc78g1g11pbb")))

(define-public crate-whiley-0.6.3 (c (n "whiley") (v "0.6.3") (d (list (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1rxvl297vln6dbvgr3n0n62kwlgsyc8dyrhlb50jd6x0ipvbqp2r")))

