(define-module (crates-io wh ee wheelsticks) #:use-module (crates-io))

(define-public crate-wheelsticks-1.0.0 (c (n "wheelsticks") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("color" "derive" "error-context" "help" "std" "suggestions" "usage" "wrap_help"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "test-case") (r "^3.2") (d #t) (k 2)))) (h "137ybrg278rbdrx6ym4aq1gn2xirmn1d3nk28p9m4q0p7604vjis")))

