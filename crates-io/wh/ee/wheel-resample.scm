(define-module (crates-io wh ee wheel-resample) #:use-module (crates-io))

(define-public crate-wheel-resample-0.1.0 (c (n "wheel-resample") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)))) (h "1i85y7n58vkcd5mgvkcs8zd3rzm5jv60rzz6lrq2kib8hm9szd7j") (y #t)))

(define-public crate-wheel-resample-0.1.1 (c (n "wheel-resample") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)))) (h "0p7fxmsg7mmw8id0ga7rkfvigq5qrc8ad78j0n8z9jdj92cq48k2") (y #t)))

(define-public crate-wheel-resample-0.1.3 (c (n "wheel-resample") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)))) (h "09igjrxrqpcmsi16dy1xpg9i8s6s3a402n2f57gqpv6mpcjic4q8")))

(define-public crate-wheel-resample-0.1.4 (c (n "wheel-resample") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)))) (h "18a0bz6kk3z7g5qmbqspy8nwfr8hc3mibhagm8s1x1b28k4j17wb")))

