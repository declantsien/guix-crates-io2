(define-module (crates-io wh ee wheel-timer2) #:use-module (crates-io))

(define-public crate-wheel-timer2-0.1.0 (c (n "wheel-timer2") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.6") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time" "macros" "rt" "net" "sync"))) (d #t) (k 2)) (d (n "tokio-tungstenite") (r "^0.16") (d #t) (k 2)))) (h "1x3w6ir1nnzyhifyg3zsbmq4gwanx8xn136pzsnnp4azv6zfx0yi")))

(define-public crate-wheel-timer2-0.1.1 (c (n "wheel-timer2") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.6") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time" "macros" "rt" "net" "sync"))) (d #t) (k 2)) (d (n "tokio-tungstenite") (r "^0.16") (d #t) (k 2)))) (h "0bjfw06jz5610qj5aca4av3ncr54ljcvbiqlmbyz4ylkn59bz3b5")))

(define-public crate-wheel-timer2-0.1.2 (c (n "wheel-timer2") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.6") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time" "macros" "rt" "net" "sync"))) (d #t) (k 2)) (d (n "tokio-tungstenite") (r "^0.16") (d #t) (k 2)))) (h "029pia7bh7rgwd4jx3di4nrcaxjpnmficxlwpg1qcvc7rpmxzbb5")))

