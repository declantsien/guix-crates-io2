(define-module (crates-io wh ee wheelhoss) #:use-module (crates-io))

(define-public crate-wheelhoss-0.1.2 (c (n "wheelhoss") (v "0.1.2") (d (list (d (n "file_diff") (r "^1") (d #t) (k 2)) (d (n "fs3") (r "^0.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1smbqxr00bwnzkv5416r41dpj2va16da4ks529cjh76swnr8aj68")))

(define-public crate-wheelhoss-0.1.3 (c (n "wheelhoss") (v "0.1.3") (d (list (d (n "file_diff") (r "^1") (d #t) (k 2)) (d (n "fs3") (r "^0.5") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0pi61yyir6m0yql38597qm4ahngayy8frzgg31ir4wdiz0sn6ab7")))

