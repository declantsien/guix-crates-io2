(define-module (crates-io wh ee wheel_timer) #:use-module (crates-io))

(define-public crate-wheel_timer-0.1.1 (c (n "wheel_timer") (v "0.1.1") (h "190s942ywvdnklsn9xh82badjakk3kkpg3w6fzmliga26c6n7d6v")))

(define-public crate-wheel_timer-0.2.0 (c (n "wheel_timer") (v "0.2.0") (h "1f7aaq8mwwhqjgy6h82clk04j81lnp2ammm09wq3qcgkg9q6zgrd")))

(define-public crate-wheel_timer-0.2.1 (c (n "wheel_timer") (v "0.2.1") (h "11ckdm2asp71qyl5hga5b8zyrvj9j1v9cs5yfhi209ijiss1j902")))

(define-public crate-wheel_timer-0.2.2 (c (n "wheel_timer") (v "0.2.2") (h "0kikwlwi6ny95lxkpxy3m2zk0mqyj95piysqi21j195lnzzmm2zi")))

(define-public crate-wheel_timer-0.3.0 (c (n "wheel_timer") (v "0.3.0") (h "0mvlbyk7w2kd3qyf4l6p4kiqjy70si7l34lfkqa7h8vldmbmfas2")))

(define-public crate-wheel_timer-0.3.1 (c (n "wheel_timer") (v "0.3.1") (h "00vm9cpaggpsq8xprikqqp59mi2hnvpcglkx2drkhz90gxrg12zk")))

