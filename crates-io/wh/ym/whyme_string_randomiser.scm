(define-module (crates-io wh ym whyme_string_randomiser) #:use-module (crates-io))

(define-public crate-whyme_string_randomiser-0.1.0 (c (n "whyme_string_randomiser") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_seeder") (r "^0.2.3") (d #t) (k 0)))) (h "179lfrs7p1gl9gpb2198f746wwlllqnfxmvy8zqijrxg7kiw1hrq")))

