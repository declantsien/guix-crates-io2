(define-module (crates-io wh ic whichlicense_classification) #:use-module (crates-io))

(define-public crate-whichlicense_classification-0.0.2 (c (n "whichlicense_classification") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pshpq7yndqlsinmyxp3fv38sg418fqgwnl1y96dlwdr3l9wajvv")))

(define-public crate-whichlicense_classification-0.0.3 (c (n "whichlicense_classification") (v "0.0.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "11knqlam6aygz5nv58cwhkr809i6p74qsknf3ick57m09v1b2d6g")))

(define-public crate-whichlicense_classification-1.0.0 (c (n "whichlicense_classification") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f81f93kv5a5419asmrwyv69zv5chg2yq6ki58y72ry5kbmj81hl")))

(define-public crate-whichlicense_classification-1.1.0 (c (n "whichlicense_classification") (v "1.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hmap") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "13wiwfj21p61x32ssshwlpcgrzyv4y09fmfngwizbgvb49vlzw0r")))

(define-public crate-whichlicense_classification-1.1.1 (c (n "whichlicense_classification") (v "1.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hmap") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "19wwmrwv7vwy8rm6wq855rv245r0czcpbgp8l0j44l6j7ppvfgfp")))

(define-public crate-whichlicense_classification-1.1.2 (c (n "whichlicense_classification") (v "1.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hmap") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "063njvxv56fyn594ivzcl3rq85rsnlvxxan4bcj9sd7f0q69zm40")))

(define-public crate-whichlicense_classification-2.0.0 (c (n "whichlicense_classification") (v "2.0.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hmap") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ihdljqdc9ycvln3b47fz6dca694ghyypwjfyikbzs0n903fjrh9")))

