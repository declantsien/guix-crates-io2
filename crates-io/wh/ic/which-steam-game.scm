(define-module (crates-io wh ic which-steam-game) #:use-module (crates-io))

(define-public crate-which-steam-game-0.1.0 (c (n "which-steam-game") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "steamworks") (r "^0.10.0") (d #t) (k 0)))) (h "13cna6w55jkc6jrq89aavvxlqqkbczkmjsrrygpd8n5wj8sxiian")))

(define-public crate-which-steam-game-0.2.0 (c (n "which-steam-game") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "steamworks") (r "^0.10.0") (d #t) (k 0)))) (h "1fj6z0r7ksfnpnqx0sgc3z0hc4j1zchcpsaxbsp4da4l0rhv94sb")))

(define-public crate-which-steam-game-0.2.1 (c (n "which-steam-game") (v "0.2.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "steamworks") (r "^0.10.0") (d #t) (k 0)))) (h "1ixbq9c0wfy4m7qh5ml4nxqx7j1wxsy1j40jhbl88p6hbd3vl132")))

