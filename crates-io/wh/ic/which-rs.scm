(define-module (crates-io wh ic which-rs) #:use-module (crates-io))

(define-public crate-which-rs-0.1.0 (c (n "which-rs") (v "0.1.0") (h "1vk37zn05ik226qdrfay93lzakh8ilr01hcbk4mr2gxk9hxydfq8")))

(define-public crate-which-rs-0.1.1 (c (n "which-rs") (v "0.1.1") (h "125qwdfymnsd54w1hg1k9ix7119jxkgdyr7mv3gfj65p6ffay1i1")))

