(define-module (crates-io wh ic which_problem) #:use-module (crates-io))

(define-public crate-which_problem-0.1.0 (c (n "which_problem") (v "0.1.0") (d (list (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0jks18f39ddfvmgvxx9sd7yg2v7c4aqizcvq4zqhg1zvf2wck2m6")))

