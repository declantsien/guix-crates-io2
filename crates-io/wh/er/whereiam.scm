(define-module (crates-io wh er whereiam) #:use-module (crates-io))

(define-public crate-whereiam-0.1.0 (c (n "whereiam") (v "0.1.0") (h "0pajzsscd6dh6czxyncx074day24pblwa21bl3pn7hiy65dcnc0s")))

(define-public crate-whereiam-1.0.0 (c (n "whereiam") (v "1.0.0") (h "08qgx391v1j16ds9vql6n90rzwphhspcgv5z78xrk32i2zhm8bih")))

(define-public crate-whereiam-1.1.0 (c (n "whereiam") (v "1.1.0") (h "1hzdhllska0pz38vxk5qy42g557vgz6cl8bpv7z7f8f1ivkscgdw")))

