(define-module (crates-io wh er whereami) #:use-module (crates-io))

(define-public crate-whereami-1.0.0 (c (n "whereami") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ir2i37ivgwsq7wjy6g9v6b011c0rqdlwh0g72c00sbvvs9j78ha")))

(define-public crate-whereami-1.1.0 (c (n "whereami") (v "1.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hgc78d6wxn7yhj98jgmk8ljam9m0bljj0084g591vkzxcssal6s")))

(define-public crate-whereami-1.1.1 (c (n "whereami") (v "1.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19kyrlkd13jdd26zd8pc4n49zr3098c4va51q8wp7mrf1xwnwri9")))

