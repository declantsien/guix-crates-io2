(define-module (crates-io wh er wherr) #:use-module (crates-io))

(define-public crate-wherr-0.1.0 (c (n "wherr") (v "0.1.0") (d (list (d (n "wherr-macro") (r "^0.1") (d #t) (k 0)))) (h "0fpf20517y9nk7xn1qlir2gw4y5rz7vpqrm0gs6wj2kl7422jvdv")))

(define-public crate-wherr-0.1.1 (c (n "wherr") (v "0.1.1") (d (list (d (n "wherr-macro") (r "^0.1") (d #t) (k 0)))) (h "1j4aczpvny331zkkcxkkf5p08pa6aj5s7x00jbdy358m7vc48308")))

(define-public crate-wherr-0.1.2 (c (n "wherr") (v "0.1.2") (d (list (d (n "wherr-macro") (r "^0.1") (d #t) (k 0)))) (h "1n9rrnmkik6kf3bpvk4jnjqgbpm06317yp6y98lv67yz6f56wx80")))

(define-public crate-wherr-0.1.3 (c (n "wherr") (v "0.1.3") (d (list (d (n "wherr-macro") (r "^0.1") (d #t) (k 0)))) (h "0ijni1a9dw80iydbkaka7m5zsb55kj0s2gh35iqf53a5vmfdfp4f")))

(define-public crate-wherr-0.1.4 (c (n "wherr") (v "0.1.4") (d (list (d (n "wherr-macro") (r "^0.1") (d #t) (k 0)))) (h "0i7v3wx8yn4pisz7b1z2dfl63r82sicclcplrp2gqmy0p33baxck")))

(define-public crate-wherr-0.1.5 (c (n "wherr") (v "0.1.5") (d (list (d (n "wherr-macro") (r "^0.1") (d #t) (k 0)))) (h "04vj0barzkqp25k8rqy0bzb07836403y3ii1jxmvxzg0bvcmsfn8")))

(define-public crate-wherr-0.1.6 (c (n "wherr") (v "0.1.6") (d (list (d (n "wherr-macro") (r "^0.1") (d #t) (k 0)))) (h "1jxi082dbqwd6wc09cqxcrppr1zijn5cwbahhfmckpr506wnrhsr")))

(define-public crate-wherr-0.1.7 (c (n "wherr") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wherr-macro") (r "^0.1") (d #t) (k 0)))) (h "1sxf2qqz7lkvrxylqnc77xki9ykcjffq9kmjqymj652vdm1znr8k") (s 2) (e (quote (("anyhow" "dep:anyhow"))))))

