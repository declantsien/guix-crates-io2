(define-module (crates-io wh er wherr-macro) #:use-module (crates-io))

(define-public crate-wherr-macro-0.1.0 (c (n "wherr-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)))) (h "0rrxjac3mpfb0z5ngw8hrhrqh84955i2gi76ic87src42wzx7fcf")))

(define-public crate-wherr-macro-0.1.1 (c (n "wherr-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)))) (h "1haqwlc2yylfa0bi2nclh3nk3ww3zxklk9bbz3jrdb7clh2l7hkc")))

(define-public crate-wherr-macro-0.1.2 (c (n "wherr-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)))) (h "00mpkx40xv3sjy2dbikkm2k0zmm0sd83z625qhh7h218bqjrsvbn")))

(define-public crate-wherr-macro-0.1.3 (c (n "wherr-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)))) (h "0vchpfg6z4bgzfw277v4lm4c46azhakv2508aira1gm6wy25127v")))

(define-public crate-wherr-macro-0.1.4 (c (n "wherr-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)))) (h "0izwf2rqbgrr6al5vqlc400gf4zr2wpkxndzwmrkkdm0rz7zafdi")))

(define-public crate-wherr-macro-0.1.5 (c (n "wherr-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)))) (h "12pl8fcl7rqrc3ciqbxczxrpag91lfdf6hl1r2qmjhbi5626h792")))

(define-public crate-wherr-macro-0.1.6 (c (n "wherr-macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)))) (h "12kxamnwi742rxcbwf2zap828jwfd5x6zx5c93raws1wxdjzg577")))

(define-public crate-wherr-macro-0.1.7 (c (n "wherr-macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)))) (h "061ymkn36c0r3sdfqg1q78lx8y589fvmdhy964hgy5dqp5rw3nfw")))

