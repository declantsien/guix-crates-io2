(define-module (crates-io wh er wheres_my_pi) #:use-module (crates-io))

(define-public crate-wheres_my_pi-0.2.0 (c (n "wheres_my_pi") (v "0.2.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "pnet") (r "^0.22.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "10wygjlica5fnzqx2i7595fi53kzjrpvv8ccs3w6pnja1pzfxqgj")))

(define-public crate-wheres_my_pi-0.3.0 (c (n "wheres_my_pi") (v "0.3.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "pnet") (r "^0.29.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "1q4wip1m8x862yc48ximnzlm99x24mjifxkf61hdnfa63y2bk8qm")))

(define-public crate-wheres_my_pi-0.4.0 (c (n "wheres_my_pi") (v "0.4.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "pnet") (r "^0.29.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0pwdkz0mnvsp8h5baciqxxyllnv4vp26z3nnfpxxnxrddslmh1x8")))

