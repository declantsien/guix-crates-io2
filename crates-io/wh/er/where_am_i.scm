(define-module (crates-io wh er where_am_i) #:use-module (crates-io))

(define-public crate-where_am_i-0.1.0 (c (n "where_am_i") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.6") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)))) (h "1q9xgp3kjddsvqxgj27ij4yilr7pvwxi41fhv3mnbfmwbljc1zc1")))

(define-public crate-where_am_i-0.1.1 (c (n "where_am_i") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.6") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02a5rfz75ig1dfl7axv4dhg2gfcfilma42g3pgi9nky5zwgad8pk")))

(define-public crate-where_am_i-0.1.2 (c (n "where_am_i") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.6") (d #t) (k 0)) (d (n "scraper") (r "^0.11.0") (d #t) (k 0)))) (h "1hf9wnxczxldg6cn66r638z02gsn0v208mdj3nk6vdcykrxjddqx")))

