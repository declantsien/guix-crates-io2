(define-module (crates-io wh er where39) #:use-module (crates-io))

(define-public crate-where39-0.1.0 (c (n "where39") (v "0.1.0") (d (list (d (n "libm") (r "^0.1.4") (d #t) (k 0)))) (h "0633rdkigy345qmq7ysplz76sagry2akfdmrya74p0d05hjfn9mx")))

(define-public crate-where39-0.1.1 (c (n "where39") (v "0.1.1") (d (list (d (n "libm") (r "^0.1.4") (d #t) (k 0)))) (h "02w4vaka327487jx1i5g0j4hnzg5jvc9cbnnx4kwzvg6kgfsc1r5")))

