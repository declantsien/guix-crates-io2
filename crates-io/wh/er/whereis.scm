(define-module (crates-io wh er whereis) #:use-module (crates-io))

(define-public crate-whereis-0.1.0 (c (n "whereis") (v "0.1.0") (d (list (d (n "nothing") (r "^0.1") (d #t) (k 0)))) (h "1c18c7cd73cb2sw2yy18cffr0xvksfzhvjdjn3905j0yxig8fpid")))

(define-public crate-whereis-0.1.1 (c (n "whereis") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nothing") (r "^0.1") (d #t) (k 0)))) (h "0ssm4in2sn17chl05cnjg6s78cfnpd6fn9019swwa7qbcyjfgk4b")))

