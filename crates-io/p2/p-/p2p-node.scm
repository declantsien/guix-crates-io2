(define-module (crates-io p2 p- p2p-node) #:use-module (crates-io))

(define-public crate-p2p-node-0.0.0 (c (n "p2p-node") (v "0.0.0") (d (list (d (n "libp2p") (r "^0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 0)))) (h "1mkip24i4z20wnzga2s91xh65fxnr45k3xcp5rb2v3jf14mnw7sf")))

