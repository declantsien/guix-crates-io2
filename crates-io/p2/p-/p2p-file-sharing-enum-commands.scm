(define-module (crates-io p2 p- p2p-file-sharing-enum-commands) #:use-module (crates-io))

(define-public crate-p2p-file-sharing-enum-commands-0.1.0 (c (n "p2p-file-sharing-enum-commands") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mds4226iah4zj6ml3gjj7sy1m1y3p5a5n1f882i8vgy9bpnl58p")))

(define-public crate-p2p-file-sharing-enum-commands-0.2.0 (c (n "p2p-file-sharing-enum-commands") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12i3p1w4rzhm5bqrbbscpp33i5a2qi4s8dzzly1qhbjx930mrcqc")))

(define-public crate-p2p-file-sharing-enum-commands-0.2.1 (c (n "p2p-file-sharing-enum-commands") (v "0.2.1") (d (list (d (n "ipconfig") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "machine-ip") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h1qsv259bpq8j8f0q55k1ya686yinhxfd0f0mm3gssxbrwwbj7y")))

(define-public crate-p2p-file-sharing-enum-commands-0.2.2 (c (n "p2p-file-sharing-enum-commands") (v "0.2.2") (d (list (d (n "ipconfig") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "machine-ip") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v1s998hsr27rk8l4x6xl70r7yx66krpmzlb4a3mvbsz9g9p4db0")))

(define-public crate-p2p-file-sharing-enum-commands-0.2.3 (c (n "p2p-file-sharing-enum-commands") (v "0.2.3") (d (list (d (n "ipconfig") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "machine-ip") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bxmzfxm7mlgj255hnzrflsg3chp77ya3h57jw85fgv0lnd1vs0p")))

