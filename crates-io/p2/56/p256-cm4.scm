(define-module (crates-io p2 #{56}# p256-cm4) #:use-module (crates-io))

(define-public crate-p256-cm4-0.1.0 (c (n "p256-cm4") (v "0.1.0") (h "1y79ycda1a70sidr2dw45pjv42pahxxapn98yzp4fz69bj5nl7qg")))

(define-public crate-p256-cm4-0.1.1 (c (n "p256-cm4") (v "0.1.1") (h "00vqlhb9ps584xa2h3hpmyx2px4sf5v0fd4z7y53v151gw5dzxpm")))

(define-public crate-p256-cm4-0.2.0 (c (n "p256-cm4") (v "0.2.0") (h "0vdh4wd7mrrkyjzx7hl0vw1krw5fir2m8lhbyqv28fb3vzksi3yd")))

(define-public crate-p256-cm4-0.3.0 (c (n "p256-cm4") (v "0.3.0") (h "0z9kfl02zd6bj96ij41m6156rj28jaxm85n3gk705vfbx850n3sv")))

