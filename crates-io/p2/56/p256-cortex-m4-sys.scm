(define-module (crates-io p2 #{56}# p256-cortex-m4-sys) #:use-module (crates-io))

(define-public crate-p256-cortex-m4-sys-0.1.0-alpha.0 (c (n "p256-cortex-m4-sys") (v "0.1.0-alpha.0") (d (list (d (n "bindgen") (r "^0.57.0") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0qkiylbmaf3g3wqnj2wm6rd3hjdy0si7x3s5pmb6imk4s7h4zfnb")))

(define-public crate-p256-cortex-m4-sys-0.1.0-alpha.1 (c (n "p256-cortex-m4-sys") (v "0.1.0-alpha.1") (d (list (d (n "bindgen") (r "^0.57.0") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0cwb6l5rwhqyxz8xrw6qfs83hvlw1rg6j8ncd0yzkaqdfar8dri0")))

(define-public crate-p256-cortex-m4-sys-0.1.0-alpha.2 (c (n "p256-cortex-m4-sys") (v "0.1.0-alpha.2") (d (list (d (n "bindgen") (r "^0.57.0") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1qxn0wpqc4vi9pw3jdsjr347pnzj3l1la7684s8035gr6kxhqigr")))

(define-public crate-p256-cortex-m4-sys-0.1.0 (c (n "p256-cortex-m4-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1pkp7603dprrqxdr3xaqxbz1qicnywk5bjjwzwwv2jqcp6j3v9sl")))

