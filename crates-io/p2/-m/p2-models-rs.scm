(define-module (crates-io p2 -m p2-models-rs) #:use-module (crates-io))

(define-public crate-p2-models-rs-0.1.0 (c (n "p2-models-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde" "rustc-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sqlx") (r "^0.5.7") (f (quote ("runtime-actix-rustls" "postgres" "chrono" "offline"))) (d #t) (k 0)))) (h "0c2v5arigg1q81sha0pqc1d18zrlbfs9dwh1vacp6nl2si7j085j")))

