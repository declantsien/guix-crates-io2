(define-module (crates-io p2 ws p2wsh-utxo) #:use-module (crates-io))

(define-public crate-p2wsh-utxo-0.1.0 (c (n "p2wsh-utxo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "001fnnv2qhi0rclspmmsknfnf43vwwciwbhwammli52j4jrv9fzb")))

