(define-module (crates-io es -s es-sys) #:use-module (crates-io))

(define-public crate-es-sys-0.0.0 (c (n "es-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)))) (h "0vdbl0125bh1dg01awcv0sm0ycrisqxia119gqsjx844xk2rh63v")))

(define-public crate-es-sys-0.0.1 (c (n "es-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)))) (h "1szy3nrljxqclb7k69cynhd2wjbsq87l5f17j0p4jyp6r0v2bbsc")))

