(define-module (crates-io es -h es-htmlform) #:use-module (crates-io))

(define-public crate-es-htmlform-0.1.0 (c (n "es-htmlform") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "103543j6j3ja95dcny0h4995n2n7xyrfyyvhsb3g3rdizdx61njk")))

(define-public crate-es-htmlform-0.1.1 (c (n "es-htmlform") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07fsrwxz4v85gz4ggj2ymxq42a0vp90hi7d7dy135wil8sc9yzhf")))

