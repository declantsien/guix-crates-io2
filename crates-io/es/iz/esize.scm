(define-module (crates-io es iz esize) #:use-module (crates-io))

(define-public crate-esize-0.1.0 (c (n "esize") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sz2npq700agrwirv0gh9apg2amzck3pylh2b6ya1vlnawgah2zl") (y #t)))

(define-public crate-esize-0.1.1 (c (n "esize") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xpk3sy8yn57a5nwcxcykv9r3pi4ycy90qwja3djmbsaz8inpmfz")))

(define-public crate-esize-0.1.2 (c (n "esize") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v0qaaf929s653ymkm5iwgb1x9cqa964dy3r6f3w0471r8g7q0z9")))

