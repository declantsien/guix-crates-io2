(define-module (crates-io es si essi-ffmpeg) #:use-module (crates-io))

(define-public crate-essi-ffmpeg-0.1.0 (c (n "essi-ffmpeg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kbxlzs8hjamvfxg2f4i4xx6885adwz17w7i47v0a5ysddid80x1")))

(define-public crate-essi-ffmpeg-0.1.1 (c (n "essi-ffmpeg") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n78jmxagba3ciqw700fv6q17a0xz4bdph26p36144m3vj2531h4")))

(define-public crate-essi-ffmpeg-0.2.0 (c (n "essi-ffmpeg") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14zd47jm781i5arl080n83fr680y7pmhsq8r75g4n7silnc3plpf")))

