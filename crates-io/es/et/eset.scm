(define-module (crates-io es et eset) #:use-module (crates-io))

(define-public crate-eset-0.1.0 (c (n "eset") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.7") (d #t) (k 2)) (d (n "rkyv") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 2)))) (h "03sbdgi4majj24c6qhj1jrvb8di300hlj2gw1v95l5dzgh2sc95i") (f (quote (("default"))))))

