(define-module (crates-io es ti estimated_read_time) #:use-module (crates-io))

(define-public crate-estimated_read_time-0.1.0 (c (n "estimated_read_time") (v "0.1.0") (h "15va3d2i8czkcb6yp2birp0iblmijfhrlh12mxxfwdsw1rsp63l0") (y #t)))

(define-public crate-estimated_read_time-1.0.0 (c (n "estimated_read_time") (v "1.0.0") (h "1mz8pkgk9v0cfzfjw659zl997gilangb78ccds8gic8h2hsgv734")))

