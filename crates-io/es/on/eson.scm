(define-module (crates-io es on eson) #:use-module (crates-io))

(define-public crate-eson-0.1.0 (c (n "eson") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "collect-mac") (r "^0.1.0") (d #t) (k 2)) (d (n "defaultmap") (r "^0.3.0") (d #t) (k 0)) (d (n "float-ord") (r "^0.1.2") (d #t) (k 0)) (d (n "group-by") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "ordermap") (r "^0.2.11") (d #t) (k 0)) (d (n "peg") (r "^0.5.4") (d #t) (k 1)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "simple-logging") (r "^1.0.1") (d #t) (k 0)) (d (n "string-intern") (r "^0.1.7") (k 0)))) (h "1r67zg9xcqlqjlf22ld38ivky639agma8y0isnkv0wsfl320pzrh")))

