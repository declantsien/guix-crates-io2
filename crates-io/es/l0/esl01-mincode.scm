(define-module (crates-io es l0 esl01-mincode) #:use-module (crates-io))

(define-public crate-esl01-mincode-0.0.1 (c (n "esl01-mincode") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "vlqencoding") (r "^0.1") (d #t) (k 0) (p "esl01-vlqencoding")))) (h "0wvb5bgbmjd4q0if09q4cpcl91c61827n1da4646szn222ihal5f")))

(define-public crate-esl01-mincode-0.3.0 (c (n "esl01-mincode") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vlqencoding") (r "^0.3") (d #t) (k 0) (p "esl01-vlqencoding")) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "10j44r143i4msawckr6n34wxfdrjc6kv6w1p1fd4i27d3h7i92r3")))

