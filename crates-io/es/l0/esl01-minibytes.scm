(define-module (crates-io es l0 esl01-minibytes) #:use-module (crates-io))

(define-public crate-esl01-minibytes-0.1.0 (c (n "esl01-minibytes") (v "0.1.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "135jsy5m7f21ix96s5vd0010n7mg5rky2kib4kr8l86ac3fmv77v")))

(define-public crate-esl01-minibytes-0.2.0 (c (n "esl01-minibytes") (v "0.2.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1c8g8q4yih3h2975447wdxfkwk9i19cyyhksa0rirdj3lyvbh9lw")))

(define-public crate-esl01-minibytes-0.3.0 (c (n "esl01-minibytes") (v "0.3.0") (d (list (d (n "bytes") (r "^1.1") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1l8sf2m63g1i7g47nk7kk6na2b86ymn1rwvhs9vq60zg4namfs00") (f (quote (("frommmap" "memmap") ("frombytes" "bytes") ("default" "frombytes" "frommmap"))))))

