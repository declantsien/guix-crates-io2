(define-module (crates-io es l0 esl01-dev-logger) #:use-module (crates-io))

(define-public crate-esl01-dev-logger-0.3.0 (c (n "esl01-dev-logger") (v "0.3.0") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0mzxi7kxfgrivrmsgkgld6p9gz3mlffpw7zzxcybfl8690261r61")))

