(define-module (crates-io es l0 esl01-vlqencoding) #:use-module (crates-io))

(define-public crate-esl01-vlqencoding-0.1.0 (c (n "esl01-vlqencoding") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "1lqngq53bc020pplmn4ni6vb4lbgzy1qzdnh4i2rapij58w5r6kb")))

(define-public crate-esl01-vlqencoding-0.3.0 (c (n "esl01-vlqencoding") (v "0.3.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0j87bwxg20f35skz8mbiwkgqb9pa4m8s8qkpbrnp6y30y6karn0a")))

