(define-module (crates-io es l0 esl01-drawdag) #:use-module (crates-io))

(define-public crate-esl01-drawdag-0.1.0 (c (n "esl01-drawdag") (v "0.1.0") (h "08h483bngspkrayz1rfy155ac3c3aibdrplzbiw244h68hvzf0zm")))

(define-public crate-esl01-drawdag-0.3.0 (c (n "esl01-drawdag") (v "0.3.0") (h "19y9m2qgmq5s6hg7j0hhjfzkk4xyddh50s8jwkwdrw3hzs24lqaq")))

