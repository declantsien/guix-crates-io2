(define-module (crates-io es l0 esl01-dag-types) #:use-module (crates-io))

(define-public crate-esl01-dag-types-0.3.0 (c (n "esl01-dag-types") (v "0.3.0") (d (list (d (n "abomonation") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "abomonation_derive") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "minibytes") (r "^0.3") (k 0) (p "esl01-minibytes")) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "13v045qwjdm632hvk602kb70w9g9f261wnf589i0gaj10hz4yxnd") (f (quote (("serialize-abomonation" "abomonation" "abomonation_derive") ("for-tests" "quickcheck"))))))

