(define-module (crates-io es l0 esl01-hgtime) #:use-module (crates-io))

(define-public crate-esl01-hgtime-0.1.0 (c (n "esl01-hgtime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "humantime") (r "^1.3") (d #t) (k 0)))) (h "1qxrfwnhb0fsv71yi4bqnglwrqidivwi1bkmddpzhzbzs3q6y490")))

(define-public crate-esl01-hgtime-0.3.0 (c (n "esl01-hgtime") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)))) (h "15axqx0dj3hlp4490f8pw9s13zxzyrzbqqyy08iggjcrax0z95ps")))

