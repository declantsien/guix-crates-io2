(define-module (crates-io es _b es_bulk) #:use-module (crates-io))

(define-public crate-es_bulk-0.1.0 (c (n "es_bulk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nw23fn7k37hlvsp2jdmz7l91d2clcrar64m5w0mpvvkrz3cs9x0")))

(define-public crate-es_bulk-0.1.1 (c (n "es_bulk") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0pvr5rmn8pahdv7x18wygdgifr0lfqgpzl69mgcpfaykhdn6kwx2")))

