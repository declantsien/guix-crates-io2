(define-module (crates-io es _b es_balk) #:use-module (crates-io))

(define-public crate-es_balk-0.1.0 (c (n "es_balk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0imdd2i9x8c0fa16ay99yxznd79zfgm2cxlaviip6f9h2np5lr5k")))

