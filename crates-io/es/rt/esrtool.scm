(define-module (crates-io es rt esrtool) #:use-module (crates-io))

(define-public crate-esrtool-0.1.0 (c (n "esrtool") (v "0.1.0") (h "0464p3s0y00b95n88m5dqzygsp1iiwhkm80cj54fg2i1nsd19g3y")))

(define-public crate-esrtool-0.1.1 (c (n "esrtool") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0nvqf136j1q303wpxfpdadhnnh4fsggg3xmvzpqbz21mwmb7c8zc")))

(define-public crate-esrtool-0.1.2 (c (n "esrtool") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0zws5fayy31sbdcb0cani66gfx6l782f83cl2mvjcavhd43ci8i3")))

(define-public crate-esrtool-0.2.0 (c (n "esrtool") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1jsd0y7ml9y2m71d232h4nwg9manz1cs37krhrydc2mc16zmrsxk")))

(define-public crate-esrtool-0.2.1 (c (n "esrtool") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1vd076k1lixm57pl3hhhgm47hdhnc4khpq21gqxayjsdmdaydm90")))

(define-public crate-esrtool-0.2.2 (c (n "esrtool") (v "0.2.2") (h "0d97nnqwi9d6518lz21iv9qrsr33g9634hldbj65935jp7l20xnx")))

(define-public crate-esrtool-0.3.0 (c (n "esrtool") (v "0.3.0") (h "1gr34lpzqqfbgqy22b1ls09d7lmp02r0dkx7xlhhpn503821ccwh")))

(define-public crate-esrtool-0.3.1 (c (n "esrtool") (v "0.3.1") (h "0v14zd1w6rb1f704fg8lw5lx7fddn1z9xd30wjsckn10s6qnkhbf")))

(define-public crate-esrtool-0.3.2 (c (n "esrtool") (v "0.3.2") (h "05815c6qy6x5zlfsai7yjpy88d31gizwwpllfa7adrhqdl5k3060")))

(define-public crate-esrtool-0.3.3 (c (n "esrtool") (v "0.3.3") (h "0nvkn5rkwf9zmh4xzqlqv689q8r2l4b6sq9877ng2nr5z8bwh5gc")))

