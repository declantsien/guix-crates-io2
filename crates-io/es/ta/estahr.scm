(define-module (crates-io es ta estahr) #:use-module (crates-io))

(define-public crate-estahr-0.1.0 (c (n "estahr") (v "0.1.0") (d (list (d (n "blake2") (r "^0.8.1") (d #t) (k 0)) (d (n "blake3") (r "^0.1.3") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "digest") (r "^0.8.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "md-5") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0qbypvf937b7pcs4mzp5zwd3c0jmimak01d1hn45a3cbl3yppf5a")))

