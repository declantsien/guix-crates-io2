(define-module (crates-io es ta estash) #:use-module (crates-io))

(define-public crate-estash-0.6.2 (c (n "estash") (v "0.6.2") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 0)) (d (n "crypto_box") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fltk") (r "^1.3.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_hc") (r "^0.3.1") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (d #t) (k 0)) (d (n "zeroize") (r "^1.3.0") (d #t) (k 0)))) (h "1n4yny87sz1qhh3rnfq8khi4p5fzbf4086mx4pvf8jli88zrwiys") (r "1.64")))

