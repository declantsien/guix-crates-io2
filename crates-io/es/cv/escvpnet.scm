(define-module (crates-io es cv escvpnet) #:use-module (crates-io))

(define-public crate-escvpnet-0.1.0 (c (n "escvpnet") (v "0.1.0") (h "04acqadjx0scxm8v6vi95dk6sr7m0w5f3g873z8xwyw77hmg15cy") (r "1.56.0")))

(define-public crate-escvpnet-0.1.1 (c (n "escvpnet") (v "0.1.1") (h "12359sq759j5fl70s1khjzyy5fn63m4cal6ig8pms08k691g6j9s") (r "1.56.0")))

(define-public crate-escvpnet-0.1.2 (c (n "escvpnet") (v "0.1.2") (h "0bnh1a93wm38vx15bwn04cyzy2a22ffa01fbdi08df087xxv44jl") (r "1.56.0")))

(define-public crate-escvpnet-0.1.3 (c (n "escvpnet") (v "0.1.3") (h "1vsfvs8g0mfmwkg349kr32aa7wxv9j6p9kgxy5zm4cjwqxd3mvxh") (r "1.56.0")))

(define-public crate-escvpnet-0.1.4 (c (n "escvpnet") (v "0.1.4") (h "0g7pi5a0ack9rd93x3m1zdh14l3s487s4wh2cql533jxcw6jsn9n") (r "1.56.0")))

(define-public crate-escvpnet-0.2.1 (c (n "escvpnet") (v "0.2.1") (h "0gfygm73gl5aj5g6k24mfpnf7gvbypa20pmkq7p7yy4mpd740z49") (r "1.56.0")))

(define-public crate-escvpnet-0.2.2 (c (n "escvpnet") (v "0.2.2") (h "0zl2ymyrj49h7iv0w4bfkb66psm2zc6av558ghjy2wyf6wnn0jhd") (r "1.56.0")))

(define-public crate-escvpnet-0.2.3 (c (n "escvpnet") (v "0.2.3") (h "110j5w7mdmmmf5n7k5jzmrn3wxq8hhl4b1ad3wyx8nxd1issd1im") (r "1.56.0")))

(define-public crate-escvpnet-0.3.0 (c (n "escvpnet") (v "0.3.0") (h "0nrw62ivclbjjfb9g1531101m8qvr3gwphnaip5h4405g3apr7am") (r "1.68.2")))

(define-public crate-escvpnet-0.3.3 (c (n "escvpnet") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("net" "io-util" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("net" "io-util" "test-util" "macros"))) (d #t) (k 2)))) (h "158r7hnbj1ikb5kh1y7irmk7krz6r77zh908k6w1b76gz4li3a0r") (r "1.68.2")))

