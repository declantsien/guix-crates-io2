(define-module (crates-io es pe espers) #:use-module (crates-io))

(define-public crate-espers-0.1.0 (c (n "espers") (v "0.1.0") (d (list (d (n "binrw") (r "^0.11") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qb6l2sj2d78zqnq3w05bbqqs320zy3va2dknz804rkmwr3hhjyl")))

