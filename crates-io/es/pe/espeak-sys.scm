(define-module (crates-io es pe espeak-sys) #:use-module (crates-io))

(define-public crate-espeak-sys-0.0.1 (c (n "espeak-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0klimqzsvnvy367sm2riq9y0d795p4fah7zj3d15dxlnhz2x5a9a") (y #t)))

(define-public crate-espeak-sys-0.0.2 (c (n "espeak-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0hm8cz9vqqf5gisvx7762qhb22gh54vhlpywwn5kq7fkvfq306gw")))

