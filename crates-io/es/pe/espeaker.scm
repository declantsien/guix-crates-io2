(define-module (crates-io es pe espeaker) #:use-module (crates-io))

(define-public crate-espeaker-0.1.0 (c (n "espeaker") (v "0.1.0") (d (list (d (n "espeakng-sys") (r "=0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "0w2hckxvqrc9r2bpkqkpdam47q2spv79yvvrsab7jhijrs93pmxy")))

(define-public crate-espeaker-0.1.1 (c (n "espeaker") (v "0.1.1") (d (list (d (n "espeakng-sys") (r "=0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "0zi0rr541icdfm277dwid44mh9nnb3rlnzv0gkimqp0ap513l4dj")))

(define-public crate-espeaker-0.1.2 (c (n "espeaker") (v "0.1.2") (d (list (d (n "espeakng-sys") (r "=0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17.0") (d #t) (k 0)))) (h "02iw11p7jqn3cljh02fxq35sy8s5wbyqlzqdabzyc2261c5g4905")))

(define-public crate-espeaker-0.1.3 (c (n "espeaker") (v "0.1.3") (d (list (d (n "espeakng-sys") (r "=0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17.0") (d #t) (k 0)))) (h "1xvfd2k8xakc3g1209x60b7kxlqzg2lyvclnh5g95bq9x92ldzy3")))

(define-public crate-espeaker-0.1.4 (c (n "espeaker") (v "0.1.4") (d (list (d (n "espeakng-sys") (r "=0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17.0") (d #t) (k 0)))) (h "13q2rld0q75x1m95xc6pz04wq0wvcv428micw6vz9jpbbc8v62q8")))

(define-public crate-espeaker-0.1.5 (c (n "espeaker") (v "0.1.5") (d (list (d (n "espeakng-sys") (r "=0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "readonly") (r "^0.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17.0") (d #t) (k 0)))) (h "0q09gc5kps74103k216iafyjdd2rj8n94n26v70yb0n5vy0aj390")))

