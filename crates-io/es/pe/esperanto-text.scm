(define-module (crates-io es pe esperanto-text) #:use-module (crates-io))

(define-public crate-esperanto-text-0.1.0 (c (n "esperanto-text") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)))) (h "1kwyi1wy0lwm53vka9pc67h39mdj7lgn2i52lg77bpbsmkrafscg")))

(define-public crate-esperanto-text-1.0.0 (c (n "esperanto-text") (v "1.0.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)))) (h "12g1p701mc27p1yri9liq4w1kaldddg3j8r1j5vrfm7fa5gg4lqw")))

