(define-module (crates-io es pe espeakng-sys) #:use-module (crates-io))

(define-public crate-espeakng-sys-0.1.0 (c (n "espeakng-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0ngxcyy617w5ixcphp3lvn9l3piv5iy9rxhg20ysqq7gqdzvscz4")))

(define-public crate-espeakng-sys-0.1.1 (c (n "espeakng-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (k 1)))) (h "1kym0k1n04lgd6hk5jawk5xpzg4cz9apzvmyz03kx27zyrg75ga8")))

(define-public crate-espeakng-sys-0.1.2 (c (n "espeakng-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.1") (k 1)))) (h "19cbws5apczi1r4l2b0r1gik87xgq1lp8dffgnvnpb27g9a789s1")))

(define-public crate-espeakng-sys-0.1.3 (c (n "espeakng-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.62.0") (k 1)))) (h "1lss4r6v93xgzkkin6jfvh03khhzbm56bml4jrlf031bzj7dl3yf") (f (quote (("default") ("clang-runtime" "bindgen/runtime")))) (y #t)))

(define-public crate-espeakng-sys-0.2.0 (c (n "espeakng-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.62.0") (k 1)))) (h "0asjbj6ccmshi9dkrc0vhi103akv18c37ybwn6wbhw8hrabvmlyg") (f (quote (("default") ("clang-runtime" "bindgen/runtime"))))))

