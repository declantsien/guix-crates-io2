(define-module (crates-io es du esdump) #:use-module (crates-io))

(define-public crate-esdump-0.1.0 (c (n "esdump") (v "0.1.0") (h "1f4mm22l1gyhpvlscc01d5dv6wjj50ahnarf425jk844k4g1h6jv")))

(define-public crate-esdump-0.1.1 (c (n "esdump") (v "0.1.1") (h "1amyisjql79hs6549p3mbd8bp09l6s21jbcjakdycq6hnn185h1a")))

