(define-module (crates-io es ca escape-bytes) #:use-module (crates-io))

(define-public crate-escape-bytes-0.0.1 (c (n "escape-bytes") (v "0.0.1") (h "0pbl4lp0pwyqyf2j2cqmxxmgbryxjs3cw32rbyz6c9pmnax15pc2") (y #t)))

(define-public crate-escape-bytes-0.1.0 (c (n "escape-bytes") (v "0.1.0") (h "1921aazdn10sgdn2b3baab2kjs5qjvhy5bz7bzggmdn5zrhx2w9q") (f (quote (("docs") ("default" "alloc") ("alloc"))))))

(define-public crate-escape-bytes-0.1.1 (c (n "escape-bytes") (v "0.1.1") (h "1ch3xdzr4lkzvkx5scqpyhpy2ywmj2p3z1wh7gyc4598x9zzdz1b") (f (quote (("docs") ("default" "alloc") ("alloc"))))))

