(define-module (crates-io es ca escape) #:use-module (crates-io))

(define-public crate-escape-0.1.0 (c (n "escape") (v "0.1.0") (h "19mn3ynvamq6wy3lcb1k8c6y26rkgp50520s9zwli9znzj8ccw6q")))

(define-public crate-escape-0.1.1 (c (n "escape") (v "0.1.1") (h "0klq91himc4il75a9n9v45x0n32vghj4phv6hs7car9gsmq62q8w")))

(define-public crate-escape-0.1.2 (c (n "escape") (v "0.1.2") (h "1k6bf0q9pir6j8jnpf77p0rp675ag3rx5313r6wa750gpg86rjzx")))

(define-public crate-escape-0.1.3 (c (n "escape") (v "0.1.3") (h "00bwvaz8hd81073gsyalmaw817r8wpa0a0ps4j2yk5s6aa35h6jy")))

(define-public crate-escape-0.1.31 (c (n "escape") (v "0.1.31") (h "1mcndw4pqgcvh85kjx19hakbanw4i5hr38x0rnr6rjfakgw8c5yd")))

