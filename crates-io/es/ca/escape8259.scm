(define-module (crates-io es ca escape8259) #:use-module (crates-io))

(define-public crate-escape8259-0.5.0 (c (n "escape8259") (v "0.5.0") (h "06lymws0z3l1qcgbacs71zb9rmfrla8pvg0jq3z70ki7pqmbclr0")))

(define-public crate-escape8259-0.5.1 (c (n "escape8259") (v "0.5.1") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "1f2qpl2icg9mkqz6n3nbmj8rl47yqgh3dhv3wj875sf61306bpcf")))

(define-public crate-escape8259-0.5.2 (c (n "escape8259") (v "0.5.2") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "1vjpfnk9fyq6qcc18bq7yfk9ahi2r12lfywr4rwcsvv6wc8ljkxs")))

