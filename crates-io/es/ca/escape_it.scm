(define-module (crates-io es ca escape_it) #:use-module (crates-io))

(define-public crate-escape_it-0.1.0 (c (n "escape_it") (v "0.1.0") (d (list (d (n "act_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "escape_it_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "gtk_estate") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("rt" "rt-multi-thread" "sync" "macros"))) (d #t) (k 0)))) (h "0y83c98pczxrs19lvdq4pg1q9lfpshy0j0f7cqz8xjsikg699m65")))

