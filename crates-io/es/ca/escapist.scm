(define-module (crates-io es ca escapist) #:use-module (crates-io))

(define-public crate-escapist-0.0.1 (c (n "escapist") (v "0.0.1") (d (list (d (n "entities") (r "^1.0.1") (d #t) (k 0)))) (h "0czyw6rm5hg6lwg801ckcgpc6f66vh2w325iq0mdldzq6kpf34m6")))

(define-public crate-escapist-0.0.2 (c (n "escapist") (v "0.0.2") (d (list (d (n "entities") (r "^1.0.1") (d #t) (k 0)))) (h "18j4b3cxsp50jxj6ljwbsciwsg8jx8kpxp74idnsbbz5sxcpw759")))

