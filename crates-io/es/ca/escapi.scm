(define-module (crates-io es ca escapi) #:use-module (crates-io))

(define-public crate-escapi-3.0.0 (c (n "escapi") (v "3.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "01d2qkfp3vysxbghhl1pp20kqfp3q6av69ckza61md70nq2lsl1a")))

(define-public crate-escapi-3.0.1 (c (n "escapi") (v "3.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "071jyizi6r1mqsyxk6rr7k9r2ivrf6w8z78gv6gk2x24gx8v5cqp")))

(define-public crate-escapi-3.0.2 (c (n "escapi") (v "3.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "02y6f6lmazs6fjz4zxnn22n4vdmp5gjx5pn3h5fqp2dh0m9jra0r")))

(define-public crate-escapi-3.0.3 (c (n "escapi") (v "3.0.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "1bzwl9dpp38bm0yfdwz98nl7wz205azix18pq0kiqlgmnqixghvw")))

(define-public crate-escapi-3.0.4 (c (n "escapi") (v "3.0.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "0amj1wz5qxbsj68c0wxv7242gb5kcnrv6wnp3bp5vx4mljnixs3l")))

(define-public crate-escapi-3.0.5 (c (n "escapi") (v "3.0.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 0)))) (h "16i0lf8dkib3z72crfz3fjpp4h6as33wpjf53a01r7cdgxl4bi04")))

(define-public crate-escapi-3.1.1 (c (n "escapi") (v "3.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.21.1") (d #t) (k 2)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "01b601vpilnjsjxcnqhqgi4dlml979v8pmkbl44ycq5d865i8ypi") (y #t)))

(define-public crate-escapi-4.0.0 (c (n "escapi") (v "4.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.21.1") (d #t) (k 2)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)))) (h "01kl4kg1q7iw5zrwv5jaqig4hifdk20ciibzkkmy0aw4lvmyv0bb")))

