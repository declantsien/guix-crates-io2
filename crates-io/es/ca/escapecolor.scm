(define-module (crates-io es ca escapecolor) #:use-module (crates-io))

(define-public crate-escapecolor-0.9.0 (c (n "escapecolor") (v "0.9.0") (h "1q2sfhqyc550wkdbn64241n3pcqfp6gdcv93x9w4p283ncg630ak") (y #t)))

(define-public crate-escapecolor-0.9.1 (c (n "escapecolor") (v "0.9.1") (h "1qn4l5667dj3673v51ha76p4paxh38bdz3lv3v09x3rb2a385h3m") (y #t)))

(define-public crate-escapecolor-0.9.2 (c (n "escapecolor") (v "0.9.2") (h "0pgfjlj4xsp81fw6mxjxwzwqfryy9kl6asva8gn7y1yfay21m3lv") (y #t)))

(define-public crate-escapecolor-0.9.3 (c (n "escapecolor") (v "0.9.3") (h "1rr7a36jrc2jg8hhnaa2hc8qbjgw3lh9h5qkc6qkhwhp1ibxmq7v") (y #t)))

(define-public crate-escapecolor-0.9.4 (c (n "escapecolor") (v "0.9.4") (h "17v0r954ljpvrhmiqgww2synf3dk0dhxirzq5kfh8i72j5pbc4ci") (y #t)))

