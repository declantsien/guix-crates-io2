(define-module (crates-io es ca escan) #:use-module (crates-io))

(define-public crate-escan-0.1.0 (c (n "escan") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1d60n7ypwzkc87qcfr0izjgvw6fzq3ni3j99s3591w20i4jhd0vm")))

(define-public crate-escan-0.1.1 (c (n "escan") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0lzf8dfnlcwcgl9pqfcldrp1b1qpnvqbhrcjnla0y9vj2k71kgsk")))

(define-public crate-escan-0.2.0 (c (n "escan") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "17bc9fskpqwz46fmfv1qx2ljcmd1kj510qbh7kdgr269njr2z97j")))

