(define-module (crates-io es ca escape_string) #:use-module (crates-io))

(define-public crate-escape_string-0.1.0 (c (n "escape_string") (v "0.1.0") (h "0i3j0yh4b6vvrw458ssgnk5j5a2xvrzzmkm95if2pk0a5k3ywbfy")))

(define-public crate-escape_string-0.1.1 (c (n "escape_string") (v "0.1.1") (h "1rhva3wmvnab6fnmg1myw84dx73i5pszqhxdij0vj0flisx5f3cm")))

(define-public crate-escape_string-0.1.2 (c (n "escape_string") (v "0.1.2") (h "1fji152hgknfixj4ny5xnrmaly09dsyk0fl3fggqzj3mk5b6gs2r")))

