(define-module (crates-io es ca escape-ascii) #:use-module (crates-io))

(define-public crate-escape-ascii-0.1.0 (c (n "escape-ascii") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^3.0") (d #t) (k 2)))) (h "0hxdzr081x7gr4i1jkhb60jj45v3dda7a1sb8mcdbiaxf5gi1nb1") (r "1.60")))

