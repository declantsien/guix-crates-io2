(define-module (crates-io es ca escaper) #:use-module (crates-io))

(define-public crate-escaper-0.1.0 (c (n "escaper") (v "0.1.0") (d (list (d (n "entities") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1.26") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1c4kysgqyfcwxl03sn376hawqkhhwa3c39yzn8r728n251039nir")))

(define-public crate-escaper-0.1.1 (c (n "escaper") (v "0.1.1") (d (list (d (n "entities") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0rhn3ipb5c3zns4wakclilfqzbmax6pwx7l367dipfj9fdxvjgm5")))

