(define-module (crates-io es ca escapade) #:use-module (crates-io))

(define-public crate-escapade-0.0.1 (c (n "escapade") (v "0.0.1") (h "0dbmwd37yqfj76nmgyf5chqglnhwpb8mj8ain6pv0cb44gak2cy2")))

(define-public crate-escapade-0.0.2 (c (n "escapade") (v "0.0.2") (h "07bz5parlhn99h22dphlbfbj52xplka0pxy5d92zwpvpr4h0c54p")))

(define-public crate-escapade-0.0.3 (c (n "escapade") (v "0.0.3") (d (list (d (n "skeptic") (r "^0.6.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 2)))) (h "0ibcyzlfydr8svl5nj9shbg9wmraqa6rclkd0bhdq3ypdv2arf9m")))

