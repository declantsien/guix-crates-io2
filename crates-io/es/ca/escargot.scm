(define-module (crates-io es ca escargot) #:use-module (crates-io))

(define-public crate-escargot-0.1.0 (c (n "escargot") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nd34bb3npgx56909wym7n3n840a2nwjr2gpb9vf9z0rv55w07j3")))

(define-public crate-escargot-0.2.0 (c (n "escargot") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gd4hjb7lyl8bicz42v40qs6122ddnnvr5d2k8qbmbhj1z6nmsyx")))

(define-public crate-escargot-0.3.0 (c (n "escargot") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10zy89x6yyh862zhkb875njwra1yaiid8vddx01rjlgdxak5knrq")))

(define-public crate-escargot-0.3.1 (c (n "escargot") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19fmn7bz1h6nlqy0mp825xwjwnrjn4xjdpwc06jl51j3fiz1znqr")))

(define-public crate-escargot-0.4.0 (c (n "escargot") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15s5x2z3i5ygwy7wkj955cjqh6dp4afp7ig4nlld1mblk2zsvfff") (f (quote (("strict_unstable") ("print"))))))

(define-public crate-escargot-0.5.0 (c (n "escargot") (v "0.5.0") (d (list (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vd9phbpd6yrnsksn2as8flvq8ykzvck2zlz143xpp42qaz9dkvl") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable"))))))

(define-public crate-escargot-0.5.1 (c (n "escargot") (v "0.5.1") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yky4k8ssv53hr87b5myy47920jf93qvzvcbwxwlcdgd1d2w40db") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable"))))))

(define-public crate-escargot-0.5.2 (c (n "escargot") (v "0.5.2") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qjjcrbmn108y1gdjqakssc7jx9b9h1snzww117x7fg1bxwkzfs4") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable"))))))

(define-public crate-escargot-0.5.3 (c (n "escargot") (v "0.5.3") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0midwwnml38rj581k7184fivg8rfgq9qld9bssnhk5538ry0rp8m") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable"))))))

(define-public crate-escargot-0.5.4 (c (n "escargot") (v "0.5.4") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02snydll7ny9hjcx447m05qj7rjwpjqyyxy3j9d054r6mcbf5zsz") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable"))))))

(define-public crate-escargot-0.5.5 (c (n "escargot") (v "0.5.5") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vrz4zh38lh00864zgrzb7iby2fw8xzc1v63yqd2gnjfaqspr1z0") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable"))))))

(define-public crate-escargot-0.5.6 (c (n "escargot") (v "0.5.6") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cwh4qc9gdrbslxnmr48gwj1yf3j0fdx3z8zgiifp6r5f257vbcy") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable"))))))

(define-public crate-escargot-0.5.7 (c (n "escargot") (v "0.5.7") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19h1yvg9x7lvyb7p75ypyb94sacns5dkxwc4fa56mcksgnhlnn7m") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable"))))))

(define-public crate-escargot-0.5.8 (c (n "escargot") (v "0.5.8") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gv1fj1ai6lfraih2shvqjn93smyj377mj0xm75ysaqf7ayn903n") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable")))) (r "1.66.0")))

(define-public crate-escargot-0.5.9 (c (n "escargot") (v "0.5.9") (d (list (d (n "assert_fs") (r "^1.1") (d #t) (k 2)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y29wad9z05wyr3xjmlxjgl0pw6rqf7fna2m8292gygzrxqbcjkh") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable")))) (r "1.73")))

(define-public crate-escargot-0.5.10 (c (n "escargot") (v "0.5.10") (d (list (d (n "assert_fs") (r "^1.1") (d #t) (k 2)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.113") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fcrqxgq20gvybnqnqfaf0bljkyvhdspa98g7mw4xl6b8il4qisg") (f (quote (("test_unstable") ("strict_unstable") ("print") ("cargo_unstable")))) (r "1.73")))

