(define-module (crates-io es sp essp) #:use-module (crates-io))

(define-public crate-essp-0.1.0 (c (n "essp") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "=0.2.36") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "1b4qh4srmflwa2c8ijh6zpayr9rfmssqanc45hr1y7dw7qh88xav")))

