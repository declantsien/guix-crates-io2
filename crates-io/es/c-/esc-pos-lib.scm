(define-module (crates-io es c- esc-pos-lib) #:use-module (crates-io))

(define-public crate-esc-pos-lib-0.1.0 (c (n "esc-pos-lib") (v "0.1.0") (h "0rrbrf171y1n3kc45vwz9g1rkzf7jv0lrkrqi8c00s64178ll1iz") (y #t)))

(define-public crate-esc-pos-lib-0.1.1 (c (n "esc-pos-lib") (v "0.1.1") (h "0zkad3hz0k26s1j8w5h3vi7v64pma31fcl2vm8y2iilg3lbb0d72") (y #t)))

(define-public crate-esc-pos-lib-0.1.2 (c (n "esc-pos-lib") (v "0.1.2") (h "1g83xjipjf1ps4227z2pd4m6bpsl078mnkv54730h9nxq94mam8m") (y #t)))

(define-public crate-esc-pos-lib-0.1.3 (c (n "esc-pos-lib") (v "0.1.3") (h "1n01nzv8b3bksf4y8wbiahnmsvzfg3p6sg57vka9wmy5i219jdg2") (y #t)))

(define-public crate-esc-pos-lib-0.1.4 (c (n "esc-pos-lib") (v "0.1.4") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "023q5kzmwsapjzgndpp8v8anqbwcvs4v28sxa7rskybq3iii3b1m")))

(define-public crate-esc-pos-lib-0.1.5 (c (n "esc-pos-lib") (v "0.1.5") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "03b7ykxf6lxfwlf01dwrcx3k5zs5gfkzljfd9l09midsdbwbczhk")))

(define-public crate-esc-pos-lib-0.1.6 (c (n "esc-pos-lib") (v "0.1.6") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "1dc2zl51yxi44xlhsv8sm3f53cr7v0cz2920iba6mhsw9bg9gg2l")))

