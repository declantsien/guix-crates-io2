(define-module (crates-io es b_ esb_fireplace) #:use-module (crates-io))

(define-public crate-esb_fireplace-0.1.0 (c (n "esb_fireplace") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0k2ni1739iqzklnzvscfnr0327v5crxf5hxjxa48bhswhsjr7gf4")))

(define-public crate-esb_fireplace-0.2.0 (c (n "esb_fireplace") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "109kcv3r5b69dj1p6g26c7ijc28793m6yqg3p6fb486z1sxl0r15")))

