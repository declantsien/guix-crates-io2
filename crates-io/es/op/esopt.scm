(define-module (crates-io es op esopt) #:use-module (crates-io))

(define-public crate-esopt-0.2.0 (c (n "esopt") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05zk95f16kjsfkdg87flxpafz443bjym6h1kh9xkzida8kj2migk") (f (quote (("floats-f64"))))))

