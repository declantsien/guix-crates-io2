(define-module (crates-io es tr estring) #:use-module (crates-io))

(define-public crate-estring-0.1.0 (c (n "estring") (v "0.1.0") (h "1zl3pjwavhss38ksv1c9chdrsrc29xlhv67hs93qdjs7c8xciyh2") (f (quote (("vec") ("number") ("bool"))))))

(define-public crate-estring-0.1.1 (c (n "estring") (v "0.1.1") (h "0cb8qhszsy90h0c6dhkax89s1bhgmp6hgby1qpwm27w86kch6w2v") (f (quote (("vec") ("prim" "number" "bool") ("number") ("bool"))))))

(define-public crate-estring-0.1.2 (c (n "estring") (v "0.1.2") (h "1naq0v1gc10vclkw19pcxcxw692wd8mk7y82xq79bjzp9s87xskj") (f (quote (("vec") ("prim" "number" "bool") ("number") ("bool"))))))

(define-public crate-estring-0.2.0 (c (n "estring") (v "0.2.0") (h "1k6jc3hyrminw4g10vz9bfgzz5s4dq609k6q49ca5pvnk1mc4psi") (f (quote (("structs") ("low-level") ("aggs")))) (r "1.59.0")))

(define-public crate-estring-0.2.1 (c (n "estring") (v "0.2.1") (h "0xvfhc6djpjbjp98y1bqna9jc7pp7hsxr5lb4hg2j3jhlbc6j02x") (f (quote (("structs") ("low-level") ("aggs")))) (r "1.59.0")))

(define-public crate-estring-0.3.0 (c (n "estring") (v "0.3.0") (h "0x0yy86g4rpnm92czlalyy0qjp2hrhm10dwdzgkr8a5fdm3ppk57") (f (quote (("structs") ("low-level") ("aggs")))) (r "1.59.0")))

