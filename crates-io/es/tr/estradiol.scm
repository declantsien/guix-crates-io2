(define-module (crates-io es tr estradiol) #:use-module (crates-io))

(define-public crate-estradiol-0.0.1 (c (n "estradiol") (v "0.0.1") (h "1y9pkgh573zggfqqq59cjyxmz3bmsn3kb1bl0nbl46ckgg3jyxvf")))

(define-public crate-estradiol-0.0.2 (c (n "estradiol") (v "0.0.2") (h "0bbxp48426fhhf7vxh0nwmfmwm05d2ajbf1j25q65bnvc5r7mryx")))

