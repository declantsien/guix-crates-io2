(define-module (crates-io es tr estratto) #:use-module (crates-io))

(define-public crate-estratto-0.0.1 (c (n "estratto") (v "0.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rustfft") (r "^6.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.7") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.7") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cb3kial0bkqdndxw36f0x7h0xgra49bc5znmm69fl4hf6jraphk")))

