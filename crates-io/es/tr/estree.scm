(define-module (crates-io es tr estree) #:use-module (crates-io))

(define-public crate-estree-0.0.1 (c (n "estree") (v "0.0.1") (d (list (d (n "easter") (r "^0.0.1") (d #t) (k 0)) (d (n "joker") (r "^0.0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "serde") (r "^0.5.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.5.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.1") (d #t) (k 0)))) (h "19zilzmv3c7iajb8rx03gzqydzahx01q47842hrgkpw2xpx377qr")))

(define-public crate-estree-0.0.2 (c (n "estree") (v "0.0.2") (d (list (d (n "easter") (r "^0.0.3") (d #t) (k 0)) (d (n "joker") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.5.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.3") (d #t) (k 0)))) (h "10y369jlaw3xmqi4wm3i6j6jpscpm2frkfgfq85rfmsrrhapd0sq")))

(define-public crate-estree-0.0.3 (c (n "estree") (v "0.0.3") (d (list (d (n "easter") (r "^0.0.3") (d #t) (k 0)) (d (n "joker") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.5.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.3") (d #t) (k 0)))) (h "0r8x5dpbvh5wyk8cm5arh8fgjnp2d7maf7r893n5l16i9zz218x9")))

(define-public crate-estree-0.0.4 (c (n "estree") (v "0.0.4") (d (list (d (n "easter") (r "^0.0.4") (d #t) (k 0)) (d (n "joker") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.4") (d #t) (k 0)))) (h "14k4drl0qspmsajlg30rs2p7ya4shj5znpa1dm25nk4wgzhbgyfn")))

(define-public crate-estree-0.0.5 (c (n "estree") (v "0.0.5") (d (list (d (n "easter") (r "^0.0.5") (d #t) (k 0)) (d (n "joker") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.5") (d #t) (k 0)))) (h "040bpb1wi1gqqdfiw2jrh06a9kx8csdfj5d04dfrqkkd3fl8cvmw")))

