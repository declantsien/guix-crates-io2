(define-module (crates-io es vg esvg) #:use-module (crates-io))

(define-public crate-esvg-0.1.0 (c (n "esvg") (v "0.1.0") (d (list (d (n "polygonical") (r "^0.3.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27") (d #t) (k 0)))) (h "1s7nhlqczg41ir8ndkq2syadyz11jdp18h69w6jmjdl0fmx2dd3l")))

(define-public crate-esvg-0.2.0 (c (n "esvg") (v "0.2.0") (d (list (d (n "polygonical") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27") (d #t) (k 0)))) (h "0wh18rkyqj28a6qy9zmm0q4rz3jhdrv2x71j61hcylafzajs4wlq")))

(define-public crate-esvg-0.3.0 (c (n "esvg") (v "0.3.0") (d (list (d (n "polygonical") (r "^0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27") (d #t) (k 0)))) (h "0y8pd63b07qspmn9g2i911h24l89yfxjc3c5vi6r9194zhrwp51c")))

(define-public crate-esvg-0.4.0 (c (n "esvg") (v "0.4.0") (d (list (d (n "polygonical") (r "^0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.29") (d #t) (k 0)))) (h "0m7ixmmii2w50scvlr1zrkjnc5ql3kl60cwgy8ql0ylnadh8f8cw")))

(define-public crate-esvg-0.5.0 (c (n "esvg") (v "0.5.0") (d (list (d (n "polygonical") (r "^0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "179m478yriyis5abbbjv1kws5gkri6dxppkfdd7iymsm11rixxad")))

