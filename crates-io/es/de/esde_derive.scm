(define-module (crates-io es de esde_derive) #:use-module (crates-io))

(define-public crate-esde_derive-1.0.0 (c (n "esde_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0mdjd6chr7mvxqflqzjhmvfiqxd04whhf2b2g9qvxxl1k1811f1i")))

