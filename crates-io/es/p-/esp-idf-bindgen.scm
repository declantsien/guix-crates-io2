(define-module (crates-io es p- esp-idf-bindgen) #:use-module (crates-io))

(define-public crate-esp-idf-bindgen-0.1.0 (c (n "esp-idf-bindgen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "globwalk") (r "^0.8") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "00b0a1z19gy4ypvmixavjvfa3cq9ws30jfmjayhpsq7cgvywmnra")))

(define-public crate-esp-idf-bindgen-0.1.1 (c (n "esp-idf-bindgen") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "globwalk") (r "^0.8") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0fq975v1f4k51jk8lkdz7h7pa0ncj9bxnd8fzbiyja1mn1am1pwj")))

