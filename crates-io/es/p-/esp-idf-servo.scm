(define-module (crates-io es p- esp-idf-servo) #:use-module (crates-io))

(define-public crate-esp-idf-servo-0.1.0 (c (n "esp-idf-servo") (v "0.1.0") (d (list (d (n "esp-idf-sys") (r "^0.33") (f (quote ("native"))) (k 0)))) (h "14kkml5cknylk72md3npjlm0vby547vmxiz67g222qhscy21p12b")))

(define-public crate-esp-idf-servo-0.1.1 (c (n "esp-idf-servo") (v "0.1.1") (d (list (d (n "esp-idf-sys") (r "^0.33") (f (quote ("native"))) (k 0)))) (h "04p0wlzyq4n4w4hxms3cgxg2shmba6i4qaa5qxql2bcj484fmc8k")))

(define-public crate-esp-idf-servo-0.1.2 (c (n "esp-idf-servo") (v "0.1.2") (d (list (d (n "esp-idf-sys") (r "^0.33") (f (quote ("native"))) (k 0)))) (h "1pyl0ypxj2v4wbj5hir1nmja7xg59dsx3fpq0q6zb6l37d6l5zm0")))

(define-public crate-esp-idf-servo-0.2.0 (c (n "esp-idf-servo") (v "0.2.0") (d (list (d (n "esp-idf-sys") (r "^0.33") (f (quote ("native"))) (k 0)))) (h "1p0r1f3injg1xfnvf576cyd9gp17rah1f8rmfnvmr761qfyk80dj")))

(define-public crate-esp-idf-servo-0.2.1 (c (n "esp-idf-servo") (v "0.2.1") (d (list (d (n "esp-idf-sys") (r "^0.33") (f (quote ("native"))) (k 0)))) (h "0chsfh1jp30b3cjcyxhmnij11wy33g12vsy3igwf06di0yyh6331")))

(define-public crate-esp-idf-servo-0.2.2 (c (n "esp-idf-servo") (v "0.2.2") (d (list (d (n "esp-idf-sys") (r "^0.33") (f (quote ("native"))) (k 0)))) (h "1zzy3vkkyg7p8xfq6sxbihc34qx9vx48f82msqkxycb25151lmky")))

(define-public crate-esp-idf-servo-0.2.3 (c (n "esp-idf-servo") (v "0.2.3") (d (list (d (n "esp-idf-sys") (r "^0.33") (f (quote ("native"))) (k 0)))) (h "1f6607zq0sihgk01mdp9bjddyr6pmxcg9xjfq8dwjn6mnxyl0c8f")))

(define-public crate-esp-idf-servo-0.2.4 (c (n "esp-idf-servo") (v "0.2.4") (d (list (d (n "esp-idf-sys") (r "^0.33") (f (quote ("native"))) (k 0)))) (h "1vcq8mjpmjhh32i76ym47df0500jlznvp6a8g3h5mw6bkvbqj0m3")))

