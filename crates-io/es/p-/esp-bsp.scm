(define-module (crates-io es p- esp-bsp) #:use-module (crates-io))

(define-public crate-esp-bsp-0.1.0 (c (n "esp-bsp") (v "0.1.0") (h "0d8ni08z87c07ddl4s1140s8k7rqimi8zfdw5gfyrgxqm07ad84v")))

(define-public crate-esp-bsp-0.2.0 (c (n "esp-bsp") (v "0.2.0") (h "10cm528qx8w45q07riygdkwpy48rzy0acjs354f8zzhyhj440r61")))

