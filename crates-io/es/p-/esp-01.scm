(define-module (crates-io es p- esp-01) #:use-module (crates-io))

(define-public crate-esp-01-0.1.0 (c (n "esp-01") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.11") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "itoa") (r "^0.4.4") (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1hy1ban4iygkmyqirmlbznaxqqi2gnwn4py4q8phcyikxk2rbvqf")))

(define-public crate-esp-01-0.1.1 (c (n "esp-01") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.11") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "itoa") (r "^0.4.4") (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1q4z3iivrrjb7h9vri42xg4g0fspf6y2nsfc14av6f65aa77c4yy")))

