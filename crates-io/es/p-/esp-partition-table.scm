(define-module (crates-io es p- esp-partition-table) #:use-module (crates-io))

(define-public crate-esp-partition-table-0.1.0 (c (n "esp-partition-table") (v "0.1.0") (d (list (d (n "embedded-storage") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "md5") (r "^0.7") (o #t) (k 0)))) (h "1z2k1b6k1nnm240s51sn3yxiv0a81lzsqlfj83ajmi0hzaz737rr") (f (quote (("default" "embedded-storage" "md5"))))))

(define-public crate-esp-partition-table-0.1.1 (c (n "esp-partition-table") (v "0.1.1") (d (list (d (n "embedded-storage") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "md5") (r "^0.7") (o #t) (k 0)))) (h "1r0iqi7cb2wcn752ibpabv8scyvjhaz8qkl5chq687smrjnb5xf2") (f (quote (("default" "embedded-storage" "md5"))))))

