(define-module (crates-io es p- esp-pbuf) #:use-module (crates-io))

(define-public crate-esp-pbuf-0.1.0 (c (n "esp-pbuf") (v "0.1.0") (d (list (d (n "embuild") (r "^0.31") (d #t) (k 1)) (d (n "esp-idf-sys") (r "^0.33") (f (quote ("native"))) (k 0)))) (h "12qcpf9crjslq254vmyspg820kac5rfimcbsfvink5h58mbjckky")))

(define-public crate-esp-pbuf-0.2.0 (c (n "esp-pbuf") (v "0.2.0") (d (list (d (n "embuild") (r "^0.31") (d #t) (k 1)) (d (n "esp-idf-sys") (r "^0.33") (f (quote ("native"))) (k 0)))) (h "1wfma2l0sf7c2c89lw6l293mlpmzgi8zldjwk9cbv28wmia630sk")))

