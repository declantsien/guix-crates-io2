(define-module (crates-io es p- esp-idf-unda) #:use-module (crates-io))

(define-public crate-esp-idf-unda-0.1.0 (c (n "esp-idf-unda") (v "0.1.0") (h "0hd9asnqbpg95sfv5s5gmyjfsm6ny38yqqm59nj5idipks1rjzw8")))

(define-public crate-esp-idf-unda-0.1.1 (c (n "esp-idf-unda") (v "0.1.1") (h "0bsq7qzz0d9rp2jw7lwcyqrg4b9g960iamvfbsbvrrnn4habmm8c")))

(define-public crate-esp-idf-unda-0.1.2 (c (n "esp-idf-unda") (v "0.1.2") (h "1c1mnln9c1ammahph39i23m6yn7809i5rmz05whrc2s89g8py14p")))

