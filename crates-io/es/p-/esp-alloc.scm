(define-module (crates-io es p- esp-alloc) #:use-module (crates-io))

(define-public crate-esp-alloc-0.1.0 (c (n "esp-alloc") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.10.1") (f (quote ("const_mut_refs"))) (t "cfg(target_arch = \"riscv32\")") (k 0)) (d (n "linked_list_allocator") (r "^0.10.1") (f (quote ("const_mut_refs"))) (t "xtensa-esp32s2-none-elf") (k 0)) (d (n "linked_list_allocator") (r "^0.10.1") (d #t) (t "xtensa-esp32-none-elf") (k 0)) (d (n "linked_list_allocator") (r "^0.10.1") (d #t) (t "xtensa-esp32s3-none-elf") (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (t "cfg(target_arch = \"riscv32\")") (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (f (quote ("esp32"))) (d #t) (t "xtensa-esp32-none-elf") (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (f (quote ("esp32s2"))) (d #t) (t "xtensa-esp32s2-none-elf") (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (f (quote ("esp32s3"))) (d #t) (t "xtensa-esp32s3-none-elf") (k 0)))) (h "1sp4zs7in3wllcd5i6d6dxl9qqhvg1270ks3p05njvx6da1l2wbl") (f (quote (("oom-handler"))))))

(define-public crate-esp-alloc-0.2.0 (c (n "esp-alloc") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.1.0") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.10.3") (f (quote ("const_mut_refs"))) (k 0)))) (h "1i2k5zfn220rvjvzg3m8hjcz02xgkjixmb77zlya5ka5v1a0g6sl") (f (quote (("oom-handler"))))))

(define-public crate-esp-alloc-0.2.1 (c (n "esp-alloc") (v "0.2.1") (d (list (d (n "critical-section") (r "^1.1.1") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.10.5") (f (quote ("const_mut_refs"))) (k 0)))) (h "08wg7nl91694j8ri936k1albiv2gv3cb70v4lg2vwpjbblwssijb") (f (quote (("oom-handler")))) (r "1.68")))

(define-public crate-esp-alloc-0.3.0 (c (n "esp-alloc") (v "0.3.0") (d (list (d (n "critical-section") (r "^1.1.1") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.10.5") (f (quote ("const_mut_refs"))) (k 0)))) (h "0z8wm150b9bdm48fysys9158yjv58lmym7v772w5ndqs4svjwyc3") (r "1.68")))

