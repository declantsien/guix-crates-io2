(define-module (crates-io es p- esp-idf-alloc) #:use-module (crates-io))

(define-public crate-esp-idf-alloc-0.1.0 (c (n "esp-idf-alloc") (v "0.1.0") (h "1a9mlrkrkswvfnn37wx1dvdnk976l48hms8pvvi8c9nibpjmlb1n")))

(define-public crate-esp-idf-alloc-0.1.1 (c (n "esp-idf-alloc") (v "0.1.1") (h "07mcxcsq9n3r9gy2kpa4rhzgpk6ajybhca99i5cpv7b5d7gv207v")))

