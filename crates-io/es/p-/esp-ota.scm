(define-module (crates-io es p- esp-ota) #:use-module (crates-io))

(define-public crate-esp-ota-0.0.1 (c (n "esp-ota") (v "0.0.1") (h "1vd2m7wvgngwq82vz1qh3pca4hhra78hphv6hdjsc6l04b11sjw7") (y #t)))

(define-public crate-esp-ota-0.1.0 (c (n "esp-ota") (v "0.1.0") (d (list (d (n "embuild") (r "^0.30.3") (d #t) (k 1)) (d (n "esp-idf-sys") (r "^0.31.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "13dvn8z9m6c1sm2hj5p0qy21d8c94ix3w30xzyrfw2yw5i38bwly") (f (quote (("default" "log"))))))

(define-public crate-esp-ota-0.2.0 (c (n "esp-ota") (v "0.2.0") (d (list (d (n "embuild") (r "^0.31.2") (d #t) (k 1)) (d (n "esp-idf-sys") (r "^0.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "10f0072pajcprigkn3nrdmn41y4nvx9fg69ddwyblbsh0mnvcx1m") (f (quote (("default" "log"))))))

