(define-module (crates-io es p- esp-synopsys-usb-otg) #:use-module (crates-io))

(define-public crate-esp-synopsys-usb-otg-0.3.1 (c (n "esp-synopsys-usb-otg") (v "0.3.1") (d (list (d (n "critical-section") (r "^1.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1bv53cvzzgb5brd19xci2l64kwfbmxa5wd2rb4d3n8f013qg4y88") (f (quote (("xcvrdly") ("hs") ("fs") ("esp32sx"))))))

(define-public crate-esp-synopsys-usb-otg-0.3.2 (c (n "esp-synopsys-usb-otg") (v "0.3.2") (d (list (d (n "critical-section") (r "^1.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "usb-device") (r "^0.2.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1v21qmr4vkspwiwd1cpmwv8k9xkbmvg4jil8yxc6if77hx777ijw") (f (quote (("xcvrdly") ("hs") ("fs") ("esp32sx"))))))

(define-public crate-esp-synopsys-usb-otg-0.4.0 (c (n "esp-synopsys-usb-otg") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "ral-registers") (r "^0.1.3") (d #t) (k 0)) (d (n "usb-device") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1dz9z0j1zgd3k6bvqrb373v5d3d62l1fz7wfrvik99d20hx8a2iq") (f (quote (("xcvrdly") ("hs") ("fs") ("esp32sx"))))))

(define-public crate-esp-synopsys-usb-otg-0.4.1 (c (n "esp-synopsys-usb-otg") (v "0.4.1") (d (list (d (n "critical-section") (r "^1.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "ral-registers") (r "^0.1.3") (d #t) (k 0)) (d (n "usb-device") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "04lmk81cnrm0wn5r9fny7fhhmygxwm4dmgsc0wz95sk5l3a8586f") (f (quote (("xcvrdly") ("hs") ("fs") ("esp32sx"))))))

