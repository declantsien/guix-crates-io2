(define-module (crates-io es p- esp-wifi-sys) #:use-module (crates-io))

(define-public crate-esp-wifi-sys-0.0.1 (c (n "esp-wifi-sys") (v "0.0.1") (h "1am65mm7xbjk7i2cjqlncym9am9vwz1am2glvk50m2bjk0yinc1h")))

(define-public crate-esp-wifi-sys-0.1.0 (c (n "esp-wifi-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 1)))) (h "0sfw9375nwqxvdisjdhy48ca3z5ya9ly4r6d67azmbydvd0qlxjz") (f (quote (("esp32s3") ("esp32s2") ("esp32c6") ("esp32c3") ("esp32c2") ("esp32") ("default"))))))

(define-public crate-esp-wifi-sys-0.2.0 (c (n "esp-wifi-sys") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 1)))) (h "0zaq1syxm02w6w6j02q7c5wilbsyiy3g54k9i68n13fxklzynrk3") (f (quote (("esp32s3") ("esp32s2") ("esp32h2") ("esp32c6") ("esp32c3") ("esp32c2") ("esp32") ("default"))))))

(define-public crate-esp-wifi-sys-0.3.0 (c (n "esp-wifi-sys") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 1)))) (h "0m36bis5c06xs1fw1g0rlkx5dzhh6qdk18ggzisld124745m26sm") (f (quote (("esp32s3") ("esp32s2") ("esp32h2") ("esp32c6") ("esp32c3") ("esp32c2") ("esp32") ("default"))))))

