(define-module (crates-io es p- esp-metadata) #:use-module (crates-io))

(define-public crate-esp-metadata-0.1.0 (c (n "esp-metadata") (v "0.1.0") (d (list (d (n "basic-toml") (r "^0.1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ly1wxhiaf9j3dg8m2rgw5gv5jdvrq4xmzrqz35niyz9h6g115qs") (r "1.60.0")))

