(define-module (crates-io es p- esp-at-driver) #:use-module (crates-io))

(define-public crate-esp-at-driver-0.1.0 (c (n "esp-at-driver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "heapless") (r "^0.6.1") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0vvm2j3nldnj23swzp8502l171g2a2z78mgal10r3wq29rmf71yd")))

