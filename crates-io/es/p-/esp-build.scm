(define-module (crates-io es p- esp-build) #:use-module (crates-io))

(define-public crate-esp-build-0.1.0 (c (n "esp-build") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.4.1") (d #t) (k 0)))) (h "0jxbgsn4vycfsicrkm4wmlwy0xqqnivj4nyapjm7pk77fj6lnjmr") (r "1.60.0")))

