(define-module (crates-io es sr essrpc_macros) #:use-module (crates-io))

(define-public crate-essrpc_macros-0.1.0 (c (n "essrpc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12wwxsblp3i8wmvqinfpx9893q1l2f6bqk5wy56c13pjw6njs9rd")))

(define-public crate-essrpc_macros-0.2.0 (c (n "essrpc_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07sh6a7iwfp031bhq7m20n5qh5f2lsd5310xsggknab3qqj21pnh")))

(define-public crate-essrpc_macros-0.3.0 (c (n "essrpc_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b2asy927mfdgw9hsg0ilvrhj4y4bhpjjazynxfrlx1v4ffhcy6n")))

(define-public crate-essrpc_macros-0.4.0 (c (n "essrpc_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11zn3032qis9frs7an1sn1n1ha5v32cwrvah5hasrdw84hjpz67x")))

