(define-module (crates-io es so esso) #:use-module (crates-io))

(define-public crate-esso-1.0.0 (c (n "esso") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1wm5yfr43qcvpd3dg0l7kpa5i05c02qzqz4gzdsld2wnkyq1ki94")))

