(define-module (crates-io es eb eseb) #:use-module (crates-io))

(define-public crate-eseb-0.1.0 (c (n "eseb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "build_stamp") (r "^1") (d #t) (k 0)) (d (n "build_stamp") (r "^1") (d #t) (k 1)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crc16") (r "^0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)))) (h "1vj6k26bf1jj10lpr12zjb68r33snpwd8xdsngc3l7sx57l373w6")))

(define-public crate-eseb-0.1.1 (c (n "eseb") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "build_stamp") (r "^1.0.2") (d #t) (k 0)) (d (n "build_stamp") (r "^1.0.2") (d #t) (k 1)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crc16") (r "^0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)))) (h "1v5lqxf89a0lmxmcfam9d8pbi5akvb389h44858pphcqqwjy8ka0")))

(define-public crate-eseb-0.1.2 (c (n "eseb") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "build_stamp") (r "^1.0.2") (d #t) (k 0)) (d (n "build_stamp") (r "^1.0.2") (d #t) (k 1)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "crc16") (r "^0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)))) (h "078z3z5acarf8xm1xjjs14zldgn4aiiik38syxs205dr34sn70m0")))

