(define-module (crates-io es pi espim) #:use-module (crates-io))

(define-public crate-espim-0.1.0 (c (n "espim") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "ureq") (r "^1.2") (f (quote ("json" "tls"))) (k 0)) (d (n "zip-extract") (r "^0.1.0") (f (quote ("deflate"))) (k 0)))) (h "1cj2z6xar29ab599bv20kdi113h04kdb21q7kcwmdg304vbd1z3l")))

(define-public crate-espim-0.1.1 (c (n "espim") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "ureq") (r "^1.2") (f (quote ("json" "tls"))) (k 0)) (d (n "zip-extract") (r "^0.1.0") (f (quote ("deflate"))) (k 0)))) (h "148zj6jrz660is7cnib3acb4vg0iaiy8jvlmv00v2k4y4q411lgz")))

(define-public crate-espim-0.2.0 (c (n "espim") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "ureq") (r "^1.2") (f (quote ("json" "tls"))) (k 0)) (d (n "zip-extract") (r "^0.1.0") (f (quote ("deflate"))) (k 0)))) (h "0ihbxv2r2rlyy6vp8can2d3mlh0a47qqddfp9ljbh2c47syhk7gk")))

(define-public crate-espim-0.2.1 (c (n "espim") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("json" "tls"))) (k 0)) (d (n "zip-extract") (r "^0.1") (f (quote ("deflate"))) (k 0)))) (h "1ajx621b9k3gjj3nm59kx3zpl55qfgmdvfrf34ilpywglx1xj4wm")))

(define-public crate-espim-0.2.2 (c (n "espim") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("json" "tls"))) (k 0)) (d (n "zip-extract") (r "^0.1") (f (quote ("deflate"))) (k 0)))) (h "0wvhi7k5alsflwl3ard5xqq5fsing6p75zz0hdsjkzcd99aq4r5w")))

