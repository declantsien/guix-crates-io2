(define-module (crates-io es l- esl-compiler) #:use-module (crates-io))

(define-public crate-esl-compiler-0.0.0 (c (n "esl-compiler") (v "0.0.0") (d (list (d (n "lsp-types") (r "~0.94") (d #t) (k 0)) (d (n "pest") (r "~2") (d #t) (k 0)) (d (n "pest_derive") (r "~2") (d #t) (k 0)) (d (n "pretty_assertions") (r "~1") (d #t) (k 2)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "snafu") (r "~0.7") (d #t) (k 0)) (d (n "strum") (r "~0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "~0.1") (d #t) (k 0)) (d (n "url") (r "~2") (d #t) (k 0)))) (h "14avi1awgvci865b24d2hk2p1csh9bd5p07q7bxff4s5z8qvss3a") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

