(define-module (crates-io es re esre) #:use-module (crates-io))

(define-public crate-esre-0.1.0 (c (n "esre") (v "0.1.0") (d (list (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "152w3myxbxqfr8623pq8lpn955hh1h46z77n9d12lv93x1d7hbx6")))

(define-public crate-esre-0.1.1 (c (n "esre") (v "0.1.1") (d (list (d (n "rstest") (r "^0.11.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "15lg8dnc4hd6vp5b4rvszngxkzbzp4wprd63q1120rg94r9zg6rm")))

