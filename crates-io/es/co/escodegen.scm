(define-module (crates-io es co escodegen) #:use-module (crates-io))

(define-public crate-escodegen-0.1.0 (c (n "escodegen") (v "0.1.0") (h "1vnrq8rx8c0m6g7sql96zv1p9wnfrysi6xiqalxbi6rcxxg3n5sy")))

(define-public crate-escodegen-0.1.1 (c (n "escodegen") (v "0.1.1") (h "1nffaq7s0fyfihpzzc6gl5fdl4x8ydbzxgm222vic2pgmsfdbhfg")))

(define-public crate-escodegen-0.1.2 (c (n "escodegen") (v "0.1.2") (h "1jac8zx2a4scx6hnfzah4vz88shy82n0ii5wbq1i833fzh4pprmg")))

(define-public crate-escodegen-0.2.0 (c (n "escodegen") (v "0.2.0") (h "1w8x1svx96jkq5z73vqy8mwvj5phcrm383xil2pszgkmyhwgx4h1")))

(define-public crate-escodegen-0.2.1 (c (n "escodegen") (v "0.2.1") (h "0b66ap3kyq8nmdhh7wwp2kzzf0rblyy45w5vszc9sy4xw1idkkfd")))

(define-public crate-escodegen-0.3.0 (c (n "escodegen") (v "0.3.0") (h "03agrm401ffpl0gqy50ynb67nav92hadb9z7hl7b4dds0g3g63wa")))

(define-public crate-escodegen-0.3.1 (c (n "escodegen") (v "0.3.1") (h "0309y5j9gq00gmsrswkbyqyr4qp53sh03j6lfalgg53py4yjaa4s") (y #t)))

(define-public crate-escodegen-0.3.2 (c (n "escodegen") (v "0.3.2") (h "12ccl1n7xasrkbhjpi5j07vvj611ihg1lgldfxaa3anqx4i1hxkp")))

(define-public crate-escodegen-0.3.3 (c (n "escodegen") (v "0.3.3") (h "0akkavij1axjm0gvq118waxbzi7njk98vgjs5r1rkavwvfa3qad1")))

(define-public crate-escodegen-0.4.0 (c (n "escodegen") (v "0.4.0") (h "0f5h3dw9bhf3ls6613s5cxyz1c0krsg8fi74vspz73afp6rmqdqn")))

(define-public crate-escodegen-0.4.1 (c (n "escodegen") (v "0.4.1") (h "1lwqd6cqqmyylwx4rmija5r50vww9w4aj860w13f2vlfpqlnkwlr")))

(define-public crate-escodegen-0.4.2 (c (n "escodegen") (v "0.4.2") (h "1pa56j0s1gdjwb57vrgm84f00cqbvyp9mkn6yz8chw00jgpz3myi")))

(define-public crate-escodegen-0.4.3 (c (n "escodegen") (v "0.4.3") (h "13m2lx8xkdqh4r4pjfnna3929xx88y1kpcidm8dh6m1x03mc35j7")))

(define-public crate-escodegen-0.4.4 (c (n "escodegen") (v "0.4.4") (h "0q0na3xn8wwv9z7vh48ss6rskxwhfb8kz0xyqg0qjaph52vyzl2y")))

