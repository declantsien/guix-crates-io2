(define-module (crates-io es dm esdm-sys) #:use-module (crates-io))

(define-public crate-esdm-sys-0.0.4 (c (n "esdm-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.69.4") (f (quote ("runtime"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1pj7bqd115ncxr3736236g6wiv1x35a7b34skq0a461s590fs75q")))

(define-public crate-esdm-sys-0.0.6 (c (n "esdm-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.69.4") (f (quote ("runtime"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1j6ciflqhdm5w7dq7bmgqngq9mdv312y9lqhhz7gkhzjx2mvhlv8")))

(define-public crate-esdm-sys-0.1.0 (c (n "esdm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (f (quote ("runtime"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1wng205c7057gz1n0xagn27rr3w9vyvipbsrdgddcn9l63p7jgaw")))

(define-public crate-esdm-sys-0.1.1 (c (n "esdm-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (f (quote ("runtime"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "05rfh78g4nrzk0w08acnb98ii0dja5p62scki55j7imnhgncf6pm")))

