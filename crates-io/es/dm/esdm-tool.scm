(define-module (crates-io es dm esdm-tool) #:use-module (crates-io))

(define-public crate-esdm-tool-0.0.6 (c (n "esdm-tool") (v "0.0.6") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand-esdm") (r "^0.0.6") (d #t) (k 0)))) (h "0hfkkims1s5427hgx9nhphkrzzbh62f2sh2vwv07fsyjs5k3cvz5") (f (quote (("default"))))))

(define-public crate-esdm-tool-0.1.0 (c (n "esdm-tool") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand-esdm") (r "^0.1.0") (d #t) (k 0)))) (h "1g87kqsddbj9d1py5d0wc2zyqlblz0m3lpnq4mhi19zsgb9q2anf") (f (quote (("default"))))))

(define-public crate-esdm-tool-0.1.1 (c (n "esdm-tool") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand-esdm") (r "^0.1.1") (d #t) (k 0)))) (h "02923im02qc75aij8apiqlfb2lbqvcr2j5jnvka7lbj0shlvq2zf") (f (quote (("default"))))))

(define-public crate-esdm-tool-0.1.2 (c (n "esdm-tool") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand-esdm") (r "^0.1.1") (d #t) (k 0)))) (h "07vg6hhxpc6fh60ryh4qf28hjqhgqjgx6dmafvv8yx0iy6wdpf79") (f (quote (("default"))))))

