(define-module (crates-io es bu esbuild-config) #:use-module (crates-io))

(define-public crate-esbuild-config-0.1.0 (c (n "esbuild-config") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.0") (d #t) (k 0)))) (h "08bflwlpxlzar9imzici91ymwwrh3axpws103790p6zy4ay4qaa6")))

(define-public crate-esbuild-config-0.2.0 (c (n "esbuild-config") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.0") (d #t) (k 0)))) (h "1rikn0mcpf4lsv1agb8wg6bjsnkqv1axw5dph03mp2bxxcb7zp4b")))

