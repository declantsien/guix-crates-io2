(define-module (crates-io es bu esbuild-rs) #:use-module (crates-io))

(define-public crate-esbuild-rs-0.0.1 (c (n "esbuild-rs") (v "0.0.1") (h "0r9r1z0iycqyrrhs47n6ry0hkrpy7r6c7qbg3qybwi66swz2rbmd")))

(define-public crate-esbuild-rs-0.0.2 (c (n "esbuild-rs") (v "0.0.2") (h "0vd0964w6gg3b0vc3dx1yxc56ykka0jd6s5fimrn9bmam9h7z027")))

(define-public crate-esbuild-rs-0.0.3 (c (n "esbuild-rs") (v "0.0.3") (h "079bil7lpncbpi0j6cgy7k6ggfl9wp3m586difyqx6700j2d4ka7")))

(define-public crate-esbuild-rs-0.0.4 (c (n "esbuild-rs") (v "0.0.4") (h "0rcrxcr4hq9clpv7bpw98ck203636zsa6jb4asgnd2mnnimv930d")))

(define-public crate-esbuild-rs-0.0.5 (c (n "esbuild-rs") (v "0.0.5") (h "046vb0a3ha8z9a4rqidvrhbcpr38vkwcii7dc4cc6l92lw64slxa")))

(define-public crate-esbuild-rs-0.0.6 (c (n "esbuild-rs") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "1p34y69mrixlld44j9v6bmlk044iyyjgx23k2hwm2xn0msv8vf4c")))

(define-public crate-esbuild-rs-0.0.7 (c (n "esbuild-rs") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "13is9byyhy78a5p390il9zwggr7vl8p451ij72h7sfydsgjzwc83")))

(define-public crate-esbuild-rs-0.0.8 (c (n "esbuild-rs") (v "0.0.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "0a8pwr7w40507nl2pjxb9m74ixc0285bgdd5wdg4kkpz7y7h2dpd")))

(define-public crate-esbuild-rs-0.1.0 (c (n "esbuild-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "1ypanwbm14y1m77zn24xm0di7fhj9f397l7ahbdiph9psz37c79l")))

(define-public crate-esbuild-rs-0.1.1 (c (n "esbuild-rs") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "14l5nbz5mcj9rsazlbhragpm5gs0fzcvg5g4abnc5q7j353gbgmg")))

(define-public crate-esbuild-rs-0.1.2 (c (n "esbuild-rs") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "0y8a5r705r19k0hfnddrzzhygq5di9gghklbnwmx4kk4ic47gdqf")))

(define-public crate-esbuild-rs-0.1.3 (c (n "esbuild-rs") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "0j2acqh1p10a43vbqc3d2jj24ha80353pdmzv6x2n93razvcwb9b")))

(define-public crate-esbuild-rs-0.2.0 (c (n "esbuild-rs") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "0icd3rh11wwr8ljz9ig6689y27qgg14wv7qa0xxi1p0f2jsv0wl7")))

(define-public crate-esbuild-rs-0.2.1 (c (n "esbuild-rs") (v "0.2.1") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "11qbnqa5vpnxvxr0nkgpfxj537dnnq2x59dbixyrfr3lyj4jps0g")))

(define-public crate-esbuild-rs-0.8.30 (c (n "esbuild-rs") (v "0.8.30") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "1sgsqhzv0waaw9a0j2hljbiya7c0zyr7bs70v5jnpg106pm3krnf")))

(define-public crate-esbuild-rs-0.12.18 (c (n "esbuild-rs") (v "0.12.18") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "0x6nixi1asmzdnjp57y3fnfyygrzrpkvk42c77wznjnr6fpszgwn")))

(define-public crate-esbuild-rs-0.12.19 (c (n "esbuild-rs") (v "0.12.19") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "1d2ka2610c41x62hg3cy6nyaijssmzci9977dcync9fxld2bplz8")))

(define-public crate-esbuild-rs-0.13.8 (c (n "esbuild-rs") (v "0.13.8") (d (list (d (n "async-std") (r "^1.6") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memorymodule-rs") (r "^0.0.3") (d #t) (t "cfg(target_env = \"msvc\")") (k 0)))) (h "14wvv648i2728ck4ihi7zgp4n4ff9ynr6jc75yf5a05xv99j6sji")))

