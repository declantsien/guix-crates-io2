(define-module (crates-io es vc esvc-core) #:use-module (crates-io))

(define-public crate-esvc-core-0.0.0 (c (n "esvc-core") (v "0.0.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "esvc-traits") (r "^0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k9bcfvcq2bznwfisqmb0b9m9121qa9mm9ys290pxa39xv80khc5")))

(define-public crate-esvc-core-0.1.0 (c (n "esvc-core") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "esvc-traits") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1mpznzm1nhii2k2crsxbk0gmzb98jjx36msf3h6gm3m5byrcaifv")))

