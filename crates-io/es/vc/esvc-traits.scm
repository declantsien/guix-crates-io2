(define-module (crates-io es vc esvc-traits) #:use-module (crates-io))

(define-public crate-esvc-traits-0.0.0 (c (n "esvc-traits") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)))) (h "0c3bzfmnlh9n3pmrkngnj50c2vmzh6sr7gvskampmw8l6bz63k3c")))

(define-public crate-esvc-traits-0.1.0 (c (n "esvc-traits") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (k 0)))) (h "1vp0a40qfi7vvhz6ccyvw6vmq1knjcxw7flhi19wpsgk6vr687mc")))

