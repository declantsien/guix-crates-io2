(define-module (crates-io es vc esvc-wasm) #:use-module (crates-io))

(define-public crate-esvc-wasm-0.0.0 (c (n "esvc-wasm") (v "0.0.0") (d (list (d (n "esvc-traits") (r "^0.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.33") (f (quote ("cranelift" "parallel-compilation"))) (k 0)))) (h "18xray0qazcsiyhj87yk5464rhbdhpyqsp4i03l7h6j72bygqg9f")))

(define-public crate-esvc-wasm-0.1.0 (c (n "esvc-wasm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "esvc-traits") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.33") (f (quote ("cranelift" "parallel-compilation"))) (k 0)))) (h "1wvraqr5jmh530kv9q3n6fcaawgpmbi10kx4wwd5gzzydjmb2mnb")))

