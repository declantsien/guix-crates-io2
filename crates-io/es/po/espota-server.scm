(define-module (crates-io es po espota-server) #:use-module (crates-io))

(define-public crate-espota-server-0.3.0 (c (n "espota-server") (v "0.3.0") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "iron") (r "^0.6.0") (d #t) (k 0)) (d (n "mount") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.80") (d #t) (k 0)) (d (n "staticfile") (r "^0.5.0") (d #t) (k 0)) (d (n "version-compare") (r "^0.0.8") (d #t) (k 0)))) (h "0f9673cg5zsvbzkv075w3hjp3m3h0j7nwzgfakxz0nm6hc4r11x2")))

