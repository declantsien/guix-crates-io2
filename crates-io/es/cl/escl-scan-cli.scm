(define-module (crates-io es cl escl-scan-cli) #:use-module (crates-io))

(define-public crate-escl-scan-cli-0.1.1 (c (n "escl-scan-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "scan") (r "^0.1.1") (d #t) (k 0) (p "escl-scan")))) (h "1cgcxv11r7pghhmzvl41y73bcfmkn4r96in0l28hn3rmhhndg1bq")))

