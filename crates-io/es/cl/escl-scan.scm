(define-module (crates-io es cl escl-scan) #:use-module (crates-io))

(define-public crate-escl-scan-0.1.0 (c (n "escl-scan") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)))) (h "1dxy56fpl4g6iy8i6k1cvkfrrhcjb34vczr836k7c51c7fqx0lgd")))

(define-public crate-escl-scan-0.1.1 (c (n "escl-scan") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)))) (h "1x5x07hf9qv3461jmlwr674g0xzi1b57s7hgl7l01l14zp8rqs4b")))

