(define-module (crates-io es cl escl) #:use-module (crates-io))

(define-public crate-escl-0.1.0 (c (n "escl") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt-multi-thread"))) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "15hkcjv2rv5knmx8mlsq7piybspqj3cw86lz0lh6f30l33d72dm1")))

(define-public crate-escl-0.1.1 (c (n "escl") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "mdns") (r "^3.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt-multi-thread"))) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1bg8qwklx9n4h6v53sl4b1wbb9sxi2rzc9k6mdbnc7x0xj3rni5a")))

(define-public crate-escl-0.2.0 (c (n "escl") (v "0.2.0") (d (list (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "mdns") (r "^3.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt-multi-thread"))) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0jcdhv3jw4yx7p7q23lxv41vxb83mj09vyjy4xhdkvpxh6r33ajb")))

