(define-module (crates-io es dl esdl) #:use-module (crates-io))

(define-public crate-esdl-0.1.0 (c (n "esdl") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gcc0jy7z3kxpkiy79vly86mdbz9hfpxng3ksy9g5slkcv1zjnfn") (f (quote (("default") ("codegen"))))))

(define-public crate-esdl-0.2.0 (c (n "esdl") (v "0.2.0") (d (list (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q601lm1libpsnxzvng33p51gp06znpc9s6jjdbjlzdf7i4a63rm") (f (quote (("default") ("codegen"))))))

(define-public crate-esdl-0.2.1 (c (n "esdl") (v "0.2.1") (d (list (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hpchhbm708xiad6dqqrg5r85ygk1bnjpwvhxdfcyyvpx6jy1383") (f (quote (("default") ("codegen"))))))

(define-public crate-esdl-0.3.0 (c (n "esdl") (v "0.3.0") (d (list (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p263vngrnr68562gpirr0hbfhxrwl92kcly54myq78scfncaxqz") (f (quote (("default") ("codegen-typescript") ("codegen-rust"))))))

(define-public crate-esdl-0.4.0 (c (n "esdl") (v "0.4.0") (d (list (d (n "heck") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-supreme") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i2spzphc5ybnr9xm8armskxj9gxn83bzrp0kkcl8g8m57ff5y6m") (f (quote (("wasm") ("default") ("codegen-typescript") ("codegen-rust" "heck"))))))

(define-public crate-esdl-0.6.0 (c (n "esdl") (v "0.6.0") (d (list (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "nom-supreme") (r "^0.8") (d #t) (k 0)) (d (n "semver") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i1vn8xv79gx9hamy0rgknkk6r430rswch40zlx3wp42rmawxhvl")))

