(define-module (crates-io es i_ esi_fastly) #:use-module (crates-io))

(define-public crate-esi_fastly-0.1.0 (c (n "esi_fastly") (v "0.1.0") (d (list (d (n "esi") (r "^0.1") (d #t) (k 0)) (d (n "fastly") (r "^0.7") (d #t) (k 0)))) (h "0dk7m48fjxab5n5g2fni4ayj2l1r84lk4r4rn9fsd16dfclln5a7")))

(define-public crate-esi_fastly-0.1.1 (c (n "esi_fastly") (v "0.1.1") (d (list (d (n "esi") (r "^0.1") (d #t) (k 0)) (d (n "fastly") (r "^0.7") (d #t) (k 0)))) (h "126sppvv7v9q20bf4kf4zvxn41jw7ngshhwashgap3y8qfd5apl8")))

(define-public crate-esi_fastly-0.1.2 (c (n "esi_fastly") (v "0.1.2") (d (list (d (n "esi") (r "^0.1") (d #t) (k 0)) (d (n "fastly") (r "^0.7") (d #t) (k 0)))) (h "0avv273pwdcx20v0a4ghw2q5qy543qid919cyx0cjw2gmmhwbvvc")))

(define-public crate-esi_fastly-0.1.3 (c (n "esi_fastly") (v "0.1.3") (d (list (d (n "esi") (r "^0.1") (d #t) (k 0)) (d (n "fastly") (r "^0.7") (d #t) (k 0)))) (h "1j5l7hymv167fgabzck6z1c1svp84s972s4pmrbq1bdsc61rj4gs")))

