(define-module (crates-io es i_ esi_client) #:use-module (crates-io))

(define-public crate-esi_client-0.8.6 (c (n "esi_client") (v "0.8.6") (d (list (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "12wvm2kqg1pdyzirj1kwk31qygy5hap5lppsppacybnnsyh1jqx0") (y #t)))

(define-public crate-esi_client-0.0.1 (c (n "esi_client") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0ga7yiy543bgq6bgprvkbj0k23mlcxid7fiq9icdw6gj3xddk0a7")))

