(define-module (crates-io es pl esplugin-ffi) #:use-module (crates-io))

(define-public crate-esplugin-ffi-1.0.0 (c (n "esplugin-ffi") (v "1.0.0") (d (list (d (n "cbindgen") (r "^0.1.14") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0701q2dl0ggh88wxf8xakwxlnxvx23g3iflj0k7wfjl7xzg4v17l") (f (quote (("ffi-headers" "cbindgen") ("default")))) (y #t)))

(define-public crate-esplugin-ffi-1.0.1 (c (n "esplugin-ffi") (v "1.0.1") (d (list (d (n "cbindgen") (r "^0.1.14") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15r2iyg0p8k1m9kfcgw8hw6z5p41480r9skkj6zbllfs4s5kp3bb") (f (quote (("ffi-headers" "cbindgen") ("default")))) (y #t)))

(define-public crate-esplugin-ffi-1.0.2 (c (n "esplugin-ffi") (v "1.0.2") (d (list (d (n "cbindgen") (r "^0.1.14") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1liin8vpjmnkw0nw7m366q9v6rkn3dky8l004gz9qgybfrxa962h") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-1.0.3 (c (n "esplugin-ffi") (v "1.0.3") (d (list (d (n "cbindgen") (r "^0.1.14") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^1.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03ppzdj96w71v2by3jv0awy71kcfwjfpg372wdvgqrpl10m26spj") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-1.0.5 (c (n "esplugin-ffi") (v "1.0.5") (d (list (d (n "cbindgen") (r "^0.1.14") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^1.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0izv7fsx24c58vjr09s6ln7dcf2lph61ynaa93havvmkpzzgkss9") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-1.0.6 (c (n "esplugin-ffi") (v "1.0.6") (d (list (d (n "cbindgen") (r "^0.1.27") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^1.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qkwm1yn2nlyjq1sr5zckwna3iz3x175n4hshgyf6cjn4bacvh5g") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-1.0.7 (c (n "esplugin-ffi") (v "1.0.7") (d (list (d (n "cbindgen") (r "^0.1.27") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^1.0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00lsh8l9yrcx4gbbi93zpg2nii8ivjlcyk5905s24av4lhqxb1n5") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-1.0.8 (c (n "esplugin-ffi") (v "1.0.8") (d (list (d (n "cbindgen") (r "^0.4.3") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^1.0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q2ikdbcnhxx716z00cyrw3093n1hzngjnjg7bkq27jvhqm887gl") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-1.0.9 (c (n "esplugin-ffi") (v "1.0.9") (d (list (d (n "cbindgen") (r "^0.5.2") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^1.0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y7cq436drckz618wj4vgx5ms5m731z6lnbmcwafin3jl70kciv4") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-1.0.10 (c (n "esplugin-ffi") (v "1.0.10") (d (list (d (n "cbindgen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^1.0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jmvy8brzmqimyh08xa7rrv7r937zp4g07pbcgpis4a0038axbl5") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-2.0.0 (c (n "esplugin-ffi") (v "2.0.0") (d (list (d (n "cbindgen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pgkh6vnyci97vn0rlsjqnbr7sg86vn3ph392nk2qjj6kcp5ldzl") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-2.0.1 (c (n "esplugin-ffi") (v "2.0.1") (d (list (d (n "cbindgen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^2.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10apfmd7vprxq1ii863zznh723h0wfbqv6p2iqhrn9z25dr1gama") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-2.1.0 (c (n "esplugin-ffi") (v "2.1.0") (d (list (d (n "cbindgen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^2.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pmwi72sbmmkhrqkl3xhff94v5hpx95z5p8g1c5hcr339f9znapk") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-2.1.1 (c (n "esplugin-ffi") (v "2.1.1") (d (list (d (n "cbindgen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^2.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03mvyjd4mqdvbzg5zvcv3ghzr2zqc2waqp0fzph75h15kvq0fp13") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-esplugin-ffi-2.1.2 (c (n "esplugin-ffi") (v "2.1.2") (d (list (d (n "cbindgen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "esplugin") (r "^2.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zbx567jvqgamljh7l3bm0phmdfqpcsih0kjmchhd0mcmadpkk8a") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

