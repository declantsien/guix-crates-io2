(define-module (crates-io es pl esplora-api) #:use-module (crates-io))

(define-public crate-esplora-api-0.1.0 (c (n "esplora-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.8") (f (quote ("json" "blocking"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.5") (f (quote ("rt-threaded" "macros" "test-util"))) (d #t) (k 2)))) (h "0k83qldpbcz2kdqikvinanmy4yky9ch893d3wdbjl24ndscvrl1z") (f (quote (("default" "reqwest" "reqwest/default") ("blocking" "reqwest" "reqwest/blocking"))))))

