(define-module (crates-io es pl esplora-btc-api) #:use-module (crates-io))

(define-public crate-esplora-btc-api-1.0.0 (c (n "esplora-btc-api") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1fzyf1lg3ais917w53a8f80ayjmbn369q7dn6aka9xcyk44lnws5")))

(define-public crate-esplora-btc-api-1.0.1 (c (n "esplora-btc-api") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "18j57fb3crh99rnrmwyzinz3s2hnfn7nr6zjrikkydrgyq6127rs")))

(define-public crate-esplora-btc-api-1.0.2 (c (n "esplora-btc-api") (v "1.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0sysb39axjwp4l10d1ak1hrzr249zm938nj8dhy6xv1imbkqwj5a")))

(define-public crate-esplora-btc-api-1.0.3 (c (n "esplora-btc-api") (v "1.0.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "default-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0raq426ah45yx9n04zf0va2dacqcgahi8lm0iz4x5ilrrvaxn37r")))

(define-public crate-esplora-btc-api-1.0.5-rc6 (c (n "esplora-btc-api") (v "1.0.5-rc6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "default-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0af0hmsgl2447q8qilz5m97p0ff6j7v9sq0jjv1rahjr9xm31hfl")))

(define-public crate-esplora-btc-api-1.0.5 (c (n "esplora-btc-api") (v "1.0.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "default-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1fvs3jhcxc538h0fawr0jfic3r8z2hlgfmr9y2par5x0d0rndk2w")))

(define-public crate-esplora-btc-api-1.1.0 (c (n "esplora-btc-api") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "default-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0ylxi99skykq6bmmfl421lc0g28f6r05n68l05gd9c1izlq964iy")))

(define-public crate-esplora-btc-api-1.2.0 (c (n "esplora-btc-api") (v "1.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "default-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1rv83rmyyjpsq6ij0gcyhx0kxywyzzzz6ld3j5x5h55hvk48hbky")))

(define-public crate-esplora-btc-api-1.3.0 (c (n "esplora-btc-api") (v "1.3.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "default-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0rdy4swbvkd2qnvf129m7k1d5nvs2v1xbfdzh5qdy8glazi9sc7h")))

