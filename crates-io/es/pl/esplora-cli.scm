(define-module (crates-io es pl esplora-cli) #:use-module (crates-io))

(define-public crate-esplora-cli-0.1.0 (c (n "esplora-cli") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.31.0") (f (quote ("serde" "std"))) (k 0)) (d (n "bitcoin_hashes") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "esplora-client") (r "^0.7.0") (f (quote ("blocking-https"))) (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (f (quote ("alloc"))) (k 0) (p "hex-conservative")) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "14pp5fgmgx1wc4ih940mwygy36c8v6kw554v66h45prk33gd33pp") (r "1.63.0")))

