(define-module (crates-io es pa espalier) #:use-module (crates-io))

(define-public crate-espalier-0.1.0 (c (n "espalier") (v "0.1.0") (h "0z8rbkzwkzxa3whxp75i5frvvb2hwl5nprj1wa01vqlkhcplp4xw")))

(define-public crate-espalier-0.2.0 (c (n "espalier") (v "0.2.0") (h "0z9k7vrw8y0jv31k18jkqzcacnqjc9hhlk5klrrqq8s3r7r33r18")))

(define-public crate-espalier-0.3.0 (c (n "espalier") (v "0.3.0") (h "0kx0dbi44vci896avfh1p9xl4y0a81286p04wi7w1jxa0lb4zjk4")))

(define-public crate-espalier-0.4.0 (c (n "espalier") (v "0.4.0") (h "133plrrjcwx5q6ssj16av88h5wprlizldn2gkvndcwnlpiclarfh")))

(define-public crate-espalier-0.4.1 (c (n "espalier") (v "0.4.1") (h "0fwbbs5rhxjf5r6yf15rhsz26hh2qh8zi23j3jf4gcw8yv6939l4")))

