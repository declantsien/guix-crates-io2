(define-module (crates-io es pa espadrille) #:use-module (crates-io))

(define-public crate-espadrille-0.1.0 (c (n "espadrille") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "102ffnpdqgqp4jidfv2clb2dqvbz4c1gq056zfcd3478bkw6kadw")))

