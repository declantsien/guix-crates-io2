(define-module (crates-io es pa espada) #:use-module (crates-io))

(define-public crate-espada-0.1.0 (c (n "espada") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1i0h7fq0p11lvw8ksmkz5c381qh4di4k657sbzlyksfk3bx0sa7h")))

(define-public crate-espada-0.2.0 (c (n "espada") (v "0.2.0") (d (list (d (n "cargo-insta") (r "^1.36.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "00p4mmw6x8yb9m127gily7by1n7fhib3dd47sd5mbp4jb932q5dj")))

(define-public crate-espada-0.3.0 (c (n "espada") (v "0.3.0") (d (list (d (n "cargo-insta") (r "^1.36.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1lc42ghzrqf5qilspvbyvzxi9zbfp6hzzh1glxjvkr6x1v08v8sr")))

(define-public crate-espada-0.3.1 (c (n "espada") (v "0.3.1") (d (list (d (n "cargo-insta") (r "^1.36.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "insta") (r "^1.36.1") (d #t) (k 2)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0mxm8q085z9n7hnpw7wkw03axn9cliqhlxp2afc8q0imdaqnl9bi")))

