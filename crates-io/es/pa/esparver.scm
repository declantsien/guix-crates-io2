(define-module (crates-io es pa esparver) #:use-module (crates-io))

(define-public crate-esparver-0.0.1 (c (n "esparver") (v "0.0.1") (h "12w2i42gz5dqdhdvkwwg61xmcihybxkghwma4h5iyfd008fs88yh")))

(define-public crate-esparver-0.0.2 (c (n "esparver") (v "0.0.2") (h "13ds6yi0nscpqk4zmzjr5k64jbf6r8nzczdssbmns0xqfpac94ca")))

(define-public crate-esparver-0.0.3 (c (n "esparver") (v "0.0.3") (d (list (d (n "carapace_spec_clap") (r "^0.1.11") (d #t) (k 0)) (d (n "clap") (r "^4.3.5") (d #t) (k 0)) (d (n "clap_complete") (r "^4.3.1") (d #t) (k 0)))) (h "0lpylmnr69s2s63ml1rn2n1klcpzkfnyyh1maid0qdahqwbivpvw")))

(define-public crate-esparver-0.0.4 (c (n "esparver") (v "0.0.4") (d (list (d (n "carapace_spec_clap") (r "^0.1.11") (d #t) (k 0)) (d (n "clap") (r "^4.3.5") (d #t) (k 0)) (d (n "clap_complete") (r "^4.3.1") (d #t) (k 0)))) (h "13vwi6llkkk8hf7cwj8mdjqq20by9031vjp01gy08xbg3clkgpsq")))

(define-public crate-esparver-0.0.5 (c (n "esparver") (v "0.0.5") (d (list (d (n "carapace_spec_clap") (r "^0.1.11") (d #t) (k 0)) (d (n "clap") (r "^4.3.5") (d #t) (k 0)) (d (n "clap_complete") (r "^4.3.1") (d #t) (k 0)))) (h "03y51949y52mrkl2dj2207hkwx93azdys610m08jj0hz4q6pm0q7")))

(define-public crate-esparver-0.0.6 (c (n "esparver") (v "0.0.6") (d (list (d (n "carapace_spec_clap") (r "^0.1.11") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.3.1") (d #t) (k 0)))) (h "0ajyqvlscvis5dwpnwfbysxyff2xgr1fail0khd5lkymajcm0axv")))

