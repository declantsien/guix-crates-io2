(define-module (crates-io es te estel_secp256k1) #:use-module (crates-io))

(define-public crate-estel_secp256k1-0.1.0 (c (n "estel_secp256k1") (v "0.1.0") (d (list (d (n "hmac") (r "^0.11.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "04mvxk993ldsn9g88ydi8hchm5p5xyrpidnzl12hmylyqiasl49l")))

(define-public crate-estel_secp256k1-0.0.2 (c (n "estel_secp256k1") (v "0.0.2") (d (list (d (n "hmac") (r "^0.11.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "02zzigfzbhbp7dfcr74igy3qrliv8kaadq1lg6xglgmplr5iil74") (y #t)))

(define-public crate-estel_secp256k1-0.2.0 (c (n "estel_secp256k1") (v "0.2.0") (d (list (d (n "hmac") (r "^0.11.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "04j8ygl7dd3mw0v7izv4kzrbc7hqfn2xwivgk30cm9ka9xyyzalm")))

(define-public crate-estel_secp256k1-0.3.0 (c (n "estel_secp256k1") (v "0.3.0") (d (list (d (n "hmac") (r "^0.11.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "1d36779h20f7wadyfqma07vhiidlsfhybz203h785pjbkswy3alm")))

(define-public crate-estel_secp256k1-0.4.0 (c (n "estel_secp256k1") (v "0.4.0") (d (list (d (n "hmac") (r "^0.11.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "1yzhghf2fr09n4fq684995xgn5s7jx8iq5nwqjjkfpknq6x0fggz")))

