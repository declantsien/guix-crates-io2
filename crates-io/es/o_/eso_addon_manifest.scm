(define-module (crates-io es o_ eso_addon_manifest) #:use-module (crates-io))

(define-public crate-eso_addon_manifest-0.1.0 (c (n "eso_addon_manifest") (v "0.1.0") (d (list (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0w4rpwgmgsqx2a40r46nm30r15d2y35f7w6z6sj85vws4pz4dsqr") (y #t)))

(define-public crate-eso_addon_manifest-0.1.1 (c (n "eso_addon_manifest") (v "0.1.1") (d (list (d (n "regex") (r "^1.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "12jxkxsjnvnry4d0f842i73vs8qsvndc071ia062v9c75iizcj81")))

