(define-module (crates-io es ax esaxx-rs) #:use-module (crates-io))

(define-public crate-esaxx-rs-0.1.0 (c (n "esaxx-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0li56vm2ccbc4r764kqkd42ak4g22z4hs8vqnp3nm512fwvggwkh")))

(define-public crate-esaxx-rs-0.1.1 (c (n "esaxx-rs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1742zp2wgvi3z4gci17i2lh700jh4mddz2n77brhhxcgwaf6w6w7")))

(define-public crate-esaxx-rs-0.1.2 (c (n "esaxx-rs") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "19h8ffyahvmrkmwrgbd5ix3bf1qw574f11vls0ddgpkfbf1mj5dz")))

(define-public crate-esaxx-rs-0.1.3 (c (n "esaxx-rs") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0r7ab0napgkqw5ch2x3l6l7a1idffx60ksjys1hj9y8m3libzw53")))

(define-public crate-esaxx-rs-0.1.4 (c (n "esaxx-rs") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1ma3sfqg8vfqmzp1d3y3kqvmdwcp37l84sx3gbz0ik28zij3kfg6")))

(define-public crate-esaxx-rs-0.1.5 (c (n "esaxx-rs") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0g2pixys01g1xriwsjzh9lrbd82lnzprn5g658ysv305zrdcfg59")))

(define-public crate-esaxx-rs-0.1.6 (c (n "esaxx-rs") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1iqx3fkfniciimz6qplyk3hp0h72idhg7mvlz8f8v29xcbn0g30k")))

(define-public crate-esaxx-rs-0.1.7 (c (n "esaxx-rs") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "05p7lzgqqmp8xqmi7m1q62m4j4s746kj486k7mxfjd5pa6rifikg")))

(define-public crate-esaxx-rs-0.1.8 (c (n "esaxx-rs") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0d8abipgfsaknrj5phlv34133sai73mmr2rgykazxagc7hjqnx0z") (f (quote (("default" "cpp") ("cpp" "cc"))))))

(define-public crate-esaxx-rs-0.1.9 (c (n "esaxx-rs") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0wrl1q61jfn2idrbmpfg51prp7b63fz0w6pry9yk8mrhk8a3b6nx") (f (quote (("default" "cpp") ("cpp" "cc"))))))

(define-public crate-esaxx-rs-0.1.10 (c (n "esaxx-rs") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1rm6vm5yr7s3n5ly7k9x9j6ra5p2l2ld151gnaya8x03qcwf05yq") (f (quote (("default" "cpp") ("cpp" "cc"))))))

