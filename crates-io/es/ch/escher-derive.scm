(define-module (crates-io es ch escher-derive) #:use-module (crates-io))

(define-public crate-escher-derive-0.1.0 (c (n "escher-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1aayfvnfvjnx9xgdz0nhk70619hh3vwkgjjiw6gw7ysbwnxily00")))

(define-public crate-escher-derive-0.2.0 (c (n "escher-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0m930kdyfv6sfnlkc8z6vkvbmvmxq2kslw29lmvl7f9272gh7mgr")))

