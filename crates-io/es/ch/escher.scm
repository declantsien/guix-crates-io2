(define-module (crates-io es ch escher) #:use-module (crates-io))

(define-public crate-escher-0.1.0 (c (n "escher") (v "0.1.0") (d (list (d (n "futures-task") (r "^0.3") (d #t) (k 0)))) (h "0815czhv089l5rni6hm25aabs4s54fd2yfsklzhngl861fzpkp32")))

(define-public crate-escher-0.2.0 (c (n "escher") (v "0.2.0") (d (list (d (n "escher-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "futures-task") (r "^0.3") (d #t) (k 0)))) (h "0ypjh5rqrlbni510da7g2801cxjqp1ih25dzsbwqhrwd2f11pdkn")))

(define-public crate-escher-0.3.0 (c (n "escher") (v "0.3.0") (d (list (d (n "escher-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-task") (r "^0.3") (d #t) (k 0)))) (h "08jfp0wc9hc3dwh7vr76djv7g5dkhb4p8ay83451y1358f1j127i")))

