(define-module (crates-io es ri esri_ascii_grid) #:use-module (crates-io))

(define-public crate-esri_ascii_grid-0.1.0 (c (n "esri_ascii_grid") (v "0.1.0") (h "02qssh33kb0zgymmzv759a3lxbqsrvhjg9nfc7y2cjziwrivcq3b")))

(define-public crate-esri_ascii_grid-0.1.1 (c (n "esri_ascii_grid") (v "0.1.1") (h "0swh245nvkh54k69br0bm3b99byz13lgx1bpdhjfv5bkcqrmr9hi")))

(define-public crate-esri_ascii_grid-0.1.2 (c (n "esri_ascii_grid") (v "0.1.2") (h "05ldkf6d54dh8cgw7fqsdfmv7v72j1i1yy2lx0fq00mkfr699d2f")))

(define-public crate-esri_ascii_grid-0.2.0 (c (n "esri_ascii_grid") (v "0.2.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1pwn0ck24wvdsf7h5x0pkg11rzds2ppr5a0hlasmiqr5z65fq0ms")))

(define-public crate-esri_ascii_grid-0.3.0 (c (n "esri_ascii_grid") (v "0.3.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1xb5451p7iijvvlxlg4baz94ghwkbsx8ac7n8n3zqiia26bhrjn8")))

(define-public crate-esri_ascii_grid-0.3.1 (c (n "esri_ascii_grid") (v "0.3.1") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0cgsngqn3iirp8hksjmhk117fc12n1dmi5030f15qsvcscarg0q9")))

(define-public crate-esri_ascii_grid-0.4.0 (c (n "esri_ascii_grid") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "14pf71imb6yyr4jqbf4n75zl1k3pnsisfxnkklfmbcxfq14y60aw")))

(define-public crate-esri_ascii_grid-0.4.1 (c (n "esri_ascii_grid") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0h13cwf4n0m4i1sk6x4m69mgn1nmgnqayxplaj3fbba35ld4xqmr")))

(define-public crate-esri_ascii_grid-0.4.2 (c (n "esri_ascii_grid") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1ppbnjadnsgxiwyibcpfimjkdq72srvi5cj08s1if9ww9x1ybs1l")))

(define-public crate-esri_ascii_grid-0.4.3 (c (n "esri_ascii_grid") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.2.19") (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1wgd8b4l5x43z7ki39kqavpg472yw7br10jl3a7agvfx0b86b13b")))

