(define-module (crates-io es vm esvm-rlp) #:use-module (crates-io))

(define-public crate-esvm-rlp-0.0.1 (c (n "esvm-rlp") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1v24apkyz485izmjv8sw9hqj3kc7s392rqdlv2ynayjya76br15a")))

