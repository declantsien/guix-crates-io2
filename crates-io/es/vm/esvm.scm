(define-module (crates-io es vm esvm) #:use-module (crates-io))

(define-public crate-esvm-0.0.1 (c (n "esvm") (v "0.0.1") (d (list (d (n "digest") (r "^0.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "esvm-bigint") (r "^0.0.1") (d #t) (k 0)) (d (n "esvm-rlp") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "ripemd160") (r "^0.5.2") (d #t) (k 0)) (d (n "secp256k1") (r "^0.6.2") (d #t) (k 0)) (d (n "sha2") (r "^0.5.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.2") (d #t) (k 0)))) (h "064b4nq0czh4mfjg2m72xn2av7xadc2mks4y43wrirqf9ms1fdrk")))

