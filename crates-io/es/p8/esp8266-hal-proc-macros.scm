(define-module (crates-io es p8 esp8266-hal-proc-macros) #:use-module (crates-io))

(define-public crate-esp8266-hal-proc-macros-0.1.0 (c (n "esp8266-hal-proc-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0wv8d08djrwah50pxrnxkjmcglmdd5nvzz2xhkzqb6zskbpfwybr")))

