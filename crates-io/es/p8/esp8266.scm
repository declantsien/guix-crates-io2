(define-module (crates-io es p8 esp8266) #:use-module (crates-io))

(define-public crate-esp8266-0.0.1 (c (n "esp8266") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0mia07n9xz8fkp7bqwxrvji36cm3cw2zciavn08qmmjwjxizc2g0")))

(define-public crate-esp8266-0.0.2 (c (n "esp8266") (v "0.0.2") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1vk5ww9kxhg634hpmrdabcyxq7rqh9pvqr7zx1i6qmssla6xnqf4")))

(define-public crate-esp8266-0.0.3 (c (n "esp8266") (v "0.0.3") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1grvimkikac8x5rbp4k4b7mlq9m48a6rhf1rjhgsszl6dgpps638")))

(define-public crate-esp8266-0.0.4 (c (n "esp8266") (v "0.0.4") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0vfd5l5kjffac6cw34bk8rm4k8j8jxv0yyk6ih6iddq0sdbvpf1z")))

(define-public crate-esp8266-0.0.5 (c (n "esp8266") (v "0.0.5") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1wi936snyx4h30hy1c7jjj32l0qi5zv79nmj9b3bvram2mj1ppsy")))

(define-public crate-esp8266-0.0.6 (c (n "esp8266") (v "0.0.6") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0x02bdqcqx1gypvamn2yqiz82q2q26z591vqw7g4h00n0grqfd4f")))

(define-public crate-esp8266-0.1.0 (c (n "esp8266") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0qk29xafb5v9z2rl7b5bs7fp6b442dfbixb8lvnhq4j1lnc9xlfi")))

(define-public crate-esp8266-0.1.1 (c (n "esp8266") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "13zfxn1hz9g09nxnz0glwmdff95g6p9cc1hjl5hkg9vn7qk89wzf")))

(define-public crate-esp8266-0.1.2 (c (n "esp8266") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0b1fln8mqr22k7269z4rn27bnkq0vyk5s4z6dsqsdksbjy3bi32x")))

(define-public crate-esp8266-0.1.3 (c (n "esp8266") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1kivr3ghmfpsg2gd3sr2w24x27b8ilcnf5k6dygpn0b7p9bi468b")))

(define-public crate-esp8266-0.1.4 (c (n "esp8266") (v "0.1.4") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0c7s1wkimp10z0zkzl3y0ypfw8h3pw4lhi84g6lkj9v9dzfrcr0q")))

(define-public crate-esp8266-0.1.5 (c (n "esp8266") (v "0.1.5") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.3.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "03lpv3plh6am9ks5siwskw4m08f1hi73kwv9gbpgcdbdfd6qq47d") (f (quote (("rt" "xtensa-lx-rt") ("default"))))))

(define-public crate-esp8266-0.2.0 (c (n "esp8266") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.3.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1rd8ffxs0kvj4vglvkhk42a7amc1hyk02vlrz126raazqr4ydzya") (f (quote (("rt" "xtensa-lx-rt") ("default"))))))

(define-public crate-esp8266-0.3.0 (c (n "esp8266") (v "0.3.0") (d (list (d (n "bare-metal") (r "^1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.4") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1xj6bj3fsja4x3jwv2jzxih8yg8bz7mbh97sriyxnr2p0pp4zbqh") (f (quote (("rt" "xtensa-lx-rt") ("default"))))))

(define-public crate-esp8266-0.4.0 (c (n "esp8266") (v "0.4.0") (d (list (d (n "bare-metal") (r "^1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.4") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1m2xmk1vpd2g74b9hmg6icg53bgz7qxyh3f9b5cs4d6flh3b0hhf") (f (quote (("rt" "xtensa-lx-rt") ("default"))))))

(define-public crate-esp8266-0.5.0 (c (n "esp8266") (v "0.5.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.6.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1bc43sgwn49pmlak9xvcm3r1b0npwmdrp9n9m3q79a5lxclnz4yv") (f (quote (("rt" "xtensa-lx-rt/esp8266") ("default" "xtensa-lx/esp8266"))))))

(define-public crate-esp8266-0.6.0 (c (n "esp8266") (v "0.6.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "1ywmi122haf1c8842lqn3bfmyx4g9gcx0v4riazqc1lfj1iifrbi") (f (quote (("rt" "xtensa-lx-rt/esp8266") ("default" "xtensa-lx/esp8266"))))))

