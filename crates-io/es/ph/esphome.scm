(define-module (crates-io es ph esphome) #:use-module (crates-io))

(define-public crate-esphome-0.1.0 (c (n "esphome") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.3") (d #t) (k 1)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "085jadpmijf0rxklzqs3b3rv29aqnqagds6bsqs9mb6ky9mz2qfk")))

