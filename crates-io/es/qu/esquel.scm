(define-module (crates-io es qu esquel) #:use-module (crates-io))

(define-public crate-esquel-0.1.0 (c (n "esquel") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlparser") (r "^0.15.0") (d #t) (k 0)))) (h "00lf800is17g5xszffn0blha93k4hkhkircqxzc418326pc167ib")))

(define-public crate-esquel-0.1.1 (c (n "esquel") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlparser") (r "^0.15.0") (d #t) (k 0)))) (h "0x0spjyyq9yk5559giiqflfyn7yhdm4f118x88drbpji7xacg2za")))

(define-public crate-esquel-0.1.2 (c (n "esquel") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "metaquery") (r "^0.4.0") (d #t) (k 0)))) (h "1a39qpnjc4z79x8s5csrddwjxlz0zgn26bj3ypa4f5i4gkbaiagn")))

