(define-module (crates-io es p_ esp_idf) #:use-module (crates-io))

(define-public crate-esp_idf-0.1.0 (c (n "esp_idf") (v "0.1.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "esp-idf-sys") (r "^0.1") (d #t) (k 0) (p "esp_idf_dev_sys")) (d (n "failure") (r "^0.1.7") (f (quote ("derive"))) (k 0)))) (h "1gs25sr9jaxqn0m74x44sd6m3f8azlwpybbmr80ab4z55fbn6vq5")))

(define-public crate-esp_idf-0.1.1 (c (n "esp_idf") (v "0.1.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "esp-idf-sys") (r "^0.1") (d #t) (k 0) (p "esp_idf_dev_sys")) (d (n "failure") (r "^0.1.7") (f (quote ("derive"))) (k 0)))) (h "0187acb3b9qdbshwmlznsjdf3rsm82x5wq9jjc8f4g6z3sbv58ys")))

