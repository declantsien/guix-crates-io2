(define-module (crates-io es p_ esp_idf_build) #:use-module (crates-io))

(define-public crate-esp_idf_build-0.1.0 (c (n "esp_idf_build") (v "0.1.0") (h "0dbnxlzzir3n9c9939472qimcbsdchhd54zxfjlpjg10nchx6ngd")))

(define-public crate-esp_idf_build-0.1.1 (c (n "esp_idf_build") (v "0.1.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "1bdfy1i67vsab5yb5m0xjshjmcfxbdlk48aspkh5q189cv39w4y6")))

(define-public crate-esp_idf_build-0.1.2 (c (n "esp_idf_build") (v "0.1.2") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "05mrw2yzwpf2wyqxqcn168g8c0spzq556zy0a4nvhnxknwl9rh36") (f (quote (("default" "build_idf") ("build_make") ("build_idf"))))))

(define-public crate-esp_idf_build-0.1.3 (c (n "esp_idf_build") (v "0.1.3") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "1lixs8h111m6cl3h6adgcww5l5l9zq8c7k4hqkx6v4m7llc6sqkp") (f (quote (("default" "build_idf") ("build_make") ("build_idf"))))))

