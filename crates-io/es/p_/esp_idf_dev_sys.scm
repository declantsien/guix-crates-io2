(define-module (crates-io es p_ esp_idf_dev_sys) #:use-module (crates-io))

(define-public crate-esp_idf_dev_sys-0.1.3 (c (n "esp_idf_dev_sys") (v "0.1.3") (h "027kq5n19551dzrd2v6glbfrbxqiazdyg6aipzsb1dmdc1qa3k55")))

(define-public crate-esp_idf_dev_sys-0.1.4 (c (n "esp_idf_dev_sys") (v "0.1.4") (h "05yc0ska2vb4w70ym7q84mdic1ahrywhqbjp88idf4f807qmnlg9")))

