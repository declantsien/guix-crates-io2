(define-module (crates-io es p_ esp_idf_logger) #:use-module (crates-io))

(define-public crate-esp_idf_logger-0.1.0 (c (n "esp_idf_logger") (v "0.1.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "esp-idf-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1sh61034lla7rs2qw3rwvnv7igyg8i3dixk17vs8cgwg712hp7sv") (f (quote (("ets_printf") ("default" "ets_printf"))))))

(define-public crate-esp_idf_logger-0.1.1 (c (n "esp_idf_logger") (v "0.1.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "esp-idf-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05zf03dvra2w9iqlf3xi6wq6gjb5479w1s4mzn618xf0917qshgj") (f (quote (("ets_printf") ("default" "ets_printf"))))))

