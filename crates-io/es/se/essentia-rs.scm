(define-module (crates-io es se essentia-rs) #:use-module (crates-io))

(define-public crate-essentia-rs-0.1.0 (c (n "essentia-rs") (v "0.1.0") (h "1nmc1afh99104fqhbs04b9ii57lp5xh1bnyy187ckj3al64c7x75") (y #t)))

(define-public crate-essentia-rs-0.1.1 (c (n "essentia-rs") (v "0.1.1") (h "1lzlnglhj4m5z5bpxyrw041cd5bnmd3jlia126ldj3schkqzrxk5") (y #t)))

(define-public crate-essentia-rs-0.1.2 (c (n "essentia-rs") (v "0.1.2") (h "06wzh56y2k6irzyad3g5g5cyj2jld3rrf93zq8h1gl9qi79vfczv")))

