(define-module (crates-io es se essence_macros) #:use-module (crates-io))

(define-public crate-essence_macros-0.1.0 (c (n "essence_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1v2lz8v85fxs8plijy3jdn8wjp1n6c1856grl38hwrmkp9vjqcln")))

(define-public crate-essence_macros-0.2.0 (c (n "essence_macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0qs34kks0qv3lw21q6viyq6f596l3amn3bljzml4gmzvwhk9jdlm")))

(define-public crate-essence_macros-0.2.1 (c (n "essence_macros") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0jsd2r2d2gf5pf4a59cxp1nk0bn5z51ir2pdjq80793l9jzm8d5c")))

