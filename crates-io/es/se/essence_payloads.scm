(define-module (crates-io es se essence_payloads) #:use-module (crates-io))

(define-public crate-essence_payloads-0.1.0 (c (n "essence_payloads") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "02ifkcgmrjxxszynngdljg2k381lxyxs2qxws5xy33hgn892g1mm")))

