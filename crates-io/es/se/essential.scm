(define-module (crates-io es se essential) #:use-module (crates-io))

(define-public crate-essential-0.1.0 (c (n "essential") (v "0.1.0") (h "0kb879d0i0ls4w1x0a58wmi6apg98bpq920r2cfjr3wmmbp5cqkc")))

(define-public crate-essential-0.1.1 (c (n "essential") (v "0.1.1") (h "059pa4fxsilija4l08z1a4jixsqrbwzi3kj70md0m2qmdaf0jfdq")))

