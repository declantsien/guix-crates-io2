(define-module (crates-io es p3 esp32p4) #:use-module (crates-io))

(define-public crate-esp32p4-0.1.0 (c (n "esp32p4") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0r8rrzlbq3a4fyj8jsf1v20078wb87lpkjf3qgihas1yjz3hng60") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32p4-0.2.0 (c (n "esp32p4") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "19nswyb9m6yjk08mxqs54d7n5wv047gigkizk5f99769n7h9sggk") (f (quote (("rt") ("impl-register-debug") ("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

