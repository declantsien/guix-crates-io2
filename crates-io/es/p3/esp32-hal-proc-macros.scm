(define-module (crates-io es p3 esp32-hal-proc-macros) #:use-module (crates-io))

(define-public crate-esp32-hal-proc-macros-0.1.0 (c (n "esp32-hal-proc-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1i4275mkq5mvkw28h7yi1jlz73inr9nyyc9g3mn7vmj13bxmpkas") (f (quote (("external_ram"))))))

(define-public crate-esp32-hal-proc-macros-0.2.0 (c (n "esp32-hal-proc-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1hpxbw55nml3vvj54m71bsbm0a7x26kqgs2dlhl5q0b2y334gq6v") (f (quote (("external_ram"))))))

