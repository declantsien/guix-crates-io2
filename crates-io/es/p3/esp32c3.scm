(define-module (crates-io es p3 esp32c3) #:use-module (crates-io))

(define-public crate-esp32c3-0.1.0 (c (n "esp32c3") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1jyqfb0v3h8yw4yrl77axvz18plln5asrgwfkyy2rvlhqsf3cgha") (f (quote (("rt" "riscv-rt") ("default")))) (y #t)))

(define-public crate-esp32c3-0.1.1 (c (n "esp32c3") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "029h5rf9xlbr5ix6k36am1cmdz424l39ph75hy58nngkkav3hdyr") (f (quote (("rt" "riscv-rt") ("default")))) (y #t)))

(define-public crate-esp32c3-0.1.2 (c (n "esp32c3") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1r5akva1vy3rhncw31s4qll36n5034mdlsf5wdjdkh9ssq180zck") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.2.0 (c (n "esp32c3") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1sh1fzwcvhpbnd79y8cikx7aiblc33azpvb9d0qbq031027x4441") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.3.0 (c (n "esp32c3") (v "0.3.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0k18xv7arfdbcfy3bibnlr4jrcqhb5wbxj9g7f1ffin6k6ikcaiq") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.4.0 (c (n "esp32c3") (v "0.4.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "01vz135ag6f9p7ib4w0ain2jjxswrgip098ds0p6b6bvz6ww3jlx") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.5.0 (c (n "esp32c3") (v "0.5.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ahiivdk4i69x1dxlh8vcfkiv3zkvbr5vv4269z5d9i3qssb9sbr") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.5.1 (c (n "esp32c3") (v "0.5.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00hs5c0p44caf79jnrgiz9lmcqd7nbxdgwrs4h9wgkwhgj4n23nc") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.6.0 (c (n "esp32c3") (v "0.6.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ynll5h7rv8pi6mfwd825xy946h5zr2ryrm3vnq0pdaydanwi7l7") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.7.0 (c (n "esp32c3") (v "0.7.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0h5xhjf0pp42k5l64pkxnv1q89d0pljyjdk1w5rchkvh1yixw8n6") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.8.0 (c (n "esp32c3") (v "0.8.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.9.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0z7b8sxq346gym6xm0iz6a85yvm5n4jvpxa3s75784l4d3zrxk5x") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.8.1 (c (n "esp32c3") (v "0.8.1") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1a7a42ppaq9iqgvbynl7vh5arn84xvjqc8nhbd63ixz20g06qyv7") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.9.0 (c (n "esp32c3") (v "0.9.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1m6s4rmxcsa2l2fzagni3i0ykg2dd9w51rajjg6l19k5kybkvash") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.9.1 (c (n "esp32c3") (v "0.9.1") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "02q2lnklax7dhpdkydc33l97c3dwn44pmssh2f8va6lxkfpncwc9") (f (quote (("rt" "riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.10.0 (c (n "esp32c3") (v "0.10.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "esp-riscv-rt") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1w8s1sznlp24c3740fw0ipi14zkpnx115mxyzxi5zlk63ysxmiq1") (f (quote (("rt" "esp-riscv-rt") ("default"))))))

(define-public crate-esp32c3-0.11.0 (c (n "esp32c3") (v "0.11.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1bkqfswwjw8fbb9c2xccjjfcyirv6n4544cfzwkqk2bvxg5lq7j7") (f (quote (("rt") ("default"))))))

(define-public crate-esp32c3-0.12.0 (c (n "esp32c3") (v "0.12.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1bi5p1njpjbyn1bnhm0mkhfcza5y8p23ijvi0drfb26m4y3qrf64") (f (quote (("rt") ("default"))))))

(define-public crate-esp32c3-0.13.0 (c (n "esp32c3") (v "0.13.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1mczw964mpbvpvnxk25b091bgbnn53m3836v5529dflznq4zfyp2") (f (quote (("rt") ("default"))))))

(define-public crate-esp32c3-0.14.0 (c (n "esp32c3") (v "0.14.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kb91hvk56nwywgx16m5ayk034snaqqfxg61amrcsx17b9gxqvqv") (f (quote (("rt") ("default")))) (r "1.65")))

(define-public crate-esp32c3-0.15.0 (c (n "esp32c3") (v "0.15.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0pxdmjx0hd7midyg2kpd9v6in8lb9wq1rmrg60mlrcr48nd7wm5f") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.65")))

(define-public crate-esp32c3-0.16.0 (c (n "esp32c3") (v "0.16.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1018kfgdb1mk6nkgg8vprq18qi4kbynsbba0dv4x4wx6jaypadqy") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.65")))

(define-public crate-esp32c3-0.17.0 (c (n "esp32c3") (v "0.17.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1gskxghskwi0xxj0fps8js2lr06v6flba0cdp8f80z3ywl64vzb9") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.65")))

(define-public crate-esp32c3-0.18.0 (c (n "esp32c3") (v "0.18.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10jq5yjyxhz209h5b2vq5jwjcsghbfhfnzlrlf77vbkl11ncrrc9") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32c3-0.19.0 (c (n "esp32c3") (v "0.19.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ndyx9vqpzxsyh4lrnpzcniicrmi8lcn1ypk2c6srm840025ylx3") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32c3-0.20.0 (c (n "esp32c3") (v "0.20.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0m8sbwz3q48h3kc97z1r0155d7ppfay8in8b24b159dhlgn7b21r") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32c3-0.21.0 (c (n "esp32c3") (v "0.21.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1i8p86hskbqwgr5mqz47g98vfhkn9qzyrpqchig6pwg4q48ffzja") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32c3-0.22.0 (c (n "esp32c3") (v "0.22.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0007xrj5ywr6wzj1kj55n5nyy2pq3pjbj4mjkpvmwrvxgv1dh8jc") (f (quote (("rt") ("impl-register-debug") ("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

(define-public crate-esp32c3-0.23.0 (c (n "esp32c3") (v "0.23.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bb9ji9zrcz3y554dgq8ki2xphzsj6a9d2zplj5kx1xmdjpzy5zi") (f (quote (("rt") ("impl-register-debug") ("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

