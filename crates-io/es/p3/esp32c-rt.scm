(define-module (crates-io es p3 esp32c-rt) #:use-module (crates-io))

(define-public crate-esp32c-rt-0.1.0 (c (n "esp32c-rt") (v "0.1.0") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "0wz9swhxp6x4q8r726rd9shx3vwnjl5rw3jz40l1wx3vik6w8ys8") (f (quote (("inline-asm" "riscv/inline-asm") ("esp32c3") ("direct-boot") ("default" "direct-boot" "esp32c3"))))))

(define-public crate-esp32c-rt-0.1.1 (c (n "esp32c-rt") (v "0.1.1") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.7") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "riscv-target") (r "^0.1.2") (d #t) (k 1)))) (h "1bfril5nkrgzd08sppm52c9lxv89y93p5a7b2q8hd8kalzl79qzk") (f (quote (("inline-asm" "riscv/inline-asm") ("esp32c3") ("direct-boot") ("default" "direct-boot" "esp32c3"))))))

