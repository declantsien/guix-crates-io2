(define-module (crates-io es p3 esp32c6) #:use-module (crates-io))

(define-public crate-esp32c6-0.1.0 (c (n "esp32c6") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0brfvsms6hjzpiqmvbd80cgf0f5q52zvrj03rv35v2nrnn3xvk6s") (f (quote (("rt") ("default"))))))

(define-public crate-esp32c6-0.2.0 (c (n "esp32c6") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1110wmdr17dlczx40vhlymczfhdfnh9g6xd03qm8gdlp9zqqlp02") (f (quote (("rt") ("default"))))))

(define-public crate-esp32c6-0.3.0 (c (n "esp32c6") (v "0.3.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1nv1vk2vk503rmz5s6gggskr9wmrsk4dy1id163hmvfdndsrx0si") (f (quote (("rt") ("default"))))))

(define-public crate-esp32c6-0.4.0 (c (n "esp32c6") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1r4ra421gmmkba1w9ykq7x2xypvhaqwf9m16dzw0n6c09bq8rpza") (f (quote (("rt") ("default")))) (r "1.65")))

(define-public crate-esp32c6-0.5.0 (c (n "esp32c6") (v "0.5.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "090x7kz9z524ql4i8x14bf66iikbc4n1si6ckpkmx142nn33zmv9") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.65")))

(define-public crate-esp32c6-0.6.0 (c (n "esp32c6") (v "0.6.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kwbvfhzq3lp1fl4b94d6kp1nisx29l1j9qanv66cfgcqfyffzf1") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.65")))

(define-public crate-esp32c6-0.7.0 (c (n "esp32c6") (v "0.7.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0da0w4yvz6wv5c57fddm3jrq6ik93ias3wcl1zvckvmmzy0dj65j") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.65")))

(define-public crate-esp32c6-0.8.0 (c (n "esp32c6") (v "0.8.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "01x500nw8njgl73wj44bshqh4s3nnap91ydpi4bzhsciirvnqiay") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32c6-0.9.0 (c (n "esp32c6") (v "0.9.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0iprd0655xwrb66jn54jscja7rnj40j64jb7hiwyy2n45kfz0whl") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32c6-0.10.0 (c (n "esp32c6") (v "0.10.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "14n8nyic8ysd0axzr18kdhp1qap7wpw376az695g2fyhd5220y3j") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32c6-0.11.0 (c (n "esp32c6") (v "0.11.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1lh99vn5p6hpf18hklhkn0pws3mdpmnzrn7hf8q1557si1qd90wn") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32c6-0.12.0 (c (n "esp32c6") (v "0.12.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1qz3mp99w9wpsfg799y8p7jqqpmnjcx1c0srgddng9x3br17a0pz") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32c6-0.13.0 (c (n "esp32c6") (v "0.13.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "04qxv14z0glhnx823ppx8s1yqd31cjdp7cl9yw2dkwfsvj1zvdxc") (f (quote (("rt") ("impl-register-debug") ("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

(define-public crate-esp32c6-0.14.0 (c (n "esp32c6") (v "0.14.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0iijiph4lpq7wadh7mfwkzxi8lbhivfak34pxwpys963m7n8422f") (f (quote (("rt") ("impl-register-debug") ("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

