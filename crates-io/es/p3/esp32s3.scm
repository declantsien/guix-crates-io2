(define-module (crates-io es p3 esp32s3) #:use-module (crates-io))

(define-public crate-esp32s3-0.1.0 (c (n "esp32s3") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.6.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0njxr9ygjywblgcm9w25spwh29fsgi8lrf9il333vsy8a2q7n48g") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32")))) (r "1.58")))

(define-public crate-esp32s3-0.2.0 (c (n "esp32s3") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "1abnnjy306m6lqkhl3ll8ldsrnj09abjlwk6jnyjyhhw73r0a5pn") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.58")))

(define-public crate-esp32s3-0.3.0 (c (n "esp32s3") (v "0.3.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1sdqjdinb60a31c4vh93jc2gddijvzmcxcpmggybc9g9xzxqi5n3") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.58")))

(define-public crate-esp32s3-0.4.0 (c (n "esp32s3") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1lk2pl5022rn2zc2s6zcys6ghqi44rrc2sw9ra5rbjj9jma4wcmm") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.5.0 (c (n "esp32s3") (v "0.5.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "16gh39qpn668gz4n21hyjnnij0j7rj1frrzbd04l22ix8psm96j6") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.6.0 (c (n "esp32s3") (v "0.6.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "12bx9xv04f2w23hs0vz856smc1krnpgzznsxl21sl5f23gbk80r0") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.7.0 (c (n "esp32s3") (v "0.7.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1rklpf7si3y3kxgx8aih9blpb6ni70n9qdsikqc4di7061wyxlcl") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.8.0 (c (n "esp32s3") (v "0.8.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "149wm45pks0m5fqph5xi1y4c2jaz7dv15vjd313khc3lka06yf3v") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.9.0 (c (n "esp32s3") (v "0.9.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1w17gicqcfiz95hpxl4ssyfdrwfx83mp6xyq0mc14brfwz8khpx4") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.10.0 (c (n "esp32s3") (v "0.10.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "053j4zb727a7ymhfnzagv6qw7imshqkvk41js31v6yx7cpn826bl") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.11.0 (c (n "esp32s3") (v "0.11.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "1v78r9027ix9rqwnn4fmnpdv3r0q6rwpdjn0x90na8i5xx0azx3a") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.12.0 (c (n "esp32s3") (v "0.12.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "04i5s88cxvn2da1ryabk2j7rbgybd97bdimkh2sbhzkyj06f8yc1") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.13.0 (c (n "esp32s3") (v "0.13.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "1dpgv32132p2bjx5c5b63w6hm7rra4q3mw78j4br0l1h71y20jbk") (f (quote (("rt" "xtensa-lx-rt/esp32s3") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.14.0 (c (n "esp32s3") (v "0.14.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)))) (h "00z1cvq0na2m1r0hkf3zh966c21z7s0y2vfccrn6m0mk1p3ibgrp") (f (quote (("rt") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.15.0 (c (n "esp32s3") (v "0.15.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0w219zclp4pacafwsqaympss9swvwv3xcw0gw9j6b10x30lp5yv3") (f (quote (("rt") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.16.0 (c (n "esp32s3") (v "0.16.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0b53sjpvws9mkg4h8ikrhqn8swh15npk895rqgp1gjs9dj72i8bm") (f (quote (("rt") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.17.0 (c (n "esp32s3") (v "0.17.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0zq5136456z086j95aspy3y57rbcm7bfd079q702f53iks7sxjb7") (f (quote (("rt") ("default" "xtensa-lx/esp32s3")))) (r "1.60")))

(define-public crate-esp32s3-0.18.0 (c (n "esp32s3") (v "0.18.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0yjggf46gxh7h2df8zh1jvz9b9aj5y04543mpsp6amv6845n1dqx") (f (quote (("rt") ("default" "xtensa-lx/esp32s3")))) (r "1.65")))

(define-public crate-esp32s3-0.18.1 (c (n "esp32s3") (v "0.18.1") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "1va47c5fxif832v1qb501yn5v81ixqv9maf6lpwz7za4157nqkhf") (f (quote (("rt") ("default" "xtensa-lx/esp32s3")))) (r "1.65")))

(define-public crate-esp32s3-0.19.0 (c (n "esp32s3") (v "0.19.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0p7cgwvm67pq50cda2pxxd66zln1bxhz9sn7cnbg7lpz0qinf1bz") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32s3")))) (r "1.65")))

(define-public crate-esp32s3-0.20.0 (c (n "esp32s3") (v "0.20.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "1376c9sxbjaxw07bdx8jy6bj9wsspra0pf2ss361ipr3d7gq5r4k") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32s3")))) (r "1.65")))

(define-public crate-esp32s3-0.21.0 (c (n "esp32s3") (v "0.21.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "049s79hqr9fm9h65g8hx1dxfas9bk7dc62backi7n3n0i95gv4b9") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32s3")))) (r "1.65")))

(define-public crate-esp32s3-0.22.0 (c (n "esp32s3") (v "0.22.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0sgnq22zak5xzc9gc6ychz9snhckp12g349pvlwxj5v4ihky8qyr") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32s3")))) (r "1.67")))

(define-public crate-esp32s3-0.23.0 (c (n "esp32s3") (v "0.23.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "04cpgckaa2bgz92nw71xifhj3pwi10xclgha5x4bi7jv8jvgw9sj") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32s3")))) (r "1.67")))

(define-public crate-esp32s3-0.24.0 (c (n "esp32s3") (v "0.24.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.9.0") (d #t) (k 0)))) (h "19i39yz9f1ns42swmfg7pb1mx13q8apchqw1r7baafm3c1jj2kh4") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/spin")))) (r "1.67")))

(define-public crate-esp32s3-0.25.0 (c (n "esp32s3") (v "0.25.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.9.0") (d #t) (k 0)))) (h "1fxb6szrn4riarcb5fmyqf9rjjd645gair8xz2rzv14rijnxbn2g") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/spin")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

(define-public crate-esp32s3-0.26.0 (c (n "esp32s3") (v "0.26.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.9.0") (d #t) (k 0)))) (h "1kwki267vgz83f3sbz5fnysamhwr82prmflyda5bwff28ys7pb2k") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/spin")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

