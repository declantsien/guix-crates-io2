(define-module (crates-io es p3 esp32h2) #:use-module (crates-io))

(define-public crate-esp32h2-0.1.0 (c (n "esp32h2") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ckahrdjq8qyzzxdby4sbrkg6hfwb52r96h28wlmm552kxldvk7y") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.65")))

(define-public crate-esp32h2-0.2.0 (c (n "esp32h2") (v "0.2.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "02jlxfyh495610mp1gznzsmhgmsz35c452f0zrrx0iwv1i7f29jx") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.65")))

(define-public crate-esp32h2-0.3.0 (c (n "esp32h2") (v "0.3.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ld43m45rvgg7flzzn4z9f09hdvynpxpcglvysr2flahmn2x2c21") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.65")))

(define-public crate-esp32h2-0.4.0 (c (n "esp32h2") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1c8imh1rq4xn1ndglz53k4d9y6rhapnf641ryxlq0rspkg288l8f") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32h2-0.5.0 (c (n "esp32h2") (v "0.5.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1z0y3p78znh8s2xbi55aii032d3ah8jpb3wgdwirw56di2y9nh1n") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32h2-0.6.0 (c (n "esp32h2") (v "0.6.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "07dvvs176x9rdcxr38spyd7kj02cl601lkrwspfn6iw7m8pblby8") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32h2-0.7.0 (c (n "esp32h2") (v "0.7.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0l9ckc2scir8w0qlj011n5yf18g5pqdmaqji6n9zfg3rwi7p9jzq") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32h2-0.8.0 (c (n "esp32h2") (v "0.8.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0sjdr95rpdwsxc1vn1952wy2w7gyv34grabzk7days1y1pkch1p6") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32h2-0.9.0 (c (n "esp32h2") (v "0.9.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "115wmrgcwp7ikm9m3c87l6kl06y588lpnmllhn6i6p9q2p95xn8l") (f (quote (("rt") ("impl-register-debug") ("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

(define-public crate-esp32h2-0.10.0 (c (n "esp32h2") (v "0.10.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1c9zm6i25dyzgnlnva8kw74vbaq6ml4p31pj9pxxpm52prg6s91b") (f (quote (("rt") ("impl-register-debug") ("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

