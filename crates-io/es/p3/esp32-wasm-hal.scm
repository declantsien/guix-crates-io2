(define-module (crates-io es p3 esp32-wasm-hal) #:use-module (crates-io))

(define-public crate-esp32-wasm-hal-0.1.0 (c (n "esp32-wasm-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "12i66f99lzdgngcdabvyd09267fhy9i9drbn9g8vxvzr3xp0n4gw") (f (quote (("logger" "log"))))))

