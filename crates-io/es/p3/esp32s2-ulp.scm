(define-module (crates-io es p3 esp32s2-ulp) #:use-module (crates-io))

(define-public crate-esp32s2-ulp-0.1.0 (c (n "esp32s2-ulp") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kbwhb17a387fpbkhnaxnwihf2z4i6jw099p8cbp9jcx2c1c905q") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

