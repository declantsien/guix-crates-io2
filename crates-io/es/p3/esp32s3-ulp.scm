(define-module (crates-io es p3 esp32s3-ulp) #:use-module (crates-io))

(define-public crate-esp32s3-ulp-0.1.0 (c (n "esp32s3-ulp") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0nmz62df38m2nss9vh71cg0xrp3swncig79g7k8ppflqwikw8f3v") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

