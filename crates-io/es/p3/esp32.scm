(define-module (crates-io es p3 esp32) #:use-module (crates-io))

(define-public crate-esp32-0.1.0 (c (n "esp32") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "08gq7p5hqdqxp9ckzvbxn2lvh0h6pn8yh8nrgz1l63bsnq0y40cm")))

(define-public crate-esp32-0.2.0 (c (n "esp32") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "164y6yns1wv9wshqk6sj16amkybb4dpn5mc9qflk1ndixwd41v24") (y #t)))

(define-public crate-esp32-0.2.1 (c (n "esp32") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "15c65msivbzc8k02d5ww2jrrq1lczarn7z5yn7wc0kqh1vq914id")))

(define-public crate-esp32-0.2.2 (c (n "esp32") (v "0.2.2") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1529wcg30whmkkid98npbl5g9b2xyg18lz5q4sd381cic7f2xdgz")))

(define-public crate-esp32-0.3.0 (c (n "esp32") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1h1bjcwz5gzhhdjxhqyj0qzkl08gsklarv07fvzm43pmxvbsgj0p")))

(define-public crate-esp32-0.4.0 (c (n "esp32") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx6-rt") (r "^0.1.0") (d #t) (k 0)))) (h "0i2fiq644yx3wmv0kmz6mlqw95dsbqi8a7amiipq9kavr7rwnppp") (f (quote (("rt") ("default" "rt"))))))

(define-public crate-esp32-0.5.0 (c (n "esp32") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx6") (r "^0.1.0") (d #t) (k 0)) (d (n "xtensa-lx6-rt") (r "^0.2.0") (d #t) (k 0)))) (h "0wnh51f61hxgggrdpl76c2zc6ildsx1v3p1baw9pddv4vd25mp5w") (f (quote (("rt") ("default" "rt"))))))

(define-public crate-esp32-0.6.0 (c (n "esp32") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx6") (r "^0.2.0") (d #t) (k 0)) (d (n "xtensa-lx6-rt") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0ylp0pabrc299mr7j4l4skcjkpzfbmv20fsdwszbf5g6hw2gfq2g") (f (quote (("rt" "xtensa-lx6-rt") ("default" "rt"))))))

(define-public crate-esp32-0.7.0 (c (n "esp32") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx6") (r "^0.2.0") (d #t) (k 0)) (d (n "xtensa-lx6-rt") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0aqk50ni478z0bj63yv6021msxr3h0jmg43lr7c3jcyajk4xd3rk") (f (quote (("rt" "xtensa-lx6-rt") ("default" "rt"))))))

(define-public crate-esp32-0.8.0 (c (n "esp32") (v "0.8.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx6") (r "^0.2.0") (d #t) (k 0)) (d (n "xtensa-lx6-rt") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0cb5yvlpfc8g4j9s2yili28a6ssjqlnv7yvdkimagf3zxy3i3qsa") (f (quote (("rt" "xtensa-lx6-rt") ("default" "rt"))))))

(define-public crate-esp32-0.9.0 (c (n "esp32") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.3.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0l8gm6yjl16hngijajy8p35fq644kak7wgrh06hliz9k6z5h427v") (f (quote (("rt" "xtensa-lx-rt") ("default")))) (y #t)))

(define-public crate-esp32-0.10.0 (c (n "esp32") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.3.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1ry635kkvka9wmqxfkfrrrmjqmn6rcc2bxhcwyn9qp6nj8z0r10g") (f (quote (("rt" "xtensa-lx-rt") ("default"))))))

(define-public crate-esp32-0.11.0 (c (n "esp32") (v "0.11.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.4.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1l8a4bx12g4ha7dg6hh9bfklrds3ffap5xlpavk6ap0m6170ygxp") (f (quote (("rt" "xtensa-lx-rt/lx6") ("default" "xtensa-lx/lx6"))))))

(define-public crate-esp32-0.12.0 (c (n "esp32") (v "0.12.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0bjn35finjgsirpmnh9njgg7q4sw1v4ms8qy448qyh76pxpf53pf") (f (quote (("rt" "xtensa-lx-rt/esp32") ("default" "xtensa-lx/esp32")))) (r "1.58")))

(define-public crate-esp32-0.13.0 (c (n "esp32") (v "0.13.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1jfgp3m4a8v85w408pg2qif1vlzc7v0n9q8sp3skr9bc7rqbw9id") (f (quote (("rt" "xtensa-lx-rt/esp32") ("default" "xtensa-lx/esp32")))) (r "1.58")))

(define-public crate-esp32-0.14.0 (c (n "esp32") (v "0.14.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "19i881v2qk2h5dqb0ai55b2vnmnjlwrr4wy7shws4qhmmkc6yb65") (f (quote (("rt" "xtensa-lx-rt/esp32") ("default" "xtensa-lx/esp32")))) (r "1.60")))

(define-public crate-esp32-0.15.0 (c (n "esp32") (v "0.15.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1fcl5aiqa60p6ji6wbnhni55kq8j2db6r8r99dplw30zjwbbmdwh") (f (quote (("rt" "xtensa-lx-rt/esp32") ("default" "xtensa-lx/esp32")))) (r "1.60")))

(define-public crate-esp32-0.16.0 (c (n "esp32") (v "0.16.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0na4mw5sazvg80pwkmb9wri2177a1q6qn3fyv1842pmyrggrfkki") (f (quote (("rt" "xtensa-lx-rt/esp32") ("default" "xtensa-lx/esp32")))) (r "1.60")))

(define-public crate-esp32-0.17.0 (c (n "esp32") (v "0.17.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "1vblf93nzg09brs1p2aj96hy5iblkc9az5nw450j4vsz2bm8ifhv") (f (quote (("rt" "xtensa-lx-rt/esp32") ("default" "xtensa-lx/esp32")))) (r "1.60")))

(define-public crate-esp32-0.18.0 (c (n "esp32") (v "0.18.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "1ayj7f3birb214yqx6pv743mdmj97xh7ckijxddrz1irz32l2sy7") (f (quote (("rt" "xtensa-lx-rt/esp32") ("default" "xtensa-lx/esp32")))) (r "1.60")))

(define-public crate-esp32-0.19.0 (c (n "esp32") (v "0.19.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "0xd8791vsl8yh23a5arrjckhc4vm89yw35rqcp68mb4gjgnba00a") (f (quote (("rt" "xtensa-lx-rt/esp32") ("default" "xtensa-lx/esp32")))) (r "1.60")))

(define-public crate-esp32-0.20.0 (c (n "esp32") (v "0.20.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)))) (h "0pd10knq06sjjivpswr5hxvfpm7pf67spcqpv65qj1a2c95ay0rv") (f (quote (("rt") ("default" "xtensa-lx/esp32")))) (r "1.60")))

(define-public crate-esp32-0.21.0 (c (n "esp32") (v "0.21.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "1rryq0i6yhw0bam9l3bbg4l3x0lci93knzz0v677k1sw05zfl4sm") (f (quote (("rt") ("default" "xtensa-lx/esp32")))) (r "1.60")))

(define-public crate-esp32-0.22.0 (c (n "esp32") (v "0.22.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "1hr77wzzsjndxryklm4lm68nqwb9m4kls3qljhhi6qj0kp2rj09k") (f (quote (("rt") ("default" "xtensa-lx/esp32")))) (r "1.60")))

(define-public crate-esp32-0.23.0 (c (n "esp32") (v "0.23.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0w98hda89qskhsqzrhsmz8hfjwimkgxdidy3iih03rip13q66y5q") (f (quote (("rt") ("default" "xtensa-lx/esp32")))) (r "1.60")))

(define-public crate-esp32-0.24.0 (c (n "esp32") (v "0.24.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0mv81gkxy5adlrhbdad8y7g10d2dqb6c24jxwscgvy5jzpb6q2jf") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32")))) (r "1.65")))

(define-public crate-esp32-0.25.0 (c (n "esp32") (v "0.25.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "01j9zq9d6raxg62yrgyhhi3g14m8y54684nzv0lq197dilgl3n2j") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32")))) (r "1.65")))

(define-public crate-esp32-0.26.0 (c (n "esp32") (v "0.26.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "19l05fvkqx6c67a9h31znxg025py7qchsh6d3xx6d80yjwjhv5jc") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32")))) (r "1.65")))

(define-public crate-esp32-0.27.0 (c (n "esp32") (v "0.27.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "1y8w1fsnpgn4akwadq1z3kz8w9vvj8zky6r9sy4345h1r04v62cx") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32")))) (r "1.67")))

(define-public crate-esp32-0.28.0 (c (n "esp32") (v "0.28.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0j139a9ai0azzzqngsmjlym98wk0s1fcf9yfy4md076hfi9wabyw") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32")))) (r "1.67")))

(define-public crate-esp32-0.29.0 (c (n "esp32") (v "0.29.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.9.0") (d #t) (k 0)))) (h "1lnq06jm29s86ky25z6dbfjqb5qs15j45nwhci9gilrp8l6c6fil") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/spin")))) (r "1.67")))

(define-public crate-esp32-0.30.0 (c (n "esp32") (v "0.30.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.9.0") (d #t) (k 0)))) (h "0jylvbkayqiivw583ax032073gjw1nfb74haq1ddk5pflx91m7l9") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/spin")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

(define-public crate-esp32-0.31.0 (c (n "esp32") (v "0.31.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.9.0") (d #t) (k 0)))) (h "13x7d9rdd5294j75ygkzc8rwam3dpprlqi0klvzhx7xj16rqg36s") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/spin")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

