(define-module (crates-io es p3 esp32c6-lp) #:use-module (crates-io))

(define-public crate-esp32c6-lp-0.1.0 (c (n "esp32c6-lp") (v "0.1.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0mq21a54s9ibrwf2v8ygkj0aqsjjraa2ixngglwcl5n4jlgpww2q") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

