(define-module (crates-io es p3 esp32s2) #:use-module (crates-io))

(define-public crate-esp32s2-0.1.0 (c (n "esp32s2") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.6.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1sbrfh6c1ahy6v7rmrfc23g7wsbrygav6cfniq2a4dgdi3z1dln1") (f (quote (("rt" "xtensa-lx-rt/esp32s2") ("default" "xtensa-lx/esp32")))) (r "1.58")))

(define-public crate-esp32s2-0.2.0 (c (n "esp32s2") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0d284w0gj9m6zfkhvskf7icv5qj6rqx95rykjkilfmq4hlpfal9v") (f (quote (("rt" "xtensa-lx-rt/esp32s2") ("default" "xtensa-lx/esp32s2")))) (r "1.58")))

(define-public crate-esp32s2-0.3.0 (c (n "esp32s2") (v "0.3.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "07jp18kg6ylv5kd0zrplm3qk18n31b5bqbfakg5gc2a9bi9bxfjz") (f (quote (("rt" "xtensa-lx-rt/esp32s2") ("default" "xtensa-lx/esp32s2")))) (r "1.58")))

(define-public crate-esp32s2-0.4.0 (c (n "esp32s2") (v "0.4.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0xp1sf3wh4qwddh5vsc2vnrdq2qixvz8r9dw3aj40apmipf2w2nr") (f (quote (("rt" "xtensa-lx-rt/esp32s2") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.5.0 (c (n "esp32s2") (v "0.5.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0bxnibws7irrc56d5xhmr78rn3ycr21kybknd5sz1rfind5mlxkc") (f (quote (("rt" "xtensa-lx-rt/esp32s2") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.6.0 (c (n "esp32s2") (v "0.6.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0n8k5vqbplff4rxx8jj0v1n7fs6bx5z684hbh8fjsmq8fkaim38m") (f (quote (("rt" "xtensa-lx-rt/esp32s2") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.7.0 (c (n "esp32s2") (v "0.7.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "11pjzv0awz3a99g6z8lf3k53vbf85fvk8zd08z9cm6vfc44j9w05") (f (quote (("rt" "xtensa-lx-rt/esp32s2") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.8.0 (c (n "esp32s2") (v "0.8.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "0yqhcyk1pwyb6dmgh1mh92dcf1b8rjaf5jil3m96z0i8igr7li91") (f (quote (("rt" "xtensa-lx-rt/esp32s2") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.9.0 (c (n "esp32s2") (v "0.9.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "0wzlq97zhx0mqysrmy7dikzw3il9dnrhqdc2hiil100qrs1gp6k7") (f (quote (("rt" "xtensa-lx-rt/esp32s2") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.10.0 (c (n "esp32s2") (v "0.10.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)) (d (n "xtensa-lx-rt") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "17gmnnj7lqjxl7z13hpw8pi015npmxf0r0bh91x34vcqv23rx1xy") (f (quote (("rt" "xtensa-lx-rt/esp32s2") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.11.0 (c (n "esp32s2") (v "0.11.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.7.0") (d #t) (k 0)))) (h "1bmw2qqmywxc1kf4hlvardi57g6yqm85cny40yy151b1vn0j1chs") (f (quote (("rt") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.12.0 (c (n "esp32s2") (v "0.12.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0q6bp04n20606ayzgzrvmxjp9jm7h1p9351h7cspfljgwqj1bb76") (f (quote (("rt") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.13.0 (c (n "esp32s2") (v "0.13.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "1if1x5y0wn5gsdnxvy8bdhbfdvy85rf8kkqsbw4yln3983vcs7zs") (f (quote (("rt") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.14.0 (c (n "esp32s2") (v "0.14.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "1p42pfscwi2xcgcqwaz4pjg2i562jxaasq7w0xbxwdlfpvnz0zip") (f (quote (("rt") ("default" "xtensa-lx/esp32s2")))) (r "1.60")))

(define-public crate-esp32s2-0.15.0 (c (n "esp32s2") (v "0.15.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0snj9pls0rjv6knh1ncya5hsw7d94gvciwzgmak9whdvcqwirbi2") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32s2")))) (r "1.65")))

(define-public crate-esp32s2-0.16.0 (c (n "esp32s2") (v "0.16.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "1a97vac5sbbzc7icpr9wx2m5007wh4vdrcp8s9l8hvl68f9w0p5z") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32s2")))) (r "1.65")))

(define-public crate-esp32s2-0.17.0 (c (n "esp32s2") (v "0.17.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "106m6ny49rw4kw5f3713v5d68k3qic1m8bd0r7wc87wri5s7m1l3") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32s2")))) (r "1.65")))

(define-public crate-esp32s2-0.18.0 (c (n "esp32s2") (v "0.18.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "1sh8hl14f1x9cckc4h6n5xhmrs9m4ajn35k016i61rysclpnrdh8") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32s2")))) (r "1.67")))

(define-public crate-esp32s2-0.19.0 (c (n "esp32s2") (v "0.19.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.8.0") (d #t) (k 0)))) (h "0ni9ycfadlgixh7hbbyv55r7fswpmlr585cdyg0yl84g25d1zcwj") (f (quote (("rt") ("impl-register-debug") ("default" "xtensa-lx/esp32s2")))) (r "1.67")))

(define-public crate-esp32s2-0.20.0 (c (n "esp32s2") (v "0.20.0") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.9.0") (d #t) (k 0)))) (h "1r3cj2nnddj98p6w7lxssmhpifx48snpdisz8lfbyzq9lklvig0g") (f (quote (("rt") ("impl-register-debug") ("default")))) (r "1.67")))

(define-public crate-esp32s2-0.21.0 (c (n "esp32s2") (v "0.21.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.9.0") (d #t) (k 0)))) (h "0qp2706c3qqqrwlfvxahk5c4i9n4mmchkinx7irgilzhcp5b52h1") (f (quote (("rt") ("impl-register-debug") ("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

(define-public crate-esp32s2-0.22.0 (c (n "esp32s2") (v "0.22.0") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "defmt") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)) (d (n "xtensa-lx") (r "^0.9.0") (d #t) (k 0)))) (h "1z5nsdryhmmb8cbdc419zjrpv6rw3ld2s40qgccvawkp6m167zrq") (f (quote (("rt") ("impl-register-debug") ("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.76")))

