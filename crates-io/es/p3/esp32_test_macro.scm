(define-module (crates-io es p3 esp32_test_macro) #:use-module (crates-io))

(define-public crate-esp32_test_macro-0.1.0 (c (n "esp32_test_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1258mba9mmkzrx26d66gzcxbmsxnxvgv56mycpzkbzhc2shbdnhm")))

(define-public crate-esp32_test_macro-0.1.1 (c (n "esp32_test_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1gpkvypxqzzzyrp0m174gg446aa19gjhmbraj1dicva9aj4qljsr")))

