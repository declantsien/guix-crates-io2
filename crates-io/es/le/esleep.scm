(define-module (crates-io es le esleep) #:use-module (crates-io))

(define-public crate-esleep-1.0.0 (c (n "esleep") (v "1.0.0") (h "1z381q7zplf8kw0j8xw7f17xgj8r9z05h5i5qw7bxs7kp9cbb91p")))

(define-public crate-esleep-1.1.0 (c (n "esleep") (v "1.1.0") (h "1nzmrr8126h2pyv72ql4cc3ik4zwdpcdcknx8cc9ami1spgcvpf2")))

