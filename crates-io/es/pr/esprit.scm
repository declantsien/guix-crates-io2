(define-module (crates-io es pr esprit) #:use-module (crates-io))

(define-public crate-esprit-0.0.1 (c (n "esprit") (v "0.0.1") (d (list (d (n "easter") (r "^0.0.1") (d #t) (k 0)) (d (n "estree") (r "^0.0.1") (d #t) (k 0)) (d (n "joker") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^0.5.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.5.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.1") (d #t) (k 0)))) (h "1mjxzbd8fr16smbbb4w99b77pnwad4ihk3f5c1nqz908j6iaxpff")))

(define-public crate-esprit-0.0.3 (c (n "esprit") (v "0.0.3") (d (list (d (n "easter") (r "^0.0.3") (d #t) (k 0)) (d (n "estree") (r "^0.0.3") (d #t) (k 0)) (d (n "joker") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.5.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.3") (d #t) (k 0)))) (h "1hfsf2q3ppd2b6jgwm2jkg4qbf9w7w2yibsvpbxcw0fn9c791ybs")))

(define-public crate-esprit-0.0.4 (c (n "esprit") (v "0.0.4") (d (list (d (n "easter") (r "^0.0.4") (d #t) (k 0)) (d (n "estree") (r "^0.0.4") (d #t) (k 0)) (d (n "joker") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.4") (d #t) (k 0)))) (h "02dnj5caiq99x4381yrcjgf45fci3zdv1n6nl4jvmjx0rs6l32x8")))

(define-public crate-esprit-0.0.5 (c (n "esprit") (v "0.0.5") (d (list (d (n "easter") (r "^0.0.5") (d #t) (k 0)) (d (n "estree") (r "^0.0.5") (d #t) (k 0)) (d (n "joker") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "unjson") (r "^0.0.5") (d #t) (k 0)))) (h "01qjb6rs7i69r4xrvcp755h4gr1l9ax7m1pkj13s7b7kfhb3p4ql")))

