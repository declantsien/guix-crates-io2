(define-module (crates-io es pr espresso) #:use-module (crates-io))

(define-public crate-espresso-0.0.1 (c (n "espresso") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jfaw6ii3i33qfxyf1wc8vr6493gcpp2vl90w0qsd1vqb0y4dnqr")))

