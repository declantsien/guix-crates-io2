(define-module (crates-io es pr espr-derive) #:use-module (crates-io))

(define-public crate-espr-derive-0.2.0 (c (n "espr-derive") (v "0.2.0") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 2)) (d (n "espr") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "ruststep") (r "^0.1.0") (d #t) (k 2)) (d (n "ruststep-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w3l40bhs4wyd337jh13nim9jq9bwywfn5pdx0ja2763rx8kl5cg")))

(define-public crate-espr-derive-0.3.0 (c (n "espr-derive") (v "0.3.0") (d (list (d (n "derive-new") (r "^0.5.9") (d #t) (k 2)) (d (n "espr") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "ruststep") (r "^0.3.0") (d #t) (k 2)) (d (n "ruststep-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (d #t) (k 2)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0alyq08plb0w5sm2bk3yhq736dghigihpzrnc11p1p6pm8h51vmv")))

