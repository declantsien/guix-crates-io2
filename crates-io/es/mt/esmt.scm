(define-module (crates-io es mt esmt) #:use-module (crates-io))

(define-public crate-esmt-0.1.0 (c (n "esmt") (v "0.1.0") (h "09q6xbag4b8in30l8c4mzr8mrgiapnsy3di9d4xwj16vs34ljf04") (y #t)))

(define-public crate-esmt-0.0.1 (c (n "esmt") (v "0.0.1") (h "1l8vasvmc5s8qafcac1a7pglj69m542hjrjddbj8806yk5d4j8l3")))

