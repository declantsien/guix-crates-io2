(define-module (crates-io es yn esyn-derive) #:use-module (crates-io))

(define-public crate-esyn-derive-0.1.0 (c (n "esyn-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0sclg8wgr8xrgj1phmlbn4i6s4lx2g3sbqnnpgks6gpw724qlsz4")))

(define-public crate-esyn-derive-0.2.0 (c (n "esyn-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0xsj76d3mjpfqcadwv5v7s3v21s8w8ixh053wd57mb11lkbhy3kk") (y #t)))

(define-public crate-esyn-derive-0.2.1 (c (n "esyn-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "0nfaq61a23sc7vzlhpq2g4md4lm58z74c74cih1mmg1bvayf4xhs") (y #t)))

(define-public crate-esyn-derive-0.2.2 (c (n "esyn-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "04ln3jd6z2mh50g0x5pz04hcyj15i5cp1c4b01qclclk2siplzcm")))

(define-public crate-esyn-derive-0.3.0 (c (n "esyn-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1yjzcn668h8whdmnmwg636l09z6r7cs4v0x27xd64czlyqpvls7z")))

(define-public crate-esyn-derive-0.4.0 (c (n "esyn-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0fvm8klcap7g31xs2cmc275ydhjnz516f7ifyq10pla1866aqaj3")))

(define-public crate-esyn-derive-0.4.1 (c (n "esyn-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1bmca9x4nnzcvzgnfanfz73rr874ahlf0x38n9d4gm2smcj52bh6")))

(define-public crate-esyn-derive-0.5.0 (c (n "esyn-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ya184im3j9lp776xn0aib9m3kjvfzv1k2qh1cyf9cpp2gvzvbz5")))

(define-public crate-esyn-derive-0.5.1 (c (n "esyn-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "11xn14c4dib0lanlm3d1wpwk878bdmhsf5adnx7nijpn3s6hki23")))

(define-public crate-esyn-derive-0.6.0 (c (n "esyn-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "180jb077bbkcxcipa9wplmjsmx1qv394p2l7i57lbgbb7czb5b7h")))

(define-public crate-esyn-derive-0.6.1 (c (n "esyn-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0kl9m895g95agv10djfflqrib7kn64z5ab7307v56gbwm0czrl5x")))

(define-public crate-esyn-derive-0.7.0 (c (n "esyn-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zw28pc209wrb33k97s8dbjs2yqg2yvj91jm0l66z0iib84r9ny3")))

(define-public crate-esyn-derive-0.8.0 (c (n "esyn-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0rl848h7sbvarpbygnid41myxzx74m663j168v58dms1bbqnylzn")))

(define-public crate-esyn-derive-0.8.1 (c (n "esyn-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0f7gcxgznfdicsjv3n4wv7bykg5v87yrqwyigxy6xqhfb36i9ayx")))

(define-public crate-esyn-derive-0.8.2 (c (n "esyn-derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0gjx3sg1hbaqc78vj8404a21j8pmwq2rxn42rvqwq9rqz8qi9fab")))

(define-public crate-esyn-derive-0.9.0 (c (n "esyn-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "09ggpq235aq22d8sgh6a00rwyhpk1yj46lbz11p99zs3mq8v60mm")))

(define-public crate-esyn-derive-0.9.1 (c (n "esyn-derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "1kswlbxmd3ds8cry67r149crga2rk8xl3k15vngypcjiwvpq0p3m")))

