(define-module (crates-io es cp escpos-rust) #:use-module (crates-io))

(define-public crate-escpos-rust-0.0.1 (c (n "escpos-rust") (v "0.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "codepage-437") (r "^0.1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rusb") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x9xdwd6g38vahrzmgsrjf46qhd21jnw5z78cwxl9kql5fywdmn7")))

(define-public crate-escpos-rust-0.0.2 (c (n "escpos-rust") (v "0.0.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "codepage-437") (r "^0.1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rusb") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f2jp8qhcc7kgdih6828nf1qb642zd1ql1qx3j5vfw74j64aidn5")))

