(define-module (crates-io es cp escpos-md) #:use-module (crates-io))

(define-public crate-escpos-md-0.1.0 (c (n "escpos-md") (v "0.1.0") (d (list (d (n "codepage-437") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1y6hy7n1nk381l6k88360b7zhdahjxsbv4fdq2cg67x35zys9ibk")))

(define-public crate-escpos-md-0.1.1 (c (n "escpos-md") (v "0.1.1") (d (list (d (n "codepage-437") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "15zvwr5fgvbzdbkfh5ifkcv56dwbzhcvx82sgklbv7s1w526026b")))

