(define-module (crates-io es cp escposify) #:use-module (crates-io))

(define-public crate-escposify-0.1.0 (c (n "escposify") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "image") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.3") (d #t) (k 0)))) (h "02g9gmsrf48wjflv3yf7l4jcrzmd12y6x79kw0fishf8d3galrn4")))

(define-public crate-escposify-0.1.1 (c (n "escposify") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "image") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.3") (d #t) (k 2)))) (h "1xr5lxnvm6id0y6ihqfjyxph4vrw0arm8c2c40yz6pzsli0pcm5x")))

(define-public crate-escposify-0.1.2 (c (n "escposify") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "image") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.3") (d #t) (k 2)))) (h "1ilwc1bii4xgl6c4akd9yw8w0lmidqhfhkjd9sjc26hkdm8513r5")))

(define-public crate-escposify-0.1.3 (c (n "escposify") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "image") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.3") (d #t) (k 2)))) (h "0hiw69gkj74jhfv4xwl23rfnb1xsmbs88kwxgi69acj7h7zm2kn1")))

(define-public crate-escposify-0.1.4 (c (n "escposify") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "image") (r "^0.12") (d #t) (k 0)) (d (n "qrcode") (r "^0.1.7") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.3") (d #t) (k 2)))) (h "10g7vilnihfikqp76g3a9gyaigyqqs05xyavylrlrr2s4mpsb3sq")))

(define-public crate-escposify-0.3.0 (c (n "escposify") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.14") (d #t) (k 0)) (d (n "qrcode") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^2.1.3") (d #t) (k 2)))) (h "0r31q3ypxwwr9h7rx57jx9ldvhc78j7g21apd07wqhj9mqfv6cn8") (f (quote (("qrcode_builder" "qrcode"))))))

(define-public crate-escposify-0.4.0 (c (n "escposify") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "qrcode") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^2.1.3") (d #t) (k 2)))) (h "1knasvkyzdx680mqj4s3028bkmfk9ab8vp7zsw78z8vcxr3aqxd0") (f (quote (("qrcode_builder" "qrcode"))))))

(define-public crate-escposify-0.4.1 (c (n "escposify") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "qrcode") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^2.1.3") (d #t) (k 2)))) (h "0g7pnb2j4ygmjlv7kv91pcmnklylxbviv363w3hy3hvqg276snhb") (f (quote (("qrcode_builder" "qrcode"))))))

(define-public crate-escposify-0.5.0 (c (n "escposify") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "qrcode") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^2.1.3") (d #t) (k 2)))) (h "0clvrpylyalc0y9m6782gnpw0lqk1g6ghcxrv4jv086z39rlz10w") (f (quote (("qrcode_builder" "qrcode"))))))

(define-public crate-escposify-0.5.1 (c (n "escposify") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "qrcode") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^2.1.3") (d #t) (k 2)))) (h "1mrfbhparmhgd6kp64xbznw5hdp8jrk1l5rfkvhw6hflv34jsfrf") (f (quote (("qrcode_builder" "qrcode"))))))

(define-public crate-escposify-0.5.2 (c (n "escposify") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "qrcode") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "0r583nsdhibv55j4bmdzahpdir90x4ziwixa97gm8hbhp71b6b2q") (f (quote (("qrcode_builder" "qrcode"))))))

(define-public crate-escposify-0.6.0 (c (n "escposify") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "qrcode") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "rusb") (r "^0.9.3") (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "0iw6d1n79852rn5l6i805iivm277w7srjmkbn8s88a2bvg5i3riz") (f (quote (("qrcode_builder" "qrcode"))))))

