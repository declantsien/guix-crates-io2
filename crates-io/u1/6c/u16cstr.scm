(define-module (crates-io u1 #{6c}# u16cstr) #:use-module (crates-io))

(define-public crate-u16cstr-0.1.0 (c (n "u16cstr") (v "0.1.0") (d (list (d (n "wchar") (r "^0.10") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0ljgkc4y7d7ys7j8ms5wjabdc2l2j7qfmf8wn2bb6xhqjraan956")))

(define-public crate-u16cstr-0.1.1 (c (n "u16cstr") (v "0.1.1") (d (list (d (n "wchar") (r "^0.10") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1xgp5nncwmgjzy36db962z3375kf02wv4hjb7qb5x1prgbyf7wh0")))

(define-public crate-u16cstr-0.2.0 (c (n "u16cstr") (v "0.2.0") (d (list (d (n "wchar") (r "^0.10") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1rygyr4fx6xjsqh077gwppdsvliyaig6qkznj922gpqdql5cjsp6")))

(define-public crate-u16cstr-0.2.1 (c (n "u16cstr") (v "0.2.1") (d (list (d (n "wchar") (r "^0.10") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1yqqr2vraadrnv7lk4cvdmy23acxxz2wblab511bl8j9gvp7jrlz")))

(define-public crate-u16cstr-0.2.2 (c (n "u16cstr") (v "0.2.2") (d (list (d (n "wchar") (r "^0.11") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1r3vxnl2bw93phv5r4nzk803x08ikv7qzjn3kk3yn1mrjpxniggg")))

(define-public crate-u16cstr-0.3.0 (c (n "u16cstr") (v "0.3.0") (d (list (d (n "wchar") (r "^0.11") (d #t) (k 0)) (d (n "widestring") (r "^0.5") (d #t) (k 0)))) (h "18s9ajg0phv0asx2cg56m0svq0hgfkjci48bhw913y7myzb6lbwc")))

(define-public crate-u16cstr-0.3.1 (c (n "u16cstr") (v "0.3.1") (d (list (d (n "wchar") (r "^0.11") (d #t) (k 0)) (d (n "widestring") (r "^0.5") (k 0)))) (h "0dfk290c4agkj4f7mcf3pw6wpkfqvza964ljsy6jg0prj0c7y0kx")))

(define-public crate-u16cstr-0.4.0 (c (n "u16cstr") (v "0.4.0") (d (list (d (n "wchar") (r "^0.11") (k 0)) (d (n "widestring") (r "^1.0") (k 0)) (d (n "widestring") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 2)))) (h "10fm18lpzgkqkszbbr2w8pl9ak4l2r61xzpam01zaasvgdym5yfj")))

(define-public crate-u16cstr-0.5.0 (c (n "u16cstr") (v "0.5.0") (d (list (d (n "widestring") (r "^1.0") (k 0)) (d (n "widestring") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 2)))) (h "1srjwb2k16zv8qp57480p0ili91pzp9mikgqgf4rhjfrnyjjgm80") (y #t)))

