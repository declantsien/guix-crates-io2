(define-module (crates-io hz la hzlang_parser) #:use-module (crates-io))

(define-public crate-hzlang_parser-1.0.0 (c (n "hzlang_parser") (v "1.0.0") (d (list (d (n "parco") (r "^3.0.1") (d #t) (k 0)))) (h "0gjaqkai5fm210bzxkmwckfm1igvgyxxbg74clznqnb6h1hgp0cx")))

(define-public crate-hzlang_parser-2.0.0 (c (n "hzlang_parser") (v "2.0.0") (d (list (d (n "parco") (r "^3.0.1") (d #t) (k 0)))) (h "1ip3k4q2shbi3lr23fbbx8pw0j0fvvk7zdgy4w1d24ggspym7z3n")))

(define-public crate-hzlang_parser-3.0.0 (c (n "hzlang_parser") (v "3.0.0") (d (list (d (n "parco") (r "^3.0.1") (d #t) (k 0)))) (h "0ikjznh1hswg1kpbgwg2fsa4sx814p0aw3aiz350yrql4yaxllw3")))

(define-public crate-hzlang_parser-4.0.0 (c (n "hzlang_parser") (v "4.0.0") (d (list (d (n "parco") (r "^3.0.1") (d #t) (k 0)))) (h "1h3q9br5gjj42b2qgk1f51mxids6bslvgv0h6lpmcxpjhdwrdx3i")))

(define-public crate-hzlang_parser-4.1.0 (c (n "hzlang_parser") (v "4.1.0") (d (list (d (n "parco") (r "^3.0.1") (d #t) (k 0)))) (h "0lfvj7k1wdvb9nbzimbyi836r2cl0y604qbsl8p64j3sncc6nsqz")))

(define-public crate-hzlang_parser-4.1.1 (c (n "hzlang_parser") (v "4.1.1") (d (list (d (n "parco") (r "^3.0.1") (d #t) (k 0)))) (h "1ypnqf85rk5kpzvq0qwwrai2w9r0y3ldk6jj4njfk1plhv6cl1hk")))

(define-public crate-hzlang_parser-5.0.0 (c (n "hzlang_parser") (v "5.0.0") (d (list (d (n "parco") (r "^3.0.1") (d #t) (k 0)))) (h "1rq7vxz19jzzr1i5l563y29imgryz02rkagisaf1sqqh91lx95cy")))

