(define-module (crates-io hz rd hzrd) #:use-module (crates-io))

(define-public crate-hzrd-0.0.1 (c (n "hzrd") (v "0.0.1") (h "1v18fj15ikzijbq8kp604sp7x9jbkynv6w539aw7vksa9aw3b7n2") (r "1.70")))

(define-public crate-hzrd-0.0.2 (c (n "hzrd") (v "0.0.2") (h "0si6zhhxzg0pj4rdfq0wfc31m5y9phwym0iba52pxjd47n2gzlij") (r "1.65")))

(define-public crate-hzrd-0.0.3 (c (n "hzrd") (v "0.0.3") (h "0nzbxlykgkbmwvhfj28mkykamflkx83vizygws0bssl7jv2bs9g8") (r "1.65")))

(define-public crate-hzrd-0.0.4 (c (n "hzrd") (v "0.0.4") (h "1wqzm4in9sr4ar7jgv0ankq5s0jc13p41jlk0zg63cwm3vpilss2") (r "1.65")))

(define-public crate-hzrd-0.0.5 (c (n "hzrd") (v "0.0.5") (h "1g2w13pk618qfw3b0kprxi2q3a7a3gj5nj31rhh409nsf5i6pvhw") (r "1.65")))

(define-public crate-hzrd-0.0.6 (c (n "hzrd") (v "0.0.6") (h "1p6ci5fap6zcw8whr1qqwbfj3cqc577rxfcxih98hcafyaq8zbxz") (r "1.70")))

