(define-module (crates-io hz gr hzgrow-r502) #:use-module (crates-io))

(define-public crate-hzgrow-r502-0.1.1 (c (n "hzgrow-r502") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "serialport") (r "^3.2.0") (d #t) (k 2)))) (h "0hq94j6nlpcxs8km3vjzlc1qnfm3hspvh4vpdnkjz1bd69cmgk2z")))

(define-public crate-hzgrow-r502-0.2.0 (c (n "hzgrow-r502") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "serialport") (r "^3.2.0") (d #t) (k 2)))) (h "1s0dykkbwvnnqy45brx567fy7ld1izxy77z3znqbl7mk6sjw41dr")))

(define-public crate-hzgrow-r502-0.2.1 (c (n "hzgrow-r502") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "serialport") (r "^3.2.0") (d #t) (k 2)))) (h "1d8p3z76lrzd5h800acfwq4qiymp0wxi0wg6a2vxb0ps0hiyvcz6")))

(define-public crate-hzgrow-r502-0.2.2 (c (n "hzgrow-r502") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "serialport") (r "^3.2.0") (d #t) (k 2)))) (h "0sazyg924jss2kqsj05m2zxxmsrva1v7ycmnr3xd094329rzrdn5")))

