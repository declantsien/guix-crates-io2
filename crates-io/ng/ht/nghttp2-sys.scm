(define-module (crates-io ng ht nghttp2-sys) #:use-module (crates-io))

(define-public crate-nghttp2-sys-0.1.0 (c (n "nghttp2-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "028j4vc9dykd5886ci1sm2ygbdf4nrx5lhk8aksmpf94s5vqhyk4") (l "nghttp2")))

(define-public crate-nghttp2-sys-0.1.1 (c (n "nghttp2-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)))) (h "1fjcvbqbbl1bs73z5wjmhbqxj6zxj5rvnsjllbznicdba0prnhri") (f (quote (("rustup" "rustfmt") ("rustfmt") ("default")))) (l "nghttp2")))

