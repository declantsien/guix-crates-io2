(define-module (crates-io ng yn ngyn_macros) #:use-module (crates-io))

(define-public crate-ngyn_macros-0.0.1 (c (n "ngyn_macros") (v "0.0.1") (d (list (d (n "ngyn_shared") (r "^0.0.1") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0p8q26nc7c13744qzycrlsynqm7ynbq259qlfksc84j22149czfg")))

(define-public crate-ngyn_macros-0.1.0 (c (n "ngyn_macros") (v "0.1.0") (d (list (d (n "ngyn_shared") (r "^0.1.0") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0xmzf8d175nl7ybix9vaasjhnwb989dyi2d2z889fgvrbgdrn8qc")))

(define-public crate-ngyn_macros-0.2.0 (c (n "ngyn_macros") (v "0.2.0") (d (list (d (n "ngyn_shared") (r "^0.1.0") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1aw4ifzylgwy9y0g8j40dmahgxj5y6ls4xgb6zpdkv9hldarfzvq")))

(define-public crate-ngyn_macros-0.2.1 (c (n "ngyn_macros") (v "0.2.1") (d (list (d (n "ngyn_shared") (r "^0.2.0") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0bwvanfv3ixdl381r8hidchzp21v910w4cr2ggrxs5nlbb85jzdh")))

(define-public crate-ngyn_macros-0.2.2 (c (n "ngyn_macros") (v "0.2.2") (d (list (d (n "ngyn_shared") (r "^0.2.1") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1gz1274y3c0ns8f2qg4gf7cdaqkji9s0wd860yr313p5fjffvqd0")))

(define-public crate-ngyn_macros-0.2.3 (c (n "ngyn_macros") (v "0.2.3") (d (list (d (n "ngyn_shared") (r "^0.2.3") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0cpsi7irmp2i143i9gr6j5a8a1dh4an42d1w6f2qw5l130s9d2y5")))

(define-public crate-ngyn_macros-0.2.4 (c (n "ngyn_macros") (v "0.2.4") (d (list (d (n "ngyn_shared") (r "^0.2.4") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0422ik96fi37rdgsp6bax1v55iwaih1b2bnqcgich1rcf3qjg5i4")))

(define-public crate-ngyn_macros-0.2.5 (c (n "ngyn_macros") (v "0.2.5") (d (list (d (n "ngyn_shared") (r "^0.2.5") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0jsmdx50cjh1pkjwqx92c3p18cxif9368pd0bh0p7f1j9dica0h6")))

(define-public crate-ngyn_macros-0.2.6 (c (n "ngyn_macros") (v "0.2.6") (d (list (d (n "ngyn_shared") (r "^0.2.6") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0mdphz2dbxv3p2m3acip38bzknbdrzszgkqicksis1nc1mvmwsmy")))

(define-public crate-ngyn_macros-0.2.7 (c (n "ngyn_macros") (v "0.2.7") (d (list (d (n "ngyn_shared") (r "^0.2.7") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0j58irkd3hiqziv7v4hs1nfpv2bjwwisxhkx5zcc97s71v9cl9kx")))

(define-public crate-ngyn_macros-0.2.8 (c (n "ngyn_macros") (v "0.2.8") (d (list (d (n "ngyn_shared") (r "^0.2.8") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1d4amfgwwibpgg8jhvawazp57lbw8v633naqd7vlp5i2n9rjhzqk")))

(define-public crate-ngyn_macros-0.3.0 (c (n "ngyn_macros") (v "0.3.0") (d (list (d (n "ngyn_shared") (r "^0.3.0") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1smmaldij88qvq7lkg9439n0qn3vff4npnhymbvwbwk8wlzk1y59")))

(define-public crate-ngyn_macros-0.3.1 (c (n "ngyn_macros") (v "0.3.1") (d (list (d (n "ngyn_shared") (r "^0.3.1") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0m6m0nv8xzwbiqdvcq1pppmx44infx61jprw14d2zl4zxlzjyshl")))

