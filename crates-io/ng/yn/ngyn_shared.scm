(define-module (crates-io ng yn ngyn_shared) #:use-module (crates-io))

(define-public crate-ngyn_shared-0.0.1 (c (n "ngyn_shared") (v "0.0.1") (d (list (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "0lqdlfyy9yj04pad608gkpsh0g15cmnpsq08a4vk3hq56ra9pla9")))

(define-public crate-ngyn_shared-0.1.0 (c (n "ngyn_shared") (v "0.1.0") (d (list (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "0s7hxahan1y485sx902vx1vylzfc98p2lnjvpk02wff1cxri6xpa")))

(define-public crate-ngyn_shared-0.2.0 (c (n "ngyn_shared") (v "0.2.0") (d (list (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "0gd42q4ya9w1w256yigxj7gkn6xi6vvshhfk28xlwvpnyglyp35r")))

(define-public crate-ngyn_shared-0.2.1 (c (n "ngyn_shared") (v "0.2.1") (d (list (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "1yv9c44xrmaq0gsf26xpq4i3dykz46q0cj71k65h0a335kh3x992")))

(define-public crate-ngyn_shared-0.2.2 (c (n "ngyn_shared") (v "0.2.2") (d (list (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "0dn5p8y6lgmdpcdaxcalxy7a821rl3c0499w7saqw5m2jc95kiwg")))

(define-public crate-ngyn_shared-0.2.3 (c (n "ngyn_shared") (v "0.2.3") (d (list (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "1hyniwdsmx8irg9lcl1fb40kv8vkp0m21z1mp8zl0pdw15k78fdq")))

(define-public crate-ngyn_shared-0.2.4 (c (n "ngyn_shared") (v "0.2.4") (d (list (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "12zpgdjjg4ypp95ivyz7ivbgasnzpmq7g83bwnw2ny1jaiz2b0mv")))

(define-public crate-ngyn_shared-0.2.5 (c (n "ngyn_shared") (v "0.2.5") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "1cb2dpcgzgk6lkx6pblnp9gi8zsndk3lbqqxxsl7wyg4azc3iikg") (s 2) (e (quote (("tide" "dep:tide"))))))

(define-public crate-ngyn_shared-0.2.6 (c (n "ngyn_shared") (v "0.2.6") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "0wf3l1rxp479macx85x7c6gzi28w77177r5yrmwrdkiaai28jlpk") (s 2) (e (quote (("tide" "dep:tide"))))))

(define-public crate-ngyn_shared-0.2.7 (c (n "ngyn_shared") (v "0.2.7") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "12mgzcwd3icgisp5ba3f3jqfn60f33h5c9wrcm6dw3risj9gr6y3") (s 2) (e (quote (("tide" "dep:tide"))))))

(define-public crate-ngyn_shared-0.2.8 (c (n "ngyn_shared") (v "0.2.8") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "1fw35ffsbjzirg6ax1w180j3g8w9sy64rzjh3rdrkk67nb392wd0") (s 2) (e (quote (("tide" "dep:tide"))))))

(define-public crate-ngyn_shared-0.3.0 (c (n "ngyn_shared") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)))) (h "1nsya7fcl1m7lymsr7w2knabha23k5802208gdid1mcnlr1dkbb5")))

(define-public crate-ngyn_shared-0.3.1 (c (n "ngyn_shared") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "nject") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "07h6v9h541in445laf5pbjmj3v7qwi7s2n29wkfh0mcvi16v28dj")))

