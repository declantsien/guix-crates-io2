(define-module (crates-io ng sp ngspice-parser) #:use-module (crates-io))

(define-public crate-ngspice-parser-0.1.0 (c (n "ngspice-parser") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1c5xl2chzh04gm8wfzyywkbii2zmac10a33av9wmkphpdk544bsg")))

(define-public crate-ngspice-parser-0.1.1 (c (n "ngspice-parser") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "16mlqmrhazvw9dbjc24rcvj3793nnssjd1al9nwslrzs6jwb1jp7")))

