(define-module (crates-io ng sp ngspice-sys) #:use-module (crates-io))

(define-public crate-ngspice-sys-0.1.0 (c (n "ngspice-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "16mspmksss9c5f77xynnr60jghx6q8wbyrypzviplspys2cmjf8g")))

(define-public crate-ngspice-sys-0.2.0 (c (n "ngspice-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6.6") (d #t) (k 0)))) (h "06wjxcp143343dh9rcnvh7wzirb2fq1wl78mknh2klbv2i62bqp0")))

(define-public crate-ngspice-sys-0.2.1 (c (n "ngspice-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "libloading") (r "^0.6.6") (d #t) (k 0)))) (h "1i5jnqkmnnkmnd0hqani78fjq14sjc13s7g5fk2yji075ycmwddq")))

