(define-module (crates-io ng ds ngds) #:use-module (crates-io))

(define-public crate-ngds-0.1.0 (c (n "ngds") (v "0.1.0") (h "0hk3bvspig8wjg4989xrkyyxiqw6a6vn6sj4ix7cya7vimxwj1r8")))

(define-public crate-ngds-0.1.1 (c (n "ngds") (v "0.1.1") (h "0mybglry8ajx25sq40v1b8h6ahdyf9vh587rjmmgfyl2mfs6nhp1")))

(define-public crate-ngds-0.1.2 (c (n "ngds") (v "0.1.2") (h "08mxvyh21j08x37bk7pd1cyficrhd1p4vl3q0wzd7dhzvln3d55f")))

