(define-module (crates-io ng ro ngrok-wrapper) #:use-module (crates-io))

(define-public crate-ngrok-wrapper-0.5.0 (c (n "ngrok-wrapper") (v "0.5.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 2)))) (h "1by448whqgpmp4alrisyk3x3bl1378q3p130j82nzz0lqa11j2c1")))

(define-public crate-ngrok-wrapper-0.5.1 (c (n "ngrok-wrapper") (v "0.5.1") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 2)))) (h "1lnmpb6db8q9nyxjv89cn9llii1wc8c5vrrkbnbzx9i537ik3rpf")))

(define-public crate-ngrok-wrapper-0.5.2 (c (n "ngrok-wrapper") (v "0.5.2") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 2)))) (h "0inphyjkg6jgf8822sla0nw1a0bkpr13jr6p102ydjqcy5nwyvg4")))

(define-public crate-ngrok-wrapper-0.5.3 (c (n "ngrok-wrapper") (v "0.5.3") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 2)))) (h "083yi1j0zb133d6kd0p763d9xsbxh56054spp31mznf8xg4dzp35")))

(define-public crate-ngrok-wrapper-0.5.4 (c (n "ngrok-wrapper") (v "0.5.4") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 2)))) (h "15rxkza9z5pxdpabwsni090663zhy7rdbn079l36vk51zb67b3yp")))

(define-public crate-ngrok-wrapper-0.5.5 (c (n "ngrok-wrapper") (v "0.5.5") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 2)))) (h "1m4a2k97bsz14qrrf7jwlsx6b643x0kkwikbjbvky7crriac35kw")))

(define-public crate-ngrok-wrapper-0.5.6 (c (n "ngrok-wrapper") (v "0.5.6") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "ureq") (r "^1") (f (quote ("json"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 2)))) (h "0f9qig01gh13qj02ih7bw18i7l951ssn5973bxr3mqsfd5rpxdj9")))

