(define-module (crates-io ng ru ngrust) #:use-module (crates-io))

(define-public crate-ngrust-1.0.0 (c (n "ngrust") (v "1.0.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "08y59rllanhskmj1rlhgba0rwzz36z6fybqiiqkizf8976llq673")))

(define-public crate-ngrust-1.0.1 (c (n "ngrust") (v "1.0.1") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1g9mr1xwg71ykm27dh2gkadlb8qa6ps3bsiw8avvk61n4bzk7v49")))

(define-public crate-ngrust-1.0.3 (c (n "ngrust") (v "1.0.3") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1ylbjf2f0g7sx2sj964a84gwvawav0saxkh0isc9v7s9g1gb270a")))

(define-public crate-ngrust-1.0.4 (c (n "ngrust") (v "1.0.4") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0ck48jibjnyg8nkhfys0ng99zrrnkqa3hkvwk6ify05jmgz0m7qs")))

(define-public crate-ngrust-1.0.5 (c (n "ngrust") (v "1.0.5") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "069kqnw2yp73min7gjzkm688avfg6a1msrzjcgbxwxd0bhqihpvi")))

