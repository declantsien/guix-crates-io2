(define-module (crates-io ng -c ng-clp) #:use-module (crates-io))

(define-public crate-ng-clp-0.1.0 (c (n "ng-clp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ccfbv769lhdr0xmijd7xbvp5kf0jkaxgy5hi5za2wjppw0fkgqn")))

(define-public crate-ng-clp-0.1.1 (c (n "ng-clp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gw22221iqyl0v87kry6x3h37455qzawxwvdrc286cwf7n0kki31")))

(define-public crate-ng-clp-0.2.0 (c (n "ng-clp") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cy208f0njmgapviaykscy8g290wmkj0c9dy6zpslj37xn8gigy3")))

(define-public crate-ng-clp-0.2.1 (c (n "ng-clp") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fhfvhk1i55gima99ax3phyxy5bamycc2lz7404zkr37hrvbxbz5")))

(define-public crate-ng-clp-0.3.0 (c (n "ng-clp") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08y04g6db3xl3rghhl3frhf5n0z9mmcr3lvd3fd7z48xnmp9z5vs")))

(define-public crate-ng-clp-0.3.1 (c (n "ng-clp") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yl33mvr6aa6ldsxag7z8d4c5manrxd1v9fwzxznhkajj9nwzdga")))

