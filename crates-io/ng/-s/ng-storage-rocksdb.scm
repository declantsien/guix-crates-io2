(define-module (crates-io ng -s ng-storage-rocksdb) #:use-module (crates-io))

(define-public crate-ng-storage-rocksdb-0.1.0 (c (n "ng-storage-rocksdb") (v "0.1.0") (d (list (d (n "ng-repo") (r "^0.1.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "serde_bare") (r "^0.5.0") (d #t) (k 0)))) (h "06bxii9w4ndw5r2s73zs16083w6vrl5cpfc7zlvjs5qp8mzd9fxd") (y #t) (r "1.74.0")))

(define-public crate-ng-storage-rocksdb-0.1.0-preview.1 (c (n "ng-storage-rocksdb") (v "0.1.0-preview.1") (d (list (d (n "ng-repo") (r "^0.1.0-preview.1") (d #t) (k 0)) (d (n "ng-rocksdb") (r "^0.21.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "serde_bare") (r "^0.5.0") (d #t) (k 0)))) (h "0si84za9m9pskc29irj3d6y80bgjwba9x2k13n5nsrigci298nl4") (y #t) (r "1.74.0")))

(define-public crate-ng-storage-rocksdb-0.1.0-preview.3 (c (n "ng-storage-rocksdb") (v "0.1.0-preview.3") (d (list (d (n "ng-repo") (r "^0.1.0-preview.1") (d #t) (k 0)) (d (n "ng-rocksdb") (r "^0.21.0-ngpreview.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "serde_bare") (r "^0.5.0") (d #t) (k 0)))) (h "0wbaqp32py581m1349lvpm8g1mn11lbisr15f9w63r0rfza0da58") (y #t) (r "1.74.0")))

(define-public crate-ng-storage-rocksdb-0.1.0-preview.5 (c (n "ng-storage-rocksdb") (v "0.1.0-preview.5") (d (list (d (n "ng-repo") (r "^0.1.0-preview.1") (d #t) (k 0)) (d (n "ng-rocksdb") (r "^0.21.0-ngpreview.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "serde_bare") (r "^0.5.0") (d #t) (k 0)))) (h "1g6nzcci7xw03iwvc9l01rvcnnbd6vxlk7xw1d5rgn98s94x9jn6") (y #t) (r "1.74.0")))

(define-public crate-ng-storage-rocksdb-0.1.0-preview.6 (c (n "ng-storage-rocksdb") (v "0.1.0-preview.6") (d (list (d (n "ng-repo") (r "^0.1.0-preview.1") (d #t) (k 0)) (d (n "ng-rocksdb") (r "^0.21.0-ngpreview.3") (d #t) (t "cfg(all(not(target_arch = \"wasm32\"),not(docsrs)))") (k 0)) (d (n "serde_bare") (r "^0.5.0") (d #t) (k 0)))) (h "0lb1g14apg5g2x590yl3j6gbzx44m0k0ifrsn023a8rx2va3snia") (r "1.74.0")))

