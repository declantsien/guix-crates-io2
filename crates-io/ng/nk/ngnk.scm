(define-module (crates-io ng nk ngnk) #:use-module (crates-io))

(define-public crate-ngnk-0.1.0 (c (n "ngnk") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "013b2n2ravfjs36fwpws4dz60dbbfa7i8x84y0ircsanpbckl0r0")))

(define-public crate-ngnk-0.1.2 (c (n "ngnk") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1hmrnpdrn4nnlqhcamw8njdjxyds54jzd5905z85g8yhwcpf91ih")))

(define-public crate-ngnk-0.1.3 (c (n "ngnk") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0sx96lnf9gxf2r7rg4rkcimdrp9avwz5v0arr5icjiw6jc6vl0sh")))

(define-public crate-ngnk-0.1.4 (c (n "ngnk") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0kqh366m7pprh6p0s9yyakgym0szdwi1jd95anvnzcza4ab9yhyr")))

(define-public crate-ngnk-0.1.7 (c (n "ngnk") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1ag45gygn35m0l6mm9jyvk7asmmi4q8cwfcif2f087cs1c0h9mwq")))

(define-public crate-ngnk-0.1.8 (c (n "ngnk") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1gn77wbzb26jmjjrjw8g3ys2z5xcirqwayjrzzlp2fg157bay85m")))

(define-public crate-ngnk-0.1.9 (c (n "ngnk") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0zflg4d3h3k3jzxypp0jqbz3kb67yfylm7f1mmadqxrnsbdj8f9z")))

(define-public crate-ngnk-0.2.0 (c (n "ngnk") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "15cx02x4x5l7gah8fsj5hvjx44r0nxqxyamf80l2w2rgkgvzrsj6")))

(define-public crate-ngnk-0.2.1 (c (n "ngnk") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1ck2igzza9b18ca24mij27xln66jzz9v6z7a4fg802bjd384155w")))

(define-public crate-ngnk-0.2.2 (c (n "ngnk") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "03vzkfbv034h03zwv2wsbh59qsh0kz3aljzw3m3400zh84i1v98i")))

(define-public crate-ngnk-0.2.3 (c (n "ngnk") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0c7axmiyf6m1yzx84siyx6drlaxklva094zl5xj3dfaygdc9c9dd")))

