(define-module (crates-io ng en ngen) #:use-module (crates-io))

(define-public crate-ngen-0.1.0 (c (n "ngen") (v "0.1.0") (h "1bw4gpcd64iccpc5j1ablm2gs4amim32nn6947izm0wr8m5jrr67")))

(define-public crate-ngen-0.1.1 (c (n "ngen") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (f (quote ("image"))) (d #t) (k 0)))) (h "1mlj9ljsnry3pwgidr0mq2kkp6dm1b0sxn1ajycjsk17zn3vqmax")))

(define-public crate-ngen-0.1.2 (c (n "ngen") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (f (quote ("image"))) (d #t) (k 0)))) (h "0gn79az16marjcdgbn4lv8znxivbnak65y5ksrzri5nzasbfdv8i")))

(define-public crate-ngen-0.1.3 (c (n "ngen") (v "0.1.3") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (f (quote ("image"))) (d #t) (k 0)))) (h "1hhcz4v3nk9601x4s5x9rvbyan0swgl66z22g3pkjs7f404lfkj8")))

(define-public crate-ngen-0.1.4 (c (n "ngen") (v "0.1.4") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (f (quote ("image"))) (d #t) (k 0)))) (h "184ds6dh5mr4ccwlxcaz54d13hqlxgkf34r9mv938w0djh8i1m15")))

