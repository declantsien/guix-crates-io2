(define-module (crates-io ng re ngrex) #:use-module (crates-io))

(define-public crate-ngrex-0.1.0 (c (n "ngrex") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1qw0xa56smqymx24289nkyrim02pvhlha4l0zalxl8j1g8l6h2ih")))

(define-public crate-ngrex-0.1.1 (c (n "ngrex") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "10jf9fbnz8w6bxmbd4xgxg0ax3pf67sqs9fa3fpg6rfysxniz8s1")))

