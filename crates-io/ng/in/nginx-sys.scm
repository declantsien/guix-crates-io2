(define-module (crates-io ng in nginx-sys) #:use-module (crates-io))

(define-public crate-nginx-sys-0.1.0 (c (n "nginx-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.0") (d #t) (k 1)) (d (n "duct") (r "^0.13.6") (d #t) (k 1)) (d (n "flate2") (r "^1.0.25") (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "ureq") (r "^2.6.2") (f (quote ("tls"))) (d #t) (k 1)) (d (n "which") (r "^4.4.0") (d #t) (k 1)))) (h "1lbiz34i09axsf333d01jl2a1hann00l1m9s4xsd3gsrhvzfa6yl")))

(define-public crate-nginx-sys-0.2.0 (c (n "nginx-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.0") (d #t) (k 1)) (d (n "duct") (r "^0.13.6") (d #t) (k 1)) (d (n "flate2") (r "^1.0.25") (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "ureq") (r "^2.6.2") (f (quote ("tls"))) (d #t) (k 1)) (d (n "which") (r "^4.4.0") (d #t) (k 1)))) (h "11i1a6fkpbl4bs1ax0750zkbnlgx9caaxrdpkcyjdxax3knb60ci")))

(define-public crate-nginx-sys-0.2.1 (c (n "nginx-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.65.0") (d #t) (k 1)) (d (n "duct") (r "^0.13.6") (d #t) (k 1)) (d (n "flate2") (r "^1.0.25") (d #t) (k 1)) (d (n "tar") (r "^0.4.38") (d #t) (k 1)) (d (n "ureq") (r "^2.6.2") (f (quote ("tls"))) (d #t) (k 1)) (d (n "which") (r "^4.4.0") (d #t) (k 1)))) (h "10bi83l6ifphkcr2ycgaxawncy81cj569487znra2ikas8y24bb1")))

