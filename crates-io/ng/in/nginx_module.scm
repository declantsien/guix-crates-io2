(define-module (crates-io ng in nginx_module) #:use-module (crates-io))

(define-public crate-nginx_module-0.1.0 (c (n "nginx_module") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)))) (h "1sbzlg6kg8b204hj5inl2pb793n1gxf5x2a68k7mnv15wf401ykm")))

(define-public crate-nginx_module-0.1.1 (c (n "nginx_module") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)))) (h "0715xlqpqk3hc0qy9fmb158fcw3761ixrx6dmlp45bbdqh7n96gl")))

(define-public crate-nginx_module-0.1.2 (c (n "nginx_module") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)))) (h "0nazqfs4b88f1wb92jbsh1rv3yi6vw8pmzifba9d9bsk444blmdv")))

(define-public crate-nginx_module-0.1.3 (c (n "nginx_module") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)))) (h "19vcvdjwqj7jg6hqjk4k6913z2vd9zj1a7z7s6vn8rk17z21prg0")))

(define-public crate-nginx_module-0.1.4 (c (n "nginx_module") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)))) (h "0n4pz20r3pgsf1d7pf92qw3vam08qgz8y8gymirkxk2jnsii7cy3")))

