(define-module (crates-io ng in nginx) #:use-module (crates-io))

(define-public crate-nginx-0.1.0 (c (n "nginx") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.31") (d #t) (k 1)))) (h "18qpf14n47696h5yxzmpax0zwan1a1mbps79i0jyzrikapm61f49")))

(define-public crate-nginx-0.1.1 (c (n "nginx") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)))) (h "167finlj53nkn2zy36qafk12kxxj8cnzp634j0h5a66z212nrxji")))

(define-public crate-nginx-0.1.2 (c (n "nginx") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)))) (h "10d7yh4fphnpsg1jvgqv9xm0bicbxi52kwf2kfjhm15rifzgxrc6")))

(define-public crate-nginx-0.2.0 (c (n "nginx") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)))) (h "107anlxw9kn2dzavhmr5vx1y2wlr6ji8chj3pa1a72cfn7vzdwf4")))

(define-public crate-nginx-0.3.0 (c (n "nginx") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)))) (h "1b04rhp6jcsqv8ddf23nvs5pjhicgmmqsj49996h5g3cy4bqh6fx")))

(define-public crate-nginx-0.4.0 (c (n "nginx") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)))) (h "0dvhg5lqabhpvlwvxx6b8vchajg28x8a9rgjqpvrvw9qzlrqjar7")))

(define-public crate-nginx-0.4.1 (c (n "nginx") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)))) (h "1irmrv4ml9c0h4bc1rb3y0pfcdvpandx53v3c58ijdk47s09pc8j")))

(define-public crate-nginx-0.5.0 (c (n "nginx") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)))) (h "1g09sa5k9alfqq0hxbd1vdwc4dby00mgzgbfzc2gn0dn54gh03cj")))

(define-public crate-nginx-0.6.0 (c (n "nginx") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)))) (h "0ck3n5z95n2444f4j0jw3fmrxyivs69q7l2algk7s9zd5nps2s52")))

(define-public crate-nginx-0.6.1 (c (n "nginx") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "1c22bz96nija505h3ny1pygmfdam3r6qiad0hk2yc1lcyladg4vc")))

(define-public crate-nginx-0.6.2 (c (n "nginx") (v "0.6.2") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "15an0g89hsy0ismvpb6wvbp5xz3pv70j86x4rgx464xcgfn3h75m")))

(define-public crate-nginx-0.7.0 (c (n "nginx") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "0nbakhvqwqcslaqqn1yii2mcp1ycyjabdizvik8wd662kp5zrn2c")))

(define-public crate-nginx-0.7.1 (c (n "nginx") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "0lwpiwjfafm38dcy5zawmb1pw90j1dji5llfwf46n1yaswgd3np8")))

(define-public crate-nginx-0.8.0 (c (n "nginx") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "1qhxmbgdciwg6xhgdi08r20hm0wc5x215nq96g88njh6dck3ppqf")))

(define-public crate-nginx-0.9.0 (c (n "nginx") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "0c53bgzypkf2yaas90isaka31m3zy1makh063w2br3x2m4ws3yxh")))

(define-public crate-nginx-0.9.1 (c (n "nginx") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "080i4z8ipfhl9sdkfiw7wx3bw3pfb29529d93rwpviyhaaxfczmz") (f (quote (("header") ("default")))) (y #t)))

(define-public crate-nginx-0.9.2 (c (n "nginx") (v "0.9.2") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "0brxy1jwfshn4riv7kl2sdfk9b6lgycpayn61ddk3184s9nr01xm") (f (quote (("header")))) (y #t)))

(define-public crate-nginx-0.9.3 (c (n "nginx") (v "0.9.3") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "1kk1yby7iwn7kj570dw5lzl1v2gd3bgl03fbpziwp3nrmyhjmwnm") (f (quote (("header")))) (y #t)))

(define-public crate-nginx-0.9.4 (c (n "nginx") (v "0.9.4") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "02y6015h4j0cc79rbrrz3v4ja1f9dq5a8kw2zg5v8rvbimb5gdxi")))

(define-public crate-nginx-0.8.1 (c (n "nginx") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "0cl19kmnlzhzfdmdp8cp0vlqb0v7vgb8k6bzdrk0pxp22w9wgyl7")))

(define-public crate-nginx-0.9.5 (c (n "nginx") (v "0.9.5") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "0qn3zdigwxb2y1gzb07qarsylby6qj30klblb8c9py0qhyl5amrq")))

(define-public crate-nginx-0.8.2 (c (n "nginx") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "0y53974kjw1bbmhrcwbd1118jxl2gx0j5r09qbnn4saz2ll800cx")))

(define-public crate-nginx-0.9.6 (c (n "nginx") (v "0.9.6") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "088llybcnc6z3hrnviqaq11la7llnmcx0d9r2b5wlyxgfdzv899b")))

(define-public crate-nginx-0.10.0 (c (n "nginx") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "1w9p1rxhjy4dicv62hi8afq1cqg3r5q3saf2y0kcwbcxl825q4sh")))

