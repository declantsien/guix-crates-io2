(define-module (crates-io ng in nginx-secure-link) #:use-module (crates-io))

(define-public crate-nginx-secure-link-0.1.0 (c (n "nginx-secure-link") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "0rxnbxxpc9q4ybkcvnx7cinfz61f1ysrvxkx7f6rzinpzamfglmi")))

(define-public crate-nginx-secure-link-0.1.1 (c (n "nginx-secure-link") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "0fhdmcylgqpv259cd4ajr4s1yji6nz08n2n3wp195xbc88lc1vpk")))

