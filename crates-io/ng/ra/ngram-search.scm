(define-module (crates-io ng ra ngram-search) #:use-module (crates-io))

(define-public crate-ngram-search-0.1.0 (c (n "ngram-search") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1qccbjbygvxcnikc6q00md70y691s0drpx8afd32r7n5vdgqvcm8") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-ngram-search-0.1.1 (c (n "ngram-search") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1jnfhifqgxqzxmpbsg5412pajz62412cb2wkzyx4irjz8pcdbnkk") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

