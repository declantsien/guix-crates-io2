(define-module (crates-io ng ra ngram) #:use-module (crates-io))

(define-public crate-ngram-0.1.0 (c (n "ngram") (v "0.1.0") (h "1w0n8xph6rd9abl6qby18j573xhlg0fnbyk796p10659h2irbw40")))

(define-public crate-ngram-0.1.1 (c (n "ngram") (v "0.1.1") (h "1wirj2ssckxib228zv9m50vi726w63zs7r331z031ksfqrwkzzz3")))

(define-public crate-ngram-0.1.2 (c (n "ngram") (v "0.1.2") (h "0mkph2bfp736g4r6jfmibq29xyiagrxqqvkqwb5k7xz2gng4j2fa")))

(define-public crate-ngram-0.1.3 (c (n "ngram") (v "0.1.3") (h "19cc77jgh05gbcc1waq7nggl868fszsq9ag4kgzf86r4bw82ya34")))

(define-public crate-ngram-0.1.4 (c (n "ngram") (v "0.1.4") (h "0lky7hj59sdwcy7aaljw4hngahcnvrq4zznslwavhhbrvywjddrq")))

(define-public crate-ngram-0.1.5 (c (n "ngram") (v "0.1.5") (h "0140c9h6dzzsslyz9maz964pv5bcjl4bfqh8l31iwwgyadwqrpi1")))

(define-public crate-ngram-0.1.6 (c (n "ngram") (v "0.1.6") (h "1w9fw8i60f0h29lvb6f0azvnfypcxf9lhccsccqy2yyzg593761c")))

(define-public crate-ngram-0.1.7 (c (n "ngram") (v "0.1.7") (h "1f5ns37l869a7pcmxkz59vvismnw5mz1cp7yykybw4hraxr44z7w")))

(define-public crate-ngram-0.1.8 (c (n "ngram") (v "0.1.8") (h "0plj85hi1p9rgjrabk3x1w0yf58k5r8vanq8cjn5rpscnwf8ysxq")))

(define-public crate-ngram-0.1.9 (c (n "ngram") (v "0.1.9") (h "0vjjv4li7gm1hv7cdss4zs66sdg6gm1v1nlw0j8zyql3ss2600kz")))

(define-public crate-ngram-0.1.10 (c (n "ngram") (v "0.1.10") (h "1gad8h48z8ky2cf2xvirgb1rri2waixdaih7s7jimhnqrarwshvs")))

(define-public crate-ngram-0.1.11 (c (n "ngram") (v "0.1.11") (h "1rdvwy1pmvw5rfyj66m9b99hl3v9k0llx7klv5z5rvj8ckmb2slg")))

(define-public crate-ngram-0.1.12 (c (n "ngram") (v "0.1.12") (h "17n64qsv4agaavwcs579kvw21rwig68zay0lgkxsh6bard0fxfax")))

(define-public crate-ngram-0.1.13 (c (n "ngram") (v "0.1.13") (h "0x9761ivkli2xjirfij7wchj4csm9dq0xj5ia80fk1sh90snpkmw")))

