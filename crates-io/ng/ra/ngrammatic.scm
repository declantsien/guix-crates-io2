(define-module (crates-io ng ra ngrammatic) #:use-module (crates-io))

(define-public crate-ngrammatic-0.2.0 (c (n "ngrammatic") (v "0.2.0") (h "0a70d6g8cwz5knk5lcc9nx2r90r9hijz6kkkydirk53y95jnwnca")))

(define-public crate-ngrammatic-0.2.1 (c (n "ngrammatic") (v "0.2.1") (h "0glxfl3jaiadslq3scsj422absvgmdyjqq08x15lhhb15xvi6z72")))

(define-public crate-ngrammatic-0.3.0 (c (n "ngrammatic") (v "0.3.0") (h "0bnc2zk1iqvlv0khk05x2a5dbb09ljkl7iphms20mv6ah8s29jpc")))

(define-public crate-ngrammatic-0.3.1 (c (n "ngrammatic") (v "0.3.1") (h "0s5jhrrz2zhz63vawi8l6nyivyaiw2wsf5ng61fykcpiva709lgn")))

(define-public crate-ngrammatic-0.3.2 (c (n "ngrammatic") (v "0.3.2") (h "1241l3d9ymgx1g4cj3gnlndm2x9nxq4znn22dvr2xnjbq3hnp7mp")))

(define-public crate-ngrammatic-0.3.3 (c (n "ngrammatic") (v "0.3.3") (h "0ysn0y867q8dsg94zrk7njj6a4a5ym0x5y2747w9ljvz6hrvwfmz")))

(define-public crate-ngrammatic-0.3.4 (c (n "ngrammatic") (v "0.3.4") (h "00dav66wi21s500d64q2vmzzbnxirbss99cwvjrzga5ib2qmmhgh")))

(define-public crate-ngrammatic-0.3.5 (c (n "ngrammatic") (v "0.3.5") (h "14b9wwcfgmih9d2dkf76kq5ficlfjzbs16bp1db5m1pmngs3wvf0")))

(define-public crate-ngrammatic-0.4.0 (c (n "ngrammatic") (v "0.4.0") (h "1zpnnpn4bs4ifmj3i7k2wb08nqr8lnxmkschlaw7znl2gsc2yvrw")))

