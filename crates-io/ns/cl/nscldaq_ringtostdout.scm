(define-module (crates-io ns cl nscldaq_ringtostdout) #:use-module (crates-io))

(define-public crate-nscldaq_ringtostdout-0.1.1 (c (n "nscldaq_ringtostdout") (v "0.1.1") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "nscldaq_ringbuffer") (r "^0.6.0") (d #t) (k 0)) (d (n "portman_client") (r "^0.1.0") (d #t) (k 0)) (d (n "proctitle") (r "^0.1.1") (d #t) (k 0)))) (h "0i83z40dbqaskp3v4jfj2rphvxar0ny1phhvppsks6z7sj8iaiv3")))

(define-public crate-nscldaq_ringtostdout-0.1.2 (c (n "nscldaq_ringtostdout") (v "0.1.2") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "nscldaq_ringbuffer") (r "^0.6.0") (d #t) (k 0)) (d (n "portman_client") (r "^0.1.0") (d #t) (k 0)) (d (n "proctitle") (r "^0.1.1") (d #t) (k 0)))) (h "0402cky0a7gqcdy7n96jgyskkrpi97rwwbdriyd4mk97mmrpxxhz")))

(define-public crate-nscldaq_ringtostdout-0.1.3 (c (n "nscldaq_ringtostdout") (v "0.1.3") (d (list (d (n "clap") (r "= 2.27.1") (d #t) (k 0)) (d (n "nscldaq_ringbuffer") (r "^0.6.0") (d #t) (k 0)) (d (n "portman_client") (r "^0.1.0") (d #t) (k 0)) (d (n "proctitle") (r "^0.1.1") (d #t) (k 0)))) (h "14ns6n3jhmabjpyvqwxna7q95ry6g78hxm7r219qmr6gi7mv2h6b")))

(define-public crate-nscldaq_ringtostdout-0.1.4 (c (n "nscldaq_ringtostdout") (v "0.1.4") (d (list (d (n "clap") (r "= 2.27.1") (d #t) (k 0)) (d (n "nscldaq_ringbuffer") (r "^0.6.0") (d #t) (k 0)) (d (n "portman_client") (r "^0.1.0") (d #t) (k 0)) (d (n "proctitle") (r "^0.1.1") (d #t) (k 0)))) (h "13w6147jinhlx6574ygvzg91p8zk3vf5gd104dslimcjd3s8m921")))

(define-public crate-nscldaq_ringtostdout-0.1.5 (c (n "nscldaq_ringtostdout") (v "0.1.5") (d (list (d (n "clap") (r "=2.27.1") (d #t) (k 0)) (d (n "nscldaq_ringbuffer") (r "^0.6.0") (d #t) (k 0)) (d (n "portman_client") (r "^0.1.0") (d #t) (k 0)) (d (n "proctitle") (r "^0.1.1") (d #t) (k 0)))) (h "16sbw3g1zsqrs9ml42lxylqx048p5j7ww5hdshpxzmrh7xwnq4yn")))

(define-public crate-nscldaq_ringtostdout-0.2.0 (c (n "nscldaq_ringtostdout") (v "0.2.0") (d (list (d (n "clap") (r "=2.27.1") (d #t) (k 0)) (d (n "nscldaq_ringbuffer") (r "^0.6.0") (d #t) (k 0)) (d (n "portman_client") (r "^0.1.0") (d #t) (k 0)) (d (n "proctitle") (r "^0.1.1") (d #t) (k 0)))) (h "0mbil5nm6p3z4c58vxmghbrr4q6x3h85cpigc57cl6rm9ara6ajd")))

