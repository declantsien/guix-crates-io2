(define-module (crates-io ns cl nscldaq_ringbuffer) #:use-module (crates-io))

(define-public crate-nscldaq_ringbuffer-0.4.0 (c (n "nscldaq_ringbuffer") (v "0.4.0") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "19pdygkaa4b861k58di1jm5d2fcf8h9pyzdjinj64dyi534kzl4q") (y #t)))

(define-public crate-nscldaq_ringbuffer-0.5.0 (c (n "nscldaq_ringbuffer") (v "0.5.0") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "1kc9jl7sjscdmzwvn5ry32hdg8y0fj73r1ir7nww425lj7xizgid")))

(define-public crate-nscldaq_ringbuffer-0.6.0 (c (n "nscldaq_ringbuffer") (v "0.6.0") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "0h2sfydsf48a928hb33xf14qir4b5ci8zl9bfy90mnn06f7qfa6h")))

(define-public crate-nscldaq_ringbuffer-0.6.1 (c (n "nscldaq_ringbuffer") (v "0.6.1") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "03z1n1xml91yxh1xymxmb57pvsirfix91fk9nnrl67di9rh5zmr0")))

(define-public crate-nscldaq_ringbuffer-0.6.2 (c (n "nscldaq_ringbuffer") (v "0.6.2") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "0cjk2wc8zlq7r7c3y9777ad53nnh2aas17bjx37w94xj2q96yipn")))

(define-public crate-nscldaq_ringbuffer-0.6.3 (c (n "nscldaq_ringbuffer") (v "0.6.3") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "0ak4b0g4bm5xlpg0gz7a32lxfvkaknmy2nm5c30jylafa6dypa37")))

(define-public crate-nscldaq_ringbuffer-0.6.4 (c (n "nscldaq_ringbuffer") (v "0.6.4") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "0cq2n9f61yyhqf5120snvy88jkl7b3702agr89l03d0w9qpplb98")))

(define-public crate-nscldaq_ringbuffer-0.7.0 (c (n "nscldaq_ringbuffer") (v "0.7.0") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "1gi7sdwalsbbm4p579zz2bfpvm01ypm4inrqqh9q0hd95ycrf7gv")))

(define-public crate-nscldaq_ringbuffer-0.7.1 (c (n "nscldaq_ringbuffer") (v "0.7.1") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "1qwqrh7j2bg4pvs01d8hs2s33rncdplwf3309lwi513z835s79s9")))

(define-public crate-nscldaq_ringbuffer-0.7.4 (c (n "nscldaq_ringbuffer") (v "0.7.4") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "0j2fwkwpmxqf9va37y4g6dr3f84qfgpaij1m5l3mbl06ksghxpmy")))

(define-public crate-nscldaq_ringbuffer-0.7.5 (c (n "nscldaq_ringbuffer") (v "0.7.5") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "13n4cd9xbyik1lv1vnw8vkvsicqp08g5r1wvr5kmw8ik9h03rhax") (y #t)))

(define-public crate-nscldaq_ringbuffer-0.7.6 (c (n "nscldaq_ringbuffer") (v "0.7.6") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "0mvpn5b2wigdch9zzgm4as3rii7ss4byspvg9lmvirbsfl6qxnh0")))

