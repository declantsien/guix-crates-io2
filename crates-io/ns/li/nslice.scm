(define-module (crates-io ns li nslice) #:use-module (crates-io))

(define-public crate-nslice-0.1.0 (c (n "nslice") (v "0.1.0") (h "13qha9z99lr3saaxabqrjxw54gdij3mc83nnfq85fzn58ihsb6cb")))

(define-public crate-nslice-0.2.0 (c (n "nslice") (v "0.2.0") (h "0k44198vmijrc5rscax6yjwl4388wn7jf3y5klrpdk8as855ynn1")))

(define-public crate-nslice-0.2.1 (c (n "nslice") (v "0.2.1") (h "1dkplq119axiq60kr7nw8xs53fxs2f56gr6hms5vf980syw22yz0") (r "1.56.1")))

