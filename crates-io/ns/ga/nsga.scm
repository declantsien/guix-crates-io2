(define-module (crates-io ns ga nsga) #:use-module (crates-io))

(define-public crate-nsga-0.1.0 (c (n "nsga") (v "0.1.0") (d (list (d (n "peeking_take_while") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1hn9b9dd9100j9pnvrvqig1pwndb0l9mwp9mb9hxv57zfpc9sgzy")))

(define-public crate-nsga-0.1.1 (c (n "nsga") (v "0.1.1") (d (list (d (n "peeking_take_while") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1crfdlpjj3i5in68ck4ax3g9b7r03hk32gbiqgk4yx8g55rj55ss")))

(define-public crate-nsga-0.1.2 (c (n "nsga") (v "0.1.2") (d (list (d (n "peeking_take_while") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0k9knf0290g7swqazvggmc8vn28yssh5bg8rl9b98i1k6f867780")))

