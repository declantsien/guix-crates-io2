(define-module (crates-io ns s- nss-sys) #:use-module (crates-io))

(define-public crate-nss-sys-0.0.1 (c (n "nss-sys") (v "0.0.1") (h "18i2170ifxihj0c731a8h21kmc91gvrvasdf25g4r147d3lip99g")))

(define-public crate-nss-sys-0.1.9 (c (n "nss-sys") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xhr4qcb84q10fpaimnkn2mqw2nh7qb8alhc2d9kx8inxshp2a36")))

