(define-module (crates-io ns s- nss-webpki) #:use-module (crates-io))

(define-public crate-nss-webpki-0.0.1 (c (n "nss-webpki") (v "0.0.1") (h "1ydlhy8r7z4nciprasy3mf0k7cc1665bjf8l07hawj6b7pg07232")))

(define-public crate-nss-webpki-0.3.2 (c (n "nss-webpki") (v "0.3.2") (d (list (d (n "mozilla-ca-certs") (r "^0.1") (d #t) (k 0)) (d (n "nss") (r "^0.7.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "untrusted") (r "^0.3.1") (d #t) (k 0)) (d (n "webpki") (r "^0.3.0") (d #t) (k 0)))) (h "16q57mi45vp4qrzphgp7ai4v2vjpzpfk146i29yb3p0s4s7dqp31")))

