(define-module (crates-io ns vg nsvg) #:use-module (crates-io))

(define-public crate-nsvg-0.1.0 (c (n "nsvg") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.18.0") (d #t) (k 0)))) (h "0qg52xv66iwyy71x3xaziwjv5lzrhjk171ndwx95mc3vqsabxb7a")))

(define-public crate-nsvg-0.2.0 (c (n "nsvg") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.26.3") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.18.0") (d #t) (k 0)))) (h "02i3f935ndcxc0g2lm1i6fnlxbl54z48x379xlc4cp58ihv925hq")))

(define-public crate-nsvg-0.2.1 (c (n "nsvg") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.26.3") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.18.0") (d #t) (k 0)))) (h "0s3sj2dnvj7mk5cvnb9rz4fj7k9f3b7cd56rm9wrnswagzl0g97y")))

(define-public crate-nsvg-0.3.0 (c (n "nsvg") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.26.3") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.18.0") (d #t) (k 0)))) (h "1d4jkpf6jqkijjv7fbp62jz9jx8v96zkxfhass782d35aqk126ks")))

(define-public crate-nsvg-0.3.1 (c (n "nsvg") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.26.3") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.18.0") (d #t) (k 0)))) (h "0ky7vkl7cc75bdvkxmmia00mmccpqd5djnbc59vamd4ng9pj5nrg")))

(define-public crate-nsvg-0.3.2 (c (n "nsvg") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.26.3") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.18.0") (d #t) (k 0)))) (h "008531z7pf4a5lrc1i5vpcs6hv50swgjbq594bs0i68kzn1mkii5")))

(define-public crate-nsvg-0.4.0 (c (n "nsvg") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.26.3") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1lfyi1d2kyp8cbk9sxqig6v2j4ym2zh2yskwy75abicwvhsf5d67")))

(define-public crate-nsvg-0.5.0 (c (n "nsvg") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.26.3") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "09gfqccm3ciq7h3y405dkqi28la15ip3ll6x6mkgi5fb8v6pqpjk") (f (quote (("default" "image"))))))

(define-public crate-nsvg-0.5.1 (c (n "nsvg") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.26.3") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0861ih38pl8bhjvgh7cikwkrd3d9hhh4ac3a3jq81jh5kha51yhb") (f (quote (("default" "image"))))))

