(define-module (crates-io ns ta nstack) #:use-module (crates-io))

(define-public crate-nstack-0.1.0 (c (n "nstack") (v "0.1.0") (d (list (d (n "kelvin") (r "^0.15") (d #t) (k 0)))) (h "1b9h0n14jvhdlnghlckibkhfndv3iq6bqlxnlj1ivl6798zhdj76")))

(define-public crate-nstack-0.2.0 (c (n "nstack") (v "0.2.0") (d (list (d (n "kelvin") (r "^0.16") (d #t) (k 0)))) (h "1896c98z50h5p1g7g7bz2j35lx6lgch3jnl4lyy4pjn0kndmgms2")))

(define-public crate-nstack-0.3.0 (c (n "nstack") (v "0.3.0") (d (list (d (n "kelvin") (r "^0.17") (d #t) (k 0)))) (h "1q1k83ps48mc1ybwlmwi9swf8pnkb4qyb78nql7l73rizj8sknj5")))

(define-public crate-nstack-0.4.0 (c (n "nstack") (v "0.4.0") (d (list (d (n "kelvin") (r "^0.18") (d #t) (k 0)))) (h "124yk9a85m1d427msq6pvjxk0xc0ndhy1a1akpx5r23vmnpx4isi")))

(define-public crate-nstack-0.5.0 (c (n "nstack") (v "0.5.0") (d (list (d (n "kelvin") (r "^0.19.0") (d #t) (k 0)))) (h "1065ngvn7srq9crp00qh7pypy60ia8g1x5agghi99hzs83a6bfj8")))

(define-public crate-nstack-0.6.0 (c (n "nstack") (v "0.6.0") (d (list (d (n "canonical") (r "^0.4") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4") (d #t) (k 0)) (d (n "canonical_host") (r "^0.4") (d #t) (k 0)) (d (n "microkelvin") (r "^0.4") (d #t) (k 0)))) (h "002i53125d760s61iwfv6mv4544z2xb3k3i7z50sc68db2l5m9ph")))

(define-public crate-nstack-0.6.1 (c (n "nstack") (v "0.6.1") (d (list (d (n "canonical") (r "^0.4") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4") (d #t) (k 0)) (d (n "canonical_host") (r "^0.4") (d #t) (k 2)) (d (n "microkelvin") (r "^0.5") (d #t) (k 0)))) (h "1l64p6lqi1qdgi8dxpf0cakfn01966l7qh32mjy36spc4nww6s8a")))

(define-public crate-nstack-0.6.2 (c (n "nstack") (v "0.6.2") (d (list (d (n "canonical") (r "^0.4") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4") (d #t) (k 0)) (d (n "canonical_host") (r "^0.4") (d #t) (k 2)) (d (n "microkelvin") (r "^0.5") (d #t) (k 0)))) (h "0vwd5yhgljcxxnl7z4msxkj0781cl47yqm8fdaz7ivxvzxsgzf50")))

(define-public crate-nstack-0.6.3 (c (n "nstack") (v "0.6.3") (d (list (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "canonical_host") (r "^0.5") (d #t) (k 2)) (d (n "microkelvin") (r "^0.5") (d #t) (k 0)))) (h "15vca225kywr0dk80dxms3z0s8xnif003g2x4gcp34jfkgh25ga2") (y #t)))

(define-public crate-nstack-0.7.0 (c (n "nstack") (v "0.7.0") (d (list (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "canonical_host") (r "^0.5") (d #t) (k 2)) (d (n "microkelvin") (r "^0.6") (d #t) (k 0)))) (h "0mrp0va59d16lb98xj1pl96vj52rfsyr76fq0v83zppqq0ab8j9w")))

(define-public crate-nstack-0.8.0 (c (n "nstack") (v "0.8.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.7") (d #t) (k 0)))) (h "0ja8hhskkayd0f0ccyn3bx63hspr5mqzzcjjclicbmak2b1r20mw")))

(define-public crate-nstack-0.8.1 (c (n "nstack") (v "0.8.1") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.9.0-rc") (d #t) (k 0)))) (h "1ylynknfxd3rwvnig2c9gpi7lzzi1c0camw657hws229246w0428") (f (quote (("persistance" "microkelvin/persistance")))) (y #t)))

(define-public crate-nstack-0.9.0 (c (n "nstack") (v "0.9.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.9") (d #t) (k 0)))) (h "1vgdbc99hsm6wk09arwsdhfwssac29v7dqyv83pq6bd3mjic115f") (f (quote (("persistance" "microkelvin/persistance"))))))

(define-public crate-nstack-0.10.0-rc.0 (c (n "nstack") (v "0.10.0-rc.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.10.0-rc") (d #t) (k 0)))) (h "18wwmbh57pn3nw5hf3jimcl4glh8ln0654zgdxq95j0jmp5b1qzl") (f (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-nstack-0.10.0 (c (n "nstack") (v "0.10.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.10") (d #t) (k 0)))) (h "1ngx7ax6i66annm060202a3m9sxhih4bv0mdp05c70plmlvwwp0x") (f (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-nstack-0.11.0-rc.0 (c (n "nstack") (v "0.11.0-rc.0") (d (list (d (n "microkelvin") (r "^0.11.0-rc.0") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.20") (d #t) (k 0)))) (h "1hiwx2va194bdv39niax2h4q61dfbzfhzmappbs1hqcskz7dwzyz")))

(define-public crate-nstack-0.12.0-rc.0 (c (n "nstack") (v "0.12.0-rc.0") (d (list (d (n "bytecheck") (r "^0.6.7") (k 0)) (d (n "microkelvin") (r "^0.13.0-rc.0") (k 0)) (d (n "microkelvin") (r "^0.13.0-rc.0") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.29") (f (quote ("validation"))) (k 0)))) (h "0ji6r1r5mz3kxyszyc14gfxa4rf4bpcciyfh1rha022crf7h7l2w")))

(define-public crate-nstack-0.13.0 (c (n "nstack") (v "0.13.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.14") (d #t) (k 0)))) (h "028lcbkkv2zyrk3dfr88maxj7qrrnbwwjflc3mvp7zh3n6j8zwbk") (f (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-nstack-0.14.0-rc.0 (c (n "nstack") (v "0.14.0-rc.0") (d (list (d (n "canonical") (r "^0.7") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.7") (d #t) (k 0)) (d (n "microkelvin") (r "^0.15.0-rc") (d #t) (k 0)))) (h "06ikindr3fnqizhssc9bpnlvbrsnm8r8xkl83zvhjyyygj94hjva") (f (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-nstack-0.15.0-rkyv.0 (c (n "nstack") (v "0.15.0-rkyv.0") (d (list (d (n "bytecheck") (r "^0.6.7") (k 0)) (d (n "microkelvin") (r "^0.16.0-rkyv") (k 0)) (d (n "microkelvin") (r "^0.16.0-rkyv") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.29") (f (quote ("validation"))) (k 0)))) (h "0sra884x7pypqbvsr1r5glmdnkqn1i4wx9x7yl96c4jcl7ffgids")))

(define-public crate-nstack-0.16.0-rc.0 (c (n "nstack") (v "0.16.0-rc.0") (d (list (d (n "microkelvin") (r "^0.17.0-rc") (d #t) (k 0)) (d (n "ranno") (r "^0.1") (d #t) (k 0)))) (h "0hf2qv2dwv23in7v06yydrwcsgw7h2gkz98jpmjas6a52wpi82wa")))

(define-public crate-nstack-0.16.0 (c (n "nstack") (v "0.16.0") (d (list (d (n "microkelvin") (r "^0.17.0-rc") (d #t) (k 0)) (d (n "ranno") (r "^0.1") (d #t) (k 0)))) (h "097jr2n5kss9ni11av6xwv6y8vp998xmrfwsrbdmbaq9ip76w48g")))

