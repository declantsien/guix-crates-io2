(define-module (crates-io ns db nsdb_secret) #:use-module (crates-io))

(define-public crate-nsdb_secret-0.1.0 (c (n "nsdb_secret") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0a7by6w22m1sc6hynj4j5ia5014nqpvm3dqw0fbg8dcx7x3hmy33")))

(define-public crate-nsdb_secret-0.1.1 (c (n "nsdb_secret") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1ys28r3vzwslcy9s7739kms1zmxwzzkgyfil9h93qf0d4wqwxp3z")))

(define-public crate-nsdb_secret-0.1.2 (c (n "nsdb_secret") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1bk00yv2s4409mjhxbarha0vqsbixq16kavq5013p88xhd66wra8")))

(define-public crate-nsdb_secret-0.1.3 (c (n "nsdb_secret") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0wi5nfx5bxyn1n49yprlr37xjny8i1lb5la3r70ng3b8pq38m3np")))

(define-public crate-nsdb_secret-0.2.0 (c (n "nsdb_secret") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "16lh00fxjg696r4drpl4wwwdqkacmgh0rv4cbcqvii74z7k61fi6")))

