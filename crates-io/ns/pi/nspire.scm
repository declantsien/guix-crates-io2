(define-module (crates-io ns pi nspire) #:use-module (crates-io))

(define-public crate-nspire-0.1.0 (c (n "nspire") (v "0.1.0") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0dljcdvzy4j4j95005d2ssbs8h1qin4k09s0pgynsg98ksi7gdjp")))

(define-public crate-nspire-0.2.0 (c (n "nspire") (v "0.2.0") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1c6wwyg2fsq34jr96xwm0cxxsryh03z63iprlv0bwyf6hvl7lzb7") (f (quote (("disable-panic-handler") ("disable-oom-handler") ("disable-allocator"))))))

(define-public crate-nspire-0.2.1 (c (n "nspire") (v "0.2.1") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)))) (h "12x1yyjvwrr7pjkxvqxrns8b12drby5zpn06342jl5kszx391y9y") (f (quote (("disable-panic-handler") ("disable-oom-handler") ("disable-eh-personality") ("disable-allocator"))))))

(define-public crate-nspire-0.3.0 (c (n "nspire") (v "0.3.0") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1fb0v4xyzg3kdiyg42i3wwqvagy5lsnbldk7zpgmgqhmbqb1zqha")))

(define-public crate-nspire-0.3.1 (c (n "nspire") (v "0.3.1") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)))) (h "09khwr8ql2xx6hd3w641x3jgvhky3dgch0akp0xd0i05va9j3w40")))

(define-public crate-nspire-0.3.2 (c (n "nspire") (v "0.3.2") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0nz8dy0v0lrhqjk72vqkp2kfx1wschnixrr7znpc4h6jmp48ws7c")))

(define-public crate-nspire-0.4.0 (c (n "nspire") (v "0.4.0") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0g8v55x6w6bss2i1x01xi2pb05fxbjkdgbf6v54xddnwbcwxhla5")))

(define-public crate-nspire-0.5.0 (c (n "nspire") (v "0.5.0") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)))) (h "048bfapllhi2mx8fh4hlgqb82bbf9lxyrb2yydc1y5g425s10rmx")))

(define-public crate-nspire-0.5.1 (c (n "nspire") (v "0.5.1") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1g1r3rp3cqa69shwxfqc61m3z82dll1x7ymlzghpqmb38g7ikywc")))

(define-public crate-nspire-0.5.2 (c (n "nspire") (v "0.5.2") (d (list (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "ndless-sys") (r "^0.1.2") (d #t) (k 0)))) (h "15434chbhwd0sspyb5kpymb0zg8ypxvkrqdlsfzj5xbv3vmklvrk")))

