(define-module (crates-io ns -s ns-scylla-orm-macros) #:use-module (crates-io))

(define-public crate-ns-scylla-orm-macros-0.1.0 (c (n "ns-scylla-orm-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (d #t) (k 0)))) (h "19disf86dks8qmjz4vqdhcri9c4d0sa01sb62bz318rsiz7rr8xm") (r "1.64")))

