(define-module (crates-io ns -s ns-std-threaded) #:use-module (crates-io))

(define-public crate-ns-std-threaded-0.1.0 (c (n "ns-std-threaded") (v "0.1.0") (d (list (d (n "abstract-ns") (r "^0.2.0") (d #t) (k 0)) (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "futures") (r "^0.1.2") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.2") (d #t) (k 0)))) (h "1dnrfr9by39iffya3f58wlz3ab2mjfbs30pn3pc7xv6yv96b4apv")))

(define-public crate-ns-std-threaded-0.2.0 (c (n "ns-std-threaded") (v "0.2.0") (d (list (d (n "abstract-ns") (r "^0.3.0") (d #t) (k 0)) (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "futures") (r "^0.1.2") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.2") (d #t) (k 0)))) (h "0pg95lfqzzd2fqhgsqpjz9rfmiw62zrhawzkwpi8xdwqv2fy9qpw")))

(define-public crate-ns-std-threaded-0.3.0 (c (n "ns-std-threaded") (v "0.3.0") (d (list (d (n "abstract-ns") (r "^0.4.0") (d #t) (k 0)) (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "futures") (r "^0.1.2") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "01p7r2qiqmwrmq7wysm85bb9slv84gyr83h73vqdp2szn3kwsax7")))

