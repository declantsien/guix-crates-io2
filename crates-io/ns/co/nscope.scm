(define-module (crates-io ns co nscope) #:use-module (crates-io))

(define-public crate-nscope-1.0.0-dev (c (n "nscope") (v "1.0.0-dev") (d (list (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "hidapi") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1qqpflf6n8hg4zl7dhzkp8limana1cdnr6hb62hkgwp96msvmb70")))

(define-public crate-nscope-1.0.0-dev.2 (c (n "nscope") (v "1.0.0-dev.2") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "hidapi") (r "~1.5") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)))) (h "1lj5maxfzqw63hd6f8fkcmzmsrsqmi254h039lbk6pp3w7sqkqbh")))

(define-public crate-nscope-1.0.0-dev.3 (c (n "nscope") (v "1.0.0-dev.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "hidapi") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 2)))) (h "1qwfyc8wbmzivd851adrvrrcyh0gi51d3nk1jl3lfsnwn4qnrf67")))

(define-public crate-nscope-1.0.0-dev.4 (c (n "nscope") (v "1.0.0-dev.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "hidapi") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 2)))) (h "00sibxqf9miyj3h7wbhkb4jrkg0jxainqq4nrk9p2d8120dqrck8")))

(define-public crate-nscope-1.0.0-dev.5 (c (n "nscope") (v "1.0.0-dev.5") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "hidapi") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 2)))) (h "1b2fdakmngia8ndjivqvz9pzmw9dzvvgfl151f82r23747z3d4gh")))

(define-public crate-nscope-1.0.0-dev.6 (c (n "nscope") (v "1.0.0-dev.6") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "hidapi") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 2)))) (h "0rydxzcv82db6ybp2rsv5ga0lnhi786yac1cc2i8ri8l88n9wkjx")))

(define-public crate-nscope-1.0.0-dev.7 (c (n "nscope") (v "1.0.0-dev.7") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "hidapi") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 2)))) (h "0gziqaslb3j5ab33v62wjg51agk5jy86mbxf5yza20j4di1d7116")))

(define-public crate-nscope-1.0.0 (c (n "nscope") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "hidapi") (r "^2.3.3") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "regex") (r "~1") (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (d #t) (k 2)))) (h "0yypq3mnqshwqrjzgys4m7m6rbjkramp4nf7zvarhywcfl3ryf8n")))

