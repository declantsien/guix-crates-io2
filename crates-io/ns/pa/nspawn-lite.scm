(define-module (crates-io ns pa nspawn-lite) #:use-module (crates-io))

(define-public crate-nspawn-lite-0.2.2 (c (n "nspawn-lite") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "myutil") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "09b3xp5pqi3rjgryds56wn9jkybzr6mzp2qbkp5fpjssrl1vxkr9")))

