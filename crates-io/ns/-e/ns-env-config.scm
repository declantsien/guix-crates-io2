(define-module (crates-io ns -e ns-env-config) #:use-module (crates-io))

(define-public crate-ns-env-config-0.1.0 (c (n "ns-env-config") (v "0.1.0") (d (list (d (n "abstract-ns") (r "^0.4.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.7") (d #t) (k 0)) (d (n "ns-router") (r "^0.1.2") (d #t) (k 0)) (d (n "ns-std-threaded") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0r8pdb5yic2ymwkl23hw9i71jhg5q3a3cvlj2n3d30jsq022qwzd")))

