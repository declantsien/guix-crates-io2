(define-module (crates-io ns wr nswrap) #:use-module (crates-io))

(define-public crate-nswrap-0.1.0 (c (n "nswrap") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linux-raw-sys") (r "^0.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.26") (f (quote ("mount"))) (d #t) (k 0)) (d (n "rustix") (r "^0.38") (f (quote ("process" "thread" "fs" "pipe"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1vga4xyx9b5r6a9c9fdnd6hsjjc0078rzis7xj6g149w4zf0flya")))

