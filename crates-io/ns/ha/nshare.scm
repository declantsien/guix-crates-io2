(define-module (crates-io ns ha nshare) #:use-module (crates-io))

(define-public crate-nshare-0.1.0 (c (n "nshare") (v "0.1.0") (d (list (d (n "image") (r "^0.23.2") (o #t) (k 0)) (d (n "nalgebra") (r "^0.20.0") (o #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (o #t) (k 0)))) (h "1ks7lazv4qc4y1d69aylk1sa2qapil61kfygf54hcnvj8znv0qda") (f (quote (("std" "nalgebra/std") ("defalut"))))))

(define-public crate-nshare-0.1.1 (c (n "nshare") (v "0.1.1") (d (list (d (n "image") (r "^0.23.2") (o #t) (k 0)) (d (n "nalgebra") (r "^0.20.0") (o #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (o #t) (k 0)))) (h "1gr47849c29nwdi86pv196gfr6ad2lsddnwxfysf9a10fr7fvmm4") (f (quote (("std" "nalgebra/std") ("defalut"))))))

(define-public crate-nshare-0.2.0 (c (n "nshare") (v "0.2.0") (d (list (d (n "image") (r "^0.23.12") (o #t) (k 0)) (d (n "nalgebra") (r "^0.23.1") (o #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (o #t) (k 0)))) (h "1wlzrvlaaafp6yz404fbrx082kna3wy8ml59zvhygbbqf6cvanzl") (f (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.3.0 (c (n "nshare") (v "0.3.0") (d (list (d (n "image") (r "^0.23.12") (o #t) (k 0)) (d (n "nalgebra") (r "^0.23.1") (o #t) (k 0)) (d (n "ndarray") (r "^0.15.0") (o #t) (k 0)))) (h "0gh3nf1q3ib19cxc0wjdgqdq2nbjmda4l4ffi9jzyr12lcfbkija") (f (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.4.0 (c (n "nshare") (v "0.4.0") (d (list (d (n "image") (r "^0.23.13") (o #t) (k 0)) (d (n "nalgebra") (r "^0.24.1") (o #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (o #t) (k 0)))) (h "1hj8jffkqcpimz90grmvnmjzfpi6y2bz4ls3xm3l9yyxhsc3rnzf") (f (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.5.0 (c (n "nshare") (v "0.5.0") (d (list (d (n "image") (r "^0.23.14") (o #t) (k 0)) (d (n "nalgebra") (r "^0.26.1") (o #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (k 0)))) (h "1awdgl76kqpxd3d38lzs54jw20frzr6b7ab43zbkl5426k16ysk2") (f (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.6.0 (c (n "nshare") (v "0.6.0") (d (list (d (n "image") (r "^0.23.14") (o #t) (k 0)) (d (n "nalgebra") (r "^0.26.2") (o #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (o #t) (k 0)))) (h "1mrkljnns7qssza65a6rmibdnim1k7jsidc5aw859dcal62gmmi6") (f (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.7.0 (c (n "nshare") (v "0.7.0") (d (list (d (n "image") (r "^0.23.14") (o #t) (k 0)) (d (n "nalgebra") (r "^0.27.1") (o #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (o #t) (k 0)))) (h "00bq0rrx3284hjlkp7v9yafn8n1hnca3g3wlkq4j2vm0fsq2j8gy") (f (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.8.0 (c (n "nshare") (v "0.8.0") (d (list (d (n "image") (r "^0.23.14") (o #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (o #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (o #t) (k 0)))) (h "1kn1l4h9aj59nchb83c9mqsj07wzkwvqkp8gvp8wnr8d2k9zhwib") (f (quote (("nalgebra_std" "nalgebra/std") ("default" "nalgebra" "nalgebra_std" "ndarray" "image"))))))

(define-public crate-nshare-0.9.0 (c (n "nshare") (v "0.9.0") (d (list (d (n "image") (r "^0.24.0") (o #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (o #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (o #t) (k 0)))) (h "1in44v5vrg2biwxrwy3102q831x1c4zarwn4dr0hfc8fsiy6ais4") (f (quote (("nalgebra_std" "nalgebra/std") ("default" "nalgebra" "nalgebra_std" "ndarray" "image"))))))

