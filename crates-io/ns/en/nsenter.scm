(define-module (crates-io ns en nsenter) #:use-module (crates-io))

(define-public crate-nsenter-0.0.1 (c (n "nsenter") (v "0.0.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "nsutils") (r "0.*") (d #t) (k 0)) (d (n "procinfo") (r "^0.3.1") (d #t) (k 0)) (d (n "unshare") (r "^0.1.14") (d #t) (k 0)))) (h "1c7zjq5vzyfgl70qdv7qbhnyl8ybcgjjdyl2fm586r4f46j0fx0k")))

