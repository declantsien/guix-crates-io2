(define-module (crates-io ns i- nsi-jupyter) #:use-module (crates-io))

(define-public crate-nsi-jupyter-0.7.0 (c (n "nsi-jupyter") (v "0.7.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13") (d #t) (k 0)) (d (n "evcxr_runtime") (r "^1.1") (d #t) (k 0)) (d (n "nsi-core") (r "^0.7.0") (f (quote ("output"))) (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "13z9mqd48jwi0bnafd28a9c1i437687yvfidqzszbf3f3dcczn6m")))

(define-public crate-nsi-jupyter-0.8.0 (c (n "nsi-jupyter") (v "0.8.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "evcxr_runtime") (r "^1.1") (d #t) (k 0)) (d (n "nsi-core") (r "^0.8") (f (quote ("output"))) (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "1mc2sd03zv8gklcc8fly370rzx0l1rzl1ix54nfip0xs6asx01zx")))

