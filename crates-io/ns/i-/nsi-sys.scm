(define-module (crates-io ns i- nsi-sys) #:use-module (crates-io))

(define-public crate-nsi-sys-0.1.0 (c (n "nsi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1fdrw274p2iv9mw0nrs6db5s985aa05v93975vanlp0nxizlhczr")))

(define-public crate-nsi-sys-0.1.1 (c (n "nsi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "download_rs") (r "^0.2.0") (d #t) (k 1)))) (h "0ir88bxh55b4vxr4cx0zjb5s70m4248kmkdr6jnbn48xjalyvbwv")))

(define-public crate-nsi-sys-0.1.3 (c (n "nsi-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "download_rs") (r "^0.2.0") (d #t) (k 1)))) (h "0if5nva25spmmp0qlr078c2advg0557xbvsx021lz0fdny6xq53m")))

(define-public crate-nsi-sys-0.1.4 (c (n "nsi-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)))) (h "0ky87xq6zvxmzk4bw67h4fki3fhimlzmd8dq9bli7qqya96d68g9") (y #t)))

(define-public crate-nsi-sys-0.1.5 (c (n "nsi-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking"))) (d #t) (k 1)))) (h "1p6207mvh13fj6h7kaqhmqsq2bis05cy2zxdk0nnnhqazqsg5512") (y #t)))

(define-public crate-nsi-sys-0.1.6 (c (n "nsi-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking"))) (d #t) (k 1)))) (h "00fa7wrcgj1sssg4w4gq9w03pvhxprciv68z0pyg3kyir0n1fcrq") (f (quote (("download_3delight_lib"))))))

(define-public crate-nsi-sys-0.1.7 (c (n "nsi-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking"))) (d #t) (k 1)))) (h "1xabsrmvp55lcwjn8996190aiz4jxjpjaai3c74hb3hbx93y0ccq") (f (quote (("download_3delight_lib"))))))

(define-public crate-nsi-sys-0.1.8 (c (n "nsi-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.6") (f (quote ("blocking"))) (d #t) (k 1)))) (h "0yj4l568hrdgaws1w30l3bai6y4zz3xvk8qxakfs4ql2rm6dax19") (f (quote (("download_3delight_lib"))))))

(define-public crate-nsi-sys-0.1.9 (c (n "nsi-sys") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking"))) (d #t) (k 1)))) (h "1wj8xj1pk50vwb3l8002090bdqchxiqh5qg4w3y23bzldhnxwj2l") (f (quote (("link_lib3delight") ("download_lib3delight"))))))

(define-public crate-nsi-sys-0.1.10 (c (n "nsi-sys") (v "0.1.10") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "1nvfmfdk20xq2h5yw2hq8n5vynivdxqc784dqaa36zq34w7frcsk") (f (quote (("link_lib3delight") ("download_lib3delight" "reqwest") ("default"))))))

(define-public crate-nsi-sys-0.1.11 (c (n "nsi-sys") (v "0.1.11") (d (list (d (n "bindgen") (r "^0.55.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "02i0dyqrmaz8731sjb42kz6c0s8x94rd2psx6l75m8hkpskjl43d") (f (quote (("link_lib3delight") ("download_lib3delight" "reqwest") ("default"))))))

(define-public crate-nsi-sys-0.1.12 (c (n "nsi-sys") (v "0.1.12") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "14k29660szhnbvhyr1lj8rjqqvc8rlkyf3j0cr4271pn0lrl24h4") (f (quote (("link_lib3delight") ("download_lib3delight" "reqwest") ("default"))))))

(define-public crate-nsi-sys-0.1.13 (c (n "nsi-sys") (v "0.1.13") (d (list (d (n "bindgen") (r ">=0.55.1, <0.56.0") (d #t) (k 1)) (d (n "reqwest") (r ">=0.10.9, <0.11.0") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "0f38a5lij9aajgfis4z9s0nklphvccbwiipgibp1kz37apga59kg") (f (quote (("link_lib3delight") ("download_lib3delight" "reqwest") ("default"))))))

(define-public crate-nsi-sys-0.1.14 (c (n "nsi-sys") (v "0.1.14") (d (list (d (n "bindgen") (r "^0.58.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "0m0hphc4pacj07h96ngzrrc1i3clk9jwj8whvlp9mrglkclkibsg") (f (quote (("link_lib3delight") ("download_lib3delight" "reqwest") ("default"))))))

(define-public crate-nsi-sys-0.2.0 (c (n "nsi-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "06k0fzqy1iyvydhmn2vbf4z15qhb20ajkiz91dnp20y9ydrq2dkr") (f (quote (("omit_functions") ("link_lib3delight") ("download_lib3delight" "reqwest") ("default"))))))

(define-public crate-nsi-sys-0.2.1 (c (n "nsi-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "01iy1k09mvk8yz3qd6rr3ziwxy6glvsj3n4snlaiwvcb932hjg6r") (f (quote (("omit_functions") ("link_lib3delight") ("download_lib3delight" "reqwest") ("default"))))))

(define-public crate-nsi-sys-0.2.2 (c (n "nsi-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "bitflags") (r "^2") (d #t) (k 0)))) (h "1pysvz78g60zzhkrhrpcavbrh0r2n6qmf838fakvn5dgr04pg36d") (f (quote (("omit_functions") ("default"))))))

(define-public crate-nsi-sys-0.7.0 (c (n "nsi-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "bitflags") (r "^2") (d #t) (k 0)))) (h "1cj4w09b5nl0j5k0byk3cvinv5f21av83038r5wh0flwq6h5x39v") (f (quote (("omit_functions") ("default"))))))

(define-public crate-nsi-sys-0.7.1 (c (n "nsi-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "bitflags") (r "^2") (d #t) (k 0)))) (h "1sd1ci0hqvva0mn4nrxi817qc5yfnnnw2wxqb9h2895n5j9dyb8a") (f (quote (("omit_functions") ("default"))))))

(define-public crate-nsi-sys-0.8.0 (c (n "nsi-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "bitflags") (r "^2") (d #t) (k 0)))) (h "18k84b7rsaz9wpll71b7dy0cjfxp2g2palw1cfggy8g636l0y8l7") (f (quote (("omit_functions") ("default"))))))

