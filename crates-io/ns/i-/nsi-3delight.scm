(define-module (crates-io ns i- nsi-3delight) #:use-module (crates-io))

(define-public crate-nsi-3delight-0.1.0 (c (n "nsi-3delight") (v "0.1.0") (d (list (d (n "nsi") (r "~0.5") (f (quote ("toolbelt"))) (d #t) (k 0)))) (h "1ybgahk3xacn90y83srafn4f064fyi2bn5cmv22d9503mnkahvd8") (y #t)))

(define-public crate-nsi-3delight-0.1.1 (c (n "nsi-3delight") (v "0.1.1") (d (list (d (n "nsi") (r "~0.6") (f (quote ("toolbelt"))) (d #t) (k 0)))) (h "1caw68bzmjy00gr165lx07v4m8gbbgb9qlmqbs2z64km8a3nsn4s")))

(define-public crate-nsi-3delight-0.7.0 (c (n "nsi-3delight") (v "0.7.0") (d (list (d (n "nsi-core") (r "^0.7.0") (d #t) (k 0)) (d (n "nsi-toolbelt") (r "^0.7.0") (d #t) (k 0)))) (h "1glagzyk3jhd5fqp2ik6lzbaa3zafi8nrgv14j4my9s9b7bi4bbw")))

(define-public crate-nsi-3delight-0.8.0 (c (n "nsi-3delight") (v "0.8.0") (d (list (d (n "nsi-core") (r "^0.8") (d #t) (k 0)) (d (n "nsi-toolbelt") (r "^0.8") (d #t) (k 0)))) (h "0bx4w3w9jwqbhqwwass9fhgf3nzi6mq63qagw7x4hfshl79nglkv")))

