(define-module (crates-io ns i- nsi-toolbelt) #:use-module (crates-io))

(define-public crate-nsi-toolbelt-0.7.0 (c (n "nsi-toolbelt") (v "0.7.0") (d (list (d (n "nsi-core") (r "^0.7.0") (d #t) (k 0)) (d (n "petname") (r "^1.1") (f (quote ("std_rng" "default_dictionary"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "ultraviolet") (r "^0.9") (f (quote ("f64"))) (d #t) (k 0)))) (h "050c0371k8r3v2q9frf4blaamaahc94az3gnwc3rb5hrdzzcpsna")))

(define-public crate-nsi-toolbelt-0.8.0 (c (n "nsi-toolbelt") (v "0.8.0") (d (list (d (n "nsi-core") (r "^0.8") (d #t) (k 0)) (d (n "petname") (r "^1.1") (f (quote ("std_rng" "default_dictionary"))) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "ultraviolet") (r "^0.9") (f (quote ("f64"))) (d #t) (k 0)))) (h "1zjvh98gsw2h3x6fnfkc25hjxpjb76pmgqb9b2hxmx3c6k44lqzy")))

