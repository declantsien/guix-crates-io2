(define-module (crates-io ns ut nsutils) #:use-module (crates-io))

(define-public crate-nsutils-0.0.1 (c (n "nsutils") (v "0.0.1") (d (list (d (n "lsns") (r "^0.0.1") (d #t) (k 0)))) (h "1acky10q44b6bkd196is120lbcl51zwqqqk8kkmjcfqp30d054iq")))

(define-public crate-nsutils-0.0.2 (c (n "nsutils") (v "0.0.2") (d (list (d (n "lsns") (r "0.*") (d #t) (k 0)) (d (n "procinfo") (r "^0.3.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.6") (d #t) (k 0)) (d (n "unshare") (r "^0.1.14") (d #t) (k 0)))) (h "1w34njs1y9cpxp1nf2lamz0d9pcb7db0xhx24xcmzm8s53vilj5f")))

(define-public crate-nsutils-0.0.3 (c (n "nsutils") (v "0.0.3") (d (list (d (n "procinfo") (r "^0.3.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.6") (d #t) (k 0)) (d (n "unshare") (r "^0.1.14") (d #t) (k 0)))) (h "0dlcjxsfkp9f65lw3zcwrka944bbblis28b02d4zh87vhd7syrqj")))

(define-public crate-nsutils-0.0.4 (c (n "nsutils") (v "0.0.4") (d (list (d (n "procinfo") (r "^0.3.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.6") (d #t) (k 0)) (d (n "unshare") (r "^0.1.14") (d #t) (k 0)))) (h "12ijsqw77m3v48rcx53pqgla23za5f1ww61vl677mam0fnd1ji7f")))

(define-public crate-nsutils-0.0.5 (c (n "nsutils") (v "0.0.5") (d (list (d (n "procinfo") (r "^0.3.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.6") (d #t) (k 0)) (d (n "unshare") (r "^0.1.14") (d #t) (k 0)))) (h "04kcwvwvcc51ykcylqzq5j641bjwxzv0m8hry7b4g01gfh4mqml8")))

