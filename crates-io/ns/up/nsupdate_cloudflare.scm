(define-module (crates-io ns up nsupdate_cloudflare) #:use-module (crates-io))

(define-public crate-nsupdate_cloudflare-0.1.0 (c (n "nsupdate_cloudflare") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surf") (r "^2.0.0-alpha.2") (d #t) (k 0)))) (h "14sqk9insjd35y5wd8qsimnc1a2011fxrna7qlkvw2zar60psv44")))

