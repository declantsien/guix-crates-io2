(define-module (crates-io ns de nsdetect) #:use-module (crates-io))

(define-public crate-nsdetect-0.1.0 (c (n "nsdetect") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21.2") (d #t) (k 0)))) (h "1ps2bxg2jafldhc27bbayyxgz1c22l8qkvv8frdj065cfcqcb9g0")))

(define-public crate-nsdetect-0.2.0 (c (n "nsdetect") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.18.0") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21.2") (d #t) (k 0)))) (h "1p91r9rvb8j5cqzaqlkkvwxc3q07nv79hp6q81p0cgnl46wcd97d")))

