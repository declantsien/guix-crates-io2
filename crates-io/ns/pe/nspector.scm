(define-module (crates-io ns pe nspector) #:use-module (crates-io))

(define-public crate-nspector-0.1.0 (c (n "nspector") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "19kwsa8dbagflcfgdam121943x4r5fym3kqlvmj5pnidr5g04asz")))

