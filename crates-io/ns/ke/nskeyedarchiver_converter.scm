(define-module (crates-io ns ke nskeyedarchiver_converter) #:use-module (crates-io))

(define-public crate-nskeyedarchiver_converter-0.1.0 (c (n "nskeyedarchiver_converter") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11qpj0l9xbw796by3xg908ik7zjlq6xpl1w18g7fq8ka533h4w24") (f (quote (("default")))) (s 2) (e (quote (("exe_serde_json" "dep:serde_json"))))))

(define-public crate-nskeyedarchiver_converter-0.1.1 (c (n "nskeyedarchiver_converter") (v "0.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sphsg53barddkpxd7k27p9vg0z9j4j1y2vs0wzkxahawkgybzaa") (f (quote (("default")))) (s 2) (e (quote (("exe_serde_json" "dep:serde_json"))))))

