(define-module (crates-io ns _r ns_rust_lib) #:use-module (crates-io))

(define-public crate-ns_rust_lib-0.1.0 (c (n "ns_rust_lib") (v "0.1.0") (h "1jmjgp6fg29nnmjds2a7pzjz9z1y0jbyhm3z03z1kxzlhlwpihrm")))

(define-public crate-ns_rust_lib-0.1.1 (c (n "ns_rust_lib") (v "0.1.1") (h "0g34d7908fy5j2gsya386bi9i5gba2r6lq92hph0aza2yhlm3lsi")))

