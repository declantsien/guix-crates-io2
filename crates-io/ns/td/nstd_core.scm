(define-module (crates-io ns td nstd_core) #:use-module (crates-io))

(define-public crate-nstd_core-0.1.0 (c (n "nstd_core") (v "0.1.0") (h "0366936bkhxfnb95a0kpjjkwhz9bsj3bw1zk351vb4cjlzycqcmi") (f (quote (("std") ("default" "std"))))))

(define-public crate-nstd_core-0.1.1 (c (n "nstd_core") (v "0.1.1") (h "084fwdrm0v113nx484b0s3czkx0i6ldsw323yzgsanf18jhqf6in") (f (quote (("std") ("default" "std"))))))

(define-public crate-nstd_core-0.1.2 (c (n "nstd_core") (v "0.1.2") (d (list (d (n "platforms") (r "^2.0.0") (k 0)))) (h "1xj5fimwmvw42ys4044xm070s93xg9lfj06fkpxxldl2jx1971bq") (f (quote (("std") ("default" "std"))))))

(define-public crate-nstd_core-0.1.3 (c (n "nstd_core") (v "0.1.3") (d (list (d (n "platforms") (r "^2.0.0") (k 0)))) (h "08x3rqkrrk6b08pc3pg9miw01asby6g4kdsqabp1yrwabpav6ka1") (f (quote (("std") ("default" "std"))))))

(define-public crate-nstd_core-0.2.0 (c (n "nstd_core") (v "0.2.0") (d (list (d (n "platforms") (r "^2.0.0") (k 0)))) (h "0wjnizsp08h862sh44r90j4g6wfa0dp6xfd4dfw69x9rysz6f6m8") (f (quote (("std") ("default" "std") ("clib"))))))

(define-public crate-nstd_core-0.2.1 (c (n "nstd_core") (v "0.2.1") (d (list (d (n "platforms") (r "^2.0.0") (k 0)))) (h "15ikdha52qqsck77xfd1rw6zrilrc513j1ba888yy6pzxai65fqr") (f (quote (("std") ("default" "std") ("clib"))))))

(define-public crate-nstd_core-0.2.2 (c (n "nstd_core") (v "0.2.2") (d (list (d (n "platforms") (r "^2.0.0") (k 0)))) (h "1c75lvv6jfh9y1k61d2m8l1ymhgglisyc36pp57ji7dal341n0mn") (f (quote (("std") ("default" "std") ("clib"))))))

(define-public crate-nstd_core-0.2.3 (c (n "nstd_core") (v "0.2.3") (d (list (d (n "platforms") (r "^2.0.0") (k 0)))) (h "0kybd1a66jc2gwwkri5q1qkpr6nwg1687zjhyjn7wqwig6hmjsv5") (f (quote (("std") ("default" "std") ("clib"))))))

(define-public crate-nstd_core-0.3.0 (c (n "nstd_core") (v "0.3.0") (d (list (d (n "platforms") (r "^2.0.0") (k 0)))) (h "0aky32vr4lnz11qgbrnd7vsqjbm5gzms44q70hjlz68yss9aw2va") (f (quote (("std") ("deps") ("default" "std") ("clib"))))))

(define-public crate-nstd_core-0.3.1 (c (n "nstd_core") (v "0.3.1") (d (list (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (k 0)))) (h "1r3s6zgm1iviw5gddmf5nkn3bh13y4w9g44f7m8l3g9pgh5x1h3b") (f (quote (("std") ("deps") ("default" "std") ("clib"))))))

(define-public crate-nstd_core-0.3.2 (c (n "nstd_core") (v "0.3.2") (d (list (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (k 0)))) (h "0il1g9n6vlk49qf0xzzxkyrhjj0kk72558b3ms8k6mp18p8k4j54") (f (quote (("std") ("deps") ("default" "std") ("clib"))))))

(define-public crate-nstd_core-0.4.0 (c (n "nstd_core") (v "0.4.0") (d (list (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (k 0)))) (h "127krmq3w9x7fb40q187vpdcwwidavifjllmcnamzv25c58djcfn") (f (quote (("std" "panics") ("panics") ("deps") ("default" "std") ("clib"))))))

(define-public crate-nstd_core-0.5.0 (c (n "nstd_core") (v "0.5.0") (d (list (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "platforms") (r "^2.0.0") (k 0)))) (h "1g8jrv5lb8h2b74asmc65y0xf37rbfag6i04frkas501vq4163xm") (f (quote (("std" "panics") ("panics") ("deps") ("default" "std") ("clib"))))))

