(define-module (crates-io ns td nstd_gui) #:use-module (crates-io))

(define-public crate-nstd_gui-0.1.0 (c (n "nstd_gui") (v "0.1.0") (d (list (d (n "nstd_events") (r "^0.1.0") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "0xxbvjr4n0prk1hwgjk12qpqjpa4rf2jk7da38261nmjsbxld5nl")))

(define-public crate-nstd_gui-0.2.0 (c (n "nstd_gui") (v "0.2.0") (d (list (d (n "nstd_events") (r "^0.2.0") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "15jn6w17d93j445m234lixyw9f5kq4fljk79n14r1bz2qachm7py") (f (quote (("clib"))))))

(define-public crate-nstd_gui-0.2.1 (c (n "nstd_gui") (v "0.2.1") (d (list (d (n "nstd_events") (r "^0.2.1") (d #t) (k 0)) (d (n "nstd_image") (r "^0.2.0") (d #t) (k 0)) (d (n "winit") (r "^0.26.0") (d #t) (k 0)))) (h "1kv7l4hl198dd6aafcz4vsjqz6mkp8qsy2s2v8zqsa6z7vli33vm") (f (quote (("clib"))))))

(define-public crate-nstd_gui-0.3.0 (c (n "nstd_gui") (v "0.3.0") (d (list (d (n "nstd_events") (r "^0.3.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_image") (r "^0.3.0") (d #t) (k 0)))) (h "0jqfx3zkynrgv7sr3q028sh8743b77i1549a8p44lr945dar8xr5") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gui-0.3.1 (c (n "nstd_gui") (v "0.3.1") (d (list (d (n "nstd_events") (r "^0.3.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_image") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)))) (h "1zl8dvvxf58p3rjl4l15x44p28a5wq7n6ircn7fkn3rgb0hk1b51") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gui-0.3.2 (c (n "nstd_gui") (v "0.3.2") (d (list (d (n "nstd_events") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_image") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)))) (h "1n3akhzsplz4kzxi92d05ra3yk15vf13k94i7wzri20ic0ygl1ba") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gui-0.3.3 (c (n "nstd_gui") (v "0.3.3") (d (list (d (n "nstd_events") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_image") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)))) (h "0mlw8s2sf7wi4nb8wjs9gifxkbaqf6v711z45g1filf38dddiw4k") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gui-0.3.4 (c (n "nstd_gui") (v "0.3.4") (d (list (d (n "nstd_events") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_image") (r "^0.3.3") (f (quote ("deps"))) (d #t) (k 0)))) (h "0hxq42mnjgxd00fgdj29wf61ykhd0c7b52ksxykigbkygwq1iy6z") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gui-0.4.0 (c (n "nstd_gui") (v "0.4.0") (d (list (d (n "nstd_events") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_image") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "0byhcqr0flm1jc39y1q0i92wmpzi9fk0rmw9bs18mzx6rykkz5vs") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gui-0.5.0 (c (n "nstd_gui") (v "0.5.0") (d (list (d (n "nstd_events") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_image") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "0ybfa95w75i6597ghbpykzdv5jqydmfl29pfmczv531j96rc60d7") (f (quote (("deps") ("clib"))))))

