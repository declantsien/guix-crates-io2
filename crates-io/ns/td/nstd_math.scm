(define-module (crates-io ns td nstd_math) #:use-module (crates-io))

(define-public crate-nstd_math-0.1.0 (c (n "nstd_math") (v "0.1.0") (h "0r7in6nh0qqwcmk3sd9r4qkhy3ndw382i3x84dkl8677rwhvii9r")))

(define-public crate-nstd_math-0.2.0 (c (n "nstd_math") (v "0.2.0") (h "0a00qd4xchkcnybd70cyhlfx4i3nxsg8ava5mn0r45lkrc2b3nbg") (f (quote (("clib"))))))

(define-public crate-nstd_math-0.3.0 (c (n "nstd_math") (v "0.3.0") (h "1457w5ywiah1ic06jg58qw51vz5kkdcwm092g30bp3xaila97wzp") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_math-0.4.0 (c (n "nstd_math") (v "0.4.0") (h "0px7dkfz5iq8n2ny7kfb5sbh0i9cm4b4syv07blb46vlvczpvknq") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_math-0.5.0 (c (n "nstd_math") (v "0.5.0") (h "02nmmkxhjsvxcvz50cvq3ji2cf27afvivr1zdd22x3v4zk6xgik6") (f (quote (("deps") ("clib"))))))

