(define-module (crates-io ns td nstd_events) #:use-module (crates-io))

(define-public crate-nstd_events-0.1.0 (c (n "nstd_events") (v "0.1.0") (d (list (d (n "nstd_input") (r "^0.1.0") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "11wa6l41vmwcsnci438jkczhwbxrp7xdq3jxipnd1nmns73ra6yc")))

(define-public crate-nstd_events-0.2.0 (c (n "nstd_events") (v "0.2.0") (d (list (d (n "nstd_input") (r "^0.2.0") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "0mr937148v3fdcwnk3fhkpxz2iaywdmjm566xyb7zdqj84ngm5vy") (f (quote (("clib"))))))

(define-public crate-nstd_events-0.2.1 (c (n "nstd_events") (v "0.2.1") (d (list (d (n "nstd_input") (r "^0.2.1") (d #t) (k 0)) (d (n "winit") (r "^0.26.0") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.11.0") (d #t) (k 0)))) (h "0q3c8hzvpipx3qp8s3qcik7qd0qbr01x8yv21g5dkjpdvsj4hbvv") (f (quote (("clib"))))))

(define-public crate-nstd_events-0.3.0 (c (n "nstd_events") (v "0.3.0") (d (list (d (n "nstd_input") (r "^0.3.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "13kg5g8x66zcn6z3gfhayrxq2mp2kr0s9gi5p9sl5xqc7vhlh9zp") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_events-0.3.1 (c (n "nstd_events") (v "0.3.1") (d (list (d (n "nstd_input") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)))) (h "0l9q8wrzxvb7x8sjj9yh7b0b50kfpbvn4v48idxcg26zigi0grzn") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_events-0.3.2 (c (n "nstd_events") (v "0.3.2") (d (list (d (n "nstd_input") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)))) (h "0xja8y9hqi4whv1wgvw5gr60ywikvhn2x7pjd9ng3m4vz7zkhl8z") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_events-0.4.0 (c (n "nstd_events") (v "0.4.0") (d (list (d (n "nstd_input") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "0yl3s96apmkn8nvnshk19l320p9sg3k01p456mllivga61xsxrcl") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_events-0.5.0 (c (n "nstd_events") (v "0.5.0") (d (list (d (n "nstd_input") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "02nrpps0vr6f4w2sikf1mbarlacgnhwvpcllkld239vz254cbnb6") (f (quote (("deps") ("clib"))))))

