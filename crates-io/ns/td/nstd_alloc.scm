(define-module (crates-io ns td nstd_alloc) #:use-module (crates-io))

(define-public crate-nstd_alloc-0.1.0 (c (n "nstd_alloc") (v "0.1.0") (h "0szlxx06x2b03mflqa5hn1cbgn0qqbidl1nbi1bnnwsc3zq8js1a")))

(define-public crate-nstd_alloc-0.2.0 (c (n "nstd_alloc") (v "0.2.0") (h "04y88kvrixm4pppjng8hgqhimzjsdjkdj3yvq4wlv6chvkwqnjdm") (f (quote (("clib"))))))

(define-public crate-nstd_alloc-0.3.0 (c (n "nstd_alloc") (v "0.3.0") (d (list (d (n "nstd_core") (r "^0.3.0") (d #t) (k 0)))) (h "0wp9pifp5glmn72l5ps83vgdp4lwchz4ss93chmp36mypfzqzz0w") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_alloc-0.3.1 (c (n "nstd_alloc") (v "0.3.1") (d (list (d (n "nstd_core") (r "^0.3.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "0kp81r08crkd6q1i4vfca116hb5ws81z9sqslnvz2ms1l808xb0j") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_alloc-0.3.2 (c (n "nstd_alloc") (v "0.3.2") (d (list (d (n "core-foundation") (r "^0.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nstd_core") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)) (d (n "windows") (r "^0.29.0") (f (quote ("Win32_Foundation" "Win32_System_Memory"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1wxx2l0jh4ny4jip5548amy2lyrfrjfv1smb5wry1g0nsfp3gsy0") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_alloc-0.3.3 (c (n "nstd_alloc") (v "0.3.3") (d (list (d (n "core-foundation") (r "^0.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nstd_core") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)) (d (n "windows") (r "^0.29.0") (f (quote ("Win32_Foundation" "Win32_System_Memory"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "04wjl8kj46glxj27jbr3yb75j1rf45h2gvc2r1757qg5bglvb8ms") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_alloc-0.4.0 (c (n "nstd_alloc") (v "0.4.0") (d (list (d (n "core-foundation") (r "^0.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nstd_core") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "windows") (r "^0.30.0") (f (quote ("Win32_Foundation" "Win32_System_Memory"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0izj4h8c32hlwhvaa30i9b07sxyhm9nbb0ibfqzyby1y340xjlgc") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_alloc-0.5.0 (c (n "nstd_alloc") (v "0.5.0") (d (list (d (n "core-foundation") (r "^0.9.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.112") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nstd_core") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "windows") (r "^0.30.0") (f (quote ("Win32_Foundation" "Win32_System_Memory"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0c65nqas1abn5ng1adra00cjdiz821gzyjq31i5i57zjb6y13qy2") (f (quote (("deps") ("clib"))))))

