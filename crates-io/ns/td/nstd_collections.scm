(define-module (crates-io ns td nstd_collections) #:use-module (crates-io))

(define-public crate-nstd_collections-0.1.0 (c (n "nstd_collections") (v "0.1.0") (d (list (d (n "nstd_alloc") (r "^0.1.0") (d #t) (k 0)))) (h "16dpmcwxbvwaj2qjkpqqhvxwpn1l85cls0c0m7f9rqknzi2pddhc")))

(define-public crate-nstd_collections-0.1.1 (c (n "nstd_collections") (v "0.1.1") (d (list (d (n "nstd_alloc") (r "^0.1.0") (d #t) (k 0)))) (h "1xg25z74lccbiw86ln6b9lg3gfa3rfwig74cpmad5c5rnsvywz23")))

(define-public crate-nstd_collections-0.2.0 (c (n "nstd_collections") (v "0.2.0") (d (list (d (n "nstd_alloc") (r "^0.2.0") (d #t) (k 0)) (d (n "nstd_core") (r "^0.2.0") (d #t) (k 0)))) (h "1d6mbgr6699i7x16pzncx41w84i4mm3kn6jzgyylgp429573g4w0") (f (quote (("clib"))))))

(define-public crate-nstd_collections-0.2.1 (c (n "nstd_collections") (v "0.2.1") (d (list (d (n "nstd_alloc") (r "^0.2.0") (d #t) (k 0)) (d (n "nstd_core") (r "^0.2.2") (d #t) (k 0)))) (h "1i2qcb2lszlgaxkrnn0i6b1mhhjlsf4al9i2833wjrvbmv4l3gl5") (f (quote (("clib"))))))

(define-public crate-nstd_collections-0.3.0 (c (n "nstd_collections") (v "0.3.0") (d (list (d (n "nstd_alloc") (r "^0.3.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "1sgnxa2rz2fz4xl103y8ia88n7ij0yyz61qz24y7bl9xr9dkdn7g") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_collections-0.3.1 (c (n "nstd_collections") (v "0.3.1") (d (list (d (n "nstd_alloc") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)))) (h "13r8s222w8cyfdzzgha0n43h33imxrj52vv1sb18bc1p5hfk2v26") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_collections-0.3.2 (c (n "nstd_collections") (v "0.3.2") (d (list (d (n "nstd_alloc") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)))) (h "1qd1vic2qf2dxsa5gws6wn7v1p8mvinh7k3zvrya0s2ada08sr4x") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_collections-0.3.3 (c (n "nstd_collections") (v "0.3.3") (d (list (d (n "nstd_alloc") (r "^0.3.3") (f (quote ("deps"))) (d #t) (k 0)))) (h "110wgywh4s8pgir7f9kp3633mqavp14drijl8lmbpvydaypjp35y") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_collections-0.4.0 (c (n "nstd_collections") (v "0.4.0") (d (list (d (n "nstd_alloc") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "1d1hcwsw656yr0cy3462gf534vg9b25lxf9pi5vrk8sr8s21cswg") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_collections-0.5.0 (c (n "nstd_collections") (v "0.5.0") (d (list (d (n "nstd_alloc") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "17amar4xa94gkg15807ixwhv40zbgb7hngzy984gl7vr1vd86p95") (f (quote (("deps") ("clib"))))))

