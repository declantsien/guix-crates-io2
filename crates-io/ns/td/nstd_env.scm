(define-module (crates-io ns td nstd_env) #:use-module (crates-io))

(define-public crate-nstd_env-0.1.0 (c (n "nstd_env") (v "0.1.0") (d (list (d (n "nstd_collections") (r "^0.1.0") (d #t) (k 0)))) (h "023zrjlw867w4skkxbf5qb6i01azhzk0fhqiyx05h37ng3mf8rxf")))

(define-public crate-nstd_env-0.2.0 (c (n "nstd_env") (v "0.2.0") (d (list (d (n "nstd_collections") (r "^0.2.0") (d #t) (k 0)))) (h "123g3mywrs1k2i5cgjzgiv01kkw3klr7lkxm6h0rgzdim0z45r5l") (f (quote (("clib"))))))

(define-public crate-nstd_env-0.3.0 (c (n "nstd_env") (v "0.3.0") (d (list (d (n "nstd_collections") (r "^0.3.0") (d #t) (k 0)))) (h "0rw8xwsp6nk0jhvywg7pvj6vycg4pd3xsn8rbpp3sjyh5x97dfbm") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_env-0.3.1 (c (n "nstd_env") (v "0.3.1") (d (list (d (n "nstd_collections") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)))) (h "1absyh466mizlnpg664xkyfjczp6ljghwsqilv8ff2cip1gaa7gd") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_env-0.3.2 (c (n "nstd_env") (v "0.3.2") (d (list (d (n "nstd_collections") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)))) (h "0c5dw23zg73bn7lfm81gg472nz3nw31nljn1wz2p2g93g4lf67jm") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_env-0.3.3 (c (n "nstd_env") (v "0.3.3") (d (list (d (n "nstd_collections") (r "^0.3.3") (f (quote ("deps"))) (d #t) (k 0)))) (h "12pawcblni452a99dpjvgrbqp45sd8gripx9xrq480x074ldn9rp") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_env-0.4.0 (c (n "nstd_env") (v "0.4.0") (d (list (d (n "nstd_collections") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "0izwy8gvp06wbwd1ia5v1h49bs8mlklhg66n5imdk1cyj98mrz43") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_env-0.5.0 (c (n "nstd_env") (v "0.5.0") (d (list (d (n "nstd_collections") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "1qbsyxwlj1z3y118n9zm9lfyxh0dh1zck27whyzpd6xism3qv5dm") (f (quote (("deps") ("clib"))))))

