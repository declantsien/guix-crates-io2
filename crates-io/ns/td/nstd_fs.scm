(define-module (crates-io ns td nstd_fs) #:use-module (crates-io))

(define-public crate-nstd_fs-0.1.0 (c (n "nstd_fs") (v "0.1.0") (d (list (d (n "nstd_collections") (r "^0.1.0") (d #t) (k 0)))) (h "06dfyl9bg1i6n076hlnhrip478px0ydkikqaydwrr6kddgkvcd0z")))

(define-public crate-nstd_fs-0.2.0 (c (n "nstd_fs") (v "0.2.0") (d (list (d (n "nstd_collections") (r "^0.2.0") (d #t) (k 0)))) (h "0gki47r5m8zb3ynrgvxzn0vidmccc5v0jlmz1lnx4fq6hsq6mcnj") (f (quote (("clib"))))))

(define-public crate-nstd_fs-0.3.0 (c (n "nstd_fs") (v "0.3.0") (d (list (d (n "nstd_collections") (r "^0.3.0") (d #t) (k 0)))) (h "1by3hhqkmynhasp6vqy4s0634r602d866fzfgfcvckzxr7l2kvqb") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_fs-0.3.1 (c (n "nstd_fs") (v "0.3.1") (d (list (d (n "nstd_collections") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)))) (h "0i9n4v71aiwnyzlwxrll8pp9vdcdy3bcb1f3nl1hns4chgyh266m") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_fs-0.3.2 (c (n "nstd_fs") (v "0.3.2") (d (list (d (n "nstd_collections") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)))) (h "02frx550j7yz66w5y11d538821bfb24pfms9b7njblvjsvhswx0f") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_fs-0.3.3 (c (n "nstd_fs") (v "0.3.3") (d (list (d (n "nstd_collections") (r "^0.3.3") (f (quote ("deps"))) (d #t) (k 0)))) (h "0p79pm14pkd5cn86j74rqcln18j4nzawyjac4b133qa87ssnrkvq") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_fs-0.4.0 (c (n "nstd_fs") (v "0.4.0") (d (list (d (n "nstd_collections") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "1404yf21jxmgf1s5ab96x1ya1isbfdp0a1wipphp3dsv2f84kv5g") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_fs-0.5.0 (c (n "nstd_fs") (v "0.5.0") (d (list (d (n "nstd_collections") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "1rks9brvkrwj5x1yi0fw4zfcw6lwv0r031cwxgxsq8dh527amyzl") (f (quote (("deps") ("clib"))))))

