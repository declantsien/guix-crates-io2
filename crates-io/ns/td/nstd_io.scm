(define-module (crates-io ns td nstd_io) #:use-module (crates-io))

(define-public crate-nstd_io-0.1.0 (c (n "nstd_io") (v "0.1.0") (h "155q66yx8apscla8k32iz1n8blprfsvq2j5k4gmr73h3qj3arwvr")))

(define-public crate-nstd_io-0.2.0 (c (n "nstd_io") (v "0.2.0") (h "11rcqhs8875l3z0jnz6ifh4mgjxq8bz851n0hd73nh1qrkp29gzi") (f (quote (("clib"))))))

(define-public crate-nstd_io-0.2.1 (c (n "nstd_io") (v "0.2.1") (d (list (d (n "nstd_core") (r "^0.2.3") (d #t) (k 0)))) (h "13gpn7arlarwdvw8js0az2h57kgljfz0l4m855a1dynn5jvhkaf9") (f (quote (("clib"))))))

(define-public crate-nstd_io-0.3.0 (c (n "nstd_io") (v "0.3.0") (d (list (d (n "nstd_core") (r "^0.3.0") (d #t) (k 0)))) (h "0ax8xfxbgakb1i936yjddfqp1vczs6mxmxpibix3mz59zcdziq64") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_io-0.3.1 (c (n "nstd_io") (v "0.3.1") (d (list (d (n "nstd_core") (r "^0.3.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "0vappvr55msgq1yxb3qc4mwgmny406wkg9pricqqv9dpc8cd16qr") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_io-0.3.2 (c (n "nstd_io") (v "0.3.2") (d (list (d (n "nstd_core") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)))) (h "1vb46x5lnhg7xlyw9fmidm1chdgpqvm6ka5140k970nkncrlh521") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_io-0.3.3 (c (n "nstd_io") (v "0.3.3") (d (list (d (n "nstd_core") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)))) (h "0yvrnghrd5l3nw8qds27a7qs1bny11pcmrgcpbwrwghgpwph7k82") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_io-0.4.0 (c (n "nstd_io") (v "0.4.0") (d (list (d (n "nstd_core") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "0f4qhmg4mkjmafz19a9n7fa0xz70nyyhnjj8xnirxbn8fwl05jkv") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_io-0.5.0 (c (n "nstd_io") (v "0.5.0") (d (list (d (n "nstd_core") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "1nh3f5dbdbpwfs4civicxdq12i0vvp24h2cm3fzixcwg2qjv5fby") (f (quote (("deps") ("clib"))))))

