(define-module (crates-io ns td nstd_gl) #:use-module (crates-io))

(define-public crate-nstd_gl-0.1.0 (c (n "nstd_gl") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "nstd_collections") (r "^0.1.0") (d #t) (k 0)) (d (n "nstd_gui") (r "^0.1.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.11.0") (f (quote ("spirv"))) (d #t) (k 0)))) (h "1byj5nskp5dp5waki9nh5929rpsvz2jdnqg77i00lmb7qi2bqjvc")))

(define-public crate-nstd_gl-0.1.1 (c (n "nstd_gl") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "nstd_core") (r "^0.1.3") (d #t) (k 0)) (d (n "nstd_gui") (r "^0.1.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.11.0") (f (quote ("spirv"))) (d #t) (k 0)))) (h "1wl9vmz83fg9y05rnd3jlc1wq2s6g1ld11lxczl2k1pgvyb4i0wn")))

(define-public crate-nstd_gl-0.2.0 (c (n "nstd_gl") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "nstd_core") (r "^0.2.0") (d #t) (k 0)) (d (n "nstd_gui") (r "^0.2.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.11.0") (f (quote ("spirv"))) (d #t) (k 0)))) (h "18lm1n1wcdz9qhzm01rp5il52pn2py0awi2bjf0rr1svwrn9i75b") (f (quote (("clib"))))))

(define-public crate-nstd_gl-0.2.1 (c (n "nstd_gl") (v "0.2.1") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "nstd_core") (r "^0.2.3") (d #t) (k 0)) (d (n "nstd_gui") (r "^0.2.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.11.1") (f (quote ("spirv"))) (d #t) (k 0)))) (h "0i74nw8ws7j5634m7dpg3ncny4hdhg4dv905rg1fy9xvgy9sw2m1") (f (quote (("clib"))))))

(define-public crate-nstd_gl-0.3.0 (c (n "nstd_gl") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "nstd_core") (r "^0.3.0") (d #t) (k 0)) (d (n "nstd_gui") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.11.1") (f (quote ("spirv"))) (d #t) (k 0)))) (h "0xh11dplhg442fkfnnrhn5gnh6sdzcvhq4xnilgjwricm1c441p1") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gl-0.3.1 (c (n "nstd_gl") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "nstd_core") (r "^0.3.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_gui") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.11.1") (f (quote ("spirv"))) (d #t) (k 0)))) (h "1k0qjs5h8jg8p07fr1qfa9nkq7shv3kxv3kgr70n4bw360a3la4v") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gl-0.3.2 (c (n "nstd_gl") (v "0.3.2") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "nstd_core") (r "^0.3.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_gui") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.11.1") (f (quote ("spirv"))) (d #t) (k 0)))) (h "0r1nh8knsb0j2vs57fx2ln8ylddny2qjx62qkafcgbpsqrs8chz5") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gl-0.3.3 (c (n "nstd_gl") (v "0.3.3") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "nstd_core") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_gui") (r "^0.3.3") (f (quote ("deps"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.11.1") (f (quote ("spirv"))) (d #t) (k 0)))) (h "18ababq8214glz6a7l5mgrbb4ycpspkps7fsk0bmgdqr7qpyq54b") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gl-0.3.4 (c (n "nstd_gl") (v "0.3.4") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "nstd_core") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_gui") (r "^0.3.4") (f (quote ("deps"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (f (quote ("spirv"))) (d #t) (k 0)))) (h "1lpzbx5003q9nrs8gp3rx34pyizqhlflm352idji5cm9ywyg4z80") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gl-0.4.0 (c (n "nstd_gl") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "nstd_core") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "nstd_gui") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (f (quote ("spirv"))) (d #t) (k 0)))) (h "1ybivkd9ys5pk5yvpbk0q2q137ka5rm4zmdm69diqj2l8q9pmxqr") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gl-0.5.0 (c (n "nstd_gl") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "nstd_gui") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (f (quote ("spirv"))) (d #t) (k 0)))) (h "03mnv3k1yy3kjcmmrwh33zq2gs4ph9iappspggdvhiy8fr9lf3bf") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_gl-0.5.1 (c (n "nstd_gl") (v "0.5.1") (d (list (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "nstd_gui") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (f (quote ("spirv"))) (d #t) (k 0)))) (h "0438q5d8rdprz31j5vkac9jqb3q8xk7gdlhhmxmblrjkqdxns9dm") (f (quote (("deps") ("clib"))))))

