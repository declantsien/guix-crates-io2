(define-module (crates-io ns td nstd_audio) #:use-module (crates-io))

(define-public crate-nstd_audio-0.1.0 (c (n "nstd_audio") (v "0.1.0") (d (list (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "nstd_fs") (r "^0.1.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "1jwg6k3sv05nmsfnzfpampkllyvyfk9khbja57ppx8pw5x6l27q1")))

(define-public crate-nstd_audio-0.2.0 (c (n "nstd_audio") (v "0.2.0") (d (list (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "nstd_fs") (r "^0.2.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "1zin06wibsgh0bgcdm938zqk2q79v83fxb6m4dg43kbp414x25cs") (f (quote (("clib"))))))

(define-public crate-nstd_audio-0.3.0 (c (n "nstd_audio") (v "0.3.0") (d (list (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "nstd_fs") (r "^0.3.0") (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "15amwji22f19nl21xhjrkybiwk1hghqxdmk2sf3ypibxpf7i02xg") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_audio-0.3.1 (c (n "nstd_audio") (v "0.3.1") (d (list (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "nstd_fs") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "15812jfi53clpzqxqk8l1s9zh2jamg8yv41bb4vwdqqcl308d4qm") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_audio-0.3.2 (c (n "nstd_audio") (v "0.3.2") (d (list (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "nstd_fs") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "14fbwdc9d9qs57c408zfy4xnxq6z9igg2li2wyq9sqyyzy758liy") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_audio-0.3.3 (c (n "nstd_audio") (v "0.3.3") (d (list (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "nstd_fs") (r "^0.3.3") (f (quote ("deps"))) (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "0kk7bh9rigxvvcsx9k5q1p0669kq7cq18fmmbb9ab3nqd2bf8fc2") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_audio-0.4.0 (c (n "nstd_audio") (v "0.4.0") (d (list (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "nstd_fs") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "rodio") (r "^0.14.0") (d #t) (k 0)))) (h "0mphq8dkhqpjy40kc6j1qsiaplj8wgizqmwll2q5f7iaiiiivpq5") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_audio-0.5.0 (c (n "nstd_audio") (v "0.5.0") (d (list (d (n "cpal") (r "^0.13.4") (d #t) (k 0)) (d (n "nstd_fs") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)) (d (n "rodio") (r "^0.15.0") (d #t) (k 0)))) (h "0g9zf8khby29p1gzl4m6s9q22pidbqly9cqgi0da06y6sn3svkq2") (f (quote (("deps") ("clib"))))))

