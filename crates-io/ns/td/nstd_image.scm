(define-module (crates-io ns td nstd_image) #:use-module (crates-io))

(define-public crate-nstd_image-0.1.0 (c (n "nstd_image") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1m219v3z7z374l7va72cn42njd5wcxwv6rdp60sa3g2wl3w08qd2")))

(define-public crate-nstd_image-0.2.0 (c (n "nstd_image") (v "0.2.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0221g0ggc91vpsqx7z3wx7i1a0bvhgmq8k9944ni3r3r6ngngh4l") (f (quote (("clib"))))))

(define-public crate-nstd_image-0.2.1 (c (n "nstd_image") (v "0.2.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nstd_core") (r "^0.2.3") (d #t) (k 0)))) (h "1aivb8fiz5vn0fa98f20sdvq6qziwdri69f7wnjhysn72r5c145d") (f (quote (("clib"))))))

(define-public crate-nstd_image-0.3.0 (c (n "nstd_image") (v "0.3.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nstd_core") (r "^0.3.0") (d #t) (k 0)))) (h "1ns481b0dy2vh9m179415ybr8zwcagg49cnhqx5916d5zhj1c495") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_image-0.3.1 (c (n "nstd_image") (v "0.3.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nstd_core") (r "^0.3.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "116whjhz1af6xc1p2vljqg3xzh9a2nb98i5bvx3wn05rvhkslc6v") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_image-0.3.2 (c (n "nstd_image") (v "0.3.2") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nstd_core") (r "^0.3.1") (f (quote ("deps"))) (d #t) (k 0)))) (h "1x3hn7af2m51b9wqbwazsp1ibs1armk7gbk7bhmvj3bv0k8c4a5m") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_image-0.3.3 (c (n "nstd_image") (v "0.3.3") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nstd_core") (r "^0.3.2") (f (quote ("deps"))) (d #t) (k 0)))) (h "0qz5nzc0x5gf9ha3kdkni3n12jc9kjc3jwyd3600kwgj082npxx2") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_image-0.4.0 (c (n "nstd_image") (v "0.4.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nstd_core") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "1zfw358xj21pwrj7jp0jj08861xgwql3cpi21v2xc2zha0pzs4cq") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_image-0.5.0 (c (n "nstd_image") (v "0.5.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "nstd_core") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "106z4dsalmfrbyyf5r7dwqf8zd81bv9cs5p2hxd5d7n4m7afslny") (f (quote (("deps") ("clib"))))))

