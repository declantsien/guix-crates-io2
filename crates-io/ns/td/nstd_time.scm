(define-module (crates-io ns td nstd_time) #:use-module (crates-io))

(define-public crate-nstd_time-0.1.0 (c (n "nstd_time") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0hy5jpncmrhxhz3d2amm3lwb1k6sfylz6n6432nq0hpzl5bvpacx")))

(define-public crate-nstd_time-0.2.0 (c (n "nstd_time") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1cg1fshxjyy5n1h5a3kpy1vcyrgqr6p4qdf444nns26z5wksy4g7") (f (quote (("clib"))))))

(define-public crate-nstd_time-0.3.0 (c (n "nstd_time") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0r26qwwf05y6wxvz56b9dgqmjf1v565afbv93frrg6vfriv1pyz0") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_time-0.4.0 (c (n "nstd_time") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1jcwzxv86v6pml8fspx8nwz3p91v392isxx132w662ak0hbnxk16") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_time-0.5.0 (c (n "nstd_time") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0dp1y9vpdih6mmw5vwfrjghgckmw0wjarlwc2vnfijv9mawzlvmw") (f (quote (("deps") ("clib"))))))

