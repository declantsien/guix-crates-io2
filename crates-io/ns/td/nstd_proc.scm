(define-module (crates-io ns td nstd_proc) #:use-module (crates-io))

(define-public crate-nstd_proc-0.1.0 (c (n "nstd_proc") (v "0.1.0") (h "081fj2hm609vll7bnrplqi0jcfkvcfc19s7f53l5g44p89874wdk")))

(define-public crate-nstd_proc-0.2.0 (c (n "nstd_proc") (v "0.2.0") (h "021391z4614bnazhnklk913h1j1y4kr7c2byqai7yjpzzqscxm0m") (f (quote (("clib"))))))

(define-public crate-nstd_proc-0.3.0 (c (n "nstd_proc") (v "0.3.0") (h "04g17ibpy56hsgbrw2pfymsz2h623k70l5fh8jf4zybk4r6sns36") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_proc-0.4.0 (c (n "nstd_proc") (v "0.4.0") (h "068ai9q5c08waj41mc79s29wnh7zlh1smxl9kjbda48chkfy92sd") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_proc-0.5.0 (c (n "nstd_proc") (v "0.5.0") (h "05bsql05py27fkvv3iq7iphmlmkhkg4ijfmqsqyhdg42g39xwgla") (f (quote (("deps") ("clib"))))))

