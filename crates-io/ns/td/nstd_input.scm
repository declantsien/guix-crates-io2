(define-module (crates-io ns td nstd_input) #:use-module (crates-io))

(define-public crate-nstd_input-0.1.0 (c (n "nstd_input") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "0m1ga3vn02r1l1jhg3cdblm2d15zw3a7g29575aacrc81hms94n4")))

(define-public crate-nstd_input-0.2.0 (c (n "nstd_input") (v "0.2.0") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "09zlpx0q48rldwjqz4jc75y4rhjdfhxq1471w9ckmnadpqly93db") (f (quote (("clib"))))))

(define-public crate-nstd_input-0.2.1 (c (n "nstd_input") (v "0.2.1") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "winit") (r "^0.26.0") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.11.0") (d #t) (k 0)))) (h "11a426ccvbgh0gp2faqyfiypykjsan17idarvbflajfnv1yhj6rp") (f (quote (("clib"))))))

(define-public crate-nstd_input-0.3.0 (c (n "nstd_input") (v "0.3.0") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "winit") (r "^0.26.0") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.11.0") (d #t) (k 0)))) (h "0fanvsr42hpfy9ag4z07cvmx1igvyzhg8b9g4qdxfc4jllp5h2zb") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_input-0.3.1 (c (n "nstd_input") (v "0.3.1") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.10.0") (d #t) (k 0)))) (h "15vchp2fm4pjv8423b2k1lj12xh5z12hj7r50x3zhv0xvqmqs1q8") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_input-0.3.2 (c (n "nstd_input") (v "0.3.2") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.11.0") (d #t) (k 0)))) (h "13mzs0lzrm6dfs5m0hc26d8cghnrfkx8iv98vqnr451fwh02k8np") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_input-0.4.0 (c (n "nstd_input") (v "0.4.0") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.11.0") (d #t) (k 0)))) (h "05pi2vjgph8w2sds4k3f5syv6hi6immmq1n22rwnq48s8nrafgn2") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_input-0.5.0 (c (n "nstd_input") (v "0.5.0") (d (list (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.11.0") (d #t) (k 0)))) (h "158xcli2c9c57jk59yyymx4rbhqjykng3dqszp45093fjkzbf9j7") (f (quote (("deps") ("clib"))))))

