(define-module (crates-io ns td nstd_os) #:use-module (crates-io))

(define-public crate-nstd_os-0.1.0 (c (n "nstd_os") (v "0.1.0") (d (list (d (n "platforms") (r "^1.1.0") (d #t) (k 0)))) (h "184dylpdz0dvkn19b8h20kfjv250lm01wizvr83zp7j4365gf49f")))

(define-public crate-nstd_os-0.2.0 (c (n "nstd_os") (v "0.2.0") (h "1x52idpcbh5vy22kw1dhd042mgjxm35wrm287vbwb8ln1iyxsn3r") (f (quote (("clib"))))))

(define-public crate-nstd_os-0.3.0 (c (n "nstd_os") (v "0.3.0") (h "0v07gpc4cl565y8q5n2qvsmw70833g25bapkgw13ssmnvffk0pq6") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_os-0.4.0 (c (n "nstd_os") (v "0.4.0") (h "1n2w0w8n3mbgnyzi27baw3w2kjfdfcr0cvvyn4f941mhxnccf8ps") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_os-0.5.0 (c (n "nstd_os") (v "0.5.0") (h "1zfkg60sbqi5773qajdcmggqhc5walrcfkcx6idvkjmv26kbhzhg") (f (quote (("deps") ("clib"))))))

