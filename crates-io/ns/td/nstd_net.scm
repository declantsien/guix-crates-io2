(define-module (crates-io ns td nstd_net) #:use-module (crates-io))

(define-public crate-nstd_net-0.1.0 (c (n "nstd_net") (v "0.1.0") (h "1xyz5h9f3k05yf0hc7h39lsjvnylkkxrfkafkjrx8nri6ly80hk7")))

(define-public crate-nstd_net-0.2.0 (c (n "nstd_net") (v "0.2.0") (h "1imh7nhmikzai9zxg90w84zwwfhs159zji1las1akkzg7n11ss84") (f (quote (("clib"))))))

(define-public crate-nstd_net-0.3.0 (c (n "nstd_net") (v "0.3.0") (h "021lzwh6p6q4riv47wrn8hcixca3r4r7v7c3ay5hcgs04c2kwk2l") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_net-0.4.0 (c (n "nstd_net") (v "0.4.0") (h "05xkm5y2i8yk758w34arxbdw5p0sj873rz6b0045lp9c275j9dd4") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_net-0.5.0 (c (n "nstd_net") (v "0.5.0") (h "1rb5n7gxwky9rmgj0mhyjffqxzpj7gf13faqimwa89qqjsplxjg9") (f (quote (("deps") ("clib"))))))

