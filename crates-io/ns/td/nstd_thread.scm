(define-module (crates-io ns td nstd_thread) #:use-module (crates-io))

(define-public crate-nstd_thread-0.1.0 (c (n "nstd_thread") (v "0.1.0") (h "0i8qkjq44m04s1hkp4n0bfmrkplk52460v58wd25r33sl19jzdwf")))

(define-public crate-nstd_thread-0.2.0 (c (n "nstd_thread") (v "0.2.0") (h "1sc2x3b1pq5wapcvzphb2dsdjrxq3s4yjfbwn3s6pcqnsbi7n905") (f (quote (("clib"))))))

(define-public crate-nstd_thread-0.3.0 (c (n "nstd_thread") (v "0.3.0") (h "0ld0i5slkd4dwlhyxr8a394qb4ggnvc9vjjq7l8jiwimxz8gviga") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_thread-0.4.0 (c (n "nstd_thread") (v "0.4.0") (h "0rbcjb7vl7fhn0ivxbicx43f9v5w6rf2bzjazazfy35n6p6w51sw") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_thread-0.5.0 (c (n "nstd_thread") (v "0.5.0") (h "094rcqr76hn96sm2pklx3wfwi9gdycyq6r447bpl7f0y5qws5qyx") (f (quote (("deps") ("clib"))))))

