(define-module (crates-io ns td nstdapi) #:use-module (crates-io))

(define-public crate-nstdapi-0.1.0 (c (n "nstdapi") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (o #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (o #t) (k 0)))) (h "0wp5k8mpsq2ch2rj98bixl738jz3m2nzml0bbw40wd8kcpzl04f3") (f (quote (("capi" "quote" "syn"))))))

(define-public crate-nstdapi-0.1.1 (c (n "nstdapi") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (o #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (o #t) (k 0)))) (h "18c6wisxa6whzgdn22x72nmn1n3mxyfjr3x0hv3l4aqqxb13kd51") (f (quote (("capi" "quote" "syn"))))))

(define-public crate-nstdapi-0.2.0 (c (n "nstdapi") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "printing" "proc-macro"))) (o #t) (k 0)))) (h "13ywd0g65psk87223hr09f1zakkkrdppp5s8kvwpj57lm1yw2fy8") (f (quote (("link" "capi" "quote" "syn") ("capi" "quote" "syn"))))))

