(define-module (crates-io ns td nstd_str) #:use-module (crates-io))

(define-public crate-nstd_str-0.1.0 (c (n "nstd_str") (v "0.1.0") (h "0c6rag79v78awhbnv77cq6lg63300g6llwz0iszqqcihwim13zkw")))

(define-public crate-nstd_str-0.2.0 (c (n "nstd_str") (v "0.2.0") (h "00k7il9af260kb4a69s70r5paa4fxnpwl2vl2k6s5akhg31ln39z") (f (quote (("clib"))))))

(define-public crate-nstd_str-0.3.0 (c (n "nstd_str") (v "0.3.0") (h "02xzdrws0ip4g5bf5kznlmvzfyncz93f2pkdg4m90v5k195gfxry") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_str-0.3.1 (c (n "nstd_str") (v "0.3.1") (d (list (d (n "nstd_collections") (r "^0.3.3") (f (quote ("deps"))) (d #t) (k 0)))) (h "09zs996rwqa2w6686ivny6wpzbv8imasxmly7kfd6ahpcsg57kl1") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_str-0.4.0 (c (n "nstd_str") (v "0.4.0") (d (list (d (n "nstd_collections") (r "^0.4.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "0q59gasm334as73b8rd17hnlmlpn7jdpia2xpb63ny7j3ffay01x") (f (quote (("deps") ("clib"))))))

(define-public crate-nstd_str-0.5.0 (c (n "nstd_str") (v "0.5.0") (d (list (d (n "nstd_collections") (r "^0.5.0") (f (quote ("deps"))) (d #t) (k 0)))) (h "0s1cgqhghm6rinjv7n8bmjvn11h5l72byflhhxyjrxdiayadzkz7") (f (quote (("deps") ("clib"))))))

