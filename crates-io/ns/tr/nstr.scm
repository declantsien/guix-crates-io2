(define-module (crates-io ns tr nstr) #:use-module (crates-io))

(define-public crate-nstr-0.1.0 (c (n "nstr") (v "0.1.0") (h "1x1mkfi21gzz9nqh1jw42n5znl0zzl0xk7mkxqlyk7pffvnivz6h")))

(define-public crate-nstr-0.2.0 (c (n "nstr") (v "0.2.0") (h "1ds20xarwpjcrab64jnc8fqjl4lyww5860pfwjs9qcx5m441w815")))

(define-public crate-nstr-0.3.0 (c (n "nstr") (v "0.3.0") (h "07xb4y0g1d7d58vv9va5vmph1rq4x1q873vsxrj7x17sdfzms6x5")))

(define-public crate-nstr-0.3.1 (c (n "nstr") (v "0.3.1") (h "0gbv0cc6nraq63sr2vga5kpqf980s85llj97r8n78dxgvr9cl1r1")))

(define-public crate-nstr-0.3.2 (c (n "nstr") (v "0.3.2") (h "0pizzbl5pvgbsqcmp0mrn5yaa9nmpdw869r8bfvwpnlg4dw2vc4v")))

