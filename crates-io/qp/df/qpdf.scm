(define-module (crates-io qp df qpdf) #:use-module (crates-io))

(define-public crate-qpdf-0.1.0 (c (n "qpdf") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.1") (d #t) (k 0)))) (h "00f8rczn772bzjp6v6yzr5nqsky646swk2vyl4p5hajhkvwd2h0n")))

(define-public crate-qpdf-0.1.1 (c (n "qpdf") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.1") (d #t) (k 0)))) (h "1zgaykf8q6825f3aidd9pkg77wwrlk1b2kb9pksfjm5d8n9p1pdp")))

(define-public crate-qpdf-0.1.2 (c (n "qpdf") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.1") (d #t) (k 0)))) (h "0davbih494abkym280jc2i4n821kh9i7gqvhri91m9bxrca1iyig")))

(define-public crate-qpdf-0.1.3 (c (n "qpdf") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.1") (d #t) (k 0)))) (h "01lsa66ffs5w7q3x9a5zqfl44fbgpiyy542qdm4nbc5gma70d65a")))

(define-public crate-qpdf-0.1.4 (c (n "qpdf") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.1") (d #t) (k 0)))) (h "119a4h43afhn7mdz4rq4ga2zmxr8jd2hmr0j8rji0ij8j3bz2gql") (y #t)))

(define-public crate-qpdf-0.1.5 (c (n "qpdf") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.1") (d #t) (k 0)))) (h "08qcdpxm6x09z54q06sp5hfdkagmwkcdjm2583x5samc2x6zspch")))

(define-public crate-qpdf-0.1.6 (c (n "qpdf") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.1") (d #t) (k 0)))) (h "1bsinhns8183fjwwz4aqz9343sbha3k5s6bjbszl17sihfgdaf7z")))

(define-public crate-qpdf-0.1.7 (c (n "qpdf") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.1") (d #t) (k 0)))) (h "0adibgr682pp47jf2nnry3gw4xfbnzsbzpcvpb4kyqdf8j9snvrd")))

(define-public crate-qpdf-0.2.0 (c (n "qpdf") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.2") (d #t) (k 0)))) (h "1f7w9jk2d4wc3afix9xd3j2myhrhyajrmyhjz59nd2vshk354fys") (f (quote (("vendored" "qpdf-sys/vendored") ("legacy"))))))

(define-public crate-qpdf-0.2.1 (c (n "qpdf") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.2") (d #t) (k 0)))) (h "091my2iqf4ql38xlv7b98jg8r52mn4539wmm091whin4q1bbvbb8") (f (quote (("vendored" "qpdf-sys/vendored") ("legacy"))))))

(define-public crate-qpdf-0.3.0 (c (n "qpdf") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.3") (d #t) (k 0)))) (h "0s8fyxas04xs5p1c1nd9hv4psnc3y0v4iikidglg40wgc97m5ml0") (f (quote (("vendored" "qpdf-sys/vendored") ("legacy"))))))

(define-public crate-qpdf-0.3.1 (c (n "qpdf") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "qpdf-sys") (r "^0.3") (d #t) (k 0)))) (h "042illdb4ljjigq426hfsp16c40cyvfkdigd0p7ii9wfkcwq35aq") (f (quote (("vendored" "qpdf-sys/vendored") ("legacy"))))))

