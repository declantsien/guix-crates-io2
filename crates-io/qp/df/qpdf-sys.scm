(define-module (crates-io qp df qpdf-sys) #:use-module (crates-io))

(define-public crate-qpdf-sys-0.1.0 (c (n "qpdf-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0khdavv3r8s96042b2zj83iims6c98f0q8q4x52i5i5yd2m3bcxp")))

(define-public crate-qpdf-sys-0.1.1 (c (n "qpdf-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1gh00yw6sxgck1my4acbs5p0hv1k3z3sfd2d7hgrkxnyzim127l9")))

(define-public crate-qpdf-sys-0.1.2 (c (n "qpdf-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1lzfwxfzgg26cr4jpc84zk5f644xdklsw1dric2msy23wvjxbllr")))

(define-public crate-qpdf-sys-0.1.3 (c (n "qpdf-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "08rwd4w9p55m7723g3d4flmkyz7wahvcs7pr59nvsj0yb3x30x8k")))

(define-public crate-qpdf-sys-0.1.4 (c (n "qpdf-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0qsikcs6g5rrmpcwc8zw4y9yl58lxrvl0gfjwwbhc0zgsvapqgm2") (y #t)))

(define-public crate-qpdf-sys-0.1.5 (c (n "qpdf-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1a0zicf8k7l98riprx9i5qw4rbyncnq3nj775q2da863i75jbq6d")))

(define-public crate-qpdf-sys-0.1.6 (c (n "qpdf-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "110l3w90898kfd6j6ykwsm1njbx9m47yhjkawadryfkzhqxdqms0")))

(define-public crate-qpdf-sys-0.1.7 (c (n "qpdf-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)))) (h "18yxs9i0z8hs5mjncdxhax2va6c4ic11dyshnl63hl9hz7vw6ix1")))

(define-public crate-qpdf-sys-0.2.0 (c (n "qpdf-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1i8wxy0hi3iqq2d0pwrmcwx2cfsx27lq2svf9w1ivsd5ggnz4n57") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-qpdf-sys-0.2.1 (c (n "qpdf-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1v1ppcqczp3hwrp8jfwydp0jyhwhw5mffsmhbd9705dndbj0cndn") (s 2) (e (quote (("vendored" "dep:cc"))))))

(define-public crate-qpdf-sys-0.3.0 (c (n "qpdf-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rkx0362v2qwbxvbcjiv2ys03469fjs9gacxkkf5iswcqcv1gzcp") (s 2) (e (quote (("vendored" "dep:cc"))))))

