(define-module (crates-io qp -p qp-postgres) #:use-module (crates-io))

(define-public crate-qp-postgres-0.1.0 (c (n "qp-postgres") (v "0.1.0") (d (list (d (n "qp") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-postgres") (r "^0.7.5") (d #t) (k 0)))) (h "08b880hmqv9kvnxn4l46iq6z3jxbs6k6qar0fcjx9ynalga5gj50") (r "1.56")))

(define-public crate-qp-postgres-0.1.1 (c (n "qp-postgres") (v "0.1.1") (d (list (d (n "qp") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-postgres") (r "^0.7.5") (d #t) (k 0)))) (h "10f8c4ndp58fslxl150kfi3l45mfshxs9ndhw8s78xpajbnm8amb") (r "1.56")))

