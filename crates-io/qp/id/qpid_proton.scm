(define-module (crates-io qp id qpid_proton) #:use-module (crates-io))

(define-public crate-qpid_proton-0.0.0 (c (n "qpid_proton") (v "0.0.0") (h "00w4xlwysr5764pvmmyrjsdn95v6aymrbjcmj1z5r3hpp9p2m7dn")))

(define-public crate-qpid_proton-0.0.1 (c (n "qpid_proton") (v "0.0.1") (h "1pdhf6x1v5s8ja59q452dr71rb3jmj2frwsqm6xjgiclxbzrhhpp")))

(define-public crate-qpid_proton-0.0.2 (c (n "qpid_proton") (v "0.0.2") (d (list (d (n "cmake") (r ">=0.1.0, <0.2.0") (d #t) (k 1)) (d (n "uuid") (r ">=0.8.0, <0.9.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rjy6dh31ipi1x06n1sryvv52448aszrbyyna1wvn44ldb0bh7ig")))

(define-public crate-qpid_proton-0.0.3 (c (n "qpid_proton") (v "0.0.3") (d (list (d (n "cmake") (r ">=0.1.0, <0.2.0") (d #t) (k 1)) (d (n "uuid") (r ">=0.8.0, <0.9.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "09sq73zgy2lfwki78mg4cdw3h0pd8g06k3rv0dsljid43l9j2dk8")))

(define-public crate-qpid_proton-0.0.4 (c (n "qpid_proton") (v "0.0.4") (d (list (d (n "cmake") (r ">=0.1.0, <0.2.0") (d #t) (k 1)) (d (n "uuid") (r ">=0.8.0, <0.9.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wk9wmi92r5zx2mmn0qp07ws5sfwbm8hgb1rb0im5hgdn5jnqql5")))

(define-public crate-qpid_proton-0.0.5 (c (n "qpid_proton") (v "0.0.5") (d (list (d (n "cmake") (r ">=0.1.0, <0.2.0") (d #t) (k 1)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "uuid") (r ">=0.8.0, <0.9.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m597mmdmscazz5ddak6mc5fmz5ar4qmfbkjdhpfqg6c4pl1y0kj")))

(define-public crate-qpid_proton-0.0.6 (c (n "qpid_proton") (v "0.0.6") (d (list (d (n "cmake") (r ">=0.1.0, <0.2.0") (d #t) (k 1)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "uuid") (r ">=0.8.0, <0.9.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fihfmksjyxkyj7w6c5fb4h32frsxngdhpyx247zp4rcchfbxzvf")))

(define-public crate-qpid_proton-0.0.7 (c (n "qpid_proton") (v "0.0.7") (d (list (d (n "cmake") (r ">=0.1.0, <0.2.0") (d #t) (k 1)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "uuid") (r ">=0.8.0, <0.9.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "14fiz4v7n3dvvjnmzfvhwka1sqv297plagmg34rhx83b4r1zqa8b")))

(define-public crate-qpid_proton-0.0.8 (c (n "qpid_proton") (v "0.0.8") (d (list (d (n "cmake") (r ">=0.1.0, <0.2.0") (d #t) (k 1)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "uuid") (r ">=0.8.0, <0.9.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0km4caf5xkz6adhqm414m6h2qlcqk7k7v79v3k22g9jqrbq1d4xc")))

(define-public crate-qpid_proton-0.0.9 (c (n "qpid_proton") (v "0.0.9") (d (list (d (n "cmake") (r ">=0.1.0, <0.2.0") (d #t) (k 1)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "uuid") (r ">=0.8.0, <0.9.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "17ar46yajj8irib5ypz724wbj70qzkav171mc0fm0mczf7qqbhm8")))

(define-public crate-qpid_proton-0.0.10 (c (n "qpid_proton") (v "0.0.10") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "qpid_proton-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0yba8wlv85b9xxvfy8djikbn99235gzarsxwcfpjgdx8p21p1f1m")))

