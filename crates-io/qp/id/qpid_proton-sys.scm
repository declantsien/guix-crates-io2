(define-module (crates-io qp id qpid_proton-sys) #:use-module (crates-io))

(define-public crate-qpid_proton-sys-0.0.1 (c (n "qpid_proton-sys") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "10p64qsxggghbfrpihhdlv48aa1l6wzlil53lxbcv9a7rfh0v5jy")))

(define-public crate-qpid_proton-sys-0.1.0 (c (n "qpid_proton-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1amn75pzh67a5vz2mm2nvxqqwyamk3j1j52lywdg68yxp3hh6xkh")))

(define-public crate-qpid_proton-sys-0.1.1 (c (n "qpid_proton-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0gbn8qkljgjy04ihdv7v9db3ad0ws6ywc2jxaw6qxdm40173lvpz")))

(define-public crate-qpid_proton-sys-0.1.2 (c (n "qpid_proton-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0qwzpqf8ckbwsp32cgn5cnsrr9aw29xh29n4if0d355hazx9wmgx")))

(define-public crate-qpid_proton-sys-0.1.3 (c (n "qpid_proton-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0n10211lvlisvxpwfv2n6xd35bzkawqmd74b1rihkzkma35sqs0v")))

(define-public crate-qpid_proton-sys-0.1.4 (c (n "qpid_proton-sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0gcbihk589jj8ap4cmi2zjfkq7z9a489d2ss0b7hq7aw06rd6fd4")))

(define-public crate-qpid_proton-sys-0.1.5 (c (n "qpid_proton-sys") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "16lm8n3lhfpi0m9101xflvv7lfv3p5x66j6brj6acbs61ghciy17")))

(define-public crate-qpid_proton-sys-0.1.6 (c (n "qpid_proton-sys") (v "0.1.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "00miz5mgzkh7avd9ay4izwljqil5bcy3sy8amzd0a0wg1as6h735")))

(define-public crate-qpid_proton-sys-0.1.7 (c (n "qpid_proton-sys") (v "0.1.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1h1g2gw5d03ixs35rd92kvrg5vxzxnkr8jgarmpgyhii7ggrw5ky")))

(define-public crate-qpid_proton-sys-0.1.8 (c (n "qpid_proton-sys") (v "0.1.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1128rmnmqm0kvj6i0xicrmpqxz5haysw0m09hkliapcclq7gykkn")))

(define-public crate-qpid_proton-sys-0.1.9 (c (n "qpid_proton-sys") (v "0.1.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1qmqyy9z2dhffjprrlwkma9mm39qpp31bl87phdryppiy6p03wq1")))

(define-public crate-qpid_proton-sys-0.1.10 (c (n "qpid_proton-sys") (v "0.1.10") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0f4l796a6ma02y1jxkxyci9nm4ms82b53s4xpjg6z31bgrmykyfv")))

(define-public crate-qpid_proton-sys-0.1.11 (c (n "qpid_proton-sys") (v "0.1.11") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1qmi5kbq1kinr4a5p6jk3pj0qsmcqjskiyg7jn9pr18rs6y412r9")))

(define-public crate-qpid_proton-sys-0.1.12 (c (n "qpid_proton-sys") (v "0.1.12") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0lgyr6gj7c2ybsc64xiv513ca3ss0q4ffpvdn1vgrap7xh4yy7m7")))

