(define-module (crates-io qp ar qparas) #:use-module (crates-io))

(define-public crate-qparas-0.1.0 (c (n "qparas") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1kfxr8m6y3vd986b3wd6m9v799fm2dpxiifylla07cnmsn0hvxr0") (r "1.56.0")))

