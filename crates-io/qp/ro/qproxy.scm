(define-module (crates-io qp ro qproxy) #:use-module (crates-io))

(define-public crate-qproxy-0.1.0 (c (n "qproxy") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "054yqy82nw43b7cd6acylcnp84jsnc4s4nyhysdx21isbjhihm0a") (y #t)))

(define-public crate-qproxy-0.1.1 (c (n "qproxy") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z9gfqxgmlcy42db22f8b9lizrfkk4wfndnv71i8y7a550zw02jk") (y #t)))

(define-public crate-qproxy-0.1.2 (c (n "qproxy") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xhpg97qgh4q6lfwkw5lk9kpr35fd9yddpyijhhdm5w4zsxi1ql9") (y #t)))

(define-public crate-qproxy-0.1.3 (c (n "qproxy") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c29fz8jaivbr9y937b2mlyr6g5644d0a13g8a4r17dqknswx9ag") (y #t)))

(define-public crate-qproxy-0.1.4 (c (n "qproxy") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g8a5p3il9whzdqcxzpqv108fd235zfw8a9ksgm36pyk0hba5wrb") (y #t)))

(define-public crate-qproxy-0.1.5 (c (n "qproxy") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10lz468i18ayiz8j66wxsrsmyqy4kkbc9gf9nksq82ljb6183hkw") (y #t)))

(define-public crate-qproxy-0.1.5-1 (c (n "qproxy") (v "0.1.5-1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14gc29xnzkiapm060jxy4invnamiyf1mh5xjzavzajxkm4rbhiqq") (y #t)))

(define-public crate-qproxy-0.1.6 (c (n "qproxy") (v "0.1.6") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05cb5fhlh8za4dmz5ly4hlgprma90fayxga6ycdr3h0grwqslyf5")))

(define-public crate-qproxy-0.2.0 (c (n "qproxy") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rv2pxqha87vc4952hg5rkknpfw0vv7lp82455wv56j8mm6zq6jy")))

(define-public crate-qproxy-0.2.1 (c (n "qproxy") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11dvzi9cv5vrwjp58pizkyfnv7vnjz0rbahs87k53kijk77hzng8")))

(define-public crate-qproxy-0.2.3 (c (n "qproxy") (v "0.2.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vyx026dkngx6435qdfrfdkipwq15cvcv5jzflhsb5ivrl2i4b5p")))

(define-public crate-qproxy-0.2.4 (c (n "qproxy") (v "0.2.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17p5jcsi81avcqc2p1ldds235s4lzafkipk0ly4a54dh5c57fjxw")))

(define-public crate-qproxy-0.2.5 (c (n "qproxy") (v "0.2.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d79nhjfzhw8phx6gv7zlnym18jc4zriv8c9fvnx1ii5164chkaz")))

(define-public crate-qproxy-0.2.6 (c (n "qproxy") (v "0.2.6") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10ap9z6giycqnfp7mwk2nm76fb28cbz0f9qmlfji0pqk789l4g89")))

(define-public crate-qproxy-0.2.7 (c (n "qproxy") (v "0.2.7") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06kmnisfdbykssk41fgwb8njhh2zbfv9qnlza0fm0ygza5jlmd0p")))

