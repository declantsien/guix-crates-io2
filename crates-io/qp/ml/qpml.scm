(define-module (crates-io qp ml qpml) #:use-module (crates-io))

(define-public crate-qpml-0.1.0 (c (n "qpml") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "08mwd07k0b8smzb4ipacpw2bgc9h1fcs3zxwxsa3dldcr7lli1i1")))

(define-public crate-qpml-0.2.0 (c (n "qpml") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1ca0px410xnhg67j0i69d89nb26afali5ldw7s7m8hrgmfcwvfy5")))

(define-public crate-qpml-0.3.0 (c (n "qpml") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1qj5bng39xgc3g756j5min9fsa00lw16dd72ry18922w4j5771sd")))

(define-public crate-qpml-0.4.0 (c (n "qpml") (v "0.4.0") (d (list (d (n "datafusion") (r "^15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1cv38yvm4i5rvv74f0gjalflgdlxmgj5mvbpdj9ipf5d139ijhip")))

(define-public crate-qpml-0.5.0 (c (n "qpml") (v "0.5.0") (d (list (d (n "datafusion") (r "^15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "10pmjviw9cmi3n97daqw0qrhk8krvhzxf0phcqywjx74xxkzx9vx")))

(define-public crate-qpml-0.6.0 (c (n "qpml") (v "0.6.0") (d (list (d (n "datafusion") (r "^15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0g36f62c562aqi1rfy3kwashd1qkl76p763lrm10fgnds8zw2063")))

(define-public crate-qpml-0.7.0 (c (n "qpml") (v "0.7.0") (d (list (d (n "datafusion") (r "^15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0fsxkglc4pff03sxy0jkw1qrwcaxhcs0im4hpr8zd6q1k7wi9hy7")))

(define-public crate-qpml-0.8.0 (c (n "qpml") (v "0.8.0") (d (list (d (n "datafusion") (r "^15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1scyxp8djd57p3m487b6hb0fnv463ndgp1njibxbnfhgzbiqv8z1")))

(define-public crate-qpml-0.9.0 (c (n "qpml") (v "0.9.0") (d (list (d (n "datafusion") (r "^15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "12hqqaaqqzpiqd6ji3jvfp8a71w63zkwqk2slmba4bc9zw9px4vj")))

(define-public crate-qpml-0.11.0 (c (n "qpml") (v "0.11.0") (d (list (d (n "datafusion") (r "^16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "0v9nm9jbc9dn326q38l5m7rmaim2il9p24xkmbs644izj3b3y8sz")))

(define-public crate-qpml-0.12.0 (c (n "qpml") (v "0.12.0") (d (list (d (n "datafusion") (r "^16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "0mdg846pzia9adrh68cp1z1sdvk9w2g0d1k96f8blpmn64fzz39a")))

(define-public crate-qpml-0.13.0 (c (n "qpml") (v "0.13.0") (d (list (d (n "datafusion") (r "^21.0") (d #t) (k 0)) (d (n "datafusion-substrait") (r "^21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "1xk8fgfk9cvs7p0r5jyq44rwjm453gapwahw042zkq6p6axz3awy")))

(define-public crate-qpml-0.14.0 (c (n "qpml") (v "0.14.0") (d (list (d (n "datafusion") (r "^34.0") (d #t) (k 0)) (d (n "datafusion-substrait") (r "^34.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "1y7pziwqw83wq30kg5nk4m3smc3zr3iz8gl2qbmiv447v9ph8k27")))

