(define-module (crates-io t4 t- t4t-games) #:use-module (crates-io))

(define-public crate-t4t-games-0.0.1 (c (n "t4t-games") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "t4t") (r "^0.0.1") (d #t) (k 0)) (d (n "test-log") (r "^0.2.12") (d #t) (k 2)))) (h "0as2k6zgwc0v1a7q8nljcf65ybaccy1yik8z1kd5g017wx2598b3")))

(define-public crate-t4t-games-0.0.2 (c (n "t4t-games") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "t4t") (r "^0.0.1") (d #t) (k 0)) (d (n "test-log") (r "^0.2.12") (d #t) (k 2)))) (h "18rzsd8glygcixb31s671njg403b4f9l52kdgzqvh32xwzi9sn4b")))

(define-public crate-t4t-games-0.0.3 (c (n "t4t-games") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "t4t") (r "^0.0.3") (d #t) (k 0)) (d (n "test-log") (r "^0.2.12") (d #t) (k 2)))) (h "1q667k4vpa1fql91vfy2k80cxipk15s0fg18ajqis7nsr7ifbq27")))

(define-public crate-t4t-games-0.0.4 (c (n "t4t-games") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "t4t") (r "^0.0.5") (d #t) (k 0)) (d (n "test-log") (r "^0.2.16") (d #t) (k 2)))) (h "0k0l2i9plasi9i570lfaarpk06qg8md0w8c2y52g96r9sq2g6g3h")))

