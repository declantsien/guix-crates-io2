(define-module (crates-io t4 _i t4_idl_parser) #:use-module (crates-io))

(define-public crate-t4_idl_parser-0.1.0 (c (n "t4_idl_parser") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom-greedyerror") (r "^0.4") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1dyn58dwcq3a7ahnqc89729v44nm5n2bjnq0423lsyf6g3j9wp36")))

(define-public crate-t4_idl_parser-0.1.1 (c (n "t4_idl_parser") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom-greedyerror") (r "^0.4") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1jbx2fya7zfc6djm8y02b7y5rivmgcxaa730hkc40i44v4cr2wff")))

