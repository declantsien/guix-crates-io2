(define-module (crates-io t4 ru t4rust-derive) #:use-module (crates-io))

(define-public crate-t4rust-derive-0.1.3 (c (n "t4rust-derive") (v "0.1.3") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0csd6sf35s2vwq13dqdx2390csaa52x2286692m46826x6lqm2aq")))

(define-public crate-t4rust-derive-0.1.4 (c (n "t4rust-derive") (v "0.1.4") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bwhrw0jk7gb7kgna63cndqs5k7cbpgpvnmz9gh2m13d6zviclm7")))

(define-public crate-t4rust-derive-0.2.0 (c (n "t4rust-derive") (v "0.2.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13k01yjm8wlrh7v9k0j3piq3ps0vgra6qh0szq4b5cs7nbplznh5")))

(define-public crate-t4rust-derive-0.3.0 (c (n "t4rust-derive") (v "0.3.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cd4r11yn1jpaxh0ih6rxisnqxkljrk8rr4j50ayjihkjdr56yxr")))

