(define-module (crates-io mk mk mkmk) #:use-module (crates-io))

(define-public crate-mkmk-0.1.0 (c (n "mkmk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0gzlb96q5flp8frr0f90d3clqcixb3r1jla40g8v2vjm5yq4cy3z")))

(define-public crate-mkmk-0.1.1 (c (n "mkmk") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "03pwmps4rqpzdfrlcn1gzl90jcl2gjd7ijhiz70iqqi1bcql8fiy")))

