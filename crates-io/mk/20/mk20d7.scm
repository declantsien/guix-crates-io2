(define-module (crates-io mk #{20}# mk20d7) #:use-module (crates-io))

(define-public crate-mk20d7-0.1.0 (c (n "mk20d7") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "02bj8na1nndd2db6b43r871kbmzh07qi90khsvmfrimpcn98injh") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-mk20d7-0.1.1 (c (n "mk20d7") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1v9nfj4a7kkxjkf8myl4ghhcq04wxysag6jalglp37dpvigharr6") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-mk20d7-0.1.2 (c (n "mk20d7") (v "0.1.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ifzyi4qk6bslbjvw3aa322d45pxkl27p2lyjmjkd3j23kff57s0") (f (quote (("rt" "cortex-m-rt"))))))

