(define-module (crates-io mk te mktemp) #:use-module (crates-io))

(define-public crate-mktemp-0.1.0 (c (n "mktemp") (v "0.1.0") (d (list (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "1qygqcim3h7is0x5n790qvf6kiwi51pxcmpkazk2j99lfvk36p39")))

(define-public crate-mktemp-0.1.1 (c (n "mktemp") (v "0.1.1") (d (list (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "0sghzykck6y49gppvsq0lbnnv8pvf39hamcwavv1568lj6mmgfs5")))

(define-public crate-mktemp-0.1.2 (c (n "mktemp") (v "0.1.2") (d (list (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "1sca3v38mlvsvhiyyizjyfpx4ddd7cz1mh99s58n7wj4blxx6cvk")))

(define-public crate-mktemp-0.2.1 (c (n "mktemp") (v "0.2.1") (d (list (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "1r020904kv6sg4mk7yicwpw6hccgpwzy5v8rk9kmi2kdlv1y7qdf")))

(define-public crate-mktemp-0.3.1 (c (n "mktemp") (v "0.3.1") (d (list (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "0sfddmp9dl1qfx525gw64s8pshd1kczm98n27ngl6rgdkvmiq03p")))

(define-public crate-mktemp-0.4.0 (c (n "mktemp") (v "0.4.0") (d (list (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "09vvgk86h3c3d0zn31y4v0krm9swm74nzn02p0ipk5swdisgrx7d")))

(define-public crate-mktemp-0.4.1 (c (n "mktemp") (v "0.4.1") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "18hd84l7k7i1mbjylagwg1miczh78cjrs55r0kggqcc28ivfcpcp")))

(define-public crate-mktemp-0.5.0 (c (n "mktemp") (v "0.5.0") (d (list (d (n "uuid") (r "~1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "02l2yz0jkgyv0s0010z3lwx5p4qd95788kvqkz9igdvvvms1zp2b")))

(define-public crate-mktemp-0.5.1 (c (n "mktemp") (v "0.5.1") (d (list (d (n "uuid") (r "~1.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1n3b0jppm5x71jy36gnik1nh19knci67h9n29b2gxbq1rpxxizk9")))

