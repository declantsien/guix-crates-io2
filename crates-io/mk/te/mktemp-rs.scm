(define-module (crates-io mk te mktemp-rs) #:use-module (crates-io))

(define-public crate-mktemp-rs-0.1.0 (c (n "mktemp-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "0wqsld601ng363pn58r87fccvp50lfy5hjy7nxl98l0hbvxp9smh")))

(define-public crate-mktemp-rs-0.2.0 (c (n "mktemp-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (k 0)))) (h "0x3vanxlbb9y7f9rxkknja86dnax2x64ia6clnzmf3v080amy54i")))

