(define-module (crates-io mk we mkwebsite) #:use-module (crates-io))

(define-public crate-mkwebsite-0.1.0 (c (n "mkwebsite") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "katex") (r "^0.4.6") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "mlua") (r "^0.9.7") (f (quote ("lua54"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (f (quote ("parse"))) (d #t) (k 0)))) (h "1my5lhblmkwwjjjkqykwwgy1lz49dcpbay6sx80gak6d42gx02i2")))

