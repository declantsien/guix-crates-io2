(define-module (crates-io mk we mkwebfont_hb-subset) #:use-module (crates-io))

(define-public crate-mkwebfont_hb-subset-0.3.0 (c (n "mkwebfont_hb-subset") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1na7is5fpkxmbiys4mpap1i3ycm2vf0j07kinawzy70j8b54bj9l") (f (quote (("bundled"))))))

