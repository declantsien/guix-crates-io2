(define-module (crates-io mk we mkwebfont_common) #:use-module (crates-io))

(define-public crate-mkwebfont_common-0.2.0-alpha1 (c (n "mkwebfont_common") (v "0.2.0-alpha1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "blake3") (r "^1.5") (f (quote ("pure"))) (d #t) (k 0)) (d (n "roaring") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.37") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "wyrand") (r "=0.2.0") (f (quote ("wyhash"))) (d #t) (k 0)) (d (n "zstd") (r "^0.13") (f (quote ("zstdmt"))) (d #t) (k 0)))) (h "1b5yr7bg9cvsp9bas0rsd84rj5z06437fv29jlxppw6gxfzj1sxv")))

