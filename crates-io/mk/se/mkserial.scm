(define-module (crates-io mk se mkserial) #:use-module (crates-io))

(define-public crate-mkserial-0.0.0 (c (n "mkserial") (v "0.0.0") (d (list (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "02ar77cbglg3xm2l0xas65f4f36qar5i8banj7sx3cr04xnqq0if")))

(define-public crate-mkserial-0.0.1 (c (n "mkserial") (v "0.0.1") (d (list (d (n "num-bigint") (r "^0.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "121c9ck4rip45yz72k28ldljhqkwn5kzc88fq5lis7nm4fhqwvaw")))

(define-public crate-mkserial-0.0.2 (c (n "mkserial") (v "0.0.2") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1xjcdah2s1wvvhczab3fxk7nisbnk21nqb2bx2w73i4zki95nm5i")))

(define-public crate-mkserial-1.0.0 (c (n "mkserial") (v "1.0.0") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1fhbcghd6kyrdsgirhv7r85k5iwrp06qybqwmr1dfl2k2ldsjz2j")))

(define-public crate-mkserial-1.0.1 (c (n "mkserial") (v "1.0.1") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (k 0)))) (h "05lbp9xkxrapsbnhz5hvy0f1z8wbdfxg4k1zmwgya8bfvpwygn8q")))

(define-public crate-mkserial-1.0.2 (c (n "mkserial") (v "1.0.2") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (k 0)))) (h "1nmnm7nismxw1fnl8j3m77laicchq153byg7dhzysqmqswykb9ah")))

(define-public crate-mkserial-1.0.3 (c (n "mkserial") (v "1.0.3") (d (list (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (k 0)))) (h "0749jj56bds4m9hj6vprghfkv3yb9rkxjxnfnxv7n5iq6xv0vr9d")))

