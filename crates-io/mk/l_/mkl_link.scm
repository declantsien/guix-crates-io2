(define-module (crates-io mk l_ mkl_link) #:use-module (crates-io))

(define-public crate-mkl_link-0.1.0 (c (n "mkl_link") (v "0.1.0") (h "03qnjn9alsv4ds55y985397dmsvjarshdnn7acv56v7d82d9c70v") (f (quote (("tbb") ("openmp") ("default"))))))

(define-public crate-mkl_link-0.1.1 (c (n "mkl_link") (v "0.1.1") (h "00gc6rfh8m8af7pjlknnalngy101f3ga7nz118yxlnpxn40i32sl") (f (quote (("tbb") ("openmp") ("default"))))))

(define-public crate-mkl_link-0.1.2 (c (n "mkl_link") (v "0.1.2") (d (list (d (n "cblas_ffi") (r "^0.1") (d #t) (k 2)))) (h "10lppyg2h56dkr1di45smg2l3n22zhlivrb5xrqjy3561k9nag3c") (f (quote (("tbb") ("openmp") ("default"))))))

