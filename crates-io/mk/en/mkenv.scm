(define-module (crates-io mk en mkenv) #:use-module (crates-io))

(define-public crate-mkenv-0.1.0 (c (n "mkenv") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "1nzjwxswbh0f0ln44mnv8nzlzxd75pjsw9hyzh4j8vwlwc632rgx")))

(define-public crate-mkenv-0.1.1 (c (n "mkenv") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "0grky7ilxymdnivhygxjv2jcf0v2i3prljsnsvypnkwlydszya37")))

(define-public crate-mkenv-0.1.2 (c (n "mkenv") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "0q0lzn3shw2r7nanln324c16phcvs29r8grygl2ffcm5zbmrcwyl")))

(define-public crate-mkenv-0.1.3 (c (n "mkenv") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "1laba6nh9ld50vjs15c7hiakl5src0cmqkzvhrzdyhwv1pjszfpz")))

(define-public crate-mkenv-0.1.4 (c (n "mkenv") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "180vr6spnad5p5yx30xm8zzk447sl2zwaj3kqy6h2q1cn7701bkl")))

(define-public crate-mkenv-0.1.5 (c (n "mkenv") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "1ff5zrkdx133n34y334sx8flw2pxq8n8xbxs7xg367qwdy9ixzcc")))

(define-public crate-mkenv-0.1.6 (c (n "mkenv") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "0a8kfxzkjaggb7b5slbgyakd45j4nrhazyw2286anydif61dn9gp")))

