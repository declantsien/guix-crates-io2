(define-module (crates-io mk py mkpydir) #:use-module (crates-io))

(define-public crate-mkpydir-0.1.0 (c (n "mkpydir") (v "0.1.0") (h "1crcd6fhhn8sqqb2sbxflqz0h2hlaxhwm5g4zj5j2mvapwk9jbzd")))

(define-public crate-mkpydir-0.1.1 (c (n "mkpydir") (v "0.1.1") (h "1z04c3sp12as9w46azkqb6ny6v2nccg9b2b5n2qmbd2pgipkrrmk")))

(define-public crate-mkpydir-0.1.3 (c (n "mkpydir") (v "0.1.3") (h "1vd4aa0ya4nwni5znaxajchw8c0j60wyzy4zg44l0h0l94ls2s6v")))

