(define-module (crates-io mk fs mkfs-ext2) #:use-module (crates-io))

(define-public crate-mkfs-ext2-0.1.0 (c (n "mkfs-ext2") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "mkfs-filesystem") (r "^0.1.0") (d #t) (k 0)))) (h "010mvbsfk5b5f81a974bg6q9x1ibhh69wbcz70axcqq4xk8q1m4b")))

