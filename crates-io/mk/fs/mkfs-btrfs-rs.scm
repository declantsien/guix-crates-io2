(define-module (crates-io mk fs mkfs-btrfs-rs) #:use-module (crates-io))

(define-public crate-mkfs-btrfs-rs-0.1.0 (c (n "mkfs-btrfs-rs") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0jkvdjba02p4648ciqa71kww3jqw37sb95bc1p15glr9ajj2qw20")))

(define-public crate-mkfs-btrfs-rs-0.1.1 (c (n "mkfs-btrfs-rs") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "00y06j271wpdlznrdl7wx9dzpdk7ry8s0sp2cbrh66ay7q1c3p60")))

