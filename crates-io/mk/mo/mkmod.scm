(define-module (crates-io mk mo mkmod) #:use-module (crates-io))

(define-public crate-mkmod-0.0.1 (c (n "mkmod") (v "0.0.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "17byp5xqfv2hbv1viz2ccyd05sndvpr63v11ymfcdbsfdaa2k56m")))

(define-public crate-mkmod-0.0.2 (c (n "mkmod") (v "0.0.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "12xbrnikdqkc80ghkrmwdvpmy9mac2r25gwx1v6rzvfg2f9r6zdf")))

