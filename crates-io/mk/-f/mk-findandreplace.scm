(define-module (crates-io mk -f mk-findandreplace) #:use-module (crates-io))

(define-public crate-mk-findandreplace-0.1.0 (c (n "mk-findandreplace") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.14") (d #t) (k 0)))) (h "1rbzjqaasp8ivdrsy1npxiwzlziiq7g22aljdn7r323d4jw70jvq")))

