(define-module (crates-io mk co mkcontext) #:use-module (crates-io))

(define-public crate-mkcontext-0.2.0 (c (n "mkcontext") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.7") (d #t) (k 0)))) (h "1840s7fzha36dlmlqphalza8f3q3m14xxmp2sn5c2l0rq4pijgm6")))

(define-public crate-mkcontext-0.2.1 (c (n "mkcontext") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.7") (d #t) (k 0)))) (h "0nyxpbdagg7fmvhjblrkb6ix4ybmlcc89nc0pvm83l4c49dkr624")))

(define-public crate-mkcontext-0.3.0 (c (n "mkcontext") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.7") (d #t) (k 0)))) (h "006h3l2aif39z57fzkh49i9zyx9n0qgx83fk47gw2pjayrv7s6jy")))

(define-public crate-mkcontext-0.4.0 (c (n "mkcontext") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.7") (d #t) (k 0)))) (h "1yvnbdan9l7iczmxn847jf7l8lwwzssdgqxkjkwjqwbfh1bk3fy8")))

(define-public crate-mkcontext-0.4.2 (c (n "mkcontext") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.7") (d #t) (k 0)))) (h "0aanlw0sabh4frks0j4phf1zyzcl6a7xamlrh5a8hf8yqjnzbamw")))

(define-public crate-mkcontext-0.4.3 (c (n "mkcontext") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tiktoken-rs") (r "^0.5.7") (d #t) (k 0)))) (h "0cp7adsajl4ry8lm21l2slafkgwl4pj1nrvjc8p49qfl7xz3v8sa")))

