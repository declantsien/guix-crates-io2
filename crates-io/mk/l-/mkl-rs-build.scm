(define-module (crates-io mk l- mkl-rs-build) #:use-module (crates-io))

(define-public crate-mkl-rs-build-0.1.0 (c (n "mkl-rs-build") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "bindgen") (r "^0.69.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 0)))) (h "0n7vq1rlhvmg6750rlxzxq1d7hizp25vmd8bwgk542gymfgi0636")))

(define-public crate-mkl-rs-build-0.3.0 (c (n "mkl-rs-build") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "bindgen") (r "^0.69.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 0)))) (h "1gg1z16ar41pxlgy3ziql92c1hy381g8fsznnjrf2hgcxiq2lbxm")))

(define-public crate-mkl-rs-build-0.3.1 (c (n "mkl-rs-build") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "bindgen") (r "^0.69.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 0)))) (h "19z4ip3n87imxnqzqnkxrwdya26mfwvn68mw6xycjxf2w0hiyr0h")))

