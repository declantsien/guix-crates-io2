(define-module (crates-io mk au mkaudiolibrary) #:use-module (crates-io))

(define-public crate-mkaudiolibrary-0.1.0 (c (n "mkaudiolibrary") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "10ljjjsb57xz6xx8imjdv85lw81hc5dw9dv32r6djvy5q7cjk26h")))

(define-public crate-mkaudiolibrary-0.1.1 (c (n "mkaudiolibrary") (v "0.1.1") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1js3m84fdcry1brfh3lq10xml98szjn7zasp1379dzqqdmjqvp3n")))

(define-public crate-mkaudiolibrary-0.1.2 (c (n "mkaudiolibrary") (v "0.1.2") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1wha3y3y0qi5arjc6xgyiv46waafc5rm3ilvwsk8pd8xnzjw19ki")))

(define-public crate-mkaudiolibrary-0.1.3 (c (n "mkaudiolibrary") (v "0.1.3") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "053ihvjlzv0mg22v38q1am5j5fdx6340xfqn4h2j1xcq8w533k8z")))

(define-public crate-mkaudiolibrary-0.1.4 (c (n "mkaudiolibrary") (v "0.1.4") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1021147lx8wdzl3fc4wj0x4d668va5ps4l25jrl6qddqsxc6nl44")))

(define-public crate-mkaudiolibrary-0.1.5 (c (n "mkaudiolibrary") (v "0.1.5") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1qqn55qcmigbvi0gbbz2nsz42dg4437mixc7wvpn6hannnd99md9")))

(define-public crate-mkaudiolibrary-0.1.6 (c (n "mkaudiolibrary") (v "0.1.6") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "199agviw2zijn3igwgyifmrs22k3hiv8h9jgsr0cr73xa5fg0v7c")))

(define-public crate-mkaudiolibrary-0.1.7 (c (n "mkaudiolibrary") (v "0.1.7") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "15rp2h72b428r5r11hx64mrdr687l5ghga6dlcqfx9j4p9rh5lnk")))

(define-public crate-mkaudiolibrary-0.1.8 (c (n "mkaudiolibrary") (v "0.1.8") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1xa4a04ar9kdwla9j1qpzp1blcpzw8il1lyk16dfabzis394cbpv")))

(define-public crate-mkaudiolibrary-0.1.9 (c (n "mkaudiolibrary") (v "0.1.9") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1m7shsmm79052nsb11pqb32jl401nxckllmf7pl56d5xsp5rmzsy")))

(define-public crate-mkaudiolibrary-0.1.10 (c (n "mkaudiolibrary") (v "0.1.10") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "15c4rk6k70w6a060xs0pyx0mhhj9wwq3a0v8p8f3rrhfbm1mgc2r")))

(define-public crate-mkaudiolibrary-0.1.11 (c (n "mkaudiolibrary") (v "0.1.11") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1r31sczch0ym6z6nw7pcr3m28qs4bqmzfbyhy949yx63wv3jbrd7")))

(define-public crate-mkaudiolibrary-0.1.12 (c (n "mkaudiolibrary") (v "0.1.12") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1rjan5s20hz0spngxjix7ccp5hal313kj4ambwpy7ff5r55hxcsj")))

(define-public crate-mkaudiolibrary-0.1.13 (c (n "mkaudiolibrary") (v "0.1.13") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1pv43cy19xppd0j3bpqn6fkbvqb46msjs5m8dxblpys6j2jgckns")))

(define-public crate-mkaudiolibrary-0.1.14 (c (n "mkaudiolibrary") (v "0.1.14") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "19yhsbdg8wsrzaj8a83cl2vk1kjrigk0z83af44x662jma8x7890")))

(define-public crate-mkaudiolibrary-0.1.15 (c (n "mkaudiolibrary") (v "0.1.15") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1qwc0w4wj8z3rla65gdr8p8kaw4ydih5k51hvfzxhas057nvl6n7")))

(define-public crate-mkaudiolibrary-0.1.16 (c (n "mkaudiolibrary") (v "0.1.16") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "08hr33i9afdnjij9r38z8cfnpnlyw8nyzxvrxah6sg08r3irgakn")))

(define-public crate-mkaudiolibrary-0.1.17 (c (n "mkaudiolibrary") (v "0.1.17") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1x89qsjlvwmsxlnvihyifnykpsa0vqnadh3xs19v5vvqzmhdbmfl")))

(define-public crate-mkaudiolibrary-0.1.18 (c (n "mkaudiolibrary") (v "0.1.18") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "09mlq35bhdw3vy16dnyhz4fsdhqi9gbvny767z0674q469hgxxnj")))

(define-public crate-mkaudiolibrary-0.1.19 (c (n "mkaudiolibrary") (v "0.1.19") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "06gw3msdvcg4z0wk1sgybxny2glr44517q1a9770gby16pka1wq7")))

(define-public crate-mkaudiolibrary-0.1.20 (c (n "mkaudiolibrary") (v "0.1.20") (d (list (d (n "libloading") (r "^0.8.0") (d #t) (k 0)))) (h "1k1iphrpc7bw07zqdwai816ffkjg0b8i2ajwn45ay1lykinswfv9")))

(define-public crate-mkaudiolibrary-0.2.0 (c (n "mkaudiolibrary") (v "0.2.0") (d (list (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "no_denormals") (r "^0.1.2") (d #t) (k 0)))) (h "1khvdmiqxjybbanwam7gr07z5msi5mw5a3iygq074xbpfxwafp3p")))

