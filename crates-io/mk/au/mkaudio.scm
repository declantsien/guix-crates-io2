(define-module (crates-io mk au mkaudio) #:use-module (crates-io))

(define-public crate-mkaudio-0.1.0 (c (n "mkaudio") (v "0.1.0") (h "1k1b3322acd8w7h07150iv1lywz9w73z507s2ixmff2zs1frnc1i") (y #t)))

(define-public crate-mkaudio-0.1.1 (c (n "mkaudio") (v "0.1.1") (h "0p5mkc3x9nyhsgda6kyi4qg7bajixc25f8ib1fyi0z1h28bxwy6f")))

