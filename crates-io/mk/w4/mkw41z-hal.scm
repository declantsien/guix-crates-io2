(define-module (crates-io mk w4 mkw41z-hal) #:use-module (crates-io))

(define-public crate-mkw41z-hal-0.1.0 (c (n "mkw41z-hal") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "mkw41z") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1b2wy4kh04zv9mljv2233afpzywq1nhj3f2yz2rkf2miilk64wwb") (f (quote (("rt" "mkw41z/rt") ("default" "rt"))))))

(define-public crate-mkw41z-hal-0.1.1 (c (n "mkw41z-hal") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "mkw41z") (r "^0.1.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "011kxr8ar3jm4qw098r6n8qbf31aq371li7ff1nbcixavinv8naq") (f (quote (("rt" "mkw41z/rt") ("default" "rt"))))))

