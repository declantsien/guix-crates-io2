(define-module (crates-io mk w4 mkw41z4) #:use-module (crates-io))

(define-public crate-mkw41z4-0.1.0 (c (n "mkw41z4") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "00ksqpi8f347382qsljq6pnrs67dc8xr22z8gw5mnlw1py6sypq3") (f (quote (("rt" "cortex-m-rt"))))))

