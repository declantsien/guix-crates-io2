(define-module (crates-io mk w4 mkw41z) #:use-module (crates-io))

(define-public crate-mkw41z-0.1.0 (c (n "mkw41z") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0vlih725gmxxlh4y8llpjjvx32v1kg2i5akc9fh87avz0b5mrhkz") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-mkw41z-0.1.1 (c (n "mkw41z") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "14ajzgq7mg3fiv7dx560gihf9564dchnsl89rviw3605rpw6a4z3") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-mkw41z-0.1.2 (c (n "mkw41z") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "090lvydv5pa5kq4fy1ry4kkfv3xaalnp1x31fy996vlh9xc1m5pz") (f (quote (("rt" "cortex-m-rt"))))))

