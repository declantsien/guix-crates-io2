(define-module (crates-io mk it mkit) #:use-module (crates-io))

(define-public crate-mkit-0.1.0 (c (n "mkit") (v "0.1.0") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mkit-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0s99zp313cwhcpywv94nh6sry7l1b3hxdpw02q7zwkpxyj5y4iir")))

(define-public crate-mkit-0.2.0 (c (n "mkit") (v "0.2.0") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mkit-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0wahaqs3d2hmayqkffiqrcwiba75wifpmas043hl4h2yz48a6dig")))

(define-public crate-mkit-0.3.0 (c (n "mkit") (v "0.3.0") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mkit-derive") (r "=0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "031420xcfyva5nhk7yqj2klfjabhxw7vznj5m5zmnci5ajvywwz2") (f (quote (("debug"))))))

(define-public crate-mkit-0.4.0 (c (n "mkit") (v "0.4.0") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mkit-derive") (r "=0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "xorfilter-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0hj8cf1npl4khhkqjmw3hn7ky7yc89kpibjv6b83lkrg83n5hg9r") (f (quote (("debug"))))))

