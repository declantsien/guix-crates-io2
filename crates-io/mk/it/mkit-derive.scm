(define-module (crates-io mk it mkit-derive) #:use-module (crates-io))

(define-public crate-mkit-derive-0.1.0 (c (n "mkit-derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0avwjv7kl7sm52x7zg1ljfllmsihvvy7wv6s8p65rl6iv1a2l7wn")))

(define-public crate-mkit-derive-0.2.0 (c (n "mkit-derive") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zm6lzh7d624q94jajrs659rxcxrbgwhzi53ab92ksgpqmah5yvh")))

(define-public crate-mkit-derive-0.3.0 (c (n "mkit-derive") (v "0.3.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0a0y3s88x0jm0sg77wfj43riybb6s7y92wkxyhw6j5bk7jzbmvn1")))

