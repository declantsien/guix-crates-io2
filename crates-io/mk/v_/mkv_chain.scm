(define-module (crates-io mk v_ mkv_chain) #:use-module (crates-io))

(define-public crate-mkv_chain-0.2.1 (c (n "mkv_chain") (v "0.2.1") (h "0ylv6gi1ldvl76061x8pknw847a5sw94z4ag8fah8lnqzahr3886")))

(define-public crate-mkv_chain-0.2.2 (c (n "mkv_chain") (v "0.2.2") (h "0y2k7khk7pqwvf1jlylvn1zn04mjgar7dvy7bbi12x49121nqzfn")))

(define-public crate-mkv_chain-0.3.0 (c (n "mkv_chain") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0q4yxdizqlxs27yr7s7ijkz2mrk0wn88hh6343i9i7nxl2vqsr73") (f (quote (("default"))))))

(define-public crate-mkv_chain-0.3.1 (c (n "mkv_chain") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1sz5lpbhdqjh21rnrz60izqlbvipa1hp8lqgzi39gd8cfar8s42v") (f (quote (("default"))))))

