(define-module (crates-io mk dt mkdtemp) #:use-module (crates-io))

(define-public crate-mkdtemp-0.1.0 (c (n "mkdtemp") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0d1di672cpg9566af3zl6h2cbyh43b80d920c6laq1ww84377clk")))

(define-public crate-mkdtemp-0.2.0 (c (n "mkdtemp") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0acykhlhc0s7pw151n6l4q3jgddbr57sc2iwk0a80yilq8aazyf1")))

