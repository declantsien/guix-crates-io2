(define-module (crates-io mk bo mkbookpdf) #:use-module (crates-io))

(define-public crate-mkbookpdf-1.0.0 (c (n "mkbookpdf") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^0.11") (d #t) (k 2)) (d (n "lopdf") (r "^0.23.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1sv0jppcs5rnqxyrqcpsmcw5dq9qjlwqxdjqw0c4l5759w98z496")))

(define-public crate-mkbookpdf-1.1.0 (c (n "mkbookpdf") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^0.12") (d #t) (k 2)) (d (n "lopdf") (r "^0.23.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0mwkm6w26fjqv773awzia0l81hwi3zrmy0cq0jfaaj70g47wnqp9")))

