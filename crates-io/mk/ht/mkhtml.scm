(define-module (crates-io mk ht mkhtml) #:use-module (crates-io))

(define-public crate-mkhtml-2.0.0 (c (n "mkhtml") (v "2.0.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "12hky6krki03kcd3laki8k3c5bc3xz4k951przf64y0xwjiwjiq8")))

(define-public crate-mkhtml-3.0.0 (c (n "mkhtml") (v "3.0.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0wy36kb2xlzhv6yq3n3nhkib7242im5jcgfcxzqfb6qhv6ah2lk2")))

(define-public crate-mkhtml-3.1.0 (c (n "mkhtml") (v "3.1.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1dnr4vh1h9r4x9knlhkc793snzjzciwbgm3w2irzcg6c2pvr2v13")))

(define-public crate-mkhtml-3.2.0 (c (n "mkhtml") (v "3.2.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "123k24h5p2cz34s9pvjafvwdby7w973jijjxw6h8i08dxlwxgizc")))

(define-public crate-mkhtml-3.3.0 (c (n "mkhtml") (v "3.3.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0bmh5z75s6kj780yg4951gyyy3qcdp3s6hzsrzghggj18iprzq0z")))

(define-public crate-mkhtml-3.3.1 (c (n "mkhtml") (v "3.3.1") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1p8chf36dvfhphlw5wi2hcr2q3x84cm3ghqpqcimvdyk0yzhab8b")))

(define-public crate-mkhtml-3.3.2 (c (n "mkhtml") (v "3.3.2") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "17793jv3cnqbnxahkjml8d5n19a15p4ylhmx9rfcv2fg55zygcn8")))

(define-public crate-mkhtml-3.4.0 (c (n "mkhtml") (v "3.4.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1lqxy8jsyfl7rfzlz82dkpn4kpcd1z6rvvyiv9j1fjcgam8v2srh")))

