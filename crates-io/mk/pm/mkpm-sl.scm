(define-module (crates-io mk pm mkpm-sl) #:use-module (crates-io))

(define-public crate-mkpm-sl-0.1.0 (c (n "mkpm-sl") (v "0.1.0") (d (list (d (n "git2") (r "^0.13.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0zbdwr73kff6hbhbjj6fmghiq6sga5m6xi38zp01q2iv3ij3qh5p")))

