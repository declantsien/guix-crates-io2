(define-module (crates-io mk pm mkpm-cl) #:use-module (crates-io))

(define-public crate-mkpm-cl-0.1.0 (c (n "mkpm-cl") (v "0.1.0") (d (list (d (n "mkpm-sl") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "12lrm9gj5hgbfn6jwp7p9l8hxhc3pgazlc2m3zfpmr0wyh66hrxn")))

(define-public crate-mkpm-cl-0.1.1 (c (n "mkpm-cl") (v "0.1.1") (d (list (d (n "mkpm-sl") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0b3v559wqpprgwacb88hbd44d9jdpgh59z0gl86gdl7ld0s0wpy2")))

