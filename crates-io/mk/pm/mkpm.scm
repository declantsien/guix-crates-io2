(define-module (crates-io mk pm mkpm) #:use-module (crates-io))

(define-public crate-mkpm-0.1.0 (c (n "mkpm") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.17.0-beta.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0y3mj1p4wnflhx494bsyz5mzsgv13ka7rsdyq5m2vr0ycikaz9gn") (y #t)))

(define-public crate-mkpm-0.1.1 (c (n "mkpm") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.17.0-beta.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fgb1vh7f6j2kfmf39q38iy5qh15s88lr6jkd24i4j5wm3qk1kmb") (y #t)))

(define-public crate-mkpm-0.1.2 (c (n "mkpm") (v "0.1.2") (d (list (d (n "indicatif") (r "^0.17.0-beta.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c4kqln8yxwd81k2qrpllrj0xvw6wm85n3ds4mx07xgfa15dak7d") (y #t)))

