(define-module (crates-io mk sw mkswap) #:use-module (crates-io))

(define-public crate-mkswap-0.1.0 (c (n "mkswap") (v "0.1.0") (d (list (d (n "hex-slice") (r "^0.1.4") (d #t) (k 2)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "14yrqpqh0vdp5wx9yx11r6gvw1rc5ah3na16m8vvipgmiw00h7c8")))

(define-public crate-mkswap-0.1.1 (c (n "mkswap") (v "0.1.1") (d (list (d (n "hex-slice") (r "^0.1.4") (d #t) (k 2)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "11kig9d12y4bnwyaipy5y0b1m9gr6r16c6zd7arhlma665p1c6qa")))

