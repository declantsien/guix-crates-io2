(define-module (crates-io mk i_ mki_fork) #:use-module (crates-io))

(define-public crate-mki_fork-0.2.1 (c (n "mki_fork") (v "0.2.1") (d (list (d (n "input") (r "^0.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "uinput") (r "^0.1.3") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11") (r "^2") (f (quote ("xlib" "xtest"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1p90vg2nq9wndxakaqn6i3646l4lshl4mqww6cd95zx0hf4fjakh")))

