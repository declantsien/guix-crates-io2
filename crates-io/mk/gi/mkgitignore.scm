(define-module (crates-io mk gi mkgitignore) #:use-module (crates-io))

(define-public crate-mkgitignore-0.2.0 (c (n "mkgitignore") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1slndkx4yx127aznxcaj21mbddqyalvsvkqy43rb7p1wc4wms67y")))

