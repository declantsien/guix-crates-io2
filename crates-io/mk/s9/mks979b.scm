(define-module (crates-io mk s9 mks979b) #:use-module (crates-io))

(define-public crate-mks979b-0.1.0 (c (n "mks979b") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "itsybitsy_m4") (r "^0.7.0") (f (quote ("usb"))) (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "usbd-serial") (r "^0.1.1") (d #t) (k 2)))) (h "1hjx438cxx4csvvyg2bf1x074arp7rvwagkvsmk0khsba6xjrpzb") (r "1.57")))

