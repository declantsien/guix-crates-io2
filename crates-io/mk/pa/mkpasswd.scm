(define-module (crates-io mk pa mkpasswd) #:use-module (crates-io))

(define-public crate-mkpasswd-0.1.0 (c (n "mkpasswd") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.4.0") (d #t) (k 0)) (d (n "rand_os") (r "^0.1.3") (d #t) (k 0)))) (h "1ci718fvz8wkxmmv5bg9304vzbv0hhl2g0csn1fizmpyijwnr3b7")))

(define-public crate-mkpasswd-0.2.0 (c (n "mkpasswd") (v "0.2.0") (d (list (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "rand_os") (r "^0.2.1") (d #t) (k 0)))) (h "15zk0sda6zrvgbvb0p4ir4ldw1zin2fb198kn987wci2w1dvr2lj")))

(define-public crate-mkpasswd-0.3.0 (c (n "mkpasswd") (v "0.3.0") (d (list (d (n "rand_core") (r "^0.5") (f (quote ("std"))) (d #t) (k 0)))) (h "18jcfcxj267dj37zz5b5cq3fjcn386d528kjc0cjll91lkmwq8a8")))

