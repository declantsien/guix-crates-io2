(define-module (crates-io mk pa mkpass) #:use-module (crates-io))

(define-public crate-mkpass-0.1.0 (c (n "mkpass") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kra92inghv7sc13zrqlir8m0gyr7szjxcx8klzvmpixqqf91sz4")))

(define-public crate-mkpass-0.1.1 (c (n "mkpass") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16s3h2x1s46vgx3y1dl5xb08r2xmf5v19blgxi62d4dq868fcb0g")))

