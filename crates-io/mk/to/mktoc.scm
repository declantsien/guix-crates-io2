(define-module (crates-io mk to mktoc) #:use-module (crates-io))

(define-public crate-mktoc-1.0.0 (c (n "mktoc") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1if2022pdnrmm3blcbk1cixy1x650ssha5nsrszs8a2fj2a8cdia")))

(define-public crate-mktoc-1.1.0 (c (n "mktoc") (v "1.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1wihjxpcpj1cfwi8ass791nyckni60zfhwvpdglgihkaqn6j9vai")))

(define-public crate-mktoc-1.1.1 (c (n "mktoc") (v "1.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1nvlwlqlllig6qdc5sn6a45dx3s4x009n7w28fv5w7q7pp9xpx1d")))

(define-public crate-mktoc-1.1.2 (c (n "mktoc") (v "1.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1jcz4blnjfnnbmqc4hkdx2vlpsm2nvgi77qv3iz8wpfzkzbf2cmb")))

(define-public crate-mktoc-2.0.0 (c (n "mktoc") (v "2.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1942l186wkzc02nw0bn72zn7p00k90hi22j7h9wha62hvym2msl5")))

(define-public crate-mktoc-2.1.0 (c (n "mktoc") (v "2.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "05iqfsyjljsdl1hssmkak5mqwjcbg2hhkyk4wkw0cfg9hr9181az")))

(define-public crate-mktoc-2.2.0 (c (n "mktoc") (v "2.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1d14bkn6ymmfgwdd1dyscz5j5yfyqwlwa7p74aryi2jh5gs3qz1h")))

(define-public crate-mktoc-2.2.1 (c (n "mktoc") (v "2.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "04sbin0iw1a27w110wq4rk4v6yvw4m651vhbilbfxcsgar9r7dk0")))

(define-public crate-mktoc-3.0.0 (c (n "mktoc") (v "3.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1gqh2m7351adq94mgj8xmb5db5ksmj270ysk2im65j9c52ii9icw")))

(define-public crate-mktoc-3.1.0 (c (n "mktoc") (v "3.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0krv92dj25xd1fbs66bx0wf8hj500k1zg98cd7izpwpw1xi60lpx")))

(define-public crate-mktoc-3.2.0 (c (n "mktoc") (v "3.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cawwc3nkfvbrgypyi6crshqqyfa6y93j33a3kyfbfq1g1xwh5ph")))

(define-public crate-mktoc-3.2.1 (c (n "mktoc") (v "3.2.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09rdvgck7gd338d47fg108azkpf9xbqhkv4l59y9rls0pa4kn3yr")))

(define-public crate-mktoc-3.3.0 (c (n "mktoc") (v "3.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "160pnxxdjs1f65q81rxynxlc1w61rq9cclnvg8msph5ff0czgz5p")))

(define-public crate-mktoc-4.0.0 (c (n "mktoc") (v "4.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0isr07pwvrn228r7i8h6vllllrbsbv5k6cviggqx0r6kmgww59sq") (r "1.64.0")))

