(define-module (crates-io mk fi mkfile) #:use-module (crates-io))

(define-public crate-mkfile-0.2.1 (c (n "mkfile") (v "0.2.1") (h "1fr7aaf1scxnr1lwf8xm3a3q27b7frbvb51w4zdp0hs1ahh9wp3g")))

(define-public crate-mkfile-0.3.0 (c (n "mkfile") (v "0.3.0") (h "0k8vd30aj2c5gg7brzxjjkprrdfvwnv9ncdyw3390x0gcf75xd8s")))

