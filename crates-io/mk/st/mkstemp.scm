(define-module (crates-io mk st mkstemp) #:use-module (crates-io))

(define-public crate-mkstemp-0.1.0 (c (n "mkstemp") (v "0.1.0") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "0avfgfbhhach6mfrch3zfcyjln7kh8kshy9rjxzn15x3is4f7j03") (y #t)))

(define-public crate-mkstemp-0.2.0 (c (n "mkstemp") (v "0.2.0") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "17ql7cp156hnw5b96bq67gazzav261ykyrvd4i5crjw4vfbqf7vg") (y #t)))

