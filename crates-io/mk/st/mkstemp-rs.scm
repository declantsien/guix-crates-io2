(define-module (crates-io mk st mkstemp-rs) #:use-module (crates-io))

(define-public crate-mkstemp-rs-0.0.1 (c (n "mkstemp-rs") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "188zvydfvmc5pj8ifjp2ylfwv1zsg30n9aynyc972is3i0dpbgi4")))

(define-public crate-mkstemp-rs-0.0.2 (c (n "mkstemp-rs") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fkq4b0jxbd7a9nxcvdm9j3rm24k2xzifjzv0485zkf4dhkan1hg")))

(define-public crate-mkstemp-rs-1.0.0 (c (n "mkstemp-rs") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kk0g83lxv7d0aahyg5csi6kp7mnhrzpakmmgs30kirh19yxl1mb")))

