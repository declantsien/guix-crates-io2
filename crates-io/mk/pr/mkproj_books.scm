(define-module (crates-io mk pr mkproj_books) #:use-module (crates-io))

(define-public crate-mkproj_books-0.1.0 (c (n "mkproj_books") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dz71rkagi8iry7qa1lpk0sk1k6awv778siclgby998q6vm6i9rq")))

