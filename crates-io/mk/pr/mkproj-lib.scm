(define-module (crates-io mk pr mkproj-lib) #:use-module (crates-io))

(define-public crate-mkproj-lib-0.1.0 (c (n "mkproj-lib") (v "0.1.0") (h "0s2y3mj7hrjznrdw50xl5z8bbbwg1djls4vj5ys5x859af7sn1ra")))

(define-public crate-mkproj-lib-0.1.1 (c (n "mkproj-lib") (v "0.1.1") (h "1j887imn3137h4nx4f0ds4f9ksdxx3sbm7k7xpjglmy0ba66mi63")))

(define-public crate-mkproj-lib-0.1.2 (c (n "mkproj-lib") (v "0.1.2") (h "013j3dgnqffmpm2dry6zzwbyapz126l0qypk1c942xmdhjsxnrra")))

(define-public crate-mkproj-lib-0.1.3 (c (n "mkproj-lib") (v "0.1.3") (h "1q6hnsi2yva15kr8hkh8qqqg5fw3kcf84awlxnbwj8wzzkz0criv")))

(define-public crate-mkproj-lib-0.1.4 (c (n "mkproj-lib") (v "0.1.4") (h "1zpz45ln2cgr9nq8684n2nayba2njmkx3i9l5i84h8z7cqm5swsf")))

(define-public crate-mkproj-lib-0.1.5 (c (n "mkproj-lib") (v "0.1.5") (h "0m8hdxylc46m2pp1ffbdnj5lvpkv3vzv6kfz1grbfkl545n02caq")))

(define-public crate-mkproj-lib-0.1.6 (c (n "mkproj-lib") (v "0.1.6") (h "06irh2nya17g5h4zdy8xnmmps2bz3nzaxsp89c1b4ackd774d0ix")))

(define-public crate-mkproj-lib-0.1.7 (c (n "mkproj-lib") (v "0.1.7") (h "036cwk8qcd6jsfs9dcbx1y9v2d1vkkslnpjvhi3yq3v4qw46yhvd")))

(define-public crate-mkproj-lib-0.1.8 (c (n "mkproj-lib") (v "0.1.8") (h "07gmf8k0ssnh0isaidzblr7wvbdhyp1wij4nj52xvb0mnz24108c")))

(define-public crate-mkproj-lib-0.1.9 (c (n "mkproj-lib") (v "0.1.9") (h "155hfw489009fv8cba4m5g6x7wxzkscd3w5iqbbzk2cl2bd0a1sa")))

(define-public crate-mkproj-lib-0.1.10 (c (n "mkproj-lib") (v "0.1.10") (h "0ikwfzqlbcn29abg3yh3g2m7d0hr0x2p085gs9h79wmn2r7f12xl")))

(define-public crate-mkproj-lib-0.1.11 (c (n "mkproj-lib") (v "0.1.11") (h "1hg24an6gws3gyikn1yjdr9dpq3jmj3nb7ckv4rcav3kvlc5ql4x")))

(define-public crate-mkproj-lib-0.1.12 (c (n "mkproj-lib") (v "0.1.12") (h "1zdrx1l5xjan1m1wi4szfqigghj6ql9hkyz6ni9vx016h1ws15bp")))

(define-public crate-mkproj-lib-0.1.13 (c (n "mkproj-lib") (v "0.1.13") (h "1l7jqc8hkq6yszs8lhbhlfvhpg33jby3illzs14s3qllifm9scjs")))

(define-public crate-mkproj-lib-0.2.0 (c (n "mkproj-lib") (v "0.2.0") (h "0hvksw53bfrykpcmpzklvwwivc22hmvjv23lsbq14hhy4id4z5d7")))

(define-public crate-mkproj-lib-0.2.1 (c (n "mkproj-lib") (v "0.2.1") (h "0ii9sg31gvp4vf2cyl8a6mk525yi67llbrirpmr6yx8x15yqj7np")))

(define-public crate-mkproj-lib-0.2.2 (c (n "mkproj-lib") (v "0.2.2") (h "0wghbs8hp8i29mqr38l889l64nv3k3098c5wv2n67srg27j9qbww")))

(define-public crate-mkproj-lib-0.2.3 (c (n "mkproj-lib") (v "0.2.3") (h "0k8d0bkl2im822jw1qvv8yy31vvv733dz48ahl0gd7jbd1jswfkn")))

(define-public crate-mkproj-lib-0.2.4 (c (n "mkproj-lib") (v "0.2.4") (h "1zppphs74vpi0bzdzhs82sibqd7b03chkaa3avdhk7rcyds41n8f")))

(define-public crate-mkproj-lib-0.2.5 (c (n "mkproj-lib") (v "0.2.5") (h "0g7m7vdv5jw4h51la7f4g19cvqqib4lqj1rz6gwldgwd3a387fs3")))

(define-public crate-mkproj-lib-0.2.6 (c (n "mkproj-lib") (v "0.2.6") (h "0mrkygzbzhf264inhd9kisk5v9l2abihd8qry3fwnsk64dml70hf")))

(define-public crate-mkproj-lib-0.2.7 (c (n "mkproj-lib") (v "0.2.7") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1wlbxqkxnjnasrrmhsgd0da4y0b0ywh1gfgw44pcd5m8z53jkq8y")))

(define-public crate-mkproj-lib-0.2.8 (c (n "mkproj-lib") (v "0.2.8") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1x5rb07gk2ww0mzpiy5v0dzq7694zfrfg4hrgy8h1hlmplvm1z2x")))

(define-public crate-mkproj-lib-0.2.9 (c (n "mkproj-lib") (v "0.2.9") (h "1x561qzkrq3sndg3i3p4zgjxa9ii41f8g4p3d2xvpk0xbh1nkl32")))

(define-public crate-mkproj-lib-0.2.10 (c (n "mkproj-lib") (v "0.2.10") (h "0gq86kfgch9g6hnfd186dka340d3mfsqjxfmmq5sbfyjrf838jws")))

