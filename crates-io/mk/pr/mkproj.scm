(define-module (crates-io mk pr mkproj) #:use-module (crates-io))

(define-public crate-mkproj-0.1.0 (c (n "mkproj") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1192y2nljy1pvl35fq5x5wvrqzq32ijahjvsipiv8p9vm1y0ckkf") (y #t)))

