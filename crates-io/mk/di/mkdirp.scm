(define-module (crates-io mk di mkdirp) #:use-module (crates-io))

(define-public crate-mkdirp-0.1.0 (c (n "mkdirp") (v "0.1.0") (h "01yajs9mclysq4nzwi19pw635xfzsqz061ga6xw7s677ya8bm5lp")))

(define-public crate-mkdirp-1.0.0 (c (n "mkdirp") (v "1.0.0") (h "1w1p232cj8fwg2479b4k8dfx9r57ashsw8kqvk98dcr99kk1skl6")))

