(define-module (crates-io mk up mkups) #:use-module (crates-io))

(define-public crate-mkups-0.1.0 (c (n "mkups") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc") (r "^2.0") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (f (quote ("mman"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1frlj15im9227xm9s8w7ks0xz446mxijpk8rsn684362qwl8p4mv")))

