(define-module (crates-io mk l2 mkl25z4) #:use-module (crates-io))

(define-public crate-mkl25z4-0.0.1 (c (n "mkl25z4") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">= 0.5.0, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1hi9smympx01da6dmp5v4s72rbdg4q4bxyx18fnsgg2dqgcwnq9j") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mkl25z4-0.0.3 (c (n "mkl25z4") (v "0.0.3") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">= 0.5.0, < 0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1mqpa082hlzavpj5jb2fi072sa2zvwpiwj1phgf1ysrly62v3yci") (f (quote (("rt" "cortex-m-rt/device"))))))

