(define-module (crates-io mk l2 mkl26z4) #:use-module (crates-io))

(define-public crate-mkl26z4-0.1.0 (c (n "mkl26z4") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1bpqvk4qzx3ka898aqdmab5lb45a7i0k0qv0jk4lnsmziygpx9p9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mkl26z4-0.1.1 (c (n "mkl26z4") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1g20s2zrfdcwh3dhgz4xiy9mhf8f9srz6jgym4ma700q9qd8sa9z") (f (quote (("rt" "cortex-m-rt/device"))))))

