(define-module (crates-io mk _d mk_desktop) #:use-module (crates-io))

(define-public crate-mk_desktop-1.0.0 (c (n "mk_desktop") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12knay15wy9vbcy6c0054hvzhk08c9k7b2c2ysgkjci6d7bdqh74")))

(define-public crate-mk_desktop-1.0.1 (c (n "mk_desktop") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14kky6nl2q5fwsw8xac04d3qwgasaj0hzy2jzh23xn3g36xnnivv")))

(define-public crate-mk_desktop-1.1.0 (c (n "mk_desktop") (v "1.1.0") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qxf5j8iavcv9sy6n4z0j7yzfwz3y4knqg8q6dxm0xiw4byif2c1")))

(define-public crate-mk_desktop-1.2.0 (c (n "mk_desktop") (v "1.2.0") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "requestty") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spinach") (r "^2") (d #t) (k 0)))) (h "0p83y8myv83hwd3xrccyd7lfhy2vgzfyqaqfxxaxsw591gbhcr0g")))

(define-public crate-mk_desktop-1.2.1 (c (n "mk_desktop") (v "1.2.1") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "requestty") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spinach") (r "^2") (d #t) (k 0)))) (h "0kap4cdxslqmw7xdzzqmlg2aa7zm1637y62y6vgw2fylmshz8d1y")))

(define-public crate-mk_desktop-1.2.2 (c (n "mk_desktop") (v "1.2.2") (d (list (d (n "clap") (r "^3.1.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "requestty") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spinach") (r "^2") (d #t) (k 0)))) (h "0p2c9j0ab4qb7g5npvfp4blvpv5k30w2nwjg3aw837wn51hldkx5")))

