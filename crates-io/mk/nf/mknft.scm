(define-module (crates-io mk nf mknft) #:use-module (crates-io))

(define-public crate-mknft-0.1.0 (c (n "mknft") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "psd") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "1qqmv7f6w4ynqicg4cgw557bf3djyxx10yfabpjhjs92d4lw64pa")))

