(define-module (crates-io mk he mkheaders) #:use-module (crates-io))

(define-public crate-mkheaders-0.1.0 (c (n "mkheaders") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "strsim") (r "^0.10.0") (d #t) (k 0)))) (h "1yj7hi005hi0mqq7901kkpbbg9s57l67b71qwwrmf5nf3129ccvi")))

