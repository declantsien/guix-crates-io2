(define-module (crates-io mk do mkdosfs) #:use-module (crates-io))

(define-public crate-mkdosfs-0.1.0 (c (n "mkdosfs") (v "0.1.0") (h "10jjhh1gywbi9wyh7hngpbj2sxvia1z8wb36d8zr79f22zsljwk1") (y #t)))

(define-public crate-mkdosfs-0.1.1 (c (n "mkdosfs") (v "0.1.1") (h "00r4s02ymhzj92jfilylzlpw4jp2a000nhr1mp70l0kp4yvyfy5n") (y #t)))

(define-public crate-mkdosfs-0.1.2 (c (n "mkdosfs") (v "0.1.2") (h "002q5i5rz8i4sxn2jvki8dhnrp0cvkmpjhg43v6ykpg4vi4dv657") (y #t)))

(define-public crate-mkdosfs-0.1.3 (c (n "mkdosfs") (v "0.1.3") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0sh9bvrrxlilg7lr0ywwc28nnzdrdyfx1mm8j1z0djjiy36nbbh4") (y #t)))

(define-public crate-mkdosfs-0.2.0 (c (n "mkdosfs") (v "0.2.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "199sqydvzasha8r3n9gn5iayyznqjbq4rj5m3nnd1fcsfcycr18i")))

(define-public crate-mkdosfs-0.2.1 (c (n "mkdosfs") (v "0.2.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1b1gvhskjlw33714xg5pqryci5pflzd5wklvz0ncqkb6c5nd9w5z")))

(define-public crate-mkdosfs-0.2.2 (c (n "mkdosfs") (v "0.2.2") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1k23rvs33lcb6jp9nk6vqn3hvsh95wj8jz011pqrp69pzprskcvf")))

(define-public crate-mkdosfs-0.2.3 (c (n "mkdosfs") (v "0.2.3") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1gcriw2bhjbmqalii1c6wsfc1ryaxg9hdkf96rg1ymdwj8w0xqbi")))

(define-public crate-mkdosfs-0.2.4 (c (n "mkdosfs") (v "0.2.4") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.1") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1sbmb0a3wmixnx9zcx7k2mky402x7g4q3kxkhp5wyxp0pb0g31in")))

(define-public crate-mkdosfs-0.2.5 (c (n "mkdosfs") (v "0.2.5") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "time") (r "^0.3.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.14") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "04i1pnd91dyan5j055k2gvlrq9bkfzma35gvw0c8yfqcc8j67fq6")))

