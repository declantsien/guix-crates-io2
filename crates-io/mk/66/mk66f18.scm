(define-module (crates-io mk #{66}# mk66f18) #:use-module (crates-io))

(define-public crate-mk66f18-0.1.0 (c (n "mk66f18") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1df36yai5nvraxxms9zdc8n3n0h8xaw6ic1n0ig7p02ik6yidjq8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mk66f18-0.2.0 (c (n "mk66f18") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "13v7dyg0l0q391v8zrnc8k2jby372hxff0zdrk6y4gz6y6hnm2zk") (f (quote (("rt" "cortex-m-rt/device"))))))

