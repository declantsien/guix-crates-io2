(define-module (crates-io mk _c mk_console) #:use-module (crates-io))

(define-public crate-mk_console-0.1.0 (c (n "mk_console") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)))) (h "1nl2yqxjcnfh0ih1gghwvbmd4srdar8v18pbhqddkqrhhyj7snl1")))

(define-public crate-mk_console-0.1.1 (c (n "mk_console") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)))) (h "1frk4cjxpc7djjnpxc5irmx37sa93fk78qj4w0b8jpsrvn03asry")))

(define-public crate-mk_console-0.1.2 (c (n "mk_console") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)))) (h "0myfflmd2jrv3g9r19867a4lc1gy4701im5d1p4m60whswpag33f")))

