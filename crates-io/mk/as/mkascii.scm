(define-module (crates-io mk as mkascii) #:use-module (crates-io))

(define-public crate-mkascii-0.1.0 (c (n "mkascii") (v "0.1.0") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)) (d (n "termsize") (r "^0.1.6") (d #t) (k 0)))) (h "1iwhc369qkaci4rr6l69bs9hx0bf48x5ghm6v9p35sd4wrx4ncvj")))

