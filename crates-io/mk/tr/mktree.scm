(define-module (crates-io mk tr mktree) #:use-module (crates-io))

(define-public crate-mktree-0.1.0 (c (n "mktree") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "egui") (r "^0.18.1") (d #t) (k 0)))) (h "06m2x2759jasdwk9pkvw954y3a1ycqhdhl1jj9l8wlfv1hdzcwq5") (y #t)))

(define-public crate-mktree-0.1.1 (c (n "mktree") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "egui") (r "^0.18.1") (d #t) (k 0)))) (h "00cvxlxs78wx854gkwwcgmmj8r6drr4ky6yxmawxmmjaa0mpiisi") (y #t)))

(define-public crate-mktree-0.1.2 (c (n "mktree") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "egui") (r "^0.18.1") (d #t) (k 0)))) (h "0rnnf1xvn5n9z42gin5rsasbhafbxr5zn9lg3352r9gh9d0kdl20") (y #t)))

(define-public crate-mktree-0.1.3 (c (n "mktree") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "egui") (r "^0.18.1") (d #t) (k 0)))) (h "0c8z41fc09bbqk68zlvvrkv5c4x4hkgc2v826zjfxn10xyy1xks6") (y #t)))

(define-public crate-mktree-0.1.4 (c (n "mktree") (v "0.1.4") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "egui") (r "^0.18.1") (d #t) (k 0)))) (h "01nxjiz7yrad65pha2nzwcl6cf9vqn8mjcf181cvy4jjlyc7q9s3") (y #t)))

(define-public crate-mktree-0.2.0 (c (n "mktree") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "egui") (r "^0.18.1") (d #t) (k 0)))) (h "04cwg8a0dgi0g6vyz0ls9400l9118anmvxf10i0ddc3crpc64s25") (y #t)))

(define-public crate-mktree-0.3.0 (c (n "mktree") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "12p6j0w9awcq6ay5ypwqr8ym1mzah793i1fns1w1dfs02vj837fx") (y #t)))

(define-public crate-mktree-0.4.0 (c (n "mktree") (v "0.4.0") (d (list (d (n "aquamarine") (r "^0.2.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "170vsszmxwiplihc4r31f5gwxb4ym01c90wxj2y0zcxhmb1j9bbm") (f (quote (("zou") ("postgres") ("mongo"))))))

(define-public crate-mktree-0.5.0 (c (n "mktree") (v "0.5.0") (d (list (d (n "aquamarine") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1cmkfdl5ja95qmhk1qfy45b0810vibn29k8ycphdb3y9jr8wl7js") (f (quote (("zou") ("postgres") ("mongo"))))))

(define-public crate-mktree-0.6.0 (c (n "mktree") (v "0.6.0") (d (list (d (n "aquamarine") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mkentity") (r "^0.1.0") (d #t) (k 0)))) (h "0y99ydj73x9spcjxhlyzdkyjkd2003y5n2vm2ybrdw85wym21ibn") (f (quote (("zou" "mkentity/zou") ("postgres" "mkentity/postgres") ("mongo" "mkentity/mongo"))))))

(define-public crate-mktree-0.6.1 (c (n "mktree") (v "0.6.1") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "mkentity") (r "^0.1.0") (d #t) (k 0)))) (h "1f1gyk7lari81am9h7wc67g12992s7ry2wgw648jagbw6fsp9q1y") (f (quote (("zou" "mkentity/zou") ("postgres" "mkentity/postgres") ("mongo" "mkentity/mongo"))))))

