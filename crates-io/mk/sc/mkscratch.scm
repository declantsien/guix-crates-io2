(define-module (crates-io mk sc mkscratch) #:use-module (crates-io))

(define-public crate-mkscratch-1.0.0 (c (n "mkscratch") (v "1.0.0") (h "1f4lrbzid8pyk93vailnjh7833qv8cmxg8ny5b7j3skk5k9k429v")))

(define-public crate-mkscratch-1.1.0 (c (n "mkscratch") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1n9j91fbrigdlyrc2kznvz4zx7v6zpiwmvn95pwzabjw9y33xph8")))

