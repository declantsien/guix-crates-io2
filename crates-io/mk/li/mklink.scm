(define-module (crates-io mk li mklink) #:use-module (crates-io))

(define-public crate-mklink-0.1.0 (c (n "mklink") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "junction") (r "^0.1.2") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (f (quote ("paw"))) (d #t) (k 0)))) (h "1c8zvy9i8bgdwnb55k5p78zcxcpks6xj28g0iyclpbw925abi1xs")))

