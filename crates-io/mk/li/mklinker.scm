(define-module (crates-io mk li mklinker) #:use-module (crates-io))

(define-public crate-mklinker-0.1.0 (c (n "mklinker") (v "0.1.0") (h "0hqrcfc5ccqaq1cscw4lvr7qbj2032xx8xg06069v8dz2ds39afd") (r "1.78")))

(define-public crate-mklinker-0.1.1 (c (n "mklinker") (v "0.1.1") (h "11kiy07n4licjg6qyx4rvb3z8h3hdcz4yqfm9pw6g8za4vvbrzkw") (r "1.78")))

(define-public crate-mklinker-0.1.2 (c (n "mklinker") (v "0.1.2") (d (list (d (n "aarch64_pgtable") (r "^0.1") (k 0)))) (h "0dy637pk78pcbyp43dqyw9fx5nksxflbdg27m5gl8rla4gkg7fa2") (r "1.78")))

(define-public crate-mklinker-0.1.3 (c (n "mklinker") (v "0.1.3") (d (list (d (n "aarch64_define") (r "^0.1") (k 0)))) (h "10k3ivljjj9xcwd0bly91qnpkw39s7w1k0wlvp1anr1jf9askd0y") (r "1.78")))

(define-public crate-mklinker-0.1.4 (c (n "mklinker") (v "0.1.4") (d (list (d (n "aarch64_define") (r "^0.1") (k 0)))) (h "1s9x5rsy2r3gzbi9f4iss1sqxddj30cs9jdkqn1k2l6wccfxdkkh") (r "1.78")))

(define-public crate-mklinker-0.1.5 (c (n "mklinker") (v "0.1.5") (d (list (d (n "aarch64_define") (r "^0.1") (k 0)))) (h "0hfh0ci9vpapvvqlqxqvnna6vq7aczw2w71iicamwv3gvazrx0cj") (r "1.78")))

