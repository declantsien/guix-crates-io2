(define-module (crates-io mk #{8d}# mk8dx_lounge_tag_extractor) #:use-module (crates-io))

(define-public crate-mk8dx_lounge_tag_extractor-0.0.1 (c (n "mk8dx_lounge_tag_extractor") (v "0.0.1") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "0576kgj75ik0xr508shr5lfcyd4jp1zd0zwbyqhn23fb0wpkd5x3")))

(define-public crate-mk8dx_lounge_tag_extractor-0.0.2 (c (n "mk8dx_lounge_tag_extractor") (v "0.0.2") (d (list (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "1z9n164pi3pgj4gbv4ipa4sigbs28pb3x7xwmmjfmqb1vsdnbl13")))

