(define-module (crates-io mk is mkisofs-rs) #:use-module (crates-io))

(define-public crate-mkisofs-rs-0.1.0 (c (n "mkisofs-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1ni78jl00jsyf553x6wcv07ynn85x7mcqplbkx78hgb5ar84frhf")))

(define-public crate-mkisofs-rs-0.1.1 (c (n "mkisofs-rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1fg55xsa3zqnvman1ybna3frjlp8ci3a02xddq9gz8jvfaq6z3px")))

(define-public crate-mkisofs-rs-0.1.2 (c (n "mkisofs-rs") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1w06ip3sxipxzgy2v6mnmh84bvh6pa18h9i31c0bgi2qp1kgdsb1")))

(define-public crate-mkisofs-rs-0.2.0 (c (n "mkisofs-rs") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0bqqkk106dj93p04j3c5ybfy53nrcly79kyqxp8vhnm355r9r4ql")))

