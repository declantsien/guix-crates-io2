(define-module (crates-io mk sl mksls) #:use-module (crates-io))

(define-public crate-mksls-1.0.0 (c (n "mksls") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.6.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0gpwkh0r64fgiv33c0d0yybijbpk40mg0l6cpgwxk9jmfwaja824")))

