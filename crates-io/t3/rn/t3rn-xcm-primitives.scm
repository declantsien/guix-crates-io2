(define-module (crates-io t3 rn t3rn-xcm-primitives) #:use-module (crates-io))

(define-public crate-t3rn-xcm-primitives-0.3.0 (c (n "t3rn-xcm-primitives") (v "0.3.0") (d (list (d (n "codec") (r "^3.1.5") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^4.0.0") (k 0)) (d (n "xcm") (r "^0.9.27") (k 0)) (d (n "xcm-executor") (r "^0.9.27") (o #t) (k 0)))) (h "1azipv19ksf7ab1qylw2ij7n46d97qa0c1g0lms4gv96bqp5jczl") (f (quote (("std" "xcm/std" "codec/std" "sp-std/std") ("frame-std" "xcm-executor/std") ("frame" "xcm-executor") ("default" "std" "frame-std"))))))

(define-public crate-t3rn-xcm-primitives-0.3.1 (c (n "t3rn-xcm-primitives") (v "0.3.1") (d (list (d (n "codec") (r "^3.1.5") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^4.0.0") (k 0)) (d (n "xcm") (r "^0") (k 0)) (d (n "xcm-executor") (r "^0") (o #t) (k 0)))) (h "0b1bq7a0x49qvjhb9p19zh0wvz0bx7ay6a3ihcf8f7hpkyp2yqi8") (f (quote (("std" "xcm/std" "codec/std" "sp-std/std") ("frame-std" "xcm-executor/std") ("frame" "xcm-executor") ("default" "std" "frame-std"))))))

(define-public crate-t3rn-xcm-primitives-0.3.2 (c (n "t3rn-xcm-primitives") (v "0.3.2") (d (list (d (n "codec") (r "^3.1.5") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^4.0.0") (k 0)) (d (n "xcm") (r "^0") (k 0)) (d (n "xcm-executor") (r "^0") (o #t) (k 0)))) (h "19ka5616qg3xw0aqn3va6vlj50x0x8ianjydj4w14k643i8kf2l6") (f (quote (("std" "xcm/std" "codec/std" "sp-std/std") ("frame-std" "xcm-executor/std") ("frame" "xcm-executor") ("default" "std" "frame-std"))))))

