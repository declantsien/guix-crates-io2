(define-module (crates-io ws #{2m}# ws2markdown) #:use-module (crates-io))

(define-public crate-ws2markdown-0.2.0 (c (n "ws2markdown") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "rfd") (r "^0.11") (d #t) (k 0)))) (h "0j34n2wj6apn7zbw2sjplkvi074ldik4l84575ff80cjiixn2pba")))

