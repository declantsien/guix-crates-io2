(define-module (crates-io ws -m ws-mock) #:use-module (crates-io))

(define-public crate-ws-mock-0.0.1 (c (n "ws-mock") (v "0.0.1") (d (list (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1cs9xrd6bkwqfx0wzb7z4ziwl3zfkajlfkd2w6s1bbkrqc5w2dpq")))

(define-public crate-ws-mock-0.1.0 (c (n "ws-mock") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "13m941wc181xzs98jbbiga51pk3kqgyf3lci8xm991shpbxz3fkr")))

(define-public crate-ws-mock-0.1.1 (c (n "ws-mock") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0kpgx8nw91c7ws81gf33gnkv8ydcif1rsac5si2dm7zsz81w0w54")))

