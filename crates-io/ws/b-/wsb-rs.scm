(define-module (crates-io ws b- wsb-rs) #:use-module (crates-io))

(define-public crate-wsb-rs-0.1.0 (c (n "wsb-rs") (v "0.1.0") (h "0ggpy2w4yrw3vl5gc1pynj1yrphbhjkhmln8rwqwfvjk9bi11b6d")))

(define-public crate-wsb-rs-0.1.1 (c (n "wsb-rs") (v "0.1.1") (h "01jdggpx61sz01893p8vmlgz6mxr3s58bq0kkvv6rr7j15gir7xh")))

(define-public crate-wsb-rs-0.1.2 (c (n "wsb-rs") (v "0.1.2") (h "0f3k8g0880ca9d20xird5indq493afvhbfspj43f5yyjqmjizlzp")))

