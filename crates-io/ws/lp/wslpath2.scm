(define-module (crates-io ws lp wslpath2) #:use-module (crates-io))

(define-public crate-wslpath2-0.0.1 (c (n "wslpath2") (v "0.0.1") (h "0g22ql7xqrm7h8ybhvzvrzj0aiy3bj8qrppzy4mifswhads37689") (y #t)))

(define-public crate-wslpath2-0.1.0 (c (n "wslpath2") (v "0.1.0") (h "10mi5zzknnvqr1rfxwajr2n9kdqkbqk9rsc5fvlnc6nxzb9jzy06")))

(define-public crate-wslpath2-0.1.1 (c (n "wslpath2") (v "0.1.1") (h "0skg57zifhvslayd87p5980j969qr0dn2xv436k26xg2izvzk9bl")))

