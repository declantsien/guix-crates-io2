(define-module (crates-io ws lp wslpath) #:use-module (crates-io))

(define-public crate-wslpath-0.0.1 (c (n "wslpath") (v "0.0.1") (h "0x7l3r0k36fmkmxq3cckfj23h54irgmc0l1d9x3m3saklhayzmk8")))

(define-public crate-wslpath-0.0.2 (c (n "wslpath") (v "0.0.2") (h "18y7bq24l8090z6a4ac0vdf667xv02ycy6ypjdm3mly45kgyr8h4")))

