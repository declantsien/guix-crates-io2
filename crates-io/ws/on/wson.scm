(define-module (crates-io ws on wson) #:use-module (crates-io))

(define-public crate-wson-0.1.0 (c (n "wson") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1529d33cbgfxkxck7pgjanahf1dni0my9vrmjk2395ypmqinn6q1")))

(define-public crate-wson-0.1.1 (c (n "wson") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0lx6s99a0g671n712hmmj1qb9lmd2p0d5jv57g9dv8hzaz25aipr")))

