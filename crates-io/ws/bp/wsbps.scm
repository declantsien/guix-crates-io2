(define-module (crates-io ws bp wsbps) #:use-module (crates-io))

(define-public crate-wsbps-0.1.0 (c (n "wsbps") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0s5i29r259gq959yp3vlq42xp6s3g3db9jyxxyw9dvimxcm8ka6a")))

(define-public crate-wsbps-0.1.1 (c (n "wsbps") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1f870dzl8qmpdhj45dv3z3nc2sh6990958pm9zv2h8b90l2k3v6w")))

(define-public crate-wsbps-0.1.2 (c (n "wsbps") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1nr47sk336yn5f97zini1q0nxd6p1nzz1lrvj4sjwcsfdd3w4hvx")))

(define-public crate-wsbps-0.1.3 (c (n "wsbps") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1vhcy2gxx72045rxpkl0r82hqhnj76szcscs5lx2agrywphp35ww")))

(define-public crate-wsbps-0.1.4 (c (n "wsbps") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1243g9myp2qql6gpqfgjwf94vwdy0zmbg4b7c5brq8dq5b04mm64")))

(define-public crate-wsbps-0.1.5 (c (n "wsbps") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1y98pjfkx9318v0divynpfkyg6x627q8b4grs4x76gqbg1v5rxx4")))

(define-public crate-wsbps-0.1.6 (c (n "wsbps") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0hy56wscv9k8mjmidh6za0r865wjl19yj1358617n884dmgh3m3h")))

(define-public crate-wsbps-0.1.7 (c (n "wsbps") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0iwiyrp2mpsrcglyp28jx4171a2c989yrcf0pxl05rz0d6rll7rx")))

(define-public crate-wsbps-0.2.0 (c (n "wsbps") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1jj0crdvx8322ay8k79mfk7lf5fbmf6zs1a6vjbj35fd9prvp09k")))

