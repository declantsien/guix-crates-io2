(define-module (crates-io ws _s ws_stream) #:use-module (crates-io))

(define-public crate-ws_stream-0.0.0 (c (n "ws_stream") (v "0.0.0") (d (list (d (n "async_runtime") (r "^0.1.7") (d #t) (k 2) (p "naja_async_runtime")) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.11") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (f (quote ("io-compat" "compat"))) (d #t) (k 0)) (d (n "futures_01") (r "^0.1") (d #t) (k 0) (p "futures")) (d (n "futures_codec") (r "^0.2.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (f (quote ("codec" "tcp"))) (k 0)) (d (n "tokio-tungstenite") (r "^0.8") (d #t) (k 0)) (d (n "tungstenite") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0dm6zsrylrmw1mx3cb023m86rc3gcj7pxiv1i5sslv99phv15yyw")))

(define-public crate-ws_stream-0.0.1 (c (n "ws_stream") (v "0.0.1") (h "1z259m497gn2r5v59b30k0fhz9xri4p473dgmdwc3l8v2dhgzriq") (f (quote (("external_doc"))))))

