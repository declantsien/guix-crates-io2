(define-module (crates-io ws _s ws_stream_endpoint) #:use-module (crates-io))

(define-public crate-ws_stream_endpoint-0.1.0 (c (n "ws_stream_endpoint") (v "0.1.0") (d (list (d (n "futures_01") (r "^0.1") (d #t) (k 0) (p "futures")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ringbuf") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (f (quote ("codec"))) (k 0)))) (h "1abbcaaax2h86rz4yd3wmgn31cqv7yjna6qqdm1wjy1a9bm6q9f9")))

