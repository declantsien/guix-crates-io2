(define-module (crates-io ws el wselector) #:use-module (crates-io))

(define-public crate-wselector-0.1.0 (c (n "wselector") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2") (d #t) (k 0)))) (h "17j71rzb33y4inah87c79wrrlr9f71jcwh44s5z8l36wwnnax337")))

