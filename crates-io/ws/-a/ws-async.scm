(define-module (crates-io ws -a ws-async) #:use-module (crates-io))

(define-public crate-ws-async-0.1.0 (c (n "ws-async") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.2.0") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hyper") (r "^0.13.0") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("blocking" "io-util" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tower-service") (r "^0.3.0") (d #t) (k 0)))) (h "0p7r0iign3lhphzm42r81gszv3hf38sz5f27dr990np96h6r5wm6")))

