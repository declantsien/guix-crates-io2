(define-module (crates-io ws -a ws-actix-web) #:use-module (crates-io))

(define-public crate-ws-actix-web-0.1.0 (c (n "ws-actix-web") (v "0.1.0") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web-actors") (r "^2.0.0") (d #t) (k 0)))) (h "1qk6v72sn81nrzjwsr7i4jmby89rlrxfbaanrf1ngk3kqzx1gcfl")))

(define-public crate-ws-actix-web-0.1.1 (c (n "ws-actix-web") (v "0.1.1") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web-actors") (r "^2.0.0") (d #t) (k 0)))) (h "0z8msf9ksqx6ahnw3y6mr0yzk8255al34ipi0mzd2k3yh670n3qf")))

(define-public crate-ws-actix-web-0.1.2 (c (n "ws-actix-web") (v "0.1.2") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web-actors") (r "^2.0.0") (d #t) (k 0)) (d (n "awc") (r "^1.0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1v69r8fr040287gzq0wf0gjl9avllvp0ff7dihw5hf95gyclj5vh")))

(define-public crate-ws-actix-web-0.1.3 (c (n "ws-actix-web") (v "0.1.3") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web-actors") (r "^2.0.0") (d #t) (k 0)) (d (n "awc") (r "^1.0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "14xd975m3z8bja2n6b26has1g2hzkri6b3j52r1plc2smfimm2vp")))

(define-public crate-ws-actix-web-0.1.4 (c (n "ws-actix-web") (v "0.1.4") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web-actors") (r "^2.0.0") (d #t) (k 0)) (d (n "awc") (r "^1.0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "03c9d3fxx0qvy163jw19x5h3a8znqmbs2kz997qsnh390n1dhm3b")))

(define-public crate-ws-actix-web-0.1.5 (c (n "ws-actix-web") (v "0.1.5") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web-actors") (r "^2.0.0") (d #t) (k 0)) (d (n "awc") (r "^1.0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1ryfww8a197q3jb5j3jb12zhha64cw1bcrwgg9js0sha8nbkddqg")))

(define-public crate-ws-actix-web-0.1.6 (c (n "ws-actix-web") (v "0.1.6") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web-actors") (r "^2.0.0") (d #t) (k 0)) (d (n "awc") (r "^1.0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1bwaqsrryrswagjx5awklmkdj5yry0h19m776mfb67v7gljvc07g")))

(define-public crate-ws-actix-web-0.1.7 (c (n "ws-actix-web") (v "0.1.7") (d (list (d (n "actix") (r "^0.9.0") (d #t) (k 0)) (d (n "actix-codec") (r "^0.2.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0.0") (d #t) (k 0)) (d (n "actix-web-actors") (r "^2.0.0") (d #t) (k 0)) (d (n "awc") (r "^1.0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1in574a8sldhvnzhb9rxd995q49qpx1adghgcwvxmr5lx40glh6p")))

(define-public crate-ws-actix-web-0.2.0 (c (n "ws-actix-web") (v "0.2.0") (d (list (d (n "actix") (r "^0.9") (d #t) (k 0)) (d (n "actix-codec") (r "^0.2") (d #t) (k 0)) (d (n "actix-web") (r "^2") (d #t) (k 0)) (d (n "actix-web-actors") (r "^2") (d #t) (k 0)) (d (n "awc") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "14m4gpbv8qravrbhbczd069mqchgygnzc1dryrnnwk0c0yk3a3b5")))

