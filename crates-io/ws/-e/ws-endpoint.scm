(define-module (crates-io ws -e ws-endpoint) #:use-module (crates-io))

(define-public crate-ws-endpoint-0.1.0 (c (n "ws-endpoint") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "stream" "macros" "time"))) (d #t) (k 2)) (d (n "tokio-tungstenite") (r "^0.10") (d #t) (k 0)) (d (n "tungstenite") (r "^0.10") (d #t) (k 0)))) (h "10mvphqxnvlwah265as6yz8c40yd046ss2dkgajflklwakwd7pnd")))

