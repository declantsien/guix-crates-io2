(define-module (crates-io ws l_ wsl_open_browser) #:use-module (crates-io))

(define-public crate-wsl_open_browser-2021.822.842 (c (n "wsl_open_browser") (v "2021.822.842") (d (list (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "131hjnd7ynpg1xlmxg4na565d01whl33yxwr6zkxd3b85algh090") (y #t)))

(define-public crate-wsl_open_browser-2021.823.702 (c (n "wsl_open_browser") (v "2021.823.702") (d (list (d (n "unwrap") (r "^1.2.1") (d #t) (k 0)))) (h "0xnycw3rv8if4n0gkwjzqqkq1nh7vwzd8dhh8gf15i2bvh12znn3")))

