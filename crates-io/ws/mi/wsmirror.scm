(define-module (crates-io ws mi wsmirror) #:use-module (crates-io))

(define-public crate-wsmirror-0.1.0 (c (n "wsmirror") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net" "time" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.15.0") (d #t) (k 0)))) (h "17sls06b8wkk2cgpsq568zx2m0h5dipgbdphhq5s2fiqizs32y8x")))

