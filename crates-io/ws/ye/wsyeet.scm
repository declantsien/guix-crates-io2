(define-module (crates-io ws ye wsyeet) #:use-module (crates-io))

(define-public crate-wsyeet-0.2.0 (c (n "wsyeet") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.18.0") (d #t) (k 0)))) (h "1wyzkb79nghz237ygsx7jkzk5z0l7i1hkhdpyhnsna9k6xmi7ilf")))

(define-public crate-wsyeet-0.2.1 (c (n "wsyeet") (v "0.2.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.18.0") (d #t) (k 0)))) (h "0ajdv4y2wnmykkam67awhiim46c60m9xyc49cn742msjdabncg1r")))

