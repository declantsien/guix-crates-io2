(define-module (crates-io ws l- wsl-get) #:use-module (crates-io))

(define-public crate-wsl-get-0.1.0 (c (n "wsl-get") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "argopt") (r "^0.1.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "windows") (r "^0.11.0") (d #t) (k 0)) (d (n "windows") (r "^0.11.0") (d #t) (k 1)))) (h "1lh0891hlkwvblfzs6zxx5rsynvi85pnc94yk1a3dzpxjm2ckjci")))

