(define-module (crates-io ws l- wsl-version) #:use-module (crates-io))

(define-public crate-wsl-version-0.1.0 (c (n "wsl-version") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0bmrn1rbwy3qd4smhr175n4p0vn1l39b6kgjyg1wiai38m464ac7") (y #t)))

(define-public crate-wsl-version-0.1.1 (c (n "wsl-version") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "1mxvb3lvjv5a1im01k9qxkvf3spwr6bdk7km8yf9lj5f2ilxcaqf") (y #t)))

(define-public crate-wsl-version-0.1.2 (c (n "wsl-version") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0qkm0s2y6a9z1wj92c9w1n75ns5wis1m0wh8m2d331wqyh44ngii") (y #t)))

(define-public crate-wsl-version-0.1.3 (c (n "wsl-version") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "019si69h3xvqhl9cj57c0xkjzc7gm6sr06nrk5kpd7ld1ci9xyhi") (y #t)))

(define-public crate-wsl-version-0.1.4 (c (n "wsl-version") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0c5ammwzh3diisss2sss6fpv1yv11ads1vrrlmvigknv1kpjd2lz") (y #t)))

(define-public crate-wsl-version-1.0.0 (c (n "wsl-version") (v "1.0.0") (h "17yb384zhnwgkwc1xs2309zqv4h2r9lvmwljdpck2ir3y7vk4a6h") (y #t) (r "1.70.0")))

