(define-module (crates-io ws df wsdf) #:use-module (crates-io))

(define-public crate-wsdf-0.1.0 (c (n "wsdf") (v "0.1.0") (d (list (d (n "epan-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wsdf-derive") (r "^0.1.0") (d #t) (k 0)))) (h "02dckaq767ybyf4laa4sqz5zjiygakppk5vgsl606ysfn61yg294")))

(define-public crate-wsdf-0.1.1 (c (n "wsdf") (v "0.1.1") (d (list (d (n "epan-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wsdf-derive") (r "^0.1.0") (d #t) (k 0)))) (h "09zxydk1knm1nq1ly4y2jqls9s1p2h0mg23219l69040xjzpw9j5")))

(define-public crate-wsdf-0.1.2 (c (n "wsdf") (v "0.1.2") (d (list (d (n "epan-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wsdf-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0mlwiqpb4lhfrk10x4bww78m1jq6q5mw8pz9flcsxfhzkv22kalq")))

