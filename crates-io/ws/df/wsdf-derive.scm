(define-module (crates-io ws df wsdf-derive) #:use-module (crates-io))

(define-public crate-wsdf-derive-0.1.0 (c (n "wsdf-derive") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0z8lnf1xaic33lxjzdg2j954bfglgl7l4pa3qhs0ghm8l7y3h2q9")))

