(define-module (crates-io ws pr wsprspots) #:use-module (crates-io))

(define-public crate-wsprspots-0.0.0 (c (n "wsprspots") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "modtype") (r "^0.7") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "091j3svm3pahkyx0avizk06nm6gqimg4z54c8zrrd4h61bqa02vy")))

(define-public crate-wsprspots-0.0.1 (c (n "wsprspots") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "modtype") (r "^0.7") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "1kaj2c9z5vbskhvdcwspdgzz72r67l7mzgj8j215sn8r6zjh192f")))

(define-public crate-wsprspots-0.0.2 (c (n "wsprspots") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "modtype") (r "^0.7") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0f9xb5fzigxacbwd7drbh602vpwwk1j6m5dv23i6k6yb43jf7wcs")))

(define-public crate-wsprspots-0.0.3 (c (n "wsprspots") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "modtype") (r "^0.7") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "10ndca4sd2scnn850av59kqahr2j19z1pllk2fpizmrpc6dnpwbd")))

(define-public crate-wsprspots-0.0.4 (c (n "wsprspots") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "modtype") (r "^0.7") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0m1b9lk2fm0qiqb4psdydi1jlh0pw1nw87zg1ydbscfxrvcldw7v")))

(define-public crate-wsprspots-0.0.5 (c (n "wsprspots") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "modtype") (r "^0.7") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0h013bfskgqlb879i94kvli3bvdabnfcrav6jipi74ysksl1ylls")))

(define-public crate-wsprspots-0.1.0 (c (n "wsprspots") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "smartstring") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "12s9vwjl59r0hp2isjy1nlz3j1c4d01604g4jqv3l2fwwdq5i1w4")))

(define-public crate-wsprspots-0.1.1 (c (n "wsprspots") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "smartstring") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "1jkckd36akx2aqic2197b0jaj2p7lqldh0kyzwfhyf4n5dvkxq0c")))

(define-public crate-wsprspots-0.1.2 (c (n "wsprspots") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "smartstring") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0hx3zids8fhflikh6z8q6hqws3gvcahqrm292jz27hzv17y11lbg")))

(define-public crate-wsprspots-0.1.3 (c (n "wsprspots") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)) (d (n "smartstring") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "1p64pgx194c4gkn2n1ancgb6d4b30cwrxnbq2amnhqlzh8d8y95r")))

(define-public crate-wsprspots-0.1.4 (c (n "wsprspots") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "smartstring") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "1c8xb3v7q09al0jvj9izjpjjl8d3nkh09llxzl35j8klfx0956is")))

(define-public crate-wsprspots-0.1.5 (c (n "wsprspots") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "smartstring") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0afzk3id8wbcm2m9qfzxaqzwks1vlgaj358ny7qc937kps7widys")))

(define-public crate-wsprspots-0.1.6 (c (n "wsprspots") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "smartstring") (r "^1.0") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "0bz9xy5p3304x861dh72zk1zgiv48z00ry9s2dpsnxf1wdi5qqxz")))

(define-public crate-wsprspots-0.1.7 (c (n "wsprspots") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "smartstring") (r "^1.0") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "12j50g0n1sfv96m2ajr53bmdcwrdx5mwhkd3jkqinqs49kb21w36")))

(define-public crate-wsprspots-0.1.8 (c (n "wsprspots") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "smartstring") (r "^1.0") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "1z2hl6w9kvc5kwjs0jf416577kljdv9gv21f6daaf0mdp78yq571")))

