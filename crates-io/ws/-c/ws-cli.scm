(define-module (crates-io ws -c ws-cli) #:use-module (crates-io))

(define-public crate-ws-cli-0.1.0 (c (n "ws-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo" "derive" "string"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "1vq876qhf8v00gqklxjh7igg8qnz2xa1bklaj0lyiqapczkdjaqz")))

(define-public crate-ws-cli-0.1.1 (c (n "ws-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo" "derive" "string"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.21.0") (d #t) (k 0)))) (h "05r90bf99dxiaa7hr9f6i1y1cqp41mfp5xrjy0zivifnjrs1skba")))

