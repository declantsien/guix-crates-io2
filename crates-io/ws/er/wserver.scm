(define-module (crates-io ws er wserver) #:use-module (crates-io))

(define-public crate-wserver-0.1.0 (c (n "wserver") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "gotham") (r "^0.5.0") (d #t) (k 0)))) (h "1hk40f0dii9scpk40hvq5j3g1z3gwp1yzhfzf2gv7087hxcs3qdz")))

(define-public crate-wserver-0.1.1 (c (n "wserver") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "gotham") (r "^0.5.0") (d #t) (k 0)))) (h "1xzqpdi4zfg93nmxlaw90bg0jnl1xshizpdid5yfrna3kvkcbd8a")))

(define-public crate-wserver-0.1.2 (c (n "wserver") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "gotham") (r "^0.5.0") (d #t) (k 0)))) (h "147is9pqsp6jf458apr2qih5hsalw2cf7mplqrf1zjxjd78nldj1")))

(define-public crate-wserver-0.1.3 (c (n "wserver") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "gotham") (r "^0.5.0") (d #t) (k 0)))) (h "1bjrgk25jzk14p6pl20paw70azkdpdm0y0kyns3rry8i62r5h3mg")))

(define-public crate-wserver-0.1.4 (c (n "wserver") (v "0.1.4") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "gotham") (r "^0.6.0") (d #t) (k 0)))) (h "01rj5bx46f4v4nyzbi8x5irid3f9zngwpxajdl98f85n0g558mc6")))

(define-public crate-wserver-0.1.5 (c (n "wserver") (v "0.1.5") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "gotham") (r "^0.7.1") (d #t) (k 0)))) (h "17nv7lkwy0wvlzc39hwc1rmz8zdvx8z673sp3xsm29kbxs2w6zpc")))

