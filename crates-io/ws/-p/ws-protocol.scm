(define-module (crates-io ws -p ws-protocol) #:use-module (crates-io))

(define-public crate-ws-protocol-0.1.0 (c (n "ws-protocol") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)))) (h "0nsnayb0cm9yhs0l2g9pj3s4pfl4agbisrnk3i3m53yjyxj5m85n")))

(define-public crate-ws-protocol-0.2.0 (c (n "ws-protocol") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)))) (h "0v7qrnam6r05xjml4b173x01pnnj8xja13lwvg9rg9xm8i043i37")))

(define-public crate-ws-protocol-0.3.0 (c (n "ws-protocol") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)))) (h "1vqik11rb2fwygzqyy7k0j5mskwjci5jfsw012g97mn3zlpf8g1h")))

