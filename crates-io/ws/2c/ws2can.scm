(define-module (crates-io ws #{2c}# ws2can) #:use-module (crates-io))

(define-public crate-ws2can-0.1.0 (c (n "ws2can") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.13") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.13") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-socketcan") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.14.0") (d #t) (k 0)))) (h "0p8rr4fr69l31ajpkhsndyn9qdadxal1w98x8d7b1q1czg0my6g9")))

