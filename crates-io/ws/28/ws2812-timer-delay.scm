(define-module (crates-io ws #{28}# ws2812-timer-delay) #:use-module (crates-io))

(define-public crate-ws2812-timer-delay-0.1.0 (c (n "ws2812-timer-delay") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.1") (d #t) (k 0)))) (h "07w523aic3a31jrvl2lr27l80gyd8wwzw3yj08yqw51jg5c4vxw2") (f (quote (("slow"))))))

(define-public crate-ws2812-timer-delay-0.2.0 (c (n "ws2812-timer-delay") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "06msx7inkcx2ym08ij8qm2ip2ny17m4cl36sbdr99i03dpsdf295") (f (quote (("slow"))))))

(define-public crate-ws2812-timer-delay-0.3.0 (c (n "ws2812-timer-delay") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "0048wb5jgadbpvlscf5ava021ls4fc6arif48hc16i3dbk13kcxr") (f (quote (("slow"))))))

