(define-module (crates-io ws #{28}# ws2811-spi) #:use-module (crates-io))

(define-public crate-ws2811-spi-0.1.0 (c (n "ws2811-spi") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "0284r6fs0y1mr5sf0k8mmllgvzadl6g4fvafw9f07n28m3n27pkf") (f (quote (("mosi_idle_high"))))))

(define-public crate-ws2811-spi-0.1.1 (c (n "ws2811-spi") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "1vmz0ljnnjmynmppsw7l4rgndj76wv5svvbaj8fvxmjhjk0bwmv9") (f (quote (("mosi_idle_high"))))))

(define-public crate-ws2811-spi-0.1.2 (c (n "ws2811-spi") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "0i2y8zdfj1w2q5kjg2jahij1f1piyn6zgz45mw2bsj5py804ary8") (f (quote (("mosi_idle_high"))))))

(define-public crate-ws2811-spi-0.1.3 (c (n "ws2811-spi") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "1kk74zb927jssk8r39rkxf7xki7gpcvr6yallbh8ibczcy1gg9sg") (f (quote (("mosi_idle_high") ("fifo_stm32f1"))))))

(define-public crate-ws2811-spi-0.1.4 (c (n "ws2811-spi") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "1r2say1n1jswqkgi7a0lnrzrqbdda3fz3cl4y4zlahk1wvxg52vm") (f (quote (("mosi_idle_high") ("fifo_stm32f1"))))))

(define-public crate-ws2811-spi-0.1.5 (c (n "ws2811-spi") (v "0.1.5") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "084hn9jgkzqxgvlh9ym7v6ib41xs3670j2myj7yid80i3q2lr0gd") (f (quote (("mosi_idle_high") ("fifo_stm32f1"))))))

