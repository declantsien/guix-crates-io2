(define-module (crates-io ws #{28}# ws2812-nop-imxrt1062) #:use-module (crates-io))

(define-public crate-ws2812-nop-imxrt1062-0.1.0 (c (n "ws2812-nop-imxrt1062") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.1") (d #t) (k 0)))) (h "19hfx06zy7586p4hqarz1ypgfnr369mljh7cnznsb9n321kq5f8v")))

(define-public crate-ws2812-nop-imxrt1062-0.1.1 (c (n "ws2812-nop-imxrt1062") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.1") (d #t) (k 0)))) (h "13f2ajcc94i384iabwg3skdkg1a9x6yyvk7xz7rcw1vldhic324b")))

(define-public crate-ws2812-nop-imxrt1062-0.2.0 (c (n "ws2812-nop-imxrt1062") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.1") (d #t) (k 0)))) (h "0y6rvgj38wplljwnd07il3vnkp8lizrqmvx3yy8i176v3wq9l8zk")))

