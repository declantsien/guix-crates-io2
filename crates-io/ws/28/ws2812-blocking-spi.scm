(define-module (crates-io ws #{28}# ws2812-blocking-spi) #:use-module (crates-io))

(define-public crate-ws2812-blocking-spi-0.1.0 (c (n "ws2812-blocking-spi") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "ws2812-spi-write-constants") (r "^0.1.0") (d #t) (k 0)))) (h "0f96mlqgc2x5a529nnbz0kp3z0crpr4s3chdpqw2f5qjzc7wsjb3")))

(define-public crate-ws2812-blocking-spi-0.2.0 (c (n "ws2812-blocking-spi") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "ws2812-spi-write-constants") (r "^0.2.0") (d #t) (k 0)))) (h "0r1cg5bj1ppy41xw9vasbqsnmn7plmzjlrj5ywdry23x2sg6c0i3")))

