(define-module (crates-io ws #{28}# ws2812-uart) #:use-module (crates-io))

(define-public crate-ws2812-uart-0.1.0 (c (n "ws2812-uart") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.1") (d #t) (k 0)))) (h "1nk1mh5yi1imgcvsyw5qddh2zslqfsx4n5a3xaklxnv0nwp90kbi")))

