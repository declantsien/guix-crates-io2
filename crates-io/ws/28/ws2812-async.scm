(define-module (crates-io ws #{28}# ws2812-async) #:use-module (crates-io))

(define-public crate-ws2812-async-0.1.0 (c (n "ws2812-async") (v "0.1.0") (d (list (d (n "embedded-hal-async") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "smart-leds") (r "^0.3.0") (d #t) (k 0)))) (h "0x371j3p7gkz3gkkv87x9djpkq93r1fpzndr7jkp948j6ic2f7rr")))

(define-public crate-ws2812-async-0.2.0 (c (n "ws2812-async") (v "0.2.0") (d (list (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "smart-leds") (r "^0.4.0") (d #t) (k 0)))) (h "02cd3h2b2656v7ks00l71bxv9hxv4hh5xf21j26x1prdciggjnw1")))

