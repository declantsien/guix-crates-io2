(define-module (crates-io ws #{28}# ws281x) #:use-module (crates-io))

(define-public crate-ws281x-0.1.0 (c (n "ws281x") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n7lr9pgaswmygx51g4vajjn48xqgm3lxz2xfzxl49w8kjanxx5m")))

