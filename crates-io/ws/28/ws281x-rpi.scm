(define-module (crates-io ws #{28}# ws281x-rpi) #:use-module (crates-io))

(define-public crate-ws281x-rpi-0.0.1 (c (n "ws281x-rpi") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.59") (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "0fns9969za1wwnnplwbsg31xkxbpfjfhgkmfc9bbk1q7nihzws6p")))

