(define-module (crates-io ws #{28}# ws2812-nop-samd21) #:use-module (crates-io))

(define-public crate-ws2812-nop-samd21-0.1.0 (c (n "ws2812-nop-samd21") (v "0.1.0") (d (list (d (n "circuit_playground_express") (r "~0.2") (d #t) (k 2)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (d #t) (k 2)) (d (n "embedded-hal") (r "~0.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-halt") (r "~0.2") (d #t) (k 2)) (d (n "smart-leds") (r "~0.1") (d #t) (k 2)) (d (n "smart-leds-trait") (r "~0.1") (d #t) (k 0)))) (h "1hjfp0v3px5f456r3jjgr36zfccm1m81pki1cyz3dvdq1gsh38c6") (y #t)))

(define-public crate-ws2812-nop-samd21-0.3.0 (c (n "ws2812-nop-samd21") (v "0.3.0") (d (list (d (n "circuit_playground_express") (r "~0.6") (d #t) (k 2)) (d (n "cortex-m") (r "~0.6.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.11") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-halt") (r "~0.2") (d #t) (k 2)) (d (n "smart-leds") (r "~0.2") (d #t) (k 2)) (d (n "smart-leds-trait") (r "~0.2") (d #t) (k 0)))) (h "0i6dlibv8izv6p9p9zy4mxd5xpik87agkwn9z8kdx8slm90b5x84") (y #t)))

