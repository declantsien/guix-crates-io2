(define-module (crates-io ws #{28}# ws2812-spi) #:use-module (crates-io))

(define-public crate-ws2812-spi-0.1.0 (c (n "ws2812-spi") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.1") (d #t) (k 0)))) (h "070w2v6fdqflykyysmbc8plia0ak17ayfixv7lxh710w166kb5r2") (f (quote (("mosi_idle_high"))))))

(define-public crate-ws2812-spi-0.2.0 (c (n "ws2812-spi") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "1xvg02xwrsgd21l36zhmdkvcsva6dq806cmyc9h1k9y9sa0awiyf") (f (quote (("mosi_idle_high"))))))

(define-public crate-ws2812-spi-0.3.0 (c (n "ws2812-spi") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "02n7sh0pkini0dx6g63nq71azjnsqlyyj4bl90bw3a8fdh6vmhn6") (f (quote (("mosi_idle_high"))))))

(define-public crate-ws2812-spi-0.4.0 (c (n "ws2812-spi") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2.0") (d #t) (k 0)))) (h "1nxqhi9xz64161vmp773j136pn4db4jljfn1ghg8cb1cvqf0p87a") (f (quote (("mosi_idle_high"))))))

