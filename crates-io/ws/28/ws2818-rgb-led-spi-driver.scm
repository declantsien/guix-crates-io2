(define-module (crates-io ws #{28}# ws2818-rgb-led-spi-driver) #:use-module (crates-io))

(define-public crate-ws2818-rgb-led-spi-driver-0.1.0 (c (n "ws2818-rgb-led-spi-driver") (v "0.1.0") (d (list (d (n "spidev") (r "^0.4.0") (d #t) (k 0)))) (h "1c7i0qixg33pb80qlrzgxwd9pszxil7g7z89rxf2x9vhd4p7xclb")))

(define-public crate-ws2818-rgb-led-spi-driver-0.1.1 (c (n "ws2818-rgb-led-spi-driver") (v "0.1.1") (d (list (d (n "spidev") (r "^0.4.0") (d #t) (k 0)))) (h "0f6mfjykcmc6687hvcm84pjly7i97hzi60hw72hb0fxahcqb91zn")))

(define-public crate-ws2818-rgb-led-spi-driver-0.1.2 (c (n "ws2818-rgb-led-spi-driver") (v "0.1.2") (d (list (d (n "spidev") (r "^0.4.0") (d #t) (k 0)))) (h "1p2ydda205z3djwgpvjlvsljnm76smfahykrz8hb4ar3s1vwig53")))

(define-public crate-ws2818-rgb-led-spi-driver-0.1.3 (c (n "ws2818-rgb-led-spi-driver") (v "0.1.3") (d (list (d (n "spidev") (r "^0.4.0") (d #t) (k 0)))) (h "1ammlism8sbv0cf9l2j3l7zxkdd6ky3yjx8hrs43r8m4kkb2c5qc")))

(define-public crate-ws2818-rgb-led-spi-driver-0.1.4 (c (n "ws2818-rgb-led-spi-driver") (v "0.1.4") (d (list (d (n "spidev") (r "^0.4.0") (d #t) (k 0)))) (h "0zwjvwz4vyiz2q3zv7kq2hj9g8g74j0gl0853jmmh7a2k9g1bam9")))

(define-public crate-ws2818-rgb-led-spi-driver-0.1.5 (c (n "ws2818-rgb-led-spi-driver") (v "0.1.5") (d (list (d (n "spidev") (r "^0.4.0") (d #t) (k 0)))) (h "1rr7z12wvyax1b7jlgviad27mw2rnjr6a75wr8qz243fwv0y5izh")))

(define-public crate-ws2818-rgb-led-spi-driver-1.0.0 (c (n "ws2818-rgb-led-spi-driver") (v "1.0.0") (d (list (d (n "spidev") (r "^0.4.0") (d #t) (k 0)))) (h "0gbwz48jyhrra1n1hqpqf65jdyjv7k4zrarv4ks0qyz98qfjjdcd")))

(define-public crate-ws2818-rgb-led-spi-driver-1.0.1 (c (n "ws2818-rgb-led-spi-driver") (v "1.0.1") (d (list (d (n "spidev") (r "^0.4.1") (d #t) (k 0)))) (h "1yf9fx5j0s8ynl9m2vjcccczsxw3wvxgllrxzldcyygpf9mq2c3x")))

(define-public crate-ws2818-rgb-led-spi-driver-2.0.0 (c (n "ws2818-rgb-led-spi-driver") (v "2.0.0") (d (list (d (n "spidev") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "1ypf7h9pd831zqrxlpd9gi5prwf6wjzcdcry4zw57cp4mq3s419z") (f (quote (("default" "adapter_spidev") ("adapter_spidev" "spidev"))))))

