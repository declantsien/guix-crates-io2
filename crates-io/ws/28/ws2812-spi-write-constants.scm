(define-module (crates-io ws #{28}# ws2812-spi-write-constants) #:use-module (crates-io))

(define-public crate-ws2812-spi-write-constants-0.1.0 (c (n "ws2812-spi-write-constants") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1v07ym1s9bh9m9fmbwii1y194mww6nfwx568v7yys741whrsphx7")))

(define-public crate-ws2812-spi-write-constants-0.2.0 (c (n "ws2812-spi-write-constants") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ycfd9nm7byjdmj3lfbfbd0ap80zk8ik6jdc4ikibq95z0lmljlz")))

