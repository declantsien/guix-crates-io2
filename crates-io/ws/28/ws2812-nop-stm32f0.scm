(define-module (crates-io ws #{28}# ws2812-nop-stm32f0) #:use-module (crates-io))

(define-public crate-ws2812-nop-stm32f0-0.1.0 (c (n "ws2812-nop-stm32f0") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "smart-leds-trait") (r "^0.2") (d #t) (k 0)))) (h "0sq7qdnb8zrb0bq2x06imaayyj23z39d7p7i1pwnfr85hsdgrhaa") (y #t)))

