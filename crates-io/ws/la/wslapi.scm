(define-module (crates-io ws la wslapi) #:use-module (crates-io))

(define-public crate-wslapi-0.1.0 (c (n "wslapi") (v "0.1.0") (d (list (d (n "minidl") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "ntdef" "winerror" "combaseapi" "handleapi" "processthreadsapi" "synchapi" "winbase" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1gira3fvjsv8x15l1r82qj0s6y8z5z2y4bqg1dz224jnkf7xa2k3")))

(define-public crate-wslapi-0.1.1 (c (n "wslapi") (v "0.1.1") (d (list (d (n "minidl") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "ntdef" "winerror" "combaseapi" "handleapi" "processthreadsapi" "synchapi" "winbase" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09h6nfg2yfmgry0y7i05dp19zc0fhhc3a836hwdd76mm5a74nkaj")))

(define-public crate-wslapi-0.1.2 (c (n "wslapi") (v "0.1.2") (d (list (d (n "minidl") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "wchar") (r "^0.6.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "ntdef" "winerror" "combaseapi" "handleapi" "processthreadsapi" "synchapi" "winbase" "winnt" "winreg"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0213ffxrgnd5vldxw5sw5x35l9yw47hymbhiyr7x1n6y2kfw2yyf")))

(define-public crate-wslapi-0.1.3 (c (n "wslapi") (v "0.1.3") (d (list (d (n "minidl") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "wchar") (r "^0.6.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "ntdef" "winerror" "combaseapi" "handleapi" "processthreadsapi" "synchapi" "winbase" "winnt" "winreg"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06gnz40c8mm3w4wf59c2bn8n44xf1rd7c0dvi31kfsk36w8wbjdg")))

