(define-module (crates-io ws dc wsdclient) #:use-module (crates-io))

(define-public crate-wsdclient-0.0.0 (c (n "wsdclient") (v "0.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1mh78q9j9nq0634fa9v9df8rb3ancfgw8xqxbrr439xwccc23ggm")))

