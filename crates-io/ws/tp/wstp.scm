(define-module (crates-io ws tp wstp) #:use-module (crates-io))

(define-public crate-wstp-0.1.0 (c (n "wstp") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "wolfram-expr") (r "^0.1.0") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1mk7lg1myg1kql39na0n5nmfx912vawnvvajxy67l9j4qgjaf7g4")))

(define-public crate-wstp-0.1.3 (c (n "wstp") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "wolfram-expr") (r "^0.1.0") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0b42kwnpq8n237g4n2b6z8mkf44c8v4n37d1ipnj6sgpnmrglda0")))

(define-public crate-wstp-0.1.4 (c (n "wstp") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.2.0") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.0") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.1.4") (d #t) (k 0)))) (h "03wp1c5p2wkj4i3cdnindq8qpfxy8hki1dydnikvdjyzqkjfccf3")))

(define-public crate-wstp-0.2.0 (c (n "wstp") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.2.1") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.0") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1pg3fk80zj8s6613fnv5kn0bd16dh3g7dn027pp4vkry6kcvw86d")))

(define-public crate-wstp-0.2.1 (c (n "wstp") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.2.1") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.0") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1hl4iaa2iwpx5g4zkvgz284nfqa4n970ayhpbl3mx4x3a42vg9gm")))

(define-public crate-wstp-0.2.2 (c (n "wstp") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.2.1") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.0") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.2.2") (d #t) (k 0)))) (h "1lrl00g39w9a330ambnd37617c6i4yfy1a4vkm0gqfp3q9c50b4l")))

(define-public crate-wstp-0.2.3 (c (n "wstp") (v "0.2.3") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.6") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.3.0") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.0") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.2.3") (d #t) (k 0)))) (h "1zrns2sfn78r1d786cidp13ynjpnwb0dz6zjm29zfz02k0yww3vx")))

(define-public crate-wstp-0.2.4 (c (n "wstp") (v "0.2.4") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.12") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.3.0") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.0") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.2.4") (d #t) (k 0)))) (h "0slg5gs10na7y043vyiy99lpbfji28gibgrhp1y2bn85kcbbbwlp")))

(define-public crate-wstp-0.2.5 (c (n "wstp") (v "0.2.5") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.13") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.3.0") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.0") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.2.5") (d #t) (k 0)))) (h "0bzf7rmkndl2mnisq1bm3xfv5fhg1fyhc30w8v8l9ry71f43ip4b")))

(define-public crate-wstp-0.2.6 (c (n "wstp") (v "0.2.6") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.13") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.4.1") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.0") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.2.6") (d #t) (k 0)))) (h "04x63zg4csg5cj8mf75hvrcr001zy44vq7b2d3l98y27a3xii5rp")))

(define-public crate-wstp-0.2.7 (c (n "wstp") (v "0.2.7") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.13") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.4.1") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.4") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.2.7") (d #t) (k 0)))) (h "1llmlpnfmvzdv3sbqc12dikpvm9b1rlbnlkpv2a91k9zkdjccnar")))

(define-public crate-wstp-0.2.8 (c (n "wstp") (v "0.2.8") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.13") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.4.1") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.4") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.2.8") (d #t) (k 0)))) (h "131wdbqh7bph9p6xmyhn7gbi2c83pkih72g5s6fnyb7mpc34dcmx") (r "1.64")))

(define-public crate-wstp-0.2.9 (c (n "wstp") (v "0.2.9") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.13") (d #t) (k 0)) (d (n "wolfram-app-discovery") (r "^0.4.1") (d #t) (k 2)) (d (n "wolfram-expr") (r "^0.1.4") (d #t) (k 0)) (d (n "wstp-sys") (r "^0.2.8") (d #t) (k 0)))) (h "0nypihiqlh0kcy0mllqvrmlrv6rbdbdp5nb57x1vmwjcvfqc6pmm") (r "1.70")))

