(define-module (crates-io ws -o ws-oled-driver) #:use-module (crates-io))

(define-public crate-ws-oled-driver-0.0.2 (c (n "ws-oled-driver") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rppal") (r "^0.14.1") (d #t) (k 0)))) (h "03f4wlkdygvry50slkrzdkkr50my0acmabwjbl4sr6z5rd6m0ywv")))

(define-public crate-ws-oled-driver-0.0.3 (c (n "ws-oled-driver") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rppal") (r "^0.14.1") (d #t) (k 0)))) (h "0ikdjfldd6888pjk8293kha0a1kdpymm556ha7dg6fcwga4vsw9p")))

(define-public crate-ws-oled-driver-0.0.4 (c (n "ws-oled-driver") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rppal") (r "^0.14.1") (d #t) (k 0)))) (h "0yn1k1x5jbpimk1fmivjhhzhchqdx994dk592sfxpj6qc8amqcqz")))

(define-public crate-ws-oled-driver-0.0.5 (c (n "ws-oled-driver") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "rppal") (r "^0.14.1") (d #t) (k 0)))) (h "0cq50pk54w9n4av0dbxdwmr6d9g40j9pcxpczn1lpa4hdlfav2cv")))

