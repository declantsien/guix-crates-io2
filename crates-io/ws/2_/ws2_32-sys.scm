(define-module (crates-io ws #{2_}# ws2_32-sys) #:use-module (crates-io))

(define-public crate-ws2_32-sys-0.0.1 (c (n "ws2_32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0lm8w4172axd79d5pl2mfg44k4k62a1rr8p390mksgl02m1rrm4y")))

(define-public crate-ws2_32-sys-0.1.0 (c (n "ws2_32-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "0rhhjdvdm321kddnk41frw009li6mf4lb75sn66y65wpgai5xpay")))

(define-public crate-ws2_32-sys-0.2.0 (c (n "ws2_32-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1n79driq2qvnaxqvfz8yr95jsn5vdpaf9p04c5p9nxjzbcw6qpzf")))

(define-public crate-ws2_32-sys-0.2.1 (c (n "ws2_32-sys") (v "0.2.1") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0ppscg5qfqaw0gzwv2a4nhn5bn01ff9iwn6ysqnzm4n8s3myz76m")))

