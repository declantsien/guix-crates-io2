(define-module (crates-io ws tr wstring_tools) #:use-module (crates-io))

(define-public crate-wstring_tools-0.1.0 (c (n "wstring_tools") (v "0.1.0") (d (list (d (n "former") (r "~0.1") (d #t) (k 0)) (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "18lij2lxijsz12mj7npiy4bqrk8v72cjz4xyfbgsh77yv67ds5nj")))

(define-public crate-wstring_tools-0.1.1 (c (n "wstring_tools") (v "0.1.1") (d (list (d (n "former") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "woptions") (r "~0.1") (d #t) (k 0)))) (h "11r9n3fcw5l41jp3hgr18gw1jsq36qghk7zz91rgfcc240iqznhk")))

(define-public crate-wstring_tools-0.1.2 (c (n "wstring_tools") (v "0.1.2") (d (list (d (n "former") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1dx07qjmb6j105bjf2gk37x2aajwn6bx4rvks7lnxsv7c1cvdbxr") (f (quote (("split") ("parse" "split") ("indentation") ("full" "indentation" "parse" "split") ("default" "indentation" "parse" "split"))))))

(define-public crate-wstring_tools-0.1.4 (c (n "wstring_tools") (v "0.1.4") (d (list (d (n "strs_tools") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1pr9a6m8d5km99wsisaj73xnw53hcgzgysmfd05isp5y2m6xsi6x") (f (quote (("use_std") ("use_alloc") ("split") ("parse" "split") ("indentation") ("full" "indentation" "parse" "split" "use_alloc") ("default" "indentation" "parse" "split" "use_std"))))))

(define-public crate-wstring_tools-0.1.5 (c (n "wstring_tools") (v "0.1.5") (d (list (d (n "strs_tools") (r "~0.1") (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "18cvc2jf627jpriajiqch2dpy5viyqh2884bh2ci8d05gp5m0m83") (f (quote (("use_std" "strs_tools/use_std") ("use_alloc" "strs_tools/use_alloc") ("split" "strs_tools/split") ("parse_number" "strs_tools/parse_number") ("parse" "split" "parse_number") ("indentation" "strs_tools/indentation") ("full" "use_std" "indentation" "parse" "split" "parse_number") ("default" "use_std" "indentation" "parse" "split" "parse_number"))))))

