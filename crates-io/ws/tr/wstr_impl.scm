(define-module (crates-io ws tr wstr_impl) #:use-module (crates-io))

(define-public crate-wstr_impl-0.1.0 (c (n "wstr_impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0mbhkdikz8bn9wwy90p3ldbhrm7gxrzz62r1bycd6dhaksgajh0f")))

(define-public crate-wstr_impl-0.2.0 (c (n "wstr_impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0dxk2dlxwkargjw1h0s6nimyh3mvm4dgyrzg78ngqcni171fval8")))

