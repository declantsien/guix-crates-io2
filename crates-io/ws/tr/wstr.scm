(define-module (crates-io ws tr wstr) #:use-module (crates-io))

(define-public crate-wstr-0.1.0 (c (n "wstr") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "widestring") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "wstr_impl") (r "^0.1") (d #t) (k 0)))) (h "02cwnqd6wa3mb91w2yzhck9j2zb7x6c9kn87fhzpxba6bshbg8ab")))

(define-public crate-wstr-0.2.0 (c (n "wstr") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "widestring") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "wstr_impl") (r "^0.2") (d #t) (k 0)))) (h "1yfbcwdwhckjv182wc90zkql34m7g20yw23vqxv0bc2vqc0l6spx")))

