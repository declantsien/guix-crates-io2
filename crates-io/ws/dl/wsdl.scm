(define-module (crates-io ws dl wsdl) #:use-module (crates-io))

(define-public crate-wsdl-0.1.0 (c (n "wsdl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "roxmltree") (r "^0.14.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0nkn75lg372f58w5pjj0wvndmwza9qzki1i6q8l4jdxpzc4f8i0h")))

(define-public crate-wsdl-0.1.1 (c (n "wsdl") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "clap") (r "^4.3") (d #t) (k 2)) (d (n "roxmltree") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "13xsma3iv5algr9aacmkaclcnrz1phwmfp8s2jmv3p9fl2x0v13b")))

(define-public crate-wsdl-0.1.2 (c (n "wsdl") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "clap") (r "^4.3") (d #t) (k 2)) (d (n "roxmltree") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "110zm1z93imq5y7wkgfjkvl72bb5iv0vy2i94g6i5kd7wvw5i8bg")))

(define-public crate-wsdl-0.1.3 (c (n "wsdl") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "clap") (r "^4.3") (d #t) (k 2)) (d (n "roxmltree") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0vb6cylwr6i5vx2k5x5hq6gmjhp6g7ypikvpmd03h32r3yljl3fl")))

