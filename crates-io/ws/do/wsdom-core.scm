(define-module (crates-io ws do wsdom-core) #:use-module (crates-io))

(define-public crate-wsdom-core-0.0.1 (c (n "wsdom-core") (v "0.0.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 2)) (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0g0pz7l188w2ad0a43xgdlqxvw2d8riks2ladaagvv40c4izzw83")))

