(define-module (crates-io ws do wsdom-ts-convert) #:use-module (crates-io))

(define-public crate-wsdom-ts-convert-0.0.1 (c (n "wsdom-ts-convert") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "winnow") (r "^0.5.17") (d #t) (k 0)))) (h "137g8mxfqfn4pwpibr848nqzp2pw3rn0g1nf0dsb13jmfmk4hsy7")))

