(define-module (crates-io ws do wsdom-dom) #:use-module (crates-io))

(define-public crate-wsdom-dom-0.0.1 (c (n "wsdom-dom") (v "0.0.1") (d (list (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "wsdom-core") (r "^0.0.1") (d #t) (k 0)) (d (n "wsdom-javascript") (r "^0.0.1") (d #t) (k 0)) (d (n "wsdom-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wsdom-macros-decl") (r "^0.0.1") (d #t) (k 0)))) (h "0grg415d7kq16kjdsv61y6232124ql26v0pc5gihdk4n871hqhck")))

