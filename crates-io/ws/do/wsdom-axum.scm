(define-module (crates-io ws do wsdom-axum) #:use-module (crates-io))

(define-public crate-wsdom-axum-0.0.1 (c (n "wsdom-axum") (v "0.0.1") (d (list (d (n "axum") (r "^0.7") (f (quote ("ws"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.13") (d #t) (k 0)) (d (n "wsdom-core") (r "^0.0.1") (d #t) (k 0)))) (h "1whbsqagy53q00klk5q5zvsrgyhhpq60z8nrc2p2csjpgqsj50yp")))

