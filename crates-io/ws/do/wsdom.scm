(define-module (crates-io ws do wsdom) #:use-module (crates-io))

(define-public crate-wsdom-0.0.1 (c (n "wsdom") (v "0.0.1") (d (list (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "wsdom-core") (r "^0.0.1") (d #t) (k 0)) (d (n "wsdom-dom") (r "^0.0.1") (d #t) (k 0)) (d (n "wsdom-javascript") (r "^0.0.1") (d #t) (k 0)) (d (n "wsdom-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wsdom-macros-decl") (r "^0.0.1") (d #t) (k 0)))) (h "1z328091igx5lw716c7mi9jsx4wpfmhn756kn81bikyin299nv3h")))

