(define-module (crates-io ws do wsdom-macros) #:use-module (crates-io))

(define-public crate-wsdom-macros-0.0.1 (c (n "wsdom-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "wsdom-ts-convert") (r "^0.0.1") (d #t) (k 0)))) (h "06zc3k4zy8grsf214zydx7s3xcki7v5zj4njjc2rdr96i90lwnhs")))

