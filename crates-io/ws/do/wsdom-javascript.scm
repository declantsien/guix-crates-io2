(define-module (crates-io ws do wsdom-javascript) #:use-module (crates-io))

(define-public crate-wsdom-javascript-0.0.1 (c (n "wsdom-javascript") (v "0.0.1") (d (list (d (n "ref-cast") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "wsdom-core") (r "^0.0.1") (d #t) (k 0)) (d (n "wsdom-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "wsdom-macros-decl") (r "^0.0.1") (d #t) (k 0)))) (h "1b0r91nbm6xsshwvvwpxsjhggs69n8jgj522g9m8qqg8girhn1b6")))

