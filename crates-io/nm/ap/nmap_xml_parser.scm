(define-module (crates-io nm ap nmap_xml_parser) #:use-module (crates-io))

(define-public crate-nmap_xml_parser-0.1.0 (c (n "nmap_xml_parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "roxmltree") (r "^0.11.0") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "01wzz5ksg41aqz2r3n28xz7n25rc74avbgra96n707z3nykaciln")))

(define-public crate-nmap_xml_parser-0.2.0 (c (n "nmap_xml_parser") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "roxmltree") (r "^0.11.0") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "0812liymsg5c409nzmbms4zr32dnsx0d7i9qy6xigjnyfrrc7hbf")))

(define-public crate-nmap_xml_parser-0.3.0 (c (n "nmap_xml_parser") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "roxmltree") (r "^0.11.0") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "006kjz9zgaqgapa6vgnj7pwrr4wc044mbh1vmm81xzwnl3a46g13")))

