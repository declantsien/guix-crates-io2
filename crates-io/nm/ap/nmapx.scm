(define-module (crates-io nm ap nmapx) #:use-module (crates-io))

(define-public crate-nmapx-1.0.0 (c (n "nmapx") (v "1.0.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dylvwin6b59kvfgd156yfdvg4qhym3pl4612c1pqdhvl1p1d63p")))

(define-public crate-nmapx-1.1.0 (c (n "nmapx") (v "1.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "1v9ln1njcy6bmw1dv9sgmfg4xzscj3k1cw083sqzsg70ha289w66")))

