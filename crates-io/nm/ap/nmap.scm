(define-module (crates-io nm ap nmap) #:use-module (crates-io))

(define-public crate-nmap-0.0.0 (c (n "nmap") (v "0.0.0") (h "0kb5sr8qf16cilqsbfbb53law4hsjf42vdb07a7kw4zi7nrmklsa")))

(define-public crate-nmap-0.1.3 (c (n "nmap") (v "0.1.3") (h "1xppka49aggia21apas82459jkvi80gdmyij6ws5rkkd0i5p5bdj")))

