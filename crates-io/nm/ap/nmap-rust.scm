(define-module (crates-io nm ap nmap-rust) #:use-module (crates-io))

(define-public crate-nmap-rust-0.1.0 (c (n "nmap-rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ipnet") (r "^2.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0vp079i6cx6z6djq3b02c87pvhsf8f9kyiji28b1b0blac1ld6cc")))

