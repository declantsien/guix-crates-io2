(define-module (crates-io nm t- nmt-rs) #:use-module (crates-io))

(define-public crate-nmt-rs-0.1.0 (c (n "nmt-rs") (v "0.1.0") (d (list (d (n "borsh") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "borsh") (r "^0.10.0") (d #t) (k 2)) (d (n "bytes") (r "^1") (k 0)) (d (n "postcard") (r "^1.0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)) (d (n "sha2") (r "^0.10.6") (k 0)))) (h "0c4ccjlvk9hw9hjfi81y47bxd5bm1yjjhs72qinkh3xxmwz171z7") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("serde" "dep:serde" "postcard/use-std"))))))

