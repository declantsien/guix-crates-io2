(define-module (crates-io nm os nmos6502) #:use-module (crates-io))

(define-public crate-nmos6502-1.0.0 (c (n "nmos6502") (v "1.0.0") (d (list (d (n "num_enum") (r "^0.5.11") (k 0)))) (h "0xy7wm8xhi5cy5qgyynb9v2fh9407mn9l1b8bscw8sxahndi83is")))

(define-public crate-nmos6502-1.0.1 (c (n "nmos6502") (v "1.0.1") (d (list (d (n "num_enum") (r "^0.5.11") (k 0)))) (h "1zzhvnwq1cdgqclca66n0cmdpwm9118l8k46h3v2ilrv5l9dbdd8")))

