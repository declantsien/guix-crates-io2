(define-module (crates-io nm ui nmuidi) #:use-module (crates-io))

(define-public crate-nmuidi-0.1.0 (c (n "nmuidi") (v "0.1.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)))) (h "00gamc36xk9as2kh3agcj7kzsa36nvgi19hy5gj99zzkprwipqr8")))

(define-public crate-nmuidi-0.1.1 (c (n "nmuidi") (v "0.1.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)))) (h "0d3bjwys0591366311kyxkb767qmv0p11y6h2ipc909lnfa4a4p0")))

(define-public crate-nmuidi-0.1.3 (c (n "nmuidi") (v "0.1.3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)))) (h "03x0jqcrsmzf9bprymaan3cqpmnqx5qci0qqijj2bkbdv7iqhkzm")))

(define-public crate-nmuidi-0.1.4 (c (n "nmuidi") (v "0.1.4") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)))) (h "041az8p8mjmpihhxvaly0ymkgjj4253r56vqwk54nmlyah5vdjaz")))

