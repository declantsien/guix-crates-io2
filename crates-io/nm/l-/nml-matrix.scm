(define-module (crates-io nm l- nml-matrix) #:use-module (crates-io))

(define-public crate-nml-matrix-0.1.1 (c (n "nml-matrix") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ln6246bsaapin7axz38rwnmhxaf3ya5xnycwwnq17n3cbc6m1a2")))

(define-public crate-nml-matrix-0.1.2 (c (n "nml-matrix") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04h5v92vva43b3h4asj03xzhg7hq2wh9qmnd96nifpbqz9llxdcx")))

(define-public crate-nml-matrix-0.1.3 (c (n "nml-matrix") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1b28jh424yrhbnyb06izlbqwq0himhzlvjha3j2h0gg28h6irn6f")))

(define-public crate-nml-matrix-0.1.4 (c (n "nml-matrix") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17jb9r43hw9k1qvqfnihsj629iwhlq51yn90csznvyw7a77dvk3b")))

(define-public crate-nml-matrix-0.1.5 (c (n "nml-matrix") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1591rylxbgnvx54s5gl17kgzsls9snlqfbjqkbflyhdllm1r4psd")))

(define-public crate-nml-matrix-0.2.0 (c (n "nml-matrix") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wq3kb40hh9xxq8lax9w0kga2xfiy0jnz39d5f50cnmfrc45kc3k")))

(define-public crate-nml-matrix-0.2.1 (c (n "nml-matrix") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02dy49v6h3q7fym77ldynpxyhc0nn61kjx4gskad4x1661mq0pph")))

(define-public crate-nml-matrix-0.3.1 (c (n "nml-matrix") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03da0rnrncfsa8gqvndnzr1vj1g4nkwxpjfxry1xavp2z02zia7c")))

(define-public crate-nml-matrix-0.4.1 (c (n "nml-matrix") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hsr3r3fx36drbh4s82a8vwmz8c77i1qlba0kygf8vwbbpmn76bp")))

