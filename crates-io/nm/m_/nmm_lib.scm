(define-module (crates-io nm m_ nmm_lib) #:use-module (crates-io))

(define-public crate-nmm_lib-0.1.0 (c (n "nmm_lib") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1chdnq6n4aq9yz3kmvfb74s15qfq6f72rv05ykc5g3c6v93pa0fg")))

(define-public crate-nmm_lib-0.1.1 (c (n "nmm_lib") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qy6vasvfhwhkkfpaib9xvk1mlmyrc5kn2bz8cwvlnzvn3xzcbrs")))

(define-public crate-nmm_lib-0.2.0 (c (n "nmm_lib") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03lm7nrmflk15lgp4vhcclf9zpajybjvywvab2w7vvd3sp1jd21f")))

