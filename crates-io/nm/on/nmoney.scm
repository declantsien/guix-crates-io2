(define-module (crates-io nm on nmoney) #:use-module (crates-io))

(define-public crate-nmoney-0.0.0 (c (n "nmoney") (v "0.0.0") (h "06vm133ga08lkdh684p8kqpd6rm88ygcm6lyribmz7x13690s8v9")))

(define-public crate-nmoney-1.0.0 (c (n "nmoney") (v "1.0.0") (h "1mhyv2jhgishh14jfpsvcbci92ahqqalvs4k41kn8rvsibjdi1r6")))

