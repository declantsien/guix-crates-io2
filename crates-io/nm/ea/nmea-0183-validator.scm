(define-module (crates-io nm ea nmea-0183-validator) #:use-module (crates-io))

(define-public crate-nmea-0183-validator-0.0.1 (c (n "nmea-0183-validator") (v "0.0.1") (d (list (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "13wyk805gr9jb3k1zbf358jr858wdlg0imsvhzh7qaaaj836i7x3") (y #t)))

(define-public crate-nmea-0183-validator-0.0.2 (c (n "nmea-0183-validator") (v "0.0.2") (d (list (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "1dqhqgh9zwj66sllvjjk45m39bsxkh6mi65s25ni4k9afalm3b29") (y #t)))

(define-public crate-nmea-0183-validator-0.0.3 (c (n "nmea-0183-validator") (v "0.0.3") (d (list (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "0r151iw88q136jlg4qkax2j01pw7dsqgjp62328ki2i6gn13j857")))

