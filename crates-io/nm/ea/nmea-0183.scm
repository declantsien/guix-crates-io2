(define-module (crates-io nm ea nmea-0183) #:use-module (crates-io))

(define-public crate-nmea-0183-0.0.1 (c (n "nmea-0183") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "19l8zd48w8d2a50mwaknl94rn4sjf25jvzpjvl9gbfadmbpvvnr1")))

(define-public crate-nmea-0183-0.0.2 (c (n "nmea-0183") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1yjsiar2sg0j7ajzn70ld2bcf12ffy1rv0m350f64l1783rr09rr")))

(define-public crate-nmea-0183-0.0.3 (c (n "nmea-0183") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1i4ic03g9f99h6wz6m15jiqfv6d5v84qhzph5cw3igjgifxymgnd")))

(define-public crate-nmea-0183-0.0.4 (c (n "nmea-0183") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0xr4kkakjm472wakzcx27lphcra84a3iy67nbaz41wfzbndgpdwi")))

(define-public crate-nmea-0183-0.0.5 (c (n "nmea-0183") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0a02bgz7kyk2rcadyfwmyvr28fwm5had64sjslg7qlqxcfwn8yzj")))

