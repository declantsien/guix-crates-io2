(define-module (crates-io nm ea nmea-parser) #:use-module (crates-io))

(define-public crate-nmea-parser-0.1.0 (c (n "nmea-parser") (v "0.1.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 0)) (d (n "bitvec") (r "^0.19.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1h6f3i1cyvs536cwzjkmmirbqsd9zwflms0vmdbzmiwxm149awk3")))

(define-public crate-nmea-parser-0.1.1 (c (n "nmea-parser") (v "0.1.1") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 0)) (d (n "bitvec") (r "^0.19.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1g5g1lnb7j95rp7jlyw3g72kjy29j3xv9531h6klwmwmlsylnr6m")))

(define-public crate-nmea-parser-0.2.0 (c (n "nmea-parser") (v "0.2.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 0)) (d (n "bitvec") (r "^0.19.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1rwix4x8alqi9mrf4ma7r531p8m94084yk8d2p1c3xg937p7cx5r")))

(define-public crate-nmea-parser-0.3.0 (c (n "nmea-parser") (v "0.3.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 0)) (d (n "bitvec") (r "^0.19.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0cxdsmdlfr8ssghfrhgnym652bm1pc5ficxh0xykdh7a8c4q06qk")))

(define-public crate-nmea-parser-0.3.1 (c (n "nmea-parser") (v "0.3.1") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 0)) (d (n "bitvec") (r "^0.19.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0r8ldqsrfva5rmm9vg1xid3y04nb2r5h4cbjm2zgfpsnmgrk1fnf")))

(define-public crate-nmea-parser-0.4.0 (c (n "nmea-parser") (v "0.4.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 0)) (d (n "bitvec") (r "^0.19.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "1gi9mwri6pdsj3w0j3wd9wz0xn9a8aswbrza7jz2c46sxg4ass1w")))

(define-public crate-nmea-parser-0.4.1 (c (n "nmea-parser") (v "0.4.1") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 0)) (d (n "bitvec") (r "^0.19.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "07bnvcva9760sbx4igc6frfdi8va6v9m3arm1licjwj2ipxgv69g")))

(define-public crate-nmea-parser-0.5.0 (c (n "nmea-parser") (v "0.5.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "bitvec") (r "^0.19.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0ky06rc5afnk7ynma38zmgrsx9yifsviiir0gz38nzfx335gsbfi")))

(define-public crate-nmea-parser-0.6.0 (c (n "nmea-parser") (v "0.6.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "bitvec") (r "^0.19.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "15mwqcip9xrbkz65nxlwrjcj9l20a77clpqgidqa8jr768v8d42d")))

(define-public crate-nmea-parser-0.7.0 (c (n "nmea-parser") (v "0.7.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "bitvec") (r "^0.20.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0dnhw8nvrnyld7fd45x09jsp42cgqmq0n838x59s8chy30c43n60")))

(define-public crate-nmea-parser-0.7.1 (c (n "nmea-parser") (v "0.7.1") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "bitvec") (r "^0.21.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0kd9ray5ws43ag80dg9jmhq914ymh7dqhkpgrc12z9cx5k1gh55f")))

(define-public crate-nmea-parser-0.7.2 (c (n "nmea-parser") (v "0.7.2") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "bitvec") (r "^0.21.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1qwk65h225w4b7axaa6fib3xbrfh6zzf00i6jxby0ikb2r8m2hap")))

(define-public crate-nmea-parser-0.8.0 (c (n "nmea-parser") (v "0.8.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "bitvec") (r "^0.21.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0pv26z8qv3wr8pqarx2mkmk6qyxd5h50jrfgb9b4v6jik48l4wlk")))

(define-public crate-nmea-parser-0.9.0 (c (n "nmea-parser") (v "0.9.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "bitvec") (r "^0.22.3") (f (quote ("alloc"))) (k 0)) (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "0w8k81vnar8wfbblpq2pxgj60d86d6jv8kly1k6ya06hk0yh4fzg")))

(define-public crate-nmea-parser-0.10.0 (c (n "nmea-parser") (v "0.10.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "chrono") (r "^0.4.22") (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0q82hrd6m9irbg8i7z8p3qkf1mwgj3drcmin682xd77dpxqwqkwm") (r "1.56")))

