(define-module (crates-io nm ea nmea0183-core) #:use-module (crates-io))

(define-public crate-nmea0183-core-0.1.0 (c (n "nmea0183-core") (v "0.1.0") (d (list (d (n "micromath") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)))) (h "01f65zc3wi9i5q9pbn014n5wnmlz6wyg1lgfj8s1c17ygfbkrx25") (y #t)))

(define-public crate-nmea0183-core-0.1.1 (c (n "nmea0183-core") (v "0.1.1") (d (list (d (n "micromath") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)))) (h "1z3ifbbam0zbm5dp3dmrzv6k58nqf0907vnyrxwxfkby2a2wikww")))

(define-public crate-nmea0183-core-0.1.2 (c (n "nmea0183-core") (v "0.1.2") (d (list (d (n "micromath") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)))) (h "14nli7j965icpbnqm2ygmn8j48am3dwly4f5lwgcmm0fy5m3yv40") (y #t)))

(define-public crate-nmea0183-core-0.1.3 (c (n "nmea0183-core") (v "0.1.3") (d (list (d (n "micromath") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4") (d #t) (k 2)))) (h "1cm1nx4sri3wcf03kqhz658p6zc4sihhp0rsmfzsix2nym5y5q2b")))

