(define-module (crates-io nm ea nmea0183) #:use-module (crates-io))

(define-public crate-nmea0183-0.1.0 (c (n "nmea0183") (v "0.1.0") (h "0h2zw8g3v2ffpq7ghp2hpmy1j86ncyx9rp6vk48qqjzz07w43fk0")))

(define-public crate-nmea0183-0.2.0 (c (n "nmea0183") (v "0.2.0") (h "02jp19m2x33v9fw2agqa15j1arvviw2qmj45dsky15slr3rb294g")))

(define-public crate-nmea0183-0.2.1 (c (n "nmea0183") (v "0.2.1") (h "1n8vmfdikdcxj0hqqz9d4bik8mglny75qv37fk5g9vklrp3jq7b1")))

(define-public crate-nmea0183-0.2.2 (c (n "nmea0183") (v "0.2.2") (h "0gzdxnmdpg4iz45b482icwnf1hirmk5xag9hc1zq6zxgnswl6b13")))

(define-public crate-nmea0183-0.2.3 (c (n "nmea0183") (v "0.2.3") (h "0w5gfqarwmhcwylfx5zb44d8w6s09pfqd5511das265g4cs0c7jn")))

(define-public crate-nmea0183-0.3.0 (c (n "nmea0183") (v "0.3.0") (h "1qz79cxlykhqvmxl4g09dmp0mq3a3va5crgqrnlczgd4fsskjz5y")))

(define-public crate-nmea0183-0.3.1 (c (n "nmea0183") (v "0.3.1") (h "1fznf0zz2276w8ksf68szr1sp2d59am42k3gqsgm47k9zf971v16")))

(define-public crate-nmea0183-0.4.0 (c (n "nmea0183") (v "0.4.0") (h "0g1xz2mwc4wymrkypg422p0fhzpxxqw6gmkiikcgwhi83v9ryl3w") (f (quote (("mtk") ("default"))))))

