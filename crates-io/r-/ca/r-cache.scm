(define-module (crates-io r- ca r-cache) #:use-module (crates-io))

(define-public crate-r-cache-0.1.0 (c (n "r-cache") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "0v898pda6vb0125qvnj5y3jc0y3zbh9hl3gr3y2cc3kq47knbk5s")))

(define-public crate-r-cache-0.2.0 (c (n "r-cache") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "0rjx8zqfdia8f6mcsrs4pqc3sya3vahfb7w0156c1mllx02b9kkk")))

(define-public crate-r-cache-0.2.1 (c (n "r-cache") (v "0.2.1") (d (list (d (n "async-std") (r "^1.6.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)))) (h "1hqb7yxwksgc5qp8z3562i6hqh3rv03ir8k2jwl0dbinc0g6gq2j")))

(define-public crate-r-cache-0.3.0 (c (n "r-cache") (v "0.3.0") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 0)))) (h "03v3v0iavfl6pvcggpj67bnfgjrp3ggzj1jb80w7lbgji7qv1mc4")))

(define-public crate-r-cache-0.3.1 (c (n "r-cache") (v "0.3.1") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 0)))) (h "0311hmb51q1kr3kfmaa9qakxqgwp2kqnfhk86pi2mhnyclpw6jcx")))

(define-public crate-r-cache-0.3.2 (c (n "r-cache") (v "0.3.2") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 0)))) (h "0zvg6yms0knfzvaqk7qzxanv5lwlrpyadfdzhv5nl6c2220bin7m")))

(define-public crate-r-cache-0.3.3 (c (n "r-cache") (v "0.3.3") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 0)))) (h "0ys4i9v1j1qwh782xfs5p2z5n2jsadcg7wqfybj97gcihjnqdsjh")))

(define-public crate-r-cache-0.3.4 (c (n "r-cache") (v "0.3.4") (d (list (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 0)))) (h "0jigajas6qs19b0mqy12fksijfcy1ir9g9xjf5xkpp4n7sx6jm34")))

(define-public crate-r-cache-0.4.0 (c (n "r-cache") (v "0.4.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)))) (h "1y5vyxz55xxvf4039m3padzlpz3ncaf6xcb6wx7a36917yf3c5rh")))

(define-public crate-r-cache-0.4.1 (c (n "r-cache") (v "0.4.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)))) (h "1nrk63aycg9ngcixxjzm5m51qvqydpxgs9fclkzysddql5m0yd8a")))

(define-public crate-r-cache-0.4.2 (c (n "r-cache") (v "0.4.2") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "1gs0mjnfa8cl4ma677lsg0kmih7vsxl9h7mps46wripkkp7b83rx")))

(define-public crate-r-cache-0.4.3 (c (n "r-cache") (v "0.4.3") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0dx94wfy1gcvf7c7k1sa8q8cg72330cjaaa1szvwa8iphpx7fsfv")))

(define-public crate-r-cache-0.4.4 (c (n "r-cache") (v "0.4.4") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "13al31ll65cd0pqqm5fgnyvhay4774afixf2vddii7hw8yy0r5ay")))

(define-public crate-r-cache-0.4.5 (c (n "r-cache") (v "0.4.5") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "094c09y7r6wjqbqv5rvm4j7nj5v7n7plgggcig4x7yi6nnwf51s5")))

(define-public crate-r-cache-0.5.0 (c (n "r-cache") (v "0.5.0") (d (list (d (n "dashmap") (r "^5.4") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0yrz3idw8za0b8711g43lblca18w0z7s9d35sk6c3njw0bhfk883")))

