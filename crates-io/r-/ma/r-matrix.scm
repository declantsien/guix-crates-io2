(define-module (crates-io r- ma r-matrix) #:use-module (crates-io))

(define-public crate-r-matrix-0.1.1 (c (n "r-matrix") (v "0.1.1") (d (list (d (n "chan") (r "^0.1.23") (d #t) (k 0)) (d (n "chan-signal") (r "^0.3") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "0ma6jh7xyphx8dzc69f9s62bk0laiqm71llcl3rmhp493g9is77w")))

(define-public crate-r-matrix-0.2.0 (c (n "r-matrix") (v "0.2.0") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.8") (f (quote ("extended-siginfo"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1lm8g197gbh9gr2hvpbi7n50n6cpa00w8ppyjx4nlz2y7m6l7v9j")))

(define-public crate-r-matrix-0.2.1 (c (n "r-matrix") (v "0.2.1") (d (list (d (n "pancurses") (r "^0.16.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.10") (f (quote ("extended-siginfo"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "00qwbfm419lhjp1sk1a4a78dcb9xmy0cn7sp5wpaz7yzcvgjyci9")))

(define-public crate-r-matrix-0.2.2 (c (n "r-matrix") (v "0.2.2") (d (list (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (f (quote ("extended-siginfo"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0ak7y5k8s96f1widjwifsrr5f76d47cngq8639p20hpb9rnxfngs")))

(define-public crate-r-matrix-0.2.3 (c (n "r-matrix") (v "0.2.3") (d (list (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (f (quote ("extended-siginfo"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0m54a4knhfmcbwk8bmxbkkq26pjfvw4bi64lln3dqlmbfqc40kzq")))

