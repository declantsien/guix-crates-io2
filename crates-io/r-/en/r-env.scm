(define-module (crates-io r- en r-env) #:use-module (crates-io))

(define-public crate-r-env-0.1.4 (c (n "r-env") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "13a0nmp7asbp6l5rknm565xbxwkhpilg13lvgzvqsjrqds7rjchc")))

(define-public crate-r-env-0.1.5 (c (n "r-env") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "1i8mbpg746f2x35gv9kji3kxadv6fz78q7lb90v3gl23v73w8kdd")))

(define-public crate-r-env-0.9.0 (c (n "r-env") (v "0.9.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "1cq18l8jrw2fwjw0842naldf0mc3m7fp8y6fl0x1y06yixwghz4f")))

(define-public crate-r-env-0.9.1 (c (n "r-env") (v "0.9.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "1hfpaqdag21p7rg4y4h79yhd6l09na9xvn73j0cwvyjkc2j2bp7z")))

