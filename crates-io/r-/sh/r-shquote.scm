(define-module (crates-io r- sh r-shquote) #:use-module (crates-io))

(define-public crate-r-shquote-0.1.0 (c (n "r-shquote") (v "0.1.0") (h "0zm9pijkb4bv26nmxr20f51h83q1ryjg7c9fdq15c18j2fgxdafv")))

(define-public crate-r-shquote-0.1.1 (c (n "r-shquote") (v "0.1.1") (h "04gpw0h3kc7ysbhpr4nbij3rdv7qxjdcqllbmlq0n2r45hrplsyp")))

