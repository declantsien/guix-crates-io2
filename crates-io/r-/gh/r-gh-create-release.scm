(define-module (crates-io r- gh r-gh-create-release) #:use-module (crates-io))

(define-public crate-r-gh-create-release-1.0.49 (c (n "r-gh-create-release") (v "1.0.49") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "regex") (r "^1.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1jn3m4z6qcvng496kay9z2smbadph9n4c55r5cvxww552d789x71")))

(define-public crate-r-gh-create-release-1.0.50 (c (n "r-gh-create-release") (v "1.0.50") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "regex") (r "^1.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1f3hip6l26mdzk4g3nfv1sz6piwskcjhjyikr2y9r553ilzy7rv4")))

(define-public crate-r-gh-create-release-1.0.51 (c (n "r-gh-create-release") (v "1.0.51") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "regex") (r "^1.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "0k0hpld1byhhi36wymv0y4r33vm1lw8fnzk10r3m38cjf5va6a07")))

(define-public crate-r-gh-create-release-1.0.53 (c (n "r-gh-create-release") (v "1.0.53") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "regex") (r "^1.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.164") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1lmg4w2fif534baa0vv82xb11h3hwswg0x4361xrjxvfk8p58cg0")))

