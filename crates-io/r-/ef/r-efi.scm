(define-module (crates-io r- ef r-efi) #:use-module (crates-io))

(define-public crate-r-efi-0.1.0 (c (n "r-efi") (v "0.1.0") (h "1gxpy4yp8k0vghkzqfckhd0jngzj3zrv8qkzmzkavq3whsswnxfq")))

(define-public crate-r-efi-0.1.1 (c (n "r-efi") (v "0.1.1") (h "0ahz8q86m9lqz8d9sxmq7m293r4dl21vpxgkvv5gbmfs0ql8iay0") (f (quote (("examples"))))))

(define-public crate-r-efi-1.0.0 (c (n "r-efi") (v "1.0.0") (h "0qkybbxb6il7df8bfvz8q3w2880qmn917d3n681cfyqc40z08xrc") (f (quote (("examples"))))))

(define-public crate-r-efi-2.0.0 (c (n "r-efi") (v "2.0.0") (h "01g4y12jlj3vc0nwrqbr6a5n38k291k48y0qdbmb7kbgz03ng3hd") (f (quote (("examples"))))))

(define-public crate-r-efi-2.1.0 (c (n "r-efi") (v "2.1.0") (h "0gdk5gf4a8vv8wzfny2bfy5zm95p2pkkbwnjr6j56w1nayg7yb4g") (f (quote (("examples"))))))

(define-public crate-r-efi-2.2.0 (c (n "r-efi") (v "2.2.0") (h "12276qb7f1v2xbfq76a6sjkzkf1h9ylz7nd0snr4wscdapmcwjkv") (f (quote (("examples"))))))

(define-public crate-r-efi-3.0.0 (c (n "r-efi") (v "3.0.0") (h "17rrxpn9h6gpc5qif415vyajya77nfdip6h4vkhnnyal6z447n7h") (f (quote (("examples"))))))

(define-public crate-r-efi-3.1.0 (c (n "r-efi") (v "3.1.0") (h "1pc3drinkpzkavpzvl4q0xnl58490qx3xpl22da1iich5cgviakf") (f (quote (("examples"))))))

(define-public crate-r-efi-3.2.0 (c (n "r-efi") (v "3.2.0") (h "0yxahgn4mwnzkwnghzy7pxycddc9jlxyq2913w2wzn8gj2d9k631") (f (quote (("examples"))))))

(define-public crate-r-efi-4.0.0 (c (n "r-efi") (v "4.0.0") (h "1f71ydg7gfxky150nk6kncwpcclklwrifcf073wxfr5kzyk5fhkc") (f (quote (("examples"))))))

(define-public crate-r-efi-4.1.0 (c (n "r-efi") (v "4.1.0") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1wz6vikh4h86c8wxxhzkq4yq262nm65a49xhwx2nfg434b34awwy") (f (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("examples") ("efiapi"))))))

(define-public crate-r-efi-4.2.0 (c (n "r-efi") (v "4.2.0") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "04f939yk94c91dss2c46gjbns9gciyjfrxnxzayssm6sngcw4psp") (f (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("native") ("examples" "native") ("efiapi"))))))

(define-public crate-r-efi-4.3.0 (c (n "r-efi") (v "4.3.0") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1q7j3bf4a5dh5lx8v32qp165jmr6sixidlr1wpwnfc1sw2b4y90f") (f (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("native") ("examples" "native") ("efiapi")))) (r "1.68")))

(define-public crate-r-efi-4.4.0 (c (n "r-efi") (v "4.4.0") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0d3saq12k8jfqr96k1jyw7156xqp7lr0a11vnws0dk646vv9cwf4") (f (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("native") ("examples" "native") ("efiapi")))) (r "1.68")))

(define-public crate-r-efi-4.5.0 (c (n "r-efi") (v "4.5.0") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "18w6vl7ir0yv82vkmk7r7gnf1pk9vhcfzjd4q3giaiw5qppkbsg9") (f (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("native") ("examples" "native") ("efiapi")))) (r "1.68")))

