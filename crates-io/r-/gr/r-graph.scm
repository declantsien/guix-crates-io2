(define-module (crates-io r- gr r-graph) #:use-module (crates-io))

(define-public crate-r-graph-0.1.0 (c (n "r-graph") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "17hk69pddd7ym25m1102l1ayw1vwvbs4wzcjcr3jz4vq2ym5bnww") (y #t)))

(define-public crate-r-graph-0.0.1 (c (n "r-graph") (v "0.0.1") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1b874l222fcbv83s21srxixgck5z1z4dcnyvmyp00lvbllrk835r") (y #t)))

(define-public crate-r-graph-0.0.11 (c (n "r-graph") (v "0.0.11") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "19nb8gjbppajawhwqvsha13fb7alcj0pvagmx8q4rfz4hx28hifm")))

(define-public crate-r-graph-0.0.12 (c (n "r-graph") (v "0.0.12") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "17gvka7fq3h8zjfjnv02q6ar6f89s0chjdnbvqa9678z6lqwpywl")))

(define-public crate-r-graph-0.0.13 (c (n "r-graph") (v "0.0.13") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "149y5q2bb397jg7rkfiih37kxqwqjxp4yvaq9cs7qmfx6f5bhii1")))

(define-public crate-r-graph-0.0.14 (c (n "r-graph") (v "0.0.14") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "06h5i5vgf56zcw4cbn9pxfsc8ys4xshgdrxsmjwrmv15ww0k29nx")))

(define-public crate-r-graph-0.1.1 (c (n "r-graph") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1z5ap1n0wqjshs76i4pv1iq9ab5rww69gglnldr8aacr2rdkqcdb")))

(define-public crate-r-graph-0.1.2 (c (n "r-graph") (v "0.1.2") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0gsmaya08ax2kiz24fw4vak32xvc6qpk0p1f8cibg4rgg0c05w2l")))

(define-public crate-r-graph-0.1.3 (c (n "r-graph") (v "0.1.3") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "060nq9pacnnrgpx9grv5988c8bi5vbcpc9iwfcg9p2ml3cnk4c2m")))

(define-public crate-r-graph-0.1.5 (c (n "r-graph") (v "0.1.5") (d (list (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "192rfys8raipfvjij1d500cnjfbfbhyn1h3im9kr7gqpxn9v6vl7")))

