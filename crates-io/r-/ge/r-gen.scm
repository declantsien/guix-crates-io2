(define-module (crates-io r- ge r-gen) #:use-module (crates-io))

(define-public crate-r-gen-0.0.1 (c (n "r-gen") (v "0.0.1") (d (list (d (n "probability") (r "^0.17.0") (d #t) (k 0)) (d (n "r-gen-macro") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "1kqpsnffwf1gfkqzv6jqn3qbh482xpsj61yb6rcdab32wsaz045p") (y #t)))

(define-public crate-r-gen-0.0.2 (c (n "r-gen") (v "0.0.2") (d (list (d (n "probability") (r "^0.17.0") (d #t) (k 0)) (d (n "r-gen-macro") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "0cipy0lhzn5piaw5ij66lnxcyb7gxg69k2jljfzxzzp69vz48yg5")))

