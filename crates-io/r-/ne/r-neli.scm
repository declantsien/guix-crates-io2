(define-module (crates-io r- ne r-neli) #:use-module (crates-io))

(define-public crate-r-neli-0.1.0 (c (n "r-neli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "rdkafka") (r "^0.26") (f (quote ("cmake-build"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zq3rcqvnv4p53zcqq7rv0jm7scsm2kra33vg7cjl0b3rjisq0lj")))

(define-public crate-r-neli-0.1.1 (c (n "r-neli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "rdkafka") (r "^0.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yjqfjvmv2bwbs5fg4q7sj5d5r1mblz10394ikri1qm1zzzvk2nk")))

(define-public crate-r-neli-0.1.3 (c (n "r-neli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "rdkafka") (r "^0.27") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1i8i3qw046k228fbcd82hady4jp1v4d73lng0a3fgrvc0hkjrn8f")))

(define-public crate-r-neli-0.1.4 (c (n "r-neli") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "rdkafka") (r "^0.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "175hwr8pipwzz2ql7rhfirp1nczkqr0sdrqp4zrs701zgyi2cshj")))

