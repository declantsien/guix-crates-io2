(define-module (crates-io r- to r-tool) #:use-module (crates-io))

(define-public crate-r-tool-0.1.0 (c (n "r-tool") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "cargo-html") (r "^0.2.1") (d #t) (k 2)) (d (n "cargo-tarpaulin") (r "^0.27.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "zip") (r "^0.6") (d #t) (k 0)) (d (n "zip-extensions") (r "^0.6") (d #t) (k 0)))) (h "0n55wl4r0xxs6vp9is1a6n4pvf60cvprc3gjbrv0ppw1jjpism9c")))

