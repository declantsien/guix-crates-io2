(define-module (crates-io r- ex r-ex) #:use-module (crates-io))

(define-public crate-r-ex-1.0.0 (c (n "r-ex") (v "1.0.0") (h "1nyr4vx3gq8d63jxb6a2wngcvc24ywlknlnn7zn5dzz0bdqbgfsz") (f (quote (("splice") ("from-ref") ("from-arr") ("endian") ("default") ("copy-from") ("carve"))))))

(define-public crate-r-ex-1.0.1 (c (n "r-ex") (v "1.0.1") (h "16mv4xnlzc4l5cm9795cpvn949yk131x8q4q4l8092ag7aa6lxs2") (f (quote (("splice") ("from-ref") ("from-arr") ("endian") ("default") ("copy-from") ("carve"))))))

