(define-module (crates-io r- lo r-lombok-macros) #:use-module (crates-io))

(define-public crate-r-lombok-macros-0.0.1 (c (n "r-lombok-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1klamxvgbs6lyidd67r68a811x5gnbg05avk7xch2ik6iv22l0kx")))

(define-public crate-r-lombok-macros-0.0.2 (c (n "r-lombok-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "14av63yh0nak2szdhbkww67cn7ywdprq21s27yj9zma1hmymprs2")))

(define-public crate-r-lombok-macros-0.0.3 (c (n "r-lombok-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0ihmha8cpsl6qsbbcw74sxmf4mwqwa0wpr9agcfrqkf5am8xxshh")))

