(define-module (crates-io #{3}# p pns) #:use-module (crates-io))

(define-public crate-pns-0.1.0 (c (n "pns") (v "0.1.0") (h "1grkhbj918yni3fkip6xqfzp513li90vfc80k9x72yawb2630vhi") (l "pns")))

(define-public crate-pns-0.2.0 (c (n "pns") (v "0.2.0") (h "0w6idmn9ijdcnx7df8ffnplzln4n7lxa6n6wmks50fjhmhxm0z2v") (l "pns")))

(define-public crate-pns-0.2.1 (c (n "pns") (v "0.2.1") (h "0i4iq5k80x1ilk3nijf6wdkhl2bdb1rks6yza6aj8xzv0fr3c03l") (l "pns")))

(define-public crate-pns-0.3.0 (c (n "pns") (v "0.3.0") (h "1c32sghn6kdlrzygfwaf66y0hkpdlbpysygclarfwsymrz84hx8q") (l "pns")))

(define-public crate-pns-0.4.0 (c (n "pns") (v "0.4.0") (h "1rv0bv3slb6zp8bppmj48z6c2lj6lyzxgllsvj0753cc8hzv5xis") (l "pns")))

(define-public crate-pns-0.4.1 (c (n "pns") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0flwfcxmdj765spxi6g1ii7kd2xn2fsvmc6nhia08rs0lw4knq03") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.5.0 (c (n "pns") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1hak3qi1c38bc24r9bchcqikxbgidi5kdacyqfd44cfiji5f6b4b") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.5.1 (c (n "pns") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "06v6h7gcyrvjz75avpbsc8d7sk47b3vs6d188gx262wzp586ay92") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.5.2 (c (n "pns") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "12xvmyql7yjz9f5i6ihp5idjky1pxsrgprkz4p45x6pdqinxnx54") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.6.0 (c (n "pns") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1xxsxh1vpmw3ay0x88rxp1nlf7i8gvnbx59i6b2nayclshcv5pn3") (f (quote (("static-build" "cc")))) (y #t) (l "pns")))

(define-public crate-pns-0.6.1 (c (n "pns") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1ly8a333r0nbnj072cq268dz4xd625cisbs0zilhnbwa5k795cs7") (f (quote (("static-build" "cc")))) (y #t) (l "pns")))

(define-public crate-pns-0.6.2 (c (n "pns") (v "0.6.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0j52rhg1kapyf9f78j5c19v5g3p1iab20c5hrhbg32jsxw6lh676") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.7.0 (c (n "pns") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "09q3hmn77ymc86s571c0g1afi85ps8aa4k7xv0kfzz9kzqy4f9nd") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.7.1 (c (n "pns") (v "0.7.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0g907zf54zdlls6rarqczq0vrdhk5zmbbin80xjvjcigk1n992hd") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.7.2 (c (n "pns") (v "0.7.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "164k1sczmk4xl4crichjaiy1apsva2c6qcn8w67cx6bz3zriy258") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.8.0 (c (n "pns") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "11r2qcka3z0q2x1hdmahj61y8h1nw5zg891lx592a5i8shhxapmn") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.9.0 (c (n "pns") (v "0.9.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "054dvs623phhsfab2br697j742cjdjf7s2ij0y2wxjjvvymwxws5") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.10.0 (c (n "pns") (v "0.10.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1fpyd1yypxp0jd0lkmbn20725nh0v9y7ahngzdg31daljz2avnz3") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.10.1 (c (n "pns") (v "0.10.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "data-stream") (r "^0.1.0") (d #t) (k 0)))) (h "0m58m2rnvgnhjp9j55710kn4k1hyf3yg84xgb3nnwx294biq4pka") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.11.0 (c (n "pns") (v "0.11.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "data-stream") (r "^0.1.0") (d #t) (k 0)))) (h "0i7hfa8wl36j0chva1wpsx1ycg8zddgg47irkxlfaacf8629ivx5") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.12.0 (c (n "pns") (v "0.12.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "data-stream") (r "^0.2.0") (d #t) (k 0)))) (h "1mxbp6l50icrgyi4yznchmzz9ifq1ssv7218ziixlsmdji4bd3i4") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.13.0 (c (n "pns") (v "0.13.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "data-stream") (r "^0.2.0") (d #t) (k 0)))) (h "0a68l0imjxzw3kih1lmgr6radjq5khb5pywb5hg71i65np9mmn9d") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.14.0 (c (n "pns") (v "0.14.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "data-stream") (r "^0.2.0") (d #t) (k 0)))) (h "1408srxchzlz3423568nys3aja1q1iv85lnvdx2c0581sbxphg2k") (f (quote (("static-build" "cc")))) (l "pns")))

(define-public crate-pns-0.15.0 (c (n "pns") (v "0.15.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "data-stream") (r "^0.2.0") (d #t) (k 0)))) (h "0ygwi4lgkrhvslbi8alv5z3k8vzhjlrgv9x74dpgxl36inacgi34") (f (quote (("static-build" "cc")))) (l "pns")))

